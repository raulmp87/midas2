package mx.com.afirme.midas2.service.impl.siniestros.valuacion.ordencompra;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.valuacion.ordencompra.OrdenCompraDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCalculoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.ClaveSubCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoEstimacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoDocumentoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicos;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicosConductor;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCBienes;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCPersonas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoAjuste;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionDeducible;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.CartaPago;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.permisos.OrdenCompraAutorizacion;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.OrdenCompraProveedorDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.OrdenCompraRecuperacionDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.BandejaOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.DetalleOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImpresionOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImpresionOrdenDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.OrdenCompraDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.UtileriasService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.conceptos.ConceptoAjusteService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.CartaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal.PerdidaTotalService;
import mx.com.afirme.midas2.service.siniestros.pagos.PagosSiniestroService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionDeducibleService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.permisos.OrdenCompraAutorizacionService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;



@Stateless
public class OrdenCompraServiceImpl implements OrdenCompraService{	

	private static final Logger LOG = Logger.getLogger(OrdenCompraServiceImpl.class);

	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private CatalogoGrupoValorService  catalogoGrupoValorService;
	
	@EJB
	private BitacoraService bitacoraService;	
	
	@EJB
	private OrdenCompraDao ordenCompraDao;	
	
	@EJB
	private ListadoService  listadoService;
	
	@EJB 
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@EJB
	private PerdidaTotalService perdidaTotalService;
	
	@EJB
	private ParametroGlobalService parametroGlobalService ; 	
	
	@EJB
	private OrdenCompraAutorizacionService ordenCompraAutorizacionService;	

	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;
	
	@EJB
	private RecuperacionDeducibleService recuperacionDeducibleService;
	
	@EJB
	private CartaSiniestroService cartaSiniestroService;
	
	@EJB
	private UtileriasService utileriasService;

	private static final String ESTATUS_CONCEPTO_TRAMITE ="Tramite";
	
	
	private String ESTATUS_AUT_PENDIENTE  ="0";
	private String ESTATUS_AUT_AUTORIZADA  ="1";
	public static final int MIDAS_APP_ID = 5;
	public static final String PARAMETRO = "VARIACION_PORC_ORDENCOMPRA";
	
	private static BigDecimal zero =BigDecimal.ZERO;	
	private static BigDecimal cien = new BigDecimal(100);	
	
	@Override
	public List<OrdenCompraDTO> obtenerOrdenesCompraSiniestro(
			Long idReporteCabina, String tipo) {
		List<OrdenCompraDTO>  listaDto = this.ordenCompraDao.obtenerOrdenesCompraSiniestro(idReporteCabina, tipo);
		return listaDto;
		
		
	}


	@Override
	public void eliminarConcepto(Long idDetalleOrdenCompra) {
		DetalleOrdenCompra detalleOrdenCompra =entidadService.findById(DetalleOrdenCompra.class, idDetalleOrdenCompra);
		entidadService.remove(detalleOrdenCompra);
		this.bitacoraService.registrar(TIPO_BITACORA.ORDEN_COMPRA, EVENTO.ORDENCOMPRA_BAJA_CONCEPTO, idDetalleOrdenCompra.toString(), "ELIMINAR DETALLE ", detalleOrdenCompra, usuarioService.getUsuarioActual().getNombreUsuario() );

		
	}

	@Override
	public void guardarConcepto(DetalleOrdenCompra detalleOrdenCompra,
			Long idOrdenCompr) {
		detalleOrdenCompra.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
		detalleOrdenCompra.setFechaModificacion(new Date());
		if(null==detalleOrdenCompra.getId()){			
			detalleOrdenCompra.setFechaModificacion(new Date());
			detalleOrdenCompra.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
		}
		
		if(null==detalleOrdenCompra.getImporte()){
			detalleOrdenCompra.setImporte(zero);
		}
		if(null==detalleOrdenCompra.getImportePagado()){
			detalleOrdenCompra.setImportePagado(zero);
		}
		if(null==detalleOrdenCompra.getIva()){
			detalleOrdenCompra.setIva(zero);
		}
		if(null==detalleOrdenCompra.getPorcIva()){
			detalleOrdenCompra.setPorcIva(zero);
		}
		
		
		if(null==detalleOrdenCompra.getPorcIvaRetenido()){
			detalleOrdenCompra.setPorcIvaRetenido(zero);
		}
		if(null==detalleOrdenCompra.getIvaRetenido()){
			detalleOrdenCompra.setIvaRetenido(zero);
		}
		
		
		if(null==detalleOrdenCompra.getIsr()){
			detalleOrdenCompra.setIsr(zero);
		}
		if(null==detalleOrdenCompra.getPorcIsr()){
			detalleOrdenCompra.setPorcIsr(zero);
		}
		List<DetalleOrdenCompra>  detalle=this.entidadService.findByProperty(DetalleOrdenCompra.class, "ordenCompra.id", idOrdenCompr);
		detalleOrdenCompra.setEstatus(ESTATUS_CONCEPTO_TRAMITE);		
		detalle.add(detalleOrdenCompra);		
		OrdenCompra ordenCompra=this.obtenerOrdenCompra(idOrdenCompr);
		ordenCompra.setDetalleOrdenCompras(detalle);
		detalleOrdenCompra.setOrdenCompra(ordenCompra);
		this.entidadService.save(detalleOrdenCompra);
		this.bitacoraService.registrar(TIPO_BITACORA.ORDEN_COMPRA, EVENTO.ORDENCOMPRA_ALTA_CONCEPTO, detalleOrdenCompra.getId().toString(), "ALTA DETALLE ", detalleOrdenCompra, usuarioService.getUsuarioActual().getNombreUsuario() );

		
	}

	@Override
	public Long guardarOrdenCompra(OrdenCompra ordenCompra) {
		OrdenCompra ordenCompraGuardada = null;
		boolean crear=false;
		String usuarioActual = usuarioService.getUsuarioActual().getNombreUsuario();
		if(null==ordenCompra.getId()){
			ordenCompra.setCodigoUsuarioCreacion(usuarioActual);
			ordenCompra.setFechaCreacion(new Date());
			crear=true;
		}else{
			ordenCompra.setCodigoUsuarioModificacion(String.valueOf(  usuarioActual ));
			ordenCompra.setFechaModificacion(new Date());
		}
		if(ordenCompra.getAplicaDeducible()==null ){
			ordenCompra.setAplicaDeducible(false);
		}
		
		CartaPago cartaPago = this.validaSiAplicaCartaPago(ordenCompra,usuarioActual);
		ordenCompraGuardada = entidadService.save(ordenCompra);
		if(cartaPago!=null){
			cartaPago.setOrdenCompra(ordenCompraGuardada);
			this.entidadService.save(cartaPago);
		}
		
		if (crear){
			this.bitacoraService.registrar(TIPO_BITACORA.ORDEN_COMPRA, EVENTO.CREACION_ORDENCOMPRA, ordenCompra.getId().toString(), "CREACION ORDEN COMPRA ", ordenCompra, usuarioService.getUsuarioActual().getNombreUsuario() );
		}else{
			this.bitacoraService.registrar(TIPO_BITACORA.ORDEN_COMPRA, EVENTO.MODIFICACION_ORDENCOMPRA, ordenCompra.getId().toString(), "MODIFICACION ORDEN COMPRA ["+ordenCompra.getEstatus()+"]", ordenCompra, usuarioService.getUsuarioActual().getNombreUsuario() );
		}
		
		return ordenCompraGuardada.getId();
	}
	
	
	private CartaPago validaSiAplicaCartaPago(OrdenCompra ordenCompra,String usuarioActual){
		CartaPago cartaPagoFinal = null; 
		if(ordenCompra.getTipoPago().equals(OrdenCompraService.PAGO_A_PROVEEDOR) && ordenCompra.getIdBeneficiario()!=null){
			CartaPago cartaPagoDTO = ordenCompra.getCartaPago();  
			ordenCompra.setCartaPago(null);
			PrestadorServicio proveedor = this.entidadService.findById(PrestadorServicio.class, ordenCompra.getIdBeneficiario());
			for(TipoPrestadorServicio tipo : proveedor.getTiposPrestador()){
				if(tipo.getNombre().equals("CIA")){
					CartaPago cartaPago = ordenCompra.getCartaPago();
					if(ordenCompra.getId()== null && cartaPago!=null){
						cartaPago.setOrdenCompra(ordenCompra);
						cartaPago.setFechaCreacion(new Date());
						cartaPago.setCodigoUsuarioCreacion(usuarioActual);	
						ordenCompra.setCartaPago(cartaPago);
						this.entidadService.save(cartaPago);
						cartaPagoFinal = cartaPago;
					}else{
						List<CartaPago> cartas = this.entidadService.findByProperty(CartaPago.class, "ordenCompra.id",ordenCompra.getId());
						CartaPago cartaAGuardar = new CartaPago();
						if(!cartas.isEmpty()){
							cartaAGuardar = cartas.get(0);
						}
						cartaAGuardar.setCodigoUsuarioCreacion(usuarioActual);
						cartaAGuardar.setCodigoUsuarioModificacion(usuarioActual);
						cartaAGuardar.setFechaModificacion(new Date());
						if(cartaPagoDTO!=null){
							cartaAGuardar.setSiniestroTercero(cartaPagoDTO.getSiniestroTercero());
							cartaAGuardar.setFechaCreacion(cartaPagoDTO.getFechaCreacion());
							cartaAGuardar.setFechaRecepcion(cartaPagoDTO.getFechaRecepcion());
						}
						this.entidadService.save(cartaAGuardar);
						cartaPagoFinal = cartaAGuardar;
					}
					break;
				}
			}
		}else{
			ordenCompra.setCartaPago(null);
		}
		return cartaPagoFinal;
	}

	
	
	
	private OrdenCompraAutorizacion obtenerAutorizacionesPorNivel (Long idOrdenCompra,Integer nivel ){
		/*autorizaciones*/
		Map<String, Object> paramsAutorizacion = new HashMap<String, Object>();
		paramsAutorizacion.put("orderCompra.id",idOrdenCompra );
		paramsAutorizacion.put("id.nivel",nivel );
		paramsAutorizacion.put("estatus",ESTATUS_AUT_AUTORIZADA );	
		List<OrdenCompraAutorizacion>  autorizacionesAut= this.entidadService.findByProperties(OrdenCompraAutorizacion.class, paramsAutorizacion);
		paramsAutorizacion.clear();
		paramsAutorizacion.put("orderCompra.id",idOrdenCompra );
		paramsAutorizacion.put("id.nivel",nivel );
		paramsAutorizacion.put("estatus",ESTATUS_AUT_PENDIENTE );	
		List<OrdenCompraAutorizacion>  autorizacionesPend= this.entidadService.findByProperties(OrdenCompraAutorizacion.class, paramsAutorizacion);
		autorizacionesAut.addAll(autorizacionesPend);
		if(!autorizacionesAut.isEmpty()){
			return autorizacionesAut.get(0);
		}else 
			return null;
	}
	
	@Override
	public TransporteImpresionDTO imprimirOrdenCompra(Long idOrdenCompra) {
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		OrdenCompra ordenCompra= this.obtenerOrdenCompra(idOrdenCompra);
		// Creación de los datasources que contienen la información a mapear en el jrxml
		List<DetalleOrdenCompraDTO> dataSourceDetalleOrdenCompraDTO = this.obtenerListaDetallesOrdenDTO(idOrdenCompra,null,false);		
		ImpresionOrdenCompraDTO impresionDto = this.detalleImpresion(idOrdenCompra);
		
		// Compilado de jReports y generación del .jasper
		JasperReport jReport = gImpresion.getOJasperReport(impresionDto.getReporte());
		String jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionConceptosOrdenCompra.jrxml";
		JasperReport jReportDetalle = gImpresion.getOJasperReport(jrxml);
		
		List<ImpresionOrdenDTO> dataSourceimpresion =new ArrayList<ImpresionOrdenDTO>();
		ImpresionOrdenDTO dimpresionDTO = new ImpresionOrdenDTO();
		dimpresionDTO.setDetalleOrdenCompraDTO(dataSourceDetalleOrdenCompraDTO);
		dimpresionDTO.setImpresionDto(impresionDto);
		dataSourceimpresion.add(dimpresionDTO); 
		// Se definen los parámetros a mapear en el jrxml
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.LOGO_SEGUROS_AFIRME);
		params.put("dataSourceimpresion", dataSourceimpresion);
		params.put("jReportDetalle", jReportDetalle);
		if (ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_GASTOAJUSTE))
			params.put("titulo",idOrdenCompra+". GASTO DE AJUSTE" ); 
		else if (ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE)){
			params.put("titulo",idOrdenCompra+". REEMBOLSO DE GASTO DE AJUSTE "); 

		}else		
			params.put("titulo",idOrdenCompra+". ORDEN DE COMPRA "+ impresionDto.getCobertura() ); 
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceimpresion);
		
		// Rellena el reporte con los datos y exporta el PDF
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
	
	} 
	public ImpresionOrdenCompraDTO detalleImpresion(Long idOrdenCompra) {
		ImpresionOrdenCompraDTO impresionDto = new ImpresionOrdenCompraDTO();
		OrdenCompra orden= this.obtenerOrdenCompra(idOrdenCompra);
		impresionDto.setIdOrdenCompra(orden.getId());
		impresionDto.setReporte(ImpresionOrdenCompraDTO.JRXML_GRAL);
		if(null!=orden.getReporteCabina() && null!=orden.getReporteCabina().getFechaOcurrido()){
			impresionDto.setFechaSiniestro(Utilerias.cadenaDeFecha(orden.getReporteCabina().getFechaOcurrido() ,"dd/MM/yyyy"));			
		}
		impresionDto.setFechaElaboracion(Utilerias.cadenaDeFecha(orden.getFechaCreacion() ,"dd/MM/yyyy"));
		ImportesOrdenCompraDTO importes = this.calcularImportes(orden.getId(),null,false);
		try {
			BeanUtils.copyProperties(impresionDto, importes);
		} catch (IllegalAccessException e) {
			LOG.error(e);
		} catch (InvocationTargetException e) {
			LOG.error(e);
		}
		OrdenCompraAutorizacion autorizacion = null;
		Usuario user= null;
			try {
			 autorizacion=this.obtenerAutorizacionesPorNivel(idOrdenCompra,ImpresionOrdenCompraDTO.AUTORIZA_ELABORO);
			 user=usuarioService.buscarUsuarioPorNombreUsuario(autorizacion.getCodigoUsuario());
			if(null!=user){
				if(!StringUtil.isEmpty(user.getNombreCompleto())){
					impresionDto.setNombreElaboro(user.getNombreCompleto());
					String fecha = Utilerias.cadenaDeFecha(autorizacion.getFechaAutorizacion() ,"dd/MM/yyyy");
					impresionDto.setFechaElaboro(fecha);
				}
			}
		}catch (Exception e){
			LOG.error(e);
		}
		try {
			 autorizacion=this.obtenerAutorizacionesPorNivel(idOrdenCompra,ImpresionOrdenCompraDTO.AUTORIZA_SUPERVISOR);
			 user=usuarioService.buscarUsuarioPorNombreUsuario(autorizacion.getCodigoUsuario());
			if(null!=user){
				if(!StringUtil.isEmpty(user.getNombreCompleto())){
					impresionDto.setNombreSupervisor(user.getNombreCompleto());
					String fecha = Utilerias.cadenaDeFecha(autorizacion.getFechaAutorizacion() ,"dd/MM/yyyy");
					impresionDto.setFechaSupervisor(fecha);
				}
			}
		}catch (Exception e){
			LOG.error(e);
		}
		
		try {
			autorizacion=this.obtenerAutorizacionesPorNivel(idOrdenCompra,ImpresionOrdenCompraDTO.AUTORIZA_GERENCIA);
			 user=usuarioService.buscarUsuarioPorNombreUsuario(autorizacion.getCodigoUsuario());
			if(null!=user){
				if(!StringUtil.isEmpty(user.getNombreCompleto())){
					impresionDto.setNombreGerencia(user.getNombreCompleto());
					String fecha = Utilerias.cadenaDeFecha(autorizacion.getFechaAutorizacion() ,"dd/MM/yyyy");
					impresionDto.setFechaGerencia(fecha);
				}
			}
		}catch (Exception e){
			LOG.error(e);
		}
		try {
			 autorizacion=this.obtenerAutorizacionesPorNivel(idOrdenCompra,ImpresionOrdenCompraDTO.AUTORIZA_DIRECCION);
			 user=usuarioService.buscarUsuarioPorNombreUsuario(autorizacion.getCodigoUsuario());
			if(null!=user){
				if(!StringUtil.isEmpty(user.getNombreCompleto())){
					impresionDto.setNombreDireccion(user.getNombreCompleto());
					String fecha = Utilerias.cadenaDeFecha(autorizacion.getFechaAutorizacion() ,"dd/MM/yyyy");
					impresionDto.setFechaDireccion(fecha);
				}
			}
		}catch (Exception e){
			LOG.error(e);
		}
		EstimacionCoberturaReporteCabina estimacion=null;
		if (null!= orden.getIdTercero() ){
			estimacion= entidadService.findById(EstimacionCoberturaReporteCabina.class,orden.getIdTercero() );
			if(null!=estimacion){
				if( !StringUtil.isEmpty(estimacion.getNombreAfectado()) ){
					impresionDto.setAseguradoOcupanteTercero( estimacion.getNombreAfectado());
				}
				impresionDto.setFolioPresupuesto(estimacion.getFolio());
			}
		}else {
			if (null!= orden.getReporteCabina() && null!= orden.getReporteCabina().getSeccionReporteCabina() &&
					null!= orden.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina()){
				if (!StringUtil.isEmpty( orden.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getNombreAsegurado())){
					String asegurado =orden.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getNombreAsegurado();
					impresionDto.setAseguradoOcupanteTercero( asegurado);
				}
				/*OBTENER FOLIO */
				if(null!=orden.getCoberturaReporteCabina()){
					List<EstimacionCoberturaReporteCabina> lista =this.entidadService.findByProperty(EstimacionCoberturaReporteCabina.class, "coberturaReporteCabina.id",orden.getCoberturaReporteCabina().getId() );
					if(!lista.isEmpty()){
						EstimacionCoberturaReporteCabina estima= lista.get(0);
						if(!StringUtil.isEmpty(estima.getFolio()) && estima.getTipoEstimacion().equalsIgnoreCase(EstimacionCoberturaReporteCabina.TipoEstimacion.DIRECTA.toString()) ){
							impresionDto.setFolioPresupuesto(estima.getFolio());
						}else
							impresionDto.setFolioPresupuesto("N/A");
					}else{
						impresionDto.setFolioPresupuesto("N/A");
					}
				}else{
					impresionDto.setFolioPresupuesto("N/A");
				}
			}
		}
		//Oficina
		if(null!= orden.getReporteCabina() && null!= orden.getReporteCabina().getOficina() && null!=orden.getReporteCabina().getOficina().getNombreOficina()){
			if(!StringUtil.isEmpty(orden.getReporteCabina().getOficina().getNombreOficina()))
				impresionDto.setOficina(orden.getReporteCabina().getOficina().getNombreOficina().toUpperCase());
		}
		if (null!= orden.getIdBeneficiario()){			
			PrestadorServicio prestador= entidadService.findById(PrestadorServicio.class, new Integer(orden.getIdBeneficiario().toString()) );
			impresionDto.setBeneficiario(prestador.getNombrePersona());
			impresionDto.setNumProveedor(prestador.getId().toString());
		}else{
			impresionDto.setBeneficiario(orden.getNomBeneficiario());
			impresionDto.setNumProveedor("N/A");
		}
		if (null != orden.getReporteCabina().getSiniestroCabina()){
			impresionDto.setSiniestro(orden.getReporteCabina().getSiniestroCabina().getNumeroSiniestro());
		}
		//dto.setCobertura(orden.getid)
		if(null!= orden.getCoberturaReporteCabina() && null!=orden.getCoberturaReporteCabina().getCoberturaDTO()){
			String desc =orden.getCoberturaReporteCabina().getCoberturaDTO().getDescripcion();
			impresionDto.setCobertura(desc);
			if (null!=orden.getCveSubTipoCalculoCobertura() && !orden.getCveSubTipoCalculoCobertura().equalsIgnoreCase("0")){
				String nombreCobertura = estimacionCoberturaSiniestroService.obtenerNombreCobertura(orden.getCoberturaReporteCabina().getCoberturaDTO(), 
						orden.getCoberturaReporteCabina().getClaveTipoCalculo(), 
						orden.getCveSubTipoCalculoCobertura(), 
						null, orden.getCoberturaReporteCabina().getCoberturaDTO().getTipoConfiguracion());
					impresionDto.setCobertura(nombreCobertura);
				
			}
		}
		//Termino ajute  y siniestro
		if (!this.entidadService.findByProperty(AutoIncisoReporteCabina.class, "incisoReporteCabina.seccionReporteCabina.reporteCabina.id", orden.getReporteCabina().getId()).isEmpty() )
		{
			AutoIncisoReporteCabina autoInciso=this.entidadService.findByProperty(AutoIncisoReporteCabina.class, "incisoReporteCabina.seccionReporteCabina.reporteCabina.id", orden.getReporteCabina().getId()).get(0);
			if (! StringUtil.isEmpty(autoInciso.getTerminoAjuste())){
				CatValorFijo catalogo=	catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO , autoInciso.getTerminoAjuste());
				if(null!=catalogo)
					impresionDto.setTerminoAjuste(catalogo.getDescripcion().toUpperCase());
			}
			if (! StringUtil.isEmpty(autoInciso.getTerminoSiniestro())){
				CatValorFijo catalogo=	catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO , autoInciso.getTerminoSiniestro());
				if(null!=catalogo)
					impresionDto.setTerminaSiniestro(catalogo.getDescripcion().toUpperCase());
			}
			
			
			if ( !StringUtil.isEmpty(autoInciso.getTipoResponsabilidad())){
				CatValorFijo catalogo=	catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_RESPONSABILIDAD ,autoInciso.getTipoResponsabilidad());
				if(null!=catalogo)
					impresionDto.setResponsabilidad(catalogo.getDescripcion().toUpperCase());
			}
			
			if(! StringUtil.isEmpty(autoInciso.getCausaSiniestro())){
				CatValorFijo catalogo=	catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.CAUSA_SINIESTRO ,autoInciso.getCausaSiniestro());
				if(null!=catalogo)
					impresionDto.setTipoSiniestro(catalogo.getDescripcion().toUpperCase());
			}
		}
		
		/*SI es GMO **/
	
		if (null!=estimacion && EnumUtil.equalsValue(estimacion.getTipoEstimacion(), TipoEstimacion.GASTOS_MEDICOS)) {
			TerceroGastosMedicos terGastosMedicos= ((TerceroGastosMedicos) estimacion);
			impresionDto.setReporte(ImpresionOrdenCompraDTO.JRXML_GMO_RCP);
			if(null!=terGastosMedicos){				
				if(null!=terGastosMedicos.getHospital()){
					if(!StringUtil.isEmpty(terGastosMedicos.getHospital().getNombrePersona())){
						impresionDto.setHospital(terGastosMedicos.getHospital().getNombrePersona().toUpperCase());
					}
					
				}else{
					if(!StringUtil.isEmpty(terGastosMedicos.getOtroHospital())){
						impresionDto.setHospital(terGastosMedicos.getOtroHospital().toUpperCase());
					}
					
				}
				if(null!= terGastosMedicos.getMedico()){
					if(!StringUtil.isEmpty(terGastosMedicos.getMedico().getNombrePersona())){
						impresionDto.setMedico(terGastosMedicos.getMedico().getNombrePersona().toUpperCase());
					}
					
				}
				
				
				if ( !StringUtil.isEmpty(  terGastosMedicos.getTipoAtencion())){
					CatValorFijo catalogo=	catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPOS_DE_ATENCION ,terGastosMedicos.getTipoAtencion());
					if(null!=catalogo)
						impresionDto.setTipoAtencion(catalogo.getDescripcion().toUpperCase());
				}
				
				String estadoPersona = terGastosMedicos.getEstado();
				if(!StringUtil.isEmpty(estadoPersona)){
					Map<String, String> estadosPersona = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPOS_DE_ESTADO_PERSONA);
					impresionDto.setEstado(estadosPersona.get(estadoPersona));
				}

			}
		}
		
		/*SI es GMC **/
		
		if (null!=estimacion && EnumUtil.equalsValue(estimacion.getTipoEstimacion(), TipoEstimacion.GASTOS_MEDICOS_OCUPANTES)){
			TerceroGastosMedicosConductor terGMC= ((TerceroGastosMedicosConductor) estimacion);
			impresionDto.setReporte(ImpresionOrdenCompraDTO.JRXML_GMO_RCP);
			if(null!=terGMC){				
				if(null!=terGMC.getHospital()){
					if(!StringUtil.isEmpty(terGMC.getHospital().getNombrePersona())){
						impresionDto.setHospital(terGMC.getHospital().getNombrePersona().toUpperCase());
					}
					
				}else{
					if(!StringUtil.isEmpty(terGMC.getOtroHospital())){
						impresionDto.setHospital(terGMC.getOtroHospital().toUpperCase());
					}
				}
				if(null!= terGMC.getMedico()){
					if(!StringUtil.isEmpty(terGMC.getMedico().getNombrePersona())){
						impresionDto.setMedico(terGMC.getMedico().getNombrePersona().toUpperCase());
					}
				}
				
				
				if ( !StringUtil.isEmpty(  terGMC.getTipoAtencion())){
					CatValorFijo catalogo=	catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPOS_DE_ATENCION ,terGMC.getTipoAtencion());
					if(null!=catalogo)
						impresionDto.setTipoAtencion(catalogo.getDescripcion().toUpperCase());
				}
				
				String estadoPersona = terGMC.getEstado();
				if(!StringUtil.isEmpty(estadoPersona)){
					Map<String, String> estadosPersona = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPOS_DE_ESTADO_PERSONA);
					impresionDto.setEstado(estadosPersona.get(estadoPersona));
				}
			}
		}
		
		/*SI es RCP **/
		if (null!=estimacion && EnumUtil.equalsValue(estimacion.getTipoEstimacion(), TipoEstimacion.RC_PERSONA)){
			impresionDto.setReporte(ImpresionOrdenCompraDTO.JRXML_GMO_RCP);
			TerceroRCPersonas terceroRCP= ((TerceroRCPersonas) estimacion);
			if(null!=terceroRCP){				
				if(null!=terceroRCP.getHospital()){
					if(!StringUtil.isEmpty(terceroRCP.getHospital().getNombrePersona())){
						impresionDto.setHospital(terceroRCP.getHospital().getNombrePersona().toUpperCase());
					}
				}else{
					if(!StringUtil.isEmpty(terceroRCP.getOtroHospital())){
						impresionDto.setHospital(terceroRCP.getOtroHospital().toUpperCase());
					}
				}
				if(null!= terceroRCP.getMedico()){
					if(!StringUtil.isEmpty(terceroRCP.getMedico().getNombrePersona())){
						impresionDto.setMedico(terceroRCP.getMedico().getNombrePersona().toUpperCase());
					}
				}
				
				if ( !StringUtil.isEmpty(  terceroRCP.getTipoAtencion())){
					CatValorFijo catalogo=	catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPOS_DE_ATENCION ,terceroRCP.getTipoAtencion());
					if(null!=catalogo)
						impresionDto.setTipoAtencion(catalogo.getDescripcion().toUpperCase());
				}
				
				String estadoPersona = terceroRCP.getEstado();
				if(!StringUtil.isEmpty(estadoPersona)){
					Map<String, String> estadosPersona = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPOS_DE_ESTADO_PERSONA);
					impresionDto.setEstado(estadosPersona.get(estadoPersona));
				}
			}
		}
		
		/*SI es RESP CIVIL VEHICULO **/
		
		if (null!=estimacion && EnumUtil.equalsValue(estimacion.getTipoEstimacion(), TipoEstimacion.RC_VEHICULO)){
			TerceroRCVehiculo terceroRCV= ((TerceroRCVehiculo) estimacion);
			impresionDto.setReporte(ImpresionOrdenCompraDTO.JRXML_AYC_DMA_RCV);
			if(null!=terceroRCV){				
				if(null!=terceroRCV.getModeloVehiculo()){
					impresionDto.setModelo(terceroRCV.getModeloVehiculo().toString());
				}
				if(null!= terceroRCV.getPlacas())
					impresionDto.setPlacas(terceroRCV.getPlacas());
				
				
				if ( !StringUtil.isEmpty(  terceroRCV.getNumeroSerie())){
					
						impresionDto.setSerie(terceroRCV.getNumeroSerie());
				}
				String vehiculo = "";
				if ( !StringUtil.isEmpty(  terceroRCV.getMarca())){
					vehiculo+=terceroRCV.getMarca().toUpperCase()+" ";
				}
				if (!StringUtil.isEmpty(  terceroRCV.getEstiloVehiculo())){
					vehiculo+= terceroRCV.getEstiloVehiculo();
				}
				impresionDto.setVehiculo(vehiculo);
				
				
			}
		}
		/*SI es RESP CIVIL BIENES **/
		
		if (null!=estimacion && EnumUtil.equalsValue(estimacion.getTipoEstimacion(), TipoEstimacion.RC_BIENES)) {
			impresionDto.setReporte(ImpresionOrdenCompraDTO.JRXML_RCB);
			TerceroRCBienes terceroRCB= ((TerceroRCBienes) estimacion);
			if(null!=terceroRCB){				
				if(!StringUtil.isEmpty(terceroRCB.getTipoBien())){
					impresionDto.setTipoBien(terceroRCB.getTipoBien().toUpperCase());
				}
				if ( !StringUtil.isEmpty(  terceroRCB.getDano())){
					
					impresionDto.setDano(terceroRCB.getDano().toUpperCase());
				}
			}
		}
		
		/*daños materiales y AYC */
		if (null!=orden.getCoberturaReporteCabina() && orden.getCoberturaReporteCabina().getCoberturaDTO()  != null
				 &&( EnumUtil.equalsValue(orden.getCoberturaReporteCabina().getCoberturaDTO()
							.getClaveTipoCalculo(), ClaveTipoCalculo.DANIOS_MATERIALES, 
							ClaveTipoCalculo.ADAPTACIONES_CONVERSIONES, ClaveTipoCalculo.ROBO_TOTAL))) {
				impresionDto.setReporte(ImpresionOrdenCompraDTO.JRXML_AYC_DMA_RCV);
				if (!this.entidadService.findByProperty(AutoIncisoReporteCabina.class, "incisoReporteCabina.seccionReporteCabina.reporteCabina.id", orden.getReporteCabina().getId()).isEmpty() )
				{
					AutoIncisoReporteCabina auto=this.entidadService.findByProperty(AutoIncisoReporteCabina.class, "incisoReporteCabina.seccionReporteCabina.reporteCabina.id", orden.getReporteCabina().getId()).get(0);
					if(null!=auto.getPlaca()){
						impresionDto.setPlacas(auto.getPlaca());			
					}
					if(null!= auto.getNumeroMotor()){
						impresionDto.setSerie(auto.getNumeroSerie());
					}
					
					if(null!=auto.getModeloVehiculo()){
						impresionDto.setModelo(auto.getModeloVehiculo().toString());
					}
					
					if (null!=auto.getMarcaId()){
						String vehiculo = "";
						MarcaVehiculoDTO marcaVehiculoDTO=entidadService.findById(MarcaVehiculoDTO.class, auto.getMarcaId());
						if(null!=marcaVehiculoDTO){
							if(!StringUtil.isEmpty(marcaVehiculoDTO.getDescripcionMarcaVehiculo()) ){
								vehiculo+=( marcaVehiculoDTO.getDescripcionMarcaVehiculo().toUpperCase() )+" ";
							}
							if(null!=marcaVehiculoDTO.getTipoVehiculoDTO() &&
									!StringUtil.isEmpty(marcaVehiculoDTO.getTipoVehiculoDTO().getDescripcionTipoVehiculo()) ){
								vehiculo+=marcaVehiculoDTO.getTipoVehiculoDTO().getDescripcionTipoVehiculo();
							}
						}
						impresionDto.setVehiculo(vehiculo);
						
					}
					
				}
			}

		return impresionDto;
	
	}



	@Override
	public List<DetalleOrdenCompra> obtenerDetallesOrdenCompra(
			Long idOrdenCompra, Long idOrdenCompraDetalle) {
		
		Map<String,Object> params = new HashMap<String, Object>();
		if(null==idOrdenCompra && null==idOrdenCompraDetalle){
			return new ArrayList<DetalleOrdenCompra>();
		}
			
		if(null!=idOrdenCompraDetalle)
			params.put("id", idOrdenCompraDetalle);			
		if(null!=idOrdenCompra)
			params.put("ordenCompra.id", idOrdenCompra);
		
		return entidadService.findByProperties(DetalleOrdenCompra.class, params);
	}

	@Override
	public OrdenCompra obtenerOrdenCompra(Long idOrdenCompra) {
		OrdenCompra  ordenCompra =null;
		List<OrdenCompra> lista=entidadService.findByProperty(OrdenCompra.class, "id", idOrdenCompra);
		if(CollectionUtils.isNotEmpty(lista)){
			ordenCompra =lista.get(0);			
		}
		if(ordenCompra!=null){
			if(ordenCompra.getAplicaDeducible()){
				BigDecimal deducible=this.obtenerDeducibleOrdenCompra(ordenCompra);
				ordenCompra.setDeducible(deducible);
			}else{
				ordenCompra.setDeducible(BigDecimal.ZERO);
			}
		}
		return ordenCompra;
	}
	
	
	private BigDecimal obtenerDeducibleOrdenCompra(OrdenCompra ordenCompra){
		BigDecimal deducible = BigDecimal.ZERO;
		IndemnizacionSiniestro indemnizacion = null;		
		if(ordenCompra!=null){			
			if(ordenCompra.getIndemnizacion()!=null){
				indemnizacion= ordenCompra.getIndemnizacion();
			}else if(ordenCompra.getIndemnizacionOrigen()!=null){
				indemnizacion=ordenCompra.getIndemnizacionOrigen();
			}
			if(!deducibleYaRecuperado(ordenCompra)){
				if(ordenCompra.getAplicaDeducible()){
					if(ordenCompra.getTipoPago() != null
							&& ordenCompra.getTipoPago().equals(OrdenCompraService.PAGO_A_BENEFICIARIO)){
						if (indemnizacion!=null){
							if (cartaSiniestroService.esCoberturaAsegurado(indemnizacion.getId())){
								deducible = recuperacionDeducibleService.obtenerDeducibleSiniestroNoRCV(ordenCompra.getReporteCabina().getId());
							}else{
								deducible =recuperacionDeducibleService.obtenerDeduciblePorEstimacion(ordenCompra.getIdTercero());
							}
							
						}else{
							deducible =recuperacionDeducibleService.obtenerDeduciblePorEstimacion(ordenCompra.getIdTercero());
						}
					}else{
						deducible = BigDecimal.ZERO;
					}
				}
			}
		}		
		return deducible;	
	}
	
	@Override
	public boolean deducibleYaRecuperado(OrdenCompra ordenCompra){
		boolean esRecuperado = false;
		
		if(ordenCompra != null //para la consulta de las ordenes de compra ya pagadas 
				&& ordenCompra.getOrdenPago() != null //(consulta si es la orden de compra con la que se aplico el deducible)
				&& ordenCompra.getOrdenPago().getLiquidacion() != null){
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("idReferencia", ordenCompra.getOrdenPago().getLiquidacion().getId());
			params.put("tipoDocumento", TipoDocumentoMovimiento.INGRESO.toString());
			params.put("tipoMovimiento", TipoMovimiento.INGRESO_DEDUCIBLES.toString());
			params.put("causaMovimiento", CausaMovimiento.POR_RECUPERACIONES.toString());
			params.put("estimacionCobertura.id", ordenCompra.getIdTercero());
			List<MovimientoCoberturaSiniestro> resultados = 
				entidadService.findByProperties(MovimientoCoberturaSiniestro.class, params);
			if(resultados != null
					&& !resultados.isEmpty()){
				esRecuperado = false; //la orden de compra es con la que se pago la liquidacion, se debe mostrar el deducible
			}else{
				esRecuperado = estimacionConDeducibleYaRecuperado(ordenCompra);
			}
		}else if(ordenCompra != null){ //para las nuevas ordenes de compra
			esRecuperado = estimacionConDeducibleYaRecuperado(ordenCompra);
		}
		
		return esRecuperado;
	}
	
	/**
	 * Busca en los movimientos del reporte si ya se recupero el deducible del pase de atencion
	 * @param ordenCompra
	 * @return
	 */
	private boolean estimacionConDeducibleYaRecuperado(OrdenCompra ordenCompra){
		boolean esRecuperado = false;
		
		Map<String, Object> params = new HashMap<String, Object>(); //(consulta si la estimacion ya tiene un deducible recuperado previamente)
		params.put("tipoDocumento", TipoDocumentoMovimiento.INGRESO.toString());
		params.put("tipoMovimiento", TipoMovimiento.INGRESO_DEDUCIBLES.toString());
		params.put("causaMovimiento", CausaMovimiento.POR_RECUPERACIONES.toString());
		params.put("estimacionCobertura.id", ordenCompra.getIdTercero());
		List<MovimientoCoberturaSiniestro> resultados = 
			entidadService.findByProperties(MovimientoCoberturaSiniestro.class, params);
		if(resultados != null
				&& !resultados.isEmpty()){
			esRecuperado = true; //Como no encontro resultados con la OC como parametro pero si hay un movimiento de aplicacion de ingreso,
									// no se debe mostrar el deducible
		}
		
		return esRecuperado;
	}
	
	
	@Override
	public List<DetalleOrdenCompraDTO> obtenerListaDetallesOrdenDTO(
			Long idOrdenCompra, Long idOrdenCompraDetalle,boolean calcularPorConcepto) {
		List<DetalleOrdenCompraDTO>  listaDto = new ArrayList<DetalleOrdenCompraDTO>();
		List<DetalleOrdenCompra>  detaOrdenCompras;
		if(calcularPorConcepto){
			detaOrdenCompras=this.obtenerDetallesOrdenCompraPorConcepto(idOrdenCompra, idOrdenCompraDetalle);
		}else{
			detaOrdenCompras=this.obtenerDetallesOrdenCompra(idOrdenCompra,idOrdenCompraDetalle);
		}
		
		for( DetalleOrdenCompra detalle : detaOrdenCompras ){
			try {
				DetalleOrdenCompraDTO dto = new DetalleOrdenCompraDTO();	
				try{
					BeanUtils.copyProperties(dto, detalle);
				}catch(Exception e){
					dto.setConceptoAjuste(detalle.getConceptoAjuste());
					dto.setCostoUnitario(detalle.getCostoUnitario());
					dto.setEstatus(detalle.getEstatus());
					dto.setId(detalle.getId());
					dto.setImporte(detalle.getImporte());
					dto.setImportePagado(detalle.getImportePagado());
					dto.setIva(detalle.getIva());
					dto.setObservaciones(detalle.getObservaciones());
					dto.setOrdenCompra(detalle.getOrdenCompra());
					dto.setPorcIva(detalle.getPorcIva());
					dto.setPorcIsr(detalle.getPorcIsr());
					dto.setPorcIvaRetenido(detalle.getPorcIvaRetenido());
					dto.setIvaRetenido(detalle.getIvaRetenido());
					dto.setIsr(detalle.getIsr());
				}
				dto.setIdOrdenCompra(detalle.getOrdenCompra().getId());
				if(null!= detalle.getOrdenCompra()){
					if (!StringUtil.isEmpty(detalle.getOrdenCompra().getTipo())){
						CatValorFijo valor=catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA, detalle.getOrdenCompra().getTipo());
						if(null!=valor){
							dto.setTipoOrden(valor.getDescripcion());
						}
					}
				}
				if(null!= detalle.getOrdenCompra().getCoberturaReporteCabina()
						&& null!=detalle.getOrdenCompra().getCoberturaReporteCabina().getCoberturaDTO()){
					String desc = detalle.getOrdenCompra().getCoberturaReporteCabina().getCoberturaDTO().getDescripcion();
					dto.setCoberturaDescripcion(desc);
					if (null!=detalle.getOrdenCompra().getCveSubTipoCalculoCobertura() && !detalle.getOrdenCompra().getCveSubTipoCalculoCobertura().equalsIgnoreCase("0")){

						String nombreCobertura = estimacionCoberturaSiniestroService.obtenerNombreCobertura(
								detalle.getOrdenCompra().getCoberturaReporteCabina().getCoberturaDTO(), 
								detalle.getOrdenCompra().getCoberturaReporteCabina().getClaveTipoCalculo(), 
								detalle.getOrdenCompra().getCveSubTipoCalculoCobertura(), 
								null, detalle.getOrdenCompra().getCoberturaReporteCabina().getCoberturaDTO().getTipoConfiguracion());
						dto.setCoberturaDescripcion(nombreCobertura);
					}
				}
				
				if (null!=detalle.getConceptoAjuste()&& null!=detalle.getConceptoAjuste().getNombre()){
					dto.setConceptoPago(detalle.getConceptoAjuste().getNombre());
				}	
				if(null==detalle.getImportePagado()){
					detalle.setImportePagado(zero);
				}
				dto.setPendiente( detalle.getImporte().subtract( detalle.getImportePagado() ) );
				 dto.setEstatus( ordenCompraAutorizacionService.obtenerEstatusDetalleCompra(idOrdenCompra,detalle.getId()));
				
				listaDto.add(dto);
			} catch (Exception e) {
				LOG.error(e);
			} 
		}
		return listaDto;
	}
	@Override
	public BigDecimal obtenerTotales(Long idEstimacionCobRepCab,
		Long idReporteCabina, Long idcoberturaReporteCabina, String tipoOrdenCompra) {
		
		Map<String, Object> parameters = new HashMap<String, Object>();	
		BigDecimal total= new BigDecimal(0.0);
		Long longZero= new Long(0);
		if(null!= idReporteCabina &&idReporteCabina!=longZero ){
			parameters.put("reporteCabina.id", idReporteCabina);
		}
		if(null!= idcoberturaReporteCabina &&idcoberturaReporteCabina!=longZero ){
			parameters.put("coberturaReporteCabina.id", idcoberturaReporteCabina);
		}
		if(null!= idEstimacionCobRepCab &&idEstimacionCobRepCab!=longZero ){
			parameters.put("idTercero", idEstimacionCobRepCab);
		}
		
		if (tipoOrdenCompra != null) {
			parameters.put("tipo", tipoOrdenCompra);
		}

		List<OrdenCompra>  ordenCompras= new ArrayList<OrdenCompra>();
		ordenCompras=	entidadService.findByProperties(OrdenCompra.class, parameters);
		for( OrdenCompra ordenCompra : ordenCompras ){
			if(ordenCompra.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_AUTORIZADA) || ordenCompra.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_TRAMITE)  || ordenCompra.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_ASOCIADA)   ){
				ImportesOrdenCompraDTO totales= this.calcularImportes(ordenCompra.getId(), null, false);
				if(null!=totales && null!=totales.getTotal()){
					total=total.add( totales.getTotal()); 

				}
			}
		}
		return total;
	}
	@Override
	public ImportesOrdenCompraDTO calcularImportes(Long idOrdenCompra,Long idOrdenCompraDetalle,boolean calcularPorConcepto) {
		ImportesOrdenCompraDTO importesDTO = new ImportesOrdenCompraDTO();
		OrdenCompra oc  = this.obtenerOrdenCompra(idOrdenCompra);
		List <DetalleOrdenCompraDTO> lista=this.obtenerListaDetallesOrdenDTO(idOrdenCompra,idOrdenCompraDetalle,calcularPorConcepto);
		BigDecimal subtotal=zero;
		BigDecimal iva=zero;
		BigDecimal ivaRetenido=zero;
		BigDecimal isr=zero;
		BigDecimal totalSinDescuentos=zero;
		BigDecimal total=zero;
		BigDecimal totalPagado=zero;
		BigDecimal totalPendiente=zero;
		BigDecimal deducible=zero;
		BigDecimal descuento=zero;
		if(null!=oc && null != oc.getDeducible())
			deducible=oc.getDeducible();
		if(null!=oc && null != oc.getDescuento())
			descuento= oc.getDescuento();
		for( DetalleOrdenCompraDTO detalle : lista ){			
			//Se inicializan los campos a cero en caso de ser nullo.
			if (null==detalle.getCostoUnitario())	
					detalle.setCostoUnitario(zero);				
			if (null==detalle.getIva())
					detalle.setIva(zero);			
			if (null==detalle.getIvaRetenido())
				detalle.setIvaRetenido(zero);			
			if (null==detalle.getIsr())
				detalle.setIsr(zero);
			if (null==detalle.getImporte())
					detalle.setImporte(zero);
			if (null==detalle.getImportePagado())
					detalle.setImportePagado(zero);
			if (null==detalle.getPendiente())
					detalle.setPendiente(zero);
				subtotal=subtotal.add(detalle.getCostoUnitario());
				iva=iva.add(detalle.getIva());
				ivaRetenido=ivaRetenido.add(detalle.getIvaRetenido());
				isr=isr.add(detalle.getIsr());				
				totalSinDescuentos=totalSinDescuentos.add (detalle.getImporte());
				totalPagado=totalPagado.add(detalle.getImportePagado());
				if(oc.getEstatus()!=null && oc.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_PAGADA)){
					totalPagado=totalPagado.subtract(deducible).subtract(descuento);
				}
				totalPendiente=totalPendiente.add(detalle.getPendiente());				
			
		}
		total=totalSinDescuentos.subtract(deducible).subtract(descuento);
		totalPendiente=total.subtract(totalPagado);			
		importesDTO = new ImportesOrdenCompraDTO();
		importesDTO.setSubtotal(subtotal);
		importesDTO.setIva(iva);
		importesDTO.setIsr(isr);
		importesDTO.setIvaRetenido(ivaRetenido);
		importesDTO.setTotalSinDescuentos(totalSinDescuentos);		
		importesDTO.setDeducible(deducible);
		importesDTO.setDescuento(descuento);		
		importesDTO.setTotal(total);		
		importesDTO.setTotalPagado(totalPagado);
		importesDTO.setTotalPendiente(totalPendiente);		
        return importesDTO;
	}
	
	/**
	 * Actualiza orden de compra sipac en caso de que exista
	 * @param valorEstimado
	 * @param estimacion
	 * @return
	 */
	public Long actualizarSipac(BigDecimal valorEstimado,
			EstimacionCoberturaReporteCabina estimacion){		
		
		CoberturaReporteCabina cobertura = estimacion.getCoberturaReporteCabina();
		ReporteCabina reporte = cobertura.getIncisoReporteCabina()
				.getSeccionReporteCabina().getReporteCabina();
		
		
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("reporteCabina.id", reporte.getId());
		params.put("coberturaReporteCabina.id", cobertura.getId());
		params.put("idTercero", estimacion.getId());		
		params.put("origen", OrdenCompraService.ORIGENSIPAC_AUTOMATICO);
		params.put("tipoProveedor", OrdenCompraService.TIPO_PROVEEDOR_CIA);
		params.put("tipoPago", OrdenCompraService.PAGO_A_PROVEEDOR);
		params.put("tipo", OrdenCompraService.TIPO_AFECTACION_RESERVA);
		params.put("sipac", true);	
		
		List<OrdenCompra> ordenes = entidadService.findByColumnsAndProperties(
				OrdenCompra.class, "id", params, 
				new HashMap<String,Object>(), queryWhereValid(false), null);		
		
		Long idOrdenCompra = ordenes.isEmpty()?null:ordenes.get(0).getId();
		
		if(idOrdenCompra!=null){
			this.actualizarSipac(valorEstimado, idOrdenCompra);
		}
		return idOrdenCompra;
	}
	
	/**
	 * Crea la orden de compra SIPAC
	 * @param importe
	 * @param estimacion
	 * @return OrdenCompra
	 */
	public void crearSipac(BigDecimal importe, 
			EstimacionCoberturaReporteCabina estimacion){
		
		CoberturaReporteCabina cobertura = estimacion.getCoberturaReporteCabina();
		ReporteCabina reporte = cobertura.getIncisoReporteCabina()
				.getSeccionReporteCabina().getReporteCabina();		
		
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("cveTipoCalculoCobertura",cobertura.getClaveTipoCalculo());
		params.put("tipoEstimacion",estimacion.getTipoEstimacion());
		
		List<ConfiguracionCalculoCoberturaSiniestro> configCalculoCoberSin = this.entidadService.findByProperties(
				ConfiguracionCalculoCoberturaSiniestro.class, params);
		
		if(!configCalculoCoberSin.isEmpty()){

			ConfiguracionCalculoCoberturaSiniestro conf = configCalculoCoberSin.get(0);
			
			Map<Long, String> mapConceptos = this.listadoService.listarConceptosPorCobertura(
					cobertura.getId(), conf.getCveSubTipoCalculoCobertura(), 
					ConceptoAjusteService.AFECTACION_RESERVA, OrdenCompraService.TIPO_SIPAC);
			
			if(!mapConceptos.isEmpty() && estimacion.getCompaniaSegurosTercero()!=null){
				
				OrdenCompra ordenCompra = new OrdenCompra();
				ordenCompra.setCoberturaReporteCabina(cobertura);
				ordenCompra.setReporteCabina(reporte);		
				
				ordenCompra.setCveSubTipoCalculoCobertura(conf.getCveSubTipoCalculoCobertura());
				
				ordenCompra.setIdCoberturaCompuesta(ordenCompra.getCoberturaReporteCabina().getId() + 
						OrdenCompraService.SEPARADOR + 
						ordenCompra.getCveSubTipoCalculoCobertura());
				ordenCompra.setTipo(OrdenCompraService.TIPO_AFECTACION_RESERVA);
				ordenCompra.setTipoPago(OrdenCompraService.PAGO_A_PROVEEDOR);
				ordenCompra.setTipoProveedor(OrdenCompraService.TIPO_PROVEEDOR_CIA);
				
				Integer idCia = estimacion.getCompaniaSegurosTercero().getId();
				ordenCompra.setIdBeneficiario(idCia);
				ordenCompra.setRfc(utileriasService.obtenerRFCbeneficiario(idCia));
				ordenCompra.setCurp(utileriasService.obtenerCURPbeneficiario(idCia));
				
				ordenCompra.setIdTercero(estimacion.getId());
				ordenCompra.setOrigen(OrdenCompraService.ORIGENSIPAC_AUTOMATICO);
				ordenCompra.setEstatus(OrdenCompraService.ESTATUS_AUTORIZADA);
				ordenCompra.setSipac(true);	
				
				List<DetalleOrdenCompra> detalleOrdenCompras = new ArrayList<DetalleOrdenCompra>();
				
				for(Entry<Long, String> entry: mapConceptos.entrySet()) {
					ConceptoAjuste concepto = this.entidadService.findById(ConceptoAjuste.class, entry.getKey());
					if(concepto!=null){
						
						detalleOrdenCompras.add(this.prepareDetalleOrdenCompra(ordenCompra, importe, 
								concepto, OrdenCompraService.ORIGENSIPAC_AUTOMATICO));	
						
						break;
					}					
				}
				
				ordenCompra.setDetalleOrdenCompras(detalleOrdenCompras);
				
				this.guardarOrdenCompra(ordenCompra);
						
			}			
		}
	}
	
	
	/**
	 * Actualiza la orden de compra generada automaticamente para el pase SIPAC
	 * @param importe
	 * @param idOrdenCompra
	 * @param estimacion
	 */
	private void actualizarSipac(BigDecimal importe, 
			Long idOrdenCompra) {

		OrdenCompra ordenCompra  = this.entidadService.findById(OrdenCompra.class, idOrdenCompra);
		
		if (!OrdenCompraService.ESTATUS_PAGADA.equals(ordenCompra.getEstatus())
				&& !OrdenCompraService.ESTATUS_ASOCIADA.equals(ordenCompra.getEstatus())
				&& !ordenCompra.getDetalleOrdenCompras().isEmpty()) {
			this.modifyDetalleOrdenCompra(importe, 
					ordenCompra.getDetalleOrdenCompras().get(0));
			entidadService.save(ordenCompra);
		}
	}
	
	/**
	 * Condicion para filtrar ordenes de compra
	 * @param incluirPagado
	 * @return
	 */
	private StringBuilder queryWhereValid(boolean incluirPagado){
		StringBuilder queryWhere = new StringBuilder();
		queryWhere.append(" and model.estatus not in ( '"+OrdenCompraService.ESTATUS_CANCELADA+"' ");
		queryWhere.append(", '"+ OrdenCompraService.ESTATUS_INDEMNIZACION  + "'  ");
		queryWhere.append(", '"+ OrdenCompraService.ESTATUS_INDEMNIZACION_FINALIZADA  + "'  ");
		queryWhere.append(", '"+ OrdenCompraService.ESTATUS_INDEMNIZACION_CANCELADA  + "'  ");
		if(incluirPagado){
			queryWhere.append(", '"+ OrdenCompraService.ESTATUS_PAGADA  + "'  ");
		}
		queryWhere.append(", '"+ OrdenCompraService.ESTATUS_RECHAZADA  + "' ) ");
		return queryWhere;
	}
	
	@Override
	public OrdenCompra generarOrdenCompraIndemnizacion(
			Long idEstimacionCoberturaReporteCabina, String tipoIndemnizacion, String beneficiario)
			throws Exception {
		EstimacionCoberturaReporteCabina estimacion  = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, idEstimacionCoberturaReporteCabina);
		if(null!=estimacion ){
			OrdenCompra ordenCompra = new OrdenCompra ();
			CoberturaReporteCabina cobertura = estimacion.getCoberturaReporteCabina();
			if(null!=cobertura ){
				ReporteCabina reporte = null;
				try {
					reporte = cobertura.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina();
				}catch (Exception e){ 	return null; }
				if(null!= reporte){
					Map<String,Object> params = new HashMap<String, Object>();
					params.put("cveTipoCalculoCobertura",cobertura.getClaveTipoCalculo());
					params.put("tipoEstimacion",estimacion.getTipoEstimacion());
					List<ConfiguracionCalculoCoberturaSiniestro> lista = this.entidadService.findByProperties(ConfiguracionCalculoCoberturaSiniestro.class, params);
					if(lista.isEmpty())
						return null;
					ConfiguracionCalculoCoberturaSiniestro conf = lista.get(0);					
					ordenCompra.setCoberturaReporteCabina(cobertura);
					ordenCompra.setCurp(null);
					ordenCompra.setCveSubTipoCalculoCobertura(conf.getCveSubTipoCalculoCobertura());
					ordenCompra.setEstatus(OrdenCompraService.ESTATUS_INDEMNIZACION);
					ordenCompra.setFactura(null);
					ordenCompra.setFacturasSiniestroList(null);
					ordenCompra.setIdBeneficiario(null);
					if(StringUtil.isEmpty( ordenCompra.getCveSubTipoCalculoCobertura())){
						ordenCompra.setIdCoberturaCompuesta(ordenCompra.getCoberturaReporteCabina().getId()+OrdenCompraService.SEPARADOR+OrdenCompraService.CVENULA);
					}else
						ordenCompra.setIdCoberturaCompuesta(ordenCompra.getCoberturaReporteCabina().getId()+OrdenCompraService.SEPARADOR+ordenCompra.getCveSubTipoCalculoCobertura());
					ordenCompra.setIdTercero(estimacion.getId());
					if(StringUtil.isEmpty(beneficiario) && !StringUtil.isEmpty(estimacion.getNombreAfectado())){
						ordenCompra.setNomBeneficiario(estimacion.getNombreAfectado());
					}else{
						ordenCompra.setNomBeneficiario(beneficiario);
					}				
					ordenCompra.setNumeroValuacion(null);
					ordenCompra.setOrigen(OrdenCompraService.ORIGEN_INDEMNIZACION_AUTOMATICA);
					ordenCompra.setReporteCabina(reporte);
					ordenCompra.setRfc(null);
					ordenCompra.setTipo(OrdenCompraService.TIPO_AFECTACION_RESERVA);
					ordenCompra.setTipoPago(OrdenCompraService.PAGO_A_BENEFICIARIO);
					ordenCompra.setTipoProveedor(null);
					
					/**GENERAR DETALLE*/
					/*Obtener concepto  ajuste*/
					ConceptoAjuste conceptoAjuste=null;
					Map<Long, String> mapConceptos = this.listadoService.listarConceptosPorCobertura(
							cobertura.getId(), conf.getCveSubTipoCalculoCobertura(), 
							ConceptoAjusteService.AFECTACION_RESERVA , tipoIndemnizacion);
			    	if(mapConceptos.isEmpty()){
			    		throw new Exception("No es posible crear la indemnizacion, no existen conceptos para el tipo indemnizacion seleccionada"); 
			    	} else{
			    		 Long idConceptoAjuste =null;
			    		 for(Entry<Long, String> entry: mapConceptos.entrySet()) {
			    			 idConceptoAjuste=entry.getKey();
			    			 if(null!=idConceptoAjuste)
			    				 break;
			    		   }
			    		 if(null !=idConceptoAjuste){
			    			 conceptoAjuste=this.entidadService.findById(ConceptoAjuste.class, idConceptoAjuste);
			    			 if(null==conceptoAjuste){
						    		throw new Exception("No es posible crear la indemnizacion, no existen conceptos para el tipo indemnizacion seleccionada"); 
						    	}
			    		 }
			    		
			    	}
					
					List<DetalleOrdenCompra> detalleOrdenCompras = new ArrayList<DetalleOrdenCompra>();
					

					BigDecimal reserva= movimientoSiniestroService.obtenerReservaAfectacion(
							idEstimacionCoberturaReporteCabina, Boolean.TRUE);
					
					detalleOrdenCompras.add(this.prepareDetalleOrdenCompra(ordenCompra, reserva, conceptoAjuste, 
							OrdenCompraService.ESTATUS_INDEMNIZACION));
					/*Asociar Detalle*/
					ordenCompra.setDetalleOrdenCompras(detalleOrdenCompras);
					this.guardarOrdenCompra(ordenCompra);
					return ordenCompra;
				}else 
					return null;
			}else
				return null;
		}else 
			return null;
	}
	
	/**
	 * Construye un detalle de orden de compra en base a una reserva
	 * @param ordenCompra
	 * @param importe
	 * @param conceptoAjuste
	 * @param observaciones
	 * @return
	 */
	private DetalleOrdenCompra prepareDetalleOrdenCompra(
			OrdenCompra ordenCompra, BigDecimal importe,
			ConceptoAjuste conceptoAjuste,
			String observaciones){
		
		DetalleOrdenCompra detalle = new DetalleOrdenCompra();		
		detalle.setConceptoAjuste(conceptoAjuste);		
		detalle.setEstatus(OrdenCompraService.ESTATUS_AUTORIZADA);		
		detalle.setOrdenCompra(ordenCompra);		
		detalle.setCodigoUsuarioCreacion(
				String.valueOf(usuarioService.getUsuarioActual().getNombreUsuario()));
		detalle.setObservaciones(
				"Orden de compra Generada Automaticamente por " + observaciones);
		this.modifyDetalleOrdenCompra(importe, detalle);		
		return detalle;		
	}
	
	/**
	 * Actualiza el detalle de la orden de compra en base al monto reserva
	 * @param importe
	 * @param detalle
	 * @return
	 */
	private DetalleOrdenCompra modifyDetalleOrdenCompra(BigDecimal importe, 
			DetalleOrdenCompra detalle){
		detalle.setCostoUnitario(importe);
		detalle.setImporte(importe);
		detalle.setImportePagado(zero);
		detalle.setIva(zero);		
		detalle.setPorcIva(zero);
		detalle.setIvaRetenido(zero);
		detalle.setIsr(zero);
		detalle.setPorcIsr(zero);
		detalle.setPorcIvaRetenido(zero);
		detalle.setCodigoUsuarioModificacion(
				String.valueOf(usuarioService.getUsuarioActual().getNombreUsuario()));	
		return detalle;
	}	
	
	public OrdenCompraDao getOrdenCompraDao() {
		return ordenCompraDao;
	}
	public void setOrdenCompraDao(OrdenCompraDao ordenCompraDao) {
		this.ordenCompraDao = ordenCompraDao;
	}
	
	@Override
	public void reiniciaIndemnizacion(Long idOrdenCompra) {
		  OrdenCompra ordenCompra=this.obtenerOrdenCompra(idOrdenCompra);
		  if(null!=ordenCompra && null!=ordenCompra.getIdOrdenCompraOrigen()   ){
			  OrdenCompra ordenCompraOrigen=this.obtenerOrdenCompra(ordenCompra.getIdOrdenCompraOrigen());
			  ordenCompraOrigen.setEstatus(OrdenCompraService.ESTATUS_INDEMNIZACION);
			  this.guardarOrdenCompra(ordenCompraOrigen);
			  perdidaTotalService.cambiarEtapaIndemnizacionAAutorizacionIndemnizacion(ordenCompraOrigen.getId());
		  }
	
	}
	
	@Override
	public boolean ordenCompraAplicaBeneficiario(Long idcoberturaReporteCabina,String cveSubTipoCalculo) {
		CoberturaReporteCabina coberturaReporteCabina = entidadService.findById(CoberturaReporteCabina.class, idcoberturaReporteCabina);
		boolean valido =false;
		if (EnumUtil.equalsValue(ClaveTipoCalculo.RESPONSABILIDAD_CIVIL, 
				coberturaReporteCabina.getCoberturaDTO().getClaveTipoCalculo())				  
				&&  EnumUtil.equalsValue(cveSubTipoCalculo, ClaveSubCalculo.RC_VEHICULO)) {
			return true;
		}
		
		if (coberturaReporteCabina.getCoberturaDTO() != null 
			&& EnumUtil.equalsValue(coberturaReporteCabina.getCoberturaDTO().getClaveTipoCalculo(), ClaveTipoCalculo.DANIOS_MATERIALES,
			ClaveTipoCalculo.ROBO_TOTAL, ClaveTipoCalculo.ADAPTACIONES_CONVERSIONES, ClaveTipoCalculo.EQUIPO_ESPECIAL)
		) {
			return true;
		}
		return valido;
	}
	
	
	@Override
	public boolean validaImportesOrdenCompraContraConceptos(Long idOrdenCompra,
			Long idOrdenCompraDetalle, BigDecimal variacion) throws Exception {
		
		if (null==variacion){
			String value = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , PARAMETRO);
			if (null!=value && StringUtils.isNumeric(value)){
				variacion=new BigDecimal (value);
			}
		}
		
		
		
		
		ImportesOrdenCompraDTO importesDTO=calcularImportes(idOrdenCompra,null,false);
		ImportesOrdenCompraDTO importesPorConceptoDTO = calcularImportes(idOrdenCompra,null,true);
		if(null== importesDTO || null==importesPorConceptoDTO)
			return true;
		
		
		BigDecimal diferenciaIva = importesPorConceptoDTO.getIva().subtract( importesDTO.getIva());
		if ( diferenciaIva.signum()==-1 ){
			diferenciaIva=diferenciaIva.multiply(new BigDecimal (-1));
		}
		
		
		BigDecimal diferenciaIvaRet = importesPorConceptoDTO.getIvaRetenido().subtract( importesDTO.getIvaRetenido());
		if ( diferenciaIvaRet.signum()==-1 ){
			diferenciaIvaRet=diferenciaIvaRet.multiply(new BigDecimal (-1));
		}
		
		
		BigDecimal diferenciaIsr = importesPorConceptoDTO.getIsr().subtract( importesDTO.getIsr());
		if ( diferenciaIsr.signum()==-1 ){
			diferenciaIsr=diferenciaIsr.multiply(new BigDecimal (-1));
		}
		
		
		if (diferenciaIva.compareTo(variacion) ==1){
			throw new Exception(String.format("La variacion del I.V.A no puede ser Mayor o Menor a $"+variacion+" (MXN)."));
		}
		
		if (diferenciaIvaRet.compareTo(variacion) ==1){
			throw new Exception(String.format("La variacion del I.V.A Retenido no puede ser Mayor o Menor a $"+variacion+" (MXN)."));
		}
		if (diferenciaIsr.compareTo(variacion) ==1){
			throw new Exception(String.format("La variacion del ISR no puede ser Mayor o Menor a $"+variacion+" (MXN)."));
		}
		
		return true;
		
	}
	
	
	private List<DetalleOrdenCompra> llenaDetallesOrdenCompraPorConcepto(List<DetalleOrdenCompra> listDetalle){
		List<DetalleOrdenCompra> listDetalleOrdenCompra=new ArrayList<DetalleOrdenCompra>();
		for (DetalleOrdenCompra detalleCompra :listDetalle){
			DetalleOrdenCompra desglose;
			try {
				desglose = (DetalleOrdenCompra) BeanUtils.cloneBean(detalleCompra);
			} catch (Exception e) {
				desglose= new DetalleOrdenCompra();
			}
			
			ConceptoAjuste conceptoAjuste=detalleCompra.getConceptoAjuste();
			//Se inicializa detalle 
			desglose.setPorcIsr(zero);
			desglose.setPorcIva(zero);
			desglose.setPorcIvaRetenido(zero);
			desglose.setImporte(zero);
			desglose.setIsr(zero);
			desglose.setIva(zero);
			desglose.setIvaRetenido(zero);
			if(null!=conceptoAjuste){
				if(null!=conceptoAjuste.getPorcIsr()){
					desglose.setPorcIsr(conceptoAjuste.getPorcIsr());
				}
				if(null!=conceptoAjuste.getPorcIva()){
					desglose.setPorcIva(conceptoAjuste.getPorcIva());
				}
				if(null!=conceptoAjuste.getPorcIvaRetenido()){
					desglose.setPorcIvaRetenido(conceptoAjuste.getPorcIvaRetenido());
				}
			}
						
			//calculo iva 
			BigDecimal iva =zero;
			if(desglose.getPorcIva().compareTo(zero)==1){
				iva = (desglose.getPorcIva().divide(cien)).multiply(desglose.getCostoUnitario());
			}
			desglose.setIva(iva);
			
			BigDecimal retenido =zero;
			if(desglose.getPorcIvaRetenido().compareTo(zero)==1){
				 retenido =(desglose.getPorcIvaRetenido().divide(cien)).multiply(desglose.getCostoUnitario());
			}
			desglose.setIvaRetenido(retenido);
			BigDecimal isr =zero;
			if(desglose.getPorcIsr().compareTo(zero)==1){
				isr =(desglose.getPorcIsr().divide(cien)).multiply(desglose.getCostoUnitario());
			}
			desglose.setIsr(isr);
			//TOTAL
			BigDecimal total= desglose.getCostoUnitario().add(desglose.getIva()).subtract(desglose.getIvaRetenido()).subtract(desglose.getIsr());
			desglose.setImporte(total);
			listDetalleOrdenCompra.add(desglose);

			}
		
		
		return listDetalleOrdenCompra;
	}
	
	
	
	@Override
	public List<DetalleOrdenCompra> obtenerDetallesOrdenCompraPorConcepto(
			Long idOrdenCompra, Long idOrdenCompraDetalle) {


		Map<String, Object> param = new HashMap<String, Object>();
		if(null!= idOrdenCompra){
			param.put("ordenCompra.id", idOrdenCompra);
		}
		if(null!= idOrdenCompraDetalle){
			param.put("id", idOrdenCompraDetalle);
		}
		List<DetalleOrdenCompra> listDetalle= this.entidadService.findByProperties(DetalleOrdenCompra.class, param);
		
		 return llenaDetallesOrdenCompraPorConcepto(listDetalle);
	
	}
	
	@Override
	public boolean validaImportesDetalleOrdenCompraContraConcepto(DetalleOrdenCompra detalleOrdenCompra, BigDecimal variacion) throws Exception {
		
		BigDecimal variacionValida=variacion;


		//BigDecimal costoUnitario=zero;
		DetalleOrdenCompra detalleOrdenCompraConcepto =  new DetalleOrdenCompra();
		List<DetalleOrdenCompra> listDetalleOrdenCompra=new ArrayList<DetalleOrdenCompra>();
		List<DetalleOrdenCompra> listDetalleOrdenCompraConcepto=new ArrayList<DetalleOrdenCompra>();
		listDetalleOrdenCompra.add(detalleOrdenCompra);
		listDetalleOrdenCompraConcepto=this.llenaDetallesOrdenCompraPorConcepto(listDetalleOrdenCompra);
		detalleOrdenCompraConcepto = listDetalleOrdenCompraConcepto.get(0);

		if (null==variacion){
			String value = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , PARAMETRO);
			if (null!=value && StringUtils.isNumeric(value)){
				variacionValida=new BigDecimal (value);
			}
		}
		BigDecimal diferenciaIva = detalleOrdenCompra.getIva().subtract( detalleOrdenCompraConcepto.getIva());
		if ( diferenciaIva.signum()==-1 ){
			diferenciaIva=diferenciaIva.multiply(new BigDecimal (-1));
		}
		
		
		BigDecimal diferenciaIvaRet = detalleOrdenCompra.getIvaRetenido().subtract( detalleOrdenCompraConcepto.getIvaRetenido());
		if ( diferenciaIvaRet.signum()==-1 ){
			diferenciaIvaRet=diferenciaIvaRet.multiply(new BigDecimal (-1));
		}
		
		
		BigDecimal diferenciaIsr = detalleOrdenCompra.getIsr().subtract( detalleOrdenCompraConcepto.getIsr());
		if ( diferenciaIsr.signum()==-1 ){
			diferenciaIsr=diferenciaIsr.multiply(new BigDecimal (-1));
		}
		
		
		if (diferenciaIva.compareTo(variacionValida) ==1){
			throw new Exception(String.format("La variacion del I.V.A no puede ser Mayor o Menor a $"+variacionValida+" (MXN)."));
		}
		
		if (diferenciaIvaRet.compareTo(variacionValida) ==1){
			throw new Exception(String.format("La variacion del I.V.A Retenido no puede ser Mayor o Menor a $"+variacionValida+" (MXN)."));
		}
		if (diferenciaIsr.compareTo(variacionValida) ==1){
			throw new Exception(String.format("La variacion del ISR no puede ser Mayor o Menor a $"+variacionValida+" (MXN)."));
		}
		
		return true;
		
	}
	
	
	@Override
	public List<OrdenCompraProveedorDTO> obtenerOrdenesCompraProveedor(Integer idProveedor){
		List<OrdenCompraProveedorDTO> dtoList = new ArrayList<OrdenCompraProveedorDTO>(); 
		List<OrdenCompra> ordenCompraList =  this.ordenCompraDao.obtenerOrdenesCompraParaFacturarPorProveedor(idProveedor);
		for(OrdenCompra orden : ordenCompraList){
			ImportesOrdenCompraDTO importeDTO = this.calcularImportes(orden.getId(),null,false);
			OrdenCompraProveedorDTO ordenCompraDTO = new OrdenCompraProveedorDTO();
			ordenCompraDTO.setFechaRegistro(orden.getFechaCreacion());
			ordenCompraDTO.setIdOrdenCompra(orden.getId());
			SiniestroCabina siniestroCabina = orden.getReporteCabina().getSiniestroCabina();
			if(siniestroCabina!=null){
				ordenCompraDTO.setIdSiniestro(siniestroCabina.getId());
				ordenCompraDTO.setNumeroSiniestro(siniestroCabina.getNumeroSiniestro());
			}
			ordenCompraDTO.setIsr(importeDTO.getIsr());
			ordenCompraDTO.setIva(importeDTO.getIva());
			ordenCompraDTO.setIvaRetenido(importeDTO.getIvaRetenido());
			ordenCompraDTO.setSubtotal(importeDTO.getSubtotal());
			ordenCompraDTO.setTotal(importeDTO.getTotal());
			if(orden.getIdTercero() != null)
			{
				EstimacionCoberturaReporteCabina estimacion = 
					entidadService.findById(EstimacionCoberturaReporteCabina.class,orden.getIdTercero());
				ordenCompraDTO.setAfectado(estimacion.getFolio() + " " + estimacion.getNombreAfectado());
			}			

			dtoList.add(ordenCompraDTO);
		}
		return dtoList;
	}
	
	
	
	@Override
	public List<OrdenCompraProveedorDTO> obtenerOrdenesCompraParaAgruparParaCompania(Integer idProveedor,Long oficinaSeleccionada, String tipoAgrupador){
		List<OrdenCompraProveedorDTO> dtoList = new ArrayList<OrdenCompraProveedorDTO>(); 
		List<OrdenCompra> ordenCompraList =  this.ordenCompraDao.obtenerOrdenesCompraParaAgruparParaCompania(idProveedor,oficinaSeleccionada, tipoAgrupador);
		for(OrdenCompra orden : ordenCompraList){
			dtoList.add(convierteOrdenCompraAOrdenCompraProveedorDTO(orden));
		}
		return dtoList;
	}
	
	
	@Override
	public OrdenCompraProveedorDTO convierteOrdenCompraAOrdenCompraProveedorDTO(OrdenCompra orden){
		OrdenCompraProveedorDTO ordenCompraDTO = new OrdenCompraProveedorDTO();
		ImportesOrdenCompraDTO importeDTO = this.calcularImportes(orden.getId(),null,false);
		EstimacionCoberturaReporteCabina estimacion = null;
		
		ordenCompraDTO.setFechaRegistro(orden.getFechaCreacion());
		ordenCompraDTO.setIdOrdenCompra(orden.getId());
		SiniestroCabina siniestroCabina = orden.getReporteCabina().getSiniestroCabina();
		if(siniestroCabina!=null){
			ordenCompraDTO.setIdSiniestro(siniestroCabina.getId());
			ordenCompraDTO.setNumeroSiniestro(siniestroCabina.getNumeroSiniestro());
		}
		String siniestroTercero = (orden.getCartaPago()!=null)?orden.getCartaPago().getSiniestroTercero():null;
		ordenCompraDTO.setSiniestroTercero(siniestroTercero);
		ordenCompraDTO.setIsr(importeDTO.getIsr());
		ordenCompraDTO.setIva(importeDTO.getIva());
		ordenCompraDTO.setIvaRetenido(importeDTO.getIvaRetenido());
		ordenCompraDTO.setSubtotal(importeDTO.getSubtotal());
		ordenCompraDTO.setTotal(importeDTO.getTotal());
		if(orden.getIdTercero() != null)
		{
			estimacion =  entidadService.findById(EstimacionCoberturaReporteCabina.class,orden.getIdTercero());
			if (estimacion!=null){
				ordenCompraDTO.setAfectado(estimacion.getNombreAfectado());
				ordenCompraDTO.setOficina(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getOficina().getNombreOficina());
				ordenCompraDTO.setFolioPaseAtencion( estimacion.getFolio() );
			}
			
		}
		
		ordenCompraDTO.setFechaCarta( (orden.getCartaPago()!=null)?orden.getCartaPago().getFechaCreacion():null );
		ordenCompraDTO.setFechaRecepcionCarta( (orden.getCartaPago()!=null)?orden.getCartaPago().getFechaRecepcion():null );
		ordenCompraDTO.setReservaDisponible(movimientoSiniestroService.obtenerReservaAfectacion(orden.getIdTercero(), Boolean.TRUE));
		return ordenCompraDTO;
		
	}
	
	
	
	
	
	
	
	
	
	@Override
	public void cancelarOrdenPago(Long idOrdenCompra) throws Exception {
		OrdenCompra ordenCompra=this.obtenerOrdenCompra(idOrdenCompra);
		if(null==ordenCompra){
			 throw new Exception ("La orden de compra numero: "+idOrdenCompra+". No existe");
		 }	
		 if (null!=ordenCompra.getOrdenPago()){
			 if(ordenCompra.getOrdenPago().getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_LIBERADA)){
				 throw new Exception ("No se puede cancelar la orden de Compra numero: "+idOrdenCompra +". Debido a que cuenta con una Orden de Pago Liberada");
			 }
			 if(ordenCompra.getOrdenPago().getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_PAGADA)){
				 throw new Exception ("No se puede cancelar la orden de Compra numero: "+idOrdenCompra +". Debido a que cuenta con una Orden de Pago Pagada");
			 }
			 
			 if(ordenCompra.getOrdenPago().getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_ASOCIADA)){
				 throw new Exception ("No se puede cancelar la orden de Compra numero: "+idOrdenCompra +". Debido a que cuenta con una Orden de Pago Asociada a una Liquidacion");
			 }
			 
		 }
		 ordenCompra.setEstatus(OrdenCompraService.ESTATUS_CANCELADA);
		  this.guardarOrdenCompra(ordenCompra);
		  this.reiniciaIndemnizacion(ordenCompra.getId());
		
	}
	@Override
	public List<OrdenCompraRecuperacionDTO> obtenerOrdenCompraRecuperacionProveedor(
			Long idReporteCabina,String coberturaCompuesta,
			List<Long> pasesAtencionId, String tipoOrdenCompra,Long idOrdenCompra) {
	 return	this.ordenCompraDao.obtenerOrdenCompraRecuperacionProveedor(idReporteCabina,  coberturaCompuesta, pasesAtencionId, tipoOrdenCompra,idOrdenCompra);
	}
	
	public BigDecimal obtenerTotalesOrdenesCompra(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo, String claveSubCalculo, Date fechaCreacionUltimoMovimiento, String tipoOrdenCompra, String estatus) {

		BigDecimal result = ordenCompraDao.obtenerTotalesOrdenesCompra(reporteCabinaId, coberturaId, estimacionId, claveTipoCalculo, claveSubCalculo,
				fechaCreacionUltimoMovimiento, tipoOrdenCompra, estatus);
		if (result == null) {
			result = BigDecimal.ZERO;
		}
		return result;
	}
	//@Override
	public String validarAplicaDeducible(Long idEstimacion, Long idOrdenCompra) {	
			List<RecuperacionDeducible>  lista = recuperacionDeducibleService.obtenerRecuperacionDeduciblePorEstimacion(idEstimacion);
			for(RecuperacionDeducible recuperacion : lista){
				Map<String,Object> params = new HashMap<String, Object>();
				params.put("idTercero",idEstimacion);
				params.put("aplicaDeducible",Boolean.TRUE);
				StringBuilder queryWhere = queryWhereValid(false);
				if(idOrdenCompra!=null){
					queryWhere.append(" and model.id not in ( "+idOrdenCompra+" ) ");
				}
				List<OrdenCompra>  ordenes  = entidadService.findByColumnsAndProperties(OrdenCompra.class, "id", params, new HashMap<String,Object>(), queryWhere, null);
				if(CollectionUtils.isNotEmpty(ordenes)){
					OrdenCompra ordenActiva  = ordenes.get(0);
					return "El pase de atencion tiene una recuperacion de deducible y esta asociado a una orden de compra activa No. "+ordenActiva.getId();
				}else{
					continue;
				}
			}

			return null;	
	}
	
	/**
	 * Se agrega 
	 */
	public String validarAplicaDeducibleIncluirPagado(Long idEstimacion, Long idOrdenCompra) {	
		List<RecuperacionDeducible>  lista = recuperacionDeducibleService.obtenerRecuperacionDeduciblePorEstimacion(idEstimacion);
		for(RecuperacionDeducible recuperacion : lista){
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("idTercero",idEstimacion);
			params.put("aplicaDeducible",Boolean.TRUE);
			StringBuilder queryWhere = queryWhereValid(true);
			if(idOrdenCompra!=null){
				queryWhere.append(" and model.id not in ( "+idOrdenCompra+" ) ");
			}
			List<OrdenCompra>  ordenes  = entidadService.findByColumnsAndProperties(OrdenCompra.class, "id", params, new HashMap<String,Object>(), queryWhere, null);
			if(CollectionUtils.isNotEmpty(ordenes)){
				OrdenCompra ordenActiva  = ordenes.get(0);
				return "El pase de atencion tiene una  orden de compra de beneficiario activa No. "+ordenActiva.getId();
			}else{
				continue;
			}
		}

		return null;	
}

	@Override
	public String validarCrearOrdenCompra(OrdenCompra ordenCompra) {
		if (!StringUtil.isEmpty(ordenCompra.getEstatus())  
				&& !ordenCompra.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_TRAMITE) 
				&& !ordenCompra.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_CANCELADA)){
			String est=ordenCompra.getEstatus();
			return "No esta permitido actualizar la orden de compra al estatus "+": "+est;
		}	
		if ( StringUtil.isEmpty(ordenCompra.getNomBeneficiario()) &&  null== (ordenCompra.getIdBeneficiario())){
			 return "Debe Capturar o Seleccionar Beneficiario";
		}
		if ( StringUtil.isEmpty(ordenCompra.getTipoPago())){	
			return "Debe Seleccionar Tipo Pago";	
		}
		if (ordenCompra.getTipoPago().equalsIgnoreCase(OrdenCompraService.PAGO_A_BENEFICIARIO)  && StringUtils.isEmpty(ordenCompra.getTipoPersona())){
			 return "Debe seleccionar el tipo persona";
		}
		
		if ( StringUtil.isEmpty(ordenCompra.getTipo())){
			 return "Debe Seleccionar Tipo Orden Compra";		
		}		
		
		if(ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_AFECTACION_RESERVA)){
			if(ordenCompra.getCoberturaReporteCabina()==null ){
				return  "Debe Seleccionar Cobertura";
			}
			if(ordenCompra.getIdTercero()==null ){
				return  "Debe Seleccionar Tercero Afectado";
			}
		}
		
		if(ordenCompra!=null  &&  ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_AFECTACION_RESERVA) 
				&& ordenCompra.getIdTercero()!= null  &&  ordenCompra.getAplicaDeducible()!= null && ordenCompra.getAplicaDeducible()){
			String mj =this.validarAplicaDeducibleIncluirPagado(ordenCompra.getIdTercero(), ordenCompra.getId());
			if(!StringUtil.isEmpty(mj)){
				return mj;
			}
			BigDecimal deducible  =this.obtenerDeducibleOrdenCompra(ordenCompra);
			if(deducible.compareTo(BigDecimal.ZERO)==0
					&& !this.deducibleYaRecuperado(ordenCompra)){				
				return "La orden de compra no puede ser de tipo Aplica deducible, el deducible del pase de atencion debe ser mayor a cero";
			}
			
		}
		
		if (ordenCompra.getTipoPago().equalsIgnoreCase(OrdenCompraService.PAGO_A_PROVEEDOR)  
				&& !StringUtil.isEmpty(ordenCompra.getTipoProveedor())
				&& ordenCompra.getTipoProveedor().equals("CIA")){
					if(!ordenCompra.getCoberturaReporteCabina().getClaveTipoCalculo().equals(CoberturaReporteCabina.ClaveTipoCalculo.DANIOS_MATERIALES.toString())){
						if(!ordenCompra.getCoberturaReporteCabina().getClaveTipoCalculo().equals(CoberturaReporteCabina.ClaveTipoCalculo.RESPONSABILIDAD_CIVIL.toString())){
							return "No se puede crear la orden de compra con la cobertura asociada";
						}
					}
			 
		}
		return null;
	}
	
	@Override
	public List<BandejaOrdenCompraDTO> buscarOrdenesCompra(
			BandejaOrdenCompraDTO filtro) {
		return this.ordenCompraDao.buscarOrdenesCompra(filtro);
	}
	
	@Override
	public Long contarOrdenesCompraBandeja(BandejaOrdenCompraDTO filtro){
		return this.ordenCompraDao.contarOrdenesCompraBandeja(filtro);
	}
	
}

