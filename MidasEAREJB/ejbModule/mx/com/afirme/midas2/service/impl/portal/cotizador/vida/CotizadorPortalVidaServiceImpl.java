package mx.com.afirme.midas2.service.impl.portal.cotizador.vida;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.domain.portal.cotizador.vida.Cobertura;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotMovilVidaDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.exeption.ErrorBuilder;
import mx.com.afirme.midas2.service.impl.movil.cotizador.vida.CotizacionMovilVidaServiceImpl;
import mx.com.afirme.midas2.service.movil.cotizador.vida.CotizacionMovilVidaService;
import mx.com.afirme.midas2.service.portal.cotizador.CotizadorPortalService;
import mx.com.afirme.midas2.service.portal.cotizador.vida.CotizadorPortalVidaService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.vida.domain.movil.cotizador.CrearCotizacionVidaParameterDTO;
import mx.com.afirme.vida.domain.movil.cotizador.OccupationDTO;
import mx.com.afirme.vida.domain.movil.cotizador.SumaAseguradaDTO;
import mx.com.afirme.vida.domain.movil.cotizador.TarifasMovilVidaDTO;
import mx.com.afirme.vida.service.tarifa.TarifaMovilVidaService;

import org.apache.log4j.Logger;

@Stateless
public class CotizadorPortalVidaServiceImpl extends EntidadHistoricoDaoImpl
	implements CotizadorPortalVidaService{
	
	private static final Logger LOG = Logger.getLogger(CotizadorPortalVidaServiceImpl.class);
	private static final Integer ID_SEYCOS_COBERTURA_BASICA = 974;
	private static final Integer ID_SEYCOS_COBERTURA_PLATINO = 1145;
	private static final String COBERTURA_BASICA = "BASICA";
	private static final String COBERTURA_PLATINO = "PLATINO";
	private static final String CVE_SEXO_MASCULINO = "M";
	private static final String CVE_SEXO_FEMENINO = "F";
	
	@EJB
	private CotizacionMovilVidaService cotizacionMovilVidaService;
	@EJB
	private CotizadorPortalService cotizacionPortalService;
	@EJB
	private GenerarPlantillaReporte generarPlantillaReporte;
	@EJB
	private TarifaMovilVidaService tarifaMovilVidaService;
	@EJB
	private MailService mailService;
	@EJB
	private CatalogoValorFijoFacadeRemote beanRemoto;
	
	public Object cotizar(CrearCotizacionVidaParameterDTO param, boolean contratar) {
		ErrorBuilder eb = new ErrorBuilder();
		String sumaAsegurada = obtenerSumaAsegurada(param.getIdSumaAsegurada());
		
		if (param.getCveSexo() == null || param.getCveSexo().isEmpty()
				|| param.getCveSexo().equals("")) {
			throw new ApplicationException(eb.addFieldError("cveSexo",
					"Debe indicar el sexo."));
		} else if (!CVE_SEXO_MASCULINO.equals(param.getCveSexo()) && !CVE_SEXO_FEMENINO.equals(param.getCveSexo())) {
			throw new ApplicationException(eb.addFieldError("cveSexo",
			"Debe indicar una clave de sexo  v\u00E1lida."));
		} else if (param.getFNacimiento() == null) {
			throw new ApplicationException(eb.addFieldError("FNacimiento",
					"Debe indicar la fecha de nacimiento."));
		} else if (UtileriasWeb
				.isFechaValida(param.getFNacimiento().toString())) {
			throw new ApplicationException(eb.addFieldError("FNacimiento",
					"La fecha de nacimiento no es v\u00E1lida."));
		} else if (param.getEstatura() == null
				|| param.getEstatura().equals("")) {
			throw new ApplicationException(eb.addFieldError("estatura",
					"Debe indicar la estatura."));
		} else if (param.getPeso() == null
				|| param.getPeso().equals("")) {
			throw new ApplicationException(eb.addFieldError("peso",
					"Debe indicar el peso."));
		} else if (param.getIdSumaAsegurada() == null
				|| param.getIdSumaAsegurada().equals("")) {
			throw new ApplicationException(eb.addFieldError("idSumaAsegurada",
					"Debe indicar una suma asegurada."));
		} else if (param.getIdGiroOcup() == null
				|| param.getIdGiroOcup().equals("")) {
			throw new ApplicationException(eb.addFieldError("idGiroOcup",
					"Debe indicar una ocupaci\u00F3n."));
		} else if (sumaAsegurada == null
				|| sumaAsegurada.equals("")) {
			throw new ApplicationException(eb.addFieldError("sumaAsegurada",
			"Debe indicar una suma asegurada."));
		}
		
		param.setSumaAsegurada(new Double(sumaAsegurada));
		param.setTipoCotizacion(CrearCotizacionVidaParameterDTO.TIPO_COTIZACION_PORTAL);
		param.setContratar(contratar);
		
		if(contratar) {
			if (param.getFormaPago() == null || param.getFormaPago().isEmpty()
					|| param.getFormaPago().equals("")) {
				throw new ApplicationException(eb.addFieldError("formaPago",
						"Debe indicar la forma de pago."));
			}  else if (param.getCobertura() == null || param.getCobertura().isEmpty()
					|| param.getCobertura().equals("")) {
				throw new ApplicationException(eb.addFieldError("cobertura",
				"Debe indicar una cobertura."));
			} else if (!COBERTURA_BASICA.equals(param.getCobertura()) && !COBERTURA_PLATINO.equals(param.getCobertura())) {
				throw new ApplicationException(eb.addFieldError("cobertura",
				"Debe indicar una cobertura  v\u00E1lida."));
			} else if(param.getNombre() == null || param.getNombre().isEmpty()
					|| param.getNombre().equals("")) {
				throw new ApplicationException("Debe proporcionar el nombre del prospecto para enviar la cotizaci\u00F3n.");
			} else if(param.getApellidoPaterno() == null || param.getApellidoPaterno().isEmpty()
					|| param.getApellidoPaterno().equals("")) {
				throw new ApplicationException("Debe proporcionar el apellido paterno del prospecto para enviar la cotizaci\u00F3n.");
			} else if(param.getApellidoMaterno() == null || param.getApellidoMaterno().isEmpty()
					|| param.getApellidoMaterno().equals("")) {
				throw new ApplicationException("Debe proporcionar el apellido materno del prospecto para enviar la cotizaci\u00F3n.");
			} else if (param.getTelCasa() == null || param.getTelCasa().isEmpty()
					|| param.getTelCasa().equals("")) {
				throw new ApplicationException(eb.addFieldError("telCasa",
						"Debe indicar el telefono del prospecto."));
			} else if(param.getMail() == null || param.getMail().isEmpty()
					|| param.getMail().equals("")) {
				throw new ApplicationException("Debe proporcionar un email para enviar la cotizaci\u00F3n.");
			} else if (param.getIdCotizacion() == null || 
					param.getIdCotizacion().equals("")) {
				throw new ApplicationException(eb.addFieldError("idCotizacion",
				"Debe indicar una cotización  v\u00E1lida."));
			}
			
			param.setIdFormaPago(cotizacionPortalService.getFormaPago(param.getFormaPago()).intValue());
			param.setIdCobertura(this.getCobertura(param.getCobertura()));
			
			CotizacionMovilDTO cotizacionMovil = new CotizacionMovilDTO();
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("idtocotizacionseycos", param.getIdCotizacion());
			try {
				cotizacionMovil = cotizacionPortalService.buscaCotizacion(params);
				param.setProcesada(cotizacionMovil.getProcesada());
			} catch (Exception e){
				LOG.error("Excepcion general en cotizar...", e);
			}
			
			if(cotizacionMovil == null || cotizacionMovil.getProcesada() == null || cotizacionMovil.getProcesada()) {
				param.setIdCotizacion(null);
				param.setIdToCotizacionMovil(null);
			} else {
				param.setIdToCotizacionMovil(cotizacionMovil.getIdtocotizacionmovil());
			}
		}
		
		return cotizacionMovilVidaService.cotizar(param);
	}

	public List<SumaAseguradaDTO> obtenerSumaAseguradaList() {
		List<SumaAseguradaDTO> listaSumaAseguradaDTO = new ArrayList<SumaAseguradaDTO>();
		List<CatalogoValorFijoDTO> catalogoValorFijoDTOs = null;
		try {
			catalogoValorFijoDTOs = beanRemoto.findByProperty(
					"id.idGrupoValores",
					Integer.valueOf(CotizacionMovilVidaServiceImpl.ID_MOSTRAR_SUMA_ASEGURADA_MOVIL));
			if (!catalogoValorFijoDTOs.isEmpty()
					&& catalogoValorFijoDTOs != null) {
				for (CatalogoValorFijoDTO estatus : catalogoValorFijoDTOs) {
					SumaAseguradaDTO sumaAseguradaDTO = new SumaAseguradaDTO();
					sumaAseguradaDTO.setSumaAsegurada(UtileriasWeb.formatoMoneda(new Double(estatus.getDescripcion())));
					sumaAseguradaDTO.setIdSumaAsegurada(new Long(estatus
							.getId().getIdDato()));
					listaSumaAseguradaDTO.add(sumaAseguradaDTO);
				}
			}
		} catch (Exception e) {
			LOG.error("Excepcion general en obtenerSumaAseguradaList...", e);
		}
		return listaSumaAseguradaDTO;
	}
	
	private String obtenerSumaAsegurada (Long idSumaAsegurada) {
		String sumaAsegurada = null;
		try {
			sumaAsegurada = beanRemoto.getDescripcionCatalogoValorFijo(
					Integer.valueOf(CotizacionMovilVidaServiceImpl.ID_MOSTRAR_SUMA_ASEGURADA_MOVIL), 
					idSumaAsegurada.toString());
		} catch (Exception e) {
			LOG.error("Excepcion general en obtenerSumaAsegurada...", e);
		}
		return sumaAsegurada;
	}
	
	public List<OccupationDTO> getGiroOcupacion(OccupationDTO filter) {
		if(filter.getGiroOcupacion() == null) {
			throw new ApplicationException("Debe proporcionar la descripci\u00F3n de una ocupaci\u00F3n.");
		}
	
		return cotizacionMovilVidaService.getGiroOcupacion(filter);
	}
	
	public List<String> getPaquetes() {
		List<String> map = new ArrayList<String>(1);
		map.add(COBERTURA_BASICA);
		map.add(COBERTURA_PLATINO);

		return map;
	}
	
	public Integer getCobertura(String cobertura) {
		Integer idCobertura = null;
		if (COBERTURA_BASICA.equals(cobertura)) {
			idCobertura = ID_SEYCOS_COBERTURA_BASICA;
		} else if (COBERTURA_PLATINO.equals(cobertura)) {
			idCobertura = ID_SEYCOS_COBERTURA_PLATINO;
		} else {
			throw new ApplicationException("Debe indicar una cobertura  v\u00E1lida.");
		}

		return idCobertura;
	}
	
	public Map<String, String> getClaveSexo() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put(CVE_SEXO_MASCULINO, "MASCULINO");
		map.put(CVE_SEXO_FEMENINO, "FEMENINO");

		return map;
	}
	
	public byte[] imprimirCotizacion(CrearCotizacionVidaParameterDTO param) {
		TarifasMovilVidaDTO tarifasMovilVidaDTO = new TarifasMovilVidaDTO();
		ResumenCotMovilVidaDTO resumenCotMovilVidaDTO = new ResumenCotMovilVidaDTO();
		CotizacionMovilDTO cotizacionMovil = new CotizacionMovilDTO();

		String mensualBasico;
		String trimestralBasico;
		String semestralBasico;
		String mensualPlatino;
		String trimestralPlatino;
		String semestralPlatino;
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("idtocotizacionseycos", param.getIdCotizacion());
		cotizacionMovil = cotizacionPortalService.buscaCotizacion(params);
		param.setFNacimiento(cotizacionMovil.getFechanacimiento());
		
		tarifasMovilVidaDTO = tarifaMovilVidaService.findFechaNacimientoTarifaMovilVida(param.getFNacimiento());
		
		MathContext mc = new MathContext(2, RoundingMode.HALF_EVEN);
		
		BigDecimal tB = tarifasMovilVidaDTO.getTarifaBasica();
		
		BigDecimal totalMB = tB.divide(new BigDecimal(CotizacionMovilVidaServiceImpl.FORMA_PAGO_PERIODO_MENSUAL), MathContext.DECIMAL32).round(mc);
		mensualBasico = UtileriasWeb.formatoMoneda(totalMB.multiply(new BigDecimal(CotizacionMovilVidaServiceImpl.CALCULA_PAGO_MENSUAL)));
			    
		BigDecimal totalTB = tB.divide(new BigDecimal(CotizacionMovilVidaServiceImpl.FORMA_PAGO_PERIODO_TRIMESTRAL), MathContext.DECIMAL32).round(mc);
		trimestralBasico = UtileriasWeb.formatoMoneda(totalTB.multiply(new BigDecimal(CotizacionMovilVidaServiceImpl.CALCULA_PAGO_TRIMESTRAL)));
				
		BigDecimal totalSB = tB.divide(new BigDecimal(CotizacionMovilVidaServiceImpl.FORMA_PAGO_PERIODO_SEMESTRAL), MathContext.DECIMAL32).round(mc);
		semestralBasico = UtileriasWeb.formatoMoneda(totalSB.multiply(new BigDecimal(CotizacionMovilVidaServiceImpl.CALCULA_PAGO_SEMESTRAL)));
				
		BigDecimal tP = tarifasMovilVidaDTO.getTarifaPlatino();
		
		BigDecimal totalMP = tP.divide(new BigDecimal(CotizacionMovilVidaServiceImpl.FORMA_PAGO_PERIODO_MENSUAL), MathContext.DECIMAL32).round(mc);
		mensualPlatino = UtileriasWeb.formatoMoneda(totalMP.multiply(new BigDecimal(CotizacionMovilVidaServiceImpl.CALCULA_PAGO_MENSUAL)));
				
		BigDecimal totalTP = tP.divide(new BigDecimal(CotizacionMovilVidaServiceImpl.FORMA_PAGO_PERIODO_TRIMESTRAL), MathContext.DECIMAL32).round(mc);
		trimestralPlatino = UtileriasWeb.formatoMoneda(totalTP.multiply(new BigDecimal(CotizacionMovilVidaServiceImpl.CALCULA_PAGO_TRIMESTRAL)));
				
		BigDecimal totalSP = tP.divide(new BigDecimal(CotizacionMovilVidaServiceImpl.FORMA_PAGO_PERIODO_SEMESTRAL), MathContext.DECIMAL32).round(mc);
		semestralPlatino = UtileriasWeb.formatoMoneda(totalSP.multiply(new BigDecimal(CotizacionMovilVidaServiceImpl.CALCULA_PAGO_SEMESTRAL)));
				
		String derechoPoliza = UtileriasWeb.formatoMoneda(CotizacionMovilVidaServiceImpl.DERECHOPOLIZA);
				
		resumenCotMovilVidaDTO.setCotizacionMovil(cotizacionMovil);
		resumenCotMovilVidaDTO.setMensualBasico(mensualBasico);
		resumenCotMovilVidaDTO.setTrimestralBasico(trimestralBasico);
		resumenCotMovilVidaDTO.setSemestralBasico(semestralBasico);
		resumenCotMovilVidaDTO.setMensualPlatino(mensualPlatino);
		resumenCotMovilVidaDTO.setTrimestralPlatino(trimestralPlatino);
		resumenCotMovilVidaDTO.setSemestralPlatino(semestralPlatino);
		resumenCotMovilVidaDTO.setEdad(cotizacionMovilVidaService.calcularEdad(param).toString());
		resumenCotMovilVidaDTO.setDerechoPoiza(derechoPoliza);
		return generarPlantillaReporte.imprimirCotizacionMovilVida(resumenCotMovilVidaDTO);
	}
	
	public void enviarCorreo(CrearCotizacionVidaParameterDTO param) {
		ByteArrayAttachment attachment = null;
		List<String> destinatarios = new ArrayList<String>();
		List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
		String nombreCompleto = null;
		
		if(param.getMail() == null || param.getMail().isEmpty()
				|| param.getMail().equals("")) {
			throw new ApplicationException("Debe proporcionar un email para enviar la cotizaci\u00F3n.");
		} else if(param.getNombre() == null || param.getNombre().isEmpty()
				|| param.getNombre().equals("")) {
			throw new ApplicationException("Debe proporcionar el nombre del prospecto para enviar la cotizaci\u00F3n.");
		} else if(param.getApellidoPaterno() == null || param.getApellidoPaterno().isEmpty()
				|| param.getApellidoPaterno().equals("")) {
			throw new ApplicationException("Debe proporcionar el apellido paterno del prospecto para enviar la cotizaci\u00F3n.");
		} else if(param.getApellidoMaterno() == null || param.getApellidoMaterno().isEmpty()
				|| param.getApellidoMaterno().equals("")) {
			throw new ApplicationException("Debe proporcionar el apellido materno del prospecto para enviar la cotizaci\u00F3n.");
		}
		
		attachment = new ByteArrayAttachment(
				CrearCotizacionVidaParameterDTO.PORTALPDF, TipoArchivo.PDF,
					this.imprimirCotizacion(param));
		
		if (attachment != null) {
			adjuntos.add(attachment);
			destinatarios.add(param.getMail().toString());
			
			/* correo a prospecto */
			nombreCompleto = param.getNombre() + " " + param.getApellidoPaterno() + " " + param.getApellidoMaterno();
			mailService.sendMail(destinatarios,
					nombreCompleto.toLowerCase().toString()
					.concat("-").concat(param.getIdCotizacion().toString()),
					 "Se le ha enviado una cotización a través de este medio.", 
					 adjuntos,
					 "Cotizacion Vida", 
					 "Estimado(s):" + nombreCompleto.toLowerCase().toString());
		}
	}
	
	@Override
	public List<String> getFormasPago() {
		return cotizacionPortalService.getFormasPago();
	}
	
	public List<Cobertura> cargarDetalleCoberturas(CrearCotizacionVidaParameterDTO param) {
		ErrorBuilder eb = new ErrorBuilder();
		List<Cobertura> coberturasList = new ArrayList<Cobertura>(1);
		
		if (param.getIdCotizacion() == null || 
				param.getIdCotizacion().equals("")) {
			throw new ApplicationException(eb.addFieldError("idCotizacion",
			"Debe indicar una cotización  v\u00E1lida."));
		} else if (param.getCobertura() == null || param.getCobertura().isEmpty()
				|| param.getCobertura().equals("")) {
			throw new ApplicationException(eb.addFieldError("cobertura",
			"Debe indicar una cobertura."));
		} else if (!COBERTURA_BASICA.equals(param.getCobertura()) && !COBERTURA_PLATINO.equals(param.getCobertura())) {
			throw new ApplicationException(eb.addFieldError("cobertura",
			"Debe indicar una cobertura  v\u00E1lida."));
		}
		
		CotizacionMovilDTO cotizacionMovil = new CotizacionMovilDTO();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("idtocotizacionseycos", param.getIdCotizacion());
		cotizacionMovil = cotizacionPortalService.buscaCotizacion(params);

		Cobertura cobertura = new Cobertura();
		cobertura.setNombreComercial(COBERTURA_BASICA);
		cobertura.setValorSumaAseguradaStr(UtileriasWeb.formatoMoneda(cotizacionMovil.getSumaasegurada()));
		coberturasList.add(cobertura);
		
		if (COBERTURA_PLATINO.equals(param.getCobertura())) {
			cobertura = new Cobertura();
			cobertura.setNombreComercial(COBERTURA_PLATINO);
			cobertura.setValorSumaAseguradaStr(UtileriasWeb.formatoMoneda(cotizacionMovil.getSumaasegurada()));
			coberturasList.add(cobertura);
		}

		return coberturasList;
	}
}
