package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.fuerzaventa.ProductoBancarioDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancario;
import mx.com.afirme.midas2.service.fuerzaventa.ProductoBancarioService;
import mx.com.afirme.midas2.service.impl.catalogos.EntidadHistoricoServiceImpl;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class ProductoBancarioServiceImpl extends EntidadHistoricoServiceImpl implements ProductoBancarioService{
	private ProductoBancarioDao productoBancarioDao;
	@Override
	public List<ProductoBancario> findByBanco(Long idBanco) {
		LogDeMidasInterfaz.log("Entrando a findByBanco... return productoBancarioDao.findByBanco("+idBanco+")" + this, Level.INFO, null);
		return productoBancarioDao.findByBanco(idBanco);
	}

	@Override
	public List<ProductoBancario> findByFilters(ProductoBancario filtro) {
		return productoBancarioDao.findByFilters(filtro);
	}
	/***Sets and gets*************************************/
	@EJB
	public void setProductoBancarioDao(ProductoBancarioDao productoBancarioDao) {
		this.productoBancarioDao = productoBancarioDao;
	}
}
