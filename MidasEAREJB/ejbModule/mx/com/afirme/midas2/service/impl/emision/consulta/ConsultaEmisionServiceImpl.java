package mx.com.afirme.midas2.service.impl.emision.consulta;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.emision.consulta.ConsultaEmisionDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.emision.consulta.Consulta;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaAgente;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaAnexo;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaCliente;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaCobertura;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaCobranza;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaEmision;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaEndoso;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaPoliza;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaSiniestro;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaVehiculo;
import mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta;
import mx.com.afirme.midas2.service.emision.consulta.ConsultaEmisionService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaJPAService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.springframework.beans.BeanUtils;

@Stateless
public class ConsultaEmisionServiceImpl implements ConsultaEmisionService {

	@Override
	public List<Consulta> navegar(ConsultaEmision consultaEmision, String seccionStr, Integer posStart, Integer count, String orderBy, String direct) {
		
		if (consultaEmision == null || seccionStr == null || seccionStr.trim().equals("")) return null;
		
		Consulta seccion = getConsultaFromSeccion(seccionStr);
		
		if (seccion == null) return null;
		
		if (posStart == null) posStart = 0;
		if (count == null) count = NUMERO_REGISTROS;
		
		
		Consulta consulta = null;
		int comparacion = 0;
		Consulta seccionNavegar = null;
		Consulta ultimaSeccionSeleccionada = null;
		List<Consulta> consultaList = null;
		
		try {
		
			PropertyUtilsBean utilsBean = new PropertyUtilsBean();
			
			for (Field field : ConsultaEmision.class.getDeclaredFields()) {
					
				consulta = (Consulta) utilsBean.getProperty(consultaEmision, field.getName());
				
				if (consulta.getClass().equals(seccion.getClass())) {
					seccionNavegar = consulta;
					seccionNavegar.setPrimerRegistroACargar(posStart);
					seccionNavegar.setNumeroMaximoRegistrosACargar(count);
					seccionNavegar.setOrderBy(orderBy);
					seccionNavegar.setDirect(direct);
				}
				
				if (consulta.getUltimaSeleccion()) {
					ultimaSeccionSeleccionada = consulta;		
					ultimaSeccionSeleccionada.setPrimerRegistroACargar(posStart);	
					ultimaSeccionSeleccionada.setNumeroMaximoRegistrosACargar(count);
					ultimaSeccionSeleccionada.setOrderBy(orderBy);
					ultimaSeccionSeleccionada.setDirect(direct);
				}
				if (seccionNavegar != null && ultimaSeccionSeleccionada != null) break;
			}
			
			
			if (ultimaSeccionSeleccionada == null) {
				consultaList = buscar(seccionNavegar, seccionNavegar.getClass());
			} else {
				comparacion = seccionNavegar.getNivel().getValue().compareTo(ultimaSeccionSeleccionada.getNivel().getValue());
				
				if (comparacion > 0 || (comparacion == 0 && seccionNavegar.getClass().equals(ultimaSeccionSeleccionada.getClass()))) {
					consultaList = buscar(seccionNavegar, seccionNavegar.getClass());
				} else {
					consultaList = buscar(ultimaSeccionSeleccionada, seccionNavegar.getClass());
					seccionNavegar.setTotalRegistros(ultimaSeccionSeleccionada.getTotalRegistros());
				}
			}
		
			return consultaList;
		
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		
	}

	@Override
	public ConsultaEmision seleccionar(ConsultaEmision consultaEmision, String seccionStr) {
		
		if (consultaEmision == null || seccionStr == null || seccionStr.trim().equals("")) return null;
		
		Consulta seccion = getConsultaFromSeccion(seccionStr);
		
		if (seccion == null) return consultaEmision;
		
		Consulta seleccionado = null;
		
		//Se usa reflection para consultar cada una de las propiedades de ConsultaEmision (que no sea ArrayList)
		//y revisar si esa propiedad es padre del objeto "seleccionado". En caso de ser asi realizar la busqueda y del listado
		//de resultados obtener el primer elemento (solo deberia haber uno) y asignarlo a dicha propiedad de ConsultaEmision
		
		PropertyUtilsBean utilsBean = new PropertyUtilsBean();
		List<Consulta> consultaList;
		Consulta consulta;
		int comparacion = 0;
		
		try {
			
			//Ciclo 1: Identificar a la Consulta seleccionada
			for (Field field : ConsultaEmision.class.getDeclaredFields()) {
				consulta = (Consulta) utilsBean.getProperty(consultaEmision, field.getName());
				
				if (seccion.getClass().equals(consulta.getClass())) {
					seleccionado = consulta.getClass().newInstance();
					BeanUtils.copyProperties(consulta, seleccionado);
					break;
				}
			}
			
			if (seleccionado == null) {
				throw new RuntimeException("Hubo un problema para obtener la consulta seleccionada...");
			}
			
			//Se limpia consultaEmision
			consultaEmision = new ConsultaEmision();
			
			//Ciclo 2 : Repoblar a consultaEmision
			for (Field field : ConsultaEmision.class.getDeclaredFields()) {
					
				consulta = (Consulta) utilsBean.getProperty(consultaEmision, field.getName());
				
				comparacion = consulta.getNivel().getValue().compareTo(seleccionado.getNivel().getValue());
				
				consulta.setUltimaSeleccion(false);
				
				if (comparacion > 0) {
					
					consultaList = buscar(seleccionado, consulta.getClass());
					
					if (consultaList != null && !consultaList.isEmpty()) {
						BeanUtils.copyProperties(consultaList.get(0), consulta);
						//utilsBean.setProperty(consultaEmision, field.getName(), consulta); En caso que no lo tenga referenciado ya
					}
					
				} else if (comparacion == 0 && seleccionado.getClass().equals(consulta.getClass())) {
					BeanUtils.copyProperties(seleccionado, consulta);
					consulta.setUltimaSeleccion(true);
				}
				
			}
				
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		
		return consultaEmision;
	}
	
	@SuppressWarnings("rawtypes")
	private List<Consulta> buscar(Consulta consulta, Class claseResultado) {
		List<Consulta> consultaListado = new ArrayListNullAware<Consulta>();
		if (consulta == null || claseResultado == null) return new ArrayListNullAware<Consulta>();
		
		try {
			//Se revisa si consulta es del tipo de claseResultado. Si es asi se procede con la busqueda
			if (consulta.getClass().equals(claseResultado)) {
				//Se revisa si el usuario es un agente, agente promotor o ejecutivo de venta. En caso de ser asi los resultados de la consulta se limitan 
				//a los que pertenezcan a dicho usuario segun su rol 
				filtraPorTipoUsuario(consulta);
				
				//Se valida si hay al menos un filtro seteado, y valida que exista un filtro especial para la secciones que lo requieran. 
				//En caso de no ser así la búsqueda no procede
				if (!validoFiltrosGenerales(consulta)) {
					 return new ArrayListNullAware<Consulta>();
				}
				
				if (claseResultado.equals(ConsultaCobranza.class)) {
					return consultaEmisionDao.getConsultaCobranza((ConsultaCobranza)consulta);
				} else if (claseResultado.equals(ConsultaCobertura.class)) {
					return consultaEmisionDao.getConsultaCobertura((ConsultaCobertura)consulta);
				} else if (claseResultado.equals(ConsultaEndoso.class)) {
					return consultaEmisionDao.getConsultaEndoso((ConsultaEndoso)consulta);
				} else if (claseResultado.equals(ConsultaAnexo.class)) {
					return consultaEmisionDao.getConsultaAnexo((ConsultaAnexo)consulta);
				} else {
					return consultaEmisionDao.getConsultaEmision(consulta);
				}
				
			} else {
				
				Consulta instanciaResultado = (Consulta) claseResultado.newInstance();
					
				//Se pobla la instancia resultado con los filtros necesarios proporcionados por consulta
				setFiltro(consulta, instanciaResultado);
				instanciaResultado.setPrimerRegistroACargar(0);
				instanciaResultado.setNumeroMaximoRegistrosACargar(NUMERO_REGISTROS);
				instanciaResultado.setOrderBy(consulta.getOrderBy());
				instanciaResultado.setDirect(consulta.getDirect());
				
				//Se manda llamar al metodo buscar usando instanciaResultado y claseResultado
				consultaListado =  buscar(instanciaResultado, claseResultado);
				consulta.setTotalRegistros(instanciaResultado.getTotalRegistros());
				return consultaListado;
				
			}
			
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}	
		
		//return null;
	}
	
	private void setFiltro(Consulta filtro, Consulta instanciaResultado){
		setFiltro(filtro, instanciaResultado, false);
	}
	
	private void setFiltro(Consulta filtro, Consulta instanciaResultado, boolean subFiltro) {
		
		int comparacion = 0;
		ConsultaAgente consultaAgente;
		ConsultaCliente consultaCliente;
		ConsultaPoliza consultaPoliza;
		ConsultaEndoso consultaEndoso;
		ConsultaAnexo consultaAnexo;
		ConsultaVehiculo consultaVehiculo;
		ConsultaCobertura consultaCobertura;
		ConsultaSiniestro consultaSiniestro;
		ConsultaCobranza consultaCobranza;
		List<Consulta> consultaList;
		Long polizaId = null;
		
		if (filtro != null && instanciaResultado != null) {
			
			comparacion = instanciaResultado.getNivel().getValue().compareTo(filtro.getNivel().getValue());
			
			if (comparacion > 0) {
				
				if (instanciaResultado instanceof ConsultaAgente) {
					
					if (filtro instanceof ConsultaPoliza) {
						consultaPoliza = (ConsultaPoliza) filtro;
					} else {
						consultaPoliza = new ConsultaPoliza();
						setFiltro(filtro, consultaPoliza, true);
					}
					
					((ConsultaAgente) instanciaResultado).setId(consultaPoliza.getAgenteId());
					
				} else if (instanciaResultado instanceof ConsultaCliente) {
					
					if (filtro instanceof ConsultaPoliza) {
						consultaPoliza = (ConsultaPoliza) filtro;
					} else {
						consultaPoliza = new ConsultaPoliza();
						setFiltro(filtro, consultaPoliza, true);
					}
					
					((ConsultaCliente) instanciaResultado).setId(consultaPoliza.getClienteId());
					
				} else if (instanciaResultado instanceof ConsultaPoliza) {
										
					if (filtro instanceof ConsultaVehiculo) {
						consultaVehiculo = (ConsultaVehiculo) filtro;
						polizaId = consultaVehiculo.getPolizaId();
							
					} else if (filtro instanceof ConsultaEndoso) {
						consultaEndoso = (ConsultaEndoso) filtro;
						polizaId = consultaEndoso.getPolizaId();
							
					} else if (filtro instanceof ConsultaAnexo) {
						consultaAnexo = (ConsultaAnexo) filtro;
						polizaId = consultaAnexo.getPolizaId();
							
					} else {
						consultaVehiculo = new ConsultaVehiculo();
						setFiltro(filtro, consultaVehiculo);
						polizaId = consultaVehiculo.getPolizaId();
					}
					
					((ConsultaPoliza) instanciaResultado).setId(polizaId);
					
					if (subFiltro) {
						
						instanciaResultado.setPrimerRegistroACargar(0);
						instanciaResultado.setNumeroMaximoRegistrosACargar(1);
					
						consultaList = buscar(instanciaResultado, instanciaResultado.getClass());
						
						if (consultaList != null && !consultaList.isEmpty()) {
							BeanUtils.copyProperties(consultaList.get(0), instanciaResultado);
						}
					}
					
				} else if (instanciaResultado instanceof ConsultaEndoso) {
					
					if (filtro instanceof ConsultaCobranza) {
						consultaCobranza = (ConsultaCobranza) filtro;
						((ConsultaEndoso) instanciaResultado).setNumeroEndoso(consultaCobranza.getNumeroEndoso());
						((ConsultaEndoso) instanciaResultado).setPolizaId(consultaCobranza.getPolizaId());
					}
					
				} else if (instanciaResultado instanceof ConsultaVehiculo) {
					
					Integer numeroInciso = null;
					
					if (filtro instanceof ConsultaCobertura) {
						consultaCobertura = (ConsultaCobertura) filtro;
						numeroInciso = consultaCobertura.getNumeroInciso();
						polizaId = consultaCobertura.getPolizaId();
						
					} else if (filtro instanceof ConsultaSiniestro) {
						consultaSiniestro = (ConsultaSiniestro) filtro;
						numeroInciso = consultaSiniestro.getNumeroInciso();
						polizaId = consultaSiniestro.getPolizaId();
						
					} else if (filtro instanceof ConsultaCobranza) {
						consultaCobranza = (ConsultaCobranza) filtro;
						numeroInciso = consultaCobranza.getNumeroInciso();
						polizaId = consultaCobranza.getPolizaId();
						
					}
					
					((ConsultaVehiculo) instanciaResultado).setNumeroInciso(numeroInciso);
					((ConsultaVehiculo) instanciaResultado).setPolizaId(polizaId);
										
				}
				
			} else if (comparacion < 0) {
				
				if (instanciaResultado instanceof ConsultaPoliza) {
					
					if (filtro instanceof ConsultaAgente) {
						consultaAgente = (ConsultaAgente) filtro;
						((ConsultaPoliza) instanciaResultado).setAgenteId(consultaAgente.getId());
						
					} else if (filtro instanceof ConsultaCliente) {
						consultaCliente = (ConsultaCliente) filtro;
						((ConsultaPoliza) instanciaResultado).setClienteId(consultaCliente.getId());
						
					}
				} else if (instanciaResultado instanceof ConsultaEndoso) {
					
					if (filtro instanceof ConsultaPoliza) {
						consultaPoliza = (ConsultaPoliza) filtro;
						((ConsultaEndoso) instanciaResultado).setPolizaId(consultaPoliza.getId());
						
					}
					
				} else if (instanciaResultado instanceof ConsultaAnexo) {
					
					if (filtro instanceof ConsultaPoliza) {
						consultaPoliza = (ConsultaPoliza) filtro;
						((ConsultaAnexo) instanciaResultado).setPolizaId(consultaPoliza.getId());
						
					}
					
				} else if (instanciaResultado instanceof ConsultaVehiculo) {
					
					if (filtro instanceof ConsultaPoliza) {
						consultaPoliza = (ConsultaPoliza) filtro;
						((ConsultaVehiculo) instanciaResultado).setPolizaId(consultaPoliza.getId());
						
					} else if (filtro instanceof ConsultaAgente) {
						consultaAgente = (ConsultaAgente) filtro;
						((ConsultaVehiculo) instanciaResultado).setAgenteId(consultaAgente.getId());
						
					} else if (filtro instanceof ConsultaCliente) {
						consultaCliente = (ConsultaCliente) filtro;
						((ConsultaVehiculo) instanciaResultado).setClienteId(consultaCliente.getId());
					}
					
				} else if (instanciaResultado instanceof ConsultaCobertura) {
					
					if (filtro instanceof ConsultaVehiculo) {
						consultaVehiculo = (ConsultaVehiculo) filtro;
						((ConsultaCobertura) instanciaResultado).setPolizaId(consultaVehiculo.getPolizaId());
						((ConsultaCobertura) instanciaResultado).setNumeroInciso(consultaVehiculo.getNumeroInciso());
						
					}
					
				} else if (instanciaResultado instanceof ConsultaSiniestro) {
					
					if (filtro instanceof ConsultaVehiculo) {
						consultaVehiculo = (ConsultaVehiculo) filtro;
						((ConsultaSiniestro) instanciaResultado).setPolizaId(consultaVehiculo.getPolizaId());
						((ConsultaSiniestro) instanciaResultado).setNumeroInciso(consultaVehiculo.getNumeroInciso());
						
					} else if (filtro instanceof ConsultaPoliza) {
						consultaPoliza = (ConsultaPoliza) filtro;
						((ConsultaSiniestro) instanciaResultado).setPolizaId(consultaPoliza.getId());
					} else if (filtro instanceof ConsultaAgente) {
						consultaAgente = (ConsultaAgente) filtro;
						((ConsultaSiniestro) instanciaResultado).setAgenteId(consultaAgente.getId());
					}
					
				} else if (instanciaResultado instanceof ConsultaCobranza) {
					
					if (filtro instanceof ConsultaVehiculo) {
						consultaVehiculo = (ConsultaVehiculo) filtro;
						((ConsultaCobranza) instanciaResultado).setPolizaId(consultaVehiculo.getPolizaId());
						((ConsultaCobranza) instanciaResultado).setNumeroInciso(consultaVehiculo.getNumeroInciso());
						
					} else if (filtro instanceof ConsultaPoliza) {
						consultaPoliza = (ConsultaPoliza) filtro;
						((ConsultaCobranza) instanciaResultado).setPolizaId(consultaPoliza.getId());
						
					} else if (filtro instanceof ConsultaEndoso) {
						consultaEndoso = (ConsultaEndoso) filtro;
						((ConsultaCobranza) instanciaResultado).setPolizaId(consultaEndoso.getPolizaId());
						((ConsultaCobranza) instanciaResultado).setNumeroEndoso(consultaEndoso.getNumeroEndoso());
						
					}
					
				}
				
			} else { //Son del mismo nivel, pero necesitan una propiedad del padre del filtro actual
				
				if (instanciaResultado instanceof ConsultaEndoso) {
					
					if (filtro instanceof ConsultaVehiculo) {
						consultaVehiculo = (ConsultaVehiculo) filtro;
						polizaId = consultaVehiculo.getPolizaId();
							
					} else if (filtro instanceof ConsultaAnexo) {
						consultaAnexo = (ConsultaAnexo) filtro;
						polizaId = consultaAnexo.getPolizaId();
					}
					
					((ConsultaEndoso) instanciaResultado).setPolizaId(polizaId);
					
					
				} else if (instanciaResultado instanceof ConsultaAnexo) {
					
					if (filtro instanceof ConsultaVehiculo) {
						consultaVehiculo = (ConsultaVehiculo) filtro;
						polizaId = consultaVehiculo.getPolizaId();
							
					} else if (filtro instanceof ConsultaEndoso) {
						consultaEndoso = (ConsultaEndoso) filtro;
						polizaId = consultaEndoso.getPolizaId();
					}
					
					((ConsultaAnexo) instanciaResultado).setPolizaId(polizaId);
					
				} else if (instanciaResultado instanceof ConsultaVehiculo) {
					
					if (filtro instanceof ConsultaEndoso) {
						consultaEndoso = (ConsultaEndoso) filtro;
						polizaId = consultaEndoso.getPolizaId();
							
					} else if (filtro instanceof ConsultaAnexo) {
						consultaAnexo = (ConsultaAnexo) filtro;
						polizaId = consultaAnexo.getPolizaId();
					}
					
					((ConsultaVehiculo) instanciaResultado).setPolizaId(polizaId);
					
				} else if (instanciaResultado instanceof ConsultaCobranza) {
					
					if (filtro instanceof ConsultaSiniestro) {
						consultaSiniestro = (ConsultaSiniestro) filtro;
						((ConsultaCobranza) instanciaResultado).setPolizaId(consultaSiniestro.getPolizaId());
						((ConsultaCobranza) instanciaResultado).setNumeroInciso(consultaSiniestro.getNumeroInciso());
						
					}
					
				} else if (instanciaResultado instanceof ConsultaAgente) {
					
					if (filtro instanceof ConsultaCliente) {
						consultaCliente = (ConsultaCliente) filtro;
						((ConsultaAgente) instanciaResultado).setId(consultaCliente.getAgenteId());
					}
					
				} else if (instanciaResultado instanceof ConsultaCliente) {
					
					if (filtro instanceof ConsultaAgente) {
						consultaAgente = (ConsultaAgente) filtro;
						((ConsultaCliente) instanciaResultado).setAgenteId(consultaAgente.getId());
					}
					
				}
				
			}
		
		}
		
	}
	
	private Consulta getConsultaFromSeccion(String seccion) {
		
		if (seccion.equals(SeccionConsulta.AGENTE)) {
			return new ConsultaAgente();
		} else if (seccion.equals(SeccionConsulta.CLIENTE)) {
			return new ConsultaCliente();
		} else if (seccion.equals(SeccionConsulta.POLIZA)) {
			return new ConsultaPoliza();
		} else if (seccion.equals(SeccionConsulta.ANEXO)) {
			return new ConsultaAnexo();
		} else if (seccion.equals(SeccionConsulta.ENDOSO)) {
			return new ConsultaEndoso();
		} else if (seccion.equals(SeccionConsulta.VEHICULO)) {
			return new ConsultaVehiculo();
		} else if (seccion.equals(SeccionConsulta.COBERTURA)) {
			return new ConsultaCobertura();
		} else if (seccion.equals(SeccionConsulta.SINIESTRO)) {
			return new ConsultaSiniestro();
		} else if (seccion.equals(SeccionConsulta.COBRANZA)) {
			return new ConsultaCobranza();
		}
		return null;
	}
	
	/**
	 * Se revisa si el usuario es un agente, agente promotor o ejecutivo de venta. En caso de ser asi los resultados de la consulta se limitan 
	 * a los que pertenezcan a dicho usuario segun su rol 
	 * @param consulta Filtro para la busqueda
	 */
	private void filtraPorTipoUsuario(Consulta consulta) {
		
		try {
			
			Agente agenteUsuarioActual = null;
			String filtroAutomaticoAgentes = null;
			String propiedadAgente = "id";
			List<Agente> agentes = new ArrayList<Agente>();
						
			//Se revisa el rol del usuario actual para determinar a que agentes seran limitadas las busquedas
			if (usuarioService.tieneRolUsuarioActual("Rol_M2_Ejecutivo_Ventas")) {
				//Ejecutivo de Venta
				
				Ejecutivo ejecutivoVenta = null;
				List<Agente> agentesPorPromotor = null;
				List<Promotoria> promotoriasEjecutivo = null;
				Usuario usuario = usuarioService.getUsuarioActual();
			
				Ejecutivo filtro = new Ejecutivo();
				filtro.setPersonaResponsable(new Persona());
				if(usuario != null && usuario.getPersonaId() != null)
					filtro.getPersonaResponsable().setIdPersona(Long.parseLong(usuario.getPersonaId()+""));
				
				List<Ejecutivo> resultadoBusqueda = ejecutivoService.findByFilters(filtro);
				
				if (resultadoBusqueda != null && resultadoBusqueda.size() > 0) {
					
					ejecutivoVenta = resultadoBusqueda.get(0);
				
					promotoriasEjecutivo = promotoriaService.findByEjecutivo(ejecutivoVenta.getId());
				
					for (Promotoria promotoria : promotoriasEjecutivo) {
						
						agentesPorPromotor = agenteMidasService.findAgentesByPromotoria(promotoria.getId());
						
						agentes.addAll(agentesPorPromotor);
					}
				
				}
					
			} else if (usuarioService.tieneRolUsuarioActual("Rol_M2_Promotor")) {
				//Promotor
				
				agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
				
				agentes = agenteMidasService.findAgentesByPromotoria(agenteUsuarioActual.getPromotoria().getId());
				
				agentes.add(agenteUsuarioActual);
				
			} else if (usuarioService.tieneRolUsuarioActual("Rol_M2_Agente")
					|| usuarioService.tieneRolUsuarioActual("Rol_M2_Conducto")
					|| usuarioService.tieneRolUsuarioActual("Rol_M2_Empresa")) {
				//Agente o Conducto
				
				agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
				
				agentes.add(agenteUsuarioActual);
				
			}
			
			filtroAutomaticoAgentes = formatoFiltroAutomaticoAgentes(agentes);
						
			//Aqui solo se setea la propiedad con el listado correspondiente en caso de que exista la propiedad en el objeto, 
			//esta se encuentre vacia y que haya un listado que poner
			if (filtroAutomaticoAgentes != null) {
				
				if (!(consulta instanceof ConsultaAgente)) {
					propiedadAgente = "agenteId";
				}
				
				PropertyUtilsBean utilsBean = new PropertyUtilsBean();
				
				if (utilsBean.getProperty(consulta, propiedadAgente) == null 
						|| ((String)utilsBean.getProperty(consulta, propiedadAgente)).trim().equals("")) {
					utilsBean.setProperty(consulta, propiedadAgente, filtroAutomaticoAgentes);
				}
			}
			
		
		} catch (NoSuchMethodException nsmex) {
			//Si no tiene alguna propiedad de id de Agente continua normalmente
			return;
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
	/**
	 * Se valida si hay al menos un filtro seteado, y valida que exista un filtro especial para la secciones que lo requieran. 
	 * @param consulta Filtro para la busqueda
	 * @return true si hay el menos un filtro seteado y un filtro especial en caso de ser una seccion que lo requiera. Regresa false en caso contrario
	 */
	private boolean validoFiltrosGenerales(Consulta consulta) {
		
		//Validaciones especiales
		if (consulta instanceof ConsultaCobranza) {
			if (((ConsultaCobranza)consulta).getPolizaId() == null) {
				return false;
			} else {
				return true;
			}
		} else if (consulta instanceof ConsultaCobertura) {
			if (((ConsultaCobertura)consulta).getPolizaId() == null || ((ConsultaCobertura)consulta).getNumeroInciso() == null) {
				return false;
			} else {
				return true;
			}
		} else if (consulta instanceof ConsultaEndoso) {
			if (((ConsultaEndoso)consulta).getPolizaId() == null) {
				return false;
			} else {
				return true;
			}
		} else if (consulta instanceof ConsultaAnexo) {
			if (((ConsultaAnexo)consulta).getPolizaId() == null) {
				return false;
			} else {
				return true;
			}
		}
		
		//Validaciones generales
		try {
			
			PropertyUtilsBean utilsBean = new PropertyUtilsBean();
			
			for (Field field : consulta.getClass().getDeclaredFields()) {
				
				if (!field.getType().isArray() 
						&& !field.getName().equals("serialVersionUID")
						&& !field.getName().startsWith("_persistence")
				) {
					
					if(utilsBean.getProperty(consulta, field.getName()) != null) {
						
						if ((field.getType().equals(String.class) 
								&& !((String)utilsBean.getProperty(consulta, field.getName())).trim().equals(""))
							|| (!field.getType().equals(String.class))) {
							return true;
						}
					}
				}
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
				
		return false;
	}
	
	public String formatoFiltroAutomaticoAgentes(List<Agente> agentes) {
		
		if (agentes == null || agentes.size() == 0) return null;
		
		StringBuilder filtro = new StringBuilder("");
		
		for (Agente agente : agentes) {
			filtro.append(agente.getIdAgente()).append(","); 
		}
			
		return filtro.substring(0, filtro.lastIndexOf(","));
	}
		
	
	private static final Integer NUMERO_REGISTROS = 1000;
	
	@EJB
	private ConsultaEmisionDao consultaEmisionDao;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private AgenteMidasService agenteMidasService;
	
	@EJB
	private PromotoriaJPAService promotoriaService;
	
	@EJB
	private EjecutivoService ejecutivoService;
	/*
	 
	 (VERSION 2)
	 
	 	 COMO SE PROCESAN LAS CONSULTAS

Dentro de una misma seccion, existe un boton de Buscar, el cual al oprimirlo toma los filtros ingresados y hace la consulta 
sobre la misma seccion, mostrando en un grid en la parte inferior de la seccion, los resultados de la consulta

Al selecccionar un elemento del grid de la parte inferior de la seccion, se muestra el detalle del elemento seleccionado en 
los campos usados como filtro. 
Ademas de esto, se limpian los valores del OG y se buscan todas las propiedades de niveles 
superiores a partir del id del elemento seleccionado. En el OG se guardan tanto el id del elemento seleccionado como todos 
los ids de sus padres (secciones de nivel superior)


Cada que se navega a una seccion (tab):

0.5) Se revisa si busquedaRealizada = false

1) Se revisa si la seccion a navegar se encuentra en el OG.
	1.1) En caso de ser asi, se revisa si tiene el nivel mas bajo en las propiedades del OG (Sobre el que se hizo la busqueda original)
		1.1.1) Si es asi, se hace una busqueda sobre los filtros de la seccion
		1.1.2) Se selecciona el elemento registrado en el OG. Se muestra su detalle en la seccion de filtros
	1.2) En caso contrario, se hace una busqueda sobre la propiedad correspondiente en el OG. Esta accion debera regresar un solo registro.
		1.2.1) Se selecciona el elemento registrado en el OG. Se muestra su detalle en la seccion de filtros

2) En caso contrario, se revisa el nivel de la seccion. Si existe una o mas propiedades con un nivel superior en el OG, estos se utilizan 
como filtros y se efectua una busqueda
	 
	 
3) Se marca la seccion con busquedaRealizada = true	 
	 
	 
	 */
	
	

}
