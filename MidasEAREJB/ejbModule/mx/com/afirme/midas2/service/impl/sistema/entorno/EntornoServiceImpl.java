package mx.com.afirme.midas2.service.impl.sistema.entorno;

import java.io.FileInputStream;
import java.util.PropertyResourceBundle;
import java.util.logging.Level;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas2.service.sistema.entorno.EntornoService;


@Stateless
public class EntornoServiceImpl implements EntornoService {

	private PropertyResourceBundle resourceBundle;
	
	public EntornoServiceImpl() {
	}
	
	@PostConstruct
	public void inicia() {		
		try {
			String nombreCompletoArchivoRecursos = ServiceLocatorP.getInstance().obtenerVariableEntorno("rutaVariablesEntorno");
			FileInputStream fis;
			fis = new FileInputStream(nombreCompletoArchivoRecursos);
			resourceBundle = new PropertyResourceBundle(fis);			
		} catch (Exception ex) {
			LogDeMidasEJB3.log("Fallo al cargar el archivo de variables de Entorno...", Level.WARNING, null);
		}
	}

	/**
	 * Obtiene el valor de una variable de Entorno de MIDAS
	 * @param nombreVariableEntorno Nombre de la variable de Entorno de Midas en el archivo VariablesEntornoMIDAS.properties
	 * @param valorDefault Valor por defecto que regresara en caso de que exista un error al cargar la variable externa
	 * @return El valor de la variable que se encuentra en el archivo VariablesEntornoMIDAS.properties
	 */
	@Override
	public String getVariable(String nombreVariableEntorno, String valorDefault) {
		String valor = null;
		if (resourceBundle != null) {
			try {
				valor = resourceBundle.getString(nombreVariableEntorno);
			} catch (Exception ex) {
				LogDeMidasEJB3.log("Fallo al cargar la variable de entorno: " + nombreVariableEntorno 
						+ " Se tomara el valor por defecto... ", Level.INFO, null);
				valor = valorDefault;
			}
		} else {
			valor = valorDefault;
		}

		return valor.trim();
	}

}
