package mx.com.afirme.midas2.service.impl.negocio.cliente;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.dao.negocio.cliente.NegocioClienteDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteJPA;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;
import mx.com.afirme.midas2.service.fuerzaventa.ClienteJPAService;
import mx.com.afirme.midas2.service.negocio.cliente.NegocioClienteService;
import mx.com.afirme.midas2.service.negocio.cliente.grupo.NegocioGrupoClienteService;

@Stateless
public class NegocioClienteServiceImpl implements NegocioClienteService {

	@EJB
	private NegocioClienteDao negocioClienteDao;
	
	@EJB
	private ClienteJPAService clienteJPAService;
	
	@EJB
	private ClienteFacadeRemote clienteFacadeRemote;
	
	@EJB
	private NegocioGrupoClienteService negocioGrupoClienteService;
	
	@PersistenceContext private EntityManager entityManager;
	
	@Override
	public boolean relacionarClienteNegocio(Long idNegocio, BigDecimal idCliente) {
		NegocioCliente negocioCliente = new NegocioCliente();
		negocioCliente.setIdCliente(idCliente);
		Negocio negocio = new Negocio();
		negocio.setIdToNegocio(idNegocio);
		negocioCliente.setNegocio(negocio);
		try{
			negocioClienteDao.persist(negocioCliente);
		}catch(RuntimeException e){
			return false;
		}
		return true;
	}

	@Override
	public List<NegocioCliente> listarPorNegocio(Long idNegocio) {
		
		List<NegocioCliente> listaClientes = negocioClienteDao.findByProperty(
				"negocio.idToNegocio", idNegocio);		
		
		for (NegocioCliente neg : listaClientes) {
			ClienteDTO clienteDTO = new ClienteDTO();
			ClienteGenericoDTO filtro = new ClienteGenericoDTO();
			filtro.setIdCliente(neg.getIdCliente());
			try {
				filtro = clienteFacadeRemote.loadByIdNoAddress(filtro);
			} catch (Exception e) {
				return null;
			}
			if (filtro != null) {
				clienteDTO.setNombre(filtro.getNombreCompleto());
				clienteDTO.setCodigoRFC(filtro.getCodigoRFC());
				clienteDTO.setIdCliente(filtro.getIdCliente());
			}
			try {
				neg.setClienteDTO(clienteDTO);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
		return (listaClientes == null)?new ArrayList<NegocioCliente>():listaClientes;
	}

	@Override
	public BigDecimal desasociarClienteNegocio(Long idNegocio, BigDecimal idCliente) {
		NegocioCliente negocioCliente = new NegocioCliente();
		negocioCliente.setIdCliente(idCliente);
		Negocio negocio = new Negocio();
		negocio.setIdToNegocio(idNegocio);
		negocioCliente.setNegocio(negocio);
		
		List<NegocioCliente> listaNegocioCliente= negocioClienteDao.findByFilters(negocioCliente);
		try{
			for(NegocioCliente negCte : listaNegocioCliente){
				negocioClienteDao.remove(negCte);
			}
		}catch(RuntimeException e){
			return listaNegocioCliente.get(0).getClienteDTO().getIdCliente();
		}
		return null;
				
	}

	@Override
	public void desasociarClientesNegocio(Long idNegocio) {
		NegocioCliente negocioCliente = new NegocioCliente();
		
		Negocio negocio = new Negocio();
		negocio.setIdToNegocio(idNegocio);
		negocioCliente.setNegocio(negocio);
		
		List<NegocioCliente> listaNegocioCliente= negocioClienteDao.findByFilters(negocioCliente);
		
		for(NegocioCliente negCte : listaNegocioCliente){
			negocioClienteDao.remove(negCte);
		}
	}

	@Override
	public Long eliminarAsociacionesClientesYGruposNegocio(Long idNegocio) {
		desasociarClientesNegocio(idNegocio);
		try{
			negocioGrupoClienteService.desasociarGruposClienteNegocio(idNegocio);
		}catch(RuntimeException e){
			return idNegocio;
		}
		return null;
	}
	
	public ClienteDTO getClientePorId(BigDecimal idCliente,String nombreUsuario){
		ClienteDTO clienteDTO = new ClienteDTO();
		try {
			clienteDTO = clienteFacadeRemote.findById(idCliente, nombreUsuario);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return clienteDTO;		
	}
	
	public ClienteJPA getClienteJPAPorIdCliente(Long idCliente) {
		ClienteJPA cliente = new ClienteJPA();
		cliente.setIdCliente(idCliente);
		List<ClienteJPA> clienteJPAs = null;
		try{
			clienteJPAs = getClienteJPAService().findByFilters(cliente); 
		}catch (Exception e){
			e.printStackTrace();
		}
		if (clienteJPAs != null && !clienteJPAs.isEmpty()) {
			return clienteJPAs.get(0);
		}
		return null;
	}
	public ClienteJPAService getClienteJPAService() {
		return clienteJPAService;
	}
	
	public void setClienteJPAService(ClienteJPAService clienteJPAService) {
		this.clienteJPAService = clienteJPAService;
	}

}
