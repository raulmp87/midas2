package mx.com.afirme.midas2.service.impl.negocio.antecedentes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.domain.negocio.antecedentes.NegocioCEAAnexos;
import mx.com.afirme.midas2.domain.negocio.antecedentes.NegocioCEAComentarios;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.antecedentes.NegocioAntecedentesService;

@Stateless 
public class NegocioAntecedentesServiceImpl implements NegocioAntecedentesService {
	
	private EntidadService entidadService;
	
	@Override
	public void saveAnexo (NegocioCEAAnexos negocioCEAAnexos){
		entidadService.save(negocioCEAAnexos);
	}
	
	@Override
	public Long updateAnexo(String accion, NegocioCEAAnexos negocioCEAAnexos) {
		try {			
			NegocioCEAAnexos expedienteAnexoAct = new NegocioCEAAnexos();
			expedienteAnexoAct = findAnexoById(negocioCEAAnexos.getId());
			expedienteAnexoAct.setTexto(negocioCEAAnexos.getTexto());
			entidadService.executeActionGrid(accion, expedienteAnexoAct);
			return expedienteAnexoAct.getId();
		} catch(RuntimeException e){
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void saveComentario(NegocioCEAComentarios negocioCEAComentarios) {
		entidadService.save(negocioCEAComentarios);
	}
	
	@Override
	public List<NegocioCEAAnexos> obtenerAntecedentesAnexos(Long idToNegocio) {
		List<NegocioCEAAnexos> negocioAntesAnexos = new ArrayList<NegocioCEAAnexos>();
		Map<String, Object> parameters = new HashMap<String, Object>();	
		StringBuilder queryString = new StringBuilder("SELECT model FROM " + NegocioCEAAnexos.class.getSimpleName() + " model ");
		queryString.append(" WHERE model.idToNegocio = :idToNegocio");
		parameters.put("idToNegocio", idToNegocio);
		queryString.append(" ORDER BY model.id");
		negocioAntesAnexos = (List<NegocioCEAAnexos>) entidadService.executeQueryMultipleResult(queryString.toString(), parameters);
		return negocioAntesAnexos;
	}
	
	@Override
	public List<NegocioCEAComentarios> obtenerAntecedentesComentarios(Long idToNegocio) {
		List<NegocioCEAComentarios> negocioAntesComentarios = new ArrayList<NegocioCEAComentarios>();
		Map<String, Object> parameters = new HashMap<String, Object>();	
		StringBuilder queryString = new StringBuilder("SELECT model from " + NegocioCEAComentarios.class.getSimpleName() + " model ");
		queryString.append(" WHERE model.idToNegocio = :idToNegocio");
		parameters.put("idToNegocio", idToNegocio);
		queryString.append(" ORDER BY model.id");
		negocioAntesComentarios = (List<NegocioCEAComentarios>) entidadService.executeQueryMultipleResult(queryString.toString(), parameters);
		return negocioAntesComentarios;
	}

	@Override
	public NegocioCEAAnexos findAnexoById(Long id){
		NegocioCEAAnexos expedienteAnexo = new NegocioCEAAnexos();
		expedienteAnexo = entidadService.findById(NegocioCEAAnexos.class, id);
		return expedienteAnexo;
	}
	
	@Override
	public List<NegocioCEAAnexos> findAnexosByFmxDocName( String fmxDocName){
		List<NegocioCEAAnexos> negocioAntesAnexos = new ArrayList<NegocioCEAAnexos>();
		negocioAntesAnexos = entidadService.findByProperty(NegocioCEAAnexos.class, "fortimaxDocNombre", fmxDocName);
		return negocioAntesAnexos;
	}
	
	@Override
	public List<NegocioCEAAnexos> findAnexosByIdToNegocio(Long idToNegocio){
		List<NegocioCEAAnexos> negocioAntesAnexos = new ArrayList<NegocioCEAAnexos>();
		negocioAntesAnexos = entidadService.findByProperty(NegocioCEAAnexos.class, "idToNegocio", idToNegocio);
		return negocioAntesAnexos;
	}
	
	@EJB
	public void setCatalogoService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
}
