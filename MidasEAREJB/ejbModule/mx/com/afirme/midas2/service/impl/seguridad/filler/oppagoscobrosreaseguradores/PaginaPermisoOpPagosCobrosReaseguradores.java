/**
 * Clase que llena Paginas y Permisos para el rol de Cabinero
 */
package mx.com.afirme.midas2.service.impl.seguridad.filler.oppagoscobrosreaseguradores;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

/**
 * @author andres.avalos
 *
 */
public class PaginaPermisoOpPagosCobrosReaseguradores {

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	private final int AG = 0;
	private final int AC = 1;
	private final int BR = 2;
	private final int RE = 3;
	private final int EX = 4;
	private final int AD = 5;
	private final int VD = 6;
	private final int AS = 7;
	private final int BU = 8;
	private final int CO = 9;
	private final int CT = 10;
	private final int GU = 11;
	private final int NV = 12;
	private final int SE = 14;
	
	public PaginaPermisoOpPagosCobrosReaseguradores(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
	
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		PaginaPermiso pp;
		
		//Paginas default
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("inicio.jsp", "/MidasWeb/sistema/inicio.do"));  //Pagina bienvenida
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cargaMenu.do"));  //Menu MIDAS
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/subRamo.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/estado.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/ciudad.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguradorCorredor.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		/* Cat�logos Reaseguro */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/contacto/listar.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/contacto/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/contacto/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);


		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/contacto/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/contacto/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/contacto/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/contacto/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/contacto/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/catalogos/contacto/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/listar.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/listar.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		/* Fin Cat�logos Reaseguro */
		
		/* Reaseguro */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/llenaComboSuscripcion.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/relacionarIngreso.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarRegistrarIngresos.jsp","/MidasWeb/contratos/ingresos/mostrarRegistrarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/registrarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/listarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/agregarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/borrarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/modificarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/administrarEgresos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/mostrarRegistrarEgreso.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/mostrarPolizas.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/obtenerEstadosCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/mostrarEstadosCuentaAdministrarEgresos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/mostrarEgresosPendientes.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/egresoCancel.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/mostrarEgreso.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/validarRelacionEgresoEdosCta.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/borraRelacionEgresoEdosCta.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/agregarEgreso.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/listarEstadosCuentaRelacionarEgreso.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/confirmarPago.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/obtenerOrdenesPago.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/rptOrdenPagoReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/obtenerReporteSoporteOrdenPago.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("administrarIngresos.jsp","/MidasWeb/reaseguro/ingresos/administrarIngresos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/validarRelacionIngresoEdosCta.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarIngreso.jsp","/MidasWeb/reaseguro/ingresos/mostrarIngreso.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/listarEstadosCuentaRelacionarIngreso.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWebreaseguro/ingresos/relacionarIngreso.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/mostrarPolizas.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/obtenerEstadosCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/mostrarEstadosCuentaAdministrarIngresos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/mostrarIngresosPendientes.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("configuracionMovimientos.jsp","/MidasWeb/reaseguro/configuracionmovimientos/listar.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/buscarPoliza.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		listaPaginaPermiso.add(pp);

		/* Fin Reaseguro */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/registrarMovimiento.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);
		
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionfacultativa/copiarParticipacionesCoberturaEndosoAnterior.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptSinReservaPendienteAcum.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptSinReservaPendienteAcumDet.do"));
	    listaPaginaPermiso.add(pp);
	    
	    
	    //Permisos para egresos
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/listarEstadosCuentaAutomaticosEgreso.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/listarEstadosCuentaFacultativosEgreso.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/listarCoberturasEstadosCuentaEgreso.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/listarExhibicionesCoberturasEstadosCuentaEgreso.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/cargarComboSecciones.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/cargarComboCoberturas.do"));
	    listaPaginaPermiso.add(pp);	    
	    
		return this.listaPaginaPermiso;
	}
	
	
	
}
