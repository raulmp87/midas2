package mx.com.afirme.midas2.service.impl.operacionessapamis.sapamisbitacoras;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasCesviDao;
import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasCiiDao;
import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasEmisionDao;
import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasOcraDao;
import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasPrevencionDao;
import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasPtDao;
import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasScdDao;
import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasSiniestroDao;
import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasSipacDao;
import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasSistemasEnvioDao;
import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasValuacionDao;
import mx.com.afirme.midas2.dao.operacionessapamis.SapAmisBitacoraEmisionDao;
import mx.com.afirme.midas2.dao.operacionessapamis.SapAmisBitacoraSiniestroDao;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasCesvi;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasCii;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasEmision;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasOcra;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasPrevencion;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasPt;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasScd;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasSiniestro;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasSipac;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasValuacion;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasistemasEnvio;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraEmision;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraSiniestros;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisControlEmision;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisControlSiniestros;
import mx.com.afirme.midas2.service.operacionessapamis.sapamisbitacoras.SapAmisBitacoraService;

@Stateless
public class SapAmisBitacoraServiceImpl implements SapAmisBitacoraService {

	SapAmisBitacoraEmisionDao sapAmisBitacoraEmisionDao;
	SapAlertasSistemasEnvioDao sapAlertasSistemasEnvioDao;
	SapAmisBitacoraSiniestroDao sapAmisBitacoraSiniestroDao;

	private SapAlertasCiiDao sapAlertasCiiDao;
	private SapAlertasEmisionDao sapAlertasEmisionDao;
	private SapAlertasOcraDao sapAlertasOcraDao;
	private SapAlertasPrevencionDao sapAlertasPrevencionDao;
	private SapAlertasPtDao sapAlertasPtDao;
	private SapAlertasScdDao sapAlertasScdDao;
	private SapAlertasSiniestroDao sapAlertasSiniestroDao;
	private SapAlertasSipacDao sapAlertasSipacDao;
	private SapAlertasValuacionDao sapAlertasValuacionDao;
	private SapAlertasCesviDao sapAlertasCesviDao;

	@Override
	public List<SapAmisBitacoraEmision> obtenerBitacoraEmisionFiltrada(String bitacoraPoliza, String bitacoraVin, String bitacoraFechaEnvio, String estatusEnvio, String cesvi, String cii, String emision, String ocra, String prevencion, String pt, String csd, String siniestro, String sipac, String valuacion) {
		return sapAmisBitacoraEmisionDao.obtenerBitacoraEmisionFiltrada(bitacoraPoliza, bitacoraVin, bitacoraFechaEnvio, estatusEnvio, cesvi, cii, emision, ocra, prevencion, pt, csd, siniestro, sipac, valuacion);
	}

	public List<SapAmisBitacoraSiniestros> obtenerBitacoraSiniestrosFiltrada(String bitacoraPoliza, String bitacoraVin, String bitacoraFechaEnvio, String estatusEnvio, String cesvi, String cii, String emision, String ocra, String prevencion, String pt, String csd, String siniestro, String sipac, String valuacion) {
		return sapAmisBitacoraSiniestroDao.obtenerBitacoraSiniestrosFiltrada(bitacoraPoliza, bitacoraVin, bitacoraFechaEnvio, estatusEnvio, cesvi, cii, emision, ocra, prevencion, pt, csd, siniestro, sipac, valuacion);
	}

	public SapAlertasistemasEnvio obtenerAlertas(String idEncabezadoAlerta) {
		return sapAlertasSistemasEnvioDao.obtenerAlertas(idEncabezadoAlerta);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardarSiniestro(SapAlertasistemasEnvio alertasEncabezado, ArrayList<SapAlertasCii> listaAlertasCii, ArrayList<SapAlertasEmision> listaAlertasEmision, ArrayList<SapAlertasOcra> listaAlertasOcra, ArrayList<SapAlertasPrevencion> listaAlertasPrevencion, ArrayList<SapAlertasPt> listaAlertasPt, ArrayList<SapAlertasScd> listaAlertasScd, ArrayList<SapAlertasSiniestro> listaAlertasSiniestro, ArrayList<SapAlertasSipac> listaAlertasSipac, ArrayList<SapAlertasValuacion> listaAlertasValuacion, ArrayList<SapAlertasCesvi> listaAlertasCesvi, SapAmisBitacoraSiniestros siniestroBitacora, SapAmisControlSiniestros siniestro) {
		
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardarEmision(SapAlertasistemasEnvio alertasEncabezado, ArrayList<SapAlertasCii> listaAlertasCii, ArrayList<SapAlertasEmision> listaAlertasEmision, ArrayList<SapAlertasOcra> listaAlertasOcra, ArrayList<SapAlertasPrevencion> listaAlertasPrevencion, ArrayList<SapAlertasPt> listaAlertasPt, ArrayList<SapAlertasScd> listaAlertasScd, ArrayList<SapAlertasSiniestro> listaAlertasSiniestro, ArrayList<SapAlertasSipac> listaAlertasSipac, ArrayList<SapAlertasValuacion> listaAlertasValuacion, ArrayList<SapAlertasCesvi> listaAlertasCesvi, SapAmisBitacoraEmision emisionBitacora, SapAmisControlEmision emision) {

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	private void guargarAlertas(ArrayList<SapAlertasCii> listaAlertasCii, ArrayList<SapAlertasEmision> listaAlertasEmision, ArrayList<SapAlertasOcra> listaAlertasOcra, ArrayList<SapAlertasPrevencion> listaAlertasPrevencion, ArrayList<SapAlertasPt> listaAlertasPt, ArrayList<SapAlertasScd> listaAlertasScd, ArrayList<SapAlertasSiniestro> listaAlertasSiniestro, ArrayList<SapAlertasSipac> listaAlertasSipac, ArrayList<SapAlertasValuacion> listaAlertasValuacion, ArrayList<SapAlertasCesvi> listaAlertasCesvi) {
		sapAlertasCiiDao.guardarAlertasCii(listaAlertasCii);
		sapAlertasEmisionDao.guardarAlertasEmision(listaAlertasEmision);
		sapAlertasOcraDao.guardarAlertasOcra(listaAlertasOcra);
		sapAlertasPrevencionDao.guardarAlertasPrevencion(listaAlertasPrevencion);
		sapAlertasPtDao.guardarAlertasPt(listaAlertasPt);
		sapAlertasScdDao.guardarAlertasScd(listaAlertasScd);
		sapAlertasSiniestroDao.guardarAlertasSiniestros(listaAlertasSiniestro);
		sapAlertasSipacDao.guardarAlertasSipac(listaAlertasSipac);
		sapAlertasValuacionDao.guardarAlertasValuacion(listaAlertasValuacion);
		sapAlertasCesviDao.guardarAlertasCesvi(listaAlertasCesvi);
	}

	@EJB
	public void setSapAmisBitacoraEmisionDao(SapAmisBitacoraEmisionDao sapAmisBitacoraEmisionDao) {
		this.sapAmisBitacoraEmisionDao = sapAmisBitacoraEmisionDao;
	}

	@EJB
	public void setSapAlertasSistemasEnvioDao(SapAlertasSistemasEnvioDao sapAlertasSistemasEnvioDao) {
		this.sapAlertasSistemasEnvioDao = sapAlertasSistemasEnvioDao;
	}

	@EJB
	public void setSapAmisBitacoraSiniestroDao(SapAmisBitacoraSiniestroDao sapAmisBitacoraSiniestroDao) {
		this.sapAmisBitacoraSiniestroDao = sapAmisBitacoraSiniestroDao;
	}

	@EJB
	public void setSapAlertasCiiDao(SapAlertasCiiDao sapAlertasCiiDao) {
		this.sapAlertasCiiDao = sapAlertasCiiDao;
	}

	@EJB
	public void setSapAlertasEmisionDao(SapAlertasEmisionDao sapAlertasEmisionDao) {
		this.sapAlertasEmisionDao = sapAlertasEmisionDao;
	}

	@EJB
	public void setSapAlertasOcraDao(SapAlertasOcraDao sapAlertasOcraDao) {
		this.sapAlertasOcraDao = sapAlertasOcraDao;
	}

	@EJB
	public void setSapAlertasPrevencionDao(SapAlertasPrevencionDao sapAlertasPrevencionDao) {
		this.sapAlertasPrevencionDao = sapAlertasPrevencionDao;
	}

	@EJB
	public void setSapAlertasPtDao(SapAlertasPtDao sapAlertasPtDao) {
		this.sapAlertasPtDao = sapAlertasPtDao;
	}

	@EJB
	public void setSapAlertasScdDao(SapAlertasScdDao sapAlertasScdDao) {
		this.sapAlertasScdDao = sapAlertasScdDao;
	}

	@EJB
	public void setSapAlertasSiniestroDao(SapAlertasSiniestroDao sapAlertasSiniestroDao) {
		this.sapAlertasSiniestroDao = sapAlertasSiniestroDao;
	}

	@EJB
	public void setSapAlertasSipacDao(SapAlertasSipacDao sapAlertasSipacDao) {
		this.sapAlertasSipacDao = sapAlertasSipacDao;
	}

	@EJB
	public void setSapAlertasValuacionDao(SapAlertasValuacionDao sapAlertasValuacionDao) {
		this.sapAlertasValuacionDao = sapAlertasValuacionDao;
	}

	@EJB
	public void setSapAlertasCesviDao(SapAlertasCesviDao sapAlertasCesviDao) {
		this.sapAlertasCesviDao = sapAlertasCesviDao;
	}
}
