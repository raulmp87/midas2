package mx.com.afirme.midas2.service.impl.condicionesGenerales;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.reporte.MidasBaseReporte;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.condicionesGenerales.CgAgenteDao;
import mx.com.afirme.midas2.dao.condicionesGenerales.CgCentroDao;
import mx.com.afirme.midas2.dao.condicionesGenerales.CgOrdenDao;
import mx.com.afirme.midas2.dao.condicionesGenerales.CgProveedorDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgAgente;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgAgenteView;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgCentro;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgOrden;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgProveedor;
import mx.com.afirme.midas2.service.ExcelRowMapper;
import mx.com.afirme.midas2.service.condicionesGenerales.CondicionesGeneralesService;
import mx.com.afirme.midas2.service.condicionesGenerales.ProcesarCargaAgente;
import mx.com.afirme.midas2.service.impl.ExcelConverter;
import mx.com.afirme.midas2.service.impl.ExcelConverter.ExcelConfig.ExcelConfigBuilder;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.commons.dbutils.DbUtils;

@Stateless 
public class CondicionesGeneralesServiceImpl implements CondicionesGeneralesService{

	private CgProveedorDao cgProveedorDao;
	private CgAgenteDao cgAgenteDao;
	private CgCentroDao cgCentroDao;
	private CgOrdenDao cgOrdenDao;
	private EntidadDao entidadDao;
	private ProcesarCargaAgente procesarCargaAgente;
	private static final String SALTO_LINEA = "<br>";
	private static final String CON_ERROR = "Total con error: ";
	private static final String EXITOSOS = "Total exitosos: ";
	private static final String PROCESADOS = "Total procesados: ";
	private static final String NO_PROCESADOS = "No se procesaron registros.";
	private static final String ERROR = "Registro con error: ";
	private static final String A_PETICION = "PET";
	@SuppressWarnings("unused")
	private static final String AUTOMATICA = "AUT";
	private static final Long DEFAULT_ALERTA = 0L;
	private static final Long DEFAULT_ESTATUS = 0L;
	private static final Long RESET_VENTAS = 0L;
	private static final Long ESTATUS_CONFIRMAR = 1L;
	private static final Long VALOR_UNO = 1L;
	private static final Long VALOR_CERO = 0L;
	public static final String PATH_PLANTILLA = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/";
	
	
	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	public void setCgProveedorDao(CgProveedorDao cgProveedorDao) {
		this.cgProveedorDao = cgProveedorDao;
	}
	
	@EJB
	public void setCgAgenteDao(CgAgenteDao cgAgenteDao) {
		this.cgAgenteDao = cgAgenteDao;
	}

	@EJB
	public void setCgCentroDao(CgCentroDao cgCentroDao) {
		this.cgCentroDao = cgCentroDao;
	}

	@EJB
	public void setCgOrdenDao(CgOrdenDao cgOrdenDao) {
		this.cgOrdenDao = cgOrdenDao;
	}

	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}
	
	@EJB
	public void setProcesarCargaAgente(ProcesarCargaAgente procesarCargaAgente) {
		this.procesarCargaAgente = procesarCargaAgente;
	}

	@Override
	public List<CgProveedor> traerListaProveedor( CgProveedor cgProveedor ) throws Exception {

		List<CgProveedor> listCgProveedor = new ArrayList<CgProveedor>();		

			if ( this.isByFiltro(cgProveedor)){				
				listCgProveedor = cgProveedorDao.findByName( cgProveedor.getNombrePrestador().trim() );
			}
			else{				
				listCgProveedor = cgProveedorDao.findAll();				
			}

		return listCgProveedor;
	}

	@Override
	public void guardarActualizarProveedor( CgProveedor cgProveedor, String usuario ) throws Exception {	
		

			if ( cgProveedor.getId() ==  null || ( cgProveedor.getId()!= null && cgProveedor.getId() < 1 )){
				
				CgProveedor cgProveedorNew = this.poblarCgProveedor(cgProveedor, usuario, true );
				cgProveedorDao.persist(cgProveedorNew);
			}
			else{
				CgProveedor cgProveedorUpd = this.poblarCgProveedor(cgProveedor, usuario, false );
				cgProveedorDao.update( cgProveedorUpd );
			}
	}
	
	private CgProveedor poblarCgProveedor( CgProveedor cgProveedor, String usuario, boolean isCreate ) throws Exception {
		
		CgProveedor cgProveedorResult = new CgProveedor();
		
		if ( isCreate ){
			cgProveedorResult = this.poblarCgProveedorCrear( cgProveedor, usuario );
		}
		else{
			cgProveedorResult = this.poblarCgProveedorEditar( cgProveedor, usuario );			
		}
		
		return cgProveedorResult;
		
		
	}
	
	private CgProveedor poblarCgProveedorCrear( CgProveedor cgProveedor, String usuario ) throws Exception {
		
		//cgProveedor.setTcprestadorservicio_id( cgProveedor.getTcprestadorservicio_id());
		
		cgProveedor.setFechaCreacion(new Date());
		cgProveedor.setUserId(usuario);
		
		return cgProveedor;
		
	}
	
	private CgProveedor poblarCgProveedorEditar( CgProveedor cgProveedor, String usuario ) throws Exception {
		
		CgProveedor cgProveedorEditar = this.traerProveedor(cgProveedor.getId());
		
		cgProveedorEditar.setContacto(cgProveedor.getContacto());
		cgProveedorEditar.setObservaciones(cgProveedor.getObservaciones());
		cgProveedorEditar.setFechaModificacion(new Date());
		cgProveedorEditar.setUserModifId(usuario);
		
		return cgProveedorEditar;
		
	}

	private boolean isByFiltro( CgProveedor cgProveedor ) throws Exception {
		
		String nombrePrestador;		
		nombrePrestador = cgProveedor.getNombrePrestador();		
		return ( nombrePrestador != null && !nombrePrestador.trim().equals("") );
				
	}

	@Override
	public CgProveedor traerProveedor(Long id) throws Exception {
		
		CgProveedor cgProveedor = cgProveedorDao.findById(id);
		//cgProveedor.setCentrosProveedor( this.traeCentrosProveedor(cgProveedor.getCentros()) );
		return cgProveedor;
	}
	
	@Override
	public List<CgCentro> traeCentrosProveedor( String centrosStr ){
		
		List<CgCentro> listResult = new ArrayList<CgCentro>();
		
		if ( centrosStr != null ){			
			String[] centrosArray = centrosStr.split(",");
			for ( String item : centrosArray ){			
				CgCentro cgCentro = cgCentroDao.findById( Long.parseLong(item) );
				listResult.add( cgCentro );
			}
		}
		
		return listResult;		
	}
	
	@Override
	public void eliminarProveedor(CgProveedor cgProveedor) throws Exception {
		
		cgProveedorDao.remove(cgProveedor);
		
	}
	
	@Override
	public void eliminarCentroProveedor( CgProveedor cgProveedor, String idToCentro ) throws Exception{
		
		CgProveedor cgProveedorUpd = this.obtenerCentros( cgProveedor, idToCentro );
		cgProveedorDao.update( cgProveedorUpd );
	}
	
	private CgProveedor obtenerCentros( CgProveedor cgProveedor, String idToCentro ) throws Exception{
		if ( cgProveedor.getCentros() != null ){
			String[] centros = cgProveedor.getCentros().split(",");
			cgProveedor.setCentros(obtenerCentrosNuevos(centros, idToCentro));		
		}
		return cgProveedor;		
	}
	
	public String obtenerCentrosNuevos(String[] centros, String idToCentro) {
		StringBuilder centrosNuevo = new StringBuilder("");
		boolean isFirst = true;
		for (String item : centros ){
			if ( !item.equals(idToCentro) ){
				if(isFirst){
					isFirst = false;
				}else{
				centrosNuevo.append(",");	
				}
				centrosNuevo.append(item);
			}
		}
		return centrosNuevo.toString();
	}
	
	@Override
	public void guardarCentroProveedor( CgProveedor cgProveedor, String idToCentro ) throws Exception{
		
		CgProveedor cgProveedorUpd = this.agregaCentroProveedor( cgProveedor, idToCentro );
		cgProveedorDao.update( cgProveedorUpd );
	}
	
	private CgProveedor agregaCentroProveedor( CgProveedor cgProveedor, String idToCentro ) throws Exception{
		
		String centrosStr = cgProveedor.getCentros();
		
		if ( centrosStr == null || ( centrosStr != null && centrosStr.equals("") ) ){
			
			cgProveedor.setCentros(idToCentro);
		}
		else{
			
			if ( !centrosStr.contains(idToCentro) ){
				centrosStr = centrosStr + "," + idToCentro;				
			}
			cgProveedor.setCentros( centrosStr );
		}
		
		return cgProveedor;
		
	}
	
	@Override
	public List<CgAgenteView> traerListaAgente( CgAgente cgAgente ) throws Exception {

		List<CgAgenteView> listAgenteView = new ArrayList<CgAgenteView>();		

		listAgenteView = cgAgenteDao.findByFilter( cgAgente );

		return listAgenteView;
	}

	@Override
	public CgAgente traerAgente(Long id) throws Exception {
		
		CgAgente cgAgente = cgAgenteDao.findById( id );
		return cgAgente;
	}

	@Override
	public CgAgenteView traerCgAgenteView( Long id ) throws Exception {
		
		CgAgenteView cgAgenteView = cgAgenteDao.findCgAgenteViewById( id );
		return cgAgenteView;
	}
	
	@Override
	public void actualizarAgente( CgAgente cgAgente, String usuario ) throws Exception {		
		
		CgAgente cgAgenteUpd = this.poblarCgAgenteEditar( cgAgente, usuario );
		cgAgenteDao.update( cgAgenteUpd );
		
	}
	
	private CgAgente poblarCgAgenteEditar( CgAgente cgAgente, String usuario ) throws Exception {
		
		CgAgente cgAgenteEditar = this.traerAgente( cgAgente.getId() );
		
		cgAgenteEditar.setCalleNumero( cgAgente.getCalleNumero() );
		cgAgenteEditar.setCodigoPostal( cgAgente.getCodigoPostal() );
		cgAgenteEditar.setColonia( cgAgente.getColonia() );
		cgAgenteEditar.setCveCiudad( cgAgente.getCveCiudad() );
		cgAgenteEditar.setCveEstado( cgAgente.getCveEstado() );
		cgAgenteEditar.setCvePais( cgAgente.getCvePais() );
		cgAgenteEditar.setDiasEntrega( cgAgente.getDiasEntrega() );
		cgAgenteEditar.setFactorSurtir( cgAgente.getFactorSurtir() );
		cgAgenteEditar.setInventario( cgAgente.getInventario() );
		cgAgenteEditar.setObservaciones( cgAgente.getObservaciones() );
		cgAgenteEditar.setPersonaRecibe( cgAgente.getPersonaRecibe() );
		cgAgenteEditar.setTelefonoRecibe( cgAgente.getTelefonoRecibe() );
		cgAgenteEditar.setPromedioMensual( cgAgente.getPromedioMensual() );
		cgAgenteEditar.setSurtidoEjecutivo( cgAgente.getSurtidoEjecutivo() );
		cgAgenteEditar.setPorcentajeSurtir( cgAgente.getPorcentajeSurtir() );
		cgAgenteEditar.setVentas( cgAgente.getVentas() );
		cgAgenteEditar.setFechaModificacion( new Date() );
		cgAgenteEditar.setUserModifId( usuario );

		return cgAgenteEditar;
		
	}

	@Override
	public List<CgCentro> traerListaCentro( CgCentro cgCentro ) throws Exception {
		
		List<CgCentro> listCgCentro = new ArrayList<CgCentro>();
		
		listCgCentro = cgCentroDao.findByFilter( cgCentro );
		
		return listCgCentro;
	}

	@Override
	public CgCentro traerCentro( Long id ) throws Exception {

		CgCentro cgCentro = new CgCentro();
		cgCentro = cgCentroDao.findById( id );
		
		return cgCentro;
	}

	@Override
	public void actualizarCentro( CgCentro cgCentro, String usuario ) throws Exception {
		
		CgCentro cgCentroUpd = this.poblarCgCentroEditar( cgCentro, usuario, false );
		cgCentroDao.update( cgCentroUpd );
		
	}

	@Override
	public void guardarCentro( CgCentro cgCentro, String usuario ) throws Exception {
		
		CgCentro cgCentroUpd = this.poblarCgCentroEditar( cgCentro, usuario, true );
		cgCentroDao.persist( cgCentroUpd );
		
	}
	
	private CgCentro poblarCgCentroEditar( CgCentro cgCentro, String usuario, boolean isCreate ) throws Exception {
		
		CgCentro cgCentroEditar = new CgCentro();
		if ( !isCreate ){
			cgCentroEditar = this.traerCentro( cgCentro.getId() );
		}
		
		cgCentroEditar.setCalleNumero( cgCentro.getCalleNumero() );
		cgCentroEditar.setCodigoPostal( cgCentro.getCodigoPostal() );
		cgCentroEditar.setColonia( cgCentro.getColonia() );
		cgCentroEditar.setCveCiudad( cgCentro.getCveCiudad() );
		cgCentroEditar.setCveEstado( cgCentro.getCveEstado() );
		cgCentroEditar.setCvePais( cgCentro.getCvePais() );
		cgCentroEditar.setInventario( cgCentro.getInventario() );
		cgCentroEditar.setObservaciones( cgCentro.getObservaciones() );
		cgCentroEditar.setPersonaRecibe( cgCentro.getPersonaRecibe() );
		cgCentroEditar.setTelefonoRecibe( cgCentro.getTelefonoRecibe() );
		cgCentroEditar.setPromedioSurtir( cgCentro.getPromedioSurtir() );
		cgCentroEditar.setSobrado( cgCentro.getSobrado() );
		cgCentroEditar.setVentas( cgCentro.getVentas() );
		
		
		if ( isCreate ){
			
			cgCentroEditar.setId( cgCentro.getId() );
			cgCentroEditar.setFechaCreacion( new Date() );
			cgCentroEditar.setUserId( usuario );
			cgCentroEditar.setEstatus( VALOR_UNO );
		}
		else{
			
			cgCentroEditar.setFechaModificacion( new Date() );
			cgCentroEditar.setUserModifId( usuario );
		}

		return cgCentroEditar;
		
	}


	@Override
	public List<CgOrden> traerListaOrden( CgOrden cgOrden, String usuario ) throws Exception {
		
		List<CgOrden> listCgOrden = new ArrayList<CgOrden>();
		if ( !usuario.equals("") ){
			listCgOrden = cgOrdenDao.findByFilterQuery( usuario);
		}
		else{
			listCgOrden = cgOrdenDao.findByFilter( cgOrden );
		}
		
		return listCgOrden;
	}
	
	@Override
	public CgOrden traerOrden( Long id ) throws Exception{
		
		CgOrden cgOrden = new CgOrden();
		cgOrden = cgOrdenDao.findById( id );
		return cgOrden;
		
	}
	
	@Override
	public void confirmarOrden( Long id, String usuario ) throws Exception {
		
		CgOrden cgOrden = cgOrdenDao.findById( id );
		
		if ( cgOrden.getTocgagenteId() != null && cgOrden.getTocgagenteId() > 0 ){
			this.reseteaVentasAgente( cgOrden, usuario );			
		}
		else{
			this.reseteaVentasCentro( cgOrden, usuario );
		}
		
		this.confirmaOrden( cgOrden, usuario );		
	}
	
	private void confirmaOrden( CgOrden cgOrden,  String usuario ) throws Exception {
		
		cgOrden.setEstatus( ESTATUS_CONFIRMAR );
		cgOrden.setUserModifId( usuario );
		cgOrden.setFechaModificacion( new Date() );
		cgOrdenDao.update( cgOrden );
		
	}
	
	private void reseteaVentasAgente( CgOrden cgOrden,  String usuario ) throws Exception {
		
		CgAgente cgAgente = cgAgenteDao.findById( cgOrden.getTocgagenteId() );
		cgAgente.setFechaUltimo( new Date() );
		cgAgente.setCantidadUltimo( cgOrden.getCantidad() );
		Long inventarioActual = cgAgente.getInventario() - cgAgente.getVentas();
		cgAgente.setInventario( inventarioActual + cgOrden.getCantidad() );
		cgAgente.setVentas( RESET_VENTAS );
		cgAgente.setUserModifId( usuario );
		cgAgente.setFechaModificacion( new Date() );
		
		cgAgenteDao.update( cgAgente );
	}

	private void reseteaVentasCentro( CgOrden cgOrden,  String usuario ) throws Exception {
		
		CgCentro cgCentro = cgCentroDao.findById( cgOrden.getTocgcentroId() );
		cgCentro.setFechaUltimo( new Date() );
		Long inventarioActual = cgCentro.getInventario() - cgCentro.getVentas();
		cgCentro.setInventario( inventarioActual + cgOrden.getCantidad() );
		cgCentro.setVentas( RESET_VENTAS );
		cgCentro.setUserModifId(usuario);
		cgCentro.setFechaModificacion( new Date() );
		
		cgCentroDao.update( cgCentro );
	}	

	@Override
	public void actualizarOrden( CgOrden cgOrden, String usuario ) throws Exception {
		
		
	}

	@Override
	public void guardarOrden( CgOrden cgOrden, String usuario, boolean isAgente ) throws Exception{
		
		CgOrden cgOrdenNew = this.poblarCgOrden(cgOrden, usuario, isAgente);
		cgOrdenDao.persist( cgOrdenNew );
		
	}
	
	private CgOrden poblarCgOrden( CgOrden cgOrden, String usuario, boolean isAgente ) throws Exception{
		
		CgOrden cgOrdenGuardar = new CgOrden();
		
		if ( isAgente ){
			
			List<CgAgenteView> agenteList = new ArrayList<CgAgenteView>();
			CgAgente cgAgente = new CgAgente();
			Agente agente = new Agente();
			
			agente.setCodigoUsuario( usuario );
			cgAgente.setAgente( agente );
			agenteList = cgAgenteDao.findByFilter( cgAgente );
			
			if ( agenteList != null && agenteList.size() > 0 ){
				cgOrdenGuardar.setCantidad( cgOrden.getCantidad() );
				CgAgenteView cgAgenteView = agenteList.get(0);
				cgOrdenGuardar.setTocgagenteId( cgAgenteView.getId() );
				cgOrdenGuardar.setFecha( cgOrden.getFecha() );
				cgOrdenGuardar.setFechaEntrega( this.getFechaEntrega( cgOrden.getFecha(), cgAgenteView.getDiasEntrega() ) );
				cgOrdenGuardar.setMotivos( cgOrden.getMotivos() );
				cgOrdenGuardar.setTipo( A_PETICION );
				cgOrdenGuardar.setAlerta( DEFAULT_ALERTA );
				cgOrdenGuardar.setEstatus( DEFAULT_ESTATUS );
				cgOrdenGuardar.setFechaCreacion( new Date() );
				cgOrdenGuardar.setUserId( usuario );
				cgOrdenGuardar.setNombreCompleto( cgAgenteView.getNombreCompleto() );
				cgOrdenGuardar.setTocgcentroId( null );
			}
			
		}
		else{
			cgOrdenGuardar.setCantidad( cgOrden.getCantidad() );
			cgOrdenGuardar.setTocgcentroId( cgOrden.getTocgcentroId() );
			cgOrdenGuardar.setFecha( cgOrden.getFecha() );
			cgOrdenGuardar.setFechaEntrega( cgOrden.getFecha() );
			cgOrdenGuardar.setMotivos( cgOrden.getMotivos() );
			cgOrdenGuardar.setTipo( A_PETICION );
			cgOrdenGuardar.setAlerta( DEFAULT_ALERTA );
			cgOrdenGuardar.setEstatus( DEFAULT_ESTATUS );
			cgOrdenGuardar.setFechaCreacion( new Date() );
			cgOrdenGuardar.setUserId( usuario );
			
		}		
		
		return cgOrdenGuardar;
	}
	
	private Date getFechaEntrega( Date date, Long dias ) throws Exception{
		
		int diasInt = (int)(long)dias;
		Calendar c = Calendar.getInstance();
		c.setTime(date); 
		c.add(Calendar.DATE, diasInt);
		return c.getTime();
		
	}

	
	private String obtenerPathArchivo( BigDecimal idToControlArchivo ) throws Exception {
		
		ControlArchivoDTO controlArchivoDTO = entidadDao.findById(ControlArchivoDTO.class, idToControlArchivo);
		String pathArchivo = null; 
		String nombreOriginalArchivo = controlArchivoDTO.getNombreArchivoOriginal();	
		String extension = (nombreOriginalArchivo.lastIndexOf(".") == -1) ? "" : nombreOriginalArchivo
				.substring(nombreOriginalArchivo.lastIndexOf("."), nombreOriginalArchivo.length());
		pathArchivo = sistemaContext.getUploadFolder() + controlArchivoDTO.getIdToControlArchivo().toString() + extension;
		
		return pathArchivo;
	}
	
	@Override
	public List<String> cargarInformacion( BigDecimal idToControlArchivo, String usuario ) throws Exception  {

		List<CgAgente> list = this.cargarInformacionDesdeExcel(idToControlArchivo);
		List<String> listResultados = new ArrayList<String>();
		
		if( list!=null && list.size() > 0 ){
			listResultados = this.guardaInformacionDB( list, usuario );
		}
		else{
			listResultados.add(NO_PROCESADOS);
		}		
		return listResultados;

	}
	
	private List<CgAgente> cargarInformacionDesdeExcel( BigDecimal idToControlArchivo ) throws Exception{
		
		InputStream is = null;
		List<CgAgente> list = null;
		
		try {
			is = new FileInputStream( this.obtenerPathArchivo(idToControlArchivo) );
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());
			
			for (String sheetName : converter.getSheetNames()) {

				list = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<CgAgente>() {

							@Override
							public CgAgente mapRow(Map<String, String> row,
									int rowNum) {
								CgAgente cgAgente = new CgAgente();
								cgAgente = poblarAgente( row );
								return cgAgente;
							}
						});
			}

		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					throw e;
				}
			}
		}
		return list;
	}
	
	private CgAgente poblarAgente( Map<String, String> row ){
		CgAgente cgAgente = new CgAgente();
		
		cgAgente.setId( obtenerValorLong(row.get("agente_id")) );
		cgAgente.setCvePais(row.get("cve_pais") );
		cgAgente.setCveEstado(row.get("cve_estado") );
		cgAgente.setCveCiudad(row.get("cve_ciudad") );								
		cgAgente.setCalleNumero(row.get("calle_numero") );
		cgAgente.setColonia(row.get("colonia") );
		cgAgente.setCodigoPostal(row.get("codigo_postal") );
		cgAgente.setPersonaRecibe(row.get("persona_recibe") );
		cgAgente.setTelefonoRecibe(row.get("telefono_recibe") );
		cgAgente.setObservaciones(row.get("observaciones") );
		cgAgente.setInventario( obtenerValorLong(row.get("inventario")) );
		cgAgente.setVentas( obtenerValorLong(row.get("ventas")) );
		cgAgente.setPromedioMensual( obtenerValorLong(row.get("inventario")) );
		cgAgente.setFactorSurtir( obtenerValorLong(row.get("factor_surtir")) );		
		cgAgente.setPorcentajeSurtir( obtenerValorLong(row.get("porcentaje_surtir")) );
		cgAgente.setDiasEntrega( obtenerValorLong(row.get("dias_entrega")) );
		cgAgente.setSurtidoEjecutivo( obtenerValorLong(row.get("surtido_ejecutivo")) );
		cgAgente.setEstatus( obtenerValorLong(row.get("estatus")) );

		
		return cgAgente;
	}
	
	private Long obtenerValorLong( String valor ){
		
		try {
			if ( valor == null || ( valor != null && valor.trim().equals("") ))
				return VALOR_CERO;
			Long inventario = Long.parseLong( valor );
			return inventario;
		} catch (NumberFormatException e) {
			return VALOR_CERO;
		}		
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private List<String> guardaInformacionDB( List<CgAgente> list, String usuario ) throws SQLException, Exception{
		
		List<String> listResultados = new ArrayList<String>();
		int contador=0, correctos=0, error=0;
		
		for (CgAgente item : list) {

			item.setUserId( usuario );
			item.setUserModifId( usuario );
			item.setFechaCreacion( new Date() );
			item.setFechaModificacion( new Date() );
			contador++;
			
			try {
				procesarCargaAgente.guardarCgAgente( item );
				correctos++;
				
			}	
			catch( Exception e ){
				listResultados.add(ERROR + contador);	
				error++;		
			}
		}
		
		
		listResultados = this.sumarizaListResultados( listResultados, contador, correctos, error ); 
		return listResultados;
		
	}	
	
	private List<String> sumarizaListResultados( List<String> list, int contador, int correctos, int error ){
		
		list.add( 0, SALTO_LINEA );
		list.add( 0, CON_ERROR + error );
		list.add( 0, EXITOSOS + correctos );
		list.add( 0, PROCESADOS + contador );
				
		return list;
	}

	@Override
	public InputStream obtenerLayout() throws Exception {
		
		
		Connection connection = null;
		ByteArrayInputStream bis = null;
		
		try {			
			
			String plantilla = PATH_PLANTILLA + "plantillaCGAgente.jrxml";
			
			InputStream  input= this.getClass().getResourceAsStream(plantilla);
			JasperDesign design = JRXmlLoader.load(input);
			JasperReport reporteDatosInventario = JasperCompileManager.compileReport(design);
			
			
			GestorReportes ges = new GestorReportes();
			byte[] d = ges.generaReporte(ConstantesReporte.TIPO_XLS, reporteDatosInventario, 
					null, connection);
			
			bis = new ByteArrayInputStream(d);
		} catch (Exception e) {
			throw e;
		}
		finally{
			DbUtils.closeQuietly(connection);			
		}
        
        return bis;	
	}

	@Override
	public void eliminarAgente( Long id, String usuario ) throws Exception {

		CgAgente cgAgente = cgAgenteDao.findById( id );
		
		cgAgente.setEstatus( VALOR_CERO );
		cgAgente.setUserModifId(usuario);
		cgAgente.setFechaModificacion( new Date() );
		cgAgenteDao.update(cgAgente);
		
	}
	
	public static class  GestorReportes extends MidasBaseReporte{
		
	}
	
	@Override
	public InputStream obtenerReporteInventario( String idCentro, String path ) throws Exception {		
		
		Connection connection = null;
		ByteArrayInputStream bis = null;
		
		try {
			Context ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("jdbc/MidasDataSource");
			connection = ds.getConnection();
			
			String plantilla = PATH_PLANTILLA + "reporteInventarioCG.jrxml";
			
			InputStream  input= this.getClass().getResourceAsStream(plantilla);
			JasperDesign design = JRXmlLoader.load(input);
			JasperReport reporteDatosInventario = JasperCompileManager.compileReport(design);
			
			
			GestorReportes ges = new GestorReportes();
			byte[] d = ges.generaReporte(ConstantesReporte.TIPO_XLS, reporteDatosInventario, 
					this.getParamsReport(idCentro), connection);
			
			bis = new ByteArrayInputStream(d);
		} catch (Exception e) {
			throw e;
		}
		finally{
			DbUtils.closeQuietly(connection);			
		}
        
        return bis;		
	}
	
	private Map<String,Object> getParamsReport( String idCentro ) throws Exception {
		
		Map<String,Object> params = new HashMap<String,Object>();
		
		CgCentro cgCentro = cgCentroDao.findById(Long.parseLong(idCentro));
		String centroEmisor = cgCentro.getGerencia().getDescripcion();
		params.put("p_centro", centroEmisor);
		params.put("p_centroid", Long.parseLong(idCentro));
		params.put("p_fecha", new Date());		
		
		return params;
		
	}
	
	@Override
	public Map<Long, String> getMapCentroEmisor() throws Exception {		
		Map<Long,String> map=new LinkedHashMap<Long, String>();
		
		CgCentro cgCentro = new CgCentro();
		cgCentro.setEstatus( VALOR_UNO );
		List<CgCentro> list = cgCentroDao.findByFilter( cgCentro );
		
		if(list!=null && !list.isEmpty()){
			for(CgCentro item:list){
					map.put( item.getId(),item.getGerencia().getDescripcion() );
			}
		}
		return map;
	}
	
	@Override
	public void eliminarCentro( Long id, String usuario ) throws Exception {

		CgCentro cgCentro = cgCentroDao.findById( id );
		
		cgCentro.setEstatus( VALOR_CERO );
		cgCentro.setUserModifId(usuario);
		cgCentro.setFechaModificacion( new Date() );
		cgCentroDao.update(cgCentro);
		
	}
	
	@Override
	public void activarCentro( Long id, String usuario ) throws Exception {

		CgCentro cgCentro = cgCentroDao.findById( id );
		
		cgCentro.setEstatus( VALOR_UNO );
		cgCentro.setUserModifId(usuario);
		cgCentro.setFechaModificacion( new Date() );
		cgCentroDao.update(cgCentro);
		
	}

}
