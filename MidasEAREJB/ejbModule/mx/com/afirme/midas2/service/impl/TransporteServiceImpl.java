package mx.com.afirme.midas2.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.exclusion.ExclusionAumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion.ExclusionRecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion.ExclusionAumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion.CoberturaExcluidaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion.ExclusionRecargoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaDTO;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import mx.com.afirme.midas2.service.TransporteService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteService;

@Stateless
public class TransporteServiceImpl implements TransporteService {
	
	protected EntidadService entidadService;
	
	protected AgenteService agenteService;	
	
	protected AgenteMidasService agenteMidasService;
	
	protected PersonaSeycosFacadeRemote personaSeycosService;
	
	protected DomicilioFacadeRemote domicilioService;
	
	@EJB
	public void setDomicilioService(DomicilioFacadeRemote domicilioService) {
		this.domicilioService = domicilioService;
	}

	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	@EJB
	public void setPersonaSeycosService(PersonaSeycosFacadeRemote personaSeycosService) {
		this.personaSeycosService = personaSeycosService;
	}

	@EJB
	public void setAgenteService(AgenteService agenteService) {
		this.agenteService = agenteService;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public AgenteDTO obtenerAgente(Integer idToAgente) {
		return agenteService.obtenerAgente(idToAgente);
		
	}

	@Override
	public CoberturaDTO obtenerCobertura(Long idToCobertura) {
		CoberturaDTO cobertura = entidadService.findById(CoberturaDTO.class, BigDecimal.valueOf(idToCobertura));
		cobertura.setAumentos(new ArrayList<AumentoVarioCoberturaDTO>());
		cobertura.setAumentoVarioCoberturaDTOs(new ArrayList<AumentoVarioCoberturaDTO>());
		cobertura.setCoaseguros(new ArrayList<CoaseguroCoberturaDTO>());
		cobertura.setCoberturasExcluidas(new ArrayList<CoberturaExcluidaDTO>());
		cobertura.setCoberturasRequeridas(new ArrayList<CoberturaRequeridaDTO>());
		cobertura.setDeducibles(new ArrayList<DeducibleCoberturaDTO>());
		cobertura.setDescuentos(new ArrayList<DescuentoVarioCoberturaDTO>());
		cobertura.setExclusionAumentoVarioCoberturaDTOs(new ArrayList<ExclusionAumentoVarioCoberturaDTO>());
		cobertura.setExclusionAumentoVarioTipoPolizaDTOs(new ArrayList<ExclusionAumentoVarioTipoPolizaDTO>());
		cobertura.setExclusionRecargoVarioCoberturaDTOs(new ArrayList<ExclusionRecargoVarioCoberturaDTO>());
		cobertura.setExclusionRecargoVarioTipoPolizaDTOs(new ArrayList<ExclusionRecargoVarioTipoPolizaDTO>());
		cobertura.setRecargos(new ArrayList<RecargoVarioCoberturaDTO>());
		cobertura.setRecargoVarioCoberturaDTOs(new ArrayList<RecargoVarioCoberturaDTO>());
		cobertura.setSecciones(new ArrayList<CoberturaSeccionDTO>());		
		return cobertura;
	}

	@Override
	public Agente obtenerDatosAgente(Long id){
		Agente filtro = new Agente();
		filtro.setId(id);
		Agente agente = agenteMidasService.loadById(filtro);
		return agente;
	}

	@Override
	public Agente obtenerDatosAgenteClave(Long idAgente) {
		Agente filtro = new Agente();
		filtro.setIdAgente(idAgente);
		Agente agente = agenteMidasService.loadByClave(filtro);
		return agente;
	}
}
