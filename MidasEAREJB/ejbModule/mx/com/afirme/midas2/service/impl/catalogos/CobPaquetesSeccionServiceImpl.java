package mx.com.afirme.midas2.service.impl.catalogos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;


import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTOId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.CobPaquetesSeccionDao;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.dto.RelacionesCobPaquetesSeccionDTO;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.catalogos.CobPaquetesSeccionService;


@Stateless
public class CobPaquetesSeccionServiceImpl implements CobPaquetesSeccionService {
	
	private final String insertedAction = "inserted";
	private final String updatedAction = "updated";
	private final String deletedAction = "deleted";

	@Override
	public List<SeccionDTO> getListarSeccionesVigentesAutos() {
		return seccionFacadeRemote.listarVigentesAutosUsables();
	}
	

	@Override
	public RelacionesCobPaquetesSeccionDTO getRelationLists(CobPaquetesSeccion cobPaquetesSeccion) {
		RelacionesCobPaquetesSeccionDTO relacionesCobPaquetesSeccionDTO = new RelacionesCobPaquetesSeccionDTO();
		List<CobPaquetesSeccion> cobPaquetesSeccionList = cobPaquetesSeccionDao.getPorSeccionPaquete(cobPaquetesSeccion);
		List<CoberturaSeccionDTO> coberturaSeccionDTOList = coberturaSeccionFacadeRemote.getVigentesPorSeccion(BigDecimal.valueOf(cobPaquetesSeccion.getId().getIdToSeccion()));
		List<CoberturaSeccionDTO> disponibles = new ArrayList<CoberturaSeccionDTO>();
		List<CoberturaSeccionDTO> asociadas= new ArrayList<CoberturaSeccionDTO>();
		List<CoberturaSeccionDTO> tempCoberturaSeccionDTOList = new ArrayList<CoberturaSeccionDTO>();
		List<CoberturaSeccionDTOId> coberturaSeccionDTOIdList = new ArrayList<CoberturaSeccionDTOId>();
		
		for(CobPaquetesSeccion item: cobPaquetesSeccionList){
			CoberturaSeccionDTOId coberturaSeccionDTOId = new CoberturaSeccionDTOId();
			CoberturaSeccionDTO coberturaSeccionDTO = new CoberturaSeccionDTO();
			coberturaSeccionDTOId.setIdtocobertura(BigDecimal.valueOf(item.getId().getIdToCobertura()));
			coberturaSeccionDTOId.setIdtoseccion(BigDecimal.valueOf(item.getId().getIdToSeccion()));
			coberturaSeccionDTO.setId(coberturaSeccionDTOId);
			coberturaSeccionDTOIdList.add(coberturaSeccionDTOId);
			coberturaSeccionDTO.setClaveObligatoriedad(BigDecimal.valueOf(item.getClaveObligatoriedad()));
			tempCoberturaSeccionDTOList.add(coberturaSeccionDTO);
		}
		
		for(CoberturaSeccionDTO item: coberturaSeccionDTOList){
			if(coberturaSeccionDTOIdList.contains(item.getId())){
				asociadas.add(item);
			}else{
				disponibles.add(item);
			}
		}
		int index = 0;
		for(CoberturaSeccionDTO item: asociadas){
			for(CoberturaSeccionDTO temp: tempCoberturaSeccionDTOList){
				if(item.getId().equals(temp.getId()))asociadas.get(index).setClaveObligatoriedad(temp.getClaveObligatoriedad());
			}
			index++;
		}
		
		relacionesCobPaquetesSeccionDTO.setAsociadas(asociadas);
		relacionesCobPaquetesSeccionDTO.setDisponibles(disponibles);
		return relacionesCobPaquetesSeccionDTO;
	}

	@Override
	public RespuestaGridRelacionDTO relacionarCoberturaSeccion(String accion,
			CobPaquetesSeccion cobPaquetesSeccion) {
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		
		if (accion.equals(insertedAction)) {
			cobPaquetesSeccionDao.persist(cobPaquetesSeccion);
		}else if ( accion.equals(updatedAction)){
			cobPaquetesSeccionDao.update(cobPaquetesSeccion);
		}else if (accion.equals(deletedAction)) {
			cobPaquetesSeccionDao.remove(cobPaquetesSeccion);
		}
		//TODO Preparar la respuesta y regresarla		
		return null;
	}

	private CobPaquetesSeccionDao cobPaquetesSeccionDao;
	private CoberturaSeccionFacadeRemote coberturaSeccionFacadeRemote;
	private SeccionFacadeRemote seccionFacadeRemote;
	
	@EJB
	public void setCobPaquetesSeccionDao(CobPaquetesSeccionDao cobPaquetesSeccionDao){
		this.cobPaquetesSeccionDao = cobPaquetesSeccionDao;
	}
	
	@EJB
	public void setCoberturaSeccionFacadeRemote(CoberturaSeccionFacadeRemote coberturaSeccionFacadeRemote){
		this.coberturaSeccionFacadeRemote = coberturaSeccionFacadeRemote;
	}
	
	@EJB
	public void setSeccionFacadeRemote(SeccionFacadeRemote seccionFacadeRemote){
		this.seccionFacadeRemote = seccionFacadeRemote;
	}

	
}
