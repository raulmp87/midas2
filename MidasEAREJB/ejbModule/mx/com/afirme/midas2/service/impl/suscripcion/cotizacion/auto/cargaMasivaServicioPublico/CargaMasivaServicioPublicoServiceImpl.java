package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.cargaMasivaServicioPublico;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.CargaMasivaServicioPublicoDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.sistema.seguridad.SeguridadContext;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublico;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublico.EstatusArchivo;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublicoDetalle;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublicoDetalle.EstatusDetalle;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublicoDetalleTradSeyMid;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionCotizacion;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionDetalle;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosEstimacionCoberturaDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.BitacoraCargaMasivaSPResultadoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaSPDetalleDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoEstatusDescriDetalleDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoEstatusDescriDetalleDTO.EstatusDescriDetalle;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CoberturaDetalleDTO;
import mx.com.afirme.midas2.service.ExcelRowMapper;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.impl.ExcelConverter;
import mx.com.afirme.midas2.service.impl.ExcelConverter.ExcelConfig.ExcelConfigBuilder;
import mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.utility.ValidateInfoFile.ClaveCoberturas;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.CargaMasivaServicioPublicoJobService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.CargaMasivaServicioPublicoService;
import mx.com.afirme.midas2.service.suscripcion.emision.EmisionService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;
import mx.com.afirme.midas2.service.vehiculo.ValorComercialVehiculoService;
import mx.com.afirme.midas2.util.MailService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

@Stateless
public class CargaMasivaServicioPublicoServiceImpl implements CargaMasivaServicioPublicoService{

	private static final Logger LOG = Logger.getLogger(CargaMasivaServicioPublicoServiceImpl.class);
	
	private static final String SALTO_LINEA = "<br>";
	private static final String CON_ERROR = "Total con error: ";
	private static final String EXITOSOS = "Total exitosos: ";
	private static final String PROCESADOS = "El archivo se almaceno con exito.";
	private static final String POR_EMITIR_DESCRI = "Detalle cargado, pendiente emitir";
	private static final String POR_VALIDAR_DESCRI = "Detalle cargado, pendiente validar";
	private static final String NUEVO_MENSAJE = "Consulte la pantalla de detalle para ver los resultados";
	
	private static final int INDIVIDUAL_VEHICLES_PRODUCT_ID = 1;
	private static final String TIPO_CONVERSION_PRODUCTO = "PRODUCTO";
	private static final String TIPO_CONVERSION_LINEA_NEGOCIO = "LINEA";
	private static final String RFC_DEFAULT = "XAXX010101000";
	
	public static final String TIPO_COTIZACION_CMSP = "CARGA_MASIVA_SERVICIO_PUBLICO";
	
	List<BitacoraCargaMasivaSPResultadoDTO> pruebaList= new ArrayList<BitacoraCargaMasivaSPResultadoDTO>();
	
	@Resource
	private javax.ejb.SessionContext sessionContext;
	
	@EJB
	private CargaMasivaServicioPublicoDao cargaMasivaServicioPublicoDao;
	@EJB
	private CargaMasivaServicioPublicoJobService cargaMasivaServicioPublicoJobService;
	@EJB
	private ValorComercialVehiculoService valorComercialVehiculoService;
	@EJB
	private EntidadService entidadService;
	@EJB
	private CargaMasivaService cargaMasivaService;
	@EJB
	private ClienteFacadeRemote clienteFacadeRemote;
	@EJB
	private CotizacionService cotizacionService;
	@EJB
	protected DireccionMidasService direccionMidasService;
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	@EJB
	protected AgenteMidasService agenteMidasService;
	@EJB
	private EmisionService emisionService;
	@EJB
	private SolicitudAutorizacionService solicitudAutorizacionService;
	@EJB
	private MailService mailService;
	
	/**
	 * Método de implementado de service para la consulta de archivos de carga masiva almacenados
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link CargaMasivaServicioPublicoDTO} archivoAdjunto
	 * @return {@link CargaMasivaServicioPublico}
	 */
	@Override
	public List<CargaMasivaServicioPublico> consultaArchivosCargaMasivaService(CargaMasivaServicioPublicoDTO  archivoAdjunto) {
		
		/******************PRUEBA OJOS**************************/
//		cargaMasivaServicioPublicoJobService.igualarTarifas(new Long(73), new BigDecimal(191), new BigDecimal(1301), new BigDecimal(1611), new Long(1),
//				  											new Long(484), "15000", "15001", new Long(2), new Long(4),
//															new ArrayList<CoberturaDetalleDTO>(), new Double(1500.00));
//		2520	AUT RES - Robo Total
//		2530	AUT RES - Responsabilidad Civil
//		2550	AUT RES - Gastos Médicos
//		2570	AUT RES - Accidentes Automovilísticos al Conductor
//		2600	AUT RES - Equipo Especial
//		2610	AUT RES - Asistencia Jurídica
//		2620	AUT RES - Asistencia en Viajes y Vial Km "0"
//		2650	AUT RES - Responsabilidad Civil Viajero
		/******************PRUEBA OJOS**************************/
		
		return cargaMasivaServicioPublicoDao.consultaArchivosCargaMasivaDao(archivoAdjunto);
	}
	
	/**
	 * Método de implementado de service para verifiar si un MD5 ya existe en la tabla y en que estatus esta
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @return boolean
	 */
	@Override
	public boolean getMd5ArchivoService(String md5CheckSum) {
		boolean existe = false;
		List<CargaMasivaServicioPublico> listArchivos =cargaMasivaServicioPublicoDao.getMd5ArchivoDao(md5CheckSum);
		if(!listArchivos.isEmpty()){
			existe = true;
		}
		return existe;
	}
	
	/**
	 * Método de implementado de service para almacenar el archivo en la base de datos
	 * @author SOFTNET - ISCJJBV 
	 * 
	 */
	public CargaMasivaServicioPublico saveArchivoService(CargaMasivaServicioPublico archivoCargaMasiva) {
		return archivoCargaMasiva = cargaMasivaServicioPublicoDao.saveArchivoDao(archivoCargaMasiva);
	}
	
	/**
	 * Método de implementado de service para almacenar el archivo en la base de datos
	 * @author SOFTNET - ISCJJBV 
	 * 
	 */
	public List<String> saveDetalleArchivoService(CargaMasivaServicioPublico archivoCargaMasiva, File archivo) {
		
		List<CargaMasivaServicioPublicoDetalle> list = null;
		try {
			list = this.cargarInformacionDesdeExcel(archivo);
		}catch (Exception e) {
			e.printStackTrace();
		}
		List<String> listResultados = new ArrayList<String>();
		
		if( list!=null && list.size() > 0 ){
			try {
				listResultados = this.guardaInformacionDB( list, archivoCargaMasiva);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			listResultados.add("No se procesaron registros.");
		}		
		return listResultados;
	}

	/**
	 * Método de implementado de service para almacenar el archivo en la base de datos
	 * @author SOFTNET - ISCJJBV 
	 * 
	 */
	public Long conteoBusquedaArchivosService(CargaMasivaServicioPublicoDTO  archivoAdjunto){
		return cargaMasivaServicioPublicoDao.conteoBusquedaArchivosDao(archivoAdjunto);
	}
	
	/*************************************Metodos de utileria para la carga de detalle****************************************/
	
	private List<String> guardaInformacionDB( List<CargaMasivaServicioPublicoDetalle> list, CargaMasivaServicioPublico archivoCargaMasiva ) throws SQLException, Exception{
		
		List<String> listResultados = new ArrayList<String>();
		int contador=0, correctos=0, error=0;
		List<CargaMasivaServicioPublicoDetalle> listEmisionDetalle = new ArrayList<CargaMasivaServicioPublicoDetalle>();
		
		for (CargaMasivaServicioPublicoDetalle item : list) {
			item.setArchivoCargaMasivaSP_ID(archivoCargaMasiva);
			item.setEstatusProceso(EstatusDetalle.POR_VALIDAR.toString());
			item.setDescripcionEstatusProceso(POR_VALIDAR_DESCRI);
			item.setCodigoUsuarioCreacion(archivoCargaMasiva.getCodigoUsuarioCreacion());
			item.setCodigoUsuarioModificacion(archivoCargaMasiva.getCodigoUsuarioModificacion());
			contador++;
			try {
//				listEmisionDetalle.add(cargaMasivaServicioPublicoDao.saveDetalleArchivoDao(item));
				listEmisionDetalle.add(item);
				correctos++;	
			}	
			catch( Exception e ){
				listResultados.add("Registro con error: " + contador);	
				error++;		
			}
		}
		try {
			archivoCargaMasiva.setCargaMasivaSerPubDetalle(listEmisionDetalle);
			cargaMasivaServicioPublicoDao.saveArchivoDao(archivoCargaMasiva);
			
			Usuario usuario = usuarioService.getUsuarioActual();
			
			cargaMasivaServicioPublicoJobService.validaDatosEmision(listEmisionDetalle, usuario);
		} catch (Exception e) {
			correctos=0;
			listEmisionDetalle.get(0).getArchivoCargaMasivaSP_ID().setEstatusArchivoCargaMasiva(EstatusArchivo.ERROR_CARGA.getEstatus());
			cargaMasivaServicioPublicoDao.updateEstatusArchivoDao(listEmisionDetalle.get(0).getArchivoCargaMasivaSP_ID());
			listResultados.add("Error en validacion: ");
			error++;
		}
		
		listResultados = this.sumarizaListResultados( listResultados, contador, correctos, error ); 
		return listResultados;
	}
	
	private List<String> sumarizaListResultados( List<String> list, int contador, int correctos, int error ){	
		list.add( 0, SALTO_LINEA );
//		list.add( 0, CON_ERROR + error );
//		list.add( 0, EXITOSOS + correctos );
		list.add( 0, PROCESADOS);
		list.add( 0, NUEVO_MENSAJE);
				
		return list;
	}
	
private List<CargaMasivaServicioPublicoDetalle> cargarInformacionDesdeExcel(File archivo) throws Exception{
		InputStream is = null;
		List<CargaMasivaServicioPublicoDetalle> list = null;
		
		try {
			is = new FileInputStream(archivo);
			ExcelConverter converter = new ExcelConverter(is, new ExcelConfigBuilder().withColumnNamesToLowerCase(true) .withTrimColumnNames(true).build());
			
			list =	converter.parseWithRowMapper(converter.getSheetNames().get(0).toString(), new ExcelRowMapper<CargaMasivaServicioPublicoDetalle>() {
						@Override
						public CargaMasivaServicioPublicoDetalle mapRow(Map<String, String> row, int rowNum) {
							CargaMasivaServicioPublicoDetalle detalleArchivo = new CargaMasivaServicioPublicoDetalle();
							detalleArchivo = poblarDetalle(row);
							return detalleArchivo;
						}
					});
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					throw e;
				}
			}
		}
		return list;
	}

	private CargaMasivaServicioPublicoDetalle poblarDetalle( Map<String, String> row ){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		CargaMasivaServicioPublicoDetalle detalleArchivo = new CargaMasivaServicioPublicoDetalle();

		detalleArchivo.setEstatus(row.get("estatus"));
		detalleArchivo.setIdNegocio(row.get("id_negocio")==null?null:new Long(row.get("id_negocio")));
		detalleArchivo.setIdProducto(row.get("id_producto")==null?INDIVIDUAL_VEHICLES_PRODUCT_ID:new Long(row.get("id_producto")));
		
			//LLENA ID DE TRADUCCIÓN DE SEYCOS A MIDAS
			BigDecimal idMidas = cargaMasivaServicioPublicoDao.obtieneConversionSeycosMidas(TIPO_CONVERSION_PRODUCTO, detalleArchivo.getIdProducto());
			if(idMidas != null){
				detalleArchivo.getCargaMasivaSerPubDetalleTradSeyMid().setProductoIdMidas(Long.valueOf(idMidas.toString()));
			}else{
				detalleArchivo.getCargaMasivaSerPubDetalleTradSeyMid().setProductoIdMidas(Long.valueOf(detalleArchivo.getIdProducto()));
			}
		
		detalleArchivo.setIdTipoPoliza(row.get("id_tipopoliza")==null?null:new Long(row.get("id_tipopoliza")));
		detalleArchivo.setIdAgente(row.get("id_agente")==null?null:new Long(row.get("id_agente")));
		detalleArchivo.setIdPaquete(row.get("id_paquete")==null?null:new Long(row.get("id_paquete")));
		detalleArchivo.setIdCentroEmisor(row.get("id_centro_emisior")==null?null:new Long(row.get("id_centro_emisior")));
		detalleArchivo.setIdOficina(row.get("id_oficina")==null?null:new Long(row.get("id_oficina")));
		detalleArchivo.setNumToPoliza(row.get("numero_de_poliza")==null?null:new Long(row.get("numero_de_poliza")));
		detalleArchivo.setNumLiquidacion(row.get("numero_de_liquidacion")==null?null:new Long(row.get("numero_de_liquidacion")));
		detalleArchivo.setAutorizacionProsa(row.get("autorizacion_prosa"));
		try {
			detalleArchivo.setFechaVigencia(row.get("f_vigencia")==null?null:dateFormat.parse(row.get("f_vigencia")));
			detalleArchivo.setFechaEmision(row.get("f_emision")==null?null:dateFormat.parse(row.get("f_emision")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		detalleArchivo.setVigencia(row.get("vigencia")==null?null:new Long(row.get("vigencia")));
		detalleArchivo.setTipoPersonaCliente(row.get("tipo_persona_cliente"));
		String rfc_archivo = row.get("rfc_del_cliente");
		detalleArchivo.setRFCCliente((rfc_archivo == null || rfc_archivo.equals("0") || rfc_archivo.isEmpty())? RFC_DEFAULT : rfc_archivo );
		detalleArchivo.setNombreRazonSocial(row.get("nombre_o_razon_social"));
		detalleArchivo.setApellidoPaterno(row.get("apellido_paterno_cliente"));
		detalleArchivo.setApellidoMaterno(row.get("apellido_materno_cliente"));
		detalleArchivo.setCodigoPostalCliente(row.get("codigo_postal_cliente"));
		detalleArchivo.setCalleCliente(row.get("calle_cliente"));
		detalleArchivo.setColoniaCliente(row.get("colonia_cliente"));
		detalleArchivo.setTelefonoCliente(row.get("telefono_del_cliente"));
		detalleArchivo.setClaveAMIS(row.get("cve_amis"));
		detalleArchivo.setModelo(row.get("modelo")==null?null:new Long(row.get("modelo")));
		detalleArchivo.setNcRepuve(row.get("nci_repuve"));
		detalleArchivo.setClaveUso(row.get("cve_uso"));
		detalleArchivo.setPlacas(row.get("placas"));
		detalleArchivo.setNumeroMotor(row.get("numero_de_motor"));
		detalleArchivo.setNumeroSerie(row.get("numero_de_serie"));

		//REVISAR SI EL VALOR ES NULL IR A ValorComercialVehiculoServiceImp PARA OBTENER VALOR COMERCIAL DEFAULT
		if(row.get("valor_comercial")==null || row.get("valor_comercial").equals("")){
			if(detalleArchivo.getClaveAMIS() != null 
					&& !detalleArchivo.getClaveAMIS().isEmpty()
					&& detalleArchivo.getModelo() != null){
				DatosEstimacionCoberturaDTO datosEstimacion = new DatosEstimacionCoberturaDTO();
				valorComercialVehiculoService.getValorComercial(datosEstimacion, Long.valueOf(detalleArchivo.getClaveAMIS()), Short.valueOf(detalleArchivo.getModelo().toString()), new Date());
				if(datosEstimacion.getSumaAseguradaProporcionadaOpcion1() == null){
					detalleArchivo.setValorComercial(datosEstimacion.getSumaAseguradaProporcionadaOpcion2()==null?null:Double.parseDouble(datosEstimacion.getSumaAseguradaProporcionadaOpcion2().toString()));
				}else{
					detalleArchivo.setValorComercial(datosEstimacion.getSumaAseguradaProporcionadaOpcion1()==null?null:Double.parseDouble(datosEstimacion.getSumaAseguradaProporcionadaOpcion1().toString()));
				}
			}
		}else{
			detalleArchivo.setValorComercial(Double.parseDouble(row.get("valor_comercial")));
		}
		
		detalleArchivo.setTotalPasajeros(row.get("total_pasajeros")==null?null:new Long(row.get("total_pasajeros")));
		detalleArchivo.setDescripcionVehiculo(row.get("descripción_del_vehículo"));
		detalleArchivo.setDerrotero(row.get("derrotero"));
		detalleArchivo.setRamal(row.get("ramal"));
		detalleArchivo.setRuta(row.get("ruta"));
		detalleArchivo.setNumeroEconomico(row.get("numero_economico"));
		detalleArchivo.setPrimaTotal(row.get("prima_total")==null?null:Double.parseDouble(row.get("prima_total")));
		detalleArchivo.setDeducibleDanosMateriales(row.get("deducible_daños_materiales")==null?null:Double.parseDouble(row.get("deducible_daños_materiales")));
		detalleArchivo.setDeducibleRoboTotal(row.get("deducible_robo_total")==null?null:Double.parseDouble(row.get("deducible_robo_total")));
		detalleArchivo.setLimiteRcTerceros(row.get("limite_rc_terceros")==null?null:Double.parseDouble(row.get("limite_rc_terceros")));
		detalleArchivo.setDeducibleRcTerceros(row.get("deducible_rc_terceros")==null?null:Double.parseDouble(row.get("deducible_rc_terceros")));
		detalleArchivo.setLimiteGastosMedicos(row.get("limite_gastos_medicos")==null?null:Double.parseDouble(row.get("limite_gastos_medicos")));
		detalleArchivo.setLimiteMuerte(row.get("limite_muerte")==null?null:Double.parseDouble(row.get("limite_muerte")));
		detalleArchivo.setLimiteRcViajero(row.get("limite_rc_viajero")==null?null:Double.parseDouble(row.get("limite_rc_viajero")));
		detalleArchivo.setDeducibleRCV(row.get("ded_rcv")==null?null:Double.parseDouble(row.get("ded_rcv")));
		detalleArchivo.setAsistenciaJuridica(row.get("asistencia_juridica"));
		detalleArchivo.setLimiteEquipoEspecial(row.get("limite_equipo_especial")==null?null:Double.parseDouble(row.get("limite_equipo_especial")));
		detalleArchivo.setAsistenciaVial(row.get("asistencia_vial"));
		detalleArchivo.setIgualacion(row.get("igualacion")==null?null:new Long(row.get("igualacion")));
		detalleArchivo.setDerechos(row.get("derechos")==null?null:Double.parseDouble(row.get("derechos")));
		detalleArchivo.setSolicitarAutorizacion(row.get("solicitar_autorizacion")==null?null:new Long(row.get("solicitar_autorizacion")));
		detalleArchivo.setCausaAutorizacion(row.get("causa_autorizacion"));
		
		detalleArchivo.setLineaNegocio(row.get("linea_negocio")==null?null:row.get("linea_negocio"));
		if(detalleArchivo.getLineaNegocio() != null){
			BigDecimal lineaNegocioId = cargaMasivaServicioPublicoDao.obtieneConversionSeycosMidas(TIPO_CONVERSION_LINEA_NEGOCIO, Long.valueOf(detalleArchivo.getLineaNegocio()));
			if(lineaNegocioId != null){
				detalleArchivo.getCargaMasivaSerPubDetalleTradSeyMid().setLineaNegIdMidas(Long.valueOf(lineaNegocioId.toString()));
			}else{
				detalleArchivo.getCargaMasivaSerPubDetalleTradSeyMid().setLineaNegIdMidas(Long.valueOf(detalleArchivo.getLineaNegocio()));
			}
		}else{
			detalleArchivo.getCargaMasivaSerPubDetalleTradSeyMid().setLineaNegIdMidas(Long.valueOf(0));
		}
		
		detalleArchivo.setLineaNegocio(row.get("linea_negocio"));
		detalleArchivo.setAnualMesesSinIntereses(row.get("anual_meses_sin_intereses")==null?null:new Long(row.get("anual_meses_sin_intereses")));
		
		Boolean existe = cargaMasivaServicioPublicoDao.existePolizaSeycos(detalleArchivo.getNumToPoliza(), detalleArchivo.getIdCentroEmisor());
		detalleArchivo.setExistePolizaSeycos(existe); 
		
		detalleArchivo.getCargaMasivaSerPubDetalleTradSeyMid().setDetalle(detalleArchivo);
		
		return detalleArchivo;
	}

	public List<CargaMasivaSPDetalleDTO> obtenerCargaMasivaDetalle(Long archivoId, Integer count, Integer posStart) {
		HashMap<String, Object> params = new HashMap<String, Object>();
//		CargaMasivaServicioPublico archivo = entidadService.findById(CargaMasivaServicioPublico.class, archivoId);
//		generarCotizacionesCargaMasivaServicioPublico(archivo);
     	params.put("archivoCargaMasivaSP_ID.idArchivoCargaMasiva", archivoId);
          	List<CargaMasivaSPDetalleDTO> listaDetalle = cargaMasivaServicioPublicoDao.busquedaDetalle(archivoId, count, posStart);
        return listaDetalle;
	}

	@Override
	public Long contarDetalle(Long archivoId, Integer count, Integer posStart) {
		return cargaMasivaServicioPublicoDao.contarDetalle(archivoId, count, posStart);
	}
	
	public String mostrarErrorDetalle( Long idDetalle){
		CargaMasivaServicioPublicoDetalle detalle =
			entidadService.findById(CargaMasivaServicioPublicoDetalle.class, idDetalle );
		
		return detalle.getDescripcionEstatusProceso();
	}
	
	/*OJOS @Asynchronous*/
	@Override
	public void generarCotizacionesCargaMasivaServicioPublico(CargaMasivaServicioPublico archivo, Usuario usuario){
		LOG.info("CMSP - generarCotizacionesCargaMasivaServicioPublico...");
		CargaMasivaServicioPublicoService processor = this.sessionContext.getBusinessObject(CargaMasivaServicioPublicoService.class);
		
		//SE CREA EL CONTEXTO DE SEGURIDAD PARA PODER OBTENER EL USUARIO EN USUARIO SERVICES YA QUE SE PERDIA DEBIDO A QUE ES ASYNCHRONOUS
		SeguridadContext context = SeguridadContext.getContext();
		context.setUsuario(usuario);
		
		for(CargaMasivaServicioPublicoDetalle detalle : archivo.getCargaMasivaSerPubDetalle()){
			
			LOG.info("CMSP - creando cotizacion para el detalle: " + detalle.getIdDetalleCargaMasiva() + " con estatus: " + detalle.getEstatusProceso());
			if(detalle.getEstatusProceso().equals(EstatusDetalle.POR_EMITIR.toString())){
				
				CargaMasivaServicioPublicoEstatusDescriDetalleDTO resultado = new CargaMasivaServicioPublicoEstatusDescriDetalleDTO();
				resultado.setListEstatusDescri(new ArrayList<EstatusDescriDetalle>());
				
				resultado = processor.generaCotizacion(detalle, resultado, usuario);
				
				String erroresCotizacion = obtenerMensajesResultado(EstatusDetalle.ERROR_COTIZACION.toString(), resultado);
				String porAutorizar = obtenerMensajesResultado(EstatusDetalle.POR_AUTORIZAR.toString(), resultado);
				
				if(	(erroresCotizacion == null
						|| erroresCotizacion.isEmpty())){
					
					Long idCotizacionLong = resultado.getNumCotizacion();
					if(idCotizacionLong != null 
							&& idCotizacionLong > 0){
						CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, BigDecimal.valueOf(idCotizacionLong));
						if(cotizacion != null){
							LOG.info("CMSP - cotizacion generada correctamente...");
							detalle.setCotizacion(cotizacion);
							
							if( ( porAutorizar == null 
									|| porAutorizar.isEmpty())){
							
								resultado = processor.emitirCotizacion(cotizacion, resultado);
								String erroresPoliza = obtenerMensajesResultado(EstatusDetalle.ERROR_POLIZA.toString(), resultado);
								if(erroresPoliza == null
										|| erroresPoliza.isEmpty()){
									Long idPolizaLong = resultado.getNumPoliza();
									if(idPolizaLong != null
											&& idPolizaLong > 0){
										PolizaDTO poliza = entidadService.findById(PolizaDTO.class, BigDecimal.valueOf(idPolizaLong));
										if(poliza != null){
											detalle.setPoliza(poliza);
											detalle.setEstatusProceso(EstatusDetalle.EMITIDA.toString());
											detalle.setDescripcionEstatusProceso("Poliza Emitida");
											if(detalle.getNumToPoliza() != null){
												poliza.setNumeroPolizaSeycos(BigDecimal.valueOf(detalle.getNumToPoliza()));
												poliza.setClavePolizaSeycos(generaNumPolizaSeycos(detalle));
												poliza = entidadService.save(poliza);
											}
										}
										
									}else{
										detalle.setEstatusProceso(EstatusDetalle.ERROR_POLIZA.toString());
										detalle.setDescripcionEstatusProceso("Error al emitir la poliza. No se obtuvo el numero de poliza.");
									}
								}else{
									detalle.setEstatusProceso(EstatusDetalle.ERROR_POLIZA.toString());
									detalle.setDescripcionEstatusProceso(erroresPoliza);
								}
							}else{
								detalle.setEstatusProceso(EstatusDetalle.POR_AUTORIZAR.toString());
								detalle.setDescripcionEstatusProceso(porAutorizar);
							}
						}
					}
				}else{
					if(  erroresCotizacion != null
									&& !erroresCotizacion.isEmpty() ){
						detalle.setEstatusProceso(EstatusDetalle.ERROR_COTIZACION.toString());
						detalle.setDescripcionEstatusProceso(erroresCotizacion);
					}else if( (porAutorizar != null
							&& !porAutorizar.isEmpty()) ){
						detalle.setEstatusProceso(EstatusDetalle.POR_AUTORIZAR.toString());
						detalle.setDescripcionEstatusProceso(porAutorizar + "\n" + erroresCotizacion);
					}else{
						detalle.setEstatusProceso(EstatusDetalle.ERROR_COTIZACION.toString());
						detalle.setDescripcionEstatusProceso("Error al generar la cotizacion");
					}
				}
				detalle = entidadService.save(detalle);
			}
		}
		
		enviarNotificacionCargaMasiva(archivo);
		
	}
	
	private String generaNumPolizaSeycos(CargaMasivaServicioPublicoDetalle detalle){
		String numPolizaSeycos = "";
		
		try{
				String numeroPoliza = "";
				try {
					if(detalle.getNumToPoliza().toString().length() <= 6){
						numeroPoliza += String.format("%06d", detalle.getNumToPoliza());
					}else{
						numeroPoliza += String.format("%08d", detalle.getNumToPoliza());
					}
				} catch (Exception e) {
				}
			
			numPolizaSeycos = String.format("%03d", detalle.getIdCentroEmisor())+"-"+numeroPoliza+"-"+String.format("%02d", 0);
		}catch(Exception e){
		}
		
		return numPolizaSeycos;
	}

	private String obtenerMensajesResultado(String estatus, CargaMasivaServicioPublicoEstatusDescriDetalleDTO resultado){
		StringBuilder mensajes = new StringBuilder();
		String mensajeFinal = "";
		
		if(resultado != null
				&& resultado.getListEstatusDescri() != null
				&& !resultado.getListEstatusDescri().isEmpty()){
			for(EstatusDescriDetalle info: resultado.getListEstatusDescri()){
				if(info.getEstatusDetalle().equals(estatus)){
					mensajes.append(info.getDescriEstatus());
					mensajes.append("\n");
				}
			}
			if(!mensajes.toString().isEmpty()){
				mensajeFinal = mensajes.substring(0, mensajes.length() - 1);
			}
		}
		
		return mensajeFinal;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public CargaMasivaServicioPublicoEstatusDescriDetalleDTO generaCotizacion(CargaMasivaServicioPublicoDetalle detalle, CargaMasivaServicioPublicoEstatusDescriDetalleDTO resultado, Usuario usuario){
		LOG.info("CMSP - generaCotizacion( detalle: " + detalle.getIdDetalleCargaMasiva() + " usuario: " + usuario.getNombreUsuario() + " )");
		
		CotizacionDTO cotizacion = new CotizacionDTO();
		SolicitudDTO solicitud = new SolicitudDTO();
		
		boolean requiereAutorizacion = false;
		boolean errorIgualacion = false;
		boolean errorTarifa = false;
		boolean errorCotizacion = false;
		
		CargaMasivaServicioPublicoService processor = this.sessionContext.getBusinessObject(CargaMasivaServicioPublicoService.class);
		
		try{
		
		LOG.info("CMSP - Cargando el negocio: " + detalle.getIdNegocio()); 
//		NEGOCIO
		Negocio negocio = entidadService.findById(Negocio.class, detalle.getIdNegocio());
		solicitud.setNegocio(negocio);
		cotizacion.setSolicitudDTO(solicitud);
		
//		INFORMACION DE MIGRACION A SEYCOS
		cotizacion.setNumCentroEmisor(detalle.getIdCentroEmisor().toString());
		cotizacion.setNumPolizaServicioPublico(detalle.getNumToPoliza().toString());
		cotizacion.setTipoCotizacion(TIPO_COTIZACION_CMSP);
		
		NegocioProducto negocioProducto = null;
		NegocioSeccion negocioSeccion = null;
		NegocioTipoUso negocioTipoUso = null;
		NegocioPaqueteSeccion negocioPaqueteSeccion = null;
		NegocioTipoPoliza negocioTipoPoliza = null;
		
		LOG.info("CMSP - Cargando forma de pago...");
//		FORMA DE PAGO
		FormaPagoDTO formaPago = null;
		CargaMasivaServicioPublicoDetalleTradSeyMid tradSeyMidasCMSP = (entidadService.findByProperty(CargaMasivaServicioPublicoDetalleTradSeyMid.class, "detalle.idDetalleCargaMasiva", detalle.getIdDetalleCargaMasiva())).get(0);
//			entidadService.findById(CargaMasivaServicioPublicoDetalleTradSeyMid.class, detalle.getIdDetalleCargaMasiva());
		
		for(NegocioProducto negProducto : negocio.getNegocioProductos()){
			if(negProducto.getProductoDTO().getIdToProducto().toString().equals(tradSeyMidasCMSP.getProductoIdMidas().toString())){
				
				formaPago = negProducto.getNegocioFormaPagos().get(0).getFormaPagoDTO();
				
				negocioTipoPoliza = negProducto.getNegocioTipoPolizaList().get(0);
				
//		NEGOCIO TIPO POLIZA
				cotizacion.setNegocioTipoPoliza(negocioTipoPoliza);
		
		LOG.info("CMSP - cargando la informacion del negocio...");
//		CARGANDO INFORMACION DEL NEGOCIO
				negocioProducto = negProducto;
				for(NegocioSeccion negSeccion : negocioTipoPoliza.getNegocioSeccionList()){
					if(negSeccion.getSeccionDTO().getIdToSeccion().toString().equals(tradSeyMidasCMSP.getLineaNegIdMidas().toString())){
						negocioSeccion = negSeccion;
						for(NegocioTipoUso negTipoUso : negSeccion.getNegocioTipoUsoList()){
							if(negTipoUso.getTipoUsoVehiculoDTO().getCodigoTipoUsoVehiculo().equals(detalle.getClaveUso())){
								negocioTipoUso = negTipoUso;
								break;
							}
						}
						for(NegocioPaqueteSeccion negPaqueteSeccion : negSeccion.getNegocioPaqueteSeccionList()){
							if(negPaqueteSeccion.getPaquete().getId().equals(detalle.getIdPaquete())){
								negocioPaqueteSeccion = negPaqueteSeccion;
								break;
							}
						}
						break;
					}
				}
				break;
			}
				
		}
		
		if( negocioProducto == null ){
			resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"El producto no esta configurado para el negocio"));
			return resultado;
		}else if (negocioSeccion == null){
			resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"La linea de negocio no esta configurada para el negocio"));
			return resultado;
		}else if(negocioTipoUso == null){
			resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"El tipo de uso no esta configurado para el negocio"));
			return resultado;
		}else if (negocioPaqueteSeccion == null){
			resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"El paquete no esta configurado para el negocio"));
			return resultado;
		}else if (negocioTipoPoliza == null){
			resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"El tipo de poliza no esta configurado para el negocio"));
			return resultado;
		}
	
		if(formaPago != null){
			cotizacion.setIdFormaPago(new BigDecimal(formaPago.getIdFormaPago()));
		}else{
			resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"Forma de Pago Invalido"));
		}
		
//		AGENTE
		List<Agente> agenteList = entidadService.findByProperty(Agente.class, "idAgente", detalle.getIdAgente());
		Agente agente = agenteList.get(0);
		solicitud.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
		
		
//		MONEDA para este producto la moneda siempre es nacional
		cotizacion.setIdMoneda(new BigDecimal(CargaMasivaServicioPublicoJobImpl.ID_MONEDA_NACIONAL_PESOS));
		
		LOG.info("CMSP - creando la cotizacion...");
		cotizacion = cargaMasivaService.crearCotizacion(cotizacion);
		
//		FECHAS VIGENCIA
		cotizacion.setFechaInicioVigencia(detalle.getFechaVigencia());
		
		Date fechaFinVigencia = cargaMasivaServicioPublicoDao.obtenerFinVigenciaPoliza(detalle.getFechaVigencia(), detalle.getVigencia());
		cotizacion.setFechaFinVigencia(fechaFinVigencia);
		

		
//		MEDIO DE PAGO
		MedioPagoDTO medioPago = cargaMasivaService.obtieneMedioPago("AGENTE");
		if(medioPago != null){
			cotizacion.setIdMedioPago(new BigDecimal(medioPago.getIdMedioPago()));
		}else{
			resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"Medio de Pago Efectivo no definido en sistema"));
		}
		
//		DERECHO POLIZA
		NegocioDerechoPoliza negocioDerechoPoliza = cargaMasivaService.obtieneNegocioDerechoPoliza(new BigDecimal(negocio.getIdToNegocio()), detalle.getDerechos());
		if(negocioDerechoPoliza != null){
			cotizacion.setNegocioDerechoPoliza(negocioDerechoPoliza);
		}else{
			resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"Derechos Invalidos"));
		}
		
//		BONO COMISION
		if(cotizacion.getPorcentajebonifcomision() == null && cotizacion.getSolicitudDTO().getNegocio().getPctPrimaCeder() != null){
			cotizacion.setPorcentajebonifcomision(cotizacion.getSolicitudDTO().getNegocio().getPctPrimaCeder());
		}
		
//		OBTIENE EL DESCUENTO GLOBAL DEFAULT
		if (cotizacion.getSolicitudDTO().getNegocio().getPctDescuentoDefault() != null && cotizacion.getPorcentajeDescuentoGlobal() == null) {
		  cotizacion.setPorcentajeDescuentoGlobal(new Double(0)); //Dejara de aplicarse el descuento global
		}
		
//		COLOCA PctPagoFraccionado SI NEGOCIO LO PERMITE
		if(cotizacion.getPorcentajePagoFraccionado() == null){
			try{
				Double pctPagoFrac = cargaMasivaService
						.getPctePagoFraccionado(cotizacion
								.getIdFormaPago().intValue(),
								cotizacion.getIdMoneda().shortValue());
				if (pctPagoFrac == null) {
					cotizacion.setPorcentajePagoFraccionado(new Double(
							0));
				} else {
					cotizacion
							.setPorcentajePagoFraccionado(pctPagoFrac);
				}
			}catch(Exception e){
				cotizacion.setPorcentajePagoFraccionado(new Double(0));
			}
		}
		
//		IVA
		Double iva = cargaMasivaService.obtieneIVAPorCodigoPostalColonia(detalle.getCodigoPostalCliente(), detalle.getColoniaCliente());
		cotizacion.setPorcentajeIva(iva);
		
		LOG.info("CMSP - cargando la informacion del cliente...");
//		CONTRATANTE - CLIENTE
		
		ClienteGenericoDTO cliente = null;
		
//		Si es RFC default que no se realice la busquedas (XAXX010101000 o 0)
		if(!detalle.getRFCCliente().equals("0")
				&& !detalle.getRFCCliente().equals(RFC_DEFAULT)){
			cliente = buscarCliente(detalle);
		}
		
		if(cliente == null){
			if(detalle.getTipoPersonaCliente().equals("PF")){
				cliente = crearCliente(detalle, resultado);
				List<ClienteGenericoDTO> resultadoClientes =  clienteFacadeRemote.findByIdRFC(cliente, null);
				if(resultadoClientes != null 
						&& !resultadoClientes.isEmpty()){
					cliente = resultadoClientes.get(0);
				}
				
				if (cliente.getIdCliente() == null){
					errorCotizacion = true;
				}
			}else{
				errorCotizacion = true;
				resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"Para personas morales es requerido el RFC de un cliente previamente dado de alta."));
			}
		}
		
		cotizacion.setNombreAsegurado(cliente.getNombreCompleto());
		cotizacion.setIdToPersonaAsegurado(cliente.getIdCliente());
		cotizacion.setIdDomicilioContratante((cliente.getIdDomicilioConsulta() != null)? BigDecimal.valueOf(cliente.getIdDomicilioConsulta()) : null);
		cotizacion.setIdToPersonaContratante(cliente.getIdCliente());
		cotizacion.setNombreContratante(cliente.getNombreCompleto());
		cotizacion.getSolicitudDTO().setClaveTipoPersona(cliente.getClaveTipoPersona());
		
		cotizacion = processor.guardarCotizacion(cotizacion);
		
		LOG.info("CMSP - construyendo el inciso...");
//		INCISO
		if(resultado.getListEstatusDescri() != null
				&& resultado.getListEstatusDescri().isEmpty()){
			
			
			IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
			IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
			incisoCotizacionId.setIdToCotizacion(cotizacion
					.getIdToCotizacion());
			incisoCotizacion.setCotizacionDTO(cotizacion);
			incisoCotizacion.setId(incisoCotizacionId);
			
			IncisoAutoCot incisoAutoCot = new IncisoAutoCot();
			
			LOG.info("CMSP - cargando negocioSeccion, tipoUso, estilo, modelo...");
//			NEGOCIO SECCION
			incisoAutoCot.setNegocioSeccionId(negocioSeccion
					.getIdToNegSeccion().longValue());
//			TIPO USO
			incisoAutoCot.setTipoUsoId(negocioTipoUso
					.getTipoUsoVehiculoDTO()
					.getIdTcTipoUsoVehiculo().longValue());
			
//			ESTILO
			EstiloVehiculoDTO estiloVehiculo = cargaMasivaService
					.obtieneEstiloVehiculoDTOPorClaveAMIS(cotizacion,
							negocioSeccion, detalle.getClaveAMIS());
			if (estiloVehiculo != null) {
				incisoAutoCot.setMarcaId(estiloVehiculo
						.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
				incisoAutoCot.setEstiloId(estiloVehiculo.getId()
						.getStrId());
				try{
					incisoAutoCot.setClaveTipoBien(estiloVehiculo.getId().getClaveTipoBien());
					incisoAutoCot.setIdVersionCarga(estiloVehiculo.getId().getIdVersionCarga());
					incisoAutoCot.setIdMoneda(cotizacion.getIdMoneda().shortValue());
				}catch(Exception e){
				}
				if(detalle.getDescripcionVehiculo() != null && !detalle.getDescripcionVehiculo().isEmpty()){
					incisoAutoCot.setDescripcionFinal(detalle.getDescripcionVehiculo().toUpperCase());
				}else{
					incisoAutoCot.setDescripcionFinal(estiloVehiculo.getDescripcionEstilo());
					detalle.setDescripcionVehiculo(estiloVehiculo.getDescripcionEstilo().toUpperCase());
				}
			}else{
				resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"Clave Amis Invalida"));
				cargaMasivaService.eliminaCotizacionError(cotizacion);
				return resultado;
			}
			
//			MODELO
			incisoAutoCot.setModeloVehiculo(detalle.getModelo().shortValue());
			incisoAutoCot.setNegocioPaqueteId(negocioPaqueteSeccion.getIdToNegPaqueteSeccion());
			
			LOG.info("CMSP - cargando ciudad, estado, max descuento...");
//			CIUDAD Y ESTADO
			String municipioId = cargaMasivaService.obtieneMunicipioIdPorCodigoPostal(detalle.getCodigoPostalCliente());
			String estadoId = "";
			if (municipioId != null) {
					incisoAutoCot.setMunicipioId(municipioId);
		
					estadoId = cargaMasivaService.obtieneEstadoIdPorMunicipio(municipioId);
					incisoAutoCot.setEstadoId(estadoId);
			}
			
//			OTORGAR EL MAXIMO DESCUENTO POR NEGOCIO-ESTADO PERMITIDO
			Double pctDescuentoEstado = 0.0;
			if(cargaMasivaService.getAplicaDescuentoNegocioPaqueteSeccion(incisoAutoCot.getNegocioPaqueteId())) {
				NegocioEstadoDescuento negocioEstadoDescuento = new NegocioEstadoDescuento();
				negocioEstadoDescuento = cargaMasivaService.findByNegocioAndEstado(
						cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio(), 
						incisoAutoCot.getEstadoId());
				if(negocioEstadoDescuento != null && negocioEstadoDescuento.getPctDescuentoDefault() != null) {
					pctDescuentoEstado = negocioEstadoDescuento.getPctDescuentoDefault();
				}
			}
			incisoAutoCot.setPctDescuentoEstado(pctDescuentoEstado);

			LOG.info("CMSP - cargando las coberturas del inciso...");
//			COBERTURAS
			List<DatoIncisoCotAuto> datoIncisoCotAutos = new ArrayList<DatoIncisoCotAuto>();
			
			List<CoberturaDetalleDTO> coberturasDetalle = new ArrayList<CoberturaDetalleDTO>();
			final int TIPO_DEDUCIBLE = 1;
			final int TIPO_SUMA_ASEGURADA = 2;
			final int TIPO_AMPARADAS = 3;
			
			incisoCotizacion.setIncisoAutoCot(incisoAutoCot);
			List<CoberturaCotizacionDTO> coberturaCotizacionList = cargaMasivaService
					.obtieneCoberturasDelInciso(incisoCotizacion,
							negocioPaqueteSeccion, estiloVehiculo.getId());
			
			Short claveContrato = 1;
			Short claveObligatoriedad = 0;
			Short valorSI = 1;
			Short valorNO = 0;
			
			if(coberturaCotizacionList != null && !coberturaCotizacionList.isEmpty()){
				
				for(CoberturaCotizacionDTO coberturaCotizacion : coberturaCotizacionList){
					
					Boolean esObligatoria = false;
	    			if(coberturaCotizacion.getClaveContrato().equals(claveContrato) && 
	    					coberturaCotizacion.getClaveObligatoriedad().equals(claveObligatoriedad)){
	    				esObligatoria = true;
	    			}
					
//					DANOS MATERIALES
					if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().toUpperCase().equals(
		    				ClaveCoberturas.COB_DA_MATERIALES.getClave())){
		    			if(detalle.getDeducibleDanosMateriales() != null){
		    				coberturaCotizacion.setValorDeducible(detalle.getDeducibleDanosMateriales());
	    					coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleDanosMateriales());
	    					coberturaCotizacion.setClaveContrato(claveContrato);
	    					coberturasDetalle.add(new CoberturaDetalleDTO(
	    							coberturaCotizacion.getId().getIdToCobertura().longValue(),
	    							detalle.getDeducibleDanosMateriales(),
	    							TIPO_DEDUCIBLE
	    							));
		    			}
					}
					else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().toUpperCase().equals(
		    				ClaveCoberturas.COB_ROBO_TOTAL.getClave())){
						if(detalle.getDeducibleRoboTotal() != null){
							coberturaCotizacion.setValorDeducible(detalle.getDeducibleRoboTotal());
	    					coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleRoboTotal());
	    					coberturaCotizacion.setClaveContrato(claveContrato);
	    					coberturasDetalle.add(new CoberturaDetalleDTO(
	    							coberturaCotizacion.getId().getIdToCobertura().longValue(),
	    							detalle.getDeducibleRoboTotal(),
	    							TIPO_DEDUCIBLE
	    							));
		    			}	
					}
					else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().toUpperCase().equals(
		    				ClaveCoberturas.COB_RES_CIVIL.getClave())){
						if(detalle.getLimiteRcTerceros() != null){
							coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteRcTerceros());
			    			coberturaCotizacion.setClaveContrato(claveContrato);
			    			coberturasDetalle.add(new CoberturaDetalleDTO(
	    							coberturaCotizacion.getId().getIdToCobertura().longValue(),
	    							detalle.getLimiteRcTerceros(),
	    							TIPO_SUMA_ASEGURADA
	    							));
		    			}
						if(detalle.getDeducibleRcTerceros() != null){
							coberturaCotizacion.setValorDeducible(detalle.getDeducibleRcTerceros());
			    			coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleRcTerceros());
			    			coberturaCotizacion.setClaveContrato(claveContrato);
			    			coberturasDetalle.add(new CoberturaDetalleDTO(
	    							coberturaCotizacion.getId().getIdToCobertura().longValue(),
	    							detalle.getDeducibleRcTerceros(),
	    							TIPO_DEDUCIBLE
	    							));
		    			}
						if(detalle.getLimiteRcTerceros() != null
								&& detalle.getDeducibleRcTerceros() == null){
							coberturaCotizacion.setValorDeducible(BigDecimal.ZERO.doubleValue());
			    			coberturaCotizacion.setPorcentajeDeducible(BigDecimal.ZERO.doubleValue());
			    			coberturaCotizacion.setClaveContrato(claveContrato);
			    			coberturasDetalle.add(new CoberturaDetalleDTO(
	    							coberturaCotizacion.getId().getIdToCobertura().longValue(),
	    							detalle.getDeducibleRCV(),
	    							TIPO_DEDUCIBLE
	    							));
						}
					}
					else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().toUpperCase().equals(
		    				ClaveCoberturas.COB_GASTOS_MEDICOS.getClave())){
						if(detalle.getLimiteGastosMedicos() != null){
							coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteGastosMedicos());
			    			coberturaCotizacion.setClaveContrato(claveContrato);
			    			coberturasDetalle.add(new CoberturaDetalleDTO(
	    							coberturaCotizacion.getId().getIdToCobertura().longValue(),
	    							detalle.getLimiteGastosMedicos(),
	    							TIPO_SUMA_ASEGURADA
	    							));
		    			}
					}
					else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().toUpperCase().equals(
		    				ClaveCoberturas.COB_ACCIDEN_AUTO_CONDUCTOR.getClave())){
						if(detalle.getLimiteMuerte() != null){
							coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteMuerte());
			    			coberturaCotizacion.setClaveContrato(claveContrato);
			    			coberturasDetalle.add(new CoberturaDetalleDTO(
	    							coberturaCotizacion.getId().getIdToCobertura().longValue(),
	    							detalle.getLimiteMuerte(),
	    							TIPO_SUMA_ASEGURADA
	    							));
		    			}else if(!esObligatoria){
			    				coberturaCotizacion.setClaveContrato(valorNO);
			    		}
					}
					else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().toUpperCase().equals(
		    				ClaveCoberturas.COB_RES_CIV_VIAJERO.getClave())){
						if(detalle.getLimiteRcViajero() != null){
							coberturaCotizacion.setDiasSalarioMinimo(detalle.getLimiteRcViajero().intValue());
			    			coberturaCotizacion.setClaveContrato(claveContrato);
			    			coberturasDetalle.add(new CoberturaDetalleDTO(
	    							coberturaCotizacion.getId().getIdToCobertura().longValue(),
	    							detalle.getLimiteRcViajero(),
	    							TIPO_SUMA_ASEGURADA
	    							));
		    			}
						if(detalle.getDeducibleRCV() != null){
							coberturaCotizacion.setValorDeducible(detalle.getDeducibleRCV());
			    			coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleRCV());
			    			coberturaCotizacion.setClaveContrato(claveContrato);
			    			coberturasDetalle.add(new CoberturaDetalleDTO(
	    							coberturaCotizacion.getId().getIdToCobertura().longValue(),
	    							detalle.getDeducibleRCV(),
	    							TIPO_DEDUCIBLE
	    							));
		    			}
						if(detalle.getLimiteRcViajero() != null
								&& detalle.getDeducibleRCV() == null){
							coberturaCotizacion.setValorDeducible(BigDecimal.ZERO.doubleValue());
			    			coberturaCotizacion.setPorcentajeDeducible(BigDecimal.ZERO.doubleValue());
			    			coberturaCotizacion.setClaveContrato(claveContrato);
			    			coberturasDetalle.add(new CoberturaDetalleDTO(
	    							coberturaCotizacion.getId().getIdToCobertura().longValue(),
	    							detalle.getDeducibleRCV(),
	    							TIPO_DEDUCIBLE
	    							));
						}
					}
					else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().toUpperCase().equals(
							ClaveCoberturas.COB_ASIS_JURIDICA.getClave())){
					    		if(detalle.getAsistenciaJuridica() != null){
					    			if(detalle.getAsistenciaJuridica().equals("SI")){
					    				coberturaCotizacion.setClaveContrato(claveContrato);
					    				coberturasDetalle.add(new CoberturaDetalleDTO(
				    							coberturaCotizacion.getId().getIdToCobertura().longValue(),
				    							null,
				    							TIPO_AMPARADAS
				    							));
					    			}else{
					    				if(!esObligatoria){
						    				coberturaCotizacion.setClaveContrato(valorNO);
						    			}
					    			}
					    		}
			 		}
					else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().toUpperCase().equals(
		    				ClaveCoberturas.COB_EQUIPO_ESPECIAL.getClave())){
						if(detalle.getLimiteEquipoEspecial() != null){
							coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteEquipoEspecial());
			    			coberturaCotizacion.setClaveContrato(claveContrato);
			    			coberturasDetalle.add(new CoberturaDetalleDTO(
	    							coberturaCotizacion.getId().getIdToCobertura().longValue(),
	    							detalle.getLimiteEquipoEspecial(),
	    							TIPO_SUMA_ASEGURADA
	    							));
		    			}
					}
					else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().toUpperCase().equals(
							ClaveCoberturas.COB_ASIS_VIAJES_VIAL.getClave())){
						if(detalle.getAsistenciaVial() != null){
			    			if(detalle.getAsistenciaVial().equals("SI")){
			    				coberturaCotizacion.setClaveContrato(claveContrato);
			    				coberturasDetalle.add(new CoberturaDetalleDTO(
		    							coberturaCotizacion.getId().getIdToCobertura().longValue(),
		    							null,
		    							TIPO_AMPARADAS
		    							));
			    			}else{
			    				if(!esObligatoria){
				    				coberturaCotizacion.setClaveContrato(valorNO);
				    			}
			    			}
			    		}
			 		}
					
					
				}
			}
			
			LOG.info("CMSP - cargando la informacion restante del inciso...");
//			INFORMACION RESTANTE DEL INCISO
			
//			Observaciones de inciso, la carga masiva de servicio publico no cuenta con esta informacion en el excel
//			incisoCotizacion.getIncisoAutoCot().setObservacionesinciso(detalle.getObservacionesInciso());
			

//			ASEGURADO
			if (detalle.getNombreRazonSocial() != null) {
				incisoCotizacion.getIncisoAutoCot().setNombreAsegurado(
						detalle.getNombreRazonSocial().toUpperCase());
				incisoCotizacion.getIncisoAutoCot().setPersonaAseguradoId(null);
			}

		
//			DATOS VEHICULO
			incisoCotizacion.getIncisoAutoCot().setNumeroMotor(
					detalle.getNumeroMotor());
			
//			NUMERO SERIE
			incisoCotizacion.getIncisoAutoCot().setNumeroSerie(detalle.getNumeroSerie());
			
			incisoCotizacion.getIncisoAutoCot().setPlaca(
					detalle.getPlacas());
			incisoCotizacion.getIncisoAutoCot().setRepuve(
					detalle.getNcRepuve());

			try{
				
				LOG.info("CMSP - guardando el inciso...");
				incisoCotizacion = processor.guardarIncisoCotizacion(incisoCotizacion, coberturaCotizacionList, datoIncisoCotAutos);
				LOG.info("CMSP - calculando el inciso...");
				incisoCotizacion = cargaMasivaService.calculoInciso(incisoCotizacion);
				
			}catch(Exception e){
				e.printStackTrace();
				incisoCotizacion = null;
			}
			try{
				LOG.info("CMSP - calculando el resumen de la cotizacion...");
				ResumenCostosDTO resumen = processor.calculaCotizacion(cotizacion);
				cotizacion.setValorPrimaTotal(new BigDecimal(resumen.getPrimaTotal()));
				LOG.info("CMSP - prima total excel: " + detalle.getPrimaTotal().longValue());
				LOG.info("CMSP - prima total calculada: " + resumen.getPrimaTotal());
//				IGUALACION
				
				if(coberturaCotizacionList != null && !coberturaCotizacionList.isEmpty()){
					for(CoberturaCotizacionDTO coberturaCotizacion : coberturaCotizacionList){
						coberturaCotizacion = entidadService.findById(CoberturaCotizacionDTO.class, coberturaCotizacion.getId());
						LOG.info("CMSP - cobertura: " + coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial() +
								" prima: " + coberturaCotizacion.getValorPrimaNeta());
					}
				}
				
				if(detalle.getPrimaTotal() != null
						&& cotizacion.getValorPrimaTotal().longValue() != detalle.getPrimaTotal().longValue()){
					if(detalle.getIgualacion() != null 
						&& detalle.getIgualacion().equals(new Long(valorSI))){
							LOG.info("CMSP - realizando igualacion de primas...");
							errorIgualacion = cargaMasivaServicioPublicoJobService.procesaIgualacion(
									cotizacion.getIdToCotizacion(), detalle.getPrimaTotal(), false, usuario);
							if(errorIgualacion){
								resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"Error al realizar la igualacion de la poliza"));
							}else{
								SolicitudExcepcionCotizacion solicitudCot =  buscarSolicitudAutorizacionIgualacion(cotizacion.getIdToCotizacion(), false);
								if(solicitudCot != null){
									requiereAutorizacion = true;
									resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.POR_AUTORIZAR.toString(),"Es necesario autorizar la igualacion."));
								}
							}
						}else{
							LOG.info("CMSP - igualando por tarifas...");
							errorTarifa = cargaMasivaServicioPublicoJobService.igualarTarifas(
									negocio.getIdToNegocio(), 
									negocioProducto.getProductoDTO().getIdToProducto(), 
									negocioTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza(), 
									negocioSeccion.getSeccionDTO().getIdToSeccion(), 
									negocioPaqueteSeccion.getPaquete().getId(),
									new Long(CargaMasivaServicioPublicoJobImpl.ID_MONEDA_NACIONAL_PESOS), 
									estadoId, municipioId, detalle.getVigencia(), detalle.getTotalPasajeros(),
									coberturasDetalle, detalle.getPrimaTotal());
							if(!errorTarifa){
								errorIgualacion = processor.procesaIgualacionTransaction(
										cotizacion.getIdToCotizacion(), detalle.getPrimaTotal(), false, usuario);
								if(errorIgualacion){
									resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"Error al realizar la igualacion por tarifas de la poliza. Prima calculada: " + resumen.getPrimaTotal()));
								}else{
									
									SolicitudExcepcionCotizacion solicitudCot =  buscarSolicitudAutorizacionIgualacion(cotizacion.getIdToCotizacion(), true);
									if(solicitudCot != null){
										solicitudAutorizacionService.terminarSolicitud(
												solicitudCot.getId(), solicitudCot.getDetalle(), usuarioService.getUsuarioActual().getId().longValue());
									}
								}
							}else{
								resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"No se pudo igualar por tarifas. Revisar las configuraciones de tarifas y de la cobertura. Prima calculada: " + resumen.getPrimaTotal()));
							}
						}
				}
				
			}catch(Exception e){
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
				resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"Ocurrio un error al generar la cotizacion."));
				errorCotizacion = true;
			}
			
			LOG.info("CMSP - terminando la cotizacion...");
			TerminarCotizacionDTO terminarCotizacionDTO = processor.terminaCotizacionTransaction(cotizacion);
			if(terminarCotizacionDTO != null){
				if(terminarCotizacionDTO.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES.getEstatus().shortValue()){
					if(detalle.getSolicitarAutorizacion() != null && detalle.getSolicitarAutorizacion().equals(Long.valueOf(valorSI))){
						Long idUsuario = usuarioService.getUsuarioActual().getId().longValue();
						SolicitudExcepcionCotizacion solicitudCot =  buscarSolicitudAutorizacionIgualacion(cotizacion.getIdToCotizacion(), false);
						Long idSolicitud = null;
						if(solicitudCot == null){
							idSolicitud = cargaMasivaService.solicitudAutorizacion(terminarCotizacionDTO.getExcepcionesList(), cotizacion.getIdToCotizacion(), idUsuario);
						}else{
							idSolicitud = solicitudCot.getId();
						}
						resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.POR_AUTORIZAR.toString(),"Solicitud Autorizacion: " + idSolicitud));
						requiereAutorizacion = true;
					}else{
						resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),terminarCotizacionDTO.getMensajeError()));
						
						List<ExcepcionSuscripcionReporteDTO> list = terminarCotizacionDTO.getExcepcionesList();
						StringBuilder mensajeError = new StringBuilder("");
						boolean isFirst = true;
						for(ExcepcionSuscripcionReporteDTO item : list){
							if(isFirst){
								isFirst = false;
							}else{
								mensajeError.append(", ");
							}
							mensajeError.append(item.getDescripcionExcepcion());
						}
						resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),mensajeError.toString() + ". Marcar SOLICITAR_AUTORIZACION en 1 en el excel para generar la(s) solicitud(es) correspondientes."));
						errorCotizacion = true;
					}
				}else if (terminarCotizacionDTO.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_SOLICITUDES.getEstatus().shortValue()){
					resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.POR_AUTORIZAR.toString(),
							"Esta cotización no puede ser terminada debido a que existen solicitudes de autorizacion pendientes. Favor de revisar la bandeja de solicitudes."));
				}else{
					resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),terminarCotizacionDTO.getMensajeError()));
				}
			}

		}//INCISO
		}catch(Exception ex){
			ex.printStackTrace();
			LOG.error(ex.getMessage(), ex);
			resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"Ocurrio un error al generar la cotizacion."));
			errorCotizacion = true;
		}
		boolean cotizacionEliminada = false;
		if(!requiereAutorizacion 
				|| errorIgualacion
				|| errorTarifa
				|| errorCotizacion){
			if (resultado.getListEstatusDescri() != null //VALIDANDO QUE HAYA ERROR EN EL RESULTADO
					&& !resultado.getListEstatusDescri().isEmpty()
					&& !obtenerMensajesResultado(EstatusDetalle.ERROR_COTIZACION.toString(), resultado).isEmpty() ) {
				//Elimina Cotizacion
				if(cotizacion != null && cotizacion.getIdToCotizacion() != null){
					LOG.info("CMSP - eliminando la cotizacion...");
					try{
						processor.eliminaCotizacionErrorTransaction(cotizacion);
					}catch(Exception ex){
						resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"Error al Generar la Cotizacion. \nError al eliminar la cotizacion erronea."));
						return resultado;
					}
					cotizacionEliminada = true;
				}
				resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"Error al Generar la Cotizacion. Cotizacion eliminada."));
			}
		}
		
		
		if(!cotizacionEliminada
				&& cotizacion != null
				&& cotizacion.getIdToCotizacion() != null){
			LOG.info("CMSP - retornando el numero de cotizacion...");
			resultado.setNumCotizacion(cotizacion.getIdToCotizacion().longValue());
		}
		
		return resultado;
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CargaMasivaServicioPublicoEstatusDescriDetalleDTO emitirCotizacion(CotizacionDTO cotizacion, CargaMasivaServicioPublicoEstatusDescriDetalleDTO resultado){
		try{
		LOG.info("CMSP - emitirCotizacion( cotizacion: " + cotizacion.getIdToCotizacion() + " )");
		Map<String, String> mensajeEmision = new HashMap<String, String>();
		
		LOG.info("CMSP - validando emision de la cotizacion...");
		TerminarCotizacionDTO validacion = cotizacionService.validarEmisionCotizacion(cotizacion.getIdToCotizacion());
		if(validacion != null){
			if(validacion.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_AGENTE.getEstatus().shortValue()){
				resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_POLIZA.toString(),"No es posible actualizar el registro"));
			}else if(validacion.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES.getEstatus().shortValue()){
				resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_POLIZA.toString(),validacion.getMensajeError() + obtenerMensajesExcepcionesEmision(validacion)));
			}else{
				resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_POLIZA.toString(),validacion.getMensajeError()));	
			}
		}else{
			LOG.info("CMSP - emisionService.emitir...");
			mensajeEmision = emisionService.emitir(cotizacion, false);
		}
		
		String idPolizaString = mensajeEmision.get("idpoliza");
		if(idPolizaString != null
				&& !idPolizaString.isEmpty()){
			LOG.info("CMSP - poliza generada: " + idPolizaString);
			resultado.setNumPoliza(Long.valueOf(idPolizaString));
		}
		}catch(Exception ex){
			resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_POLIZA.toString(),"Error al emitir la poliza"));
		}
		
		return resultado;
		
	}
	
	private SolicitudExcepcionCotizacion buscarSolicitudAutorizacionIgualacion(BigDecimal idToCotizacion, boolean autorizarDetalle){
		SolicitudExcepcionCotizacion solicitud = null;
		List<SolicitudExcepcionCotizacion> resultado = 
			entidadService.findByProperty(SolicitudExcepcionCotizacion.class, "toIdCotizacion", idToCotizacion);
		if(resultado != null
				&& !resultado.isEmpty()){
			for(SolicitudExcepcionCotizacion sol : resultado){
				if(sol.getDetalle() != null	
						&& !sol.getDetalle().isEmpty()){
					for(SolicitudExcepcionDetalle det : sol.getDetalle()){
						if(det.getDescripcion().equals("IGUALACION PRIMAS")){
							solicitud = sol;
							if(autorizarDetalle){
								det.setEstatus(SolicitudAutorizacionService.EstatusDetalleSolicitud.AUTORIZADA.valor());
							}
						}
						break;
					}
				}
				if(solicitud != null){
					break;
				}
			}
		}
		return solicitud;
	}
	
	private String obtenerMensajesExcepcionesEmision(TerminarCotizacionDTO validacion){
		StringBuilder mensajes = new StringBuilder();
		mensajes.append(" ( ");
		if(validacion != null
				&& validacion.getExcepcionesList() != null
				&& !validacion.getExcepcionesList().isEmpty()){
			for(ExcepcionSuscripcionReporteDTO excepcion: validacion.getExcepcionesList()){
				mensajes.append(excepcion.getDescripcionExcepcion());
				mensajes.append(", ");
			}
		}
		mensajes.delete(mensajes.length() -2, mensajes.length());
		mensajes.append(" ) ");
		return mensajes.toString();
	}
	
	private void enviarNotificacionCargaMasiva(CargaMasivaServicioPublico archivo){
		//ENVIO DE CORREO... 
		if(usuarioService.getUsuarioActual().getEmail() != null && usuarioService.getUsuarioActual().getEmail() != "" ){
			List<String> destinatarios = new ArrayList<String>();
			destinatarios.add(usuarioService.getUsuarioActual().getEmail());
			
			String title = "Resumen Carga Masiva Servicio P\u00FAblico.";
			String body = this.buildBody(archivo.getCargaMasivaSerPubDetalle());
			
			try {
				mailService.sendMail(destinatarios, title, body);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private String buildBody(List<CargaMasivaServicioPublicoDetalle> listEmisionDetalle){
		String result = "";
		
		StringBuilder body = new StringBuilder();
		body.append("Se realizo la carga masiva del archivo: ");
		body.append(listEmisionDetalle.get(0).getArchivoCargaMasivaSP_ID().getNombreArchivoCargaMasiva().toString());
		body.append("<br>Con el siguiente resumen: <br>");
		
		Iterator<CargaMasivaServicioPublicoDetalle> iterEmisionDetalle = listEmisionDetalle.iterator();
		while (iterEmisionDetalle.hasNext()) {
			CargaMasivaServicioPublicoDetalle item = (CargaMasivaServicioPublicoDetalle) iterEmisionDetalle.next();
			body.append("&emsp;P\u00F3liza:");
			if(item.getNumToPoliza() != null && item.getNumToPoliza() > 0){
				body.append(item.getNumToPoliza());
				body.append("<br>");
			}else{
				body.append("Sin N\u00FAmero de P\u00F3liza<br>");
			}
			body.append("&emsp;&emsp;" + item.getDescripcionEstatusProceso());
			body.append("<br>");
		}
		
		body.append("<br><br>Notificacion automatica generada por el sistema.");
		result = body.toString();
		
		return result;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public IncisoCotizacionDTO guardarIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion, 
			List<CoberturaCotizacionDTO> coberturaCotizacionList, List<DatoIncisoCotAuto> datoIncisoCotAutos){
		return cargaMasivaService.guardaIncisoCotizacion(incisoCotizacion,
				coberturaCotizacionList, datoIncisoCotAutos);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public CotizacionDTO guardarCotizacion(CotizacionDTO cotizacion){
		return cotizacionService.guardarCotizacion(cotizacion);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ResumenCostosDTO calculaCotizacion(CotizacionDTO cotizacion){
		return cargaMasivaService.calculaCotizacion(cotizacion);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean procesaIgualacionTransaction(BigDecimal idToCotizacion, Double primaTotal, Boolean restaurarDescuento, Usuario usuario){
		return cargaMasivaServicioPublicoJobService.procesaIgualacion(
				idToCotizacion, primaTotal, restaurarDescuento, usuario);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void eliminaCotizacionErrorTransaction(CotizacionDTO cotizacion){
		cargaMasivaService.eliminaCotizacionError(cotizacion);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public TerminarCotizacionDTO terminaCotizacionTransaction(CotizacionDTO cotizacion){ 
		return cargaMasivaService.terminaCotizacion(cotizacion);
	}
	
	private ClienteGenericoDTO buscarCliente(CargaMasivaServicioPublicoDetalle detalle){
		ClienteGenericoDTO clienteBusqueda = null;
		try{
		
			ClienteGenericoDTO filtroCliente = new ClienteGenericoDTO();
			filtroCliente.setCodigoRFC(detalle.getRFCCliente());
			List<ClienteGenericoDTO> resultadoClientes =  clienteFacadeRemote.findByIdRFC(filtroCliente, null);
		
			if(resultadoClientes != null 
					&& !resultadoClientes.isEmpty()){
				
				for (ClienteGenericoDTO cliente : resultadoClientes) {
					
					if (cliente.getIdDomicilioConsulta() != null && cliente.getCodigoPostalFiscal() != null
							&& !cliente.getCodigoPostalFiscal().equals("")) {
						
						if (((detalle.getColoniaCliente() == null || detalle.getColoniaCliente().equals(""))
								&& (detalle.getCodigoPostalCliente() == null || detalle.getCodigoPostalCliente().equals("")))
							|| ((detalle.getColoniaCliente() != null 
									&& detalle.getColoniaCliente().equals(cliente.getNombreColoniaFiscal().trim()))
								&&(detalle.getCodigoPostalCliente() != null 
											&& detalle.getCodigoPostalCliente().equals(cliente.getCodigoPostalFiscal())))) {
							clienteBusqueda = cliente;
							break;
						}
					}
				}
			}
		}catch(Exception ex){
			LOG.error(ex);
		}
		
		return clienteBusqueda;
	}
	
	/**
	 * Solo se utiliza para persona fisica debido a que no se tiene la informacion del representante legal
	 * y esta informacion es requerida para personas morales.
	 * @param detalle
	 * @param resultado
	 * @return
	 */
	private ClienteGenericoDTO crearCliente(CargaMasivaServicioPublicoDetalle detalle,  CargaMasivaServicioPublicoEstatusDescriDetalleDTO resultado){
		ClienteGenericoDTO cliente = new ClienteGenericoDTO();
		
		short claveTipoPersona = (detalle.getTipoPersonaCliente().equals("PF"))? 
				ClienteFacadeRemote.CLAVE_PERSONA_FISICA : ClienteFacadeRemote.CLAVE_PERSONA_MORAL;
		DecimalFormat decFormat = new DecimalFormat("00000");
		String postalCode = decFormat.format(Integer.parseInt(detalle.getCodigoPostalCliente().trim()));
		List<ColoniaMidas> listColonias = direccionMidasService.obtenerColoniasPorCP(postalCode);
		ColoniaMidas colonia = null;
		if(!listColonias.isEmpty()){
			colonia = listColonias.get(0);
		}
//		DATOS GENERALES
		cliente.setIdNegocio(detalle.getIdNegocio());
		
		cliente.setClaveTipoPersona(claveTipoPersona);
		cliente.setNombre(detalle.getNombreRazonSocial());
		
//		Si es RFC default (XAXX010101000 o 0) se usa Fecha Default (01/01/1970) 
		if(!detalle.getRFCCliente().equals("0")
				&& !detalle.getRFCCliente().equals(RFC_DEFAULT)){
			cliente.setFechaNacimiento(obtenerFechaNacimientoCliente(detalle.getRFCCliente(), resultado));
		}else{
			cliente.setFechaNacimiento(new DateTime(1970, 1, 1, 0, 0, 0, 0).toDate());
		}
		
		if(claveTipoPersona == ClienteFacadeRemote.CLAVE_PERSONA_MORAL){
			cliente.setRazonSocial(detalle.getNombreRazonSocial());
			cliente.setRazonSocialFiscal(detalle.getNombreRazonSocial());
			cliente.setFechaConstitucion(cliente.getFechaNacimiento());
			cliente.setNombreContacto(detalle.getNombreRazonSocial());
			cliente.setFechaNacimientoFiscal(cliente.getFechaConstitucion());
			cliente.setIdRepresentante(BigDecimal.ZERO); //En caso de que se quiera implementar para personas morales 
														//hay que poner un valor valido en esta variable
		}
		cliente.setApellidoPaterno(detalle.getApellidoPaterno());
		cliente.setApellidoMaterno(detalle.getApellidoMaterno());
		cliente.setSexo("M");
		
		cliente.setCodigoRFC(detalle.getRFCCliente());
		cliente.setTelefono(detalle.getTelefonoCliente());
		cliente.setEstadoCivil("S");
		
		cliente.setClaveNacionalidad(colonia.getCiudad().getEstado().getPais().getId());
		cliente.setClaveEstadoNacimiento(colonia.getCiudad().getEstado().getId());
		cliente.setIdEstadoString(colonia.getCiudad().getEstado().getId());
		cliente.setEstadoNacimiento(colonia.getCiudad().getEstado().getDescripcion());
		cliente.setClaveCiudadNacimiento(StringUtils.leftPad(colonia.getCiudad().getId(), 5, '0'));
		
		cliente.setNombreCalle(detalle.getCalleCliente());
		cliente.setCodigoPostal(detalle.getCodigoPostalCliente());
		cliente.setNombreColonia(detalle.getColoniaCliente());
		cliente.setIdColoniaString(detalle.getColoniaCliente());
		
		
		cliente.setIdMunicipioString(StringUtils.leftPad(colonia.getCiudad().getId(), 5, '0'));
		cliente.setTipoSituacionString("A");
		
//		DATOS FISCALES
		cliente.setCodigoRFCFiscal(detalle.getRFCCliente());
		cliente.setIdEstadoFiscal(colonia.getCiudad().getEstado().getId());
		cliente.setTelefonoFiscal(detalle.getTelefonoCliente());
		cliente.setCodigoPostalFiscal(detalle.getCodigoPostalCliente());
		cliente.setNombreColoniaFiscal(detalle.getColoniaCliente());
		cliente.setNombreCalleFiscal(detalle.getCalleCliente());
		cliente.setIdMunicipioFiscal(StringUtils.leftPad(colonia.getCiudad().getId(), 5, '0'));
		cliente.setNombreFiscal(detalle.getNombreRazonSocial());
		cliente.setApellidoPaternoFiscal(detalle.getApellidoPaterno());
		cliente.setApellidoMaternoFiscal(detalle.getApellidoMaterno());
		
		
		try{ 
			cliente = clienteFacadeRemote.saveFullData(cliente, null, true);
		}catch(Exception ex){
			LOG.error("Error al crear el cliente", ex);
			resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"Error al salvar el Cliente"));
		}
		
		
		return cliente;
	}
	
	private Date obtenerFechaNacimientoCliente(String RFC, CargaMasivaServicioPublicoEstatusDescriDetalleDTO resultado){
		Date fechaNacimientoDate = null;
		try{
			Matcher matcher = Pattern.compile("\\d+").matcher(RFC);
			matcher.find();
			String fechaRFC = matcher.group();
			
			Integer fechaAnio = Integer.valueOf(fechaRFC.substring(0, 2));
			Integer fechaMes = Integer.valueOf(fechaRFC.substring(2, 4));
			Integer fechaDia = Integer.valueOf(fechaRFC.substring(4, 6));
			
			int anioActual = DateTime.now().getYear();
			int anioActualCorto = anioActual % 100;
			fechaAnio = (fechaAnio <= anioActualCorto)? 
					anioActual - anioActualCorto + fechaAnio : 
						anioActual - anioActualCorto - 100 + fechaAnio; 
			
			fechaNacimientoDate = new DateTime(fechaAnio, fechaMes, fechaDia, 0, 0, 0, 0).toDate();
			
		}catch(Exception ex){
			LOG.error("Error al obtener la fecha de nacimiento del RFC, utilizando fecha default", ex);
//			resultado.getListEstatusDescri().add(resultado.new EstatusDescriDetalle(EstatusDetalle.ERROR_COTIZACION.toString(),"Error al salvar el Cliente(Fecha de Nacimiento RFC)"));
			fechaNacimientoDate = new DateTime(1970, 1, 1, 0, 0, 0, 0).toDate();
		}
		return fechaNacimientoDate;
	}
}
