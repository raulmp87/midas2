package mx.com.afirme.midas2.service.impl.bonos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.bonos.ConfigBonoExcepNegocioDao;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoExcepNegocio;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.service.bonos.ConfigBonoExcepNegocioService;

@Stateless
public class ConfigBonoExcepNegocioServiceImpl implements ConfigBonoExcepNegocioService{
	private ConfigBonoExcepNegocioDao dao;
	@EJB
	public void setConfigBonoExcepNegocioDao(ConfigBonoExcepNegocioDao dao) {
		this.dao = dao;
	}
	@Override
	public List<ConfigBonoExcepNegocio> loadByConfigBono(ConfigBonos configBonos) throws Exception {
		return dao.loadByConfigBono(configBonos);
	}
		
}
