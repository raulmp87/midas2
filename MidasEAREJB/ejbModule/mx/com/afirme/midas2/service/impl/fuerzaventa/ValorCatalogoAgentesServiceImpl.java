package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fuerzaventa.ValorCatalogoAgentesDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoCatalogoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class ValorCatalogoAgentesServiceImpl implements ValorCatalogoAgentesService{
	private ValorCatalogoAgentesDao dao;
	
	@Override
	public ValorCatalogoAgentes loadById(ValorCatalogoAgentes valorCatalogoAgentes)throws Exception{ 
		return dao.loadById(valorCatalogoAgentes);
	}
	/**
	 * Obtiene la lista de catalogos segun los filtros que se proporcionen
	 * @param filtro
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ValorCatalogoAgentes> findByFilters(ValorCatalogoAgentes filtro)throws Exception {
		return dao.findByFilters(filtro);
	}
	/**
	 * Obtiene la lista de catalogos segun los filtros que se proporcionen
	 * @param filtro
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ValorCatalogoAgentes> findByFiltersLike(ValorCatalogoAgentes filtro)throws Exception {
		return dao.findByFiltersLike(filtro);
	}
	/**
	 * Obtiene el elemento de un catalogo por su nombre de valor y del catalogo en el que se encuentra
	 * @param nombreCatalogo
	 * @param nombreElementoCatalogo
	 * @return
	 * @throws Exception
	 */
	@Override
	public ValorCatalogoAgentes obtenerElementoEspecifico(String nombreCatalogo,String nombreElemento) throws Exception {
		return dao.obtenerElementoEspecifico(nombreCatalogo, nombreElemento);
	}
	/**
	 * Obtiene la lista de elementos de un catalogo por su nombre
	 * @param nombreCatalogo es el nombre del grupo catalogo del cual se quieren obtener la lista de valores.
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ValorCatalogoAgentes> obtenerElementosPorCatalogo(String nombreCatalogo)throws Exception {
		return dao.obtenerElementosPorCatalogo(nombreCatalogo);
	}
	/**
	 * Obtiene el id de un elemento del catalogo especifico.
	 * @param nombreCatalogo
	 * @param nombreElementoCatalogo
	 * @return
	 * @throws Exception
	 */
	@Override
	public Long obtenerIdElementEspecifico(String nombreCatalogo, String nombreElemento)throws Exception {
		return dao.obtenerIdElementEspecifico(nombreCatalogo, nombreElemento);
	}
	/**
	 * Obtiene los catalogos por filtrado
	 */
	@Override
	public List<GrupoCatalogoAgente> catalogFindByFilters(GrupoCatalogoAgente filtro) throws Exception {
		return dao.catalogFindByFilters(filtro);
	}
	/**
	 * Obtiene un catalogo por el nombre del catalogo a obtener
	 */
	@Override
	public GrupoCatalogoAgente obtenerCatalogoPorDescripcion(String nombreCatalogo)throws Exception {
		return dao.obtenerCatalogoPorDescripcion(nombreCatalogo);
	}
	/**
	 * Obtiene el id del catalogo por su nombre
	 */
	@Override
	public Long obtenerIdCatalogoPorDescripcion(String descripcion) throws Exception {
		return dao.obtenerIdCatalogoPorDescripcion(descripcion);
	}
	
	public GrupoCatalogoAgente loadCatalogById(GrupoCatalogoAgente filtro) throws Exception{
		return dao.loadCatalogById(filtro);
	}
	/**
	 * ====================================================================================================
	 *										Sets & Gets
	 * ====================================================================================================
	 **/
	@EJB
	public void setDao(ValorCatalogoAgentesDao dao) {
		this.dao = dao;
	}
	
	@Override
	public List<ValorCatalogoAgentes> findByTipoCedula(ValorCatalogoAgentes tipoCedula) throws Exception {
		return dao.findByTipoCedula(tipoCedula);
	}
	@Override
	public String deleteTipoCedula(ValorCatalogoAgentes tipoCedula) throws Exception {		
		return dao.deleteTipoCedula(tipoCedula);
	}
	@Override
	public Long guardarElementoCatalogoEnSeycos(ValorCatalogoAgentes arg0) throws Exception {
		return dao.guardarElementoCatalogoEnSeycos(arg0);
	}
	@Override
	public Long eliminarElementoCatalogoEnSeycos(ValorCatalogoAgentes elemento) throws Exception{
		return dao.eliminarElementoCatalogoEnSeycos(elemento);
	}
	/**
	 * Obtiene la lista de elementos por un catalogo con opcion a excluir elementos
	 * @param nombreCatalogo
	 * @param excluirElementos
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ValorCatalogoAgentes> obtenerElementosPorCatalogo(String nombreCatalogo,String... excluirElementos) throws Exception{
		return dao.obtenerElementosPorCatalogo(nombreCatalogo,excluirElementos);
	}
	
	/**
	 * Obtiene la lista de elementos por un catalogo con opcion a obtener ciertos elementos
	 * @param nombreCatalogo
	 * @param elementosEspecificos
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ValorCatalogoAgentes> obtenerElementosEspecificosPorCatalogo(String nombreCatalogo,String... elementosEspecificos) throws Exception{
		return dao.obtenerElementosEspecificosPorCatalogo(nombreCatalogo,elementosEspecificos);
	}
}
