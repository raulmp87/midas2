package mx.com.afirme.midas2.service.impl.sapamis.sistemas;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.sapamis.sistemas.SapAmisSiniestrosDao;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisSiniestros;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.procesos.SapAmisDistpatcherWSService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisSiniestrosService;

/*******************************************************************************
 * Nombre Interface: 	SapAmisSiniestrosServiceImpl.
 * 
 * Descripcion: 		Se utiliza para el manejo del Modulo de Siniestros.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class SapAmisSiniestrosServiceImpl implements SapAmisSiniestrosService {
	private static final long serialVersionUID = 1L;

	@EJB private SapAmisSiniestrosDao sapAmisSiniestrosDao;
	@EJB private SapAmisUtilsService sapAmisUtilsService; 
	@EJB private SapAmisDistpatcherWSService sapAmisDistpatcherWSService;
    private static final int OP_ALTA = 1;
    private static final int OP_MODIFICACION = 2;
    private static final int OP_CANCELACION = 3;
    private static final Long ESTATUS_PENDIENTE = new Long(1);
    private static final int CANTIDAD_DE_REGISTROS_PROCESAR = 1000;
    private static final int INVOLUCRADO_ASEGURADO = 1;
    private static final int INVOLUCRADO_TERCERO = 2;

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis() {
		String[] accesos = sapAmisUtilsService.obtenerAccesos();
		this.sendRegSapAmis(accesos);
    }

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis(String[] accesos) {
		this.sendRegSapAmis(OP_ALTA, accesos);
		this.sendRegSapAmis(OP_MODIFICACION, accesos);
		this.sendRegSapAmis(OP_CANCELACION, accesos);
    }

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis(ParametrosConsulta parametrosConsulta){
		this.sendRegSapAmis(sapAmisUtilsService.obtenerAccesos(), parametrosConsulta);
	}

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis(String[] accesos, ParametrosConsulta parametrosConsulta){
    	List<SapAmisSiniestros> sapAmisSiniestrosList = this.obtenerPorFiltros(parametrosConsulta, CANTIDAD_DE_REGISTROS_PROCESAR, 1);
    	for(SapAmisSiniestros sapAmisSiniestros: sapAmisSiniestrosList){
    		if(sapAmisSiniestros.getInvolucradoSiniestro() != null && sapAmisSiniestros.getInvolucradoSiniestro().getId().intValue() == INVOLUCRADO_ASEGURADO){
    			sapAmisDistpatcherWSService.enviarSiniestro(sapAmisSiniestros, accesos);
    		}
    	}
    	for(SapAmisSiniestros sapAmisSiniestros: sapAmisSiniestrosList){
    		if(sapAmisSiniestros.getInvolucradoSiniestro() != null && sapAmisSiniestros.getInvolucradoSiniestro().getId().intValue() == INVOLUCRADO_TERCERO){
    			sapAmisDistpatcherWSService.enviarSiniestro(sapAmisSiniestros, accesos);
    		}
    	}
    }
    
    private void sendRegSapAmis(int operacion, String[] accesos) {
    	ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
    	parametrosConsulta.setEstatus(ESTATUS_PENDIENTE);
    	parametrosConsulta.setOperacion(new Long(operacion));
    	this.sendRegSapAmis(accesos, parametrosConsulta);
    }

	@Override
	public List<SapAmisSiniestros> obtenerPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		return sapAmisSiniestrosDao.obtenerPorFiltros(parametrosConsulta, numRegXPag, numPagina);
	}	
}