package mx.com.afirme.midas2.service.impl.calculos;

import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas2.dao.calculos.CalculoComisionesDao;
import mx.com.afirme.midas2.dao.calculos.CalculoComisionesDao.EstatusCalculoComisiones;
import mx.com.afirme.midas2.dao.calculos.DetalleCalculoComisionesDao;
import mx.com.afirme.midas2.domain.calculos.CalculoComisionEjecucion;
import mx.com.afirme.midas2.domain.calculos.CalculoComisiones;
import mx.com.afirme.midas2.domain.calculos.DetalleCalculoComisiones;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.comisiones.ConfigComisiones;
import mx.com.afirme.midas2.domain.sistema.parametro.ParametroGlobal;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.CalculoBonoEjecucionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.CalculoComisionesView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.calculos.CalculoComisionesService;
import mx.com.afirme.midas2.service.calculos.InterfazMizarService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.impl.sistema.parametro.ParametroGlobalServiceImpl;
import mx.com.afirme.midas2.service.impuestosestatales.RetencionImpuestosService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.threads.MailThreadSupport;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.MidasException;
import mx.com.afirme.midas2.utils.MailServiceSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class CalculoComisionesServiceImpl implements CalculoComisionesService{
	
	@Override
	public void deleteCalculoComisiones(Long arg0) throws MidasException {
		dao.deleteCalculoComisiones(arg0);
	}

	@Override
	public void deleteCalculoComisiones(CalculoComisiones arg0)throws MidasException {
		dao.deleteCalculoComisiones(arg0);
	}

	@Override
	public List<CalculoComisiones> findByFilters(CalculoComisiones arg0) {
		return dao.findByFilters(arg0);
	}

	@Override
	public Long generarCalculo(Long arg0,List<AgenteView> agentesDelCalculo,Long idCalculoTemporal,String queryAgentes) throws MidasException {
		Long idCalculo = null;
		idCalculo = dao.generarCalculo(arg0,agentesDelCalculo,idCalculoTemporal,queryAgentes);
		return idCalculo;
	}

	@Override
	public Long generarCalculo(ConfigComisiones arg0,List<AgenteView> agentesDelCalculo,Long idCalculoTemporal,String queryAgentes) throws MidasException {
		Long idCalculo = null;
		idCalculo = dao.generarCalculo(arg0,agentesDelCalculo,idCalculoTemporal,queryAgentes);
		return idCalculo;
	}
	
	/**
	 * Metodo para recalcular las comisiones por medio del id de calculo de comisiones
	 * @param idConfigComisiones
	 * @param agentesDelCalculo
	 * @param idCalculoComisiones
	 * @return
	 * @throws MidasException
	 */
	public Long recalcularCalculoComisiones(Long idConfigComisiones,List<AgenteView> agentesDelCalculo,Long idCalculoComisiones,Long idCalculoTemporal,String queryAgentes) throws MidasException{
		
		
		Long respuesta = dao.recalcularCalculoComisiones(idConfigComisiones,agentesDelCalculo,idCalculoComisiones,idCalculoTemporal,queryAgentes);
		
		CalculoComisiones calculo = entidadService.evictAndFindById(CalculoComisiones.class, idCalculoComisiones);
		
		if (calculo.getClaveEstatus().getValor().equals(EstatusCalculoComisiones.ERROR_PROCESAR.getValue())) {
			
			throw new MidasException(getMessage("midas.agente.comisiones.calculo.sinmovimientos"));
			
		}
		
		return respuesta;
		
	}
	
	@Override
	public List<AgenteView> cargarAgentesPorConfiguracion(Long idConfigComisiones) throws MidasException{
		return dao.cargarAgentesPorConfiguracion(idConfigComisiones);
	}

	@Override
	public CalculoComisiones loadById(Long arg0) throws MidasException {
		return dao.loadById(arg0);
	}

	@Override
	public CalculoComisiones loadById(CalculoComisiones arg0) throws MidasException {
		return dao.loadById(arg0);
	}

	@Override
	public Long saveCalculoComisiones(CalculoComisiones arg0) {
		return dao.saveCalculoComisiones(arg0);
	}
	@Override
	public List<AgenteView> obtenerAgentes(ConfigComisiones configuracion) throws Exception{
		return dao.obtenerAgentes(configuracion);
	}

	@Override
	public void autorizarPreview(CalculoComisiones calculo) {
				
		if (calculo == null || calculo.getId() == null) {
			
			throw new RuntimeException(getMessage("midas.agente.comisiones.autorizacion.calculo.vacio"));
			
		}
		
		calculo = entidadService.evictAndFindById(CalculoComisiones.class, calculo.getId());
		
		
		if (calculo.getClaveEstatus() == null || calculo.getClaveEstatus().getValor() == null 
				|| (!calculo.getClaveEstatus().getValor().equals(EstatusCalculoComisiones.PENDIENTE_AUTORIZAR.getValue())
						&& !calculo.getClaveEstatus().getValor().equals(EstatusCalculoComisiones.PROCESO_AUTORIZACION.getValue()))) {
			
			throw new RuntimeException(getMessage("midas.agente.comisiones.autorizacion.estatus.incorrecto"));
			
		}
		
		if(retencionImpuestosService.isCalculoRetencionImpuestosEnProceso()) {
			throw new RuntimeException(getMessage("midas.retencion.impuestos.calculo.enproceso"));
		}
		
		bloquearProcesoCalculoComisiones(calculo);
	
		try {
			
			Long idCalculo = calculo.getId();
			
			/*
			Si el estatus actual es 'EN PROCESO DE AUTORIZACION', saltarse todas las validaciones iniciales y 
			empezar a partir de la generacion de solicitudes de cheque.
			*/
			
			if (calculo.getClaveEstatus().getValor().equals(EstatusCalculoComisiones.PENDIENTE_AUTORIZAR.getValue())) {
				
				//Se cambia al estatus 'EN PROCESO DE AUTORIZACION' para marcar al calculo
				
				actualizarEstatus(calculo, EstatusCalculoComisiones.PROCESO_AUTORIZACION);
							
				//Se valida la autorización
				
				if (!esAutorizacionValida(idCalculo)) {
					
					actualizarEstatus(calculo, EstatusCalculoComisiones.PENDIENTE_RECALCULAR);
					
					throw new RuntimeException(getMessage("midas.agente.comisiones.autorizacion.movimiento.ocupado"));
					
				}
				
				//Se marcan los movimientos de agente con el id del calculo autorizado
				
				dao.marcaMovimientosAgente(idCalculo);
				
			}
						
			//Se crean las solicitudes de cheque en Seycos
			
			dao.generarSolicitudDeChequePorCalculo(idCalculo);
			
			//Se crean las solicitudes de cheque en Mizar
			
			generarSolicitudesChequeMizar(idCalculo);
			
			// Si todo salio OK, se cambia el estatus del preview a 'Autorizado'
			
			actualizarEstatus(calculo, EstatusCalculoComisiones.AUTORIZADO);
			
			//Se notifica por correo
			mailThreadMethodSupport(idCalculo,
					GenericMailService.P_PAGO_COMISIONES,
					GenericMailService.M_PGO_COMISIONES_AUTORIZACION_DEL_MOVIMIENTO, null,
					GenericMailService.T_GENERAL,"enviarCorreoAutorizacionMovimiento");
			
			
		} catch (RuntimeException e) {
			
			throw e;
			
		} catch (Exception e) {
			
			throw new RuntimeException(e.getMessage());
			
		} finally {

			desbloquearProcesoCalculoComisiones(calculo);

		}
		
	}
	
	/**
	 * Se cambia el estatus del calculo a cancelado siempre y cuando no se haya autorizado el calculo, una vez ya autorizado
	 * ya no podra cancelarse.
	 * @param idCalculo Clave del calculo a cancelar
	 * @throws Exception
	 */
	@Override
	public void cancelarCalculoPreview(CalculoComisiones calculo) throws Exception{
		
		if(isNull(calculo) || isNull(calculo.getId())){
			onError("Favor de proporcionar la clave del calculo que desea cancelar");
		}
		dao.actualizarEstatusCalculo(calculo.getId(), EstatusCalculoComisiones.CANCELADO);
		
	}
	/**
	 * indica si del listado de agentes alguno de ellos ya existe en algun calculo previo pendiente por autorizar, si es asi,
	 * entonces en cada calculo se va a cambiar de estatus a Pendiente-Recalcular, para que al momento de consultar un calculo si
	 * tiene este estatus se pida recalcular. 
	 * @param listaAgenteCalculo
	 * @return
	 * @throws MidasException
	 */
	public void validarAgentesEnCalculosPendientes(Long idCalculoTemporal)throws MidasException{
		dao.validarAgentesEnCalculosPendientes(idCalculoTemporal);
	}	
	/**
	 * Metodo para generar un reporte del calculo de comisiones en excel. 
	 * @param calculoCoisiones
	 * @throws Exception
	 */
	@Override
	public TransporteImpresionDTO generarReporteComisiones(CalculoComisiones calculoComisiones)
			throws Exception {
		return dao.generarReporteComisiones(calculoComisiones);
	}
	
	/**
	 * Muestra el listado de calculos que pueden pagarse a MIZAR. Estos deben de ser los unicos cheques 
	 * marcados en MIZAR como transferencias electronicas
	 * @param filtro Permite filtrar por fechas los calculso pendientes por pagar
	 * @return
	 */
	@Override
	public List<CalculoComisiones> obtenerCalculosPendientesPorPagar(CalculoComisiones filter) throws MidasException{
		return dao.obtenerCalculosPendientesPorPagar(filter);
	}
	
	/**
	 * Obtiene la lista de configuraciones programadas activas que tienen modo automatico.
	 * @return
	 * @throws MidasException
	 */
	@Override
	public List<ConfigComisiones> obtenerConfiguracionesAutomaticasActivas() throws MidasException{
		return dao.obtenerConfiguracionesAutomaticasActivas();
	}
	
	@Override
	public void mailThreadMethodSupport(Long id, Long idProceso,
			Long idMovimiento, String mensaje, int tipoTemplate, String methodToExcecute) {
		MailThreadSupport t = MailThreadSupport.getInstance();
		t.setService(this);
		t.setIdProceso(idProceso);
		t.setIdMovimiento(idMovimiento);
		t.setTipoTemplate(tipoTemplate);
		t.setId(id);
		t.setMethodToExcecute(methodToExcecute);
		Thread thread = new Thread(t);
		thread.setName("NotificacionCalculo" + id);
		thread.start();
	}

	@Override
	public synchronized void enviarCorreoAutorizacionMovimiento(Long idCalculo, Long idProceso,
			Long idMovimiento, String mensaje, int tipoTemplate) {
		CalculoComisiones calculo = dao.findById(CalculoComisiones.class,
				idCalculo);
		List<DetalleCalculoComisiones> detalleCalculo = dao.findByProperty(
				DetalleCalculoComisiones.class, "calculoComisiones.id",
				calculo.getId());
		// Notifica al administrador de la autorizacion
		List<ByteArrayAttachment> attachmentGeneral = null;
		TransporteImpresionDTO reporteGlobal = generarPlantillaReporte
				.imprimirRepGlobalComisionYbono(null,
						calculo.getFechaCalculo(), calculo.getId(),
						Boolean.FALSE);
		if (reporteGlobal != null && reporteGlobal.getByteArray() != null) {
			attachmentGeneral = new ArrayList<ByteArrayAttachment>();
			ByteArrayAttachment byteArrayAttachment = new ByteArrayAttachment();
			byteArrayAttachment
					.setTipoArchivo(ByteArrayAttachment.TipoArchivo.XLS);
			byteArrayAttachment.setContenidoArchivo(reporteGlobal
					.getByteArray());
			byteArrayAttachment
					.setNombreArchivo("reportePagoComisionesAgenteGlobal_calculo"
							+ calculo.getFechaCalculo() + ".xls");
			attachmentGeneral.add(byteArrayAttachment);
		}
		mailService.sendMailToAdminAgentes("AUTORIZACION PAGO DE COMISIONES", MailServiceSupport.mensajeAdminAgentePagoComisionesAutorizacion(calculo
				.getImporteTruncate().toString()), "AUTORIZACION PAGO DE COMISIONES", attachmentGeneral, null);
		// Envia la notificacion por agente
		for (DetalleCalculoComisiones detalle : detalleCalculo) {
			try {
			Map<String,Map<String, List<String>>> mapCorreos = agenteMidasService.obtenerCorreos(detalle
						.getAgente().getId(), idProceso, idMovimiento);
				if (mapCorreos != null && !mapCorreos.isEmpty()) {
					List<ByteArrayAttachment> attachment = null;
					 TransporteImpresionDTO reporte =  generarPlantillaReporte.imprimirRepGlobalComisionYbono(
								detalle.getAgente().getIdAgente(),
								calculo.getFechaCalculo(), calculo.getId(),
								Boolean.FALSE);
					if (reporte != null && reporte.getByteArray() != null) {
						attachment = new ArrayList<ByteArrayAttachment>();
						ByteArrayAttachment byteArrayAttachment = new ByteArrayAttachment();
						byteArrayAttachment.setTipoArchivo(ByteArrayAttachment.TipoArchivo.XLS);
						byteArrayAttachment.setContenidoArchivo(reporte.getByteArray());
						byteArrayAttachment.setNombreArchivo("reportePagoComisionesAgente_" + detalle.getAgente().getIdAgente()+".xls");
						attachment.add(byteArrayAttachment);
					}
					for (Entry<String, Map<String, List<String>>> map : mapCorreos
							.entrySet()) {
						StringBuilder message = new StringBuilder(MailServiceSupport
								.mensajePagoComisionesAutorizacionMovimiento(
										detalle.getAgente().getPersona()
												.getNombreCompleto(), detalle
												.getImporte().toString()));
						if (mapCorreos.get(map.getKey()).get(GenericMailService.NOTA) != null && StringUtils.isNotBlank(mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0))) {
							message.append(mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0));
						}
						mailService.sendMailAgenteNotificacion(
								mapCorreos.get(map.getKey()).get("Para"),
								mapCorreos.get(map.getKey()).get("C.C."),
								mapCorreos.get(map.getKey()).get("C.CO."),
								"PAGO DE COMISIONES", message.toString(), attachment,
								"PAGO DE COMISIONES", null,
								GenericMailService.T_GENERAL);
					}
				}
			} catch (RuntimeException error) {
				error.printStackTrace();
			}
		}
	}
	
	@Override
	public synchronized Map<String,Map<String, List<String>>> obtenerCorreos(Long id, Long idProceso,
			Long idMovimiento) {
		return agenteMidasService.obtenerCorreos(id, idProceso, idMovimiento);
	}

	@Override
	public synchronized void enviarCorreo(Map<String,Map<String, List<String>>> mapCorreos, String mensaje,String tituloMensaje,
			int tipoTemplate,List<ByteArrayAttachment> attachment) {
		if (mapCorreos != null && !mapCorreos.isEmpty()) {
			for (Entry<String, Map<String, List<String>>> map : mapCorreos
					.entrySet()) {
				mailService.sendMailAgenteNotificacion(
						mapCorreos.get(map.getKey()).get("Para"), mapCorreos
								.get(map.getKey()).get("C.C."),
						mapCorreos.get(map.getKey()).get("C.CO."), mensaje,
						null, null, "Notificacion", null, tipoTemplate);
			}
		}
	}
	
	public Long guardarAgentesEnTemporal(List<AgenteView> agentes,Long idCalculoTemporal) throws MidasException{
		return dao.guardarAgentesEnTemporal(agentes,idCalculoTemporal);
	}
	
	@Override
	public Long getNextIdCalculoTemporal() throws MidasException {
		return dao.getNextIdCalculoTemporal();
	}
	
	/**
	 * Obtiene el query para la lista de agentes de acuerdo a la configuracion.
	 * @param idConfigComisiones
	 * @return
	 * @throws MidasException
	 */
	public String cargarAgentesPorConfiguracionQuery(Long idConfigComisiones) throws MidasException{
		return dao.cargarAgentesPorConfiguracionQuery(idConfigComisiones);
	}

	@Override
	public void enviarCorreoGeneracionPreview(Long idCalculo, Long idProceso,
			Long idMovimiento, String mensaje, int tipoTemplate) {
		
		CalculoComisiones calculo = dao.findById(CalculoComisiones.class, idCalculo);
		List<ByteArrayAttachment> attachmentGeneral = null;
		TransporteImpresionDTO reporte;
		
		try {
			reporte = generarReporteComisiones(calculo);
		
		 
		if (reporte != null && reporte.getByteArray() != null) {
			attachmentGeneral = new ArrayList<ByteArrayAttachment>();
			ByteArrayAttachment byteArrayAttachment = new ByteArrayAttachment();
			byteArrayAttachment
					.setTipoArchivo(ByteArrayAttachment.TipoArchivo.XLS);
			byteArrayAttachment.setContenidoArchivo(reporte
					.getByteArray());
			byteArrayAttachment
					.setNombreArchivo("Reporte Calculo Comisiones "+darFormatoFecha(new Date())+".xls");
			attachmentGeneral.add(byteArrayAttachment);
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mailService.sendMailToAdminAgentes("GENERACION PREVIEW DE CALCULO DE COMISIONES",
				MailServiceSupport
						.mensajeAdminAgentePagoComisionesPreview(calculo
								.getImporteTotalString()),
				"GENERACION PREVIEW", attachmentGeneral,"PAGO_COMISIONES");
	}

	public void enviarNotificacionesGeneracionPreviews()    
	{//TODO Revisar el horario en que se debera ejecutar.
		logger.info("Ejecución de tarea enviarNotificacionesGeneracionPreviews...");
		ValorCatalogoAgentes estatusEjecucion = null;
		try {
			estatusEjecucion = catalogoService.obtenerElementoEspecifico("Estatus de Ejecucion Calculos Bono-Provision", "Terminado");
		} catch (Exception e1) {
			
			logger.error(e1);
		}
		
		Map<String,Object> params = new HashMap<String,Object>();	
		params.put("estatusEnvio", 0);
		params.put("estatusEjecucion", estatusEjecucion);
		List<CalculoComisionEjecucion> listaEjecucionesCalculoComision = entidadService.findByProperties(CalculoComisionEjecucion.class, params);
			
			for(CalculoComisionEjecucion ejecucion : listaEjecucionesCalculoComision)
			{
				try{   
					this.enviarCorreoGeneracionPreview(ejecucion.getIdCalculoComisiones(), GenericMailService.P_PAGO_COMISIONES, 
							GenericMailService.M_GENERACION_DE_PREVIEW, null, GenericMailService.T_GENERAL);
					
					ejecucion.setEstatusEnvio(1);
					entidadService.save(ejecucion);
					
				}catch(Exception e)
				{
					logger.error("Error en el proceso de envio de notificaciones de Generacion de Previews. Id Ejecucion:" 
							+ ejecucion.getId() + "Id Calculo: " + ejecucion.getIdCalculoComisiones(), e);
				}
			}					
	}

	@Override
	public List<CalculoComisionesView> findByFiltersView(CalculoComisiones filtro) {
		return dao.findByFiltersView(filtro);
	}

	@Override
	public Long eliminarComisionesCero(Long idCalculo) throws MidasException {
		return dao.EliminarComisionesCero(idCalculo);
	}
	
	@Override
	public List<CalculoBonoEjecucionesView> listaCalculoComisionesMonitorEjecucion() throws MidasException
	{
		return dao.listaCalculoComisionesMonitorEjecucion();
	}
	
	private String darFormatoFecha(Date fecha){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(fecha);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	private boolean esAutorizacionValida(Long idCalculo) {
		
		Integer numeroCalculosComisionPrevios  = dao.obtenerNumeroCalculosComisionPrevios(idCalculo);
		
		if (numeroCalculosComisionPrevios != null 
				&& numeroCalculosComisionPrevios.intValue() == 0) {
			
			return true;
			
		}
		
		return false;
		
	}
	
	private void generarSolicitudesChequeMizar(Long idCalculo) throws Exception {
		
		List<DetalleCalculoComisiones> detallesCalculo = null;
		Long idSolicitudCheque = null;
		
		detallesCalculo = detalleCalculoComisionesDao.listarDetalleDeCalculoNoAutorizado(idCalculo);
			
		for (DetalleCalculoComisiones detalleCalculo : detallesCalculo) {
			
			if (detalleCalculo == null || detalleCalculo.getId() == null) {
				
				continue;
				
			}
								
			idSolicitudCheque = detalleCalculo.getIdSolicitudCheque();
			
			if (idSolicitudCheque == null) {
				
				throw new RuntimeException(getMessage("midas.agente.comisiones.autorizacion.cheque.proceso.error", "SEYCOS"));
				
			}
						
			try {
				
				logger.info(getMessage("midas.agente.comisiones.autorizacion.cheque.proceso.detalle.inicio", idSolicitudCheque, idCalculo));
				
				interfazMizarService.preparaSolicitudChequeMizar(detalleCalculo);
								
			} catch (Exception e) {
				
				logger.error(getMessage("midas.agente.comisiones.autorizacion.cheque.proceso.detalle.error", idSolicitudCheque, idCalculo));
				
				actualizarDetalle(detalleCalculo, EstatusCalculoComisiones.ERROR_PROCESAR);
								
				throw new RuntimeException(getMessage("midas.agente.comisiones.autorizacion.cheque.proceso.error", "MIZAR"));
				
			}
			
			logger.info(getMessage("midas.agente.comisiones.autorizacion.cheque.proceso.detalle.fin", idSolicitudCheque, idCalculo));
			
			actualizarDetalle(detalleCalculo, EstatusCalculoComisiones.AUTORIZADO);
			
		}
		
		interfazMizarService.importarSolicitudesChequesAMizar(idCalculo);
	
	}
	
	private void bloquearProcesoCalculoComisiones(CalculoComisiones calculo) {
		
		if (calculo.isEnProceso()) {
			
			throw new RuntimeException(getMessage("midas.agente.comisiones.calculo.enproceso"));
			
		}
		
		calculo.setEnProceso(true);
		
		dao.saveCalculoComisiones(calculo);
		
	}
	
	private void desbloquearProcesoCalculoComisiones(CalculoComisiones calculo) {
					
		calculo.setEnProceso(false);
		
		dao.saveCalculoComisiones(calculo);
		
	}
	
	private String getMessage(String key, Object... params) {
		
		return Utilerias.getMensajeRecurso(
				sistemaContext.getArchivoRecursosBack(), key, params);
		
	}
	
	private void actualizarEstatus(CalculoComisiones calculo, EstatusCalculoComisiones estatus) throws Exception {
		
		ValorCatalogoAgentes claveEstatus = dao.obtenerClaveEstatusCalculo(estatus);
		
		calculo.setClaveEstatus(claveEstatus);
		
		dao.saveCalculoComisiones(calculo);
		
		
	}
	
	private void actualizarDetalle(DetalleCalculoComisiones detalleCalculo, EstatusCalculoComisiones estatus) throws Exception {
			
		ValorCatalogoAgentes claveEstatus = dao.obtenerClaveEstatusCalculo(estatus);
		
		detalleCalculo.setClaveEstatus(claveEstatus);
		
		detalleCalculoComisionesDao.saveDetalleCalculoComisiones(detalleCalculo);
	
	}
	
	public void initialize() {
			String timerInfo = "TimerNotificacionesGeneracionPreviews";
			cancelarTemporizador(timerInfo);
			iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.minute("*/5");
				expression.hour("*");
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerNotificacionesGeneracionPreviews", false));
				
				logger.info("Timer TimerNotificacionesGeneracionPreviews configurado");
			} catch (Exception e) {
				logger.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		logger.info("Cancelar Timers TimerNotificacionesGeneracionPreviews");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			logger.error("Error al detener Timer TimerNotificacionesGeneracionPreviews:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		enviarNotificacionesGeneracionPreviews();
	}
	
	
	@EJB
	private CalculoComisionesDao dao;
	
	@EJB
	private MailService mailService;
	
	@EJB
	private AgenteMidasService agenteMidasService;
	
	@EJB
	private GenerarPlantillaReporte generarPlantillaReporte;
	
	@EJB
	private InterfazMizarService interfazMizarService;
	
	@EJB
	private DetalleCalculoComisionesDao detalleCalculoComisionesDao;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private ValorCatalogoAgentesService catalogoService;
		
	private static Logger logger = Logger.getLogger(CalculoComisionesServiceImpl.class);
	
	@Resource	
	private TimerService timerService;
	
	@EJB
	private ParametroGlobalService parametroGlobalService;
	
	@EJB
	private RetencionImpuestosService retencionImpuestosService;
	
}
