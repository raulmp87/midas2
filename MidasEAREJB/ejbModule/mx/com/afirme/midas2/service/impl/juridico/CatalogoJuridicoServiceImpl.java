package mx.com.afirme.midas2.service.impl.juridico;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import mx.com.afirme.midas2.domain.juridico.CatalogoJuridico;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.juridico.OficinaJuridico;
import mx.com.afirme.midas2.dto.juridico.CatalogoJuridicoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.juridico.CatalogoJuridicoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

@Stateless
public class CatalogoJuridicoServiceImpl implements CatalogoJuridicoService{


	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	@EJB
	CatalogoGrupoValorService catalogoGrupoValorService;
	
	@EJB
	ListadoService  listadoService;



	@Override
	public void guardar(CatalogoJuridico catalogo) {
		catalogo.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
		catalogo.setFechaModificacion(new Date());
		if(null==catalogo.getId()){
			CatalogoJuridico filter=new CatalogoJuridico();
			filter.setEstatus(null);
			filter.setFechaCreacion(null);
			filter.setFechaModificacion(null);
			filter.setTipoCatalogo(catalogo.getTipoCatalogo());
			Long numero=this.entidadService.getIncrementedProperty(CatalogoJuridico.class, "numero", filter);
			catalogo.setNumero(numero);
			catalogo.setFechaCreacion(new Date());
			catalogo.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
		}
		
		if (catalogo.getEstatus() ){
			catalogo.setFechaActivo(new Date());
			catalogo.setFechaInactivo(null);
		}else {
			catalogo.setFechaInactivo(new Date());

		}
			
		
		this.entidadService.save(catalogo);
	}



	@Override
	public void guardarDelegacion(CatalogoJuridicoDTO catalogo) {
		catalogo.setTipoCatalogo(CatalogoJuridicoService.TIPO_CAT_JURIDICO_DELEGACION);
		CatalogoJuridico catJuridico = this.getCatalogoJuridico(catalogo );
		if(null!=catJuridico){
			this.guardar(catJuridico);			
		}	
	}
	
	@Override
	public CatalogoJuridico getCatalogoJuridico(CatalogoJuridicoDTO catalogo){
		CatalogoJuridico catJuridico =null;
		if(null!=catalogo.getId()){
			catJuridico=this.entidadService.findById(CatalogoJuridico.class, catalogo.getId());
		}else{
			catJuridico =new CatalogoJuridico();
		}
		
		if (!StringUtil.isEmpty(catalogo.getEstatus()) ){
			if(catalogo.getEstatus().equalsIgnoreCase(""+CatalogoJuridicoService.ESTATUS_CAT_JURIDICO_ACTIVO)){
				catJuridico.setEstatus(true);
			}else if(catalogo.getEstatus().equalsIgnoreCase(""+CatalogoJuridicoService.ESTATUS_CAT_JURIDICO_INACTIVO)){
				catJuridico.setEstatus(false);
			}
		}
		catJuridico.setFechaActivo(catalogo.getFechaActivo());
		catJuridico.setFechaInactivo(catalogo.getFechaInactivo());
		
		OficinaJuridico oficinaJuridico= this.entidadService.findById(OficinaJuridico.class, new Long (catalogo.getIdOficina()));
		if(null!=oficinaJuridico){
			catJuridico.setOficinaJuridico(oficinaJuridico);
		}
		catJuridico.setId(catalogo.getId());
		catJuridico.setNombre(catalogo.getNombre());
		catJuridico.setNumero(catalogo.getNumero());
		catJuridico.setTipoCatalogo(catalogo.getTipoCatalogo());
		return catJuridico;
	}


	@Override
	public void guardarMotivoReclamacion(CatalogoJuridicoDTO catalogo) {
		catalogo.setTipoCatalogo(CatalogoJuridicoService.TIPO_CAT_JURIDICO_MOTIVOS);
		CatalogoJuridico catJuridico = this.getCatalogoJuridico(catalogo );
		if(null!=catJuridico){
			this.guardar(catJuridico);			
		}
	}



	@Override
	public void guardarProcedimiento(CatalogoJuridicoDTO catalogo) {
		catalogo.setTipoCatalogo(CatalogoJuridicoService.TIPO_CAT_JURIDICO_PROCEDIMIENTO);
		CatalogoJuridico catJuridico = this.getCatalogoJuridico(catalogo);
		if(null!=catJuridico){
			this.guardar(catJuridico);			
		}		
	}



	@Override
	public CatalogoJuridico obtener(CatalogoJuridico filtro) {
		CatalogoJuridico catalogoJuridico= new CatalogoJuridico ();
		List<CatalogoJuridico>  listaCatalogoJuridico =this.obtenerListado(filtro);
		if(null!=listaCatalogoJuridico && !listaCatalogoJuridico.isEmpty()){
			return listaCatalogoJuridico.get(0);
		}
		return catalogoJuridico;
	}



	@Override
	public List<CatalogoJuridicoDTO> obtenerDelegaciones(CatalogoJuridico filtro) {
		
		List<CatalogoJuridicoDTO>  listaDto = new ArrayList<CatalogoJuridicoDTO>();
		filtro.setTipoCatalogo( CatalogoJuridicoService.TIPO_CAT_JURIDICO_DELEGACION);		
		List<CatalogoJuridico>  listaCatalogoJuridico =this.obtenerListado(filtro);		
		for (CatalogoJuridico catalogo : listaCatalogoJuridico){
			CatalogoJuridicoDTO dto=this.getCatalogoFiltro(catalogo);
			if(null!=dto){
				listaDto.add(dto);
			}
		}
		return listaDto;
	}

	@Override
	public CatalogoJuridicoDTO getCatalogoFiltro(CatalogoJuridico catalogo){
		CatalogoJuridicoDTO catalogoFiltro= new CatalogoJuridicoDTO ();
		CatValorFijo valorEstatus=null;
		//VALOR ESTATUS
		if(null!=catalogo.getEstatus()){
			

			if(catalogo.getEstatus()){
				valorEstatus=	this.catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_CAT_JURIDICO, ""+CatalogoJuridicoService.ESTATUS_CAT_JURIDICO_ACTIVO);
				catalogoFiltro.setEstatus(""+CatalogoJuridicoService.ESTATUS_CAT_JURIDICO_ACTIVO);
			}else if(false == catalogo.getEstatus()){
				valorEstatus=	this.catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_CAT_JURIDICO, ""+CatalogoJuridicoService.ESTATUS_CAT_JURIDICO_INACTIVO);
				catalogoFiltro.setEstatus(""+CatalogoJuridicoService.ESTATUS_CAT_JURIDICO_INACTIVO);
			}
			if(null!=valorEstatus){
				catalogoFiltro.setEstatusDesc(valorEstatus.getDescripcion());
			}
		}
		
		if(null!=catalogo.getFechaActivo()){
			catalogoFiltro.setFechaActivo(catalogo.getFechaActivo());		
			catalogoFiltro.setFechaActivoStr(Utilerias.cadenaDeFecha(catalogo.getFechaActivo(),"dd/MM/yyyy"));
		}
		
		if(null!=catalogo.getFechaInactivo()){
			catalogoFiltro.setFechaInactivo(catalogo.getFechaInactivo());
			catalogoFiltro.setFechaInactivoStr(Utilerias.cadenaDeFecha(catalogo.getFechaInactivo(),"dd/MM/yyyy"));
		}
		
		
		
		catalogoFiltro.setId(catalogo.getId());
		
		if(null!=catalogo.getOficinaJuridico()){
			catalogoFiltro.setIdOficina(catalogo.getOficinaJuridico().getId());
			if(!StringUtil.isEmpty(catalogo.getOficinaJuridico().getNombreOficina())){
				catalogoFiltro.setNombreOficina(catalogo.getOficinaJuridico().getNombreOficina());
			}
		}
		catalogoFiltro.setNombre(catalogo.getNombre());
		catalogoFiltro.setNumero(catalogo.getNumero());
		catalogoFiltro.setTipoCatalogo(catalogo.getTipoCatalogo());
		
		
		return catalogoFiltro;
	}
	
	
	@Override
	public List<CatalogoJuridico> obtenerListado(CatalogoJuridico filtro) {
		List<CatalogoJuridico>  listado=this.entidadService.findByFilterObject(CatalogoJuridico.class, filtro, "numero");
		return listado;
	}



	@Override
	public List<CatalogoJuridicoDTO> obtenerMotivosReclamacion(
			CatalogoJuridico filtro) {
		
		List<CatalogoJuridicoDTO>  listaDto = new ArrayList<CatalogoJuridicoDTO>();
		Map<String, Object> parameters = new HashMap<String, Object>();	
		parameters.put("tipoCatalogo", CatalogoJuridicoService.TIPO_CAT_JURIDICO_MOTIVOS);
		List<CatalogoJuridico>  listaCatalogoJuridico =entidadService.findByProperties(CatalogoJuridico.class, parameters);
		for (CatalogoJuridico catalogo : listaCatalogoJuridico){
			CatalogoJuridicoDTO dto=this.getCatalogoFiltro(catalogo);
			if(null!=dto){
				listaDto.add(dto);
			}
		
		}
		
		return listaDto;
	}



	@Override
	public List<CatalogoJuridicoDTO> obtenerProcedimientos(CatalogoJuridico filtro) {
		List<CatalogoJuridicoDTO>  listaDto = new ArrayList<CatalogoJuridicoDTO>();
		filtro.setTipoCatalogo(CatalogoJuridicoService.TIPO_CAT_JURIDICO_PROCEDIMIENTO);
		List<CatalogoJuridico>  listaCatalogoJuridico =this.obtenerListado(filtro);		
		for (CatalogoJuridico catalogo : listaCatalogoJuridico){
			CatalogoJuridicoDTO dto=this.getCatalogoFiltro(catalogo);
			if(null!=dto){
				listaDto.add(dto);
			}
		
		}
		return listaDto;
	}
	

	
	
}
