package mx.com.afirme.midas2.service.impl.siniestros.notificaciones;

import java.io.Serializable;
import java.util.HashMap;

import mx.com.afirme.midas2.exeption.NegocioEJBExeption;

import org.apache.log4j.Logger;

public class Data implements Serializable {
	public static final Logger log = Logger.getLogger(Data.class);
	private static final long serialVersionUID = 1L;

	private HashMap<String, Serializable> dataMap = new HashMap<String, Serializable>();

	public HashMap<String, Serializable> getDataMap() {
		return dataMap;
	}

	public Data(HashMap<String, Serializable> dataMap) {
		this.dataMap = dataMap;
	}

	/**
	 * Funcion para obtener informacion de el HashMap, validando que existe y
	 * validando el casteo (evitar runtime exceptions de casteos).
	 * 
	 * despues validar "a mano" que el return no sea null para evitar
	 * nullpointer exceptions
	 * 
	 * @param key
	 * @param clase
	 * @return
	 */
	@SuppressWarnings("unchecked")
	<K> K obtener(String key, Class<K> clase) throws NegocioEJBExeption {
		if (key == null || clase == null) {
			throw new NegocioEJBExeption("ENV_NOT_MAP.0",
					"Parametros key y clase no deben de venir nulls");
		}
		K objeto = null;
		try {
			objeto = (K) dataMap.get(key);
			if (objeto == null) {
				throw new NegocioEJBExeption(
						"ENV_NOT_MAP.1",
						"Es requerido agregar "
								+ key
								+ "diferente de null al mapa para enviar notificaciones");
			}
		} catch (ClassCastException e) {
			throw new NegocioEJBExeption("ENV_NOT_MAP.2",
					"Es requerido agregar " + key
							+ " al mapa te tipo de clase: " + clase.getName());

		} catch (Exception e) {
			throw new NegocioEJBExeption("ENV_NOT_MAP.3",
					"Es requerido agregar " + key
							+ " al mapa para enviar notificaciones");
		}
		return objeto;
	}
}
