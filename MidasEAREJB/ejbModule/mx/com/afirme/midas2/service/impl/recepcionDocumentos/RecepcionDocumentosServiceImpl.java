package mx.com.afirme.midas2.service.impl.recepcionDocumentos;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;

import mx.com.afirme.midas2.dao.recepcionDocumentos.RecepcionDocumentosDao;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.domain.recepcionDocumentos.RecepcionDocumentos;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.service.recepcionDocumentos.RecepcionDocumentosService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MidasException;

@Stateless
public class RecepcionDocumentosServiceImpl implements RecepcionDocumentosService{

	private RecepcionDocumentosDao recepDao;	
	@Resource	
	private TimerService timerService;	
	@EJB
	private SistemaContext sistemaContext;
	public static final Logger LOG = Logger.getLogger(RecepcionDocumentosServiceImpl.class);
	
	@EJB
	public void setRecepDao(RecepcionDocumentosDao recepDao) {
		this.recepDao = recepDao;
	}
	
	@Override
	public void actualizarStatusRecibos(DocumentosAgrupados docsAgrupados, Integer mesInicio, Integer mesFin)throws Exception{
		recepDao.actualizarStatusRecibos(docsAgrupados, mesInicio, mesFin);		
	}

	@Override
	public void saveRechazoDocumentos(RecepcionDocumentos recepDoc, Integer mesInicio, Integer mesFin)throws MidasException {
		recepDao.saveRechazoDocumentos(recepDoc, mesInicio, mesFin);		
	}	
		
	@Override
	public List<EntregoDocumentosView> listaFacturasMesAnio(Long anioMes, Long idAgente) throws Exception {
		// TODO Auto-generated method stub
		return recepDao.listaFacturasMesAnio(anioMes, idAgente);
	}
	
	@Override
	public void generaFacturaAgenteComis_job() throws Exception {
		LOG.info("Iniciando tarea generaFacturaAgenteComis_job");
		recepDao.generaFacturaAgenteComis_job();
		LOG.info("Tarea generaFacturaAgenteComis_job ejecutada");
	}
	
	public void initialize() {
		String timerInfo = "TimerGeneraFacturaComisionAgte";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0 2 * * ?
				expression.minute(0);
				expression.hour(2);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerGeneraFacturaComisionAgte", false));
				
				LOG.info("Tarea TimerGeneraFacturaComisionAgte configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerGeneraFacturaComisionAgte");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerGeneraFacturaComisionAgte:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		try {
			generaFacturaAgenteComis_job();
		} catch(Exception e) {
			LOG.error("Error al ejecutar tarea TimerGeneraFacturaComisionAgte", e);
		}
		
	}

}
