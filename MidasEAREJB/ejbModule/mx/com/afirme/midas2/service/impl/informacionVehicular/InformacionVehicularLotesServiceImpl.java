package mx.com.afirme.midas2.service.impl.informacionVehicular;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoEquivalenciaDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.cesvi.vinplus.Respuesta;
import mx.com.afirme.midas2.domain.poliza.diferenciasamis.DiferenciasAmis;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.datosAmis.DatosAmisService;
import mx.com.afirme.midas2.service.diferenciasAmis.DiferenciasAmisService;
import mx.com.afirme.midas2.service.fronterizos.FronterizosService;
import mx.com.afirme.midas2.service.informacionVehicular.InformacionVehicularLotesService;
import mx.com.afirme.midas2.service.informacionVehicular.InformacionVehicularService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.wsClient.cesvi.lotes.VinplusBindingStub;
import mx.com.afirme.midas2.wsClient.cesvi.lotes.VinplusLocator;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.log4j.Logger;

@Stateless
public class InformacionVehicularLotesServiceImpl implements InformacionVehicularLotesService{
	
	@EJB
	protected EstiloVehiculoFacadeRemote estiloVehiculoFacadeRemote;
	
	@EJB
	private DiferenciasAmisService diferenciasAmisService;

	@EJB
	protected InformacionVehicularService informacionVehicularService;	

	@EJB
	private DatosAmisService datosAmisService;

	@EJB
	protected ParametroGeneralService parametroGeneralService;
	
	private UsuarioService usuarioService;
	
	@EJB(beanName="UsuarioServiceDelegate")
    public void setUsuarioService(UsuarioService usuarioService) {
          this.usuarioService = usuarioService;
    }
	
	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private FronterizosService fronterizosService;

	@EJB
	protected EntidadService entidadService;
	
	public static final String SEPARADOR_VEHICULO = ",\\|";
	public static final String SEPARADOR_VEHICULO_CARACTERISTICAS = ",";
	public static final int  LIMITE_VIN = 3500;
	public static final int TIEMPO_ESPERA = 60000 * 60;
	public static final String ENVIO_TERMINADO = "1";

	private static final Logger LOG = Logger.getLogger(InformacionVehicularLotesServiceImpl.class);

	private void getConsultaVin(byte[] vin, String nombreLote) {
        try {
            VinplusLocator locator = new VinplusLocator();
            VinplusBindingStub service = (VinplusBindingStub)locator.getvinplusPort(sistemaContext.getCesviFlotillasEndpoint());
            service.consultaVin(nombreLote, vin);
        } catch (Exception e) {
            LOG.error("Información Vehicular Lotes getConsultaVin:", e);
        }
    }

	private byte[] getDescargaVin(String nombreLote) {
		byte[] vinLote = null;
        try {
            VinplusLocator locator = new VinplusLocator();
            VinplusBindingStub service = (VinplusBindingStub)locator.getvinplusPort(sistemaContext.getCesviFlotillasEndpoint());
            vinLote = service.decargaLote(nombreLote);
        } catch (Exception e) {
            LOG.error("Informacion Vehicular Lotes getDescargaVin:", e);
        }
        return vinLote;       
    }	
	
	private String getConsultaLote(String nombreLote) {
		String consultaLote = null;
        try {
            VinplusLocator locator = new VinplusLocator();
            VinplusBindingStub service = (VinplusBindingStub)locator.getvinplusPort(sistemaContext.getCesviFlotillasEndpoint());
            consultaLote = service.consultaLote(nombreLote);
        } catch (Exception e) {
            LOG.error("Informacion Vehicular Lotes getConsultaLote:", e);
        }
        return consultaLote;       
    }	
	
	public String envioLoteTerminado(String nombreLote) {
		return this.getConsultaLote(nombreLote);
	}
	
	private void validaInformacionVehicular(List<Respuesta> vehiculosEmitidos, List<Respuesta> listaVehiculosCesvi) {
		Usuario usuario = usuarioService.getUsuarioActual();
		String mensaje = "";

		Map<Long, String> datosAmis = datosAmisService.getListarDatos();
		
		for (Respuesta vehiculoEmitido : vehiculosEmitidos) {
			if(vehiculoEmitido.getClaveTipoSolicitud() == 0 || vehiculoEmitido.getClaveTipoSolicitud() == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO ||
						vehiculoEmitido.getClaveTipoSolicitud() == SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS) {
					LinkedList<DiferenciasAmis> listaDiferencias = new LinkedList<DiferenciasAmis>();
					List<Respuesta> vehiculosCesvi = this.getVehiculosCesviFiltrados(listaVehiculosCesvi, vehiculoEmitido.getVIN());
					
					diferenciasAmisService.eliminarDiferenciasAmisPorInciso(vehiculoEmitido.getIdToPoliza(), vehiculoEmitido.getNumeroInciso());
					
					if(vehiculosCesvi != null && vehiculosCesvi.size() != 0) {

						listaDiferencias= this.getDiferenciasInformacionVehicular(vehiculosCesvi, vehiculoEmitido, datosAmis);
						boolean vinCorrecto = false;
						if(listaDiferencias != null && listaDiferencias.size() > 0) {
							//Guardar diferencias entre el inciso y cada uno de los vehiculos de cesvi
							for (DiferenciasAmis diferencias :listaDiferencias) {
								if(diferencias.getClaveError() != 0) {
									if(usuario != null && usuario.getNombre() != null) {
										diferencias.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
									}
									diferenciasAmisService.guardarDiferenciasAmis(diferencias);
								} else {
									vinCorrecto = true;
									break;
								}
							}
							
							if(!vinCorrecto){
								Collections.sort(listaDiferencias, new Comparator<DiferenciasAmis>() {
									@Override
									public int compare(DiferenciasAmis o1, DiferenciasAmis o2) {
										return o1.getNumeroDiferencias().compareTo(o2.getNumeroDiferencias());
									}
								});
								
								if(listaDiferencias.getFirst().getMensajeError() != null) {
									mensaje = listaDiferencias.getFirst().getMensajeError();
								}
								
								//Obtener mensaje con diferencias entre el inciso y el vehiculo mas parecido de cesvi
								if(listaDiferencias.getFirst().getClaveError() == 1 && !mensaje.isEmpty()) {
									mensaje = informacionVehicularService.getMensaje(1, vehiculoEmitido.getVIN(), " [" + listaDiferencias.getFirst().getMensajeError() + "].");
								}
							}
						}
					} else {
						//Si no se encontro informacion del numero de serie, guardar error
						mensaje = informacionVehicularService.getMensaje(DiferenciasAmis.ERROR_VIN_NO_ENCONTRADO, vehiculoEmitido.getVIN(), "");
						DiferenciasAmis diferenciasAmis = new DiferenciasAmis();
						diferenciasAmis = this.poblarDiferencias(diferenciasAmis, vehiculoEmitido);
						diferenciasAmis.setClaveError(DiferenciasAmis.ERROR_VIN_NO_ENCONTRADO);
						diferenciasAmis.setMensajeError(mensaje);
						if(usuario != null && usuario.getNombre() != null) {
							diferenciasAmis.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
						}
						diferenciasAmisService.guardarDiferenciasAmis(diferenciasAmis);
					}
					
					//Guardar mensaje de error para el inciso
					if(mensaje != null && !mensaje.isEmpty()) {
						vehiculoEmitido.setObservacionesSesa(mensaje);
						this.guardarObservacionesSesa(vehiculoEmitido);
					}
					
					try {
						fronterizosService.actualizarClaves(vehiculoEmitido.getCVE_AMIS(), vehiculoEmitido.getCVE_SESA(), vehiculoEmitido.getEstiloId());
					} catch (Exception e) {
						LOG.error("Ocurrio una excepcion al actualizar claves amis y sesas"+e);
					}
					
			} else if(vehiculoEmitido.getClaveTipoSolicitud() == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO ||
						vehiculoEmitido.getClaveTipoSolicitud() == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA) {
					diferenciasAmisService.cambiarEstatusPorInciso(vehiculoEmitido.getIdToPoliza(), DiferenciasAmis.ESTATUS_CANCELADA, vehiculoEmitido.getNumeroInciso());
			} else if (vehiculoEmitido.getClaveTipoSolicitud() == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS ||
						vehiculoEmitido.getClaveTipoSolicitud() == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA){
					diferenciasAmisService.cambiarEstatusPorInciso(vehiculoEmitido.getIdToPoliza(), DiferenciasAmis.ESTATUS_PENDIENTE_REVISION, vehiculoEmitido.getNumeroInciso());
			}
		}
	}
	

	private LinkedList<DiferenciasAmis> getDiferenciasInformacionVehicular (List<Respuesta> vehiculosCesvi, Respuesta vehiculoEmitido,  Map<Long, String> datosAmis) {
		LinkedList<DiferenciasAmis> listaDiferencias = new LinkedList<DiferenciasAmis>();

		//Determinar numero de diferencias entre el inciso y los vehiculos retornados por cesvi
		try {
			StringBuilder mensajeError = new StringBuilder("");
			for (Respuesta informacionVehicular : vehiculosCesvi) {
				Integer numeroDiferencias = 0;
				mensajeError.delete(0,mensajeError.length());
				DiferenciasAmis diferenciasAmis = new DiferenciasAmis();

				//Si cesvi retorna un resultado diferente de 1 entonces es error del numero de serie
				if(!DiferenciasAmis.VIN_SIN_ERRORES.equals(informacionVehicular.getRESULTADO())) {
					diferenciasAmis = new DiferenciasAmis();
					listaDiferencias = new LinkedList<DiferenciasAmis>();
					diferenciasAmis = this.poblarDiferencias(diferenciasAmis, vehiculoEmitido);
					diferenciasAmis.setNumeroDiferencias(0);
					diferenciasAmis.setClaveError(Integer.parseInt(informacionVehicular.getRESULTADO()));
					diferenciasAmis.setMensajeError(informacionVehicularService.getMensaje(Integer.parseInt(informacionVehicular.getRESULTADO()), diferenciasAmis.getNumeroSerie(), ""));
					listaDiferencias.add(diferenciasAmis);
					break;
				} else {
					//Si se encontro informacion del numero de serie, compararla con el vehiculo emitido
					String[] estiloId = null;
					estiloId = vehiculoEmitido.getEstiloId().split("_");
					EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(estiloId[0], estiloId[1],
								BigDecimal.valueOf(Long.valueOf(estiloId[2]))); 
					EstiloVehiculoDTO estilo = estiloVehiculoFacadeRemote.findById(estiloVehiculoId);
					
					if (estilo != null) {
						if (datosAmis.containsValue("MARCA") && !informacionVehicular.getMARCA().equalsIgnoreCase(estilo.getMarcaVehiculoDTO().getDescripcionMarcaVehiculo() != null ?  estilo.getMarcaVehiculoDTO().getDescripcionMarcaVehiculo() : "")) {
							boolean marcaCorrecta = false;
							List<MarcaVehiculoEquivalenciaDTO> marcaVehiculoEquivalenciaDTOs = entidadService.findByProperty(MarcaVehiculoEquivalenciaDTO.class, "CODIGOMARCAVEHICULO", estilo.getMarcaVehiculoDTO().getCodigoMarcaVehiculo());
							for (MarcaVehiculoEquivalenciaDTO marcaVehiculoEquivalenciaDTO : marcaVehiculoEquivalenciaDTOs) {
								if (informacionVehicular.getMARCA().equalsIgnoreCase(marcaVehiculoEquivalenciaDTO.getDescripcionMarcaVehiculo())) {
									marcaCorrecta = true;
									break;
								}
							}
							
							if (!marcaCorrecta) {
								diferenciasAmis.setMarca(informacionVehicular.getMARCA() != null ? informacionVehicular.getMARCA() : "");
								numeroDiferencias ++;
								mensajeError.append(mensajeError.toString().isEmpty() ? "Marca" : ", Marca");
							}
						}

						if(datosAmis.containsValue("PUERTAS") && !informacionVehicular.getPUERTAS().replaceAll(" PUERTAS", "").equalsIgnoreCase(estilo.getNumeroPuertas() != null ? estilo.getNumeroPuertas().toString() : "")) {
							diferenciasAmis.setPuertas(informacionVehicular.getPUERTAS() != null ? informacionVehicular.getPUERTAS() : "");
							numeroDiferencias ++;
							mensajeError.append(mensajeError.toString().isEmpty() ? "Numero de puertas" : ", Numero de puertas");
						}

						if(datosAmis.containsValue("CILINDROS") && !informacionVehicular.getNO_CILINDRO().equalsIgnoreCase(informacionVehicularService.getModificadoresDescripcion(DiferenciasAmis.GRUPO_CILINDROS, estilo.getClaveCilindros()))) {
							diferenciasAmis.setCilindros(informacionVehicular.getNO_CILINDRO() != null ? informacionVehicular.getNO_CILINDRO() : "");
							numeroDiferencias ++;
							mensajeError.append(mensajeError.toString().isEmpty() ? "Cilindros" : ", Cilindros");
						}
					}
					
					if(datosAmis.containsValue("ANO") && !informacionVehicular.getANO().equalsIgnoreCase(vehiculoEmitido.getMODELO() != null ? vehiculoEmitido.getMODELO() : "")) {
						diferenciasAmis.setAnos(informacionVehicular.getANO() != null ? informacionVehicular.getANO() : "");
						numeroDiferencias ++;
						mensajeError.append(mensajeError.toString().isEmpty() ? "Modelo" : ", Modelo");
					}

					if(numeroDiferencias == 0) {
						//Si se encontro que la informacion retornada por cesvi coincide exactamente con el vehiculo emitido entonces se guardan sus claves
						if(datosAmis != null && datosAmis.size() > 0) {
							vehiculoEmitido.setCVE_AMIS(informacionVehicular.getCVE_AMIS());
							vehiculoEmitido.setCVE_SESA(informacionVehicular.getCVE_SESA());
							this.guardarObservacionesSesa(vehiculoEmitido);
						}
						
						diferenciasAmis = new DiferenciasAmis();
						listaDiferencias = new LinkedList<DiferenciasAmis>();
						diferenciasAmis.setClaveError(0);
						diferenciasAmis.setNumeroDiferencias(0);
						listaDiferencias.add(diferenciasAmis);
						break;
					} else {
						//Si se encontraron diferencias entre la informacion del numero de serie y el vehiculo emitido, guardar error
						diferenciasAmis = this.poblarDiferencias(diferenciasAmis, vehiculoEmitido);
						diferenciasAmis.setClaveUnica(informacionVehicular.getCLAVE_UNICA() != null ? informacionVehicular.getCLAVE_UNICA() : "");
						diferenciasAmis.setVersion(informacionVehicular.getVERSION_C() != null ? informacionVehicular.getVERSION_C() : "");
						diferenciasAmis.setNumeroDiferencias(numeroDiferencias);
						diferenciasAmis.setClaveError(DiferenciasAmis.ERROR_VIN_INFORMACION_NO_COINCIDE);
						diferenciasAmis.setMensajeError(mensajeError.toString());
						listaDiferencias.add(diferenciasAmis);
					}
				}
			}	
		} catch (Exception e) {
			LOG.error("Informacion Vehicular Lotes getDiferenciasInformacionVehicular:", e);
		}

		return listaDiferencias;
	}

	private DiferenciasAmis poblarDiferencias (DiferenciasAmis diferenciasAmis, Respuesta vehiculoEmitido) {
		BigDecimal idToCotizacion = vehiculoEmitido.getIdToCotizacion();
		BigDecimal numeroInciso = vehiculoEmitido.getNumeroInciso();

		diferenciasAmis.setIdToPoliza(vehiculoEmitido.getIdToPoliza());
		diferenciasAmis.setIdToCotizacion(idToCotizacion);
		diferenciasAmis.setNumeroInciso(numeroInciso);
		diferenciasAmis.setNumeroSerie(vehiculoEmitido.getVIN() != null ? vehiculoEmitido.getVIN() : "");
		diferenciasAmis.setFechaCreacion(new Date());
		diferenciasAmis.setEstatus(DiferenciasAmis.ESTATUS_PENDIENTE_REVISION);
		diferenciasAmis.setActivo(DiferenciasAmis.ACTIVO);
		return diferenciasAmis;
	}
	
	private String obtenerNombreLote() {
		String nombreLote = "";
		Date fechaConsulta = this.obtenerFechaConsulta();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd"); 
		String fecha = sdf.format(fechaConsulta);
		nombreLote = "AFIRME_" + fecha;
		return nombreLote;
	}
	
	private Date obtenerFechaConsulta() {
		Integer dias = this.getDias(DiferenciasAmis.DIAS_VIN_FLOTILLA);
		GregorianCalendar fechaConsulta = new GregorianCalendar();
		fechaConsulta.setTime(new Date());
		fechaConsulta.add(GregorianCalendar.DATE, +dias);
		return fechaConsulta.getTime();
	}
	
	private void enviarLoteVin(String nombreLote, List<Respuesta> vinVehiculosEmitidos) {
		 try {
			 List<List<Respuesta>> listasVin = new ArrayList<List<Respuesta>>();
		     boolean finDeLista = false; 
		        
			  do{
		         if (vinVehiculosEmitidos.size() > LIMITE_VIN){
		        	 List<Respuesta> sublist = vinVehiculosEmitidos.subList(0, LIMITE_VIN);
		        	 listasVin.add(sublist);
		        	 vinVehiculosEmitidos = vinVehiculosEmitidos.subList(LIMITE_VIN, vinVehiculosEmitidos.size());
			     } else {
			         List<Respuesta> sublist = vinVehiculosEmitidos;
			         listasVin.add(sublist);
			         finDeLista = true;
			     }
		       } while(!finDeLista); 
			  

			  for (List<Respuesta> listaVin : listasVin) {
				  String cadenaVin = obtenerCadenaVin(listaVin);  
				  byte[] vinLote =  cadenaVin.toString().getBytes();
				  //Enviar lote de vin
				  this.getConsultaVin(vinLote, nombreLote);
			  }
		 } catch (Exception e) {
	         LOG.error("Informacion Vehicular Lotes enviarLoteVin:", e);
	     }
	}
	
	public String obtenerCadenaVin(List<Respuesta> listaVin) {
		StringBuilder cadenaVin = new StringBuilder("");
		for (Respuesta vin : listaVin) {
			  if(cadenaVin != null && !cadenaVin.toString().equals("")){
				  cadenaVin.append(",").append(vin.getVIN());
			  } else {
				  cadenaVin.append(vin.getVIN());
			  }
		  }
		return cadenaVin.toString();
	}
	
	private List<Respuesta> obtenerLoteVin(String nombreLote) {
		List<Respuesta> lista = new ArrayList<Respuesta>();
        try {
    		//Consultar informacion vehicular
    		String vinLoteResultadoStr = "";
    		byte[] vinLoteResultado = this.getDescargaVin(nombreLote);
    		if(vinLoteResultado != null && vinLoteResultado.length > 0) {
    			vinLoteResultadoStr = new String(vinLoteResultado);
    		}
    		
    		if(!vinLoteResultadoStr.isEmpty()) {
    			String[] vehiculos = vinLoteResultadoStr.split(SEPARADOR_VEHICULO);
            	for (String vin: vehiculos) {
            		String[] caracteristicasVehiculo = vin.split(SEPARADOR_VEHICULO_CARACTERISTICAS);
            		if (caracteristicasVehiculo.length >= 12) {
            			int i = 0;
            			Respuesta respuesta = new Respuesta();
            			respuesta.setRESULTADO(caracteristicasVehiculo[i++]);
            			respuesta.setCLASE(caracteristicasVehiculo[i++]);
            			respuesta.setMARCA(caracteristicasVehiculo[i++]);
            			respuesta.setMODELO(caracteristicasVehiculo[i++]);
            			respuesta.setANO(caracteristicasVehiculo[i++]);
            			respuesta.setTIPO(caracteristicasVehiculo[i++]);
            			respuesta.setPUERTAS(caracteristicasVehiculo[i++]);
            			respuesta.setPAIS_ORIGEN(caracteristicasVehiculo[i++]);
            			respuesta.setMOTOR(caracteristicasVehiculo[i++]); //CILINDRADA
            			respuesta.setNO_CILINDRO(caracteristicasVehiculo[i++]);
            			respuesta.setTRACCION(caracteristicasVehiculo[i++]);//EJES
            			respuesta.setPLANTA_ENSAMBLE(caracteristicasVehiculo[i++]);
            			respuesta.setVIN(caracteristicasVehiculo[caracteristicasVehiculo.length -1]);
            			lista.add(respuesta);
            		} else {
            			Respuesta respuesta = new Respuesta();
            			respuesta.setRESULTADO(String.valueOf(DiferenciasAmis.ERROR_VIN_LONGITUD_RESPUESTA));
            			lista.add(respuesta);
            		}
            	}
    		}
        } catch (Exception e) {
        	LOG.error("Informacion Vehicular Lotes obtenerLoteVin:", e);
        }
        return lista;
    }
	
	private List<Respuesta> getVehiculosCesviFiltrados(List<Respuesta> listaVehiculosCesvi) {
		//Eliminar duplicados
		Set<Respuesta> vehiculosFiltrados =  new HashSet<Respuesta>();
		vehiculosFiltrados.addAll(listaVehiculosCesvi);
		listaVehiculosCesvi.clear();
		listaVehiculosCesvi.addAll(vehiculosFiltrados);
        
		return  listaVehiculosCesvi;
	}
	
	@SuppressWarnings("unchecked")
	private List<Respuesta> getVehiculosCesviFiltrados(List<Respuesta> listaVehiculosCesvi, final String vin) {
		Predicate predicate = new Predicate(){
			@Override
			public boolean evaluate(Object item) {
				Respuesta respuesta = (Respuesta)item;
				return respuesta.getVIN().equals(vin);
			}
		};
		return (List<Respuesta>) CollectionUtils.select(listaVehiculosCesvi, predicate);
	}

	@Override
	public void enviaInformacionVehicular () {
		if (informacionVehicularService.validacionActiva(DiferenciasAmis.VALIDACION_VIN_FLOTILLA_ACTIVADA)) {
			//Obtener numeros de serie de vehiculos emitidos el dia anterior
			List<Respuesta> vinVehiculosEmitidos = this.getVinVehiculosEmitidos();
			
			if(vinVehiculosEmitidos != null && vinVehiculosEmitidos.size() > 0) {
				//Obtener nombre del lote
				String nombreLote = this.obtenerNombreLote();
				
				if (nombreLote != null && !nombreLote.isEmpty()) {
					//Enviar numeros vin
					this.enviarLoteVin(nombreLote, vinVehiculosEmitidos);
				}
			} else {
				LOG.info("Información Vehicular Lotes: No se encontró información para enviar");
			}
		} else {
			LOG.info("Información Vehicular Lotes: Validación por lotes desactivada");
		}
	}
	
	public void validaInfoVehicularByEstiloPolFlo () {
		if (informacionVehicularService.validacionActiva(DiferenciasAmis.VALIDACION_VIN_FLOTILLA_ACTIVADA)) {
		    //Obtener nombre del lote
			String nombreLote = this.obtenerNombreLote();
			
			/*String envioLoteTerminado = "";
			if (nombreLote != null && !nombreLote.isEmpty()) {
				envioLoteTerminado = this.envioLoteTerminado(nombreLote);
			}
			
			if (envioLoteTerminado.equals(ENVIO_TERMINADO)) {*/
				List<Respuesta> listaVehiculosCesvi = new ArrayList<Respuesta>();
				if (nombreLote != null && !nombreLote.isEmpty()) {
					//Obtener respuesta
					listaVehiculosCesvi = this.obtenerLoteVin(nombreLote);
				}

				if(listaVehiculosCesvi != null && listaVehiculosCesvi.size() > 0) {
					listaVehiculosCesvi = this.getVehiculosCesviFiltrados(listaVehiculosCesvi);
				}	
				
				if(listaVehiculosCesvi != null && listaVehiculosCesvi.size() > 0) {
					List<Respuesta> vehiculosEmitidos = this.getVehiculosEmitidos();
					if(vehiculosEmitidos != null && vehiculosEmitidos.size() > 0) {
						//Validar diferencias
						this.validaInformacionVehicular(vehiculosEmitidos, listaVehiculosCesvi);
					}
				} else {
					LOG.info("Información Vehicular Lotes: No se encontró información para validar");
				}
			/*} else {
				try {
					Thread.sleep(TIEMPO_ESPERA);
					this.validaInformacionVehicular();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}*/
		} else {
			LOG.info("Información Vehicular Lotes: Validación por lotes desactivada");
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<Respuesta> getVinVehiculosEmitidos() {
		 List<Respuesta> vehiculosEmitidos = new ArrayList<Respuesta>();
		 String spName="MIDAS.pkgAUT_Generales.spAut_VinVehiculosEmitidosSesa";
		 String [] atributosDTO = { "VIN"};
		 String [] columnasCursor = { "numeroSerie"};
		 try {
				StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceParametro("pFechaIni", this.obtenerFechaConsulta());
				storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.domain.cesvi.vinplus.Respuesta", atributosDTO, columnasCursor);
				
				vehiculosEmitidos = (List<Respuesta>)storedHelper.obtieneListaResultados();
			} catch (Exception e) {
				LogDeMidasInterfaz.log(
						"MIDAS.pkgAUT_Generales.spAut_VinVehiculosEmitidosSesa..."
								+ this, Level.WARNING, e);
			}
		
		return vehiculosEmitidos;
	}
	
	@SuppressWarnings("unchecked")
	private List<Respuesta> getVehiculosEmitidos() {
		 List<Respuesta> vehiculosEmitidos = new ArrayList<Respuesta>();
		 String spName="MIDAS.pkgAUT_Generales.spAut_VehiculosEmitidosSesa";
		 String [] atributosDTO = { "idToPoliza","idToCotizacion", "idToInciso", "numeroInciso", "VIN", "estiloId", "claveTipoSolicitud"};
		 String [] columnasCursor = { "idToPoliza","idToCotizacion", "idToInciso", "numeroInciso", "numeroSerie", "estiloId", "claveTipoSolicitud"};
		 try {
				StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceParametro("pFechaIni", this.obtenerFechaConsulta());
				storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.domain.cesvi.vinplus.Respuesta", atributosDTO, columnasCursor);
				
				vehiculosEmitidos = (List<Respuesta>)storedHelper.obtieneListaResultados();
			} catch (Exception e) {
				LogDeMidasInterfaz.log(
						"MIDAS.pkgAUT_Generales.spAut_VehiculosEmitidosSesa..."
								+ this, Level.WARNING, e);
			}
		
		return vehiculosEmitidos;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)	
	private void guardarObservacionesSesa(Respuesta vehiculoEmitido) {
		String spName="MIDAS.pkgAUT_Generales.spAut_GuardarObservacionesSesa";
		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceParametro("pIdToInciso", vehiculoEmitido.getIdToInciso());
			storedHelper.estableceParametro("pObservacionesSesa", vehiculoEmitido.getObservacionesSesa());
			storedHelper.estableceParametro("pClaveAmis", vehiculoEmitido.getCVE_AMIS());
			storedHelper.estableceParametro("pClaveSesa", vehiculoEmitido.getCVE_SESA());
			
			storedHelper.ejecutaActualizar();
		} catch (Exception e) {
			LogDeMidasInterfaz.log(
					"Excepcion general en MIDAS.pkgAUT_Generales.spAut_GuardarObservacionesSesa..."
							+ this, Level.WARNING, e);
		}	
	}
	
	private Integer getDias (BigDecimal codigoParametroGeneral) {
		Integer dias = -1;
		ParametroGeneralId id = new ParametroGeneralId();
		id.setIdToGrupoParametroGeneral(new BigDecimal(13));
		id.setCodigoParametroGeneral(codigoParametroGeneral);
		ParametroGeneralDTO parametro = parametroGeneralService.findById(id );
		if(parametro != null && parametro.getValor() != null) {
			dias = Integer.parseInt(parametro.getValor());
		}
		return dias;
	}
}
