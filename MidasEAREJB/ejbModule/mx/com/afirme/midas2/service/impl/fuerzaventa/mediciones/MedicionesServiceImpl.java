package mx.com.afirme.midas2.service.impl.fuerzaventa.mediciones;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.asm.dto.RoleDTO;
import com.asm.dto.UserDTO;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFilenameFilter;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.mediciones.conf.ConfiguracionMedicionesAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.mediciones.conf.ConfiguracionMedicionesPromotoria;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.fuerzaventa.mediciones.Reporte;
import mx.com.afirme.midas2.dto.fuerzaventa.mediciones.Reporte.Tipo;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.fuerzaventa.mediciones.MedicionesService;
import mx.com.afirme.midas2.service.fuerzaventa.mediciones.conf.ConfiguracionMedicionesService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MailService;

@Stateless
public class MedicionesServiceImpl implements MedicionesService {
	
	/* 	metodos publicos */
	
	@Override
	public String obtenerPenultimoPeriodoMensual() {
		
		return obtenerPeriodoMensual(new Date(), true);
		
	}


	@Override
	public Reporte obtenerPenultimoReporteMensual(UserDTO usuario) {
		
		return obtenerReporte(usuario, Tipo.PENULTIMO_REPORTE_MENSUAL);
		
	}


	@Override
	public String obtenerUltimoPeriodoMensual() {
		
		return obtenerPeriodoMensual(new Date(), false);
	
	}


	@Override
	public Reporte obtenerUltimoReporteMensual(UserDTO usuario) {
		
		return obtenerReporte(usuario, Tipo.ULTIMO_REPORTE_MENSUAL);
		
	}


	@Override
	public Reporte obtenerUltimoReporteSemanal(UserDTO usuario) {
		
		return obtenerReporte(usuario, Tipo.ULTIMO_REPORTE_SEMANAL);
		
	}
	
	@Override
	public Boolean validarUsuario (UserDTO usuario) {
		
		return (obtenerClave(usuario, obtenerEsPromotor(usuario)).intValue() > 0)
				&& esUltimoReporteSemanalDisponible(usuario);
		
		
	}
	
	@Override
	public void enviarCorreosReporteSemanal() {
		
		log.info("Se ha lanzado de manera manual el proceso de envio de Correos con el Reporte Semanal de Mediciones de Agentes y Promotores.");
		
		try {
		
			procesoEnviarCorreosReporteSemanal();
			
		} catch (Exception ex) {
			
			log.error("Error en el proceso manual de envio de Correos con el Reporte Semanal de Mediciones de Agentes y Promotores:", ex);
			
			throw new RuntimeException(ex);
			
		}
		
	}
	
		
	/* schedules */
				
	@Schedule(dayOfWeek="Tue", hour="20", persistent=false)
	private void limpiar() {
		
		try {
						
			log.info("Comenzando proceso limpiar()...");
			
			/**
			
			1) Se calculan las carpetas del corte de la semana actual y las carpetas correspondientes a los 2 ultimos cortes mensuales																										
			antes del corte de la semana actual																										
																													
			2) Navegando a traves de la carpeta raiz del servidor de QlikView, se revisará carpeta por carpeta. Si la carpeta que																										
			se está revisando no es ninguna de las 3 carpetas calculadas, entonces se borra.																										

			 */
			
			 
			StringBuilder sb = new StringBuilder();
			
			sb.append("((?!(")
				.append(obtenerCorte(Tipo.ULTIMO_REPORTE_SEMANAL))
				.append("|")
				.append(obtenerCorte(Tipo.ULTIMO_REPORTE_MENSUAL))
				.append("|")
				.append(obtenerCorte(Tipo.PENULTIMO_REPORTE_MENSUAL))
				.append(")).)*");
			
			
			String regExFoldersToClean = sb.toString();

			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("", usuarioReportes(), passwordReportes());
			SmbFile base = new SmbFile(obtenerRutaBase(), auth);
		    
		    SmbFile[] fobjs = base.listFiles(new RegexFilenameFilter(regExFoldersToClean));
		    
		    for (SmbFile fobj : fobjs) {
		    	
		    	fobj.delete();
		    	
		    	
		    }
		    					
		}  catch (Exception ex) {
    		
			log.error("Error en proceso limpiar():", ex);
    		
    	}
		
		log.info("Proceso limpiar() ha terminado.");
		
	}
	
	
	@Schedule(dayOfWeek="Mon", hour="20", persistent=false)
	private void procesoAutomaticoEnviarCorreosReporteSemanal() {
		
		log.info("Se ha lanzado automaticamente el proceso de envio de Correos con el Reporte Semanal de Mediciones de Agentes y Promotores.");
		
		try {
		
			procesoEnviarCorreosReporteSemanal();
			
		} catch (Exception ex) {
			
			log.error("Error en el proceso automatico de envio de Correos con el Reporte Semanal de Mediciones de Agentes y Promotores:", ex);
			
		}
		
	}
		
	private void procesoEnviarCorreosReporteSemanal() {
		
		log.info("Comenzando proceso enviarCorreosReporteSemanal()...");
		
		String correosAdicionales = configuracionService.obtenerCorreosAdicionales();
		
		List<String> cc = Arrays.asList(correosAdicionales.split(";"));
		
		List<ConfiguracionMedicionesAgente> configuracionesAgente = entidadDao.findByProperty(ConfiguracionMedicionesAgente.class, "habilitado", true);
		
		List<ConfiguracionMedicionesPromotoria> configuracionesPromotoria = entidadDao.findByProperty(ConfiguracionMedicionesPromotoria.class, "habilitado", true);
		
		for (ConfiguracionMedicionesAgente configuracionAgente : configuracionesAgente) {
			
			try {
				
				enviarCorreoReporteSemanal(configuracionAgente, cc);
				
			} catch (Exception ex) {
				
				log.error("Error en envio mediciones Agente " + configuracionAgente.getAgente().getIdAgente() + ":", ex);
				
			}
						
		}
		
		for (ConfiguracionMedicionesPromotoria configuracionPromotoria : configuracionesPromotoria) {
			
			try {
				
				enviarCorreoReporteSemanal(configuracionPromotoria, cc);
				
			} catch (Exception ex) {
				
				log.error("Error en envio mediciones Promotoria " + configuracionPromotoria.getPromotoria().getIdPromotoria() + ":", ex);
				
			}			
			
		}
		
		log.info("Proceso enviarCorreosReporteSemanal() ha terminado.");
		
	}
	
	/* 	metodos privados */
	
	private Boolean esUltimoReporteSemanalDisponible(UserDTO usuario) {
		
		try {
		
			obtenerUltimoReporteSemanal(usuario);
		
		} catch (Exception ex) {
			
			return false;
			
		}
		
		return true;
		
	}
	
	private void enviarCorreoReporteSemanal(Object config, List<String> cc) throws IOException, ParseException{
		
		Long clave;
		boolean esPromotor = false;
		List<ByteArrayAttachment> adjuntos;
		List<String> para = new ArrayListNullAware<String>();
		String asunto;
		String cuerpo;
		String rol;
		String fechaCorteCorreo;
		Map<String, String> mapInlineImages;
		
		String corte = obtenerCorte(Tipo.ULTIMO_REPORTE_SEMANAL);
		
		if (config instanceof ConfiguracionMedicionesPromotoria) {
			
			para.add(((ConfiguracionMedicionesPromotoria)config).getEmail());
			
			clave = ((ConfiguracionMedicionesPromotoria)config).getPromotoria().getIdPromotoria();
			
			esPromotor = true;
						
		} else {
			
			para.add(((ConfiguracionMedicionesAgente)config).getEmail());
			
			clave = ((ConfiguracionMedicionesAgente)config).getAgente().getIdAgente();
			
		}
	
		//Si se obtiene el reporte se adjunta al correo, de lo contrario el correo no se mandara
				
		Reporte reporte = obtenerArchivoReporte(clave, corte, esPromotor);
		
		adjuntos = new ArrayList<ByteArrayAttachment>();
		
		adjuntos.add(new ByteArrayAttachment(
				reporte.getFileName(), 
				ByteArrayAttachment.TipoArchivo.PDF, 
				IOUtils.toByteArray(reporte.getInputStream())));
		
		fechaCorteCorreo = convertirFormato(corte, FORMATO_DIA_CORTE, FORMATO_CORTE_CORREO);
				
		rol = (esPromotor?ROL_PROMOTOR_ED:ROL_AGENTE_ED);
		
		asunto = Utilerias.getMensajeRecurso(
				SistemaPersistencia.ARCHIVO_RECURSOS, "midas.mediciones.agentes.correo.asunto", fechaCorteCorreo, rol, clave.toString());
		
		cuerpo = Utilerias.getMensajeRecurso(
				SistemaPersistencia.ARCHIVO_RECURSOS, "midas.mediciones.agentes.correo.cuerpo",	rol);
						
		mapInlineImages = new HashMap<String, String>();
		
		mapInlineImages.put("header_img", sistemaContext.getDirectorioImagenesParametroGlobal() +"reportes/mediciones_header.jpg");
		
		mapInlineImages.put("footer_img", sistemaContext.getDirectorioImagenesParametroGlobal() +"reportes/mediciones_footer.jpg");
			
		/*
		 sendMessage(List<String> recipients, String subject, String messageContent, 
		    String from, String[] attachments, List<ByteArrayAttachment> fileAttachments,
		    List<String> cc,List<String> cco, Map<String, String> mapInlineImages)
		 */
		
		log.info("Enviando correo Mediciones para : " + rol + " " + clave);
		
		mailService.sendMessage(para, asunto, cuerpo,
				"administrador.midas@afirme.com", null, adjuntos, 
				cc, null, mapInlineImages);
		
	}
	
	
	private String obtenerCorte(Tipo tipo) throws ParseException {
		
		Date hoy = new Date();
		
		String corte;
						
		switch (tipo) {
        case ULTIMO_REPORTE_SEMANAL: default:
            
        	corte = obtenerCorteSemanal(hoy);
            
            break;
                
        case ULTIMO_REPORTE_MENSUAL:
            
        	corte = obtenerCortePeriodoMensual(obtenerPeriodoMensual(hoy, false));
            
            break;
                     
        case PENULTIMO_REPORTE_MENSUAL:
            
        	corte = obtenerCortePeriodoMensual(obtenerPeriodoMensual(hoy, true));
            
            break;
       
		}
		
		return corte;
			
	}
	
	
	private String obtenerCortePeriodoMensual(String mes) throws ParseException {
		
		GregorianCalendar cal = new GregorianCalendar();
		
		cal.setFirstDayOfWeek(DIA_CORTE + 1); // esto hace a DIA_CORTE el ultimo dia de la semana
		
		cal.setTime(new SimpleDateFormat(FORMATO_PERIODO_MENSUAL).parse(mes));
				
		cal.add(GregorianCalendar.MONTH, 1);
		
		cal.set(GregorianCalendar.DAY_OF_MONTH, 1);
		
		//si el primer dia del mes siguiente es tambien el primer dia de la semana entonces el corte mensual es un dia antes (ultimo dia del mes evaluado)
		
		if (cal.get(GregorianCalendar.DAY_OF_WEEK) == cal.getFirstDayOfWeek()) {
			
			cal.add(GregorianCalendar.DAY_OF_YEAR, -1);
		
		} else if (cal.get(GregorianCalendar.DAY_OF_WEEK) != DIA_CORTE) {
		
			//de lo contrario el corte mensual es en el primer fin de semana del siguiente mes al evaluado
			
			cal.add(GregorianCalendar.DAY_OF_WEEK, 8 - cal.get(GregorianCalendar.DAY_OF_WEEK));  
			
		}		
		
		return new SimpleDateFormat(FORMATO_DIA_CORTE).format(cal.getTime());
				
	}
		
	
	private String obtenerCorteSemanal(Date origen) {
		
		GregorianCalendar cal = new GregorianCalendar();
				
		cal.setTime(origen);
		
		cal.add(GregorianCalendar.DAY_OF_WEEK, -1);
		
		cal.add(GregorianCalendar.DAY_OF_WEEK, - (cal.get(GregorianCalendar.DAY_OF_WEEK) - 1));  //regresa la fecha del ultimo DIA_CORTE 
	
		return new SimpleDateFormat(FORMATO_DIA_CORTE).format(cal.getTime());
		
	}
	
	
    private String obtenerPeriodoMensual(Date origen, boolean esPenultimo) {
		
    	GregorianCalendar cal = new GregorianCalendar();
		
    	cal.setFirstDayOfWeek(DIA_CORTE + 1); // esto hace a DIA_CORTE el ultimo dia de la semana
    	
    	cal.setMinimalDaysInFirstWeek(1);
    	
		cal.setTime(origen);
		
		//
		if (cal.get(GregorianCalendar.WEEK_OF_MONTH) == 1) {
			
			cal.set(GregorianCalendar.DAY_OF_MONTH, 1);
			
			if(cal.get(GregorianCalendar.DAY_OF_WEEK) != (DIA_CORTE + 1)) {
				
				//Last month period is a month before last month
				cal.add(GregorianCalendar.MONTH, -2);
				
			} else {
				
				//Last month period is last month
				cal.add(GregorianCalendar.MONTH, -1);
				
			}
			
			
		} else {
			
			//Last month period is last month
			cal.add(GregorianCalendar.MONTH, -1);
			
		}

		
		if (esPenultimo) {
			
			//A month before last month period
			cal.add(GregorianCalendar.MONTH, -1);
			
		}
		
		return new SimpleDateFormat(FORMATO_PERIODO_MENSUAL).format(cal.getTime());
		
	}
    
    private Long obtenerAgenteId(UserDTO usuario) {
    	
    	Long agenteId = (Long) usuario.getUserAttributeValue("agentId");
    	
    	if (agenteId == null) {
    	
    		Agente agente = entidadDao.findByProperty(Agente.class, "codigoUsuario", usuario.getUsername()).get(0);
    		
    		if (agente != null) {
    		 
    			agenteId = agente.getIdAgente();
    			
    		}
    		
    	}
    	
    	if (agenteId == null) {
    	
    		throw new ApplicationException("No se puede encontrar el agente ligado al usuario");
    		
    	}
    	
    	return agenteId;
    	    	
    }
    
    @SuppressWarnings("unchecked")
	private boolean obtenerEsPromotor(UserDTO usuario) {
    	
    	Predicate predicadoAgentePromotor = new Predicate() {
			public boolean evaluate(Object arg0) {
				
				RoleDTO rolEvaluado = (RoleDTO) arg0;
				
				return (rolEvaluado.getApplication().getApplicationId() == ED_APPLICATION_ID
						&& (rolEvaluado.getRoleName().equals(ROL_AGENTE_ED) 
								|| rolEvaluado.getRoleName().equals(ROL_PROMOTOR_ED)));
				
			}
		};
    			
		List<RoleDTO> rolesAgentePromotor = (List<RoleDTO>) CollectionUtils.select(usuario.getRoleList(), predicadoAgentePromotor);
		
		//En ED un usuario no puede tener roles de Agente y Promotor al mismo tiempo
    	
		if (rolesAgentePromotor == null || rolesAgentePromotor.size() == 0) {
			
			throw new ApplicationException("El usuario debe ser un agente o un promotor");
			
		}
				
		return (rolesAgentePromotor.get(0).getRoleName().equals(ROL_PROMOTOR_ED)?true:false);
    	
    }
    
    private Long obtenerClave(UserDTO usuario, boolean esPromotor) {
    	
		Long clave;
		
		Long agenteId = obtenerAgenteId(usuario);
		
		if (esPromotor) {
						
			Agente agente = entidadDao.findByProperty(Agente.class, "idAgente", agenteId).get(0);
			
			clave = agente.getPromotoria().getIdPromotoria();
			
			ConfiguracionMedicionesPromotoria confPromotoria = configuracionService.getConfigPromotoria(clave);
			
			if (confPromotoria == null) {
				
				throw new ApplicationException("El promotor no esta configurado");
				
			}
			
			if (!confPromotoria.getHabilitado()) {
				
				throw new ApplicationException("El promotor no esta habilitado");
				
			}
			
			
		} else {
			
			clave = agenteId;
			
			ConfiguracionMedicionesAgente confAgente = configuracionService.getConfigAgente(clave);
			
			if (confAgente == null) {
				
				throw new ApplicationException("El agente no esta configurado");
				
			}
			
			if (!confAgente.getHabilitado()) {
				
				throw new ApplicationException("El agente no esta habilitado");
				
			}
			
		}
    	
    	return clave;
    	
    }
    
    
	private Reporte obtenerReporte (UserDTO usuario, Tipo tipo) {
    	
    	try {
	    	//Se valida al usuario antes de entregarle el reporte
	    	
    		boolean esPromotor = obtenerEsPromotor(usuario);
    		
    		Long clave = obtenerClave(usuario, esPromotor);
			 
	    	String corte = obtenerCorte(tipo);
	    	   	
	    	return obtenerArchivoReporte(clave, corte, esPromotor);
	    	
    	} catch (Exception ex) {
    		
    		if (!(ex instanceof ApplicationException)) {
    			
    			ex.printStackTrace();
    			
    			throw new ApplicationException(ex.toString());
    			
    		}
    		
    		throw (ApplicationException) ex;
    		
    	}
    	    	
    }
    
 
	private Reporte obtenerArchivoReporte(Long clave, String corte, boolean esPromotor) throws IOException {
				
		
		jcifs.Config.setProperty("jcifs.resolveOrder", "WINS,DNS,BCAST,LMHOSTS" );   //LMHOSTS,WINS,BCAST,DNS

		jcifs.Config.setProperty("jcifs.smb.client.responseTimeout", "5000" );      //30000

		jcifs.Config.setProperty("jcifs.smb.client.dfs.disabled", "true" );
		
		
		String regExFileName = ".*<corte>\\((\\d*_)*<clave>([_\\)])*(.)*\\)(.)*\\.pdf".replace("<corte>", corte).replace("<clave>", clave.toString());
		
		
		NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("", usuarioReportes(), passwordReportes());
	    SmbFile base = new SmbFile(obtenerRutaReportes(corte, esPromotor), auth);
	    
	    SmbFile[] fobjs = base.listFiles(new RegexFilenameFilter(regExFileName));
	    
	    
	    if (fobjs.length == 0) {
			
			throw new ApplicationException("El reporte no fue encontrado");
			
		}
		
	    SmbFile reportFile = fobjs[0];
		
		Reporte reporte = new Reporte();
		
		reporte.setCorte(corte);
		reporte.setFileName(reportFile.getName());
		reporte.setContentType(CONTENT_TYPE_PDF);
		reporte.setInputStream(new BufferedInputStream(new SmbFileInputStream(reportFile), 8192));
		
		return reporte;
		
	}
		
	private String obtenerRutaReportes(String corte, boolean esPromotor) {
		
		//<RutaBase>/YYYYMMDD/Agentes
		//<RutaBase>/YYYYMMDD/Promotores
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(obtenerRutaBase())
			.append(corte)
			.append(SEPARADOR)
			.append(esPromotor?"Promotores":"Agentes").append(SEPARADOR);
				
		return sb.toString();
		
	}
			
	private String obtenerRutaBase() {
		
		return configuracionService.obtenerRutaBaseReportes();
		
	}
	
	
	private String usuarioReportes() {
		
		return configuracionService.obtenerUsuarioRutaBaseReportes();
		
	}
	
	private String passwordReportes() {
		
		return configuracionService.obtenerPasswordRutaBaseReportes();
		
	}
	
	
	private String convertirFormato(String fechaEnFormatoOrigen, String formatoOrigen, String formatoDestino) {
		
		try {
			
			SimpleDateFormat sdf = new SimpleDateFormat(formatoOrigen);
		
			Date fecha;
		
			fecha = sdf.parse(fechaEnFormatoOrigen);
		
		
			sdf.applyPattern(formatoDestino);
				
			return sdf.format(fecha);
			
		} catch (ParseException e) {
		
			throw new RuntimeException(e);
			
		}
		
	}
	
	
	private static class RegexFilenameFilter implements SmbFilenameFilter {
	    
	    private final String regex;

	    public RegexFilenameFilter(String regex) {
	        this.regex = regex;
	    }

	    @Override
	    public boolean accept(SmbFile dir, String name) throws SmbException {
	        return name.matches(regex);
	    }

	   
	}
	
	
	
	/* Definiciones generales */
	
	public static final int DIA_CORTE = GregorianCalendar.SUNDAY; 
	
	public static final String FORMATO_DIA_CORTE = "yyyyMMdd";
	
	public static final String FORMATO_CORTE_CORREO = "dd/MM/yyyy";
	
	public static final String FORMATO_PERIODO_MENSUAL = "yyyyMM";
			
	private static final String CONTENT_TYPE_PDF = "application/pdf";
		
	public static final String SEPARADOR = "/";
	
	public static final int ED_APPLICATION_ID = 4; 
	
	private static final String ROL_AGENTE_ED = "Agente";
	
	private static final String ROL_PROMOTOR_ED = "Promotor";
		
	private static Logger log = Logger.getLogger(MedicionesServiceImpl.class);	

	/* Referencias */
	
	@EJB
	private EntidadDao entidadDao;
	
	@EJB
	private ConfiguracionMedicionesService configuracionService;
	
	@EJB
	private MailService mailService;
	
	@EJB
	private SistemaContext sistemaContext;
	
}

