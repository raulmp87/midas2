package mx.com.afirme.midas2.service.impl.siniestros.notificaciones;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudDataEnTramiteDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.AdjuntoConfigNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ConfiguracionNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina.EnumModoEnvio;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina.EnumTipoCorreo;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina.EnumTipoDestinatario;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SolicitudReporteCabinaDTO;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.cita.CitaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.depuracion.ConfiguracionDepuracionReserva;
import mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones.ConfiguracionNotificacionDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.notificaciones.ConfiguracionNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.BitacoraNotificacionService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.BitacoraNotificacionService.BitacoraRegistroDetalleNotificacionDTO;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.utils.CommonUtils;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

@Stateless
public class EnvioNotificacionesServiceImpl implements
		EnvioNotificacionesService {
	
	private static final String INICIOCAMPO= "\\{\\$";
	private static final String FINCAMPO = "\\}";
	private static final String REGEX = INICIOCAMPO + "([^}]+)" + FINCAMPO;
	private static final Pattern PATTERN = Pattern.compile(REGEX);
	private static final String NO_DATA_FOUND = "No capturado";
	private static final String USUARIO_MIGRACION = "MIGSINIE";
	private static final String USUARIO_ADMINISTRADOR = "M2ADMINI";
	
	
	public static final Logger LOGGER = Logger
			.getLogger(EnvioNotificacionesServiceImpl.class);
	@EJB
	private ConfiguracionNotificacionesService configService;
	@Resource
	private TimerService timerService;
	@EJB
	private MailService mailService;
	@EJB
	private ClienteFacadeRemote clienteFacade;
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	@EJB
	private ListadoService listadoService;
	@EJB
	private static EntidadService entidadService;
	@EJB
	private BitacoraNotificacionService bitacoraNotificacionService;
	@EJB
	private AgenteMidasService agenteMidasService;
	
	
	// Mapas usados para los valores en los catalagos (CatValorFijo)
	private Map<String, String> mapModoEnvio;
	private Map<String, String> mapTipoDesti;
	private Map<String, String> mapTipoCorre;
	
	private SistemaContext sistemaContext;
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	
	/**
	 * Cargar parametros generales de configuracion de notificaciones basandose en reporte cabina
	 * @param mapa actual, crea uno en caso de ser <code>null</code>
	 * @param reporte
	 * @return
	 */
	@Override
	public Map<String, Serializable> cargarParametrosGeneralesSiniestros(ReporteCabina reporte, 
			Map<String, Serializable> mapa){
		Map<String, Serializable> dataArg;
		if(mapa == null){
			dataArg = new HashMap<String, Serializable>();
		}else{
			dataArg = new HashMap<String, Serializable>(mapa);
		}
		if(reporte != null){
				dataArg.put(ATR_REPORTE, reporte);
				dataArg.put(ATR_REPORTCAB_GMH, reporte.getNumeroReporte());
				dataArg.put(ATR_NUMREPORTE, reporte.getNumeroReporte());
				dataArg.put(ATR_PERSONAREPORTA, reporte.getPersonaReporta());
				dataArg.put(ATR_TELEFONOREPORTA, reporte.getLadaTelefono().concat(reporte.getTelefono()));
				if(reporte.getLugarOcurrido() == null){
					dataArg.put(ATR_UBICASINIE, NO_DATA_FOUND);
					dataArg.put(ATR_CIUDADSINIESTRO, NO_DATA_FOUND);
					dataArg.put(ATR_ESTADOSINIESTRO, NO_DATA_FOUND);
					
				}else{
					dataArg.put(ATR_UBICASINIE, reporte.getLugarOcurrido().toString());
					dataArg.put(ATR_CIUDADSINIESTRO, reporte.getLugarOcurrido().getCiudad().getDescripcion().toString());
					dataArg.put(ATR_ESTADOSINIESTRO, reporte.getLugarOcurrido().getEstado().getDescripcion().toString() );
				}
				dataArg.put(ATR_HORAREPORTE, reporte.getHoraOcurrido());
				if(reporte.getPoliza() == null){
					dataArg.put(ATR_NUMPOLIZA, NO_DATA_FOUND);
				}else{
					dataArg.put(ATR_NUMPOLIZA, reporte.getPoliza().getNumeroPolizaFormateada());
				}
				
				String horaAsignacion = "";
				if(reporte.getFechaHoraAsignacion() != null){
					DateTime getHoraAsignacion = new DateTime(reporte.getFechaHoraAsignacion());
					int horas = getHoraAsignacion.getHourOfDay();
					int minutos = getHoraAsignacion.getMinuteOfHour();
					int segundos = getHoraAsignacion.getSecondOfMinute();
					horaAsignacion = String.valueOf(horas) +":"+String.valueOf(minutos)+":"+String.valueOf(segundos);
				}else{
					horaAsignacion = "Hora no asignada";
				}
				dataArg.put(ATR_FECHAHORAASIG, horaAsignacion);
			
				if(reporte.getLugarOcurrido() == null){
					dataArg.put(ATR_TIPOSINIESTRO, NO_DATA_FOUND);
				}else{
					if(reporte.getLugarOcurrido().getZona() == null){
						dataArg.put(ATR_TIPOSINIESTRO, NO_DATA_FOUND);
					}else{
						if (reporte.getLugarOcurrido().getZona().equals("CD") ){	
							dataArg.put(ATR_TIPOSINIESTRO, "CIUDAD");
						}else{
							dataArg.put(ATR_TIPOSINIESTRO, "CARRETERA");
						}
					}
				}
				
				if(reporte.getLugarOcurrido() == null){
					dataArg.put(ATR_CIUDADESTADO, NO_DATA_FOUND );
				}else{

					String strCiudad = "";
					String strEstado = "";
					if(reporte.getLugarOcurrido().getCiudad() == null ){
						strCiudad = " " ;
					}else{
						strCiudad = reporte.getLugarOcurrido().getCiudad().getDescripcion().toString() ;
					}
					if(reporte.getLugarOcurrido().getEstado() == null ){ 
						strEstado = " ";
					}else{
						strEstado = reporte.getLugarOcurrido().getEstado().getDescripcion().toString() ;
					}
					dataArg.put(ATR_CIUDADESTADO, strCiudad.concat(", "+ strEstado) );
				}

			if(!dataArg.containsKey(ATR_OFICINAID) && reporte.getOficina() != null){
				dataArg.put(ATR_OFICINAID, reporte.getOficina().getId());
				dataArg.put(ATR_OFICINA, reporte.getOficina().getNombreOficina()); 
			}		
			if(!dataArg.containsKey(ATR_AJUSTADOR) && reporte.getAjustador() != null){
				dataArg.put(ATR_AJUSTADOR, reporte.getAjustador().getPersonaMidas());
			}
			if(!dataArg.containsKey(ATR_CABINERO)){
				try {
					if( (reporte.getCodigoUsuarioCreacion().equals(USUARIO_MIGRACION))||(reporte.getCodigoUsuarioCreacion().equals(USUARIO_ADMINISTRADOR)) ){
						String cabineroUsr = reporte.getCodigoUsuarioCreacion();
						dataArg.put(ATR_CABINERO, cabineroUsr);
					}else{
						Usuario cabinero = usuarioService.buscarUsuarioPorNombreUsuario(reporte.getCodigoUsuarioCreacion());
						dataArg.put(ATR_CABINERO, cabinero);
					}
					
				} catch (Exception e) {
					LOGGER.error("cabinero no encontrado", e);
				}					
			}
			if (!dataArg.containsKey(ATR_OPERACABINA)){
				try{
					if( (reporte.getCodigoUsuarioCreacion().equals(USUARIO_MIGRACION))||(reporte.getCodigoUsuarioCreacion().equals(USUARIO_ADMINISTRADOR)) ){
						String cabineroUsr = reporte.getCodigoUsuarioCreacion();
						dataArg.put(ATR_OPERACABINA, cabineroUsr);
					}else{
						Usuario operadorCabina = usuarioService.buscarUsuarioPorNombreUsuario(reporte.getCodigoUsuarioCreacion());
						dataArg.put(ATR_OPERACABINA, operadorCabina.getNombreUsuario() );
					}
				}catch(Exception e){
					LOGGER.error("Operador de cabina no encontrado", e);
				}
			}
			if(!dataArg.containsKey(ATR_POLIZADTO) && reporte.getPoliza() != null){
				dataArg.put(ATR_POLIZADTO, reporte.getPoliza());
				dataArg.put(ATR_POLIZA, reporte.getPoliza());
				if(reporte.getPoliza().getCotizacionDTO() != null){
					if(!dataArg.containsKey(ATR_COTIZACION)){
						dataArg.put(ATR_COTIZACION, reporte.getPoliza().getCotizacionDTO());
					}
					if(!dataArg.containsKey(ATR_AGENTE)){
						try {	
							Agente filtro = new Agente();
							filtro.setId(reporte.getAgenteId().longValue());
							Agente agente = agenteMidasService.loadById(filtro);
							dataArg.put(ATR_AGENTE, agente);							
						} catch (Exception e) {
							LOGGER.error("Agente no encontrado", e);
						}
					}
					if(!dataArg.containsKey(ATR_CLIENTE)){
						try {
							ClienteDTO clienteDTO = clienteFacade.findById(
									reporte.getPoliza().getCotizacionDTO().getIdToPersonaContratante(), "");
							dataArg.put(ATR_CLIENTE, clienteDTO);				
						} catch (Exception e) {
							LOGGER.error("Cliente no encontrado",e);
						}
					}
					if(!dataArg.containsKey(ATR_INCISO) && reporte.getSeccionReporteCabina() != null && 
							reporte.getSeccionReporteCabina().getIncisoReporteCabina() != null){
						dataArg.put(ATR_INCISO, reporte.getSeccionReporteCabina().getIncisoReporteCabina());
					}
				}
			}
		}
		return dataArg;
	}
	
	
	@Override
	public void enviarNotificacionSiniestros(String codigo, Map<String, Serializable> dataArg) {
		try {
			Map<String, Serializable> parametros = new HashMap<String, Serializable>(dataArg);
			ReporteCabina reporte = (ReporteCabina)dataArg.get(ATR_REPORTE); 
				
			if(reporte != null){
				parametros = cargarParametrosGeneralesSiniestros(reporte, dataArg);
				
			}else{
				LOGGER.info("#*#*#*#*#*#*#*# El objeto reporte se encuentra vacio, no se puede asignar al objeto reporte de tipo ReporteCabina");
			}
			
			enviarNotificacion(codigo, parametros);

		} catch (NegocioEJBExeption e) {
			LOGGER.error("codigo: " + e.getErrorCode() + " mensaje: "
					+ e.getMessage(), e);
		} catch (Exception e) {
			LOGGER.error("error al enviar notificacion cabina", e);
		}

	}

	/***
	 * {@inheritDoc}
	 */
	@Override
	public void notificacionCabinaCreacionReporte(ReporteCabina reporte) {
		Map<String, Serializable> dataArg = new HashMap<String, Serializable>();
		dataArg.put(ATR_REPORTE, reporte);
		enviarNotificacionSiniestros(
				EnumCodigo.CABINA_ASIGNACION_AJUSTADOR.toString(), dataArg);

	}

	/***
	 * {@inheritDoc}
	 */
	@Override
	public void notificacionCabinaAsignacionAjustador(ReporteCabina reporte) {
		Map<String, Serializable> dataArg = new HashMap<String, Serializable>();
		dataArg.put(ATR_REPORTE, reporte);
		
		enviarNotificacionSiniestros(EnumCodigo.CABINA_ARRIBO_AJUSTADOR.toString(),
				dataArg);
	}

	/***
	 * {@inheritDoc}
	 */
	@Override
	public void notificacionCabinaArriboAjustador(ReporteCabina reporte) {
		Map<String, Serializable> dataArg = new HashMap<String, Serializable>();
		dataArg.put(ATR_REPORTE, reporte);
		enviarNotificacionSiniestros(
				EnumCodigo.CABINA_TERMINO_SINIESTRO.toString(), dataArg);
	}

	/***
	 * {@inheritDoc}
	 */
	@Override
	public void notificacionCartaCobertura(ReporteCabina reporte,
			PolizaDTO poliza, AutoIncisoReporteCabina auto) {
		HashMap<String, Serializable> dataArg = new HashMap<String, Serializable>();
		dataArg.put(ATR_REPORTE, reporte);
		dataArg.put(ATR_OFICINAID, reporte.getOficina().getId());
		dataArg.put(ATR_POLIZA, poliza);
		dataArg.put(ATR_AUTO, auto);
		enviarNotificacionSiniestros(
				EnumCodigo.CARTA_COBERTURA_CON_POLIZA_VALIDA.toString(),
				dataArg);
	}

	/***
	 * {@inheritDoc}
	 */
	@Override
	public void notificacionCartaCoberturaSinPoliza(ReporteCabina reporte,
			AutoIncisoReporteCabina auto,String tipo) {
		HashMap<String, Serializable> dataArg = new HashMap<String, Serializable>();
		SolicitudReporteCabinaDTO solicitudReporteCabina = new SolicitudReporteCabinaDTO();
		Map<String,Object> params = new HashMap<String,Object>();
		List<SolicitudDTO> listaSolicitud = entidadService.findByProperties( SolicitudDTO.class, params );	
		
		BigDecimal idsolicitud = new BigDecimal(reporte.getIdToSolicitud());
		String numeroSerie = auto.getNumeroSerie();
		
		params.put( "idToSolicitud", idsolicitud );
		params.put( "tieneCartaCobertura", 1 );
		
		
		
		if ( listaSolicitud != null && !listaSolicitud.isEmpty() ){
			params = new HashMap<String,Object>();
			params.put( "idToSolicitud", idsolicitud );
			params.put( "serie", numeroSerie );
			List<SolicitudDataEnTramiteDTO> solicitudDataEnTramite = entidadService.findByProperties( SolicitudDataEnTramiteDTO.class, params );
			listaSolicitud.get(0).setNombreCompleto(
					listaSolicitud.get(0).getNombrePersona().concat(" ").concat(
							StringUtil.isEmpty(listaSolicitud.get(0).getApellidoPaterno())? "" : listaSolicitud.get(0).getApellidoPaterno()).concat(
									" ").concat(StringUtil.isEmpty(listaSolicitud.get(0).getApellidoMaterno())?"":
										listaSolicitud.get(0).getApellidoMaterno()).trim());
			solicitudReporteCabina.setSolicitud( listaSolicitud.get(0) ); 
			solicitudReporteCabina.setSolicitudDataEnTramite( solicitudDataEnTramite.get(0) );
		}
		dataArg.put(ATR_REPORTE, reporte);
		dataArg.put(ATR_OFICINAID, reporte.getOficina().getId());
		dataArg.put(ATR_AUTO, auto);
		if(solicitudReporteCabina.getSolicitud() == null){
			dataArg.put("asegurado", "NO SE ENCUENTRA ASEGURADO EN LA CARTA");
				
		}else{
			dataArg.put("asegurado", solicitudReporteCabina.getSolicitud().getNombreCompleto());
		}
		dataArg.put("ciudad", reporte.getLugarOcurrido().getCiudad().getDescripcion().toString());
		dataArg.put("estado", reporte.getLugarOcurrido().getEstado().getDescripcion().toString());
		dataArg.put("fechaReporte", reporte.getFechaHoraOcurrido());
		dataArg.put("horaReporte", reporte.getHoraOcurrido());
		dataArg.put("ajustador", reporte.getAjustador().getNombrePersonaAjustador());
		dataArg.put("conductor", reporte.getPersonaConductor());
		dataArg.put("vehiculo",solicitudReporteCabina.getSolicitudDataEnTramite().getDescripcion() );
		dataArg.put("modelo", solicitudReporteCabina.getSolicitudDataEnTramite().getModelo());
		dataArg.put("serie", auto.getNumeroSerie());
		enviarNotificacionSiniestros(
				EnumCodigo.CARTA_COBERTURA_CON_POLIZA_INVALIDA + tipo,
				dataArg);
	}

	/***
	 * {@inheritDoc}
	 */
	@Override
	@Deprecated
	public void notificacionCartaCoberturaVariosIncisos(ReporteCabina reporte,
			PolizaDTO poliza, AutoIncisoReporteCabina auto, String tipo) {
		HashMap<String, Serializable> dataArg = new HashMap<String, Serializable>();
		dataArg.put(ATR_REPORTE, reporte);
		dataArg.put(ATR_OFICINAID, reporte.getOficina().getId());
		dataArg.put(ATR_POLIZA, poliza);
		dataArg.put(ATR_AUTO, auto);
		enviarNotificacionSiniestros(
				EnumCodigo.CARTA_COBERTURA_VARIOS_INCISOS + tipo,
				dataArg);
	}

	/***
	 * {@inheritDoc}
	 */
	@Override
	public void notificacionErrorOCRA(ReporteCabina reporte, Serializable error) {
		Map<String, Serializable> dataArg = new HashMap<String, Serializable>();
		dataArg.put(ATR_REPORTE, reporte.getNumeroReporte());
		dataArg.put(ATR_OFICINAID, reporte.getOficina().getId());
		dataArg.put(ATR_ERROR, reporte.getError());
		dataArg.put(ATR_ERRORCTG, "" );
		enviarNotificacionSiniestros(EnumCodigo.ERROR_ENVIO_OCRA.toString(), dataArg);
	}

	

	/***
	 * {@inheritDoc}
	 */
	@Override
	public void notificacionAfectacionIncisoCorreoCliente(ReporteCabina reporte) {
		Map<String, Serializable> dataArg = new HashMap<String, Serializable>();
		dataArg.put(ATR_REPORTE, reporte);
		
		enviarNotificacionSiniestros(
				EnumCodigo.AFECTACION_DE_INCISO_CORREO_CLIENTE.toString(),
				dataArg);
	}



	/***
	 * {@inheritDoc}
	 */
	@Override	
	public void enviarNotificacion(String codigo,
			Map<String, Serializable> dataArg){
		enviarNotificacion(codigo, dataArg, null, null);
	}
	
	/***
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Asynchronous
	public void enviarNotificacion(String codigo,
			Map<String, Serializable> dataArg, String folio, EmailDestinaratios destinatarios){
		enviarNotificacion(codigo, dataArg, folio, destinatarios, null);
	}
	
	/***
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Asynchronous
	public void enviarNotificacion(String codigo,
			Map<String, Serializable> dataArg, String folio, EmailDestinaratios destinatarios, 
			List<ByteArrayAttachment> adjuntos){
		
		this.enviarNotificacion(codigo,dataArg, folio, destinatarios, 
				adjuntos,null);			
	}
	
	/***
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Asynchronous
	public void enviarNotificacion(String codigo,
			Map<String, Serializable> dataArg, String folio, EmailDestinaratios destinatarios, 
			List<ByteArrayAttachment> adjuntos, 
			Map<String, String> mapInlineImages){
		boolean sinOficina = configService.movimientoSinOficina(codigo);
		Long oficinaId = null;
		
		if (codigo == null) {
			throw new NegocioEJBExeption("ENV_NOT_00",
					"Se requiere un código para enviar notificación");
		}
		if (dataArg == null) {
			throw new NegocioEJBExeption("ENV_NOT_01",
					"Se requiere un mapa de datos para enviar notificación");
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		Data data = new Data((HashMap)dataArg);
		if(!sinOficina){
			oficinaId = data.obtener(ATR_OFICINAID, Long.class);
			if (oficinaId == null) {
				throw new NegocioEJBExeption("ENV_NOT_02",
						"Es requerido setear la oficina para obtener la configuracion");
			}
		}
		
		try {
			// Obtener lista de configuraciones
			List<ConfiguracionNotificacionCabina> configs;
			if(!sinOficina){
				configs = obtenerConfiguraciones(
						codigo, oficinaId);
			}else{
				configs = obtenerConfiguraciones(
						codigo, null);
			}
			
			if(!configs.isEmpty()){			
				// Por cada configuracion a ejecutar
				for (ConfiguracionNotificacionCabina config : configs) {
					LOGGER.debug("Envio de notificacion para config de movimiento: "
							+ config.getMovimientoProceso().getDescripcion());
					if (config.esActiva() && 
							continuarEnvioPorConfAgentePolizaInciso(config, dataArg)) {
						if (config.getMovimientoProceso().esProgramada()) {
							programarNotificacion(config.getMovimientoProceso()
									.getTiempoTranscurrir(), config,
									dataArg);
						} else {
							try{
								notificarAhora(config, data, folio, destinatarios, adjuntos, mapInlineImages);
							}catch(Exception ex){
								LOGGER.error("No se pudo enviar el correo para la configuracion para el codigo: ".concat(codigo), ex);
							}
						}
					} else {
						LOGGER.debug("Envio desactivado de notificacion para config: "
								+ config.getMovimientoProceso().getDescripcion());
					}
				}
			}else{
				LOGGER.info("ENV_NOT_03 - No hay configuracion que notificar para el codigo - ".concat(codigo));
			}
		} catch (NegocioEJBExeption e) {
			LOGGER.error(e);
			throw e;
		} catch (Exception e) {
			LOGGER.error("Error al enviar notificaciones: " + e.getMessage(),e);
		}
	}
	
	/**
	 * Revisar parametros adicionales de Num Poliza, Num inciso y Agente dentro de la configuracion
	 * @param config
	 * @param dataArg
	 * @return
	 */
	private boolean continuarEnvioPorConfAgentePolizaInciso(ConfiguracionNotificacionCabina config, Map<String, Serializable> dataArg){					
		try{
			if(config.getAgenteId() != null){
				Agente agente = (Agente)dataArg.get(ATR_AGENTE);
				if(config.getAgenteId().intValue() != agente.getIdAgente().intValue()){
					throw new NegocioEJBExeption("001", "El agente de la poliza no esta configurado para esta notificacion");
				}														
			}
			if(!StringUtil.isEmpty(config.getNumPoliza())){								
				PolizaDTO poliza = (PolizaDTO)dataArg.get(ATR_POLIZA);
				if(!config.getNumPoliza().equals(poliza.getNumeroPolizaFormateada())){
					throw new NegocioEJBExeption("002", "El numero de poliza no esta configurado para esta notificacion");
				}
			}
			if(!StringUtil.isEmpty(config.getNumInciso())){
				Integer inciso = (Integer)dataArg.get(ATR_INCISO);
				String[] incisos;
				boolean contenidoEnIncisos = false;
				if(config.getNumInciso().contains("-")){
					//rango 1-10
					incisos = config.getNumInciso().split("-");
					if(inciso >= Integer.parseInt(incisos[0]) && 
							inciso  <= Integer.parseInt(incisos[1])){
						contenidoEnIncisos = true;
					} 
				}else{									
					incisos = config.getNumInciso().split(",");				
					for(String i : incisos){
						if(inciso == Integer.parseInt(i)){
							contenidoEnIncisos = true;
						}
					}
										
				}
				if(!contenidoEnIncisos){
					throw new NegocioEJBExeption("003", "El numero de inciso no esta configurado para esta notificacion");
				}
			}		
		}catch(NegocioEJBExeption ex){
			LOGGER.info(ex.getMessage(), ex);
			return false;
		}catch(Exception ex){			
			LOGGER.info("La configuracion contiene atributos adicionales de POLIZA/INCISO/AGENTE pero no pudo obtener estos valores del mapa de datos", 
					ex);
			return false;
		}		
		return true;				
	}
	
	
	public void notificarAhora(ConfiguracionNotificacionCabina config, Data data, String folio, 
			EmailDestinaratios otrosDestinatarios){
		notificarAhora(config, data, folio, otrosDestinatarios, null);
	}
	
	public void notificarAhora(ConfiguracionNotificacionCabina config, Data data, String folio, 
			EmailDestinaratios otrosDestinatarios, List<ByteArrayAttachment> adjuntos){
		this.notificarAhora(config, data, folio, otrosDestinatarios, adjuntos, null);
	}

	public void notificarAhora(ConfiguracionNotificacionCabina config, Data data, String folio, 
			EmailDestinaratios otrosDestinatarios, List<ByteArrayAttachment> adjuntos, 
			Map<String, String> mapInlineImages) {			
		if (config.esActiva() && validarEnvio(config, data)) {
			EmailDestinaratios destinatarios = obtenerDestinatarios(config,
					data, null);	
			destinatarios.integrar(otrosDestinatarios);
			String titulo = generarTitulo(config, data);			
			String cuerpoMensaje = generarMensaje(config, data);
			List<ByteArrayAttachment> docsAdjuntos;
			if(adjuntos == null){
				docsAdjuntos = new ArrayList<ByteArrayAttachment>();
			}else{
				docsAdjuntos = new ArrayList<ByteArrayAttachment>(adjuntos);
			}
			docsAdjuntos.addAll(obtenerAdjuntos(config));
			enviarEmail(destinatarios.getPara(), destinatarios.getCc(),
					destinatarios.getCco(), titulo, cuerpoMensaje, docsAdjuntos, mapInlineImages);
			if(config.getMovimientoProceso().getLlevarRegistro()){
				registrarNotificacion(folio, config.getMovimientoProceso().getCodigo(), 
						data.getDataMap(), destinatarios);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private void programarNotificacion(Long milisegundos,
			ConfiguracionNotificacionCabina config,
			Map<String, Serializable> dataArg) {
		if(sistemaContext.getTimerActivo()) {
			LOGGER.info("Entro a configurar timer config");
			dataArg.put("config", config.getId());		
			TimerConfig tconfig = new TimerConfig();
			tconfig.setPersistent(false);
			tconfig.setInfo((HashMap)dataArg);		
			timerService.createSingleActionTimer(milisegundos, tconfig);
		}
	}

	@SuppressWarnings("unchecked")
	@Timeout
	public void notificacionProgramada(Timer timer) {
		try {
			HashMap<String, Serializable> timerData;
			timerData = (HashMap<String, Serializable>) timer.getInfo();
			Long configId = (Long) timerData.get("config");
			ConfiguracionNotificacionCabina config = entidadService.findById(ConfiguracionNotificacionCabina.class, configId);			
			Data data = new Data(timerData);
			notificarAhora(config, data, null, null);
		} catch (Exception e) {
			LOGGER.error("fallo de correr el Scheduler WebSphere_EJB_Timer_Service (WebSphere_EJB_Timer_Service) por alguna excepcion: "
					+ e.getClass().toString()
					+ " ++ Mensaje: "
					+ e.getMessage(), e);
		}
	}

	private List<ConfiguracionNotificacionCabina> obtenerConfiguraciones(
			String codigo, Long oficinaId) {
		// Validar si con el codigo funciona el filtro, o habria que traer el id
		// del movimiento
		ConfiguracionNotificacionDTO configDTO = new ConfiguracionNotificacionDTO();
		configDTO.setCodigo(codigo);
		
		// # SI EL CODIGO CONTIENE EL POST-FIJO "_SIN_OFICINA" SE ELIMINA ESTA DEL FILTRO DE BUSQUEDA
		if(!configService.movimientoSinOficina(codigo)){
			configDTO.setOficinaId(oficinaId);
		}
		
		return configService.obtenerConfiguracionesPorFiltros(configDTO);
	}

	private EnumCodigo obtenerCodigoValueOf(String codigo) {
		EnumCodigo enumCodigo = null;
		try {
			enumCodigo = EnumCodigo.valueOf(codigo);
		} catch (Exception e) {
			LOGGER.error("Codigo: " + codigo + " no valido", e);
		}
		return enumCodigo;
	}

	private Boolean validarEnvio(ConfiguracionNotificacionCabina config,
			Data data) {

		EnumCodigo enumCodigo = obtenerCodigoValueOf(config
				.getMovimientoProceso().getCodigo());

		// Si codigo no tiene ENUM, no tiene validacion, por lo tanto siempre es
		// true
		if (enumCodigo == null) {
			return true;
		}

		switch (enumCodigo) {
		case SN_RCIA_EDO_CUENTA:
			break;
		case SN_RCIA_EDO_CUENTA_SNCORREO:
			break;
		case SVM_SOLICITUD_PRORROGA:
			break;
		case SVM_NOT_COMPRADOR_CANC_ADJUDICACION:
			break;
		case SVM_NOT_COMPRADOR_PRORROGA:
			break;
		case SVM_RECIBO_CONFORMIDAD:
			break;
		case SVM_ORDEN_SALIDA:
			break;
		case SVM_NOTIFICACION_COMPRADOR:
			break;
		case ENVIO_LIGA_AGENTE:
			break;
		case COTIZACION_LIGA_AGENTE:
			break;
		case CARATULA_CLIENTE_LIGA_AGENTE:
			break;
		case CABINA_ASIGNACION_AJUSTADOR:
		case CABINA_ARRIBO_AJUSTADOR:
		case CABINA_TERMINO_SINIESTRO:
		default:
			// en caso de un cambio en cabina, sacar informacion general

			ReporteCabina reporte = data
					.obtener(ATR_REPORTE, ReporteCabina.class);
			if (reporte == null) {
				// No se pudo obtener reporte para validar
				return false;
			}else{
				reporte = 
					entidadService.findById(ReporteCabina.class, reporte.getId());
			}

			CitaReporteCabina cita = reporte.getCitaReporteCabina();
			if (cita != null && cita.getFechaCita() != null) {
				 // no ha notificaciones para cuando hay cita.
				return false;
			}

			switch (enumCodigo) {
			case CABINA_ASIGNACION_AJUSTADOR:
				if (reporte.getFechaHoraAsignacion() != null){
					 // si ya se ha asignado ajustador
					return false;
				}
				break;
			case CABINA_ARRIBO_AJUSTADOR:
				if (reporte.getFechaHoraContacto() != null){
					 // si ya ha llegado ajustador
					return false;
				}
				break;
			case CABINA_TERMINO_SINIESTRO:
				if (reporte.getFechaHoraTerminacion() != null){
					return false;
				}
				break;
			default:
				break;
			}
			break;
		}
		return true;
	}

	private String generarTitulo(ConfiguracionNotificacionCabina config,
			Data dataMap) {				
		Map<String, Map<String, Object>> mapaPlantilla = llenarMapaDatos(dataMap);
		return formateatPlantilla(config.getMovimientoProceso().getTitulo(),
				mapaPlantilla);
	}
	
	private  Map<String, Map<String, Object>> llenarMapaDatos(Data dataMap){
		Map<String, Map<String, Object>> mapaPlantilla = new HashMap<String, Map<String, Object>>();
		Map<String, Serializable> data = dataMap.getDataMap();
		Set<String> keys = data.keySet();
		Iterator<String> iterator = keys.iterator();
		while(iterator.hasNext()){
			String key = iterator.next();
			Serializable objeto = data.get(key);
			if(objeto instanceof Entidad){
				Map<String, Object> mapa = convertirObjetoAMapa(objeto);			
				mapaPlantilla.put(key, mapa);
			}else{
				Map<String, Object> mapa = new HashMap<String, Object>();
				mapa.put(key, objeto);
				mapaPlantilla.put(key, mapa);
			}
		}		
		return mapaPlantilla;
	}

	

	private static String formateatPlantilla(String plantilla,
			Map<String, Map<String, Object>> mapa){
		Matcher m = PATTERN.matcher(plantilla);
		String result = plantilla;
		while (m.find()) {
			try {
				String[] textoAReemplazar = m.group(1).split("\\.");				
				Map<String, Object> objetoMapa;
				String propiedad;
				String newVal;
				//parche para template de propiedad simple
				if(textoAReemplazar.length == 1){
					LOGGER.info("EnvioNotificaciones => Buscando propiedad " + textoAReemplazar[0]);
					if (mapa.containsKey(textoAReemplazar[0])) {
						objetoMapa = mapa.get(textoAReemplazar[0]);
						newVal = obtenerValorMapa(objetoMapa, textoAReemplazar[0]);		
						result = result.replaceFirst(REGEX, newVal);
					}else {
						throw new NegocioEJBExeption("ENV_NOT_05",
								"Error al generar la plantilla, propiedad: "
										+ textoAReemplazar[0]
										+ " no valida del objeto en mapa. ");
					}										
				}else{
					LOGGER.info("EnvioNotificaciones => Buscando propiedad " + textoAReemplazar[0]+"."+textoAReemplazar[1]);
					if (mapa.containsKey(textoAReemplazar[0])) {
						objetoMapa = mapa.get(textoAReemplazar[0]);
					} else {
						throw new NegocioEJBExeption("ENV_NOT_04",
								"Error al generar la plantilla, falta agregar al mapa el objeto: "
										+ textoAReemplazar[0]);
					}
					propiedad = textoAReemplazar[1];
					if (objetoMapa.containsKey(propiedad)) {
						newVal = obtenerValorMapa(objetoMapa, propiedad);		
						result = result.replaceFirst(REGEX, newVal != null ? newVal : "");										
					} else {
						throw new NegocioEJBExeption("ENV_NOT_05",
								"Error al generar la plantilla, propiedad: "
										+ propiedad
										+ " no valida del objeto en mapa: "
										+ textoAReemplazar[0]);
					}					
				}
			} catch (Exception e) {
				LOGGER.error(e);
				throw new NegocioEJBExeption("ENV_NOT_06",
						"Error al generar la plantilla, validar textos "
								+ "a reemplazar de tipo {$keyMap.propiedad}: " + (e.getMessage() != null ? e.getMessage() : ""));
			}
		}
		return result;
	}

	private static String obtenerValorMapa(Map<String, Object> objetoMapa, String propiedad){
		String newVal = "";		
		Object obj = objetoMapa.get(propiedad);
		if(obj != null){
			newVal = obj.toString();
		}else{
			LOGGER.debug("EnvioNotificaciones => Propiedad vacia para " + propiedad);
		}
		return newVal;
	}
	
	
	@SuppressWarnings("unchecked")
	private static Map<String, Object> convertirObjetoAMapa(Object objeto) {
		Map<String, Object> mapaObjeto = new HashMap<String, Object>();
		Object entidad;
		try {
			if(objeto != null){
				//Para evitar problema de lazy initialization
				if(objeto instanceof Entidad){
					entidad = cargarObjeto((Entidad)objeto);
				}else{
					entidad = objeto;
				}
				mapaObjeto = BeanUtils.describe(entidad);
			}
		} catch (Exception e) {		
			LOGGER.error("No se genero Mapa apartir de objeto", e);
		}

		return mapaObjeto;
	}
	
	@SuppressWarnings("unchecked")
	private static <E extends Entidad, K> E cargarObjeto(E obj){
		E entidad = null;
		try{
			K id = obj.getKey();
			entidad = (E)entidadService.findById(obj.getClass(), id);			
		}catch(Exception e){
			LOGGER.error("No se puede cargar el objeto de tipo " + obj.getClass().getName() + " para el valor " + obj, e);
		}
		return entidad;		
	}

	private String generarMensaje(ConfiguracionNotificacionCabina config,
			Data dataMap){			
		Map<String, Map<String, Object>> mapaPlantilla = llenarMapaDatos(dataMap);
		Map<String, Object> notas = new HashMap<String, Object>();
		notas.put("nota", !StringUtil.isEmpty(config.getNotas()) ? config.getNotas() : "");
		mapaPlantilla.put("notas", notas);
		return formateatPlantilla(config
				.getMovimientoProceso().getPlantilla(), mapaPlantilla);					
	}

	private void cargarCatalogosDestinatarios() {
		mapModoEnvio = listadoService
				.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.MODO_ENVIO);
		mapTipoDesti = listadoService
				.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_DESTINATARIO);
		mapTipoCorre = listadoService
				.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_CORREO);
	}

	private EmailDestinaratios obtenerDestinatarios(
			ConfiguracionNotificacionCabina config, Data dataArg, EmailDestinaratios otrosDestinatariosDirectos){
		EmailDestinaratios listaDestinatarios = new EmailDestinaratios();
		List<DestinatarioConfigNotificacionCabina> destinatariosConfig = config
				.getDestinatariosNotificacion();

		cargarCatalogosDestinatarios();

		for (DestinatarioConfigNotificacionCabina destinatarioConfig : destinatariosConfig) {
			String strModoEnvio = null;
			EnumModoEnvio enumModoEnvio = null;
			String strTipoDest = null;
			EnumTipoDestinatario enumTipoDest = null;
			String strTipoCorreo = null;
			EnumTipoCorreo enumTipoCorreo = null;

			try {
				// Obtener el modo de envio tipo Enum (PARA, CC, CCO)
				strModoEnvio = mapModoEnvio.get(destinatarioConfig
						.getModoEnvio().toString());
				enumModoEnvio = EnumModoEnvio.valueOf(enumString(strModoEnvio));

				// Obtener el tipo destinatario de tipo Enum ([Roles])
				strTipoDest = mapTipoDesti.get(destinatarioConfig
						.getTipoDestinatario().toString());
				try {
					enumTipoDest = EnumTipoDestinatario
							.valueOf(enumString(strTipoDest));
				} catch (Exception e) {
					// Si no se tiene el strTipoDest se deduce que es un rol
					try {
						strTipoDest = strTipoDest.replaceAll("\\s", "_");
						strTipoDest = EnumTipoDestinatario.ROL_PREFIJO
								+ strTipoDest;
						enumTipoDest = EnumTipoDestinatario.ROL;
					} catch (Exception ex) {
						LOGGER.error("Error al general el nombre del rol", ex);
						continue;
					}
				}
				// Obtener el tipo de correo de tipo Enum (COREOPERSONAL,
				// CORREOSADICIONALES o CORREOSTODOS) CORREOERSONAL por default
				enumTipoCorreo = EnumTipoCorreo.CORREOPERSONAL;

				if (enumTipoDest != EnumTipoDestinatario.OTROS) {
					strTipoCorreo = mapTipoCorre.get(destinatarioConfig
							.getCorreosEnvio().toString());
					enumTipoCorreo = EnumTipoCorreo
							.valueOf(enumString(strTipoCorreo));
				}
			} catch (Exception e) {
				LOGGER.error("error al sacar los destinatarios", e);
			}

			// Si no se el tipo destinatario (rol) o el modo envio (Para, CC o
			// CCO) pasar al siguiente destinatarioConfig
			if (enumTipoDest == null || enumModoEnvio == null) {
				continue;
			}

			try{
				// Sacar correo(s) usando el tipo de destinatario
				switch (enumTipoDest) {
				case OTROS:
					// Obtener correo y nombre desde la configuracion (nombre,
					// puesto, correo)
					String correoOtro = destinatarioConfig.getCorreo();
					String nombreOtro = destinatarioConfig.getNombre();
	
					listaDestinatarios.agregar(enumModoEnvio, correoOtro,
							nombreOtro);
					break;
				case ROL:
					List<Usuario> usuarios = null;
					try {
						usuarios = usuarioService
								.buscarUsuariosPorNombreRol(strTipoDest);
					} catch (Exception e) {
						LOGGER.error("Usuario(s) por rol no encontrado", e);
					}
	
					if (usuarios != null) {
						for (Usuario usuario : usuarios) {
	
							// Obtener correo(s) de usuario deacuerdo al tipo de
							// coreo
							for (String correo : obtenerCorreos(usuario,
									enumTipoCorreo)) {
								listaDestinatarios.agregar(enumModoEnvio, correo,
										usuario.getNombreCompleto());
							}
	
						}
					}
					break;
				case AJUSTADOR:
					PersonaMidas ajustador = dataArg.obtener(ATR_AJUSTADOR,
							PersonaMidas.class);
	
					for (String correo : obtenerCorreos(ajustador, enumTipoCorreo)) {
						listaDestinatarios.agregar(enumModoEnvio, correo,
								ajustador.getNombre());
					}
					break;
				case CABINERO:
					// Cabinero se toma como la persona que atendio el reporte
					Usuario cabinero = dataArg.obtener(ATR_CABINERO, Usuario.class);
	
					for (String correo : obtenerCorreos(cabinero, enumTipoCorreo)) {
						listaDestinatarios.agregar(enumModoEnvio, correo,
								cabinero.getNombreCompleto());
					}
					break;
				case AGENTES:
					Agente agente = dataArg.obtener(ATR_AGENTE, Agente.class);
	
					for (String correo : obtenerCorreos(agente, enumTipoCorreo)) {
						listaDestinatarios.agregar(enumModoEnvio, correo,
								agente.getPersona().getNombreCompleto());
					}
	
					break;
				case CLIENTE:
					ClienteDTO clienteDTO = dataArg.obtener(ATR_CLIENTE,
							ClienteDTO.class);
					listaDestinatarios.agregar(enumModoEnvio,
							clienteDTO.getEmail(), clienteDTO.getNombre());
					break;
				case INTERESADO:
					try{
						String nombreInteresado = dataArg.obtener(ATR_INTERESADO, String.class);
						String[] emailInteresado = dataArg.obtener(ATR_EMAILINTERESADO, String.class).split(";");
						for(String email : emailInteresado){
							listaDestinatarios.agregar(enumModoEnvio, email, nombreInteresado);
						}
					}catch(Exception ex){
						LOGGER.error("No se econtro informacion para el envio de notificacion del interesado ", ex);
					}
					break;
				default:
					break;
				}
			}catch(Exception ex){
				LOGGER.error("No pudo obtenerse el correo para el tipo de destinatario " + enumTipoDest, ex);
			}
		}
		return listaDestinatarios;
	}

	private String enumString(String enumString) {
		return enumString.toUpperCase().replaceAll("\\.", "")
				.replaceAll("\\s", "");		
	}

	private List<String> obtenerCorreos(Agente agente,
			EnumTipoCorreo enumTipoCorreo) {
		List<String> correosARegresar = new ArrayList<String>();
		switch (enumTipoCorreo) {
		// case para ejecutar los dos casos
		case CORREOSTODOS:
		case CORREOSADICIONALES:
			// Obtener correos adicionales
			String correos = agente.getPersona().getCorreosAdicionales();
			if (correos != null && correos.length() > 0){
				String[] adicionales = correos.split(";");
				for(String str : adicionales){
					correosARegresar.add(str);
				}
			}
			
			// break solo en caso de tipo de correo = ADICIONALES
			if (enumTipoCorreo.ordinal() == EnumTipoCorreo.CORREOSADICIONALES
					.ordinal()){
				break;
			}
		case CORREOPERSONAL:
			String correo = agente.getPersona().getEmail();
			if (correo != null && correo.length() > 0){
				correosARegresar.add(correo);
			}
			
		default:
			LOGGER.debug("Tipo de correo no valido");
			break;
		}
		return correosARegresar;
	}

	private List<String> obtenerCorreos(Usuario usuario,
			EnumTipoCorreo enumTipoCorreo) {
		List<String> correosARegresar = new ArrayList<String>();
		switch (enumTipoCorreo) {
		// case para ejecutar los dos casos
		case CORREOSTODOS:
			// No existe correos adicionales para Usuarios
		case CORREOSADICIONALES:
		case CORREOPERSONAL:
			String correo = usuario.getEmail();
			if (correo != null && correo.length() > 0){
				correosARegresar.add(correo);
			}
			break;
		default:
			LOGGER.debug("Tipo de correo no valido");
		}
		return correosARegresar;
	}

	private List<String> obtenerCorreos(PersonaMidas persona,
			EnumTipoCorreo enumTipoCorreo) {
		List<String> correosARegresar = new ArrayList<String>();
		switch (enumTipoCorreo) {
		// case para ejecutar los dos casos
		case CORREOSTODOS:
		case CORREOSADICIONALES:
			String correosAdicionales = persona.getContacto()
					.getCorreosAdicionales();
			try {
				// Correos Adicionales deacuerdo a MailServiceSupport @67
				if (correosAdicionales != null
						&& correosAdicionales.length() > 0){
					correosARegresar.addAll(java.util.Arrays
							.asList(correosAdicionales.split(";")));
				}
			} catch (Exception e) {
				LOGGER.error("error al sacar correos adicionales", e);
			}

			// break solo en caso de tipo de correo = ADICIONALES
			if (enumTipoCorreo.ordinal() == EnumTipoCorreo.CORREOSADICIONALES
					.ordinal()){
				break;
			}
		case CORREOPERSONAL:
			String correo = persona.getContacto().getCorreoPrincipal();
			if (correo != null && correo.length() > 0){
				correosARegresar.add(correo);
			}
			break;
		default:
			LOGGER.debug("Tipo de correo no valido");
		}
		return correosARegresar;
	}

	/**
	 * Este método busca los archivos adjuntos relacionados a una configuración específica y los devuelve como ByteArrayAttachment
	 * para ser incluidos como adjuntos en la notificacion por correo.
	 * @param config recibe la configuracion del envío de notificaciones
	 * @return Listado de ByteArrayAttachment
	 */
	private List<ByteArrayAttachment> obtenerAdjuntos(
			ConfiguracionNotificacionCabina config) {
		List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
		List<AdjuntoConfigNotificacionCabina> archivosAdjuntos = configService.obtenerAdjuntos(config);
		for(AdjuntoConfigNotificacionCabina adj : archivosAdjuntos){
			ByteArrayAttachment attatchment =  new ByteArrayAttachment();
			attatchment.setContenidoArchivo(adj.getArchivo());
			attatchment.setNombreArchivo(adj.getDescripcion());
			
			if (adj.getTipoArchivo()==null){
				attatchment.setTipoArchivo(TipoArchivo.DESCONOCIDO);
			}else if(adj.getTipoArchivo().equals("application/pdf")){
				attatchment.setTipoArchivo(TipoArchivo.PDF);
			}else if(adj.getTipoArchivo().equals("image/jpeg")){
				attatchment.setTipoArchivo(TipoArchivo.IMAGEN_JPG);
			}else if(adj.getTipoArchivo().equals("text/plain")){
				attatchment.setTipoArchivo(TipoArchivo.TEXTO);
			}else if(adj.getTipoArchivo().equals("text/html")){
				attatchment.setTipoArchivo(TipoArchivo.HTML);
			}else if(adj.getTipoArchivo().equals("application/pdf")){
				attatchment.setTipoArchivo(TipoArchivo.PDF);
			}else{
				attatchment.setTipoArchivo(TipoArchivo.DESCONOCIDO);
			}
			
			adjuntos.add(attatchment);
		}
		return adjuntos;
	}

	private void enviarEmail(List<String> para, List<String> cc,
			List<String> cco, String titulo, String cuerpoMensaje,
			List<ByteArrayAttachment> adjuntos, Map<String, String> mapInlineImages) {
		if(para == null || para.isEmpty()){
			LOGGER.info("No existen datos para el tipo de remitente PARA, se utiliza CC o BCC");
			if(cc != null && !cc.isEmpty()){
				para = new ArrayList<String>(cc);
				cc = new ArrayList<String>();
			}else if(cco != null && !cco.isEmpty()){
				para = new ArrayList<String>(cco);
				cco = new ArrayList<String>();
			}
		}				
		LOGGER.debug("Envio de correo con datos: ");
		LOGGER.debug("para: " + (CommonUtils.isNotEmptyList(para) ? para.toString() : ""));
		LOGGER.debug("cc: " + (CommonUtils.isNotEmptyList(cc) ? cc.toString() : "")); 
		LOGGER.debug("cco: " + (CommonUtils.isNotEmptyList(cco) ? cco.toString() : ""));
		LOGGER.debug("titulo:  " + titulo);
		LOGGER.debug("mensaje: " + cuerpoMensaje);
		LOGGER.debug("aduntos: " + adjuntos.toString());

		
		if (CommonUtils.isNotEmptyList(para) || CommonUtils.isNotEmptyList(cc) || CommonUtils.isNotEmptyList(cco)){
			mailService.sendMessage(para, titulo, cuerpoMensaje,
					"midasweb@afirme.com", null, adjuntos, cc, cco, mapInlineImages);
		}
	}

	/***
	 * {@inheritDoc}
	 */
	@Override
	public void notificacionDepuracionReserva(ReporteCabina reporte, ConfiguracionDepuracionReserva configuracion) {
		HashMap<String, Serializable> dataArg = new HashMap<String, Serializable>();
		dataArg.put(ATR_REPORTE, reporte);		
		dataArg.put(ATR_CONF, configuracion);
		enviarNotificacionSiniestros(
				EnumCodigo.DEPURACION_RESERVA.toString(),
				dataArg);
	}
	
	
	@Override
	public void notificacionSinConceptoAjuste(ReporteCabina reporte) {
		Map<String, Serializable> dataArg = new HashMap<String, Serializable>();
		dataArg.put(ATR_REPORTE, reporte);
		dataArg.put(ATR_OFICINAID, reporte.getOficina().getId());
		enviarNotificacionSiniestros(EnumCodigo.SIN_CONCEPTO_COBERTURA.toString(), dataArg);
	}
	
	@Override
	public void notificacionConvertirSiniestroHgs(ReporteCabina reporte, String folio) {
		Map<String, Serializable> dataArg = new HashMap<String, Serializable>();
		dataArg.put(ATR_FOLIO, folio);
		dataArg.put(ATR_REPORTE, reporte);
		dataArg.put(ATR_OFICINAID, reporte.getOficina().getId());
		enviarNotificacionSiniestros(EnumCodigo.CONVERTIR_SINIESTRO_HGS_PERDIDA_TOTAL.toString(), dataArg);
	}	
	
	@Override
	public void notificarErrorValidacionValuacion(String reporte, String mensaje) {
		Map<String, Serializable> dataArg = new HashMap<String, Serializable>();
		dataArg.put(ATR_REPORTE, reporte);
		dataArg.put("reglas", mensaje);
		
		enviarNotificacionSiniestros(EnumCodigo.ERROR_VALIDACION_VALUACION.toString(), dataArg);
	}

	@Override
	public void notificarErrorValidacionValuacion(String reporte, String id, String nombre) {
		Map<String, Serializable> dataArg = new HashMap<String, Serializable>();
		dataArg.put(ATR_REPORTE, reporte);
		dataArg.put("id"     , id);
		dataArg.put("nombre" , nombre);
		enviarNotificacionSiniestros(EnumCodigo.PRESTADOR_SERVICIO_HGS_NO_VALIDO.toString(), dataArg);
	}

	@Override
	public void notificarRecurrentes(){
		LOGGER.info("Iniciando tarea notificarRecurrentes()...");
		List<BitacoraRegistroDetalleNotificacionDTO> recurrentes = bitacoraNotificacionService.obtenerRecurrentes();
		Long fechaActual = (new Date()).getTime();		
		Type mapType = new TypeToken<Map<String, Serializable>>() {}.getType();
		Gson gson = new GsonBuilder().registerTypeAdapter(Serializable.class, 
				new SerializableTypeAdapter()).setDateFormat("dd/MM/yyyy").create();
		for(BitacoraRegistroDetalleNotificacionDTO dto : recurrentes){
			//es recurrente
			try{
				if(dto.getConfiguracion().getMovimientoProceso().getRecurrente()){
					Long tiempoRecurrencia = dto.getConfiguracion().getMovimientoProceso().getTiempoRecurrencia();
					Long fechaEnvio = dto.getFechaEnvio().getTime();
					//el tiempo de recurrencia ya expiro?
					if(dto.getConfiguracion().esActiva() && ((fechaActual - fechaEnvio) >= tiempoRecurrencia)){
						//enviar nuevamente la notificacion llenando el template				
						HashMap<String, Serializable> dataArg = gson.fromJson(dto.getMapaDatos(), mapType);
						Integer consecutivo = dto.getSecuenciaEnvio();
						dataArg.put("secuenciaEnvio", ++consecutivo);
						Data data = new Data(dataArg);
						List<String> destinatario = new ArrayList<String>();
						destinatario.add(dto.getDestinatario());						
						String titulo = generarTitulo(dto.getConfiguracion(), data);
						String cuerpoMensaje = generarMensaje(dto.getConfiguracion(), data);
						List<ByteArrayAttachment> adjuntos = obtenerAdjuntos(dto.getConfiguracion());
						enviarEmail(destinatario, null, null, titulo, cuerpoMensaje, adjuntos, null);
						//TODO implementar logica adicional si se requiere enviar imagenes embebidas
						if(dto.getConfiguracion().getMovimientoProceso().getLlevarRegistro()){
							bitacoraNotificacionService.salvarBitacora(dto.getFolio(), 
									dto.getConfiguracion().getMovimientoProceso().getCodigo(), 
									dto.getDestinatario(), dataArg);
						}
					}
					
				}	
			}catch(Exception ex){
				LOGGER.error("No se pudo enviar la notificacion recurrente con id registro: " + dto.getIdRegistro(), ex);
			}
		}
	}
	
	public void registrarNotificacion(String folio, String codigo, Map<String, Serializable> mapaDatos, 
			EmailDestinaratios destinatarios){
		String id = folio; 
		if(StringUtil.isEmpty(folio)){
			id = codigo.concat("-").concat(String.valueOf((new Date()).getTime()));
		}		
		if(destinatarios != null){
			bitacoraNotificacionService.salvarBitacora(id, codigo, destinatarios, mapaDatos);			
		}
		
	}
	
	public static class SerializableTypeAdapter implements
			JsonSerializer<Object>, JsonDeserializer<Serializable>,
			InstanceCreator<Serializable> {

		@Override
		public JsonElement serialize(Object src, Type typeOfSrc,
				JsonSerializationContext context) {
			return new JsonPrimitive(src.toString());
		}

		@Override
		public Serializable deserialize(JsonElement json, Type typeOfT,
				JsonDeserializationContext context) throws JsonParseException {			
			JsonPrimitive jsonPrimitive = json.getAsJsonPrimitive();			
			if (jsonPrimitive.isString()) {
				return jsonPrimitive.getAsString();
			}else if (jsonPrimitive.isNumber()){
				return jsonPrimitive.getAsNumber();
			}else if (jsonPrimitive.isJsonArray()){
				return jsonPrimitive.getAsJsonArray().toString();
			}else if (jsonPrimitive.isJsonObject()){
				return jsonPrimitive.getAsJsonObject().toString();
			}else {
				throw new IllegalStateException(
						"Expected a string/number/array/object field, but was " + json);
			}
		}

		@Override
		public Serializable createInstance(Type type) {
			return "";
		}
	}

	/**
	 * Define un adjunto de tipo <code>ByteArrayAttachment</code> que sirve para enviar documentos en correos
	 * @param adjunto Arreglo de bytes que contiene el archivo
	 * @param nombreArchivo Nombre del archivo
	 * @param tipoArchivo Tipo de archivo definido en ByteArrayAttachment.TipoArchivo
	 * @return <code>ByteArrayAttachment</code> conteniendo el archivo a adjuntar en el correo
	 */
	@Override
	public ByteArrayAttachment generarAdjuntoCorreo(byte[] adjunto,
			String nombreArchivo, TipoArchivo tipoArchivo) {
		ByteArrayAttachment attachment =  new ByteArrayAttachment();
		attachment.setContenidoArchivo(adjunto);
		attachment.setNombreArchivo(nombreArchivo);
		attachment.setTipoArchivo(tipoArchivo);
		return attachment;
	}
}
