package mx.com.afirme.midas2.service.impl.movil.cotizador.vida;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.naming.Context;

import org.apache.commons.dbutils.DbUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;

import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas2.service.movil.cotizador.vida.ImpresionCotizacionMovilVidaService;
import mx.com.afirme.vida.domain.movil.cotizador.QuotationParametersDTO;
import mx.com.afirme.vida.domain.movil.cotizador.ReportConfigurationDTO;
import mx.com.afirme.vida.domain.movil.cotizador.ReportServiceDTO;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 */
@Stateless
public class ImpresionCotizacionMovilVidaServiceImpl
		implements ImpresionCotizacionMovilVidaService {

	ByteArrayOutputStream output;
	private SimpleJdbcCall buscaCotizacionImpresion;
	
	public byte[] getLifeInsuranceApplication(String identifier,
		      String contentTypeReport){
			  ReportServiceDTO reportDTO = new ReportServiceDTO();
		    byte[] byteArray = null;
		    List inputByteArray = new ArrayList();
		    ReportConfigurationDTO confDTO = null;
		    //Connection connection = null;
		    output = new ByteArrayOutputStream();
		    try {
		      //ReportHelper helper = ReportHelper.getInstance();
		      reportDTO.setQuotationId(identifier);
		      //LogDeMidasInterfaz.info("quotationId==>" + identifier);
		      reportDTO.setContentTypeReport(contentTypeReport);
		      //LogDeMidasInterfaz.info("contentTypeReport==>" + contentTypeReport);

		      reportDTO = getLifeInsuranceApplication(reportDTO);
		      //connection = ConnectionManager.getConnection();
		      //LogDeMidasInterfaz.info("La conexion esta cerrada? " + connection.isClosed());

		      //Get all reports
		      Iterator iterator = reportDTO.getReports().iterator();
		      while(iterator.hasNext()){
		        confDTO = (ReportConfigurationDTO)iterator.next();
		        try{
		          byteArray = generateReport(reportDTO.getContentTypeReport(),
		              confDTO.getReport(), confDTO.getParameters());
		        }catch(JRException ex){
		            LogDeMidasInterfaz.log("Error while generating report for type " +
		                confDTO.getReportType() + " for quotation " +
		                identifier + ". Returning null at that report ",  Level.INFO, null);
		              byteArray = null;
		        }
		      } //end if

		      for(int i = 0; i < confDTO.getNumOfCopies(); i++){
		        inputByteArray.add(byteArray);
		      }
		      //generate single PDF
		      pdfConcantenate(inputByteArray, output);
		      LogDeMidasInterfaz.log("PrintReport: Se genero el reporte exitosamente", Level.INFO, null);
		    } catch (Exception e){
		      LogDeMidasInterfaz.log("Exception ==> "+ e, Level.INFO, null);
		    }
		    return output != null ? output.toByteArray() : null;
	}
	public ReportServiceDTO getLifeInsuranceApplication(
		      ReportServiceDTO reportDTO) {
		    QuotationParametersDTO quotationDTO = null;
		    List reports = new ArrayList();

		    
		    if (reportDTO.getQuotationId() != null) {
		      quotationDTO = buscarCotizacionImpresion(
		            new Integer(reportDTO.getQuotationId()));
		    }else {
		     
		    }

		    reports.add(getLifeInsuranceApplicationReport(reportDTO, quotationDTO));
		    reportDTO.setReports(reports);
		    return reportDTO;
	}

	public QuotationParametersDTO buscarCotizacionImpresion(Integer quotationId) {
		List<QuotationParametersDTO> quotationDTOList;
		QuotationParametersDTO quotationDTO = new QuotationParametersDTO();
		try {
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter.addValue("P_IDENTIFIER_ID", quotationId.toString())
					.addValue("P_STATUS", 1);

			Map<String, Object> execute = this.buscaCotizacionImpresion
					.execute(sqlParameter);
			BigDecimal idCodResp = (BigDecimal) execute.get("P_MESSAGE");
			quotationDTOList = (List<QuotationParametersDTO>) execute
					.get("P_RESULT_CURSOR");
			quotationDTO = quotationDTOList.get(0);
		} catch (Exception e) {
			quotationDTO = null;
	    	LogDeMidasInterfaz.log("Exception ==> "+ e, Level.INFO, null);
		}
		return quotationDTO;
	}
	 @SuppressWarnings("unchecked")
	public Object getLifeInsuranceApplicationReport(ReportServiceDTO reportDTO,
		      QuotationParametersDTO quotationDTO ) {
		  ReportConfigurationDTO confDTO = new ReportConfigurationDTO();

		  try{
		    JasperReport report = 
		      getJasperReport("/mx/com/afirme/vida/service/impl/impresiones/jrxml/" +
		          "SolicitudDeAseguramiento.jrxml");
		    Map parameters = new HashMap();
		      parameters.put("P_ID_COTIZACION", new Integer(reportDTO.getQuotationId()));
		      parameters.put("IMAGE_LOGO",SistemaPersistencia.LOGO_AFIRME_SEGUROS_INVERTIDO);
		      parameters.put("SUBREPORTE_CONTRATANTE",
		          getJasperReport("/mx/com/afirme/vida/service/impl/impresiones/jrxml/" +
		                  "SubreporteSolicitudDatosContratante.jrxml"));

		      parameters.put("SUBREPORTE_TITULAR",
		          getJasperReport("/mx/com/afirme/vida/service/impl/impresiones/jrxml/" +
		                  "SubreporteSolicitudDatosTitular.jrxml"));

		      parameters.put("SUBREPORTE_BENEFICIARIOS",
		          getJasperReport("/mx/com/afirme/vida/service/impl/impresiones/jrxml/" +
		                  "SubreporteSolicitudDatosBeneficiarios.jrxml"));

		     parameters.put("SUBREPORTE_BENEFICIARIOS_SUB",
		          getJasperReport("/mx/com/afirme/vida/service/impl/impresiones/jrxml/" +
		                "SubreporteSolicitudDatosBeneficiarioSub.jrxml"));

		      parameters.put("SUBREPORTE_CUESTIONARIO",
		          getJasperReport("/mx/com/afirme/vida/service/impl/impresiones/jrxml/" +
		                "SubreporteSolicitudCuestionario.jrxml"));

		      if(quotationDTO.getHasMoreThanOneInsuredPerson()){
		        parameters.put("SUBREPORTE_ASEGURADOS",
		            getJasperReport("/mx/com/afirme/vida/service/impl/impresiones/jrxml/" +
		                  "SubreporteSolicitudDatosAsegurados.jrxml"));
		      }

		      //LOG.info("Success load DTO from getLifeInsurancePolicy method in class " +
		        //  "ReportHelper");

		      confDTO.setParameters(parameters);
		      confDTO.setReport(report);
		      confDTO.setReportType("LIFE_APPLICATION");
		      //FIXME la tabla TBL_PROPERTY no acepta valores de mas de 40 caracteres
		      confDTO.setNumOfCopies(1);
		    } catch (JRException e) {
		       LogDeMidasInterfaz.log("Error while configuring application report ==> "+ e, Level.INFO, null);
		    }
		    return confDTO;
		 }

	public JasperReport getJasperReport(String internalURL)
			throws JRException {
		JasperReport jasperReport = null;
		if (jasperReport == null) {
			InputStream reportStream = ImpresionCotizacionMovilVidaServiceImpl.class
					.getResourceAsStream(internalURL);
			JasperDesign jasperDesign = JRXmlLoader.load(reportStream);
			jasperReport = JasperCompileManager.compileReport(jasperDesign);
			try {
				reportStream.close();
			} catch (IOException exception) {
		    	LogDeMidasInterfaz.log("Exception ==> "+ exception, Level.INFO, null);
			} // End of try/catch
		} // End of if
		return jasperReport;
	}

	public byte[] generateReport(String reportType, JasperReport jasperReport,
			Map parameters) throws JRException {
		JasperPrint jasperPrint;
		Connection connection=null;
		Context ctx;
		ByteArrayOutputStream outputStream=null;
		/*try {
			ctx = new InitialContext();
			connection = dataSource.getConnection();
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}*/
		
		try{
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,connection);
			outputStream = new ByteArrayOutputStream();
			JRPdfExporter pdfExporter = new JRPdfExporter();
			pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
					outputStream);
			pdfExporter.exportReport();
		}
		catch(Exception e){
	    	 LogDeMidasInterfaz.log("Exception ==> "+ e, Level.INFO, null);
		}finally{
	    	DbUtils.closeQuietly(connection);
		}
		
		return outputStream.toByteArray();

	}
	public void  pdfConcantenate(List inputByteArray, OutputStream outputStream){
	    //LOG.info("Beginning PDF copy");
	      try {
	          int pageOffset = 0;
	          int f = 0;
	          ArrayList master = new ArrayList();
	          Document document = null;
	          PdfCopy  writer = null;

	          // we create a reader for a certain document

	          Iterator iterator = inputByteArray.iterator();

	          while(iterator.hasNext()){
	            //LOG.info("Copying document number " + (f + 1));
	            byte[] data = (byte[])iterator.next();
	            if(data == null){
	              //LOG.info("This PDF has null data, moving to next one...");
	              f++;
	              continue;
	            }
	            PdfReader reader;
	            try {
	            	 reader = new PdfReader(data);
	            } catch (Exception e) {
	  	    	    LogDeMidasInterfaz.log("Unable to finish PDF Copy ==> "+ e, Level.INFO, null);
	                continue;
	            }
	            reader.consolidateNamedDestinations();
	            // we retrieve the total number of pages
	            int n = reader.getNumberOfPages();
	            List bookmarks = SimpleBookmark.getBookmark(reader);
	            if (bookmarks != null) {
	              if (pageOffset != 0)
	                SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
	                master.addAll(bookmarks);
	            }

	            pageOffset += n;

	            if (f == 0) {
	              // step 1: creation of a document-object
	              document = new Document(reader.getPageSizeWithRotation(1));
	              // step 2: we create a writer that listens to the document
	              writer = new PdfCopy(document, outputStream);
	              // step 3: we open the document
	              document.open();
	            }

	            // step 4: we add content
	            PdfImportedPage page;
	            for (int i = 0; i < n; ) {
	             // LOG.info("Adding page number " + (i + 1));
	              ++i;
	              page = writer.getImportedPage(reader, i);
	              writer.addPage(page);
	            }

	            PRAcroForm form = reader.getAcroForm();
	            if (form != null){
	              writer.copyAcroForm(reader);
	            }
	            f++;
	          }
	          if (!master.isEmpty()){
	            writer.setOutlines(master);
	          }
	          // step 5: we close the document
	          if(document != null){
	            document.close();
	          }else{
	          	//LOG.info("Could not close the document because it is null");
	          }
	         // LOG.info("Finishing PDFs copy");
	      }
	      catch(Exception e) {
	    	  LogDeMidasInterfaz.log("Unable to finish PDF Copy ==> "+ e, Level.INFO, null);
	      }
	  }
}