package mx.com.afirme.midas2.service.impl.emision.ejecutivoColoca;

import java.util.Comparator;

import com.afirme.eibs.services.EibsUserService.UserEibsVO;

public class EjecutivoColocaSort implements Comparator<UserEibsVO>{

	@Override
	public int compare(UserEibsVO object1, UserEibsVO object2) {
		return object1.getUserFullName().compareTo(object2.getUserFullName());
	}

}
