package mx.com.afirme.midas2.service.impl.textoadicional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;

import org.apache.commons.io.IOUtils;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotFacadeRemote;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.TexAdicionalCot;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.textoadicional.TextoAdicionalService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;

@Stateless
public class TextoAdicionalServiceImpl implements TextoAdicionalService {
protected EntidadDao entidadDao;
protected TexAdicionalCotFacadeRemote texAdicionalCotFacadeRemote;
protected CotizacionFacadeRemote cotizacionFacadeRemote;
protected UsuarioService usuarioService;
protected EntidadService entidadService;
protected SolicitudAutorizacionService autorizacionService;


	@Override
	public List<TexAdicionalCotDTO> listarFiltrado(CotizacionDTO cotizacion) {
		TexAdicionalCotDTO texAdicionalCotDTO = new TexAdicionalCotDTO();
		texAdicionalCotDTO.setCotizacion(cotizacion);
		
		List<TexAdicionalCotDTO> list = texAdicionalCotFacadeRemote.listarFiltrado(texAdicionalCotDTO);
		if(list != null && !list.isEmpty()){
			for(TexAdicionalCotDTO item : list){
				try{
					if(item.getDescripcionTexto() != null && IsRichText(item.getDescripcionTexto())){
						RTFEditorKit rtfParser = new RTFEditorKit();
						Document document = rtfParser.createDefaultDocument();
						rtfParser.read(IOUtils.toInputStream(item.getDescripcionTexto()), document, 0);
						String text = document.getText(0, document.getLength());
						item.setDescripcionTexto(text);
					}
				}catch(Exception e){			
				}				
			}
		}
		return list;
	}
	
    public static boolean IsRichText(String testString)
    {
        if ((testString != null) &&
            (testString.trim().startsWith("{\\rtf")))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

	@Override
	public void eliminaRow(TexAdicionalCotDTO texAdicionalCotDTO){
		entidadDao.remove(texAdicionalCotDTO);
	}

	@Override
	public void actualizarGrid(String accion,TexAdicionalCotDTO texAdicionalCotDTO,CotizacionDTO cotizacion){
		Usuario usuario = usuarioService.getUsuarioActual();
		
		texAdicionalCotDTO.setCotizacion(cotizacion);
    	texAdicionalCotDTO.setDescripcionTexto(texAdicionalCotDTO.getDescripcionTexto().toUpperCase());
		//texAdicionalCotDTO.setClaveAutorizacion(TexAdicionalCot.CLAVEAUTORIZACION_EN_PROCESO);
		texAdicionalCotDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
		texAdicionalCotDTO.setNombreUsuarioModificacion(usuario.getNombreUsuario());
		texAdicionalCotDTO.setCodigoUsuarioAutorizacion("");
		texAdicionalCotDTO.setFechaModificacion(new Date());		
		if(accion.equalsIgnoreCase("updated")){
        	texAdicionalCotFacadeRemote.update(texAdicionalCotDTO);
        }else{
        	texAdicionalCotFacadeRemote.save(texAdicionalCotDTO);
        }
	 }

@Override
public void guardarGrid(CotizacionDTO cotizacion, TexAdicionalCotDTO texAdicionalCotDTO){
	Usuario usuario = usuarioService.getUsuarioActual();
	
	texAdicionalCotDTO.setCotizacion(cotizacion);
	texAdicionalCotDTO.setDescripcionTexto(texAdicionalCotDTO.getDescripcionTexto().toUpperCase());
	texAdicionalCotDTO.setFechaCreacion(new Date());
	texAdicionalCotDTO.setClaveAutorizacion(TexAdicionalCot.CLAVEAUTORIZACION_EN_PROCESO);
	texAdicionalCotDTO.setFechaCreacion(new Date());
	texAdicionalCotDTO.setFechaModificacion(new Date());
	texAdicionalCotDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
	texAdicionalCotDTO.setNombreUsuarioCreacion(usuario.getNombreUsuario());
	texAdicionalCotDTO.setNumeroSecuencia(new BigDecimal(1));

	texAdicionalCotFacadeRemote.save(texAdicionalCotDTO);

}



	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}
	
	@EJB
	public void setTexAdicionalCotFacadeRemote(
			TexAdicionalCotFacadeRemote texAdicionalCotFacadeRemote) {
		this.texAdicionalCotFacadeRemote = texAdicionalCotFacadeRemote;
	}

	@EJB
	public void setCotizacionFacadeRemote(
			CotizacionFacadeRemote cotizacionFacadeRemote) {
		this.cotizacionFacadeRemote = cotizacionFacadeRemote;
	}

	@EJB(beanName = "UsuarioServiceDelegate") 
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}	
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setAutorizacionService(SolicitudAutorizacionService autorizacionService) {
		this.autorizacionService = autorizacionService;
	}
}
