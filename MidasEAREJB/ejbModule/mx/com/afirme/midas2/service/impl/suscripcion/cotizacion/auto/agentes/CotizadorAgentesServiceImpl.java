package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.agentes;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.direccion.DireccionFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.clientesapi.dto.ClienteUnicoDTO;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.agentes.VehiculoView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAutoDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.CaracteristicasVehiculoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.condicionespecial.CondicionEspecialCotizacionService;
import mx.com.afirme.midas2.service.fortimax.DocumentoCarpetaFortimaxService;
import mx.com.afirme.midas2.service.fortimax.FortimaxV2Service;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.NegocioTipoPolizaService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.agentes.CotizadorAgentesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.anexos.DocAnexosService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobranzainciso.CobranzaIncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.http.ResponseEntity;

import com.js.util.StringUtil;

@Stateless
public class CotizadorAgentesServiceImpl implements CotizadorAgentesService{
	
	private static final String CARPETA_DOC_COTIZADOR_AGENTES = "ARTICULO 140";
	private static final String NOMBRE_APLIZACION = "CLIENTES";
	private static final Integer FIELD_VALUESMAX = 6;
	private static final int PERSONA_ASEGURADO = 1;
	private static final int NOMBRE_ASEGURADO = 2;
	private static final int CLIENTE = 3;
	private static final int CONDUCTOR = 4;
	private static final int PERSONA_CONTRATANTE = 5;
	public static final int VALORMAXIMOUNIDADVIGENCIA = 365;
	public static final String NUEVOCOTIZADOR = "C_N_COTIZADOR";
	public static final String LIGA_AGENTE = "LIGA_AGENTE";
	public DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private Integer usuarioExterno = 2;
	
	public static final Logger LOG = Logger.getLogger(CotizadorAgentesServiceImpl.class);
	
	@Override
	public List<AgenteView> getAgentesPorDescripcion(String descripcionBusquedaAgente){
		List<AgenteView> agentesList = new ArrayList<AgenteView>(1);
		
		if(descripcionBusquedaAgente != null){
			agentesList = listadoService.listaAgentesPorDescripcionLightWeightAutorizados(descripcionBusquedaAgente.trim().toUpperCase());
		}else{
			agentesList = new ArrayList<AgenteView>(1);
		}
		return agentesList;
	}
	
	@Override
	public List<AgenteView> getAgentesPorPromotoria(Long idPromotoria){
		List<AgenteView> agentesList = new ArrayList<AgenteView>(1);
		if(idPromotoria != null){
			Agente filtroAgente = new Agente();
			
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(new Date());
			gcFechaFin.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFechaFin.set(GregorianCalendar.MINUTE, 0);
			gcFechaFin.set(GregorianCalendar.SECOND, 0);
			gcFechaFin.set(GregorianCalendar.MILLISECOND, 0);
			filtroAgente.setFechaVencimientoCedula(gcFechaFin.getTime());
			
			filtroAgente.getPromotoria().setIdPromotoria(idPromotoria);
			agentesList = agenteMidasService.findByFilterLightWeight(filtroAgente);
		}else{
			agentesList = new ArrayList<AgenteView>();
		} 
		return agentesList;
	}
	
	@Override
	public Map<String,String> getEstiloPorMarcaNegocioyDescripcion(BigDecimal idTcMarcaVehiculo, 
			Short modeloVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, 
			Long autoIncisoContinuityId, String descripcionBusquedaEstilos, BigDecimal idAgrupadorPasajeros){
		Map<String, String> estiloList = null;
		if(descripcionBusquedaEstilos != null){
			estiloList = listadoService.
				getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMonedaAgente(idTcMarcaVehiculo, modeloVehiculo, 
					idToNegSeccion, idMoneda, null, descripcionBusquedaEstilos, idAgrupadorPasajeros);
		}else{
			estiloList = new HashMap<String,String>(1);
		}
		
		return estiloList;
	}
	
	@Override
	public Map<String, Map<String, String>> getListasVehiculo(CotizacionDTO cotizacion, 
			String idEstiloVehiculo, Short idModeloVehiculo, BigDecimal idNegocioSeccion, String idToEstado){
		
		Map<String, Map<String, String>> list = new HashMap<String, Map<String, String>>(1);
		Long idNegocio = cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio();
		Long idProducto = cotizacion.getNegocioTipoPoliza().getNegocioProducto().getIdToNegProducto();
		BigDecimal idTipoPoliza = cotizacion.getNegocioTipoPoliza().getIdToNegTipoPoliza();
		
		if(idNegocio != null && idProducto != null && idTipoPoliza != null){
			 Map<String, String> mapNegocioSeccion = new HashMap<String, String>(1);
			 List<NegocioSeccion> listarNegocios = negocioSeccionService.getSeccionListByIdNegocioProductoTipoPoliza(
				BigDecimal.valueOf(idProducto), idNegocio, idTipoPoliza);
			 for(NegocioSeccion item : listarNegocios){
				 mapNegocioSeccion.put(item.getIdToNegSeccion().toString(), item.getSeccionDTO().getDescripcion().toString());
			 }
			 list.put("listarNegocios", mapNegocioSeccion);
			 list.put("negocioProductoList", listadoService.getMapNegProductoPorNegocioString(idNegocio));
			 list.put("negocioTipoPolizaList", listadoService.getMapNegTipoPolizaPorNegProductoFlotillaOIndividualString(idProducto,0));
			 list.put("estadoMap", listadoService.getMapEstadosMX());
			 if(idToEstado!=null)
				 list.put("municipioMap", listadoService.getMapMunicipiosPorEstado(idToEstado));
		}
		if(idNegocioSeccion != null)
			list.put("marcaMap", listadoService.getMapMarcaVehiculoPorNegocioSeccionString(idNegocioSeccion));
		if(idEstiloVehiculo != null )
			list.put("modeloMap", listadoService.getMapModeloVehiculoPorMonedaEstiloNegocioSeccionString(cotizacion.getIdMoneda(), idEstiloVehiculo, idNegocioSeccion));
		if( idNegocioSeccion != null && idEstiloVehiculo!= null)
			list.put("tipoUsoMap", listadoService.getMapTipoUsoVehiculoPorEstiloVehiculoString(idEstiloVehiculo, idNegocioSeccion));
		
		return list;
	}
	
	public List<EsquemaPagoCotizacionDTO> verEsquemaPagoAgente(CotizacionDTO cotizacion){
		List<EsquemaPagoCotizacionDTO> esquemaPagoCotizacionList = new ArrayList<EsquemaPagoCotizacionDTO>(1);
		BigDecimal derecho = cotizacion.getValorDerechosUsuario()!=null?new BigDecimal(cotizacion.getValorDerechosUsuario()):BigDecimal.ZERO;
		if(cotizacion.getIdToCotizacion()!=null){	
			BigDecimal porcentajeIVA = cotizacion.getPorcentajeIva()!=null?new BigDecimal(cotizacion.getPorcentajeIva()):BigDecimal.ZERO;	  
			esquemaPagoCotizacionList = cotizacionService.setEsquemaPagoCotizacion(cotizacion,cotizacion.getFechaInicioVigencia(),
					cotizacion.getFechaInicioVigencia(),cotizacion.getFechaFinVigencia(),cotizacion.getValorTotalPrimas(), 
				  derecho,  porcentajeIVA,  cotizacion.getValorPrimaTotal());
		}
		
		return esquemaPagoCotizacionList;
	}
	/**
	 * @param cotizacion
	 * @return ResumenCostosDTO
	 */
	public ResumenCostosDTO mostrarResumenTotalesCotizacionAgente(CotizacionDTO cotizacion){
		return calculoService.obtenerResumenCotizacion(cotizacion, false);
	}
	/**
	 * JFGG
	 * @param cotizacion
	 * @param statusExpetion
	 * @return ResumenCostosDTO
	 */
	public ResumenCostosDTO mostrarResumenTotalesCotizacionAgente(CotizacionDTO cotizacion, boolean statusExpetion){
		return calculoService.obtenerResumenCotizacion(cotizacion, false, statusExpetion);
	}
	
	@Override
	public List<CaracteristicasVehiculoDTO> mostrarOtrasCaracteristicas(String estiloSeleccionado, String modificadores){
		String[] estiloId = null;
		String modificadoresDescripcion = "";
		if(!StringUtil.isEmpty(estiloSeleccionado)){
			estiloId = estiloSeleccionado.split("_");
			EstiloVehiculoId id = new EstiloVehiculoId(estiloId[0], estiloId[1], BigDecimal.valueOf(Long.valueOf(estiloId[2]))); 
			EstiloVehiculoDTO estiloModificarCaracteristicas = entidadService.findById(EstiloVehiculoDTO.class, id);
			if(modificadores == null || modificadores.isEmpty()){
				modificadoresDescripcion = incisoService.getModificadoresDescripcionDefautlt(estiloModificarCaracteristicas);
			}
		}
		return incisoService.listarCaracteristicas(modificadoresDescripcion);
	}
	
	@Override
	public IncisoAutoCot definirOtrasCaracteristicas(
			List<CaracteristicasVehiculoDTO> caracteristicasVehiculoList, String descripcionBase){
		return incisoService.definirOtrasCaract(caracteristicasVehiculoList, descripcionBase);
	}
	
	@Override
	public List<CoberturaCotizacionDTO> mostrarCorberturaCotizacion(
			List<CoberturaCotizacionDTO> coberturaCotizacionBase, IncisoCotizacionDTO incisoCotizacion){
		CotizacionDTO cotizacion = incisoCotizacion.getCotizacionDTO();		
		List<CoberturaCotizacionDTO>coberturaCotizacionList = coberturaCotizacionBase;
		
		if(coberturaCotizacionList==null){
			if(CotizacionDTO.TIPO_COTIZACION_SERVICIO_PUBLICO.equals(cotizacion.getTipoCotizacion())){
				coberturaCotizacionList = verDetalleIncisoAgente(incisoCotizacion);
			} else {
				NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class,
						incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId());
				Long numeroSecuencia = new Long("1");
				numeroSecuencia = numeroSecuencia + incisoService.maxSecuencia(cotizacion.getIdToCotizacion());
				try{
					NegocioTipoPoliza negocioTipoPoliza = negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza();
					BigDecimal codigoAgente = null;
					if(cotizacion != null
							&& cotizacion.getSolicitudDTO() != null){
						codigoAgente = cotizacion.getSolicitudDTO().getCodigoAgente();
					}
					coberturaCotizacionList = coberturaService.getCoberturas(negocioTipoPoliza.getNegocioProducto().getNegocio().getIdToNegocio(), 
						negocioTipoPoliza.getNegocioProducto().getProductoDTO().getIdToProducto(), 
						negocioTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza(), 
						negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), 
						negocioPaqueteSeccion.getPaquete().getId(), incisoCotizacion.getIncisoAutoCot().getEstadoId(), incisoCotizacion.getIncisoAutoCot().getMunicipioId(), 
						cotizacion.getIdToCotizacion(), cotizacion.getIdMoneda().shortValue(), numeroSecuencia,
						incisoCotizacion.getIncisoAutoCot().getEstiloId(), incisoCotizacion.getIncisoAutoCot().getModeloVehiculo().longValue(), 
						new BigDecimal(incisoCotizacion.getIncisoAutoCot().getTipoUsoId()), null, null, true, incisoCotizacion.getIncisoAutoCot().getNumeroSerie(), incisoCotizacion.getIncisoAutoCot().getVinValido(), codigoAgente);					
				}catch(Exception e){
					LOG.error(e.getMessage(), e);
					coberturaCotizacionList = new ArrayList<CoberturaCotizacionDTO>(1);
				}
			}
		}
			
		return 	coberturaCotizacionList;
	}
	
	@Override
	public IncisoCotizacionDTO saveDatosComplementariosAsegurado(IncisoCotizacionDTO incisoCotizacion, Integer saveAsegurado){
		
		ClienteUnicoDTO cliente = null;
		DireccionDTO direccion = null;
		BigDecimal idCliente = null;
		String nombreAsegurado = null;
		Long personaContratante = null;
		IncisoCotizacionDTO incisoCotizacionDto = new IncisoCotizacionDTO();
		if(saveAsegurado!=null){
			int save = saveAsegurado.intValue();
			if (incisoCotizacion.getCotizacionDTO().getIdToPersonaContratante() != null){
					personaContratante = incisoCotizacion.getCotizacionDTO().getIdToPersonaContratante().longValue();
					if(save==PERSONA_CONTRATANTE)
						idCliente = new BigDecimal(personaContratante);
				
			}else if (incisoCotizacion.getIncisoAutoCot().getPersonaAseguradoId() != null 
					&& (save==CLIENTE || save==CONDUCTOR || save==PERSONA_ASEGURADO)){
				idCliente = new BigDecimal(incisoCotizacion.getIncisoAutoCot().getPersonaAseguradoId());
			}
			
			nombreAsegurado= incisoCotizacion.getIncisoAutoCot().getNombreAsegurado().toUpperCase();
			if(idCliente!=null){
					cliente = getClienteGenerico(idCliente);
			}
			
			incisoCotizacionDto =  entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacion.getId());
			
			switch (save) {
			case PERSONA_ASEGURADO:
				if (cliente != null) {
					incisoCotizacionDto.getIncisoAutoCot().setPersonaAseguradoId(
							cliente.getIdCliente().longValue());
					incisoCotizacionDto.getIncisoAutoCot().setNombreAsegurado(
							cliente.getNombreCompleto());
					
					
					
				}
	        	break;
			case NOMBRE_ASEGURADO:
				incisoCotizacionDto.getIncisoAutoCot().setPersonaAseguradoId(null);
				incisoCotizacionDto.getIncisoAutoCot().setNombreAsegurado(nombreAsegurado);
	        	break;
			case CLIENTE:
				if (incisoCotizacionDto.getDireccionDTO() != null && incisoCotizacionDto.getDireccionDTO().getIdToDireccion() != null) {
					direccion = direccionFacadeRemote.findById(incisoCotizacionDto.getDireccionDTO().getIdToDireccion());
				}
				
				if (cliente != null) {
					incisoCotizacionDto.getIncisoAutoCot().setPersonaAseguradoId(
							cliente.getIdCliente().longValue());
					incisoCotizacionDto.getIncisoAutoCot().setNombreAsegurado(
							cliente.obtenerNombreCliente());
				}
				incisoCotizacionDto.setDireccionDTO(direccion);
	        	break;
			case CONDUCTOR:
				if (cliente != null) {
					incisoCotizacionDto.getIncisoAutoCot().setNombreConductor(
							cliente.getNombre());
					incisoCotizacionDto.getIncisoAutoCot().setMaternoConductor(
							cliente.getApellidoMaterno());
					incisoCotizacionDto.getIncisoAutoCot().setPaternoConductor(
							cliente.getApellidoPaterno());
					incisoCotizacionDto.getIncisoAutoCot().setFechaNacConductor(
							Utilerias.obtenerFechaDeCadena(dateFormat.format(cliente.getFechaNacimiento())));
				}
	        	break;
			case PERSONA_CONTRATANTE:
				CotizacionDTO cotizacion = entidadService.findById(
						CotizacionDTO.class, incisoCotizacionDto.getId().getIdToCotizacion());
				if (cliente != null) {
					cotizacion.setIdToPersonaContratante(
							cliente.getIdCliente());
					cotizacion.getSolicitudDTO().setClaveTipoPersona(cliente.getClaveTipoPersona());
					cotizacion.setNombreContratante(cliente.getNombreCompleto());
					if(cliente.getIdDomicilioConsulta()!=null)
						cotizacion.setIdDomicilioContratante(
								BigDecimal.valueOf(cliente.getIdDomicilioConsulta()));	
					entidadService.save(cotizacion);
				}
				incisoCotizacionDto.setCotizacionDTO(cotizacion);
	        	break;
			}

			incisoService.update(incisoCotizacionDto);
			cobranzaIncisoService.validarCambioAseguradoCobranzaInciso(incisoCotizacionDto.getId().getIdToCotizacion(),
					incisoCotizacionDto.getId().getNumeroInciso());
		}
		
		return incisoCotizacionDto;
	}

	@Override
	public Long cambioNegocio(BigDecimal idNegocioSeccion){
		Long idNegocioPaqueteBase = null;
		String parameter = getValorParameter(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_COTIZACION_PAQUETE_BASE);
		
		Map<Long, String> listNegPaqSeccio =  listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(idNegocioSeccion);
		if(!listNegPaqSeccio.isEmpty()){
			idNegocioPaqueteBase = listNegPaqSeccio.keySet().iterator().next();
		}else{
			idNegocioPaqueteBase = Long.valueOf(parameter);
		}
		return idNegocioPaqueteBase;
	}
	
	@Override
	public IncisoCotizacionDTO saveDatosVehiculo(VehiculoView vehiculo, IncisoCotizacionDTO inciso,
			boolean isSave){
		IncisoCotizacionDTO incisoCotizacion = null;
		if(isSave)
			incisoCotizacion = entidadService.findById(IncisoCotizacionDTO.class, inciso.getId());
		else
			incisoCotizacion = inciso;
		
		String descripcionFinal = inciso.getIncisoAutoCot().getDescripcionFinal();
		String idEstado = inciso.getIncisoAutoCot().getEstadoId();
		String idMunicipio = inciso.getIncisoAutoCot().getMunicipioId();
		incisoCotizacion.getIncisoAutoCot().setNegocioSeccionId(vehiculo.getIdNegocioSeccion().longValue());
		incisoCotizacion.getIncisoAutoCot().setTipoUsoId(vehiculo.getIdTipoUso().longValue());
		incisoCotizacion.getIncisoAutoCot().setModeloVehiculo(vehiculo.getIdModeloVehiculo().shortValue());
		incisoCotizacion.getIncisoAutoCot().setEstiloId(vehiculo.getIdEstiloVehiculo());
		incisoCotizacion.getIncisoAutoCot().setMarcaId(vehiculo.getIdMarcaVehiculo());
		incisoCotizacion.getIncisoAutoCot().setDescripcionFinal(descripcionFinal);
		incisoCotizacion.getIncisoAutoCot().setEstadoId(idEstado);
		incisoCotizacion.getIncisoAutoCot().setMunicipioId(idMunicipio);
		incisoCotizacion.getIncisoAutoCot().setNumeroSerie(vehiculo.getNumeroSerie());
		incisoCotizacion.getIncisoAutoCot().setVinValido(vehiculo.getVinValido());
		incisoCotizacion.getIncisoAutoCot().setCoincideEstilo(vehiculo.getCoincideEstilo());
		incisoCotizacion.getIncisoAutoCot().setObservacionesSesa(vehiculo.getObservacionesSesa());
		incisoCotizacion.getIncisoAutoCot().setObservacionesinciso(inciso.getIncisoAutoCot().getObservacionesinciso());
		incisoCotizacion.getIncisoAutoCot().setCodigoPostal(inciso.getIncisoAutoCot().getCodigoPostal());
		if(isSave)
			incisoCotizacion = incisoService.update(incisoCotizacion);
		
		return incisoCotizacion;
	}
	
	@Override
	public List<CoberturaCotizacionDTO> verDetalleIncisoAgente(IncisoCotizacionDTO incisoCotizacionDTO){
		List<CoberturaCotizacionDTO> coberturaCotizacionList = new LinkedList<CoberturaCotizacionDTO>();
		List<CoberturaCotizacionDTO> coberturasList = coberturaService.getCoberturas(incisoCotizacionDTO.getIncisoAutoCot().getNegocioPaqueteId(),
	    		incisoCotizacionDTO.getId());
	    Collections.sort(coberturaCotizacionList = coberturasList, new Comparator<CoberturaCotizacionDTO>() {
				public int compare(CoberturaCotizacionDTO n1,
						CoberturaCotizacionDTO n2) {
					return n1
							.getCoberturaSeccionDTO()
							.getCoberturaDTO()
							.getNumeroSecuencia()
							.compareTo(
									n2.getCoberturaSeccionDTO().getCoberturaDTO()
											.getNumeroSecuencia());
				}
			});
	    return coberturaCotizacionList;
	}
	
	@Override
	public List<CatalogoDocumentoFortimax> getListaDocumentosFortimax(Long idToPersonaContratante){
		List<CatalogoDocumentoFortimax> list = new LinkedList<CatalogoDocumentoFortimax>();
		
		try {
			ClienteGenericoDTO cliente = getClienteGenerico(new BigDecimal(idToPersonaContratante));		
			list = documentoCarpetaService.obtenerDocumentosPorAplicacionTipoPersona(NOMBRE_APLIZACION,
					cliente.getClaveTipoPersona().longValue());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			list = new LinkedList<CatalogoDocumentoFortimax>();
		}
		return list;
	}

	@Override
	public Map<String, String> getURLFortimax(Long idToPersonaContratante){
		Map<String, String> urls = new HashMap<String, String>(1);
		String user = sistemaContext.getFortimaxV2User();
		String password = sistemaContext.getFortimaxV2Password();
		try {
			String[] documentos =  fortimaxV2Service.getDocumentFortimax(idToPersonaContratante, NOMBRE_APLIZACION);
			for( String doc : documentos)	{
				if(!StringUtil.isEmpty(doc)){					
					String nodo=fortimaxV2Service.getNodo(idToPersonaContratante, NOMBRE_APLIZACION, doc, "D", true);					
					if(!StringUtil.isEmpty(nodo)){
						String url = fortimaxV2Service.generateLinkToDocument(idToPersonaContratante, NOMBRE_APLIZACION,
								doc, user, password, nodo);
						if(!StringUtil.isEmpty(url)){
							urls.put(doc, url);
						}
					}
					
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return urls;
	}
	
	@Override
	public IncisoCotizacionDTO saveDatosComplementarios(List<File> files, List<String> fileNames,
			List<String> contentTypes, IncisoCotizacionDTO incisoCotizacionDTO){
		
		IncisoCotizacionDTO incisoCotizacion =  setDatosConductor(incisoCotizacionDTO);
		
		//guardamos los documentos en Fortimax
		try {
			String[] expediente = crearExpedienteFortimax(incisoCotizacion.getCotizacionDTO().getIdToPersonaContratante());
			for(int i = 0; i<= files.size()-1; i++){
				guardaDocumentos(files.get(i), fileNames.get(i), expediente);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		return incisoCotizacion;
	}
	
	/**
	 * Validar si la cotizacion esta completa y lista para emitir
	 */
	@Override
	public MensajeDTO validarListaParaEmitir(BigDecimal idToCotizacion) {
		MensajeDTO mensaje = new MensajeDTO();
		if(idToCotizacion!=null){
			CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);		
			Short estatusConfiguracion = ConfiguracionDatoInciso.Estatus.TODOS.getEstatus();
			if(cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)){
				estatusConfiguracion = ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus();
			}		
			List<RamoTipoPolizaDTO> ramos = cotizacion.getTipoPolizaDTO().getRamoTipoPolizaList(); 	
			List<SubRamoDTO> subRamos = getSubRamos(idToCotizacion);
			List<ConfiguracionDatoInciso> configuracion = 
				configuracionDatoIncisoDao.getConfiguracionDatosIncisoCotAuto(estatusConfiguracion);
			List<DatoIncisoCotAuto> riesgoCotizacion = datoIncisoCotAutoDao.getDatosIncisoCotAutoPorCotizacion(idToCotizacion);
			mensaje = validarListaParaEmitir( cotizacion,  ramos, subRamos, configuracion, riesgoCotizacion);
		}
		return mensaje;
	}
	
	private MensajeDTO validarListaParaEmitir(CotizacionDTO cotizacion, List<RamoTipoPolizaDTO> ramos, List<SubRamoDTO> subRamos,
			List<ConfiguracionDatoInciso> configuracion, List<DatoIncisoCotAuto> riesgoCotizacion) {
		MensajeDTO mensajeDTO = new MensajeDTO();
		boolean isRdyForEmission = true;
		boolean isValid = true;
		String msj 	= "";
		StringBuilder msjs = new StringBuilder("");
		int complementados = 0;
		
		for(IncisoCotizacionDTO incisoDTO : cotizacion.getIncisoCotizacionDTOs()){
			isValid = true;
			IncisoAutoCot incisoAutoCot = incisoDTO.getIncisoAutoCot();
			List<String> datosAuto = setDatosAuto(incisoAutoCot);
			if(UtileriasWeb.validaDatosEmpty(datosAuto)){
				isValid = false;		
				msjs.append("Debe capturar los datos complementarios del veh\u00EDculo");
			}else{
				if(incisoService.infoConductorEsRequerido(incisoAutoCot.getIncisoCotizacionDTO()) 
						&& validaDatosConductor(incisoAutoCot)){
						isValid = false;
						msjs.append("Debe capturar los datos del conductor");
				}
				if(isValid){	
					isValid = cotizacionService.validaConfiguracionDatosRiesgo(configuracion, riesgoCotizacion, incisoDTO.getSeccionCotizacion(), ramos, subRamos);
					if(!isValid)
						msjs.append("Debe capturar todos los datos adicionales del paquete");
				}
			}
			//Valida Observaciones
			try{
				
				Usuario usuario = usuarioService.getUsuarioActual();
				if(usuario!=null && usuario.getTipoUsuario()!=null && usuario.getTipoUsuario()>0) {
					usuarioExterno = usuario.getTipoUsuario();
				}
				
				if(isValid && usuarioExterno == 1 && incisoService.observacionesEsRequerido(incisoDTO) && 
							(incisoAutoCot.getObservacionesinciso() == null || 
									incisoAutoCot.getObservacionesinciso().isEmpty())){
						isValid = false;
						msjs.append("Debe capturar las Observaciones");
				}
			}catch(Exception e){}
						
			if(isValid){
				complementados++;
			}else{
				isRdyForEmission = false;
			}
		}
		msj = msjs.toString();
		
		//validar contratante y validar forma pago
		if(isRdyForEmission && 
				cotizacion.getIdToPersonaContratante() == null){
			isRdyForEmission = false;
			msj = "Debe de seleccionar un contratante ";
		}
		
		if(isRdyForEmission){
			msj = "Se tienen complementados " + complementados + " incisos de "+ cotizacion.getIncisoCotizacionDTOs().size();	
		}
		mensajeDTO.setMensaje(msj);
		mensajeDTO.setVisible(isRdyForEmission);
		return mensajeDTO;		
	}

	private List<String> setDatosAuto(IncisoAutoCot incisoAutoCot) {
		List<String> datosAuto = new ArrayList<String>(1);
		datosAuto.add(incisoAutoCot.getNumeroSerie());
		datosAuto.add(incisoAutoCot.getNumeroMotor());
		datosAuto.add(incisoAutoCot.getPlaca());
		datosAuto.add(incisoAutoCot.getNombreAsegurado());
		return datosAuto;
	}

	@Override
	public String getValorParameter(BigDecimal idToGrupoParamGen, BigDecimal codigoParma){
		ParametroGeneralId parametroGeneralId = new ParametroGeneralId();
		parametroGeneralId.setIdToGrupoParametroGeneral(idToGrupoParamGen);
		parametroGeneralId.setCodigoParametroGeneral(codigoParma);
		ParametroGeneralDTO parameter = entidadService.findById(ParametroGeneralDTO.class, parametroGeneralId);
		if(parameter!=null){
			return parameter.getValor();
		}
		return null;
	}
	
	@Override
	public ClienteUnicoDTO getClienteGenerico(BigDecimal idPersona) {
		ClienteUnicoDTO cliente = null;
			
		
		ResponseEntity<Long> idClienteUnico= clienteRest.clienteUnificado(null,idPersona.longValue());
		
			cliente=clienteRest.findClienteUnicoById(idClienteUnico.getBody());
			
		
		return cliente;		
	}

	@Override
	public Agente getAgente(Long codigoAgente){
		Agente filtroAgente = new Agente();
		filtroAgente.setId(codigoAgente.longValue());		
		return getAgente(filtroAgente);
	}
	
	@Override
	public String getBienAsegurado(IncisoCotizacionDTO incisoCotizacion){
		String bienasegurado= "";
		if(incisoCotizacion.getIncisoAutoCot().getMarcaId()!=null){
			MarcaVehiculoDTO marcaVehiculoDTO= entidadService.findById(MarcaVehiculoDTO.class, incisoCotizacion.getIncisoAutoCot().getMarcaId());
			bienasegurado = bienasegurado.concat(marcaVehiculoDTO.getDescription());
				
		}
		if(incisoCotizacion.getIncisoAutoCot().getModeloVehiculo()!=null){
			bienasegurado = bienasegurado.concat(" ").concat(incisoCotizacion.getIncisoAutoCot().getModeloVehiculo().toString());				
		}
		if(incisoCotizacion.getIncisoAutoCot().getDescripcionFinal()!=null){
			bienasegurado = bienasegurado.concat(" ").concat(incisoCotizacion.getIncisoAutoCot().getDescripcionFinal());
		}
		return bienasegurado;
	}
	
	@Override
	public NegocioTipoPoliza getNegocioTipoPoliza(CotizacionDTO cotizacion){
		Long idToNegProducto = listadoService.getNegocioProducto(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio(), 
				cotizacion.getSolicitudDTO().getProductoDTO().getIdToProducto());
	  return negocioTipoPolizaService.getPorIdNegocioProductoIdToTipoPoliza(idToNegProducto, cotizacion.getTipoPolizaDTO().getIdToTipoPoliza());
	}
	
	@Override
	public List<Negocio> getNegociosPorAgente(int codigoAgente, String claveNegocio){
		return negocioService.listarNegociosPorAgenteUnionNegociosLibres(codigoAgente, claveNegocio);
	}
	
	@Override
	public NegocioDerechoPoliza getNegocioDerechoPoliza(Long idNegocio){
		NegocioDerechoPoliza negocioDerechoPoliza = new NegocioDerechoPoliza();
		
		if(idNegocio!=null){
			Long idToNegDerechoPoliza = listadoService.getDerechoPolizaDefault(BigDecimal.valueOf(idNegocio));

			negocioDerechoPoliza.setIdToNegDerechoPoliza(idToNegDerechoPoliza);
		}
		return negocioDerechoPoliza;
	}

	@Override
	public NegocioDerechoPoliza getNegocioDerechoPoliza(Long idNegocio, BigDecimal idToCotizacion, BigDecimal numeroInciso){
		NegocioDerechoPoliza negocioDerechoPoliza = new NegocioDerechoPoliza();
		
		if(idToCotizacion != null
				&& numeroInciso != null){
			Long idToNegDerechoPoliza = listadoService.getDerechoPolizaDefault(idToCotizacion, numeroInciso);
			
			negocioDerechoPoliza.setIdToNegDerechoPoliza(idToNegDerechoPoliza);
		}else if(idNegocio!=null){
			Long idToNegDerechoPoliza = listadoService.getDerechoPolizaDefault(BigDecimal.valueOf(idNegocio));

			negocioDerechoPoliza.setIdToNegDerechoPoliza(idToNegDerechoPoliza);
		}
		return negocioDerechoPoliza;
	}
	
	@Override
	public Agente getAgente(Agente filtroAgente) {
		Agente agente = new Agente();
		List<AgenteView> agenteViewResult =  agenteMidasService.findByFilterLightWeight(filtroAgente);
		if (agenteViewResult != null && !agenteViewResult.isEmpty()) {
			agente.setIdAgente(agenteViewResult.get(0).getIdAgente());
			agente.setPersona(new Persona());
			agente.getPersona().setNombreCompleto(agenteViewResult.get(0).getNombreCompleto());
			agente.setId(agenteViewResult.get(0).getId());
		}
		return agente;
	}

	private boolean validaDatosConductor(IncisoAutoCot incisoAutoCot) {
		List<String> datos = new ArrayList<String>(1);
		datos.add(incisoAutoCot.getNumeroLicencia());
		datos.add(incisoAutoCot.getNombreConductor());
		datos.add(incisoAutoCot.getPaternoConductor());
		datos.add(incisoAutoCot.getOcupacionConductor());
		if(UtileriasWeb.validaDatosEmpty(datos)){
			return true;
		}
			
		if(incisoAutoCot.getFechaNacConductor() == null){
			return true;
		}
		
		return false;
	}

	private void guardaDocumentos(File file,
			String fileName, String[] expediente){
		TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
		byte[] fileBytes;
		try {
			fileBytes = FileUtils.readFileToByteArray(file);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
		transporteImpresionDTO.setByteArray(fileBytes);
		
		String documentoNombre = fileName + "." + FilenameUtils.getExtension(fileName);
		
		try {
			fortimaxV2Service.uploadFile(documentoNombre, transporteImpresionDTO, NOMBRE_APLIZACION, expediente, 
					CARPETA_DOC_COTIZADOR_AGENTES);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}		
	}

    private String[] crearExpedienteFortimax(BigDecimal id) throws Exception {
    	String[] strReturns;
    	
		ClienteGenericoDTO cliente = getClienteGenerico(id);				
				
		String[] fieldValues = new String[FIELD_VALUESMAX];
		
		fieldValues[0]=id.toString();
		
		if(cliente.getClaveTipoPersona()==1){
			fieldValues[1]=cliente.getNombre();		
			fieldValues[2]=cliente.getApellidoPaterno();
			fieldValues[3]=cliente.getApellidoMaterno();
		}
		else{
			fieldValues[1]=cliente.getRazonSocial();		
			fieldValues[2]=cliente.getRazonSocial();
			fieldValues[3]=cliente.getRazonSocial();
		}
		fieldValues[4]=cliente.getClaveTipoPersona().toString();
		fieldValues[5]="1";	
		strReturns = fortimaxV2Service.generateExpedient(NOMBRE_APLIZACION, fieldValues);
		
		if (strReturns[0].equals("false")) {
			LOG.info("No se pudo crear el expediente en Fortimax para la cotizacion.");
			throw new RuntimeException("No se pudo crear el expediente en Fortimax para la cotizacion.");
		}
		
		return fieldValues;
	}

	private IncisoCotizacionDTO setDatosConductor(IncisoCotizacionDTO incisoCotizacionDTO) {
		IncisoCotizacionDTO incisoCotizacion =  entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionDTO.getId());
		IncisoAutoCot incisoAuto = incisoCotizacionDTO.getIncisoAutoCot();
		incisoCotizacion.getIncisoAutoCot().setFechaNacConductor(incisoAuto.getFechaNacConductor());
		incisoCotizacion.getIncisoAutoCot().setNumeroLicencia(incisoAuto.getNumeroLicencia());
		incisoCotizacion.getIncisoAutoCot().setOcupacionConductor(incisoAuto.getOcupacionConductor());
		incisoCotizacion.getIncisoAutoCot().setObservacionesinciso(incisoAuto.getObservacionesinciso());
		incisoCotizacion.getIncisoAutoCot().setNumeroMotor(incisoAuto.getNumeroMotor());
		incisoCotizacion.getIncisoAutoCot().setNumeroSerie(incisoAuto.getNumeroSerie());
		incisoCotizacion.getIncisoAutoCot().setPlaca(incisoAuto.getPlaca());
		incisoCotizacion.getIncisoAutoCot().setRepuve(incisoAuto.getRepuve());
		incisoCotizacion.getIncisoAutoCot().setNombreConductor(incisoAuto.getNombreConductor());
		incisoCotizacion.getIncisoAutoCot().setMaternoConductor(incisoAuto.getMaternoConductor());
		incisoCotizacion.getIncisoAutoCot().setPaternoConductor(incisoAuto.getPaternoConductor());
		//Se guarda el inciso de la cotizacion
		incisoCotizacion = incisoService.update(incisoCotizacion);
		return incisoCotizacion;
	}

	@SuppressWarnings("unchecked")
	private List<SubRamoDTO> getSubRamos(BigDecimal idToCotizacion) {
		final String queryString = "select model from SubRamoDTO as model where model.idTcSubRamo in "
			+ "(select distinct toc.idTcSubramo from CoberturaCotizacionDTO as toc where toc.id.idToCotizacion = :idToCotizacion )";
		Map<String, Object> map = new HashMap<String, Object>(1);
		map.put("idToCotizacion", idToCotizacion);
		return entidadService.executeQueryMultipleResult(queryString, map);
	}
	
	@Override
	public Map<Object, String> obtenerDiasSalario(){
		Map<Object, String> listDiasSalario = new HashMap<Object, String>(1);
		
		List<CatalogoValorFijoDTO> catalogoValorFijoDTOs = entidadService.findByProperty(
				CatalogoValorFijoDTO.class, "id.idGrupoValores", CatalogoValorFijoDTO.IDGRUPO_DIAS_SALARIO);
		if (!catalogoValorFijoDTOs.isEmpty() && catalogoValorFijoDTOs != null) {
			for (CatalogoValorFijoDTO estatus : catalogoValorFijoDTOs) {
				listDiasSalario.put(estatus.getId().getIdDato(),
						estatus.getDescripcion());
			}
		}
		return listDiasSalario;
	}
	
	@Override
	public Short obtenerNumeroAsientos(IncisoCotizacionDTO incisoCotizacion){
		Short numeroAsientos = null;
		if(incisoCotizacion != null && incisoCotizacion.getIncisoAutoCot() != null &&
				incisoCotizacion.getIncisoAutoCot().getEstadoId() != null){
			EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
			estiloVehiculoId.valueOf(incisoCotizacion.getIncisoAutoCot().getEstiloId());
			EstiloVehiculoDTO estiloVehiculoDTO  =  entidadService.findById(EstiloVehiculoDTO.class, estiloVehiculoId);
			numeroAsientos = estiloVehiculoDTO.getNumeroAsientos();
		}
		return numeroAsientos;
	}	

	@Override
	public String setNombreAgenteCompleto(CotizacionDTO cotizacion) {
		String nombreCliente = null;
		if(cotizacion.getIdToPersonaContratante()!=null){
			ClienteGenericoDTO cliente = getClienteGenerico(cotizacion.getIdToPersonaContratante());
			 if(cliente!=null){
				 nombreCliente = cliente.getNombreCompleto();					 
			 }		
		}
		return nombreCliente;
	}
	
	@Override
	public EstiloVehiculoDTO getEstiloVehiculo(String estiloId){
		EstiloVehiculoDTO style = null;
		if(estiloId != null && !estiloId.trim().equals("")){
			EstiloVehiculoId id = new EstiloVehiculoId();
			id.valueOf(estiloId);		
			style = entidadService.findById(EstiloVehiculoDTO.class, id);
		}	
		return style;
	}

	@Override
	public Map<String, String> validaCotizacion(CotizacionDTO cotizacion, BigDecimal idTipoPoliza) {
		Map<String, String> mapError = new HashMap<String, String>(1);
		DateTime inicioVigencia = new DateTime(cotizacion.getFechaInicioVigencia());
		DateTime finVigencia = new DateTime(cotizacion.getFechaFinVigencia());
		long days = Days.daysBetween(inicioVigencia,finVigencia).getDays();
		if(days<0){
			mapError.put("mensaje", "La fecha de inicio de vigencia no puede ser mayor o igual a la de fin de vigencia");
			return mapError;
		}
		if(idTipoPoliza!=null){
			NegocioTipoPoliza negocioTipoPoliza  = entidadService.findById(NegocioTipoPoliza.class, idTipoPoliza);
			if(negocioTipoPoliza!=null){
				cotizacion.setTipoPolizaDTO(negocioTipoPoliza.getTipoPolizaDTO());
				Long valorMaximoUnidadVigencia = cotizacion.getTipoPolizaDTO().getProductoDTO().getValorMaximoUnidadVigencia().longValue();
				// Valida que los días de vigencia de la cotizacion no sean mayor a 365 dias
				if ( valorMaximoUnidadVigencia<= VALORMAXIMOUNIDADVIGENCIA && 
						days > VALORMAXIMOUNIDADVIGENCIA){
					mapError.put("mensaje", "Los dias de vigencia de la cotizacion no pueden ser mayores a un anio");
					return mapError;
				}
				// Valida que los días de vigencia de la cotizacion no sean mayores a los del producto
				if (days > valorMaximoUnidadVigencia) {
					mapError.put("mensaje", "Los dias de la cotizacion son mayores a los dias de vigencia del producto");
					return mapError;
				}
				// Valida que la fecha fin fija del negocio sea igual que la 
				// Fecha fin de la cotizacion y compara las fechas
				if(cotizacion.getSolicitudDTO()!= null && cotizacion.getSolicitudDTO().getNegocio()!=null){
					Negocio negocio = entidadService.findById(Negocio.class,cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
					cotizacion.getSolicitudDTO().setNegocio(negocio);
				}
				Date fechaFija = cotizacion.getSolicitudDTO().getNegocio().getFechaFinVigenciaFija();
				if ( fechaFija!= null && 
						!DateUtils.isSameDay(fechaFija, cotizacion.getFechaFinVigencia())) {
					mapError.put("mensaje","La fecha fin de vigencia debe de ser igual a " + dateFormat.format(fechaFija));
					mapError.put("fechaFijaFinVigencia", dateFormat.format(fechaFija));
					return mapError;
				}
			}
		}
		return mapError;	
	}

	@Override
	public CotizacionDTO crearCotizacion(CotizacionDTO cotizacionDto, BigDecimal numeroInciso){
		CotizacionDTO cotizacion = cotizacionDto;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		SolicitudDTO sol = cotizacion.getSolicitudDTO();
		
		Long idNegocio = cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio();
		NegocioDerechoPoliza negocioDerechoPoliza = this.getNegocioDerechoPoliza(idNegocio, cotizacionDTO.getIdToCotizacion(), numeroInciso);
		cotizacion.setNegocioDerechoPoliza(negocioDerechoPoliza);		
		Negocio negocio = entidadService.findById(Negocio.class,cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
		cotizacion.getSolicitudDTO().setNegocio(negocio);
		Integer idFormaPago = listadoService.getPrimeraFormasdePagoByNegTipoPoliza(
					cotizacion.getNegocioTipoPoliza().getNegocioProducto().getIdToNegProducto());
		Double porcentajeRecargoPagoFraccionado=listadoService.getPctePagoFraccionado(idFormaPago,cotizacion.getIdMoneda().shortValue());
		cotizacion.setIdFormaPago(BigDecimal.valueOf(idFormaPago));
		cotizacion.setPorcentajebonifcomision(Double.valueOf("0.0"));
		cotizacion.setPorcentajePagoFraccionado(porcentajeRecargoPagoFraccionado);
		cotizacion.setPorcentajeIva(Double.valueOf("16"));
		cotizacion.setFolio(NUEVOCOTIZADOR);
		if(!cotizacion.getTipoCotizacion().equals(LIGA_AGENTE)){
			cotizacion.setTipoCotizacion(NUEVOCOTIZADOR);
		}
	    cotizacion.setIdMedioPago(BigDecimal.valueOf(MedioPagoDTO.MEDIO_PAGO_AGENTE));

	    if( sol!= null && sol.getCodigoUsuarioCreacion() != null ){
	    	cotizacionDTO = cotizacionService.crearCotizacion(cotizacion,sol.getCodigoUsuarioCreacion());
	    } else {
	    	cotizacionDTO = cotizacionService.crearCotizacion(cotizacion,null);
	    }

	    return cotizacionDTO;
	}

	@Override
	public void generaAnexosYCondiciones(BigDecimal idToCotizacion){
		try{
			//Generar condiciones obligatorias
			condicionEspecialCotizacionService.generaCondicionesBaseCompletas(idToCotizacion);
			
			docAnexosService.generateCobDocumentsByCotizacion(idToCotizacion);
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
	}
	
	private CotizacionService cotizacionService;
    
	private CoberturaService coberturaService;

	private CobranzaIncisoService cobranzaIncisoService;
	
	private CalculoService calculoService;
	private ClientesApiService  clienteRest;
	private ClienteFacadeRemote clienteFacadeRemote;
	
	private DireccionFacadeRemote direccionFacadeRemote;
	
	private EntidadService entidadService;
	
	private IncisoService incisoService;
	
	private ListadoService listadoService;

	private DocumentoCarpetaFortimaxService documentoCarpetaService;
	
	private NegocioSeccionService negocioSeccionService;
	
	private ConfiguracionDatoIncisoDao configuracionDatoIncisoDao;
	
	private DatoIncisoCotAutoDao datoIncisoCotAutoDao;
	
	private AgenteMidasService agenteMidasService;
	
	private NegocioTipoPolizaService negocioTipoPolizaService;
	
	private NegocioService negocioService;
	
	private FortimaxV2Service fortimaxV2Service;

	private SistemaContext sistemaContext;
	
	private CondicionEspecialCotizacionService condicionEspecialCotizacionService;
	
	private DocAnexosService docAnexosService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	@EJB
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}
	
	@EJB
	public void setCoberturaService(CoberturaService coberturaService) {
		this.coberturaService = coberturaService;
	}
	
	@EJB
	public void setCobranzaIncisoService(CobranzaIncisoService cobranzaIncisoService) {
		this.cobranzaIncisoService = cobranzaIncisoService;
	}	
	
	@EJB
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@EJB
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}
	
	@EJB
	public void setDireccionFacadeRemote(DireccionFacadeRemote direccionFacadeRemote) {
		this.direccionFacadeRemote = direccionFacadeRemote;
	}
	
	@EJB
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}

	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setDocumentoCarpetaService(
			DocumentoCarpetaFortimaxService documentoCarpetaService) {
		this.documentoCarpetaService = documentoCarpetaService;
	}
	
	@EJB
	public void setNegocioSeccionService(NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}
	
	@EJB
	public void setConfiguracionDatoIncisoDao(
			ConfiguracionDatoIncisoDao configuracionDatoIncisoDao) {
		this.configuracionDatoIncisoDao = configuracionDatoIncisoDao;
	}
	
	@EJB
	public void setDatoIncisoCotAutoDao(DatoIncisoCotAutoDao datoIncisoCotAutoDao) {
		this.datoIncisoCotAutoDao = datoIncisoCotAutoDao;
	}
	
	@EJB	
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}

	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	@EJB
	public void setNegocioTipoPolizaService(NegocioTipoPolizaService negocioTipoPolizaService) {
		this.negocioTipoPolizaService = negocioTipoPolizaService;
	}
	
	@EJB
	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}

	@EJB
	public void setFortimaxV2Service(FortimaxV2Service fortimaxV2Service) {
		this.fortimaxV2Service = fortimaxV2Service;
	}
	
	@EJB
	public void setCondicionEspecialCotizacionService(CondicionEspecialCotizacionService condicionEspecialCotizacionService) {
		this.condicionEspecialCotizacionService = condicionEspecialCotizacionService;
	}

	@EJB
	public void setDocAnexosService(DocAnexosService docAnexosService) {
		this.docAnexosService = docAnexosService;
	}
	@EJB
	public void setClienteRest(ClientesApiService clienteRest) {
		this.clienteRest = clienteRest;
	}

}
