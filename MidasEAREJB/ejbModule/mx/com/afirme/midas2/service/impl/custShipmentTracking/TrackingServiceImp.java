package mx.com.afirme.midas2.service.impl.custShipmentTracking;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.custShipmentTracking.TrackingDao;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasiva;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.BitacoraGuia;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.RegistroGuiasProveedor;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.RegistroGuiasProveedorId;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.ReporteEfectividadEntrega;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.custShipmentTracking.TrackingService;
import mx.com.afirme.midas2.service.poliza.PolizaService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class TrackingServiceImp implements TrackingService{

	public static final int numeroPolizaLenght = 3;
	@EJB
	private TrackingDao trackingDao;
	
	@EJB
	private PolizaService polizaService;
	
	@EJB
	private GenerarPlantillaReporte generarPlantillaReporte;
	
	@Resource
	private TimerService timerService;
	
	@EJB
	private SistemaContext sistemaContext;
	public static final Logger LOG = Logger.getLogger(TrackingServiceImp.class);
	
	@Override
	public void getGuias(List<String> guias){
		trackingDao.getGuias(guias);
	}
	
	@Override
	public void saveCargaMasiva(List<RegistroGuiasProveedorId> ids){
		for (RegistroGuiasProveedorId id : ids) {
			RegistroGuiasProveedor registro = trackingDao.findRegistroById(id);
			if(registro==null){
				saveRegistroGuiasProveedor(id, false);
			}
		}
	}
	
	private void saveRegistroGuiasProveedor(RegistroGuiasProveedorId id,
			boolean procesado) {
		RegistroGuiasProveedor registro = new RegistroGuiasProveedor();
		registro.setId(id);
		registro.setProcesado(procesado);
		trackingDao.saveRegistroGuiasProveedor(registro);
		
	}
	
	@Override
	public void procesarEstatusPendientes(){
		List<RegistroGuiasProveedor> registros = trackingDao.getRegiostosGuiaSinProcesar();
		for(RegistroGuiasProveedor registro : registros){
			String numeroPolizaRenovada = registro.getId().getIdToPolizaRenovada();
			try{
				if(validaNumeroPoliza(numeroPolizaRenovada)){
					String[] numeroPoliza = numeroPolizaRenovada.split("-");
					PolizaDTO poliza  = new PolizaDTO();
					poliza = polizaService.find(numeroPoliza[0].substring(0, 2), 
							numeroPoliza[0].substring(2), new Integer(numeroPoliza[1]), 
							new Integer(numeroPoliza[2].substring(1)));
						
					if(poliza!=null){
						BitacoraGuia bitacora = saveBitacora(registro.getId().getIdGuia(),
								poliza.getIdToPoliza(), poliza.getIdToOrdenRenovacion());
						registro.setProcesado(true);
						trackingDao.updateRegistroGuia(registro);
						trackingDao.generaDetallePendienteGuia(bitacora);
					}
				}
			}catch(Exception e){
				LOG.info("Error al procesar numero poliza: "+numeroPolizaRenovada);
				LOG.error("Error:" + e.getMessage(), e);
				registro.setProcesado(true);
				trackingDao.updateRegistroGuia(registro);				
			}
		}
	}

	@Override
	public TransporteImpresionDTO getReporteEfectividad(OrdenRenovacionMasiva ordenRenovacion)throws Exception{
		OrdenRenovacionMasiva ordenRenovacionConFechas = validaFechaCreacion(ordenRenovacion);
		List<ReporteEfectividadEntrega> listDataSource = new ArrayList<ReporteEfectividadEntrega>(1);
		listDataSource = trackingDao.getDatosReporteEfectividadEntrega(ordenRenovacionConFechas);
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("where", getWhere(ordenRenovacionConFechas));
		params.put("estatusRechazo", BitacoraGuia.estatusGuia.DEVOLUCION.valor());
		return generarPlantillaReporte.imprimirReporteEfectividadEntrega(listDataSource, params);		
	}

	private String getWhere(OrdenRenovacionMasiva or) {
		String where = "";
		SimpleDateFormat simpleFormat = (SimpleDateFormat) DateFormat.getDateInstance();
		simpleFormat.applyPattern("dd/MM/yyyy");
      	if(or.getIdToOrdenRenovacion()==null){
      		if(or.getNumeroPoliza()==null)
      			where = " (trunc(ORM.FECHACREACION) BETWEEN to_date('"+simpleFormat.format(or.getFechaCreacion())+
      			"','dd/MM/yyyy') AND to_date('"+simpleFormat.format(or.getFechaCreacionHasta())+"','dd/MM/yyyy') )";
      		else
      			where += " POLIZA.NUMEROPOLIZA = "+ or.getNumeroPoliza() +" ";
      		if(or.getClaveEstatus()!=null)
      			where += (where.isEmpty()?"":" AND ").concat("ORM.CLAVEESTATUS = "+ or.getClaveEstatus()) ;
      		if(or.getNombreUsuarioCreacion()!=null && !or.getNombreUsuarioCreacion().isEmpty())
      			where += (where.isEmpty()?"":" AND ").concat(" ORM.NOMBREUSUARIOCREACION = "+ or.getNombreUsuarioCreacion()) ;
      	}else{
      		where = " ORM.IDTOORDENRENOVACION= "+or.getIdToOrdenRenovacion();
      	}
		return where;
	}
	
	@Override
	public void procesarTareaEstatusPendientes(){
		LOG.info("Iniciando ejecución tarea procesarTareaEstatusPendientes");
		trackingDao.procesarTareaEstatusPendientes();
	}
	
	@Override
	public List<BitacoraGuia> getBitacoraByIdToPoliza(BigDecimal idToPoliza){
		return trackingDao.getBitacoraByIdToPoliza(idToPoliza);
	}
	
	@Override
	public String getEstatusBitacora(Integer estatus){
		return trackingDao.getEstatusBitacora(estatus);
	}
	
	private BitacoraGuia saveBitacora(String idGuia, BigDecimal idToPoliza, 
			BigDecimal idToOrdenRenovacion) {
		BitacoraGuia bitacora = new BitacoraGuia();
		bitacora.setFecha(new Date());
		bitacora.setIdGuia(idGuia);
		bitacora.setIdToPoliza(idToPoliza);
		bitacora.setIdToOrdenRenovacion(idToOrdenRenovacion);
		bitacora.setEstatus(BitacoraGuia.estatusGuia.PROCESADA.valor());
		return trackingDao.saveBitacora(bitacora);
		
	}

	private boolean validaNumeroPoliza(String numeroPoliza){
		boolean isValido = true;

		String[] numeroPolizaFormat = numeroPoliza.split("-");
		if(numeroPolizaFormat==null){
			isValido = false;
		}
		if(numeroPolizaFormat.length!=numeroPolizaLenght){
			isValido = false;
		}
		if(numeroPolizaFormat[0]==null && numeroPolizaFormat[1]==null && numeroPolizaFormat[2]==null){
			isValido = false;
		}
			
		return isValido;
	}
	
	private OrdenRenovacionMasiva validaFechaCreacion(OrdenRenovacionMasiva ordenRenovacion) {
		final Calendar theDate = GregorianCalendar.getInstance();
		if(ordenRenovacion.getFechaCreacion() !=null
				&& ordenRenovacion.getFechaCreacionHasta()==null){
			theDate.add(Calendar.MONTH, 1);
			theDate.add(Calendar.DATE, -1);
			ordenRenovacion.setFechaCreacionHasta(theDate.getTime());
			
		}else if(ordenRenovacion.getFechaCreacion() ==null
				&& ordenRenovacion.getFechaCreacionHasta()!=null){
			theDate.set(Calendar.DATE, 1);
			ordenRenovacion.setFechaCreacion(theDate.getTime());
			
		}else if(ordenRenovacion.getFechaCreacion() ==null
				&& ordenRenovacion.getFechaCreacionHasta()==null){
			theDate.set(Calendar.DATE, 1);
			ordenRenovacion.setFechaCreacion(theDate.getTime());
			theDate.add(Calendar.MONTH, 1);
			theDate.add(Calendar.DATE, -1);
			ordenRenovacion.setFechaCreacionHasta(theDate.getTime());
		}
		if(ordenRenovacion.getNombreUsuarioCreacion().isEmpty()){
			ordenRenovacion.setNombreUsuarioCreacion(null);
		}
		return ordenRenovacion;
	}

	public PolizaService getPolizaService() {
		return polizaService;
	}

	public void setPolizaService(PolizaService polizaService) {
		this.polizaService = polizaService;
	}
	
	public void initialize() {
		String timerInfo = "TimerTracking";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {				
				expression.minute(0);
				expression.hour(2);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerTracking", false));
				
				LOG.info("Tarea TimerTracking configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerTracking");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerTracking:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		procesarTareaEstatusPendientes();
	}
	
}
