package mx.com.afirme.midas2.service.impl.vida;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import mx.com.afirme.midas.danios.reportes.reportercs.caducidad.CaducidadesDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.calificaciones.CalificacionesREASDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao;
import mx.com.afirme.midas2.dao.vida.ArchivosCompDao;
import mx.com.afirme.midas2.domain.vida.ValoresRescate;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.vida.ArchivosCompService;

@Stateless
public class ArchivosCompServiceImpl implements ArchivosCompService {
	
	private InputStream is;
	
	private String resultado;
	private String extension;
	public static final String EXITOSO = "exitoso";
	public static final String ERROR = "error";
	
	private List<CaducidadesDTO> listaCadAmis = new ArrayList<CaducidadesDTO>();
	private List<ValoresRescate> listaValRescate = new ArrayList<ValoresRescate>();
	private List<CalificacionesREASDTO> listaCalReas = new ArrayList<CalificacionesREASDTO>();
	
	private static final String TIPO_AMIS = "1";
	private static final String TIPO_VAL_RESCATE = "2";
	private static final String TIPO_CALIFICACIONES = "3";
	private Date fechaCorte;
	
	String error = "";

		
	@EJB
	private EntidadHistoricoDao entidadService;

	@EJB
	private SistemaContext sistemaContext;

	@EJB
	private ArchivosCompDao archivoCompDao;

	@Override
	public String procesarInfo(BigDecimal idToControlArchivo, String tipoArchivo, String fechaCorte) {
		
		try {
			this.fechaCorte = getFechaFromString(fechaCorte);
		} catch (ParseException e) {
			LogDeMidasEJB3.log("Falla al obtnener la fecha", Level.SEVERE, e);
		}
		try {
			// Cargar el archivo
			is = getArchivoDatos(idToControlArchivo);
			
			if(tipoArchivo.equalsIgnoreCase(TIPO_AMIS)){
				listaCadAmis = listarCadAmis();
				
				if(listaCadAmis != null)
				{
					archivoCompDao.borraTablasVida(listaCadAmis.get(0).getAnio().intValue(),Integer.parseInt(TIPO_AMIS));
					for (CaducidadesDTO caducidad : listaCadAmis) {
								archivoCompDao.insertaCaducidad(
									caducidad); 
						  }
					resultado = "exitoso";
				}else{
					resultado = error;
				}
				
			}else if(tipoArchivo.equals(TIPO_VAL_RESCATE)){
				listaValRescate = listarValoresRescate();
				//archivoCompDao.borraTablasVida(this.fechaCorte,Integer.parseInt(TIPO_VAL_RESCATE));
				for (ValoresRescate valorRescate : listaValRescate) {
						archivoCompDao.insertaValoresRescate(
								valorRescate);
					  }
					resultado = error;
					
			}else if(tipoArchivo.equals(TIPO_CALIFICACIONES)){
				listaCalReas=listarCalReas();
				StringBuilder log1 = new StringBuilder("");
				StringBuilder log2 = new StringBuilder("");
				int registrosOK = 0;
				int registrosFAIL01 = 0;
				int registrosFAIL02 = 0;
				for (CalificacionesREASDTO calRea : listaCalReas) {
					
					
					switch (archivoCompDao.actualizaCalReas(calRea)) {
					case 0:log1.append(calRea.getRgre()).append("<br>");registrosFAIL01++;
						break;
					case 1:registrosOK++;break;
					case 2:registrosFAIL02++;
						log2.append(calRea.getRgre()).append("<br>");
						break;
					default:
						break;
					}
							
				}
				resultado = "<table><tr><td>" +
						"Registros actualizados : " + registrosOK + "</td></tr>" +
						"<tr><td>Reaseguradoras no encontrados en Sistema : " + registrosFAIL01 + " <br>" + log1.toString() +"</td></tr>" +
						"<td>Clave Calificacion no encontrados en Sistema : " + registrosFAIL02 + " <br>" + log2.toString() + "</td></tr></table>";
				;
				System.out.println(resultado);
			}

		} catch (RuntimeException ex) {
			resultado = ERROR;
			LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
		}
		
		return resultado;
	}

	private InputStream getArchivoDatos(BigDecimal idToControlArchivo) {

		InputStream archivo = null;
		ControlArchivoDTO controlArchivoDTO = entidadService.findById(ControlArchivoDTO.class, idToControlArchivo);
		String nombreCompletoArchivo = null;
		String nombreOriginalArchivo = controlArchivoDTO.getNombreArchivoOriginal();
		extension = (nombreOriginalArchivo.lastIndexOf('.') == -1) ? ""	: nombreOriginalArchivo.substring(
						nombreOriginalArchivo.lastIndexOf('.'),
						nombreOriginalArchivo.length());

		LogDeMidasEJB3.log("Buscando el archivo", Level.INFO, null);
		nombreCompletoArchivo = controlArchivoDTO.getIdToControlArchivo().toString() + extension;
		try {
			archivo = new FileInputStream(sistemaContext.getUploadFolder()
					+ nombreCompletoArchivo);
		} catch (FileNotFoundException re) {
			LogDeMidasEJB3.log("Archivo no encontrado", Level.SEVERE, re);
		}

		return archivo;
	}
	
	public List<CaducidadesDTO> listarCadAmis() {
		List<CaducidadesDTO> listaCadAmis = null;
		try {
			
			listaCadAmis = new ArrayList<CaducidadesDTO>();
			Workbook plantilla = getTipoExcel();
			Sheet worksheet = plantilla.getSheetAt(0);
			Integer anio = 0;
			String tipoMoneda = "", plan = "";
			Integer porcentaje = 0;
			Integer anioVigencia = 0;
			
			Row row;
			Cell cell;
			
			for (int i = 1; i <= worksheet.getLastRowNum(); i++) {
				row = worksheet.getRow(i);
				if (!isEmptyRow(row)) {
					
					for (int j = 0; j <= 4; j++) {
						
						//if (j == 0 || j == 8 || j == 9 || j == 14 || j == 19) {
							cell = row.getCell(j);

							if (j == 0 ){
								if(cell != null && cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
									anio = (int) (cell.getNumericCellValue());
									if(anio<2000){error = "Linea : " + i+1 + " ANIO : Formato no Valido";listaCadAmis = null;break;}
								}else{
									error = "Linea : " + i+1 + "ANIO vacio/No Numerico";
									listaCadAmis = null;
									break;
								}
												
							} else if (j == 1){
								
								if(cell != null 
									&& cell.getCellType() != Cell.CELL_TYPE_BLANK
									&& cell.getCellType() == Cell.CELL_TYPE_STRING) {
									plan = (cell.getStringCellValue());
									
									if(plan.equals("T")||plan.equals("D")||plan.equals("V"))
									{
										error = "";
									}else{
										error = "Linea : " + i+1 + " Campo: 'PLAN' Formato no Valido(T-D-V)";
										listaCadAmis = null;break;
									}
								}else{
									error = "Linea : " + i+1 + " Campo: 'PLAN' Vacio";
									listaCadAmis = null;
									break;
								}
															
							}else if (j == 2){
								
								if(cell != null 
								&& cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
									anioVigencia = (int) cell.getNumericCellValue();
									if(anioVigencia>115){error = "Linea : " + (i+1) + " ANIOVIGENCIA : Formato no Valido";listaCadAmis = null;break;}
								}else{
									error = "Linea : " + i+1 + " Campo: 'ANIOVIGENCIA' Vacio/No Numerico";
									listaCadAmis = null;
									break;
								}
									
							}else if (j == 3){
								if(cell != null
							
									&& cell.getCellType() != Cell.CELL_TYPE_BLANK
									&& cell.getCellType() == Cell.CELL_TYPE_STRING) {
									tipoMoneda = (cell.getStringCellValue());
									if(tipoMoneda.equals("MN")||tipoMoneda.equals("DLS"))
									{
										error = "";
									}else{
										error = "Linea : " + i+1 + " Campo: 'TIPOMONEDA' Formato no Valido(MN-DLS)";
										listaCadAmis = null;break;
									}
								}else{
									error = "Linea : " + i+1 + " Campo: 'TIPOMONEDA' Vacio";
									listaCadAmis = null;
									break;
								}
								    
							}
							else if (j == 4){
								if(cell != null
									&& cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
								porcentaje = (int) (cell.getNumericCellValue());
								if(porcentaje>=100){error = "Linea : " + i+1 + " PORCENTAJE : Formato no Valido";listaCadAmis = null;break;}
								}else{
									error = "Linea : " + i+1 + " Campo: 'PORCENTAJE' Vacio/No Numerico";
									listaCadAmis = null;
									break;
								}
							}
						//}
						
					}
					if(listaCadAmis != null)
					{
						listaCadAmis.add(new CaducidadesDTO( new BigDecimal(anio), plan, new BigDecimal(anioVigencia), tipoMoneda, new BigDecimal(porcentaje)));
					}else{
						break;
					}
					
				}
			}
			
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					LogDeMidasEJB3.log("Falla al cerrar el Stream del archivo", Level.SEVERE, e);
				}
			}
		}
		return listaCadAmis;		
	}
	
	public List<ValoresRescate> listarValoresRescate() {
		List<ValoresRescate> listaValoresRescate = null;
		try {
			
			listaValoresRescate = new ArrayList<ValoresRescate>();
			Workbook plantilla = getTipoExcel();
			Sheet worksheet = plantilla.getSheetAt(0);
			String producto = null;
			int edadContratacion = 0, temporalidad = 0, moneda = 0;
			int anioVigencia = 0;
			Double valorRescate = 0.0;
			
			Row row;
			Cell cell;
			
			for (int i = 3; i <= worksheet.getLastRowNum(); i++) {
				row = worksheet.getRow(i);
				if (!isEmptyRow(row)) {
					
					for (int j = 0; j <= 5; j++) {
						
							cell = row.getCell(j);

							if (j == 0 && cell != null 
									&& cell.getCellType() != Cell.CELL_TYPE_BLANK
									&& cell.getCellType() == Cell.CELL_TYPE_STRING) {
								producto = cell.getStringCellValue();
															
							} else if (j == 1 && cell != null 
									&& cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
								temporalidad = (int) (cell.getNumericCellValue());
															
							}else if (j == 2 && cell != null 
									&& cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
								moneda = (int) cell.getNumericCellValue();
															
							}else if (j == 3 && cell != null
									&& cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
								    edadContratacion = (int) (cell.getNumericCellValue());
								    
							}
							else if (j == 4 && cell != null
									&& cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
								    anioVigencia = (int) (cell.getNumericCellValue());
								    
							}
							else if (j == 5 && cell != null
									&& cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
									valorRescate = (Double) (cell.getNumericCellValue());
								    
							}
						
					}
					listaValoresRescate.add(new ValoresRescate(producto, temporalidad, moneda, edadContratacion, anioVigencia, valorRescate));
					
				}
			}
			
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					LogDeMidasEJB3.log("Falla al cerrar el Stream del archivo", Level.SEVERE, e);
				}
			}
		}
		return listaValoresRescate;		
	}
	
	public List<CalificacionesREASDTO> listarCalReas() {
		List<CalificacionesREASDTO> listaCalReas = null;
		try {			
			listaCalReas = new ArrayList<CalificacionesREASDTO>();
			Workbook plantilla = getTipoExcel();
			Sheet worksheet = plantilla.getSheetAt(0);
			String snp = null, ambest = null, rgre = null, otras = null;
			Row row;
			Cell cell;
		
			for (int i = 5; i <= worksheet.getLastRowNum(); i++) {
				row = worksheet.getRow(i);
				if (!isEmptyRow(row)) {
					for (int j = 5; j <= 10; j++) {
					
							cell = row.getCell(j);
							if (j == 5 && cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK&& cell.getCellType() == Cell.CELL_TYPE_STRING) {
								rgre = cell.getStringCellValue();
							
							}else if (j == 6 && cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK && cell.getCellType() == Cell.CELL_TYPE_STRING) {
								ambest = cell.getStringCellValue();
							}else if (j == 8 && cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK&& cell.getCellType() == Cell.CELL_TYPE_STRING) {
								snp = cell.getStringCellValue();
							}else if (j == 10 && cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
								    otras = cell.getStringCellValue();
							}	
					  
					}
					if(rgre!=null){
					listaCalReas.add(new CalificacionesREASDTO(rgre, ambest, snp, otras, 0));
				}
					otras = null;
					ambest = null;
					rgre = null;
					snp = null;
			 }
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, e);
				}
			}
		}
			return listaCalReas;
	}

	public static String getPoliza(String polizaCompleta) {

		String poliza = "";
		 try{
			 if(polizaCompleta != null){
				int  primera  = Integer.parseInt(polizaCompleta.split("-")[0]);
				int  tercera  = Integer.parseInt(polizaCompleta.split("-")[2]);
				BigDecimal  segunda  = new BigDecimal(polizaCompleta.split("-")[1]);
		   
				poliza = primera+"-"+segunda+"-"+tercera;
				
				return poliza;			
			 }
	        }catch(NumberFormatException ex){
	        	LogDeMidasEJB3.log("Error al convertir el numero de la poliza:", Level.SEVERE, ex);
	        }catch(ArrayIndexOutOfBoundsException ex){
	        	LogDeMidasEJB3.log("Error al convertir el numero de la poliza:", Level.SEVERE, ex);
		    }

		return poliza;
	}
	
	public Workbook getTipoExcel() {
		Workbook excel = null;
		if (extension.equalsIgnoreCase(".XLSX")) {
			try {
				excel = new XSSFWorkbook(is);
				return excel;
			} catch (IOException ex) {
				LogDeMidasEJB3.log("Extension de archivo invalida", Level.SEVERE, ex);
			}
		} else if (extension.equalsIgnoreCase(".XLS")) {
			try {
				excel = new HSSFWorkbook(is);
				return excel;
			} catch (IOException ex) {
				LogDeMidasEJB3.log("Extension de archivo invalida", Level.SEVERE, ex);
			}
		} else {
			    LogDeMidasEJB3.log("El archivo no tiene un formato estandar", Level.SEVERE, null);
		}
		return excel;
	}

	public static boolean isEmptyRow(Row row) {
		boolean isEmptyRow = true;
		for (int cellNum = row.getFirstCellNum(); cellNum < row
				.getLastCellNum(); cellNum++) {
			Cell cell = row.getCell(cellNum);
			if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK
					&& StringUtils.isNotBlank(cell.toString())) {
				isEmptyRow = false;
			}
		}
		return isEmptyRow;
	}
	
	public static Date getVigencia(int valor, String vigenciaCompleta){
		 
		 Date sql = null;
		
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			 	java.util.Date fecha = null;
				try {
					if(valor==0){
						fecha = format.parse(vigenciaCompleta.substring(0, 10));
						}else if (valor==1){
						fecha = format.parse(vigenciaCompleta.substring(13, 24));	
						}
				} catch (ParseException e) {
					LogDeMidasEJB3.log("Falla al convertir la fecha", Level.SEVERE, e);
				}
			 	sql = new java.sql.Date(fecha.getTime());
		   
			 return sql;
	 }
	
	/**
	 * Method getFechaFromString
	 * 
	 * @param fecha
	 *            : la fecha como String con formato "dd/MM/yyyy"
	 * @return date: la fecha transformada a un objeto java.sql.Date
	 */
	public static Date getFechaFromString(String fecha) throws ParseException {
		java.util.Date fechaCorte = null;
		Date sqlDate = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
		fechaCorte = sdf.parse((fecha));
		sqlDate = new java.sql.Date(fechaCorte.getTime());
		} catch (ParseException e) {
			LogDeMidasEJB3.log("Falla al obtnener la fecha", Level.SEVERE, e);
		}
		return sqlDate;
	}

}
