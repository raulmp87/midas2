package mx.com.afirme.midas2.service.impl.siniestros.recuperacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.liquidacion.LiquidacionSiniestroDao;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.RecuperacionDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina.EnumModoEnvio;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCalculoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteRoboSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.RecepcionDocumentosSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.RecepcionDocumentosSiniestro.TipoPersona;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.notificaciones.BitacoraNotificacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento.TipoSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.VentaSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.VentaSalvamento.EstatusSalvamento;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.siniestros.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.IndicadoresVentaDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioFiltro;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal.PerdidaTotalService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.BitacoraNotificacionService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EmailDestinaratios;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSalvamentoService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.LocalDate;

@Stateless
public class RecuperacionSalvamentoServiceImpl extends RecuperacionServiceImpl implements RecuperacionSalvamentoService{

	private static final String SI_VALUE = "SI";
	private static final String NO_VALUE = "NO";
	private static final Logger log = Logger.getLogger(RecuperacionSalvamentoServiceImpl.class);
	
	@Resource
	private javax.ejb.SessionContext sessionContext;
	
	@Resource	
	private TimerService timerService;
	
	@EJB
	private PrestadorDeServicioService prestadordDeServicioService;
	
	@EJB
	private BitacoraService bitacoraService;
	
	@EJB
	private RecuperacionDao recuperacionDao;
	
	@EJB
	private LiquidacionSiniestroDao liquidacionSiniestroDao;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private RecuperacionService recuperacionService;
	
	@EJB
	private EnvioNotificacionesService notificacionService;
	
	@EJB
	private IngresoService ingresoService;
	
	@EJB
	private PerdidaTotalService perdidaTotalService;
	
	@EJB
	private BitacoraNotificacionService bitacoraNotificacionService;
	
	@EJB
	private ParametroGlobalService parametroGlobalService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	@Override
	public void guardarRecuperacionSalvamento(Long estimacionId,
			BigDecimal valorEstimado, String ubicacionDeterminacion,
			TipoSalvamento tipoSalvamento, Boolean abandonoIncosteable,
			String direccionSalvamento, String codigoCiudadMidas) {
		this.guardarRecuperacionSalvamento(estimacionId, valorEstimado, 
				ubicacionDeterminacion, tipoSalvamento, 
				abandonoIncosteable, direccionSalvamento, 
				codigoCiudadMidas, null);
	}
	
	@Override
	public void guardarRecuperacionSalvamento(Long estimacionId,
			BigDecimal valorEstimado, String ubicacionDeterminacion,
			TipoSalvamento tipoSalvamento, Boolean abandonoIncosteable,
			String direccionSalvamento, String codigoCiudadMidas, String motivoIncosteable) {
		String usuario = usuarioService.getUsuarioActual().getNombreUsuario();		
		RecuperacionSalvamento salvamento = obtenerRecuperacionSalvamento(estimacionId);
		CiudadMidas ciudad = entidadService.findById(CiudadMidas.class, codigoCiudadMidas);
		if(salvamento == null){			
			EstimacionCoberturaReporteCabina estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, estimacionId);
			salvamento = new RecuperacionSalvamento(valorEstimado, abandonoIncosteable, ubicacionDeterminacion, 
					direccionSalvamento, ciudad, estimacion, tipoSalvamento, usuario, motivoIncosteable);			
			salvamento.setNumero(entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO));	
			guardarRecuperacionSalvamento(salvamento);
			guardarCoberturasRecuperacion(salvamento, estimacion);
			guardarPasesRecuperacion(salvamento.getId(), estimacion);
			bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.RECUPERACION_SALVAMENTO, salvamento.getId().toString(),
					"Se ha generado una Recuperacion de Salvamento con id" + salvamento.getId(), salvamento, usuario );		
		}else{
			salvamento.setValorEstimado(valorEstimado);
			salvamento.setAbandonoIncosteable(abandonoIncosteable);
			salvamento.setTipoSalvamento(tipoSalvamento);
			salvamento.setDireccionSalvamento(direccionSalvamento);
			salvamento.setUbicacionDeterminacion(ubicacionDeterminacion);
			salvamento.setCiudad(ciudad);			
			guardarRecuperacionSalvamento(salvamento);
		}		
	}
	
	@Override
	public void guardarRecuperacionSalvamento(RecuperacionSalvamento salvamento) {		
		String usuario = usuarioService.getUsuarioActual().getNombreUsuario();	
		if(salvamento.getId() != null && salvamento.getFechaInicioSubasta() != null && !tieneLiquidacionPagada(salvamento.getEstimacion().getId())){
			throw new NegocioEJBExeption("-SVM001", " Se requiere una liquidación aplicada para guardar la fecha de incio de subasta");
		}
		actualizarInformacionOrigenSalvamento(salvamento);
		if(salvamento.getAbandonoIncosteable().booleanValue()){			
			if(StringUtil.isEmpty(salvamento.getMotivoAbandonoIncosteable())){
				throw new RuntimeException("Se requiere un motivo de abandono en caso de marcarse como tal");
			}
			salvamento.setCodigoUsuarioAbandono(usuario);
			salvamento.setEstatus(EstatusRecuperacion.INACTIVO.toString());
			
			// CANCELAR NOTIFICACION ENVIO RECURRENTE
			this.cancelaEnvioRecurrenteSalvamento(salvamento);
			
		}else{
			salvamento.setMotivoAbandonoIncosteable(null);
			salvamento.setCodigoUsuarioAbandono(null);
			salvamento.setEstatus(EstatusRecuperacion.REGISTRADO.toString());
		}
		salvamento.setCodigoUsuarioModificacion(usuario);
		salvamento.setFechaModificacion(new Date());	
		
		entidadService.save(salvamento);
	}
	
	/**
	 * Actualiza la informacion de la indemnizacion o el reporte de robo a partir del cual se genero la recuperacion de salvamento
	 * @param salvamento
	 * @return
	 */
	private void actualizarInformacionOrigenSalvamento(RecuperacionSalvamento salvamento){
		String usuario = usuarioService.getUsuarioActual().getNombreUsuario();
		if(salvamento.getTipoSalvamento().toString().equals(TipoSalvamento.POR_DANIOS.toString())){
			List<IndemnizacionSiniestro> indemnizaciones = 
				entidadService.findByProperty(IndemnizacionSiniestro.class, "ordenCompra.idTercero", salvamento.getEstimacion().getId());
			if(indemnizaciones != null 
					&& !indemnizaciones.isEmpty()){
				IndemnizacionSiniestro indemnizacion = indemnizaciones.get(0);
				indemnizacion.setUbicacionSalvamento(salvamento.getUbicacionDeterminacion());
				indemnizacion.setEstimadoValuador(salvamento.getValorEstimado());
				indemnizacion.setCodigoUsuarioModificacion(usuario);
				indemnizacion.setFechaModificacion(new Date());
				entidadService.save(indemnizacion);
				if(indemnizacion.getRecepcionDocumentosSiniestro() != null){
					RecepcionDocumentosSiniestro recepcion = indemnizacion.getRecepcionDocumentosSiniestro();
					recepcion.setDireccion(salvamento.getDireccionSalvamento());
					recepcion.setCodigoUsuarioModificacion(usuario);
					recepcion.setFechaModificacion(new Date());
					entidadService.save(recepcion);
				}
			}
		}else if(salvamento.getTipoSalvamento().toString().equals(TipoSalvamento.RT.toString())){
			List<ReporteRoboSiniestro> robos = 
				entidadService.findByProperty(ReporteRoboSiniestro.class, "coberturaReporteCabinaId", salvamento.getEstimacion().getCoberturaReporteCabina().getId());
			if(robos != null
					&& !robos.isEmpty()){
				ReporteRoboSiniestro robo = robos.get(0);
				robo.setUbicacionVehiculo(salvamento.getUbicacionDeterminacion());
				robo.setMontoProvicion(salvamento.getValorEstimado());
				robo.setCodigoUsuarioModificacion(usuario);
				robo.setFechaModificacion(new Date());
				entidadService.save(robo);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public RecuperacionSalvamento obtenerRecuperacionSalvamento(
			Long estimacionId) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("estimacionId", estimacionId);
		param.put("estatus", EstatusRecuperacion.CANCELADO.toString()); //fix flujo indemnizacion
		List<RecuperacionSalvamento> salvamentos = 
			entidadService.executeQueryMultipleResult(
					"SELECT model FROM RecuperacionSalvamento model WHERE model.estimacion.id = :estimacionId AND model.estatus <> :estatus", 
					param);			
		if(salvamentos != null && salvamentos.size() > 0){
			return salvamentos.get(0);
		}
		return null;
	}

	@Override
	public void inactivarRecuperacionSalvamento(Long id, String causaInactivacion) {
		RecuperacionSalvamento salvamento = entidadService.findById(RecuperacionSalvamento.class, id);
		salvamento.setEstatus(EstatusRecuperacion.INACTIVO.toString());
		salvamento.setCausaInactivacion(causaInactivacion);
		salvamento.setActivo(false);
		entidadService.save(salvamento);
	}

	@Override
	public void reactivarRecuperacionSalvamento(Long id, String causaActivacion) {
		RecuperacionSalvamento salvamento = entidadService.findById(RecuperacionSalvamento.class, id);
		salvamento.setEstatus(EstatusRecuperacion.REGISTRADO.toString());
		salvamento.setCausaInactivacion(causaActivacion);
		salvamento.setActivo(true);
		entidadService.save(salvamento);
	}

	@Override
	public void cancelarRecuperacion(Long id, String motivo){
		super.cancelarRecuperacion(id, motivo);
		
		// CANCELAR ADJUDICACION
		this.cancelarAdjudicacion(id, "CANCELAR VENTA", true );
	}
	
	@Override
	public RecuperacionSalvamento obtenerRecuperacionPorId(Long id){
		RecuperacionSalvamento recuperacion = super.obtenerRecuperacion(RecuperacionSalvamento.class, id);
		//completado de auto inciso 
		if(recuperacion != null){
			/*AutoIncisoReporteCabina autoInciso = recuperacion.getReporteCabina().
				getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();			
			this.obtenerDatosAuto(autoInciso);*/
			AutoIncisoReporteCabina autoInciso = this.obtenerDatosAutoByRecuperacionSalvamento((Recuperacion)recuperacion);
			recuperacion.setPtDocumentada(perdidaTotalService.esPTDocumentada(recuperacion.getEstimacion().getId()) ? SI_VALUE : NO_VALUE);
			recuperacion.setIndemnizada(perdidaTotalService.esPTIndemnizada(recuperacion.getEstimacion().getId()) ? SI_VALUE : NO_VALUE);
		}
		return recuperacion;
	}
	
	/**
	 * Metodo para determinar si la cobertura de la estimacion es de asegurado o de tercero
	 * @param recuperacion
	 * @return true si es cobertura de asegurado, false si es cobertura de tercero
	 */
	private boolean esCoberturaAsegurado(RecuperacionSalvamento recuperacion){ 
		String claveTipoCalculo = recuperacion.getEstimacion().getCoberturaReporteCabina().getClaveTipoCalculo();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("cveTipoCalculoCobertura", claveTipoCalculo);
		List<ConfiguracionCalculoCoberturaSiniestro> resultados = 
			entidadService.findByProperties(ConfiguracionCalculoCoberturaSiniestro.class, params);
		if(resultados != null
				&& !resultados.isEmpty()
				&& resultados.get(0).getCubre().compareTo(
				ConfiguracionCalculoCoberturaSiniestro.ValoresCubre.ASEGURADO.toString()) == 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public AutoIncisoReporteCabina obtenerDatosAutoByRecuperacionSalvamento(Recuperacion recuperacion){

		AutoIncisoReporteCabina autoInciso = recuperacion.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();
		
		if (autoInciso.getTipoUsoId() != null) {
			TipoUsoVehiculoDTO tipoUso = entidadService.findById(TipoUsoVehiculoDTO.class, new BigDecimal(autoInciso.getTipoUsoId()));				
			if (tipoUso != null) {
				autoInciso.setDescTipoUso(tipoUso.getDescription());
			}
		}
		
		if (autoInciso.getMarcaId() != null) {
			autoInciso.setDescMarca(entidadService.findById(MarcaVehiculoDTO.class, autoInciso.getMarcaId()).getDescription());	
		}			
		
		if (autoInciso.getEstiloId() != null) {
			EstiloVehiculoDTO estilo = entidadService.findById(EstiloVehiculoDTO.class, 
					getEstiloVehiculoId(autoInciso.getEstiloId()));				
			if (estilo != null) {
				autoInciso.setDescEstilo(estilo.getDescription());
			}				
		}
		if (autoInciso.getColor() != null) {
			autoInciso.setDescColor(catalogoGrupoValorService.obtenerValorPorCodigo(
					CatGrupoFijo.TIPO_CATALOGO.COLOR, autoInciso.getColor()).getDescripcion());
		}
	
		return autoInciso;
	}
	
private AutoIncisoReporteCabina obtenerDatosAutoByRecuperacionSalvamentoAseguradoYTercero(Recuperacion recuperacion){
		
		if(this.esCoberturaAsegurado((RecuperacionSalvamento)recuperacion)){
			return this.obtenerDatosAutoByRecuperacionSalvamento(recuperacion);
		}else{
			AutoIncisoReporteCabina autoInciso = new AutoIncisoReporteCabina();
			
			EstimacionCoberturaReporteCabina estimacion = ((RecuperacionSalvamento)recuperacion).getEstimacion();
			
			TerceroRCVehiculo tercero = ((TerceroRCVehiculo)estimacion);
			if(tercero != null){
				if(tercero.getModeloVehiculo() != null){
					autoInciso.setModeloVehiculo(tercero.getModeloVehiculo());
				}
				autoInciso.setNumeroSerie(tercero.getNumeroSerie());
				autoInciso.setDescMarca((tercero.getMarca() != null)? tercero.getMarca():"");
				autoInciso.setDescEstilo((tercero.getEstiloVehiculo() != null)? tercero.getEstiloVehiculo() : "");
				autoInciso.setNumeroMotor("-");
				if( !StringUtil.isEmpty(tercero.getColor())){
					autoInciso.setDescColor((catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.COLOR, tercero.getColor())).getDescripcion());
				}
			}
			
			return autoInciso;
		}
		
	}
	
	private EstiloVehiculoId getEstiloVehiculoId(String estiloId) {
		EstiloVehiculoId estiloVehiculoId = null;
		String[] arrayEstilo = StringUtils.split(estiloId, '_');
		if (arrayEstilo.length > 2) {
			estiloVehiculoId = new EstiloVehiculoId(arrayEstilo[0], arrayEstilo[1], new BigDecimal(arrayEstilo[2]));
		}		
		return estiloVehiculoId;		
	}

	
	private boolean tieneLiquidacionPagada(Long estimacionId){		
		EstimacionCoberturaReporteCabina estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, estimacionId);
		List<OrdenCompra> ordenesCompra= entidadService.findByProperty(OrdenCompra.class, 
				"coberturaReporteCabina.id" , estimacion.getCoberturaReporteCabina().getId());
		for(OrdenCompra compra : ordenesCompra){
			if(compra.getOrdenPago() != null && !compra.getEstatus().equals(OrdenCompraService.ESTATUS_CANCELADA)
					&& compra.getOrdenPago().getLiquidacion() != null && compra.getOrdenPago().getLiquidacion().getId() != null){
					LiquidacionSiniestro liquidacion = compra.getOrdenPago().getLiquidacion();
					entidadService.refresh(liquidacion); //actualizar estatus que fue modificado directo en BD por procedure
					if(liquidacion.getEstatus().equals(LiquidacionSiniestro.EstatusLiquidacionSiniestro.APLICADA.getValue())){
						return true;
					}
			}
		}
		return false;
	}

	@Override
	public void editarVentaSalvamento(Long recuperacionId, VentaSalvamento ventaSalvamento) {
		
		RecuperacionSalvamento recuperacion = this.entidadService.findById(RecuperacionSalvamento.class, recuperacionId);
		VentaSalvamento ventaSalvamentoActiva = recuperacion.obtenerVentaActiva();
		
		ventaSalvamentoActiva.setNoFactura(ventaSalvamento.getNoFactura());
		ventaSalvamentoActiva.setCorreo(ventaSalvamento.getCorreo());
		ventaSalvamentoActiva.setFechaFactura(ventaSalvamento.getFechaFactura());
		ventaSalvamentoActiva.setFechaEntrega(ventaSalvamento.getFechaEntrega());
		ventaSalvamentoActiva.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		ventaSalvamentoActiva.setFechaModificacion(new Date());
		
		this.entidadService.save(ventaSalvamentoActiva);
	}

	@Override
	public void guardarVentaSalvamento(Long recuperacionId, VentaSalvamento ventaSalvamento) {
		
		RecuperacionSalvamento recuperacion =  this.obtenerRecuperacionSalvamentoById(recuperacionId);
		
		if(recuperacion.getId() != null && recuperacion.getFechaInicioSubasta() != null && !tieneLiquidacionPagada(recuperacion.getEstimacion().getId())){
			throw new NegocioEJBExeption("-SVM001", " Se requiere una liquidación aplicada y fecha de inicio de subasta para guardar la venta de salvamento");
		}
			
		BigDecimal valorEstimadoDeterminacion = BigDecimal.ZERO;

		IndemnizacionSiniestro indemnizacion = this.recuperacionDao.obtenerDeterminacionPorRecuperacion(recuperacion.getEstimacion().getId());

		valorEstimadoDeterminacion = indemnizacion.getValorUnidad();
//		if( indemnizacion.getEsPagoDanios() ){
//			valorEstimadoDeterminacion = indemnizacion.getTotalPagoDanios();
//		}else{
//			valorEstimadoDeterminacion = indemnizacion.getTotalIndemnizar();
//		}

		ventaSalvamento.setEstatus                 ( EstatusSalvamento.ACT );
		ventaSalvamento.setEficienciaIndeminizacion( ventaSalvamento.getTotalVenta().divide(valorEstimadoDeterminacion,2 ).multiply(new BigDecimal(100)) );
		if( recuperacion.getValorEstimado() != null ){
			ventaSalvamento.setEficienciaProvision     ( ( ventaSalvamento.getTotalVenta().divide(recuperacion.getValorEstimado(),2 ) ).multiply(new BigDecimal(100)) );
		}else{
			ventaSalvamento.setEficienciaProvision(BigDecimal.ZERO);
		}

		ventaSalvamento.setCodigoUsuarioCreacion   ( usuarioService.getUsuarioActual().getNombreUsuario() );
		ventaSalvamento.setRecuperacionSalvamento  ( recuperacion );

		recuperacion.setEstatus(EstatusRecuperacion.PENDIENTE.toString());


		// # AUMENTAR EN 1 LA CANIDAD DE SUBASTA
		if (recuperacion.getSubastaActual() == null) { recuperacion.setSubastaActual(new Long(0)); }
		recuperacion.setSubastaActual(recuperacion.getSubastaActual()+1);
		recuperacion.agregarVenta(ventaSalvamento);
		this.entidadService.save(recuperacion);

		// GENERAR RECUPERACION
		this.generarIngreso(recuperacion);

		// # GENERAR REFERENCIAS BANCARIAS
		recuperacionService.generarReferenciaBancaria(recuperacion.getId());

		this.notificarGanadorSalvamento(recuperacion,ventaSalvamento);

		bitacoraService.registrar(TIPO_BITACORA.SALVAMENTO, EVENTO.VENTA_SALVAMENTO, recuperacion.getId().toString(), "CREAR VENTA SALVAMENTO", " RECUPERACION SALVAMENTO {ID} "+recuperacion.getId(), this.usuarioService.getUsuarioActual().getNombreUsuario() );

	}

	@Override
	public Map<Long,String> obtenerCompradores() {
		
		Map<Long,String> mCompradores = new TreeMap<Long,String>();
		PrestadorServicioFiltro filtroPrestadorServicio = new PrestadorServicioFiltro();
		filtroPrestadorServicio.setTipoPrestadorStr("COMP");
		List<PrestadorServicioRegistro> lPrestadorServicio =  this.prestadordDeServicioService.buscar(filtroPrestadorServicio);
		
		for(PrestadorServicioRegistro prestador : lPrestadorServicio){
			mCompradores.put(prestador.getId(), prestador.getNombrePersona() );
		}
		
		return mCompradores;
	}

	@Override
	public VentaSalvamento obtenerVentaSalvamentoInicial(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cancelarAdjudicacion(Long recuperacionId,String motivoCancelacion,  boolean cancelarIngreso) {
		
		RecuperacionSalvamento recuperacion = this.obtenerRecuperacionSalvamentoById(recuperacionId);
		
		if( recuperacion != null ){
			
			VentaSalvamento ventaSalvamento = recuperacion.obtenerVentaActiva();//this.obtenerVentaSalvamentoActiva(recuperacionId);
			
			if( ventaSalvamento != null ){
			
				ventaSalvamento.setEstatus(EstatusSalvamento.CANC);
				ventaSalvamento.setCODIGO_USUARIO_CANCELACION(this.usuarioService.getUsuarioActual().getNombreUsuario());
				ventaSalvamento.setFechaCancelacionDeSubasta(new Date());
				ventaSalvamento.setComentarioCancelacion(motivoCancelacion);
				
				// REINICIAR LOS DATOS DE PRORROGA
				recuperacion.setEstatus(EstatusRecuperacion.REGISTRADO.toString());
				recuperacion.setFechaLimiteProrroga(null);
				recuperacion.setFechaInicioSubasta(null);
				recuperacion.setComentariosProrroga(null);
				recuperacion.setSolicitanteProrroga(null);
				recuperacion.setFechaSolicitudProrroga(null);
				recuperacion.setSolicitudProrroga(Boolean.FALSE);
				recuperacion.setAutorizacionProrroga(Boolean.FALSE);
				recuperacion.setAutorizadorProrroga(null);
				recuperacion.setFechaAutorizacionProrroga(null);

				recuperacion.setBancoConfirmacionIngreso(null);
				recuperacion.setCuentaConfirmacionIngreso(null);
				recuperacion.setFechaDepositoConfirmacionIngreso(null);
				recuperacion.setMontoConfirmacionIngreso(null);
				recuperacion.setFechaDepositoConfirmacion(null);
				recuperacion.setConfirmadoPor(null);
				
				
				this.entidadService.save(ventaSalvamento);
				this.entidadService.save(recuperacion);
				
				// ELIMINAR REFERENCIAS BANCARIAS
				this.eliminarReferenciasBancariasByRecuperacionId(recuperacionId);
				
				// CANCELAR INGRESO
				if( cancelarIngreso ) {
					this.cancelarIngreso(recuperacion);
				}
				
				// BITACORA				
				bitacoraService.registrar(TIPO_BITACORA.SALVAMENTO, EVENTO.CANCELAR_VENTA_SALVAMENTO, ventaSalvamento.getId().toString(), "CANCELAR VENTA SALVAMENTO", "VENTA SALVAMENTO {ID} "+ventaSalvamento.getId(), this.usuarioService.getUsuarioActual().getNombreUsuario() );
				
				// NOTIFICACION DE CANCELAR ADJUDICACION
				this.notificarCancelarAdjudicacion(recuperacion, ventaSalvamento);
			}
			
			// CANCELAR NOTIFICACION ENVIO RECURRENTE
			this.cancelaEnvioRecurrenteSalvamento(recuperacion);
		}
		
	}
	
	private void cancelarIngreso(RecuperacionSalvamento salvamento){
		Ingreso ingreso = obtenerIngresoActivo(salvamento.getId());	
		if ( ingreso != null ){
			ingresoService.cancelarIngresoPendientePorAplicar(ingreso, "CANCELACION_POR_VENTA");
		}else{
			throw new NegocioEJBExeption("-SVM003", "No existe ingreso pendiente para el recuperación: "+salvamento.getNumero());
		}		
	}

	@Override
	public void autorizarProrroga(Long recuperacionId, String respuestaAutorizacion, Date fechaProrroga, String comentario) {
		
		RecuperacionSalvamento recuperacion = this.entidadService.findById(RecuperacionSalvamento.class, recuperacionId);
		
		recuperacion.setFechaAutorizacionProrroga(new Date());
		recuperacion.setAutorizadorProrroga(this.usuarioService.getUsuarioActual().getNombreUsuario());
		recuperacion.setAutorizacionProrroga( ( respuestaAutorizacion.toLowerCase().equals("s") ? Boolean.TRUE : Boolean.FALSE ) );
		recuperacion.setFechaLimiteProrroga(fechaProrroga);
		recuperacion.setComentariosProrroga(comentario);
		
		this.entidadService.save(recuperacion);
		
		// BITACORA				
		bitacoraService.registrar(TIPO_BITACORA.SALVAMENTO, EVENTO.AUTORIZAR_PRORROGA_SALVAMENTO, recuperacionId.toString(), "AUTORIZAR PRORROGA", "RECUPERACION SALVAMENTO {ID} "+recuperacionId, this.usuarioService.getUsuarioActual().getNombreUsuario() );

		// ELIMINAR REFERENCIAS BANCARIAS
		this.eliminarReferenciasBancariasByRecuperacionId(recuperacionId);
		// # GENERAR REFERENCIAS BANCARIAS
		recuperacionService.generarReferenciaBancaria(recuperacion.getId());
		
		if( respuestaAutorizacion.toLowerCase().equals("s") ){
			this.notificarProrrogaAprobada(recuperacion,recuperacion.obtenerVentaActiva());
			
		}
		
		// CANCELAR NOTIFICACION ENVIO RECURRENTE
		this.cancelaEnvioRecurrenteSalvamento(recuperacion);
		
	}
	
	public void confirmarIngreso(Long idRecuperacion,Long bancoId, String cuenta, Date fechaConfirmacion,BigDecimal montoConfirmado){
		
		RecuperacionSalvamento recuperacion = this.entidadService.findById(RecuperacionSalvamento.class, idRecuperacion);
		
		recuperacion.setBancoConfirmacionIngreso(bancoId);
		recuperacion.setCuentaConfirmacionIngreso(cuenta);
		recuperacion.setFechaDepositoConfirmacionIngreso(fechaConfirmacion);
		recuperacion.setMontoConfirmacionIngreso(montoConfirmado);
		recuperacion.setFechaDepositoConfirmacion(new Date());
		recuperacion.setConfirmadoPor(this.usuarioService.getUsuarioActual().getNombreUsuario());
		
		this.entidadService.save(recuperacion);
		
		this.notificacionOrdenSalida(recuperacion, recuperacion.obtenerVentaActiva());
		this.notificacionConformidad(recuperacion, recuperacion.obtenerVentaActiva());
		
		// CANCELAR NOTIFICACION ENVIO RECURRENTE
		this.cancelaEnvioRecurrenteSalvamento(recuperacion);
		
	}

	@Override
	public boolean esUsuarioValidoParaAutorizarProrroga(Usuario usuario) {
		
		String[] nombreRoles = {"Rol_M2_Coordinador_Cobro_Companias_Salvamentos","Rol_M2_Gerente_Operaciones_Indemnizacion"};
		
		if( this.usuarioService.tieneRol(nombreRoles, usuario) ){
			return true;
		}
		
		return false;
	}

	@Override
	public void solicitarProrroga(Long recuperacionId, Date fechaProrroga, String comentario) {
		
		RecuperacionSalvamento recuperacion = this.entidadService.findById(RecuperacionSalvamento.class, recuperacionId);
		
		recuperacion.setFechaSolicitudProrroga(new Date());
		recuperacion.setFechaLimiteProrroga(fechaProrroga);
		recuperacion.setComentariosProrroga(comentario);
		recuperacion.setSolicitanteProrroga(this.usuarioService.getUsuarioActual().getNombreUsuario());
		recuperacion.setAutorizacionProrroga(Boolean.FALSE);
		recuperacion.setSolicitudProrroga(Boolean.TRUE);
		
		this.entidadService.save(recuperacion);
		
		// BITACORA				
		bitacoraService.registrar(TIPO_BITACORA.SALVAMENTO, EVENTO.SOLICITAR_PRORROGA_SALVAMENTO, recuperacionId.toString(), "SOLICITAR PRORROGA", "RECUPERACION SALVAMENTO {ID} "+recuperacionId, this.usuarioService.getUsuarioActual().getNombreUsuario() );
		
		this.notificarSolicitudConProrroga(recuperacion);
		
	}
	
	
	private void notificarSolicitudConProrroga(RecuperacionSalvamento recuperacion){
		
		HashMap<String, Serializable> notificacion = new HashMap<String, Serializable>();
		
		String solicitante = "";
	
		//AutoIncisoReporteCabina autoInciso = this.obtenerDatosAuto(recuperacion.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina());
		AutoIncisoReporteCabina autoInciso = this.obtenerDatosAutoByRecuperacionSalvamentoAseguradoYTercero((Recuperacion)recuperacion);
		solicitante = this.usuarioService.getUsuarioActual().getNombre()+" "+this.usuarioService.getUsuarioActual().getApellidoPaterno()+" "+this.usuarioService.getUsuarioActual().getApellidoMaterno();
		
		notificacion.put("comprador"              , recuperacion.obtenerVentaActiva().getComprador().getNombrePersona());
		notificacion.put("marca"                  , autoInciso.getDescMarca() );
		notificacion.put("tipo"                   , autoInciso.getDescEstilo() );
		notificacion.put("modelo"                 , autoInciso.getModeloVehiculo() );
		notificacion.put("fechaAdjudicacion"      , this.generaFechaAmigable(recuperacion.obtenerVentaActiva().getFechaCreacion()) );
		notificacion.put("fechaLimiteProrroga"    , this.generaFechaAmigable(recuperacion.getFechaLimiteProrroga() ));
		notificacion.put("solicitante"            , (StringUtils.isEmpty(solicitante) ? "" : solicitante ) );
		notificacion.put("fechaSolicitudProrroga" , this.generaFechaAmigable(recuperacion.getFechaSolicitudProrroga() ));
		notificacion.put("referencias"            , this.obtenerReferenciasBancarias(recuperacion.getId()) );
		
		this.notificacionService.enviarNotificacion(EnvioNotificacionesService.EnumCodigo.SVM_SOLICITUD_PRORROGA.toString(), notificacion);
		
	}
	
	private String quitarSimbolosDestinatario(VentaSalvamento ventaSalvamento){
		String nombreSinFormato = "";
		if(ventaSalvamento != null
				&& ventaSalvamento.getComprador() != null){
			nombreSinFormato = ventaSalvamento.getComprador().getNombrePersona();
		}
		if(nombreSinFormato != null){
        	return nombreSinFormato.replaceAll("\\W"," "); }
        return nombreSinFormato;
	}
	
	private void notificarGanadorSalvamento(RecuperacionSalvamento recuperacion, VentaSalvamento ventaSalvamento){
		
		// NOTIFICACION DE CORREO
        EmailDestinaratios destinatarios = new EmailDestinaratios();
        
        String nombreCompleto = this.quitarSimbolosDestinatario(ventaSalvamento);
        
        destinatarios.agregar(EnumModoEnvio.PARA, ventaSalvamento.getCorreo() , nombreCompleto ); 
        
        HashMap<String, Serializable> notificacion = new HashMap<String, Serializable>();
        
        //AutoIncisoReporteCabina autoInciso = this.obtenerDatosAuto();
        AutoIncisoReporteCabina autoInciso = this.obtenerDatosAutoByRecuperacionSalvamentoAseguradoYTercero((Recuperacion)recuperacion);
		
		// OBTENER DATOS DE COMPRADOR
		PrestadorServicio prestadorServicio = this.entidadService.findById(PrestadorServicio.class, ventaSalvamento.getComprador().getId());
        
		notificacion.put("comprador"  , prestadorServicio.getNombrePersona() );
		notificacion.put("numSubasta" , ventaSalvamento.getNoSubasta());
        notificacion.put("totalVenta" , ventaSalvamento.getTotalVenta());
        notificacion.put("marca"      , autoInciso.getDescMarca());
		notificacion.put("tipo"       , autoInciso.getDescEstilo());
		notificacion.put("modelo"     , autoInciso.getModeloVehiculo());
        notificacion.put("serie"      , recuperacion.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getNumeroSerie() );
        notificacion.put("motor"      , recuperacion.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getNumeroMotor() );
        notificacion.put("color"      , autoInciso.getDescColor() );
        notificacion.put("textoOperacion","el ganador"  );
        
        notificacion.put("referencias", this.obtenerReferenciasBancarias(recuperacion.getId()) );
       
        this.notificacionService.enviarNotificacion(
				EnvioNotificacionesService.EnumCodigo.SVM_NOTIFICACION_COMPRADOR.toString() , 
				notificacion , 
				ventaSalvamento.getNoSubasta() , destinatarios,
				this.generarPDFInstrucctivoPago(recuperacion.getId()) ); 
        
	}


	private void notificarCancelarAdjudicacion(RecuperacionSalvamento recuperacion, VentaSalvamento ventaSalvamento){
		
		// NOTIFICACION DE CORREO
        EmailDestinaratios destinatarios = new EmailDestinaratios();
        
        String nombreCompleto = this.quitarSimbolosDestinatario(ventaSalvamento);
        
        destinatarios.agregar(EnumModoEnvio.PARA, ventaSalvamento.getCorreo() , nombreCompleto ); 
        
        HashMap<String, Serializable> notificacion = new HashMap<String, Serializable>();
        
        AutoIncisoReporteCabina autoInciso = this.obtenerDatosAutoByRecuperacionSalvamentoAseguradoYTercero((Recuperacion)recuperacion);
        
        // OBTENER DATOS DE COMPRADOR
		PrestadorServicio prestadorServicio = this.entidadService.findById(PrestadorServicio.class, ventaSalvamento.getComprador().getId());
        
		notificacion.put("comprador"  , prestadorServicio.getNombrePersona() );
		notificacion.put("motivoCancelacion" , StringUtil.isEmpty( ventaSalvamento.getComentarioCancelacion() ) ? "": ventaSalvamento.getComentarioCancelacion() );
        notificacion.put("marca"      , StringUtil.isEmpty( autoInciso.getDescMarca()  )  ? "":autoInciso.getDescMarca()  );
		notificacion.put("tipo"       , StringUtil.isEmpty( autoInciso.getDescEstilo()  ) ? "":autoInciso.getDescEstilo() );
		notificacion.put("modelo"     , autoInciso.getModeloVehiculo() );
		notificacion.put("serie"      , autoInciso.getNumeroSerie() );
		notificacion.put("motor"      , autoInciso.getNumeroMotor() );
        notificacion.put("color"      , StringUtil.isEmpty( autoInciso.getDescColor()  )  ? "":autoInciso.getDescColor() );
       
        this.notificacionService.enviarNotificacion(
        											EnvioNotificacionesService.EnumCodigo.SVM_NOT_COMPRADOR_CANC_ADJUDICACION.toString() , 
        											notificacion , 
        											ventaSalvamento.getNoSubasta() , destinatarios); 
        
	}
	
	
	public IndicadoresVentaDTO obtenerIndicadoresVenta(Long recuperacionId){
		
		// # OBJETO A RETORNAR
		IndicadoresVentaDTO indicadoresVentaDto   = new IndicadoresVentaDTO();
		String tipoSiniestroPerdidaTotal = "";
		
		// # OBTENER DATOS DE RECUPERACION Y VENTA ACTIVA
		RecuperacionSalvamento recuperacion  =  this.obtenerRecuperacionSalvamentoById(recuperacionId);
		Ingreso                ingresoActivo =  obtenerIngresoActivo(recuperacion.getId());
		
		if( recuperacion != null ){
			
			// # SOLO LIQUIDACION
			LiquidacionSiniestro liquidacionSiniestro = this.liquidacionSiniestroDao.obtenerLiquidacionPorEstimacion(recuperacion.getEstimacion().getId());
			if( liquidacionSiniestro!= null ){
				indicadoresVentaDto.setFechaIndeminizacion(liquidacionSiniestro.getFechaAplicada() );
				indicadoresVentaDto.setFechaProvision           (liquidacionSiniestro.getFechaAplicada());
				indicadoresVentaDto.setTiempoDeIndeminizacion   (this.diferenciaDiasFechas(liquidacionSiniestro.getFechaAplicada(), recuperacion.getEstimacion().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getSiniestroCabina().getFechaCreacion() ));
			}
			
			// # SOLO RECUPERACION
			indicadoresVentaDto.setFechaSiniestro           (recuperacion.getEstimacion().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getFechaHoraOcurrido() );
			indicadoresVentaDto.setFechaInicioSubasta       (recuperacion.getFechaInicioSubasta());
			indicadoresVentaDto.setFechaConfirmacionIngreso (recuperacion.getFechaDepositoConfirmacionIngreso());
			indicadoresVentaDto.setValorProvisionado        (recuperacion.getValorEstimado());
			if( recuperacion.getFechaDepositoConfirmacionIngreso() != null && recuperacion.getFechaDepositoConfirmacion() != null ){
				indicadoresVentaDto.setTiempoAplicacionIngreso  (this.diferenciaDiasFechas(recuperacion.getFechaDepositoConfirmacionIngreso() , recuperacion.getFechaDepositoConfirmacion() ));
			}
			
			Ingreso ingreso = this.obtenerIngresoActivo(recuperacionId);
			if(ingreso!=null && ingreso.getEstatus().equals(Ingreso.EstatusIngreso.APLICADO.getValue())){
				indicadoresVentaDto.setFechaAplicacionIngreso   (ingreso.getFechaIngreso());
			}
			
			if( ingresoActivo != null ){
				if( recuperacion.getFechaCreacion() != null && ingresoActivo.getFechaIngreso() != null ){
					indicadoresVentaDto.setTiempoDeSubasta          (this.diferenciaDiasFechas(recuperacion.getFechaCreacion(), ingresoActivo.getFechaIngreso() ));
				}
				if( indicadoresVentaDto.getFechaProvision()!=null && indicadoresVentaDto.getFechaAplicacionIngreso()!= null ){
					indicadoresVentaDto.setTiempoPromedioVenta      (this.diferenciaDiasFechas( indicadoresVentaDto.getFechaProvision(),indicadoresVentaDto.getFechaAplicacionIngreso() ));
				}
				
				if( recuperacion.getFechaDepositoConfirmacion() != null ){
					indicadoresVentaDto.setTiempoPromedioTotalVenta (this.diferenciaDiasFechas( recuperacion.getEstimacion().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getFechaCreacion(), recuperacion.getFechaDepositoConfirmacion() ));
				}
			}
			
			// # SOLO VENTA SALVAMENTO ACTIVA
			VentaSalvamento ventaSalvamentoActiva = recuperacion.obtenerVentaActiva();
			if( ventaSalvamentoActiva != null ){
				indicadoresVentaDto.setFechaAsignacion       (recuperacion.obtenerVentaActiva().getFechaCreacion());
				indicadoresVentaDto.setFechaCierreDeSubasta  ( ventaSalvamentoActiva.getFechaCierreDeSubasta());
				indicadoresVentaDto.setTotalDeVenta		     ( ventaSalvamentoActiva.getTotalVenta() );
				//indicadoresVentaDto.setEficienciaProvision   ( ( recuperacion.obtenerVentaActiva().getTotalVenta().divide(recuperacion.getValorEstimado(),2) ).multiply(new BigDecimal(100) ));
				indicadoresVentaDto.setEficienciaProvision   (ventaSalvamentoActiva.getEficienciaProvision());
				if( recuperacion.getFechaInicioSubasta() != null && indicadoresVentaDto.getFechaAsignacion()!=null ){
					indicadoresVentaDto.setTiempoDeAsignacion    (this.diferenciaDiasFechas(recuperacion.getFechaInicioSubasta(), indicadoresVentaDto.getFechaAsignacion() ));
				}
				if( indicadoresVentaDto.getFechaAsignacion()!=null && indicadoresVentaDto.getFechaConfirmacionIngreso() != null ){
					indicadoresVentaDto.setTiempoConfirmacionIngreso(this.diferenciaDiasFechas(indicadoresVentaDto.getFechaAsignacion(), indicadoresVentaDto.getFechaConfirmacionIngreso() ) );
				}
			}
			
			// # SOLO ORDEN COMPRA
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("idTercero", recuperacion.getEstimacion().getId() );
			params.put("estatus"  , OrdenCompraService.ESTATUS_PAGADA );
			List<OrdenCompra> lOrdenCompra = this.entidadService.findByProperties(OrdenCompra.class, params);
			
			// # SOLO INDEMNIZACION
			IndemnizacionSiniestro indemnizacion      = this.recuperacionDao.obtenerDeterminacionPorRecuperacion(recuperacion.getEstimacion().getId());			
			if( indemnizacion != null ){
				
				indicadoresVentaDto.setValorEstimadoEnDeterminacion(indemnizacion.getValorUnidad());
//				if( indemnizacion.getEsPagoDanios() ){
//					indicadoresVentaDto.setValorEstimadoEnDeterminacion( indemnizacion.getTotalPagoDanios() );
//				}else{
//					indicadoresVentaDto.setValorEstimadoEnDeterminacion( indemnizacion.getTotalIndemnizar() );
//				}
				
				if( ventaSalvamentoActiva != null ){
					//indicadoresVentaDto.setEficienciaIndeminizacion ( (ventaSalvamentoActiva.getTotalVenta().divide(indicadoresVentaDto.getValorEstimadoEnDeterminacion(), 2 ) ).multiply(new BigDecimal(100)) );
					indicadoresVentaDto.setEficienciaIndeminizacion( ventaSalvamentoActiva.getEficienciaIndeminizacion() );
				}
				indicadoresVentaDto.setTiempoAutorizacionPagoOrdenCompra( this.diferenciaDiasFechas(indemnizacion.getRecepcionDocumentosSiniestro().getFechaCreacion(), indemnizacion.getOrdenCompra().getFechaCreacion() ) );
				indicadoresVentaDto.setTiempoPagoDesdeDocumentacion( this.diferenciaDiasFechas(indemnizacion.getRecepcionDocumentosSiniestro().getFechaCreacion(), indemnizacion.getFechaAutIndemnizacion() ) );
				
				if( !lOrdenCompra.isEmpty() ){
					
					indicadoresVentaDto.setFechaRecepcionPapeleo       (lOrdenCompra.get(0).getFechaModificacion());
					
					// # SOLO PERDIDA TOTAL
					Map<String,String> mTipoSiniestro = this.perdidaTotalService.obtenerTiposSiniestroPerdidaTotal();
					tipoSiniestroPerdidaTotal = this.perdidaTotalService.obtenerTipoSiniestro(lOrdenCompra.get(0), mTipoSiniestro);
					
					if ( tipoSiniestroPerdidaTotal != null ){
						if( tipoSiniestroPerdidaTotal.equals("PT") ){
							indicadoresVentaDto.setFechaDeDeterminacion(indemnizacion.getFechaAutPerdidaTotal());
						}else{
							indicadoresVentaDto.setFechaDeDeterminacion(indemnizacion.getFechaCreacion());
						}
						
						indicadoresVentaDto.setTiempoDeDocumentacion	   ( this.diferenciaDiasFechas(indicadoresVentaDto.getFechaDeDeterminacion(),indemnizacion.getRecepcionDocumentosSiniestro().getFechaCreacion()) );
						indicadoresVentaDto.setTiempoPagoDesdeDeterminacion( this.diferenciaDiasFechas(indicadoresVentaDto.getFechaDeDeterminacion(), indemnizacion.getFechaAutIndemnizacion()) );
					}
				}
				
				
			}
			
			

		}
		
		 return indicadoresVentaDto;
	}
	
	
	private int diferenciaDiasFechas(Date fechaInicial, Date fechaFinal){
		LocalDate fechaInicialTruncate = new LocalDate(fechaInicial);
		LocalDate fechaFinalTruncate = new LocalDate(fechaFinal);
		
//		DateTime f1 = new DateTime(fechaInicial);
//		DateTime f2 = new DateTime(fechaFinal);
//		DateUtils.truncate(input, Calendar.MONTH)
		return Math.abs(Days.daysBetween(fechaInicialTruncate, fechaFinalTruncate).getDays());
		
	}
	
	
	public List<VentaSalvamento> obtenerVentasPorRecuperacion(Long recuperacionId){
		
		Map<String,Object> params = new HashMap<String,Object>();
		
		params.put("recuperacionSalvamento.id", recuperacionId);
		
		List<VentaSalvamento> lVentas =  this.entidadService.findByProperties(VentaSalvamento.class, params);
		
		Collections.sort(lVentas, new Comparator<VentaSalvamento>(){
			public int compare(VentaSalvamento vs1, VentaSalvamento vs2){
				return vs2.getId().compareTo(vs1.getId());
			}
		});
		
		return lVentas;
		
	}

	@Override
	public BigDecimal obtenerMontoRecuperacionSalvamento(Long siniestroId,
			EstatusRecuperacion estatus) {
		return this.recuperacionDao.obtenerMontoRecuperacionSalvamento(siniestroId, estatus);
	}

	@Override
	public BigDecimal obtenerIvaRecuperacionSalvamento(Long siniestroId,
			EstatusRecuperacion estatus) {
		return this.recuperacionDao.obtenerIvaRecuperacionSalvamento(siniestroId, estatus);
	}
	
	private String obtenerReferenciasBancarias(Long recuperacionId){
		
        //List<ReferenciaBancariaDTO> lReferenciasBancarias = this.obtenerReferenciasBancaria(recuperacion.getId());
		List<ReferenciaBancariaDTO> lReferenciasBancarias = this.obtenerReferenciasBancaria(recuperacionId);
        StringBuilder tablaReferencias = new StringBuilder();
        tablaReferencias.append("<table border='1' class='txt_mail' ><tr><th>Banco</th> <th>Cuenta</th> <th>Clave</th> <th>Referencia</th> </tr>");
        for( ReferenciaBancariaDTO lRef : lReferenciasBancarias ){
        	tablaReferencias.append("<tr>");
        	tablaReferencias.append("<td>");
        	tablaReferencias.append(lRef.getBancoMidas().getNombre());
        	tablaReferencias.append("</td>");
        	tablaReferencias.append("<td>");
        	tablaReferencias.append(lRef.getCuentaBancaria());
        	tablaReferencias.append("</td>");
        	tablaReferencias.append("<td>");
        	tablaReferencias.append(lRef.getClabe());
        	tablaReferencias.append("</td>");
        	tablaReferencias.append("<td>");
        	tablaReferencias.append(lRef.getNumeroReferencia());
        	tablaReferencias.append("</td>");
        	tablaReferencias.append("</tr>");
        }
        tablaReferencias.append("</table>");
        
        return tablaReferencias.toString();
		
	}
	
	
	public VentaSalvamento obtenerVentaSalvamentoActiva(Long recuperacionId){
		
		RecuperacionSalvamento recuperacion = this.obtenerRecuperacionSalvamentoById(recuperacionId); //this.entidadService.findById(RecuperacionSalvamento.class, recuperacionId);
		
		if( recuperacion != null ){
			
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("recuperacionSalvamento.id", recuperacion.getId() );
			params.put("estatus", EstatusSalvamento.ACT );
			List<VentaSalvamento> ventaSalvamento = this.entidadService.findByProperties(VentaSalvamento.class, params);
			
			if( !ventaSalvamento.isEmpty() ){
				return ventaSalvamento.get(0);
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	
	public RecuperacionSalvamento obtenerRecuperacionSalvamentoById(Long recuperacionId){
		
		RecuperacionSalvamento recuperacion = null;
		
		recuperacion = entidadService.findById(RecuperacionSalvamento.class, recuperacionId);
		
		return recuperacion;
	}
	
	private void notificarProrrogaAprobada(RecuperacionSalvamento recuperacion, VentaSalvamento ventaSalvamento){
		
		try{
			
			EmailDestinaratios destinatarios = new EmailDestinaratios();
			
			String nombreCompleto = this.quitarSimbolosDestinatario(ventaSalvamento);
			
	        destinatarios.agregar(EnumModoEnvio.PARA, ventaSalvamento.getCorreo() , nombreCompleto ); 
			
			HashMap<String, Serializable> notificacion = new HashMap<String, Serializable>();
		
			AutoIncisoReporteCabina autoInciso = this.obtenerDatosAutoByRecuperacionSalvamentoAseguradoYTercero((Recuperacion)recuperacion);
			
			
			notificacion.put("comprador"              , recuperacion.obtenerVentaActiva().getComprador().getNombrePersona());
			notificacion.put("fechaLimitePago"        , this.generaFechaAmigable(recuperacion.getFechaLimiteProrroga() ));
			notificacion.put("totalVenta"             , recuperacion.obtenerVentaActiva().getTotalVenta() );
			notificacion.put("marca"      , autoInciso.getDescMarca());
			notificacion.put("tipo"       , autoInciso.getDescEstilo());
			notificacion.put("modelo"     , autoInciso.getModeloVehiculo());
			notificacion.put("serie"      , autoInciso.getNumeroSerie() );
			notificacion.put("motor"      , autoInciso.getNumeroMotor() );
	        notificacion.put("color"      , autoInciso.getDescColor() );
	        notificacion.put("referencias"            , this.obtenerReferenciasBancarias(recuperacion.getId()) );
			
			this.notificacionService.enviarNotificacion(
						EnvioNotificacionesService.EnumCodigo.SVM_NOT_COMPRADOR_PRORROGA.toString(), 
						notificacion,
						ventaSalvamento.getNoSubasta() , 
						destinatarios,
						this.generarPDFInstrucctivoPago(recuperacion.getId()) );
		
		}catch(Exception e){
			this.log.error("Error RecuperacionSalvamentoService notificarProrrogaAprobada"+e);
		}
	}
	
	private String generaFechaAmigable(Date fecha){
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy",new Locale("es","MX"));
		
		return sdf.format(fecha);
	}
	
	
	@Override
	public void guardaSalvamentoConProvision(Long recuperacionId, RecuperacionSalvamento recuperacionDTO){
		RecuperacionSalvamento recuperacionBase = this.obtenerRecuperacionPorId(recuperacionId);
		Boolean seValidaProvision = recuperacionDTO.getAbandonoIncosteable()!=recuperacionBase.getAbandonoIncosteable()?Boolean.TRUE:Boolean.FALSE;
		RecuperacionSalvamentoService processor = this.sessionContext.getBusinessObject(RecuperacionSalvamentoService.class);        
		processor.salvarSalvamento(recuperacionBase, recuperacionDTO);
		if(seValidaProvision){
			processor.invocaProvision(recuperacionBase.getId());
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public RecuperacionSalvamento salvarSalvamento(RecuperacionSalvamento recuperacionBase, RecuperacionSalvamento recuperacionDTO){
		
		recuperacionBase.setMotivoAbandonoIncosteable(recuperacionDTO.getMotivoAbandonoIncosteable());
		if( recuperacionDTO.getAbandonoIncosteable() ){
			recuperacionBase.setEstatus(Recuperacion.EstatusRecuperacion.INACTIVO.name());
		}else{
			recuperacionBase.setEstatus(Recuperacion.EstatusRecuperacion.REGISTRADO.name());
		}
		recuperacionBase.setAbandonoIncosteable      (recuperacionDTO.getAbandonoIncosteable());
		recuperacionBase.setEstacionamientoTaller    (recuperacionDTO.getEstacionamientoTaller());
		recuperacionBase.setContactoTallerUbicacion  (recuperacionDTO.getContactoTallerUbicacion() );
		recuperacionBase.setFechaInicioSubasta       (recuperacionDTO.getFechaInicioSubasta());
		recuperacionBase.getCiudad().setId           (recuperacionDTO.getCiudad().getId() );
		recuperacionBase.setDireccionSalvamento		 (recuperacionDTO.getDireccionSalvamento());	
		recuperacionBase.setMotivoCancelacion(recuperacionDTO.getMotivoCancelacion());
		this.guardarRecuperacionSalvamento(recuperacionBase);
		return recuperacionBase;
	}
	
	
	
	private void notificacionOrdenSalida(RecuperacionSalvamento recuperacion, VentaSalvamento ventaSalvamento){
		
		EmailDestinaratios destinatarios = new EmailDestinaratios();
		
		String nombreCompleto = this.quitarSimbolosDestinatario(ventaSalvamento);
		
        destinatarios.agregar(EnumModoEnvio.PARA, ventaSalvamento.getCorreo() , nombreCompleto ); 
		
		HashMap<String, Serializable> notificacion = new HashMap<String, Serializable>();
	
		AutoIncisoReporteCabina autoInciso = this.obtenerDatosAutoByRecuperacionSalvamentoAseguradoYTercero((Recuperacion)recuperacion);
		String[] aFrendlyFecha = this.generaFechaAmigable(new Date()).split(" ");
		
		notificacion.put("numeroSiniestro"        , recuperacion.getReporteCabina().getSiniestroCabina().getNumeroSiniestro() );
		notificacion.put("marca"      , autoInciso.getDescMarca());
		notificacion.put("tipo"       , autoInciso.getDescEstilo());
		notificacion.put("modelo"     , autoInciso.getModeloVehiculo());
		notificacion.put("serie"      , autoInciso.getNumeroSerie() );
		notificacion.put("color"      , autoInciso.getDescColor() );
		notificacion.put("comprador"  , recuperacion.obtenerVentaActiva().getComprador().getNombrePersona());
		notificacion.put("taller"      , recuperacion.getEstacionamientoTaller() );
		notificacion.put("direccionUbicacion"      , recuperacion.getDireccionSalvamento() );
		notificacion.put("ciudadUbicacion"      , recuperacion.getCiudad().getDescripcion() );
		notificacion.put("estadoUbicacion"      , recuperacion.getEdoUbicacion().getDescripcion() );
		notificacion.put("ciudadOficina"        , recuperacion.getCiudad().getDescripcion() );
		notificacion.put("estadoOficina"        , recuperacion.getEdoUbicacion().getDescripcion() );
		notificacion.put("diaFechaActual"       ,  aFrendlyFecha[0] );
		notificacion.put("mesFechaActual"       ,  aFrendlyFecha[1] );
		notificacion.put("anioFechaActual"      ,  aFrendlyFecha[2] );
		
		
		this.notificacionService.enviarNotificacion(
					EnvioNotificacionesService.EnumCodigo.SVM_ORDEN_SALIDA.toString(), 
					notificacion,
					ventaSalvamento.getNoSubasta() , 
					destinatarios);
	}
	
	
	private void notificacionConformidad(RecuperacionSalvamento recuperacion, VentaSalvamento ventaSalvamento){
		
		EmailDestinaratios destinatarios = new EmailDestinaratios();
		
		String nombreCompleto = this.quitarSimbolosDestinatario(ventaSalvamento);
        
        destinatarios.agregar(EnumModoEnvio.PARA, ventaSalvamento.getCorreo() , nombreCompleto ); 
		
		HashMap<String, Serializable> notificacion = new HashMap<String, Serializable>();
	
		AutoIncisoReporteCabina autoInciso = this.obtenerDatosAutoByRecuperacionSalvamentoAseguradoYTercero((Recuperacion)recuperacion);
		String[] aFrendlyFecha = this.generaFechaAmigable(new Date()).split(" ");
		
		notificacion.put("ciudadOficina"        , recuperacion.getCiudad().getDescripcion() );
		notificacion.put("estadoOficina"        , recuperacion.getEdoUbicacion().getDescripcion() );
		notificacion.put("diaFechaActual"       ,  aFrendlyFecha[0] );
		notificacion.put("mesFechaActual"       ,  aFrendlyFecha[1] );
		notificacion.put("anioFechaActual"      ,  aFrendlyFecha[2] );
		notificacion.put("marca"      , autoInciso.getDescMarca());
		notificacion.put("tipo"       , autoInciso.getDescEstilo());
		notificacion.put("modelo"     , autoInciso.getModeloVehiculo());
		notificacion.put("serie"      , autoInciso.getNumeroSerie() );
		notificacion.put("color"      , autoInciso.getDescColor() );
		
		this.notificacionService.enviarNotificacion(
					EnvioNotificacionesService.EnumCodigo.SVM_RECIBO_CONFORMIDAD.toString(), 
					notificacion,
					ventaSalvamento.getNoSubasta() , 
					destinatarios);
	}
	
	@Override
	public void notificaVentasPorConfirmarIngresoProrroga(){
		System.out.println(" ++++++++++++++++++++++ JOB CANCELAR ENVIO RECURRENTE SALVAMENTO ++++++++++++++++++++++ ");
		
		bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.NOTIFICACION_SALVAMENTO_RECURRENTE, "0",
				"SE INVOCA JOB DE NOTIFICACIÓN "+new Date().toString(), "", "M2ADMINI" );		
		
		// FOLIO RECUPERACION recuperacion.id-recuperacion.numero-SPV
		EnvioNotificacionesService.EnumCodigo notificacionAEnviar = null;
		String folio = "",referenciasBancarias;
		int tipoDeNotificacion = 0, difHoras = 0;
		DateTime hoy = new DateTime(new Date());
		DateTime fechaDifHoras = null;
		AutoIncisoReporteCabina autoInciso = null;
		HashMap<String, Serializable> notificacion = new HashMap<String, Serializable>();
		List<BitacoraNotificacion> lBitacora = null;
		
		// OBTENER DATOS A PROCESAR
		List<RecuperacionSalvamento> lRecuperaciones = this.recuperacionDao.obtenerVentasPorConfirmarIngresoProrroga();
		
		for( RecuperacionSalvamento salvamento : CollectionUtils.emptyIfNull(lRecuperaciones) ){
			
			
			// A-¿NO TIENE FECHA DE CONFIRMACION INGRESO Y TIENE CONFIRMACION DE PRORROGA? 24 HRS
			// B-¿NO TIENE FECHA DE CONFIRMACION INGRESO Y NO TIENE CONFIRMACION DE PRORROGA? 48 HRS
			if( salvamento.getFechaDepositoConfirmacionIngreso() == null ){
				if( salvamento.obtenerVentaActiva() != null && salvamento.isAutorizacionProrroga() ){ // A
					folio = salvamento.getId()+"-"+salvamento.getNumero()+"-PRORROGA-SPV";
					tipoDeNotificacion = 1;
					difHoras = 24;
					notificacionAEnviar = EnvioNotificacionesService.EnumCodigo.SVM_RECORDATORIO_PAGO_DIARIO_POST_PRORROGA;
				}else if( salvamento.obtenerVentaActiva() != null && !salvamento.isAutorizacionProrroga() ){ // B
					folio = salvamento.getId()+"-"+salvamento.getNumero()+"-VENTA-SPV";
					tipoDeNotificacion = 2;
					difHoras = 48;
					notificacionAEnviar = EnvioNotificacionesService.EnumCodigo.SVM_RECORDATORIO_COMPRADOR;
				}
				
				bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.NOTIFICACION_SALVAMENTO_RECURRENTE, folio,
						"ITERACION DE RESULTADOS | TIPO NOTIFICACION: "+tipoDeNotificacion+" | DIF HORAS: "+difHoras,notificacionAEnviar, "M2ADMINI" );
			}
			
			lBitacora = entidadService.findByProperty(BitacoraNotificacion.class, "folio", folio);
			
			if( lBitacora.isEmpty() ){
				
				bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.NOTIFICACION_SALVAMENTO_RECURRENTE, folio,
						"NO HAY RESULTADOS EN LA BITACORA ","", "M2ADMINI" );
				
				// LLENAR FECHA A SACAR DIFERENCIA
				fechaDifHoras = new DateTime( ( tipoDeNotificacion == 2 ) ? salvamento.getFechaAutorizacionProrroga() : salvamento.obtenerVentaActiva().getFechaCreacion() );
				
				bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.NOTIFICACION_SALVAMENTO_RECURRENTE, folio,
						"DIFERENCIA DE HORAS: "+ Hours.hoursBetween( fechaDifHoras,hoy ).getHours() +">="+difHoras  ,"", "M2ADMINI" );
				
				System.out.println(" ++++++++++++++++++++++ JOB CANCELAR ENVIO RECURRENTE SALVAMENTO ++++++++++++++++++++++ DIFERENCIA HORAS: "+Hours.hoursBetween( fechaDifHoras,hoy ).getHours());
				// CALCULAR DIFERENCIA DE FECHAS
				if( Hours.hoursBetween(fechaDifHoras, hoy ).getHours() >= difHoras  ){
					
					bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.NOTIFICACION_SALVAMENTO_RECURRENTE, folio,
							"DIFERENCIA DE HORAS VALIDA ","", "M2ADMINI" );
	
					// OBTIENE DATOS DE VEHICULO
					autoInciso = this.obtenerDatosAutoByRecuperacionSalvamentoAseguradoYTercero(salvamento);
	
					// OBTIENE REFERENCIAS BANCARIAS
					referenciasBancarias =  this.obtenerReferenciasBancarias(salvamento.getId());
	
					// PROCESA DATOS POR TIPO DE NOTIFICACION - DATOS COMUNES EN LA NOTIFICACION
					notificacion.put("comprador"   , salvamento.obtenerVentaActiva().getComprador().getNombrePersona());
					notificacion.put("totalVenta"  , salvamento.obtenerVentaActiva().getTotalVenta() );
					notificacion.put("marca"       , autoInciso.getDescMarca());
					notificacion.put("tipo"        , autoInciso.getDescEstilo());
					notificacion.put("modelo"      , autoInciso.getModeloVehiculo());
					notificacion.put("serie"       , autoInciso.getNumeroSerie() );
					notificacion.put("motor"       , autoInciso.getNumeroMotor() );
					notificacion.put("color"       , autoInciso.getDescColor() );
					notificacion.put("referencias" , referenciasBancarias);
					
					bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.NOTIFICACION_SALVAMENTO_RECURRENTE, folio,
							"DIFERENCIA DE HORAS VALIDA - MAPA A ENVIAR ",notificacion.toString(), "M2ADMINI" );
	
	
					if( tipoDeNotificacion == 1 ){
						notificacion.put("numSubasta" , salvamento.obtenerVentaActiva().getNoSubasta() );
						notificacion.put("fechaLimitePago" , this.generaFechaAmigable(salvamento.getFechaLimiteProrroga() ));
					}else{
						notificacion.put("numSubasta" , salvamento.obtenerVentaActiva().getNoSubasta() );
					}
	
					this.bitacoraNotificacionService.salvarBitacora(
									folio, 
									notificacionAEnviar, 
									salvamento.obtenerVentaActiva().getCorreo(), 
									notificacion);
					
					this.bitacoraNotificacionService.cambiarReenvio(folio, true);
	
					System.out.println(" ++++++++++++++++++++++ JOB CANCELAR ENVIO RECURRENTE SALVAMENTO ++++++++++++++++++++++ MANDA CORREO: "+Hours.hoursBetween( fechaDifHoras,hoy ).getHours());
					
					// SE ACTIVA PARA PRUEBAS EN CAP
					/*EmailDestinaratios destinatarios = new EmailDestinaratios();
				        destinatarios.agregar(EnumModoEnvio.PARA, "0julio.vasquez@afirme.com" , salvamento.obtenerVentaActiva().getComprador().getNombrePersona() );
	
				        this.notificacionService.enviarNotificacion(
				        		notificacionAEnviar.toString(), 
								notificacion,
								salvamento.obtenerVentaActiva().getNoSubasta() , 
								destinatarios);*/
				        
				    bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.NOTIFICACION_SALVAMENTO_RECURRENTE, folio,
								"FIN ","", "M2ADMINI" );
					
				}// CIERRA if( Hours.hoursBetween(fechaDifHoras, hoy )
				
			}// CIERRA if( lBitacora.isEmpty()
			
		}// CIERRA for( RecuperacionSalvamento salvamento
		
	}

	private void cancelaEnvioRecurrenteSalvamento(RecuperacionSalvamento salvamento){
		String folio = "";
		try{
			
			if( salvamento.obtenerVentaActiva() != null && salvamento.isAutorizacionProrroga() ){ // A
				folio = salvamento.getId()+"-"+salvamento.getNumero()+"-PRORROGA-SPV";
			}else if( salvamento.obtenerVentaActiva() != null && !salvamento.isAutorizacionProrroga() ){ // B
				folio = salvamento.getId()+"-"+salvamento.getNumero()+"-VENTA-SPV";
			}
			
			List<BitacoraNotificacion> lBitacora = entidadService.findByProperty(BitacoraNotificacion.class, "folio", folio);
			
			if( lBitacora != null && !lBitacora.isEmpty() ){
				// APAGA EL REENVIO
				this.bitacoraNotificacionService.cambiarReenvio(folio, false);
				
				// SI SE APAGA LA NOTIFICACION DE PRORROGA TAMBIÉN SE DEBERA DESACTIVAR LA DE VENTA YA QUE ES LA PRIMERA QUE SE INSERTA
				if( folio.contains("PRORROGA") ){
					folio = salvamento.getId()+"-"+salvamento.getNumero()+"-VENTA-SPV";
					this.bitacoraNotificacionService.cambiarReenvio(folio, false);
				}
			}
		
		}catch(Exception e){
			log.error("Error al cancelar envio recurrente Salvamento ID: "+salvamento.getId());
		}
		
	}

	@Override
	public void actualizarInformacionRecuperacionSalvamentoEnIndemnizacion(Long idIndemnizacion){
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		RecepcionDocumentosSiniestro recepcion = indemnizacion.getRecepcionDocumentosSiniestro();
		RecuperacionSalvamento recuperacion = this.obtenerRecuperacionSalvamento(indemnizacion.getOrdenCompra().getIdTercero());
		
		if(recuperacion != null){
			if(recepcion != null){
//				SI ES PERSONA MORAL, SE CALCULA EL IVA DEL VALOR ESTIMADO DEL VALUADOR EN LA RECUPERACION DE SALVAMENTO
				if(recepcion.getTipoPersona() != null && EnumUtil.equalsValue(TipoPersona.PERSONA_MORAL, recepcion.getTipoPersona())){
					String ivaSalvamentoString = parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS, ParametroGlobalService.IVA_VENTA_SALVAMENTO);
					BigDecimal porcentajeIvaSalvamento = new BigDecimal(ivaSalvamentoString);
					recuperacion.setPorcentajeValorEstimado(porcentajeIvaSalvamento);
					BigDecimal divisor = porcentajeIvaSalvamento.add(new BigDecimal(100));
					recuperacion.setSubtotalValorEstimado(recuperacion.getValorEstimado().multiply(new BigDecimal(100)).divide(divisor, 2, RoundingMode.HALF_UP));
					recuperacion.setIvaValorEstimado(recuperacion.getValorEstimado().subtract(recuperacion.getSubtotalValorEstimado()));
				}else{
					recuperacion.setPorcentajeValorEstimado(BigDecimal.ZERO);
					recuperacion.setIvaValorEstimado(BigDecimal.ZERO);
					recuperacion.setSubtotalValorEstimado(recuperacion.getValorEstimado());
				}
			}else{
				recuperacion.setPorcentajeValorEstimado(BigDecimal.ZERO);
				recuperacion.setIvaValorEstimado(BigDecimal.ZERO);
				recuperacion.setSubtotalValorEstimado(recuperacion.getValorEstimado());
			}
			entidadService.save(recuperacion);
		}
	}
	
	public void initialize() {
		String timerInfo = "TimerRecuperacionSalvamento";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.minute(0);
				expression.hour(3);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerRecuperacionSalvamento", false));
				
				log.info("Tarea TimerRecuperacionSalvamento configurado");
			} catch (Exception e) {
				log.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		log.info("Cancelar Tarea TimerRecuperacionSalvamento");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			log.error("Error al detener TimerRecuperacionSalvamento:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		notificaVentasPorConfirmarIngresoProrroga();
	}
}
