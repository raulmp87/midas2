package mx.com.afirme.midas2.service.impl.cfdi;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.ws.cliente.PrintReportClient;
import mx.com.afirme.midas2.service.cfdi.CancelarCFDIAcuseCancService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;


@Stateless
public class CancelarCFDIAcuseCancServiceImpl implements CancelarCFDIAcuseCancService {		
	
	private PrintReportClient printReportClient;
	
	public static final Logger LOG = Logger.getLogger(CancelarCFDIAcuseCancServiceImpl.class);
	
	@EJB
	public void setPrintReportClient(PrintReportClient printReportClient) {
		this.printReportClient = printReportClient;
	}
	
	@Resource	
	private TimerService timerService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	@Override
	public void cancelarCFDI() {
		try {
			LOG.info("entrando a CancelarCFDIAcuseCancServiceImpl.cancelarCFDI... ");
	        
	        // Proceso de cancelación de CFDIs con acuse
			printReportClient.cancelarCFDI();   
	        LOG.info("tarea cancelarCFDI ejecutada");
			
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("error general en CancelarCFDIAcuseCancServiceImpl.cancelarCFDI... ", e);
		}
	}
	
	public void initialize() {
		String timerInfo = "TimerCancelarCFDI";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.minute("*/30");
				expression.hour("*");
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerCancelarCFDI", false));
				
				LOG.info("Tarea TimerCancelarCFDI configurado por LGEC");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerCancelarCFDI");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerCancelarCFDI:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		cancelarCFDI();
	}
}

