package mx.com.afirme.midas2.service.impl.bitemporal.suscripcion;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoInterfazServiciosRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cobranza.CoberturaCobranzaDTO;
import mx.com.afirme.midas.interfaz.cobranza.CobranzaService;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoFacadeRemote;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.bitemporal.BitemporalDao;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.endoso.cotizacion.auto.CotizacionEndosoDao;
import mx.com.afirme.midas2.dao.suscripcion.endoso.MovimientoEndosoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.Cotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.comision.BitemporalComision;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspInc;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.CondicionEspCotContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.CondicionEspIncContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.BitemporalTexAdicionalCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.TexAdicionalCotContinuity;
import mx.com.afirme.midas2.domain.catalogos.DomicilioImpresion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.migracion.MigEndososValidos;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.bonocomision.NegocioBonoComision;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.MovimientoEndoso;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.MovimientoEndoso.Concepto;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente.TipoEmision;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.suscripcion.endoso.movimiento.AuxiliarCalculoDTO;
import mx.com.afirme.midas2.dto.suscripcion.endoso.movimiento.ContenedorPrimasMovimientoDTO;
import mx.com.afirme.midas2.dto.suscripcion.endoso.movimiento.FuenteMovimientoDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.MovimientoEndosoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.TipoMovimientoEndoso;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.BeanUtils;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.Continuity;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.RecordStatus;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class MovimientoEndosoBitemporalServiceNewImpl implements MovimientoEndosoBitemporalService {
	private boolean esMergeMovimientoAjuste = false;
	
	@Override
	public void borrarMovimientos(ControlEndosoCot controlEndosoCot) {
		movimientoEndosoDao.remove(controlEndosoCot.getId());
	}

	@Override
	public void calcular(BitemporalCotizacion cotizacion) {
		
		Map<String, Object> values = new LinkedHashMap<String, Object>();
		values.put("cotizacionId", cotizacion.getContinuity().getNumero());
		values.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		List<ControlEndosoCot> controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class,values);
		if(!controlEndosoCotList.isEmpty()&&controlEndosoCotList.get(0)!=null){
			ControlEndosoCot controlEndosoCot = controlEndosoCotList.get(0);
			this.calcular(controlEndosoCot);
		}	
		
	}
	
	@Override
	public void calcular(ControlEndosoCot controlEndosoCot) {
		this.calcular(controlEndosoCot,false);
	}

	@Override
	public void calcular(ControlEndosoCot controlEndosoCot, Boolean esMigrada) {
		
		List<MovimientoEndoso> movimientosEndoso = new ArrayListNullAware<MovimientoEndoso>();
		List<MovimientoEndoso> movimientosEndososEAPAnteriores = new ArrayListNullAware<MovimientoEndoso>();
		List<MovimientoEndoso> movimientosEndososAFuturo = new ArrayListNullAware<MovimientoEndoso>();
		
		controlEndosoCot.setMigrada(esMigrada);
		
		//se borran los movimientos para que estos sean generados de nuevo.
		this.borrarMovimientos(controlEndosoCot);
		
		movimientosEndoso = generarMovimientosEndoso(controlEndosoCot, null);
		
		short tipoEndoso = controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue();
		
		if ((tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA
				|| tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO)
			&& !esMigrada) {
			

				movimientosEndososEAPAnteriores = getMovimientosEndososEAPAnteriores(movimientosEndoso, controlEndosoCot, tipoEndoso);
				movimientosEndososAFuturo = getMovimientosEndososAFuturo(movimientosEndoso, controlEndosoCot, tipoEndoso);
				mergeMovimientosEndoso(controlEndosoCot, movimientosEndoso, movimientosEndososEAPAnteriores, null);
				mergeMovimientosEndoso(controlEndosoCot, movimientosEndoso, movimientosEndososAFuturo, null);
				
				if(controlEndosoCot.getPrimaTotalIgualar() == null){
					for(MovimientoEndoso movimiento : movimientosEndoso){
						if(movimiento.getTipo().equals(TipoMovimientoEndoso.POLIZA)){
							for(MovimientoEndoso movimientoInc : movimiento.getMovimientosEndoso()){
								if(movimientoInc.getTipo().equals(TipoMovimientoEndoso.INCISO)){
									calculaFactoresCobranzaInciso(movimientoInc);
								}							
							}						
						}
						if(movimiento.getTipo().equals(TipoMovimientoEndoso.INCISO)){
							calculaFactoresCobranzaInciso(movimiento);
						}
					}
				}
			}			
				
		for (MovimientoEndoso movimiento : movimientosEndoso) {
			entidadService.save(movimiento);
		}
		
	}

	@Override
	public NegocioDerechoEndoso getNegocioDerechoEndoso(BitemporalCotizacion cotizacion) {
		
		NegocioDerechoEndoso derechosEndoso = null;
		
		if (cotizacion.getValue().getNegocioDerechoEndosoId() != null) {
			derechosEndoso = entidadService.findById(NegocioDerechoEndoso.class, cotizacion.getValue().getNegocioDerechoEndosoId());
		} else {
			
			Boolean esAltaInciso = (cotizacion.getValue().getSolicitud().getClaveTipoEndoso() != null
					? (cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)
							:false);
			
			Map<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("negocio.idToNegocio", cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio());
			params.put("esAltaInciso", esAltaInciso);
			params.put("claveDefault", true);
			params.put("activo", true);
			
			List<NegocioDerechoEndoso> negocioDerechoEndosoTemp = entidadService.findByProperties(NegocioDerechoEndoso.class, params);
			
			if(negocioDerechoEndosoTemp != null && negocioDerechoEndosoTemp.size() > 0){
				derechosEndoso = negocioDerechoEndosoTemp.get(0);
			}
		}
		
		return derechosEndoso;
	}

	@Override
	public Double getPctFormaPago(Integer formaPagoId, Short monedaId) {
		FormaPagoIDTO formaPagoIDTO = new FormaPagoIDTO();
		formaPagoIDTO.setIdFormaPago(formaPagoId);
		formaPagoIDTO.setIdMoneda(monedaId);
		try {
			List<FormaPagoIDTO> formaPagoList = formaPagoFacadeRemote
					.findByProperty(formaPagoIDTO, "", "A");
			if (formaPagoList != null)
				return formaPagoList.get(0)
						.getPorcentajeRecargoPagoFraccionado().doubleValue();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return null;
	}
	
	/**
	 * Obtiene el ultimo bitemporal conocido que no sea nulo para una fecha dada.
	 */
	public <T extends Bitemporal> T getLatestBitemporal(Continuity<?, T> continuity, Class<T> bitemporalClass, DateTime validOn) {
		T t = continuity.getBitemporalProperty().get(validOn);
		T t2 = continuity.getBitemporalProperty().getInProcess(validOn);
		if (t2 != null) {
			return t2;
		}
		return t;
	}

	/**
	 * Metodos privados
	 */
	
	/**
	 * XXX Generacion de movimientos
	 */
	 
	private void mergeMovimientosEndoso (ControlEndosoCot controlEndosoCot, List<MovimientoEndoso> movimientosOriginales, List<MovimientoEndoso> movimientosAgregados,
			MovimientoEndoso movimientoPadre) {
		
		if (movimientosOriginales != null && movimientosAgregados != null
				&& movimientosOriginales.size() > 0 && movimientosAgregados.size() > 0) {
			
			int comparacion = compareTipoMovimiento(movimientosOriginales.get(0).getTipo(), movimientosAgregados.get(0).getTipo());
			
			if (comparacion > 0) {
				for (MovimientoEndoso movimientoOriginal : movimientosOriginales) {
					mergeMovimientosEndoso (controlEndosoCot, movimientoOriginal.getMovimientosEndoso(), movimientosAgregados, movimientoOriginal);
				}
			} else if (comparacion < 0) {
				for (MovimientoEndoso movimientoAgregado : movimientosAgregados) {
					mergeMovimientosEndoso (controlEndosoCot, movimientosOriginales, movimientoAgregado.getMovimientosEndoso(), movimientoPadre);
				}
			} else {
				//Ambos son del mismo tipo
				boolean found = false;
				MovimientoEndoso movimiento = null; 
				boolean mergeHijos = true;
				
				 for (MovimientoEndoso movimientoAgregado : movimientosAgregados) {
					 found = false;
					 
					 if (movimientoAgregado.getTipo().equals(TipoMovimientoEndoso.INCISO) 
							 || (movimientoAgregado.getTipo().equals(TipoMovimientoEndoso.COBERTURA) 
									 && isSameContinuity(movimientoPadre, movimientoAgregado))) {
						 
						 for (MovimientoEndoso movimientoOriginal : movimientosOriginales) {
							 
							if (isSameContinuity(movimientoOriginal, movimientoAgregado)) {
								found = true;
								//Se suman todos los conceptos del movimiento agregado dentro de los valores del movimiento original 
								mergeMovimiento(movimientoOriginal, movimientoAgregado);
								break;
							}
						 }
						 
						 if (!found) {
							 movimiento = new MovimientoEndoso(movimientoAgregado);
							 movimiento.setControlEndosoCot(controlEndosoCot);
							 movimiento.setMovimientoEndoso(movimientoPadre);
							 if(movimientoAgregado.getTipo().equals(TipoMovimientoEndoso.INCISO)){
								 mergeHijos = false;
								 for(MovimientoEndoso movimientoCobertura : movimientoAgregado.getMovimientosEndoso()){
									 MovimientoEndoso movimientoCobMerge = new MovimientoEndoso(movimientoCobertura);
									 movimientoCobMerge.setControlEndosoCot(controlEndosoCot);
									 movimientoCobMerge.setMovimientoEndoso(movimiento);
									 movimiento.getMovimientosEndoso().add(movimientoCobMerge);
								 }								 
								 
							 }
							 movimientosOriginales.add(movimiento);
						 }
						 
					 }
				}
				 
				//Hace merge a los hijos
				if (!movimientosOriginales.get(0).getTipo().equals(TipoMovimientoEndoso.COBERTURA) && mergeHijos) {
					for (MovimientoEndoso movimientoOriginal : movimientosOriginales) {
						mergeMovimientosEndoso (controlEndosoCot, movimientoOriginal.getMovimientosEndoso(), movimientosAgregados, movimientoOriginal);
					}
				}
				 
			}
			
		}
		
	}
	
	
	private List<MovimientoEndoso> generarMovimientosEndoso(ControlEndosoCot controlEndosoCot, List<IncisoContinuity> incisosBaja) {
		
		List<MovimientoEndoso> movimientosEndoso = new ArrayListNullAware<MovimientoEndoso>();
		List<FuenteMovimientoDTO> fuentes = new ArrayListNullAware<FuenteMovimientoDTO>();
		
		//Se obtiene la fecha de Inicio de Vigencia del endoso
		DateTime fechaInicioVigencia = TimeUtils.getDateTime(controlEndosoCot.getValidFrom());
		//Se obtiene el tipo de Endoso
		short tipoEndoso = controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue();
		
		//COTIZACION
		CotizacionContinuity cotizacionContinuity = entidadContinuityService.findContinuityByBusinessKey(CotizacionContinuity.class, 
				CotizacionContinuity.BUSINESS_KEY_NAME, controlEndosoCot.getCotizacionId());
		
		
		//Se cargan algunos datos auxiliares para el posterior calculo de los movimientos
		AuxiliarCalculoDTO auxiliar = cargaAuxiliar(cotizacionContinuity, controlEndosoCot, tipoEndoso, fechaInicioVigencia);
		
		//Se valida el tipo de endoso para saber si se obtendran bitemporales o movimientos para generar los nuevos movimientos
		fuentesMovimiento = cargaFuentesMovimiento(cotizacionContinuity, controlEndosoCot, fechaInicioVigencia, tipoEndoso, incisosBaja, auxiliar);
		
		//Se cargan coberturas por parte de Cobranza (en caso de se necesario, para CFP)
		cargaCoberturasCobranza(controlEndosoCot, tipoEndoso);
		
		//COTIZACION
		fuentes = filtraFuentesMovimiento(TipoMovimientoEndoso.POLIZA);
		
		for (FuenteMovimientoDTO fuente : fuentes) {
			movimientosEndoso.add(agregarMovimiento (controlEndosoCot, fechaInicioVigencia, 
					tipoEndoso, null, fuente));
		}
		
		if (movimientosEndoso.size() == 0) {
			//INCISOS y AUTOINCISOS
			fuentes = filtraFuentesMovimiento(TipoMovimientoEndoso.INCISO);
			
			for (FuenteMovimientoDTO fuente : fuentes) {
				movimientosEndoso.add(agregarMovimiento (controlEndosoCot, fechaInicioVigencia, 
						tipoEndoso, null, fuente));
			}
			
			if (movimientosEndoso.size() == 0) {
				//COBERTURAS
				fuentes = filtraFuentesMovimiento(TipoMovimientoEndoso.COBERTURA);
				
				for (FuenteMovimientoDTO fuente : fuentes) {
					movimientosEndoso.add(agregarMovimiento (controlEndosoCot, fechaInicioVigencia, 
							tipoEndoso, null, fuente));
				}
				
			}
		}
		
		//Calculo de prima neta en movimientos para validar la aplicacion de derechos
		Boolean valorDerechosCero = false;
		if(tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS){
			valorDerechosCero = calculoEndosoDisminucion(movimientosEndoso, auxiliar);
			if(valorDerechosCero){
				updateDerechosCeroCotizacion(cotizacionContinuity, fechaInicioVigencia);
			}
		}
		
		//Se calculan los movimientos (Tienen que calcularse hasta que ya estan generados todos los movimientos)
		for (MovimientoEndoso movimiento : movimientosEndoso) {
			calculaMovimiento(movimiento, auxiliar, valorDerechosCero);
		}
		
		return movimientosEndoso;
				
	}
	
	private void updateDerechosCeroCotizacion(CotizacionContinuity cotizacionContinuity, DateTime validFrom){
		BitemporalCotizacion cotizacion = null;
		Long idnegDerecho = null;
		cotizacion = cotizacionContinuity.getCotizaciones().getInProcess(validFrom);
		
		if (cotizacion == null) {
			cotizacion = cotizacionContinuity.getCotizaciones().get(validFrom);
		}
		
		if(cotizacion != null){
			NegocioDerechoEndoso derechosEndoso = null;
			
			if (cotizacion.getValue().getNegocioDerechoEndosoId() != null) {
				derechosEndoso = entidadService.findById(NegocioDerechoEndoso.class, cotizacion.getValue().getNegocioDerechoEndosoId());
			} 
			if(derechosEndoso == null || derechosEndoso.getImporteDerecho().doubleValue() > 0){
				//Busca derechos cero en negocio
				Map<String, Object> params = new LinkedHashMap<String, Object>();
				params.put("negocio.idToNegocio", cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio());
				params.put("esAltaInciso", false);
				params.put("importeDerecho", 0.0);
				//params.put("claveDefault", true);
				//params.put("activo", true);
				
				List<NegocioDerechoEndoso> negocioDerechoEndosoTemp = entidadService.findByProperties(NegocioDerechoEndoso.class, params);				
				if(negocioDerechoEndosoTemp != null && negocioDerechoEndosoTemp.size() > 0){
					derechosEndoso = negocioDerechoEndosoTemp.get(0);
					idnegDerecho = derechosEndoso.getIdToNegDerechoEndoso();
				}else{
					//Crea derecho cero en caso de que no exista
					derechosEndoso = new NegocioDerechoEndoso();
					derechosEndoso.setActivo(false);
					derechosEndoso.setClaveDefault(false);
					derechosEndoso.setEsAltaInciso(false);
					derechosEndoso.setImporteDerecho(0.0);
					
					Negocio negocio = entidadService.findById(Negocio.class, cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio());
					derechosEndoso.setNegocio(negocio);
					
					idnegDerecho = (Long) entidadService.saveAndGetId(derechosEndoso);
				}
				if(idnegDerecho != null){
					cotizacion.getValue().setNegocioDerechoEndosoId(idnegDerecho);
					entidadBitemporalService.saveInProcess(cotizacion, validFrom);
				}
			}
		}
	}
	
	private boolean calculoEndosoDisminucion(List<MovimientoEndoso> movimientosEndoso, AuxiliarCalculoDTO auxiliar){
		
		boolean esDisminucion = false;
		double primaNeta = BigDecimal.ZERO.doubleValue();
		
		for (MovimientoEndoso movimiento : movimientosEndoso) {
			//Copia movimientos para no afectar movimientos de calculo
			MovimientoEndoso movimientoPN = new MovimientoEndoso();
			BeanUtils.copyProperties(movimiento, movimientoPN);
			List<MovimientoEndoso> listPN = new ArrayList<MovimientoEndoso>(1);
			for(MovimientoEndoso item : movimientoPN.getMovimientosEndoso()){
				MovimientoEndoso movimientoPN1 = new MovimientoEndoso();
				BeanUtils.copyProperties(item, movimientoPN1);
				movimientoPN1.setMovimientoEndoso(movimientoPN);
				listPN.add(movimientoPN1);
			}
			movimientoPN.setMovimientosEndoso(listPN);
			//Calcula solo la prima neta
			calculaMovimientoPrimaNeta(movimientoPN, auxiliar);
			if(movimientoPN.getTipo().equals(TipoMovimientoEndoso.INCISO)){
					if(movimientoPN != null && movimientoPN.getValorMovimiento() != null){
						primaNeta += movimientoPN.getValorMovimiento().doubleValue();
					}
			}
		}
		
		BigDecimal primaNetaBD = new BigDecimal(primaNeta).setScale(2, BigDecimal.ROUND_HALF_UP);
		if(primaNetaBD.doubleValue() <= 0){
			esDisminucion = true;
		}
		
		return esDisminucion;
	}
	
		
	private MovimientoEndoso agregarMovimiento (ControlEndosoCot controlEndosoCot, DateTime fechaInicioVigencia, short tipoEndoso, 
			MovimientoEndoso movimientoPadre, FuenteMovimientoDTO fuenteMovimiento) {
		
		Object source = fuenteMovimiento.getSource();
		Object target = fuenteMovimiento.getTarget();
		Object bitemporal = null;
		MovimientoEndoso movimiento = null;
		String descripcionMovimiento = null;
		List<FuenteMovimientoDTO> fuentes = new ArrayListNullAware<FuenteMovimientoDTO>();
		
		TipoMovimientoEndoso tipoMovimiento = getTipoMovimientoEndoso(source, target);
		
		TipoOperacion tipoOperacion = getTipoOperacion (source, target, tipoEndoso);
		
		//Si no hubo cambio no procesa un movimiento
		if (tipoOperacion != null && tipoOperacion.equals(TipoOperacion.NINGUNO)) {
			return null;
		}
		
		//Si se trata de un movimiento de autoinciso que no sea modificacion, no procesa el movimiento
		if (((source != null && source instanceof BitemporalAutoInciso) 
				|| (target != null && target instanceof BitemporalAutoInciso)) 
			&& !tipoOperacion.equals(TipoOperacion.MODIFICADO)) {
			return null;
		}
		
		descripcionMovimiento = generarDescripcionMovimiento(controlEndosoCot, tipoEndoso, tipoMovimiento, tipoOperacion, source, target);
		
		//Si no hubo descripcionMovimiento no procesa un movimiento (porque no detecto ningun cambio que lo generara)
		if (descripcionMovimiento == null) {
			return null;
		}
		
		if (tipoOperacion == null 
				|| !tipoOperacion.equals(TipoOperacion.BORRADO) 
				|| (tipoMovimiento.equals(TipoMovimientoEndoso.COBERTURA) && target != null)) {
			bitemporal = getBitemporalObject(target);
		} else {
			bitemporal = getBitemporalObject(source);
		}
		
		movimiento = new MovimientoEndoso();
		
		movimiento.setControlEndosoCot(controlEndosoCot);
		movimiento.setDescripcion(descripcionMovimiento);
		movimiento.setTipo(tipoMovimiento);
		movimiento.setFuenteMovimiento(fuenteMovimiento);
		
		if (bitemporal instanceof BitemporalCoberturaSeccion) {
			movimiento.setBitemporalCoberturaSeccion((BitemporalCoberturaSeccion)bitemporal);
		} else if (bitemporal instanceof BitemporalInciso) {
			movimiento.setBitemporalInciso((BitemporalInciso)bitemporal);
		} else if (bitemporal instanceof BitemporalAutoInciso) {
			movimiento.setBitemporalInciso(((BitemporalAutoInciso)bitemporal).getContinuity().getParentContinuity()
					.getBitemporalProperty().getInProcess(fechaInicioVigencia));
		} else if (bitemporal instanceof BitemporalCondicionEspInc) {
			movimiento.setBitemporalInciso(((BitemporalCondicionEspInc)bitemporal).getContinuity().getParentContinuity()
					.getBitemporalProperty().getInProcess(fechaInicioVigencia));
		}
		
		movimiento.setValidacionInciso(fuenteMovimiento.getValidacionInciso());
				
		if (movimientoPadre != null) {
			movimiento.setMovimientoEndoso(movimientoPadre);
		}
		
		//Se efectua el calculo del movimiento
		//calculaMovimiento(movimiento, tipoOperacion, auxiliar, source, target);
		
		//Se agregan sus movimientos hijos
		switch (tipoMovimiento) {
			case POLIZA: {
				
				//INCISOS y AUTOINCISOS
				fuentes = filtraFuentesMovimiento(TipoMovimientoEndoso.INCISO);
				
				for (FuenteMovimientoDTO fuente : fuentes) {
					movimiento.getMovimientosEndoso().add(agregarMovimiento (controlEndosoCot, fechaInicioVigencia, 
							tipoEndoso, movimiento, fuente));
				}
				break;
			}	
			case INCISO: {
				
				if ((source == null || !(source instanceof BitemporalAutoInciso) || !(source instanceof BitemporalCondicionEspInc)) 
						&& (target == null || !(target instanceof BitemporalAutoInciso) || !(target instanceof BitemporalCondicionEspInc))) {
					
					//COBERTURAS
					fuentes = filtraFuentesMovimiento(TipoMovimientoEndoso.COBERTURA);
					
					for (FuenteMovimientoDTO fuente : fuentes) {
						if (isSameContinuity(bitemporal, fuente)) {
							movimiento.getMovimientosEndoso().add(agregarMovimiento (controlEndosoCot, fechaInicioVigencia, 
									tipoEndoso, movimiento, fuente));
						}
					}
					
				}
				break;
			}
		}
		
		return movimiento;
		
	}
	
	
	private List<FuenteMovimientoDTO> cargaFuentesMovimiento(CotizacionContinuity cotizacionContinuity, ControlEndosoCot controlEndosoCot, 
			DateTime fechaInicioVigencia, short tipoEndoso, List<IncisoContinuity> incisosBaja, AuxiliarCalculoDTO auxiliar) {
		
		List<FuenteMovimientoDTO> fuentes = new ArrayListNullAware<FuenteMovimientoDTO>();
		List<MovimientoEndoso> movimientosInversos = null;
		Map<String,Object> properties = null;
		MovimientoEndoso movimientoIncisoAuxiliar = null;
		List<FuenteMovimientoDTO> fuentesCobertura = null;
		List<FuenteMovimientoDTO> fuentesInciso = null;
		List<EndosoIDTO> validacionesInciso = new ArrayList<EndosoIDTO>();
		BigDecimal polizaId = null;
		boolean found = false;
		boolean cancelacionEAP = false;
		Long controlEndosoCotACancelarId = null;
		
		switch (tipoEndoso) {
			
			case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA: {
				 
				//Los movimientos del ultimo endoso de la poliza, que debe de ser el endoso de CPP o CAP)
				properties = new HashMap<String,Object>();
				properties.put("cotizacionId", cotizacionContinuity.getNumero());
				properties.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EMITIDA);	
				
				StringBuilder queryWhere = new StringBuilder();
				StringBuilder orderClause = new StringBuilder(" order by model.id DESC");
				
				queryWhere.append(" and model.solicitud.claveTipoEndoso IN (" + SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA + "," + SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL + ") "); 		
				
				List<ControlEndosoCot> controlEndosoCotList = entidadService.findByColumnsAndProperties(ControlEndosoCot.class, "id,cotizacionId,claveEstatusCot,solicitud,"
						+ "validFrom,recordFrom,idCancela,numeroEndosoAjusta,primaTotalIgualar,porcentajePagoFraccionado", 
						properties, new HashMap<String,Object>(), queryWhere, orderClause);
				
				ControlEndosoCot controlEndosoCotCancelacionPoliza = controlEndosoCotList.get(0);
				
				properties = new HashMap<String,Object>();
				properties.put("controlEndosoCot.id", controlEndosoCotCancelacionPoliza.getId());
								
				movimientosInversos = entidadService.findByProperties(MovimientoEndoso.class, properties);
				
				for (MovimientoEndoso movimientoInverso : movimientosInversos) {
					
					if (movimientoInverso.getTipo().equals(TipoMovimientoEndoso.POLIZA)) {
						fuentes.add(new FuenteMovimientoDTO(movimientoInverso.getTipo(), null, 
								cotizacionContinuity.getCotizaciones().getInProcess(fechaInicioVigencia)));
					} else {
						fuentes.add(new FuenteMovimientoDTO(movimientoInverso.getTipo(), null, movimientoInverso));
					}
				}
				
				break;
			}
			case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS: {
				
				//Cada movimiento de un listado de controlEndosoCot de Baja de inciso ordenados en orden descendente
				//prefiltrando solo los ultimos movimientos correspondientes a los incisos a rehabilitar
				
				//INCISOS
				List<IncisoContinuity> incisoContinuities = bitemporalDao.getContinuitiesInProcess(IncisoContinuity.class, 
						cotizacionContinuity);
				movimientosInversos = new ArrayList<MovimientoEndoso>();
				
				properties = new HashMap<String,Object>();
				properties.put("cotizacionId", cotizacionContinuity.getNumero());
				properties.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EMITIDA);
				
				StringBuilder queryWhere = new StringBuilder();
				StringBuilder orderClause = new StringBuilder(" order by model.id DESC");
				
				queryWhere.append(" and model.solicitud.claveTipoEndoso IN (" + SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO + "," + SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL + ") ");
				
				List<ControlEndosoCot> controlEndosoCotList = entidadService.findByColumnsAndProperties(ControlEndosoCot.class, "id,cotizacionId,claveEstatusCot,solicitud,"
						+ "validFrom,recordFrom,idCancela,numeroEndosoAjusta,primaTotalIgualar,porcentajePagoFraccionado", 
						properties, new HashMap<String,Object>(), queryWhere, orderClause);
				
				for (ControlEndosoCot controlEndosoCotBajaInciso : controlEndosoCotList) {
					
					properties = new HashMap<String,Object>();
					properties.put("controlEndosoCot.id", controlEndosoCotBajaInciso.getId());
									
					//movimientosInversos = entidadService.findByProperties(MovimientoEndoso.class, properties);
					movimientosInversos = entidadService.findByPropertiesWithOrder(MovimientoEndoso.class, properties, "id ASC");
										
					for (MovimientoEndoso movimientoInversoParcial : movimientosInversos) {
						found = false;
						
						for (FuenteMovimientoDTO fuenteAgregada : fuentes) {
							if (isSameContinuity(movimientoInversoParcial, fuenteAgregada)) {
								found = true;
							}
						}
						
						if (found) break;
						
						for (IncisoContinuity incisoContinuity : incisoContinuities) {
							if (isSameContinuity(incisoContinuity, movimientoInversoParcial)) {
								fuentes.add(new FuenteMovimientoDTO(movimientoInversoParcial.getTipo(), null, movimientoInversoParcial));
								break;
							}
						}
					}
				}
				
				break;
			}
			case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO: {
				
				//Los movimientos del controlEndosoCot indicado por idCancela del controlEndosoCot de la rehabilitacion de endoso
				properties = new HashMap<String,Object>();
				properties.put("controlEndosoCot.id", controlEndosoCot.getIdCancela());
								
				movimientosInversos = entidadService.findByProperties(MovimientoEndoso.class, properties);
				
				for (MovimientoEndoso movimientoInverso : movimientosInversos) {
					fuentes.add(new FuenteMovimientoDTO(movimientoInverso.getTipo(), null, movimientoInverso));
				}
				
				break;
			}
			case SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA: {
				
				List<MovimientoEndoso> movimientos = null;
				List<MovimientoEndoso> movimientosConPrima = new ArrayListNullAware<MovimientoEndoso>();
				if (controlEndosoCot.getNumeroEndosoAjusta() != null) {
					if (controlEndosoCot.getNumeroEndosoAjusta().shortValue() > 0) {
						
						properties = new HashMap<String,Object>();
						properties.put("idToCotizacion", cotizacionContinuity.getNumero());
						properties.put("id.numeroEndoso", controlEndosoCot.getNumeroEndosoAjusta());
						
						List<EndosoDTO> endosos = entidadService.findByProperties(EndosoDTO.class, properties);
						
						if (endosos != null && endosos.size() > 0) {
							
							properties = new HashMap<String,Object>();
							properties.put("cotizacionId", cotizacionContinuity.getNumero());
							properties.put("validFrom", endosos.get(0).getValidFrom());
							properties.put("recordFrom", endosos.get(0).getRecordFrom());
							
							List<ControlEndosoCot> controlEndosoCots = entidadService.findByProperties(ControlEndosoCot.class, properties);
							if (controlEndosoCots != null && controlEndosoCots.size() > 0) {
								
								//Se filtran los movimientos con prima
								properties = new HashMap<String,Object>();
								properties.put("controlEndosoCot.id", controlEndosoCots.get(0).getId());
												
								movimientos = entidadService.findByProperties(MovimientoEndoso.class, properties);
								
								
								for (MovimientoEndoso movimiento : movimientos) {
									if (movimiento.getValorMovimiento() != null && movimiento.getValorMovimiento().compareTo(CERO) != 0) {
										if(movimiento.getTipo().equals(TipoMovimientoEndoso.COBERTURA)){
												if(movimiento.getBitemporalCoberturaSeccion().getContinuity().getCoberturaDTO().getCoberturaEsPropia().equals("1")){
													movimientosConPrima.add(movimiento);
												}
										}else{
											movimientosConPrima.add(movimiento);
										}
									}
								}
							}
						}
						
					} else {
						//Se Generan movimientos para EAP de endoso 0 (solo trae los de cobertura e inciso con valor de prima)
						generaMovimientosEndosoCeroConPrima(movimientosConPrima, cotizacionContinuity);
					}
				}
								
				boolean porcentajeComisionCalculado = false;
				Double porcentajeComisionPoliza = 0D;
				for (MovimientoEndoso movimiento : movimientosConPrima) {
					
					if (!porcentajeComisionCalculado && movimiento.getTipo().equals(TipoMovimientoEndoso.COBERTURA)) {
						//Se obtiene el porcentaje de Comision default de la poliza
						BitemporalCoberturaSeccion cobertura = (BitemporalCoberturaSeccion)getBitemporalObject(movimiento);
						porcentajeComisionPoliza = getPorcentajeComisionPoliza (cotizacionContinuity, cobertura.getContinuity().getCoberturaDTO());
						porcentajeComisionCalculado = true;
						
					}
					
					fuentes.add(new FuenteMovimientoDTO(movimiento.getTipo(), null, movimiento));
				}
				//Se guarda el porcentaje de Comision como dato auxiliar para los calculos posteriores
				auxiliar.setPorcentajeComision(porcentajeComisionPoliza);
				
				break;
			}
			default: {
				
				/**
				 Si se trata de una Cancelacion de Poliza ,una Cancelacion de Endoso o una Cancelacion de Inciso, 
				 independientemente si son automaticas o a peticion, se validan los Pagos Realizados en Seycos para 
				 saber como deben ser cancelados cada uno de sus incisos. De ahi validar si todos los incisos 
				 involucrados en los movimientos son validos, y por cada uno calcular el factorIgualacionCobranza 
				 y actualizar con este valor todos los movimientos de coberturas de ese inciso y tambien el 
				 movimiento del inciso.

				 */
				
				//Se obtiene la polizaId
				properties = new HashMap<String,Object>();
				properties.put("cotizacionDTO.idToCotizacion", cotizacionContinuity.getNumero());
				List<PolizaDTO> polizaList = entidadService.findByProperties(PolizaDTO.class, properties);
				if (polizaList != null && !polizaList.isEmpty()) {
					polizaId = polizaList.get(0).getIdToPoliza();
				}
				
					 
				if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA) {
					
					validacionesInciso = endosoService.validaPagosRealizados (polizaId, new Long(0), null, fechaInicioVigencia.toDate(), 
							TipoEmision.CANC_POL_AUTOS);
					
					
				} else if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO) {
					
					Short numeroEndoso = 0;
					
					ControlEndosoCot controlEndosoCotACancelar = entidadService.findById(ControlEndosoCot.class, controlEndosoCot.getIdCancela());
					
					Map<String, Object> values = new LinkedHashMap<String, Object>();
					values.put("idToCotizacion", new BigDecimal(controlEndosoCotACancelar.getCotizacionId()));
					values.put("recordFrom", controlEndosoCotACancelar.getRecordFrom());
					List<EndosoDTO> endosos = (List<EndosoDTO>)entidadService.findByProperties(EndosoDTO.class, values);
					
					numeroEndoso = (endosos != null && !endosos.isEmpty()?endosos.get(0).getId().getNumeroEndoso(): 0);
					
					if(!esMergeMovimientoAjuste) { //LPV Bandera para no validar pagos de EAP en Cancelaciones (la cancelacion en si ya valida los pagos)
						validacionesInciso = endosoService.validaPagosRealizados (polizaId, new Long(numeroEndoso), null, fechaInicioVigencia.toDate(), 
							TipoEmision.CANC_END_AUTOS);
					}
					
					//En caso de ser una cancelacion de EAP
					if (controlEndosoCotACancelar.getNumeroEndosoAjusta() != null) {
						
						cancelacionEAP = true;
						controlEndosoCotACancelarId = controlEndosoCotACancelar.getId();
						
					}
					
				} else if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO) {
					
					String continuitiesString = "";
					
					String[] continuitiesStringIds = null;
					// Se ajusta para traer unicamente aquellos incisos que seran dados de baja
					//List<BitemporalInciso> incisos = (List<BitemporalInciso>) cotizacionContinuity.getIncisoContinuities().getToBeEnded(fechaInicioVigencia);
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("continuity.cotizacionContinuity.id", cotizacionContinuity.getId());
					params.put("recordStatus", RecordStatus.TO_BE_ENDED);
					List<BitemporalInciso> incisos = bitemporalDao.findByPropertiesWithOrder(BitemporalInciso.class, params, fechaInicioVigencia, "continuity.numero");
					continuitiesString = obtenerContinuitiesString(incisos);					
					if(!continuitiesString.isEmpty()) {
						continuitiesString = continuitiesString.substring(0, continuitiesString.lastIndexOf(","));
						continuitiesStringIds = continuitiesString.split(",");			
					}
					
					validacionesInciso = endosoService.validaPagosRealizados (polizaId, null, continuitiesStringIds, fechaInicioVigencia.toDate(), 
							TipoEmision.CANC_INCISO_AUTOS);
				
				}else if(tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL 
						|| tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL)
				{
					List<BitemporalInciso> incisos = (List<BitemporalInciso>) cotizacionContinuity.getIncisoContinuities().getToBeEnded(fechaInicioVigencia);
					
					// Se asume 1 solo registro, el endosos de PT solamente se puede dar de baja/cancelar 1 inciso por endoso
					Integer numeroInciso = incisos.get(0).getContinuity().getNumero();
					
					validacionesInciso = endosoService.validaEndosoPT(polizaId, numeroInciso, fechaInicioVigencia.toDate(), 
							controlEndosoCot.getSolicitud().getClaveMotivoEndoso());					
				}
			
				if (!cancelacionEAP) {
					fuentes.add(new FuenteMovimientoDTO(TipoMovimientoEndoso.POLIZA, cotizacionContinuity.getCotizaciones().get(fechaInicioVigencia), 
							cotizacionContinuity.getCotizaciones().getInProcess(fechaInicioVigencia)));
					
					//INCISOS
					List<IncisoContinuity> incisoContinuities = bitemporalDao.getContinuitiesInProcess(IncisoContinuity.class, 
							cotizacionContinuity);
					
					for (IncisoContinuity incisoContinuity : incisoContinuities) {
						
						/*
						if (incisoContinuity.getIncisos().get(fechaInicioVigencia) != null 
								|| incisoContinuity.getIncisos().getInProcess(fechaInicioVigencia) != null) {
							fuentes.add(new FuenteMovimientoDTO(TipoMovimientoEndoso.INCISO, incisoContinuity.getIncisos().get(fechaInicioVigencia), 
									incisoContinuity.getIncisos().getInProcess(fechaInicioVigencia)));
						}
						*/
						BitemporalInciso itemGet = incisoContinuity.getIncisos().get(fechaInicioVigencia);
						BitemporalInciso itemProcess = incisoContinuity.getIncisos().getInProcess(fechaInicioVigencia);
						if (itemGet != null || itemProcess != null) {
							fuentes.add(new FuenteMovimientoDTO(TipoMovimientoEndoso.INCISO, itemGet, itemProcess));
						}
					}
					
					//AUTOINCISOS
					List<AutoIncisoContinuity> autoIncisoContinuities = bitemporalDao.getContinuitiesInProcess(AutoIncisoContinuity.class, 
							cotizacionContinuity);
					
					for (AutoIncisoContinuity autoIncisoContinuity : autoIncisoContinuities) {
						/*
						if (autoIncisoContinuity.getIncisoAutos().get(fechaInicioVigencia) != null 
								|| autoIncisoContinuity.getIncisoAutos().getInProcess(fechaInicioVigencia) != null) {
							fuentes.add(new FuenteMovimientoDTO(TipoMovimientoEndoso.INCISO, autoIncisoContinuity.getIncisoAutos().get(fechaInicioVigencia), 
									autoIncisoContinuity.getIncisoAutos().getInProcess(fechaInicioVigencia)));
						}*/
						BitemporalAutoInciso itemGet = autoIncisoContinuity.getIncisoAutos().get(fechaInicioVigencia);
						BitemporalAutoInciso itemProcess = autoIncisoContinuity.getIncisoAutos().getInProcess(fechaInicioVigencia);
						if (itemGet != null || itemProcess != null) {
							fuentes.add(new FuenteMovimientoDTO(TipoMovimientoEndoso.INCISO, itemGet, itemProcess));
						}						
					}
					
					//CONDICIONES ESPECIALES INCISO
					List<CondicionEspIncContinuity> condicionesIncisoContinuities = bitemporalDao.getContinuitiesInProcess(
																			CondicionEspIncContinuity.class, cotizacionContinuity);
					
					if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES || tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO) {
						for (CondicionEspIncContinuity condicionIncisoContinuity : condicionesIncisoContinuities) {
							/*
							if (condicionIncisoContinuity.getCondEspInciso().get(fechaInicioVigencia) != null
								|| condicionIncisoContinuity.getCondEspInciso().getInProcess(fechaInicioVigencia) != null) {
									fuentes.add(new FuenteMovimientoDTO(TipoMovimientoEndoso.INCISO,  null, 
											condicionIncisoContinuity.getCondEspInciso().getInProcess(fechaInicioVigencia)));
								}
							*/
							BitemporalCondicionEspInc itemGet = condicionIncisoContinuity.getCondEspInciso().get(fechaInicioVigencia);
							BitemporalCondicionEspInc itemProcess = condicionIncisoContinuity.getCondEspInciso().getInProcess(fechaInicioVigencia);
							if (itemGet != null || itemProcess != null) {
								fuentes.add(new FuenteMovimientoDTO(TipoMovimientoEndoso.INCISO, itemGet, itemProcess));
							}
						}	
					} else if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_CONDICIONES) {
						for (CondicionEspIncContinuity condicionIncisoContinuity : condicionesIncisoContinuities) {
							
							if (condicionIncisoContinuity.getCondEspInciso().get(fechaInicioVigencia) != null
								|| condicionIncisoContinuity.getCondEspInciso().getToBeEnded(fechaInicioVigencia) != null) {
									fuentes.add(new FuenteMovimientoDTO(TipoMovimientoEndoso.INCISO,  null, 
											condicionIncisoContinuity.getCondEspInciso().getToBeEnded(fechaInicioVigencia)));
								}
						}	
					}
					
				
					
					//COBERTURAS
					List<CoberturaSeccionContinuity> coberturaSeccionContinuities = null;
					
					if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO) {
						properties = new HashMap<String,Object>();
						properties.put("seccionIncisoContinuity.incisoContinuity.cotizacionContinuity.numero", cotizacionContinuity.getNumero());
						coberturaSeccionContinuities = entidadService.findByPropertiesWithOrder(CoberturaSeccionContinuity.class, properties);
					} else {
						coberturaSeccionContinuities = bitemporalDao.getContinuitiesInProcess(CoberturaSeccionContinuity.class, 
								cotizacionContinuity);
					}
					
					Collection<BitemporalCotizacion> cotizaciones = entidadBitemporalService.findInProcessByParentBusinessKey(
							CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, cotizacionContinuity.getNumero(), fechaInicioVigencia);
					DateTime fechaCFP = null;
					if(cotizaciones != null && !cotizaciones.isEmpty()){
						GregorianCalendar gcFechaValidez = new GregorianCalendar();
						gcFechaValidez.setTime(cotizaciones.iterator().next().getValue().getFechaFinVigencia());
						gcFechaValidez.add(GregorianCalendar.SECOND, -1);
						fechaCFP = new DateTime(gcFechaValidez.getTime());
					}else{
						fechaCFP = fechaInicioVigencia;
					}
					
					for (CoberturaSeccionContinuity coberturaSeccionContinuity : coberturaSeccionContinuities) {				
						BitemporalCoberturaSeccion itemProcess = coberturaSeccionContinuity.getCoberturaSecciones().getInProcess(fechaInicioVigencia);
						//En cambio de forma de pago solo se trae los contratados
						if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO) {
							BitemporalCoberturaSeccion itemGet = coberturaSeccionContinuity.getCoberturaSecciones().get(fechaCFP);
							if(itemGet != null && itemGet.getValue().getClaveContrato().intValue() == 1){
								fuentes.add(new FuenteMovimientoDTO(TipoMovimientoEndoso.COBERTURA, itemGet, itemProcess));
							}
						}else{
							BitemporalCoberturaSeccion itemGet = coberturaSeccionContinuity.getCoberturaSecciones().get(fechaInicioVigencia);						
							if (itemGet != null || itemProcess != null) {
								fuentes.add(new FuenteMovimientoDTO(TipoMovimientoEndoso.COBERTURA, itemGet, itemProcess));
							}							
						}
					}
					
				} else {
					
					//Los movimientos del controlEndosoCot indicado por idCancela en caso de tratarse de un EAP
					properties = new HashMap<String,Object>();
					properties.put("controlEndosoCot.id", controlEndosoCotACancelarId);
									
					movimientosInversos = entidadService.findByProperties(MovimientoEndoso.class, properties);
					
					for (MovimientoEndoso movimientoInverso : movimientosInversos) {
						
						if (incisosBaja != null && incisosBaja.size() > 0) {
							//Aqui se filtra por los incisos a dar de baja en caso de que haya alguno
							for (IncisoContinuity incisoBaja : incisosBaja) {
								if (isSameContinuity(incisoBaja, movimientoInverso)) {
									fuentes.add(new FuenteMovimientoDTO(movimientoInverso.getTipo(), null, movimientoInverso));	
									break;
								}
							}
						} else {
							fuentes.add(new FuenteMovimientoDTO(movimientoInverso.getTipo(), null, movimientoInverso));	
						}
						
					}
					
					
				}
				
				break;
			}
			
		}
		
		//Aqui se agregan movimientos de inciso "Auxiliares" para el calculo en caso de que algun movimiento de cobertura no tenga padre
		fuentesCobertura = filtraFuentesMovimiento(fuentes, TipoMovimientoEndoso.COBERTURA);
		
		for (FuenteMovimientoDTO fuenteCobertura : fuentesCobertura) {
			
			found = false;
			fuentesInciso = filtraFuentesMovimiento(fuentes, TipoMovimientoEndoso.INCISO);
			
			for (FuenteMovimientoDTO fuenteInciso : fuentesInciso) {
				if (isSameContinuity(fuenteInciso, fuenteCobertura)) {
					found = true;
					break;
				}
			}
			
			if (!found) {
				
				movimientoIncisoAuxiliar = new MovimientoEndoso();
				movimientoIncisoAuxiliar.setControlEndosoCot(controlEndosoCot);
				movimientoIncisoAuxiliar.setDescripcion(" ");
				movimientoIncisoAuxiliar.setBitemporalInciso(((BitemporalCoberturaSeccion)getBitemporalObject(fuenteCobertura))
						.getContinuity().getParentContinuity().getParentContinuity().getBitemporalProperty().getInProcess(fechaInicioVigencia));
				movimientoIncisoAuxiliar.setTipo(TipoMovimientoEndoso.INCISO);
				
				fuentes.add(new FuenteMovimientoDTO(TipoMovimientoEndoso.INCISO, null, movimientoIncisoAuxiliar));
			}
			
		}
		
		//En caso de EAP, se customizan las validaciones inciso a partir de primaTotalIgualar y valorTotalPrimasEmitido 
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA) {
				
				BigDecimal valorPrimaTotalEmitida = null;
				
				//Se obtiene el total de primas emitido del endoso a ajustar
				properties = new HashMap<String,Object>();
				properties.put("idToCotizacion", cotizacionContinuity.getNumero());
				properties.put("id.numeroEndoso", controlEndosoCot.getNumeroEndosoAjusta());
				
				List<EndosoDTO> endosos = entidadService.findByProperties(EndosoDTO.class, properties);
				
				if (endosos != null && endosos.size() > 0) {
					valorPrimaTotalEmitida = new BigDecimal(endosos.get(0).getValorPrimaTotal());
				}
				
				//Se obtienen validacionesInciso a partir de primaTotalIgualar y valorTotalPrimasEmitido
				validacionesInciso = getValidacionesIncisoEAP(controlEndosoCot, valorPrimaTotalEmitida,
						filtraFuentesMovimiento(fuentes, TipoMovimientoEndoso.INCISO), auxiliar);
				
		}
		
		
		
		//Aqui se agregan las validaciones de Cobranza realizadas a Seycos en caso de ser requeridas
		//Se encuentra la validacion del inciso correspondiente al inciso de la fuente
		if (validacionesInciso != null && !validacionesInciso.isEmpty()) {
			EndosoIDTO validacionInciso = null;
			Predicate findId = new Predicate() {
				@Override
				public boolean evaluate(Object arg0) {
					EndosoIDTO obj = (EndosoIDTO)arg0; 

					return obj.getIdCobertura() == null && obj.getNumeroInciso() != null 
							&& obj.getNumeroInciso().equals(MovimientoEndosoBitemporalServiceNewImpl.getNumeroInciso());
				}
			};
			
			Predicate findIdCob = new Predicate() {
				@Override
				public boolean evaluate(Object arg0) {
					EndosoIDTO obj = (EndosoIDTO)arg0; 

					return obj.getIdCobertura() != null && obj.getIdCobertura().equals(MovimientoEndosoBitemporalServiceNewImpl.getIdCobertura())
					&& obj.getNumeroInciso() != null && obj.getNumeroInciso().equals(MovimientoEndosoBitemporalServiceNewImpl.getNumeroInciso());
				}
			};
			
			fuentesInciso = filtraFuentesMovimiento(fuentes, TipoMovimientoEndoso.INCISO);
			
			for (FuenteMovimientoDTO fuente : fuentesInciso) {
				if (getBitemporalObject(fuente) instanceof BitemporalInciso) {
					MovimientoEndosoBitemporalServiceNewImpl.setNumeroInciso(((BitemporalInciso)getBitemporalObject(fuente)).getContinuity().getNumero());
					validacionInciso = (EndosoIDTO)CollectionUtils.find(validacionesInciso, findId);
					fuente.setValidacionInciso(validacionInciso);
				}
			}
			
			fuentesInciso = filtraFuentesMovimiento(fuentes, TipoMovimientoEndoso.COBERTURA);
			
			for (FuenteMovimientoDTO fuente : fuentesInciso) {
				//if (getBitemporalObject(fuente) instanceof BitemporalInciso) {					
					MovimientoEndosoBitemporalServiceNewImpl.setNumeroInciso(((BitemporalCoberturaSeccion)getBitemporalObject(fuente))
							.getContinuity().getParentContinuity().getIncisoContinuity().getNumero());
					MovimientoEndosoBitemporalServiceNewImpl.setIdCobertura(((BitemporalCoberturaSeccion)getBitemporalObject(fuente))
							.getContinuity().getCoberturaDTO().getIdToCobertura().intValue());
					validacionInciso = (EndosoIDTO)CollectionUtils.find(validacionesInciso, findIdCob);
					fuente.setValidacionInciso(validacionInciso);
				//}
			}
			
		}
				
		return fuentes;
	}
	
	public String obtenerContinuitiesString(List<BitemporalInciso> incisos) {
		StringBuilder continuitiesString = new StringBuilder("");
		for (BitemporalInciso inciso : incisos) {
			continuitiesString.append(inciso.getEntidadContinuity().getId()).append(",");
		}
		return continuitiesString.toString();
	}

	private List<FuenteMovimientoDTO> filtraFuentesMovimiento (TipoMovimientoEndoso tipoMovimiento) {
		return filtraFuentesMovimiento(fuentesMovimiento, tipoMovimiento);
	}
	
	private List<FuenteMovimientoDTO> filtraFuentesMovimiento (List<FuenteMovimientoDTO> fuentesOriginales, TipoMovimientoEndoso tipoMovimiento) {
		
		List<FuenteMovimientoDTO> fuentesFiltradas = new ArrayListNullAware<FuenteMovimientoDTO>();
		
		for (FuenteMovimientoDTO fuente : fuentesOriginales) {
			
			if (fuente.getTipoMovimientoEndoso().equals(tipoMovimiento)) {
				fuentesFiltradas.add(fuente);
			}
			
		}
		
		return fuentesFiltradas;
		
	}
	
	private void mergeMovimiento (MovimientoEndoso movimientoOriginal, MovimientoEndoso movimientoAgregado) {
		if(movimientoOriginal.getValorMovimiento() == null){
			movimientoOriginal.setValorMovimiento(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorMovimiento(
				(movimientoOriginal.getValorMovimiento().add(movimientoAgregado.getValorMovimiento().multiply(movimientoAgregado.getFactorCobranzaPrimaNeta()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaPrimaNeta(UNO);
		
		if(movimientoOriginal.getValorBonificacionComision() == null){
			movimientoOriginal.setValorBonificacionComision(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorBonificacionComision(
				(movimientoOriginal.getValorBonificacionComision().add(movimientoAgregado.getValorBonificacionComision().multiply(movimientoAgregado.getFactorCobranzaBonificacionComision()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaBonificacionComision(UNO);
		
		if(movimientoOriginal.getValorBonificacionRPF() == null){
			movimientoOriginal.setValorBonificacionRPF(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorBonificacionRPF(
				(movimientoOriginal.getValorBonificacionRPF().add(movimientoAgregado.getValorBonificacionRPF().multiply(movimientoAgregado.getFactorCobranzaBonificacionRPF()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaBonificacionRPF(UNO);
		
		if(movimientoOriginal.getValorRPF() == null){
			movimientoOriginal.setValorRPF(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorRPF(
				(movimientoOriginal.getValorRPF().add(movimientoAgregado.getValorRPF().multiply(movimientoAgregado.getFactorCobranzaRPF()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaRPF(UNO);
		
		if(movimientoOriginal.getValorComision() == null){
			movimientoOriginal.setValorComision(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorComision(
				(movimientoOriginal.getValorComision().add(movimientoAgregado.getValorComision().multiply(movimientoAgregado.getFactorCobranzaComision()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaComision(UNO);
		
		if(movimientoOriginal.getValorComisionRPF() == null){
			movimientoOriginal.setValorComisionRPF(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorComisionRPF(
				(movimientoOriginal.getValorComisionRPF().add(movimientoAgregado.getValorComisionRPF().multiply(movimientoAgregado.getFactorCobranzaComisionRPF()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaComisionRPF(UNO);
		
		if(movimientoOriginal.getValorDerechos() == null){
			movimientoOriginal.setValorDerechos(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorDerechos(
				(movimientoOriginal.getValorDerechos().add(movimientoAgregado.getValorDerechos().multiply(movimientoAgregado.getFactorCobranzaDerechos()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaDerechos(UNO);
		
		if(movimientoOriginal.getValorIva() == null){
			movimientoOriginal.setValorIva(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorIva(
				(movimientoOriginal.getValorIva().add(movimientoAgregado.getValorIva().multiply(movimientoAgregado.getFactorCobranzaIva()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaIva(UNO);
		
		if(movimientoOriginal.getValorSobreComisionAgente() == null){
			movimientoOriginal.setValorSobreComisionAgente(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorSobreComisionAgente(
				(movimientoOriginal.getValorSobreComisionAgente().add(movimientoAgregado.getValorSobreComisionAgente().multiply(movimientoAgregado.getFactorCobranzaSobreComisionAgente()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaSobreComisionAgente(UNO);
		
		if(movimientoOriginal.getValorSobreComisionProm() == null){
			movimientoOriginal.setValorSobreComisionProm(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorSobreComisionProm(
				(movimientoOriginal.getValorSobreComisionProm().add(movimientoAgregado.getValorSobreComisionProm().multiply(movimientoAgregado.getFactorCobranzaSobreComisionProm()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaSobreComisionProm(UNO);
		
		if(movimientoOriginal.getValorBonoAgente() == null){
			movimientoOriginal.setValorBonoAgente(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorBonoAgente(
				(movimientoOriginal.getValorBonoAgente().add(movimientoAgregado.getValorBonoAgente().multiply(movimientoAgregado.getFactorCobranzaBonoAgente()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaBonoAgente(UNO);
		
		if(movimientoOriginal.getValorBonoProm() == null){
			movimientoOriginal.setValorBonoProm(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorBonoProm(
				(movimientoOriginal.getValorBonoProm().add(movimientoAgregado.getValorBonoProm().multiply(movimientoAgregado.getFactorCobranzaBonoProm()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaBonoProm(UNO);
		
		if(movimientoOriginal.getValorCesionDerechosAgente() == null){
			movimientoOriginal.setValorCesionDerechosAgente(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorCesionDerechosAgente(
				(movimientoOriginal.getValorCesionDerechosAgente().add(movimientoAgregado.getValorCesionDerechosAgente().multiply(movimientoAgregado.getFactorCobranzaCesionDerechosAgente()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaCesionDerechosAgente(UNO);
		
		if(movimientoOriginal.getValorCesionDerechosProm() == null){
			movimientoOriginal.setValorCesionDerechosProm(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorCesionDerechosProm(
				(movimientoOriginal.getValorCesionDerechosProm().add(movimientoAgregado.getValorCesionDerechosProm().multiply(movimientoAgregado.getFactorCobranzaCesionDerechosProm()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaCesionDerechosProm(UNO);
		
		if(movimientoOriginal.getValorSobreComisionUDIAgente() == null){
			movimientoOriginal.setValorSobreComisionUDIAgente(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorSobreComisionUDIAgente(
				(movimientoOriginal.getValorSobreComisionUDIAgente().add(movimientoAgregado.getValorSobreComisionUDIAgente().multiply(movimientoAgregado.getFactorCobranzaSobreComisionUDIAgente()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaSobreComisionUDIAgente(UNO);
		
		if(movimientoOriginal.getValorSobreComisionUDIProm() == null){
			movimientoOriginal.setValorSobreComisionUDIProm(BigDecimal.ZERO);
		}
		movimientoOriginal.setValorSobreComisionUDIProm(
				(movimientoOriginal.getValorSobreComisionUDIProm().add(movimientoAgregado.getValorSobreComisionUDIProm().multiply(movimientoAgregado.getFactorCobranzaSobreComisionUDIProm()))).setScale(16, BigDecimal.ROUND_HALF_UP)
				);
		movimientoOriginal.setFactorCobranzaSobreComisionUDIProm(UNO);
		
	}
	
	
	private String generarDescripcionMovimiento (ControlEndosoCot controlEndosoCot, short tipoEndoso, TipoMovimientoEndoso tipoMovimiento, 
			TipoOperacion tipoOperacion, Object source, Object target) {
		
		Object bitemporalSource = null;
		Object bitemporalTarget = null;
		ControlEndosoCot controlEndosoCotCE = null;
		Map<String,Object> properties = null;
		List <MovimientoEndoso> movimientos = null;
		
		//Si se trata de un movimiento de Inciso Auxiliar, se regresa una descripcion vacia
		if (tipoMovimiento.equals(TipoMovimientoEndoso.INCISO) && target != null && target instanceof MovimientoEndoso) {
			MovimientoEndoso movimiento = (MovimientoEndoso) target;
			if (movimiento.getDescripcion().trim().equals("")) {
				return " ";
			}
		}
		
		//Se sacan objetos bitemporales en caso de que source y target sean objetos MovimientoEndoso
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO) {
			
			controlEndosoCotCE = entidadService.findById(ControlEndosoCot.class, controlEndosoCot.getIdCancela());
			properties = new HashMap<String,Object>();
			properties.put("controlEndosoCot.id", controlEndosoCotCE.getIdCancela());
							
			movimientos = entidadService.findByProperties(MovimientoEndoso.class, properties);
			
			
			//Se obtienen los movimientos del endoso a rehabilitar y se revisa coincidencia con su bitemporal para usar su descripcion
			if(tipoMovimiento.equals(TipoMovimientoEndoso.POLIZA)){
				for (MovimientoEndoso movimiento : movimientos) {
					if (movimiento.getTipo().equals(tipoMovimiento)) {
						return movimiento.getDescripcion();
					}
				}
			}else{
				for (MovimientoEndoso movimiento : movimientos) {
					if (isSameContinuity(movimiento, target)) {
						return movimiento.getDescripcion();
					}
				}
			}
			
		}
		
		bitemporalSource = getBitemporalObject(source);
		bitemporalTarget = getBitemporalObject(target);
		
		
		
		switch (tipoMovimiento) {
			case POLIZA:
				return generarDescripcionMovimientoGeneral(controlEndosoCot, tipoEndoso, tipoOperacion, bitemporalSource, bitemporalTarget);
			case INCISO:
				return generarDescripcionMovimientoInciso(controlEndosoCot, tipoEndoso, tipoOperacion, bitemporalSource, bitemporalTarget);
			case COBERTURA:
				return generarDescripcionMovimientoCobertura(controlEndosoCot, tipoEndoso, tipoOperacion, bitemporalSource, bitemporalTarget);
		}
		
		return null;
	}
	
	private String generarDescripcionMovimientoGeneral (ControlEndosoCot controlEndosoCot, short tipoEndoso, TipoOperacion tipoOperacion, 
			Object source, Object target) {
		
		BitemporalCotizacion cotizacionSource = (BitemporalCotizacion) source;
		BitemporalCotizacion cotizacionTarget = (BitemporalCotizacion) target;
		
		switch (tipoOperacion) {
			case AGREGADO:
				switch (tipoEndoso) {
					case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA: 
					
						return Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.rehabpoliza.descripcion",
								df.format(controlEndosoCot.getValidFrom()), 
								df.format(getFechaFinVigenciaPoliza(cotizacionTarget.getContinuity(), TimeUtils.getDateTime(controlEndosoCot.getValidFrom())).toDate()));
						
					
					case SolicitudDTO.CVE_TIPO_ENDOSO_EXTENSION_VIGENCIA: 
						
						return Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.extvigencia.descripcion",
								df.format(controlEndosoCot.getValidFrom()), 
								df.format(cotizacionTarget.getValidityInterval().getInterval().getEnd().toDate()));
						
					
					default:
						
						throw new RuntimeException("El movimiento de aumento en Cotizacion no corresponde al " +
								" tipo de Endoso de Rehab. de Poliza o Ext de Vigencia");
				}
				
				
				
			case BORRADO:
				
				return Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cancpoliza.descripcion");
				
			case MODIFICADO: {
				
				switch (tipoEndoso) {
					case SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_ANEXO: 
					
						return Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.inclusionanexo.descripcion");
						
					
					case SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_TEXTO: {
						
						List<BitemporalTexAdicionalCot> listTextos = (List<BitemporalTexAdicionalCot>)cotizacionTarget.getContinuity()
							.getTexAdicionalCotContinuities().getInProcess(cotizacionTarget.getValidityInterval().getInterval().getStart());
						
						StringBuilder descripcion = new StringBuilder();
						descripcion.append(Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.inclusiontexto.descripcion"));
						descripcion.append(":");
						for(BitemporalTexAdicionalCot texto : listTextos){
							if (texto.getRecordInterval()==null) {
								descripcion.append(" ");
								descripcion.append(System.getProperty("line.separator"));
								descripcion.append(" ");
								descripcion.append(System.getProperty("line.separator"));
								descripcion.append(texto.getValue().getDescripcionTexto());
							}
						}
						
						return descripcion.toString();
						
					}
					
					case SolicitudDTO.CVE_TIPO_ENDOSO_EXCLUSION_TEXTO: {

						Collection<TexAdicionalCotContinuity> lstTexAdicionalCotContinuity = entidadContinuityService.findContinuitiesByParentBusinessKey(
								 TexAdicionalCotContinuity.class, TexAdicionalCotContinuity.PARENT_KEY_NAME, cotizacionTarget.getContinuity().getId());
						
						StringBuilder descripcion = new StringBuilder();
						descripcion.append(Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.exclusiontexto.descripcion"));
						descripcion.append(":");
						for (TexAdicionalCotContinuity texAdicionalCotContinuity : lstTexAdicionalCotContinuity) {
							if (texAdicionalCotContinuity.getTexAdicionales().hasRecordsInProcess()) {
								BitemporalTexAdicionalCot bitemporalTexAdicionalCot = texAdicionalCotContinuity.getTexAdicionales().get(new DateTime(controlEndosoCot.getValidFrom()));
								if(bitemporalTexAdicionalCot != null){
									descripcion.append(" ");
									descripcion.append(System.getProperty("line.separator"));
									descripcion.append(" ");
									descripcion.append(System.getProperty("line.separator"));
									descripcion.append(bitemporalTexAdicionalCot.getValue().getDescripcionTexto());
								}
							}
						}
						
						return descripcion.toString();
						
					}
					
					case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_AGENTE: {
						
						Agente agenteSource = getAgente(cotizacionSource.getValue().getSolicitud().getCodigoAgente().longValue());
						Agente agenteTarget = getAgente(cotizacionTarget.getValue().getSolicitud().getCodigoAgente().longValue());
						
						return Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambioagente.descripcion",
								agenteSource.getPersona().getNombreCompleto(), agenteSource.getIdAgente() + "",
								agenteTarget.getPersona().getNombreCompleto(), agenteTarget.getIdAgente() + "");
					}
					
					case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO: {
						
						return Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cfp.descripcion",
								cotizacionSource.getValue().getFormaPago().getDescripcion(),
								cotizacionTarget.getValue().getFormaPago().getDescripcion());
						
					}
					
					case SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES: {
						
						List<BitemporalCondicionEspCot> listCondiciones = (List<BitemporalCondicionEspCot>)cotizacionTarget.getContinuity()
							.getCondicionEspecialCotContinuities().getInProcess(cotizacionTarget.getValidityInterval().getInterval().getStart());
						StringBuilder descripcion = new StringBuilder();
						if (listCondiciones != null && !listCondiciones.isEmpty()) {
							StringBuilder descripcionCondiciones = new StringBuilder();
							
							
							for (BitemporalCondicionEspCot bitemp : listCondiciones) {
								
								if (bitemp.getRecordInterval()==null) {
									CondicionEspecial condicion = entidadService.findById(CondicionEspecial.class, bitemp.getContinuity().getCondicionEspecialId());
									descripcionCondiciones.append(" ");
									descripcionCondiciones.append(System.getProperty("line.separator"));
									descripcionCondiciones.append(condicion.getCodigo());
									descripcionCondiciones.append(" - ");
									descripcionCondiciones.append(condicion.getNombre());
								}
							}
							
							if (descripcionCondiciones.length() > 0) {
								descripcion.append(Utilerias.getMensajeRecurso(
										SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.condicionesEspecialesPoliza"));
								descripcion.append(":");
								descripcion.append(descripcionCondiciones);
								return descripcion.toString();
							} else {
								return null;
							}
							
						} 
						
					} 
					case SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_CONDICIONES: {

						Collection<CondicionEspCotContinuity> lstCondicionesContinuity = entidadContinuityService.findContinuitiesByParentBusinessKey(
								CondicionEspCotContinuity.class, "cotizacionContinuity.id", cotizacionTarget.getContinuity().getId());
						
						StringBuilder descripcion = new StringBuilder();
						StringBuilder descripcionCondiciones = new StringBuilder();
						
						for (CondicionEspCotContinuity condicionContinuity : lstCondicionesContinuity) {
							if (condicionContinuity.getCondicionesEspecialesCotizacion().hasRecordsInProcess()) {
								BitemporalCondicionEspCot bitemporalCondicionEspCot = condicionContinuity.getCondicionesEspecialesCotizacion().get(new DateTime(controlEndosoCot.getValidFrom()));
								if( bitemporalCondicionEspCot != null) {
									CondicionEspecial condicion = entidadService.findById(CondicionEspecial.class, bitemporalCondicionEspCot.getContinuity().getCondicionEspecialId());
									descripcionCondiciones.append(" ");
									descripcionCondiciones.append(System.getProperty("line.separator"));
									descripcionCondiciones.append(condicion.getCodigo());
									descripcionCondiciones.append(" - ");
									descripcionCondiciones.append(condicion.getNombre());
								}
							}
						}
						
						if (descripcionCondiciones.length() > 0) {
							descripcion.append(Utilerias.getMensajeRecurso(
									SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.condicionesEspecialesBaja"));
							descripcion.append(":");
							descripcion.append(descripcionCondiciones);
							return descripcion.toString();
						} else {
							return null;
						}
						
						
					} case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_CONDUCTO_COBRO: {
						return Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambioconductocobro.descripcion",
								cotizacionSource.getValue().getMedioPago().getDescripcion(),
								cotizacionTarget.getValue().getMedioPago().getDescripcion());
					}
					
					default: {
						
						//Datos del contratante
						StringBuilder descripcion = new StringBuilder("");
						DomicilioImpresion domicilioSource = null;
						DomicilioImpresion domicilioTarget = null;
						ClienteDTO clienteSource = null;
						ClienteDTO clienteTarget = null;
						boolean cambioRFC = false;
						
						
						if (!cotizacionTarget.getValue().getPersonaContratanteId().equals(cotizacionSource.getValue().getPersonaContratanteId())) {
							
							descripcion.append(Utilerias.getMensajeRecurso(
									SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatoscontratante.idasegurado.descripcion",
									cotizacionSource.getValue().getPersonaContratanteId() + "",
									cotizacionTarget.getValue().getPersonaContratanteId() + "")).append(System.getProperty("line.separator"));
											
						}
						
						//Se revisa si cambio el RFC del contratante
						try {
							clienteSource = clienteFacade.findById(new BigDecimal(cotizacionSource.getValue().getPersonaContratanteId()), "SISTEMA");
							clienteTarget = clienteFacade.findById(new BigDecimal(cotizacionTarget.getValue().getPersonaContratanteId()), "SISTEMA");
							
							if (clienteSource != null && clienteTarget != null) {
								if (clienteSource.getCodigoRFC()== null) {
									if (clienteTarget.getCodigoRFC() != null) {
										cambioRFC = true;
									}
									
								} else {
									cambioRFC = !clienteSource.getCodigoRFC().equals(clienteTarget.getCodigoRFC());
								}
							}
							
							if (cambioRFC) {
								descripcion.append(Utilerias.getMensajeRecurso(
										SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatoscontratante.rfc.descripcion",
										clienteSource.getCodigoRFC(),
										clienteTarget.getCodigoRFC())).append(System.getProperty("line.separator"));
							}
							
							
						} catch (Exception ex) {
							System.out.println("Error al obtener los datos de cliente de Seycos para generar descripcion de movimientos...");
						}
						
						if (!cotizacionTarget.getValue().getNombreContratante().equals(cotizacionSource.getValue().getNombreContratante())) {
							descripcion.append(Utilerias.getMensajeRecurso(
									SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatoscontratante.nombre.descripcion",
									cotizacionSource.getValue().getNombreContratante(),
									cotizacionTarget.getValue().getNombreContratante())).append(System.getProperty("line.separator"));
						}
						
						try {
							if (!cotizacionTarget.getValue().getDomicilioContratanteId().equals(cotizacionSource.getValue().getDomicilioContratanteId())) {
								
									domicilioSource = domicilioFacade.findDomicilioImpresionById(
											cotizacionSource.getValue().getDomicilioContratanteId().longValue());
									
									domicilioTarget = domicilioFacade.findDomicilioImpresionById(
											cotizacionTarget.getValue().getDomicilioContratanteId().longValue());
									
									
									descripcion.append(Utilerias.getMensajeRecurso(
											SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatoscontratante.domicilio.descripcion",
											domicilioSource.getCalleNumero(),
											domicilioSource.getNombreColonia(),
											domicilioSource.getCodigoPostal(),
											domicilioSource.getCiudad(),
											domicilioSource.getEstado(),
											domicilioTarget.getCalleNumero(),
											domicilioTarget.getNombreColonia(),
											domicilioTarget.getCodigoPostal(),
											domicilioTarget.getCiudad(),
											domicilioTarget.getEstado())).append(System.getProperty("line.separator"));
								
							}
						} catch (Exception ex) {
							System.out.println("Error al obtener los datos de domicilio de Seycos para generar descripcion de movimientos...");
						}
												
						if(descripcion.length() > 0) {
							return descripcion.toString();	
						}
						
					}
						
						
				}
				
				break;
				
			}
		}
		
		
		return null;
		
	}
	
	private String generarDescripcionMovimientoInciso (ControlEndosoCot controlEndosoCot, short tipoEndoso, TipoOperacion tipoOperacion, 
			Object source, Object target) {
		
		BitemporalInciso incisoSource = null;
		BitemporalInciso incisoTarget = null;
		
		switch (tipoOperacion) {
			case AGREGADO: {
				
				if (target instanceof BitemporalInciso) {
					
					incisoTarget = (BitemporalInciso) target;
					
					switch (tipoEndoso) {
						case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS:
							return Utilerias.getMensajeRecurso(
									SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.rehabinciso.descripcion",
									incisoTarget.getContinuity().getNumero(),
									getNumeroEndosoBajaInciso(incisoTarget));
							
						case SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA:
							return Utilerias.getMensajeRecurso(
									SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.ajusteprima.inciso.descripcion",
									incisoTarget.getContinuity().getNumero());
							
						case SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO: {
							
							Map<String,Object> properties = new HashMap<String,Object>();
							properties.put("id", controlEndosoCot.getIdCancela());
											
							List<ControlEndosoCot> controles = entidadService.findByProperties(ControlEndosoCot.class, properties);
							
							if (controles != null && controles.size() > 0 && controles.get(0).getNumeroEndosoAjusta() != null) {
								return Utilerias.getMensajeRecurso(
										SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.ajusteprima.inciso.cancelacion.descripcion",
										incisoTarget.getContinuity().getNumero());
							}
							
						}
						
						default:
							return Utilerias.getMensajeRecurso(
									SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.altainciso.descripcion",
									incisoTarget.getContinuity().getNumero());
					}
					
				} else if (target instanceof BitemporalCondicionEspInc && 
						(tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO  || tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES)) {
					StringBuilder descripcion = new StringBuilder("");
					
					BitemporalCondicionEspInc condicionSource = (BitemporalCondicionEspInc) target;					
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.condicionesEspecialesInciso"));
					if (condicionSource.getRecordInterval() == null) {
						CondicionEspecial condicion = entidadService.findById(CondicionEspecial.class, condicionSource.getContinuity().getCondicionEspecialId());
		
						descripcion.append(" ");
						descripcion.append(condicion.getCodigo());
						descripcion.append(" - ");
						descripcion.append(condicion.getNombre());
					}
										
					
					return descripcion.toString();
					}
			}
			
			case BORRADO: {
				if (target instanceof BitemporalCondicionEspInc &&  (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_CONDICIONES)) {
					StringBuilder descripcion = new StringBuilder("");
					
					BitemporalCondicionEspInc condicionSource = (BitemporalCondicionEspInc) target;					
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.condicionesEspecialesInciso"));
					if (condicionSource.getRecordInterval() == null) {
						CondicionEspecial condicion = entidadService.findById(CondicionEspecial.class, condicionSource.getContinuity().getCondicionEspecialId());
		
						descripcion.append(" ");
						descripcion.append(System.getProperty("line.separator"));
						descripcion.append(condicion.getCodigo());
						descripcion.append(" - ");
						descripcion.append(condicion.getNombre());
					}										
					
					return descripcion.toString();
					
				} else {
					incisoSource = (BitemporalInciso) source;
					
					return Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.bajainciso.descripcion",
							incisoSource.getContinuity().getNumero());
				}
				
				
			}
			
			case MODIFICADO: {
				if (!(target instanceof BitemporalAutoInciso)) return " ";
				BitemporalAutoInciso autoIncisoSource = (BitemporalAutoInciso) source;
				BitemporalAutoInciso autoIncisoTarget = (BitemporalAutoInciso) target;
				
				StringBuilder descripcion = new StringBuilder("");
				
				if (autoIncisoTarget.getValue().getNombreAsegurado() != null 
						&& !autoIncisoTarget.getValue().getNombreAsegurado().equals(autoIncisoSource.getValue().getNombreAsegurado())) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatosasegurado.descripcion",
							autoIncisoSource.getValue().getNombreAsegurado(),
							autoIncisoTarget.getValue().getNombreAsegurado())).append(System.getProperty("line.separator"));
				}
				
				if (
						(autoIncisoTarget.getValue().getNombreConductor() != null 
								&& !autoIncisoTarget.getValue().getNombreConductor().equals(autoIncisoSource.getValue().getNombreConductor()))
						||
						(autoIncisoTarget.getValue().getPaternoConductor() != null 
								&& !autoIncisoTarget.getValue().getPaternoConductor().equals(autoIncisoSource.getValue().getPaternoConductor()))
						||
						(autoIncisoTarget.getValue().getMaternoConductor() != null 
								&& !autoIncisoTarget.getValue().getMaternoConductor().equals(autoIncisoSource.getValue().getMaternoConductor()))
				) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatosvehiculo.nombreconductor.descripcion",
							(autoIncisoSource.getValue().getNombreConductor() != null ? autoIncisoSource.getValue().getNombreConductor() : "-"),
							(autoIncisoSource.getValue().getPaternoConductor() != null ? autoIncisoSource.getValue().getPaternoConductor() : ""),
							(autoIncisoSource.getValue().getMaternoConductor() != null ? autoIncisoSource.getValue().getMaternoConductor() : ""),
							autoIncisoTarget.getValue().getNombreConductor(),
							autoIncisoTarget.getValue().getPaternoConductor(),
							autoIncisoTarget.getValue().getMaternoConductor()
					)).append(System.getProperty("line.separator"));
				
				}
				
				if (autoIncisoTarget.getValue().getFechaNacConductor() != null 
						&& !autoIncisoTarget.getValue().getFechaNacConductor().equals(autoIncisoSource.getValue().getFechaNacConductor())) {
					SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatosvehiculo.fechanac.descripcion",
							(autoIncisoSource.getValue().getFechaNacConductor() != null ? formato.format(autoIncisoSource.getValue().getFechaNacConductor()) : "-"),
							formato.format(autoIncisoTarget.getValue().getFechaNacConductor()))).append(System.getProperty("line.separator"));
					
				}
				
				if (autoIncisoTarget.getValue().getOcupacionConductor() != null 
						&& !autoIncisoTarget.getValue().getOcupacionConductor().equals(autoIncisoSource.getValue().getOcupacionConductor())) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatosvehiculo.ocupacion.descripcion",
							(autoIncisoSource.getValue().getOcupacionConductor() != null ? autoIncisoSource.getValue().getOcupacionConductor() : "-"),
							autoIncisoTarget.getValue().getOcupacionConductor())).append(System.getProperty("line.separator"));
					
				}
												
				if (autoIncisoTarget.getValue().getNumeroMotor() != null 
						&& !autoIncisoTarget.getValue().getNumeroMotor().equals(autoIncisoSource.getValue().getNumeroMotor())) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatosvehiculo.nummotor.descripcion",
							autoIncisoSource.getValue().getNumeroMotor(),
							autoIncisoTarget.getValue().getNumeroMotor())).append(System.getProperty("line.separator"));
					
				}
				
				if (autoIncisoTarget.getValue().getNumeroSerie() != null 
						&& !autoIncisoTarget.getValue().getNumeroSerie().equals(autoIncisoSource.getValue().getNumeroSerie())) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatosvehiculo.numserie.descripcion",
							autoIncisoSource.getValue().getNumeroSerie(),
							autoIncisoTarget.getValue().getNumeroSerie())).append(System.getProperty("line.separator"));
					
				}
				
				if (autoIncisoTarget.getValue().getPlaca() != null 
						&& !autoIncisoTarget.getValue().getPlaca().equals(autoIncisoSource.getValue().getPlaca())) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatosvehiculo.numplaca.descripcion",
							autoIncisoSource.getValue().getPlaca(),
							autoIncisoTarget.getValue().getPlaca())).append(System.getProperty("line.separator"));
					
				}
				
				if (autoIncisoTarget.getValue().getDescripcionFinal() != null 
						&& !autoIncisoTarget.getValue().getDescripcionFinal().equals(autoIncisoSource.getValue().getDescripcionFinal())) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatosvehiculo.descfinal.descripcion",
							autoIncisoSource.getValue().getDescripcionFinal(),
							autoIncisoTarget.getValue().getDescripcionFinal())).append(System.getProperty("line.separator"));
					
				}
				
				if (autoIncisoTarget.getValue().getRepuve() != null
						&& !autoIncisoTarget.getValue().getRepuve().equals(autoIncisoSource.getValue().getRepuve())) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatosvehiculo.repuve.descripcion",
							(autoIncisoSource.getValue().getRepuve() != null ? autoIncisoSource.getValue().getRepuve() : "-"),
							autoIncisoTarget.getValue().getRepuve())).append(System.getProperty("line.separator"));
					
				}
				
				if (autoIncisoTarget.getValue().getObservacionesinciso() != null
						&& !autoIncisoTarget.getValue().getObservacionesinciso().equals(autoIncisoSource.getValue().getObservacionesinciso())) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatosvehiculo.observaciones.descripcion",
							(autoIncisoSource.getValue().getObservacionesinciso() != null ? autoIncisoSource.getValue().getObservacionesinciso() : "-"),
							autoIncisoTarget.getValue().getObservacionesinciso())).append(System.getProperty("line.separator"));
					
				}
				
				if (autoIncisoTarget.getValue().getNumeroLicencia() != null
						&& !autoIncisoTarget.getValue().getNumeroLicencia().equals(autoIncisoSource.getValue().getNumeroLicencia())) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatosvehiculo.numlicencia.descripcion",
							(autoIncisoSource.getValue().getNumeroLicencia() != null ? autoIncisoSource.getValue().getNumeroLicencia() : "-"),
							autoIncisoTarget.getValue().getNumeroLicencia())).append(System.getProperty("line.separator"));
					
				}
				
				if (autoIncisoTarget.getValue().getPaquete() != null
						&& !autoIncisoTarget.getValue().getPaquete().equals(autoIncisoSource.getValue().getPaquete())) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiopaquete.descripcion",
							autoIncisoSource.getValue().getPaquete().getDescripcion(),
							autoIncisoTarget.getValue().getPaquete().getDescripcion())).append(System.getProperty("line.separator"));
					
				}
				
				if (autoIncisoTarget.getValue().getModeloVehiculo() != null
						&& !autoIncisoTarget.getValue().getModeloVehiculo().equals(autoIncisoSource.getValue().getModeloVehiculo())) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiomodelo.descripcion",
							autoIncisoSource.getValue().getModeloVehiculo().toString(),
							autoIncisoTarget.getValue().getModeloVehiculo().toString())).append(System.getProperty("line.separator"));
					
				}
				
				if (autoIncisoTarget.getValue().getTipoUsoId() != null
						&& !autoIncisoTarget.getValue().getTipoUsoId().equals(autoIncisoSource.getValue().getTipoUsoId())) {
					try{
					TipoUsoVehiculoDTO tipoUsoSource = entidadService.findById(TipoUsoVehiculoDTO.class, new BigDecimal(autoIncisoSource.getValue().getTipoUsoId()));
					TipoUsoVehiculoDTO tipoUsoTarget = entidadService.findById(TipoUsoVehiculoDTO.class, new BigDecimal(autoIncisoTarget.getValue().getTipoUsoId()));
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiotipouso.descripcion",
							tipoUsoSource.getDescripcionTipoUsoVehiculo(),
							tipoUsoTarget.getDescripcionTipoUsoVehiculo())).append(System.getProperty("line.separator"));
					}catch(Exception e){
					}
				}
				
				if (autoIncisoTarget.getValue().getRutaCirculacion() != null 
						&& !autoIncisoTarget.getValue().getRutaCirculacion().equals(autoIncisoSource.getValue().getRutaCirculacion())) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatosvehiculo.rutaCirculacion.descripcion",
							autoIncisoTarget.getValue().getRutaCirculacion(),
							autoIncisoTarget.getValue().getRutaCirculacion())).append(System.getProperty("line.separator"));
					
				}				
				
				if (autoIncisoTarget.getValue().getTipoAgrupacionRecibosInciso() != null
						&& !autoIncisoTarget.getValue().getTipoAgrupacionRecibosInciso().equals(autoIncisoSource.getValue().getTipoAgrupacionRecibosInciso())) {
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambioTipoAgrupacionRecibos.descripcion",
							CotizacionDTO.TIPO_AGRUPACION_RECIBOS.findByEstatus(autoIncisoTarget.getValue()
									.getTipoAgrupacionRecibosInciso()))).append(System.getProperty("line.separator"));
				}
				
				if(descripcion.length() > 0) {
					return descripcion.toString();	
				}
				
				break;
			}
			
		}
		
		return null;
	}
	
	private String generarDescripcionMovimientoCobertura (ControlEndosoCot controlEndosoCot, short tipoEndoso, TipoOperacion tipoOperacion, 
			Object source, Object target) {
		
		BitemporalCoberturaSeccion coberturaSource = (BitemporalCoberturaSeccion) source;
		BitemporalCoberturaSeccion coberturaTarget = (BitemporalCoberturaSeccion) target;
		
		switch (tipoOperacion) {
			case AGREGADO: {
				
				IncisoContinuity incisoContinuity = coberturaTarget.getContinuity().getSeccionIncisoContinuity().getIncisoContinuity();
				BitemporalInciso bitemporalInciso = getLatestBitemporal(incisoContinuity, BitemporalInciso.class, 
						coberturaTarget.getValidityInterval().getInterval().getStart());
				
				switch (tipoEndoso) {
					case SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA:
						return Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.ajusteprima.cobertura.descripcion",
								coberturaTarget.getContinuity().getCoberturaDTO().getNombreComercial(),
								bitemporalInciso.getContinuity().getNumero());
					
					case SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO: {
						
						Map<String,Object> properties = new HashMap<String,Object>();
						properties.put("id", controlEndosoCot.getIdCancela());
										
						List<ControlEndosoCot> controles = entidadService.findByProperties(ControlEndosoCot.class, properties);
						
						if (controles != null && controles.size() > 0 && controles.get(0).getNumeroEndosoAjusta() != null) {
							return Utilerias.getMensajeRecurso(
									SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.ajusteprima.cobertura.cancelacion.descripcion",
									coberturaTarget.getContinuity().getCoberturaDTO().getNombreComercial(),
									bitemporalInciso.getContinuity().getNumero());
						}
						
					}
					
					default:
						String descripcionMovimientoCobertura = Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.altacobertura.descripcion",
								coberturaTarget.getContinuity().getCoberturaDTO().getNombreComercial(),
								bitemporalInciso.getContinuity().getNumero()); 
						
						if (coberturaTarget.getContinuity().getCoberturaDTO().getClaveFuenteSumaAsegurada().trim().equals("0")) {
							//Suma asegurada proporcionada por el usuario
							descripcionMovimientoCobertura += " " +
							Utilerias.getMensajeRecurso(
									SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.altacobertura.sumaasegurada",
									coberturaTarget.getValue().getValorSumaAsegurada());
						}
						
						return descripcionMovimientoCobertura;
						
				}
				
			}
				
			case BORRADO: {
				
				switch (tipoEndoso) {
					case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO: {
						if(coberturaTarget != null) {
							BitemporalCotizacion cotizacionSource = coberturaTarget.getEntidadContinuity().getParentContinuity()
								.getParentContinuity().getParentContinuity().getBitemporalProperty()
									.get(coberturaTarget.getValidityInterval().getInterval().getStart()); 
							
							BitemporalCotizacion cotizacionTarget = coberturaTarget.getEntidadContinuity().getParentContinuity()
								.getParentContinuity().getParentContinuity().getBitemporalProperty()
									.getInProcess(coberturaTarget.getValidityInterval().getInterval().getStart());
							
							String descripcionSource = getDescripcionFormaPago(cotizacionSource.getValue().getFormaPago().getIdFormaPago(), 
									cotizacionSource.getValue().getMoneda().getIdTcMoneda());
							
							String descripcionTarget = getDescripcionFormaPago(cotizacionTarget.getValue().getFormaPago().getIdFormaPago(), 
									cotizacionTarget.getValue().getMoneda().getIdTcMoneda());
							
							return Utilerias.getMensajeRecurso(
									SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cfp.descripcion",
									descripcionSource,
									descripcionTarget);
						}else{
							return " ";
						}
					}
				
					default: {
				
						DateTime validoEn = TimeUtils.getDateTime(controlEndosoCot.getValidFrom());
						
						String nombreComercial = (coberturaTarget!= null 
								? coberturaTarget.getContinuity().getCoberturaDTO().getNombreComercial()
										:coberturaSource.getContinuity().getCoberturaDTO().getNombreComercial());
						
						IncisoContinuity incisoContinuity = coberturaSource.getContinuity().getSeccionIncisoContinuity().getIncisoContinuity();
						BitemporalInciso bitemporalInciso = getLatestBitemporal(incisoContinuity, BitemporalInciso.class, validoEn);
						
						return Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.bajacobertura.descripcion",
								nombreComercial,
								bitemporalInciso.getContinuity().getNumero());
					}
				}
				
			}
				
			case MODIFICADO: {
				StringBuilder descripcion = new StringBuilder("");
				
				if (coberturaTarget.getValue().getClaveContrato().shortValue() == 1) {
				
					if (coberturaTarget.getContinuity().getCoberturaDTO().getClaveFuenteSumaAsegurada().trim().equals("0")
							&& !coberturaTarget.getValue().getValorSumaAsegurada().equals(coberturaSource.getValue().getValorSumaAsegurada())) {
						
						if(coberturaTarget.getContinuity().getCoberturaDTO().getNombreComercial().toUpperCase().equals(CoberturaDTO.NOMBRE_LIMITE_RC_VIAJERO)){
							descripcion.append(Utilerias.getMensajeRecurso(
									SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodiassalariominimo.descripcion",
									coberturaSource.getValue().getDiasSalarioMinimo(),
									coberturaTarget.getValue().getDiasSalarioMinimo())).append(System.getProperty("line.separator"));
						}else{
							descripcion.append(Utilerias.getMensajeRecurso(
									SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiosumaasegcobertura.descripcion",
									coberturaSource.getValue().getValorSumaAsegurada(),
									coberturaTarget.getValue().getValorSumaAsegurada())).append(System.getProperty("line.separator"));
						}
						
					}
					
					if (coberturaTarget.getContinuity().getCoberturaDTO().getClaveTipoDeducible().trim().equals("1")) {
						
						if (!coberturaTarget.getValue().getPorcentajeDeducible().equals(coberturaSource.getValue().getPorcentajeDeducible())) {
							
							descripcion.append(Utilerias.getMensajeRecurso(
									SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodeduciblecobertura.descripcion",
									coberturaSource.getValue().getPorcentajeDeducible(),
									coberturaTarget.getValue().getPorcentajeDeducible())).append(System.getProperty("line.separator"));
						}
						
					} else {
						
						if (!coberturaTarget.getValue().getValorDeducible().equals(coberturaSource.getValue().getValorDeducible())) {
							
							descripcion.append(Utilerias.getMensajeRecurso(
									SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodeduciblecobertura.descripcion",
									coberturaSource.getValue().getValorDeducible(),
									coberturaTarget.getValue().getValorDeducible())).append(System.getProperty("line.separator"));
						}
						
					}
										
					if (
							(coberturaTarget.getValue().getValorPrimaDiaria() != null &&
							!coberturaTarget.getValue().getValorPrimaDiaria().equals(coberturaSource.getValue().getValorPrimaDiaria()))
							||
							(coberturaSource.getValue().getValorPrimaDiaria() != null && coberturaTarget.getValue().getValorPrimaDiaria() == null)
					) {
						
						descripcion.append(Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambioprimanetacobertura.descripcion",
								NumberFormat.getCurrencyInstance().format(coberturaSource.getValue().getValorPrimaNeta()),
								NumberFormat.getCurrencyInstance().format(coberturaTarget.getValue().getValorPrimaNeta())))
									.append(System.getProperty("line.separator"));
					}
				
				}
				if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO) {
					
					BitemporalCotizacion cotizacionSource = coberturaTarget.getEntidadContinuity().getParentContinuity()
						.getParentContinuity().getParentContinuity().getBitemporalProperty()
							.get(coberturaTarget.getValidityInterval().getInterval().getStart()); 
					
					BitemporalCotizacion cotizacionTarget = coberturaTarget.getEntidadContinuity().getParentContinuity()
						.getParentContinuity().getParentContinuity().getBitemporalProperty()
							.getInProcess(coberturaTarget.getValidityInterval().getInterval().getStart());
					
					String descripcionSource = getDescripcionFormaPago(cotizacionSource.getValue().getFormaPago().getIdFormaPago(), 
							cotizacionSource.getValue().getMoneda().getIdTcMoneda());
					
					String descripcionTarget = getDescripcionFormaPago(cotizacionTarget.getValue().getFormaPago().getIdFormaPago(), 
							cotizacionTarget.getValue().getMoneda().getIdTcMoneda());
					
					descripcion.append(Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cfp.descripcion",
							descripcionSource,
							descripcionTarget))
							.append(System.getProperty("line.separator"));
				}
				
				if (descripcion.length() > 0) {
					
					IncisoContinuity incisoContinuity = coberturaTarget.getContinuity().getSeccionIncisoContinuity().getIncisoContinuity();
					BitemporalInciso bitemporalInciso = getLatestBitemporal(incisoContinuity, BitemporalInciso.class, 
							coberturaTarget.getValidityInterval().getInterval().getStart());
					
					descripcion.insert(0, Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.movimiento.endoso.cambiodatoscobertura.descripcion",
							coberturaTarget.getContinuity().getCoberturaDTO().getNombreComercial(),
							bitemporalInciso.getContinuity().getNumero()));
				}
				
				if(descripcion.length() > 0) {
					return descripcion.toString();	
				}
				break;
			}
				
		}
		
		return null;
	}
	
	
	/**
	 * XXX Calculo de movimientos
	 */
	
	private void calculaMovimiento(MovimientoEndoso movimiento, AuxiliarCalculoDTO auxiliar, boolean valorDerechosCero) {
		
		ControlEndosoCot controlEndosoCot = movimiento.getControlEndosoCot();
		MovimientoEndoso movimientoInverso = null;
		BitemporalCoberturaSeccion coberturaSource = null;
		BitemporalCoberturaSeccion coberturaTarget = null;
		Double porcentajeComision = null;
		Double primaDiaria = 0D;
		BigDecimal valorTotalPrimas = CERO;
		BigDecimal valorRPF = CERO;
		BigDecimal valorComision = CERO;
		BigDecimal valorComisionRPF = CERO;
		BigDecimal valorBonificacionComision = CERO;
		BigDecimal valorBonificacionComisionRPF = CERO;
		BigDecimal valorDerechos = CERO;
		BigDecimal valorIva = CERO;
		BigDecimal valorSobreComisionAgente = CERO;
		BigDecimal valorSobreComisionProm = CERO;
		BigDecimal valorBonoAgente = CERO;
		BigDecimal valorBonoProm = CERO;
		BigDecimal valorCesionDerechosAgente = CERO;
		BigDecimal valorCesionDerechosProm = CERO;
		BigDecimal valorSobreComisionUDIAgente = CERO;
		BigDecimal valorSobreComisionUDIProm = CERO;
		
		BigDecimal valorSobreComision = CERO;
		BigDecimal valorBono = CERO;
		BigDecimal valorCesionDerechos = CERO;
		BigDecimal valorSobreComisionUDI = CERO;
		
		short tipoEndoso = controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue();
		boolean calcularConceptos = true;
		Object source = movimiento.getFuenteMovimiento().getSource();
		Object target = movimiento.getFuenteMovimiento().getTarget();
		TipoOperacion tipoOperacion = getTipoOperacion (source, target, tipoEndoso);
				
		if (target != null && target instanceof MovimientoEndoso) {
			movimientoInverso = (MovimientoEndoso) target;
		}
		
		if (movimiento.getTipo().equals(TipoMovimientoEndoso.COBERTURA)) {
			//Se obtiene el Porcentaje de Comision
			if (auxiliar.getPorcentajeComision() != null) {
				porcentajeComision = auxiliar.getPorcentajeComision();
			} else {
				porcentajeComision = this.getPorcentajeComision(auxiliar.getComisiones(), 
						movimiento.getBitemporalCoberturaSeccion().getContinuity().getCoberturaDTO());
			}
			
			//Comienza el calculo de los conceptos
			
			switch (tipoEndoso) {
				case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO: {
					
					valorRPF = getValorRPFCambioFormaPago(movimiento.getBitemporalCoberturaSeccion(), auxiliar);
					
					//Calcular tambien ComisionRPF cuando se trata de endoso de CFP
					valorComisionRPF = valorRPF.multiply(new BigDecimal(porcentajeComision/100D), mathCtxFactor);
					
					valorBonificacionComisionRPF = valorComisionRPF.multiply(new BigDecimal(auxiliar.getPorcentajeBonificacionComision()/ 100D), mathCtxFactor);
					
					//Calcular tambien IVA cuando se trata de endoso de CFP
					valorIva = (valorTotalPrimas.subtract(valorBonificacionComision).add(valorDerechos).add(valorRPF).subtract(valorBonificacionComisionRPF))
					.multiply(new BigDecimal(auxiliar.getPorcentajeIVA() / 100D));
					
					break;
				}
				case SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA: {
					
					if (movimientoInverso.getControlEndosoCot().getSolicitud()
							.getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA) {
						
						//Si se trata del calculo de los incisos ajustados para el EAP
						valorTotalPrimas = movimientoInverso.getValorMovimiento();
						
					} else {
						
						calcularConceptos = false;
						
						valorDerechos = movimiento.getMovimientoEndoso().getValorDerechos().divide(new BigDecimal(getNumeroMovsCoberturaEnInciso(movimiento)), 
								mathCtxFactor);
						valorIva = (movimiento.getMovimientoEndoso().getValidacionInciso().getIva().doubleValue() > 0 ? UNO.negate() : UNO);
					
					}
					
					break;
				}
				default: {
					if (movimientoInverso != null) {
						
						if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO) {
							//Cancelacion de EAP
							/**
							 -El valor de prima diaria del Endoso EAP se debe obtener con el producto del valor del movimiento 
							 (prima neta o Total de Primas) y su respectivo factor de igualacion, dividido entre los dias de vigencia del endoso EAP.

							 -Obtener el inverso del producto del valor de prima diaria por los dias de vigencia del endoso de Cancelacion de Endoso. 
							 Este será el nuevo valor de Total de Primas para el endoso de Cancelacion de Endoso.
							 */
							Integer diferenciaDias = Days.daysBetween(
									TimeUtils.getDateTime(movimientoInverso.getControlEndosoCot().getValidFrom()), 
									TimeUtils.getDateTime(controlEndosoCot.getValidFrom())).getDays();
							
							Integer diasVigenciaEAP = auxiliar.getDiasVigencia() + diferenciaDias;
							
							BigDecimal primaDiariaEAP = (movimientoInverso.getValorMovimiento().multiply(movimientoInverso.getFactorCobranzaPrimaNeta()))
								.setScale(16, BigDecimal.ROUND_HALF_UP).divide(new BigDecimal(diasVigenciaEAP), mathCtxFactor);
							
							valorTotalPrimas = primaDiariaEAP.multiply(new BigDecimal(auxiliar.getDiasVigencia())).negate();
							
					} else {
						// Rehabilitaciones
						calcularConceptos = false;				

						valorTotalPrimas = movimientoInverso.getValorMovimiento().negate();
						valorRPF = movimientoInverso.getValorRPF().negate();
						valorComision = movimientoInverso.getValorComision().negate();
						valorComisionRPF = movimientoInverso.getValorComisionRPF().negate();
						valorBonificacionComision = movimientoInverso.getValorBonificacionComision().negate();
						valorBonificacionComisionRPF = movimientoInverso.getValorBonificacionRPF().negate();
						valorDerechos = movimientoInverso.getValorDerechos().negate();
						valorIva = movimientoInverso.getValorIva().negate();
						valorSobreComisionAgente = movimientoInverso.getValorSobreComisionAgente().negate();
						valorSobreComisionProm = movimientoInverso.getValorSobreComisionProm().negate();
						valorBonoAgente = movimientoInverso.getValorBonoAgente().negate();
						valorBonoProm = movimientoInverso.getValorBonoProm().negate();
						valorCesionDerechosAgente = movimientoInverso.getValorCesionDerechosAgente().negate();
						valorCesionDerechosProm = movimientoInverso.getValorCesionDerechosProm().negate();
						valorSobreComisionUDIAgente = movimientoInverso.getValorSobreComisionUDIAgente().negate();
						valorSobreComisionUDIProm = movimientoInverso.getValorSobreComisionUDIProm().negate();					
						   
						// Se asignan al movimiento los factores de cobranza
						movimiento.setFactorCobranzaPrimaNeta(movimientoInverso.getFactorCobranzaPrimaNeta());
						movimiento.setFactorCobranzaRPF(movimientoInverso.getFactorCobranzaRPF());
						movimiento.setFactorCobranzaComision(movimientoInverso.getFactorCobranzaComision());
						movimiento.setFactorCobranzaComisionRPF(movimientoInverso.getFactorCobranzaComisionRPF());
						movimiento.setFactorCobranzaBonificacionComision(movimientoInverso.getFactorCobranzaBonificacionComision());
						movimiento.setFactorCobranzaBonificacionRPF(movimientoInverso.getFactorCobranzaBonificacionRPF());
						movimiento.setFactorCobranzaDerechos(movimientoInverso.getFactorCobranzaDerechos());
						movimiento.setFactorCobranzaIva(movimientoInverso.getFactorCobranzaIva());
						movimiento.setFactorCobranzaSobreComisionAgente(movimientoInverso.getFactorCobranzaSobreComisionAgente());
						movimiento.setFactorCobranzaSobreComisionProm(movimientoInverso.getFactorCobranzaSobreComisionProm());
						movimiento.setFactorCobranzaBonoAgente(movimientoInverso.getFactorCobranzaBonoAgente());
						movimiento.setFactorCobranzaBonoProm(movimientoInverso.getFactorCobranzaBonoProm());
						movimiento.setFactorCobranzaCesionDerechosAgente(movimientoInverso.getFactorCobranzaCesionDerechosAgente());
						movimiento.setFactorCobranzaCesionDerechosProm(movimientoInverso.getFactorCobranzaCesionDerechosProm());
						movimiento.setFactorCobranzaSobreComisionUDIAgente(movimientoInverso.getFactorCobranzaSobreComisionUDIAgente());
						movimiento.setFactorCobranzaSobreComisionUDIProm(movimientoInverso.getFactorCobranzaSobreComisionUDIProm());						

					}
					} else {
						
						if (tipoOperacion.equals(TipoOperacion.MODIFICADO)) {
							coberturaSource = (BitemporalCoberturaSeccion) getBitemporalObject(source);
							coberturaTarget = (BitemporalCoberturaSeccion) getBitemporalObject(target);
							
							primaDiaria = (coberturaTarget.getValue().getValorPrimaDiaria()!= null?coberturaTarget.getValue().getValorPrimaDiaria(): 0) - 
								(coberturaSource.getValue().getValorPrimaDiaria()!= null?coberturaSource.getValue().getValorPrimaDiaria():0);
						
						} else if (tipoOperacion.equals(TipoOperacion.BORRADO)) {
							
							
								if(movimiento.getFuenteMovimiento().getTarget() != null && movimiento.getFuenteMovimiento().getSource() instanceof BitemporalCoberturaSeccion)
								{
									primaDiaria = (((BitemporalCoberturaSeccion)movimiento.getFuenteMovimiento().getSource()).getValue().getValorPrimaDiaria()!=null
											?((BitemporalCoberturaSeccion)movimiento.getFuenteMovimiento().getSource()).getValue().getValorPrimaDiaria():0) * -1;
									
								}else
								{
									primaDiaria = (movimiento.getBitemporalCoberturaSeccion().getValue().getValorPrimaDiaria()!=null
											?movimiento.getBitemporalCoberturaSeccion().getValue().getValorPrimaDiaria():0) * -1;								
								}							
	
						} else {
							primaDiaria = (movimiento.getBitemporalCoberturaSeccion().getValue().getValorPrimaDiaria()!=null
									?movimiento.getBitemporalCoberturaSeccion().getValue().getValorPrimaDiaria():0);
						}
						
						valorTotalPrimas = new BigDecimal(primaDiaria * auxiliar.getDiasVigencia());
						
					}
					break;
				}
			}
			
			if (calcularConceptos && !valorTotalPrimas.equals(BigDecimal.ZERO)) {
				BigDecimal factorIgualacion = null;
				try{
				if(movimiento.getMovimientoEndoso() != null && movimiento.getMovimientoEndoso().getFuenteMovimiento() != null && 
						movimiento.getMovimientoEndoso().getFuenteMovimiento().getValidacionInciso() != null){
					factorIgualacion = movimiento.getMovimientoEndoso().getFuenteMovimiento().getValidacionInciso().getFactorDctoIgualacion();
				}
				}catch(Exception e){
					
				}
				valorComision = valorTotalPrimas.multiply(new BigDecimal(porcentajeComision/100D), mathCtxFactor);
								
				valorBonificacionComision = valorComision.multiply(new BigDecimal(auxiliar.getPorcentajeBonificacionComision() / 100), mathCtxFactor);
				if(factorIgualacion != null && factorIgualacion.compareTo(new BigDecimal(0)) != 0){
					if(factorIgualacion.compareTo(new BigDecimal(0)) <= 0){
						valorBonificacionComision = valorBonificacionComision.multiply(new BigDecimal(-1));
					}
				}
				//valorBonificacionComision = valorBonificacionComision.abs().multiply(new BigDecimal(valorTotalPrimas.signum())); 
				
				
				if (tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO) {
					valorRPF = valorTotalPrimas.multiply(new BigDecimal(auxiliar.getPorcentajeFormaPago()/100D), mathCtxFactor);
					
					valorComisionRPF = valorRPF.multiply(new BigDecimal(porcentajeComision/100D), mathCtxFactor);
				}

				valorBonificacionComisionRPF = valorComisionRPF.multiply(new BigDecimal(auxiliar.getPorcentajeBonificacionComision()/ 100D), mathCtxFactor);
				if(factorIgualacion != null && factorIgualacion.compareTo(new BigDecimal(0)) != 0){
					if(factorIgualacion.compareTo(new BigDecimal(0)) <= 0){
						valorBonificacionComisionRPF = valorBonificacionComisionRPF.multiply(new BigDecimal(-1));
					}
				}
				//valorBonificacionComisionRPF = valorBonificacionComisionRPF.abs().multiply(new BigDecimal(valorTotalPrimas.signum()));
				
				valorDerechos = movimiento.getMovimientoEndoso().getValorDerechos().divide(new BigDecimal(getNumeroMovsCoberturaEnInciso(movimiento)), 
						mathCtxFactor);
				
				valorIva = (valorTotalPrimas.subtract(valorBonificacionComision).add(valorDerechos).add(valorRPF).subtract(valorBonificacionComisionRPF))
					.multiply(new BigDecimal(auxiliar.getPorcentajeIVA() / 100D));
				
				
				//SobreComisiones
				valorSobreComision = valorTotalPrimas.multiply((auxiliar.getPorcentajeSobreComision().divide(CIEN, mathCtxFactor)), mathCtxFactor)
					.multiply(UNO.subtract(auxiliar.getPorcentajeSobreComisionUDI()).divide(CIEN, mathCtxFactor), mathCtxFactor); 
			
				valorSobreComisionAgente = valorSobreComision.multiply(auxiliar.getPorcentajeSobreComisionAgente().divide(CIEN, mathCtxFactor), mathCtxFactor);
				valorSobreComisionProm = valorSobreComision.multiply(auxiliar.getPorcentajeSobreComisionProm().divide(CIEN, mathCtxFactor), mathCtxFactor);
			
				valorBono = valorTotalPrimas.multiply((auxiliar.getPorcentajeBono().divide(CIEN, mathCtxFactor)), mathCtxFactor)
					.multiply(UNO.subtract(auxiliar.getPorcentajeSobreComisionUDI()).divide(CIEN, mathCtxFactor), mathCtxFactor); 
			
				valorBonoAgente = valorBono.multiply(auxiliar.getPorcentajeBonoAgente().divide(CIEN, mathCtxFactor), mathCtxFactor);
				valorBonoProm = valorBono.multiply(auxiliar.getPorcentajeBonoProm().divide(CIEN, mathCtxFactor), mathCtxFactor);
		
				valorSobreComisionUDI = valorTotalPrimas.multiply((auxiliar.getPorcentajeSobreComisionUDI().divide(CIEN, mathCtxFactor)), mathCtxFactor); 
			
				valorSobreComisionUDIAgente = valorSobreComisionUDI.multiply(auxiliar.getPorcentajeSobreComisionUDIAgente().divide(CIEN, mathCtxFactor), mathCtxFactor);
				valorSobreComisionUDIProm = valorSobreComisionUDI.multiply(auxiliar.getPorcentajeSobreComisionUDIProm().divide(CIEN, mathCtxFactor), mathCtxFactor);
						
				if (auxiliar.getPorcentajeCesionDerechos() != null) {
					valorCesionDerechos = valorDerechos.multiply(auxiliar.getPorcentajeCesionDerechos(), mathCtxFactor);
				} else if (auxiliar.getImporteCesionDerechos() != null) {
					if (auxiliar.getImporteCesionDerechos().compareTo(valorDerechos) <= 0) {
						valorCesionDerechos = auxiliar.getImporteCesionDerechos(); 
					} else {
						valorCesionDerechos = valorDerechos;
					}
				}
				
				valorCesionDerechosAgente = valorCesionDerechos.multiply((auxiliar.getPorcentajeCesionDerechosAgente().divide(CIEN, mathCtxFactor)), mathCtxFactor);
				valorCesionDerechosProm = valorCesionDerechos.multiply((auxiliar.getPorcentajeCesionDerechosProm().divide(CIEN, mathCtxFactor)), mathCtxFactor);
				
			}
									
			//Se asignan al movimiento los valores primarios
			movimiento.setValorMovimiento(valorTotalPrimas);
			movimiento.setValorRPF(valorRPF);
			movimiento.setValorComision(valorComision);
			movimiento.setValorComisionRPF(valorComisionRPF);
			movimiento.setValorBonificacionComision(valorBonificacionComision);
			movimiento.setValorBonificacionRPF(valorBonificacionComisionRPF);
			movimiento.setValorDerechos(valorDerechos);
			movimiento.setValorIva(valorIva);
			movimiento.setValorSobreComisionAgente(valorSobreComisionAgente);
			movimiento.setValorSobreComisionProm(valorSobreComisionProm);
			movimiento.setValorBonoAgente(valorBonoAgente);
			movimiento.setValorBonoProm(valorBonoProm);
			movimiento.setValorCesionDerechosAgente(valorCesionDerechosAgente);
			movimiento.setValorCesionDerechosProm(valorCesionDerechosProm);
			movimiento.setValorSobreComisionUDIAgente(valorSobreComisionUDIAgente);
			movimiento.setValorSobreComisionUDIProm(valorSobreComisionUDIProm);
			
			//Se acumulan los contadores de las coberturas con conceptos diferentes a cero
			incrementaContadorCoberturasConcepto(movimiento, Concepto.PRIMA_NETA, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.RPF, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.COMISION, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.COMISION_RPF, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.BONIFICACION_COMISION, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.BONIFICACION_RPF, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.DERECHOS, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.IVA, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.SOBRECOMISIONAGENTE, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.SOBRECOMISIONPROM, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.BONOAGENTE, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.BONOPROM, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.CESIONDERECHOSAGENTE, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.CESIONDERECHOSPROM, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.SOBRECOMISIONUDIAGENTE, 1);
			incrementaContadorCoberturasConcepto(movimiento, Concepto.SOBRECOMISIONUDIPROM, 1);
						
			//Se acumulan los valores primarios a nivel de inciso
			incrementaValorConceptoInciso(movimiento, Concepto.PRIMA_NETA, null);
			incrementaValorConceptoInciso(movimiento, Concepto.RPF, null);
			incrementaValorConceptoInciso(movimiento, Concepto.COMISION, null);
			incrementaValorConceptoInciso(movimiento, Concepto.COMISION_RPF, null);
			incrementaValorConceptoInciso(movimiento, Concepto.BONIFICACION_COMISION, null);
			incrementaValorConceptoInciso(movimiento, Concepto.BONIFICACION_RPF, null);
			incrementaValorConceptoInciso(movimiento, Concepto.IVA, null);
			incrementaValorConceptoInciso(movimiento, Concepto.SOBRECOMISIONAGENTE, null);
			incrementaValorConceptoInciso(movimiento, Concepto.SOBRECOMISIONPROM, null);
			incrementaValorConceptoInciso(movimiento, Concepto.BONOAGENTE, null);
			incrementaValorConceptoInciso(movimiento, Concepto.BONOPROM, null);
			incrementaValorConceptoInciso(movimiento, Concepto.CESIONDERECHOSAGENTE, null);
			incrementaValorConceptoInciso(movimiento, Concepto.CESIONDERECHOSPROM, null);
			incrementaValorConceptoInciso(movimiento, Concepto.SOBRECOMISIONUDIAGENTE, null);
			incrementaValorConceptoInciso(movimiento, Concepto.SOBRECOMISIONUDIPROM, null);
			
		} else if (movimiento.getTipo().equals(TipoMovimientoEndoso.INCISO)) {
			
			if(valorDerechosCero){
				valorDerechos = BigDecimal.ZERO;
			}else{
				if (movimientoInverso != null) {
					valorDerechos = getValorDerechos(movimientoInverso, controlEndosoCot, auxiliar);
				} else {
					valorDerechos = getValorDerechos(movimiento, controlEndosoCot, auxiliar);
				}	
			}
			
			movimiento.setValorDerechos(valorDerechos);
			
			//Se calculan sus movimientos de cobertura
			for (MovimientoEndoso movimientoCobertura : movimiento.getMovimientosEndoso()) {
				calculaMovimiento(movimientoCobertura, auxiliar, valorDerechosCero);
			}
			
			//Se calculan los factores Cobranza a nivel inciso
			calculaFactoresCobranzaInciso(movimiento);
			
			
			if(tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA 
					|| tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO || 
					tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS) //Nota:Cuando se trata de un endoso de Rehabilitacion los factores ya vienen establecidos.
			{
				// Se asignan al movimiento los factores de cobranza
				movimiento.setFactorCobranzaPrimaNeta(movimientoInverso.getFactorCobranzaPrimaNeta());
				movimiento.setFactorCobranzaRPF(movimientoInverso.getFactorCobranzaRPF());
				movimiento.setFactorCobranzaComision(movimientoInverso.getFactorCobranzaComision());
				movimiento.setFactorCobranzaComisionRPF(movimientoInverso.getFactorCobranzaComisionRPF());
				movimiento.setFactorCobranzaBonificacionComision(movimientoInverso.getFactorCobranzaBonificacionComision());
				movimiento.setFactorCobranzaBonificacionRPF(movimientoInverso.getFactorCobranzaBonificacionRPF());
				movimiento.setFactorCobranzaDerechos(movimientoInverso.getFactorCobranzaDerechos());
				movimiento.setFactorCobranzaIva(movimientoInverso.getFactorCobranzaIva());
				movimiento.setFactorCobranzaSobreComisionAgente(movimientoInverso.getFactorCobranzaSobreComisionAgente());
				movimiento.setFactorCobranzaSobreComisionProm(movimientoInverso.getFactorCobranzaSobreComisionProm());
				movimiento.setFactorCobranzaBonoAgente(movimientoInverso.getFactorCobranzaBonoAgente());
				movimiento.setFactorCobranzaBonoProm(movimientoInverso.getFactorCobranzaBonoProm());
				movimiento.setFactorCobranzaCesionDerechosAgente(movimientoInverso.getFactorCobranzaCesionDerechosAgente());
				movimiento.setFactorCobranzaCesionDerechosProm(movimientoInverso.getFactorCobranzaCesionDerechosProm());
				movimiento.setFactorCobranzaSobreComisionUDIAgente(movimientoInverso.getFactorCobranzaSobreComisionUDIAgente());
				movimiento.setFactorCobranzaSobreComisionUDIProm(movimientoInverso.getFactorCobranzaSobreComisionUDIProm());								
			}
			else
			{
				//Se calculan los factores Cobranza a nivel cobertura
				for (MovimientoEndoso movimientoCobertura : movimiento.getMovimientosEndoso()) {
					
					if(tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL 
							|| tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL )
					{
						// Se calculan los factores usando el mismo metodo que calcula a nivel inciso
						
						// OFAL: 
						// Se forza a que los movimientos respeten el signo contrario al devuelto por stp_valida_endoso_pt, 
						// cubriendo casos especiales con endosos de Ajuste de Primas.
						// se hace en este punto para aislar el efecto deseado unicamente a los endosos de PT
						
						forzarSignosConceptosValidacionesInciso(movimientoCobertura, true);
						
						calculaFactoresCobranzaInciso(movimientoCobertura);
						
					}else
					{
						if (movimientoCobertura.getBitemporalCoberturaSeccion().getContinuity().getCoberturaDTO().getCoberturaEsPropia().equals("1") ||
							 controlEndosoCot.getSolicitud().getProductoDTO().getCodigo().equals("32")){
							calculaFactoresCobranzaCobertura(movimientoCobertura);
						}
					}										
				}				
			}
			
			
		} else if (movimiento.getTipo().equals(TipoMovimientoEndoso.POLIZA)) {
			//Se calculan sus movimientos de inciso
			for (MovimientoEndoso movimientoInciso : movimiento.getMovimientosEndoso()) {
				calculaMovimiento(movimientoInciso, auxiliar, valorDerechosCero);
			}
		}
		
	}
	
	/**
	 * Obtiene el valor de Total de Primas a partir de la Prima Total
	 * @param auxiliar Objeto con datos de ayuda para el calculo
	 * @param primaTotal Prima Total de la cual parte el calculo inverso
	 * @param valorDerechosEAP Valor de derechos a considerar para el calculo (Actualmente no se considera, viene con un valor de CERO)
	 * @return El valor de Total de Primas
	 */
	private BigDecimal calculoInverso (AuxiliarCalculoDTO auxiliar, BigDecimal primaTotal, BigDecimal valorDerechosEAP) {
		
		BigDecimal valorTotalPrimas = null;
		BigDecimal valorRPF = null;
		BigDecimal valorDerechos = valorDerechosEAP;
		BigDecimal valorIva = null;
		
		BigDecimal temp = null;
		BigDecimal porcentajeBonificacionComisionReal = null;
		
		valorIva = primaTotal.subtract(primaTotal.divide(UNO.add(new BigDecimal(auxiliar.getPorcentajeIVA() / 100D)), mathCtxFactor))
			.setScale(2, BigDecimal.ROUND_HALF_UP);
		
		temp = primaTotal.subtract(valorIva);
		
		temp = temp.subtract(valorDerechos);
		
		valorRPF = temp.subtract(temp.divide(UNO.add(new BigDecimal(auxiliar.getPorcentajeFormaPago() / 100D)), mathCtxFactor))
			.setScale(2, BigDecimal.ROUND_HALF_UP);
		
		temp = temp.subtract(valorRPF);
				
		
		porcentajeBonificacionComisionReal = new BigDecimal(auxiliar.getPorcentajeComision()/100D)
			.multiply(auxiliar.getPorcentajeUDI().divide(CIEN, mathCtxFactor), mathCtxFactor)
			.multiply(new BigDecimal(auxiliar.getPorcentajeBonificacionComision()/100D), mathCtxFactor);
		
				
		valorTotalPrimas = temp.divide(UNO.subtract(porcentajeBonificacionComisionReal), mathCtxFactor)
			.setScale(2, BigDecimal.ROUND_HALF_UP);
		
		
		return valorTotalPrimas;
		
	}
	
	
	/**
	 * XXX Igualacion de movimientos
	 */
	private void calculaFactoresCobranzaInciso(MovimientoEndoso movimientoInciso) {
		
		if (movimientoInciso.getValidacionInciso() == null) {
			return;
		}
		
		
		BigDecimal factorCobranzaPrimaNeta = UNO;
		BigDecimal factorCobranzaRPF = UNO;
		BigDecimal factorCobranzaBonificacionComision = UNO;
		BigDecimal factorCobranzaBonificacionRPF = UNO;
		BigDecimal factorCobranzaComision = UNO;
		BigDecimal factorCobranzaComisionRPF = UNO;
		BigDecimal factorCobranzaDerechos = UNO;
		BigDecimal factorCobranzaIva = UNO;
		BigDecimal factorCobranzaSobreComisionAgente = UNO;
		BigDecimal factorCobranzaSobreComisionProm = UNO;
		BigDecimal factorCobranzaBonoAgente = UNO;
		BigDecimal factorCobranzaBonoProm = UNO;
		BigDecimal factorCobranzaCesionDerechosAgente = UNO;
		BigDecimal factorCobranzaCesionDerechosProm = UNO;
		BigDecimal factorCobranzaSobreComisionUDIAgente = UNO;
		BigDecimal factorCobranzaSobreComisionUDIProm = UNO;
		
		Boolean ajusteValorCeroPrimaNeta = false;
		Boolean ajusteValorCeroRPF = false;
		Boolean ajusteValorCeroBonificacionComision = false;
		Boolean ajusteValorCeroBonificacionRPF = false;
		Boolean ajusteValorCeroComision = false;
		Boolean ajusteValorCeroComisionRPF = false;
		Boolean ajusteValorCeroDerechos = false;
		Boolean ajusteValorCeroSobreComisionAgente = false;
		Boolean ajusteValorCeroSobreComisionProm = false;
		Boolean ajusteValorCeroBonoAgente = false;
		Boolean ajusteValorCeroBonoProm = false;
		Boolean ajusteValorCeroCesionDerechosAgente = false;
		Boolean ajusteValorCeroCesionDerechosProm = false;
		Boolean ajusteValorCeroSobreComisionUDIAgente = false;
		Boolean ajusteValorCeroSobreComisionUDIProm = false;
		
		EndosoIDTO validacionInciso = movimientoInciso.getValidacionInciso();
		
		//Se revisa si el calculo es de tipo S (Seycos) o M (Midas) para determinar el factorIgualacionCobranza
		if (validacionInciso.getCalculo().trim().equals(calculoSeycos)) {
			
			ContenedorPrimasMovimientoDTO contenedor = calculaValoresMovimientoIncisoCoberturasPropias(movimientoInciso);
			
			if (movimientoInciso.getValorMovimiento().compareTo(CERO) == 0
					&& !validacionInciso.getPrimaNeta().equals(0D)) {
				movimientoInciso.setValorMovimiento(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getPrimaNeta()));
				ajusteValorCeroPrimaNeta = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.PRIMA_NETA, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorMovimiento().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getPrimaNeta()).subtract(contenedor.getValorMovimientoExternas());
				BigDecimal valor2 = movimientoInciso.getValorMovimiento().subtract(contenedor.getValorMovimientoExternas());
				factorCobranzaPrimaNeta = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaPrimaNeta = new BigDecimal(validacionInciso.getPrimaNeta()).divide(movimientoInciso.getValorMovimiento(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorRPF().compareTo(CERO) == 0
					&& !validacionInciso.getRecargoPF().equals(0D)) {
				movimientoInciso.setValorRPF(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getRecargoPF()));
				ajusteValorCeroRPF = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.RPF, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorRPF().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getRecargoPF()).subtract(contenedor.getValorRPFExternas());
				BigDecimal valor2 = movimientoInciso.getValorRPF().subtract(contenedor.getValorRPFExternas());
				factorCobranzaRPF = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaRPF = new BigDecimal(validacionInciso.getRecargoPF()).divide(movimientoInciso.getValorRPF(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorBonificacionComision().compareTo(CERO) == 0
					&& !validacionInciso.getBonificacionComision().equals(0D)) {
				movimientoInciso.setValorBonificacionComision(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getBonificacionComision()));
				ajusteValorCeroBonificacionComision = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.BONIFICACION_COMISION, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorBonificacionComision().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getBonificacionComision()).subtract(contenedor.getValorBonificacionComisionExternas());
				BigDecimal valor2 = movimientoInciso.getValorBonificacionComision().subtract(contenedor.getValorBonificacionComisionExternas());
				factorCobranzaBonificacionComision = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaBonificacionComision = new BigDecimal(validacionInciso.getBonificacionComision()).divide(movimientoInciso.getValorBonificacionComision(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorBonificacionRPF().compareTo(CERO) == 0
					&& !validacionInciso.getBonificacionComisionRPF().equals(0D)) {
				movimientoInciso.setValorBonificacionRPF(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getBonificacionComisionRPF()));
				ajusteValorCeroBonificacionRPF = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.BONIFICACION_RPF, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorBonificacionRPF().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getBonificacionComisionRPF()).subtract(contenedor.getValorBonificacionRPFExternas());
				BigDecimal valor2 = movimientoInciso.getValorBonificacionRPF().subtract(contenedor.getValorBonificacionRPFExternas());
				factorCobranzaBonificacionRPF = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaBonificacionRPF = new BigDecimal(validacionInciso.getBonificacionComisionRPF()).divide(movimientoInciso.getValorBonificacionRPF(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorComision().compareTo(CERO) == 0
					&& !validacionInciso.getComision().equals(0D)) {
				movimientoInciso.setValorComision(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getComision()));
				ajusteValorCeroComision = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.COMISION, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorComision().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getComision()).subtract(contenedor.getValorComisionExternas());
				BigDecimal valor2 = movimientoInciso.getValorComision().subtract(contenedor.getValorComisionExternas());
				factorCobranzaComision = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaComision = new BigDecimal(validacionInciso.getComision()).divide(movimientoInciso.getValorComision(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorComisionRPF().compareTo(CERO) == 0
					&& !validacionInciso.getComisionRPF().equals(0D)) {
				movimientoInciso.setValorComisionRPF(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getComisionRPF()));
				ajusteValorCeroComisionRPF = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.COMISION_RPF, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorComisionRPF().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getComisionRPF()).subtract(contenedor.getValorComisionRPFExternas());
				BigDecimal valor2 = movimientoInciso.getValorComisionRPF().subtract(contenedor.getValorComisionRPFExternas());
				factorCobranzaComisionRPF = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaComisionRPF = new BigDecimal(validacionInciso.getComisionRPF()).divide(movimientoInciso.getValorComisionRPF(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorDerechos().compareTo(CERO) == 0
					&& !validacionInciso.getDerechos().equals(0D)) {
				movimientoInciso.setValorDerechos(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getDerechos()));
				ajusteValorCeroDerechos = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.DERECHOS, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorDerechos().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getDerechos()).subtract(contenedor.getValorDerechosExternas());
				BigDecimal valor2 = movimientoInciso.getValorDerechos().subtract(contenedor.getValorDerechosExternas());
				factorCobranzaDerechos = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaDerechos = new BigDecimal(validacionInciso.getDerechos()).divide(movimientoInciso.getValorDerechos(), mathCtxFactor).abs();
			}
			
			//No se contempla ajusteValorCero para Iva (nunca deberia ser cero)
			if (movimientoInciso.getValorIva().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getIva()).subtract(contenedor.getValorIvaExternas());
				BigDecimal valor2 = movimientoInciso.getValorIva().subtract(contenedor.getValorIvaExternas());
				factorCobranzaIva = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaIva = new BigDecimal(validacionInciso.getIva()).divide(movimientoInciso.getValorIva(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorSobreComisionAgente().compareTo(CERO) == 0
					&& !validacionInciso.getSobreComisionAgente().equals(0D)) {
				movimientoInciso.setValorSobreComisionAgente(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getSobreComisionAgente()));
				ajusteValorCeroSobreComisionAgente = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.SOBRECOMISIONAGENTE, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorSobreComisionAgente().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getSobreComisionAgente()).subtract(contenedor.getValorSobreComisionAgenteExternas());
				BigDecimal valor2 = movimientoInciso.getValorSobreComisionAgente().subtract(contenedor.getValorSobreComisionAgenteExternas());
				factorCobranzaSobreComisionAgente = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaSobreComisionAgente = new BigDecimal(validacionInciso.getSobreComisionAgente()).divide(movimientoInciso.getValorSobreComisionAgente(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorSobreComisionProm().compareTo(CERO) == 0
					&& !validacionInciso.getSobreComisionProm().equals(0D)) {
				movimientoInciso.setValorSobreComisionProm(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getSobreComisionProm()));
				ajusteValorCeroSobreComisionProm = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.SOBRECOMISIONPROM, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorSobreComisionProm().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getSobreComisionProm()).subtract(contenedor.getValorSobreComisionPromExternas());
				BigDecimal valor2 = movimientoInciso.getValorSobreComisionProm().subtract(contenedor.getValorSobreComisionPromExternas());
				factorCobranzaSobreComisionProm = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaSobreComisionProm = new BigDecimal(validacionInciso.getSobreComisionProm()).divide(movimientoInciso.getValorSobreComisionProm(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorBonoAgente().compareTo(CERO) == 0
					&& !validacionInciso.getBonoAgente().equals(0D)) {
				movimientoInciso.setValorBonoAgente(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getBonoAgente()));
				ajusteValorCeroBonoAgente = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.BONOAGENTE, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorBonoAgente().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getBonoAgente()).subtract(contenedor.getValorBonoAgenteExternas());
				BigDecimal valor2 = movimientoInciso.getValorBonoAgente().subtract(contenedor.getValorBonoAgenteExternas());
				factorCobranzaBonoAgente = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaBonoAgente = new BigDecimal(validacionInciso.getBonoAgente()).divide(movimientoInciso.getValorBonoAgente(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorBonoProm().compareTo(CERO) == 0
					&& !validacionInciso.getBonoProm().equals(0D)) {
				movimientoInciso.setValorBonoProm(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getBonoProm()));
				ajusteValorCeroBonoProm = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.BONOPROM, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorBonoProm().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getBonoProm()).subtract(contenedor.getValorBonoPromExternas());
				BigDecimal valor2 = movimientoInciso.getValorBonoProm().subtract(contenedor.getValorBonoPromExternas());
				factorCobranzaBonoProm = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaBonoProm = new BigDecimal(validacionInciso.getBonoProm()).divide(movimientoInciso.getValorBonoProm(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorCesionDerechosAgente().compareTo(CERO) == 0
					&& !validacionInciso.getCesionDerechosAgente().equals(0D)) {
				movimientoInciso.setValorCesionDerechosAgente(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getCesionDerechosAgente()));
				ajusteValorCeroCesionDerechosAgente = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.CESIONDERECHOSAGENTE, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorCesionDerechosAgente().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getCesionDerechosAgente()).subtract(contenedor.getValorCesionDerechosAgenteExternas());
				BigDecimal valor2 = movimientoInciso.getValorCesionDerechosAgente().subtract(contenedor.getValorCesionDerechosAgenteExternas());
				factorCobranzaCesionDerechosAgente = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaCesionDerechosAgente = new BigDecimal(validacionInciso.getCesionDerechosAgente()).divide(movimientoInciso.getValorCesionDerechosAgente(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorCesionDerechosProm().compareTo(CERO) == 0
					&& !validacionInciso.getCesionDerechosProm().equals(0D)) {
				movimientoInciso.setValorCesionDerechosProm(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getCesionDerechosProm()));
				ajusteValorCeroCesionDerechosProm = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.CESIONDERECHOSPROM, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorCesionDerechosProm().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getCesionDerechosProm()).subtract(contenedor.getValorCesionDerechosPromExternas());
				BigDecimal valor2 = movimientoInciso.getValorCesionDerechosProm().subtract(contenedor.getValorCesionDerechosPromExternas());
				factorCobranzaCesionDerechosProm = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaCesionDerechosProm = new BigDecimal(validacionInciso.getCesionDerechosProm()).divide(movimientoInciso.getValorCesionDerechosProm(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorSobreComisionUDIAgente().compareTo(CERO) == 0
					&& !validacionInciso.getSobreComisionUDIAgente().equals(0D)) {
				movimientoInciso.setValorSobreComisionUDIAgente(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getSobreComisionUDIAgente()));
				ajusteValorCeroSobreComisionUDIAgente = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.SOBRECOMISIONUDIAGENTE, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorSobreComisionUDIAgente().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getSobreComisionUDIAgente()).subtract(contenedor.getValorSobreComisionUDIAgenteExternas());
				BigDecimal valor2 = movimientoInciso.getValorSobreComisionUDIAgente().subtract(contenedor.getValorSobreComisionUDIAgenteExternas());
				factorCobranzaSobreComisionUDIAgente = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaSobreComisionUDIAgente = new BigDecimal(validacionInciso.getSobreComisionUDIAgente()).divide(movimientoInciso.getValorSobreComisionUDIAgente(), mathCtxFactor).abs();
			}
			
			if (movimientoInciso.getValorSobreComisionUDIProm().compareTo(CERO) == 0
					&& !validacionInciso.getSobreComisionUDIProm().equals(0D)) {
				movimientoInciso.setValorSobreComisionUDIProm(getAjusteValorCeroInciso(movimientoInciso, validacionInciso.getSobreComisionUDIProm()));
				ajusteValorCeroSobreComisionUDIProm = true;
				incrementaContadorCoberturasConcepto(movimientoInciso, Concepto.SOBRECOMISIONUDIPROM, getNumeroMovsCoberturaEnInciso(movimientoInciso));
			}
			
			if (movimientoInciso.getValorSobreComisionUDIProm().compareTo(CERO) != 0) {
				BigDecimal valor1 = new BigDecimal(validacionInciso.getSobreComisionUDIProm()).subtract(contenedor.getValorSobreComisionUDIPromExternas());
				BigDecimal valor2 = movimientoInciso.getValorSobreComisionUDIProm().subtract(contenedor.getValorSobreComisionUDIPromExternas());
				factorCobranzaSobreComisionUDIProm = valor1.divide(valor2, mathCtxFactor).abs();
				//factorCobranzaSobreComisionUDIProm = new BigDecimal(validacionInciso.getSobreComisionUDIProm()).divide(movimientoInciso.getValorSobreComisionUDIProm(), mathCtxFactor).abs();
			}
			
		}
		
		//Se actualiza el movimiento del inciso con los factores calculados
		movimientoInciso.setFactorCobranzaPrimaNeta(factorCobranzaPrimaNeta);
		movimientoInciso.setFactorCobranzaRPF(factorCobranzaRPF);
		movimientoInciso.setFactorCobranzaComision(factorCobranzaComision);
		movimientoInciso.setFactorCobranzaComisionRPF(factorCobranzaComisionRPF);
		movimientoInciso.setFactorCobranzaBonificacionComision(factorCobranzaBonificacionComision);
		movimientoInciso.setFactorCobranzaBonificacionRPF(factorCobranzaBonificacionRPF);
		movimientoInciso.setFactorCobranzaDerechos(factorCobranzaDerechos);
		movimientoInciso.setFactorCobranzaIva(factorCobranzaIva);
		movimientoInciso.setFactorCobranzaSobreComisionAgente(factorCobranzaSobreComisionAgente);
		movimientoInciso.setFactorCobranzaSobreComisionProm(factorCobranzaSobreComisionProm);
		movimientoInciso.setFactorCobranzaBonoAgente(factorCobranzaBonoAgente);
		movimientoInciso.setFactorCobranzaBonoProm(factorCobranzaBonoProm);
		movimientoInciso.setFactorCobranzaCesionDerechosAgente(factorCobranzaCesionDerechosAgente);
		movimientoInciso.setFactorCobranzaCesionDerechosProm(factorCobranzaCesionDerechosProm);
		movimientoInciso.setFactorCobranzaSobreComisionUDIAgente(factorCobranzaSobreComisionUDIAgente);
		movimientoInciso.setFactorCobranzaSobreComisionUDIProm(factorCobranzaSobreComisionUDIProm);
		
		movimientoInciso.setAjusteValorCeroPrimaNeta(ajusteValorCeroPrimaNeta);
		movimientoInciso.setAjusteValorCeroRPF(ajusteValorCeroRPF);
		movimientoInciso.setAjusteValorCeroComision(ajusteValorCeroComision);
		movimientoInciso.setAjusteValorCeroComisionRPF(ajusteValorCeroComisionRPF);
		movimientoInciso.setAjusteValorCeroBonificacionComision(ajusteValorCeroBonificacionComision);
		movimientoInciso.setAjusteValorCeroBonificacionRPF(ajusteValorCeroBonificacionRPF);
		movimientoInciso.setAjusteValorCeroDerechos(ajusteValorCeroDerechos);
		movimientoInciso.setAjusteValorCeroSobreComisionAgente(ajusteValorCeroSobreComisionAgente);
		movimientoInciso.setAjusteValorCeroSobreComisionProm(ajusteValorCeroSobreComisionProm);
		movimientoInciso.setAjusteValorCeroBonoAgente(ajusteValorCeroBonoAgente);
		movimientoInciso.setAjusteValorCeroBonoProm(ajusteValorCeroBonoProm);
		movimientoInciso.setAjusteValorCeroCesionDerechosAgente(ajusteValorCeroCesionDerechosAgente);
		movimientoInciso.setAjusteValorCeroCesionDerechosProm(ajusteValorCeroCesionDerechosProm);
		movimientoInciso.setAjusteValorCeroSobreComisionUDIAgente(ajusteValorCeroSobreComisionUDIAgente);
		movimientoInciso.setAjusteValorCeroSobreComisionUDIProm(ajusteValorCeroSobreComisionUDIProm);
		
		//Actualiza factorCobranzaCobertura
		actualizaFactorCobranzaCoberturas(movimientoInciso);
	}
	
	private void actualizaFactorCobranzaCoberturas(MovimientoEndoso movimientoInciso){
		
		if(movimientoInciso == null || !movimientoInciso.getTipo().equals(TipoMovimientoEndoso.INCISO)){
			return;
		}
		
		BigDecimal factorCobranzaPrimaNeta = movimientoInciso.getFactorCobranzaPrimaNeta();
		BigDecimal factorCobranzaRPF = movimientoInciso.getFactorCobranzaRPF();
		BigDecimal factorCobranzaBonificacionComision = movimientoInciso.getFactorCobranzaBonificacionComision();
		BigDecimal factorCobranzaBonificacionRPF = movimientoInciso.getFactorCobranzaBonificacionRPF();
		BigDecimal factorCobranzaComision = movimientoInciso.getFactorCobranzaComision();
		BigDecimal factorCobranzaComisionRPF = movimientoInciso.getFactorCobranzaComisionRPF();
		BigDecimal factorCobranzaDerechos = movimientoInciso.getFactorCobranzaDerechos();
		BigDecimal factorCobranzaIva = movimientoInciso.getFactorCobranzaIva();
		BigDecimal factorCobranzaSobreComisionAgente = movimientoInciso.getFactorCobranzaSobreComisionAgente();
		BigDecimal factorCobranzaSobreComisionProm = movimientoInciso.getFactorCobranzaSobreComisionProm();
		BigDecimal factorCobranzaBonoAgente = movimientoInciso.getFactorCobranzaBonoAgente();
		BigDecimal factorCobranzaBonoProm = movimientoInciso.getFactorCobranzaBonoProm();
		BigDecimal factorCobranzaCesionDerechosAgente = movimientoInciso.getFactorCobranzaCesionDerechosAgente();
		BigDecimal factorCobranzaCesionDerechosProm = movimientoInciso.getFactorCobranzaCesionDerechosProm();
		BigDecimal factorCobranzaSobreComisionUDIAgente = movimientoInciso.getFactorCobranzaSobreComisionUDIAgente();
		BigDecimal factorCobranzaSobreComisionUDIProm = movimientoInciso.getFactorCobranzaSobreComisionUDIProm();
		
		Boolean ajusteValorCeroPrimaNeta = movimientoInciso.getAjusteValorCeroPrimaNeta();
		Boolean ajusteValorCeroRPF = movimientoInciso.getAjusteValorCeroRPF();
		Boolean ajusteValorCeroBonificacionComision = movimientoInciso.getAjusteValorCeroBonificacionComision();
		Boolean ajusteValorCeroBonificacionRPF = movimientoInciso.getAjusteValorCeroBonificacionRPF();
		Boolean ajusteValorCeroComision = movimientoInciso.getAjusteValorCeroComision();
		Boolean ajusteValorCeroComisionRPF = movimientoInciso.getAjusteValorCeroComisionRPF();
		Boolean ajusteValorCeroDerechos = movimientoInciso.getAjusteValorCeroDerechos();
		Boolean ajusteValorCeroSobreComisionAgente = movimientoInciso.getAjusteValorCeroSobreComisionAgente();
		Boolean ajusteValorCeroSobreComisionProm = movimientoInciso.getAjusteValorCeroSobreComisionProm();
		Boolean ajusteValorCeroBonoAgente = movimientoInciso.getAjusteValorCeroBonoAgente();
		Boolean ajusteValorCeroBonoProm = movimientoInciso.getAjusteValorCeroBonoProm();
		Boolean ajusteValorCeroCesionDerechosAgente = movimientoInciso.getAjusteValorCeroCesionDerechosAgente();
		Boolean ajusteValorCeroCesionDerechosProm = movimientoInciso.getAjusteValorCeroCesionDerechosProm();
		Boolean ajusteValorCeroSobreComisionUDIAgente = movimientoInciso.getAjusteValorCeroSobreComisionUDIAgente();
		Boolean ajusteValorCeroSobreComisionUDIProm = movimientoInciso.getAjusteValorCeroSobreComisionUDIProm();
		
		//Se actualiza el movimiento de cobertura con los factores calculados
		for(MovimientoEndoso movimientoCobertura : movimientoInciso.getMovimientosEndoso()){

			movimientoCobertura.setFactorCobranzaPrimaNeta(factorCobranzaPrimaNeta);
			movimientoCobertura.setFactorCobranzaRPF(factorCobranzaRPF);
			movimientoCobertura.setFactorCobranzaComision(factorCobranzaComision);
			movimientoCobertura.setFactorCobranzaComisionRPF(factorCobranzaComisionRPF);
			movimientoCobertura.setFactorCobranzaBonificacionComision(factorCobranzaBonificacionComision);
			movimientoCobertura.setFactorCobranzaBonificacionRPF(factorCobranzaBonificacionRPF);
			movimientoCobertura.setFactorCobranzaDerechos(factorCobranzaDerechos);
			movimientoCobertura.setFactorCobranzaIva(factorCobranzaIva);
			movimientoCobertura.setFactorCobranzaSobreComisionAgente(factorCobranzaSobreComisionAgente);
			movimientoCobertura.setFactorCobranzaSobreComisionProm(factorCobranzaSobreComisionProm);
			movimientoCobertura.setFactorCobranzaBonoAgente(factorCobranzaBonoAgente);
			movimientoCobertura.setFactorCobranzaBonoProm(factorCobranzaBonoProm);
			movimientoCobertura.setFactorCobranzaCesionDerechosAgente(factorCobranzaCesionDerechosAgente);
			movimientoCobertura.setFactorCobranzaCesionDerechosProm(factorCobranzaCesionDerechosProm);
			movimientoCobertura.setFactorCobranzaSobreComisionUDIAgente(factorCobranzaSobreComisionUDIAgente);
			movimientoCobertura.setFactorCobranzaSobreComisionUDIProm(factorCobranzaSobreComisionUDIProm);
			
			movimientoCobertura.setAjusteValorCeroPrimaNeta(ajusteValorCeroPrimaNeta);
			movimientoCobertura.setAjusteValorCeroRPF(ajusteValorCeroRPF);
			movimientoCobertura.setAjusteValorCeroComision(ajusteValorCeroComision);
			movimientoCobertura.setAjusteValorCeroComisionRPF(ajusteValorCeroComisionRPF);
			movimientoCobertura.setAjusteValorCeroBonificacionComision(ajusteValorCeroBonificacionComision);
			movimientoCobertura.setAjusteValorCeroBonificacionRPF(ajusteValorCeroBonificacionRPF);
			movimientoCobertura.setAjusteValorCeroDerechos(ajusteValorCeroDerechos);
			movimientoCobertura.setAjusteValorCeroSobreComisionAgente(ajusteValorCeroSobreComisionAgente);
			movimientoCobertura.setAjusteValorCeroSobreComisionProm(ajusteValorCeroSobreComisionProm);
			movimientoCobertura.setAjusteValorCeroBonoAgente(ajusteValorCeroBonoAgente);
			movimientoCobertura.setAjusteValorCeroBonoProm(ajusteValorCeroBonoProm);
			movimientoCobertura.setAjusteValorCeroCesionDerechosAgente(ajusteValorCeroCesionDerechosAgente);
			movimientoCobertura.setAjusteValorCeroCesionDerechosProm(ajusteValorCeroCesionDerechosProm);
			movimientoCobertura.setAjusteValorCeroSobreComisionUDIAgente(ajusteValorCeroSobreComisionUDIAgente);
			movimientoCobertura.setAjusteValorCeroSobreComisionUDIProm(ajusteValorCeroSobreComisionUDIProm);
		}
	}
	
	private ContenedorPrimasMovimientoDTO calculaValoresMovimientoIncisoCoberturasPropias(MovimientoEndoso movimientoInciso){
		ContenedorPrimasMovimientoDTO contenedorPrimas = new ContenedorPrimasMovimientoDTO();
		if(movimientoInciso.getTipo().equals(TipoMovimientoEndoso.INCISO)){
			List<MovimientoEndoso> movimientoCoberturas = movimientoInciso.getMovimientosEndoso();
			for(MovimientoEndoso movimiento: movimientoCoberturas){
				if(movimiento.getBitemporalCoberturaSeccion().getContinuity().getCoberturaDTO().getCoberturaEsPropia().equals("1")) {
					contenedorPrimas.getValorMovimientoPropias().add(movimiento.getValorMovimiento());
					contenedorPrimas.getValorBonificacionComisionPropias().add(movimiento.getValorBonificacionComision());
					contenedorPrimas.getValorBonificacionRPFPropias().add(movimiento.getValorBonificacionRPF());
					contenedorPrimas.getValorBonoAgentePropias().add(movimiento.getValorBonoAgente());
					contenedorPrimas.getValorBonoPromPropias().add(movimiento.getValorBonoProm());
					contenedorPrimas.getValorCesionDerechosAgentePropias().add(movimiento.getValorCesionDerechosAgente());
					contenedorPrimas.getValorCesionDerechosPromPropias().add(movimiento.getValorCesionDerechosProm());
					contenedorPrimas.getValorComisionPropias().add(movimiento.getValorComision());
					contenedorPrimas.getValorComisionRPFPropias().add(movimiento.getValorComisionRPF());
					contenedorPrimas.getValorDerechosPropias().add(movimiento.getValorDerechos());
					contenedorPrimas.getValorIvaPropias().add(movimiento.getValorIva());
					contenedorPrimas.getValorRPFPropias().add(movimiento.getValorRPF());
					contenedorPrimas.getValorSobreComisionAgentePropias().add(movimiento.getValorSobreComisionAgente());
					contenedorPrimas.getValorSobreComisionPromPropias().add(movimiento.getValorSobreComisionProm());
					contenedorPrimas.getValorSobreComisionUDIAgentePropias().add(movimiento.getValorSobreComisionUDIAgente());
					contenedorPrimas.getValorSobreComisionUDIPromPropias().add(movimiento.getValorSobreComisionUDIProm());
				}else{
					contenedorPrimas.getValorMovimientoExternas().add(movimiento.getValorMovimiento());
					contenedorPrimas.getValorBonificacionComisionExternas().add(movimiento.getValorBonificacionComision());
					contenedorPrimas.getValorBonificacionRPFExternas().add(movimiento.getValorBonificacionRPF());
					contenedorPrimas.getValorBonoAgenteExternas().add(movimiento.getValorBonoAgente());
					contenedorPrimas.getValorBonoPromExternas().add(movimiento.getValorBonoProm());
					contenedorPrimas.getValorCesionDerechosAgenteExternas().add(movimiento.getValorCesionDerechosAgente());
					contenedorPrimas.getValorCesionDerechosPromExternas().add(movimiento.getValorCesionDerechosProm());
					contenedorPrimas.getValorComisionExternas().add(movimiento.getValorComision());
					contenedorPrimas.getValorComisionRPFExternas().add(movimiento.getValorComisionRPF());
					contenedorPrimas.getValorDerechosExternas().add(movimiento.getValorDerechos());
					contenedorPrimas.getValorIvaExternas().add(movimiento.getValorIva());
					contenedorPrimas.getValorRPFExternas().add(movimiento.getValorRPF());
					contenedorPrimas.getValorSobreComisionAgenteExternas().add(movimiento.getValorSobreComisionAgente());
					contenedorPrimas.getValorSobreComisionPromExternas().add(movimiento.getValorSobreComisionProm());
					contenedorPrimas.getValorSobreComisionUDIAgenteExternas().add(movimiento.getValorSobreComisionUDIAgente());
					contenedorPrimas.getValorSobreComisionUDIPromExternas().add(movimiento.getValorSobreComisionUDIProm());
				}
			}
		}
		return contenedorPrimas;
	}
	

	
	private void calculaFactoresCobranzaCobertura(MovimientoEndoso movimientoCobertura) {
		
		//Regresa si no es propia

		/* Ya no aplica porque las coberturas propias ya no se consideran en los movimientos de ajustes de prima
		if(!movimientoCobertura.getBitemporalCoberturaSeccion().getContinuity().getCoberturaDTO().getCoberturaEsPropia().equals("1")){
			return;
		}
		*/
		
		if (movimientoCobertura.getMovimientoEndoso() != null 
				&& movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroDerechos() != null 
				&& movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroDerechos()
				&& movimientoCobertura.getValorMovimiento().compareTo(CERO) != 0) { // Fix: Distribuir derechos solo entre coberturas que tienen un valor de prima neta diferente de 0
			movimientoCobertura.setValorDerechos(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorDerechos()));
		}
		
		movimientoCobertura.setFactorCobranzaDerechos(getFactorCobranzaCobertura(movimientoCobertura, Concepto.DERECHOS));
		movimientoCobertura.setFactorCobranzaIva(getFactorCobranzaCobertura(movimientoCobertura, Concepto.IVA));
		
		if (movimientoCobertura.getMovimientoEndoso().getValidacionInciso() == null) {
			return;
		}
			
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroPrimaNeta()) {
			movimientoCobertura.setValorMovimiento(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorMovimiento()));
		}
			
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroRPF()) {
			movimientoCobertura.setValorRPF(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorRPF()));
		}
		
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroBonificacionComision()) {
			movimientoCobertura.setValorBonificacionComision(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorBonificacionComision()));
		}
		
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroBonificacionRPF()) {
			movimientoCobertura.setValorBonificacionRPF(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorBonificacionRPF()));
		}
		
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroComision()) {
			movimientoCobertura.setValorComision(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorComision()));
		}
		
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroComisionRPF()) {
			movimientoCobertura.setValorComisionRPF(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorComisionRPF()));
		}
				
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroSobreComisionAgente()) {
			movimientoCobertura.setValorSobreComisionAgente(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorSobreComisionAgente()));
		}
		
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroSobreComisionProm()) {
			movimientoCobertura.setValorSobreComisionProm(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorSobreComisionProm()));
		}
		
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroBonoAgente()) {
			movimientoCobertura.setValorBonoAgente(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorBonoAgente()));
		}
	
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroBonoProm()) {
			movimientoCobertura.setValorBonoProm(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorBonoProm()));
		}
		
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroCesionDerechosAgente()) {
			movimientoCobertura.setValorCesionDerechosAgente(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorCesionDerechosAgente()));
		}
		
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroCesionDerechosProm()) {
			movimientoCobertura.setValorCesionDerechosProm(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorCesionDerechosProm()));
		}
		
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroSobreComisionUDIAgente()) {
			movimientoCobertura.setValorSobreComisionUDIAgente(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorSobreComisionUDIAgente()));
		}
		
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroSobreComisionUDIProm()) {
			movimientoCobertura.setValorSobreComisionUDIProm(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorSobreComisionUDIProm()));
		}
		
		movimientoCobertura.setFactorCobranzaPrimaNeta(getFactorCobranzaCobertura(movimientoCobertura, Concepto.PRIMA_NETA));
		movimientoCobertura.setFactorCobranzaRPF(getFactorCobranzaCobertura(movimientoCobertura, Concepto.RPF));
		movimientoCobertura.setFactorCobranzaComision(getFactorCobranzaCobertura(movimientoCobertura, Concepto.COMISION));
		movimientoCobertura.setFactorCobranzaComisionRPF(getFactorCobranzaCobertura(movimientoCobertura, Concepto.COMISION_RPF));
		movimientoCobertura.setFactorCobranzaBonificacionComision(getFactorCobranzaCobertura(movimientoCobertura, Concepto.BONIFICACION_COMISION));
		movimientoCobertura.setFactorCobranzaBonificacionRPF(getFactorCobranzaCobertura(movimientoCobertura, Concepto.BONIFICACION_RPF));
		movimientoCobertura.setFactorCobranzaSobreComisionAgente(getFactorCobranzaCobertura(movimientoCobertura, Concepto.SOBRECOMISIONAGENTE));
		movimientoCobertura.setFactorCobranzaSobreComisionProm(getFactorCobranzaCobertura(movimientoCobertura, Concepto.SOBRECOMISIONPROM));
		movimientoCobertura.setFactorCobranzaBonoAgente(getFactorCobranzaCobertura(movimientoCobertura, Concepto.BONOAGENTE));
		movimientoCobertura.setFactorCobranzaBonoProm(getFactorCobranzaCobertura(movimientoCobertura, Concepto.BONOPROM));
		movimientoCobertura.setFactorCobranzaCesionDerechosAgente(getFactorCobranzaCobertura(movimientoCobertura, Concepto.CESIONDERECHOSAGENTE));
		movimientoCobertura.setFactorCobranzaCesionDerechosProm(getFactorCobranzaCobertura(movimientoCobertura, Concepto.CESIONDERECHOSPROM));
		movimientoCobertura.setFactorCobranzaSobreComisionUDIAgente(getFactorCobranzaCobertura(movimientoCobertura, Concepto.SOBRECOMISIONUDIAGENTE));
		movimientoCobertura.setFactorCobranzaSobreComisionUDIProm(getFactorCobranzaCobertura(movimientoCobertura, Concepto.SOBRECOMISIONUDIPROM));
		
	}
		
	private BigDecimal getFactorCobranzaCobertura (MovimientoEndoso movimientoCobertura, Concepto concepto) {
		
		BigDecimal factorCobranza = null;
		BigDecimal valorConceptoOriginal = null;
		BigDecimal valorConceptoIgualado = null;
		BigDecimal valorConceptoIgualadoRedondeado = null;
		BigDecimal diferencia = null;
		BigDecimal diferenciaInciso = null;
		Integer contadorCoberturasConcepto = null;
		
		
		//Se obtiene el factor Cobranza normal (el obtenido a partir del factor del movimiento a nivel inciso)
		switch (concepto) {
			case PRIMA_NETA: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaPrimaNeta();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasPrimaNeta() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasPrimaNeta() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorMovimiento();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaPrimaNeta() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaPrimaNeta() : CERO);
				break;
			}
			case RPF: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaRPF();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasRPF() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasRPF() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorRPF();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaRPF() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaRPF() : CERO);
				break;
			}
			case BONIFICACION_COMISION: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaBonificacionComision();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasBonificacionComision() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasBonificacionComision() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorBonificacionComision();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaBonificacionComision() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaBonificacionComision() : CERO);
				break;
			}
			case BONIFICACION_RPF: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaBonificacionRPF();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasBonificacionRPF() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasBonificacionRPF() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorBonificacionRPF();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaBonificacionRPF() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaBonificacionRPF() : CERO);
				break;
			}
			case COMISION: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaComision();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasComision() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasComision() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorComision();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaComision() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaComision() : CERO);
				break;
			}
			case COMISION_RPF: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaComisionRPF();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasComisionRPF() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasComisionRPF() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorComisionRPF();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaComisionRPF() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaComisionRPF() : CERO);
				break;
			}
			case DERECHOS: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaDerechos();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasDerechos() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasDerechos() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorDerechos();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaDerechos() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaDerechos() : CERO);
				break;
			}
			case IVA: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaIva();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasIva() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasIva() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorIva();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaIva() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaIva() : CERO);
				break;
			}
			case SOBRECOMISIONAGENTE: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaSobreComisionAgente();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasSobreComisionAgente() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasSobreComisionAgente() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorSobreComisionAgente();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaSobreComisionAgente() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaSobreComisionAgente() : CERO);
				break;
			}
			case SOBRECOMISIONPROM: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaSobreComisionProm();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasSobreComisionProm() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasSobreComisionProm() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorSobreComisionProm();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaSobreComisionProm() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaSobreComisionProm() : CERO);
				break;
			}
			case BONOAGENTE: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaBonoAgente();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasBonoAgente() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasBonoAgente() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorBonoAgente();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaBonoAgente() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaBonoAgente() : CERO);
				break;
			}
			case BONOPROM: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaBonoProm();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasBonoProm() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasBonoProm() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorBonoProm();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaBonoProm() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaBonoProm() : CERO);
				break;
			}
			case CESIONDERECHOSAGENTE: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaCesionDerechosAgente();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasCesionDerechosAgente() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasCesionDerechosAgente() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorCesionDerechosAgente();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaCesionDerechosAgente() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaCesionDerechosAgente() : CERO);
				break;
			}
			case CESIONDERECHOSPROM: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaCesionDerechosProm();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasCesionDerechosProm() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasCesionDerechosProm() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorCesionDerechosProm();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaCesionDerechosProm() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaCesionDerechosProm() : CERO);
				break;
			}
			case SOBRECOMISIONUDIAGENTE: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaSobreComisionUDIAgente();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasSobreComisionUDIAgente() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasSobreComisionUDIAgente() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorSobreComisionUDIAgente();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaSobreComisionUDIAgente() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaSobreComisionUDIAgente() : CERO);
				break;
			}
			case SOBRECOMISIONUDIPROM: {
				factorCobranza = movimientoCobertura.getMovimientoEndoso().getFactorCobranzaSobreComisionUDIProm();
				contadorCoberturasConcepto = (movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasSobreComisionUDIProm() != null 
						? movimientoCobertura.getMovimientoEndoso().getNumeroCoberturasSobreComisionUDIProm() : 0);
				valorConceptoOriginal = movimientoCobertura.getValorSobreComisionUDIProm();
				diferenciaInciso = (movimientoCobertura.getMovimientoEndoso().getDiferenciaSobreComisionUDIProm() != null 
						? movimientoCobertura.getMovimientoEndoso().getDiferenciaSobreComisionUDIProm() : CERO);
				break;
			}
		}
		
		
		if (valorConceptoOriginal.compareTo(CERO) != 0) {
			contadorCoberturasConcepto -= 1;
			
			valorConceptoIgualado = valorConceptoOriginal.multiply(factorCobranza);
			valorConceptoIgualadoRedondeado = valorConceptoIgualado.setScale(16, BigDecimal.ROUND_HALF_UP);
			diferencia = valorConceptoIgualadoRedondeado.subtract(valorConceptoIgualado);
			diferenciaInciso = diferenciaInciso.add(diferencia);
			
			if (contadorCoberturasConcepto.intValue() == 0) {
				//Se asume que se esta procesando la ultima de las coberturas con valor diferente a cero de ese inciso, 
				//y por lo tanto diferenciaInciso tiene la diferencia total a nivel inciso
				
				diferenciaInciso = diferenciaInciso.setScale(16, BigDecimal.ROUND_HALF_UP);
				
				valorConceptoIgualadoRedondeado = valorConceptoIgualadoRedondeado.subtract(diferenciaInciso);
				
				factorCobranza = valorConceptoIgualadoRedondeado.divide(valorConceptoOriginal, mathCtxFactor).abs();
			} else {
				//Se actualizan los valores de contadorCoberturasConcepto y diferenciaInciso
				switch (concepto) {
					case PRIMA_NETA: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasPrimaNeta(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaPrimaNeta(diferenciaInciso);
						break;
					}
					case RPF: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasRPF(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaRPF(diferenciaInciso);
						break;
					}
					case BONIFICACION_COMISION: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasBonificacionComision(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaBonificacionComision(diferenciaInciso);
						break;
					}
					case BONIFICACION_RPF: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasBonificacionRPF(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaBonificacionRPF(diferenciaInciso);
						break;
					}
					case COMISION: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasComision(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaComision(diferenciaInciso);
						break;
					}
					case COMISION_RPF: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasComisionRPF(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaComisionRPF(diferenciaInciso);
						break;
					}
					case DERECHOS: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasDerechos(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaDerechos(diferenciaInciso);
						break;
					}
					case IVA: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasIva(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaIva(diferenciaInciso);
						break;
					}
					case SOBRECOMISIONAGENTE: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasSobreComisionAgente(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaSobreComisionAgente(diferenciaInciso);
						break;
					}
					case SOBRECOMISIONPROM: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasSobreComisionProm(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaSobreComisionProm(diferenciaInciso);
						break;
					}
					case BONOAGENTE: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasBonoAgente(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaBonoAgente(diferenciaInciso);
						break;
					}
					case BONOPROM: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasBonoProm(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaBonoProm(diferenciaInciso);
						break;
					}
					case CESIONDERECHOSAGENTE: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasCesionDerechosAgente(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaCesionDerechosAgente(diferenciaInciso);
						break;
					}
					case CESIONDERECHOSPROM: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasCesionDerechosProm(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaCesionDerechosProm(diferenciaInciso);
						break;
					}
					case SOBRECOMISIONUDIAGENTE: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasSobreComisionUDIAgente(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaSobreComisionUDIAgente(diferenciaInciso);
						break;
					}
					case SOBRECOMISIONUDIPROM: {
						movimientoCobertura.getMovimientoEndoso().setNumeroCoberturasSobreComisionUDIProm(contadorCoberturasConcepto);
						movimientoCobertura.getMovimientoEndoso().setDiferenciaSobreComisionUDIProm(diferenciaInciso);
						break;
					}
				}
				
			}
		}
		
		return factorCobranza;
		
	}
	
	
	/**
	 * XXX Metodos de Ayuda
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Short getNumeroEndosoBajaInciso (BitemporalInciso incisoTarget) {
		
		DateTime validFrom = incisoTarget.getValidityInterval().getInterval().getStart();
		
		Short numeroEndosoBajaInciso = 0;
		
		List<BitemporalInciso> incisos = incisoTarget.getEntidadContinuity().getBitemporalProperty().getEvolution(validFrom); 
		
		Collections.sort(incisos, new Comparator(){
			public int compare(Object o1, Object o2) {
				return ((BitemporalInciso)o1).getId().compareTo(((BitemporalInciso)o2).getId());
			}
		});
		
		BitemporalInciso ultimoIncisoBitemporal = incisos.get(incisos.size()-1);
		
		DateTime recordFrom = null;
		
		if (ultimoIncisoBitemporal.getRecordInterval().getInterval().getEnd().equals(TimeUtils.endOfTime())) {
			recordFrom = ultimoIncisoBitemporal.getRecordInterval().getInterval().getStart();
			validFrom = ultimoIncisoBitemporal.getValidityInterval().getInterval().getEnd();
		} else {
			recordFrom = ultimoIncisoBitemporal.getRecordInterval().getInterval().getEnd();
			validFrom = ultimoIncisoBitemporal.getValidityInterval().getInterval().getStart();
		}
		
		Long cotizacionId = incisoTarget.getContinuity().getParentContinuity().getNumero().longValue();
		
		
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("idToCotizacion", cotizacionId);
		params.put("validFrom", validFrom.toDate());
		params.put("recordFrom", recordFrom.toDate());
		
		List<EndosoDTO> endosos = entidadService.findByProperties(EndosoDTO.class, params);
		
		if (endosos != null && endosos.size() > 0) {
			numeroEndosoBajaInciso = endosos.get(0).getId().getNumeroEndoso();
		}
		
		return numeroEndosoBajaInciso;
		
	}
	
	private Agente getAgente(Long agenteId) {
		Agente agente = null;
		List<AgenteView> agenteViewList = new ArrayList<AgenteView>();
		Agente filtroAgente = new Agente();
		filtroAgente.setIdAgente(agenteId);
		agenteViewList = agenteMidasService.findByFilterLightWeight(filtroAgente);
		if (agenteViewList != null && !agenteViewList.isEmpty()) {
			AgenteView agenteTmp = agenteMidasService.findByFilterLightWeight(filtroAgente).get(0);
			agente = new Agente();
			agente.getPersona().setNombreCompleto(agenteTmp.getNombreCompleto());
			agente.setIdAgente(agenteTmp.getIdAgente());
		} else {
			agente = entidadService.findById(Agente.class, agenteId);			
		}
		
		return agente;
	}
		
	private String getDescripcionFormaPago(Integer formaPagoId, Short monedaId) {
		FormaPagoIDTO formaPagoIDTO = new FormaPagoIDTO();
		formaPagoIDTO.setIdFormaPago(formaPagoId);
		formaPagoIDTO.setIdMoneda(monedaId);
		try {
			List<FormaPagoIDTO> formaPagoList = formaPagoFacadeRemote
					.findByProperty(formaPagoIDTO, "", "A");
			if (formaPagoList != null)
				return formaPagoList.get(0)
						.getDescripcion();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return null;
	}
	
	
	/**
	 * Indicates the operation type based in the source and the target.
	 */
	private TipoOperacion getTipoOperacion(Object source, Object target, short tipoEndoso) {
		
		if (target != null) {
			switch (tipoEndoso) {
				case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO:
					return null;
				case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA:
				case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS:
					return TipoOperacion.AGREGADO;
				case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO:
					return TipoOperacion.MODIFICADO;
			}
		}
		
		
		if (source != null && target != null) {
			
			if (source instanceof BitemporalCoberturaSeccion) {
				
				BitemporalCoberturaSeccion coberturaSource = (BitemporalCoberturaSeccion)source;
				BitemporalCoberturaSeccion coberturaTarget = (BitemporalCoberturaSeccion)target;
				
				if (coberturaSource.getValue().getClaveContrato().shortValue() == 1 
						&& coberturaTarget.getValue().getClaveContrato().shortValue() == 0) {
					return TipoOperacion.BORRADO;
				} else if(coberturaSource.getValue().getClaveContrato().shortValue() == 0 
						&& coberturaTarget.getValue().getClaveContrato().shortValue() == 1) {
					return TipoOperacion.AGREGADO;
				}
				
			}
			
			return TipoOperacion.MODIFICADO;
		}
		if (source != null && target == null) {
			if (source instanceof BitemporalCoberturaSeccion) {
				BitemporalCoberturaSeccion coberturaSource = (BitemporalCoberturaSeccion)source;
				if (coberturaSource.getValue().getClaveContrato().shortValue() == 1){
					return TipoOperacion.BORRADO;
				}else{
					return TipoOperacion.NINGUNO;
				} 
			}
			return TipoOperacion.BORRADO;
		}
		if (source == null && target != null) {
			if (target instanceof BitemporalCoberturaSeccion) {
				BitemporalCoberturaSeccion coberturaSource = (BitemporalCoberturaSeccion)target;
				if (coberturaSource.getValue().getClaveContrato().shortValue() == 1){
					return TipoOperacion.AGREGADO;
				}else{
					return TipoOperacion.NINGUNO;
				} 
			}
			return TipoOperacion.AGREGADO;
		}
		return TipoOperacion.NINGUNO;
	}
	
	private TipoMovimientoEndoso getTipoMovimientoEndoso (Object source, Object target) {
		
		Object obj = null;
		
		if (source != null) {
			obj = source;
		} else if (target != null) {
			obj = target;
		} else {
			throw new RuntimeException("getTipoMovimientoEndoso debe tener al menos un parametro...");
		}
		
		if (obj instanceof BitemporalCoberturaSeccion) {
			return TipoMovimientoEndoso.COBERTURA;
		} else if (obj instanceof BitemporalInciso || obj instanceof BitemporalAutoInciso || obj instanceof BitemporalCondicionEspInc) {
			return TipoMovimientoEndoso.INCISO;
		} else if (obj instanceof BitemporalCotizacion) {
			return TipoMovimientoEndoso.POLIZA;
		} else if (obj instanceof MovimientoEndoso) {
			MovimientoEndoso movimiento = (MovimientoEndoso)obj;
			return movimiento.getTipo();
		}
		return null;
	}
	
	/**
	 * compara 2 TipoMovimientoEndoso
	 * @param tipoOrigen TipoMovimientoEndoso origen
	 * @param tipoOtro TipoMovimientoEndoso con el que se compara
	 * @return Un valor > 0 si tipoOrigen > tipoOtro, < 0 si tipoOrigen < tipoOtro, 0 si son iguales
	 */
	private int compareTipoMovimiento (TipoMovimientoEndoso tipoOrigen, TipoMovimientoEndoso tipoOtro) {
		
		switch (tipoOrigen) {
			case POLIZA: {
				if (!tipoOtro.equals(TipoMovimientoEndoso.POLIZA)) {
					return 1;
				}
				break;
			}				
			case INCISO: {
				switch (tipoOtro) {
					case POLIZA: {
						return -1;
					}
					case COBERTURA: {
						return 1;
					}
				}
				break;
			}	
			case COBERTURA: {
				if (!tipoOtro.equals(TipoMovimientoEndoso.COBERTURA)) {
					return -1;
				}
				break;
			}
		}
		
		return 0;
	}
	
	
	/**
	 * Obtiene el objeto bitemporal para agregarlo en el movimiento
	 * @param obj El objeto a evaluar
	 * @return El objeto bitemporal para agregarlo en el movimiento
	 */
	private Object getBitemporalObject(Object obj) {
		
		if (obj != null && obj instanceof MovimientoEndoso) {
			MovimientoEndoso movimiento = (MovimientoEndoso)obj;
			
			switch (movimiento.getTipo()) {
				case INCISO:
					return movimiento.getBitemporalInciso();
				case COBERTURA:
					return movimiento.getBitemporalCoberturaSeccion();
			}
			return null;
		} else if (obj != null && obj instanceof FuenteMovimientoDTO) {
			FuenteMovimientoDTO fuente = (FuenteMovimientoDTO)obj;
			return getBitemporalObject(fuente.getTarget() != null ? fuente.getTarget() : fuente.getSource());
		}
		
		return obj;
		
	}
		
	private boolean isSameContinuity (Object original, Object compared) {
		
		Object objOriginal = getBitemporalObject(original);
		Object objCompared = getBitemporalObject(compared);
		
		if (objOriginal != null && objCompared != null) {
			if (objOriginal instanceof BitemporalInciso && objCompared instanceof BitemporalInciso) {
				
				if (((BitemporalInciso)objOriginal).getContinuity().getId().equals(((BitemporalInciso)objCompared).getContinuity().getId())) {
					return true;
				}
			} else if (objOriginal instanceof BitemporalCoberturaSeccion && objCompared instanceof BitemporalCoberturaSeccion) {
				if (((BitemporalCoberturaSeccion)objOriginal).getContinuity().getId().equals(((BitemporalCoberturaSeccion)objCompared).getContinuity().getId())) {
					return true;
				}
			} else if (objOriginal instanceof BitemporalInciso && objCompared instanceof BitemporalCoberturaSeccion) {
				if (((BitemporalInciso)objOriginal).getContinuity().getId()
						.equals(((BitemporalCoberturaSeccion)objCompared).getContinuity().getParentContinuity().getParentContinuity().getId())) {
					return true;
				}
			} else if (objOriginal instanceof IncisoContinuity && objCompared instanceof BitemporalInciso) {
				
				if (((IncisoContinuity)objOriginal).getId().equals(((BitemporalInciso)objCompared).getContinuity().getId())) {
					return true;
				}
			} else if (objOriginal instanceof IncisoContinuity && objCompared instanceof BitemporalCoberturaSeccion) {
				if (((IncisoContinuity)objOriginal).getId()
						.equals(((BitemporalCoberturaSeccion)objCompared).getContinuity().getParentContinuity().getParentContinuity().getId())) {
					return true;
				}
			}
			
		}
		
		return false;
		
	}
	
	/**
	 * Revisa en un endoso de Baja de Inciso o Rehabilitacion de Inciso si el inciso a evaluar aplica derechos de Poliza, de Endoso o ninguno
	 * @param movimientoInciso Movimiento a nivel inciso
	 * @param controlEndosoCot
	 * @return  Se va a regresar:
	 * 0 -> Si no aplica derechos
	 * 1 -> Si aplica derechos de poliza
	 * 2 -> Si aplica derechos de endoso
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Integer revisaAplicaDerechosPolizaEndosoBajaRehabInciso(MovimientoEndoso movimientoInciso, ControlEndosoCot controlEndosoCot) {
		
		Integer aplicacionDerechos = 0;
		
		if(controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO
				|| controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS) {
			
			CotizacionContinuity cotizacionContinuity = entidadContinuityService.findContinuityByBusinessKey(CotizacionContinuity.class, 
					CotizacionContinuity.BUSINESS_KEY_NAME, controlEndosoCot.getCotizacionId());
			
			DateTime fechaInicioVigenciaPoliza = getFechaInicioVigenciaPoliza (cotizacionContinuity);
			
			DateTime validFrom = new DateTime(controlEndosoCot.getValidFrom());
			
			if (validFrom.equals(fechaInicioVigenciaPoliza)) {
				aplicacionDerechos = 1;
			} else {
				List<BitemporalInciso> incisos = movimientoInciso.getBitemporalInciso().getEntidadContinuity().getBitemporalProperty().getEvolution(validFrom); 
				
				Collections.sort(incisos, new Comparator(){
					public int compare(Object o1, Object o2) {
						return ((BitemporalInciso)o1).getId().compareTo(((BitemporalInciso)o2).getId());
					}
				});
				
				if (validFrom.equals(incisos.get(0).getValidityInterval().getInterval().getStart())) {
					aplicacionDerechos = 2;
				}
			}
		}
		
		// Valida derechos prorrateados
		if (aplicacionDerechos == 0 && (controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO
				|| controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA)) {
			
			CotizacionContinuity cotizacionContinuity = entidadContinuityService.findContinuityByBusinessKey(CotizacionContinuity.class,
					CotizacionContinuity.BUSINESS_KEY_NAME,controlEndosoCot.getCotizacionId());
			
			DateTime validFrom = new DateTime(controlEndosoCot.getValidFrom());
			
			BitemporalCotizacion bc = cotizacionContinuity.getBitemporalProperty().get(validFrom);			
			Integer idFormaPago = bc.getValue().getFormaPago().getIdFormaPago();
			Short idMoneda = bc.getValue().getMoneda().getIdTcMoneda();

			FormaPagoIDTO formaPagoIDTO = null;
			if(idFormaPago != null && idMoneda != null){
				try {
	        		formaPagoIDTO = formaPagoInterfazServiciosRemote.getPorIdClaveNegocio(idFormaPago.intValue(), 
	        				idMoneda, "SYSTEM", "A");
					if(formaPagoIDTO != null && formaPagoIDTO.getDerechosProrrateados() != null
							&& formaPagoIDTO.getDerechosProrrateados().trim().equals("V")) {
						aplicacionDerechos = 3;
					}					
				} catch (SystemException e) {
					System.out.println("Error al obtener los datos de formapago de Seycos...");
				}			
			}			
		}
				
		return aplicacionDerechos;
	}
	
	private BigDecimal getValorDerechos(MovimientoEndoso movimientoInciso, ControlEndosoCot controlEndosoCot, AuxiliarCalculoDTO auxiliar) {
		
		BigDecimal valorDerechos = CERO;
		Integer aplicaDerechosInciso = revisaAplicaDerechosPolizaEndosoBajaRehabInciso(movimientoInciso, controlEndosoCot);
		short tipoEndoso = controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue();
		
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA
				|| tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO
				|| tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS) {
			//Asumiendo que en los movimientos de endosos de rehabilitacion vienen originalmmente los valores de sus respectivas cancelaciones
			valorDerechos = movimientoInciso.getValorDerechos().multiply(new BigDecimal(-1));
		} else if ((movimientoInciso != null && movimientoInciso.getValorMovimiento() != null && movimientoInciso.getValorMovimiento().compareTo(CERO) > 0 
				&& (tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA)	
				&& (tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO)
				&& (tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO)
				&& (tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS)
				&& (tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA)) 
			|| auxiliar.getAplicaDerechosEndoso() || aplicaDerechosInciso.intValue() == 2) {
			
			valorDerechos = auxiliar.getValorDerechosEndoso();
			
		} else if(auxiliar.getAplicaDerechosPoliza() || aplicaDerechosInciso.intValue() == 1) {
			valorDerechos = auxiliar.getValorDerechosPoliza();
		} else if (aplicaDerechosInciso.intValue() == 3) {
			List<PolizaDTO> listPol = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", controlEndosoCot.getCotizacionId());
			if(listPol != null && !listPol.isEmpty()){
				BitemporalInciso incisoSource = (BitemporalInciso) movimientoInciso.getBitemporalInciso();
				if(incisoSource != null) {
					valorDerechos = movimientoEndosoDao.obtienePrimaDerechosProrrata(listPol.get(0).getIdToPoliza(), incisoSource.getContinuity().getNumero(), controlEndosoCot.getValidFrom());
				}
			}
		}
		
		return valorDerechos;
		
	}
	
	
	private Collection<BitemporalComision> getPorcentajesComisionesCotizacion(CotizacionContinuity cotizacionContinuity, DateTime validFrom, 
			ControlEndosoCot controlEndosoCotCancelacionPoliza) {
		
		Collection<BitemporalComision> comisiones = null;
		//Si se trata de una rehabilitacion de poliza
		if (controlEndosoCotCancelacionPoliza != null) {
			
			DateTime validoEn = TimeUtils.getDateTime(controlEndosoCotCancelacionPoliza.getValidFrom());
			DateTime conocidoEn = new DateTime(controlEndosoCotCancelacionPoliza.getRecordFrom());
			
			comisiones = entidadBitemporalService.obtenerCancelados(cotizacionContinuity.getComisionContinuities().getValue(), 
						BitemporalComision.class, validoEn, conocidoEn);
			
		} else { //Cualquier otro endoso
			//comisiones = cotizacionContinuity.getComisionContinuities().get(validFrom);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("continuity.cotizacionContinuity.id", cotizacionContinuity.getId());		
			comisiones = (List<BitemporalComision>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalComision.class, 
					params, validFrom, null, true);	
			if(comisiones == null || comisiones.isEmpty()){
				comisiones = (List<BitemporalComision>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalComision.class, 
						params, validFrom, null, false);					
			}
		}
		
		if (comisiones == null) throw new RuntimeException("Los porcentajes de las comisiones no deben ser nulos");
		
		return comisiones;
	}
	
	private void cargaCoberturasCobranza(ControlEndosoCot controlEndosoCot, short tipoEndoso) {
		
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO) {
			
			BigDecimal lineaId = null;
			//Si el endoso corresponde a una poliza migrada se pasa el idSolicitud de Seycos negativo para los calculos de cobranza
			if (controlEndosoCot.getMigrada()) {
				
				Map<String, Object> params = new LinkedHashMap<String, Object>();
				params.put("idToCotizacion", controlEndosoCot.getCotizacionId());
				
				List<EndosoDTO> endosos = entidadService.findByPropertiesWithOrder(EndosoDTO.class, params, "id.numeroEndoso DESC");
				
				if (endosos != null && endosos.size() > 0) {
					short numeroEndoso = endosos.get(0).getId().getNumeroEndoso();
				
				
					params = new LinkedHashMap<String, Object>();
					params.put("id.idVersionPol", new Long(numeroEndoso+1));
					params.put("idToPoliza", endosos.get(0).getPolizaDTO().getIdToPoliza().longValue());
					List<MigEndososValidos> migEndososValidos = entidadService.findByProperties(MigEndososValidos.class, params);
					if (migEndososValidos!=null && !migEndososValidos.isEmpty()) {
						lineaId = new BigDecimal(migEndososValidos.get(0).getIdSolicitud()).negate();
					}
				}
				
			}
			
			//Se obtienen las coberturas modificadas por el servicio de cobranza
			coberturasCobranza = cobranzaService
					.consultaCoberturasConSaldoPendiente(controlEndosoCot.getSolicitud().getIdToPolizaEndosada(), lineaId);
			
		}
		
		
	}
	
	
	private BigDecimal getValorRPFCambioFormaPago (BitemporalCoberturaSeccion cobertura, AuxiliarCalculoDTO auxiliar) {
		
		BigDecimal valorTotalPrimasCFP = CERO;
		
		for (CoberturaCobranzaDTO coberturaCobranza : coberturasCobranza) {
			if ((cobertura.getContinuity().getCoberturaDTO().getIdToCobertura().compareTo(coberturaCobranza.getCoberturaId()) == 0)
					&& (new BigDecimal(cobertura.getContinuity().getParentContinuity().getParentContinuity().getNumero())
						.compareTo(coberturaCobranza.getNumeroInciso()) == 0)) {
				
				valorTotalPrimasCFP = new BigDecimal(coberturaCobranza.getValorPN());
				break;
			}
		}
		
		return valorTotalPrimasCFP.multiply(new BigDecimal(auxiliar.getPorcentajeFormaPago()/100D), mathCtxFactor)
			.subtract(valorTotalPrimasCFP.multiply(new BigDecimal(auxiliar.getPorcentajeFormaPagoAnterior()/100D), mathCtxFactor)); 
		
	}
	
	private AuxiliarCalculoDTO cargaAuxiliar(CotizacionContinuity cotizacionContinuity, ControlEndosoCot controlEndosoCot, short tipoEndoso, DateTime validFrom) {
		
		ControlEndosoCot controlEndosoCotCancelacionPoliza = null;
		BitemporalCotizacion cotizacion = null;
		BitemporalCotizacion cotizacionAnterior = null;
		NegocioDerechoEndoso derechosEndoso = null;
		Long negocioDerechoEndosoId = null; 
		boolean aplicaDerechosPoliza = false;
		boolean aplicaDerechosEndoso = false;
		DateTime fechaFinVigenciaPoliza = null;
		AuxiliarCalculoDTO auxiliar = new AuxiliarCalculoDTO();
		List<NegocioBonoComision> negocioBonoComisiones = null;
		Long negocioId = null;
		
		cotizacion = cotizacionContinuity.getCotizaciones().getInProcess(validFrom);
		
		if (cotizacion == null) {
			cotizacion = cotizacionContinuity.getCotizaciones().get(validFrom);
		}
		
		//Se obtiene la polizaId
		Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("cotizacionDTO.idToCotizacion", cotizacionContinuity.getNumero());
		List<PolizaDTO> polizaList = entidadService.findByProperties(PolizaDTO.class, properties);
		
		switch (tipoEndoso) {
			case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO: {
				cotizacionAnterior = cotizacionContinuity.getCotizaciones().get(validFrom);
				auxiliar.setPorcentajeFormaPagoAnterior(cotizacionAnterior.getValue().getPorcentajePagoFraccionado());
				break;
			}
			case SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA:
			case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA: {
				
				if(polizaList != null && !polizaList.isEmpty() && polizaList.get(0).getCotizacionDTO().getFechaInicioVigencia()
						.equals(controlEndosoCot.getValidFrom())){
					aplicaDerechosPoliza = true;
				}
				
				if (tipoEndoso ==  SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA) {
					properties = new HashMap<String,Object>();
					properties.put("cotizacionId", cotizacionContinuity.getNumero());
					properties.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EMITIDA);
					
					StringBuilder queryWhere = new StringBuilder();
					StringBuilder orderClause = new StringBuilder(" order by model.id DESC");
					
					queryWhere.append(" and model.solicitud.claveTipoEndoso IN (" + SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA + "," + SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL + ") "); 
										
					List<ControlEndosoCot> controlEndosoCotList = entidadService.findByColumnsAndProperties(ControlEndosoCot.class, "id,cotizacionId,claveEstatusCot,solicitud,"
							+ "validFrom,recordFrom,idCancela,numeroEndosoAjusta,primaTotalIgualar,porcentajePagoFraccionado", 
							properties, new HashMap<String,Object>(), queryWhere, orderClause);
					
					controlEndosoCotCancelacionPoliza = controlEndosoCotList.get(0);
				}
				
				break;
			}
			case SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO: {
				
				ControlEndosoCot controlEndosoCotAfectado = entidadService.findById(ControlEndosoCot.class, controlEndosoCot.getIdCancela());
				
				//Si es una cancelacion de EAP
				if (controlEndosoCotAfectado.getNumeroEndosoAjusta() != null) {
					auxiliar.setValorDerechosEndoso(CERO);
					break;
				}
				
				if (controlEndosoCotAfectado.getValidFrom().equals(controlEndosoCot.getValidFrom())) {
					aplicaDerechosEndoso = true;
				}
				
				DateTime validoEn = TimeUtils.getDateTime(controlEndosoCotAfectado.getValidFrom());
				DateTime conocidoEn = new DateTime(controlEndosoCotAfectado.getRecordFrom());
				
				Cotizacion cotizacionEndosoACancelar = cotizacion.getEntidadContinuity().getBitemporalProperty().on(validoEn, conocidoEn);
				
				if (cotizacionEndosoACancelar != null) {
					negocioDerechoEndosoId = cotizacionEndosoACancelar.getNegocioDerechoEndosoId();
					if(negocioDerechoEndosoId != null){
						derechosEndoso = entidadService.findById(NegocioDerechoEndoso.class, negocioDerechoEndosoId);
						auxiliar.setValorDerechosEndoso(new BigDecimal(derechosEndoso.getImporteDerecho() * -1));
					}else{
						auxiliar.setValorDerechosEndoso(BigDecimal.ZERO);
					}

				}
				
				break;
			}
			
			case SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL:
			case SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL:{
				
				auxiliar.setValorDerechosEndoso(BigDecimal.ZERO);	
				break;
			}			
			default: {
				derechosEndoso = getNegocioDerechoEndoso(cotizacion);
				if(derechosEndoso == null){
					derechosEndoso = new NegocioDerechoEndoso();
					derechosEndoso.setImporteDerecho(0.0);
				}
				
				if (tipoEndoso ==  SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO) {
					auxiliar.setValorDerechosEndoso(new BigDecimal(derechosEndoso.getImporteDerecho() * -1));
				} else {
					auxiliar.setValorDerechosEndoso(new BigDecimal(derechosEndoso.getImporteDerecho()));
					
					if(cotizacion.getValue().getNegocioDerechoEndosoId() != null) {
						aplicaDerechosEndoso = true;
					}
					
				}
				
				break;
			}
		
		}
		
		if (tipoEndoso ==  SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA) {
			auxiliar.setPorcentajeFormaPago(controlEndosoCot.getPorcentajePagoFraccionado());
		} else {
			auxiliar.setPorcentajeFormaPago(cotizacion.getValue().getPorcentajePagoFraccionado());
		}
		
		//Se obtienen los dias de vigencia del endoso
		fechaFinVigenciaPoliza = getFechaFinVigenciaPoliza(cotizacionContinuity, validFrom);
		auxiliar.setDiasVigencia(Days.daysBetween(validFrom, fechaFinVigenciaPoliza).getDays());
		
		auxiliar.setValorDerechosPoliza(new BigDecimal(cotizacion.getValue().getNegocioDerechoPoliza().getImporteDerecho() * -1));
		auxiliar.setAplicaDerechosEndoso(aplicaDerechosEndoso);
		auxiliar.setAplicaDerechosPoliza(aplicaDerechosPoliza);
		
		auxiliar.setComisiones(getPorcentajesComisionesCotizacion(cotizacionContinuity, validFrom, controlEndosoCotCancelacionPoliza));
		auxiliar.setPorcentajeBonificacionComision(cotizacion.getValue().getPorcentajebonifcomision());
		auxiliar.setPorcentajeIVA(cotizacion.getValue().getPorcentajeIva());
		
		//Se obtienen comisiones de negocio
		NegocioBonoComision negocioBono = new NegocioBonoComision();
		if(polizaList != null && !polizaList.isEmpty()){
			negocioId = polizaList.get(0).getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio();
			negocioBonoComisiones = entidadService.findByProperty(NegocioBonoComision.class, "negocio.idToNegocio", negocioId);
			if(negocioBonoComisiones != null && !negocioBonoComisiones.isEmpty()){
				negocioBono = negocioBonoComisiones.get(0);
				auxiliar.setPorcentajeUDI(negocioBono.getPctUDI() != null ? negocioBono.getPctUDI() : CIEN); //Se pone CIEN como Default por conveniencia
				auxiliar.setPorcentajeSobreComision(negocioBono.getPctSobreComision() != null ? negocioBono.getPctSobreComision() : CERO);
				auxiliar.setPorcentajeSobreComisionAgente(negocioBono.getPctAgenteSobreComision() != null ? negocioBono.getPctAgenteSobreComision() : CERO);
				auxiliar.setPorcentajeSobreComisionProm(negocioBono.getPctPromotorSobreComision() != null ? negocioBono.getPctPromotorSobreComision() : CERO);
				auxiliar.setPorcentajeBono(negocioBono.getPctBono() != null ? negocioBono.getPctBono() : CERO);
				auxiliar.setPorcentajeBonoAgente(negocioBono.getPctAgenteBono() != null ? negocioBono.getPctAgenteBono() : CERO);
				auxiliar.setPorcentajeBonoProm(negocioBono.getPctPromotorBono() != null ? negocioBono.getPctPromotorBono() : CERO);
				auxiliar.setPorcentajeCesionDerechos(negocioBono.getPctDerechos() != null ? negocioBono.getPctDerechos() : null);
				auxiliar.setPorcentajeCesionDerechosAgente(negocioBono.getPctAgenteDerechos() != null ? negocioBono.getPctAgenteDerechos() : CERO);
				auxiliar.setPorcentajeCesionDerechosProm(negocioBono.getPctPromotorDerechos() != null ? negocioBono.getPctPromotorDerechos() : CERO);
				auxiliar.setPorcentajeSobreComisionUDI(negocioBono.getPctUDI() != null ? negocioBono.getPctUDI() : CERO);
				auxiliar.setPorcentajeSobreComisionUDIAgente(negocioBono.getPctAgenteUDI() != null ? negocioBono.getPctAgenteUDI() : CERO);
				auxiliar.setPorcentajeSobreComisionUDIProm(negocioBono.getPctPromotorUDI() != null ? negocioBono.getPctPromotorUDI() : CERO);
				auxiliar.setImporteCesionDerechos(negocioBono.getImporteDerechos() != null ? negocioBono.getImporteDerechos() : null);
			} else {
				auxiliar.setPorcentajeUDI(CIEN); //Se pone CIEN como Default por conveniencia
				auxiliar.setPorcentajeSobreComision(CERO);
				auxiliar.setPorcentajeSobreComisionAgente(CERO);
				auxiliar.setPorcentajeSobreComisionProm(CERO);
				auxiliar.setPorcentajeBono(CERO);
				auxiliar.setPorcentajeBonoAgente(CERO);
				auxiliar.setPorcentajeBonoProm(CERO);
				auxiliar.setPorcentajeCesionDerechos(null);
				auxiliar.setPorcentajeCesionDerechosAgente(CERO);
				auxiliar.setPorcentajeCesionDerechosProm(CERO);
				auxiliar.setPorcentajeSobreComisionUDI(CERO);
				auxiliar.setPorcentajeSobreComisionUDIAgente(CERO);
				auxiliar.setPorcentajeSobreComisionUDIProm(CERO);
				auxiliar.setImporteCesionDerechos(null);
			}
		}
		
		return auxiliar;
		
	}
	
	private void incrementaContadorCoberturasConcepto(MovimientoEndoso movimiento, Concepto concepto, int incremento) {
		
		MovimientoEndoso movimientoInciso = null;
		
		if (movimiento.getTipo().equals(TipoMovimientoEndoso.COBERTURA)) {
			movimientoInciso = movimiento.getMovimientoEndoso();
		} else if (movimiento.getTipo().equals(TipoMovimientoEndoso.INCISO)) {
			movimientoInciso = movimiento;
		}
		
		if (movimientoInciso == null) throw new RuntimeException("incrementaContadorCoberturasConcepto: El Movimiento de tipo Inciso no puede ser nulo");
		
		switch (concepto) {
			case PRIMA_NETA: {
				if (movimiento.getValorMovimiento().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasPrimaNeta(
							(movimientoInciso.getNumeroCoberturasPrimaNeta() != null
									?movimientoInciso.getNumeroCoberturasPrimaNeta():0) + incremento);
				}
				break;
			}
			case RPF: {
				if (movimiento.getValorRPF().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasRPF(
							(movimientoInciso.getNumeroCoberturasRPF() != null
									?movimientoInciso.getNumeroCoberturasRPF():0) + incremento);
				}
				break;
			}
			case BONIFICACION_COMISION: {
				if (movimiento.getValorBonificacionComision().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasBonificacionComision(
							(movimientoInciso.getNumeroCoberturasBonificacionComision() != null
									?movimientoInciso.getNumeroCoberturasBonificacionComision():0) + incremento);
				}
				break;
			}
			case BONIFICACION_RPF: {
				if (movimiento.getValorBonificacionRPF().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasBonificacionRPF(
							(movimientoInciso.getNumeroCoberturasBonificacionRPF() != null
									?movimientoInciso.getNumeroCoberturasBonificacionRPF():0) + incremento);
				}
				break;
			}
			case COMISION: {
				if (movimiento.getValorComision().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasComision(
							(movimientoInciso.getNumeroCoberturasComision() != null
									?movimientoInciso.getNumeroCoberturasComision():0) + incremento);
				}
				break;
			}
			case COMISION_RPF: {
				if (movimiento.getValorComisionRPF().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasComisionRPF(
							(movimientoInciso.getNumeroCoberturasComisionRPF() != null
									?movimientoInciso.getNumeroCoberturasComisionRPF():0) + incremento);
				}
				break;
			}
			case DERECHOS: {
				if (movimiento.getValorDerechos().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasDerechos(
							(movimientoInciso.getNumeroCoberturasDerechos() != null
									?movimientoInciso.getNumeroCoberturasDerechos():0) + incremento);
				}
				break;
			}
			case IVA: {
				if (movimiento.getValorIva().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasIva(
							(movimientoInciso.getNumeroCoberturasIva() != null
									?movimientoInciso.getNumeroCoberturasIva():0) + incremento);
				}
				break;
			}
			case SOBRECOMISIONAGENTE: {
				if (movimiento.getValorSobreComisionAgente().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasSobreComisionAgente(
							(movimientoInciso.getNumeroCoberturasSobreComisionAgente() != null
									?movimientoInciso.getNumeroCoberturasSobreComisionAgente():0) + incremento);
				}
				break;
			}
			case SOBRECOMISIONPROM: {
				if (movimiento.getValorSobreComisionProm().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasSobreComisionProm(
							(movimientoInciso.getNumeroCoberturasSobreComisionProm() != null
									?movimientoInciso.getNumeroCoberturasSobreComisionProm():0) + incremento);
				}
				break;
			}
			case BONOAGENTE: {
				if (movimiento.getValorBonoAgente().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasBonoAgente(
							(movimientoInciso.getNumeroCoberturasBonoAgente() != null
									?movimientoInciso.getNumeroCoberturasBonoAgente():0) + incremento);
				}
				break;
			}
			case BONOPROM: {
				if (movimiento.getValorBonoProm().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasBonoProm(
							(movimientoInciso.getNumeroCoberturasBonoProm() != null
									?movimientoInciso.getNumeroCoberturasBonoProm():0) + incremento);
				}
				break;
			}
			case CESIONDERECHOSAGENTE: {
				if (movimiento.getValorCesionDerechosAgente().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasCesionDerechosAgente(
							(movimientoInciso.getNumeroCoberturasCesionDerechosAgente() != null
									?movimientoInciso.getNumeroCoberturasCesionDerechosAgente():0) + incremento);
				}
				break;
			}
			case CESIONDERECHOSPROM: {
				if (movimiento.getValorCesionDerechosProm().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasCesionDerechosProm(
							(movimientoInciso.getNumeroCoberturasCesionDerechosProm() != null
									?movimientoInciso.getNumeroCoberturasCesionDerechosProm():0) + incremento);
				}
				break;
			}
			case SOBRECOMISIONUDIAGENTE: {
				if (movimiento.getValorSobreComisionUDIAgente().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasSobreComisionUDIAgente(
							(movimientoInciso.getNumeroCoberturasSobreComisionUDIAgente() != null
									?movimientoInciso.getNumeroCoberturasSobreComisionUDIAgente():0) + incremento);
				}
				break;
			}
			case SOBRECOMISIONUDIPROM: {
				if (movimiento.getValorSobreComisionUDIProm().compareTo(CERO) != 0) {
					movimientoInciso.setNumeroCoberturasSobreComisionUDIProm(
							(movimientoInciso.getNumeroCoberturasSobreComisionUDIProm() != null
									?movimientoInciso.getNumeroCoberturasSobreComisionUDIProm():0) + incremento);
				}
				break;
			}
		}
		
	}
	
	/**
	 * Incrementa el valor de un concepto de movimiento de inciso con el valor proporcionado
	 * @param movimiento Movimiento de cobertura cuyo movimiento Padre (inciso) se desea incrementar
	 * @param concepto concepto del movimiento que se quiere incrementar
	 * @param incremento Cantidad que se desea incrementar. En caso de ser nulo, se incrementara el valor del concepto del movimiento de Cobertura
	 */
	private void incrementaValorConceptoInciso(MovimientoEndoso movimiento, Concepto concepto, BigDecimal incremento) {
		
		MovimientoEndoso movimientoInciso = movimiento.getMovimientoEndoso();
		
		switch (concepto) {
			case PRIMA_NETA: {
				incremento = (incremento != null?incremento:movimiento.getValorMovimiento());
				movimientoInciso.setValorMovimiento(
						(movimientoInciso.getValorMovimiento()!=null?movimientoInciso.getValorMovimiento():CERO).add(incremento));
				break;
			}
			case RPF: {
				incremento = (incremento != null?incremento:movimiento.getValorRPF());
				movimientoInciso.setValorRPF(
						(movimientoInciso.getValorRPF()!=null?movimientoInciso.getValorRPF():CERO).add(incremento));
				break;
			}
			case BONIFICACION_COMISION: {
				incremento = (incremento != null?incremento:movimiento.getValorBonificacionComision());
				movimientoInciso.setValorBonificacionComision(
						(movimientoInciso.getValorBonificacionComision()!=null?movimientoInciso.getValorBonificacionComision():CERO).add(incremento));
				break;
			}
			case BONIFICACION_RPF: {
				incremento = (incremento != null?incremento:movimiento.getValorBonificacionRPF());
				movimientoInciso.setValorBonificacionRPF(
						(movimientoInciso.getValorBonificacionRPF()!=null?movimientoInciso.getValorBonificacionRPF():CERO).add(incremento));
				break;
			}
			case COMISION: {
				incremento = (incremento != null?incremento:movimiento.getValorComision());
				movimientoInciso.setValorComision(
						(movimientoInciso.getValorComision()!=null?movimientoInciso.getValorComision():CERO).add(incremento));
				break;
			}
			case COMISION_RPF: {
				incremento = (incremento != null?incremento:movimiento.getValorComisionRPF());
				movimientoInciso.setValorComisionRPF(
						(movimientoInciso.getValorComisionRPF()!=null?movimientoInciso.getValorComisionRPF():CERO).add(incremento));
				break;
			}
			case DERECHOS: {
				incremento = (incremento != null?incremento:movimiento.getValorDerechos());
				movimientoInciso.setValorDerechos(
						(movimientoInciso.getValorDerechos()!=null?movimientoInciso.getValorDerechos():CERO).add(incremento));
				break;
			}
			case IVA: {
				incremento = (incremento != null?incremento:movimiento.getValorIva());
				movimientoInciso.setValorIva(
						(movimientoInciso.getValorIva()!=null?movimientoInciso.getValorIva():CERO).add(incremento));
				break;
			}
			case SOBRECOMISIONAGENTE: {
				incremento = (incremento != null?incremento:movimiento.getValorSobreComisionAgente());
				movimientoInciso.setValorSobreComisionAgente(
						(movimientoInciso.getValorSobreComisionAgente()!=null?movimientoInciso.getValorSobreComisionAgente():CERO).add(incremento));
				break;
			}
			case SOBRECOMISIONPROM: {
				incremento = (incremento != null?incremento:movimiento.getValorSobreComisionProm());
				movimientoInciso.setValorSobreComisionProm(
						(movimientoInciso.getValorSobreComisionProm()!=null?movimientoInciso.getValorSobreComisionProm():CERO).add(incremento));
				break;
			}
			case BONOAGENTE: {
				incremento = (incremento != null?incremento:movimiento.getValorBonoAgente());
				movimientoInciso.setValorBonoAgente(
						(movimientoInciso.getValorBonoAgente()!=null?movimientoInciso.getValorBonoAgente():CERO).add(incremento));
				break;
			}
			case BONOPROM: {
				incremento = (incremento != null?incremento:movimiento.getValorBonoProm());
				movimientoInciso.setValorBonoProm(
						(movimientoInciso.getValorBonoProm()!=null?movimientoInciso.getValorBonoProm():CERO).add(incremento));
				break;
			}
			case CESIONDERECHOSAGENTE: {
				incremento = (incremento != null?incremento:movimiento.getValorCesionDerechosAgente());
				movimientoInciso.setValorCesionDerechosAgente(
						(movimientoInciso.getValorCesionDerechosAgente()!=null?movimientoInciso.getValorCesionDerechosAgente():CERO).add(incremento));
				break;
			}
			case CESIONDERECHOSPROM: {
				incremento = (incremento != null?incremento:movimiento.getValorCesionDerechosProm());
				movimientoInciso.setValorCesionDerechosProm(
						(movimientoInciso.getValorCesionDerechosProm()!=null?movimientoInciso.getValorCesionDerechosProm():CERO).add(incremento));
				break;
			}
			case SOBRECOMISIONUDIAGENTE: {
				incremento = (incremento != null?incremento:movimiento.getValorSobreComisionUDIAgente());
				movimientoInciso.setValorSobreComisionUDIAgente(
						(movimientoInciso.getValorSobreComisionUDIAgente()!=null?movimientoInciso.getValorSobreComisionUDIAgente():CERO).add(incremento));
				break;
			}
			case SOBRECOMISIONUDIPROM: {
				incremento = (incremento != null?incremento:movimiento.getValorSobreComisionUDIProm());
				movimientoInciso.setValorSobreComisionUDIProm(
						(movimientoInciso.getValorSobreComisionUDIProm()!=null?movimientoInciso.getValorSobreComisionUDIProm():CERO).add(incremento));
				break;
			}
		}
		
	}
	
	private Double getPorcentajeComision(Collection<BitemporalComision> comisiones,	CoberturaDTO coberturaOriginal) {
		Double pctComision = 0D;
		for (BitemporalComision comision : comisiones) {
			if (comision.getContinuity().getSubRamoDTO().getIdTcSubRamo()
					.intValue() == coberturaOriginal.getSubRamoDTO()
					.getIdTcSubRamo().intValue()) {
				pctComision = comision.getValue()
						.getPorcentajeComisionDefault().doubleValue();
				break;
			}
		}
		return pctComision;
	}
	
	private Double getPorcentajeComisionPoliza(CotizacionContinuity cotizacionContinuity, CoberturaDTO coberturaOriginal) {
		Double pctComision = 0D;	
		DateTime fechaInicioVigenciaPoliza = getFechaInicioVigenciaPoliza (cotizacionContinuity);
		
		Collection<BitemporalComision> comisiones = cotizacionContinuity
			.getComisionContinuities().getInProcess(fechaInicioVigenciaPoliza);
		
		for (BitemporalComision comision : comisiones) {
			if (comision.getContinuity().getSubRamoDTO().getIdTcSubRamo()
					.intValue() == coberturaOriginal.getSubRamoDTO()
					.getIdTcSubRamo().intValue()
					&& comision.getValue().getClaveTipoPorcentajeComision().shortValue() == 1) {
				pctComision = comision.getValue()
						.getPorcentajeComisionDefault().doubleValue();
				break;
			}
		}
		return pctComision;
		
		
	}
	
	private Integer getNumeroMovsCoberturaEnInciso(MovimientoEndoso movimiento) {
		MovimientoEndoso movimientoInciso = null;
		Integer numeroMovimientosCoberturaEnInciso = 0;
		
		if (movimiento.getTipo().equals(TipoMovimientoEndoso.COBERTURA)) {
			movimientoInciso = movimiento.getMovimientoEndoso();
		} else if (movimiento.getTipo().equals(TipoMovimientoEndoso.INCISO)) {
			movimientoInciso = movimiento;
		}
		
		if (movimientoInciso != null &&  movimientoInciso.getMovimientosEndoso() != null) {
			//numeroMovimientosCoberturaEnInciso = movimientoInciso.getMovimientosEndoso().size();
			for(MovimientoEndoso movimientoCont : movimientoInciso.getMovimientosEndoso()){
					//Se contabiliza el movimiento de cobertura si no depende de una cobertura
				if(movimientoCont.getFuenteMovimiento().getTarget() != null && movimientoCont.getFuenteMovimiento().getSource() != null  
						&& movimientoCont.getFuenteMovimiento().getSource() instanceof BitemporalCoberturaSeccion)
				{
					BitemporalCoberturaSeccion coberturaSource = (BitemporalCoberturaSeccion)movimientoCont.getFuenteMovimiento().getSource();
					BitemporalCoberturaSeccion coberturaTarget = (BitemporalCoberturaSeccion)movimientoCont.getFuenteMovimiento().getTarget();		
					
					if (coberturaSource.getValue().getClaveContrato().shortValue() == 1 
							&& coberturaTarget.getValue().getClaveContrato().shortValue() == 0) {
						
						if(coberturaSource.getValue().getValorPrimaDiaria() != null
								&& coberturaSource.getValue().getValorPrimaDiaria().doubleValue() != 0){
							numeroMovimientosCoberturaEnInciso++;
						}						
					}
					else
					{
						if(movimientoCont.getBitemporalCoberturaSeccion() != null){
							if(movimientoCont.getBitemporalCoberturaSeccion().getValue().getValorPrimaDiaria() != null
									&& movimientoCont.getBitemporalCoberturaSeccion().getValue().getValorPrimaDiaria().doubleValue() != 0){
								numeroMovimientosCoberturaEnInciso++;
							}
						}else{
							numeroMovimientosCoberturaEnInciso++;
						}
					}
					
				}else
				{
					if(movimientoCont.getBitemporalCoberturaSeccion() != null){
						if(movimientoCont.getBitemporalCoberturaSeccion().getValue().getValorPrimaDiaria() != null
								&& movimientoCont.getBitemporalCoberturaSeccion().getValue().getValorPrimaDiaria().doubleValue() != 0){
							numeroMovimientosCoberturaEnInciso++;
						}
					}else{
						numeroMovimientosCoberturaEnInciso++;
					}
					
				}
				
					
			}
		}
		if(numeroMovimientosCoberturaEnInciso.intValue() == 0){
			numeroMovimientosCoberturaEnInciso = 1;
		}
		
		return numeroMovimientosCoberturaEnInciso;
	}
	
	private BigDecimal getAjusteValorCeroInciso(MovimientoEndoso movimientoInciso, Object valorConcepto) {
		
		//Se asume que valorConcepto es diferente de null y es instancia de Double o BigDecimal
		BigDecimal valor = null;
		BigDecimal ajusteValorCero = new BigDecimal(getNumeroMovsCoberturaEnInciso(movimientoInciso));
		
		if (valorConcepto instanceof Double) {
			valor = new BigDecimal((Double) valorConcepto);
		} else {
			valor = (BigDecimal) valorConcepto;
		}
		
		//Se usa el signo contrario a lo que se tiene que igualar 
		if (valor.compareTo(CERO) > 0) {
			ajusteValorCero = ajusteValorCero.negate();
		}
		
		return ajusteValorCero;
		
	}
	
	private BigDecimal getAjusteValorCeroCobertura (BigDecimal valorConcepto) {
		
		if (valorConcepto.compareTo(CERO) < 0) {
			return UNO.negate();
		}
		
		return UNO;
		
	}
	
	private List<EndosoIDTO> getValidacionesIncisoEAP(ControlEndosoCot controlEndosoCot, BigDecimal valorPrimaTotalEmitida,
			List<FuenteMovimientoDTO> fuentesInciso, AuxiliarCalculoDTO auxiliar) {
		List<EndosoIDTO> validacionesInciso = new ArrayListNullAware<EndosoIDTO>();
		BigDecimal valorTotalPrimasIncisoAjustado = null;
		MovimientoEndoso movimientoAuxiliar = null;
		MovimientoEndoso movimientoCalculado = null;
		BigDecimal cociente = null;
		BigDecimal residuo = null;
		Integer numeroInciso = null;
		int contador = 0;
		BigDecimal valorDerechosEAP = CERO;
		
		BigDecimal numeroMovimientosInciso = (fuentesInciso != null ? new BigDecimal(fuentesInciso.size()) : CERO);
				
		BigDecimal primaTotalIgualar = controlEndosoCot.getPrimaTotalIgualar();
		
		
		//Se obtiene la prima total que se tiene que emitir para que se ajuste a la prima a la que quiere llegar el usuario
		BigDecimal diferenciaPrimaTotal = primaTotalIgualar.subtract(valorPrimaTotalEmitida);
		
		//Mediante ingenieria inversa se obtiene el Total de Primas a emitir (ajuste)
		BigDecimal ajuste = calculoInverso(auxiliar, diferenciaPrimaTotal, valorDerechosEAP);
				
		//Se distribuye el ajuste entre objetos EndosoIDTO en el concepto de valorPrimaNeta usando 
		//como referencia los movimientos a nivel de inciso del endoso EAP
		
		cociente = ajuste.divideToIntegralValue(numeroMovimientosInciso);
		
		residuo = ajuste.remainder(numeroMovimientosInciso).setScale(2, BigDecimal.ROUND_HALF_UP);
		
		
		for (FuenteMovimientoDTO fuenteInciso : fuentesInciso) {
			
			if (contador < 2) {
				//Si es el Primer inciso, este se calcula con el residuo
				valorTotalPrimasIncisoAjustado = cociente.add(residuo);
				
				movimientoCalculado = new MovimientoEndoso(controlEndosoCot, TipoMovimientoEndoso.COBERTURA, "Calculo inciso EAP");
				movimientoCalculado.setMovimientoEndoso(new MovimientoEndoso());
				movimientoCalculado.getMovimientoEndoso().setValorDerechos(valorDerechosEAP);
				movimientoCalculado.getMovimientoEndoso().getMovimientosEndoso().add(movimientoCalculado);
				movimientoAuxiliar = new MovimientoEndoso(controlEndosoCot, TipoMovimientoEndoso.COBERTURA, "Auxiliar");
				movimientoAuxiliar.setValorMovimiento(valorTotalPrimasIncisoAjustado);
				movimientoCalculado.setFuenteMovimiento(new FuenteMovimientoDTO(TipoMovimientoEndoso.COBERTURA, null, movimientoAuxiliar));
				
				calculaMovimiento(movimientoCalculado, auxiliar, false);
				
				//El siguiente inciso ya no incluye el residuo
				residuo = CERO;
			} 
			
			numeroInciso = ((BitemporalInciso)getBitemporalObject(fuenteInciso)).getContinuity().getNumero();
			
			//Se llena el objeto EndosoIDTO a partir del MovimientoEndoso
			validacionesInciso.add(transformaMovimientoEndoso(movimientoCalculado, numeroInciso));
			
			contador ++;
		}
		
		return validacionesInciso;
	}
	
	/**
	 * Se obtiene un objeto EndosoIDTO a partir de un movimiento. Esto para su posterior igualacion
	 * @param movimiento Objeto MovimientoEndoso a transformar
	 * @param numeroInciso Numero de Inciso al que corresponde el objeto EndosoIDTO
	 * @return Un objeto EndosoIDTO con los datos de calculo del objeto MovimientoEndoso
	 */
	private EndosoIDTO transformaMovimientoEndoso(MovimientoEndoso movimiento, Integer numeroInciso) {
		
		EndosoIDTO endosoIDTO = new EndosoIDTO();
		
		endosoIDTO.setCalculo(calculoSeycos);
		endosoIDTO.setNumeroInciso(numeroInciso);
		endosoIDTO.setPrimaNeta(movimiento.getValorMovimiento().negate().doubleValue());
		endosoIDTO.setRecargoPF(movimiento.getValorRPF().negate().doubleValue());
		endosoIDTO.setComision(movimiento.getValorComision().negate().doubleValue());
		endosoIDTO.setComisionRPF(movimiento.getValorComisionRPF().negate().doubleValue());
		//Bonificaciones siempre deben ser mismo signo que la prima neta
		BigDecimal valorBonifComis = movimiento.getValorBonificacionComision().abs().multiply(new BigDecimal(new BigDecimal(endosoIDTO.getPrimaNeta()).signum()));
		endosoIDTO.setBonificacionComision(valorBonifComis.doubleValue());
		BigDecimal valorBonifRpf = movimiento.getValorBonificacionRPF().abs().multiply(new BigDecimal(new BigDecimal(endosoIDTO.getPrimaNeta()).signum()));
		endosoIDTO.setBonificacionComisionRPF(valorBonifRpf.doubleValue());
		endosoIDTO.setDerechos(movimiento.getValorDerechos().negate().doubleValue());
		endosoIDTO.setIva(movimiento.getValorIva().negate().doubleValue());
		endosoIDTO.setSobreComisionAgente(movimiento.getValorSobreComisionAgente().negate().doubleValue());
		endosoIDTO.setSobreComisionProm(movimiento.getValorSobreComisionProm().negate().doubleValue());
		endosoIDTO.setBonoAgente(movimiento.getValorBonoAgente().negate().doubleValue());
		endosoIDTO.setBonoProm(movimiento.getValorBonoProm().negate().doubleValue());
		endosoIDTO.setCesionDerechosAgente(movimiento.getValorCesionDerechosAgente().negate().doubleValue());
		endosoIDTO.setCesionDerechosProm(movimiento.getValorCesionDerechosProm().negate().doubleValue());
		endosoIDTO.setSobreComisionUDIAgente(movimiento.getValorSobreComisionUDIAgente().negate().doubleValue());
		endosoIDTO.setSobreComisionUDIProm(movimiento.getValorSobreComisionUDIProm().negate().doubleValue());
		
		return endosoIDTO;
		
		
	}
	
	/**
	 * Genera movimientos para EAP del Endoso 0 (solo trae de coberturas e incisos con valor de prima)
	 * @param movimientosConPrima Listado de movimientos con prima
	 * @param cotizacionContinuity Continuidad de la cotizacion
	 */
	private void generaMovimientosEndosoCeroConPrima(List<MovimientoEndoso> movimientosConPrima, CotizacionContinuity cotizacionContinuity) {
		
		List<SeccionIncisoContinuity> seccionIncisoContinuities = null;
		List<BitemporalInciso> incisos = null;
		List<BitemporalCoberturaSeccion> coberturas = null;
		List<Integer> incisosEndosoCero=null;
		boolean conPrima = false;
		
		ControlEndosoCot controlEndosoCot = new ControlEndosoCot();
		controlEndosoCot.setSolicitud(new SolicitudDTO());
		controlEndosoCot.getSolicitud().setClaveTipoEndoso(new BigDecimal(SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS));//Cualquiera diferente a EAP
		
		DateTime fechaInicioVigenciaPoliza = getFechaInicioVigenciaPoliza (cotizacionContinuity);
				
		incisos = (List<BitemporalInciso>) cotizacionContinuity.getIncisoContinuities().get(fechaInicioVigenciaPoliza);
		
		incisosEndosoCero=this.getIncisosEndosoCero(incisos, cotizacionContinuity.getNumero());
		
		for (BitemporalInciso inciso : incisos) {
			if (!incisosEndosoCero.contains(inciso.getContinuity().getNumero())){
				continue;
			}
			
			conPrima = false;
			seccionIncisoContinuities = (List<SeccionIncisoContinuity>) inciso.getEntidadContinuity().getIncisoSeccionContinuities().getValue();
			
			coberturas = (List<BitemporalCoberturaSeccion>) seccionIncisoContinuities.get(0).getCoberturaSeccionContinuities().get(fechaInicioVigenciaPoliza);
			
			for (BitemporalCoberturaSeccion cobertura : coberturas) {
				if (cobertura.getValue().getValorPrimaDiaria() != null && cobertura.getValue().getValorPrimaDiaria().doubleValue() != 0 &&
						cobertura.getContinuity().getCoberturaDTO().getCoberturaEsPropia().equals("1")) {
					//Se agrega movimiento de cobertura
					movimientosConPrima.add(new MovimientoEndoso(controlEndosoCot, TipoMovimientoEndoso.COBERTURA, "COB END 0", cobertura));
					conPrima = true;
				}
			}
			
			if (conPrima) {
				//Se agrega movimiento de inciso
				movimientosConPrima.add(new MovimientoEndoso(controlEndosoCot, TipoMovimientoEndoso.INCISO, "INC END 0", inciso));
			}
		}
		
	}
	
	private List<Integer> getIncisosEndosoCero(List<BitemporalInciso> listaIncisos, Integer idToCotizacion){
		List<Integer> incisosEndosoCero=new ArrayList<Integer>(0);
		Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("idToCotizacion", idToCotizacion);
		properties.put("id.numeroEndoso", 0);
		
		EndosoDTO endosos = (EndosoDTO)entidadService.findByProperties(EndosoDTO.class, properties).get(0);
		for (CoberturaEndosoDTO coberturaEndoso:endosos.getCoberturaEndosoDTOs()){
			Integer numeroInciso=coberturaEndoso.getId().getNumeroInciso().intValue();
			if (!incisosEndosoCero.contains(numeroInciso)){
				incisosEndosoCero.add(numeroInciso);
			}
		}
		
		return incisosEndosoCero;
	}
	
	private DateTime getFechaInicioVigenciaPoliza (CotizacionContinuity cotizacionContinuity) {
		List <IntervalWrapper> intervalos = cotizacionContinuity.getBitemporalProperty().getMergedValidityIntervals();
		return intervalos.get(0).getInterval().getStart();
	}
	
	private DateTime getFechaFinVigenciaPoliza (CotizacionContinuity cotizacionContinuity, DateTime validOn) {
		
		BitemporalCotizacion cotizacion = null;
		
		if (cotizacionContinuity.getBitemporalProperty().getInProcess(validOn) != null) {
			cotizacion = cotizacionContinuity.getBitemporalProperty().getInProcess(validOn);
		} else if (cotizacionContinuity.getBitemporalProperty().get(validOn) != null) {
			cotizacion = cotizacionContinuity.getBitemporalProperty().get(validOn);
		} else {
			List<BitemporalCotizacion> cotizacionesHistory = cotizacionContinuity.getBitemporalProperty().getHistory();
			if (cotizacionesHistory != null && !cotizacionesHistory.isEmpty()) {
				cotizacion = cotizacionesHistory.get(cotizacionesHistory.size() - 1);
			}
		}
		
		return TimeUtils.getDateTime(cotizacion.getValue().getFechaFinVigencia());
		
	}
	
	private List<MovimientoEndoso> getMovimientosEndososEAPAnteriores (List<MovimientoEndoso> movimientosEndoso, ControlEndosoCot controlEndosoCot, 
			short tipoEndoso) {
		
		List<MovimientoEndoso> movimientosEndosoEAPAnteriores = new ArrayListNullAware<MovimientoEndoso>();
		List<IncisoContinuity> incisosBaja = null;
		ControlEndosoCot controlEndosoCotCE = new ControlEndosoCot();
		controlEndosoCotCE.setSolicitud(new SolicitudDTO());
		controlEndosoCotCE.getSolicitud().setClaveTipoEndoso(new BigDecimal(SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO));
		controlEndosoCotCE.setValidFrom(controlEndosoCot.getValidFrom());
		controlEndosoCotCE.setCotizacionId(controlEndosoCot.getCotizacionId());
		
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO) {
			
			incisosBaja = new ArrayListNullAware<IncisoContinuity>();
			
			//Para este endoso los movimientos deben ser de inciso
			for (MovimientoEndoso movimiento : movimientosEndoso) {
				incisosBaja.add(movimiento.getBitemporalInciso().getContinuity());
			}
			
		}
		
		//Se obtiene un listado de ControlEndosoCot de endosos EAP con fecha de inicio de vigencia menor o igual a la fecha de inicio de vigencia del endoso de cancelacion
		//que aun no hayan sido cancelados
		List<ControlEndosoCot> controlEndosoCotEAPList = cotizacionEndosoDao.getControlEndosoCotEndososEAPAnteriores(controlEndosoCot);
		esMergeMovimientoAjuste = true; //LPV Bandera para no validar pagos en EAP en Cancelaciones
		for (ControlEndosoCot controlEndosoCotEAP : controlEndosoCotEAPList) {
			
			controlEndosoCotCE.setIdCancela(controlEndosoCotEAP.getId());
			if (movimientosEndosoEAPAnteriores.size() == 0) {
				movimientosEndosoEAPAnteriores = generarMovimientosEndoso(controlEndosoCotCE, incisosBaja);
			} else {
				mergeMovimientosEndoso(controlEndosoCot, movimientosEndosoEAPAnteriores, generarMovimientosEndoso(controlEndosoCotCE, incisosBaja), null);
			}
			
		}
		esMergeMovimientoAjuste = false; //LPV Bandera para no validar pagos en EAP en Cancelaciones
		return movimientosEndosoEAPAnteriores;
		
	}
	
	private List<MovimientoEndoso> getMovimientosEndososAFuturo (List<MovimientoEndoso> movimientosEndoso, ControlEndosoCot controlEndosoCot, 
			short tipoEndoso) {
		
		List<MovimientoEndoso> movimientosIncisoEndososAFuturo = new ArrayListNullAware<MovimientoEndoso>();
		List<MovimientoEndoso> movimientosCoberturaEndososAFuturo = new ArrayListNullAware<MovimientoEndoso>();
		List<MovimientoEndoso> movimientosAFuturoResponse = new ArrayListNullAware<MovimientoEndoso>();
		String strIncisosBaja = null;
		List<MovimientoEndoso> movimientosInciso = null;
		MovimientoEndoso movimientoIncisoAFuturo = null;
		MovimientoEndoso movimientoCoberturaAFuturo = null;
		
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO) {
			//Para este endoso los movimientos deben ser de inciso
			movimientosInciso = movimientosEndoso;
			
			strIncisosBaja = obtenerStrIncisosBaja(movimientosInciso);
			strIncisosBaja = strIncisosBaja.substring(0, strIncisosBaja.lastIndexOf(","));
		} else {
			//Se trata de una cancelacion de poliza y tiene un movimiento de tipo POLIZA
			movimientosInciso = movimientosEndoso.get(0).getMovimientosEndoso();
		}
		
		//Se obtiene un listado de MovimientoEndoso con todos los inversos de los valores de los conceptos multiplicados por sus respectivos 
		//factores de igualacion de todos los movimientos de cobertura de los incisos a dar de baja en todos los controlEndosoCot de la poliza 
		//con fecha de validez mayor a la fecha de inicio de vigencia del endoso
		//que aun no hayan sido cancelados
		movimientosIncisoEndososAFuturo = getSumarizadoMovimientosCoberturaEndososAFuturo(controlEndosoCot, strIncisosBaja,true);
		
		Predicate findIncisoId = new Predicate() {
			@Override
			public boolean evaluate(Object arg0) {
				MovimientoEndoso obj = (MovimientoEndoso)arg0; 
				return obj.getBitemporalInciso().getId().equals(MovimientoEndosoBitemporalServiceNewImpl.getBitemporalId());
			}
		};
		
		Predicate findIncisoContinuityId = new Predicate() {
			@Override
			public boolean evaluate(Object arg0) {
				MovimientoEndoso obj = (MovimientoEndoso)arg0; 
				return obj.getBitemporalInciso().getContinuity().getId().equals(MovimientoEndosoBitemporalServiceNewImpl.getContinuityId());
			}
		};
		
		for (MovimientoEndoso movimientoIncFuturo : movimientosIncisoEndososAFuturo) {
							
			for (MovimientoEndoso movimientoInciso : movimientosInciso) {
				
				MovimientoEndosoBitemporalServiceNewImpl.setContinuityId(movimientoIncFuturo.getBitemporalInciso().getContinuity().getId());
				movimientoIncisoAFuturo = (MovimientoEndoso)CollectionUtils.find(movimientosAFuturoResponse, findIncisoContinuityId);
				
				if(movimientoIncisoAFuturo == null){				
	
				if (isSameContinuity(movimientoInciso, movimientoIncFuturo)) {
					//Obtiene inciso a procesar
					Integer numeroInciso = movimientoIncFuturo.getBitemporalInciso().getContinuity().getNumero();
					//Obtiene coberturas
					movimientosCoberturaEndososAFuturo = getSumarizadoMovimientosCoberturaEndososAFuturo(controlEndosoCot, numeroInciso.toString(),false);
			
					for (MovimientoEndoso movimientoCobertura : movimientosCoberturaEndososAFuturo) {
						
						MovimientoEndosoBitemporalServiceNewImpl.setBitemporalId(movimientoInciso.getBitemporalInciso().getId());
						movimientoIncisoAFuturo = (MovimientoEndoso)CollectionUtils.find(movimientosAFuturoResponse, findIncisoId);
						
						if (movimientoIncisoAFuturo == null) {
							
							movimientoIncisoAFuturo = new MovimientoEndoso(movimientoInciso);
							movimientoIncisoAFuturo.setDescripcion("MOV. INCISO A FUTURO");
							movimientoIncisoAFuturo.setValorMovimiento(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaPrimaNeta(UNO);
							movimientoIncisoAFuturo.setValorBonificacionComision(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaBonificacionComision(UNO);
							movimientoIncisoAFuturo.setValorBonificacionRPF(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaBonificacionRPF(UNO);
							movimientoIncisoAFuturo.setValorRPF(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaRPF(UNO);
							movimientoIncisoAFuturo.setValorComision(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaComision(UNO);
							movimientoIncisoAFuturo.setValorComisionRPF(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaComisionRPF(UNO);
							movimientoIncisoAFuturo.setValorDerechos(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaDerechos(UNO);
							movimientoIncisoAFuturo.setValorIva(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaIva(UNO);
							movimientoIncisoAFuturo.setValorSobreComisionAgente(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaSobreComisionAgente(UNO);
							movimientoIncisoAFuturo.setValorSobreComisionProm(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaSobreComisionProm(UNO);
							movimientoIncisoAFuturo.setValorBonoAgente(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaBonoAgente(UNO);
							movimientoIncisoAFuturo.setValorBonoProm(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaBonoProm(UNO);
							movimientoIncisoAFuturo.setValorCesionDerechosAgente(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaCesionDerechosAgente(UNO);
							movimientoIncisoAFuturo.setValorCesionDerechosProm(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaCesionDerechosProm(UNO);
							movimientoIncisoAFuturo.setValorSobreComisionUDIAgente(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaSobreComisionUDIAgente(UNO);
							movimientoIncisoAFuturo.setValorSobreComisionUDIProm(CERO);
							movimientoIncisoAFuturo.setFactorCobranzaSobreComisionUDIProm(UNO);
							
							movimientosAFuturoResponse.add(movimientoIncisoAFuturo);
						}
						
						movimientoCoberturaAFuturo = new MovimientoEndoso(movimientoCobertura);
						
						movimientoCoberturaAFuturo.setMovimientoEndoso(movimientoIncisoAFuturo);
						movimientoIncisoAFuturo.getMovimientosEndoso().add(movimientoCoberturaAFuturo);
						
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.PRIMA_NETA, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.BONIFICACION_COMISION, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.BONIFICACION_RPF, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.RPF, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.COMISION, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.COMISION_RPF, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.DERECHOS, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.IVA, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.SOBRECOMISIONAGENTE, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.SOBRECOMISIONPROM, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.BONOAGENTE, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.BONOPROM, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.CESIONDERECHOSAGENTE, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.CESIONDERECHOSPROM, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.SOBRECOMISIONUDIAGENTE, null);
						incrementaValorConceptoInciso(movimientoCoberturaAFuturo, Concepto.SOBRECOMISIONUDIPROM, null);
					
					}
				} else {

					movimientoIncisoAFuturo = new MovimientoEndoso(movimientoIncFuturo);
					movimientoIncisoAFuturo.setDescripcion("MOV. INCISO A FUTURO");					
					movimientosAFuturoResponse.add(movimientoIncisoAFuturo);
					//Obtiene inciso a procesar
					Integer numeroInciso = movimientoIncFuturo.getBitemporalInciso().getContinuity().getNumero();
					//Obtiene coberturas
					movimientosCoberturaEndososAFuturo = getSumarizadoMovimientosCoberturaEndososAFuturo(controlEndosoCot, numeroInciso.toString(),false);
					
					for (MovimientoEndoso movimientoCobertura : movimientosCoberturaEndososAFuturo) {
						movimientoCoberturaAFuturo = new MovimientoEndoso(movimientoCobertura);						
						movimientoCoberturaAFuturo.setMovimientoEndoso(movimientoIncisoAFuturo);
						movimientoIncisoAFuturo.getMovimientosEndoso().add(movimientoCoberturaAFuturo);						
					}
				}
			}
			}
		}
				
		return movimientosAFuturoResponse;
		
	}
	
	public String obtenerStrIncisosBaja(
			List<MovimientoEndoso> movimientosInciso) {
		StringBuilder strIncisosBaja = new StringBuilder("");
		
		for (MovimientoEndoso movimiento : movimientosInciso) {
			strIncisosBaja.append(movimiento.getBitemporalInciso().getContinuity().getNumero()).append(",");
		}
		return strIncisosBaja.toString();
	}

	private List<MovimientoEndoso> getSumarizadoMovimientosCoberturaEndososAFuturo(ControlEndosoCot controlEndosoCot, String strIncisosBaja, boolean groupInciso) {
		
		BitemporalCoberturaSeccion cobertura = null;
		BitemporalInciso bitemporalInciso = null;
		MovimientoEndoso movimiento = null;
		List<MovimientoEndoso> movimientos = new ArrayListNullAware<MovimientoEndoso>();
		
		List <Object[]> sumarizados = cotizacionEndosoDao.getSumarizadoMovimientosCoberturaEndososAFuturo(controlEndosoCot, strIncisosBaja, groupInciso);
		
		/*
		COBEND.IDTOCOBERTURA,
		SUM (COBEND.VALORPRIMANETA) * -1 AS VALORPRIMANETA,
		SUM (COBEND.VALORBONIFCOMISION) * -1 AS VALORBONIFCOMISION,
		SUM (COBEND.VALORBONIFCOMRECPAGOFRAC) * -1 AS VALORBONIFCOMRECPAGOFRAC,
		SUM (COBEND.VALORRECARGOPAGOFRAC) * -1 AS VALORRECARGOPAGOFRAC,
		SUM (COBEND.VALORCOMISION) * -1 AS VALORCOMISION,
		SUM (COBEND.VALORCOMISIONRECPAGOFRAC) * -1 AS VALORCOMISIONRECPAGOFRAC,
		SUM (COBEND.VALORDERECHOS) * -1 AS VALORDERECHOS,
		SUM (COBEND.VALORIVA) * -1 AS VALORIVA,
		MAX (IDTOSECCION) AS MAXSECCION,
		SUM (COBEND.VALORSOBRECOMISIONAGENTE) * -1 AS VALORSOBRECOMISIONAGENTE,
		SUM (COBEND.VALORSOBRECOMISIONPROM) * -1 AS VALORSOBRECOMISIONPROM,
		SUM (COBEND.VALORBONOAGENTE) * -1 AS VALORBONOAGENTE,
		SUM (COBEND.VALORBONOPROM) * -1 AS VALORBONOPROM,
		SUM (COBEND.VALORCESIONDERECHOSAGENTE) * -1 AS VALORCESIONDERECHOSAGENTE,
		SUM (COBEND.VALORCESIONDERECHOSPROM) * -1 AS VALORCESIONDERECHOSPROM,
		SUM (COBEND.VALORSOBRECOMUDIAGENTE) * -1 AS VALORSOBRECOMUDIAGENTE,
		SUM (COBEND.VALORSOBRECOMUDIPROM) * -1 AS VALORSOBRECOMUDIPROM
		*/
		
		
		//Se transforman a movimientos
		for (Object[] row : sumarizados) {
			
			//El primer elemento es el id de la cobertura, el ultimo es el id maximo de seccion
			movimiento = new MovimientoEndoso();
			movimiento.setControlEndosoCot(controlEndosoCot);
			movimiento.setDescripcion("A FUTURO");			
			
			if(groupInciso){
				bitemporalInciso = getIncisoBitemporalValido(controlEndosoCot.getCotizacionId(), new BigDecimal(row[0] +"")); 
				movimiento.setTipo(TipoMovimientoEndoso.INCISO);
				movimiento.setBitemporalInciso(bitemporalInciso);								
			}else{
				cobertura = getCoberturaBitemporalValida(controlEndosoCot.getCotizacionId(), new BigDecimal(row[0] +""), new BigDecimal(row[9] +"")); 
				movimiento.setTipo(TipoMovimientoEndoso.COBERTURA);
				movimiento.setBitemporalCoberturaSeccion(cobertura);
			}
			
			movimiento.setValorMovimiento(new BigDecimal(row[1] +""));
			movimiento.setFactorCobranzaPrimaNeta(UNO);
			
			movimiento.setValorBonificacionComision(new BigDecimal(row[2] +""));
			movimiento.setFactorCobranzaBonificacionComision(UNO);
			
			movimiento.setValorBonificacionRPF(new BigDecimal(row[3] +""));
			movimiento.setFactorCobranzaBonificacionRPF(UNO);
			
			movimiento.setValorRPF(new BigDecimal(row[4] +""));
			movimiento.setFactorCobranzaRPF(UNO);
			
			movimiento.setValorComision(new BigDecimal(row[5] +""));
			movimiento.setFactorCobranzaComision(UNO);
			
			movimiento.setValorComisionRPF(new BigDecimal(row[6] +""));
			movimiento.setFactorCobranzaComisionRPF(UNO);
			
			movimiento.setValorDerechos(new BigDecimal(row[7] +""));
			movimiento.setFactorCobranzaDerechos(UNO);
			
			movimiento.setValorIva(new BigDecimal(row[8] +""));
			movimiento.setFactorCobranzaIva(UNO);
			
			movimiento.setValorSobreComisionAgente(new BigDecimal(row[10] +""));
			movimiento.setFactorCobranzaSobreComisionAgente(UNO);
			
			movimiento.setValorSobreComisionProm(new BigDecimal(row[11] +""));
			movimiento.setFactorCobranzaSobreComisionProm(UNO);
			
			movimiento.setValorBonoAgente(new BigDecimal(row[12] +""));
			movimiento.setFactorCobranzaBonoAgente(UNO);
			
			movimiento.setValorBonoProm(new BigDecimal(row[13] +""));
			movimiento.setFactorCobranzaBonoProm(UNO);
			
			movimiento.setValorCesionDerechosAgente(new BigDecimal(row[14] +""));
			movimiento.setFactorCobranzaCesionDerechosAgente(UNO);
			
			movimiento.setValorCesionDerechosProm(new BigDecimal(row[15] +""));
			movimiento.setFactorCobranzaCesionDerechosProm(UNO);
			
			movimiento.setValorSobreComisionUDIAgente(new BigDecimal(row[16] +""));
			movimiento.setFactorCobranzaSobreComisionUDIAgente(UNO);
			
			movimiento.setValorSobreComisionUDIProm(new BigDecimal(row[17] +""));
			movimiento.setFactorCobranzaSobreComisionUDIProm(UNO);
						
			if(movimiento.getBitemporalInciso() != null || movimiento.getBitemporalCoberturaSeccion() != null){
				movimientos.add(movimiento);
			}
		}
		
		return movimientos;
		
	}
	
	/**
	 * Obtiene el primer bitemporal valido para la cobertura solicitada
	 * @param coberturaId id de CoberturaDTO
	 * @param seccionId id de SeccionDTO
	 * @return El primer bitemporal valido para la cobertura solicitada
	 */
	private BitemporalCoberturaSeccion getCoberturaBitemporalValida (Long cotizacionId, BigDecimal coberturaId, BigDecimal seccionId) {
		
		Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("seccionIncisoContinuity.incisoContinuity.cotizacionContinuity.numero", cotizacionId);
		properties.put("seccionIncisoContinuity.incisoContinuity.numero", seccionId);
		properties.put("coberturaDTO.idToCobertura", coberturaId);
		
		List<CoberturaSeccionContinuity> coberturaSeccionContinuities = entidadService.findByPropertiesWithOrder(CoberturaSeccionContinuity.class, properties);
		
		if (coberturaSeccionContinuities != null && !coberturaSeccionContinuities.isEmpty()) {
			for(int i = 0; i < coberturaSeccionContinuities.size(); i++){
			CoberturaSeccionContinuity coberturaSeccionContinuity = coberturaSeccionContinuities.get(i);
			List <IntervalWrapper> intervalos = coberturaSeccionContinuity.getBitemporalProperty().getCurrentMergedValidityIntervals(true);
			try{
			if (intervalos != null && !intervalos.isEmpty()) {
				return coberturaSeccionContinuity.getBitemporalProperty().get(intervalos.get(0).getInterval().getStart());
			} else {
				return coberturaSeccionContinuity.getBitemporalProperty().getHistory().get(0);
			}
			}catch(Exception e){
			}
			}
		}
		
		return null;
	}
	
	private BitemporalInciso getIncisoBitemporalValido (Long cotizacionId, BigDecimal numeroInciso) {
		
		Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("cotizacionContinuity.numero", cotizacionId);
		properties.put("numero", numeroInciso);
		
		List<IncisoContinuity> incisoContinuities = entidadService.findByPropertiesWithOrder(IncisoContinuity.class, properties);
		
		if (incisoContinuities != null && !incisoContinuities.isEmpty()) {
			for(int i = 0; i < incisoContinuities.size(); i++){
			IncisoContinuity incisoContinuity = incisoContinuities.get(i);
			List <IntervalWrapper> intervalos = incisoContinuity.getBitemporalProperty().getCurrentMergedValidityIntervals(true);
			try{
			if (intervalos != null && !intervalos.isEmpty()) {
				return incisoContinuity.getBitemporalProperty().get(intervalos.get(0).getInterval().getStart());
			} else {
				return incisoContinuity.getBitemporalProperty().getHistory().get(0);
			}
			}catch(Exception e){
			}
			}
		}
		
		return null;
	}	
	
	/**
	 * LPV Calculo de prima Neta en endosos de movimiento para validar aplicacion de derechos
	 */
	
	private void calculaMovimientoPrimaNeta(MovimientoEndoso movimiento, AuxiliarCalculoDTO auxiliar) {
		
		ControlEndosoCot controlEndosoCot = movimiento.getControlEndosoCot();
		MovimientoEndoso movimientoInverso = null;
		BitemporalCoberturaSeccion coberturaSource = null;
		BitemporalCoberturaSeccion coberturaTarget = null;

		Double primaDiaria = 0D;
		BigDecimal valorTotalPrimas = CERO;
		
		short tipoEndoso = controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue();
		boolean calcularConceptos = true;
		Object source = movimiento.getFuenteMovimiento().getSource();
		Object target = movimiento.getFuenteMovimiento().getTarget();
		TipoOperacion tipoOperacion = getTipoOperacion (source, target, tipoEndoso);
				
		if (target != null && target instanceof MovimientoEndoso) {
			movimientoInverso = (MovimientoEndoso) target;
		}
		
		if (movimiento.getTipo().equals(TipoMovimientoEndoso.COBERTURA)) {
						
			//Comienza el calculo de los conceptos			
			switch (tipoEndoso) {
				case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO: {
					break;
				}
				case SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA: {
					
					if (movimientoInverso.getControlEndosoCot().getSolicitud()
							.getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA) {
						
						//Si se trata del calculo de los incisos ajustados para el EAP
						valorTotalPrimas = movimientoInverso.getValorMovimiento();
						
					} else {
						
						calcularConceptos = false;
					
					}
					
					break;
				}
				default: {
					if (movimientoInverso != null) {
						
						if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO) {
							//Cancelacion de EAP
							/**
							 -El valor de prima diaria del Endoso EAP se debe obtener con el producto del valor del movimiento 
							 (prima neta o Total de Primas) y su respectivo factor de igualacion, dividido entre los dias de vigencia del endoso EAP.

							 -Obtener el inverso del producto del valor de prima diaria por los dias de vigencia del endoso de Cancelacion de Endoso. 
							 Este será el nuevo valor de Total de Primas para el endoso de Cancelacion de Endoso.
							 */
							Integer diferenciaDias = Days.daysBetween(
									TimeUtils.getDateTime(movimientoInverso.getControlEndosoCot().getValidFrom()), 
									TimeUtils.getDateTime(controlEndosoCot.getValidFrom())).getDays();
							
							Integer diasVigenciaEAP = auxiliar.getDiasVigencia() + diferenciaDias;
							
							BigDecimal primaDiariaEAP = (movimientoInverso.getValorMovimiento().multiply(movimientoInverso.getFactorCobranzaPrimaNeta()))
								.setScale(16, BigDecimal.ROUND_HALF_UP).divide(new BigDecimal(diasVigenciaEAP), mathCtxFactor);
							
							valorTotalPrimas = primaDiariaEAP.multiply(new BigDecimal(auxiliar.getDiasVigencia())).negate();
							
					} else {
						// Rehabilitaciones
						calcularConceptos = false;				

						valorTotalPrimas = movimientoInverso.getValorMovimiento().negate();
											   
						// Se asignan al movimiento los factores de cobranza
						movimiento.setFactorCobranzaPrimaNeta(movimientoInverso.getFactorCobranzaPrimaNeta());

					}
					} else {
						
						if (tipoOperacion.equals(TipoOperacion.MODIFICADO)) {
							coberturaSource = (BitemporalCoberturaSeccion) getBitemporalObject(source);
							coberturaTarget = (BitemporalCoberturaSeccion) getBitemporalObject(target);
							
							primaDiaria = (coberturaTarget.getValue().getValorPrimaDiaria()!= null?coberturaTarget.getValue().getValorPrimaDiaria(): 0) - 
								(coberturaSource.getValue().getValorPrimaDiaria()!= null?coberturaSource.getValue().getValorPrimaDiaria():0);
						
						} else if (tipoOperacion.equals(TipoOperacion.BORRADO)) {
							
							if(movimiento.getFuenteMovimiento().getTarget() != null && movimiento.getFuenteMovimiento().getSource() instanceof BitemporalCoberturaSeccion)
							{
								primaDiaria = (((BitemporalCoberturaSeccion)movimiento.getFuenteMovimiento().getSource()).getValue().getValorPrimaDiaria()!=null
										?((BitemporalCoberturaSeccion)movimiento.getFuenteMovimiento().getSource()).getValue().getValorPrimaDiaria():0) * -1;
								
							}else
							{
								primaDiaria = (movimiento.getBitemporalCoberturaSeccion().getValue().getValorPrimaDiaria()!=null
										?movimiento.getBitemporalCoberturaSeccion().getValue().getValorPrimaDiaria():0) * -1;								
							}							
							
						} else {
							primaDiaria = (movimiento.getBitemporalCoberturaSeccion().getValue().getValorPrimaDiaria()!=null
									?movimiento.getBitemporalCoberturaSeccion().getValue().getValorPrimaDiaria():0);
						}
						
						valorTotalPrimas = new BigDecimal(primaDiaria * auxiliar.getDiasVigencia());
						
					}
					break;
				}
			}
			
			if (calcularConceptos && !valorTotalPrimas.equals(BigDecimal.ZERO)) {
				
				//
			}
									
			//Se asignan al movimiento los valores primarios
			movimiento.setValorMovimiento(valorTotalPrimas);
			
			//Se acumulan los contadores de las coberturas con conceptos diferentes a cero
			incrementaContadorCoberturasConcepto(movimiento, Concepto.PRIMA_NETA, 1);

						
			//Se acumulan los valores primarios a nivel de inciso
			incrementaValorConceptoInciso(movimiento, Concepto.PRIMA_NETA, null);

			
		} else if (movimiento.getTipo().equals(TipoMovimientoEndoso.INCISO)) {
			
						
			//Se calculan sus movimientos de cobertura
			for (MovimientoEndoso movimientoCobertura : movimiento.getMovimientosEndoso()) {
				calculaMovimientoPrimaNeta(movimientoCobertura, auxiliar);
			}
			
			//Se calculan los factores Cobranza a nivel inciso
			calculaFactoresCobranzaInciso(movimiento);
			
			
			if(tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA 
					|| tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO || 
					tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS) //Nota:Cuando se trata de un endoso de Rehabilitacion los factores ya vienen establecidos.
			{
				// Se asignan al movimiento los factores de cobranza
				movimiento.setFactorCobranzaPrimaNeta(movimientoInverso.getFactorCobranzaPrimaNeta());
								
			}
			else
			{
				//Se calculan los factores Cobranza a nivel cobertura
				for (MovimientoEndoso movimientoCobertura : movimiento.getMovimientosEndoso()) {
					calculaFactoresCobranzaCoberturaPrimaNeta(movimientoCobertura);
				}				
			}
			
			
		} else if (movimiento.getTipo().equals(TipoMovimientoEndoso.POLIZA)) {
			//Se calculan sus movimientos de inciso
			for (MovimientoEndoso movimientoInciso : movimiento.getMovimientosEndoso()) {
				calculaMovimientoPrimaNeta(movimientoInciso, auxiliar);
			}
		}
		
	}
	
	private void calculaFactoresCobranzaCoberturaPrimaNeta(MovimientoEndoso movimientoCobertura) {
			
		
		if (movimientoCobertura.getMovimientoEndoso().getValidacionInciso() == null) {
			return;
		}
			
		if (movimientoCobertura.getMovimientoEndoso().getAjusteValorCeroPrimaNeta()) {
			movimientoCobertura.setValorMovimiento(getAjusteValorCeroCobertura(movimientoCobertura.getMovimientoEndoso().getValorMovimiento()));
		}
		
		
		movimientoCobertura.setFactorCobranzaPrimaNeta(getFactorCobranzaCobertura(movimientoCobertura, Concepto.PRIMA_NETA));
	}
	
	/**
	 * Ajusta los signos de cada de uno de los conceptos de movimientoEndoso, en base a los valores devueltos 
	 * en el objeto validacionInciso.
	 * 
	 * @param movimientoEndoso el movimiento que se desea ajustar
	 * @param invertirSigno bandera auxiliar, en caso de requerir invertir los signos setear en true.
	 *  
	 */
	private void forzarSignosConceptosValidacionesInciso(MovimientoEndoso movimientoEndoso, Boolean invertirSigno)
	{
		if (movimientoEndoso.getValidacionInciso() == null) {
			return;
		}
		
		BigDecimal s1 = UNO;
		BigDecimal s2 = UNO.negate();
		
		if(invertirSigno != null && invertirSigno)
		{
			s1 = UNO.negate();
			s2 = UNO;
		}
		
		BigDecimal sigNum;
		
		sigNum = movimientoEndoso.getValidacionInciso().getPrimaNeta() > 0 ? s1 : s2;
		movimientoEndoso.setValorMovimiento(movimientoEndoso.getValorMovimiento().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getRecargoPF() > 0 ? s1 : s2;
		movimientoEndoso.setValorRPF(movimientoEndoso.getValorRPF().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getComision() > 0 ? s1 : s2;
		movimientoEndoso.setValorComision(movimientoEndoso.getValorComision().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getComisionRPF() > 0 ? s1 : s2;
		movimientoEndoso.setValorComisionRPF(movimientoEndoso.getValorComisionRPF().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getBonificacionComision() > 0 ? s1 : s2;
		movimientoEndoso.setValorBonificacionComision(movimientoEndoso.getValorBonificacionComision().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getBonificacionComisionRPF() > 0 ? s1 : s2;
		movimientoEndoso.setValorBonificacionRPF(movimientoEndoso.getValorBonificacionRPF().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getDerechos() > 0 ? s1 : s2;
		movimientoEndoso.setValorDerechos(movimientoEndoso.getValorDerechos().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getIva() > 0 ? s1 : s2;
		movimientoEndoso.setValorIva(movimientoEndoso.getValorIva().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getSobreComisionAgente() > 0 ? s1 : s2;
		movimientoEndoso.setValorSobreComisionAgente(movimientoEndoso.getValorSobreComisionAgente().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getSobreComisionProm() > 0 ? s1 : s2;
		movimientoEndoso.setValorSobreComisionProm(movimientoEndoso.getValorSobreComisionProm().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getBonoAgente() > 0 ? s1 : s2;
		movimientoEndoso.setValorBonoAgente(movimientoEndoso.getValorBonoAgente().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getBonoProm() > 0 ? s1 : s2;
		movimientoEndoso.setValorBonoProm(movimientoEndoso.getValorBonoProm().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getCesionDerechosAgente() > 0 ? s1 : s2;
		movimientoEndoso.setValorCesionDerechosAgente(movimientoEndoso.getValorCesionDerechosAgente().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getCesionDerechosProm() > 0 ? s1 : s2;
		movimientoEndoso.setValorCesionDerechosProm(movimientoEndoso.getValorCesionDerechosProm().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getSobreComisionUDIAgente() > 0 ? s1 : s2;
		movimientoEndoso.setValorSobreComisionUDIAgente(movimientoEndoso.getValorSobreComisionUDIAgente().abs().multiply(sigNum));
		
		sigNum = movimientoEndoso.getValidacionInciso().getSobreComisionUDIProm() > 0 ? s1 : s2;
		movimientoEndoso.setValorSobreComisionUDIProm(movimientoEndoso.getValorSobreComisionUDIProm().abs().multiply(sigNum));
		
	}
	
	
	/**
	 * XXX Variables de Clase
	 */
	private final MathContext mathCtxFactor = new MathContext(16, RoundingMode.HALF_UP);
	private final BigDecimal CERO = BigDecimal.ZERO;
	private final BigDecimal UNO = BigDecimal.ONE;
	private final BigDecimal CIEN = new BigDecimal(100D);
	private enum TipoOperacion {
		AGREGADO, BORRADO, MODIFICADO, NINGUNO
	}
	private static final SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	
	private static final String calculoSeycos = "S";
	
	private List<FuenteMovimientoDTO> fuentesMovimiento = new ArrayListNullAware<FuenteMovimientoDTO>();
	
	private List<CoberturaCobranzaDTO> coberturasCobranza = new ArrayListNullAware<CoberturaCobranzaDTO>();
	
	private static Integer numeroInciso;
	private static Integer idCobertura;

	private static Long bitemporalId;
	private static Long continuityId;
	
	public static Integer getNumeroInciso() {
		return numeroInciso;
	}

	public static void setNumeroInciso(Integer numeroInciso) {
		MovimientoEndosoBitemporalServiceNewImpl.numeroInciso = numeroInciso;
	}
	
	public static Integer getIdCobertura() {
		return idCobertura;
	}

	public static void setIdCobertura(Integer idCobertura) {
		MovimientoEndosoBitemporalServiceNewImpl.idCobertura = idCobertura;
	}
	
	public static Long getBitemporalId() {
		return bitemporalId;
	}

	public static void setBitemporalId(Long bitemporalId) {
		MovimientoEndosoBitemporalServiceNewImpl.bitemporalId = bitemporalId;
	}


	/**
	 * XXX Referencias a servicios
	 */
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private FormaPagoFacadeRemote formaPagoFacadeRemote;
	
	@EJB
	private MovimientoEndosoDao movimientoEndosoDao;
	
	@EJB
	private AgenteMidasService agenteMidasService;
	
	@EJB
	private BitemporalDao bitemporalDao;
	
	@EJB
	private EntidadContinuityService entidadContinuityService;
	
	@EJB
	private EntidadBitemporalService entidadBitemporalService;
	
	@EJB
	private CobranzaService cobranzaService;
	
	@EJB
	private EndosoService endosoService;
	
	@EJB
	private CotizacionEndosoDao cotizacionEndosoDao;
	
	@EJB
	private DomicilioFacadeRemote domicilioFacade;
	
	@EJB
	private ClienteFacadeRemote clienteFacade;
	
	@EJB
	private PolizaSiniestroService polizaSiniestroService;
	
	@EJB
	private FormaPagoInterfazServiciosRemote formaPagoInterfazServiciosRemote;	
	
	@EJB
	private EntidadContinuityDao entidadContinuityDao;	

	@Override
	public String generarDescripcionMovimientoGeneral(ControlEndosoCot arg0,
			short arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void setContinuityId(Long continuityId) {
		MovimientoEndosoBitemporalServiceNewImpl.continuityId = continuityId;
	}

	public static Long getContinuityId() {
		return continuityId;
	}
	
}
