package mx.com.afirme.midas2.service.impl.bitemporal.endoso.inciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.inciso.IncisoService;
import mx.com.afirme.midas2.service.bitemporal.endoso.inciso.IncisoSoporteService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.joda.time.DateTime;

@Stateless(name="IncisoEndosoServiceImpl")
public class IncisoServiceImpl implements IncisoService {

	@Override
	public BitemporalInciso asociarIncisoEnCotizacion(BitemporalInciso inciso, DateTime validoEn) {

		inciso.getEntidadContinuity().getAutoIncisoContinuity().getBitemporalProperty()
			.getInProcess(validoEn).getValue().setAsociadaCotizacion(1);
		
		inciso.setEntidadContinuity(entidadContinuityDao.update(inciso.getEntidadContinuity()));
		
		return inciso;
		
	}

	@Override
	public void bajaInciso(Long incisoId, DateTime validoEn) {
		BitemporalInciso inciso = entidadBitemporalService.getInProcessByKey(IncisoContinuity.class, incisoId, validoEn);
		entidadBitemporalService.remove(inciso, validoEn);
	}

	@Override
	public BitemporalInciso getInciso(Long cotizacionId, Long incisoId, DateTime validoEn) {
		if (incisoId != null) {
			return entidadBitemporalService.getInProcessByKey(IncisoContinuity.class, incisoId, validoEn);
		} else {
			return entidadBitemporalService.getNew(BitemporalInciso.class, cotizacionId);
		}
	}
	
	@Override
	public BitemporalAutoInciso getAutoInciso(Long autoIncisoId, DateTime validoEn) {
		if (autoIncisoId != null) {
			return entidadBitemporalService.getInProcessByKey(AutoIncisoContinuity.class, autoIncisoId, validoEn);
		} else {
			return entidadBitemporalService.getNew(BitemporalAutoInciso.class, null);
		}
	}
	
	@Override
	public BitemporalInciso prepareGuardarIncisoBorrador(BitemporalInciso inciso, BitemporalAutoInciso autoInciso, Map<String, String> datosRiesgo,
			List<BitemporalCoberturaSeccion> coberturas, DateTime validoEn) {
		return incisoSoporteService.guardaIncisoBorrador(inciso, autoInciso, datosRiesgo, coberturas, validoEn);
	}
	
	
	@Override
	public BitemporalInciso calcularIncisoBorrador(BitemporalInciso inciso, BitemporalAutoInciso autoInciso, Map<String, String> datosRiesgo,
			List<BitemporalCoberturaSeccion> coberturas, DateTime validoEn) {
		//Se crea/actualiza el inciso
		inciso = incisoSoporteService.guardaIncisoBorrador(inciso, autoInciso, datosRiesgo, coberturas, validoEn);
		
		//Se calcula el inciso
		inciso = calculoService.calcular(inciso, validoEn, null, false);
		
		//Regresa el inciso creado/actualizado
		return inciso;
		
	}
	
	@Override
	public BitemporalInciso multiplicarInciso(Long incisoId, DateTime validoEn) {				
		
		return incisoSoporteService.copiarInciso(incisoId, validoEn);
	}
	
	@Override
	/**
	 * Reasignar la secuencia de los incisos que esten en cotizacion en endoso de alta de inciso 
	 * cuando se elimina un inciso de este grupo
	 */
	public void reasignarSecuencia(Long cotizacionContinuityId, Integer numeroInciso) {		
		
		String query = "SELECT incisob FROM BitemporalInciso incisob WHERE incisob.continuity.cotizacionContinuity.id = " +  cotizacionContinuityId 
		 + " AND incisob.value.numeroSecuencia > " + numeroInciso + " AND incisob.continuity.numero > " +  numeroInciso + " ORDER BY  incisob.continuity.numero" ;

		List<BitemporalInciso> lstBitemporalInciso = entidadService.executeQueryMultipleResult(query, null);		

		if (!lstBitemporalInciso.isEmpty()) {
			for (BitemporalInciso biInciso: lstBitemporalInciso) {
				
				biInciso.getValue().setNumeroSecuencia(biInciso.getValue().getNumeroSecuencia() - 1);
				biInciso.getContinuity().setNumero(biInciso.getContinuity().getNumero() - 1);
				
				entidadContinuityDao.update(biInciso.getContinuity());
			}			
		}

	}
	
	public EndosoDTO getUltimoEndosoInciso(BigDecimal idToPoliza, Long incisoId){
		EndosoDTO endoso = null;
				
		try{
			
			IncisoContinuity inciso = entidadContinuityDao.findByKey(IncisoContinuity.class, incisoId);
			
			String queryString = "Select Distinct model " +
			" FROM EndosoDTO model, CoberturaEndosoDTO ce  " +
			" WHERE model.id.idToPoliza = :idToPoliza " +
			" AND model.id.idToPoliza = ce.id.idToPoliza " +
			" AND model.id.numeroEndoso = ce.id.numeroEndoso " +
			" and ce.id.numeroInciso = :numeroInciso " +
			" and model.id.numeroEndoso = ( " +
			"	Select max(ce1.id.numeroEndoso) " +
			"	From CoberturaEndosoDTO ce1 " +
			"	Where ce1.id.idToPoliza = :idToPoliza " +
			"	And ce1.id.numeroInciso = :numeroInciso " +
			" )";
			
			Query query = entityManager.createQuery(queryString.toString(), EndosoDTO.class);						
			query.setParameter("idToPoliza", idToPoliza);
			query.setParameter("numeroInciso", inciso.getNumero());
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			endoso = (EndosoDTO) query.getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}		
		
		return endoso;
	}
	
	@EJB
	private EntidadContinuityDao entidadContinuityDao;
	
	@EJB
	private EntidadBitemporalService entidadBitemporalService;
	
	@EJB
	private CalculoService calculoService;
	
	@EJB
	private IncisoSoporteService incisoSoporteService;
	
	@EJB
	private EntidadService entidadService;
	
	@PersistenceContext 
	private EntityManager entityManager;	
	
}
