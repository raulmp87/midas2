package mx.com.afirme.midas2.service.impl.sapamis.accionesalertas;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAccionesRelacion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.accionesalertas.SapAmisAccionesRelacionService;

@Stateless
public class SapAmisAccionesRelacionServiceImpl implements SapAmisAccionesRelacionService{
	private EntidadService entidadService;

	@Override
	public List<SapAmisAccionesRelacion> findAll() {
		List<SapAmisAccionesRelacion> retorno = entidadService.findAll(SapAmisAccionesRelacion.class);
		return retorno;
	}

	@Override
	public SapAmisAccionesRelacion findById(long id) {
		SapAmisAccionesRelacion retorno = new SapAmisAccionesRelacion();
		if(id >= 0){
			retorno = entidadService.findById(SapAmisAccionesRelacion.class, id);
		}
		return retorno;
	}

	@Override
	public List<SapAmisAccionesRelacion> findByProperty(String propertyName, Object property) {
		List<SapAmisAccionesRelacion> emailNegocioAlertasList = new ArrayList<SapAmisAccionesRelacion>();
		if(propertyName != null && !propertyName.equals("") && property != null){
			emailNegocioAlertasList = entidadService.findByProperty(SapAmisAccionesRelacion.class, propertyName, property);
		}
		return emailNegocioAlertasList;
	}

	@Override
	public List<SapAmisAccionesRelacion> findByStatus(boolean status) {
		List<SapAmisAccionesRelacion> emailNegocioAlertas = entidadService.findByProperty(SapAmisAccionesRelacion.class, "estatus", status?0:1);
		return emailNegocioAlertas;
	}

	@Override
	public SapAmisAccionesRelacion saveObject(SapAmisAccionesRelacion sapAmisAccionesRelacion) {
		if(sapAmisAccionesRelacion != null && sapAmisAccionesRelacion.getIdSapAmisAccionesRelacion() >= 0){
			sapAmisAccionesRelacion.setIdSapAmisAccionesRelacion((Long)entidadService.saveAndGetId(sapAmisAccionesRelacion));
		}
		return sapAmisAccionesRelacion;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}