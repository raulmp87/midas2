package mx.com.afirme.midas2.service.impl.negocio.cliente;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas2.dao.negocio.cliente.ClienteAsociadoJPADao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.cliente.ClienteAsociadoJPA;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.cliente.ClienteAsociadoJPAService;
import mx.com.afirme.midas2.service.negocio.cliente.NegocioClienteService;

@Stateless
public class ClienteAsociadoJPAServiceImpl implements ClienteAsociadoJPAService {

	@EJB
	private ClienteAsociadoJPADao clienteAsociadoJPADao;
	@EJB
	private EntidadService entidadService;
	@EJB
	private NegocioClienteService negocioClienteService;
	
	@Override
	public List<NegocioCliente> listarClientesAsociados(Long idToNegocio) {
		Negocio negocio = entidadService.findById(Negocio.class, idToNegocio);
		List<NegocioCliente> resultAsociadosList = new ArrayList<NegocioCliente>();
		List<ClienteAsociadoJPA> asociados = clienteAsociadoJPADao
				.findByProperty("idToNegocio", idToNegocio);
		if (asociados != null && !asociados.isEmpty()) {
			for (ClienteAsociadoJPA item : asociados) {
				NegocioCliente negocioCliente = new NegocioCliente();
				negocioCliente.setId(idToNegocio);
				negocioCliente.setIdCliente(BigDecimal.valueOf(item
						.getIdCliente()));
				negocioCliente.setNegocio(negocio);
				ClienteDTO clienteDTO = new ClienteDTO();
				clienteDTO.setNombre(item.getNombre());
				clienteDTO.setCodigoRFC(item.getCodigoRFC());
				clienteDTO
						.setIdCliente(BigDecimal.valueOf(item.getIdCliente()));
				negocioCliente.setClienteDTO(clienteDTO);
				resultAsociadosList.add(negocioCliente);
			}
		}else{
			resultAsociadosList = negocioClienteService.listarPorNegocio(idToNegocio);
		}
		return resultAsociadosList;
	}

}
