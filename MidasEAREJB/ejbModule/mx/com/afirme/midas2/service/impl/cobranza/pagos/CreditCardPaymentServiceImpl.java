package mx.com.afirme.midas2.service.impl.cobranza.pagos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboFacade;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioFacadeRemote;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.ws.cliente.PrintReportClient;
import mx.com.afirme.midas2.domain.cobranza.pagos.Attempt;
import mx.com.afirme.midas2.domain.cobranza.pagos.PROSAException;
import mx.com.afirme.midas2.domain.cobranza.pagos.PROSATransaction;
import mx.com.afirme.midas2.domain.cobranza.pagos.PROSATransactionResponse;
import mx.com.afirme.midas2.domain.cobranza.pagos.RecibosFoleados;
import mx.com.afirme.midas2.domain.cobranza.pagos.RespuestaPagoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.cobranza.pagos.CreditCardPaymentService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;
import mx.com.afirme.midas2.util.MailService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

@Stateless
public class CreditCardPaymentServiceImpl implements CreditCardPaymentService{
	
	private static final Logger LOG = Logger.getLogger(CreditCardPaymentServiceImpl.class);
	public static final String DATASOURCE = "jdbc/MidasDataSource";
	private static final int monedaDls = 840;
	private static final int ciento = 100;
	
	private ListadoService listadoService;

	@EJB
	private MailService mailService;
	@EJB
	private GenerarPlantillaReporte templateGenerator;
	@EJB
	private PrintReportClient printReport;
	@EJB
	private ImpresionRecibosService impresionRecibosService;
	
	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	private UsuarioService usuarioService;	
	
	private TipoCambioFacadeRemote tipoCambioService;
	
	@EJB(beanName = "UsuarioServiceDelegate") 
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@EJB
	public void setTipoCambioService(TipoCambioFacadeRemote tipoCambioService) {
		this.tipoCambioService = tipoCambioService;
	}

	@Override
	public List<ReciboDTO> findReceipts(String insurancePolicy, String subsection, String siglasRFC, String fechaRFC, String homoClaveRFC) {
		ReciboFacade reciboFacade = new ReciboFacade();
		List<ReciboDTO> receiptsDTO = new ArrayList<ReciboDTO>();
		try {
			receiptsDTO = reciboFacade.findByPolicyAndRFC(insurancePolicy, subsection, siglasRFC, fechaRFC, homoClaveRFC);
		} catch(Exception ex) {
			LOG.info("Ocurrio un error al consultar la Póliza. Revise que los datos introducidos sean correctos.");
			LOG.error("Ocurrio un error al consultar la Póliza. Revise que los datos introducidos sean correctos. ",ex);
		}
		return receiptsDTO;
	}

	@Override
	public PolizaDTO findPolicy(String insurancePolicy) throws PROSAException {
		StoredProcedureHelper spPoliza = null;
		PolizaDTO policyDTO = null;
		try {
			spPoliza = new StoredProcedureHelper("SEYCOS.PKG_CC_PAYMENT.POLICY_DATA");

			String[] parts = insurancePolicy.split("-");
			spPoliza.estableceMapeoResultados(
					PolizaDTO.class.getCanonicalName(), "nombreAsegurado,"
							+ "nombreProducto",
					"solicitante," + "producto");
			spPoliza.estableceParametro("idCentroEmis", parts[0]);
			spPoliza.estableceParametro("numPoliza", parts[1]);
			spPoliza.estableceParametro("numRenovPol", parts[2]);

			policyDTO = (PolizaDTO) spPoliza.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new PROSAException(PROSAException.RECEIPTS_NOT_FOUND);
		}
		return policyDTO;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void notificaIBS (ReciboDTO receipt) throws Exception {
		try {
			StoredProcedureHelper spNotificaIBS = new StoredProcedureHelper("SEYCOS.PKG_CC_PAYMENT.NOTIFY_IBS", DATASOURCE);
			spNotificaIBS.estableceParametro("pIdCotizacion", receipt.getIdCotizacion());
			LOG.info("id_Cotizacion "+receipt.getIdCotizacion());
			spNotificaIBS.estableceParametro("pLinea", receipt.getIdLinea());
			LOG.info("id_Linea "+receipt.getIdLinea());
			spNotificaIBS.estableceParametro("pInciso", receipt.getIdInciso());
			LOG.info("id_Inciso "+receipt.getIdInciso());
			spNotificaIBS.estableceParametro("pIdRecibo", receipt.getIdRecibo());
			LOG.info("id_Recibo "+receipt.getIdRecibo());
			spNotificaIBS.ejecutaActualizar();			
		} catch(Exception e) {					
			LOG.error("No se pudo notificar a IBS del pago del recibo " + receipt.getIdRecibo() + ": " ,e);
		}		
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void generarRemesa(PROSATransactionResponse transactionResponse, String [] receipts) throws Exception {
		try {
			StoredProcedureHelper spGeneraRemesa = new StoredProcedureHelper("SEYCOS.PKG_CC_PAYMENT.GENERA_REMESA", DATASOURCE);
			spGeneraRemesa.estableceParametro("pRecibos", StringUtils.join(receipts,","));
			spGeneraRemesa.estableceParametro("pREFERENCIA", transactionResponse.getRefNum());
			spGeneraRemesa.estableceParametro("pIMPORTE", transactionResponse.getTotal());
			spGeneraRemesa.estableceParametro("pAFILIACION", transactionResponse.getMerchant());
			spGeneraRemesa.estableceParametro("pIDCODIGO", transactionResponse.getAuth());
			spGeneraRemesa.estableceParametro("pDESCCODIGO", transactionResponse.getResponse());
			spGeneraRemesa.ejecutaActualizar();		
		} catch(Exception ex) {
			LOG.error("Error al ejecutar el proceso SEYCOS.PKG_CC_PAYMENT.GENERA_REMESA. ",ex);
		}
	}
	
	@Override	
	public RespuestaPagoDTO payReceipts(PROSATransactionResponse transactionResponse, String[] receipts) throws PROSAException, Exception {
		RespuestaPagoDTO respuestaPagoDTO = new RespuestaPagoDTO();
		PolizaDTO poliza = null;
		poliza = findPolicy(transactionResponse.getPolicy());
		ReciboFacade reciboFacade = new ReciboFacade();
		List<ReciboDTO> receiptsDTO = reciboFacade.findById(transactionResponse.getPolicy(), transactionResponse.getSubsection(), receipts);		
		List<String> addresses = new ArrayList<String>(1);
		addresses.add(usuarioService.getUsuarioActual().getEmail());
		respuestaPagoDTO.setCorreos(addresses);		
		boolean paid = false;
		transactionResponse.setIdCotiza(receiptsDTO.get(0).getIdCotizacion().toString());		
		LOG.info("Prepara para guardar en bitacoraprosa....");
		transactionResponse.store();
		LOG.info("Guardo en bitacoraprosa....");		
		if(receiptsDTO.size() == receipts.length) {
			if (transactionResponse.isApproved()) {				
				if (transactionResponse.isValidDigest()) {					
					LOG.info("Ejecuta el Procedure (Genera_Remesa)");
					generarRemesa(transactionResponse, receipts);	
					LOG.info("Termina de ejecutar el Procedure (Genera_Remesa)");					
					RecibosFoleados reciboFoleado = new RecibosFoleados();
					for (ReciboDTO receipt : receiptsDTO) {					
						LOG.info("Inicia Proceso para Folear Recibos....");
						reciboFoleado = folearRecibos(receipt);
						receipt.setLlaveFiscal(reciboFoleado.getLlaveFiscal());
						respuestaPagoDTO.setReciboLlaveFiscal(reciboFoleado.getLlaveFiscal());
						if (reciboFoleado.getErrorCode() != null) {
							if(reciboFoleado.getErrorCode() != 0)
							LOG.info("Ocurrio un error al generar la Llave Fiscal. "+ reciboFoleado.getErrorCode() + " . "+reciboFoleado.getErrorDesc());
						}						
						LOG.info("Termina Proceso de Folear Recibo:= "+receipt.getIdRecibo()+ "  LlaveFiscal(ReciboDTO):= " +receipt.getLlaveFiscal());
						
						LOG.info("Inicia Proceso para Notificar a IBS....");
						notificaIBS(receipt);
						LOG.info("Termina Proceso de Notificar a IBS....");				
					}
					paid = true;
					try {
						LOG.info("Ejecuta metodo que genera plantilla de correo para envio de Comprobante de Pago");
						String mensaje = mailService.generarPlantillaPortal(receiptsDTO, transactionResponse, poliza);					
						LOG.info("Intenta enviar el correo con los siguientes parametros: (destinatario): "
								+addresses.get(0)+" - (Asunto): Notificacion de Pago - (cuerpoCorreo): "
								+mensaje+"  No lleva Adjuntos.");
						mailService.sendMail(addresses, "Notificación de Pago", mensaje);												
					} catch(Exception e) {
						LOG.error("No se pudo enviar comprobante de pago por email, transaccion " + transactionResponse.getRefNum() + ": " , e);
						throw new PROSAException(PROSAException.ENVIO_COMPROBANTE_ERROR  + transactionResponse.getRefNum());
					 }
				} else {
					LOG.info("Digest invalido");
					throw new PROSAException(PROSAException.DIGEST_ERROR);
				 }
			} else {
				LOG.info("Transaccion denegada");
				throw new PROSAException(PROSAException.TRANSACTION_DENIED);
			 }
		}		
		respuestaPagoDTO.setPoliza(transactionResponse.getPolicy());
		respuestaPagoDTO.setPagado(paid);
		return respuestaPagoDTO;
	}
	
	@Override
	public RecibosFoleados folearRecibos (ReciboDTO recibo) {
		RecibosFoleados reciboResult = null;
		try {
			StoredProcedureHelper foleaReciboSP = new StoredProcedureHelper("SEYCOS.PKG_CC_PAYMENT.STP_GET_RECEIPT_KEY");
			StringBuilder parametros = new StringBuilder();
			parametros.append("llaveFiscal,");
			parametros.append("errorCode,");
			parametros.append("errorDesc,");
			parametros.append("fechaAsignacion");
			StringBuilder columnasBD = new StringBuilder();
			columnasBD.append("KEY,");
			columnasBD.append("ERROR_CODE,");
			columnasBD.append("ERROR_DESC,");
			columnasBD.append("ASIGNATION_DATE");
			String userName = "INTMIDAS";
			foleaReciboSP.estableceMapeoResultados(RecibosFoleados.class.getCanonicalName(), parametros.toString(), columnasBD.toString());
			foleaReciboSP.estableceParametro("P_QUOTATION_ID", recibo.getIdCotizacion());
			foleaReciboSP.estableceParametro("P_CONSECUTIVE_NUMBER", recibo.getNumeroExhibicion());
			foleaReciboSP.estableceParametro("P_RECEIPT_ID", recibo.getIdRecibo());
			foleaReciboSP.estableceParametro("P_USERNAME", userName);
			reciboResult = (RecibosFoleados) foleaReciboSP.obtieneResultadoSencillo();
		} catch (Exception ex) {
			LOG.error("Ocurrio un error al folear el recibo:  "+recibo.getIdRecibo(), ex);
		}
		return reciboResult;
	}
	
	@Override
	public List<ReciboDTO> buscaRecibosById(String poliza, String inciso, String [] recibos) {
		ReciboFacade reciboFacade = new ReciboFacade();
		return reciboFacade.findById(poliza, null, recibos);		 
	}
	
	@Override
	public void envioPDFyXML(String llaveFiscal, List<String> addresses, String poliza) {
		LOG.info("Llave Fiscal::  "+llaveFiscal);
		StringBuilder mensajeMail = new StringBuilder();
		mensajeMail.append("Se ha enviado el recibo de la poliza: " +poliza + "\r\n");

		StringBuilder title = new StringBuilder();
		title.append("Recibo: " + llaveFiscal);

		StringBuilder subject = new StringBuilder();
		subject.append("Comprobante Fiscal.");

		StringBuilder greeting = new StringBuilder();
		greeting.append("Estimado(a): "
				+ usuarioService.getUsuarioActual().getNombreCompleto().trim());
		try {
			List<ByteArrayAttachment> attachment = new ArrayList<ByteArrayAttachment>();
			ByteArrayAttachment pdf = new ByteArrayAttachment();
			ByteArrayAttachment xml = new ByteArrayAttachment();
			byte[] reciboFiscalPDF   = null;
			byte[] reciboFiscalXML   = null;
			
			reciboFiscalPDF = impresionRecibosService.getReciboFiscal(llaveFiscal);
			if (reciboFiscalPDF != null && reciboFiscalPDF.length>0) {
				pdf.setTipoArchivo(ByteArrayAttachment.TipoArchivo.PDF);
				pdf.setNombreArchivo(llaveFiscal + ".pdf");
				LOG.info(" Preparando el archivo PDF");							
				pdf.setContenidoArchivo(reciboFiscalPDF);								
				attachment.add(pdf);
			} else {
				LOG.info(" No se Extrajo el archivo PDF....");		
			}
			
			reciboFiscalXML = printReport.getDigitalBill(llaveFiscal, "xml");
			if (reciboFiscalXML!= null && reciboFiscalXML.length>0) {
				xml.setTipoArchivo(ByteArrayAttachment.TipoArchivo.DESCONOCIDO );
			  	xml.setNombreArchivo("Factura.xml");
			  	LOG.info("Preparando el archivo XML....");
				xml.setContenidoArchivo( reciboFiscalXML);
				attachment.add( xml); 
			} else {
				LOG.info("No se Extrajo el archivo XML");		
			}			
			if (attachment != null && attachment.size() >0) {
				LOG.info("--> attachment.size:  " + attachment.size());
				mailService.sendMail(addresses, title.toString() , mensajeMail.toString(), attachment, subject.toString(), greeting.toString());
			}
		} catch (Exception e) {
			LOG.error("No se pudo enviar el Recibo..", e);
		}
	}

	private int getLockoutTime(String policy) {
		Attempt attempt = new Attempt();
		int timeRemaining = 0;
		StoredProcedureHelper spLockoutTime = null; 
		try {
			spLockoutTime = new StoredProcedureHelper("SEYCOS.PKG_CC_PAYMENT.GET_LOCKOUT_TIME");
			spLockoutTime.estableceMapeoResultados(
					Attempt.class.getCanonicalName(),
					"lastAttempt," + "timeElapsed",
					"lastAttempt," + "timeElapsed");
			spLockoutTime.estableceParametro("pPoliza", policy);
			attempt = (Attempt) spLockoutTime.obtieneResultadoSencillo();
			if (attempt == null) {
				attempt = new Attempt();
				attempt.setLastAttempt(BigDecimal.ZERO);
			}
		} catch(Exception e) {
			e.printStackTrace();
			LOG.error("No se ha podido obtener el tiempo de bloqueo de la póliza " + policy);
		}
		
		switch (attempt.getLastAttempt().intValue()) {
			case 1:
				timeRemaining = PROSATransactionResponse.FIRST_ATTEMPT_LOCKOUT_TIME - attempt.getTimeElapsed().intValue();
				break;
			case 2:
				timeRemaining = PROSATransactionResponse.SECOND_ATTEMPT_LOCKOUT_TIME - attempt.getTimeElapsed().intValue();
				break;
			case 3:
				timeRemaining = PROSATransactionResponse.THIRD_ATTEMPT_LOCKOUT_TIME - attempt.getTimeElapsed().intValue();
				break;
			default:
				timeRemaining = 0;
				break;
		}
		return timeRemaining;
	}

	private int getTransactionCurrency(List<ReciboDTO> receipts) throws PROSAException, Exception {
		BigDecimal currency = receipts.get(0).getIdMoneda();
		for (ReciboDTO receipt: receipts) {
			if (currency.compareTo(receipt.getIdMoneda()) != 0) {
				throw new PROSAException(PROSAException.INVALID_CURRENCY);
			}
		}
		return currency.intValue(); 
	}
	
	private double getTransactionTotal(List<ReciboDTO> receipts)throws PROSAException {
		double total = 0.0;
		double tipoCambio = 1.0d;
		for (ReciboDTO receipt: receipts) {			
			total = total + receipt.getImporte();			
		}
		
		if (total <= 0) {
			throw new PROSAException(PROSAException.INVALID_AMMOUNT);
		}
		
		if (receipts.get(0).getIdMoneda().intValue() == monedaDls) {
			try {
				tipoCambio = tipoCambioService.obtieneTipoCambioPorDia(new Date (), "SEGASIS");
			} catch (Exception ex) {
				LOG.error("No se pudo consultar el tipo de cambio al día. "+ new Date().getTime(), ex);
			}
			return total * tipoCambio;			
		}		
		return total;
	}
	
	public String validReceipts(String receipts) {
		StoredProcedureHelper storedHelper = null;
		String descRespuesta = "";
		try {
			storedHelper = new StoredProcedureHelper("SEYCOS.PKG_CC_PAYMENT.VALID_RECEIPTS");
			storedHelper.estableceParametro("pRecibos", receipts);
			storedHelper.ejecutaActualizar();
			descRespuesta = storedHelper.getDescripcionRespuesta();
		} catch (Exception e) {
			LOG.error("No se pudo validar los recibos " + receipts);
		}
		return descRespuesta;
	}
	
	public String validPoliza(String insurancePolicy, String subsection, String siglasRFC, String fechaRFC) {
		StoredProcedureHelper spValidPoliza = null;
		String descRespuesta = "";
		try {
			String[] parts = insurancePolicy.split("-");
			spValidPoliza = new StoredProcedureHelper("SEYCOS.PKG_CC_PAYMENT.VALID_POLIZA");
			spValidPoliza.estableceParametro("idCentroEmis", parts[0]);
			spValidPoliza.estableceParametro("numPoliza", parts[1]);
			spValidPoliza.estableceParametro("numRenovPol", parts[2]);
			spValidPoliza.estableceParametro("siglasRFC", siglasRFC);
			spValidPoliza.estableceParametro("fechaRFC", fechaRFC);
			spValidPoliza.ejecutaActualizar();
			descRespuesta = spValidPoliza.getDescripcionRespuesta();
		} catch (Exception ex) {
			LOG.error("No se pudo consultar informacion de la poliza: " +insurancePolicy+" -> ",ex);
		}
		return descRespuesta;
	}

	@Override
	public PROSATransaction requestTransaction(String insurancePolicy, String subsection, String[] receipts) throws PROSAException, Exception {
		ReciboFacade reciboFacade = new ReciboFacade();
		if (receipts == null) {
			throw new PROSAException(PROSAException.RECEIPTS_NOT_SELECTED);
		}
		List<ReciboDTO> receiptsDTO = reciboFacade.findById(insurancePolicy, subsection, receipts);
		
		PROSATransaction transaction = new PROSATransaction();
		if (receiptsDTO.size() > 0) {
			int timeLeft = getLockoutTime(insurancePolicy);
			LOG.info("timeLeft := "+timeLeft);
			if (timeLeft <= 0) {
				String valid = validReceipts(StringUtils.join(receipts, ","));
				if(StringUtils.isBlank(valid)){
					transaction.setMerchant(listadoService.ObtieneMerchantPortalPagosWeb());
					transaction.setUrl_back(listadoService.ObtieneUrlBackPortalPagosWeb());
					transaction.setCurrency(getTransactionCurrency(receiptsDTO));
					double total = getTransactionTotal(receiptsDTO) * ciento;
					transaction.setTotal((int) Math.round(total));
					transaction.setPolicy(insurancePolicy);
					if (subsection != null) {
						transaction.setSubsection(subsection);
					}
					transaction.setOrderId(String.valueOf(generateOrderId()));
				} else {
					throw new PROSAException(valid);
				}
			} else {
				LOG.info("timeLeft := "+timeLeft);
				StringBuilder sbError = new StringBuilder();
				sbError.append(PROSAException.LOCKED_OUT);
				sbError.append(" Intente de nuevo dentro de ");
				sbError.append(timeLeft);
				sbError.append(" minutos.");
				LOG.info("sbError:  " + sbError.toString());
				throw new PROSAException(PROSAException.LOCKED_OUT +" "+ sbError.toString());
			}
		} else {
			throw new PROSAException(PROSAException.RECEIPTS_NOT_FOUND);
		}
		return transaction;
	}

	private int generateOrderId() throws Exception {
		StoredProcedureHelper spNextOrder = new StoredProcedureHelper("SEYCOS.PKG_CC_PAYMENT.NEXTORDER", DATASOURCE);
		return spNextOrder.ejecutaActualizar();
	}

	@Override
	public Double obtieneTipoCambioAlDia(Date fechaHoy, String usuario) {
		double tipoCambio = 1.0d;
		try {
			tipoCambio = tipoCambioService.obtieneTipoCambioPorDia(new Date (), usuario);
		} catch (Exception ex) {
			LOG.error("No se pudo consultar el tipo de cambio al día, "+ new Date().getTime(), ex);
		}
		return tipoCambio;
	}
}