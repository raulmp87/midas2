package mx.com.afirme.midas2.service.impl.siniestros.pagos;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas2.dao.siniestros.pagos.PagosSiniestroDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.FacturaLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.DesglosePagoConceptoDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.OrdenPagosDTO;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.liquidacion.LiquidacionSiniestroService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.pagos.PagosSiniestroService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.FacturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSalvamentoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;

@Stateless
public class PagosSiniestroServiceImpl implements PagosSiniestroService{
	
	private EstadoFacadeRemote estadoFacadeRemote;
	
	@EJB
	private PagosSiniestroDao pagosSiniestroDao;	
	
	@EJB
	private OrdenCompraService ordenCompraService;

	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private CatalogoGrupoValorService  catalogoGrupoValorService;
	
	@EJB
	private EnvioNotificacionesService envioNotificacionesService;
	
	@EJB 
	EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@EJB
	RecepcionFacturaService recepcionFacturaService;
	
	@EJB
	BitacoraService bitacoraService;
	
	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;
	
	@EJB
	private RecuperacionSalvamentoService recuperacionSalvamentoService;
	
	@EJB
	private LiquidacionSiniestroService liquacionSiniestroService;	
	
	@EJB
	private ParametroGlobalService parametroGlobalService;
	
	public static final String CODIGOCORREO = "CANCELACION_ORDENPAGO";
	private static final BigDecimal zero =BigDecimal.ZERO;	
	
	/**
	 * Guarda el detalle de una orden de pago 
	 * @param DetalleOrdenPago detalle
	 */
	@Override
	public void guardarDetalleOrdenPago(DetalleOrdenCompra detalle) {
		detalle.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
		detalle.setFechaModificacion(new Date());
		if(null==detalle.getId()){
			detalle.setFechaCreacion(new Date());
			detalle.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
		}
		 this.entidadService.save(detalle);
	}

	/**
	 * Guarda una orden de pago
	 * @param OrdenPagoSiniestro ordenPago
	 */

	@Override
	public OrdenPagoSiniestro guardarOrdenPago(OrdenPagoSiniestro ordenPago) {
		boolean nuevo =false;
		
		if(null==ordenPago.getId()){
			ordenPago.setFechaCreacion(new Date());
			ordenPago.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			nuevo=true;
		}else{
			ordenPago.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			ordenPago.setFechaModificacion(new Date());
			
		}
		 this.entidadService.save(ordenPago);
		 if(nuevo){
				this.bitacoraService.registrar(TIPO_BITACORA.ORDEN_PAGO, EVENTO.CREACION_ORDENPAGO, ordenPago.getId().toString(), "CREACION ORDEN PAGO", ordenPago, usuarioService.getUsuarioActual().getNombreUsuario() );
		 }else{
				this.bitacoraService.registrar(TIPO_BITACORA.ORDEN_PAGO, EVENTO.MODIFICACION_ORDENPAGO, ordenPago.getId().toString(), "MODIFICACION ORDEN PAGO ["+ordenPago.getEstatus()+"] ", ordenPago, usuarioService.getUsuarioActual().getNombreUsuario() );
		 }
		 return ordenPago;
	}	
	/**
	 * Obtiene el detalle de una orden de pago 
	 * @param Long idOrdenPago
	 */
	@Override
	public List<DesglosePagoConceptoDTO> getDetalleOrdenPago(Long idOrdenPago, Boolean calcularReserva) {
		
			BigDecimal reserva=zero;
			String 		tipoPago="";
			BigDecimal reservaDisponible=zero;			
			//CALCULAR TOTALES 
			BigDecimal totalPagar=zero;
			BigDecimal totalPagado=zero;		
			BigDecimal  ivaTotalPorPagar=zero;
			BigDecimal  isrTotalPorPagar=zero;
			BigDecimal  ivaRetenidoTotalPorPagar=zero;
			BigDecimal  salvamentoTotalPorPagar=zero;
			BigDecimal  salvamentoSubtotalPorPagar=zero;
			BigDecimal  totalSinDescuentosPorPagar=zero;
			BigDecimal  subTotalPorPagar=zero;				
			BigDecimal  deducibleTotal=zero;
			BigDecimal  descuentoTotal=zero;			
			OrdenPagoSiniestro ordenPago=this.entidadService.findById(OrdenPagoSiniestro.class, idOrdenPago);
			List<DesglosePagoConceptoDTO> desglosePagoConceptoDTO= new ArrayList<DesglosePagoConceptoDTO>();
			List<DesglosePagoConceptoDTO> listaDegloseOrdenesPago= new ArrayList<DesglosePagoConceptoDTO>();
			OrdenCompra ordenCompra=this.ordenCompraService.obtenerOrdenCompra( ordenPago.getOrdenCompra().getId());
				Map<String, Object> param = new HashMap<String, Object>();
				param.put("ordenCompra.ordenPago.id", idOrdenPago);
				if(null!= ordenPago   ){	
					if ( !StringUtil.isEmpty(ordenPago.getTipoPago() )  ){
							tipoPago=(this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.TIPO_ORDEN_PAGO, ordenPago.getTipoPago()));
					}										
					if(calcularReserva != null && calcularReserva && null!= ordenPago.getOrdenCompra()){
						
						if( null!=ordenCompra.getCoberturaReporteCabina()  && null!=ordenCompra.getIdTercero()){					
							reserva = movimientoSiniestroService.obtenerReservaAfectacion(ordenCompra.getIdTercero(), Boolean.FALSE);					
							reservaDisponible = movimientoSiniestroService.obtenerReservaAfectacion(ordenCompra.getIdTercero(), Boolean.TRUE);
						}						
					}	
					if(null!=ordenCompra.getDeducible()){
						deducibleTotal=ordenCompra.getDeducible();
					}
					if(null!=ordenCompra.getDescuento()){
						descuentoTotal=ordenCompra.getDescuento();
					}
				}
				List<DetalleOrdenCompra> listDetalle= this.entidadService.findByProperties(DetalleOrdenCompra.class, param);
						for (DetalleOrdenCompra detalleOP: listDetalle){							
							DesglosePagoConceptoDTO desglose = new DesglosePagoConceptoDTO();
							desglose.setReserva(reserva);
							desglose.setReservaDisponible(reservaDisponible);	
							desglose.setPago(tipoPago);							
							if (!StringUtil.isEmpty(ordenCompra.getTipo())){
								CatValorFijo valor=catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA, ordenCompra.getTipo());
								if(null!=valor){
									desglose.setTipoOrden(valor.getDescripcion());
								}
							}
							if (null!=detalleOP.getConceptoAjuste()&& null!=detalleOP.getConceptoAjuste().getNombre()){
								desglose.setConceptoPago(detalleOP.getConceptoAjuste().getNombre());
							}
							desglose.setIdDetalleOrdenCompra(detalleOP.getId());
							desglose.setOrdenCompra(detalleOP.getOrdenCompra());
							desglose.setIdDetalleOrdenPago(detalleOP.getId());
							if(null!=detalleOP.getCostoUnitario())
								desglose.setSubtotal(detalleOP.getCostoUnitario());
							else
								desglose.setSubtotal(zero);
							if(null!=detalleOP.getCostoUnitario())
								desglose.setCostoUnitario(detalleOP.getCostoUnitario());
							else
								desglose.setCostoUnitario(zero);
							
							if(null!=detalleOP.getPorcIva())
								desglose.setPorIva(detalleOP.getPorcIva());				
							else
								desglose.setPorIva(zero);
							
							if(null!=detalleOP.getIva())
								desglose.setIva(detalleOP.getIva());				
							else
								desglose.setIva(zero);

							if(null!=detalleOP.getImporte())
								desglose.setImporte(detalleOP.getImporte());				
							else
								desglose.setImporte(zero);
							
							if(null!=detalleOP.getImporte())
								desglose.setTotal(detalleOP.getImporte());				
							else
								desglose.setTotal(zero);
							if(null!=detalleOP.getPorcIvaRetenido())
								desglose.setPorIvaRetenido(detalleOP.getPorcIvaRetenido());				
							else
								desglose.setPorIvaRetenido(zero);
							if(null!=detalleOP.getIvaRetenido())
								desglose.setIvaRetenido(detalleOP.getIvaRetenido());				
							else
								desglose.setIvaRetenido(zero);
							
							if(null!=detalleOP.getPorcIsr())
								desglose.setPorIsr(detalleOP.getPorcIsr());				
							else
								desglose.setPorIsr(zero);
							
							if(null!=detalleOP.getIsr())
								desglose.setIsr(detalleOP.getIsr());				
							else
								desglose.setIsr(zero);
							
							if(null!=detalleOP.getImportePagado()){
								desglose.setImportePagado(detalleOP.getImportePagado());
							}else{
								desglose.setImportePagado(zero);
							}
							BigDecimal totalAPagar=desglose.getTotal();//.subtract(desglose.getImportePagado());
							desglose.setTotalPagar(totalAPagar);
							//OBTIENE DE LA ORDEN DE PAGO 
							desglose.setIdDetalleOrdenPago(detalleOP.getId());							
							//salvamento
							desglose.setTotalSalvamento(zero);
							RecuperacionSalvamento salvamento = recuperacionSalvamentoService.obtenerRecuperacionSalvamento(
									detalleOP.getOrdenCompra().getIdTercero());
							if(salvamento != null && 
									!salvamento.getEstatus().equals(Recuperacion.EstatusRecuperacion.CANCELADO)){
								desglose.setTotalSalvamento(salvamento.getValorEstimado());								
							}							
							//totales de  descuento y deducible de orden de pago
							desglose.setDescuentosTotal(descuentoTotal);
							desglose.setDeduciblesTotal(deducibleTotal);				
							desglosePagoConceptoDTO.add(desglose);							
							//----------CALCULA IMPORTES 
							if(null!=desglose.getTotalPagar()){
								totalSinDescuentosPorPagar=	totalSinDescuentosPorPagar.add(desglose.getTotalPagar());
							}
							if(null!=desglose.getImportePagado()){
								totalPagado=totalPagado.add(desglose.getImportePagado());
							}
							if(null!=desglose.getIva()){
								ivaTotalPorPagar=	ivaTotalPorPagar.add(desglose.getIva());
							}
							if(null!=desglose.getIsr()){
								isrTotalPorPagar=	isrTotalPorPagar.add(desglose.getIsr());
							}
							if(null!=desglose.getIvaRetenido()){
								ivaRetenidoTotalPorPagar=	ivaRetenidoTotalPorPagar.add(desglose.getIvaRetenido());
							}
							if(null!=desglose.getSalvamento()){
								salvamentoSubtotalPorPagar=	salvamentoSubtotalPorPagar.add(desglose.getSalvamento());
							}
							if(null!=desglose.getTotalSalvamento()){
								salvamentoTotalPorPagar=	salvamentoTotalPorPagar.add(desglose.getTotalSalvamento());
							}		
							if(null!=desglose.getSubtotal()){
								subTotalPorPagar=	subTotalPorPagar.add(desglose.getSubtotal());
							}
						}
						totalPagar= totalSinDescuentosPorPagar.subtract(deducibleTotal).subtract(descuentoTotal);
						//iterar nuevamente detalle para Setear Importes de TOTALES 
						for (DesglosePagoConceptoDTO desglose : desglosePagoConceptoDTO){
							desglose.setTotalesPorPagar(totalPagar);
							desglose.setTotalSinDescuentosPorPagar(totalSinDescuentosPorPagar);
							desglose.setTotalesPagados(totalPagado);							
							desglose.setIvaTotalPorPagar(ivaTotalPorPagar);
							desglose.setIvaRetenidoTotalPorPagar(ivaRetenidoTotalPorPagar);
							desglose.setIsrTotalPorPagar(isrTotalPorPagar);
							desglose.setSubTotalPorPagar(subTotalPorPagar);
							desglose.setSalvamentoTotalPorPagar(salvamentoTotalPorPagar);
							desglose.setSalvamentoSubtotalPorPagar(salvamentoSubtotalPorPagar);	
							listaDegloseOrdenesPago.add(desglose);
						}
						
			return listaDegloseOrdenesPago;
		}
	/**
	 * busca una orden de pago ya sea por id de orden de pago o por el id de orden de
	 * compra y/o orden pago.
	 * 
	 * @param irOrdenCompra
	 * @param idOrdenPago
	 */
	@Override
	public OrdenPagoSiniestro obtenerOrdenPago(Long idOrdenCompra, Long idOrdenPago) {
		Map<String, Object> parameters = new HashMap<String, Object>();	
		Long longZero= new Long(0);
		if(null!= idOrdenCompra &&idOrdenCompra!=longZero ){
			parameters.put("ordenCompra.id", idOrdenCompra);
		}
		if(null!= idOrdenPago &&idOrdenPago!=longZero ){
			parameters.put("id", idOrdenPago);
		}
		List<OrdenPagoSiniestro> ordenPago= new ArrayList<OrdenPagoSiniestro>();
		ordenPago=	entidadService.findByProperties(OrdenPagoSiniestro.class, parameters);
		if (!ordenPago.isEmpty())
			return ordenPago.get(0);
		else
			return null;
	}
	/**
	 * Obtiene los totales de una orden de pago especifica.
	 * @param OrdenPagoSiniestro ordenPago
	 */
	@Override
	public DesglosePagoConceptoDTO obtenerTotales(Long ordenPagoId, Boolean calcularReserva) {
		List<DesglosePagoConceptoDTO> lst=this.getDetalleOrdenPago(ordenPagoId, calcularReserva);
		
		if(!lst.isEmpty()){
			return lst.get(0);
		}
		return null;		
	}

	/**
	 * Get total details for pay order through stored procedure from PKGSIN_ORDENCOMPRA. joksrc
	 * 
	 * @param Long ordenPagoId 
	 * @param Boolean calcularReserva
	 */
	@Override
	public DesglosePagoConceptoDTO obtenerTotalesSP(Long ordenPagoId, Boolean calcularReserva){
		List<DesglosePagoConceptoDTO> lst = this.pagosSiniestroDao.obtenerDetalleOrdenPago(ordenPagoId, calcularReserva);
		
		if(!lst.isEmpty()){
			return lst.get(0);
		}
		return null;		
	}
	
	@Override
	public  DesglosePagoConceptoDTO obtenerTotalesPorDesglose(List<DesglosePagoConceptoDTO> lstdetalleDeglose) {
		DesglosePagoConceptoDTO degloseTotales= new DesglosePagoConceptoDTO();
		BigDecimal subtotal=zero;//subtotal
		BigDecimal iva= zero;//iva
		BigDecimal ivaRetenido= zero;//deglosePorConcepto.ivaRetenido
		BigDecimal isr= zero;//deglosePorConcepto.isr
		BigDecimal total=zero;//deglosePorConcepto.total
		BigDecimal salvamento=zero;//deglosePorConcepto.totalSalvamento
		String conceptos= "";
		int contador = 0;
		if (!lstdetalleDeglose.isEmpty()){
				for (DesglosePagoConceptoDTO desglose : lstdetalleDeglose){
					if(null!= desglose.getSubtotal())
						subtotal=subtotal.add( desglose.getSubtotal());
					 
					if(null!= desglose.getIva())
						iva=iva.add( desglose.getIva());
					
					if(null!= desglose.getIvaRetenido())
						ivaRetenido=ivaRetenido.add( desglose.getIvaRetenido());
					
					if(null!= desglose.getIsr())
						isr=isr.add( desglose.getIsr());
					
					if(null!= desglose.getTotal())
						total=total.add( desglose.getTotal());
					
					if(null!= desglose.getTotalSalvamento())
						salvamento=salvamento.add( desglose.getTotalSalvamento());
					
					if(!StringUtil.isEmpty(desglose.getConceptoPago())  ){
	                      if (contador ==0) 
	                    	  conceptos += desglose.getConceptoPago();
	                      else  
	                    	  conceptos += ", "+ desglose.getConceptoPago();
	                    contador++;
	                  }			
				}
				degloseTotales.setSubtotal(subtotal);
				degloseTotales.setIva(iva);
				degloseTotales.setIvaRetenido(ivaRetenido);
				degloseTotales.setIsr(isr);
				degloseTotales.setTotal(total);
				degloseTotales.setTotalSalvamento(salvamento);
				degloseTotales.setConceptoPago(conceptos);
				return degloseTotales;
			
			}else
				return null; 
	}	
	
	
	@Override
	public  DesglosePagoConceptoDTO obtenerDegloseTotalesPorOrdenPago(OrdenPagoSiniestro ordenPago) {
		List<DesglosePagoConceptoDTO> lstdetalle=this.getDetalleOrdenPago(ordenPago.getId(),true);
		if (!lstdetalle.isEmpty()){
			return this.obtenerTotalesPorDesglose(lstdetalle);
		}else
				return null; 
	}
	/**
	 * Busca ordenes  de pago segun el filtro seleccionado. si solo compra esta habilitado, omite los datos de la orden de pago.
	 * @param  OrdenPagosDTO filtro
	 * @param boolean soloOrdenCompra
	 */
	@Override
	public List<OrdenPagosDTO> buscarOrdenesPago(OrdenPagosDTO filtro,boolean soloOrdenCompra ) {
		return pagosSiniestroDao.buscarOrdenesPago(filtro, soloOrdenCompra);
	}

	
	/**
	 * obtiene las cantidades totales de una orden de pago
	 * @param Long idEstimacionCobRepCab
	 * @param Long idReporteCabina
	 * @param Long idcoberturaReporteCabina
	 */
	@Override
	public BigDecimal obtenerTotales(Long idEstimacionCobRepCab,
		Long idReporteCabina, Long idcoberturaReporteCabina) {
		Map<String, Object> parameters = new HashMap<String, Object>();	
		BigDecimal total=zero;
		Long longZero= new Long(0);
		if(null!= idReporteCabina &&idReporteCabina!=longZero ){
			parameters.put("reporteCabina.id", idReporteCabina);
		}
		if(null!= idcoberturaReporteCabina &&idcoberturaReporteCabina!=longZero ){
			parameters.put("coberturaReporteCabina.id", idcoberturaReporteCabina);
		}
		if(null!= idEstimacionCobRepCab &&idEstimacionCobRepCab!=longZero ){
			parameters.put("idTercero", idEstimacionCobRepCab);
		}
		//Orden de compra autorizada
		parameters.put("estatus", OrdenCompraService.ESTATUS_AUTORIZADA);
		List<OrdenCompra>  ordenCompras= new ArrayList<OrdenCompra>();
		ordenCompras=	entidadService.findByProperties(OrdenCompra.class, parameters);
		
		for( OrdenCompra ordenCompra : ordenCompras ){
			parameters.clear();
			parameters.put("ordenCompra.id",  ordenCompra.getId());
			parameters.put("estatus", PagosSiniestroService.ESTATUS_LIBERADA);
			List<OrdenPagoSiniestro>  ordenPagos= new ArrayList<OrdenPagoSiniestro>();
			ordenPagos=	entidadService.findByProperties(OrdenPagoSiniestro.class, parameters);
			for( OrdenPagoSiniestro ordenPago : ordenPagos ){
				List<DesglosePagoConceptoDTO> lst= this.getDetalleOrdenPago(ordenPago.getId(),false);
				if(!lst.isEmpty()){
					DesglosePagoConceptoDTO desglose=lst.get(0);
					total= total.add(desglose.getTotalesPorPagar());
				}
			}
		}
		return total;
	}	
	/**
	 * Obtiene una orden de pago por reporte cabina, cobertura , estimacion , tipo pago y estatus del pago.
	 * @param OrdenPagoSiniestro ordenPago
	 */
	@Override
	public List<OrdenPagoSiniestro> obtenerOrdenesPago(Long idEstimacionCobRepCab,
			Long idReporteCabina, Long idcoberturaReporteCabina,
			String tipoPago, String estatusPago) {

		Map<String, Object> parameters = new HashMap<String, Object>();	
		Long longZero= new Long(0);
		List<OrdenPagoSiniestro>  listaOrdenPagosSiniestros= new ArrayList<OrdenPagoSiniestro>();
		if(null!= idReporteCabina &&idReporteCabina!=longZero ){
			parameters.put("reporteCabina.id", idReporteCabina);
		}
		if(null!= idcoberturaReporteCabina &&idcoberturaReporteCabina!=longZero ){
			parameters.put("coberturaReporteCabina.id", idcoberturaReporteCabina);
		}
		if(null!= idEstimacionCobRepCab &&idEstimacionCobRepCab!=longZero ){
			parameters.put("idTercero", idEstimacionCobRepCab);
		}
		parameters.put("estatus", OrdenCompraService.ESTATUS_AUTORIZADA);
		List<OrdenCompra>  ordenCompras= new ArrayList<OrdenCompra>();
		ordenCompras=	entidadService.findByProperties(OrdenCompra.class, parameters);
		for( OrdenCompra ordenCompra : ordenCompras ){
			parameters.clear();
			parameters.put("ordenCompra.id",  ordenCompra.getId());
			if(!StringUtil.isEmpty(tipoPago)){
				parameters.put("tipoPago",  tipoPago);
			}
			
			if(!StringUtil.isEmpty(estatusPago)){
				parameters.put("estatus", estatusPago);
			}
			List<OrdenPagoSiniestro>  ordenPagos= new ArrayList<OrdenPagoSiniestro>();
			ordenPagos=	entidadService.findByProperties(OrdenPagoSiniestro.class, parameters);
			if(!ordenPagos.isEmpty())
				listaOrdenPagosSiniestros.addAll(ordenPagos);
		}
		return listaOrdenPagosSiniestros;
	}
	
	 	/**
		 * Cancela una orden de pago
		 * @param ordePago
		 */
	@Override
	public void cancelarOrdenPago(Long idOrdenPago) throws Exception{
		OrdenPagoSiniestro ordenPago= this.entidadService.findById(OrdenPagoSiniestro.class, idOrdenPago);
		if(null==ordenPago){
			 throw new Exception ("La orden de pago numero: "+ordenPago+". No existe");
		 }	
		if(!StringUtil.isEmpty(ordenPago.getEstatus())  && ordenPago.getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_PAGADA)){
			throw new Exception("La orden de compra no puede Cancelarse, ya que se encuentra pagada.");
			
		}
		if(!StringUtil.isEmpty(ordenPago.getEstatus())  && ordenPago.getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_CANCELADA)){
			throw new Exception("La orden de compra ya esta cancelada.");
		}		
		ordenPago.setEstatus(PagosSiniestroService.ESTATUS_CANCELADA);
		this.guardarOrdenPago(ordenPago);
		
	}

	/**
	 * Envia correo de cancelacion de orden compra
	 * @param Long idOrdenCompra
	 */
	@Override
	public void solicitarCancelacionOrdenCompra(Long idOrdenPago) {
		String usuarioSistema =usuarioService.getUsuarioActual().getNombreCompleto();
		OrdenPagosDTO filtro= new OrdenPagosDTO();
		filtro.setId(idOrdenPago);
		List<OrdenPagosDTO> list=this.buscarOrdenesPago(filtro,false);
		if(!list.isEmpty()){
			filtro=list.get(0);
			filtro.setUsuarioSistema(usuarioSistema);
			OrdenCompra ordenCompra=this.ordenCompraService.obtenerOrdenCompra(filtro.getNumOrdenCompra());
			ReporteCabina reporte = ordenCompra.getReporteCabina(); 
			HashMap<String, Serializable> dataArg = new HashMap<String, Serializable>();
			dataArg.put("reporte", reporte);  
			if(StringUtil.isEmpty(filtro.getProveedor()))
				filtro.setProveedor("N/A");
			
			if(StringUtil.isEmpty(filtro.getBeneficiario()))
				filtro.setBeneficiario("N/A");
			
			if(StringUtil.isEmpty(filtro.getTipoPago()))
				filtro.setTipoPago("N/A");			
			
			if(StringUtil.isEmpty(filtro.getConceptoPago()))
				filtro.setConceptoPago("N/A");
			

			if(StringUtil.isEmpty(filtro.getTerminoAjuste()))
				filtro.setTerminoAjuste("N/A");
			
			
			if(StringUtil.isEmpty(usuarioSistema))
				filtro.setUsuarioSistema("N/A");
			else 
				filtro.setUsuarioSistema(usuarioSistema);
				
			
			if(StringUtil.isEmpty(filtro.getComentarios()))
				filtro.setComentarios("N/A");
			
			dataArg.put("numOrdenCompra", filtro.getNumOrdenCompra() );
			dataArg.put("proveedor"     , filtro.getProveedor()      );
			dataArg.put("beneficiario"  , filtro.getBeneficiario()   );
			dataArg.put("tipoPago"      , filtro.getTipoPago()       );
			dataArg.put("conceptoPago"  , filtro.getConceptoPago()   );
			dataArg.put("terminoAjuste" , filtro.getTerminoAjuste()  );
			dataArg.put("usuarioSistema", filtro.getUsuarioSistema() );
			dataArg.put("comentarios"   , filtro.getComentarios()    );
			dataArg.put(EnvioNotificacionesService.ATR_OFICINAID, reporte.getOficina().getId());
			dataArg.put(EnvioNotificacionesService.ATR_REPORTE, reporte);
			envioNotificacionesService.enviarNotificacionSiniestros(CODIGOCORREO, dataArg);
		}
		
	}
	
	
	
	
	 @EJB
		public void setEstadoFacadeRemote(EstadoFacadeRemote estadoFacadeRemote) {
			this.estadoFacadeRemote = estadoFacadeRemote;
		}
	 
	 public PagosSiniestroDao getPagosSiniestroDao() {
			return pagosSiniestroDao;
		}
		public void setPagosSiniestroDao(PagosSiniestroDao pagosSiniestroDao) {
			this.pagosSiniestroDao = pagosSiniestroDao;
		}

		public OrdenCompraService getOrdenCompraService() {
			return ordenCompraService;
		}
		public void setOrdenCompraService(OrdenCompraService ordenCompraService) {
			this.ordenCompraService = ordenCompraService;
		}
		public EntidadService getEntidadService() {
			return entidadService;
		}
		public void setEntidadService(EntidadService entidadService) {
			this.entidadService = entidadService;
		}
		public UsuarioService getUsuarioService() {
			return usuarioService;
		}
		public void setUsuarioService(UsuarioService usuarioService) {
			this.usuarioService = usuarioService;
		}
		public CatalogoGrupoValorService getCatalogoGrupoValorService() {
			return catalogoGrupoValorService;
		}
		public void setCatalogoGrupoValorService(
				CatalogoGrupoValorService catalogoGrupoValorService) {
			this.catalogoGrupoValorService = catalogoGrupoValorService;
		}
		public EstimacionCoberturaSiniestroService getEstimacionCoberturaSiniestroService() {
			return estimacionCoberturaSiniestroService;
		}

		public void setEstimacionCoberturaSiniestroService(
				EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService) {
			this.estimacionCoberturaSiniestroService = estimacionCoberturaSiniestroService;
		}
		public EstadoFacadeRemote getEstadoFacadeRemote() {
			return estadoFacadeRemote;
		}
		
	
		@Override
		public List<DocumentoFiscal> buscarFacturasLiquidacion(Long idProveedor){
			List<DocumentoFiscal> listaFacturasLiquidacion = new ArrayList<DocumentoFiscal>();
			if(idProveedor != null){	
				//listaFacturasLiquidacion = this.liquacionSiniestroService.ordenaFacturasLiquidacion( pagosSiniestroDao.buscarFacturasLiquidacion(idProveedor) );
				listaFacturasLiquidacion = this.pagosSiniestroDao.getFacturasConOrdenesCompraOrderByDateLow(idProveedor);
			}
			return listaFacturasLiquidacion;			
		}
		
		@Override
		public List<FacturaLiquidacionDTO> buscarOrdenesPagoFactura(Long idFactura)
		{
			List<FacturaLiquidacionDTO> listaOrdenesPagoSiniestros = new ArrayList<FacturaLiquidacionDTO>();			
			listaOrdenesPagoSiniestros = pagosSiniestroDao.buscarOrdenesPagoFactura(idFactura);
			for(FacturaLiquidacionDTO registro:listaOrdenesPagoSiniestros)
			{				
				DesglosePagoConceptoDTO desglose;
				desglose = this.obtenerTotalesSP(registro.getOrdenPago().getId(), Boolean.FALSE);
				registro.setDesgloseConceptos(desglose);
			}
			return listaOrdenesPagoSiniestros;			
		}

		


		@Override
		public OrdenPagoSiniestro autorizarOrdenPago(Long idOrdenPago) {
			BigDecimal total = zero;
			OrdenPagoSiniestro ordenPago = this.entidadService.findById(OrdenPagoSiniestro.class,idOrdenPago);
			OrdenCompra oc = this.entidadService.findById(OrdenCompra.class, ordenPago.getOrdenCompra().getId());
			Long idTercero = null;	
			if (oc.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_AFECTACION_RESERVA)){
				if(null!=oc.getCoberturaReporteCabina()) {
					idTercero=oc.getIdTercero();
				}
				BigDecimal reservaDisp = movimientoSiniestroService.obtenerReservaAfectacion(idTercero, Boolean.TRUE);
				DesglosePagoConceptoDTO desgloseDTO= this.obtenerTotales(ordenPago.getId(),false);
				total=desgloseDTO.getTotalesPorPagar();
				if(reservaDisp.subtract(total).compareTo(zero)==1){
					ordenPago.setTipoPago(PagosSiniestroService.TIPOPAGO_PARCIAL);
				}else if(reservaDisp.subtract(total).compareTo(zero)==0){
					ordenPago.setTipoPago(PagosSiniestroService.TIPOPAGO_TOTAL);
				}
				List <OrdenPagoSiniestro> listOrdenP	=null;
				if(null!=oc.getCoberturaReporteCabina())
					listOrdenP=this.obtenerOrdenesPago(oc.getIdTercero(), oc.getReporteCabina().getId(),oc.getCoberturaReporteCabina().getId(),PagosSiniestroService.TIPOPAGO_TOTAL, PagosSiniestroService.ESTATUS_LIBERADA);
				else	
					listOrdenP	=	this.obtenerOrdenesPago(oc.getIdTercero(), oc.getReporteCabina().getId(),null, PagosSiniestroService.TIPOPAGO_TOTAL, PagosSiniestroService.ESTATUS_LIBERADA);
				if(!listOrdenP.isEmpty()){
					ordenPago.setTipoPago(PagosSiniestroService.TIPOPAGO_COMPLEMENTO);
				}
			}
			ordenPago.setEstatus(ESTATUS_LIBERADA);
			ordenPago= this.guardarOrdenPago(ordenPago);
			
			if ( !ordenPago.getEstatus().isEmpty() && ordenPago.getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_LIBERADA) ){
				oc.setEstatus(OrdenCompraService.ESTATUS_ASOCIADA);
			}
			DocumentoFiscal factura = null;
			List<DocumentoFiscal> listaFac = this.recepcionFacturaService.obtenerFacturasRegistradaByOrdenCompra(oc.getId(), FacturaSiniestroService.ESTATUS_FACTURA_REGISTRADA);
			if (null!=listaFac && !listaFac.isEmpty()){
				factura=listaFac.get(0);
			}
			if(null!=factura && !StringUtil.isEmpty(factura.getNumeroFactura())){
				oc.setFactura(factura.getNumeroFactura());
			}
			this.entidadService.save(oc);
			return ordenPago;
		}
		@Override
		public OrdenPagoSiniestro generacionAutomaticaOrdenPago(
				Long idOrdenCompra ) {
			if(null==idOrdenCompra){
				return null;
			}
			List<OrdenPagoSiniestro>  listaOP = this.entidadService.findByProperty(OrdenPagoSiniestro.class, "ordenCompra.id", idOrdenCompra);
			if(null!=listaOP && !listaOP.isEmpty()){
				return null;
			}
			OrdenCompra ordenCompra = this.entidadService.findById(OrdenCompra.class, idOrdenCompra);
			DocumentoFiscal factura = null;
			List<DocumentoFiscal> listaFac = this.recepcionFacturaService.obtenerFacturasRegistradaByOrdenCompra(idOrdenCompra, FacturaSiniestroService.ESTATUS_FACTURA_REGISTRADA);
			if (null!=listaFac && !listaFac.isEmpty()){
				factura=listaFac.get(0);
			}
			OrdenPagoSiniestro ordenPago = new OrdenPagoSiniestro();
			if(null!=factura && !StringUtil.isEmpty(factura.getNumeroFactura())){
				ordenPago.setFactura(factura.getNumeroFactura());
			}
			ordenPago.setComentarios("Generacion Automatica de Orden de Pago");
			ordenPago.setEstatus(PagosSiniestroService.ESTATUS_PREREGISTRO);						 
			 ordenPago.setOrdenCompra(ordenCompra);			
			 ordenPago.setOrigenPago(ordenCompra.getTipoAfectado());
			this.guardarOrdenPago(ordenPago);
			if(null!=ordenPago){
				ordenCompra.setOrdenPago(ordenPago);
				this.entidadService.save(ordenCompra);
			}
			return ordenPago;
		}
	
	/**Deberá obtener una lista de nombres de beneficiarios de Orden de Compra donde su orden de pago no aparezca en alguna liquidación.
	  *@param String beneficiario
	 * @return  List<String>   
	 * */
	@Override
	public List<String> buscarBeneficiario(String beneficiario) {
		return this.pagosSiniestroDao.buscarBeneficiario(beneficiario);
	}
	
	@Override
	public List<Map<String, Object>> buscarBeneficiariosLiquidacion()
	{
		List<Map<String, Object>> listaBeneficiarios = new ArrayList<Map<String, Object>>();
		 List<String> list = this.pagosSiniestroDao.buscarBeneficiario(null);
		for(String beneficiario : list){
			Map<String, Object> beneficiarioMap = new HashMap<String, Object>();		
		
			beneficiarioMap.put("id", beneficiario);
			beneficiarioMap.put("label", beneficiario.replaceAll("\"", "'"));
			listaBeneficiarios.add(beneficiarioMap);					
		}
		return listaBeneficiarios;
	}
	
	/**Deberá obtener dado un nombre de beneficiario, sus órdenes de pago que no aparezcan en alguna liquidación.
	 * 
	 * @param beneficiario
	 * @return List<OrdenPagoSiniestro>
	 */
	@Override
	public List<OrdenPagoSiniestro> buscarOrdenPagoIndemnizacion(
			String beneficiario) {
		return this.pagosSiniestroDao.buscarOrdenPagoIndemnizacion(beneficiario);
	}	
	
    @Override
	public List<OrdenPagoSiniestro> buscarOrdenesPagoIndemnizacion(String nombreBeneficiario)
	{    	
    	List<OrdenPagoSiniestro> listaOrdenesPago = pagosSiniestroDao.buscarOrdenesPagoIndemnizacion(nombreBeneficiario);
    	for(OrdenPagoSiniestro registro:listaOrdenesPago)
		{				
			DesglosePagoConceptoDTO desglose;
			desglose = this.obtenerTotalesSP(registro.getId(),Boolean.FALSE);
			registro.setTotales(desglose);
		}    	
		return listaOrdenesPago;		
	}


	@Override
	public OrdenPagosDTO obtenerOrdenPago(Long idOrdenCompra) {
		OrdenPagosDTO ordenPago = null;
		OrdenPagosDTO filtro = new OrdenPagosDTO();
		filtro.setNumOrdenCompra(idOrdenCompra);		
		List<OrdenPagosDTO> list= buscarOrdenesPago(filtro,false);		
		if(!list.isEmpty() && list.get(0).getId() != null){
			ordenPago = list.get(0);
		}else{
			generacionAutomaticaOrdenPago(idOrdenCompra);
			list= buscarOrdenesPago(filtro,false);
			ordenPago = list.get(0);
		}
		//obtener salvamento registrado
		if(ordenPago.getOrdenCompra().getIndemnizacion() != null){
			RecuperacionSalvamento salvamento = recuperacionSalvamentoService.obtenerRecuperacionSalvamento(
					ordenPago.getOrdenCompra().getIdTercero());
			if(salvamento != null && !salvamento.getEstatus().equals(Recuperacion.EstatusRecuperacion.CANCELADO) && 
					!salvamento.getEstatus().equals(Recuperacion.EstatusRecuperacion.INACTIVO)){			
				//beneficiario
				ordenPago.setSalvamento(salvamento.getValorEstimado());
				ordenPago.setIvaSalvamento(zero);
				ordenPago.setTotalSalvamento(salvamento.getValorEstimado());		
				if(ordenPago.getOrdenCompra().getIdBeneficiario() != null){
					//proveedor
					PrestadorServicio prestador = entidadService.findById(PrestadorServicio.class, 
							ordenPago.getOrdenCompra().getIdBeneficiario().intValue());
					if(prestador != null && prestador.getPersonaMidas() != null && 
							prestador.getPersonaMidas().getTipoPersona().equals(PersonaMidas.TIPO_PERSONA.PM.toString())){
						String iva = parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS, ParametroGlobalService.IVA_VENTA_SALVAMENTO);
						ordenPago.setIvaSalvamento(!StringUtil.isEmpty(iva)? new BigDecimal(iva):BigDecimal.ZERO);
						BigDecimal valorIva = BigDecimal.ONE.add(ordenPago.getIvaSalvamento().divide(BigDecimal.valueOf(100d)));
						ordenPago.setSalvamento(salvamento.getValorEstimado().divide(valorIva, 4, RoundingMode.HALF_UP));
					}
				}
			}
		}
		return ordenPago;
	}		
		
}
