package mx.com.afirme.midas2.service.impl.emision.ppct;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDTO;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.emision.ppct.PagoProveedorDao;
import mx.com.afirme.midas2.domain.contabilidad.SolicitudCheque;
import mx.com.afirme.midas2.domain.emision.ppct.CoberturaSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.OrdenPago;
import mx.com.afirme.midas2.domain.emision.ppct.Proveedor;
import mx.com.afirme.midas2.domain.emision.ppct.Recibo;
import mx.com.afirme.midas2.domain.emision.ppct.ReciboProveedor;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.emision.ppct.SaldoDTO;
import mx.com.afirme.midas2.dto.emision.ppct.StatusSolicitudChequeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.calculos.SolicitudChequesMizarService;
import mx.com.afirme.midas2.service.emision.ppct.PagoProveedorService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;


@Stateless
public class PagoProveedorServiceImpl implements PagoProveedorService {
	
	@Override
	public OrdenPago obtenerPreeliminar(OrdenPago ordenPago, String sessionId) {
		
		BigDecimal numeroDisponibles = BigDecimal.ZERO;
		Proveedor proveedor = ordenPago.getProveedor();
		
		if(proveedor == null || proveedor.getId() == null){
			
			throw new RuntimeException("El proveedor no esta definido");
			
		}
		
		Date fechaCorte = obtieneUltimoDiaMes (ordenPago.getFechaCorte());
		
		//Quita el marcado de 'en edicion' de cualquier recibo que estuviera editandose previamente
		pagoProveedorDao.desasignarRecibosPreeliminares(sessionId);
				
		//Se buscan todos los recibos pagados hasta la fecha de corte que no pertenezcan a una Orden de Pago ni esten editandose en este momento
		//Los marca como 'en edicion' usando el sessionId
		numeroDisponibles = pagoProveedorDao.asignarRecibosPreeliminares(proveedor, fechaCorte, sessionId);
		
		if (numeroDisponibles.compareTo(BigDecimal.ZERO) == 0) {
		
			throw new RuntimeException("No hay recibos disponibles para generar la Orden de Pago");
		
		}
		
		proveedor = entidadDao.findById(Proveedor.class, proveedor.getId());
		
		
		//Obtiene el importe preeliminar de la Orden de Pago 
		BigDecimal total = pagoProveedorDao.obtenerImportePreeliminar(sessionId);
		ordenPago.setProveedor(proveedor);
		ordenPago.setFechaCorte(fechaCorte);
		ordenPago.setImportePreeliminar(total==null?BigDecimal.ZERO:total);
		
		return ordenPago;
	}
	
	@Override
	public void desasignarRecibosPreeliminares(String sessionId) {
		
		pagoProveedorDao.desasignarRecibosPreeliminares(sessionId);
	
	}
		
	@Override
	public BigDecimal seleccionar(BigDecimal reciboId, BigDecimal importePreeliminar) {
		
		BigDecimal factor = new BigDecimal("-1");
		
		importePreeliminar = (importePreeliminar==null? BigDecimal.ZERO:importePreeliminar);
		
		ReciboProveedor recibo = entidadDao.findById(ReciboProveedor.class, reciboId);
		
		recibo.setSeleccionado((recibo.getSeleccionado() != null ? !recibo.getSeleccionado() : true));
		
		entidadDao.update(recibo);
		
		//Dependiendo si se esta seleccionando/ des-seleccionando, se resta/suma el monto del recibo al importe preeliminar de la Orden de Pago
		//(En este caso se trata de seleccionar los recibos que se van a excluir de la Orden de Pago)
		if (!recibo.getSeleccionado()) {
			factor = BigDecimal.ONE;
		}
		
		return importePreeliminar.add(recibo.getMonto().abs().multiply(factor));
		
	}
	
	@Override
	public OrdenPago guardarOrdenPago(OrdenPago ordenPago, String sessionId) {
		
		//Se valida que al menos exista un recibo en la Orden de Pago (mediante el importe preeliminar)
		if (ordenPago == null || ordenPago.getImportePreeliminar() == null || ordenPago.getImportePreeliminar().compareTo(BigDecimal.ZERO) <= 0) {
			throw new RuntimeException("La Orden de Pago debe incluir al menos un recibo");
		}
		
		if (ordenPago.getId() != null) {
			
			ordenPago = pagoProveedorDao.obtenerOrdenPago(ordenPago);
			
		}
		
		if (ordenPago.getSolicitudCheque() == null || ordenPago.getSolicitudCheque().getId() == null) {
			
			//Se guarda la Orden de Pago para obtener su id y su folio
			ordenPago = pagoProveedorDao.guardarOrdenPago(ordenPago);
					
			//Se asignan los respectivos recibos (solo los que quedaron sin seleccionar y pertenecen a la sesion en progreso)
			pagoProveedorDao.asignarRecibos(ordenPago, sessionId);
			
			//Se genera la solicitud de cheque de Seycos
			SolicitudCheque solicitudCheque = pagoProveedorDao.obtenerSolicitudCheque(ordenPago);
			
			ordenPago.setSolicitudCheque(solicitudCheque);
			
			ordenPago = pagoProveedorDao.guardarOrdenPago(ordenPago);
			
		}
		
		//Se manda la solicitud de Cheque de Seycos a MIZAR
		enviarSolicitudChequeMIZAR(ordenPago);
				
		//Quita el marcado de 'en edicion'
		pagoProveedorDao.desasignarRecibosPreeliminares(sessionId);
				
		return ordenPago;
	}
	
	@Override
	public OrdenPago cancelarOrdenPago(OrdenPago ordenPago) {
		
		ordenPago = pagoProveedorDao.obtenerOrdenPago(ordenPago);
		
		if (!ordenPago.getSolicitudCheque().getStatus().equals(StatusSolicitudChequeDTO.PENDIENTE)) {
			throw new RuntimeException("La Orden de Pago no se puede cancelar debido a su estatus actual");
		}
		
		ordenPago.getSolicitudCheque().setStatus(StatusSolicitudChequeDTO.CANCELADO);
		
		entidadDao.update(ordenPago.getSolicitudCheque());
				
		return ordenPago;
	}

	@Override
	public List<Proveedor> obtenerProveedores() {
		
		return entidadDao.findAll(Proveedor.class);

	}

	@Override
	public List<CoberturaSeycos> obtenerCoberturasProveedor(Proveedor proveedor, String claveNegocio) {
		return pagoProveedorDao.obtenerCoberturasProveedor(proveedor, claveNegocio);
		
	}
	
	@Override
	public List<StatusSolicitudChequeDTO> obtenerStatus() {
				
		List<StatusSolicitudChequeDTO> statusSolicitudChequeList = new ArrayListNullAware<StatusSolicitudChequeDTO>();
		
		statusSolicitudChequeList.add(new StatusSolicitudChequeDTO(StatusSolicitudChequeDTO.PENDIENTE));
		statusSolicitudChequeList.add(new StatusSolicitudChequeDTO(StatusSolicitudChequeDTO.SOLICITADO));
		statusSolicitudChequeList.add(new StatusSolicitudChequeDTO(StatusSolicitudChequeDTO.TERMINADO));
		statusSolicitudChequeList.add(new StatusSolicitudChequeDTO(StatusSolicitudChequeDTO.CANCELADO));
		
		return statusSolicitudChequeList;
	}

	@Override
	public List<OrdenPago> obtenerOrdenesPago(OrdenPago filtro) {
		
		if (filtro.getRangoFechas() != null) {
			
			if (filtro.getRangoFechas().getFechaInicio() != null) {
			
				filtro.getRangoFechas().setFechaInicio(obtieneUltimoDiaMes (filtro.getRangoFechas().getFechaInicio()));
				
			}
			
			if (filtro.getRangoFechas().getFechaFin() != null) {
				
				filtro.getRangoFechas().setFechaFin(obtieneUltimoDiaMes (filtro.getRangoFechas().getFechaFin()));
				
			}
			
		}
		
		if (filtro.getClaveNegocio() == null) {
			
			throw new RuntimeException("El filtro debe contar con una Clave de Negocio");
		
		}
		
				
		return pagoProveedorDao.obtenerOrdenesPago(filtro);
		
	}

	@Override
	public List<Recibo> obtenerRecibosProveedor(ReciboProveedor filtro, Boolean historicos) {
		
		if (filtro == null) {
		
			throw new RuntimeException("El filtro no debe ser nulo");
			
		}
				
		if (historicos) {
			
			if (filtro.getOrdenPago() == null || filtro.getOrdenPago().getId() == null) {
				
				throw new RuntimeException("El filtro debe contar con una Orden de Pago");
				
			}
			
			return pagoProveedorDao.obtenerRecibosOrdenPago(filtro);
			
		} else {
			
			if (filtro.getSesion() == null || filtro.getSesion().trim().equals("")) {
				
				throw new RuntimeException("El filtro debe contar con un id de sesion");
				
			}
			
			if(filtro.getConfiguracionPago() == null || filtro.getConfiguracionPago().getProveedor() == null || filtro.getConfiguracionPago().getProveedor().getId() == null) {
				
				throw new RuntimeException("El filtro debe contener al proveedor");
				
			}
			
			return pagoProveedorDao.obtenerRecibosPreeliminares(filtro);
		}
		
	}
		
	@Override
	public OrdenPago obtenerOrdenPago(OrdenPago filtro) {
		
		return pagoProveedorDao.obtenerOrdenPago(filtro);
		
	}
	
	@Override
	public void guardarOrdenesPagoAutomaticas() {

		logger.info("Ejecutando tarea guardarOrdenesPagoAutomaticas");
		
		Date fechaCorte = new Date();
		
		OrdenPago ordenPago = null;
		
		List<Proveedor> proveedores = obtenerProveedores();
		
		StringBuilder sessionId = new StringBuilder("-12345JOB");
		
		for (Proveedor proveedor : proveedores) {
			
			try {
				
				sessionId.append(proveedor.getId().toString());
				
				logInfo("Procesando Orden de Pago al proveedor: " + proveedor.getNombre() + ", a la fecha de Corte: " + fechaCorte 
						+ " , id sesion: " + sessionId);
				
				ordenPago = obtenerPreeliminar(proveedor, fechaCorte, sessionId.toString());
				
				ordenPago = guardarOrdenPago(ordenPago, sessionId.toString());
				
				logInfo("Se genero la Orden de Pago " + ordenPago.getFolio() + " al proveedor: " + proveedor.getNombre());
				
			} catch (Exception ex) {
				
				logError("Fallo la generacion de Orden de Pago al proveedor: " + proveedor.getNombre() + ", a la fecha de Corte: " + fechaCorte);
			
			}
			
		}
		
	}
	
	@Override
	public TransporteImpresionDTO obtenerDetalleReporteSaldos(SaldoDTO filtro) {
		
		return obtenerReporteSaldos(pagoProveedorDao.obtenerDetalleReporteSaldos(filtro), SaldoDTO.DETALLE, SaldoDTO.DETALLE + "_" + filtro.getPeriodo());
		
	}
	
	@Override
	public TransporteImpresionDTO obtenerResumenReporteSaldos(SaldoDTO filtro) {
		
		return obtenerReporteSaldos(pagoProveedorDao.obtenerResumenReporteSaldos(filtro), SaldoDTO.RESUMEN, SaldoDTO.RESUMEN + "_" + filtro.getPeriodo());				
		
	}
	
	private TransporteImpresionDTO obtenerReporteSaldos(Collection<?> dataSource, String documento, String nombreArchivo) {
		
		ExcelExporter exporter = new ExcelExporter(SaldoDTO.class, documento);
		
		return exporter.exportCSV(dataSource, nombreArchivo);
				
	}
	
	private OrdenPago obtenerPreeliminar(Proveedor proveedor, Date fechaCorte, String sessionId) {
			OrdenPago ordenPago = new OrdenPago();
			ordenPago.setFechaCorte(fechaCorte);
			ordenPago.setProveedor(proveedor);
		return obtenerPreeliminar(ordenPago, sessionId);
	}

	private void enviarSolicitudChequeMIZAR(OrdenPago ordenPago) {
		
		try {
		
			Long idSolicitudCheque = ordenPago.getSolicitudCheque().getId().longValue();
			
			SolicitudChequeDTO solicitudChequeDTO = obtenerSolicitudChequeProveedor(idSolicitudCheque);
			
			mizarService.agregarSolicitudChequeMIZAR(solicitudChequeDTO);
			
			mizarService.procesarSolicitudesChequeEnMIZAR(idSolicitudCheque);
				
		} catch (Exception ex) {
			
			throw new RuntimeException(ex);
		
		}
		
	}
	
	private SolicitudChequeDTO obtenerSolicitudChequeProveedor(Long idSolicitudCheque) {
		
		SolicitudChequeDTO solicitudCheque = solicitudChequeFacade.obtenerSolicitudCheque(idSolicitudCheque);	
		
		solicitudCheque.setIdSesion(solicitudCheque.getIdSolCheque());
		solicitudCheque.setIdAgente(0L);
		solicitudCheque.setAuxiliar(0L);
		solicitudCheque.setPctIVAAcreditable(16);
		solicitudCheque.setClaveTipoOperacion(85);
		
	    return solicitudCheque;   
		
	}
	
	private void logInfo(String descripcion) {
		
		logger.info(descripcion);
		
	}
	
	private void logError(String descripcion) {
		
		logger.warn(descripcion);
		StoredProcedureErrorLog.doLog2("PPCT", StoredProcedureErrorLog.TipoAccion.GUARDAR, "guardarOrdenesPagoAutomaticas", descripcion);
		
	}
	
	private Date obtieneUltimoDiaMes (Date fechaOriginal) {
		
		Calendar calendar = Calendar.getInstance();  
        calendar.setTime(fechaOriginal);  

        calendar.add(Calendar.MONTH, 1);  
        calendar.set(Calendar.DAY_OF_MONTH, 1);  
        calendar.add(Calendar.DATE, -1);  
        
		return DateUtils.truncate(calendar.getTime(), Calendar.DATE);
		
	}
	
	public void initialize() {
		String timerInfo = "TimerGuardarOrdenesPagoAutomaticas";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0 0 L * ?
				expression.minute(0);
				expression.hour(0);
				expression.dayOfMonth("Last");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerGuardarOrdenesPagoAutomaticas", false));
				
				logger.info("Tarea TimerGuardarOrdenesPagoAutomaticas configurado");
			} catch (Exception e) {
				logger.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		logger.info("Cancelar Tarea TimerGuardarOrdenesPagoAutomaticas");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			logger.error("Error al detener TimerGuardarOrdenesPagoAutomaticas:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		guardarOrdenesPagoAutomaticas();
	}
		
	
	private static Logger logger = Logger.getLogger(PagoProveedorServiceImpl.class);
	
	@EJB
	private PagoProveedorDao pagoProveedorDao;
	
	@EJB
	private EntidadDao entidadDao;
	
	@EJB
	private SolicitudChequeFacadeRemote solicitudChequeFacade;
	
	@EJB
	private SolicitudChequesMizarService mizarService;
	
	@Resource	
	private TimerService timerService;
	
	@EJB
	private SistemaContext sistemaContext;

}
