/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Marzo 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaFrecuenciaPagosDao;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaFrecuenciaPagosDaoImpl;
import mx.com.afirme.midas2.domain.compensaciones.CaFrecuenciaPagos;
import mx.com.afirme.midas2.service.compensaciones.CaFrecuenciaPagosService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless

public class CaFrecuenciaPagosServiceImpl  implements CaFrecuenciaPagosService {
	
	@EJB
	private CaFrecuenciaPagosDao CaFrecuenciaPagos;
	private static final Logger LOG = LoggerFactory.getLogger(CaFrecuenciaPagosDaoImpl.class);

    public void save(CaFrecuenciaPagos entity) {   
	    try {
	    	LOG.info(">> save()");
	    	CaFrecuenciaPagos.save(entity);	  
	    	LOG.info("<< save()");
	    } catch (RuntimeException re) {	 
	    	LOG.error("Información del Error", re);
	        throw re;
	    }
    }
    

    public void delete(CaFrecuenciaPagos entity) { 
    	try {
    		LOG.info(">> delete()");
    		CaFrecuenciaPagos.delete(entity);
    		LOG.info("<< delete()");
    	} catch (RuntimeException re) {
    		LOG.error("Información del Error", re);
    		throw re;
    	}
    }
    

    public CaFrecuenciaPagos update(CaFrecuenciaPagos entity) {   
        try {
        	LOG.info(">> update()");
	        CaFrecuenciaPagos result = CaFrecuenciaPagos.update(entity);
	        LOG.info("<< update()");
	        return result;
	    } catch (RuntimeException re) {
	    	LOG.error("Información del Error", re);
            throw re;
	    }
    }
    
    public CaFrecuenciaPagos findById( Long id) {
    	LOG.info(">> findById()");
	    try {
            CaFrecuenciaPagos instance = CaFrecuenciaPagos.findById(id);
           	LOG.info("<< findById()");
            return instance;
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
        }
        return null;
    }   


    public List<CaFrecuenciaPagos> findByProperty(String propertyName, final Object value) {    	
		try {
			return CaFrecuenciaPagos.findByProperty(propertyName, value);
		} catch (RuntimeException re) {
			
			return null;
		}
	}			
	public List<CaFrecuenciaPagos> findByNombre(Object nombre) {
		return findByProperty(CaFrecuenciaPagosDaoImpl.NOMBRE, nombre);
	}
	
	public List<CaFrecuenciaPagos> findByValor(Object valor) {
		return findByProperty(CaFrecuenciaPagosDaoImpl.VALOR, valor);
	}
	
	public List<CaFrecuenciaPagos> findByDivisor(Object valor) {
		return findByProperty(CaFrecuenciaPagosDaoImpl.DIVISOR, valor);
	}
	
	public List<CaFrecuenciaPagos> findByUsuario(Object usuario) {
		return findByProperty(CaFrecuenciaPagosDaoImpl.USUARIO, usuario);
	}
	
	public List<CaFrecuenciaPagos> findByBorradologico(Object borradologico) {
		return findByProperty(CaFrecuenciaPagosDaoImpl.BORRADOLOGICO, borradologico);
	}
	
	public List<CaFrecuenciaPagos> findAll() {	
		try {
			return CaFrecuenciaPagos.findAll();
		} catch (RuntimeException re) {
			return null;
		}
	}
	
}