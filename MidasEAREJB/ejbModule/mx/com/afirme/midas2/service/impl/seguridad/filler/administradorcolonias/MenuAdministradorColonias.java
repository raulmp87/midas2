package mx.com.afirme.midas2.service.impl.seguridad.filler.administradorcolonias;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

public class MenuAdministradorColonias {
	private List<Menu> listaMenu = null;
	
	public MenuAdministradorColonias() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		Menu menu;
				
		menu = new Menu(new Integer("1"),"m4","Producto", "Submenu Producto", null, true);
		listaMenu.add(menu);				
	
		menu = new Menu(new Integer("2"),"m4_3","Cat�logos", "Submenu Cat�logos", null, true);
		listaMenu.add(menu);
			
		menu = new Menu(new Integer("3"),"m4_3_1","Autos", "Submenu Autos", null, false);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("4"),"m4_3_2","Vida", "Submenu Vida", null, false);
		listaMenu.add(menu);
				
		menu = new Menu(new Integer("5"),"m4_3_3","Da�os", "Submenu Da�os", null, true);
		listaMenu.add(menu);		
		
		menu = new Menu(new Integer("6"),"m4_3_3_11","Colonias", "Submenu Colonias", null, true);
		listaMenu.add(menu);
								
		menu = new Menu(new Integer("7"),"m6","Pendientes", "Submenu Pendientes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("8"),"m6_1","Lista de Pendientes", "Submenu Lista de Pendientes", null, true);
		listaMenu.add(menu);
						
		return this.listaMenu;
	}
}
