package mx.com.afirme.midas2.service.impl.poliza.seguroobligatorio;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.joda.time.DateTime;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.DatoSeccionContinuity;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.ParametroSeguroObligatorio;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.PolizaAnexa;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.PolizaAnexaId;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;

@Stateless
public class SeguroObligatorioServiceImpl implements SeguroObligatorioService{
	
	@PersistenceContext 
	private EntityManager entityManager;
	
	@EJB
	protected IncisoService incisoService;
	
	@EJB
	protected EntidadService entidadService;
	
	@EJB
	protected NegocioSeccionService negocioSeccionService;
	
	@EJB
	protected EntidadBitemporalService entidadBitemporalService;	
	
	public List<IncisoCotizacionDTO> getIncisosCotizacion(BigDecimal idToCotizacion){
		return incisoService.findByCotizacionId(idToCotizacion);
	}
	
	public Negocio getNegocioSeguroObligatorio(BigDecimal idToPoliza){
		Negocio negocio = null;
		try{
			/*
			CatalogoValorFijoId idNegSeguroObligatorio = new CatalogoValorFijoId();
			idNegSeguroObligatorio.setIdDato(1);
			idNegSeguroObligatorio.setIdGrupoValores(CatalogoValorFijoDTO.IDGRUPO_NEGOCIO_SEGURO_OBLIGATORIO);
			CatalogoValorFijoDTO idNegocioCVF = entidadService.findById(CatalogoValorFijoDTO.class, idNegSeguroObligatorio);
			
			negocio = entidadService.findById(Negocio.class, Long.valueOf(idNegocioCVF.getDescripcion()));
			*/
			String queryString = "SELECT MIDAS.PKGAUT_SEGUROOBLIGATORIO.fnAut_ObtieneNegSegObligatorio(" +idToPoliza+
						") FROM DUAL" +
						" ";

			Query query = entityManager.createNativeQuery(queryString.toString());			
					
			BigDecimal negocioId = (BigDecimal) query.getSingleResult();
				
			if(negocioId != null && negocioId.longValue() > 0){
				negocio = this.entidadService.findById(Negocio.class, negocioId.longValue());
			}
		}catch(Exception e){
			e.printStackTrace();
		}		
		return negocio;
	}
	
	public Boolean esNegocioSeguroObligatorio(Long idToNegocio){
		try{
			String queryString = "Select model " +
			" FROM CatalogoValorFijoDTO model  " +
			" WHERE model.id.idGrupoValores = :idGrupoValores " +
			" AND model.descripcion = :idNegocio " +
			" ";
			
			Query query = entityManager.createQuery(queryString.toString(), CatalogoValorFijoDTO.class);						
			query.setParameter("idGrupoValores", CatalogoValorFijoDTO.IDGRUPO_NEGOCIO_SEGURO_OBLIGATORIO);
			query.setParameter("idNegocio", idToNegocio);
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			CatalogoValorFijoDTO valor = (CatalogoValorFijoDTO) query.getSingleResult();
			if(valor != null){
				return true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public Double getPrimaTotalSeguroObligatorio(){
		Double primaTotal = 1.0;
		try{
			CatalogoValorFijoId idNegSeguroObligatorio = new CatalogoValorFijoId();
			idNegSeguroObligatorio.setIdDato(1);
			idNegSeguroObligatorio.setIdGrupoValores(CatalogoValorFijoDTO.IDGRUPO_PRIMA_SEGURO_OBLIGATORIO);
			CatalogoValorFijoDTO idNegocioCVF = entidadService.findById(CatalogoValorFijoDTO.class, idNegSeguroObligatorio);
			
			primaTotal =  Double.valueOf(idNegocioCVF.getDescripcion());
			
		}catch(Exception e){
			e.printStackTrace();
		}		
		return primaTotal;
	}	
	
	public NegocioSeccion getNegSeccionSOInciso(Negocio negSeguroOblig, CotizacionDTO cotizacion, IncisoCotizacionDTO inciso){
		NegocioSeccion negocioSeccion = null;
		try{
			//Obtiene producto Seguro Obligatorio
			//List<NegocioProducto> productoList = entidadService.findByProperty(NegocioProducto.class, "negocio.idToNegocio", negSeguroOblig.getIdToNegocio());
			//ProductoDTO productoDTO = productoList.get(0).getProductoDTO();
			//Obtiene tipoPoliza Seguro Obligatorio
			//List<NegocioTipoPoliza> negocioTipoPolizaList = entidadService.findByProperty(NegocioTipoPoliza.class, "negocioProducto.idToNegProducto", productoList.get(0).getIdToNegProducto());
			//TipoPolizaDTO tipoPolizaDTO = negocioTipoPolizaList.get(0).getTipoPolizaDTO();
			
			negocioSeccion = negocioSeccionService.getPorProductoNegocioTipoPolizaSeccionDescripcion(cotizacion.getSolicitudDTO().getProductoDTO(), negSeguroOblig, cotizacion.getTipoPolizaDTO(), inciso.getSeccionCotizacion().getSeccionDTO());
			//negocioSeccion = negocioSeccionService.getPorProductoNegocioTipoPolizaSeccionDescripcion(productoDTO, negSeguroOblig, tipoPolizaDTO, inciso.getSeccionCotizacion().getSeccionDTO());
		}catch(Exception e){
			
		}		
		return negocioSeccion;
	}
	
	public NegocioPaqueteSeccion obtieneNegocioPaqueteSeccionPorNegSeccion(BigDecimal idToNegSeccion){
		Map<String, Object> parametros3 = new LinkedHashMap<String, Object>();
		parametros3.put("negocioSeccion.idToNegSeccion", idToNegSeccion);
		return entidadService.findByProperties(NegocioPaqueteSeccion.class, parametros3).get(0);
	}
	
	public NegocioDerechoPoliza obtieneNegocioDerechoPolizaSOByNegocio(Long idToNegocio){
		return obtenerDerechosPolizaCero(idToNegocio);
	}
	
	public void relacionaPolizaSeguroObligatorio(PolizaDTO original, PolizaDTO anexa){
		if(anexa != null){
			try{
				PolizaAnexaId id = new PolizaAnexaId(original.getIdToPoliza(), anexa.getIdToPoliza());
				PolizaAnexa polizaAnexa = new PolizaAnexa();
				polizaAnexa.setId(id);
				polizaAnexa.setPolizaAnexaDTO(anexa);
				polizaAnexa.setOrigen(PolizaAnexa.ORIGEN_MIDAS);
				
				entidadService.save(polizaAnexa);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			//Actualiza Anexa
			try{
				anexa.setClaveAnexa(PolizaDTO.CLAVE_POLIZA_ANEXA);
				entidadService.save(anexa);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<BigDecimal> obtienePolizasCrearSO(String idToPolizaList){
		List<BigDecimal> polizaList = new ArrayList<BigDecimal>(1);
		
		try{
			String queryString = "Select model.idToPoliza FROM PolizaDTO model  " +
					" WHERE TRIM(model.codigoProducto) IN (31,32) " +
					" AND model.claveEstatus = 1 " +
					" AND model.claveAnexa IS NULL " +
					" AND model.idToPoliza NOT IN (Select t2.id.idToPoliza From PolizaAnexa t2)" +
					" AND model.cotizacionDTO.fechaFinVigencia >= :fechaActual " +
					" ";
			if(idToPolizaList != null && !idToPolizaList.isEmpty()){
				queryString+="AND model.idToPoliza IN ("+idToPolizaList+")";
			}
			
			Query query = entityManager.createQuery(queryString.toString(),BigDecimal.class);			
			GregorianCalendar fechaSeguimiento = new GregorianCalendar();
			fechaSeguimiento.setTime(new Date());
			fechaSeguimiento.set(GregorianCalendar.HOUR_OF_DAY, 0);
			fechaSeguimiento.set(GregorianCalendar.MINUTE, 0);
			fechaSeguimiento.set(GregorianCalendar.SECOND, 0);
			fechaSeguimiento.set(GregorianCalendar.MILLISECOND, 0);
			query.setParameter("fechaActual",fechaSeguimiento.getTime());
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
						
			return query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return polizaList;
	}
	
	
	public MensajeDTO validaVehiculoSO(BigDecimal idToPoliza, BigDecimal numeroInciso) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_SEGUROOBLIGATORIO.spAut_ValidaVehiculo";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pNumeroInciso", numeroInciso);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return mensajeDTO;
	}
	
	/*
	 * Valida si poliza Anexa tiene algun pago
	 */
	public MensajeDTO validaPagoSO(BigDecimal idToPoliza) {
		MensajeDTO mensajeDTO = null;
		try {
			//Obtiene idToPoliza Voluntaria
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id.idToPolizaAnexa", idToPoliza);
			params.put("origen", PolizaAnexa.ORIGEN_MIDAS);
			List<PolizaAnexa> polizaList = entidadService.findByProperties(PolizaAnexa.class, params);			
			//List<PolizaAnexa> polizaList = entidadService.findByProperty(PolizaAnexa.class, "id.idToPolizaAnexa", idToPoliza);
			
			PolizaAnexa poliza = polizaList.get(0);
			mensajeDTO = validaPagoBaseSO(poliza.getId().getIdToPoliza());
		} catch (Exception e) {
			throw new RuntimeException("Error en : MIDAS.PKGAUT_SEGUROOBLIGATORIO.spAut_ValidaPagoSO", e);
		}
		return mensajeDTO;
	}
	
	/*
	 * Valida si poliza Anexa es Valida por Poliza Base 
	 */
	public MensajeDTO validaPagoBaseSO(BigDecimal idToPoliza) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_SEGUROOBLIGATORIO.spAut_ValidaPagoSO";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {			
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return mensajeDTO;
	}
	
	public void saveCoberturaCotizacion(CoberturaCotizacionDTO cobertura){
		entidadService.save(cobertura);
	}
	
	/*
	 * 	Obtiene derechos 0 para la poliza obligatoria
	 */
	public NegocioDerechoPoliza obtenerDerechosPolizaCero(Long idToNegocio) {
		List<NegocioDerechoPoliza> negocioDerechoPolizas = new LinkedList<NegocioDerechoPoliza>();
		Map<String, Object> params = new HashMap<String, Object>();
		Double cero = 0.0;
		params.put("negocio.idToNegocio", idToNegocio);
		params.put("importeDerecho", cero);
		
		try{
			negocioDerechoPolizas =	entidadService.findByProperties(NegocioDerechoPoliza.class, params);
			if(negocioDerechoPolizas != null && !negocioDerechoPolizas.isEmpty()){
				return negocioDerechoPolizas.get(0);
			}else{
				//Si no existe lo crea
				Negocio negocio = entidadService.findById(Negocio.class, idToNegocio);
				
				NegocioDerechoPoliza negocioDerechoPoliza = new NegocioDerechoPoliza();
				negocioDerechoPoliza.setActivo(false);
				negocioDerechoPoliza.setClaveDefault(false);
				negocioDerechoPoliza.setImporteDerecho(cero);
				negocioDerechoPoliza.setNegocio(negocio);
				
				return entidadService.save(negocioDerechoPoliza);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public PolizaDTO obtienePolizaAnexa(BigDecimal idToPoliza){
		PolizaDTO polizaSO = null;
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id.idToPoliza", idToPoliza);
		params.put("origen", PolizaAnexa.ORIGEN_MIDAS);
		List<PolizaAnexa> polizaList = entidadService.findByProperties(PolizaAnexa.class, params);								
		//List<PolizaAnexa> polizaList = entidadService.findByProperty(PolizaAnexa.class, "id.idToPoliza", idToPoliza);
		if(polizaList != null && polizaList.size() > 0){
			PolizaAnexa polizaAnexa = polizaList.get(0);
			polizaSO = polizaAnexa.getPolizaAnexaDTO();
		}
		
		return polizaSO;
	}
	
	public List<BigDecimal> obtieneIncisosCrearSO(BigDecimal idToPoliza, Short numeroEndoso){
		List<BigDecimal> numeroIncisoList = new ArrayList<BigDecimal>(1);
		
		try{
			String queryString = "Select DISTINTC model.id.numeroInciso FROM CoberturaEndosoDTO model  " +
					" WHERE model.id.idToPoliza = :idToPoliza " +
					" AND model.id.numeroEndoso = :numeroEndoso " +
					" ";
			
			Query query = entityManager.createQuery(queryString.toString(),BigDecimal.class);			

			query.setParameter("idToPoliza", idToPoliza);
			query.setParameter("numeroEndoso", numeroEndoso);
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
						
			return query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return numeroIncisoList;
	}
	
	public BitemporalCotizacion obtieneBitemporalCotizacionByIdCotizacion(BigDecimal idToCotizacion, DateTime validOn){
		return entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, idToCotizacion, validOn);
	}
	
	public List<EntidadBitemporal> obtenerIncisoCotinuityList(Long continuityId, DateTime validOn){
		return (List<EntidadBitemporal>) entidadBitemporalService.findByParentKey(IncisoContinuity.class, IncisoContinuity.PARENT_KEY_NAME, continuityId, validOn);
	}
	
	public List<EntidadBitemporal> obtenerAutoIncisoCotinuityList(Long continuityId, DateTime validOn){
		return (List<EntidadBitemporal>) entidadBitemporalService.findByParentKey(AutoIncisoContinuity.class, AutoIncisoContinuity.PARENT_KEY_NAME, continuityId, validOn);
	}
	
	public List<EntidadBitemporal> obtenerSeccionIncisoCotinuityList(Long continuityId, DateTime validOn){
		return (List<EntidadBitemporal>) entidadBitemporalService.findByParentKey(SeccionIncisoContinuity.class, SeccionIncisoContinuity.PARENT_KEY_NAME, continuityId, validOn);
	}
	
	public List<EntidadBitemporal> obtenerCoberturasCotinuityList(Long continuityId, DateTime validOn){
		return (List<EntidadBitemporal>) entidadBitemporalService.findByParentKey(CoberturaSeccionContinuity.class, CoberturaSeccionContinuity.PARENT_KEY_NAME, continuityId, validOn);
	}
	
	public List<EntidadBitemporal> obtenerDatoSeccionContinuityList(Long continuityId, DateTime validOn){
		return (List<EntidadBitemporal>) entidadBitemporalService.findByParentKey(DatoSeccionContinuity.class, DatoSeccionContinuity.PARENT_KEY_NAME, continuityId, validOn);
	}
	
	public Double getSumaAseguradaSeguroObligatorio(Long negocioPaqueteId){
		Double valorSumaAsegurada = 0.0;
		
		try{
			List<NegocioCobPaqSeccion> list = entidadService.findByProperty(NegocioCobPaqSeccion.class, "negocioPaqueteSeccion.idToNegPaqueteSeccion", negocioPaqueteId);
			NegocioCobPaqSeccion negocioCobPaqSeccion = list.get(0);
			valorSumaAsegurada = negocioCobPaqSeccion.getValorSumaAseguradaDefault();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return valorSumaAsegurada;
	}
	
	/*
	 * Copia Endoso de Alta para Seguro Obligatorio 
	 */
	public MensajeDTO emiteEndosoAltaSO(BigDecimal idToPoliza, Short numeroEndoso) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_SEGUROOBLIGATORIO.spAut_EndosoAltaSO";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {			
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pNumeroEndoso", numeroEndoso);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return mensajeDTO;
	}
	
	/*
	 * Copia Endoso de Cambio de Datos para Seguro Obligatorio 
	 */
	public MensajeDTO emiteEndosoDatosSO(BigDecimal idToPoliza, Short numeroEndoso) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_SEGUROOBLIGATORIO.spAut_EndosoDatosSO";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {			
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pNumeroEndoso", numeroEndoso);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return mensajeDTO;
	}
	
	public Date obtieneFechaValidez(){
		Date fechaValidez = new Date();
		try{
			String queryString = "Select model " +
					" FROM CatalogoValorFijoDTO model  " +
					" WHERE model.id.idGrupoValores = :idGrupoValores " +
					" AND model.id.idDato = :idDato " +
					" ";
			
			Query query = entityManager.createQuery(queryString.toString(), CatalogoValorFijoDTO.class);			

			query.setParameter("idGrupoValores", CatalogoValorFijoDTO.IDGRUPO_FECHA_SEGURO_OBLIGATORIO);
			query.setParameter("idDato", 1);
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			CatalogoValorFijoDTO valor = (CatalogoValorFijoDTO) query.getSingleResult();
			
	    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    	
	    	try {
	    		fechaValidez = sdf.parse(valor.getDescripcion().trim());
			} catch (ParseException e) {
				LogDeMidasEJB3.log("Fallo al formatear la fecha " + valor.getDescripcion() + " con el formato " 
						+ SistemaPersistencia.FORMATO_FECHA + ". Se retornara la fecha actual.", Level.WARNING, null);
			}
						
			return fechaValidez;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return fechaValidez;
	}
	
	
	public Boolean estaHabilitado(BigDecimal tipo){
		try{
			//Se usa entityManager para poder hacer refresh de persistencia sin necesidad de flush cache
			String queryString = "Select model " +
					" FROM ParametroSeguroObligatorio model  " +
					" WHERE model.id = :tipo " +
					" ";
			
			Query query = entityManager.createQuery(queryString.toString(), ParametroSeguroObligatorio.class);			
			query.setParameter("tipo", tipo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				
			ParametroSeguroObligatorio paramso = (ParametroSeguroObligatorio) query.getSingleResult();
							
			return paramso.getHabilitado();
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
}
