package mx.com.afirme.midas2.service.impl.catalogos.cobertura;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.catalogos.cobertura.CoberturaDTODao;
import mx.com.afirme.midas2.service.catalogos.cobertura.CoberturaDTOService;

@Stateless
public class CoberturaDTOServiceImpl implements CoberturaDTOService{
	
	private CoberturaDTODao coberturaDTODao;
	
	@EJB
	public void setCoberturaDTODao(CoberturaDTODao coberturaDTODao) {
		this.coberturaDTODao = coberturaDTODao;
	}


	@Override
	public List<CoberturaDTO> getListarFiltrado(CoberturaDTO coberturaDTO,Boolean mostrarInactivos){
		return coberturaDTODao.getListarFiltrado(coberturaDTO,mostrarInactivos);
	}
	
	@Override
	public List<CoberturaDTO> listarVigentesAutos(){
		return coberturaDTODao.listarVigentesAutos();
	}
	
}
