package mx.com.afirme.midas2.service.impl.juridico;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.juridico.JuridicoDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.juridico.CatalogoJuridico;
import mx.com.afirme.midas2.domain.juridico.OficinaJuridico;
import mx.com.afirme.midas2.domain.juridico.ReclamacionJuridico;
import mx.com.afirme.midas2.domain.juridico.ReclamacionOficioIdJuridico;
import mx.com.afirme.midas2.domain.juridico.ReclamacionOficioJuridico;
import mx.com.afirme.midas2.domain.juridico.ReclamacionReservaIdJuridico;
import mx.com.afirme.midas2.domain.juridico.ReclamacionReservaJuridico;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.dto.juridico.ReclamacionJuridicoDTO;
import mx.com.afirme.midas2.dto.juridico.ReclamacionOficioJuridicoDTO;
import mx.com.afirme.midas2.dto.juridico.ReclamacionReservaJuridicoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.juridico.JuridicoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ValuacionReporteService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.JpaUtil;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.commons.lang.StringUtils;

@Stateless
public class JuridicoServiceImpl implements JuridicoService{
	
	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private JuridicoDao juridicoDao;
	
	@EJB
	private CatalogoSiniestroService catalogoSiniestroService;
	
	@EJB
	private ListadoService listadoService;
	
	@EJB
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@EJB
	private MailService mailService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	private static final int INVALIDO = -1;
	
	@Override
	public void bloquearAsegurado(String numeroPoliza, String asegurado) {
		String fecha = new SimpleDateFormat("EEEE dd 'de' MMMM 'de' yyyy '-' HH:mm:ss",new Locale("es")).format(new Date());
		String numeroPolizaCorreo = (StringUtil.isEmpty(numeroPoliza))? "" : numeroPoliza;
		String aseguradoCorreo = (StringUtil.isEmpty(asegurado))? "" : asegurado;
		String usuarioCorreo = (StringUtil.isEmpty(usuarioService.getUsuarioActual().getNombreUsuario()))? "" : 
			usuarioService.getUsuarioActual().getNombreUsuario();
			
		enviarNotificacionBloqueoAsegurado(fecha, numeroPolizaCorreo, aseguradoCorreo, usuarioCorreo);
	}

	@Override
	public void notificacionReserva(Long numeroReclamacion, Integer numeroOficio, Long idSiniestro, String numeroPoliza, String asegurado){
		String fecha = new SimpleDateFormat("EEEE dd 'de' MMMM 'de' yyyy '-' HH:mm:ss",new Locale("es")).format(new Date());
		String numeroPolizaCorreo = (StringUtil.isEmpty(numeroPoliza))? "" : numeroPoliza;
		String aseguradoCorreo = (StringUtil.isEmpty(asegurado))? "" : asegurado;
		String coberturasCorreo = generarListaReservasParaNotificacion(numeroReclamacion, numeroOficio, idSiniestro);
		String usuarioCorreo = (StringUtil.isEmpty(usuarioService.getUsuarioActual().getNombreUsuario()))? "" : 
			usuarioService.getUsuarioActual().getNombreUsuario();
			
		enviarNotificacionReservaSiniestros(fecha, numeroPolizaCorreo, aseguradoCorreo, usuarioCorreo, coberturasCorreo);
		enviarNotificacionReservaContabilidad(fecha, numeroPolizaCorreo, aseguradoCorreo, usuarioCorreo, coberturasCorreo);
	}
	
	/**
	 * Envia un correo al Director Tecnico Autos para solicitar el bloqueo de un asegurado en la Reclamacion de Juridico
	 * @param fecha
	 * @param numeroPoliza
	 * @param asegurado
	 * @param usuario
	 */
	private void enviarNotificacionBloqueoAsegurado(String fecha, String numeroPoliza, String asegurado, String usuario){
		String keyMensajeNotificacion = "midas.juridico.reclamacion.correo.bloqueo.mensaje";
		List<String> direcciones = obtenerCorreosPorRol(sistemaContext.getRolDirectorTecnicoAutos());
		//test
//		direcciones.add("0ruben.diaz@afirme.com");
							
		mailService.sendMail(direcciones, Utilerias.getMensajeRecurso(
				SistemaPersistencia.ARCHIVO_RECURSOS, "midas.juridico.reclamacion.correo.bloqueo.titulo"), 
				Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, keyMensajeNotificacion,
						fecha,
						numeroPoliza,
						asegurado,
						usuario),
				null, 
				Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, "midas.juridico.reclamacion.correo.bloqueo.titulo")
						,
				Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, "midas.juridico.reclamacion.correo.bloqueo.saludo"));
	}
	
	/**
	 * Envia notificacion al Subdirector de Siniestros Autos para notificar el cambio de la reserva en las reclamaciones
	 * @param fecha
	 * @param numeroPoliza
	 * @param asegurado
	 * @param usuario
	 * @param coberturas
	 */
	private void enviarNotificacionReservaSiniestros(String fecha, String numeroPoliza, String asegurado, String usuario, String coberturas){
		String keyMensajeNotificacion = "midas.juridico.reclamacion.correo.reserva.mensaje";
		List<String> direcciones = obtenerCorreosPorRol(sistemaContext.getRolSubdirectorSiniestrosAuto());
		//test
//		direcciones.add("0ruben.diaz@afirme.com");
							
		mailService.sendMail(direcciones, Utilerias.getMensajeRecurso(
				SistemaPersistencia.ARCHIVO_RECURSOS, "midas.juridico.reclamacion.correo.reserva.titulo"), 
				Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, keyMensajeNotificacion,
						fecha,
						numeroPoliza,
						asegurado,
						coberturas,
						usuario),
				null, 
				Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, "midas.juridico.reclamacion.correo.reserva.titulo")
						,
				Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, "midas.juridico.reclamacion.correo.reserva.saludo.siniestros"));			
	}
	
	/**
	 * Notifica al Gerente de Contabilidad los cambios en las reservas en las reclamaciones de juridico
	 * @param fecha
	 * @param numeroPoliza
	 * @param asegurado
	 * @param usuario
	 * @param coberturas
	 */
	private void enviarNotificacionReservaContabilidad(String fecha, String numeroPoliza, String asegurado, String usuario, String coberturas){
		String keyMensajeNotificacion = "midas.juridico.reclamacion.correo.reserva.mensaje";
		List<String> direcciones = obtenerCorreosPorRol(sistemaContext.getRolGerenteContabilidad());
		//test
//		direcciones.add("0ruben.diaz@afirme.com");
							
		mailService.sendMail(direcciones, Utilerias.getMensajeRecurso(
				SistemaPersistencia.ARCHIVO_RECURSOS, "midas.juridico.reclamacion.correo.reserva.titulo"), 
				Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, keyMensajeNotificacion,
						fecha,
						numeroPoliza,
						asegurado,
						coberturas,
						usuario),
				null, 
				Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, "midas.juridico.reclamacion.correo.reserva.titulo")
						,
				Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, "midas.juridico.reclamacion.correo.reserva.saludo.contabilidad"));			
	}
	
	/**
	 * obtiene la lista de correos electronicos de los usuarios que tienen un rol determinado
	 * @param rol
	 * @return
	 */
	private List<String> obtenerCorreosPorRol(String rol) {
		List<String> direcciones = new ArrayList<String>();
		
		Usuario user = usuarioService.getUsuarioActual();
		String idSesionUsuario = user.getIdSesionUsuario();
		List<Usuario> usuarios = 
			usuarioService.buscarUsuariosSinRolesPorNombreRol(
					rol, 
					ValuacionReporteService.NOMBRE_USUARIO_BUSQUEDA, 
					idSesionUsuario);
		
		for(Usuario usuario: usuarios){
			direcciones.add(usuario.getEmail());
		}
		
		return direcciones;
	}
	
	/**
	 * Genera la lista de reservas para la notificacion de reserva que se va a enviar en el correo para siniestros y contabilidad
	 * @param numeroReclamacion
	 * @param numeroOficio
	 * @param idSiniestro
	 * @return
	 */
	private String generarListaReservasParaNotificacion(Long numeroReclamacion, Integer numeroOficio, Long idSiniestro){
		StringBuilder listaReservasNotificacion = new StringBuilder();
		List<ReclamacionReservaJuridicoDTO> reservas = obtenerReservasOficio(numeroReclamacion, numeroOficio, idSiniestro);
		
		for(ReclamacionReservaJuridicoDTO reserva : reservas){
			listaReservasNotificacion.append("<li>");
			listaReservasNotificacion.append(reserva.getNombreCobertura());
			listaReservasNotificacion.append(" - ");
			listaReservasNotificacion.append(reserva.getReservaMontoAutoridad().toString());
			listaReservasNotificacion.append("</li>");
		}
		
		return listaReservasNotificacion.toString();
	}

	@Override
	public List<ReclamacionOficioJuridicoDTO> obtenerOficios(Long numeroReclamacion) {
		List<ReclamacionOficioJuridicoDTO> oficiosDTO = new ArrayList<ReclamacionOficioJuridicoDTO>();
		
		if(numeroReclamacion != null){
			ReclamacionJuridicoFiltro reclamacionFiltro = new ReclamacionJuridicoFiltro();
			reclamacionFiltro.setNumeroReclamacion(numeroReclamacion);
			List<ReclamacionOficioJuridico> oficios = catalogoSiniestroService.buscar(ReclamacionOficioJuridico.class, reclamacionFiltro, new String("id.oficio").concat("-").concat(JpaUtil.DESC));
			
			oficiosDTO = crearListaOficios(oficios);
		}
		
		return oficiosDTO;
	}
	
	private List<ReclamacionOficioJuridicoDTO> crearListaOficios(List<ReclamacionOficioJuridico> oficios){
		List<ReclamacionOficioJuridicoDTO> oficiosDTO = new ArrayList<ReclamacionOficioJuridicoDTO>();
		Map<String,String> tiposReclamacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RECLAMACION_JURIDICO);
		Map<String,String> ramos = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RAMO_JURIDICO);
		Map<String,String> etapasJuicio = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ETAPA_JUICIO_JURIDICO);
		Map<String,String> estatuses = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_RECLAMACION_JURIDICO);
		Map<String,String> clasificaciones = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CLASIFICACION_RECLAMACION_JURIDICO);
		Map<String,String> tiposQueja = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_QUEJA_JURIDICO);
		
		for(ReclamacionOficioJuridico oficio: oficios){
			ReclamacionOficioJuridicoDTO oficioDTO = crearOficioDTO(oficio, tiposReclamacion, ramos, etapasJuicio, 
					estatuses, clasificaciones, tiposQueja, oficios.size());
			oficiosDTO.add(oficioDTO);
		}
		
		return oficiosDTO;
	}
	
	/**
	 * Carga la informacion de la ReclamacionOficioJuridico a un DTO para la presentacion de la informacion
	 * @param oficio
	 * @return
	 */
	private ReclamacionOficioJuridicoDTO crearOficioDTO(ReclamacionOficioJuridico oficio, Map<String,String> tiposReclamacion, 
			Map<String,String> ramos, Map<String,String> etapasJuicio, 
			Map<String,String> estatuses, 
			Map<String,String> clasificaciones, Map<String,String> tiposQueja, Integer numeroOficios){
		ReclamacionOficioJuridicoDTO oficioDTO = new ReclamacionOficioJuridicoDTO();
		
		oficioDTO.setNumeroReclamacion(oficio.getId().getReclamacionJuridicoId());
		oficioDTO.setTipoReclamacion(oficio.getReclamacionJuridico().getTipoReclamacion());
		oficioDTO.setTipoReclamacionDesc(tiposReclamacion.get(oficio.getReclamacionJuridico().getTipoReclamacion()));
		oficioDTO.setNumeroOficio(oficio.getId().getOficio());
		oficioDTO.setFechaNotificacion(oficio.getReclamacionJuridico().getFechaNotificacion());
		oficioDTO.setAsegurado(oficio.getAsegurado());
		oficioDTO.setRamo(oficio.getReclamacionJuridico().getRamo());
		oficioDTO.setRamoDesc(ramos.get(oficio.getReclamacionJuridico().getRamo()));
		oficioDTO.setNumeroPoliza(oficio.getReclamacionJuridico().getNumeroPoliza());
		oficioDTO.setSiniestroId((oficio.getReclamacionJuridico().getSiniestro() != null)? oficio.getReclamacionJuridico().getSiniestro().getId() : null);
		oficioDTO.setNumeroSiniestro((oficio.getReclamacionJuridico().getSiniestro() != null)? oficio.getReclamacionJuridico().getSiniestro().getNumeroSiniestro() : oficio.getReclamacionJuridico().getNumeroSiniestro());
		oficioDTO.setOficinaSiniestro((oficio.getReclamacionJuridico().getOficinaSiniestro() != null)? oficio.getReclamacionJuridico().getOficinaSiniestro().getId().toString() : null);
		oficioDTO.setOficinaSiniestroDesc((oficio.getReclamacionJuridico().getOficinaSiniestro() != null)? oficio.getReclamacionJuridico().getOficinaSiniestro().getNombreOficina() : null);
		oficioDTO.setMontoReclamado(oficio.getMontoReclamado());
		oficioDTO.setAsignado(oficio.getAsignado());
		oficioDTO.setProcedimiento((oficio.getProcedimiento() != null)? oficio.getProcedimiento().getId() : null);
		oficioDTO.setProcedimientoDesc((oficio.getProcedimiento() != null) ? oficio.getProcedimiento().getNombre() : null);
		oficioDTO.setExpedienteJuridico(oficio.getExpediente());
		oficioDTO.setJustificacion(oficio.getDescripcion());
		oficioDTO.setOficinaJuridica((oficio.getReclamacionJuridico().getOficinaJuridico() != null)? oficio.getReclamacionJuridico().getOficinaJuridico().getId().toString() : null);
		oficioDTO.setOficinaJuridicaDesc((oficio.getReclamacionJuridico().getOficinaJuridico() != null)? oficio.getReclamacionJuridico().getOficinaJuridico().getNombreOficina() : null);
		oficioDTO.setEstado((oficio.getEstado() != null)? oficio.getEstado().getId() : null);
		oficioDTO.setEstadoDesc((oficio.getEstado() != null)? oficio.getEstado().getDescripcion() : null);
		oficioDTO.setMunicipio((oficio.getCiudad() != null)? oficio.getCiudad().getId() : null);
		oficioDTO.setMunicipioDesc((oficio.getCiudad() != null)? oficio.getCiudad().getDescripcion() : null);
		oficioDTO.setDelegacion((oficio.getDelegacion() != null)? oficio.getDelegacion().getId() : null);
		oficioDTO.setDelegacionDesc((oficio.getDelegacion() != null)? oficio.getDelegacion().getNombre() : null);
		oficioDTO.setMotivoReclamacion((oficio.getMotivoReclamacion() != null)? oficio.getMotivoReclamacion().getId(): null);
		oficioDTO.setMotivoReclamacionDesc((oficio.getMotivoReclamacion() != null)? oficio.getMotivoReclamacion().getNombre(): null);
		oficioDTO.setEstatus(oficio.getEstatus());
		oficioDTO.setEstatusDesc(estatuses.get(oficioDTO.getEstatus()));
		oficioDTO.setEsUltimoOficio((oficio.getId().getOficio() == numeroOficios)? true : false);
		oficioDTO.setReclamante(oficio.getReclamante());
		if(oficioDTO.getTipoReclamacion().compareTo(ReclamacionJuridico.TIPOS_RECLAMACION.GESTION_ELECTRONICA.codigo) == 0){
			oficioDTO.setClasificacionReclamacion(oficio.getClasificacionReclamacion());
			oficioDTO.setClasificacionReclamacionDesc(clasificaciones.get(oficioDTO.getClasificacionReclamacion()));
			oficioDTO.setTipoQueja(oficio.getTipoQueja());
			oficioDTO.setTipoQuejaDesc(oficioDTO.getTipoQueja());
		}
		if(oficioDTO.getTipoReclamacion().compareTo(ReclamacionJuridico.TIPOS_RECLAMACION.LITIGIOS.codigo) == 0){
			oficioDTO.setActor(oficio.getActor());
			oficioDTO.setDemandado(oficio.getDemandado());
			oficioDTO.setEtapaJuicio(oficio.getEtapaJuicio());
			oficioDTO.setEtapaJuicioDesc(etapasJuicio.get(oficio.getEtapaJuicio()));
			oficioDTO.setProbabilidadExito(oficio.getProbabilidadExito());
			oficioDTO.setTotalOficios(oficio.getReclamacionJuridico().getOficios().size());
			oficioDTO.setParteEnJuicio(oficio.getParteEnJuicio());
			oficioDTO.setUbicacion(((oficio.getEstado() != null)? oficio.getEstado().getDescripcion() + ", " : "" ) + ((oficio.getCiudad() != null)? oficio.getCiudad().getDescripcion() : ""));
		}
		
		return oficioDTO;
	}

	/**
	 * Recupera el siguiente numeroOficio para la reclamacion dada y le resta uno para obtener el ultimo que se inserto.
	 * nunca es 0, por que la primera reclamacion se crea junto con el primer oficio. 
	 * @param numeroReclamacion
	 * @return
	 */
	private Integer obtenerIdUltimoOficio(Long numeroReclamacion) {
		Integer idUltimoOficio = juridicoDao.obtenerSecuenciaOficioJuridico(numeroReclamacion) - 1;
		return idUltimoOficio;
	}
	
	@Override
	public ReclamacionOficioJuridicoDTO obtenerOficio(Long numeroReclamacion, Integer numeroOficio) {
		if(numeroOficio == null){
			numeroOficio = obtenerIdUltimoOficio(numeroReclamacion);
		}
		
		ReclamacionJuridicoFiltro reclamacionFiltro = new ReclamacionJuridicoFiltro();
		reclamacionFiltro.setNumeroReclamacion(numeroReclamacion);
		reclamacionFiltro.setNumeroOficio(numeroOficio);
		List<ReclamacionOficioJuridico> oficios = catalogoSiniestroService.buscar(ReclamacionOficioJuridico.class, reclamacionFiltro, new String("id.oficio").concat("-").concat(JpaUtil.DESC));
		ReclamacionOficioJuridicoDTO oficioDTO = crearListaOficios(oficios).get(0);
		return oficioDTO;
	}

	@Override
	public List<ReclamacionJuridicoDTO> obtenerReclamaciones(
			ReclamacionJuridicoFiltro reclamacionFiltro) {
		List<ReclamacionOficioJuridico> oficios = catalogoSiniestroService.buscar(ReclamacionOficioJuridico.class, reclamacionFiltro, new String("id.reclamacionJuridicoId DESC, e.id.oficio").concat("-").concat(JpaUtil.DESC));
		List<ReclamacionJuridicoDTO> reclamaciones = this.crearListaReclamaciones(oficios);
		return reclamaciones;
	}
	
	/**
	 * Crea una lista de reclamaciones a partir de los oficios que se encontraron en la busqueda 
	 * @param oficios
	 * @return
	 */
	private List<ReclamacionJuridicoDTO> crearListaReclamaciones(List<ReclamacionOficioJuridico> oficios){
		List<ReclamacionJuridicoDTO> reclamaciones = new ArrayList<ReclamacionJuridicoDTO>();
		Map<String,String> tiposReclamacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RECLAMACION_JURIDICO);
		Map<String,String> ramos = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RAMO_JURIDICO);
		Map<String,String> estatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_RECLAMACION_JURIDICO);
		
		for(ReclamacionOficioJuridico oficio: oficios){
			ReclamacionJuridicoDTO reclamacion = new ReclamacionJuridicoDTO();
			
			reclamacion.setNumeroReclamacion(oficio.getReclamacionJuridico().getId());
			reclamacion.setTipoReclamacion(tiposReclamacion.get(oficio.getReclamacionJuridico().getTipoReclamacion()));
			reclamacion.setTotalOficios(oficio.getReclamacionJuridico().getOficios().size());
			reclamacion.setOficinaJuridica((oficio.getReclamacionJuridico().getOficinaJuridico() != null)? oficio.getReclamacionJuridico().getOficinaJuridico().getNombreOficina() : null);
			reclamacion.setNumeroPoliza(oficio.getReclamacionJuridico().getNumeroPoliza());
			reclamacion.setAsegurado(oficio.getAsegurado());
			reclamacion.setNumeroSiniestro((oficio.getReclamacionJuridico().getSiniestro() != null)? oficio.getReclamacionJuridico().getSiniestro().getNumeroSiniestro() : null);
			reclamacion.setOficinaSiniestro((oficio.getReclamacionJuridico().getOficinaSiniestro() != null)? oficio.getReclamacionJuridico().getOficinaSiniestro().getNombreOficina() : null);
			reclamacion.setExpedienteJuridico(oficio.getExpediente());
			reclamacion.setFechaNotificacion(oficio.getReclamacionJuridico().getFechaNotificacion());
			reclamacion.setDemandado(oficio.getDemandado());
			reclamacion.setReclamante(oficio.getReclamante());
			reclamacion.setAsignado(oficio.getAsignado());
			reclamacion.setProcedimiento((oficio.getProcedimiento() != null)? oficio.getProcedimiento().getNombre() : null);
			reclamacion.setRamo(ramos.get(oficio.getReclamacionJuridico().getRamo()));
			reclamacion.setEstado((oficio.getEstado() != null)? oficio.getEstado().getDescripcion() : null);
			reclamacion.setEstatus(estatus.get(oficio.getEstatus()));
			reclamacion.setEstatusFinal(oficio.getReclamacionJuridico().getEstatus());
			
			reclamaciones.add(reclamacion);
		}
		
		return reclamaciones;
	}

	@Override
	public List<ReclamacionReservaJuridicoDTO> obtenerReservasOficio(Long numeroReclamacion, Integer numeroOficio, Long idSiniestro){
		List<ReclamacionReservaJuridicoDTO> reservas = new ArrayList<ReclamacionReservaJuridicoDTO>();
		SiniestroCabina siniestro = null;
		ReclamacionOficioJuridico oficio = null;
		
										//BUSCANDO EL SINIESTRO
		if(idSiniestro != null){		//			CARGANDO EL SINIESTRO EN CASO DE QUE EL numeroReclamacion Y numeroOFICIO VENGAN null
			siniestro = entidadService.findById(SiniestroCabina.class, idSiniestro);
		}
		
										//BUSCANDO EL OFICIO
		oficio = obtenerReclamacionOficioJuridico(numeroReclamacion, numeroOficio);
		if(oficio != null 
				&& siniestro == null){	//			CARGANDO EL SINIESTRO EN CASO DE QUE idSiniestro VENGA null
			siniestro = oficio.getReclamacionJuridico().getSiniestro();
		}

		
//		CARGAR LA INFORMACION DEL OFICIO Y DEL SINIESTRO
		if(siniestro != null){
			reservas = estimacionCoberturaSiniestroService.obtenerReservaCoberturaJuridico(siniestro.getReporteCabina().getId());
			if(oficio != null){
				if(oficio.getReservas() != null 
						&& !oficio.getReservas().isEmpty()){
					for(ReclamacionReservaJuridicoDTO reservaDTO : reservas){
						for(ReclamacionReservaJuridico reservaOficio : oficio.getReservas()){
							if(reservaOficio.getId().getClaveSubCobertura().compareTo(reservaDTO.getClaveSubCobertura()) == 0){
								reservaDTO.setReclamacionId(numeroReclamacion);
								reservaDTO.setOficio(numeroOficio);
								reservaDTO.setReservaMontoAutoridad(reservaOficio.getMontoReservadoAutoridad());
								break;
							}
						}
					}
				}else{
					for(ReclamacionReservaJuridicoDTO reserva : reservas){
						reserva.setReservaMontoAutoridad(BigDecimal.ZERO);
					}
				}
			}else{
				for(ReclamacionReservaJuridicoDTO reserva : reservas){
					reserva.setReservaMontoAutoridad(BigDecimal.ZERO);
				}
			}
		}
					
		return reservas;
	}
	
	/**
	 * Obtiene el oficio con el numeroReclamacion y numeroOficio correspondientes. Los dos datos son necesarios para que se ejecute.
	 * @param numeroReclamacion
	 * @param numeroOficio
	 * @return
	 */
	private ReclamacionOficioJuridico obtenerReclamacionOficioJuridico(Long numeroReclamacion, Integer numeroOficio){
		ReclamacionOficioJuridico oficio = null;
		
		if(numeroReclamacion != null 
				&& numeroOficio != null){
			ReclamacionJuridicoFiltro reclamacionFiltro = new ReclamacionJuridicoFiltro();
			reclamacionFiltro.setNumeroReclamacion(numeroReclamacion);
			reclamacionFiltro.setNumeroOficio(numeroOficio);
			List<ReclamacionOficioJuridico> oficios = catalogoSiniestroService.buscar(ReclamacionOficioJuridico.class, reclamacionFiltro, new String("id.oficio").concat("-").concat(JpaUtil.DESC));
			if(oficios != null 
					&& !oficios.isEmpty()){
				oficio = oficios.get(0);
			}
		}
		
		return oficio;
	}

	@Override
	public ReclamacionOficioJuridicoDTO salvarReclamacion(ReclamacionOficioJuridicoDTO reclamacionCapturada, List<ReclamacionReservaJuridicoDTO> reservasCapturadas){
		ReclamacionJuridico reclamacion = null;
		ReclamacionOficioJuridico oficio = null;
		ReclamacionOficioJuridicoDTO oficioDTO = null;
		Map<String,String> tiposReclamacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RECLAMACION_JURIDICO);
		Map<String,String> ramos = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RAMO_JURIDICO);
		Map<String,String> etapasJuicio = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ETAPA_JUICIO_JURIDICO);
		Map<String,String> estatuses = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_RECLAMACION_JURIDICO);
		Map<String,String> clasificaciones = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CLASIFICACION_RECLAMACION_JURIDICO);
		Map<String,String> tiposQueja = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_QUEJA_JURIDICO);
		
		if(reclamacionCapturada != null){
			reclamacion = buscarCrearReclamacionJuridico(reclamacionCapturada.getNumeroReclamacion());
			this.actualizarReclamacionJuridico(reclamacion, reclamacionCapturada);
			
			oficio = this.crearNuevoOficio(reclamacion, reclamacionCapturada);
			oficio = entidadService.save(oficio);
			reclamacion.getOficios().add(oficio);
			
			this.guardarReservasOficio(oficio.getId().getReclamacionJuridicoId(), oficio.getId().getOficio(), reservasCapturadas);
			
			oficioDTO = crearOficioDTO(oficio, tiposReclamacion, ramos, etapasJuicio, estatuses, clasificaciones, tiposQueja, 1);
			return oficioDTO;
		}else{
			return oficioDTO;
		}
	}
	
	/**
	 * Metodo que si se le pasa un numero de reclamacion, la recupera de la BD, y si se le pasa null
	 * crea una nueva reclamacion
	 * @param numeroReclamacion
	 * @return
	 */
	private ReclamacionJuridico buscarCrearReclamacionJuridico(Long numeroReclamacion){
		ReclamacionJuridico reclamacion = null;
		
		if(numeroReclamacion == null){
			reclamacion = new ReclamacionJuridico();
			reclamacion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
			reclamacion.setFechaCreacion(new Date());
		}else{
			reclamacion = entidadService.findById(ReclamacionJuridico.class, numeroReclamacion);
		}
		return reclamacion;
	}
	
	/**
	 * Actualiza la reclamacion con la informacion capturada en el nuevo oficio
	 * @param reclamacion
	 * @param informacionCapturada
	 */
	private void actualizarReclamacionJuridico(ReclamacionJuridico reclamacion, ReclamacionOficioJuridicoDTO informacionCapturada){
		reclamacion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		reclamacion.setFechaModificacion(new Date());
		
		reclamacion.setEstatus(informacionCapturada.getEstatus());
		reclamacion.setFechaNotificacion(informacionCapturada.getFechaNotificacion());
		reclamacion.setSiniestro((informacionCapturada.getSiniestroId() != null) ? entidadService.findById(SiniestroCabina.class, informacionCapturada.getSiniestroId()) : null);
		reclamacion.setOficinaJuridico((!StringUtil.isEmpty(informacionCapturada.getOficinaJuridica())) ? entidadService.findById(OficinaJuridico.class, Long.parseLong(informacionCapturada.getOficinaJuridica())) : null);
		reclamacion.setOficinaSiniestro((!StringUtil.isEmpty(informacionCapturada.getOficinaSiniestro())) ? entidadService.findById(Oficina.class, Long.parseLong(informacionCapturada.getOficinaSiniestro())) : null);
		reclamacion.setNumeroPoliza(informacionCapturada.getNumeroPoliza());
		reclamacion.setNumeroSiniestro(informacionCapturada.getNumeroSiniestro());
		reclamacion.setRamo(informacionCapturada.getRamo());
		reclamacion.setTipoReclamacion(informacionCapturada.getTipoReclamacion());
		
		reclamacion = entidadService.save(reclamacion);
	}
	
	/**
	 * Crea un nuevo oficio con la informacion capturada
	 * @param reclamacionCapturada
	 * @return
	 */
	private ReclamacionOficioJuridico crearNuevoOficio(ReclamacionJuridico reclamacion, ReclamacionOficioJuridicoDTO reclamacionCapturada){
		ReclamacionOficioJuridico reclamacionOficio = new ReclamacionOficioJuridico();
		
		ReclamacionOficioIdJuridico idOficio = new ReclamacionOficioIdJuridico(
				reclamacion.getId(), 
				juridicoDao.obtenerSecuenciaOficioJuridico(reclamacion.getId())	); 
		
		reclamacionOficio.setFechaCreacion(new Date());
		reclamacionOficio.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		reclamacionOficio.setFechaModificacion(new Date());
		reclamacionOficio.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		
		reclamacionOficio.setId(idOficio);
		reclamacionOficio.setActor(reclamacionCapturada.getActor());
		reclamacionOficio.setAsegurado(reclamacionCapturada.getAsegurado());
		reclamacionOficio.setAsignado(reclamacionCapturada.getAsignado());
		reclamacionOficio.setClasificacionReclamacion(reclamacionCapturada.getClasificacionReclamacion());
		reclamacionOficio.setCiudad((reclamacionCapturada.getMunicipio() != null) ? entidadService.findById(CiudadMidas.class, reclamacionCapturada.getMunicipio()) : null);
		reclamacionOficio.setDelegacion((reclamacionCapturada.getDelegacion() != null) ? entidadService.findById(CatalogoJuridico.class, reclamacionCapturada.getDelegacion()) : null);
		reclamacionOficio.setDemandado(reclamacionCapturada.getDemandado());
		reclamacionOficio.setDescripcion(reclamacionCapturada.getJustificacion());
		reclamacionOficio.setEstado((reclamacionCapturada.getEstado() != null) ? entidadService.findById(EstadoMidas.class, reclamacionCapturada.getEstado()) : null);
		reclamacionOficio.setEstatus(reclamacionCapturada.getEstatus());
		reclamacionOficio.setEtapaJuicio(reclamacionCapturada.getEtapaJuicio());
		reclamacionOficio.setExpediente(reclamacionCapturada.getExpedienteJuridico());
		reclamacionOficio.setMontoReclamado(reclamacionCapturada.getMontoReclamado());
		reclamacionOficio.setMotivoReclamacion((reclamacionCapturada.getMotivoReclamacion() != null) ? entidadService.findById(CatalogoJuridico.class, reclamacionCapturada.getMotivoReclamacion()) : null);
		reclamacionOficio.setParteEnJuicio(reclamacionCapturada.getParteEnJuicio());
		reclamacionOficio.setProbabilidadExito(reclamacionCapturada.getProbabilidadExito());
		reclamacionOficio.setProcedimiento((reclamacionCapturada.getProcedimiento() != null) ? entidadService.findById(CatalogoJuridico.class, reclamacionCapturada.getProcedimiento()) : null);
		reclamacionOficio.setReclamacionJuridico(reclamacion);
		reclamacionOficio.setReclamante(reclamacionCapturada.getReclamante());
		reclamacionOficio.setTipoQueja(reclamacionCapturada.getTipoQueja());
		
		return reclamacionOficio;
	}

	/**
	 * Guarda las reservas capturadas para un oficio en particular
	 * @param numeroReclamacion
	 * @param numeroOficio
	 * @param reservasCapturadas
	 */
	private void guardarReservasOficio(Long numeroReclamacion, Integer numeroOficio, List<ReclamacionReservaJuridicoDTO> reservasCapturadas){
		if(reservasCapturadas != null 
				&& !reservasCapturadas.isEmpty()){
			for(ReclamacionReservaJuridicoDTO reservaDTO : reservasCapturadas){
				ReclamacionReservaJuridico reserva = new ReclamacionReservaJuridico();
				ReclamacionReservaIdJuridico reservaId = new ReclamacionReservaIdJuridico();
				
				reservaId.setClaveSubCobertura(reservaDTO.getClaveSubCobertura());
				reservaId.setCoberturaId(reservaDTO.getCoberturaId());
				reservaId.setOficio(numeroOficio);
				reservaId.setReclamacionJuridicoId(numeroReclamacion);
				
				reserva.setId(reservaId);
				reserva.setMontoReservadoAutoridad(reservaDTO.getReservaMontoAutoridad());
				reserva.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
				reserva.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
				reserva.setFechaCreacion(new Date());
				reserva.setFechaModificacion(new Date());
				
				entidadService.save(reserva);
			}
		}
	}
	
	@Override
	public Boolean validarPoliza(String numeroPoliza){
		Boolean existePoliza = false;
		List<PolizaDTO> resultado = null;
		
		String[] elementosNumPoliza = numeroPoliza.split("-");
		
		if(elementosNumPoliza.length ==3){
			String codigoProducto 		=  StringUtils.rightPad(elementosNumPoliza[0].substring(0,2),8);
			String codigoTipoPoliza 	= StringUtils.rightPad(elementosNumPoliza[0].substring(2),8);
			int numPoliza 				= (StringUtil.isNumeric(elementosNumPoliza[1]))? Integer.parseInt( elementosNumPoliza[1] ) : INVALIDO;
			int numeroRenovacion 		= (StringUtil.isNumeric(elementosNumPoliza[2]))? Integer.parseInt( elementosNumPoliza[2] ) : INVALIDO;
			
			if(numPoliza == INVALIDO || numeroRenovacion == INVALIDO){
				return existePoliza;
			}
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("codigoProducto", codigoProducto);
			params.put("codigoTipoPoliza", codigoTipoPoliza);
			params.put("numeroPoliza", numPoliza);
			params.put("numeroRenovacion", numeroRenovacion);
			resultado = entidadService.findByProperties(PolizaDTO.class, params);
		
			if(resultado != null 
					&& !resultado.isEmpty()){
				if(resultado.get(0).getFolioPoliza().compareToIgnoreCase(numeroPoliza) == 0){
					existePoliza = true;
				}
			}
		}
		
		return existePoliza;
	}
	
}