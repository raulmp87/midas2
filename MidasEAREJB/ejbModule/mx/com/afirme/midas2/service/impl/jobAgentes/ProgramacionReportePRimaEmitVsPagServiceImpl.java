package mx.com.afirme.midas2.service.impl.jobAgentes;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.domain.jobAgentes.TareaProgramada;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionReportePRimaEmitVsPagService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
@Stateless
public class ProgramacionReportePRimaEmitVsPagServiceImpl implements ProgramacionReportePRimaEmitVsPagService{
	
	@Resource	
	private TimerService timerService;	
	@EJB
	private SistemaContext sistemaContext;
	public static final Logger LOG = Logger.getLogger(ProgramacionReportePRimaEmitVsPagServiceImpl.class);

	@Override
	public void executeTasks() {
		getTaskToDo(null);
	}

	@Override
	public List<TareaProgramada> getTaskToDo(String conceptoEjecucionAutomatica) {
		LOG.info("Ejecutando ProgramacionReportePRimaEmitVsPagService.getTaskToDo()...");
		String sp="MIDAS.PKGREPORTES_AGENTES.stp_repPrimaPagadaVSEmitida";	
		StoredProcedureHelper storedHelper = null;
		try {
			Date fecha=new Date();
			storedHelper = new StoredProcedureHelper(sp, StoredProcedureHelper.DATASOURCE_MIDAS);
			SimpleDateFormat formatea = new SimpleDateFormat("dd/MM/yyyy");
			DateFormat anios = new SimpleDateFormat ("yyyy");
			DateFormat meses = new SimpleDateFormat ("MM");
			String fechaFin = formatea.format(fecha);
			String fechaInicio = "01/"+meses.format(fecha)+"/"+anios.format(fecha);
			storedHelper.estableceParametro("pFini", fechaInicio);
			storedHelper.estableceParametro("pFfinal", fechaFin);
		    storedHelper.ejecutaActualizar();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void initialize() {
		String timerInfo = "TimerProgramReportePrimaEmitidaVsPagada";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0 23 L * ?
				expression.minute(0);
				expression.hour(23);
				expression.dayOfMonth("Last");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerProgramReportePrimaEmitidaVsPagada", false));
				
				LOG.info("Tarea TimerProgramReportePrimaEmitidaVsPagada configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerProgramReportePrimaEmitidaVsPagada");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerProgramReportePrimaEmitidaVsPagada:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		executeTasks();
	}

}
