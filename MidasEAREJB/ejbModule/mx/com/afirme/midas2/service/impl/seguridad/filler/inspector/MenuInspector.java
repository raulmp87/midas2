package mx.com.afirme.midas2.service.impl.seguridad.filler.inspector;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

public class MenuInspector {
private List<Menu> listaMenu = null;
	
	public MenuInspector() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		Menu menu;
		menu = new Menu(new Integer("1"),"m1","Emision", "Menu ppal Emision", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m2","Siniestros", "Menu ppal Siniestos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m3","Reaseguro", "Menu ppal Reaseguro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("4"),"m1_2","Vida", "Submenu Vida", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m1_3","Da�os", "Submenu Da�os", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("6"),"m1_3_1","Solicitudes", "Submenu Solicitudes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("7"),"m1_3_2","Ordenes de Trabajo", "Submenu Ordenes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("8"),"m2_3","Da�os", "Da�os", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("9"),"m2_3_4","Cat�logos", "Siniestros � Catalogo", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("10"),"m2_3_4_1","Ajustador", "Siniestros � Catalogo � Ajustador", "/MidasWeb/catalogos/ajustador/listar.do|contenido|null", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("11"),"menu","", "Raiz del menu", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("12"),"m4","Producto", "Menu ppal Producto", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("13"),"m4_1","Productos", "Configuracion de Productos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("14"),"m4_2","Tarifas", "Configuracion de Tarifas", "/MidasWeb/sistema/configuracion/listar.do|contenido|cargandoTree('configuracionTarifa','treeboxbox_tree')", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("15"),"m4_3","Cat�logos", "Productos - Cat�logos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("16"),"m4_3_3","Da�os", "Productos - Cat�logos - Da�os", null, true);
		listaMenu.add(menu);		
		
		menu = new Menu(new Integer("17"),"m4_3_3_17","Productos", "Catalogo de Productos", "/MidasWeb/catalogos/producto/listar.do|contenido|null", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("18"),"m3_2_3","Administraci�n de Facultativo", "Administraci�n de Facultativo", "/MidasWeb/realizarContratoFacultativo.do|contenido|null", true);
		listaMenu.add(menu);
		
		
		/**
		 * Menus nuevos de autos
		 */
		
		menu = new Menu(new Integer("19"),"m4_3_1_1","Productos", "Productos", "/MidasWeb/catalogos/producto/listar.do?negocio=A|contenido|null", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("20"),"m4_3_1_2","Coberturas", "Coberturas", "/MidasWeb/catalogos/cobertura/listar.do|contenido|null", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("21"),"m4_3_1_3","Riesgos", "Riesgos", "/MidasWeb/catalogos/riesgo/listar.do|contenido|null", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("22"),"m4_1_1","Autos", "Autos", "/MidasWeb/sistema/configuracion/listar.do?negocio=A|contenido|cargandoTree('configuracionProducto','treeboxbox_tree')", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("23"),"m4_1_2","Da�os", "Da�os", "/MidasWeb/sistema/configuracion/listar.do?negocio=D|contenido|cargandoTree('configuracionProducto','treeboxbox_tree')", true);
		listaMenu.add(menu);
		
		
		return this.listaMenu;
	}
}
