package mx.com.afirme.midas2.service.impl.movil.cotizador;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.consultas.formapago.FormaPagoInterfazServiciosRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.dao.negocio.estadodescuento.NegocioEstadoDescuentoDao;
import mx.com.afirme.midas2.dao.negocio.producto.NegocioProductoDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionDao;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.movil.RespuestaCargaMasiva;
import mx.com.afirme.midas2.domain.movil.cotizador.AdministracionUsuarioMovil;
import mx.com.afirme.midas2.domain.movil.cotizador.CatalogoEstatusActualizacion;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.DatosDesglosePagosMovil;
import mx.com.afirme.midas2.domain.movil.cotizador.DescripcionVehiculo;
import mx.com.afirme.midas2.domain.movil.cotizador.DescuentosAgenteDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.Estado;
import mx.com.afirme.midas2.domain.movil.cotizador.MarcaVehiculo;
import mx.com.afirme.midas2.domain.movil.cotizador.ModeloVehiculo;
import mx.com.afirme.midas2.domain.movil.cotizador.SolicitudCotizacionMovil;
import mx.com.afirme.midas2.domain.movil.cotizador.TarifasDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.TarifasMovilCarga;
import mx.com.afirme.midas2.domain.movil.cotizador.TipoVehiculo;
import mx.com.afirme.midas2.domain.movil.cotizador.ValidacionClavePromo;
import mx.com.afirme.midas2.domain.movil.cotizador.ValidacionEstatusUsuarioActual;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotMovilDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.exeption.ErrorBuilder;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.estiloVehiculo.EstiloVehiculoService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.movil.cotizador.CotizacionMovilService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioDerechosService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.NegocioTipoPolizaService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.tarifa.DescuentoAgenteService;
import mx.com.afirme.midas2.service.tarifa.TarifaAgrupadorTarifaService;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.DateUtils.Indicador;


import oracle.jdbc.driver.OracleTypes;
import oracle.jdbc.internal.OracleConnection;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 *
 */
@Stateless
public class CotizacionMovilServiceImpl extends EntidadHistoricoDaoImpl implements CotizacionMovilService {
	
	private static final Logger LOG = Logger.getLogger(CotizacionMovilServiceImpl.class);
	// property constants
	public static final String CODIGOMARCAVEHICULO = "codigomarcavehiculo";
	public static final String DESCRIPCIONMARCAVEHICULO = "descripcionmarcavehiculo";
	public static final int CODIGO_MONEDA=484;
	public static final Long ID_NEGOCIO = Long.valueOf("43");
	public static final Long ID_PAQUETE = Long.valueOf("1");
	public static final String TIPODESCUENTOAGENTE="A";
	public static final String TIPODESCUENTOESTADO="E";
	public static final String CLAVENEGOCIO="A";
	public static final String USUARIO_MIDAS = "SISTEMA";
	public static final BigDecimal GRUPO_COTIZACION_MOVIL=new BigDecimal(83);
	public static final BigDecimal CANTIDAD_MAXIMA_CARGA_MASIVA_TARIFAS=new BigDecimal(83000);
	public static final BigDecimal CANTIDAD_MAXIMA_COTIZACIONES_AUTO=new BigDecimal(84000);
	public static final BigDecimal CANTIDAD_MAXIMA_COTIZACIONES_VIDA=new BigDecimal(85000);
	public static final BigDecimal CANTIDAD_MAXIMA_COTIZACIONES_CASA=new BigDecimal(86000);
	public static final BigDecimal CANTIDAD_MAXIMA_COTIZACIONES_PYME=new BigDecimal(87000);
	
	private SimpleJdbcCall actualizarVistaMaterializada;
	private SimpleJdbcCall cargaMasivaTarifasMovil;
	private JdbcTemplate jdbcTemplate;
	//public static final String CLAVEUSUARIO = "VMCASPAL";
	public static final BigDecimal FORMA_PAGO_ANUAL = BigDecimal.ONE;
	@EJB
	private EntidadService entidadService;	
	@PersistenceContext
	private EntityManager entityManager;
	@EJB
	private CotizacionService cotizacionService;
	@EJB
	private IncisoService incisoService;
	@EJB
	private CoberturaService coberturaService;
	@EJB
	private CalculoService calculoService;
	@EJB
	private NegocioService negocioService;	
	@EJB
	private NegocioTipoPolizaService negocioTipoPolizaService;
	@EJB
	private NegocioDerechosService negocioDerechosService;
	@EJB
	private FormaPagoInterfazServiciosRemote formaPagoInterfazServicios;
	@EJB
	private NegocioSeccionService negocioSeccionService;
	@EJB
	private NegocioPaqueteSeccionDao negocioPaqueteSeccionDao;
	@EJB
	private EstiloVehiculoService estiloVehiculoService;
	@EJB
	private ClienteFacadeRemote clienteFacade;	
	@EJB
	private NegocioProductoDao negocioProductoDao;
	@EJB
	private AgenteMidasService agenteMidasService;
	@EJB
	private MarcaVehiculoFacadeRemote marcaVehiculoFacade;
	@EJB
	private NegocioAgrupadorTarifaSeccionDao negAgrupadorTarifaSeccionDao;
	@EJB
	private TarifaAgrupadorTarifaService tarifaAgrupadorService;
	@EJB
	private CotizacionFacadeRemote cotizacionFacadeRemote;
	@EJB 
	private EstiloVehiculoFacadeRemote estiloVehiculoFacadeRemote;
	@EJB
	EstadoFacadeRemote estadoFacade;
	@EJB
	SeccionFacadeRemote seccionFacade;
	@EJB
	SeccionCotizacionFacadeRemote seccionCotizacionFacadeRemote;
	@EJB
	FormaPagoInterfazServiciosRemote formaPagoInterfazServiciosRemote;
	@EJB
	private MunicipioFacadeRemote municipioFacadeRemote;
	@EJB
	private DescuentoAgenteService descuentoAgenteService;
	
	private UsuarioService usuarioService;
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	boolean usuarioValido=false;	

	@EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	private List<String> registrosAfectados = new ArrayList<String>();
	private Connection con ;
	@SuppressWarnings("unused")
	private DataSource dataSource;
	
	@Resource(name = "jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {		
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.con = DataSourceUtils.getConnection(dataSource);
		this.actualizarVistaMaterializada = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName("MIDAS")
				.withCatalogName("PKG_COTIZADOR_MOVIL_AUTOS")
				.withProcedureName("actualizarVistaMaterializada")
				.declareParameters(
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Codigo", Types.VARCHAR));
		this.cargaMasivaTarifasMovil = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("PKG_AUT_CARGA_TARIFAS_MOVIL")
		.withProcedureName("SP_CARGA_TARIFAS_MOVIL")
		.declareParameters(
				new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
				new SqlOutParameter("pDesc_Codigo", Types.VARCHAR),				
				new SqlParameter("TARIFASMOVILLISTA", Types.ARRAY,"MIDAS.TARIFASMOVIL_LIST"));
	}

	public List<MarcaVehiculo> getMarcas() {
		LOG.info("finding all MarcaVehiculo instances");
		List<MarcaVehiculo> marcasList = new ArrayList<MarcaVehiculo>();
		Query query =null;
		try {
			final String queryString = " SELECT * FROM  MIDAS.VM_TAFIFAS ORDER BY MARCAVEHICULO ";
			query = entityManager.createNativeQuery(queryString);
			List<Object[]> listaObj = query.getResultList();
			for(Object[] object : listaObj){
				MarcaVehiculo marcaVehiculo = new MarcaVehiculo();
				try{
					marcaVehiculo.setId((Long)object[0]);	
				}
				catch(Exception e){
					marcaVehiculo.setId(new Long(((BigDecimal)object[0]).toString()));	
				}
				
				marcaVehiculo.setDescripcionMarcaVehiculo((String)object[1]);
				marcasList.add(marcaVehiculo);
			}
			return marcasList;
		} catch (RuntimeException re) {
			LOG.error("find all failed", re);
			throw re;
		}
	}
	public List<ModeloVehiculo> getModelos(Long marcaId) {
		LOG.info("finding all ModeloVehiculo instances");
		List<ModeloVehiculo> modelosList = new ArrayList<ModeloVehiculo>();
		Query query =null;
		TarifasDTO tarifa = entityManager.find(TarifasDTO.class, marcaId);
		try {
			
			final String queryString = "select distinct max(model.idtctarifas) as id,model.modelovehiculo from TarifasDTO model "
				    +"  inner join EstiloVehiculoDTO es on (es.descripcionEstilo=model.descripciontarifa) "
					+ " where model.marcavehiculo = '"
					+ tarifa.getMarcavehiculo()
					+ "'"
					+ " and  model.bajalogica=1 "
					+ " group by model.modelovehiculo "
					+ " order by model.modelovehiculo desc ";
			query = entityManager.createQuery(queryString);
			List<Object[]> listaObj = query.getResultList();
			for(Object[] object : listaObj){
				ModeloVehiculo modeloVehiculo = new ModeloVehiculo();
				modeloVehiculo.setId((Long)object[0]);
				modeloVehiculo.setModeloVehiculo( (Short)object[1]);
				modelosList.add(modeloVehiculo);
			}
			return modelosList;
		} catch (RuntimeException re) {
			LOG.error("find all failed",re);
			throw re;
		}
	}
	public List<Estado> getEstados(Long tipoVehiculoId) {
		LOG.info(">>finding all Estado instances");
		List<Estado> estadoList = new ArrayList<Estado>();
		Query query =null;
		TarifasDTO tarifa = entityManager
				.find(TarifasDTO.class, tipoVehiculoId);
		try {
			final String queryString = "select distinct max(model.idtctarifas) as id,model.estado from TarifasDTO model "
					+" INNER JOIN EstadoDTO ST ON( ST.stateName=model.estado) "
					+ " where model.marcavehiculo = '"
					+ tarifa.getMarcavehiculo()
					+ "'"
					+ " and  model.modelovehiculo= '"
					+ tarifa.getModelovehiculo()
					+ "'"
					+ " and  model.tipovehiculo= '"
					+ tarifa.getTipovehiculo()
					+ "'"
					+ " and  model.bajalogica=1 "
					+ " group by model.estado "
					+ " order by model.estado asc ";
			query = entityManager.createQuery(queryString);
			List<Object[]> listaObj = query.getResultList();
			for(Object[] object : listaObj){
				Estado estado = new Estado();
				estado.setId((Long)object[0]);
				estado.setNombreEstado( (String)object[1]);
				estadoList.add(estado);
			}
			return estadoList;
		} catch (RuntimeException re) {
			LOG.error("find all failed",re);
			throw re;
		}
	}
	public List<Estado> getALLEstados() {
		LOG.info(">>>.getALLEstados() finding all Estado instances");
		List<Estado> estadoList = new ArrayList<Estado>();
		Query query = null;
		try {
			final String queryString = "select distinct max(model.idtctarifas) as id,model.estado from TarifasDTO model "
				    + " where  model.bajalogica=1 "
					+ " group by model.estado "
					+ " order by model.estado asc ";
			query = entityManager.createQuery(queryString);
			List<Object[]> listaObj = query.getResultList();
			for (Object[] object : listaObj) {
				Estado estado = new Estado();
				estado.setId((Long) object[0]);
				estado.setNombreEstado((String) object[1]);
				estadoList.add(estado);
			}
			return estadoList;
		} catch (RuntimeException re) {
			LOG.error("find all failed",re);
			throw re;
		}
	}
	
	public List<TipoVehiculo> getTipos(Long modeloId) {
		LOG.info(">>>finding all TipoVehiculo instances");
		List<TipoVehiculo> estadoList = new ArrayList<TipoVehiculo>();
		Query query =null;
		TarifasDTO tarifa = entityManager.find(TarifasDTO.class, modeloId);
		try {
			final String queryString = "select distinct max(model.idtctarifas) as id,model.tipovehiculo from TarifasDTO model "
				    +"  inner join EstiloVehiculoDTO es on (es.descripcionEstilo=model.descripciontarifa) "
					+ " where model.marcavehiculo = '"
					+ tarifa.getMarcavehiculo()
					+ "'"
					+ " and  model.modelovehiculo= '"
					+ tarifa.getModelovehiculo()
					+ "'"
					+ " and  model.bajalogica=1 "
					+ " group by model.tipovehiculo "
					+ " order by model.tipovehiculo asc ";
			query = entityManager.createQuery(queryString);
			List<Object[]> listaObj = query.getResultList();
			for(Object[] object : listaObj){
				TipoVehiculo estado = new TipoVehiculo();
				estado.setId((Long)object[0]);
				estado.setDescripcionTipoVehiculo((String)object[1]);
				estadoList.add(estado);
			}
			return estadoList;
		} catch (RuntimeException re) {
			LOG.error("find all failed",re);
			throw re;
		}
	}
	public List<DescripcionVehiculo> getDescripcionVehiculo(Long estadoId) {
		LOG.info(">>finding all DescripcionVehiculo instances");
		List<DescripcionVehiculo> estadoList = new ArrayList<DescripcionVehiculo>();
		Query query =null;
		TarifasDTO tarifa = entityManager.find(TarifasDTO.class, estadoId);
		try {
			final String queryString = "select distinct max(model.idtctarifas) as id,model.descripciontarifa from TarifasDTO model "
					+" inner join EstiloVehiculoDTO es on (es.descripcionEstilo=model.descripciontarifa) "
					+ " where model.marcavehiculo = '"
					+ tarifa.getMarcavehiculo()
					+ "'"
					+ " and  model.modelovehiculo= '"
					+ tarifa.getModelovehiculo()
					+ "'"
					+ " and  model.tipovehiculo= '"
					+ tarifa.getTipovehiculo()
					+ "'"
					+ " and  model.estado= '"
					+ tarifa.getEstado()
					+ "'"
					+ " and  model.bajalogica=1 "
					+ " group by model.descripciontarifa "
					+ " order by model.descripciontarifa asc ";
			query = entityManager.createQuery(queryString);
			List<Object[]> listaObj = query.getResultList();
			for(Object[] object : listaObj){
				DescripcionVehiculo descripcionVehiculo = new DescripcionVehiculo();
				descripcionVehiculo.setId((Long)object[0]);
				descripcionVehiculo.setDescripcionVehiculo((String)object[1]);
				estadoList.add(descripcionVehiculo);
			}
			return estadoList;
		} catch (RuntimeException re) {
			LOG.error("find all failed",re);
			throw re;
		}
	}
	public RespuestaCargaMasiva setTarifasExcelList(List<TarifasDTO> tarifasExcelList,TarifasDTO tarifaParam){
		LOG.info(">>>Saving all TarifasExcelList instances");
		RespuestaCargaMasiva respuesta= new RespuestaCargaMasiva();
		int cont = 0;
		int registrosNoInsertados=0;
		int registrosInsertados=0;
		int registrosActualizados=0;
		registrosAfectados.clear();
		try {
			int contador=0;
			if ((tarifasExcelList != null)
					|| (tarifasExcelList.isEmpty() != false)) {
				for (TarifasDTO tarifa : tarifasExcelList) {
					contador++;
					if (tarifa.isTarifaValida()) {
							List<TarifasDTO>  list=existeTarifa(tarifa);
							if (list==null||list.isEmpty()) {
								LOG.info("*************Entra a no :existeTarifa");
								tarifa.setIdtctarifas(Long.valueOf("0"));
								saveTarifa(tarifa);
								registrosInsertados++;
								cont++;
								
							} else {
								LOG.info("*************Entra a :existeTarifa");
								this.actualizarTarifas(tarifa,list);
								registrosActualizados++;
								cont++;
							}
					} else {
						registrosAfectados.add("Registro "
								+ String.valueOf(cont) + " "
								+ "Estilo["+tarifa.getDescripciontarifa()+"] o Marca ["+tarifa.getMarcavehiculo()+"] no validos ");
						registrosNoInsertados++;
						cont++;
					}
					
					LOG.info("Tarifa numero =" + contador + " ; de "
							+ tarifasExcelList.size());
				}

			} else {
				registrosAfectados.add("Registro " + String.valueOf(cont) + " "
						+ "Vacios ");
				registrosNoInsertados++;
				cont++;
			}
			
		} catch (Exception e) {
			LOG.error("Saving  TarifasExcelList  failed",e);
			String error = e.toString().replace("java.lang.Exception:", "");
			registrosNoInsertados++;
			cont++;		
			registrosAfectados.add("Registro "+String.valueOf(cont)+" "+error);
		}	
		respuesta.setRegistrosAfectados(registrosAfectados);
		respuesta.setRegistrosActualizados(registrosActualizados);
		respuesta.setRegistrosInsertados(registrosInsertados);
		respuesta.setRegistrosNoInsertados(registrosNoInsertados);
		return respuesta;
	}
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public RespuestaCargaMasiva saveTarifasExcelList(List<TarifasDTO> tarifasExcelList,TarifasDTO tarifaParam){
		LOG.info(">>>saveTarifasExcelList()");
		RespuestaCargaMasiva respuesta= new RespuestaCargaMasiva();
		List<TarifasMovilCarga> lista= new ArrayList<TarifasMovilCarga>();
		List<String> listaErrores=new ArrayList<String>();
		int cont = 0;
		int registrosNoInsertados=0;
		int registrosInsertados=0;
		ResultadoCarga resultado=new ResultadoCarga();
		registrosAfectados.clear();
		try {
			int contador=0;
			if ((tarifasExcelList != null)) {
				for (TarifasDTO tarifa : tarifasExcelList) {
					contador++;
					if (tarifa.isTarifaValida()) {
						 TarifasMovilCarga tarifaTmp=poblarTarifasMovil(tarifa);
						 if(tarifaTmp!=null){
							 lista.add(tarifaTmp);
							 registrosInsertados++;
							 cont++;
						 }
						 else{
							 registrosAfectados.add("Registro " + String.valueOf(cont) + " "
										+ "Datos inconsistentes");
							 cont++;
						 }						 
						
					} else {
						registrosAfectados.add("Registro "
								+ String.valueOf(cont) + " "
								+ tarifa.getMensajeTarifaValida());
						registrosNoInsertados++;
						cont++;
					}
					
					LOG.info("Tarifa numero =" + contador + " ; de "
							+ tarifasExcelList.size());
				}
				
			//ejecutar carga masiva db
				resultado=saveTarifasMovilCarga(lista);
				listaErrores=resultado.getErroresList();
			} else {
				registrosAfectados.add("Registro " + String.valueOf(cont) + " "
						+ "Vacios ");
				registrosNoInsertados++;
				cont++;
			}
			
		} catch (Exception e) {
			LOG.error("Saving  TarifasExcelList  failed",e);
			String error = e.toString().replace("java.lang.Exception:", "");
			registrosNoInsertados++;
			cont++;		
			registrosAfectados.add("Registro "+String.valueOf(cont)+" "+error);
		}	
		for(String error:listaErrores){
			registrosAfectados.add(error);
		}
		respuesta.setRegistrosAfectados(registrosAfectados);
		respuesta.setRegistrosActualizados(Integer.valueOf(resultado.getActualizados().toString()));
		respuesta.setRegistrosInsertados(Integer.valueOf(resultado.getInsertados().toString()));
		respuesta.setRegistrosNoInsertados(Integer.valueOf(resultado.getErrores().toString()));
		return respuesta;
	}
	public void saveTarifa(TarifasDTO entity) {
		LOG.info(">>>saving Tctarifas instance");
		try {
				entity.setFecharegistro(new Date());
				entityManager.persist(entity);
			LOG.info("save successful=> marca "+entity.getMarcavehiculo() + "estilo="+entity.getMarcavehiculo());
		} catch (RuntimeException re) {
			LOG.error("save failed",re);
			throw re;
		}
	}

	private void actualizarTarifas(TarifasDTO tarifa, List<TarifasDTO> tarifas) {
		for (TarifasDTO object : tarifas) {
			if (object != null) {
				object.setBajalogica(tarifa.getBajalogica());
				object.setClavetarifa(tarifa.getClavetarifa());
				object.setTarifaamplia(tarifa.getTarifaamplia());
				object.setTarifalimitada(tarifa.getTarifalimitada());
				object.setOc(tarifa.getOc());
				update(object);
			}

		}
	}
	public void update(TarifasDTO entity) {
		LOG.info(">>update Tctarifas instance");
		try {
			entity.setFecharegistro(new Date());
			entityManager.merge(entity);
				LOG.info("update  successful=> marca "+entity.getMarcavehiculo() + "estilo="+entity.getMarcavehiculo());
		} catch (RuntimeException re) {
			LOG.error("update failed",re);
			throw re;
		}
	}

	public void saveCotizacionMovil(CotizacionMovilDTO entity) {
		LOG.info("saving CotizacionMovilDTO instance" );
		try {
			if(entity.getTipoDescuento()==null){
				entity.setTipoDescuento("0");
			}
			entidadService.save(entity);
			LOG.info("save CotizacionMovilDTO successful");
		} catch (RuntimeException re) {
			LOG.error("save CotizacionMovilDTO failed",re);
			throw re;
		}
	}
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CotizacionMovilDTO getCotizacionAuto(
			SolicitudCotizacionMovil solicitud) {
		LOG.info(">>> save CotizacionMovilDTO instance : " + solicitud);
		boolean descuentoAgente = false;
		boolean descuentoEstado = false;
		boolean aplicaDescuento = false;
		TarifasDTO entidad = null;
		CotizacionMovilDTO cotizacionMovil = new CotizacionMovilDTO();
		Usuario usuario = null;
		String nombreProspecto = "";
		String apellidoPaterno = "";
		String apellidoMaterno = "";
		String telefonoProspecto = "";
		String emailProspecto = "";
		ErrorBuilder eb = new ErrorBuilder();
		boolean agenteValido = false;
		Agente filtro = new Agente();
		DescuentosAgenteDTO descuentoAgenteDTO = null;
		List<DescuentosAgenteDTO> descuentoAgenteDTOList = null;
		
		if(solicitud.getTipoCotizacion() == null) {
			solicitud.setTipoCotizacion(SolicitudCotizacionMovil.TIPO_COTIZACION_MOVIL);
		}
		//buscar promo de usuario portal													
		usuario = usuarioService.getUsuarioActual();		
		if(!isNotNull(usuario)) {
			nombreProspecto = solicitud.getNombre();
			apellidoPaterno = solicitud.getApellidoPaterno();
			apellidoMaterno = solicitud.getApellidoMaterno();
			telefonoProspecto = solicitud.getTelefono();
			emailProspecto = solicitud.getEmail();
			usuario = usuarioService.buscarUsuarioPorNombreUsuario(USUARIO_MIDAS);
			usuario.setNombreUsuario(USUARIO_MIDAS);
			usuario.setNombre(nombreProspecto);
			usuario.setApellidoPaterno(apellidoPaterno);
			usuario.setApellidoMaterno(apellidoMaterno);
			usuario.setEmail(emailProspecto);
			usuario.setTelefonoCelular(telefonoProspecto);
			usuario.setPromoCode("0");
		} else {
			nombreProspecto = (regresaObjectEsNuloParametro(usuario.getNombreCompleto(), " ").toString());	
			telefonoProspecto = (regresaObjectEsNuloParametro(usuario.getTelefonoCelular(), "").toString());
			emailProspecto = (regresaObjectEsNuloParametro(usuario.getEmail(), " ").toString());
			
		}
		usuarioValido = isUserValid(usuario,
				SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL
						.equals(solicitud.getTipoCotizacion()));
		if (usuario != null) {
			if (usuario.getPromoCode() != null) {
				descuentoAgenteDTOList = descuentoAgenteService
						.findByPropertyActivos(CLAVENEGOCIO,"clavepromo",
								usuario.getPromoCode());
			} else {
				usuario.setPromoCode("0");
				descuentoAgenteDTOList = descuentoAgenteService
						.findByPropertyActivos(CLAVENEGOCIO,"clavepromo",
								usuario.getPromoCode());
				solicitud.setClaveagente(null);// para que no aplique descuento
												// por agente
			}
		}
		if (descuentoAgenteDTOList != null) {
			for (DescuentosAgenteDTO descuentoAgenteDTOTmp : descuentoAgenteDTOList) {
				if (descuentoAgenteDTOTmp != null) {
					descuentoAgenteDTO = descuentoAgenteDTOTmp;
					solicitud.setClaveagente(descuentoAgenteDTO
							.getClaveagente());
					filtro.setIdAgente(new Long(descuentoAgenteDTO
							.getClaveagente()));
				}

			}
		}

		if (((descuentoAgenteDTOList == null) || (descuentoAgenteDTOList
				.isEmpty())) && (descuentoAgenteDTO == null)) {
			throw new ApplicationException(eb.addFieldError("clavePromo",
					"Invalido o Desactivado"));
		} else {
			if (solicitud.getClaveagente() != null) {
				try {
					List<Agente> agenteList;
					agenteList = agenteMidasService.findByFilters(filtro);
					for (Agente agenteTmp : agenteList) {
						if (agenteTmp != null)
							agenteValido = true;
					}
				} catch (Exception e) {
					agenteValido = false;
					LOG.error("Ocurrio un error en getCotizacionAuto " + e);
				}
				aplicaDescuento = true;
			} else {
				solicitud.setClaveagente("0");
				aplicaDescuento = false;
			}
			if (!agenteValido) {
				throw new ApplicationException(eb.addFieldError("clave promo",
						"no existe"));
			} else if (usuario != null) {
				entidad = entityManager.find(TarifasDTO.class,
						solicitud.getIdDescripcionEstilo());
				
				if (entidad.getTarifaamplia() == null || entidad.getTarifalimitada() == null) {
					throw new ApplicationException(eb.addFieldError("error",
							"Las tarifas no est\u00E1n configuradas para el estado y veh\u00EDculo proporcionados."));
				}
				
				if (SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL.equals(solicitud.getTipoCotizacion())) {
					if (entidad.getTarifaBasica() == null || entidad.getTarifaPlata() == null ||
							entidad.getTarifaOro() == null || entidad.getTarifaPlatino() == null) {
						throw new ApplicationException(eb.addFieldError("error",
						"Las tarifas no est\u00E1n configuradas para el estado y veh\u00EDculo proporcionados."));
					} 
				} else {
					entidad.setTarifaBasica(new BigDecimal(regresaObjectEsNuloParametro(entidad.getTarifaBasica(),
							BigDecimal.ZERO).toString()));
					
					entidad.setTarifaPlata(new BigDecimal(regresaObjectEsNuloParametro(entidad.getTarifaPlata(),
							BigDecimal.ZERO).toString()));
					
					entidad.setTarifaOro(new BigDecimal(regresaObjectEsNuloParametro(entidad.getTarifaOro(),
							BigDecimal.ZERO).toString()));
					
					entidad.setTarifaPlatino(new BigDecimal(regresaObjectEsNuloParametro(entidad.getTarifaPlatino(),
							BigDecimal.ZERO).toString()));
				}
				
				try {
					if(!usuarioValido){											
						cotizacionMovil.setValorPrimaAmplia(entidad.getTarifaamplia());
						cotizacionMovil.setValorPrimaLimitada(entidad.getTarifalimitada());
						cotizacionMovil.setUsuario(null);
					}
						cotizacionMovil.setDescuentoAgente(descuentoAgenteDTO);
						cotizacionMovil.setClavepromo(descuentoAgenteDTO
								.getClavepromo());
						cotizacionMovil.setPorcentajeDescuento(descuentoAgenteDTO
								.getPorcentaje());
						cotizacionMovil.setUsuario(usuario);
					cotizacionMovil.setClavetarifa(entidad.getClavetarifa());
					cotizacionMovil.setDescripciontarifa(entidad
							.getDescripciontarifa());
					cotizacionMovil.setEstado(entidad.getEstado());
					cotizacionMovil.setFecharegistro(new Date());
					cotizacionMovil.setGrupovehiculos(entidad
							.getGrupovehiculos());
					cotizacionMovil.setIdtocotizacionmovil(solicitud.getIdToCotizacionMovil() == null ? 
							Long.valueOf("0") : solicitud.getIdToCotizacionMovil());
					cotizacionMovil
							.setMarcavehiculo(entidad.getMarcavehiculo());
					cotizacionMovil.setModelovehiculo(entidad
							.getModelovehiculo());
					cotizacionMovil.setOc(entidad.getOc());
					cotizacionMovil.setTarifaamplia(entidad.getTarifaamplia());
					cotizacionMovil.setTarifalimitada(entidad
							.getTarifalimitada());
					cotizacionMovil.setTarifaBasica(entidad.getTarifaBasica());
					cotizacionMovil.setTarifaPlata(entidad.getTarifaPlata());
					cotizacionMovil.setTarifaOro(entidad.getTarifaOro());
					cotizacionMovil.setTarifaPlatino(entidad.getTarifaPlatino());
					cotizacionMovil.setTipovehiculo(entidad.getTipovehiculo());
					cotizacionMovil.setUserId(Long.valueOf(usuario.getId()));
					cotizacionMovil.setClaveagente(solicitud.getClaveagente());
					cotizacionMovil.setNombreProspecto(nombreProspecto);
					cotizacionMovil.setTelefonoProspecto(telefonoProspecto);
					cotizacionMovil.setEmailProspecto(emailProspecto);
					cotizacionMovil.setClaveNegocio(CLAVENEGOCIO);
					cotizacionMovil.setEstatusProspecto(new Short("1"));
					cotizacionMovil.setUuId(regresaObjectEsNuloParametro(solicitud.getUuid(), " ").toString());
					cotizacionMovil.setOsMovil(regresaObjectEsNuloParametro(solicitud.getOs(), " ").toString());
					cotizacionMovil.setTipoCotizacion(solicitud.getTipoCotizacion());
					cotizacionMovil.setProcesada(solicitud.getContratar());
					String idEstadoNacimiento = "28";
					String idCiudad = "28";
					// idEstadoNacimiento
					List<EstadoDTO> estadoList = estadoFacade.findByProperty(
							"stateName", entidad.getEstado());
					for (EstadoDTO estadoTmp : estadoList) {
						idEstadoNacimiento = estadoTmp.getStateId();
					}
					//Validar que el estado tenga muncipios configurados
					final List<MunicipioDTO> municipioList = municipioFacadeRemote
							.findByProperty("stateId", idEstadoNacimiento);
					if (CollectionUtils.isEmpty(municipioList)) {
						throw new ApplicationException(eb.addFieldError(
								"error", "Cotize con otro Estado"));
					} else {

						try {
							if (cotizacionMovil.getClaveagente() != null&&usuarioValido) {
								if (descuentoAgenteDTO.getClavepromo().trim()
										.equals("0")) {
									aplicaDescuento = false;
									LOG.debug("aplicaDescuento" + aplicaDescuento);
								} else {
									LOG.debug("aplicaDescuento" + aplicaDescuento);
									if (((new BigDecimal(
											descuentoAgenteDTO.getPorcentaje()) == BigDecimal.ZERO) || descuentoAgenteDTO
											.getPorcentaje().trim().equals("0"))
											&& (descuentoAgenteDTO
													.getClavepromo().trim()
													.equals("0") == false)) {
										descuentoEstado = true;
										descuentoAgente = false;
									} else {
										descuentoAgente = true;
										descuentoEstado = false;
									}
								}

							}
							
							LOG.info(".getCotizacionAuto().aplicaDescuento : " + aplicaDescuento
									+";descuentoEstado="+descuentoEstado+"descuentoAgente="+descuentoAgente);
							
							if (solicitud.getIdPaquete() == null) {
								solicitud.setIdPaquete(ID_PAQUETE);
							}
							
							Double valorFactura = 0.0;
							if(SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL.equals(
									solicitud.getTipoCotizacion())) {
								if (solicitud.getIdPaquete().compareTo(Paquete.AMPLIA) == 0) {
									valorFactura = entidad.getTarifaamplia().doubleValue();
								} if (solicitud.getIdPaquete().compareTo(Paquete.LIMITADA) == 0) {
									valorFactura = entidad.getTarifalimitada().doubleValue();
								} else if (solicitud.getIdPaquete().compareTo(Paquete.BASICA) == 0) {
									valorFactura = entidad.getTarifaBasica().doubleValue();
								} else if (solicitud.getIdPaquete().compareTo(Paquete.PLATA) == 0) {
									valorFactura = entidad.getTarifaPlata().doubleValue();
								} else if (solicitud.getIdPaquete().compareTo(Paquete.ORO) == 0) {
									valorFactura = entidad.getTarifaOro().doubleValue();
								} else if (solicitud.getIdPaquete().compareTo(Paquete.PLATINO) == 0) {
									valorFactura = entidad.getTarifaPlatino().doubleValue();
								}
							} else {
								valorFactura = entidad.getTarifaamplia().doubleValue();
							}

							if(valorFactura <= 0) {
								throw new ApplicationException(eb.addFieldError("valorFactura",
								"debe ser mayor a 0"));
							}
							if(usuarioValido){
							cotizacionMovil = creaCotizacion(cotizacionMovil,
									ID_NEGOCIO, solicitud.getIdPaquete(),
									entidad.getDescripciontarifa(), entidad
											.getModelovehiculo().toString(),
									entidad.getMarcavehiculo(),
									idEstadoNacimiento, valorFactura,
									usuario.getNombre() != null ? usuario.getNombre(): "",
									usuario.getApellidoPaterno() != null ? usuario.getApellidoPaterno(): "",
									usuario.getApellidoMaterno() != null ? usuario.getApellidoMaterno(): "",
									idEstadoNacimiento, idCiudad,
									idEstadoNacimiento,
									telefonoProspecto,
									emailProspecto, descuentoEstado,
									aplicaDescuento, descuentoAgente, solicitud);
							}


						} catch (Exception e) {
							LOG.error("Ocurrio un error en getCotizacionAuto " ,e);
						}
						
						try {
							saveCotizacionMovil(cotizacionMovil);
						} catch (Exception e) {
							LOG.error("Ocurrio un error en getCotizacionAuto ",
									e);
						}
					
					}

				} catch (RuntimeException re) {
					LOG.error("save CotizacionMovilDTO failed",re);
					return null;
				}
			}
		}
		return cotizacionMovil;
	}

	public List<TarifasDTO> findAllTarifas() {
		LOG.info(">>finding all TarifasDTO instances");
		try {
			final String queryString = "select model from TarifasDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LOG.error("find all failed",re);
			throw re;
		}
	}
	public List<TarifasDTO> findTarifasByFilters(TarifasDTO filter){
		LOG.info(">>findTarifasByFilters  TarifasDTO instances");
		List<TarifasDTO>  lista=new ArrayList<TarifasDTO>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		try{
			queryString.append(" select distinct * ");
			queryString.append(" from MIDAS.TCTARIFAS entidad ");
			queryString.append(" where ");
			if(isNotNull(filter)){
				int index=1;
					if(filter.getIdtctarifas()!=null){
						addCondition(queryString, " entidad.idtctarifas=? ");
						params.put(index, filter.getIdtctarifas());
						index++;					
					}
					if(filter.getEstado()!=null){
						addCondition(queryString, " entidad.estado=? ");
						params.put(index, filter.getEstado());
						index++;					
					}
					if(filter.getMarcavehiculo()!=null){
						addCondition(queryString, " entidad.marcavehiculo=? ");
						params.put(index, filter.getMarcavehiculo());
						index++;					
					}
					if(filter.getTipovehiculo()!=null){
						addCondition(queryString, " entidad.tipovehiculo=? ");
						params.put(index, filter.getTipovehiculo());
						index++;					
					}
					if(filter.getGrupovehiculos()!=null){
						addCondition(queryString, " entidad.grupovehiculos=? ");
						params.put(index, filter.getGrupovehiculos());
						index++;					
					}
	
				if(params.isEmpty()){
					int lengthWhere="where ".length();
					queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
				}
				String finalQuery=getQueryString(queryString)+" order by entidad.idtctarifas desc ";
				Query query=entityManager.createNativeQuery(finalQuery,TarifasDTO.class);
				if(!params.isEmpty()){
					for(Integer key:params.keySet()){
						query.setParameter(key,params.get(key));
					}
				}
				//query.setMaxResults(100);
				lista=query.getResultList();
			}
		
		} catch (RuntimeException re) {
			LOG.error("findTarifasByFilters all failed",re);
			throw re;
		}
		return lista;
		
	}
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CotizacionMovilDTO creaCotizacion(
			CotizacionMovilDTO cotizacionMovil, Long idNegocio, Long idPaquete,
			String estilo, String modelo, String marca,
			String idEstadoCirculacion, Double valorFactura,
			String nombreAsegurado, String apellidoPaterno,
			String apellidoMaterno, String idEstadoNacimiento, String idCiudad,
			String idEstado, String telefonoCasa, String email,
			boolean descuentoEstado, boolean aplicaDescuento,boolean descuentoAgente, SolicitudCotizacionMovil solicitudMovil) {

		Double descuento = 0.0;
		String folio = "000000001";
		String idMunicipioCirculacion = "0";
		CotizacionDTO cotizacion;
		BigDecimal idCotizacion = null;
		ErrorBuilder eb = new ErrorBuilder();

		Agente agente = new Agente();
		List<Agente> agenteList;
		cotizacion = new CotizacionDTO();
		SolicitudDTO solicitud = new SolicitudDTO();
		Date inicioVigencia = new Date();
		Date finVigencia = DateUtils.add(inicioVigencia, Indicador.Days, 365);
		agente.setIdAgente(new Long(cotizacionMovil.getDescuentoAgente()
				.getClaveagente()));
		agenteList = agenteMidasService.findByFilters(agente);
		for (Agente agenteTmp : agenteList) {
			if (agenteTmp != null)
				agente = agenteTmp;
		}
		solicitud.setAgente(agente);
		solicitud.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
		Negocio negocio = negocioService.findById(idNegocio);
		solicitud.setNegocio(negocio);

		NegocioProducto negocioProducto = negocioProductoDao
				.getNegocioProductoByIdNegocioIdProducto(
						negocio.getIdToNegocio(), new BigDecimal(191));

		NegocioTipoPoliza negocioTipoPoliza = negocioTipoPolizaService
				.getPorIdNegocioProductoIdToTipoPoliza(
						negocioProducto.getIdToNegProducto(), negocioProducto
								.getProductoDTO().getTiposPoliza().get(0)
								.getIdToTipoPoliza());
		negocioTipoPoliza.setNegocioProducto(negocioProducto);

		NegocioDerechoPoliza negocioDerechoPoliza = negocioDerechosService
				.obtenerDechosPolizaDefault(idNegocio);
		cotizacion.setSolicitudDTO(solicitud);
		cotizacion.setNegocioTipoPoliza(negocioTipoPoliza);
		cotizacion.setFolio(folio);
		if (solicitudMovil.getIdFormaPago() == null) {
			solicitudMovil.setIdFormaPago(FORMA_PAGO_ANUAL);
		}		
		cotizacion.setTipoCotizacion(solicitudMovil.getTipoCotizacion());
		if(solicitudMovil.getIdToCotizacionMidas() == null) {
		cotizacion = cotizacionService
			.crearCotizacion(cotizacion, cotizacionMovil.getUsuario().getNombreUsuario());
		} else {
			cotizacion = entidadService.findById(CotizacionDTO.class, solicitudMovil.getIdToCotizacionMidas());
			cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		}
		
		FormaPagoIDTO formaPago = null;
		try {
			formaPago = formaPagoInterfazServicios.getPorId(new Integer(
					solicitudMovil.getIdFormaPago().intValue()), (short) CODIGO_MONEDA, null);
			if (formaPago == null)
				formaPago = formaPagoInterfazServicios.getPorId(cotizacion
						.getIdFormaPago().intValue(), (short) CODIGO_MONEDA,
						cotizacion.getCodigoUsuarioModificacion());
			if (formaPago == null)
				formaPago = formaPagoInterfazServicios.getPorId(60,
						(short) CODIGO_MONEDA, null);
			if (formaPago == null)
				formaPago = formaPagoInterfazServicios.getPorId(60,
						(short) CODIGO_MONEDA,
						cotizacion.getCodigoUsuarioModificacion());
		} catch (SystemException e) {
			LOG.error("Ocurrio un error en creaCotizacion " + e);
			throw new RuntimeException(e);
		} catch (NullPointerException e) {
			LOG.error("Ocurrio un error en creaCotizacion " + e);
		}
		// cotizacion =
		// cotizacionService.crearCotizacion(cotizacion,claveUsuario.substring(0,
		// 8));
		cotizacion.setNegocioDerechoPoliza(negocioDerechoPoliza);
		cotizacion.setFechaInicioVigencia(inicioVigencia);
		cotizacion.setFechaFinVigencia(finVigencia);
		cotizacion.setIdFormaPago(solicitudMovil.getIdFormaPago());
		cotizacion.setIdMedioPago(new BigDecimal(negocioProducto
				.getNegocioMedioPagos().get(0).getMedioPagoDTO()
				.getIdMedioPago()));
		cotizacion.setIdMoneda(new BigDecimal(CODIGO_MONEDA));

		if (formaPago != null) {
			cotizacion.setPorcentajePagoFraccionado(formaPago
					.getPorcentajeRecargoPagoFraccionado().doubleValue());
		}
		cotizacion.setPorcentajebonifcomision(0.0);
		/* Datos de contratante */
		cotizacion.setNombreContratante(nombreAsegurado.concat(" ")
				.concat(apellidoPaterno).concat(" ").concat(apellidoMaterno).trim());
		cotizacion.setNombreAsegurado(nombreAsegurado.concat(" ")
				.concat(apellidoPaterno).concat(" ").concat(apellidoMaterno).trim());
		cotizacionService.guardarCotizacion(cotizacion);
		idCotizacion = cotizacion.getIdToCotizacion();

		cotizacion = entidadService.findById(CotizacionDTO.class, idCotizacion);
		cotizacionMovil.setIdtocotizacionmidas(cotizacion.getIdToCotizacion());
		// Guardar inciso
		IncisoCotizacionDTO incisoCotizacion = guardaInciso(cotizacion,
				idNegocio, idPaquete, estilo, modelo, marca,
				idEstadoCirculacion, idMunicipioCirculacion, valorFactura,
				descuento, nombreAsegurado, apellidoPaterno, apellidoMaterno);

		/*
		 * vertifica tipo de descuento 1.- Estado 2.- Agente
		 */

		BigDecimal descuentoCot = BigDecimal.ZERO;
		BigDecimal tarifaLimitada = null, tarifaBasica = null, tarifaPlata = null, tarifaOro = null, tarifaPlatino = null, tarifaAmplia = null;
		cotizacionMovil.setIdtocotizacionmidas(idCotizacion);
		LOG.info(".creaCotizacion(). aplicaDescuento => " + aplicaDescuento 
				+"estado="+descuentoEstado+";agente="+descuentoAgente);
		if (aplicaDescuento == true) {
			if (descuentoEstado == false) {
				LOG.info(".creaCotizacion() Aplicando descuento agente de  cotizacion => " + cotizacion  + ";TIPODESCUENTOAGENTE=>"+TIPODESCUENTOAGENTE);
				if (cotizacionMovil.getClaveagente() != null) {

					if (cotizacionMovil.getClaveagente().trim().equals("0")) {
						descuentoCot = BigDecimal.ZERO;
						LOG.info(".creaCotizacion() descuentoCot=> " + descuentoCot );
					} else {
						/*List<DescuentosAgenteDTO> descuentosAgenteDTO = descuentoAgenteService
								.findByClaveagente(cotizacionMovil
										.getClaveagente());
						LOG.info(".creaCotizacion() descuentosAgenteDTO=> " + cotizacionMovil.getDescuentoAgente() ,
								Level.INFO, null);*/
						try {
							descuentoCot = new BigDecimal(cotizacionMovil.getDescuentoAgente()
									.getPorcentaje());
							LOG.info(".creaCotizacion() Try=> descuentoCot=> " + descuentoCot);

						} catch (Exception e) {
							LOG.error("Ocurrio un error en creaCotizacion " + e);
							descuentoCot = BigDecimal.ZERO;
							LOG.info(".creaCotizacion() Catch=> descuentoCot=> " + descuentoCot );
						}
						cotizacionMovil.setPorcentajeDescuento(descuentoCot.toString());
						cotizacionMovil.setTipoDescuento(TIPODESCUENTOAGENTE);
						LOG.info(".creaCotizacion() descuentoCot.toString()=> " + descuentoCot.toString());
						LOG.info(".creaCotizacion() cotizacionMovil.getPorcentajeDescuento()=> " + cotizacionMovil.getPorcentajeDescuento());
					}

				}
				tarifaAmplia = getMontoPagoRecibo(formaPago,
						cotizacionMovil, descuentoCot, true,
						cotizacionMovil.getTarifaamplia(),aplicaDescuento,descuentoEstado,descuentoAgente);
				tarifaLimitada = getMontoPagoRecibo(
						formaPago, cotizacionMovil, descuentoCot, true,
						cotizacionMovil.getTarifalimitada(),aplicaDescuento,descuentoEstado,descuentoAgente);
				tarifaBasica = getMontoPagoRecibo(
						formaPago, cotizacionMovil, descuentoCot, true,
						cotizacionMovil.getTarifaBasica(),aplicaDescuento,descuentoEstado,descuentoAgente); 
				tarifaPlata = getMontoPagoRecibo(
						formaPago, cotizacionMovil, descuentoCot, true,
						cotizacionMovil.getTarifaPlata(),aplicaDescuento,descuentoEstado,descuentoAgente);
				tarifaOro = getMontoPagoRecibo(
						formaPago, cotizacionMovil, descuentoCot, true,
						cotizacionMovil.getTarifaOro(),aplicaDescuento,descuentoEstado,descuentoAgente);
				tarifaPlatino = getMontoPagoRecibo(
						formaPago, cotizacionMovil, descuentoCot, true,
						cotizacionMovil.getTarifaPlatino(),aplicaDescuento,descuentoEstado,descuentoAgente);
			} else {
				cotizacionMovil.setPorcentajeDescuento(NegocioEstadoDescuentoDao
						.findByNegocioAndEstado(
								ID_NEGOCIO,
								idEstadoCirculacion)
						.getPctDescuento().toString());
				cotizacionMovil.setTipoDescuento(TIPODESCUENTOESTADO);
				LOG.info(".getCotizacionAuto() Aplicando descuento de estado de  cotizacion => " + cotizacion + ";TIPODESCUENTOESTADO=>"+TIPODESCUENTOESTADO);
				tarifaAmplia = getMontoPagoRecibo(formaPago,
						cotizacionMovil, BigDecimal.ZERO, true,
						cotizacionMovil.getTarifaamplia(),aplicaDescuento,descuentoEstado,descuentoAgente);
				tarifaLimitada = getMontoPagoRecibo(
						formaPago, cotizacionMovil, BigDecimal.ZERO, true,
						cotizacionMovil.getTarifalimitada(),aplicaDescuento,descuentoEstado,descuentoAgente);
				tarifaBasica = getMontoPagoRecibo(
						formaPago, cotizacionMovil, BigDecimal.ZERO, true,
						cotizacionMovil.getTarifaBasica(),aplicaDescuento,descuentoEstado,descuentoAgente);
				tarifaPlata = getMontoPagoRecibo(
						formaPago, cotizacionMovil, BigDecimal.ZERO, true,
						cotizacionMovil.getTarifaPlata(),aplicaDescuento,descuentoEstado,descuentoAgente);
				tarifaOro = getMontoPagoRecibo(
						formaPago, cotizacionMovil, BigDecimal.ZERO, true,
						cotizacionMovil.getTarifaOro(),aplicaDescuento,descuentoEstado,descuentoAgente);
				tarifaPlatino = getMontoPagoRecibo(
						formaPago, cotizacionMovil, BigDecimal.ZERO, true,
						cotizacionMovil.getTarifaPlatino(),aplicaDescuento,descuentoEstado,descuentoAgente);
			}
		
			if (idPaquete.compareTo(Paquete.AMPLIA) == 0) {
				valorFactura = Double.parseDouble(tarifaAmplia.toString());
			} if (idPaquete.compareTo(Paquete.LIMITADA) == 0) {
				valorFactura = Double.parseDouble(tarifaLimitada.toString());
			} else if (idPaquete.compareTo(Paquete.BASICA) == 0) {
				valorFactura = Double.parseDouble(tarifaBasica.toString());
			} else if (idPaquete.compareTo(Paquete.PLATA) == 0) {
				valorFactura = Double.parseDouble(tarifaPlata.toString());
			} else if (idPaquete.compareTo(Paquete.ORO) == 0) {
				valorFactura = Double.parseDouble(tarifaOro.toString());
			} else if (idPaquete.compareTo(Paquete.PLATINO) == 0) {
				valorFactura = Double.parseDouble(tarifaPlatino.toString());
		}
		}
		else{
			cotizacionMovil.setPorcentajeDescuento("0");
			tarifaAmplia = cotizacionMovil.getTarifaamplia();
			tarifaLimitada=cotizacionMovil.getTarifalimitada();
			tarifaBasica = cotizacionMovil.getTarifaBasica();
			tarifaPlata = cotizacionMovil.getTarifaPlata();
			tarifaOro = cotizacionMovil.getTarifaOro();
			tarifaPlatino = cotizacionMovil.getTarifaPlatino();
			LOG.info(".crearCotizacion() No se aplico  descuento de  cotizacion => " + cotizacion);
		}
		
		if(valorFactura <= 0) {
			throw new ApplicationException(eb.addFieldError("valorFactura",
			"debe ser mayor a 0"));
		}
		
		LOG.info(".crearCotizacion()  valorFactura=>"+valorFactura);
		LOG.info(".crearCotizacion()  tarifaLimitada=>"+tarifaLimitada);
		cotizacionMovil.setValorPrimaTotal(new BigDecimal(valorFactura
				.toString()));
		cotizacionMovil.setValorPrimaAmplia(tarifaAmplia);
		cotizacionMovil.setValorPrimaLimitada(tarifaLimitada);
		cotizacionMovil.setValorPrimaBasica(tarifaBasica);
		cotizacionMovil.setValorPrimaPlata(tarifaPlata);
		cotizacionMovil.setValorPrimaOro(tarifaOro);
		cotizacionMovil.setValorPrimaPlatino(tarifaPlatino);

		// Recalcula cotizacion
		calculoService.igualarPrima(idCotizacion, valorFactura, false);
		cotizacion = cotizacionService.descuentoGlobalCalculado(cotizacion);
		incisoCotizacion = calculoService.calcular(incisoCotizacion);
		cotizacionService.terminarCotizacion(incisoCotizacion.getId()
				.getIdToCotizacion(), false);
		calculoService.obtenerResumenGeneral(incisoCotizacion
				.getCotizacionDTO());
		return cotizacionMovil;

	}
	
		
	private IncisoCotizacionDTO guardaInciso(CotizacionDTO cotizacion,
			Long idNegocio, Long idPaquete, String estiloDescripcion,
			String modelo, String marca, String idEstadoCirculacion,
			String idMunicipioCirculacion, Double valorFactura,
			Double descuento, String nombreAsegurado, String apellidoPaterno,
			String apellidoMaterno) {
		
		BigDecimal idSeccion = idNegocio.longValue() == 92 ? new BigDecimal(
				1609) : new BigDecimal(1601);
		
		Integer estilo=null;
		descuento=null;
		IncisoCotizacionId id = new IncisoCotizacionId();
		id.setIdToCotizacion(cotizacion.getIdToCotizacion());
		String seccion = idNegocio.longValue() == 92 ? "AUTOPLAZO AUTOS"
				: "AUTOMOVILES INDIVIDUALES";
		
		Negocio negocio = negocioService.findById(idNegocio);
		
		if (descuento != null
				&& descuento.doubleValue() >= 0.0
				&& descuento.doubleValue() <= negocio
						.getDescuentoMaxOriginacion().doubleValue()) {
			cotizacion.setDescuentoOriginacion(descuento);
		} else {
			cotizacion.setDescuentoOriginacion(Double.valueOf(0d));
		}
		cotizacionFacadeRemote.update(cotizacion);
	
		NegocioProducto negocioProducto = negocioProductoDao
				.getNegocioProductoByIdNegocioIdProducto(
						negocio.getIdToNegocio(), new BigDecimal(191));

		NegocioTipoPoliza negocioTipoPoliza = negocioTipoPolizaService
				.getPorIdNegocioProductoIdToTipoPoliza(
						negocioProducto.getIdToNegProducto(), negocioProducto
								.getProductoDTO().getTiposPoliza().get(0)
								.getIdToTipoPoliza());
		negocioTipoPoliza.setNegocioProducto(negocioProducto);
		
		
		NegocioSeccion negocioSeccion = negocioSeccionService
		.getByProductoNegocioTipoPolizaSeccionDescripcion(
				negocioProducto.getProductoDTO(),negocio,
				negocioTipoPoliza.getTipoPolizaDTO(),seccion);
		NegocioPaqueteSeccion negocioPaqueteSeccion = negocioPaqueteSeccionDao
				.getPorIdNegSeccionIdPaquete(
						negocioSeccion.getIdToNegSeccion(),
						idPaquete != null ? idPaquete : Paquete.AMPLIA);
		
		NegocioAgrupadorTarifaSeccion negAgrupadorTarifaSeccion = negAgrupadorTarifaSeccionDao
				.buscarPorNegocioSeccionYMoneda(negocioSeccion, new BigDecimal(
						CODIGO_MONEDA));
		TarifaAgrupadorTarifa tarifaAgrupador = tarifaAgrupadorService
				.getPorNegocioAgrupadorTarifaSeccion(negAgrupadorTarifaSeccion);
		
		List<EstiloVehiculoDTO> estiloList = estiloVehiculoFacadeRemote
				.listarDistintosFiltrado(negocioSeccion.getSeccionDTO().getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien(), 
						new Integer(modelo),
						estiloDescripcion, 
						negocioSeccion.getIdToNegSeccion());
		for (EstiloVehiculoDTO estiloTmp : estiloList) {
			estilo = Integer.valueOf(estiloTmp.getId().getClaveEstilo());
		}
		if(estilo==null){
			estiloList = estiloVehiculoFacadeRemote
			.listarDistintosFiltrado(negocioSeccion.getSeccionDTO().getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien(), 
					new Integer(modelo),
					estiloDescripcion, 
					null);
		for (EstiloVehiculoDTO estiloTmp : estiloList) {
			estilo = Integer.valueOf(estiloTmp.getId().getClaveEstilo());
		}
		}

		LOG.info(".guardaInciso()  estilo =>"   +estilo);
		
		String claveEstilo = StringUtils.leftPad(Integer.toString(estilo), 5,
		"0");
		IncisoCotizacionDTO incisoCotizacion;
		IncisoAutoCot incisoAuto;
		if (cotizacion.getIncisoCotizacionDTOs() != null
				&& cotizacion.getIncisoCotizacionDTOs().size() > 0) {
			incisoCotizacion = cotizacion.getIncisoCotizacionDTOs().get(0);
			incisoAuto = incisoCotizacion.getIncisoAutoCot();
		} else {
			incisoCotizacion = new IncisoCotizacionDTO();
			incisoCotizacion.setId(id);
			incisoCotizacion.setCotizacionDTO(cotizacion);
			incisoCotizacion.setIdToSeccion(idSeccion);
			
			incisoAuto = new IncisoAutoCot();
		}
		
		// MarcaVehiculoDTO marcaVehiculo = marcaVehiculoFacade.findById(new
		// BigDecimal(marca.intValue()));
		MarcaVehiculoDTO marcaVehiculo = new MarcaVehiculoDTO() ;
		List<MarcaVehiculoDTO> marcaVehiculoList = marcaVehiculoFacade
				.findByProperty("descripcionMarcaVehiculo", marca);
		for (MarcaVehiculoDTO marcaVehiculoTmp : marcaVehiculoList) {
			if(marcaVehiculoTmp.getTipoBienAutosDTO().getClaveTipoBien().trim().equals("AUTOS"))
			marcaVehiculo=marcaVehiculoTmp;
		}
		/*
		 * //nombre conductor incisoAuto.setNombreConductor(nombreAsegurado);
		 * incisoAuto.setPaternoConductor(apellidoPaterno);
		 * incisoAuto.setMaternoConductor(apellidoMaterno);
		*/
		List<MunicipioDTO> municipioList = municipioFacadeRemote
				.findByProperty("stateId", idEstadoCirculacion);
		String MunicipioCirculacionID=municipioList.get(0).getId().toString();
		incisoAuto.setEstadoId(idEstadoCirculacion);
		incisoAuto.setMunicipioId(MunicipioCirculacionID);
		incisoAuto.setMarcaId(marcaVehiculo.getIdTcMarcaVehiculo());//
		incisoAuto.setDescripcionFinal(claveEstilo.concat(" - ").concat(
				estiloVehiculoService.getDescripcionPorClaveAmis(claveEstilo)));
		incisoAuto.setEstiloId(
				marcaVehiculo.getTipoBienAutosDTO().getClaveTipoBien().concat("_").concat(claveEstilo).concat("_")
				.concat(tarifaAgrupador.getId().getIdVertarifa().toString()));
		incisoAuto.setModeloVehiculo(new Short(modelo));// /////cambiar moddelo
														// 2014.0
		incisoAuto.setNegocioSeccionId(negocioSeccion.getIdToNegSeccion()
				.longValue());
		incisoAuto.setClaveTipoBien(marcaVehiculo.getTipoBienAutosDTO().getClaveTipoBien());
		incisoAuto.setNegocioPaqueteId(negocioPaqueteSeccion
				.getIdToNegPaqueteSeccion());// (long) 8924
		incisoAuto.setTipoUsoId((long) 9);// Tipo de Uso Normal
		incisoAuto.setAsociadaCotizacion(new Integer(1));
		incisoAuto
				.setObservacionesinciso("Endoso de BENEFICIARIO PREFERENTE: En caso de Siniestro indemnizable bajo el amparo de esta Poliza, este sera pagado preferentemente a:\n"
						+ "ARRENDADORA AFIRME, S.A. DE C.V. SOFOM ER AFIRME GRUPO FINANCIERO "
						+ "Hasta por el interes que le corresponda.\n\nPARA UNIDADES NUEVAS DENTRO DE SUS PRIMEROS 12 MESES DE USO LA SUMA ASEGURADA SER�? EL VALOR FACTURA.\n\n");
		incisoCotizacion.setIncisoAutoCot(incisoAuto);
		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
		estiloVehiculoId.valueOf(incisoAuto.getEstiloId());

		Long numeroSecuencia = new Long("1");
		
		List<CoberturaCotizacionDTO> coberturaCotizacionList = coberturaService
				.getCoberturas(negocioPaqueteSeccion.getNegocioSeccion()
						.getNegocioTipoPoliza().getNegocioProducto()
						.getNegocio().getIdToNegocio(), negocioPaqueteSeccion
						.getNegocioSeccion().getNegocioTipoPoliza()
						.getNegocioProducto().getProductoDTO()
						.getIdToProducto(), negocioPaqueteSeccion
						.getNegocioSeccion().getNegocioTipoPoliza()
						.getTipoPolizaDTO().getIdToTipoPoliza(),
						negocioPaqueteSeccion.getNegocioSeccion()
								.getSeccionDTO().getIdToSeccion(),
						negocioPaqueteSeccion.getPaquete().getId(),
						incisoCotizacion.getIncisoAutoCot().getEstadoId(),
						incisoCotizacion.getIncisoAutoCot().getMunicipioId(),
						incisoCotizacion.getId().getIdToCotizacion(),
						incisoCotizacion.getCotizacionDTO().getIdMoneda()
								.shortValue(), numeroSecuencia,
						estiloVehiculoId.getClaveEstilo(), incisoCotizacion
								.getIncisoAutoCot().getModeloVehiculo()
								.longValue(), new BigDecimal(incisoCotizacion
								.getIncisoAutoCot().getTipoUsoId()), null, null);

		for (CoberturaCotizacionDTO cobertura : coberturaCotizacionList) {
			cobertura
					.setValorPrimaNeta(cobertura.getValorPrimaNeta() == null ? 0D
							: cobertura.getValorPrimaNeta());
			cobertura
					.setValorSumaAsegurada(cobertura.getValorSumaAsegurada() == null ? 0D
							: cobertura.getValorSumaAsegurada());
			cobertura
					.setValorCoaseguro(cobertura.getValorCoaseguro() == null ? 0D
							: cobertura.getValorCoaseguro());
			cobertura
					.setValorDeducible(cobertura.getValorDeducible() == null ? 0D
							: cobertura.getValorDeducible());
			cobertura.setValorCuota(cobertura.getValorCuota() == null ? 0D
					: cobertura.getValorCuota());
			cobertura
					.setValorCuotaOriginal(cobertura.getValorCuotaOriginal() == null ? 0D
							: cobertura.getValorCuotaOriginal());
			cobertura
					.setValorCuotaOriginal(cobertura.getValorCuotaOriginal() == null ? 0D
							: cobertura.getValorCuotaOriginal());
			cobertura
					.setPorcentajeCoaseguro(cobertura.getPorcentajeCoaseguro() == null ? 0D
							: cobertura.getPorcentajeCoaseguro());
			cobertura
					.setPorcentajeDeducible(cobertura.getPorcentajeDeducible() == null ? 0D
							: cobertura.getPorcentajeDeducible());
			if (cobertura.getClaveAutCoaseguro().intValue() != 0)
				cobertura.setClaveAutCoaseguro((short)1);
			else
				cobertura.setClaveAutCoaseguro((short)0);
			if (cobertura.getClaveAutCoaseguro().intValue() != 0)
				cobertura.setClaveAutDeducible((short)1);
			else
				cobertura.setClaveAutDeducible((short)0);		
			if (cobertura
					.getId()
					.getIdToCobertura()
					.equals(CoberturaDTO.IDTOCOBERTURA_DANIOS_MATERIALES_VALOR_CONVENIDO)
					|| cobertura
							.getId()
							.getIdToCobertura()
							.equals(CoberturaDTO.IDTOCOBERTURA_ROBO_TOTAL_VALOR_CONVENIDO)) {
				cobertura.setValorSumaAsegurada(valorFactura);
			}			
		}
		incisoCotizacion = incisoService.prepareGuardarInciso(incisoCotizacion
				.getId().getIdToCotizacion(), incisoCotizacion
				.getIncisoAutoCot(), incisoCotizacion, coberturaCotizacionList);
													
		incisoCotizacion = calculoService.calcular(incisoCotizacion);
		
		cotizacionService.terminarCotizacion(incisoCotizacion.getId()
				.getIdToCotizacion(), false);
		calculoService.obtenerResumenGeneral(incisoCotizacion
				.getCotizacionDTO());
		return incisoCotizacion;
	}

	private void saveClienteCot(BigDecimal idToCotizacion,
			String nombreAsegurado, String apellidoPaterno,
			String apellidoMaterno, String RFC, String claveSexo,
			String claveEstadoCivil, String idEstadoNacimiento,
			Date fechaNacimiento, String codigoPostal, String idCiudad,
			String idEstado, String colonia, String calleNumero,
			  String telefonoCasa,String telefonoOficina,String email){
		
//		CARGA INFORMACION DE CLIENTE
		ClienteGenericoDTO cliente = new ClienteGenericoDTO();
		cliente.setApellidoPaterno(apellidoPaterno != null ? apellidoPaterno
				: "");
		cliente.setApellidoMaterno(apellidoMaterno != null ? apellidoMaterno
				: "");
		cliente.setNombre(nombreAsegurado != null ? nombreAsegurado : "");
		cliente.setNombreFiscal(cliente.getNombre());
		cliente.setApellidoPaternoFiscal(cliente.getApellidoPaterno());
		cliente.setApellidoMaternoFiscal(cliente.getApellidoMaterno());
		cliente.setNombreColonia(colonia != null ? colonia : "");
		cliente.setNombreColoniaFiscal(cliente.getNombreColonia());
		cliente.setNombreColoniaCobranza(cliente.getNombreColonia());
		cliente.setCodigoRFC(RFC != null ? RFC : "");
		cliente.setClaveNacionalidad("PAMEXI");
		cliente.setTipoSituacionString("A");
		cliente.setEstadoCivil(claveEstadoCivil);
		cliente.setEstadoNacimiento(idEstadoNacimiento);		
		cliente.setIdPaisString("PAMEXI");
		cliente.setIdEstadoString(idEstado != null ? idEstado : "");
		cliente.setIdMunicipioString(idCiudad != null ? idCiudad : "");
		cliente.setCodigoPostal(codigoPostal != null ? codigoPostal : "");
		cliente.setNombreCalle(calleNumero != null ? calleNumero : "");
		cliente.setIdEstadoCobranza(cliente.getIdEstadoString());
		cliente.setIdEstadoFiscal(cliente.getIdEstadoString());
		cliente.setIdMunicipioCobranza(cliente.getIdMunicipioString());
		cliente.setIdMunicipioFiscal(cliente.getIdMunicipioString());
		cliente.setCodigoPostalCobranza(cliente.getCodigoPostal());
		cliente.setCodigoPostalFiscal(cliente.getCodigoPostal());
		cliente.setClaveTipoPersona((short) 1);
		cliente.setEmail(email != null ? email : "");
		cliente.setEmailCobranza(cliente.getEmail());
		cliente.setNumeroTelefono(telefonoCasa);
		cliente.setTelefono(telefonoCasa);
		cliente.setTelefonoOficinaContacto(telefonoOficina);
		cliente.setSexo(claveSexo);
		cliente.setFechaNacimiento(fechaNacimiento != null ? fechaNacimiento
				: new Date());

		try {
			cliente = clienteFacade.saveFullData(cliente, null, false);
		} catch (Exception e) {
			LOG.error("Ocurrio un error en saveClienteCot " + e);
		}
		if (cliente != null) {
			//asociamos el cliente con la cotizacion
			cotizacionService.actualizarDatosContratanteCotizacion(
					idToCotizacion, cliente.getIdCliente(), null);
		}
	}
	
	public List<ResumenCotMovilDTO> inicializarDatosParaImpresion(
			SolicitudCotizacionMovil solicitud,
			CotizacionMovilDTO cotizacionMovil) {
		LOG.info(
				"finding inicializarDatosParaImpresion solicitud => "
				+solicitud);
		boolean aplicaDescuento = false;
		boolean descuentoAgente = false;
		boolean descuentoEstado = false;
		TarifasDTO tarifasDTO = entityManager.find(TarifasDTO.class,
				solicitud.getIdDescripcionEstilo());
		CotizacionDTO cotizacion = cotizacionFacadeRemote
				.findById(cotizacionMovil.getIdtocotizacionmidas());
		LOG.info("finding CotizacionDTO cotizacion => " + cotizacion);
		List<ResumenCotMovilDTO> resumenMovilList = new ArrayList<ResumenCotMovilDTO>();
		NegocioSeccion negocioSeccion =null;
		List<NegocioSeccion> negocioSeccionList = negocioSeccionService
				.getSeccionListByCotizacion(cotizacion);
			for(NegocioSeccion negocioSeccionTmp:negocioSeccionList){
				if(negocioSeccionTmp!=null)//cotizacion.getIncisoCotizacionDTOs().get(0).getIncisoAutoCot().getNegocioSeccionId()
				negocioSeccion=negocioSeccionTmp;
			}
			/*negocio seccion*/
		String seccion = ID_NEGOCIO.longValue() == 92 ? "AUTOPLAZO AUTOS"
				: "AUTOMOVILES INDIVIDUALES";
			
		Negocio negocio = negocioService.findById(ID_NEGOCIO);
			
		NegocioProducto negocioProducto = negocioProductoDao
				.getNegocioProductoByIdNegocioIdProducto(
						negocio.getIdToNegocio(), new BigDecimal(191));

			NegocioTipoPoliza negocioTipoPoliza = negocioTipoPolizaService
				.getPorIdNegocioProductoIdToTipoPoliza(
						negocioProducto.getIdToNegProducto(), negocioProducto
								.getProductoDTO().getTiposPoliza().get(0)
								.getIdToTipoPoliza());
			negocioTipoPoliza.setNegocioProducto(negocioProducto);
			
			negocioSeccion = negocioSeccionService
			.getByProductoNegocioTipoPolizaSeccionDescripcion(
					negocioProducto.getProductoDTO(),negocio,
					negocioTipoPoliza.getTipoPolizaDTO(),seccion);
			/**/
			
		List<NegocioFormaPago> negocioFormaPagos = entidadService
				.findByProperty(NegocioFormaPago.class,
						"negocioProducto.idToNegProducto", negocioSeccion
								.getNegocioTipoPoliza().getNegocioProducto()
								.getIdToNegProducto());
			
			List<NegocioPaqueteSeccion> negocioPaqueteSecciones = entidadService
				.findByProperty(NegocioPaqueteSeccion.class,
						"negocioSeccion.idToNegSeccion",
							negocioSeccion.getIdToNegSeccion());
			BigDecimal descuento=BigDecimal.ZERO;
			if(cotizacionMovil.getClaveagente()!=null){			
			if (cotizacionMovil.getDescuentoAgente().getClavepromo().trim()
					.equals("0")) {
				aplicaDescuento=false;/// no aplicar descuento
			} else {
				DescuentosAgenteDTO descuentosAgenteDTO=cotizacionMovil.getDescuentoAgente();
				if (descuentosAgenteDTO.getClavepromo().trim()
						.equals("0")) {
					LOG.info(".inicializarDatosParaImpresion... no aplica Descuento : "
									+ aplicaDescuento);
					aplicaDescuento=false;/// no aplicar descuento
				} else {
				/**/
					aplicaDescuento = true;
					LOG.info(".inicializarDatosParaImpresion.aplicaDescuento: "
									+ aplicaDescuento);
					if (((new BigDecimal(descuentosAgenteDTO
							.getPorcentaje()) == BigDecimal.ZERO) || descuentosAgenteDTO
							.getPorcentaje().trim().equals("0"))) {
						descuentoEstado = true;// aplicar descuento por estado
						descuentoAgente = false;
						LOG.info(this.getClass().getName()
										+ ".inicializarDatosParaImpresion.aplicaDescuentoEstado : "
										+ descuentoEstado);
					} else {
						descuentoEstado = false;
						descuentoAgente = true;
						LOG.info(".inicializarDatosParaImpresion.aplicaDescuentoAgente : "
										+ descuentoAgente);
						descuento = new BigDecimal(descuentosAgenteDTO // descuento
																				// por
																				// agente
						.getPorcentaje());
					}
				}
				
			}
		}
		for (NegocioPaqueteSeccion negocioPaqueteSeccion : negocioPaqueteSecciones) {//	amplio-limitada, cotizacion portal basico-plata-oro-platino		
				for (NegocioFormaPago negocioFormaPago : negocioFormaPagos) {//anual-mensual,semestral,contado
				if (negocioPaqueteSeccion.getPaquete().getDescripcion().toUpperCase().trim().equals("AMPLIA") ||
						negocioPaqueteSeccion.getPaquete().getDescripcion().toUpperCase().trim().equals("LIMITADA") ||
						(SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL.equals(solicitud.getTipoCotizacion()) && (negocioPaqueteSeccion.getPaquete().getDescripcion().toUpperCase().trim().equals("BASICO") ||
								negocioPaqueteSeccion.getPaquete().getDescripcion().toUpperCase().trim().equals("PLATA AFIRME") ||
								negocioPaqueteSeccion.getPaquete().getDescripcion().toUpperCase().trim().equals("ORO AFIRME") ||
								negocioPaqueteSeccion.getPaquete().getDescripcion().toUpperCase().trim().equals("PLATINO AFIRME")))) {
						FormaPagoIDTO formaPagoIDTO = null;
						BigDecimal primerRecibo=BigDecimal.ZERO;
						BigDecimal subsecuente=BigDecimal.ZERO;
						
						try {
						formaPagoIDTO = formaPagoInterfazServiciosRemote
								.getPorId(negocioFormaPago.getFormaPagoDTO()
										.getIdFormaPago(),
										(short) CODIGO_MONEDA, null);
						} catch (SystemException e) {
							LOG.error("Ocurrio un error en inicializarDatosParaImpresion " + e);
						}
						
					BigDecimal tarifa = BigDecimal.ZERO;
					if (negocioPaqueteSeccion.getPaquete().getDescripcion()
							.toUpperCase().trim().equals("AMPLIA")) {
						tarifa = tarifasDTO.getTarifaamplia();
					} if (negocioPaqueteSeccion.getPaquete().getDescripcion()
							.toUpperCase().trim().equals("LIMITADA")) {
						tarifa = tarifasDTO.getTarifalimitada();
					} else if (negocioPaqueteSeccion.getPaquete().getDescripcion()
							.toUpperCase().trim().equals("BASICO")) {
						tarifa = tarifasDTO.getTarifaBasica();
					} else if (negocioPaqueteSeccion.getPaquete().getDescripcion()
							.toUpperCase().trim().equals("PLATA AFIRME")) {
						tarifa = tarifasDTO.getTarifaPlata();
					} else if (negocioPaqueteSeccion.getPaquete().getDescripcion()
							.toUpperCase().trim().equals("ORO AFIRME")) {
						tarifa = tarifasDTO.getTarifaOro();
					} else if (negocioPaqueteSeccion.getPaquete().getDescripcion()
							.toUpperCase().trim().equals("PLATINO AFIRME")) {
						tarifa = tarifasDTO.getTarifaPlatino();
					}
					
						primerRecibo = getMontoPagoRecibo(formaPagoIDTO,
								cotizacionMovil, descuento, true,
							tarifa, aplicaDescuento,
								descuentoEstado, descuentoAgente);
						subsecuente = getMontoPagoRecibo(formaPagoIDTO,
								cotizacionMovil, descuento, false,
								tarifa,
								aplicaDescuento, descuentoEstado,
								descuentoAgente);
						
						DatosDesglosePagosMovil datosDesglosePagosMovil= new DatosDesglosePagosMovil();
						
					datosDesglosePagosMovil.setPrimerPago(Double
							.parseDouble(obtenerFormatoNumero(primerRecibo)));
					datosDesglosePagosMovil.setPagosSubsecuentes(Double
							.parseDouble(obtenerFormatoNumero(subsecuente)));
					datosDesglosePagosMovil.setFormaPago(negocioFormaPago
							.getFormaPagoDTO().getDescripcion());
					datosDesglosePagosMovil.setMontoIVA(cotizacion
							.getMontoIVA());
					if(SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL.equals(solicitud.getTipoCotizacion())) {
							datosDesglosePagosMovil.setPrimaNetaTotal(tarifa.doubleValue());
					} else {
						datosDesglosePagosMovil.setPrimaNetaTotal(tarifasDTO.getTarifaamplia().doubleValue());
					}
					datosDesglosePagosMovil.setPrimaTotal(Double.parseDouble(obtenerFormatoNumero(primerRecibo)));
					datosDesglosePagosMovil.setDescripcionPaquete(negocioPaqueteSeccion.getPaquete().getDescripcion());
							
						ResumenCotMovilDTO resumenCotMovilDTO = new ResumenCotMovilDTO();
					resumenCotMovilDTO.setPaquete(negocioPaqueteSeccion.getPaquete());
					resumenCotMovilDTO.setFormaPago(negocioFormaPago.getFormaPagoDTO());
						resumenCotMovilDTO.setCotizacionMovil(cotizacionMovil);		
						resumenCotMovilDTO.setFormaPagoIDTO(formaPagoIDTO);
					resumenCotMovilDTO.setDatosDesglosePagosMovil(datosDesglosePagosMovil);
						resumenMovilList.add(resumenCotMovilDTO);
					}
				}
			}
			return resumenMovilList;	
	}	
	@EJB
	NegocioEstadoDescuentoDao NegocioEstadoDescuentoDao;

		public BigDecimal getMontoPagoRecibo(FormaPagoIDTO formaPagoIDTO,
			CotizacionMovilDTO cotizacionMovil, BigDecimal descuento,
			boolean conIva, BigDecimal  tarifa,boolean aplicaDescuento,boolean aplicaDescuentoPorEstado,boolean aplicaDescuentoPorAgente) {
		CotizacionDTO cotizacion = cotizacionFacadeRemote
				.findById(cotizacionMovil.getIdtocotizacionmidas());
		LOG.info("finding getMontoPagoRecibo CotizacionDTO cotizacion => " + cotizacion);
		BigDecimal porcentajeRecargoPagoFraccionado = BigDecimal.ZERO;
		BigDecimal monto = BigDecimal.ZERO;
		Integer numRecibos = 0;
		if(aplicaDescuento==true){//if descuento==0
			LOG.info("aplicando descuento cotizacion => " + cotizacion);
			if (aplicaDescuentoPorEstado==true) {

				descuento = new BigDecimal(NegocioEstadoDescuentoDao
						.findByNegocioAndEstado(
								ID_NEGOCIO,
								cotizacion.getIncisoCotizacionDTOs().get(0)
										.getIncisoAutoCot().getEstadoId())
						.getPctDescuento().toString());
				if (descuento == BigDecimal.ZERO || descuento == null) {
					descuento = BigDecimal.ONE;
				}
				descuento = BigDecimal.ONE.subtract(
						descuento.divide(new BigDecimal(100)),
						MathContext.DECIMAL128);
				
				LOG.info("aplicando descuento por estado getMontoPagoRecibo descuento  => " + descuento);
			} else if(aplicaDescuentoPorAgente==true) {
				descuento = BigDecimal.ONE.subtract(
						descuento.divide(new BigDecimal(100)),
						MathContext.DECIMAL128);
				descuento = roundBigDecimal(descuento);
				LOG.info("aplicando descuento por agente getMontoPagoRecibo descuento  => " + descuento);
			}
		}
		else{
			if (descuento == BigDecimal.ZERO || descuento == null) {
				descuento = BigDecimal.ONE;
			}
			LOG.info("no aplica descuento cotizacion => " + cotizacion);
		}


		if (formaPagoIDTO != null && cotizacionMovil.getClaveagente() != null) {
			porcentajeRecargoPagoFraccionado = formaPagoIDTO
					.getPorcentajeRecargoPagoFraccionado();
			numRecibos = formaPagoIDTO.getNumeroRecibosGenerados();
		}
		if (numRecibos == null || numRecibos == 0) {
			numRecibos = 1;
		}

		BigDecimal iva = new BigDecimal(cotizacion.getValorDerechosUsuario())
				.multiply(new BigDecimal(1.16));
		BigDecimal op1 = tarifa.subtract(iva);
		BigDecimal op2 = new BigDecimal("1")
				.add(porcentajeRecargoPagoFraccionado
						.divide(new BigDecimal(100)));
		BigDecimal op3 = op1.divide(new BigDecimal(String.valueOf(numRecibos)),
				MathContext.DECIMAL128);
		BigDecimal op4 = op3.multiply(descuento, MathContext.DECIMAL128);
		BigDecimal op5 = op4.multiply(op2, MathContext.DECIMAL128);
		BigDecimal op6 = op5.add(iva);
		if (conIva == true)
			monto = op6;
		else
			monto = op5;
		if (formaPagoIDTO != null) {
			if (formaPagoIDTO.getDescripcion().contains("ANUAL")
					&& conIva == false) {
				monto = BigDecimal.ZERO;
			}
		}

		return monto;
	}
	public ValidacionClavePromo validarClavePromo(ValidacionClavePromo param) {
		LOG.info("validando  ... ClavePromo instance : " + param.getClavePromo());
		DescuentosAgenteDTO descuentosAgenteDTO = new DescuentosAgenteDTO();
		ErrorBuilder eb = new ErrorBuilder();
		try{
			if(!param.getClavePromo().isEmpty()||param.getClavePromo()!=null){
				descuentosAgenteDTO.setClavepromo(param.getClavePromo());
				if (descuentoAgenteService.existeDescuentoAgente(descuentosAgenteDTO)==false){
					param.setEsValido(false);
					throw new ApplicationException(eb.addFieldError("clavePromo",
							"No Existe"));
				}else{
					param.setEsValido(true);
				}
			}else{
				throw new ApplicationException(eb.addFieldError("clavePromo",
				"La clave promo esta vacia"));
			}
		
		} catch (RuntimeException re) {
			LOG.error("error ClavePromo instance",re);
			throw re;
		}
		return param;
	}
	public static BigDecimal roundBigDecimal(final BigDecimal input){
		return input.round(new MathContext(input.toBigInteger().toString()
				.length(), RoundingMode.HALF_UP));
	} 
	public String redondearBigDecimal(BigDecimal numero){
	    String result;
		NumberFormat formateador = new DecimalFormat("#,##0.00");
		Double num= Double.parseDouble(numero.toString());
		result=formateador.format(num);
		result.replace(".", "");
		result.replace(",", ".");
		return result;
	}
	public String obtenerFormatoNumero(BigDecimal cantidad){
		Locale locale  = new Locale("es", "MX");
		String pattern = "###.##";
		DecimalFormat decimalFormat = (DecimalFormat) NumberFormat
				.getNumberInstance(locale);
		decimalFormat.applyPattern(pattern); 

		double amount = Double.parseDouble(String.valueOf(cantidad));
		return decimalFormat.format(amount);
	}
	public List<CotizacionMovilDTO> findCotizacionByFilters(CotizacionMovilDTO filter, String tipoCotizacion){
		LOG.info("findByFilters  CotizacionMovilDTO instances");
		List<CotizacionMovilDTO>  lista=new ArrayList<CotizacionMovilDTO>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		try{
			queryString.append(" select entidad.IDTOCOTIZACIONMOVIL, entidad.GRUPOVEHICULOS, entidad.MODELOVEHICULO, entidad.TIPOVEHICULO, entidad.MARCAVEHICULO, entidad.ESTADO, entidad.DESCRIPCIONTARIFA, entidad.OC, entidad.TARIFAAMPLIA, entidad.TARIFALIMITADA, entidad.CLAVETARIFA, entidad.CLAVEAGENTE, entidad.CLAVEPROMO, entidad.IDTOCOTIZACIONMIDAS, entidad.IDTOCOTIZACIONSEYCOS, entidad.FECHAREGISTRO, entidad.IDUSUARIO, entidad.IDTCDESCUENTOSAGENTE, entidad.FECHANACIMIENTO, entidad.SA, entidad.SEXO, entidad.VALORPRIMATOTAL, entidad.VALORPRIMALIMITADA, entidad.VALORPRIMAAPMPLIA ,	entidad.CLAVENEGOCIO ,	entidad.PESO ,	entidad.ESTATURA ,	entidad.PORCENTAJEDESCUENTO,entidad.TIPODESCUENTO, entidad.IDGIROOCUPACION, UPPER(entidad.NOMBREPROSPECTO) as NOMBREPROSPECTO, UPPER(entidad.EMAILPROSPECTO) as EMAILPROSPECTO, entidad.TELEFONOPROSPECTO, entidad.ESTATUSPROSPECTO, entidad.UUID, UPPER(entidad.OSMOVIL) as OSMOVIL, entidad.GRUPOVEHICULOS, ");
			queryString.append(" entidad.tarifaBasica, entidad.tarifaPlata, entidad.tarifaPlatino, entidad.tarifaOro ");
			queryString.append(" from MIDAS.TOCOTIZACIONMOVIL entidad ");
			queryString.append(" where ");
			if(isNotNull(filter)){
				int index=1;
				if(isNotNull(filter.getIdtocotizacionmidas())){
					addCondition(queryString, " entidad.idtocotizacionmidas=? ");
					params.put(index, filter.getIdtocotizacionmidas());
					index++;
				}
				if(isNotNull(filter.getIdtocotizacionmovil())){
					addCondition(queryString, " entidad.idtocotizacionmovil =? ");
					params.put(index, filter.getIdtocotizacionmovil());
					index++;
				}
				if(isNotNull(filter.getIdtocotizacionseycos())){
					addCondition(queryString, " entidad.idtocotizacionseycos=? ");
					params.put(index, filter.getIdtocotizacionseycos());
					index++;
				}
				if(isNotNull(filter.getEstado())){
					addCondition(queryString, " entidad.estado=? ");
					params.put(index, filter.getClaveagente());
					index++;
				}
				if(isNotNull(filter.getClaveNegocio())){
					addCondition(queryString, " entidad.clavenegocio=? ");
					params.put(index, filter.getClaveNegocio());
					index++;
				}
				if(isNotNull(filter.getUserId())){
					addCondition(queryString, " entidad.idusuario=? ");
					params.put(index, filter.getUserId());
					index++;
				}
				if(isNotNull(filter.getClaveagente())){
					addCondition(queryString, " entidad.claveagente=? ");
					params.put(index, filter.getClaveagente());
					index++;
				}
				if(isNotNull(filter.getClavepromo())){
					addCondition(queryString, " entidad.clavepromo=? ");
					params.put(index, filter.getClaveagente());
					index++;
				}
				if(isNotNull(filter.getEmailProspecto())){
					addCondition(queryString, " UPPER(entidad.emailprospecto) LIKE ? ");
					params.put(index, "%" + filter.getEmailProspecto().toUpperCase() + "%");
					index++;
				}
				if(isNotNull(filter.getTelefonoProspecto())){
					addCondition(queryString, " UPPER(entidad.telefonoprospecto) LIKE ? ");
					params.put(index, "%" + filter.getTelefonoProspecto().toUpperCase() + "%");
					index++;
				}
				if(isNotNull(filter.getEstatusProspecto())){
					addCondition(queryString, " entidad.estatusprospecto =? ");
					params.put(index, filter.getEstatusProspecto());
					index++;
				}
				if(isNotNull(tipoCotizacion)){
					if(SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL.equals(tipoCotizacion)) {
						addCondition(queryString, " entidad.tipoCotizacion =? ");
					} else {
						addCondition(queryString, " (entidad.tipoCotizacion is null or entidad.tipoCotizacion =?) ");
					}
					params.put(index, tipoCotizacion);
					index++;
				}
				if(params.isEmpty()){
					int lengthWhere="where ".length();
					queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
				}
				String finalQuery=getQueryString(queryString)+" order by entidad.idtocotizacionmovil desc ";
				Query query=entityManager.createNativeQuery(finalQuery,CotizacionMovilDTO.class);
				if(!params.isEmpty()){
					for(Integer key:params.keySet()){
						query.setParameter(key,params.get(key));
					}
				}
				query.setMaxResults(100);
				lista=query.getResultList();
			}
		
		} catch (RuntimeException re) {
			LOG.error("findByFilters CotizacionMovilDTO all failed",re);
			throw re;
		}
		return lista;
		
	}
	public List<AdministracionUsuarioMovil> findAdministracionUsuarioByFilters(AdministracionUsuarioMovil filter){
		LOG.info(">>>findByFilters  AdministracionUsuarioMovil instances");
		List<AdministracionUsuarioMovil>  lista=new ArrayList<AdministracionUsuarioMovil>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		try{
			queryString.append("  select distinct   idusuario  as idusuario,estatusprospecto as bajalogica,count(idtocotizacionmovil)  as numeroCotizaciones ");
			queryString.append(" from MIDAS.TOCOTIZACIONMOVIL entidad");
			queryString.append(" where ");
			if(isNotNull(filter)){
				int index=1;
				if(isNotNull(filter.getIdusuario())){
					addCondition(queryString, " entidad.idusuario =? ");
					params.put(index, filter.getIdusuario());
					index++;
				}
				if(isNotNull(filter.getEmail())){
					queryString.append("UPPER(entidad.emailprospecto) LIKE UPPER('%");
					queryString.append(filter.getEmail()); 
					queryString.append("%') ");
					queryString.append(" and ");
					index++;
				}
				if(isNotNull(filter.getNumeroTelefono())){
					queryString.append("UPPER(entidad.telefonoprospecto) LIKE UPPER('%");
					queryString.append(filter.getNumeroTelefono()); 
					queryString.append("%') ");
					queryString.append(" ");
					index++;
				}
				if(isNotNull(filter.getBajalogica())){
					addCondition(queryString, " entidad.estatusprospecto =? ");
					params.put(index, filter.getBajalogica());
					index++;
				}
				if(params.isEmpty()){
					int lengthWhere="where ".length();
					queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
				}
				
				String finalQuery=getQueryString(queryString)+"  group by entidad.idusuario,entidad.estatusprospecto,entidad.estatusprospecto order by numeroCotizaciones desc ";
				Query query=entityManager.createNativeQuery(finalQuery);
				if(!params.isEmpty()){
					for(Integer key:params.keySet()){
						query.setParameter(key,params.get(key));
					}
				}
				query.setMaxResults(100);
				//lista=query.getResultList();
				List<Object[]> listaObj = query.getResultList();
				for (Object[] object : listaObj) {
					AdministracionUsuarioMovil user = new AdministracionUsuarioMovil();
					user.setIdusuario(new Long(((BigDecimal) object[0]).toString()));
					Usuario usuario=usuarioService.buscarUsuarioPorId(new Integer(user.getIdusuario().toString()));
					user.setEmail(usuario.getEmail());
					user.setNombreCompleto(regresaObjectEsNuloParametro(usuario.getNombreCompleto()," ").toString());
					user.setNumeroCotizaciones(new Long(((BigDecimal) object[2]).toString()));
				    if(usuario.isActivo()){
				    	user.setBajalogica(new Short("1"));	
				    }
				    else{
				    	user.setBajalogica(new Short("0"));	
				    }
					
					user.setNumeroTelefono(usuario.getTelefonoCelular());
					lista.add(user);
				}
				
			}
		
		} catch (RuntimeException re) {
			LOG.error("findByFilters AdministracionUsuarioMovil  failed", re);
			throw re;
		}
		return lista;
	}
	public String actualizar(AdministracionUsuarioMovil filter){
		LOG.info(">>>.actualizando ...  AdministracionUsuarioMovil instances");
		final StringBuilder queryString=new StringBuilder("");
		Long idUser=filter.getIdusuario();
		String result;
		boolean estatus;
		try{
			if(idUser!=null){

				if(filter.getBajalogica()==new Short("1")||filter.getBajalogica().equals(new Short("1"))){
					estatus=true;
				}
				else{
					estatus=false;
				}
				usuarioService.activateDeactivateUser(new Integer(idUser.toString()), estatus, usuarioService.getUsuarioActual().getId());
				queryString.append(" UPDATE  MIDAS.TOCOTIZACIONMOVIL entidad ");
				queryString.append(" SET entidad.ESTATUSPROSPECTO=");
				queryString.append(filter.getBajalogica());
				queryString.append(" where ");
				queryString.append(" entidad.IDUSUARIO=");
				queryString.append(idUser);
					String finalQuery=getQueryString(queryString);
					Query query=entityManager.createNativeQuery(finalQuery);
					query.executeUpdate();
					
					LOG.info(".terimando actualizacion ...  AdministracionUsuarioMovil instances estatus="+estatus);
			}
			else{
				result="Debe seleccionar un Usuario";
			}
			result="Actualizacion Exitosa";
			
		} catch (RuntimeException re) {
			result=re.toString();
			LOG.error("actualizacion fallida AdministracionUsuarioMovil ", re);
			throw re;
		}
		return result;
	}
	public static Object regresaObjectEsNuloParametro(Object parametro,
			Object retorno) {
		if (parametro == null)
			return retorno;
		else
			return parametro;
	}
	public ValidacionEstatusUsuarioActual validarEstatusUsuarioActual(SolicitudCotizacionMovil solicitud){
		Usuario usuario = usuarioService.getUsuarioActual();
		ValidacionEstatusUsuarioActual validacionUsuario= new ValidacionEstatusUsuarioActual();
		try{
			if(usuario!=null){
				validacionUsuario.setActivo(usuario.isActivo());
				if(usuario.isActivo()){
					validacionUsuario.setActivo(true);
					validacionUsuario.setMensaje("Usuario activo");
				}else{
					validacionUsuario.setMensaje("Su usuario esta desactivado, favor de comunicarse con la compañia.");
				}
			}else{
				if(solicitud.getClaveNegocio().equals("A")){
					String cantidad= getCantidadMaximaCotizacionesAuto();
					LOG.info("getCantidadMaximaCotizacionesAuto()"+Integer.parseInt(cantidad));
					LOG.info("GetUUID--> "+solicitud.getUuid());
					BigDecimal numero = getNumeroCotizacion(solicitud.getUuid(),solicitud.getClaveNegocio());
					LOG.info("getNumeroCotizacion--> "+numero.intValue());
					if(numero.intValue() > Integer.parseInt(cantidad)){
						validacionUsuario.setMensaje("Usted ya no puede seguir cotizando");
					}else{
						validacionUsuario.setActivo(true);
						validacionUsuario.setMensaje("Usuario Activo");
					}
				}
				if(solicitud.getClaveNegocio().equals("V")){
					String cantidad= getCantidadMaximaCotizacionesVida();
					BigDecimal numero = getNumeroCotizacion(solicitud.getUuid(),solicitud.getClaveNegocio());
					if(numero.intValue() > Integer.parseInt(cantidad)){
						validacionUsuario.setMensaje("Usted ya no puede seguir cotizando");
					}else{
						validacionUsuario.setActivo(true);
						validacionUsuario.setMensaje("Usuario Activo");
					}
				}
				if(solicitud.getClaveNegocio().equals("C")){
					String cantidad= getCantidadMaximaCotizacionesCasa();
					BigDecimal numero = getNumeroCotizacion(solicitud.getUuid(),solicitud.getClaveNegocio());
					if(numero.intValue() > Integer.parseInt(cantidad)){
						validacionUsuario.setMensaje("Usted ya no puede seguir cotizando");
					}else{
						validacionUsuario.setActivo(true);
						validacionUsuario.setMensaje("Usuario Activo");
					}
				}
				if(solicitud.getClaveNegocio().equals("P")){
					String cantidad= getCantidadMaximaCotizacionesPyme();
					BigDecimal numero = getNumeroCotizacion(solicitud.getUuid(),solicitud.getClaveNegocio());
					if(numero.intValue() > Integer.parseInt(cantidad)){
						validacionUsuario.setMensaje("Usted ya no puede seguir cotizando");
					}else{
						validacionUsuario.setActivo(true);
						validacionUsuario.setMensaje("Usuario Activo");
					}
				}
			}
		}catch (Exception re) {
			LOG.error("Ocurrio Un Error validarEstatusUsuarioActual",re);
		}
		return validacionUsuario;
	}
	
	
	public boolean existeEstado(String estado){
		List<EstadoDTO> estadoList = estadoFacade.findByProperty(
				"stateName", estado.trim());
		try{
			if(estadoList.get(0).getStateId()!=null){
				return true;
			}
			else{
				return false;
			}
		}
		catch(Exception e){
			return false;
		}
	}

	public boolean existeEstiloMarcaVehiculo(TarifasDTO tarifa){
		LOG.info(">>>finding  existeEstiloMarcaVehiculo  MARCAVEHICULO:"+tarifa.getMarcavehiculo()
				+";DESCRIPCIONTARIFA:"+tarifa.getDescripciontarifa());
		final StringBuilder sb = new StringBuilder("");
		boolean existe=false;
		try {
			sb.append(" SELECT distinct  m.* FROM midas.tcmarcavehiculo m ");
			sb.append(" INNER JOIN MIDAS.tcestilovehiculo es on (es.clavetipobien = m.clavetipobien and es.idtcmarcavehiculo=m.idtcmarcavehiculo) ");
			sb.append( "where ".concat(queryUpperTrim("es.descripcionestilo")).concat("= ?1 "));
			sb.append( "and ".concat(queryUpperTrim("m.descripcionmarcavehiculo")).concat("= ?2 "));
			
			Query query = entityManager.createNativeQuery(sb.toString(),MarcaVehiculoDTO.class);	
			query.setParameter(1, tarifa.getDescripciontarifa().trim().toUpperCase());
			query.setParameter(2, tarifa.getMarcavehiculo().trim().toUpperCase());
			List<MarcaVehiculoDTO> listaObj = query.getResultList();
			
			LOG.info("size=>"+listaObj.size());
			if(listaObj.get(0)!=null){
				existe=true;
				LOG.info("existeEstiloMarcaVehiculo  :"+"["+existe+"]");
			}
		} catch (RuntimeException re) {
			LOG.error("existeEstiloMarcaVehiculo Exception=",re);
			LOG.error("existeEstiloMarcaVehiculo  :"+"["+existe+"]", re);
			
			return existe;
		}
		return existe;
	}
	public EstiloVehiculoDTO getEstiloVehiculo(TarifasDTO tarifa){
		LOG.info("getEstiloVehiculo()  CLAVETARIFA:"+tarifa.getClavetarifa()+ "marca:"+tarifa.getMarcavehiculo());
		final StringBuilder sb = new StringBuilder("");
		EstiloVehiculoDTO estilo= null;
		try {
			sb.append(" SELECT distinct  es.* FROM midas.tcmarcavehiculo m ");
			sb.append(" INNER JOIN midas.tcestilovehiculo es on (es.clavetipobien = m.clavetipobien and es.idtcmarcavehiculo=m.idtcmarcavehiculo) ");
			sb.append(" where es.claveestilo=?1 ");
			sb.append(" and m.DESCRIPCIONMARCAVEHICULO=?2 ");
	
			Query query = entityManager.createNativeQuery(sb.toString(),EstiloVehiculoDTO.class);	
			query.setParameter(1, tarifa.getClavetarifa());	
			query.setParameter(2, tarifa.getMarcavehiculo());	
			List<EstiloVehiculoDTO> listaObj = query.getResultList();			
			LOG.info("size=>"+listaObj.size());
			if(!listaObj.isEmpty()){
				for(EstiloVehiculoDTO object:listaObj){
					if(object!=null){
						estilo =new EstiloVehiculoDTO();
						estilo=object;
						break;
					}
				}
			}
			else{
				estilo =new EstiloVehiculoDTO();
				estilo=getEstiloVehiculoPorDescripcion(tarifa);
			}
			
		} catch (RuntimeException re) {
			LOG.error("getEstiloVehiculo Exception=",re);
			estilo=null;
		}
		return estilo;
	}
	private EstiloVehiculoDTO getEstiloVehiculoPorDescripcion(TarifasDTO tarifa){
		LOG.info("getEstiloVehiculo()  marca:"+tarifa.getMarcavehiculo() +";descripcionEstilo:"+tarifa.getDescripciontarifa());
		final StringBuilder sb = new StringBuilder("");
		EstiloVehiculoDTO estilo= null;
		try {
			sb.append(" SELECT distinct  es.* FROM midas.tcmarcavehiculo m ");
			sb.append(" INNER JOIN midas.tcestilovehiculo es on (es.clavetipobien = m.clavetipobien and es.idtcmarcavehiculo=m.idtcmarcavehiculo) ");
			sb.append(" where es.DESCRIPCIONESTILO=?1 ");
			sb.append(" and m.DESCRIPCIONMARCAVEHICULO=?2 ");
			Query query = entityManager.createNativeQuery(sb.toString(),EstiloVehiculoDTO.class);	
			query.setParameter(1, tarifa.getDescripciontarifa());
			query.setParameter(2, tarifa.getMarcavehiculo());
			List<EstiloVehiculoDTO> listaObj = query.getResultList();			
			LOG.info("size=>"+listaObj.size());
			for(EstiloVehiculoDTO object:listaObj){
				if(object!=null){
					estilo =new EstiloVehiculoDTO();
					estilo=object;
					break;
				}
			}
		} catch (RuntimeException re) {
			LOG.error("getEstiloVehiculo Exception=",re);
			estilo=null;
		}
		return estilo;
	}
	public  boolean isNotEmpty(Object obj){
		if(obj!=null){
			if(obj.toString().trim().equals(""))
				return false;
			else
				return true;
		}
		else
			return false;
		    
	}
	public List<TarifasDTO> existeTarifa(TarifasDTO tarifa){
		LOG.info(">>>finding  existeTarifa[MIDAS.TCTARIFAS] MARCAVEHICULO:"+tarifa.getMarcavehiculo()
				+";TIPOVEHICULO:"+tarifa.getTipovehiculo()
				+";DESCRIPCIONTARIFA:"+tarifa.getDescripciontarifa()
				+";MODELOVEHICULO:"+tarifa.getModelovehiculo()
				+";ESTADO:"+tarifa.getEstado());
		List<TarifasDTO> listaObj=null;
		try {
			final StringBuilder sb = new StringBuilder(200);
			sb.append(" select * from midas.TCTARIFAS ");
			sb.append( "where ".concat(queryUpperTrim("MARCAVEHICULO")).concat("= ?1 "));
			sb.append( "and ".concat(queryUpperTrim("TIPOVEHICULO")).concat("= ?2 "));
			sb.append( "and ".concat(queryUpperTrim("DESCRIPCIONTARIFA")).concat("= ?3 "));
			sb.append( "and ".concat(queryUpperTrim("MODELOVEHICULO")).concat("= ?4 "));
			sb.append( "and ".concat(queryUpperTrim("ESTADO")).concat("= ?5 "));
			
			Query query = entityManager.createNativeQuery(sb.toString(),TarifasDTO.class);
			query.setParameter(1, tarifa.getMarcavehiculo().trim().toUpperCase());
			query.setParameter(2, tarifa.getTipovehiculo().trim().toUpperCase());
			query.setParameter(3, tarifa.getDescripciontarifa().trim().toUpperCase());	
			query.setParameter(4, tarifa.getModelovehiculo());	
			query.setParameter(5, tarifa.getEstado().trim().toUpperCase());	
			listaObj = query.getResultList();
			//LOG.info("query=>"+sb.toString());
			LOG.info("size=>"+listaObj.size());
		} catch (RuntimeException re) {
			LOG.error("find existeTarifa failed",re);
			return null;		
		}
		return listaObj;
	}
	private String queryUpperTrim(String field){
		return " UPPER(TRIM( ".concat(field).concat(" )) ");
	}
	public  String desactivarTarifaEstados (CatalogoEstatusActualizacion param){
		LOG.info(">>>.desactivarTarifaEstados ...   instances");
		final StringBuilder queryString=new StringBuilder("");
		String result=null;
		boolean estatus;
		try {
			queryString.append(" UPDATE  MIDAS.TCTARIFAS entidad ");
			queryString.append(" SET entidad.BAJALOGICA=?1");
			queryString.append(" where ");
			queryString.append(" entidad.ESTADO=?2");
			String finalQuery = getQueryString(queryString);
			
			Query query = entityManager.createNativeQuery(finalQuery);
			query.setParameter(1, param.getBajaLogica());
			query.setParameter(2, param.getDescripcion());
			query.executeUpdate();
			LOG.info(".terimando desactivarTarifaEstados .. instances estatus=");
		} catch (RuntimeException re) {
			result = re.toString();
			LOG.error("actualizacion fallida desactivarTarifaEstados ", re);
			throw re;
		}
		return result;
	}
	
	public  String desactivarTarifasMarcas (CatalogoEstatusActualizacion param){
		LOG.info(">>>.desactivarTarifasMarcas ... instances");
		final StringBuilder queryString=new StringBuilder("");
		String result=null;
		//boolean estatus;
		try {
			queryString.append(" UPDATE  MIDAS.TCTARIFAS entidad ");
			queryString.append(" SET entidad.BAJALOGICA=?1");
			queryString.append(" where ");
			queryString.append(" entidad.MARCAVEHICULO=?2");
			String finalQuery = getQueryString(queryString);			
			Query query = entityManager.createNativeQuery(finalQuery);
			query.setParameter(1, param.getBajaLogica());
			query.setParameter(2, param.getDescripcion());
			query.executeUpdate();	
			LOG.info(".terimando desactivarTarifasMarcas .. instances estatus=");
		} catch (RuntimeException re) {
			result = re.toString();
			LOG.error("actualizacion fallida desactivarTarifas ", re);
			throw re;
		}
		return result;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String actualizarVistaMaterializada() {
		Map<String, Object> execute = this.actualizarVistaMaterializada
				.execute();
		BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
		String pDesc_Codigo = (String) execute.get("pDesc_Codigo");
		LOG.info("pDesc_Codigo"+pDesc_Codigo);
		LOG.info("idCodResp"+idCodResp);
		return pDesc_Codigo;
	}

	public List<CatalogoEstatusActualizacion> obtenerCatalogoEstados() {
		LOG.info(">>>finding all obtenerCatalogoEstados instances");
		List<CatalogoEstatusActualizacion> estadoList = new ArrayList<CatalogoEstatusActualizacion>();
		Query query =null;
		final StringBuilder queryString=new StringBuilder("");
		try {
			queryString.append("select  distinct max(model.idtctarifas) as id ,model.estado from TarifasDTO model ");
			queryString.append(" group by model.estado ");
			queryString.append(" order by model.estado ");
			query = entityManager.createQuery(queryString.toString());
			List<Object[]> listaObj = query.getResultList();
			for(Object[] object : listaObj){
				CatalogoEstatusActualizacion estado = new CatalogoEstatusActualizacion();
				estado.setDescripcion( (String)object[1]);
				estadoList.add(estado);
			}
			return estadoList;
		} catch (RuntimeException re) {
			LOG.error("find all obtenerCatalogoEstados failed", re);
			throw re;
		}
	}

	public List<CatalogoEstatusActualizacion> obtenerCatalogoMarcas() {
		LOG.info(">>>finding all obtenerCatalogoMarcas instances");
		List<CatalogoEstatusActualizacion> estadoList = new ArrayList<CatalogoEstatusActualizacion>();
		Query query =null;
		final StringBuilder queryString=new StringBuilder("");
		try {
			queryString.append( "select  distinct max(model.idtctarifas) as id ,model.marcavehiculo from TarifasDTO model ");
			queryString.append(" group by model.marcavehiculo ");
			queryString.append(" order by model.marcavehiculo ");
			query = entityManager.createQuery(queryString.toString());
			List<Object[]> listaObj = query.getResultList();
			for(Object[] object : listaObj){
				CatalogoEstatusActualizacion estado = new CatalogoEstatusActualizacion();
				estado.setDescripcion( (String)object[1]);
				estadoList.add(estado);
			}
			return estadoList;
		} catch (RuntimeException re) {
			LOG.error("find all obtenerCatalogoMarcas failed", re);
			throw re;
		}
	}
	private  ParametroGeneralId getParametroGeneralId(BigDecimal codigoParametro){
		 ParametroGeneralId id = new ParametroGeneralId();
		 id.setIdToGrupoParametroGeneral(GRUPO_COTIZACION_MOVIL);
	     id.setCodigoParametroGeneral(codigoParametro);
	     return id;
	}
	private String getValor(BigDecimal codigoParametro){
		 final ParametroGeneralDTO parametro = parametroGeneralFacade.findById(getParametroGeneralId(codigoParametro));
		 return parametro.getValor();
	}
	public String getCantidadMaximaCargaMasivaTarifas (){
		return getValor(CANTIDAD_MAXIMA_CARGA_MASIVA_TARIFAS);
	}
	public boolean isUserValid(Usuario usuario, boolean usuarioPortal) {
		if(usuarioPortal) {
			return true;
		}
		return usuario != null 
				&& isValid(usuario.getEmail())
				&& isValid(usuario.getNombre())
				&& isValid(usuario.getApellidoPaterno())
				&& isValid(usuario.getApellidoMaterno())
				&& isValid(usuario.getTelefonoCelular());

	}
	
	public String getCantidadMaximaCotizacionesAuto() {
		return getValor(CANTIDAD_MAXIMA_COTIZACIONES_AUTO);
	}
	public String getCantidadMaximaCotizacionesVida() {
		return getValor(CANTIDAD_MAXIMA_COTIZACIONES_VIDA);
	}
	public String getCantidadMaximaCotizacionesCasa() {
		return getValor(CANTIDAD_MAXIMA_COTIZACIONES_CASA);
	}
	public String getCantidadMaximaCotizacionesPyme() {
		return getValor(CANTIDAD_MAXIMA_COTIZACIONES_PYME);
	}
	
	private BigDecimal getNumeroCotizacion(String id, String claveNegocio) {
		BigDecimal numeroCotizacion;
        LOG.trace(">>finding getNumeroCotizacion =>" + "; id=>" + id +"--"+claveNegocio);
        try {
              final StringBuilder sb = new StringBuilder(200);
              sb.append(" SELECT count (uuid)");
              sb.append(" from midas.tocotizacionmovil ");
              sb.append(" where uuid = '?1'");
              sb.append(" and clavenegocio = '?2'");
     
              Query query = entityManager.createNativeQuery(sb.toString());
              query.setParameter(1, id);
              query.setParameter(2, claveNegocio);
              query.setMaxResults(100);
              numeroCotizacion = (BigDecimal) query.getSingleResult();
              LOG.info("numeroCotizacion =>" + numeroCotizacion);   
        } catch (RuntimeException re) {
        	LOG.error("find getNumeroCotizacion failed", re);
              return null;            
        }
        return numeroCotizacion;
  }
  private TarifasMovilCarga poblarTarifasMovil(TarifasDTO tarifa){
	  TarifasMovilCarga tarifaCarga = new TarifasMovilCarga();
	  try{
		  BigDecimal oc=new BigDecimal(tarifa.getModelovehiculo()==null?new Short("1"):tarifa.getModelovehiculo());
		 
		  tarifaCarga.setClavetarifa(tarifa.getClavetarifa());
		  tarifaCarga.setDescripciontarifa(tarifa.getDescripciontarifa());
		  tarifaCarga.setEstado(tarifa.getEstado());
		  tarifaCarga.setGrupovehiculos(tarifa.getGrupovehiculos());
		  tarifaCarga.setMarcavehiculo(tarifa.getMarcavehiculo());
		  tarifaCarga.setModelovehiculo(new BigDecimal(tarifa.getModelovehiculo()));
		  tarifaCarga.setOc(oc);
		  tarifaCarga.setTarifaamplia(tarifa.getTarifaamplia());
		  tarifaCarga.setTarifalimitada(tarifa.getTarifalimitada());
		  tarifaCarga.setTipovehiculo(tarifa.getTipovehiculo());
		  tarifaCarga.setBajalogica(new BigDecimal(tarifa.getBajalogica()));
	  }
	  catch(Exception e){
		  tarifaCarga=null;
	  }
	 return tarifaCarga;
	  	  
  }
  @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String cargaMasivaTarifasMovil(List<TarifasMovilCarga> tarifasMovilCargaList) {
	  LOG.info(">>cargaMasivaTarifasMovil");

	  MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter
				.addValue("TARIFASMOVILLISTA", tarifasMovilCargaList);
		Map<String, Object> execute = this.cargaMasivaTarifasMovil
				.execute(sqlParameter);
		BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
		String pDesc_Codigo = (String) execute.get("pDesc_Codigo");
		LOG.info("pDesc_Codigo"+pDesc_Codigo);
		LOG.info("idCodResp"+idCodResp);
		return pDesc_Codigo;
	}
   @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private ResultadoCarga saveTarifasMovilCarga(List<TarifasMovilCarga> lista) {
		LOG.info(">>saveTarifasMovilCarga");
		ErrorBuilder eb= new ErrorBuilder();
		ResultadoCarga resultado=new ResultadoCarga();
		List<String> listaErrores = new ArrayList<String>();		
		try{
			try{
				if (con != null && con.isClosed()==false) {
					OracleConnection oracleCon = con.unwrap(OracleConnection.class);					
					ArrayDescriptor arrayDescr = ArrayDescriptor.createDescriptor("MIDAS.TARIFASMOVIL_LIST", oracleCon);					
					Object[] arrayObjects = new Object[lista.size()];
					int i = 0;
					for (TarifasMovilCarga o : lista) {
						Object[] arrayVector = new Object[]{
								o.getGrupovehiculos(),
								o.getModelovehiculo(), 
								o.getTipovehiculo(),
								o.getMarcavehiculo(), 
								o.getEstado(),
								o.getDescripciontarifa(), 
								o.getOc(),
								o.getTarifaamplia(), 
								o.getTarifalimitada(),
								o.getClavetarifa(), 
								o.getBajalogica()
						};
						arrayObjects[i] = arrayVector;
						i = i +1;
					}
					
					ARRAY array = new ARRAY(arrayDescr, oracleCon, arrayObjects);
					StringBuilder sb = new StringBuilder();
					sb.append(" BEGIN  " );
					sb.append(" MIDAS.PKG_AUT_CARGA_TARIFAS_MOVIL.SP_CARGA_TARIFAS_MOVIL( " );
					sb.append(" TARIFASMOVILLISTA => ?, ");
					sb.append(" ERRORES => ?, ");
					sb.append(" PINSERTADOS => ?,");
					sb.append(" PACTUALIZADOS => ?," );
					sb.append(" PERRORES => ?); ");
					sb.append(" END; ");
					CallableStatement vectoresInsertSP = oracleCon.prepareCall(sb.toString());
					vectoresInsertSP.setArray(1, array);
					vectoresInsertSP.registerOutParameter(2,OracleTypes.ARRAY,"MIDAS.ERRORES_TARIFAS_LIST");
					vectoresInsertSP.registerOutParameter(3,OracleTypes.NUMBER);
					vectoresInsertSP.registerOutParameter(4,OracleTypes.NUMBER);
					vectoresInsertSP.registerOutParameter(5,OracleTypes.NUMBER);
					vectoresInsertSP.execute();
					ARRAY arrayOut =  (ARRAY)vectoresInsertSP.getArray(2);
					BigDecimal insertados=(BigDecimal)vectoresInsertSP.getBigDecimal(3);
					BigDecimal actualizados=(BigDecimal)vectoresInsertSP.getBigDecimal(4);
					BigDecimal noInsertados=(BigDecimal)vectoresInsertSP.getBigDecimal(5);					
					try{
						Object[] arrayObjectOut = (Object[])arrayOut.getArray();
						for (int ii = 0; ii < arrayObjectOut.length; ii++) { 
							  STRUCT person = (STRUCT) arrayObjectOut[ii];
							  if(person!=null){
								  Object[] personAttributes = person.getAttributes();
								  if(personAttributes!=null){
									  BigDecimal id = (BigDecimal)personAttributes[0];
									  String error = (String)personAttributes[1];
									  System.out.println("\n id: "+id+", error: "+error);
									  listaErrores.add(error);
								  }								  
							  }
							  
							}
					}
					catch(Exception e){
						LOG.error("STRUCT",e);
						throw new RuntimeException(e);
					}					
					
					resultado.setErroresList(listaErrores);
					resultado.setInsertados(insertados);
					resultado.setActualizados(actualizados);
					resultado.setErrores(noInsertados);
					
				}
			}catch(SQLException sqlEx){
				
				LOG.error("MIDAS.PKG_AUT_CARGA_TARIFAS_MOVIL.SP_CARGA_TARIFAS_MOVIL ha fallado : guardarLista", sqlEx);
				throw new ApplicationException(eb.addFieldError("cargaTarifas",
						"Error en la carga de Tarifas"+sqlEx));
			}
		}catch (RuntimeException re) {
			
			LOG.error("MIDAS.PKG_AUT_CARGA_TARIFAS_MOVIL.SP_CARGA_TARIFAS_MOVIL ha fallado : guardarLista",re);
			throw new ApplicationException(eb.addFieldError("cargaTarifas",
					"Error en la carga de Tarifas"+re));
		}
		LOG.info("<<saveTarifasMovilCarga");
		return resultado;
	}
}

class ResultadoCarga  {
    private List<String> erroresList;
	private BigDecimal insertados;
    private BigDecimal errores;
    private BigDecimal actualizados;
    
    public List<String> getErroresList() {
		return erroresList;
	}
	public void setErroresList(List<String> erroresList) {
		this.erroresList = erroresList;
	}
	public BigDecimal getInsertados() {
		return insertados;
	}
	public void setInsertados(BigDecimal insertados) {
		this.insertados = insertados;
	}
	public BigDecimal getErrores() {
		return errores;
	}
	public void setErrores(BigDecimal errores) {
		this.errores = errores;
	}
	public BigDecimal getActualizados() {
		return actualizados;
	}
	public void setActualizados(BigDecimal actualizados) {
		this.actualizados = actualizados;
	}
}
    

