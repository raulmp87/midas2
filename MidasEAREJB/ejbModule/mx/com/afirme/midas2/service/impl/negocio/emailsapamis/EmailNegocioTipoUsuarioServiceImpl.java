package mx.com.afirme.midas2.service.impl.negocio.emailsapamis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioTipoUsuario;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.emailsapamis.EmailNegocioTipoUsuarioService;

@Stateless
public class EmailNegocioTipoUsuarioServiceImpl implements EmailNegocioTipoUsuarioService{
	private EntidadService entidadService;
	
	@Override
	public EmailNegocioTipoUsuario completeObject(EmailNegocioTipoUsuario arg0) {
		EmailNegocioTipoUsuario retorno = new EmailNegocioTipoUsuario(); 
		if(arg0 != null){
			if(arg0.getIdEmailNegocioTipoUsuario() < 1){
				retorno = findIdByAttributes(arg0);
			}else{
				if(!validateAttributes(arg0)){
					retorno = findById(arg0.getIdEmailNegocioTipoUsuario());
				}
			}
		}else{
			retorno.setIdEmailNegocioTipoUsuario(Long.valueOf(-1));
		}
		return retorno;
	}

	@Override
	public List<EmailNegocioTipoUsuario> findAll() {
		List<EmailNegocioTipoUsuario> retorno = entidadService.findAll(EmailNegocioTipoUsuario.class);
		return retorno;
	}

	@Override
	public EmailNegocioTipoUsuario findById(Long arg0) {
		EmailNegocioTipoUsuario retorno = new EmailNegocioTipoUsuario();
		if(arg0 != null && arg0 >= 0){
			retorno = entidadService.findById(EmailNegocioTipoUsuario.class, arg0);
		}
		return retorno;
	}

	@Override
	public List<EmailNegocioTipoUsuario> findByProperty(String arg0, Object arg1) {
		List<EmailNegocioTipoUsuario> emailNegocioAlertasList = new ArrayList<EmailNegocioTipoUsuario>();
		if(arg0 != null && !arg0.equals("") && arg1 != null){
			emailNegocioAlertasList = entidadService.findByProperty(EmailNegocioTipoUsuario.class, arg0, arg1);
		}
		return emailNegocioAlertasList;
	}

	@Override
	public List<EmailNegocioTipoUsuario> findByStatus(boolean arg0) {
		List<EmailNegocioTipoUsuario> emailNegocioAlertas = entidadService.findByProperty(EmailNegocioTipoUsuario.class, "estatus", arg0?0:1);
		return emailNegocioAlertas;
	}

	@Override
	public EmailNegocioTipoUsuario saveObject(EmailNegocioTipoUsuario arg0) {
		if(arg0 != null && arg0.getIdEmailNegocioTipoUsuario() >= 0){
			arg0.setIdEmailNegocioTipoUsuario((Long)entidadService.saveAndGetId(arg0));
		}
		return arg0;
	}
	
	private boolean validateAttributes(EmailNegocioTipoUsuario arg0){
		boolean retorno = true;
		if(retorno){
			retorno = arg0.getEmailNegocioTipoUsuario() != null;
		}
		return retorno;
	}

	@Override
	public EmailNegocioTipoUsuario findIdByAttributes(EmailNegocioTipoUsuario arg0) {
		if(validateAttributes(arg0)){
			Map<String,Object> parametros = new HashMap<String,Object>();
			parametros.put("emailNegocioTipoUsuario", arg0.getEmailNegocioTipoUsuario());
			List<EmailNegocioTipoUsuario> emailList = entidadService.findByProperties(EmailNegocioTipoUsuario.class, parametros);
			if(emailList.size()>0){
				arg0.setIdEmailNegocioTipoUsuario(emailList.get(0).getIdEmailNegocioTipoUsuario());
			}else{
				arg0.setIdEmailNegocioTipoUsuario(Long.valueOf(0));
			}
		}else{
			arg0.setIdEmailNegocioTipoUsuario(Long.valueOf(-1));
		}
		return arg0;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public EmailNegocioTipoUsuario findByDesc(String arg0) {
		EmailNegocioTipoUsuario retorno = new EmailNegocioTipoUsuario();
		if(arg0 != null && !arg0.equals("")){
			List<EmailNegocioTipoUsuario> list = entidadService.findByProperty(EmailNegocioTipoUsuario.class, "emailNegocioTipoUsuario", arg0);
			if(list.size() > 0){
				retorno = list.get(0);
			}else{
				retorno.setIdEmailNegocioTipoUsuario(new Long(0));
				retorno.setEmailNegocioTipoUsuario(arg0);
			}
		}
		return retorno;
	}
}