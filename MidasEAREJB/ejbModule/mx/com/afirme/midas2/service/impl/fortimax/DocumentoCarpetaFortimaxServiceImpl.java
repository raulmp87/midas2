package mx.com.afirme.midas2.service.impl.fortimax;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fortimax.DocumentoCarpetaFortimaxDao;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.service.fortimax.DocumentoCarpetaFortimaxService;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class DocumentoCarpetaFortimaxServiceImpl implements DocumentoCarpetaFortimaxService{
	private DocumentoCarpetaFortimaxDao dao;
	@Override
	public void delete(Long arg0) throws MidasException {
		dao.delete(arg0);
	}

	@Override
	public Long delete(CatalogoDocumentoFortimax arg0) throws MidasException {
		return dao.delete(arg0);
	}
	@Override
	public List<CatalogoDocumentoFortimax> obtenerDocumentosPorAplicacion(String nombreAplicacion) throws MidasException{
		return dao.obtenerDocumentosPorAplicacion(nombreAplicacion);
	}
	@Override
	public List<CatalogoDocumentoFortimax> obtenerDocumentosPorAplicacionTipoPersona(String nombreAplicacion,Long idTipoPersona) throws MidasException{
		return dao.obtenerDocumentosPorAplicacionTipoPersona(nombreAplicacion,idTipoPersona);
	}

	@Override
	public List<CatalogoDocumentoFortimax> obtenerDocumentosPorCarpeta(String arg0, String arg1) throws MidasException {
		return dao.obtenerDocumentosPorCarpeta(arg0, arg1);
	}

	@Override
	public List<CatalogoDocumentoFortimax> obtenerDocumentosPorCarpetaConTipoPersona(String arg0, String arg1, Long arg2) throws MidasException {
		return dao.obtenerDocumentosPorCarpetaConTipoPersona(arg0, arg1,arg2);
	}

	@Override
	public List<CatalogoDocumentoFortimax> obtenerDocumentosRequeridosPorCarpeta(String arg0, String arg1) throws MidasException {
		return dao.obtenerDocumentosRequeridosPorCarpeta(arg0, arg1);
	}

	@Override
	public List<CatalogoDocumentoFortimax> obtenerDocumentosRequeridosPorCarpetaConTipoPersona(String arg0, String arg1, Long arg2) throws MidasException {
		return dao.obtenerDocumentosRequeridosPorCarpetaConTipoPersona(arg0, arg1, arg2);
	}

	@Override
	public Long save(CatalogoDocumentoFortimax arg0) throws MidasException {
		return dao.save(arg0);
	}
	/**
	 * ======================================================================
	 * Setters and getters
	 * ======================================================================
	 */
	@EJB
	public void setDao(DocumentoCarpetaFortimaxDao dao) {
		this.dao = dao;
	}
}
