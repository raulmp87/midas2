package mx.com.afirme.midas2.service.impl.reportes.historial.poliza;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.reportes.historial.poliza.ReporteHistorialPolizaDAO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.ReporteHistorialPolizaDTO;
import mx.com.afirme.midas2.service.reportes.historial.poliza.ReporteHistorialPolizaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.agentes.CotizadorAgentesService;
import mx.com.afirme.midas2.util.ExcelExporter;

/**
 * Clase que implementa la l\u000f3gica propuesta por la interfaz de <b>ReporteHistoricoPolizaService</b>
 * 
 * @author AFIRME
 * 
 * @since 28072016
 * 
 * @version 1.0
 *
 */
@Stateless
public class ReporteHistorialPolizaServiceImpl implements Serializable, ReporteHistorialPolizaService {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ReporteHistorialPolizaDAO dao;
	
	@EJB
	private CotizadorAgentesService agenteService;

	@Override
	public List<ReporteHistorialPolizaDTO> consultarHistorialPolizasPorAgente(Long idAgente){
		Agente result = null;
		List<ReporteHistorialPolizaDTO> resultados = new ArrayList<ReporteHistorialPolizaDTO>();
		if(idAgente != null && idAgente > 0L){
    		Agente filtro = new Agente();
    		filtro.setId(idAgente);
    		result = agenteService.getAgente(filtro);
		}
		if(result != null && result.getIdAgente() != null && result.getIdAgente() > 0L)
				resultados = dao.consultarHistorialPolizaPorAgente(result.getIdAgente());
		return resultados;
	}

	@Override
	public List<ReporteHistorialPolizaDTO> consultarHistorialPorNumeroPoliza(Long numPoliza){
		List<ReporteHistorialPolizaDTO> resultados = new ArrayList<ReporteHistorialPolizaDTO>();
		if(numPoliza != null && numPoliza > 0L)
			resultados = dao.consultarHistorialPorNumeroPoliza(numPoliza);
		return resultados;
	}
	
	@Override
	public TransporteImpresionDTO generarReporte(List<ReporteHistorialPolizaDTO> dataSource, String nombreLibro){
		ExcelExporter exporter = new ExcelExporter(ReporteHistorialPolizaDTO.class);
		return exporter.exportXLS(dataSource, nombreLibro);
	}

	public ReporteHistorialPolizaDAO getDao() {
		return dao;
	}

	public void setDao(ReporteHistorialPolizaDAO dao) {
		this.dao = dao;
	}

	public CotizadorAgentesService getAgenteService() {
		return agenteService;
	}

	public void setAgenteService(CotizadorAgentesService agenteService) {
		this.agenteService = agenteService;
	}
}
