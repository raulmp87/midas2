package mx.com.afirme.midas2.service.impl.juridico;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.juridico.CitaJuridicoDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.juridico.CitaJuridico;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.juridico.CitaJuridicoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MailService;

@Stateless
public class CitaJuridicoServiceImpl implements CitaJuridicoService{
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private CitaJuridicoDao citaJuridicoDao;
	
	@EJB
	private ListadoService listadoService;
	
	@EJB
	private MailService mailService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private SistemaContext sistemaContext;

	@Override
	public CitaJuridico obtener(Long id) {
		
		return entidadService.findById(CitaJuridico.class, id);
	}

	@Override
	public List<CitaJuridico> filtrar(CitaJuridicoFiltro filtro) {
		
		List<CitaJuridico> listadoCitas = citaJuridicoDao.filtrar(filtro);		
		obtenerDescripcionesCatalogos(listadoCitas);
		
		return listadoCitas;
	}
	
	@Override
	public Long filtrarPaginado(CitaJuridicoFiltro filtro) {
		
		return citaJuridicoDao.filtrarPaginado(filtro);
	}

	@Override
	public CitaJuridico guardar(CitaJuridico cita) {
		
		cita = entidadService.save(cita);
		enviarNotificacion(cita); 
		
		return cita;
	}

	@Override
	public void eliminar(Long id) {
		
		CitaJuridico cita = entidadService.findById(CitaJuridico.class, id);
		entidadService.remove(cita);
	}
	
	private void obtenerDescripcionesCatalogos(List<CitaJuridico> listadoCitas)
	{
		Map<String, String> listaTiposReclamacion;
		Map<String, String> listaRamosJuridico;
		Map<String, String> listaEstatusCitaJuridico;
		
		listaTiposReclamacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RECLAMACION_JURIDICO);
		listaRamosJuridico = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RAMO_JURIDICO);
		listaEstatusCitaJuridico = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_CITA_JURIDICO);
		
		for(CitaJuridico cita:listadoCitas)
		{
			cita.setDescripcionTipoReclamacion(listaTiposReclamacion.get(cita.getTipoReclamacion()));
			cita.setDescripcionRamo(listaRamosJuridico.get(cita.getRamo()));
			cita.setDescripcionEstatus(listaEstatusCitaJuridico.get(cita.getEstatus()));
		}		
	}
	
	private void enviarNotificacion(CitaJuridico cita)
	{
		List<CitaJuridico> listadoCitas = new ArrayList<CitaJuridico>();	
		listadoCitas.add(cita);
		obtenerDescripcionesCatalogos(listadoCitas);
		cita = listadoCitas.get(0);
		
		String keyMensajeNotificacion = cita.getCodigoUsuarioModificacion() != null?
				"midas.juridico.citas.correo.cuerpo.modificacion":
					"midas.juridico.citas.correo.cuerpo.nueva";
		
		Usuario usuarioActual = usuarioService.getUsuarioActual();
		
		List<Usuario> usuarios = 
			usuarioService.buscarUsuariosSinRolesPorNombreRol(
					sistemaContext.getRolDirectorJuridico(),
					usuarioActual.getNombreUsuario(), 
					usuarioActual.getIdSesionUsuario());
		
		List<String> direccionesDirectoresJuridico = new ArrayList<String>();
		
		for(Usuario directorJuridico:usuarios)
		{
			direccionesDirectoresJuridico.add(directorJuridico.getEmail());		
		}
		
		//test
		//direccionesDirectoresJuridico.add("orlando.abasta@afirme.com");
		
		if(usuarioService.tieneRolUsuarioActual(sistemaContext.getRolAnalistaJuridico()))
		{						
			mailService.sendMail(direccionesDirectoresJuridico, Utilerias.getMensajeRecurso(
					SistemaPersistencia.ARCHIVO_RECURSOS, "midas.juridico.citas.correo.titulo"), 
					Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, keyMensajeNotificacion,
							cita.getId(),
							cita.getFechaInicio(),
							cita.getFechaFin(),
							cita.getNombreUsuario(),
							cita.getAsignado(),
							cita.getDescripcionEstatus(),
							cita.getDescripcionTipoReclamacion(),
							cita.getLugarCita(),
							cita.getDescripcionRamo(),
							cita.getMotivo()),
					null, 
					Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.juridico.citas.correo.titulo")
							,
					Utilerias.getMensajeRecurso(
							SistemaPersistencia.ARCHIVO_RECURSOS, "midas.juridico.citas.correo.saludo"));
		}				
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public CitaJuridicoDao getCitaJuridicoDao() {
		return citaJuridicoDao;
	}

	public void setCitaJuridicoDao(CitaJuridicoDao citaJuridicoDao) {
		this.citaJuridicoDao = citaJuridicoDao;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public MailService getMailService() {
		return mailService;
	}

	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public SistemaContext getSistemaContext() {
		return sistemaContext;
	}

	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
}
