package mx.com.afirme.midas2.service.impl.bitemporal.suscripcion.emision;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.compensaciones.CalculoCompensacionesDao;
import mx.com.afirme.midas2.dao.pkg.PkgAutGeneralesDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.MovimientoEndoso;
import mx.com.afirme.midas2.service.avisos.AvisosService;
import mx.com.afirme.midas2.service.bitemporal.informacionVehicular.InformacionVehicularBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.MovimientoEndosoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.diferenciasAmis.DiferenciasAmisService;
import mx.com.afirme.midas2.service.fronterizos.FronterizosService;
import mx.com.afirme.midas2.service.impl.avisos.AvisosServiceImpl;
import mx.com.afirme.midas2.service.informacionVehicular.InformacionVehicularService;
import mx.com.afirme.midas2.util.DateUtils;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

 
@Stateless
public class EmisionEndosoBitemporalServiceImpl extends
		EmisionEndosoBitemporalSoporteService implements
		EmisionEndosoBitemporalService {
	
	private static final Logger LOG = Logger.getLogger(EmisionEndosoBitemporalServiceImpl.class);
	private CalculoCompensacionesDao calculoCompensacionesDao;
	@EJB
	private EntidadDao dao;
	@EJB
	private EntidadContinuityDao continuityDao;
	@EJB
	private PkgAutGeneralesDao pkgAutGeneralesDao;
	@EJB
	private MovimientoEndosoBitemporalService movimientoEndosoBitemporalService;
	@EJB
	private InformacionVehicularService informacionVehicularService;
	@EJB
	private InformacionVehicularBitemporalService informacionVehicularBitemporalService;
	@EJB
	private DiferenciasAmisService diferenciasAmisService;
		
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)	
	public EndosoDTO emiteEndoso(Long cotizacionContinuityId,
			DateTime validoEn, short tipoEndoso) {
		return emiteEndosoPrivado(cotizacionContinuityId, validoEn, tipoEndoso, true, null);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public EndosoDTO emiteEndosoDesagrupacionRecibos(Long cotizacionContinuityId,
			DateTime validoEn, short tipoEndoso)
	{
		return emiteEndosoPrivado(cotizacionContinuityId, validoEn, tipoEndoso, true, null);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)	
	public EndosoDTO emiteEndoso(Long cotizacionContinuityId,
			DateTime validoEn, short tipoEndoso, boolean emiteRecibos) {
		return emiteEndosoPrivado(cotizacionContinuityId, validoEn, tipoEndoso, emiteRecibos, null);
	}
		
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EndosoDTO emiteEndosoMigracion(Long cotizacionContinuityId,
			DateTime validoEn, short tipoEndoso) {
		return emiteEndosoPrivado(cotizacionContinuityId, validoEn, tipoEndoso, false, null);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)	
	public EndosoDTO emiteEndosoPerdidaTotal(Long cotizacionContinuityId,
			DateTime validoEn, short tipoEndoso, Long idSiniestroPT) {
		return emiteEndosoPrivado(cotizacionContinuityId, validoEn, tipoEndoso, true, idSiniestroPT);
	}
	
	
	private EndosoDTO emiteEndosoPrivado(Long cotizacionContinuityId,
			DateTime validoEn, short tipoEndoso, boolean emiteRecibos, Long idSiniestroPT) {
		//FIXME: Quitar el parametro tipoEndoso, debido a que no se utiliza
		EndosoDTO endoso = this.emite(cotizacionContinuityId, validoEn, emiteRecibos, idSiniestroPT);
		//TODO: descomentar cuando se termine el SP
		//this.generaRecibos(endoso);
		return endoso;
	}
	
	@SuppressWarnings("unused")
	private void generaRecibos(EndosoDTO endoso) {
		try {
			List<ReciboDTO> recibos = pkgAutGeneralesDao.emiteRecibos(endoso
					.getId().getIdToPoliza()
					+ "|"
					+ endoso.getId().getNumeroEndoso());
			if (recibos != null && recibos.size() > 0) {
				ReciboDTO recibo = recibos.get(0);
				if (endoso != null) {
					endoso.setLlaveFiscal(recibo.getLlaveFiscal());
					entidadService.save(endoso);
				}
			}
		} catch (Exception e) {
		}
	}
	
	@SuppressWarnings("unchecked")
	private EndosoDTO emite(Long cotizacionContinuityId, DateTime validoEn, boolean emiteRecibos, Long idSiniestroPT) {
		//Precondiciones
		if (cotizacionContinuityId == null) {
			throw new IllegalArgumentException(
					"La continuidad de la cotizacion no debe ser nula");
		}
		
		if (validoEn == null) {
			throw new IllegalArgumentException("validoEn no puede ser nulo para la cotizacionContinuityId " + cotizacionContinuityId);
		}
		
		CotizacionContinuity cotizacionContinuity = entidadContinuityService
				.findContinuityByKey(CotizacionContinuity.class,
						cotizacionContinuityId);

		if (cotizacionContinuity == null) {
			throw new IllegalArgumentException(
					"No se pudo encontrar el CotizacionContinuity del id:"
							+ cotizacionContinuityId);
		}
		
		Boolean aplicaDerechos = false;
		EndosoDTO endoso = null;
		
		BitemporalCotizacion cotizacionBitemporal = cotizacionContinuity
				.getCotizaciones().getInProcess(validoEn);
		//FIXME:Este bloque IF aparentemente esta de más ya que si el getInProcess no regresa nada tampoco lo hara el get.
		if(cotizacionBitemporal == null){
			cotizacionBitemporal = cotizacionContinuity
			.getCotizaciones().get(validoEn);			
		}
		if (cotizacionBitemporal == null) {
			throw new RuntimeException(
					"No fue posible encontrar una CotizacionBitemporal cotizacionContinuityId"
							+ cotizacionContinuityId + " en " + validoEn);
		}
		
		ControlEndosoCot controlEndosoCot  = null;
		/*Map<String, Object> values = new LinkedHashMap<String, Object>();
		values.put("cotizacionId", cotizacionBitemporal.getContinuity().getNumero());
		values.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		List<ControlEndosoCot> controlEndosoCotList = (List<ControlEndosoCot>)entidadService.findByProperties(ControlEndosoCot.class,values);*/
		
		String query = "SELECT model FROM ControlEndosoCot model WHERE model.cotizacionId ="
			+ cotizacionBitemporal.getContinuity().getNumero()
			+ " AND model.claveEstatusCot IN("+ CotizacionDTO.ESTATUS_COT_EN_PROCESO +","+ CotizacionDTO.ESTATUS_COT_TERMINADA +")";
		
		List<ControlEndosoCot> controlEndosoCotList = (List<ControlEndosoCot>) entidadService.executeQueryMultipleResult(query, null);
		
		if (controlEndosoCotList.size() == 0) {
			throw new IllegalStateException(
					"No se pudo encontrar el ControlEndosoCot correspondiente a la CotizacionContinuity: "
							+ cotizacionContinuity
							+ " para emitir un endoso es necesario que se encuentre y esté con claveEstatusCot en proceso ("
							+ CotizacionDTO.ESTATUS_COT_EN_PROCESO + ").");
		}
		
		controlEndosoCot = controlEndosoCotList.get(0);
		LogDeMidasInterfaz.log("controlEndosoCot => " + controlEndosoCot, Level.INFO, null);
		SolicitudDTO solicitud = controlEndosoCot.getSolicitud();
		LogDeMidasInterfaz.log("solicitud => " + solicitud, Level.INFO, null);
		Boolean esMigrada = (!emiteRecibos)?true:false;
		//se realiza el calculo de los movimientos.
		movimientoEndosoBitemporalService.calcular(controlEndosoCot,esMigrada);
		endosoService.igualarPrimaMovimientosEndoso(controlEndosoCot);
		endosoService.ajustaMovimientosEndoso(controlEndosoCot);
		
		
		PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, solicitud.getIdToPolizaEndosada());
		LogDeMidasInterfaz.log("polizaDTO idToPoliza => " + polizaDTO.getIdToPoliza(), Level.INFO, null);
		
		// Crear cabecera de Endoso.
		EndosoId id = new EndosoId(
				polizaDTO.getIdToPoliza(),
				(short) endosoService.getNumeroEndoso(polizaDTO.getIdToPoliza()));

		endoso = new EndosoDTO(id, polizaDTO,
				BigDecimal.valueOf(cotizacionContinuity.getNumero()),
				null,
				solicitud.getFechaInicioVigenciaEndoso(), 
				cotizacionBitemporal.getValue().getFechaFinVigencia(), 0D, 0D, 0D,
				0D, 0D, 0D, 0D);
		endoso.setValorBonifComisionRPF(0D);
		endoso.setValorComision(0D);
		endoso.setValorComisionRPF(0D);
		
		endoso.setValidFrom(validoEn.toDate());
		Date recordFrom = TimeUtils.now().toDate();
		endoso.setTipoCambio(cotizacionBitemporal.getValue().getTipoCambio());
		
		if (cotizacionBitemporal.getValue().getNegocioDerechoEndosoId() == null) 
		{
			for (NegocioDerechoEndoso negocioDerechoEndoso : cotizacionBitemporal.getValue().getSolicitud().getNegocio().getNegocioDerechoEndosos()) {
				if (negocioDerechoEndoso.getClaveDefault()) {
					cotizacionBitemporal.getValue().setNegocioDerechoEndosoId(negocioDerechoEndoso.getIdToNegDerechoEndoso());
					break;
				}
			}
		}		
	
		switch (solicitud.getClaveTipoEndoso().shortValue()) {
		case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS: 
		case SolicitudDTO.CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS:	   
			List<MovimientoEndoso> movimientosInciso = this.getMovimientosInciso(controlEndosoCot);
			endoso = emiteEndoso(cotizacionContinuity, cotizacionBitemporal,
					polizaDTO, endoso, movimientosInciso, solicitud, aplicaDerechos);
			// OFAL - NOTA: Se agrega un segundo de diferencia, ya que cuando se requiere emitir un endoso de desagrupacion de recibos 
			// como prerrequisito para un endoso de Perdida total, se genera un conflicto al emitirse ambos endosos usando la misma referencia de tiempo 
			// proporcionada por el metodo TimeUtils.now()
			recordFrom = DateUtils.add(recordFrom, DateUtils.Indicador.Seconds, 1);
			
			break;			
		case SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA:
		case SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL:	  
		case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA:
			if (polizaDTO.getClaveEstatus().intValue() == PolizaDTO.Status.Vigente.ordinal()) {
				polizaDTO.setClaveEstatus(new Short(PolizaDTO.Status.Cancelada.ordinal() + ""));
				if(polizaDTO.getCotizacionDTO().getFechaInicioVigencia().equals(controlEndosoCot.getValidFrom())){
					aplicaDerechos = true;
				}
			} else {
				polizaDTO.setClaveEstatus(new Short(PolizaDTO.Status.Vigente.ordinal() + ""));
			}
			entidadService.save(polizaDTO);
		default:
			endoso.setCancela(this.obtenerNumeroEndosoPorCancelarRehabilitar(controlEndosoCot, solicitud.getClaveTipoEndoso().shortValue()));
			List<MovimientoEndoso> movimientos = this.getMovimientos(controlEndosoCot);
			endoso = emiteEndoso(cotizacionContinuity, cotizacionBitemporal,
					polizaDTO, endoso, movimientos, solicitud, aplicaDerechos);
			break;
		}
		endoso.setClaveTipoEndoso(this.calculaTipoEndoso(endoso));
		endoso.setIdSiniestroPT(idSiniestroPT);
				
		continuityDao.commitContinuity(cotizacionContinuityId, recordFrom);	
		
		endoso.setValidFrom(validoEn.toDate());
		endoso.setRecordFrom(recordFrom);
		endoso.setFechaCreacion(recordFrom);
		
		entidadService.save(endoso);
		endosoService.actualizaControlEndosoCot(polizaDTO.getCotizacionDTO().getIdToCotizacion(), 
				Long.valueOf(CotizacionDTO.ESTATUS_COT_EMITIDA),validoEn.toDate(), recordFrom);

		endosoService.actualizaAgrupadoresEndoso(polizaDTO.getIdToPoliza(), new BigDecimal(endoso.getId().getNumeroEndoso()));
		short cveMotivoEndoso = null != solicitud.getClaveMotivoEndoso() ? solicitud.getClaveMotivoEndoso() : 0;
		
		if (emiteRecibos) {
			this.emiteRecibos(endoso, id, solicitud.getClaveTipoEndoso().shortValue(), cveMotivoEndoso, polizaDTO.getIdToPoliza());
		}		
		
		//Valida estilo contra la informacion vehicular de CESVI, en poliza individual
		try {
			if (cotizacionBitemporal.getValue().getTipoPoliza().getClaveAplicaFlotillas() != null &&
					cotizacionBitemporal.getValue().getTipoPoliza().getClaveAplicaFlotillas() == 0) {
				informacionVehicularBitemporalService.validaInfoVehicularByEstiloPolInd(cotizacionBitemporal, validoEn, 
						solicitud.getClaveTipoEndoso(), polizaDTO);
			} 
		} catch(Exception e){
			LOG.error(e);
		}
		
		try {
			if (cotizacionBitemporal.getValue().getTipoPoliza().getClaveAplicaFlotillas() != null
					&& cotizacionBitemporal.getValue().getTipoPoliza().getClaveAplicaFlotillas() == 0) {
				fronterizosService.migrarClavesCotizacionBitemporal(cotizacionBitemporal, validoEn);
			}
		} catch (Exception e) {
			LOG.error(e);
		}
		
		
		//envia avisos a cobranza
		if(SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA==solicitud.getClaveTipoEndoso().shortValue() &&
				SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO!=cveMotivoEndoso){
			avisosService.enviarAviso(polizaDTO.getIdToPoliza(),AvisosServiceImpl.AVISO_CANCELACION,
					solicitud.getClaveTipoEndoso().shortValue());
		}
		
		return endoso;
	}

	

	@SuppressWarnings("unchecked")
	public List<MovimientoEndoso> getMovimientos(
			ControlEndosoCot controlEndosoCot) {
		if(controlEndosoCot!=null){
			String query = "select model from MovimientoEndoso model where model.controlEndosoCot.id ="
					+ controlEndosoCot.getId()
					+ " and model.bitemporalCoberturaSeccion.id is not null";
	
			return (List<MovimientoEndoso>) dao
					.executeQueryMultipleResult(query, null);
		}else{
			return new ArrayList<MovimientoEndoso>();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<MovimientoEndoso> getMovimientosInciso(
			ControlEndosoCot controlEndosoCot) {
		if(controlEndosoCot!=null){
			String query = "select model from MovimientoEndoso model where model.controlEndosoCot.id ="
					+ controlEndosoCot.getId()
					+ " and model.bitemporalInciso.id is not null";
	
			return (List<MovimientoEndoso>) dao
					.executeQueryMultipleResult(query, null);
		}else{
			return new ArrayList<MovimientoEndoso>();
		}
		
	}

	@Override
	public short calculaTipoEndoso(EndosoDTO endoso) {
		short endosoCalculado = (short)2;//default
		switch (endoso.getClaveTipoEndoso()) {
		case SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA:
		case SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL:	
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_CANCELACION;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_AUMENTO;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO:
		case SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_CANCELACION_INCISO;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_AGENTE:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_CAMBIO_DATOS;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_CAMBIO_DATOS;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_CANCELACION_ENDOSO;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_EXTENSION_VIGENCIA:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_AUMENTO;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_ANEXO:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_CAMBIO_DATOS;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_TEXTO:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_CAMBIO_DATOS;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS:
		case SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA:
			//Calcular de acuerdo a movimientos
			if(endoso.getValorPrimaTotal()>0){
				endosoCalculado = EndosoDTO.TIPO_ENDOSO_AUMENTO;
			}else if(endoso.getValorPrimaTotal() == 0){
				endosoCalculado =  EndosoDTO.TIPO_ENDOSO_CAMBIO_DATOS;
			}else{
				endosoCalculado = EndosoDTO.TIPO_ENDOSO_DISMINUCION;
			}
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_REHABILITACION_INCISO;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_REHABILITACION_ENDOSO;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_REHABILITACION;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_DESAGRUPACION_RECIBOS; 
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES:
		case SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_CONDICIONES:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_CAMBIO_DATOS;
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_CONDUCTO_COBRO:
			endosoCalculado = EndosoDTO.TIPO_ENDOSO_CAMBIO_DATOS;
			break;
		default:
			break;
		}
		return endosoCalculado;
	}
	
	private Short obtenerNumeroEndosoPorCancelarRehabilitar(
			ControlEndosoCot controlEndosoCot, Short tipoEndoso) {

		Short numeroEndoso = 0;

		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO
				|| tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO) 
		{
			ControlEndosoCot controlEndosoCotCancelacion = null;

			if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO) 
			{
				ControlEndosoCot ControlEndosoCotRehabilitacion = entidadService
						.findById(ControlEndosoCot.class,
								controlEndosoCot.getIdCancela());
				controlEndosoCot = ControlEndosoCotRehabilitacion;
			}

			controlEndosoCotCancelacion = entidadService.findById(
					ControlEndosoCot.class, controlEndosoCot.getIdCancela());

			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("idToCotizacion",
					controlEndosoCotCancelacion.getCotizacionId());
			parametros.put("recordFrom",
					controlEndosoCotCancelacion.getRecordFrom());
			List<EndosoDTO> listaEndosos = entidadService.findByProperties(
					EndosoDTO.class, parametros);

			if (!listaEndosos.isEmpty()) {
				numeroEndoso = listaEndosos.get(0).getId().getNumeroEndoso();
			}
		}

		return numeroEndoso;
	}
	
	private void emiteRecibos(EndosoDTO endoso,EndosoId id, short tipoEndoso, short motivoEndoso, BigDecimal idToPoliza){		
		try{
			StringBuilder pIdentificador = new StringBuilder();
			pIdentificador.append(id.getIdToPoliza().toString());
			pIdentificador.append("|");
			pIdentificador.append(id.getNumeroEndoso().toString());
			List<ReciboDTO> recibos = pkgAutGeneralesDao.emiteRecibosEndoso(pIdentificador.toString());
			if(recibos != null && recibos.size() > 0){
				ReciboDTO recibo = recibos.get(0);
				endoso = entidadService.findById(EndosoDTO.class, id);
				if(endoso!= null){
					endoso.setLlaveFiscal(recibo.getLlaveFiscal());
					endoso.setSolictudCheque(recibo.getSolicitudCheque());
					entidadService.save(endoso);
				}
			}
			//envia avisos a cobranza
			if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA &&
					motivoEndoso != SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO){
				avisosService.enviarAviso(idToPoliza,AvisosServiceImpl.AVISO_ALTA_NOTA,tipoEndoso);
			}			
		}catch(Exception e){
			LogDeMidasInterfaz.log("Excepcion EmisionEndosoBitemporalServiceImpl.emiteRecibos idToPoliza => " + idToPoliza, Level.WARNING, e);
		}
	}
	
	@EJB
	private FronterizosService fronterizosService;
	@EJB
	private AvisosService avisosService;


    @EJB
	public void setCalculoCompensacionesDao(
			CalculoCompensacionesDao calculoCompensacionesDao) {
		this.calculoCompensacionesDao = calculoCompensacionesDao;
	}
	
}
