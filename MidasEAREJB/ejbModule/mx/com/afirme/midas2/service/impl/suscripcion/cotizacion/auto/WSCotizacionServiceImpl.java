package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoId;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoInterfazServiciosRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudFacadeRemote;
import mx.com.afirme.midas2.dao.negocio.estadodescuento.NegocioEstadoDescuentoDao;
import mx.com.afirme.midas2.dao.negocio.producto.NegocioProductoDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CotizacionView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.ContratanteView;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.domain.ws.autoplazo.Cliente;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.negocio.producto.tipopoliza.seccion.paquete.RelacionesNegocioPaqueteDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosGeneralDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoPolizaService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioDerechosService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.NegocioTipoPolizaService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.WSCotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.comision.ComisionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.emision.EmisionService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.service.suscripcion.pago.PolizaPagoService;
import mx.com.afirme.midas2.service.tarifa.TarifaAgrupadorTarifaService;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionNegocioAutosService;
import mx.com.afirme.midas2.service.impl.excepcion.ExcepcionSuscripcionNegocioAutosServiceImpl;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.extrainfo.CotizacionExtService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class WSCotizacionServiceImpl implements WSCotizacionService{
	
	private static Logger LOG = Logger.getLogger(WSCotizacionServiceImpl.class);
	private static final String DEFAULTONLINEAUTOS = "000000000";
	private static final String DEFAULTONLINECAMIONES = "000000001";
	private static final String CONDICIONES_ESPECIALES_EMPLEADOS = "PÓLIZA SUJETA A LAS CONDICIONES ESPECIALES PARA EMPLEADOS DE CONSORCIO AFIRME/VILLACERO ";
	
	@EJB
	private CotizacionService cotizacionService;
	@EJB
	private IncisoService incisoService;
	@EJB
	private EntidadService entidadService;
	@EJB
	private CoberturaService coberturaService;
	@EJB
	private CalculoService calculoService;
	@EJB
	private NegocioService negocioService;	
	@EJB
	private NegocioTipoPolizaService negocioTipoPolizaService;
	@EJB
	private NegocioDerechosService negocioDerechosService;
	@EJB
	private FormaPagoInterfazServiciosRemote formaPagoInterfazServicios;
	@EJB
	private NegocioSeccionService negocioSeccionService;
	@EJB
	private NegocioPaqueteSeccionService negocioPaqueteSeccionService;
	@EJB
	private NegocioPaqueteSeccionDao negocioPaqueteSeccionDao;
	@EJB
	private PolizaPagoService pagoService;
	@EJB
	private ClienteFacadeRemote clienteFacade;	
	@EJB
	private EmisionService emisionService;
	@EJB
	private NegocioProductoDao negocioProductoDao;
	@EJB
	private AgenteMidasService agenteMidasService;
	@EJB
	private MarcaVehiculoFacadeRemote marcaVehiculoFacade;
	@EJB
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	@EJB
	private EndosoService endosoService;
	@EJB
	private NegocioAgrupadorTarifaSeccionDao negAgrupadorTarifaSeccionDao;
	@EJB
	private TarifaAgrupadorTarifaService tarifaAgrupadorService;
	@EJB
	private CotizacionFacadeRemote cotizacionFacadeRemote;
	@EJB
	private EjecutivoService ejecutivoService;
	@EJB
	private SolicitudFacadeRemote solicitudFacadeRemote;
	@EJB 
	private EstiloVehiculoFacadeRemote estiloVehiculoFacade;
	@EJB
	private SeccionFacadeRemote seccionService;
	@EJB
	private CargaMasivaService cargaMasivaService;
	@EJB
    private EndosoPolizaService endosoPolizaService;
    @EJB
    private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
    @EJB
    private ComisionService comisionService;
	@EJB
	private SistemaContext sistemaContext;
	@EJB
	private NegocioEstadoDescuentoDao negocioEstadoDescuentoDao;
	@EJB 
	private ModeloVehiculoFacadeRemote modeloVehiculoFacade;	
	@EJB
	private EstadoFacadeRemote estadoFacadeRemote;
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String validacionDatosCotizacion(String claveUsuario, Long idNegocio, Long idPaquete, String nombreAsegurado,
    		String apellidoPaterno,String apellidoMaterno,	String rfc,String claveSexo, String telefonoCasa,
    		String telefonoOficina,String email)throws SystemException {
    	StringBuilder mensajeGeneral = new StringBuilder();
    	StringBuilder mensajebody = new StringBuilder();
    	
		if(idPaquete==null){
			if(mensajebody.length() > 0){
				mensajebody.append(UtileriasWeb.SEPARADOR_COMA);
			}
			mensajebody.append("Paquete");
		}
		
		if(idNegocio==null){
			if(mensajebody.length() > 0){
				mensajebody.append(UtileriasWeb.SEPARADOR_COMA);
			}
			mensajebody.append("Negocio");
		}
		
		if(mensajebody.length() > 0){
			mensajebody.append(" son requeridos.");
			mensajebody.append(" Los datos " + mensajebody);
		}
		mensajeGeneral.append(mensajebody);

		mensajebody = new StringBuilder();
		mensajebody.append(validalongitud("Apellido Paterno", apellidoPaterno, 20));
		mensajebody.append(validalongitud("Apellido Materno", apellidoMaterno, 20));
		mensajebody.append(validalongitud("Nombre Asegurado", nombreAsegurado, 40));
		mensajebody.append(validalongitud("clave Sexo", claveSexo, 1));
		mensajebody.append(validalongitud("Telefono Casa", telefonoCasa, 12));
		mensajebody.append(validalongitud("Telefono Oficina", telefonoOficina, 12));
		mensajebody.append(validalongitud("Email", email, 60));
		
		mensajeGeneral.append(mensajebody);
    	
    	return mensajeGeneral.toString();
    }

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CotizacionDTO creaCotizacion(Integer idSucursal,String claveUsuario, Long idNegocio, Double descuento, String folio,Long idPaquete, Integer estilo, String modelo, Integer marca,
										String idEstadoCirculacion,String idMunicipioCirculacion, Double valorFactura,String nombreAsegurado,String apellidoPaterno,String apellidoMaterno,
										String rfc,String claveSexo,String claveEstadoCivil,String idEstadoNacimiento,Date fechaNacimiento,String codigoPostal,
										String idCiudad,String idEstado,String colonia,String calleNumero, String telefonoCasa,String telefonoOficina,String email,
										String numeroSerie, String numeroMotor, String codigoPostalCirculacion)throws SystemException {

		final int lengthFiledId = 5;
		
		idEstadoCirculacion = StringUtils.leftPad(idEstadoCirculacion, lengthFiledId, UtileriasWeb.STRING_CERO);
		idMunicipioCirculacion = StringUtils.leftPad(idMunicipioCirculacion, lengthFiledId, UtileriasWeb.STRING_CERO);
		idEstadoNacimiento = StringUtils.leftPad(idEstadoNacimiento, lengthFiledId, UtileriasWeb.STRING_CERO);
		idCiudad =  StringUtils.leftPad(idCiudad, lengthFiledId, UtileriasWeb.STRING_CERO);
		idEstado = StringUtils.leftPad(idEstado, lengthFiledId, UtileriasWeb.STRING_CERO);
		
		LOG.info("Entrando a ");
		LOG.info("creaCotizacion => \n claveUsuario => " + claveUsuario + "\n idNegocio => " + idNegocio + "\n folio => " + folio);
		LOG.info("idPaquete => " + idPaquete);

		BigDecimal idCotizacion = null;
		String user = UtileriasWeb.STRING_EMPTY;
		
		Agente agente = new Agente();
		final int lengthClaveUsuario = 8;
		if(claveUsuario.length() >= lengthClaveUsuario){
			user = claveUsuario.substring(0, lengthClaveUsuario);
		}else{
			user = claveUsuario;
		}
		
		try {
			agente = getAgenteBySucursal(idSucursal);
		} catch (Exception e) {
			LOG.error("No se ha encontrado Sucursal", e);
			throw new SystemException("No se ha encontrado Sucursal");
		}
		
		//Valida descuento dado
		descuento = validaDescuento(estilo, idNegocio, idPaquete, idEstadoCirculacion, descuento, folio);
		
		//Valida estado-municipio
		try{
			HashMap<String,Object> propNeg = new HashMap<String,Object>(1);
			propNeg.put("negocio.idToNegocio",idNegocio);
			List<NegocioEstado> negEstadoList = entidadService.findByProperties(NegocioEstado.class, propNeg);
			
			if(negEstadoList != null && !negEstadoList.isEmpty()){
				HashMap<String,Object> properties = new HashMap<String,Object>();
				properties.put("estadoDTO.stateId",idEstadoCirculacion);
				properties.put("negocio.idToNegocio",idNegocio);
				List<NegocioEstado> negocioEstadoList = entidadService.findByProperties(NegocioEstado.class, properties);
				if(negocioEstadoList == null || negocioEstadoList.isEmpty()){
					throw new SystemException("Estado no valido ("+idEstadoCirculacion+")");
				}
				if(codigoPostalCirculacion!=null && !codigoPostalCirculacion.isEmpty()){
					int idEstadoin=Integer.parseInt(idEstadoCirculacion)/1000;
					String resp =estadoFacadeRemote.validarCodigoPostal(codigoPostalCirculacion,idEstadoin);
					if(resp.equals("0")){
						throw new SystemException("Codigo postal de circulacion  no valido ("+codigoPostalCirculacion+")");						
					}
				}
				HashMap<String,Object> propertiesCity = new HashMap<String,Object>();
				propertiesCity.put("stateId",idEstadoCirculacion);
				propertiesCity.put("municipalityId",idMunicipioCirculacion);
				List<MunicipioDTO> municipioList = entidadService.findByProperties(MunicipioDTO.class, propertiesCity);			
				if(municipioList == null || municipioList.isEmpty()){
					throw new SystemException("Municipio no valido ("+idMunicipioCirculacion+")");
				}						
			}
		}catch(Exception e){
			LOG.error("Fallo al validar el estado-municipio", e);
			throw new SystemException(e.getMessage()!=null?e.getMessage():"Fallo al validar el estado-municipio");
		}
		
		folio = getFolio(folio, estilo);

		boolean isNewCot = evaluaFolioNuevaCotizacion(folio);	
		idCotizacion = getCotizacion(folio, agente, idNegocio, isNewCot, user);
		
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idCotizacion);

		//Recalcula si cambio negocio, paquete, estilo, modelo, marca, estadoDeCirculacion, municipioCirculacion, valorFactura, descuento
		boolean hasChanges = false;
		BigDecimal oldValorPrimaTotal = null;
		if(!isNewCot){
			oldValorPrimaTotal = cotizacion.getPrimaTarifa()!=null?BigDecimal.valueOf(cotizacion.getPrimaTarifa()):cotizacion.getValorPrimaTotal();
			List<IncisoCotizacionDTO> incisos = incisoService.getIncisos(idCotizacion);
			hasChanges = this.cambioDatosAuto(cotizacion, incisos, idNegocio, idPaquete, estilo, modelo, marca, idEstadoCirculacion,
					idMunicipioCirculacion, valorFactura,descuento);
		}
		
		//Guardar inciso
		IncisoCotizacionDTO incisoCotizacion = this.guardaInciso(cotizacion,idNegocio, idPaquete, estilo, modelo, marca, idEstadoCirculacion, 
				idMunicipioCirculacion, valorFactura,descuento,nombreAsegurado, apellidoPaterno, apellidoMaterno, numeroMotor, numeroSerie,
				codigoPostalCirculacion);
		
		//Recalcula cotizacion
		incisoCotizacion = calculoService.calcular(incisoCotizacion);
		ResumenCostosGeneralDTO resumenCostosGeneralDTO = calculoService.obtenerResumenGeneral(incisoCotizacion.getCotizacionDTO(), false);
		
		LOG.info("CREA COTIZACION>> Valor de hasChanges: "+hasChanges + " Valor de isNewCot: "+isNewCot+" IDCOTIZACION: "+cotizacion.getIdToCotizacion());
		/////-------------------------------------
		igualarPrimaCot(cotizacion, isNewCot, hasChanges, oldValorPrimaTotal, BigDecimal.valueOf(resumenCostosGeneralDTO.getCaratula().getPrimaTotal()));
		TerminarCotizacionDTO terminarCotizacionDTO = cotizacionService.terminarCotizacion(cotizacion.getIdToCotizacion(), false);
		cotizacionService.validaTerminarCotizacionWS(terminarCotizacionDTO);
		
		this.saveClienteCot(cotizacion.getIdToCotizacion(), nombreAsegurado, apellidoPaterno, apellidoMaterno, rfc, claveSexo, claveEstadoCivil, 
			idEstadoNacimiento,	fechaNacimiento, codigoPostal, idCiudad, idEstado, colonia, calleNumero, telefonoCasa, telefonoOficina, email);
		
		cotizacion = entidadService.findById(CotizacionDTO.class, idCotizacion);
		
		return cotizacion;						
	}

	private BigDecimal getCotizacion(String folio, Agente agente, Long idNegocio, boolean isNewCot, String user) {
		CotizacionDTO cotizacion;

		Date inicioVigencia = new Date();		
		
		Date finVigencia = getFechaVigencia(inicioVigencia);
		
		Negocio negocio = negocioService.findById(idNegocio);
		
		NegocioProducto negocioProducto = negocioProductoDao.getNegocioProductoByIdNegocioIdProducto(
				negocio.getIdToNegocio(), new BigDecimal (191));
		
		NegocioTipoPoliza negocioTipoPoliza = negocioTipoPolizaService.getPorIdNegocioProductoIdToTipoPoliza(
				negocioProducto.getIdToNegProducto(),
				negocioProducto.getProductoDTO().getTiposPoliza().get(0).getIdToTipoPoliza());

		negocioTipoPoliza.setNegocioProducto(negocioProducto);
		
		NegocioDerechoPoliza negocioDerechoPoliza = negocioDerechosService.obtenerDechosPolizaDefault(idNegocio);

		BigDecimal idFormaPago = new BigDecimal (negocioProducto.getNegocioFormaPagos().get(0).getFormaPagoDTO().getIdFormaPago());
		
		FormaPagoIDTO formaPago = new FormaPagoIDTO();
		try {
			formaPago = formaPagoInterfazServicios.getPorId(new Integer(idFormaPago.intValue()), (short) MonedaDTO.MONEDA_PESOS, null);
		} catch (SystemException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		
		if(isNewCot){			
			cotizacion = new CotizacionDTO();
			SolicitudDTO solicitud = new SolicitudDTO();		
			solicitud.setAgente(agente);		
			solicitud.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
			solicitud.setNegocio(negocio);
			cotizacion.setSolicitudDTO(solicitud);
			cotizacion.setNegocioTipoPoliza(negocioTipoPoliza);
			cotizacion.setFolio(folio);
			cotizacion.setTipoCotizacion(CotizacionDTO.TIPO_COTIZACION_AUTOPLAZO);			
			cotizacion = cotizacionService.crearCotizacion(cotizacion,user);			
			cotizacion.setNegocioDerechoPoliza(negocioDerechoPoliza);
			cotizacion.setValorDerechosUsuario(negocioDerechoPoliza.getImporteDerecho());
			cotizacion.setFechaInicioVigencia(inicioVigencia);
			cotizacion.setFechaFinVigencia(finVigencia);
			cotizacion.setIdFormaPago(idFormaPago);
			cotizacion.setIdMedioPago(new BigDecimal(negocioProducto
					.getNegocioMedioPagos().get(0).getMedioPagoDTO()
					.getIdMedioPago()));
			cotizacion.setIdMoneda(new BigDecimal(MonedaDTO.MONEDA_PESOS));
			cotizacion.setPorcentajePagoFraccionado(formaPago
					.getPorcentajeRecargoPagoFraccionado().doubleValue());
			cotizacion.setPorcentajebonifcomision(negocio.getPctPrimaCeder());
							
			cotizacionService.guardarCotizacion(cotizacion);

			generaComisiones(cotizacion);
			
		}else{
			List<CotizacionDTO> cotizacionByFolio = getCotizacionByFolio(folio);
			cotizacion = cotizacionByFolio.get(0);
			cotizacion.setNegocioDerechoPoliza(negocioDerechoPoliza);
			cotizacion.setValorDerechosUsuario(negocioDerechoPoliza.getImporteDerecho());
			cotizacion = actualizaDatosCotizacion(cotizacion, agente, negocio, negocioTipoPoliza, folio, user, formaPago, idFormaPago);			
		}
		
		return cotizacion.getIdToCotizacion();
	}

	private boolean cambioDatosAuto(CotizacionDTO cotizacion, List<IncisoCotizacionDTO> incisos, Long idNegocio, Long idPaquete, Integer estilo, 
			String modelo, Integer marca, String idEstadoCirculacion, String idMunicipioCirculacion, Double valorFactura, Double descuento) {
		final int claveEstiloLength = 5;
		boolean result = false;
		if(CollectionUtils.isNotEmpty(incisos)){
			IncisoCotizacionDTO incisoCotizacion = incisos.get(0);
			IncisoAutoCot incisoAutoCot = incisoCotizacion.getIncisoAutoCot();
			NegocioPaqueteSeccion negocioPaqueteSeccion = negocioPaqueteSeccionDao.findById(incisoAutoCot.getNegocioPaqueteId());
			Long oldIdPaquete = negocioPaqueteSeccion.getPaquete().getId(); 
			Long oldIdNegocio = negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(); 
			String oldEstilo = incisoAutoCot.getDescripcionFinal().substring(0, claveEstiloLength);
			Short oldModelo = incisoAutoCot.getModeloVehiculo();
			BigDecimal oldMarca = incisoAutoCot.getMarcaId();
			String oldIdEstadoCirculacion = incisoAutoCot.getEstadoId();
			String oldIdMunicipioCirculacion = incisoAutoCot.getMunicipioId();
			Double oldDescuento = incisoAutoCot.getPctDescuentoEstado();
			
			String newEstilo = StringUtils.leftPad(Integer.toString(estilo), claveEstiloLength,UtileriasWeb.STRING_CERO);
			
			if(idPaquete.intValue() != oldIdPaquete.intValue()){
				LOG.info("idPaquete antes: "+oldIdPaquete+" idPaquete nuevo: "+idPaquete);
				result = true;
			}else if(idNegocio.intValue() != oldIdNegocio.intValue()){
				LOG.info("idNegocio antes: "+oldIdNegocio+" idNegocio nuevo: "+idNegocio);
				result = true;
			}else if(!newEstilo.equals(oldEstilo)){
				LOG.info("estilo antes: "+oldEstilo+" estilo nuevo: "+newEstilo);
				result = true;
			}else if(oldModelo.compareTo(Short.valueOf(modelo)) != 0){
				LOG.info("modelo antes: "+oldModelo+" modelo nuevo: "+modelo);
				result = true;
			}else if(oldMarca.compareTo(BigDecimal.valueOf(marca)) != 0){
				LOG.info("marca antes: "+oldMarca+" marca nuevo: "+marca);
				result = true;
			}else if(!oldIdEstadoCirculacion.equals(idEstadoCirculacion)){
				LOG.info("idEstadoCirculacion antes: "+oldIdEstadoCirculacion+" idEstadoCirculacion nuevo: "+idEstadoCirculacion);
				result = true;
			}else if(!oldIdMunicipioCirculacion.equals(idMunicipioCirculacion)){
				LOG.info("idMunicipioCirculacion antes: "+oldIdMunicipioCirculacion+" idMunicipioCirculacion nuevo: "+idMunicipioCirculacion);
				result = true;
			}else if(oldDescuento.compareTo(descuento) != 0){
				LOG.info("descuento antes: "+oldDescuento+" descuento nuevo: "+descuento);
				result = true;
			}
		}
		
		return result;
	}

	private CotizacionDTO actualizaDatosCotizacion (CotizacionDTO cotizacion, Agente agente,Negocio negocio,
			NegocioTipoPoliza negocioTipoPoliza,String folio, String claveUsuario, FormaPagoIDTO formaPago, BigDecimal idFormaPago){
		
		SolicitudDTO solicitud = entidadService.findById(SolicitudDTO.class, 
				cotizacion.getSolicitudDTO().getIdToSolicitud());

		Date inicioVigencia = new Date();		
		Date finVigencia = getFechaVigencia(inicioVigencia);
		
		solicitud.setAgente(agente);		
		solicitud.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
		solicitud.setNegocio(negocio);
		cotizacion.setNegocioTipoPoliza(negocioTipoPoliza);
		cotizacion.setFolio(folio);
		
		NegocioProducto negocioProducto = cotizacion.getNegocioTipoPoliza().getNegocioProducto();
		negocioProducto = entidadService.findById(NegocioProducto.class,
				cotizacion.getNegocioTipoPoliza().getNegocioProducto().getIdToNegProducto());
		
		solicitud.setProductoDTO(negocioProducto.getProductoDTO());
		solicitud.setClaveEstatus(SolicitudDTO.Estatus.TERMINADA.getEstatus());
		solicitud.setFechaCreacion(Calendar.getInstance().getTime());
		solicitud.setClaveTipoSolicitud((short) 1);
		if (solicitud.getIdToPolizaAnterior() == null) {
			solicitud.setIdToPolizaAnterior(BigDecimal.ZERO);
		}
		
		if(agente != null){
			solicitud.setCodigoEjecutivo(new BigDecimal(agente.getPromotoria().getEjecutivo().getIdEjecutivo()));
			solicitud.setNombreAgente(agente.getPersona().getNombreCompleto());
			if(agente.getPromotoria().getEjecutivo() != null){
				Ejecutivo filtroEjecutivo =new Ejecutivo();
				filtroEjecutivo.setId(agente.getPromotoria().getEjecutivo().getId());
	            Ejecutivo ejecutivo = ejecutivoService.loadById(filtroEjecutivo);
	            if(ejecutivo != null){
					solicitud.setNombreEjecutivo(ejecutivo.getPersonaResponsable().getNombreCompleto());
					solicitud.setNombreOficina(ejecutivo.getGerencia().getPersonaResponsable().getNombreCompleto());
					solicitud.setIdOficina(new BigDecimal(ejecutivo.getGerencia().getId()));
	            }
			}					
			solicitud.setNombreOficinaAgente(agente.getPromotoria().getDescripcion());
		}			
		
        solicitud.setCodigoAgente(solicitud.getCodigoAgente()==null?BigDecimal.ZERO:solicitud.getCodigoAgente());
        solicitud.setClaveOpcionEmision((short) 0);
        solicitud.setFechaCreacion(new Date());
        solicitud.setCodigoUsuarioCreacion(claveUsuario);
        solicitud.setFechaModificacion(new Date());
        solicitud.setCodigoUsuarioModificacion(claveUsuario);
        solicitud.setClaveTipoPersona((short) 1);
        solicitud.setNombrePersona(claveUsuario);	        
      //Actualiza vigencia de la cotizacion
		cotizacion.setIdMoneda(new BigDecimal(MonedaDTO.MONEDA_PESOS));
		cotizacion.setPorcentajebonifcomision(negocio.getPctPrimaCeder());
		cotizacion.setIdFormaPago(idFormaPago);
		cotizacion.setPorcentajePagoFraccionado(formaPago
				.getPorcentajeRecargoPagoFraccionado().doubleValue());
		cotizacion.setFechaInicioVigencia(inicioVigencia);
		cotizacion.setFechaFinVigencia(finVigencia);
		cotizacion.setSolicitudDTO(solicitudFacadeRemote.update(solicitud));
		generaComisiones(cotizacion);
		return	cotizacionFacadeRemote.update(cotizacion);
	}
	
	private Agente getAgenteBySucursal (Integer idSucursal){
		Agente agente = new Agente();

		if(idSucursal == null || idSucursal.intValue() == 0){
			//sucursal default
			idSucursal = 1;
		}
		
        if (idSucursal != null && idSucursal.intValue() != 0){        	
        	LOG.info("Buscando Agente para idSucursal " + idSucursal);
            agente = agenteMidasService.getAgenteByIdSucursal(idSucursal);
		}		
		if(agente == null){
			 agente = agenteMidasService.getAgenteByIdSucursal(1);
		}
		return agente;
	}
	
	/**
	 * Guarda cliente y lo relaciona a la cotizacion
	 * @param idToCotizacion
	 * @param nombreAsegurado
	 * @param apellidoPaterno
	 * @param apellidoMaterno
	 * @param rfc
	 * @param claveSexo
	 * @param claveEstadoCivil
	 * @param idEstadoNacimiento
	 * @param fechaNacimiento
	 * @param codigoPostal
	 * @param idCiudad
	 * @param estado
	 * @param colonia
	 * @param calleNumero
	 * @param telefonoCasa
	 * @param telefonoOficina
	 * @param email
	 * @throws Exception 
	 */
	private void saveClienteCot (BigDecimal idToCotizacion,String nombreAsegurado,String apellidoPaterno,String apellidoMaterno,String rfc,String claveSexo,String claveEstadoCivil,
			  String idEstadoNacimiento,Date fechaNacimiento,String codigoPostal,String idCiudad,String idEstado,String colonia,String calleNumero,
			  String telefonoCasa,String telefonoOficina,String email){
		if(codigoPostal != null && !codigoPostal.isEmpty() && !UtileriasWeb.STRING_CERO.equalsIgnoreCase(codigoPostal)){
				
			//        CARGA INFORMACION DE CLIENTE
			LOG.info("Guardando datos del Cliente ");
			ClienteGenericoDTO cliente = new ClienteGenericoDTO();
			cliente.setApellidoPaterno(UtileriasWeb.getString(apellidoPaterno));
			cliente.setApellidoMaterno(UtileriasWeb.getString(apellidoMaterno));
			cliente.setNombre(UtileriasWeb.getString(nombreAsegurado));
			cliente.setNombreFiscal(cliente.getNombre());
			cliente.setApellidoPaternoFiscal(cliente.getApellidoPaterno());
			cliente.setApellidoMaternoFiscal(cliente.getApellidoMaterno());
			cliente.setNombreColonia(UtileriasWeb.getString(colonia));
			cliente.setNombreColoniaFiscal(cliente.getNombreColonia());
			cliente.setNombreColoniaCobranza(cliente.getNombreColonia());
			cliente.setCodigoRFC(UtileriasWeb.getString(rfc));
			cliente.setClaveNacionalidad(UtileriasWeb.KEY_PAIS_MEXICO);
			cliente.setTipoSituacionString("A");
			cliente.setEstadoCivil(claveEstadoCivil);
			cliente.setEstadoNacimiento(idEstadoNacimiento);		
			cliente.setIdPaisString(UtileriasWeb.KEY_PAIS_MEXICO);
			cliente.setIdEstadoString(UtileriasWeb.getString(idEstado));
			cliente.setIdMunicipioString(UtileriasWeb.getString(idCiudad));
			cliente.setCodigoPostal(codigoPostal != null ? String.format("%05d", Integer.parseInt(codigoPostal)) : UtileriasWeb.STRING_EMPTY);
			cliente.setNombreCalle(UtileriasWeb.getString(calleNumero));
			cliente.setNombreCalleCobranza(UtileriasWeb.getString(calleNumero));
			cliente.setNombreCalleFiscal(UtileriasWeb.getString(calleNumero));
			cliente.setIdEstadoCobranza(cliente.getIdEstadoString());
			cliente.setIdEstadoFiscal(cliente.getIdEstadoString());
			cliente.setIdMunicipioCobranza(cliente.getIdMunicipioString());
			cliente.setIdMunicipioFiscal(cliente.getIdMunicipioString());
			cliente.setCodigoPostalCobranza(cliente.getCodigoPostal());
			cliente.setCodigoPostalFiscal(cliente.getCodigoPostal());
			cliente.setClaveTipoPersona((short) 1);
			cliente.setEmail(UtileriasWeb.getString(email));
			cliente.setEmailCobranza(cliente.getEmail());
			cliente.setNumeroTelefono(telefonoCasa);
			cliente.setTelefono(telefonoCasa);
			cliente.setTelefonoOficinaContacto(telefonoOficina);
			cliente.setSexo(claveSexo);
			cliente.setFechaNacimiento(fechaNacimiento != null ? fechaNacimiento: new Date());
		
			try {
				cliente = clienteFacade.saveFullData(cliente, null, false);
			} catch (Exception e) {
				LOG.error("Ocurrio un error al guardar cliente", e);
			}
			if (cliente != null) {
				//asociamos el cliente con la cotizacion
				LOG.info("idCliente => " + cliente.getIdClienteString());
				cotizacionService.actualizarDatosContratanteCotizacion(	idToCotizacion, cliente.getIdCliente(),	null);		
			}
		}
	}	
	
	/**
	 * Guarda inciso a cotizacion segun sean las caracteristicas del vehiculo
	 * @param cotizacion
	 * @param idPaquete
	 * @param estilo
	 * @param modelo
	 * @param marca
	 * @param idEstado
	 * @param idMunicipio
	 * @param valorFactura
	 * @return InciscoCotizacionDTO
	 * @throws SystemException 
	 */
	private IncisoCotizacionDTO guardaInciso(CotizacionDTO cotizacion,Long idNegocio, Long idPaquete, 
			Integer estilo, String modelo, Integer marca, String idEstadoCirculacion,
			String idMunicipioCirculacion, Double valorFactura,Double descuento,String nombreAsegurado,
			String apellidoPaterno,String apellidoMaterno, String numeroMotor, String numeroSerie,
			String codigoPostalCirculacion) throws SystemException{
		final int claveEstiloLength = 5;
		LOG.info("Entrando a guardarInciso \n idToCotizacion => " + cotizacion.getIdToCotizacion());
		LOG.info("folio => " + cotizacion.getFolio() + "\n idNegocio => " + idNegocio + "\n marca => " + marca);
		LOG.info("estilo => " + estilo+ "\n modelo => " + modelo+ " \n valorFactura => " + valorFactura);
		LOG.info("idEstadoCirculacion => " + idEstadoCirculacion+" \n idMunicipioCirculacion => " + idMunicipioCirculacion);
		LOG.info("codigoPostalCirculacion => " + codigoPostalCirculacion);

		String claveEstilo = StringUtils.leftPad(Integer.toString(estilo), 
				claveEstiloLength,UtileriasWeb.STRING_CERO);
		SeccionDTO seccion = new SeccionDTO();
		List<EstiloVehiculoDTO> estiloVehiculoList = estiloVehiculoFacade.findByProperty("id.claveEstilo", claveEstilo);
		Long tipoUso = null;
		boolean isNegocioBase = idNegocio.longValue() == 516;
		boolean isAuto = estiloVehiculoList.get(0).getIdTcTipoVehiculo().intValue() == 1;
		BigDecimal seccionId = null;
		Long numeroSecuencia = 1l;
		
		if(isAuto){
			seccionId = isNegocioBase ? SeccionDTO.LINEA_NEGOCIO_AUT_INDIVIDUALES : SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_AUTOS;
			tipoUso = 9l;
		}else{
			seccionId = isNegocioBase ? SeccionDTO.LINEA_NEGOCIO_CAM_INDIVIDUALES : SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_CAMIONES;
			tipoUso = 19l;
		}
		seccion = seccionService.findById(seccionId);
		seccionId =seccion.getIdToSeccion();
		LOG.debug("lineaNegocio => " + seccion.getDescripcion());

		IncisoCotizacionId id = new IncisoCotizacionId();
		id.setIdToCotizacion(cotizacion.getIdToCotizacion());

		String mesesValorFactura = Paquete.AMPLIA.intValue() == 1 ? "24" : "12";
		final String condicionEspecialEmpleado = isNegocioBase ? CONDICIONES_ESPECIALES_EMPLEADOS : UtileriasWeb.STRING_EMPTY;
		
		Negocio negocio = negocioService.findById(idNegocio);
		
		cotizacionFacadeRemote.update(cotizacion);
		
		NegocioSeccion negocioSeccion = getNegocioSeccion(negocio, seccion.getDescripcion());
		
		NegocioPaqueteSeccion negocioPaqueteSeccion = negocioPaqueteSeccionDao.getPorIdNegSeccionIdPaquete(
				negocioSeccion.getIdToNegSeccion(),idPaquete != null ? idPaquete : Paquete.AMPLIA);		
		
		NegocioAgrupadorTarifaSeccion negAgrupadorTarifaSeccion = negAgrupadorTarifaSeccionDao.buscarPorNegocioSeccionYMoneda(negocioSeccion, 
				new BigDecimal(MonedaDTO.MONEDA_PESOS));
		TarifaAgrupadorTarifa tarifaAgrupador = tarifaAgrupadorService.getPorNegocioAgrupadorTarifaSeccion(negAgrupadorTarifaSeccion);
		
		IncisoCotizacionDTO incisoCotizacion = getIncisoCotizacionDTO(cotizacion, seccionId, id);
		
		IncisoAutoCot incisoAuto = getIncisoAutoCot(incisoCotizacion.getIncisoAutoCot(),claveEstilo, marca, tarifaAgrupador.getId().getIdVertarifa(), 
				nombreAsegurado, apellidoPaterno, apellidoMaterno, idEstadoCirculacion, idMunicipioCirculacion, modelo, negocioSeccion.getIdToNegSeccion(),
				negocioPaqueteSeccion.getIdToNegPaqueteSeccion(), tipoUso, mesesValorFactura, condicionEspecialEmpleado, numeroMotor, numeroSerie,
				codigoPostalCirculacion);
	
		incisoAuto.setPctDescuentoEstado(descuento);
		
		incisoCotizacion.setIncisoAutoCot(incisoAuto);

		List<CoberturaCotizacionDTO> coberturaCotizacionList = getCoberturas(negocioPaqueteSeccion, incisoAuto.getEstadoId(),
				incisoAuto.getMunicipioId(),incisoCotizacion.getId().getIdToCotizacion(), cotizacion.getIdMoneda().shortValue(), numeroSecuencia, 
				claveEstilo, incisoAuto.getModeloVehiculo().longValue(), new BigDecimal(incisoAuto.getTipoUsoId()),	valorFactura);
			
		if(!isAuto){			
			incisoCotizacion = this.datosAdicionalesPaquete(coberturaCotizacionList, cotizacion, incisoCotizacion);	
		}else{
			if(incisoCotizacion.getValorPrimaNeta()==null){
				incisoCotizacion.setValorPrimaNeta(0D);
			}
			if(incisoCotizacion.getValorPrimaNeta().compareTo(0D)==0){
				//guarda los nuevos montos de las coberturas
				incisoCotizacion = incisoService.prepareGuardarIncisoAgente(incisoCotizacion.getId().getIdToCotizacion(), incisoAuto, 
						incisoCotizacion, coberturaCotizacionList,null, null, null, null);
			}else{
				incisoCotizacion = incisoService.prepareGuardarInciso(incisoCotizacion.getId().getIdToCotizacion(), incisoAuto, 
						incisoCotizacion, coberturaCotizacionList);
			}
		}	
		return incisoCotizacion;	
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private IncisoCotizacionDTO  datosAdicionalesPaquete(List<CoberturaCotizacionDTO> coberturaCotizacionList,CotizacionDTO cotizacion,
			IncisoCotizacionDTO incisoCotizacion){
		List<DatoIncisoCotAuto> datoIncisoCotAutos = new ArrayList<DatoIncisoCotAuto>();
	
		CoberturaCotizacionDTO responsabilidadCivilCoberturaCotizacion = cargaMasivaService
		.getCoberturaCotizacion(coberturaCotizacionList, CoberturaDTO.RESPONSABILIDAD_CIVIL_AUTOS_NOMBRE_COMERCIAL);
		
			if (responsabilidadCivilCoberturaCotizacion != null && responsabilidadCivilCoberturaCotizacion.getClaveContratoBoolean()) {
				DatoIncisoCotAuto datoIncisoCotAutoNumeroRemolques = cargaMasivaService.obtieneDatoIncisoCotAutoSubRamo(cotizacion, 
						responsabilidadCivilCoberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(), 
						ConfiguracionDatoInciso.DescripcionEtiqueta.NUMERO_REMOLQUES, String.valueOf(0));
				
				datoIncisoCotAutos.add(datoIncisoCotAutoNumeroRemolques);
			}
			
			CoberturaCotizacionDTO danosOcasionadosPorCargaCoberturaCotizacion = cargaMasivaService
			.getCoberturaCotizacion(coberturaCotizacionList, CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL);
			
			DatoIncisoCotAuto datoIncisoCotAutoTipoCarga = cargaMasivaService
			.obtieneDatoIncisoCotAutoCobertura(cotizacion,
					danosOcasionadosPorCargaCoberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(),
					ConfiguracionDatoInciso.DescripcionEtiqueta.TIPO_CARGA, String.valueOf(10));
			
			datoIncisoCotAutos.add(datoIncisoCotAutoTipoCarga);
			
			IncisoCotizacionDTO inciso = cargaMasivaService.guardaIncisoCotizacion(incisoCotizacion,
					coberturaCotizacionList, datoIncisoCotAutos);			
			
			return inciso;
	}

	
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public  PolizaDTO emitir(BigDecimal idCotizacion, String numeroCredito, String numeroSerie,
    		String numeroMotor, BigDecimal idToPoliza,  String folio)throws SystemException{
    			
		return emision(idCotizacion, numeroCredito, numeroSerie, numeroMotor, idToPoliza,  folio, null);
	}

	@Override
	public List<String> listPaquetesByIdNegocio(Long idNegocio) {
        LOG.info("Entrando a listPaqueteByIdNegocio ");
        LOG.info("idNegocio => " + idNegocio);
        
		List<String> paquetes = new ArrayList<String>(1);
		RelacionesNegocioPaqueteDTO relacionesNegocioPaqueteDTO = new RelacionesNegocioPaqueteDTO();
		String seccion = idNegocio.longValue() == 516 ? "AUTOMOVILES INDIVIDUALES" : "AUTOPLAZO AUTOS";
		
		Negocio negocio = negocioService.findById(idNegocio);
		
		NegocioSeccion negocioSeccion = getNegocioSeccion(negocio, seccion);
		
		relacionesNegocioPaqueteDTO = negocioPaqueteSeccionService.getRelationLists(negocioSeccion);
		
		for (NegocioPaqueteSeccion negocioPaqueteSeccion : relacionesNegocioPaqueteDTO.getAsociadas()) {
			if (idNegocio.intValue() == 491){
				if (negocioPaqueteSeccion.getPaquete().getId().intValue() == 1){
					paquetes.add(negocioPaqueteSeccion.getPaquete().getId().toString()+"-AMPLIA PLUS");
				}else{
					paquetes.add(negocioPaqueteSeccion.getPaquete().getId().toString()+"-AMPLIA");
				}
			}else{
				paquetes.add(negocioPaqueteSeccion.getPaquete().getId().toString() + UtileriasWeb.SEPARADOR_MEDIO + negocioPaqueteSeccion.getPaquete().getDescripcion());				
			}	
		}
		
		return paquetes;
	}
	
	private boolean evaluaFolioNuevaCotizacion(String folio){
		//Revisamos si el folio ya tiene asignada una cotizacion en el sistema.
		List<CotizacionDTO> cotizacionByFolio = getCotizacionByFolio(folio);
		boolean creaNuevaCotizacion = true;		
		
		if(cotizacionByFolio != null && cotizacionByFolio.size() > 0){
			
			if(!cotizacionByFolio.get(0).getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EMITIDA)){
				creaNuevaCotizacion = false;	
			}
		}
		
		return creaNuevaCotizacion;		
	}
	
	private void cancelaPoliza(String folio, BigDecimal idToPoliza){
		if(idToPoliza!=null && idToPoliza.intValue()>0){
			this.cancelaPoliza(idToPoliza);
		}
		
		List<CotizacionDTO> cotizacionByFolio = getCotizacionByFolio(folio);
		for(CotizacionDTO cotizacion : cotizacionByFolio){
			if(cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EMITIDA)){
				List<PolizaDTO> polizas = entidadService.findByProperty(PolizaDTO.class,
						"cotizacionDTO.idToCotizacion", cotizacion.getIdToCotizacion());
				for(PolizaDTO poliza: polizas){
					if(poliza.getIdToPoliza().compareTo(idToPoliza)!=0 && 
							PolizaDTO.CLAVE_POLIZA_VIGENTE.equals(poliza.getClaveEstatus())){
						try{
							this.cancelaPoliza(poliza.getIdToPoliza());
						}catch(RuntimeException e){
				        	throw new RuntimeException(e.getMessage(), e);
						}
					}
				}
			}
		}		
	}

	@Override
	public byte[] imprimirPoliza(BigDecimal idPoliza) {
		long ultimoEndosoValidFromDateTimeStr = 0l;
		long ultimoEndosoRecordFromDateTimeStr = 0l;
		Date fechaInicio = new Date();
	
		try{
			EndosoDTO ultimoEndosoDTO = endosoService.getUltimoEndosoByValidFrom(idPoliza.longValue(), fechaInicio);
			
			if (ultimoEndosoDTO != null) {
				ultimoEndosoValidFromDateTimeStr = ultimoEndosoDTO.getValidFrom().getTime();
				ultimoEndosoRecordFromDateTimeStr = ultimoEndosoDTO.getRecordFrom().getTime();
			} else {
				ultimoEndosoValidFromDateTimeStr = fechaInicio.getTime();
				ultimoEndosoRecordFromDateTimeStr = fechaInicio.getTime();					
			}
			
			 DateTime validOnDT = new DateTime(ultimoEndosoValidFromDateTimeStr);
			 DateTime recordFromDT = new DateTime(ultimoEndosoRecordFromDateTimeStr);
			 Locale locale = new Locale("es", "MX");
	
			TransporteImpresionDTO transporte = 
				generaPlantillaReporteBitemporalService.imprimirPoliza(idPoliza,
						 locale,  validOnDT,  recordFromDT,
						false, true,true,
						true, 1,1,
						 (short) 2,Boolean.TRUE,
						false); 
			return transporte.getByteArray();				
		 }catch (Exception e) {
			return UtileriasWeb.STRING_EMPTY.getBytes();
		 }	
	}
	
	private void cancelaPoliza (BigDecimal idToPoliza){
		 if (idToPoliza != null && idToPoliza.intValue()>0){
            try {	           
		        LOG.info("Cancelando Poliza " + idToPoliza);
		        Date fechaIniVigenciaEndoso = null;
		        BitemporalCotizacion bitCotizacion = null;
		        BigDecimal idToSolicitud = null;
		        
	            //se obtiene la poliza
	            PolizaDTO poliza = entidadService.findById(PolizaDTO.class,
	                    idToPoliza);
	            fechaIniVigenciaEndoso = poliza.getCotizacionDTO().getFechaInicioVigencia();
	
	            //
	            bitCotizacion = endosoPolizaService.getCotizacionEndosoCancelacionPoliza(idToPoliza, fechaIniVigenciaEndoso, "1", (short)18);
	
	            if(bitCotizacion!= null){
	                idToSolicitud = bitCotizacion.getValue().getSolicitud().getIdToSolicitud();
	            }
	
	            //PrepareCotizacion
	            bitCotizacion = endosoPolizaService.prepareBitemporalEndoso(bitCotizacion.getContinuity().getId(), 
	            		TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	
	            //Crea Cotizacion
	            endosoPolizaService.guardaCotizacionEndosoCancelacionPoliza(bitCotizacion,idToSolicitud,
	                    fechaIniVigenciaEndoso);
	            //prepare emitir
	            bitCotizacion = endosoPolizaService.prepareBitemporalEndoso(bitCotizacion.getContinuity().getId(),
	                     TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	
	            //emitir
	            emisionEndosoBitemporalService.emiteEndoso(bitCotizacion.getContinuity().getId(),
	                    TimeUtils.getDateTime(fechaIniVigenciaEndoso),
	                    bitCotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
	
	            LOG.info("La poliza idToPoliza => " + idToPoliza + " fue cancelada con exito");
	        } catch(NegocioEJBExeption ne){
	        	LOG.error("No fue posible cancelar la poliza " + idToPoliza, ne);
	        	throw new RuntimeException(Utilerias.getMensajeRecurso(SistemaPersistencia.RECURSO_MENSAJES_EXCEPCIONES, ne.getErrorCode()), ne );
	        }catch (Exception e) {
			    LOG.error("No fue posible cancelar la poliza " + idToPoliza, e);
			}
		}
	}

	private IncisoCotizacionDTO getIncisoCotizacionDTO(CotizacionDTO cotizacion, BigDecimal seccionId,
			IncisoCotizacionId id) {
		IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
		IncisoAutoCot incisoAuto = new IncisoAutoCot();
		if(cotizacion.getIncisoCotizacionDTOs()!=null && cotizacion.getIncisoCotizacionDTOs().size() > 0){
			incisoCotizacion = cotizacion.getIncisoCotizacionDTOs().get(0);
			
			if(incisoCotizacion.getSeccionCotizacion().getSeccionDTO().getIdToSeccion().longValue() != seccionId.longValue()){
				incisoService.borrarInciso(incisoCotizacion);
				incisoCotizacion = new IncisoCotizacionDTO();
				incisoCotizacion.setId(id);
				incisoCotizacion.setCotizacionDTO(cotizacion);
				incisoCotizacion.setIdToSeccion(seccionId);
			}else{
				incisoAuto = incisoCotizacion.getIncisoAutoCot();
			}			
		}
		else{			
			incisoCotizacion = new IncisoCotizacionDTO();
			incisoCotizacion.setId(id);
			incisoCotizacion.setCotizacionDTO(cotizacion);
			incisoCotizacion.setIdToSeccion(seccionId);
		}		
		incisoCotizacion.setIncisoAutoCot(incisoAuto);
		
		return incisoCotizacion;
	}

	private IncisoAutoCot getIncisoAutoCot(IncisoAutoCot incisoAutoCot, String claveEstilo, Integer marca, Long tarifaAgrupadorId, 
			String nombreAsegurado, String apellidoPaterno, String apellidoMaterno, String idEstadoCirculacion, String idMunicipioCirculacion, 
			String modelo, BigDecimal idToNegSeccion, Long idToNegPaqueteSeccion, Long tipoUso, String mesesValorFactura, 
			String condicionEspecialEmpleado, String numeroMotor, String numeroSerie, String codigoPostalCirculacion ) throws SystemException {
		IncisoAutoCot incisoAuto = incisoAutoCot;
		
		MarcaVehiculoDTO marcaVehiculo = marcaVehiculoFacade.findById(new BigDecimal(marca.intValue()));
		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(marcaVehiculo.getTipoBienAutosDTO().getClaveTipoBien(), claveEstilo, 
				new BigDecimal(tarifaAgrupadorId));
		
		EstiloVehiculoDTO estiloVehiculo = new EstiloVehiculoDTO();
		estiloVehiculo = estiloVehiculoFacade.findById(estiloVehiculoId);		
		if(estiloVehiculo==null){
			throw new SystemException("No se encontraron estilos para la clave: " + claveEstilo);
		}
		
		ModeloVehiculoId modeloVehiculoId = new ModeloVehiculoId();
		modeloVehiculoId.setClaveEstilo(claveEstilo);
		modeloVehiculoId.setClaveTipoBien(marcaVehiculo.getTipoBienAutosDTO().getClaveTipoBien());
		modeloVehiculoId.setIdMoneda(new BigDecimal(MonedaDTO.MONEDA_PESOS));
		modeloVehiculoId.setIdVersionCarga(new BigDecimal(tarifaAgrupadorId));
		modeloVehiculoId.setModeloVehiculo(new Short(modelo));
		ModeloVehiculoDTO modeloVehiculo = modeloVehiculoFacade.findById(modeloVehiculoId);		
		if (modeloVehiculo == null){
			throw new SystemException("No fue posible encontrar el modelo "+modelo+" dado");
		}	
		
		//nombre conductor
		incisoAuto.setNombreConductor(nombreAsegurado);
		incisoAuto.setPaternoConductor(apellidoPaterno);
		incisoAuto.setMaternoConductor(apellidoMaterno);		
		incisoAuto.setEstadoId(idEstadoCirculacion);
		incisoAuto.setMunicipioId(idMunicipioCirculacion);
		incisoAuto.setMarcaId(marcaVehiculo.getIdTcMarcaVehiculo());		
		incisoAuto.setDescripcionFinal(claveEstilo.concat(" - ").concat(estiloVehiculo.getDescripcionEstilo()));
		incisoAuto.setEstiloId(marcaVehiculo.getTipoBienAutosDTO().getClaveTipoBien()
				.concat(UtileriasWeb.SEPARADOR).concat(claveEstilo)
				.concat(UtileriasWeb.SEPARADOR).concat(tarifaAgrupadorId.toString()));
		incisoAuto.setModeloVehiculo(new Short(modelo));
		incisoAuto.setNegocioSeccionId(idToNegSeccion.longValue());
		incisoAuto.setClaveTipoBien(marcaVehiculo.getTipoBienAutosDTO().getClaveTipoBien());
		incisoAuto.setNegocioPaqueteId(idToNegPaqueteSeccion);
		// Tipo de Uso Normal
		incisoAuto.setTipoUsoId(tipoUso);
		incisoAuto.setNumeroMotor(numeroMotor);
		incisoAuto.setNumeroSerie(numeroSerie);
		incisoAuto.setAsociadaCotizacion(new Integer(1));
		incisoAuto.setObservacionesinciso(String.format(Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.cotizacion.texto.observaciones.inciso"),
				mesesValorFactura, condicionEspecialEmpleado));
		incisoAutoCot.setCodigoPostal(Utilerias.obtenerLong(codigoPostalCirculacion));
		return incisoAuto;
	}
	
	/**
	 * Obtenermos el listado de las coberturas para la cotizacion
	 * @param negocioPaqueteSeccion
	 * @param estadoId
	 * @param municipioId
	 * @param idToCotizacion
	 * @param idMoneda
	 * @param numeroSecuencia
	 * @param claveEstilo
	 * @param modeloVehiculo
	 * @param tipoUsoId
	 * @param valorFactura
	 * @return
	 */
	private List<CoberturaCotizacionDTO> getCoberturas(NegocioPaqueteSeccion negocioPaqueteSeccion, String estadoId,
			String municipioId, BigDecimal idToCotizacion, short idMoneda,	Long numeroSecuencia, String claveEstilo, 
			long modeloVehiculo, BigDecimal tipoUsoId, Double valorFactura) {
		
		NegocioTipoPoliza negocioTipoPoliza = negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza();
		
		List<CoberturaCotizacionDTO> coberturaCotizacionList = coberturaService.getCoberturas(
				negocioTipoPoliza.getNegocioProducto().getNegocio().getIdToNegocio(), 
				negocioTipoPoliza.getNegocioProducto().getProductoDTO().getIdToProducto(), 
				negocioTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza(), 
				negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), 
				negocioPaqueteSeccion.getPaquete().getId(), estadoId, municipioId, idToCotizacion,
				idMoneda,	numeroSecuencia, claveEstilo, modeloVehiculo, tipoUsoId, null, null);		
		
		for (CoberturaCotizacionDTO cobertura : coberturaCotizacionList) {
			cobertura.setValorPrimaNeta(setDefaultDouble(cobertura.getValorPrimaNeta()));				
			cobertura.setValorSumaAsegurada(setDefaultDouble(cobertura.getValorSumaAsegurada()));
			cobertura.setValorCoaseguro(setDefaultDouble(cobertura.getValorCoaseguro()));
			cobertura.setValorDeducible(setDefaultDouble(cobertura.getValorDeducible()));
			cobertura.setValorCuota(setDefaultDouble(cobertura.getValorCuota()));
			cobertura.setValorCuotaOriginal(setDefaultDouble(cobertura.getValorCuotaOriginal()));
			cobertura.setValorCuotaOriginal(setDefaultDouble(cobertura.getValorCuotaOriginal()));
			cobertura.setPorcentajeCoaseguro(setDefaultDouble(cobertura.getPorcentajeCoaseguro()));
			cobertura.setPorcentajeDeducible(setDefaultDouble(cobertura.getPorcentajeDeducible()));
			cobertura.setClaveAutCoaseguro(setDefaultShort(cobertura.getClaveAutCoaseguro()));
			cobertura.setClaveAutDeducible(setDefaultShort(cobertura.getClaveAutDeducible()));
			
			if (cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_DANIOS_MATERIALES_VALOR_CONVENIDO) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_ROBO_TOTAL_VALOR_CONVENIDO) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_DANIOS_MATERIALES) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_ROBO_TOTAL) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_DANIOS_MATERIALES_VF) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_ROBO_VF) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_DANIOS_MATERIALES_CAMIONES) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_ROBO_CAMIONES) == 0) {
				cobertura.setValorSumaAsegurada(valorFactura);
			}			
		}
		
		return coberturaCotizacionList;
	}

	private Short setDefaultShort(Short claveAutCoaseguro) {
		if(claveAutCoaseguro!=null){
			if (claveAutCoaseguro.intValue() != 0){
				claveAutCoaseguro = (short)1;
			}else{
				claveAutCoaseguro = (short)0;
			}
		}
		return claveAutCoaseguro;
	}

	private Double setDefaultDouble(Double valor) {
		Double nuevoValor = valor;
		if(nuevoValor==null){
			nuevoValor = 0D;
		}
		return nuevoValor;
	}
	
	/**
	 * Agregar las comisiones en caso de la cotizacion creada no cuente con ellas 
	 * @param cotizacion
	 */
	private void generaComisiones(CotizacionDTO cotizacion) {
		BigDecimal idToCotizacion = cotizacion.getIdToCotizacion();
		
		List<ComisionCotizacionDTO> comisionCotizacionDTO = entidadService.findByProperty(
				ComisionCotizacionDTO.class, "id.idToCotizacion", idToCotizacion); 
		
		if (CollectionUtils.isEmpty(comisionCotizacionDTO)) {
			comisionService.generarComision(cotizacion);
		}
		
	}
	
	/**
	 * Permite obtener la fecha final de Vigencia
	 * @param inicioVigencia
	 * @return
	 */
	private Date getFechaVigencia(Date inicioVigencia) {	
		//LPV Se agrega un anio a la poliza para no afectar en anios bisiestos
		Calendar fechaC = Calendar.getInstance();
		fechaC.setTime(inicioVigencia);		
		fechaC.add(Calendar.MONTH, 12);
		return fechaC.getTime();
	}
	
	/**
	 * Se utiliza este metodo para obtener el folio en caso de que este venga null
	 * @param folio
	 * @param estilo
	 * @return
	 */
	private String getFolio(String folio, Integer estilo) {
		String folioNew = folio;
		
		if (folio.isEmpty()){
			final int lengthFiledId = 5;
			String claveEstilo = StringUtils.leftPad(Integer.toString(estilo), lengthFiledId,UtileriasWeb.STRING_CERO);
			List<EstiloVehiculoDTO> estiloVehiculoList = estiloVehiculoFacade.findByProperty("id.claveEstilo", claveEstilo);
			
			if(estiloVehiculoList.get(0).getIdTcTipoVehiculo().intValue() == 1){
				folioNew = DEFAULTONLINEAUTOS;
			}else{
				folioNew = DEFAULTONLINECAMIONES;
			}
		}
		return folioNew;
	}
	
	/**
	 * Obtenemo el Objecto NegocioSeccion para la cotizacion
	 * @param negocio
	 * @param descripcionSeccion
	 * @return
	 */
	private NegocioSeccion getNegocioSeccion(Negocio negocio, String descripcionSeccion) {
		NegocioProducto negocioProducto = negocioProductoDao.getNegocioProductoByIdNegocioIdProducto(negocio.getIdToNegocio(), new BigDecimal (191));

		NegocioTipoPoliza negocioTipoPoliza = negocioTipoPolizaService.getPorIdNegocioProductoIdToTipoPoliza(negocioProducto.getIdToNegProducto(),
						negocioProducto.getProductoDTO().getTiposPoliza().get(0).getIdToTipoPoliza());
		
		negocioTipoPoliza.setNegocioProducto(negocioProducto);		
		
		NegocioSeccion negocioSeccion = negocioSeccionService.getByProductoNegocioTipoPolizaSeccionDescripcion(
				negocioProducto.getProductoDTO(), negocio, negocioTipoPoliza.getTipoPolizaDTO(), descripcionSeccion);
		
		return negocioSeccion;
	}

	private String validalongitud(String campo, String valor, Integer valorMaximo) {
		String mensaje = UtileriasWeb.STRING_EMPTY;
		if(valor!=null && valor.length() > valorMaximo){
				mensaje = campo + " excede el maximo aceptado de "+ valorMaximo +". ";
		}
		return mensaje;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public PolizaDTO emision(BigDecimal idCotizacion, String numeroCredito, String numeroSerie, 
			String numeroMotor, BigDecimal idToPoliza, String folio, Cliente cliente)throws SystemException{
		
		LOG.info("Entrando a emitir poliza \n idCotizacion => " + idCotizacion);
    	LOG.info("numeroCredito => " + numeroCredito + "\n numeroSerie => " + numeroSerie);
    	LOG.info("numeroMotor => " + numeroMotor + "\n idToPoliza => " + idToPoliza);

		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class,
				idCotizacion);
		
		BigDecimal idToPolizaCot = validaCotizacionEmitida(cotizacion);
		if(idToPolizaCot==null){
			if (UtileriasWeb.esCadenaVacia(numeroMotor)){
				throw new SystemException("Error al emitir la poliza: Favor de validar el Numero de Motor del Vehiculo");
			}
			
			if (UtileriasWeb.esCadenaVacia(numeroSerie)){
				throw new SystemException("Error al emitir la poliza: Favor de validar el Numero de Serie del Vehiculo");
			}
			
		    try {
		    	this.cancelaPoliza(folio, idToPoliza);
		    }catch(Exception e){
		    	throw new SystemException("Error al intentar cancelar las polizas anteriores " +e.getMessage(), e);
		    }
		
			try {	
				
				if(cliente!=null && cliente.getCodigoPostal() != null && !cliente.getCodigoPostal().isEmpty() 
						&& !UtileriasWeb.STRING_CERO.equalsIgnoreCase(cliente.getCodigoPostal())){
			    	LOG.info("saveCliente => " + cliente.getNombres());
					this.saveClienteCot(cotizacion.getIdToCotizacion(), 
							cliente.getNombres(), cliente.getApellidoPaterno(), 
							cliente.getApellidoMaterno(), cliente.getRfc(), cliente.getClaveSexo(), 
							cliente.getClaveEstadoCivil(), cliente.getIdEstadoNacimiento(),	
							cliente.getFechaNacimiento(), cliente.getCodigoPostal(), 
							cliente.getIdCiudad(), cliente.getIdEstado(), cliente.getColonia(),
							cliente.getCalleYNumero(), cliente.getTelefonoCasa(), 
							cliente.getTelefonoOficina(), cliente.getEmail()); 
				}
				
				cotizacion = entidadService.findById(CotizacionDTO.class,
						idCotizacion);
				
				saveDatosVehiculo(numeroMotor, numeroSerie, cotizacion);
	
				saveCuentaPago(cotizacion, numeroCredito);
		
			} catch (Exception e) {
	            LOG.error("No se guardo el cliente", e);
			}
			
	        if(cotizacion.getIdToPersonaContratante() == null){
				throw new SystemException("Contratante no guardado en cotizacion, Revisar Codigo Postal");
			}
	        BigDecimal oldValorPrimaTotal = cotizacion.getPrimaTarifa()==null?null:BigDecimal.valueOf(cotizacion.getPrimaTarifa());
	        igualarPrimaCot(cotizacion, false, false, oldValorPrimaTotal, cotizacion.getValorPrimaTotal());
			
	        StringBuilder errores = validaEmisionCotizacion(cotizacion.getIdToCotizacion());
			if(errores != null){
				throw new SystemException("errores: " + errores);
			}
			idToPolizaCot = emitirCotizacion(cotizacion);
		}
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, idToPolizaCot);
		
		return poliza;
	}

	private void saveDatosVehiculo(String numeroMotor, String numeroSerie,
			CotizacionDTO cotizacion)throws SystemException {
		IncisoCotizacionDTO incisoCot = cotizacion.getIncisoCotizacionDTOs().get(0);
		
		IncisoAutoCot incisoAuto = incisoCot.getIncisoAutoCot();
		incisoAuto.setNumeroMotor(numeroMotor);
		incisoAuto.setNumeroSerie(numeroSerie);
		incisoAuto.setPlaca(UtileriasWeb.SEPARADOR_MEDIO);
		
		incisoAuto.setPersonaAseguradoId(cotizacion.getIdToPersonaContratante().longValue());
		incisoAuto.setNombreAsegurado(cotizacion.getNombreContratante());

		incisoService.guardarIncisoAutoCot(incisoAuto);
	}
	
	private void saveCuentaPago(CotizacionDTO cotizacion, String numeroCredito) {

		CuentaPagoDTO cuentaPagoDTO = new CuentaPagoDTO();
		cuentaPagoDTO.setTipoConductoCobro(CuentaPagoDTO.TipoCobro.EFECTIVO);
		cuentaPagoDTO.setTipoPersona(cotizacion.getSolicitudDTO()
				.getClaveTipoPersona().toString());
		
		cuentaPagoDTO.setIdCliente(cotizacion.getIdToPersonaContratante());

		cotizacion.setIdConductoCobroCliente(null);
		cotizacion.setIdMedioPago(cuentaPagoDTO.getTipoConductoCobro() != null ? 
				BigDecimal.valueOf(cuentaPagoDTO.getTipoConductoCobro().valor()) : null);
		cotizacion.setNumeroCredito(numeroCredito);
		entidadService.save(cotizacion);
		pagoService.guardarConductoCobroCotizacionAInciso(cotizacion
					.getIdToCotizacion());
		
	}
	/**
	 * JFGG
	 * @param cotizacionDto
	 * @return
	 */
	public  Map<String, Object> evalueExcepcion (CotizacionDTO cotizacionDto){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean res = false;
		try{
			if(cotizacionDto.getIgualacionNivelCotizacion() == null) {
				cotizacionDto.setIgualacionNivelCotizacion(false);
			}
			if(cotizacionDto.getValorPrimaTotal() == null) {
				cotizacionDto.setValorPrimaTotal(new BigDecimal(0));
			}
			
			List<? extends ExcepcionSuscripcionReporteDTO> listaExcepciones = evaluarCotizacion(cotizacionDto);
			
			//ExcepcionSuscripcion
			if(listaExcepciones != null && !listaExcepciones.isEmpty ()) {
				res = true;
				long idExcepciones = 0;
				for (ExcepcionSuscripcionReporteDTO item : listaExcepciones) {
					if ( idExcepciones == 0 ) { idExcepciones = item.getIdExcepcion (); }
				}
				map.put("mensaje", CotizacionExtService.MensajeExcepcion);
				map.put("idExcepcion", String.valueOf(idExcepciones));
			}
			map.put("statusExpetion", String.valueOf(res));
		} catch(Exception e) {
			LOG.error(this.getClass().getName() + ".EvalueExcepcion.Excepcion: " + e, e);
		}
		return map;
	}
	
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(CotizacionDTO cotizacion){
		ExcepcionSuscripcionNegocioAutosService excepcionSuscripcionNegocioAutosService = new ExcepcionSuscripcionNegocioAutosServiceImpl ();
		return excepcionSuscripcionNegocioAutosService.evaluarCotizacion(cotizacion, null);
	}
	private StringBuilder validaEmisionCotizacion(BigDecimal idToCotizacion) {
		TerminarCotizacionDTO validacion = cotizacionService.validarEmisionCotizacion(idToCotizacion);
		StringBuilder errores = null;
		if(validacion != null){
			errores = new StringBuilder();
			for(ExcepcionSuscripcionReporteDTO exception : validacion.getExcepcionesList()){
				if(errores.length() > 0){
					errores.append(UtileriasWeb.SEPARADOR_COMA);
				}
				errores.append(exception.getDescripcionExcepcion().concat(": ").concat(exception.getDescripcionRegistroConExcepcion()));
			}
		}
		return errores;
	}
	
	private BigDecimal emitirCotizacion(CotizacionDTO cotizacion) throws SystemException {		
        Map<String, String> result = new HashMap<String, String>(1);
        result = emisionService.emitir(cotizacion, false);
		if(!result.containsKey(PolizaDTO.IDPOLIZA_EMISON)){
			throw new SystemException("Error al emitir la poliza:" +result.get(PolizaDTO.MENSAJE_EMISON));
		}
		
		return new BigDecimal(result.get(PolizaDTO.IDPOLIZA_EMISON)); 
	}
	
	private BigDecimal validaCotizacionEmitida(CotizacionDTO cotizacion){
		BigDecimal idToPolizaNew = null;
        if(cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EMITIDA)){
        	List<PolizaDTO> polizas = entidadService.findByProperty(PolizaDTO.class,
					"cotizacionDTO.idToCotizacion", cotizacion.getIdToCotizacion());
    		idToPolizaNew = polizas.get(0).getIdToPoliza();
        }
		return idToPolizaNew;
	}

	private List<CotizacionDTO> getCotizacionByFolio(String folio) {
		List<CotizacionDTO> cotizacionByFolio = entidadService.findByProperty(CotizacionDTO.class, "folio", folio);
		return cotizacionByFolio;
	}
	
	private void igualarPrimaCot(CotizacionDTO cotizacion, boolean isNewCot, boolean hasChanges,
			BigDecimal oldValorPrimaTotal, BigDecimal primaTotal) {
		if(!isNewCot && !hasChanges && oldValorPrimaTotal!=null && primaTotal.compareTo(oldValorPrimaTotal) != 0){
			LOG.info("CREA COTIZACION >> Ingresa a calculoService.igualarPrima y calculoService.obtenerResumenGeneral");
			calculoService.igualarPrima(cotizacion.getIdToCotizacion(), oldValorPrimaTotal.doubleValue(), false);
			calculoService.obtenerResumenGeneral(cotizacion);
		}
	}
	
	private Date getFechaValidaDescuento(){
		Date fecha = null;
		try{
			HashMap<String,Object> properties = new HashMap<String,Object>(1);
			properties.put("id.idToGrupoParametroGeneral",ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERAL);
			properties.put("id.codigoParametroGeneral",ParametroGeneralDTO.CODIGO_PARAM_GENERAL_VALIDAR_NUEVO_DESC_AUTOPLAZO);
			
			List<ParametroGeneralDTO> list = entidadService.findByProperties(ParametroGeneralDTO.class, properties);
			
			fecha = Utilerias.getDateFromString(list.get(0).getValor(), SistemaPersistencia.FORMATO_FECHA);
		}catch(Exception e){
			LOG.error("Fallo al obtener fecha de validez de descuento", e);
		}
		return fecha;
	}
	
	private Double validaDescuento(Integer estilo, Long idNegocio, Long idPaquete, String idEstadoCirculacion, 
			Double descuento, String folio) throws SystemException{
				
		
		//Valida si folio fue creado antes de la obtencion del descuento
		Date fecha = getFechaValidaDescuento();
		try{
			List<CotizacionDTO> cotizacionList = this.getCotizacionByFolio(folio);
			
			if(cotizacionList != null && !cotizacionList.isEmpty()){
				CotizacionDTO cotizacion = cotizacionList.get(0);
				
				if(cotizacion != null && fecha != null && cotizacion.getFechaCreacion().before(fecha)){
					List<IncisoCotizacionDTO> incisos = incisoService.getIncisos(cotizacion.getIdToCotizacion());
					return incisos.get(0).getIncisoAutoCot().getPctDescuentoEstado();
				}
			}
		}catch(Exception e){
			LOG.error("Fallo al validar fecha de validez de descuento", e);
		}
		
		try{
			String claveEstilo = StringUtils.leftPad(Integer.toString(estilo), 5,UtileriasWeb.STRING_CERO);
			List<EstiloVehiculoDTO> estiloVehiculoList = estiloVehiculoFacade.findByProperty("id.claveEstilo", claveEstilo);
			boolean isNegocioBase = idNegocio.longValue() == Negocio.NEGOCIO_BASE_WS;
			boolean isAuto = estiloVehiculoList.get(0).getIdTcTipoVehiculo().intValue() == 1;
			BigDecimal seccionId;
			
			if(isAuto){
				seccionId = isNegocioBase ? SeccionDTO.LINEA_NEGOCIO_AUT_INDIVIDUALES : SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_AUTOS;
			}else{
				seccionId = isNegocioBase ? SeccionDTO.LINEA_NEGOCIO_CAM_INDIVIDUALES : SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_CAMIONES;
			}
			
			HashMap<String,Object> properties = new HashMap<String,Object>();
			properties.put("negocioSeccion.negocioTipoPoliza.negocioProducto.negocio.idToNegocio",idNegocio);
			properties.put("negocioSeccion.negocioTipoPoliza.negocioProducto.productoDTO.idToProducto",ProductoDTO.AUTOMOVILES_INDIVIDUALES);
			properties.put("negocioSeccion.seccionDTO.idToSeccion",seccionId);
			properties.put("paquete.id",idPaquete);
			List<NegocioPaqueteSeccion> negocioPaqueteList = entidadService.findByProperties(NegocioPaqueteSeccion.class, properties);
			
			if(negocioPaqueteList != null && !negocioPaqueteList.isEmpty() &&
					negocioPaqueteList.get(0).getAplicaPctDescuentoEdo() == 1){
				
				NegocioEstadoDescuento negocioEstadoDescuento = negocioEstadoDescuentoDao.findByNegocioAndEstado(idNegocio, idEstadoCirculacion);
				if(negocioEstadoDescuento != null){
					descuento = descuento!=null?descuento:0;
					descuento = negocioEstadoDescuento.getPctDescuentoDefault() + descuento;
					
					if(descuento > negocioEstadoDescuento.getPctDescuento()){
						throw new SystemException("Descuento mayor al maximo por estado ("+negocioEstadoDescuento.getPctDescuento()+")");
					}
				}
			}
		}catch(Exception e){
			LOG.error("Fallo al validar el descuento por estado", e);
			throw new SystemException(e.getMessage()!=null?e.getMessage():"Fallo al validar el descuento por estado");
		}
		return descuento;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CotizacionDTO creaCotizacion(CotizacionView cotizacionView) throws SystemException{
		
		ContratanteView contratante = cotizacionView.getContratante();
		
		if(cotizacionView.getDatosAdicionales()==null){
			throw new SystemException(
					"Debe especificar datosAdicionales:{idSucursal:'',claveUsuario:'',valorFactura:'',generaCotizacion:''}");
		}
		
		if(cotizacionView.getPaquete()==null){
			throw new SystemException(
					"Debe especificar paquete:{idPaquete:'',pctDescuentoEstado:'optional'}");
		}
		
		if(cotizacionView.getVehiculo()==null){
			throw new SystemException(
					"Debe especificar vehiculo:{idEstilo:'',modelo:'',idMarca:''}");
		}
		
		if(cotizacionView.getDatosPoliza()==null){
			throw new SystemException(
					"Debe especificar datosPoliza:{idNegocio:''}");
		}
		
		if(cotizacionView.getZonaCirculacion()==null){
			throw new SystemException(
					"Debe especificar zonaCirculacion:{idEstadoCirculacion:'',idMunicipioCirculacion:'',codigoPostalCirculacion:'optional'}");
		}
		
		return this.creaCotizacion(cotizacionView.getDatosAdicionales().getIdSucursal(),
				cotizacionView.getDatosAdicionales().getClaveUsuario(), 
				cotizacionView.getDatosPoliza().getIdNegocio(),
				cotizacionView.getPaquete().getPctDescuentoEstado(), 
				cotizacionView.getFolio(), 
				cotizacionView.getPaquete().getIdPaquete(),  
				cotizacionView.getVehiculo().getIdEstilo().intValue(),  
				cotizacionView.getVehiculo().getModelo().toString(),  
				cotizacionView.getVehiculo().getIdMarca().intValue(), 
				cotizacionView.getZonaCirculacion().getIdEstadoCirculacion(),
				cotizacionView.getZonaCirculacion().getIdMunicipioCirculacion(),  
				cotizacionView.getDatosAdicionales().getValorFactura(),	
				contratante!=null?contratante.getNombreContratante():null, 
				contratante!=null?contratante.getApellidoPaterno():null, 
				contratante!=null?contratante.getApellidoMaterno():null, 
				contratante!=null?contratante.getRfc():null, 
				contratante!=null?String.valueOf(contratante.getClaveSexo()):null,
				contratante!=null?String.valueOf(contratante.getClaveEstadoCivil()):null,
				contratante!=null?contratante.getIdEstadoNacimiento():null, 
				contratante!=null?this.getFechaNacimiento(contratante.getFechaNacimiento()):null,
				contratante!=null?contratante.getCodigoPostal():null,
				contratante!=null?contratante.getIdCiudad():null,
				contratante!=null?contratante.getIdEstado():null, 
				contratante!=null?contratante.getNombreColonia():null, 
				contratante!=null?contratante.getCalle():null,
				contratante!=null?contratante.getTelefonoCasa():null, 
				contratante!=null?contratante.getTelefonoOficina():null, 
				contratante!=null?contratante.getEmail():null,
				null, 
				null,
				cotizacionView.getZonaCirculacion().getCodigoPostalCirculacion());
	}
	
	private Date getFechaNacimiento(String fechaParam){
		Date fechaNacContratante = null;
		if (fechaParam!=null){			
			try{			 
	    		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
	    		fechaNacContratante = formatter.parse(fechaParam);
			}catch(Exception e){
				LOG.error("Fallo al obtener fecha de nacimiento", e);
			}	
		}
		return fechaNacContratante;
	}
}
