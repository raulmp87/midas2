package mx.com.afirme.midas2.service.impl.reaseguro.contraparte;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao;
import mx.com.afirme.midas2.dao.reaseguro.contraparte.ImporteVidaDao;
import mx.com.afirme.midas2.domain.reaseguro.contraparte.AjustesManuales;
import mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte.CargaContraParteDTO;
import mx.com.afirme.midas2.domain.reaseguro.contraparte.ContratosAutomaticos;
import mx.com.afirme.midas2.domain.reaseguro.contraparte.DistribucionSise;
import mx.com.afirme.midas2.domain.reaseguro.contraparte.PolizasFacultadas;
import mx.com.afirme.midas2.domain.reaseguro.contraparte.SiniestrosPendientes;
import mx.com.afirme.midas2.service.ExcelRowMapper;
import mx.com.afirme.midas2.service.impl.ExcelConverter;
import mx.com.afirme.midas2.service.impl.ExcelConverter.ExcelConfig.ExcelConfigBuilder;
import mx.com.afirme.midas2.service.reaseguro.datosContraparte.ImporteVidaService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class ImporteVidaServiceImpl implements ImporteVidaService {
	
	private InputStream is;
	
	private String resultado;
	private String extension;
	private static final String EXITOSO = "exitoso";
	private static final String ERROR = "error";
	private static final String EXISTENTE = "existente";
	private static final String RECARGA = "recarga";
	private static final String STATUS_PENDIENTE = "PENDIENTE";
	private static final String STATUS_PAGO_PARCIAL = "PAGO PARCIAL";
	private static final String TIPO_PENDIENTE = "1";
	private static final String TIPO_POL_FACULTADAS = "2";
	private static final String TIPO_CONT_AUTOMATICOS = "3";
	private static final String TIPO_SISE = "4";
	private static final String TIPO_AJUSTES = "5";
	private static final String ESTATUS = "CARGADO";
	private static final String ESTATCARGA = "bloqueado";
	private static final String CARGA = "carga";
	private static final int ESTATUSCARGA = 0;
	private static final BigDecimal PORCENTAJE_CESION = new BigDecimal(100);
	private static final String INVALIDO = "inv\u00e1lido";
	private BigDecimal tipoCambio = null;
	//private Date fechaCorte;
	private Date fCorte;
	CargaContraParteDTO cargaDTO;
	
	private List<SiniestrosPendientes> listaPendientes = new ArrayList<SiniestrosPendientes>();
	private List<PolizasFacultadas> listaPolFacultadas = new ArrayList<PolizasFacultadas>();
	private List<ContratosAutomaticos> listaContratosAutomaticos = new ArrayList<ContratosAutomaticos>();
	private List<DistribucionSise> listaDistribucionSise = new ArrayList<DistribucionSise>();
	private List<AjustesManuales> listaAjustesManuales = new ArrayList<AjustesManuales>();
	private List<String> listaErrores = new ArrayList<String>();
	
	
	

		
	@EJB
	private EntidadHistoricoDao entidadService;

	@EJB
	private SistemaContext sistemaContext;

	@EJB
	private ImporteVidaDao importeVidaDao;

	@Override
	public String procesarInfo(BigDecimal idToControlArchivo, String tipoArchivo, String fechaCorte, String usuario, String accion, BigDecimal tipoCambio) {
		
		String reporte = "";
		resultado = EXITOSO;
		listaErrores.clear();
		
		//try {
			//this.fechaCorte = getFechaFromString(fechaCorte);
			fCorte = Utilerias.obtenerFechaDeCadena(fechaCorte);
			this.tipoCambio = tipoCambio;
		/*} catch (ParseException e) {
			LogDeMidasEJB3.log("Falla al obtnener la fecha", Level.SEVERE, e);
		}*/
					
			// Cargar el archivo
			is = getArchivoDatos(idToControlArchivo);
			
			if(tipoArchivo.equalsIgnoreCase(TIPO_PENDIENTE)){
				
				int registros = importeVidaDao.getRegistros(fCorte, TIPO_PENDIENTE);
				int candado = importeVidaDao.estatusCarga(fCorte, "SINIESTROS_PENDIENTES");
				
				
				if (candado == ESTATUSCARGA){
					
					if (registros == 0 && accion.equalsIgnoreCase(CARGA)) {
						resultado = cargarSinPendientes(idToControlArchivo);
					}else if(registros > 0 && accion.equalsIgnoreCase(CARGA)){
						resultado = EXISTENTE;
					}else if(registros > 0 && accion.equalsIgnoreCase(RECARGA)){
						//importeVidaDao.borraRegistros(fCorte, TIPO_PENDIENTE);
						resultado = cargarSinPendientes(idToControlArchivo);
					}
					
					
					if(resultado.equalsIgnoreCase(EXITOSO)){
						
						if (registros == 0 && accion.equalsIgnoreCase(CARGA)) {
							reporte = "SINIESTROS_PENDIENTES";
							llenarCargaContraParteDTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
							importeVidaDao.insertaEstatusCarga(cargaDTO);
						}else{
							reporte = "SINIESTROS_PENDIENTES";
							llenarCargaContraParteDTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
							importeVidaDao.actualizaEstatusCarga(cargaDTO);
						}
					  }
				  }else{
						resultado = ESTATCARGA;
				  }
					
			}else if(tipoArchivo.equals(TIPO_POL_FACULTADAS)){
				
				int registros = importeVidaDao.getRegistros(fCorte, TIPO_POL_FACULTADAS);
				int candado = importeVidaDao.estatusCarga(fCorte, "POLIZAS_FACULTADAS");
				
				
				if (candado == ESTATUSCARGA){
					
					if (registros == 0 && accion.equalsIgnoreCase(CARGA)) {
						resultado = cargarPolizasFacultadas(idToControlArchivo);
					}else if(registros > 0 && accion.equalsIgnoreCase(CARGA)){
						resultado = EXISTENTE;
					}else if(registros > 0 && accion.equalsIgnoreCase(RECARGA)){
						//importeVidaDao.borraRegistros(fCorte, TIPO_POL_FACULTADAS);
						resultado = cargarPolizasFacultadas(idToControlArchivo);
					}
					
					
					if(resultado.equalsIgnoreCase(EXITOSO)){
						
						if (registros == 0 && accion.equalsIgnoreCase(CARGA)) {
							reporte = "POLIZAS_FACULTADAS";
							llenarCargaContraParteDTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
							importeVidaDao.insertaEstatusCarga(cargaDTO);
						}else{
							reporte = "POLIZAS_FACULTADAS";
							llenarCargaContraParteDTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
							importeVidaDao.actualizaEstatusCarga(cargaDTO);
						}
					  }
				  }else{
						resultado = ESTATCARGA;
				  }				
					
			}else if(tipoArchivo.equals(TIPO_CONT_AUTOMATICOS)){
				
				int registros = importeVidaDao.getRegistros(fCorte, TIPO_CONT_AUTOMATICOS);
				int candado = importeVidaDao.estatusCarga(fCorte, "CONTRATOS_AUTOMATICOS");
				
				
				if (candado == ESTATUSCARGA){
					
					if (registros == 0 && accion.equalsIgnoreCase(CARGA)) {
						resultado = cargarContratosAutomaticos(idToControlArchivo);
					}else if(registros > 0 && accion.equalsIgnoreCase(CARGA)){
						resultado = EXISTENTE;
					}else if(registros > 0 && accion.equalsIgnoreCase(RECARGA)){
						//importeVidaDao.borraRegistros(fCorte, TIPO_CONT_AUTOMATICOS);
						resultado = cargarContratosAutomaticos(idToControlArchivo);
					}
					
					
					if(resultado.equalsIgnoreCase(EXITOSO)){
						
						if (registros == 0 && accion.equalsIgnoreCase(CARGA)) {
							reporte = "CONTRATOS_AUTOMATICOS";
							llenarCargaContraParteDTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
							importeVidaDao.insertaEstatusCarga(cargaDTO);
						}else{
							reporte = "CONTRATOS_AUTOMATICOS";
							llenarCargaContraParteDTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
							importeVidaDao.actualizaEstatusCarga(cargaDTO);
						}
					  }
				  }else{
						resultado = ESTATCARGA;
				  }					
			}else if(tipoArchivo.equals(TIPO_SISE)){
				
				int registros = importeVidaDao.getRegistros(fCorte, TIPO_SISE);
				int candado = importeVidaDao.estatusCarga(fCorte, "DISTRIBUCION_SISE");
				
				
				if (candado == ESTATUSCARGA){
					
					if (registros == 0 && accion.equalsIgnoreCase(CARGA)) {
						resultado = cargarArchivoSise(idToControlArchivo);
					}else if(registros > 0 && accion.equalsIgnoreCase(CARGA)){
						resultado = EXISTENTE;
					}else if(registros > 0 && accion.equalsIgnoreCase(RECARGA)){
						//importeVidaDao.borraRegistros(fCorte, TIPO_SISE);
						resultado = cargarArchivoSise(idToControlArchivo);
					}
					
					
					if(resultado.equalsIgnoreCase(EXITOSO)){
						
						if (registros == 0 && accion.equalsIgnoreCase(CARGA)) {
							reporte = "SISE";
							llenarCargaContraParteDTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
							importeVidaDao.insertaEstatusCarga(cargaDTO);
						}else{
							reporte = "SISE";
							llenarCargaContraParteDTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
							importeVidaDao.actualizaEstatusCarga(cargaDTO);
						}
					  }
				  }else{
						resultado = ESTATCARGA;
				  }					
			}else if(tipoArchivo.equals(TIPO_AJUSTES)){
				
				int registros = importeVidaDao.getRegistros(fCorte, TIPO_AJUSTES);
				int candado = importeVidaDao.estatusCarga(fCorte, "AJUSTES_MANUALES");
				
				
				if (candado == ESTATUSCARGA){
					
					if (registros == 0 && accion.equalsIgnoreCase(CARGA)) {
						resultado = cargarAjustesManuales(idToControlArchivo);
					}else if(registros > 0 && accion.equalsIgnoreCase(CARGA)){
						resultado = EXISTENTE;
					}else if(registros > 0 && accion.equalsIgnoreCase(RECARGA)){
						//importeVidaDao.borraRegistros(fCorte, TIPO_SISE);
						resultado = cargarAjustesManuales(idToControlArchivo);
					}
					
					
					if(resultado.equalsIgnoreCase(EXITOSO)){
						
						if (registros == 0 && accion.equalsIgnoreCase(CARGA)) {
							reporte = "AJUSTES";
							llenarCargaContraParteDTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
							importeVidaDao.insertaEstatusCarga(cargaDTO);
						}else{
							reporte = "AJUSTES";
							llenarCargaContraParteDTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
							importeVidaDao.actualizaEstatusCarga(cargaDTO);
						}
					  }
				  }else{
						resultado = ESTATCARGA;
				  }					
			}

			// Regresa el resultado			
			if(resultado.equalsIgnoreCase(ERROR) && listaErrores.size()>0){
					resultado = obtenerResultado(listaErrores);
								
				}	
		return resultado;
	}
	
	public String obtenerResultado(List<String> listaErrores) {
		StringBuilder result = new StringBuilder("");
		for(String errores : listaErrores) {
			result.append(errores).append("<br>");
		}		
		return result.toString();
	}
	
	/**
	 * Procesa la informacion del reporte Siniestros Pendientes.
	 * @param idToControlArchivo
	 * @return resultado.
	 */		
	public String cargarSinPendientes(BigDecimal idToControlArchivo){
			
		try {
		// Cargar el archivo
		is = getArchivoDatos(idToControlArchivo);
		
		listaPendientes = listarSiniestrosPendientes();
		if(resultado.equals(EXITOSO)){
		
		//Primero se borra los registros encontrados para la fecha de corte.
	    importeVidaDao.borraRegistros(fCorte, TIPO_PENDIENTE);
		
		for (SiniestrosPendientes importe : listaPendientes) {
				if(importe.getStatus().equalsIgnoreCase(STATUS_PENDIENTE) || importe.getStatus().equalsIgnoreCase(STATUS_PAGO_PARCIAL)){
					importeVidaDao.insertaSiniestrosPendientes(
						importe.getPoliza(),
						importe.getMoneda(), 
						importe.getAnio_Siniestro(),
						importe.getNum_Siniestro(), 
						importe.getReserva(), 
						importe.getCosto_Siniestro(),
						importe.getAfirme(), 
						importe.getHannover_Life(),
						importe.getMafre(), 
						importe.getStatus(),
						importe.getfCorte(),
						importe.getInicioVigencia(),
						importe.getCobertura());
				    }
			     } 
		     }else{
		    	 resultado = ERROR;
		     }
		
		} catch (RuntimeException ex) {
			resultado = ERROR;
			LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
		}
			
		return resultado;
					
	}
	
	/**
	 * Procesa la informacion del reporte Polizas Facultadas.
	 * @param idToControlArchivo
	 * @return resultado.
	 */		
	public String cargarPolizasFacultadas(BigDecimal idToControlArchivo){
			
		try {			
		// Cargar el archivo
		is = getArchivoDatos(idToControlArchivo);
		
		listaPolFacultadas = listarPolizasFacultadas();
		if(resultado.equals(EXITOSO)){
			
			//Primero se borra los registros encontrados para la fecha de corte.
			importeVidaDao.borraRegistros(fCorte, TIPO_POL_FACULTADAS);
		
			for (PolizasFacultadas importe : listaPolFacultadas) {
				importeVidaDao.insertaPolizasFacultadas(
						importe.getPoliza(),
						importe.getMoneda(), 
						importe.getAnioSiniestro(),
						importe.getNumSiniestro(), 
						importe.getReserva(), 
						importe.getCostoSiniestro(),
						importe.getAfirme(), 
						importe.getMontoDistribucion(),
						importe.getStatus(),
						importe.getRgre(),
						importe.getReasegurador(),
						importe.getfCorte()); 
				
			  }
			
		     }else{
		    	 resultado = ERROR;
		     }
		
		} catch (RuntimeException ex) {
			resultado = ERROR;
			LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
		}
			
		return resultado;
					
	}
	
	/**
	 * Procesa la informacion del reporte Contratos Automaticos.
	 * @param idToControlArchivo
	 * @return resultado.
	 */		
	public String cargarContratosAutomaticos(BigDecimal idToControlArchivo){
		
		try {			
		// Cargar el archivo
		is = getArchivoDatos(idToControlArchivo);
		
		listaContratosAutomaticos=listarContratosAutomaticos();
		if(resultado.equals(EXITOSO)){
			
			//Primero se borra los registros encontrados para la fecha de corte.
			importeVidaDao.borraRegistros(fCorte, TIPO_CONT_AUTOMATICOS);
		
			for (ContratosAutomaticos importe : listaContratosAutomaticos) {
				importeVidaDao.insertaContratosAutomaticos(
						importe.getContrato(),
						importe.getReaseguradores(), 
						importe.getRgre(),
						importe.getParticipacion(),
						importe.getVigencia_Ini(),
						importe.getVigencia_Fin(),
						importe.getfCorte(),
						importe.getCobertura());
			 }
			
		     }else{
		    	 resultado = ERROR;
		     }
		
		} catch (RuntimeException ex) {
			resultado = ERROR;
			LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
		}
			
		return resultado;
					
	}
	
	/**
	 * Procesa la informacion del reporte SISE.
	 * @param idToControlArchivo
	 * @return resultado.
	 */		
	public String cargarArchivoSise(BigDecimal idToControlArchivo){
		
		try {			
		// Cargar el archivo
		is = getArchivoDatos(idToControlArchivo);
		
		listaDistribucionSise=listarDistribucionSise();
		if(resultado.equals(EXITOSO)){
		
			//Primero se borra los registros encontrados para la fecha de corte.
			importeVidaDao.borraRegistros(fCorte, TIPO_SISE);
			
			for (DistribucionSise importe : listaDistribucionSise) {
				importeVidaDao.insertaDistribucionSise(
						importe.getSiniestro(),
						importe.getReasegurador(), 
						importe.getRgre(),
						importe.getReserva(),
						importe.getMoneda(),
						importe.getTipoCambio(),
						importe.getfCorte());
			 }
			
		     }else{
		    	 resultado = ERROR;
		     }
		
		} catch (RuntimeException ex) {
			resultado = ERROR;
			LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
		}
			
		return resultado;
					
	}
	
	/**
	 * Procesa la informacion del reporte Ajustes Manuales.
	 * @param idToControlArchivo
	 * @return resultado.
	 */		
	public String cargarAjustesManuales(BigDecimal idToControlArchivo){
		
		try {			
		// Cargar el archivo
		is = getArchivoDatos(idToControlArchivo);
		
		listaAjustesManuales=listarAjustesManuales();
		if(resultado.equals(EXITOSO)){
		
			//Primero se borra los registros encontrados para la fecha de corte.
			importeVidaDao.borraRegistros(fCorte, TIPO_AJUSTES);
			
			for (AjustesManuales importe : listaAjustesManuales) {
				importeVidaDao.insertaAjustes(
						importe.getSiniestro(),
						importe.getReasegurador(), 
						importe.getCnsf(),
						importe.getMoneda(),
						importe.getReserva(),
						importe.getAnio(),
						importe.getSistema(),
						importe.getTipoCambio(),
						importe.getfCorte());
			 }
			
		     }else{
		    	 resultado = ERROR;
		     }
		
		} catch (RuntimeException ex) {
			resultado = ERROR;
			LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
		}
			
		return resultado;
					
	}

	private InputStream getArchivoDatos(BigDecimal idToControlArchivo) {

		InputStream archivo = null;
		ControlArchivoDTO controlArchivoDTO = entidadService.findById(ControlArchivoDTO.class, idToControlArchivo);
		String nombreCompletoArchivo = null;
		String nombreOriginalArchivo = controlArchivoDTO.getNombreArchivoOriginal();
		extension = (nombreOriginalArchivo.lastIndexOf('.') == -1) ? ""	: nombreOriginalArchivo.substring(
						nombreOriginalArchivo.lastIndexOf('.'),
						nombreOriginalArchivo.length());

		LogDeMidasEJB3.log("Buscando el archivo", Level.INFO, null);
		nombreCompletoArchivo = controlArchivoDTO.getIdToControlArchivo().toString() + extension;
		try {
			archivo = new FileInputStream(sistemaContext.getUploadFolder()
					+ nombreCompletoArchivo);
		} catch (FileNotFoundException re) {
			LogDeMidasEJB3.log("Archivo no encontrado", Level.SEVERE, re);
		}

		return archivo;
	}

	/**
	 * Convierte informacion del archivo excel Siniestros Pendientes.
	 * @return listarSiniestrosPendientes.
	 */	
	public List<SiniestrosPendientes> listarSiniestrosPendientes() {
		List<SiniestrosPendientes> listaSiniestrosPendientes = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());

			for (String sheetName : converter.getSheetNames()) {

				listaSiniestrosPendientes = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<SiniestrosPendientes>() {
							public SiniestrosPendientes mapRow(Map<String, String> row,
									int rowNum) {
								SiniestrosPendientes siniestrosPendientes = new SiniestrosPendientes(fCorte);
								try {
									validaPendientes(1, row.get("poliza"), rowNum);
									siniestrosPendientes.setPoliza(getPoliza(row.get("poliza")));
									validaPendientes(2, row.get("moneda"), rowNum);
									if(row.get("moneda") == null || row.get("moneda").equals("") || row.get("moneda").isEmpty()){
										siniestrosPendientes.setMoneda(0);
									}else{
										siniestrosPendientes.setMoneda(Integer.parseInt(row.get("moneda")));
									}
									validaPendientes(3, row.get("anio_siniestro"), rowNum);
									if(row.get("anio_siniestro") == null || row.get("anio_siniestro").equals("") || row.get("anio_siniestro").isEmpty()){
										siniestrosPendientes.setAnio_Siniestro(0);
									}else{
										siniestrosPendientes.setAnio_Siniestro(Integer.parseInt(row.get("anio_siniestro")));
									}									
									validaPendientes(4, row.get("num_siniestro"), rowNum);
									if(row.get("num_siniestro") == null || row.get("num_siniestro").equals("") || row.get("num_siniestro").isEmpty()){
										siniestrosPendientes.setNum_Siniestro(0);
									}else{
										siniestrosPendientes.setNum_Siniestro(Integer.parseInt(row.get("num_siniestro")));
									}									
									validaPendientes(5, row.get("reserva"), rowNum);
									if(row.get("reserva") == null || row.get("reserva").equals("") || row.get("reserva").isEmpty()){
										siniestrosPendientes.setReserva(BigDecimal.ZERO);
									}else{
									siniestrosPendientes.setReserva(new BigDecimal(row.get("reserva")));
									}
									validaPendientes(6, row.get("costo_siniestro"), rowNum);
									if(row.get("costo_siniestro") == null || row.get("costo_siniestro").equals("") || row.get("costo_siniestro").isEmpty()){
										siniestrosPendientes.setCosto_Siniestro(BigDecimal.ZERO);
									}else{
										siniestrosPendientes.setCosto_Siniestro(new BigDecimal(row.get("costo_siniestro")));
									}									
									validaPendientes(7, row.get("afirme"), rowNum);
									if(row.get("afirme") == null || row.get("afirme").equals("") || row.get("afirme").isEmpty()){
										siniestrosPendientes.setAfirme(BigDecimal.ZERO);
									}else{
										siniestrosPendientes.setAfirme(new BigDecimal(row.get("afirme")));
									}									
									validaPendientes(8, row.get("hannover_life-re"), rowNum);
									if(row.get("hannover_life-re") == null || row.get("hannover_life-re").equals("") || row.get("hannover_life-re").isEmpty()){
										siniestrosPendientes.setHannover_Life(BigDecimal.ZERO);
									}else{
										siniestrosPendientes.setHannover_Life(new BigDecimal(row.get("hannover_life-re")));
									}
									validaPendientes(9, row.get("mapfre-re"), rowNum);
									if(row.get("mapfre-re") == null || row.get("mapfre-re").equals("") || row.get("mapfre-re").isEmpty()){
										siniestrosPendientes.setMafre(BigDecimal.ZERO);
									}else{
										siniestrosPendientes.setMafre(new BigDecimal(row.get("mapfre-re")));
									}
									validaPendientes(10, row.get("status"), rowNum);
									if(row.get("status") == null || row.get("status").equals("") || row.get("status").isEmpty()){
										siniestrosPendientes.setStatus(INVALIDO);
									}else{
										siniestrosPendientes.setStatus(row.get("status"));
									}
									validaPendientes(11, row.get("inicio_vigencia"), rowNum);
									siniestrosPendientes.setInicioVigencia(getFechaFromString(row.get("inicio_vigencia")));
									siniestrosPendientes.setCobertura(row.get("cobertura"));

								} catch (NumberFormatException e) {
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
								} catch (ParseException e) {
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
									e.printStackTrace();
								}
								return siniestrosPendientes;
							}
						});
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaSiniestrosPendientes;
	}
	
	/**
	 * Convierte informacion del archivo excel Polizas Facultadas.
	 * @return listaPolizasFacultadas.
	 */	
	public List<PolizasFacultadas> listarPolizasFacultadas() {
		List<PolizasFacultadas> listaPolizasFacultadas = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());

			for (String sheetName : converter.getSheetNames()) {

				listaPolizasFacultadas = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<PolizasFacultadas>() {
							public PolizasFacultadas mapRow(Map<String, String> row,
									int rowNum) {
								PolizasFacultadas polizasFacultadas = new PolizasFacultadas(fCorte);
								try {
									validaPolFac(1, row.get("poliza"), rowNum);
									validaPendientes(1, row.get("poliza"), rowNum);
									polizasFacultadas.setPoliza(row.get("poliza"));
									validaPendientes(2, row.get("moneda"), rowNum);
									if(row.get("moneda") == null || row.get("moneda").equals("") || row.get("moneda").isEmpty()){
										polizasFacultadas.setMoneda(0);
									}else{
										polizasFacultadas.setMoneda(Integer.parseInt(row.get("moneda")));
									}
									validaPendientes(3, row.get("anio_siniestro"), rowNum);
									if(row.get("anio_siniestro") == null || row.get("anio_siniestro").equals("") || row.get("anio_siniestro").isEmpty()){
										polizasFacultadas.setAnioSiniestro(0);
									}else{
										polizasFacultadas.setAnioSiniestro(Integer.parseInt(row.get("anio_siniestro")));
									}									
									validaPendientes(4, row.get("num_siniestro"), rowNum);
									if(row.get("num_siniestro") == null || row.get("num_siniestro").equals("") || row.get("num_siniestro").isEmpty()){
										polizasFacultadas.setNumSiniestro(0);
									}else{
										polizasFacultadas.setNumSiniestro(Integer.parseInt(row.get("num_siniestro")));
									}									
									validaPendientes(5, row.get("reserva"), rowNum);
									if(row.get("reserva") == null || row.get("reserva").equals("") || row.get("reserva").isEmpty()){
										polizasFacultadas.setReserva(BigDecimal.ZERO);
									}else{
									polizasFacultadas.setReserva(new BigDecimal(row.get("reserva")));
									}
									validaPendientes(6, row.get("costo_siniestro"), rowNum);
									if(row.get("costo_siniestro") == null || row.get("costo_siniestro").equals("") || row.get("costo_siniestro").isEmpty()){
										polizasFacultadas.setCostoSiniestro(BigDecimal.ZERO);
									}else{
										polizasFacultadas.setCostoSiniestro(new BigDecimal(row.get("costo_siniestro")));
									}									
									validaPendientes(7, row.get("afirme"), rowNum);
									if(row.get("afirme") == null || row.get("afirme").equals("") || row.get("afirme").isEmpty()){
										polizasFacultadas.setAfirme(BigDecimal.ZERO);
									}else{
										polizasFacultadas.setAfirme(new BigDecimal(row.get("afirme")));
									}									
									validaPendientes(8, row.get("monto_distribucion"), rowNum);
									if(row.get("monto_distribucion") == null || row.get("monto_distribucion").equals("") || row.get("monto_distribucion").isEmpty()){
										polizasFacultadas.setMontoDistribucion(BigDecimal.ZERO);
									}else{
										polizasFacultadas.setMontoDistribucion(new BigDecimal(row.get("monto_distribucion")));
									}									
									validaPendientes(9, row.get("status"), rowNum);
									if(row.get("status") == null || row.get("status").equals("") || row.get("status").isEmpty()){
										polizasFacultadas.setStatus(INVALIDO);
									}else{
										polizasFacultadas.setStatus(row.get("status"));
									}
									validaPolFac(10, row.get("rgre"), rowNum);
									validaPendientes(10, row.get("rgre"), rowNum);
									if(row.get("rgre") == null || row.get("rgre").equals("") || row.get("rgre").isEmpty()){
										polizasFacultadas.setRgre(INVALIDO);
									}else{
										polizasFacultadas.setRgre(row.get("rgre"));
									}
									validaPolFac(11, row.get("reasegurador"), rowNum);
									validaPendientes(11, row.get("reasegurador"), rowNum);
									if(row.get("reasegurador") == null || row.get("reasegurador").equals("") || row.get("reasegurador").isEmpty()){
										polizasFacultadas.setReasegurador(INVALIDO);
									}else{
										polizasFacultadas.setReasegurador(row.get("reasegurador"));
									}	
								} catch (NumberFormatException e) {
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
								}
								return polizasFacultadas;
							}
						});
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaPolizasFacultadas;
	}
	
	
	/**
	 * Convierte informacion del archivo excel Contratos Automaticos.
	 * @return listaContratosAutomaticos.
	 */	
	public List<ContratosAutomaticos> listarContratosAutomaticos() {
		List<ContratosAutomaticos> listaContratosAutomaticos = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());

			for (String sheetName : converter.getSheetNames()) {

				listaContratosAutomaticos = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<ContratosAutomaticos>() {
							public ContratosAutomaticos mapRow(Map<String, String> row,
									int rowNum) {
								ContratosAutomaticos contratosAutomaticos = new ContratosAutomaticos(fCorte);
								try {
									validaContratosAutomaticos(1, row.get("contrato"), rowNum);
									if(row.get("contrato") == null || row.get("contrato").equals("") || row.get("contrato").isEmpty()){
										contratosAutomaticos.setContrato(INVALIDO);
									}else{
										contratosAutomaticos.setContrato(row.get("contrato"));
									}
									if(row.get("reaseguradores") == null || row.get("reaseguradores").equals("") || row.get("reaseguradores").isEmpty()){
										contratosAutomaticos.setReaseguradores(INVALIDO);
									}else{
										contratosAutomaticos.setReaseguradores(row.get("reaseguradores"));
									}
									validaContratosAutomaticos(3, row.get("rgre"), rowNum);
									if(row.get("rgre") == null || row.get("rgre").equals("") || row.get("rgre").isEmpty()){
										contratosAutomaticos.setReaseguradores(INVALIDO);
									}else{
										contratosAutomaticos.setRgre(row.get("rgre"));
									}
									validaContratosAutomaticos(4, row.get("participacion"), rowNum);	
									if(row.get("participacion") != null || !row.get("participacion").equals("") || !row.get("participacion").isEmpty()){
										if (new BigDecimal(row.get("participacion")).compareTo(BigDecimal.ONE) <= 0){
											contratosAutomaticos.setParticipacion(new BigDecimal(row.get("participacion")).multiply(PORCENTAJE_CESION));	
										}else{
											contratosAutomaticos.setParticipacion(new BigDecimal(row.get("participacion")));											
										}
									}
									validaContratosAutomaticos(5, row.get("inicio_vigencia"), rowNum);	
									contratosAutomaticos.setVigencia_Ini(getFechaFromString(row.get("inicio_vigencia")));
									validaContratosAutomaticos(6, row.get("fin_vigencia"), rowNum);	
									contratosAutomaticos.setVigencia_Fin(getFechaFromString(row.get("fin_vigencia")));
									contratosAutomaticos.setCobertura(row.get("cobertura"));
								} catch (NumberFormatException e) {
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
								} catch (ParseException e) {
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
								}
								return contratosAutomaticos;
							}
						});
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaContratosAutomaticos;
	}
	
	/**
	 * Convierte informacion del archivo excel SISE.
	 * @return listaDistribucionSise.
	 */	
	public List<DistribucionSise> listarDistribucionSise() {
		List<DistribucionSise> listaDistribucionSise = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());

			for (String sheetName : converter.getSheetNames()) {

				listaDistribucionSise = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<DistribucionSise>() {
							public DistribucionSise mapRow(Map<String, String> row,
									int rowNum) {
								DistribucionSise sise = new DistribucionSise(fCorte, tipoCambio);
								try {
									validaNulos(1, row.get("siniestro"), rowNum);
									if(row.get("siniestro") == null || row.get("siniestro").equals("") || row.get("siniestro").isEmpty()){
										sise.setSiniestro(0);
									}else{
										sise.setSiniestro(new BigDecimal(row.get("siniestro")).intValue());
									}
									if(row.get("reasegurador") == null || row.get("reasegurador").equals("") || row.get("reasegurador").isEmpty()){
										sise.setReasegurador(INVALIDO);
									}else{
										sise.setReasegurador(row.get("reasegurador"));
									}
									validaNulos(3, row.get("clave"), rowNum);
									if(row.get("clave") == null || row.get("clave").equals("") || row.get("clave").isEmpty()){
										sise.setRgre(INVALIDO);
									}else{
										sise.setRgre(row.get("clave"));
									}
									validaNulos(4, row.get("reserva"), rowNum);
									if(row.get("reserva") == null || row.get("reserva").equals("") || row.get("reserva").isEmpty()){
										sise.setReserva(BigDecimal.ZERO);
									}else{
										sise.setReserva(new BigDecimal(row.get("reserva")));
									}
									validaNulos(5, row.get("moneda"), rowNum);
									if(row.get("moneda") == null || row.get("moneda").equals("") || row.get("moneda").isEmpty()){
										sise.setMoneda(INVALIDO);
									}else{
										sise.setMoneda(row.get("moneda"));
									}
								} catch (NumberFormatException e) {
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
								}
								return sise;
							}
						});
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaDistribucionSise;
	}
	
	/**
	 * Convierte informacion del archivo excel Ajustes Manuales.
	 * @return listaAjustesManuales.
	 */	
	public List<AjustesManuales> listarAjustesManuales() {
		List<AjustesManuales> listaAjustesManuales = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());

			for (String sheetName : converter.getSheetNames()) {

				listaAjustesManuales = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<AjustesManuales>() {
							public AjustesManuales mapRow(Map<String, String> row,
									int rowNum) {
								AjustesManuales ajustes = new AjustesManuales(fCorte, tipoCambio);
								try {
									validaNulos(1, row.get("siniestro"), rowNum);	
									if(row.get("siniestro") == null || row.get("siniestro").equals("") || row.get("siniestro").isEmpty()){
										ajustes.setSiniestro(0);
									}else{
										ajustes.setSiniestro(new BigDecimal(row.get("siniestro")).intValue());
									}
									if(row.get("reasegurador") == null || row.get("reasegurador").equals("") || row.get("reasegurador").isEmpty()){
										ajustes.setReasegurador(INVALIDO);
									}else{
										ajustes.setReasegurador(row.get("reasegurador"));
									}
									validaNulos(3, row.get("cnsf"), rowNum);
									if(row.get("cnsf") == null || row.get("cnsf").equals("") || row.get("cnsf").isEmpty()){
										ajustes.setCnsf(INVALIDO);
									}else{
										ajustes.setCnsf(row.get("cnsf"));
									}
									validaNulos(4, row.get("moneda"), rowNum);
									if(row.get("moneda") == null || row.get("moneda").equals("") || row.get("moneda").isEmpty()){
										ajustes.setMoneda(INVALIDO);
									}else{
										ajustes.setMoneda(row.get("moneda"));
									}
									validaNulos(5, row.get("reserva_pendiente_reas"), rowNum);
									if(row.get("reserva_pendiente_reas") == null || row.get("reserva_pendiente_reas").equals("") || row.get("reserva_pendiente_reas").isEmpty()){
										ajustes.setReserva(BigDecimal.ZERO);
									}else{
										ajustes.setReserva(new BigDecimal(row.get("reserva_pendiente_reas")));
									}
									validaNulos(6, row.get("anio"), rowNum);
									if(row.get("anio") == null || row.get("anio").equals("") || row.get("anio").isEmpty()){
										ajustes.setAnio(INVALIDO);
									}else{
										ajustes.setAnio(row.get("anio"));
									}
									validaNulos(7, row.get("sistema"), rowNum);
									if(row.get("sistema") == null || row.get("sistema").equals("") || row.get("sistema").isEmpty()){
										ajustes.setSistema(INVALIDO);
									}else{
										ajustes.setSistema(row.get("sistema"));
									}
								} catch (NumberFormatException e) {
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
								}
								return ajustes;
							}
						});
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaAjustesManuales;
	}


	public static String getPoliza(String polizaCompleta) {

		String poliza = "";
		 try{
			 if(polizaCompleta != null){
				int  primera  = Integer.parseInt(polizaCompleta.split("-")[0]);
				int  tercera  = Integer.parseInt(polizaCompleta.split("-")[2]);
				BigDecimal  segunda  = new BigDecimal(polizaCompleta.split("-")[1]);
		   
				poliza = primera+"-"+segunda+"-"+tercera;
				
				return poliza;			
			 }
	        }catch(NumberFormatException ex){
	        	LogDeMidasEJB3.log("Error al convertir el numero de la poliza:", Level.SEVERE, ex);
	        }catch(ArrayIndexOutOfBoundsException ex){
	        	LogDeMidasEJB3.log("Error al convertir el numero de la poliza:", Level.SEVERE, ex);
		    }

		return poliza;
	}
	
	
	public static Date getFechaFromString(String fecha) throws ParseException {
		java.util.Date fechaCorte = null;
		Date sqlDate = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
		fechaCorte = sdf.parse((fecha));
		sqlDate = new java.sql.Date(fechaCorte.getTime());
		} catch (ParseException e) {
			LogDeMidasEJB3.log("Falla al obtnener la fecha", Level.SEVERE, e);
		}
		return sqlDate;
	}
	
	/**
	 * Llena CargaContraParteDTO.
	 * @param fCorte
	 * @param estatus
	 * @param reporte
	 * @return resultado.
	 */	
	public void llenarCargaContraParteDTO(java.util.Date fCorte,String estatus, String reporte, String usuario, BigDecimal idToControlArchivo){
		java.util.Date fechaActual = new java.util.Date();
		cargaDTO =  new CargaContraParteDTO();
		cargaDTO.setFechaFin(fCorte);
		cargaDTO.setEstatus(estatus);
		cargaDTO.setReporte(reporte);
		cargaDTO.setUsuario(usuario);
		cargaDTO.setFechaCarga(fechaActual);
		cargaDTO.setIdArchivo(idToControlArchivo.intValueExact());
		cargaDTO.setCandado(0);
	}
	
	/**
	 * Valida campos del archivo excel.
	 * @param col
	 * @param reporte
	 * @param row
	 * @return resultado.
	 */	
	public String validaPendientes(int col, String reporte, int row){
		   
		if(reporte == null || reporte.equals("") || reporte.isEmpty()){
				row = row+2;
				listaErrores.add("Fila: "+row+" Columna: "+col+" Valor inv\u00e1lido.");
				resultado = ERROR;
		}
	  
	return resultado;
	
	}
	
	/**
	 * Valida campos del archivo excel.
	 * @param col
	 * @param reporte
	 * @param row
	 * @return resultado.
	 */	
	public String validaPolFac(int col, String reporte, int row){
		switch(col) {
		case (1):{
			if(reporte == null || reporte.equals("") || reporte.isEmpty()){
				row = row+2;
				listaErrores.add("Campo POLIZA. Fila: "+row+" Columna: "+col+" Valor inv\u00e1lido.");
				resultado = ERROR;
			}
			break;
		}
		case (10):{
			if(reporte == null || reporte.equals("") || reporte.isEmpty()){
			row = row+2;
			listaErrores.add("Campo RGRE. Fila: "+row+" Columna: "+col+" Valor inv\u00e1lido.");
			resultado = ERROR;
			}else if(reporte.length()>20){
			row = row+2;
				listaErrores.add("Campo RGRE. Fila: "+row+" Columna: "+col+" Valor inv\u00e1lido.");
				resultado = ERROR;
			}
			break;
		}	
		case (11):{
			if(reporte == null || reporte.equals("") || reporte.isEmpty()){
			row = row+2;
			listaErrores.add("Campo REASEGURADOR. Fila: "+row+" Columna: "+col+" Valor inválido.");
			resultado = ERROR;
			}
			break;
		}		
	  }
		
	  return resultado;
	
	}
	
	/**
	 * Valida campos del archivo excel.
	 * @param col
	 * @param reporte
	 * @param row
	 * @return resultado.
	 */	
	public String validaContratosAutomaticos(int col, String reporte, int row){
		   
		if(reporte == null || reporte.equals("") || reporte.isEmpty()){
				row = row+2;
				listaErrores.add("Fila: "+row+" Columna: "+col+" Valor inv\u00e1lido.");
				resultado = ERROR;
		}
	  
	  return resultado;
	}
	
	  /**
		 * Valida campos del archivo excel.
		 * @param col
		 * @param reporte
		 * @param row
		 * @return resultado.
		 */	
		public String validaNulos(int col, String reporte, int row){
			   
			if(reporte == null || reporte.equals("") || reporte.isEmpty()){
					row = row+2;
					listaErrores.add("Fila: "+row+" Columna: "+col+" Valor inv\u00e1lido.");
					resultado = ERROR;
			}
		  
		  return resultado;
	
	}

}
