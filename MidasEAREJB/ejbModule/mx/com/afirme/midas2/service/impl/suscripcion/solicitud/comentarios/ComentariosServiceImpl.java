package mx.com.afirme.midas2.service.impl.suscripcion.solicitud.comentarios;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoFacadeRemote;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.comentarios.ComentariosService;
import mx.com.afirme.midas2.util.VaultService;

@Stateless
public class ComentariosServiceImpl implements ComentariosService{

	private EntidadDao entidadDao; 
	private DocumentoDigitalSolicitudFacadeRemote documentoDigitalSolicitudFacadeRemote;
	private List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTOList = new ArrayList<DocumentoDigitalSolicitudDTO>();
	private UsuarioService usuarioService;
	private ControlArchivoFacadeRemote controlArchivoFacadeRemote;
	private VaultService vaultService;
	
	public static final Short TIPO_COMENTARIO_SOLICITUD = 0;
	public static final Short TIPO_COMENTARIO_LISTAR_SOLICITUD = 3;
	public static final Short TIPO_COMENTARIO_COTIZACION = 1;
	public static final Short TIPO_COMENTARIO_COTIZACION_CANCELACION = 2;
	
	
	@EJB(beanName="UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@EJB
	public void setDocumentoDigitalSolicitudFacadeRemote(
			DocumentoDigitalSolicitudFacadeRemote documentoDigitalSolicitudFacadeRemote) {
		this.documentoDigitalSolicitudFacadeRemote = documentoDigitalSolicitudFacadeRemote;
	}

	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}

	@Override
	public List<Comentario> getComentarioPorSolicitud(BigDecimal idSolicitud) {
		return entidadDao.findByProperty(Comentario.class, "solicitudDTO.idToSolicitud", idSolicitud);		
	}

	@Override
	public void guardarComentario(Comentario comentario) {
		Usuario usuario = usuarioService.getUsuarioActual();
		comentario.setCodigoUsuarioCreacion(usuario.getNombreUsuario());		
		if(comentario.getId()==null){
			comentario.setFechaCreacion(new Date());
			comentario.setValor(comentario.getValor().toUpperCase());
			try{
				entidadDao.persist(comentario);
			}catch(Exception e) {
				e.printStackTrace();
			}
		
		}
		else{																												  
			documentoDigitalSolicitudDTOList.addAll(documentoDigitalSolicitudFacadeRemote.findByProperty("idToControlArchivo", comentario.getDocumentoDigitalSolicitudDTO().getControlArchivo().getIdToControlArchivo()));
			comentario.setDocumentoDigitalSolicitudDTO(documentoDigitalSolicitudDTOList.get(documentoDigitalSolicitudDTOList.size()-1)); 
			entidadDao.update(comentario);
		}				
	}

	@Override
	public void eliminarComentario(Comentario comentario) {
		//Elimina documento
		try{
			if(comentario.getDocumentoDigitalSolicitudDTO() != null){
				DocumentoDigitalSolicitudDTO documento = entidadDao.findById(DocumentoDigitalSolicitudDTO.class, comentario.getDocumentoDigitalSolicitudDTO().getIdToDocumentoDigitalSolicitud());
				entidadDao.remove(documento);
				comentario.setDocumentoDigitalSolicitudDTO(null);
			}
		}catch(Exception e){
			
		}
		entidadDao.remove(comentario);
	}

	@Override
	public void guardarComentario(String cometario, SolicitudDTO solicitud) {
		Usuario usuario = usuarioService.getUsuarioActual();
		Comentario comentario = new Comentario();
		comentario.setFechaCreacion(new Date());
		comentario.setValor(cometario.toUpperCase());
		comentario.setSolicitudDTO(solicitud);
		comentario.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		entidadDao.persist(comentario);
	}

	@Override
	public void copiarComentarios(BigDecimal idSolicitudBase, BigDecimal idSolicitudCopia) {
		List<Comentario> comentariosOriginales = this.getComentarioPorSolicitud(idSolicitudBase);
		Comentario comentario= null;
		DocumentoDigitalSolicitudDTO documento = null;
		SolicitudDTO solicitudNueva = entidadDao.findById(SolicitudDTO.class, idSolicitudCopia);
		for(Comentario item: comentariosOriginales){
			comentario = new Comentario();
			comentario.setSolicitudDTO(solicitudNueva);
			comentario.setCodigoUsuarioCreacion(item.getCodigoUsuarioCreacion());
			comentario.setFechaCreacion(item.getFechaCreacion());
			comentario.setValor(item.getValor());
			if(item.getDocumentoDigitalSolicitudDTO() != null){
				documento = new DocumentoDigitalSolicitudDTO();
				documento.setCodigoUsuarioCreacion(item.getDocumentoDigitalSolicitudDTO().getCodigoUsuarioCreacion());
				documento.setCodigoUsuarioModificacion(item.getDocumentoDigitalSolicitudDTO().getCodigoUsuarioModificacion());
				
				ControlArchivoDTO controlArchivoBase = controlArchivoFacadeRemote.findById(item.getDocumentoDigitalSolicitudDTO().getIdToControlArchivo());
				ControlArchivoDTO controlArchivoNuevo = new ControlArchivoDTO();
				controlArchivoNuevo.setClaveTipo(controlArchivoBase.getClaveTipo());
				controlArchivoNuevo.setNombreArchivoOriginal(controlArchivoBase.getNombreArchivoOriginal());

				controlArchivoNuevo = controlArchivoFacadeRemote.save(controlArchivoNuevo);
				
				vaultService.copyFile(controlArchivoBase, controlArchivoNuevo);
				
				documento.setControlArchivo(controlArchivoNuevo);
				documento.setFechaCreacion(item.getDocumentoDigitalSolicitudDTO().getFechaCreacion());
				documento.setFechaModificacion(item.getDocumentoDigitalSolicitudDTO().getFechaModificacion());
				documento.setIdToControlArchivo(controlArchivoNuevo.getIdToControlArchivo());
				documento.setNombreUsuarioCreacion(item.getDocumentoDigitalSolicitudDTO().getNombreUsuarioCreacion());
				documento.setNombreUsuarioModificacion(item.getDocumentoDigitalSolicitudDTO().getNombreUsuarioModificacion());
				documento.setSolicitudDTO(solicitudNueva);
				
				comentario.setDocumentoDigitalSolicitudDTO(documento);
			}
			entidadDao.persist(comentario);
		}
		
	}
	
	@Override
	public void actualizaEstatusSolicitudPorComentario(BigDecimal idSolicitud) {
		List<Comentario> comentarios = entidadDao.findByProperty(Comentario.class, "solicitudDTO.idToSolicitud", idSolicitud);
		if(comentarios != null && comentarios.size() > 0){
			for(Comentario comentario: comentarios){
				if(comentario.getDocumentoDigitalSolicitudDTO() != null){
					SolicitudDTO solicitud = entidadDao.findById(SolicitudDTO.class, idSolicitud);
					solicitud.setClaveEstatus(SolicitudDTO.Estatus.PROCESO.getEstatus());
					entidadDao.update(solicitud);
					break;
				}
			}
		}
	}
	
	@Override
	public String validaComentarioSolicitudCotizacion(String valor, Short tipoComentario){
		String sufijo = "";
		if(tipoComentario.equals(TIPO_COMENTARIO_SOLICITUD) || tipoComentario.equals(TIPO_COMENTARIO_LISTAR_SOLICITUD)){
			sufijo = "SOL-";
		}
		if(tipoComentario.equals(TIPO_COMENTARIO_COTIZACION) || tipoComentario.equals(TIPO_COMENTARIO_COTIZACION_CANCELACION)){
			sufijo = "COT-";
		}
		if(valor.indexOf(sufijo) == -1 || valor.indexOf(sufijo) > 0){
			valor = sufijo + valor;
		}
		return valor;
	}
	
	@Override
	public void guardarComentarioCoberturaAdicional(Comentario comentario){
		try{
			comentario.setFechaCreacion(new Date());
			entidadDao.persist(comentario);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@EJB
	public void setControlArchivoFacadeRemote(
			ControlArchivoFacadeRemote controlArchivoFacadeRemote) {
		this.controlArchivoFacadeRemote = controlArchivoFacadeRemote;
	}
	@EJB
	public void setVaultService(VaultService vaultService) {
		this.vaultService = vaultService;
	}
}
