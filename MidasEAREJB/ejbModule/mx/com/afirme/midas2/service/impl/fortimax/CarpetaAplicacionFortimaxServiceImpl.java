package mx.com.afirme.midas2.service.impl.fortimax;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fortimax.CarpetaAplicacionFortimaxDao;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CarpetaAplicacionFortimax;
import mx.com.afirme.midas2.service.fortimax.CarpetaAplicacionFortimaxService;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class CarpetaAplicacionFortimaxServiceImpl implements CarpetaAplicacionFortimaxService{
	private CarpetaAplicacionFortimaxDao dao;
	@Override
	public void delete(Long arg0) throws MidasException {
		dao.delete(arg0);
	}

	@Override
	public Long delete(CarpetaAplicacionFortimax arg0) throws MidasException {
		return dao.delete(arg0);
	}

	@Override
	public List<CarpetaAplicacionFortimax> obtenerCarpetasPorAplicacion(Long arg0) throws MidasException {
		return dao.obtenerCarpetasPorAplicacion(arg0);
	}
	@Override
	public CarpetaAplicacionFortimax obtenerCarpetaEspecificaPorAplicacion(String nombreAplicacion,String nombreCarpeta) throws MidasException{
		return dao.obtenerCarpetaEspecificaPorAplicacion(nombreAplicacion,nombreCarpeta);
	}

	@Override
	public List<CarpetaAplicacionFortimax> obtenerCarpetasPorAplicacion(String arg0) throws MidasException {
		return dao.obtenerCarpetasPorAplicacion(arg0);
	}

	@Override
	public Long save(CarpetaAplicacionFortimax arg0) throws MidasException {
		return dao.save(arg0);
	}
	/**
	 * ===================================================================
	 * Setters and getters
	 * ===================================================================
	 */
	@EJB
	public void setDao(CarpetaAplicacionFortimaxDao dao) {
		this.dao = dao;
	}
}
