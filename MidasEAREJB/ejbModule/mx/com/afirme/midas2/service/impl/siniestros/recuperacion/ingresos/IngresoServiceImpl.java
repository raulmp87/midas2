package mx.com.afirme.midas2.service.impl.siniestros.recuperacion.ingresos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.ingresos.IngresoDao;
import mx.com.afirme.midas2.domain.emisionFacturas.EmisionFactura;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoDocumentoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.provision.MovimientoProvisionSiniestros;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CoberturaRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso.EstatusIngreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.PaseRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.TipoRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCruceroJuridica;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionDeducible;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSipac;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranza;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranza.TipoAplicacionCobranza;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranzaCuenta;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranzaCuenta.TipoAplicacionCuenta;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranzaDevolucion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranzaIngreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranzaIngreso.TipoAplicacionIngreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.ConceptoGuia;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.MovimientoCuentaAcreedor;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.MovimientoCuentaAcreedor.TIPO_MOVIMIENTO_ACREEDOR;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.MovimientoManual;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.MovimientoManualDet;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ingresos.IngresoDevolucion;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.siniestros.ListarIngresoDTO;
import mx.com.afirme.midas2.dto.siniestros.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.DepositoBancarioDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.ImportesIngresosDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.MovimientoAcreedorDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.MovimientoManualDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.liquidacion.LiquidacionSiniestroService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.provision.SiniestrosProvisionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSalvamentoService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoDevolucionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.MidasException;
import mx.com.afirme.midas2.utils.CommonUtils;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionProveedorService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

@Stateless
public class IngresoServiceImpl implements IngresoService{

	private static final int ACTIVA = 1;
	private static final int INACTIVA = 0;
	private static final int TODOS_LOS_INGRESOS = 1;
	public static final int CANCELACION_POR_DEVOLUCION = 1;
	public static final int CANCELACION_CUENTA_ACREEDORA = 2;
	public static final int CANCELACION_CUENTA_VALIDACION_ACTION = 3; // USADO SOLO BITACOREAR DE DONDE SE ESTA SOLICITANDO LA VALIDACION EN obtenerOrigenDedudicleRecuperacionIngreso(Long keyIngreso) 
	public static final int CANCELACION_REVERSA = 3;
	public static final int MIDAS_APP_ID = 5;
	public static final String PARAMETRO = "VARIACION_APL_INGRESO_MANUAL";

	private static final int TIPO_CANCELACION_REVERSA  = 1;
	private static final int TIPO_CANCELACION_CANCELADO  = 2;
	private static final Logger log = Logger.getLogger(IngresoServiceImpl.class);

	@Resource
	private javax.ejb.SessionContext sessionContext;
	@EJB
	private IngresoDao ingresoDao;

	@EJB
	private EntidadService entidadService;

	@EJB
	BitacoraService                bitacoraService;

	@EJB
	private ParametroGlobalService parametroGlobalService ; 

	@EJB
	RecuperacionService recuperacionService;

	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	@EJB
	MovimientoSiniestroService movimientoSiniestroService ;

	@EJB
	LiquidacionSiniestroService liquidacionSiniestroService;

	@EJB
	IngresoDevolucionService ingresoDevolucionService;

	@EJB
	RecuperacionSalvamentoService recuperacionSalvamentoService;
	
	@EJB
	private EnvioNotificacionesService notificacionService;
	
	@EJB
	private SiniestrosProvisionService siniestroProvisionService;
	
	@EJB
	private RecuperacionProveedorService recuperacionProveedor;

	@EJB
	private SistemaContext sistemaContext;
	
	@Resource	
	private TimerService timerService;

	@Override
	public List<ListarIngresoDTO> buscarIngresos(ListarIngresoDTO filtroIngreso, String ingresosConcat) {
		this.configurarServicioParaBusqueda(filtroIngreso);
		filtroIngreso.setIngresosConcat(ingresosConcat);
		filtroIngreso.setBanderaBusquedaConFiltro(ACTIVA);
		List<ListarIngresoDTO> list = ingresoDao.buscarIngresos(filtroIngreso);
		return list;
	}

	private void configurarServicioParaBusqueda(ListarIngresoDTO filtroIngreso){
		if(filtroIngreso.getServicioParticular() != null && filtroIngreso.getServicioParticular() == 1){
			filtroIngreso.setServicio((short)1);
		}
		if(filtroIngreso.getServicioPublico() != null && filtroIngreso.getServicioPublico() == 1){
			filtroIngreso.setServicio((short)2);
		}
		if( filtroIngreso.getServicioParticular() != null && filtroIngreso.getServicioPublico() != null && 
				filtroIngreso.getServicioPublico() == 1  && filtroIngreso.getServicioParticular() == 1){
			filtroIngreso.setServicio((short)3);
		}
	}

	@Deprecated
	private Integer getPermitirFacturacion(Long ingresoId){
		Ingreso ingreso = entidadService.findById(Ingreso.class, ingresoId);
		if(ingreso.getEstatus().equals(Ingreso.EstatusIngreso.CANCELADO_POR_DEVOLUCION))
			return 0;
		if(ingreso.getEstatus().equals(Ingreso.EstatusIngreso.CANCELADO_ENVIO_CUENTA_ACREEDORA))
			return 0;
		if(ingreso.getEstatus().equals(Ingreso.EstatusIngreso.CANCELADO_INGRESO_PENDIENTE_POR_APLICAR))
			return 0;
		List<EmisionFactura> list =  entidadService.findByProperty(EmisionFactura.class, "ingreso.id", ingresoId);
		if(list != null && list.size() >0){
			for(EmisionFactura ef : list){
				if(ef.getEstatus().equals(EmisionFactura.EstatusFactura.FACTURADA.getValue()))
					return 0;
				if(ef.getEstatus().equals(EmisionFactura.EstatusFactura.PROCESO.getValue()))
					return 0;
			}
			return 1;
		}
		if(ingreso.getEstatus().equals(Ingreso.EstatusIngreso.APLICADO.getValue()))
			if(ingreso.getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.PROVEEDOR.getValue()))
				if(ingreso.getRecuperacion().getTipo().equals(Recuperacion.MedioRecuperacion.VENTA.toString()))
					return 1;
				else
					return 0;
			else
				return 1;
		if(ingreso.getEstatus().equals(Ingreso.EstatusIngreso.PENDIENTE.getValue())){
			if(!ingreso.getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.SALVAMENTO.getValue()))
				return 1;
			else
				return 0;
		}	

		return 0;
	}

	@Override
	public List<ListarIngresoDTO> buscarIngresos(String ingresosConcat) {
		ListarIngresoDTO filtroIngreso = new ListarIngresoDTO();
		filtroIngreso.setBanderaBusquedaConFiltro(INACTIVA);
		filtroIngreso.setIngresosConcat(ingresosConcat);
		List<ListarIngresoDTO> list = ingresoDao.buscarIngresos(filtroIngreso);
		return list;
	}

	@Override
	public String cancelarIngresoPendientePorAplicar(Ingreso ingreso,String motivo) {

		// SI EL INGRESO ESTA RELACIONADO A UNA RECUPERACION DE JURIDICO O CRUCERO TRONAR LA OPERACION
		//Se elimina la validacion por la modificacion en el caso de uso CDU Cancelar Ingreso Pendiente por Aplicar
		//this.ingresoNoValido(ingreso.getId());
		
		Date hoy = new Date();

		if( ingreso.getEstatus().equals(Ingreso.EstatusIngreso.PENDIENTE.getValue()) ){
			ingreso.setFechaModificacion(hoy);
			ingreso.setFechaCancelacion (hoy);
			ingreso.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			ingreso.setCodigoUsuarioCancelacion (usuarioService.getUsuarioActual().getNombreUsuario());
			ingreso.setEstatus(EstatusIngreso.CANCELADO_INGRESO_PENDIENTE_POR_APLICAR.getValue());
			ingreso.setMotivoCancelacion(motivo);

			this.entidadService.save(ingreso);

			this.bitacoraService.registrar(TIPO_BITACORA.HGS, EVENTO.CANCELAR_INGRESO_PENDIENTE_APLICAR, ingreso.getId().toString() , "Ingreso a Cancelar: "+ingreso.getId(),"", usuarioService.getUsuarioActual().getNombreUsuario());

		}

		return null;
	}

	public void cancelarIngresoPendienteRecuperacionRegistrada(Long idIngreso,String motivo){
		Ingreso ingreso = entidadService.findById(Ingreso.class, idIngreso);
		Date hoy = new Date();

		this.cancelarIngresoPendientePorAplicar(ingreso, motivo);

		if(CommonUtils.isNotNull(ingreso.getRecuperacion())){
		
			// CANCELACION DE LA ADJUDICACION SI ES SALVAMENTO
			if( ingreso.getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.SALVAMENTO.getValue() ) ) {
				this.recuperacionSalvamentoService.cancelarAdjudicacion( ingreso.getRecuperacion().getId(), "CANCELACION DESDE INGRESO PENDIENTE POR APLICAR" , false);
			}
			
			// NOTIFICACION DEL INGRESO CON FACTURA CANCELADA
			this.notificarIngresoFacturaCancelada(ingreso);
		
			if (EnumUtil.equalsValue(TipoRecuperacion.CRUCEROJURIDICA, ingreso.getRecuperacion().getTipo())) {
				ingreso.getRecuperacion().setEstatus(EstatusRecuperacion.CANCELADO.name());
				ingreso.getRecuperacion().setFechaCancelacion(hoy);
				ingreso.getRecuperacion().setMotivoCancelacion(motivo);
			}else{
				ingreso.getRecuperacion().setEstatus(EstatusRecuperacion.REGISTRADO.name());
			}
			ingreso.getRecuperacion().setFechaModificacion(hoy);
			ingreso.getRecuperacion().setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			this.entidadService.save(ingreso.getRecuperacion());

		}
	}

	@Override
	public List<DepositoBancarioDTO> buscarDepositosBancarios(DepositoBancarioDTO filtro, String depositosConcat, Boolean esConsulta){
		if(null==filtro){
			filtro = new DepositoBancarioDTO();
		}
		filtro.setDepositosConcat(depositosConcat);
		List<DepositoBancarioDTO> resultados = ingresoDao.buscarDepositosBancarios(filtro, esConsulta);
		return resultados;
	}

	public List<DepositoBancarioDTO> buscarDepositosBancarios(String depositosConcat, Boolean esConsulta){
		DepositoBancarioDTO filtro = new DepositoBancarioDTO();
		filtro.setDepositosConcat(depositosConcat);
		List<DepositoBancarioDTO> resultados = ingresoDao.buscarDepositosBancarios(filtro, esConsulta);
		return resultados;
	}

	/**
	 * Generar un movimiento de Cargo para una cuenta acreedora
	 * 
	 * @param idMovto
	 * @param monto
	 * @param aplicacionMovtoId
	 */
	@Override
	public void cargoMovimientoAcreedor(Long idMovto, BigDecimal monto, Long aplicacionMovtoId) {
		MovimientoCuentaAcreedor movimiento =this.entidadService.findById(MovimientoCuentaAcreedor.class, idMovto);
		if(null!=movimiento){
			Long aplicacionOriginalId=null;
			Long aplicacionCancelacionId=null;
			Long cuentaId=null;
			if(null!= movimiento.getAplicacionOriginal() ){
				aplicacionOriginalId=movimiento.getAplicacionOriginal().getId();
			}
			if(null!= movimiento.getAplicacionCancelacion() ){
				aplicacionCancelacionId=movimiento.getAplicacionCancelacion().getId();
			}

			if(null!= movimiento.getCuenta() ){
				cuentaId=movimiento.getCuenta().getId();
			}
			this.insertarMovimientoAcreedor(aplicacionOriginalId, aplicacionCancelacionId, aplicacionMovtoId, cuentaId, monto, TIPO_MOVIMIENTO_ACREEDOR.CARGO,movimiento.getId());                         
		}

	}

	@Override
	public void abonoMovimientoAcreedor(Long idMovto, BigDecimal monto, Long aplicacionMovtoId) {
		MovimientoCuentaAcreedor movimiento =this.entidadService.findById(MovimientoCuentaAcreedor.class, idMovto);
		if(null!=movimiento){

			Long aplicacionOriginalId=null;                 
			Long aplicacionCancelacionId=null;
			Long cuentaId=null;
			if(null!= movimiento.getAplicacionOriginal() ){
				aplicacionOriginalId=movimiento.getAplicacionOriginal().getId();
			}

			if(null!= movimiento.getAplicacionCancelacion() ){
				aplicacionCancelacionId=movimiento.getAplicacionCancelacion().getId();
			}

			if(null!= movimiento.getCuenta() ){
				cuentaId=movimiento.getCuenta().getId();
			}
			this.insertarMovimientoAcreedor(aplicacionOriginalId, aplicacionCancelacionId, aplicacionMovtoId, cuentaId, monto, TIPO_MOVIMIENTO_ACREEDOR.ABONO,movimiento.getId());                        
		}

	}


	@Override
	public void aplicaRefunic(String refunic, BigDecimal monto,
			String descripcion) throws MidasException{
		this.ingresoDao.aplicaRefunic(refunic, monto, descripcion);     
	}

	/**
	 * <b>SE AGREGA EN DISE�O APLICAR INGRESO MANUAL</b>
	 * M�todo que genera un Movimiento sobre MovimientoCuentaAcreedor
	 * Crear un objeto <b>movimiento </b>de tipo MovimientoCuentaAcreedor , asociar la
	 * <b>aplicacionOriginal</b>, <b>aplicacionCancelacion </b>y
	 * <b>aplicacionMovimiento</b>, la <b>cuenta </b>(buscar sobre ConceptoGuias con
	 * entidadService.findById) y el <b>monto</b>
	 * Hacer un entidadService.findByPropertiesWithOrder sobre
	 * MovimientoCuentaAcreedor usando la <b>aplicacionOriginal </b>y ordenar por
	 * fecha, Si existen movimientos se deben cambiar todos a INACTIVO, adem�s se debe
	 * obtener el �ltimo movimiento para comparar el saldo de este contra el nuevo
	 * monto, realizar la resta para obtener el <b>saldo </b>del nuevo mvimiento.
	 * 
	 * Asignar el <b>saldo </b>al <b>movimiento </b>y hacer un entidadService.save
	 * 
	 * @param aplicacionOriginalId
	 * @param aplicacionCancelacionId
	 * @param aplicacionMovtoId
	 * @param cuentaId
	 * @param monto
	 */
	@Override
	public MovimientoCuentaAcreedor generarMovimientoAcreedor(Long aplicacionOriginalId, Long aplicacionCancelacionId, Long aplicacionMovtoId, Long cuentaId, BigDecimal monto){
		return insertarMovimientoAcreedor(aplicacionOriginalId, aplicacionCancelacionId, aplicacionMovtoId, cuentaId, monto, TIPO_MOVIMIENTO_ACREEDOR.ORIGEN, null);             
	}


	private MovimientoCuentaAcreedor insertarMovimientoAcreedor(Long aplicacionOriginalId,Long aplicacionCancelacionId, Long aplicacionMovtoId,Long cuentaId, BigDecimal monto, TIPO_MOVIMIENTO_ACREEDOR tipoMovimientoAcreedor,Long movimientoAcredorId){
		AplicacionCobranza aplicacionOriginal=null;
		AplicacionCobranza aplicacionMovimiento=null;
		AplicacionCobranza aplicacionCancelacion=null;

		ConceptoGuia guia = null;
		if(null!=aplicacionOriginalId){
			aplicacionOriginal=this.entidadService.findById(AplicacionCobranza.class, aplicacionOriginalId);
		}
		if(null!=aplicacionCancelacionId){
			aplicacionCancelacion=this.entidadService.findById(AplicacionCobranza.class, aplicacionCancelacionId);
		}
		if(null!=aplicacionMovtoId){
			aplicacionMovimiento=this.entidadService.findById(AplicacionCobranza.class, aplicacionMovtoId);
		}

		if(null!=cuentaId){
			guia=this.entidadService.findById(ConceptoGuia.class, cuentaId);
		}
		MovimientoCuentaAcreedor movimiento= new MovimientoCuentaAcreedor();
		movimiento.setAplicacionOriginal(aplicacionOriginal);
		movimiento.setAplicacionMovimiento(aplicacionMovimiento);
		movimiento.setAplicacionCancelacion(aplicacionCancelacion);
		movimiento.setCuenta(guia);
		movimiento.setMonto(monto);
		movimiento.setTipo(tipoMovimientoAcreedor.getValue()); 
		movimiento.setSaldoFinal(monto);
		movimiento.setFechaCreacion(new Date());
		movimiento.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));

		if (!TIPO_MOVIMIENTO_ACREEDOR.ORIGEN.equals(tipoMovimientoAcreedor)) {
			MovimientoCuentaAcreedor movimientoAcredor  =this.entidadService.findById(MovimientoCuentaAcreedor.class, movimientoAcredorId);

			if(null!=movimientoAcredor){                                  

				if(null!=monto && null!=movimientoAcredor.getSaldoFinal() ){

					if (TIPO_MOVIMIENTO_ACREEDOR.CARGO.equals(tipoMovimientoAcreedor)) {
						movimiento.setSaldoFinal(movimientoAcredor.getSaldoFinal().subtract(monto));
					} else if (TIPO_MOVIMIENTO_ACREEDOR.ABONO.equals(tipoMovimientoAcreedor)) {                
						movimiento.setSaldoFinal(movimientoAcredor.getSaldoFinal().add(monto));                 
					}
				}

				movimientoAcredor.setEstatus(false);
				this.entidadService.save(movimientoAcredor);
			}                              
		} 
		if(movimiento.getSaldoFinal().compareTo(BigDecimal.ZERO)==0){
			movimiento.setEstatus(false);
			
		}
		this.entidadService.save(movimiento);

		return movimiento;
	}


	/**
	 * <b>SE AGREGA EN DISE�O DE APLICAR INGRESO MANUAL</b>
	 * <b>
	 * </b>M�todo que realiza la actualizaci�n del Ingreso a Aplicado.
	 * 
	 * Cambiar el estatus del ingreso a APLICADO, actualizar la ingreso .fechaIngreso
	 * = fechaAplicacion., ingreso.codigo_usuario_igreso = usuario actual
	 * 
	 * Cambiar el estatus de o las recuperaciones asociadas al ingreso a APLICADO,
	 * actualizar la recuperacion.fechaAplicacion= fechaAplicacion,
	 * recuperacion.codigo_usuario_aplicacion = usuario actual
	 * 
	 * @param ingresoId
	 * @param fechaAplicacion
	 */
	 @Override
	 public void aplicarIngreso(Long ingresoId, Date fechaAplicacion) {
		 Ingreso ingreso = this.obtenerIngreso(ingresoId);
		 if(null!= ingreso){
			 ingreso.setEstatus(Ingreso.EstatusIngreso.APLICADO.getValue());
			 ingreso.setFechaIngreso(fechaAplicacion);
			 ingreso.setCodigoUsuarioIngreso(usuarioService.getUsuarioActual().getNombreUsuario());
			 this.entidadService.save(ingreso);
			 ingreso.getRecuperacion().setEstatus(Recuperacion.EstatusRecuperacion.RECUPERADO.name());
			 ingreso.getRecuperacion().setFechaAplicacion(fechaAplicacion);
			 ingreso.getRecuperacion().setCodigoUsuarioAplicacion(usuarioService.getUsuarioActual().getNombreUsuario());
			 ingreso.getRecuperacion().setFechaModificacion(new Date());
			 this.entidadService.save(ingreso.getRecuperacion());
			 this.recuperacionProveedor.cancelarEnvioNotificacion(ingreso.getRecuperacion().getId());

			 
			 // PROVISIONA APLICACION DE INGRESO
			 //this.provisionaAplicacionIngreso(ingreso.getId().toString());
		 }

	 }
	 
	 /**
	  * <b>SE AGREGA EN DISE�O DE APLICAR INGRESO MANUAL</b>
	  * 
	  * 
	  * M�todo que genera los movimientos de Ingreso correspondientes ya sean de
	  * Ingreso o Cancelaci�n de Ingreso para los Movimientos de las Afectaciones de
	  * Cobertura o Gastos de Ajuste.
	  * 
	  * Obtener el <b>ingreso</b>, obtener la o las <b>recuperaciones </b>asociadas.
	  * 
	  * Iterar las <b>recuperaciones</b>
	  * Por cada <b>recuperacion </b>diferencias el <b>tipoRecuperacion</b>.
	  * 
	  * CASO: PROVEEDOR - Obtener la <b>ordenCompra </b>asociada, identificar el tipo
	  * TIPO ORDEN COMPRA (OC): Invocar al m�todo estimacionCoberturaSiniestroService.
	  * generarMovimientoAfectacion
	  * 
	  * TIPO GAST AJUSTE (GA) O REEMBOLSO DE GASTO DE AJUSTE (RGA): Invocar al m�todo
	  * reporteCabinaService.generarMovimientoReporte
	  * 
	  * 
	  * CASO: DEDUCIBLES- Obtener la <b>estimacion </b>asociada, e invocar al m�todo
	  * estimacionCoberturaSiniestroService.generarMovimiento
	  * 
	  * Se debe generar la Descripci�n de a cuerdo a la RDN
	  * 
	  * @param ingresoId
	  * @param tipoMovimiento
	  */
	 private void generarMovimientosIngreso(Long ingresoId, String tipoMovimiento) {
		 Ingreso ingreso  = this.obtenerIngreso(ingresoId);
		 if(ingreso!=null){
			 String usuario = usuarioService.getUsuarioActual().getNombreUsuario();
			 String descripcion = (TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?"APLICACION":"CANCELACION")
					 + " INGRESO " +  ingreso.getNumero();
			 
			 if(ingreso.getRecuperacion().getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.PROVEEDOR.toString())){
				 RecuperacionProveedor recuperacionProveedor= ((RecuperacionProveedor) ingreso.getRecuperacion());
				 if(null !=recuperacionProveedor   ){
					 BigDecimal monto =BigDecimal.ZERO;
					 if (recuperacionProveedor.getMedio().equalsIgnoreCase(Recuperacion.MedioRecuperacion.REEMBOLSO.toString())){
						 monto=recuperacionProveedor.getSubTotal();
					 }else if(recuperacionProveedor.getMedio().equalsIgnoreCase(Recuperacion.MedioRecuperacion.VENTA.toString())){
						 monto=recuperacionProveedor.getSubTotalVenta();
					 }
					 OrdenCompra oc= recuperacionProveedor.getOrdenCompra();
					 if(oc.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_AFECTACION_RESERVA)){
						 this.movimientoSiniestroService.generarMovimiento(oc.getIdTercero(), 
								 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?monto:monto.negate(),
								 TipoDocumentoMovimiento.INGRESO, 
								 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?
										 TipoMovimiento.INGRESO_PROVEEDOR:TipoMovimiento.CANCELACION_INGRESO_PROVEEDOR,
								 CausaMovimiento.POR_RECUPERACIONES, usuario, descripcion);

					 }else if(oc.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_GASTOAJUSTE)){
						 this.movimientoSiniestroService.generarMovimientoGA(oc.getReporteCabina().getId(),oc.getId(), 
								 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?monto:monto.negate(),
								 TipoDocumentoMovimiento.INGRESO, 
								 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?
										 TipoMovimiento.INGRESO_PROVEEDOR:TipoMovimiento.CANCELACION_INGRESO_PROVEEDOR, 
								 CausaMovimiento.POR_RECUPERACIONES, usuario, descripcion);

					 }else if (oc.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE)){
						 this.movimientoSiniestroService.generarMovimientoGA(oc.getReporteCabina().getId(),oc.getId(), 
								 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?monto:monto.negate(), 
								 TipoDocumentoMovimiento.INGRESO, 
								 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?
										 TipoMovimiento.INGRESO_PROVEEDOR:TipoMovimiento.CANCELACION_INGRESO_PROVEEDOR,
								 CausaMovimiento.POR_RECUPERACIONES, usuario, descripcion);
					 } 
				 }

			 }else if(ingreso.getRecuperacion().getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.DEDUCIBLE.toString()) ){
				 RecuperacionDeducible recuperacionDeducible= ((RecuperacionDeducible) ingreso.getRecuperacion());
				 if(null !=recuperacionDeducible   ){
					 EstimacionCoberturaReporteCabina estimacion= recuperacionDeducible.getEstimacionCobertura();
					 if(null!= estimacion ){
						 this.movimientoSiniestroService.generarMovimiento(estimacion.getId(), 
								 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?
										 recuperacionDeducible.getMontoFinalDeducible()
										 :recuperacionDeducible.getMontoFinalDeducible().negate(), 
								 TipoDocumentoMovimiento.INGRESO, 
								 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?
										 TipoMovimiento.INGRESO_DEDUCIBLES:TipoMovimiento.CANCELACION_INGRESO_DEDUCIBLES,
								 CausaMovimiento.POR_RECUPERACIONES, null, descripcion);
					 }
				 }
			 }else if(ingreso.getRecuperacion().getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.COMPANIA.toString()) ){
				 RecuperacionCompania recuperacionCompania = (entidadService.findByProperty(RecuperacionCompania.class, "id", ingreso.getRecuperacion().getId())).get(0);
				 if(null !=recuperacionCompania  && !CollectionUtils.isEmpty(recuperacionCompania.getCoberturas()) ){
					 CoberturaRecuperacion cobertura =  recuperacionCompania.getCoberturas().get(0);
					 if(cobertura !=null && !CollectionUtils.isEmpty(cobertura.getPasesAtencion()) ){
						 PaseRecuperacion pase= cobertura.getPasesAtencion().get(0);
						 if(pase !=null){
							 EstimacionCoberturaReporteCabina estimacion =pase.getEstimacionCobertura();
							 if(null!= estimacion ){
								 this.movimientoSiniestroService.generarMovimiento(estimacion.getId(), 
										 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?
												 recuperacionCompania.getImporte():recuperacionCompania.getImporte().negate(), 
										 TipoDocumentoMovimiento.INGRESO,
										 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?
												 TipoMovimiento.INGRESO_COMPANIAS:TipoMovimiento.CANCELACION_INGRESO_COMPANIAS,
										 CausaMovimiento.POR_RECUPERACIONES, null, descripcion);
							 }
						 }
						
					 }
					 
					 
				 }
			 }else if(ingreso.getRecuperacion().getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.SALVAMENTO.toString()) ){
				 RecuperacionSalvamento recuperacionSalvamento= ((RecuperacionSalvamento) ingreso.getRecuperacion());
				 if(null !=recuperacionSalvamento  && recuperacionSalvamento.getEstimacion()!=null ){
					 EstimacionCoberturaReporteCabina estimacion =recuperacionSalvamento.getEstimacion();
					 if(null!= estimacion && recuperacionSalvamento.obtenerVentaActiva() !=null){
						 this.movimientoSiniestroService.generarMovimiento(estimacion.getId(), 
								 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?
										 recuperacionSalvamento.obtenerVentaActiva().getSubtotal()
										 :recuperacionSalvamento.obtenerVentaActiva().getSubtotal().negate(), 
								 TipoDocumentoMovimiento.INGRESO, 
								 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?
										 TipoMovimiento.INGRESO_SALVAMENTOS:TipoMovimiento.CANCELACION_INGRESO_SALVAMENTOS,
								 CausaMovimiento.POR_RECUPERACIONES, null, descripcion);
					 }
				 }
			 }else if(ingreso.getRecuperacion().getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.toString()) ){
				 RecuperacionCruceroJuridica recuperacionCrucero= ((RecuperacionCruceroJuridica) ingreso.getRecuperacion());
				 Long estimacionID = this.comprobarSiRecuperacionTienePase(recuperacionCrucero.getSiniestroCabina().getReporteCabina());
				 this.movimientoSiniestroService.generarMovimiento(estimacionID, 
						 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?
								 recuperacionCrucero.getMontoFinal():recuperacionCrucero.getMontoFinal().negate(), 
						 TipoDocumentoMovimiento.INGRESO, 
						 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?
								 TipoMovimiento.INGRESO_CRUCERO_JURIDICO:TipoMovimiento.CANCELACION_INGRESO_CRUCERO_JURIDICO,						 
						 CausaMovimiento.POR_RECUPERACIONES, null, descripcion);
			 }else if(ingreso.getRecuperacion().getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.SIPAC.toString()) ){
				 RecuperacionSipac recuperacionSipac = ((RecuperacionSipac) ingreso.getRecuperacion());
				 EstimacionCoberturaReporteCabina estimacion = recuperacionSipac.getEstimacion();
				 this.movimientoSiniestroService.generarMovimiento(estimacion.getId(), 
						 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?
								 recuperacionSipac.getValorEstimado():recuperacionSipac.getValorEstimado().negate(), 
						 TipoDocumentoMovimiento.INGRESO, 
						 TipoAplicacionCobranza.A.toString().equals(tipoMovimiento)?
								 TipoMovimiento.INGRESO_SIPAC:TipoMovimiento.CANCELACION_INGRESO_SIPAC, 
						 CausaMovimiento.POR_RECUPERACIONES, null, descripcion);
			 }
		 }
	 }
	 
	 @Override
	 public Long comprobarSiRecuperacionTienePase (ReporteCabina reporte){
		 Long idEstimacion = null;
		 EstimacionCoberturaReporteCabina paseFinal = null;
		 Map<String,Object> params = new HashMap<String, Object>();		 
		 if(reporte != null){
			 params.put("coberturaReporteCabina.incisoReporteCabina.id", reporte.getSeccionReporteCabina().getIncisoReporteCabina().getId());
			 List<EstimacionCoberturaReporteCabina>  pases = this.entidadService.findByProperties(EstimacionCoberturaReporteCabina.class, params);
			 for(EstimacionCoberturaReporteCabina pase:  pases){
	             if(pase.getCoberturaReporteCabina().getClaveTipoCalculo().equals("DM")){
	            	 paseFinal = pase;
	                  break;
	             }
			 }
	         if(paseFinal == null){
	             for(EstimacionCoberturaReporteCabina pase:  pases){
	            	 if(pase.getCoberturaReporteCabina().getClaveTipoCalculo().equals("GMO")){
	            		 paseFinal = pase;
	            		 break;
	            		 }
	            	 }
	         }   
		 }
		 if(paseFinal != null ){
			 idEstimacion = paseFinal.getId();
		 }
		 return idEstimacion;
	 }
	 
	 /**
	  * <b>SE AGREGA EN DISE�O DE APLICAR INGRESO MANUAL</b>
	  * <b>
	  * </b>Invocar al m�todo <b><i>obtenerIngreso </i></b>y asignarlo a una variable
	  * <b>ingreso</b>.
	  * 
	  * Obtener el listado de aplicaciones del ingreso mediante <b>ingreso.
	  * getAplicaciones</b> y asignarlo a una variable <b>aplicaciones</b>
	  * 
	  * Iterar las <b>aplicaciones </b>y comparar la <b>fechaCreacion </b>de la
	  * <b>aplicacion </b>contra la <b>fechaAplicacion </b>del <b>ingreso </b>para
	  * saber cu�l es la que le corresponde y se debe  asignar a una variable
	  * <b>aplicacion</b>.
	  * 
	  * De la <b>aplicacion </b>obtener el listado de <b>ingresos</b>.
	  * 
	  * Iterar cada <b>ingreso</b>y asignar sus id a una cadena String
	  * <b>ingresosIds</b>.
	  * 
	  * Invocar al m�todo <b>buscarIngresos </b>para retornar el listado convertido
	  * 
	  * @param ingresoId
	  */
	 @Override
	 public List<ListarIngresoDTO> obtenerIngresosAplicacion(Long ingresoId) {
		 Ingreso ingreso = this.obtenerIngreso(ingresoId);
		 if(null!=ingreso){
			 List<AplicacionCobranzaIngreso> aplicaciones  =this.entidadService.findByProperty(AplicacionCobranzaIngreso.class, "ingreso.id", ingresoId);
			 for(AplicacionCobranzaIngreso aplicacion : aplicaciones){
				 if(  Utilerias.removerHorasMinutosSegundos( aplicacion.getAplicacion().getFechaCreacion()) .compareTo(Utilerias.removerHorasMinutosSegundos( ingreso.getFechaIngreso())   )==0){
					 String ingresosIds="";
					 int contador =0;
					 for (AplicacionCobranzaIngreso ingresos :aplicacion.getAplicacion().getAplicacionesIngreso()){
						 if (contador ==0) 
							 ingresosIds += ingresos.getIngreso().getId();
						 else  
							 ingresosIds += ","+ingresos.getIngreso().getId();
						 contador++;
					 }
					 return this.buscarIngresos(ingresosIds);
				 }
			 }
		 }
		 return null;
	 }

	 @Deprecated
	 public AplicacionCobranza obtenerAplicacionPorIngreso(Long ingresoId) {
		 Ingreso ingreso = this.obtenerIngreso(ingresoId);
		 AplicacionCobranza aplicaciones = null;
		 if(null!=ingreso){
			 aplicaciones  =ingreso.getAplicacionesIngreso().get(0).getAplicacion();
		 }
		 return aplicaciones;

	 }



	 /**
	  * Obtener la Aplicacion de Cobranza donde se encuentra actualmente un Ingreso
	  * @param ingresoId
	  * @param tipoAplicacion opcional
	  * @return
	  */
	 public AplicacionCobranzaIngreso obtenerAplicacionActualPorIngreso(Long ingresoId, TipoAplicacionCobranza tipoAplicacion) {

		 AplicacionCobranzaIngreso aplicacion = null;
		 Map<String, Object> params = new HashMap<String, Object>();
		 params.put("ingreso.id", ingresoId);

		 if (tipoAplicacion != null) {
			 params.put("aplicacion.tipo",tipoAplicacion);
		 }


		 List<AplicacionCobranzaIngreso> aplicaciones  =this.entidadService.findByPropertiesWithOrder(AplicacionCobranzaIngreso.class, params, "fechaCreacion desc");

		 if (CollectionUtils.isNotEmpty(aplicaciones)) {
			 aplicacion = aplicaciones.get(0);
		 }              
		 return aplicacion;            
	 }


	 /**
	  * <b>SE AGREGA EN DISE�O DE APLICAR INGRESO MANUAL</b>
	  * <b>
	  * </b>Invocar al m�todo <b><i>obtenerIngreso </i></b>y asignarlo a una variable
	  * <b>ingreso</b>.
	  * 
	  * Obtener el listado de aplicaciones del ingreso mediante <b>ingreso.
	  * getAplicaciones</b> y asignarlo a una variable <b>aplicaciones</b>
	  * 
	  * Iterar las <b>aplicaciones </b>y comparar la <b>fechaCreacion </b>de la
	  * <b>aplicacion </b>contra la <b>fechaAplicacion </b>del <b>ingreso </b>para
	  * saber cu�l es la que le corresponde y se debe  asignar a una variable
	  * <b>aplicacion</b>.
	  * 
	  * De la <b>aplicacion </b>obtener el listado de movimientos de tipo DEPOSITO
	  * mediante un entidadService.findByProperties y asignarlo a una variable llamada
	  * <b>depositos</b>.
	  * 
	  * Invocar al m�todo <b>buscarDepositosBancarios </b>con el String de ids
	  * concatenados
	  * 
	  * A cada deposito asignarle la d<b>epositoDTO.setFechaDeposito = aplicacion.
	  * getFechaCreacion</b> y d<b>epositoDTO.setMonto = deposito.getMontoAplicado</b>
	  * 
	  * Asignar este <b>depositoDTO </b>a una variable <b>List<DepositoBancarioDTO>
	  * depositosAplicados </b>que es la que retornar� el m�todo
	  * 
	  * @param ingresoId
	  */
	 @Override
	 public List<DepositoBancarioDTO> obtenerDepositosAplicacion(Long ingresoId) {
		 Ingreso ingreso = this.obtenerIngreso(ingresoId);
		 if(null!=ingreso){
			 Map<String, Object> parametros = new HashMap<String, Object>();
			 parametros.put("ingreso.id", ingresoId);
			 parametros.put("aplicacion.tipo", AplicacionCobranza.TipoAplicacionCobranza.A);
			 List<AplicacionCobranzaIngreso> aplicaciones  =this.entidadService.findByPropertiesWithOrder(AplicacionCobranzaIngreso.class,parametros, "fechaCreacion desc");
			 List<DepositoBancarioDTO> depositosBancarioDTO  = new ArrayList<DepositoBancarioDTO>();
			 for(AplicacionCobranzaIngreso aplicacion : aplicaciones){
				 if(  Utilerias.removerHorasMinutosSegundos( aplicacion.getAplicacion().getFechaCreacion()) .compareTo(Utilerias.removerHorasMinutosSegundos( ingreso.getFechaIngreso())   )==0){
					 Map<String,Object> params = new LinkedHashMap<String,Object>();
					 params.put("aplicacion.id", aplicacion.getId().getAplicacionId());
					 params.put("tipo",AplicacionCobranzaCuenta.TipoAplicacionCuenta.D);
					 List<AplicacionCobranzaCuenta>  depositos = this.entidadService.findByProperties(AplicacionCobranzaCuenta.class, params);
					 for (AplicacionCobranzaCuenta deposito : depositos){
						 List<DepositoBancarioDTO> lstDto=this.buscarDepositosBancarios(null, deposito.getIdentificador(), Boolean.TRUE);
						 if(null!=lstDto && !lstDto.isEmpty() ){
							 DepositoBancarioDTO dto = lstDto.get(0);
							 dto.setFechaDeposito(aplicacion.getFechaCreacion());
							 dto.setMonto(deposito.getMontoAplicado());
							 depositosBancarioDTO.add(dto);
						 }
					 }
					 return depositosBancarioDTO;
				 }
			 }
		 }
		 return null;
	 }
	 /**
	  * Metodo que realizara la aplicacion del o los Ingresos seleccionados contra
	  * ciertos movimientos.
	  * 	  
	  * @param ingresosId
	  * @param depositos
	  * @param movAcreedores
	  */
	 @Override
	 public String aplicarIngreso(String ingresosId, String depositos,
			 String movAcreedores, String movManuales) throws MidasException{
		 IngresoService processor = this.sessionContext.getBusinessObject(IngresoService.class);        
		 AplicacionCobranza apliacion = processor.ejecutaAplicacionIngreso(ingresosId, depositos, movAcreedores, movManuales);
		 try {
			 processor.contabilizaAplicacionIngreso(apliacion.getId());
			 
		 } catch (Exception e) {
			 throw new MidasException(e.getMessage());
		 }
		 
		 // PROVISIONA APLICACION DE INGRESO
		 this.provisionaAplicacionIngreso(ingresosId);
		 
		 return null;         
	 }
	 
	 
	 private void provisionaAplicacionIngreso(String ingresosId){

		 String[] aIngresos = ingresosId.split(",");
		 Long estimacionId = null;
		 Ingreso ingreso   = null;
		 RecuperacionProveedor  recuperacionProveedor  = null;
		 RecuperacionSalvamento recuperacionSalvamento = null;
		 boolean bandera = false;
		 
		 
		 for( int i=0; i<= aIngresos.length-1; i++ ){
			 this.siniestroProvisionService.generaYGuardaMovimientoDeAplicacionDeIngreso(new Long(aIngresos[i]));
			 ingreso = this.entidadService.findById(Ingreso.class, new Long(aIngresos[i]));
			 
			 if(ingreso.getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.SALVAMENTO.getValue())){
				 
				 recuperacionSalvamento = (RecuperacionSalvamento)ingreso.getRecuperacion();
				 estimacionId           = recuperacionSalvamento.getEstimacion().getId();
				 bandera                = Boolean.TRUE;
				 
			 }else if(ingreso.getRecuperacion().getTipo().equals( Recuperacion.TipoRecuperacion.PROVEEDOR.getValue() )){
				 
				 recuperacionProveedor = (RecuperacionProveedor)ingreso.getRecuperacion();
				 estimacionId          = recuperacionProveedor.getOrdenCompra().getIdTercero();
				 bandera               = Boolean.TRUE;
			 }else if( ingreso.getRecuperacion().getTipo().equals( Recuperacion.TipoRecuperacion.COMPANIA.getValue() ) ){
				 
				 EstimacionCoberturaReporteCabina estimacion = siniestroProvisionService.obtenerEstimacionDeRecuperacionCia( ingreso.getRecuperacion().getId()  );
				 estimacionId          = estimacion.getId();
				 bandera               = Boolean.TRUE;
			 }
			 
			 if( bandera ){
				 this.siniestroProvisionService.actualizaProvision( estimacionId , MovimientoProvisionSiniestros.Origen.PASE );
				 if(recuperacionSalvamento!=null){
					 this.siniestroProvisionService.actualizaProvision( recuperacionSalvamento.getId() , MovimientoProvisionSiniestros.Origen.RECUPERACION_SALVAMENTO );
				 }
			 }
			 
		 }
		 
	 }

	 @Override
	 @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	 public AplicacionCobranza ejecutaAplicacionIngreso(String ingresosId, String depositos,
			 String movAcreedores, String movManuales) throws MidasException{
		 String mensaje =this.validaAplicarIngreso(ingresosId, depositos, movAcreedores, movManuales);
		 if(!StringUtils.isEmpty(mensaje)){
			 throw new MidasException(mensaje);
		 }
		 List<ListarIngresoDTO> lstIngresoDTO= new ArrayList<ListarIngresoDTO>();
		 if(!StringUtil.isEmpty(ingresosId)){
			 ingresosId= ingresosId.trim();
			 lstIngresoDTO= this.buscarIngresos(ingresosId);
		 }
		 List<DepositoBancarioDTO> lstDepositosBancarios= new ArrayList<DepositoBancarioDTO>();
		 if(!StringUtil.isEmpty(depositos)){
			 depositos= depositos.trim();
			 String[] lstDepositos=  depositos.split(",") ;
			 lstDepositosBancarios= this.buscarDepositosBancarios(null, depositos, true);

		 }
		 List<MovimientoAcreedorDTO> lstMovimientoAcreedorDTO= new ArrayList<MovimientoAcreedorDTO>();
		 if(!StringUtil.isEmpty(movAcreedores)){
			 movAcreedores= movAcreedores.trim();
			 String[] lstMovAcreedores=  movAcreedores.split(",") ;
			 MovimientoAcreedorDTO filtro =new MovimientoAcreedorDTO ();
			 filtro.setMovAcreedoresConcat(movAcreedores);
			 lstMovimientoAcreedorDTO= this.buscarMovimientosAcreedores(filtro, movAcreedores, true);
		 }
		 List<MovimientoManualDTO> lstMovimientoManualDTO= new ArrayList<MovimientoManualDTO>();
		 if(!StringUtil.isEmpty(movManuales)){
			 movManuales= movManuales.trim();
			 String[] lstMovManuales=  movManuales.split(",") ;
			 MovimientoManualDTO filtro =new MovimientoManualDTO ();
			 filtro.setMovManualConcat(movManuales);
			 lstMovimientoManualDTO= this.buscarMovimientosManuales(filtro, movManuales, true);
		 }
		 ImportesIngresosDTO importe  = this.calculaImportes(ingresosId, depositos, movAcreedores, movManuales, false);
		 //VALIDACIONES INGRESOS
		 BigDecimal montoIngresos=importe.getTotalesIngresosAplicar();
		 BigDecimal montoDepositos= importe.getTotalesDepositosBancarios();
		 BigDecimal montoMovtosAcreedor= importe.getTotalesMovimientosAcreedores();
		 BigDecimal montoMovtosManual=importe.getTotalesMovimientosManuales();
		 BigDecimal sumaMovtos= montoDepositos.add(montoMovtosAcreedor).add(montoMovtosManual);

		 /*1 Generar la aplicacion, crear un nuevo objeto AplicacionCobranza de tipo
                  APLICACION guardar y obtener el idAplicacion generado.

                 2  Por cada Ingreso utilizar el m�todo agregar de AplicacionCobranza para
                  asociarlo a dicha aplicaci�n

                 3 Por cada dep�sito utilizar el m�todo agregar de AplicacionCobranza para
                  asociarlo a dicha aplicaci�n

                 4 Por cada movimiento utilizar el m�todo agregar de AplicacionCobranza
                  para asociarlo a dicha aplicaci�n
		  * */
		 //1
		 AplicacionCobranza aplicacionCobranza=new AplicacionCobranza(TipoAplicacionCobranza.A, "APLICACION INGRESO",   usuarioService.getUsuarioActual().getNombreUsuario());
		 this.entidadService.save(aplicacionCobranza);
		 //2
		 ConceptoGuia conceptoGuia = null;
		 for (ListarIngresoDTO  ingresoDTO : lstIngresoDTO){
			 Ingreso ingreso = this.entidadService.findById(Ingreso.class, ingresoDTO.getIngresoId());
			 conceptoGuia = obtenerConceptoGuiaParaIngreso(ingreso.getRecuperacion().getTipo());
			 aplicacionCobranza.agregar(ingreso, TipoAplicacionIngreso.A, usuarioService.getUsuarioActual().getNombreUsuario(), conceptoGuia);
		 }
		 //3
		 for (DepositoBancarioDTO  depositoBancarioDTO : lstDepositosBancarios){
			 if( montoIngresos.compareTo(BigDecimal.ZERO)!=1  ){
					break; 
			 }
			 BigDecimal montoAplicado=BigDecimal.ZERO;
			 if(depositoBancarioDTO.getMonto().compareTo(montoIngresos)<1){
				 montoAplicado=depositoBancarioDTO.getMonto();
			 }else{
				 montoAplicado=montoIngresos;
			 }
			 conceptoGuia = obtenerConceptoGuiaParaCuenta("REFUNIC",null);                            
			 aplicacionCobranza.agregar(depositoBancarioDTO.getRefunic(), TipoAplicacionCuenta.D, depositoBancarioDTO.getMonto(), 
					 montoAplicado, usuarioService.getUsuarioActual().getNombreUsuario(), depositoBancarioDTO.getDescripcionMovimiento(), conceptoGuia);
			 //this.aplicaRefunic(depositoBancarioDTO.getRefunic(), montoAplicado, aplicacionCobranza.getId().toString());
			 montoIngresos=montoIngresos.subtract(montoAplicado);
		 }
		 //4
		 for (MovimientoAcreedorDTO  movimientoAcreedorDTO : lstMovimientoAcreedorDTO){
			 if( montoIngresos.compareTo(BigDecimal.ZERO)!=1  ){
					break; 
			 }
			 BigDecimal montoAplicado=BigDecimal.ZERO;
			 if(movimientoAcreedorDTO.getImporte().compareTo(montoIngresos)<1){
				 montoAplicado=movimientoAcreedorDTO.getImporte();
			 }else{
				 montoAplicado=montoIngresos;
			 }

			 conceptoGuia = obtenerConceptoGuiaParaCuenta(null,movimientoAcreedorDTO.getIdCuentaContable());
			 aplicacionCobranza.agregar(movimientoAcreedorDTO.getMovimientoId().toString(), TipoAplicacionCuenta.A, movimientoAcreedorDTO.getImporte(), 
					 montoAplicado, usuarioService.getUsuarioActual().getNombreUsuario(),"", conceptoGuia);
			 this.cargoMovimientoAcreedor(movimientoAcreedorDTO.getMovimientoId(), montoAplicado, aplicacionCobranza.getId());
			 montoIngresos=montoIngresos.subtract(montoAplicado);

		 }
		 for (MovimientoManualDTO  movimientoDTO : lstMovimientoManualDTO){
			 if( montoIngresos.compareTo(BigDecimal.ZERO)!=1  ){
					break; 
			 }
			 BigDecimal montoAplicado=BigDecimal.ZERO;
			 if(movimientoDTO.getImporte().compareTo(montoIngresos)<1){
				 montoAplicado=movimientoDTO.getImporte();
			 }else{
				 montoAplicado=montoIngresos;
			 }

			 conceptoGuia = obtenerConceptoGuiaParaCuenta(movimientoDTO.getCodigoCuentaContable(),null);
			 aplicacionCobranza.agregar(movimientoDTO.getIdMovimiento().toString(), TipoAplicacionCuenta.M, movimientoDTO.getImporte(), 
					 montoAplicado, usuarioService.getUsuarioActual().getNombreUsuario(),"", conceptoGuia);
			 this.insertaMovimientoManual(movimientoDTO.getIdMovimiento(), montoAplicado, aplicacionCobranza.getId(), TIPO_MOVIMIENTO_ACREEDOR.CARGO);
			 montoIngresos=montoIngresos.subtract(montoAplicado);

		 }
		 this.entidadService.save(aplicacionCobranza);
		 for (ListarIngresoDTO  ingresoDTO : lstIngresoDTO){
			 this.aplicarIngreso(ingresoDTO.getIngresoId(), new Date());
			 this.generarMovimientosIngreso(ingresoDTO.getIngresoId(), TipoAplicacionCobranza.A.toString());
		 }
		 return aplicacionCobranza;
	 }


	 /**
	  * Metodo que Aplicara los Ingresos que se encuentren Pendientes por Aplicar y que
	  * tengan relacionados Referencias Bancarias. Se debe ejecutar con hroa por
	  * definir
	  */

	 @Override
	 public void aplicarIngresoAutomatico() {
		LogDeMidasInterfaz.log("Inicia Ejecucion aplicarIngresoAutomatico.." + this, Level.INFO, null);
		 List<Ingreso>  ingresos = this.entidadService.findByProperty(Ingreso.class, "estatus",Ingreso.EstatusIngreso.PENDIENTE.getValue() );
		 for (Ingreso ingreso:ingresos){ 
			 Recuperacion recuperacion=ingreso.getRecuperacion();
			 if (null !=recuperacion && !recuperacion.getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.COMPANIA.getValue()) ){
				 List<ReferenciaBancariaDTO> referenciasBancarias =this.recuperacionService.obtenerReferenciasBancaria(recuperacion.getId(), recuperacion.getMedio());
				 List<ReferenciaBancariaDTO> referencias  = new ArrayList<ReferenciaBancariaDTO>();

				 String referencia =null;
				 int count =0;
				 for(ReferenciaBancariaDTO referenciaDTO :referenciasBancarias ){
					 if(!StringUtils.isEmpty(referenciaDTO.getNumeroReferencia())){
						 if(count==0){
							 referencias.add(referenciaDTO);
							 referencia=referenciaDTO.getNumeroReferencia();
						 }else{
							 if(!referencia.equals(referenciaDTO.getNumeroReferencia())){
								 referencia=referenciaDTO.getNumeroReferencia();
								 referencias.add(referenciaDTO);                                                                                                          
							 }
						 }
						 count++;
					 }
				 }
				 DepositoBancarioDTO deposito  =null;
				 for(ReferenciaBancariaDTO referenciaDTO :referencias ){
					 DepositoBancarioDTO filtro = new DepositoBancarioDTO();
					 filtro.setReferencia(referenciaDTO.getNumeroReferencia());
					 List<DepositoBancarioDTO> depositos =this.buscarDepositosBancarios(filtro, null, false);
					 if (null!=depositos && !depositos.isEmpty()){
						 deposito=depositos.get(0);
						 if(deposito.getReferencia()==null){
							 deposito.setReferencia(referenciaDTO.getNumeroReferencia());
						 }
						 break;
					 }
				 }
				 if(null!=deposito ){
					 BigDecimal montoDeposito=BigDecimal.ZERO;
					 BigDecimal montoIngreso=BigDecimal.ZERO;
					 if(null!=deposito.getMonto()){
						 montoDeposito= deposito.getMonto();                                                                                            
					 }
					 if(null!=ingreso.getMonto()){
						 montoIngreso= ingreso.getMonto();                                                                                                  
					 }
					 try {
						 String value = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , PARAMETRO);  
						 BigDecimal variacion=BigDecimal.ONE;
						 if (null!=value && StringUtils.isNumeric(value)){
							 variacion=new BigDecimal (value);
						 }
						 BigDecimal diferencia = montoIngreso.subtract(montoDeposito);						 
						 if(diferencia.compareTo(variacion) ==1 || diferencia.compareTo(BigDecimal.ZERO) ==-1  ){
							 this.bitacoraService.registrar(TIPO_BITACORA.INGRESO_APL_AUTOMATICA, EVENTO.INGRESO_APL_AUTOMATICA_RECHAZO, "Ingreso: "+ingreso.getId().toString()+", Ref: "+deposito.getReferencia() , "Cantidad del deposito $"+montoDeposito+" e Ingreso $"+montoIngreso+" no cuadran. Variacion "+variacion ,"", usuarioService.getUsuarioActual().getNombreUsuario());
							 continue;
						 }
						 LogDeMidasInterfaz.log("Aplica Ingreso "+ ingreso.getId() + this, Level.INFO, null);
						 this.aplicarIngreso(ingreso.getId().toString(),deposito.getRefunic(), null, null);
						 this.bitacoraService.registrar(TIPO_BITACORA.INGRESO_APL_AUTOMATICA, EVENTO.INGRESO_APL_AUTOMATICA_EXITO, "Ingreso: "+ingreso.getId().toString()+", Ref: "+deposito.getReferencia() , "Aplicacion automatica Exitosa" ,"", usuarioService.getUsuarioActual().getNombreUsuario());

					 } catch (MidasException e) {
						 continue;
					 }
				 }
			 }
		 }
			LogDeMidasInterfaz.log("Fin Ejecucion aplicarIngresoAutomatico.." + this, Level.INFO, null);


	 }

	 @Override
	 public List<ListarIngresoDTO> buscarIngresos(ListarIngresoDTO filtro) {
		 // TODO Auto-generated method stub
		 return null;
	 }

	 @Override
	 public List<MovimientoAcreedorDTO> buscarMovimientosAcreedores(
			 MovimientoAcreedorDTO filtro, String movAcreedoresConcat,
			 Boolean esConsulta) {
		 List<MovimientoAcreedorDTO> resultados = ingresoDao.buscarMovimientosAcreedores(filtro, movAcreedoresConcat, esConsulta);
		 return resultados;
	 }




	 @Override
	 public String cancelacionAcuenta(Long idIngreso, String motivo, String cuentaDeposito, String comentario, int formaAplicar, int tipoCancelacion,int subTipoCancelacion) {
		 
		 AplicacionCobranza aplicacionCancelacion = null;
		 List<Ingreso> lIngresos = new ArrayList<Ingreso>();
		 List<MovimientoCuentaAcreedor> movimientosAcreedores = null; 
		 String mensajeRegreso = "";
		
		 try{
			
			 String tipoCancelacionAplicar = "";
			 Bitacora.EVENTO evento = null;
			 AplicacionCobranzaIngreso aplicacionIngreso = this.obtenerAplicacionActualPorIngreso(idIngreso, null);
			 AplicacionCobranza aplicacion = aplicacionIngreso.getAplicacion();
	
			// SI EL INGRESO ESTA RELACIONADO A UNA RECUPERACION DE JURIDICO O CRUCERO TRONAR LA OPERACION
			//Se elimina la validacion por la modificacion en el caso de uso CDU Cancelar Ingreso Pendiente por Aplicar
//			 this.ingresoNoValido(idIngreso);
	
			 if( formaAplicar == TODOS_LOS_INGRESOS ){
	
				 // OBTENER TODOS LOS INGRESOS(HERMANOS) ASOCIADOS A LA APLICACION POR MEDIO DEL INGRESO SELECCIONADO 
				 for(AplicacionCobranzaIngreso lAplicacionesIngreso : aplicacion.getAplicacionesIngreso() ){
					 Ingreso ingresoValido = lAplicacionesIngreso.getIngreso();
					 if(ingresoValido.getEstatus().equals(Ingreso.EstatusIngreso.APLICADO.getValue())){
						 lIngresos.add(ingresoValido);
					 }
				 }
	
			 }else{
				 Ingreso ingresoACancelar = this.obtenerIngreso(idIngreso);
				 lIngresos.add(ingresoACancelar);
			 }
			 
			 // VALIDAR SI SE PUEDE CANCELAR EL INGRESO
			 if( this.isPerdidaTotalCancelarIngreso(lIngresos, CANCELACION_CUENTA_ACREEDORA) ) {
				 throw new NegocioEJBExeption("CANC_CUENTA_ACREEDORA","EL INGRESO ("+lIngresos.get(0).getNumero()+") NO SE PUEDE CANCELAR POR SER DE PERDIDA TOTAL Y/O TENER UNA RECUPERACIÓN DE DEDUCIBLE ORIGINADO EN UN PASE") ;
			 }
			 
			 /*List<OrdenPagoSiniestro> lOrdenesPago = null;
			 for(Ingreso ingreso: lIngresos){
				 lOrdenesPago  = this.obtenerOrdenesPago(ingreso.getRecuperacion());
				 // # SI EXISTE AL MENOS UNA ORDEN DE PAGO CON PERDIDA TOTAL MANDAR ERROR
				 if( this.isPerdidaTotal(lOrdenesPago) ){
					 throw new NegocioEJBExeption("CANC_CUENTA_ACREEDORA","EL INGRESO ("+ingreso.getNumero()+") NO SE PUEDE CANCELAR POR SER DE PERDIDA TOTAL")
				 }
			 }*/
	
			 // # ITERAR LOS INGRESOS POR CANCELAR
			 for( Ingreso lIngreso : lIngresos ){
				 Ingreso ingresoActualizado = null;
				 Recuperacion recuperacionActualizada = null;
				 if(tipoCancelacion == CANCELACION_CUENTA_ACREEDORA){
	
					 evento = Bitacora.EVENTO.CANCELAR_INGRESO_CUENTA_ACREEDORA;
	
					 if( subTipoCancelacion == TIPO_CANCELACION_REVERSA ){
	
						 ingresoActualizado = this.actualizaIngreso(lIngreso, motivo, Ingreso.EstatusIngreso.PENDIENTE, Boolean.FALSE);
						 recuperacionActualizada = this.actualizaRecuperacion(lIngreso.getRecuperacion(), Recuperacion.EstatusRecuperacion.PENDIENTE );
	
						 tipoCancelacionAplicar="CANCELACION CUENTA ACREEDORA subtipo -> TIPO_CANCELACION_REVERSA";
	
					 }else  if( subTipoCancelacion == TIPO_CANCELACION_CANCELADO ){
	
						 ingresoActualizado = this.actualizaIngreso(lIngreso, motivo, Ingreso.EstatusIngreso.CANCELADO_ENVIO_CUENTA_ACREEDORA, Boolean.TRUE);
						 if(lIngreso.getRecuperacion().getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.SIPAC.toString()) ){
							 recuperacionActualizada = this.actualizaRecuperacion(lIngreso.getRecuperacion(), Recuperacion.EstatusRecuperacion.PENDIENTE );
						 } else {
							 recuperacionActualizada = this.actualizaRecuperacion(lIngreso.getRecuperacion(), Recuperacion.EstatusRecuperacion.REGISTRADO );
						 }
	
						 tipoCancelacionAplicar="CANCELACION CUENTA ACREEDORA subtipo -> TIPO_CANCELACION_CANCELADO";
					 }
				 }else if (tipoCancelacion == CANCELACION_POR_DEVOLUCION){
	
					 evento = Bitacora.EVENTO.CANCELAR_INGRESO_DEVOLUCION;
	
					 tipoCancelacionAplicar = "CANCELACION POR DEVOLUCION";
					 ingresoActualizado = this.actualizaIngreso(lIngreso, motivo, Ingreso.EstatusIngreso.CANCELADO_POR_DEVOLUCION, Boolean.TRUE);
				 	 
					 if(lIngreso.getRecuperacion().getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.SIPAC.toString()) ){
						 recuperacionActualizada = this.actualizaRecuperacion(lIngreso.getRecuperacion(), Recuperacion.EstatusRecuperacion.PENDIENTE );
					 } else {
						 recuperacionActualizada = this.actualizaRecuperacion(lIngreso.getRecuperacion(), Recuperacion.EstatusRecuperacion.REGISTRADO ); 
					 }
					 //Genera la solicitud de cheque por ingreso 
					 IngresoDevolucion ingresoDevolucion = new IngresoDevolucion();
					 ingresoDevolucion.setIngreso(lIngreso);
					 ingresoDevolucionService.generar(ingresoDevolucion, Long.valueOf(cuentaDeposito));
					 
					 // CANCELAR ADJUDICACION DEL SALVAMENTO SI EXISTE
					 if( lIngreso.getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.SALVAMENTO.getValue() ) ) {
						 this.recuperacionSalvamentoService.cancelarAdjudicacion( lIngreso.getRecuperacion().getId(), "CANCELACION DESDE INGRESO APLICADO POR DEVOLUCIÓN" , false);
					 }
					 
				 }
	
				 this.entidadService.save(ingresoActualizado);
				 this.entidadService.save(recuperacionActualizada);
				 
			 }
			 
			 String codigoUsuarioCreacion = usuarioService.getUsuarioActual().getNombreUsuario();
			 aplicacionCancelacion = new AplicacionCobranza(AplicacionCobranza.TipoAplicacionCobranza.C, motivo , codigoUsuarioCreacion  );
			 
			 IngresoService processorService = this.sessionContext.getBusinessObject(IngresoService.class);
			 movimientosAcreedores = processorService.generarAplicacionCancelacion(aplicacion, null,null, lIngresos, cuentaDeposito, aplicacionCancelacion );
			 processorService.contabilizaCancelacion(aplicacionCancelacion.getId());
			 //GENERAR MOVIMIENTO
			 this.generarMovimientoAjusteReserva(lIngresos);
			 //CREA LAS PROVISIONES EN CASO DE SER DE SALVAMENTO O COMPAÑIA
			 //this.validaProvisiones(lIngresos);
			 // BITACORA
			 for(Ingreso ingreso: lIngresos){
				 this.bitacoraService.registrar(Bitacora.TIPO_BITACORA.INGRESO, evento , ingreso.getId().toString(), tipoCancelacionAplicar,"", this.usuarioService.getUsuarioActual().getNombreUsuario());
			 }
		 
		 }catch(Exception e){
			 
			 mensajeRegreso = ( StringUtil.isEmpty(e.getMessage() ) ? "NO SE PUDO CANCELAR EL INGRESO SELECCIONADO" : e.getMessage()+"." );

			 try {
				 if( aplicacionCancelacion != null ){
					 if(movimientosAcreedores!=null && !movimientosAcreedores.isEmpty()){
						 this.entidadService.removeAll(movimientosAcreedores);
					 }
					 this.entidadService.remove(aplicacionCancelacion);
				 }
		
				 for( Ingreso ingreso : CollectionUtils.emptyIfNull(lIngresos)){
					 ingreso.setCodigoUsuarioCancelacion(null);
					 ingreso.setMotivoCancelacion(null);
					 ingreso.setFechaCancelacion(null);
					 ingreso.setEstatus(Ingreso.EstatusIngreso.APLICADO.getValue());
					 ingreso.getRecuperacion().setEstatus(Recuperacion.EstatusRecuperacion.RECUPERADO.getValue());
					 this.entidadService.save(ingreso.getRecuperacion());
					 this.entidadService.save(ingreso);
				 }
				 
			 }catch(Exception ex) {
				 log.error("ERROR en proceso rollback manual: aplicacionCancelacion,movimientosAcreedores,ingreso,recuperacion ",ex);
				 mensajeRegreso = "NO SE PUDO CANCELAR EL INGRESO SELECCIONADO";
			 }
			 
			 log.error("ERROR EN cancelacionAcuenta",e);
			 
		 }
		 
		 return mensajeRegreso;
	 }
	 
	 
	 @Deprecated
	 private void validaProvisiones(AplicacionCobranza aplicacion){
		 List<Ingreso> ingresos = new ArrayList<Ingreso>();
		 for(AplicacionCobranzaIngreso appCobIngreso : aplicacion.getAplicacionesIngreso()){
			 ingresos.add(appCobIngreso.getIngreso());
		 }
		 this.validaProvisiones(ingresos);
	 }
	 
	 @Deprecated
	 private void validaProvisiones(List<Ingreso> ingresos){
		 for(Ingreso ingreso : ingresos){
			 if(ingreso.getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.COMPANIA.getValue())){
				 this.recuperacionService.invocaProvision(ingreso.getRecuperacion().getId());
			 }
		 }
	 }


	 private ConceptoGuia obtenerConceptoGuiaParaIngreso(String tipoRecuperacion){
		 ConceptoGuia.TIPO_CONCEPTO_GUIA tipo = null;
		 if(tipoRecuperacion.equals(Recuperacion.TipoRecuperacion.PROVEEDOR.toString())){
			 tipo = ConceptoGuia.TIPO_CONCEPTO_GUIA.INGRESO_PROVEEDOR;
		 }else if(tipoRecuperacion.equals(Recuperacion.TipoRecuperacion.DEDUCIBLE.toString())){
			 tipo = ConceptoGuia.TIPO_CONCEPTO_GUIA.INGRESO_DEDUCIBLE;
		 }else if(tipoRecuperacion.equals(Recuperacion.TipoRecuperacion.COMPANIA.toString())){
			 tipo = ConceptoGuia.TIPO_CONCEPTO_GUIA.INGRESO_COMPANIA;
		 }else if(tipoRecuperacion.equals(Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.toString())){
			 tipo = ConceptoGuia.TIPO_CONCEPTO_GUIA.INGRESO_CRUC_JUR;
		 }else if(tipoRecuperacion.equals(Recuperacion.TipoRecuperacion.SALVAMENTO.toString())){
			 tipo = ConceptoGuia.TIPO_CONCEPTO_GUIA.INGRESO_SALVAMENTO;
		 }else if(tipoRecuperacion.equals(Recuperacion.TipoRecuperacion.SIPAC.toString())){
			 tipo = ConceptoGuia.TIPO_CONCEPTO_GUIA.INGRESO_SIPAC;
		 }              
		 List<ConceptoGuia> conceptos =  entidadService.findByProperty(ConceptoGuia.class, "tipo", tipo);
		 if(conceptos != null && conceptos.size() > 0){
			 return conceptos.get(0);
		 }
		 return null;
	 }


	 private ConceptoGuia obtenerConceptoGuiaParaCuenta(String codigo, Long id){
		 List<ConceptoGuia> conceptos =null;
		 if(id!=null){
			 conceptos=  entidadService.findByProperty(ConceptoGuia.class, "id", id);
		 }else{
			 conceptos =  entidadService.findByProperty(ConceptoGuia.class, "codigo", codigo);
		 }
		 if(conceptos != null && conceptos.size() > 0){
			 return conceptos.get(0);
		 }
		 return null;
	 }



	 @Override
	 public List<String>  cancelacionDeReversa(Long idIngreso, String motivo,String cuentaDeposito, String comentario,String ngresosPendientesConcat,int formaAplicar,int tipoCancelacion){

		 String[] aIngresos = ngresosPendientesConcat.split(",");
		 String tipoCancelacionAplicar = "";
		 List<Ingreso> lIngresosPorAplicar = new ArrayList<Ingreso>();
		 List<Ingreso> lIngresosPorCancelar = new ArrayList<Ingreso>();
		 List<Ingreso> lValidarPT = new ArrayList<Ingreso>(); // SE UNEN lIngresosPorAplicar Y lIngresosPorCancelar PARA VALIDAR LA PT
		 List<MovimientoCuentaAcreedor> movimientos = null;
		 List<String> lMensajeValidaciones = new ArrayList<String>();
		 BigDecimal totalPorAplicar = BigDecimal.ZERO,totalPorcancelar = BigDecimal.ZERO;
		 List<Ingreso> lIngresos = new ArrayList<Ingreso>();
		 AplicacionCobranzaIngreso aplicacionIngreso = null;
		 AplicacionCobranza aplicacion = null;
		 AplicacionCobranza aplicacionCancelacion = null;
		 String mensajeRegreso = "";

		 try {
			 
			 aplicacionIngreso = this.obtenerAplicacionActualPorIngreso(idIngreso, null);
			 aplicacion = aplicacionIngreso.getAplicacion();
	
			 for( int i=0; i<=aIngresos.length-1; i++){
				 lIngresosPorAplicar.add( this.obtenerIngreso(new Long(aIngresos[i])) );
			 }
	
			 if( formaAplicar == TODOS_LOS_INGRESOS ){
	
				 // OBTENER TODOS LOS INGRESOS(HERMANOS) ASOCIADOS A LA APLICACION POR MEDIO DEL INGRESO SELECCIONADO 
				 for(AplicacionCobranzaIngreso lAplicacionesIngreso : aplicacion.getAplicacionesIngreso() ){
					 Ingreso ingresoValido = lAplicacionesIngreso.getIngreso();
					 if(ingresoValido.getEstatus().equals(Ingreso.EstatusIngreso.APLICADO.getValue())){
						 lIngresosPorCancelar.add(ingresoValido);
					 }
				 }
	
			 }else{
				 Ingreso ingresoACancelar = this.obtenerIngreso(idIngreso);
				 lIngresosPorCancelar.add(ingresoACancelar);
	
			 }
			 
			 // # VALIDAR NINGUN INGRESO SE ENCUENTRE COMO PERDIDA TOTAL
			 lValidarPT.addAll(lIngresosPorCancelar);
			 lValidarPT.addAll(lIngresosPorAplicar);
			 
			 // VALIDAR SI SE PUEDE CANCELAR EL INGRESO
			 if( this.isPerdidaTotalCancelarIngreso(lValidarPT, CANCELACION_POR_DEVOLUCION) ) {
				 throw new NegocioEJBExeption("CANC_CUENTA_ACREEDORA","EL INGRESO ("+lIngresos.get(0).getNumero()+") NO SE PUEDE CANCELAR POR SER DE PERDIDA TOTAL Y/O TENER UNA RECUPERACIÓN DE DEDUCIBLE ORIGINADO EN UN PASE"); 
			 }
			 
			 /*List<OrdenPagoSiniestro> lOrdenesPago = null;
			 for(Ingreso ingresoPT: lValidarPT){
				 lOrdenesPago  = this.obtenerOrdenesPago(ingresoPT.getRecuperacion());
				 // # SI EXISTE AL MENOS UNA ORDEN DE PAGO CON PERDIDA TOTAL MANDAR ERROR
				 if( this.isPerdidaTotal(lOrdenesPago) ){
					 throw new NegocioEJBExeption("CANC_CUENTA_ACREEDORA","EL INGRESO ("+ingresoPT.getNumero()+") NO SE PUEDE CANCELAR POR SER DE PERDIDA TOTAL")
				 }
				 
			 }*/
	
			 // # VALIDAR INGRESOS CONCATENADOS SE ENCUENTREN EN ESTATUS DE PENDIENTE
			 lMensajeValidaciones = this.sonIngresosValidosPorAplicar(lIngresosPorCancelar, lIngresosPorAplicar);
	
			 if( lMensajeValidaciones.isEmpty() ){
	
				 for( Ingreso lIngreso : lIngresosPorAplicar ){
					 totalPorAplicar = totalPorAplicar.add(lIngreso.getMonto());
				 }
	
				 StringBuilder ingresosConcat = new StringBuilder();
	
				 // # ITERAR LOS INGRESOS POR CANCELAR
				 Boolean esPrimero = Boolean.TRUE;
				 for( Ingreso lIngreso : lIngresosPorCancelar ){
					 lIngresos.add(lIngreso);
					 Ingreso ingresoActualizado = null;
					 Recuperacion recuperacionActualizada = null;
					 if( tipoCancelacion == TIPO_CANCELACION_REVERSA ){
						 tipoCancelacionAplicar = "TIPO CANCELACION REVERSA";
						 ingresoActualizado = this.actualizaIngreso(lIngreso, motivo, Ingreso.EstatusIngreso.PENDIENTE, Boolean.FALSE);
						 recuperacionActualizada = this.actualizaRecuperacion(lIngreso.getRecuperacion(), Recuperacion.EstatusRecuperacion.PENDIENTE );
					 }else  if( tipoCancelacion == TIPO_CANCELACION_CANCELADO ){
						 tipoCancelacionAplicar = "TIPO CANCELACION CANCELADO";
						 ingresoActualizado = this.actualizaIngreso(lIngreso, motivo, Ingreso.EstatusIngreso.CANCELADO_POR_REVERSA, Boolean.TRUE);
						 recuperacionActualizada = this.actualizaRecuperacion(lIngreso.getRecuperacion(), Recuperacion.EstatusRecuperacion.REGISTRADO );
					 }
					 this.entidadService.save(ingresoActualizado);
					 this.entidadService.save(recuperacionActualizada);
					 totalPorcancelar = totalPorcancelar.add(lIngreso.getMonto());
	
					 if(esPrimero){
						 esPrimero = Boolean.FALSE;
					 }else{
						 ingresosConcat.append(",");
					 }
					 ingresosConcat.append(lIngreso.getId());
	
				 }
				 
				 String codigoUsuarioCreacion = usuarioService.getUsuarioActual().getNombreUsuario();
				 aplicacionCancelacion = new AplicacionCobranza(AplicacionCobranza.TipoAplicacionCobranza.C, motivo , codigoUsuarioCreacion  );
				 // APLICAR INGRESO
	
				 IngresoService processorService = this.sessionContext.getBusinessObject(IngresoService.class);
				 movimientos = processorService.generarAplicacionCancelacion(aplicacion, null,null, lIngresos, cuentaDeposito, aplicacionCancelacion );
				 processorService.contabilizaCancelacion(aplicacionCancelacion.getId());
				 StringBuilder movimientosConcat = new StringBuilder();
				 esPrimero = Boolean.TRUE;
				 for(MovimientoCuentaAcreedor movimiento : movimientos){
					 if(esPrimero){
						 esPrimero = Boolean.FALSE;
					 }else{
						 movimientosConcat.append(",");
					 }
					 movimientosConcat.append(movimiento.getId());
				 }
	
				 this.aplicarIngreso(ngresosPendientesConcat, null, movimientosConcat.toString(), null);
				 this.generarMovimientoAjusteReserva(lIngresosPorAplicar);
					 
			 }
	
			 // BITACORA
			for(Ingreso ingreso: lIngresos){
				this.bitacoraService.registrar(Bitacora.TIPO_BITACORA.INGRESO, Bitacora.EVENTO.CANCELAR_INGRESO_DEVOLUCION, ingreso.getId().toString(),tipoCancelacionAplicar,"", this.usuarioService.getUsuarioActual().getNombreUsuario());
			}
		
		}catch(Exception e) {
			
			mensajeRegreso = ( StringUtil.isEmpty(e.getMessage() ) ? "NO SE PUDO CANCELAR EL INGRESO SELECCIONADO" : e.getMessage()+"." );
			
			if(aplicacionCancelacion!=null){
				 if(movimientos!=null && !movimientos.isEmpty()){
					 this.entidadService.removeAll(movimientos);
				 }
				 this.entidadService.remove(aplicacionCancelacion);
			 }
			 
		     for( Ingreso ingreso : CollectionUtils.emptyIfNull(lIngresosPorCancelar)){		 
				 ingreso.setCodigoUsuarioCancelacion(null);
				 ingreso.setMotivoCancelacion(null);
				 ingreso.setFechaCancelacion(null);
				 ingreso.setEstatus(Ingreso.EstatusIngreso.APLICADO.getValue());
				 ingreso.getRecuperacion().setEstatus(Recuperacion.EstatusRecuperacion.RECUPERADO.getValue());
				 this.entidadService.save(ingreso.getRecuperacion());
				 this.entidadService.save(ingreso);
			 }
			 lMensajeValidaciones.add(mensajeRegreso);
		}

		 return lMensajeValidaciones;
	 }

	 @Override
	 @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	 public void contabilizaCancelacion(Long aplicacionCancancelacionId){
		 try {
			this.ingresoDao.contabilizaAplicacionIngreso(aplicacionCancancelacionId);
		} catch (MidasException e) {		
			e.printStackTrace();
		}
	 }



	 private void generarMovimientoAjusteReserva(List<Ingreso> ingresosCancelacion){

		 for(Ingreso lIngresos : ingresosCancelacion ){
			 this.generarMovimientosIngreso(lIngresos.getId(), TipoAplicacionCobranza.C.toString());
		 }

	 }


	 @Override
	 @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	 public List<MovimientoCuentaAcreedor> generarAplicacionCancelacion(AplicacionCobranza aplicacionOrigen, TipoAplicacionCobranza tipoAplicacion,String descripcion, List<Ingreso> listaIngresos, String cuentaDeposito,AplicacionCobranza aplicacionCancelacion){

		 String codigoUsuarioCreacion = usuarioService.getUsuarioActual().getNombreUsuario();
		 List<MovimientoCuentaAcreedor> movimientos = new ArrayList<MovimientoCuentaAcreedor>();


		 if( !listaIngresos.isEmpty() ){
			 ConceptoGuia conceptoGuia = null;
			 for( Ingreso lIngreso : listaIngresos ){
				 conceptoGuia = obtenerConceptoGuiaParaIngreso(lIngreso.getRecuperacion().getTipo());
				 // APLICACION DE INGRESO
				 aplicacionCancelacion.agregar(lIngreso, TipoAplicacionIngreso.A , codigoUsuarioCreacion, conceptoGuia);

				 // CUENTA DEPOSITO VIENE DEL FRONT Y SE INDENTIFICA COMO CUENTA ACREEDORA
				 ConceptoGuia conceptoAcreedor = this.entidadService.findById(ConceptoGuia.class, new Long(cuentaDeposito));
				 // APLICACION COBRANZA
				 aplicacionCancelacion.agregar(cuentaDeposito, TipoAplicacionCuenta.A, lIngreso.getMonto(), 
						 BigDecimal.ZERO, usuarioService.getUsuarioActual().getNombreUsuario(), "Por cancelacion", conceptoAcreedor,AplicacionCobranzaCuenta.TipoOperacionCobranza.A);


				 if(aplicacionCancelacion.getId() == null){
					 this.entidadService.save(aplicacionCancelacion);  
				 }
				 MovimientoCuentaAcreedor movimiento = this.generarMovimientoAcreedor(aplicacionOrigen.getId(), aplicacionCancelacion.getId() , null, new Long(cuentaDeposito) , lIngreso.getMonto() );
				 movimientos.add(movimiento);
			 }
		 }
		 return movimientos;

	 }





	 private boolean isPerdidaTotal( List<OrdenPagoSiniestro> lOrdenesPago ){

		 boolean isPerdidaTotal = false;

		 for( OrdenPagoSiniestro lOrdenPagoSiniestro : lOrdenesPago ){
			 isPerdidaTotal = this.liquidacionSiniestroService.isIndemnizacionesPT(lOrdenPagoSiniestro);

			 if( isPerdidaTotal ){
				 break;
			 }
		 }

		 return isPerdidaTotal;
	 }



	 private Ingreso actualizaIngreso(Ingreso ingreso,String motivo, Ingreso.EstatusIngreso estatusIngreso, Boolean seCancela ){

		 Date hoy = new Date();
		 if(seCancela){
			 ingreso.setFechaCancelacion (hoy);
			 ingreso.setCodigoUsuarioCancelacion(usuarioService.getUsuarioActual().getNombreUsuario());
		 }
		 ingreso.setFechaModificacion(hoy);
		 ingreso.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		 ingreso.setMotivoCancelacion(motivo);
		 ingreso.setEstatus(estatusIngreso.getValue());
		 return ingreso;
	 }

	 private Recuperacion actualizaRecuperacion(Recuperacion recuperacion, Recuperacion.EstatusRecuperacion estatus){

		 Date hoy = new Date();

		 recuperacion.setFechaModificacion(hoy);
		 recuperacion.setCodigoUsuarioModificacion((usuarioService.getUsuarioActual().getNombreUsuario()));
		 recuperacion.setEstatus(estatus.name());

		 return recuperacion;
	 }


	 /**
	  * 
	  * Obtener el Ingreso correspondiente mediante un entidadService.findById
	  * 
	  * @param id
	  */
	  @Override
	  public Ingreso obtenerIngreso(Long id) {
		  return this.entidadService.findById(Ingreso.class, id);
	  }
	  /**
	   *Obtener el listado de Movimientos Acreedores asociados a la aplicacion del
	   * Ingreso.
	   * @param ingresoId
	   */
	  @Override
	  public List<MovimientoAcreedorDTO> obtenerMovtoAcreedorAplicacion( Long ingresoId) {
		  List<MovimientoAcreedorDTO> movimientosAcreedorDTO  = new ArrayList<MovimientoAcreedorDTO>();

		  Ingreso ingreso = this.obtenerIngreso(ingresoId);
		  if(null!=ingreso){
			  List<AplicacionCobranzaIngreso> aplicaciones  =this.entidadService.findByProperty(AplicacionCobranzaIngreso.class, "ingreso.id", ingresoId);
			  for(AplicacionCobranzaIngreso aplicacion : aplicaciones){
				  if(  Utilerias.removerHorasMinutosSegundos( aplicacion.getAplicacion().getFechaCreacion()) .compareTo(Utilerias.removerHorasMinutosSegundos( ingreso.getFechaIngreso())   )==0){
					  Map<String,Object> params = new LinkedHashMap<String,Object>();
					  params.put("aplicacion.id", aplicacion.getId().getAplicacionId());
					  params.put("tipo",AplicacionCobranzaCuenta.TipoAplicacionCuenta.A);
					  List<AplicacionCobranzaCuenta>  depositos = this.entidadService.findByProperties(AplicacionCobranzaCuenta.class, params);
					  String acreedorConcat="";                                                           
					  for (AplicacionCobranzaCuenta deposito : depositos){
						  acreedorConcat=deposito.getIdentificador();
						  MovimientoAcreedorDTO filtro =new MovimientoAcreedorDTO ();
						  filtro.setMovAcreedoresConcat(acreedorConcat);
						  if(!StringUtil.isEmpty(acreedorConcat.trim())){
							  List<MovimientoAcreedorDTO> lst= this.buscarMovimientosAcreedores(filtro, acreedorConcat, true);
							  if(null!=lst && !lst.isEmpty()){
								  MovimientoAcreedorDTO dto =lst.get(0);
								  dto.setImporte(deposito.getMontoAplicado());
								  movimientosAcreedorDTO.add(dto);
							  }
						  }
					  }
				  }
			  }
		  }
		  return movimientosAcreedorDTO;

	  }
	  
	  @Override
	  public List<MovimientoManualDTO> obtenerMovtoManualAplicacion(Long ingresoId) {
		  List<MovimientoManualDTO> movimientosManuales  = new ArrayList<MovimientoManualDTO>();

		  Ingreso ingreso = this.obtenerIngreso(ingresoId);
		  if (ingreso != null) {
			  List<AplicacionCobranzaIngreso> aplicaciones  =this.entidadService.findByProperty(AplicacionCobranzaIngreso.class, "ingreso.id", ingresoId);
			  for(AplicacionCobranzaIngreso aplicacion : aplicaciones){
				  if (Utilerias.removerHorasMinutosSegundos(aplicacion.getAplicacion().getFechaCreacion()).compareTo(Utilerias.removerHorasMinutosSegundos(ingreso.getFechaIngreso())) == 0) {
					 
					  Map<String,Object> params = new LinkedHashMap<String,Object>();
					  params.put("aplicacion.id", aplicacion.getId().getAplicacionId());
					  params.put("tipo",AplicacionCobranzaCuenta.TipoAplicacionCuenta.M);
					  List<AplicacionCobranzaCuenta> movimientos = this.entidadService.findByProperties(AplicacionCobranzaCuenta.class, params);
					  
					  String movimientosConcat="";                                                         
					  for (AplicacionCobranzaCuenta movimiento : movimientos){
						  movimientosConcat = movimiento.getIdentificador();
						  MovimientoManualDTO filtro =new MovimientoManualDTO();
						  filtro.setMovManualConcat(movimientosConcat);
						  if(!StringUtil.isEmpty(movimientosConcat.trim())){
							  List<MovimientoManualDTO> lst= this.buscarMovimientosManuales(filtro, movimientosConcat, true);
							  if(CollectionUtils.isNotEmpty(lst)){
								  MovimientoManualDTO dto =lst.get(0);
								  dto.setImporte(movimiento.getMontoAplicado());
								  movimientosManuales.add(dto);
							  }
						  }
					  }
				  }
			  }
		  }
		  return movimientosManuales;

	  }



	  @Override
	  public List<OrdenPagoSiniestro> obtenerOrdenesPago(Recuperacion recuperacion) {


		  List<OrdenPagoSiniestro> lOrdenPagoSiniestro = new ArrayList<OrdenPagoSiniestro>();

		  if( recuperacion.getTipo().equals(Recuperacion.TipoRecuperacion.PROVEEDOR.toString() ) ){
			  RecuperacionProveedor recuperacionProveedor = (RecuperacionProveedor)recuperacion;

			  // # OBTENER LA ORDEN DE COMPRA ASOCIADA A LA RECUPERACION
			  OrdenCompra ordenCompra = recuperacionProveedor.getOrdenCompra();
			  lOrdenPagoSiniestro.add(ordenCompra.getOrdenPago());

		  }else if(recuperacion.getTipo().equals(Recuperacion.TipoRecuperacion.DEDUCIBLE.toString() )){

			  HashMap<String,Object> params = new HashMap<String,Object>();
			  RecuperacionDeducible recuperacionDeducible = (RecuperacionDeducible)recuperacion;

			  // # OBTENER LA ORDENPAGOSINIESTRO ASOCIADA A LA ORDEN DE COMPRA
			  params.put("ordenCompra.idTercero", recuperacionDeducible.getEstimacionCobertura().getId() );

			  lOrdenPagoSiniestro = this.entidadService.findByProperties(OrdenPagoSiniestro.class, params);
		  
		  }else if(recuperacion.getTipo().equals(Recuperacion.TipoRecuperacion.SALVAMENTO )){
			  
			  HashMap<String,Object> params = new HashMap<String,Object>();
			  RecuperacionSalvamento recuperacionSalvamento = (RecuperacionSalvamento) recuperacion;
			  
			 // # OBTENER LA ORDENPAGOSINIESTRO ASOCIADA A LA ORDEN DE COMPRA
			  params.put("ordenCompra.idTercero", recuperacionSalvamento.getEstimacion().getId() );

			  lOrdenPagoSiniestro = this.entidadService.findByProperties(OrdenPagoSiniestro.class, params);
		  }

		  return lOrdenPagoSiniestro;
	  }

	  @Override
	  public List<String> sonIngresosValidosPorAplicar(List<Ingreso> lIngresoACancelar,
			  List<Ingreso> lIngresosPorAplicar) {

		  List<String> lMensaje = new ArrayList<String>();
		  BigDecimal totalPorAplicar = BigDecimal.ZERO,totalACancelar = BigDecimal.ZERO;
		  String noIngresoConcatPorAplicar = null;

		  // # VALIDAR QUE ESTEN EN ESTATUS PENDIENTE Y SUMAR EL MONTO DE SOLO LOS PENDIENTES
		  for( Ingreso lIngresoPorAplicar : lIngresosPorAplicar ){
			  if ( lIngresoPorAplicar.getEstatus().equals(Ingreso.EstatusIngreso.PENDIENTE) ){
				  lMensaje.add("EL ingreso "+lIngresoPorAplicar.getNumero()+" no se encuentra en estatus pendiente por aplicar");
			  }else{
				  totalPorAplicar = totalPorAplicar.add(lIngresoPorAplicar.getMonto()) ;
				  noIngresoConcatPorAplicar += lIngresoPorAplicar.getNumero();
			  }
		  }

		  if( lMensaje.isEmpty() ){

			  // SUMAR EL TOTAL DE LOS IN
			  for( Ingreso lIngreso : lIngresoACancelar ){
				  totalACancelar = totalACancelar.add(lIngreso.getMonto()) ;
			  }              

			  // # TOTALPENDIENTE DEDE SER MENOR O IGUAL AL MONTO DEL INGRESO A CANCELAR, SI NO, SE AGREGA A LISTA DE MENSAJES
			  if( totalPorAplicar.compareTo(totalACancelar) > 0  ){
				  lMensaje.add("Los ingresos a cancelar no tienen saldo suficiente para trapasar a los ingresos pendientes ");
			  }

		  }

		  return lMensaje;
	  }

	  @Override
	  public ImportesIngresosDTO calculaImportes (String ingresosId, String depositos, String movAcreedores, String movManuales, Boolean consulta){
		  ImportesIngresosDTO importesDTO=new ImportesIngresosDTO();
		  List<ListarIngresoDTO> lstIngresoDTO= new ArrayList<ListarIngresoDTO>();
		  List<DepositoBancarioDTO> lstDepositos= new ArrayList<DepositoBancarioDTO>();
		  List<MovimientoAcreedorDTO> lstAcreedor= new ArrayList<MovimientoAcreedorDTO>();     
		  List<MovimientoManualDTO> lstManuales = new ArrayList<MovimientoManualDTO>();
		  BigDecimal montoIngresos= BigDecimal.ZERO;
		  BigDecimal montoDepositos= BigDecimal.ZERO;
		  BigDecimal montoAcreedores= BigDecimal.ZERO;
		  BigDecimal montoManual= BigDecimal.ZERO;
		  BigDecimal sumaMovs=BigDecimal.ZERO;
		  BigDecimal diferencia=BigDecimal.ZERO;             
		  if(!consulta){
			  if(!StringUtil.isEmpty(ingresosId)){
				  ingresosId=ingresosId.trim();
				  lstIngresoDTO= this.buscarIngresos(ingresosId);
				  for (ListarIngresoDTO  ingresoDTO : lstIngresoDTO){
					  if(null!=ingresoDTO.getMontoFinalRecuperado()){
						  montoIngresos =montoIngresos.add(ingresoDTO.getMontoFinalRecuperado());
					  }
				  }
			  }
			  if(!StringUtil.isEmpty(depositos)){
				  String depositoId= depositos.trim();
				  lstDepositos= this.buscarDepositosBancarios(depositoId, true);
				  for (DepositoBancarioDTO  depositoDTO : lstDepositos){
					  if(null!=depositoDTO.getMonto()){
						  montoDepositos =montoDepositos.add(depositoDTO.getMonto());
					  }
				  }
			  }
			  if(!StringUtil.isEmpty(movAcreedores)){
				  String acreedoresId= movAcreedores.trim();
				  MovimientoAcreedorDTO filtro = new  MovimientoAcreedorDTO();
				  filtro.setMovAcreedoresConcat(acreedoresId);
				  lstAcreedor= this.buscarMovimientosAcreedores(filtro, acreedoresId, true);
				  for (MovimientoAcreedorDTO  acreedorDTO : lstAcreedor){
					  if(null!=acreedorDTO.getImporte()){
						  montoAcreedores =montoAcreedores.add(acreedorDTO.getImporte());
					  }
				  }
			  }
			  if(!StringUtil.isEmpty(movManuales)){
				  String movimientos= movManuales.trim();
				  MovimientoManualDTO filtro = new  MovimientoManualDTO();
				  filtro.setMovManualConcat(movimientos);
				  lstManuales= this.buscarMovimientosManuales(filtro, movimientos, true);
				  for (MovimientoManualDTO  manualDTO : lstManuales){
					  if(null!=manualDTO.getImporte()){
						  montoManual =montoManual.add(manualDTO.getImporte());
					  }
				  }
			  }
		  }else{
			  Long ingresoId = new Long( 0);
			  if(!StringUtils.isEmpty(ingresosId)  && StringUtils.isNumeric(ingresosId.trim())    ){
				  ingresoId= new Long( ingresosId.trim());
			  }


			  lstIngresoDTO= this.obtenerIngresosAplicacion(ingresoId);			  
			  for (ListarIngresoDTO  ingresoDTO : CollectionUtils.emptyIfNull(lstIngresoDTO)){
				  if(null!=ingresoDTO.getMontoFinalRecuperado()){
					  montoIngresos =montoIngresos.add(ingresoDTO.getMontoFinalRecuperado());
				  }
			  }
			  
			  lstDepositos= this.obtenerDepositosAplicacion(ingresoId);			 
			  for (DepositoBancarioDTO  depositoDTO : CollectionUtils.emptyIfNull(lstDepositos)){
				  if(null!=depositoDTO.getMonto()){
					  montoDepositos =montoDepositos.add(depositoDTO.getMonto());
				  }
			  }
			  
			  lstAcreedor= this.obtenerMovtoAcreedorAplicacion(ingresoId);		
			  for (MovimientoAcreedorDTO  acreedorDTO :  CollectionUtils.emptyIfNull(lstAcreedor)){
				  if(null!= acreedorDTO.getImporte()){
					  montoAcreedores=montoAcreedores.add(acreedorDTO.getImporte());
				  }
			  }
			  
			  lstManuales= this.obtenerMovtoManualAplicacion(ingresoId);			
			  for (MovimientoManualDTO  manualDto : CollectionUtils.emptyIfNull(lstManuales)){
				  if(null!= manualDto.getImporte()){
					  montoManual=montoManual.add(manualDto.getImporte());
				  }
			  }
		
		  }
		  sumaMovs=sumaMovs.add(montoDepositos).add(montoAcreedores).add(montoManual);
		  importesDTO.setTotalesDepositosBancarios(montoDepositos);
		  importesDTO.setTotalesIngresosAplicar(montoIngresos);
		  importesDTO.setTotalesMovimientosAcreedores(montoAcreedores);
		  importesDTO.setTotalesMovimientosManuales(montoManual);
		  diferencia= montoIngresos.subtract(sumaMovs);
		  importesDTO.setDiferencia(diferencia);
		  return importesDTO;
	  }

	  @Override
	  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	  public void contabilizaAplicacionIngreso(Long aplicacionId) throws MidasException{
		  this.ingresoDao.contabilizaAplicacionIngreso(aplicacionId);
	  }


	  @Override
	  public String validaAplicarIngreso(String ingresosId, String depositos,
			  String movAcreedores, String movManuales) throws MidasException {
		  List<ListarIngresoDTO> lstIngresoDTO= new ArrayList<ListarIngresoDTO>();
		  if(!StringUtil.isEmpty(ingresosId)){
			  ingresosId= ingresosId.trim();
			  lstIngresoDTO= this.buscarIngresos(ingresosId);
		  }
		  ImportesIngresosDTO importe  = this.calculaImportes(ingresosId, depositos, movAcreedores, movManuales, false);
		  //VALIDACIONES INGRESOS
		  BigDecimal montoIngresos=importe.getTotalesIngresosAplicar();
		  BigDecimal montoDepositos= importe.getTotalesDepositosBancarios();
		  BigDecimal montoMovtosAcreedor= importe.getTotalesMovimientosAcreedores();
		  BigDecimal montoMovtosManual=importe.getTotalesMovimientosManuales();
		  BigDecimal sumaMovtos= montoDepositos.add(montoMovtosAcreedor).add(montoMovtosManual);
		  if(null!=lstIngresoDTO && !lstIngresoDTO.isEmpty()){
			  ListarIngresoDTO ingreso = lstIngresoDTO.get(0);
			  for (ListarIngresoDTO  ingresoDTO : lstIngresoDTO){
				  if(!ingresoDTO.getEstatus().equalsIgnoreCase(Ingreso.EstatusIngreso.PENDIENTE.getValue())){
					  return ("No es posible Aplicar el Ingreso, Un ingreso tiene estatus diferente de PENDIENTE ");
				  }
				  if(!ingreso.getTipoRecuperacion().equalsIgnoreCase(ingreso.getTipoRecuperacion())){
					  return ("No es posible Aplicar el Ingreso, Los ingresos deben ser del misto tipo de Recuperacion ");
				  }
			  }
		  }
		  String value = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , PARAMETRO);     
		  BigDecimal variacion=BigDecimal.ONE;
		  if (null!=value && StringUtils.isNumeric(value)){
			  variacion=new BigDecimal (value);
		  }

		  if(sumaMovtos.compareTo(BigDecimal.ZERO)==0){
			  return ("No es posible Aplicar el Ingreso, La suma de los movimientos  (Monto Depositos + Monto Movimientos Acreedores) Debe ser Mayor a Cero");

		  }              
		  BigDecimal diferencia = montoIngresos.subtract(sumaMovtos);
		  if(diferencia.compareTo(variacion) ==1){
			  return ("No es posible Aplicar el Ingreso, La diferencia  Monto Ingreso - (Monto Depositos - Monto Movimientos Acreedores) es mayor a $"+variacion+".00");
		  }
		  return null;
	  }

	  /***
	   * Valida si el ingreso viene de Juridico o Crucero
	   * @param ingresoId
	   */
	  private void ingresoNoValido(Long ingresoId) {
		  Ingreso ingreso = this.entidadService.findById(Ingreso.class, ingresoId);

		  if (ingreso != null) {
			  if (EnumUtil.equalsValue(TipoRecuperacion.CRUCEROJURIDICA, ingreso.getRecuperacion().getTipo())) {
				  throw new NegocioEJBExeption("CANC_REVERSA_CANCELAR","EL INGRESO NO PUEDE ESTAR ASOCIADO A UNA RECUPERACION DE JURIDICO O CRUCERO");
			  }              
		  }              
	  }

	  @Override
	  public void registrarMovtoManual(Long cuentaId, BigDecimal importe, String causaMovimiento) {
		  MovimientoManual movimiento = new MovimientoManual();
		  String codigoUsuarioCreacion = usuarioService.getUsuarioActual().getNombreUsuario();
		  ConceptoGuia cuenta =  this.entidadService.findById(ConceptoGuia.class, cuentaId);
		  movimiento.setCausaMovimiento(causaMovimiento);
		  movimiento.setFechaCreacion(new Date());
		  movimiento.setCuenta(cuenta);
		  movimiento.setCodigoUsuarioCreacion(codigoUsuarioCreacion);
		  movimiento.agregar(importe, codigoUsuarioCreacion, new Date());

		  entidadService.save(movimiento);
	  }

	  @Override
	  public void eliminarMovtoManual(Long movimientoId){
		  MovimientoManual movimiento = entidadService.findById(MovimientoManual.class, movimientoId);
		  movimiento.setActivo(Boolean.FALSE);
		  entidadService.save(movimiento);
	  }

	  @Override
	  public List<MovimientoManualDTO> buscarMovimientosManuales(MovimientoManualDTO filtro, String movimientosConcat, Boolean esConsulta) {
		  return ingresoDao.buscarMovimientosManuales(filtro, movimientosConcat, esConsulta);
	  }

	  

	  public MovimientoManualDet insertaMovimientoManual(Long idMovto, BigDecimal importe, Long aplicacionMovtoId, TIPO_MOVIMIENTO_ACREEDOR tipoMovimiento) {
		  MovimientoManual movimiento = this.entidadService.findById(MovimientoManual.class, idMovto);
		  MovimientoManualDet movimientoDet = new MovimientoManualDet();
		  movimientoDet.setMovimientoManual(movimiento);
		  movimientoDet.setImporte(importe);
		  movimientoDet.setFechaCreacion(new Date());
		  movimientoDet.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());

		  if (TIPO_MOVIMIENTO_ACREEDOR.CARGO.equals(tipoMovimiento)) {
			  movimientoDet.setImporte(importe.negate());
		  }
		  
		  movimientoDet = this.entidadService.save(movimientoDet);

		  return movimientoDet;
	  }

        
    	
    	/***
    	 * Obtiene el saldo final de una cuenta acreedora
    	 * @param cuentaId
    	 */
    	@Override
    	public Double obtenerSaldoCuentaAcreedora(Long cuentaId){
    		Double saldo = Double.valueOf(0);
    		int i = 0;
    		Map<String,Object> params = new LinkedHashMap<String,Object>();
    		params.put("cuenta.id", cuentaId);
    		params.put("estatus", Boolean.TRUE);
    		List<MovimientoCuentaAcreedor> movimientos = this.entidadService.findByProperties(MovimientoCuentaAcreedor.class, params);
    		
    		if(CommonUtils.isNotEmptyList(movimientos)){
    			while(i < movimientos.size()){
    				saldo += movimientos.get(i++).getSaldoFinal().doubleValue();
    			}			
    		}
    		
    		return saldo;
    		
    	}

    	/***
    	 * Aplicar monto a cuenta acreedora
    	 * @param cuentaId
    	 * @param monto
    	 */
    	@Override
    	public String aplicarMontoCuentaAcreedora(IngresoDevolucion ingresoDevolucion) {
    		List<MovimientoAcreedorDTO> lstMovimientoAcreedorDTO = new ArrayList<MovimientoAcreedorDTO>();
    		MovimientoAcreedorDTO filtro =new MovimientoAcreedorDTO ();
    		filtro.setCuentaContable(String.valueOf(ingresoDevolucion.getCuentaAcreedora().getId()));
    		lstMovimientoAcreedorDTO = this.buscarMovimientosAcreedores(filtro, null, false);
    		try{
        		if(CommonUtils.isNotEmptyList(lstMovimientoAcreedorDTO)){
        			Collections.sort(lstMovimientoAcreedorDTO,new Comparator<MovimientoAcreedorDTO>() {
        				@Override
        				public int compare(MovimientoAcreedorDTO object1, MovimientoAcreedorDTO object2) {
        					return object1.getFechaTraspaso().compareTo(object2.getFechaTraspaso());
        				}
        			});
        			String codigoUsuarioCreacion = usuarioService.getUsuarioActual().getNombreUsuario();
        			ConceptoGuia conceptoGuia = obtenerConceptoGuiaPorTipo(ConceptoGuia.TIPO_CONCEPTO_GUIA.DEVOLUCION_CHEQUE);
        			AplicacionCobranza aplicacion = new AplicacionCobranza(AplicacionCobranza.TipoAplicacionCobranza.D, "SOLICITUD DE CHEQUE - CANCELACION POR DEVOLUCION", codigoUsuarioCreacion);

        			AplicacionCobranzaDevolucion aplicacionDevolucion = new AplicacionCobranzaDevolucion(ingresoDevolucion, 
        					                                                                             ingresoDevolucion.getFormaPago().equals(IngresoDevolucion.FormaPago.CHQ)?
        					                                                                            		             AplicacionCobranzaDevolucion.TipoAplicacionDevolucion.C : 
        					                                                                            		             AplicacionCobranzaDevolucion.TipoAplicacionDevolucion.T, 
        					                                                                             codigoUsuarioCreacion, conceptoGuia);
        			aplicacion.agregar(aplicacionDevolucion);
        			
        			aplicacion.agregar(ingresoDevolucion.getCuentaAcreedora().getId().toString(),
        								AplicacionCobranzaCuenta.TipoAplicacionCuenta.A,
        								ingresoDevolucion.getImporteDevolucion(),
        								BigDecimal.ZERO,
        								codigoUsuarioCreacion,
        								"Generación de Cheque por Cancelación",
        								ingresoDevolucion.getCuentaAcreedora(), AplicacionCobranzaCuenta.TipoOperacionCobranza.C);

        			this.entidadService.save(aplicacion);

        			aplicarMontoCuentaAcreedora(lstMovimientoAcreedorDTO, ingresoDevolucion.getImporteDevolucion(), aplicacion.getId());
        			
        			if(ingresoDevolucion.getMontoRestante().compareTo(BigDecimal.ZERO) == 1){
            			aplicacion.agregar(ingresoDevolucion.getCuentaAcreedoraRestante().getId().toString(),
    							AplicacionCobranzaCuenta.TipoAplicacionCuenta.A,
    							ingresoDevolucion.getMontoRestante(),
    							BigDecimal.ZERO,
    							codigoUsuarioCreacion,
    							"Devolución Restante Cheque Cuenta Acreedora",
    							ingresoDevolucion.getCuentaAcreedoraRestante(), AplicacionCobranzaCuenta.TipoOperacionCobranza.A);    				
            			this.insertarMovimientoAcreedor(aplicacion.getId(), aplicacion.getId(), aplicacion.getId(), ingresoDevolucion.getCuentaAcreedoraRestante().getId(), ingresoDevolucion.getMontoRestante(), TIPO_MOVIMIENTO_ACREEDOR.ORIGEN, null);
        			}

        		}    			
    		}catch(Exception e){
    			return "OCURRIO UN ERROR. NO FUE POSIBLE GENERAR LA CONTABILIDAD.";
    		}
    		
    		return null;
    	}
    	
    	private void aplicarMontoCuentaAcreedora(List<MovimientoAcreedorDTO> lstMovimientoAcreedorDTO, BigDecimal monto, Long aplicacionId){
    		if(CommonUtils.isNotEmptyList(lstMovimientoAcreedorDTO)){
    			BigDecimal importeMovimiento = lstMovimientoAcreedorDTO.get(0).getImporte().compareTo(monto) == -1 ? lstMovimientoAcreedorDTO.get(0).getImporte() : monto;
    			cargoMovimientoAcreedor(lstMovimientoAcreedorDTO.get(0).getMovimientoId(),  importeMovimiento, aplicacionId);
    			
    			BigDecimal montoRestante = monto.subtract(lstMovimientoAcreedorDTO.get(0).getImporte());
    			if(montoRestante.compareTo(BigDecimal.ZERO) == 1){
    				lstMovimientoAcreedorDTO.remove(0);
    				aplicarMontoCuentaAcreedora(lstMovimientoAcreedorDTO, montoRestante, aplicacionId);
    			}
    		}

    	}
    	
        private ConceptoGuia obtenerConceptoGuiaPorTipo(ConceptoGuia.TIPO_CONCEPTO_GUIA tipo){
            List<ConceptoGuia> conceptos =  entidadService.findByProperty(ConceptoGuia.class, "tipo", tipo);
            if(conceptos != null && conceptos.size() > 0){
                  return conceptos.get(0);
            }
            return null;
      }
        
      @Override
      public String reversaMontoCuentaAcreedora(IngresoDevolucion ingresoDevolucion){
    	  String codigoUsuarioCreacion = usuarioService.getUsuarioActual().getNombreUsuario();
		  AplicacionCobranza aplicacion = new AplicacionCobranza(AplicacionCobranza.TipoAplicacionCobranza.C, "SOLICITUD DE CHEQUE - CANCELACION SOLICITUD CHEQUE", codigoUsuarioCreacion);
		  ConceptoGuia conceptoGuia = obtenerConceptoGuiaPorTipo(ConceptoGuia.TIPO_CONCEPTO_GUIA.DEVOLUCION_CHEQUE);
		  try{
			  AplicacionCobranzaDevolucion aplicacionDevolucion = new AplicacionCobranzaDevolucion(ingresoDevolucion, 
					  																			   ingresoDevolucion.getFormaPago().equals(IngresoDevolucion.FormaPago.CHQ)?
					  																					   	AplicacionCobranzaDevolucion.TipoAplicacionDevolucion.C : 
					  																					   	AplicacionCobranzaDevolucion.TipoAplicacionDevolucion.T, 
					  																		       codigoUsuarioCreacion, conceptoGuia,
					  																		       AplicacionCobranzaDevolucion.TipoOperacionDevolucion.C);
		      aplicacion.agregar(aplicacionDevolucion);
			  aplicacion.agregar(ingresoDevolucion.getCuentaAcreedora().getId().toString(),
						AplicacionCobranzaCuenta.TipoAplicacionCuenta.A,
						ingresoDevolucion.getImporteDevolucion(),
						BigDecimal.ZERO,
						codigoUsuarioCreacion,
						"Cancelación Generación de Cheque por Cancelación",
						ingresoDevolucion.getCuentaAcreedora(), AplicacionCobranzaCuenta.TipoOperacionCobranza.A);			 																		   
			  
			  if(ingresoDevolucion.getMontoRestante().compareTo(BigDecimal.ZERO) == 1){
	  			aplicacion.agregar(ingresoDevolucion.getCuentaAcreedoraRestante().getId().toString(),
						AplicacionCobranzaCuenta.TipoAplicacionCuenta.A,
						ingresoDevolucion.getMontoRestante(),
						BigDecimal.ZERO,
						codigoUsuarioCreacion,
						"Cancelación Devolución Restante Cheque Cuenta Acreedora",
						ingresoDevolucion.getCuentaAcreedoraRestante(), AplicacionCobranzaCuenta.TipoOperacionCobranza.C);
			  }
			  
			  this.entidadService.save(aplicacion);
			  
			  //Aplicar montos
			  if(ingresoDevolucion.getMontoRestante().compareTo(BigDecimal.ZERO) == 1){
				  List<MovimientoAcreedorDTO> lstMovimientoAcreedorDTO = new ArrayList<MovimientoAcreedorDTO>();
				  MovimientoAcreedorDTO filtro =new MovimientoAcreedorDTO ();
				  filtro.setCuentaContable(String.valueOf(ingresoDevolucion.getCuentaAcreedoraRestante().getId()));
				  lstMovimientoAcreedorDTO = this.buscarMovimientosAcreedores(filtro, null, false);
	
		      		if(CommonUtils.isNotEmptyList(lstMovimientoAcreedorDTO)){
		    			Collections.sort(lstMovimientoAcreedorDTO,new Comparator<MovimientoAcreedorDTO>() {
		    				@Override
		    				public int compare(MovimientoAcreedorDTO object1, MovimientoAcreedorDTO object2) {
		    					return object1.getFechaTraspaso().compareTo(object2.getFechaTraspaso());
		    				}
		    			});
		  			  
		    			aplicarMontoCuentaAcreedora(lstMovimientoAcreedorDTO, ingresoDevolucion.getMontoRestante(), aplicacion.getId());
		      		}
				  
			  }
			  
	   		 this.insertarMovimientoAcreedor(aplicacion.getId(), aplicacion.getId(), aplicacion.getId(), ingresoDevolucion.getCuentaAcreedora().getId(), ingresoDevolucion.getImporteDevolucion(), TIPO_MOVIMIENTO_ACREEDOR.ORIGEN, null);
		  }catch(Exception e){
  			return "OCURRIO UN ERROR. NO FUE POSIBLE GENERAR LA REVERSA DE LA CONTABILIDAD.";
  		}
    	 return null;
      }
      
      public Ingreso obtenerIngresoActivo(Long recuperacionId) {
    	  Ingreso ingresoActivo = null;
    	  Map<String, Object> params = new HashMap<String, Object>();
    	  
    	  params.put("recuperacion.id", recuperacionId);
    	  
    	  List<Ingreso> ingresos = entidadService.findByPropertiesWithOrder(Ingreso.class, params, "fechaCreacion desc");
    	  
    	  for(Ingreso ingreso : CollectionUtils.emptyIfNull(ingresos)){
  			if(ingreso.getEstatus().equals(Ingreso.EstatusIngreso.PENDIENTE.getValue())
  					|| ingreso.getEstatus().equals(Ingreso.EstatusIngreso.APLICADO.getValue())){
  				ingresoActivo = ingreso;
  				break;
  			}
  		}
    	  
    	  return ingresoActivo;
      }
      
      
      private void notificarIngresoFacturaCancelada(Ingreso ingreso) {
    	  
    	  try {
    		  //Ingreso _ingreso = this.entidadService.findById(Ingreso.class, new Long("4841"));
    		  //this.notificarIngresoFacturaCancelada(_ingreso);
    		  HashMap<String, Serializable> notificacion = new HashMap<String, Serializable>();
	    	  Map<String, Object> params = new HashMap<String, Object>();
	    	  params.put("ingreso", ingreso);
	    	  
	    	  List<EmisionFactura> lEmision = this.entidadService.findByProperties(EmisionFactura.class, params);
	    	  
	    	  for(EmisionFactura emision : CollectionUtils.emptyIfNull(lEmision)){ 
	    		  
	    		  if( emision.getEstatus().equals(EmisionFactura.EstatusFactura.FACTURADA.getValue()) ){
	    			  
	    			  notificacion.put("ingreso", ingreso.getNumero());
	    			  notificacion.put("factura", emision.getFolioFactura());
	    			  notificacion.put("razonSocial", emision.getNombreRazonSocial() );
	    			  
	    			  this.notificacionService.enviarNotificacion("CANCELACION_INGRESO_FACTURA", notificacion);
	    		  }
	    	  }
    	  }catch(Exception e) {
    		  log.error("Error IngresoServiceImp notificarIngresoFacturaCancelada "+e);
    	  }
      }
      
      /***
       * VALIDAD SI LA SOLICITUD DE CANCELACION TIENE UNA RECUPERACION DE DEDUCIBLE, SI ESTA TIENE COMO ORIGEN 'LIQ' 
       * RETORNA TRUE Y SI PUEDE CANCELAR, SI TIENE COMO ORIGEN 'PASE' FALSE Y SI PUEDE CANCELAR
       * @param lIngresos
       * @param tipoCancelacion  CANCELACION_POR_DEVOLUCION = 1 | CANCELACION_CUENTA_ACREEDORA = 2
       * @return TRUE: PUEDE CANCELAR | FALSE: NO PUEDE CANCELAR
       */
      @Override
      public boolean isOrigenDedudicleEsLiq(List<Ingreso> lIngresos , int tipoCancelacion) {

    	  	Boolean isCancelaIngreso = true;
    	  	
    	  	try {
    	  	
	    	  	if( !lIngresos.isEmpty() ) {
	    	  		
	    	  		//OBTENER LA RECUPERACION DEL INGRESO
	    	  		Recuperacion recuperacion = lIngresos.get(0).getRecuperacion();

	    	  		if( recuperacion != null && recuperacion.getTipo().equals(TipoRecuperacion.DEDUCIBLE.getValue()) ) {
	    	  			//VALIDAR ORIGEN DE LA RECUPERACION
	    	  			RecuperacionDeducible recuperacionDeducible = (RecuperacionDeducible) recuperacion;
	    	  			if( recuperacionDeducible.getOrigenDeducible().equals( RecuperacionDeducible.Origen.PASE.getValue() ) ) {
	    	  				isCancelaIngreso = false; 
	    	  			}
	    	  		}
	    	  	}
    	  	
    	  	}catch(Exception e) {
    	  		log.error("ERROR IngresoServiceImp.isPuedeCancelarIngreso ",e);
    	  	}
    	  	
    	  	return isCancelaIngreso;
    	  
      }
      
      /**
       * REGRESA EL ORIGEN DEL DEDUCIBLE, SI EL INGRESO TIENE UNA RECUPERACION DE DEDUCIBLE
       * @param keyIngreso
       * @return
       */
      @Override
      public String obtenerOrigenDedudicleRecuperacionIngreso(Long keyIngreso) {

  	  	String origenDeducible = "NA";
  	    List<Ingreso> lIngresos  = null; 
  	  	
  	  	try {
  	  			Ingreso ingreso = this.entidadService.findById(Ingreso.class, keyIngreso );
  	  			
  	  		    lIngresos = new ArrayList<Ingreso>();
  	  		    lIngresos.add(ingreso);
  	  			
	    	  	if( ingreso != null ) {
	    	  		if( !this.isOrigenDedudicleEsLiq(lIngresos, CANCELACION_CUENTA_VALIDACION_ACTION) ) {
	    	  			origenDeducible = "PASE";
	    	  		}else {
	    	  			origenDeducible = "LIQ";
	    	  		}
	    	  	}
  	  	
  	  	}catch(Exception e) {
  	  		log.error("ERROR IngresoServiceImp.isOrigenDedudicleEsLiq ",e);
  	  	}
  	  	
  	  	return origenDeducible;
  	  
      }
      
      private boolean obtenerOrigenDedudicleRecuperacionIngreso(List<Ingreso> lIngresos) {
    	  
    	  if( !lIngresos.isEmpty() ) {
    		  
    		  for(Ingreso ingreso: lIngresos){
    			  
    			  if( this.obtenerOrigenDedudicleRecuperacionIngreso(ingreso.getId()).equals(RecuperacionDeducible.Origen.PASE.getValue()) ) {
    				  return true;
    			  }	
    		  }
    	  }
    	  return false;
      }
      
      
      /**
       * VALIDA SI EL INGRESO A CANCELAR TIENE ORDENES DE COMPRA CON PERDIDA TOTAL
       * @param lIngresos
       * @param tipoCancelacion
       * @return
       */
      private boolean isPerdidaTotalCancelarIngreso(List<Ingreso> lIngresos , int tipoCancelacion) {
    	  
    	  Boolean isPerdidaTotal = false;
    	  
    	  try {
    		  
    		  List<OrdenPagoSiniestro> lOrdenesPago = null;
    		  if( !lIngresos.isEmpty() ) {
    			  
    			  // SI EL ORIGEN NO ES PASE Y RECUPERACION DE DEDUCIBLE PUEDE CANCELAR
	    		  if( !this.obtenerOrigenDedudicleRecuperacionIngreso(lIngresos) ) {
	    		  
	    			  for(Ingreso ingreso: lIngresos){
	    				  lOrdenesPago  = this.obtenerOrdenesPago(ingreso.getRecuperacion());
					  	  // # SI EXISTE AL MENOS UNA ORDEN DE PAGO CON PERDIDA TOTAL MANDAR ERROR
					  	  if( this.isPerdidaTotal(lOrdenesPago) ){
					  				isPerdidaTotal = true;
					  	  }   				 
					  }
			       }
    		  }
	    	  
    	  }catch(Exception e) {
    		  log.error("ERROR IngresoServiceImp.isPerdidaTotalCancelarIngreso",e);
    	  }
    	  
    	  return isPerdidaTotal;
      }
      
    public void initialize() {
  		String timerInfo = "TimerIngreso";
  		cancelarTemporizador(timerInfo);
  		iniciarTemporizador();
  	}
  	
  	public void iniciarTemporizador() {
  		if(sistemaContext.getTimerActivo()) {
  			ScheduleExpression expression = new ScheduleExpression();
  			try {  				
  				expression.minute(0);
  				expression.hour(2);
  				expression.dayOfWeek("*");
  				
  				timerService.createCalendarTimer(expression, new TimerConfig("TimerIngreso", false));
  				
  				log.info("Tarea TimerIngreso configurado");
  			} catch (Exception e) {
  				log.error("Error al configurar Timer:" + e.getMessage(), e);
  			}
  		}
  	}
  	
  	public void cancelarTemporizador(String timerInfo) {
  		log.info("Cancelar Tarea TimerIngreso");
  		try {
  			if (timerService.getTimers() != null) {
  				for (Timer timer : timerService.getTimers())
  					if (timer.getInfo() != null
  							|| timer.getInfo().equals(timerInfo))
  						timer.cancel();
  			}
  		} catch (Exception e) {
  			log.error("Error al detener TimerIngreso:" + e.getMessage(), e);
  		}
  	}
  	
  	@Timeout
  	public void execute() {
  		aplicarIngresoAutomatico();
  	}
}
