package mx.com.afirme.midas2.service.impl.sapamis.catalogos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.sapamis.catalogos.RelSistemaOperacion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.catalogos.RelSistemaOperacionService;

/*******************************************************************************
 * Nombre Interface: 	RelSistemaOperacionServiceImpl.
 * 
 * Descripcion: 		Contiene la logica del manejo de los Servicios para el 
 * 						objeto RelSistemaOperacion.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class RelSistemaOperacionServiceImpl implements RelSistemaOperacionService{
	private static final long serialVersionUID = 1L;
	
	@EJB private EntidadService entidadService;

	@Override
	public List<RelSistemaOperacion> findByStatusAndOperation(boolean estatusReg, long modulo) {
		Map<String,Object> parametros = new HashMap<String,Object>();
		parametros.put("estatus", estatusReg?0:1);
		parametros.put("modulo", modulo);
		return entidadService.findByProperties(RelSistemaOperacion.class, parametros);
	}
}