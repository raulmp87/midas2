package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fuerzaventa.GrupoAgenteDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoAgente;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.fuerzaventa.GrupoAgenteService;

/**
 * 
 * @author Efren Rodriguez
 *
 */
@Stateless
public class GrupoAgenteServiceImpl implements GrupoAgenteService {
	@EJB
	private GrupoAgenteDao dao;
	
	/* --- METHODS --- */
	public GrupoAgente loadById(GrupoAgente grupoAgenteDTO) {
		return dao.loadById(grupoAgenteDTO);
	}
	public List<GrupoAgente> findByFilters(GrupoAgente filtro) {
		return dao.findByFilters(filtro);
	}
	public boolean deleteGrupo(GrupoAgente filtro) {
		return dao.deleteGrupo(filtro);
	}
	public List<Object[]> obtenerAgentesAsociadosEnGrupo(GrupoAgente tipoGrupoAgenteDTO) {
		return dao.obtenerAgentesAsociadosEnGrupo(tipoGrupoAgenteDTO);
	}
	public void guardarAgentesAsociadosEnGrupo(Long idGrupo, String idsAgentes) {
		this.dao.guardarAgentesAsociadosEnGrupo(idGrupo, idsAgentes);
	}
	public List<Object[]> filtrarAgentesDeGrupo(Long idGrupo, String clave, String nombre, String gerencia) {
		return dao.filtrarAgentesDeGrupo(idGrupo,clave,nombre,gerencia);
	}
	public List<AgenteView> filtrarAgente(Agente filtroAgente) {
		return dao.filtrarAgente(filtroAgente);
	}
	
}
