package mx.com.afirme.midas2.service.impl.suscripcion.endoso;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoId;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoDTO;
import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.interfaz.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerService;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.catalogos.condicionesespeciales.CondicionEspecialDao;
import mx.com.afirme.midas2.dao.suscripcion.impresion.EdicionImpresionDAO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.Cotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento.BitemporalDocAnexoCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.BitemporalTexAdicionalCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.TexAdicionalCotContinuity;
import mx.com.afirme.midas2.domain.catalogos.DomicilioImpresion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.migracion.MigEndososValidosEd;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionInciso;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionIncisoCobertura;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaInciso;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaPoliza;
import mx.com.afirme.midas2.dto.impresiones.DatosCoberturasDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosIncisoDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosPolizaDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.seccion.dato.ConfiguracionDatoIncisoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.inciso.IncisoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;
import mx.com.afirme.midas2.service.folioReexpedibleService.FolioReexpedibleService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.emision.ImpresionesServiceImpl;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService.FoliosPermitidosImpresion;
import mx.com.afirme.midas2.service.suscripcion.endoso.ImpresionBitemporalService;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.js.util.StringUtil;

@Stateless
public class ImpresionBitemporalServiceImpl implements
		ImpresionBitemporalService {
	
	private static final Logger LOG = Logger
			.getLogger(ImpresionBitemporalServiceImpl.class);

	private MonedaDTO moneda;
	private FormaPagoDTO formaPago;
	private DomicilioImpresion domicilioCliente;
	private ClienteGenericoDTO cliente;
	private List<BitemporalInciso> bitemporalIncisoList = new ArrayList<BitemporalInciso>();
	private List<CoberturaCotizacionDTO> coberturasCotizacionList = null;
	private List<CatalogoValorFijoDTO> posiblesDeducibles = null;
	
	private Map<String,String> identificadorAct = null;
	
	@EJB
	private TipoServicioVehiculoFacadeRemote tipoServicioVehiculoFacadeRemote;
	@EJB
	private IncisoViewService incisoViewService;
	@EJB
	private ListadoIncisosDinamicoService listadoIncisosDinamicoService;
	protected EntidadService entidadService;
	protected DomicilioFacadeRemote domicilioFacadeRemote;
	protected ClienteFacadeRemote clienteFacadeRemote;
	protected CalculoService calculoService;
	protected EntidadBitemporalService entidadBitemporalService;
	protected IncisoBitemporalService incisosBitemporalService;
	protected ConfiguracionDatoIncisoBitemporalService configDatoIncisoBitemporalService;
	@EJB
	protected AgenteMidasService agenteMidasService;
	protected EntidadContinuityService entidadContinuityService;
	protected SistemaContext sistemaContext;
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	
	@EJB
	private EndosoFacadeRemote endosoInterfazFacade;
	
	@EJB
	private CondicionEspecialService condicionEspecialService;
	
	@EJB
	protected CondicionEspecialDao condicionEspecialDao;
	
	@EJB
	protected EntidadContinuityDao entidadContinuityDao;
	
	@EJB
	protected CondicionEspecialBitemporalService condicionEspecialBitemporalService;
	
	@EJB
	private FileManagerService fileManagerService;
	
	@EJB
	private FolioReexpedibleService folioReexpedibleService;
	
	@EJB
	protected EdicionImpresionDAO edicionImpresionDao;
	
	@EJB
	private ListadoService listadoService;
	
	@Override
	public DatosCaratulaPoliza llenarReferenciasBancariasPolizaDTO(BigDecimal idToPoliza,
			DateTime validOn, DateTime recordFrom, Short claveTipoEndoso,
			Boolean esSituacionActual) {
		DatosCaratulaPoliza datosCaratulaPoliza = new DatosCaratulaPoliza();
		
		// Se obtiene la informacion de la poliza y el ID de su cotizacion asociada
		PolizaDTO polizaDTO = new PolizaDTO();
		polizaDTO = entidadService.findById(PolizaDTO.class, idToPoliza);
		
		// Se obtiene el objeto que representa o contienen la informacion de la cotizacion
		BigDecimal idToCotizacion = polizaDTO.getCotizacionDTO().getIdToCotizacion();
		BitemporalCotizacion bitempCotizacion = null;
		CotizacionContinuity cotizacionContinuity2 = entidadContinuityService.findContinuityByBusinessKey(CotizacionContinuity.class,
						CotizacionContinuity.BUSINESS_KEY_NAME, idToCotizacion);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity.id", cotizacionContinuity2.getId());
		List<BitemporalCotizacion> bitempCotizacionList =  (List<BitemporalCotizacion>) entidadBitemporalService.listarFiltrado(BitemporalCotizacion.class, params, validOn, recordFrom, false);
		if(bitempCotizacionList != null && !bitempCotizacionList.isEmpty()){
			bitempCotizacion = bitempCotizacionList.get(0);
		}
		
		if (bitempCotizacion == null && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION) {
			Long id = cotizacionContinuity2.getId();
			Collection<BitemporalCotizacion> collectionBitemporalCotizacion =
				entidadBitemporalService.obtenerCancelados(BitemporalCotizacion.class, id, validOn,	recordFrom);
			if (!collectionBitemporalCotizacion.isEmpty()) {
				bitempCotizacion = collectionBitemporalCotizacion.iterator().next();
			}
		}
		
		Cotizacion cotizacionBitemporalValue = bitempCotizacion.getValue();
		
	formaPago = cotizacionBitemporalValue.getFormaPago();
		moneda = cotizacionBitemporalValue.getMoneda();
		BigDecimal idPersonaContratante = new BigDecimal(cotizacionBitemporalValue.getPersonaContratanteId());
		BigDecimal idDomicilioContratante = cotizacionBitemporalValue.getDomicilioContratanteId();
		getDatosCliente(idPersonaContratante, idDomicilioContratante);
		
		DatosPolizaDTO datosPolizaDTO = new DatosPolizaDTO();
		datosPolizaDTO.setNumeroPoliza(polizaDTO.getNumeroPolizaFormateada());
		datosPolizaDTO.setIdCentroEmisor(polizaDTO.getIdCentroEmisor());
		datosPolizaDTO.setFechaInicioVigencia(cotizacionBitemporalValue.getFechaInicioVigencia());
		datosPolizaDTO.setFechaFinVigencia(cotizacionBitemporalValue.getFechaFinVigencia());
		datosPolizaDTO.setIdToPersonaContratante(idPersonaContratante);
		datosPolizaDTO.setDescripcionMoneda(moneda.getDescripcion());
		if (formaPago != null) {
			datosPolizaDTO.setDescripcionFormaPago(formaPago.getDescripcion());
		}
		datosPolizaDTO.setNombreContratante(cotizacionBitemporalValue.getNombreContratante().toUpperCase());

		if (domicilioCliente != null) {
			StringBuilder domicilio = new StringBuilder("");
			domicilio.append(UtileriasWeb.getString(domicilioCliente.getCalleNumero()));
			domicilio.append(UtileriasWeb.getString(domicilioCliente.getNombreColonia()));
			domicilio.append(UtileriasWeb.getString(domicilioCliente.getEstado()));
			domicilio.append(!StringUtil.isEmpty(domicilioCliente.getCiudad()) 
					? ", " + domicilioCliente.getCiudad() : "");
			domicilio.append(!StringUtil.isEmpty(domicilioCliente.getCodigoPostal())
					? ", \nC.P. " + domicilioCliente.getCodigoPostal() : "");

			datosPolizaDTO.setTxDomicilio(domicilio.toString());
			datosPolizaDTO.setCalleYNumero(domicilioCliente.getCalleNumero());
			datosPolizaDTO.setColonia(domicilioCliente.getNombreColonia());
			datosPolizaDTO.setCiudad(domicilioCliente.getCiudad());
			datosPolizaDTO.setEstado(domicilioCliente.getEstado());
			datosPolizaDTO.setCodigoPostal(domicilioCliente.getCodigoPostal());
		}
		
		if(cliente != null){			
			datosPolizaDTO.setRfcContratante(UtileriasWeb.getString(cliente.getCodigoRFC()));
		}
		
		datosPolizaDTO.setTitulo(getTituloImpresion(polizaDTO.getCotizacionDTO().getClaveEstatus(), 
				cotizacionBitemporalValue.getTipoPoliza().getDescripcion()));
		
		try {
			setObservacionesCaratulaPoliza(bitempCotizacion, datosPolizaDTO, validOn, recordFrom, claveTipoEndoso);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Error setObservacionesCaratulaPoliza => ", Level.INFO, e);
		}
		
		datosPolizaDTO.setObservaciones(UtileriasWeb.getString(datosPolizaDTO.getObservaciones()));
		
		String identificador = this.getIdentificadorCaratulaPoliza(bitempCotizacion).get("identificador");
		datosPolizaDTO.setIdentificador(identificador);
		datosPolizaDTO.setClavePolizaSeycos(polizaDTO.getClavePolizaSeycos());
		datosPolizaDTO.setNumeroFolio(getFolioPoliza(polizaDTO));
		datosCaratulaPoliza.setDatosPolizaDTO(datosPolizaDTO);
		datosCaratulaPoliza
		.setDatasourceReferenciasBancarias(listadoIncisosDinamicoService
				.getReferenciasBancarias(idToPoliza));
		datosCaratulaPoliza.setPolizaDTO(polizaDTO);
		
		return datosCaratulaPoliza;
	}
	
	
	@Override
	public DatosCaratulaPoliza llenarPolizaDTO(BigDecimal idToPoliza,
			DateTime validOn, DateTime recordFrom, Short claveTipoEndoso,
			Boolean esSituacionActual) {
		DatosCaratulaPoliza datosCaratulaPoliza = new DatosCaratulaPoliza();
		
		// Se obtiene la informacion de la poliza y el ID de su cotizacion asociada
		PolizaDTO polizaDTO = new PolizaDTO();
		polizaDTO = entidadService.findById(PolizaDTO.class, idToPoliza);
		
		// Se obtiene el objeto que representa o contienen la informacion de la cotizacion
		BigDecimal idToCotizacion = polizaDTO.getCotizacionDTO().getIdToCotizacion();
		BitemporalCotizacion bitempCotizacion = null;
		CotizacionContinuity cotizacionContinuity2 = entidadContinuityService.findContinuityByBusinessKey(CotizacionContinuity.class,
						CotizacionContinuity.BUSINESS_KEY_NAME, idToCotizacion);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity.id", cotizacionContinuity2.getId());
		List<BitemporalCotizacion> bitempCotizacionList =  (List<BitemporalCotizacion>) entidadBitemporalService.listarFiltrado(BitemporalCotizacion.class, params, validOn, recordFrom, false);
		if(bitempCotizacionList != null && !bitempCotizacionList.isEmpty()){
			bitempCotizacion = bitempCotizacionList.get(0);
		}
		
		if (bitempCotizacion == null && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION) {
			Long id = cotizacionContinuity2.getId();
			Collection<BitemporalCotizacion> collectionBitemporalCotizacion =
				entidadBitemporalService.obtenerCancelados(BitemporalCotizacion.class, id, validOn,	recordFrom);
			if (!collectionBitemporalCotizacion.isEmpty()) {
				bitempCotizacion = collectionBitemporalCotizacion.iterator().next();
			}
		}
		
		Cotizacion cotizacionBitemporalValue = bitempCotizacion.getValue();
		
		formaPago = cotizacionBitemporalValue.getFormaPago();
		moneda = cotizacionBitemporalValue.getMoneda();

		BigDecimal idPersonaContratante = new BigDecimal(cotizacionBitemporalValue.getPersonaContratanteId());
		BigDecimal idDomicilioContratante = cotizacionBitemporalValue.getDomicilioContratanteId();
		getDatosCliente(idPersonaContratante, idDomicilioContratante);
		
		DatosPolizaDTO datosPolizaDTO = new DatosPolizaDTO();
		datosPolizaDTO.setNumeroPoliza(polizaDTO.getNumeroPolizaFormateada());
		Short numeroEndoso = getNumeroEndoso(esSituacionActual, polizaDTO, recordFrom);

		ResumenCostosDTO resumenCostosDTO = calculoService.resumenCostosEmitidos(idToPoliza, numeroEndoso, null);

		datosPolizaDTO.setIdCentroEmisor(polizaDTO.getIdCentroEmisor());
		datosPolizaDTO.setFechaInicioVigencia(cotizacionBitemporalValue.getFechaInicioVigencia());
		datosPolizaDTO.setFechaFinVigencia(cotizacionBitemporalValue.getFechaFinVigencia());
		datosPolizaDTO.setPrimaNetaCotizacion(resumenCostosDTO.getPrimaNetaCoberturas());
		datosPolizaDTO.setDerechosPoliza(resumenCostosDTO.getDerechos());
		datosPolizaDTO.setDescuento(resumenCostosDTO.getDescuento());
		datosPolizaDTO.setPrimaNetaTotal(resumenCostosDTO.getPrimaTotal());
		datosPolizaDTO.setTotalPrimas(resumenCostosDTO.getTotalPrimas());
		datosPolizaDTO.setMontoRecargoPagoFraccionado(resumenCostosDTO.getRecargo());
		datosPolizaDTO.setMontoIVA(resumenCostosDTO.getIva());
		datosPolizaDTO.setIdToPersonaContratante(idPersonaContratante);
		datosPolizaDTO.setDescripcionMoneda(moneda.getDescripcion());
		if (formaPago != null) {
			datosPolizaDTO.setDescripcionFormaPago(formaPago.getDescripcion());
		}
		datosPolizaDTO.setNombreContratante(cotizacionBitemporalValue.getNombreContratante().toUpperCase());

		if (domicilioCliente != null) {
			StringBuilder domicilio = new StringBuilder("");
			domicilio.append(UtileriasWeb.getString(domicilioCliente.getCalleNumero()));
			domicilio.append(UtileriasWeb.getString(domicilioCliente.getNombreColonia()));
			domicilio.append(UtileriasWeb.getString(domicilioCliente.getEstado()));
			domicilio.append(!StringUtil.isEmpty(domicilioCliente.getCiudad()) 
					? ", " + domicilioCliente.getCiudad() : "");
			domicilio.append(!StringUtil.isEmpty(domicilioCliente.getCodigoPostal())
					? ", \nC.P. " + domicilioCliente.getCodigoPostal() : "");

			datosPolizaDTO.setTxDomicilio(domicilio.toString());
			datosPolizaDTO.setCalleYNumero(domicilioCliente.getCalleNumero());
			datosPolizaDTO.setColonia(domicilioCliente.getNombreColonia());
			datosPolizaDTO.setCiudad(domicilioCliente.getCiudad());
			datosPolizaDTO.setEstado(domicilioCliente.getEstado());
			datosPolizaDTO.setCodigoPostal(domicilioCliente.getCodigoPostal());
		}
		
		if(cliente != null){			
			datosPolizaDTO.setRfcContratante(UtileriasWeb.getString(cliente.getCodigoRFC()));
		}
		
		datosPolizaDTO.setTitulo(getTituloImpresion(polizaDTO.getCotizacionDTO().getClaveEstatus(), 
				cotizacionBitemporalValue.getTipoPoliza().getDescripcion()));
		
		try {
			setObservacionesCaratulaPoliza(bitempCotizacion, datosPolizaDTO, validOn, recordFrom, claveTipoEndoso);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Error setObservacionesCaratulaPoliza => ", Level.INFO, e);
		}
		
		datosPolizaDTO.setObservaciones(UtileriasWeb.getString(datosPolizaDTO.getObservaciones()));
		
		String identificador = this.getIdentificadorCaratulaPoliza(bitempCotizacion).get("identificador");
		datosPolizaDTO.setIdentificador(identificador);
		datosPolizaDTO.setClavePolizaSeycos(polizaDTO.getClavePolizaSeycos());
		datosPolizaDTO.setNumeroFolio(getFolioPoliza(polizaDTO));
		datosCaratulaPoliza.setDatosPolizaDTO(datosPolizaDTO);
		datosCaratulaPoliza
				.setDatasourceLineasNegocio(listadoIncisosDinamicoService
						.getDatosLineaNegocio(idToPoliza, numeroEndoso));
		datosCaratulaPoliza.setPolizaDTO(polizaDTO);
		
		return datosCaratulaPoliza;
	}
	
	private String getTituloImpresion(Short claveEstatusPoliza, String descripcionTipoPoliza) {
		String titulo = "";
		if(claveEstatusPoliza.equals(CotizacionDTO.ESTATUS_COT_EMITIDA)){
			titulo = "P\u00D3LIZA DE SEGUROS PARA " + descripcionTipoPoliza;
		}else{
			titulo = "COTIZACI\u00D3N DE SEGUROS PARA " + descripcionTipoPoliza;
		}
		return titulo;
	}

	private Short getNumeroEndoso(Boolean esSituacionActual, PolizaDTO polizaDTO, DateTime recordFrom) {
		Short numeroEndoso= null;
		if (!esSituacionActual) {
			numeroEndoso = incisoViewService.getNumeroEndoso(polizaDTO.getCotizacionDTO().getIdToCotizacion().longValue(),
					recordFrom.toDate());
			
			if (numeroEndoso == null) {
				numeroEndoso = (short) 0;
			}
		}
		return numeroEndoso;
	}

	private void getDatosCliente(BigDecimal idPersonaContratante, BigDecimal idDomicilioContratante) {
		// Se obtiene el objeto ClienteGenerico asociado al contratante de la poliza
		// para obtener posterirmenta algunos datos generales
		ClienteGenericoDTO filtroClienteByIdContratante = new ClienteGenericoDTO();
		filtroClienteByIdContratante.setIdCliente(idPersonaContratante);
		try {
			cliente = clienteFacadeRemote.getRFCPorCliente(idPersonaContratante);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Error loadByIdNoAddress", Level.INFO, e);
		}
		
		// Se obtiene el domicilio del contratante de la poliza
		try {
			if (idDomicilioContratante != null) {
				domicilioCliente = domicilioFacadeRemote.findDomicilioImpresionById(idDomicilioContratante.longValue());
			} else {
				if (cliente != null) {
					cliente = clienteFacadeRemote.loadById(filtroClienteByIdContratante);
					if (cliente.getIdDomicilioConsulta() != null) {
						domicilioCliente = domicilioFacadeRemote.findDomicilioImpresionById(cliente.getIdDomicilioConsulta());
					}
				}
			}
		} catch (Exception ex) {
			LogDeMidasInterfaz.log("Error domicilioCliente => ", Level.INFO, ex);
		}
	}

	private void setObservacionesCaratulaPoliza(BitemporalCotizacion bitempCotizacion, DatosPolizaDTO datosPolizaDTO, 
			DateTime validOn, DateTime recordFrom, Short claveTipoEndoso){
		LogDeMidasInterfaz.log("Entrando a setObservacionesCaratulaPoliza", Level.FINE, null);		
		 
		 Long cotizacionContinuityId = bitempCotizacion.getContinuity().getId();
		 
		 Collection<TexAdicionalCotContinuity> textosAdicionales = entidadContinuityService.findContinuitiesByParentBusinessKey(
				 TexAdicionalCotContinuity.class, TexAdicionalCotContinuity.PARENT_KEY_NAME, cotizacionContinuityId);
		 
		 List<TexAdicionalCotContinuity> textosAdicionalesList = new ArrayList<TexAdicionalCotContinuity>(1);
		 textosAdicionalesList.addAll(textosAdicionales);
			//Sort
			if(textosAdicionales != null && !textosAdicionales.isEmpty()){
				Collections.sort(textosAdicionalesList, 
						new Comparator<TexAdicionalCotContinuity>() {				
							public int compare(TexAdicionalCotContinuity n1, TexAdicionalCotContinuity n2){
								return n1.getId().compareTo(n2.getId());
							}
						});
			}
			
		 StringBuilder stringBuilder = new StringBuilder();
		 for(TexAdicionalCotContinuity item: textosAdicionalesList){
			 //BitemporalTexAdicionalCot textoAdicional = item.getTexAdicionales().get(validOn, recordFrom);
			 BitemporalTexAdicionalCot textoAdicional = null;
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("continuity.id", item.getId());
				List<BitemporalTexAdicionalCot> bitempList =  (List<BitemporalTexAdicionalCot>) entidadBitemporalService.listarFiltrado(BitemporalTexAdicionalCot.class, params, validOn, recordFrom, false);
				if(bitempList != null && !bitempList.isEmpty()){
					textoAdicional = bitempList.get(0);
				}
			 if(textoAdicional != null){
				 if(textoAdicional.getValue().getDescripcionTexto()!= null && textoAdicional.getValue().getDescripcionTexto().length() > 300){
					 stringBuilder.append(textoAdicional.getValue().getDescripcionTexto().substring(0,300));
					 stringBuilder = new StringBuilder(stringBuilder.substring(0,stringBuilder.lastIndexOf(" ")));
					 stringBuilder.append("...");
				 }else{
					 stringBuilder.append(textoAdicional.getValue().getDescripcionTexto());
				 }
				 stringBuilder.append(System.getProperty("line.separator"));
			 }
		 }

		 if(stringBuilder.toString() != null && !stringBuilder.toString().equals("")){
			 datosPolizaDTO.setObservaciones(stringBuilder.toString().toUpperCase());
		 }else{
			 datosPolizaDTO.setObservaciones("");
		 }

		 LogDeMidasInterfaz.log("Saliendo de setObservacionesCaratulaPoliza", Level.INFO, null);
	}
	
	@Override
	public Map<String,String> getIdentificadorCaratulaPoliza(BitemporalCotizacion bitempCotizacion) {
		Map<String,String> toReturn = new HashMap<String,String>(1);
		//Identificador
		//Centro Emisor - Gerencia - Oficina - Promotoria - Clave Agente
		String identificador = "";
		String datosAgente="";
		if (bitempCotizacion.getValue().getSolicitud().getCodigoAgente() != null) {
			String idAgente = "000";
			try{
				Agente agente = new Agente();
				agente.setId(bitempCotizacion.getValue().getSolicitud().getCodigoAgente().longValue());
				
				try{
					if(identificadorAct != null && agente.getId() == Long.parseLong(identificadorAct.get("idAgente"))){
						return identificadorAct;
					}
				}catch(Exception e){
				}
				
				agente = agenteMidasService.loadByIdImpresiones(agente);
				idAgente = agente.getIdAgente().toString();
				
				datosAgente="Canal de Venta: "+agente.getPromotoria().
						   getEjecutivo().getGerencia().getCentroOperacion().getDescripcion()+
						   " Clave y Nombre del Conducto: "+idAgente+" "+agente.getPersona().getNombreCompleto();

				if (agente.getPromotoria().getEjecutivo().getGerencia()
						.getCentroOperacion().getIdCentroOperacion() != null) {
					identificador += agente.getPromotoria().getEjecutivo()
							.getGerencia().getCentroOperacion()
							.getIdCentroOperacion()
							+ "-";
				} else {
					identificador += "000-";
				}
				if (bitempCotizacion.getValue().getSolicitud().getCodigoEjecutivo() != null) {
					identificador += bitempCotizacion.getValue().getSolicitud().getCodigoEjecutivo() + "-";
				} else {
					identificador += "000-";
				}
				if (bitempCotizacion.getValue().getSolicitud().getIdOficina() != null) {
					identificador += bitempCotizacion.getValue().getSolicitud().getIdOficina() + "-";
				} else {
					identificador += "000-";
				}
				if (agente.getPromotoria().getIdPromotoria() != null) {
					identificador += agente.getPromotoria().getIdPromotoria() + "-";
				} else {
					identificador += "0000-";
				}
				if (bitempCotizacion.getValue().getSolicitud().getCodigoAgente() != null) {
					identificador += idAgente + "";
				} else {
					identificador += "0";
				}

			} catch (Exception e) {
				identificador = "000-000-000-0000-" 
						+ idAgente;
			}
			toReturn.put("idAgente", idAgente);
		}
		toReturn.put("identificador", identificador);
		toReturn.put("datosAgente", datosAgente);
		identificadorAct = toReturn;

		return toReturn;
	}
	
	@Override
	public List<DatosCaratulaInciso> llenarIncisoDTO(
			BitemporalCotizacion bCotizacion, String numeroPoliza, DateTime validOn, DateTime recordFrom,
			PolizaDTO polizaDTO, DatosPolizaDTO datosPolizaDTO,
			Short numeroEndoso,	Short claveTipoEndoso, boolean incluirCondicionesEsp) {
		return this.llenarIncisoDTO(bCotizacion, numeroPoliza, validOn, recordFrom, true, 0, 0, polizaDTO,
				datosPolizaDTO, numeroEndoso, claveTipoEndoso, incluirCondicionesEsp, null);
	}
	
	@Override
	public List<DatosCaratulaInciso> llenarIncisoDTO( BitemporalCotizacion bCotizacion, String numeroPoliza,
			DateTime validOn, DateTime recordFrom,
			int incisoInicial, int incisoFinal, PolizaDTO polizaDTO,
			DatosPolizaDTO datosPolizaDTO, Short numeroEndoso, Short claveTipoEndoso, boolean incluirCondicionesEsp) {
		return this.llenarIncisoDTO(bCotizacion, numeroPoliza, validOn, recordFrom,
				false, incisoInicial,
				incisoFinal, polizaDTO, datosPolizaDTO, numeroEndoso, claveTipoEndoso, incluirCondicionesEsp, null);
	}
	
	@Override
	public List<DatosCaratulaInciso> llenarIncisoDTO(
			BitemporalCotizacion bCotizacion, String numeroPoliza,
			DateTime validOn, DateTime recordFrom, int incisoInicial,
			int incisoFinal, PolizaDTO polizaDTO,
			DatosPolizaDTO datosPolizaDTO, Short numeroEndoso,
			Short claveTipoEndoso, boolean incluirCondicionesEsp,
			EdicionInciso edicionInciso) {
		return this.llenarIncisoDTO(bCotizacion, numeroPoliza, validOn, recordFrom,
				false, incisoInicial,
				incisoFinal, polizaDTO, datosPolizaDTO, numeroEndoso, claveTipoEndoso, incluirCondicionesEsp, edicionInciso);
		
	}

	@Override
	public List<DatosCaratulaInciso> llenarIncisoDTO(
			BitemporalCotizacion bCotizacion, String numeroPoliza,
			DateTime validOn, DateTime recordFrom, PolizaDTO polizaDTO,
			DatosPolizaDTO datosPolizaDTO, Short numeroEndoso,
			Short claveTipoEndoso, boolean incluirCondicionesEsp,
			EdicionInciso edicionInciso) {
		return this.llenarIncisoDTO(bCotizacion, numeroPoliza, validOn, recordFrom, true, 0, 0, polizaDTO,
				datosPolizaDTO, numeroEndoso, claveTipoEndoso, incluirCondicionesEsp, edicionInciso);
	}
	
	@Override
	public Collection<byte[]> imprimirAnexosPoliza(
			BitemporalCotizacion bitemporalCotizacion, DateTime validOn, DateTime knownOn) throws IOException {	

		return this.imprimirAnexosEndoso(bitemporalCotizacion, validOn, knownOn, null);
	}
	
	@Override
	public Collection<byte[]> imprimirAnexosEndoso(
			BitemporalCotizacion bitemporalCotizacion, DateTime validOn, DateTime knownOn, ControlEndosoCot controlEndosoCot) throws IOException {
		
        Collection<byte[]> anexosPdf = new ArrayList<byte[]>();		
		
		Collection<BitemporalDocAnexoCot> lstBiEndosoDocAnexo = new ArrayList<BitemporalDocAnexoCot>();
		Collection<BitemporalDocAnexoCot> listaAnexos = new ArrayList<BitemporalDocAnexoCot>();

		//Se obtienen los registros de Anexos
		
		if(controlEndosoCot != null)//Anexos por Endoso
		{
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("continuity.cotizacionContinuity.id", bitemporalCotizacion.getEntidadContinuity().getId());
			params.put("recordInterval.from", controlEndosoCot.getRecordFrom());
			lstBiEndosoDocAnexo = entidadBitemporalService.listarFiltrado(BitemporalDocAnexoCot.class, params, validOn, knownOn, false);			
		}
		else //Anexos por Poliza
		{
			lstBiEndosoDocAnexo = bitemporalCotizacion.getContinuity()
			.getDocAnexoContinuities().get(validOn, knownOn);			
		}		

		for (BitemporalDocAnexoCot biAnexoCot : lstBiEndosoDocAnexo)
		{			
			if(biAnexoCot.getValue().getClaveSeleccion() != 0) {
				listaAnexos.add(biAnexoCot.getContinuity()
						.getDocAnexos().get(validOn,knownOn));
			}
		}	
		
		Iterator<BitemporalDocAnexoCot> itAnexos = listaAnexos.iterator();
		while(itAnexos.hasNext()) {
			try{
				BitemporalDocAnexoCot anexo = itAnexos.next();
				Integer controlArchivoId = anexo.getContinuity().getControlArchivoId();
				ControlArchivoDTO controlArchivo = entidadService.findById(ControlArchivoDTO.class, new BigDecimal(controlArchivoId));

				String fileName = controlArchivo.obtenerNombreArchivoFisico();

				//Restringe a unicamente archivos PDF
				if(!fileName.contains(".pdf")) {
					continue;				
				}

				//Se obtiene el archivo Anexo
				byte byteArray[] = fileManagerService.downloadFile(fileName,  controlArchivoId.toString());
				anexosPdf.add(byteArray);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			} 
		}	

		return anexosPdf;		
	}
	
	/**
	 * Obtiene el numero de inciso en la poliza de Seycos a partir del inciso de Midas
	 * @param inciso Inciso de Midas
	 * @param clavePolizaSeycos Clave de la poliza de Seycos
	 * @return El numero de inciso de Seycos
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public String obtenerNumeroIncisoSeycos(BitemporalInciso inciso, String clavePolizaSeycos) {

		if (clavePolizaSeycos == null || clavePolizaSeycos.trim().equals("")) return inciso.getValue().getNumeroSecuencia().toString();
																				//getContinuity().getNumero().toString();
		BigDecimal idPoliza = null;
		Short numeroEndosoAltaInciso = 0;
		boolean esMigrado = true;
		DateTime recordFrom = null;
		BitemporalInciso primerIncisoBitemporal = null;
		Long cotizacionId = null;
		String numeroSecuencia = null;
		List<BitemporalInciso> incisos = null;
		List<EndosoDTO> endosos = null;
		Map<String, Object> params = null;
		
		DateTime validFrom = inciso.getValidityInterval().getInterval().getStart();
				
		incisos = inciso.getEntidadContinuity().getBitemporalProperty().getEvolution(validFrom); 
		
		Collections.sort(incisos, new Comparator(){
			public int compare(Object o1, Object o2) {
				return ((BitemporalInciso)o1).getId().compareTo(((BitemporalInciso)o2).getId());
			}
		});
		
		primerIncisoBitemporal = incisos.get(0);
		
		recordFrom = primerIncisoBitemporal.getRecordInterval().getInterval().getStart();
		validFrom = primerIncisoBitemporal.getValidityInterval().getInterval().getStart();
		
		cotizacionId = inciso.getContinuity().getParentContinuity().getNumero().longValue();
		
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, BigDecimal.valueOf(cotizacionId));
		if(cotizacion.getNumPolizaServicioPublico() != null
				&& !cotizacion.getNumPolizaServicioPublico().isEmpty()){
			return  inciso.getValue().getNumeroSecuencia().toString();
		}
		
		params = new LinkedHashMap<String, Object>();
		params.put("idToCotizacion", cotizacionId);
		params.put("validFrom", validFrom.toDate());
		params.put("recordFrom", recordFrom.toDate());
		
		endosos = entidadService.findByProperties(EndosoDTO.class, params);
		
		if (endosos != null && endosos.size() > 0) {
			numeroEndosoAltaInciso = endosos.get(0).getId().getNumeroEndoso();
			idPoliza = endosos.get(0).getId().getIdToPoliza();
		}
		
		if (numeroEndosoAltaInciso > 0) {
			esMigrado = esEndosoMigrado(idPoliza, numeroEndosoAltaInciso);
		}
		
		if (esMigrado) {
			numeroSecuencia = inciso.getValue().getNumeroSecuencia().toString();
			return Integer.parseInt(numeroSecuencia.substring(numeroSecuencia.length() - 4)) + "";
		}
		
		return inciso.getValue().getNumeroSecuencia().toString();
		//return inciso.getContinuity().getNumero().toString();
	}
	
	/**
	 * Obtiene el numero de endoso en la poliza de Seycos a partir del endoso de Midas
	 * @param endoso Endoso de Midas
	 * @param clavePolizaSeycos Clave de la poliza de Seycos
	 * @return El numero de endoso de Seycos
	 */
	@Override
	public String obtenerNumeroEndosoSeycos(EndosoDTO endoso, String clavePolizaSeycos) {
		
		if (clavePolizaSeycos == null || clavePolizaSeycos.trim().equals("")) return endoso.getId().getNumeroEndoso().toString();
		try{
			BigDecimal numEndosoSeycos = null;
			boolean esMigrado = true;
			BigDecimal idSolicitudSeycos = null;
		
			esMigrado = esEndosoMigrado(endoso.getId().getIdToPoliza(), endoso.getId().getNumeroEndoso());
				
			if (esMigrado) {
			
				idSolicitudSeycos = endosoInterfazFacade.obtieneConversionSeycos("ENDOSO", 
					endoso.getId().getIdToPoliza() + "|" + endoso.getId().getNumeroEndoso());
			
				numEndosoSeycos = endosoInterfazFacade.obtieneNumeroEndosoSeycos(idSolicitudSeycos);
			
				return numEndosoSeycos + "";
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return endoso.getId().getNumeroEndoso().toString();
	}
	
	
	private boolean esEndosoMigrado(BigDecimal idPoliza, Short numeroEndoso) {
		
		BigDecimal idSolicitudSeycos = null;
		Map<String, Object> params = null;
		List<MigEndososValidosEd> endososMigrados = null;
		
		idSolicitudSeycos = endosoInterfazFacade.obtieneConversionSeycos("ENDOSO", idPoliza + "|" + numeroEndoso);
		
		params = new LinkedHashMap<String, Object>();
		params.put("idSolicitud", idSolicitudSeycos);
		params.put("idToPoliza", idPoliza);
		endososMigrados = entidadService.findByProperties(MigEndososValidosEd.class, params);
		
		if (endososMigrados != null && endososMigrados.size() > 0) {
			return true;
		}
		
		return false;
	}

	private List<DatosCaratulaInciso> llenarIncisoDTO(BitemporalCotizacion bCotizacion, String numeroPoliza,
			DateTime validOn, DateTime recordFrom, boolean incluirTososLosIncisos, int incisoInicial,
			int incisoFinal, PolizaDTO polizaDTO, DatosPolizaDTO datosPolizaDTO, Short numeroEndoso, Short claveTipoEndoso,
			boolean incluirCondicionesEsp, EdicionInciso edicionInciso) {

		String detalleTitulo = "";
		
		ResumenCostosDTO resumenInciso = null;
		
		boolean esServicioPublico = false;
		
		if(polizaDTO != null
				&& polizaDTO.getCotizacionDTO() != null
				&& polizaDTO.getCotizacionDTO().getSolicitudDTO() != null
				&& polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio() != null
				&& polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio() != null){
			Map<String, Object> spvData = listadoService.getSpvData(polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio());
			esServicioPublico = (Boolean)spvData.get("esServicioPublico");
		}
		
		// Objeto que sera llenado y devuelto con la información completa de los incisos que se requieran
		// para su impresión
		List<DatosCaratulaInciso> datosCaratulaIncisoList = new ArrayList<DatosCaratulaInciso>();
		Cotizacion cotizacion = bCotizacion.getValue();
		CotizacionContinuity cotizacionCntity = bCotizacion.getContinuity();
		Map<String, Object> paramsInc = new HashMap<String, Object>();
		paramsInc.put("continuity.cotizacionContinuity.id", cotizacionCntity.getId());
		bitemporalIncisoList = (List<BitemporalInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalInciso.class,
				paramsInc, validOn, recordFrom, false, "continuity.numero");

		if (bitemporalIncisoList.isEmpty() && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION) {
			Long parentKey = cotizacionCntity.getId();
			
			Collection<IncisoContinuity> collectionIncisoContinuity =
				entidadContinuityService.findContinuitiesByParentKey(IncisoContinuity.class,
						IncisoContinuity.PARENT_KEY_NAME, parentKey);
			
			if (!collectionIncisoContinuity.isEmpty()) {
				bitemporalIncisoList = (List<BitemporalInciso>) entidadBitemporalService.obtenerCancelados(collectionIncisoContinuity,
						BitemporalInciso.class, validOn, recordFrom);
				
				Collections.sort(bitemporalIncisoList, new Comparator<BitemporalInciso>(){
					public int compare(BitemporalInciso o1, BitemporalInciso o2) {
						return ((BitemporalInciso)o1).getContinuity().getNumero().compareTo(
								((BitemporalInciso)o2).getContinuity().getNumero());
					}
				});
			}
		}

		// Seleccion por numero de Inciso
		if (incisoInicial > 0 && incisoFinal > 0 && incisoFinal >= incisoInicial) {
			for (int i = 0; i < bitemporalIncisoList.size(); i++) {
				BitemporalInciso item = bitemporalIncisoList.get(i);
				if (item.getValue().getNumeroSecuencia() < incisoInicial
						|| item.getValue().getNumeroSecuencia() > incisoFinal) {
					bitemporalIncisoList.remove(i);
					i--;
				}
			}
		}
		
		String subTitulo = "";
		String tipoInciso = "";
		String numeroImpresion = "";
		
		detalleTitulo = "P\u00D3LIZA DE SEGURO PARA";
		subTitulo = "INFORMACI\u00D3N GENERAL DE LA P\u00D3LIZA";
		tipoInciso = "P\u00F3liza:";
		numeroImpresion = numeroPoliza;
		
		// Si no se cuentan con los datos de la poliza para obtener los datos
		// del contratante se obtienen de nuevo
		if (datosPolizaDTO == null || datosPolizaDTO.getTxDomicilio() == null
				|| datosPolizaDTO.getRfcContratante() == null) {

			if (datosPolizaDTO == null) {
				datosPolizaDTO = new DatosPolizaDTO();
			}
			BigDecimal idPersonaContratante = new BigDecimal(cotizacion.getPersonaContratanteId());
			BigDecimal idDomicilioContratante = cotizacion.getDomicilioContratanteId();
			getDatosCliente(idPersonaContratante, idDomicilioContratante);

			datosPolizaDTO.setIdToPersonaContratante(new BigDecimal(cotizacion.getPersonaContratanteId()));
			if (cliente != null) {
				datosPolizaDTO.setRfcContratante(cliente.getCodigoRFC());
			}

			if (domicilioCliente != null) {
				datosPolizaDTO.setCalleYNumero(domicilioCliente.getCalleNumero());
				datosPolizaDTO.setColonia(domicilioCliente.getNombreColonia());
				datosPolizaDTO.setCiudad(domicilioCliente.getCiudad());
				datosPolizaDTO.setEstado(domicilioCliente.getEstado());
				datosPolizaDTO.setCodigoPostal(domicilioCliente.getCodigoPostal());
			}
		}

		posiblesDeducibles = entidadService.findByProperty(
				CatalogoValorFijoDTO.class, "id.idGrupoValores", CatalogoValorFijoDTO.IDGRUPO_POSIBLES_DEDUCIBLES);

		for (BitemporalInciso bitempInciso : bitemporalIncisoList) {
			Collection<BitemporalSeccionInciso> collectionBitemporalSeccionInciso = null;
			BitemporalSeccionInciso bitempSeccionInciso = null;
			Map<String, Object> paramsSec = new HashMap<String, Object>(1);
			paramsSec.put("continuity.incisoContinuity.id", bitempInciso.getContinuity().getId());		
			collectionBitemporalSeccionInciso = (List<BitemporalSeccionInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalSeccionInciso.class, 
					paramsSec, validOn, recordFrom, false);
			if(collectionBitemporalSeccionInciso.isEmpty() 
					&& claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION){
				IncisoContinuity ic = bitempInciso.getContinuity();
				Collection<SeccionIncisoContinuity> collectionSeccionIncisoContinuities = 
					entidadContinuityService.findContinuitiesByParentKey(
							SeccionIncisoContinuity.class, SeccionIncisoContinuity.PARENT_KEY_NAME, 
							ic.getId());
				
				if(!collectionSeccionIncisoContinuities.isEmpty()){
					Long continuitiId = collectionSeccionIncisoContinuities.iterator().next().getId();
					collectionBitemporalSeccionInciso = entidadBitemporalService.obtenerCancelados(
							BitemporalSeccionInciso.class, continuitiId, validOn, recordFrom);
					bitempSeccionInciso = collectionBitemporalSeccionInciso.iterator().next();
				}
			}else{
				bitempSeccionInciso = collectionBitemporalSeccionInciso.iterator().next();
			}
			
			Collection<BitemporalCoberturaSeccion> bitempCobertSeccCollection = null;
			SeccionIncisoContinuity sic_local = bitempSeccionInciso.getEntidadContinuity();
			Map<String, Object> paramsCob = new HashMap<String, Object>();
			paramsCob.put("continuity.seccionIncisoContinuity.id", sic_local.getId());		
			bitempCobertSeccCollection = (List<BitemporalCoberturaSeccion>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalCoberturaSeccion.class, 
					paramsCob, validOn, recordFrom, false);

			if(bitempCobertSeccCollection.isEmpty() && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION){
				bitempCobertSeccCollection = new ArrayList<BitemporalCoberturaSeccion>();
				Collection<CoberturaSeccionContinuity> collectionCoberturaSeccionContinuity = 
					entidadContinuityService.findContinuitiesByParentKey(
						CoberturaSeccionContinuity.class, CoberturaSeccionContinuity.PARENT_KEY_NAME, 
						sic_local.getId());
				if(!collectionCoberturaSeccionContinuity.isEmpty()){
					for(CoberturaSeccionContinuity csc_current : collectionCoberturaSeccionContinuity){
						Long continuitiId = csc_current.getId();
						Collection<BitemporalCoberturaSeccion> bitempCobertSeccCollection_temp = 
							entidadBitemporalService.obtenerCancelados(BitemporalCoberturaSeccion.class, 
									continuitiId, validOn, recordFrom);
						BitemporalCoberturaSeccion bcs_current = bitempCobertSeccCollection_temp.iterator().next();
						bitempCobertSeccCollection.add(bcs_current);
					}
				}
			}

			// TODO: Checar si interpreté bien lo de autoCotizaciónInciso.
			BitemporalAutoInciso bai = null;
			AutoIncisoContinuity aic = bitempInciso.getContinuity().getAutoIncisoContinuity();
			
			Map<String, Object> paramsAut = new HashMap<String, Object>(1);
			paramsAut.put("continuity.id", aic.getId());		
			List<BitemporalAutoInciso> baiCollection = (List<BitemporalAutoInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalAutoInciso.class, 
					paramsAut, validOn, recordFrom, false);
			
			if(baiCollection != null && !baiCollection.isEmpty()){
				bai = baiCollection.get(0);
			}
			
			if(bai == null && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION){
				Long continuitiId = aic.getId();
				Collection<BitemporalAutoInciso> collectionBAI = entidadBitemporalService.obtenerCancelados(
						BitemporalAutoInciso.class, continuitiId, validOn, recordFrom);
				bai = collectionBAI.iterator().next();
			}
				
			if (bai != null) {
				DatosIncisoDTO dataSourceInciso = new DatosIncisoDTO();
				DatosCaratulaInciso datosCaratulaInciso = new DatosCaratulaInciso();
				dataSourceInciso.setIdInciso(bitempInciso.getValue().getNumeroSecuencia().toString());
				dataSourceInciso.setNumeroCotizacion(cotizacionCntity.getNumeroCotizacion());
				dataSourceInciso.setIdSeccion(
						bitempSeccionInciso.getContinuity().getSeccion().getIdToSeccion().toString());
				try{
					List<BitemporalInciso> incisoEvolution = null;
					incisoEvolution = bitempInciso.getEntidadContinuity().getBitemporalProperty().getEvolution(validOn);
					if(incisoEvolution.isEmpty()){	
						incisoEvolution = bitempInciso.getEntidadContinuity().getBitemporalProperty().getHistory(validOn);
					}

					Collections.sort(incisoEvolution, new Comparator<Object>(){
						public int compare(Object o1, Object o2) {
							return ((BitemporalInciso)o1).getId().compareTo(((BitemporalInciso)o2).getId());
						}
					});					
					dataSourceInciso.setFechaInicioVigencia(
						new Date(incisoEvolution.get(0).getValidityInterval().getInterval().getStartMillis()));
				}catch(Exception e){
					dataSourceInciso.setFechaInicioVigencia(new Date(
							bitempInciso.getValidityInterval().getInterval().getStartMillis()));
				}
				dataSourceInciso.setFechaFinVigencia(cotizacion.getFechaFinVigencia());

				resumenInciso = calculoService.resumenCostosEmitidos(polizaDTO.getIdToPoliza(), numeroEndoso, bitempInciso.getContinuity().getNumero());
				
				boolean imprimirUMA = listadoService.imprimirLeyendaUMA(dataSourceInciso.getFechaInicioVigencia());

				if (resumenInciso != null) {
					dataSourceInciso.setPrimaNetaCotizacion(UtileriasWeb.getDoubleValue(resumenInciso.getPrimaNetaCoberturas()));
					dataSourceInciso.setDescuentoComisionCedida(resumenInciso.getDescuentoComisionCedida());
					dataSourceInciso.setDescuento(UtileriasWeb.getDoubleValue(resumenInciso.getDescuentoComisionCedida()));
					dataSourceInciso.setDerechosPoliza(UtileriasWeb.getDoubleValue(resumenInciso.getDerechos()));
					dataSourceInciso.setIdToPersonaContratante(
							new BigDecimal(cotizacion.getPersonaContratanteId()));
					dataSourceInciso.setPrimaNetaTotal(UtileriasWeb.getDoubleValue(resumenInciso.getPrimaTotal()));
					dataSourceInciso.setDescuento(resumenInciso.getDescuento());
					dataSourceInciso.setTotalPrimas(UtileriasWeb.getDoubleValue(resumenInciso.getTotalPrimas()));
					dataSourceInciso.setMontoRecargoPagoFraccionado(UtileriasWeb.getDoubleValue(resumenInciso.getRecargo()));
					dataSourceInciso.setMontoIVA(UtileriasWeb.getDoubleValue(resumenInciso.getIva()));
				}
						
				dataSourceInciso.setNombreContratante(getNombreContratante(cotizacion, bai));
				dataSourceInciso.setTitulo(detalleTitulo + " " 
						+ bitempSeccionInciso.getEntidadContinuity().getSeccion().getDescripcion().toUpperCase());
				dataSourceInciso.setSubTitulo(subTitulo);
				dataSourceInciso.setTipoInciso(tipoInciso);
				dataSourceInciso.setNumeroPoliza(numeroImpresion);
				dataSourceInciso.setNombreContratanteInciso(getNombreContratanteInciso(cotizacion, bai));
				String identificador = getIdentificadorCaratulaPoliza(bCotizacion).get("identificador");
				dataSourceInciso.setIdentificador(identificador);
				dataSourceInciso.setTipoPoliza(cotizacion.getTipoPoliza().getDescripcion());
				dataSourceInciso.setIdToPersonaContratante(datosPolizaDTO.getIdToPersonaContratante());
				dataSourceInciso.setRfcContratante(datosPolizaDTO.getRfcContratante());
				dataSourceInciso.setCalleYNumero(datosPolizaDTO.getCalleYNumero());
				dataSourceInciso.setColonia(datosPolizaDTO.getColonia());
				dataSourceInciso.setCiudad(datosPolizaDTO.getCiudad());
				dataSourceInciso.setEstado(datosPolizaDTO.getEstado());
				dataSourceInciso.setCodigoPostal(datosPolizaDTO.getCodigoPostal());
				dataSourceInciso.setIdClase(bai.getEmbedded().getClaveTipoBien());			
				dataSourceInciso.setModelo(bai.getEmbedded().getModeloVehiculo().toString());				
				
				
				
			
					
				
				if (bai.getEmbedded().getCodigoPostal() != null){
					dataSourceInciso.setCodigoPostalCirculacion(bai.getEmbedded().getCodigoPostal().toString());
				}
				
				if (bai.getEmbedded().getPlaca() != null){
					dataSourceInciso.setNumeroPlacas(bai.getEmbedded().getPlaca().toUpperCase());
				}
				if (bai.getEmbedded().getNumeroMotor() != null){
					dataSourceInciso.setNumeroMotor(bai.getEmbedded().getNumeroMotor().toUpperCase());
				}
				if(bai.getEmbedded().getNumeroSerie() != null){
					dataSourceInciso.setNumeroSerie(bai.getEmbedded().getNumeroSerie().toUpperCase());
				}
				EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();			
				String[]idCompuestoEstilo=bai.getEmbedded().getEstiloId().split("_");
				estiloVehiculoId.setClaveEstilo(idCompuestoEstilo[1]);
				estiloVehiculoId.setClaveTipoBien(idCompuestoEstilo[0]);			
				estiloVehiculoId.setIdVersionCarga(BigDecimal.valueOf(Long.parseLong(idCompuestoEstilo[2]))) ;
				EstiloVehiculoDTO estiloVehiculoDTO = 
					entidadService.findById(EstiloVehiculoDTO.class, estiloVehiculoId);
				Short numeroDeAsientosDelVehiculo = estiloVehiculoDTO.getNumeroAsientos();
				dataSourceInciso.setNumeroAsientos(numeroDeAsientosDelVehiculo.toString());
				dataSourceInciso.setIdClase(idCompuestoEstilo[1]);
				dataSourceInciso.setDescripcionClase(bai.getEmbedded().getDescripcionFinal());
				dataSourceInciso.setTextoCopia("COPIA");
				MarcaVehiculoDTO marcaVehiculoDTO= entidadService.findById(
						MarcaVehiculoDTO.class, bai.getEmbedded().getMarcaId());
				if (bCotizacion.getValue() != null && bCotizacion.getValue().getFormaPago() != null) {
					dataSourceInciso.setDescripcionFormaPago(bCotizacion.getValue().getFormaPago().getDescripcion());
				}
				dataSourceInciso.setMarca(marcaVehiculoDTO.getDescription());
				if (moneda != null) {
					dataSourceInciso.setDescripcionMoneda(moneda.getDescripcion());
				} else {
					dataSourceInciso.setDescripcionMoneda(bCotizacion.getValue().getMoneda().getDescripcion());
				}
				
				this.setObservacionesIniciso(bitempInciso, dataSourceInciso, validOn, recordFrom, claveTipoEndoso,bai);
				
				TipoUsoVehiculoDTO tipoUsoVehiculoDTO = entidadService.findById(TipoUsoVehiculoDTO.class,BigDecimal.valueOf(bai.getEmbedded().getTipoUsoId()));
				
				try {
					TipoServicioVehiculoDTO tipoServicioVehiculoDTO = entidadService.findById(TipoServicioVehiculoDTO.class,new BigDecimal(bai.getEmbedded().getTipoServicioId()));
					dataSourceInciso.setTipoServicio(tipoServicioVehiculoDTO.getDescripcionTipoServVehiculo());
				} catch (RuntimeException exception) {
					dataSourceInciso.setTipoServicio("No disponible");
				}
				
				dataSourceInciso.setTipoUso(tipoUsoVehiculoDTO.getDescription().toUpperCase());
				dataSourceInciso.setTextoAgregado("");
				// TODO: Averiguar que debera ir y de donde se obtiene el tipo de carga o si es de carga o no
				dataSourceInciso.setTipoCarga("");
				setTipoCargaInciso(bitempInciso, dataSourceInciso, validOn, recordFrom, claveTipoEndoso, bai);
				
				List<BitemporalCoberturaSeccion> bitemporalCoberturaSeccionListSoloContratadas = 
					filtrarCoberturasNoContratadas(bitempCobertSeccCollection);
				//Parametros Generale
				ParametroGeneralId idParametro=new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_COBERTURA_RC_USA,new BigDecimal(920010));
				ParametroGeneralDTO parametro=parametroGeneralFacade.findById(idParametro);
				String[] valoresParametros=null;
				if(parametro !=null && parametro.getValor()!=null){
					 valoresParametros=parametro.getValor().split(UtileriasWeb.SEPARADOR_COMA);	
				}
				//Valida NationalUnity
				for(BitemporalCoberturaSeccion cobertura: bitemporalCoberturaSeccionListSoloContratadas){
					List<BigDecimal> coberturasNU = new ArrayList<BigDecimal>(1);
					for( int i = 0; valoresParametros!=null && i <= valoresParametros.length - 1; i++)
					{
						if( StringUtils.isNumeric(valoresParametros[i]) ){
							BigDecimal idCobertura= new  BigDecimal(valoresParametros[i]);
							coberturasNU.add(idCobertura);
						}
					}
					
					if(coberturasNU.contains(cobertura.getContinuity().getCoberturaDTO().getIdToCobertura()) &&
						(!StringUtil.isEmpty(bai.getValue().getNombreAsegurado()) && !StringUtil.isEmpty(bai.getValue().getNombreConductor())    )){
						dataSourceInciso.setNationalUnity(true);
						break;
					}
				}
				
				dataSourceInciso.setClavePolizaSeycos(polizaDTO.getClavePolizaSeycos());
				dataSourceInciso.setNumeroIncisoSeycos(obtenerNumeroIncisoSeycos(bitempInciso, polizaDTO.getClavePolizaSeycos()));
				
				//TODO Quitar este parche (revert a toda la revision de este cambio, cuando sea posible)
				//Parche necesario para nationalUnity
				dataSourceInciso.setNumeroSecuenciaInciso( bitempInciso.getContinuity().getNumero().toString());
				
				//Solo se ejecuta en caso de que la edicionInciso no sea null
				setInformacionEdicionInciso(dataSourceInciso, edicionInciso);
				
				datosCaratulaInciso.setDataSourceIncisos(dataSourceInciso);
				
				List<BitemporalCoberturaSeccion> bitempoCoberturaSeccionListOrdenada = 
					ordenaCoberturas(bitemporalCoberturaSeccionListSoloContratadas);
				
				List<DatosCoberturasDTO> dcDTO = 
					llenarCoberturasList(bitempoCoberturaSeccionListOrdenada, numeroDeAsientosDelVehiculo, estiloVehiculoId, 
							bCotizacion.getValue().getMoneda().getIdTcMoneda(), bai.getEmbedded().getModeloVehiculo(), imprimirUMA, esServicioPublico);
				
				//Solo se ejecuta en caso de que la edicionInciso no sea null
				setInformacionCoberturasEdicionInciso(dcDTO, edicionInciso);
				datosCaratulaInciso.setDataSourceCoberturas(dcDTO);			
				
				if (incluirCondicionesEsp) {
					try{
						List<CondicionEspecial> condicionesEspecialesInciso = 
							obtenerCondicionesEspecialesInciso(bitempInciso, validOn, recordFrom);
						datosCaratulaInciso.getDataSourceIncisos().setCondicionesEspeciales( condicionesEspecialesInciso );
					}catch(Exception e){
						LogDeMidasEJB3.getLogger().log(Level.SEVERE, "Condiciones Especiales " + bCotizacion.getContinuity().getNumero() 
								+ " inciso " + datosCaratulaInciso.getDataSourceIncisos().getNumeroSecuenciaInciso(), e);
					}
				}
				
				datosCaratulaIncisoList.add(datosCaratulaInciso);
			}
		}
		
		return datosCaratulaIncisoList;
	}
	
	/**
	 * si el parametro edicionInciso no es null, se carga la informacion de las coberturas capturadas en la edicion del inciso
	 * @param dcDTO
	 * @param edicionInciso
	 */
	private void setInformacionCoberturasEdicionInciso(List<DatosCoberturasDTO> datosCoberturasOriginales, EdicionInciso edicionInciso){
		if(edicionInciso != null
				&& edicionInciso.getCoberturas() != null){
			for(DatosCoberturasDTO informacionOriginal : datosCoberturasOriginales){
				for(EdicionIncisoCobertura informacionEditada: edicionInciso.getCoberturas()){
					if(informacionOriginal.getRiesgo().equals(informacionEditada.getRiesgo())){
						if(!StringUtil.isEmpty(informacionEditada.getSumaAsegurada())){
							informacionOriginal.setSumaAsegurada(informacionEditada.getSumaAsegurada());}
						if(!StringUtil.isEmpty(informacionEditada.getDeducible())){
							informacionOriginal.setDeducible(informacionEditada.getDeducible());}
						break;
		}}}}
	}
	
	/**
	 * Si el parametro edicionInciso no es null, se carga la informacion de la Edicion en la impresion
	 * @param datosInciso
	 * @param edicionInciso
	 */
	private void setInformacionEdicionInciso(DatosIncisoDTO datosInciso, EdicionInciso edicionInciso){
		if(edicionInciso != null){
			if(!StringUtil.isEmpty(edicionInciso.getCalleNumero() )){
				datosInciso.setCalleYNumero(edicionInciso.getCalleNumero() );}
			if(!StringUtil.isEmpty(edicionInciso.getColonia() )){
				datosInciso.setColonia( edicionInciso.getColonia() );}
			if(!StringUtil.isEmpty(edicionInciso.getEstado())){
				datosInciso.setCiudad(null);
				datosInciso.setEstado(edicionInciso.getEstado());}
			if(!StringUtil.isEmpty(edicionInciso.getCodigoPostal() )){
				datosInciso.setCodigoPostal( edicionInciso.getCodigoPostal() );}
			if(!StringUtil.isEmpty(edicionInciso.getNombreContratante() )){
				datosInciso.setNombreContratante( edicionInciso.getNombreContratante() );}
			if(!StringUtil.isEmpty(edicionInciso.getNombreContratanteInciso() )){
				datosInciso.setNombreContratanteInciso( edicionInciso.getNombreContratanteInciso() );}
			if(!StringUtil.isEmpty(edicionInciso.getObservaciones() )){
				datosInciso.setObservaciones( edicionInciso.getObservaciones() );}
			if(!StringUtil.isEmpty(edicionInciso.getRfcContratante() )){
				datosInciso.setRfcContratante( edicionInciso.getRfcContratante() );}
			if(!StringUtil.isEmpty(edicionInciso.getTipoServicio() )){
				datosInciso.setTipoServicio( edicionInciso.getTipoServicio() );}
			if(!StringUtil.isEmpty(edicionInciso.getTipoUso() )){
				datosInciso.setTipoUso( edicionInciso.getTipoUso() );}
			if( edicionInciso.getIdToPersonaContratante() != null ){
				datosInciso.setIdToPersonaContratante( edicionInciso.getIdToPersonaContratante() );}
			if(edicionInciso.getMontoIva() != null){
				datosInciso.setMontoIVA( edicionInciso.getMontoIva() );
				Double primaNetaTotal = datosInciso.getPrimaNetaCotizacion() + 
					datosInciso.getMontoRecargoPagoFraccionado() + datosInciso.getDerechosPoliza() + datosInciso.getMontoIVA();
				datosInciso.setPrimaNetaTotal(primaNetaTotal);}
		}
	}
	
	@Override
	public List<DatosCoberturasDTO> obtenerCoberturasEdicionInciso(BitemporalCotizacion bitemporaCotizacion,
			DateTime validOn, DateTime recordFrom, Short claveTipoEndoso){
		
		List<DatosCoberturasDTO> datosCoberturas = new ArrayList<DatosCoberturasDTO>();
		CotizacionContinuity cotizacionCntity = bitemporaCotizacion.getContinuity();
		Map<String, Object> paramsInc = new HashMap<String, Object>();
		paramsInc.put("continuity.cotizacionContinuity.id", cotizacionCntity.getId());
		bitemporalIncisoList = (List<BitemporalInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalInciso.class,
				paramsInc, validOn, recordFrom, false, "continuity.numero");

		if (bitemporalIncisoList.isEmpty() && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION) {
			Long parentKey = cotizacionCntity.getId();
			
			Collection<IncisoContinuity> collectionIncisoContinuity =
				entidadContinuityService.findContinuitiesByParentKey(IncisoContinuity.class,
						IncisoContinuity.PARENT_KEY_NAME, parentKey);
			
			if (!collectionIncisoContinuity.isEmpty()) {
				bitemporalIncisoList = (List<BitemporalInciso>) entidadBitemporalService.obtenerCancelados(collectionIncisoContinuity,
						BitemporalInciso.class, validOn, recordFrom);
				
				Collections.sort(bitemporalIncisoList, new Comparator<BitemporalInciso>(){
					@Override
					public int compare(BitemporalInciso o1, BitemporalInciso o2) {
						return o1.getContinuity().getNumero().compareTo(
								o2.getContinuity().getNumero());
					}
				});
			}
		}
		
		for (BitemporalInciso bitempInciso : bitemporalIncisoList) {
			Collection<BitemporalSeccionInciso> collectionBitemporalSeccionInciso;
			BitemporalSeccionInciso bitempSeccionInciso = null;
			Map<String, Object> paramsSec = new HashMap<String, Object>(1);
			paramsSec.put("continuity.incisoContinuity.id", bitempInciso.getContinuity().getId());		
			collectionBitemporalSeccionInciso = entidadContinuityDao.listarBitemporalsFiltrado(BitemporalSeccionInciso.class, 
					paramsSec, validOn, recordFrom, false);
			if(collectionBitemporalSeccionInciso.isEmpty() 
					&& claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION){
				IncisoContinuity ic = bitempInciso.getContinuity();
				Collection<SeccionIncisoContinuity> collectionSeccionIncisoContinuities = 
					entidadContinuityService.findContinuitiesByParentKey(
							SeccionIncisoContinuity.class, SeccionIncisoContinuity.PARENT_KEY_NAME, 
							ic.getId());
				
				if(!collectionSeccionIncisoContinuities.isEmpty()){
					Long continuitiId = collectionSeccionIncisoContinuities.iterator().next().getId();
					collectionBitemporalSeccionInciso = entidadBitemporalService.obtenerCancelados(
							BitemporalSeccionInciso.class, continuitiId, validOn, recordFrom);
					bitempSeccionInciso = collectionBitemporalSeccionInciso.iterator().next();
				}
			}else{
				bitempSeccionInciso = collectionBitemporalSeccionInciso.iterator().next();
			}
			
			Collection<BitemporalCoberturaSeccion> bitempCobertSeccCollection;
			SeccionIncisoContinuity sicLocal = bitempSeccionInciso.getEntidadContinuity();
			Map<String, Object> paramsCob = new HashMap<String, Object>();
			paramsCob.put("continuity.seccionIncisoContinuity.id", sicLocal.getId());		
			bitempCobertSeccCollection = entidadContinuityDao.listarBitemporalsFiltrado(BitemporalCoberturaSeccion.class, 
					paramsCob, validOn, recordFrom, false);
	
			if(bitempCobertSeccCollection.isEmpty() && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION){
				bitempCobertSeccCollection = new ArrayList<BitemporalCoberturaSeccion>();
				Collection<CoberturaSeccionContinuity> collectionCoberturaSeccionContinuity = 
					entidadContinuityService.findContinuitiesByParentKey(
						CoberturaSeccionContinuity.class, CoberturaSeccionContinuity.PARENT_KEY_NAME, 
						sicLocal.getId());
				if(!collectionCoberturaSeccionContinuity.isEmpty()){
					for(CoberturaSeccionContinuity csc_current : collectionCoberturaSeccionContinuity){
						Long continuitiId = csc_current.getId();
						Collection<BitemporalCoberturaSeccion> bitempCobertSeccCollectionTemp = 
							entidadBitemporalService.obtenerCancelados(BitemporalCoberturaSeccion.class, 
									continuitiId, validOn, recordFrom);
						BitemporalCoberturaSeccion bcsCurrent = bitempCobertSeccCollectionTemp.iterator().next();
						bitempCobertSeccCollection.add(bcsCurrent);
					}
				}
			}
			
			List<BitemporalCoberturaSeccion> bitempoCoberturaSeccionListOrdenada = 
				ordenaCoberturas(bitempCobertSeccCollection);
			
			List<BitemporalCoberturaSeccion> bitemporalCoberturaSeccionListSoloContratadas = 
				filtrarCoberturasNoContratadas(bitempoCoberturaSeccionListOrdenada);
			
			List<DatosCoberturasDTO> datosCoberturasInciso = 
				llenarCoberturasList(bitemporalCoberturaSeccionListSoloContratadas);
			for(DatosCoberturasDTO datoInciso : datosCoberturasInciso){
				boolean yaExiste = false;
				for(DatosCoberturasDTO datoCobertura : datosCoberturas){
					if(datoCobertura.getRiesgo().equals(datoInciso.getRiesgo())){
						yaExiste = true;
						break;
					}
				}
				if(!yaExiste){
					datosCoberturas.add(datoInciso);
				}
			}
		}
		return datosCoberturas;
	}
	
	/**
	 * Obtiene la lista de coberturas solamente con la informacion necesaria para mostrar en la edicion de inciso.
	 * @param coberturasList
	 * @return
	 */
	private List<DatosCoberturasDTO> llenarCoberturasList(Collection<BitemporalCoberturaSeccion> coberturasList){
		List<DatosCoberturasDTO> dataSourceCoberturas = new ArrayList<DatosCoberturasDTO>();
		for(BitemporalCoberturaSeccion bitempCoberturaSeccion : coberturasList){

			CoberturaDTO coberturaDTO = bitempCoberturaSeccion.getContinuity().getCoberturaDTO();
			DatosCoberturasDTO datosCoberturasDTO = new DatosCoberturasDTO(); 
			 
			datosCoberturasDTO.setRiesgo(coberturaDTO.getNumeroSecuencia().intValue());
			datosCoberturasDTO.setNombreCobertura(coberturaDTO.getNombreComercial());
			datosCoberturasDTO.setIdToCobertura(coberturaDTO.getIdToCobertura());
			
			dataSourceCoberturas.add(datosCoberturasDTO);
		}		
		
		return dataSourceCoberturas;
	}
	
	private String getNombreContratanteInciso(Cotizacion cotizacion,
			BitemporalAutoInciso bai) {
		String nombreContratanteInciso = null;
		String nombreContratante = getNombreContratante(cotizacion, bai);
		String nombreAsegurado = bai.getEmbedded().getNombreAsegurado();
		if(cotizacion.getPersonaContratanteId() != null && bai.getEmbedded().getPersonaAseguradoId() != null && bai.getEmbedded().getPersonaAseguradoId().equals(cotizacion.getPersonaContratanteId())){
			nombreContratanteInciso = "";
		}else{
			if (nombreAsegurado != null &&
				!nombreContratante.trim().equalsIgnoreCase(nombreAsegurado.trim())){
				nombreContratanteInciso =  " Y/O " + bai.getEmbedded().getNombreAsegurado();
			}else{
				nombreContratanteInciso = "";
			}
		}		
		return nombreContratanteInciso;
	}

	private String getNombreContratante(Cotizacion cotizacion, BitemporalAutoInciso bai) {
		String nombreContratante = null;	
		if(cotizacion.getNombreContratante() != null){
			if(bai.getEmbedded().getNombreAsegurado() != null && cotizacion.getNombreContratante().trim().equals(
					bai.getEmbedded().getNombreAsegurado().trim())){					
				nombreContratante = cotizacion.getNombreContratante();
			}else{
				nombreContratante = cotizacion.getNombreContratante().toUpperCase();
			}
		}
		return nombreContratante;
	}

	private List<CondicionEspecial> obtenerCondicionesEspecialesInciso(
			BitemporalInciso bitemporalInciso, DateTime validOn, DateTime recordFrom) throws IOException {

		List<CondicionEspecial> condiciones = new ArrayList<CondicionEspecial>();
		
		condiciones = condicionEspecialDao.obtenerCondicionesIncisoValidasInciso(bitemporalInciso.getContinuity().getId(), validOn, recordFrom);
		
		return condiciones;
	}

	private List<BitemporalCoberturaSeccion> filtrarCoberturasNoContratadas(
			Collection<BitemporalCoberturaSeccion> bitempCobertSeccCollection) {
		List<BitemporalCoberturaSeccion> soloCoberturasContratadasList = new ArrayList<BitemporalCoberturaSeccion>();
		
		for (BitemporalCoberturaSeccion bcs : bitempCobertSeccCollection) {
			if (bcs.getEmbedded().getClaveContrato().intValue() == 1) {
				soloCoberturasContratadasList.add(bcs);
			}
		}
		
		return soloCoberturasContratadasList;
	}

	private List<BitemporalCoberturaSeccion> ordenaCoberturas(
			Collection<BitemporalCoberturaSeccion> bitempCobertSeccCollection) {
		
		List<BitemporalCoberturaSeccion> bitempCoberturaSeccionList =  new ArrayList<BitemporalCoberturaSeccion>();
		bitempCoberturaSeccionList.addAll(bitempCobertSeccCollection);
		
		Collections.sort(bitempCoberturaSeccionList, new Comparator<BitemporalCoberturaSeccion>() {
			public int compare(BitemporalCoberturaSeccion bcs1, BitemporalCoberturaSeccion bcs2){
				BigDecimal bd1 = bcs1.getContinuity().getCoberturaDTO().getIdToCobertura();
				BigDecimal bd2 = bcs2.getContinuity().getCoberturaDTO().getIdToCobertura();
				
				return bd1.compareTo(bd2);
			}
		});
		
		return bitempCoberturaSeccionList;
	}

	/**
	 * 
	 * @param incisoCotizacionDTO
	 * @param datosIncisoDTO
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 */
	private void setObservacionesIniciso(BitemporalInciso incisoCotizacionDTO, DatosIncisoDTO datosIncisoDTO, 
			DateTime validOn, DateTime recordFrom, Short claveTipoEndoso,BitemporalAutoInciso autoInciso){
		LogDeMidasInterfaz.log("Entrando a setObservacionesIniciso", Level.FINE, null);		
		 Map<String, String> valores = new LinkedHashMap<String, String>();
		 String accion = null;
		 
		 // Se imprimiran solamente polizas, no cotizaciones y/o polizas canceladas
		 boolean isInProcess = false;
		 //incisoCotizacionDTO.getContinuity().getParentContinuity().getNumero().longValue();
		 /*Long incisoCotizacionParentContinuityId = 
			 incisoCotizacionDTO.getContinuity().getParentContinuity().getId();
		 
		 List<ControlDinamicoRiesgoDTO> controles = 
			 configDatoIncisoBitemporalService.getDatosRiesgo(
				 incisoCotizacionDTO.getContinuity().getId(), validOn.toDate(), recordFrom.toDate(), valores, 
				 incisoCotizacionParentContinuityId, accion, 
				 claveTipoEndoso, isInProcess);
		 */
		 List<ControlDinamicoRiesgoDTO> controles = 
			 configDatoIncisoBitemporalService.obtieneDescripcionDatosImpresion(
				 incisoCotizacionDTO.getContinuity().getId(), validOn, recordFrom); //poliza 4664
		 
		 
		 StringBuilder stringBuilder = new StringBuilder();
		 for(ControlDinamicoRiesgoDTO controlDinamicoRiesgoDTO: controles){
			 stringBuilder.append(controlDinamicoRiesgoDTO.getEtiqueta().toUpperCase() + " " + controlDinamicoRiesgoDTO.getDescripcionNivel());
			 stringBuilder.append(": ");
			 if(controlDinamicoRiesgoDTO.getValor() != null ){
				 String valor = controlDinamicoRiesgoDTO.getValor();
				 /*
				 if(controlDinamicoRiesgoDTO.getLista() != null && !controlDinamicoRiesgoDTO.getLista().isEmpty() &&
						 controlDinamicoRiesgoDTO.getIdGrupoValores() != null){
					 //CatalogoValorFijoId cat = new CatalogoValorFijoId();
					 //cat.setIdDato(Integer.parseInt(valor));
					 //cat.setIdGrupoValores(controlDinamicoRiesgoDTO.getIdGrupoValores().intValue());
					 //valor = controlDinamicoRiesgoDTO.getLista().get(cat.toString());
					 valor = controlDinamicoRiesgoDTO.getLista().get(valor);
				 }*/
				 if(valor == null){
					 valor = "";
				 }
				 if(valor.length() > 300){
					 stringBuilder.append(valor.substring(0,300));
					 stringBuilder = new StringBuilder(stringBuilder.substring(0,stringBuilder.lastIndexOf(" ")));
					 stringBuilder.append("...");
				 }else{
					 stringBuilder.append(valor);
				 }
			 }
			 stringBuilder.append(System.getProperty("line.separator"));
		 }
		 if(StringUtils.isNotBlank( autoInciso.getValue().getObservacionesinciso())) {
			 stringBuilder.append("OBSERVACIONES: " + autoInciso.getValue().getObservacionesinciso());
		 }
		 if(StringUtils.isNotBlank( autoInciso.getValue().getObservacionesSesa())) {
             if(stringBuilder.toString() != null && !stringBuilder.toString().equals("")){
                 stringBuilder.append((stringBuilder.toString().endsWith("\n") ? "":"\n") + "NOTA INFORMATIVA: El presente texto se presenta \u00fanicamente con fines informativos y de control de calidad. " + autoInciso.getValue().getObservacionesSesa());
             } else {
            	 stringBuilder.append("OBSERVACIONES: \nNOTA INFORMATIVA: El presente texto se presenta \u00fanicamente con fines informativos y de control de calidad. " + autoInciso.getValue().getObservacionesSesa());
             }
         }
		 if(stringBuilder.toString() != null && !stringBuilder.toString().equals("")){
			 datosIncisoDTO.setObservaciones(stringBuilder.toString());
					 //.substring(0, stringBuilder.toString().length()-2));
		 }else{
			 datosIncisoDTO.setObservaciones("");
		 }

		 LogDeMidasInterfaz.log("Saliendo de setObservacionesIniciso", Level.INFO, null);
	}
	
	@SuppressWarnings("unused")
	private List<DatosCoberturasDTO> llenarCoberturasList(Collection<BitemporalCoberturaSeccion> coberturasList,
			Short numeroDeAsientosDelVehiculo, EstiloVehiculoId estiloVehiculoId,  Short idTcMoneda, Short modelo, boolean imprimirUMA){
			return llenarCoberturasList(coberturasList, numeroDeAsientosDelVehiculo, estiloVehiculoId, idTcMoneda, modelo, imprimirUMA, false);
	}

	/**
	 * 
	 * @param coberturasList
	 * @param numeroDeAsientosDelVehiculo
	 * @param estiloVehiculoId 
	 * @param modelo 
	 * @param idTcMoneda 
	 * @param  
	 * @return
	 */
	private List<DatosCoberturasDTO> llenarCoberturasList(Collection<BitemporalCoberturaSeccion> coberturasList,
			Short numeroDeAsientosDelVehiculo, EstiloVehiculoId estiloVehiculoId,  Short idTcMoneda, Short modelo, boolean imprimirUMA, boolean esSeguroObligatorio){
		List<DatosCoberturasDTO> dataSourceCoberturas = new ArrayList<DatosCoberturasDTO>();
		for(BitemporalCoberturaSeccion bitempCoberturaSeccion : coberturasList){
			CoberturaSeccion coberturaSeccion = bitempCoberturaSeccion.getEmbedded();

			CoberturaDTO coberturaDTO = bitempCoberturaSeccion.getContinuity().getCoberturaDTO();

			Short claveTipoDeducible = coberturaSeccion.getClaveTipoDeducible();
			String strClaveTipoDeducible = 
				claveTipoDeducible != null ? claveTipoDeducible.toString() : null;
			//coberturaCotizacionDTO.setDescripcionDeducible(getDescripcionDeducible(strClaveTipoDeducible));
			coberturaSeccion.setDescripcionDeducible(getDescripcionDeducible(strClaveTipoDeducible));
			
			if (imprimirUMA && (coberturaSeccion.getDescripcionDeducible()!=null && coberturaSeccion.getDescripcionDeducible().indexOf(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF) >=0)){
				coberturaSeccion.setDescripcionDeducible(coberturaSeccion.getDescripcionDeducible().replace(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF,
							ImpresionesServiceImpl.TIPO_VALOR_SA_UMA));
			 }
			
			DatosCoberturasDTO datosCoberturasDTO = new DatosCoberturasDTO();
			datosCoberturasDTO.setSumaAsegurada(getSumaAsegurada(coberturaDTO, numeroDeAsientosDelVehiculo, coberturaSeccion, estiloVehiculoId,
					idTcMoneda, modelo, esSeguroObligatorio));
			datosCoberturasDTO.setDeducible(getDeducibleConfig(coberturaDTO, coberturaSeccion));	 
			 
			datosCoberturasDTO.setRiesgo(coberturaDTO.getNumeroSecuencia().intValue());
			datosCoberturasDTO.setNombreCobertura(coberturaDTO.getNombreComercial());
			//datosCoberturasDTO.setPrimaCobertura(coberturaCotizacionDTO.getValorPrimaNeta());
			datosCoberturasDTO.setPrimaCobertura(coberturaSeccion.getValorPrimaNeta());
			
			//Valida datos a mostrar
			 if(coberturaDTO.getClaveMostrarPolDescripcion() != null && coberturaDTO.getClaveMostrarPolDescripcion().equals(UtileriasWeb.STRING_CERO)){
				 datosCoberturasDTO.setNombreCobertura(" ");
			 }
			 if(coberturaDTO.getClaveMostrarPolDeducible() != null && coberturaDTO.getClaveMostrarPolDeducible().equals(UtileriasWeb.STRING_CERO)){
				 datosCoberturasDTO.setDeducible(" ");
			 }
			 if(coberturaDTO.getClaveMostrarPolSumaAsegurada() != null && coberturaDTO.getClaveMostrarPolSumaAsegurada().equals(UtileriasWeb.STRING_CERO)){
				 datosCoberturasDTO.setSumaAsegurada(" ");
			 }
			 if(coberturaDTO.getClaveMostrarPolPrimaNeta() != null && coberturaDTO.getClaveMostrarPolPrimaNeta().equals(UtileriasWeb.STRING_CERO)){
				 datosCoberturasDTO.setPrimaCobertura(0.0);
			 }
			 datosCoberturasDTO.setFechaRegistro(coberturaDTO.getFechaRegistro());
			 datosCoberturasDTO.setNumeroRegistro(coberturaDTO.getNumeroRegistro());
			 datosCoberturasDTO.setIdToCobertura(coberturaDTO.getIdToCobertura());
			 
			 if (imprimirUMA && (datosCoberturasDTO.getSumaAsegurada()!=null && datosCoberturasDTO.getSumaAsegurada().indexOf(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF) >=0)){
				 datosCoberturasDTO.setSumaAsegurada(datosCoberturasDTO.getSumaAsegurada().replace(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF,
							ImpresionesServiceImpl.TIPO_VALOR_SA_UMA));
			 }
			 
			 if (imprimirUMA && (datosCoberturasDTO.getDeducible()!=null && datosCoberturasDTO.getDeducible().indexOf(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF) >=0)){
				 datosCoberturasDTO.setDeducible(datosCoberturasDTO.getDeducible().replace(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF,
							ImpresionesServiceImpl.TIPO_VALOR_SA_UMA));
			 }
			
			dataSourceCoberturas.add(datosCoberturasDTO);
		}		
		
		return dataSourceCoberturas;		
	}
	
	private String getDeducibleConfig(CoberturaDTO coberturaDTO,
			CoberturaSeccion coberturaSeccion) {
		CatalogoValorFijoDTO catalogoValorFijo = null;
		String deducible = "";
		if(coberturaSeccion.getClaveTipoDeducible() != null){
			 catalogoValorFijo = 
					entidadService.findById(CatalogoValorFijoDTO.class, new CatalogoValorFijoId(
							CatalogoValorFijoDTO.IDGRUPO_POSIBLES_DEDUCIBLES, Integer.valueOf(coberturaDTO.getClaveTipoDeducible())));
		 }			 
		 if(coberturaSeccion.getClaveTipoDeducible() == null || coberturaSeccion.getClaveTipoDeducible().intValue() == 0){				 
			 deducible = catalogoValorFijo != null ? catalogoValorFijo.getDescripcion() : "" ;					 
		 }else if(coberturaSeccion.getClaveTipoDeducible().intValue() == CoberturaDTO.CLAVE_TIPO_DEDUCIBLE_PCTE){
			 deducible = coberturaSeccion.getPorcentajeDeducible() != null ? 
					coberturaSeccion.getPorcentajeDeducible().intValue() + " %" : "";
		 }else{
			 deducible = coberturaSeccion.getValorDeducible() != null ? 
					 coberturaSeccion.getValorDeducible().intValue() + 
					 (catalogoValorFijo != null ? " " + catalogoValorFijo.getDescripcion() : "") : "";				 
		}
		 
		return deducible;
	}
	
	@SuppressWarnings("unused")
	private String getSumaAsegurada(CoberturaDTO coberturaDTO, Short numeroDeAsientosDelVehiculo, 
			CoberturaSeccion coberturaSeccion, EstiloVehiculoId estiloVehiculoId, Short idTcMoneda, Short idModelo) {
		return getSumaAsegurada(coberturaDTO, numeroDeAsientosDelVehiculo, coberturaSeccion, estiloVehiculoId, idTcMoneda, idModelo, false);
	}

	private String getSumaAsegurada(CoberturaDTO coberturaDTO, Short numeroDeAsientosDelVehiculo, 
			CoberturaSeccion coberturaSeccion, EstiloVehiculoId estiloVehiculoId, Short idTcMoneda, Short idModelo, boolean esSeguroObligatorio) {
		 String valorSumaAseguradaStr = "";
		 switch(Integer.parseInt(coberturaDTO.getClaveFuenteSumaAsegurada())){
		 case CoberturaDTO.VALOR_PROPORCIONADO:
		 case CoberturaDTO.VALOR_FACTURA:
		 case CoberturaDTO.VALOR_CONVENIDO:
			 NumberFormat nf = NumberFormat.getCurrencyInstance();
			 Double valorSumaAsegurada = coberturaSeccion.getValorSumaAsegurada();
			 if(valorSumaAsegurada == null){
				 valorSumaAsegurada = 0d;
			 }
			 LogDeMidasInterfaz.log("valorSumaAsegurada => " + valorSumaAsegurada, Level.INFO, null);
			 if(CoberturaDTO.GASTOS_MEDICOS_NOMBRE_COMERCIAL.equals(coberturaDTO.getNombreComercial())){
				 valorSumaAsegurada = (esSeguroObligatorio)? valorSumaAsegurada : valorSumaAsegurada * numeroDeAsientosDelVehiculo;
				 valorSumaAseguradaStr = nf.format(valorSumaAsegurada);
			 }else if(CoberturaDTO.NOMBRE_LIMITE_RC_VIAJERO.equals(coberturaDTO.getNombreComercial())){
				 valorSumaAseguradaStr = coberturaSeccion.getDiasSalarioMinimo()+" UMA";
			 }else{
				 valorSumaAseguradaStr = nf.format(valorSumaAsegurada);
			 }
			 break;
		 case CoberturaDTO.VALOR_COMERCIAL:
			 valorSumaAseguradaStr = CoberturaDTO.DESCRIPCION_TIPO_SA_VALOR_COMERCIAL;
			 break;
		 
		 case CoberturaDTO.VALOR_AMPARADA:
			 valorSumaAseguradaStr = CoberturaDTO.DESCRIPCION_TIPO_SUMA_ASEGURADA_AMPARADA;
			 break;
		 case CoberturaDTO.VALOR_CARATULA:
			 ModeloVehiculoId id = new ModeloVehiculoId();
			 id.setClaveEstilo(estiloVehiculoId.getClaveEstilo());
			 id.setClaveTipoBien(estiloVehiculoId.getClaveTipoBien());
			 id.setIdVersionCarga(estiloVehiculoId.getIdVersionCarga());
			 id.setIdMoneda(new BigDecimal(idTcMoneda));
			 id.setModeloVehiculo(idModelo);
			 ModeloVehiculoDTO modelo = entidadService.findById(ModeloVehiculoDTO.class, id);
			 if(modelo!=null && modelo.getValorCaratula()!=null){
				 valorSumaAseguradaStr = UtileriasWeb.formatoMoneda(modelo.getValorCaratula());
			 }
			 break;			 
		  default:
			  valorSumaAseguradaStr = "";
			break;
		 }
		return valorSumaAseguradaStr;
	}

	private String getDescripcionDeducible(String tipoDeducible) {
		String descripcion = "";
		int valorTipoDeducible;
		if (tipoDeducible != null) {
			valorTipoDeducible = Integer.valueOf(tipoDeducible);
			for (CatalogoValorFijoDTO valor : posiblesDeducibles) {
				if (valor.getId().getIdDato() == valorTipoDeducible) {
					descripcion = valor.getDescripcion();
					break;
				}
			}
		}
		return descripcion;
	}
	
	private void setTipoCargaInciso(BitemporalInciso bitemporalInciso, DatosIncisoDTO datosIncisoDTO, 
			DateTime validoEn, DateTime recordFrom, Short claveTipoEndoso,BitemporalAutoInciso autoInciso){
		//Obtiene Tipo de Carga    
		List<ControlDinamicoRiesgoDTO> lista = configDatoIncisoBitemporalService.obtieneDescripcionDatosImpresion(bitemporalInciso.getContinuity().getId(),
				validoEn, recordFrom);
		for(ControlDinamicoRiesgoDTO dato: lista){
    		if(dato.getDescripcionNivel().equals(CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL.toUpperCase())
    				&& dato.getValor() != null &&  !dato.getValor().isEmpty()){
				datosIncisoDTO.setTipoCarga(dato.getValor());
    		}
    	}
	}
	
	private String getFolioPoliza(PolizaDTO poliza){
		String folio = poliza.getCotizacionDTO().getFolio();
		if(folio != null && folio.trim().length() > 0){
			String[] folioSplit = folio.trim().split("_");
			if(!(folioSplit[0].equals(FoliosPermitidosImpresion.AUP.toString()) || folioSplit[0].equals(FoliosPermitidosImpresion.TSSO.toString()))){
				folio = "";
			}
		} else {
			folio = "";
		}
		
		return folio;
	}
	
	public MonedaDTO getMoneda() {
		return moneda;
	}

	public void setMoneda(MonedaDTO moneda) {
		this.moneda = moneda;
	}

	public FormaPagoDTO getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(FormaPagoDTO formaPago) {
		this.formaPago = formaPago;
	}

	public DomicilioImpresion getDomicilioCliente() {
		return domicilioCliente;
	}

	public void setDomicilioCliente(DomicilioImpresion domicilioCliente) {
		this.domicilioCliente = domicilioCliente;
	}

	public ClienteGenericoDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteGenericoDTO cliente) {
		this.cliente = cliente;
	}
	
	public List<CoberturaCotizacionDTO> getCoberturasCotizacionList() {
		return coberturasCotizacionList;
	}

	public void setCoberturasCotizacionList(
			List<CoberturaCotizacionDTO> coberturasCotizacionList) {
		this.coberturasCotizacionList = coberturasCotizacionList;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setDomicilioFacadeRemote(DomicilioFacadeRemote domicilioFacadeRemote) {
		this.domicilioFacadeRemote = domicilioFacadeRemote;
	}
	
	@EJB
	public void setClienteFacateRemote(ClienteFacadeRemote cfr) {
		this.clienteFacadeRemote = cfr;
	}
	
	@EJB
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@EJB
	public void setEntidadBitemporalService(EntidadBitemporalService ebs) {
		this.entidadBitemporalService = ebs;
	}
	
	@EJB
	public void setIncisosBitemporalService(IncisoBitemporalService ibs) {
		this.incisosBitemporalService = ibs;
	}
	
	@EJB
	public void setConfigDatoIncisoBitemporalService(
			ConfiguracionDatoIncisoBitemporalService cdis) {
		this.configDatoIncisoBitemporalService = cdis;
	}
	
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	@EJB
	public void setEntidadContinuityService(EntidadContinuityService entidadContinuityService){
		this.entidadContinuityService = entidadContinuityService;
	}
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public CondicionEspecialService getCondicionEspecialService() {
		return condicionEspecialService;
	}

	public void setCondicionEspecialService(
			CondicionEspecialService condicionEspecialService) {
		this.condicionEspecialService = condicionEspecialService;
	}

	public FolioReexpedibleService getFolioReexpedibleService() {
		return folioReexpedibleService;
	}

	public void setFolioReexpedibleService(
			FolioReexpedibleService folioReexpedibleService) {
		this.folioReexpedibleService = folioReexpedibleService;
	}
	
}
