/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaBancaGerenciaDao;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaBancaGerenciaDaoImpl;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaGerencia;
import mx.com.afirme.midas2.service.compensaciones.CaBancaGerenciaService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless

public class CaBancaGerenciaServiceImpl  implements CaBancaGerenciaService {
	
	@EJB
	private CaBancaGerenciaDao caBancaGerencia;
	private static final Logger LOG = LoggerFactory.getLogger(CaBancaGerenciaDaoImpl.class);

    public void save(CaBancaGerencia entity) {   
	    try {
	    	LOG.info(">> save()");
	    	caBancaGerencia.save(entity);	  
	    	LOG.info("<< save()");
	    } catch (RuntimeException re) {	 
	    	LOG.error("Información del Error", re);
	        throw re;
	    }
    }
    

    public void delete(CaBancaGerencia entity) { 
    	try {
    		LOG.info(">> delete()");
    		caBancaGerencia.delete(entity);
    		LOG.info("<< delete()");
    	} catch (RuntimeException re) {
    		LOG.error("Información del Error", re);
    		throw re;
    	}
    }
    

    public CaBancaGerencia update(CaBancaGerencia entity) {   
        try {
        	LOG.info(">> update()");
	        CaBancaGerencia result = caBancaGerencia.update(entity);
	        LOG.info("<< update()");
	        return result;
	    } catch (RuntimeException re) {
	    	LOG.error("Información del Error", re);
            throw re;
	    }
    }
    
    public CaBancaGerencia findById( Long id) {
    	LOG.info(">> findById()");
	    try {
            CaBancaGerencia instance = caBancaGerencia.findById(id);
           	LOG.info("<< findById()");
            return instance;
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
        }
        return null;
    }   


    public List<CaBancaGerencia> findByProperty(String propertyName, final Object value) {    	
		try {
			return caBancaGerencia.findByProperty(propertyName, value);
		} catch (RuntimeException re) {
			
			return null;
		}
	}			

	
	public List<CaBancaGerencia> findAll() {	
		try {
			return caBancaGerencia.findAll();
		} catch (RuntimeException re) {
			return null;
		}
	}
	
}