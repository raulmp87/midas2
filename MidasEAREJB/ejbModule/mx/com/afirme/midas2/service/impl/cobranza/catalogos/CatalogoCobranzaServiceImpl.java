package mx.com.afirme.midas2.service.impl.cobranza.catalogos;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.cobranza.catalogos.RelacionGrupoAgenteDTO;
import mx.com.afirme.midas2.dto.cobranza.catalogos.RelacionGrupoAgenteIdDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.cobranza.catalogos.CatalogoCobranzaService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

@Stateless
public class CatalogoCobranzaServiceImpl implements CatalogoCobranzaService{

	@Override
	public void baja(RelacionGrupoAgenteDTO relacionGrupoAgente) {
		relacionGrupoAgente = entidadService.findById(RelacionGrupoAgenteDTO.class, 
				relacionGrupoAgente.getId());
		relacionGrupoAgente.setEstatus(0);
		
		entidadService.save(relacionGrupoAgente);
	}

	@Override
	public List<RelacionGrupoAgenteDTO> getListaRelacionGrupoAgente(RelacionGrupoAgenteDTO filtro) {
		List<RelacionGrupoAgenteDTO> list = new ArrayList<RelacionGrupoAgenteDTO>(1);
		Map<String, Object> filter = new HashMap<String, Object>(1);
		filter.put("estatus", 1);
		RelacionGrupoAgenteIdDTO id = filtro.getId();
		if(id!=null){
			if(id.getNumeroPoliza()!=null){
				filter.put("id.numeroPoliza",id.getNumeroPoliza());
			}
			if(id.getCentroEmisor()!=null){
				filter.put("id.centroEmisor",id.getCentroEmisor());			
			}
			if(id.getNumeroRenovacion()!=null){
				filter.put("id.numeroRenovacion",id.getNumeroRenovacion());		
			}
	
			if(filtro.getNumeroCuenta()!=null){
				filter.put("numeroCuenta",filtro.getNumeroCuenta());		
			}
		}
		list = entidadService.findByProperties(RelacionGrupoAgenteDTO.class,filter);
		return list;
	}

	@Override
	public void save(RelacionGrupoAgenteDTO relacionGrupoAgente) {
		relacionGrupoAgente.setUsuarioModifica(usuarioService.getUsuarioActual().getNombreUsuario());
		relacionGrupoAgente.setFechaModifica(new Date());
		entidadService.save(relacionGrupoAgente);
	}
	
	@EJB
	public EntidadService entidadService;
	
	@EJB(beanName = "UsuarioServiceDelegate") 
	public UsuarioService usuarioService;
	
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	

}
