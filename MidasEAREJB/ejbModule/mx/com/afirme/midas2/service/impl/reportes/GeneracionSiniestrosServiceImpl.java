package mx.com.afirme.midas2.service.impl.reportes;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.danios.reportes.reportercs.ReporteRCSDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.service.reportes.GeneracionSiniestrosService;
import mx.com.afirme.midas2.util.MidasException; 

@Stateless    
public class GeneracionSiniestrosServiceImpl implements GeneracionSiniestrosService{
			
	private String getStringDate( Date d ){
		
		String strDate;			
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        strDate = formatter.format(d);
		
		return strDate;
	}
	
	@Override
	public InputStream exportarReporte( Date fInicial, Date fFinal) throws MidasException {
		InputStream is;
		StoredProcedureHelper storedHelper;
		StringBuilder descripcionParametros = new StringBuilder();
		
		String nombreSP = "SEYCOS.PKG_ARCHIVOS_RESERVAS.SP_SINIESTROS_REPORTE";
		
		try {
			
			String[] nombreParametros = {"FINI", 
					"FFIN",
					"USUARIO"};
			Object[] valorParametros={getStringDate(fInicial),
					getStringDate(fFinal),
				0};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					"registro",
					"registro");
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append(", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log("Consultando reporte bases emision. SP a invocar: "+nombreSP+", Parametros: "+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log("Finaliza consulta reporte bases emision. SP invocado: "+nombreSP+", Parametros: "+descripcionParametros.toString()+
					"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);

			byte[] bytes;
			bytes = toArrayByte(listaResultado);
			is = new ByteArrayInputStream(bytes);
	        
	        return is;
	
		} catch (Exception e) {
			throw new RuntimeException("Hubo un error exportarReporte VIGOR :" +  e.getMessage() + " " + e.getCause(), e);
	    } 
	}
	
	private byte[] toArrayByte( List<ReporteRCSDTO> list ) throws Exception {
		
		byte[] bytes;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		StringBuilder sb = new StringBuilder();
		out.writeBytes("UEN|MONE|OFI|AÑO|NUM_SINIESTRO|NUM_POLIZA|RST|INICIO_VIGENCIA|F_VTO|STATUS|F_REPORTE|F_OCURRIDO|F_REGISTRO|FECHA_MOVIMIENTO|COD_MOV|MONTO|MONTO_RET|LOC|EXT|SISTEMA|ESTINI|AJUSTEPOS|AJUSTENEG|ESTINI_RET|APOS_RET|ANEG_RET|TIPERRICX|RAMO\n");
		if(list != null)
			for (ReporteRCSDTO item : list) {
				sb.append(item.getRegistro()+System.getProperty("line.separator"));
			}
		out.writeBytes(sb.toString());
		bytes = baos.toByteArray();
		
		return bytes; 
	}
	
}
