package mx.com.afirme.midas2.service.impl.siniestros.configuracion.horario.base;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.service.impl.siniestros.catalogo.CatalogoSiniestroServiceImpl;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService.CatalogoFiltro;

@SuppressWarnings("hiding")
public class ServicioCatalogoBase<K extends CatalogoFiltro, E extends Entidad, I extends Serializable>
		extends CatalogoSiniestroServiceImpl {

	private Class<E>	entityClass;

	@Override
	public <K extends CatalogoFiltro, E extends Entidad> List<E> buscar(
			Class<E> entidad, K filtro) {
		List<E> listaEntidades = catalogoSiniestroDAO.buscar(entidad, filtro);
		return listaEntidades;
	}

	@Override
	public <E extends Entidad, I> E obtener(Class<E> entityClass, I id) {
		return entidadService.findById(entityClass, id);
	}

	@Override
	public <E extends Entidad> void salvar(E entidad) {
		entidadService.save(entidad);
	}

	public List<E> buscar(K filtro) {
		List<E> listaEntidades = this.buscar(entityClass, filtro);
		return listaEntidades;
	}

	public E obtener(I id) {
		E entidad = this.obtener(entityClass, id);
		return entidad;
	}

	public E guardar(E entidad) {
		return entidadService.save(entidad);
	}

}
