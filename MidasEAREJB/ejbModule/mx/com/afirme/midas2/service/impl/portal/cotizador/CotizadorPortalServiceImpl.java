package mx.com.afirme.midas2.service.impl.portal.cotizador;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionDao;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.portal.cotizador.Cobertura;
import mx.com.afirme.midas2.domain.portal.cotizador.Estado;
import mx.com.afirme.midas2.domain.portal.cotizador.Vehiculo;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.portal.cotizador.CotizadorPortalService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


@Stateless
public class CotizadorPortalServiceImpl  implements CotizadorPortalService{
	
	private static final Logger LOG = Logger.getLogger(CotizadorPortalService.class);
	@PersistenceContext
	private EntityManager entityManager;
	@EJB
	private CotizacionFacadeRemote cotizacionFacadeRemote;
	@EJB
	private EntidadService entidadService;
	@EJB
	private CoberturaService coberturaService;
	@EJB
	private IncisoService incisoService;
	@EJB
	private NegocioPaqueteSeccionDao negocioPaqueteSeccionDao;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Vehiculo> getVehiculos(String descripcionVehiculo) throws ApplicationException {
		if(descripcionVehiculo == null || descripcionVehiculo.isEmpty())
			throw new ApplicationException("Debe proporcionar la descripci\u00F3n de un veh\u00EDculo.");
		
		String[] parametros = null;
		
		if (descripcionVehiculo != null) {
			parametros = descripcionVehiculo.split(" ");
		}
	
		final StringBuilder sb = new StringBuilder();
		sb.append("SELECT distinct vehiculo.descripcionVehiculo FROM (SELECT ");
		sb.append("CONCAT(CONCAT(CONCAT(tarifa.modelovehiculo, ' '), CONCAT(tarifa.tipovehiculo, ' ')),");
		sb.append("CONCAT(CONCAT(tarifa.marcavehiculo, ' '), tarifa.descripciontarifa)) descripcionVehiculo ");
		sb.append("from midas.tctarifas tarifa) vehiculo ");

		if(parametros != null) {
			final StringBuilder sbWhere = new StringBuilder();
			
			for (String parametro: parametros) {
				if(!sbWhere.toString().isEmpty())
					sbWhere.append("AND ");
				sbWhere.append("UPPER(vehiculo.descripcionVehiculo) LIKE UPPER('%");
				sbWhere.append(parametro.trim());
				sbWhere.append("%') "); 
			}
			
			if(!sbWhere.toString().isEmpty()) {
				sb.append("where ");
				sb.append(sbWhere);
			}
		}

		sb.append("group by vehiculo.descripcionVehiculo ");
		sb.append("order by vehiculo.descripcionVehiculo");
				
		final Query query = entityManager.createNativeQuery(sb.toString());
		query.setMaxResults(20);
		Object result  = query.getResultList();
		
		List<Vehiculo> vehiculos = new ArrayList<Vehiculo>(1);
		if(result instanceof List)  {
			List<Object> listaResultados = (List<Object>) result;				
			for(Object object : listaResultados){
				Object singleResult = (Object) object;
				String descripcion = null;
				descripcion = (String)singleResult;
				
				Vehiculo vehiculo = new Vehiculo();
				vehiculo.setDescripcionVehiculo(descripcion);
				vehiculos.add(vehiculo);
			}
		}
		
		return query.getResultList();
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Estado> getEstados(String estado)  throws ApplicationException {
		if(estado == null || estado.isEmpty())
			throw new ApplicationException("Debe proporcionar la descripci\u00F3n de un Estado.");
		
		final StringBuilder sb = new StringBuilder();
		sb.append("select distinct model.estado as nombreEstado from TarifasDTO model ");
		sb.append("where UPPER(model.estado) LIKE UPPER('%");
		sb.append(estado); 
		sb.append("%') ");
		sb.append("group by model.estado ");
		sb.append("order by model.estado asc ");
				
		final Query query = entityManager.createQuery(sb.toString(), Estado.class);
	
		return query.getResultList();
	}
	
	@Override
	public List<String> getPaquetes() {
		List<String> paquetes = new ArrayList<String>(1);
		paquetes.add("AMPLIA");
		paquetes.add("BASICO");
		paquetes.add("LIMITADA");
		paquetes.add("ORO AFIRME");
		paquetes.add("PLATA AFIRME");
		paquetes.add("PLATINO AFIRME");

		return paquetes;
	}
	
	@Override
	public Long getPaquete(String descripcionPaquete) throws ApplicationException {
		final StringBuilder sb = new StringBuilder();
		sb.append("SELECT paquete.idPaquete FROM midas.tcpaquete paquete where UPPER(paquete.descripcionPaquete) = UPPER(?)");
		
		final Query query = entityManager.createNativeQuery(sb.toString());
		query.setParameter(1, descripcionPaquete);
		
		Long id = null;
		try {
			Object singleResult = query.getSingleResult();
			if(singleResult instanceof BigDecimal){
				BigDecimal idBd = (BigDecimal)singleResult;
				id = idBd.longValue();
			} else if (singleResult instanceof Long){
				id = (Long)singleResult;
			} else if (singleResult instanceof Double){
				Double idBd = (Double)singleResult;
				id = idBd.longValue();
			}
		} catch(Exception e){
			LOG.error(e.getMessage(), e);
			throw new ApplicationException("No se pudo obtener el paquete.");
		}		
		
		return id;
	}
	
	@Override
	public Long getTarifa(String estado, String descripcionVehiculo) throws ApplicationException{
		final StringBuilder sb = new StringBuilder();
		sb.append("SELECT max(vehiculo.idtctarifas) FROM (SELECT tarifa.idtctarifas, tarifa.estado, ");
		sb.append("CONCAT(CONCAT(CONCAT(tarifa.modelovehiculo, ' '), CONCAT(tarifa.tipovehiculo, ' ')),");
		sb.append("CONCAT(CONCAT(tarifa.marcavehiculo, ' '), tarifa.descripciontarifa)) descripcionVehiculo ");
		sb.append("from midas.tctarifas tarifa where tarifa.estado = '");
		sb.append(estado); 
		sb.append("') vehiculo ");
		sb.append("where vehiculo.descripcionVehiculo = '");
		sb.append(descripcionVehiculo); 
		sb.append("'");
		
		final Query query = entityManager.createNativeQuery(sb.toString());
		Long id = null;
		
		try {
			Object singleResult = query.getSingleResult();
			if(singleResult instanceof BigDecimal){
				BigDecimal idBd = (BigDecimal)singleResult;
				id = idBd.longValue();
			} else if (singleResult instanceof Long){
				id = (Long)singleResult;
			} else if (singleResult instanceof Double){
				Double idBd = (Double)singleResult;
				id = idBd.longValue();
			} else if (singleResult instanceof String) {
				id = Long.valueOf((String)singleResult);
			}
		} catch(Exception e){
			LOG.error(e.getMessage(), e);
			throw new ApplicationException("No se pudo obtener la configuraci\u00F3n de tarifas para la cotizaci\u00F3n.");
		}	
		
		if (id == null) {
			throw new ApplicationException("No se encontr\u00F3 la tarifa para el Estado y veh\u00EDculo proporcionados.");
		}
		
		return id;
	}
	
	public CotizacionMovilDTO buscaCotizacion(Map<String,Object> params) throws ApplicationException {
		int index = 0;
		boolean setParameters = false;
		CotizacionMovilDTO cotizacionMovilDTO = new CotizacionMovilDTO();

		if (params != null && !params.isEmpty()) {
			final StringBuilder sb = new StringBuilder();
			sb.append("select model from CotizacionMovilDTO model ");
			sb.append("where ");
			
			setParameters = true;
			for (String property : params.keySet()) {
				String propertyMap = getValidProperty(property);
				sb.append("model." + property + "=:" + propertyMap + ",");
				index++;
			}
			sb.delete(sb.length()-1, sb.length());
			
			if(setParameters) {
				final Query query = entityManager.createQuery(sb.toString(), CotizacionMovilDTO.class);
				this.setQueryParameters(query, params);
				query.setMaxResults(1);
				
				try {
					Object object = query.getSingleResult();
					if (object != null)
						cotizacionMovilDTO = (CotizacionMovilDTO)object;
				} catch(Exception e){
					LOG.error(e.getMessage(), e);
					throw new ApplicationException("No fue posible obtener la cotizaci\u00F3n.");
				}
			}
		} else {
			throw new ApplicationException("Debe proporcionar un n\u00FAmero de cotizaci\u00F3n.");
		}

		return cotizacionMovilDTO;
	}
	
	public List<Cobertura> cargarDetalleCoberturas(BigDecimal idToCotizacion, String descripcionPaquete) throws ApplicationException {
		if(idToCotizacion == null)
			throw new ApplicationException("Debe proporcionar un n\u00FAmero de cotizaci\u00F3n.");
		
		if(descripcionPaquete == null || descripcionPaquete.isEmpty())
			throw new ApplicationException("Debe proporcionar la descripci\u00F3n de un paquete.");

		List<CoberturaCotizacionDTO>coberturaCotizacionList = new ArrayList<CoberturaCotizacionDTO>(1);
		List<Cobertura>coberturasList = new ArrayList<Cobertura>(1);
	
		CotizacionDTO cotizacion = cotizacionFacadeRemote.findById(idToCotizacion);
		if(cotizacion == null) {
			throw new ApplicationException("Debe proporcionar un n\u00FAmero v\u00E1lido de cotizaci\u00F3n.");
		}
				
		IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
		incisoCotizacionId.setIdToCotizacion(idToCotizacion);
		incisoCotizacionId.setNumeroInciso(BigDecimal.ONE);
		IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
		incisoCotizacion.setId(incisoCotizacionId);
		incisoCotizacion.setCotizacionDTO(cotizacion);
		incisoCotizacion =  entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacion.getId());
		
		Long idPaquete = this.getPaquete(descripcionPaquete);
		NegocioPaqueteSeccion negocioPaqueteSeccion = negocioPaqueteSeccionDao.getPorIdNegSeccionIdPaquete(
				new BigDecimal(incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId()), idPaquete);
		
		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
		estiloVehiculoId.valueOf(incisoCotizacion.getIncisoAutoCot().getEstiloId());
		
		if(incisoCotizacion.getIncisoAutoCot().getEstadoId() != null &&
				incisoCotizacion.getIncisoAutoCot().getEstadoId().trim().equals("")){
			incisoCotizacion.getIncisoAutoCot().setEstadoId(null);
		}
		
		if(incisoCotizacion.getIncisoAutoCot().getMunicipioId() != null &&
				incisoCotizacion.getIncisoAutoCot().getMunicipioId().trim().equals("")){
			incisoCotizacion.getIncisoAutoCot().setMunicipioId(null);
		}
		
		Long numeroSecuencia = new Long("1");
		numeroSecuencia = numeroSecuencia + incisoService.maxSecuencia(cotizacion.getIdToCotizacion());
		
		try {
			NegocioTipoPoliza negocioTipoPoliza = negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza();
			coberturaCotizacionList = coberturaService.getCoberturas(negocioTipoPoliza.getNegocioProducto().getNegocio().getIdToNegocio(), 
				negocioTipoPoliza.getNegocioProducto().getProductoDTO().getIdToProducto(), 
				negocioTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza(), 
				negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), 
				negocioPaqueteSeccion.getPaquete().getId(), incisoCotizacion.getIncisoAutoCot().getEstadoId(), incisoCotizacion.getIncisoAutoCot().getMunicipioId(), 
				cotizacion.getIdToCotizacion(), cotizacion.getIdMoneda().shortValue(), numeroSecuencia,
				estiloVehiculoId.getClaveEstilo(), incisoCotizacion.getIncisoAutoCot().getModeloVehiculo().longValue(), 
				new BigDecimal(incisoCotizacion.getIncisoAutoCot().getTipoUsoId()), null, null);
			
			for(CoberturaCotizacionDTO coberturas : coberturaCotizacionList) {
				if(coberturas.getClaveContrato().compareTo((short)1) == 0) {
					Cobertura cobertura = new Cobertura();
					cobertura.setNombreComercial(coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
					cobertura.setClaveContrato(coberturas.getClaveContrato());
					cobertura.setDescripcionSumaAsegurada("");
					
					if (coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(Cobertura.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO)) {
						if(coberturas.getValorSumaAseguradaMax() != 0.0 && coberturas.getValorSumaAseguradaMin() != 0.0) {
							if (coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().compareTo(Cobertura.RC_VIAJERO_AUT) == 0 ||
									coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().compareTo(Cobertura.RC_VIAJERO_BUS) == 0  ||
									coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().compareTo(Cobertura.RC_VIAJERO_CAM) == 0 ||
									coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().compareTo(Cobertura.RC_VIAJERO_MOT) == 0 ) {
								cobertura.setValorSumaAseguradaStr(coberturas.getValorSumaAsegurada() + coberturas.getDescripcionDeducible());
							} else {
								cobertura.setDescripcionSumaAsegurada("*Entre " +
										(coberturas.getValorSumaAseguradaMin() != null ? UtileriasWeb.formatoMoneda(coberturas.getValorSumaAseguradaMin()) : UtileriasWeb.formatoMoneda(0.0)) + " y " + 
										(coberturas.getValorSumaAseguradaMax() != null ? UtileriasWeb.formatoMoneda(coberturas.getValorSumaAseguradaMax()).toString() : UtileriasWeb.formatoMoneda(0.0)));
								cobertura.setValorSumaAseguradaStr(UtileriasWeb.formatoMoneda(coberturas.getValorSumaAsegurada()));
							}
						} else {
							if (coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().compareTo(Cobertura.RC_VIAJERO_AUT) == 0 ||
									coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().compareTo(Cobertura.RC_VIAJERO_BUS) == 0  ||
									coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().compareTo(Cobertura.RC_VIAJERO_CAM) == 0 ||
									coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().compareTo(Cobertura.RC_VIAJERO_MOT) == 0 ) {
								cobertura.setValorSumaAseguradaStr(coberturas.getValorSumaAsegurada() + coberturas.getDescripcionDeducible());
							} else {
								cobertura.setValorSumaAseguradaStr(UtileriasWeb.formatoMoneda(coberturas.getValorSumaAsegurada()));
							}
						}
					} else if(coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(Cobertura.CLAVE_FUENTE_SA_VALOR_COMERCIAL)){
						cobertura.setValorSumaAseguradaStr("Valor Comercial");
					} else if(coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(Cobertura.CLAVE_FUENTE_SA_VALOR_FACTURA)) {
						cobertura.setValorSumaAseguradaStr(coberturas.getValorSumaAsegurada() + " Valor Factura");
					} else if (coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(Cobertura.CLAVE_FUENTE_SA_VALOR_CONVENIDO)) {
						cobertura.setValorSumaAseguradaStr("Valor Convenido");
					} else if(coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(Cobertura.CLAVE_FUENTE_SA_VALOR_AMPARADA)) {
						cobertura.setValorSumaAseguradaStr("Amparada");
					}
					
					if (coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoDeducible().equals("0")) {
						cobertura.setValorDeducibleStr(coberturas.getDescripcionDeducible());
					} else if (coberturas.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoDeducible().equals(Cobertura.CLAVE_TIPO_DEDUCIBLE_PORCENTAJE)) {
						cobertura.setValorDeducibleStr(coberturas.getDescripcionDeducible() + " " +
								coberturas.getPorcentajeDeducible());
					} else {
						cobertura.setValorDeducibleStr(coberturas.getDescripcionDeducible() + " " +
								coberturas.getValorDeducible());
					}

					coberturasList.add(cobertura);
				}
			}
		} catch(Exception e){
			LOG.error(e.getMessage(), e);
			throw new ApplicationException("Error al cargar el detalle de la cotizaci\u00F3n, favor de intentarlo m\u00E1s tarde.");
		}
		
		return 	coberturasList;
	}
	
	@Override
	public BigDecimal getFormaPago(String descripcionFormaPago) throws ApplicationException{
		final StringBuilder sb = new StringBuilder();
		sb.append("select model.idFormaPago from FormaPagoDTO model ");
		sb.append("where model.descripcion = :descripcion");
					
		final Query query = entityManager.createQuery(sb.toString(), CotizacionMovilDTO.class);
		query.setParameter("descripcion", descripcionFormaPago);
		query.setMaxResults(1);

		BigDecimal id = null;
		
		try {
			Object singleResult = query.getSingleResult();
			if(singleResult instanceof BigDecimal) {
				id = (BigDecimal)singleResult;
			} else if (singleResult instanceof Long) {
				id = BigDecimal.valueOf((Long)singleResult);
			} else if (singleResult instanceof Double) {
				id = BigDecimal.valueOf((Double)singleResult);
			} else if (singleResult instanceof Integer) {
				id = BigDecimal.valueOf((Integer)singleResult);
			}
		} catch(Exception e){
			LOG.error(e.getMessage(), e);
			throw new ApplicationException("No se pudo obtener la forma de pago para la cotizaci\u00F3n.");
		}	
		
		if (id == null)
			throw new ApplicationException("No se pudo obtener la forma de pago para la cotizaci\u00F3n");
		
		return id;
	}
	
	private void setQueryParameters(Query entityQuery,
			Map<String, Object> parameters) {
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>) it
					.next();
			entityQuery.setParameter(pairs.getKey(), pairs.getValue());
		}
	}
	
	public static String getValidProperty(String property) {
		String validProperty = property;
		StringBuilder str = new StringBuilder("");
		if (property.contains(".")) {
			int i = 0;
			for (String word : property.split("\\.")) {
				if (i == 0) {
					word = word.toLowerCase();
				} else {
					word = StringUtils.capitalize(word);
				}
				str.append(word);
				i++;
			}
			validProperty = str.toString();
		}
		return validProperty;
	}
	
	@Override
	public List<String> getFormasPago() {
		List<String> formasPago = new ArrayList<String>(1);
		formasPago.add("ANUAL");
		formasPago.add("SEMESTRAL");
		formasPago.add("TRIMESTRAL");
		formasPago.add("MENSUAL");

		return formasPago;
	}
}
