package mx.com.afirme.midas2.service.impl.suscripcion.endoso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.afirme.recibos.WFacturaService;
import com.js.util.StringUtil;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas2.dao.catalogos.condicionesespeciales.CondicionEspecialDao;
import mx.com.afirme.midas2.dao.fuerzaventa.AgenteMidasDao;
import mx.com.afirme.midas2.dao.suscripcion.impresion.EdicionImpresionDAO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspInc;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionEndoso;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionEndosoId;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionInciso;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionIncisoCobertura;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionIncisoId;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionPoliza;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionPolizaId;
import mx.com.afirme.midas2.dto.CondicionesEspIncisosDTO;
import mx.com.afirme.midas2.dto.impresiones.ContenedorDatosImpresion;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaInciso;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaPoliza;
import mx.com.afirme.midas2.dto.impresiones.DatosCoberturasDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosIncisoDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosLineasDeNegocioDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosPolizaDTO;
import mx.com.afirme.midas2.dto.impresiones.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO.TipoCobro;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.inciso.IncisoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.documentos.anexos.DocumentoAnexoCoberturaService;
import mx.com.afirme.midas2.service.documentos.anexos.DocumentoAnexoSeccionService;
import mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.emision.ImpresionesServiceImpl.GestorReportes;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.service.suscripcion.endoso.ImpresionBitemporalService;
import mx.com.afirme.midas2.service.suscripcion.endoso.ImpresionEndosoService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.impresiones.GeneraPlantillaCartaRenovacionService;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.fill.JRGzipVirtualizer;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.commons.io.IOUtils;

import com.afirme.insurance.services.PrintReport.PrintReport;
import com.afirme.insurance.services.PrintReport.PrintReportServiceLocator;

import mx.com.afirme.midas2.util.FileUtils;

@Stateless
public class GeneraPlantillaReporteBitemporalServiceImpl implements
		GeneraPlantillaReporteBitemporalService {

	private static Logger LOG = Logger.getLogger(GeneraPlantillaReporteBitemporalServiceImpl.class);
	public static final String INDIVIDUAL = "31";
	public static final String FLOTILLA = "32";
	public static final Long NEGOCIO_AUTOPLAZO = new Long(92);
	public static final String UR_JRXML = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml";
	public static final String KEY_LOGO_IMG = "P_IMAGE_LOGO";
	public static final String KEY_SIGNATURES = "P_SIGNATURES";
	public static final String UR_JRXML_SO = "/mx/com/afirme/midas2/service/impl/movil/cotizador/enlaceafirme/jrxml";
	// Servicios utilizados

	protected ImpresionBitemporalService impresionBitemporalService;
	protected EntidadBitemporalService entidadBitemporalService;
	protected EntidadService entidadService;
	protected EntidadContinuityService entidadContinuityService;
	protected GeneraPlantillaCartaRenovacionService generaPlantillaCartaRenovacionService;
	protected ImpresionRecibosService impresionRecibosService;
	protected CondicionEspecialService condicionEspecialService;
	protected IncisoBitemporalService incisoBitemporalService;
	protected CondicionEspecialBitemporalService condicionEspecialBitemporalService;
	protected SeguroObligatorioService seguroObligatorioService;
	protected CondicionEspecialDao condicionEspecialDao;
	protected AgenteMidasDao agenteMidasDao;
	protected EdicionImpresionDAO edicionImpresionDao;
	protected UsuarioService usuarioService;
	protected ImpresionEndosoService impresionEndosoService;
	
	
	private SeccionFacadeRemote seccionFacade;
	private DocumentoAnexoSeccionService documentoAnexoSeccionService;
	private DocumentoAnexoCoberturaService documentoAnexoCoberturaService;
	private BitemporalCotizacion bitemporalCotizacionDatosAgente;
	
	private ListadoService listadoService;
	private Long idToNegocio;
	
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	

	private String getLeyendaISyF(Date fechaRegistro, String numeroRegistro){
		SimpleDateFormat sdf = new SimpleDateFormat("dd 'de' MMMMM 'del' yyyy", new Locale("es", "MX"));
		String leyendaCNSF = "En cumplimiento a lo dispuesto en el art\u00EDculo 202 de la Ley de Instituciones de Seguros y de Fianzas, la documentaci\u00F3n contractual " +
				"y la nota t\u00E9cnica que integran este producto de seguro, quedaron registradas ante la Comisi\u00F3n Nacional de Seguros y Fianzas, " +
				"a partir del d\u00EDa "+sdf.format(fechaRegistro)+", con el n\u00FAmero "+numeroRegistro+".";
		return leyendaCNSF;
	}
	
	@Override
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			Short claveTipoEndoso, boolean incluirCondicionesEsp) {
		boolean incluirCaratula = true;
		boolean incluirRecibo = true;
		boolean incluirIncisos = true;
		boolean incluirCertificadoRC = true;
		boolean incluirAnexosIncisos = true;

		TransporteImpresionDTO tiDTO = imprimirPoliza(idToPoliza, locale,
				validOn, recordFrom, incluirCaratula, incluirRecibo,
				incluirIncisos, incluirCertificadoRC, incluirAnexosIncisos,
				claveTipoEndoso, incluirCondicionesEsp);
		return tiDTO;
	}

	@Override
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirCertificadoRC, boolean incluirAnexosIncisos,
			int incisoInicial, int incisoFinal, Short claveTipoEndoso,
			Boolean esSituacionActual, boolean incluirCondicionesEsp) {
		//
		boolean incluirTodosLosIncisos = false;
		boolean incluirReferenciasBancarias = false;
		
		return imprimirPoliza(idToPoliza, locale, validOn, recordFrom,
				incluirCaratula, incluirRecibo, incluirTodosLosIncisos,
				incluirCertificadoRC, incluirAnexosIncisos,incluirReferenciasBancarias, incisoInicial,
				incisoFinal, claveTipoEndoso, esSituacionActual,
				incluirCondicionesEsp,true);
	}

	@Override
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirIncisos, boolean incluirCertificadoRC,
			boolean incluirAnexosIncisos, Short claveTipoEndoso,
			boolean incluirCondicionesEsp) {
	
		boolean incluirReferenciasBancarias = false;
		TransporteImpresionDTO tiDTO = imprimirPoliza(idToPoliza, locale,
				validOn, recordFrom, incluirCaratula, incluirRecibo,
				incluirIncisos, incluirCertificadoRC, incluirAnexosIncisos,incluirReferenciasBancarias, 0,
				0, claveTipoEndoso, false, incluirCondicionesEsp,true);

		return tiDTO;
	}
	
	@Override
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirIncisos, boolean incluirCertificadoRC,
			boolean incluirAnexosIncisos, Short claveTipoEndoso,
			boolean incluirCondicionesEsp,boolean incluirReferenciasBancarias) {
		//
		TransporteImpresionDTO tiDTO = imprimirPoliza(idToPoliza, locale,
				validOn, recordFrom, incluirCaratula, incluirRecibo,
				incluirIncisos, incluirCertificadoRC, incluirAnexosIncisos,incluirReferenciasBancarias, 0,
				0, claveTipoEndoso, false, incluirCondicionesEsp,true);

		return tiDTO;
	}
	
	/**
	 * Imprimir poliza
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param recordFrom
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirIncisos
	 * @param incluirCertificadoRC
	 * @param incluirAnexosIncisos
	 * @param claveTipoEndoso
	 * @param incluirCondicionesEsp
	 * @param incluirReferenciasBancarias
	 * @param aviso
	 * @param derechos
	 * @return
	 */
	@Override
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirIncisos, boolean incluirCertificadoRC,
			boolean incluirAnexosIncisos, Short claveTipoEndoso,
			boolean incluirCondicionesEsp,boolean incluirReferenciasBancarias, boolean aviso, boolean derechos) {
		//
		TransporteImpresionDTO tiDTO = imprimirPoliza2(idToPoliza, locale,
				validOn, recordFrom, incluirCaratula, incluirRecibo,
				incluirIncisos, incluirCertificadoRC, incluirAnexosIncisos,incluirReferenciasBancarias, 0,
				0, claveTipoEndoso, false, incluirCondicionesEsp,true, aviso, derechos);

		return tiDTO;
	}
	
	@Override
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirIncisos, boolean incluirCertificadoRC,
			boolean incluirAnexosIncisos, boolean incluirReferenciasBancarias, Short claveTipoEndoso,
			boolean incluirCondicionesEsp,boolean incluirCartaCargo) {
		TransporteImpresionDTO tiDTO = imprimirPoliza(idToPoliza, locale,
				validOn, recordFrom, incluirCaratula, incluirRecibo,
				incluirIncisos, incluirCertificadoRC, incluirAnexosIncisos,incluirReferenciasBancarias, 0,
				0, claveTipoEndoso, false, incluirCondicionesEsp,incluirCartaCargo);

		return tiDTO;
	}

	/**
	 * Produce una impresión de la poliza de acuerdo a la configuración de los
	 * parametros recibidos.
	 * 
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirTodosLosIncisos
	 * @param incluirCertificadoRC
	 * @param incluirAnexosIncisos
	 * @param  incluirReferenciasBancarias
	 * @param incisoInicial
	 * @param incisoFinal
	 * @return
	 */
	private TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirTodosLosIncisos, boolean incluirCertificadoRC,
			boolean incluirAnexosIncisos,boolean incluirReferenciasBancarias, int incisoInicial, int incisoFinal,
			Short claveTipoEndoso, Boolean esSituacionActual,
			boolean incluirCondicionesEsp,boolean incluirCartaCargo) {
		
		TransporteImpresionDTO tiDTO = imprimirPoliza(idToPoliza, locale,
				validOn, recordFrom, incluirCaratula, incluirRecibo,
				incluirTodosLosIncisos, incluirCertificadoRC, incluirAnexosIncisos,incluirReferenciasBancarias, incisoInicial,
				incisoFinal, claveTipoEndoso, esSituacionActual, incluirCondicionesEsp, false, false,incluirCartaCargo);

		return tiDTO;
	}
	
	/**
	 * Produce una impresión de la poliza de acuerdo a la configuración de los
	 * parametros recibidos.
	 * 
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirTodosLosIncisos
	 * @param incluirCertificadoRC
	 * @param incluirAnexosIncisos
	 * 	 * @param  incluirReferenciasBancarias
	 * @param incisoInicial
	 * @param incisoFinal
	 * @param aviso
	 * @param derechos
	 * @return
	 */
	private TransporteImpresionDTO imprimirPoliza2(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirTodosLosIncisos, boolean incluirCertificadoRC,
			boolean incluirAnexosIncisos,boolean incluirReferenciasBancarias, int incisoInicial, int incisoFinal,
			Short claveTipoEndoso, Boolean esSituacionActual,
			boolean incluirCondicionesEsp, boolean incluirCartaCargo, boolean aviso, boolean derechos) {
		
		TransporteImpresionDTO tiDTO = imprimirPoliza(idToPoliza, locale,
				validOn, recordFrom, incluirCaratula, incluirRecibo,
				incluirTodosLosIncisos, incluirCertificadoRC, incluirAnexosIncisos,incluirReferenciasBancarias, incisoInicial,
				incisoFinal, claveTipoEndoso, esSituacionActual, incluirCondicionesEsp, false, false, incluirCartaCargo, aviso, derechos);

		return tiDTO;
	}
	
	/**
	 * Produce una impresión de la poliza de acuerdo a la configuración de los
	 * parametros recibidos.
	 * 
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirTodosLosIncisos
	 * @param incluirCertificadoRC
	 * @param incluirAnexosIncisos
	 * @param incisoInicial
	 * @param incisoFinal
	 * @return
	 */
	@Override
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirTodosLosIncisos, boolean incluirCertificadoRC,
			boolean incluirAnexosIncisos, boolean incluirReferenciasBancarias, int incisoInicial, int incisoFinal,
			Short claveTipoEndoso, Boolean esSituacionActual,
			boolean incluirCondicionesEsp, boolean incluirEdicionPoliza, boolean imprimirEdicionInciso,
			boolean incluirCartaCargo) {
		
		TransporteImpresionDTO tiDTO = imprimirPoliza(idToPoliza,
				locale, validOn, recordFrom,
				incluirCaratula, incluirRecibo,
				incluirTodosLosIncisos, incluirCertificadoRC,
				incluirAnexosIncisos, incluirReferenciasBancarias, incisoInicial, incisoFinal,
				claveTipoEndoso, esSituacionActual,
				incluirCondicionesEsp, incluirEdicionPoliza, imprimirEdicionInciso,  incluirCartaCargo, false, false);
		
		return tiDTO;
	}

	
	/**
	 * Produce una impresión de la poliza de acuerdo a la configuración de los
	 * parametros recibidos.
	 * 
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirTodosLosIncisos
	 * @param incluirCertificadoRC
	 * @param incluirAnexosIncisos
	 * @param incisoInicial
	 * @param incisoFinal
	 * @param aviso
	 * @param derechos
	 * @return
	 */
	@Override
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirTodosLosIncisos, boolean incluirCertificadoRC,
			boolean incluirAnexosIncisos, boolean incluirReferenciasBancarias, int incisoInicial, int incisoFinal,
			Short claveTipoEndoso, Boolean esSituacionActual,
			boolean incluirCondicionesEsp, boolean incluirEdicionPoliza, boolean imprimirEdicionInciso,  boolean incluirCartaCargo , boolean aviso, boolean derechos) {
		BigDecimal idToCotizacion = null;
		EdicionInciso edicionInciso = null;
		PolizaDTO polizaDTO = null;
		if (validOn == null) {
			validOn = new DateTime();
		}
		if(imprimirEdicionInciso){
			edicionInciso = edicionImpresionDao.buscarEdicionInciso(idToPoliza, validOn, recordFrom, claveTipoEndoso);
		}
		String llaveFiscal = null;
		byte[] reciboFiscal = null;
		byte[] nationalUnity = null;

		TransporteImpresionDTO transporte = new TransporteImpresionDTO();
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		List<byte[]> inputByteArray = new LinkedList<byte[]>();

		DatosCaratulaPoliza datosCaratulaPoliza = null;
		
		try {
			//
			// Caratula de la poliza
			//
			polizaDTO = entidadService.findById(PolizaDTO.class, idToPoliza);
			idToCotizacion = polizaDTO.getCotizacionDTO().getIdToCotizacion();
			
			//Obtenemos el endoso que se desea imprimir
			EndosoDTO endoso = getEndosoAImprimir(polizaDTO.getIdToPoliza(), recordFrom);
			
			boolean esSeguroObligatorio = esSeguroObligatorio(polizaDTO);
			boolean esImprimeAnexosSeccion = this.imprimirAnexosPorLogicaNegocio(polizaDTO, incluirTodosLosIncisos, incluirCaratula, incisoFinal);
			
			// Se obtiene el objeto bitemporal para la cotización con el ID y la
			// correspondiente
			// fechas de validez
			
			BitemporalCotizacion bitemporalCotizacion = getBitemporalCotizacion(validOn, recordFrom, idToCotizacion,claveTipoEndoso);
			this.bitemporalCotizacionDatosAgente=bitemporalCotizacion;
			
			if (incluirCaratula || incluirEdicionPoliza) {
				
				// Se obtienen los datos generales de la poliza
				datosCaratulaPoliza = impresionBitemporalService.llenarPolizaDTO(idToPoliza, validOn, 
						recordFrom,claveTipoEndoso, esSituacionActual);
				
			} else {
				datosCaratulaPoliza = new DatosCaratulaPoliza();
				datosCaratulaPoliza.setContenedorDatosImpresion(new ContenedorDatosImpresion());
				datosCaratulaPoliza.setPolizaDTO(polizaDTO);
			}
			
			//
			// Incisos
			//
			
			List<DatosCaratulaInciso> dataSourceIncisos = new LinkedList<DatosCaratulaInciso>();
			String numPolizaFormateada = datosCaratulaPoliza.getPolizaDTO()
					.getNumeroPolizaFormateada();
			
			if (incluirTodosLosIncisos) {
				dataSourceIncisos = impresionBitemporalService.llenarIncisoDTO(
						bitemporalCotizacion, numPolizaFormateada, validOn,
						recordFrom, datosCaratulaPoliza.getPolizaDTO(),
						datosCaratulaPoliza.getDatosPolizaDTO(),
						endoso.getId().getNumeroEndoso(), claveTipoEndoso, incluirCondicionesEsp, edicionInciso);
			} else if (incisoInicial > 0 && incisoFinal > 0
					&& incisoFinal >= incisoInicial) {
				dataSourceIncisos = impresionBitemporalService.llenarIncisoDTO(
						bitemporalCotizacion, numPolizaFormateada, validOn,
						recordFrom, incisoInicial, incisoFinal,
						datosCaratulaPoliza.getPolizaDTO(), datosCaratulaPoliza.getDatosPolizaDTO(),
						endoso.getId().getNumeroEndoso(), claveTipoEndoso, incluirCondicionesEsp, edicionInciso);
			}
			
			if (incluirCaratula || incluirEdicionPoliza) {
				if (datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio().equals(GeneraPlantillaReporteBitemporalServiceImpl.NEGOCIO_AUTOPLAZO)){
					String observacionesAutoplazo=dataSourceIncisos.get(0).getDataSourceIncisos().getObservaciones();
					datosCaratulaPoliza.getDatosPolizaDTO().setObservaciones((observacionesAutoplazo!=null?observacionesAutoplazo:""));
				}
				
				if(incluirCaratula){
					byte[] polizaPDFbytes = imprimirPoliza(datosCaratulaPoliza,	locale, esSeguroObligatorio);
					inputByteArray.add(polizaPDFbytes);
				}
				
				if(incluirEdicionPoliza){
					byte[] edicionPolizaPDFbytes = imprimirEdicionPoliza(idToPoliza, validOn, recordFrom, claveTipoEndoso, esSituacionActual, locale).getByteArray();
					inputByteArray.add(edicionPolizaPDFbytes);
				}
				
				if (incluirCartaCargo){
					if (datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getIdMedioPago() != null
							&& datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getIdMedioPago().shortValue() != TipoCobro.EFECTIVO.valor()) {
						Agente agente = new Agente();
						agente.setId(bitemporalCotizacion.getValue().getSolicitud().getCodigoAgente().longValue());
						datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getSolicitudDTO().setAgente(agenteMidasDao.loadByIdImpresiones(agente));
						byte[] reporteCargoCuenta = llenarReporteCargoCuenta(datosCaratulaPoliza, locale);
						if (reporteCargoCuenta != null	&& reporteCargoCuenta.length > 0) {
							inputByteArray.add(reporteCargoCuenta);
						}
					}
				}
			}
			
			// Se agrega el ID de la cotización a la poliza
			datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().setIdToCotizacion(idToCotizacion);

			//
			// Recibos
			//
			// reciboFiscal - Se solicitan recibos relacionados a la poliza
			if (incluirRecibo) {

				//Si solo se requeriere imprimir un solo inciso
				if (!incluirTodosLosIncisos && incisoInicial > 0 && incisoFinal > 0 && incisoInicial == incisoFinal) {
					llaveFiscal = impresionRecibosService.getLlaveFiscal(idToPoliza, incisoInicial);
				} else {						
					llaveFiscal = endoso.getLlaveFiscal();
					LogDeMidasEJB3.getLogger().log(Level.INFO, "Obteniendo recibo para llave fiscal " 
							+ llaveFiscal + " para idToCotizacion " + idToCotizacion);
				}
				reciboFiscal = impresionRecibosService.getReciboFiscal(llaveFiscal);
				if (reciboFiscal != null) {
					inputByteArray.add(reciboFiscal);
				}
			}

			
			
			// Condiciones Especiales de la Poliza
			if ( incluirCondicionesEsp ) {
				boolean polizaTipoFlotilla = false; 
				try {
					// CE por poliza
					List<CondicionEspecial> dataSourceCondicionesEspecialesPoliza = 
						//condicionEspecialBitemporalService.obtenerCondicionesEspecialesPoliza(bitemporalCotizacion, validOn, recordFrom);
						condicionEspecialDao.obtenerCondicionesCotizacionValidas(bitemporalCotizacion.getContinuity().getId(), 
																				validOn, recordFrom);					
						// CE por incisos
					List<CondicionEspecial> dataSourceCondicionesEspecialesIncisos = 
						condicionEspecialDao.obtenerCondicionesIncisoValidas(bitemporalCotizacion.getContinuity().getId(), 
																			validOn, recordFrom, incisoInicial, incisoFinal);
						//condicionEspecialBitemporalService.obtenerCondicionesEspecialesIncisos(bitemporalCotizacion, validOn, recordFrom);
					
					if(!dataSourceCondicionesEspecialesIncisos.isEmpty() ){
						polizaTipoFlotilla = true;
					}
					
					if( !dataSourceCondicionesEspecialesPoliza.isEmpty() ){
						inputByteArray.add(	this.imprimirCondicionesPoliza(	
								dataSourceCondicionesEspecialesPoliza, dataSourceCondicionesEspecialesIncisos, 
								datosCaratulaPoliza.getPolizaDTO().getNumeroPolizaFormateada(), polizaTipoFlotilla) );
					}
					
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
					LogDeMidasEJB3.getLogger().log(Level.SEVERE, "Condiciones Especiales " + idToCotizacion, e);
				}
			}
			
			//
			// Incisos y certificados
			//
			if (!dataSourceIncisos.isEmpty()) {
				idToNegocio=polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio();
				for (DatosCaratulaInciso datosInciso : dataSourceIncisos) {
					inputByteArray.add(imprimirInciso(datosInciso, locale, esSeguroObligatorio, edicionInciso));
					// Condiciones Especiales de los Incisos (Resumen)
					if (incluirCondicionesEsp) {
						try{							
							if (datosInciso.getDataSourceIncisos().getCondicionesEspeciales() != null && 
									!datosInciso.getDataSourceIncisos().getCondicionesEspeciales().isEmpty()){
								inputByteArray.add( this.imprimirCondicionesInciso(datosInciso.getDataSourceIncisos(), "Condiciones Especiales por Inciso",locale) );
							}
						}catch(Exception e){
							LogDeMidasEJB3.getLogger().log(Level.SEVERE, "Condiciones Especiales " + idToCotizacion + " inciso " + datosInciso.getDataSourceIncisos().getNumeroSecuenciaInciso(), e);
						}
					}
					if (incluirCertificadoRC) {
						if (datosInciso.getDataSourceIncisos().isNationalUnity()) {
							String idCotizacion = idToCotizacion.multiply(BigDecimal.valueOf(-1)).toString();
							Integer idSeccion = Integer.valueOf(datosInciso.getDataSourceIncisos().getIdSeccion());
							Integer idInciso = Integer.valueOf(datosInciso.getDataSourceIncisos().getNumeroSecuenciaInciso());

							nationalUnity = this.getNationalUnity(idCotizacion, idSeccion, idInciso);

							if (nationalUnity != null && nationalUnity.length > 0) {
								inputByteArray.add(nationalUnity);
							}
						}
					}
				
					//Anexos por seccion, coberturas
					try {
						Long idSeccion = Long.parseLong( datosInciso.getDataSourceIncisos().getIdSeccion() );
						if ( esImprimeAnexosSeccion && !this.getTipoAgrupacion(polizaDTO).equals("agrupacion") ){
							inputByteArray.addAll( documentoAnexoSeccionService.getAnexosSeccion(idSeccion) );
							
							List<DatosCoberturasDTO> coberturasDTOList = datosInciso.getDataSourceCoberturas();
							List<BigDecimal> coberturasList = new ArrayList<BigDecimal>();
							for ( DatosCoberturasDTO item : coberturasDTOList ){
								coberturasList.add( item.getIdToCobertura() );								
							}
							inputByteArray.addAll(documentoAnexoCoberturaService.getAnexosCobertura(coberturasList));
						}						
					} catch (Exception e) {
						LogDeMidasEJB3.getLogger().log(Level.SEVERE, "imprimirAnexosSeccionCoberturas " + idToCotizacion, e);
					}
				}
			}

			if (incluirAnexosIncisos) {
				try {
					inputByteArray.addAll(impresionBitemporalService
							.imprimirAnexosPoliza(bitemporalCotizacion,
									validOn, recordFrom));
				} catch (Exception e) {
					LogDeMidasEJB3.getLogger().log(Level.SEVERE, "imprimirAnexosPoliza " + idToCotizacion, e);
				}

			}
			
			if (incluirReferenciasBancarias) {
				try {
					datosCaratulaPoliza = impresionBitemporalService.llenarReferenciasBancariasPolizaDTO(idToPoliza, validOn, 
							recordFrom,claveTipoEndoso, esSituacionActual);					
						byte[] polizaPDFbytes = imprimirReferenciaBancaria(datosCaratulaPoliza,	locale, esSeguroObligatorio);
						inputByteArray.add(polizaPDFbytes);
				} catch (Exception e) {
					LogDeMidasEJB3.getLogger().log(Level.SEVERE, "imprimirReferenciasBancarias " + idToCotizacion, e);
				}

			}
			//Anexos por seccion, coberturas
			if ( this.getTipoAgrupacion(polizaDTO).equals("agrupacion") && incluirCaratula){
				
				try {
					inputByteArray.addAll( documentoAnexoSeccionService.getAnexosSeccion(polizaDTO) );
					inputByteArray.addAll( documentoAnexoCoberturaService.getAnexosCobertura( polizaDTO ) );
				} catch (Exception e) {
					LogDeMidasEJB3.getLogger().log(Level.SEVERE, "imprimirAnexosSeccionCoberturasPorAgrupacion " + idToCotizacion, e);
				}					
			}
			
			// Cartas para renovacion
			try {
				if (polizaDTO.getNumeroRenovacion().compareTo(0) > 0) {
					TransporteImpresionDTO cartas = generaPlantillaCartaRenovacionService
							.llenarCartaRenovacion(polizaDTO, locale);
					inputByteArray.add(cartas.getByteArray());
				}
			} catch (Exception e) {
				LogDeMidasEJB3.getLogger().log(Level.SEVERE, "llenarCartaRenovacion " + idToCotizacion, e);
			}

			// AVISO DE PRIVACIDAD Y CARTA DE DERECHOS :: CAMBIAR CONDICION CUANDO SEA POR CONFIGURACION DEL NEGOCIO
//			if (datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getSolicitudDTO().getNegocio().getDescripcionNegocio().toUpperCase().contains("360")){
				
				if(aviso) { 
					
					InputStream avisoPrivacidad = this
							.getClass()
							.getResourceAsStream(UR_JRXML+"/AP_Autos.pdf");
					
					byte[] pdfAvisoPrivacidad = null;
					
					try {
						pdfAvisoPrivacidad = IOUtils.toByteArray(avisoPrivacidad);
						inputByteArray.add(pdfAvisoPrivacidad);
					} catch (IOException e) {
						LogDeMidasEJB3.getLogger().log(Level.SEVERE, " AVISO DE PRIVACIDAD" + idToCotizacion, e);
					}
				}
				
				if(derechos) {
					
					InputStream cartaDerechos = this
							.getClass()
							.getResourceAsStream(UR_JRXML+"/DERECHOS_DE_LOS_ASEGURADOS.pdf");
					
					byte[] pdfCartaDerechos = null;
					
					try {
						pdfCartaDerechos = IOUtils.toByteArray(cartaDerechos);
						inputByteArray.add(pdfCartaDerechos);
					} catch (IOException e) {
						LogDeMidasEJB3.getLogger().log(Level.SEVERE, "CARTA DE DERECHOS " + idToCotizacion, e);
					}
				}		
//			}
			
			pdfConcantenate(inputByteArray, output);

			transporte.setByteArray(output.toByteArray());
		} catch (Exception e) {
			LogDeMidasEJB3.getLogger().log(Level.SEVERE, "imprimirPoliza " + idToPoliza, e);
			throw new RuntimeException(e);
		}

		return transporte;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void pdfConcantenate(List<byte[]> inputByteArray,
			ByteArrayOutputStream outputStream) {

		FileUtils.pdfConcantenate(inputByteArray, outputStream);

	}

	@Override
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirCertificadoRC,boolean  incluirReferenciaBancaria, boolean incluirAnexosIncisos,
			int incisoInicial, int incisoFinal, Short claveTipoEndoso,
			Boolean esSituacionActual, boolean incluirCondicionesEsp) {		
		boolean incluirTodosLosIncisos = false;		
		return imprimirPoliza(idToPoliza, locale, validOn, recordFrom,
				incluirCaratula, incluirRecibo, incluirTodosLosIncisos,
				incluirCertificadoRC, incluirAnexosIncisos,incluirReferenciaBancaria, incisoInicial,
				incisoFinal, claveTipoEndoso, esSituacionActual,
				incluirCondicionesEsp,true);
	}
	
	/**
	 * Imprimir Poliza
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param recordFrom
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirCertificadoRC
	 * @param incluirReferenciaBancaria
	 * @param incluirAnexosIncisos
	 * @param incisoInicial
	 * @param incisoFinal
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @param incluirCondicionesEsp
	 * @param aviso
	 * @param derechos
	 * @return
	 */
	@Override
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirCertificadoRC,boolean  incluirReferenciaBancaria, boolean incluirAnexosIncisos,
			int incisoInicial, int incisoFinal, Short claveTipoEndoso,
			Boolean esSituacionActual, boolean incluirCondicionesEsp, boolean aviso, boolean derechos) {
		boolean incluirTodosLosIncisos = false;		
		return imprimirPoliza2(idToPoliza, locale, validOn, recordFrom,
				incluirCaratula, incluirRecibo, incluirTodosLosIncisos,
				incluirCertificadoRC, incluirAnexosIncisos,incluirReferenciaBancaria, incisoInicial,
				incisoFinal, claveTipoEndoso, esSituacionActual,
				incluirCondicionesEsp,true, aviso, derechos);
	}
	
	
	/**
	 * Usa el Servicio Web de recibos
	 * 
	 * @param idToCotizacion
	 * @param idLinea
	 * @param idInciso
	 * @return
	 */
	private byte[] getNationalUnity(String idToCotizacion, Integer idLinea,
			Integer idInciso) {
		byte[] pdf = null;

		try {
			pdf = this.getServicioImpresionED().getNationalUnityCertificateNew(
					idToCotizacion, idLinea, idInciso, "application/pdf");
		} catch (Exception ex) {
			LogDeMidasEJB3.getLogger().log(Level.SEVERE, "National Unity" + idToCotizacion + " inciso " + idInciso, ex);
		}
		return pdf;
	}

	/**
	 * Obtiene y devuelve un objeto del tipo PrintReport, por medio del Servicio
	 * Web de reciboss
	 * 
	 * @return Devuelve un objeto del tipo PrintReport
	 */
	private PrintReport getServicioImpresionED() {
		PrintReportServiceLocator locator = null;
		PrintReport pr = null;
		try {
			String urlWsImp = SistemaPersistencia.URL_WS_IMPRESION;// "172.20.73.33";
			String puertoWsImp = SistemaPersistencia.PUERTO_WS_IMPRESION;// "9080";
			locator = new PrintReportServiceLocator(urlWsImp, puertoWsImp);
			pr = locator.getPrintReport();
		} catch (Exception e) {
			throw new RuntimeException("WebService de recibos no disponible");
		}
		return pr;
	}
	
	private WFacturaService getServicioFacturaED() {

		String targetEndPoint = SistemaPersistencia.URL_WS_IMPRESION_FACTURA;
		WFacturaService b = null;
		try {
			b = new WFacturaService(new URL(targetEndPoint));
		} catch (Exception e) {
			LOG.error(e);
		}
		return b;

	}
	
	private Boolean esSeguroObligatorio(PolizaDTO poliza){
		Negocio negocio = seguroObligatorioService.getNegocioSeguroObligatorio(poliza.getIdToPoliza());
		if(negocio != null && poliza != null && poliza.getCotizacionDTO() != null && poliza.getCotizacionDTO().getSolicitudDTO() != null &&
				poliza.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio().equals(negocio.getIdToNegocio())){
			return true;
		}
		return false;
	}
	
	private InputStream obtieneCaratulaSeguroObligatorio (DatosCaratulaInciso inciso){
		//Si es seguro obligatorio normal 
		InputStream caratulaIncisoStream = this.getClass().getResourceAsStream(UR_JRXML+"/CaratulaIncisoSO.jasper");
		
		return caratulaIncisoStream;
	}
	
	private InputStream obtieneDesgloceSeguroObligatorio (DatosCaratulaInciso inciso){
		//Si es seguro obligatorio normal 
		InputStream caratulaIncisoStream = this.getClass().getResourceAsStream(UR_JRXML+"/DesgloseCoberturasSO.jasper");
		//Si es seguro obligatorio de servicio publico 		
//		if(inciso.getDataSourceIncisos().getIdSeccion().equals(SeccionDTO.LINEA_NEG_AUT_SERVICIO_PUBLICO.toPlainString()) ||
//				inciso.getDataSourceIncisos().getIdSeccion().equals(SeccionDTO.LINEA_NEG_BUS_SERVICIO_PUBLICO.toPlainString()) ||
//				inciso.getDataSourceIncisos().getIdSeccion().equals(SeccionDTO.LINEA_NEG_AUT_SERVICIO_PUBLICO_MTY.toPlainString())){
//			caratulaIncisoStream = this.getClass().getResourceAsStream(UR_JRXML+"/DesgloseCoberturasSOTAXI.jrxml");
//		}
		return caratulaIncisoStream;
	}
	
	private byte[] imprimirInciso(DatosCaratulaInciso inciso, Locale locale, Boolean esSeguroObligatorio, EdicionInciso edicionInciso) {
		byte[] pdfInciso = null;
		boolean imprimirUMA = listadoService.imprimirLeyendaUMA(inciso.getDataSourceIncisos().getFechaInicioVigencia());

		try {
			InputStream caratulaIncisoStream = this
			.getClass()
			.getResourceAsStream(UR_JRXML+"/CaratulaIncisoM2.jasper");
			
			if(esSeguroObligatorio){
				caratulaIncisoStream = obtieneCaratulaSeguroObligatorio(inciso);
			}
			
			InputStream desgloseCoberturasStream = this
					.getClass()
					.getResourceAsStream(UR_JRXML+"/DesgloseCoberturasM2.jasper");
			if(esSeguroObligatorio){
				desgloseCoberturasStream = obtieneDesgloceSeguroObligatorio(inciso);
			}
			JasperReport reporteCoberturas = (JasperReport) JRLoader.loadObject(desgloseCoberturasStream);
			Map<String, Object> parameters = new HashMap<String, Object>(1);
			// Por cada uno de estos DatosIncisoDTO se hará una página
			List<DatosIncisoDTO> reporteDatasourceInciso = new ArrayList<DatosIncisoDTO>(1);
			DatosIncisoDTO diDTO = inciso.getDataSourceIncisos();
			reporteDatasourceInciso.add(diDTO);
			if (locale != null) {
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
			parameters.put(KEY_LOGO_IMG,
					SistemaPersistencia.LOGO_SEGUROS_AFIRME);
			parameters.put(KEY_SIGNATURES,
					SistemaPersistencia.FIRMA_FUNCIONARIO);
			// Estas son las coberturas para cada inciso
			List<DatosCoberturasDTO> dcDTOList = inciso
					.getDataSourceCoberturas();
			parameters.put("coberturasDataSource", dcDTOList);
			parameters.put("coberturasReport", reporteCoberturas);
			SeccionDTO seccionDTO = seccionFacade.findById(new BigDecimal(diDTO.getIdSeccion()));
			Date fechaRegistro = esSeguroObligatorio?dcDTOList.get(0).getFechaRegistro():seccionDTO.getFechaRegistro();
			String numeroRegistro = esSeguroObligatorio?dcDTOList.get(0).getNumeroRegistro():seccionDTO.getNumeroRegistro();
			parameters.put("leyendaCNSF", this.getLeyendaISyF(fechaRegistro, numeroRegistro));
			parameters.put("datosAgenteCaratula", diDTO.getNumeroCotizacion()!=null?
					this.impresionBitemporalService.getIdentificadorCaratulaPoliza(this.bitemporalCotizacionDatosAgente).get("datosAgente"):"");
			Map<String, Object> spvData = listadoService.getSpvData(idToNegocio);
			parameters.put("numeroTelefonoSPV",spvData.get("numeroTelefonoSPV"));
			parameters.put("spvNumberDF",spvData.get("spvNumberDF"));
			parameters.put("esServicioPublico",spvData.get("esServicioPublico"));
			parameters.put("imprimirUMA", imprimirUMA);
			parameters.put(JRParameter.REPORT_VIRTUALIZER,
					new JRGzipVirtualizer(2));
			if(edicionInciso != null){
				if(!edicionInciso.getImprimirAgenteCaratula() || !StringUtil.isEmpty(edicionInciso.getDatosAgenteCaratula()) ){
					parameters.put("datosAgenteCaratula", edicionInciso.getDatosAgenteCaratula());}
				if(edicionInciso.getImprimirPrimas() != null){
					parameters.put("imprimirPrimas", edicionInciso.getImprimirPrimas());}
				if(!StringUtil.isEmpty(edicionInciso.getHoraInicioVigencia())){
					parameters.put("horaInicioVigencia", edicionInciso.getHoraInicioVigencia());}
				if(!StringUtil.isEmpty(edicionInciso.getHoraFinVigencia())){
					parameters.put("horaFinVigencia", edicionInciso.getHoraFinVigencia());}
				if(!StringUtil.isEmpty(edicionInciso.getTelefono1())){
					parameters.put("telefono1", edicionInciso.getTelefono1());}
				if(!StringUtil.isEmpty(edicionInciso.getDescTelefono1())){
					parameters.put("descTelefono1", edicionInciso.getDescTelefono1());}
				if(!StringUtil.isEmpty(edicionInciso.getTelefono2())){
					parameters.put("telefono2", edicionInciso.getTelefono2());}
				if(!StringUtil.isEmpty(edicionInciso.getDescTelefono2())){
					parameters.put("descTelefono2", edicionInciso.getDescTelefono2());}
				if(!StringUtil.isEmpty(edicionInciso.getTelefono3())){
					parameters.put("telefono3", edicionInciso.getTelefono3());}
				if(!StringUtil.isEmpty(edicionInciso.getDescTelefono3())){
					parameters.put("descTelefono3", edicionInciso.getDescTelefono3());}
//				parameters.put("coberturasEditadas", edicionInciso.getCoberturas());
			}
			
			JRBeanCollectionDataSource dsInciso = new JRBeanCollectionDataSource(
					reporteDatasourceInciso);
			pdfInciso = JasperRunManager.runReportToPdf(caratulaIncisoStream,
					parameters, dsInciso);
		} catch (JRException ex) {
			throw new RuntimeException(ex);
		}
		return pdfInciso;
	}

	/**
	 * Se genera un reporte Jasper y se exporta al
	 * formato PDF
	 * 
	 * @param datosCaratulaPoliza
	 *            -
	 * @param locale
	 *            -
	 * @param esSeguroObligatorio 
	 * @return Devuelve el reporte Jasper-PDF como un array de bytes
	 */
	private byte[] imprimirReferenciaBancaria(DatosCaratulaPoliza datosCaratulaPoliza,
			Locale locale, Boolean esSeguroObligatorio) {
		LOG.trace(">> imprimirReferenciaBancaria()");

		byte[] pdfPoliza = null;
		List<DatosPolizaDTO> dataSourcePoliza = new LinkedList<DatosPolizaDTO>();
		List<ReferenciaBancariaDTO> datasourceReferenciasBancarias = datosCaratulaPoliza
				.getDatasourceReferenciasBancarias();
		try {
			InputStream referenciaBancaria = this
			.getClass()
			.getResourceAsStream(UR_JRXML+"/FormatoPagoPrimas.jasper");
			JasperReport reporteReferenciaBancaria = (JasperReport) JRLoader.loadObject(referenciaBancaria);
			InputStream desgloseLineasStream = this
					.getClass()
					.getResourceAsStream(UR_JRXML+"/FormatoPagoPrimasSub.jasper");
			
			JasperReport reporteLineas = (JasperReport) JRLoader.loadObject(desgloseLineasStream);

			Map<String, Object> parameters = new HashMap<String, Object>(1);
			if (locale != null) {
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
			parameters.put(KEY_LOGO_IMG,
					SistemaPersistencia.LOGO_SEGUROS_AFIRME);
			parameters.put("lineasDataSource", datasourceReferenciasBancarias);
			parameters.put("lineasReport", reporteLineas);
			parameters.put(JRParameter.REPORT_VIRTUALIZER,
					new JRGzipVirtualizer(2));
			dataSourcePoliza.add(datosCaratulaPoliza.getDatosPolizaDTO());
			JRBeanCollectionDataSource dsPoliza = new JRBeanCollectionDataSource(
					dataSourcePoliza);
			pdfPoliza = generarReporte(reporteReferenciaBancaria, parameters, dsPoliza);
		} catch (JRException ex) {
			throw new RuntimeException(ex);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		LOG.trace("<< imprimirReferenciaBancaria()");
		return pdfPoliza;
	}
	/**
	 * Se mezclan la plantilla JRXML y los datos recibidos en el parámetro
	 * datosCaratulaPoliza, y se genera un reporte Jasper y se exporta al
	 * formato PDF
	 * 
	 * @param datosCaratulaPoliza
	 *            -
	 * @param locale
	 *            -
	 * @param esSeguroObligatorio 
	 * @return Devuelve el reporte Jasper-PDF como un array de bytes
	 */
	private byte[] imprimirPoliza(DatosCaratulaPoliza datosCaratulaPoliza,
			Locale locale, Boolean esSeguroObligatorio) {
		byte[] pdfPoliza = null;
		List<DatosPolizaDTO> dataSourcePoliza = new LinkedList<DatosPolizaDTO>();
		List<DatosLineasDeNegocioDTO> datasourceLineasNegocio = datosCaratulaPoliza
				.getDatasourceLineasNegocio();

		try {
			InputStream caratulaPolizaStream = this
					.getClass()
					.getResourceAsStream(UR_JRXML+"/CaratulaPolizaM2.jasper");
			JasperReport reportePoliza = (JasperReport) JRLoader.loadObject(caratulaPolizaStream);

			InputStream desgloseLineasStream = this
					.getClass()
					.getResourceAsStream(UR_JRXML+"/DesgloseLineasM2.jasper");
			
			JasperReport reporteLineas = (JasperReport) JRLoader.loadObject(desgloseLineasStream);

			Map<String, Object> parameters = new HashMap<String, Object>(1);
			if (locale != null) {
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
			parameters.put(KEY_LOGO_IMG,
					SistemaPersistencia.LOGO_SEGUROS_AFIRME);
			parameters.put(KEY_SIGNATURES,
					SistemaPersistencia.FIRMA_FUNCIONARIO);			
			parameters.put("lineasDataSource", datasourceLineasNegocio);
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date fechaRegistro = esSeguroObligatorio?dateFormat.parse("21/01/2015"):datasourceLineasNegocio.get(0).getFechaRegistro();
			String numeroRegistro = esSeguroObligatorio?"CNSF-S0094-0597-2014":datasourceLineasNegocio.get(0).getNumeroRegistro();
			parameters.put("leyendaCNSF", this.getLeyendaISyF(fechaRegistro,numeroRegistro));
			parameters.put("datosAgenteCaratula", this.impresionBitemporalService.getIdentificadorCaratulaPoliza(bitemporalCotizacionDatosAgente).get("datosAgente"));
			parameters.put("lineasReport", reporteLineas);
			parameters.put(JRParameter.REPORT_VIRTUALIZER,
					new JRGzipVirtualizer(2));
			dataSourcePoliza.add(datosCaratulaPoliza.getDatosPolizaDTO());
			JRBeanCollectionDataSource dsPoliza = new JRBeanCollectionDataSource(
					dataSourcePoliza);
			pdfPoliza = generarReporte(reportePoliza, parameters, dsPoliza);
		} catch (JRException ex) {
			throw new RuntimeException(ex);
		} catch (ParseException ex) {
			throw new RuntimeException(ex);
		}
		return pdfPoliza;
	}

	private byte[] llenarReporteCargoCuenta(DatosCaratulaPoliza datosCaratulaPoliza, Locale locale) {

		byte[] pdfCargoCuenta = null;
		Connection connection = null;
		try {
			InputStream cargoCuentaStream = this
					.getClass()
					.getResourceAsStream(UR_JRXML+"/CartaCargoAutomatico.jasper");
			JasperReport reporteCargoCuenta = (JasperReport) JRLoader.loadObject(cargoCuentaStream);

			Map<String, Object> parameters = new HashMap<String, Object>(1);
			if (locale != null) {
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
			parameters.put(KEY_LOGO_IMG,
					SistemaPersistencia.LOGO_SEGUROS_AFIRME);
			parameters.put("P_IDENTIFIER_ID", datosCaratulaPoliza.getPolizaDTO().getIdToPoliza().toString());
			parameters.put("P_NUM_COTIZACION", datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getSolicitudDTO().getIdToSolicitud());
			parameters.put("P_FH_EMISION", datosCaratulaPoliza.getPolizaDTO().getFechaCreacion());
			parameters.put("P_NOM_PRODUCTO", datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getSolicitudDTO().getProductoDTO().getNombreComercial());
			parameters.put("P_F_INI_VIGENCIA", datosCaratulaPoliza.getDatosPolizaDTO().getFechaInicioVigenciaStr());
			parameters.put("P_F_FIN_VIGENCIA", datosCaratulaPoliza.getDatosPolizaDTO().getFechaFinVigenciaStr());
			parameters.put("P_NUMERO_POLIZA", datosCaratulaPoliza.getDatosPolizaDTO().getNumeroPoliza());
			parameters.put("P_NOM_FORMA_PAGO", datosCaratulaPoliza.getDatosPolizaDTO().getDescripcionFormaPago());
			parameters.put("P_ID_COND_COBRO", datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getIdMedioPago());
			parameters.put("P_NOMBRE_CLIENTE", datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getNombreContratante());
			parameters.put("P_ID_AGENTE", new BigDecimal(datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getSolicitudDTO().getAgente().getIdAgente()));
			parameters.put("P_NOMBRE_AGENTE", datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getSolicitudDTO().getNombreAgente());
			parameters.put("P_NOM_OFICINA", datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getSolicitudDTO().getNombreOficina());
			parameters.put("P_DIRECCION_SUCURSAL", datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getSolicitudDTO().getAgente().getPersona().getDomicilios().get(0).getCiudad()+
					", "+datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().getSolicitudDTO().getAgente().getPersona().getDomicilios().get(0).getEstado());

			Context ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("jdbc/MidasDataSource");
			connection = ds.getConnection();

			JasperPrint jp = JasperFillManager.fillReport(reporteCargoCuenta,
					parameters, connection);

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			JRPdfExporter pdfExporter = new JRPdfExporter();
			pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
			pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
					outputStream);
			pdfExporter.exportReport();
			return outputStream.toByteArray();

		} catch (Exception e) {
			LogDeMidasEJB3.log(
					"Error al obtener reporte de cargo a cuenta para poliza "
							+ datosCaratulaPoliza.getPolizaDTO().getIdToPoliza(), Level.SEVERE, e);
		} finally {
			DbUtils.closeQuietly(connection);
		}

		return pdfCargoCuenta;
	}

	/**
	 * 
	 * @param configuracionReporte
	 * @param parametros
	 * @param colleccionObjeto
	 * @return
	 */
	private byte[] generarReporte(JasperReport configuracionReporte,
			Map<String, Object> parametros,
			JRBeanCollectionDataSource colleccionObjeto) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(
					configuracionReporte, parametros, colleccionObjeto);
			JasperExportManager.exportReportToPdfStream(jasperPrint, out);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		return out.toByteArray();
	}

	@Override
	public byte[] imprimirCondicionesPoliza( List<CondicionEspecial> dataSourceCondicionesEspecialesPoliza,
											 List<CondicionEspecial> dataSourceCondicionesEspecialesIncisos,
											 String numeroPoliza,
											 boolean polizaTipoFlotilla ) {
		
		byte[] pdfCondicionesEspecialesPolizaIncisos = null;
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		
		/* Compilado de jReports y generación del .jasper */
		String jrxml = UR_JRXML+"/CaratulaPolizaM2CondicionesEspecialesGral.jasper";
		JasperReport jReportCondicionesEspecialesGral = gImpresion.getOJasperReport( jrxml );

		jrxml = UR_JRXML+"/CaratulaPolizaM2CondicionesEspeciales.jasper";
		JasperReport jReportCondicionesEspeciales = gImpresion.getOJasperReport( jrxml );

		jrxml = UR_JRXML+"/CaratulaPolizaM2CondicionesEspecialesPolizaGral.jasper";
		JasperReport jReportCondicionesEspecialesPolizaGral = gImpresion.getOJasperReport( jrxml );
		
		jrxml = UR_JRXML+"/CaratulaPolizaM2CondicionesEspecialesPoliza.jasper";
		JasperReport jReportCondicionesEspecialesPoliza = gImpresion.getOJasperReport( jrxml );

		jrxml = UR_JRXML+"/CaratulaPolizaM2CondicionesEspecialesIncisosSinRepetirGral.jasper";
		JasperReport jReportCondicionesEspecialesIncisosGral = gImpresion.getOJasperReport( jrxml );
		
		jrxml = UR_JRXML+"/CaratulaPolizaM2CondicionesEspecialesIncisosSinRepetir.jasper";
		JasperReport jReportCondicionesEspecialesIncisos = gImpresion.getOJasperReport( jrxml );
		
		/* Se definen los parámetros a mapear en el jrxml */
		Map<String, Object> params = new HashMap<String, Object>();

		params.put( "jReportCondicionesEspecialesGral",       jReportCondicionesEspecialesGral );
		params.put( "jReportCondicionesEspeciales",           jReportCondicionesEspeciales );
		
		params.put( "jReportCondicionesEspecialesPolizaGral", jReportCondicionesEspecialesPolizaGral );
		params.put( "jReportCondicionesEspecialesPoliza",	  jReportCondicionesEspecialesPoliza );
		params.put( "dataSourceCondicionesEspecialesPoliza",  dataSourceCondicionesEspecialesPoliza );
		
		params.put( "jReportCondicionesEspecialesIncisosGral",jReportCondicionesEspecialesIncisosGral );
		params.put( "jReportCondicionesEspecialesIncisos",    jReportCondicionesEspecialesIncisos );
		params.put( "dataSourceCondicionesEspecialesIncisos", dataSourceCondicionesEspecialesIncisos );
		
		params.put( "jSubReportCondicionPoliza", jReportCondicionesEspeciales);
		params.put( "jSubReportCondicionIncisos", jReportCondicionesEspecialesIncisos);
		
		params.put( "numeroPoliza",                           numeroPoliza );
		params.put( "polizaTipoFlotilla",                     String.valueOf(polizaTipoFlotilla) );
		params.put( KEY_LOGO_IMG,          			      SistemaPersistencia.LOGO_SEGUROS_AFIRME );
		
//		JRBeanCollectionDataSource dsCondicionesEspecialesPolizaInciso = new JRBeanCollectionDataSource( dataSourceCondicionesEspecialesPoliza );

		
		pdfCondicionesEspecialesPolizaIncisos = gImpresion.getTImpresionDTO( jReportCondicionesEspecialesGral,
																			 new JREmptyDataSource(1), 
																			 params ).getByteArray();
		return pdfCondicionesEspecialesPolizaIncisos;
	}	
	
	@Override
	public byte[] imprimirCondicionesInciso(DatosIncisoDTO datosInciso,	String titulo, Locale locale) {
		
		byte[] pdfCondicionesEspecialesPolizaIncisos = null;
		List<DatosIncisoDTO> dataSourceCondicionesEspecialesIncisoDetalle = new ArrayList<DatosIncisoDTO>();
		dataSourceCondicionesEspecialesIncisoDetalle.add( datosInciso );
		
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		
		/* Compilado de jReports y generación del .jasper */
		String jrxml = UR_JRXML+"/CaratulaPolizaM2CondicionesEspecialesInciso.jasper";
		JasperReport jReportCondicionesEspecialesInciso = gImpresion.getOJasperReport( jrxml );

		jrxml = UR_JRXML+"/CaratulaPolizaM2CondicionesEspecialesIncisoDetalle.jasper";
		JasperReport jReportCondicionesEspecialesIncisoDetalle = gImpresion.getOJasperReport( jrxml );
		
		/* Se definen los parámetros a mapear en el jrxml */
		Map<String, Object> params = new HashMap<String, Object>();

		params.put( "jReportCondicionesEspecialesInciso",           jReportCondicionesEspecialesInciso );
		
		params.put( "jReportCondicionesEspecialesIncisoDetalle",	jReportCondicionesEspecialesIncisoDetalle );
		params.put( "dataSourceCondicionesEspecialesIncisoDetalle", dataSourceCondicionesEspecialesIncisoDetalle );
		
		params.put( "titulo",                                       titulo ); 
		params.put( KEY_LOGO_IMG,          			            SistemaPersistencia.LOGO_SEGUROS_AFIRME );
		
		JRBeanCollectionDataSource dsCondicionesEspecialesInciso = new JRBeanCollectionDataSource( dataSourceCondicionesEspecialesIncisoDetalle );

		pdfCondicionesEspecialesPolizaIncisos = gImpresion.getTImpresionDTO( jReportCondicionesEspecialesInciso,
																			 dsCondicionesEspecialesInciso, 
																			 params ).getByteArray();
		return pdfCondicionesEspecialesPolizaIncisos;

	}
	
	@Override
	public byte[] imprimirCondicionesEspecialesEndoso(
			List<CondicionesEspIncisosDTO> dataSource, String titulo, String numeroPoliza) {
		
		byte[] pdfCondicionesEspecialesEndoso = null;
		
		List<CondicionesEspIncisosDTO> dataSourceCondicionesEspecialesEndosoIncisoDetalle = dataSource; 
		
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		
		/* Compilado de jReports y generación del .jasper */
		String jrxml = UR_JRXML+"/CaratulaEndosoCondicionesEspeciales.jrxml";
		JasperReport jReportCondicionesEspecialesEndoso = gImpresion.getOJasperReport( jrxml );

		//jrxml = UR_JRXML+"/CaratulaEndosoCondicionesEspecialesDetalleIncisos.jrxml";
		//JasperReport jReportCondicionesEspecialesEndosoDetalleIncisos = gImpresion.getOJasperReport( jrxml );
		
		/* Se definen los parámetros a mapear en el jrxml */
		Map<String, Object> params = new HashMap<String, Object>();

		params.put( "jReportCondicionesEspecialesEndoso",                 jReportCondicionesEspecialesEndoso );
		
		//params.put( "jReportCondicionesEspecialesEndosoDetalleIncisos",	  jReportCondicionesEspecialesEndosoDetalleIncisos );
		params.put( "dataSourceCondicionesEspecialesEndosoIncisoDetalle", dataSourceCondicionesEspecialesEndosoIncisoDetalle );
		
		params.put( "titulo",                                             titulo ); 
		params.put( "numeroPoliza",                                       numeroPoliza ); 
		params.put( KEY_LOGO_IMG,          			                  SistemaPersistencia.LOGO_SEGUROS_AFIRME );
		
		JRBeanCollectionDataSource dsCondicionesEspecialesEndoso = new JRBeanCollectionDataSource( dataSourceCondicionesEspecialesEndosoIncisoDetalle );

		pdfCondicionesEspecialesEndoso = gImpresion.getTImpresionDTO( jReportCondicionesEspecialesEndoso,
																	  dsCondicionesEspecialesEndoso, 
																	  params ).getByteArray();
		return pdfCondicionesEspecialesEndoso;

	}

	@Override
	public String obtenerCadenaIncisoCondicionEspecial(CondicionEspecial condicionEsp, 
			BitemporalCotizacion bCotizacionFiltrado, DateTime dateTimeTemp, DateTime recordFrom ) {
		
		final StringBuilder retorno = new StringBuilder();
		List<BitemporalInciso> incisosTotalesValidos = (List<BitemporalInciso>) bCotizacionFiltrado.getContinuity().getIncisoContinuities().get(dateTimeTemp, recordFrom);
		
		
		for( BitemporalInciso bitempInciso : incisosTotalesValidos ){
			Long incisoContinuityId = bitempInciso.getContinuity().getId();
			Collection<BitemporalCondicionEspInc> bitemporalCondicionEspInc = 
				condicionEspecialBitemporalService.obtenerCondicionesIncisoContinuity(incisoContinuityId, dateTimeTemp, recordFrom);

			for (BitemporalCondicionEspInc item : bitemporalCondicionEspInc) {
				if( item != null ){
					CondicionEspecial condicion = condicionEspecialService.obtenerCondicion(item.getContinuity().getCondicionEspecialId());
					if(condicionEsp.equals(condicion)){
						retorno.append(bitempInciso.getValue().getNumeroSecuencia() + ",");
						break;
					}
				}
			}
		}
		
		if(retorno.length() > 0) {
			return retorno.substring(0, retorno.length() - 1);
		} else {
			return retorno.toString();
		}
		

	}
	
	public List<CondicionEspecial> obtenerCondicionesEspecialesIncisosEndoso(
			BitemporalCotizacion bitempCotizacion, DateTime validOnDT,
			DateTime recordFromDT){
		
		List<CondicionEspecial> condiciones = new ArrayList<CondicionEspecial>();
		
		Collection<BitemporalInciso> bitemporalesInc = 
			incisoBitemporalService.getIncisoList(bitempCotizacion.getContinuity()); 
		
		try{
			for( BitemporalInciso bitempInciso : bitemporalesInc ){
				Long incisoContinuityId = bitempInciso.getContinuity().getId();
				Collection<BitemporalCondicionEspInc> bitemporalCondicionEspInc = 
					condicionEspecialBitemporalService.obtenerCondicionesIncisoContinuity(incisoContinuityId, validOnDT, recordFromDT);
				
				
				for (BitemporalCondicionEspInc item : bitemporalCondicionEspInc) {
					if( item != null ){
						
						if(item.getRecordInterval().getInterval().getStart().equals(recordFromDT)){
							CondicionEspecial condicion = condicionEspecialService.obtenerCondicion(item.getContinuity().getCondicionEspecialId());
							if (!condiciones.contains(condicion)) {
								condiciones.add(condicion);
							}
						}		
					}
				}
			}
		}catch (Exception e) {
			;
		}
		return condiciones;
		
	}
	
	private boolean imprimirAnexosPorLogicaNegocio( PolizaDTO polizaDTO, boolean incluirTodosLosIncisos, 
			boolean incluirCaratula, int incisoFinal ) throws Exception{

		boolean imprime = false;
		
		if ( 	polizaDTO.getCodigoProducto() != null 
				&& polizaDTO.getCodigoProducto().equals(GeneraPlantillaReporteBitemporalServiceImpl.INDIVIDUAL) 
				&& (incluirTodosLosIncisos || (incisoFinal > 0)) ){
			
				imprime = true;
		}
		if ( polizaDTO.getCodigoProducto() != null 
				&& polizaDTO.getCodigoProducto().equals(GeneraPlantillaReporteBitemporalServiceImpl.FLOTILLA) ){

			if ( this.getTipoAgrupacion(polizaDTO).equals("ubicacion") && 
					(incluirTodosLosIncisos || (incisoFinal > 0)) ){
				imprime = true;
			}	
			if ( this.getTipoAgrupacion(polizaDTO).equals("agrupacion") && incluirCaratula ){
				imprime = true;
			}		
		}
		
		return imprime;
	}
	
	private String getTipoAgrupacion( PolizaDTO polizaDTO ) throws Exception {
		
		if ( polizaDTO.getCotizacionDTO().getTipoAgrupacionRecibos() == null || 
				polizaDTO.getCotizacionDTO().getTipoAgrupacionRecibos().equals(CotizacionDTO.TIPO_AGRUPACION_RECIBOS.UBICACION.getValue()) ){
			
			return "ubicacion";
		}
		else{
			
			return "agrupacion";
		}		
	}

	@Override
	public TransporteImpresionDTO imprimirPolizaSO(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom, Short claveTipoEndoso, String eMail) {
		TransporteImpresionDTO transporte = new TransporteImpresionDTO();
		
		PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, idToPoliza);
		
		DatosCaratulaPoliza datosCaratulaPoliza = new DatosCaratulaPoliza();
		datosCaratulaPoliza.setContenedorDatosImpresion(new ContenedorDatosImpresion());
		datosCaratulaPoliza.setPolizaDTO(polizaDTO);
		
		//Obtenemos el endoso que se desea imprimir
		EndosoDTO endoso = getEndosoAImprimir(polizaDTO.getIdToPoliza(), recordFrom);
		
		//Se obtiene la Bitemporal de la cotización
		BitemporalCotizacion bitemporalCotizacion = getBitemporalCotizacion(validOn, recordFrom, 
				polizaDTO.getCotizacionDTO().getIdToCotizacion(), claveTipoEndoso);

		//Obtenemos datos del Inciso
		List<DatosCaratulaInciso> dataSourceIncisos = impresionBitemporalService.llenarIncisoDTO(
				bitemporalCotizacion, polizaDTO.getNumeroPolizaFormateada(), validOn,
				recordFrom, 0, 1, datosCaratulaPoliza.getPolizaDTO(), datosCaratulaPoliza.getDatosPolizaDTO(),
				endoso.getId().getNumeroEndoso(), claveTipoEndoso, false);
		
		idToNegocio=polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio();
		//Obtenemos datos del Agente
		Agente agente = new Agente();
		agente.setId(bitemporalCotizacion.getValue().getSolicitud().getCodigoAgente().longValue()); 
		agente = agenteMidasDao.loadByIdImpresiones(agente);
		
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, polizaDTO.getCotizacionDTO().getIdToCotizacion());
		
		for(DatosCaratulaInciso inciso : dataSourceIncisos){
			byte[] caratula = imprimirCaratulaSO(inciso, locale, agente, eMail, cotizacion.getFolio());
			if(caratula!=null){
				transporte.setByteArray(caratula);
			}
		}
		
		transporte.setContentType(ConstantesReporte.TIPO_TXT_UTF8);
		transporte.setFileName("Certificado_Seguro_Obligatorio.txt");
		return transporte;
	}

	private byte[] imprimirCaratulaSO(DatosCaratulaInciso inciso, Locale locale, 
			Agente agente, String eMail, String noFolio) {
		byte[] certificado = null;

		try {
			InputStream caratulaStream = this.getClass().
				getResourceAsStream(UR_JRXML_SO+"/Caratula_M2.jasper");
			
			JasperReport reporteInciso = (JasperReport) JRLoader.loadObject(caratulaStream);
			
			InputStream desgloseCoberturasStream = this.getClass().
				getResourceAsStream(UR_JRXML_SO+"/DesgloseCoberturasM2CSO.jasper");
			
			JasperReport reporteCoberturas = (JasperReport) JRLoader.loadObject(desgloseCoberturasStream);
			
			Map<String, Object> parameters = new HashMap<String, Object>(1);
			// Por cada uno de estos DatosIncisoDTO se hará una página
			List<DatosIncisoDTO> reporteDatasourceInciso = new ArrayList<DatosIncisoDTO>(1);
			DatosIncisoDTO diDTO = inciso.getDataSourceIncisos();
			String observaciones = diDTO.getObservaciones();
			
			reporteDatasourceInciso.add(diDTO);
			if (locale != null) {
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
			

			
			// Estas son las coberturas para cada inciso
			List<DatosCoberturasDTO> dcDTOList = inciso.getDataSourceCoberturas();
			parameters.put("coberturasDataSource", dcDTOList);
			parameters.put("coberturasReport", reporteCoberturas);

			parameters.put("idAgente", agente.getIdAgente());
			parameters.put("telCelularAgente",agente.getPersona().getTelefonoCelular());
			parameters.put("title", "CERTIFICADO DE SEGURO PARA\n AUTOMOVILES INDIVIDUALES");
			parameters.put("subtitle", "Póliza No. ");
			parameters.put("numeroTelefonoSPV", listadoService.getSpvData(idToNegocio).get("numeroTelefonoSPV"));
			parameters.put("e_mail", eMail);
			parameters.put("noFolio", noFolio);
			parameters.put("observaciones", observaciones);
			parameters.put(JRParameter.REPORT_VIRTUALIZER,
					new JRGzipVirtualizer(2));
			JRBeanCollectionDataSource dsInciso = new JRBeanCollectionDataSource( reporteDatasourceInciso);
			GestorReportes gestorReportes = new GestorReportes();			
			
			certificado = gestorReportes.generaReporte(ConstantesReporte.TIPO_TXT, reporteInciso, parameters, dsInciso);
		} catch (JRException ex) {
			throw new RuntimeException(ex.getMessage(), ex);
		}
		return certificado;
	}
	
	private BitemporalCotizacion getBitemporalCotizacion(DateTime validOn, DateTime recordFrom, 
			BigDecimal idToCotizacion, Short claveTipoEndoso) {

		BitemporalCotizacion bitemporalCotizacion = null;
		CotizacionContinuity cotizacionContinuity = entidadContinuityService
				.findContinuityByBusinessKey(CotizacionContinuity.class,
						CotizacionContinuity.BUSINESS_KEY_NAME,
						idToCotizacion);

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity.id", cotizacionContinuity.getId());
		List<BitemporalCotizacion> bitempCotizacionList =  (List<BitemporalCotizacion>) entidadBitemporalService.listarFiltrado(BitemporalCotizacion.class, params, validOn, recordFrom, false);
		if(bitempCotizacionList != null && !bitempCotizacionList.isEmpty()){
			bitemporalCotizacion = bitempCotizacionList.get(0);
		}
		
		if (bitemporalCotizacion == null
				&& claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION) {
			Long id = cotizacionContinuity.getId();
			Collection<BitemporalCotizacion> collectionBitemporalCotizacion = entidadBitemporalService
					.obtenerCancelados(BitemporalCotizacion.class, id,
							validOn, recordFrom);
			if (collectionBitemporalCotizacion.size() > 0) {
				bitemporalCotizacion = collectionBitemporalCotizacion
						.iterator().next();
			}
		}
		
		return bitemporalCotizacion;
	}

	/**
	 * Obtenemos el endoso que se desea imprimir
	 * @param idToPoliza
	 * @param recordFrom
	 **/
	private EndosoDTO getEndosoAImprimir(BigDecimal idToPoliza,
			DateTime recordFrom) {
		EndosoDTO endoso = new EndosoDTO();
		
		Map<String, Object> properties = new HashMap<String, Object>(1);
		properties.put("polizaDTO.idToPoliza", idToPoliza);
		// El recordFrom es unico entre endoso
		properties.put("recordFrom", recordFrom.toDate());
		List<EndosoDTO> endosoDTOList = entidadService.findByProperties(EndosoDTO.class, properties);
		if(endosoDTOList!=null && !endosoDTOList.isEmpty()){
			// La lista siempre tiene un solo elemento.
			endoso = endosoDTOList.get(0);
		}

		return endoso;
	}
	
	@Override
	public EdicionPoliza obtenerEdicionPoliza(BigDecimal idToPoliza, DateTime validOn, 
					DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual){
		
		return this.obtenerEdicionPoliza(idToPoliza, validOn, recordFrom, claveTipoEndoso, esSituacionActual, false);
		
	}
	
	/**
	 * Se crea este metodo privado para el caso en que se quiere restaurar la edicion de poliza,
	 * se le agrega el boleano restaurar informacion que si es verdadero vuelve a cargar la 
	 * informacion original de la poliza en la entidad
	 * @param idToPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @param restaurarInformacion
	 * @return
	 */
	private EdicionPoliza obtenerEdicionPoliza(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual, boolean restaurarInformacion){
		EdicionPoliza edicionPoliza;
		DatosCaratulaPoliza datosCaratulaPoliza;
		
		if(esSituacionActual == null){
			esSituacionActual = Boolean.FALSE;
		}
		if (validOn == null) {
			validOn = new DateTime();
		}
		
		try {
			// Se obtienen los datos generales de la poliza
			datosCaratulaPoliza = impresionBitemporalService.llenarPolizaDTO(idToPoliza, validOn, 
					recordFrom,claveTipoEndoso, esSituacionActual);
			// Se obtiene el objeto bitemporal para la cotización con el ID y la
			// correspondiente fechas de validez
			PolizaDTO polizaDTO = datosCaratulaPoliza.getPolizaDTO();
			BigDecimal idToCotizacion = polizaDTO.getCotizacionDTO().getIdToCotizacion();
			BitemporalCotizacion bitemporalCotizacion = getBitemporalCotizacion(validOn, recordFrom, idToCotizacion,claveTipoEndoso);
			this.bitemporalCotizacionDatosAgente=bitemporalCotizacion;
			String datosAgenteCaratula = impresionBitemporalService.getIdentificadorCaratulaPoliza(bitemporalCotizacionDatosAgente).get("datosAgente");
			
			//Obteniendo la edicion de la poliza
			edicionPoliza =  edicionImpresionDao.buscarEdicionPoliza(idToPoliza, validOn, recordFrom, claveTipoEndoso, esSituacionActual);
			
			if(edicionPoliza == null || restaurarInformacion){
				//Cargando la informacion editable en la entidad
				edicionPoliza = new EdicionPoliza();
				edicionPoliza.setHoraInicioVigencia("12");
				edicionPoliza.setHoraFinVigencia("12");
				edicionPoliza.setNombreContratante(datosCaratulaPoliza.getDatosPolizaDTO().getNombreContratante());
				edicionPoliza.setTxDomicilio(datosCaratulaPoliza.getDatosPolizaDTO().getTxDomicilio());
				edicionPoliza.setRfcContratante(datosCaratulaPoliza.getDatosPolizaDTO().getRfcContratante());
				edicionPoliza.setIdToPersonaContratante(datosCaratulaPoliza.getDatosPolizaDTO().getIdToPersonaContratante());
				edicionPoliza.setObservaciones(datosCaratulaPoliza.getDatosPolizaDTO().getObservaciones());
				edicionPoliza.setMontoIva(datosCaratulaPoliza.getDatosPolizaDTO().getMontoIVA());
				edicionPoliza.setDatosAgenteCaratula(datosAgenteCaratula);
			}
			
			edicionPoliza.setDatosPoliza(datosCaratulaPoliza.getDatosPolizaDTO());
			edicionPoliza.setDatosLineasDeNegocio(datosCaratulaPoliza.getDatasourceLineasNegocio());
			edicionPoliza.setEsSeguroObligatorio(esSeguroObligatorio(polizaDTO));
			
		}catch (Exception e) {
			LogDeMidasEJB3.getLogger().log(Level.SEVERE, "obtenerEdicionPoliza " + idToPoliza, e);
			throw new RuntimeException(e);
		}
		
		return edicionPoliza;
	}
	
	@Override
	public boolean existeEdicionPoliza(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual){
		boolean existeEdicionPoliza = false;
		if(esSituacionActual == null){
			esSituacionActual = Boolean.FALSE;
		}
		if (validOn == null) {
			validOn = new DateTime();
		}
		EdicionPoliza edicionPoliza =  edicionImpresionDao.buscarEdicionPoliza(idToPoliza, validOn, recordFrom, claveTipoEndoso, esSituacionActual);
		if(edicionPoliza != null){
			existeEdicionPoliza = true;
		}
		return existeEdicionPoliza;
	}
	
	@Override
	public boolean existeEdicionInciso(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso){
		boolean existeEdicionInciso = false;
		if (validOn == null) {
			validOn = new DateTime();
		}
		EdicionInciso edicionInciso =  edicionImpresionDao.buscarEdicionInciso(idToPoliza, validOn, recordFrom, claveTipoEndoso);
		if(edicionInciso != null){
			existeEdicionInciso = true;
		}
		return existeEdicionInciso;
	}
	
	@Override
	public EdicionPoliza guardarEdicionPoliza(EdicionPoliza edicionCapturada,
			BigDecimal idToPoliza, DateTime validOn, DateTime recordFrom,
			Short claveTipoEndoso, Boolean esSituacionActual) {
		
		if(esSituacionActual == null){
			esSituacionActual = Boolean.FALSE;
		}
		if (validOn == null) {
			validOn = new DateTime();
		}
		
		Long version = edicionImpresionDao.obtenerSecuenciaEdicionPoliza(idToPoliza, validOn, recordFrom, claveTipoEndoso, esSituacionActual);
		EdicionPolizaId id = new EdicionPolizaId(idToPoliza,validOn.toDate(),recordFrom.toDate(),claveTipoEndoso,esSituacionActual,version);
		
		edicionCapturada.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		edicionCapturada.setFechaCreacion(new Date());
		edicionCapturada.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		edicionCapturada.setFechaModificacion(new Date());
		
		edicionCapturada.setId(id);
		
		return entidadService.save(edicionCapturada);
	}

	@Override
	public EdicionPoliza restaurarEdicionPoliza(BigDecimal idToPoliza, DateTime validOn, 
					DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual){
		//Generando la id de la edicion de poliza restaurada
		EdicionPolizaId id = new EdicionPolizaId();
		id.setIdToPoliza(idToPoliza);
		id.setValidOn(validOn.toDate());
		id.setRecordFrom(recordFrom.toDate());
		id.setClaveTipoEndoso(claveTipoEndoso);
		id.setEsSituacionActual((esSituacionActual != null)? 
				esSituacionActual : Boolean.FALSE);
		Long version = edicionImpresionDao.obtenerSecuenciaEdicionPoliza(id.getIdToPoliza(), new DateTime(id.getValidOn()), 
				new DateTime(id.getRecordFrom()), id.getClaveTipoEndoso(), id.getEsSituacionActual());
		id.setVersion(version);
		
		//Recuperando la informacion original de la caratula de la poliza
		EdicionPoliza informacionOriginal = obtenerEdicionPoliza(id.getIdToPoliza(), 
				new DateTime(id.getValidOn()), new DateTime(id.getRecordFrom()), id.getClaveTipoEndoso(), id.getEsSituacionActual(), true);
		
		informacionOriginal.setId(id);
		
		informacionOriginal.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		informacionOriginal.setFechaCreacion(new Date());
		informacionOriginal.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		informacionOriginal.setFechaModificacion(new Date());
		
		informacionOriginal = entidadService.save(informacionOriginal);
		
		return informacionOriginal;
	}

	@Override
	public TransporteImpresionDTO imprimirEdicionPoliza(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual, Locale locale) {
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		JasperReport jReport = null;
		JasperReport jReporteDetalleLineas = null;
		JRBeanCollectionDataSource collectionDataSource = null;
		Map<String, Object> params = null;
	
		try{
		EdicionPoliza edicion = obtenerEdicionPoliza(idToPoliza, validOn, recordFrom, claveTipoEndoso, esSituacionActual);	
		
		jReport = gImpresion.getOJasperReport(UR_JRXML + "/CaratulaPolizaM2.jasper");
		jReporteDetalleLineas = gImpresion.getOJasperReport(UR_JRXML + "/DesgloseLineasM2.jasper");
		
		DatosPolizaDTO datosPoliza = edicion.getDatosPoliza();
		datosPoliza.setNombreContratante(edicion.getNombreContratante());
		datosPoliza.setTxDomicilio(edicion.getTxDomicilio());
		datosPoliza.setRfcContratante(edicion.getRfcContratante());
		datosPoliza.setIdToPersonaContratante(edicion.getIdToPersonaContratante());
		datosPoliza.setObservaciones(edicion.getObservaciones());
		datosPoliza.setMontoIVA(edicion.getMontoIva());
		Double primaNetaTotal = datosPoliza.getPrimaNetaCotizacion() + datosPoliza.getMontoRecargoPagoFraccionado() 
			+ datosPoliza.getDerechosPoliza() + datosPoliza.getMontoIVA();
		datosPoliza.setPrimaNetaTotal(primaNetaTotal);
		
		List<DatosPolizaDTO> dataSourceImpresion = new ArrayList<DatosPolizaDTO>();
		dataSourceImpresion.add(edicion.getDatosPoliza());
		List<DatosLineasDeNegocioDTO> lineasDataSource = edicion.getDatosLineasDeNegocio();
		
		params = new HashMap<String, Object>();
		if (locale != null) {
			params.put(JRParameter.REPORT_LOCALE, locale);
		}
		params.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_SEGUROS_AFIRME);
		params.put(KEY_SIGNATURES, SistemaPersistencia.FIRMA_FUNCIONARIO);
		params.put("lineasDataSource", lineasDataSource);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaRegistro = edicion.isEsSeguroObligatorio()?dateFormat.parse("21/01/2015"):lineasDataSource.get(0).getFechaRegistro();
		String numeroRegistro = edicion.isEsSeguroObligatorio()?"CNSF-S0094-0597-2014":lineasDataSource.get(0).getNumeroRegistro();
		params.put("leyendaCNSF", this.getLeyendaISyF(fechaRegistro,numeroRegistro));
		params.put("datosAgenteCaratula", edicion.getDatosAgenteCaratula());
		params.put("horaInicioVigencia", edicion.getHoraInicioVigencia());
		params.put("horaFinVigencia", edicion.getHoraFinVigencia());
		params.put("imprimirPrimas", edicion.getImprimirPrimas());
		params.put("lineasReport", jReporteDetalleLineas);
		collectionDataSource = new JRBeanCollectionDataSource(dataSourceImpresion);
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
		
	}
	
	@Override
	public EdicionInciso obtenerEdicionInciso(BigDecimal idToPoliza, 
			DateTime validOn, DateTime recordFrom, Short claveTipoEndoso){
		EdicionInciso edicion = null;
		
		try{
			edicion = edicionImpresionDao.buscarEdicionInciso(idToPoliza, validOn, recordFrom, claveTipoEndoso);
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}
		
		return edicion;
	}
	
	@Override
	public EdicionInciso guardarEdicionInciso(EdicionInciso edicionCapturada, BigDecimal idToPoliza, 
			DateTime validOn, DateTime recordFrom, Short claveTipoEndoso, List<DatosCoberturasDTO> datosCoberturas){
		if (validOn == null) {
			validOn = new DateTime();
		}
		
		Long version = edicionImpresionDao.obtenerSecuenciaEdicionInciso(idToPoliza, validOn, recordFrom, claveTipoEndoso);
		EdicionIncisoId id = new EdicionIncisoId(idToPoliza,validOn.toDate(),recordFrom.toDate(),claveTipoEndoso,version);
		
		String usuario = usuarioService.getUsuarioActual().getNombreUsuario();
		
		edicionCapturada.setCodigoUsuarioCreacion(usuario);
		edicionCapturada.setFechaCreacion(new Date());
		edicionCapturada.setCodigoUsuarioModificacion(usuario);
		edicionCapturada.setFechaModificacion(new Date());
		
		edicionCapturada.setId(id);
		
		edicionCapturada.setCoberturas(obtenerListaCoberturasEdicionInciso(datosCoberturas, edicionCapturada, usuario));
		
		return entidadService.save(edicionCapturada);
	}
	
	/**
	 * Crea la lista con la informacion modificada de las coberturas del inciso
	 * @param datosCoberturas
	 * @param edicionInciso
	 * @param usuario
	 * @return
	 */
	private List<EdicionIncisoCobertura> obtenerListaCoberturasEdicionInciso(
			List<DatosCoberturasDTO> datosCoberturas, EdicionInciso edicionInciso, String usuario){
		List<EdicionIncisoCobertura> coberturas = new ArrayList<EdicionIncisoCobertura>();
		if(datosCoberturas != null
				&& !datosCoberturas.isEmpty()){
			for(DatosCoberturasDTO datosCobertura: datosCoberturas){
				EdicionIncisoCobertura cobertura = new EdicionIncisoCobertura();
				cobertura.setRiesgo(datosCobertura.getRiesgo());
				cobertura.setSumaAsegurada(datosCobertura.getSumaAsegurada());
				cobertura.setDeducible(datosCobertura.getDeducible());
				cobertura.setEdicionInciso(edicionInciso);
				cobertura.setCodigoUsuarioCreacion(usuario);
				cobertura.setFechaCreacion(new Date());
				cobertura.setCodigoUsuarioModificacion(usuario);
				cobertura.setFechaModificacion(new Date());
				coberturas.add(cobertura);
		}}
		return coberturas;
	}
	
	@Override
	public EdicionInciso restaurarEdicionInciso(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso){
		//Generando la id de la edicion de inciso restaurada
		EdicionIncisoId id = new EdicionIncisoId();
		id.setIdToPoliza(idToPoliza);
		id.setValidOn(validOn.toDate());
		id.setRecordFrom(recordFrom.toDate());
		id.setClaveTipoEndoso(claveTipoEndoso);
		Long version = edicionImpresionDao.obtenerSecuenciaEdicionInciso(id.getIdToPoliza(), new DateTime(id.getValidOn()), 
				new DateTime(id.getRecordFrom()), id.getClaveTipoEndoso());
		id.setVersion(version);
		
		//Toda la informacion se pone vacia o nula o falso para que al imprimir se tome la informacion original del inciso
		EdicionInciso borrarInformacion = new EdicionInciso();
		
		borrarInformacion.setCalleNumero("");
		borrarInformacion.setCiudad("");
		borrarInformacion.setCodigoPostal("");
		borrarInformacion.setColonia("");
		borrarInformacion.setCoberturas(null);
		borrarInformacion.setDatosAgenteCaratula("");
		borrarInformacion.setHoraFinVigencia("");
		borrarInformacion.setHoraInicioVigencia("");
		borrarInformacion.setIdToPersonaContratante(null);
		borrarInformacion.setImprimirPrimas(Boolean.TRUE);
		borrarInformacion.setImprimirAgenteCaratula(Boolean.TRUE);
		borrarInformacion.setMontoIva(null);
		borrarInformacion.setNombreContratante("");
		borrarInformacion.setNombreContratanteInciso("");
		borrarInformacion.setObservaciones("");
		borrarInformacion.setRfcContratante("");
		borrarInformacion.setTipoServicio("");
		borrarInformacion.setTipoUso("");
		borrarInformacion.setTelefono1("");
		borrarInformacion.setDescTelefono1("");
		borrarInformacion.setTelefono2("");
		borrarInformacion.setDescTelefono2("");
		borrarInformacion.setTelefono3("");
		borrarInformacion.setDescTelefono3("");
		
		borrarInformacion.setId(id);
		borrarInformacion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		borrarInformacion.setFechaCreacion(new Date());
		borrarInformacion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		borrarInformacion.setFechaModificacion(new Date());
		
		borrarInformacion = entidadService.save(borrarInformacion);
		
		return borrarInformacion;
	}
	
	@Override
	public List<DatosCoberturasDTO> obtenerCoberturasEdicionInciso(DateTime validOn, DateTime recordFrom, 
			BigDecimal idToPoliza, Short claveTipoEndoso){
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, idToPoliza);
		BigDecimal idToCotizacion = poliza.getCotizacionDTO().getIdToCotizacion();
		BitemporalCotizacion bitemporalCotizacion = this.getBitemporalCotizacion(validOn, recordFrom, idToCotizacion, claveTipoEndoso);
		List<DatosCoberturasDTO> datosCoberturasEdicionInciso = impresionBitemporalService.obtenerCoberturasEdicionInciso(
				bitemporalCotizacion, validOn, recordFrom, claveTipoEndoso);
		
		EdicionInciso edicion = edicionImpresionDao.buscarEdicionInciso(idToPoliza, validOn, recordFrom, claveTipoEndoso);
		if(edicion != null){
			for(DatosCoberturasDTO informacionOriginal : datosCoberturasEdicionInciso){
				for(EdicionIncisoCobertura informacionEditada: edicion.getCoberturas()){
					if(informacionOriginal.getRiesgo().equals(informacionEditada.getRiesgo())){
						if(!StringUtil.isEmpty(informacionEditada.getSumaAsegurada())){
							informacionOriginal.setSumaAsegurada(informacionEditada.getSumaAsegurada());}
						if(!StringUtil.isEmpty(informacionEditada.getDeducible())){
							informacionOriginal.setDeducible(informacionEditada.getDeducible());}
						break;
		}}}}	
		return datosCoberturasEdicionInciso;
	}
	
	@Override
	public boolean existeEdicionEndoso(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso){
		boolean existeEdicionEndoso = false;
		
		EdicionEndoso edicionEndoso =  edicionImpresionDao.buscarEdicionEndoso(idToCotizacion, recordFrom, claveTipoEndoso);
		if(edicionEndoso != null){
			existeEdicionEndoso= true;
		}
		
		return existeEdicionEndoso;
	}
	
	@Override
	public EdicionEndoso obtenerEdicionEndoso(BigDecimal idToCotizacion, Date recordFrom, 
					Short claveTipoEndoso){
		boolean restaurarInformacion = false;
		return obtenerEdicionEndoso(idToCotizacion, recordFrom, claveTipoEndoso, restaurarInformacion);
	}
	
	/**
	 * Se crea este metodo privado para el caso en que se quiere restaurar la edicion de endoso,
	 * se le agrega el boleano restaurar informacion que si es verdadero vuelve a cargar la 
	 * informacion original de la poliza en la entidad
	 * @param idToCotizacion
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @param restaurarInformacion
	 * @return
	 */
	private EdicionEndoso obtenerEdicionEndoso(BigDecimal idToCotizacion, Date recordFrom, 
					Short claveTipoEndoso, boolean restaurarInformacion){
		EdicionEndoso edicionEndoso = null;
		
		try {
			//Obteniendo la edicion del endoso
			edicionEndoso =  edicionImpresionDao.buscarEdicionEndoso(idToCotizacion, recordFrom, claveTipoEndoso);
			
			if(edicionEndoso == null || restaurarInformacion){
				//Cargando la informacion editable en la entidad
				edicionEndoso = new EdicionEndoso();
				edicionEndoso.setHoraInicioVigenciaEndoso("12");
				edicionEndoso.setHoraFinVigenciaEndoso("12");
				edicionEndoso.setHoraInicioVigenciaPoliza("12");
				edicionEndoso.setHoraFinVigenciaPoliza("12");
			}
			
			edicionEndoso.setInformacionImpresionEndoso(impresionEndosoService.obtenerInformacionImpresionEndoso(idToCotizacion, recordFrom, claveTipoEndoso));
			
		}catch (Exception e) {
			LogDeMidasEJB3.getLogger().log(Level.SEVERE, "obtenerEdicionEndoso " + idToCotizacion, e);
			throw new RuntimeException(e);
		}
		
		return edicionEndoso;
	}
	
	@Override
	public EdicionEndoso guardarEdicionEndoso(EdicionEndoso edicionEndoso, BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso){
		String usuario = usuarioService.getUsuarioActual().getNombreUsuario();
		
		Long version = edicionImpresionDao.obtenerSecuenciaEdicionEndoso(idToCotizacion, recordFrom, claveTipoEndoso);
		EdicionEndosoId id = new EdicionEndosoId(idToCotizacion, recordFrom, claveTipoEndoso, version);
		edicionEndoso.setId(id);
		
		edicionEndoso.setCodigoUsuarioCreacion(usuario);
		edicionEndoso.setFechaCreacion(new Date());
		edicionEndoso.setCodigoUsuarioModificacion(usuario);
		edicionEndoso.setFechaModificacion(new Date());
		
		return entidadService.save(edicionEndoso);
	}
	
	@Override
	public EdicionEndoso restaurarEdicionEndoso(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso){
		EdicionEndoso edicionEndoso = obtenerEdicionEndoso(idToCotizacion, recordFrom, claveTipoEndoso, true);
		guardarEdicionEndoso(edicionEndoso, idToCotizacion, recordFrom, claveTipoEndoso);
		
		return edicionEndoso;
	}
	
	/**
	 * @param impresionBitempService
	 *            the impresionBitempService to set
	 */
	@EJB
	public void setImpresionBitemporalService(
			ImpresionBitemporalService impresionBitemporalService) {
		this.impresionBitemporalService = impresionBitemporalService;
	}

	@EJB
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setEntidadContinuityService(
			EntidadContinuityService entidadContinuityService) {
		this.entidadContinuityService = entidadContinuityService;
	}

	@EJB
	public void setGeneraPlantillaCartaRenovacionService(
			GeneraPlantillaCartaRenovacionService generaPlantillaCartaRenovacionService) {
		this.generaPlantillaCartaRenovacionService = generaPlantillaCartaRenovacionService;
	}

	@EJB
	public void setImpresionRecibosService(
			ImpresionRecibosService impresionRecibosService) {
		this.impresionRecibosService = impresionRecibosService;
	}

	@EJB
	public void setCondicionEspecialService(
			CondicionEspecialService condicionEspecialService) {
		this.condicionEspecialService = condicionEspecialService;
	}

	@EJB
	public void setCondicionEspecialBitemporalService(
			CondicionEspecialBitemporalService condicionEspecialBitemporalService) {
		this.condicionEspecialBitemporalService = condicionEspecialBitemporalService;
	}

	@EJB
	public void setIncisoBitemporalService(
			IncisoBitemporalService incisoBitemporalService) {
		this.incisoBitemporalService = incisoBitemporalService;
	}
	
	@EJB
	public void setSeguroObligatorioService(
			SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}

	@EJB
	public void setCondicionEspecialDao(CondicionEspecialDao condicionEspecialDao) {
		this.condicionEspecialDao = condicionEspecialDao;
	}

	public SeccionFacadeRemote getSeccionFacade() {
		return seccionFacade;
	}

	@EJB
	public void setSeccionFacade(SeccionFacadeRemote seccionFacade) {
		this.seccionFacade = seccionFacade;
	}


	@EJB
	public void setDocumentoAnexoSeccionService(
			DocumentoAnexoSeccionService documentoAnexoSeccionService) {
		this.documentoAnexoSeccionService = documentoAnexoSeccionService;
	}

	@EJB
	public void setDocumentoAnexoCoberturaService(
			DocumentoAnexoCoberturaService documentoAnexoCoberturaService) {
		this.documentoAnexoCoberturaService = documentoAnexoCoberturaService;
	}
	
	@EJB
	public void setAgenteMidasDao(AgenteMidasDao agenteMidasDao) {
		this.agenteMidasDao = agenteMidasDao;
	}
	
	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}	
	
	@EJB
	public void setEdicionImpresionDao(EdicionImpresionDAO edicionImpresionDao){
		this.edicionImpresionDao = edicionImpresionDao;
	}
	
	@EJB(beanName="UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService){
		this.usuarioService = usuarioService;
	}
	
	
	@EJB
	public void setImpresionEndosoService(
			ImpresionEndosoService impresionEndosoService) {
		this.impresionEndosoService = impresionEndosoService;
	}
	
}
