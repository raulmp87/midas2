package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.inciso.configuracion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoFacadeRemote;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionIncisoTipoUsoService;

@Stateless
public class ConfiguracionIncisoTipoUsoServiceImpl implements
		ConfiguracionIncisoTipoUsoService {
	
	private TipoUsoVehiculoFacadeRemote tipoUsoVehiculoFacadeRemote;

	@Override
	public List<TipoUsoVehiculoDTO> findAll() {
		return tipoUsoVehiculoFacadeRemote.findAll();
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Override
	public List<TipoUsoVehiculoDTO> findAll(BigDecimal... ids) {
		String spName="MIDAS.pkgAUT_Servicios.spAUT_TipUsoVehPorTipoVehiculo";
		String [] atributosDTO = { "idTcTipoUsoVehiculo", "descripcionTipoUsoVehiculo" };
		String [] columnasCursor = { "IDTCTIPOUSOVEHICULO", "DESCRIPCIONTIPOUSOVEHICULO"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdLineaNegocio", ids[0]);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO", atributosDTO, columnasCursor);
			
			return storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName,e);
		}
	}
	



	@Override
	public TipoUsoVehiculoDTO findById(BigDecimal arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TipoUsoVehiculoDTO findById(CatalogoValorFijoId arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TipoUsoVehiculoDTO findById(double arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TipoUsoVehiculoDTO> listRelated(Object arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public TipoUsoVehiculoFacadeRemote getTipoUsoVehiculoFacadeRemote() {
		return tipoUsoVehiculoFacadeRemote;
	}
	
	@EJB
	public void setTipoUsoVehiculoFacadeRemote(
			TipoUsoVehiculoFacadeRemote tipoUsoVehiculoFacadeRemote) {
		this.tipoUsoVehiculoFacadeRemote = tipoUsoVehiculoFacadeRemote;
	}
	
}
