package mx.com.afirme.midas2.service.impl.prestamos;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;

import mx.com.afirme.midas2.dao.prestamos.PagarePrestamoAnticipoDao;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.domain.prestamos.PagarePrestamoAnticipo;
import mx.com.afirme.midas2.dto.impresiones.PagarePrestamoAnticipoImpresion;
import mx.com.afirme.midas2.service.prestamos.PagarePrestamoAnticipoService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class PagarePrestamoAnticipoServiceImpl implements PagarePrestamoAnticipoService{

	private PagarePrestamoAnticipoDao dao;	
	@Resource	
	private TimerService timerService;	
	@EJB
	private SistemaContext sistemaContext;
	public static final Logger LOG = Logger.getLogger(PagarePrestamoAnticipoServiceImpl.class);
	
	@Override
	public List<PagarePrestamoAnticipo> loadByIdConfigPrestamo(ConfigPrestamoAnticipo obj) throws Exception {
		return dao.loadByIdConfigPrestamo(obj);
	}

	@Override
	public PagarePrestamoAnticipo save(PagarePrestamoAnticipo obj, Long idConfigPrestamos)throws Exception {
		return dao.save(obj, idConfigPrestamos);
	}

	@Override
	public PagarePrestamoAnticipo save(List<PagarePrestamoAnticipo> list, Long idConfigPrestamos)throws Exception {
		return dao.save(list, idConfigPrestamos);
	}

	@Override
	public PagarePrestamoAnticipo updateEstatusPagare(PagarePrestamoAnticipo obj, String elementoCatalogo)throws Exception {
		return dao.updateEstatusPagare(obj, elementoCatalogo);
	}
	
	public List<PagarePrestamoAnticipo> updateEstatusPagare(List<PagarePrestamoAnticipo> list, String elementoCatalogo) throws Exception {
		return dao.updateEstatusPagare(list, elementoCatalogo);
	}
	
	/************************setters & getters*******************************/

	@EJB
	public void setDao(PagarePrestamoAnticipoDao dao) {
		this.dao = dao;
	}

	@Override
	public boolean llenarYGuardarListaPagares(String pagaresAgregados,ConfigPrestamoAnticipo config) {
		return dao.llenarYGuardarListaPagares(pagaresAgregados, config);
	}

	@Override
	public PagarePrestamoAnticipo loadById(PagarePrestamoAnticipo pagare) throws Exception {
		return dao.loadById(pagare);
	}

	@Override
	public PagarePrestamoAnticipoImpresion infoImpresionPagare(PagarePrestamoAnticipo pagare) throws Exception {
			return dao.infoImpresionPagare(pagare);
	}

	@Override
	public PagarePrestamoAnticipoImpresion infoImpresionPagarePrincipal(ConfigPrestamoAnticipo config) throws Exception {
		return dao.infoImpresionPagarePrincipal(config);
	}

	@Override
	public PagarePrestamoAnticipo aplicaPagare(String pagaresAgregados,ConfigPrestamoAnticipo config,PagarePrestamoAnticipo pagareActivado) throws Exception{
		return dao.aplicaPagare(pagaresAgregados,config, pagareActivado);
	}
	
	@Override
	public void activacionAutomaticaDePagares() throws Exception {
		LOG.info("Ejecutando tarea de activacionAutomaticaDePagares");
		dao.activacionAutomaticaDePagares();
	}
	
	public void initialize() {
		String timerInfo = "TimerActivaPagaresDePrestamosYAnticipos";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0 2 * * ?
				expression.minute(0);
				expression.hour(2);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerActivaPagaresDePrestamosYAnticipos", false));
				
				LOG.info("Tarea TimerActivaPagaresDePrestamosYAnticipos configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerActivaPagaresDePrestamosYAnticipos");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerActivaPagaresDePrestamosYAnticipos:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		try {
			activacionAutomaticaDePagares();
		} catch(Exception e) {
			LOG.error("Error al ejecutar tarea TimerActivaPagaresDePrestamosYAnticipos", e);
		}
			
	}
	
}
