package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.comision;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionId;
import mx.com.afirme.midas2.domain.negocio.bonocomision.NegocioBonoComision;
import mx.com.afirme.midas2.dto.impresiones.DatosNegocioDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.comision.ComisionService;
@Stateless
public class ComisionServiceImpl implements ComisionService {
	private ComisionCotizacionFacadeRemote comisionCotizacionFacadeRemote;
	private SubRamoFacadeRemote subRamoFacadeRemote;
	private EntidadService entidadService;
	@Override
	public void generarComision(CotizacionDTO cotizacion) {
	
		BigDecimal porcentajeComisionAgente=null;
		List<NegocioBonoComision> negocioBonoComisions;
		negocioBonoComisions=entidadService.findByProperty(
				NegocioBonoComision.class, "negocio.idToNegocio",
				cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
		if (negocioBonoComisions!=null && negocioBonoComisions.size()>0 && negocioBonoComisions.get(0)!= null && negocioBonoComisions.get(0).getPctComisionAgte()!=null){
			porcentajeComisionAgente=negocioBonoComisions.get(0).getPctComisionAgte();
		}
		List<SubRamoDTO> listaSubRamosCotizacion = new LinkedList<SubRamoDTO>();
		listaSubRamosCotizacion	= subRamoFacadeRemote
				.getSubRamosPorTipoPoliza(cotizacion.getTipoPolizaDTO()
						.getIdToTipoPoliza());

		for (SubRamoDTO subRamoDTO : listaSubRamosCotizacion) {
			ComisionCotizacionId id = new ComisionCotizacionId();
			id.setIdTcSubramo(subRamoDTO.getIdTcSubRamo());
			id.setIdToCotizacion(cotizacion.getIdToCotizacion());
			id.setTipoPorcentajeComision((short) 1);

			ComisionCotizacionDTO comisionCotizacionDTO = new ComisionCotizacionDTO();
			comisionCotizacionDTO.setId(id);
			comisionCotizacionDTO.setCotizacionDTO(cotizacion);
			comisionCotizacionDTO.setSubRamoDTO(subRamoDTO);
			comisionCotizacionDTO.setValorComisionCotizacion(new BigDecimal(0));
			comisionCotizacionDTO.setClaveAutComision((short) 0);
				if(porcentajeComisionAgente!= null){
				comisionCotizacionDTO.setPorcentajeComisionCotizacion(porcentajeComisionAgente);
				comisionCotizacionDTO.setPorcentajeComisionDefault(porcentajeComisionAgente);
				}else{
				comisionCotizacionDTO.setPorcentajeComisionCotizacion(new BigDecimal(subRamoDTO
						.getPorcentajeMaximoComisionRO()));
				comisionCotizacionDTO.setPorcentajeComisionDefault(new BigDecimal(subRamoDTO
						.getPorcentajeMaximoComisionRO()));
				}
			comisionCotizacionFacadeRemote.save(comisionCotizacionDTO);
		}
		
	}
	
	@EJB
	public void setComisionCotizacionFacadeRemote(
			ComisionCotizacionFacadeRemote comisionCotizacionFacadeRemote) {
		this.comisionCotizacionFacadeRemote = comisionCotizacionFacadeRemote;
	}
	
	@EJB
	public void setSubRamoFacadeRemote(SubRamoFacadeRemote subRamoFacadeRemote) {
		this.subRamoFacadeRemote = subRamoFacadeRemote;
	}
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

}
