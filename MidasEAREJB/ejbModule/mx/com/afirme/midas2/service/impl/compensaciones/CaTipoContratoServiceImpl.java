/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 

package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoContratoDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoContrato;
import mx.com.afirme.midas2.service.compensaciones.CaTipoContratoService;

import org.apache.log4j.Logger;

@Stateless

public class CaTipoContratoServiceImpl  implements CaTipoContratoService {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@EJB
	private CaTipoContratoDao tipoContratocaDao;
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaTipoContratoServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaTipoContrato entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoContrato entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoContrato entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoContrato 	::		CaTipoContratoServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	tipoContratocaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoContrato 	::		CaTipoContratoServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoContrato 	::		CaTipoContratoServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaTipoContrato entity.
	  @param entity CaTipoContrato entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoContrato entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoContrato 	::		CaTipoContratoServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaTipoContrato.class, entity.getId());
	        	tipoContratocaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoContrato 	::		CaTipoContratoServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoContrato 	::		CaTipoContratoServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaTipoContrato entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoContrato entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoContrato entity to update
	 @return CaTipoContrato the persisted CaTipoContrato entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaTipoContrato update(CaTipoContrato entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoContrato 	::		CaTipoContratoServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoContrato result = tipoContratocaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoContrato 	::		CaTipoContratoServiceImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoContrato 	::		CaTipoContratoServiceImpl	::	update	::	ERROR	::	",re);
            throw re;
        }
    }
    
    public CaTipoContrato findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoContratoServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoContrato instance = tipoContratocaDao.findById(id);//entityManager.find(CaTipoContrato.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoContratoServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoContratoServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaTipoContrato entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoContrato property to query
	  @param value the property value to match
	  	  @return List<CaTipoContrato> found by query
	 */
    public List<CaTipoContrato> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoContratoServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoContrato model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoContratoServiceImpl	::	findByProperty	::	FIN	::	");
			return tipoContratocaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoContratoServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoContrato> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoContrato> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoContrato> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoContrato> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaTipoContrato entities.
	  	  @return List<CaTipoContrato> all CaTipoContrato entities
	 */
	public List<CaTipoContrato> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoContratoServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoContrato model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoContratoServiceImpl	::	findAll	::	FIN	::	");
			return tipoContratocaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoContratoServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}