/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoCompensacionDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCompensacion;
import mx.com.afirme.midas2.service.compensaciones.CaTipoCompensacionService;

import org.apache.log4j.Logger;

@Stateless

public class CaTipoCompensacionServiceImpl  implements CaTipoCompensacionService {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@EJB
	private CaTipoCompensacionDao tipoCompensacioncaDao;
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaTipoCompensacionServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaTipoCompensacion entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoCompensacion entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoCompensacion entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoCompensacion 	::		CaTipoCompensacionServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	tipoCompensacioncaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoCompensacion 	::		CaTipoCompensacionServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoCompensacion 	::		CaTipoCompensacionServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaTipoCompensacion entity.
	  @param entity CaTipoCompensacion entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoCompensacion entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoCompensacion 	::		CaTipoCompensacionServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaTipoCompensacion.class, entity.getId());
	        	tipoCompensacioncaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoCompensacion 	::		CaTipoCompensacionServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoCompensacion 	::		CaTipoCompensacionServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaTipoCompensacion entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoCompensacion entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoCompensacion entity to update
	 @return CaTipoCompensacion the persisted CaTipoCompensacion entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaTipoCompensacion update(CaTipoCompensacion entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoCompensacion 	::		CaTipoCompensacionServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoCompensacion result = tipoCompensacioncaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoCompensacion 	::		CaTipoCompensacionServiceImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoCompensacion 	::		CaTipoCompensacionServiceImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaTipoCompensacion findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoCompensacionServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoCompensacion instance = tipoCompensacioncaDao.findById(id);//entityManager.find(CaTipoCompensacion.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoCompensacionServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoCompensacionServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaTipoCompensacion entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoCompensacion property to query
	  @param value the property value to match
	  	  @return List<CaTipoCompensacion> found by query
	 */
    public List<CaTipoCompensacion> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoCompensacionServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoCompensacion model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoCompensacionServiceImpl	::	findByProperty	::	FIN	::	");
			return tipoCompensacioncaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoCompensacionServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoCompensacion> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoCompensacion> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoCompensacion> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoCompensacion> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaTipoCompensacion entities.
	  	  @return List<CaTipoCompensacion> all CaTipoCompensacion entities
	 */
	public List<CaTipoCompensacion> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoCompensacionServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoCompensacion model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoCompensacionServiceImpl	::	findAll	::	FIN	::	");
			return tipoCompensacioncaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoCompensacionServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}