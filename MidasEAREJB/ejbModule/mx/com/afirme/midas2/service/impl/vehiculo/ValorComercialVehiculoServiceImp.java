package mx.com.afirme.midas2.service.impl.vehiculo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.vehiculo.ValorComercialVehiculoDao;
import mx.com.afirme.midas2.domain.vehiculo.ValorComercialVehiculo;
import mx.com.afirme.midas2.dto.siniestros.DatosEstimacionCoberturaDTO;
import mx.com.afirme.midas2.dto.vehiculo.ValorReferenciaComercialAutoDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.vehiculo.ValorComercialVehiculoService;

import org.joda.time.DateTime;
import org.joda.time.Months;


@Stateless
public class ValorComercialVehiculoServiceImp implements ValorComercialVehiculoService  {
	
	
	private EntidadService entidadService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	public UsuarioService usuarioService;
	
	@EJB
	private ValorComercialVehiculoDao valorComercialVehiculoDao;

	@Override
	public List<ValorComercialVehiculo> cargarRegistros(
			List<ValorComercialVehiculo> listaRegistros) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int desactivarRegistro(ValorComercialVehiculo registro) {
		
		try{
		
			// # BUSCAR DATOS DEL REGISTRO
			registro = this.entidadService.findById(ValorComercialVehiculo.class, registro.getId() );
			registro.setEstatusRegistro((short) 0);
			registro.setCodigoUsuarioModificacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
			registro.setFechaModificacion(new Date());
			
			this.entidadService.save(registro);
			
			return 1;
		}catch(Exception e){
			return 0;
		}
		
	}

	@Override
	public List<ValorComercialVehiculo> findByFilters(
			ValorComercialVehiculo filtro) {
		
		// # TRUE - RECIENTE | FALSE - TODAS
		boolean tipo = ( filtro.getTipoBusquedaFecha() == 1 ) ? true:false;
		
		return this.valorComercialVehiculoDao.findByFilters(filtro, tipo);
	}

	@Override
	public List<Integer> obtenerAniosValorComercial() {
		
		return this.valorComercialVehiculoDao.obtenerAnioValorComercial();
		
	}

	@Override
	public List<Integer> obtenerAnioModelos() {
		return this.valorComercialVehiculoDao.obtenerAnioModelo();
	}

	@Override
	public String validarRegistro(ValorComercialVehiculo registro, int tipo) {

		String mensaje = "";
		switch(tipo){
			case 1: // MES NO MAYOR A 2 MESES
				SimpleDateFormat dd = new SimpleDateFormat("dd");
				SimpleDateFormat ymd = new SimpleDateFormat("yyyy/MM/dd");
				Date hoy = new Date();
				DateTime hoyDt = new DateTime(hoy);
				
				String fechaBase = registro.getValorComercialAnio()+"/"+registro.getValorComercialMes()+"/"+dd.format(hoy);
				try {
					Date dFechaBase = ymd.parse(fechaBase);
					DateTime dFechaBaseDt = new DateTime(dFechaBase);
					
					Months meses = Months.monthsBetween(hoyDt,dFechaBaseDt);
					
					if ( meses.getMonths() > 2 ){
						mensaje = "El año:"+registro.getValorComercialAnio()+" y mes: "+registro.getValorComercialMes()+" del valor comercial es Mayor a 2 meses. ";
					}
					
				} catch (ParseException e) {
					mensaje = "Error: el año: "+registro.getValorComercialAnio()+" o mes: "+registro.getValorComercialMes()+" ingresado no pudo ser usado para la validación requerida. ";
				}
				
				break;
			case 2: // REGISTROS DUPLICADOS
				
				Map<String,Object> valorComercialParams  = new LinkedHashMap<String, Object>();
				valorComercialParams.put("claveAmis"         , registro.getClaveAmis() );
				valorComercialParams.put("valorComercialMes"  , registro.getValorComercialMes() );
				valorComercialParams.put("valorComercialAnio", registro.getValorComercialAnio() );
				
				List<ValorComercialVehiculo> lValorComercialVehiculo = this.entidadService.findByProperties(ValorComercialVehiculo.class, valorComercialParams);
				
				if ( !lValorComercialVehiculo.isEmpty() ){
					
					boolean esIgualValorComercial = false;
					
					// # ITERAR PARA ENCONTRAR SI LA CANTIDAD ES IGUAL O DIFRENTE
					for(ValorComercialVehiculo lvcv : lValorComercialVehiculo){
						if ( lvcv.getValorComercial().equals( registro.getValorComercial() ) ){
							esIgualValorComercial = true;
							break;
						}
					}
					
					if( esIgualValorComercial ){
						mensaje = "*** Registro ya existe *** La clave amis: "+registro.getClaveAmis()+", el mes: "+ this.convertirMeses( registro.getValorComercialMes() )+" ,el año "+registro.getValorComercialAnio()+" por la cantidad: "+registro.getValorComercial()+" ya se encuentran registrados en la base de datos. ";
					}else{
						mensaje = "*** Se registró el monto de Suma Asegurada, sin embargo ya existía un monto previamente registrado para el mismo Período con los datos siguientes: amis - "+registro.getClaveAmis()+", mes - "+registro.getValorComercialMes()+", el año "+registro.getValorComercialAnio()+" y el monto - "+registro.getValorComercial()+" ya se encuentran registrados en la base de datos. ";
					}
				}
				
				break;
		}

		return mensaje;
	}
	
	
	private String convertirMeses(int mes){
		
		String nombreMes = "";
		
		if ( mes == 0 ){
			nombreMes = "Enero";
		}else if ( mes == 2 ){
			nombreMes = "Febrero";
		}else if ( mes == 3 ){
			nombreMes = "Marzo";
		}else if ( mes == 4 ){
			nombreMes = "Abril";
		}else if ( mes == 5 ){
			nombreMes = "Mayo";
		}else if ( mes == 6 ){
			nombreMes = "Junio";
		}else if ( mes == 7 ){
			nombreMes = "Julio";
		}else if ( mes == 8 ){
			nombreMes = "Agosto";
		}else if ( mes == 9 ){
			nombreMes = "Septiembre";
		}else if ( mes == 10 ){
			nombreMes = "Octubre";
		}else if ( mes == 11 ){
			nombreMes = "Noviembre";
		}else if ( mes == 12 ){
			nombreMes = "Diciembre";
		}
		
		return nombreMes;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	
	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.service.vehiculo.ValorComercialVehiculoService#getValorComercial(mx.com.afirme.midas2.dto.siniestros.DatosEstimacionCoberturaDTO, java.lang.Long, short, java.util.Date)
	 */
	@Override
	public void getValorComercial(DatosEstimacionCoberturaDTO datosEstimacion,Long claveAmis, short modelo, Date fechaConsulta) {
		List<ValorReferenciaComercialAutoDTO> valoresReferenciaList 	= null;
		Long idProveedor 												= null;
		
		valoresReferenciaList = valorComercialVehiculoDao.getValorComercial(claveAmis, modelo, fechaConsulta);
		
		if(valoresReferenciaList != null && valoresReferenciaList.size()>0){
			for( ValorReferenciaComercialAutoDTO valor : valoresReferenciaList){
				if( idProveedor != null){
					if( !idProveedor.equals( valor.getIdProveedor() ) ){
						datosEstimacion.setSumaAseguradaProporcionadaOpcion2( valor.getValorComercial() );
						break;
					}
					
				}else{
					idProveedor = valor.getIdProveedor();
					datosEstimacion.setSumaAseguradaProporcionadaOpcion1( valor.getValorComercial() );
				}
				
			}
			
			
		}

	}

}
