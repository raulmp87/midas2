package mx.com.afirme.midas2.service.impl;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoAplica;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoClaveAmis;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasivaDet;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.ValidacionService;
import mx.com.afirme.midas2.service.bonos.ConfigBonoRangoAplicaService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.negocio.estadodescuento.NegocioEstadoDescuentoService;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.anexos.DocAnexosService;

@Stateless
public class ValidacionServiceImpl implements ValidacionService {
	public static final Logger LOG = Logger.getLogger(ValidacionServiceImpl.class); 
	/* Properties */
	private CotizacionService cotizacionService;
	private EntidadService entidadService;
	private AgenteMidasService agenteMidasService;
	private SistemaContext sistemaContext;
	private FortimaxService fortimaxService;
	private ClienteFacadeRemote clienteFacade;
	private DocAnexosService docAnexosService;
	private ConfigBonoRangoAplicaService configBonoRangoAplicaService;
	private NegocioEstadoDescuentoService negocioEstadoDescuentoService;
	private RenovacionMasivaService renovacionMasivaService;
	private ListadoService listadoService;
	public ValidacionServiceImpl() {

	}

	/* Methods */
	@Override
	public Map<Boolean,Double> validaDescuentoGlobal(Double porcentaje,
			BigDecimal idToCotizacion) {
		CotizacionDTO cotizacionDTO = entidadService.findById(
				CotizacionDTO.class, idToCotizacion);
		if (cotizacionDTO != null) {
			cotizacionDTO.setPorcentajeDescuentoGlobal(porcentaje);
			return cotizacionService.compararDescuento(cotizacionDTO);
		}

		return null;
	}
	
	@Override
	public Boolean validaFechaCotizacion(String fechaInicioVigencia,
			String fechaFinVigencia, BigDecimal idToCotizacion) {
		Date iniDate = null;
		Date finDate = null;
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");		
		try{
			iniDate = format.parse(fechaInicioVigencia);
			finDate = format.parse(fechaFinVigencia);
		}catch(ParseException ex){
			throw new RuntimeException("Imposible convertir las fechas " + fechaFinVigencia + " y " + fechaFinVigencia, ex);
		}		
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		if(cotizacionDTO != null){		
			Map<Integer,Integer> result = cotizacionService.compararVigenciaCotizacionPorProducto(cotizacionDTO,iniDate,finDate);
			if(result != null && (result.containsKey(Integer.valueOf(1)) || result.containsKey(0))){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public Map<Integer,Integer> validaFechaCotizacion(Date fechaInicioVigencia,
			Date fechaFinVigencia, BigDecimal idToCotizacion) {
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		if(cotizacionDTO != null){
				return cotizacionService.compararVigenciaCotizacionPorProducto(cotizacionDTO,fechaInicioVigencia,fechaFinVigencia);
			}
		return null;
	}
	
	/**
	 * Obtiene las lineas de negocio asociadas a una
	 * linea de negocio
	 * @return true si tiene mas de 1 seccion asociada
	 */
	@Override
	public Boolean obtenerLineasNegocioAsociadas(BigDecimal idToPoliza) {
		
		if( idToPoliza.intValue() <= 0 ) {
			return false;
		}
		List<SeccionDTO> seccionDTOs = null;
		try{
			NegocioTipoPoliza negTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, idToPoliza);
			if (negTipoPoliza != null) {
				seccionDTOs = entidadService.findByProperty(SeccionDTO.class,"tipoPolizaDTO",negTipoPoliza.getTipoPolizaDTO());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
			if (seccionDTOs != null && seccionDTOs.size() > 0) {
				return true;
		}
		return false;
	}
	
	public boolean numeroAgenteValido(Long numeroAgente){
		boolean isValid=true;
        Agente filtro=new Agente();
        filtro.setIdAgente(numeroAgente);
        List<Agente> lista=agenteMidasService.findByFilters(filtro);
        //Si la lista esta vacia o es nula, significa que no hay ninguna agente con esa clave, por lo tanto es valida.
        if(lista!=null&& !lista.isEmpty()){
              //Sino hay que validar que si existe un agente con esa clave no sea el mismo agente(en el caso de actualizacion ya que se vuelve a validar la clave del agente)
              Long idAgenteValidar=numeroAgente;
              ciclo:
              for(Agente a:lista){
                   if(a.getIdAgente().equals(idAgenteValidar)){
                         //Si ya hay un agente con esta clave se verifica que no sea el mismo
                         //Si hay un agente con la misma clave pero diferente id, significa que otro agente ya tiene asignado esa clave, entonces no es valido.
                         if(!a.getId().equals(numeroAgente)){
                               isValid=false;
                               break ciclo;
                         }
                   }
              }
        } 
        return isValid;
	}
	
	@Override 
	public boolean validarExisteCrecimiento(Long idConfiguracion){
		boolean noRangoCrecimiento=true;
		List<ConfigBonoRangoAplica>rangos = entidadService.findByProperty(ConfigBonoRangoAplica.class, "configBono.id",idConfiguracion);
		for(ConfigBonoRangoAplica rang:rangos){
			if (rang.getPctCrecimientoInicial() != null
					&& rang.getPctCrecimientoFinal() != null
					&& (rang.getPctCrecimientoFinal().compareTo(BigDecimal.ZERO) != 0 
							|| rang.getPctCrecimientoInicial().compareTo(BigDecimal.ZERO) != 0)) {
				noRangoCrecimiento = false;
				break;
			}
		}
		return noRangoCrecimiento;
	}
	
	@Override
	public Boolean validaSupMetaXConfigBono(Long idConfiguracion) {
		boolean noRangoSupMeta=true;
		List<ConfigBonoRangoAplica>rangos = entidadService.findByProperty(ConfigBonoRangoAplica.class, "configBono.id",idConfiguracion);
		for(ConfigBonoRangoAplica rang:rangos){
			if(rang.getPctSupMetaInicial() !=null 
					&& rang.getPctSupMetaFinal() !=null
							&& (rang.getPctSupMetaFinal().compareTo(BigDecimal.ZERO) != 0 
									|| rang.getPctSupMetaFinal().compareTo(BigDecimal.ZERO) != 0)){
				noRangoSupMeta=false;
				break;
			}
		}
		return noRangoSupMeta;
	}
	
	@Override
	public Boolean existeRangoConfigurado(Long idConfiguracion) {
		Boolean existeRangoConfigurado = false;
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("configBono.id", idConfiguracion);
		Long existe =this.entidadService.findByPropertiesCount(ConfigBonoRangoAplica.class, map);
		if(existe>0){
			existeRangoConfigurado = true;
		}
		System.out.println("*************************el valor de existeRangoConfigurado= "+existeRangoConfigurado);
		return existeRangoConfigurado;
	}
	@Override
	public Boolean validaTamanioCampoBonificacion(Long arg0) {
		// TODO Auto-generated method stub
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("configBono.id", arg0);
		List <ConfigBonoRangoAplica> lista = this.entidadService.findByProperties(ConfigBonoRangoAplica.class, param);
		Boolean a = new Boolean(false);
		int cont =0;
		for(ConfigBonoRangoAplica obj : lista){
			if(tamanioCampoBonif(obj.getValorAplicacion())==false){
				cont++;
			}
		}
		return (cont>0)?false:true;
	}
	
	private  boolean tamanioCampoBonif (BigDecimal object){ 
		 Pattern p = Pattern.compile("^([0-9]{0,4})(\\.[0-9]{1,2})?$");
		 if(object!=null){
			 Matcher m = p.matcher(object.toString());
			 return m.find();
		 }else{
			 return true;
		 }
	}

	@Override
	public Map<Boolean,String> validaDescuentoPorEstado(Double porcentaje,
			Long idToNegocio, String idToEstado, BigDecimal idToCotizacion, Long idToNegPaqueteSeccion) {
		CotizacionDTO cotizacionDTO = null;
		Double descuentoMaximo = 0.0;
		boolean permiteDescEdo = false;
		
		if(idToNegPaqueteSeccion != null) {
			permiteDescEdo = listadoService.getAplicaDescuentoNegocioPaqueteSeccion(idToNegPaqueteSeccion);
		}
		
		LOG.info("Validar descuento por estado max para cot " + idToCotizacion + " --> " +permiteDescEdo);
		
		Map<Boolean, String> map = new HashMap<Boolean, String>(1);
		if(idToNegocio != null && idToEstado != null && permiteDescEdo) {
			NegocioEstadoDescuento negocioEstadoDescuento = negocioEstadoDescuentoService.findByNegocioAndEstado(
					idToNegocio, idToEstado);
			if(negocioEstadoDescuento != null && negocioEstadoDescuento.getPctDescuento() != null) {
				descuentoMaximo = negocioEstadoDescuento.getPctDescuento();
			}
		}
		
		//Renovaciones
		if(idToCotizacion != null) {
			cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
			if(cotizacionDTO.getSolicitudDTO() != null && cotizacionDTO.getSolicitudDTO().getEsRenovacion() != null &&
					cotizacionDTO.getSolicitudDTO().getEsRenovacion().intValue() == 1){
				Map<String,Object> params = new HashMap<String, Object>();
				params.put("idToCotizacion", idToCotizacion);
				params.put("claveEstatus", OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_COTIZACION);
				List<OrdenRenovacionMasivaDet> resultList = entidadService.findByProperties(OrdenRenovacionMasivaDet.class, params);
				if(resultList != null && resultList.size() > 0) {
					for(OrdenRenovacionMasivaDet element: resultList){
						//Aplica el descuento por estado + descuento por no siniestro
						if(element.getDescuentoNoSiniestro() != null) {
							descuentoMaximo = descuentoMaximo + element.getDescuentoNoSiniestro();
						}
						
						break;
					}
				} else {
					//Renovacion directa
					descuentoMaximo = 100.0;
				}
			}
		}
		
		if(descuentoMaximo > 100) {
			descuentoMaximo = 100.0;
		}
		
		if (permiteDescEdo){
			//Si se permite desc por edo entonces el desc que se esta aplicando puede ser por: 
			//a) desc por estado, b) desc x edo y desc por igualacion, c) desc por edo + desc por no siniestro
			if (porcentaje > descuentoMaximo) {
				map.put(true, descuentoMaximo.toString());
			} else {
				map.put(false, porcentaje.toString());
			}
		} else {
			//Si no se permite desc por edo entonces el desc que se esta aplicando es por igualacion
			map.put(false, "NO APLICA");
		}
		return map;
	}

	@Override
	public Boolean validaRangosGridAmis(List<ConfigBonoRangoClaveAmis> lista) {
		
		Collections.sort(lista,  
				new Comparator<ConfigBonoRangoClaveAmis>(){
			public int compare(ConfigBonoRangoClaveAmis object1,ConfigBonoRangoClaveAmis object2) {
				if(object1!=null && object2!=null){
					if(object1.getCve_amis_ini()!=null && object2.getCve_amis_ini()!=null){
						return Long.valueOf(object1.getCve_amis_ini()).compareTo(Long.valueOf(object2.getCve_amis_ini()));
					}else{
						return 0;
					}
				}else{
				return 0;
				}
			}
		});

		int cont=0;
		Boolean resultado = true;
		if(!lista.isEmpty() && lista.size()>1){
			for(ConfigBonoRangoClaveAmis a:lista){
				if(isNotNull(a)){
					cont++;
					ConfigBonoRangoClaveAmis b = new ConfigBonoRangoClaveAmis();
					if(cont<=lista.size()-1){
						b = lista.get(cont);
						if(cont < lista.size()){
							if (compararDosRangosString(a.getCve_amis_fin(),b.getCve_amis_ini())==false){
								resultado=false;
								break;
							}
						}
					}
				}
			}
		}
		return resultado;
	}
	
	@Override
	public Boolean validarContratanteAsignadoCotizacion(Long idToCotizacion) {
		CotizacionDTO cotizacion = cotizacionService.obtenerCotizacionPorId(BigDecimal.valueOf(idToCotizacion));
		return cotizacion != null && cotizacion.getIdToPersonaContratante() != null;
	}
	
	private  Boolean compararDosRangosString(String a2, String b1){
		if(isNotNull(a2) && isNotNull(b1)){
			if(Long.valueOf(b1).compareTo(Long.valueOf(a2))==1){
				return true;
			}else{
				return false;
			}
		}else {
			return false;
		}
	}
	/* Set & Get */
	@EJB
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}	
	
	@EJB	
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}

	@EJB
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}
	
	@EJB
	public void setClienteFacade(ClienteFacadeRemote clienteFacade) {
		this.clienteFacade = clienteFacade;
	}
	
	@EJB
	public void setDocAnexosService(DocAnexosService docAnexosService) {
		this.docAnexosService = docAnexosService;
	}

	@EJB
	public void setConfigBonoRangoAplicaService(
			ConfigBonoRangoAplicaService configBonoRangoAplicaService) {
		this.configBonoRangoAplicaService = configBonoRangoAplicaService;
	}

	@EJB
	public void setNegocioEstadoDescuentoService(
			NegocioEstadoDescuentoService negocioEstadoDescuentoService) {
		this.negocioEstadoDescuentoService = negocioEstadoDescuentoService;
	}
	
	@EJB
	public void setRenovacionMasivaService(
			RenovacionMasivaService renovacionMasivaService) {
		this.renovacionMasivaService = renovacionMasivaService;
	}

	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

}
