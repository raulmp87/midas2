package mx.com.afirme.midas2.service.impl.siniestros.catalogo.oficina;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.catalogo.oficina.CatalogoSiniestroOficinaDao;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.OficinaEstado;
import mx.com.afirme.midas2.domain.sistema.parametro.ParametroGlobal;
import mx.com.afirme.midas2.domain.sistema.parametro.ParametroGlobalId;
import mx.com.afirme.midas2.service.impl.siniestros.catalogo.CatalogoSiniestroServiceImpl;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.oficina.CatalogoSiniestroOficinaService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.StringUtil;

@Stateless
public class CatalogoSiniestroOficinaServiceImpl extends CatalogoSiniestroServiceImpl implements CatalogoSiniestroOficinaService  {
	
	private static int APLICACION_PARAM_GLOBAL = 5;
	private static String POSTFIJO_OFICINA_WS ="_WS_ENDPOINT";
	
	private static final Logger log = Logger.getLogger(CatalogoSiniestroOficinaServiceImpl.class);
	
	@EJB
	private CatalogoSiniestroOficinaDao catalogoSiniestroOficinaDao;

	@EJB
	private CatalogoSiniestroService catalogoSiniestroService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private ParametroGlobalService parametroGlobalService;
	
	@Override
	public List<EstadoMidas> estadosDisponiblesPorPais(String paisId, Long oficinaId) {
		return catalogoSiniestroOficinaDao.estadosDisponiblesPorPais(paisId, oficinaId);
	}

	@Override
	public Oficina salvarOficina(Oficina oficina) {
		oficina.setFechaCreacion(new Date());
		oficina.setFechaModificacion(new Date());
		//Por default, se crea la oficina como inactivo
		oficina.setEstatus(0);
		oficina.setConsecutivoReferencia((long) contadorOficinas());
		oficina.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		oficina.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		
		// SALVAR DATOS EN PARAMETRO GLOBAL
		if( oficina.getIsWebService() ) {
			this.generaEndpointOficina(oficina,true);
		}
		
		return entidadService.save(oficina);
	}

	private int contadorOficinas(){
		// OBTIENE ULTIMO VALOR INSERTAR Y LE SUMA UNO PARA GUARDARSE COMO CONSECUTIVO_REFERENCIA
		List<Oficina> lOficina = this.entidadService.findAll(Oficina.class);
		return lOficina.size()+1 ;
	}

	@Override
	public Oficina actualizarDatosOficina(Oficina oficina, Long idEntidad) {
		Oficina entidadPrevia =  entidadService.findById(Oficina.class, idEntidad);		
		if(entidadPrevia.getEstatus().intValue() != oficina.getEstatus().intValue()){
			if( oficina.getEstatus().equals(1)){
				oficina.setFechaCambioEstatus(null);
			}else{
				oficina.setFechaCambioEstatus(new Date());
			}
		}
		oficina.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		oficina.setFechaModificacion(new Date());
		oficina.setConsecutivoReferencia(entidadPrevia.getConsecutivoReferencia());
		oficina.setCodigoUsuarioCreacion(entidadPrevia.getCodigoUsuarioCreacion());
		oficina.setFechaCreacion(entidadPrevia.getFechaCreacion());
		oficina.setOficinaEstados(entidadPrevia.getOficinaEstados());
		
		// VALIDACION DE ENDPOINT 
		if( oficina.getIsWebService() ) {
			this.generaEndpointOficina(oficina,true);
		}else {
			
			// SI ES FALSO
			// PUEDE EXISTIR (ENTIDAD_PREVIA ES TRUE) Y SE DESACTIVA
			// PUEDE NO EXISTIR Y SE CREA
			
			if( entidadPrevia.getIsWebService() == true ) {
				this.generaEndpointOficina(oficina,false);
			}else {
				this.generaEndpointOficina(oficina,true);
			}
			
		}
		
		return entidadService.save(oficina);	
	}

	@Override
	public Oficina relacionarOficina(Long idOficina,
			EstadoMidas estadoTransaccion) {
		
		List<Oficina> listaOficinas = new ArrayList<Oficina>();
		OficinaFiltro filtroOficina = new OficinaFiltro();
		OficinaEstado nuevaRealacion = new OficinaEstado();
		Oficina entidad = new Oficina();
		
		filtroOficina.setId(Integer.valueOf(idOficina.toString()));
		listaOficinas =  catalogoSiniestroService.buscar(Oficina.class, filtroOficina);
		entidad = listaOficinas.get(0);
		nuevaRealacion.setEstado(estadoTransaccion);
		nuevaRealacion.setOficina(entidad);
		entidad.getOficinaEstados().add(nuevaRealacion);
			
		return entidadService.save(entidad);
	}

	public CatalogoSiniestroService getCatalogoSiniestroService() {
		return catalogoSiniestroService;
	}

	public void setCatalogoSiniestroService(
			CatalogoSiniestroService catalogoSiniestroService) {
		this.catalogoSiniestroService = catalogoSiniestroService;
	}

	@Override
	public Oficina eliminarRelacion(Long idOficina, String idEstadoTrans) {
		
		List<Oficina> listaOficinas = new ArrayList<Oficina>();
		OficinaFiltro filtroOficina = new OficinaFiltro();
		Oficina entidad = new Oficina();
		
		filtroOficina.setId(Integer.valueOf(idOficina.toString()));
		listaOficinas =  catalogoSiniestroService.buscar(Oficina.class, filtroOficina);
		entidad = listaOficinas.get(0);
		
		for(OficinaEstado relacionOficina:entidad.getOficinaEstados()){				
			if(relacionOficina.getEstado().getId().equals(idEstadoTrans)){
				entidad.getOficinaEstados().remove(relacionOficina);
				break;
			}
			
		}

		return entidadService.save(entidad);
	}

	@Override
	public Oficina relacionarTodosEstados(Long idOficina,
			List<EstadoMidas> estadosDisponibles) {
		
		List<Oficina> listaOficinas = new ArrayList<Oficina>();
		OficinaFiltro filtroOficina = new OficinaFiltro();
		filtroOficina.setId(Integer.valueOf(idOficina.toString()));
		listaOficinas =  catalogoSiniestroService.buscar(Oficina.class, filtroOficina);
		Oficina entidad = new Oficina();
		entidad = listaOficinas.get(0);
		
		for(EstadoMidas estadoDisponible: estadosDisponibles){
			
			OficinaEstado nuevaRealacion = new OficinaEstado();
			nuevaRealacion.setEstado(estadoDisponible);
			nuevaRealacion.setOficina(entidad);
			entidad.getOficinaEstados().add(nuevaRealacion);
		
		}
		
		return entidadService.save(entidad);
	}

	@Override
	public Oficina eliminarRelacionTodosEstados(Long idOficina) {
		
		OficinaFiltro filtroOficina = new OficinaFiltro();
		List<Oficina> listaOficinas = new ArrayList<Oficina>();
		
		filtroOficina.setId(Integer.valueOf(idOficina.toString()));
		listaOficinas =  catalogoSiniestroService.buscar(Oficina.class, filtroOficina);
		Oficina entidad = new Oficina();
		entidad = listaOficinas.get(0);
		System.out.println(entidad.getOficinaEstados().size());
		entidad.setOficinaEstados(new HashSet<OficinaEstado>());
		
		return entidadService.save(entidad);
	}
	
	@Override
	public String validaOficinaConTipoDeServicio(Long oficinaId, String tipoServicio){
		String mensaje = null;
		if(oficinaId!=null && tipoServicio != null && !tipoServicio.equals("")){
			Oficina oficina = this.entidadService.findById(Oficina.class, oficinaId);
			if(!oficina.getTipoServicio().equals(tipoServicio)){
				mensaje = "No corresponde el \"Tipo de Servicio\" con la \"Oficina\" seleccionada";
			}
		}
		return mensaje;
	}
	
	

	private void generaEndpointOficina(Oficina oficina, boolean activo) {
		try {
			ParametroGlobalId paramGlobalId = new ParametroGlobalId();
			paramGlobalId.setAplicacionId(APLICACION_PARAM_GLOBAL);
			paramGlobalId.setParametroId(oficina.getClaveOficina()+POSTFIJO_OFICINA_WS);
			
			ParametroGlobal parametroGlobal = new ParametroGlobal();
			parametroGlobal.setActivo(activo);
			parametroGlobal.setDescripcion("ENDPOITN GENERADO PARA LA OFICINA: "+oficina.getClaveOficina());
			parametroGlobal.setId(paramGlobalId);
			parametroGlobal.setValor(oficina.getEndpoint().trim().toLowerCase());
			parametroGlobal.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
			parametroGlobal.setFechaCreacion(new Date());
			
			parametroGlobalService.guardarParametroGlobal(parametroGlobal);
		}catch(Exception e) {
			log.error("Error CatalogoSiniestroOficinaServiceImp generaEndpointOficina: "+e);
		}
	}
	
	@Override
	public Oficina obtenerWsEnpoint(Oficina oficina) {
		
		try {
			String endpointOficina = parametroGlobalService.obtenerValorPorIdParametro(APLICACION_PARAM_GLOBAL, oficina.getClaveOficina()+POSTFIJO_OFICINA_WS);
			
			if( !StringUtil.isEmpty( endpointOficina ) ) {
				oficina.setEndpoint(endpointOficina);
			}
		}catch(Exception e) {
			log.error("Error CatalogoSiniestroOficinaServiceImp obtenerWsEnpoint: "+e);
		}
		return oficina;
	}
	
	

}
