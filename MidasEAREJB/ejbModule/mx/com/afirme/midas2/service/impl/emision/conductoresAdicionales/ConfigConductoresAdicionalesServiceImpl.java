package mx.com.afirme.midas2.service.impl.emision.conductoresAdicionales;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.domain.emision.conductoresAdicionales.PolizaAutoSeycos;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.emision.conductoresAdicionales.ConfigConductoresAdicionalesService;

/**
 * Session Bean implementation class ConfigConductoresAdicionalesServiceImpl
 * @author Adriana Flores
 */
@Stateless
public class ConfigConductoresAdicionalesServiceImpl implements ConfigConductoresAdicionalesService {
	@EJB
	private EntidadService entidadService;
	@EJB
	private PolizaFacadeRemote polizaFacade;
    /**
     * Default constructor. 
     */
	public ConfigConductoresAdicionalesServiceImpl() {
		
	}
	
	/**
     * @see ConfigConductoresAdicionalesService#guardarConductoresAdicionales(PolizaAutoSeycos)
     */
	public int guardarConductoresAdicionales(PolizaAutoSeycos polizaAutoSeycos) {
		return polizaFacade.guardarConductoresAdicionales(polizaAutoSeycos);
	}

	/**
     * @see ConfigConductoresAdicionalesService#buscarPoliza(Map<String, Object>)
     */
	public PolizaAutoSeycos buscarPoliza(Map<String, Object> filtros) {
		List<Object[]> results = polizaFacade.buscarPolizaSeycos(filtros);
		PolizaAutoSeycos poliza = null;
		if(results != null){
			if ( results.size() > 0 ){ 
				poliza = (PolizaAutoSeycos) results.get(0)[0];
				poliza.setCodigoProducto((String) results.get(0)[1]);
				poliza.setCodigoTipoPoliza((String) results.get(0)[2]);
				poliza.setNumeroRenovacion(Utilerias.obtenerInteger(results.get(0)[3]));
				poliza.setNumeroPoliza(Utilerias.obtenerInteger(results.get(0)[4]));
				poliza.setFechaInicioVigencia(Utilerias.obtenerDate(results.get(0)[5]));
				poliza.setFechaFinVigencia(Utilerias.obtenerDate(results.get(0)[6]));
				poliza.setClaveEstatus((String)results.get(0)[7]);
				poliza.setNombreContratante((String) results.get(0)[8]);
				poliza.setDescripcionTipoPoliza((String) results.get(0)[9]);
				poliza.setPaquete((String) results.get(0)[10]);
			}
		}
		return poliza;
	}

	
	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public PolizaFacadeRemote getPolizaFacade() {
		return polizaFacade;
	}

	public void setPolizaFacade(PolizaFacadeRemote polizaFacade) {
		this.polizaFacade = polizaFacade;
	}
}
