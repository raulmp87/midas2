package mx.com.afirme.midas2.service.impl.negocio.emailsapamis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioLog;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.general.EmailService;
import mx.com.afirme.midas2.service.negocio.emailsapamis.EmailNegocioLogService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatAlertasSapAmisService;

@Stateless
public class EmailNegocioLogServiceImpl implements EmailNegocioLogService{
	private EntidadService entidadService;
	private CatAlertasSapAmisService catAlertasSapAmisService;
	private EmailService emailService;

	@Override
	public List<EmailNegocioLog> findAll() {
		List<EmailNegocioLog> retorno = entidadService.findAll(EmailNegocioLog.class);
		return retorno;
	}

	@Override
	public EmailNegocioLog findById(Long arg0) {
		EmailNegocioLog retorno = new EmailNegocioLog();
		if(arg0 != null && arg0 >= 0){
			retorno = entidadService.findById(EmailNegocioLog.class, arg0);
		}
		return retorno;
	}

	@Override
	public List<EmailNegocioLog> findByProperty(String arg0, Object arg1) {
		List<EmailNegocioLog> emailNegocioAlertasList = new ArrayList<EmailNegocioLog>();
		if(arg0 != null && !arg0.equals("") && arg1 != null){
			emailNegocioAlertasList = entidadService.findByProperty(EmailNegocioLog.class, arg0, arg1);
		}
		return emailNegocioAlertasList;
	}

	@Override
	public List<EmailNegocioLog> findByStatus(boolean arg0) {
		List<EmailNegocioLog> emailNegocioAlertas = entidadService.findByProperty(EmailNegocioLog.class, "estatus", arg0?0:1);
		return emailNegocioAlertas;
	}

	@Override
	public EmailNegocioLog saveObject(EmailNegocioLog arg0) {
		if(arg0 != null && arg0.getIdEmailNegocioLog() >= 0){
			arg0.setIdEmailNegocioLog((Long)entidadService.saveAndGetId(arg0));
		}
		return arg0;
	}
	
	private boolean validateAttributes(EmailNegocioLog arg0){
		boolean retorno = true;
		if(retorno){
			retorno = arg0.getAlerta() != null;
		}
		if(retorno){
			retorno = arg0.getEmail() != null;
		}
		if(retorno){
			retorno = arg0.getCuerpo() != null && !arg0.getCuerpo().equals("");
		}
		if(retorno){
			retorno = arg0.getFecha() != null;
		}
		return retorno;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setCatAlertasSapAmisService(CatAlertasSapAmisService catAlertasSapAmisService) {
		this.catAlertasSapAmisService = catAlertasSapAmisService;
	}

	@EJB
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}
}