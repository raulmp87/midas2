/**
 * 
 */
package mx.com.afirme.midas2.service.impl.siniestros.pagos.facturas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas2.dao.siniestros.pagos.facturas.RecepcionFacturaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.DatosGralOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.DevolucionesFacturasDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.ReporteFacturaDevolucionDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.pagos.PagosSiniestroService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.FacturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * @author admin
 *
 */
@Stateless
public class FacturaSiniestroServiceImpl implements FacturaSiniestroService {
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private OrdenCompraService ordenCompraService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	private ListadoService listadoService;
	
	@EJB
	private CatalogoSiniestroService catalogoSiniestroService;
	
	@EJB
	private PagosSiniestroService pagosSiniestroService;
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@EJB
	private EnvioNotificacionesService envioNotificacionesService;
	
	@EJB
	private RecepcionFacturaDao recepcionFacturaDao;

	
	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	
	@Override
	public List<DevolucionesFacturasDTO> buscarFacturas(FiltroFactura filtro) {
		return transformFacturaSiniestroToDevolucionesFacturasDTO ( catalogoSiniestroService.buscar(DocumentoFiscal.class, filtro) );
	}

	
	@Override
	public void cambiarEstatusFactura(String estatus, Long idFactura) {
		// TODO Auto-generated method stub

	}


	
	@Override
	public void registrarFactura(DocumentoFiscal factura,Long idOrdenCompra){
		OrdenCompra ordenCompra 		= null;
		String usuarioActual 			= String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() );
		//TODO Corregir relacion con ordenes de compra
		ordenCompra = ordenCompraService.obtenerOrdenCompra(idOrdenCompra);
		ordenCompra.setFactura( factura.getNumeroFactura());
		factura.setEstatus(ESTATUS_FACTURA_REGISTRADA);
		factura.setCodigoUsuarioCreacion( usuarioActual );
		factura.setCodigoUsuarioModificacion( usuarioActual);
		factura.setFechaCreacion( Calendar.getInstance().getTime());
		List<OrdenCompra> ordenesCompra= new ArrayList<OrdenCompra>();
		ordenesCompra.add(ordenCompra);
		factura.setOrdenesCompra(ordenesCompra);
		entidadService.save(factura);
		
		entidadService.save(ordenCompra);
	}
	
	
	@Override
	public void devolverFactura(DocumentoFiscal factura ){
		String usuarioActual 				= String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() );
		DocumentoFiscal facturaGuardar 	= null;
		boolean actualizarOrdenComra 		= Boolean.FALSE;
		OrdenCompra ordenCompra 			= null;
		
		facturaGuardar = obtenerFacturaRegistrada(factura);
		
		if(facturaGuardar != null){
			
			if(facturaGuardar.getEstatus()!= null && facturaGuardar.getEstatus().equals(ESTATUS_FACTURA_REGISTRADA)){
				actualizarOrdenComra = Boolean.TRUE;
			}
			//TODO Corregir relacion con ordenes de compra
			//facturaGuardar.setOrdenesCompra(factura.getOrdenesCompra() );
			facturaGuardar.setMotivoDevolucion(factura.getMotivoDevolucion());
			facturaGuardar.setNumeroFactura(factura.getNumeroFactura());
			facturaGuardar.setComentariosDevolucion( factura.getComentariosDevolucion());
			facturaGuardar.setUsuarioDevolucion( factura.getUsuarioDevolucion());
			facturaGuardar.setFechaDevolucion( factura.getFechaDevolucion());
			facturaGuardar.setEntregada( factura.getEntregada());
			facturaGuardar.setFechaEntrega( factura.getFechaEntrega());
			facturaGuardar.setCancelarOrdenCompra( factura.getCancelarOrdenCompra());
			facturaGuardar.setEstatus(ESTATUS_FACTURA_DEVUELTA);
			facturaGuardar.setCodigoUsuarioCreacion( usuarioActual );
			facturaGuardar.setCodigoUsuarioModificacion( usuarioActual);
			
			if(facturaGuardar.getFechaCreacion() == null){
				facturaGuardar.setFechaCreacion( Calendar.getInstance().getTime());
			}
			
			entidadService.save(facturaGuardar);
			
			if(actualizarOrdenComra){
				//TODO Corregir relacion con ordenes de compra
				/*ordenCompra = ordenCompraService.obtenerOrdenCompra(facturaGuardar.getOrdenesCompra().get(0).getId());
				ordenCompra.setFactura("");
				entidadService.save(ordenCompra);*/
				
				
				List<OrdenCompra> lstOrdenesC = facturaGuardar.getOrdenesCompra();
				for(OrdenCompra ordencompra : lstOrdenesC){
					ordencompra.setFactura("");
					entidadService.save(ordenCompra);
				}
			}
			
			this.envioCorreoDevolucionFactura(facturaGuardar);
			
			if(facturaGuardar.getCancelarOrdenCompraStr()){
				this.envioCorreosolicitarCancelacionOrdenCompra(facturaGuardar);
			}
			
		}
	}
	
	@Override
	public void editarDevolucion(DocumentoFiscal factura){
		DocumentoFiscal facturaEdicion = null;
		
		facturaEdicion  = this.getFacturaById(factura.getId());
		
		facturaEdicion.setEntregada(factura.getEntregada());
		facturaEdicion.setFechaEntrega( factura.getFechaEntrega());
		
		factura=facturaEdicion;
		
		entidadService.save(facturaEdicion);
	}

	private DocumentoFiscal obtenerFacturaRegistrada(DocumentoFiscal factura ){
		List<DocumentoFiscal> listFacturas	= null;
		DocumentoFiscal facturaRegistrada 	= new DocumentoFiscal();
		Map<String, Object> params 			= new HashMap<String, Object>();
		
		params.put("numeroFactura", factura.getNumeroFactura());
		//TODO Corregir relacion con ordenes de compra
		//params.put("ordenCompra.id", factura.getOrdenesCompra().get(0).getId());
		params.put("estatus", ESTATUS_FACTURA_REGISTRADA);
		listFacturas = entidadService.findByProperties(DocumentoFiscal.class, params);
		
		
		if(listFacturas!=null && listFacturas.size()>0){
			facturaRegistrada = listFacturas.get(0);
		}
		
		return facturaRegistrada;
	}
	
	@Override
	public void validarFacturaRegistrada(DocumentoFiscal facturaSiniestro)throws Exception {
		List<DocumentoFiscal> listaFactura 		= null;
		OrdenCompra ordenCompra 			= null;		
		Map<String, Object> params 			= new HashMap<String, Object>();
		if (null!=facturaSiniestro.getOrdenesCompra() &&!facturaSiniestro.getOrdenesCompra().isEmpty())
			ordenCompra = entidadService.findById(OrdenCompra.class, facturaSiniestro.getOrdenesCompra().get(0).getId());
		else {
			return ;
		}
		params.put("numeroFactura", facturaSiniestro.getNumeroFactura());		
		if (null!=ordenCompra   && !StringUtil.isEmpty(  ordenCompra.getNomBeneficiario()))
			params.put("ordenCompra.nomBeneficiario", ordenCompra.getNomBeneficiario());
		
		listaFactura = entidadService.findByProperties(DocumentoFiscal.class, params);
		
		if(listaFactura != null && listaFactura.size() >0 ){
			throw new Exception("Factura ya registrada para la orden " + listaFactura.get(0).getOrdenesCompra().get(0).getId());
		}
	}
	
	@Override
	public DatosGralOrdenCompraDTO getCabeceraOrdenCompra(Long idOrdenCompra){
		OrdenCompra ordenCompra 							= null;
		DatosGralOrdenCompraDTO datosGralOrdenCompraDTO 	= new DatosGralOrdenCompraDTO();
		PrestadorServicio prestador 						= null;
		
		ordenCompra = ordenCompraService.obtenerOrdenCompra(idOrdenCompra);
		
		if(ordenCompra.getIdBeneficiario() != null){
			prestador= entidadService.findById(PrestadorServicio.class, new Integer(ordenCompra.getIdBeneficiario().toString()) );
		}
		
		if(prestador != null && prestador.getPersonaMidas() != null){
			datosGralOrdenCompraDTO.setProveedor(prestador.getNombrePersona());
		}
		
		datosGralOrdenCompraDTO.setNumeroOrdenCompra( ordenCompra.getId());
		datosGralOrdenCompraDTO.setTipoPago( obtenerTipoPago(ordenCompra) );
		datosGralOrdenCompraDTO.setConceptoDePago(  ordenCompra.getDetalleConceptos() ); 
		datosGralOrdenCompraDTO.setTerminoDeAjuste( obtenerTerminoAjuste(ordenCompra) );
		if (null!=ordenCompra.getCoberturaReporteCabina())
			datosGralOrdenCompraDTO.setCoberturaAfectada(ordenCompra.getCoberturaReporteCabina().getCoberturaDTO().getNombreComercial());
		
		return datosGralOrdenCompraDTO;
	}
	
	
	
	@Override
	public void validarSiYaTieneFacturaRegistrada(Long idOrdenCompra) throws Exception{
		OrdenCompra ordenCompra 	= null;
		
		ordenCompra = ordenCompraService.obtenerOrdenCompra(idOrdenCompra);
		
		if(ordenCompra.getFactura()!= null && ordenCompra.getFactura().length()>0){
			throw new Exception("La orden de compra ya Tiene una factura registrada");
		}
	}
	
	
	
	@Override
	public List<DevolucionesFacturasDTO> obtenerDevolucionesOrdenCompra(Long idOrdenCompra){
		
		List<DocumentoFiscal> facturas = recepcionFacturaDao.obtenerFacturasRegistradaByOrdenCompra(idOrdenCompra, null);
		
		return transformFacturaSiniestroToDevolucionesFacturasDTO(facturas);
	}
	
	
	
	@Override
	public void realizarEdicionMasivaDevoluciones(String idsEdicionMasivaDevolucion,String entregadaA , Date fechaEntrega){	
		String[] ids 							= idsEdicionMasivaDevolucion.split(",");
		DocumentoFiscal facturaSiniestro 		= null;
		Long id 								= 0l;
		
		if(ids != null & ids.length > 0){
			for(String idFacturaSiniestro: ids){
				id = Long.parseLong( idFacturaSiniestro) ;
				facturaSiniestro = entidadService.findById(DocumentoFiscal.class, id);
				facturaSiniestro.setEntregada(entregadaA);
				facturaSiniestro.setFechaEntrega(fechaEntrega);
				entidadService.save(facturaSiniestro);
			}
		}
		
	}
	
	@Override
	public DocumentoFiscal getFacturaById(Long idFacturaSiniestro){
		return entidadService.findById(DocumentoFiscal.class, idFacturaSiniestro);
	}
	
	
	@Override
	public Boolean validacionMontoTotalOrdenCompraFactura( DocumentoFiscal factura , Long idOrdenCompra){
		ImportesOrdenCompraDTO importes		= null;
		Boolean resultado 					= Boolean.TRUE;
		if (null!= idOrdenCompra){
			importes = ordenCompraService.calcularImportes(idOrdenCompra,null, false);
			if(factura.getMontoTotal().compareTo(importes.getTotal()) != 0){
				resultado = Boolean.FALSE;
			}
		}
		
		
		return resultado;
	}
	
	
	@Override
	public TransporteImpresionDTO imprimirDevolucionFactura(Long idFacturaSiniestro){
				
		DocumentoFiscal facturaSiniestro 						= null;
		DatosGralOrdenCompraDTO datosGralOrdenCompraDTO 		= null;
		ReporteFacturaDevolucionDTO reporteFacturaDevolucionDTO = new ReporteFacturaDevolucionDTO();
		List<ReporteFacturaDevolucionDTO> dataSourceImpresion 	= new ArrayList<ReporteFacturaDevolucionDTO>();
		Map<String,String> mapMotivosDevolucion 				= null;
		GeneradorImpresion gImpresion 							= new GeneradorImpresion();
		JasperReport jReport 									= gImpresion.getOJasperReport(REPORTE_DEVOLUCION_FACTURA);
		JRBeanCollectionDataSource collectionDataSource 		= null;
		Map<String, Object> params 								= new HashMap<String, Object>();
		
		
		facturaSiniestro = this.getFacturaById(idFacturaSiniestro);
		mapMotivosDevolucion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_CANCELACION_ORDENPAGO);	
		facturaSiniestro.setMotivoDevolucionStr( mapMotivosDevolucion.get( facturaSiniestro.getMotivoDevolucion()));
		//TODO Corregir relacion con ordenes de compra
		datosGralOrdenCompraDTO = this.getCabeceraOrdenCompra(facturaSiniestro.getOrdenesCompra().get(0).getId());
		
		reporteFacturaDevolucionDTO.setDatosGralOrdenCompraDTO(datosGralOrdenCompraDTO);
		reporteFacturaDevolucionDTO.setFacturaSiniestro(facturaSiniestro);
		
		dataSourceImpresion.add(reporteFacturaDevolucionDTO);
		params.put("PRUTAIMAGEN", SistemaPersistencia.LOGO_SEGUROS_AFIRME);
		
		collectionDataSource = new JRBeanCollectionDataSource(dataSourceImpresion);
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
	}
	
	
	
	@Override
	public void envioCorreoDevolucionFactura(DocumentoFiscal factura) {
		
		HashMap<String, Serializable> dataArg 		= new HashMap<String, Serializable>();
		Map<String,String> mapMotivosDevolucion		= null;
		OrdenCompra ordenCompra 					= null;
		ReporteCabina reporte 						= null;
		
		//TODO Corregir relacion con ordenes de compra
		ordenCompra = entidadService.findById(OrdenCompra.class, factura.getOrdenesCompra().get(0).getId());
		reporte = ordenCompra.getReporteCabina();
		
		mapMotivosDevolucion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_CANCELACION_ORDENPAGO);	
		factura.setMotivoDevolucionStr( mapMotivosDevolucion.get( factura.getMotivoDevolucion()));
		
		if(StringUtil.isEmpty(factura.getComentariosDevolucion())){
			factura.setComentariosDevolucion("N/A");
		}
		
		dataArg.put("facturaSiniestro", factura);
		//dataArg.put("reporte", reporte);  //es necesario agregarlo
		 //es necesario agregarlo
		dataArg.put(EnvioNotificacionesService.ATR_OFICINAID, reporte.getOficina().getId());
		dataArg.put(EnvioNotificacionesService.ATR_REPORTE, reporte);
		
		envioNotificacionesService.enviarNotificacionSiniestros(CODIGOCORREO_DEVOLUCION, dataArg);		
	}
	
	@Override
	public void envioCorreosolicitarCancelacionOrdenCompra(DocumentoFiscal factura) {
		
		Long idOrdenCompra 									= factura.getOrdenesCompra().get(0).getId();
		DatosGralOrdenCompraDTO datosGralOrdenCompraDTO 	= null;
		OrdenCompra ordenCompra 							= null;
		String usuarioSistema 								= usuarioService.getUsuarioActual().getNombreCompleto();
		HashMap<String, Serializable> dataArg 				= new HashMap<String, Serializable>();
		ReporteCabina reporte 								= null;
		
		
		datosGralOrdenCompraDTO = getCabeceraOrdenCompra(idOrdenCompra);
		datosGralOrdenCompraDTO.setUsuario(usuarioSistema);
		datosGralOrdenCompraDTO.setComentarios(factura.getComentariosDevolucion());
		ordenCompra = this.entidadService.findById(OrdenCompra.class, idOrdenCompra);
		reporte = ordenCompra.getReporteCabina(); 
			
		dataArg.put("datosGralOrdenCompraDTO", datosGralOrdenCompraDTO);  
		 //es necesario agregarlo
		dataArg.put(EnvioNotificacionesService.ATR_OFICINAID, reporte.getOficina().getId());
		dataArg.put(EnvioNotificacionesService.ATR_REPORTE, reporte);
		envioNotificacionesService.enviarNotificacionSiniestros("CANCELACION_ORDENPAGO", dataArg);
	}
	
	@Override
	public void validateOrdenCompraConFacturaRegistradaAntesDevolver(Long idOrdenCompra,String factura) throws Exception{

		Map<String, Object> params					= new HashMap<String, Object>();
		List<DocumentoFiscal> listFacturas			= null;
		DocumentoFiscal facturaSiniestro 			= null;
		
		if(factura != null){
		
			params.put("ordenCompra.id", idOrdenCompra);
			params.put("estatus", ESTATUS_FACTURA_REGISTRADA);
			
			//listFacturas = entidadService.findByProperties(FacturaSiniestro.class, params);
			listFacturas=this.recepcionFacturaDao.obtenerFacturasRegistradaByOrdenCompra(idOrdenCompra,ESTATUS_FACTURA_REGISTRADA);
			if(listFacturas != null && listFacturas.size()>0){
				facturaSiniestro = listFacturas.get(0);
				if(!facturaSiniestro.getNumeroFactura().equals(factura)){
					throw new Exception("La Orden compra tiene la factura "+ facturaSiniestro.getNumeroFactura() + " registrada,favor de devolverla primero" );
				}
			}
		}
	}
		
	@Override
	public void validateNumeroFacturaYaDevuelta(DocumentoFiscal factura) throws Exception{
		Map<String, Object> params				= new HashMap<String, Object>();
		List<DocumentoFiscal> listFacturas		= null;
		OrdenCompra ordenCompra 				= null;
		PrestadorServicio prestador 			= null;
		String nombreProveedor 					= "";
		
		
		params.put("numeroFactura", factura.getNumeroFactura());
		params.put("estatus", ESTATUS_FACTURA_DEVUELTA);
		
		listFacturas = entidadService.findByProperties(DocumentoFiscal.class, params);
		
		if(listFacturas != null && listFacturas.size()>0){
			//TODO Corregir relacion con ordenes de compra
			ordenCompra = entidadService.findById(OrdenCompra.class, factura.getOrdenesCompra().get(0).getId());
			for(DocumentoFiscal facturaDevuelta : listFacturas){
				//TODO Corregir relacion con ordenes de compra
				if(ordenCompra.getIdBeneficiario().equals( facturaDevuelta.getOrdenesCompra().get(0).getIdBeneficiario())){
					
					prestador= entidadService.findById(PrestadorServicio.class, new Integer(ordenCompra.getIdBeneficiario().toString()) );
					if(prestador != null && prestador.getPersonaMidas() != null){
						nombreProveedor = prestador.getNombrePersona();
					}
					
					throw new Exception("La factura "+ factura.getNumeroFactura() + " del Proveedor "+ nombreProveedor +" ya fue devuelta, en la orden de compra :"+ facturaDevuelta.getOrdenesCompra().get(0).getId() );
				}
			}
		}
		
	}
	
	
	public Boolean validatePuedeRegistrarFactura( Long idOrdenCompra ){
		Map<String, Object> params					= new HashMap<String, Object>();
		List<DocumentoFiscal> listFacturas			= new ArrayList<DocumentoFiscal>();
		Boolean resultado 							= Boolean.TRUE;
		
		params.put("ordenCompra.id", idOrdenCompra);
		params.put("estatus", ESTATUS_FACTURA_REGISTRADA);
		
		listFacturas.addAll( entidadService.findByProperties(DocumentoFiscal.class, params) );
		
		params.clear();
		params.put("ordenCompra.id", idOrdenCompra);
		params.put("estatus", ESTATUS_FACTURA_PAGADA);
		
		listFacturas.addAll( entidadService.findByProperties(DocumentoFiscal.class, params) );
		
		if(listFacturas != null && listFacturas.size()>0){
			resultado = Boolean.FALSE;
		}
		
		return resultado;
	}
	
	
	
	
	
	private String obtenerTerminoAjuste( OrdenCompra ordenCompra  ){
		CatValorFijo catalogo 				= null;
		AutoIncisoReporteCabina autoInciso 	= null;
		String resultado 					= "";
		
		if (null!=ordenCompra.getReporteCabina().getSeccionReporteCabina()){
			autoInciso= entidadService.findByProperty(AutoIncisoReporteCabina.class, "incisoReporteCabina.seccionReporteCabina.reporteCabina.id", ordenCompra.getReporteCabina().getId()).get(0);
	        if (! StringUtil.isEmpty(autoInciso.getTerminoAjuste())){
	        	catalogo = catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO , autoInciso.getTerminoAjuste());
	             if(null!=catalogo){
	            	 resultado = catalogo.getDescripcion().toUpperCase();
	             }
	        }
		}
        return resultado;
	}
	
	
	private String obtenerTipoPago( OrdenCompra ordenCompra ){
		CatValorFijo catalogo 				= null;
		String resultado 					= "";
		
		catalogo = catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_PAGOORDEN_COMPRA , ordenCompra.getTipoPago());
		if(null!=catalogo){
       	 resultado = catalogo.getDescripcion().toUpperCase();
        }
		
		return resultado;
	}
	
	
	private List<DevolucionesFacturasDTO> transformFacturaSiniestroToDevolucionesFacturasDTO( List<DocumentoFiscal> listadoFacturas){
		List<DevolucionesFacturasDTO> historial 	= new ArrayList<DevolucionesFacturasDTO>();
		DevolucionesFacturasDTO devolucionDTO 		= null;
		Map<String,String> mapMotivosDevolucion 	= null; 
		PrestadorServicio prestador 				= null;
		
		mapMotivosDevolucion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_CANCELACION_ORDENPAGO);	
		
		if( listadoFacturas!=null && listadoFacturas.size() >0){
			for(DocumentoFiscal factura: listadoFacturas){
				
				if(factura.getEstatus().equals(ESTATUS_FACTURA_DEVUELTA)){
					devolucionDTO = new DevolucionesFacturasDTO();
					
					devolucionDTO.setIdFactura( factura.getId());
					devolucionDTO.setFechaDevolcion( factura.getFechaDevolucion());
					devolucionDTO.setNumeroFactura( factura.getNumeroFactura());
					devolucionDTO.setMotivo( mapMotivosDevolucion.get( factura.getMotivoDevolucion()) );
					devolucionDTO.setDevueltaPor( factura.getUsuarioDevolucion());
					devolucionDTO.setFechaEntrega( factura.getFechaEntrega());
					if(!StringUtil.isEmpty( factura.getEntregada())){
						devolucionDTO.setEntregadaA( factura.getEntregada().toUpperCase());
					}
					
					//TODO Corregir relacion con ordenes de compra
					if( null!=factura.getOrdenesCompra() ){
						//TODO Corregir relacion con ordenes de compra
						devolucionDTO.setOrdenCompra( factura.getOrdenesCompra().get(0).getId().toString());
						//TODO Corregir relacion con ordenes de compra
						if(factura.getOrdenesCompra().get(0).getIdBeneficiario() != null){
							//TODO Corregir relacion con ordenes de compra
							prestador= entidadService.findById(PrestadorServicio.class, new Integer(factura.getOrdenesCompra().get(0).getIdBeneficiario().toString()) );
						}
						
						if(prestador != null && prestador.getPersonaMidas() != null){
							devolucionDTO.setProveedor(prestador.getNombrePersona());
						}
					}
					
					historial.add(devolucionDTO);
				}
				
			}
		}
		
		return historial; 
	}



	

}
