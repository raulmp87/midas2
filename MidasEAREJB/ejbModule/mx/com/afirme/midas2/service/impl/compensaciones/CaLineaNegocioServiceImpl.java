/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaLineaNegocioDao;
import mx.com.afirme.midas2.domain.compensaciones.CaLineaNegocio;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.service.compensaciones.CaLineaNegocioService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;

import org.apache.log4j.Logger;

@Stateless

public class CaLineaNegocioServiceImpl  implements CaLineaNegocioService {
	public static final String CONTRAPRESTACION = "contraprestacion";
	public static final String NEGOCIO_ID = "negocioId";
	public static final String VALORPORCENTAJE = "valorPorcentaje";
	public static final String VALORID = "valorId";
	public static final String VALORDESCRIPCION = "valorDescripcion";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@EJB
	private CaLineaNegocioDao lineaNegociocaDao;
	
    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaLineaNegocioServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaLineaNegocio entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaLineaNegocio entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaLineaNegocio entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaLineaNegocio 	::		CaLineaNegocioServiceImpl	::	save	::	INICIO	::	");
	        try {
//            entityManager.persist(entity);
	        	lineaNegociocaDao.update(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaLineaNegocio 	::		CaLineaNegocioServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaLineaNegocio 	::		CaLineaNegocioServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaLineaNegocio entity.
	  @param entity CaLineaNegocio entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaLineaNegocio entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaLineaNegocio 	::		CaLineaNegocioServiceImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaLineaNegocio.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaLineaNegocio 	::		CaLineaNegocioServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.info("	::	[INF]	::	Error al eliminar CaLineaNegocio 	::		CaLineaNegocioServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaLineaNegocio entity and return it or a copy of it to the sender. 
	 A copy of the CaLineaNegocio entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaLineaNegocio entity to update
	 @return CaLineaNegocio the persisted CaLineaNegocio entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaLineaNegocio update(CaLineaNegocio entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaLineaNegocio 	::		CaLineaNegocioServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaLineaNegocio result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaLineaNegocio 	::		CaLineaNegocioServiceImpl	::	update	::	FIN	::	");
	            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaLineaNegocio 	::		CaLineaNegocioServiceImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaLineaNegocio findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaLineaNegocioServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaLineaNegocio instance = entityManager.find(CaLineaNegocio.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaLineaNegocioServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaLineaNegocioServiceImpl	::	findById	::	ERROR	::	",re);
	            return null;
        }
    }    
    

/**
	 * Find all CaLineaNegocio entities with a specific property value.  
	 
	  @param propertyName the name of the CaLineaNegocio property to query
	  @param value the property value to match
	  	  @return List<CaLineaNegocio> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<CaLineaNegocio> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaLineaNegocioServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaLineaNegocio model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaLineaNegocioServiceImpl	::	findByProperty	::	FIN	::	");
					return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaLineaNegocioServiceImpl	::	findByProperty	::	ERROR	::	",re);
				return null;
		}
	}

    public List<CaLineaNegocio> findByContraprestacion(Object contraprestacion
	) {
		return findByProperty(CONTRAPRESTACION, contraprestacion
		);
	}
	public List<CaLineaNegocio> findByNegocioId(Object negocioId
	) {
		return findByProperty(NEGOCIO_ID, negocioId
		);
	}
	
	public List<CaLineaNegocio> findByValorporcentaje(Object valorporcentaje
	) {
		return findByProperty(VALORPORCENTAJE, valorporcentaje
		);
	}
	
	public List<CaLineaNegocio> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	public List<CaLineaNegocio> findByValorid(Object valorid
	) {
		return findByProperty(VALORID, valorid
		);
	}
	public List<CaLineaNegocio> findByValordescripcion(Object valordescripcion
	) {
		return findByProperty(VALORDESCRIPCION, valordescripcion
		);
	}
	
	public List<CaLineaNegocio> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	
	/**
	 * Find all CaLineaNegocio entities.
	  	  @return List<CaLineaNegocio> all CaLineaNegocio entities
	 */
	@SuppressWarnings("unchecked")
	public List<CaLineaNegocio> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaLineaNegocioServiceImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaLineaNegocio model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaLineaNegocioServiceImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaLineaNegocioServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
	public CaLineaNegocio findByPropertyUniqueResult(String propertyName, Object value
	){
		CaLineaNegocio result;
	      LOGGER.info(" ::  [INF] ::  Buscando por Propiedad  ::    CaLineaNegocioServiceImpl  ::  findByPropertyUniqueResult  ::  INICIO  ::  ");
	      try {
	        final String queryString = "select model from CaLineaNegocio model where model." 
	          + propertyName + "= :propertyValue";
	      Query query = entityManager.createQuery(queryString);
	      query.setParameter("propertyValue", value);     
	      result = (CaLineaNegocio) query.getSingleResult();
	    } catch (RuntimeException re) {
	      LOGGER.error("  ::  [ERR] ::  Error al buscar por Propiedad   ::    CaLineaNegocioServiceImpl  ::  findByPropertyUniqueResult  ::  ERROR ::  ",re);
	      result = null;
	    }
	    LOGGER.info(" ::  [INF] ::  Se busco por Propiedad  ::    CaLineaNegocioServiceImpl  ::  findByPropertyUniqueResult  ::  FIN ::  ");
	    return result;    
		
	}
	public CaLineaNegocio findByNegocioIdValorId(Long idNegocio, String idValor	){
		LOGGER.info(" ::  [INF] ::  Buscando por idNegocio & idValor  ::    CaLineaNegocioServiceImpl ::  findByNegocioIdValorId  ::  INICIO  ::  ");
		CaLineaNegocio result = null;
		try {			
			final String queryString = "select model from CaLineaNegocio model where model." 
		          						+ NEGOCIO_ID + "= :idNegocio and model." + VALORID + " = :idValor";
			Query query = entityManager.createQuery(queryString, CaLineaNegocio.class);
			query.setParameter("idNegocio", idNegocio);
			query.setParameter("idValor", idValor);
			LOGGER.info(" ::  [INF] ::  Error al buscar por idNegocio & idValor  ::    CaLineaNegocioServiceImpl ::  findByNegocioIdValorId  ::  FIN ::  ");
			result = (CaLineaNegocio) query.getSingleResult();
			} catch (Exception e) {
				LOGGER.error("  ::  [ERR] ::  Se busco por idNegocio & idValor  ::    CaLineaNegocioServiceImpl ::  findByNegocioIdValorId  ::  ERROR ::  ",e);
				result = null;
			}
			return result;
	}
	@SuppressWarnings("unchecked")
	public List<CaLineaNegocio> findByNegocioIdandContraprestacion(Long negocioId, boolean contraprestacion){
		List<CaLineaNegocio> result = null;
		LOGGER.info(" ::  [INF] ::  Buscando por idNegocio & Contraprestacion  ::    CaLineaNegocioServiceImpl ::  findByNegocioIdandContraprestacion  ::  INICIO  ::  ");
		try{
			final String queryString = "select model from CaLineaNegocio model where model." 
					+ NEGOCIO_ID + "= :idNegocio and model." + CONTRAPRESTACION + " = :contraprestacion";
			Query query = entityManager.createQuery(queryString, CaLineaNegocio.class);
			query.setParameter("idNegocio", negocioId);
			query.setParameter("contraprestacion", contraprestacion);
			result = query.getResultList();
		}catch (Exception e) {
			LOGGER.error("  ::  [ERR] ::  Error al buscar por idNegocio & Contraprestacion    ::    CaLineaNegocioServiceImpl ::  findByNegocioIdandContraprestacion  ::  ERROR ::  ",e);
			result = null;
		}
		LOGGER.info(" ::  [INF] ::  Se busco por idNegocio & Contraprestacion    ::    CaLineaNegocioServiceImpl ::  findByNegocioIdandContraprestacion  ::  FIN ::  ");
		return result;
	}
	
	
	public void guardarLineasNegocio(CaParametros caParametros, String lineasNegocio){
		
		if(lineasNegocio != null && !lineasNegocio.trim().isEmpty()){
			
			StringTokenizer token = new StringTokenizer(lineasNegocio, "$");
			
			CaLineaNegocio lineaPersistir;
			
			while (token.hasMoreElements()) {
				String linea = token.nextToken();
				int indiceId = linea.indexOf(ConstantesCompensacionesAdicionales.PROPIEDADID);
				int indiceValor = linea.indexOf(ConstantesCompensacionesAdicionales.PROPIEDADVALOR);
				int indiceDescripcion = linea.indexOf(ConstantesCompensacionesAdicionales.PROPIEDADDESCRIPCION);
				String valorId = linea.substring(indiceId + ConstantesCompensacionesAdicionales.PROPIEDADID.length(),indiceValor);
				String valorPorcentaje = linea.substring(indiceValor + ConstantesCompensacionesAdicionales.PROPIEDADVALOR.length(), indiceDescripcion);
				String valorDescripcion = linea.substring(indiceDescripcion + ConstantesCompensacionesAdicionales.PROPIEDADDESCRIPCION.length(), linea.length());
				
				if(valorPorcentaje.equals("")){
					valorPorcentaje = "0";
				}
				lineaPersistir = new CaLineaNegocio();								
				lineaPersistir.setCaParametros(caParametros);
				lineaPersistir.setCaCompensacion(caParametros.getCaCompensacion());
				lineaPersistir.setContraprestacion(caParametros.getCaCompensacion().getContraprestacion());
				lineaPersistir.setNegocioId(caParametros.getCaCompensacion().getNegocioId());
				lineaPersistir.setValorPorcentaje(valorPorcentaje);
				lineaPersistir.setValorId(valorId);
				lineaPersistir.setValorDescripcion(valorDescripcion);
				lineaPersistir.setFechaCreacion(new Date());
				lineaPersistir.setFechaModificacion(new Date());
				lineaPersistir.setUsuario(caParametros.getCaCompensacion().getUsuario());
				lineaPersistir.setBorradoLogico(ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
				
				this.save(lineaPersistir);
			}
		}
	}
}