package mx.com.afirme.midas2.service.impl.suscripcion.solicitud.autorizacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.suscripcion.solicitud.autorizacion.SolicitudAutorizacionDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.BitemporalTexAdicionalCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.TexAdicionalCot;
import mx.com.afirme.midas2.domain.excepcion.ExcepcionSuscripcion;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionCotizacion;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionDetalle;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionNegocioDescripcionDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionNegocioAutosService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.comentarios.ComentariosService;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class SolicitudAutorizacionServiceImpl implements SolicitudAutorizacionService {

	private SolicitudAutorizacionDao solicitudDao;	
	private EntidadService entidadService;	
	private ExcepcionSuscripcionNegocioAutosService excepcionSuscripcionNegocioAutosService;
	private UsuarioService usuarioService;	
	private EntidadBitemporalService entidadBitemporalService;	
	protected EntidadContinuityDao entidadContinuityDao;
	private CotizacionService cotizacionService;
	private ComentariosService comentariosService;
	
	public static final String TITULO_CORREO_EXCEPCION = "Se ha detectado una excepci\u00F3n en la cotizaci\u00F3n"; 
	
	public static final String SALUDO_CORREO_EXCEPCION = "A quien corresponda.";
	
	private static final String MENSAJE_CANCELACION_COTIZACION = "COTIZACION CANCELADA POR RECHAZO DE AUTORIZACION PARA EMISION DE PERSONA MORAL";
	
	private static final Short TIPO_COMENTARIO_COTIZACION_CANCELACION = 2;
	
	private static final String SOLICITUD_DETALLE = "EMISION DE POLIZA CON PERSONA MORAL";
	
	@EJB
	public void setEntidadContinuityDao(EntidadContinuityDao entidadContinuityDao) {
		this.entidadContinuityDao = entidadContinuityDao;
	}

	@EJB
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}

	@EJB(beanName="UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@EJB
	public void setExcepcionSuscripcionNegocioAutosService(
			ExcepcionSuscripcionNegocioAutosService excepcionSuscripcionNegocioAutosService) {
		this.excepcionSuscripcionNegocioAutosService = excepcionSuscripcionNegocioAutosService;
	}
	
	@EJB
	public void setSolicitudDao(SolicitudAutorizacionDao solicitudDao) {
		this.solicitudDao = solicitudDao;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}
	
	@EJB
	public void setComentariosService(ComentariosService comentariosService) {
		this.comentariosService = comentariosService;
	}

	@Override
	public List<SolicitudExcepcionCotizacion> listarSolicitudes(
			SolicitudExcepcionCotizacion filtro) {	 
		 List<SolicitudExcepcionCotizacion> lista = solicitudDao.listarSolicitudes(filtro);
		 for(SolicitudExcepcionCotizacion item : lista){
			 try {
				 Usuario usuario = usuarioService.buscarUsuarioPorId(item.getUsuarioAutorizador().intValue());				
				 item.setNombreAutorizador(usuario != null ? usuario.getNombreCompleto() : null );
				 usuario = usuarioService.buscarUsuarioPorId(item.getUsuarioSolicitante().intValue());
				 item.setNombreSolicitante(usuario != null ? usuario.getNombreCompleto() : null );
			} catch (Exception e) {
			}
		 }
		return solicitudDao.listarSolicitudes(filtro);
	}
	
	@Override
	public List<SolicitudExcepcionCotizacion> obtenerSolicitudesHistoricas(
			BigDecimal idCotizacion) {
		SolicitudExcepcionCotizacion filtro = new SolicitudExcepcionCotizacion();
		filtro.setToIdCotizacion(idCotizacion);		
		return solicitudDao.listarSolicitudes(filtro, true);
	}
	
	@Override
	public List<SolicitudExcepcionDetalle> obtenerSolicitudesHistoricasDetalle(
			BigDecimal idCotizacion, BigDecimal idInciso) {
		SolicitudExcepcionCotizacion filtro = new SolicitudExcepcionCotizacion();
		filtro.setToIdCotizacion(idCotizacion);		
		return solicitudDao.listarDetalleSolicitudesPorInciso(filtro, true, idInciso);
	}


	@Override
	public void guardarSolicitud(SolicitudExcepcionCotizacion solicitud) {
		entidadService.save(solicitud);
		solicitud.setDetalle(new ArrayList<SolicitudExcepcionDetalle>());
	}

	@Override
	public SolicitudExcepcionCotizacion obtenerSolicitud(Long id) {	
		return entidadService.findById(SolicitudExcepcionCotizacion.class, id);
	}

	@Override
	public List<SolicitudExcepcionCotizacion> obtenerSolicitud(
			BigDecimal idCotizacion) {
		SolicitudExcepcionCotizacion filtro = new SolicitudExcepcionCotizacion();
		filtro.setToIdCotizacion(idCotizacion);		
		return listarSolicitudes(filtro);
	}

	@Override
	public List<SolicitudExcepcionDetalle> listarDetalleParaSolicitud(Long id) {
		return solicitudDao.listarDetalle(id);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void terminarSolicitud(Long id, Long usuarioAutorizador) {
		SolicitudExcepcionCotizacion solicitud = obtenerSolicitudConDetalle(id);		
		for(SolicitudExcepcionDetalle detalle : solicitud.getDetalle()){
			if(detalle.getEstatus() == SolicitudAutorizacionService.EstatusDetalleSolicitud.EN_PROCESO.valor()){
				throw new RuntimeException("Se requiere que todos los detalles tengan un estatus diferente a EN PROCESO");
			}
		}		
		solicitud.setEstatus(EstatusSolicitud.TERMINADA.valor());
		solicitud.setUsuarioAutorizador(usuarioAutorizador);
		entidadService.save(solicitud);						
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardarEstatusDetalleSolicitud(Long idSolicitud, List<SolicitudExcepcionDetalle> detalles){
		for(SolicitudExcepcionDetalle detalle : detalles){
			if(detalle.getEstatus() == SolicitudAutorizacionService.EstatusDetalleSolicitud.EN_PROCESO.valor()){
				continue;
			}
			
			SolicitudExcepcionDetalle detalleTmp = entidadService.findById(SolicitudExcepcionDetalle.class, detalle.getId());
			if (detalleTmp.getDescripcion().trim().equals(SOLICITUD_DETALLE)) {
				SolicitudExcepcionCotizacion solicitud = obtenerSolicitud(idSolicitud);
				if (detalle.getEstatus().equals(SolicitudAutorizacionService.EstatusDetalleSolicitud.AUTORIZADA.valor())
						|| detalle.getEstatus().equals(SolicitudAutorizacionService.EstatusDetalleSolicitud.CANCELADA.valor())) {
					autorizarDetalle(solicitud.getId(), detalle.getId(),detalle.getObservacion());
				} else if (detalle.getEstatus().equals(SolicitudAutorizacionService.EstatusDetalleSolicitud.RECHAZADA.valor())) {
					rechazarDetalle(solicitud.getId(), detalle.getId(),detalle.getObservacion());
					cancelarCotizacion(solicitud.getToIdCotizacion());
				}
			} else {
				if (detalle.getEstatus() == SolicitudAutorizacionService.EstatusDetalleSolicitud.AUTORIZADA.valor()) {
					autorizarDetalle(idSolicitud, detalle.getId(),detalle.getObservacion());
				} else if (detalle.getEstatus() == SolicitudAutorizacionService.EstatusDetalleSolicitud.CANCELADA.valor()) {
					cancelarDetalle(idSolicitud, detalle.getId(),detalle.getObservacion());
				} else if (detalle.getEstatus() == SolicitudAutorizacionService.EstatusDetalleSolicitud.RECHAZADA.valor()) {
					rechazarDetalle(idSolicitud, detalle.getId(),detalle.getObservacion());
				}
			}
			
			//Actualizar Status en tabla Endoso Inclusion Textos
			
			detalle = entidadService.findById(SolicitudExcepcionDetalle.class, detalle.getId());
			
			if(detalle.getIdExcepcion().longValue() == new Short(ExcepcionSuscripcionService.TipoExcepcionFija.ENDOSO_INCLUSION_TEXTO.valor()).longValue() 
					&& detalle.getEstatus()!= null)
			{	
				actualizarEstatusAutorizacionTextoAdicional(detalle);				
			}
			if(detalle.getIdExcepcion().longValue() == new Short(ExcepcionSuscripcionService.TipoExcepcionFija.INCLUSION_TEXTO.valor()).longValue() 
					&& detalle.getEstatus()!= null)
			{	
				actualizarEstatusAutorizacionTextoAdicionalCot(detalle);				
			}
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void terminarSolicitudMasiva(List<SolicitudExcepcionCotizacion> solicitudes, Long usuarioAutorizador, Short tipo) {
		
		for(SolicitudExcepcionCotizacion solicitudId : solicitudes){
			Long id = solicitudId.getId();
			SolicitudExcepcionCotizacion solicitud = obtenerSolicitudConDetalle(id);		
			for(SolicitudExcepcionDetalle detalle : solicitud.getDetalle()){
				if(detalle.getEstatus().equals(SolicitudAutorizacionService.EstatusDetalleSolicitud.EN_PROCESO.valor())){
					
					if (detalle.getDescripcion().trim().equals(SOLICITUD_DETALLE)){
						if(tipo.equals(SolicitudAutorizacionService.EstatusDetalleSolicitud.AUTORIZADA.valor()) ||
								tipo.equals(SolicitudAutorizacionService.EstatusDetalleSolicitud.CANCELADA.valor())){
							autorizarDetalle(solicitud.getId(), detalle.getId(), detalle.getObservacion());
						}else if(tipo.equals(SolicitudAutorizacionService.EstatusDetalleSolicitud.RECHAZADA.valor())){
							rechazarDetalle(solicitud.getId(), detalle.getId(), detalle.getObservacion());
							cancelarCotizacion(solicitud.getToIdCotizacion());
						}
					}else{
						if(tipo.equals(SolicitudAutorizacionService.EstatusDetalleSolicitud.AUTORIZADA.valor())){
							autorizarDetalle(solicitud.getId(), detalle.getId(), detalle.getObservacion());
						}else if(tipo.equals(SolicitudAutorizacionService.EstatusDetalleSolicitud.CANCELADA.valor())){
							cancelarDetalle(solicitud.getId(), detalle.getId(), detalle.getObservacion());
						}else if(tipo.equals(SolicitudAutorizacionService.EstatusDetalleSolicitud.RECHAZADA.valor())){
							rechazarDetalle(solicitud.getId(), detalle.getId(), detalle.getObservacion());
						}
					}
					//Actualizar Status en tabla Endoso Inclusion Textos
					detalle = entidadService.findById(SolicitudExcepcionDetalle.class, detalle.getId());
					if(detalle.getIdExcepcion().longValue() == new Short(ExcepcionSuscripcionService.TipoExcepcionFija.ENDOSO_INCLUSION_TEXTO.valor()).longValue() 
						&& detalle.getEstatus()!= null){	
						actualizarEstatusAutorizacionTextoAdicional(detalle);				
					}
					if(detalle.getIdExcepcion().longValue() == new Short(ExcepcionSuscripcionService.TipoExcepcionFija.INCLUSION_TEXTO.valor()).longValue() 
						&& detalle.getEstatus()!= null){	
						actualizarEstatusAutorizacionTextoAdicionalCot(detalle);				
					}
				}
			}		
			solicitud.setEstatus(EstatusSolicitud.TERMINADA.valor());
			solicitud.setUsuarioAutorizador(usuarioAutorizador);
			entidadService.save(solicitud);	
		}
	}
	
	private void cancelarCotizacion(BigDecimal idToCotizacion){
		Comentario comentario=new Comentario();
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		
		if (cotizacion != null && cotizacion.getSolicitudDTO() != null){
			comentario.setSolicitudDTO(cotizacion.getSolicitudDTO());
			comentario.setCodigoUsuarioCreacion("MIDAS");
			comentario.setValor(comentariosService.validaComentarioSolicitudCotizacion(MENSAJE_CANCELACION_COTIZACION, TIPO_COMENTARIO_COTIZACION_CANCELACION));
			comentariosService.guardarComentario(comentario);
		}
		
		cotizacionService.cancelarCotizacion(idToCotizacion);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void terminarSolicitud(Long idSolicitud, List<SolicitudExcepcionDetalle> detalles, Long usuarioAutorizador){
		guardarEstatusDetalleSolicitud(idSolicitud, detalles);
		terminarSolicitud(idSolicitud, usuarioAutorizador);
	}
	
	
	@Override
	public void cancelarSolicitud(Long id, Long usuarioAutorizador) {
		SolicitudExcepcionCotizacion solicitud = obtenerSolicitudConDetalle(id);		
		for(SolicitudExcepcionDetalle detalle : solicitud.getDetalle()){
			if (detalle.getDescripcion().trim().equals(SOLICITUD_DETALLE)) {
				autorizarDetalle(solicitud.getId(), detalle.getId(),detalle.getObservacion());
			}else{
				cancelarDetalle(id, detalle.getId(), "SOL CANCELADA");
			}
		}						
		solicitud.setEstatus(EstatusSolicitud.CANCELADA.valor());
		solicitud.setUsuarioAutorizador(usuarioAutorizador);
		entidadService.save(solicitud);					
	}

	@Override
	public void autorizarDetalle(Long idSolicitud, Long idDetalle, String observacion) {
		SolicitudExcepcionDetalle detalle = entidadService.findById(SolicitudExcepcionDetalle.class, idDetalle);
		if(detalle != null){
			detalle.setEstatus(EstatusDetalleSolicitud.AUTORIZADA.valor());
			detalle.setObservacion(observacion!=null?observacion.trim().toUpperCase():null);
			entidadService.save(detalle);
		}	
	}

	@Override
	public void rechazarDetalle(Long idSolicitud, Long idDetalle, String observacion) {
		SolicitudExcepcionDetalle detalle = entidadService.findById(SolicitudExcepcionDetalle.class, idDetalle);
		if(detalle != null){
			detalle.setEstatus(EstatusDetalleSolicitud.RECHAZADA.valor());
			detalle.setObservacion(observacion!=null?observacion.trim().toUpperCase():null);
			entidadService.save(detalle);
		}			
	}

	@Override
	public void cancelarDetalle(Long idSolicitud, Long idDetalle, String observacion) {
		SolicitudExcepcionDetalle detalle = entidadService.findById(SolicitudExcepcionDetalle.class, idDetalle);
		if(detalle != null){
			detalle.setEstatus(EstatusDetalleSolicitud.CANCELADA.valor());
			detalle.setObservacion(observacion!=null?observacion.trim().toUpperCase():null);
			entidadService.save(detalle);
		}		
	}

	@Override
	public SolicitudExcepcionCotizacion obtenerSolicitudConDetalle(Long id) {
		SolicitudExcepcionCotizacion solicitud = obtenerSolicitud(id);
		solicitud.setDetalle(listarDetalleParaSolicitud(id));		
		return solicitud;
	}
	
	@Override
	public SolicitudExcepcionCotizacion obtenerSolicitudConDetalle(BigDecimal idCotizacion) {
		SolicitudExcepcionCotizacion solicitud = null;
		List<SolicitudExcepcionCotizacion> lista = obtenerSolicitud(idCotizacion);
		if(!lista.isEmpty()){
			solicitud = lista.get(0);
			solicitud.setDetalle(listarDetalleParaSolicitud(solicitud.getId()));		
		}
		return solicitud;
	}

	@Override
	public void agregarSolicitud(BigDecimal idCotizacion, String descripcionSolicitud, 
			Long usuarioSolicitante, Long usuarioAutorizador, String cveNegocio, 
			String descripcionDetalle, String observacionDetalle, BigDecimal idSeccion, 
			BigDecimal idInciso, BigDecimal idCobertura, Long idExcepcion,
			String provocadorExcepcion, String claveProvocadorExcepcion){
		SolicitudExcepcionCotizacion solicitud = null;
		List<SolicitudExcepcionCotizacion> solicitudes = obtenerSolicitud(idCotizacion);
		if(!solicitudes.isEmpty()){
			solicitud = solicitudes.get(0);
			solicitud.setCveNegocio(cveNegocio);
			solicitud.setEstatus(EstatusSolicitud.EN_PROCESO.valor());
			solicitud.setToIdCotizacion(idCotizacion);
			solicitud.setDescripcion(descripcionSolicitud);
			solicitud.setUsuarioAutorizador(usuarioAutorizador);
			solicitud.setUsuarioSolicitante(usuarioSolicitante);
			solicitud.setFechaSolicitud(new Date());
		}else{
			solicitud = generaContenedorSolicitud(null, idCotizacion, 
					descripcionSolicitud, usuarioSolicitante, usuarioAutorizador, cveNegocio);
		}			
		SolicitudExcepcionDetalle detalle = generaContenedorDetalle(null, descripcionDetalle, 
				observacionDetalle, idSeccion, idInciso, idCobertura, idExcepcion,
				provocadorExcepcion, claveProvocadorExcepcion);		
		solicitud.addDetalle(detalle);
		entidadService.save(solicitud);
	}

	@Override
	public void agregarExcepcionASolicitud(Long idSolicitud, String descripcion, 
			String observacion, BigDecimal idSeccion, BigDecimal idInciso, 
			BigDecimal idCobertura, Long idExcepcion, String provocadorExcepcion, 
			String claveProvocadorExcepcion){
		SolicitudExcepcionCotizacion solicitud = entidadService.findById(
				SolicitudExcepcionCotizacion.class, idSolicitud);
		if(solicitud!=null){
			SolicitudExcepcionDetalle detalle = generaContenedorDetalle(null, descripcion, 
					observacion, idSeccion, idInciso, idCobertura, idExcepcion, provocadorExcepcion, claveProvocadorExcepcion);
			solicitud.addDetalle(detalle);
			entidadService.save(solicitud);
		}
	}

	private SolicitudExcepcionDetalle generaContenedorDetalle(Long id, String descripcionDetalle, 
			String observacionDetalle, BigDecimal idSeccion, BigDecimal idInciso, BigDecimal idCobertura,
			Long idExcepcion, String provocadorExcepcion, String claveProvocadorExcepcion){
		SolicitudExcepcionDetalle detalle = new SolicitudExcepcionDetalle();
		detalle.setId(id);
		detalle.setDescripcion(descripcionDetalle);
		detalle.setObservacion(observacionDetalle);
		detalle.setEstatus(EstatusDetalleSolicitud.EN_PROCESO.valor());
		detalle.setIdCobertura(idCobertura);
		detalle.setFechaDetalle(new Date());
		detalle.setIdExcepcion(idExcepcion);
		detalle.setIdSeccion(idSeccion);
		detalle.setIdInciso(idInciso);		
		detalle.setClaveProvocadorExcepcion(null);
		detalle.setProvocadorExcepcion(provocadorExcepcion);
		detalle.setClaveProvocadorExcepcion(claveProvocadorExcepcion);
		return detalle;
	}
	
	private SolicitudExcepcionCotizacion generaContenedorSolicitud(Long id, BigDecimal idCotizacion, String descripcionSolicitud, 
			Long usuarioSolicitante, Long usuarioAutorizador, String cveNegocio){
		SolicitudExcepcionCotizacion solicitud = new SolicitudExcepcionCotizacion();
		solicitud.setId(id);
		solicitud.setCveNegocio(cveNegocio);
		solicitud.setEstatus(EstatusSolicitud.EN_PROCESO.valor());
		solicitud.setToIdCotizacion(idCotizacion);
		solicitud.setDescripcion(descripcionSolicitud);
		solicitud.setUsuarioAutorizador(usuarioAutorizador);
		solicitud.setUsuarioSolicitante(usuarioSolicitante);
		solicitud.setFechaSolicitud(new Date());
		return solicitud;
	}
	
	
	@Override
	public Long listarSolicitudesCount(SolicitudExcepcionCotizacion filtro, Date fechaHasta) {
		return  solicitudDao.listarSolicitudesCount(filtro, true);
	}
	
	@Override
	public List<SolicitudExcepcionCotizacion> listarSolicitudes(
			SolicitudExcepcionCotizacion filtro, Date fechaHasta) {
		 List<SolicitudExcepcionCotizacion> lista = solicitudDao.listarSolicitudes(filtro);
		 for(SolicitudExcepcionCotizacion item : lista){
			 try {
				 Usuario usuario = usuarioService.buscarUsuarioPorId(item.getUsuarioAutorizador().intValue());				
				 item.setNombreAutorizador(usuario != null ? usuario.getNombreCompleto() : null );
				 usuario = usuarioService.buscarUsuarioPorId(item.getUsuarioSolicitante().intValue());
				 item.setNombreSolicitante(usuario != null ? usuario.getNombreCompleto() : null );
			} catch (Exception e) {
			}
		 }
		return lista;
	}

	@Override
	public Long guardarSolicitudDeAutorizacion(BigDecimal idCotizacion,
			String descripcionSolicitud, Long usuarioSolicitante,
			Long usuarioAutorizador, String cveNegocio,
			List<ExcepcionSuscripcionReporteDTO> excepciones) {
		boolean tieneDetalle = false;
		Long idDetalle = null;
		SolicitudExcepcionDetalle detalle = null;		
		Long id = guardarSolicitud(idCotizacion, descripcionSolicitud, usuarioSolicitante, usuarioAutorizador, cveNegocio);
		SolicitudExcepcionCotizacion solicitud = entidadService.findById(SolicitudExcepcionCotizacion.class, id);
		for(ExcepcionSuscripcionReporteDTO reporte : excepciones){
			ExcepcionSuscripcion excepcion = entidadService.findById(ExcepcionSuscripcion.class, reporte.getIdExcepcion());			
			if(excepcion.getRequiereAutorizacion()){
				tieneDetalle = true;
				detalle = generaContenedorDetalle(idDetalle, reporte.getDescripcionExcepcion(), reporte.getObservacion(), 
						reporte.getIdLineaNegocio(), reporte.getIdInciso(), reporte.getIdCobertura(), reporte.getIdExcepcion(), 
						reporte.getDescripcionRegistroConExcepcion(), null);
				solicitud.addDetalle(detalle);
			}
			
			if(excepcion.getRequiereNotificacion()){
				//notificar
				String recipientes = excepcion.getCorreoNotificacion();
				String titulo = TITULO_CORREO_EXCEPCION + " " + idCotizacion;
				String saludo = SALUDO_CORREO_EXCEPCION;
				String cuerpo = excepcionSuscripcionNegocioAutosService.generarCuerpoMensajeNotificacion(reporte);
				excepcionSuscripcionNegocioAutosService.enviarNotificacion(recipientes, titulo, titulo, saludo, cuerpo);					
			}
			
		}
		if(tieneDetalle){
			entidadService.save(solicitud);
		}else{
			entidadService.remove(solicitud);
			id = null;
		}
		return id;
	}

	/**
	 * Crear nueva solicitud o actualizar en caso de que ya exista.
	 * @param idCotizacion
	 * @param descripcionSolicitud
	 * @param usuarioSolicitante
	 * @param usuarioAutorizador
	 * @param cveNegocio
	 * @return
	 */
	private Long salvarSolicitud(BigDecimal idCotizacion, String descripcionSolicitud, Long usuarioSolicitante,
			Long usuarioAutorizador, String cveNegocio){
		Long id = null;
		SolicitudExcepcionCotizacion solicitud = null;
		List<SolicitudExcepcionCotizacion> solicitudes = obtenerSolicitud(idCotizacion);
		if(!solicitudes.isEmpty()){
			solicitud = solicitudes.get(0);
			solicitud.setCveNegocio(cveNegocio);
			solicitud.setEstatus(EstatusSolicitud.EN_PROCESO.valor());
			solicitud.setToIdCotizacion(idCotizacion);
			solicitud.setDescripcion(descripcionSolicitud);
			solicitud.setUsuarioAutorizador(usuarioAutorizador);
			solicitud.setUsuarioSolicitante(usuarioSolicitante);
			solicitud.setFechaSolicitud(new Date());
			entidadService.save(solicitud);
			id = solicitud.getId();			
		}else{
			solicitud = generaContenedorSolicitud(null, idCotizacion, 
					descripcionSolicitud, usuarioSolicitante, usuarioAutorizador, cveNegocio);
			id = (Long)entidadService.saveAndGetId(solicitud);
		}	
		return id;
	}
	
	/**
	 * Crea una nueva solicitud
	 * @param idCotizacion
	 * @param descripcionSolicitud
	 * @param usuarioSolicitante
	 * @param usuarioAutorizador
	 * @param cveNegocio
	 * @return
	 */
	private Long guardarSolicitud(BigDecimal idCotizacion, String descripcionSolicitud, Long usuarioSolicitante,
			Long usuarioAutorizador, String cveNegocio){		
		SolicitudExcepcionCotizacion solicitud = generaContenedorSolicitud(null, idCotizacion, 
					descripcionSolicitud, usuarioSolicitante, usuarioAutorizador, cveNegocio);
		return (Long)entidadService.saveAndGetId(solicitud);		
	}
	

	@Override
	public List<SolicitudExcepcionCotizacion> listarSolicitudesInvalidas(
			BigDecimal idToCotizacion) {
		List<SolicitudExcepcionCotizacion> lista = solicitudDao.listarSolicitudesEstatusInvalido(idToCotizacion);		
		return lista;
	}

	@Override
	public Short obtenerEstatusParaExcepcion(BigDecimal idCotizacion, BigDecimal idSeccion, 
			BigDecimal idInciso, BigDecimal idCobertura, Long idExcepcion) {
		Short estatus = null;		
		List<SolicitudExcepcionDetalle> solicitudes = obtenerSolicitudesHistoricasDetalle(idCotizacion, idInciso);
		for(SolicitudExcepcionDetalle detalle : solicitudes){
			if(estatus == null && detalle.getIdExcepcion().longValue() == idExcepcion && revisarSiValorExiste(idSeccion, detalle.getIdSeccion()) && 
					revisarSiValorExiste(idInciso, detalle.getIdInciso()) && revisarSiValorExiste(idCobertura, detalle.getIdCobertura())){
				estatus = detalle.getEstatus();					
			}
		}
		return estatus;
	}
	
	private boolean revisarSiValorExiste(BigDecimal uno, BigDecimal dos){
		boolean equal = false;
		if(uno == null && dos == null){
			equal = true;
		}else if(uno != null && dos != null && uno.equals(dos)){
			equal = true;
		}
		return equal;
	}
	
	
	@Override
	public ExcepcionSuscripcionNegocioDescripcionDTO verDetalleExcepcion(Long idExcepcion){
		ExcepcionSuscripcionNegocioDescripcionDTO excepcion = excepcionSuscripcionNegocioAutosService.obtenerExcepcion(idExcepcion);
		return excepcion;
	}
	
	
	@Override
	public SolicitudExcepcionDetalle obtenerDetalleParaExcepcion(BigDecimal idCotizacion,
			final Long idExcepcion, final BigDecimal idSeccion, final BigDecimal idInciso, final BigDecimal idCobertura) {
		List<SolicitudExcepcionDetalle> lista = obtenerSolicitudConDetalle(idCotizacion).getDetalle();
		Predicate evaluacion = new Predicate() {			
			@Override
			public boolean evaluate(Object arg0) {				
				SolicitudExcepcionDetalle detalle = (SolicitudExcepcionDetalle)arg0;
				if (detalle.getIdExcepcion().longValue() == idExcepcion.longValue() && 
						(idSeccion == null || detalle.getIdSeccion().longValue() == idSeccion.longValue()) && 
							(idInciso == null || detalle.getIdInciso().longValue() == idInciso.longValue())) {
					if ((detalle.getIdCobertura() != null && idCobertura != null && detalle.getIdCobertura().longValue() == idCobertura.longValue()) || 
							(detalle.getIdCobertura() == null && idCobertura == null)) {
						return true;
					} else {
						return false;
					}
				}
				return false;
			}
		};
		return (SolicitudExcepcionDetalle)CollectionUtils.find(lista, evaluacion);
	}

	@Override
	public List<Usuario> getListUsuarios(String name) {
		List<Usuario> solicitantesList = new ArrayList<Usuario>();
		try{
			solicitantesList = usuarioService.buscarUsuarioPorNombreCompleto(name);
		}catch(Exception e){
			e.printStackTrace();
		}
		return solicitantesList;
	}
	
	private void actualizarEstatusAutorizacionTextoAdicional(SolicitudExcepcionDetalle detalle)	{		
		Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("cotizacionId", detalle.getSolicitudExcepcionCotizacion().getToIdCotizacion().longValue());
		properties.put("claveEstatusCot",CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		List<ControlEndosoCot> controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class,properties);
		ControlEndosoCot controlEndosoCot = controlEndosoCotList.iterator().next();
	
		BitemporalCotizacion cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME,
				detalle.getSolicitudExcepcionCotizacion().getToIdCotizacion(), TimeUtils.getDateTime(controlEndosoCot.getValidFrom()));
		
		Collection<BitemporalTexAdicionalCot> lstBitemporalTexAdicionalCot = cotizacion.getContinuity().getTexAdicionalCotContinuities().getInProcess(TimeUtils.getDateTime(controlEndosoCot.getValidFrom()));	
	 	Usuario usuario = usuarioService.getUsuarioActual();
		
		for (BitemporalTexAdicionalCot bitemporalTexAdicionalCot : lstBitemporalTexAdicionalCot) {
			bitemporalTexAdicionalCot.getValue().setClaveAutorizacion(detalle.getEstatus());
			bitemporalTexAdicionalCot.getValue().setCodigoUsuarioAutorizacion(usuario.getNombreUsuario());
			
			entidadContinuityDao.update(bitemporalTexAdicionalCot.getEntidadContinuity());
		}				
	}
	
	private void actualizarEstatusAutorizacionTextoAdicionalCot(SolicitudExcepcionDetalle detalle)	{		
		
		List<TexAdicionalCotDTO> texAdicionalList = entidadService.findByProperty(TexAdicionalCotDTO.class, "cotizacion.idToCotizacion",  detalle.getSolicitudExcepcionCotizacion().getToIdCotizacion());
		Usuario usuario = usuarioService.getUsuarioActual();
		
		if(texAdicionalList != null && !texAdicionalList.isEmpty()){
			for(TexAdicionalCotDTO item: texAdicionalList){
				if(item.getClaveAutorizacion().equals(TexAdicionalCot.CLAVEAUTORIZACION_EN_PROCESO)){
					item.setClaveAutorizacion(detalle.getEstatus());
					item.setCodigoUsuarioAutorizacion(usuario.getNombreUsuario());
					item.setNombreUsuarioModificacion(usuario.getNombreCompleto());
					item.setFechaModificacion(new Date());
					entidadService.save(item);
				}
			}
		}
	}
}
