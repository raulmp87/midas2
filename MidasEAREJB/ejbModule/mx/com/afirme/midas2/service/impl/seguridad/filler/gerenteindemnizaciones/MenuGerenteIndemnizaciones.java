package mx.com.afirme.midas2.service.impl.seguridad.filler.gerenteindemnizaciones;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

public class MenuGerenteIndemnizaciones {
	
	private List<Menu> listaMenu = null;

	public MenuGerenteIndemnizaciones() {
		super();
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"m1","Emision", "Menu ppal Emision", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m2","Siniestros", "Menu ppal Siniestos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m3","Reaseguro", "Menu ppal Reaseguro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("4"),"m1_2","Vida", "Submenu Vida", null, false);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m1_3","Da�os", "Submenu Da�os", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("6"),"m1_3_1","Solicitudes", "Submenu Solicitudes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("7"),"m1_3_2","Ordenes de Trabajo", "Submenu Ordenes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("8"),"m2_3","Da�os", "Da�os", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("9"),"m2_3_4","Cat�logos", "Siniestros � Catalogo", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("10"),"m2_3_4_1","Ajustador", "Siniestros � Catalogo � Ajustador", "/MidasWeb/catalogos/ajustador/listar.do|contenido|null", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("11"),"menu","", "Raiz del menu", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("12"),"m3_2","descripcion", "descripcion2", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("13"),"m3_2_1","descripcion", "descripcion2", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("14"),"m3_2_1_2","descripcion", "descripcion2", "/MidasWeb/contratos/linea/listarLineaNegociacion.do|contenido|cargarComponentesLineaNegociacion()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("15"),"m3_2_1_1","descripcion", "descripcion2", "/MidasWeb/contratos/linea/listarLineaContrato.do|contenido|cargarComponentesLineaContrato()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("16"),"m3_2_1_3","descripcion", "descripcion2", "/MidasWeb/contratos/linea/listarLineaVigencia.do|contenido|cargarComponentesLineaVigencia()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("17"),"m3_2_1_4","descripcion", "descripcion2", "/MidasWeb/contratos/linea/listarLineaVigencia.do|contenido|cargarComponentesLineaVigencia()", true);
		listaMenu.add(menu);
		return this.listaMenu;
		
	}		
	

}
