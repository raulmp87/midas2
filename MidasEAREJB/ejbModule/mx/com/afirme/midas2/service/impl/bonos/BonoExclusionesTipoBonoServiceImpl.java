package mx.com.afirme.midas2.service.impl.bonos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.bonos.BonoExclusionesTipoBonoDao;
import mx.com.afirme.midas2.domain.bonos.BonoExclusionesTipoBono;
import mx.com.afirme.midas2.service.bonos.BonoExclusionesTipoBonoService;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author sams
 *
 */
@Stateless
public class BonoExclusionesTipoBonoServiceImpl implements BonoExclusionesTipoBonoService{

	private BonoExclusionesTipoBonoDao dao;
	
	@Override
	public List<BonoExclusionesTipoBono> loadByConfiguration(Long idBono)
			throws MidasException {
		
		return dao.loadByConfiguration(idBono);
	}

	@EJB
	public void setDao(BonoExclusionesTipoBonoDao dao) {
		this.dao = dao;
	}
	

}
