/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nreynoso 
 * 
 */
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaRamoDao;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;
import mx.com.afirme.midas2.service.compensaciones.CaRamoService;

import org.apache.log4j.Logger;

@Stateless

public class CaRamoServiceImpl  implements CaRamoService {
	public static final String NOMBRE = "nombre";
	public static final String IDENTIFICADOR = "identificador";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@EJB
	private CaRamoDao ramocaDao;
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaRamoServiceImpl.class);
    
		/**
	 Perform an initial save of a previously unsaved CaRamo entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaRamo entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaRamo entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaRamo 	::		CaRamoServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	ramocaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaRamo 	::		CaRamoServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaRamo 	::		CaRamoServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaRamo entity.
	  @param entity CaRamo entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaRamo entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaRamo 	::		CaRamoServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaRamo.class, entity.getId());
	        	ramocaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaRamo 	::		CaRamoServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaRamo 	::		CaRamoServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaRamo entity and return it or a copy of it to the sender. 
	 A copy of the CaRamo entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaRamo entity to update
	 @return CaRamo the persisted CaRamo entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaRamo update(CaRamo entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaRamo 	::		CaRamoServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaRamo result = ramocaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaRamo 	::		CaRamoServiceImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaRamo 	::		CaRamoServiceImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaRamo findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaRamoServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaRamo instance = ramocaDao.findById(id);//entityManager.find(CaRamo.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaRamoServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaRamoServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaRamo entities with a specific property value.  
	 
	  @param propertyName the name of the CaRamo property to query
	  @param value the property value to match
	  	  @return List<CaRamo> found by query
	 */
    public List<CaRamo> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaRamoServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaRamo model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaRamoServiceImpl	::	findByProperty	::	FIN	::	");
			return ramocaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaRamoServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaRamo> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaRamo> findByIdentificador(Object identificador
	) {
		return findByProperty(IDENTIFICADOR, identificador
		);
	}
	
	public List<CaRamo> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaRamo> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaRamo> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaRamo entities.
	  	  @return List<CaRamo> all CaRamo entities
	 */
	public List<CaRamo> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaRamoServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaRamo model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaRamoServiceImpl	::	findAll	::	FIN	::	");
			return ramocaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaRamoServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}