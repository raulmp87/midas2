package mx.com.afirme.midas2.service.impl.calculos;

import static mx.com.afirme.midas2.utils.CommonUtils.isNull;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDTO;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeFacadeRemote;
import mx.com.afirme.midas2.dao.calculos.SolicitudChequesMizarDao;
import mx.com.afirme.midas2.dao.calculos.SolicitudChequesMizarDao.ClavesTransaccionesContables;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.contabilidad.SolicitudCheque;
import mx.com.afirme.midas2.dto.calculos.DetalleSolicitudCheque;
import mx.com.afirme.midas2.dto.emision.ppct.StatusSolicitudChequeDTO;
import mx.com.afirme.midas2.service.calculos.SolicitudChequesMizarService;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@SuppressWarnings("unused")
@Stateless
public class SolicitudChequesMizarServiceImpl implements SolicitudChequesMizarService{
	
	public Long iniciarCalculoComisiones(Long idConfiguracionComisiones,Long idCalculoComisiones,Long idCalculoTemporal,String queryAgentes) throws MidasException{
		return dao.iniciarCalculoComisiones(idConfiguracionComisiones,idCalculoComisiones,idCalculoTemporal,queryAgentes);
	}
	
	public Long iniciarCalculoBonos(Long pIdCalculoIn,Long pIdProgramacion,Long pIdConfigBono,Long pIdModoEjec,Date pFechaAlta,Date pFechaEjecucion,String pHora,Long pPeriodoEjecucion,Long pTipoConfiguracion,String pUsuarioAlta,Long pTipoBono,Long pClaveEstatus,Integer pActivo,Integer pEsNegocio) throws MidasException{
		return dao.iniciarCalculoBonos(pIdCalculoIn,pIdProgramacion,pIdConfigBono,pIdModoEjec,pFechaAlta,pFechaEjecucion,pHora,pPeriodoEjecucion,pTipoConfiguracion,pUsuarioAlta,pTipoBono,pClaveEstatus,pActivo,pEsNegocio);
	}
	
	@Override
	public Long iniciarCalculoProvisiones(Long pIdCalculoIn,Long pIdProgramacion,Long pIdConfigBono,Long pIdModoEjec,Date pFechaAlta,Date pFechaEjecucion,String pHora,Long pPeriodoEjecucion,Long pTipoConfiguracion,String pUsuarioAlta,Long pTipoBono,Long pClaveEstatus,Integer pActivo,Integer pEsNegocio) throws MidasException {
		return dao.iniciarCalculoProvisiones(pIdCalculoIn,pIdProgramacion,pIdConfigBono,pIdModoEjec,pFechaAlta,pFechaEjecucion,pHora,pPeriodoEjecucion,pTipoConfiguracion,pUsuarioAlta,pTipoBono,pClaveEstatus,pActivo,pEsNegocio);
	}
		
	/**
	 * Metodo para contabilizar movimientos y afectar cuentas contables de Mizar.
	 * @param identificador Es el identificador para obtener el cursor CONTABLE, en caso de COMISIONES y BONOS, se envia idCalculo|idAgente, en caso del prestamo solo se envia el idPrestamo
	 * @param claveTransaccionContable dependiendo de la clave contable
	 * @throws MidasException
	 */
	public void contabilizarMovimientos(String identificador,ClavesTransaccionesContables claveTransaccionContable) throws MidasException{
		dao.contabilizarMovimientos(identificador,claveTransaccionContable);
	}
	
	/**
	 * Agrega una solicitud de cheque en MIZAR a partir de una solicitud de cheque de SEYCOS
	 * @param solicitudCheque Solicitud de cheque de SEYCOS
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void agregarSolicitudChequeMIZAR(SolicitudChequeDTO solicitudCheque) throws Exception {
				
		if (isNull(solicitudCheque.getIdSesion())) {
			
			throw new RuntimeException("Falta el identificador de la sesión de las solicitudes de cheque que se procesarán en MIZAR");
		
		}
		
		if (isNull(solicitudCheque.getIdSolCheque())) {
			
			throw new RuntimeException("Falta el identificador de la solicitud de cheque que se agregará en MIZAR");
		
		}
		
		/*
		//TODO Descomentar esta validación cuando se agregue un estatus de solicitud de cheque especial que identifique al mismo tiempo que
		//esta pendiente y que proviene de una interfaz de MIDAS (ejemplo: StatusSolicitudChequeDTO.PENDIENTE_MIDAS = "X")
		
		SolicitudCheque solicitudChequeEntidad = entidadDao.findById(SolicitudCheque.class, solicitudCheque.getIdSolCheque());
		
		if (!solicitudChequeEntidad.getStatus().equals(StatusSolicitudChequeDTO.PENDIENTE)) {
			
			throw new RuntimeException("La solicitud de cheque ya había sido procesada por MIZAR");
			
		}
		*/
		
		solicitudCheque.setIdAgente((solicitudCheque.getIdAgente() != null ? solicitudCheque.getIdAgente() : 0L));
		solicitudCheque.setTipoPago((solicitudCheque.getTipoPago() != null ? solicitudCheque.getTipoPago() : "CH"));
		solicitudCheque.setTipoCambio((solicitudCheque.getTipoCambio() != null ? solicitudCheque.getTipoCambio() : 1));
		solicitudCheque.setPctIVAAcreditable((solicitudCheque.getPctIVAAcreditable() != null ? solicitudCheque.getPctIVAAcreditable() : 0));
		solicitudCheque.setPctIVARetenido((solicitudCheque.getPctIVARetenido() != null ? solicitudCheque.getPctIVARetenido() : 0));
		solicitudCheque.setPctISRRetenido((solicitudCheque.getPctISRRetenido() != null ? solicitudCheque.getPctISRRetenido() : 0));
		solicitudCheque.setClaveTipoOperacion((solicitudCheque.getClaveTipoOperacion() != null ? solicitudCheque.getClaveTipoOperacion() : 0));
		
		dao.agregarEncabezadoSolicitudChequeMIZAR(solicitudCheque);
		
		List<DetalleSolicitudCheque> detallesSolicitudCheque 
			= solicitudChequeFacade.obtenerDetallePorSolicitudCheque(solicitudCheque.getIdSolCheque().longValue());
		
		for (DetalleSolicitudCheque detalleSolicitudCheque : detallesSolicitudCheque) {
			
			detalleSolicitudCheque.setRubro(detalleSolicitudCheque.getRubro().trim().equals("R7") ? "IVAA" : detalleSolicitudCheque.getRubro());
			
			dao.agregarDetalleSolicitudChequeMIZAR(detalleSolicitudCheque, solicitudCheque);
			
		}

	}
	
	/**
	 * Procesa en MIZAR las solicitudes de cheque recien importadas desde SEYCOS
	 * @param idSesion Es el identificador de la sesión con la que están relacionadas todas las solicitudes de cheque que serán procesadas
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void procesarSolicitudesChequeEnMIZAR(Long idSesion) throws Exception {
		
		if (isNull(idSesion)) {
			
			throw new RuntimeException("Falta el identificador de la sesión de las solicitudes de cheque que se procesarán en MIZAR");
		
		}
		
		dao.procesarSolicitudesChequeEnMIZAR(idSesion);
		
	}
	
	@EJB
	private SolicitudChequesMizarDao dao;
	
	@EJB
	private SolicitudChequeFacadeRemote solicitudChequeFacade;
	
	@EJB
	private EntidadDao entidadDao;
	
}
