package mx.com.afirme.midas2.service.impl.tarifa;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaFacadeRemote;
import mx.com.afirme.midas2.dao.tarifa.TarifaVersionDao;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersion;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;
import mx.com.afirme.midas2.service.tarifa.TarifaVersionService;

@Stateless
public class TarifaVersionServiceImpl implements TarifaVersionService {

	public TarifaVersion cambiarEstatus(TarifaVersionId id, Short claveEstatus) {
		
		if (claveEstatus.equals(TarifaVersion.ESTATUS_ACTIVO)) {
			List<TarifaVersion> tarifaVersionList = tarifaVersionDao.cargarTarifas(id);
			for(TarifaVersion modif: tarifaVersionList){
				modif.setClaveEstatus(TarifaVersion.ESTATUS_INACTIVO);
				tarifaVersionDao.update(modif);
			}
		}
				
		TarifaVersion tarifaVersion = tarifaVersionDao.findById(id);
		tarifaVersion.setClaveEstatus(claveEstatus);
		
		if (claveEstatus.equals(TarifaVersion.ESTATUS_ACTIVO)) {
			tarifaVersion.setFechaActivacion(new Date());
			tarifaVersion.setFechaInicioVigencia(new Date());
		}
		
		
		tarifaVersionDao.update(tarifaVersion);
		
		
		if (claveEstatus.equals(TarifaVersion.ESTATUS_BORRADO)) {
			
			Long nuevaVersion = 1L;
			
			List<Long> versiones = tarifaVersionDao.cargarVersiones(id);
			
			if(versiones != null && versiones.size() > 0) {
				Collections.sort(versiones);
				
				nuevaVersion = versiones.get(0);
			}
			
			TarifaVersionId nuevoId = new TarifaVersionId();
			nuevoId.setIdConcepto(tarifaVersion.getId().getIdConcepto());
			nuevoId.setIdMoneda(tarifaVersion.getId().getIdMoneda());
			nuevoId.setIdRiesgo(tarifaVersion.getId().getIdRiesgo());
			nuevoId.setVersion(nuevaVersion);
			
			return obtieneTarifaVersion(nuevoId);
			
		}
				
		return tarifaVersion;
	}

	public LinkedHashMap<String, String> cargarVersiones(TarifaVersionId id) {
		
		List<Long> versiones = tarifaVersionDao.cargarVersiones(id);
		
		if(versiones != null && versiones.size() > 0) {
			Collections.sort(versiones);
		}
		
		
		LinkedHashMap<String, String> mapaVersiones = new LinkedHashMap<String, String>();
		
		for (Long version : versiones) {
			mapaVersiones.put(version.toString(), version.toString());
		}
		
		if (mapaVersiones.size() == 0) {
			mapaVersiones.put(id.getVersion().toString(), id.getVersion().toString());
		}
		
		return mapaVersiones;
	}

	public TarifaVersion findById(TarifaVersionId id) {
		return obtieneTarifaVersion(id);
	}

	public TarifaVersion generarNuevaVersion(TarifaVersionId id) {
		
		Long nuevaVersion = null;
		TarifaVersion tarifaVersion = tarifaVersionDao.findById(id);
		
		if (tarifaVersion != null) {
			
			nuevaVersion = tarifaVersionDao.generarNuevaVersion(id, tarifaVersion.getTarifaConcepto().getClaveNegocio(), 
					tarifaVersion.getTarifaConcepto().getTipoTarifa());	
			
		}
		
		if (nuevaVersion != null) {
			id.setVersion(nuevaVersion);
		}
		
		return obtieneTarifaVersion(id);
	}
		
	public TarifaVersion cargarPorValorMenu(String valorMenu, Long version,Long idMoneda) {
		
		int indiceGuion = valorMenu.lastIndexOf("_");
		
		int indiceParentesis = valorMenu.lastIndexOf("(");
		
		Long idConcepto = Long.parseLong(valorMenu.substring(indiceParentesis-2, indiceParentesis));
		
		Long idRiesgo = Long.parseLong(valorMenu.substring(indiceGuion + 1, indiceParentesis-2));
		
		if (idMoneda == null ) {
			idMoneda = Long.valueOf(MonedaDTO.MONEDA_PESOS);
		}
		
		TarifaVersionId id = new TarifaVersionId();
		
		id.setIdConcepto(idConcepto);
		id.setIdMoneda(idMoneda);
		id.setIdRiesgo(idRiesgo);
		
		
		if (version == null ) {
			
			List<Long> versiones = tarifaVersionDao.cargarVersiones(id);
			
			if(versiones != null && versiones.size() > 0) {
				Collections.sort(versiones);
				version = versiones.get(0);
			} else {
				version = 1L;
			}
		}
		
		id.setVersion(version);
		
		
		//Por ser la primera vez que se carga, se setearon valores default para
		//moneda y version
		
		return obtieneTarifaVersion(id);
	}
	
	public Map<String, String> cargarMonedas() {
		
		List<MonedaDTO> monedas = monedaFacade.findAll();
		Map<String, String> mapaMonedas = new LinkedHashMap<String, String>();
		
		for (MonedaDTO moneda : monedas) {
			mapaMonedas.put(moneda.getIdTcMoneda().toString(), moneda.getDescription());
		}
				
		return mapaMonedas;
	}
	
	public Map<String, String> cargarMonedasByTarifaVersion(TarifaVersionId id) {
		
		List<Long> monedas = tarifaVersionDao.cargarMonedas(id);
		List<MonedaDTO> monedasAll = monedaFacade.findAll();
		
		if(monedas != null && monedas.size() > 0) {
			Collections.sort(monedas);
		}
		
		
		LinkedHashMap<String, String> mapaMonedas = new LinkedHashMap<String, String>();
		
		for (Long version : monedas) {
			for(MonedaDTO moneda : monedasAll){
				if(version.equals(new Long(moneda.getIdTcMoneda()))){
					mapaMonedas.put(moneda.getIdTcMoneda().toString(), moneda.getDescription());
				}
			}			
		}
		
		if (mapaMonedas.size() == 0) {
			mapaMonedas.put(id.getIdMoneda().toString(), id.getIdMoneda().toString());
		}
		
		return mapaMonedas;
	}
	/**
	 * Devuelve una instancia de TarifaVersion (vacia en caso de que no exista alguna con el id especificado)
	 * @param id Id de la TarifaVersion a obtener
	 * @return
	 */
	private TarifaVersion obtieneTarifaVersion(TarifaVersionId id) {
		
		TarifaVersion tarifaVersion = tarifaVersionDao.findById(id);
		
		if (tarifaVersion == null) {
			tarifaVersion = new TarifaVersion();
			tarifaVersion.setId(id);
		}
			
		return tarifaVersion;
	}
	
	private TarifaVersionDao tarifaVersionDao;
	
	private MonedaFacadeRemote monedaFacade;
	
	@EJB
	public void setTarifaVersionDao(TarifaVersionDao tarifaVersionDao) {
		this.tarifaVersionDao = tarifaVersionDao;
	}

	@EJB
	public void setMonedaFacade(MonedaFacadeRemote monedaFacade) {
		this.monedaFacade = monedaFacade;
	}
	

}
