package mx.com.afirme.midas2.service.impl.cobranza.reportes;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.cobranza.reportes.ReportesCobranzaDao;
import mx.com.afirme.midas2.domain.cobranza.reportes.CalendarioGonherDTO;
import mx.com.afirme.midas2.domain.cobranza.reportes.CargoAutoDomiTC;
import mx.com.afirme.midas2.service.cobranza.reportes.ReportesCobranzaService;

@Stateless
public class ReportesCobranzaServiceImpl implements ReportesCobranzaService{

	@EJB
	private ReportesCobranzaDao reportesCobranzaDao;
	
	@Override
	public List<CargoAutoDomiTC> getReporteCargoAutoDomiTC() {
		return reportesCobranzaDao.getReporteCargoAutoDomiTC();
	}
	
	@Override
	public CalendarioGonherDTO getFechaSiguienteProgramacion(){
		return reportesCobranzaDao.getFechaSiguienteProgramacion();
	}
	
	@Override
	public void saveConfrimProgramacion(CalendarioGonherDTO calendarioGonherDTO){
		reportesCobranzaDao.saveConfrimProgramacion(calendarioGonherDTO);
	}

	public void setReportesCobranzaDao(ReportesCobranzaDao reportesCobranzaDao) {
		this.reportesCobranzaDao = reportesCobranzaDao;
	}
	
	
}