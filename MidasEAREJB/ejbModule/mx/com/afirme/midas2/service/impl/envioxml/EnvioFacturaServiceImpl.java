package mx.com.afirme.midas2.service.impl.envioxml;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.PersistenceException;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.dao.envioxml.EnvioFacturaDao;
import mx.com.afirme.midas2.dao.envioxml.EnvioFacturaDetalleDao;
import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;
import mx.com.afirme.midas2.domain.envioxml.EnvioFacturaDet;
import mx.com.afirme.midas2.service.envioxml.EnvioFacturaService;
import mx.com.afirme.midas2.ws.axosnet.AxnAdditionals;
import mx.com.afirme.midas2.ws.axosnet.AxnResponse;
import mx.com.afirme.midas2.ws.axosnet.ValResponse;
import mx.com.afirme.midas2.ws.axosnet.WSValidacionesSATPortProxy;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;


@Stateless
public class EnvioFacturaServiceImpl implements EnvioFacturaService {

	private EnvioFacturaDao envioFacturaDao;
	private EnvioFacturaDetalleDao envioFacturaDetalleDao;

	private static final Integer CLAVE_STATUS_REPORTADO = 1;
	private static final Integer CLAVE_STATUS_NO_REPORTADO = 0;
	
	private static final Integer CLAVE_STATUS_ACEPTADO = 1;
	private static final Integer CLAVE_STATUS_RECHAZADO = 0;
	private static final Integer CLAVE_VALIDACION_FISCAL = 1;
	private static final Integer CLAVE_VALIDACION_COMERCIAL = 2;
	private static Logger log = Logger.getLogger("EnvioFacturaServiceImpl");
	
	@Override
	public long getIncrementedProperty(EnvioFactura envioFactura){
			EnvioFactura filter = new EnvioFactura();
			filter.setOrigenEnvio(envioFactura.getOrigenEnvio());
			filter.setRespuestas(null);
			return envioFacturaDao.getIncrementedProperty(EnvioFactura.class, "idOrigenEnvio", filter);
	}

	@Override
	public List<EnvioFactura> findByOperation(EnvioFactura envioFactura, Date fechaFinal, Date fechaInicial){
		List<EnvioFactura> listaEnvios = envioFacturaDao.listarFilradoSolicitudGrid(envioFactura, fechaInicial, fechaFinal);
		return listaEnvios;
	}
	
	@Override
	public List<EnvioFactura> ObtenerComplementosPagoFactura(String  uuidFactura){
		List<EnvioFactura> listaEnvios = new ArrayList<EnvioFactura>();
		for (EnvioFactura envioFactura : this.findByProperty("uuidDocRelacionado", uuidFactura)) {
			if (envioFactura.getTipoComprobante().equals("P") && envioFactura.getEstatusEnvio()==1){
				listaEnvios.add(envioFactura);
			}
		}	
		return listaEnvios;
	}
	
	@Override
	public List<EnvioFactura> ObtenerComplementosPagoFactura(Long  idLote){
		List<EnvioFactura> listaEnvios = new ArrayList<EnvioFactura>();
		for (EnvioFactura envioFactura : this.findByProperty("idLote", idLote)) {
			if (envioFactura.getTipoComprobante().equals("P") && envioFactura.getEstatusEnvio()==1){
				listaEnvios.add(envioFactura);
			}
		}	
		return listaEnvios;
	}
	
	
	@Override
	public List<EnvioFactura> findByProperty(String  nombrePropiedad, Object valorPropiedad){		
		return envioFacturaDao.findByProperty(EnvioFactura.class, nombrePropiedad, valorPropiedad);
	}
	
	@Override
	public List<EnvioFacturaDet> findByIdEnvio(Long idEnvio){
		List<EnvioFacturaDet> listaEnviosDet = envioFacturaDao.findByProperty(EnvioFacturaDet.class, "idEnvio", idEnvio);
		return listaEnviosDet;
	}	
	
	public String getComprobante(File archivo, String xml) throws SystemException, Exception{
		String comprobante = ""; 
		FileInputStream fis = null;
		try {   
			if(archivo!=null){
				fis = new FileInputStream(archivo);
				StringBuilder  stringBuilder = new StringBuilder();
			    InputStreamReader inputStreamReader = new InputStreamReader((InputStream)fis);		    
			    
			    BufferedReader br = new BufferedReader(inputStreamReader);
			    String line = null;

			    while ((line = br.readLine()) != null) {
			    	stringBuilder.append(line);
			    }   
			    
			    comprobante = stringBuilder.toString();
			    
			}else if(xml!=null){
				comprobante = xml;
			}
			//se cambia el tipo de encoding para corregir error del sello digital.
			//comprobante = new String(comprobante.getBytes(), "UTF-8");
			
		} catch(FileNotFoundException e) {
			log.error("Error el Archivo XML esta vacio", e);
			throw new SystemException ("Error al obtener el contenido del archivo");
		} catch (Exception e) {
			log.error("Error al convertir Archivo a String", e);
			throw e;
		} finally {
			IOUtils.closeQuietly(fis);
		}
		if("".equals(comprobante)){
			throw new SystemException("El comprobante XML esta vacio");
		}
		
		return comprobante;
	}
	
	@Override
	public EnvioFactura saveEnvio(EnvioFactura envioFactura, String ext) throws SystemException, Exception{
		if("xml".equalsIgnoreCase(ext.trim())){
			log.info("XML FileName: " + envioFactura.getFacturaXml().getName());
			return saveEnvio(envioFactura);
		}else if("zip".equalsIgnoreCase(ext.trim())){
			List<EnvioFactura> facturas = procesarZipFacturas(envioFactura);
			if(facturas==null || (facturas!=null && facturas.isEmpty())){
				throw new SystemException("No se encontraron comprobantes en el ZIP");
			}
			List<EnvioFactura> facturasSinException = new LinkedList<EnvioFactura>();
			for(EnvioFactura factura: facturas){
				try{
					facturasSinException.add(this.saveEnvio(factura, false));
				}catch(Exception e){
					log.error("Error el Archivo XML: [{}]", e);
				}
			}
			for(EnvioFactura facturaSinException: facturasSinException){
				this.guardaDatosEnvio(facturaSinException);
			}
			return envioFactura; 
		}
		return null;
	}
	
	private List<EnvioFactura> procesarZipFacturas(EnvioFactura envioFactura) {
		final int bufferSize = 2048;
		EnvioFactura factura = null;
		List<EnvioFactura> facturas = new LinkedList<EnvioFactura>();
		ZipEntry entry = null;
		byte[] buffer = new byte[bufferSize];		
		try {
			ZipInputStream zip = new ZipInputStream(new FileInputStream(envioFactura.getFacturaXml()));
			while ((entry = zip.getNextEntry()) != null) {
				String fileName = entry.getName();
				if(fileName.endsWith(".xml") || fileName.endsWith(".XML")){
					log.info("ZIP FileName :"+fileName);
					int size=0;
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					while ((size = zip.read(buffer, 0, buffer.length)) != -1) {
						bos.write(buffer, 0, size);
						buffer = new byte[bufferSize];
					}
					String comprobante = bos.toString();
					factura = new EnvioFactura();
					factura.setCuentaAfectada(envioFactura.getCuentaAfectada() !=null ? envioFactura.getCuentaAfectada() : "");
					factura.setIdOrigenEnvio(envioFactura.getIdOrigenEnvio());
					factura.setOrigenEnvio(envioFactura.getOrigenEnvio());
					factura.setComprobante(comprobante);
					factura.setIdDepartamento(envioFactura.getIdDepartamento());
					facturas.add(factura);
					zip.closeEntry();
				}
			}
			zip.closeEntry();
			zip.close();
		} catch (FileNotFoundException e) {
			log.error("Error al convertir Archivo a String", e);
		} catch (IOException e) {
			log.error("Error al convertir Archivo a String", e);
		}
		return facturas;
	}

	public EnvioFactura saveEnvio(EnvioFactura envioFactura) throws FileNotFoundException, PersistenceException, SystemException, Exception {
		return this.saveEnvio(envioFactura, true);
	}

	/**
	 * Se usa save igual a true cuando es una carga masiva de xml y el guardado de la info se haces despues de validar todos los xml
	 */
	@Override
	public EnvioFactura saveEnvio(EnvioFactura envioFactura, boolean save) throws PersistenceException, SystemException, Exception {
		
		
		if(envioFactura!=null && (envioFactura.getFacturaXml()==null && envioFactura.getComprobante()==null)){
			throw new SystemException("Seleccionar archivo XML a validar");
		}
		
		String comprobante = this.getComprobante(envioFactura.getFacturaXml(), envioFactura.getComprobante());
		String cuentaAfectada = envioFactura.getCuentaAfectada() != null ? envioFactura.getCuentaAfectada() : "";
		
		WSValidacionesSATPortProxy proxySAT = null;
		ValResponse response = null;
	    	      					
		try {
			
			//Para los desarrolladores de siniestros  --> se cambio la regla de negocio 
			//el subtotal de la factura se compara contra el importe gravable + el importe excento
			//subtotal = honorarios.getImpGravable().add(honorarios.getImpExcento());
			
			//Log parametros que se envian a validador
			log.info("Información enviada a Validador AxosNet: ");
			log.info("Comprobante: " + comprobante);
			log.info("Razon Social: " + envioFactura.getRazonSocial());
			log.info("RFC Axosnet: " + envioFactura.getImporte());
			log.info("Importe: " + envioFactura.getImporte());
			log.info("IVA: " + envioFactura.getIva());
			log.info("ISR: " + envioFactura.getIsr());
			log.info("Iva Retenido: " + envioFactura.getIvaRetenido());
			log.info("Iva Trasladado: " + envioFactura.getIvaTrasladado());
			log.info("Descuento: " + envioFactura.getDescuento());
			log.info("Total: " + envioFactura.getTotal());
			log.info("Tolerancia: " + envioFactura.getTolerancia());
			log.info("ID Sociedad: " + envioFactura.getIdSociedad());
			log.info("ID Departamento: " + envioFactura.getIdDepartamento());
			log.info("Referencia: " + envioFactura.getReferencia());
			log.info("Concepto: " + envioFactura.getConcepto());
			
			proxySAT =  new WSValidacionesSATPortProxy();
						
			response = proxySAT.verificaComprobanteFiscalDigital(comprobante!=null?comprobante:"", 
																envioFactura.getRazonSocial()!=null?envioFactura.getRazonSocial():"", 
																envioFactura.getRfcAxosnet()!=null?envioFactura.getRfcAxosnet():"",
																envioFactura.getImporte()!=null?envioFactura.getImporte():"", 
																envioFactura.getIva()!=null?envioFactura.getIva():"", 
																envioFactura.getIsr()!=null?envioFactura.getIsr():"", 
																envioFactura.getIvaRetenido()!=null?envioFactura.getIvaRetenido():"",  
																envioFactura.getIvaTrasladado()!=null?envioFactura.getIvaTrasladado():"",  
																envioFactura.getDescuento()!=null?envioFactura.getDescuento():"",
																envioFactura.getTotal()!=null?envioFactura.getTotal():"",   
																envioFactura.getTolerancia()!=null?envioFactura.getTolerancia():"",  
																envioFactura.getIdSociedad()!=null?envioFactura.getIdSociedad():"", 
																envioFactura.getIdDepartamento()!=null?envioFactura.getIdDepartamento():"",  
																envioFactura.getReferencia()!=null?envioFactura.getReferencia():"", 
																envioFactura.getConcepto()!=null?envioFactura.getConcepto():"");
			
			
			
		} catch(Exception e) {
			log.error("Error de comunicacion con WS ValidacionesSAT");
			throw new SystemException ("Error en el servicio que guarda la factura", e);
		}
		
		if (response.isValid()) {
			//Si regresa que es valido pero sin mensages y sin informacion adicional es por que fallo el WS
			if(response.getAdditionals() == null || response.getAdditionals().isEmpty()) {
				throw new SystemException("Error en el servicio que valida la Factura, No se regresó información adicional");
			}				
					
			if(response.getMessages() == null || response.getMessages().isEmpty()) {
				throw new SystemException("Error al cargar su Factura, Revise el archivo enviado");
			}
			
		}
		
		return  this.saveEnvio(response, envioFactura.getOrigenEnvio(), envioFactura.getIdOrigenEnvio(), cuentaAfectada, save);
	}
	
	/**
	 * Este metodo deberia ser privado y solo mandarse llamar del metodo saveEnvio de la class EnvioFacturaServiceImpl
	 * por usuarse en agentes se hace una excepcion
	 * @param response
	 * @param origenEnvio
	 * @param idOrigenEnvio
	 * @param cuentaAfectada
	 * @return
	 * @throws Exception
	 */
	public EnvioFactura saveEnvio(Object obj, String origenEnvio, Long idOrigenEnvio, String cuentaAfectada, boolean save) throws Exception{
		final int tipoRespuestaEnRepositorio = 20;
		final int tipoRespuestaErrores = 500;
		ValResponse valResponse = (ValResponse)obj;
		
		EnvioFactura envioFactura = new EnvioFactura();
		
		envioFactura.setIdOrigenEnvio(idOrigenEnvio);
		envioFactura.setOrigenEnvio(origenEnvio);
		envioFactura.setCuentaAfectada(cuentaAfectada);
		
		String fecha_cfdi = null;
		String folio_cfdi = null;
		String importe_cfdi = null;
		String moneda_cfdi = null;
		String rfc_emisor_cfdi = null;
		String rfc_receptor_cfdi = null;
		String serie_cfdi = null;
		String tipo_cambio_cfdi = null;
		String uuid_cfdi = null;
		
		List<AxnResponse> mensajes = new LinkedList<AxnResponse>();
		List<AxnAdditionals> additionals = new LinkedList<AxnAdditionals>();
		List<EnvioFacturaDet> mensajesEnvio = new LinkedList<EnvioFacturaDet>();
		EnvioFacturaDet mensajeEnvio = null;
		
		envioFactura.setNumeroErrores(0);
		envioFactura.setValid(valResponse.isValid());
		additionals = valResponse.getAdditionals();
		mensajes = valResponse.getMessages();
	
		try{
									
			for(AxnResponse mensaje: mensajes){
				
				/* el mensaje.id es el que define que tipo de validacion es, si es menor que 20 es fiscal
				 * los espacios que quedan entre el 12 y 20 se deben llenar convalidaciones fiscales del 30 en 
				 * adelante con validaciones comerciales.
				 * 
				•	0 - Datos del XML
				•	10 - Validación de versión (Fiscal)
				•	11 - Validación de primer sello (Fiscal)
				•	12 - Validación de segundo sello (Fiscal)
				•	20 - Validación de factura previamente ingresada (Comercial)
				•	21 - Validación de la razón social del emisor (Comercial)
				•	22 - Validación del RFC del emisor (Comercial)
				•	23 - Validación del subtotal (Comercial)
				•	24 - Validación del IVA (Trasladado + Retenido) (Comercial)
				•	25 - Validación del impuesto retenido ISR (Comercial)
				•	26 - Validación de impuesto IVA retenido (Comercial)
				•	27 - Validación del impuesto IVA trasladado (Comercial)
				•	28 - Validación de descuento (Comercial)
				•	29 - Validación del total (Comercial)
				•	30 - Validación del RFC del receptor (Comercial)
				*/
								
				mensajeEnvio =  new EnvioFacturaDet();
								
				mensajeEnvio.setDescripcionRespuesta(mensaje.getMessage());
				mensajeEnvio.setIdTipoRespuesta(mensaje.getId());				
				if(mensaje.getId() < tipoRespuestaEnRepositorio) {
					mensajeEnvio.setTipoRespuesta(CLAVE_VALIDACION_FISCAL);
				} else {
					mensajeEnvio.setTipoRespuesta(CLAVE_VALIDACION_COMERCIAL);
					if(mensaje.getId() == tipoRespuestaEnRepositorio) {
						envioFactura.setFisicamenteEnRepositorio(true);
					}
					
				}
				
				if (mensaje.getCode().trim().equals("ER") && Integer.valueOf(mensaje.getId()).intValue() != tipoRespuestaErrores) {
					envioFactura.setNumeroErrores(envioFactura.getNumeroErrores()+1);
				}
				
				mensajesEnvio.add(mensajeEnvio);
				
			}
		} catch(Exception e) {
			log.error("Error al guardar detalle de envio tipo: "+origenEnvio+" id:"+idOrigenEnvio, e);
			throw new PersistenceException("Error al guardar los datos de la factura");
		}
		
		try {
			
			if(additionals!=null){
				for( AxnAdditionals  additional :additionals){
					
					if("ID".equals(additional.getCode()) && additional.getId() == 0){
						uuid_cfdi = additional.getAdditional();
					}else if("MT".equals(additional.getCode()) && additional.getId() == 0){
						importe_cfdi = additional.getAdditional();
					}else if("RE".equals(additional.getCode()) && additional.getId() == 0){
						rfc_emisor_cfdi = additional.getAdditional();
					}else if("FO".equals(additional.getCode()) && additional.getId() == 0){
						folio_cfdi = additional.getAdditional();
					}else if("SE".equals(additional.getCode()) && additional.getId() == 0){
						serie_cfdi = additional.getAdditional();
					}else if("RR".equals(additional.getCode()) && additional.getId() == 0){
						rfc_receptor_cfdi = additional.getAdditional();
					}else if("FE".equals(additional.getCode()) && additional.getId() == 0){
						fecha_cfdi = additional.getAdditional();
					}else if("MN".equals(additional.getCode()) && additional.getId() == 0){
						moneda_cfdi = additional.getAdditional();
					}else if("TC".equals(additional.getCode()) && additional.getId() == 0){
						tipo_cambio_cfdi = additional.getAdditional();
					}
				}
			}
			
			envioFactura.setFechaEnvio(new Date());
						
			envioFactura.setUuidCfdi(uuid_cfdi);
			envioFactura.setRfcEmisorCfdi(rfc_emisor_cfdi);
			envioFactura.setRfcReceptorCfdi(rfc_receptor_cfdi);
			envioFactura.setSerieCfdi(serie_cfdi);
			envioFactura.setFolioCfdi(folio_cfdi);
			envioFactura.setFechaCfdi(fecha_cfdi);
			envioFactura.setImporteCfdi(importe_cfdi);
			envioFactura.setMonedaCfdi(moneda_cfdi);
			envioFactura.setTipoCambioCfdi(tipo_cambio_cfdi);
			
			envioFactura.setRespuestas(mensajesEnvio);
			if(save){				
				this.guardaDatosEnvio(envioFactura);
			}
						
		} catch(Exception e) {
			log.error("Error al guardar el envio de la factura tipo: "+origenEnvio+" id:"+idOrigenEnvio, e);
			throw new SystemException("Error al guardar el envio de la factura");
		}
		return envioFactura;
	}
	
	public void guardaDatosEnvio(EnvioFactura envioFactura) {
		//Tambien se considera envio aceptado en el caso de que el WS mande un rechazo porque ya existia la factura en el repositorio
		//pero aun no se encuentra aceptado en el Inventario de Facturas
		if (envioFactura.isValid() || (envioFactura.getNumeroErrores() == 1 && envioFactura.isFisicamenteEnRepositorio() && existeEnInventarioFacturas(envioFactura))) {
			envioFactura.setEstatusEnvio(CLAVE_STATUS_ACEPTADO);
			envioFactura.setEstatusReporte(CLAVE_STATUS_NO_REPORTADO);
		} else {
			envioFactura.setEstatusEnvio(CLAVE_STATUS_RECHAZADO);
			envioFactura.setEstatusReporte(CLAVE_STATUS_REPORTADO);
		}
	
	
		envioFactura = envioFacturaDao.guardaEnvio(envioFactura);
	
		for (EnvioFacturaDet mensajeEnvioI : envioFactura.getRespuestas()) {
			mensajeEnvioI.setIdEnvio(envioFactura.getIdEnvio());
		}
	
		envioFacturaDetalleDao.guardarDetalleEnvio(envioFactura.getRespuestas());
	}
	
	public boolean existeEnInventarioFacturas(EnvioFactura envioFactura) {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uuidCfdi", envioFactura.getUuidCfdi());
		params.put("estatusEnvio", CLAVE_STATUS_ACEPTADO);
		params.put("idOrigenEnvio", envioFactura.getIdOrigenEnvio()); 
		params.put("origenEnvio", envioFactura.getOrigenEnvio()); 
		
		List<EnvioFactura> lista = envioFacturaDao.findByProperties(EnvioFactura.class, params);
		
		if (lista != null && !lista.isEmpty()) {
			
			return true;
			
		}
		
		return false;
		
	}
	
	
	public EnvioFacturaDao getEnvioFacturaDao() {
		return envioFacturaDao;
	}
	
	@EJB
	public void setEnvioFacturaDao(EnvioFacturaDao envioFacturaDao) {
		this.envioFacturaDao = envioFacturaDao;
	}
		
	public EnvioFacturaDetalleDao getEnvioFacturaDetalleDao() {
		return envioFacturaDetalleDao;
	}

	@EJB
	public void setEnvioFacturaDetalleDao(EnvioFacturaDetalleDao envioFacturaDetalleDao) {
		this.envioFacturaDetalleDao = envioFacturaDetalleDao;
	}


}
