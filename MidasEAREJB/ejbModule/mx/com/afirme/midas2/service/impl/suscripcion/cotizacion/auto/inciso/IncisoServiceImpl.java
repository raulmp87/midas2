package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.inciso;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoCoberturaCot;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoCoberturaCotId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoCoberturaCot;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoCoberturaCotId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO_;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId_;
import mx.com.afirme.midas2.dao.bitemporal.endoso.inciso.IncisoDao;
import mx.com.afirme.midas2.domain.catalogos.ServVehiculoLinNegTipoVeh;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.CaracteristicasVehiculoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.IncisoNumeroSerieDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

import com.js.util.StringUtil;

@Stateless
public class IncisoServiceImpl extends IncisoSoporteService implements
		IncisoService {
	private static Logger LOG = Logger.getLogger(IncisoServiceImpl.class);
	@EJB private EntidadService entidadService;
	@PersistenceContext private EntityManager entityManager;
	@EJB private IncisoDao incisoDao;
	
	@Override
	public List<CaracteristicasVehiculoDTO> listarCaracteristicas(String modificadoresDescripcion) {

		VarModifDescripcion varModifDescripcion = new VarModifDescripcion();
		List<CaracteristicasVehiculoDTO> caracteristicasVehiculoList = new ArrayList<CaracteristicasVehiculoDTO>();
		List<CatalogoValorFijoDTO> catalogoValorFijoList = new ArrayList<CatalogoValorFijoDTO>();
		List<VarModifDescripcion> varModifDescripcionList = new ArrayList<VarModifDescripcion>();

		catalogoValorFijoList = catalogoValorFijoFacadeRemote.findByProperty(
				CatalogoValorFijoDTO_.id.getName() + "."
						+ CatalogoValorFijoId_.idGrupoValores.getName(), 321);
		for (CatalogoValorFijoDTO catalogoValorFijoDTOf : catalogoValorFijoList) {
			CaracteristicasVehiculoDTO caracteristicasVehiculoDTO = new CaracteristicasVehiculoDTO();
			caracteristicasVehiculoDTO.setDescripcion(catalogoValorFijoDTOf.getDescripcion());
			if(modificadoresDescripcion != null){
				try{
				caracteristicasVehiculoDTO.setValorSeleccionado(getValorSeleccionadoCaracteristicas(catalogoValorFijoDTOf.getId().getIdDato(), modificadoresDescripcion));
				}catch(Exception e){
				}
			}
			varModifDescripcion.setIdGrupo(catalogoValorFijoDTOf.getId().getIdDato());
			varModifDescripcionList = varModifDescripcionDao.findByFilters(varModifDescripcion);
			Map<String, String> listadoVariablesModificadoras = new LinkedHashMap<String, String>();
			for (VarModifDescripcion varModifDescripcionf : varModifDescripcionList) {
//				listadoVariablesModificadoras.put(catalogoValorFijoDTOf.getId()
//						.getIdGrupoValores()
//						+ "-"
//						+ catalogoValorFijoDTOf.getId().getIdDato()
//						+ "-"
//						+ varModifDescripcionf.getNumeroSecuencia()
//						+ "-"
//						+ varModifDescripcionf.getValor(),
//						varModifDescripcionf.getValor());
				listadoVariablesModificadoras.put(varModifDescripcionf.getValor(), varModifDescripcionf.getValor());
				caracteristicasVehiculoDTO.setListadoVariablesModificadoras(listadoVariablesModificadoras);				
			}
			caracteristicasVehiculoList.add(caracteristicasVehiculoDTO);
		}
		return caracteristicasVehiculoList;
	}
	
	private String getValorSeleccionadoCaracteristicas(int idDato, String modificadoresDescripcion){
		String[] modificadores =  modificadoresDescripcion.split("\\|");
		String valor = UtileriasWeb.STRING_EMPTY;
		switch(idDato){
		case IncisoAutoCot.TRANSMISION:
			valor = modificadores[0];
			break;
		case IncisoAutoCot.CILINDROS:
			valor = modificadores[1];
			break;
		case IncisoAutoCot.BOLSAS_AIRE:
			valor = modificadores[2];
			break;
		case IncisoAutoCot.SIS_ELECTRICO:
			valor = modificadores[3];
			break;
		case IncisoAutoCot.VESTIDURAS:
			valor = modificadores[4];
			break;
		case IncisoAutoCot.QUEMACOCOS:
			valor = modificadores[5];
			break;
		case IncisoAutoCot.VERSION:
			valor = modificadores[6];
			break;
		case IncisoAutoCot.FRENOS:
			valor = modificadores[7];
			break;
		case IncisoAutoCot.SONIDO:
			valor = modificadores[8];
			break;
		case IncisoAutoCot.AIR_ACONDIC:
			valor = modificadores[9];
			break;
		case IncisoAutoCot.COMBUSTIBLE:
			valor = modificadores[10];
			break;
		case IncisoAutoCot.ALARMA_FABR:
			valor = modificadores[11];
			break;
		case IncisoAutoCot.INYECCION:
			valor = modificadores[12];
			break;
		default:
			break;
		}
		
		return valor;
	}

	@Override
	public IncisoAutoCot definirOtrasCaract(List<CaracteristicasVehiculoDTO> caracteristicasVehiculoList, String descripcionBase) {
		IncisoAutoCot incisoAutoCot = new IncisoAutoCot();
		StringBuilder sModificadores = new StringBuilder();
		StringBuilder sDescripcion = new StringBuilder();
		String temp;
		for (CaracteristicasVehiculoDTO caracteristicasVehiculoDTO : caracteristicasVehiculoList) {
			
			if (caracteristicasVehiculoDTO.getValorSeleccionado() != null
					&& !caracteristicasVehiculoDTO.getValorSeleccionado()
							.equals(UtileriasWeb.STRING_EMPTY)) {
				temp = caracteristicasVehiculoDTO.getValorSeleccionado();
			}else{
				temp = UtileriasWeb.STRING_EMPTY;
			}	
			sModificadores.append(StringUtils.rightPad(temp, 6, " "));
			sModificadores.append(UtileriasWeb.SEPARADOR_VERTICAL);
			if(!StringUtil.isEmpty(temp)){
				sDescripcion.append(temp.trim());
				sDescripcion.append(" ");		
			}
		}
		incisoAutoCot.setModificadoresDescripcion(sModificadores.toString());
		incisoAutoCot.setDescripcionFinal(sDescripcion.toString());		
		return incisoAutoCot;
	}
	
	@Override
	public String getModificadoresDescripcionDefautlt(EstiloVehiculoDTO estilo){
		String modificadores = UtileriasWeb.STRING_EMPTY;
		
		modificadores += (estilo.getClaveTransmision()==null?UtileriasWeb.STRING_EMPTY:estilo.getClaveTransmision());
		modificadores += UtileriasWeb.SEPARADOR_VERTICAL + (estilo.getClaveCilindros()==null?UtileriasWeb.STRING_EMPTY:estilo.getClaveCilindros());
		modificadores += UtileriasWeb.SEPARADOR_VERTICAL + (estilo.getClaveBolsasAire()==null?UtileriasWeb.STRING_EMPTY:estilo.getClaveBolsasAire());
		modificadores += UtileriasWeb.SEPARADOR_VERTICAL + (estilo.getClaveSistemaElectrico()==null?UtileriasWeb.STRING_EMPTY:estilo.getClaveSistemaElectrico());
		modificadores += UtileriasWeb.SEPARADOR_VERTICAL + (estilo.getClaveVestidura()==null?UtileriasWeb.STRING_EMPTY:estilo.getClaveVestidura());
		modificadores += UtileriasWeb.SEPARADOR_VERTICAL + (estilo.getClaveQuemaCocos()==null?UtileriasWeb.STRING_EMPTY:estilo.getClaveQuemaCocos());
		modificadores += UtileriasWeb.SEPARADOR_VERTICAL + (estilo.getClaveVersion()==null?UtileriasWeb.STRING_EMPTY:estilo.getClaveVersion());
		modificadores += UtileriasWeb.SEPARADOR_VERTICAL + (estilo.getClaveFrenos()==null?UtileriasWeb.STRING_EMPTY:estilo.getClaveFrenos());
		modificadores += UtileriasWeb.SEPARADOR_VERTICAL + (estilo.getClaveSonido()==null?UtileriasWeb.STRING_EMPTY:estilo.getClaveSonido());
		modificadores += UtileriasWeb.SEPARADOR_VERTICAL + (estilo.getClaveAireAcondicionado()==null?UtileriasWeb.STRING_EMPTY:estilo.getClaveAireAcondicionado());
		modificadores += UtileriasWeb.SEPARADOR_VERTICAL + (estilo.getClaveTipoCombustible()==null?UtileriasWeb.STRING_EMPTY:estilo.getClaveTipoCombustible());
		modificadores += UtileriasWeb.SEPARADOR_VERTICAL + (estilo.getClaveAlarmaFabricante()==null?UtileriasWeb.STRING_EMPTY:estilo.getClaveAlarmaFabricante());
		modificadores += UtileriasWeb.SEPARADOR_VERTICAL + (estilo.getClaveTipoInyeccion()==null?UtileriasWeb.STRING_EMPTY:estilo.getClaveTipoInyeccion());
		
		return modificadores;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)	
	public void multiplicarInciso(IncisoCotizacionId incisoCotizacionId,
			int numeroCopias) {
		IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
		incisoCotizacionDTO = incisoCotizacionFacadeLocal.findById(incisoCotizacionId);
		this.copiarInciso(incisoCotizacionDTO, incisoCotizacionDTO.getId()
				.getIdToCotizacion());
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public IncisoCotizacionDTO prepareGuardarIncisoAgente(BigDecimal idToCotizacion,
			IncisoAutoCot incisoAutoCot, IncisoCotizacionDTO incisoCotizacionDTO,
			List<CoberturaCotizacionDTO> coberturaCotizacionList, Double valorPrimaNeta, Double valorSumaAsegurada, 
			Short claveObligatoriedad, Short claveContrato) {
		return prepareGuardarIncisoBase(idToCotizacion,	incisoAutoCot, incisoCotizacionDTO,
				coberturaCotizacionList, valorPrimaNeta, valorSumaAsegurada, claveObligatoriedad, 
				claveContrato, true);
	}
	
	/**
	 * Setea los datos de un inciso para que se pueda guardar. Se usa en renovaciones
	 * 
	 * @param IncisoAutoCot
	 * @param idToCotizacion
	 * @author martin
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public IncisoCotizacionDTO prepareGuardarInciso(BigDecimal idToCotizacion,
			IncisoAutoCot incisoAutoCot, IncisoCotizacionDTO incisoCotizacionDTO,
			List<CoberturaCotizacionDTO> coberturaCotizacionList, Double valorPrimaNeta, Double valorSumaAsegurada, 
			Short claveObligatoriedad, Short claveContrato) {
		return prepareGuardarIncisoBase(idToCotizacion,	incisoAutoCot, incisoCotizacionDTO,
				coberturaCotizacionList, valorPrimaNeta, valorSumaAsegurada, claveObligatoriedad, 
				claveContrato, false);
	}
	
	/**
	 * Setea los datos de un inciso para que se pueda guardar. Se usa en renovaciones
	 * 
	 * @param IncisoAutoCot
	 * @param idToCotizacion
	 * @author martin
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	private IncisoCotizacionDTO prepareGuardarIncisoBase(BigDecimal idToCotizacion,
			IncisoAutoCot incisoAutoCot, IncisoCotizacionDTO incisoCotizacionDTO,
			List<CoberturaCotizacionDTO> coberturaCotizacionList, Double valorPrimaNeta, Double valorSumaAsegurada, 
			Short claveObligatoriedad, Short claveContrato, boolean isAgente) {
		LogDeMidasEJB3.log(this.getClass().getName() + " -->Entrando a método prepareGuardarIncisoBase", Level.INFO, null);
		LogDeMidasEJB3.log("incisoCotizacionDTO.getId():"+incisoCotizacionDTO.getId(), Level.INFO, null);
		
		Long negocioPaqueteIdGuardado = null;
		IncisoCotizacionDTO inciso = null;
		boolean isServicioPublico = false;
		if (incisoCotizacionDTO.getId() != null ){
			inciso = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionDTO.getId());
			if(inciso != null){
				LogDeMidasEJB3.log("Eliminar descuentos... incisoId:"+inciso.getId(), Level.INFO, null);
				calculoService.eliminarDescuentos(inciso);
				LogDeMidasEJB3.log("Eliminar recargos... incisoId:"+inciso.getId(), Level.INFO, null);
				calculoService.eliminarRecargos(inciso);
				LogDeMidasEJB3.log("Buscando la cotizaciónId:"+idToCotizacion, Level.INFO, null);
				CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
				cotizacion.setIgualacionNivelCotizacion(Boolean.FALSE);
				cotizacion.setPrimaTotalAntesDeIgualacion(null);
				cotizacion.setDescuentoIgualacionPrimas(null);
				LogDeMidasEJB3.log("Se actualiza la cotización", Level.INFO, null);
				entidadService.save(cotizacion);
				isServicioPublico = CotizacionDTO.TIPO_COTIZACION_SERVICIO_PUBLICO.equals(cotizacion.getTipoCotizacion());
			}
		}
		
		NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class,incisoAutoCot.getNegocioPaqueteId());
		SeccionDTO seccionDTO = negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO();
		
		if (inciso != null && seccionDTO.getIdToSeccion().longValue() == inciso.getSeccionCotizacion().getId().getIdToSeccion().longValue()){				
			for(CoberturaCotizacionDTO coberturaDTO : coberturaCotizacionList){						
				coberturaDTO.getId().setNumeroInciso(inciso.getId().getNumeroInciso());
			}
			negocioPaqueteIdGuardado = inciso.getIncisoAutoCot().getNegocioPaqueteId();
			inciso.setIncisoAutoCot(incisoAutoCot);
			inciso = entidadService.save(inciso);
			if(!(incisoAutoCot.getNegocioPaqueteId().intValue() == negocioPaqueteIdGuardado.intValue())){					
				this.cambiarPaquete(inciso.getId().getIdToCotizacion(), inciso.getId().getNumeroInciso(), incisoAutoCot.getNegocioPaqueteId(), coberturaCotizacionList);					
			}else if(isAgente){
				this.cambiarPaqueteAgente(inciso.getId().getIdToCotizacion(), inciso.getId().getNumeroInciso(), 
						incisoAutoCot.getNegocioPaqueteId(), coberturaCotizacionList, isServicioPublico);
			}else{
				if(coberturaCotizacionList != null){
					updateCoberturas(coberturaCotizacionList);					
				}					
				//inciso.getSeccionCotizacion().setCoberturaCotizacionLista(coberturaCotizacionList);
				//entidadService.save(inciso);
				//guardar(inciso);	
//				NegocioPaqueteSeccion negocioPaquete = entidadService.findById(NegocioPaqueteSeccion.class, incisoAutoCot.getNegocioPaqueteId());
//				incisoAutoCot.setPaquete(negocioPaquete.getPaquete());
//				inciso = guardar(getIncisoCotizacionDTO(
//						idToCotizacion, incisoAutoCot, incisoCotizacionDTO,
//						coberturaCotizacionList));
			}
		}else{
			if(inciso != null && seccionDTO.getIdToSeccion().longValue() != inciso.getSeccionCotizacion().getId().getIdToSeccion().longValue()){									
				for(CoberturaCotizacionDTO coberturaDTO : coberturaCotizacionList){			
					coberturaDTO.getId().setIdToSeccion(seccionDTO.getIdToSeccion());
					//ValidaDeducible no negativo
					Double cero = new Double(0);
					if(coberturaDTO.getValorDeducible() != null && coberturaDTO.getValorDeducible().compareTo(cero) < 0){
						coberturaDTO.setValorDeducible(cero);
					}
					if(coberturaDTO.getPorcentajeDeducible() != null && coberturaDTO.getPorcentajeDeducible().compareTo(cero) < 0){
						coberturaDTO.setPorcentajeDeducible(cero);
					}
				}	
				//eliminarInciso(inciso);
			}
			NegocioPaqueteSeccion negocioPaquete = entidadService.findById(NegocioPaqueteSeccion.class, incisoAutoCot.getNegocioPaqueteId());
			incisoAutoCot.setPaquete(negocioPaquete.getPaquete());
			inciso = guardar(getIncisoCotizacionDTO(
					idToCotizacion, incisoAutoCot, incisoCotizacionDTO,
					coberturaCotizacionList, valorPrimaNeta, valorSumaAsegurada, 
					claveObligatoriedad, claveContrato));
		}
		return inciso;
	}

	/**
	 * Setea los datos de un inciso para que se pueda guardar
	 * 
	 * @param IncisoAutoCot
	 * @param idToCotizacion
	 * @author martin
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public IncisoCotizacionDTO prepareGuardarInciso(BigDecimal idToCotizacion,
			IncisoAutoCot incisoAutoCot,
			IncisoCotizacionDTO incisoCotizacionDTO,
			List<CoberturaCotizacionDTO> coberturaCotizacionList) {
		return prepareGuardarInciso(idToCotizacion, incisoAutoCot, incisoCotizacionDTO, coberturaCotizacionList,
				null, null, null, null);
	}
	
	@SuppressWarnings("unused")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void eliminarInciso(IncisoCotizacionDTO inciso){
		entidadService.remove(inciso);
	}
	
	@Override
	public List<CoberturaCotizacionDTO> obtenerCoberturasParaLaConfiguracion(BigDecimal idToCotizacion, BigDecimal idInciso, Long idToNegPaqueteSeccion){
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);	
		IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, new IncisoCotizacionId(idToCotizacion, idInciso));
		SeccionCotizacionDTO seccionCotizacion = inciso.getSeccionCotizacion();
		Long negocioId = cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio(); 
		BigDecimal productoId = cotizacion.getSolicitudDTO().getProductoDTO().getIdToProducto();		
		BigDecimal tipoDePolizaId = cotizacion.getTipoPolizaDTO().getIdToTipoPoliza();				
		BigDecimal lineaId = seccionCotizacion.getSeccionDTO().getIdToSeccion();
		Short idMoneda = cotizacion.getIdMoneda().shortValue();		
		NegocioPaqueteSeccion negocioPaquete = entidadService.findById(NegocioPaqueteSeccion.class, idToNegPaqueteSeccion);		
		List<CoberturaCotizacionDTO> nuevasCoberturas = coberturaService
		.getCoberturas(negocioId, productoId, tipoDePolizaId, lineaId,
				negocioPaquete.getPaquete().getId(), cotizacion.getIdToCotizacion(), idMoneda);			
		for(CoberturaCotizacionDTO cobertura : nuevasCoberturas){
			cobertura.getId().setNumeroInciso(inciso.getId().getNumeroInciso());
			cobertura.setSeccionCotizacionDTO(inciso.getSeccionCotizacion());
			cobertura.setValorPrimaNeta(cobertura.getValorPrimaNeta()==null?0D:cobertura.getValorPrimaNeta());				
			cobertura.setValorSumaAsegurada(cobertura.getValorSumaAsegurada()==null?0D:cobertura.getValorSumaAsegurada());
			cobertura.setValorCoaseguro(cobertura.getValorCoaseguro()==null?0D:cobertura.getValorCoaseguro());
			cobertura.setValorDeducible(cobertura.getValorDeducible()==null?0D:cobertura.getValorDeducible());
			cobertura.setValorCuota(cobertura.getValorCuota()==null?0D:cobertura.getValorCuota());
			cobertura.setValorCuotaOriginal(cobertura.getValorCuotaOriginal()==null?0D:cobertura.getValorCuotaOriginal());
			cobertura.setValorCuotaOriginal(cobertura.getValorCuotaOriginal()==null?0D:cobertura.getValorCuotaOriginal());
			cobertura.setPorcentajeCoaseguro(cobertura.getPorcentajeCoaseguro()==null?0D:cobertura.getPorcentajeCoaseguro());
			cobertura.setPorcentajeDeducible(cobertura.getPorcentajeDeducible()==null?0D:cobertura.getPorcentajeDeducible());
			if (cobertura.getClaveAutCoaseguro().intValue() != 0){
				cobertura.setClaveAutCoaseguro((short)1);
			}else{
				cobertura.setClaveAutCoaseguro((short)0);
			}
			if (cobertura.getClaveAutCoaseguro().intValue() != 0){
				cobertura.setClaveAutDeducible((short)1);
			}else{
				cobertura.setClaveAutDeducible((short)0);	
			}
			//cobertura.setRiesgoCotizacionLista(incisoService.obtenerRiesgos)
		}		
		return nuevasCoberturas;
	}
	
	public IncisoCotizacionDTO update(IncisoCotizacionDTO incisoCotizacionDTO){
		incisoCotizacionDTO.getIncisoAutoCot().setAsociadaCotizacion(1);
		Long idNegocioPaqueteSeccion = incisoCotizacionDTO.getIncisoAutoCot().getNegocioPaqueteId() == null ? 
				0L : incisoCotizacionDTO.getIncisoAutoCot().getNegocioPaqueteId();
		NegocioPaqueteSeccion negocioPaqueteSeccion = negocioPaqueteSeccionDao.findById(idNegocioPaqueteSeccion);
		if(negocioPaqueteSeccion == null || negocioPaqueteSeccion.getAplicaPctDescuentoEdo() == 0) {
			incisoCotizacionDTO.getIncisoAutoCot().setPctDescuentoEstado(0.0);
		} 
		return incisoCotizacionFacadeLocal.update(incisoCotizacionDTO);
	}

	public IncisoCotizacionDTO copiarInciso(IncisoCotizacionDTO incisoBase,
			BigDecimal idCotizacionDestino) {
		final Map<String, Object> parametros = new LinkedHashMap<String, Object>();
		final BeanUtilsBean utilsBean = new BeanUtilsBean();
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class,idCotizacionDestino);
		incisoBase = entidadService.findById(IncisoCotizacionDTO.class, incisoBase.getId());
		IncisoCotizacionDTO incisoNuevo = new IncisoCotizacionDTO();
		incisoNuevo.setCotizacionDTO(cotizacion);
		incisoNuevo.setValorPrimaNeta(incisoBase.getValorPrimaNeta());
		incisoNuevo.setClaveAutInspeccion(incisoBase.getClaveAutInspeccion());
		incisoNuevo.setClaveEstatusInspeccion(incisoBase
				.getClaveEstatusInspeccion());
		incisoNuevo.setClaveTipoOrigenInspeccion(incisoBase
				.getClaveTipoOrigenInspeccion());
		incisoNuevo.setClaveMensajeInspeccion(incisoBase
				.getClaveMensajeInspeccion());
		
		incisoNuevo.setIdClienteCob(incisoBase.getIdClienteCob());
		incisoNuevo.setIdMedioPago(incisoBase.getIdMedioPago());
		incisoNuevo.setIdConductoCobroCliente(incisoBase.getIdConductoCobroCliente());

		// Aumenta en 1 el numero de inciso
		IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
		incisoCotizacionId.setIdToCotizacion(cotizacion.getIdToCotizacion());
		incisoCotizacionId.setNumeroInciso(incisoCotizacionFacadeLocal.maxIncisos(
				cotizacion.getIdToCotizacion()).add(BigDecimal.ONE));
		incisoNuevo.setId(incisoCotizacionId);
		// Aumenta en 1 el numero de secuencia
		Long maxSecuencia = new Long("1");
		maxSecuencia = maxSecuencia
				+ incisoCotizacionFacadeLocal.maxSecuencia(
						cotizacion.getIdToCotizacion());
		incisoNuevo.setNumeroSecuencia(maxSecuencia);

		incisoNuevo
				.setSeccionCotizacionList(new ArrayList<SeccionCotizacionDTO>());
		copiarEstructura(incisoNuevo, incisoBase);
		cotizacion.getIncisoCotizacionDTOs().add(incisoNuevo);
		entidadService.save(cotizacion);
			
		try {
			colonarDatosdeRiesgo(parametros, incisoNuevo, incisoBase, utilsBean);
			clonarDescuentoCoberturaCot(parametros, incisoNuevo, incisoBase, utilsBean);
			clonarRecargoCobertura(parametros, incisoNuevo, incisoBase, utilsBean);
		} catch (IllegalAccessException e) {
			LOG.error(e.getMessage(), e);
		} catch (InvocationTargetException e) {
			LOG.error(e.getMessage(), e);
		}
		return incisoNuevo;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public IncisoCotizacionDTO copiarInciso(IncisoCotizacionDTO incisoBase,
			BigDecimal idCotizacionDestino, Long numero) {
		
		incisoCotizacionFacadeLocal.copiarInciso(idCotizacionDestino, incisoBase.getId().getNumeroInciso(), new BigDecimal(numero), null);
		return new IncisoCotizacionDTO();
	}

	private void colonarDatosdeRiesgo(Map<String, Object> parametros,
			IncisoCotizacionDTO incisoNuevo, IncisoCotizacionDTO incisoBase,
			BeanUtilsBean utilsBean) throws IllegalAccessException,
			InvocationTargetException {
		parametros.put("idToCotizacion", incisoBase.getId().getIdToCotizacion());
		parametros.put("numeroInciso",incisoBase.getId().getNumeroInciso());
		final BigDecimal numeroIncisoNuevo = incisoNuevo.getId().getNumeroInciso();
		final BigDecimal idToCotizacion = incisoNuevo.getId().getIdToCotizacion();
		List<DatoIncisoCotAuto> datoIncisoCotAutoBaseList = entidadService.findByProperties(DatoIncisoCotAuto.class, parametros);
		if(datoIncisoCotAutoBaseList != null && !datoIncisoCotAutoBaseList.isEmpty()){
			for(DatoIncisoCotAuto datoIncisoCotAutoBase : datoIncisoCotAutoBaseList){
				DatoIncisoCotAuto datoIncisoCotAuto = new DatoIncisoCotAuto();
				utilsBean.copyProperties(datoIncisoCotAuto, datoIncisoCotAutoBase);
				datoIncisoCotAuto.setId(null);
				datoIncisoCotAuto.setIdToCotizacion(idToCotizacion);
				datoIncisoCotAuto.setNumeroInciso(numeroIncisoNuevo);
				entidadService.save(datoIncisoCotAuto);
			}
		}
	}
	
	/**
	 * Clona los descuentos cobertura de un 
	 * inciso
	 * @param parametros
	 * @param incisoNuevo
	 * @param incisoBase
	 * @param utilsBean
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @author martin
	 */
	private void clonarDescuentoCoberturaCot(Map<String, Object> parametros,
			IncisoCotizacionDTO incisoNuevo, IncisoCotizacionDTO incisoBase,
			BeanUtilsBean utilsBean) throws IllegalAccessException,
			InvocationTargetException {
				parametros.clear();
				parametros.put("id.idToCotizacion", incisoBase.getId().getIdToCotizacion());
				parametros.put("id.numeroInciso", incisoBase.getId().getNumeroInciso());
				final List<DescuentoCoberturaCot> descuentoCoberturaCots = entidadService.findByProperties(DescuentoCoberturaCot.class, parametros);
				final Long numeroIncisoNuevo = incisoNuevo.getId().getNumeroInciso().longValue();
				final Long idToCotizacion = incisoNuevo.getId().getIdToCotizacion().longValue();
				if (descuentoCoberturaCots != null){
					for(DescuentoCoberturaCot coberturaCotBase : descuentoCoberturaCots) {
						DescuentoCoberturaCot descuentoCoberturaCot = new DescuentoCoberturaCot();
						DescuentoCoberturaCotId descuentoCoberturaCotId = new DescuentoCoberturaCotId();
						descuentoCoberturaCot.setId(descuentoCoberturaCotId);
						descuentoCoberturaCot.getId().setIdToCobertura(coberturaCotBase.getId().getIdToCobertura());
						descuentoCoberturaCot.getId().setIdToDescuentoVario(coberturaCotBase.getId().getIdToDescuentoVario());
						descuentoCoberturaCot.getId().setIdToSeccion(coberturaCotBase.getId().getIdToSeccion());
						descuentoCoberturaCot.getId().setNumeroInciso(numeroIncisoNuevo);
						descuentoCoberturaCot.getId().setIdToCotizacion(idToCotizacion);
						descuentoCoberturaCot.setClaveAutorizacion(coberturaCotBase.getClaveAutorizacion());
						descuentoCoberturaCot.setClaveComercialTecnico(coberturaCotBase.getClaveComercialTecnico());
						descuentoCoberturaCot.setClaveNivel(coberturaCotBase.getClaveNivel());
						descuentoCoberturaCot.setClaveContrato(coberturaCotBase.getClaveContrato());
						descuentoCoberturaCot.setClaveObligatoriedad(coberturaCotBase.getClaveObligatoriedad());
						descuentoCoberturaCot.setCoberturaDTO(coberturaCotBase.getCoberturaDTO());
						descuentoCoberturaCot.setCodigoUsuarioAutorizacion(coberturaCotBase.getCodigoUsuarioAutorizacion());
						descuentoCoberturaCot.setDescuentoDTO(coberturaCotBase.getDescuentoDTO());
						descuentoCoberturaCot.setFechaAutorizacion(coberturaCotBase.getFechaAutorizacion());
						descuentoCoberturaCot.setFechaSolicitudAutorizacion(coberturaCotBase.getFechaSolicitudAutorizacion());
						descuentoCoberturaCot.setValorDescuento(coberturaCotBase.getValorDescuento());
						entidadService.save(descuentoCoberturaCot);
					}
				}
	}
	
	/**
	 * Clona los recargos cobertura de un inciso
	 * @param parametros
	 * @param incisoNuevo
	 * @param incisoBase
	 * @param utilsBean
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @author martin
	 */
	private void clonarRecargoCobertura(Map<String, Object> parametros,
			IncisoCotizacionDTO incisoNuevo, IncisoCotizacionDTO incisoBase,
			BeanUtilsBean utilsBean) throws IllegalAccessException, InvocationTargetException{
		parametros.clear();
		parametros.put("id.idToCotizacion", incisoBase.getId().getIdToCotizacion());
		parametros.put("id.numeroInciso", incisoBase.getId().getNumeroInciso());
		final Long numeroIncisoNuevo = incisoNuevo.getId().getNumeroInciso().longValue();
		final Long idToCotizacion = incisoNuevo.getId().getIdToCotizacion().longValue();
		final List<RecargoCoberturaCot> recargoCoberturaCots = entidadService.findByProperties(RecargoCoberturaCot.class, parametros);
		if (recargoCoberturaCots != null) {
			for(RecargoCoberturaCot recargoBase : recargoCoberturaCots) {
				RecargoCoberturaCot recargoCoberturaCotNuevo = new RecargoCoberturaCot();
				RecargoCoberturaCotId recargoCoberturaCotId = new RecargoCoberturaCotId();
				recargoCoberturaCotNuevo.setId(recargoCoberturaCotId);
				recargoCoberturaCotId.setIdToCobertura(recargoBase.getId().getIdToCobertura());
				recargoCoberturaCotId.setIdToCotizacion(idToCotizacion);
				recargoCoberturaCotId.setIdToRecargoVario(recargoBase.getId().getIdToRecargoVario());
				recargoCoberturaCotId.setIdToSeccion(recargoBase.getId().getIdToSeccion());
				recargoCoberturaCotId.setNumeroInciso(numeroIncisoNuevo);
				recargoCoberturaCotNuevo.setRecargoVarioDTO(recargoBase.getRecargoVarioDTO());
				recargoCoberturaCotNuevo.setCoberturaDTO(recargoBase.getCoberturaDTO());
				recargoCoberturaCotNuevo.setClaveAutorizacion(recargoBase.getClaveAutorizacion());
				recargoCoberturaCotNuevo.setCodigoUsuarioAutorizacion(recargoBase.getCodigoUsuarioAutorizacion());
				recargoCoberturaCotNuevo.setValorRecargo(recargoBase.getValorRecargo());
				recargoCoberturaCotNuevo.setClaveObligatoriedad(recargoBase.getClaveObligatoriedad());
				recargoCoberturaCotNuevo.setClaveContrato(recargoBase.getClaveContrato());
				recargoCoberturaCotNuevo.setClaveComercialTecnico(recargoBase.getClaveComercialTecnico());
				recargoCoberturaCotNuevo.setClaveNivel(recargoBase.getClaveNivel());
				recargoCoberturaCotNuevo.setFechaSolicitudAutorizacion(recargoBase.getFechaSolicitudAutorizacion());
				recargoCoberturaCotNuevo.setFechaAutorizacion(recargoBase.getFechaAutorizacion());
				entidadService.save(recargoCoberturaCotNuevo);
			}
		}
	}
	
	@Override
	public List<IncisoCotizacionDTO> getIncisos(BigDecimal IdToCotizacion) {
		List<IncisoCotizacionDTO> incisos = new LinkedList<IncisoCotizacionDTO>();
		incisos = incisoCotizacionFacadeLocal
				.findByProperty("id.idToCotizacion",
						IdToCotizacion);

		for (IncisoCotizacionDTO item : incisos) {
			List<SeccionCotizacionDTO> secciones = seccionCotizacionFacadeRemote
					.listarPorIncisoCotizacionId(item.getId());
			item.setSeccionCotizacionList(secciones);
		}
		return incisos;
	}

	/**
	 * Acualiza los numeros de secuencia de todos los incisos de una
	 * cotizacion
	 * 
	 * @author maritn
	 */
	@Override
	public void actualizaNumeroSecuencia(BigDecimal idToCotizacion,
			Long numeroSecuencia) {
		incisoCotizacionFacadeLocal.actualizaNumeroSecuencia(idToCotizacion, numeroSecuencia);

	}

	/**
	 * Borra un inciso
	 * @return el numero de secuencia del inciso eliminado
	 *  @author martin
	 */
	@Override
	public Long borrarInciso(IncisoCotizacionDTO incisoCotizacionDTO) {
		incisoCotizacionDTO = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionDTO.getId());
		// Borrar el inciso
		try {			
			calculoService.eliminarDescuentos(incisoCotizacionDTO);
			calculoService.eliminarRecargos(incisoCotizacionDTO);
			eliminarDatosRiesgo(incisoCotizacionDTO);
			entidadService.remove(incisoCotizacionDTO);
		}catch (RuntimeException exception) {
			LOG.error(exception.getMessage(), exception);
		}
		return incisoCotizacionDTO.getNumeroSecuencia();
	}
	
	@Override
	public Long borrarInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso){
		IncisoCotizacionDTO inciso = entidadService.findById(
				IncisoCotizacionDTO.class, new IncisoCotizacionId(idToCotizacion, numeroInciso));
		return borrarInciso(inciso);
	}
		
	@Override
	public void eliminarDatosRiesgo(IncisoCotizacionDTO inciso){
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idToCotizacion", inciso.getId().getIdToCotizacion());
		parameters.put("numeroInciso", inciso.getId().getNumeroInciso());		
		List<DatoIncisoCotAuto> riesgos = entidadService.findByProperties(DatoIncisoCotAuto.class, parameters);		
		for(DatoIncisoCotAuto riesgo : riesgos){
			entidadService.remove(riesgo);
		} 
	}
	
	/**
	 * Lista todos los Incisos correspondientes a una cotizacion con filtros
	 * 
	 * @param idToCotizacion
	 *            Opcional
	 * @param nuermoInciso
	 *            opcional
	 * @param numeroSecuencia
	 *            Opcional
	 * @param idToSeccion
	 *            Opcional
	 * @param paqueteId
	 *            Opcional
	 * @return List<IncisoCotizacionDTO>
	 * @author martin
	 */
	@Override
	public List<IncisoCotizacionDTO> listarIncisosConFiltro(IncisoCotizacionDTO filtro) {
		List<IncisoCotizacionDTO> lisIncisoCotizacionDTOs = null;
		try{
			if (filtro.getIdToSeccion() != null) {
				NegocioSeccion negSeccion = entidadService.findById(
						NegocioSeccion.class, filtro.getIdToSeccion());
				// Set Seccion
				if (negSeccion != null) {
					filtro.setIdToSeccion(negSeccion.getSeccionDTO()
							.getIdToSeccion());
				}
			}		
			if (filtro.getIncisoAutoCot().getNegocioPaqueteId() != null) {
				NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService
						.findById(NegocioPaqueteSeccion.class, filtro
								.getIncisoAutoCot().getNegocioPaqueteId());
				// Set Negocio paquete
				if(negocioPaqueteSeccion != null) {
					filtro.getIncisoAutoCot().setNegocioPaqueteId(
							negocioPaqueteSeccion.getIdToNegPaqueteSeccion());
				}				
			} 
			lisIncisoCotizacionDTOs = incisoCotizacionFacadeLocal.listarIncisosConFiltro(filtro);
			for (IncisoCotizacionDTO incisoCotizacionDTO : lisIncisoCotizacionDTOs) {
				incisoCotizacionDTO.setValorPrimaTotal(getValorPrimaTotal(incisoCotizacionDTO));
				//Obtener medio de pago por inciso
				findMedioPago(incisoCotizacionDTO);
			}	
		}catch(Exception ex){
			LOG.error(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
		return lisIncisoCotizacionDTOs;
	}

	@Override
	public Long listarIncisosConFiltroCount(IncisoCotizacionDTO filtro) {
		if (filtro.getIdToSeccion() != null) {
			NegocioSeccion negSeccion = entidadService.findById(
					NegocioSeccion.class, filtro.getIdToSeccion());
			// Set Seccion
			filtro.setIdToSeccion(negSeccion.getSeccionDTO().getIdToSeccion());
		}
		
		if (filtro.getIncisoAutoCot().getNegocioPaqueteId() != null) {
			NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService
					.findById(NegocioPaqueteSeccion.class, filtro
							.getIncisoAutoCot().getNegocioPaqueteId());
			// Set Negocio paquete
			filtro.getIncisoAutoCot().setNegocioPaqueteId(
					negocioPaqueteSeccion.getIdToNegPaqueteSeccion());
			return incisoCotizacionFacadeLocal
					.listarIncisosConFiltroCount(filtro);
		} else {
			return incisoCotizacionFacadeLocal
					.listarIncisosConFiltroCount(filtro);
		}
	}
	
	public Map<String, String> borrarIncisoAutoCot(IncisoAutoCot incisoAutoCot) {
		Map<String, String> mensaje = new LinkedHashMap<String, String>();
		try {
			incisoAutoCot = entidadService.getReference(IncisoAutoCot.class,
					incisoAutoCot.getId());
			entidadService.remove(incisoAutoCot);
			// Exito
			mensaje.put("tipoMensaje", "30");
			mensaje.put("mensaje", "El inciso se eliminar; exitosamente.");
		} catch (RuntimeException e) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, e);
			// Error
			mensaje.put("tipoMensaje", "10");
			mensaje.put("mensaje", "Error al borrar Inciso");
			throw e;
		}
		return mensaje;
	}
	
	@Override
	public void borrarIncisoAutoCotNoAsignados(BigDecimal idToCotizacion) {
		List<IncisoCotizacionDTO> lista = findNoAsignadosByCotizacionId(idToCotizacion);
		try {

			for(IncisoCotizacionDTO item : lista){
				if(item.getIncisoAutoCot() == null || item.getIncisoAutoCot().getAsociadaCotizacion() == null || 
						item.getIncisoAutoCot().getAsociadaCotizacion() == 0){
					borrarInciso(item);
				}
			}
			
		} catch (RuntimeException e) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, e);
			throw e;
		}
	}

	@Override
	public List<IncisoCotizacionDTO> findByCotizacionId(
			BigDecimal idToCoTizacion) {
		return incisoCotizacionFacadeLocal.findByCotizacionId(idToCoTizacion);
	}


	@Override
	public void complementarDatosInciso(IncisoCotizacionDTO inciso) {
		entidadService.save(inciso);
	}

	@Override
	public void guardarIncisoAutoCot(IncisoAutoCot incisoAutoCot) {
		entidadService.save(incisoAutoCot);
	}

	@Override
	public IncisoAutoCot obtenerIncisoAutoCotPorId(Long id) {
		IncisoAutoCot autoCot = new IncisoAutoCot();
		autoCot = entidadService.findById(IncisoAutoCot.class, id);
		if (autoCot != null) {
			return autoCot;
		}
		return autoCot;
	}

	@Override
	public ResumenCostosDTO obtenerResumenInciso(BigDecimal idToCotizacion, IncisoAutoCot incisoAutoCot, IncisoCotizacionDTO incisoCotizacionDTO, List<CoberturaCotizacionDTO> coberturaCotizacionList) {
		ResumenCostosDTO resumenCostosDTO = new ResumenCostosDTO();
		IncisoCotizacionDTO incisoCotizacionDTOTemp = new IncisoCotizacionDTO();
		incisoCotizacionDTOTemp = getIncisoCotizacionDTO(idToCotizacion, incisoAutoCot, incisoCotizacionDTO, coberturaCotizacionList, null, null, null, null);
		resumenCostosDTO = calculoService.obtenerResumen(incisoCotizacionDTOTemp);
		return resumenCostosDTO;
	}

	private IncisoCotizacionDTO getIncisoCotizacionDTO(
			BigDecimal idToCotizacion, IncisoAutoCot incisoAutoCot,
			IncisoCotizacionDTO incisoTemp,
			List<CoberturaCotizacionDTO> coberturaCotizacionList,
			Double valorPrimaNeta, Double valorSumaAsegurada, 
			Short claveObligatoriedad, Short claveContrato) {
		// Obtener cotizacion
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class,idToCotizacion);		
		// obtener seccionDTO
		NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class,incisoAutoCot.getNegocioPaqueteId());
		SeccionDTO seccionDTO = negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO();
		
		// Nuevo inciso
		IncisoCotizacionDTO inciso = new IncisoCotizacionDTO();
		inciso.setCotizacionDTO(cotizacion);
		inciso.setId(new IncisoCotizacionId());
		inciso.getId().setIdToCotizacion(idToCotizacion);	
		if(incisoTemp.getId() != null && incisoTemp.getId().getNumeroInciso() != null){
			inciso.getId().setNumeroInciso(incisoTemp.getId().getNumeroInciso());
		}
		inciso.setNumeroSecuencia(incisoTemp.getNumeroSecuencia());		
		inciso.setValorPrimaNeta(incisoTemp.getValorPrimaNeta());
		inciso.setEmailContacto(incisoTemp.getEmailContacto());
		if(incisoTemp.getIdMedioPago()!=null && incisoTemp.getIdMedioPago().compareTo(BigDecimal.ZERO)>0){
			inciso.setIdMedioPago(incisoTemp.getIdMedioPago());
		}		
		
		// Obtener seccionCotizacion		
		SeccionCotizacionDTO seccion = new SeccionCotizacionDTO();
		seccion.setClaveContrato(claveContrato);
		seccion.setClaveObligatoriedad(claveObligatoriedad);
		seccion.setValorPrimaNeta(valorPrimaNeta);
		seccion.setValorSumaAsegurada(valorSumaAsegurada);
		seccion.setId(new SeccionCotizacionDTOId(idToCotizacion, inciso.getId()
				.getNumeroInciso(), seccionDTO.getIdToSeccion()));
		// Obtener List Coberturas
		seccion.setCoberturaCotizacionLista(coberturaCotizacionList);
		seccion.setSeccionDTO(seccionDTO);
		seccion.setIncisoCotizacionDTO(inciso);
		inciso.setSeccionCotizacionList(new ArrayList<SeccionCotizacionDTO>());
		inciso.getSeccionCotizacionList().add(seccion);
				
		TipoUsoVehiculoDTO tipoUsoVehiculo = entidadService.findById(TipoUsoVehiculoDTO.class, 
				BigDecimal.valueOf(incisoAutoCot.getTipoUsoId()));
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id.idToSeccion", seccionDTO.getIdToSeccion());
		params.put("id.idTcTipoVehiculo", tipoUsoVehiculo.getIdTcTipoVehiculo());

		BigDecimal idTcTipoServicioVehiculo = null;
		try {
			if (incisoAutoCot.getTipoServicioId()==null){
				NegocioTipoServicio negocioTipoServicio = negocioTipoServicioDao.getDefaultByNegSeccionEstiloVehiculo(negocioPaqueteSeccion.getNegocioSeccion().getIdToNegSeccion(), tipoUsoVehiculo.getIdTcTipoVehiculo());
				if(negocioTipoServicio != null){
					idTcTipoServicioVehiculo = negocioTipoServicio.getTipoServicioVehiculoDTO().getIdTcTipoServicioVehiculo();
					incisoAutoCot.setTipoServicioId(idTcTipoServicioVehiculo);
				}
			}
		} catch(Exception e) {

		}
		
		if(idTcTipoServicioVehiculo == null && incisoAutoCot.getTipoServicioId()==null){
			List<ServVehiculoLinNegTipoVeh> negTipoVeh = entidadService.findByProperties(ServVehiculoLinNegTipoVeh.class,params);
			if(!negTipoVeh.isEmpty()){
				incisoAutoCot.setTipoServicioId(negTipoVeh.get(0).getTipoServicioVehiculoDTO().getIdTcTipoServicioVehiculo());
			}
		}
		
		// Guardar informacion de pago
		inciso.setIdClienteCob(incisoTemp.getIdClienteCob());
		inciso.setIdConductoCobroCliente(incisoTemp.getIdConductoCobroCliente());
		inciso.setIdMedioPago(incisoTemp.getIdMedioPago());
		inciso.setConductoCobro(incisoTemp.getConductoCobro());
		inciso.setInstitucionBancaria(incisoTemp.getInstitucionBancaria());
		inciso.setTipoTarjeta(incisoTemp.getTipoTarjeta());
		inciso.setNumeroTarjetaClave(incisoTemp.getNumeroTarjetaClave());
		inciso.setCodigoSeguridad(incisoTemp.getCodigoSeguridad());
		inciso.setFechaVencimiento(incisoTemp.getFechaVencimiento());
		
		// Obtener incisoAutoCot
		incisoAutoCot.setIncisoCotizacionDTO(inciso);
		inciso.setIncisoAutoCot(incisoAutoCot);	
		return inciso;
	}

	@Override
	public void guardarDatosAdicionalesPaquete(Map<String, String> datosRiesgo,
			BigDecimal negocioSeccionId, BigDecimal cotizacionId,
			BigDecimal numeroInciso) {
		if (datosRiesgo != null) {
			NegocioSeccion negocioSeccion = entidadService.findById(
					NegocioSeccion.class, negocioSeccionId);

			for (Entry<String, String> entry : datosRiesgo.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();
				ConfiguracionDatoIncisoId id = configuracionDatoIncisoService
						.getConfiguracionDatoIncisoId(key);

				DatoIncisoCotAuto datoIncisoCotAuto = datoIncisoCotAutoService
						.getDatoIncisoByConfiguracion(
								cotizacionId,
								negocioSeccion.getSeccionDTO().getIdToSeccion(),
								numeroInciso, id.getIdTcRamo(),
								id.getIdTcSubRamo(), id.getIdToCobertura(),
								id.getClaveDetalle(), id.getIdDato());

				if (datoIncisoCotAuto != null) {
					datoIncisoCotAuto.setValor(value);
					entidadService.save(datoIncisoCotAuto);
				} else {
					datoIncisoCotAuto = new DatoIncisoCotAuto();
					datoIncisoCotAuto.setClaveDetalle(new BigDecimal(id
							.getClaveDetalle()));
					datoIncisoCotAuto.setIdDato(id.getIdDato());
					datoIncisoCotAuto.setIdTcRamo(id.getIdTcRamo());
					datoIncisoCotAuto.setIdTcSubRamo(id.getIdTcSubRamo());
					datoIncisoCotAuto.setIdToCobertura(id.getIdToCobertura());
					datoIncisoCotAuto.setIdToCotizacion(cotizacionId);
					datoIncisoCotAuto.setIdToSeccion(negocioSeccion
							.getSeccionDTO().getIdToSeccion());
					datoIncisoCotAuto.setNumeroInciso(numeroInciso);
					datoIncisoCotAuto.setValor(value);
					entidadService.save(datoIncisoCotAuto);
				}
			}
		}
	}

	@Override
	public Long maxSecuencia(BigDecimal idToCotizacion) {
		return incisoCotizacionFacadeLocal.maxSecuencia(idToCotizacion);
	}
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	@Override
	public Boolean infoConductorEsRequerido(IncisoCotizacionDTO incisoCotizacionDTO) {
		ParametroGeneralId idParametro=new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_COBERTURA_RC_USA,new BigDecimal(920010));
		ParametroGeneralDTO parametro=parametroGeneralFacade.findById(idParametro);
		String[] valoresParametros=null;
		if(parametro !=null && parametro.getValor()!=null){
			 valoresParametros=parametro.getValor().split(UtileriasWeb.SEPARADOR_COMA);
			
		}
		incisoCotizacionDTO = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionDTO.getId());
		Boolean requerido = false;
		//Validar si la cobertura esta presente y esta contratada en el inciso, en caso de cumplirse lo anterior (requerido = true)
		List<SeccionCotizacionDTO> lista = incisoCotizacionDTO.getSeccionCotizacionList();
		for(SeccionCotizacionDTO item : lista){
			CoberturaCotizacionId id = new CoberturaCotizacionId();
			id.setIdToCotizacion(item.getId().getIdToCotizacion());
			id.setIdToSeccion(item.getId().getIdToSeccion());
			id.setNumeroInciso(item.getId().getNumeroInciso());
			CoberturaCotizacionDTO cobertura = item.getCoberturaDeLista(id);
			
			for( int i = 0; valoresParametros!=null && i <= valoresParametros.length - 1; i++)
			{
				if( StringUtils.isNumeric(valoresParametros[i]) ){
					id.setIdToCobertura(new BigDecimal(valoresParametros[i]));
					cobertura = item.getCoberturaDeLista(id);
					if(cobertura != null){
						Short claveContrato = 1;
						if(cobertura.getClaveContrato().equals(claveContrato)){
							requerido = true;
							break;
						}
					}
				}
			}
		}
		return requerido;
	}
	
	@Override
	public Boolean observacionesEsRequerido(IncisoCotizacionDTO incisoCotizacionDTO) {
		incisoCotizacionDTO = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionDTO.getId());
		Boolean requerido = false;
		BigDecimal idCoberturaDanoOcasionadosPorLaCarga = new BigDecimal(2840);
		
		//Validar si la cobertura esta presente y esta contratada en el inciso, en caso de cumplirse lo anterior (requerido = true)
		List<SeccionCotizacionDTO> lista = incisoCotizacionDTO.getSeccionCotizacionList();
		for(SeccionCotizacionDTO item : lista){
			CoberturaCotizacionId id = new CoberturaCotizacionId();
			id.setIdToCobertura(idCoberturaDanoOcasionadosPorLaCarga);
			id.setIdToCotizacion(item.getId().getIdToCotizacion());
			id.setIdToSeccion(item.getId().getIdToSeccion());
			id.setNumeroInciso(item.getId().getNumeroInciso());
			CoberturaCotizacionDTO cobertura = item.getCoberturaDeLista(id);
			if(cobertura != null){
				Short claveContrato = 1;
				if(cobertura.getClaveContrato().equals(claveContrato)){
					requerido = true;
					break;
				}
			}
		}
		return requerido;
	}

	@Override
	public List<IncisoNumeroSerieDTO> numerosSerieRepetidos(BigDecimal idToCotizacion, BigDecimal numeroInciso, String numeroSerie,
			Date fechaIniVigencia, Date fechaFinVigencia) {
		List<IncisoNumeroSerieDTO> repetidos = new ArrayList<IncisoNumeroSerieDTO>();
		try{
			if(numeroInciso == null){
				numeroInciso = BigDecimal.valueOf(0);
			}
			repetidos = incisoAutoDao.numerosSerieRepetidos(idToCotizacion, numeroInciso, numeroSerie, 
					fechaIniVigencia, fechaFinVigencia);
		}catch(Exception e){			
		}
		return repetidos;
	}

	@Override
	public boolean existenNumeroSerieRepetidos(BigDecimal idToCotizacion, BigDecimal numeroInciso, String numeroSerie,
			Date fechaIniVigencia, Date fechaFinVigencia) {
		return CollectionUtils.isEmpty(numerosSerieRepetidos(idToCotizacion, numeroInciso, numeroSerie, fechaIniVigencia, fechaFinVigencia)) ? 
				false : true;
	}	
	
	@Override
	public Double getValorPrimaTotal(IncisoCotizacionDTO incisoCotizacionDTO) {
		IncisoCotizacionDTO inciso = null;
		CotizacionDTO cotizacionDTO = null;
		Double primaTotal = null;
		final BigDecimal idCotizacion = incisoCotizacionDTO.getId().getIdToCotizacion();		
		try{					
			cotizacionDTO = entidadService.findById(CotizacionDTO.class, idCotizacion);
			if (incisoCotizacionDTO.getValorPrimaTotal() == null) {
				if (cotizacionDTO != null) {
					double comisionCedida = 0.0;
					double totalPrimasNetasParcial = 0.0;
					double sumaValorPrimaNetaCoberturasPropias = 0.0;
					inciso = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionDTO.getId());
					for (CoberturaCotizacionDTO coberturaCotizacionDTO : inciso.getSeccionCotizacion().getCoberturaCotizacionLista()) {
						double comisionCedidaParcial = 0.0;
						double descuentoParcial = 0.0;
						double comisionMenosBonificacion = 0.0;
						final BigDecimal idTcSubRamo = coberturaCotizacionDTO.getIdTcSubramo();
						totalPrimasNetasParcial = coberturaCotizacionDTO.getValorPrimaNeta() - descuentoParcial;
						// Descuento global solo aplica para propias
							if (coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getCoberturaEsPropia().equals("1")) {
								sumaValorPrimaNetaCoberturasPropias = sumaValorPrimaNetaCoberturasPropias
										+ coberturaCotizacionDTO.getValorPrimaNeta();
								if (cotizacionDTO.getPorcentajeDescuentoGlobal() != null) {
									descuentoParcial = coberturaCotizacionDTO.getValorPrimaNeta()
											* (cotizacionDTO.getPorcentajeDescuentoGlobal() / 100);
								}
							}
	
						final Predicate findId = new Predicate() {
							@Override
							public boolean evaluate(Object arg0) {
								ComisionCotizacionDTO id = (ComisionCotizacionDTO) arg0;
								return (id.getId().getIdToCotizacion().equals(idCotizacion)
										&& id.getId().getIdTcSubramo().equals(idTcSubRamo) && id
										.getId().getTipoPorcentajeComision().equals((short) 1));
							}
						};
	
						final ComisionCotizacionDTO comisionCotizacionDTO = (ComisionCotizacionDTO) CollectionUtils.find(
								cotizacionDTO.getComisionCotizacionDTOs(), findId);
	
						if (comisionCotizacionDTO != null) {
							if (cotizacionDTO.getPorcentajebonifcomision() != null) {
								comisionMenosBonificacion = ((cotizacionDTO.getPorcentajebonifcomision() * comisionCotizacionDTO
										.getPorcentajeComisionDefault().doubleValue()) / 100);
							}
							comisionCedidaParcial = totalPrimasNetasParcial * (comisionMenosBonificacion / 100);
						}
						comisionCedida = comisionCedida + comisionCedidaParcial;
					}
					
					final double valorPrimaNetaMenosComision = inciso.getValorPrimaNeta() - comisionCedida;	
					final double recargo = valorPrimaNetaMenosComision * cotizacionDTO.getPorcentajePagoFraccionado() / 100;
					// VPT = VPN + Derechos + Recargos + IVA
					primaTotal = (valorPrimaNetaMenosComision + cotizacionDTO.getValorDerechosUsuario() + recargo)
							* (1 + cotizacionDTO.getPorcentajeIva() / 100);					
				}
			}
			}catch(Exception e){
				LOG.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
			return primaTotal;
	}
	
	@Override
	public Long borrarIncisoMasivo(IncisoCotizacionDTO incisoCotizacionDTO) {
		incisoCotizacionDTO = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionDTO.getId());
		// Borrar el inciso
		try {
			entidadService.remove(incisoCotizacionDTO);
		}catch (RuntimeException exception) {
			LOG.error(exception.getMessage(), exception);
		}
		return incisoCotizacionDTO.getNumeroSecuencia();
	}
	
	@Override
	public IncisoCotizacionDTO getIncisoCoberturaCotizacionesInicializadas(
			BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		//FIXME:El fetch join no me funciono. Tratar de ver como podemos optimizar este query para que se inicialicen las secciones y las coberturas.
		TypedQuery<IncisoCotizacionDTO> query = entityManager.createQuery("select model from IncisoCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion and model.id.numeroInciso = :numeroInciso", IncisoCotizacionDTO.class);
		query.setParameter("idToCotizacion", idToCotizacion);
		query.setParameter("numeroInciso", numeroInciso);
		List<IncisoCotizacionDTO> resultList = query.getResultList();
		if (resultList.size() == 1) {
			return resultList.get(0);
		}
		return null;
	}

	@Override
	public List<IncisoCotizacionDTO> findNoAsignadosByCotizacionId(BigDecimal idToCotizacion) {
		return incisoCotizacionFacadeLocal.findNoAsignadosByCotizacionId(idToCotizacion);
	}
	
	public void findMedioPago(IncisoCotizacionDTO incisoCotizacion){
		if(incisoCotizacion.getIdMedioPago() != null){
			if(incisoCotizacion.getIdMedioPago().intValue() == MedioPagoDTO.MEDIO_PAGO_AGENTE){
				incisoCotizacion.setDescripcionMedioPago("EFECTIVO");
			}else{
				MedioPagoDTO medioPago = entidadService.findById(MedioPagoDTO.class, incisoCotizacion.getIdMedioPago().intValue());
				incisoCotizacion.setDescripcionMedioPago(medioPago.getDescripcion());
			}
		}
		if(incisoCotizacion.getIdClienteCob() != null && incisoCotizacion.getIdMedioPago() != null 
				&& incisoCotizacion.getIdConductoCobroCliente() != null){
			ClienteGenericoDTO cliente = new ClienteGenericoDTO();
			cliente.setIdCliente(incisoCotizacion.getIdClienteCob());
			try{
				List<ClienteGenericoDTO> list = clienteFacadeRemote.loadMediosPagoPorCliente(cliente, "SYSTEM");
				for(ClienteGenericoDTO dto : list){
					if(dto.getIdTipoConductoCobro()!=null &&  dto.getIdConductoCobranza() != null && 
							dto.getIdTipoConductoCobro().equals(incisoCotizacion.getIdMedioPago().intValue()) && dto.getIdConductoCobranza().equals(incisoCotizacion.getIdConductoCobroCliente())){
						incisoCotizacion.setNumeroTarjetaCobranza(dto.getNumeroTarjetaCobranza());
					}
				}
				if(list == null || list.isEmpty()) {
					incisoCotizacion.setNumeroTarjetaCobranza("Agregar nuevo...");				
				}
			}catch(Exception ex){
				LOG.error(ex.getMessage(), ex);
			}
		}else{
			incisoCotizacion.setNumeroTarjetaCobranza("Agregar nuevo...");
		}
	}
	
	public CondicionEspecial copiarCondicionesEspeciales(CondicionEspecial conEspecialBase) {
		conEspecialBase = entidadService.findById(CondicionEspecial.class, conEspecialBase.getId());
		CondicionEspecial condEspecial = new CondicionEspecial();
		BeanUtils.copyProperties(conEspecialBase, condEspecial);
		return condEspecial;
	}
	
	@Override
	public List<IncisoCotizacionDTO> getIncisosRango(
			BigDecimal idToCotizacion, BigDecimal incisoInicial, BigDecimal incisoFinal) {
		String queryStr = " select model from IncisoCotizacionDTO model " +
				" where model.id.idToCotizacion = :idToCotizacion ";
				
		if(incisoInicial != null && incisoInicial != null){
			queryStr = queryStr + " and model.id.numeroInciso >= :incisoInicial " +
			" and model.id.numeroInciso <= :incisoFinal ";
		}
				
		queryStr = queryStr + " order by model.id.numeroInciso";
		TypedQuery<IncisoCotizacionDTO> query = entityManager.createQuery(queryStr, IncisoCotizacionDTO.class);
		query.setParameter("idToCotizacion", idToCotizacion);
		if(incisoInicial != null && incisoInicial != null){
			query.setParameter("incisoInicial", incisoInicial);
			query.setParameter("incisoFinal", incisoFinal);
		}
		List<IncisoCotizacionDTO> resultList = query.getResultList();
		return resultList;
	}
	
	@Override
	public void guardarConductoCobroCotizacionAInciso(CotizacionDTO cotizacionDTO) {
		incisoDao.guardarConductoCobroCotizacionAInciso(cotizacionDTO);		
	}
}
