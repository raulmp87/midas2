package mx.com.afirme.midas2.service.impl.impuestosestatales;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.impuestosestatales.RetencionImpuestosDao;
import mx.com.afirme.midas2.dao.impuestosestatales.RetencionImpuestosDao.TipoAccionConfiguracion;
import mx.com.afirme.midas2.dao.impuestosestatales.RetencionImpuestosDao.TipoEjecucionCalculo;
import mx.com.afirme.midas2.dao.impuestosestatales.RetencionImpuestosDao.TipoEstatusEjecucionCalculo;
import mx.com.afirme.midas2.domain.calculos.CalculoComisiones;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.catalogos.retencionimpuestos.RetencionCedularesView;
import mx.com.afirme.midas2.domain.catalogos.retencionimpuestos.RetencionImpuestos;
import mx.com.afirme.midas2.domain.sistema.parametro.ParametroGlobal;
import mx.com.afirme.midas2.dto.impuestosestatales.RetencionImpuestosDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.impuestosestatales.RetencionImpuestosService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.utils.MailServiceImpl;

import org.apache.log4j.Logger;

@Stateless
public class RetencionImpuestosServiceImpl implements RetencionImpuestosService {
	
	private static final String MAIL_NOTIFICACION_TITULO = "Notificacion Impuestos Cedulares";
	private static final String MAIL_NOTIFICACION_DESTINO = MailServiceImpl.EMAIL_ADMINISTRADOR_MIDAS;
	private static final String MAIL_NOTIFICACION_KEY_CUERPO = "midas.agente.retencion.impuestos.correo.cuerpo";
	
	private static final String CONFIGURACION_ACTIVA = "1";
	private static final String CONFIGURACION_INACTIVA = "0";
	
	private static final String CAMPO_FILTRO_ID = "ID";
	private static final String CAMPO_FILTRO_ESTADO = "Estado";
	private static final String CAMPO_FILTRO_PORCENTAJE = "Porcentaje";
	private static final String CAMPO_FILTRO_CONCEPTO = "Concepto";
	private static final String CAMPO_FILTRO_TIPOCOMISION= "Tipo Comision";
	private static final String CAMPO_FILTRO_PERSONALIDADJURIDICA = "Personalidad Juridica";
	private static final String CAMPO_FILTRO_FECHAINICIOVIGENCIA = "Fecha Inicio Vigencia";
	private static final String CAMPO_FILTRO_FECHAFINVIGENCIA = "Fecha Fin Vigencia";
	private static final String CAMPO_FILTRO_ESTATUS = "Estatus";
	
	private static final String QUERY_PARAM_ENPROCESO = "enProceso";
	private static final String QUERY_PARAM_ESTATUS = "estatus";
	
	private static final String KEY_PREFIJO_ERORR = "midas.retencion.impuestos.configuracion.";
	private static final String KEY_ERROR_DATO_INVALIDO = "guardado.datoinvalido";
	private static final String KEY_ERROR_CONFIGURACION_VIGENTE = "guardado.configuracionvigente";
	private static final String KEY_ERROR_GUARDADO = "guardado";
	private static final String KEY_ERROR_BLOQUEO_RETENCION = "midas.retencion.impuestos.calculo.enproceso";
	private static final String KEY_ERROR_BLOQUEO_COMISION = "midas.agente.comisiones.calculo.enproceso";
	
	private static final String DATE_FORMAT = "dd/MM/yyyy";
	
	public void guardarNuevaConfiguracion(RetencionImpuestosDTO filtro) {
		RetencionImpuestos configuracion = null;
		TipoAccionConfiguracion accion = TipoAccionConfiguracion.ALTA;
		
		if(isCalculoRetencionImpuestosEnProceso()) {
			throw new RuntimeException(Utilerias.getMensajeRecurso(sistemaContext.getArchivoRecursosBack(), KEY_ERROR_BLOQUEO_RETENCION));
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(QUERY_PARAM_ENPROCESO, Boolean.TRUE);
		long cuantos = entidadService.findByPropertiesCount(CalculoComisiones.class, params);
		
		if(cuantos > 0) {
			throw new RuntimeException(Utilerias.getMensajeRecurso(sistemaContext.getArchivoRecursosBack(), KEY_ERROR_BLOQUEO_COMISION));
		}
		
		validarCampos(filtro);
		filtro.setUsuario(usuarioService.getUsuarioActual().getNombreUsuario());
		
		if(filtro.getId() != null) {
			RetencionImpuestos tmp = entidadService.findById(RetencionImpuestos.class, filtro.getId());
			accion = TipoAccionConfiguracion.ACTIVACION;
			if(tmp.getEstatus().equals(filtro.getEstatus())) {
				accion = TipoAccionConfiguracion.ACTUALIZACION;
			}
		}
		
		boolean isVigente = filtro.getFechaFinVig() == null ? true : Utilerias.fechaEntreRango(new Date(), filtro.getFechaInicioVig(), filtro.getFechaFinVig());
		configuracion = entidadService.save(new RetencionImpuestos(filtro));
		em.flush();
		filtro.setId(configuracion.getId());
//		envioNotificacionCambioConfiguraciones(configuracion, accion);
		
		if (filtro.getEstatus().equals(CONFIGURACION_ACTIVA) && isVigente) {
			procesoCalculoRetenciones(filtro, accion);
		}
	}
	
	public void validarCampos(RetencionImpuestosDTO configuracion) {
		SimpleDateFormat formato = new SimpleDateFormat(DATE_FORMAT);
		List<RetencionImpuestos> configuracionesPorEstado;
		Date currentDate = org.apache.commons.lang.time.DateUtils.truncate(new Date(), Calendar.DATE);
		RetencionImpuestos tmp;

		if(configuracion.getId() != null) {
			tmp = entidadService.findById(RetencionImpuestos.class, configuracion.getId());
			if(tmp != null) {
				String fechaInicioFiltro = formato.format(configuracion.getFechaInicioVig());
				String fechaInicioOrigen = formato.format(tmp.getFechaInicioVig());
				if(!configuracion.getId().equals(tmp.getId())) {
					throw new RuntimeException(getMensajeErrorFiltro(KEY_ERROR_DATO_INVALIDO, CAMPO_FILTRO_ID));
				}
				if(!configuracion.getEstado().getId().equals(tmp.getEstado().getId())) {
					throw new RuntimeException(getMensajeErrorFiltro(KEY_ERROR_DATO_INVALIDO, CAMPO_FILTRO_ESTADO));
				}
				if(!configuracion.getPorcentaje().equals(tmp.getPorcentaje())) {
					throw new RuntimeException(getMensajeErrorFiltro(KEY_ERROR_DATO_INVALIDO, CAMPO_FILTRO_PORCENTAJE));
				}
				if(!configuracion.getConcepto().equals(tmp.getConcepto())) {
					throw new RuntimeException(getMensajeErrorFiltro(KEY_ERROR_DATO_INVALIDO, CAMPO_FILTRO_CONCEPTO));
				}
				if(!configuracion.getTipoComision().getId().equals(tmp.getTipoComision().getId())) {
					throw new RuntimeException(getMensajeErrorFiltro(KEY_ERROR_DATO_INVALIDO, CAMPO_FILTRO_TIPOCOMISION));
				}
				if(!configuracion.getPersonalidadJuridica().getId().equals(tmp.getPersonalidadJuridica().getId())) {
					throw new RuntimeException(getMensajeErrorFiltro(KEY_ERROR_DATO_INVALIDO, CAMPO_FILTRO_PERSONALIDADJURIDICA));
				}
				if(!fechaInicioFiltro.equals(fechaInicioOrigen)) {
					throw new RuntimeException(getMensajeErrorFiltro(KEY_ERROR_DATO_INVALIDO, CAMPO_FILTRO_FECHAINICIOVIGENCIA));
				}
				
				if(configuracion.getFechaFinVig() != null) {
					if(tmp.getFechaFinVig() != null) {
						Date fechaFinFiltroTru = org.apache.commons.lang.time.DateUtils.truncate(configuracion.getFechaFinVig(), Calendar.DATE);
						if(fechaFinFiltroTru.before(currentDate)) {
							throw new RuntimeException(getMensajeErrorFiltro(KEY_ERROR_DATO_INVALIDO, CAMPO_FILTRO_FECHAFINVIGENCIA));
						}
					}
					if(configuracion.getFechaFinVig().before(currentDate)) {
						throw new RuntimeException(getMensajeErrorFiltro(KEY_ERROR_DATO_INVALIDO, CAMPO_FILTRO_FECHAFINVIGENCIA));
					}
				}
				
				if(tmp.getEstatus().equals(CONFIGURACION_ACTIVA) && configuracion.getEstatus().equals(CONFIGURACION_INACTIVA)) {
					throw new RuntimeException(getMensajeErrorFiltro(KEY_ERROR_GUARDADO, CAMPO_FILTRO_ESTATUS));
				}
			}
		}
		
		if(configuracion.getEstatus().equals(CONFIGURACION_ACTIVA)) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("estatus", CONFIGURACION_ACTIVA);
			params.put("estado.stateId", configuracion.getEstado().getStateId());
			configuracionesPorEstado = entidadService.findByProperties(RetencionImpuestos.class, params);
			if(configuracionesPorEstado != null && !configuracionesPorEstado.isEmpty()) {
				for(RetencionImpuestos e : configuracionesPorEstado) {
					if(!e.getId().equals(configuracion.getId())) {
						if(e.getFechaFinVig() == null ||
								(!configuracion.getFechaFinVig().after(e.getFechaFinVig()) && !e.getFechaInicioVig().after(configuracion.getFechaFinVig())) ||
								(!configuracion.getFechaInicioVig().after(e.getFechaFinVig()) && !configuracion.getFechaInicioVig().before(e.getFechaInicioVig()))
							) {
							throw new RuntimeException(getMensajeErrorFiltro(KEY_ERROR_CONFIGURACION_VIGENTE, null));
						}
					}
				}
			}
		}
	}
	
	public String getMensajeErrorFiltro(String accion, String nombreFiltro) {
		String key = KEY_PREFIJO_ERORR + accion;
		return Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, key, nombreFiltro);
	}

	@Override
	public void desactivarConfiguracion(RetencionImpuestosDTO filtro) {
		RetencionImpuestos tmp;
		TipoAccionConfiguracion accion = TipoAccionConfiguracion.INACTIVACION;
		
		if(isCalculoRetencionImpuestosEnProceso()) {
			throw new RuntimeException(Utilerias.getMensajeRecurso(sistemaContext.getArchivoRecursosBack(), KEY_ERROR_BLOQUEO_RETENCION));
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(QUERY_PARAM_ENPROCESO, Boolean.TRUE);
		long cuantos = entidadService.findByPropertiesCount(CalculoComisiones.class, params);
		
		if(cuantos > 0) {
			throw new RuntimeException(Utilerias.getMensajeRecurso(sistemaContext.getArchivoRecursosBack(), KEY_ERROR_BLOQUEO_COMISION));
		}
		
		try {
			tmp = entidadService.findById(RetencionImpuestos.class, filtro.getId());
			tmp.setEstatus("0");
			tmp.setUsuario(usuarioService.getUsuarioActual().getNombreUsuario());
			tmp = entidadService.save(tmp);
//			envioNotificacionCambioConfiguraciones(tmp, accion);
			filtro.setUsuario(tmp.getUsuario());
			procesoCalculoRetenciones(filtro, accion);
		} catch(IllegalArgumentException iae) {
			String msg = "Error al intentar desactivar la configuracion";
			LOG.error(msg, iae);
			throw new RuntimeException(msg);
		} catch(EntityNotFoundException enfe) {
			String msg = "Error al intentar desactivar la configuracion: No se pudo obtener el estado de la entidad";
			LOG.error(msg, enfe);
			throw new RuntimeException(msg);
		}
	}
	
	private void procesoCalculoRetenciones(RetencionImpuestosDTO filtro, TipoAccionConfiguracion accion) {
		Long idResultado;
		RetencionCedularesView monitor = new RetencionCedularesView();
		try {
			monitor.setId(Long.valueOf(0));
			monitor.setIdConfiguracion(filtro.getId());
			monitor.setTipoEjecucion(new ValorCatalogoAgentes());
			monitor.getTipoEjecucion().setClave(accion.getClave());
			monitor.setEstatusEjecucion(new ValorCatalogoAgentes());
			monitor.getEstatusEjecucion().setClave(TipoEstatusEjecucionCalculo.POR_INICIAR.getClave());
			monitor.setAvance(Long.valueOf(0));
			monitor.setUsuario(filtro.getUsuario());
			idResultado = retencionImpuestosDao.setMonitorAvance(monitor);
			if(idResultado.compareTo(Long.valueOf(0)) == 0) {
				throw new RuntimeException("Error al registrar avance inicial en monitor de Impuestos Estatales");
			}
			bloquearCalculoRetencionImpuestos(idResultado);
			monitor.setId(idResultado);
			retencionImpuestosDao.ejecutaProcesoRetencion(filtro, monitor, TipoEjecucionCalculo.USUARIO);
		} catch(Exception e) {
			LOG.error("Error al ejecutar el proceso de retencion de Impuestos Estatales || " + e.getMessage(), e);
		}
	}

	public List<RetencionCedularesView> listarMonitorRetencionImpuestos() {
		return retencionImpuestosDao.listarMonitorRetencionImpuestos();
	}

	public List<RetencionImpuestosDTO> listarHistorico(RetencionImpuestosDTO filtro) {
		List<RetencionImpuestosDTO> configuraciones = retencionImpuestosDao.listarHistorico(filtro);
		configuraciones = fillDTOList(configuraciones);
		return configuraciones;
	}
	
	public void envioNotificacionCambioConfiguraciones(RetencionImpuestos configuracion, TipoAccionConfiguracion accion) {
		RetencionImpuestos retencion = new RetencionImpuestos();
		EstadoDTO edo = new EstadoDTO();
		
		try {
			String titulo = MAIL_NOTIFICACION_TITULO;
			ParametroGlobal parametroGlobal = parametroGlobalService.obtenerPorIdParametro(ParametroGlobalService.AP_MIDAS, ParametroGlobalService.CORREONOTIRETIMPUESTO);
			Long tmpId = 0L;
			String tmpEstado = "";
			String tmpDesc = "";
			String tmpEstatus = "";

			retencion = entidadService.findById(RetencionImpuestos.class, configuracion.getId());
			if(retencion != null) {
				tmpId = retencion.getId();
				if (retencion.getEstado() != null && retencion.getEstado().getStateId() !=null) {
					edo = entidadService.findById(EstadoDTO.class,retencion.getEstado().getStateId());
					tmpEstado = edo.getStateName();
				}
				if(retencion.getConcepto() != null) {
					tmpDesc = retencion.getConcepto();
				}
				if(retencion.getEstatus() != null) {
					tmpEstatus = retencion.getEstatus();
				}
			}

			String bodyCorreo = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, MAIL_NOTIFICACION_KEY_CUERPO, accion.getClave(), tmpId, tmpDesc, tmpEstado, tmpEstatus);
			
			List<String> destinatario = new ArrayList<String>();
			if (!parametroGlobal.getValor().isEmpty()) {
				destinatario.add(parametroGlobal.getValor());
			} else {
				destinatario.add("");
			}
			
			mailService.sendMessage(destinatario, titulo, bodyCorreo, MAIL_NOTIFICACION_DESTINO, null, null, null, null,	null);
		} catch (Exception e) {
			String msg = "Error al enviar la notificacion de cambio de configuracion de Impuestos Cedulares";
			LOG.error(msg, e);
		}
	}
	
	public List<RetencionImpuestosDTO> findByFiltersView(RetencionImpuestosDTO filtro) {
		List<RetencionImpuestosDTO> configuraciones = retencionImpuestosDao.findByFiltersView(filtro);
		configuraciones = fillDTOList(configuraciones);
		return configuraciones.isEmpty() ? new ArrayList<RetencionImpuestosDTO>() : configuraciones; 
	}
	
	private void bloquearCalculoRetencionImpuestos(Long idMonitor) {
		RetencionCedularesView tmp = entidadService.findById(RetencionCedularesView.class, idMonitor);
		tmp.setEstatus(Integer.valueOf(1));
		entidadService.save(tmp);
	}
	
	public boolean isCalculoRetencionImpuestosEnProceso() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(QUERY_PARAM_ESTATUS, Integer.valueOf(1));
		long result = entidadService.findByPropertiesCount(RetencionCedularesView.class, params);
		return result == 0 ? false : true;
	}
	
	private List<RetencionImpuestosDTO> fillDTOList(List<RetencionImpuestosDTO> listExample) {
		for(RetencionImpuestosDTO elem : listExample) {
			ValorCatalogoAgentes tipoComision = new ValorCatalogoAgentes();
			ValorCatalogoAgentes personalidadJuridica = new ValorCatalogoAgentes();
			
			try {
				EstadoDTO estado = entidadService.findById(EstadoDTO.class, elem.getEstadoId());
				
				elem.setEstado(estado);
				
				tipoComision.setId(elem.getTipoComisionId());
				tipoComision = valorCatalogoAgentesService.loadById(tipoComision);
				
				elem.setTipoComision(tipoComision);
				
				personalidadJuridica.setId(elem.getPersonalidadJuridicaId());
				personalidadJuridica = valorCatalogoAgentesService.loadById(personalidadJuridica);
				
				elem.setPersonalidadJuridica(personalidadJuridica);
				
				if(elem.getFechaModificacionTimestamp() != null) {
					StringBuilder sb = new StringBuilder(elem.getFechaModificacionTimestamp().stringValue());
					String fecha = sb.substring(0, sb.indexOf(" "));
					String[] fechaArr = fecha.split("-");
					String tiempo = sb.substring(fecha.length()).trim();
					String[] tiempoArr = tiempo.split("\\.");
					
					String timestamp = (fechaArr[2].length() < 2 ? "0" + fechaArr[2] : fechaArr[2]) + "/" +
							(fechaArr[1].length() < 2 ? "0" + fechaArr[1] : fechaArr[1]) + "/" +
							(fechaArr[0]) + " " +
							(tiempoArr[0].length() < 2 ? "0" + tiempoArr[0] : tiempoArr[0]) + ":" +
							(tiempoArr[1].length() < 2 ? "0" + tiempoArr[1] : tiempoArr[1]);
					elem.setFechaModificacionString(timestamp);
				}
			} catch(Exception e) {
				LOG.error("Ha ocurrido un error al intentar obtener el listado de configuraciones.", e);
				throw new RuntimeException("Ha ocurrido un error al intentar obtener el listado de configuraciones.");
			}
		}
		return listExample;
	}

//	@Schedule(minute="*/30", persistent=false)
//	private void execute() {
//		LOG.info("Iniciando ejecucion automatizada de Retencion de Impuestos Estatales");
//		Map<String, Object> params = new HashMap<String, Object>();
//		params.put(QUERY_PARAM_ENPROCESO, Boolean.TRUE);
//		long cuantos = entidadService.findByPropertiesCount(CalculoComisiones.class, params);
//		
//		if(cuantos > 0) {
//			throw new RuntimeException(Utilerias.getMensajeRecurso(sistemaContext.getArchivoRecursosBack(), KEY_ERROR_BLOQUEO_COMISION));
//		}
//		
//		if(isCalculoRetencionImpuestosEnProceso()) {
//			throw new RuntimeException(Utilerias.getMensajeRecurso(sistemaContext.getArchivoRecursosBack(), KEY_ERROR_BLOQUEO_RETENCION));
//		}
//		
//		retencionImpuestosDao.ejecutaProcesoRetencion(null, null, TipoEjecucionCalculo.JOB);
//	}
	
	@PersistenceContext
	private EntityManager em;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private ParametroGlobalService parametroGlobalService;
	
	@EJB
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	
	@EJB
	private MailService mailService;
	
	@EJB
	private RetencionImpuestosDao retencionImpuestosDao;
	
	@EJB
	private ListadoService listadoService;
	
	private static final Logger LOG = Logger.getLogger(RetencionImpuestosServiceImpl.class);
	
}
