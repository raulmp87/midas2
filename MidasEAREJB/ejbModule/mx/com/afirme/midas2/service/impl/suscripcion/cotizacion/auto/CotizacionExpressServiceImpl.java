package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.consultas.formapago.FormaPagoInterfazServiciosRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.NegocioSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionDAO;
import mx.com.afirme.midas2.dao.pkg.PkgAutServiciosDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.CoberturaPrimaCotExpressDao;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccionId;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CoberturaCotizacionExpress;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CoberturaPrimaCotExpress;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CotizacionExpress;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotExpressDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.CotizacionExpressDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteService;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioDerechosService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicioService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUsoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionExpressService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;


/**
 * @author amosomar
 *
 */
@Stateless
public class CotizacionExpressServiceImpl implements CotizacionExpressService {

	private CoberturaPrimaCotExpressDao coberturaPrimaCotExpressDao;
	private NegocioDerechosService negocioDerechosService;
	private FormaPagoInterfazServiciosRemote formaPagoInterfazServicios;
	private NegocioTipoUsoService negocioTipoUsoService;
	private NegocioTipoServicioService negocioTipoServicioService;
	private NegocioAgrupadorTarifaSeccionDao negocioAgrupadorTarifaSeccionDao;	
	private PkgAutServiciosDao pkgAutServiciosDao;
	private NegocioSeccionDao negocioSeccionDao;
	private AgenteService agenteService;
	private UsuarioService usuarioService;
	private CoberturaService coberturaService;
	private EntidadDao entidadDao;
	private CotizacionService cotizacionService;
	private NegocioCobPaqSeccionDAO negocioCobPaqSeccionDAO;
	
	@EJB
	public void setNegocioCobPaqSeccionDAO(
			NegocioCobPaqSeccionDAO negocioCobPaqSeccionDAO) {
		this.negocioCobPaqSeccionDAO = negocioCobPaqSeccionDAO;
	}
	
	@EJB
	public void setCoberturaPrimaCotExpressDao(
			CoberturaPrimaCotExpressDao coberturaPrimaCotExpressDao) {
		this.coberturaPrimaCotExpressDao = coberturaPrimaCotExpressDao;
	}
	
	@EJB
	public void setNegocioDerechosService(
			NegocioDerechosService negocioDerechosService) {
		this.negocioDerechosService = negocioDerechosService;
	}
	
	@EJB
	public void setFormaPagoInterfazServicios(
			FormaPagoInterfazServiciosRemote formaPagoInterfazServicios) {
		this.formaPagoInterfazServicios = formaPagoInterfazServicios;
	}
	
	@EJB
	public void setNegocioTipoUsoService(
			NegocioTipoUsoService negocioTipoUsoService) {
		this.negocioTipoUsoService = negocioTipoUsoService;
	}
	
	@EJB
	public void setNegocioTipoServicioService(
			NegocioTipoServicioService negocioTipoServicioService) {
		this.negocioTipoServicioService = negocioTipoServicioService;
	}
	
	@EJB
	public void setNegocioAgrupadorTarifaSeccionDao(
			NegocioAgrupadorTarifaSeccionDao negocioAgrupadorTarifaSeccionDao) {
		this.negocioAgrupadorTarifaSeccionDao = negocioAgrupadorTarifaSeccionDao;
	}
	
	@EJB
	public void setPkgAutServiciosDao(PkgAutServiciosDao pkgAutServiciosDao) {
		this.pkgAutServiciosDao = pkgAutServiciosDao;
	}
	
	@EJB
	public void setNegocioSeccionDao(NegocioSeccionDao negocioSeccionDao) {
		this.negocioSeccionDao = negocioSeccionDao;
	}
	
	@EJB
	public void setAgenteService(AgenteService agenteService) {
		this.agenteService = agenteService;
	}
	
	@EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@EJB
	public void setCoberturaService(CoberturaService coberturaService) {
		this.coberturaService = coberturaService;
	}
	
	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}
	
	@EJB
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	@Override
	public CotizacionExpressDTO crearNuevaCotizacionExpressDTO() {
		CotizacionExpressDTO cotizacionExpressDTO = new CotizacionExpressDTO();
		
		//Obtiene los valores de negocioDefault
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("claveNegocio", "A");
		params.put("claveDefault", true);
		List<Negocio> negocioList = entidadDao.findByProperties(Negocio.class, params);
		if(negocioList != null  && negocioList.size() > 0){
			Negocio negocio = negocioList.get(0);
			
			List<NegocioProducto> negocioProductoList = entidadDao.findByProperty(NegocioProducto.class, "negocio.idToNegocio", negocio.getIdToNegocio());
			NegocioProducto negocioProducto = negocioProductoList.get(0);
			List<NegocioTipoPoliza> negocioTipoPolizaList = entidadDao.findByProperty(NegocioTipoPoliza.class, "negocioProducto.idToNegProducto", negocioProducto.getIdToNegProducto());
			//List<NegocioTipoPoliza> negocioTipoPolizaList = entidadDao.findByProperty(NegocioTipoPoliza.class, "negocioProducto.idToNegProducto", new Long(19));
			NegocioTipoPoliza negocioTipoPoliza = negocioTipoPolizaList.get(0);
			List<NegocioSeccion> negocioSeccionList = entidadDao.findByProperty(NegocioSeccion.class, "negocioTipoPoliza.idToNegTipoPoliza", negocioTipoPoliza.getIdToNegTipoPoliza());
			NegocioSeccion negocioSeccion = negocioSeccionList.get(0);
//			List<NegocioAgente> negocioAgenteList = entidadDao.findByProperty(NegocioAgente.class, "negocio.idToNegocio", negocio.getIdToNegocio());
//			NegocioAgente negocioAgente = negocioAgenteList.get(0);
//			Agente agente = new Agente();
			
			cotizacionExpressDTO.setIdMoneda(getMonedaDefault());
			cotizacionExpressDTO.setNegocioSeccion(negocioSeccion);
//			cotizacionExpressDTO.setAgente(agente);
			//Puede ser que este deba ser obtenido de algun otro lado.
			cotizacionExpressDTO.setVariableModificadoraDescripcion("default");
			cotizacionExpressDTO.setPaso(1l);
		}
		return cotizacionExpressDTO;
	}
	
	/**
	 * Obtiene la moneda default para la cotizacion express.
	 * @return
	 */
	private Short getMonedaDefault() {
		return new Short("484");
	}
	
	private NegocioSeccion getNegocioSeccionDefault(Negocio negocio) {
		// TODO: Obtener la linea de negocio default para cotizador express
		BigDecimal idNegocioSeccion = new BigDecimal("341");
		negocioSeccionDao.findByProperty("", negocio.getIdToNegocio());
		return negocioSeccionDao.findById(idNegocioSeccion);
	}
	
	@Override
	public CotizacionExpress cotizar(CotizacionExpress cotizacionExpress, EstiloVehiculoDTO estiloVehiculoDTO,
			NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion, NegocioTipoUso negocioTipoUsoDefault,
			NegocioTipoServicio negocioTipoServicioDefault, String claveCirculacion) {
		//Buscar las coberturas para esta cotizacion.
		cotizacionExpress.setCoberturaCotizacionExpresses(buscarCoberturas(
				cotizacionExpress, estiloVehiculoDTO, negocioAgrupadorTarifaSeccion, 
				negocioTipoUsoDefault, negocioTipoServicioDefault, claveCirculacion));
		calcularPrimaTotal(cotizacionExpress);
		return cotizacionExpress;
	}

	@Override
	public CotizacionExpressDTO cotizarPaqueteFormaPago(
			CotizacionExpressDTO cotizacionExpressDTO) {
		NegocioSeccion negocioSeccion = cotizacionExpressDTO
				.getNegocioSeccion();
		
		List<NegocioFormaPago> negocioFormaPagos = new LinkedList<NegocioFormaPago>();
		
		negocioFormaPagos = entidadDao.findByProperty(
				NegocioFormaPago.class, "negocioProducto.idToNegProducto",
				negocioSeccion.getNegocioTipoPoliza().getNegocioProducto()
						.getIdToNegProducto());
		
		List<NegocioPaqueteSeccion> negocioPaqueteSecciones = new LinkedList<NegocioPaqueteSeccion>();
		negocioPaqueteSecciones = entidadDao
				.findByProperty(NegocioPaqueteSeccion.class,
						"negocioSeccion.idToNegSeccion",
						negocioSeccion.getIdToNegSeccion());
		
		EstiloVehiculoDTO estiloVehiculoDTO = entidadDao.findById(EstiloVehiculoDTO.class, 	
				new EstiloVehiculoId(cotizacionExpressDTO.getModeloVehiculo().getId().getClaveTipoBien(), 
				cotizacionExpressDTO.getModeloVehiculo().getId().getClaveEstilo(), 
				cotizacionExpressDTO.getModeloVehiculo().getId().getIdVersionCarga()));
		
		NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion = 
			negocioAgrupadorTarifaSeccionDao.buscarPorNegocioSeccionYMoneda(
					cotizacionExpressDTO.getNegocioSeccion(), 
					new BigDecimal(cotizacionExpressDTO.getIdMoneda()));
		
		NegocioTipoUso negocioTipoUsoDefault = negocioTipoUsoService.buscarDefault(
				cotizacionExpressDTO.getNegocioSeccion());
		
		NegocioTipoServicio negocioTipoServicioDefault = negocioTipoServicioService.buscarDefault(
				cotizacionExpressDTO.getNegocioSeccion());
		
		String claveCirculacion = pkgAutServiciosDao
		.getZonaCirculacion(Integer.valueOf(cotizacionExpressDTO.getEstado()
				.getStateId()), Integer.valueOf(cotizacionExpressDTO
				.getMunicipio().getMunicipalityId()));
		
		BigDecimal gastosExpedicion = getGastosExpedicion(negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio());
		
		CotizacionExpress cotizacionExpress = null;
		for (NegocioPaqueteSeccion negocioPaqueteSeccion : negocioPaqueteSecciones) {
			for (NegocioFormaPago negocioFormaPago : negocioFormaPagos) {
				cotizacionExpress = new CotizacionExpress();				
				populateCotizacionExpress(cotizacionExpressDTO, cotizacionExpress);
				cotizacionExpress.setGastosExpedicion(gastosExpedicion);
				cotizacionExpress.setPaquete(negocioPaqueteSeccion.getPaquete());
				cotizacionExpress.setFormaPago(negocioFormaPago.getFormaPagoDTO());
				// Cotizacion express add list
				cotizacionExpress = cotizar(cotizacionExpress, estiloVehiculoDTO, negocioAgrupadorTarifaSeccion, 
						negocioTipoUsoDefault, negocioTipoServicioDefault, claveCirculacion);
				cotizacionExpressDTO.addCotizacionExpress(cotizacionExpress);
			}
		}
		return cotizacionExpressDTO;
	}
	
	public void populateCotizacionExpress(CotizacionExpressDTO cotizacionExpressDTO, CotizacionExpress cotizacionExpress) {
		cotizacionExpress.setModeloVehiculo(cotizacionExpressDTO.getModeloVehiculo());
		cotizacionExpress.setEstado(cotizacionExpressDTO.getEstado());
		cotizacionExpress.setMunicipio(cotizacionExpressDTO.getMunicipio());
		cotizacionExpress.setVariableModificadoraDescripcion(cotizacionExpressDTO.getVariableModificadoraDescripcion());
		cotizacionExpress.setNegocioSeccion(cotizacionExpressDTO.getNegocioSeccion());
		cotizacionExpress.setIdMoneda(cotizacionExpressDTO.getIdMoneda());
		cotizacionExpress.setIva(getIva(cotizacionExpressDTO.getFrontera()));
	}

	/**
	 * Se hace cargo de calcular la prima total para la cotizacion dada.
	 * @param cotizacionExpress
	 */
	private void calcularPrimaTotal(CotizacionExpress cotizacionExpress) {
		//Calcular el recargo por pago fraccionado y calcular la prima total.
		List<CoberturaCotizacionExpress> coberturaCotizacionExpresses = new LinkedList<CoberturaCotizacionExpress>();
		coberturaCotizacionExpresses = cotizacionExpress.getCoberturaCotizacionExpresses();

		BigDecimal totalPrimas = BigDecimal.ZERO;
		BigDecimal primaNetaB = BigDecimal.ZERO;
		BigDecimal descuentoGlobal = BigDecimal.ZERO;
		BigDecimal descuentoComision = BigDecimal.ZERO;
		CoberturaPrimaCotExpress coberturaPrimaCotExpress  = null;
		for (CoberturaCotizacionExpress coberturaCotizacionExpress : coberturaCotizacionExpresses) {
			coberturaPrimaCotExpress =  new CoberturaPrimaCotExpress();
			coberturaPrimaCotExpress = coberturaCotizacionExpress.getCoberturaPrimaCotExpress();
			totalPrimas = totalPrimas.add(coberturaPrimaCotExpress.getPrimaNetaARdt());
			primaNetaB = primaNetaB.add(coberturaPrimaCotExpress.getPrimaNetaB());
		}
		cotizacionExpress.setTotalPrimas(totalPrimas);
		cotizacionExpress.setPrimaNetaB(primaNetaB);	
		cotizacionExpress.setDescuentoGlobal(descuentoGlobal);
		cotizacionExpress.setDescuentoComision(descuentoComision);
		
		final BigDecimal cien = new BigDecimal("100");
		BigDecimal iva = cotizacionExpress.getIva();
		BigDecimal primaNeta = totalPrimas.subtract(descuentoComision);
		BigDecimal financiamiento = primaNeta.multiply(getPorcentajeRecargoPagoFraccionado(cotizacionExpress).divide(cien));		
		BigDecimal primaNeta2 = primaNeta.add(financiamiento).add(cotizacionExpress.getGastosExpedicion());
		BigDecimal totalIva = primaNeta2.multiply(iva.divide(cien));
		BigDecimal primaTotal = primaNeta2.add(totalIva);
		
		cotizacionExpress.setPrimaNeta(primaNeta);
		cotizacionExpress.setFinanciamiento(financiamiento);
		cotizacionExpress.setGastosExpedicion(cotizacionExpress.getGastosExpedicion());
		cotizacionExpress.setTotalIva(totalIva);
		cotizacionExpress.setPrimaTotal(primaTotal);
		cotizacionExpress.setIva(iva);
	}

	
	//FIXME: Esto debe estar como un servicio centralizado para todo aquel proceso que necesite saber el porcentaje de IVA.
	private BigDecimal getIva(boolean frontera) {
		final BigDecimal ivaFrontera = new BigDecimal("11");
		final BigDecimal ivaNoFrontera = new BigDecimal("16");
		if (frontera) {
			return ivaFrontera;
		}
		return ivaNoFrontera;
	}
	
	private AgenteDTO getAgenteDefault() {
		final int idAgenteDefault = 80000; 
		return agenteService.obtenerAgente(idAgenteDefault);
	}

	private BigDecimal getGastosExpedicion(Long idNegocio) {	
		return new BigDecimal(negocioDerechosService.obtenerDechosPolizaDefault(idNegocio).getImporteDerecho());
	}
	
	private BigDecimal getGastosExpedicion(CotizacionExpress cotizacionExpress) {
		Long idNegocio = cotizacionExpress.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio();
		return new BigDecimal(negocioDerechosService.obtenerDechosPolizaDefault(idNegocio).getImporteDerecho());
	}

	private BigDecimal getPorcentajeRecargoPagoFraccionado(CotizacionExpress cotizacionExpress) {
		Integer idFormaPago = cotizacionExpress.getFormaPago().getIdFormaPago();
		FormaPagoIDTO formaPago;
		try {
			formaPago = formaPagoInterfazServicios.getPorIdClaveNegocio(idFormaPago, cotizacionExpress.getIdMoneda(), "","A");
		} catch (SystemException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return formaPago.getPorcentajeRecargoPagoFraccionado();
	}

	/**
	 * A partir de una lista de <code>coberturaPrimaCotExpress</code> genera una lista de <code>coberturaCotizacionExpress</code>
	 * @param coberturaPrimaCotExpressList
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<CoberturaCotizacionExpress> crearCoberturaCotizacionList(
			List<CoberturaPrimaCotExpress> coberturaPrimaCotExpressList, List<NegocioCobPaqSeccion> coberturasNegocio) {
		List<CoberturaCotizacionExpress> coberturaCotizacionExpresses = new ArrayList<CoberturaCotizacionExpress>();
		CoberturaCotizacionExpress coberturaCotizacionExpress = null;
		for (CoberturaPrimaCotExpress coberturaPrimaCotExpress : coberturaPrimaCotExpressList) {
			//Filtra por negocio express
			final BigDecimal idToCobertura = coberturaPrimaCotExpress.getCobertura().getIdToCobertura();
			CobPaquetesSeccion configuracion = null;
			try{
				CobPaquetesSeccionId id = new CobPaquetesSeccionId(coberturaPrimaCotExpress.getIdLineaNegocio(), 
					coberturaPrimaCotExpress.getPaquete().getId(), idToCobertura.longValue());
				configuracion =  entidadDao.findById(CobPaquetesSeccion.class, id);
				if(configuracion == null){
					configuracion = new CobPaquetesSeccion();
					configuracion.setClaveObligatoriedad(CoberturaDTO.OPCIONAL);
				}
			}catch(Exception e){
				//Se toma como obligatoria en caso de error
				configuracion = new CobPaquetesSeccion();
				configuracion.setClaveObligatoriedad(CoberturaDTO.OBLIGATORIO);				
			}
			
			if(!configuracion.getClaveObligatoriedad().equals(CoberturaDTO.OPCIONAL)) {
				Predicate predicate = new Predicate() {	
					@Override
					public boolean evaluate(Object arg0) {
						NegocioCobPaqSeccion config = (NegocioCobPaqSeccion)arg0;
						return config.getCoberturaDTO().getIdToCobertura().equals(idToCobertura);
					}
				};
				List<NegocioCobPaqSeccion> coberturaList = (List<NegocioCobPaqSeccion>) CollectionUtils.select(coberturasNegocio, predicate);
				if(coberturaList != null && !coberturaList.isEmpty()){
					coberturaCotizacionExpress = new CoberturaCotizacionExpress();
					coberturaCotizacionExpress.setCoberturaPrimaCotExpress(coberturaPrimaCotExpress);
					coberturaCotizacionExpress.setSumaAseguradaStr(coberturaService
							.getCoberturaSumaAseguradaStr(
									coberturaPrimaCotExpress.getCobertura(),
									coberturaPrimaCotExpress.getSumaAsegurada()));
					coberturaCotizacionExpresses.add(coberturaCotizacionExpress);
				}
			}
			
		}
		return coberturaCotizacionExpresses;
	}

	/**
	 * Obtiene las coberturas correspondientes para la cotizacion.
	 * @param cotizacionExpress
	 * @return
	 */
	private List<CoberturaCotizacionExpress> buscarCoberturas(
			CotizacionExpress cotizacionExpress, EstiloVehiculoDTO estiloVehiculoDTO, 
			NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion, NegocioTipoUso negocioTipoUsoDefault,
			NegocioTipoServicio negocioTipoServicioDefault, String claveCirculacion) {
		CoberturaPrimaCotExpress coberturaPrimaCotExpress = new CoberturaPrimaCotExpress();		
		coberturaPrimaCotExpress.setIdLineaNegocio(cotizacionExpress.getNegocioSeccion().getSeccionDTO().getIdToSeccion().longValue());	
		coberturaPrimaCotExpress.setIdAgrupadorTarifa(negocioAgrupadorTarifaSeccion.getIdToAgrupadorTarifa().longValue());
		coberturaPrimaCotExpress.setIdVerAgrupadorTarifa(negocioAgrupadorTarifaSeccion.getIdVerAgrupadorTarifa().longValue());
		coberturaPrimaCotExpress.setIdMoneda(cotizacionExpress.getIdMoneda().longValue());
		coberturaPrimaCotExpress.setIdVersionCarga(cotizacionExpress.getModeloVehiculo().getId().getIdVersionCarga().longValue());
		coberturaPrimaCotExpress.setClaveTipoBien(cotizacionExpress.getModeloVehiculo().getId().getClaveTipoBien());
		coberturaPrimaCotExpress.setClaveEstilo(cotizacionExpress.getModeloVehiculo().getId().getClaveEstilo());
		coberturaPrimaCotExpress.setModeloVehiculo(cotizacionExpress.getModeloVehiculo().getId().getModeloVehiculo());
		coberturaPrimaCotExpress.setIdTipoVehiculo(estiloVehiculoDTO.getIdTcTipoVehiculo().longValue());
		coberturaPrimaCotExpress.setIdMarcaVehiculo(estiloVehiculoDTO.getMarcaVehiculoDTO().getIdTcMarcaVehiculo().longValue());
		coberturaPrimaCotExpress.setClaveZonaCirculacion(claveCirculacion);
		coberturaPrimaCotExpress.setClaveUsoVehiculo(negocioTipoUsoDefault.getTipoUsoVehiculoDTO().getCodigoTipoUsoVehiculo());
		coberturaPrimaCotExpress.setClaveServicioVehiculo(negocioTipoServicioDefault.getTipoServicioVehiculoDTO().getCodigoTipoServicioVehiculo());
		coberturaPrimaCotExpress.setPaquete(cotizacionExpress.getPaquete());
		
		//Obtiene Coberturas Asociadas al negocio
		//edo y municipio
		List<NegocioCobPaqSeccion> coberturasNegocio = null;
		
		coberturasNegocio = negocioCobPaqSeccionDAO.getCoberturas(
				cotizacionExpress.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
				cotizacionExpress.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getProductoDTO().getIdToProducto(),
				cotizacionExpress.getNegocioSeccion().getNegocioTipoPoliza().getTipoPolizaDTO().getIdToTipoPoliza(), 
				cotizacionExpress.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), cotizacionExpress.getPaquete().getId(), 
				cotizacionExpress.getEstado().getStateId(), cotizacionExpress.getMunicipio().getMunicipalityId(), cotizacionExpress.getIdMoneda());
		if(coberturasNegocio.isEmpty()){
			//edo
			coberturasNegocio = negocioCobPaqSeccionDAO.getCoberturas(
					cotizacionExpress.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
					cotizacionExpress.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getProductoDTO().getIdToProducto(),
					cotizacionExpress.getNegocioSeccion().getNegocioTipoPoliza().getTipoPolizaDTO().getIdToTipoPoliza(), 
					cotizacionExpress.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), cotizacionExpress.getPaquete().getId(), 
					cotizacionExpress.getEstado().getStateId(), null, cotizacionExpress.getIdMoneda());
		}
		if(coberturasNegocio.isEmpty()){
			coberturasNegocio = negocioCobPaqSeccionDAO.getCoberturas(
					cotizacionExpress.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
					cotizacionExpress.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getProductoDTO().getIdToProducto(),
					cotizacionExpress.getNegocioSeccion().getNegocioTipoPoliza().getTipoPolizaDTO().getIdToTipoPoliza(), 
					cotizacionExpress.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), cotizacionExpress.getPaquete().getId(), 
					null, null, cotizacionExpress.getIdMoneda());
		}	
		
		return crearCoberturaCotizacionList(coberturaPrimaCotExpressDao.buscarPorCombinacion(coberturaPrimaCotExpress), coberturasNegocio);
	}

	@Override
	public CotizacionDTO cotizarFormalmente(CotizacionExpress cotizacionExpress) {
		return cotizacionService.solicitudCotizacionFormal(cotizacionExpress);
	}
	
	@Override
	public BigDecimal getIdVersionCarga() {
		return new BigDecimal("1");
	}
	
	@Override
	public boolean isBusquedaAgenteVisible() {
		Usuario usuario = usuarioService.getUsuarioActual();
		List<Rol> roles = usuario.getRoles();
		for (Rol rol: roles) {
			if (rol.getDescripcion().startsWith("Rol_Suscriptor")) {
				return true;
			}
		}
		return false;
	}

	@Override
	public List<ResumenCotExpressDTO> inicializarDatosParaImpresion(
			CotizacionExpressDTO cotizacionExpressDTO) {	
		Map<String,EsquemaPagoCotizacionDTO> esquemasCotExpress = new LinkedHashMap<String,EsquemaPagoCotizacionDTO>();
		List<ResumenCotExpressDTO> resumenExpressList = new ArrayList<ResumenCotExpressDTO>();
//	 cotizarPaqueteFormaPago(cotizacionExpressDTO);
		NegocioSeccion negocioSeccion = cotizacionExpressDTO.getNegocioSeccion();
			List<NegocioFormaPago> negocioFormaPagos = entidadDao.findByProperty(
					NegocioFormaPago.class, "negocioProducto.idToNegProducto",
					negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getIdToNegProducto());
			
			List<NegocioPaqueteSeccion> negocioPaqueteSecciones = entidadDao
					.findByProperty(NegocioPaqueteSeccion.class,"negocioSeccion.idToNegSeccion",
							negocioSeccion.getIdToNegSeccion());
			
			GregorianCalendar gcFechaIni = new GregorianCalendar();
			gcFechaIni.setTime(new Date());		
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(cotizacionService.getFinVigenciaDefault(negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getProductoDTO(), gcFechaIni.getTime()));
			
			EstiloVehiculoDTO estiloVehiculoDTO = entidadDao.findById(EstiloVehiculoDTO.class, 	
					new EstiloVehiculoId(cotizacionExpressDTO.getModeloVehiculo().getId().getClaveTipoBien(), 
					cotizacionExpressDTO.getModeloVehiculo().getId().getClaveEstilo(), 
					cotizacionExpressDTO.getModeloVehiculo().getId().getIdVersionCarga()));
			
			NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion = 
				negocioAgrupadorTarifaSeccionDao.buscarPorNegocioSeccionYMoneda(
						cotizacionExpressDTO.getNegocioSeccion(), 
						new BigDecimal(cotizacionExpressDTO.getIdMoneda()));
			
			NegocioTipoUso negocioTipoUsoDefault = negocioTipoUsoService.buscarDefault(
					cotizacionExpressDTO.getNegocioSeccion());
			
			NegocioTipoServicio negocioTipoServicioDefault = negocioTipoServicioService.buscarDefault(
					cotizacionExpressDTO.getNegocioSeccion());
			
			String claveCirculacion = pkgAutServiciosDao
			.getZonaCirculacion(Integer.valueOf(cotizacionExpressDTO.getEstado()
					.getStateId()), Integer.valueOf(cotizacionExpressDTO
					.getMunicipio().getMunicipalityId()));
			
			BigDecimal gastosExpedicion = getGastosExpedicion(negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio());
			
			for (NegocioPaqueteSeccion negocioPaqueteSeccion : negocioPaqueteSecciones) {				
				for (NegocioFormaPago negocioFormaPago : negocioFormaPagos) {
					CotizacionExpress cotizacionExpress = new CotizacionExpress();
					ResumenCotExpressDTO resumenCotExpressDTO = new ResumenCotExpressDTO();
					cotizacionExpress.setGastosExpedicion(gastosExpedicion);
					populateCotizacionExpress(cotizacionExpressDTO, cotizacionExpress);
					resumenCotExpressDTO.setPaquete(negocioPaqueteSeccion.getPaquete());
					resumenCotExpressDTO.setFormaPago(negocioFormaPago.getFormaPagoDTO());	
					cotizacionExpress.setPaquete(negocioPaqueteSeccion.getPaquete());
					cotizacionExpress.setFormaPago(negocioFormaPago.getFormaPagoDTO());
					cotizacionExpress = cotizar(cotizacionExpress, estiloVehiculoDTO, 
							negocioAgrupadorTarifaSeccion, negocioTipoUsoDefault, 
							negocioTipoServicioDefault, claveCirculacion);
					
					resumenCotExpressDTO.setCotizacionExpress(cotizacionExpress);					
					
					EsquemaPagoCotizacionDTO esquemaPagoCotizacionDTO = cotizacionService.findEsquemaPagoCotizacion(gcFechaIni.getTime(),
													gcFechaIni.getTime(), gcFechaFin.getTime(),cotizacionExpress.getFormaPago().getIdFormaPago(), 
													cotizacionExpress.getPrimaNeta(),cotizacionExpress.getGastosExpedicion(), 
													cotizacionExpress.getIva(), cotizacionExpress.getIdMoneda().intValue(), null, true);
	
				esquemasCotExpress.put(negocioPaqueteSeccion.getPaquete().getId().toString()+"-"+
						negocioFormaPago.getFormaPagoDTO().getIdFormaPago().toString(), esquemaPagoCotizacionDTO);
				resumenCotExpressDTO.setEsquemaPagoExpress(esquemasCotExpress);
				resumenExpressList.add(resumenCotExpressDTO);
				}
			}

			return resumenExpressList;	
	}
}
