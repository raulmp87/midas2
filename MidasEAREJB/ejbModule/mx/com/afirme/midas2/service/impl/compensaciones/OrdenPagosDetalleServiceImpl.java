package mx.com.afirme.midas2.service.impl.compensaciones;
// default package


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;
import mx.com.afirme.midas2.dao.compensaciones.OrdenPagosDetalleDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.domain.compensaciones.OrdenPagosDetalle;
import mx.com.afirme.midas2.service.compensaciones.OrdenPagosDetalleService;



/**
 * Facade for entity OrdenPagosDetalle.
 * 
 * @see .OrdenPagosDetalle
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class OrdenPagosDetalleServiceImpl extends EntidadHistoricoDaoImpl implements
		OrdenPagosDetalleService {
	// property constants
	public static final String IDTOPOLIZA = "idtopoliza";
	public static final String ID_RECIBO = "idRecibo";
	public static final String NOMBRE_BENEFICIARIO = "nombreBeneficiario";
	public static final String CLAVE_NOMBRE = "claveNombre";
	public static final String NOMBRE_CONTRATANTE = "nombreContratante";
	public static final String IMPORTE_PRIMA = "importePrima";
	public static final String IMPORTE_BS = "importeBs";
	public static final String IMPORTE_DERPOL = "importeDerpol";
	public static final String IMPORTE_CUM_META = "importeCumMeta";
	public static final String IMPORTE_UTILIDAD = "importeUtilidad";
	public static final String SUBTOTAL = "subtotal";
	public static final String IVA = "iva";
	public static final String IVA_RETENIDO = "ivaRetenido";
	public static final String ISR = "isr";
	public static final String ESTATUS_FACTURA = "estatusFactura";
	public static final String ESTATUS_ORDENPAGO = "estatusOrdenpago";
	public static final String ESTATUS_ORDPAG_GEN = "estatusOrdpagGen";
	public static final String IMPORTE_TOTAL = "importeTotal";
	
	@EJB
	private OrdenPagosDetalleDao ordenPagosDetalleDao;

	private static final Logger LOGGER = Logger.getLogger(OrdenPagosDetalleServiceImpl.class);
	
	public void save(OrdenPagosDetalle entity) {
		LOGGER.debug(">>Guardando OrdenPagosDetalle	");
		try {
			this.ordenPagosDetalleDao.save(entity);
			LOGGER.debug("Se Guardo OrdenPagosDetalle");
		} catch (RuntimeException re) {
			LOGGER.error(
					"Error al guardar OrdenPagosDetalle ",
					re);
			throw re;
		}
	}

	/**
	 * Delete a persistent OrdenPagosDetalle entity.
	 * 
	 * @param entity
	 *            OrdenPagosDetalle entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(OrdenPagosDetalle entity) {
		LOGGER.debug(">>Eliminando OrdenPagosDetalle");
		try {
			this.ordenPagosDetalleDao.delete(entity);
			LOGGER.debug("Se Elimino OrdenPagosDetalle ");
		} catch (RuntimeException re) {
			LOGGER.error(
					"Error al eliminar OrdenPagosDetalle ",
					re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved OrdenPagosDetalle entity and return it or a
	 * copy of it to the sender. A copy of the OrdenPagosDetalle entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            OrdenPagosDetalle entity to update
	 * @return OrdenPagosDetalle the persisted OrdenPagosDetalle entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public OrdenPagosDetalle update(OrdenPagosDetalle entity) {
		LOGGER.debug(">>Actualizando OrdenPagosDetalle update()");
		try {
			OrdenPagosDetalle result = ordenPagosDetalleDao.update(entity);
			LOGGER.debug("Se Actualizo OrdenPagosDetalle	");
			return result;
		} catch (RuntimeException re) {
			LOGGER.error(
					"Error al actualizar OrdenPagosDetalle",
					re);
			throw re;
		}
	}

	public OrdenPagosDetalle findById(Long id) {
		LOGGER.debug("	Buscando por Id ");
				
		try {
			OrdenPagosDetalle instance = ordenPagosDetalleDao.findById(id);
			LOGGER.debug("	Se busco por Id ");
			return instance;
		} catch (RuntimeException re) {
			LOGGER.error(
					"Error al buscar por Id ",
					re);
			throw re;
		}
	}

	/**
	 * Find all OrdenPagosDetalle entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the OrdenPagosDetalle property to query
	 * @param value
	 *            the property value to match
	 * @return List<OrdenPagosDetalle> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<OrdenPagosDetalle> findByProperty(String propertyName,
			final Object value) {
		LOGGER.debug(">>Buscando por Propiedad 	findByProperty");
		List<OrdenPagosDetalle> result;
		try {
			result = this.ordenPagosDetalleDao.findByProperty(propertyName,value);
		} catch (RuntimeException re) {
			LOGGER.error(
					"Error al buscar por Propiedad",
					re);
			throw re;
		}
		return result;
	}

	public List<OrdenPagosDetalle> findByIdtopoliza(Object idtopoliza) {
		return findByProperty(IDTOPOLIZA, idtopoliza);
	}

	public List<OrdenPagosDetalle> findByIdRecibo(Object idRecibo) {
		return findByProperty(ID_RECIBO, idRecibo);
	}

	public List<OrdenPagosDetalle> findByNombreBeneficiario(
			Object nombreBeneficiario) {
		return findByProperty(NOMBRE_BENEFICIARIO, nombreBeneficiario);
	}

	public List<OrdenPagosDetalle> findByClaveNombre(Object claveNombre) {
		return findByProperty(CLAVE_NOMBRE, claveNombre);
	}

	public List<OrdenPagosDetalle> findByNombreContratante(
			Object nombreContratante) {
		return findByProperty(NOMBRE_CONTRATANTE, nombreContratante);
	}

	public List<OrdenPagosDetalle> findByImportePrima(Object importePrima) {
		return findByProperty(IMPORTE_PRIMA, importePrima);
	}

	public List<OrdenPagosDetalle> findByImporteBs(Object importeBs) {
		return findByProperty(IMPORTE_BS, importeBs);
	}

	public List<OrdenPagosDetalle> findByImporteDerpol(Object importeDerpol) {
		return findByProperty(IMPORTE_DERPOL, importeDerpol);
	}

	public List<OrdenPagosDetalle> findByImporteCumMeta(Object importeCumMeta) {
		return findByProperty(IMPORTE_CUM_META, importeCumMeta);
	}

	public List<OrdenPagosDetalle> findByImporteUtilidad(
			Object importeUtilidad) {
		return findByProperty(IMPORTE_UTILIDAD, importeUtilidad);
	}

	public List<OrdenPagosDetalle> findBySubtotal(Object subtotal) {
		return findByProperty(SUBTOTAL, subtotal);
	}

	public List<OrdenPagosDetalle> findByIva(Object iva) {
		return findByProperty(IVA, iva);
	}

	public List<OrdenPagosDetalle> findByIvaRetenido(Object ivaRetenido) {
		return findByProperty(IVA_RETENIDO, ivaRetenido);
	}

	public List<OrdenPagosDetalle> findByIsr(Object isr) {
		return findByProperty(ISR, isr);
	}

	public List<OrdenPagosDetalle> findByEstatusFactura(Object estatusFactura) {
		return findByProperty(ESTATUS_FACTURA, estatusFactura);
	}

	public List<OrdenPagosDetalle> findByEstatusOrdenpago(
			Object estatusOrdenpago) {
		return findByProperty(ESTATUS_ORDENPAGO, estatusOrdenpago);
	}

	public List<OrdenPagosDetalle> findByEstatusOrdpagGen(
			Object estatusOrdpagGen) {
		return findByProperty(ESTATUS_ORDPAG_GEN, estatusOrdpagGen);
	}

	public List<OrdenPagosDetalle> findByImporteTotal(Object importeTotal) {
		return findByProperty(IMPORTE_TOTAL, importeTotal);
	}

	/**
	 * Find all OrdenPagosDetalle entities.
	 * 
	 * @return List<OrdenPagosDetalle> all OrdenPagosDetalle entities
	 */
	@SuppressWarnings("unchecked")
	public List<OrdenPagosDetalle> findAll() {
		LOGGER.debug(">>buscar todo()");
		List <OrdenPagosDetalle> result;
		try {
			result = this.ordenPagosDetalleDao.findAll();
		} catch (RuntimeException re) {
			LOGGER.error(
					" Error al buscar todo ",
					re);
			throw re;
		}
		return result;
	}
	public  List<OrdenPagosDetalle >  obtenerOrdenPagosDetalleProcesoPorId(Long liquidacionId){ 
		LOGGER.debug("obtenerOrdenPagosDetalleProcesoPorId() identificadorBenId => {"+liquidacionId+"} ");
	    List<OrdenPagosDetalle > lista = new ArrayList<OrdenPagosDetalle>();
	    try {
	    	lista=ordenPagosDetalleDao.obtenerOrdenPagosDetalleProcesoPorId(liquidacionId);
	    } catch (RuntimeException re) {
	    	LOGGER.error("-- obtenerOrdenPagosDetalleProcesoPorId()", re);
	        throw re; 
	    }    
	    return lista;
	}
	
	public  List<OrdenPagosDetalle >  obtenerOrdenPagosDetalleGeneradoPorId(Long folio){ 
		LOGGER.debug("obtenerOrdenPagosDetalleGeneradoPorId() folio => {"+folio+"}");
	    List<OrdenPagosDetalle > lista = new ArrayList<OrdenPagosDetalle>();
	    try {
	    	lista=this.ordenPagosDetalleDao.obtenerOrdenPagosDetalleGeneradoPorId(folio);
	    } catch (RuntimeException re) {
	    	LOGGER.error("-- obtenerOrdenPagosDetalleGeneradoPorId()", re);
	        throw re; 
	    }    
	    return lista;
	}
	
	public Boolean excluirRecibosOrdenPagosDetallePorId(Long ordenPagoId ,Long folioId ){
		LOGGER.debug("excluirRecibosobtenerOrdenPagosDetallePorId() OrdenPagoId => {"+ordenPagoId+"} reciboId => {"+folioId+"} ");
			try{				
				return this.ordenPagosDetalleDao.excluirRecibosOrdenPagosDetallePorId(ordenPagoId,folioId);			
			}catch (RuntimeException re) {
			LOGGER.error("-- excluirRecibosobtenerOrdenPagosDetallePorId()", re);
			throw re;
		}
	}
	public Boolean incluirRecibosOrdenPagosDetallePorId(Long ordenPagoId ,Long folioId ){
		LOGGER.debug("excluirRecibosobtenerOrdenPagosDetallePorId() OrdenPagoId => {"+ordenPagoId+"} folioId => {"+folioId+"} ");
		try{
			return this.ordenPagosDetalleDao.incluirRecibosOrdenPagosDetallePorId(ordenPagoId,folioId);
		} catch (RuntimeException re) {
			LOGGER.error("-- excluirRecibosobtenerOrdenPagosDetallePorId()", re);
			throw re;
		}
	}

	public Boolean actualizarEstatusOrdenPagosDetalle(Long folio){
		LOGGER.debug("actualizarEstatusOrdenPagosDetalle() OrdenPagoId => {"+folio+"}");
		try{
			return this.ordenPagosDetalleDao.actualizarEstatusOrdenPagosDetalle(folio);
		} catch (RuntimeException re) {
			LOGGER.error("-->actualizarEstatusOrdenPagosDetalle()", re);
			throw re;
		}		
	}
	
	public Boolean actualizarEstatusOrdenPagosDetalleAGenerado(Long folio){
		LOGGER.debug("actualizarEstatusOrdenPagosDetalle() OrdenPagoId => {"+folio+"}");
		try{
			return this.ordenPagosDetalleDao.actualizarEstatusOrdenPagosDetalleAGenerado(folio);
		} catch (RuntimeException re) {
			LOGGER.error("-->actualizarEstatusOrdenPagosDetalle()", re);
			throw re;
		}		
	}
	
	public List<OrdenPagosDetalle> obtenerHonorariosOrdenPagos(Long idParametro, String estatusRecibo){  
		LOGGER.debug("obtenerHonorariosOrdenPagos() idAgente => {"+idParametro+"}  estatusRecibo =>{"+estatusRecibo+"}");
	    List<OrdenPagosDetalle > lista = new ArrayList<OrdenPagosDetalle>();
	    try {
	    	lista = ordenPagosDetalleDao.obtenerHonorariosOrdenPagos(idParametro,estatusRecibo);      
	    } catch (RuntimeException re) {
	    	LOGGER.error("--error obtenerHonorariosOrdenPagos()", re);
	        throw re; 
	    }	    
	    return lista;
	}
	
	public BigDecimal consultarPagoPorUtilidad(Long compensacionId, Long entidadPersonaId){		
		LOGGER.debug(">> consultarPagoPorUtilidad()");
		BigDecimal pago = null;		
		try{
			pago = ordenPagosDetalleDao.consultarPagoPorUtilidad(compensacionId,entidadPersonaId);			
		}catch(RuntimeException re){
			LOGGER.error("Información del Error", re);
			pago = BigDecimal.ZERO;
		}		
		LOGGER.debug("<< consultarPagoPorUtilidad()");		
		return pago;		
	}	

	public BigDecimal consultarProvisionPorUtilidad(Long compensacionId, Long entidadPersonaId){		
		LOGGER.debug(">> consultarProvisionPorUtilidad()");		
		BigDecimal provision = null;		
		try{			
			provision = ordenPagosDetalleDao.consultarProvisionPorUtilidad(compensacionId,entidadPersonaId);			
		}catch (Exception e) {
			LOGGER.error("Información del Error", e);
			provision = BigDecimal.ZERO;
		}		
		LOGGER.debug("<< consultarProvisionPorUtilidad()");
		return provision;
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String guardarSiniestralidad(String datos) {	
		return ordenPagosDetalleDao.guardarSiniestralidad(datos);
	}
}