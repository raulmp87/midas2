package mx.com.afirme.midas2.service.impl.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.siniestros.cabina.reportecabina.MovimientoSiniestroDao;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoDocumentoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.MovimientoSiniestroDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.log4j.Logger;

@Stateless
public class MovimientoSiniestroServiceImpl implements MovimientoSiniestroService{

	@Resource
	private javax.ejb.SessionContext sessionContext;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private MovimientoSiniestroDao movimientoSiniestroDao;
	
	@EJB	 
	private OrdenCompraService ordenCompraService;	
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	private static final Logger log = Logger.getLogger(MovimientoSiniestroServiceImpl.class);

	
	@Override
	public BigDecimal obtenerImporteMovimientos(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo,String claveSubCalculo, TipoDocumentoMovimiento tipoDocumento, 
			TipoMovimiento tipoMovimiento, CausaMovimiento causaMovimiento, Date fechaUltimoMovimiento) {
		
		return movimientoSiniestroDao.obtenerImporteMovimientos(reporteCabinaId, coberturaId, estimacionId, claveTipoCalculo, 
				claveSubCalculo, tipoDocumento, tipoMovimiento, causaMovimiento, fechaUltimoMovimiento);
	}
	

	@Override
	public BigDecimal obtenerReserva(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo,String claveSubCalculo,  Date fechaCreacionUltimoMovimiento, Boolean obtenerDisponible) {
		
		BigDecimal montoEstimacion = obtenerMontoEstimacionActual(reporteCabinaId, coberturaId, estimacionId, 
				claveTipoCalculo, claveSubCalculo, fechaCreacionUltimoMovimiento);
		
		BigDecimal montosPago = ordenCompraService.obtenerTotalesOrdenesCompra(reporteCabinaId, coberturaId, estimacionId, claveTipoCalculo, 
				claveSubCalculo, fechaCreacionUltimoMovimiento, OrdenCompraService.TIPO_AFECTACION_RESERVA, OrdenCompraService.ESTATUS_PAGADA);
						
		BigDecimal reserva = montoEstimacion.subtract(montosPago);		
		
		if (obtenerDisponible) {
					
			BigDecimal totalAutorizado = ordenCompraService.obtenerTotalesOrdenesCompra(reporteCabinaId, coberturaId, estimacionId, claveTipoCalculo, 
					claveSubCalculo, fechaCreacionUltimoMovimiento, OrdenCompraService.TIPO_AFECTACION_RESERVA, OrdenCompraService.ESTATUS_AUTORIZADA);
			
			BigDecimal totalTramite = ordenCompraService.obtenerTotalesOrdenesCompra(reporteCabinaId, coberturaId, estimacionId, claveTipoCalculo, 
					claveSubCalculo, fechaCreacionUltimoMovimiento, OrdenCompraService.TIPO_AFECTACION_RESERVA, OrdenCompraService.ESTATUS_TRAMITE);
			
			BigDecimal totalAsociada = ordenCompraService.obtenerTotalesOrdenesCompra(reporteCabinaId, coberturaId, estimacionId, claveTipoCalculo, 
					claveSubCalculo, fechaCreacionUltimoMovimiento, OrdenCompraService.TIPO_AFECTACION_RESERVA, OrdenCompraService.ESTATUS_ASOCIADA);
			
			reserva = reserva.subtract(totalAutorizado.add(totalTramite.add(totalAsociada)));
			
		}		
		
		return reserva ;
	}

	@Override
	public BigDecimal obtenerMontoEstimacionActual(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo,String claveSubCalculo,  Date fechaCreacionUltimoMovimiento) {
		
		BigDecimal monto = this.obtenerImporteMovimientos(reporteCabinaId, coberturaId, estimacionId, 
				claveTipoCalculo, claveSubCalculo, TipoDocumentoMovimiento.ESTIMACION, null, null, null);
		
		if (monto == null) {
			monto = BigDecimal.ZERO;
		}
		return monto;
	}
	
	@Override
	public BigDecimal obtenerMontoPagos(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo,String claveSubCalculo,  Date fechaCreacionUltimoMovimiento) {
		BigDecimal monto = this.obtenerImporteMovimientos(reporteCabinaId, coberturaId, estimacionId, 
				claveTipoCalculo, claveSubCalculo, TipoDocumentoMovimiento.PAGO, null, null, null);
		
		if (monto == null) {
			monto = BigDecimal.ZERO;
		}
		return monto;
	}
	
	@Override
	public BigDecimal obtenerMontoIngresos(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo,String claveSubCalculo,  Date fechaCreacionUltimoMovimiento) {
		
		BigDecimal monto = this.obtenerImporteMovimientos(reporteCabinaId, coberturaId, estimacionId, 
				claveTipoCalculo, claveSubCalculo, TipoDocumentoMovimiento.INGRESO, null, null, null);
		
		if (monto == null) {
			monto = BigDecimal.ZERO;
		}
		return monto;
	}
	
	@Override
	public BigDecimal obtenerMontoEstimacionReporte(Long reporteCabinaId) {
		return this.obtenerMontoEstimacionActual(reporteCabinaId, null, null, null, null, null);
	}
	
	@Override
	public BigDecimal obtenerMontoEstimacionCobertura(Long coberturaId, String claveSubCalculo) {
		return this.obtenerMontoEstimacionActual(null, coberturaId, null, null, claveSubCalculo, null);
	}
	
	@Override
	public BigDecimal obtenerMontoEstimacionAfectacion(Long estimacionId) {
		return this.obtenerMontoEstimacionActual(null, null, estimacionId, null, null, null);
	}	
	
	@Override
	public BigDecimal obtenerMontoPagosReporte(Long reporteCabinaId) {
		return this.obtenerMontoPagos(reporteCabinaId, null, null, null, null, null);
	}
	
	@Override
	public BigDecimal obtenerMontoPagosCobertura(Long coberturaId, String claveSubCalculo) {
		return this.obtenerMontoPagos(null, coberturaId, null, claveSubCalculo, null, null);
	}
	
	
	@Override
	public BigDecimal obtenerMontoPagosAfectacion(Long estimacionId) {
		return this.obtenerMontoPagos(null, null, estimacionId, null, null, null);
	}
	
	@Override
	public BigDecimal obtenerMontoIngresosReporte(Long reporteCabinaId) {
		return this.obtenerMontoIngresos(reporteCabinaId, null, null, null, null, null);
	}
	
	@Override
	public BigDecimal obtenerMontoIngresosCobertura(Long coberturaId, String claveSubCalculo) {
		return this.obtenerMontoIngresos(null, coberturaId, null, claveSubCalculo, null, null);
	}
	
	@Override
	public BigDecimal obtenerMontoIngresosAfectacion(Long estimacionId) {
		return this.obtenerMontoIngresos(null, null, estimacionId, null, null, null);
	}
	
	@Override
	public BigDecimal obtenerReservaReporte(Long reporteCabinaId,  Boolean obtenerDisponible) {
		return this.obtenerReserva(reporteCabinaId, null, null, null, null, null, obtenerDisponible);
	}
	
	@Override
	public BigDecimal obtenerReservaCobertura(Long coberturaId, String claveSubCalculo, Boolean obtenerDisponible) {
		return this.obtenerReserva(null, coberturaId, null, claveSubCalculo, null, null, obtenerDisponible);
	}
	
	@Override
	public BigDecimal obtenerReservaAfectacion(Long estimacionId, Boolean obtenerDisponible) {
		return this.obtenerReserva(null, null, estimacionId, null, null, null, obtenerDisponible);
	}

	@Override
	public MovimientoCoberturaSiniestro generarAjusteReserva(Long estimacionId , BigDecimal reserva,
			CausaMovimiento causaMovimiento, String codigoUsuario) {
		
		MovimientoSiniestroService processor = this.sessionContext.getBusinessObject(MovimientoSiniestroService.class);	
		
		BigDecimal importe = null;
		
		TipoMovimiento tipoMovimiento = null;
		
		BigDecimal reservaActual =  obtenerReservaAfectacion(estimacionId, Boolean.TRUE);
		

		if (reserva.compareTo(reservaActual) == 1) {
			// si nueva reserva es mayor
			importe= reserva.subtract(reservaActual);
			if (importe.signum() == -1) {
				importe = importe.multiply(new BigDecimal(-1));
			}
			tipoMovimiento = TipoMovimiento.AJUSTE_AUMENTO;
		
		} else if (reserva.compareTo(reservaActual) ==-1) {
			 // si nueva reserva es menor
			importe = reservaActual.subtract(reserva);
			if (importe.signum() ==- 1) {
				importe = importe.multiply(new BigDecimal (-1));
			}
			tipoMovimiento = TipoMovimiento.AJUSTE_DISMINUCION;
			importe = importe.negate();
			
		} else {
			return null;
		}		
		
		MovimientoCoberturaSiniestro movimiento = processor.generarMovimiento(estimacionId, importe, 
				TipoDocumentoMovimiento.ESTIMACION, tipoMovimiento, causaMovimiento, codigoUsuario, null);
		
		if(movimiento != null){
			invocarInterfazContable(movimiento);
		}
		
		return movimiento;
	}
	

	@Override
	public MovimientoCoberturaSiniestro  generarAjusteEstimacion(Long estimacionId , BigDecimal importe, 
			CausaMovimiento causaMovimiento, String codigoUsuario) {
		
		TipoMovimiento tipoMovimiento = null;
		
		MovimientoSiniestroService processor = this.sessionContext.getBusinessObject(MovimientoSiniestroService.class);	
		
		BigDecimal estimacionActual = obtenerMontoEstimacionAfectacion(estimacionId);


		List<MovimientoCoberturaSiniestro> movimientos = 
			movimientoSiniestroDao.obtenerMovimientos(null, null, estimacionId, null, null, TipoDocumentoMovimiento.ESTIMACION, null, null, null);


		if (estimacionActual.compareTo(BigDecimal.ZERO) != 0) {
			importe = importe.subtract(estimacionActual);
		} 
		
		if (movimientos == null || movimientos.size() == 0) {
			tipoMovimiento = TipoMovimiento.ESTIMACION_ORIGINAL;
		} else {
			if (importe.compareTo(BigDecimal.ZERO) > 0) {
				tipoMovimiento = TipoMovimiento.AJUSTE_AUMENTO;
			} else if (importe.compareTo(BigDecimal.ZERO) < 0) {
				tipoMovimiento = TipoMovimiento.AJUSTE_DISMINUCION;
			}
		}
		
		MovimientoCoberturaSiniestro movimiento = processor.generarMovimiento(estimacionId, importe, 
				TipoDocumentoMovimiento.ESTIMACION, tipoMovimiento, causaMovimiento, codigoUsuario, null);
		
		if(movimiento != null){
			invocarInterfazContable(movimiento);
		}
		
		return movimiento;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public MovimientoCoberturaSiniestro  generarMovimiento(Long idEstimacionCobertura , BigDecimal importeMovimiento, 
			TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, CausaMovimiento causaMovimiento, 
			String codigoUsuario, String descripcion) {
		MovimientoCoberturaSiniestro movimiento = null;
		//Si el tipo de movimiento es <code>null</null> significa que no se ha detectado un aumento o disminucion y por lo tanto no se genera un movimiento
		if(tipoMovimiento != null){			
			movimiento = new MovimientoCoberturaSiniestro();
			if(idEstimacionCobertura!=null){
				List<EstimacionCoberturaReporteCabina> estimaciones = this.entidadService.findByProperty(EstimacionCoberturaReporteCabina.class, "id", idEstimacionCobertura);
				EstimacionCoberturaReporteCabina estimacion = estimaciones.get(0);
				movimiento.setEstimacionCobertura(estimacion);
			}
			movimiento.setCausaMovimiento(causaMovimiento.toString());
			movimiento.setImporteMovimiento(importeMovimiento);
			movimiento.setTipoMovimiento(tipoMovimiento.toString());
			movimiento.setTipoDocumento(tipoDocumento.toString());
			if (StringUtil.isEmpty(codigoUsuario)) {
				codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
			}
			
			if (!StringUtil.isEmpty(descripcion)) {
				movimiento.setDescripcion(descripcion);
			}
			movimiento.setCodigoUsuarioCreacion(codigoUsuario);
		
			movimiento.setFechaCreacion(new Date());
			movimiento = entidadService.save(movimiento);
		}
		return movimiento;
	}


	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void invocarInterfazContable(MovimientoCoberturaSiniestro movimiento){
		try {
			MovimientoCoberturaSiniestro movimientoresultado = movimientoSiniestroDao.contabilizaReserva(movimiento.getId());
			movimiento.setLoteContable(movimientoresultado.getLoteContable());
			movimiento.setFechaContabilizacion(movimientoresultado.getFechaContabilizacion());
			if (movimiento.getLoteContable() != null && movimiento.getFechaContabilizacion() != null) {
				movimiento.setContabilizado(Boolean.TRUE);
			}
			entidadService.save(movimiento);
		} catch(Exception ex) {
			log.error("***** ERROR AL LLAMAR INTERFAZ CONTABLE ***** ", ex);
		}	
	}

	@Override
	public MovimientoSiniestro  generarMovimientoGA(Long idReporteCabina , Long idOrdenCompra, BigDecimal importeMovimiento, 
			TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, CausaMovimiento causaMovimiento, 
			String codigoUsuario, String descripcion){
		
		MovimientoSiniestro movimiento = new MovimientoSiniestro();
		
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class, idReporteCabina);		
		movimiento.setReporteCabina(reporte);
		movimiento.setCausaMovimiento(causaMovimiento.toString());
		movimiento.setImporteMovimiento(importeMovimiento);
		movimiento.setTipoMovimiento(tipoMovimiento.toString());
		movimiento.setTipoDocumento(tipoDocumento.toString());
		if (StringUtil.isEmpty(codigoUsuario)) {
			codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		}
		
		if (idOrdenCompra != null) {
			OrdenCompra ordenCompra = entidadService.findById(OrdenCompra.class, idOrdenCompra);
			movimiento.setOrdenCompra(ordenCompra);
		}
		
		if (!StringUtil.isEmpty(descripcion)) {
			movimiento.setDescripcion(descripcion);
		}
		movimiento.setCodigoUsuarioCreacion(codigoUsuario);
		movimiento.setFechaCreacion(new Date());
		movimiento = entidadService.save(movimiento);
		return movimiento;
	}
	

	@Override
	public List<MovimientoSiniestroDTO> obtenerHistoricoMovimientos(MovimientoSiniestroDTO filtroBusqueda){
		List<MovimientoSiniestroDTO> listaMovimientosDTO = movimientoSiniestroDao.obtenerMovimientos(filtroBusqueda);
			
		return listaMovimientosDTO;
	}
	

	@Override
	public List<MovimientoSiniestroDTO> obtenerHistoricoMovimientosRobos(MovimientoSiniestroDTO filtroBusqueda){
		filtroBusqueda.setClaveTipoCalculo(ClaveTipoCalculo.ROBO_TOTAL.getValue());
		
		List<MovimientoSiniestroDTO> listaMovimientosDTO = movimientoSiniestroDao.obtenerMovimientos(filtroBusqueda);
			
		return listaMovimientosDTO;
	}

	@Override
	public List<MovimientoSiniestroDTO> obtenerHistoricoMovimientosGA(MovimientoSiniestroDTO filtroBusqueda) {
		List<MovimientoSiniestroDTO> listaMovimientosDTO = movimientoSiniestroDao.obtenerMovimientosGA(filtroBusqueda);
		
		return listaMovimientosDTO;
	}


	@Override
	public BigDecimal obtenerMontoEstimacionGA(Long reporteCabinaId) {
		return this.obtenerImporteMovimientosGA(reporteCabinaId, null, TipoDocumentoMovimiento.ESTIMACION, null, null, null);
	}


	@Override
	public BigDecimal obtenerMontoPagosGA(Long reporteCabinaId) {
		return this.obtenerImporteMovimientosGA(reporteCabinaId, null, TipoDocumentoMovimiento.PAGO, null, null, null);
	}


	@Override
	public BigDecimal obtenerMontoIngresosGA(Long reporteCabinaId) {
		return this.obtenerImporteMovimientosGA(reporteCabinaId, null, TipoDocumentoMovimiento.INGRESO, null, null, null);
	}


	@Override
	public BigDecimal obtenerImporteMovimientosGA(Long reporteCabinaId, Long ordenCompraId, 
			TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, CausaMovimiento causaMovimiento, Date fechaUltimoMovimiento) {
		
		return movimientoSiniestroDao.obtenerImporteMovimientosGA(reporteCabinaId, ordenCompraId, tipoDocumento, 
				tipoMovimiento, causaMovimiento, fechaUltimoMovimiento);
	}

	@Override
	public Map<String,String> obtenerUsuariosMovSiniestros(){
		Map<String,String> usuarios = movimientoSiniestroDao.obtenerUsuariosMovSiniestros();
		return usuarios;
	}
	
	@Override
	public BigDecimal obtenerImporteMovimientos(MovimientoSiniestroDTO filtroBusqueda){
		BigDecimal importe = null;
		importe = movimientoSiniestroDao.obtenerImporteMovimientos(filtroBusqueda);
		
		return importe;
	}
	
	@Override
	public BigDecimal obtenerImporteMovimientosGA(MovimientoSiniestroDTO filtroBusqueda){
		BigDecimal importe = null;
		importe = movimientoSiniestroDao.obtenerImporteMovimientosGA(filtroBusqueda);
		return importe;
	}
}
