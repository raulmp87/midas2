package mx.com.afirme.midas2.service.impl.bonos;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas2.dao.bonos.ConfigBonosDao;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.bonos.BonoExcepcionPoliza;
import mx.com.afirme.midas2.domain.bonos.BonoExclusionTipoCedula;
import mx.com.afirme.midas2.domain.bonos.BonoExclusionesTipoBono;
import mx.com.afirme.midas2.domain.bonos.CalculoBono;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoAplicaAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoAplicaPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoCentroOperacion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoCobertura;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoEjecutivo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoExcepAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoExcepNegocio;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoGerencia;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoLineaVenta;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPoliza;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPrioridad;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoProducto;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRamo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoAplica;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoClaveAmis;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSeccion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSituacion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSubramo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoTipoAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoTipoPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.bonos.DetalleCalculoBono;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoCatalogoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.bonos.ConfigBonosDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.fuerzaventa.ConfigBonosNegView;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.ExcepcionesPolizaView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.fuerzaventa.GuiaHonorariosAgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.EnvioReporteCalculoBonoMensualDTO;
import mx.com.afirme.midas2.dto.reportesAgente.EnvioReporteCalculoBonoMensualDTO.EstatusEnvioRepCBMensual;
import mx.com.afirme.midas2.dto.reportesAgente.EnvioReporteCalculoBonoMensualDTO.TipoBeneficiarioCBMensual;
import mx.com.afirme.midas2.dto.reportesAgente.EnvioReporteCalculoBonoMensualDTO.TipoEnvioRepCBMensual;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.bonos.BonoExclusionTipoCedulaService;
import mx.com.afirme.midas2.service.bonos.BonoExclusionesTipoBonoService;
import mx.com.afirme.midas2.service.bonos.ConfigBonoExcepAgenteService;
import mx.com.afirme.midas2.service.bonos.ConfigBonoExcepNegocioService;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.DocumentoAgenteService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.threads.MailThreadSupport;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.utils.MailServiceSupport;
import net.sf.jasperreports.engine.JRException;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class ConfigBonosServiceImpl implements ConfigBonosService{	
	
	public  static final Logger log = Logger.getLogger(ConfigBonosServiceImpl.class);
	
	private static final String  TIPO_DOCUMENTO = "tipoDocumento";
	
	@Override
	public void deleteConfiguration(ConfigBonos config) throws Exception {
		configBonosDao.deleteConfiguration(config);
	}
	
	@EJB(beanName = "UsuarioServiceDelegate")
	public UsuarioService usuarioService;

	@Override
	public List<ConfigBonos> findByFilters(ConfigBonos config) throws Exception {
		return configBonosDao.findByFilters(config);
	}

	@Override
	public List<ConfigBonosDTO> findByFiltersView(ConfigBonosDTO config){
			
		return configBonosDao.findByFiltersView(config);
	}

	@Override
	public List<ValorCatalogoAgentes> getCatalogoModoEjecucion()throws Exception {
		return configBonosDao.getCatalogoModoEjecucion();
	}

	@Override
	public List<ValorCatalogoAgentes> getCatalogoPeriodo() throws Exception {
		return configBonosDao.getCatalogoPeriodo();
	}

	@Override
	public List<CentroOperacionView> getCentroOperacionList() throws Exception {
		return configBonosDao.getCentroOperacionList();
	}
	
	@Override
	public List<GenericaAgentesView> getCoberturasPorLineasNegocio(List<ConfigBonoSeccion> lineasNegocio) throws Exception{
		return configBonosDao.getCoberturasPorLineasNegocio(lineasNegocio);
	}

	@Override
	public List<EjecutivoView> getEjecutivoList() throws Exception {
		return configBonosDao.getEjecutivoList();
	}

	@Override
	public List<ConfigBonoEjecutivo> getEjecutivosPorConfiguracion(ConfigBonos config) throws Exception {
		return configBonosDao.getEjecutivosPorConfiguracion(config);
	}

	@Override
	public List<GerenciaView> getGerenciaList() throws Exception {
		return configBonosDao.getGerenciaList();
	}

	@Override
	public List<ConfigBonoGerencia> getGerenciasPorConfiguracion(ConfigBonos config) throws Exception {
		return configBonosDao.getGerenciasPorConfiguracion(config);
	}

	@Override
	public List<ValorCatalogoAgentes> getLineasVenta() throws Exception {
		return configBonosDao.getLineasVenta();
	}

	@Override
	public List<ValorCatalogoAgentes> getPrioridadList() throws Exception {
		return configBonosDao.getPrioridadList();
	}

	@Override
	public List<ConfigBonoPrioridad> getPrioridadesPorConfiguracion(ConfigBonos config) throws Exception {
		return configBonosDao.getPrioridadesPorConfiguracion(config);
	}

	@Override
	public List<GenericaAgentesView> getProductosPorLineaVenta(String list) throws Exception {
		return configBonosDao.getProductosPorLineaVenta(list);
	}

	@Override
	public List<PromotoriaView> getPromotoriaList() throws Exception {
		return configBonosDao.getPromotoriaList();
	}

	@Override
	public List<ConfigBonoPromotoria> getPromotoriasPorConfiguracion(ConfigBonos config) throws Exception {
		return configBonosDao.getPromotoriasPorConfiguracion(config);
	}

	@Override
	public List<GenericaAgentesView> getRamosPorProductos(List<ConfigBonoProducto> productos)throws Exception {
		return configBonosDao.getRamosPorProductos(productos);
	}

	@Override
	public List<ValorCatalogoAgentes> getSituacionList() throws Exception {
		return configBonosDao.getSituacionList();
	}

	@Override
	public List<ConfigBonoSituacion> getSituacionesPorConfiguracion(ConfigBonos config) throws Exception {
		return configBonosDao.getSituacionesPorConfiguracion(config);
	}

	@Override
	public List<GenericaAgentesView> getSubramosPorRamos(List<ConfigBonoRamo> ramos)throws Exception {
		return configBonosDao.getSubramosPorRamos(ramos);
	}

	@Override
	public List<ValorCatalogoAgentes> getTipoDeAgenteList() throws Exception {
		return configBonosDao.getTipoDeAgenteList();
	}

	@Override
	public List<ValorCatalogoAgentes> getTipoPromotoriaList() throws Exception {
		return configBonosDao.getTipoPromotoriaList();
	}

	@Override
	public List<ConfigBonoTipoAgente> getTiposAgentePorConfiguracion(ConfigBonos config) throws Exception {
		return configBonosDao.getTiposAgentePorConfiguracion(config);
	}

	@Override
	public List<ConfigBonoTipoPromotoria> getTiposPromotoriaPorConfiguracion(ConfigBonos config) throws Exception {
		return configBonosDao.getTiposPromotoriaPorConfiguracion(config);
	}

	@Override
	public ConfigBonos loadById(ConfigBonos config) throws Exception {
		return configBonosDao.loadById(config);
	}
	
	@Override
	public ConfigBonos saveConfiguration(ConfigBonos config) {
		
		if (config == null) {
			
			throw new RuntimeException("Favor de proporcionar la configuracion a guardar");
			
		}
		   
		if (config.getModoAplicacion() != null && config.getModoAplicacion().getId()==null) config.setModoAplicacion(null);
		if (config.getPeriodoComparacion() != null && config.getPeriodoComparacion().getId()==null) config.setPeriodoComparacion(null);
		if (config.getProduccionSobre().getId() != null && config.getProduccionSobre().getId()==null) config.setProduccionSobre(null);
		if (config.getSiniestralidadSobre() != null && config.getSiniestralidadSobre().getId()==null) config.setSiniestralidadSobre(null);
				
		config = entidadService.save(config);
					
		saveConfigRelationships(config);
		
		//TODO revisar si quedara definitivamente el campo IDTCCLASIFICACIONBONO que se actualiza en la siguiente funcion.
		try{
			configBonosDao.actualizarClasificacionConfigBono(config.getId()); 
		}catch(Exception e)
		{   
			throw new RuntimeException("Error al intentar actualizar la clasificacion del bono", e);
		}
		config = entidadService.evictAndFindById(ConfigBonos.class, config.getId());
		
		return config;
		
	}
		
	@Override
	public List<GenericaAgentesView> getProductosPorLineaVentaView(String lineasVenta) throws Exception{
		return configBonosDao.getProductosPorLineaVentaView(lineasVenta);
	}
	
	@Override
	public List<GenericaAgentesView> getRamosPorProductosView(String productos) throws Exception {
		return configBonosDao.getRamosPorProductosView(productos);
	}
	
	@Override
	public List<GenericaAgentesView> getSubramosPorRamoView(String ramos) throws Exception {
		return configBonosDao.getSubramosPorRamoView(ramos);
	}

	@Override
	public List<GenericaAgentesView> getLineasNegocioPorRamos(List<ConfigBonoRamo> ramo) throws Exception {
		return configBonosDao.getLineasNegocioPorRamos(ramo);
	}
	
	@Override
	public List<GenericaAgentesView> getCoberturasPorLineasNegocioView(String lineasNegocio) throws Exception {
		return configBonosDao.getCoberturasPorLineasNegocioView(lineasNegocio);
	}

	@Override
	public List<GenericaAgentesView> getLineaNegocioPorRamoView(String ramos) throws Exception {
		return configBonosDao.getLineaNegocioPorRamoView(ramos);
	}

	@Override
	public List<GerenciaView> getGerenciasConCentrosOperacionExcluyentes(List<Long> configGerencias,List<Long> configCentrosOperacion){
		return configBonosDao.getGerenciasConCentrosOperacionExcluyentes(configGerencias,configCentrosOperacion);
	}
	
	@Override
	public List<EjecutivoView> getEjecutivosConGerenciasExcluyentes(List<Long> configEjecutivos,List<Long> configGerencias,List<Long> configCentros){
		return configBonosDao.getEjecutivosConGerenciasExcluyentes(configEjecutivos,configGerencias,configCentros);
	}
	
	@Override
	public List<PromotoriaView> getPromotoriasConEjecutivosExcluyentes(List<Long> configPromotorias,List<Long> configEjecutivos,List<Long> configGerencias,List<Long> configCentros){
		return configBonosDao.getPromotoriasConEjecutivosExcluyentes(configPromotorias,configEjecutivos,configGerencias,configCentros);
	}
	
	public List<ConfigBonosNegView> getListNegociosDesasociado(ConfigBonosNegView filtro){
		return configBonosDao.getListNegociosDesasociado(filtro);
	}
	
	public List <ExcepcionesPolizaView> getPolizaExcepsiones(ExcepcionesPolizaView idPoliza, ConfigBonos configuracion){
		return configBonosDao.getPolizaExcepsiones(idPoliza, configuracion);
	}
	
	@Override
	public List<GenericaAgentesView> getProductos() throws Exception {
		return configBonosDao.getProductos();
	}

	@Override
	public List<GenericaAgentesView> getRamos() throws Exception {
		return configBonosDao.getRamos();
	}
	
	@Override
	public ExcepcionesPolizaView getInfoPoliza(ExcepcionesPolizaView poliza) throws Exception {
		return configBonosDao.getInfoPoliza(poliza);
	}

	@Override
	public List<ConfigBonoCobertura> getCoberturasPorConfiguracion(ConfigBonos config) throws Exception {
		return configBonosDao.getCoberturasPorConfiguracion(config);
	}

	@Override
	public List<ConfigBonoLineaVenta> getLineaVentaPorConfiguracion(ConfigBonos config) throws Exception {
		return configBonosDao.getLineaVentaPorConfiguracion(config);
	}
	
	@Override
	public List<GenericaAgentesView> getCoberturasRelacionados(ConfigBonos arg0)throws Exception {
		return configBonosDao.getCoberturasRelacionados(arg0);
	}

	@Override
	public List<GenericaAgentesView> getLineaNegociosRelacionados(ConfigBonos arg0) throws Exception {
		return configBonosDao.getLineaNegociosRelacionados(arg0);
	}

	@Override
	public List<ValorCatalogoAgentes> getLineasVentasRelacionadas(ConfigBonos arg0) throws Exception {
		return configBonosDao.getLineasVentasRelacionadas(arg0);
	}

	@Override
	public List<GenericaAgentesView> getProductoRelacionados(ConfigBonos arg0)throws Exception {
		return configBonosDao.getProductoRelacionados(arg0);
	}

	@Override
	public List<GenericaAgentesView> getRamoRelacionados(ConfigBonos arg0)throws Exception {
		return configBonosDao.getRamoRelacionados(arg0);
	}

	@Override
	public List<GenericaAgentesView> getSubRamoRelacionados(ConfigBonos arg0) throws Exception {
		return configBonosDao.getSubRamoRelacionados(arg0);
	}
	@Override
	public List<GenericaAgentesView> getProductosPorClaveLinea(String clave) throws Exception{
		return configBonosDao.getProductosPorClaveLinea(clave);
	}
	
	@Override
	public List<ExcepcionesPolizaView>getPolizaByIDConfigBono(Long id) throws Exception{
		return configBonosDao.getPolizaByIDConfigBono(id);
	}
	
	@Override
	public List<Agente>getAgentesByIdConfigBono(Long id) throws Exception{
		return configBonosDao.getAgentesByIdConfigBono(id);
	}
	
	@Override
	public List<ConfigBonoAplicaPromotoria>getAplicaPromotoriasByIdConfigBono(Long id)throws Exception{
		return configBonosDao.getAplicaPromotoriasByIdConfigBono(id);
	}
	
	@Override
	public List<Agente>getAplicaAgentesByIdConfigBono(Long id)throws Exception{
		return configBonosDao.getAplicaAgentesByIdConfigBono(id);
	}
	
	@Override
	public ConfigBonos clonarObjeto(ConfigBonos original) throws Exception {
		
		original = entidadService.getReference(ConfigBonos.class, original.getId());
		
		ConfigBonos copia = obtenerCopiaConfiguracion(original);

		copia = entidadService.save(copia);
				
		copiarExcepcionesConfiguracion(original, copia);

		saveConfigRelationships(copia);
		
		return copia;
		
	}
	
	@Override
	public ConfigBonos saveExclusiones(ConfigBonos exclusiones) throws Exception{
		return configBonosDao.saveExclusiones(exclusiones);
	}
	
	@Override
	public List<AgenteView> getAgentePorConfiguracion(ConfigBonos configuracion) throws Exception{
		return configBonosDao.getAgentePorConfiguracion(configuracion);
	}
	
	/**
	 * Obtiene los agentes resultantes de acuerdo a al configuracion de bonos seleccionada.
	 * @param configuracion
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<AgenteView> obtenerAgentesPorConfiguracionCapturada(ConfigBonos configuracion) throws Exception{
		return configBonosDao.obtenerAgentesPorConfiguracionCapturada(configuracion);
	}
	
	/**
	 * Metodo para obtener el listado de agentes por lista de centros de operacion seleccionados, ejecutivos, promotorias, etc...
	 * @param centroOperacionList
	 * @param ejecutivoList
	 * @param gerenciaList
	 * @param promotoriaList
	 * @param situacionesList
	 * @param tiposAgenteList
	 * @param tiposPromotoriaList
	 * @param agenteList
	 * @param prioridadesList
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<AgenteView> obtenerAgentesPorConfiguracionCapturada(List<CentroOperacion> centroOperacionList,
			List<Ejecutivo> ejecutivoList,
			List<Gerencia> gerenciaList,
			List<Promotoria> promotoriaList,
			List<ValorCatalogoAgentes> situacionesList,
			List<ValorCatalogoAgentes> tiposAgenteList,
			List<ValorCatalogoAgentes> tiposPromotoriaList,
			List<Agente> agenteList,
			List<ValorCatalogoAgentes> prioridadesList) throws Exception{
		return configBonosDao.obtenerAgentesPorConfiguracionCapturada(centroOperacionList,ejecutivoList,gerenciaList,promotoriaList,situacionesList,tiposAgenteList,tiposPromotoriaList,agenteList,prioridadesList);
	}
	
	@Override
	public List<PromotoriaView> obtenerPromotoriasPorConfiguracionCapturada(List<CentroOperacion> centroOperacionList,
			List<Ejecutivo> ejecutivoList,
			List<Gerencia> gerenciaList,
			List<Promotoria> promotoriaList,			
			List<ValorCatalogoAgentes> tiposPromotoriaList) throws Exception{
		
		return configBonosDao.obtenerPromotoriasPorConfiguracionCapturada(centroOperacionList,ejecutivoList,gerenciaList,promotoriaList,tiposPromotoriaList);
		
	}

	@Override
	public synchronized void enviarDocPorCorreoAgentes(List<Long> ids,Map<String,Object> genericMap, String formatoSalida) throws JRException{
		
		String sTipoDocumento = (String) genericMap.get(TIPO_DOCUMENTO);
		
		if(TransporteImpresionDTO.DOC_RECIBO_HONORARIOS.equalsIgnoreCase(sTipoDocumento)){
			
			List<GuiaHonorariosAgenteView> guiasHonorario = documentoAgenteService.obtenerGuiasHonorariosPorIds(ids);
			
			for (GuiaHonorariosAgenteView guia : guiasHonorario) {
				enviarGuiaHonorarioAgentePorCorreo(guia,genericMap);
			}
		}
		
		else{
			for (Long id : ids) {
				enviarDocPorCorreo(id,genericMap,formatoSalida);
			}
			
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Boolean enviarReporteCalculoBonoMensual(EnvioReporteCalculoBonoMensualDTO envio) throws Exception
	{
		Boolean resultado = Boolean.TRUE;   
		ValorCatalogoAgentes estatusFallido = new ValorCatalogoAgentes();
		ValorCatalogoAgentes estatusEnviado = new ValorCatalogoAgentes();
		
		try{			
			
			estatusEnviado = valorCatalogoAgentesService.obtenerElementoEspecifico(GrupoCatalogoAgente.GRUPO_CAT_AGTE_ESTATUS_ENVIO_REPCB_MENSUAL,
					EstatusEnvioRepCBMensual.ENVIADO.getValue());			
			
			estatusFallido = valorCatalogoAgentesService.obtenerElementoEspecifico(GrupoCatalogoAgente.GRUPO_CAT_AGTE_ESTATUS_ENVIO_REPCB_MENSUAL,
					EstatusEnvioRepCBMensual.ERROR.getValue());
			
			this.enviarReporteCalculoBonoMensual(envio.getIdBeneficiario(), 
					envio.getCalculoBono() != null?envio.getCalculoBono().getId():null,  
							envio.getCalculoBono() == null 
							&& envio.getPeriodo() != null? envio.getPeriodo().split("-")[0]:null, 
									envio.getCalculoBono() == null 
			    						&& envio.getPeriodo() != null? envio.getPeriodo().split("-")[1]:null,
									envio.getTipoBeneficiario().getId());
			
			envio.setEstatus(estatusEnviado);
			envio.setDetalleEstatus("ok");
			envio.setFechaEnvio(new Date());
			
		}catch(Exception e)
		{    
			log.error("Ha ocurrido un error al enviar la notificacion de Reporte Calculo Bono Mensual " + envio.getId(), e);
							
			envio.setEstatus(estatusFallido);  
			envio.setDetalleEstatus(ExceptionUtils.getStackTrace(e).substring(0, 1000));
			envio.setFechaEnvio(new Date());
			resultado = Boolean.FALSE;
			
		}finally
		{
			entidadService.save(envio);
		}
		
		return resultado;
	}	
	
	private void enviarReporteCalculoBonoMensual(Long idBeneficiario, Long idCalculoBono,  
			String anio, String mes, Long tipoBeneficiario)
	{  		
		List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
		ByteArrayAttachment file = new ByteArrayAttachment();
		TransporteImpresionDTO report = null;
		List<String> destinatarios = new ArrayList<String>();
		List<String> destinatariosCC = new ArrayList<String>();
		String titulo = "";
		String cuerpoMensaje = "";
		String periodo = "";
		String nombreBeneficiario = "";
		String nombreArchivo = "";
		String claveBeneficiario = String.valueOf(idBeneficiario);
		SimpleDateFormat sdf = new SimpleDateFormat("MMMM-yyyy",new Locale("es", "MX"));	
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
		
		if(idBeneficiario != null && 
				idCalculoBono != null)//REPORTE BONO PAGADO
		{
			report = generarPlantillaReporte.imprimirReporteAgenteCalculoBonoPagado(idBeneficiario, 
					GenerarPlantillaReporte.TIPO_USUARIO_AGENTE, idCalculoBono);
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("calculoBono.id", idCalculoBono);
			params.put("idBeneficiario", idBeneficiario);
			DetalleCalculoBono detalleCalculoBono = null;//entidadService.findByProperties(DetalleCalculoBono.class, params).get(0);
			String para = "";
			
			if(tipoBeneficiario.equals(GenerarPlantillaReporte.TIPO_BENEFICIARIO_AGENTE))
			{
				Agente agente = entidadService.findById(Agente.class, idBeneficiario);
				claveBeneficiario = String.valueOf(agente.getIdAgente());
				para =  agente.getPersona().getEmail();
				destinatariosCC.addAll(agenteMidasService.obtenerDireccionesCorreoSuperioresAgente(agente).values());
				params.put("calculoBono.id", idCalculoBono);
				params.put("idBeneficiario", agente.getIdAgente());
				detalleCalculoBono = entidadService.findByProperties(DetalleCalculoBono.class, params).get(0);
					
			}else if (tipoBeneficiario.equals(GenerarPlantillaReporte.TIPO_BENEFICIARIO_PROMOTOR))
			{
				Promotoria promotoria = entidadService.findById(Promotoria.class, idBeneficiario);
				para = promotoria.getCorreoElectronico();
				destinatariosCC.addAll(promotoriaService.obtenerDireccionesCorreoSuperioresPromotoria(promotoria).values());
				params.put("calculoBono.id", idCalculoBono);
				params.put("idBeneficiario", promotoria.getIdPromotoria());
				detalleCalculoBono = entidadService.findByProperties(DetalleCalculoBono.class, params).get(0);
			}
			
			//para = "orlando.abasta@afirme.com"; //TEST TODO Remover
			
			nombreBeneficiario = detalleCalculoBono.getBeneficiario();			
			
			titulo = "Pago de Bono";
			nombreArchivo = "Reporte Bono Mensual";
			periodo = sdf.format(detalleCalculoBono.getFechaAplicacion());
			sdf = new SimpleDateFormat("dd/MMMM/yyyy", new Locale("es", "MX"));
			String fechaAplicacion = sdf.format(detalleCalculoBono.getFechaAplicacion());
			nombreArchivo = "Reporte Bono Pagado " + claveBeneficiario + ".pdf" ;
			String ligaED = "https://www.segurosafirme.com.mx/SeycosPortal/";
			
			destinatarios.add(para);
			
			cuerpoMensaje = Utilerias.getMensajeRecurso(
					SistemaPersistencia.ARCHIVO_RECURSOS, "midas.agente.reportes.reporteCalculoBonoMensual.bonoPagado.correo.cuerpo",
					nombreBeneficiario,
					currencyFormatter.format(detalleCalculoBono.getBonoPagar()),
					fechaAplicacion,
					periodo,
					ligaED);
			
		}else if(idBeneficiario != null && anio != null 
				&& !anio.isEmpty() && mes != null && !mes.isEmpty() 
				&& tipoBeneficiario != null)//REPORTE BONOS PERIODO
		{
			report = generarPlantillaReporte.imprimirReporteAgenteCalculoBonoMensual(idBeneficiario, 
					GenerarPlantillaReporte.TIPO_USUARIO_AGENTE, 
					anio, mes, tipoBeneficiario);
			
			titulo = "Resumen de Pago de Bonos";
			periodo = sdf.format(Utilerias.obtenerFechaDeCadena("01/"+mes+"/"+anio));			
			
			if(tipoBeneficiario.equals(GenerarPlantillaReporte.TIPO_BENEFICIARIO_AGENTE))
			{
				Agente agente = entidadService.findById(Agente.class, idBeneficiario);
				claveBeneficiario = String.valueOf(agente.getIdAgente());
				String para = agente.getPersona().getEmail();
				//para = "orlando.abasta@afirme.com"; //TEST TODO Remover
				destinatarios.add(para);
				
				//destinatariosCC = null;//TEST TODO Remover
				nombreBeneficiario = agente.getPersona().getNombreCompleto();
				
			}else if(tipoBeneficiario.equals(GenerarPlantillaReporte.TIPO_BENEFICIARIO_PROMOTOR))
			{
				Promotoria promotoria = entidadService.findById(Promotoria.class, idBeneficiario);					
				String para = promotoria.getCorreoElectronico();
				destinatarios.add(para);
				
				nombreBeneficiario = promotoria.getDescripcion();
			}
			
			nombreArchivo = "Reporte Bono Mensual_ " + claveBeneficiario + "_.pdf";
			
			cuerpoMensaje = Utilerias.getMensajeRecurso(
					SistemaPersistencia.ARCHIVO_RECURSOS, "midas.agente.reportes.reporteCalculoBonoMensual.resumenPagoBonos.correo.cuerpo",
					nombreBeneficiario,
					periodo
					);		
						
		}else
		{
			throw new IllegalArgumentException("Error los parametros proporcionados estan incompletos.");
		}
		
		file.setTipoArchivo(ByteArrayAttachment.TipoArchivo.PDF);
		file.setNombreArchivo(nombreArchivo);
		file.setContenidoArchivo(report.getByteArray());
		adjuntos.add(file);		
		
		Map<String, String> mapInlineImages = new HashMap<String, String>();
		mapInlineImages.put("image1", sistemaContext.getDirectorioImagenesParametroGlobal() +"Cintillo_Seguros.jpg");
		
		mailService.sendMessage(destinatarios, titulo, cuerpoMensaje,
				"administrador.midas@afirme.com", null, adjuntos, destinatariosCC, null, mapInlineImages);
	}
	
	@Override
	public void registrarEnvioNotificacionRepCalculoBonoMensual(Long idBeneficiario, Long idCalculoBono,  
			String anio, String mes, TipoBeneficiarioCBMensual enumTipoBeneficiario, TipoEnvioRepCBMensual enumTipoEnvio, 
			EstatusEnvioRepCBMensual enumEstatusEnvio, 
			String detalleEstatus, Date fechaCreacion, Date fechaEnvio) throws Exception
	{
		EnvioReporteCalculoBonoMensualDTO envio = new EnvioReporteCalculoBonoMensualDTO();
		
		if(idCalculoBono != null)
		{
			CalculoBono calculoBono = entidadService.findById(CalculoBono.class, idCalculoBono);
			envio.setCalculoBono(calculoBono);
		}		
		
		ValorCatalogoAgentes tipoBeneficiario = new ValorCatalogoAgentes();
		tipoBeneficiario = valorCatalogoAgentesService.obtenerElementoEspecifico(GrupoCatalogoAgente.GRUPO_CAT_AGTE_TIPOS_BENEFICIARIO, enumTipoBeneficiario.getValue());
		
		ValorCatalogoAgentes tipoEnvio = new ValorCatalogoAgentes();
		tipoEnvio = valorCatalogoAgentesService.obtenerElementoEspecifico(GrupoCatalogoAgente.GRUPO_CAT_AGTE_MODOS_EJECUCION_COMISIONES, enumTipoEnvio.getValue());
		
		ValorCatalogoAgentes estatus = new ValorCatalogoAgentes();	
		estatus = valorCatalogoAgentesService.obtenerElementoEspecifico(GrupoCatalogoAgente.GRUPO_CAT_AGTE_ESTATUS_ENVIO_REPCB_MENSUAL,enumEstatusEnvio.getValue());		
		
		envio.setIdBeneficiario(idBeneficiario);
		envio.setPeriodo(anio + "-" + StringUtils.leftPad(mes, 2, "0"));
		envio.setTipoBeneficiario(tipoBeneficiario);
		envio.setTipoEnvio(tipoEnvio);
		envio.setEstatus(estatus);		
		envio.setDetalleEstatus(detalleEstatus);
		envio.setFechaCreacion(fechaCreacion);
		envio.setFechaEnvio(fechaEnvio);
		
		entidadService.save(envio);				
	}
		
	@Override
	public void registrarEnviosNotificacionRepCalculoBonoMensual(List<Long> idsBeneficiarios, Long idCalculoBono,  
			String anio, String mes, TipoBeneficiarioCBMensual enumTipoBeneficiario, TipoEnvioRepCBMensual enumTipoEnvio, 
			EstatusEnvioRepCBMensual enumEstatusEnvio, String detalleEstatus) throws Exception
	{
		for(Long idBeneficiario: idsBeneficiarios)
		{
			this.registrarEnvioNotificacionRepCalculoBonoMensual(idBeneficiario, idCalculoBono,  
					 anio, mes, enumTipoBeneficiario, enumTipoEnvio, enumEstatusEnvio, detalleEstatus, new Date(), null);
		}	
	}
	
	@Override
	public void enviarNotificacionesRepCalculoBonoMensualRegistradas(TipoEnvioRepCBMensual enumTipoEnvio, 
			EstatusEnvioRepCBMensual enumEstatusEnvio, Date filtroFecha) throws Exception
	{
		String errorEnvios = " No fue posible enviar el Reporte a los siguientes beneficiarios: ";  
		Map<String,Object> params = new HashMap<String,Object>();
		StringBuilder queryWhere = new StringBuilder();
		
		if(enumTipoEnvio != null)
		{
			ValorCatalogoAgentes tipoEnvio = new ValorCatalogoAgentes();	
			try {
				tipoEnvio = valorCatalogoAgentesService.obtenerElementoEspecifico(GrupoCatalogoAgente.GRUPO_CAT_AGTE_MODOS_EJECUCION_COMISIONES,enumTipoEnvio.getValue());
			} catch (Exception e) {
				
				log.error("Ocurrio un error al intentar obtener el catalogo tipo Envio", e);
			}
			
			params.put("tipoEnvio", tipoEnvio);
		}		
		
		if(enumEstatusEnvio != null)
		{
			ValorCatalogoAgentes estatus = new ValorCatalogoAgentes();	
			try {
				estatus = valorCatalogoAgentesService.obtenerElementoEspecifico(GrupoCatalogoAgente.GRUPO_CAT_AGTE_ESTATUS_ENVIO_REPCB_MENSUAL,enumEstatusEnvio.getValue());
			} catch (Exception e) {
				
				log.error("Ocurrio un error al intentar obtener el catalogo estatus envio", e);
			}
			
			params.put("estatus", estatus);			
		}
		
		if(filtroFecha != null && enumTipoEnvio.equals(TipoEnvioRepCBMensual.MANUAL))
		{
			String fechaTruncada = DateUtils.toString(filtroFecha,
			"yyyy-MM-dd HH:mm:ss");
			queryWhere.append(" and model.fechaCreacion >= '" + fechaTruncada + "'");
		}		
		
		List<EnvioReporteCalculoBonoMensualDTO> listaEnvios = 
			entidadService.findByColumnsAndProperties(EnvioReporteCalculoBonoMensualDTO.class, 
					"id,idBeneficiario,tipoBeneficiario,calculoBono,periodo,tipoEnvio,estatus,detalleEstatus,fechaCreacion,fechaEnvio", 
					params, null, queryWhere, null);		
				
		for(EnvioReporteCalculoBonoMensualDTO envio :listaEnvios)
		{   
			Boolean resultadoEnvio = configBonosService.enviarReporteCalculoBonoMensual(envio);
				
			if(resultadoEnvio == Boolean.FALSE)
			{  
				errorEnvios = errorEnvios.concat(" [" + envio.getIdBeneficiario() + ": " + envio.getDetalleEstatus() + "]");	
			}			
		}
		
		if(errorEnvios.contains("["))
		{
			throw new NegocioEJBExeption("", errorEnvios);			
		}
	}
	
	@Override
	public void enviarNotificacionesRepCalculoBonosMensual()
	{
		log.info("Ejecutando tarea enviarNotificacionesRepCalculoBonosMensual...");
		Date fechaFiltroEnvioNotificacion = new Date();		
		try{		
		this.enviarNotificacionesRepCalculoBonoMensualRegistradas(TipoEnvioRepCBMensual.AUTOMATICO, 
				EstatusEnvioRepCBMensual.REGISTRADO, fechaFiltroEnvioNotificacion);		
		}catch(Exception e)
		{
			log.error(e);
		}
	}

	@Override
	public void mailThreadMethodSupport(List<Long> ids,Map<String,Object> genericParams,String methodToExcecute, String formatSalida) {
		MailThreadSupport t = MailThreadSupport.getInstance();
		t.setIds(ids);
		t.setService(this);
		t.setMethodToExcecute(methodToExcecute);
		t.setGenericParams(genericParams);
		t.setFormatoSalida(formatSalida);
		Thread th = new Thread(t);
		th.start();
	}
	
	@Override
	public TransporteImpresionDTO obtenerDocumentoAgente(Long idAgente,
			String tipoDocumento, String anio, String mes, Boolean guiaLectura, Boolean detalle, Boolean MostrarColAdicionales,
			Boolean afirmeComunica, String afirmeComunicaText, String tipoSalidaArchivo, BigDecimal solicitudChequeId)throws JRException {
		
		TransporteImpresionDTO reporte = null;
		
		Agente agente = new Agente();
		agente.setId(idAgente);
		agente = agenteMidasService.loadById(agente);
		
		if(TransporteImpresionDTO.DOC_ESTADO_CUENTA.equalsIgnoreCase(tipoDocumento))
		{	
			String[] nombreRoles = {"Rol_M2_AdministradorAgente","Rol_M2_CoordinadorAgente"}; 
			boolean tieneRole=usuarioService.tieneRolUsuarioActual(nombreRoles);
			Long anioMes = new Long(anio + mes);
			reporte = generarPlantillaReporte
					.imprimirReporteEstadoDeCuentaAgente(8l,
							agente.getIdAgente(), anioMes,
							guiaLectura,
							detalle,
							MostrarColAdicionales,
							afirmeComunica, 
							afirmeComunicaText,
							(Boolean)tieneRole);
			
		}
		else if(TransporteImpresionDTO.DOC_DETALLE_PRIMAS.equalsIgnoreCase(tipoDocumento))
		{					
			reporte = generarPlantillaReporte.imprimirReporteDetallePrimas(anio, mes, agente, tipoSalidaArchivo);						
		}
		else if(TransporteImpresionDTO.DOC_RECIBO_HONORARIOS.equalsIgnoreCase(tipoDocumento))
		{
			reporte = generarPlantillaReporte.imprimirReciboHonorarios(solicitudChequeId);					
		}

		return reporte;
	}
	
	@Override
	public List<ConfigBonoRangoAplica> getRangosPorConfiguracion(ConfigBonos config) throws Exception {
		return configBonosDao.getRangosPorConfiguracion(config);
	}

	@Override
	public List<GenericaAgentesView> getLineasNegocioPorProducto(List<ConfigBonoProducto> productos) throws Exception {
		return configBonosDao.getLineasNegocioPorProducto(productos);
	}

	@Override
	public List<GenericaAgentesView> getLineasNegocioPorProductoView(String productos) throws Exception {
		return configBonosDao.getLineasNegocioPorProductoView(productos);
	}
	
	@Override
	public List<GenericaAgentesView> getLineasNegocioPorRamoProductoView(String productos, String ramos) throws Exception {
		return configBonosDao.getLineasNegocioPorRamoProductoView(productos,ramos);
	}
	@Override
	public List<ConfigBonoRangoClaveAmis> getListaBonoRangosClaveAmis(ConfigBonos config) throws Exception {
		return configBonosDao.getListaBonoRangosClaveAmis(config);
	}

	@Override
	public void saveConfigBonoRangosClaveAmis(List<ConfigBonoRangoClaveAmis> aQuedar,ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoRangoClaveAmis> aBorrar = new ArrayListNullAware<ConfigBonoRangoClaveAmis>();
		List<ConfigBonoRangoClaveAmis> aCrear = new ArrayListNullAware<ConfigBonoRangoClaveAmis>();
		aQuedar.removeAll(Collections.singleton(null));
		List<ConfigBonoRangoClaveAmis> actuales = entidadService.findByProperty(ConfigBonoRangoClaveAmis.class,"configBono.id",config.getId());
				
		for (ConfigBonoRangoClaveAmis actual : actuales) {
			
			filtro = getConfigBonoRangoClaveAmisPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoRangoClaveAmis elemento : aQuedar) {
			
			filtro = getConfigBonoRangoClaveAmisPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfigBono(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoRangoClaveAmis elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoRangoClaveAmis elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	@Override
	public void saveConfigBonoExclusionTipoBono(List<ConfigBonos> aQuedar, ConfigBonos config) {
		
		Predicate filtro;
		List<BonoExclusionesTipoBono> aBorrar = new ArrayListNullAware<BonoExclusionesTipoBono>();
		List<BonoExclusionesTipoBono> aCrear = new ArrayListNullAware<BonoExclusionesTipoBono>();
		BonoExclusionesTipoBono nuevaRelacion;
		aQuedar.removeAll(Collections.singleton(null));
		List<BonoExclusionesTipoBono> actuales = entidadService.findByProperty(BonoExclusionesTipoBono.class,"idConfig.id",config.getId());
				
		for (BonoExclusionesTipoBono actual : actuales) {
			
			filtro = getConfigBonoExclusionTipoBonoPredicate(actual.getIdBono());
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonos elemento : aQuedar) {
			
			filtro = getConfigBonoExclusionTipoBonoPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				nuevaRelacion = new BonoExclusionesTipoBono();
				nuevaRelacion.setIdConfig(config);
				nuevaRelacion.setIdBono(elemento);
				aCrear.add(nuevaRelacion);
				
			}
			
		}
		
		for (BonoExclusionesTipoBono elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (BonoExclusionesTipoBono elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	
	@Override
	public void saveConfigBonoExclusionTipoCedula(List<ValorCatalogoAgentes> aQuedar, ConfigBonos config) {
		
		Predicate filtro;
		List<BonoExclusionTipoCedula> aBorrar = new ArrayListNullAware<BonoExclusionTipoCedula>();
		List<BonoExclusionTipoCedula> aCrear = new ArrayListNullAware<BonoExclusionTipoCedula>();
		BonoExclusionTipoCedula nuevaRelacion;
		aQuedar.removeAll(Collections.singleton(null));
		List<BonoExclusionTipoCedula> actuales = entidadService.findByProperty(BonoExclusionTipoCedula.class,"idConfig.id",config.getId());
				
		for (BonoExclusionTipoCedula actual : actuales) {
			
			filtro = getConfigBonoExclusionTipoCedulaPredicate(actual.getIdTipoBono());
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ValorCatalogoAgentes elemento : aQuedar) {
			
			filtro = getConfigBonoExclusionTipoCedulaPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				nuevaRelacion = new BonoExclusionTipoCedula();
				nuevaRelacion.setIdConfig(config);
				nuevaRelacion.setIdTipoBono(elemento);
				aCrear.add(nuevaRelacion);
				
			}
			
		}
		
		for (BonoExclusionTipoCedula elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (BonoExclusionTipoCedula elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	@Override
	public void saveConfigBonoExcepcionAgente(List<ConfigBonoExcepAgente> aQuedar, ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoExcepAgente> aBorrar = new ArrayListNullAware<ConfigBonoExcepAgente>();
		List<ConfigBonoExcepAgente> aCrear = new ArrayListNullAware<ConfigBonoExcepAgente>();
		aQuedar.removeAll(Collections.singleton(null));
		List<ConfigBonoExcepAgente> actuales = entidadService.findByProperty(ConfigBonoExcepAgente.class,"configBono.id",config.getId());
				
		for (ConfigBonoExcepAgente actual : actuales) {
			
			filtro = getConfigBonoExcepcionAgentePredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoExcepAgente elemento : aQuedar) {
			
			filtro = getConfigBonoExcepcionAgenteForUpdatePredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfigBono(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoExcepAgente elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoExcepAgente elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	
	@Override
	public void saveConfigBonoExcepcionNegocio(List<ConfigBonoExcepNegocio> aQuedar, ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoExcepNegocio> aBorrar = new ArrayListNullAware<ConfigBonoExcepNegocio>();
		List<ConfigBonoExcepNegocio> aCrear = new ArrayListNullAware<ConfigBonoExcepNegocio>();
		aQuedar.removeAll(Collections.singleton(null));
		List<ConfigBonoExcepNegocio> actuales = entidadService.findByProperty(ConfigBonoExcepNegocio.class,"configBono.id",config.getId());
				
		for (ConfigBonoExcepNegocio actual : actuales) {
			
			filtro = getConfigBonoExcepcionNegocioPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoExcepNegocio elemento : aQuedar) {
			
			filtro = getConfigBonoExcepcionNegocioForUpdatePredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfigBono(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoExcepNegocio elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoExcepNegocio elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	@Override
	public void saveConfigBonoExcepcionPoliza(List<BonoExcepcionPoliza> aQuedar, ConfigBonos config) {
		
		Predicate filtro;
		List<BonoExcepcionPoliza> aBorrar = new ArrayListNullAware<BonoExcepcionPoliza>();
		List<BonoExcepcionPoliza> aCrear = new ArrayListNullAware<BonoExcepcionPoliza>();
		aQuedar.removeAll(Collections.singleton(null));
		List<BonoExcepcionPoliza> actuales = entidadService.findByProperty(BonoExcepcionPoliza.class,"idConfig.id",config.getId());
				
		for (BonoExcepcionPoliza actual : actuales) {
			
			filtro = getConfigBonoExcepcionPolizaPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (BonoExcepcionPoliza elemento : aQuedar) {
			
			filtro = getConfigBonoExcepcionPolizaForUpdatePredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setIdConfig(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (BonoExcepcionPoliza elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (BonoExcepcionPoliza elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	
	private void saveLineaVentaConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoLineaVenta> aBorrar = new ArrayListNullAware<ConfigBonoLineaVenta>();
		List<ConfigBonoLineaVenta> aCrear = new ArrayListNullAware<ConfigBonoLineaVenta>();
		
		List<ConfigBonoLineaVenta> actuales = entidadService.findByProperty(ConfigBonoLineaVenta.class, "configBono.id",config.getId());
		List<ConfigBonoLineaVenta> aQuedar = config.getLineaVenta();
		aQuedar.removeAll(Collections.singleton(null));
		
		for (ConfigBonoLineaVenta actual : actuales) {
			
			filtro = getConfigBonoLineaVentaPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoLineaVenta elemento : aQuedar) {
			
			filtro = getConfigBonoLineaVentaPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfigBono(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoLineaVenta elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoLineaVenta elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
		
	private void saveCentroOperacionConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoCentroOperacion> aBorrar = new ArrayListNullAware<ConfigBonoCentroOperacion>();
		List<ConfigBonoCentroOperacion> aCrear = new ArrayListNullAware<ConfigBonoCentroOperacion>();
		
		List<ConfigBonoCentroOperacion> actuales = entidadService.findByProperty(ConfigBonoCentroOperacion.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoCentroOperacion> aQuedar = config.getListaCentroOperaciones();
		aQuedar.removeAll(Collections.singleton(null));
						
		for (ConfigBonoCentroOperacion actual : actuales) {
			
			filtro = getConfigBonoCentroOperacionPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoCentroOperacion elemento : aQuedar) {
			
			filtro = getConfigBonoCentroOperacionPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoCentroOperacion elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoCentroOperacion elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	

	private void saveEjecutivoConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoEjecutivo> aBorrar = new ArrayListNullAware<ConfigBonoEjecutivo>();
		List<ConfigBonoEjecutivo> aCrear = new ArrayListNullAware<ConfigBonoEjecutivo>();
		
		List<ConfigBonoEjecutivo> actuales = entidadService.findByProperty(ConfigBonoEjecutivo.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoEjecutivo> aQuedar = config.getListaEjecutivos();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoEjecutivo actual : actuales) {
			
			filtro = getConfigBonoEjecutivoPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoEjecutivo elemento : aQuedar) {
			
			filtro = getConfigBonoEjecutivoPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoEjecutivo elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoEjecutivo elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}

	}

	private void saveGerenciaConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoGerencia> aBorrar = new ArrayListNullAware<ConfigBonoGerencia>();
		List<ConfigBonoGerencia> aCrear = new ArrayListNullAware<ConfigBonoGerencia>();
		
		List<ConfigBonoGerencia> actuales = entidadService.findByProperty(ConfigBonoGerencia.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoGerencia> aQuedar = config.getListaGerencias();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoGerencia actual : actuales) {
			
			filtro = getConfigBonoGerenciaPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoGerencia elemento : aQuedar) {
			
			filtro = getConfigBonoGerenciaPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoGerencia elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoGerencia elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}

	}
	
	private void savePrioridadConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoPrioridad> aBorrar = new ArrayListNullAware<ConfigBonoPrioridad>();
		List<ConfigBonoPrioridad> aCrear = new ArrayListNullAware<ConfigBonoPrioridad>();
		
		List<ConfigBonoPrioridad> actuales = entidadService.findByProperty(ConfigBonoPrioridad.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoPrioridad> aQuedar = config.getListaPrioridades();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoPrioridad actual : actuales) {
			
			filtro = getConfigBonoPrioridadPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoPrioridad elemento : aQuedar) {
			
			filtro = getConfigBonoPrioridadPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoPrioridad elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoPrioridad elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}

	}
	
	private void savePromotoriaConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoPromotoria> aBorrar = new ArrayListNullAware<ConfigBonoPromotoria>();
		List<ConfigBonoPromotoria> aCrear = new ArrayListNullAware<ConfigBonoPromotoria>();
		
		List<ConfigBonoPromotoria> actuales = entidadService.findByProperty(ConfigBonoPromotoria.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoPromotoria> aQuedar = config.getListaPromotorias();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoPromotoria actual : actuales) {
			
			filtro = getConfigBonoPromotoriaPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoPromotoria elemento : aQuedar) {
			
			filtro = getConfigBonoPromotoriaPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoPromotoria elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoPromotoria elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	private void saveSituacionConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoSituacion> aBorrar = new ArrayListNullAware<ConfigBonoSituacion>();
		List<ConfigBonoSituacion> aCrear = new ArrayListNullAware<ConfigBonoSituacion>();
		
		List<ConfigBonoSituacion> actuales = entidadService.findByProperty(ConfigBonoSituacion.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoSituacion> aQuedar = config.getListaSituaciones();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoSituacion actual : actuales) {
			
			filtro = getConfigBonoSituacionPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoSituacion elemento : aQuedar) {
			
			filtro = getConfigBonoSituacionPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoSituacion elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoSituacion elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	private void saveTipoAgenteConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoTipoAgente> aBorrar = new ArrayListNullAware<ConfigBonoTipoAgente>();
		List<ConfigBonoTipoAgente> aCrear = new ArrayListNullAware<ConfigBonoTipoAgente>();
		
		List<ConfigBonoTipoAgente> actuales = entidadService.findByProperty(ConfigBonoTipoAgente.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoTipoAgente> aQuedar = config.getListaTipoAgentes();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoTipoAgente actual : actuales) {
			
			filtro = getConfigBonoTipoAgentePredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoTipoAgente elemento : aQuedar) {
			
			filtro = getConfigBonoTipoAgentePredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoTipoAgente elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoTipoAgente elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	private void saveTipoPromotoriaConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoTipoPromotoria> aBorrar = new ArrayListNullAware<ConfigBonoTipoPromotoria>();
		List<ConfigBonoTipoPromotoria> aCrear = new ArrayListNullAware<ConfigBonoTipoPromotoria>();
		
		List<ConfigBonoTipoPromotoria> actuales = entidadService.findByProperty(ConfigBonoTipoPromotoria.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoTipoPromotoria> aQuedar = config.getListaTiposPromotoria();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoTipoPromotoria actual : actuales) {
			
			filtro = getConfigBonoTipoPromotoriaPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoTipoPromotoria elemento : aQuedar) {
			
			filtro = getConfigBonoTipoPromotoriaPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoTipoPromotoria elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoTipoPromotoria elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	private void saveAgentesConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoAgente> aBorrar = new ArrayListNullAware<ConfigBonoAgente>();
		List<ConfigBonoAgente> aCrear = new ArrayListNullAware<ConfigBonoAgente>();
		
		List<ConfigBonoAgente> actuales = entidadService.findByProperty(ConfigBonoAgente.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoAgente> aQuedar = config.getListaAgentes();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoAgente actual : actuales) {
			
			filtro = getConfigBonoAgentePredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoAgente elemento : aQuedar) {
			
			filtro = getConfigBonoAgentePredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoAgente elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoAgente elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	private void saveAplicaAgentesConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoAplicaAgente> aBorrar = new ArrayListNullAware<ConfigBonoAplicaAgente>();
		List<ConfigBonoAplicaAgente> aCrear = new ArrayListNullAware<ConfigBonoAplicaAgente>();
		
		List<ConfigBonoAplicaAgente> actuales = entidadService.findByProperty(ConfigBonoAplicaAgente.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoAplicaAgente> aQuedar = config.getListaAplicaAgentes();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoAplicaAgente actual : actuales) {
			
			filtro = getConfigBonoAplicaAgentePredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoAplicaAgente elemento : aQuedar) {
			
			filtro = getConfigBonoAplicaAgentePredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoAplicaAgente elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoAplicaAgente elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	private void saveAplicaPromotoriasConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoAplicaPromotoria> aBorrar = new ArrayListNullAware<ConfigBonoAplicaPromotoria>();
		List<ConfigBonoAplicaPromotoria> aCrear = new ArrayListNullAware<ConfigBonoAplicaPromotoria>();
		
		List<ConfigBonoAplicaPromotoria> actuales = entidadService.findByProperty(ConfigBonoAplicaPromotoria.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoAplicaPromotoria> aQuedar = config.getListaAplicaPromotorias();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoAplicaPromotoria actual : actuales) {
			
			filtro = getConfigBonoAplicaPromotoriaPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoAplicaPromotoria elemento : aQuedar) {
			
			filtro = getConfigBonoAplicaPromotoriaPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoAplicaPromotoria elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoAplicaPromotoria elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	private void saveProductoConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoProducto> aBorrar = new ArrayListNullAware<ConfigBonoProducto>();
		List<ConfigBonoProducto> aCrear = new ArrayListNullAware<ConfigBonoProducto>();
		
		List<ConfigBonoProducto> actuales = entidadService.findByProperty(ConfigBonoProducto.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoProducto> aQuedar = config.getProductos();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoProducto actual : actuales) {
			
			filtro = getConfigBonoProductoPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoProducto elemento : aQuedar) {
			
			filtro = getConfigBonoProductoPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoProducto elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoProducto elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	private void saveRamoConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoRamo> aBorrar = new ArrayListNullAware<ConfigBonoRamo>();
		List<ConfigBonoRamo> aCrear = new ArrayListNullAware<ConfigBonoRamo>();
		
		List<ConfigBonoRamo> actuales = entidadService.findByProperty(ConfigBonoRamo.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoRamo> aQuedar = config.getRamos();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoRamo actual : actuales) {
			
			filtro = getConfigBonoRamoPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoRamo elemento : aQuedar) {
			
			filtro = getConfigBonoRamoPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoRamo elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoRamo elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	private void saveSubramoConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoSubramo> aBorrar = new ArrayListNullAware<ConfigBonoSubramo>();
		List<ConfigBonoSubramo> aCrear = new ArrayListNullAware<ConfigBonoSubramo>();
		
		List<ConfigBonoSubramo> actuales = entidadService.findByProperty(ConfigBonoSubramo.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoSubramo> aQuedar = config.getSubramos();
		aQuedar.removeAll(Collections.singleton(null));	
		
		for (ConfigBonoSubramo actual : actuales) {
			
			filtro = getConfigBonoSubramoPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoSubramo elemento : aQuedar) {
			
			filtro = getConfigBonoSubramoPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoSubramo elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoSubramo elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	private void saveSeccionConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoSeccion> aBorrar = new ArrayListNullAware<ConfigBonoSeccion>();
		List<ConfigBonoSeccion> aCrear = new ArrayListNullAware<ConfigBonoSeccion>();
		
		List<ConfigBonoSeccion> actuales = entidadService.findByProperty(ConfigBonoSeccion.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoSeccion> aQuedar = config.getSecciones();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoSeccion actual : actuales) {
			
			filtro = getConfigBonoSeccionPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoSeccion elemento : aQuedar) {
			
			filtro = getConfigBonoSeccionPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoSeccion elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoSeccion elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
	
	private void saveCoberturaConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoCobertura> aBorrar = new ArrayListNullAware<ConfigBonoCobertura>();
		List<ConfigBonoCobertura> aCrear = new ArrayListNullAware<ConfigBonoCobertura>();
		
		List<ConfigBonoCobertura> actuales = entidadService.findByProperty(ConfigBonoCobertura.class,"configuracionBonos.id",config.getId());
		List<ConfigBonoCobertura> aQuedar = config.getCoberturas();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoCobertura actual : actuales) {
			
			filtro = getConfigCoberturaPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoCobertura elemento : aQuedar) {
			
			filtro = getConfigCoberturaPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfiguracionBonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoCobertura elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoCobertura elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}

	private void savePolizasConfig(ConfigBonos config) {
		
		Predicate filtro;
		List<ConfigBonoPoliza> aBorrar = new ArrayListNullAware<ConfigBonoPoliza>();
		List<ConfigBonoPoliza> aCrear = new ArrayListNullAware<ConfigBonoPoliza>();
		
		List<ConfigBonoPoliza> actuales = entidadService.findByProperty(ConfigBonoPoliza.class,"configbonos.id",config.getId());
		List<ConfigBonoPoliza> aQuedar = config.getListaConfigPolizas();
		aQuedar.removeAll(Collections.singleton(null));
				
		for (ConfigBonoPoliza actual : actuales) {
			
			filtro = getConfigBonoPolizaPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoPoliza elemento : aQuedar) {
			
			filtro = getConfigBonoPolizaPredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfigbonos(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoPoliza elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoPoliza elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
	}
		
	private Predicate getConfigBonoLineaVentaPredicate (final ConfigBonoLineaVenta filtro) {
		
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false; 
				return filtro.getIdLineaVenta().equals(((ConfigBonoLineaVenta) arg0).getIdLineaVenta());
				
			}
		};
		
	}
	
	private Predicate getConfigBonoCentroOperacionPredicate(
			final ConfigBonoCentroOperacion filtro) {
		
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getCentroOperacion().getId().equals(((ConfigBonoCentroOperacion) arg0).getCentroOperacion().getId());
				
			}
		};
		
	}

	private Predicate getConfigBonoEjecutivoPredicate(final ConfigBonoEjecutivo filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getEjecutivo().getId().equals(((ConfigBonoEjecutivo) arg0).getEjecutivo().getId());
				
			}
		};
		
	}

	private Predicate getConfigBonoGerenciaPredicate(final ConfigBonoGerencia filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getGerencia().getId().equals(((ConfigBonoGerencia) arg0).getGerencia().getId());
				
			}
		};
		
	}


	private Predicate getConfigBonoPrioridadPredicate(final ConfigBonoPrioridad filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getPrioridadAgente().getId().equals(((ConfigBonoPrioridad) arg0).getPrioridadAgente().getId());
				
			}
		};
		
	}


	private Predicate getConfigBonoPromotoriaPredicate(
			final ConfigBonoPromotoria filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getPromotoria().getId().equals(((ConfigBonoPromotoria) arg0).getPromotoria().getId());
				
			}
		};
		
	}


	private Predicate getConfigBonoSituacionPredicate(final ConfigBonoSituacion filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getSituacionAgente().getId().equals(((ConfigBonoSituacion) arg0).getSituacionAgente().getId());
				
			}
		};
		
	}

	private Predicate getConfigBonoTipoAgentePredicate(
			final ConfigBonoTipoAgente filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getTipoAgente().getId().equals(((ConfigBonoTipoAgente) arg0).getTipoAgente().getId());
				
			}
		};
		
	}

	private Predicate getConfigBonoTipoPromotoriaPredicate(
			final ConfigBonoTipoPromotoria filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getTipoPromotoria().getId().equals(((ConfigBonoTipoPromotoria) arg0).getTipoPromotoria().getId());
				
			}
		};
		
	}

	private Predicate getConfigBonoAgentePredicate(final ConfigBonoAgente filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getAgente().getId().equals(((ConfigBonoAgente) arg0).getAgente().getId());
				
			}
		};
		
	}


	private Predicate getConfigBonoAplicaAgentePredicate(
			final ConfigBonoAplicaAgente filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getAgente().getId().equals(((ConfigBonoAplicaAgente) arg0).getAgente().getId());
				
			}
		};
		
	}


	private Predicate getConfigBonoAplicaPromotoriaPredicate(
			final ConfigBonoAplicaPromotoria filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getPromotoria().getId().equals(((ConfigBonoAplicaPromotoria) arg0).getPromotoria().getId());
				
			}
		};
		
	}

	private Predicate getConfigBonoProductoPredicate(final ConfigBonoProducto filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getIdProducto().equals(((ConfigBonoProducto) arg0).getIdProducto());
				
			}
		};
		
	}

	private Predicate getConfigBonoRamoPredicate(final ConfigBonoRamo filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getIdRamo().equals(((ConfigBonoRamo) arg0).getIdRamo());
				
			}
		};
		
	}


	private Predicate getConfigBonoSubramoPredicate(final ConfigBonoSubramo filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getIdSubramo().equals(((ConfigBonoSubramo) arg0).getIdSubramo());
				
			}
		};
		
	}

	private Predicate getConfigBonoSeccionPredicate(final ConfigBonoSeccion filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getIdSeccion().equals(((ConfigBonoSeccion) arg0).getIdSeccion());
				
			}
		};
		
	}


	private Predicate getConfigCoberturaPredicate(final ConfigBonoCobertura filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getIdCobertura().equals(((ConfigBonoCobertura) arg0).getIdCobertura());
				
			}
		};
		
	}

	
	
	private Predicate getConfigBonoPolizaPredicate(final ConfigBonoPoliza filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getIdPoliza().equals(((ConfigBonoPoliza) arg0).getIdPoliza());
				
			}
		};
		
	}
	
	private Predicate getConfigBonoRangoClaveAmisPredicate (final ConfigBonoRangoClaveAmis filtro) {
		
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				ConfigBonoRangoClaveAmis elemento = (ConfigBonoRangoClaveAmis) arg0;
										
				return (filtro != null
						&& filtro.getCve_amis_ini() != null
						&& filtro.getCve_amis_ini().equals(elemento.getCve_amis_ini())
						&& filtro.getCve_amis_fin() != null
						&& filtro.getCve_amis_fin().equals(elemento.getCve_amis_fin()));
				
			}
		};
		
	}

	private Predicate getConfigBonoExclusionTipoBonoPredicate (final ConfigBonos filtro) {
		
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				if (arg0 instanceof ConfigBonos) {
					
					return filtro.getId().equals(((ConfigBonos) arg0).getId());
					
				} else {
					
					return filtro.getId().equals(((BonoExclusionesTipoBono) arg0).getIdBono().getId());
					
				}
				
			}
		};
		
	}

	private Predicate getConfigBonoExclusionTipoCedulaPredicate (final ValorCatalogoAgentes filtro) {
		
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				if (arg0 instanceof ValorCatalogoAgentes) {
					
					return filtro.getId().equals(((ValorCatalogoAgentes) arg0).getId());
					
				} else {
					
					return filtro.getId().equals(((BonoExclusionTipoCedula) arg0).getIdTipoBono().getId());
					
				}
				
			}
		};
		
	}

	private Predicate getConfigBonoExcepcionAgentePredicate (final ConfigBonoExcepAgente filtro) {
		
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getId().equals(((ConfigBonoExcepAgente) arg0).getId());
				
			}
		};
		
	}
	
	private Predicate getConfigBonoExcepcionNegocioPredicate (final ConfigBonoExcepNegocio filtro) {
		
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getId().equals(((ConfigBonoExcepNegocio) arg0).getId());
				
			}
		};
		
	}
	
	private Predicate getConfigBonoExcepcionPolizaPredicate (final BonoExcepcionPoliza filtro) {
	
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getIdExcepcionPoliza().equals(((BonoExcepcionPoliza) arg0).getIdExcepcionPoliza());
				
			}
		};
		
	}
			
	
	/*Predicados para detectar cambios en relaciones con datos extras*/
	
	private Predicate getConfigBonoExcepcionAgenteForUpdatePredicate (final ConfigBonoExcepAgente filtro) {
		
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				ConfigBonoExcepAgente elemento = (ConfigBonoExcepAgente) arg0;
										
				return (filtro != null 
						&& filtro.getId() != null 
						&& filtro.getId().equals(elemento.getId())
						&& filtro.getValorMontoPcteAgente() != null
						&& filtro.getValorMontoPcteAgente().equals(elemento.getValorMontoPcteAgente())
						&& filtro.getNoAplicaDescuentoExcepcion() != null
						&& filtro.getNoAplicaDescuentoExcepcion().equals(elemento.getNoAplicaDescuentoExcepcion()));
				
			}
		};
		
	}
	
	private Predicate getConfigBonoExcepcionNegocioForUpdatePredicate (final ConfigBonoExcepNegocio filtro) {
		
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				ConfigBonoExcepNegocio elemento = (ConfigBonoExcepNegocio) arg0;
										
				return (filtro != null
						&& filtro.getId() != null
						&& filtro.getId().equals(elemento.getId())
						&& filtro.getValorMontoPcteAgente() != null
						&& filtro.getValorMontoPcteAgente().equals(elemento.getValorMontoPcteAgente())
						&& filtro.getValorMontoPctePromotoria() != null
						&& filtro.getValorMontoPctePromotoria().equals(elemento.getValorMontoPctePromotoria())
						&& filtro.getDescuentoMaximo() != null
						&& filtro.getDescuentoMaximo().equals(elemento.getDescuentoMaximo()));
				
			}
		};
		
	}
	
	private Predicate getConfigBonoExcepcionPolizaForUpdatePredicate (final BonoExcepcionPoliza filtro) {
	
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				BonoExcepcionPoliza elemento = (BonoExcepcionPoliza) arg0;
										
				return (filtro != null
						&& filtro.getIdExcepcionPoliza() != null
						&& filtro.getIdExcepcionPoliza().equals(elemento.getIdExcepcionPoliza())
						&& filtro.getValorMontoPcteAgente() != null
						&& filtro.getValorMontoPcteAgente().equals(elemento.getValorMontoPcteAgente())
						&& filtro.getValorMontoPctePromotoria() != null
						&& filtro.getValorMontoPctePromotoria().equals(elemento.getValorMontoPctePromotoria())
						&& filtro.getNoAplicaDescuentoExcepcion() != null
						&& filtro.getNoAplicaDescuentoExcepcion().equals(elemento.getNoAplicaDescuentoExcepcion())
						&& filtro.getDescuentoPoliza() != null
						&& filtro.getDescuentoPoliza().equals(elemento.getDescuentoPoliza()));
				
			}
		};
		
	}

	private ArrayList<String> getListaCorreosAdicionales(String correoAdiconal){
		ArrayList<String> address = new ArrayList<String>();
		
		if (correoAdiconal.contains(";")){
			String[] correoAdicional = correoAdiconal.split(";");
			for(String str1 :correoAdicional){
				address.add(str1);
			}
		}else{
			address.add(correoAdiconal);
		}
		 return address;
	}
	
	private ConfigBonos obtenerCopiaConfiguracion (ConfigBonos original) throws Exception {
		
		ConfigBonos copia = new ConfigBonos();
		

		copia.setTipoBono(original.getTipoBono());
		copia.setDescripcion("copia " + original.getDescripcion());
		copia.setPagoSinFactura(original.getPagoSinFactura());
		copia.setFechaFinVigencia(original.getFechaFinVigencia());
		copia.setFechaInicioVigencia(original.getFechaInicioVigencia());
		copia.setFechaVencimientoCedula(original.getFechaVencimientoCedula());
		copia.setIdMoneda(original.getIdMoneda());
		copia.setAjusteDeOficina(original.getAjusteDeOficina());
		copia.setPeriodoAjuste(original.getPeriodoAjuste());
		copia.setIdBonoAjustar(original.getIdBonoAjustar());
		copia.setProduccionSobre(original.getProduccionSobre());
		copia.setSiniestralidadSobre(original.getSiniestralidadSobre());
		copia.setProduccionMinima(original.getProduccionMinima());
		copia.setPorcentajeSiniestralidadMaxima(original.getPorcentajeSiniestralidadMaxima());
		copia.setGlobal(original.getGlobal());
		copia.setPeriodoInicial(original.getPeriodoInicial());
		copia.setPeriodoFinal(original.getPeriodoFinal());
		copia.setModoAplicacion(original.getModoAplicacion());
		copia.setPorcentajeImporteDirecto(original.getPorcentajeImporteDirecto());
		copia.setPorcentajeAplicacionDirecto(original.getPorcentajeAplicacionDirecto());
		copia.setImporteAplicacionDirecto(original.getImporteAplicacionDirecto());
		copia.setPeriodoComparacion(original.getPeriodoComparacion());
		copia.setTipoBeneficiario(original.getTipoBeneficiario());
		copia.setTodasPromotorias(original.getTodasPromotorias());
		copia.setTodosAgentes(original.getTodosAgentes());
		copia.setActivo(original.getActivo());
		copia.setConDescuentoMayorIgual(original.getConDescuentoMayorIgual());
		copia.setTodasPolizas(original.getTodasPolizas());
		copia.setContratadoConSobreComision(original.getContratadoConSobreComision());
		copia.setDiasAntesVencimiento(original.getDiasAntesVencimiento());
		copia.setTipoPersona(original.getTipoPersona());
		copia.setConIncisosMayorIgual(original.getConIncisosMayorIgual());
		copia.setIdNivelSiniestralidad(original.getIdNivelSiniestralidad());
		copia.setIdSubtipoBono(original.getIdSubtipoBono());
		copia.setIdNivelProductividad(original.getIdNivelProductividad());
		copia.setIdNivelCrecimiento(original.getIdNivelCrecimiento());
		copia.setListaCentroOperaciones(childListClone(ConfigBonoCentroOperacion.class, configBonosDao.getCentrosOperacionPorConfiguracion(original)));
		copia.setLineaVenta(childListClone(ConfigBonoLineaVenta.class, configBonosDao.getLineaVentaPorConfiguracion(original)));
		copia.setProductos(childListClone(ConfigBonoProducto.class, configBonosDao.getProductosPorConfiguracion(original)));
		copia.setRamos(childListClone(ConfigBonoRamo.class, configBonosDao.getRamosPorConfiguracion(original)));
		copia.setSubramos(childListClone(ConfigBonoSubramo.class, configBonosDao.getSubRamosPorConfiguracion(original)));
		copia.setSecciones(childListClone(ConfigBonoSeccion.class, configBonosDao.getSeccionesPorConfiguracion(original)));
		copia.setCoberturas(childListClone(ConfigBonoCobertura.class, configBonosDao.getCoberturasPorConfiguracion(original)));
		copia.setListaGerencias(childListClone(ConfigBonoGerencia.class, configBonosDao.getGerenciasPorConfiguracion(original)));
		copia.setListaEjecutivos(childListClone(ConfigBonoEjecutivo.class, configBonosDao.getEjecutivosPorConfiguracion(original)));
		copia.setListaPromotorias(childListClone(ConfigBonoPromotoria.class, configBonosDao.getPromotoriasPorConfiguracion(original)));
		copia.setListaTiposPromotoria(childListClone(ConfigBonoTipoPromotoria.class, configBonosDao.getTiposPromotoriaPorConfiguracion(original)));
		copia.setListaTipoAgentes(childListClone(ConfigBonoTipoAgente.class, configBonosDao.getTiposAgentePorConfiguracion(original)));
		copia.setListaPrioridades(childListClone(ConfigBonoPrioridad.class, configBonosDao.getPrioridadesPorConfiguracion(original)));
		copia.setListaSituaciones(childListClone(ConfigBonoSituacion.class, configBonosDao.getSituacionesPorConfiguracion(original)));
		copia.setListaAgentes(childListClone(ConfigBonoAgente.class, entidadService.findByProperty(ConfigBonoAgente.class, "configuracionBonos", original)));
		copia.setListaConfigPolizas(childListClone(ConfigBonoPoliza.class, entidadService.findByProperty(ConfigBonoPoliza.class, "configbonos", original)));
		copia.setListaAplicaAgentes(childListClone(ConfigBonoAplicaAgente.class, entidadService.findByProperty(ConfigBonoAplicaAgente.class, "configuracionBonos", original)));
		copia.setListaAplicaPromotorias(childListClone(ConfigBonoAplicaPromotoria.class, configBonosDao.getAplicaPromotoriasByIdConfigBono(original.getId())));
		
		return copia;
	}
	
	private void saveConfigRelationships(ConfigBonos config) {
	
		//TODO Arreglar el modelo para que no se necesiten este tipo de metodos
		
		saveLineaVentaConfig(config);
		saveCentroOperacionConfig(config);
		saveGerenciaConfig(config);
		saveEjecutivoConfig(config);
		savePromotoriaConfig(config);
		savePrioridadConfig(config);
		saveTipoPromotoriaConfig(config);
		saveTipoAgenteConfig(config);
		saveSituacionConfig(config);
		saveAgentesConfig(config);
		savePolizasConfig(config);
		saveAplicaAgentesConfig(config);
		saveAplicaPromotoriasConfig(config);
		saveProductoConfig(config);
		saveRamoConfig(config);
		saveSubramoConfig(config);
		saveSeccionConfig(config);
		saveCoberturaConfig(config);
		
	}
	
	private void copiarExcepcionesConfiguracion (ConfigBonos original, ConfigBonos copia) throws Exception {
		
		ConvertUtilsBean convertUtilsBean = BeanUtilsBean.getInstance().getConvertUtils();
		convertUtilsBean.register(false, false, 0);
				
		List<BonoExcepcionPoliza>listExcepcionPolizas = entidadService.findByProperty(BonoExcepcionPoliza.class,"idConfig.id" , original.getId());
		BonoExcepcionPoliza objPolizaInsertar;
		for(BonoExcepcionPoliza objPoliza : listExcepcionPolizas){
			objPolizaInsertar = new BonoExcepcionPoliza();
			BeanUtilsBean.getInstance().copyProperties(objPolizaInsertar,objPoliza);
			objPolizaInsertar.setIdConfig(copia);
			objPolizaInsertar.setIdExcepcionPoliza(null);
			entidadService.save(objPolizaInsertar);
		}
		
		List<BonoExclusionesTipoBono> listExclusionTipoBono = bonoExclusionesTipoBonoservice.loadByConfiguration(original.getId());
		BonoExclusionesTipoBono objTipoBonoInsertar;
		for(BonoExclusionesTipoBono objTipoBono:listExclusionTipoBono){
			objTipoBonoInsertar = new BonoExclusionesTipoBono();
			BeanUtilsBean.getInstance().copyProperties(objTipoBonoInsertar,objTipoBono);
			objTipoBonoInsertar.setIdConfig(copia);
			objTipoBonoInsertar.setId(null);
			entidadService.save(objTipoBonoInsertar);
		}	
		
		List<BonoExclusionTipoCedula> listCedulaExclusiones = bonoExclusionTipoCedulaService.findById(original.getId());
		BonoExclusionTipoCedula objCedulaInsertar;
		for(BonoExclusionTipoCedula objCedula:listCedulaExclusiones ){
			objCedulaInsertar = new BonoExclusionTipoCedula();
			BeanUtilsBean.getInstance().copyProperties(objCedulaInsertar,objCedula);
			objCedulaInsertar.setIdConfig(copia);
			objCedulaInsertar.setIdExclusionCedula(null);
			entidadService.save(objCedulaInsertar);
		}
		
		List<ConfigBonoExcepAgente> listExcepcionAgente = configBonoExcepAgenteService.loadByConfigBono(original);
		ConfigBonoExcepAgente objAgenteInsertar;
		for(ConfigBonoExcepAgente objAgente : listExcepcionAgente){
			objAgenteInsertar = new ConfigBonoExcepAgente();
			BeanUtilsBean.getInstance().copyProperties(objAgenteInsertar,objAgente);
			objAgenteInsertar.setConfigBono(copia);
			objAgenteInsertar.setId(null);
			entidadService.save(objAgenteInsertar);
		}
		
		List<ConfigBonoExcepNegocio> listExcepcionNegocio = configBonoExcepNegocioService.loadByConfigBono(original);
		ConfigBonoExcepNegocio objExcepNegInsertar;
		for(ConfigBonoExcepNegocio objExcepcionNegocio : listExcepcionNegocio){
			objExcepNegInsertar = new ConfigBonoExcepNegocio();
			BeanUtilsBean.getInstance().copyProperties(objExcepNegInsertar,objExcepcionNegocio);
			objExcepNegInsertar.setConfigBono(copia);
			objExcepNegInsertar.setId(null);
			entidadService.save(objExcepNegInsertar);
		}
		
		List<ConfigBonoRangoClaveAmis>  listClaveAMIS =  getListaBonoRangosClaveAmis(original);
		ConfigBonoRangoClaveAmis objExcepAmisInsertar;
		for(ConfigBonoRangoClaveAmis objExcepcionAmis : listClaveAMIS){
			objExcepAmisInsertar = new ConfigBonoRangoClaveAmis();
			BeanUtilsBean.getInstance().copyProperties(objExcepAmisInsertar,objExcepcionAmis);
			objExcepAmisInsertar.setConfigBono(copia);
			objExcepAmisInsertar.setId(null);
			entidadService.save(objExcepAmisInsertar);
		}
		
		// Se modifica la propiedad defaultNull = true para el clonado de los rangos 
		// Se deben mantener los nulos ya que de esa manera se identifca que un concepto dentro del rango no debe ser considerado.		
		convertUtilsBean.register(false, true, 0);
		
		List<ConfigBonoRangoAplica>  rangos =  getRangosPorConfiguracion(original);
		ConfigBonoRangoAplica rango;
		for(ConfigBonoRangoAplica objRango : rangos){
			rango = new ConfigBonoRangoAplica();
			BeanUtilsBean.getInstance().copyProperties(rango,objRango);
			rango.setConfigBono(copia);
			rango.setId(null);
			entidadService.save(rango);
		}
				
	}
	
	
	private <E extends Entidad> List<E> childListClone (Class<E> childClass, List<E> originalChildList) throws Exception {
		
		
		String property = null;
		
		if (childClass.getSimpleName().equals("ConfigBonoLineaVenta")) {
			
			property = "configBono";
			
		} else if (childClass.getSimpleName().equals("ConfigBonoPoliza")) {
			
			property = "configbonos";
			
		} else {
			
			property = "configuracionBonos";
			
		}
		
		Field configField = childClass.getDeclaredField(property);
		Field idField = childClass.getDeclaredField("id");
		
		configField.setAccessible(true);
		idField.setAccessible(true);
		
		for (E child : originalChildList) {
			
			entidadService.detach(child); 
			
			configField.set(child, null);
			idField.set(child, null);
			
		}		
		
		return originalChildList;
	}
		
	@EJB
	private ConfigBonosDao configBonosDao;
	
	@EJB
	private ConfigBonosService configBonosService;
	
	@EJB
	private AgenteMidasService agenteMidasService;
	
	@EJB
	private PromotoriaService promotoriaService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private MailService mailService;
	
	@EJB
	private GenerarPlantillaReporte generarPlantillaReporte;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private BonoExclusionesTipoBonoService bonoExclusionesTipoBonoservice;

	@EJB
	private BonoExclusionTipoCedulaService bonoExclusionTipoCedulaService;

	@EJB
	private ConfigBonoExcepAgenteService configBonoExcepAgenteService;

	@EJB
	private ConfigBonoExcepNegocioService configBonoExcepNegocioService;

	@EJB
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	
	@EJB
	private DocumentoAgenteService documentoAgenteService;
	
	@Resource	
	private TimerService timerService;
	
	@Override
	public List<AgenteView> obtenerAgentesPorFuerzaVenta(Long centroOperacion, Long ejecutivo,
			Long gerencia, Long promotorias, Long clasificacionAgente, Long claveAgente) throws Exception {
		return configBonosDao.obtenerAgentesPorFuerzaVenta(centroOperacion, ejecutivo, gerencia, promotorias, clasificacionAgente, claveAgente);
	}
	
	public void initialize() {
			String timerInfo = "TimerNotificacionesRepCalculoBonosMensual";
			cancelarTemporizador(timerInfo);
			iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.minute(0);
				expression.hour("9-19");
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerNotificacionesRepCalculoBonosMensual", false));
				
				log.info("Timer TimerNotificacionesRepCalculoBonosMensual configurado");
			} catch (Exception e) {
				log.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		log.info("Cancelar Timer TimerNotificacionesRepCalculoBonosMensual");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			log.error("Error al detener Timer TimerNotificacionesRepCalculoBonosMensual:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		enviarNotificacionesRepCalculoBonosMensual();
	}
	
	
	private void  enviarDocPorCorreo(Long id,Map<String,Object> genericMap, String formatoSalida) throws JRException{
		
		String sTipoDocumento = (String) genericMap.get(TIPO_DOCUMENTO);
		String nombreArchivo = "";
		String mensaje = "";
		StringBuilder mes = new StringBuilder();
		List<String> address = null;
		
		address = new ArrayList<String>();
		Agente agente = new Agente();
		agente.setId(id);
		agente = agenteMidasService.loadById(agente);
		final String correo = (agente.getPersona()!=null && agente.getPersona().getEmail()!=null)?agente.getPersona().getEmail():null;
		final String correoAdiconal = (agente.getPersona()!=null && agente.getPersona().getCorreosAdicionales()!=null)?agente.getPersona().getCorreosAdicionales():null;
		if(StringUtils.isNotBlank(correoAdiconal)) {
			address.addAll(getListaCorreosAdicionales(correoAdiconal));
		}
		if (StringUtils.isNotBlank(correo)) {
			address.add(correo);
		}
		List<ByteArrayAttachment> attachment = new ArrayList<ByteArrayAttachment>();
		ByteArrayAttachment file = new ByteArrayAttachment();
		TransporteImpresionDTO report = null;
		mes.delete(0,mes.length());
		
		String anio = (String)genericMap.get("anio");
		mes.append((String)genericMap.get("mes"));
		if(Integer.parseInt(mes.toString())<10){
			 mes.insert(0,"0");				
		}
		
		if(TransporteImpresionDTO.DOC_ESTADO_CUENTA.equalsIgnoreCase(sTipoDocumento))
		{	
			Long anioMes = new Long(anio + mes.toString());
			report = generarPlantillaReporte
					.imprimirReporteEstadoDeCuentaAgente(8l,
							agente.getIdAgente(), anioMes,
							(Boolean) genericMap.get("guiaLectura"),
							(Boolean) genericMap.get("segAfirmeComunica"),
							(Boolean) genericMap.get("detalle"),
							(Boolean) genericMap.get("MostrarColAdicionales"),
							(String) genericMap.get("afirmeComunicaText"),
							(Boolean) genericMap.get("tieneRole"));
			nombreArchivo = "EstadoDeCuentaAgente.pdf";
			file.setTipoArchivo(ByteArrayAttachment.TipoArchivo.PDF);
			file.setNombreArchivo(nombreArchivo);
			mensaje = MailServiceSupport.mensajeAgenteEstadoDeCuenta(agente.getPersona().getNombreCompleto(),Integer.valueOf(anio), Integer.valueOf(mes.toString()));
		}
		else if(TransporteImpresionDTO.DOC_DETALLE_PRIMAS.equalsIgnoreCase(sTipoDocumento))
		{					
			report = generarPlantillaReporte.imprimirReporteDetallePrimas(anio, mes.toString(), agente, formatoSalida);//**XLS
			nombreArchivo = "DetallePrimasAgente."+formatoSalida;
			mensaje = MailServiceSupport.mensajeAgenteDetallePrimas(agente.getPersona().getNombreCompleto(),Integer.valueOf(anio), Integer.valueOf(mes.toString()));
			if(formatoSalida.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
				file.setTipoArchivo(ByteArrayAttachment.TipoArchivo.XLS);					
			}else{
				file.setTipoArchivo(ByteArrayAttachment.TipoArchivo.PDF);
			}
		}			
		try {
			if (report != null && report.getByteArray() != null && agente.getIdAgente()!=null ) {
				file.setContenidoArchivo(report.getByteArray());
				file.setNombreArchivo(agente.getIdAgente()+"_" + nombreArchivo);
				attachment.add(file);
				mailService.sendMailAgenteNotificacion(address, null, null,
						TIPO_DOCUMENTO + " AGENTE " + agente.getIdAgente(),
						mensaje, attachment, "Notificacion", null,
						GenericMailService.T_GENERAL);
			}
		} catch (Exception e) {
			String mensajeExcepetion = "Error: " + e.getMessage();
			log.error(mensajeExcepetion, e);
		}
	}
	
	private void enviarGuiaHonorarioAgentePorCorreo(GuiaHonorariosAgenteView guia,Map<String,Object> genericMap){
		
		String sTipoDocumento = (String) genericMap.get(TIPO_DOCUMENTO);
		String nombreArchivo = "";
		String mensaje = "";
		StringBuilder mes = new StringBuilder();
		List<String> address  = new ArrayList<String>();
		Agente agente = new Agente();
		agente.setId(guia.getIdAgente());
		agente = agenteMidasService.loadById(agente);
		
		final String correo = (agente.getPersona()!=null && agente.getPersona().getEmail()!=null)?agente.getPersona().getEmail():null;
		final String correoAdiconal = (agente.getPersona()!=null && agente.getPersona().getCorreosAdicionales()!=null)?agente.getPersona().getCorreosAdicionales():null;
		
		if(StringUtils.isNotBlank(correoAdiconal)) {
			address.addAll(getListaCorreosAdicionales(correoAdiconal));
		}
		
		if (StringUtils.isNotBlank(correo)) {
			address.add(correo);
		}
		
		List<ByteArrayAttachment> attachment = new ArrayList<ByteArrayAttachment>();
		ByteArrayAttachment file = new ByteArrayAttachment();
		TransporteImpresionDTO report = null;
		mes.delete(0,mes.length());
		
		String anio = (String)genericMap.get("anio");
		mes.append((String)genericMap.get("mes"));
		if(Integer.parseInt(mes.toString())<10){
			 mes.insert(0,"0");				
		}
		
		try{
			mensaje = MailServiceSupport.mensajeAgenteReciboHonorarios(agente.getPersona().getNombreCompleto(),Integer.valueOf(anio), Integer.valueOf(mes.toString()));				
			
				report = generarPlantillaReporte.imprimirReciboHonorarios(guia.getSolicitudChequeId());
				nombreArchivo =String.format("%s-%.0f%s", "ReciboDeHonorarios",guia.getSolicitudChequeId(),".pdf") ;
				file.setTipoArchivo(ByteArrayAttachment.TipoArchivo.PDF);
				file.setContenidoArchivo(report.getByteArray());
				file.setNombreArchivo(agente.getIdAgente()+"_" + nombreArchivo);
				attachment.add(file);
		

			mailService.sendMailAgenteNotificacion(address, null, null,
					sTipoDocumento + " AGENTE " + agente.getIdAgente(),
					mensaje, attachment, "Notificacion", null,
					GenericMailService.T_GENERAL);
		} catch (Exception e) {
			String mensajeExcepetion = "Error: " + e.getMessage();
			log.error(mensajeExcepetion, e);
		}			
	}
}
