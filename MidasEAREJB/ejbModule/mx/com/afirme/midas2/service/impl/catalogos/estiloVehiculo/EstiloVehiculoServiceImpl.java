package mx.com.afirme.midas2.service.impl.catalogos.estiloVehiculo;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.catalogos.estiloVehiculo.EstiloVehiculoDao;
import mx.com.afirme.midas2.service.catalogos.estiloVehiculo.EstiloVehiculoService;
@Stateless
public class EstiloVehiculoServiceImpl implements EstiloVehiculoService{
	
	private EstiloVehiculoDao estiloDao;
	
	@Override
	public String getDescripcionPorClaveAmis(String claveEstilo) {
		return estiloDao.getDescripcionPorClaveAmis(claveEstilo);
	}
	
	public EstiloVehiculoDao getEstiloDao() {
		return estiloDao;
	}
	@EJB
	public void setEstiloDao(EstiloVehiculoDao estiloDao) {
		this.estiloDao = estiloDao;
	}
	
}
