package mx.com.afirme.midas2.service.impl.cargos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.cargos.ConfigCargosDao;
import mx.com.afirme.midas2.domain.cargos.ConfigCargos;
import mx.com.afirme.midas2.dto.Cargos.DetalleCargosView;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.service.cargos.ConfigCargosService;

@Stateless
public class ConfigCargosServiceImpl implements ConfigCargosService {

	private ConfigCargosDao dao;
	
	@Override
	public ConfigCargos aplicar(ConfigCargos configCargos) throws Exception {
		return dao.aplicar(configCargos);
	}

	@Override
	public ConfigCargos loadById(ConfigCargos configCargos) throws Exception {
		return dao.loadById(configCargos);
	}

	@Override
	public ConfigCargos save(ConfigCargos configCargos) throws Exception {
		return dao.save(configCargos);
	}

	@Override
	public ConfigCargos updateEstatus(ConfigCargos configCargos, String estatus)throws Exception {
		return dao.updateEstatus(configCargos, estatus);
	}
	
	@Override
	public List<DetalleCargosView> findByFilters(ConfigCargos configCargos)	throws Exception {
		// TODO Auto-generated method stub
		return dao.findByFilters(configCargos);
	}
	
	@EJB
	public void setDao(ConfigCargosDao dao) {
		this.dao = dao;
	}
	
	@Override
	public void auditarDocumentosEntregadosCargos(Long idCargo,Long idAgente,String nombreAplicacion) throws Exception {
		dao.auditarDocumentosEntregadosCargos(idCargo, idAgente, nombreAplicacion);
	}

	@Override
	public List<EntregoDocumentosView> consultaEstatusDocumentos(Long idCargo,Long idAgente) throws Exception {
		return dao.consultaEstatusDocumentos(idCargo, idAgente);
	}

	@Override
	public void crearYGenerarDocumentosFortimax(ConfigCargos config) throws Exception {
		dao.crearYGenerarDocumentosFortimax(config);
	}
	
}
