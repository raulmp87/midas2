package mx.com.afirme.midas2.service.impl.relaciones;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTOId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionFacadeRemote;
import mx.com.afirme.midas2.domain.catalogos.GrupoVariablesModificacionPrima;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.catalogos.GrupoVariablesModificacionPrimaService;
import mx.com.afirme.midas2.service.relaciones.RelacionesCoberturaService;

@Stateless
public class RelacionesCoberturaServiceImpl implements
		RelacionesCoberturaService {

	@Override
	public List<GrupoVariablesModificacionPrima> obtenerGruposVariablesAsociados(BigDecimal idSeccion, 
			BigDecimal idCobertura) {
		CoberturaSeccionDTOId id = new CoberturaSeccionDTOId();
		id.setIdtocobertura(idCobertura);
		id.setIdtoseccion(idSeccion);
		CoberturaSeccionDTO cobertura = coberturaSeccionFacade.findById(id);		
		return cobertura.getGruposVariablesModificacionPrimas();
	}

	@Override
	public List<GrupoVariablesModificacionPrima> obtenerGruposVariablesDisponibles(BigDecimal idSeccion, 
			BigDecimal idCobertura) {
		
		CoberturaSeccionDTOId id = new CoberturaSeccionDTOId();
		id.setIdtocobertura(idCobertura);
		id.setIdtoseccion(idSeccion);
		CoberturaSeccionDTO cobertura = coberturaSeccionFacade.findById(id);	
		List<GrupoVariablesModificacionPrima> gruposAsociados = cobertura.getGruposVariablesModificacionPrimas();
		
		List<GrupoVariablesModificacionPrima> grupos = grupoVariablesService.findAll();
		
		List<GrupoVariablesModificacionPrima> gruposDisponibles = new ArrayList<GrupoVariablesModificacionPrima>();
		
		for (GrupoVariablesModificacionPrima grupo : grupos) {
			if (!gruposAsociados.contains(grupo)) {
				gruposDisponibles.add(grupo);
			}
		}
		
		return gruposDisponibles;
	}

	@Override
	public RespuestaGridRelacionDTO relacionarGruposVariables(String accion, BigDecimal idSeccion,
			BigDecimal idCobertura, Long idGrupoVariables) {

		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		
		CoberturaSeccionDTOId id = new CoberturaSeccionDTOId();
		id.setIdtocobertura(idCobertura);
		id.setIdtoseccion(idSeccion);
		CoberturaSeccionDTO cobertura = coberturaSeccionFacade.findById(id);	
		
		
		GrupoVariablesModificacionPrima grupo = grupoVariablesService.findById(idGrupoVariables);
		
		if (accion.equals("inserted") || accion.equals("updated")) {
			
			if (!cobertura.getGruposVariablesModificacionPrimas().contains(grupo)) {
				cobertura.getGruposVariablesModificacionPrimas().add(grupo);
			}
			
		} else if (accion.equals("deleted")) {
			if (cobertura.getGruposVariablesModificacionPrimas().contains(grupo)) {
				cobertura.getGruposVariablesModificacionPrimas().remove(grupo);
			}
		}
		
		coberturaSeccionFacade.update(cobertura);
				
		//TODO Preparar la respuesta y regresarla		
		return null;
	}
	
	
	private CoberturaSeccionFacadeRemote coberturaSeccionFacade;
	
	private GrupoVariablesModificacionPrimaService grupoVariablesService;
	
	@EJB
	public void setCoberturaSeccionFacade(CoberturaSeccionFacadeRemote coberturaSeccionFacade) {
		this.coberturaSeccionFacade = coberturaSeccionFacade;
	}
	
	@EJB
	public void setGrupoVariablesService(
			GrupoVariablesModificacionPrimaService grupoVariablesService) {
		this.grupoVariablesService = grupoVariablesService;
	}
	

}
