package mx.com.afirme.midas2.service.impl.personadireccion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.personadireccion.DireccionMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.personadireccion.PaisMidas;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;

@Stateless
public class DireccionMidasServiceImpl implements DireccionMidasService {

	@EJB
	private EntidadService entidadService;
	
	@Override
	public void guardar(DireccionMidas direccion) {
		entidadService.save(direccion);	
	}

	@Override
	public DireccionMidas obtener(Long id) {
		return entidadService.findById(DireccionMidas.class, id);	
	}

	@Override
	public List<PaisMidas> obtenerPaises() {	
		return entidadService.findAll(PaisMidas.class);
	}

	@Override
	public List<EstadoMidas> obtenerEstados(String paisId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pais.id", paisId);
		return entidadService.findByProperties(EstadoMidas.class, params);
	}

	@Override
	public List<CiudadMidas> obtenerCiudad(String estadoId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("estado.id", estadoId);
		return entidadService.findByProperties(CiudadMidas.class, params);
	}

	@Override
	public List<ColoniaMidas> obtenerColoniasPorCiudad(String ciudadId){		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ciudad.id", ciudadId);
		return entidadService.findByProperties(ColoniaMidas.class, params);
	}

	@Override
	public List<ColoniaMidas> obtenerColoniasPorCP(String codigoPostal){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("codigoPostal", codigoPostal);
		return entidadService.findByProperties(ColoniaMidas.class, params);
	}

	public CiudadMidas obtenerCiudadPorCP(String codigoPostal){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("codigoPostal", codigoPostal);
		List<ColoniaMidas> colonias = entidadService.findByProperties(ColoniaMidas.class, params);
		if(colonias != null && colonias.size() > 0){
			return colonias.get(0).getCiudad();
		}
		return null;
	}

	@Override
	public String obtenerCodigoPostalPorColonia(String coloniaId) {
		ColoniaMidas colonia = entidadService.findById(ColoniaMidas.class, coloniaId);
		if(colonia != null){
			return colonia.getCodigoPostal();
		}
		return null;
	}

	@Override
	public CiudadMidas obtenerCiudadPorColonia(String coloniaId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pColoniaId", coloniaId);
		@SuppressWarnings("unchecked")
		List<CiudadMidas> ciudades = entidadService.executeQueryMultipleResult(
				"SELECT city FROM CiudadMidas city INNER JOIN ColoniaMidas col where col.ciudad.id = city.id AND col.id = :pColoniaId", 
				params);
		if(ciudades != null && ciudades.size() > 0){
			return ciudades.get(0);
		}
		return null;		
	}
	
}
