package mx.com.afirme.midas2.service.impl.migracion;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.migracion.MigEndososValidos;
import mx.com.afirme.midas2.domain.migracion.MigEndososValidosEd;
import mx.com.afirme.midas2.domain.migracion.PkgAutMigracion;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.migracion.MigEndososValidosService;
import mx.com.afirme.midas2.service.migracion.MigracionEndosoService;
import mx.com.afirme.midas2.service.tarea.EmisionPendienteService;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class MigracionEndosoServiceImpl implements MigracionEndosoService {

	private static final int STORED_PROCEDURE_EXITO = 0;
	private static final Logger log = Logger.getLogger(MigracionEndosoServiceImpl.class);
	
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	private EntityManager entityManager;
	private PkgAutMigracion pkgAutMigracion;
	private MigEndososValidosService migEndososValidosService;
	private EmisionPendienteService emisionPendienteService;
	private EntidadService entidadService;
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setEmisionPendienteService(
			EmisionPendienteService emisionPendienteService) {
		this.emisionPendienteService = emisionPendienteService;
	}

	@EJB
	public void setPkgAutMigracion(PkgAutMigracion pkgAutMigracion) {
		this.pkgAutMigracion = pkgAutMigracion;
	}
	
	@EJB
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}
	
	@EJB
	public void setMigEndososValidosService(
			MigEndososValidosService migEndososValidosService) {
		this.migEndososValidosService = migEndososValidosService;
	}
	
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
		
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void migrarEndoso(MigEndososValidos migEndososValidos) {
		log.info("Procesando el MigEndosoValidos: " + migEndososValidos);
		Integer idCodResp = null;
		String descResp = null;
		Map<String, Object> out = null;
		try {
			out = pkgAutMigracion.migraEndoso(migEndososValidos);
			idCodResp = ((BigDecimal) out.get("pIdCodResp")).intValue();
			descResp = (String) out.get("pDescResp");			
		} catch(Exception ex) {
			migEndososValidos.setClaveProceso(MigEndososValidos.CLAVE_PROCESO_INVALIDO);
			migEndososValidos.setTexto("No se pudo ejecutar el Stored Procedure de Migración. " + migEndososValidos.getId() + 
					" Excepcion: " + getStrackTraceString(ex));
			migEndososValidosService.actualizar(migEndososValidos);
			throw new RuntimeException(ex);
		}
		

		if (!idCodResp.equals(STORED_PROCEDURE_EXITO)) {
			log.info("El stored procedure regresó el código de error: "
					+ idCodResp + " y la descripción:" + descResp + " "
					+ migEndososValidos.getId());
			migEndososValidos.setClaveProceso(MigEndososValidos.CLAVE_PROCESO_INVALIDO);
			migEndososValidos.setTexto(descResp);
			migEndososValidosService.actualizar(migEndososValidos);
			throw new RuntimeException("El stored procedure no se ejecuto exitosamente.");
		}
		
		try {
			log.info("Se ejecuto el stored procedure exitosamente." + migEndososValidos.getId());
			Long cotizacionContinuityId = ((BigDecimal) out.get("pIdMCotizacionC")).longValue();				
		    String claveTipoEndoso = (String) out.get("pClaveTipoEndoso");
		    claveTipoEndoso = claveTipoEndoso.trim();
			
			entityManager.getEntityManagerFactory().getCache().evictAll();
			
			if(claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_CAP.getClaveTipoEndoso()) || 
					claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_CPP.getClaveTipoEndoso()) || 
					claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_REAP.getClaveTipoEndoso()) || 
					claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_BIN.getClaveTipoEndoso()) || 
					claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_CAI.getClaveTipoEndoso()) || 
					claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_CEP.getClaveTipoEndoso()) || 
					claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_CAE.getClaveTipoEndoso()) ||
					claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_REAE.getClaveTipoEndoso()) || 
					claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_REIC.getClaveTipoEndoso()) || 
					claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_REPP.getClaveTipoEndoso()))
			{
				CotizacionContinuity cotizacionContinuity = entidadService.findById(CotizacionContinuity.class, cotizacionContinuityId);
				emisionPendienteService.procesaEndosoMigracion(new Long(cotizacionContinuity.getNumero()), claveTipoEndoso);
				
			}else
			{			
				Long controlEndosoCotId = ((BigDecimal) out.get("pIdControlEndosoCot")).longValue();
				ControlEndosoCot controlEndosoCot = entityManager.find(ControlEndosoCot.class, controlEndosoCotId);
				//Emitir
				DateTime validoEn = TimeUtils.getDateTime(controlEndosoCot.getValidFrom());
				short tipoEndoso = controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue();
				log.info("Emitiendo el endoso para la cotizacionContinuityId: "
						+ cotizacionContinuityId + " validoEn:" + validoEn
						+ " tipoEndoso:" + tipoEndoso);
				emisionEndosoBitemporalService.emiteEndosoMigracion(cotizacionContinuityId, validoEn, tipoEndoso);				
			}
			
			log.info("Emitido con éxito. " + migEndososValidos.getId());
			
			log.info("Ejecutando ajuste final de endosos de " + migEndososValidos.getId());
			try {
				pkgAutMigracion.ajusteFinalEndoso(migEndososValidos.getId()
						.getIdCotizacion(), migEndososValidos.getId()
						.getIdVersionPol(), migEndososValidos.getIdSolicitud(),
						migEndososValidos.getIdToPoliza());
			}catch(Exception e) {
				final String mensajeError = "No se pudo ejecutar el ajuste final de endoso de " + migEndososValidos.getId();
				log.error(mensajeError);
				migEndososValidos.setTexto(mensajeError);
			}
			migEndososValidos.setClaveProceso(MigEndososValidos.CLAVE_PROCESO_VALIDO);
			migEndososValidosService.actualizar(migEndososValidos);	
		} catch(Exception e) {
			log.info("Ocurrio un error al emitir el endoso " + migEndososValidos.getId(), e);
			migEndososValidos.setClaveProceso(MigEndososValidos.CLAVE_PROCESO_INVALIDO);
			migEndososValidos.setTexto(getStrackTraceString(e));
			migEndososValidosService.actualizar(migEndososValidos);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void migrarEndosoEmisionDelegada(MigEndososValidosEd migEndososValidosEd) {
		log.info("Procesando el MigEndosoValidosEd: " + migEndososValidosEd.getId());
		Integer idCodResp = null;
		String descResp = null;
		Map<String, Object> out = null;
		try {
			out = pkgAutMigracion.migraEndosoEd(migEndososValidosEd);
			idCodResp = ((BigDecimal) out.get("pIdCodResp")).intValue();
			descResp = (String) out.get("pDescResp");			
		} catch(Exception ex) {
			migEndososValidosEd.setClaveProceso(MigEndososValidos.CLAVE_PROCESO_INVALIDO);
			migEndososValidosEd.setTexto("No se pudo ejecutar el Stored Procedure de Migración. " + migEndososValidosEd.getId() + 
					" Excepcion: " + getStrackTraceString(ex));
			migEndososValidosService.actualizar(migEndososValidosEd);
			throw new RuntimeException(ex);
		}
		
		if (!idCodResp.equals(STORED_PROCEDURE_EXITO)) {
			log.info("El stored procedure regresó el código de error: "
					+ idCodResp + " y la descripción:" + descResp + " "
					+ migEndososValidosEd.getId());
			migEndososValidosEd.setClaveProceso(MigEndososValidos.CLAVE_PROCESO_INVALIDO);
			migEndososValidosEd.setTexto(descResp);
			migEndososValidosService.actualizar(migEndososValidosEd);
			throw new RuntimeException("El stored procedure no se ejecuto exitosamente.");
		}
		
		try {
			log.info("Se ejecuto el stored procedure exitosamente." + migEndososValidosEd.getId());
			Long cotizacionContinuityId = ((BigDecimal) out.get("pIdMCotizacionC")).longValue();				
		    String claveTipoEndoso = (String) out.get("pClaveTipoEndoso");
		    claveTipoEndoso = claveTipoEndoso.trim();
			
			entityManager.getEntityManagerFactory().getCache().evictAll();

			if(!claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_CAP.getClaveTipoEndoso()) &&
					!claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_CPP.getClaveTipoEndoso()) && 
					!claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_REAP.getClaveTipoEndoso()) && 
					!claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_CAI.getClaveTipoEndoso()) &&
					!claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_CEP.getClaveTipoEndoso()) && 
					!claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_CAE.getClaveTipoEndoso()) &&
					!claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_REAE.getClaveTipoEndoso()) &&
					!claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_REIC.getClaveTipoEndoso()) && 
					!claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_REPP.getClaveTipoEndoso()))
			{			
			
				if(claveTipoEndoso.equalsIgnoreCase(EmisionPendiente.ClaveTipoEndoso.CLAVETIPOENDOSO_BIN.getClaveTipoEndoso()))
				{
					CotizacionContinuity cotizacionContinuity = entidadService.findById(CotizacionContinuity.class, cotizacionContinuityId);
					emisionPendienteService.procesaEndosoMigracion(new Long(cotizacionContinuity.getNumero()), claveTipoEndoso);
					
				}else
				{
					Long controlEndosoCotId = ((BigDecimal) out.get("pIdControlEndosoCot")).longValue();
					ControlEndosoCot controlEndosoCot = entityManager.find(ControlEndosoCot.class, controlEndosoCotId);
					//Emitir
					DateTime validoEn = TimeUtils.getDateTime(controlEndosoCot.getValidFrom());
					short tipoEndoso = controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue();
					log.info("Emitiendo el endoso para la cotizacionContinuityId: "
							+ cotizacionContinuityId + " validoEn:" + validoEn
							+ " tipoEndoso:" + tipoEndoso);
					emisionEndosoBitemporalService.emiteEndosoMigracion(cotizacionContinuityId, validoEn, tipoEndoso);				
				}
			
			}
			
			log.info("Emitido con éxito. " + migEndososValidosEd.getId());
			
			log.info("Ejecutando ajuste final de endosos de " + migEndososValidosEd.getId());
			try {
				pkgAutMigracion.ajusteFinalEndoso(migEndososValidosEd.getId()
						.getIdCotizacion(), migEndososValidosEd.getId()
						.getIdVersionPol(), migEndososValidosEd.getIdSolicitud(),
						migEndososValidosEd.getIdToPoliza());
			}catch(Exception e) {
				final String mensajeError = "No se pudo ejecutar el ajuste final de endoso de " + migEndososValidosEd.getId();
				log.error(mensajeError);
				migEndososValidosEd.setTexto(mensajeError);
			}
			migEndososValidosEd.setClaveProceso(MigEndososValidos.CLAVE_PROCESO_VALIDO);
			migEndososValidosService.actualizar(migEndososValidosEd);		
		} catch(Exception e) {
			log.info("Ocurrio un error al emitir el endoso " + migEndososValidosEd.getId(), e);
			migEndososValidosEd.setClaveProceso(MigEndososValidosEd.CLAVE_PROCESO_INVALIDO);
			migEndososValidosEd.setTexto(getStrackTraceString(e));
			migEndososValidosService.actualizar(migEndososValidosEd);
			throw new RuntimeException(e);
		}
	}

	
	private String getStrackTraceString(Throwable throwable) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		throwable.printStackTrace(pw);
		return sw.toString(); // stack trace as a string
	}
}
