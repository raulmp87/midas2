package mx.com.afirme.midas2.service.impl.provisiones;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas2.dao.provisiones.ProvisionesBonosAgenteDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProvisionesAgentes;
import mx.com.afirme.midas2.domain.provisiones.ProvisionImportesRamo;
import mx.com.afirme.midas2.domain.provisiones.ToAjusteProvision;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.ProvisionesBonoAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.ProvisionesImportePorRamo;
import mx.com.afirme.midas2.dto.fuerzaventa.AjusteProvisionDto;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.provisiones.ProvisionesBonosAgenteService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class ProvisionesBonosAgenteServiceImpl implements ProvisionesBonosAgenteService{
	private ProvisionesBonosAgenteDao dao;
	@EJB
	private AgenteMidasService agenteMidasService;
	@Resource	
	private TimerService timerService;	
	@EJB
	private SistemaContext sistemaContext;
	public static final Logger LOG = Logger.getLogger(ProvisionesBonosAgenteServiceImpl.class);

	@EJB
	public void setDao(ProvisionesBonosAgenteDao dao) {
		this.dao = dao;
	}
	
	

	@Override
	public ProvisionesAgentes autorizaProvision(ProvisionesAgentes provision) throws Exception {
		return dao.autorizaProvision(provision);
	}

	@Override
	public void creaProvisionAutomatica() throws Exception {
		LOG.info("Iniciando tarea creaProvisionAutomatica");
		dao.creaProvisionAutomatica();
		LOG.info("Tarea creaProvisionAutomatica ejecutada");
	}
	
	@Override
	public List<ProvisionesBonoAgentesView> findByFilterWiew(ProvisionesAgentes filtro)	throws Exception {
		return dao.findByFilterWiew(filtro);
	}

	@Override
	public ProvisionesAgentes loadById(Long idProvision) throws Exception {
		return dao.loadById(idProvision);
	}

	@Override
	public List<ProvisionesImportePorRamo> getRamosByFilterProvision(ProvisionImportesRamo filtrosDetalleProvision) throws Exception {
		return dao.getRamosByFilterProvision(filtrosDetalleProvision);
	}



	@Override
	public ProvisionImportesRamo updateImporteRamo(ProvisionImportesRamo ramo) throws Exception {
		return dao.updateImporteRamo(ramo);
	}


	@Override
	public List<ProvisionImportesRamo> updateImporteXRamo(List<ProvisionImportesRamo> ramos,ProvisionImportesRamo filtrosDetalleProvision) throws Exception {
		return dao.updateImporteXRamo(ramos,filtrosDetalleProvision);
	}



	@Override
	public List<GenericaAgentesView> creaFiltroAgente(Long idProvision) throws Exception {
		return dao.creaFiltroAgente(idProvision);
	}



	@Override
	public List<GenericaAgentesView> creaFiltroBonos(Long idProvision) throws Exception {
		return dao.creaFiltroBonos(idProvision);
	}



	@Override
	public List<GenericaAgentesView> creaFiltroEjecutivo(Long idProvision)	throws Exception {
		return dao.creaFiltroEjecutivo(idProvision);
	}



	@Override
	public List<GenericaAgentesView> creaFiltroGerencia(Long idProvision) throws Exception {
		return dao.creaFiltroGerencia(idProvision);
	}



	@Override
	public List<GenericaAgentesView> creaFiltroGerente(Long idProvision) throws Exception {
		return dao.creaFiltroGerente(idProvision);
	}



	@Override
	public List<GenericaAgentesView> creaFiltroLineaNegocio(Long idProvision) throws Exception {
		return dao.creaFiltroLineaNegocio(idProvision);
	}



	@Override
	public List<GenericaAgentesView> creaFiltroLineaVenta(Long idProvision) throws Exception {
		return dao.creaFiltroLineaVenta(idProvision);
	}



	@Override
	public List<GenericaAgentesView> creaFiltroProducto(Long idProvision)	throws Exception {
		return dao.creaFiltroProducto(idProvision);
	}



	@Override
	public List<GenericaAgentesView> creaFiltroPromotoria(Long idProvision) throws Exception {
		return dao.creaFiltroPromotoria(idProvision);
	}



	@Override
	public List<ProvisionesImportePorRamo> importeTotalPorRamo(Long idProvision) throws Exception {
		return dao.importeTotalPorRamo(idProvision);
	}



	@Override
	public ProvisionesAgentes autorizarProvision(Long idProvision) throws Exception {
		return dao.autorizarProvision(idProvision);
	}



	@Override
	public ProvisionImportesRamo existeRegistroImporteRamo(ProvisionImportesRamo filtrosDetalleProvision) throws Exception {
		return dao.existeRegistroImporteRamo(filtrosDetalleProvision);
	}

	@Override
	public Map<String, Map<String, List<String>>> obtenerCorreos(Long id,
			Long idProceso, Long idMovimiento) {
		return agenteMidasService.obtenerCorreos(id, idProceso, idMovimiento);
	}



	@Override
	public void enviarCorreo(Map<String, Map<String, List<String>>> mapCorreos,
			String mensaje, String tituloMensaje, int tipoTemplate,List<ByteArrayAttachment> attachment) {
		agenteMidasService.enviarCorreo(mapCorreos, mensaje, tituloMensaje, tipoTemplate,attachment);
	}



	@Override
	public void mailThreadMethodSupport(Long id, Long idProceso,
			Long idMovimiento, String mensaje, int tipoTemplate,
			String methodToExcecute) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public AjusteProvisionDto listAjusteProvision(Long idProvision)
			throws Exception {
		// TODO Auto-generated method stub
		return dao.listAjusteProvision(idProvision);
	}



	@Override
	public String saveAjusteProvision(List<ToAjusteProvision> listAjusteProvision)
			throws Exception {
		// TODO Auto-generated method stub
		return dao.saveAjusteProvision(listAjusteProvision);
	}
	
	public void initialize() {
		String timerInfo = "TimerActivaProvisionesBonosAgentes";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
//		if(sistemaContext.getTimerActivo()) {
//			ScheduleExpression expression = new ScheduleExpression();
//			try {
//				//0 5 * * * ?
//				expression.minute(5);
//				expression.hour("*");
//				expression.dayOfWeek("*");
//				
//				timerService.createCalendarTimer(expression, new TimerConfig("TimerActivaProvisionesBonosAgentes", false));
//				
//				LOG.info("Tarea TimerActivaProvisionesBonosAgentes configurado");
//			} catch (Exception e) {
//				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
//			}
//		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerActivaProvisionesBonosAgentes");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerActivaProvisionesBonosAgentes:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		try {
			creaProvisionAutomatica();
		} catch(Exception e) {
			LOG.error("Error al ejecutar tarea TimerActivaProvisionesBonosAgentes", e);
		}
		
	}

}
