package mx.com.afirme.midas2.service.impl.compensaciones;

// default package

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.ConnectException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.sql.DataSource;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas2.dao.compensaciones.CatalogoCompensacionesDao;
import mx.com.afirme.midas2.dao.fuerzaventa.EnvioFacturaAgenteDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.pagos.facturas.RecepcionFacturaDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoCatalogoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.compensaciones.CaEnvioXml;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;
import mx.com.afirme.midas2.domain.compensaciones.DatosDetallePagosPorRecibo;
import mx.com.afirme.midas2.domain.compensaciones.LiquidacionCompensaciones;
import mx.com.afirme.midas2.domain.compensaciones.OrdenPagosDetalle;
import mx.com.afirme.midas2.domain.compensaciones.OrdenesPagoDTO;
import mx.com.afirme.midas2.domain.compensaciones.OrdenesPagoFiltroDTO;
import mx.com.afirme.midas2.domain.compensaciones.ReporteAutorizarOrdenPago;
import mx.com.afirme.midas2.domain.compensaciones.ReporteDetallePrimas;
import mx.com.afirme.midas2.domain.compensaciones.ReporteFiltrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.PolizasProveedor;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.MensajeValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal.OrigenDocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestro;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.DatosHonorariosAgentes;
import mx.com.afirme.midas2.dto.wrapper.CatalogoValorFijoComboDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.compensaciones.LiquidacionCompensacionesService;
import mx.com.afirme.midas2.service.envioxml.EnvioFacturaService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.impl.fuerzaventa.EnvioFacturaAgenteServiceImpl;
import mx.com.afirme.midas2.service.impl.siniestros.pagos.facturas.RecepcionFacturaServiceImpl;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.siniestros.solicitudcheque.SolicitudChequeService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales.TIPO_PROCESO;
import mx.com.afirme.midas2.ws.axosnet.AxnResponse;
import mx.com.afirme.midas2.ws.axosnet.ValResponse;
import mx.com.afirme.midas2.ws.axosnet.WSValidacionesSATPortProxy;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Retenciones.Retencion;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Traslados.Traslado;
import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.io.IOUtils;
import org.apache.xmlbeans.XmlException;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;


/**
 * Facade for entity LiquidacionCompensaciones.
 * @see .LiquidacionCompensaciones
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class LiquidacionCompensacionesServiceImpl extends EntidadHistoricoDaoImpl implements LiquidacionCompensacionesService {
	//property constants
	public static final String ESTATUS = "estatus";
	public static final String ID_TCMONEDA = "idTcmoneda";
	public static final String TIPO_OPERACION = "tipoOperacion";
	public static final String TIPO_LIQUIDACION = "tipoLiquidacion";
	public static final String BANCO = "banco";
	public static final String CUENTA = "cuenta";
	public static final String NO_CHEQUE_REF = "noChequeRef";
	public static final String SOLICITADO_POR = "solicitadoPor";
	public static final String AUTORIZADO_POR = "autorizadoPor";
	public static final String RECHAZADO_POR = "rechazadoPor";
	public static final String IMPORTE_PRIMA = "importePrima";
	public static final String IMPORTE_BS = "importeBs";
	public static final String IMPORTE_DERPOL = "importeDerpol";
	public static final String IMPORTE_CUM_META = "importeCumMeta";
	public static final String IMPORTE_UTILIDAD = "importeUtilidad";
	public static final String SUBTOTAL = "subtotal";
	public static final String IMPORTE_TOTAL = "importeTotal";
	public static final String IVA = "iva";
	public static final String IVA_RETENIDO = "ivaRetenido";
	public static final String ISR = "isr";
	public static final String COMENTARIOS = "comentarios";
	public static final String CODIGO_USUARIO_CREACION = "codigoUsuarioCreacion";
	public static final String CODIGO_USUARIO_MODIFICACION = "codigoUsuarioModificacion";
	public static final String ORIGEN_LIQUIDACION = "origenLiquidacion";
	public static final String BENEFICIARIO = "beneficiario";
	public static final String BANCO_BENEFICIARIO = "bancoBeneficiario";
	public static final String CLABE_BENEFICIARIO = "clabeBeneficiario";
	public static final String RFC_BENEFICIARIO = "rfcBeneficiario";
	public static final String CORREO = "correo";
	public static final String TELEFONO_LADA = "telefonoLada";
	public static final String TELEFONO_NUMERO = "telefonoNumero";
	public static final String ID_REMESA = "idRemesa";
	public static final String NETO_POR_PAGAR = "netoPorPagar";
	public static final String TRAMITE_FACTURA = "TRAMITE-FACTURA";
	
	public static final BigDecimal VALORFACTURA = new BigDecimal(869000);

	public static final String NUMERO_FACTURA_FICTICIA = "xxxxxxxx";
	
    @PersistenceContext private EntityManager entityManager;
	
    private static final Logger LOG = LoggerFactory
	.getLogger(LiquidacionCompensacionesServiceImpl.class);
    @EJB
    private EnvioFacturaService envioFacturaService;
    
    private EnvioFacturaAgenteDao envioFacturaDao;
    
    private ParametroGeneralFacadeRemote parametroGeneral;
    
    @EJB 
    private GenerarPlantillaReporte generarPlantillaReporte;
    
    @EJB 
	private SolicitudChequeService solicitudChequeService ;
    
    private JdbcTemplate jdbcTemplate;
    
    private AgenteMidasService agenteMidasService;
    
    @EJB 
    private RecepcionFacturaService recepcionFacturaService;
    
    @EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
    
    @EJB
	private EntidadService entidadService;
    
    @EJB
	private RecepcionFacturaDao recepcionFacturaDao;
    
    
    @EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
    
    private SimpleJdbcCall agruparOrden;
    private SimpleJdbcCall generarOrdenesPago;
    private SimpleJdbcCall enviarLiquidacionFacura;
    private SimpleJdbcCall liberarLiquidacionFacura;
    private SimpleJdbcCall eliminarLiquidacion;
    private SimpleJdbcCall excluirOrdenPago;
    private SimpleJdbcCall obtenerOrdenPagoProceso;
    private SimpleJdbcCall obtenerOrdenPagoPendXPag;
	private SimpleJdbcCall contabilizarCheques;	
	private SimpleJdbcCall buscarOrdenesporLiquidacion;
	private SimpleJdbcCall buscarLiquidacionXEstatus;	
	private SimpleJdbcCall reciboHonorarios;
	private SimpleJdbcCall reporteAutorizarOrdenPago;
	private SimpleJdbcCall reporteDetallePrima;
	
	@SuppressWarnings("unused")
	private DataSource dataSource;
		/**
	 Perform an initial save of a previously unsaved LiquidacionCompensaciones entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity LiquidacionCompensaciones entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(LiquidacionCompensaciones entity) {
    			LOG.info("saving LiquidacionCompensaciones instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LOG.info("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LOG.info("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent LiquidacionCompensaciones entity.
	  @param entity LiquidacionCompensaciones entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(LiquidacionCompensaciones entity) {
    				LOG.info("deleting LiquidacionCompensaciones instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(LiquidacionCompensaciones.class, entity.getId());
            entityManager.remove(entity);
            			LOG.info("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LOG.info("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved LiquidacionCompensaciones entity and return it or a copy of it to the sender. 
	 A copy of the LiquidacionCompensaciones entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity LiquidacionCompensaciones entity to update
	 @return LiquidacionCompensaciones the persisted LiquidacionCompensaciones entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public LiquidacionCompensaciones update(LiquidacionCompensaciones entity) {
    				LOG.info("updating LiquidacionCompensaciones instance", Level.INFO, null);
	        try {
            LiquidacionCompensaciones result = entityManager.merge(entity);
            			LOG.info("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LOG.info("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public LiquidacionCompensaciones findById(Long id) {
    				LOG.info("finding LiquidacionCompensaciones instance with id: " + id, Level.INFO, null);
	        try {
            LiquidacionCompensaciones instance = entityManager.find(LiquidacionCompensaciones.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LOG.info("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
  
    public List<OrdenPagosDetalle> findByLiquidacionId(Long liquidacionId){
    	LOG.info(" >> findByLiquidacionId()");
    		List<OrdenPagosDetalle> lista = new ArrayList<OrdenPagosDetalle>();
    		try {
    			final StringBuilder queryString = new StringBuilder(200);
    			queryString.append(" SELECT ord.liquidacion_id,LPAD(TP.CODIGOPRODUCTO,2,0)  ||LPAD(TP.CODIGOTIPOPOLIZA,2,0)  ||LPAD(TP.NUMEROPOLIZA,6,0)  AS POLIZA, ord.nombre_contratante, ord.numeroendoso, ");
    			queryString.append(" BC.nombre  AS PRIMABASE, SUM(ord.imp_base_cal_prima) AS IMPORTEPRIMABASE, SUM(ord.importe_prima) AS IMPORTE_PRIMA, ");
    			queryString.append(" NVL( SUM(ord.importe_derpol),0) AS IMPORTE_DERPOL, NVL(SUM(ord.importe_bs),0)    AS IMPORTE_BS, ");
    			queryString.append(" NVL(SUM(ord.importe_prima+ord.importe_derpol+ord.importe_bs),0)  AS IMPORTE_TOTAL,NVL(SUM(ord.recargo),0 ) AS RECARGO,NVL(SUM( ord.comision_sistema), 0)    AS COMISION_SISTEMA, ");
    			queryString.append(" NVL(SUM(ord.comision_ajuste), 0) AS COMISION_AJUSTE,ram.nombre as RAMO,ord.ID_RECIBO ");
    			queryString.append(" FROM MIDAS.ca_ordenpagos_detalle ord" );
    			queryString.append(" INNER JOIN MIDAS.ca_basecalculo bc ON ord.basecalculo_id = bc.id ");
    			queryString.append(" INNER JOIN MIDAS.ca_liquidacion cl ON ord.liquidacion_id = cl.id  ");
    			queryString.append(" INNER JOIN MIDAS.CA_RAMO RAM  ON ord.ramo_id = ram.id ");
    			queryString.append(" INNER JOIN MIDAS.topoliza tp ON ord.idtopoliza        = tp.idtopoliza");
    			queryString.append(" WHERE ord.liquidacion_id  = ").append(liquidacionId);
    			queryString.append(" GROUP BY ord.liquidacion_id, LPAD(TP.CODIGOPRODUCTO,2,0)  ||LPAD(TP.CODIGOTIPOPOLIZA,2,0) ||LPAD(TP.NUMEROPOLIZA,6,0), ord.nombre_contratante, ord.numeroendoso,");
    			queryString.append(" BC.nombre,ram.nombre,ord.ID_RECIBO ");
    			//queryString.append(" order by TP.NUMEROPOLIZA ");
    		Query query = entityManager.createNativeQuery(queryString.toString());
    	List<Object[]> rows = query.getResultList();
    	for(Object[] row : rows){
    		OrdenPagosDetalle op = new OrdenPagosDetalle();
    		op.setLiquidacionId(Long.valueOf(row[0].toString()));
    		op.setNumeroPoliza(row[1].toString());
    	    op.setNombreContratante(row[2].toString());
    	    op.setNumeroEndoso(Long.valueOf(row[3].toString()));
    	    op.setNombreContratante(row[4].toString());
    	    op.setImportePrimaBase(((BigDecimal) row[5]).doubleValue());
    	    op.setImportePrima(((BigDecimal) row [6]).doubleValue());
    	    op.setImporteDerpol(((BigDecimal) row [7]).doubleValue());
    	    op.setImporteBs(((BigDecimal) row [8]).doubleValue());
    	    op.setImporteTotal(((BigDecimal) row [9]).doubleValue());
    	    op.setRecargo(((BigDecimal) row [10]).doubleValue());
    	    op.setComisionSistema(((BigDecimal) row [11]).doubleValue());
    	    op.setComisionAjuste(((BigDecimal) row [12]).doubleValue());
    	    op.setRamo(row[13].toString());
    	    op.setIdRecibo(Long.valueOf(row[14].toString()));
    	    lista.add(op);
    	}
    	
    		
    		 LOG.info("size="+lista.size());
   	      LOG.info("query="+query.toString());
    		} catch(RuntimeException re){
    			LOG.error("---error obtener findByLiquidacionId()", re);
    			throw re;
    		}
    		return lista;
    }
    
  
/**
	 * Find all LiquidacionCompensaciones entities with a specific property value.  
	 
	  @param propertyName the name of the LiquidacionCompensaciones property to query
	  @param value the property value to match
	  	  @return List<LiquidacionCompensaciones> found by query
	 */
    public List<LiquidacionCompensaciones> findByProperty(String propertyName, final Object value
        ) {
    				LOG.info("finding LiquidacionCompensaciones instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from LiquidacionCompensaciones model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LOG.info("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			

    
    
    @SuppressWarnings({ "unchecked", "deprecation" })
    public List<LiquidacionCompensaciones> obtenerOrdenPagoSoliFactura(String estatus) {
    	LOG.info(" >>>obtenerOrdenPagoSoliFactura() ");
    	List<LiquidacionCompensaciones> lista = new ArrayList<LiquidacionCompensaciones>();
    	try{
    		final StringBuilder queryString = new StringBuilder(200);
    		queryString.append(" select LIQ.ID,LIQ.CVE_TIPO_ENTIDAD AS tipobeneficiario ,LIQ.NOMBRE_BENEFICIARIO AS nombre, ");
    		queryString.append(" LIQ.FECHA_CREACION AS fechaCorte, NVL(LIQ.SUBTOTAL,0) AS subtotal, ");
    		queryString.append(" NVL(LIQ.IVA,0) AS iva,NVL(LIQ.iva_retenido,0) AS ivaRetenido, ");
    		queryString.append(" NVL(LIQ.isr,0) AS isr,NVL(LIQ.importe_total,0) As total, ");
    		queryString.append(" opd.estatus_factura,opd.estatus_ordenpago ");
    		queryString.append(" from MIDAS.ca_liquidacion LIQ ");
    		queryString.append(" INNER JOIN MIDAS.CA_ORDENPAGOS_DETALLE OPD ON (OPD.LIQUIDACION_ID = LIQ.ID) ");
    		queryString.append(" where LIQ.ESTATUS= ?1 ");
    		queryString.append(" group by LIQ.ID, LIQ.CVE_TIPO_ENTIDAD , LIQ.NOMBRE_BENEFICIARIO, LIQ.FECHA_CREACION, LIQ.SUBTOTAL ,LIQ.IVA,LIQ.iva_retenido, ");
    		queryString.append(" LIQ.isr, LIQ.importe_total,opd.estatus_factura,opd.estatus_ordenpago ");
    		//queryString.append(" ORDER BY LIQ.id ");
    		String finalQuery=getQueryString(queryString) ;
  		    Query query=entityManager.createNativeQuery(finalQuery);
    		query.setParameter(1, estatus); 
    	    LOG.info("size="+lista.size());
            LOG.info("query="+query.toString());  
            query.setMaxResults(100);
            
            List<Object[]> resultList=query.getResultList();
            for (Object[] result : resultList){
            	LiquidacionCompensaciones liqCom = new LiquidacionCompensaciones();
            	liqCom.setId(new Long(((BigDecimal)result[0]).toString()));
            	liqCom.setCveTipoEntidad(result[1].toString());
            	liqCom.setNombreBeneficiario(result[2].toString());
            	liqCom.setFecha(Utilerias.getDateFromString(result[3].toString(),SistemaPersistencia.FORMATO_FECHA_ORACLE));
            	liqCom.setSubtotal(new Double(result[4].toString()));
            	liqCom.setIva(new Double(result[5].toString()));
            	liqCom.setIvaRetenido(new Double(result[6].toString()));
            	liqCom.setIsr(new Double(result[7].toString()));
            	liqCom.setImporteTotal(new Double(result[8].toString()));
            	liqCom.setEstatusFactura(new Short(((BigDecimal)result[9]).toString()));
                liqCom.setEstatusOrdenPago(new Short(((BigDecimal)result[10]).toString()));
                lista.add(liqCom);
            }
    	}catch(RuntimeException re){
    		LOG.error("--obtenerOrdenPagoSoliFactura()", re);
    		throw re;
    	}
    	return lista;
	}			
    
	public List<LiquidacionCompensaciones> findByEstatus(Object estatus) {
		return findByProperty(ESTATUS, estatus);
	}
	
	public List<LiquidacionCompensaciones> findByIdTcmoneda(Object idTcmoneda) {
		return findByProperty(ID_TCMONEDA, idTcmoneda);
	}
	
	public List<LiquidacionCompensaciones> findByTipoOperacion(Object tipoOperacion) {
		return findByProperty(TIPO_OPERACION, tipoOperacion);
	}
	
	public List<LiquidacionCompensaciones> findByTipoLiquidacion(Object tipoLiquidacion) {
		return findByProperty(TIPO_LIQUIDACION, tipoLiquidacion);
	}
	
	public List<LiquidacionCompensaciones> findByBanco(Object banco) {
		return findByProperty(BANCO, banco);
	}
	
	public List<LiquidacionCompensaciones> findByCuenta(Object cuenta) {
		return findByProperty(CUENTA, cuenta);
	}
	
	public List<LiquidacionCompensaciones> findByNoChequeRef(Object noChequeRef) {
		return findByProperty(NO_CHEQUE_REF, noChequeRef);
	}
	
	public List<LiquidacionCompensaciones> findBySolicitadoPor(Object solicitadoPor) {
		return findByProperty(SOLICITADO_POR, solicitadoPor);
	}
	
	public List<LiquidacionCompensaciones> findByAutorizadoPor(Object autorizadoPor) {
		return findByProperty(AUTORIZADO_POR, autorizadoPor);
	}
	
	public List<LiquidacionCompensaciones> findByRechazadoPor(Object rechazadoPor) {
		return findByProperty(RECHAZADO_POR, rechazadoPor);
	}
	
	public List<LiquidacionCompensaciones> findByImportePrima(Object importePrima) {
		return findByProperty(IMPORTE_PRIMA, importePrima);
	}
	
	public List<LiquidacionCompensaciones> findByImporteBs(Object importeBs) {
		return findByProperty(IMPORTE_BS, importeBs);
	}
	
	public List<LiquidacionCompensaciones> findByImporteDerpol(Object importeDerpol) {
		return findByProperty(IMPORTE_DERPOL, importeDerpol);
	}
	
	public List<LiquidacionCompensaciones> findByImporteCumMeta(Object importeCumMeta) {
		return findByProperty(IMPORTE_CUM_META, importeCumMeta);
	}
	
	public List<LiquidacionCompensaciones> findByImporteUtilidad(Object importeUtilidad) {
		return findByProperty(IMPORTE_UTILIDAD, importeUtilidad);
	}
	
	public List<LiquidacionCompensaciones> findBySubtotal(Object subtotal) {
		return findByProperty(SUBTOTAL, subtotal);
	}
	
	public List<LiquidacionCompensaciones> findByImporteTotal(Object importeTotal) {
		return findByProperty(IMPORTE_TOTAL, importeTotal);
	}
	
	public List<LiquidacionCompensaciones> findByIva(Object iva) {
		return findByProperty(IVA, iva);
	}
	
	public List<LiquidacionCompensaciones> findByIvaRetenido(Object ivaRetenido) {
		return findByProperty(IVA_RETENIDO, ivaRetenido);
	}
	
	public List<LiquidacionCompensaciones> findByIsr(Object isr) {
		return findByProperty(ISR, isr);
	}
	
	public List<LiquidacionCompensaciones> findByComentarios(Object comentarios) {
		return findByProperty(COMENTARIOS, comentarios);
	}
	
	public List<LiquidacionCompensaciones> findByCodigoUsuarioCreacion(Object codigoUsuarioCreacion) {
		return findByProperty(CODIGO_USUARIO_CREACION, codigoUsuarioCreacion);
	}
	
	public List<LiquidacionCompensaciones> findByCodigoUsuarioModificacion(Object codigoUsuarioModificacion) {
		return findByProperty(CODIGO_USUARIO_MODIFICACION, codigoUsuarioModificacion);
	}
	
	public List<LiquidacionCompensaciones> findByOrigenLiquidacion(Object origenLiquidacion) {
		return findByProperty(ORIGEN_LIQUIDACION, origenLiquidacion);
	}
	
	public List<LiquidacionCompensaciones> findByBeneficiario(Object beneficiario) {
		return findByProperty(BENEFICIARIO, beneficiario);
	}
	
	public List<LiquidacionCompensaciones> findByBancoBeneficiario(Object bancoBeneficiario) {
		return findByProperty(BANCO_BENEFICIARIO, bancoBeneficiario);
	}
	
	public List<LiquidacionCompensaciones> findByClabeBeneficiario(Object clabeBeneficiario) {
		return findByProperty(CLABE_BENEFICIARIO, clabeBeneficiario);
	}
	
	public List<LiquidacionCompensaciones> findByRfcBeneficiario(Object rfcBeneficiario) {
		return findByProperty(RFC_BENEFICIARIO, rfcBeneficiario);
	}
	
	public List<LiquidacionCompensaciones> findByCorreo(Object correo) {
		return findByProperty(CORREO, correo);
	}
	
	public List<LiquidacionCompensaciones> findByTelefonoLada(Object telefonoLada) {
		return findByProperty(TELEFONO_LADA, telefonoLada);
	}
	
	public List<LiquidacionCompensaciones> findByTelefonoNumero(Object telefonoNumero) {
		return findByProperty(TELEFONO_NUMERO, telefonoNumero);
	}
	
	public List<LiquidacionCompensaciones> findByIdRemesa(Object idRemesa) {
		return findByProperty(ID_REMESA, idRemesa);
	}
	
	public List<LiquidacionCompensaciones> findByNetoPorPagar(Object netoPorPagar) {
		return findByProperty(NETO_POR_PAGAR, netoPorPagar);
	}
	
	
	/**
	 * Find all LiquidacionCompensaciones entities.
	  	  @return List<LiquidacionCompensaciones> all LiquidacionCompensaciones entities
	 */
	@SuppressWarnings("unchecked")
	public List<LiquidacionCompensaciones> findAll() {
					LOG.info("finding all LiquidacionCompensaciones instances", Level.INFO, null);
			try {
			final String queryString = "select model from LiquidacionCompensaciones model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LOG.info("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@Resource(name="jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {						
		//Consult agruparOrden en PKG_CA_LIQUIDACION de MIDAS
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.agruparOrden = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("PKG_CA_LIQUIDACION")
		.withProcedureName("agruparOrden")
		.declareParameters(
				new SqlParameter("pRamo", Types.VARCHAR),
				new SqlParameter("pFecIni", Types.DATE),
				new SqlParameter("pFecFin", Types.DATE),
				new SqlParameter("pAgente", Types.NUMERIC),
				new SqlParameter("pPromotor", Types.NUMERIC),
				new SqlParameter("pProveedor", Types.NUMERIC),
				new SqlParameter("pContratante", Types.VARCHAR),
				new SqlParameter("pGerencia", Types.NUMERIC),
				new SqlParameter("pNegocio", Types.NUMERIC),
				new SqlParameter("pGrupo", Types.NUMERIC),
				new SqlParameter("pTipoCom", Types.VARCHAR),
				new SqlParameter("pUsuario", Types.VARCHAR),
				new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
				new SqlOutParameter("pDesc_Resp", Types.VARCHAR),	
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Object>() {
					@Override
					public OrdenesPagoDTO mapRow(ResultSet rs, int index) throws SQLException {
						OrdenesPagoDTO ordenPagos = new OrdenesPagoDTO();
						ordenPagos.setId(rs.getLong("ID"));
						ordenPagos.setTipobeneficiario(rs.getString("TIPO_BENEFICIARIO"));
						ordenPagos.setClaveNombre(rs.getString("CLAVE_NOMBRE"));
						ordenPagos.setImportePrima(rs.getDouble("IMPORTE_PRIMA"));
						ordenPagos.setImporteBs(rs.getDouble("IMPORTE_BS"));
						ordenPagos.setImporteDerpol(rs.getDouble("IMPORTE_DERPOL"));
						ordenPagos.setImporteCumMeta(rs.getDouble("IMPORTE_CUM_META"));
						ordenPagos.setImporteUtilidad(rs.getDouble("IMPORTE_UTILIDAD"));
						ordenPagos.setImporteTotal(rs.getDouble("TOTAL"));
						return ordenPagos;
					}
		}));
		//Consult generarOrdenesPago en PKG_CA_CONTABILIDAD de MIDAS
		this.generarOrdenesPago = new SimpleJdbcCall(jdbcTemplate)
			.withSchemaName("MIDAS")
			.withCatalogName("PKG_CA_LIQUIDACION")
			.withProcedureName("generarOrdenesPago")
				.declareParameters(
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Resp", Types.VARCHAR),
						new SqlParameter("pLiquidacionesIds", Types.VARCHAR),
						new SqlOutParameter("p_cursorPolizasProvee", OracleTypes.CURSOR, new RowMapper<Object>() {
							@Override
							public PolizasProveedor mapRow(ResultSet rs, int index) throws SQLException {
								PolizasProveedor polizasProveedorX = new PolizasProveedor();
								polizasProveedorX.setIdPoliza(String.valueOf(rs.getBigDecimal("ID_POLIZA") + ", "));
								return polizasProveedorX;
							}
				}));
		
		//Consult cotabilizarCheques en PKG_CA_CONTABILIDAD de MIDAS
		this.contabilizarCheques = new SimpleJdbcCall(jdbcTemplate)
			.withSchemaName("MIDAS")
			.withCatalogName("PKG_CA_CONTABILIDAD")
			.withProcedureName("contabilizarCheques")
				.declareParameters(
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Resp", Types.VARCHAR),
						new SqlParameter("pLiquidacionesIds", Types.VARCHAR),
						new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Object>() {
							@Override
							public SolicitudChequeSiniestro mapRow(ResultSet rs, int index) throws SQLException {
								SolicitudChequeSiniestro solicitudCheque = new SolicitudChequeSiniestro();
								solicitudCheque.setId(rs.getLong("ID"));
								solicitudCheque.setIdSolCheque(rs.getLong("IDSOLCHEQUE"));
								solicitudCheque.setOrigenSolicitud(rs.getString("ORIGEN_SOLICITUD"));
								return solicitudCheque;
							}
				})
						);
		//Consult excluirOrdenPago en PKG_CA_LIQUIDACION de MIDAS
		this.excluirOrdenPago = new SimpleJdbcCall(jdbcTemplate)
			.withSchemaName("MIDAS")
			.withCatalogName("PKG_CA_LIQUIDACION")
			.withProcedureName("excluirOrdenPago")
				.declareParameters(
						new SqlParameter("pIdLiquidacion", Types.NUMERIC),
						new SqlParameter("pIdRecibo", Types.NUMERIC),
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Resp", Types.VARCHAR)
			);
		
		//Consult obtenerOrdenPagoProceso en PKG_CA_CONTABILIDAD de MIDAS
		this.obtenerOrdenPagoProceso = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("PKG_CA_LIQUIDACION")
		.withProcedureName("obtenerOrdenPagoProceso")
		.declareParameters(
				new SqlParameter("pUsuario", Types.VARCHAR),
				new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
				new SqlOutParameter("pDesc_Resp", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Object>() {
					@Override
					public OrdenesPagoDTO mapRow(ResultSet rs, int index) throws SQLException {
						OrdenesPagoDTO ordenPagos = new OrdenesPagoDTO();
						ordenPagos.setId(rs.getLong("ID"));
						ordenPagos.setTipobeneficiario(rs.getString("TIPO_BENEFICIARIO"));
						ordenPagos.setClaveNombre(rs.getString("CLAVE_NOMBRE"));
						ordenPagos.setImportePrima(rs.getDouble("IMPORTE_PRIMA"));
						ordenPagos.setImporteBs(rs.getDouble("IMPORTE_BS"));
						ordenPagos.setImporteDerpol(rs.getDouble("IMPORTE_DERPOL"));
						ordenPagos.setImporteCumMeta(rs.getDouble("IMPORTE_CUM_META"));
						ordenPagos.setImporteUtilidad(rs.getDouble("IMPORTE_UTILIDAD"));
						ordenPagos.setImporteTotal(rs.getDouble("TOTAL"));
						return ordenPagos;
					}
		}));
		
		//Consult obtenerOrdenPagoProceso en PKG_CA_CONTABILIDAD de MIDAS
		this.obtenerOrdenPagoPendXPag = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("PKG_CA_LIQUIDACION")
		.withProcedureName("obtenerOrdenPagoPendXPag")
		.declareParameters(
				new SqlParameter("pUsuario", Types.VARCHAR),
				new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
				new SqlOutParameter("pDesc_Resp", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Object>() {
					@Override
					public OrdenesPagoDTO mapRow(ResultSet rs, int index) throws SQLException {
						OrdenesPagoDTO ordenPagos = new OrdenesPagoDTO();
						ordenPagos.setId(rs.getLong("ID"));
						ordenPagos.setTipobeneficiario(rs.getString("TIPO_BENEFICIARIO"));
						ordenPagos.setClaveNombre(rs.getString("CLAVE_NOMBRE"));
						ordenPagos.setImportePrima(rs.getDouble("IMPORTE_PRIMA"));
						ordenPagos.setImporteBs(rs.getDouble("IMPORTE_BS"));
						ordenPagos.setImporteDerpol(rs.getDouble("IMPORTE_DERPOL"));
						ordenPagos.setImporteCumMeta(rs.getDouble("IMPORTE_CUM_META"));
						ordenPagos.setImporteUtilidad(rs.getDouble("IMPORTE_UTILIDAD"));
						ordenPagos.setImporteTotal(rs.getDouble("TOTAL"));
						return ordenPagos;
					}
		}));
		
		//Consult enviarLiquidacionFacura en PKG_CA_LIQUIDACION de MIDAS
		this.enviarLiquidacionFacura = new SimpleJdbcCall(jdbcTemplate)
			.withSchemaName("MIDAS")
			.withCatalogName("PKG_CA_LIQUIDACION")
			.withProcedureName("enviarLiquidacionFacura")
				.declareParameters(
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Resp", Types.VARCHAR),
						new SqlParameter("pIdLiquidacion ", Types.NUMERIC)
			);
		
		//Consult liberarLiquidacionFacura en PKG_CA_LIQUIDACION de MIDAS
		this.liberarLiquidacionFacura = new SimpleJdbcCall(jdbcTemplate)
			.withSchemaName("MIDAS")
			.withCatalogName("PKG_CA_LIQUIDACION")
			.withProcedureName("liberarLiquidacionFacura")
				.declareParameters(
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Resp", Types.VARCHAR),
						new SqlParameter("pIdLiquidacion ", Types.NUMERIC),
						new SqlParameter("pEstatus",Types.VARCHAR),
						new SqlParameter("pNombreArchivo",Types.VARCHAR)
			);
		
		//Consult eliminarLiquidacion en PKG_CA_LIQUIDACION de MIDAS
		this.eliminarLiquidacion = new SimpleJdbcCall(jdbcTemplate)
			.withSchemaName("MIDAS")
			.withCatalogName("PKG_CA_LIQUIDACION")
			.withProcedureName("eliminarLiquidacion")
				.declareParameters(
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Resp", Types.VARCHAR),
						new SqlParameter("pIdLiquidacion ", Types.NUMERIC)
			);
		//Consulta ORDENPAGOS REFERENTE A LIQUIDACION
		this.buscarOrdenesporLiquidacion = new SimpleJdbcCall(jdbcTemplate)
			.withSchemaName("MIDAS")
			.withCatalogName("PKG_CA_ORDENPAGO")
			.withProcedureName("obtenerDetallePagosxRecibo")
				.declareParameters(
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Resp", Types.VARCHAR),
						new SqlParameter("pIdLiquidacion", Types.VARCHAR),
						new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Object>() {
							@Override
							public DatosDetallePagosPorRecibo mapRow(ResultSet rs, int index) throws SQLException {
								DatosDetallePagosPorRecibo objeto = new DatosDetallePagosPorRecibo();
								objeto.setFolio(rs.getBigDecimal("FOLIO"));
								objeto.setCompensacionId(rs.getBigDecimal("COMPENSACION_ID"));
								objeto.setRamo(rs.getString("RAMO"));
								objeto.setPoliza(rs.getString("POLIZA"));
								objeto.setRecibo(rs.getBigDecimal("RECIBO"));
								objeto.setEndoso(rs.getBigDecimal("ENDOSO"));
								objeto.setFechaAplicacionPago(rs.getDate("FECHA_APLICACION_PAGO"));
								objeto.setTipoPago(rs.getString("TIPO_PAGO"));
								objeto.setTipoBeneficiario(rs.getString("TIPO_BENEFICIARIO"));
								objeto.setClave(rs.getBigDecimal("CLAVE"));
								objeto.setNombreBeneficiario(rs.getString("NOMBRE_BENEFICIARIO"));
								objeto.setBaseCalculo(rs.getString("BASE_CALCULO"));
								objeto.setImportePrima(rs.getBigDecimal("IMPORTE_PRIMA"));
								objeto.setPorcentajecompprima(rs.getBigDecimal("PORCENTAJECOMPPRIMA"));
								objeto.setImporteBs(rs.getBigDecimal("IMPORTE_BS"));
								objeto.setPorcentajeBajaSin(rs.getBigDecimal("PORCENTAJE_BAJA_SIN"));
								objeto.setImporteDerpol(rs.getBigDecimal("IMPORTE_DERPOL"));
								objeto.setPorcComDerpol(rs.getBigDecimal("PORC_COM_DERPOL"));
								objeto.setImpCumlimientoMeta(rs.getBigDecimal("IMP_CUMLIMIENTO_META"));
								objeto.setPorcCumplimientoMeta(rs.getBigDecimal("PORC_CUMPLIMIENTO_META"));
								objeto.setImporteUtilidad(rs.getBigDecimal("IMPORTE_UTILIDAD"));
								objeto.setPorcUtilidad(rs.getBigDecimal("PORC_UTILIDAD"));
								objeto.setSubtotal(rs.getBigDecimal("SUBTOTAL"));
								objeto.setIva(rs.getBigDecimal("IVA"));
								objeto.setIvaRetenido(rs.getBigDecimal("IVA_RETENIDO"));
								objeto.setIsr(rs.getBigDecimal("ISR"));
								objeto.setImporteTotal(rs.getBigDecimal("IMPORTE_TOTAL"));
								return objeto;
							}
				}));
		
		this.buscarLiquidacionXEstatus = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("PKG_CA_LIQUIDACION")
		.withProcedureName("buscarLiquidacionXEstatus")
			.declareParameters(
					new SqlParameter("pIdLiquidacion", Types.NUMERIC),
					new SqlParameter("pRamo", Types.VARCHAR),
					new SqlParameter("pNegocio", Types.NUMERIC),
					new SqlParameter("pContratante", Types.VARCHAR),
					new SqlParameter("pAgente", Types.NUMERIC),
					new SqlParameter("pPromotor", Types.NUMERIC),
					new SqlParameter("pProveedor", Types.NUMERIC),
					new SqlParameter("pGerencia", Types.NUMERIC),
					new SqlParameter("pEstatusFactura", Types.NUMERIC),
					new SqlParameter("pEstatusOrdenPago", Types.NUMERIC),
					new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
					new SqlOutParameter("pDesc_Resp", Types.VARCHAR),
					new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Object>() {
						@Override
						public OrdenesPagoDTO mapRow(ResultSet rs, int index) throws SQLException {
							OrdenesPagoDTO objeto = new OrdenesPagoDTO();
							objeto.setFolio(rs.getLong("ID"));
							objeto.setRamo(rs.getString("RAMO"));
							objeto.setNegocio(rs.getString("NEGOCIO_ID"));
							objeto.setContratante(rs.getString("NOMBRE_CONTRATANTE"));
							objeto.setTipoEntidad(rs.getString("CVE_TIPO_ENTIDAD"));
							objeto.setEstatusFactura(rs.getString("ESTATUS_FACTURA"));
							objeto.setEstatusOrdenpago(rs.getString("ESTATUS_ORDENPAGO"));
							objeto.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
							objeto.setImporteTotal(rs.getDouble("IMPORTE_TOTAL"));
							objeto.setSolicitudCheque((rs.getInt("SOL_CHEQUE")==0)?null:new Integer(rs.getInt("SOL_CHEQUE")));							
							return objeto;
						}
			}));
		
		  this.reciboHonorarios = new SimpleJdbcCall(jdbcTemplate)
			.withSchemaName("MIDAS")
			.withCatalogName("PKG_CA_REPORTES")
			.withProcedureName("reciboHonorarios").declareParameters(		
					    new SqlParameter("pAnioMes", Types.NUMERIC),
						new SqlParameter("pCveAgente", Types.NUMERIC),
					    new SqlParameter("pCvePromotor", Types.NUMERIC),
					    new SqlParameter("pCveProveedor", Types.NUMERIC),
						new SqlOutParameter("pNomBeneficiario", Types.VARCHAR),
						new SqlOutParameter("pTipoBeneficiario", Types.VARCHAR),
						new SqlOutParameter("pIdBeneficiario",  Types.NUMERIC),
						new SqlOutParameter("pFolio",  Types.NUMERIC),
						new SqlOutParameter("pImpTotalLetra",  Types.NUMERIC),
						new SqlOutParameter("pImpGravado",  Types.NUMERIC),
					    new SqlOutParameter("pImpExento",  Types.NUMERIC),
					    new SqlOutParameter("pSubTotal",  Types.NUMERIC),
						new SqlOutParameter("pIva",  Types.NUMERIC),
						new SqlOutParameter("pISR",  Types.NUMERIC),
						new SqlOutParameter("pIvaRetenido",  Types.NUMERIC),
						new SqlOutParameter("pImpTotal",  Types.NUMERIC)						
		           );	
			this.reporteAutorizarOrdenPago = new SimpleJdbcCall(jdbcTemplate)
			.withSchemaName("MIDAS")
			.withCatalogName("PKG_CA_REPORTES")
			.withProcedureName("autorizarOrdenPagos")
				.declareParameters(
						new SqlParameter("pIdLiquidacion", Types.VARCHAR),
						new SqlOutParameter("pImpTotalComp", Types.NUMERIC),
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Resp", Types.VARCHAR),
						new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Object>() {
							@Override
							public ReporteAutorizarOrdenPago mapRow(ResultSet rs, int index) throws SQLException {
								ReporteAutorizarOrdenPago objeto = new ReporteAutorizarOrdenPago();
								objeto.setGerencia(rs.getString("GERENCIA"));
								objeto.setPromotoria(rs.getString("PROMOTORIA"));
								objeto.setIdBeneficiario(rs.getLong("IDENTIFICADOR_BEN_ID"));
								objeto.setNombreBeneficiario(rs.getString("NOMBRE_BENEFICIARIO"));
								objeto.setImporteTotal(rs.getDouble("IMPORTE_TOTAL"));
								objeto.setNombreBanco(rs.getString("BANCO"));
								objeto.setNoCuenta(rs.getString("CUENTA"));							
								
								return objeto;
							}
				}));
			this.reporteDetallePrima = new SimpleJdbcCall(jdbcTemplate)
			.withSchemaName("MIDAS")
			.withCatalogName("PKG_CA_REPORTES")
			.withProcedureName("obtenerReporteDetallePrima")
				.declareParameters(
						new SqlParameter("pAnioMes", Types.NUMERIC),
						new SqlParameter("pCveAgente", Types.NUMERIC),
						new SqlParameter("pCvePromotor", Types.NUMERIC),
						new SqlParameter("pCveProveedor", Types.NUMERIC),
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Resp", Types.VARCHAR),
						new SqlOutParameter("pIdentificadorBenId", Types.NUMERIC),
						new SqlOutParameter("pNomBeneficiario", Types.VARCHAR),
						new SqlOutParameter("pRfcBeneficiario", Types.VARCHAR),
						new SqlOutParameter("pDomicilio", Types.VARCHAR),
						new SqlOutParameter("pCentroOperacion", Types.VARCHAR),
						new SqlOutParameter("pGerencia", Types.VARCHAR),
						new SqlOutParameter("pEjectutivo", Types.VARCHAR),
						new SqlOutParameter("pPromotoria", Types.VARCHAR),
						new SqlOutParameter("pNumCedula", Types.VARCHAR),
						new SqlOutParameter("pVenceCedula", Types.VARCHAR),
						new SqlOutParameter("estatusAgente", Types.VARCHAR),
						
						new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Object>() {
							@Override
							public ReporteDetallePrimas mapRow(ResultSet rs, int index) throws SQLException {
								ReporteDetallePrimas objeto = new ReporteDetallePrimas();
								objeto.setDiaCorte(rs.getString("FECHA_CORTE")+"");
								objeto.setNum_poliza(rs.getString("IDTOPOLIZA"));
								objeto.setIdRecibo(rs.getBigDecimal("ID_RECIBO").toString());
								objeto.setNomRamo(rs.getString("NOMBRE"));
								objeto.setNomSubramo(rs.getString("ID_SUBR_CONTABLE"));
								objeto.setDesc_moneda(rs.getString("DESC_MONEDA"));
								objeto.setImp_prima_neta((Double)rs.getDouble("PRIMA_NETA"));
								objeto.setImp_rcgos_pagofr((Double)rs.getDouble("RECARGOS"));
								objeto.setImp_prima_total((Double)rs.getDouble("TOTAL"));
								objeto.setImp_prima_riesgo((Double)rs.getDouble("IMP_BASE_CAL_RIESGO"));
								objeto.setImp_prima_cedida((Double)rs.getDouble("IMP_BASE_CAL_CEDIDA"));
								objeto.setImp_der_pol_bc((Double)rs.getDouble("IMP_DERECHOS"));
								objeto.setImp_prima((Double)rs.getDouble("IMPORTE_PRIMA"));
								objeto.setImp_B_S((Double)rs.getDouble("IMPORTE_BS"));
								objeto.setImp_Der_Pol((Double)rs.getDouble("IMPORTE_DERPOL"));
								objeto.setSubTotal((Double)rs.getDouble("SUBTOTAL"));
								objeto.setImp_iva_acred((Double)rs.getDouble("IVA"));
								objeto.setImp_iva_ret((Double)rs.getDouble("IVA_RETENIDO"));
								objeto.setImp_isr((Double)rs.getDouble("ISR"));
								objeto.setTotalporpoliza((Double)rs.getDouble("TOTALXPOLIZA"));
								objeto.setDiaAplicacion(rs.getString("DIA_APLICACION"));
								objeto.setNum_endoso(rs.getInt("NUM_ENDOSO")+"");
								objeto.setImp_comis_agte(Double.parseDouble(rs.getInt("IMP_COM_AGT")+""));
								objeto.setForma_pago(rs.getString("DESCRIPCIONFORMAPAGO"));
								objeto.setTotalpormovimiento(rs.getDouble("TOTAL_MOVIMIENTO"));
								objeto.setComis_acum_dia(rs.getDouble("COMISION_ACUMULADA"));
							
								return objeto;
							}
				}));
			
		
	}
	
	@SuppressWarnings("unchecked")
	public List<OrdenesPagoDTO> buscarLiquidacionXEstatus(OrdenesPagoDTO  param) {
		List<OrdenesPagoDTO> lstOrden = null;
		LOG.info(">> entrando buscarLiquidacionXEstatus()");
		Map<String, Object> execute = null;
			try {
				if (param != null) {
					LOG.info("pIdLiquidacion=>"+param.getId());
				    LOG.info("pRamo=>"+param.getRamo());
				    LOG.info("pNegocio=>"+param.getNegocio());
				    LOG.info("pContratante=>"+param.getNombreContratante());
				    LOG.info("pAgente=>"+param.getAgente());
				    LOG.info("pPromotor=>"+param.getPromotor());
				    LOG.info("pProveedor=>"+param.getProveedor());
				    LOG.info("pGerencia=>"+param.getGerencia());				    
				    LOG.info("pEstatusFactura=>"+param.getEstatusFactura());
				    LOG.info("pEstatusOrdenPago=>"+param.getEstatusOrdenpago());
					MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
					sqlParameter.addValue("pIdLiquidacion", param.getId());
					sqlParameter.addValue("pRamo", param.getRamo());
					sqlParameter.addValue("pNegocio", param.getNegocio());
					sqlParameter.addValue("pContratante", param.getNombreContratante());
					sqlParameter.addValue("pAgente", param.getAgente());
					sqlParameter.addValue("pPromotor", param.getPromotor());
					sqlParameter.addValue("pProveedor", param.getProveedor());
					sqlParameter.addValue("pGerencia", param.getGerencia());
					sqlParameter.addValue("pEstatusFactura", param.getEstatusFactura());
					sqlParameter.addValue("pEstatusOrdenPago", param.getEstatusOrdenpago());
					execute = buscarLiquidacionXEstatus.execute(sqlParameter);					
					lstOrden = (List<OrdenesPagoDTO>) execute.get("pCursor");
					BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
					String pDesc_Resp = (String) execute.get("pDesc_Resp");
					LOG.info("Desc_Resp=>"+pDesc_Resp);
					LOG.info("idCodResp=>"+idCodResp);
					LOG.info("listSize=>"+lstOrden.size());
				}else{
					LOG.error("param Regresa Vacio");
				}
			} catch (Exception e) {
				LOG.error("Excepcion general en buscarLiquidacionXEstatus()", e);
			}
		return lstOrden;
		
	}
	@SuppressWarnings("unchecked")
	public List<OrdenesPagoDTO> agruparOrden(OrdenesPagoDTO  param) {
		List<OrdenesPagoDTO> lstOrden = null;
		Map<String, Object> execute = null;
			try {
				if (param != null) {
					LOG.info("pRamo=>"+param.getRamo());
				    LOG.info("pFecIni=>"+param.getFechaInicio());
				    LOG.info("pFecFin=>"+param.getFechaFin());
				    LOG.info("pAgente=>"+param.getAgente());
				    LOG.info("pPromotor=>"+param.getPromotor());
				    LOG.info("pProveedor=>"+param.getProveedor());
				    LOG.info("pContratante=>"+param.getContratante());
				    LOG.info("pGerencia=>"+param.getGerencia());
				    LOG.info("pNegocio=>"+param.getNegocio());
				    LOG.info("pGrupo=>"+param.getGrupo());
				    LOG.info("pTipoCom=>"+param.getConcepto());
				    LOG.info("pUsuario=>"+param.getUsuario());
					MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
					sqlParameter.addValue("pRamo", param.getRamo());
					sqlParameter.addValue("pFecIni", param.getFechaInicio());
					sqlParameter.addValue("pFecFin", param.getFechaFin());
					sqlParameter.addValue("pAgente", param.getAgente());
					sqlParameter.addValue("pPromotor", param.getPromotor());
					sqlParameter.addValue("pProveedor", param.getProveedor());
					sqlParameter.addValue("pContratante", param.getContratante());
					sqlParameter.addValue("pGerencia", param.getGerencia());
					sqlParameter.addValue("pNegocio", param.getNegocio());
					sqlParameter.addValue("pGrupo", param.getGrupo());
					sqlParameter.addValue("pTipoCom", param.getConcepto());
					sqlParameter.addValue("pUsuario", param.getUsuario());
					execute = agruparOrden.execute(sqlParameter);					
					lstOrden = (List<OrdenesPagoDTO>) execute.get("pCursor");
					BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
					String pDesc_Resp = (String) execute.get("pDesc_Resp");
					LOG.info("Desc_Resp"+pDesc_Resp);
					LOG.info("idCodResp"+idCodResp);
				}else{
					LOG.error("param Regresa Vacio");
				}
			} catch (Exception e) {
				LOG.error("Excepcion general en agruparOrden", e);
			}
		return lstOrden;
		
	}
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String generarOrdenPagos(String liquidacionesId){
		LOG.info(">>generarOrdenPagos()");		
		List<PolizasProveedor> listaPolizas;
		String polizas= null;
		try{
			LOG.info("liquidacionesId=>"+liquidacionesId);
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter.addValue("pLiquidacionesIds", liquidacionesId);
			Map<String, Object> execute = this.generarOrdenesPago.execute(sqlParameter);			
			BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
			String pDesc_Resp = (String) execute.get("pDesc_Resp");
			listaPolizas = (List<PolizasProveedor>) execute.get("p_cursorPolizasProvee");
			StringBuilder builder = new StringBuilder();
			int contador = 0;
			if(listaPolizas.size() > 0){
				for(PolizasProveedor poliza: listaPolizas){
					builder.append(poliza.getIdPoliza());
					contador ++;
				}
				builder.append(" : " + String.valueOf(contador));
				polizas = builder.toString();
				
			}
			
			LOG.info("Desc_Resp+gOP>>"+pDesc_Resp);
			LOG.info("idCodResp+gOP>>"+idCodResp);
		}catch (Exception e) {
			LOG.error("Excepcion general en generarOrdenPagos", e);
		}
		
		return polizas;
	}
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<DatosDetallePagosPorRecibo> buscarOrdenesporLiquidacion(Long liquidacionId){
		LOG.info(">>buscarOrdenesporLiquidacion()");
		List<DatosDetallePagosPorRecibo> lstCheque;
		try{
			LOG.info("liquidacionId=>"+liquidacionId);
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter.addValue("pIdLiquidacion", liquidacionId);
			Map<String, Object> execute = this.buscarOrdenesporLiquidacion.execute(sqlParameter);			
			BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
			String pDesc_Resp = (String) execute.get("pDesc_Resp");
			lstCheque = (List<DatosDetallePagosPorRecibo>) execute.get("pCursor");
			LOG.info("Desc_Resp>>"+pDesc_Resp);
			LOG.info("idCodResp>>"+idCodResp);			
		}catch (Exception e) {
			LOG.error("buscarOrdenesporLiquidacion()", e);
			return new ArrayList<DatosDetallePagosPorRecibo>() ;
		}
		return lstCheque;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<SolicitudChequeSiniestro> contabilizarCheques(String liquidacionesId){
		LOG.info(">>contabilizarCheques()");
		List<SolicitudChequeSiniestro> lstCheque = null;
		try{
			if (liquidacionesId != null) {				
				LOG.info("liquidacionesId=>"+liquidacionesId);
				MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
				sqlParameter.addValue("pLiquidacionesIds", liquidacionesId);
				Map<String, Object> execute = this.contabilizarCheques.execute(sqlParameter);				
				lstCheque = (List<SolicitudChequeSiniestro>) execute.get("pCursor");
				BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
				String pDesc_Resp = (String) execute.get("pDesc_Resp");
				LOG.info(">>Desc_Resp+Cch>>"+pDesc_Resp);
				LOG.info(">>IdCodResp+Cch>>"+idCodResp);		
				////ENVIAR LIQUIDACION MIZAR///
				if (idCodResp == BigDecimal.ZERO){
					LOG.info("Contabilizar Cheque exitoso!!!");	
					for(SolicitudChequeSiniestro objCehque : lstCheque){					
						migrarChequeMizarCompensaciones(objCehque.getId());						
					} 					
				}else{
					throw new Exception("Ocurrio un error al Contabilizar Cheque+> "+pDesc_Resp+"-"+idCodResp);
				}
			}else{
				LOG.error("liquidacionesId Regresa Vacio");
			}
		}catch (Exception e) {
			throw new ApplicationException("Ocurrio un error al Contabilizar Cheque "+e.getMessage());			
		}
		return lstCheque;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void excluirOrdenPago(Long idLiquidacion, Long idRecibo){
		LOG.info(">>excluirOrdenPago()");		
		try{
			LOG.info("idLiquidacion=>"+idLiquidacion);
			LOG.info("idRecibo=>"+idRecibo);
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter.addValue("pIdLiquidacion", idLiquidacion);
			sqlParameter.addValue("pIdRecibo", idRecibo);
			Map<String, Object> execute = this.excluirOrdenPago.execute(sqlParameter);
			BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
			String pDesc_Resp = (String) execute.get("pDesc_Resp");
			LOG.info("Desc_Resp>> "+pDesc_Resp);
			LOG.info("idCodResp>> "+idCodResp);
		}catch (Exception e) {
			LOG.error("Excepcion general en excluirOrdenPago", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<OrdenesPagoDTO> obtenerOrdenPagoProceso(String  usuario) {
		LOG.info(">>obtenerOrdenPagoProceso()");
		List<OrdenesPagoDTO> lstobtenerOrden = null;
		Map<String, Object> execute = null;
			try {
				if (usuario != null) {
				    LOG.info("pUsuario=>"+usuario);
					MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
					sqlParameter.addValue("pUsuario", usuario);
					execute = obtenerOrdenPagoProceso.execute(sqlParameter);					
					lstobtenerOrden = (List<OrdenesPagoDTO>) execute.get("pCursor");					
					BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
					String pDesc_Resp = (String) execute.get("pDesc_Resp");
					LOG.info("Desc_Resp"+pDesc_Resp);
					LOG.info("idCodResp"+idCodResp);					
				}else{
					LOG.error("param Regresa Vacio");
				}
			} catch (Exception e) {
				LOG.error("Excepcion general en obtenerOrdenPagoProceso", e);
			}
		return lstobtenerOrden;
	}
	
	@SuppressWarnings("unchecked")
	public List<OrdenesPagoDTO> obtenerOrdenPagoPendXPag(String  usuario) {
		LOG.info(">>obtenerOrdenPagoPendXPag()");
		List<OrdenesPagoDTO> lstobtenerOrden = null;
		Map<String, Object> execute = null;
			try {
				if (usuario != null) {
				    LOG.info("pUsuario=>"+usuario);
					MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
					sqlParameter.addValue("pUsuario", usuario);
					execute = obtenerOrdenPagoPendXPag.execute(sqlParameter);					
					lstobtenerOrden = (List<OrdenesPagoDTO>) execute.get("pCursor");					
					BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
					String pDesc_Resp = (String) execute.get("pDesc_Resp");
					LOG.info("Desc_Resp"+pDesc_Resp);
					LOG.info("idCodResp"+idCodResp);					
				}else{
					LOG.error("param Regresa Vacio");
				}
			} catch (Exception e) {
				LOG.error("Excepcion general en obtenerOrdenPagoPendXPag", e);
			}
		return lstobtenerOrden;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void enviarLiquidacionFactura(Long idLiquidacion){
		LOG.info(">>enviarLiquidacionFacura()");		
		try{
			LOG.info("idLiquidacion=>"+idLiquidacion);
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter.addValue("pIdLiquidacion", idLiquidacion);
			Map<String, Object> execute = this.enviarLiquidacionFacura.execute(sqlParameter);
			BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
			String pDesc_Resp = (String) execute.get("pDesc_Resp");
			LOG.info("Desc_Resp"+pDesc_Resp);
			LOG.info("idCodResp"+idCodResp);
		}catch (Exception e) {
			LOG.error("Excepcion general en enviarLiquidacionFacura", e);
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void liberarLiquidacionFactura(Long idLiquidacion, String nombreArchivo){
		LOG.info(">>liberarLiquidacionFacura()");		
		try{
			LOG.info("idLiquidacion=>"+idLiquidacion);
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter.addValue("pIdLiquidacion", idLiquidacion);
			sqlParameter.addValue("pEstatus",null);
			sqlParameter.addValue("pNombreArchivo", nombreArchivo);
			Map<String, Object> execute = this.liberarLiquidacionFacura.execute(sqlParameter);
			BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
			String pDesc_Resp = (String) execute.get("pDesc_Resp");
			LOG.info("Desc_Resp"+pDesc_Resp);
			LOG.info("idCodResp"+idCodResp);
		}catch (Exception e) {
			LOG.error("Excepcion general en liberarLiquidacionFacura", e);
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void eliminarLiquidacion(Long idLiquidacion){
		LOG.info(">>eliminarLiquidacion()");		
		try{
			LOG.info("idLiquidacion=>"+idLiquidacion);
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter.addValue("pIdLiquidacion", idLiquidacion);
			Map<String, Object> execute = this.eliminarLiquidacion.execute(sqlParameter);
			BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
			String pDesc_Resp = (String) execute.get("pDesc_Resp");
			LOG.info("Desc_Resp"+pDesc_Resp);
			LOG.info("idCodResp"+idCodResp);
		}catch (Exception e) {
			LOG.error("Excepcion general en eliminarLiquidacion", e);
		}
	}
	
	@Override
	public void migrarChequeMizarCompensaciones(Long idLiquidacion)throws Exception {
		LOG.info(">>entrando migrarChequeMizarCompensaciones");
		try{			
			//SolicitudChequeSiniestro solicitudCheque=this.entidadService.findById(SolicitudChequeSiniestro.class, idLiquidacion);
			solicitudChequeService.migrarChequeMizar(idLiquidacion);		
		}catch (Exception e) {
			LOG.error("Excepcion general en migrarChequeMizarCompensaciones", e.getMessage());
			throw new Exception("Ocurrio un error al migrarChequeMizarCompensaciones "+e.getMessage());
		}				
	}
	
	@SuppressWarnings("unchecked")
	public  List<LiquidacionCompensaciones> obtenerOrdenPagosGenerado(){  
		LOG.info(" >>>obtenerOrdenPagosGenerado() ");
	    List<LiquidacionCompensaciones> lista = new ArrayList<LiquidacionCompensaciones>();
	    try {
	      final StringBuilder queryString = new StringBuilder(200);
	      queryString.append(" SELECT CL.ID, CL.ESTATUS, CL.CVE_TIPO_ENTIDAD,CL.NOMBRE_BENEFICIARIO, ");
		  queryString.append(" CL.IMPORTE_PRIMA, CL.IMPORTE_BS, CL.IMPORTE_DERPOL, CL.IMPORTE_CUM_META, CL.IMPORTE_UTILIDAD, ");
		  queryString.append(" CL.IVA, CL.IVA_RETENIDO, CL.SUBTOTAL, CL.IMPORTE_TOTAL ");
		  queryString.append(" FROM MIDAS.CA_LIQUIDACION CL ");
		  queryString.append(" WHERE ESTATUS= 'TRAMITE'");
		  queryString.append(" ORDER BY CL.ID DESC ");
	      Query query = entityManager.createNativeQuery(queryString.toString() ,LiquidacionCompensaciones.class);       
	      lista=query.getResultList();

	      LOG.info("size="+lista.size());
	      LOG.info("query="+query.toString());                
	    } catch (RuntimeException re) {
	    	LOG.error("--error obtenerOrdenPagosGenerado()", re);
	        throw re; 
		}
	    return lista;
	}
	
	public EnvioFacturaAgente persistEnvioFacturaCa(Long idAgente,Long idLiquidacion, File archivo, String userName) throws FileNotFoundException, PersistenceException, Exception {
		
		final int tipoRespuestaErrores = 500;
		List<ParametroGeneralDTO> parametros = new ArrayList<ParametroGeneralDTO>();
		List<AxnResponse> mensajes = new ArrayList<AxnResponse>();
		List<ValorCatalogoAgentes> listValor = new ArrayList<ValorCatalogoAgentes>();
		List<CaEnvioXml> mensajesEnvio = new ArrayListNullAware<CaEnvioXml>();
		EnvioFacturaAgente envio = null;
		GrupoCatalogoAgente gpo = new GrupoCatalogoAgente();
		CaEnvioXml mensajeEnvioCa = null;
		FileInputStream fis = null;
		ValResponse response = null;
		Agente agente = new Agente();
		LiquidacionCompensaciones liquidacion = new LiquidacionCompensaciones();
	    StringBuilder  stringBuilder = new StringBuilder();
	    ValorCatalogoAgentes statusAceptado = new ValorCatalogoAgentes();
	    ValorCatalogoAgentes statusRechazado = new ValorCatalogoAgentes();
	    
	    BigDecimal subtotal = BigDecimal.ZERO;
	    String facturaString = new String();
	    String line = null;	   
	    String tolerancia = null;
		String empresa = null;
		String departamento = null;
		String uuid = null;
		boolean fisicamenteEnRepositorio = false;
		int numeroErrores = 0;		
		boolean excepcionImportes = false;
		boolean pruebas = true;

		try {
			liquidacion = this.findById(idLiquidacion);
			if(liquidacion.getEstatus().equals(TRAMITE_FACTURA.toString()) && liquidacion.getIdentificadorBenId() != null){
				excepcionImportes = true;				
			}
		} catch(Exception e) {
			LOG.error("Error al preparar datos de Envio Factura Agente");
			LOG.error(e.getMessage(), e);
			throw new PersistenceException();
		}
		
		try {
			//se obtienen los parametros, se colocaron en base de datos para que sean mas faciles de cambiar
			parametros =  parametroGeneral.findByProperty("grupoParametroGeneral.descripcionGrupoParametroGral", EnvioFacturaAgenteServiceImpl.PARAMETROS_ENVIO_FACTURA_AGENTE);
			
			//se inicializan los parametros
			for (ParametroGeneralDTO parametro : parametros) {
				if("Tolerancia".equals(parametro.getDescripcionParametroGeneral())){
					tolerancia = parametro.getValor();
				}else if("Empresa".equals(parametro.getDescripcionParametroGeneral())){
					empresa = parametro.getValor();
				}else if("Departamento".equals(parametro.getDescripcionParametroGeneral())){
					departamento = parametro.getValor();
				}
		}
			
			gpo =envioFacturaDao.findByProperty(GrupoCatalogoAgente.class, "descripcion", EnvioFacturaAgenteServiceImpl.ESTATUS_ENVIO_FACTURA_AGENTE).get(0);
			listValor = gpo.getValorCatalogoAgentes();
			for (ValorCatalogoAgentes val: listValor) {
				
				if (val.getClave().trim().equals(EnvioFacturaAgenteServiceImpl.CLAVE_STATUS_ACEPTADO)) {
					
					statusAceptado = val;
					
				} else if (val.getClave().trim().equals(EnvioFacturaAgenteServiceImpl.CLAVE_STATUS_RECHAZADO)) {
					
					statusRechazado = val;
					
				}
				
			}
		} catch(Exception e) {
			LOG.error("Error al cargar parametros para Validacion de XML");
			LOG.error(e.getMessage(), e);
			throw e;
		}
		
		if(liquidacion.getCveTipoEntidad().equals("AGENTE")|| liquidacion.getCveTipoEntidad().equals("PROMOTOR")){ //Si es agente
		
			try{			
				agente.setIdAgente(idAgente);
				agente = agenteMidasService.loadByClave(agente);			
			} catch(Exception e) {
				LOG.error("Error al cargar datos de Agente para Validacion de XML");
				LOG.error(e.getMessage(), e);
				throw e;
			}
			
			try {
				fis = new FileInputStream(archivo);
			    InputStreamReader inputStreamReader = new InputStreamReader((InputStream)fis);		    
			    BufferedReader br = new BufferedReader(inputStreamReader);
			    line = null;
			    while ((line = br.readLine()) != null) {
			    	stringBuilder.append(line);
			    }   
			    facturaString = stringBuilder.toString();
			    //se cambia el tipo de encoding para corregir error del sello digital.
				facturaString = new String(facturaString.getBytes(), "UTF-8");
			    uuid = getUUIDFromXML(facturaString);
			} catch(FileNotFoundException e) {
				LOG.error("Error el Archivo XML esta vacio");
				LOG.error(e.getMessage(), e);
				throw e;
			} catch (Exception e) {
				LOG.error("Error al convertir Archivo a String");
				LOG.error(e.getMessage(), e);
				throw e;
			} finally {
				fis.close();
			}
			try {
				WSValidacionesSATPortProxy	proxySAT =  new WSValidacionesSATPortProxy();
				
				envio = new EnvioFacturaAgente();
				envio.setStatus(statusRechazado);
				
				try{
					Object  obj = proxySAT.verificaComprobanteFiscalDigital(facturaString, "", 
																	(agente.getPersona().getRfc()==null)?"":agente.getPersona().getRfc().replace(" ", ""),
																	(subtotal==BigDecimal.ZERO || excepcionImportes)?"":subtotal.toString(), 
																	"", 
																	(liquidacion.getIsr()== Double.valueOf(0) || excepcionImportes)?"":liquidacion.getIsr().toString(), 
																	(liquidacion.getIvaRetenido()== Double.valueOf(0) || excepcionImportes)?"":liquidacion.getIvaRetenido().toString(), 
																	(liquidacion.getIva()== Double.valueOf(0) || excepcionImportes)?"":liquidacion.getIva().toString(), 
																	"", 
																	(liquidacion.getImporteTotal()== Double.valueOf(0) || excepcionImportes)?"":liquidacion.getImporteTotal().toString(),  
																	tolerancia, 
																	empresa, 
																	departamento, 
																	uuid,
																	"");
				 
				 try{
						/**
						 * Almacena los datos de respuesta de axosnet en los envios generales para tener un concentrado de todos los envios al axosnet
						 * despues hay que dejar de guardar esta informacion en toenvioxmlagente
						 */
						this.envioFacturaService.saveEnvio(obj,  "COMP", idLiquidacion, "", true);
					} catch(Exception e) {
						LOG.error("Error al guardar el envio en tabla generica agente: "+idAgente);
						LOG.error(e.getMessage(), e);
					}
					
					response = (ValResponse)obj;
					mensajes = response.getMessages();
					
				 
					}catch(Exception e) {
						LOG.error("Error de datos proxySAT.verificaComprobanteFiscalDigital()");
						LOG.error(e.getMessage(), e);
						throw new ConnectException();
					}
				
			} catch(Exception e) {
				LOG.error("Error de comunicacion con WS ValidacionesSAT");
				LOG.error(e.getMessage(), e);
					}
					
			try {
				envio  = new EnvioFacturaAgente();	
				envio.setIdAgente(idAgente);
				envio.setNombreArchivo(uuid + ".xml");
				envio.setUuid(uuid);
				
				if(mensajes.size()>0){
					String mensajeAux="\n";
					for(AxnResponse m:mensajes){
						if(m.getCode().equals("ER"))
							mensajeAux+=m.getMessage()+"\n";
					}
					envio.setMessagesFactura(mensajeAux);
				}
				
				//Tambien se considera envio aceptado en el caso de que el WS mande un rechazo porque ya existia la factura en el repositorio
				//pero aun no se encuentra aceptado en el Inventario de Facturas
			    if(!pruebas){
					if (response.isValid() || (fisicamenteEnRepositorio)) {   
						this.liberarLiquidacionFactura(idLiquidacion,envio.getNombreArchivo());
						this.update(liquidacion);
						envio.setStatus(statusAceptado);
					} else{
						envio.setStatus(statusRechazado);
					}
				}else{
					this.liberarLiquidacionFactura(idLiquidacion,envio.getNombreArchivo());//PARA PRUEBAS SE ACTUALIZA EL ESTATUS
				}
			    
				envio.setUsuario(userName);
				envio = envioFacturaDao.guardaEnvio(envio);
				
				/**
				 * Aqui se relaciona los datos del xml con los cheques para la e-contabilidad siempre y cuando el xml este correcto
				 */
				/*if (response.isValid() || (numeroErrores == 1 && fisicamenteEnRepositorio && !existeEnInventarioFacturas(uuid))) {
					this.envioFacturaDao.saveRelationXMLCheque(idAgente, anioMes, envioFactura.getIdEnvio());	
				}*/
				
			} catch(Exception e) {
				LOG.error("Error al Actualizar la Liquidacion");
				LOG.error(e.getMessage(), e);
				throw e;
			}
		}else if(liquidacion.getCveTipoEntidad().equals("PROVEEDOR")){//Si es proveedor
			envio  = new EnvioFacturaAgente();	
			EnvioValidacionFactura  envioValidacionFactura = this.procesarFacturaProveedores(Integer.valueOf(idAgente.toString()), archivo, liquidacion);
			 uuid = getUUIDFromXML(envioValidacionFactura.getFactura().getXmlFactura());
			 envio.setNombreArchivo(uuid+".xml");
			
			if(envioValidacionFactura.getFactura().getEstatus()!=DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue()){
				//this.entidadService.save(factura);
			    envio.setStatus(statusAceptado);
			    this.liberarLiquidacionFactura(idLiquidacion,envio.getNombreArchivo());
			}else{
				envio.setStatus(statusRechazado);
				if(envioValidacionFactura.getMensajes().size()>0){
					String mensajeAux="\n";
					for(MensajeValidacionFactura list: envioValidacionFactura.getMensajes()){
							mensajeAux+=list.getMensaje().toString()+"\n";
					}
					envio.setMessagesFactura(mensajeAux);
				}
			}
			if(pruebas){
				this.liberarLiquidacionFactura(idLiquidacion,envio.getNombreArchivo());//para pruebas cambia el estatus
			}
		}
			return envio;
	}
	
	private String getUUIDFromXML(String xmlString) {
		final int prefijo = 6;
		//Ej. UUID="259F974F-94A4-4FA0-8E69-2A9CEAC9FB00" 
		String chunck = xmlString.substring(xmlString.toUpperCase().indexOf("UUID") + prefijo);
		return chunck.substring(0, chunck.indexOf("\""));
		
		
	}
	
	///////Sirve para validar Factura de Proveedores de compensaciones///no completado 
	public DocumentoFiscal procesaXML(ByteArrayOutputStream bos,Integer idProveedor){
		DocumentoFiscal factura = new DocumentoFiscal();
		String facturaXML;
		try {
			facturaXML = bos.toString("UTF-8");
			Comprobante comprobante = recepcionFacturaService.parsearCFD(bos);
			DocumentoFiscal facturaFiscal = this.convierteComprobanteAFactura(comprobante,facturaXML);
			
				if(recepcionFacturaService.esValidoRFCPorProveedor(facturaFiscal.getRfcEmisor(), idProveedor)){
					DocumentoFiscal facturaExistente = this.recepcionFacturaDao.validaSiExiste(idProveedor,facturaFiscal);
					if(facturaExistente!= null){
						if(!facturaExistente.getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue()) && !facturaExistente.getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.PAGADA.getValue())){
							this.actualizaInformacionDeLaFacturaExistente(facturaFiscal, facturaExistente);
							factura = facturaFiscal;
						}else{
							factura = facturaExistente;
						}
					}else{
						PrestadorServicio proveedor = entidadService.findById(PrestadorServicio.class, Integer.valueOf(idProveedor.toString()));
						facturaFiscal.setProveedor(proveedor);
						factura = facturaFiscal;
					}
				}else{
					LOG.error("El RFC NO COINCIDE CON EL PROVEEDOR");
					factura = facturaFiscal;
					factura.setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
				}
			
		} catch (UnsupportedEncodingException e) {
			LOG.error(e.getMessage(), e);
			factura = generarFacturaFicticia();
		} catch (XmlException e) {
			LOG.error(e.getMessage(), e);
			factura = generarFacturaFicticia();
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			factura = generarFacturaFicticia();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			factura = generarFacturaFicticia();
		}
		return factura;
	}
	
public EnvioValidacionFactura procesarFacturaProveedores(Integer proveedorId, File XmlFile,LiquidacionCompensaciones liquidacion) {
		
		//List<DocumentoFiscal> facturas = new ArrayList<DocumentoFiscal>();	
		FileInputStream fis = null;
		DocumentoFiscal factura=null;
		try {
			fis = new FileInputStream(XmlFile);
			byte[] bytes = new byte[(int) XmlFile.length()];
			fis.read(bytes);
			ByteArrayOutputStream bos = new ByteArrayOutputStream( );
			bos.write(bytes);
			factura = this.procesaXML(bos,proveedorId);
			this.entidadService.save(factura);
			//facturas.add(factura);
		
		} catch (FileNotFoundException e) {
			LOG.error(e.getMessage(),e);
		} catch (IOException e) {
			LOG.error(e.getMessage(),e);
		} finally {
			IOUtils.closeQuietly(fis);
		}
		
		EnvioValidacionFactura envioValidacion= creaEnvioValidacionComp(factura);
		factura.setLiquidacionCompensaciones(liquidacion);
		List<EnvioValidacionFactura> listaValidacionFactura = new ArrayList<EnvioValidacionFactura>();
		listaValidacionFactura.add(envioValidacion);
			
		recepcionFacturaService.validaProcesamientoFactura(listaValidacionFactura,proveedorId);
		listaValidacionFactura = (List<EnvioValidacionFactura>) this.entidadService.saveAll(listaValidacionFactura);
		
		
		return recepcionFacturaService.validaAsociarLaFacturaLiquidacion(factura,listaValidacionFactura.get(0));

	}

	public EnvioValidacionFactura creaEnvioValidacionComp(DocumentoFiscal factura){
		final String ORIGEN = "COMP";
		String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		//List<EnvioValidacionFactura> envioValidacionList = new ArrayList<EnvioValidacionFactura>();
		Long batchId = this.recepcionFacturaDao.obtenerBatchId();
		//for(DocumentoFiscal factura : facturas){
			EnvioValidacionFactura envioValidacionFactura = new EnvioValidacionFactura();
			envioValidacionFactura.setFactura(factura);
			envioValidacionFactura.setIdBatch(batchId);
			envioValidacionFactura.setEstatus(RecepcionFacturaServiceImpl.ENVIO_VALIDACION_PENDIENTE);
			envioValidacionFactura.setOrigen(ORIGEN);
			envioValidacionFactura.setFechaEnvio(new Date());
			envioValidacionFactura.setCodigoUsuarioCreacion(codigoUsuario);
			envioValidacionFactura.setCodigoUsuarioModificacion(codigoUsuario);
			//envioValidacionList.add(envioValidacionFactura);
		//}
		return envioValidacionFactura;
	}
	
	
	
	@SuppressWarnings("unchecked")
	private  LiquidacionCompensaciones obtenerLiquidacionPorFiltro(String estatus, Long idAgente){  
		LOG.debug("LiquidacionCompensaciones() estatus => {} idAgente => {} " ,estatus,idAgente);
		LiquidacionCompensaciones resultLiquidacion = null;
		final StringBuilder queryString = new StringBuilder("");
	    try {
	      queryString.append(" SELECT CLIQ.* FROM MIDAS.CA_LIQUIDACION CLIQ  ");
		  queryString.append(" WHERE CLIQ.ESTATUS = ");
		  queryString.append("'"+estatus+"'");
		  queryString.append(" AND CLIQ.IDENTIFICADOR_BEN_ID = ");
		  queryString.append(idAgente);
	      Query query = entityManager.createNativeQuery(queryString.toString(), LiquidacionCompensaciones.class);
//	      query.setParameter(1, estatus);	      
//	      query.setParameter(2, idAgente);
	      List<LiquidacionCompensaciones> listaObj = query.getResultList();			
			LOG.info("size=>"+listaObj.size());
			for(LiquidacionCompensaciones object:listaObj){
				if(object!=null){
					resultLiquidacion = new LiquidacionCompensaciones();
					resultLiquidacion= object;
					break;
				}
			}
		} catch (RuntimeException re) {
			LOG.error("LiquidacionCompensaciones Exception=",re);
			resultLiquidacion=null;
		}
		return resultLiquidacion;
	}
	

	public  List<LiquidacionCompensaciones> obtenerLiquidacionesPorFiltro(String estatus, Long idAgente){  
		LOG.debug("LiquidacionCompensaciones() estatus => {} idAgente => {} " ,estatus,idAgente);
		final StringBuilder queryString = new StringBuilder();
	    try {
	    	queryString.append("select model from LiquidacionCompensaciones model where (model.estatus='"+estatus+"'");
	    	queryString.append(" and model.estatus<>'PROCESO')");
	    	queryString.append(" and ((model.identificadorBenId = "+idAgente+" and model.cveTipoEntidad IN ('AGENTE','PROVEEDOR'))");
	    	queryString.append(" or (model.identificadorBenId IN (SELECT a.promotoria.id FROM Agente a WHERE a.idAgente ="+idAgente+") ");
	    	queryString.append(" and model.identificadorBenId IN (SELECT p.id FROM Promotoria p WHERE p.idAgentePromotor ="+idAgente+") ");
	    	queryString.append(" and model.cveTipoEntidad='PROMOTOR'");
	    	queryString.append(" ))");
			Query query = entityManager.createQuery(queryString.toString());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LOG.error("LiquidacionCompensaciones Exception=",re);
			return null;
		}
	}
	public  List<LiquidacionCompensaciones> obtenerLiquidacionesPorFiltro(Long idAgente){  
		LOG.debug("LiquidacionCompensaciones() idAgente => {} " ,idAgente);
		final StringBuilder queryString = new StringBuilder();
	    try {
	    	queryString.append("select model from LiquidacionCompensaciones model where model.estatus<>'PROCESO'");
	    	queryString.append(" and ((model.identificadorBenId = "+idAgente+" and model.cveTipoEntidad IN ('AGENTE','PROVEEDOR'))");
	    	queryString.append(" or (model.identificadorBenId IN (SELECT a.promotoria.id FROM Agente a WHERE a.idAgente ="+idAgente+") ");
	    	queryString.append(" and model.identificadorBenId IN (SELECT p.id FROM Promotoria p WHERE p.idAgentePromotor ="+idAgente+") ");
	    	queryString.append(" and model.cveTipoEntidad='PROMOTOR'");
	    	queryString.append(" ))");
			Query query = entityManager.createQuery(queryString.toString());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LOG.error("LiquidacionCompensaciones Exception=",re);
			return null;
		}
	}
	
	public String getValidaDatosEnvioXml (){
		return getValor(VALORFACTURA);
	}
	
	private String getValor(BigDecimal codigoParametro){
		 final ParametroGeneralDTO parametro = parametroGeneralFacade.findById(getParametroGeneralId(codigoParametro));
		 return parametro.getValor();
	}
	
	private  ParametroGeneralId getParametroGeneralId(BigDecimal codigoParametro){
		 ParametroGeneralId id = new ParametroGeneralId();
		 id.setIdToGrupoParametroGeneral(new BigDecimal(88));
	     id.setCodigoParametroGeneral(codigoParametro);
	     return id;
	}
	@SuppressWarnings("unchecked")
	public TransporteImpresionDTO obtenerReciboHonorarios(ReporteFiltrosDTO parametros){
		DatosHonorariosAgentes datosHonorariosAgentes = new DatosHonorariosAgentes();
		TransporteImpresionDTO transporteImpresionDTO = null;
		try{
				MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
				sqlParameter.addValue("pAnioMes", parametros.getAnioMes());
				sqlParameter.addValue("pCveAgente", parametros.getCveAgente());
				sqlParameter.addValue("pCvePromotor", parametros.getCvePromotor());
				sqlParameter.addValue("pCveProveedor", parametros.getCveProveedor());
    			Map<String, Object> execute = this.reciboHonorarios.execute(sqlParameter);	
    			String nomBeneficiario = (String) execute.get("pNomBeneficiario");
    			String tipoBeneficiario = (String) execute.get("pTipoBeneficiario");
    			BigDecimal idBeneficiario =   (BigDecimal) execute.get("pIdBeneficiario");
    			BigDecimal folio =   (BigDecimal)execute.get("pFolio");
    			BigDecimal impTotalLetra = (BigDecimal) execute.get("pImpTotalLetra");
    			BigDecimal impGravado =   (BigDecimal)execute.get("pImpGravado");
    			BigDecimal impExento = (BigDecimal)execute.get("pImpExento");
    			BigDecimal subTotal = (BigDecimal)execute.get("pSubTotal");
	    		BigDecimal Iva = (BigDecimal)execute.get("pIva");
	    		BigDecimal ISR = (BigDecimal)execute.get("pISR");
	    		BigDecimal ivaRetenido = (BigDecimal)execute.get("pIvaRetenido");
	    		BigDecimal impTotal = (BigDecimal)execute.get("pImpTotal");
    			
	    		datosHonorariosAgentes.setNombre(nomBeneficiario);
    			datosHonorariosAgentes.setNombreGerencia(tipoBeneficiario);
	    		datosHonorariosAgentes.setIdAgente(idBeneficiario.longValue());    
    			datosHonorariosAgentes.setOrigen(folio.toString());
    			datosHonorariosAgentes.setImporteTotalCLetra(Utilerias.convertNumberToLetter(impTotalLetra.doubleValue()));    	
    			datosHonorariosAgentes.setImporteGravable(impGravado);
    			datosHonorariosAgentes.setImporteExento(impExento);
	    		datosHonorariosAgentes.setImporteIva(Iva);
	    		datosHonorariosAgentes.setImporteIsrRetenido(ISR);
	    		datosHonorariosAgentes.setImporteIvaRetenido(ivaRetenido);
	    		datosHonorariosAgentes.setImporteTotal(impTotal); 

	    		byte[] d =  generarPlantillaReporte.imprimirReciboHonorariosComp(datosHonorariosAgentes);
	    	    transporteImpresionDTO = new  TransporteImpresionDTO();
	    		transporteImpresionDTO.setByteArray(d);
				
		}catch (Exception e) {
			LOG.error("-- reporte de Honorarios ERROR ", e);
		}

		return transporteImpresionDTO;
	  }	
	
	@SuppressWarnings("unchecked")
	public OrdenesPagoDTO  getReporteAutorizarOrdenPagos(String idLiquidacion){
		OrdenesPagoDTO ordenesPagoDTO = new OrdenesPagoDTO();
		ArrayList<ReporteAutorizarOrdenPago> reporteAutorizarOrdenPagos = null;
	try{
				MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
				sqlParameter.addValue("pIdLiquidacion",idLiquidacion);
    			Map<String, Object> execute = this.reporteAutorizarOrdenPago.execute(sqlParameter);	
    			reporteAutorizarOrdenPagos = (ArrayList<ReporteAutorizarOrdenPago>) execute.get("pCursor");
	    		BigDecimal impTotal = (BigDecimal)execute.get("pImpTotalComp");
    			ordenesPagoDTO.setReporteAutorizarOrdenPagos(reporteAutorizarOrdenPagos);
	    		ordenesPagoDTO.setSumatoria(impTotal.doubleValue()); 
	
		}catch (Exception e) {
			LOG.error("-- reporte de AutorizacionOP ERROR ", e);
		}
		return ordenesPagoDTO;
	  }
	
	@SuppressWarnings("unchecked")
	public Map<String,Object>  getReporteDetallePrimas(ReporteFiltrosDTO reporteFiltro){	
		Map<String,Object> params=null;
		try{
					MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
					sqlParameter.addValue("pAnioMes",reporteFiltro.getAnioMes());
					sqlParameter.addValue("pCveAgente",reporteFiltro.getCveAgente());
					sqlParameter.addValue("pCvePromotor",reporteFiltro.getCvePromotor());
					sqlParameter.addValue("pCveProveedor",reporteFiltro.getCveProveedor());
					params= this.reporteDetallePrima.execute(sqlParameter);	
			}catch (Exception e) {
				LOG.error("-- reporte de Detalle Primas ERROR ", e);
			}
		return params;	
	 }
	
	private DocumentoFiscal generarFacturaFicticia(){
		DocumentoFiscal facturaFicticia = new DocumentoFiscal();
		String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		
		facturaFicticia.setNumeroFactura(NUMERO_FACTURA_FICTICIA);
		facturaFicticia.setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
		facturaFicticia.setCodigoUsuarioCreacion(codigoUsuario);
		facturaFicticia.setCodigoUsuarioModificacion(codigoUsuario);
		
		return facturaFicticia;
	}
	
	private void actualizaInformacionDeLaFacturaExistente(DocumentoFiscal facturaNueva, DocumentoFiscal facturaExistente){
		facturaNueva.setId(facturaExistente.getId());
		facturaNueva.setFechaCreacion(facturaExistente.getFechaCreacion());
		facturaNueva.setCodigoUsuarioCreacion(facturaExistente.getCodigoUsuarioCreacion());
		facturaNueva.setFechaDevolucion(facturaExistente.getFechaModificacion());
	}
	@EJB
	private CatalogoValorFijoFacadeRemote catalogoValorFijoFacadeRemote;
	@EJB
	private CatalogoCompensacionesDao catalogoCompensacionesDao;
	
	public OrdenesPagoFiltroDTO obtenerFiltrosOrdenPagos(Long tipo){
		TIPO_PROCESO proceso=TIPO_PROCESO.parse(tipo);
		List<CatalogoValorFijoComboDTO> estatusFacuraList= new ArrayList<CatalogoValorFijoComboDTO>();
		List<CatalogoValorFijoComboDTO> estatusOrdenpagoList= new ArrayList<CatalogoValorFijoComboDTO>();
		List<CaRamo>  ramosList= new ArrayList<CaRamo>();
		switch (proceso) {
		case CONSULTA :
			List<CatalogoValorFijoDTO> gruposEstatusFactura = catalogoValorFijoFacadeRemote.findByProperty("id.idGrupoValores", 506);
			List<CatalogoValorFijoDTO> gruposEstatusOrdenpago = catalogoValorFijoFacadeRemote.findByProperty("id.idGrupoValores", 505);
			ramosList=  catalogoCompensacionesDao.listarRamos();
			for(CatalogoValorFijoDTO grupo : gruposEstatusFactura){
				estatusFacuraList.add(new CatalogoValorFijoComboDTO(grupo));
			}
			for(CatalogoValorFijoDTO grupo : gruposEstatusOrdenpago){
				estatusOrdenpagoList.add(new CatalogoValorFijoComboDTO(grupo));
			}
			break;

		default:
			break;
		}
		return new OrdenesPagoFiltroDTO(estatusFacuraList,estatusOrdenpagoList,ramosList);
	}
	private DocumentoFiscal convierteComprobanteAFactura(Comprobante comprobante, String xmlFactura ){
		DocumentoFiscal factura = new DocumentoFiscal();
		String numeroFactura = comprobante.getFolio();
		String nombreEmisor = comprobante.getEmisor().getNombre();
		String rfcEmisor = comprobante.getEmisor().getRfc();
		String nombreReceptor = comprobante.getReceptor().getNombre();
		String rfcReceptor = comprobante.getReceptor().getRfc();
		String monedaPago = comprobante.getMoneda();
		String tipoDocumentoFiscal = recepcionFacturaService.obtenerTipoDeDocumentoFiscal(comprobante);
		BigDecimal montoTotal = comprobante.getTotal();
		BigDecimal subTotal = comprobante.getSubTotal();
		Impuestos impuestos = comprobante.getImpuestos();
		BigDecimal iva = recepcionFacturaService.obtenerImpuestoTraslados(impuestos, Traslado.Impuesto.IVA);
		BigDecimal ivaRetenido = recepcionFacturaService.obtenerImpuestoRetenciones(impuestos, Retencion.Impuesto.IVA);
		BigDecimal isr = recepcionFacturaService.obtenerImpuestoRetenciones(impuestos, Retencion.Impuesto.ISR);
		Date fechaFactura = comprobante.getFecha().getTime();
		
		String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		
		factura.setXmlFactura(xmlFactura);
		factura.setNumeroFactura(numeroFactura);
		factura.setEstatus(RecepcionFacturaServiceImpl.FACTURA_PROCESADA);
		factura.setNombreEmisor(nombreEmisor);
		factura.setRfcEmisor(rfcEmisor);
		factura.setNombreReceptor(nombreReceptor);
		factura.setRfcReceptor(rfcReceptor);
		factura.setMonedaPago(monedaPago);
		factura.setTipo(tipoDocumentoFiscal);
		factura.setIva(iva);
		factura.setIvaRetenido(ivaRetenido);
		factura.setIsr(isr);
		factura.setSubTotal(subTotal);
		factura.setMontoTotal(montoTotal);
		factura.setCodigoUsuarioCreacion(codigoUsuario);
		factura.setCodigoUsuarioModificacion(codigoUsuario);
		factura.setOrigen(OrigenDocumentoFiscal.LSN);
		factura.setFechaFactura(fechaFactura);
		return factura;
	}
	
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	public ParametroGeneralFacadeRemote getParametroGeneral() {
		return parametroGeneral;
	}
	
	@EJB
	public void setParametroGeneral(ParametroGeneralFacadeRemote parametroGeneral) {
		this.parametroGeneral = parametroGeneral;
	}

	public EnvioFacturaAgenteDao getEnvioFacturaDao() {
		return envioFacturaDao;
	}
	@EJB
	public void setEnvioFacturaDao(EnvioFacturaAgenteDao envioFacturaDao) {
		this.envioFacturaDao = envioFacturaDao;
	}
	
	
	
}