/**
 * Clase que llena las opciones de Menu para el rol de Cabinero
 */
package mx.com.afirme.midas2.service.impl.seguridad.filler.opreaseguroautomatico;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

/**
 * @author andres.avalos
 *
 */
public class MenuOpReaseguroAutomatico {

	private List<Menu> listaMenu = null;
		
	public MenuOpReaseguroAutomatico() {
		listaMenu = new ArrayList<Menu>();
	}
	
public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"menu","", "Raiz del menu", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m3","Reaseguro", "Menu ppal Reaseguro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m3_1","Reaseguro", "Catalogos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("4"),"m3_1_1","Reaseguro", "Reasegurador-Corredor", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m3_1_2","Reaseguro", "Contacto", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("6"),"m3_1_3","Reaseguro", "Cuenta Banco", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("7"),"m3_2","Reaseguro", "Contratos Proporcionales", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("8"),"m3_2_1","Reaseguro", "Administraci�n de L�neas y Contratos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("9"),"m3_2_1_1","Reaseguro", "Negociaci�n", "/MidasWeb/contratos/linea/listarLineaNegociacion.do|contenido|cargarComponentesLineaNegociacion()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("10"),"m3_2_1_2","Reaseguro", "Contrato", "/MidasWeb/contratos/linea/listarLineaContrato.do|contenido|cargarComponentesLineaContrato()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("11"),"m3_2_1_3","Reaseguro", "Vigencia", "/MidasWeb/contratos/linea/listarLineaVigencia.do|contenido|cargarComponentesLineaVigencia()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("36"),"m3_6","Reaseguro", "Estados de Cuenta", "/MidasWeb/reaseguro/estadoscuenta/mostrarPrincipalEstadosCuenta.do|contenido|dhx_init_tabbars()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("37"),"m5_3","Reaseguro", "Reportes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("38"),"m5_3_2_2","Reaseguro", "Movimientos por Reasegurador", "/MidasWeb/reaseguro/reportes/buscarPoliza.do|contenido|cargarComponentesRptMovtosPorReasegurador()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("37"),"m5","Reaseguro", "Reportes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("38"),"m5_3","Reaseguro", "Reportes Reaseguro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("44"),"m5_3_2","Reaseguro", "Movimientos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("45"),"m5_3_2_1","Reaseguro", "Reporte de Movimientos por Contrato", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("46"),"m5_3_2_2","Reaseguro", "Reporte de Movimientos de Primas por Reasegurador (REAS)", "/MidasWeb/reaseguro/reportes/buscarPoliza.do|contenido|cargarComponentesRptMovtosPorReasegurador()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("47"),"m5_3_2_3","Reaseguro", "Reporte de Soporte de Contratos de una P�liza", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("48"),"m5_3_2_4","Reaseguro", "Reporte de Distribuci�n", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("49"),"m5_3_2_5","Reaseguro", "Reporte de Emisi�n", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("50"),"m5_3_3","Reaseguro", "Siniestros", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("51"),"m5_3_3_1","Reaseguro", "Reporte de Siniestros por Reasegurador (Borderaux)", "", true);
		listaMenu.add(menu);		
		
		menu = new Menu(new Integer("63"),"m7","Ayuda", "Ayuda MIDAS", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("64"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("65"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
		
		return this.listaMenu;
		
	}
	
}
