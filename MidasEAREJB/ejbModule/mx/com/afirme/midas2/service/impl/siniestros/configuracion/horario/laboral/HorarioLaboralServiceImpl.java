package mx.com.afirme.midas2.service.impl.siniestros.configuracion.horario.laboral;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.siniestros.catalogo.CatalogoSiniestroDao;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.laboral.HorarioLaboral;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.impl.siniestros.catalogo.CatalogoSiniestroServiceImpl;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.laboral.HorarioLaboralService;

import org.apache.log4j.Logger;

/**
 * 
 * @author josegarza
 * 
 */
@Stateless
public class HorarioLaboralServiceImpl extends CatalogoSiniestroServiceImpl implements HorarioLaboralService {

	public static final Logger		log	= Logger.getLogger(HorarioLaboralServiceImpl.class);

	@Resource
	private Validator				validator;

	@EJB
	private EntidadService			entidadService;

	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService			usuarioService;

	@EJB
	private CatalogoSiniestroDao	catalogoSiniestroDAO;

	@Override
	public List<HorarioLaboral> listar(HorarioLaboralFiltro entidadFltro) {
		return buscar(HorarioLaboral.class, entidadFltro);
	}

	@Override
	public void guardar(HorarioLaboral horarioLaboral) {
		validarCamposObligatoriosAltaDeHorario(horarioLaboral);
		HorarioLaboral horarioPrevio = null;
		if (horarioLaboral.getId() != null) {
			horarioPrevio = entidadService.findById(HorarioLaboral.class,
					horarioLaboral.getId());
		}
		validarHorarioPredeterminado(horarioLaboral, horarioPrevio);
		validarHorariosExistentes(horarioLaboral);
		validarHorariosPredeterminados(horarioLaboral);
		validarHorariosEdicion(horarioLaboral, horarioPrevio);
		setearValoresControl(horarioLaboral, horarioPrevio);
		this.salvar(horarioLaboral);
	}

	@Override
	public HorarioLaboral obtener(Long id) {
		HorarioLaboral horarioLaboral = entidadService.findById(
				HorarioLaboral.class, id);
		return horarioLaboral;
	}

	@Override
	public <K extends CatalogoFiltro, E extends Entidad> List<E> buscar(
			Class<E> entidad, K filtro) {
		List<E> listaEntidades = catalogoSiniestroDAO.buscar(entidad, filtro);
		return listaEntidades;
	}

	@Override
	public <E extends Entidad, K> E obtener(Class<E> entityClass, K id) {
		return entidadService.findById(entityClass, id);
	}

	@Override
	public <E extends Entidad> void salvar(E entidad) {
		this.entidadService.save(entidad);
	}

	/**
	 * Funcion que valida la RDN del CDU {272C8CDD-EAEE-4ceb-A965-F915602C3C72}
	 * / Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: Campos
	 * Obligatorios Alta de Horario
	 */
	private void validarCamposObligatoriosAltaDeHorario(
			HorarioLaboral horarioLaboral) throws ConstraintViolationException {
		Set<ConstraintViolation<HorarioLaboral>> constraintViolations = validator
				.validate(horarioLaboral);
		if (constraintViolations.size() > 0) {
			throw new ConstraintViolationException(
					new HashSet<ConstraintViolation<?>>(constraintViolations));
		}
	}

	/**
	 * Funcion que valida la RDN del CDU {14307D31-BB5D-4ae3-97F5-C83B0B02DE25}
	 * / Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: Validar
	 * Disponibilidad Configurada - RDN No debe existir más de un horario
	 * enlistado donde ambos tengan la misma hora de entrada y la misma hora de
	 * salida
	 */
	private void validarHorariosExistentes(HorarioLaboral horarioLaboral)
			throws NegocioEJBExeption {
		String msgError = "Horario ya existente con el mismo horario de Entrada y Salida";

		HorarioLaboralFiltro filtro = new HorarioLaboralFiltro();
		if (horarioLaboral.getId() == null) {
			filtro.setHoraEntradaCode(horarioLaboral.getHoraEntradaCode());
			filtro.setHoraSalidaCode(horarioLaboral.getHoraSalidaCode());
			List<HorarioLaboral> existentes = buscar(HorarioLaboral.class,
					filtro);
			if (existentes != null && existentes.size() > 0) {
				throwNegocioEJBExeption(msgError);
			}
		}
	}

	/**
	 * Funcion que valida la RDN {XXXXXXXX-XXXXX-XXXX-XXXXX-XXXXXXXXXXXX} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: XXXXX XXX XXXX
	 * - RDN Si es el default no permitir quitarle el default ni inactivar.
	 */
	private void validarHorarioPredeterminado(HorarioLaboral horarioLaboral,
			HorarioLaboral horarioPrevio) throws NegocioEJBExeption {
		String msgError = "No puede quitarle el default al horario o poner inactivo el horario por default";

		if (horarioPrevio != null && horarioPrevio.getPredeterminado()) {
			if (horarioLaboral.getPredeterminado() == false
					|| horarioLaboral.getEstatus() == HorarioLaboral.Estatus.INACTIVO
							.getObtenerEstatus()) {
				throwNegocioEJBExeption(msgError);
			}
		}
	}

	/**
	 * Funcion que valida la RDN {B4CAB926-F76D-4e2b-AD4F-2AC13C27CC60} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: Horario por
	 * Default - RDN No podrá haber dos horarios como "Default" al mismo tiempo,
	 * por lo que solo podrá existir un solo horario como default.
	 */
	private void validarHorariosPredeterminados(HorarioLaboral horarioLaboral)
			throws NegocioEJBExeption {
		Boolean predeterminado = horarioLaboral.getPredeterminado();
		Boolean activo = (horarioLaboral.getEstatus() == 1);
		if (!activo) {
			horarioLaboral.setPredeterminado(false);
		} else {
			HorarioLaboralFiltro filtro = new HorarioLaboralFiltro();

			// Buscar si hay predeterminado
			filtro = new HorarioLaboralFiltro();
			filtro.setPredeterminado(true);
			// filtro.setEstatus(1);
			List<HorarioLaboral> predeterminados = buscar(HorarioLaboral.class,
					filtro);
			// Si hay predeterminado
			if (predeterminados != null && predeterminados.size() > 0) {
				// Si se va a guardar como predeterminado, quitar
				// predeterminado anterior
				if (predeterminado && activo) {
					for (HorarioLaboral predeterminadoActual : predeterminados) {
						predeterminadoActual.setPredeterminado(false);
						this.salvar(predeterminadoActual);
					}
				}
				// Si no hay predeterminado, guardar como predeterminado
			} else {
				horarioLaboral
						.setPredeterminado((horarioLaboral.getEstatus() == 1));
			}
		}
	}

	/**
	 * Funcion que valida la RDN {XXXXXXXX-XXXXX-XXXX-XXXXX-XXXXXXXXXXXX} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: XXXXX XXX XXXX
	 * - RDN No podrá editar las horas de un horario guardado previamente.
	 */
	private void validarHorariosEdicion(HorarioLaboral horarioLaboral,
			HorarioLaboral horarioPrevio) throws NegocioEJBExeption {
		String msgError = "No pueden editar las horas de un horario guardado previamente";

		if (horarioPrevio != null) {
			if (!horarioLaboral.getHoraEntradaCode().equalsIgnoreCase(
					horarioPrevio.getHoraEntradaCode())
					|| !(horarioLaboral.getHoraSalidaCode()
							.equalsIgnoreCase(horarioPrevio.getHoraSalidaCode()))) {
				throwNegocioEJBExeption(msgError);
			}
		}
	}

	private void throwNegocioEJBExeption(String msgError)
			throws NegocioEJBExeption {
		String[] mensajesArray = { msgError };
		throw new NegocioEJBExeption("Error", mensajesArray, "validacion");
	}

	private void setearValoresControl(HorarioLaboral horarioLaboral,
			HorarioLaboral horarioPrevio) {
		String codigoUsuarioActual = this.usuarioService.getUsuarioActual()
				.getNombreUsuario();
		String codigoUsuarioCreacion = (horarioPrevio == null) ? codigoUsuarioActual
				: horarioPrevio.getCodigoUsuarioCreacion();
		Date fechaCreacion = (horarioPrevio == null) ? new Date()
				: horarioPrevio.getFechaCreacion();

		horarioLaboral.setFechaModificacion(new Date());
		horarioLaboral.setCodigoUsuarioModificacion(codigoUsuarioActual);

		horarioLaboral.setFechaCreacion(fechaCreacion);
		horarioLaboral.setCodigoUsuarioCreacion(codigoUsuarioCreacion);

	}

}
