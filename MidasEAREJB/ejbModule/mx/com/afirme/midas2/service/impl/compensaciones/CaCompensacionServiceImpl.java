/**
 * 
 * Powered by AddMotions. 
 * Monterrey,Mexico
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.compensaciones.CaBancaPresupuestoAnualDao;
import mx.com.afirme.midas2.dao.compensaciones.CaCompensacionDao;
import mx.com.afirme.midas2.dao.compensaciones.CalculoCompensacionesDao;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaBancaPresupuestoAnualDaoImpl;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaCompensacionDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.compensaciones.CaAutorizacion;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaPresupuestoAnual;
import mx.com.afirme.midas2.domain.compensaciones.CaBitacora;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaEstatus;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoAutorizacion;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoContrato;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoEntidad;
import mx.com.afirme.midas2.domain.compensaciones.Proveedor;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.dto.compensaciones.ProductoVidaDTO;
import mx.com.afirme.midas2.service.compensaciones.CaAutorizacionService;
import mx.com.afirme.midas2.service.compensaciones.CaBaseCalculoService;
import mx.com.afirme.midas2.service.compensaciones.CaBitacoraService;
import mx.com.afirme.midas2.service.compensaciones.CaCompensacionService;
import mx.com.afirme.midas2.service.compensaciones.CaCorreosService;
import mx.com.afirme.midas2.service.compensaciones.CaEntidadPersonaService;
import mx.com.afirme.midas2.service.compensaciones.CaEstatusService;
import mx.com.afirme.midas2.service.compensaciones.CaLineaNegocioService;
import mx.com.afirme.midas2.service.compensaciones.CaParametrosService;
import mx.com.afirme.midas2.service.compensaciones.CaRamoService;
import mx.com.afirme.midas2.service.compensaciones.CaRangosService;
import mx.com.afirme.midas2.service.compensaciones.CaSubramosDaniosService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoAutorizacionService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoCompensacionService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoCondicionCalculoService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoContratoService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoEntidadService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoMonedaService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoProvisionService;
import mx.com.afirme.midas2.service.compensaciones.CatalogoCompensacionesService;
import mx.com.afirme.midas2.service.compensaciones.negociovida.NegocioVidaService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaJPAService;
import mx.com.afirme.midas2.service.negocio.agente.NegocioAgenteService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales.RAMO;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

@Stateless

public class CaCompensacionServiceImpl  implements CaCompensacionService {
	
	@EJB
	private CaCompensacionDao compensAdiciocaDao;
	
	@EJB
	private CaRamoService ramocaService;
	
	@EJB
	private CaEntidadPersonaService entidadPersonacaService;
	
	@EJB
	private CaAutorizacionService caAutorizacionService;
	
	@EJB
	private CaTipoAutorizacionService caTipoAutorizacionService;
	
	@EJB
	private CaEstatusService caEstatusService; 
	
	@EJB 
	private CaTipoEntidadService tipoEntidadcaService;
	
	@EJB
	private CaParametrosService parametrosGralescaService;
	
	@EJB
	private AgenteMidasService agenteMidasService;
	
	@EJB
	private CaTipoCompensacionService tipoCompensacioncaService;
	
	@EJB
	private CaRangosService caRangosService;
	
	@EJB
	private CaLineaNegocioService caLineaNegocioService;	
	
	@EJB
	private CatalogoCompensacionesService catalogoCompensacionesService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private CaTipoContratoService tipoContratoService;
	
	@EJB
	private CaBitacoraService bitacoraService;
	
	@EJB
	private CaCorreosService caCorreosService;
	
	@EJB
	private CalculoCompensacionesDao calculoCompensacionesDao;
	
	@EJB
	private CaSubramosDaniosService caSubramosDaniosService;
	
	@EJB
	private CaBaseCalculoService caBaseCalculoService;
	
	@EJB
	private CaTipoMonedaService caTipoMonedaService;
	
	@EJB
	private CaTipoCondicionCalculoService caTipoCondicionCalculoService;
	
	@EJB
	private CaTipoProvisionService caTipoProvisionService;
	
	@EJB
	private NegocioAgenteService negocioAgenteService;
	
	@EJB
	private PromotoriaJPAService promotoriaJPAService;
	
	@EJB
	private NegocioVidaService negocioVidaService;
	
	@EJB
	private CaBancaPresupuestoAnualDao caBancaPresupuestoAnualDao;
	
	private final static String PROCEDURE_MODIFICACION_VIDA = "MIDAS.PKG_CA_ORDENPAGO.cambiarConfiguracionVida";
	
	private List<CaBitacora> listBitacora;
	
    private static final Logger LOG = Logger.getLogger(CaCompensacionServiceImpl.class);

	/**
	 Perform an initial save of a previously unsaved CaCompensacion entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaCompensacion entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public CaCompensacion save(CaCompensacion entity) {
    	
    	LOG.debug(">> save()");
    	try {

    		compensAdiciocaDao.save(entity);
    		
    		LOG.debug(" << save()");
	        } catch (RuntimeException re) {
	        LOG.error("Informacion del error", re);
	        	throw re;
        }
	        return entity;
    }
    
    /**
	 Delete a persistent CaCompensacion entity.
	  @param entity CaCompensacion entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaCompensacion entity) {
    	
    	LOG.debug(">> delete()");
    	
    	try {

    		compensAdiciocaDao.delete(entity);
            
	    LOG.debug(" << delete()");
    	} catch (RuntimeException re) {
	        	LOG.error("Informacion del error", re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaCompensacion entity and return it or a copy of it to the sender. 
	 A copy of the CaCompensacion entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaCompensacion entity to update
	 @return CaCompensacion the persisted CaCompensacion entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    
    public CaCompensacion update(CaCompensacion entity) {
    	LOG.debug(">> update()");
    	try {
    		entity.setUsuario(usuarioService.getUsuarioActual().getNombre());
    		entity.setFechaModificacion(new Date());	
    		CaCompensacion result = compensAdiciocaDao.update(entity);
            LOG.debug(" << update()");
	        return result;
        } catch (RuntimeException re) {
        	LOG.error("Informacion del error", re);
        	throw re;
        }
    }
    
    public CaCompensacion findById( Long id) {
    	LOG.debug(">> findById()");
    	CaCompensacion result;    	
    	try {
    		result = compensAdiciocaDao.findById(id);   		
        } catch (RuntimeException re) {
        	LOG.error("Informacion",re);
        	result = null;
        }        
        LOG.debug(" << findById()");
        return result;
    }    
    

/**
	 * Find all CaCompensacion entities with a specific property value.  
	 
	  @param propertyName the name of the CaCompensacion property to query
	  @param value the property value to match
	  	  @return List<CaCompensacion> found by query
	 */
    
	public List<CaCompensacion> findByProperty(String propertyName, final Object value){
		LOG.debug(">> findByProperty()");
    	List<CaCompensacion> result;
    	try {
			
    		result = compensAdiciocaDao.findByProperty(propertyName, value);
    		
		} catch (RuntimeException re) {
			LOG.error("Informacion",re);
			result = null;
		}
		LOG.debug(" << findByProperty()");
		return result;
	}			
	
	public List<CaCompensacion> findByPolizaId(Object polizaId) {
		
		return compensAdiciocaDao.findByPolizaId(polizaId);
	}
	
	public List<CaCompensacion> findByNegocioId(Object negocioId) {
		
		return compensAdiciocaDao.findByNegocioId(negocioId);
	}
	
	public List<CaCompensacion> findByContraprestacion(Object contraprestacion){
		
		return compensAdiciocaDao.findByContraprestacion(contraprestacion);
		
	}
	
	public List<CaCompensacion> findByCuadernoconcurso(Object cuadernoconcurso) {
		
		return compensAdiciocaDao.findByContraprestacion(cuadernoconcurso);
		
	}
	
	public List<CaCompensacion> findByConvenioespecial(Object convenioespecial) {
		
		return compensAdiciocaDao.findByConvenioespecial(convenioespecial);
		
	}
	
	public List<CaCompensacion> findByRetroactivo(Object retroactivo) {
		
		return compensAdiciocaDao.findByRetroactivo(retroactivo);
		
	}
	
	public List<CaCompensacion> findByModificable(Object modificable) {
		
		return compensAdiciocaDao.findByModificable(modificable);
		
	}
	
	public List<CaCompensacion> findByExcepciondocumentos(Object excepciondocumentos) {
		
		return compensAdiciocaDao.findByExcepciondocumentos(excepciondocumentos);
		
	}
	
	public List<CaCompensacion> findByCompensacionporsubramos(Object compensacionporsubramos) {
		
		return compensAdiciocaDao.findByCompensacionporsubramos(compensacionporsubramos);
		
	}
	
	public List<CaCompensacion> findByUsuario(Object usuario) {
		
		return compensAdiciocaDao.findByUsuario( usuario);
		
	}
	
	public List<CaCompensacion> findByBorradologico(Object borradologico) {
		
		return compensAdiciocaDao.findByBorradologico(borradologico);
		
	}
	
	public BigDecimal findIdCompensAdicioNext(){
		LOG.debug(">> findIdCompensAdicioNext()");
		
		BigDecimal result=new BigDecimal(0);
		
		try {			
			result = compensAdiciocaDao.findIdCompensAdicioNext();
		
		} catch (RuntimeException re) {
			LOG.error("Informacion",re);
		}
		LOG.debug(" << findIdCompensAdicioNext()");
		return result;
	}
	
	/**
	 * Find all CaCompensacion entities.
	  	  @return List<CaCompensacion> all CaCompensacion entities
	 */	
	public List<CaCompensacion> findAll() {
		LOG.debug(">> findAll()");
		List<CaCompensacion> result;		
		try {

			result = compensAdiciocaDao.findAll();
			
		} catch (RuntimeException re) {
			LOG.error("Informacion del error", re);
			result = null;
		}
		LOG.debug(" << findAll()");
		return result;
	}	
	
	public CaCompensacion  findByRamoAndFieldAndContraprestacion(Long ramoId, String field, Object valueFiled, boolean contraprestacion){
				
		LOG.debug(">> findByRamoAndFieldAndContraprestacion()");
		
		CaCompensacion caCompenacion = null;
		
		try{
			caCompenacion = compensAdiciocaDao.findByRamoAndFieldAndContraprestacion(ramoId, field, valueFiled, contraprestacion);
		
		}catch (Exception e) {
			LOG.error("Informacion del error", e);
		}
		
		LOG.debug(">> findByRamoAndFieldAndContraprestacion()");
		
		return caCompenacion;
	}
	
	/**
	 * Busqueda de compensacion
	 */
	public CaCompensacion findConfiguracion(CompensacionesDTO compensacionDTO, RAMO ramo){
	    
		LOG.debug(">> findForRamoAndContraprestacion()");
		
		CaCompensacion caCompensacion = null;
        try {	            
            switch (ramo) {
	            case AUTOS:
					caCompensacion = this.findByRamoAndFieldAndContraprestacion(ramo.getValor(), CaCompensacionDaoImpl.NEGOCIO_ID , compensacionDTO.getIdNegocio(), compensacionDTO.isContraprestacion());        		
					compensacionDTO.setRamo(ramocaService.findById(ramo.getValor()));
					break;
				case DANOS:
					caCompensacion = this.findByRamoAndFieldAndContraprestacion(ramo.getValor(), CaCompensacionDaoImpl.COTIZACION_ID , compensacionDTO.getCotizacionId(), compensacionDTO.isContraprestacion());        		
					compensacionDTO.setRamo(ramocaService.findById(ramo.getValor()));
					break;
				case VIDA:
					caCompensacion = this.findByRamoAndFieldAndContraprestacion(ramo.getValor(), CaCompensacionDaoImpl.NEGOCIOVIDA_ID , compensacionDTO.getIdNegocioVida(), compensacionDTO.isContraprestacion());        		
					compensacionDTO.setRamo(ramocaService.findById(ramo.getValor()));
					break;
				case BANCA:
					caCompensacion = this.findByRamoAndFieldAndContraprestacion(ramo.getValor(), CaCompensacionDaoImpl.CONFIGURACIONBANCA_ID , compensacionDTO.getIdConfiguracionBanca(), compensacionDTO.isContraprestacion());        		
					compensacionDTO.setRamo(ramocaService.findById(ramo.getValor()));    		
					break;
			}
        	            
        	if( compensacionDTO.isContraprestacion()){
        		this.configContraprestacion(caCompensacion, compensacionDTO);	
        	} else{
        		this.configCompensacion(caCompensacion, compensacionDTO);
        	}        	
             
        	parametrosGralescaService.findParametrosForCompensacion(caCompensacion, compensacionDTO);
        	
        } catch (Exception e) {
        	LOG.error("Informacion del error", e);
             caCompensacion = null;
        }
        
        LOG.debug(" << findForRamoAndContraprestacion()");
        
        return caCompensacion;
	}
	
	/**
	 * Configura la Contraprestacion
	 * @param caCompensacion
	 * @param compensacionDTO
	 * @param listEntidadPersona
	 * @param caEntidadPersona
	 */
	private void configContraprestacion(CaCompensacion caCompensacion,CompensacionesDTO compensacionDTO){
		 
		LOG.debug(">> configContraprestacion()");
		
		List<CaEntidadPersona> listEntidadPersona = null;	
	
		if (caCompensacion != null){
			listEntidadPersona = entidadPersonacaService.findByCompensAdicioId(caCompensacion.getId());		
			compensacionDTO.setUsuarioModificacion(caCompensacion.getUsuario());
			compensacionDTO.setUltimaModificacion(caCompensacion.getFechaModificacion());
			/**
			 *El estatus de Autorizada para la Contraprestaciones
			 *en vida indica que pueden hacerce modificaciones
			 */
			compensacionDTO.setEstatusContraprestacionId(caCompensacion.getCaEstatus().getId() != null ? caCompensacion.getCaEstatus().getId().intValue() : 0);
			if(RAMO.VIDA.getValor().equals(compensacionDTO.getRamo().getId())){	
				compensacionDTO.setFechaModificacionVida(caCompensacion.getFechaModificacionVida());
				compensacionDTO.setAplicaComision(caCompensacion.getAplicaComision() ? 1 : 0);
			}
			
		}else{
			listEntidadPersona = new ArrayList<CaEntidadPersona>();
		}
		
		CaEntidadPersona caEntidadPersona = compensacionDTO.getCaEntidadPersona();
		if(caEntidadPersona != null){
			compensacionDTO.setCaTipoContrato(caEntidadPersona.getCaTipoContrato());
			compensacionDTO.setClaveProveedorContra(caEntidadPersona.getIdProveedor().toString());
			compensacionDTO.setNombreProveedorContra(caEntidadPersona.getNombres());
		}else if(!listEntidadPersona.isEmpty()){
			caEntidadPersona = listEntidadPersona.get(0);
			compensacionDTO.setCaTipoContrato(caEntidadPersona.getCaTipoContrato());
			compensacionDTO.setClaveProveedorContra(caEntidadPersona.getIdProveedor().toString());
			compensacionDTO.setNombreProveedorContra(caEntidadPersona.getNombres());
			compensacionDTO.setCaEntidadPersona(caEntidadPersona);
		}
		compensacionDTO.setListEntidadPersonaca(listEntidadPersona);
		LOG.debug(" << configContraprestacion()");
    	
	}
	
	/**
	 * Configura la Compensacion
	 * @param caCompensacion
	 * @param compensacionDTO
	 * @param listEntidadPersona
	 * @param caEntidadPersona
	 */
	private void configCompensacion(CaCompensacion caCompensacion,CompensacionesDTO compensacionDTO){

		LOG.debug(">> configCompensacion()");
		
		List<CaEntidadPersona> listEntidadPersona = null;	
		
		if (caCompensacion != null){
			listEntidadPersona = entidadPersonacaService.findByCompensAdicioId(caCompensacion.getId());
			compensacionDTO.setUsuarioModificacion(caCompensacion.getUsuario());
			compensacionDTO.setUltimaModificacion(caCompensacion.getFechaModificacion());
			/**
			 *El estatus de Autorizada para la compensacion
			 *en vida indica que pueden hacerce modificaciones
			 */
			compensacionDTO.setEstatusCompensacionId(caCompensacion.getCaEstatus().getId() != null ? caCompensacion.getCaEstatus().getId().intValue() : 0);
			if(RAMO.VIDA.getValor().equals(compensacionDTO.getRamo().getId())){	
				compensacionDTO.setFechaModificacionVida(caCompensacion.getFechaModificacionVida());
				compensacionDTO.setAplicaComision(caCompensacion.getAplicaComision() ? 1 : 0);
			}
		}else if(RAMO.AUTOS.getValor().equals(compensacionDTO.getRamo().getId())){	
			listEntidadPersona = new ArrayList<CaEntidadPersona>();
			List<NegocioAgente> listNegocioAgente = negocioAgenteService.listarNegocioAgentesAsociados(compensacionDTO.getIdNegocio());
			
			for (NegocioAgente agente : listNegocioAgente) {
				promotoriaJPAService.loadById(agente.getAgente().getPromotoria());
				CaTipoEntidad tipoentidadca = tipoEntidadcaService.findById(ConstantesCompensacionesAdicionales.AGENTE);
				Agente agenteActual = agente.getAgente();
				String nombre = new StringBuilder(agenteActual.getPersona().getNombre()).append(" ").append(agenteActual.getPersona().getApellidoPaterno()).append(" ").append(agenteActual.getPersona().getApellidoMaterno()).toString();
				CaEntidadPersona entidadPersonaca = new CaEntidadPersona(null,
																		agenteActual, 
																		tipoentidadca, 
																		caCompensacion, 
																		nombre,
																		null,
																		new Date(), 
																		new Date(), 
																		usuarioService.getUsuarioActual().getNombre(),
																		ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
				listEntidadPersona.add(entidadPersonaca);
			}
		}else{
			listEntidadPersona = new ArrayList<CaEntidadPersona>();
		}
		
		CaEntidadPersona caEntidadPersona = compensacionDTO.getCaEntidadPersona();
		if(caEntidadPersona != null){			
			compensacionDTO.setNombreAgenteGral(caEntidadPersona.getNombres());				
			if(RAMO.VIDA.getValor().equals(compensacionDTO.getRamo().getId())){
				compensacionDTO.setClaveAgenteGral(caEntidadPersona.getIdAgente().toString());				
				DatosSeycos agenteSeycosFind = new DatosSeycos();
				agenteSeycosFind.setIdAgente(caEntidadPersona.getIdAgente().toString());
				agenteSeycosFind.setIdEmpresa(caEntidadPersona.getIdEmpresa().toString());
				agenteSeycosFind.setDateFsit(caEntidadPersona.getFsit());
				List<DatosSeycos> listDatosSeycos = negocioVidaService.findAgenteSeycosConPromotor(agenteSeycosFind);
				if(!listDatosSeycos.isEmpty()){
					agenteSeycosFind = listDatosSeycos.get(0);
					if(agenteSeycosFind.getIdSupervisoria() != null && Long.valueOf(agenteSeycosFind.getIdSupervisoria().trim()) > 0){
						compensacionDTO.setClavePromotorGral(agenteSeycosFind.getIdSupervisoria());
						compensacionDTO.setNombrePromotorGral(agenteSeycosFind.getNombreSupervisoria());
					}
				}
				if(agenteSeycosFind!=null)
					compensacionDTO.setDatosSeycos(agenteSeycosFind);
			}else{
				compensacionDTO.setClaveAgenteGral(caEntidadPersona.getAgente().getIdAgente().toString());
				Persona pReponsable = caEntidadPersona.getAgente().getPromotoria().getPersonaResponsable();
				if(pReponsable != null && pReponsable.getIdPersona() != null){
					compensacionDTO.setClavePromotorGral(caEntidadPersona.getAgente().getPromotoria().getId().toString());
					compensacionDTO.setNombrePromotorGral(caEntidadPersona.getAgente().getPromotoria().getDescripcion());
				}
			}
		}else if(!listEntidadPersona.isEmpty()){
			caEntidadPersona = listEntidadPersona.get(0);
			compensacionDTO.setNombreAgenteGral(caEntidadPersona.getNombres());	
			if(RAMO.VIDA.getValor().equals(compensacionDTO.getRamo().getId())){
				compensacionDTO.setClaveAgenteGral(caEntidadPersona.getIdAgente().toString());				
				DatosSeycos agenteSeycosFind = new DatosSeycos();
				agenteSeycosFind.setIdAgente(caEntidadPersona.getIdAgente().toString());
				agenteSeycosFind.setIdEmpresa(caEntidadPersona.getIdEmpresa().toString());
				agenteSeycosFind.setDateFsit(caEntidadPersona.getFsit());
				List<DatosSeycos> listDatosSeycos = negocioVidaService.findAgenteSeycosConPromotor(agenteSeycosFind);
				if(!listDatosSeycos.isEmpty()){
					agenteSeycosFind = listDatosSeycos.get(0);
					if(agenteSeycosFind.getIdSupervisoria() != null && Long.valueOf(agenteSeycosFind.getIdSupervisoria().trim()) > 0){
						compensacionDTO.setClavePromotorGral(agenteSeycosFind.getIdSupervisoria());
						compensacionDTO.setNombrePromotorGral(agenteSeycosFind.getNombreSupervisoria());
					}
				}
				if(agenteSeycosFind!=null)
					compensacionDTO.setDatosSeycos(agenteSeycosFind);
			}else{				
				compensacionDTO.setClaveAgenteGral(caEntidadPersona.getAgente().getIdAgente().toString());
				Persona pReponsable = caEntidadPersona.getAgente().getPromotoria().getPersonaResponsable();
				if(pReponsable != null && pReponsable.getIdPersona() != null){
				compensacionDTO.setClavePromotorGral(caEntidadPersona.getAgente().getPromotoria().getId().toString());
				compensacionDTO.setNombrePromotorGral(caEntidadPersona.getAgente().getPromotoria().getDescripcion());
				}				
			}
			compensacionDTO.setCaEntidadPersona(caEntidadPersona);
		}	
		compensacionDTO.setListEntidadPersonaca(listEntidadPersona);		
		LOG.debug(" << configCompensacion()");
	}
	
	/**
	 * Autorizacion de Compensacion Adicional
	 * 
	 * @param ramo
	 * @param compensacionId
	 * @param identificador
	 */
	public void autorizarCompensacion(String ramo, Long compensacionId, Long identificador, Long tipoAutorizacionId, boolean flagContra){
		LOG.debug(">> autorizarCompensacion()");
		CaCompensacion compensacion = null;		
		
		RAMO caRamo = RAMO.findByType(ramo);
		
		switch (caRamo) {		
			case AUTOS:			
				compensacion = this.findByRamoAndFieldAndContraprestacion(caRamo.getValor(), CaCompensacionDaoImpl.NEGOCIO_ID, identificador, flagContra);			
				if(compensacion != null){				
					this.buscarAutorizacionesCompensaciones(compensacion, tipoAutorizacionId);				
				}
				break;
				
			case DANOS:			
				compensacion = this.findByRamoAndFieldAndContraprestacion(caRamo.getValor(), CaCompensacionDaoImpl.COTIZACION_ID, identificador, flagContra);			
				if(compensacion != null){				
					this.buscarAutorizacionesCompensaciones(compensacion, tipoAutorizacionId);				
				}
				break;
				
			case VIDA:			
				compensacion = this.findByRamoAndFieldAndContraprestacion(caRamo.getValor(), CaCompensacionDaoImpl.NEGOCIOVIDA_ID, identificador, flagContra);			
				if(compensacion != null){				
					this.buscarAutorizacionesCompensaciones(compensacion, tipoAutorizacionId);				
				}
				break;		
				
			case BANCA:			
				compensacion = this.findByRamoAndFieldAndContraprestacion(caRamo.getValor(), CaCompensacionDaoImpl.CONFIGURACIONBANCA_ID, identificador, flagContra);			
				if(compensacion != null){				
					this.buscarAutorizacionesCompensaciones(compensacion, tipoAutorizacionId);				
				}
				break;
		}
		LOG.debug("<< autorizarCompensacion()");
	}
	
	/**
	 * Busca las Autorizaciones Correspondientes a la
	 * Compensacion o Contraprestacion
	 * @param caCompensacion
	 * @param tipoAutorizacionId
	 * @param caEstatus
	 */
	private void buscarAutorizacionesCompensaciones(CaCompensacion caCompensacion, Long tipoAutorizacionId){
		LOG.debug(">> buscarAutorizacionesCompensaciones()");
		
		CaEstatus caEstatus;

		List<CaAutorizacion> listAutorizacionCompensacion = caAutorizacionService.findByCompensacionid(caCompensacion.getId());
		
		if(listAutorizacionCompensacion != null && !listAutorizacionCompensacion.isEmpty()){			
			/**
			 * Autorizaciones para Contraprestaciones
			 */
			if(caCompensacion.getContraprestacion()){
				
				boolean autorizacionTecnicaAgente = false, autorizacionJuridico = false;
				
				
				for(CaAutorizacion caAutorizacion: listAutorizacionCompensacion){
					
					if(caAutorizacion.getCaTipoAutorizacion().getId().equals(ConstantesCompensacionesAdicionales.AUTORIZACION_DIRECTORTECNICO)
							|| caAutorizacion.getCaTipoAutorizacion().getId().equals(ConstantesCompensacionesAdicionales.AUTORIZACION_ADMONAGENTES)){
						
						autorizacionTecnicaAgente = caAutorizacion.getCaEstatus().getId().equals(ConstantesCompensacionesAdicionales.ESTATUS_APROBADO);
						
						if(autorizacionTecnicaAgente && tipoAutorizacionId.equals(ConstantesCompensacionesAdicionales.AUTORIZACION_JURIDICO)){
							
							for(CaAutorizacion caAutorizacionFinal: listAutorizacionCompensacion){
								
								if(caAutorizacionFinal.getCaTipoAutorizacion().getId().equals(ConstantesCompensacionesAdicionales.AUTORIZACION_JURIDICO)){
									
									autorizacionJuridico = caAutorizacionFinal.getCaEstatus().getId().equals(ConstantesCompensacionesAdicionales.ESTATUS_APROBADO);
									
									if(!autorizacionJuridico){
										caEstatus = caEstatusService.findById(ConstantesCompensacionesAdicionales.ESTATUS_APROBADO);
										caAutorizacionFinal.setCaEstatus(caEstatus);						
										caAutorizacionService.update(caAutorizacionFinal);	
										caCompensacion.setCaEstatus(caEstatus);
										this.update(caCompensacion);
										listBitacora = bitacoraService.findByProperty(CaBitacoraServiceImpl.COMPENSACION_ID, caCompensacion.getId());
										if(listBitacora != null && listBitacora.size() > 2){
											this.aprobarMoficacionCompensacion(caCompensacion);
										}
										this.guardarBitacora(caCompensacion.getId(),ConstantesCompensacionesAdicionales.AUTORIZACION_AREA_JURIDICA);
									}
									break;
								}
							}
							break;
						}else if(caAutorizacion.getCaTipoAutorizacion().getId().equals(ConstantesCompensacionesAdicionales.AUTORIZACION_DIRECTORTECNICO)
								&& tipoAutorizacionId.equals(ConstantesCompensacionesAdicionales.AUTORIZACION_DIRECTORTECNICO)){
							caEstatus = caEstatusService.findById(ConstantesCompensacionesAdicionales.ESTATUS_APROBADO);
							caAutorizacion.setCaEstatus(caEstatus);						
							caAutorizacionService.update(caAutorizacion);	
							this.update(caCompensacion);
							this.guardarBitacora(caCompensacion.getId(),ConstantesCompensacionesAdicionales.AUTORIZACION_AREA_TECNICA);							
							/**
							 * Notificacion por envio de correo a Juridico para que autorice la Contraprestacion
							 */
							caCorreosService.notificacionAutorizacionesCorreo(ConstantesCompensacionesAdicionales.AUTORIZACION_AREA_TECNICA, caCompensacion);	
							break;
						}else if(caAutorizacion.getCaTipoAutorizacion().getId().equals(ConstantesCompensacionesAdicionales.AUTORIZACION_ADMONAGENTES)
								&& tipoAutorizacionId.equals(ConstantesCompensacionesAdicionales.AUTORIZACION_ADMONAGENTES)){
							caEstatus = caEstatusService.findById(ConstantesCompensacionesAdicionales.ESTATUS_APROBADO);
							caAutorizacion.setCaEstatus(caEstatus);						
							caAutorizacionService.update(caAutorizacion);
							this.update(caCompensacion);
							this.guardarBitacora(caCompensacion.getId(),ConstantesCompensacionesAdicionales.AUTORIZACION_ADMINISTRADOR_AGENTE);
							/**
							 * Notificacion por envio de correo a Juridico para que autorice la Contraprestacion
							 */
							caCorreosService.notificacionAutorizacionesCorreo(ConstantesCompensacionesAdicionales.AUTORIZACION_ADMINISTRADOR_AGENTE, caCompensacion);	
							break;
						}
						
					}					
				}
				
			}
			/**
			 * Autorizaciones para Compensaciones
			 */
			else{
				
				for(CaAutorizacion caAutorizacion: listAutorizacionCompensacion){					
					/**
					 * Para las Compensaciones el Area Tecnica o Admin de Agentes puede autorizar la Compensacion
					 */
					if(caAutorizacion.getCaTipoAutorizacion().getId().equals(tipoAutorizacionId)){
						caEstatus = caEstatusService.findById(ConstantesCompensacionesAdicionales.ESTATUS_APROBADO);
						caAutorizacion.setCaEstatus(caEstatus);						
						caAutorizacionService.update(caAutorizacion);	
						caCompensacion.setCaEstatus(caEstatus);
						this.update(caCompensacion);		
						listBitacora = bitacoraService.findByProperty(CaBitacoraServiceImpl.COMPENSACION_ID, caCompensacion.getId());
						if(listBitacora != null && listBitacora.size() > 1){
							this.aprobarMoficacionCompensacion(caCompensacion);
						}
						if(tipoAutorizacionId.equals(ConstantesCompensacionesAdicionales.AUTORIZACION_DIRECTORTECNICO)){
							this.guardarBitacora(caCompensacion.getId(), ConstantesCompensacionesAdicionales.AUTORIZACION_AREA_TECNICA);
						}else if(tipoAutorizacionId.equals(ConstantesCompensacionesAdicionales.AUTORIZACION_ADMONAGENTES)){
							this.guardarBitacora(caCompensacion.getId(), ConstantesCompensacionesAdicionales.AUTORIZACION_ADMINISTRADOR_AGENTE);
						}	                      
					}					
				}
				
			}
			
			
		}
		LOG.debug("<< buscarAutorizacionesCompensaciones()");	
	}
	
	
	/**
	 * 
	 * @param caCompensacion
	 */
	private void aprobarMoficacionCompensacion(CaCompensacion caCompensacion){
				
		if(RAMO.VIDA.getValor().equals(caCompensacion.getCaRamo().getId())){			
			if(caCompensacion.getFechaModificacionVida() != null){
				this.modificacionConfiguracionVida(caCompensacion);
			}			
		}
		if(RAMO.DANOS.getValor().equals(caCompensacion.getCaRamo().getId())){
			CaCompensacion auxCompensacion;
			if(caCompensacion.getContraprestacion()){//Contraprestacion
				//validar si tiene compensacion
				auxCompensacion = this.findByRamoAndFieldAndContraprestacion(caCompensacion.getCaRamo().getId(),CaCompensacionDaoImpl.COTIZACION_ID,caCompensacion.getCotizacionId().longValue(),false);			
				//validar si la compensacion esta autorizada
				if(auxCompensacion!=null){
					if(auxCompensacion.getCaEstatus().getNombre().equals("APROBADO")){
						if(caCompensacion.getIdReciboModificacion()>0)
							this.compensAdiciocaDao.setConfiguracionByIdRecibo(caCompensacion.getIdReciboModificacion());
					}
				}else{
					if(caCompensacion.getIdReciboModificacion()>0)
						this.compensAdiciocaDao.setConfiguracionByIdRecibo(caCompensacion.getIdReciboModificacion());
				}
			}else{//Compensacion
				//validar si tiene contraprestacion
				auxCompensacion = this.findByRamoAndFieldAndContraprestacion(caCompensacion.getCaRamo().getId(),CaCompensacionDaoImpl.COTIZACION_ID, caCompensacion.getCotizacionId().longValue(),true);			
				//validar si la contraprestacion esta autorizada
				if(auxCompensacion!=null){
					if(auxCompensacion.getCaEstatus().getNombre().equals("APROBADO")){
						if(caCompensacion.getIdReciboModificacion()>0)
							this.compensAdiciocaDao.setConfiguracionByIdRecibo(caCompensacion.getIdReciboModificacion());
					}
				}else{
					if(caCompensacion.getIdReciboModificacion()>0)
						this.compensAdiciocaDao.setConfiguracionByIdRecibo(caCompensacion.getIdReciboModificacion());
				}
			}
        }
		if(RAMO.AUTOS.getValor().equals(caCompensacion.getCaRamo().getId())){
			if(caCompensacion.getId()>0){
				this.compensAdiciocaDao.setConfiguracionByIdCompensacionAut(caCompensacion.getId().intValue());
			}
		}
	}
	
	/**
	 * Guardado de la Autorizacion relacionada a una Compensacion
	 * @param caCompensacion
	 * @param tipoAutorizacionId
	 */
	private void guardarAutorizacion(CaCompensacion caCompensacion, Long tipoAutorizacionId, CaEstatus caEstatus){
		LOG.debug(">> guardarAutorizacion()");
		CaTipoAutorizacion caTipoAutorizacion = caTipoAutorizacionService.findById(tipoAutorizacionId);
		
		CaAutorizacion autorizacion = new CaAutorizacion();
		autorizacion.setCaCompensacion(caCompensacion);
		autorizacion.setCaTipoAutorizacion(caTipoAutorizacion);
		autorizacion.setFechaCreacion(new Date());
		autorizacion.setFechaModificacion(new Date());
		autorizacion.setBorradoLogico(ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
		autorizacion.setUsuario(caCompensacion.getUsuario());
		autorizacion.setValor(caTipoAutorizacion.getNombre());
		
		autorizacion.setCaEstatus(caEstatus);
		
		caAutorizacionService.save(autorizacion);
		LOG.debug("<< guardarAutorizacion()");
	}

	/**
	 * Guardado de la Compensacion Adicional
	 * @return
	 */
	public void guardarCompensacion(List<CompensacionesDTO> listCompensacionesDTO) {
		LOG.debug(">> guardarCompensacion()");
		
		CompensacionesDTO compensacionDTO = null;
		CaCompensacion caCompensacion = null;		
		boolean isNewRecord = false;
		
		/***
		 * Validacion de Datos
		 */
		if(listCompensacionesDTO.isEmpty()){
			return;		
		}else{
			compensacionDTO = listCompensacionesDTO.get(0);	
		}
						
		RAMO ramo = RAMO.findByType(compensacionDTO.getRamo().getIdentificador());
		
		switch (ramo) {
		
			case AUTOS:
				caCompensacion = this.findByRamoAndFieldAndContraprestacion(ramo.getValor(),  CaCompensacionDaoImpl.NEGOCIO_ID, compensacionDTO.getIdNegocio(), compensacionDTO.isContraprestacion());
				if (caCompensacion == null) {
					caCompensacion = new CaCompensacion();
					caCompensacion.setContraprestacion(compensacionDTO.isContraprestacion());
					caCompensacion.setNegocioId(compensacionDTO.getIdNegocio());
					caCompensacion = this.crearCompesacion(caCompensacion, ramo);
					isNewRecord = true;
				}else{
					caCompensacion = this.actualizarCompensacion(caCompensacion);
					isNewRecord = false;
				}
				break;
				
			case DANOS:
				caCompensacion = this.findByRamoAndFieldAndContraprestacion(ramo.getValor(),  CaCompensacionDaoImpl.COTIZACION_ID, compensacionDTO.getCotizacionId(), compensacionDTO.isContraprestacion());
				if (caCompensacion == null) {
					caCompensacion = new CaCompensacion();
					caCompensacion.setContraprestacion(compensacionDTO.isContraprestacion());
					caCompensacion.setCotizacionId(new BigDecimal(compensacionDTO.getCotizacionId()));
					if(compensacionDTO.getPolizaId() != null && compensacionDTO.getPolizaId().longValue() > 0){
						caCompensacion.setPolizaId(compensacionDTO.getPolizaId());
					}
					caCompensacion = this.crearCompesacion(caCompensacion, ramo);
					isNewRecord = true;
				}else{
					caCompensacion.setIdReciboModificacion(compensacionDTO.getIdRecibo());
					caCompensacion = this.actualizarCompensacion(caCompensacion);
					isNewRecord = false;
					
				}
				break;
				
			case VIDA:				
				caCompensacion = this.findByRamoAndFieldAndContraprestacion(ramo.getValor(),  CaCompensacionDaoImpl.NEGOCIOVIDA_ID, compensacionDTO.getIdNegocioVida(), compensacionDTO.isContraprestacion());
				if (caCompensacion == null) {
					caCompensacion = new CaCompensacion();
					caCompensacion.setContraprestacion(compensacionDTO.isContraprestacion());
					caCompensacion.setNegocioVidaId(compensacionDTO.getIdNegocioVida());
					caCompensacion.setAplicaComision(compensacionDTO.getAplicaComision() == 1);
					caCompensacion.setTopeMaximo(compensacionDTO.getTopeMaximo());
					caCompensacion.setFormaPagoBanca(compensacionDTO.getFormaPagoBanca());
					caCompensacion = this.crearCompesacion(caCompensacion, ramo);
					isNewRecord = true;
				}else{
					if(compensacionDTO.getFechaModificacionVida() == null){
						throw new RuntimeException("Es Necesario Capturar la Fecha de Modificacion para guardar");
					}
					caCompensacion.setFechaModificacionVida(compensacionDTO.getFechaModificacionVida());
					caCompensacion = this.actualizarCompensacion(caCompensacion);
					isNewRecord = false;
				}
				break;
				
			case BANCA:
				caCompensacion = this.findByRamoAndFieldAndContraprestacion(ramo.getValor(),  CaCompensacionDaoImpl.CONFIGURACIONBANCA_ID, compensacionDTO.getIdConfiguracionBanca(), compensacionDTO.isContraprestacion());
				if (caCompensacion == null) {
					caCompensacion = new CaCompensacion();
					caCompensacion.setContraprestacion(compensacionDTO.isContraprestacion());
					caCompensacion.setConfiguracionBancaId(compensacionDTO.getIdConfiguracionBanca());
					caCompensacion = this.crearCompesacion(caCompensacion, ramo);
					isNewRecord = true;
				}else{
					caCompensacion = this.actualizarCompensacion(caCompensacion);
					isNewRecord = false;
				}
				break;
	
		}
		
		if(caCompensacion != null){
			boolean isContraprestacion = caCompensacion.getContraprestacion();
			Long idTipoEntidad = isContraprestacion ? ConstantesCompensacionesAdicionales.PROVEEDOR : ConstantesCompensacionesAdicionales.AGENTE;
			
			
			/**
			 * Guarda las relaciones de la per


a
			 * 
			 */
			CaEntidadPersona entidadPersona;
			
			for(CompensacionesDTO compensacionVo: listCompensacionesDTO){
				
				entidadPersona = this.guardarEntidadPersonaCompensacion(caCompensacion, compensacionVo, idTipoEntidad);
				
				switch (ramo) {
				
					case AUTOS:	
						if(isContraprestacion){							
							this.guardarPorPrimaCompensacion(caCompensacion, compensacionVo, entidadPersona);
							this.guardarPorSiniestralidadCompensacion(caCompensacion, compensacionVo, entidadPersona);
						}else{
							this.guardarPorPrimaCompensacion(caCompensacion, compensacionVo, entidadPersona);
							this.guardarPorSiniestralidadCompensacion(caCompensacion, compensacionVo, entidadPersona);
							this.guardarPorDerechoDePoliza(caCompensacion, compensacionVo, entidadPersona);
						}
						
					break;
					
					case DANOS:	
						if(isContraprestacion){							
							this.guardarPorPrimaCompensacion(caCompensacion, compensacionVo, entidadPersona);
							this.guardarPorSiniestralidadCompensacion(caCompensacion, compensacionVo, entidadPersona);
							this.guardarPorUtilidad(caCompensacion, compensacionVo, entidadPersona);
						}else{
							this.guardarPorPrimaCompensacion(caCompensacion, compensacionVo, entidadPersona);
							this.guardarPorSiniestralidadCompensacion(caCompensacion, compensacionVo, entidadPersona);
							this.guardarPorDerechoDePoliza(caCompensacion, compensacionVo, entidadPersona);
						}		
					break;
					
					case VIDA:	
						if(isContraprestacion){							
							this.guardarPorPrimaCompensacion(caCompensacion, compensacionVo, entidadPersona);
							this.guardarPorSiniestralidadCompensacion(caCompensacion, compensacionVo, entidadPersona);
						}else{
							this.guardarPorPrimaCompensacion(caCompensacion, compensacionVo, entidadPersona);
							this.guardarPorSiniestralidadCompensacion(caCompensacion, compensacionVo, entidadPersona);
						}						
					break;
					
					case BANCA:	
						if(isContraprestacion){							
							this.guardarPorPrimaCompensacion(caCompensacion, compensacionVo, entidadPersona);
							this.guardarPorSiniestralidadCompensacion(caCompensacion, compensacionVo, entidadPersona);
							this.guardarPorCumplimientoDeMeta(caCompensacion, compensacionVo, entidadPersona);
						}					
					break;
					
				}
			}
			
			if(isNewRecord){
				this.guardarDefaultAutorizorizacion(caCompensacion, isContraprestacion);
				this.guardarBitacora(caCompensacion.getId(), ConstantesCompensacionesAdicionales.CONFIGURACION);
				this.caCorreosService.notificacionAutorizacionesCorreo(ConstantesCompensacionesAdicionales.CONFIGURACION, caCompensacion);
			}else{
				/**
				 * Si es el Ramo VIDA, las autorizaciones se regresan 
				 * al estatus de PENDIENTE y se notifica por correo
				 * a los usuarios para que autoricen la modificacion
				 */
				if(ramo.equals(RAMO.VIDA)){
					this.updateEstatusCompensacionVida(caCompensacion, ConstantesCompensacionesAdicionales.ESTATUS_PENDIENTE);
					this.guardarBitacora(caCompensacion.getId(), ConstantesCompensacionesAdicionales.MODIFICACION);
					this.caCorreosService.notificacionAutorizacionesCorreo(ConstantesCompensacionesAdicionales.MODIFICACION, caCompensacion);
					
				} else if(ramo.equals(RAMO.DANOS)){
					this.updateEstatusCompensacionDanios(caCompensacion, ConstantesCompensacionesAdicionales.ESTATUS_PENDIENTE);
					this.guardarBitacora(caCompensacion.getId(), ConstantesCompensacionesAdicionales.MODIFICACION);
					this.caCorreosService.notificacionAutorizacionesCorreo(ConstantesCompensacionesAdicionales.MODIFICACION, caCompensacion);
					
				} else if(ramo.equals(RAMO.AUTOS)){
					this.updateEstatusCompensacionAutos(caCompensacion, ConstantesCompensacionesAdicionales.ESTATUS_PENDIENTE);
					this.guardarBitacora(caCompensacion.getId(), ConstantesCompensacionesAdicionales.MODIFICACION);
					this.caCorreosService.notificacionAutorizacionesCorreo(ConstantesCompensacionesAdicionales.MODIFICACION, caCompensacion);
					LOG.debug("PROCESO CAMBIO ESTATUS A PENDIENTE EXITOSO AUTOS");
				}else {
					this.guardarBitacora(caCompensacion.getId(), ConstantesCompensacionesAdicionales.MODIFICACION);
				}
			}
			

		}
		LOG.debug("<< guardarCompensacion()");
	}
		
	/**
	 * 
	 * @param caCompensacion
	 * @param ramo
	 * @return
	 */
	private CaCompensacion crearCompesacion(CaCompensacion caCompensacion, RAMO ramo){
		LOG.debug(">> crearCompesacion()");
		try{
			caCompensacion.setCaRamo(ramocaService.findById(ramo.getValor()));
			caCompensacion.setCaEstatus(caEstatusService.findById(ConstantesCompensacionesAdicionales.ESTATUS_PENDIENTE));
			caCompensacion.setFechaCreacion(new Date());
			caCompensacion.setFechaModificacion(new Date());
			caCompensacion.setUsuario(usuarioService.getUsuarioActual().getNombre());
			caCompensacion.setBorradoLogico(ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
			this.save(caCompensacion);
		}catch(RuntimeException re){
			LOG.error("Informacion del Error", re);
			caCompensacion = null;
			throw re;
		}
		LOG.debug("<< crearCompesacion()");
		return caCompensacion;
	}
	
	/**
	 * 
	 * @param caCompensacion
	 * @return
	 */
	private CaCompensacion actualizarCompensacion(CaCompensacion caCompensacion){	
		LOG.debug(">> actualizarCompensacion()");
		try{
			caCompensacion = this.update(caCompensacion);
		}catch(RuntimeException re){
			LOG.error("Informacion del Error", re);
			caCompensacion = null;
			throw re;
		}		
		LOG.debug("<< actualizarCompensacion()");
		return caCompensacion;		
	}
	
	/**
	 * Guarda las Entidades de Personas que 
	 * Puedan estar relacionada a la compensacion 
	 * @param listCompensacionesDTO
	 * @param listNegocioAgente
	 * @param listEntidadPersonaca
	 */
	private CaEntidadPersona guardarEntidadPersonaCompensacion(CaCompensacion caCompensacion, CompensacionesDTO compensacionVo, Long idTipoEntidad) {
		LOG.debug(">> guardarEntidadPersonaCompensacion()");

		//Este Campo se esta utilizando como la clave de Agentes o Provedores
		Long idEntidadPersona = compensacionVo.getCaEntidadPersona().getId();
		
		/**
		 * Validacion
		 * Busqueda de Personas ligadas a la compensacion
		 */
		CaEntidadPersona caEntidadPersona = null;	
		
		if(caCompensacion.getCaRamo().getId().equals(RAMO.VIDA.getValor()) && !caCompensacion.getContraprestacion()){
			caEntidadPersona = entidadPersonacaService.findByCompensacionAndTipoEntidadAndClave(caCompensacion.getId(),compensacionVo.getDatosSeycos(), idTipoEntidad);
		}else{
			caEntidadPersona = entidadPersonacaService.findByCompensacionAndTipoEntidadAndClave(caCompensacion.getId(),idEntidadPersona, idTipoEntidad);
		}
		
		/**
		 * Si no existe se agregara ala compensacion
		 */
		if(caEntidadPersona == null){
			/**
			 * Tipo de Persona
			 */
			CaTipoEntidad tipoEntidadAgente = tipoEntidadcaService.findById(idTipoEntidad);
			caEntidadPersona  = new CaEntidadPersona();
			
			if(idTipoEntidad.equals(ConstantesCompensacionesAdicionales.AGENTE)){
				
				if(caCompensacion.getCaRamo().getId().equals(RAMO.VIDA.getValor()) && !caCompensacion.getContraprestacion()){
					DatosSeycos agenteSeycosId = compensacionVo.getDatosSeycos();
					LOG.debug(" --Guadardado AgenteSeycos ");
					List<DatosSeycos> listDatosSeycos = negocioVidaService.findAgenteSeycos(agenteSeycosId);					
					if(!listDatosSeycos.isEmpty()){
						LOG.debug(" --AgenteSeycosId Encontrado ");
						DatosSeycos agenteSeycos = listDatosSeycos.get(0);								
						caEntidadPersona.setIdAgente(new Integer(agenteSeycosId.getIdAgente()));
						caEntidadPersona.setIdEmpresa(new Integer(agenteSeycosId.getIdEmpresa()));
						caEntidadPersona.setFsit(agenteSeycosId.getDateFsit());
						caEntidadPersona.setNombres(agenteSeycos.getNombreCompleto());
					}
				
				}else{				
					Agente agenteActual = new Agente();
					agenteActual.setIdAgente(idEntidadPersona);
					agenteActual = agenteMidasService.loadByClave(agenteActual);
					caEntidadPersona.setAgente(agenteActual);
					
					String nombre = new StringBuilder(agenteActual.getPersona().getNombre()).append(" ").append(agenteActual.getPersona().getApellidoPaterno()).append(" ").append(agenteActual.getPersona().getApellidoMaterno()).toString();
					caEntidadPersona.setNombres(nombre);
				}
				
				caEntidadPersona.setConvenioEspecial(compensacionVo.getInclusionesGral() == ConstantesCompensacionesAdicionales.INCLUIRCONVENIOESPECIAL);
				caEntidadPersona.setCuadernoConcurso(compensacionVo.getInclusionesGral() == ConstantesCompensacionesAdicionales.INCLUIRCUADERNO);
				
			}else if(idTipoEntidad.equals(ConstantesCompensacionesAdicionales.PROVEEDOR)){
				
				List<Proveedor> listProveedores = catalogoCompensacionesService.listarProveedoresId(idEntidadPersona.toString());
				
				if(listProveedores != null && !listProveedores.isEmpty()){				
					caEntidadPersona.setIdProveedor(idEntidadPersona);	
					caEntidadPersona.setNombres(listProveedores.get(0).getNombre());
				}
				CaTipoContrato caTipoContrato = tipoContratoService.findById(compensacionVo.getCaTipoContrato().getId());
				caEntidadPersona.setCaTipoContrato(caTipoContrato);
			}				
			
			caEntidadPersona.setCaTipoEntidad(tipoEntidadAgente);
			caEntidadPersona.setCaCompensacion(caCompensacion);			
			caEntidadPersona.setFechaCreacion(new Date());
			caEntidadPersona.setFechaModificacion(new Date());
			caEntidadPersona.setUsuario(caCompensacion.getUsuario());
			caEntidadPersona.setBorradoLogico(false);
			entidadPersonacaService.save(caEntidadPersona);
		}else{
			caEntidadPersona.setFechaModificacion(new Date());
			caEntidadPersona.setUsuario(caCompensacion.getUsuario());
			entidadPersonacaService.update(caEntidadPersona);
		}
		LOG.debug("<< guardarEntidadPersonaCompensacion()");
		return caEntidadPersona;
	}
	
	/**
	 * Guarda la Prima de la Compensacion
	 * @param caCompensacion
	 * @param compensacionVo
	 * @param entidadPersona
	 */
	private void guardarPorPrimaCompensacion(CaCompensacion caCompensacion, CompensacionesDTO compensacionVo, CaEntidadPersona entidadPersona){
		LOG.debug(">> guardarPorPrimaCompensacion()");		
		/**
		 * Validacion existe Prima
		 */
		CaTipoCompensacion caTipoCompensacion = tipoCompensacioncaService.findById(new Long(ConstantesCompensacionesAdicionales.PORPRIMA));
		CaParametros parametrosGralescaPrima = parametrosGralescaService.findByTipocompeidCompeIdPersoId(caTipoCompensacion,caCompensacion,entidadPersona);
		
		boolean esPorPrima = caCompensacion.getContraprestacion() ? compensacionVo.isPorPrimaContra() : compensacionVo.isPorPrima();
		boolean isNewRecord = parametrosGralescaPrima == null;

		if (esPorPrima) {			
			
			if(isNewRecord){
				parametrosGralescaPrima = new CaParametros();
				parametrosGralescaPrima.setFechaCreacion(new Date());	
				parametrosGralescaPrima.setFechaModificacion(new Date());
				parametrosGralescaPrima.setBorradoLogico(ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
				parametrosGralescaPrima.setCaTipoCompensacion(caTipoCompensacion);
				parametrosGralescaPrima.setCaCompensacion(caCompensacion);				
			}else{
				parametrosGralescaPrima.setFechaModificacion(new Date());
			}
			
			parametrosGralescaPrima.setUsuario(caCompensacion.getUsuario());	
			parametrosGralescaPrima.setParametroActivo(esPorPrima);
			parametrosGralescaPrima.setCaEntidadPersona(entidadPersona);			
			
			if(caCompensacion.getContraprestacion()){
				parametrosGralescaPrima.setPorcentajePago(compensacionVo.getPorcePrimaContra());
				parametrosGralescaPrima.setMontoPago(compensacionVo.getMontoPrimaContra());
				parametrosGralescaPrima.setCaBaseCalculo(caBaseCalculoService.findById(compensacionVo.getBaseCalculocaPrimaContra().getId()));
				parametrosGralescaPrima.setCaTipoMoneda(caTipoMonedaService.findById(compensacionVo.getTipoMonedacaPrimaContra().getId())); 
				if (caCompensacion.getCaRamo().getId() == RAMO.AUTOS.getValor()){
				parametrosGralescaPrima.setCaTipoCondicionCalculo(caTipoCondicionCalculoService.findById(compensacionVo.getCondicionesCalcPrimaContra().getId()));
				}else if (caCompensacion.getCaRamo().getId() == RAMO.DANOS.getValor()){
					parametrosGralescaPrima.setCaTipoProvision(caTipoProvisionService.findById(compensacionVo.getTipoProvisioncaPrima().getId()));
				} else if (caCompensacion.getCaRamo().getId() == RAMO.VIDA.getValor()){
					parametrosGralescaPrima.setIva(compensacionVo.getIvaPrima() == 1);
					parametrosGralescaPrima.setComisionAgente(compensacionVo.getComisionAgente() == 1);
				}else if(caCompensacion.getCaRamo().getId() == RAMO.BANCA.getValor()){
					parametrosGralescaPrima.setBonoFijoVida(compensacionVo.getBonoFijoVida());
				}
			}else{				
				parametrosGralescaPrima.setPorcentajePago(compensacionVo.getPorcePrima());
				parametrosGralescaPrima.setMontoPago(compensacionVo.getMontoPrima());
				parametrosGralescaPrima.setPorcentajeAgenteProveedor(compensacionVo.getPorceAgentePrima());
				parametrosGralescaPrima.setPorcentajePromotor(compensacionVo.getPorcePromotorPrima());
				parametrosGralescaPrima.setCaBaseCalculo(caBaseCalculoService.findById(compensacionVo.getBaseCalculocaPrima().getId()));
				if (caCompensacion.getCaRamo().getId() == RAMO.AUTOS.getValor()){
				parametrosGralescaPrima.setCaTipoCondicionCalculo(caTipoCondicionCalculoService.findById(compensacionVo.getCondicionesCalcPrima().getId()));
				}else if (caCompensacion.getCaRamo().getId() == RAMO.DANOS.getValor()){
					parametrosGralescaPrima.setCaTipoProvision(caTipoProvisionService.findById(compensacionVo.getTipoProvisioncaPrima().getId()));
				} else if (caCompensacion.getCaRamo().getId() == RAMO.VIDA.getValor()){
					parametrosGralescaPrima.setIva(compensacionVo.getIvaPrima() == 1);
					parametrosGralescaPrima.setComisionAgente(compensacionVo.getComisionAgente() == 1);
				}
			}
							
			if(isNewRecord){
				parametrosGralescaService.save(parametrosGralescaPrima);
			}else{
				parametrosGralescaService.update(parametrosGralescaPrima);
			}			
			
			if (caCompensacion.getCaRamo().getId() == RAMO.AUTOS.getValor()){					
				Long tipoCalculoId = caCompensacion.getContraprestacion() ? compensacionVo.getCondicionesCalcPrimaContra().getId(): compensacionVo.getCondicionesCalcPrima().getId(); 
				if (tipoCalculoId != null && tipoCalculoId == ConstantesCompensacionesAdicionales.CONAJUSTECOMISIONPACTADA) {
					caLineaNegocioService.guardarLineasNegocio(parametrosGralescaPrima, compensacionVo.getLineasNegocio());
				}	
			}else if(caCompensacion.getCaRamo().getId() == RAMO.DANOS.getValor()){
				caSubramosDaniosService.guardarListadoSubRamosDanios(compensacionVo.getListSubRamosDaniosView(), compensacionVo.getCotizacionId(), null, parametrosGralescaPrima);
			}			
			
		}else if(!isNewRecord){
			parametrosGralescaPrima.setParametroActivo(esPorPrima);
			parametrosGralescaPrima.setUsuario(caCompensacion.getUsuario());
			parametrosGralescaPrima.setFechaModificacion(new Date());
			parametrosGralescaService.update(parametrosGralescaPrima);
		}
		LOG.debug("<< guardarPorPrimaCompensacion()");
	}
	
	/**
	 * Guarda la configuracion por Siniestralidad
	 * de la compensacion
	 * @param caCompensacion
	 * @param compensacionVo
	 * @param entidadPersona
	 */
	private void guardarPorSiniestralidadCompensacion(CaCompensacion caCompensacion, CompensacionesDTO compensacionVo, CaEntidadPersona entidadPersona){
		LOG.debug(">> guardarPorSiniestralidadCompensacion()");
				
		/**
		 * Validacion existe configuracion
		 */
		CaTipoCompensacion caTipoCompensacion = tipoCompensacioncaService.findById(new Long(ConstantesCompensacionesAdicionales.PORBAJASINIESTRALIDAD));
 		CaParametros parametrosGralescaBajaSiniestralidad = parametrosGralescaService.findByTipocompeidCompeIdPersoId(caTipoCompensacion,caCompensacion,entidadPersona);
 		
	 	boolean esPorBajaSini = caCompensacion.getContraprestacion() ? compensacionVo.isPorSiniestralidadContra() : compensacionVo.isPorSiniestralidad();
	 	boolean isNewRecord = parametrosGralescaBajaSiniestralidad == null;

	 	if (esPorBajaSini) {
	 		
 	 		if(isNewRecord){
 	 			parametrosGralescaBajaSiniestralidad = new CaParametros(); 	 			
 				parametrosGralescaBajaSiniestralidad.setFechaCreacion(new Date());
 				parametrosGralescaBajaSiniestralidad.setFechaModificacion(new Date());
 				parametrosGralescaBajaSiniestralidad.setBorradoLogico(ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
 				parametrosGralescaBajaSiniestralidad.setCaTipoCompensacion(caTipoCompensacion);
 				parametrosGralescaBajaSiniestralidad.setCaCompensacion(caCompensacion);
 	 		}else{
 	 			parametrosGralescaBajaSiniestralidad.setFechaModificacion(new Date());
 	 		} 	
 	 		
 	 		parametrosGralescaBajaSiniestralidad.setIva(compensacionVo.getIvaSiniestralidad() == 1);
 	 		parametrosGralescaBajaSiniestralidad.setParametroActivo(esPorBajaSini);
 	 		parametrosGralescaBajaSiniestralidad.setUsuario(caCompensacion.getUsuario());			
			parametrosGralescaBajaSiniestralidad.setCaEntidadPersona(entidadPersona);	
			
			if(caCompensacion.getContraprestacion()){
				parametrosGralescaBajaSiniestralidad.setPorcentajePago(compensacionVo.getPorceSiniestralidadContra());
				parametrosGralescaBajaSiniestralidad.setMontoPago(compensacionVo.getMontoSiniestralidadContra());
				parametrosGralescaBajaSiniestralidad.setCaBaseCalculo(caBaseCalculoService.findById(compensacionVo.getBaseCalculocaSiniestralidadContra().getId()));
				parametrosGralescaBajaSiniestralidad.setCaTipoMoneda(caTipoMonedaService.findById(compensacionVo.getTipoMonedacaSiniestralidadContra().getId()));					
			}else{
				parametrosGralescaBajaSiniestralidad.setPorcentajePago(compensacionVo.getPorceSiniestralidad());
				parametrosGralescaBajaSiniestralidad.setPorcentajeAgenteProveedor(compensacionVo.getPorceAgenteSiniestralidad());
				parametrosGralescaBajaSiniestralidad.setPorcentajePromotor(compensacionVo.getPorcePromotorSiniestralidad());
				parametrosGralescaBajaSiniestralidad.setCaBaseCalculo(caBaseCalculoService.findById(compensacionVo.getBaseCalculocaSiniestralidad().getId()));	
			}
			
			if(isNewRecord){
				parametrosGralescaService.save(parametrosGralescaBajaSiniestralidad);				
			}else{
				parametrosGralescaService.update(parametrosGralescaBajaSiniestralidad);
			}
			
			/**
			 * Niveles de Rangos para Baja Siniestralidad
			 */
			caRangosService.guardarRangos(parametrosGralescaBajaSiniestralidad, compensacionVo.getNivelesRangos());
			
	 	}else if(!isNewRecord){
			parametrosGralescaBajaSiniestralidad.setParametroActivo(esPorBajaSini);
			parametrosGralescaBajaSiniestralidad.setUsuario(caCompensacion.getUsuario());
			parametrosGralescaBajaSiniestralidad.setFechaModificacion(new Date());
			parametrosGralescaService.update(parametrosGralescaBajaSiniestralidad);
		}
	 	
 		LOG.debug("<< guardarPorSiniestralidadCompensacion()");
		
	}

	
	/**
	 * Guarda la configuracion Por Derecho de Poliza
	 * de la Compensacion
	 * @param caCompensacion
	 * @param compensacionVo
	 * @param entidadPersona
	 */
	private void guardarPorDerechoDePoliza(CaCompensacion caCompensacion, CompensacionesDTO compensacionVo, CaEntidadPersona entidadPersona){
		LOG.debug(">> guardarPorDerechoDePoliza()");
		
		/**
		 * Validacion existe configuracion
		 */
		CaTipoCompensacion caTipoCompensacion = tipoCompensacioncaService.findById(new Long(ConstantesCompensacionesAdicionales.DERECHOPOLIZA));
		CaParametros parametrosGralescaDerechoPoliza = parametrosGralescaService.findByTipocompeidCompeIdPersoId(caTipoCompensacion,caCompensacion,entidadPersona);
		
		boolean esPorDerechoPoliza = compensacionVo.isPorPoliza();
		boolean isNewRecord = parametrosGralescaDerechoPoliza == null;

		if (esPorDerechoPoliza) {
			
			if(isNewRecord){
				parametrosGralescaDerechoPoliza = new CaParametros();
				parametrosGralescaDerechoPoliza.setFechaCreacion(new Date());
				parametrosGralescaDerechoPoliza.setFechaModificacion(new Date());
				parametrosGralescaDerechoPoliza.setCaTipoCompensacion(caTipoCompensacion);
				parametrosGralescaDerechoPoliza.setCaCompensacion(caCompensacion);
				parametrosGralescaDerechoPoliza.setCaEntidadPersona(entidadPersona);
				parametrosGralescaDerechoPoliza.setBorradoLogico(ConstantesCompensacionesAdicionales.BORRADOLOGICONO);				
			}else{
				parametrosGralescaDerechoPoliza.setFechaModificacion(new Date());
			}			
			parametrosGralescaDerechoPoliza.setParametroActivo(esPorDerechoPoliza);
			parametrosGralescaDerechoPoliza.setUsuario(caCompensacion.getUsuario());			
			
			parametrosGralescaDerechoPoliza.setPorcentajePago(compensacionVo.getPorcePoliza());
			parametrosGralescaDerechoPoliza.setMontoPago(compensacionVo.getMontoPoliza());
			parametrosGralescaDerechoPoliza.setPorcentajeAgenteProveedor(compensacionVo.getPorceAgentePoliza());
			parametrosGralescaDerechoPoliza.setPorcentajePromotor(compensacionVo.getPorcePromotorPoliza());
			parametrosGralescaDerechoPoliza.setEmisionExterna(compensacionVo.getEmisionExterna());
			parametrosGralescaDerechoPoliza.setEmisionInterna(compensacionVo.getEmisionInterna());
			
			if(isNewRecord){
				parametrosGralescaService.save(parametrosGralescaDerechoPoliza);
			}else{
				parametrosGralescaService.update(parametrosGralescaDerechoPoliza);
			}
		
		}else if(!isNewRecord){
			parametrosGralescaDerechoPoliza.setParametroActivo(esPorDerechoPoliza);
			parametrosGralescaDerechoPoliza.setUsuario(caCompensacion.getUsuario());
			parametrosGralescaDerechoPoliza.setFechaModificacion(new Date());
			parametrosGralescaService.update(parametrosGralescaDerechoPoliza);
		}
		
		LOG.debug("<< guardarPorDerechoDePoliza()");
	}
	
	/**
	 * Guarda la configuracion Por Utilidad
	 * de la Compensacion
	 * @param caCompensacion
	 * @param compensacionVo
	 * @param entidadPersona
	 */
	private void guardarPorUtilidad(CaCompensacion caCompensacion, CompensacionesDTO compensacionVo, CaEntidadPersona entidadPersona){
		LOG.debug(">> guardarPorUtilidad()");
		/**
		 * Validacion existe configuracion
		 */
		CaTipoCompensacion caTipoCompensacion = tipoCompensacioncaService.findById(new Long(ConstantesCompensacionesAdicionales.PORUTILIDAD));
		CaParametros parametrosPorUtilidad = parametrosGralescaService.findByTipocompeidCompeIdPersoId(caTipoCompensacion,caCompensacion,entidadPersona);
		
		boolean esPorUtilidad = compensacionVo.isPorUtilidad();
		boolean isNewRecord = parametrosPorUtilidad == null;
		
		if (esPorUtilidad) {
			
			if(isNewRecord){
				parametrosPorUtilidad = new CaParametros();
				parametrosPorUtilidad.setFechaCreacion(new Date());
				parametrosPorUtilidad.setFechaModificacion(new Date());		
				parametrosPorUtilidad.setBorradoLogico(ConstantesCompensacionesAdicionales.BORRADOLOGICONO);				
				parametrosPorUtilidad.setCaTipoCompensacion(caTipoCompensacion);
				parametrosPorUtilidad.setCaCompensacion(caCompensacion);
			}else{
				parametrosPorUtilidad.setFechaModificacion(new Date());				
			}
			parametrosPorUtilidad.setParametroActivo(esPorUtilidad);
			parametrosPorUtilidad.setUsuario(caCompensacion.getUsuario());
		
			parametrosPorUtilidad.setCaEntidadPersona(entidadPersona);			
			parametrosPorUtilidad.setMontoUtilidades(compensacionVo.getMontoDeLasUtilidades());
			parametrosPorUtilidad.setPorcentajePago(compensacionVo.getPorcenUtilidadesUtilidad());
			parametrosPorUtilidad.setMontoPago(compensacionVo.getMontoUtilidadesUtilidad());		
			
			if(isNewRecord){
				parametrosGralescaService.save(parametrosPorUtilidad);
			}else{
				parametrosGralescaService.update(parametrosPorUtilidad);
			}
		
		}else if(!isNewRecord){
			parametrosPorUtilidad.setParametroActivo(esPorUtilidad);
			parametrosPorUtilidad.setUsuario(caCompensacion.getUsuario());
			parametrosPorUtilidad.setFechaModificacion(new Date());
			parametrosGralescaService.update(parametrosPorUtilidad);
		}
		
		LOG.debug("<< guardarPorUtilidad()");
	}
	
	/**
	 * Guarda la configuracion Por Cumplimiento de Meta
	 * @param caCompensacion
	 * @param compensacionVo
	 * @param entidadPersona
	 */
	private void guardarPorCumplimientoDeMeta(CaCompensacion caCompensacion, CompensacionesDTO compensacionVo, CaEntidadPersona entidadPersona){
		/**
		 * Validacion existe configuracion
		 */
		CaTipoCompensacion caTipoCompensacion = tipoCompensacioncaService.findById(new Long(ConstantesCompensacionesAdicionales.PORCUMPLIMIENTODEMETA));
		CaParametros parametrosPorCumplimientoMeta = parametrosGralescaService.findByTipocompeidCompeIdPersoId(caTipoCompensacion,caCompensacion,entidadPersona);
		
		boolean esPorCumplimientoDeMeta = compensacionVo.isPorCumplimientoDeMeta();
		boolean isNewRecord = parametrosPorCumplimientoMeta == null;
		
		if (esPorCumplimientoDeMeta) {
			
			if(isNewRecord){
				parametrosPorCumplimientoMeta = new CaParametros();
				parametrosPorCumplimientoMeta.setFechaCreacion(new Date());
				parametrosPorCumplimientoMeta.setFechaModificacion(new Date());
				parametrosPorCumplimientoMeta.setBorradoLogico(ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
				parametrosPorCumplimientoMeta.setCaTipoCompensacion(caTipoCompensacion);
				parametrosPorCumplimientoMeta.setCaCompensacion(caCompensacion);
			}else{
				parametrosPorCumplimientoMeta.setFechaModificacion(new Date());
			}
			parametrosPorCumplimientoMeta.setParametroActivo(esPorCumplimientoDeMeta);
			parametrosPorCumplimientoMeta.setUsuario(caCompensacion.getUsuario());
			parametrosPorCumplimientoMeta.setPresupuestoAnual(compensacionVo.getPresupuestoAnual());
			parametrosPorCumplimientoMeta.setCaEntidadPersona(entidadPersona);		
			parametrosPorCumplimientoMeta.setCaBaseCalculo(caBaseCalculoService.findById(compensacionVo.getBaseCalculoCumplimientoMeta().getId()));
			parametrosPorCumplimientoMeta.setCaTipoMoneda(caTipoMonedaService.findById(compensacionVo.getCaTipoMonedaCumplimientoMeta().getId()));

			if(isNewRecord){
				parametrosGralescaService.save(parametrosPorCumplimientoMeta);
			}else{
				parametrosGralescaService.update(parametrosPorCumplimientoMeta);
			}		
			/**
			 * Niveles de Rangos 
			 */
			caRangosService.guardarRangos(parametrosPorCumplimientoMeta, compensacionVo.getNivelesRangosCumplimientoMeta());				
		
		}else if(!isNewRecord){
			parametrosPorCumplimientoMeta.setParametroActivo(esPorCumplimientoDeMeta);
			parametrosPorCumplimientoMeta.setUsuario(caCompensacion.getUsuario());
			parametrosPorCumplimientoMeta.setFechaModificacion(new Date());
			parametrosGralescaService.update(parametrosPorCumplimientoMeta);
		}
		
	}
	
	/**
	 * Guarda por default el listado de autorizaciones
	 * @param caCompensacion
	 * @param contraprestacion
	 */
	private void guardarDefaultAutorizorizacion(CaCompensacion caCompensacion, boolean contraprestacion){
		LOG.debug(">> guardarDefaultAutorizorizacion()");
			
		CaEstatus caEstatus = caEstatusService.findById(ConstantesCompensacionesAdicionales.ESTATUS_PENDIENTE);
		
		if(contraprestacion){
			this.guardarAutorizacion(caCompensacion, ConstantesCompensacionesAdicionales.AUTORIZACION_DIRECTORTECNICO, caEstatus);
			this.guardarAutorizacion(caCompensacion, ConstantesCompensacionesAdicionales.AUTORIZACION_ADMONAGENTES, caEstatus);
			this.guardarAutorizacion(caCompensacion, ConstantesCompensacionesAdicionales.AUTORIZACION_JURIDICO, caEstatus);
		}else{		
			this.guardarAutorizacion(caCompensacion, ConstantesCompensacionesAdicionales.AUTORIZACION_DIRECTORTECNICO, caEstatus);
			this.guardarAutorizacion(caCompensacion, ConstantesCompensacionesAdicionales.AUTORIZACION_ADMONAGENTES, caEstatus);
		}
		LOG.debug("<< guardarDefaultAutorizorizacion()");
	}
	

	/**
	 * 
	 * @param compensacionId
	 * @param tipoAutorizacionId
	 */
	private void guardarBitacora(Long compensacionId, String movimiento){
		LOG.debug(">> guardarBitacora()");
		CaBitacora bitacora = new CaBitacora();
		bitacora.setUsuario(usuarioService.getUsuarioActual().getNombre());
		bitacora.setMovimiento(movimiento);
		bitacora.setFecha(new Date());
		bitacora.setCompensacionId(compensacionId);		
		bitacoraService.save(bitacora);
		LOG.debug("<< guardarBitacora()");
	}
	
	/**
	 * Provisiona la Compensacion Por Utilidad
	 * @param polizaId
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public int provisionarPorUtilidad(long idToPoliza, long entidadPersonaId, BigDecimal montoPago,BigDecimal montoTotal){
		LOG.debug(">> provisionarPorUtilidad()");
		int result = 0;
		
		try{
			result = calculoCompensacionesDao.provisionarPorUtilidadCompensacion(idToPoliza, entidadPersonaId, montoPago,montoTotal);	
		
		}catch (Exception e) {
			LOG.error("Informacion del Error", e);
			result = 1;
		}
		LOG.debug("<< provisionarPorUtilidad()");
		return result;
	}
	
	/**
	 * Solicita el pago por utilidad
	 * @param idToPoliza
	 * @param entidadPersonaId
	 * @param montoPago
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean solicitarPagoPorUtilidad(long idToPoliza, long entidadPersonaId, BigDecimal montoPago){
		LOG.debug(">> solicitarPagoPorUtilidad()");
		boolean result = true;
		try{
			int error = calculoCompensacionesDao.generarOrdenUtilidad(idToPoliza, entidadPersonaId, montoPago);		
			result = error == 0;
		}catch (Exception e) {
			LOG.error("Informacion del Error", e);
			result = false;
		}
		LOG.debug("<< solicitarPagoPorUtilidad()");
		return result;
	}
	
	/**
	 * 
	 * @param idRamo
	 * @param field
	 * @param valueField
	 * @return
	 */
	public List<CaCompensacion> listCompensaciones(Long idRamo,  String field, Object valueField){
		return compensAdiciocaDao.listCompensaciones(idRamo, field, valueField);
	}
	
	
	public boolean cargarExcel(File fileUpload, Long configuracionBancaId){
		LOG.debug(">> cargarExcel()");
		boolean cargar = true;
		try{		
			Workbook workbook =	WorkbookFactory.create(new FileInputStream(fileUpload));
						
			//Solo se lee la primera hoja del excel
			Sheet sheet = workbook.getSheetAt(0);
			//Numero maximo de columnas a leer
			int numCol = 14;
			//Numero maximo de rows a leer
			int numRow = 18;
			//Listado de Filas
			Iterator<Row> iteratorRow = sheet.iterator();		
			//La primera Fila queda omitida, son los headers de las columnas
			if(iteratorRow.hasNext()){
				iteratorRow.next();
			}
			
			List<CaBancaPresupuestoAnual> listPresupuesto = new ArrayList<CaBancaPresupuestoAnual>();
			List<CaBancaPresupuestoAnual> listPresupuestoRegistrado = this.caBancaPresupuestoAnualDao.findByProperty(CaBancaPresupuestoAnualDaoImpl.CA_CONFIGURACION_BANCA, configuracionBancaId);
			
			if(!listPresupuestoRegistrado.isEmpty()){
				this.caBancaPresupuestoAnualDao.deleteRecordsByProperty(CaBancaPresupuestoAnualDaoImpl.CA_CONFIGURACION_BANCA, configuracionBancaId);		        
			}
			
			Row row;
			Cell cellValorMonto;
			Cell cellValorRamo;
			Integer rowIndex = 0;		
			
			while(iteratorRow.hasNext()){
				row = iteratorRow.next();				
				int mes = 1;				
				for(int colIndex = 2; colIndex < numCol; colIndex++){											
					cellValorMonto = row.getCell(colIndex);
					cellValorRamo = row.getCell(0);
					if(cellValorMonto != null && cellValorRamo != null){
						listPresupuesto.add(new CaBancaPresupuestoAnual(configuracionBancaId, this.getRamoSeycosId(cellValorRamo), mes, this.getValorCelll(cellValorMonto)));						
					}
					mes++;
				}					
				rowIndex++;				
				if(rowIndex == numRow){
					break;
				}				
			}				
			
			LOG.debug("listPresupuesto: "+listPresupuesto.size());
			
			for(CaBancaPresupuestoAnual presupuesto: listPresupuesto){
				this.caBancaPresupuestoAnualDao.save(presupuesto);				
			}
			
		}catch(Exception e){
			LOG.error("Informacion del Error", e);
			fileUpload.delete();			
			cargar = false;			
		}finally{
			if(fileUpload != null){
				fileUpload.deleteOnExit();
			}			
		}
		LOG.debug("<< cargarExcel()");
		
		return cargar;
	}

	
	private String getRamoSeycosId(Cell cell){		
		String valor;
		switch (cell.getCellType()) {		
			case Cell.CELL_TYPE_NUMERIC:
					valor = String.valueOf(cell.getNumericCellValue());
					break;			
			case Cell.CELL_TYPE_STRING:
					valor = cell.getStringCellValue();
					break;
			default:
					valor = "";
					break;
		}		
		return valor;
	}
	
	private BigDecimal getValorCelll(Cell cell){		
		BigDecimal valor;		
		switch (cell.getCellType()) {		
			case Cell.CELL_TYPE_NUMERIC:
					valor = new BigDecimal(cell.getNumericCellValue());
					break;			
			case Cell.CELL_TYPE_STRING:
					valor = new BigDecimal(cell.getStringCellValue());
					break;
			default:
					valor = BigDecimal.ZERO;
					break;
		}		
		return valor;
	}
	
	/**
	 * 
	 * @param caCompensacion
	 * @param estatus
	 */
	private void updateEstatusCompensacionVida(CaCompensacion caCompensacion, Long estatus){		
		CaEstatus caEstatus = caEstatusService.findById(estatus);		
		List<CaAutorizacion> listAutorizaciones = this.caAutorizacionService.findByCompensacionid(caCompensacion.getId());		
		//Actualiza los estatus de las Autorizaciones
		for(CaAutorizacion caAutorizacion: listAutorizaciones){			
			caAutorizacion.setFechaModificacion(new Date());
			caAutorizacion.setUsuario(caCompensacion.getUsuario());
			caAutorizacion.setCaEstatus(caEstatus);
			this.caAutorizacionService.update(caAutorizacion);
		}
		//Actualiza el Estatus de la Compensacion
		caCompensacion.setCaEstatus(caEstatus);
		this.update(caCompensacion);
		
		List<CaCompensacion> listCompensaciones = this.listCompensaciones(RAMO.VIDA.getValor(), ConstantesCompensacionesAdicionales.NEGOCIOVIDA_ID, caCompensacion.getNegocioVidaId());
		
		for(CaCompensacion ca: listCompensaciones){
			ca.setAplicaComision(caCompensacion.getAplicaComision());
			ca.setFechaModificacionVida(caCompensacion.getFechaModificacionVida());
			this.update(ca);
		}
	}
	private void updateEstatusCompensacionDanios(CaCompensacion caCompensacion, Long estatus){	
		CaEstatus caEstatus = caEstatusService.findById(estatus);		
		List<CaAutorizacion> listAutorizaciones = this.caAutorizacionService.findByCompensacionid(caCompensacion.getId());		
		//Actualiza los estatus de las Autorizaciones
		for(CaAutorizacion caAutorizacion: listAutorizaciones){			
			caAutorizacion.setFechaModificacion(new Date());
			caAutorizacion.setUsuario(caCompensacion.getUsuario());
			caAutorizacion.setCaEstatus(caEstatus);
			this.caAutorizacionService.update(caAutorizacion);
		}
		//Actualiza el Estatus de la Compensacion
		caCompensacion.setCaEstatus(caEstatus);
		this.update(caCompensacion);
		List<CaCompensacion> listCompensaciones = this.listCompensaciones(RAMO.DANOS.getValor(), ConstantesCompensacionesAdicionales.COTIZACION_ID, caCompensacion.getCotizacionId());
		for(CaCompensacion ca: listCompensaciones){
			ca.setIdReciboModificacion(caCompensacion.getIdReciboModificacion());
			this.update(ca);
		}
    }
	private void updateEstatusCompensacionAutos(CaCompensacion caCompensacion, Long estatus){	
		CaEstatus caEstatus = caEstatusService.findById(estatus);		
		List<CaAutorizacion> listAutorizaciones = this.caAutorizacionService.findByCompensacionid(caCompensacion.getId());		
		//Actualiza los estatus de las Autorizaciones
		for(CaAutorizacion caAutorizacion: listAutorizaciones){			
			caAutorizacion.setFechaModificacion(new Date());
			caAutorizacion.setUsuario(caCompensacion.getUsuario());
			caAutorizacion.setCaEstatus(caEstatus);
			this.caAutorizacionService.update(caAutorizacion);
		}
		//Actualiza el Estatus de la Compensacion
		caCompensacion.setCaEstatus(caEstatus);
		this.update(caCompensacion);
		List<CaCompensacion> listCompensaciones = this.listCompensaciones(RAMO.AUTOS.getValor(), ConstantesCompensacionesAdicionales.COTIZACION_ID, caCompensacion.getCotizacionId());
		for(CaCompensacion ca: listCompensaciones){
			ca.setIdReciboModificacion(caCompensacion.getIdReciboModificacion());
			this.update(ca);
		}
    }

	private void modificacionConfiguracionVida(CaCompensacion caCompensacion){
		LOG.debug(">> modificacionConfiguracionVida()");		
		StoredProcedureHelper storedHelper = null;		
		try {			
			storedHelper = new StoredProcedureHelper(PROCEDURE_MODIFICACION_VIDA, StoredProcedureHelper.DATASOURCE_MIDAS);			
			storedHelper.estableceParametro("pFechaVigencia", caCompensacion.getFechaModificacionVida());
			storedHelper.estableceParametro("pIdCompensacion", caCompensacion.getId());
			int res = storedHelper.ejecutaActualizar();				
			LOG.debug("Ejecucion: " + PROCEDURE_MODIFICACION_VIDA + ", respuesta: " + res);		
			if(res == 0){
				throw new RuntimeException("Ocurrio un error al ejecutar el procedimiento:\n"+PROCEDURE_MODIFICACION_VIDA);
			}
		} catch (RuntimeException e) {			
			LOG.error("Informacion del Error", e);
			throw e;
		} catch (Exception e) {
			throw new RuntimeException("Ocurrio un error al ejecutar el procedimiento:\n"+PROCEDURE_MODIFICACION_VIDA);
		}
		LOG.debug("<< modificacionConfiguracionVida()");		
	}
	
	public List<CompensacionesDTO> getlistRecibosPendientesPagar(Long idPoliza){

		return compensAdiciocaDao.getlistRecibosPendientesPagar(idPoliza);	
	}
	
	@Override
	public List<ProductoVidaDTO> getProductosVida() {
		return compensAdiciocaDao.getProductosVida();
	}
	
}
