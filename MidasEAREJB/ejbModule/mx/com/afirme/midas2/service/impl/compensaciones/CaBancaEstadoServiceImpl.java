/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaBancaEstadoDao;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaBancaEstadoDaoImpl;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaEstado;
import mx.com.afirme.midas2.service.compensaciones.CaBancaEstadoService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless

public class CaBancaEstadoServiceImpl  implements CaBancaEstadoService {
	
	@EJB
	private CaBancaEstadoDao caBancaEstado;
	private static final Logger LOG = LoggerFactory.getLogger(CaBancaEstadoDaoImpl.class);

    public void save(CaBancaEstado entity) {   
	    try {
	    	LOG.info(">> save()");
	    	caBancaEstado.save(entity);	  
	    	LOG.info("<< save()");
	    } catch (RuntimeException re) {	 
	    	LOG.error("Información del Error", re);
	        throw re;
	    }
    }
    

    public void delete(CaBancaEstado entity) { 
    	try {
    		LOG.info(">> delete()");
    		caBancaEstado.delete(entity);
    		LOG.info("<< delete()");
    	} catch (RuntimeException re) {
    		LOG.error("Información del Error", re);
    		throw re;
    	}
    }
    

    public CaBancaEstado update(CaBancaEstado entity) {   
        try {
        	LOG.info(">> update()");
	        CaBancaEstado result = caBancaEstado.update(entity);
	        LOG.info("<< update()");
	        return result;
	    } catch (RuntimeException re) {
	    	LOG.error("Información del Error", re);
            throw re;
	    }
    }
    
    public CaBancaEstado findById( Long id) {
    	LOG.info(">> findById()");
	    try {
            CaBancaEstado instance = caBancaEstado.findById(id);
           	LOG.info("<< findById()");
            return instance;
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
        }
        return null;
    }   


    public List<CaBancaEstado> findByProperty(String propertyName, final Object value) {    	
		try {
			return caBancaEstado.findByProperty(propertyName, value);
		} catch (RuntimeException re) {
			
			return null;
		}
	}			

	
	public List<CaBancaEstado> findAll() {	
		try {
			return caBancaEstado.findAll();
		} catch (RuntimeException re) {
			return null;
		}
	}
	
}