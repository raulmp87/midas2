package mx.com.afirme.midas2.service.impl.bitemporal.suscripcion.cotizacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.Cotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.DescuentoPorVolumenAuto;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.CotizacionBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class CotizacionBitemporalServiceImpl implements
		CotizacionBitemporalService {

	@EJB
	private EntidadService entidadService;

	@Override
	public BitemporalCotizacion getBitemporalCotizacion(BigDecimal numeroCotizacion) {
		return this.getBitemporalCotizacion(numeroCotizacion, TimeUtils.now());
	}

	@Override
	public BitemporalCotizacion getBitemporalCotizacion(BigDecimal numeroCotizacion,
			DateTime validoEn) {

		List<CotizacionContinuity> continuityList = entidadService
				.findByProperty(CotizacionContinuity.class, "numero",
						numeroCotizacion);

		if (continuityList != null && continuityList.size() > 0) {
			CotizacionContinuity continuity = continuityList.get(0);
			if(validoEn != null){
				BitemporalCotizacion bitemporal = continuity.getCotizaciones().get(validoEn);
				if(bitemporal == null){
					bitemporal = continuity.getCotizaciones().getInProcess();
				}
				return bitemporal;
			}else{
				return continuity.getCotizaciones().getInProcess();
			}			
		}
		return null;
	}

	@Override
	public Cotizacion getBitemporalCotizacion(Long continuityId,
			DateTime validoEn) {
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("continuity.id", continuityId);
		CotizacionContinuity continuity= entidadService.findById(CotizacionContinuity.class, continuityId);
		if (continuity != null) {
			return continuity.getCotizaciones().on(validoEn);
		}
		return null;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Override
	public Double getDescuentoPorVolumenaNivelPoliza(BitemporalCotizacion cotizacion,
			DateTime validoEn){		
		
		List<BitemporalInciso> listaIncisos = new ArrayList<BitemporalInciso>(cotizacion.getContinuity().getIncisoContinuities().getInProcess(validoEn));		
		
		List<DescuentoPorVolumenAuto> descuentoPorVolumenAutoList =  entidadService.findAll(DescuentoPorVolumenAuto.class);	
		
		Double descuento = new Double(0.0);
		if (listaIncisos != null && listaIncisos.size()>0){
			for(DescuentoPorVolumenAuto item : descuentoPorVolumenAutoList){
				if (listaIncisos.size() > item.getMinTotalSubsections().intValue() && listaIncisos.size() < item.getMaxTotalSubsections().intValue()){
					descuento = item.getDefaultDiscountVolumen();
					break;
				}
			}
		}
		return descuento;
	}

}
