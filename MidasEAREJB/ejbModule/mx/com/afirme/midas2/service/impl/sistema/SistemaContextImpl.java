package mx.com.afirme.midas2.service.impl.sistema;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.namespace.QName;

import org.apache.log4j.Logger;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.sistema.entorno.EntornoService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;


@Stateless
public class SistemaContextImpl implements SistemaContext {

	private static final Logger log = Logger.getLogger(SistemaContextImpl.class);
	private EntornoService entornoService;
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	private ParametroGlobalService parametroGlobalService;

	@EJB
	public void setParametroGeneralFacade(
			ParametroGeneralFacadeRemote parametroGeneralFacade) {
		this.parametroGeneralFacade = parametroGeneralFacade;
	}

	@EJB
	public void setEntornoService(EntornoService entornoService) {
		this.entornoService = entornoService;
	}

	@EJB
	public void setParametroGlobalService(
			ParametroGlobalService parametroGlobalService) {
		this.parametroGlobalService = parametroGlobalService;
	}

	public SistemaContextImpl() {

	}

	@Override
	public String getUploadFolder() {
		if (System.getProperty("os.name").toLowerCase().indexOf("windows") != -1) {
			return entornoService.getVariable("midas.sistema.uploadFolder",
					"c:/MidasWeb/ArchivosAnexos/");
		} else {
			return entornoService.getVariable("midas.sistema.linux.uploadFolder",
					"/apps/profile1/MIDASPDF/");
		}
	}

	@Override
	public String getSmtpHostName() {
		return entornoService.getVariable("email.smtp.host.name", "mail.cdsi.com.mx");
	}

	@Override
	public boolean getSeguridadActivada() {
		return Boolean.parseBoolean(entornoService.getVariable("midas.seguridad.activada","true"));
		//ACTIVA_ASM 
		//return Boolean.parseBoolean(entornoService.getVariable("midas.seguridad.activada","true"));
	}

	@Override
	public boolean getAsmActivo() {
		return Boolean.valueOf(entornoService.getVariable("midas.asm.activo","true"));
		//ACTIVA_ASM 
		//return Boolean.valueOf(entornoService.getVariable("midas.asm.activo","true"));
	}


	@Override
	public String getAsmWscContext() {
		return getAsmBaseUrl();
	}

	@Override
	public String getAsmBaseUrl() {
		if (getBaseUrl( "midas.ws.asm.baseUrl") != null)
			return entornoService.getVariable("midas.ws.asm.baseUrl",   getBaseUrl( "midas.ws.asm.baseUrl"));
		else
			return entornoService.getVariable("midas.ws.asm.baseUrl",   "http://dev3.afirme.com.mx/ASMWeb");
			
	}

	private String getBaseUrl(String prop) {
		String propPath;
		String baseUrl = "";
		if (System.getProperty("os.name").toLowerCase().indexOf("windows") != -1) {
			propPath =   "c:/MidasWeb/variables-entorno-midas2.properties";
		} else {
			propPath =  "/apps/profile1/variables-entorno-midas2.properties";
		}
		
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(propPath));
			baseUrl = p.getProperty(prop);
		} catch (Exception e) {
			log.info("No se encontro el archivo de las propiedades");
		}

		if (baseUrl != null && !baseUrl.isEmpty() )
			return  baseUrl ;
		else
			return  null;
	}
	
	@Override
	public String getAsmLogin() {
		return getAsmBaseUrl() + "/security/showLogin.do";
	}

	@Override
	public String getUsuarioAccesoMidas() {
		return "usuarioAccesoMIDAS";
	}

	@Override
	public String getUsuarioAccesoPortal() {
		return "PortalUserLogin";
	}

	@Override
	public String getMidasAppId() {
		return entornoService.getVariable("midas.appid","5");
	}

	@Override
	public String getMidasLogin() {
		String midasUrl = getBaseUrl("midas.ws.midas2.baseUrl");
		if( getBaseUrl("midas.ws.midas2.baseUrl") != null)
			return entornoService.getVariable("midas.login", getBaseUrl("midas.ws.midas2.baseUrl") + "/sistema/inicio.do");
		else
			return entornoService.getVariable("midas.login", "http://dev.afirme.com.mx/MidasWeb/sistema/inicio.do");
		//ACTIVA_ASM 
		//return entornoService.getVariable("midas.login","http://localhost:9083/MidasWeb/sistema/inicio.do");
	}

	@Override
	public String getAccionPaginado() {
		return entornoService.getVariable("midas.sistema.accionpaginado","listarFiltradoPaginado.do");
	}

	@Override
	public String getListaPaginada() {
		return "listaPaginada";
	}

	@Override
	public String getAccesoDenegado() {
		return "/MidasWeb/accesoDenegado.jsp";
	}

	@Override
	public String getUrlInicio() {
		return "/MidasWeb/sistema/inicio.do";
	}

	@Override
	public String getUrlError() {
		return entornoService.getVariable("midas.login.error","http://localhost:9082/MidasWeb/login.jsp");
	}

	@Override
	public String getUsuarioSistema() {
		return "SISTEMA";
	}

	@Override
	public String getUrlLoginSinSeguridad() {
		return "/MidasWeb/loginB.jsp";
	}

	@Override
	public String getUrlCambioPassword() {
		return "/MidasWeb/cambiaPassword.jsp";
	}

	@Override
	public String getArchivoMenuMidas() {
		return "MenuMIDAS.xml";
	}

	@Override
	public String getArchivoRecursos() {
		return "mx.com.afirme.midas.RecursoDeMensajes";
	}

	@Override
	public String getArchivoRecursosFront() {
		return "globalmessages";
	}

	@Override
	public String getArchivoRecursosBack() {
		return "mx.com.afirme.midas.danios.reportes.comun.Reportes";
	}

	@Override
	public String getRolAdminProductos() {
		return "Rol_Admin_Productos";
	}

	@Override
	public String getRolAgente() {
		return "Rol_Agente";
	}

	@Override
	public String getRolAgenteExterno() {
		return "Rol_Agente_Ext";
	}

	@Override
	public String getRolAgenteEspecial() {
		return "Rol_Agente_Especial";
	}

	@Override
	public String getRolAsignadorOt() {
		return "Rol_Asignador_Ot";
	}

	@Override
	public String getRolAsignadorSol() {
		return "Rol_Asignador_Sol";
	}

	@Override
	public String getRolCoordinadorEmi() {
		return "Rol_Coordinador_Emi";
	}

	@Override
	public String getRolDirectorTecnico() {
		return "Rol_Director_Tecnico";
	}

	@Override
	public String getRolEmisor() {
		return "Rol_Emisor";
	}

	@Override
	public String getRolMesaControl() {
		return "Rol_Mesa_Control";
	}

	@Override
	public String getRolSupervisorSuscriptor() {
		return "Rol_Superv_Suscrptor";
	}

	@Override
	public String getRolSuscriptorOt() {
		return "Rol_Suscriptor_Ot";
	}

	@Override
	public String getRolSuscriptorCot() {
		return "Rol_Suscriptor_Cot";
	}

	@Override
	public String getRolReporteDanios() {
		return "Rol_Reportes_Danios";
	}

	@Override
	public String getRolConsultaDanios() {
		return "Rol_Consulta_Danios";
	}

	@Override
	public String getRolSuscriptorExt() {
		return "Rol_Suscriptor_Ext";
	}

	@Override
	public String getRolCabinero() {
		return "Rol_Cabinero";
	}

	@Override
	public String getRolCoordinadorSiniestros() {
		return "Rol_Coord_Siniestros";
	}

	@Override
	public String getRolCoordinadorSiniestrosFacultativo() {
		return "Rol_Coord_Sin_Facult";
	}

	@Override
	public String getRolGerenteSiniestros() {
		return "Rol_Gerente_Sin";
	}

	@Override
	public String getRolGerenteSiniestrosFacultativo() {
		return "Rol_Grnte_Sin_Facult";
	}

	@Override
	public String getRolAnalistaAdministrativo() {
		return "Rol_Analista_Admin";
	}

	@Override
	public String getRolAnalistaAdministrativoFacultativo() {
		return "Rol_Anlist_Adm_Facul";
	}

	@Override
	public String getRolDirectorDeOperaciones() {
		return "Rol_Dir_De_Operac";
	}

	@Override
	public String getRolAjustador() {
		return "Rol_Ajustador";
	}

	@Override
	public String getRolReportesSiniestros() {
		return "Rol_Reportes_Sin";
	}

	@Override
	public String getRolConsultaSiniestros() {
		return "Rol_Consulta_Sin";
	}

	@Override
	public String getRolOpReaseguroAutomatico() {
		return "Rol_Op_Reaseguro_Aut";
	}

	@Override
	public String getRolOpAsistenteSubdirectorReaseguro() {
		return "Rol_Op_Ast_Sbdir_Rea";
	}

	@Override
	public String getRolSubdirectorReaseguro() {
		return "Rol_Subdirector_Rea";
	}

	@Override
	public String getRolDirectorReaseguro() {
		return "Rol_Director_Rea";
	}

	@Override
	public String getRolGerenteReaseguro() {
		return "Rol_Gerente_Rea";
	}

	@Override
	public String getRolOpReaseguroFacultativo() {
		return "Rol_Op_Rea_Facult";
	}

	@Override
	public String getRolActuario() {
		return "Rol_Actuario";
	}

	@Override
	public String getRolOpPagosCobrosReaseguradores() {
		return "Rol_Op_Pagos_Cob_Rea";
	}

	@Override
	public String getRolCoordinadorAdministrativoReaseguro() {
		return "Rol_Coord_Adm_Rea";
	}

	@Override
	public String getRolReportesReaseguro() {
		return "Rol_Reportes_Rea";
	}

	@Override
	public String getRolConsultaReaseguro() {
		return "Rol_Consulta_Rea";
	}

	@Override
	public String getRolMailReaseguro() {
		return "Rol_Mail_Rea";
	}

	@Override
	public String getRolEspAdministradorColonias() {
		return "Rol_Esp_Adm_Colonias";
	}

	@Override
	public String getExitoso() {
		return "exitoso";
	}

	@Override
	public String getNoExitoso() {
		return "noExitoso";
	}

	@Override
	public String getNoDisponible() {
		return "noDisponible";
	}

	@Override
	public String getAlterno() {
		return "alterno";
	}

	@Override
	public String getAlternoPortal() {
		return "alternoPortal";
	}

	@Override
	public String getNotificationChannel() {
		return "/notification/";
	}

	@Override
	public String getRolAdministradorAutos(){
		return "Rol_M2_Administrador";
	}

	@Override
	public String getRolGerenteTecnicoAutos(){
		return "Rol_M2_Gerente_Tecnico";
	}

	@Override
	public String getRolCoordinadorAutos() {
		return "Rol_M2_Coordinador_Suscripcion";
	}

	@Override
	public String getRolSuscriptorAutos() {
		return "Rol_M2_Suscriptor";
	}

	@Override
	public String getRolMesaControlAutos() {
		return "Rol_M2_Mesa_Control";
	}

	@Override
	public String getRolPromotorAutos(){
		return "Rol_M2_Promotor";
	}

	@Override
	public String getRolAgenteAutos() {
		return "Rol_M2_Agente";
	}

	@Override
	public String getRolUsuarioWebAutos(){
		return "Rol_M2_Usuario_Web";
	}

	@Override
	public String getRolSucursalEjecutivo(){
		return "Rol_M2_Sucursales_Ejecutivos";
	}

	@Override
	public String getRolDirectorJuridico()
	{
		return "Rol_M2_Director_Juridico";		
	}

	@Override
	public String getRolAnalistaJuridico()
	{
		return "Rol_M2_Analista_Juridico";
	}

	@Override
	public String getRolGerenteOperacionIndemnizacionesAutos()
	{
		return "Rol_M2_Gerente_Operaciones_Indemnizacion";		
	}

	@Override
	public String getRolCoordinadorOperaciones()
	{
		return "Rol_M2_Coordinador_Operaciones";		
	}


	@Override
	public String getRolDirectorTecnicoAutos(){
		return "Rol_M2_Director_Tecnico_Autos";
	}

	@Override
	public String getRolGerenteContabilidad(){
		return "Rol_M2_Gerente_Contabilidad";
	}

	public String getRolSubdirectorSiniestrosAuto(){
		return "Rol_M2_SubDirector_Siniestros_Autos";
	}

	@Override
	public String getRolAdministradorAgentes() {
		return "Rol_M2_AdministradorAgente";
	}

	@Override
	public String getRolCoordinadorAgentes() {
		return "Rol_M2_CoordinadorAgente";
	}

	@Override
	public String getExpresionCronCancelacionEndososAuto() {
		return entornoService.getVariable("midas.sistema.tareas.endoso.cancelendosoautomatica.expresioncron","0 0 5 * * ? 2011");
	}

	@Override
	public String getEnvioProovedorFTP(){
		return entornoService.getVariable("midas.sistema.envioProveedor.sFTP","172.30.4.36");
	}

	@Override
	public int getEnvioProovedorPort(){
		return Integer.parseInt(entornoService.getVariable("midas.sistema.envioProveedor.port","22"));
	}

	@Override
	public String getEnvioProovedorUser(){
		return entornoService.getVariable("midas.sistema.envioProveedor.sUser","ftpmidas");
	}

	@Override
	public String getEnvioProovedorPassword(){
		return entornoService.getVariable("midas.sistema.envioProveedor.sPassword","S3gur0$2013");
	}

	@Override
	public String getEnvioProovedorDirectory(){
		return entornoService.getVariable("midas.sistema.envioProveedor.directory","/home/sftp/sftp_midas");
	}

	@Override
	public String getExpedienteFortimaxEndPointPath(){
		return entornoService.getVariable("midas.fortimax.expedientesWSDL.path","mx.com.afirme.midas2.wsClient.fortimax.expedientes.ExpedienteService.wsdl");
	}

	@Override
	public String getExpedienteFortimaxEndPointPathWSDL(){
		return entornoService.getVariable("midas.fortimax.expedientesWSDL.pathWSDL","http://fortimax5.afirme.com.mx/seguros5/services/ExpedienteService?wsdl");
	}

	@Override
	public String getDocumentoFortimaxEndPointPath() {
		return entornoService.getVariable("midas.fortimax.documentosWSDL.path","mx.com.afirme.midas2.wsClient.fortimax.documentos.DocumentoService.wsdl");
	}

	@Override
	public String getDocumentoFortimaxEndPointPathWSDL() {
		return entornoService.getVariable("midas.fortimax.documentosWSDL.pathWSDL","http://fortimax5.afirme.com.mx/seguros5/services/DocumentoService?wsdl");
	}

	@Override
	public String getLinkFortimaxEndPointPathWSDL() {
		return entornoService.getVariable("midas.fortimax.ligaIfimaxWSDL.pathWSDL","http://fortimax5.afirme.com.mx/seguros5/services/LinkService?wsdl");
	}

	@Override
	public String getCarpetaRaizFortimax() {
		return entornoService.getVariable("midas.fortimax.agentes.carpetaRaiz","AGENTES SEGUROS AFIRME");
	}

	@Override
	public String getTituloApFortimax() {
		return entornoService.getVariable("midas.fortimax.agentes.tituloAplicacion","AGENTES");
	}

	@Override
	public String getCarpetaRaizClientes() {
		return entornoService.getVariable("midas.fortimax.agentes.carpetaRaizClientes","CLIENTES");		
	}

	@Override
	public String getEibsHost() {
		return entornoService.getVariable("eibs.config.host", "172.20.1.196");	
	}

	@Override
	public String getEibsPort() {
		return entornoService.getVariable("eibs.config.port", "47800");
	}

	@Override
	public String getEibsTimeout() {
		return entornoService.getVariable("eibs.config.timeout", "120000");
	}

	@Override
	public String getPrintReportEndpoint(){
		return entornoService.getVariable("webservice.client.printreport.endpoint", "http://localhost:9080/InsuranceWebServices/services/PrintReport");
	}

	@Override
	public String getRootDirFiles(){
		return entornoService.getVariable("webservice.client.printreport.rootDir", "/apps/profile3/");
	}

	@Override
	public String getEibsServiceURL(){
		return entornoService.getVariable("webservice.client.eibs.services.endpoint", "http://172.20.1.19:47800/eIbsWebServiceWar/services/");
	}

	@Override
	public String uploadFileFortimaxEndPointPathWSDL() {		
		return entornoService.getVariable("midas.fortimax.uploadFileWSDL.pathWSDL","http://fortimax5.afirme.com.mx/seguros5/services/DocumentoService?wsdl");		
	}

	@Override
	public String getDirectorioImagenes() {
		return entornoService.getVariable("midas.directorio.imagenes","http://localhost:9082/MidasWeb/img/");//NO SUBIR cambio local
	}

	@Override
	public String getDirectorioImagenesParametroGlobal() {

		String directorio = parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS , 
				ParametroGlobalService.URL_DIRECTORIO_IMAGENES_IP);

		if(directorio == null || directorio.isEmpty())
		{
			directorio = this.getDirectorioImagenes(); //Default
		}

		return directorio;
	}

	@Override
	public String getAmbienteSistema() {
		return entornoService.getVariable("midas.sistema.ambiente","DES");
	}

	@Override
	public URL getMultiasistenciaSendAlertPortWsdlUrl() {
		String multiAsistenciaSendAlertPortWsdl = entornoService.getVariable("webservice.client.multiAsistenciaSendAlertPortWsdlUrl","http://201.155.175.187:9080/SOSMobil/SendAlertPort?wsdl");
		try {
			return new URL(multiAsistenciaSendAlertPortWsdl);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}
	@Override
	public URL getAxosnetValidadorSATWsdlUrl(){
		String axosnetValidadorSATWsdl = entornoService.getVariable("webservice.client.axosnetValidadorSATWsdlUrl","http://172.30.4.172:30020/WSValidadorSAT/WSValidacionesSAT");
		try {
			return new URL(axosnetValidadorSATWsdl);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public QName getAxosnetValidadorSATQName(){
		String axosnetValidadorSATNameSpace = entornoService.getVariable("webservice.client.axosnetValidadorSATNameSpace","http://sat.ws.axosnet.com/");
		String axosnetValidadorSATName = entornoService.getVariable("webservice.client.axosnetValidadorSATName","WSValidacionesSAT");
		try{
			return new QName(axosnetValidadorSATNameSpace, axosnetValidadorSATName);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public String getAjustadorMovilSenderKey() {
		return entornoService.getVariable("midas.siniestroMovil.senderKey","AIzaSyCX1IgHeVCIKvEzKt0BbbxJojQrqxhvfXM");
	}

	@Override
	public String getEnlaceMovilSenderKey() {
		return entornoService.getVariable("midas.enlaceMovil.senderKey", "AIzaSyCMAvoQXJY0lmFJyyNwgiEAR4-W4jzR2EA");
	}

	@Override
	public String getClienteMovilSenderKey() {
		return entornoService.getVariable("midas.clienteMovil.senderKey", "AIzaSyCkBiQtNipQH7QteDo8-lPmzKhIYvd91mY");
	}

	@Override
	public String getFortimaxV2BaseEndpointUrl() {
		ParametroGeneralId id = new ParametroGeneralId(new BigDecimal("26"), new BigDecimal("26001"));
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getFortimaxV2User() {
		ParametroGeneralId id = new ParametroGeneralId(new BigDecimal("26"), new BigDecimal("26003"));
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getFortimaxV2Password() {
		ParametroGeneralId id = new ParametroGeneralId(new BigDecimal("26"), new BigDecimal("26002"));
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getCesviIndividualEndpoint(){

		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERAL, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_CESVI_ENDPOINT_IND);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro!=null?parametro.getValor():null;		
	}

	@Override
	public URL getCesviFlotillasEndpoint(){
		String cesviEndpoint = entornoService.getVariable("webservice.client.cesviFlotillasEndpoint","http://172.30.5.32:30014/vinplus/ws_lotes/ws_lotes.php");
		try {
			return new URL(cesviEndpoint);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public URL getCesviClavesEndpoint(){
		String cesviEndpoint = entornoService.getVariable("webservice.client.cesviClavesEndpoint","http://172.30.5.32:30014/vinplus/ws_cve_afirme/ws_claves.php");
		try {
			return new URL(cesviEndpoint);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String getTrakingUser() {
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_TRACKING, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_TRACKING_USER);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getTrakingPassword() {
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_TRACKING, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_TRAKING_PASSWORD);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getTrakingIdSuscripcion() {
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_TRACKING, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_TRACKING_ID);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getTrackingEndPoint(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_TRACKING, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_TRACKING_ENDPOINT);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getPasswordCertificadoAPNs(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_ENLACE_APNS, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_ENLACE_APNS_PW_CERTIFICADO);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getNombreCertificadoAPNs(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_ENLACE_APNS, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_ENLACE_APNS_NOMBRE_CERTIFICADO);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getApnsEnvironment(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_ENLACE_APNS, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_ENLACE_APNS_ENVIRONMENT);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getLongitudRegistrationIdIOS(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_ENLACE_APNS, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_ENLACE_APNS_LONGITUD_REGISTRATIONID_IOS);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getIdRolAgente(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_REPORTE_AGENTES, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_REPORTE_AGENTES_ID_ROL_AGENTE);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getIdRolPromotor(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_REPORTE_AGENTES, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_REPORTE_AGENTES_ID_ROL_PROMOTOR);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getKBBEndPoint(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERAL, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_KBB_ENDPOINT);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);

		return parametro!=null?parametro.getValor():null;
	}

	@Override
	public String getKBBUser(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERAL, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_KBB_USER);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);

		return parametro!=null?parametro.getValor():null;
	}

	@Override
	public String getKBBActiva(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERAL, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_KBB_ACTIVA);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);

		return parametro!=null?parametro.getValor():null;
	}

	@Override
	public String getMarcaFronterizo(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERAL, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_FRONTERIZO_MARCA);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);

		return parametro!=null?parametro.getValor():null;
	}

	@Override
	public String getEstiloFronterizo(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERAL, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_FRONTERIZO_ESTILO);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);

		return parametro!=null?parametro.getValor():null;
	}

	@Override
	public String getCondicionFronterizo() {
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERAL, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_FRONTERIZO_CONDICION);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);

		return parametro!=null?parametro.getValor():null;
	}

	@Override
	public String getPasswordCertificadoClienteSuite(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_CLIENTE_SUITE, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_CLIENTE_SUITE_PW_CERTIFICADO);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getNombreCertificadoClienteSuite(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_CLIENTE_SUITE, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_CLIENTE_SUITE_NOMBRE_CERTIFICADO);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getClienteSuiteEnvironment(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_CLIENTE_SUITE, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_CLIENTE_SUITE_ENVIRONMENT);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getRutaCertificadoClienteMovil(){
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_CLIENTE_SUITE, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_RUTA_CERTIFICADO);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	@Override
	public String getAjustadorMovilDeviceApplicationId() {
		return "ajustadorMovil";
	}

	@Override
	public boolean getTimerActivo() {
		return Boolean.valueOf(entornoService.getVariable("midas.timer.activo","true"));
	}


	@Override
	public String getEnvioProvAsisVidaFTP(){
		return entornoService.getVariable("midas.sistema.envioProvAsisVida.sFTP","172.25.1.222");
	}

	@Override
	public int getEnvioProvAsisVidaPort(){
		return Integer.parseInt(entornoService.getVariable("midas.sistema.envioProvAsisVida.port","22"));
	}

	@Override
	public String getEnvioProvAsisVidaUser(){
		return entornoService.getVariable("midas.sistema.envioProvAsisVida.sUser","segasis");
	}

	@Override
	public String getEnvioProvAsisVidaPassword(){
		return entornoService.getVariable("midas.sistema.envioProvAsisVida.sPassword","SAOeZmSfHg");
	}

	@Override
	public String getEnvioProvAsisVidaDirectory(){
		return entornoService.getVariable("midas.sistema.envioProvAsisVida.directory","/home/sftp_vida_asis_qa");
	}	
	
}

