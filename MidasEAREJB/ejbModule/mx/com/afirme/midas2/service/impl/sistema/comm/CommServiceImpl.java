package mx.com.afirme.midas2.service.impl.sistema.comm;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.domain.sistema.comm.CommBitacora;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacora.STATUS_BITACORA;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacoraDetalle;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacoraDetalle.STATUS_COMM;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso.TIPO_COMM;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso.TIPO_PROCESO;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso;
import mx.com.afirme.midas2.dto.TransporteCommDTO;
import mx.com.afirme.midas2.exeption.CommException;
import mx.com.afirme.midas2.exeption.CommException.CODE;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.sistema.comm.CommBitacoraService;
import mx.com.afirme.midas2.service.sistema.comm.CommService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.T;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public abstract class CommServiceImpl<T> implements CommService<T>{
	
	public static final Logger log = Logger.getLogger(CommServiceImpl.class);
	
	@Resource
	protected javax.ejb.SessionContext sessionContext;
	
	protected CommService<T> processor;
	
	@EJB
	protected EnvioNotificacionesService envioNotificacionesService;
	
	@EJB
	protected CommBitacoraService commBitacoraService;
	
	private Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
	
	@Override
	public Boolean permiteEnviar(String folio){
		CommBitacora bitacora = commBitacoraService.obtenerBitacora(folio);
		if(bitacora == null || bitacora.getStatus().equals(STATUS_BITACORA.ERROR.name() ) || 
				bitacora.getStatus().equals(STATUS_BITACORA.CANCELADO.name() )){
			return true;
		}
		return false;
	}
	
	@Override
	public void realizarEnvio(T obj) throws CommException{	
		TransporteCommDTO<T> comm = new TransporteCommDTO<T>();
		try{			
			comm = init(obj);
			processor.registrar(comm);
			comm = enviar(comm);
			cierreExitoso(comm);
		}catch(CommException ex){
			log.error("Error CommService CommException: "+ex);			
			if(ex.getCode().equals(CODE.COMM_ERROR)){
				comm.setStatusComm(STATUS_COMM.NOK_COMM_ERROR);
				comm.setStatusDescripcion( ex.getCleanMessage() );
				cierreError(comm);
			}else{
				comm.setStatusComm(STATUS_COMM.NOK_DATA_ERROR);
				comm.setStatusDescripcion( ex.getCleanMessage() );
				cierreError(comm);
				throw ex;
			}
			notificarError(comm, ex.getMessage());
		}catch(Exception ex){
			log.error("Error CommService realizarEnvio Exception: "+ex);						
			comm.setStatusComm(STATUS_COMM.NOK_FATAL_ERROR);	
			comm.setStatusTramite(STATUS_BITACORA.ERROR);
			comm.setStatusDescripcion(imprimirTrazaError(ex));
			notificarError(comm, ex.getMessage());
			processor.registrarDetalle(comm);
			throw new CommException(CODE.GENERAL_ERROR, ex.getMessage());
		}		
	}
	
	public void notificarError(TransporteCommDTO<T> comm, String msj){		
		if(comm == null || comm.getTipoProceso() == null){
			throw new CommException(CODE.PARAMS_ERROR, "No puede identificarse el proceso");
		}			
		CommProceso proceso = commBitacoraService.obtenerProceso(comm.getTipoProceso());
		if(proceso != null && proceso.getNotificar().booleanValue() && proceso.getCodigoProcesoNotificacion() != null){
			String folio = comm.getFolio();
			String error = "";
			Map<String, Serializable> params = new HashMap<String, Serializable>();
			params.put("folio", folio);
			if(comm.getStatusComm() == null || comm.getStatusComm().equals(STATUS_COMM.NOK_FATAL_ERROR)){
				error = "Error general";
			}else if(comm.getStatusComm().equals(STATUS_COMM.NOK_COMM_ERROR)){
				error = "Error en la comunicación";
			}else if(comm.getStatusComm().equals(STATUS_COMM.NOK_DATA_ERROR)){
				error = "Error en los datos enviados";
			}
			params.put("error", error);
			params.put("msj", StringUtil.isEmpty(msj)?"":msj);
			notificar(comm.getCodigoNotificacion(), params);
		}
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void registrar(TransporteCommDTO<T> comm) {
		if(comm == null || comm.getTipoProceso() == null){
			throw new CommException(CODE.PARAMS_ERROR, "No puede identificarse el proceso");
		}			
		CommProceso proceso = commBitacoraService.obtenerProceso(comm.getTipoProceso());
		if(proceso != null){
			CommBitacora bitacora = commBitacoraService.obtenerBitacora(comm.getFolio());
			if(bitacora == null){			
				bitacora = new CommBitacora(proceso, comm.getFolio(), 
					comm.getUsuario()==null?"M2ADMINI":comm.getUsuario().getNombreUsuario(), convertirAJson(comm.getObjetoASerializar()));
				try{
					commBitacoraService.registrarBitacora(bitacora);
				}catch(Exception ex){
					log.error(ex);
				}
			}			
		}		
	}
	

	@Override
	public Boolean reintentar(TransporteCommDTO<T> comm) {				
		if(comm == null || comm.getStatusComm() == null || StringUtil.isEmpty(comm.getFolio())){
			throw new CommException(CODE.PARAMS_ERROR, "No puede identificarse el folio");
		}		
		if(comm.getStatusComm().equals(STATUS_COMM.PENDIENTE) || comm.getStatusComm().equals(STATUS_COMM.OK)){
			return false;
		}		
		CommProceso proceso = commBitacoraService.obtenerProceso(comm.getTipoProceso());							
		if(!comm.getStatusComm().equals(STATUS_COMM.NOK_DATA_ERROR) && proceso.getMaxNumReintentos() != null && proceso.getMaxNumReintentos().intValue() > 0){
			CommBitacoraDetalle filtro = new CommBitacoraDetalle();
			filtro.setBitacora(new CommBitacora());
			filtro.getBitacora().setFolio(comm.getFolio());
			filtro.setStatusComm(STATUS_COMM.PENDIENTE.toString());
			List<CommBitacoraDetalle> detalle = commBitacoraService.obtenerBitacoraDetalle(filtro);
			if(detalle == null || detalle.size() <= proceso.getMaxNumReintentos().intValue()){
				return true;
			}
		}
		return false;
	}
		
	
	@Override
	public void notificar(EnvioNotificacionesService.EnumCodigo codigoNotificacion, 
			Map<String, Serializable> datosNotificacion) {
		try{
			envioNotificacionesService.enviarNotificacion(codigoNotificacion.toString(), (HashMap<String,Serializable>)datosNotificacion);
		}catch(Exception ex){
			log.error("No fue posible enviar notificacion: ", ex);
		}
	}
	
	public void cierreError(TransporteCommDTO<T> comm){
		while(reintentar(comm)){			
			try{
				comm.setStatusComm(STATUS_COMM.PENDIENTE);
				processor.registrarDetalle(comm);
				enviar(comm);
				comm.setStatusComm(STATUS_COMM.OK);
				comm.setStatusTramite(STATUS_BITACORA.TERMINADO);
			}catch(CommException ex){
				log.error("WS Error cierreError: "+ex);
				comm.setStatusTramite(STATUS_BITACORA.ERROR);
				if(ex.getCode().equals(CODE.COMM_ERROR)){
					comm.setStatusComm(STATUS_COMM.NOK_COMM_ERROR);
					comm.setStatusDescripcion( ex.getCleanMessage() );
				}else if(ex.getCode().equals(CODE.DATA_ERROR)){
					comm.setStatusComm(STATUS_COMM.NOK_DATA_ERROR);
					comm.setStatusDescripcion( ex.getCleanMessage() );
				}				
			}finally{
				processor.registrarDetalle(comm);
			}
		}
	}
	
	public void cierreExitoso(TransporteCommDTO<T> comm){
		comm.setStatusComm(STATUS_COMM.OK);
		comm.setStatusTramite(STATUS_BITACORA.TERMINADO);
		processor.registrarDetalle(comm);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void registrarDetalle(TransporteCommDTO<T> comm) {
		if(comm == null || StringUtil.isEmpty(comm.getFolio())){
			throw new CommException(CODE.PARAMS_ERROR, "No puede identificarse el folio");
		}		
		CommBitacora bitacora = commBitacoraService.obtenerBitacora(comm.getFolio());
		if(bitacora != null){
			CommBitacoraDetalle detalle = new CommBitacoraDetalle(
					convertirAJson(comm.getObjetoASerializar()), comm.getStatusComm(), comm.getUsuario()==null?"M2ADMINI":comm.getUsuario().getNombreUsuario(), comm.getStatusDescripcion());
			bitacora.agregarDetalle(detalle);
			if(comm.getStatusTramite() != null){
				bitacora.setStatus(comm.getStatusTramite().toString());
			}
			commBitacoraService.registrarBitacora(bitacora);
		}
	}
	
	protected String convertirAJson(T obj){	
		try{ 
			return gson.toJson(obj);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	
	@Override
	public void registrarErrorPrevioEnvio(TransporteCommDTO comm,String mensajeError ) {
		
		processor.registrar(comm);
								
		comm.setStatusComm(STATUS_COMM.NOK_DATA_ERROR);	
		comm.setStatusTramite(STATUS_BITACORA.ERROR);
		comm.setStatusDescripcion(mensajeError);
		processor.registrarDetalle(comm);
		
	}

}
