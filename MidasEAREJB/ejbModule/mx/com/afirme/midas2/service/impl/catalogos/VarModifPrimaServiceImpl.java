package mx.com.afirme.midas2.service.impl.catalogos;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.catalogos.VarModifPrimaDao;
import mx.com.afirme.midas2.domain.catalogos.VarModifPrima;
import mx.com.afirme.midas2.dto.ControlDinamicoDTO;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.wrapper.VariableModifPrimaComboDTO;
import mx.com.afirme.midas2.service.catalogos.VarModifPrimaService;
import mx.com.afirme.midas2.utils.Convertidor;

@Stateless
public class VarModifPrimaServiceImpl implements
VarModifPrimaService {

	@Override
	public List<VarModifPrima> findAll() {
		return grupoDao.findAll();
	}
	
	@Override
	public VarModifPrima findById(Long id) {
		return grupoDao.findById(id);
	}
	

	private VarModifPrimaDao grupoDao;
	private Convertidor convertidor;
	
	@EJB
	public void setGrupoDao(VarModifPrimaDao grupoDao) {
		this.grupoDao = grupoDao;
	}
	
	@EJB
	public void setConvertidor(Convertidor convertidor) {
		this.convertidor = convertidor;
	}

	
	public VarModifPrima findByClave(Long clave) {
		
		VarModifPrima filtroTcVarModifPrima = new VarModifPrima();
		
		filtroTcVarModifPrima.setId(clave);
		
		List<VarModifPrima> descGrupoTcVarModifPrimaList = grupoDao.findByFilters(filtroTcVarModifPrima);
		if (descGrupoTcVarModifPrimaList != null && descGrupoTcVarModifPrimaList.size() > 0) {
			return descGrupoTcVarModifPrimaList.get(0);
		}
		return null;
	}
	
	public List<VarModifPrima> findByFilters(VarModifPrima filtroDescPrima) {
		if(filtroDescPrima==null)filtroDescPrima = new VarModifPrima();
		return grupoDao.findByFilters(filtroDescPrima);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remove(VarModifPrima tcVarModifPrima) {
		tcVarModifPrima = grupoDao.getReference(tcVarModifPrima.getId());
		grupoDao.remove(tcVarModifPrima);
	}
	
	public VarModifPrima save(VarModifPrima tcVarModifPrima) {
		
		if (tcVarModifPrima.getId() != null) {
			return grupoDao.update(tcVarModifPrima);
		} else {
			grupoDao.persist(tcVarModifPrima);
			return tcVarModifPrima;
		}
	}

	@Override
	public List<RegistroDinamicoDTO> getRegistrosDinamicos() {
		List<VarModifPrima> lista=findAll();
		List<RegistroDinamicoDTO> registro=convertidor.getListaRegistrosDinamicos(lista);
		return registro;
	}

	@Override
	public RegistroDinamicoDTO verDetalle(String id, String accion,Class<?> vista) {
		RegistroDinamicoDTO registro=null;
		VarModifPrima bean=null;
		if(TipoAccionDTO.getAgregarModificar().equals(accion)){
			bean=new VarModifPrima(id);
			registro=convertidor.getRegistroDinamico(bean);
			if(registro!=null){
				List<ControlDinamicoDTO> controles=registro.getControles();
				convertidor.setAttribute("editable", false, controles,"id","numeroSecuencia","rangoMinimo","rangoMaximo","valor");
			}
		}else if(TipoAccionDTO.getEliminar().equals(accion)){
			Long clave=getId(id);
			bean=findById(clave);
			registro=convertidor.getRegistroDinamico(bean);
			if(registro!=null){
				List<ControlDinamicoDTO> controles=registro.getControles();
				if(controles!=null && !controles.isEmpty()){
					for(ControlDinamicoDTO control:controles){
						if("grupo.claveTipoDetalle".equals(control.getAtributoMapeo())){
							String claveTipoDetalle=control.getValor();
							if(claveTipoDetalle!=null && !claveTipoDetalle.isEmpty()){
								//Si la clave es 
								if("0".equals(claveTipoDetalle)){
									convertidor.setAttribute("valor", "-", controles,"rangoMinimo");
									convertidor.setAttribute("valor", "-", controles,"rangoMaximo");
									
								}else{
									convertidor.setAttribute("valor", "-", controles,"valor");
								}
							}
						}
					}
				}
				//Cuando es baja, ningun campo es editable
				convertidor.setAttribute("editable", false, controles);
			}
		}else if(TipoAccionDTO.getEditar().equals(accion)){
			Long clave=getId(id);
			bean=findById(clave);
			registro=convertidor.getRegistroDinamico(bean);
			if(registro!=null){
				List<ControlDinamicoDTO> controles=registro.getControles();
				if(controles!=null && !controles.isEmpty()){
					for(ControlDinamicoDTO control:controles){
						if("grupo.claveTipoDetalle".equals(control.getAtributoMapeo())){
							String claveTipoDetalle=control.getValor();
							if(claveTipoDetalle!=null && !claveTipoDetalle.isEmpty()){
								//Si la clave es 
								if("0".equals(claveTipoDetalle)){
									convertidor.setAttribute("editable",false, controles,"grupo.id","numeroSecuencia","rangoMinimo","rangoMaximo");
									convertidor.setAttribute("valor", "-", controles,"rangoMinimo");
									convertidor.setAttribute("valor", "-", controles,"rangoMaximo");
									
								}else{
									convertidor.setAttribute("editable",false, controles,"grupo.id","numeroSecuencia","valor");
									convertidor.setAttribute("valor", "-", controles,"valor");
								}
							}
						}
					}
				}
				//Cuando es cambio solo los ids no son editables
				convertidor.setAttribute("editable", false, controles,"id");
			}
		}
		return registro;
	}
	
	public List<VariableModifPrimaComboDTO> getListaVarModifPrimaComboDTO(Long idGrupo){
		List<VarModifPrima> lista = grupoDao.findByGrupoModifPrima(idGrupo);
		
		List<VariableModifPrimaComboDTO> listaResultado = new ArrayList<VariableModifPrimaComboDTO>();
		
		for(VarModifPrima tmp : lista){
			listaResultado.add(
					new VariableModifPrimaComboDTO(tmp));
		}
		
		return listaResultado;
	}
	
	/**
	 * Obtiene el id a partir de una clave con formato: VMP_#id
	 * @param clave
	 * @return
	 */
	private Long getId(String clave){
		Long id=null;
		if(clave!=null && !clave.isEmpty() && clave.startsWith("VMP_")){
			String[] parts=clave.split("_");
			//Si tiene 2 elementos
			if(parts!=null && parts.length==2){
				String idPart=parts[1];
				id=Long.parseLong(idPart);
			}
		}
		return id;
	}

	@Override
	public List<RegistroDinamicoDTO> getRegistrosDinamicos(Object filtro) {
		//TODO implementar este método, ahora no se usa para este catálogo.
		throw new RuntimeException("Not implemented yet");
	}

	
}
