/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoEntidadDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoEntidad;
import mx.com.afirme.midas2.service.compensaciones.CaTipoEntidadService;

import org.apache.log4j.Logger;

@Stateless

public class CaTipoEntidadServiceImpl  implements CaTipoEntidadService {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@EJB
	private CaTipoEntidadDao tipoEntidadcaDao;
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaTipoEntidadServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaTipoEntidad entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoEntidad entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoEntidad entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoEntidad 	::		CaTipoEntidadServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	tipoEntidadcaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoEntidad 	::		CaTipoEntidadServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoEntidad 	::		CaTipoEntidadServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaTipoEntidad entity.
	  @param entity CaTipoEntidad entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoEntidad entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoEntidad 	::		CaTipoEntidadServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaTipoEntidad.class, entity.getId());
	        	tipoEntidadcaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoEntidad 	::		CaTipoEntidadServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoEntidad 	::		CaTipoEntidadServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaTipoEntidad entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoEntidad entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoEntidad entity to update
	 @return CaTipoEntidad the persisted CaTipoEntidad entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaTipoEntidad update(CaTipoEntidad entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoEntidad 	::		CaTipoEntidadServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoEntidad result = tipoEntidadcaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoEntidad 	::		CaTipoEntidadServiceImpl	::	update	::	FIN	::	");
	        return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoEntidad 	::		CaTipoEntidadServiceImpl	::	update	::	ERROR	::	",re);
            throw re;
        }
    }
    
    public CaTipoEntidad findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoEntidadServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoEntidad instance = tipoEntidadcaDao.findById(id);//entityManager.find(CaTipoEntidad.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoEntidadServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoEntidadServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaTipoEntidad entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoEntidad property to query
	  @param value the property value to match
	  	  @return List<CaTipoEntidad> found by query
	 */
    public List<CaTipoEntidad> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoEntidadServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoEntidad model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoEntidadServiceImpl	::	findByProperty	::	FIN	::	");
			return tipoEntidadcaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoEntidadServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoEntidad> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoEntidad> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoEntidad> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoEntidad> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaTipoEntidad entities.
	  	  @return List<CaTipoEntidad> all CaTipoEntidad entities
	 */
	public List<CaTipoEntidad> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoEntidadServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoEntidad model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoEntidadServiceImpl	::	findAll	::	FIN	::	");
			return tipoEntidadcaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoEntidadServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}