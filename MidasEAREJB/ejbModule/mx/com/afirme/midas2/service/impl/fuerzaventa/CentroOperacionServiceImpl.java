package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fuerzaventa.CentroOperacionDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.service.fuerzaventa.CentroOperacionService;
@Stateless
//@TransactionManagement(TransactionManagementType.CONTAINER)
public class CentroOperacionServiceImpl implements CentroOperacionService{
	private CentroOperacionDao centroOperacionDao;
	
	@EJB
	public void setCentroOperacionDao(CentroOperacionDao centroOperacionDao) {
		this.centroOperacionDao = centroOperacionDao;
	}

	@Override
	public CentroOperacion saveFull(CentroOperacion arg0) throws Exception {
		return centroOperacionDao.saveFull(arg0);
	}
	
	@Override
	public void unsubscribe(CentroOperacion arg0) throws Exception {
		centroOperacionDao.unsubscribe(arg0);
	}

	@Override
	public List<CentroOperacion> findByFilters(CentroOperacion arg0) {
		return centroOperacionDao.findByFilters(arg0);
	}

	@Override
	public List<CentroOperacionView> findByFiltersView(CentroOperacion arg0){
		return centroOperacionDao.findByFiltersView(arg0);
	}
	
	@Override
	public CentroOperacion loadById(CentroOperacion entidad){
		return centroOperacionDao.loadById(entidad, null);
	}
	
	@Override
	public CentroOperacion loadById(CentroOperacion entidad, String fechaHistorico){
		return centroOperacionDao.loadById(entidad, fechaHistorico);
	}
	
	@Override
	public List<CentroOperacionView> getList(boolean isOnlyActive){
		return centroOperacionDao.getList(isOnlyActive);
	}
}
