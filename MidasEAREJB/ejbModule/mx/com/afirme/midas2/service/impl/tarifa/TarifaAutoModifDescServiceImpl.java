package mx.com.afirme.midas2.service.impl.tarifa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.com.afirme.midas2.dao.tarifa.TarifaAutoModifDescDao;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifDesc;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifDescId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;
import mx.com.afirme.midas2.dto.ControlDinamicoDTO;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.tarifa.TarifaAutoModifDescService;
import mx.com.afirme.midas2.utils.Convertidor;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TarifaAutoModifDescServiceImpl extends JpaTarifaService<TarifaAutoModifDescId, TarifaAutoModifDesc> implements TarifaAutoModifDescService {

	@EJB
	private TarifaAutoModifDescDao tarifaAutoModifDescDao;
	private Convertidor convertidor;
	
	@EJB
	public void setConvertidor(Convertidor convertidor) {
		this.convertidor = convertidor;
	}
	
	@Override
	public List<TarifaAutoModifDesc> findByFilters(TarifaAutoModifDesc filtro) {
		return tarifaAutoModifDescDao.findByFilters(filtro);
	}
	
	@Override
	public List<TarifaAutoModifDesc> findByTarifaVersionId(
			TarifaVersionId tarifaVersionId) {
		return tarifaAutoModifDescDao.findByTarifaVersionId(tarifaVersionId);
	}

	@Override
	public RegistroDinamicoDTO verDetalle(
			TarifaVersionId tarifaVersionId,
			String id,
			String accion, Class<?> vista) {
		TarifaAutoModifDesc tarifa = new TarifaAutoModifDesc();
		RegistroDinamicoDTO registro = null;
		
		if(TipoAccionDTO.getAgregarModificar().equals(accion)){
			TarifaAutoModifDescId idTarifa = new TarifaAutoModifDescId(tarifaVersionId);
			tarifa.setId(idTarifa);
			registro = convertidor.getRegistroDinamico(tarifa, vista);
		}else if(TipoAccionDTO.getEliminar().equals(accion)){
			TarifaAutoModifDescId tarifaId = new TarifaAutoModifDescId(id);
			tarifa = findById(TarifaAutoModifDesc.class, tarifaId);
			registro = convertidor.getRegistroDinamico(tarifa, vista);
			if(registro!=null){
				List<ControlDinamicoDTO> controles=registro.getControles();
				convertidor.setAttribute("editable", false, controles);
			}
		}else if(TipoAccionDTO.getEditar().equals(accion)){
			TarifaAutoModifDescId tarifaId = new TarifaAutoModifDescId(id);
			tarifa = findById(TarifaAutoModifDesc.class, tarifaId);
			registro = convertidor.getRegistroDinamico(tarifa, vista);
			if(registro!=null){
				List<ControlDinamicoDTO> controles=registro.getControles();
				convertidor.setAttribute("editable", false, controles,"varModifDescripcion.idGrupo");
			}
		}
		
		return registro;
	}

	@Override
	public List<RegistroDinamicoDTO> getRegistrosDinamicosPorTarifaVersionId(
			TarifaVersionId tarifaVersionId, Class<?> vista) {
		List<TarifaAutoModifDesc> tarifas = this.findByTarifaVersionId(tarifaVersionId);
		List<RegistroDinamicoDTO> registros = convertidor.getListaRegistrosDinamicos(tarifas, vista);
		
		return registros;
	}

	public TarifaAutoModifDesc getNewObject(){
		return new TarifaAutoModifDesc();
	}
}
