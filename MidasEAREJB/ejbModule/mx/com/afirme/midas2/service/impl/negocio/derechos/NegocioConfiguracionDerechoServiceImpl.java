package mx.com.afirme.midas2.service.impl.negocio.derechos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas2.dao.negocio.derechos.NegocioConfiguracionDerechoDao;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioConfiguracionDerecho;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.dto.negocio.derechos.NegocioConfiguracionDerechoDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.MotorDecisionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioConfiguracionDerechoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.agentes.CotizadorAgentesService;

@Stateless
public class NegocioConfiguracionDerechoServiceImpl implements NegocioConfiguracionDerechoService{

	@EJB
	private NegocioConfiguracionDerechoDao negocioConfiguracionDerechoDao;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private CotizadorAgentesService cotizadorAgentesService;
	
	@Override
	public NegocioConfiguracionDerecho guardarNegocioConfiguracionDerecho(NegocioConfiguracionDerecho negocioConfigDerecho){
		if(negocioConfigDerecho != null){
			if(!existeConfiguracionDerecho(negocioConfigDerecho)){
				if(negocioConfigDerecho.getTipoDerecho().equals(NegocioConfiguracionDerecho.TipoDerecho.POLIZA.toString())){
					NegocioDerechoPoliza derechoPoliza = 
						entidadService.findById(NegocioDerechoPoliza.class, negocioConfigDerecho.getIdToNegDerecho());
					negocioConfigDerecho.setImporteDerecho(derechoPoliza.getImporteDerecho());
					
				}else{
					NegocioDerechoEndoso derechoEndoso = 
						entidadService.findById(NegocioDerechoEndoso.class, negocioConfigDerecho.getIdToNegDerecho());
					negocioConfigDerecho.setImporteDerecho(derechoEndoso.getImporteDerecho());
				}
				if(negocioConfigDerecho.getTipoUsoVehiculo() != null 
						&& negocioConfigDerecho.getTipoUsoVehiculo().getIdTcTipoUsoVehiculo() == null){
					negocioConfigDerecho.setTipoUsoVehiculo(null);
				}
				if(negocioConfigDerecho.getEstado() != null 
						&& negocioConfigDerecho.getEstado().getStateId() == null
						|| negocioConfigDerecho.getEstado().getStateId().isEmpty()){
					negocioConfigDerecho.setEstado(null);
				}
				if(negocioConfigDerecho.getMunicipio() != null 
						&& negocioConfigDerecho.getMunicipio().getCityId() == null
						|| negocioConfigDerecho.getMunicipio().getCityId().isEmpty()){
					negocioConfigDerecho.setMunicipio(null);
				}
				negocioConfigDerecho.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
				return entidadService.save(negocioConfigDerecho);
			}
		}
		return null;
	}
	
	@Override
	public List<NegocioConfiguracionDerechoDTO> buscarNegocioConfiguracionDerecho(NegocioConfiguracionDerecho filtroConfigDerecho){
		return negocioConfiguracionDerechoDao.buscarConfiguracionesDerecho(filtroConfigDerecho);
	}
	
	private boolean existeConfiguracionDerecho(NegocioConfiguracionDerecho negocioConfigDerecho){
		List<NegocioConfiguracionDerechoDTO> resultado = buscarNegocioConfiguracionDerecho(negocioConfigDerecho);
		if(resultado != null
				&& !resultado.isEmpty()){
			return true;
		}
		return false;
	}

	@Override
	public Long conteoBusquedaConfiguracionesDerecho(NegocioConfiguracionDerecho negocioConfiguracionDerecho){
		return negocioConfiguracionDerechoDao.conteoBusquedaConfiguracionesDerecho(negocioConfiguracionDerecho);
	}
	
	@Override
	public void eliminarNegocioConfiguracionDerecho(NegocioConfiguracionDerecho configuracionDerecho){
		NegocioConfiguracionDerecho entidad = entidadService.findById(NegocioConfiguracionDerecho.class, configuracionDerecho.getIdToNegConfigDerecho());
		entidadService.remove(entidad);
		
	}
	
	@Override
	public MotorDecisionDTO obtenerFiltroDerechoParaCotizacion(BigDecimal idToCotizacion, BigDecimal numeroInciso){
		MotorDecisionDTO filtroDerecho = null;
		
		if(idToCotizacion != null
				&& numeroInciso != null){
			filtroDerecho = new MotorDecisionDTO();
			
			//recuperando la informacion de la cotizacion
			IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
			incisoCotizacionId.setIdToCotizacion(idToCotizacion);
			incisoCotizacionId.setNumeroInciso(numeroInciso);
			IncisoCotizacionDTO incisoCotizacion =  entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionId);
			CotizacionDTO cotizacion = incisoCotizacion.getCotizacionDTO();
			
			if(cotizacion.getNegocioTipoPoliza() == null
					|| cotizacion.getNegocioTipoPoliza().getIdToNegTipoPoliza() == null){
				NegocioTipoPoliza negocioTipoPoliza = cotizadorAgentesService.getNegocioTipoPoliza(cotizacion);
				cotizacion.setNegocioTipoPoliza(negocioTipoPoliza);
			}
			
			//inicia la carga de los filtros para la busqueda
			filtroDerecho.setTipoConsulta(MotorDecisionDTO.TipoConsulta.TIPO_CONSULTA_DERECHOS);
			filtroDerecho.setIdToNegocio(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
			
			if( incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId() != null
					&& incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId() != null
					&& cotizacion.getIdMoneda() != null){
				
				List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosMotor = new ArrayList<MotorDecisionDTO.FiltrosMotorDecisionDTO>();
				
				//filtros requeridos para la busqueda
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.TRUE,"negocio.idToNegocio",cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio()));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.TRUE,"tipoPoliza.idToNegTipoPoliza",cotizacion.getNegocioTipoPoliza().getIdToNegTipoPoliza()));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.TRUE,"seccion.idToNegSeccion",incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId()));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.TRUE,"paquete.idToNegPaqueteSeccion",incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId()));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.TRUE,"moneda.idTcMoneda",cotizacion.getIdMoneda()));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.TRUE,"tipoDerecho",NegocioConfiguracionDerecho.TipoDerecho.POLIZA.toString()));
				
				EstadoDTO estado = null;
				CiudadDTO municipio = null;
				TipoUsoVehiculoDTO tipoUsoVehiculo = null;
				
				if(incisoCotizacion.getIncisoAutoCot().getEstadoId() != null){
					estado = entidadService.findById(EstadoDTO.class, incisoCotizacion.getIncisoAutoCot().getEstadoId());
				}
				if(incisoCotizacion.getIncisoAutoCot().getMunicipioId() != null){
					municipio = entidadService.findById(CiudadDTO.class, incisoCotizacion.getIncisoAutoCot().getMunicipioId());
				}
				if(incisoCotizacion.getIncisoAutoCot().getTipoUsoId() != null){
					tipoUsoVehiculo = entidadService.findById(TipoUsoVehiculoDTO.class, BigDecimal.valueOf(incisoCotizacion.getIncisoAutoCot().getTipoUsoId()));
				}
				
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.FALSE,"estado",estado));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.FALSE,"municipio",municipio));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.FALSE,"tipoUsoVehiculo",tipoUsoVehiculo));
				
				filtroDerecho.setFiltrosDTO(filtrosMotor);
				
			}
			
		}
		
		return filtroDerecho;
	}
	
	@Override
	public MotorDecisionDTO obtenerFiltroDerechoParaCotizacion(Long idNegocio, BigDecimal idTipoPoliza, Long idSeccion, 
			Long idPaquete, BigDecimal idMoneda,  String idEstado, String idMunicipio, Long idTipoUso){
		MotorDecisionDTO filtroDerecho = null;
		
			filtroDerecho = new MotorDecisionDTO();
			
			//inicia la carga de los filtros para la busqueda
			filtroDerecho.setTipoConsulta(MotorDecisionDTO.TipoConsulta.TIPO_CONSULTA_DERECHOS);
			filtroDerecho.setIdToNegocio(idNegocio);
			
				
				List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosMotor = new ArrayList<MotorDecisionDTO.FiltrosMotorDecisionDTO>();
				
				//filtros requeridos para la busqueda
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.TRUE,"negocio.idToNegocio",idNegocio));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.TRUE,"tipoPoliza.idToNegTipoPoliza",idTipoPoliza));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.TRUE,"seccion.idToNegSeccion",idSeccion));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.TRUE,"paquete.idToNegPaqueteSeccion",idPaquete));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.TRUE,"moneda.idTcMoneda",idMoneda));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.TRUE,"tipoDerecho",NegocioConfiguracionDerecho.TipoDerecho.POLIZA.toString()));
				
				EstadoDTO estado = null;
				CiudadDTO municipio = null;
				TipoUsoVehiculoDTO tipoUsoVehiculo = null;
				
				if(idEstado != null){
					estado = entidadService.findById(EstadoDTO.class, idEstado);
				}
				if(idMunicipio != null){
					municipio = entidadService.findById(CiudadDTO.class, idMunicipio);
				}
				if(idTipoUso != null){
					tipoUsoVehiculo = entidadService.findById(TipoUsoVehiculoDTO.class, BigDecimal.valueOf(idTipoUso));
				}
				
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.FALSE,"estado",estado));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.FALSE,"municipio",municipio));
				filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
						Boolean.FALSE,"tipoUsoVehiculo",tipoUsoVehiculo));
				
				filtroDerecho.setFiltrosDTO(filtrosMotor);

		
		return filtroDerecho;
	}
}
