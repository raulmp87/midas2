package mx.com.afirme.midas2.service.impl.compensaciones;

import java.math.BigDecimal;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import mx.com.afirme.midas2.dao.compensaciones.CaBancaPrimPagCPDao;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaBancaPrimPagCPDaoImpl;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaPrimPagCP;
import mx.com.afirme.midas2.service.compensaciones.CaBancaPrimPagCPService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class CaBancaPrimPagCPServiceImpl implements CaBancaPrimPagCPService {

	private static final Logger LOG = LoggerFactory.getLogger(CaBancaPrimPagCPDaoImpl.class);	
	@EJB
	CaBancaPrimPagCPDao caBancaPrimaPagCPDao;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	public CaBancaPrimPagCP findConfiguracionActual(int anio, int mes){
		LOG.info("findConfiguracionActual() >>");
		CaBancaPrimPagCP configuracion = null;
		try{
			Calendar now = Calendar.getInstance();
			//int anio = now.get(Calendar.YEAR);
			//int mes = now.get(Calendar.MONTH)+1;
			final StringBuilder queryString = new StringBuilder(200);
			queryString.append(" SELECT * FROM MIDAS.CA_BANCA_PRIMPAG_CP ");
			queryString.append(" WHERE ANIO  =?1 AND MES =?2");
			Query query = entityManager.createNativeQuery(queryString.toString(),CaBancaPrimPagCP.class);
			query.setParameter(1, anio);
			query.setParameter(2, mes);
			configuracion = (CaBancaPrimPagCP) query.getSingleResult();
		}catch(RuntimeException re){
			if(re instanceof NoResultException){
				LOG.info(" -- No se encontraron resultados ");
			}else{
				LOG.error("Información del Error", re);
			}
			
		}
		return configuracion;
	}
	public BigDecimal findByAnioMes(Double anio, Double mes){
		LOG.info("findByAnioMes() >>");
		BigDecimal resultado = null;
		try{
			final StringBuilder queryString = new StringBuilder(200);
			queryString.append(" SELECT ID FROM MIDAS.CA_BANCA_PRIMPAG_CP ");
	        queryString.append(" WHERE ANIO  =?1 AND MES =?2");
	        Query query = entityManager.createNativeQuery(queryString.toString());
	        query.setParameter(1,anio);
	        query.setParameter(2,mes);
	        query.setMaxResults(1);
	        List resultList = query.getResultList();
	        if(resultList != null  && !resultList.isEmpty()){
	        	resultado = (BigDecimal) resultList.get(0);
	        }
	      
		}catch(RuntimeException re){
			throw re;
		}
		return resultado;
	} 
	
		
	public void save(CaBancaPrimPagCP entity){
		LOG.info(">> guardarConfiguracion()");
		try{
			caBancaPrimaPagCPDao.save(entity);
		}catch(RuntimeException re){
			throw re;
			
		}
	}
	
	
}
