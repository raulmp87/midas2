package mx.com.afirme.midas2.service.impl.suscripcion.solicitud;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoFacadeRemote;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudFacadeRemote;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteService;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.SolicitudPolizaService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.comentarios.ComentariosService;
import mx.com.afirme.midas2.util.MailService;

@Stateless
public class SolicitudPolizaServiceImpl implements SolicitudPolizaService {

	public static final String TITULO_CORREO_CREACION="Ingreso de Solicitud Seguros Afirme Autos "; 
	public static final String SALUDO_CORREO_CREACION="A quien corresponda.";
	public static final String NEGOCIO_AUTOS="A";
	
	protected SolicitudFacadeRemote solicitudFacadeRemote;
	protected EntidadService entidadService;
	protected EstadisticasEstadoFacadeRemote estadisticas;
	protected MailService mailService;
	protected CotizacionService cotizacionService;
	protected UsuarioService usuarioService;
	protected SistemaContext sistemaContext;
	protected AgenteService agenteService;
	protected CotizacionFacadeRemote cotizacionFacadeRemote;
	protected EstadisticasEstadoFacadeRemote estadisticasEstadoFacadeRemote;
	private AgenteMidasService agenteMidasService;
	private EjecutivoService ejecutivoService;
	private ComentariosService comentariosService;
	
	@EJB	
	public void setComentariosService(ComentariosService comentariosService) {
		this.comentariosService = comentariosService;
	}	
	@EJB	
	public void setAgenteService(AgenteService agenteService) {
		this.agenteService = agenteService;
	}	
	@EJB	
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	@EJB(beanName = "UsuarioServiceDelegate") 
	public void setUsuarioService(
			UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	@EJB
	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}	
	@EJB
	public void setEstadisticas(EstadisticasEstadoFacadeRemote estadisticas) {
		this.estadisticas = estadisticas;
	}	
	@EJB
	public void setSolicitudFacadeRemote(SolicitudFacadeRemote solicitudFacadeRemote) {
		this.solicitudFacadeRemote = solicitudFacadeRemote;
	}	

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}
	
	@EJB	
	public void setCotizacionFacadeRemote(CotizacionFacadeRemote cotizacionFacadeRemote) {
		this.cotizacionFacadeRemote = cotizacionFacadeRemote;
	}
	
	@Override
	public SolicitudDTO actualizar(SolicitudDTO solicitudDTO) {
		Usuario usuario = usuarioService.getUsuarioActual();
		solicitudDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
		this.setValoresFuerzaDeVenta(solicitudDTO);
		solicitudDTO = solicitudFacadeRemote.update(solicitudDTO);
		estadisticasEstadoFacadeRemote.registraEstadistica(solicitudDTO, SolicitudDTO.ACCION.MODIFICACION);
		// Manda el correo
		enviaCorreoCreacion(solicitudDTO);
		return solicitudDTO;
	}
	@Override
	public BigDecimal agregarSolicitud(SolicitudDTO solicitudDTO) {
		Usuario usuario = usuarioService.getUsuarioActual();
		solicitudDTO.setClaveEstatus(SolicitudDTO.Estatus.PROCESO.getEstatus());
		solicitudDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		solicitudDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
		solicitudDTO.setNombrePersona(solicitudDTO.getNombrePersona().toUpperCase());
		if(solicitudDTO.getApellidoPaterno() != null){
			solicitudDTO.setApellidoPaterno(solicitudDTO.getApellidoPaterno().toUpperCase());
		}
		if(solicitudDTO.getApellidoMaterno() != null){
			solicitudDTO.setApellidoMaterno(solicitudDTO.getApellidoMaterno().toUpperCase());
		}
		this.setValoresFuerzaDeVenta(solicitudDTO);
		solicitudDTO.setIdToSolicitud((BigDecimal)entidadService.saveAndGetId(solicitudDTO));
		estadisticasEstadoFacadeRemote.registraEstadistica(solicitudDTO, SolicitudDTO.ACCION.CREACION);
		if(solicitudDTO != null && solicitudDTO.getIdToSolicitud() != null){
			this.enviaCorreoCreacion(solicitudDTO);
		}
		return solicitudDTO.getIdToSolicitud();
	}

	private void setValoresFuerzaDeVenta(SolicitudDTO solicitudDTO) {
		Agente agente = new Agente();
		try {
			agente.setId(solicitudDTO.getCodigoAgente().longValue());
			agente = agenteMidasService.loadById(agente);
			if (agente != null) {
				solicitudDTO.setNombreAgente(agente.getPersona()
						.getNombreCompleto());
				if (agente.getPromotoria().getEjecutivo() != null) {
					Ejecutivo filtroEjecutivo = new Ejecutivo();
					filtroEjecutivo.setId(agente.getPromotoria().getEjecutivo()
							.getId());
					Ejecutivo ejecutivo = ejecutivoService
							.loadById(filtroEjecutivo);
					if (ejecutivo != null) {
						solicitudDTO.setNombreEjecutivo(ejecutivo
								.getPersonaResponsable().getNombreCompleto());
						solicitudDTO.setNombreOficina(ejecutivo.getGerencia()
								.getPersonaResponsable().getNombreCompleto());
						solicitudDTO.setIdOficina(new BigDecimal(ejecutivo
								.getGerencia().getId()));
					}
					solicitudDTO.setNombreOficinaAgente(agente.getPromotoria()
							.getDescripcion());
				}

				/*
				 * solicitudDTO.setNombreAgente(agente.getNombre());
				 * solicitudDTO.setNombreEjecutivo(agente.getNombreOficina());
				 * solicitudDTO.setNombreOficina(agente.getNombreGerencia());
				 * solicitudDTO.setIdOficina(new
				 * BigDecimal(agente.getIdGerencia()));
				 * solicitudDTO.setNombreOficinaAgente
				 * (agente.getNombrePromotoria());
				 */
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public List<SolicitudDTO> buscarSolicitud(SolicitudDTO solicitudDTO, Date fechaInicial, Date fechaFinal,Long idToNegocioProducto) {
		List<SolicitudDTO> solicitudes = new ArrayList<SolicitudDTO>();	
		Negocio negocio = new Negocio();
		if(solicitudDTO.getNegocio() != null){
			negocio = solicitudDTO.getNegocio();
		}
		negocio.setClaveNegocio(NEGOCIO_AUTOS);
		solicitudDTO.setNegocio(negocio);
		
		
		final Map<String,Object> params = new HashMap<String, Object>();
		solicitudes = solicitudFacadeRemote.listarFilradoSolicitudGrid(solicitudDTO, fechaInicial, fechaFinal,idToNegocioProducto);
		String columns = new String();
		List<CotizacionDTO> cotizacion = null;
		try{
			for(SolicitudDTO dto : solicitudes) {
				params.clear();
				columns = "claveEstatus";
				params.put("solicitudDTO.idToSolicitud", dto.getIdToSolicitud());
				cotizacion = entidadService.findByColumnsAndProperties(CotizacionDTO.class, columns, params, new HashMap<String,Object>(), null, null);
				if (!cotizacion.isEmpty() && cotizacion != null) {
					dto.setClaveEstatusTransient(cotizacion.get(0)
							.getClaveEstatus());
				}
				//Agente
				if(dto.getCodigoAgente() != null){
					Agente agente = new Agente();
					agente.setId(dto.getCodigoAgente().longValue());
					List<AgenteView> agenteViewList = agenteMidasService.findByFilterLightWeight(agente);
					if (agenteViewList != null && !agenteViewList.isEmpty()) {
						AgenteView agenteView = agenteViewList.get(0);
						agente.setIdAgente(agenteView.getIdAgente());
						dto.setAgente(agente);
					}
				}
				params.clear();
				dto.setEsBusquedaRapida(false);
			}
		}catch(Exception exception){
			exception.printStackTrace();
		}
		return solicitudes;
	}
	
	@Override
	public List<SolicitudDTO> busquedaRapida(SolicitudDTO filtro){
		List<SolicitudDTO> solicitudes = new ArrayList<SolicitudDTO>();
		filtro.setFechaCreacion(Calendar.getInstance().getTime());
		filtro.setClaveEstatus(SolicitudDTO.Estatus.PROCESO.getEstatus());
		Negocio negocio = new Negocio();
		negocio.setClaveNegocio(NEGOCIO_AUTOS);
		filtro.setNegocio(negocio);
		//TODO: Si es <> a mesa de control, agregar usuario creacion
		//Usuario usuario = usuarioService.getUsuarioActual();		
		//filtro.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		
		solicitudes = solicitudFacadeRemote.listarFiltradoGrid(filtro);
		for (SolicitudDTO solicitud : solicitudes) {
			//Agente
			if(solicitud.getCodigoAgente() != null){
				Agente agente = entidadService.findById(Agente.class, solicitud.getCodigoAgente().longValue());
				solicitud.setAgente(agente);
			}
			solicitud.setEsBusquedaRapida(true);
		}
		return solicitudes;
	}

	@Override
	public Long obtenerTotalPaginacion(SolicitudDTO solicitudDTO, Date fechaFinal){	
		Negocio negocio = new Negocio();
		if(solicitudDTO.getNegocio() != null){
			negocio = solicitudDTO.getNegocio();
		}
		negocio.setClaveNegocio(NEGOCIO_AUTOS);
		solicitudDTO.setNegocio(negocio);
		
		return solicitudFacadeRemote.obtenerTotalFiltrado(solicitudDTO, fechaFinal);
		
	}
	
	@Override
	public Long obtenerTotalPaginacion(SolicitudDTO filtro){
		if (filtro == null) {
			filtro = new SolicitudDTO();
		}
		//List<SolicitudDTO> solicitudes = new ArrayList<SolicitudDTO>();
		filtro.setFechaCreacion(Calendar.getInstance().getTime());
		filtro.setClaveEstatus(SolicitudDTO.Estatus.PROCESO.getEstatus());
		Negocio negocio = new Negocio();
		negocio.setClaveNegocio(NEGOCIO_AUTOS);
		filtro.setNegocio(negocio);
		//TODO: Si es <> a mesa de control, agregar usuario creacion
		//Usuario usuario = usuarioService.getUsuarioActual();					
		//filtro.setCodigoUsuarioCreacion(usuario.getNombreUsuario());

		return (long) solicitudFacadeRemote.listarFilradoSolicitudGridCount(filtro,null,null,null);
//		return solicitudFacadeRemote.obtenerTotalFiltradoRapido(filtro);
		
	}
	
	@Override
	public SolicitudDTO findById(BigDecimal id) {
		return solicitudFacadeRemote.findById(id);
	}
	@Override
	public void rechazarSolicitud(SolicitudDTO solicitud) {
		solicitud.setClaveEstatus(SolicitudDTO.Estatus.RECHAZADA.getEstatus());
		solicitudFacadeRemote.update(solicitud);
		this.enviaCorreoCreacion(solicitud);
		estadisticasEstadoFacadeRemote.registraEstadistica(solicitud, SolicitudDTO.ACCION.RECHAZO);
	}
	
	
	@Override
	public void asignarCoordinador(SolicitudDTO solicitud) {
		estadisticasEstadoFacadeRemote.registraEstadistica(solicitud, SolicitudDTO.ACCION.ASIGNACION_COORDINADOR);
		solicitudFacadeRemote.update(solicitud);
	}	
	
	@Override
	public SolicitudDTO asignarSuscriptor(SolicitudDTO solicitud) {
		estadisticasEstadoFacadeRemote.registraEstadistica(solicitud, SolicitudDTO.ACCION.ASIGNACION_SUSCRIPTOR);
		solicitud.setClaveEstatus(SolicitudDTO.Estatus.TERMINADA.getEstatus());
		this.enviaCorreoCreacion(solicitud);
		solicitud.setClaveEstatus(SolicitudDTO.Estatus.PROCESO.getEstatus());
		solicitudFacadeRemote.update(solicitud);
		estadisticasEstadoFacadeRemote.registraEstadistica(solicitud, SolicitudDTO.ACCION.ASIGNACION_SUSCRIPTOR);		
		return solicitud;
	}
	
	public CotizacionDTO crearCotizacionSolicitud(SolicitudDTO solicitud) {
		solicitud.setClaveEstatus(SolicitudDTO.Estatus.TERMINADA.getEstatus());
		solicitudFacadeRemote.update(solicitud);
		estadisticasEstadoFacadeRemote.registraEstadistica(solicitud, SolicitudDTO.ACCION.TERMINAR);
		CotizacionDTO cotizacion = new CotizacionDTO();
		List<CotizacionDTO> cotizacionDTOList = new ArrayList<CotizacionDTO>(1);
		cotizacionDTOList = entidadService.findByProperty(CotizacionDTO.class, "solicitudDTO.idToSolicitud", solicitud.getIdToSolicitud());
		if (cotizacionDTOList == null || cotizacionDTOList.isEmpty()){
			cotizacion= cotizacionService.crearCotizacion(solicitud,CotizacionDTO.ESTATUS_COT_ASIGNADA);
			estadisticasEstadoFacadeRemote.registraEstadistica(cotizacion, CotizacionDTO.ACCION.CREACION);
		}else{
			cotizacion = cotizacionDTOList.get(0);
			cotizacion.setNombreContratante(solicitud.getNombrePersona() + " " + solicitud.getApellidoPaterno() + " " + solicitud.getApellidoMaterno());
			cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
			cotizacionFacadeRemote.update(cotizacion);
			estadisticasEstadoFacadeRemote.registraEstadistica(cotizacion, CotizacionDTO.ACCION.CREACION);
		}
		return cotizacion;
	}
	
	@Override
	public SolicitudDTO reasignarSuscriptor(SolicitudDTO solicitud) {
		Usuario usuario = usuarioService.getUsuarioActual();
        solicitud.setFechaModificacion(new Date());
        solicitud.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
		estadisticasEstadoFacadeRemote.registraEstadistica(solicitud, SolicitudDTO.ACCION.REASIGNACION_SUSCRIPTOR);
		solicitudFacadeRemote.update(solicitud);
		return solicitud;
	}
	
	public Comentario comentario(SolicitudDTO solicitud,Comentario comentario,String valor){
		solicitud= entidadService.findById(SolicitudDTO.class, solicitud.getIdToSolicitud());
		
	    Comentario comentarioBase = new Comentario();
	    if(valor != null){
	    	Usuario usuario = usuarioService.getUsuarioActual();
	    	Short cero = 0;
	    	valor = comentariosService.validaComentarioSolicitudCotizacion(valor, cero);
	    	comentarioBase.setValor(valor.toUpperCase());
	    	comentarioBase.setFechaCreacion(new Date());
	    	comentarioBase.setSolicitudDTO(solicitud);
	    	comentarioBase.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
	    	entidadService.save(comentarioBase);
		}		
		return comentarioBase;	
	}
	
	private void  enviaCorreoCreacion(SolicitudDTO solicitud){
		if(solicitud.getEmailContactos()!= null && solicitud.getEmailContactos().length() > 0){
			String msg=null;
		 if(solicitud.getClaveEstatus() == SolicitudDTO.Estatus.TERMINADA.getEstatus() ){ 
		     msg = "Su solicitud fue aceptada y se encuentra en proceso de cotizaci\u00f3n";
		 }else if (solicitud.getClaveEstatus() == SolicitudDTO.Estatus.RECHAZADA.getEstatus()) {
			 msg = "Su solicitud fue rechazada. Motivo: " + solicitud.getMotivoRechazo();
		 }else if (solicitud.getClaveEstatus() == SolicitudDTO.Estatus.CANCELADA.getEstatus()) {
			 msg = "Su solicitud fue cancelada";
		 } else{
			 msg = "Su solicitud ha sido recibida, su n\u00famero de tr\u00e1mite es ("
					+ solicitud.getNumeroSolicitud()
					+ "),  el riesgo ser\u00e1 aceptado hasta que sea confirmado por el coordinador";
		 }
			List<String> correos = new ArrayList<String>();
			for(String correo: solicitud.getEmailContactos().split(";")){
				correos.add(correo);
			}
			mailService.sendMail(correos,
					TITULO_CORREO_CREACION + solicitud.getNumeroSolicitud(),
					msg, null,
					TITULO_CORREO_CREACION + solicitud.getNumeroSolicitud(),
					SALUDO_CORREO_CREACION);

		}
	}
	
	private void  sendEmailCustom(SolicitudDTO solicitud,String message,String title,String subject, String greeting){
		
		if (solicitud.getEmailContactos() != null
				&& solicitud.getEmailContactos().length() > 0) {
			if (greeting == null) {
				greeting = SALUDO_CORREO_CREACION;
			}
			List<String> address = new ArrayList<String>();
			for (String correo : solicitud.getEmailContactos().split(";")) {
				address.add(correo);
			}
			mailService.sendMail(address, title, message, null, subject,
					greeting);
		}
		}
	
	@Override
	public BigDecimal cancelarSolicitud(BigDecimal idToSolicitud){
		SolicitudDTO solicitud = entidadService.findById(SolicitudDTO.class, idToSolicitud);
		solicitud.setClaveEstatus(SolicitudDTO.Estatus.CANCELADA.getEstatus());
		this.enviaCorreoCreacion(solicitud);
		Usuario usuario = usuarioService.getUsuarioActual();
		solicitud.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
		this.setValoresFuerzaDeVenta(solicitud);
		solicitud = solicitudFacadeRemote.update(solicitud);
		estadisticasEstadoFacadeRemote.registraEstadistica(solicitud, SolicitudDTO.ACCION.CANCELACION);		
		return idToSolicitud;
	}
	
	@EJB
	public void setEstadisticasEstadoFacadeRemote(
			EstadisticasEstadoFacadeRemote estadisticasEstadoFacadeRemote) {
		this.estadisticasEstadoFacadeRemote = estadisticasEstadoFacadeRemote;
	}
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	@EJB
	public void setEjecutivoService(EjecutivoService ejecutivoService) {
		this.ejecutivoService = ejecutivoService;
	}
}
