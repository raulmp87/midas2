package mx.com.afirme.midas2.service.impl.catalogos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.com.afirme.midas2.dao.catalogos.PaqueteDao;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.service.catalogos.PaqueteService;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;

import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PaqueteServiceImpl implements PaqueteService {

	@EJB
	private EntidadService entidadService;
	@EJB
	private ParametroGeneralService parametroGeneralService;

	public List<Paquete> findByFilters(Paquete filtroPaquete) {
		if(filtroPaquete==null)filtroPaquete = new Paquete();
		return this.agregarDescripcionTipoSeguro(paqueteDao.findByFilters(filtroPaquete));
	}

	private PaqueteDao paqueteDao;
	
	@EJB
	public void setPaqueteDao(PaqueteDao paqueteDao) {
		this.paqueteDao = paqueteDao;
	}
	
	public List<Paquete> listAll() {
		List<Paquete> paqueteList = entidadService.findAll(Paquete.class);
		return this.agregarDescripcionTipoSeguro(paqueteList);
	}

	public Paquete prepareVerDetalle(Long id) {
		Paquete paquete  = entidadService.findById(Paquete.class, id);
		paquete = this.agregarDescripcionTipoSeguro(paquete);
		return paquete;
	}

	public boolean guardar(Paquete paquete){
		if(paquete!=null){
			try{
				entidadService.save(paquete);
				return true;
			}catch(Exception e){
				return false;
			}
		}else{
			return false;
		}
	}

	private Paquete agregarDescripcionTipoSeguro(Paquete paqueteParam){
		List<ParametroGeneralDTO> parametroGeneralDTOList = parametroGeneralService.listByGroup("Tipo de Seguro");
		if(paqueteParam != null){ 
			if(paqueteParam.getClaveAmisTipoSeguro() != null && paqueteParam.getClaveAmisTipoSeguro() > 0){
				paqueteParam.setDescripcionAmisTipoSeguro("-");
				for(ParametroGeneralDTO parametroG : parametroGeneralDTOList){
					if(parametroG.getValor().equals(("" + paqueteParam.getClaveAmisTipoSeguro()))){
						paqueteParam.setDescripcionAmisTipoSeguro(paqueteParam.getClaveAmisTipoSeguro() + " - " + parametroG.getDescripcionParametroGeneral());
						break;
					}
				}
			} else {
				paqueteParam.setDescripcionAmisTipoSeguro("Es requerido homologar el Catálogo.");
			}
		} 
		return paqueteParam;
	}
	private List<Paquete> agregarDescripcionTipoSeguro(List<Paquete> paqueteListParam){
		for(int c=0; c<paqueteListParam.size(); c++){
			paqueteListParam.get(c).setDescripcionAmisTipoSeguro(agregarDescripcionTipoSeguro(paqueteListParam.get(c)).getDescripcionAmisTipoSeguro());
		}
		return paqueteListParam;
	}
}
