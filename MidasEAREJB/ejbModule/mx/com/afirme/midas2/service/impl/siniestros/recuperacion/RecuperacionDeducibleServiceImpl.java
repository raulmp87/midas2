package mx.com.afirme.midas2.service.impl.siniestros.recuperacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.ClaveSubCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoEstimacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CoberturaRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.PaseRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.MedioRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.OrigenRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.TipoOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.TipoRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionDeducible;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ReferenciaBancaria;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.ValuacionHgs;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.siniestros.EstimacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EnumCodigo;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionDeducibleService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.hgs.HgsService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.log4j.Logger;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class RecuperacionDeducibleServiceImpl extends RecuperacionServiceImpl implements RecuperacionDeducibleService{

	public static final Logger LOG = Logger
	.getLogger(RecuperacionDeducibleServiceImpl.class);
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private RecuperacionService recuperacionService;
	
	@EJB
	private EnvioNotificacionesService envioNotificacionesService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private EntidadBitemporalService entidadBitemporalService;
	
	@EJB
	private PolizaSiniestroService polizaSiniestroService;  
		
	@EJB
	private ParametroGlobalService parametroGlobalService;
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@EJB
	private MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote;	
	
	@EJB
	protected EstiloVehiculoFacadeRemote estiloVehiculoFacadeRemote;
	
	@EJB
	HgsService hgsService;	
	
	@EJB
	BitacoraService bitacoraService;
	
	private BigDecimal porcentajeDeducibleValorComercial;
	
	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private IngresoService ingresoService;
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
	NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
	
	public enum CONCEPTO_ANTIGUEDAD{
		ANTIGUEDAD_CREACION,
		ANTIGUEDAD_COBRO		
	}
	
	/**
	 * 
	 * Servicio que actualizará el estatus de la recuperación.
	 * 
	 * Deberá validar si la cobertura del pase de atención tiene deducible. Si lo
	 * tiene deberá hacer lo siguiente:
	 * 
	 * Para Inactivar:
	 * <ul>
	 * 	<li>El Ajuste realizado a la Reserva es identificado como "Ajuste de Menos" y
	 * la Reserva de la Cobertura relacionada es igual a cero; $ 0.00.</li>
	 * </ul>
	 * <ul>
	 * 	<li>El Estatus de la Recuperación de Deducible relacionada al Pase de Atención
	 * relacionado a la Cobertura es "<b>Pendiente por Recuperar</b>" o
	 * "<b>Registrado</b>".</li>
	 * </ul>
	 * 
	 * Para Activar:
	 * <ul>
	 * 	<li>El Ajuste realizado a la Reserva es identificado como "Ajuste de Más" y la
	 * Reserva de la Cobertura relacionada es mayor a cero; $ 0.00.</li>
	 * </ul>
	 * <ul>
	 * 	<li>El Estatus de la Recuperación de Deducible relacionada al Pase de Atención
	 * relacionado a la Cobertura es "<b>Inactivo</b>".</li>
	 * </ul>
	 * 
	 * @param reporteId
	 * @param estimacionId
	 * @param reservaActual
	 * @param tipoAjuste
	 */
	@Override
	public void actualizarAjusteReserva(Long estimacionId, BigDecimal reservaActual, String tipoAjuste){
		RecuperacionDeducible recuperacion = obtenerRecuperacionPorEstimacionId(estimacionId);
		if(recuperacion!=null){
			if(tipoAjuste.equalsIgnoreCase(MovimientoCoberturaSiniestro.TipoMovimiento.AJUSTE_DISMINUCION.toString()) && 
					reservaActual.compareTo(BigDecimal.ZERO) == 0){
				inactivarRecuperacion(estimacionId, "Por Ajuste de Menos");				
			}else if(tipoAjuste.equalsIgnoreCase(MovimientoCoberturaSiniestro.TipoMovimiento.AJUSTE_AUMENTO.toString()) && 
					reservaActual.compareTo(BigDecimal.ZERO) > 0){
				activarRecuperacion(estimacionId, "Por Ajuste de Más");
			}							
		}	
	}

	public RecuperacionDeducible obtenerRecuperacionPorEstimacionId(Long estimacionId){
		List<RecuperacionDeducible> deducibles = this.obtenerRecuperacionDeduciblePorEstimacion(estimacionId);
		if(deducibles != null && deducibles.size() > 0){
			return deducibles.get(0);
		}
		return null;
	}
	
	public void inactivarRecuperacion(Long estimacionId, String motivo){
		List<RecuperacionDeducible> deducibles = this.obtenerRecuperacionDeduciblePorEstimacion(estimacionId);
		
		for(RecuperacionDeducible recuperacion : CollectionUtils.emptyIfNull(deducibles)){
			if(recuperacion.getEstatus().equalsIgnoreCase(EstatusRecuperacion.REGISTRADO.toString()) ||
					recuperacion.getEstatus().equalsIgnoreCase(EstatusRecuperacion.PENDIENTE.toString())){
				recuperacion.setEstatus(EstatusRecuperacion.INACTIVO.toString());
				recuperacion.setCausaInactivacion(motivo);
				recuperacion.setFechaInactivacion(new Date());
				this.guardar(recuperacion);
				
				if(recuperacion.getIngresoActivo() != null){
					Ingreso ingreso = entidadService.findById(Ingreso.class, recuperacion.getIngresoActivo().getId());
					ingresoService.cancelarIngresoPendientePorAplicar(ingreso, "Por inactivacion de recuperacion de deducible");
				}
			}
		}
	}
	
	public void activarRecuperacion(Long estimacionId, String motivo){
		List<RecuperacionDeducible> deducibles = this.obtenerRecuperacionDeduciblePorEstimacion(estimacionId);
		
		for(RecuperacionDeducible recuperacion : CollectionUtils.emptyIfNull(deducibles)){
			if(recuperacion.getEstatus().equalsIgnoreCase(EstatusRecuperacion.INACTIVO.toString())){
				recuperacion.setEstatus(EstatusRecuperacion.REGISTRADO.toString()); //Revisar estatus				
				recuperacion.setCausaInactivacion(motivo);
				recuperacion.setFechaInactivacion(new Date());			
				this.guardar(recuperacion);
			}
		}		
	}
	

	/**
	 * Si se cambió el campo de Aplica Deducible a "No", y
	 * existe una Recuperación de Deducible, y
	 * el Estatus de la Recuperación sea "<b>Registrado</b>" o "<b>Pendiente por
	 * Recuperar</b>"
	 * <b>Se actualizará el estatus de la Recuperación a "Inactivo".</b>
	 * 
	 * Si se cambió el campo de Aplica Deducible a "Si", y
	 * existe una Recuperación de Deducible, y
	 * el Estatus de la Recuperación sea "<b>Inactivo</b>"
	 * <b>Se actualizará el estatus de la Recuperación a "Registrado".</b>
	 * <b>
	 * </b><b>Se deberán registrar en la entidad Recuperacion los siguientes campos:
	 * </b>
	 * <b>
	 * </b><b>estatus = INACTIVO</b>
	 * <b>causaInactivacion= "</b>Por Modificación en Pase de Atención<b>"</b>
	 * <b>fechaInactivacion=Fecha actual</b>
	 * 
	 * @param reporteId
	 * @param estimacionId
	 * @param aplicaDeducible
	 */
	@Override
	public void actualizarAplicaDeducible(Long reporteId, Long estimacionId, Boolean aplicaDeducible){
		if(aplicaDeducible != null){					
			if(aplicaDeducible){
				activarRecuperacion(estimacionId, "Por Modificación en Pase de Atención");				
			}else{
				inactivarRecuperacion(estimacionId, "Por Modificación en Pase de Atención");
			}
		}
	}

	/**
	 * Deberá validar si existe una recuperacion para el pase de atención valuado.
	 * Deberá obtener la recuperación utilizando el id del pase de atención.
	 * Deberá tomar de la valuación y actualizar en la recuperación lo siguiente.
	 * 
	 * <ul>
	 * 	<li><font color="#0000ff"><b>Tipo de Proceso</b></font></li>
	 * 	<li><font color="#0000ff"><b>Número valuación</b></font></li>
	 * 	<li><font color="#0000ff"><b>Valuador</b></font></li>
	 * 	<li><font color="#0000ff"><b>Fecha Ingreso</b></font></li>
	 * 	<li><font color="#0000ff"><b>Fecha Entrega Vehículo</b></font></li>
	 * </ul>
	 * 
	 * <b>BITÁCORA</b>
	 * <b>
	 * </b>Invocar a BitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.
	 * ACTUALIZAR_RECUPERACION_HGS, recuperacionId, "Se <font
	 * color="#0f0f0f">actualiza la recuperacion " + recuperacionId + de acuerdo a la
	 * valuación " + numValuacion, recuperacion, usuarioService.getUsuarioActual().
	 * getNombreUsuario())</font>
	 * 
	 * @param valuacionHgs
	 */
	@Override
	public void actualizarHGS(ValuacionHgs valuacionHgs){
		
		List<RecuperacionDeducible> resultados = this.obtenerRecuperacionDeduciblePorEstimacion(valuacionHgs.getPaseatencionId());
		
		if(!resultados.isEmpty())
		{
			RecuperacionDeducible recuperacion = resultados.get(0);
			
			if(valuacionHgs.getClaveTipoProceso() != null)
			{
				CatValorFijo catalogo = catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_PROCESO_VALUACION,
						String.valueOf(valuacionHgs.getClaveTipoProceso())); 
				recuperacion.setTipoProceso(catalogo.getDescripcion());				
			}
			
			if(valuacionHgs.getValuacionHgs() != null)
			{
				recuperacion.setNumeroValuacion(String.valueOf(valuacionHgs.getValuacionHgs()));				
			}	
			
			recuperacion.setNombreValuador(valuacionHgs.getValuadorNombre());
			recuperacion.setFechaIngreso(valuacionHgs.getFechaIngresoTaller());
			recuperacion.setFechaEntrega(valuacionHgs.getFechaTerminacion());	
			
			Usuario usuario = new Usuario();
			usuario.setNombreUsuario("SINUSHGS");
			this.usuarioService.setUsuarioActual(usuario);
			
			this.guardar(recuperacion);
		}

	}

	/**
	 * Se deberá identificar si el pase de atención recibido tiene Recuperación de
	 * Deducible en estatus "PROCESADA", y se deberá cambiar al estatus "PENDIENTE POR
	 * RECUPERAR".
	 * 
	 * <b>BITÁCORA</b>
	 * <b>
	 * </b>Invocar a BitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.
	 * ACTUALIZAR_RECUPERACION, recuperacionId, "Se <font color="#0f0f0f">actualiza la
	 * recuperacion " + recuperacionId + de acuerdo a la valuación " + numValuacion,
	 * recuperacion, usuarioService.getUsuarioActual().getNombreUsuario())</font>
	 * 
	 * @param estimacionId
	 */
	@Override
	public void actualizarPendienteRecuperar(Long reporteId, Long estimacionId){
		
		List<RecuperacionDeducible> resultados = this.obtenerRecuperacionDeduciblePorEstimacion(estimacionId);
		
		if(!resultados.isEmpty())
		{
			RecuperacionDeducible recuperacion = resultados.get(0);
			if(recuperacion.getEstatus().equalsIgnoreCase(EstatusRecuperacion.REGISTRADO.toString())) //TODO revisar el estatus
			{
				recuperacion.setEstatus(EstatusRecuperacion.PENDIENTE.toString());	
				this.guardar(recuperacion);
			}
		}
		

	}

	/**
	 * Este servicio ejecutará la cancelación de una recuperación de deducible.
	 * 
	 * Deberá invocar el servicio
	 * 
	 * RecuperacionService.cancelarRecuperacion(Long recuperacionId, String motivo).
	 * 
	 * Deberá invocar la notificación de cancelación invocando el método
	 * notificarCancelacion(RecuperacionDeducible recuperacion).
	 * 
	 * @param recuperacionId
	 * @param motivo
	 */
	@Override
	public void cancelar(Long recuperacionId, String motivo){		
		
		recuperacionService.cancelarRecuperacion(recuperacionId, motivo);
		
		List<RecuperacionDeducible> resultados = entidadService.findByProperty(RecuperacionDeducible.class, "id", recuperacionId);
		if(!resultados.isEmpty())
		{
			this.notificarCancelacion(resultados.get(0));			
		}		
	}

	/**
	 * Generar/actualizar una nueva Recuperación de Deducible.
	 * -Recibirá el id de reporte y el del pase de atención.
	 * 
	 * Deberá validar si la cobertura del pase de atención tiene deducible. Si lo
	 * tiene deberá hacer lo siguiente:
	 * 
	 * -Obtendrá:
	 * 
	 * <b>Recuperación Genérica</b>
	 * numero - Consecutivo de Recuperaciones.
	 * fechaCreacion - Fecha actual.
	 * tipo - "DEDUCIBLE"
	 * medio - "REEMBOLSO"
	 * estatus - "REGISTRADO"
	 * origen -  "AUTOMATICA"
	 * 
	 * <b>Recuperación Deducible</b>
	 * estimacionCobertura - Del pase de atención
	 * tipoOrdenesCompra - "Afectación de la Reserva"
	 * afectado - Beneficiario del Pase de Atención.
	 * marcaId - De acuerdo al Pase de Atención relacionado. De la entidad de
	 * AutoIncisoReporteCabina.
	 * estiloId - De acuerdo al Pase de Atención relacionado. De la entidad de
	 * AutoIncisoReporteCabina.
	 * modeloVehiculo - De acuerdo al Pase de Atención relacionado. De la entidad de
	 * AutoIncisoReporteCabina.
	 * tipoDeducible - Se obtiene de CoberturaReporteCabina.<font
	 * color="#0000c0">claveTipoDeducible</font>
	 * porcentajeDeducible -
	 * monto de Deducible -
	 * taller -
	 * 
	 * Se deberán crear las referencias bancarias correspondientes a la Recuperación
	 * invocando el servicio RecuperacionService.generarReferenciaBancaria.
	 * 
	 * 
	 * Deberá invocar el método notificarClienteDeducible(RecuperacionDeducible).
	 * 
	 * @param reporteId
	 * @param estimacionId
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void generar(Long reporteId, Long estimacionId, BigDecimal montoDeducible,BigDecimal montoFinalDeducible){
		RecuperacionDeducible recuperacionDeducible = null;
		EstimacionCoberturaReporteCabina estimacionCobertura = entidadService.findById(EstimacionCoberturaReporteCabina.class, estimacionId);		 					
		if( estimacionCobertura != null ){		
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("estimacionId", estimacionId);
			parameters.put("origen", RecuperacionDeducible.Origen.PASE.getValue());
			
			List<RecuperacionDeducible> lRecuperacionDeducible = entidadService.executeQueryMultipleResult(
					"SELECT model FROM RecuperacionDeducible model WHERE model.estatus <> 'CANCELADO' and model.estimacionCobertura.id = :estimacionId and model.origenDeducible = :origen ", 
					parameters); 
			if( lRecuperacionDeducible.isEmpty() & estimacionCobertura.getAplicaDeducible() ){ 
				
				ReporteCabina reporteCabina = entidadService.findById(ReporteCabina.class, reporteId);
				recuperacionDeducible = new RecuperacionDeducible();
				recuperacionDeducible.setActivo(Boolean.TRUE);
				recuperacionDeducible.setNumero(this.entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO));
				recuperacionDeducible.setTipo(TipoRecuperacion.DEDUCIBLE.toString());
				recuperacionDeducible.setMedio(MedioRecuperacion.REEMBOLSO.toString());
				recuperacionDeducible.setEstatus(EstatusRecuperacion.REGISTRADO.toString());
				recuperacionDeducible.setOrigen(OrigenRecuperacion.AUTOMATICA.toString());	
				recuperacionDeducible.setTipoOrdenCompra(TipoOrdenCompra.AFECTACION.toString());
				
				recuperacionDeducible.setReporteCabina(reporteCabina);
				recuperacionDeducible.setEstimacionCobertura(estimacionCobertura);
				recuperacionDeducible.setTipoOrdenCompra(TipoOrdenCompra.AFECTACION.toString());
				recuperacionDeducible.setAfectado(estimacionCobertura.getNombreAfectado());
				Long marcaId = estimacionCobertura.getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getMarcaId() != null ? 
							estimacionCobertura.getCoberturaReporteCabina().getIncisoReporteCabina()
							.getAutoIncisoReporteCabina().getMarcaId().longValue():null;
				recuperacionDeducible.setMarcaId(marcaId);
				recuperacionDeducible.setEstiloId(estimacionCobertura.getCoberturaReporteCabina()
						.getIncisoReporteCabina().getAutoIncisoReporteCabina().getEstiloId());
				Integer modeloVehiculo = estimacionCobertura.getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getModeloVehiculo() != null ? 
							estimacionCobertura.getCoberturaReporteCabina()
							.getIncisoReporteCabina().getAutoIncisoReporteCabina().getModeloVehiculo().intValue() : null;		
				recuperacionDeducible.setModeloVehiculo(modeloVehiculo);	
				String claveTipoDeducible = estimacionCobertura.getCoberturaReporteCabina().getDescripcionDeducible() != null ?
						estimacionCobertura.getCoberturaReporteCabina().getDescripcionDeducible().toString():null;
				recuperacionDeducible.setTipoDeducible(claveTipoDeducible);
				String porcentajeDeducible = estimacionCobertura.getCoberturaReporteCabina().getPorcentajeDeducible() != null ?
						estimacionCobertura.getCoberturaReporteCabina().getPorcentajeDeducible().toString() : null;
				recuperacionDeducible.setPorcentajeDeducible(porcentajeDeducible);
				recuperacionDeducible.setMontoDeducible(montoDeducible);		
				
					recuperacionDeducible.setMontoFinalDeducible(montoFinalDeducible);
				//Nota: Taller de acuerdo a comentado por Arturo se llena hasta que llega la informacion de valuacion de HGS
				
				recuperacionDeducible = this.guardar(recuperacionDeducible);
				
				CoberturaRecuperacion coberturaRecuperacion = new CoberturaRecuperacion();
				coberturaRecuperacion.setCoberturaReporte(estimacionCobertura.getCoberturaReporteCabina());
				coberturaRecuperacion.setClaveSubCalculo(estimacionCobertura.getCoberturaReporteCabina().getClaveTipoCalculo());     	    
			    coberturaRecuperacion.setRecuperacion(recuperacionDeducible);
			    entidadService.save(coberturaRecuperacion);				    
			    
			    PaseRecuperacion paseRecuperacion = new PaseRecuperacion(coberturaRecuperacion, estimacionCobertura);	
			    entidadService.save(paseRecuperacion);		 
			 				
				recuperacionService.generarReferenciaBancaria(recuperacionDeducible.getId()); 
				notificarClienteDeducible(recuperacionDeducible);
			
			}else{			
				if( !lRecuperacionDeducible.isEmpty() ){
					//si la recuperacion ya existe y el monto de ducible es diferente entonces 
					//se debe actualizar el campo y regenerar las referencias
					recuperacionDeducible = lRecuperacionDeducible.get(0);
					BigDecimal montoDeducibleActual = recuperacionDeducible.getMontoFinalDeducible();
					if(montoDeducibleActual.longValue() != montoFinalDeducible.longValue()){
						lRecuperacionDeducible.get(0).setMontoFinalDeducible(montoFinalDeducible);
						recuperacionService.generarReferenciaBancaria(recuperacionDeducible.getId()); 
						this.guardar(lRecuperacionDeducible.get(0));
						notificarClienteDeducible(recuperacionDeducible);
					}
				}				
			}
		}		
	}

	/**
	 * 
	 * @param recuperacion
	 */
	@Override
	public RecuperacionDeducible guardar(RecuperacionDeducible recuperacion){
		
		Bitacora.EVENTO evento;
		Long estimacionId;
		
		if(recuperacion.getId() == null)
		{
			recuperacion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
			evento = EVENTO.CREAR_RECUPERACION;
		}
		else
		{			
			recuperacion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			recuperacion.setFechaModificacion(new Date());	
			evento = EVENTO.EDITAR_RECUPERACION;
		}

		recuperacion = entidadService.save(recuperacion);
		
		estimacionId = recuperacion.getEstimacionCobertura().getId();
		
		bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, evento, recuperacion.getId().toString(), 
				 String.format("Se crea/actualiza la recuperación [ %1$s ] de acuerdo a la valuación [ %2$s ]", 
						 String.valueOf(recuperacion.getNumero()), 
						 String.valueOf(estimacionId)), 
				 recuperacion.toString(), usuarioService.getUsuarioActual().getNombreUsuario());
		
		
		return recuperacion;
	}

	/**
	 * Se enviará una notificación previamente configurada en el módulo de
	 * notificaciones.
	 * 
	 * Se invocará el servicio notificacionesService
	 * 
	 * @param recuperacion
	 */
	private void notificarCancelacion(RecuperacionDeducible recuperacion){

		HashMap<String, Serializable> parametros = new HashMap<String, Serializable>();		
		parametros.put("numeroRecuperacion", recuperacion.getNumero());
		parametros.put("numeroSiniestro", recuperacion.getReporteCabina().getSiniestroCabina() != null ? 
				recuperacion.getReporteCabina().getSiniestroCabina().getNumeroSiniestro():null);
		parametros.put("nombreCobertura", recuperacion.getEstimacionCobertura().getCoberturaReporteCabina()
				.getCoberturaDTO().getNombreComercial());
		parametros.put("numeroPaseAtencion", recuperacion.getEstimacionCobertura().getId());
		parametros.put("montoDeducible", "\\" + currencyFormatter.format(recuperacion.getMontoFinalDeducible()));
		parametros.put("fechaCancelacion", simpleDateFormat.format(recuperacion.getFechaCancelacion()));
		parametros.put("usuarioCancelacion", recuperacion.getCodigoUsuarioCancelacion()); 
		parametros.put("motivoCancelacion", recuperacion.getMotivoCancelacion());
		parametros.put("directorioImagenes", sistemaContext.getDirectorioImagenes());
		parametros.put(EnvioNotificacionesService.ATR_OFICINAID, recuperacion.getReporteCabina().getOficina().getId());
		parametros.put(EnvioNotificacionesService.ATR_REPORTE, recuperacion.getReporteCabina());				
		envioNotificacionesService.enviarNotificacionSiniestros(EnumCodigo.NOTIF_CANCELA_DEDUCIBLE.toString(), parametros);
	}

	/**
	 * Se enviará una notificación previamente configurada en el módulo de
	 * notificaciones al cliente.
	 * 
	 * Deberá configurarse de acuerdo a la siguiente RDN:
	 * 
	 * @param recuperacion
	 */
	private void notificarClienteDeducible(RecuperacionDeducible recuperacion){ //TODO manejar Exception
		
		try{
		
			BitemporalCotizacion cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, 
					CotizacionContinuity.BUSINESS_KEY_NAME, recuperacion.getReporteCabina().getPoliza().getCotizacionDTO().getIdToCotizacion(), 
															TimeUtils.getDateTime(recuperacion.getFechaCreacion()));
			Map<String, Serializable> parametros = new HashMap<String, Serializable>();	
			//datos configuracion
			parametros.put("nombreAsegurado", recuperacion.getEstimacionCobertura().getNombreAfectado());
			parametros.put("interesado", recuperacion.getEstimacionCobertura().getNombreAfectado());
			parametros.put("emailInteresado", recuperacion.getEstimacionCobertura().getEmail());
			parametros.put("numeroSiniestro", recuperacion.getReporteCabina().getSiniestroCabina() != null ? 
					recuperacion.getReporteCabina().getSiniestroCabina().getNumeroSiniestro():null);
			parametros.put("fechaSiniestro", simpleDateFormat.format(recuperacion.getReporteCabina().getFechaHoraOcurrido()));
			parametros.put("montoDeducible", "\\" + currencyFormatter.format(recuperacion.getMontoFinalDeducible()));
			parametros.put("tipoDeducible", recuperacion.getTipoDeducible());
			parametros.put("oficinaId", recuperacion.getReporteCabina().getOficina().getId());
			
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("recuperacion.id", recuperacion.getId());
			List<ReferenciaBancaria> referencias = entidadService.findByPropertiesWithOrder(ReferenciaBancaria.class, params, "consecutivo DESC");			
			parametros.put("referencia", referencias.size() != 0 ? referencias.get(0).getReferencia():null);			
			parametros.put("nombreAjustador", recuperacion.getReporteCabina().getAjustador() != null ? 
					recuperacion.getReporteCabina().getAjustador().getNombrePersonaAjustador() : null);
			parametros.put("telefonoAjustador",recuperacion.getReporteCabina().getAjustador() != null ?
					recuperacion.getReporteCabina().getAjustador().getPersonaMidas().getContacto().getTelCelularCompleto() : null);
			parametros = envioNotificacionesService.cargarParametrosGeneralesSiniestros(recuperacion.getReporteCabina(), parametros); 
			
			Map<String, String> mapInlineImages = new HashMap<String, String>();
			mapInlineImages.put("image1", sistemaContext.getDirectorioImagenesParametroGlobal() +"Cintillo_Seguros.jpg");
						
			envioNotificacionesService.enviarNotificacion(EnumCodigo.NOTIF_CLIENTE_DEDUCIBLE.toString(), 
					parametros, null, 
					null, 
					super.generarPDFInstrucctivoPago(recuperacion.getId()),
					mapInlineImages);
		
		}catch(Exception ex){
			LOG.error("No fue posible enviar la notificacion de deducible al cliente ", ex);
		}
	}

	/**
	 * Se deberá obtener en base a un id de recuperación todo el detalle de la
	 * recuperación de deducible.
	 * 
	 * Se deberá obtener/generar los siguientes campos:
	 * 
	 * <font color="#0000ff">tipoSiniestro </font>- Tipo de Siniestro relacionado al
	 * Pase de Atención de acuerdo al Siniestro. AutoIncisoReporteCabina.
	 * causaSiniestro
	 * <font color="#ff0000">Se obtendrá la descripción del valor fijo con la clave
	 * CAUSA_SINIESTRO.</font>
	 * 
	 * <font color="#0000ff">tipoProceso</font> - Proceso relacionado a la Valuación
	 * del Pase de Atención de acuerdo al Siniestro.
	 * <font color="#0f0f0f">S</font>e obtendrá el valor tal cual aparece en la tabla.
	 * 
	 * 
	 * <font color="#0000ff">numValuacion </font>- Número de Valuación HGS relacionado
	 * al Pase de Atención de acuerdo al Siniestro.
	 * <font color="#0f0f0f">S</font>e obtendrá el valor tal cual aparece en la tabla.
	 * 
	 * 
	 * <font color="#0000ff">fechaIngreso </font>- Fecha de ingreso del vehículo a
	 * taller.
	 * <font color="#0f0f0f">S</font>e obtendrá el valor tal cual aparece en la tabla.
	 * 
	 * 
	 * <font color="#0000ff">fechaEntrega</font> - Fecha de entrega del vehiculo al
	 * cliente.
	 * <font color="#0f0f0f">S</font>e obtendrá el valor tal cual aparece en la tabla.
	 * 
	 * 
	 * <font color="#0000ff">nombreValuador </font>- Nombre del valuador del siniestro.
	 * 
	 * <font color="#0f0f0f">Se deberá verificar si este campo se encuentra vacío, de
	 * ser así, se deberá obtener de </font>ValuacionHgs.valuadorNombre.
	 * 
	 * <font color="#0000ff">tiempoCreacion</font> - Días Transcurridos desde la
	 * Creación de la Recuperación.
	 * nDias = fechaActual - fechaCreacion
	 * 
	 * <font color="#0000ff">antiguedadCobro</font> - Días Transcurridos desde que se
	 * genero el Ingreso con estatus "Ingreso Posible"
	 * nDias = fechaActual - fechaIngreso
	 * 
	 * <font color="#0000ff">afectado</font> - Afectado del siniestro. Se obtiene de
	 * EstimacionCoberturaReporteCabina.nombreAfectado.
	 * 
	 * <font color="#0000ff">taller</font> - Taller indicado en el Pase de Atención
	 * relacionado. Se obtiene de la entidad TerceroDanosMateriales.taller.
	 * personaMidas.nombre.
	 * 
	 * <font color="#0000ff">marcaId</font> - Marca del Vehículo involucrado en el
	 * Pase de Atención. Se obtiene de AutoIncisoReporteCabina.marcaId
	 * 
	 * <font color="#0000ff">marcaDesc</font> - Descripción de la marca del vehiculo.
	 * <font color="#ff0000">Se obtiene de MarcaVehiculoDTO donde marcaId =
	 * idTcMarcaVehiculo</font>
	 * 
	 * <font color="#0000ff">estiloId</font> - Estilo del Vehículo involucrado en el
	 * Pase de Atención. Se obtiene de AutoIncisoReporteCabina.estiloId
	 * 
	 * <font color="#0000ff">estiloDesc</font> - Descripción del estilo del vehiculo.
	 * 
	 * <font color="#ff0000">Se debe crear un objeto EstiloVehiculoId con estiloId y
	 * buscarlo en la entidad EstiloVehiculoDTO. La descripción es el atributo
	 * descripcionEstilo de esta entidad.</font>
	 * 
	 * <font color="#0000ff">modeloVehiculo </font>- Modelo del Vehículo involucrado
	 * en el Pase de Atención. Se obtiene de AutoIncisoReporteCabina.modeloVehiculo.
	 * 
	 * tipoDeducible - Tipo de cálculo de deducible.
	 * <font color="#0f0f0f">S</font>e obtendrá el valor tal cual aparece en la tabla.
	 * 
	 * 
	 * porcentajeDeducible -
	 * <font color="#0f0f0f">S</font>e obtendrá el valor tal cual aparece en la tabla.
	 * 
	 * Valor del porcentaje de deducible. CoberturaReporteCabina.<font
	 * color="#0000c0">porcentajeDeducible</font>
	 * 
	 * Monto de Deducible - Monto del deducible configurado en el pase de atención.
	 * <font color="#0f0f0f">S</font>e obtendrá el valor tal cual aparece en la tabla.
	 * 
	 * Monto de Deducible identificado en el Pase de Atención. CoberturaReporteCabina.
	 * <font color="#0000c0">valorDeducible</font>
	 * 
	 * Monto Final Deducible - Monto Final.
	 * <font color="#0f0f0f">S</font>e obtendrá el valor tal cual aparece en la tabla.
	 * 
	 * Númerico Moneda - ($ 00,000.00) - Monto de Deducible identificado en el Pase de
	 * Atención al definir la Recuperación. - Solo Lectura
	 * 
	 *    Cancelado por - Alfanumérico - Usuario que canceló la Recueración - Solo
	 * Lectura**
	 * 
	 *    Motivo de Cancelación - Alfanumérico - Motivo de Cancelación de la
	 * Recuperación - Solo Lectura 
	 * 
	 * @param recuperacionId
	 */
	@Override
	public RecuperacionDeducible obtener(Long recuperacionId){
		     
		RecuperacionDeducible recuperacion = entidadService.findByProperty(RecuperacionDeducible.class,"id",recuperacionId).get(0);
		
		EstimacionCoberturaReporteCabina estimacion = recuperacion.getEstimacionCobertura();		
		
		CatValorFijo catalogo = catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.CAUSA_SINIESTRO, 
				estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getCausaSiniestro());
		
		recuperacion.setTipoSiniestro(catalogo.getDescripcion());			
		
		recuperacion.setAntiguedadCreacion(obtenerDiasTranscurridosConcepto(recuperacion, CONCEPTO_ANTIGUEDAD.ANTIGUEDAD_CREACION));	
		recuperacion.setAntiguedadCobro(obtenerDiasTranscurridosConcepto(recuperacion, CONCEPTO_ANTIGUEDAD.ANTIGUEDAD_COBRO));	

		
		if(recuperacion.getMarcaId() != null) 
		{
			recuperacion.setDescripcionMarca(
			marcaVehiculoFacadeRemote.findById(recuperacion.getMarcaId().doubleValue())
			.getDescripcionMarcaVehiculo());			
		}
		   
		if(!StringUtil.isEmpty(recuperacion.getEstiloId()))
		{
			String[] estiloId = null;
			estiloId = recuperacion.getEstiloId().split("_");
			EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(estiloId[0], estiloId[1],
						BigDecimal.valueOf(Long.valueOf(estiloId[2]))); 
			EstiloVehiculoDTO estilo = estiloVehiculoFacadeRemote.findById(estiloVehiculoId);

			recuperacion.setDescripcionEstilo(estilo.getDescripcionEstilo());
		}
		
		Map<String,Object> datosVehiculo = hgsService.obtenerDatosVehiculo(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina()
				.getReporteCabina(), estimacion.getId());
		recuperacion.setTaller(String.valueOf(datosVehiculo.get("nombreTaller")));		
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("recuperacion.id", recuperacion.getId());
		List<ReferenciaBancaria> referencias = entidadService.findByPropertiesWithOrder(ReferenciaBancaria.class, params , "consecutivo DESC");
		if(!referencias.isEmpty())
		{
			recuperacion.setReferencia(referencias.get(0).getReferencia());			
		}		
		   
		return recuperacion;
	}

	/**
	 * Se implementará RDN: Generar Cálculo de Días Transcurridos.
	 * 
	 * Se recibirá la recuperacion y el concepto antiguedad que se desea calcular y se retornarán los días transcurridos 
	 * 
	 * @param recuperacion La entidad recuperacion para obtener los datos requeridos para el calculo
	 * @param concepto valor del enum de los conceptos permitidos por el calculo
	 * 
	 */
	private Integer obtenerDiasTranscurridosConcepto(RecuperacionDeducible recuperacion, CONCEPTO_ANTIGUEDAD concepto)
	{
		Double difDias = null;
		
		switch(concepto)
		{
			case ANTIGUEDAD_CREACION:
				
				if(recuperacion.getEstatus().equalsIgnoreCase(EstatusRecuperacion.RECUPERADO.toString()) 
						&& recuperacion.getFechaAplicacion() != null)
				{
					difDias = DateUtils.dateDiff(recuperacion.getFechaAplicacion()
							,recuperacion.getFechaCreacion(), DateUtils.Indicador.Days);
					recuperacion.setAntiguedadCreacion(difDias.intValue());	
					
				}else
				{
					difDias = DateUtils.dateDiff(Calendar.getInstance().getTime()
							,recuperacion.getFechaCreacion(), DateUtils.Indicador.Days);
					recuperacion.setAntiguedadCreacion(difDias.intValue());					
				}
					
				break;
		
			case ANTIGUEDAD_COBRO:			
				
				if(recuperacion.getIngresoActivo() != null && recuperacion.getIngresoActivo().getFechaCreacion() != null)
				{
					Date fechaCreacionIngreso = recuperacion.getIngresoActivo().getFechaCreacion();
					
					if(recuperacion.getEstatus().equalsIgnoreCase(EstatusRecuperacion.RECUPERADO.toString()) 
							&& recuperacion.getFechaAplicacion() != null)
					{
						difDias = DateUtils.dateDiff(recuperacion.getFechaAplicacion()
								,fechaCreacionIngreso, DateUtils.Indicador.Days);
						recuperacion.setAntiguedadCreacion(difDias.intValue());	
						
					}else
					{
						difDias = DateUtils.dateDiff(Calendar.getInstance().getTime()
								,fechaCreacionIngreso, DateUtils.Indicador.Days);
						recuperacion.setAntiguedadCreacion(difDias.intValue());					
					}
				}		
				
				break;		
		}		
		
		return difDias != null ? difDias.intValue(): null;
	}	

	
	public BigDecimal calcularDeducible(ReporteCabina reporteCabina,String tipoPaseAtencion){
		
		BigDecimal monto = BigDecimal.ZERO;
		String clvTipoCalculo;
		Map<String,String> mEquivalencias = new HashMap<String,String>();
		String[] aClvTipoCalculo = null;
		
		
		//         PASE ATENCION,CLAVE TIPO CALCULO BD   
		mEquivalencias.put("DMA","DM");
		mEquivalencias.put("GME","GMO");
		mEquivalencias.put("GMC","GMO");
		mEquivalencias.put("RCB","RC");
		mEquivalencias.put("RCP","RC");
		mEquivalencias.put("RCV","RC");
		mEquivalencias.put("RCJ","RC");
		mEquivalencias.put("ESD","EQE,RCEM,RT");
		
		// OBTENER LLAVE
		clvTipoCalculo = mEquivalencias.get(tipoPaseAtencion);
		
		// SI EL tipoPaseAtencion ES ESD SE DEBE PARTIR LA LLAVE Y PASARLA A UN ARREGLO
		if( clvTipoCalculo != null && clvTipoCalculo.equals("EQE,RCEM,RT")  ){
			aClvTipoCalculo = clvTipoCalculo.split(",");
		}
		
		try{
			if( clvTipoCalculo!= null ){
				if( reporteCabina != null ){
					List<CoberturaReporteCabina> lCoberturaReporteCabina = reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getCoberturaReporteCabina();
					
					if( !lCoberturaReporteCabina.isEmpty() ){
						for(CoberturaReporteCabina lCobertura: lCoberturaReporteCabina){
							
							// ITERA aClvTipoCalculo QUE SON 3 PASES
							if (EnumUtil.equalsValue(TipoEstimacion.DIRECTA, tipoPaseAtencion)) {
								if( aClvTipoCalculo.length > 0 ){
									for( int i = 0; i<=aClvTipoCalculo.length-1; i++  ){
										if( aClvTipoCalculo[i].equals(lCobertura.getClaveTipoCalculo()) ){
											monto = this.validaMontoDeducible(lCobertura.getClaveTipoDeducible(),lCobertura.getValorDeducible(),lCobertura.getIncisoReporteCabina().getAutoIncisoReporteCabina().getCausaSiniestro(), lCobertura.getPorcentajeDeducible(),lCobertura.getValorSumaAsegurada());
											break;
										}
									}
								}
							}else{
								if( lCobertura.getClaveTipoCalculo().equals(clvTipoCalculo) ){
									// OBTENER CLAVE Y CALCULAR MONTO
									monto = this.validaMontoDeducible(lCobertura.getClaveTipoDeducible(),lCobertura.getValorDeducible(),lCobertura.getIncisoReporteCabina().getAutoIncisoReporteCabina().getCausaSiniestro(), lCobertura.getPorcentajeDeducible(),lCobertura.getValorSumaAsegurada());
									break;
								}
							}
						}
					}
				} // if( reporteCabina != null ){
			}else{
				LOG.error("calcularDeducible - ERROR EQUIVALENCIA NO ENCONTRADA EN EL PASE DE ATENCION ");
			}
		}catch(Exception e){
			LOG.error("calcularDeducible ERROR: "+e);
		}
		
		return monto;
	}
	
	
	private BigDecimal validaMontoDeducible(Short clavTipoDeducible,Double valorDeducible, String causaSiniestro, Double porcentajeDeducible, Double valorSumaAsegurada){
		BigDecimal monto = BigDecimal.ZERO;
		monto = this.generarCantidadDeducible(clavTipoDeducible,valorDeducible,causaSiniestro, porcentajeDeducible,valorSumaAsegurada);
		return monto;
	}
	
	
	/****
	 * Se elimina del calculo cuando es cristales ya que no se cuenta con el pase de atención
	 * @param clavTipoDeducible
	 * @param valorDeducible
	 * @param causaSiniestro
	 * @param porcentajeDeducible
	 * @param valorSumaAsegurada
	 * @return
	 */
	private BigDecimal generarCantidadDeducible(Short clavTipoDeducible,Double valorDeducible, String causaSiniestro, Double porcentajeDeducible, Double valorSumaAsegurada){
		
		BigDecimal montoDeducible = BigDecimal.ZERO;
		
		if (clavTipoDeducible != null) {
			switch(clavTipoDeducible)
			{
				case  EstimacionCoberturaSiniestroDTO.TIPO_DEDUCIBLE_VALOR: 					
					break;					
				case  EstimacionCoberturaSiniestroDTO.TIPO_DEDUCIBLE_DSMVGDF: //TODO confirmar si aplica la misma regla
					montoDeducible = valorDeducible != null ? new BigDecimal(valorDeducible) : BigDecimal.ZERO;					
					break;
				
				case  EstimacionCoberturaSiniestroDTO.TIPO_DEDUCIBLE_PORCENTAJE: 
					
					BigDecimal porcDeducible;
					BigDecimal montoSA;
					
					porcDeducible = porcentajeDeducible != null ? new BigDecimal(porcentajeDeducible).divide(new BigDecimal(100)) : BigDecimal.ZERO;
					montoSA       = valorSumaAsegurada != null  ? new BigDecimal(valorSumaAsegurada):BigDecimal.ZERO;	
					
					montoDeducible = porcDeducible.multiply(montoSA);
					
					// APLICA PARA CALCULO DESDE EL JS EN DMA
					porcentajeDeducibleValorComercial = porcDeducible;
												
					break;
				case EstimacionCoberturaSiniestroDTO.TIPO_DEDUCIBLE_CARATULA:
					
					break;					
			}
		}
		
		return montoDeducible;
	}
	
	
	public BigDecimal obtenerPorcentajeDeducible(){
		return new BigDecimal( parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS , ParametroGlobalService.PORCENTAJE_DEDUCIBLE_CRISTALES)).divide(new BigDecimal(100) );
	}

	@Override
	public void generar(Long estimacionId,BigDecimal montoFinalDeducible) {
		
		List<RecuperacionDeducible> lRecuperacionDeducible = this.obtenerRecuperacionDeduciblePorEstimacion(estimacionId);
		
		if( !lRecuperacionDeducible.isEmpty() ){
			lRecuperacionDeducible.get(0).setMontoFinalDeducible(montoFinalDeducible);
			this.guardar(lRecuperacionDeducible.get(0));
		}
		
	}

	public BigDecimal obtenerPorcentajeDeducibleValorComercial(){
		return this.porcentajeDeducibleValorComercial;
	}
	
	
	/**
	 * Obtener el monto total de deducible para todo un siniestro, exceptuando la cobertura de RCV
	 * @param reporteId
	 * @return
	 */
	@Override
	public BigDecimal obtenerDeducibleSiniestroNoRCV(Long reporteId){
		BigDecimal deducible = BigDecimal.ZERO;
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id", reporteId);
		params.put("origenDeducible", RecuperacionDeducible.Origen.PASE.getValue());
		List<RecuperacionDeducible> recuperaciones = entidadService.findByProperties(RecuperacionDeducible.class, params);
		
		
		
		for(RecuperacionDeducible rec : CollectionUtils.emptyIfNull(recuperaciones)){	
			String claveSubCalculo = rec.getEstimacionCobertura().getCveSubCalculo();
			if((StringUtil.isEmpty(claveSubCalculo) || 
					(!StringUtil.isEmpty(claveSubCalculo) && 
							!EnumUtil.equalsValue(ClaveSubCalculo.RC_VEHICULO, claveSubCalculo) &&
							!EnumUtil.equalsValue(ClaveSubCalculo.RC_BIENES, claveSubCalculo) &&
							!EnumUtil.equalsValue(ClaveSubCalculo.RC_PERSONA, claveSubCalculo))) && 
							rec.getMontoFinalDeducible() != null && 
							!rec.getEstatus().equals(EstatusRecuperacion.CANCELADO.toString()) &&
							!rec.getEstatus().equals(EstatusRecuperacion.RECUPERADO.toString()) && // se agrega condicion por que en las indemnizacion no se debe considerar los deducibles con estatus reuperado 
							!rec.getEstatus().equals(EstatusRecuperacion.INACTIVO.toString()) ){
				deducible = deducible.add(rec.getMontoFinalDeducible());
			}
		}
		return deducible;
	}
	
	/**
	 * Obtener el monto total de deducible por estimacion
	 * @param reporteId
	 * @return
	 */
	@Override
	public BigDecimal obtenerDeduciblePorEstimacion(Long estimacionId){
		BigDecimal deducible = BigDecimal.ZERO;
		if(estimacionId != null){
			EstimacionCoberturaReporteCabina estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, estimacionId);
			if(estimacion != null){
				
				List<RecuperacionDeducible> recuperaciones = this.obtenerRecuperacionDeduciblePorEstimacion(estimacionId);
				
				for(RecuperacionDeducible recuperacion : CollectionUtils.emptyIfNull(recuperaciones)){
					if(recuperacion.getMontoFinalDeducible() != null&& 
							!recuperacion.getEstatus().equals(EstatusRecuperacion.CANCELADO.toString()) &&
							!recuperacion.getEstatus().equals(EstatusRecuperacion.RECUPERADO.toString()) &&
							!recuperacion.getEstatus().equals(EstatusRecuperacion.INACTIVO.toString()) ){
						deducible = deducible.add(recuperacion.getMontoFinalDeducible());
					}
				}
				
			}
		}
		return deducible;
	}
	
	
	/**
	 * Obtener monto deducible por cobertura y cve sub calculo (RCV, RCP, RCB)
	 * @param idCoberturaReporte
	 * @param cveSubCalculo
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public BigDecimal obtenerDeduciblePorCobertura(Long idCoberturaReporte, String cveSubCalculo){
		BigDecimal monto = BigDecimal.ZERO;		
		if(idCoberturaReporte != null){
			Map<String, Object>  params = new HashMap<String, Object>(1);
			params.put("idCoberturaReporte", idCoberturaReporte);
			params.put("origen", RecuperacionDeducible.Origen.PASE.getValue());
			StringBuilder query = new StringBuilder("SELECT COALESCE(SUM(model.montoFinalDeducible),0) FROM RecuperacionDeducible model WHERE ");
			query.append("model.estimacionCobertura.coberturaReporteCabina.id = :idCoberturaReporte AND model.estatus NOT IN ('CANCELADO', 'INACTIVO','RECUPERADO') ");
			query.append(" AND model.origenDeducible = :origen ");
			if(!StringUtil.isEmpty(cveSubCalculo)){
				query.append(" AND model.estimacionCobertura.cveSubCalculo = :cveSubCalculo ");
				params.put("cveSubCalculo", cveSubCalculo);
			}
			List<BigDecimal> totales =  entidadService.executeQueryMultipleResult(query.toString(), params);
			if(totales != null && totales.size() > 0){
				monto = totales.get(0);
			}
		}
		return monto;
	}
	
	
	/**
	 * Obtener recuperaciones de deducible por estimacion
	 * @param reporteId
	 * @return
	 */
	@Override
	public List<RecuperacionDeducible> obtenerRecuperacionDeduciblePorEstimacion(Long estimacionId){
		if(estimacionId != null){
			EstimacionCoberturaReporteCabina estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, estimacionId);
			if(estimacion != null){
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("estimacionCobertura.id", estimacionId);
				params.put("origenDeducible", RecuperacionDeducible.Origen.PASE.getValue());
				List<RecuperacionDeducible> recuperaciones = entidadService.findByProperties(RecuperacionDeducible.class, params);
				return recuperaciones;				
			}
		}
		return null;
	}
	
	/**
	 * Obtener el monto total por estatus para un siniestro en particular
	 * @param siniestroId
	 * @param estatus
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal obtenerMontoRecuperacionDeducible(Long siniestroId, Recuperacion.EstatusRecuperacion estatus){
		BigDecimal monto = BigDecimal.ZERO;			
		Map<String, Object>  params = new HashMap<String, Object>();
		StringBuilder query = new StringBuilder("SELECT COALESCE(SUM(model.montoFinalDeducible),0) FROM RecuperacionDeducible model WHERE ");
		query.append("model.reporteCabina.siniestroCabina.id = :siniestroId AND model.estatus = :estatus AND model.origenDeducible = :origen");
		params.put("siniestroId", siniestroId);
		params.put("estatus", estatus.toString());
		params.put("origen", RecuperacionDeducible.Origen.PASE.getValue());
		List<BigDecimal> totales =  entidadService.executeQueryMultipleResult(query.toString(), params);
		if(totales != null && totales.size() > 0){
			monto = totales.get(0);
		}
		return monto;
	}

	@Override
	public void actualizarRecuperacion(Long recuperacionId, BigDecimal monto) {
		RecuperacionDeducible recuperacion = entidadService.findById(RecuperacionDeducible.class, recuperacionId);
		if(recuperacion != null && recuperacion.getMontoFinalDeducible().longValue() != monto.longValue()){				
			recuperacion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			recuperacion.setFechaModificacion(new Date());
			bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.EDITAR_RECUPERACION, recuperacion.getId().toString(), 
					 String.format("Se actualiza monto final de la recuperación [ %1$s ] de [ %2$s ] a [ %3$s ]", 
							 String.valueOf(recuperacion.getNumero()), 							 
							 String.valueOf(recuperacion.getMontoFinalDeducible()),
							 String.valueOf(monto)), recuperacion.toString(), 
							 usuarioService.getUsuarioActual().getNombreUsuario());
			recuperacion.setMontoFinalDeducible(monto);
			recuperacion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			recuperacion.setFechaModificacion(new Date());		
			entidadService.save(recuperacion);
			recuperacionService.generarReferenciaBancaria(recuperacion.getId());
			notificarClienteDeducible(recuperacion);
		}	
	} 

}
