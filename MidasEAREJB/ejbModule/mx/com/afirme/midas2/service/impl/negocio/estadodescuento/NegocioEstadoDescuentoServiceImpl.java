package mx.com.afirme.midas2.service.impl.negocio.estadodescuento;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.estadodescuento.NegocioEstadoDescuentoDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.calculo.CalculoDao;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.dto.negocio.estadodescuento.RelacionesNegocioEstadoDescuento;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.estadodescuento.NegocioEstadoDescuentoService;
import mx.com.afirme.midas2.service.negocio.zonacirculacion.NegocioEstadoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

@Stateless 
public class NegocioEstadoDescuentoServiceImpl implements NegocioEstadoDescuentoService{
	private EntidadService entidadService;
	protected EstadoFacadeRemote estadoFacadeRemote;
	private NegocioEstadoService negocioEstadoService;
	private CalculoDao calculoDao;
	private NegocioEstadoDescuentoDao negocioEstadoDescuentoDao;
	private EntidadDao catalogoDao;
	
	protected UsuarioService usuarioService;
	
	@EJB(beanName="UsuarioServiceDelegate")
    public void setUsuarioService(UsuarioService usuarioService) {
          this.usuarioService = usuarioService;
    }	

	@Override
	public RelacionesNegocioEstadoDescuento getRelationLists(Long idToNegocio) {
		List<NegocioEstadoDescuento> negocioEstadoDescuentoAsociadas = null;
		
		List<NegocioEstado> estadosAsociados =  null;
		estadosAsociados = negocioEstadoService.obtenerEstadosPorNegocioId(idToNegocio);
		if(estadosAsociados.isEmpty()){
			negocioEstadoDescuentoAsociadas =  this.getPoridToNegocio(idToNegocio);
		} else {
			negocioEstadoDescuentoAsociadas =  this.getNegocioAndEstadosAsociados(idToNegocio);
		}
		
		RelacionesNegocioEstadoDescuento relacionesNegocioEstadoDescuento = new RelacionesNegocioEstadoDescuento();
		relacionesNegocioEstadoDescuento.setAsociadas(negocioEstadoDescuentoAsociadas);
		return relacionesNegocioEstadoDescuento;
	 }

  	List<NegocioEstadoDescuento> getPoridToNegocio(Long idToNegocio) {
		List<NegocioEstadoDescuento> negocioEstadoDescuento = new ArrayList<NegocioEstadoDescuento>();
		negocioEstadoDescuento = entidadService.findByProperty(
				NegocioEstadoDescuento.class, "negocio.idToNegocio", idToNegocio);

		return negocioEstadoDescuento;

	}
  	
  	@Override
  	public List<NegocioEstadoDescuento> getNegocioAndEstadosAsociados(Long idToNegocio) {
		List<NegocioEstadoDescuento> negocioEstadoDescuento = new ArrayList<NegocioEstadoDescuento>();
		negocioEstadoDescuento = negocioEstadoDescuentoDao.findByNegocioAndEstadosAsociados(idToNegocio);

		return negocioEstadoDescuento;

	}
  	
	@Override
	public Map<Long, String> getNegocioEstadosPorNegocioId(Long idToNegocio){
		Map<Long, String>  map = new LinkedHashMap<Long, String>();
		
		List<NegocioEstadoDescuento> negocioEstadoDescuento = new ArrayList<NegocioEstadoDescuento>();
		negocioEstadoDescuento = entidadService.findByProperty(
				NegocioEstadoDescuento.class, "negocio.idToNegocio", idToNegocio);
		
        for(NegocioEstadoDescuento item :negocioEstadoDescuento){
            map.put(item.getId(), item.getEstadoDTO().getStateName());
        }
        return map;
}	
  	
	@Override
	public Long relacionarNegocio(String accion, NegocioEstadoDescuento negocioEstadoDescuento) {
		String zona = "";
		Double pctDescuentoMax = 0.0;
		String country_id = "PAMEXI";
        List<EstadoDTO> posibles = estadoFacadeRemote.findByProperty("countryId", country_id);
        List<NegocioEstadoDescuento> asociados = this.getPoridToNegocio(negocioEstadoDescuento.getNegocio().getIdToNegocio());
  
        try{
        	if(asociados.size() == 0) {
        		for (EstadoDTO item : posibles) {
            		if (!item.getStateName().equalsIgnoreCase("DESCONOCIDO")) {
            			//tomar zona y descuento por estado-ciudad de la conf. inicial
            			zona = calculoDao.getZonaCirculacion(BigDecimal.ZERO, Integer.valueOf(item.getStateId()),0, null);
                        pctDescuentoMax = calculoDao.getDescuentoMaximoPorEstado(Integer.valueOf(item.getStateId()));
                        NegocioEstadoDescuento negocioEstadoDto = new NegocioEstadoDescuento();
                        negocioEstadoDto.setNegocio(negocioEstadoDescuento.getNegocio());
                        negocioEstadoDto.setEstadoDTO(item);
                        negocioEstadoDto.setZona(zona != null ? zona : "");
                        negocioEstadoDto.setPctDescuento(pctDescuentoMax);
                        negocioEstadoDto.setPctDescuentoDefault(negocioEstadoDescuento.getNegocio().getPctDescuentoDefault() != null ? negocioEstadoDescuento.getNegocio().getPctDescuentoDefault() : 0.0);
                        negocioEstadoDto.setZipCode("");
                        negocioEstadoDto.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
                		entidadService.saveAndGetId(negocioEstadoDto);
            		}
            	}
        	}
        } catch(RuntimeException e){
			return negocioEstadoDescuento.getId();
		}
        
        return null;
	}
	
	@Override
	public Long guardar(String accion, NegocioEstadoDescuento negocioEstadoDescuento) {
		try {
			negocioEstadoDescuento.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			catalogoDao.executeActionGrid(accion, negocioEstadoDescuento);
		} catch(RuntimeException e){
			return negocioEstadoDescuento.getId();
		}
		return null;
	}
	
	@Override
	public List<NegocioEstadoDescuento> obtenerEstadosPorNegocioId(Long idToNegocio) {
			return negocioEstadoDescuentoDao.findByProperty(NegocioEstadoDescuento.class, "negocio.idToNegocio", idToNegocio);
	}
	
	@Override
	public NegocioEstadoDescuento findByNegocioAndEstado(Long idToNegocio, String idToEstado) {
		return negocioEstadoDescuentoDao.findByNegocioAndEstado(idToNegocio, idToEstado);
	}

	@Override
	public String obtenerZipCode(Long idToNegocio, String stateId) {
		return negocioEstadoDescuentoDao.obtenerZipCode(idToNegocio, stateId);
	}

	@EJB
	public void setCatalogoService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setNegocioEstadoDescuentoDao(
			NegocioEstadoDescuentoDao negocioEstadoDescuentoDao) {
		this.negocioEstadoDescuentoDao = negocioEstadoDescuentoDao;
	}

	@EJB
	public void setEstadoFacadeRemote(EstadoFacadeRemote estadoFacadeRemote) {
		this.estadoFacadeRemote = estadoFacadeRemote;
	}

	@EJB
	public void setCalculoDao(CalculoDao calculoDao) {
		this.calculoDao = calculoDao;
	}

	@EJB
	public void setNegocioEstadoService(
			NegocioEstadoService negocioEstadoService) {
		this.negocioEstadoService = negocioEstadoService;
	}
	
	@EJB
	public void setCatalogoDao(EntidadDao catalogoDao) {
		this.catalogoDao = catalogoDao;
	}
}
