package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fuerzaventa.ClienteJPADao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteJPA;
import mx.com.afirme.midas2.service.fuerzaventa.ClienteJPAService;
@Stateless
//@TransactionManagement(TransactionManagementType.CONTAINER)
public class ClienteJPAServiceImpl  implements ClienteJPAService{
	private ClienteJPADao clienteJPADao;
	
	@EJB
	public void setClienteJPADao(ClienteJPADao clienteJPADao) {
		this.clienteJPADao = clienteJPADao;
	}
	
	@Override
	public List<ClienteJPA> findByFilters(ClienteJPA arg0) {
		return clienteJPADao.findByFilters(arg0);
	}

	@Override
	public ClienteJPA loadById(ClienteJPA arg0) {
		return clienteJPADao.loadById(arg0);
	}

	@Override
	public ClienteJPA loadById(Long idCliente) {
		ClienteJPA clienteJPA = new ClienteJPA();
		clienteJPA.setIdCliente(idCliente);
		return loadById(clienteJPA);
	}

}
