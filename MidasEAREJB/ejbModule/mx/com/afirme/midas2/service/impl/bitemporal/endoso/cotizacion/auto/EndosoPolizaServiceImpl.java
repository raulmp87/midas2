package mx.com.afirme.midas2.service.impl.bitemporal.endoso.cotizacion.auto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoPolizaService;

import org.joda.time.DateTime;
import org.joda.time.Days;

import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class EndosoPolizaServiceImpl extends EndosoServiceImpl implements EndosoPolizaService {

	
	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public BitemporalCotizacion getCotizacionEndosoCancelacionPoliza(BigDecimal polizaId,
			Date fechaInicioVigencia, String accionEndoso, Short motivoEndoso)  {
		BitemporalCotizacion bc = this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA, 
				motivoEndoso, accionEndoso);
		return bc;
	}
	
	@Override
	public CotizacionEndosoDTO getCotizacionEndosoRehabilitacionPoliza(BigDecimal polizaId,
			Date fechaInicioVigencia, String accionEndoso, short motivoEndoso){
		
		return getCotizacionEndosoRehabilitacionPoliza(polizaId, fechaInicioVigencia, accionEndoso, 
				motivoEndoso, EmisionPendiente.POLIZA_NO_MIGRADA);		
	}
	
	@Override
	public CotizacionEndosoDTO getCotizacionEndosoRehabilitacionPoliza(BigDecimal polizaId,
			Date fechaInicioVigencia, String accionEndoso, short motivoEndoso, short migrada){
		
		CotizacionEndosoDTO cotizacionEndoso = new CotizacionEndosoDTO();
		ControlEndosoCot controlEndoso = null;
		BitemporalCotizacion cotizacionOriginal = null;
		BitemporalCotizacion cotizacionRehabilitacion = null;
		List<BitemporalCotizacion> cotizaciones = null;
		DateTime rehabilitacionValidaDesde = TimeUtils.getDateTime(fechaInicioVigencia);
		DateTime fechaEmisionCancelacion = null;
		DateTime cancelacionValidaDesde = null;
		DateTime fechaFinPoliza = null;
		PolizaDTO poliza = null;
		
		if (polizaId == null) {
			throw new IllegalArgumentException("[getCotizacionEndosoRehabilitacionPoliza] El id de la póliza es nula.");
		}
		
		//Se setean valores default
		cotizacionEndoso.setAplicaCorrimientoVigencias(false);
		cotizacionEndoso.setOrigenCancelacionAutomatica(false);
		cotizacionEndoso.setRequiereCorrimientoVigencias(0);
		cotizacionEndoso.setNumeroEndoso(this.getNumeroEndoso(polizaId, "%06d"));
		
		//se obtiene la poliza
		poliza = entidadService.findById(PolizaDTO.class, polizaId);
		EndosoDTO endoso = getEndoso(poliza, new Long(0));
		
		if (accionEndoso.equals(TipoAccionDTO.getNuevoEndosoCot())) {
			aplicarValidaciones(endoso, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA, motivoEndoso,migrada);
		}
		
		controlEndoso = getControlEndosoCancelacion(poliza.getCotizacionDTO().getIdToCotizacion(), accionEndoso);
		
		fechaEmisionCancelacion = new DateTime(controlEndoso.getRecordFrom());
		cancelacionValidaDesde = TimeUtils.getDateTime(controlEndoso.getValidFrom());
		cotizacionEndoso.setFechaEmisionCancelacion(fechaEmisionCancelacion);
		cotizacionEndoso.setCancelacionValidaDesde(cancelacionValidaDesde);
		
		CotizacionContinuity cotizacionContinuity = entidadContinuityDao
			.findByProperty(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, 
					poliza.getCotizacionDTO().getIdToCotizacion()).iterator().next();
		
		cotizaciones = (List<BitemporalCotizacion>) entidadBitemporalService
			.obtenerCancelados(BitemporalCotizacion.class, cotizacionContinuity.getId(), cancelacionValidaDesde, fechaEmisionCancelacion);
	
		if (cotizaciones != null && cotizaciones.size() > 0) {
			cotizacionOriginal = cotizaciones.get(0);
		}
		
		if (cotizacionContinuity.getBitemporalProperty().getInProcess() != null) {
			cotizacionRehabilitacion = cotizacionContinuity.getBitemporalProperty().getInProcess();
		} else if (cotizacionContinuity.getBitemporalProperty().get() != null) {
			cotizacionRehabilitacion = cotizacionContinuity.getBitemporalProperty().get();
		} else {
			List<BitemporalCotizacion> cotizacionesEspecial = cotizacionContinuity.getBitemporalProperty().getHistory();
			if (cotizacionesEspecial != null && !cotizacionesEspecial.isEmpty()) {
				cotizacionRehabilitacion = cotizacionesEspecial.get(cotizacionesEspecial.size() - 1);
			}
			if (cotizacionRehabilitacion == null) {
				cotizacionesEspecial = cotizacionContinuity.getBitemporalProperty().getEvolution(rehabilitacionValidaDesde);
				if (cotizacionesEspecial != null && !cotizacionesEspecial.isEmpty()) {
					cotizacionRehabilitacion = cotizacionesEspecial.get(cotizacionesEspecial.size() - 1);
				}
			}
			
		}
		
		fechaFinPoliza = TimeUtils.getDateTime(cotizacionRehabilitacion.getValue().getFechaFinVigencia());
		cotizacionEndoso.setFechaFinPoliza(fechaFinPoliza.toDate());
		cotizacionRehabilitacion = null;
		
		//Se determina si la cancelacion de la poliza fue Automatica	
		if (convierteMotivoEndosoSeycos(controlEndoso.getSolicitud().getClaveMotivoEndoso())
					.equals(SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO)) {
			
			cotizacionEndoso.setOrigenCancelacionAutomatica(true);
			
			if (rehabilitacionValidaDesde.compareTo(cancelacionValidaDesde) > 0) {
									
				int diasCorrimiento = Days.daysBetween(cancelacionValidaDesde, rehabilitacionValidaDesde).getDays();
				cotizacionEndoso.setFechaFinPolizaRecorrida(fechaFinPoliza.plusDays(diasCorrimiento).toDate());
				cotizacionEndoso.setAplicaCorrimientoVigencias(true);
				
				if (accionEndoso.equals(TipoAccionDTO.getEditarEndosoCot()) || accionEndoso.equals(TipoAccionDTO.getConsultarEndosoCot())) {
					//Se determina si requiere corrimiento de vigencia
					cotizacionRehabilitacion = cotizacionOriginal.getEntidadContinuity().getBitemporalProperty()
						.getInProcess(fechaFinPoliza);
					
					if (cotizacionRehabilitacion != null 
							&& cotizacionRehabilitacion.getValidityInterval().getInterval().getEnd().toDate()
							.compareTo(cotizacionEndoso.getFechaFinPolizaRecorrida()) == 0) {
						cotizacionEndoso.setRequiereCorrimientoVigencias(1);
					}
				}
			}
		}
		
		
		return cotizacionEndoso;
	}
	
	/**
	 * Obtiene la cotizacion bitemporal valida para la continuidad y fecha recibidas
	 */
	@Override
	public BitemporalCotizacion prepareBitemporalEndoso(  Long continuityId, 
			 DateTime validoEn) {
		LogDeMidasInterfaz.log("Entrando a ", Level.INFO, null);
		BitemporalCotizacion cotizacion = null;
		
		cotizacion = entidadBitemporalService.getByKeyOld(CotizacionContinuity.class, continuityId, validoEn);
		
		LogDeMidasInterfaz.log("cotizacion => " + cotizacion, Level.INFO, null);
		//LogDeMidasInterfaz.log("incisos => " + cotizacion.getContinuity().getIncisoContinuities().get().size(), Level.INFO, null);
		LogDeMidasInterfaz.log("Saliendo de prepareBitemporalEndoso", Level.INFO, null);
		return  cotizacion;
	}
	
	@Override
	public void guardaCotizacionEndosoCancelacionPoliza(BitemporalCotizacion cotizacion, BigDecimal idToSolicitud, 
			Date fechaInitVigencia) {
		guardaCotizacionEndosoCancelacionPoliza(cotizacion,idToSolicitud,
				fechaInitVigencia, null);
	}
	
	public void guardaCotizacionEndosoCancelacionPoliza(BitemporalCotizacion cotizacion, BigDecimal idToSolicitud, 
			Date fechaInitVigencia, BigDecimal primaTotalIgualar) {
		LogDeMidasInterfaz.log("Entrandoa a guardaCotizacionEndosoCancelacionPoliza", Level.INFO, null);
		
		LogDeMidasInterfaz.log("cotizacion => " + cotizacion, Level.INFO, null);
		LogDeMidasInterfaz.log("idToSolicitud => " + idToSolicitud, Level.INFO, null);
		LogDeMidasInterfaz.log("fechaInitVigencia => " + fechaInitVigencia, Level.INFO, null);
		
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, idToSolicitud);
		LogDeMidasInterfaz.log("solicitudDTO => " + solicitudDTO, Level.INFO, null);
		
		cotizacion.getValue().setSolicitud(solicitudDTO);
		cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		LogDeMidasInterfaz.log("cotizacion => " + cotizacion, Level.INFO, null);
		
		boolean hasCotizacionRecordsInProcess = cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess();
		LogDeMidasInterfaz.log("hasCotizacionRecordsInProcess => " + hasCotizacionRecordsInProcess, Level.INFO, null);
		if(hasCotizacionRecordsInProcess){
			entidadContinuityDao.rollBackContinuity(cotizacion.getContinuity().getId());
		}else{
			//Guardo registro historico en tabla control endosos
			saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO),
					fechaInitVigencia, null, primaTotalIgualar);
		}
		
		entidadBitemporalService.remove(cotizacion, TimeUtils.getDateTime(fechaInitVigencia));
		
		LogDeMidasInterfaz.log("Saliendo de guardaCotizacionEndosoCancelacionPoliza", Level.INFO, null);
	}
	
	@Override
	public void guardaCotizacionEndosoRehabilitacionPoliza(BigDecimal polizaId, Date fechaInicioVigencia, 
			Integer requiereCorrimientoVigencias, String accionEndoso) {
		guardaCotizacionEndosoRehabilitacionPoliza(polizaId, fechaInicioVigencia, requiereCorrimientoVigencias, 
				accionEndoso, SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardaCotizacionEndosoRehabilitacionPolizaAutomatica(BigDecimal polizaId, Date fechaInicioVigencia, 
			String accionEndoso,short claveMotivoEndoso, short migrada) {
		guardaCotizacionEndosoRehabilitacionPoliza(polizaId, fechaInicioVigencia, null, 
				accionEndoso, claveMotivoEndoso, migrada);
	}
	
	private void guardaCotizacionEndosoRehabilitacionPoliza(BigDecimal polizaId, Date fechaInicioVigencia, 
			Integer requiereCorrimientoVigencias, String accionEndoso, short claveMotivoEndoso) {
		
		this.guardaCotizacionEndosoRehabilitacionPoliza(polizaId, fechaInicioVigencia, 
				requiereCorrimientoVigencias, accionEndoso, claveMotivoEndoso, EmisionPendiente.POLIZA_NO_MIGRADA);
		
		/*BitemporalCotizacion cotizacion = null;
		DateTime validoEn = TimeUtils.getDateTime(fechaInicioVigencia);
		if(accionEndoso.equals(TipoAccionDTO.getEditarEndosoCot())) {
			
			cotizacion = initCotizacionEndoso(polizaId,
					fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA, 
					claveMotivoEndoso, accionEndoso);
			
			entidadContinuityDao.rollBackContinuity(cotizacion.getContinuity().getId());	
		}
		
		
		cotizacion = initCotizacionEndoso(polizaId,
				fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA, 
				claveMotivoEndoso, TipoAccionDTO.getNuevoEndosoCot());
		
		
		CotizacionEndosoDTO cotizacionEndoso = getCotizacionEndosoRehabilitacionPoliza(polizaId, fechaInicioVigencia, accionEndoso, claveMotivoEndoso);
		
		//Se guarda registro historico en tabla control endosos
		saveControlEndosoCot(cotizacion.getValue().getSolicitud(), cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO),
				fechaInicioVigencia);
		
		
		//Se regresa la situación de la póliza a la situación anterior al endoso de cancelación
		cotizacion.getEntidadContinuity().getBitemporalProperty().revert(cotizacionEndoso.getFechaEmisionCancelacion(), true);
		
		if (validoEn.compareTo(cotizacionEndoso.getCancelacionValidaDesde()) > 0) {
			//Se crea una cancelación (Remove) por el intervalo comprendido entre la fecha de cancelación anterior 
			//y fecha de inicio de vigencia del endoso actual
			cotizacion.getEntidadContinuity().getBitemporalProperty().end(
					new IntervalWrapper(TimeUtils.interval(cotizacionEndoso.getCancelacionValidaDesde(), validoEn)), true);
			
			if (requiereCorrimientoVigencias != null && requiereCorrimientoVigencias.equals(1)) {
				//Se guarda un inProcess a partir de la fecha de inicio de vigencia del endoso actual 
				//y hasta la fecha de fin de vigencia de póliza extendido
				cotizacion = cotizacion.getEntidadContinuity().getBitemporalProperty().getInProcess(validoEn);
				this.generaCotizacionEndosoExtensionVigencia(cotizacion, cotizacionEndoso.getFechaFinPolizaRecorrida());
			}
		}
		
		entidadService.save(cotizacion.getEntidadContinuity());*/
		
	}
	
	private void guardaCotizacionEndosoRehabilitacionPoliza(BigDecimal polizaId, Date fechaInicioVigencia, 
			Integer requiereCorrimientoVigencias, String accionEndoso, short claveMotivoEndoso, short migrada) {
		
		BitemporalCotizacion cotizacion = null;
		DateTime validoEn = TimeUtils.getDateTime(fechaInicioVigencia);
		if(accionEndoso.equals(TipoAccionDTO.getEditarEndosoCot())) {
			
			cotizacion = initCotizacionEndoso(polizaId,
					fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA, 
					claveMotivoEndoso, accionEndoso, migrada);
			
			entidadContinuityDao.rollBackContinuity(cotizacion.getContinuity().getId());	
		}
		
		
		cotizacion = initCotizacionEndoso(polizaId,	fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA, 
				claveMotivoEndoso, TipoAccionDTO.getNuevoEndosoCot(), migrada);
		
		
		CotizacionEndosoDTO cotizacionEndoso = getCotizacionEndosoRehabilitacionPoliza(polizaId, fechaInicioVigencia, accionEndoso, claveMotivoEndoso, migrada);
		
		//Se guarda registro historico en tabla control endosos
		saveControlEndosoCot(cotizacion.getValue().getSolicitud(), cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO),
				fechaInicioVigencia);
		
		
		//Se regresa la situación de la póliza a la situación anterior al endoso de cancelación
		cotizacion.getEntidadContinuity().getBitemporalProperty().revert(cotizacionEndoso.getFechaEmisionCancelacion(), true);
		
		if (validoEn.compareTo(cotizacionEndoso.getCancelacionValidaDesde()) > 0) {
			//Se crea una cancelación (Remove) por el intervalo comprendido entre la fecha de cancelación anterior 
			//y fecha de inicio de vigencia del endoso actual
			cotizacion.getEntidadContinuity().getBitemporalProperty().end(
					new IntervalWrapper(TimeUtils.interval(cotizacionEndoso.getCancelacionValidaDesde(), validoEn)), true);
			
			if (requiereCorrimientoVigencias != null && requiereCorrimientoVigencias.equals(1)) {
				//Se guarda un inProcess a partir de la fecha de inicio de vigencia del endoso actual 
				//y hasta la fecha de fin de vigencia de póliza extendido
				cotizacion = cotizacion.getEntidadContinuity().getBitemporalProperty().getInProcess(validoEn);
				this.generaCotizacionEndosoExtensionVigencia(cotizacion, cotizacionEndoso.getFechaFinPolizaRecorrida());
			}
		}
		
		entidadService.save(cotizacion.getEntidadContinuity());
		
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardaCotizacionEndosoCancelacionPolizaPTAutomatico(BitemporalCotizacion cotizacion, String[] continuitiesIds, BigDecimal idToSolicitud, 
			Date fechaInitVigencia)
	{
		this.guardaCotizacionEndosoCancelacionPoliza(cotizacion, idToSolicitud, 
				fechaInitVigencia);
		
		if(continuitiesIds != null)
    	{
    		List<BitemporalInciso> listaIncisosCotizacion = new ArrayList<BitemporalInciso>();
        	for(String idString:continuitiesIds)
    		{
    			BitemporalInciso bitemporalInciso = new BitemporalInciso();			
    			bitemporalInciso = incisoService.getInciso(cotizacion.getContinuity().getId(), Long.valueOf(idString), TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
    			listaIncisosCotizacion.add(bitemporalInciso);			
    		}
        	
        	for(BitemporalInciso bitemporalInciso : listaIncisosCotizacion)
        	{
        		entidadBitemporalService.remove(bitemporalInciso, TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));    		
        	}    		
    	}
	}
		
}
