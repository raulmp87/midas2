package mx.com.afirme.midas2.service.impl.movil.cotizador.seguroobligatorio;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.vigencia.VigenciaDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublico;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublicoDeduciblesAd;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO.TipoCobro;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.movil.cotizador.seguroobligatorio.CotizacionMovilSeguroObligatorioService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.agentes.CotizadorAgentesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.emision.EmisionService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.service.suscripcion.pago.PolizaPagoService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.SolicitudDataEnTramiteService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;
import mx.com.afirme.midas2.service.tarifa.TarifaServicioPublicoService;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.UtileriasWeb;
import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

@Stateless
public class CotizacionMovilSeguroObligatorioServiceImpl implements CotizacionMovilSeguroObligatorioService{

	private static final int SUBSRINGMESSAGE = 27;
	public static final Logger LOG = Logger.getLogger(CotizacionMovilSeguroObligatorioServiceImpl.class);
	public static final String IDMEDIOPAGO = "idMedioPago";
	public static final String CUENTAPAGO = "cuentaPago";
	public static final BigDecimal ID_TEMPLATE_ENVIO_PDF = BigDecimal.valueOf(Long.valueOf("16"));
	public static final String ESPOLIZA = "S";
	public static final String ESCOTIZACION = "N";
	public static final int DATOS_COTIZACION = 1;
	private static final int VALORMAXIMOUNIDADVIGENCIA = 365;
	
	private SimpleJdbcCall getListCotizacionSO;
	private SimpleJdbcCall getTemplateEmailSO;
	private SimpleJdbcCall getEmailContratanteSO;
	
	private JdbcTemplate jdbcTemplate;
	@EJB
	private EntidadService entidadService;
	@EJB
	private CalculoService calculoService;
	@EJB
	private CotizacionService cotizacionService;
	@EJB
	private ListadoService listadoService;
	@EJB
	private PolizaPagoService pagoService;
	@EJB
	private TarifaServicioPublicoService tarifaServicioPublicoService;
	@EJB
	private CotizadorAgentesService cotizadorAgentesService;
	@EJB
	private GenerarPlantillaReporte generarPlantillaReporte;
	@EJB
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	@EJB
	private EndosoService endosoService;
	@EJB
	private IncisoService incisoService;
	@EJB
	private CoberturaService coberturaService;
	@EJB
	private MailService mailService;
	@EJB
	private SistemaContext sistemaContext;
	@EJB
	private ClienteFacadeRemote clienteFacade;
	@EJB
	private EmisionService emisionService;
	@EJB
	private SolicitudDataEnTramiteService solicitudDataEnTramiteService;
	@EJB
	private SolicitudAutorizacionService autorizacionService;
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	@EJB
	private NegocioSeccionService negocioSeccionService;
	
	@SuppressWarnings("unused")
	private DataSource dataSource;	
	
	@Resource(name = "jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		
		this.getListCotizacionSO = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName("MIDAS")
				.withCatalogName("app_movil_cotizacion_so")
				.withProcedureName("get_list_cotizacionSO")
				.declareParameters(
						new SqlParameter("P_ID", Types.INTEGER),
						new SqlParameter("P_ISPOLIZA", Types.VARCHAR),
						new SqlOutParameter("P_STATUS", Types.NUMERIC),
						new SqlOutParameter("P_ERROR_DESCRIPTION", Types.VARCHAR),
						new SqlOutParameter("P_RESULT_CURSOR", OracleTypes.CURSOR,
								new RowMapper<IncisoCotizacionDTO>() {
									@Override
									public IncisoCotizacionDTO mapRow(ResultSet rs, int rowNum)
											throws SQLException {
										IncisoCotizacionDTO inciso = new IncisoCotizacionDTO();
										CotizacionDTO cotizacion = new CotizacionDTO();
										cotizacion.setIdToCotizacion(rs.getBigDecimal("idToCotizacion"));
										cotizacion.setNombreContratante(rs.getString("nombreContratante"));
										cotizacion.setClaveEstatus(rs.getShort("claveEstatus"));
										IncisoCotizacionId idInciso = new IncisoCotizacionId(cotizacion.getIdToCotizacion(), 
												rs.getBigDecimal("inciso"));
										IncisoAutoCot incisoAuto = new IncisoAutoCot();
										inciso.setId(idInciso);
										incisoAuto.setDescripcionFinal(rs.getString("vehiculo"));
										inciso.setIncisoAutoCot(incisoAuto);
										inciso.setCotizacionDTO(cotizacion);
										return inciso;
									}
								}));
		this.getTemplateEmailSO = new SimpleJdbcCall(jdbcTemplate)
			.withSchemaName("MIDAS")
			.withCatalogName("app_movil_cotizacion_so")
			.withFunctionName("fn_getTemplateEmailSO")
			.declareParameters(
					new SqlParameter("P_ID", Types.INTEGER),
					new SqlOutParameter("P_TEMPLATE", Types.VARCHAR));
		
		this.getEmailContratanteSO=new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_movil_cotizacion_so")
		.withFunctionName("FN_GETEMAILCLIENT")
		.declareParameters(
				new SqlParameter("P_ID", Types.INTEGER),
				new SqlParameter("P_ISPOLIZA", Types.VARCHAR),
				new SqlOutParameter("P_EMAIL", Types.VARCHAR));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<IncisoCotizacionDTO> getListCotizacionSeguroObligatorio(
			String id, String isPoliza) {
		LOG.info("--> en getListCotizacionSeguroObligatorio");
		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("P_ID",Integer.valueOf(id))
		.addValue("P_ISPOLIZA", isPoliza);
		
		boolean ejecutar = false;
		
		if (isPoliza.equals("S")){
			ejecutar = true;
		} else{
			boolean tieneRolPromotor = false;
			CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, 
						BigDecimal.valueOf(Long.valueOf(id)));
			
			if (cotizacion == null){
				LOG.info("--> la cotizacion "+id+" no existe");
				throw new ApplicationException("La cotizacion " + id + " no existe");
			}
			
			BigDecimal codigoAgenteCotizacion = cotizacion.getSolicitudDTO().getCodigoAgente();
			LOG.info("--> codigoAgente:"+codigoAgenteCotizacion);
			if (codigoAgenteCotizacion == null){
				throw new ApplicationException("La cotizacion " + id + " no tiene un agente relacionado");
			}
			
			try {
				List<Rol> roles = usuarioService.getUsuarioActual().getRoles();
				
				for (Rol item: roles){
					if ( item.getId() == Integer.parseInt(sistemaContext.getIdRolPromotor())) {
						tieneRolPromotor = true;
		            }
				}
			} catch(Exception e){
				LOG.error("--> error al buscar los roles del usuario");
				throw new ApplicationException("Error al buscar los roles del usuario");
			}
			
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			
			if (agenteUsuarioActual != null && agenteUsuarioActual.getId().equals(Long.parseLong(""+codigoAgenteCotizacion))){
				ejecutar = true;
			} else {
				if (tieneRolPromotor){
					//obtenemos la promotoría del promotor
					Long idPromotoria = null;
					if (agenteUsuarioActual != null && agenteUsuarioActual.getPromotoria() != null && agenteUsuarioActual.getPromotoria().getIdPromotoria() != null){
						idPromotoria = agenteUsuarioActual.getPromotoria().getIdPromotoria();
						LOG.info("--> idPromotoria = "+idPromotoria);
						//buscar sus agentes
						Map<Long, String> agentes = listadoService.getMapAgentesPorPromotoria(idPromotoria);
						
						if (agentes.size() > 0){
							Iterator<Entry<Long, String>> it = agentes.entrySet().iterator();
							while(it.hasNext()){
								Map.Entry<Long, String> pairs = it.next();
								LOG.info("--> key:"+pairs.getKey()+" value:"+pairs.getValue());
								if (pairs.getKey().equals(Long.parseLong(""+codigoAgenteCotizacion))){
									ejecutar = true;
								}
							}
						}
					}
				}
			}
		}
		
		Map<String, Object> execute = null;
		List<IncisoCotizacionDTO> incisos = null;
		
		if (ejecutar){
			try {
				execute = getListCotizacionSO.execute(parameter);
				incisos = (List<IncisoCotizacionDTO>) execute.get("P_RESULT_CURSOR");
			} catch(Exception e){
				LOG.error("Error al obtener la cotizacion "+id+"\n"+e.getMessage());
				throw new ApplicationException("La cotización "+id+" no existe.");
			}
		}
		
		return incisos;
	}
	
	@Override
	public Map<String, Object> getDatosInicialesCSO( String id ){
		Map<String,Object> mapCSO = new HashMap<String,Object>(1);
		Long idToNegocio = getIdToNegocioBaseCSO();
		mapCSO.put("idToNegocio", idToNegocio);
		
		if(idToNegocio!=null){
			Long idToNegProducto =  Long.valueOf(UtileriasWeb.getValueMap(
					listadoService.getMapNegProductoPorNegocioString(idToNegocio)));
			mapCSO.put("idToNegProducto", idToNegProducto);
			
			if(idToNegProducto!=null){
				String idToNegTipoPoliza = UtileriasWeb.getValueMap(
						listadoService.getMapNegTipoPolizaPorNegProductoFlotillaOIndividualString(
						idToNegProducto,0));
				mapCSO.put("idToNegTipoPoliza", getBigDecimalValue(idToNegTipoPoliza));
				BigDecimal idTcVigencia = (BigDecimal)entidadService.findAll(VigenciaDTO.class).get(0).getId();
				mapCSO.put("idTcVigencia", idTcVigencia);
				
				BigDecimal idFormaPago = listadoService.getIdToFormaDePagoByNegocioValid(
						getBigDecimalValue(idToNegTipoPoliza), idTcVigencia);
				
				mapCSO.put("idFormaPago", idFormaPago);
				
				List<NegocioSeccion> listarNegocios = negocioSeccionService.getSeccionListByIdNegocioProductoTipoPoliza(
						BigDecimal.valueOf(idToNegProducto), idToNegocio, getBigDecimalValue(idToNegTipoPoliza));
				BigDecimal idToNegocioSeccion = null;
				if(listarNegocios.size()>0){
					idToNegocioSeccion = listarNegocios.get(0).getIdToNegSeccion();
				}
				mapCSO.put("idToNegocioSeccion", idToNegocioSeccion);
				
				if (idToNegocioSeccion!=null){
					Map<Long, String> map = new LinkedHashMap<Long, String>(1);
					
					map = listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(idToNegocioSeccion);
					Long  idToNegPaqueteSeccion = null;
					Iterator<Entry<Long, String>> it = map.entrySet().iterator(); //listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(idToNegocioSeccion).entrySet().iterator();
					
					while (it.hasNext()){
						Map.Entry<Long, String> pairs = it.next();
						idToNegPaqueteSeccion = pairs.getKey();
					  break;
					}
					
					mapCSO.put("idToNegPaqueteSeccion", idToNegPaqueteSeccion);
					
					mapCSO.put("negocioPaquetesSeccion", map);
				}
			}
			Long idToNegDerechoPoliza = listadoService.getDerechoPolizaDefault(BigDecimal.valueOf(idToNegocio));
			mapCSO.put("idToNegDerechoPoliza", idToNegDerechoPoliza);
			mapCSO.put("estados", getListEstadosByIdNegocio(idToNegocio));
			
			mapCSO.put("codigoAgente", getClaveAgenteUsuario());
		}
		mapCSO.put("idToMoneda", MonedaDTO.MONEDA_PESOS);
				
		return mapCSO;
	}

	@Override
	public BigDecimal generarCotizacionServicioPublico(CotizacionDTO cotizacionDTO, BigDecimal idAgrupadorPasajeros,
			String claveTarifa, String idEstado) throws ApplicationException{
		CotizacionDTO cotizacion = cotizacionDTO;
		BigDecimal idToCotizacion = BigDecimal.ZERO;
		Map<String, Object> datos = getDatosInicialesCSO(null);
		
		cotizacion.setFechaInicioVigencia(new Date());
			
		GregorianCalendar gcFechaFin = new GregorianCalendar();
		gcFechaFin.setTime(new Date());
		BigDecimal idTcVigencia = (BigDecimal)datos.get("idTcVigencia");
		if(BigDecimal.ONE.compareTo(idTcVigencia)==0) {
			gcFechaFin.add(GregorianCalendar.YEAR, 1);
		} else {
			gcFechaFin.add(GregorianCalendar.MONTH, 6);
		}

	    if(cotizacion.getSolicitudDTO() == null) {
			cotizacion.setSolicitudDTO(new SolicitudDTO());
		}
	    if(cotizacion.getSolicitudDTO().getNegocio()== null) {
			cotizacion.getSolicitudDTO().setNegocio(new Negocio());
		}
	    if(cotizacion.getNegocioTipoPoliza() == null) {
			cotizacion.setNegocioTipoPoliza(new NegocioTipoPoliza());
	    	cotizacion.getNegocioTipoPoliza().setNegocioProducto(new NegocioProducto());
		}
	    if(cotizacion.getNegocioDerechoPoliza()==null){
	    	cotizacion.setNegocioDerechoPoliza(new NegocioDerechoPoliza());
	    }
		Long idToNegDerechoPoliza = (Long)datos.get("idToNegDerechoPoliza");
		BigDecimal idToNegTipoPoliza = (BigDecimal)datos.get("idToNegTipoPoliza");
		Long idToNegProducto = (Long)datos.get("idToNegProducto");
		cotizacion.setFechaFinVigencia(gcFechaFin.getTime());		
	    cotizacion.setPorcentajebonifcomision(Double.valueOf("0.0"));
	    cotizacion.getNegocioTipoPoliza().getNegocioProducto().setIdToNegProducto(idToNegProducto);
	    cotizacion.getSolicitudDTO().getNegocio().setIdToNegocio(getIdToNegocioBaseCSO());
	    cotizacion.getNegocioDerechoPoliza().setIdToNegDerechoPoliza(idToNegDerechoPoliza);
	    cotizacion.getNegocioTipoPoliza().setIdToNegTipoPoliza(idToNegTipoPoliza);
	    cotizacion.setPorcentajeIva(Double.valueOf("16"));
	    cotizacion.setIdMedioPago(BigDecimal.valueOf(MedioPagoDTO.MEDIO_PAGO_AGENTE));
		cotizacion.setTipoCotizacion(CotizacionDTO.TIPO_COTIZACION_SEGURO_OBLIGATORIO);
		cotizacion.setPorcentajePagoFraccionado(Double.valueOf("0.0"));
		cotizacion.setIdMoneda(new BigDecimal(MonedaDTO.MONEDA_PESOS));
		cotizacion.setIdFormaPago((BigDecimal)datos.get("idFormaPago"));
		cotizacion.getSolicitudDTO().setCodigoAgente((BigDecimal)datos.get("codigoAgente"));
		cotizacion = cotizacionService.crearCotizacion(cotizacion, cotizacion.getCodigoUsuarioCreacion());
		LOG.info("--> después de crearCotizacion idToCotizacion:"+cotizacion.getIdToCotizacion());
		idToCotizacion = cotizacion.getIdToCotizacion();
		try{
			generaIncisoCotizacion(cotizacion, datos, idAgrupadorPasajeros, claveTarifa, idEstado);
			LOG.info("--> despues de generarIncisoCotizacion");
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return idToCotizacion;
	}
	
	private void generaIncisoCotizacion(CotizacionDTO cotizacionDTO, Map<String,Object> map, 
			BigDecimal idAgrupadorPasajeros, String claveTarifa, String idEstado){
		LOG.info("--> en generarIncisoCotizacion idToCotizacion:"+cotizacionDTO.getIdToCotizacion());
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, cotizacionDTO.getIdToCotizacion());

		IncisoCotizacionDTO incisoCotizacion = null;
			if(cotizacion != null){
				incisoCotizacion = setIncisoCot(cotizacion);
				BigDecimal idToNegocioSeccion = (BigDecimal)map.get("idToNegocioSeccion");
				String idMarca = UtileriasWeb.getValueMap(
						listadoService.getMapMarcaVehiculoPorNegocioSeccionString(idToNegocioSeccion));
				String idTipoUso = null; 
				Short idModelo = null;
				try{
					idTipoUso = listadoService.getMapTipoUsoVehiculoByNegocio(idToNegocioSeccion).keySet().iterator().next().toString();
					idModelo = listadoService.getMapModeloVehiculoPorMonedaMarcaNegocioSeccion(cotizacion.getIdMoneda(), 
						getBigDecimalValue(idMarca), idToNegocioSeccion).keySet().iterator().next();
				}catch(RuntimeException e){
					LOG.error(e.getMessage(), e);
				}

				Long idToNegocioPaqueteId = (Long)map.get("idToNegPaqueteSeccion");
				incisoCotizacion.getIncisoAutoCot().setNegocioPaqueteId(idToNegocioPaqueteId);
				incisoCotizacion.getIncisoAutoCot().setNegocioSeccionId(idToNegocioSeccion.longValue());
				incisoCotizacion.getIncisoAutoCot().setEstadoId(idEstado);
				String municipioId = idEstado.substring(0, 4)+"1";
				incisoCotizacion.getIncisoAutoCot().setMunicipioId(municipioId);
				
				incisoCotizacion.getIncisoAutoCot().setTipoUsoId(Long.valueOf(idTipoUso));
				incisoCotizacion.getIncisoAutoCot().setMarcaId(getBigDecimalValue(idMarca));
				incisoCotizacion.getIncisoAutoCot().setIdMoneda(cotizacion.getIdMoneda().shortValue());
				incisoCotizacion.getIncisoAutoCot().setModeloVehiculo(idModelo);
				incisoCotizacion.getIncisoAutoCot().setEstiloId(UtileriasWeb.STRING_EMPTY);
				incisoCotizacion.getIncisoAutoCot().setIdAgrupadorPasajeros(idAgrupadorPasajeros);
				incisoCotizacion.setIdMedioPago(BigDecimal.valueOf(MedioPagoDTO.MEDIO_PAGO_AGENTE));
			}

			List<CoberturaCotizacionDTO>coberturasList = coberturasSO("", claveTarifa, incisoCotizacion);
			
			recalcularCoberturasInciso(incisoCotizacion, coberturasList);
	}
	
	private List<CoberturaCotizacionDTO> coberturasSO(String idCoberturaNA, String claveTarifa, 
			IncisoCotizacionDTO incisoCotizacion){
		List<CoberturaCotizacionDTO> coberturaCotizacionList = new ArrayList<CoberturaCotizacionDTO>(1);
		List<CoberturaCotizacionDTO>coberturasList = mostrarCorberturaCotizacion(incisoCotizacion);
		String[] clavesTarifa = claveTarifa.split(UtileriasWeb.SEPARADOR_COMA);
        for(CoberturaCotizacionDTO cobertura : coberturasList){
    		BigDecimal idToCobertura = cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura();
        	for(String clave : clavesTarifa){
        		String[] tarifa = clave.split(UtileriasWeb.SEPARADOR_MEDIO);
        		if(tarifa!=null && idToCobertura.compareTo(BigDecimal.valueOf(Long.valueOf(tarifa[0])))==0){
    				cobertura.setValorSumaAsegurada(tarifa[1]);
    				if (tarifa.length>2 && tarifa[2] != null) {
    						cobertura.setValorDeducible(tarifa[2]);
    				}
    				break;
        		}
        	}
        	
            cobertura.setClaveContrato(Short.valueOf("1"));
            coberturaCotizacionList.add(cobertura);
         }
        return coberturaCotizacionList;
	}
	
	private void recalcularCoberturasInciso(IncisoCotizacionDTO incisoCotizacion, 
			List<CoberturaCotizacionDTO> coberturasList){
		IncisoCotizacionDTO incisoGuardado = null;
		try{
			if(incisoCotizacion != null && incisoCotizacion.getId() != null){
				LOG.info("--> finding incisoGuardado... incisoCotizacion.getId().getIdToCotizacion():"+incisoCotizacion.getId().getIdToCotizacion());
				incisoGuardado = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacion.getId());
				if(incisoGuardado != null){
					NegocioPaqueteSeccion negPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class, incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId());
					SeccionDTO seccionDTO = negPaqueteSeccion.getNegocioSeccion().getSeccionDTO();
					if(incisoGuardado.getSeccionCotizacion().getSeccionDTO().getIdToSeccion().longValue() != seccionDTO.getIdToSeccion().longValue()){
						incisoService.borrarInciso(incisoCotizacion);
						incisoCotizacion.getId().setNumeroInciso(null);
					}
				}
			}
			LOG.info("--> incisoCotizacion.getId().getIdToCotizacion():"+incisoCotizacion.getId().getIdToCotizacion());
			incisoCotizacion = incisoService.prepareGuardarIncisoAgente(
					incisoCotizacion.getId().getIdToCotizacion(),
					incisoCotizacion.getIncisoAutoCot(), incisoCotizacion,
					coberturasList,null, null, null, null);
		}catch(Exception ex){
			LOG.error("Error al calcular el inciso"+ ex.getMessage());
		}
	}
	
	@Override
	public Map<String,Object> getDatosCobranza(String idToCotizacion, Integer idMedioPago){
		Map<String,Object> mapCobranza = new HashMap<String,Object>(1);
		CotizacionDTO cotizacion = validaCotizacion(idToCotizacion);

		mapCobranza.put("idToPersonaContratante", cotizacion.getIdToPersonaContratante());
		mapCobranza.put("claveEstatus",cotizacion.getClaveEstatus());
		
		if(CotizacionDTO.ESTATUS_COT_EMITIDA==cotizacion.getClaveEstatus()){
			PolizaDTO poliza = getPolizaByIdToCotizacion(idToCotizacion);
			mapCobranza.put("idToPoliza", poliza.getIdToPoliza());
		}
		
		ResumenCostosDTO resumen = calculoService.obtenerResumenCotizacion(cotizacion, true);
		cotizacion.setPrimaNetaTotal(resumen.getPrimaNetaCoberturas());
		try{
			EsquemaPagoCotizacionDTO esquemaPagoCotizacionDTO = cotizacionService.findEsquemaPagoCotizacion(cotizacion);
			mapCobranza.put("montoPagoInicial", esquemaPagoCotizacionDTO.getPagoInicial().getImporteFormat());
		}catch(Exception e){
			throw new ApplicationException("No es posible obtener el primer pago.");
		}

		mapCobranza.put("mediosPago", listadoService.getMediosPagoSO(
				cotizacion.getSolicitudDTO().getNegocio()));

		Map<Boolean, String> mapDatosIguales = new HashMap<Boolean, String>(1);
		mapDatosIguales.put(true, "SI");
		mapDatosIguales.put(false, "NO");
		
		mapCobranza.put("mapDatosIguales", mapDatosIguales);
		try{
			mapCobranza.put("conductosDeCobro",listadoService.getConductosCobro(
					cotizacion.getIdToPersonaContratante().longValue(),	Integer.parseInt(cotizacion.getIdMedioPago()+UtileriasWeb.STRING_EMPTY)));
		} catch(RuntimeException e) {
			LOG.error(e.getMessage(), e);
		}

		mapCobranza.put(UtileriasWeb.BANCOS.toLowerCase(), listadoService.getBancos());
		
		if(cotizacion.getIdConductoCobroCliente() != null){
			mapCobranza.putAll(getDatosConductoCobroClienteByCotizacion(idToCotizacion));
			mapCobranza.put("idConductoCobro", cotizacion.getIdConductoCobroCliente().toString());
		}else{
			mapCobranza.put(CUENTAPAGO, new CuentaPagoDTO());
			mapCobranza.put(IDMEDIOPAGO, cotizacion.getIdMedioPago());
		}
		
		return mapCobranza;
	}

	
	@Override
	public Map<String,Object> getConductosCobroCotizacion(String idToCotizacion) throws ApplicationException {
		Map<String,Object> mapCobranza = new HashMap<String,Object>(1);
		CotizacionDTO cotizacion = validaCotizacion(idToCotizacion);
		
		try{
			if (cotizacion != null){
				mapCobranza.put("conductosDeCobro",listadoService.getConductosCobro(
					cotizacion.getIdToPersonaContratante().longValue(),	Integer.parseInt(cotizacion.getIdMedioPago()+UtileriasWeb.STRING_EMPTY)));
			}
		} catch(RuntimeException e) {
			LOG.error("--> Error al obtener los conductos cobro\n"+e.getMessage(), e);
			throw new ApplicationException("Error al obtener los conductos de cobro");
		}
		
		return mapCobranza;
	}

	private PolizaDTO getPolizaByIdToCotizacion(String idToCotizacion) {
		List<PolizaDTO> poliza = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", 
				BigDecimal.valueOf(Long.valueOf(idToCotizacion)));
		if(poliza!=null && poliza.size()>0){
			return poliza.get(0);	
		}
		return null;
	}

	private Map<String, Object> getDatosConductoCobroClienteByCotizacion(
			String idToCotizacion) {
		Map<String, Object> mapDatosConductoCobro = new HashMap<String, Object>(1);
		CuentaPagoDTO cuentaPagoDTO = getConducto(idToCotizacion, null, null);
		if (cuentaPagoDTO != null){
			mapDatosConductoCobro.put(CUENTAPAGO, cuentaPagoDTO);
			mapDatosConductoCobro.put(IDMEDIOPAGO, cuentaPagoDTO.getTipoConductoCobro()!=null?
					cuentaPagoDTO.getTipoConductoCobro().valor(): null);
			mapDatosConductoCobro.put("datosIguales", true);
			mapDatosConductoCobro.putAll(getDatosDomiciliacion(cuentaPagoDTO));		
		}
		return mapDatosConductoCobro;
	}

	@Override
	public Map<String, Object> guardarCobranza(String idToCotizacion, Integer idMedioPago, String tipoTarjeta, 
			Integer idBanco, CuentaPagoDTO cuentaPagoDTO){
		Map<String, Object> map = new HashMap<String, Object>(1);
		MensajeDTO mensaje = new MensajeDTO();
		
		if(cuentaPagoDTO==null){
			throw new ApplicationException("Favor de validar los datos de la cuenta de pago");
		}

		CotizacionDTO cotizacion = validaCotizacion(idToCotizacion);
		
		if(cotizacion !=null){
			map.put("claveEstatus",cotizacion.getClaveEstatus());
			for(TipoCobro tCobro: CuentaPagoDTO.TipoCobro.values()){
				Short tipoCobro = tCobro.valor();
				if(tipoCobro.equals(idMedioPago.shortValue())){
					if(tipoCobro.equals(CuentaPagoDTO.TipoCobro.TARJETA_CREDITO.valor())){
						if (tipoTarjeta.equals(CuentaPagoDTO.TipoTarjeta.MASTERCARD) ){
							cuentaPagoDTO.setTipoTarjeta(CuentaPagoDTO.TipoTarjeta.MASTERCARD);
						}else{
							cuentaPagoDTO.setTipoTarjeta(CuentaPagoDTO.TipoTarjeta.VISA);
						}
					}else if(tipoCobro.equals(CuentaPagoDTO.TipoCobro.CUENTA_AFIRME.valor())){
						cuentaPagoDTO.setIdBanco(idBanco.longValue());
					}else if (tipoCobro.equals(CuentaPagoDTO.TipoCobro.DOMICILIACION.valor())){
						if (cuentaPagoDTO.getDiaPago() == null){
							Calendar calendar = Calendar.getInstance();
							cuentaPagoDTO.setDiaPago(calendar.get(Calendar.DAY_OF_MONTH));
						}
					}
					cuentaPagoDTO.setTipoConductoCobro(tCobro);
					break;
				}			
			}			
			
			// Tipo de persona
			cuentaPagoDTO.setTipoPersona(cotizacion.getSolicitudDTO().getClaveTipoPersona().toString());
			// Constrante
			cuentaPagoDTO.setIdCliente(cotizacion.getIdToPersonaContratante());
			try{
				pagoService.guardarConductoCobro(cotizacion.getIdToCotizacion(), cuentaPagoDTO);
				pagoService.guardarConductoCobroCotizacionAInciso(cotizacion.getIdToCotizacion());
			}catch(Exception e){
				String message = e.getMessage();
				if(message.indexOf("java.lang.RuntimeException:") != -1){
					throw new ApplicationException(message.substring(message.indexOf("java.lang.RuntimeException:")+SUBSRINGMESSAGE,message.length()));
				}else{
					throw new ApplicationException("No es posible salvar los datos de la cobranza");
				}
			}
			mensaje.setTipoMensaje(PolizaDTO.ICONO_CONFIRM);
			mensaje.setMensaje("Acci\u00F3n realizada correctamente.");
		}
		map.put("mensaje", mensaje);
		return map;
	}

	@Override
	public Map<String,Object> getDatosConducto(String idToCotizacion, BigDecimal idToPersonaContratante,
			String idConductoCobro){
		Map<String,Object> mapDatosConducto = new HashMap<String,Object>(1);
		CuentaPagoDTO cuentaPagoDTO = new CuentaPagoDTO();
		if(idConductoCobro!=null && idToCotizacion!=null){
			validaCotizacion(idToCotizacion);
			
			cuentaPagoDTO = getConducto(idToCotizacion, idToPersonaContratante, idConductoCobro);
			mapDatosConducto.putAll(getDatosDomiciliacion(cuentaPagoDTO));
		}
		mapDatosConducto.put(CUENTAPAGO, cuentaPagoDTO);
		return mapDatosConducto;
	}


	@Override
	public Map<String,Object> getDatosTC(CuentaPagoDTO cuentaPagoDTO, Integer idMedioPago){
		if(cuentaPagoDTO==null){
			cuentaPagoDTO = new CuentaPagoDTO();
		}

		Map<String,Object> mapDatosCuenta = new HashMap<String,Object>(1);
		switch (idMedioPago) {
		case CuentaPagoDTO.TARJETA_CREDITO:
			if (cuentaPagoDTO.getDiaPago() == null) {
				Calendar calendar = Calendar.getInstance();
				mapDatosCuenta.put("diaPago", calendar.get(Calendar.DAY_OF_MONTH));
			}
			if(cuentaPagoDTO.getTipoTarjeta() != null) {
				mapDatosCuenta.put("tipoTarjeta", cuentaPagoDTO.getTipoTarjeta().valor());
			}
			if(cuentaPagoDTO.getCuentaNoEncriptada() != null){
				mapDatosCuenta.put("promociones", listadoService.getPromocionesTC(cuentaPagoDTO.getCuentaNoEncriptada()));
			}
			
			mapDatosCuenta.putAll(getCatalogo(UtileriasWeb.BANCOS, null));
			break;
		case CuentaPagoDTO.DOMICILIACION:
			mapDatosCuenta.putAll(getCatalogo(UtileriasWeb.BANCOS, null));
			break;
		default:
			break;			
		}
		return mapDatosCuenta;
	}
	
	@Override
	public Map<String, Object> getResumenCosto(String id, String isPoliza, BigDecimal numeroInciso){
		ResumenCostosDTO resumen = new ResumenCostosDTO();
		Map<String,Object> mapResumenPoliza = new HashMap<String,Object>(1);
		String mensaje = UtileriasWeb.STRING_EMPTY;
		if(ESPOLIZA.equalsIgnoreCase(isPoliza)){
			PolizaDTO poliza = validaPolizaDTO(id);
			resumen = calculoService.resumenCostosCaratulaPoliza(poliza.getIdToPoliza());
			mensaje = "La p\u00f3liza se emiti\u00f3 correctamente. </br>El n\u00famero de  P\u00f3liza es: "+poliza.getNumeroPolizaFormateada();
			
		} else if(ESCOTIZACION.equalsIgnoreCase(isPoliza)){
			CotizacionDTO cotizacion = validaCotizacion(id);
			if(cotizacion!=null){
				IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId(cotizacion.getIdToCotizacion(), numeroInciso);
				IncisoCotizacionDTO incisoCotizacion = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionId);
				incisoCotizacion.setCotizacionDTO(cotizacion);
				mensaje = cotizadorAgentesService.getBienAsegurado(incisoCotizacion);
			}
			resumen =  calculoService.obtenerResumenCotizacion(cotizacion, false);
		}
		mapResumenPoliza.put("resumen", resumen);
		mapResumenPoliza.put("idToCotizacion", id);
		mapResumenPoliza.put("isPoliza", isPoliza);
		mapResumenPoliza.put("msn", mensaje);
		
		return mapResumenPoliza;
	}

	@Override
	public Map<String, Object> getCatalogo(String idCatalogo, String id){
		Map<String,Object> mapCatalogo = new HashMap<String,Object>(1);
		if(UtileriasWeb.CIUDADES.equalsIgnoreCase(idCatalogo)){
			mapCatalogo.put(UtileriasWeb.CIUDADES.toLowerCase(),listadoService.getMunicipiosPorEstadoId(null, id));
		}else if(UtileriasWeb.COLONIAS.equalsIgnoreCase(idCatalogo)){
			mapCatalogo.put(UtileriasWeb.COLONIAS.toLowerCase(),listadoService.getColoniasPorCPMidas(id));
		}else if(UtileriasWeb.ESTADOS.equalsIgnoreCase(idCatalogo)){
			mapCatalogo.put(UtileriasWeb.ESTADOS.toLowerCase(), 
						listadoService.getMapEstados(UtileriasWeb.KEY_PAIS_MEXICO));
		}else if(UtileriasWeb.BANCOS.equalsIgnoreCase(idCatalogo)){
			mapCatalogo.put(UtileriasWeb.BANCOS.toLowerCase(),listadoService.getBancos());
		}
		
		return mapCatalogo;
	}
	
	@Override
	public MensajeDTO validarListaParaEmitir(String idToCotizacion){
		return cotizadorAgentesService.validarListaParaEmitir(BigDecimal.valueOf(Long.valueOf(idToCotizacion)));
	}
	
	private CuentaPagoDTO getConducto(String idToCotizacion, 
			BigDecimal idToPersonaContratante, String idConductoCobro){
		CuentaPagoDTO cuentaPagoDTO = new CuentaPagoDTO();
		try {
			if (idToPersonaContratante==null) {
				cuentaPagoDTO = pagoService.obtenerConductoCobro(
						BigDecimal.valueOf(Long.valueOf(idToCotizacion)));
			} else {
				cuentaPagoDTO = pagoService.obtenerConductoCobro(
								idToPersonaContratante, Long.valueOf(idConductoCobro));								
			}			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return cuentaPagoDTO;
	}
	
	private Map<String, Object> getDatosDomiciliacion(CuentaPagoDTO cuentaPagoDTO) {

		Map<String,Object> mapDatosDomiciliacion = new HashMap<String,Object>(1);
		mapDatosDomiciliacion.put(UtileriasWeb.ESTADOS.toLowerCase(), listadoService.getMapEstados(
				UtileriasWeb.KEY_PAIS_MEXICO));
		if (cuentaPagoDTO != null && cuentaPagoDTO.getDomicilio() != null) {			
			if (cuentaPagoDTO.getDomicilio().getClaveEstado() != null) {
				mapDatosDomiciliacion.put(UtileriasWeb.CIUDADES.toLowerCase(), listadoService
						.getMapMunicipiosPorEstado(cuentaPagoDTO.getDomicilio().getClaveEstado()));
			}
			if (cuentaPagoDTO.getDomicilio().getClaveCiudad() != null) {
				mapDatosDomiciliacion.put(UtileriasWeb.COLONIAS.toLowerCase(), 
						listadoService.getMapColonias(cuentaPagoDTO.getDomicilio().getClaveCiudad()));
			}
		}
		return mapDatosDomiciliacion;
	}

	@Override
	public Map<String, Object> getTarifasServicioPublico(
			TarifaServicioPublico tarifaServicioPublico) throws ApplicationException {
		List<TarifaServicioPublicoDeduciblesAd> tarifaDeduciblesAdicionales = new ArrayList<TarifaServicioPublicoDeduciblesAd>(1);
		
		try {
			LOG.info("--> Obtener tarifas servicio publico a nivel estado y municipio");
			tarifaDeduciblesAdicionales = tarifaServicioPublicoService.obtenerDeduciblesAdicionalesSerivicioPublico(tarifaServicioPublico);
			if(tarifaDeduciblesAdicionales.size() == 0) {
				LOG.info("--> Obtener tarifas servicio publico a nivel estado");	
				tarifaServicioPublico.setCiudadDTO(new CiudadDTO());
				tarifaDeduciblesAdicionales = tarifaServicioPublicoService.obtenerDeduciblesAdicionalesSerivicioPublico(tarifaServicioPublico);
			}
		} catch (Exception e) {
			LOG.info("Error al obtener las tarifas:\n"+e.getMessage());
			throw new ApplicationException("Error al obtener las tarifas");
		}
		
		Map<String,Object> result = new HashMap<String,Object>(1);
		if (tarifaDeduciblesAdicionales.size() > 0){
			
			Map<Double, List<TarifaServicioPublicoDeduciblesAd>> listaAgrupadaPorSA = new HashMap<Double, List<TarifaServicioPublicoDeduciblesAd>>(1);
			for (TarifaServicioPublicoDeduciblesAd dto: tarifaDeduciblesAdicionales){ 
				if (listaAgrupadaPorSA.containsKey(dto.getValorSumaAsegurada())) {
					listaAgrupadaPorSA.get(dto.getValorSumaAsegurada()).add(dto);
				} else {
					List<TarifaServicioPublicoDeduciblesAd> lista = new ArrayList<TarifaServicioPublicoDeduciblesAd>(1);
					lista.add(dto);
					listaAgrupadaPorSA.put(dto.getValorSumaAsegurada(), lista);
				}
			}
			for(Map.Entry<Double, List<TarifaServicioPublicoDeduciblesAd>> entry : listaAgrupadaPorSA.entrySet()) {
				int cont = 1;
				Map<String,Object> mapaDeducible = new HashMap<String,Object>(1);
				for (TarifaServicioPublicoDeduciblesAd dto: entry.getValue()){
					Map<String, String> map = new HashMap<String, String>(1);
					map.put("valorDeducible1", dto.getValorDeducible1()+UtileriasWeb.STRING_EMPTY);
					map.put("valorDeducible2", dto.getValorDeducible2()+UtileriasWeb.STRING_EMPTY);
					map.put("valorDeducible3", dto.getValorDeducible3()+UtileriasWeb.STRING_EMPTY);
					map.put("idToCobertura", dto.getCoberturaDTO().getIdToCobertura()+UtileriasWeb.STRING_EMPTY);
					map.put("valorSumaAsegurada", dto.getValorSumaAsegurada()+UtileriasWeb.STRING_EMPTY);
					map.put("valorDeducible", dto.getNegocioDeducibleCob().getValorDeducible()+UtileriasWeb.STRING_EMPTY);
					map.put("valorSumaAseguradaStr", dto.getValorSumaAseguradaStr());
						
					mapaDeducible.put(UtileriasWeb.STRING_EMPTY+cont, map);
					cont++;
				}
				result.put(entry.getKey().toString(), mapaDeducible);
			}
		}

		return result;
	}
	
	@Override
	public Map<BigDecimal, String> getListMarcasByNegSeccion(
			BigDecimal idToNegSeccion) throws ApplicationException {
		if (idToNegSeccion == null){
			throw new ApplicationException("La línea de negocio es requerido");
		}
		
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>(); 
			
		try {
			map = listadoService.getMapMarcaVehiculoPorNegocioSeccion(idToNegSeccion);
		} catch (Exception e){
			throw new ApplicationException("Ocurrió un error al obtner la lista de marcas.\n"+e.getMessage());
		}
		
		return map;
	}

	@Override
	public Map<Short, Short> getListModelosByMarca(
			BigDecimal idToNegSeccion, BigDecimal idMarca)
			throws ApplicationException {
		Map<Short, Short> map = new LinkedHashMap<Short, Short>(1); 
		
		if (idToNegSeccion == null || idToNegSeccion.compareTo(BigDecimal.ZERO) == 0){
			throw new ApplicationException("La línea de negocio es requerido");
		}
		
		if (idMarca == null || idMarca.compareTo(BigDecimal.ZERO) == 0){
			throw new ApplicationException("La marca es requerido");
		}
		
		try {
			map = listadoService.getMapModeloVehiculoPorMonedaMarcaNegocioSeccion(
					BigDecimal.valueOf(MonedaDTO.MONEDA_PESOS), idMarca, idToNegSeccion);
		} catch(Exception e){
			LOG.error("Ocurrió un error al obtener la lista de paquetes\n"+e.getMessage());
			throw new ApplicationException("Ocurrió un error al obtener la lista de paquetes");
		}
		
		return map;
	}
	
	@Override
	public TransporteImpresionDTO getPDFSeguroObligatorio(String id, String isPoliza,Locale locale, String isMovil)
		throws ApplicationException{
		TransporteImpresionDTO certificado = null;
		if(ESPOLIZA.equalsIgnoreCase(isPoliza)){
			certificado = generaPlantillaPoliza(id, locale, isMovil.equals("S"));
		}else if (ESCOTIZACION.equalsIgnoreCase(isPoliza)){
			certificado = generaPlantillaCotizacion(id, locale, isMovil.equals("S"));			
		}
		if(certificado==null || certificado.getByteArray()==null){
			throw new ApplicationException("No se genero el certificado para las "+
					(ESPOLIZA.equalsIgnoreCase(isPoliza)?" Póliza ":"Cotización"));
		}
		return certificado;
	}
	
	private IncisoCotizacionDTO setIncisoCot(CotizacionDTO cotizacion) {
		IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
		incisoCotizacion.setId(new IncisoCotizacionId());
		
		if(incisoCotizacion.getId().getIdToCotizacion() == null){
			incisoCotizacion.getId().setIdToCotizacion(cotizacion.getIdToCotizacion());
		}
		incisoCotizacion.setCotizacionDTO(cotizacion);
		
		if(incisoCotizacion.getIncisoAutoCot()==null){
			IncisoAutoCot incisoAutoCot = new IncisoAutoCot();
			incisoCotizacion.setIncisoAutoCot(incisoAutoCot);
		}
		incisoCotizacion.getIncisoAutoCot().setAsociadaCotizacion(1);
		return incisoCotizacion;
	}
	
	private List<CoberturaCotizacionDTO> mostrarCorberturaCotizacion(IncisoCotizacionDTO incisoCotizacion){
		CotizacionDTO cotizacionDTO = incisoCotizacion.getCotizacionDTO();		
		List<CoberturaCotizacionDTO>coberturaListCotizacion = new ArrayList<CoberturaCotizacionDTO>(1);
		NegocioPaqueteSeccion negPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class,incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId());
		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
		estiloVehiculoId.valueOf(incisoCotizacion.getIncisoAutoCot().getEstiloId());
		if(incisoCotizacion.getIncisoAutoCot().getEstadoId() != null){
			if(incisoCotizacion.getIncisoAutoCot().getEstadoId().trim().equals(UtileriasWeb.STRING_EMPTY)) {
				incisoCotizacion.getIncisoAutoCot().setEstadoId(null);
			}
		}
		if(incisoCotizacion.getIncisoAutoCot().getMunicipioId() != null){
			if(incisoCotizacion.getIncisoAutoCot().getMunicipioId().trim().equals(UtileriasWeb.STRING_EMPTY)) {
				incisoCotizacion.getIncisoAutoCot().setMunicipioId(null);
			}
		}
		Long numeroSecuencia = Long.valueOf("1");
		numeroSecuencia = numeroSecuencia + incisoService.maxSecuencia(cotizacionDTO.getIdToCotizacion());
		try{
			NegocioTipoPoliza negTipoPoliza = negPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza();
			coberturaListCotizacion = coberturaService.getCoberturas(negTipoPoliza.getNegocioProducto().getNegocio().getIdToNegocio(), 
					negTipoPoliza.getNegocioProducto().getProductoDTO().getIdToProducto(), 
					negTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza(),
				negPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), 
				negPaqueteSeccion.getPaquete().getId(), incisoCotizacion.getIncisoAutoCot().getEstadoId(), incisoCotizacion.getIncisoAutoCot().getMunicipioId(), 
				cotizacionDTO.getIdToCotizacion(), cotizacionDTO.getIdMoneda().shortValue(), numeroSecuencia,
				estiloVehiculoId.getClaveEstilo(), incisoCotizacion.getIncisoAutoCot().getModeloVehiculo().longValue(), 
				new BigDecimal(incisoCotizacion.getIncisoAutoCot().getTipoUsoId()), null, null);					
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			coberturaListCotizacion = new ArrayList<CoberturaCotizacionDTO>(1);
		}
		return 	coberturaListCotizacion;
	}
	
	@Override
	public MensajeDTO saveCotizacion(IncisoCotizacionDTO inciso, String formaString) 
	throws ApplicationException{
		MensajeDTO mensaje = new MensajeDTO();
		int forma = Integer.valueOf(formaString);
		try{
			IncisoCotizacionDTO incisoGuardado = entidadService.findById(IncisoCotizacionDTO.class, inciso.getId());
			IncisoAutoCot incisoAutoGurdado = incisoGuardado.getIncisoAutoCot();
			CotizacionDTO cotizacionGuardada = validaCotizacion(inciso.getId().getIdToCotizacion().toString());
			if(forma==DATOS_COTIZACION){
				IncisoAutoCot incisoAut = inciso.getIncisoAutoCot();
				CotizacionDTO cotizacion = inciso.getCotizacionDTO();
				incisoAutoGurdado.setModeloVehiculo(incisoAut.getModeloVehiculo());
				incisoAutoGurdado.setMarcaId(incisoAut.getMarcaId());
				incisoAutoGurdado.setEstiloId(incisoAut.getEstiloId());
				incisoAutoGurdado.setCoincideEstilo(false);
				incisoAutoGurdado.setDescripcionFinal(incisoAut.getDescripcionFinal());
				incisoAutoGurdado.setClaveSimilar(false);
				incisoAutoGurdado.setVinValido(false);
				cotizacionGuardada.setFechaInicioVigencia(cotizacion.getFechaInicioVigencia());
				cotizacionGuardada.setFechaFinVigencia(cotizacion.getFechaFinVigencia());
				cotizacionGuardada.setNombreContratante(cotizacion.getNombreContratante());
				incisoGuardado.setCotizacionDTO(cotizacionGuardada);
			}
	
			List<CoberturaCotizacionDTO>coberturasList =  mostrarCorberturaCotizacion(incisoGuardado);
			
			if(validaCotizacion(cotizacionGuardada)){
				cotizacionService.updateSolicitudCotizacion(cotizacionGuardada);
			}
			recalcularCoberturasInciso(incisoGuardado, coberturasList);
			mensaje.setMensaje("Acci\u00F3n realizada correctamente");
			mensaje.setTipoMensaje(PolizaDTO.ICONO_CONFIRM);
		}catch(RuntimeException e){
			LOG.error(e.getMessage(), e);
			throw new ApplicationException("Error al guardar la cotizaci\u00F3n");
		}
		
		return mensaje;
	}
	
	private boolean validaCotizacion(CotizacionDTO cotizacionDTO) {
		BigDecimal idTipoPoliza = cotizacionDTO.getNegocioTipoPoliza().getIdToNegTipoPoliza();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		DateTime inicioVigencia = new DateTime(cotizacionDTO.getFechaInicioVigencia());
		DateTime finVigencia = new DateTime(cotizacionDTO.getFechaFinVigencia());
		long days = Days.daysBetween(inicioVigencia,finVigencia).getDays();
		
		if(idTipoPoliza!=null){
			NegocioTipoPoliza negocioTipoPoliza  = entidadService.findById(NegocioTipoPoliza.class, idTipoPoliza);
			if(negocioTipoPoliza!=null){
				cotizacionDTO.setTipoPolizaDTO(negocioTipoPoliza.getTipoPolizaDTO());
				Long valorMaximoUnidadVigencia = cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getValorMaximoUnidadVigencia().longValue();
				// Valida que los días de vigencia de la cotizacion no sean mayor a 365 dias
				if ( valorMaximoUnidadVigencia<= VALORMAXIMOUNIDADVIGENCIA && 
						days > VALORMAXIMOUNIDADVIGENCIA){
						LOG.error("Los dias de vigencia de la cotizacion no pueden ser mayores a un anio");
						return false;
				}
				// Valida que los días de vigencia de la cotizacion no sean mayores a los del producto
				if (days > valorMaximoUnidadVigencia) {
						LOG.error("Los dias de la cotizacion son mayores a los dias de vigencia del producto");
						return false;
				}
				// Valida que la fecha fin fija del negocio sea igual que la 
				// Fecha fin de la cotizacion y compara las fechas
				Date fechaFija = cotizacionDTO.getSolicitudDTO().getNegocio().getFechaFinVigenciaFija();
				if ( fechaFija!= null && 
						!DateUtils.isSameDay(fechaFija, cotizacionDTO.getFechaFinVigencia())) {
					LOG.error("La fecha fin de vigencia debe de ser igual a <br/>" + dateFormat.format(fechaFija));
					return false;
				}
			}
		}
		return true;	
	}

	@Override
	public MensajeDTO envioPDFEmail(String id, Locale locale, String isPoliza){
		List<String> address = new ArrayList<String>(1);
		List<ByteArrayAttachment> attachments = new ArrayList<ByteArrayAttachment>(1);
		TransporteImpresionDTO transporte = new TransporteImpresionDTO();
		MensajeDTO mensaje = new MensajeDTO();
		ResumenCostosDTO resumen = new ResumenCostosDTO();
		String nameFile = UtileriasWeb.STRING_EMPTY;
		String subject = "Resumen de la ";
		String template = UtileriasWeb.STRING_EMPTY;
		String emailContratante = getMailContratate(id, isPoliza);
		
		if(emailContratante==null || emailContratante.isEmpty()){
			throw new ApplicationException("No hay información de Contratante, para envio de correo");
		}
		
		String tipo = UtileriasWeb.STRING_EMPTY;
		if(ESCOTIZACION.equalsIgnoreCase(isPoliza)){
			CotizacionDTO cotizacion = validaCotizacion(id);
			nameFile = "cotizacion_"+id+".pdf";			
			transporte = generarPlantillaReporte.imprimirCotizacion(
						BigDecimal.valueOf(Long.valueOf(id)), locale, true, true, false);
			tipo += "Cotización No."+id;
			resumen =  calculoService.obtenerResumenCotizacion(cotizacion, false);
			template = getTemplateMail(resumen, id, "Cotización");
			
		}else if(ESPOLIZA.equalsIgnoreCase(isPoliza)){
			PolizaDTO poliza = validaPolizaDTO(id);			
			nameFile = "Poliza_"+poliza.getNumeroPolizaFormateada()+".pdf";
			transporte = generaPlantillaPoliza(id, locale, emailContratante, false);
			tipo += "Póliza No."+poliza.getNumeroPolizaFormateada();
			resumen = calculoService.resumenCostosCaratulaPoliza(poliza.getIdToPoliza());
			template = getTemplateMail(resumen, poliza.getNumeroPolizaFormateada(), "Póliza");
		}
		if(transporte!=null){
			ByteArrayAttachment attachmentFile = new ByteArrayAttachment(); 
			attachmentFile.setTipoArchivo(TipoArchivo.PDF);
			attachmentFile.setNombreArchivo(nameFile);
			attachmentFile.setContenidoArchivo(transporte.getByteArray());
			attachments.add(attachmentFile);
			address.add(emailContratante);
			
			try{
				mailService.sendMail(address, subject+tipo, template, attachments);
				LOG.info("despues de enviar mail");
				mensaje.setMensaje("Se envió "+tipo+" al correo "+emailContratante+".");
				mensaje.setTipoMensaje(PolizaDTO.ICONO_CONFIRM);
			}catch(Exception e){
				LOG.info(e.getMessage(), e);
				throw new ApplicationException("Error al enviar correo, "+emailContratante);
			}	
		}else{
			throw new ApplicationException("Error al enviar correo, no se puede generar el PDF de la "+tipo);
		}
		return mensaje;
	}

	private String getTemplateMail(ResumenCostosDTO resumen, String id, String isPoliza) {
		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("P_ID", ID_TEMPLATE_ENVIO_PDF);
		
		Map<String, Object> execute = getTemplateEmailSO.execute(parameter);
		
		String template = (String) execute.get("P_TEMPLATE");
		
		Map<String, String> param = new HashMap<String, String>(1);
		param.put("\\$id", id);
		param.put("\\$isPoliza", isPoliza);
		param.put("\\$primaNetaConberturasPropias",resumen.getPrimaNetaConberturasPropias().toString());
		param.put("\\$descuentoComisionCedida",resumen.getDescuentoComisionCedida().toString());
		param.put("\\$totalPrimas",resumen.getTotalPrimas().toString());
		param.put("\\$recargo",resumen.getRecargo().toString());
		param.put("\\$derechos",resumen.getDerechos().toString());
		param.put("\\$iva",resumen.getIva().toString());
		param.put("\\$primaTotal",resumen.getPrimaTotal().toString());
		param.put("\\$application.path", getDirectorioImagenesMailTemplate());
		
		template = setParameterTemplate(param, template);		
		
		return template.toString();
	}
	
	/**
	 * Se valida que exista cotizacion en la base de datos para el Id
	 * @param id
	 * @return
	 */
	private CotizacionDTO validaCotizacion(String id) {
		CotizacionDTO cotizacion = new CotizacionDTO(); 
		if(id!=null){
			BigDecimal idToCotizacion = BigDecimal.valueOf(Long.valueOf(id));
			cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
			if(cotizacion==null){
				throw new ApplicationException("No se encontro cotizacion para el numero"+id);
			}
		}else{
			throw new ApplicationException("No se encontro cotizacion para el numero");
		}
		return cotizacion;
	}

	/**
	 * Se valida que la poliza a buscar se encuentre en la bd
	 * @param id
	 * @return
	 */
	private PolizaDTO validaPolizaDTO(String id) {
		PolizaDTO poliza = new PolizaDTO();
		if(id!=null){
			BigDecimal idToPoliza = BigDecimal.valueOf(Long.valueOf(id));
			poliza = entidadService.findById(PolizaDTO.class, idToPoliza);
			if(poliza==null){
				throw new ApplicationException("No existe P\u00f3liza para el idToPoliza: "+id);
			}
		}else{
			throw new ApplicationException("No se encontro cotizacion para el numero");
		}
		return poliza;
	}
	
	private TransporteImpresionDTO generaPlantillaPoliza(String id, Locale locale, boolean isMovil){
			String eMail = getMailContratate(id, ESPOLIZA);
			return generaPlantillaPoliza(id, locale, eMail, isMovil);
	}

	private TransporteImpresionDTO generaPlantillaPoliza(String id, Locale locale, 
			String eMail, boolean isMovil) {
		TransporteImpresionDTO pdf = new TransporteImpresionDTO();	
		PolizaDTO poliza = validaPolizaDTO(id);
		
		List<EndosoDTO> endosoPolizaList = endosoService.findByPropertyWithDescriptions("id.idToPoliza", 
				poliza.getIdToPoliza(), Boolean.TRUE);
		
		if(endosoPolizaList != null && endosoPolizaList.size()>0){
			EndosoDTO ultimoEndosoDTO = endosoPolizaList.get(endosoPolizaList.size() - 1);
			if(isMovil){
				pdf = generaPlantillaReporteBitemporalService.imprimirPolizaSO(
						poliza.getIdToPoliza(), locale, new DateTime(ultimoEndosoDTO.getValidFrom()), 
						new DateTime(ultimoEndosoDTO.getRecordFrom().getTime()), ultimoEndosoDTO.getClaveTipoEndoso(),eMail);
			}else{
				pdf = generaPlantillaReporteBitemporalService.imprimirPoliza(poliza.getIdToPoliza(), locale, new DateTime(ultimoEndosoDTO.getValidFrom()),
						new DateTime(ultimoEndosoDTO.getRecordFrom().getTime()), true, false, true, false, false, ultimoEndosoDTO.getClaveTipoEndoso(), false);
				pdf.setContentType(ConstantesReporte.TIPO_PDF);
				pdf.setFileName("Poliza_"+poliza.getNumeroPolizaFormateada()+".pdf");
			}
		}
		return pdf;
	}
	
	@Override
	public String generaPlantillaPoliza(String id, String isPoliza, Locale locale) {
		StringBuilder plantilla = new StringBuilder("");

		TransporteImpresionDTO pdf = new TransporteImpresionDTO();	
		pdf = getPDFSeguroObligatorio(id, isPoliza, locale, "S") ;
		
		InputStream is = new ByteArrayInputStream(pdf.getByteArray()); 
		BufferedReader br = new BufferedReader(new InputStreamReader(is));

		String line;
		try {
			while ((line = br.readLine()) != null) {
				plantilla.append(line).append("\n");
			}
			br.close();
		} catch (IOException e) {
			LOG.error(e);
		}

		return plantilla.toString();
	}
	
	private TransporteImpresionDTO generaPlantillaCotizacion(String id, Locale locale, boolean isMovil) {
		BigDecimal idToCotizacion = BigDecimal.valueOf(Long.valueOf(id));
		return generarPlantillaReporte.imprimirCotizacion(idToCotizacion, locale, true, true, isMovil,"", false);
	}
	
	private String getMailContratate(String id, String isPoliza) {
		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("P_ID", BigDecimal.valueOf(Long.valueOf(id)));
		parameter.addValue("P_ISPOLIZA", isPoliza);
		
		Map<String, Object> execute = getEmailContratanteSO.execute(parameter);
		
		String email = (String) execute.get("P_EMAIL");
		
		return email;
	}
	
	public String setParameterTemplate(Map<String, String> map, String template){
		Iterator<Entry<String, String>> it = map.entrySet().iterator();
		String nuevoTemplate = template;
		while(it.hasNext()){
			Map.Entry<String, String> pairs = it.next();
			nuevoTemplate = nuevoTemplate.replaceAll(pairs.getKey(), pairs.getValue());
		}
		
		return nuevoTemplate;
	}
	
	private String getDirectorioImagenesMailTemplate() {
		return sistemaContext.getDirectorioImagenes();
	}

	@Override
	public List<ClienteDTO> buscarCliente(ClienteDTO cliente)
			throws ApplicationException {
		List<ClienteDTO> listaClientes = new ArrayList<ClienteDTO>(1);
		
		try {
			//No consultar el listado si no se capturan datos
			if(cliente != null && ((cliente.getNombre() != null && !cliente.getNombre().equals(UtileriasWeb.STRING_EMPTY) ) ||
					(cliente.getApellidoPaterno() != null && !cliente.getApellidoPaterno().equals(UtileriasWeb.STRING_EMPTY) ) ||
					(cliente.getApellidoMaterno() != null && !cliente.getApellidoMaterno().equals(UtileriasWeb.STRING_EMPTY) ) )){
				listaClientes = clienteFacade.listarFiltrado(cliente, null);
				LOG.info("--> listaClientes.size():"+listaClientes.size());
			}
		} catch (Exception e) {
			LOG.error("Error al buscar el cliente:\n"+e.getMessage());
			throw new ApplicationException("Error al buscar el cliente");
		}
		
		return listaClientes;
	}
	
	
	@Override
	public ClienteDTO getClienteById(BigDecimal idCliente, String userName)
			throws ApplicationException {
		ClienteDTO asegurado = null;
		
		try {
			asegurado = clienteFacade.findById(idCliente, userName);
		} catch(Exception e){
			LOG.error("Error al buscar el cliente by id "+idCliente+" :\n"+e.getMessage());
			throw new ApplicationException("Error al buscar el cliente by id "+idCliente);
		}
		
		return asegurado;
	}

	@Override
	public BigDecimal guardarCliente(ClienteGenericoDTO cliente)
			throws ApplicationException {
		try {
			if(cliente!=null){
				cliente.setIdCliente(getBigDecimalValue(cliente.getIdClienteString()));
				cliente.setIdToPersona(getBigDecimalValue(cliente.getIdToPersonaString()));
				if(!UtileriasWeb.esCadenaVacia(cliente.getClaveTipoPersonaString())){
					cliente.setClaveTipoPersona(Short.parseShort(cliente.getClaveTipoPersonaString()));
				}
				cliente.setIdEstadoNacimiento(getBigDecimalValue(cliente.getIdEstadoNacimientoString()));
				cliente.setIdColonia(getBigDecimalValue(cliente.getIdColoniaString()));
				cliente.setIdMunicipio(getBigDecimalValue(cliente.getIdMunicipioString()));
				cliente.setIdEstado(getBigDecimalValue(cliente.getIdEstadoString()));
				
				if(!UtileriasWeb.esCadenaVacia(cliente.getCodigoRFC())){
					cliente.setCodigoRFCFiscal(cliente.getCodigoRFC());
				}
				if(!UtileriasWeb.esCadenaVacia(cliente.getTelefono())){
					cliente.setTelefonoFiscal(cliente.getTelefono());
				}
			}
			cliente=clienteFacade.saveFullData(cliente, null, false);
		} catch (Exception e) {
			LOG.info("--> error al guardar el cliente:\n"+e.getMessage());
			throw new ApplicationException("Error al guardar el cliente");
		}
		return cliente.getIdCliente();
	}
	
	private BigDecimal getBigDecimalValue(String idString){
		BigDecimal id = null;
		if(!UtileriasWeb.esCadenaVacia(idString)){
			id = BigDecimal.valueOf(Long.valueOf(idString));
		}
		return id;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Object> emitir(String id){
		HashMap<String, Object> map = new HashMap<String, Object>(1);
		MensajeDTO msn = new MensajeDTO();
		CotizacionDTO cotizacion = validaCotizacion(id);
		LOG.info("Entrando a emitir cotizacion : " + cotizacion.getIdToCotizacion());
		try{						
			emisionService.validacionPreviaRecibos(cotizacion.getIdToCotizacion());
			TerminarCotizacionDTO validacion = cotizacionService.validarEmisionCotizacion(cotizacion.getIdToCotizacion());
			if(validacion != null){
				if(validacion.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES.getEstatus().shortValue()){
					List<ExcepcionSuscripcionReporteDTO> excepcionesList = validacion.getExcepcionesList();
					if(excepcionesList != null && !excepcionesList.isEmpty()){
						//Crea solicitudes de autorizacion para ser validadas despues
						autorizacionService.guardarSolicitudDeAutorizacion(cotizacion.getIdToCotizacion(), UtileriasWeb.STRING_EMPTY, 
								usuarioService.getUsuarioActual().getId().longValue(), null, Negocio.CLAVE_NEGOCIO_AUTOS, excepcionesList);
						throw new RuntimeException("La cotizacion cuenta con Solicitudes de Autorizacion pendientes");
					}
				}else{
					throw new RuntimeException("Se encontraron algunos errores en al Emitir");
				}
			}
			Map<String, String> mensajeEmision = emisionService.emitir(cotizacion, false);
			String icono = mensajeEmision.get(PolizaDTO.ICONO_EMISION);
			String mensaje = mensajeEmision.get(PolizaDTO.MENSAJE_EMISON);
			msn.setTipoMensaje(icono);
			
			if(icono.equals(PolizaDTO.ICONO_CONFIRM)){
				//Se borran registros de datos de la solicitud si fuera una solicitud de poliza en tramite
				solicitudDataEnTramiteService.deleteBySolicitud(cotizacion.getIdToCotizacion());
				saveDocumentoDigitalSolicitud(cotizacion.getSolicitudDTO().getIdToSolicitud());
				
				String idPoliza = mensajeEmision.get(PolizaDTO.IDPOLIZA_EMISON);
				PolizaDTO poliza = entidadService.findById(PolizaDTO.class, BigDecimal.valueOf(Long.valueOf(idPoliza)));
				try{
					envioPDFEmail(idPoliza, null, ESPOLIZA);
				}catch(RuntimeException e){
					LOG.error("No fue posible enviar la poliza al correo "+ e.getMessage(), e);
				}
				LOG.info("Saliendo de emitirCotizacion idCotizaci\u00f3n : " + cotizacion.getIdToCotizacion());
				mensaje += " " + poliza.getNumeroPolizaFormateada();
				msn.setMensaje(mensaje);
				map.put("mensaje", msn);
				map.put("idToPoliza", poliza.getIdToPoliza());
			}
		}catch(RuntimeException e){
			LOG.info("-- ERROR: " + e.getMessage(), e);
			throw new ApplicationException(e.getMessage());
		}catch(Exception e){
			LOG.info("-- ERROR: " + e.getMessage(), e);
			throw new ApplicationException("Error al emitir la poliza");
		}
		return map;
	}
	
	private void saveDocumentoDigitalSolicitud(BigDecimal idToSolicitud){
		Map<String,Object> params = new HashMap<String, Object>(1);
		params.put("solicitudDTO.idToSolicitud", idToSolicitud);
		params.put("esCartaCobertura", 1);
		List<DocumentoDigitalSolicitudDTO> documentosDigitalesSolicitudDTO =  entidadService.findByProperties(
				DocumentoDigitalSolicitudDTO.class, params);
		if(documentosDigitalesSolicitudDTO!=null && !documentosDigitalesSolicitudDTO.isEmpty()){
			DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO = documentosDigitalesSolicitudDTO.get(0);
			documentoDigitalSolicitudDTO.setFiles(null);
			//Se borran los bytes del archivo carta cobertura
			entidadService.save(documentoDigitalSolicitudDTO);
		}
	}

	@Override
	public Map<String, Object> getDatosCotizacion(CotizacionDTO cotizacion)
			throws ApplicationException {
		Map<String, Object> map = new HashMap<String, Object>(1);
		
		map.put("primaTarifa", cotizacion.getPrimaTarifa());
		map.putAll(getDatosInicialesCSO(null));
		map.put("folio", cotizacion.getFolio());
		if(cotizacion.getIdToPersonaContratante()!=null){
			map.put("cliente", getClienteById(cotizacion.getIdToPersonaContratante(), cotizacion.getCodigoUsuarioCotizacion()));
		}

		map.put("nombreContratante", (cotizacion.getNombreContratante() != null)? cotizacion.getNombreContratante(): "");
		map.putAll(getIncisoAutoCotByIdCotizacion(cotizacion.getIdToCotizacion().toString()));
		
		return map;
	}
	
	@Override
	public Map<String, Object> getIncisoAutoCotByIdCotizacion(String idToCotizacion) 
	throws ApplicationException {
		Map<String, Object> map = new HashMap<String, Object>(1);
		
		IncisoAutoCot incisoAutoCot = null;
		IncisoCotizacionId id = new IncisoCotizacionId(BigDecimal.valueOf(Long.valueOf(idToCotizacion)), BigDecimal.ONE);

		IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, id);
		
		if (inciso != null && inciso.getIncisoAutoCot()!=null){
			incisoAutoCot = inciso.getIncisoAutoCot();
			map.put("idMarca", incisoAutoCot.getMarcaId());
			map.put("idModelo", incisoAutoCot.getModeloVehiculo());
			map.put("nombreProspecto", incisoAutoCot.getNombreAsegurado());
			map.put("numeroMotor", incisoAutoCot.getNumeroMotor());
			map.put("numeroSerie", incisoAutoCot.getNumeroSerie());
			map.put("placa", incisoAutoCot.getPlaca());
			map.put("observacionesInciso", incisoAutoCot.getObservacionesinciso());
			map.put("idEstilo", incisoAutoCot.getEstiloId());
			map.put("idEstado", incisoAutoCot.getEstadoId());
			map.put("idMunicipio", incisoAutoCot.getMunicipioId());
			map.put("idMoneda", incisoAutoCot.getIdMoneda());
			map.put("descripcionFinal", incisoAutoCot.getDescripcionFinal());
			map.put("repuve", incisoAutoCot.getRepuve());
			map.put("idAgrupadorPasajeros", incisoAutoCot.getIdAgrupadorPasajeros());
			map.put("numeroLicencia", incisoAutoCot.getNumeroLicencia());
			map.put("observacionesInciso", incisoAutoCot.getObservacionesinciso());
		}
		
		return map;
	}

	@Override
	public Map<String, String> getEstiloPorMarcaNegocioyDescripcion(String idToCotizacion
			, IncisoAutoCot incisoAutoCot){

		Map<String, String> estiloList = new HashMap<String,String>(1);
		IncisoCotizacionId id = new IncisoCotizacionId(BigDecimal.valueOf(Long.valueOf(idToCotizacion)), BigDecimal.ONE);
		IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, id);
		if (inciso != null && inciso.getIncisoAutoCot()!=null){
			IncisoAutoCot incisoAutoCotGuardado = inciso.getIncisoAutoCot();
			if(incisoAutoCot.getDescripcionFinal() != null){
				estiloList = listadoService.
					getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMonedaAgente(
							incisoAutoCot.getMarcaId(), incisoAutoCot.getModeloVehiculo(), 
							BigDecimal.valueOf(incisoAutoCotGuardado.getNegocioSeccionId()), 
							new BigDecimal(incisoAutoCotGuardado.getIdMoneda()),
							null, incisoAutoCot.getDescripcionFinal(), 
							incisoAutoCotGuardado.getIdAgrupadorPasajeros());
			}
		}
		
		return estiloList;		
	}

	private Long getIdToNegocioBaseCSO() {
		Long valor = null;
		ParametroGeneralId id = new ParametroGeneralId(
				ParametroGeneralDTO.GRUPO_PARAM_GENERAL_MOVIL_SO,
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_ID_NEGOCIO_BASE);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		if(parametro!=null && parametro.getValor()!=null){
			valor = Long.valueOf(parametro.getValor());
		}
		return valor;
	}

	private Map<String, String> getListEstadosByIdNegocio(Long idToNegocio)
			throws ApplicationException {
		
		LOG.info("obteniendo los estados del negocio "+idToNegocio);
		Map<String, String> map = new LinkedHashMap<String, String>(1); 
		
		try {
			map = listadoService.getEstadosPorNegocioId(idToNegocio, false);
		} catch(Exception e){
			LOG.error("Error obteniendo la lista de estados por negocio\n"+e.getMessage());
			throw new ApplicationException(e.getMessage());
		}
		
		return map;
	}
	
	private BigDecimal getClaveAgenteUsuario(){
		BigDecimal claveAgente = null;
		if(usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")
				|| usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
			LOG.info("El usuario actual tiene el permiso FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual o FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual");
			LOG.info("Obtener el Agente de acuerdo al usuario Actual");
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			if (agenteUsuarioActual!=null){
				claveAgente = BigDecimal.valueOf(agenteUsuarioActual.getId());
			}
		}
		return claveAgente;
	}
}