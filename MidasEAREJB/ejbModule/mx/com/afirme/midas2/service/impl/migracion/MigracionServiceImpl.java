package mx.com.afirme.midas2.service.impl.migracion;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoFacadeRemote;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerResponse;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerService;
import mx.com.afirme.midas.sistema.controlArchivo.migracion.MigracionControlArchivoAutoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.migracion.MigracionControlArchivoAutoDTO.Estatus;
import mx.com.afirme.midas.sistema.controlArchivo.migracion.MigracionControlArchivoAutoFacadeRemote;
import mx.com.afirme.midas2.domain.migracion.MigEndososValidos;
import mx.com.afirme.midas2.domain.migracion.MigEndososValidosEd;
import mx.com.afirme.midas2.domain.migracion.PkgAutMigracion;
import mx.com.afirme.midas2.service.migracion.EstadoMigracion;
import mx.com.afirme.midas2.service.migracion.MigracionEndosoPorPolizaService;
import mx.com.afirme.midas2.service.migracion.MigracionService;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

@Stateless
public class MigracionServiceImpl implements MigracionService {
	
	private static final Logger LOG = Logger.getLogger(MigracionServiceImpl.class);
	private static EstadoMigracion ESTADO = EstadoMigracion.APAGADA;
	private static EstadoMigracion ESTADOED = EstadoMigracion.APAGADA;
	
	/**
	 * Regresa el estado en la que está actualmente la migración.
	 * @return
	 */
	
	private EntityManager entityManager;
	private MigracionEndosoPorPolizaService migracionEndosoPorPolizaService;
	private PkgAutMigracion pkgAutMigracion;
	
	@EJB
	private MigracionControlArchivoAutoFacadeRemote migracionControlArchivoAutoFacade;
	
	@EJB
	private ControlArchivoFacadeRemote controlArchivoFacade;
	
	@EJB
	private FileManagerService fileManagerService;
	
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@EJB
	public void setMigracionEndosoPorPolizaService(
			MigracionEndosoPorPolizaService migracionEndosoPorPolizaService) {
		this.migracionEndosoPorPolizaService = migracionEndosoPorPolizaService;
	}
	
	@EJB
	public void setPkgAutMigracion(PkgAutMigracion pkgAutMigracion) {
		this.pkgAutMigracion = pkgAutMigracion;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void migrarEndosos() {
		if (getEstadoMigracion().equals(EstadoMigracion.INICIADA)) {
			throw new RuntimeException(
					"La migracion ya había sido iniciada anteriormente. " +
					"Detenga la migración y espere unos minutos a que se detenga el proceso. " +
					"Después vuelva a intentar ejecutar la migración.");
		}
		ESTADO = EstadoMigracion.INICIADA;
		
		List<Long> idCotizaciones;

		String jpql = "select m.id.idCotizacion from MigEndososValidos m where m.claveProceso = :claveProceso group by m.id.idCotizacion";
		//Limpiar cache para asegurar que los datos se actualicen.
		entityManager.getEntityManagerFactory().getCache().evictAll();
		TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
		query.setParameter("claveProceso", MigEndososValidos.CLAVE_PROCESO_NO_PROCESADO);
		idCotizaciones = query.getResultList();		
		LOG.info("Total de polizas con endosos a procesar: " + idCotizaciones.size());
		
		for (int i = 0 ; i < idCotizaciones.size(); i++) {
			//Se ha ejecutado la señal de que debemos detener la migración.
			if (getEstadoMigracion().equals(EstadoMigracion.DETENIDA)) {
				return;
			}
			Long idCotizacion = idCotizaciones.get(i);
			LOG.info("Llamadas asincronas, programando idCotizacion:  " + idCotizacion 
					+ " " + (i+1)+ "/" + idCotizaciones.size());
			String jpql2 = "select m from MigEndososValidos m where m.id.idCotizacion = :idCotizacion order by m.id.idVersionPol";
			TypedQuery<MigEndososValidos> query2 = entityManager.createQuery(jpql2, MigEndososValidos.class);
			query2.setParameter("idCotizacion", idCotizacion);
			List<MigEndososValidos> migEndososValidosList = query2.getResultList();			
			LOG.info("Total de endosos de idCotizacion " + idCotizacion + ": " + migEndososValidosList.size());
			migracionEndosoPorPolizaService.migrarEndosos(migEndososValidosList);
		}
		ESTADO = EstadoMigracion.APAGADA;
	}

	@Override
	public void detenerMigracionEndosos() {
		LOG.info("Se empieza con el proceso para detener la migración de endosos que estaban siendo procesados.");
		ESTADO = EstadoMigracion.DETENIDA;
	}
	
	@Override
	public void detenerMigracionEndososEmisionDelegada() {
		LOG.info("Se empieza con el proceso para detener la migración de endosos de Emisión Delegada que estaban siendo procesados.");
		ESTADOED = EstadoMigracion.DETENIDA;
	}

	@Override
	public EstadoMigracion getEstadoMigracion() {
		return ESTADO;
	}
	
	@Override
	public EstadoMigracion getEstadoMigracionEd() {
		return ESTADOED;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	//@Schedule(minute="*/10", hour="*", persistent=false) //Deshabilitado para DEV
	public void migrarEndososEmisionDelegada() {
		if (getEstadoMigracionEd().equals(EstadoMigracion.INICIADA)) {
			LOG.error("La migracion de endosos de Emisión Delegada ya había sido iniciada anteriormente. Espere a que finalice.");
			return;
		}
		ESTADOED = EstadoMigracion.INICIADA;
		
		LOG.info("Comenzando con el proceso de migracion de endosos de Emisión Delegada.");
		LOG.info("Migrando pólizas de Emisión Delegada.");
		try {
			pkgAutMigracion.migraPolizasEd();
		} catch(Exception e) {
			LOG.error("No se pudo migrar las polizas de Emisión Delegada. Se termina con el proceso de migracion.", e);
			ESTADOED = EstadoMigracion.APAGADA;
			return;
		}
		LOG.info("Las pólizas de Emisión Delegada fueron migradas exitosamente.");
		
		try {
			List<Long> idCotizaciones;
			String jpql = "select m.id.idCotizacion from MigEndososValidosEd m where m.claveProceso = :claveProceso group by m.id.idCotizacion";
			//Limpiar cache para asegurar que los datos se actualicen.
			entityManager.getEntityManagerFactory().getCache().evict(MigEndososValidosEd.class);
			TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
			query.setParameter("claveProceso", MigEndososValidosEd.CLAVE_PROCESO_NO_PROCESADO);
			idCotizaciones = query.getResultList();		
			LOG.info("Total de polizas con endosos a procesar: " + idCotizaciones.size());
			
			for (int i = 0 ; i < idCotizaciones.size(); i++) {
				//Se ha ejecutado la señal de que debemos detener la migración.
				if (getEstadoMigracionEd().equals(EstadoMigracion.DETENIDA)) {
					LOG.info("Se suspende la migracion de endosos de Emisión Delegada debido a que se solicitó que se detuviera.");
					return;
				}
				Long idCotizacion = idCotizaciones.get(i);
				LOG.info("Procesando idCotizacion:  " + idCotizacion 
						+ " " + (i+1)+ "/" + idCotizaciones.size());
				String jpql2 = "select m from MigEndososValidosEd m where m.id.idCotizacion = :idCotizacion order by m.id.idVersionPol";
				TypedQuery<MigEndososValidosEd> query2 = entityManager.createQuery(jpql2, MigEndososValidosEd.class);
				query2.setParameter("idCotizacion", idCotizacion);
				List<MigEndososValidosEd> migEndososValidosEdList = query2.getResultList();			
				LOG.info("Total de endosos de idCotizacion " + idCotizacion + ": " + migEndososValidosEdList.size());
				migracionEndosoPorPolizaService.migrarEndososEmisionDelegada(migEndososValidosEdList);
			}
		} catch(Throwable e) {
			LOG.error("No fue posible migrar los endosos de Emisión Delegada.", e);
		}
		
		/*
		 * Valida si faltan endosos o polizas de migrarse de midas a seycos
		 * Solo valida lo creado hasta el dia anterior
		 */
		LOG.info("Migrando endosos de Midas.");
		try{			
			pkgAutMigracion.migraEndososMidas();			
		}catch(Exception e){
			LOG.error("No fue posible migrar los endosos de Midas.", e);
		}
		LOG.info("Los endosos de Midas fueron migrados exitosamente.");
		
		ESTADOED = EstadoMigracion.APAGADA;
	}
	
	/**
	 * Mueve los archivos del sistema de archivos del servidor hacia el nuevo
	 * repositorio (Fortimax).
	 */
	//@Schedule(minute="*/10", hour="*", persistent=false) //Deshabilitado para DEV
	public void migrarArchivosFortimax() {
		LOG.info("Inicia migrado de archivos a Fortimax.");
		try{
			String switchMigracion = parametroGeneralFacade.findById(
					new ParametroGeneralId(FileManagerService.GRUPO_PARAMETRO_FORTIMAX,FileManagerService.PARAMETRO_SWITCH_MIGRACION), true).getValor();			
			
			if(switchMigracion == null || Integer.parseInt(switchMigracion) == 1){
				final List<MigracionControlArchivoAutoDTO> pendientes = migracionControlArchivoAutoFacade.findByEstatus(Estatus.PENDIENTE);
				for (MigracionControlArchivoAutoDTO controlMigracion : pendientes) {
					final ControlArchivoDTO controlArchivo = controlMigracion.getControlArchivo();
					final String fileName = fileManagerService.getFileName(controlArchivo);
					int claveTipo = Integer.parseInt(controlArchivo.getClaveTipo()); 
					FileManagerResponse result = null;

					Map<String,Object> parametros = new HashMap<String,Object>();
					parametros.put("tipo",claveTipo);
					parametros.put("highAvailability",false);
					
					parametros.put("expediente", controlArchivo.getIdToControlArchivo().toString());

					try {
						final byte[] bytes = fileManagerService.downloadFileFromDisk(fileName);
						if(bytes == null) {
							throw new Exception (String.format("No se encontró el archivo %s en el sistema de archivos", fileName ));
						}
						
						result  = fileManagerService.uploadFileFortimax(fileName, controlArchivo.getIdToControlArchivo().toString(), bytes,parametros);

						if (result==null) {
							throw new Exception (String.format("Falló la carga del archivo %s", fileName));
						}
						controlMigracion.setEstatus(Estatus.MIGRADO.getValor());
						controlMigracion.setDetalleEstatus("Migrado");
						controlMigracion.setFechaEstatus(new Date());
						migracionControlArchivoAutoFacade.update(controlMigracion);
						
						controlArchivo.setCodigoExterno(result.getNodo());
						controlArchivoFacade.update(controlArchivo);
					} catch (Exception e) {
						LOG.error("-- migrar()", e);
						controlMigracion.setEstatus(Estatus.ERROR.getValor());
						controlMigracion.setDetalleEstatus(e.getMessage() + "\n" + ExceptionUtils.getStackTrace(e));
						controlMigracion.setFechaEstatus(new Date());
						migracionControlArchivoAutoFacade.update(controlMigracion);
					}			
				}
			}
		}catch(Exception e){
			LOG.error("-- migrar()", e);
		}
		LOG.info("Termina migrado de archivos a Fortimax.");
		}
}
