/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaRangosDao;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.domain.compensaciones.CaRangos;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoRango;
import mx.com.afirme.midas2.service.compensaciones.CaRangosService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoRangoService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;

import org.apache.log4j.Logger;

@Stateless

public class CaRangosServiceImpl  implements CaRangosService {
	public static final String NOMBRE = "nombre";
	public static final String VALORCOMPENSACION = "valorCompensacion";
	public static final String VALORMINIMO = "valorMinimo";
	public static final String VALORMAXIMO = "valorMaximo";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	public static final String PARAMETROSGRALESID = "caParametros.id";

	@EJB
	private CaRangosDao rangoscaDao;
	
	@EJB
	private CaTipoRangoService caTipoRangoService;
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaRangosServiceImpl.class);
    
		/**
	 Perform an initial save of a previously unsaved CaRangos entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaRangos entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaRangos entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaRangos 	::		CaRangosServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	rangoscaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaRangos 	::		CaRangosServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaRangos 	::		CaRangosServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaRangos entity.
	  @param entity CaRangos entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaRangos entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaRangos 	::		CaRangosServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaRangos.class, entity.getId());
	        	rangoscaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaRangos 	::		CaRangosServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaRangos 	::		CaRangosServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaRangos entity and return it or a copy of it to the sender. 
	 A copy of the CaRangos entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaRangos entity to update
	 @return CaRangos the persisted CaRangos entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaRangos update(CaRangos entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaRangos 	::		CaRangosServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaRangos result = rangoscaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaRangos 	::		CaRangosServiceImpl	::	update	::	FIN	::	");
	        return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaRangos 	::		CaRangosServiceImpl	::	update	::	ERROR	::	",re);
        	throw re;
        }
    }
    
    public CaRangos findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaRangosServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaRangos instance = rangoscaDao.findById(id);//entityManager.find(CaRangos.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaRangosServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaRangosServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaRangos entities with a specific property value.  
	 
	  @param propertyName the name of the CaRangos property to query
	  @param value the property value to match
	  	  @return List<CaRangos> found by query
	 */
    public List<CaRangos> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaRangosServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaRangos model where model." 
//			 						+ propertyName + "= :propertyValue" + " and model." + BORRADOLOGICO + " = :borradoLogico";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
//			query.setParameter("borradoLogico", ConstantesCompensacionesAdicionales.borradoLogicoNo);			
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaRangosServiceImpl	::	findByProperty	::	FIN	::	");
			return rangoscaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaRangosServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaRangos> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaRangos> findByValorminimo(Object valorminimo
	) {
		return findByProperty(VALORMINIMO, valorminimo
		);
	}
	
	public List<CaRangos> findByValormaximo(Object valormaximo
	) {
		return findByProperty(VALORMAXIMO, valormaximo
		);
	}
	
	public List<CaRangos> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaRangos> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	public List<CaRangos> findByValorcompensacion(Object valorcompensacion
	) {
		return findByProperty(VALORCOMPENSACION, valorcompensacion
		);
	}
	
	
	/**
	 * Find all CaRangos entities.
	  	  @return List<CaRangos> all CaRangos entities
	 */
	public List<CaRangos> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaRangosServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaRangos model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaRangosServiceImpl	::	findAll	::	FIN	::	");
			return rangoscaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaRangosServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
	public List<CaRangos> findAllbyParametrosGralesId(Long parametrosGralesId){
		return findByProperty(PARAMETROSGRALESID, parametrosGralesId);		
	}
	
	/**
	 * 
	 * @param caParametros
	 * @param rangos
	 */
	public void guardarRangos(CaParametros caParametros, String rangos){
		if(rangos != null && !rangos.trim().isEmpty()){
			
			this.deleteRangos(caParametros);
			
			StringTokenizer tokenRangos = new StringTokenizer(rangos,ConstantesCompensacionesAdicionales.TOKENLIMITADOR);
			
			while(tokenRangos.hasMoreElements()){
				
				String nivel = tokenRangos.nextToken();
				
				int indiceNombre = nivel.indexOf("nombreR");
				int indiceInicial = nivel.indexOf("inicialR");
				int indiceFinal = nivel.indexOf("finalR");
				int indiceCompensacion = nivel.indexOf("compensacionR");
				
				String nombreR = nivel.substring(indiceNombre + "nombreR".length(),indiceInicial);
				String inicialR = nivel.substring(indiceInicial + "inicialR".length(),indiceFinal);
				String finalR = nivel.substring(indiceFinal + "fianlR".length(),indiceCompensacion);
				String compensacionR = nivel.substring(indiceCompensacion + "compensacionR".length(),nivel.length());
				
				CaTipoRango tipoRango = caTipoRangoService.findAll().get(0);
				CaRangos rangoPersistir = new CaRangos();							
				rangoPersistir.setCaParametros(caParametros);
				rangoPersistir.setCaTipoRango(tipoRango);
				rangoPersistir.setNombre(nombreR);
				rangoPersistir.setValorCompensacion(new Long(compensacionR));
				rangoPersistir.setValorMinimo(new BigDecimal(inicialR));
				rangoPersistir.setValorMaximo(new BigDecimal(finalR));
				rangoPersistir.setFechaCreacion(new Date());
				rangoPersistir.setFechamodificacion(new Date());
				rangoPersistir.setUsuario(caParametros.getUsuario());
				rangoPersistir.setBorradoLogico(ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
	            this.save(rangoPersistir);
			}
		}				
	}
	
	private void deleteRangos(CaParametros caParametros){
		List<CaRangos> listRangos = this.findAllbyParametrosGralesId(caParametros.getId());
		for(CaRangos caRangos: listRangos){
			this.delete(caRangos);
		}
	}
	
	public void guardarRangos(CaParametros caParametros, List<CaRangos> listCaRangos){
		
		if(listCaRangos != null && !listCaRangos.isEmpty()){
			CaRangos caRangosNew = null;
			CaTipoRango tipoRango = caTipoRangoService.findAll().get(0);
			for(CaRangos caRangos: listCaRangos){
				
				if(caRangos.getId()>0){
					caRangos.setCaParametros(caParametros);
					caRangos.setCaTipoRango(tipoRango);
					caRangos.setFechamodificacion(new Date());
					caRangos.setUsuario(caParametros.getUsuario());
					caRangos.setBorradoLogico(ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
					this.update(caRangos);
				}else{
					caRangosNew = new CaRangos();							
					caRangosNew.setCaParametros(caParametros);
					caRangosNew.setCaTipoRango(tipoRango);
					caRangosNew.setNombre(caRangos.getNombre());
					caRangosNew.setValorCompensacion(caRangos.getValorCompensacion());
					caRangosNew.setValorMinimo(caRangos.getValorMinimo());
					caRangosNew.setValorMaximo(caRangos.getValorMaximo());
					caRangosNew.setFechaCreacion(new Date());
					caRangosNew.setFechamodificacion(new Date());
					caRangosNew.setUsuario(caParametros.getUsuario());
					caRangosNew.setBorradoLogico(ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
					this.save(caRangosNew);
				}
			}			
		}
	}
	
}