package mx.com.afirme.midas2.service.impl.bitemporal.suscripcion.cotizacion.cobertura;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTOId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.catalogos.CobPaquetesSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.NegocioProductoDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.NegocioSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionDAO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.Cotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccionId;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNeg;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNegCobertura;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.cobertura.CoberturaBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.emision.ImpresionesServiceImpl;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.NegocioTipoPolizaService;
import mx.com.afirme.midas2.service.suscripcion.cambiosglobales.ConfiguracionPlantillaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.joda.time.DateTime;

@Stateless
public class CoberturaBitemporalServiceImpl implements
		CoberturaBitemporalService {

	public static List<CatalogoValorFijoDTO> posiblesDeducibles = null;

	@EJB
	private NegocioCobPaqSeccionDAO negocioCobPaqSeccionDAO;
	@EJB
	private CobPaquetesSeccionDao cobPaquetesSeccionDao;
	@EJB
	private EntidadService entidadService;
	@EJB
	private ConfiguracionPlantillaService configuracionPlantillaService;
	@EJB
	private NegocioProductoDao negocioProductoDao;
	@EJB
	private NegocioTipoPolizaService negocioTipoPolizaService;
	@EJB
	private NegocioSeccionDao negocioSeccionDao;
	@EJB
	private NegocioPaqueteSeccionDao negocioPaqueteSeccionDao;
	@EJB
	private EntidadBitemporalService entidadBitemporalService;
	@EJB
	private EntidadContinuityDao entidadContinuityDao;
	@EJB
	private CoberturaService coberturaService;
					
	@Override
	public List<BitemporalCoberturaSeccion> mostrarCoberturas(BitemporalAutoInciso autoInciso, Long cotizacionContinuityId, DateTime validoEn) {
		
		//Si el id de su inciso es nulo
		if (autoInciso.getEntidadContinuity().getParentContinuity().getId() == null) {
			//Se muestran las coberturas por caracteristicas del autoInciso
			return getCoberturas(autoInciso, cotizacionContinuityId, validoEn);
		} else {
			BitemporalAutoInciso autoIncisoOriginal = entidadBitemporalService.getInProcessByKey
				(AutoIncisoContinuity.class, autoInciso.getEntidadContinuity().getId(), validoEn); 
				
			//Si hubo cambios en autoInciso
			if (!autoInciso.getValue().equals(autoIncisoOriginal.getValue())) {
				
				BitemporalAutoInciso autoIncisoOriginalBefore = entidadBitemporalService.getByKey
					(AutoIncisoContinuity.class, autoInciso.getEntidadContinuity().getId(), validoEn);
				
				if (autoIncisoOriginalBefore != null && autoInciso.getValue().equals(autoIncisoOriginalBefore.getValue())) {
					//Se le da rollback a las coberturas para que queden como estuvieron originalmente (antes de calcular el endoso de movimientos)
					entidadContinuityDao.rollBackCoberturas(autoIncisoOriginalBefore.getEntidadContinuity().getIncisoContinuity()
							.getIncisoSeccionContinuities().getInProcess(validoEn).iterator().next().getEntidadContinuity().getId());
					//Se guarda el cambio de paquete y datos de AutoInciso
					autoIncisoOriginal.setValue(autoIncisoOriginalBefore.getValue());
					entidadBitemporalService.saveInProcess(autoIncisoOriginal, validoEn);
					//Se muestran las coberturas por el id del inciso
					return getCoberturas(autoInciso.getEntidadContinuity().getParentContinuity().getId(), validoEn, false); //Contratadas y no contratadas
				} else {
					//Se muestran las coberturas por caracteristicas del autoInciso
					return getCoberturas(autoInciso, cotizacionContinuityId, validoEn);
				}
			} else {
				//Se muestran las coberturas por el id del inciso
				return getCoberturas(autoInciso.getEntidadContinuity().getParentContinuity().getId(), validoEn, false); //Contratadas y no contratadas
			}
		}
		
	}
	
	@Override
	public List<BitemporalCoberturaSeccion> getCoberturas(
			Long incisoContinuityId, DateTime validoEn, boolean soloContratadas) {
		
		List<BitemporalCoberturaSeccion> coberturas = new ArrayList<BitemporalCoberturaSeccion>();
		List<NegocioCobPaqSeccion> coberturasNegocio = null;
		NegocioCobPaqSeccion coberturaNegocio = null;
		List<NegocioDeducibleCob> deducibles = null;
		Map<String, Object> params = null;
		
		BitemporalInciso inciso = entidadBitemporalService
			.prepareEndorsementBitemporalEntity(BitemporalInciso.class, incisoContinuityId, null, validoEn);
		
		List<BitemporalCoberturaSeccion> coberturasSeccion = (List<BitemporalCoberturaSeccion>) inciso.getEntidadContinuity().getIncisoSeccionContinuities()
			.getInProcess(validoEn).iterator().next().getEntidadContinuity().getCoberturaSeccionContinuities().getInProcess(validoEn);
		
		if (soloContratadas) {
			for (BitemporalCoberturaSeccion coberturaSeccion : coberturasSeccion) {
				if (coberturaSeccion.getValue().getClaveContrato().intValue() == 1) {
					coberturas.add(coberturaSeccion);
				}
			}
		} else {
			coberturas = coberturasSeccion;
		}
		
		
		//Se setean valores transient necesarios para que las coberturas se muestren correctamente
		
		if (posiblesDeducibles == null) {
			params = new LinkedHashMap<String, Object>();
			params.put("id.idGrupoValores", Integer.valueOf(38));
			posiblesDeducibles = entidadService.findByProperties(
					CatalogoValorFijoDTO.class, params);
		}
		
		coberturasNegocio = getCoberturasNegocio (inciso, validoEn);

		for (BitemporalCoberturaSeccion cobertura : coberturas) {
			
			coberturaNegocio = null;
			
			if (coberturasNegocio != null && coberturasNegocio.contains(cobertura)) {
				coberturaNegocio = coberturasNegocio.get(coberturasNegocio.indexOf(cobertura));
			}
			
			if (coberturaNegocio != null) {
				
				deducibles = entidadService
					.findByProperty(NegocioDeducibleCob.class,
						"negocioCobPaqSeccion.idToNegCobPaqSeccion",
						coberturaNegocio.getIdToNegCobPaqSeccion());
								
				cobertura.getValue().setValorSumaAseguradaMax(coberturaNegocio.getValorSumaAseguradaMax());
				
				cobertura.getValue().setValorSumaAseguradaMin(coberturaNegocio.getValorSumaAseguradaMin());
				
			} else {
				
				deducibles = new ArrayList<NegocioDeducibleCob>(1);
				
				cobertura.getValue().setValorSumaAseguradaMax(0D);
				
				cobertura.getValue().setValorSumaAseguradaMin(0D);
			}
			
			cobertura.getValue().setDeducibles(deducibles);
			
			cobertura.getValue().setDescripcionDeducible(
					getDescripcionDeducible(cobertura.getEntidadContinuity().getCoberturaDTO().getClaveTipoDeducible()));
			
			if (cobertura.getValue().getDescripcionDeducible() != null && cobertura.getValue().getDescripcionDeducible().indexOf(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF) >= 0){
				cobertura.getValue().setDescripcionDeducible(cobertura.getValue().getDescripcionDeducible().replace(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF, ImpresionesServiceImpl.TIPO_VALOR_SA_UMA));
			}
		}

		//Se ordenan
		Collections.sort(coberturas, new Comparator<BitemporalCoberturaSeccion>() {
	            @Override
	            public int compare(BitemporalCoberturaSeccion n1,
	                       BitemporalCoberturaSeccion n2) {
	                 return n1.getContinuity().getCoberturaDTO().getNumeroSecuencia()
	                             .compareTo(n2.getContinuity().getCoberturaDTO().getNumeroSecuencia());
	            }
	    });
		
		return coberturas;
	}

	@Override
	public List<BitemporalCoberturaSeccion> getCoberturas(
			BigDecimal lineaNegocioId, Long paqueteId,
			Long cotizacionContinuityId) {
		return this.getCoberturas(lineaNegocioId, paqueteId, null, null, null,
				null, null, null, null, cotizacionContinuityId, null);
	}

	@Override
	public List<BitemporalCoberturaSeccion> getCoberturas(
			BigDecimal lineaNegocioId, Long paqueteId, String estadoId,
			String municipioId, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUsoId, String modificadoresPrima,
			String modificadoresDescripcion, Long cotizacionContinuityId, DateTime validoEn) {

		return this.getCoberturas(lineaNegocioId, paqueteId, estadoId,
				municipioId, claveEstilo, modeloVehiculo, tipoUsoId,
				modificadoresPrima, modificadoresDescripcion, true,
				cotizacionContinuityId, validoEn);
	}

	@Override
	public List<BitemporalCoberturaSeccion> getCoberturas(
			BigDecimal lineaNegocioId, Long paqueteId, String estadoId,
			String municipioId, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUsoId, String modificadoresPrima,
			String modificadoresDescripcion, boolean aplicaCalculo,
			Long cotizacionContinuityId, DateTime validoEn) {
		boolean tienePlantilla = false;
		CotizacionContinuity cotizacionContinuity = entidadService.findById(
				CotizacionContinuity.class, cotizacionContinuityId);

		BitemporalCotizacion cotizacionBit = cotizacionContinuity.getCotizaciones().getInProcess();
		if(cotizacionBit == null && validoEn != null){
			cotizacionBit = cotizacionContinuity.getCotizaciones().getInProcess(validoEn);
		}
		Cotizacion cotizacion = cotizacionBit.getValue();

		NegocioProducto negocioProducto = negocioProductoDao
				.getNegocioProductoByIdNegocioIdProducto(cotizacion
						.getSolicitud().getNegocio().getIdToNegocio(),
						cotizacion.getSolicitud().getProductoDTO()
								.getIdToProducto());
		NegocioTipoPoliza negTipoPoliza = negocioTipoPolizaService
				.getPorIdNegocioProductoIdToTipoPoliza(negocioProducto
						.getIdToNegProducto(), cotizacion.getTipoPoliza()
						.getIdToTipoPoliza());
		NegocioSeccion negocioSeccion = negocioSeccionDao
				.getPorIdNegTipoPolizaIdToSeccion(
						negTipoPoliza.getIdToNegTipoPoliza(), lineaNegocioId);
		NegocioPaqueteSeccion negocioPaqueteSeccion = negocioPaqueteSeccionDao
				.getPorIdNegSeccionIdPaquete(
						negocioSeccion.getIdToNegSeccion(), paqueteId);
		ConfiguracionPlantillaNeg plantilla = configuracionPlantillaService
				.obtenerPlantilla(
						BigDecimal.valueOf(cotizacionContinuity.getNumero()),
						negocioSeccion.getIdToNegSeccion(),
						new BigDecimal(negocioPaqueteSeccion
								.getIdToNegPaqueteSeccion()));
		Long plantillaId = null;
		if (plantilla != null && plantilla.getPlantillaId() != null) {
			tienePlantilla = true;
			plantillaId = plantilla.getPlantillaId();
		}

		return this.getCoberturas(lineaNegocioId, paqueteId, estadoId,
				municipioId, claveEstilo, modeloVehiculo, tipoUsoId,
				modificadoresPrima, modificadoresDescripcion, tienePlantilla,
				aplicaCalculo, plantillaId, cotizacionContinuityId, validoEn);
	}

	@Override
	public List<BitemporalCoberturaSeccion> getCoberturas(
			BigDecimal lineaNegocioId, Long paqueteId, String estadoId,
			String municipioId, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUsoId, String modificadoresPrima,
			String modificadoresDescripcion, boolean tienePlantilla,
			boolean aplicaCalculo, Long plantillaId, Long cotizacionContinuityId, DateTime validoEn ) {

		CotizacionContinuity cotizacionContinuity = entidadService.findById(
				CotizacionContinuity.class, cotizacionContinuityId);

		BitemporalCotizacion cotizacionBit = cotizacionContinuity
				.getCotizaciones().getInProcess();
		if(cotizacionBit == null && validoEn != null){
			cotizacionBit = cotizacionContinuity.getCotizaciones().getInProcess(validoEn);
		}
		Cotizacion cotizacion = cotizacionBit.getValue();

		List<NegocioCobPaqSeccion> coberturasNegocio = null;
		List<BitemporalCoberturaSeccion> coberturas = new ArrayList<BitemporalCoberturaSeccion>();

//		List<NegocioCobPaqSeccion> coberturasNegocioDefault = negocioCobPaqSeccionDAO
//				.getCoberturas(cotizacion.getSolicitud().getNegocio()
//						.getIdToNegocio(), cotizacion.getSolicitud()
//						.getProductoDTO().getIdToProducto(), cotizacion
//						.getTipoPoliza().getIdToTipoPoliza(), lineaNegocioId,
//						paqueteId, estadoId, municipioId, cotizacion
//								.getMoneda().getIdTcMoneda());

		Map<String, Object> params = new LinkedHashMap<String, Object>();

		if (posiblesDeducibles == null) {
			params.put("id.idGrupoValores", Integer.valueOf(38));
			posiblesDeducibles = entidadService.findByProperties(
					CatalogoValorFijoDTO.class, params);
		}
		
		coberturasNegocio = coberturaService.getCoberturaNegocioConfig(cotizacion.getSolicitud().getNegocio()
				.getIdToNegocio(), cotizacion.getSolicitud()
				.getProductoDTO().getIdToProducto(), cotizacion
				.getTipoPoliza().getIdToTipoPoliza(),
				lineaNegocioId, paqueteId, estadoId, municipioId,
				cotizacion.getMoneda().getIdTcMoneda(), tipoUsoId, cotizacion.getSolicitud().getCodigoAgente(),
				(cotizacion.getSolicitud().getEsRenovacion() == null 
						|| cotizacion.getSolicitud().getEsRenovacion() == 0)? false : true);
		//edo y municipio
//		coberturasNegocio = negocioCobPaqSeccionDAO.getCoberturas(cotizacion.getSolicitud().getNegocio()
//				.getIdToNegocio(), cotizacion.getSolicitud()
//				.getProductoDTO().getIdToProducto(), cotizacion
//				.getTipoPoliza().getIdToTipoPoliza(),
//				lineaNegocioId, paqueteId, estadoId, municipioId,
//				cotizacion.getMoneda().getIdTcMoneda());
//		if(coberturasNegocio.isEmpty()){
//			//edo
//			coberturasNegocio = negocioCobPaqSeccionDAO.getCoberturas(cotizacion.getSolicitud().getNegocio()
//			.getIdToNegocio(), cotizacion.getSolicitud()
//			.getProductoDTO().getIdToProducto(), cotizacion
//			.getTipoPoliza().getIdToTipoPoliza(),
//			lineaNegocioId, paqueteId, estadoId, null,
//			cotizacion.getMoneda().getIdTcMoneda());
//		}
//		if(coberturasNegocio.isEmpty()){
//			coberturasNegocio = negocioCobPaqSeccionDAO.getCoberturas(cotizacion.getSolicitud().getNegocio()
//					.getIdToNegocio(), cotizacion.getSolicitud()
//					.getProductoDTO().getIdToProducto(), cotizacion
//					.getTipoPoliza().getIdToTipoPoliza(),
//					lineaNegocioId, paqueteId, null, null,
//					cotizacion.getMoneda().getIdTcMoneda());
//		}	

//		if (aplicaCalculo) {
//			List<NegocioCobPaqSeccion> coberturasNegocioZonaCirculacion = negocioCobPaqSeccionDAO
//					.getCoberturas(cotizacion.getSolicitud().getNegocio()
//							.getIdToNegocio(), cotizacion.getSolicitud()
//							.getProductoDTO().getIdToProducto(), cotizacion
//							.getTipoPoliza().getIdToTipoPoliza(),
//							lineaNegocioId, paqueteId, estadoId, municipioId,
//							cotizacion.getMoneda().getIdTcMoneda());
//
//			List<NegocioCobPaqSeccion> coberturasNegocioPersonalizadas = negocioCobPaqSeccionDAO
//					.getCoberturas(cotizacion.getSolicitud().getNegocio()
//							.getIdToNegocio(), cotizacion.getSolicitud()
//							.getProductoDTO().getIdToProducto(), cotizacion
//							.getTipoPoliza().getIdToTipoPoliza(),
//							lineaNegocioId, paqueteId, "notNull", "notNull",
//							cotizacion.getMoneda().getIdTcMoneda());
//
//			List<NegocioCobPaqSeccion> resta = new ArrayList<NegocioCobPaqSeccion>(
//					coberturasNegocioDefault);
//
//			for (NegocioCobPaqSeccion todo : coberturasNegocioDefault) {
//				for (NegocioCobPaqSeccion personalizada : coberturasNegocioPersonalizadas) {
//					if (todo.getCoberturaDTO().getIdToCobertura().longValue() == personalizada
//							.getCoberturaDTO().getIdToCobertura().longValue()) {
//						resta.remove(todo);
//					}
//				}
//			}
//			coberturasNegocio = (List<NegocioCobPaqSeccion>) CollectionUtils
//					.union(coberturasNegocioZonaCirculacion, resta);
//		} else {
//			coberturasNegocio = coberturasNegocioDefault;
//		}

		BitemporalCoberturaSeccion coberturaBit = null;
		CoberturaSeccion cobertura = null;
		CoberturaSeccionContinuity coberturaContinuity = null;

		CoberturaSeccionDTO coberturaSeccion = null;
		CoberturaSeccionDTOId idCob = null;
		CobPaquetesSeccionId id = null;

		for (NegocioCobPaqSeccion coberturaNegocio : coberturasNegocio) {
			id = new CobPaquetesSeccionId(lineaNegocioId.longValue(),
					paqueteId, coberturaNegocio.getCoberturaDTO()
							.getIdToCobertura().longValue());
			CobPaquetesSeccion configuracion = cobPaquetesSeccionDao
					.findById(id);
			if (configuracion == null) {
				// se elimino la configuracion del producto pero sigue
				// existiendo en el negocio
				configuracion = new CobPaquetesSeccion();
				configuracion.setClaveObligatoriedad(CoberturaDTO.OPCIONAL);
			}
			idCob = new CoberturaSeccionDTOId(lineaNegocioId, coberturaNegocio
					.getCoberturaDTO().getIdToCobertura());
			coberturaSeccion = entidadService.findById(
					CoberturaSeccionDTO.class, idCob);

			coberturaContinuity = new CoberturaSeccionContinuity();
			coberturaBit = new BitemporalCoberturaSeccion();
			cobertura = new CoberturaSeccion();

			coberturaContinuity.setCoberturaDTO(coberturaSeccion
					.getCoberturaDTO());
			cobertura.setCoberturaSeccionDTO(coberturaSeccion);

			switch (configuracion.getClaveObligatoriedad()) {
			case CoberturaDTO.OPCIONAL:
				cobertura.setClaveContrato((short) 0);
				break;
			default:
				cobertura.setClaveContrato((short) 1);
				break;
			}

			cobertura.setClaveObligatoriedad(configuracion
					.getClaveObligatoriedad());
			cobertura.setClaveTipoDeducible(Short
					.valueOf(coberturaNegocio.getCoberturaDTO().getClaveTipoDeducible()));
			cobertura.setClaveTipoLimiteDeducible(Short
					.valueOf(coberturaNegocio.getCoberturaDTO()
							.getClaveTipoLimiteDeducible()));
			cobertura.setSubRamoId(coberturaNegocio.getCoberturaDTO()
					.getSubRamoDTO().getIdTcSubRamo().longValue());

			Predicate equalCoa = new Predicate() {
				@Override
				public boolean evaluate(Object arg0) {
					CoaseguroCoberturaDTO dto = (CoaseguroCoberturaDTO) arg0;
					return dto.getClaveDefault() == (short) 1;
				}
			};

			CoaseguroCoberturaDTO coaseguro = (CoaseguroCoberturaDTO) CollectionUtils
					.find(coberturaNegocio.getCoberturaDTO().getCoaseguros(),
							equalCoa);
			cobertura.setValorCoaseguro(coaseguro != null ? coaseguro
					.getValor() : 0.0d);
			Predicate equalDed = new Predicate() {
				@Override
				public boolean evaluate(Object arg0) {
					NegocioDeducibleCob dto = (NegocioDeducibleCob) arg0;
					return dto.getClaveDefault() == (short) 1;
				}
			};
			List<NegocioDeducibleCob> deducibles = entidadService
					.findByProperty(NegocioDeducibleCob.class,
							"negocioCobPaqSeccion.idToNegCobPaqSeccion",
							coberturaNegocio.getIdToNegCobPaqSeccion());

			NegocioDeducibleCob deduclible = (NegocioDeducibleCob) CollectionUtils
					.find(deducibles, equalDed);
			cobertura.setValorDeducible(deduclible != null ? deduclible
					.getValorDeducible() : 0.0d);
			cobertura.setPorcentajeDeducible(deduclible != null ? deduclible.getValorDeducible(): 0.0d);

			cobertura
					.setDescripcionDeducible(getDescripcionDeducible(coberturaNegocio
							.getCoberturaDTO().getClaveTipoDeducible()));

			cobertura.setValorMinimoLimiteDeducible(coberturaNegocio
					.getCoberturaDTO().getValorMinimoDeducible());
			cobertura.setValorMaximoLimiteDeducible(coberturaNegocio
					.getCoberturaDTO().getValorMaximoDeducible());
			cobertura.setValorSumaAsegurada(coberturaNegocio
					.getValorSumaAseguradaDefault());
			cobertura.setValorSumaAseguradaMax(coberturaNegocio
					.getValorSumaAseguradaMax());
			cobertura.setValorSumaAseguradaMin(coberturaNegocio
					.getValorSumaAseguradaMin());
			cobertura.setValorPrimaNeta(0D);
			
			String nombreComercial = cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase();
			if(nombreComercial.equals(CoberturaDTO.NOMBRE_LIMITE_RC_VIAJERO)){
				cobertura.setDiasSalarioMinimo(coberturaNegocio.getValorSumaAseguradaDefault().intValue());
			}			

			cobertura.setClaveAutCoaseguro((short) 0);
			cobertura.setClaveAutDeducible((short) 0);
			cobertura.setNumeroAgrupacion((short) 0);
			if (tienePlantilla) {
				ConfiguracionPlantillaNegCobertura configuracionPlantilla = configuracionPlantillaService
						.obtenerCobertura(plantillaId, coberturaContinuity
								.getCoberturaDTO().getIdToCobertura());
				this.obtenerCoberturaDePlantilla(cobertura,
						configuracionPlantilla);
			}

			cobertura.setDeducibles(deducibles);
			
			if (cobertura.getDescripcionDeducible() != null && cobertura.getDescripcionDeducible().indexOf(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF) >= 0){
				cobertura.setDescripcionDeducible(cobertura.getDescripcionDeducible().replace(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF, ImpresionesServiceImpl.TIPO_VALOR_SA_UMA));
			}
			
			coberturaBit = new BitemporalCoberturaSeccion(cobertura,
					coberturaContinuity);
			coberturas.add(coberturaBit);
		}

		Collections.sort(coberturas,
				new Comparator<BitemporalCoberturaSeccion>() {
					@Override
					public int compare(BitemporalCoberturaSeccion n1,
							BitemporalCoberturaSeccion n2) {
						return n1
								.getContinuity()
								.getCoberturaDTO()
								.getNumeroSecuencia()
								.compareTo(
										n2.getContinuity().getCoberturaDTO()
												.getNumeroSecuencia());
					}
				});

		return coberturas;
	}
	
	private List<NegocioCobPaqSeccion> getCoberturasNegocio (BitemporalInciso inciso, DateTime validoEn) {
		
		List<NegocioCobPaqSeccion> coberturasNegocio = null;
		
		BitemporalCotizacion cotizacion = inciso.getEntidadContinuity().getParentContinuity().getBitemporalProperty().getInProcess(validoEn);
		BigDecimal lineaNegocioId = inciso.getEntidadContinuity().getIncisoSeccionContinuities().getInProcess(validoEn).iterator().next().getEntidadContinuity().getSeccion().getIdToSeccion();
		BitemporalAutoInciso autoInciso = inciso.getEntidadContinuity().getAutoIncisoContinuity().getBitemporalProperty().getInProcess(validoEn);
		
		Paquete paquete = autoInciso.getValue().getPaquete();
		if(autoInciso.getValue().getNegocioPaqueteId() != null){
			NegocioPaqueteSeccion negPaqSec = entidadService.findById(NegocioPaqueteSeccion.class, autoInciso.getValue().getNegocioPaqueteId());
			paquete = negPaqSec.getPaquete();
		}
		
		//edo y municipio
		coberturasNegocio = negocioCobPaqSeccionDAO.getCoberturas(
				cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio(), 
				cotizacion.getValue().getSolicitud().getProductoDTO().getIdToProducto(), 
				cotizacion.getValue().getTipoPoliza().getIdToTipoPoliza(),
				lineaNegocioId,paquete.getId(), 
				autoInciso.getValue().getEstadoId(), 
				autoInciso.getValue().getMunicipioId(),
				cotizacion.getValue().getMoneda().getIdTcMoneda());
		if(coberturasNegocio.isEmpty()) {
			//edo
			coberturasNegocio = negocioCobPaqSeccionDAO.getCoberturas(
					cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio(), 
					cotizacion.getValue().getSolicitud().getProductoDTO().getIdToProducto(), 
					cotizacion.getValue().getTipoPoliza().getIdToTipoPoliza(),
					lineaNegocioId, paquete.getId(), 
					autoInciso.getValue().getEstadoId(), 
					null,
					cotizacion.getValue().getMoneda().getIdTcMoneda());
		}
		if(coberturasNegocio.isEmpty()) {
			coberturasNegocio = negocioCobPaqSeccionDAO.getCoberturas(
					cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio(), 
					cotizacion.getValue().getSolicitud().getProductoDTO().getIdToProducto(), 
					cotizacion.getValue().getTipoPoliza().getIdToTipoPoliza(),
					lineaNegocioId, paquete.getId(), 
					null, 
					null,
					cotizacion.getValue().getMoneda().getIdTcMoneda());
		}	
		
		return coberturasNegocio;

	}


	private String getDescripcionDeducible(String tipoDeducible) {
		String descripcion = "";
		int valorTipoDeducible;
		if (tipoDeducible != null) {
			valorTipoDeducible = Integer.valueOf(tipoDeducible);

			for (CatalogoValorFijoDTO valor : posiblesDeducibles) {
				if (valor.getId().getIdDato() == valorTipoDeducible) {
					descripcion = valor.getDescripcion();
					break;
				}
			}
		}
		return descripcion;
	}

	private void obtenerCoberturaDePlantilla(CoberturaSeccion cobertura,
			ConfiguracionPlantillaNegCobertura configuracion) {
		if (configuracion != null) {
			cobertura
					.setClaveContrato(configuracion.getContratada() ? (short) 1
							: (short) 0);
			cobertura.setValorSumaAsegurada(configuracion.getSumaAsegurada());
			cobertura.setValorDeducible(configuracion.getDeducible());
		}

	}
	
	private List<BitemporalCoberturaSeccion> getCoberturas(BitemporalAutoInciso autoInciso, Long cotizacionContinuityId, DateTime validoEn) {
		
		NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class,autoInciso.getValue().getNegocioPaqueteId());
		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
		estiloVehiculoId.valueOf(autoInciso.getValue().getEstiloId());
		
		return this.getCoberturas (negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion(),
				negocioPaqueteSeccion.getPaquete().getId(), 
				autoInciso.getValue().getEstadoId(), 
				autoInciso.getValue().getMunicipioId(), 
				estiloVehiculoId.getClaveEstilo(),
				autoInciso.getValue().getModeloVehiculo().longValue(),
				new BigDecimal(autoInciso.getValue().getTipoUsoId()),
				null, 
				null, 
				cotizacionContinuityId, validoEn);
			
	}
	
}
