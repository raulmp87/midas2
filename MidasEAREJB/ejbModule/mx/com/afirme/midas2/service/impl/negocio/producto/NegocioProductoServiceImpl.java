package mx.com.afirme.midas2.service.impl.negocio.producto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda.MonedaTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda.MonedaTipoPolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.CobPaquetesSeccionDao;
import mx.com.afirme.midas2.dao.catalogos.ValorSeccionCobAutosDao;
import mx.com.afirme.midas2.dao.negocio.NegocioDao;
import mx.com.afirme.midas2.dao.negocio.producto.NegocioProductoDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.NegocioSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionDao;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutos;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutosId;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.Negocio_;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto_;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.mediopago.NegocioMedioPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.NegocioProductoService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;

@Stateless
public class NegocioProductoServiceImpl implements NegocioProductoService{
	
	private EntidadService entidadService;
	Map<BigDecimal, CoberturaDTO> coberturas = new LinkedHashMap<BigDecimal, CoberturaDTO>();
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}


	@EJB
	private SolicitudFacadeRemote solicitudFacade;
	
	@EJB
	private ProductoFacadeRemote productoFacade;
	
	@EJB
	private TipoPolizaFacadeRemote tipoPolizaFacade;
	
	@EJB 
	private CoberturaFacadeRemote coberturaFacade;
	
	@EJB
	private SeccionFacadeRemote seccionFacade;
	
	@EJB
	private NegocioSeccionService negocioSeccionService;
	
	@EJB
	private NegocioSeccionDao negocioSeccionDao;
	
	@EJB
	private NegocioDao negocioDao;
	
	@EJB
	private NegocioPaqueteSeccionDao negocioPaqueteSeccionDao;
	
	@EJB
	private CobPaquetesSeccionDao cobPaquetesSeccionDao;
	
	@EJB
	private NegocioProductoDao negocioProductoDao;
	
	@EJB
	private MonedaTipoPolizaFacadeRemote monedaTipoPolizaFacadeRemote;
	
	@EJB
	private ValorSeccionCobAutosDao valorSeccionCobAutosDao;
	
	
	@Override
	public Long agregarProducto(Long idNegocio, Long idProducto) {

		NegocioProducto negocioProducto = new NegocioProducto();
		
		ProductoDTO productoDTO = productoFacade.findById(new BigDecimal(idProducto.doubleValue()));
		
		Negocio negocio = negocioDao.findById(idNegocio);
		
		negocioProducto.setNegocio(negocio);
		negocioProducto.setProductoDTO(productoDTO);
		
		negocioProductoDao.persist(negocioProducto);
		
		registrarDatosPagos(negocioProducto, productoDTO);
		
		List<TipoPolizaDTO> listTipoPol = tipoPolizaFacade.listarPorIdProducto(productoDTO.getIdToProducto(), false, true);
		
		this.registrarTiposPoliza(negocioProducto, listTipoPol);
		
		negocioProducto = negocioProductoDao.update(negocioProducto);
		
		List<SeccionDTO> listSeccion = null;
		NegocioSeccion negocioSeccion = null;
		
		for(NegocioTipoPoliza negocioTipoPol : negocioProducto.getNegocioTipoPolizaList()){
			negocioTipoPol.setNegocioSeccionList(new ArrayList<NegocioSeccion>());
			
			List<MonedaTipoPolizaDTO> listaMonedasTipoPol = monedaTipoPolizaFacadeRemote.findByTipoPolizaId( 
					negocioTipoPol.getTipoPolizaDTO().getIdToTipoPoliza());
			
			listSeccion = seccionFacade.listarPorIdTipoPoliza(negocioTipoPol.getTipoPolizaDTO().getIdToTipoPoliza(), true, true);
			for(SeccionDTO seccion : listSeccion){
				negocioSeccion = new NegocioSeccion();
				negocioSeccion.setSeccionDTO(seccion);
				negocioSeccion.setNegocioTipoPoliza(negocioTipoPol);
				
				negocioSeccionDao.persist(negocioSeccion);
				
				negocioSeccion = negocioSeccionService.poblarAgrupTarifaSeccion(negocioSeccion);
				
				negocioSeccion = negocioSeccionDao.update(negocioSeccion);
				
				negocioTipoPol.getNegocioSeccionList().add(negocioSeccion);
				
				negocioSeccion.setNegocioPaqueteSeccionList(new ArrayList<NegocioPaqueteSeccion>());
				
				//2. Se obtienen los paquetes relacionados con la seccion para registrar objetos NegocioPaqueteSeccion
				for(Paquete paquete : seccion.getPaquetes()){
					NegocioPaqueteSeccion negPaqSec = new NegocioPaqueteSeccion();
					
					negPaqSec.setPaquete(paquete);
					negPaqSec.setNegocioSeccion(negocioSeccion);
					//TODO verificar qué dato see coloca en este campo
					negPaqSec.setModeloAntiguedadMax(BigDecimal.ZERO);
					negPaqSec.setAplicaPctDescuentoEdo(0);
					negocioPaqueteSeccionDao.persist(negPaqSec);
					
					negPaqSec.setNegocioCobPaqSeccionList(new ArrayList<NegocioCobPaqSeccion>());
					
					List<CobPaquetesSeccion> listCobPaquete = cobPaquetesSeccionDao.getPorSeccionPaquete(
							seccion.getIdToSeccion().longValue(),paquete.getId());
					
					//3. Los objetos NegocioPaqueteSeccion contienen registros hijos tipo NegocioCobPaqSeccion
					for(CobPaquetesSeccion cobPaquetesSeccion : listCobPaquete){
						
						for(MonedaTipoPolizaDTO monedaTipoPol : listaMonedasTipoPol){
							
							NegocioCobPaqSeccion negocioCobPaqSeccion = new NegocioCobPaqSeccion();
							negocioCobPaqSeccion.setNegocioPaqueteSeccion(negPaqSec);
							
							CoberturaDTO cobertura = this.getCoberturaDTO(new BigDecimal(cobPaquetesSeccion.getId().getIdToCobertura()));
							
							negocioCobPaqSeccion.setCoberturaDTO(cobertura);
							
							negocioCobPaqSeccion.setClaveTipoDeduciblePP((short)0);
							negocioCobPaqSeccion.setClaveTipoDeduciblePT((short)0);
							negocioCobPaqSeccion.setClaveTipoSumaAsegurada(new Short(cobertura.getClaveTipoSumaAsegurada()));
							
							negocioCobPaqSeccion.setMonedaDTO(new MonedaDTO());
							negocioCobPaqSeccion.getMonedaDTO().setIdTcMoneda(monedaTipoPol.getId().getIdMoneda().shortValue());
							
							//Consultar valorSeccionCobAutos
							
							ValorSeccionCobAutosId vscaId = new ValorSeccionCobAutosId(
									seccion.getIdToSeccion().longValue(), cobPaquetesSeccion.getId().getIdToCobertura().longValue(), 
									monedaTipoPol.getId().getIdMoneda().longValue(), 0l);
							
							ValorSeccionCobAutos valorSeccionCobAutos = valorSeccionCobAutosDao.findById(vscaId);
							
							if(valorSeccionCobAutos != null){
								
								negocioCobPaqSeccion.setValorSumaAseguradaMin(valorSeccionCobAutos.getValorSumaAseguradaMin());
								negocioCobPaqSeccion.setValorSumaAseguradaMax(valorSeccionCobAutos.getValorSumaAseguradaMax());
								negocioCobPaqSeccion.setValorSumaAseguradaDefault(valorSeccionCobAutos.getValorSumaAseguradaDefault());
							}
							else{
								negocioCobPaqSeccion.setValorSumaAseguradaMin(0d);
								negocioCobPaqSeccion.setValorSumaAseguradaMax(0d);
								negocioCobPaqSeccion.setValorSumaAseguradaDefault(0d);
							}
							
							entidadService.save(negocioCobPaqSeccion);
//							//Se agregan los deducibles defaul del producto
							negocioCobPaqSeccion.setNegocioDeducibleCobList(new ArrayList<NegocioDeducibleCob>());
//							List<DeducibleCoberturaDTO> deduciblesPorCobertura = deducibleCoberturaFacadeRemote.listarDeduciblesPorCobertura(cobertura.getIdToCobertura());
							List<DeducibleCoberturaDTO> deduciblesPorCobertura = cobertura.getDeducibles();
							int secuencia = 0;
							for(DeducibleCoberturaDTO deducible: deduciblesPorCobertura){
								secuencia++;
								NegocioDeducibleCob deducibleNegocio = new NegocioDeducibleCob();
								deducibleNegocio.setNegocioCobPaqSeccion(negocioCobPaqSeccion);
								deducibleNegocio.setValorDeducible(deducible.getValor());
								deducibleNegocio.setNumeroSecuencia(secuencia);
								deducibleNegocio.setClaveDefault(deducible.getClaveDefault());
								entidadService.save(deducibleNegocio);
							}
							
//							negPaqSec.getNegocioCobPaqSeccionList().add(negocioCobPaqSeccion);
							
						}
						
					}
					
					negPaqSec = negocioPaqueteSeccionDao.update(negPaqSec);
					
					negocioSeccion.getNegocioPaqueteSeccionList().add(negPaqSec);
				}
				
			}
			
		}
		//se registran los NegocioSeccion en cascada.
		negocioProducto = negocioProductoDao.update(negocioProducto);
	
		return negocioProducto.getIdToNegProducto();
	}
	
	public CoberturaDTO getCoberturaDTO(BigDecimal idToCobertura){
		return entidadService.findById(CoberturaDTO.class, idToCobertura);
		/*if(this.coberturas.containsKey(idToCobertura)){
			return this.coberturas.get(idToCobertura);
		}else{
			return entidadService.findById(CoberturaDTO.class, idToCobertura);
		}*/
	}
	
	private final void obtenerProductosDisponibles(List<ProductoDTO> productoDTOs, List<NegocioProducto> negocioProductos,List<ProductoDTO> disponiblesList){
		int indiceNegocioEncontrado = 0;
		boolean productoIncluido = false;
		for (ProductoDTO productoTMP : productoDTOs) {
			productoIncluido = false;

			for (int i = 0; i < negocioProductos.size(); i++) {
				NegocioProducto negProd = negocioProductos.get(i);
				if (negProd.getProductoDTO().equals(productoTMP)) {
					// el producto en curso y está relacionado
					productoIncluido = true;
					indiceNegocioEncontrado = i;
					break;
				}
			}

			if (!productoIncluido) {
				// si no ya está incluido, se mete en la lista
				disponiblesList.add(productoTMP);
			} else {
				// si está incluido, se saca de la lista de negocio producto,
				// para disminuir las iteraciones
				negocioProductos.remove(indiceNegocioEncontrado);
			}
		}
	}
	
	public List<ProductoDTO> listarProductosGroupBy(Long idToNegocio){
		return productoFacade.listarProductosGroupBy(idToNegocio);
	}
	
	public List<ProductoDTO> obtenerProductosDisponiblesRelacionar(Long idNegocio){
		
		List<ProductoDTO> listaProductos = new ArrayList<ProductoDTO>();
		
		Negocio negocio = negocioDao.findById(idNegocio);
		
		ProductoDTO prodFiltro = new ProductoDTO();
		prodFiltro.setClaveNegocio(negocio.getClaveNegocio());
		List<ProductoDTO> listaTotalProductos = productoFacade.listarFiltrado(prodFiltro, true);
		
		List<NegocioProducto> listaNegprod = negocioProductoDao.findByProperty("negocio.idToNegocio", idNegocio);
		
		obtenerProductosDisponibles(listaTotalProductos, listaNegprod, listaProductos);
		
		return listaProductos;
	}

	@Override
	public List<NegocioProducto> obtenerNegociosRelacionados(Long idNegocio) {
		return negocioProductoDao.findByProperty(NegocioProducto_.negocio.getName()+"."+Negocio_.idToNegocio.getName(), idNegocio);
	}
	
	public void registrarTiposPoliza(NegocioProducto negocioProducto,List<TipoPolizaDTO> listTipoPol){
		negocioProducto.setNegocioTipoPolizaList(new ArrayList<NegocioTipoPoliza>());
		
		NegocioTipoPoliza negocioTipoPoliza = null;
		
		for(TipoPolizaDTO tipoPol : listTipoPol){
			negocioTipoPoliza = new NegocioTipoPoliza();			
			negocioTipoPoliza.setTipoPolizaDTO(tipoPol);
			negocioTipoPoliza.setNegocioProducto(negocioProducto);
			
			negocioProducto.getNegocioTipoPolizaList().add(negocioTipoPoliza);			
			registraCoberturas(coberturaFacade.listarVigentesPorTipoPoliza(tipoPol.getIdToTipoPoliza()));
		}
	}
	
	public void registraCoberturas(List<CoberturaDTO> coberturas){
		for(CoberturaDTO item: coberturas){
			if(!this.coberturas.containsKey(item.getIdToCobertura())){
				this.coberturas.put(item.getIdToCobertura(), item);
			}
		}
	}
	
	public static void registrarDatosPagos(NegocioProducto negocioProducto, ProductoDTO productoDTO){
		negocioProducto.setNegocioFormaPagos(new ArrayList<NegocioFormaPago>());
		negocioProducto.setNegocioMedioPagos(new ArrayList<NegocioMedioPago>());
		
		for(FormaPagoDTO formaPago : productoDTO.getFormasPago()){
			NegocioFormaPago negocioFormaPago = new NegocioFormaPago();
			negocioFormaPago.setFormaPagoDTO(formaPago);
			negocioFormaPago.setNegocioProducto(negocioProducto);
			
			negocioProducto.getNegocioFormaPagos().add(negocioFormaPago);
		}
		
		for(MedioPagoDTO medioPago : productoDTO.getMediosPago()){
			NegocioMedioPago negocioMedioPago = new NegocioMedioPago(null, negocioProducto, medioPago);
			
			negocioProducto.getNegocioMedioPagos().add(negocioMedioPago);
		}
	}
	
	@Override
	public NegocioProducto getNegocioProductoByCotizacion(CotizacionDTO cotizacion){
		NegocioProducto negocioProducto = null;
			SolicitudDTO solicitud = solicitudFacade.findById(cotizacion.getSolicitudDTO().getIdToSolicitud());
			negocioProducto = negocioProductoDao.getNegocioProductoByIdNegocioIdProducto(solicitud.getNegocio().getIdToNegocio(), solicitud.getProductoDTO().getIdToProducto());
		return negocioProducto;		
	}
	
	
	@Override
	public Long deleteProducto(Long idToNegProducto){
		NegocioProducto  negocioProducto =new NegocioProducto();
		negocioProducto = entidadService.findById(NegocioProducto.class, idToNegProducto);
		try{ 
			entidadService.remove(negocioProducto);
		}catch(RuntimeException e){
			return idToNegProducto;
		}
		return null;
	}
	

}
