package mx.com.afirme.midas2.service.impl.tarifa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.sumaaseguradaadicional.SumaAseguradaAdicionalCoberturaDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionDAO;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccionId;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublico;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublicoDeduciblesAd;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublicoSumaAseguradaAd;
import mx.com.afirme.midas2.dto.RelacionesTarifaServicioPublicoDTO;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.impl.ListadoServiceImpl;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducible.NegocioDeduciblesService;
import mx.com.afirme.midas2.service.tarifa.TarifaServicioPublicoService;
import mx.com.afirme.midas2.util.UtileriasWeb;

@Stateless
public class TarifaServicioPublicoServiceImpl extends ListadoServiceImpl implements
	TarifaServicioPublicoService{
	private static final Logger LOG = Logger.getLogger(TarifaServicioPublicoServiceImpl.class);
	
	private NegocioCobPaqSeccionDAO negocioCobPaqSeccionDAO;
	private EntidadService entidadService;
	private NegocioDeduciblesService negocioDeduciblesService;
	
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;

	@Override
	public RelacionesTarifaServicioPublicoDTO getRelationList(
			TarifaServicioPublico tarifaServicioPublicoFiltro) {
		
		RelacionesTarifaServicioPublicoDTO relacionesTarifaServicioPublico = new RelacionesTarifaServicioPublicoDTO();
		List<TarifaServicioPublico> tarifaServicioPublicoObligatorias = new ArrayList<TarifaServicioPublico>(1);
		List<TarifaServicioPublico> tarifaServicioPublicoAdicionales = new ArrayList<TarifaServicioPublico>(1);
		List<TarifaServicioPublicoSumaAseguradaAd> tarifaServicioPublicoSumaAseguradaAd = new ArrayList<TarifaServicioPublicoSumaAseguradaAd>(1);
		List<TarifaServicioPublicoDeduciblesAd> tarifaServicioPublicodDeduciblesAd = new ArrayList<TarifaServicioPublicoDeduciblesAd>(1);
		
		NegocioCobPaqSeccion negocioCobPaqSeccion = new NegocioCobPaqSeccion();
		negocioCobPaqSeccion.setEstadoDTO(tarifaServicioPublicoFiltro.getEstadoDTO());
		negocioCobPaqSeccion.setCiudadDTO(tarifaServicioPublicoFiltro.getCiudadDTO());
		negocioCobPaqSeccion.setMonedaDTO(tarifaServicioPublicoFiltro.getMonedaDTO());
		negocioCobPaqSeccion.setNegocioPaqueteSeccion(tarifaServicioPublicoFiltro.getNegocioPaqueteSeccion());
		
		List<NegocioCobPaqSeccion> negocioCobPaqSeccionAsociadas = new ArrayList<NegocioCobPaqSeccion>(1); 
		negocioCobPaqSeccionAsociadas = negocioCobPaqSeccionDAO.getNegocioCobPaqSeccionAsociados(negocioCobPaqSeccion);

		for(NegocioCobPaqSeccion negocioCobPaqSeccionAsociada: negocioCobPaqSeccionAsociadas){
			CobPaquetesSeccionId cobPaquetesSeccionId = new CobPaquetesSeccionId();
			cobPaquetesSeccionId.setIdPaquete(negocioCobPaqSeccionAsociada.getNegocioPaqueteSeccion().getPaquete().getId());
			cobPaquetesSeccionId.setIdToCobertura(negocioCobPaqSeccionAsociada.getCoberturaDTO().getIdToCobertura().longValue());
			cobPaquetesSeccionId.setIdToSeccion(negocioCobPaqSeccionAsociada.getNegocioPaqueteSeccion().getNegocioSeccion().getSeccionDTO().getIdToSeccion().longValue());
			CobPaquetesSeccion cobPaquetesSeccion =  entidadService.findById(CobPaquetesSeccion.class, cobPaquetesSeccionId);
			
			if(cobPaquetesSeccion != null) {
				TarifaServicioPublico tarifaServicioPublico = new TarifaServicioPublico();
				tarifaServicioPublico.setEstadoDTO(negocioCobPaqSeccionAsociada.getEstadoDTO());
				tarifaServicioPublico.setCiudadDTO(negocioCobPaqSeccionAsociada.getCiudadDTO());
				tarifaServicioPublico.setCoberturaDTO(negocioCobPaqSeccionAsociada.getCoberturaDTO());
				tarifaServicioPublico.setMonedaDTO(negocioCobPaqSeccionAsociada.getMonedaDTO());
				tarifaServicioPublico.setNegocioPaqueteSeccion(negocioCobPaqSeccionAsociada.getNegocioPaqueteSeccion());
				tarifaServicioPublico.setVigenciaDTO(tarifaServicioPublicoFiltro.getVigenciaDTO());				
			
				List<TarifaServicioPublico> tarifa = new ArrayList<TarifaServicioPublico>(1);
				tarifa = entidadService.findByProperties(TarifaServicioPublico.class, this.getParametros(tarifaServicioPublico));
				if(tarifa != null && !tarifa.isEmpty()) {
					tarifaServicioPublico.setPrima(tarifa.get(0).getPrima());
					tarifaServicioPublico.setId(tarifa.get(0).getId());
				} else {
					tarifaServicioPublico.setPrima(0.0);
				}
				
				String descripcion = this.obtenerDescripcionDeducible(tarifaServicioPublico.getCoberturaDTO());
				
				if(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() > negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()) {
					tarifaServicioPublico.setValorSumaAsegurada("Entre " +
							(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin() != null ? UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()) : UtileriasWeb.formatoMoneda(0.0)) + " y " + 
							(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() != null ? UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax()).toString() : UtileriasWeb.formatoMoneda(0.0)) + " con SA Default " + 
							(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault() != null ? UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()).toString() : UtileriasWeb.formatoMoneda(0.0)));
				} else {
					String valorSumaAsegurada = negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault() != null ?
							UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()) : UtileriasWeb.formatoMoneda(0.0);
					tarifaServicioPublico.setValorSumaAsegurada(valorSumaAsegurada + (tarifaServicioPublico.getCoberturaDTO().getClaveTipoDeducible().equals("0") ? "" : descripcion)) ;
				}
				
				if(tarifaServicioPublico.getCoberturaDTO().getClaveTipoDeducible().equals("0")  && CoberturaDTO.CLAVE_TIPO_SUMA_ASEGURADA_AMPARADA.equals(tarifaServicioPublico.getCoberturaDTO().getClaveTipoSumaAsegurada())) {
					tarifaServicioPublico.setValorSumaAsegurada(CoberturaDTO.DESCRIPCION_TIPO_SUMA_ASEGURADA_AMPARADA);
				}

				if(cobPaquetesSeccion.getClaveObligatoriedad().compareTo((short) 0) == 0) {
					tarifaServicioPublicoObligatorias.add(tarifaServicioPublico);
					
					tarifaServicioPublicoSumaAseguradaAd = this.getSumasAseguradasAdicionales(tarifaServicioPublico, tarifaServicioPublicoSumaAseguradaAd);
					
					if(tarifaServicioPublico.getCoberturaDTO() != null &&(this.esParametroValido(tarifaServicioPublico.getCoberturaDTO().getIdToCobertura(),ParametroGeneralDTO.CODIGO_PARAM_GENERAL_COBERTURA_OBLIGATORIA_SERVICIO_PUBLICO))) {
						tarifaServicioPublicodDeduciblesAd = this.getDeduciblesAdicionales(negocioCobPaqSeccionAsociada, tarifaServicioPublico, tarifaServicioPublicodDeduciblesAd);
					}
				} else {
					tarifaServicioPublicoAdicionales.add(tarifaServicioPublico);
				}
			}
		}
		
		relacionesTarifaServicioPublico.setObligatorias(tarifaServicioPublicoObligatorias);
		relacionesTarifaServicioPublico.setAdicionales(tarifaServicioPublicoAdicionales);
		relacionesTarifaServicioPublico.setSumasAseguradasAdicionales(tarifaServicioPublicoSumaAseguradaAd);
		relacionesTarifaServicioPublico.setDeduciblesAdicionales(tarifaServicioPublicodDeduciblesAd);
		return relacionesTarifaServicioPublico;
	}
	
	public List<TarifaServicioPublicoSumaAseguradaAd> getSumasAseguradasAdicionales (TarifaServicioPublico tarifaServicioPublico, List<TarifaServicioPublicoSumaAseguradaAd> tarifaServicioPublicoSumaAseguradaAd) {
		List<SumaAseguradaAdicionalCoberturaDTO> sumasAdicionales = new ArrayList<SumaAseguradaAdicionalCoberturaDTO>(1);
		if(tarifaServicioPublico.getCoberturaDTO() != null) {
			sumasAdicionales = tarifaServicioPublico.getCoberturaDTO().getSumasAseguradasAdicionales();
		}
		
		for(SumaAseguradaAdicionalCoberturaDTO sumaAdicional: sumasAdicionales){
			TarifaServicioPublicoSumaAseguradaAd tarifaSumaAseguradaAdicional = new TarifaServicioPublicoSumaAseguradaAd();
			tarifaSumaAseguradaAdicional.setEstadoDTO(tarifaServicioPublico.getEstadoDTO());
			tarifaSumaAseguradaAdicional.setCiudadDTO(tarifaServicioPublico.getCiudadDTO());
			tarifaSumaAseguradaAdicional.setCoberturaDTO(tarifaServicioPublico.getCoberturaDTO());
			tarifaSumaAseguradaAdicional.setMonedaDTO(tarifaServicioPublico.getMonedaDTO());
			tarifaSumaAseguradaAdicional.setVigenciaDTO(tarifaServicioPublico.getVigenciaDTO());
			tarifaSumaAseguradaAdicional.setNegocioPaqueteSeccion(tarifaServicioPublico.getNegocioPaqueteSeccion());
			tarifaSumaAseguradaAdicional.setIdSumaAseguradaAd(sumaAdicional.getId());
			
			List<TarifaServicioPublicoSumaAseguradaAd> tarifaSA = new ArrayList<TarifaServicioPublicoSumaAseguradaAd>(1);
			tarifaSA = entidadService.findByProperties(TarifaServicioPublicoSumaAseguradaAd.class, this.getParametrosSumaAsegurada(tarifaSumaAseguradaAdicional));
			if(tarifaSA != null && !tarifaSA.isEmpty()) {
				tarifaSumaAseguradaAdicional.setId(tarifaSA.get(0).getId());
				tarifaSumaAseguradaAdicional.setPrima(tarifaSA.get(0).getPrima());
			} else {
				tarifaSumaAseguradaAdicional.setPrima(0.0);
			}
			tarifaSumaAseguradaAdicional.setValorSumaAsegurada(sumaAdicional.getValor());
			tarifaServicioPublicoSumaAseguradaAd.add(tarifaSumaAseguradaAdicional);
		}
		
		return tarifaServicioPublicoSumaAseguradaAd;
	}
	
	public List<TarifaServicioPublicoDeduciblesAd> getDeduciblesAdicionales (NegocioCobPaqSeccion negocioCobPaqSeccionAsociada, TarifaServicioPublico tarifaServicioPublico, List<TarifaServicioPublicoDeduciblesAd> tarifaServicioPublicodDeduciblesAd) {
		List<NegocioDeducibleCob> deducibles = negocioDeduciblesService.obtenerDeduciblesCobertura(negocioCobPaqSeccionAsociada.getIdToNegCobPaqSeccion());
			
		for (TarifaServicioPublicoDeduciblesAd.ClaveSumaAsegurada sumasAseguradasDeducibles : TarifaServicioPublicoDeduciblesAd.ClaveSumaAsegurada.values()) {
			for(NegocioDeducibleCob deducible: deducibles){
				TarifaServicioPublicoDeduciblesAd tarifaServicioPublicoDeduciblesAdicional = new TarifaServicioPublicoDeduciblesAd();
				tarifaServicioPublicoDeduciblesAdicional.setEstadoDTO(tarifaServicioPublico.getEstadoDTO());
				tarifaServicioPublicoDeduciblesAdicional.setCiudadDTO(tarifaServicioPublico.getCiudadDTO());
				tarifaServicioPublicoDeduciblesAdicional.setCoberturaDTO(tarifaServicioPublico.getCoberturaDTO());
				tarifaServicioPublicoDeduciblesAdicional.setMonedaDTO(tarifaServicioPublico.getMonedaDTO());
				tarifaServicioPublicoDeduciblesAdicional.setVigenciaDTO(tarifaServicioPublico.getVigenciaDTO());
				tarifaServicioPublicoDeduciblesAdicional.setNegocioPaqueteSeccion(tarifaServicioPublico.getNegocioPaqueteSeccion());
				tarifaServicioPublicoDeduciblesAdicional.setNegocioDeducibleCob(deducible);
				tarifaServicioPublicoDeduciblesAdicional.setClaveSumaAsegurada(sumasAseguradasDeducibles.getValor());
					
				List<TarifaServicioPublicoDeduciblesAd> tarifaDe = new ArrayList<TarifaServicioPublicoDeduciblesAd>(1);
				tarifaDe = entidadService.findByProperties(TarifaServicioPublicoDeduciblesAd.class, this.getParametrosDeducibles(tarifaServicioPublicoDeduciblesAdicional));
				if(tarifaDe != null && !tarifaDe.isEmpty()) {
					tarifaServicioPublicoDeduciblesAdicional.setId(tarifaDe.get(0).getId());
					tarifaServicioPublicoDeduciblesAdicional.setValorDeducible1(tarifaDe.get(0).getValorDeducible1());
					tarifaServicioPublicoDeduciblesAdicional.setValorDeducible2(tarifaDe.get(0).getValorDeducible2());
					tarifaServicioPublicoDeduciblesAdicional.setValorDeducible3(tarifaDe.get(0).getValorDeducible3());
					if (this.esParametroValido(negocioCobPaqSeccionAsociada.getNegocioPaqueteSeccion().getNegocioSeccion().getIdToNegSeccion(),
											ParametroGeneralDTO.CODIGO_PARAM_GENERAL_LINEA_AUTOBUSES_SERVICIO_PUBLICO)){
						tarifaServicioPublicoDeduciblesAdicional.setValorDeducible4(tarifaDe.get(0).getValorDeducible4());
						tarifaServicioPublicoDeduciblesAdicional.setValorDeducible5(tarifaDe.get(0).getValorDeducible5());
						tarifaServicioPublicoDeduciblesAdicional.setValorDeducible6(tarifaDe.get(0).getValorDeducible6());
					}
				} else {
					tarifaServicioPublicoDeduciblesAdicional.setValorDeducible1(0.0);
					tarifaServicioPublicoDeduciblesAdicional.setValorDeducible2(0.0);
					tarifaServicioPublicoDeduciblesAdicional.setValorDeducible3(0.0);
					if (this.esParametroValido(negocioCobPaqSeccionAsociada.getNegocioPaqueteSeccion().getNegocioSeccion().getIdToNegSeccion(),
											ParametroGeneralDTO.CODIGO_PARAM_GENERAL_LINEA_AUTOBUSES_SERVICIO_PUBLICO)){
						tarifaServicioPublicoDeduciblesAdicional.setValorDeducible4(0.0);
						tarifaServicioPublicoDeduciblesAdicional.setValorDeducible5(0.0);
						tarifaServicioPublicoDeduciblesAdicional.setValorDeducible6(0.0);
					}
				}
				
				if(tarifaServicioPublicoDeduciblesAdicional.getClaveSumaAsegurada().compareTo(TarifaServicioPublicoDeduciblesAd.ClaveSumaAsegurada.Maxima.getValor()) == 0) {
					if((negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() > negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()) ||
							(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax().equals(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin())&&
							negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax().equals(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()))) {
						tarifaServicioPublicoDeduciblesAdicional.setValorSumaAsegurada(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax());
						tarifaServicioPublicoDeduciblesAdicional.setValorSumaAseguradaStr(tarifaServicioPublico.getCoberturaDTO().getDescripcion() + " " + 
								UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax()));
						tarifaServicioPublicodDeduciblesAd.add(tarifaServicioPublicoDeduciblesAdicional);
					}
				} else if(tarifaServicioPublicoDeduciblesAdicional.getClaveSumaAsegurada().compareTo(TarifaServicioPublicoDeduciblesAd.ClaveSumaAsegurada.Minima.getValor()) == 0) {
					if(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin() < negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()) {
						tarifaServicioPublicoDeduciblesAdicional.setValorSumaAsegurada(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin());
						tarifaServicioPublicoDeduciblesAdicional.setValorSumaAseguradaStr(tarifaServicioPublico.getCoberturaDTO().getDescripcion() + " " + 
								UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()));
						tarifaServicioPublicodDeduciblesAd.add(tarifaServicioPublicoDeduciblesAdicional);
					}
				}else{
					if (negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() > negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()) {
						tarifaServicioPublicoDeduciblesAdicional.setValorSumaAsegurada(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault());
						tarifaServicioPublicoDeduciblesAdicional.setValorSumaAseguradaStr(tarifaServicioPublico.getCoberturaDTO().getDescripcion() + " " + 
								UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()));
						tarifaServicioPublicodDeduciblesAd.add(tarifaServicioPublicoDeduciblesAdicional);
					}
				}
			}
		}
		
		return tarifaServicioPublicodDeduciblesAd;
	}

	@Override
	public RespuestaGridRelacionDTO guardarMontosCoberturasAdicionales(String accion, TarifaServicioPublico tarifaServicioPublico) {
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		entidadService.executeActionGrid(accion, tarifaServicioPublico);
		respuesta.setTipoMensaje("30");
		return respuesta;
	}
	
	@Override
	public RespuestaGridRelacionDTO guardarSumasAseguradasAdicionales(String accion, TarifaServicioPublicoSumaAseguradaAd tarifaServicioPublicoSumaAseguradaAd) {
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		entidadService.executeActionGrid(accion, tarifaServicioPublicoSumaAseguradaAd);
		respuesta.setTipoMensaje("30");
		return respuesta;
	}
	
	@Override
	public RespuestaGridRelacionDTO guardarDeduciblesAdicionales(String accion, TarifaServicioPublicoDeduciblesAd tarifaServicioPublicDeduciblesAd) {
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		entidadService.executeActionGrid(accion, tarifaServicioPublicDeduciblesAd);
		respuesta.setTipoMensaje("30");
		return respuesta;
	}
	
	@Override
	public RelacionesTarifaServicioPublicoDTO getTarifasServicioPublico(TarifaServicioPublico tarifaServicioPublicoFiltro) {
		LOG.info("--> en getTarifasServicioPublico");
		RelacionesTarifaServicioPublicoDTO relacionesTarifaServicioPublico = new RelacionesTarifaServicioPublicoDTO();
		try {
			LOG.info("Obtener las tarifas a nivel estado, municipio");
			relacionesTarifaServicioPublico = this.getTarifasRelacionadasServicioPublico(tarifaServicioPublicoFiltro, relacionesTarifaServicioPublico);
			LOG.info("--> Después de obtener las tarifas a nivel estado y municipio");
			if(relacionesTarifaServicioPublico.getDeduciblesAdicionales().size() == 0 || relacionesTarifaServicioPublico.getObligatorias().size() == 0) {
				LOG.info("Obtener las tarifas a nivel estado");
				tarifaServicioPublicoFiltro.setCiudadDTO(new CiudadDTO());
				relacionesTarifaServicioPublico = this.getTarifasRelacionadasServicioPublico(tarifaServicioPublicoFiltro, relacionesTarifaServicioPublico);
			}
			LOG.info("--> Después del IF de obtener las tarifas a nivel estado");
			if(relacionesTarifaServicioPublico.getDeduciblesAdicionales().size() == 0 || relacionesTarifaServicioPublico.getObligatorias().size() == 0) {
				LOG.info("Obtener las tarifas");
				tarifaServicioPublicoFiltro.setEstadoDTO(new EstadoDTO());
				relacionesTarifaServicioPublico = this.getTarifasRelacionadasServicioPublico(tarifaServicioPublicoFiltro, relacionesTarifaServicioPublico);
			}
			LOG.info("--> Después del IF de obtener las tarifas sin filtro de edo y cd");
		} catch (Exception e) {
			LOG.error("Ocurrio un error al obtener las terifas de servicio publico --> getTarifasServicioPublico", e);
		}
		
		LOG.info("--> terminando el método getTarifasServicioPublico");
		return relacionesTarifaServicioPublico;		
	}
	
	@Override
	public boolean esParametroValido(BigDecimal valorParametro, BigDecimal parametroGeneral){
		boolean esParametroValidoTmp=false;
		
		ParametroGeneralId id=new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_COBERTURA_OBLIGATORIA_SERVICIO_PUBLICO,
													 parametroGeneral);
		ParametroGeneralDTO parametro=parametroGeneralFacade.findById(id);
		
		if(parametro !=null && parametro.getValor()!=null){
			String[] valoresParametros=parametro.getValor().split(UtileriasWeb.SEPARADOR_COMA);
			for(String valoresParametrosTmp : valoresParametros){
				if(valoresParametrosTmp.trim().equals(valorParametro.toString())){
					esParametroValidoTmp=true;
					break;
				}
			}
		}
		
		return esParametroValidoTmp;
	}
	
	public RelacionesTarifaServicioPublicoDTO getTarifasRelacionadasServicioPublico(TarifaServicioPublico tarifaServicioPublicoFiltro, RelacionesTarifaServicioPublicoDTO  relacionesTarifaServicioPublico) {
		List<TarifaServicioPublico> tarifaCoberturasAdicionales = new ArrayList<TarifaServicioPublico>(1);
		List<TarifaServicioPublicoSumaAseguradaAd> tarifaSumasAseguradasAdicionales = new ArrayList<TarifaServicioPublicoSumaAseguradaAd>(1);
		List<TarifaServicioPublicoDeduciblesAd> tarifaDeduciblesAdicionales = new ArrayList<TarifaServicioPublicoDeduciblesAd>(1);
		Map<String,Object> params = new HashMap<String, Object>();
		
		params = this.getParametros(tarifaServicioPublicoFiltro);
		
		tarifaCoberturasAdicionales = entidadService.findByProperties(TarifaServicioPublico.class, params);
		tarifaSumasAseguradasAdicionales = entidadService.findByProperties(TarifaServicioPublicoSumaAseguradaAd.class, params);
		relacionesTarifaServicioPublico.setSumasAseguradasAdicionales(tarifaSumasAseguradasAdicionales);

		List<TarifaServicioPublico> tarifaServicioPublicoAdicionales = new ArrayList<TarifaServicioPublico>(1);
		List<TarifaServicioPublico> tarifaServicioPublicoObligatorias = new ArrayList<TarifaServicioPublico>(1);
		NegocioCobPaqSeccion negocioCobPaqSeccion = new NegocioCobPaqSeccion();
		negocioCobPaqSeccion.setEstadoDTO(tarifaServicioPublicoFiltro.getEstadoDTO());
		negocioCobPaqSeccion.setCiudadDTO(tarifaServicioPublicoFiltro.getCiudadDTO());
		negocioCobPaqSeccion.setMonedaDTO(tarifaServicioPublicoFiltro.getMonedaDTO());
		negocioCobPaqSeccion.setNegocioPaqueteSeccion(tarifaServicioPublicoFiltro.getNegocioPaqueteSeccion());
		List<NegocioCobPaqSeccion> negocioCobPaqSeccionAsociadas = new ArrayList<NegocioCobPaqSeccion>(1); 
		negocioCobPaqSeccionAsociadas = negocioCobPaqSeccionDAO.getNegocioCobPaqSeccionAsociados(negocioCobPaqSeccion);
		
		for(NegocioCobPaqSeccion negocioCobPaqSeccionAsociada: negocioCobPaqSeccionAsociadas){
			CobPaquetesSeccionId cobPaquetesSeccionId = new CobPaquetesSeccionId();
			cobPaquetesSeccionId.setIdPaquete(negocioCobPaqSeccionAsociada.getNegocioPaqueteSeccion().getPaquete().getId());
			cobPaquetesSeccionId.setIdToCobertura(negocioCobPaqSeccionAsociada.getCoberturaDTO().getIdToCobertura().longValue());
			cobPaquetesSeccionId.setIdToSeccion(negocioCobPaqSeccionAsociada.getNegocioPaqueteSeccion().getNegocioSeccion().getSeccionDTO().getIdToSeccion().longValue());
			CobPaquetesSeccion cobPaquetesSeccion =  entidadService.findById(CobPaquetesSeccion.class, cobPaquetesSeccionId);
			TarifaServicioPublico tarifa = new TarifaServicioPublico();
			tarifa.setCoberturaDTO(negocioCobPaqSeccionAsociada.getCoberturaDTO());
			tarifa.setMonedaDTO(negocioCobPaqSeccionAsociada.getMonedaDTO());
			tarifa.setNegocioPaqueteSeccion(negocioCobPaqSeccionAsociada.getNegocioPaqueteSeccion());
			tarifa.setVigenciaDTO(tarifaServicioPublicoFiltro.getVigenciaDTO());	
			String descripcion = this.obtenerDescripcionDeducible(tarifa.getCoberturaDTO());
		
			if(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() > negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()) {
				tarifa.setValorSumaAsegurada("Entre " +
						(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin() != null ? UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()) : UtileriasWeb.formatoMoneda(0.0)) + " y " + 
						(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() != null ? UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax()).toString() : UtileriasWeb.formatoMoneda(0.0)) + " con SA Default " +
						(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault() != null ? UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()).toString() : UtileriasWeb.formatoMoneda(0.0)));
			} else {
				String valorSumaAsegurada = negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault() != null ?
						UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()) : UtileriasWeb.formatoMoneda(0.0);
				tarifa.setValorSumaAsegurada(valorSumaAsegurada + (tarifa.getCoberturaDTO().getClaveTipoDeducible().equals("0") ? "" : descripcion)) ;
			}
			
			if(tarifa.getCoberturaDTO().getClaveTipoDeducible().equals("0")  && CoberturaDTO.CLAVE_TIPO_SUMA_ASEGURADA_AMPARADA.equals(tarifa.getCoberturaDTO().getClaveTipoSumaAsegurada())) {
				tarifa.setValorSumaAsegurada(CoberturaDTO.DESCRIPCION_TIPO_SUMA_ASEGURADA_AMPARADA);
			}
		
			if(cobPaquetesSeccion != null && cobPaquetesSeccion.getClaveObligatoriedad().compareTo((short) 0) == 0) {
				tarifaServicioPublicoObligatorias.add(tarifa);
				
				//Obtener deducibles relacionados a la cobertura RC cuando es obligatoria
				if (this.esParametroValido(tarifa.getCoberturaDTO().getIdToCobertura(),ParametroGeneralDTO.CODIGO_PARAM_GENERAL_COBERTURA_OBLIGATORIA_SERVICIO_PUBLICO)) {
					List<NegocioDeducibleCob> deducibles = negocioDeduciblesService.obtenerDeduciblesCobertura(negocioCobPaqSeccionAsociada.getIdToNegCobPaqSeccion());
					for (NegocioDeducibleCob deducible:deducibles) {
						LOG.info("Obtener los deducibles RC idToCobertura " + tarifa.getCoberturaDTO().getIdToCobertura() +
								" negocioDeducibleCob.id " + deducible.getId());
						params.put("coberturaDTO.idToCobertura", tarifa.getCoberturaDTO().getIdToCobertura());
						params.put("negocioDeducibleCob.id", deducible.getId());
						List<TarifaServicioPublicoDeduciblesAd> deduciblesAdicionales = entidadService.findByProperties(TarifaServicioPublicoDeduciblesAd.class, params);
						
						for (TarifaServicioPublicoDeduciblesAd deducibleAdicional:deduciblesAdicionales) {
							if(deducibleAdicional.getClaveSumaAsegurada().compareTo(TarifaServicioPublicoDeduciblesAd.ClaveSumaAsegurada.Maxima.getValor()) == 0) {
								if((negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() > negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()) ||
										(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax().equals(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin())&&
												negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax().equals(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()))) {
									deducibleAdicional.setValorSumaAsegurada(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax());
									deducibleAdicional.setValorSumaAseguradaStr(tarifa.getCoberturaDTO().getDescripcion() + " " + 
											UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax()));
									tarifaDeduciblesAdicionales.add(deducibleAdicional);
								}
							} else if(deducibleAdicional.getClaveSumaAsegurada().compareTo(TarifaServicioPublicoDeduciblesAd.ClaveSumaAsegurada.Minima.getValor()) == 0) {
								if(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin() < negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()) {
									deducibleAdicional.setValorSumaAsegurada(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin());
									deducibleAdicional.setValorSumaAseguradaStr(tarifa.getCoberturaDTO().getDescripcion() + " " + 
											UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()));
									tarifaDeduciblesAdicionales.add(deducibleAdicional);
								}
							}else{
								if (negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() > negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()) {
									deducibleAdicional.setValorSumaAsegurada(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault());
									deducibleAdicional.setValorSumaAseguradaStr(tarifa.getCoberturaDTO().getDescripcion() + " " +
											UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()));
									tarifaDeduciblesAdicionales.add(deducibleAdicional);
								}
							}
						}
					}
				}
			} else {
				for(TarifaServicioPublico tarifaCoberturasAdi:tarifaCoberturasAdicionales) {
					if(tarifa.getCoberturaDTO().getIdToCobertura().compareTo(tarifaCoberturasAdi.getCoberturaDTO().getIdToCobertura()) == 0) {
						tarifaCoberturasAdi.setValorSumaAsegurada(tarifa.getValorSumaAsegurada());
						tarifaServicioPublicoAdicionales.add(tarifaCoberturasAdi);
						break;
					}
				}
			}
			
			for(TarifaServicioPublicoDeduciblesAd tarifaDeducible :relacionesTarifaServicioPublico.getDeduciblesAdicionales()) {
				if(tarifaDeducible.getCoberturaDTO().getIdToCobertura().compareTo(negocioCobPaqSeccionAsociada.getCoberturaDTO().getIdToCobertura()) == 0) {
					if(tarifaDeducible.getClaveSumaAsegurada().compareTo(TarifaServicioPublicoDeduciblesAd.ClaveSumaAsegurada.Maxima.getValor()) == 0) {
						if((negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() > negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()) ||
								(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax().equals(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin())&&
										negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax().equals(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()))) {
							tarifaDeducible.setValorSumaAsegurada(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax());
							tarifaDeducible.setValorSumaAseguradaStr(tarifaDeducible.getCoberturaDTO().getDescripcion() + " " + 
									UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax()));
						}
					} else if(tarifaDeducible.getClaveSumaAsegurada().compareTo(TarifaServicioPublicoDeduciblesAd.ClaveSumaAsegurada.Minima.getValor()) == 0) {
						if(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin() < negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()) {
							tarifaDeducible.setValorSumaAsegurada(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin());
							tarifaDeducible.setValorSumaAseguradaStr(tarifaDeducible.getCoberturaDTO().getDescripcion() + " " + 
									UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()));
						}
					}else{
						if (negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() > negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()) {
							tarifaDeducible.setValorSumaAsegurada(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault());
							tarifaDeducible.setValorSumaAseguradaStr(tarifaDeducible.getCoberturaDTO().getDescripcion() + " " +
									UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()));
						}
					}
				}
			}
		}

		relacionesTarifaServicioPublico.setDeduciblesAdicionales(tarifaDeduciblesAdicionales);
		relacionesTarifaServicioPublico.setObligatorias(tarifaServicioPublicoObligatorias);
		relacionesTarifaServicioPublico.setAdicionales(tarifaServicioPublicoAdicionales);
		
		tarifaSumasAseguradasAdicionales = new ArrayList<TarifaServicioPublicoSumaAseguradaAd>(1);

		for(TarifaServicioPublicoSumaAseguradaAd sumasAdicionales: relacionesTarifaServicioPublico.getSumasAseguradasAdicionales()){
			for(TarifaServicioPublico coberturasObligatorias: tarifaServicioPublicoObligatorias){
				if(sumasAdicionales.getCoberturaDTO().getIdToCobertura().compareTo(coberturasObligatorias.getCoberturaDTO().getIdToCobertura()) == 0){
					for(SumaAseguradaAdicionalCoberturaDTO suma :sumasAdicionales.getCoberturaDTO().getSumasAseguradasAdicionales()) {
						if(suma.getId().compareTo(sumasAdicionales.getIdSumaAseguradaAd()) == 0) {
							sumasAdicionales.setValorSumaAsegurada(suma.getValor());
							break;
						}
					}
					tarifaSumasAseguradasAdicionales.add(sumasAdicionales);
				}
			}
		}
		
		relacionesTarifaServicioPublico.setSumasAseguradasAdicionales(tarifaSumasAseguradasAdicionales);
		
		return relacionesTarifaServicioPublico;	
	}
	
	public Map<String,Object> getParametros(TarifaServicioPublico tarifaServicioPublico) {
		Map<String,Object> params = new HashMap<String, Object>();
		if(tarifaServicioPublico.getNegocioPaqueteSeccion() != null && tarifaServicioPublico.getNegocioPaqueteSeccion().getIdToNegPaqueteSeccion() != null){
			params.put("negocioPaqueteSeccion.idToNegPaqueteSeccion", tarifaServicioPublico.getNegocioPaqueteSeccion().getIdToNegPaqueteSeccion());
		}
		
		if(tarifaServicioPublico.getMonedaDTO() != null && tarifaServicioPublico.getMonedaDTO().getIdTcMoneda()!= null){
			params.put("monedaDTO.idTcMoneda", tarifaServicioPublico.getMonedaDTO().getIdTcMoneda());
		}
		
		if(tarifaServicioPublico.getEstadoDTO() != null && tarifaServicioPublico.getEstadoDTO().getStateId() != null) {
			params.put("estadoDTO.stateId", tarifaServicioPublico.getEstadoDTO().getStateId());
		}
		
		if(tarifaServicioPublico.getCiudadDTO() != null && tarifaServicioPublico.getCiudadDTO().getCityId() != null) {
			params.put("ciudadDTO.cityId", tarifaServicioPublico.getCiudadDTO().getCityId());
		}
		
		if(tarifaServicioPublico.getVigenciaDTO() != null && tarifaServicioPublico.getVigenciaDTO().getIdTcVigencia() != null) {
			params.put("vigenciaDTO.idTcVigencia", tarifaServicioPublico.getVigenciaDTO().getIdTcVigencia());
		}
		
		if(tarifaServicioPublico.getCoberturaDTO() != null && tarifaServicioPublico.getCoberturaDTO().getIdToCobertura() != null) {
			params.put("coberturaDTO.idToCobertura", tarifaServicioPublico.getCoberturaDTO().getIdToCobertura());
		}
		
		return params;
	}
	
	public Map<String,Object> getParametrosDeducibles(TarifaServicioPublicoDeduciblesAd tarifaServicioPublico) {
		Map<String,Object> params = new HashMap<String, Object>();
		if(tarifaServicioPublico.getNegocioPaqueteSeccion() != null && tarifaServicioPublico.getNegocioPaqueteSeccion().getIdToNegPaqueteSeccion() != null){
			params.put("negocioPaqueteSeccion.idToNegPaqueteSeccion", tarifaServicioPublico.getNegocioPaqueteSeccion().getIdToNegPaqueteSeccion());
		}
		
		if(tarifaServicioPublico.getMonedaDTO() != null && tarifaServicioPublico.getMonedaDTO().getIdTcMoneda()!= null){
			params.put("monedaDTO.idTcMoneda", tarifaServicioPublico.getMonedaDTO().getIdTcMoneda());
		}
		
		if(tarifaServicioPublico.getEstadoDTO() != null && tarifaServicioPublico.getEstadoDTO().getStateId() != null) {
			params.put("estadoDTO.stateId", tarifaServicioPublico.getEstadoDTO().getStateId());
		}
		
		if(tarifaServicioPublico.getCiudadDTO() != null && tarifaServicioPublico.getCiudadDTO().getCityId() != null) {
			params.put("ciudadDTO.cityId", tarifaServicioPublico.getCiudadDTO().getCityId());
		}
		
		if(tarifaServicioPublico.getVigenciaDTO() != null && tarifaServicioPublico.getVigenciaDTO().getIdTcVigencia() != null) {
			params.put("vigenciaDTO.idTcVigencia", tarifaServicioPublico.getVigenciaDTO().getIdTcVigencia());
		}
		
		if(tarifaServicioPublico.getCoberturaDTO() != null && tarifaServicioPublico.getCoberturaDTO().getIdToCobertura() != null) {
			params.put("coberturaDTO.idToCobertura", tarifaServicioPublico.getCoberturaDTO().getIdToCobertura());
		}
		
		if(tarifaServicioPublico.getNegocioDeducibleCob() != null && tarifaServicioPublico.getNegocioDeducibleCob().getId() != null) {
			params.put("negocioDeducibleCob.id", tarifaServicioPublico.getNegocioDeducibleCob().getId());
		}

		if(tarifaServicioPublico.getClaveSumaAsegurada() != null) {
			params.put("claveSumaAsegurada", tarifaServicioPublico.getClaveSumaAsegurada());
		}

		return params;
	}
	
	public Map<String,Object> getParametrosSumaAsegurada(TarifaServicioPublicoSumaAseguradaAd tarifaServicioPublico) {
		Map<String,Object> params = new HashMap<String, Object>();
		if(tarifaServicioPublico.getNegocioPaqueteSeccion() != null && tarifaServicioPublico.getNegocioPaqueteSeccion().getIdToNegPaqueteSeccion() != null){
			params.put("negocioPaqueteSeccion.idToNegPaqueteSeccion", tarifaServicioPublico.getNegocioPaqueteSeccion().getIdToNegPaqueteSeccion());
		}
		
		if(tarifaServicioPublico.getMonedaDTO() != null && tarifaServicioPublico.getMonedaDTO().getIdTcMoneda()!= null){
			params.put("monedaDTO.idTcMoneda", tarifaServicioPublico.getMonedaDTO().getIdTcMoneda());
		}
		
		if(tarifaServicioPublico.getEstadoDTO() != null && tarifaServicioPublico.getEstadoDTO().getStateId() != null) {
			params.put("estadoDTO.stateId", tarifaServicioPublico.getEstadoDTO().getStateId());
		}
		
		if(tarifaServicioPublico.getCiudadDTO() != null && tarifaServicioPublico.getCiudadDTO().getCityId() != null) {
			params.put("ciudadDTO.cityId", tarifaServicioPublico.getCiudadDTO().getCityId());
		}
		
		if(tarifaServicioPublico.getVigenciaDTO() != null && tarifaServicioPublico.getVigenciaDTO().getIdTcVigencia() != null) {
			params.put("vigenciaDTO.idTcVigencia", tarifaServicioPublico.getVigenciaDTO().getIdTcVigencia());
		}
		
		if(tarifaServicioPublico.getCoberturaDTO() != null && tarifaServicioPublico.getCoberturaDTO().getIdToCobertura() != null) {
			params.put("coberturaDTO.idToCobertura", tarifaServicioPublico.getCoberturaDTO().getIdToCobertura());
		}
		
		if(tarifaServicioPublico.getIdSumaAseguradaAd() != null) {
			params.put("idSumaAseguradaAd", tarifaServicioPublico.getIdSumaAseguradaAd());
		}
		
		return params;
	}
	
	public String obtenerDescripcionDeducible(CoberturaDTO cobertura){
		String descripcionDeducible = "";
		
		CatalogoValorFijoDTO catalogo = new CatalogoValorFijoDTO();
		
		if (cobertura.getClaveTipoDeducible() != null) {
			try {
				catalogo = entidadService.findById(CatalogoValorFijoDTO.class, new CatalogoValorFijoId(
						SistemaPersistencia.GRUPO_CLAVE_TIPO_DEDUCIBLE, Integer.valueOf(cobertura.getClaveTipoDeducible())));
				
				if (catalogo!= null && catalogo.getValue() != null && !catalogo.getValue().trim().equalsIgnoreCase("N/A")){
					descripcionDeducible = " " + catalogo.getValue();
				}
			} catch(Exception e){}
		}
		
		return descripcionDeducible;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setNegocioCobPaqSeccionDAO(
			NegocioCobPaqSeccionDAO negocioCobPaqSeccionDAO) {
		this.negocioCobPaqSeccionDAO = negocioCobPaqSeccionDAO;
	}

	@EJB
	public void setNegocioDeduciblesService(
			NegocioDeduciblesService negocioDeduciblesService) {
		this.negocioDeduciblesService = negocioDeduciblesService;
	}
	
	public List<TarifaServicioPublicoDeduciblesAd> obtenerDeduciblesAdicionalesSerivicioPublico(TarifaServicioPublico tarifaServicioPublicoFiltro) {
		LOG.info("--> en obtenerDeduciblesAdicionalesSerivicioPublico ...");
		List<TarifaServicioPublicoDeduciblesAd> tarifaDeduciblesAdicionales = new ArrayList<TarifaServicioPublicoDeduciblesAd>(1);
		Map<String,Object> params = new HashMap<String, Object>();
		params = this.getParametros(tarifaServicioPublicoFiltro);
		
		NegocioCobPaqSeccion negocioCobPaqSeccion = new NegocioCobPaqSeccion();
		negocioCobPaqSeccion.setEstadoDTO(tarifaServicioPublicoFiltro.getEstadoDTO());
		negocioCobPaqSeccion.setCiudadDTO(tarifaServicioPublicoFiltro.getCiudadDTO());
		negocioCobPaqSeccion.setMonedaDTO(tarifaServicioPublicoFiltro.getMonedaDTO());
		negocioCobPaqSeccion.setNegocioPaqueteSeccion(tarifaServicioPublicoFiltro.getNegocioPaqueteSeccion());
		List<NegocioCobPaqSeccion> negocioCobPaqSeccionAsociadas = new ArrayList<NegocioCobPaqSeccion>(1); 
		negocioCobPaqSeccionAsociadas = negocioCobPaqSeccionDAO.getNegocioCobPaqSeccionAsociados(negocioCobPaqSeccion);
		
		for(NegocioCobPaqSeccion negocioCobPaqSeccionAsociada: negocioCobPaqSeccionAsociadas){
			CobPaquetesSeccionId cobPaquetesSeccionId = new CobPaquetesSeccionId();
			cobPaquetesSeccionId.setIdPaquete(negocioCobPaqSeccionAsociada.getNegocioPaqueteSeccion().getPaquete().getId());
			cobPaquetesSeccionId.setIdToCobertura(negocioCobPaqSeccionAsociada.getCoberturaDTO().getIdToCobertura().longValue());
			cobPaquetesSeccionId.setIdToSeccion(negocioCobPaqSeccionAsociada.getNegocioPaqueteSeccion().getNegocioSeccion().getSeccionDTO().getIdToSeccion().longValue());
			CobPaquetesSeccion cobPaquetesSeccion =  entidadService.findById(CobPaquetesSeccion.class, cobPaquetesSeccionId);
			TarifaServicioPublico tarifa = new TarifaServicioPublico();
			tarifa.setCoberturaDTO(negocioCobPaqSeccionAsociada.getCoberturaDTO());
			tarifa.setMonedaDTO(negocioCobPaqSeccionAsociada.getMonedaDTO());
			tarifa.setNegocioPaqueteSeccion(negocioCobPaqSeccionAsociada.getNegocioPaqueteSeccion());
			tarifa.setVigenciaDTO(tarifaServicioPublicoFiltro.getVigenciaDTO());	
			String descripcion = this.obtenerDescripcionDeducible(tarifa.getCoberturaDTO());
		
			if(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() > negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()) {
				tarifa.setValorSumaAsegurada("Entre " +
						(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin() != null ? UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()) : UtileriasWeb.formatoMoneda(0.0)) + " y " + 
						(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() != null ? UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax()).toString() : UtileriasWeb.formatoMoneda(0.0))+ " con SA Default " + 
						(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault() != null ? UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()).toString() : UtileriasWeb.formatoMoneda(0.0)));
			} else {
				String valorSumaAsegurada = negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault() != null ?
						UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()) : UtileriasWeb.formatoMoneda(0.0);
				tarifa.setValorSumaAsegurada(valorSumaAsegurada + (tarifa.getCoberturaDTO().getClaveTipoDeducible().equals("0") ? "" : descripcion)) ;
			}
			
			if(tarifa.getCoberturaDTO().getClaveTipoDeducible().equals("0")  && CoberturaDTO.CLAVE_TIPO_SUMA_ASEGURADA_AMPARADA.equals(tarifa.getCoberturaDTO().getClaveTipoSumaAsegurada())) {
				tarifa.setValorSumaAsegurada(CoberturaDTO.DESCRIPCION_TIPO_SUMA_ASEGURADA_AMPARADA);
			}
		
			if(cobPaquetesSeccion != null && cobPaquetesSeccion.getClaveObligatoriedad().compareTo((short) 0) == 0) {
				//Obtener deducibles relacionados a la cobertura RC cuando es obligatoria
				if (this.esParametroValido(tarifa.getCoberturaDTO().getIdToCobertura(),ParametroGeneralDTO.CODIGO_PARAM_GENERAL_COBERTURA_OBLIGATORIA_SERVICIO_PUBLICO)) {
					List<NegocioDeducibleCob> deducibles = negocioDeduciblesService.obtenerDeduciblesCobertura(negocioCobPaqSeccionAsociada.getIdToNegCobPaqSeccion());
					for (NegocioDeducibleCob deducible:deducibles) {
						LOG.info("Obtener los deducibles RC idToCobertura " + tarifa.getCoberturaDTO().getIdToCobertura() +
								" negocioDeducibleCob.id " + deducible.getId());
						params.put("coberturaDTO.idToCobertura", tarifa.getCoberturaDTO().getIdToCobertura());
						params.put("negocioDeducibleCob.id", deducible.getId());
						List<TarifaServicioPublicoDeduciblesAd> deduciblesAdicionales = entidadService.findByProperties(TarifaServicioPublicoDeduciblesAd.class, params);
		
						for (TarifaServicioPublicoDeduciblesAd deducibleAdicional:deduciblesAdicionales) {
							if(deducibleAdicional.getClaveSumaAsegurada().compareTo(TarifaServicioPublicoDeduciblesAd.ClaveSumaAsegurada.Maxima.getValor()) == 0) {
								if((negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() > negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()) ||
										(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax().equals(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin())&&
												negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax().equals(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()))) {
									deducibleAdicional.setValorSumaAsegurada(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax());
									deducibleAdicional.setValorSumaAseguradaStr(tarifa.getCoberturaDTO().getDescripcion() + " " +
											UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax()));
									tarifaDeduciblesAdicionales.add(deducibleAdicional);
								}
							} else if(deducibleAdicional.getClaveSumaAsegurada().compareTo(TarifaServicioPublicoDeduciblesAd.ClaveSumaAsegurada.Minima.getValor()) == 0) {
								if(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin() < negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()) {
									deducibleAdicional.setValorSumaAsegurada(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin());
									deducibleAdicional.setValorSumaAseguradaStr(tarifa.getCoberturaDTO().getDescripcion() + " " +
											UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaMin()));
									tarifaDeduciblesAdicionales.add(deducibleAdicional);
								}
							}else{
								if (negocioCobPaqSeccionAsociada.getValorSumaAseguradaMax() > negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()) {
									deducibleAdicional.setValorSumaAsegurada(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault());
									deducibleAdicional.setValorSumaAseguradaStr(tarifa.getCoberturaDTO().getDescripcion() + " " +
											UtileriasWeb.formatoMoneda(negocioCobPaqSeccionAsociada.getValorSumaAseguradaDefault()));
									tarifaDeduciblesAdicionales.add(deducibleAdicional);
								}
							}
						}
					}
				}
			} else {

			}
		}
		
		return tarifaDeduciblesAdicionales;	
	}
}