package mx.com.afirme.midas2.service.impl.compensaciones;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CatalogoCompensacionesDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.compensaciones.CaBaseCalculo;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.Proveedor;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.service.compensaciones.CatalogoCompensacionesService;

@Stateless
public class CatalogoCompensacionesServiceImpl implements CatalogoCompensacionesService{

	@EJB
	private CatalogoCompensacionesDao catalogoCompensacionesDao;
	
	@Override
	public List<CaBaseCalculo> listarBaseCalculo() {
		return catalogoCompensacionesDao.listarBaseCalculo();
	}

	@Override
	public List<CaRamo> listarRamos() {
		return catalogoCompensacionesDao.listarRamos();
	}

	@Override
	public List<CaTipoCompensacion> listarTipoCompensacion() {
		return catalogoCompensacionesDao.listarTipoCompensacion();
	}
	
	public List<Proveedor> listarProveedores(String nombreProveedor){
		return catalogoCompensacionesDao.listarProveedores(nombreProveedor);
	}
	
	public List<Negocio> listarNegocios(String descripcion){
		return catalogoCompensacionesDao.listarNegocios(descripcion);
	}
	public List<Proveedor> listarProveedoresId(String id){
		return catalogoCompensacionesDao.listarProveedoresId(id);
	}
	public List<CaEntidadPersona> listarAgentesProveedores(String nombre,Long tipo){
		return catalogoCompensacionesDao.listarAgentesProveedores(nombre, tipo);
	}	

	public List<Promotoria> listarPromotores(String nombrePromotor){
		return catalogoCompensacionesDao.listarPromotores(nombrePromotor);
	}

	
}
