package mx.com.afirme.midas2.service.impl.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCruceroJuridica;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionCruceroJuridicaService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.utils.CommonUtils;

@Stateless
public class RecuperacionCruceroJuridicaServiceImpl extends RecuperacionServiceImpl implements RecuperacionCruceroJuridicaService{
	
	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	@EJB
	BitacoraService bitacoraService;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public RecuperacionCruceroJuridica guardar(RecuperacionCruceroJuridica recuperacion) {
		Bitacora.EVENTO evento;
		if(CommonUtils.isNull(recuperacion.getId())){
			recuperacion.setFechaCreacion(new Date());
			recuperacion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
			recuperacion.setTipo(Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.toString());
			recuperacion.setOrigen(Recuperacion.OrigenRecuperacion.AUTOMATICA.toString());
			recuperacion.setNumero(entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO));
			recuperacion.setEstatus(Recuperacion.EstatusRecuperacion.REGISTRADO.toString());
			entidadService.save(recuperacion);
			evento = EVENTO.CREAR_RECUPERACION;
		}else{
			recuperacion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			recuperacion.setFechaModificacion(new Date());
			entidadService.save(recuperacion);
			evento = EVENTO.EDITAR_RECUPERACION;
		}
		bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, evento, recuperacion.getId().toString(), 
				 String.format("Se crea/actualiza la recuperación de crucero [ %1$s ] de acuerdo al siniestro [ %2$s ]", 
						 String.valueOf(recuperacion.getNumero()), 
						 String.valueOf(recuperacion.getSiniestroCabina().getId())), 
				 recuperacion.toString(), usuarioService.getUsuarioActual().getNombreUsuario());
	return recuperacion;
	}
	
	@Override
	public BigDecimal montoTotalRecuperacionCrucero(Long siniestroCabinaId, Recuperacion.EstatusRecuperacion estatusRecuperacion){
		BigDecimal montoTotal = BigDecimal.ZERO;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("siniestroCabina.id", siniestroCabinaId);
		params.put("estatus", estatusRecuperacion.toString());
		List<RecuperacionCruceroJuridica> list = entidadService.findByProperties(RecuperacionCruceroJuridica.class, params);
		for(RecuperacionCruceroJuridica e: list){
			montoTotal = montoTotal.add(e.getMontoFinal());
		}
		return montoTotal;
	}

}
