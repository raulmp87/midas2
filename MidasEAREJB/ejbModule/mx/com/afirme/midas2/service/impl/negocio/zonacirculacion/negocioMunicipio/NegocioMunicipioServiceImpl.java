package mx.com.afirme.midas2.service.impl.negocio.zonacirculacion.negocioMunicipio;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.zonaCirculacion.NegocioMunicipioDao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioMunicipio;
import mx.com.afirme.midas2.dto.negocio.zonacirculacion.negocioMunicipio.RelacionesNegocioMunicipio;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.zonacirculacion.negocioMunicipio.NegocioMunicipioService;

@Stateless
public class NegocioMunicipioServiceImpl implements NegocioMunicipioService {
	private EntidadService entidadService;
	private EntidadDao catalogoDao;
	private NegocioMunicipioDao municipioDao;
	
	@Override
	public RelacionesNegocioMunicipio getRelationLists(Long id) {
		NegocioEstado negocioEstado = new NegocioEstado();
		negocioEstado = entidadService.findById(NegocioEstado.class, id);
		
		List<NegocioMunicipio> negocioMunicipiosAsociadas = entidadService.findByProperty(NegocioMunicipio.class,
				"negocioEstado.id", id);

		List<MunicipioDTO> posibles = entidadService.findByProperty(
				MunicipioDTO.class, "stateId", negocioEstado.getEstadoDTO()
						.getStateId());

		List<NegocioMunicipio> negocioMunicipioDisponibles = new ArrayList<NegocioMunicipio>(
				1);

		RelacionesNegocioMunicipio relacionesNegocioMunicipio = new RelacionesNegocioMunicipio();

		List<MunicipioDTO> tipoMunicipiosAsociados = new ArrayList<MunicipioDTO>(
				1);

		for (NegocioMunicipio item : negocioMunicipiosAsociadas) {
			tipoMunicipiosAsociados.add(item.getMunicipioDTO());
		}

		for (MunicipioDTO item : posibles) {
			if (!tipoMunicipiosAsociados.contains(item)) {
				NegocioMunicipio negocioMunicipio = new NegocioMunicipio();
				negocioMunicipio.setMunicipioDTO(item);
				negocioMunicipioDisponibles.add(negocioMunicipio);
			}
		}

		relacionesNegocioMunicipio.setAsociadas(negocioMunicipiosAsociadas);
		relacionesNegocioMunicipio.setDisponibles(negocioMunicipioDisponibles);
		return relacionesNegocioMunicipio;
	}

	@Override
	public Long relacionarNegocio(String accion,NegocioMunicipio negocioMunicipio) {
		try{
			if(negocioMunicipio.getId() != null && accion.equals("deleted")){
				negocioMunicipio = entidadService.findById(NegocioMunicipio.class, negocioMunicipio.getId());
			}
			catalogoDao.executeActionGrid(accion, negocioMunicipio);
		}catch(RuntimeException e){
			return negocioMunicipio.getId();
		}
		return null;
    }
	
	@Override
	public List<NegocioMunicipio> obtenerMunicipiosPorEstadoId(Long idToNegocio,String idToEstado) {
		return municipioDao.obtenerMunicipiosPorEstadoId(idToNegocio,idToEstado);
	}
	
	@Override
	public String setComboNegocioEstados() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public String setFindMunicipios(Long idMunicipio) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	} 
	
	@Override
	public Long obtenerMunicipiosPorEstadoId(Long idToEstado) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@EJB
	public void setCatalogoService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}


	@EJB
	public void setCatalogoDao(EntidadDao catalogoDao) {
		this.catalogoDao = catalogoDao;
	
	}

	@EJB
	public void setMunicipioDao(NegocioMunicipioDao municipioDao) {
		this.municipioDao = municipioDao;
	}

	@Override
	public NegocioMunicipio findByNegocioAndEstadoAndMunicipio(
			Long idToNegocio, String stateId, String municipalityId) {
		return municipioDao.findByNegocioAndEstadoAndMunicipio(idToNegocio, stateId, municipalityId);
	}

	@Override
	public NegocioMunicipio findByNegocioAndEstadoAndMunicipio(Negocio negocio,
			EstadoDTO estado, MunicipioDTO municipio) {
		return municipioDao.findByNegocioAndEstadoAndMunicipio(negocio, estado, municipio);
	}
	
}
	
