package mx.com.afirme.midas2.service.impl.bitemporal.siniestros;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.MidasInterfaceBaseAuto;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.giro.CatalogoGiroDTO;
import mx.com.afirme.midas.catalogos.giro.CatalogoGiroFacadeRemote;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.interfaz.cliente.DireccionCliente;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudDataEnTramiteDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.bitemporal.siniestros.PolizaSiniestroDao;
import mx.com.afirme.midas2.dao.endoso.cotizacion.auto.CotizacionEndosoDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionDAO;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoDao;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspInc;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.CondicionEspCotContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.CondicionEspIncContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.BitemporalDatoSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.DatoSeccionContinuity;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;
import mx.com.afirme.midas2.domain.cobranza.prorroga.ToProrroga;
import mx.com.afirme.midas2.domain.condicionesespeciales.AreaCondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.FactorAreaCondEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.VarAjusteCondicionEsp;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.BitacoraEventoSiniestro.TipoEvento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CondicionEspecialReporte;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina.EstatusVigenciaInciso;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MonitorEmisionPolizaLog;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SeccionReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SolicitudReporteCabinaDTO;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicos;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.gastoajuste.GastoAjusteReporte;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacion;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacionVigencia;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.dto.CondicionEspecialDTO;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosContratanteDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosPersonalizacionDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoPolizaDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ProgramaPagoDTO;
import mx.com.afirme.midas2.dto.siniestros.ReciboProgramaPagoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.VarModifDescripcionService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.cobranza.prorroga.ProrrogaService;
import mx.com.afirme.midas2.service.impl.siniestros.catalogo.solicitudautorizacion.CatSolicitudAutorizacionServiceImpl;
import mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.emision.ImpresionesServiceImpl;
import mx.com.afirme.midas2.service.negocio.tarifa.NegocioTarifaService;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionIncisoGrupoProliberAJService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionIncisoGrupoProliberAVService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.SolicitudDataEnTramiteService;
import mx.com.afirme.midas2.service.tarifa.TarifaAgrupadorTarifaService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDateTime;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class PolizaSiniestroServiceImpl implements PolizaSiniestroService{

	public static final String ESTATUS_INCISO_VIGENTE 		= "VIGENTE";
	public static final String ESTATUS_INCISO_VENCIDA 		= "NO VIGENTE";
	public static final String ESTATUS_INCISO_CANCELADO		= "CANCELADO";
	public static final int GRUPO_CILINDROS 				= 20;
	public static final String CODIGO_FECHA_OCURRIDO 		= "001";
	public static final String CODIGO_HORA_OCURRIDO 		= "002";
	public static final String CODIGO_LUGAR_OCURRIDO 		= "003";
	public static final String CODIGO_PASE_MEDICO	 		= "007";
	public static final String CODIGO_TIPO_SINIESTRO 		= "004";
	public static final String CODIGO_TIPO_RESPONSABILIDAD	= "005";
	public static final String CODIGO_EDAD_CONDUCTOR		= "008";
	public static final String CODIGO_GENERO				= "009";
	public static final String CODIGO_TIPO_LICENCIA			= "010";
	public static final String CODIGO_NUM_OCUPANTES			= "011";
	public static final String CODIGO_TERCERO_ASEGURADO		= "012";
	public static final String CODIGO_AREA_CABINA			= "CAB";
	public static final String CALCULO_PASE_MEDICO	 		= "GM";
	public static final String CODIGO_SERVICIO		 		= "SER";
	public static final String CODIGO_NUM_EVENTOS	 		= "EVE";
	public static final String CARTA_COBERTURA_ESTATUS		= "EN PROCESO";
	public static final String CARTA_COBERTURA_MOTIVO		= "POR CARTA COBERTURA";
	private static String MENSAGES=SistemaPersistencia.RECURSO_MENSAJES_EXCEPCIONES;
	private static final int TRANSCURRIDAS_24HRS = 24;
	private static final int TRANSCURRIDAS_48HRS = 48;
	private static final int TRANSCURRIDAS_72HRS = 72;
	public static final Logger log = Logger.getLogger(PolizaSiniestroServiceImpl.class);
	List<String> mensajesEvaluacion = new ArrayList<String>();
	
//	MOTIVO CANCELACION QUE PERMITE AUTORIZACION
	public static final String MOTIVO_FALTA_PAGO = "CANCELADA POR FALTA DE PAGO";
//		"CANCELADA A PETICION"; //BORRAR, PARA PRUEBAS
	
	private DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	
	@EJB
	public EntidadService entidadService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	public UsuarioService usuarioService;
	
	@EJB
	public IncisoViewService incisoViewService;
	
	@EJB
	private PolizaSiniestroDao polizaSiniestroDao;
	
	@EJB
	public CondicionEspecialBitemporalService condicionEspecialBitemporalService;
	
	@EJB
	CondicionEspecialService condicionEspecialService;
	
	@EJB
	EntidadContinuityService entidadContinuityService;
	
	@EJB
    private ClienteFacadeRemote clienteFacadeRemote;
	
	@EJB
	private PaisFacadeRemote paisFacadeRemote;
	
	@EJB
	private EstadoFacadeRemote estadoFacadeRemote;
	
	@EJB
	private MunicipioFacadeRemote municipioFacadeRemote;
	
	@EJB
	private VarModifDescripcionService varModifDescripcionService;
		
	@EJB
	private CatalogoGiroFacadeRemote giroFacadeRemote;
	
	@EJB
	private SolicitudDataEnTramiteService solicitudDataEnTramiteService;
	
	@EJB
	private ProrrogaService prorrogaService;
	
	@EJB
    private ReporteCabinaService reporteCabinaService;
		
	@EJB
	private SiniestroCabinaService siniestroCabinaService;
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@EJB
	private ListadoService listadoService;
	
	@EJB 
	private EnvioNotificacionesService envioNotificacionesService;
	
	@EJB
	private CotizacionEndosoDao cotizacionEndosoDao;
	
	@EJB
	private NegocioCobPaqSeccionDAO negocioCobPaqSeccionDAO;
	
	@EJB
	private ConfiguracionDatoIncisoDao configuracionDatoIncisoDao;
	
	@EJB
	private NegocioTarifaService negocioTarifaService;
	
	@EJB
	private TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService;
	
	@EJB
	private DireccionMidasService direccionMidasService;

	private ServiceLocatorP serviceLocator;
		
	private AutoIncisoReporteCabina autoInciso;
	
	@EJB
	private EntidadContinuityDao entidadContinuityDao;
	
	@Resource	
	private TimerService timerService;
	
	private SistemaContext sistemaContext;
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}

	
	/**
	 * Obtiene las Coberturas asociadas a un Inciso de una Póliza
	 */
	@Override
	public List<BitemporalCoberturaSeccion> obtenerCoberturasInciso( Long incisoContinuityId, DateTime validOn, DateTime knownOn, Date fechaReporteSiniestro ) {
		log.debug( "PolizaSiniestroService.obtenerCoberturasInciso :: incisoContinuityId:  " + incisoContinuityId + " validOn: " + validOn + " recordFrom: " + knownOn );
		List<BitemporalCoberturaSeccion> biCoberturaSeccionList =new ArrayList<BitemporalCoberturaSeccion>();
		NegocioCobPaqSeccion negocioCobPaqSeccion;

		try{

			Map<String, Object> params = new LinkedHashMap<String, Object>();

			BitemporalInciso biInciso =  incisoViewService.getInciso(incisoContinuityId, validOn.toDate(), knownOn.toDate());
			
			CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, new BigDecimal(biInciso.getContinuity().getCotizacionContinuity().getNumero()));
	
			BitemporalAutoInciso bAutoInciso = (BitemporalAutoInciso) getCurrentBitemporal(biInciso.getContinuity().getAutoIncisoContinuity(), 
					new DateTime(fechaReporteSiniestro), biInciso.getRecordInterval().getInterval().getStart()/*ofal*/, cotizacion.getIdToCotizacion());
			
			/*BitemporalCotizacion  bCotizacion = (BitemporalCotizacion) getCurrentBitemporal(biInciso.getContinuity().getCotizacionContinuity(), 
					new DateTime(fechaReporteSiniestro), null, cotizacion.getIdToCotizacion());*/			
			
			Collection<SeccionIncisoContinuity> incisosSeccion = entidadContinuityService.findContinuitiesByParentKey(SeccionIncisoContinuity.class, 
					SeccionIncisoContinuity.PARENT_KEY_NAME, incisoContinuityId);
			
			boolean imprimirUMA = listadoService.imprimirLeyendaUMA(cotizacion.getFechaInicioVigencia());
			
			for (SeccionIncisoContinuity seccionIncisoContinuity: incisosSeccion) {
				BitemporalSeccionInciso bitemporalSeccionInciso = (BitemporalSeccionInciso) getCurrentBitemporal(seccionIncisoContinuity, 
						new DateTime(fechaReporteSiniestro), biInciso.getRecordInterval().getInterval().getStart()/*ofal*/, cotizacion.getIdToCotizacion());
			
				if (bitemporalSeccionInciso != null) {
					Collection<CoberturaSeccionContinuity> coberturasSeccion = entidadContinuityService.findContinuitiesByParentKey(
							CoberturaSeccionContinuity.class, CoberturaSeccionContinuity.PARENT_KEY_NAME, bitemporalSeccionInciso.getContinuity().getId());
				
					for (CoberturaSeccionContinuity coberturaSeccionContinuity : coberturasSeccion) {
						BitemporalCoberturaSeccion  bitemporalCobertura = (BitemporalCoberturaSeccion) getCurrentBitemporal(
								coberturaSeccionContinuity, new DateTime(fechaReporteSiniestro), 
								biInciso.getRecordInterval().getInterval().getStart()/*ofal*/, cotizacion.getIdToCotizacion());
					
						if (bitemporalCobertura != null && bitemporalCobertura.getValue().getClaveContrato() == 1) {
							biCoberturaSeccionList.add(bitemporalCobertura);						
						}
					}
				}				
			}

			Collections.sort(biCoberturaSeccionList, new Comparator<BitemporalCoberturaSeccion>() {
				@Override
				public int compare(BitemporalCoberturaSeccion n1,
						BitemporalCoberturaSeccion n2) {
					return n1.getContinuity()
					.getCoberturaDTO()
					.getNumeroSecuencia()
					.compareTo(
							n2.getContinuity().getCoberturaDTO()
							.getNumeroSecuencia());
				}
			});
			
			params = new HashMap<String, Object>();
			params.put("id.idGrupoValores", Integer.valueOf(38));
			
			List<CatalogoValorFijoDTO> posiblesDeducibles = entidadService.findByProperties(CatalogoValorFijoDTO.class, params);


			for (BitemporalCoberturaSeccion coberturaSeccion : biCoberturaSeccionList) {
				negocioCobPaqSeccion = negocioCobPaqSeccionDAO.getLimitesSumaAseguradaPorCobertura(coberturaSeccion.getContinuity().getCoberturaDTO().getIdToCobertura(),bAutoInciso.getValue().getEstadoId(), 
						bAutoInciso.getValue().getMunicipioId(), cotizacion.getIdMoneda(), bAutoInciso.getValue().getNegocioPaqueteId());

				if (negocioCobPaqSeccion != null && negocioCobPaqSeccion.getValorSumaAseguradaMax() != null && negocioCobPaqSeccion.getValorSumaAseguradaMin() != null) {
					coberturaSeccion.getValue().setValorSumaAseguradaMax(negocioCobPaqSeccion.getValorSumaAseguradaMax());
					coberturaSeccion.getValue().setValorSumaAseguradaMin(negocioCobPaqSeccion.getValorSumaAseguradaMin());
				} else {
					coberturaSeccion.getValue().setValorSumaAseguradaMax(new Double("0"));
					coberturaSeccion.getValue().setValorSumaAseguradaMin(new Double("0"));
				}

				coberturaSeccion.getValue().setDescripcionDeducible(getDescripcionDeducible(posiblesDeducibles, coberturaSeccion.getContinuity().getCoberturaDTO().getClaveTipoDeducible()));
				
				if (imprimirUMA && (coberturaSeccion.getValue().getDescripcionDeducible() != null && coberturaSeccion.getValue().getDescripcionDeducible().indexOf(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF) >= 0)){
					coberturaSeccion.getValue().setDescripcionDeducible(coberturaSeccion.getValue().getDescripcionDeducible().replace(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF,
							ImpresionesServiceImpl.TIPO_VALOR_SA_UMA));
				}
			}
		}catch( Exception ex ){
			log.error(ex);
			ex.printStackTrace();
    	}
		return biCoberturaSeccionList;
	}
	
	private String getDescripcionDeducible(List<CatalogoValorFijoDTO> posiblesDeducibles, String tipoDeducible) {
		String descripcion = "";
		int valorTipoDeducible;
		if (tipoDeducible != null) {
			valorTipoDeducible = Integer.valueOf(tipoDeducible);

			for (CatalogoValorFijoDTO valor: posiblesDeducibles) {
				if (valor.getId().getIdDato() == valorTipoDeducible) {
					descripcion = valor.getDescripcion();					
					break;
				}
			}
		}
		return descripcion;
	}

	private DatosPersonalizacionDTO buscarDatosPersonalizacion(Long incisoContinuityId, Date validOn, Date knownOn, Date fechaHoraSiniestro) {
		
		DatosPersonalizacionDTO datosPersonalizacionDTO 		= null;
		EstiloVehiculoDTO estiloVehiculoDTO  					= null;
		BitemporalAutoInciso bitemporalAutoInciso 				= null;
		
		IncisoContinuity incisoContinuity = entidadContinuityService.findContinuityByKey(IncisoContinuity.class, incisoContinuityId);		
		
		BitemporalInciso bitemporalInciso = incisoContinuity.getBitemporalProperty().get(new DateTime(validOn), new DateTime(knownOn));
		
		bitemporalAutoInciso = (BitemporalAutoInciso) getCurrentBitemporal(incisoContinuity.getAutoIncisoContinuity(),
				new DateTime(fechaHoraSiniestro), bitemporalInciso.getRecordInterval().getInterval().getStart()/*ofal*/, 
				new BigDecimal(incisoContinuity.getCotizacionContinuity().getNumero()));
		
		if (bitemporalAutoInciso != null) {
			
			estiloVehiculoDTO = entidadService.findById(EstiloVehiculoDTO.class, getEstiloVehiculoId(bitemporalAutoInciso.getValue().getEstiloId()));
				
			if(estiloVehiculoDTO != null){
				datosPersonalizacionDTO = transformEstiloVehiculoToDatosPersonalizacion(estiloVehiculoDTO);
			}else{
				datosPersonalizacionDTO = new DatosPersonalizacionDTO();
			}
			
		}
		
		return datosPersonalizacionDTO;
	}
	
	private EstiloVehiculoId getEstiloVehiculoId(String estiloId) {
		String[] arrayEstilo = StringUtils.split(estiloId, '_');
		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(arrayEstilo[0], arrayEstilo[1], new BigDecimal(arrayEstilo[2]));
		return estiloVehiculoId;		
	}
							 

	@Override
	public IncisoSiniestroDTO buscarDetalleInciso(IncisoSiniestroDTO filtro, Date validOn, Date knownOn, Date fechaHoraSiniestro) {
		
		IncisoSiniestroDTO incisoSiniestroDTO 		= new IncisoSiniestroDTO();
		CotizacionContinuity cotizacionContinuity	= null;
		BitemporalCotizacion cotizacionBitemporal 	= null;
		IncisoContinuity incisoContinuity 			= null;
		CotizacionDTO cotizacion					= null;
		PolizaDTO polizaAnterior 					= null;
		BigDecimal idCotizacion 					= null;
		BigDecimal personaContratanteId				= null;
		Long incisoContinuityId 					= filtro.getIncisoContinuityId();
		ToProrroga prorroga             			= null;
		
		incisoSiniestroDTO = buscarInciso(filtro, validOn, knownOn);

		incisoContinuity = entidadContinuityService.findContinuityByKey(IncisoContinuity.class, incisoContinuityId);
		if (incisoContinuity != null) {
			cotizacionContinuity = incisoContinuity.getCotizacionContinuity();
			
			cotizacionBitemporal = (BitemporalCotizacion) getCurrentBitemporal(cotizacionContinuity, 
					new DateTime(fechaHoraSiniestro), new DateTime(knownOn)/*ofal*/, new BigDecimal(incisoContinuity.getCotizacionContinuity().getNumero()));

			if (cotizacionBitemporal != null) {				

				idCotizacion = BigDecimal.valueOf( cotizacionBitemporal.getContinuity().getNumero() ) ;
				cotizacion = entidadService.findById(CotizacionDTO.class, idCotizacion );
				personaContratanteId = BigDecimal.valueOf( cotizacionBitemporal.getValue().getPersonaContratanteId() );

				if( !cotizacion.getSolicitudDTO().getIdToPolizaAnterior().equals(BigDecimal.ZERO)){
					polizaAnterior = entidadService.findById(PolizaDTO.class, cotizacion.getSolicitudDTO().getIdToPolizaAnterior());

					if(polizaAnterior != null){
						incisoSiniestroDTO.setNumeroPolizaAnterior( polizaAnterior.getNumeroPolizaFormateada() );
					}else{
						incisoSiniestroDTO.setNumeroPolizaAnterior( "" );
					}
				}else {
					incisoSiniestroDTO.setNumeroPolizaAnterior( "" );
				}
				
				//Date fechaRealEmisionPolizaFn = entidadService.findById(PolizaDTO.class, incisoSiniestroDTO.getIdToPoliza()).getFechaCreacion(); 
				//incisoSiniestroDTO.setFechaRealEmisionPoliza(fechaRealEmisionPolizaFn);
				incisoSiniestroDTO.setProducto(  cotizacion.getSolicitudDTO().getProductoDTO().getDescripcion() );
				incisoSiniestroDTO.setMedioPago( cotizacionBitemporal.getValue().getMedioPago().getDescripcion() );
				incisoSiniestroDTO.setMoneda( cotizacionBitemporal.getValue().getMoneda().getDescripcion() );
				incisoSiniestroDTO.setFormaPago( cotizacionBitemporal.getValue().getFormaPago().getDescripcion());
				incisoSiniestroDTO.setDatosContratante( obtenerDatosContratante(personaContratanteId));
				incisoSiniestroDTO.setDatosPersonalizacion( buscarDatosPersonalizacion(incisoContinuityId, validOn, knownOn, fechaHoraSiniestro));
			}			
		}
		
		prorroga = this.obtenerProrroga(idCotizacion.longValue(), incisoSiniestroDTO.getNumeroInciso().longValue(), filtro.getFechaReporteSiniestro() );
		
		if( prorroga != null ){
			incisoSiniestroDTO.setTieneProrroga(true);
		}		
		
		return incisoSiniestroDTO;
	}


	@Override
	public IncisoSiniestroDTO buscarInciso(IncisoSiniestroDTO filtro, Date validOn, Date knownOn ) {
		
		IncisoSiniestroDTO incisoSiniestroDTO 		= new IncisoSiniestroDTO();
		BitemporalInciso incisoBitemporal 			= null;
		IncisoContinuity incisoContinuity 			= null;
		Long incisoContinuityId 					= filtro.getIncisoContinuityId();
		
		
		incisoContinuity = entidadContinuityService.findContinuityByKey(IncisoContinuity.class, incisoContinuityId);
		
		if (incisoContinuity != null) {
		incisoBitemporal = incisoContinuity.getBitemporalProperty().get( new DateTime(validOn) , new DateTime(knownOn) );
		
			if (incisoBitemporal != null) {
				loadInfoIncisoSiniestroDTO(incisoSiniestroDTO, incisoBitemporal , filtro.getFechaReporteSiniestro() );		
			}			
		}		
		
		return incisoSiniestroDTO;
	}


	@Override
	public List<IncisoPolizaDTO> buscarIncisosPoliza(String numeroPoliza, String nombreAsegurado, String numeroSerie, 
			Integer numeroInciso, Date fechaOcurridoSiniestro, Short tipoPoliza) {
		if (tipoPoliza == null) {
			tipoPoliza = (short) 0;
		}
		
		return  polizaSiniestroDao.buscarIncisosPoliza(numeroPoliza, nombreAsegurado, numeroSerie, 
														numeroInciso, fechaOcurridoSiniestro, tipoPoliza);
	}
	
	/**
	 * Validar si el objeto filtro contiene los suficientes datos para realizar una busqueda de polizas
	 * @param filtro
	 * @return
	 */
	private boolean validarFiltroVacioPoliza(IncisoSiniestroDTO filtro){
		int contador = 0;		
		if(!StringUtil.isEmpty(filtro.getNumeroPoliza())){
			contador++;
		}
		if(filtro.getAutoInciso() != null){
			if(!StringUtil.isEmpty(filtro.getAutoInciso().getNumeroSerie())){
				contador++;
			}
			if(!StringUtil.isEmpty(filtro.getAutoInciso().getNumeroMotor())){
				contador++;
			}
			if(!StringUtil.isEmpty(filtro.getAutoInciso().getPlaca())){
				contador++;
			}				
		}
		if(!StringUtil.isEmpty(filtro.getNombreContratante())){
			contador++;
		}
		if(!StringUtil.isEmpty(filtro.getNumPolizaSeycos())){
			contador++;
		}
		if(filtro.getNumeroInciso() != null){
			contador++;
		}
		if(filtro.getCvePolizaMidas()!= null){
			contador++;
		}
		
		if(filtro.getFechaIniVigencia() != null){
			contador++;
		}
		if(filtro.getFechaFinVigencia() != null){
			contador++;
		}
		if(!StringUtil.isEmpty(filtro.getNumeroFolio())){
			contador++;
		}
		if(contador > 0){
			return true;
		}
		return false;
	}
	
	@Override
	public List<IncisoSiniestroDTO> buscarPolizaFiltro(IncisoSiniestroDTO filtro, Date validOn, Date knownOn) {
		List<IncisoSiniestroDTO> incisos = new ArrayList<IncisoSiniestroDTO>(1);
		Boolean existenVigentes = false;
		
		try {
			Date validOnNoTime = null;
			
			//Desde la pantalla de busquedas de polizas la fecha se recibe null y se coloca la fecha actual
			if(validOn != null) {
				validOnNoTime = format.parse(format.format(validOn));
			} else {
				validOnNoTime = format.parse(format.format(new Date()));
			}
			if(knownOn == null) {
				knownOn = new Date();			
			}
			
		
			
		if(validarFiltroVacioPoliza(filtro)){	
			incisos = polizaSiniestroDao.buscarIncisosPolizaFiltro(filtro, validOnNoTime, knownOn);
		}
		//List<IncisoPolizaDTO> dto = polizaSiniestroDao.buscarIncisosPoliza(null, null, "HGFHGH45435", 1, null, (short) 0);
		
		for (IncisoSiniestroDTO inciso : incisos) {
			if (inciso.getEstatus() == 0) {
				existenVigentes = true;
				
			}
			
			inciso.setValidOnMillis(inciso.getValidOn().getTime());
			inciso.setRecordFromMillis(inciso.getRecordFrom().getTime());
		}
		
		//siempre se buscaran tambien las cartas coberturas
		if ((!StringUtil.isEmpty(filtro.getAutoInciso().getNumeroSerie())) && 
				filtro.getAutoInciso().getNumeroSerie().length() >= 5 || filtro.getIdToSolicitud() != null) {			
			incisos.addAll(buscarSolicitudesPoliza(filtro.getAutoInciso().getNumeroSerie(), filtro.getIdToSolicitud()));		
		}
		
		
		} catch (ParseException e) {			
			e.printStackTrace();
		}
		return incisos;
	}


	@Override
	public List<ControlDinamicoRiesgoDTO> obtenerDatosRiesgo(Long incisoContinuityId, Long idToCotizacion, Date validon, Date knownOn, Date fechaReporteSiniestro) {
		
		return obtenerDatosRiesgo(incisoContinuityId, null, validon, knownOn, fechaReporteSiniestro, null);
	}
	
	@Override
	public List<ControlDinamicoRiesgoDTO> obtenerDatosRiesgo(Long incisoContinuityId, Long idToCotizacion, Date validon, 
			Date knownOn, Date fechaReporteSiniestro, Long coberturaId) {
		
		return getDatosRiesgo(incisoContinuityId, coberturaId, validon, knownOn, fechaReporteSiniestro);
	}

	private void loadInfoIncisoSiniestroDTO(IncisoSiniestroDTO incisoSiniestroDTO , BitemporalInciso bitemporalInciso , Date fechaReporteSiniestro){
		
		Long incisoContinuityId 					= bitemporalInciso.getContinuity().getId();	
		PolizaDTO poliza 							= null;
		List<PolizaDTO> listPoliza 					= null;		
		Integer numeroInciso 						= null;
		Date fechaRealInciso						= null;
		Date fechaEmisionInciso						= null;
		//fecha real del iniciso by joksrc
		fechaRealInciso = polizaSiniestroDao.obtenerFechaVigenciaInicioRealPorInciso(incisoContinuityId);
		fechaEmisionInciso = polizaSiniestroDao.obtenerFechaEmisionPorInciso(incisoContinuityId);
		
		incisoSiniestroDTO.setFechaRealEmisionPoliza(fechaEmisionInciso);
		
		incisoSiniestroDTO.setFechaVigenciaIniRealInciso(fechaRealInciso);
		incisoSiniestroDTO.setIncisoContinuityId( incisoContinuityId );
		incisoSiniestroDTO.setFechaIniVigencia( bitemporalInciso.getValidityInterval().getInterval().getStart().toDate() );
		incisoSiniestroDTO.setFechaFinVigencia( bitemporalInciso.getValidityInterval().getInterval().getEnd().toDate()  );
		incisoSiniestroDTO.setFechaEmision( bitemporalInciso.getValidityInterval().getInterval().getStart().toDate() );
		incisoSiniestroDTO.setValidOn( bitemporalInciso.getValidityInterval().getInterval().getStart().toDate() );
		incisoSiniestroDTO.setValidOnMillis( bitemporalInciso.getValidityInterval().getInterval().getStart().toDate().getTime());
		incisoSiniestroDTO.setRecordFrom( bitemporalInciso.getRecordInterval().getInterval().getStart().toDate() );
		incisoSiniestroDTO.setRecordFromMillis(  bitemporalInciso.getRecordInterval().getInterval().getStart().toDate().getTime());
		
		numeroInciso = bitemporalInciso.getContinuity().getNumero();
		
		incisoSiniestroDTO.setNumeroInciso( numeroInciso );
		incisoSiniestroDTO.setNumeroSecuencia(bitemporalInciso.getValue().getNumeroSecuencia());
		incisoSiniestroDTO.setMotivo("");
		
		validarVigenciaInciso(fechaReporteSiniestro, incisoSiniestroDTO, bitemporalInciso);
		
		listPoliza = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", new BigDecimal(bitemporalInciso.getContinuity().getCotizacionContinuity().getNumero()));
		
		if( listPoliza != null && listPoliza.size() >0){
			poliza = listPoliza.get(0);
			incisoSiniestroDTO.setNumeroPoliza(poliza.getNumeroPolizaFormateada());
			incisoSiniestroDTO.setIdToPoliza( poliza.getIdToPoliza() );
		}

		incisoSiniestroDTO.setIdToCotizacion(new BigDecimal(bitemporalInciso.getContinuity().getCotizacionContinuity().getNumero()));
		
		BitemporalCotizacion biCotizacion = (BitemporalCotizacion) getCurrentBitemporal(bitemporalInciso.getContinuity().getCotizacionContinuity(), 
				new DateTime(fechaReporteSiniestro), bitemporalInciso.getRecordInterval().getInterval().getStart()/*ofal*/, poliza.getCotizacionDTO().getIdToCotizacion());
		
		if (biCotizacion != null) {			
			incisoSiniestroDTO.setNombreContratante( biCotizacion.getValue().getNombreContratante());
			
		}		

		setAutoIncisoData(bitemporalInciso, incisoSiniestroDTO, fechaReporteSiniestro);
		
		//Motivo de cobranza y validacion de vigencia
		obtenerMotivoCobranza(incisoSiniestroDTO, poliza.getIdToPoliza().longValue(), poliza.getCotizacionDTO().getIdToCotizacion(),
				incisoSiniestroDTO.getIdToSeccion(), numeroInciso, fechaReporteSiniestro);		
	} 
	
	private void setAutoIncisoData(BitemporalInciso bitemporalInciso, IncisoSiniestroDTO incisoSiniestroDTO, Date fechaReporteSiniestro) {
		
		BitemporalAutoInciso bitemporalAutoInciso = (BitemporalAutoInciso) getCurrentBitemporal(bitemporalInciso.getContinuity().getAutoIncisoContinuity(), 
				new DateTime(fechaReporteSiniestro), bitemporalInciso.getRecordInterval().getInterval().getStart()/*ofal*/,
				new BigDecimal(bitemporalInciso.getContinuity().getCotizacionContinuity().getNumero()));
		
		if( bitemporalAutoInciso != null){
			
			if (bitemporalAutoInciso.getValue().getMarcaId() != null) {
				bitemporalAutoInciso.getValue().setDescMarca(entidadService.findById(MarcaVehiculoDTO.class, bitemporalAutoInciso.getValue().getMarcaId()).getDescription());	
			}			
			
			if (bitemporalAutoInciso.getValue().getEstiloId() != null) {
				EstiloVehiculoDTO estilo = entidadService.findById(EstiloVehiculoDTO.class, getEstiloVehiculoId(bitemporalAutoInciso.getValue().getEstiloId()));
				
				if (estilo != null) {
					bitemporalAutoInciso.getValue().setDescEstilo(estilo.getDescription());
				}				
			}
			
			if (bitemporalAutoInciso.getValue().getTipoUsoId() != null) {			
				
				TipoUsoVehiculoDTO tipoUso = entidadService.findById(TipoUsoVehiculoDTO.class, new BigDecimal(bitemporalAutoInciso.getValue().getTipoUsoId()));
				
				if (tipoUso != null) {
					bitemporalAutoInciso.getValue().setDescTipoUso(tipoUso.getDescription());
				}
			}
			incisoSiniestroDTO.setAutoInciso( bitemporalAutoInciso.getValue() );		
			
			//joksrc business line
			BigDecimal idSeccion = new BigDecimal(0);
			idSeccion = polizaSiniestroDao.getIdSeccionFromBitemporalTablesSeccion(bitemporalInciso.getContinuity().getId());
			SeccionDTO lineaNegocio = entidadService.findById(SeccionDTO.class, idSeccion);
			
			if (lineaNegocio != null){
				incisoSiniestroDTO.setNombreLinea(lineaNegocio.getNombreComercial());
				incisoSiniestroDTO.setIdToSeccion(lineaNegocio.getIdToSeccion());
			}
			
		}
	}
	
	@Deprecated
	//Get business line from MSECCIONINCISIOC table. Before, it was consulted from another table, this due that business rules was changed. By joksrc
	private BigDecimal getIdSeccionFromBitemporalSeccionInciso(Long idContinuidad, DateTime fechaSiniestroReporte, DateTime bitempIntervalStart){
		BigDecimal idSeccion = new BigDecimal(0);
		
		Map<String, Object> paramsSec = new HashMap<String, Object>(1);
		paramsSec.put("continuity.incisoContinuity.id", idContinuidad);		
		Collection<BitemporalSeccionInciso> collectionBitemporalSeccionInciso = (List<BitemporalSeccionInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalSeccionInciso.class, 
				paramsSec, 
				new DateTime(fechaSiniestroReporte), new DateTime(bitempIntervalStart), 
				false);
		
		if(!collectionBitemporalSeccionInciso.isEmpty()){
			Iterator<BitemporalSeccionInciso> bitSeccionIncisoIterator = collectionBitemporalSeccionInciso.iterator();
			if(bitSeccionIncisoIterator.hasNext()){
				BitemporalSeccionInciso bitSeccionInciso = bitSeccionIncisoIterator.next();
				bitSeccionInciso.getContinuity().getSeccion().getDescripcion();
				
				idSeccion=(bitSeccionInciso.getContinuity().getSeccion().getIdToSeccion());
			}
		}
		
		return idSeccion;
	}
	
	private void validarVigenciaInciso(Date fechaReporteSiniestro, IncisoSiniestroDTO incisoSiniestroDTO, BitemporalInciso bitemporalInciso) {
		Date validTo 		= bitemporalInciso.getValidityInterval().getInterval().getEnd().toDate();
		Date recordTo 		= bitemporalInciso.getRecordInterval().getInterval().getEnd().toDate() ; 
		String descripcionVigencia = null;
		IncisoSiniestroDTO.EstatusIncisoSiniestro estatus = IncisoSiniestroDTO.EstatusIncisoSiniestro.VIGENTE;
		
		if (fechaReporteSiniestro.compareTo(validTo) < 0 && recordTo.compareTo(fechaReporteSiniestro) > 0) {
			descripcionVigencia = ESTATUS_INCISO_VIGENTE;
			estatus = IncisoSiniestroDTO.EstatusIncisoSiniestro.VIGENTE;
		} else {
			List<ControlEndosoCot> endosos = this.cotizacionEndosoDao.obtenerEndosoInciso(
					bitemporalInciso.getContinuity().getCotizacionContinuity().getNumero().longValue(), 
					bitemporalInciso.getContinuity().getId()
					, null, null, null);
			if (endosos != null && !endosos.isEmpty()) {
				
				for (ControlEndosoCot endoso : endosos) {
					
					if (endoso.getRecordFrom().compareTo(bitemporalInciso.getRecordInterval().getInterval().getEnd().toDate()) == 0) {
						if (endoso.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO 
								|| endoso.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA
								|| endoso.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL
								|| endoso.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL) {
							
							descripcionVigencia = ESTATUS_INCISO_CANCELADO;
							estatus = IncisoSiniestroDTO.EstatusIncisoSiniestro.CANCELADO;
						} else if (endoso.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO) {
							ControlEndosoCot endosoCancelado = entidadService.findById(ControlEndosoCot.class, endoso.getIdCancela());
							if (endosoCancelado.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO) {
								descripcionVigencia = ESTATUS_INCISO_CANCELADO;
								estatus = IncisoSiniestroDTO.EstatusIncisoSiniestro.CANCELADO;
							} else {
								descripcionVigencia = ESTATUS_INCISO_VENCIDA;
								estatus = IncisoSiniestroDTO.EstatusIncisoSiniestro.NO_VIGENTE;
							}
							
						} else {
							descripcionVigencia = ESTATUS_INCISO_VENCIDA;
							estatus = IncisoSiniestroDTO.EstatusIncisoSiniestro.NO_VIGENTE;
						}
					} else {
						if (bitemporalInciso.getValidityInterval().getInterval().getEnd().toDate().compareTo(fechaReporteSiniestro) == -1
								&& bitemporalInciso.getRecordInterval().getInterval().getEnd().compareTo(new DateTime(TimeUtils.END_OF_TIME)) == 0) {
							if (endoso.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO 
									|| endoso.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA
									|| endoso.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL
									|| endoso.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL) {
								
								descripcionVigencia = ESTATUS_INCISO_CANCELADO;
								estatus = IncisoSiniestroDTO.EstatusIncisoSiniestro.CANCELADO;
							} else {
								descripcionVigencia = ESTATUS_INCISO_VENCIDA;
								estatus = IncisoSiniestroDTO.EstatusIncisoSiniestro.NO_VIGENTE;
							}
						}
					}
				}
				
				if (descripcionVigencia == null) {
					descripcionVigencia = ESTATUS_INCISO_VENCIDA;
					estatus = IncisoSiniestroDTO.EstatusIncisoSiniestro.NO_VIGENTE;
				}
				
			} else {
				descripcionVigencia = ESTATUS_INCISO_VENCIDA;
				estatus = IncisoSiniestroDTO.EstatusIncisoSiniestro.NO_VIGENTE;
			}
		}
		incisoSiniestroDTO.setDescEstatus(descripcionVigencia);
		incisoSiniestroDTO.setEstatus((short) estatus.ordinal());
	}

	@Override
	public List<IncisoSiniestroDTO> buscarSolicitudesPoliza(String numeroSerie, BigDecimal idSolicitud) {
		List<SolicitudDataEnTramiteDTO> listSolicitudes = polizaSiniestroDao.buscarSolicitudesPoliza(numeroSerie, idSolicitud);
		
		return  transformListSolicitudDataEnTramiteToIncisoSiniestro( listSolicitudes );
	}
	
	
	@Override
	public IncisoSiniestroDTO buscarSolicitudPolizaById(BigDecimal idToSolicitudDataEnTramite) {	
		
		return transformSolicitudDataEnTramiteToIncisoSiniestro(  solicitudDataEnTramiteService.findById(idToSolicitudDataEnTramite) );
	}
	
	

	@Override
	public DatosContratanteDTO obtenerDatosContratante(BigDecimal personaContratanteId) {
		
		DatosContratanteDTO datosContratanteDTO 	= null;
		ClienteGenericoDTO clienteGenericoDTO 		= new ClienteGenericoDTO();
		
		clienteGenericoDTO.setIdCliente(personaContratanteId);
		
		try {
			clienteGenericoDTO = clienteFacadeRemote.loadById(clienteGenericoDTO);
			datosContratanteDTO =  transformClienteGenericoToDatosContratante(clienteGenericoDTO);
			datosContratanteDTO.setIdPersonaContratante(personaContratanteId.longValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return datosContratanteDTO;
	}
	
	
	private DatosContratanteDTO transformClienteGenericoToDatosContratante( ClienteGenericoDTO clienteGenericoDTO ){
		
		DatosContratanteDTO datosContratanteDTO 	= new DatosContratanteDTO();
		String nombreContratante 					= clienteGenericoDTO.getNombreCompleto();
		String pais 								= ""; 
		String estado 								= "";
		String municipio 							= "";
		
		if( clienteGenericoDTO.getIdPaisString() !=null && !clienteGenericoDTO.getIdPaisString().equals("") ){
			pais = paisFacadeRemote.findById( clienteGenericoDTO.getIdPaisString() ).getDescription();
		}
		
		if( clienteGenericoDTO.getIdEstadoFiscal() !=null && !clienteGenericoDTO.getIdEstadoFiscal().equals("") ){
			estado = estadoFacadeRemote.findById( clienteGenericoDTO.getIdEstadoFiscal() ).getDescription();
		}
		
		if( clienteGenericoDTO.getIdMunicipioFiscal() !=null && !clienteGenericoDTO.getIdMunicipioFiscal().equals("") ){
			municipio = municipioFacadeRemote.findById( clienteGenericoDTO.getIdMunicipioFiscal()).getDescription();
		}
		
		datosContratanteDTO.setNombreContratante( nombreContratante );
		datosContratanteDTO.setRfcFiscal(clienteGenericoDTO.getCodigoRFCFiscal() );
		datosContratanteDTO.setPaisFiscal( pais );
		datosContratanteDTO.setEstadoFiscal( estado );
		datosContratanteDTO.setMunicipioFiscal( municipio );
		datosContratanteDTO.setColoniaFiscal( clienteGenericoDTO.getNombreColoniaFiscal() );
		datosContratanteDTO.setCalleNumeroFiscal( clienteGenericoDTO.getNombreCalleFiscal() );
		datosContratanteDTO.setCpFiscal( clienteGenericoDTO.getCodigoPostalFiscal() );
		
		return datosContratanteDTO;
	}
	
	
	private  DatosPersonalizacionDTO transformEstiloVehiculoToDatosPersonalizacion( EstiloVehiculoDTO estiloVehiculoDTO ){
		
		DatosPersonalizacionDTO datosPersonalizacionDTO 		= new DatosPersonalizacionDTO();
		String cilindros 										= "";
		List<VarModifDescripcion> listVarModifDescripcion 		= null;
		VarModifDescripcion filtroVarModifDescripcion 			= new VarModifDescripcion();
		
		if (estiloVehiculoDTO.getClaveCilindros() != null) {
			filtroVarModifDescripcion.setIdGrupo( GRUPO_CILINDROS );
			filtroVarModifDescripcion.setId( Long.parseLong(estiloVehiculoDTO.getClaveCilindros()) );
			//filtroVarModifDescripcion.set
			listVarModifDescripcion = varModifDescripcionService.findByFilters(filtroVarModifDescripcion);			
			
			if (listVarModifDescripcion != null && listVarModifDescripcion.size()>0) {
				cilindros = listVarModifDescripcion.get(0).getDescription();
			}
		}
		
		
		datosPersonalizacionDTO.setTransmision( estiloVehiculoDTO.getClaveTransmision() );
		datosPersonalizacionDTO.setTipoAlarma( estiloVehiculoDTO.getClaveAlarmaFabricante() );
		datosPersonalizacionDTO.setPuertas( estiloVehiculoDTO.getNumeroPuertas() );
		datosPersonalizacionDTO.setCilindros( cilindros );
		
		return datosPersonalizacionDTO;
	}
	
	
	private List<IncisoSiniestroDTO> transformListSolicitudDataEnTramiteToIncisoSiniestro( List<SolicitudDataEnTramiteDTO> listSolicitudes){
		
		List<IncisoSiniestroDTO> listInciso = new ArrayList<IncisoSiniestroDTO>();
		
		for(SolicitudDataEnTramiteDTO solicitudDataEnTramiteDTO :listSolicitudes ){
			IncisoSiniestroDTO inciso 	= new IncisoSiniestroDTO();
			AutoInciso autoInciso 		= new AutoInciso();
			
			inciso.setNumeroSerie( solicitudDataEnTramiteDTO.getSerie());
			autoInciso.setDescripcionFinal( solicitudDataEnTramiteDTO.getDescripcion());
			autoInciso.setObservacionesinciso( solicitudDataEnTramiteDTO.getObservaciones());
			
			inciso.setIdToSolicitudDataEnTramite( solicitudDataEnTramiteDTO.getIdToSolicitudDataEnTramite());
			inciso.setModelo( solicitudDataEnTramiteDTO.getModelo());
			inciso.setFechaIniVigencia( solicitudDataEnTramiteDTO.getFechaInicioVigencia());
			inciso.setFechaFinVigencia( solicitudDataEnTramiteDTO.getFechaFinVigencia());
			inciso.setIdToSolicitud( solicitudDataEnTramiteDTO.getIdToSolicitud());
			inciso.setAutoInciso(autoInciso);
			
			listInciso.add(inciso);
		}
		
		return listInciso;
	}
	
	private IncisoSiniestroDTO transformSolicitudDataEnTramiteToIncisoSiniestro( SolicitudDataEnTramiteDTO solicitudDataEnTramiteDTO){
		
		
		IncisoSiniestroDTO inciso 	= new IncisoSiniestroDTO();
		AutoInciso autoInciso 		= new AutoInciso();
		
		autoInciso.setNumeroSerie( solicitudDataEnTramiteDTO.getSerie());
		autoInciso.setDescripcionFinal( solicitudDataEnTramiteDTO.getDescripcion());
		autoInciso.setObservacionesinciso( solicitudDataEnTramiteDTO.getObservaciones());
		
		inciso.setIdToSolicitudDataEnTramite( solicitudDataEnTramiteDTO.getIdToSolicitudDataEnTramite());
		inciso.setModelo( solicitudDataEnTramiteDTO.getModelo());
		inciso.setFechaIniVigencia( solicitudDataEnTramiteDTO.getFechaInicioVigencia());
		inciso.setFechaFinVigencia( solicitudDataEnTramiteDTO.getFechaFinVigencia());
		inciso.setIdToSolicitud( solicitudDataEnTramiteDTO.getIdToSolicitud());
		inciso.setAutoInciso(autoInciso);
		inciso.setDescEstatus(CARTA_COBERTURA_ESTATUS);
		inciso.setMotivo(CARTA_COBERTURA_MOTIVO);		
		return inciso;
	}

	
	/**
	 * Obtiene las Condiciones Especiales asociadas a un Inciso de una Poliza
	 */
	@Override		// validOn, recordFrom
	public List<CondicionEspecialDTO> buscarCondicionesEspeciales ( Long incisoContinuityId, Long idReporteCabina, Date fechaReporteSiniestro,
			Date validOn, Date knownOn) {
		List<CondicionEspecialDTO> condicionesEspecialesList = new ArrayList<CondicionEspecialDTO>();
		CondicionEspecialDTO condicionEspecialDTO = null;
		
		List<CondicionEspecialReporte> condEspecialReporte = entidadService.findByProperty(CondicionEspecialReporte.class, 
																"reporteCabina.id", idReporteCabina);

		if (condEspecialReporte.size() > 0) {
			for(CondicionEspecialReporte condicionReporte:condEspecialReporte){
				condicionEspecialDTO = new CondicionEspecialDTO();
				if( condicionReporte.getCondicionEspecial().getNivelAplicacion() == 0 ){
					condicionReporte.getCondicionEspecial().setNivelAplicacionStr( "P\u00F3liza" );
				}else if( condicionReporte.getCondicionEspecial().getNivelAplicacion() == 1 ){
					condicionReporte.getCondicionEspecial().setNivelAplicacionStr( "Inciso" );
				}else{
					condicionReporte.getCondicionEspecial().setNivelAplicacionStr( "P\u00F3liza e Inciso" );
				}
				condicionEspecialDTO.setCondicion( condicionReporte.getCondicionEspecial() );
				condicionEspecialDTO.setClaveContrato(condicionReporte.getClaveContrato());
				condicionesEspecialesList.add( condicionEspecialDTO );
			}
		} else {
			
			try{

				BitemporalInciso biInciso = incisoViewService.getInciso( incisoContinuityId, validOn, knownOn );
				
				BigDecimal idtoCotizacion = new BigDecimal(biInciso.getContinuity().getCotizacionContinuity().getNumero());
				
				BitemporalCotizacion  biCotizacion = (BitemporalCotizacion) getCurrentBitemporal(biInciso.getContinuity().getCotizacionContinuity(), 
						new DateTime(fechaReporteSiniestro), biInciso.getRecordInterval().getInterval().getStart()/*ofal*/, idtoCotizacion);

				Collection<CondicionEspCotContinuity> condicionesCotContinuities = entidadContinuityService.findContinuitiesByParentKey(CondicionEspCotContinuity.class, 
						CondicionEspCotContinuity.PARENT_KEY_NAME, biCotizacion.getContinuity().getId());
				
				for (CondicionEspCotContinuity condicioCotContinuity: condicionesCotContinuities) {
					BitemporalCondicionEspCot bitemporalCondicionCot = (BitemporalCondicionEspCot) getCurrentBitemporal(condicioCotContinuity, 
							new DateTime(fechaReporteSiniestro), biCotizacion.getRecordInterval().getInterval().getStart()/*ofal*/, idtoCotizacion);
				
					if (bitemporalCondicionCot != null) {
						CondicionEspecial condicion = entidadService.findById(CondicionEspecial.class, bitemporalCondicionCot.getContinuity().getCondicionEspecialId());
						
						if (condicion != null) {
							condicionEspecialDTO = new CondicionEspecialDTO();
							condicionEspecialDTO.setClaveContrato(false);
							
							if( condicion.getNivelAplicacion() == 0 ){
								condicion.setNivelAplicacionStr( "Póliza" );
							}else if( condicion.getNivelAplicacion() == 1 ){
								condicion.setNivelAplicacionStr( "Inciso" );
							}else{
								condicion.setNivelAplicacionStr( "Póliza e Inciso" );
							}
							
							condicionEspecialDTO.setCondicion(condicion);						
							condicionesEspecialesList.add(condicionEspecialDTO);
						}						
					}
				}
				
				
				Collection<CondicionEspIncContinuity> condicionesIncContinuities = entidadContinuityService.findContinuitiesByParentKey(
						CondicionEspIncContinuity.class, CondicionEspIncContinuity.PARENT_KEY_NAME, biInciso.getContinuity().getId());
				
				for (CondicionEspIncContinuity condicioIncContinuity: condicionesIncContinuities) {
					BitemporalCondicionEspInc bitemporalCondicionInc = (BitemporalCondicionEspInc) getCurrentBitemporal(condicioIncContinuity, 
							new DateTime(fechaReporteSiniestro), biInciso.getRecordInterval().getInterval().getStart()/*ofal*/, idtoCotizacion);
				
					if (bitemporalCondicionInc != null) {
						CondicionEspecial condicion = entidadService.findById(CondicionEspecial.class, bitemporalCondicionInc.getContinuity().getCondicionEspecialId());
						
						if (condicion != null) {
							condicionEspecialDTO = new CondicionEspecialDTO();
							condicionEspecialDTO.setClaveContrato(false);
							
							if( condicion.getNivelAplicacion() == 0 ){
								condicion.setNivelAplicacionStr( "Póliza" );
							}else if( condicion.getNivelAplicacion() == 1 ){
								condicion.setNivelAplicacionStr( "Inciso" );
							}else{
								condicion.setNivelAplicacionStr( "Póliza e Inciso" );
							}
							
							condicionEspecialDTO.setCondicion(condicion);						
							condicionesEspecialesList.add(condicionEspecialDTO);
						}						
					}
				}
				
				
			
			}catch( Exception ex ){
				log.error(ex);
				ex.printStackTrace();
			}
		}
		return condicionesEspecialesList;
	}

	public CondicionEspecialReporte obtenerCondicionEspReporte ( Long idReporteCabina, Long idCondicionEspecial ){
		
		List<CondicionEspecialReporte> condicionesEspecialesReporte = new ArrayList<CondicionEspecialReporte>();
		List<CondicionEspecial> condicionEspecial = new ArrayList<CondicionEspecial>();
		Map<String,Object> params = new HashMap<String,Object>();
		params.put( "reporteCabina.id", idReporteCabina );
		params.put( "condicionEspecial.id", idCondicionEspecial );
		
		if( idReporteCabina != null && idCondicionEspecial != null ){
			condicionesEspecialesReporte =  entidadService.findByProperties( CondicionEspecialReporte.class, params);
		}else{
		return null;
		}
		
		if( condicionesEspecialesReporte != null && !condicionesEspecialesReporte.isEmpty() ){
			params = new HashMap<String,Object>();
			params.put( "id", idCondicionEspecial );
			
			condicionEspecial =  entidadService.findByProperties( CondicionEspecial.class, params);
			condicionesEspecialesReporte.get(0).setCondicionEspecial( condicionEspecial.get(0) );
			return condicionesEspecialesReporte.get(0);
		}else{
		return null;
		}
	}
	
	/**
	 * Consulta el numeroInciso que se encuentra asociado al reporte
	 */
	private Integer buscarIncisoReporteCabina(Long incisoContinuityId){

		Integer numeroInciso = null;
		
		try{
			IncisoContinuity incisoContinuity = entidadContinuityService.findContinuityByKey(IncisoContinuity.class, incisoContinuityId);
			
			numeroInciso = incisoContinuity.getNumero();
			
		}catch (Exception ex) {
			log.error(ex);
		}
		
		return numeroInciso;
	}
	
	/**
	 * Valida si el Inciso se encuentra asociado al reporte
	 */
	@Override
	public Integer incisoAsociadoReporteCabina( Long incisoContinuityId, Long idToPoliza, Long idReporte, Date fechaReporteSiniestro ){
		
		List<IncisoReporteCabina> incisosReporte 	= null;
		Integer incisosAsociadosReporte 			= 0;
		Integer numeroInciso 						= null;
		String numeroPoliza 						= null;
		Integer numeroIncisoAsignado				= null;
		String numeroPolizaAsignado 				= null;
		PolizaDTO poliza 							= null;
		ReporteCabina reporteCabina 				= null;
		Map<String,Object> params 					= null;
		IncisoReporteCabina incisoReporteCabina		= null;
		
		numeroInciso = this.buscarIncisoReporteCabina( incisoContinuityId);
		
		params = new HashMap<String,Object>();
		params.put( "numeroInciso", numeroInciso );
		params.put( "seccionReporteCabina.reporteCabina.id", idReporte );
		
		incisosReporte =  entidadService.findByProperties( IncisoReporteCabina.class, params);
		
		if( incisosReporte != null && !incisosReporte.isEmpty() ){
			poliza = entidadService.findById(PolizaDTO.class, BigDecimal.valueOf(idToPoliza));
			numeroPoliza = poliza.getNumeroPolizaFormateada();
			
			reporteCabina = entidadService.findById(ReporteCabina.class, idReporte);
			numeroPolizaAsignado = reporteCabina.getPoliza().getNumeroPolizaFormateada();
			
			incisoReporteCabina = obtenerIncisoAsignadoAlReporte(idReporte);
			if( incisoReporteCabina != null ){	
				numeroIncisoAsignado = incisoReporteCabina.getNumeroInciso();
			}
			
			if(numeroPoliza.equals(numeroPolizaAsignado)){
				if( numeroInciso.equals(numeroIncisoAsignado)){
					incisosAsociadosReporte = 1;
				}
			}
			
		}
		return incisosAsociadosReporte;
	}
	
	

	
	
	/**
	 * Asocia Condiciones especiales a un Reporte
	 */
	@Override
	public void asignarCondicionesEspeciales( Long idReporte, String condicionesEspeciales, TIPO_VALIDACION_CONDICION tipoValidacion){
	
		mensajesEvaluacion = new ArrayList<String>();
		ReporteCabina reporteCabina = entidadService.findById(ReporteCabina.class, idReporte);
		CondicionEspecialReporte condicionEspecialReporte = null;	
		
		CondicionEspecial condicionEspecial;
		if( condicionesEspeciales != null && !condicionesEspeciales.equals("") ){
			String delimiter = ",";
			String[] temp = condicionesEspeciales.split(delimiter);
			for( int i=0; i<temp.length; i++ ){
				Long idCondicionEspecial = new Long( Integer.parseInt(temp[i]) );
				condicionEspecial = entidadService.findById(CondicionEspecial.class, idCondicionEspecial);
				mensajesEvaluacion.addAll(this.validaCondicionEspecial(condicionEspecial, reporteCabina, tipoValidacion));
			}
		}
		if(mensajesEvaluacion.size() <= 0){
			desasignarCondicionesEspecialesReporte(idReporte);
		// Asocia Condiciones Especiales un Reporte Cabina
		 if( condicionesEspeciales != null && !condicionesEspeciales.equals("") ){
			String delimiter = ",";
			String[] temp = condicionesEspeciales.split(delimiter);
			for( int i=0; i<temp.length; i++ ){
				String idString = temp[i];
				Long idCondicionEspecial = new Long( Integer.parseInt(idString) );
					condicionEspecialReporte = obtenerCondicionEspReporte(idReporte, idCondicionEspecial);
					condicionEspecialReporte.setClaveContrato(true);
				
				condicionEspecialReporte = entidadService.save( condicionEspecialReporte );
			}
			reporteCabinaService.guardarEvento(idReporte, TipoEvento.EVENTO_15, "Se asignaron condiciones especiales al reporte");
		 }
	}
	}

	/**
	 * Borra las Condiciones Especiales asociadas a un Reporte Cabina
	 * @param idReporte
	 */
	private void eliminaCondicionesEspecialesReporte( Long idReporte ){
		List<CondicionEspecialReporte> condicionesEspecialesReporteList = new ArrayList<CondicionEspecialReporte>();
		Map<String,Object> params = null;
		
		if( idReporte != null ){
			params = new HashMap<String,Object>();
			params.put( "reporteCabina.id", idReporte );
			
			condicionesEspecialesReporteList = entidadService.findByProperties(CondicionEspecialReporte.class, params );
			if( !condicionesEspecialesReporteList.isEmpty() ){
				entidadService.removeAll( condicionesEspecialesReporteList );
			}
		}
	}

	/**
	 * Llena la entidad con la informacion correspondiente
	 * @param idReporte
	 * @param idCondicionEspecial
	 * @return
	 */
	private CondicionEspecialReporte llenarCondicionEspecial( Long idReporte, Long idCondicionEspecial ){

		CondicionEspecialReporte condicionEspecialReporte = new CondicionEspecialReporte();
		CondicionEspecial condicionEspecial = new CondicionEspecial();
		ReporteCabina reporteCabina = new ReporteCabina();
		
		String codigoUsuario = this.findCurrentUser(null);
		
		reporteCabina.setId( idReporte );
		condicionEspecial =  entidadService.findById( CondicionEspecial.class, idCondicionEspecial );
		condicionEspecialReporte.setReporteCabina( reporteCabina );
		condicionEspecialReporte.setCondicionEspecial( condicionEspecial );
		condicionEspecialReporte.setCodigoUsuarioCreacion(codigoUsuario);
		condicionEspecialReporte.setFechaCreacion(Calendar.getInstance().getTime());	
		condicionEspecialReporte.setFechaModificacion(Calendar.getInstance().getTime());
		condicionEspecialReporte.setClaveContrato(false);

		return condicionEspecialReporte;

	}

	/**
	 * Obtiene el nombreUsuario del usuario logueado
	 * @param String usuario - si la petición viene de un WS este tendrá valor en caso contrario sera nulo
	 * @return
	 */
    private String findCurrentUser(String usuario){
    	
    	if( !StringUtil.isEmpty(usuario) ){
    		return usuario;
    	}else{
    		return String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario());
    	}
    	
    }
    
    /**
     * Consulta si el inciso se encuentra vigente o con Solicitud de Autorización con estatus Autorizada
     * @param incisoContinuityId
     * @param idToPoliza
     * @param fechaReporteSiniestro
     * @return
     */
    @Override
    public int incisoVigenteAutorizado( Long incisoContinuityId, Long idToPoliza, Date validOn, Date knownOn, Date fechaReporteSiniestro,
    		Short estatusInciso, Long idToReporte){

		int incisoVigenteAutorizado = 0;
		int incisoAutorizado = 0;
		
		//trae un inciso si se encuentra vigente
		BitemporalInciso biInciso = incisoViewService.getInciso( incisoContinuityId, validOn, knownOn);	
		incisoAutorizado = this.incisoSolicitudAutorizada(incisoContinuityId, idToPoliza, idToReporte);
		
		if( (biInciso != null && !biInciso.getValidityInterval().getInterval().getEnd().toDate().before(fechaReporteSiniestro) 
				&& estatusInciso != (short) IncisoSiniestroDTO.EstatusIncisoSiniestro.CANCELADO.ordinal()
				&& estatusInciso != (short) IncisoSiniestroDTO.EstatusIncisoSiniestro.NO_VIGENTE.ordinal()) 
				|| incisoAutorizado == 1 ){
			incisoVigenteAutorizado = 1;
		}
		
		return incisoVigenteAutorizado;
    }
    
    /**
     * Consulta si el inciso se encuentra con Solicitud de Autorización con estatus Autorizada
     * @param incisoContinuityId
     * @param idToPoliza
     * @param fechaReporteSiniestro
     * @return
     */
    private int incisoSolicitudAutorizada( Long incisoContinuityId, Long idToPoliza, Long reporteId){
		Integer numeroInciso = this.buscarIncisoReporteCabina( incisoContinuityId);
		
		return estatusIncisoAutorizacion(idToPoliza, reporteId, numeroInciso, CatSolicitudAutorizacionServiceImpl.ESTATUS_SOLICITUD_AUTORIZADO);
		

    }
    
    @Override 
    public Short estatusIncisoAutorizacion(Long idToPoliza, Long reporteId, Integer numeroInciso, Integer estatus) {
    	
    	List<SolicitudAutorizacionVigencia> listIncisos = null;
    	Short incisoVigente = -1;

		Map<String,Object> params = new HashMap<String,Object>();
		params.put( "poliza.idToPoliza", idToPoliza );
		params.put( "reporte.id", reporteId);
		params.put( "numeroInciso", numeroInciso );
		
		if (estatus != null) {
			params.put( "estatus", estatus);
		}
		
		
		listIncisos =  entidadService.findByPropertiesWithOrder(SolicitudAutorizacionVigencia.class, params, "id desc");
		
		if (CollectionUtils.isNotEmpty(listIncisos)) {
			incisoVigente = listIncisos.get(0).getEstatus().shortValue();
		}
		
		return incisoVigente;
    }
    
    
    @Override
    public String asignarIncisoReporte(Long reporteCabinaId, Long incisoContinuityId, Date fechaHoraOcurrido, String usuario) {
    	
    	String mensaje = null;
    	BitemporalInciso biInciso = incisoViewService.getInciso(incisoContinuityId, fechaHoraOcurrido, fechaHoraOcurrido );
    	
    	if (biInciso != null) {    		
    		
    		Map<String,Object> params = new HashMap<String, Object>();
    		params.put("cotizacionDTO.idToCotizacion", biInciso.getContinuity().getParentContinuity().getNumero());
    		
    		List<PolizaDTO> polizas = entidadService.findByProperties(PolizaDTO.class, params);
    		PolizaDTO poliza = polizas.get(0);
    		
    		BigDecimal idToSeccion = this.obtenerIdToSeccion(biInciso, fechaHoraOcurrido);
    		ReporteCabina reporteCabina = this.llenarReporteCabina( reporteCabinaId, poliza.getIdToPoliza().longValue(), usuario, null );
			entidadService.save( reporteCabina );
			SeccionReporteCabina seccionReporteCabina = this.llenarSeccionReporteCabina( reporteCabina, reporteCabinaId, idToSeccion, usuario );
			entidadService.save( seccionReporteCabina );	
			IncisoReporteCabina incisoReporteCabina = this.llenarIncisoReporteCabina( biInciso, fechaHoraOcurrido, seccionReporteCabina, reporteCabinaId, poliza.getIdToPoliza().longValue(), usuario );
		    entidadService.save( incisoReporteCabina );
		    AutoIncisoReporteCabina autoIncisoReporteCabina = this.llenarAutoIncisoReporteCabina( biInciso, fechaHoraOcurrido, incisoReporteCabina, reporteCabinaId, usuario );
		    entidadService.save( autoIncisoReporteCabina );
		    entidadService.save( reporteCabina );

		    List<BitemporalCoberturaSeccion> listBiCoberturaSeccion = this.obtenerCoberturasInciso(incisoContinuityId, new DateTime(fechaHoraOcurrido), new DateTime(fechaHoraOcurrido), fechaHoraOcurrido);
			
			if( listBiCoberturaSeccion != null && !listBiCoberturaSeccion.isEmpty() ){			
				this.copiarCoberturas( listBiCoberturaSeccion, incisoReporteCabina );
			}
    	} else {
    		mensaje = "El inciso no se encuentra vigente";
    	}
    	
    	return mensaje;
    }
    
    /**
	 * Asigna un Inciso de una Poliza a un Reporte Cabina
	 */
	@Override
	public void asignarInciso(Long idReporte, Long idToPoliza, Long incisoContinuityId, Date validOn, Date knownOn, Date fechaReporteSiniestro) throws Exception{
		List<BitemporalCoberturaSeccion> listBiCoberturaSeccion = new ArrayList<BitemporalCoberturaSeccion>();
		ReporteCabina reporteCabina = null;
		SeccionReporteCabina seccionReporteCabina = null;
		IncisoReporteCabina incisoReporteCabina = null;
		BitemporalInciso biInciso = null;
		AutoIncisoReporteCabina autoIncisoReporteCabina = null;
		List<CoberturaReporteCabina> listCoberturaReporteCabina = null;
		BigDecimal idToSeccion = null;
		Boolean tieneVip = false;
		
		try{
			biInciso = incisoViewService.getInciso(incisoContinuityId, validOn, knownOn );
			//idToSeccion = this.obtenerIdToSeccion(biInciso, fechaReporteSiniestro);
			//joksrc business line
			idToSeccion = polizaSiniestroDao.getIdSeccionFromBitemporalTablesSeccion(incisoContinuityId);
			
			reporteCabina = this.llenarReporteCabina( idReporte, idToPoliza, null, knownOn );
			entidadService.save( reporteCabina );
			
			this.eliminaEstructuraDeReporte(reporteCabina.getId());
			
			seccionReporteCabina = this.llenarSeccionReporteCabina( reporteCabina, idReporte, idToSeccion, null );
			entidadService.save( seccionReporteCabina );	
		    incisoReporteCabina = this.llenarIncisoReporteCabina( biInciso, fechaReporteSiniestro, seccionReporteCabina, idReporte, idToPoliza, null );
		    entidadService.save( incisoReporteCabina );
		    autoIncisoReporteCabina = this.llenarAutoIncisoReporteCabina( biInciso, fechaReporteSiniestro, incisoReporteCabina, idReporte, null );
		    entidadService.save( autoIncisoReporteCabina );
		    entidadService.save( reporteCabina );
			
		    listCoberturaReporteCabina = entidadService.findByProperty( CoberturaReporteCabina.class, "incisoReporteCabina.id", incisoReporteCabina.getId() );
			
		    if (listCoberturaReporteCabina != null && !listCoberturaReporteCabina.isEmpty()) {
		    	entidadService.removeAll( listCoberturaReporteCabina );
		    }
			listBiCoberturaSeccion = this.obtenerCoberturasInciso( biInciso.getContinuity().getId(), new DateTime(validOn), new DateTime(knownOn), fechaReporteSiniestro);
			
			if( listBiCoberturaSeccion != null && !listBiCoberturaSeccion.isEmpty() ){			
				this.copiarCoberturas( listBiCoberturaSeccion, incisoReporteCabina );
			}
			eliminaCondicionesEspecialesReporte(idReporte);
			CondicionEspecialReporte condicionEspecialReporte;
			List<CondicionEspecialDTO> condicionesEspecialesList = buscarCondicionesEspeciales(biInciso.getContinuity().getId(),
					idReporte, fechaReporteSiniestro, validOn, knownOn);
			for(CondicionEspecialDTO  condicionEspecialReporteDTO:condicionesEspecialesList){
				
				// # IDENTIFICA UNA CONDICION ESPECIAL VIP
				if( !tieneVip ){
					if( condicionEspecialReporteDTO.getCondicion().getVip() !=  null ){
						if( condicionEspecialReporteDTO.getCondicion().getVip() )
							tieneVip = true;
					}
				}
				
				condicionEspecialReporte = new CondicionEspecialReporte();
				condicionEspecialReporte = this.llenarCondicionEspecial( idReporte, condicionEspecialReporteDTO.getCondicion().getId() );
				condicionEspecialReporte = entidadService.save( condicionEspecialReporte );
			}
					
			reporteCabinaService.guardarEvento(idReporte, TipoEvento.EVENTO_13, "Se Asigno el Inciso " + incisoReporteCabina.getNumeroInciso() + " de la Poliza " + reporteCabina.getPoliza().getNumeroPolizaFormateada());
						
			try{						
				notificaAsignacionPoliza(reporteCabina, tieneVip);
			}catch(Exception ex){
				log.error(ex);
			}
		}catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex);
			throw ex;
		}
	}
	
	private void eliminaEstructuraDeReporte(Long idReporte){
		
		Map<String,Object> params = new HashMap<String,Object>();
		params.put( "reporteCabina.id", idReporte );
		List<SeccionReporteCabina> listSeccionReporteCabina = entidadService.findByProperties( SeccionReporteCabina.class, params );
		if( listSeccionReporteCabina != null && !listSeccionReporteCabina.isEmpty() ){
			List<CoberturaReporteCabina> coberturas = listSeccionReporteCabina.get(0).getIncisoReporteCabina().getCoberturaReporteCabina();
			this.entidadService.removeAll(coberturas);
			
			params = new HashMap<String,Object>();
			params.put("id", listSeccionReporteCabina.get(0).getIncisoReporteCabina().getId());
			IncisoReporteCabina inciso = this.entidadService.findByProperties(IncisoReporteCabina.class, params).get(0);
			this.entidadService.remove(inciso);
			
			params = new HashMap<String,Object>();
			params.put("id", listSeccionReporteCabina.get(0).getId());
			SeccionReporteCabina seccion = this.entidadService.findByProperties(SeccionReporteCabina.class, params).get(0);
			this.entidadService.remove(seccion);
			
		}
		
	}
	
	
	
	/***
	 * Notifica si existe una condicion especial
	 */
	private void notificaAsignacionPoliza(ReporteCabina reporte, Boolean tieneVip){
		
		try{			
			Map<String, Serializable> dataArg = new HashMap<String, Serializable>();
			dataArg.put("dia", LocalDateTime.now().dayOfWeek().getAsText(new Locale("es")).toUpperCase().
					concat(" ").concat(LocalDateTime.now().dayOfMonth().getAsString()).concat(" de "));
			dataArg.put("mes", LocalDateTime.now().monthOfYear().getAsText(new Locale("es")).toUpperCase());
			dataArg.put("anio", LocalDateTime.now().year().getAsText());
			dataArg.put("numeroReporte", reporte.getNumeroReporte());
			dataArg.put("numeroPoliza", reporte.getPoliza().getNumeroPolizaFormateada());
			dataArg.put("fechaReporte", (new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(reporte.getFechaHoraReporte()));
			dataArg.put("inciso", reporte.getSeccionReporteCabina().getIncisoReporteCabina().getNumeroInciso());			
			dataArg.put("reporta", reporte.getPersonaReporta());
			dataArg.put("telefonoContacto", reporte.getLadaTelefono().concat(reporte.getTelefono()));					
			dataArg.put("conductor", reporte.getPersonaConductor());			
			dataArg.put("vehiculo", reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getDescripcionFinal());
			dataArg.put("modelo", reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getModeloVehiculo());
			dataArg.put("serie", reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getNumeroSerie());					
			dataArg.put("oficinaId", reporte.getOficina().getId());		
			
			DatosContratanteDTO contratante = obtenerDatosContratante(BigDecimal.valueOf(reporte.getPersonaContratanteId()));			
			dataArg.put("asegurado", contratante.getNombreContratante());
			
			if(reporte.getAjustador() != null){
				dataArg.put("ajustador", reporte.getAjustador().getNombrePersonaAjustador());
			}else{
				dataArg.put("ajustador", "Sin ajustador asignado");
			}
			
			LugarSiniestroMidas ocurrido = reporte.getLugarOcurrido();
			if(ocurrido != null){
				dataArg.put("ubicacionSiniestro", ocurrido.toString());
			}else{
				dataArg.put("ubicacionSiniestro", "Aun no ha sido definida");
			}
			
			dataArg = envioNotificacionesService.cargarParametrosGeneralesSiniestros(reporte, dataArg);
			
			if(tieneVip){
				envioNotificacionesService.enviarNotificacion(EnvioNotificacionesService.EnumCodigo.CONDICIONES_ESPECIALES_VIP.toString(),  dataArg);
			}else{
				envioNotificacionesService.enviarNotificacion(EnvioNotificacionesService.EnumCodigo.ASIGNAR_POLIZA_A_REPORTE_DE_SINIESTRO.toString(),  dataArg);
			}
			
		}catch(Exception e){			
			log.error("Asignar Poliza a reporte: Error al notificar condicion VIP ", e);
		}
		
	}
    
	/**
	 * Asigna la Póliza al ReporteCabina
	 * @param idReporte
	 * @param idToPoliza
	 * @return
	 */
	private ReporteCabina llenarReporteCabina( Long idReporte, Long idToPoliza, String usuario,Date knownOn){
		ReporteCabina reporteCabina = null;
		PolizaDTO poliza = null;
		reporteCabina = entidadService.findById(ReporteCabina.class, idReporte);
		int modificaRegistro = 1;
		
		if( reporteCabina != null ){
			
			poliza = entidadService.findById( PolizaDTO.class, new BigDecimal(idToPoliza) );
			reporteCabina.setPoliza( poliza );
			
			if( reporteCabina.getPoliza() != null ){
				if( reporteCabina.getPoliza().getIdToPoliza() != null ){
					reporteCabina.setIdToSolicitud( null );
				}			
				
				reporteCabina.setClaveNegocio(poliza.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getClaveNegocio());
				reporteCabina.setTipoCotizacion(poliza.getCotizacionDTO().getTipoCotizacion());
			
			}		
			
			CotizacionContinuity cotContinuity = entidadContinuityService.findContinuityByBusinessKey(CotizacionContinuity.class,  
								CotizacionContinuity.BUSINESS_KEY_NAME, poliza.getCotizacionDTO().getIdToCotizacion());
			
			BitemporalCotizacion biCotizacion = (BitemporalCotizacion) getCurrentBitemporal(cotContinuity, 
					new DateTime(reporteCabina.getFechaHoraOcurrido()), (knownOn != null ? new DateTime(knownOn) : new DateTime(reporteCabina.getFechaHoraOcurrido()))/*ofal*/, new BigDecimal(cotContinuity.getNumero()));		
			
			if (biCotizacion != null) {
				reporteCabina.setAgenteId(biCotizacion.getValue().getSolicitud().getCodigoAgente());
				MonedaDTO moneda = biCotizacion.getValue().getMoneda();
				if(moneda== null){
						moneda = this.entidadService.findById(MonedaDTO.class, Short.valueOf((short)MonedaDTO.MONEDA_PESOS));
				}
				reporteCabina.setMoneda(biCotizacion.getValue().getMoneda());
				reporteCabina.setNombreAgente(biCotizacion.getValue().getSolicitud().getNombreAgente());
				reporteCabina.setPersonaContratanteId(biCotizacion.getValue().getPersonaContratanteId());					
			}		
		}
		
		reporteCabina = (ReporteCabina) this.llenarDatosControlEntidad( reporteCabina, modificaRegistro, usuario );
		
		return reporteCabina;
	}
	
	/**
	 * Asigna ReporteCabina y Seccion a SeccionReporteCabina
	 * @param reporteCabina
	 * @param idReporte
	 * @param idToSeccion
	 * @return
	 */
	private SeccionReporteCabina llenarSeccionReporteCabina( ReporteCabina reporteCabina, Long idReporte, BigDecimal idToSeccion, String usuario ){
		List<SeccionReporteCabina> listSeccionReporteCabina = null;
		SeccionReporteCabina seccionReporteCabina = null;
		SeccionDTO seccion = null;
		Map<String,Object> params = null;
		int modificaRegistro = 0;
		
		params = new HashMap<String,Object>();
		params.put( "reporteCabina.id", idReporte );
		listSeccionReporteCabina = entidadService.findByProperties( SeccionReporteCabina.class, params );
		if( listSeccionReporteCabina != null && !listSeccionReporteCabina.isEmpty() ){
			seccionReporteCabina = listSeccionReporteCabina.get(0);
		} else {
			seccionReporteCabina = new SeccionReporteCabina();
		}
		
		seccionReporteCabina.setReporteCabina( reporteCabina );
		seccion = entidadService.findById(SeccionDTO.class, idToSeccion);
		
		if( seccion.getIdToSeccion().intValue() != idToSeccion.intValue() ){
			modificaRegistro = 1;
			seccion.setIdToSeccion( idToSeccion );
		}
		
		seccionReporteCabina.setSeccionDTO( seccion );
		seccionReporteCabina = (SeccionReporteCabina) this.llenarDatosControlEntidad( seccionReporteCabina, modificaRegistro, usuario );
		reporteCabina.setSeccionReporteCabina(seccionReporteCabina);
		return seccionReporteCabina;
	} 
	 
	/**
	 * Asigna seccionReporteCabina, numeroInciso y nombreAsegurado a IncisoReporteCabina
	 * @param biInciso
	 * @param validOn
	 * @param incisoSiniestro
	 * @param idReporte
	 * @return
	 */
	private IncisoReporteCabina llenarIncisoReporteCabina( BitemporalInciso biInciso, Date fechaReporteSiniestro, 
			SeccionReporteCabina seccionReporteCabina, Long idReporte, Long idToPoliza, String usuario){
		IncisoReporteCabina incisoReporteCabina = null;
		BitemporalCotizacion biCotizacion = null;
		int modificaRegistro = 0;
		String nombreAsegurado = "";
		
		incisoReporteCabina = obtenerIncisoAsignadoAlReporte(idReporte);	
			
		if (incisoReporteCabina == null) {
			incisoReporteCabina = new IncisoReporteCabina();
		}
		
		incisoReporteCabina.setSeccionReporteCabina( seccionReporteCabina );
		
		// Se obtiene nombre asegurado
		 biCotizacion = (BitemporalCotizacion) getCurrentBitemporal(biInciso.getContinuity().getCotizacionContinuity(), 
				 new DateTime(fechaReporteSiniestro), biInciso.getRecordInterval().getInterval().getStart()/*ofal*/, new BigDecimal(biInciso.getContinuity().getCotizacionContinuity().getNumero()));		
		if (biCotizacion != null) {
			nombreAsegurado = biCotizacion.getValue().getNombreContratante();
		}
		
		incisoReporteCabina.setNombreAsegurado( nombreAsegurado );
		
		// Se eliminan las condiciones especiales que se hallan asociado al reporte de cabina.
		if( incisoReporteCabina.getNumeroInciso() != null && incisoReporteCabina.getNumeroInciso().intValue() != biInciso.getContinuity().getNumero() ){
			this.eliminaCondicionesEspecialesReporte( idReporte );
			modificaRegistro = 1;
		}

		incisoReporteCabina.setNumeroInciso(biInciso.getContinuity().getNumero());
		incisoReporteCabina.setValidFrom(biInciso.getValidityInterval().getInterval().getStart().toDate());
		incisoReporteCabina.setRecordFrom(biInciso.getRecordInterval().getInterval().getStart().toDate());
		incisoReporteCabina.setValidTo(biInciso.getValidityInterval().getInterval().getEnd().toDate());
		incisoReporteCabina.setRecordTo(biInciso.getRecordInterval().getInterval().getEnd().toDate());		
		
		asignarEstatusInciso(incisoReporteCabina, biInciso, fechaReporteSiniestro, idToPoliza);
		
		incisoReporteCabina = (IncisoReporteCabina) this.llenarDatosControlEntidad( incisoReporteCabina, modificaRegistro, usuario );
		seccionReporteCabina.setIncisoReporteCabina(incisoReporteCabina);
		return incisoReporteCabina;
	} 
	
	private void asignarEstatusInciso(IncisoReporteCabina incisoReporte, BitemporalInciso biInciso, 
			Date fechaReporteSiniestro, Long idToPoliza) {
		IncisoSiniestroDTO incisodto = new IncisoSiniestroDTO();
		validarVigenciaInciso(fechaReporteSiniestro, incisodto, biInciso);
		
		//BigDecimal idToSeccion = this.obtenerIdToSeccion(biInciso, fechaReporteSiniestro);
		//joksrc business line 
		BigDecimal idToSeccion = polizaSiniestroDao.getIdSeccionFromBitemporalTablesSeccion(biInciso.getContinuity().getId());
		obtenerMotivoCobranza(incisodto, idToPoliza, new BigDecimal(biInciso.getContinuity().getCotizacionContinuity().getNumero()),
				idToSeccion, biInciso.getContinuity().getNumero(), fechaReporteSiniestro);
				
		String estatus = null;
		
		switch(incisodto.getEstatus()) {
			case 0: estatus = "VIGENTE";
				    break;
				
			case 1: estatus = "NOVIGENTE";
					break;
				
			case 2: estatus = "CANCELADO";
					break;
			
			default: estatus = null;
		}
		
		incisoReporte.setEstatusInciso(estatus);	
		
		if (incisodto.getEstatus() == 0) {
			incisoReporte.setSituacionCobranza(incisodto.getMotivo());
		} else {
			incisoReporte.setSituacionCobranza(null);
		}
	}
	
	/**
	 * Asigna IncisoReporteCabina y numero de serie a AutoIncisoReporteCabina
	 * @param biInciso
	 * @param validOn
	 * @param incisoReporteCabina
	 * @param idReporte
	 * @return
	 */
	private AutoIncisoReporteCabina llenarAutoIncisoReporteCabina( BitemporalInciso biInciso, Date fechaReporteSiniestro, IncisoReporteCabina incisoReporteCabina, Long idReporte, String usuario ){
		BitemporalAutoInciso biAutoInciso = null;
		List<AutoIncisoReporteCabina> listAutoIncisoReporteCabina = null;
		AutoIncisoReporteCabina autoIncisoReporteCabina = new AutoIncisoReporteCabina();
		Map<String,Object> params = null;
		int modificaRegistro = 0;
		
		params = new HashMap<String,Object>();
		params.put( "incisoReporteCabina.seccionReporteCabina.reporteCabina.id", idReporte );
		listAutoIncisoReporteCabina = entidadService.findByProperties( AutoIncisoReporteCabina.class, params );
		
		if( listAutoIncisoReporteCabina != null && !listAutoIncisoReporteCabina.isEmpty() ){
			autoIncisoReporteCabina = listAutoIncisoReporteCabina.get(0);
		}
		
		autoIncisoReporteCabina.setIncisoReporteCabina( incisoReporteCabina );
		
		biAutoInciso = (BitemporalAutoInciso) getCurrentBitemporal(biInciso.getContinuity().getAutoIncisoContinuity(), 
				new DateTime(fechaReporteSiniestro), biInciso.getRecordInterval().getInterval().getStart()/*ofal*/,
				new BigDecimal(biInciso.getContinuity().getCotizacionContinuity().getNumero()));	
		
		autoIncisoReporteCabina.setNumeroMotor(biAutoInciso.getValue().getNumeroMotor());
		autoIncisoReporteCabina.setNumeroSerie( biAutoInciso.getValue().getNumeroSerie() );
		autoIncisoReporteCabina.setEstiloId( biAutoInciso.getValue().getEstiloId());
		autoIncisoReporteCabina.setMarcaId( biAutoInciso.getValue().getMarcaId());
		autoIncisoReporteCabina.setModeloVehiculo( biAutoInciso.getValue().getModeloVehiculo());
		autoIncisoReporteCabina.setDescripcionFinal( biAutoInciso.getValue().getDescripcionFinal());
		autoIncisoReporteCabina.setEstadoId( biAutoInciso.getValue().getEstadoId());
		autoIncisoReporteCabina.setTipoUsoId(biAutoInciso.getValue().getTipoUsoId());
		autoIncisoReporteCabina.setNombreCompletoConductor(biAutoInciso.getValue().getNombreCompletoConductor());
		
		
		if (biAutoInciso.getValue().getPersonaAseguradoId() != null) {
			autoIncisoReporteCabina.setPersonaAseguradoId(biAutoInciso.getValue().getPersonaAseguradoId());
			
			/*ClienteGenericoDTO filtro = new ClienteGenericoDTO();
			filtro.setIdCliente(new BigDecimal(biAutoInciso.getValue().getPersonaAseguradoId()));
			
			try {
				filtro = clienteFacadeRemote.loadById( filtro );
				
				autoIncisoReporteCabina.setRfc(filtro.getCodigoRFC());
				autoIncisoReporteCabina.setCurp(filtro.getCodigoCURP());
			} catch (Exception e) {
			}*/
			
		}
		
		autoIncisoReporteCabina = (AutoIncisoReporteCabina) this.llenarDatosControlEntidad( autoIncisoReporteCabina, modificaRegistro, usuario );
		incisoReporteCabina.setAutoIncisoReporteCabina(autoIncisoReporteCabina);
		return autoIncisoReporteCabina;
	}
	
	/**
	 * Llena los datos de control para cada entidad que hereda de MidasAbstracto
	 * @param entidad
	 * @param modificaRegistro
	 * @param usuario - en caso de venir por WS este campo viene con datos
	 * @return
	 */
	private MidasAbstracto llenarDatosControlEntidad(MidasAbstracto entidad, int modificaRegistro, String usuario){
		
		if( modificaRegistro == 1 ){
			entidad.setCodigoUsuarioModificacion( this.findCurrentUser(usuario) );
			entidad.setFechaModificacion( new Date() );
		}else{
			entidad.setCodigoUsuarioCreacion( this.findCurrentUser(usuario) );
			entidad.setFechaCreacion( new Date() );
			//entidad.setFechaModificacion( new Date() );
		}
		
		return entidad;
	}
	
	/**
	 * Obtiene los datos del cliente
	 * @param idCliente
	 * @return
	 */
	@Override
	public ClienteGenericoDTO buscarDatosCliente( BigDecimal idCliente ){
		ClienteGenericoDTO filtro = new ClienteGenericoDTO();
		EstadoDTO estado = new EstadoDTO();
		CatalogoGiroDTO giro = new CatalogoGiroDTO();
		filtro.setIdCliente( idCliente );
		
		try {
			filtro = clienteFacadeRemote.loadById( filtro );
			
			if( filtro.getClaveTipoPersona() == 1 ){
				filtro.setClaveTipoPersonaString( "PF" );
			}
			if( filtro.getClaveTipoPersona() == 2 ){
				filtro.setClaveTipoPersonaString( "PM" );
			}
			
			if ( filtro.getClaveEstadoNacimiento() != null && !filtro.getClaveEstadoNacimiento().equals("") ){
				estado = estadoFacadeRemote.findById( filtro.getClaveEstadoNacimiento() );
				filtro.setEstadoNacimiento( estado.getStateName() );
			}
			
			if ( filtro.getIdGiro() != null && !filtro.getIdGiro().equals("") ){
					giro = giroFacadeRemote.findById( new Integer(filtro.getIdGiro()) );
					filtro.setIdGiro( giro.getNombreGiro() );
			}
		} catch (Exception ex) {
			log.error(ex);
		}
		return filtro;
	}	  	   

	@Override
	public List<ProgramaPagoDTO> obtenerProgramasPago(Long idToPoliza, Integer numeroInciso, Date fechaReporteSiniestro){
		List<ProgramaPagoDTO> programasPagoList = polizaSiniestroDao.obtenerProgramasPago(idToPoliza, numeroInciso, fechaReporteSiniestro);
		return programasPagoList;
	}
	
	@Override
	public List<ReciboProgramaPagoDTO> obtenerRecibosPrograma(Long idCotizacionSeycos, Long numeroPrograma, Date fechaReporteSiniestro){
		List<ReciboProgramaPagoDTO> recibosProgramaPagoList = polizaSiniestroDao.obtenerRecibosPrograma(idCotizacionSeycos, numeroPrograma, fechaReporteSiniestro);
		return recibosProgramaPagoList;
	}
	
	/**
	 * Muestra las direcciones del cliente ya sea personal o moral
	 */
	@Override
	public List<DireccionCliente>  buscarDireccionesCliente( BigDecimal idCliente ){
		ClienteGenericoDTO filtro = new ClienteGenericoDTO();
		List<DireccionCliente> direcciones = new ArrayList<DireccionCliente>();
		DireccionCliente direccionPersonal = null;
		DireccionCliente direccionFiscal = null;;
		
		filtro = this.buscarDatosCliente( idCliente );
		
		if( filtro != null ){
			
			direccionPersonal = this.llenarDireccionPersonalCliente( filtro );
			direccionFiscal = this.llenarDireccionFiscalCliente( filtro );
			direcciones.add( direccionPersonal );
			direcciones.add( direccionFiscal );
			
		}
		return direcciones;
	}
	
	/**
	 * llena la direccion Personal de un cliente
	 * @param filtro
	 * @return
	 */
	private DireccionCliente llenarDireccionPersonalCliente( ClienteGenericoDTO filtro ){
		DireccionCliente direccion = null;
		PaisDTO pais = new PaisDTO();
		EstadoDTO estado = new EstadoDTO();
		MunicipioDTO municipio = new MunicipioDTO();

		direccion = new DireccionCliente();
		direccion.setTipoDireccion( "Personal" );
		
		if ( filtro.getIdPaisString() != null && !filtro.getIdPaisString().equals("") ){
			pais = paisFacadeRemote.findById( filtro.getIdPaisString() );
			direccion.setPais( pais.getCountryName() );
		}
		
		if ( filtro.getIdEstadoString() != null && !filtro.getIdEstadoString().equals("") ){
			estado = estadoFacadeRemote.findById( filtro.getIdEstadoString() );
			direccion.setEstado( estado.getStateName() );
		}
		
		if ( filtro.getIdMunicipioString() != null && !filtro.getIdMunicipioString().equals("") ){
			municipio = municipioFacadeRemote.findById( filtro.getIdMunicipioString() );
			direccion.setMunicipio( municipio.getMunicipalityName() );
		}
		
		direccion.setColonia( filtro.getNombreColonia() );
		direccion.setCalleNumero( filtro.getNombreCalle() );
		direccion.setCodigoPostal( filtro.getCodigoPostal() );

		return direccion;	
	}
	
	/**
	 * llena la direccion Fiscal de un cliente
	 * @param filtro
	 * @return
	 */
	private DireccionCliente llenarDireccionFiscalCliente( ClienteGenericoDTO filtro ){
		DireccionCliente direccion = null;
		PaisDTO pais = new PaisDTO();
		EstadoDTO estado = new EstadoDTO();
		MunicipioDTO municipio = new MunicipioDTO();

		direccion = new DireccionCliente();
		direccion.setTipoDireccion( "Fiscal" );
		
		if ( filtro.getIdPaisString() != null && !filtro.getIdPaisString().equals("") ){
			pais = paisFacadeRemote.findById( filtro.getIdPaisString() );
			direccion.setPais( pais.getCountryName() );
		}
		
		if ( filtro.getIdEstadoFiscal() != null && !filtro.getIdEstadoFiscal().equals("") ){
			estado = estadoFacadeRemote.findById( filtro.getIdEstadoFiscal() );
			direccion.setEstado( estado.getStateName() );
		}
		
		if ( filtro.getIdMunicipioFiscal() != null && !filtro.getIdMunicipioFiscal().equals("") ){
			municipio = municipioFacadeRemote.findById( filtro.getIdMunicipioFiscal() );
			direccion.setMunicipio( municipio.getMunicipalityName() );
		}
		
		direccion.setColonia( filtro.getNombreColoniaFiscal() );
		direccion.setCalleNumero( filtro.getNombreCalleFiscal() );
		direccion.setCodigoPostal( filtro.getCodigoPostalFiscal() );

		return direccion;
	}
	
	/**
	 * Envia una solicitud de autorizacion por vigencia. El estatus puede ser para un inciso NO_VIGENTE o para uno CANCELADO
	 * @param idToPoliza
	 * @param numeroInciso
	 * @param reporteId
	 * @param estatusInciso
	 */
	@Override
    public void enviarSolicitudAutorizacionVigencia(Long idToPoliza, BigDecimal numeroInciso, Long reporteId, Short estatusInciso){
		SolicitudAutorizacionVigencia solicitudAutorizacionVigencia = this.createSolicitudAutorizacionVigencia(idToPoliza, numeroInciso, reporteId, estatusInciso);
		this.entidadService.save(solicitudAutorizacionVigencia);
	}
	
	
	
/**
	 * 
	 * 
	 * @param idToPoliza Id de la poliza
	 * @return  Estatus de la solicitud de autorización  
	 * 	   -2  - 	No aplica por ser Vigente
	 * 	   -1  - 	Sin Solicitud
	 * 		0  - 	Pendiente
	 * 		1  - 	Aprobado
	 * 		2  - 	Rechazado
	 */
	@Override
	public Integer validaSiTieneAutorizacionEnEsperaYEstatus(Long idToPoliza , String vigenciaInciso, Long idToReporte) {
		Integer estatusSolicitud = -1;
		if(vigenciaInciso.equals(ESTATUS_INCISO_VIGENTE)){
			estatusSolicitud = -2;
//		} else if (vigenciaInciso.equals(ESTATUS_INCISO_CANCELADO)) {
//			estatusSolicitud = -3;
		} else {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", idToReporte);
			params.put("poliza.idToPoliza", new BigDecimal(idToPoliza));
			List<ReporteCabina> reporte = entidadService.findByProperties(ReporteCabina.class, params);
			
			//Verificar si el inciso ya fue asignado al reporte cuando no es vigente y no paso por autorizacion, caso migracion
			if (CollectionUtils.isNotEmpty(reporte)) {
				estatusSolicitud = -2;
			} else {
				params = new HashMap<String, Object>();
				params.put("poliza.idToPoliza", idToPoliza);
				params.put("reporte.id", idToReporte);
				List<SolicitudAutorizacionVigencia> solicitudes = entidadService.findByPropertiesWithOrder(SolicitudAutorizacionVigencia.class, params, new String[]{"id"});
				SolicitudAutorizacionVigencia solicitud = null;
				if(solicitudes!= null && solicitudes.size()>0){
					solicitud = solicitudes.get(solicitudes.size()-1);	
					estatusSolicitud = solicitud.getEstatus(); 
				}
			}
			
			
		}
		return estatusSolicitud;
	}



	private SolicitudAutorizacionVigencia createSolicitudAutorizacionVigencia(Long idToPoliza, BigDecimal numeroInciso, Long reporteId, Short estatusInciso){
		final Integer ESTATUS_PENDIENTE = 0; 
		PolizaDTO poliza = new PolizaDTO();
		SolicitudAutorizacionVigencia solicitudAutorizacionVigencia = new SolicitudAutorizacionVigencia();

		poliza.setIdToPoliza( BigDecimal.valueOf( idToPoliza ) );
		
		solicitudAutorizacionVigencia.setPoliza(poliza);
		solicitudAutorizacionVigencia.setNumeroInciso(numeroInciso);
		solicitudAutorizacionVigencia.setEstatus(ESTATUS_PENDIENTE);
		ReporteCabina reporteCabina = this.entidadService.findById(ReporteCabina.class, reporteId);
		Oficina oficinaReporteCabina = reporteCabina.getOficina();
		solicitudAutorizacionVigencia.setOficina(oficinaReporteCabina);
		SolicitudAutorizacion  solicitudAutorizacion = createSolicitudAutorizacion(estatusInciso);
		solicitudAutorizacionVigencia.setSolicitudAutorizacion(solicitudAutorizacion);
		solicitudAutorizacionVigencia.setReporte(reporteCabina);
		return solicitudAutorizacionVigencia;
	}
	
	private SolicitudAutorizacion createSolicitudAutorizacion(Short estatusInciso){
		final Integer ESTATUS_PENDIENTE = 1; 
		String TIPO_SOLICITUD = estatusInciso.shortValue() == 2 ? "CAN" : "SAV";
		String userId = usuarioService.getUsuarioActual().getNombreUsuario();
		SolicitudAutorizacion solicitudAutorizacion = new SolicitudAutorizacion();
		solicitudAutorizacion.setCodUsuarioAutorizador(userId);
		solicitudAutorizacion.setEstatus(ESTATUS_PENDIENTE);
		solicitudAutorizacion.setFechaCreacion(Calendar.getInstance().getTime());
		solicitudAutorizacion.setFechaModEstatus(Calendar.getInstance().getTime());
		solicitudAutorizacion.setFechaModificacion(Calendar.getInstance().getTime());
		solicitudAutorizacion.setCodigoUsuarioCreacion(userId);
		solicitudAutorizacion.setCodigoUsuarioModificacion(userId);
		solicitudAutorizacion.setTipoSolicitud(TIPO_SOLICITUD);
		this.entidadService.save(solicitudAutorizacion);
		return solicitudAutorizacion;
	}
	
	
	/**
	 * Consulta los datos de la solicitud del reporte cabina
	 */
	@Override
	public SolicitudReporteCabinaDTO buscarCartaCobertura ( BigDecimal idToSolicitud, String serie ){
		SolicitudReporteCabinaDTO solicitudReporteCabina = new SolicitudReporteCabinaDTO();
		List<SolicitudDTO> listSolicitud = new ArrayList<SolicitudDTO>();
		List<SolicitudDataEnTramiteDTO> solicitudDataEnTramite = new ArrayList<SolicitudDataEnTramiteDTO>();
		Map<String,Object> params = new HashMap<String,Object>();
		params.put( "idToSolicitud", idToSolicitud );
		params.put( "tieneCartaCobertura", 1 );
		listSolicitud = entidadService.findByProperties( SolicitudDTO.class, params );
		if ( listSolicitud != null && !listSolicitud.isEmpty() ){
			params = new HashMap<String,Object>();
			params.put( "idToSolicitud", idToSolicitud );
			params.put( "serie", serie );
			solicitudDataEnTramite = entidadService.findByProperties( SolicitudDataEnTramiteDTO.class, params );
			listSolicitud.get(0).setNombreCompleto(
					listSolicitud.get(0).getNombrePersona().concat(" ").concat(
							StringUtil.isEmpty(listSolicitud.get(0).getApellidoPaterno())? "" : listSolicitud.get(0).getApellidoPaterno()).concat(
									" ").concat(StringUtil.isEmpty(listSolicitud.get(0).getApellidoMaterno())?"":
										listSolicitud.get(0).getApellidoMaterno()).trim());
			solicitudReporteCabina.setSolicitud( listSolicitud.get(0) ); 
			solicitudReporteCabina.setSolicitudDataEnTramite( solicitudDataEnTramite.get(0) );
			//Consultar documento
			List<DocumentoDigitalSolicitudDTO> documentoDigitalList = new ArrayList<DocumentoDigitalSolicitudDTO>();
			params = new HashMap<String,Object>();
			params.put( "solicitudDTO.idToSolicitud", idToSolicitud );
			params.put( "esCartaCobertura", 1 );
			//params.put( "serie", serie );
			documentoDigitalList = entidadService.findByProperties( DocumentoDigitalSolicitudDTO.class, params );
			if ( null!=documentoDigitalList  && !documentoDigitalList.isEmpty() ){
				solicitudReporteCabina.setDocumentoDigitalSolicitudDTO(documentoDigitalList.get(0));
			}
		}
		
		return solicitudReporteCabina;
	}

	@Override
	public void asignarCartaCobertura( Long idReporte, String idToSolicitud, String numeroSerie ) {
		ReporteCabina reporteCabina = null;
		SeccionReporteCabina seccionReporteCabina = null;
		IncisoReporteCabina incisoReporteCabina = null;
		AutoIncisoReporteCabina autoIncisoReporteCabina = null;
	
		try{	
			reporteCabina = this.llenarReporteCabinaSolicitud( idReporte, idToSolicitud );

			if( reporteCabina != null ){
				seccionReporteCabina = this.llenarSeccionReporteCabinaDatosMinimosRequeridos( reporteCabina, idReporte, null );
			    incisoReporteCabina = this.llenarIncisoReporteCabinaDatosMinimosRequeridos( seccionReporteCabina, idReporte );
				autoIncisoReporteCabina = this.llenarAutoIncisoReporteCabinaDatosMinimosRequeridos( incisoReporteCabina, idReporte, numeroSerie );
				
				entidadService.save( seccionReporteCabina );
				entidadService.save( incisoReporteCabina );
				autoIncisoReporteCabina = entidadService.save( autoIncisoReporteCabina );
				reporteCabinaService.guardarEvento(idReporte, TipoEvento.EVENTO_17, "Se Asigno la Solicitud de Poliza para el numero de Serie : "+numeroSerie);
			}
			//TODO REGISTRAR EVENTO (Asignación de Solicitud de Póliza)
		}catch (Exception ex) {
			log.error(ex);
		}
	}
	
	/**
	 * Asigna la Solicitud al ReporteCabina
	 * y los datos mínimos requeridos
	 * @param idReporte
	 * @param idToSolicitud
	 * @return
	 */
	private ReporteCabina llenarReporteCabinaSolicitud( Long idReporte, String idToSolicitud ){
		ReporteCabina reporteCabina = null;
		int modificaRegistro = 0;
		
		reporteCabina = entidadService.findById(ReporteCabina.class, idReporte);

		if( reporteCabina != null ){
			modificaRegistro = 1;
			reporteCabina.setIdToSolicitud( idToSolicitud );
			reporteCabina.setPoliza(null);
			reporteCabina.setAgenteId(null);
			reporteCabina = (ReporteCabina) this.llenarDatosControlEntidad( reporteCabina, modificaRegistro, null );
		}
		
		return reporteCabina;
	}
	
	/**
	 * Asigna ReporteCabina a SeccionReporteCabina
	 * y los datos mínimos requeridos
	 * @param reporteCabina
	 * @return
	 */
	private SeccionReporteCabina llenarSeccionReporteCabinaDatosMinimosRequeridos( ReporteCabina reporteCabina, Long idReporte, String usuario ){
		List<SeccionReporteCabina> listSeccionReporteCabina = new ArrayList<SeccionReporteCabina>();
		SeccionReporteCabina seccionReporteCabina = null;
		Map<String,Object> params = null;
		int modificaRegistro = 0;
		
		params = new HashMap<String,Object>();
		params.put( "reporteCabina.id", idReporte );
		
		listSeccionReporteCabina = entidadService.findByProperties( SeccionReporteCabina.class, params );
		
		if( listSeccionReporteCabina != null && !listSeccionReporteCabina.isEmpty() ){
			seccionReporteCabina = listSeccionReporteCabina.get(0);
			modificaRegistro = 1;
		} else {
			seccionReporteCabina = new SeccionReporteCabina();
		}
		
		seccionReporteCabina.setReporteCabina( reporteCabina );
		seccionReporteCabina.setSeccionDTO( null );
		seccionReporteCabina = (SeccionReporteCabina) this.llenarDatosControlEntidad( seccionReporteCabina, modificaRegistro, usuario );
		
		return seccionReporteCabina;
	}
	
	/**
	 * Asigna SeccionReporteCabina y número de inciso a IncisoReporteCabina
	 * y los datos mínimos requeridos
	 * @param seccionReporteCabina
	 * @return
	 */
	private IncisoReporteCabina llenarIncisoReporteCabinaDatosMinimosRequeridos( SeccionReporteCabina seccionReporteCabina, Long idReporte ){
		List<IncisoReporteCabina> listIncisoReporteCabina = new ArrayList<IncisoReporteCabina>();
		IncisoReporteCabina incisoReporteCabina = null;
		int numeroInciso = 0;
		Map<String,Object> params = null;
		int modificaRegistro = 0;
		
		params = new HashMap<String,Object>();
		params.put( "seccionReporteCabina.reporteCabina.id", idReporte );
		listIncisoReporteCabina = entidadService.findByProperties( IncisoReporteCabina.class, params );
		
		if( listIncisoReporteCabina != null && !listIncisoReporteCabina.isEmpty() ){
			incisoReporteCabina = listIncisoReporteCabina.get(0);
			modificaRegistro = 1;
		} else {
			incisoReporteCabina = new IncisoReporteCabina();
		}

		incisoReporteCabina.setSeccionReporteCabina( seccionReporteCabina );
		incisoReporteCabina.setNumeroInciso( numeroInciso );
		
		incisoReporteCabina = (IncisoReporteCabina) this.llenarDatosControlEntidad( incisoReporteCabina, modificaRegistro, null );

		return incisoReporteCabina;
	}
	
	/**
	 * Asigna IncisoReporteCabina a AutoIncisoReporteCabina
	 * y los datos mínimos requeridos
	 * @param incisoReporteCabina
	 * @return
	 */
	private AutoIncisoReporteCabina llenarAutoIncisoReporteCabinaDatosMinimosRequeridos( IncisoReporteCabina incisoReporteCabina, Long idReporte, String numeroSerie ){
		List<AutoIncisoReporteCabina> listAutoIncisoReporteCabina = null;
		AutoIncisoReporteCabina autoIncisoReporteCabina = null;
		Map<String,Object> params = null;
		int modificaRegistro = 0;
		
		params = new HashMap<String,Object>();
		params.put( "incisoReporteCabina.seccionReporteCabina.reporteCabina.id", idReporte );
		listAutoIncisoReporteCabina = entidadService.findByProperties( AutoIncisoReporteCabina.class, params );
		
		if( listAutoIncisoReporteCabina != null && !listAutoIncisoReporteCabina.isEmpty() ){
			autoIncisoReporteCabina = listAutoIncisoReporteCabina.get(0);
			modificaRegistro = 1;
		} else {
			autoIncisoReporteCabina = new AutoIncisoReporteCabina();
		}
		
		autoIncisoReporteCabina.setIncisoReporteCabina( incisoReporteCabina );
		autoIncisoReporteCabina.setNumeroSerie( numeroSerie );
		autoIncisoReporteCabina = (AutoIncisoReporteCabina) this.llenarDatosControlEntidad( autoIncisoReporteCabina, modificaRegistro, null );
		
		return autoIncisoReporteCabina;
	}
	
	/**
	 * Obtiene los datos de la prorroga
	 * @param idToCotizacion
	 * @param incisoContinuityId
	 * @return
	 */
	@Override
	public ToProrroga obtenerProrroga( Long idToCotizacion, Long numeroInciso, Date fechaReporteSiniestro ){
		List<ToProrroga> listProrroga 	= null;
		ToProrroga prorroga 			= null;

		listProrroga = prorrogaService.listarPorIdCotizacion( idToCotizacion, numeroInciso);
		
		if( listProrroga != null && !listProrroga.isEmpty() )
			prorroga = listProrroga.get(0);
		return prorroga;
	}
	
	/**
	 * Obtiene el idToSeccion
	 * @param incisoContinuityId
	 * @param validOn
	 * @return
	 */
	private BigDecimal obtenerIdToSeccion(BitemporalInciso biInciso, Date fechaReporteSiniestro) {
		BigDecimal idToSeccion = null;
		BitemporalAutoInciso biAutoInciso = (BitemporalAutoInciso) getCurrentBitemporal(biInciso.getContinuity().getAutoIncisoContinuity(), 
				new DateTime(fechaReporteSiniestro), biInciso.getRecordInterval().getInterval().getStart()/*ofal*/,
				new BigDecimal(biInciso.getContinuity().getCotizacionContinuity().getNumero()));
		
		NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, new BigDecimal(biAutoInciso.getValue().getNegocioSeccionId()));
		SeccionDTO seccion =  negocioSeccion.getSeccionDTO();

		idToSeccion = seccion.getIdToSeccion();
		
		return idToSeccion;
	}

	@Override
	public IncisoSiniestroDTO obtenerDatosIncisoAsignadoReporte(Long idReporte) {

		IncisoSiniestroDTO incisoSiniestroDTO = new IncisoSiniestroDTO();
		
		ReporteCabina reporteCabina = entidadService.findById(ReporteCabina.class, idReporte);
		
		IncisoReporteCabina incisoReporte = obtenerIncisoAsignadoAlReporte(idReporte);

		PolizaDTO poliza = reporteCabina.getPoliza();
		
		AutoInciso autoInciso = new AutoInciso();

		incisoSiniestroDTO.setNumeroInciso(incisoReporte.getNumeroInciso());
		incisoSiniestroDTO.setNumeroPoliza(poliza.getNumeroPolizaFormateada());		
		
		if (reporteCabina.getMoneda() != null) {
			incisoSiniestroDTO.setMoneda(reporteCabina.getMoneda().getDescripcion());
		}
		
		if (reporteCabina.getPersonaContratanteId() != null) {
			incisoSiniestroDTO.setDatosContratante(obtenerDatosContratante(BigDecimal.valueOf(reporteCabina.getPersonaContratanteId())));
		}	
		
		incisoSiniestroDTO.setProducto(poliza.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getDescripcion());
		incisoSiniestroDTO.setAgente(reporteCabina.getNombreAgente());		
		incisoSiniestroDTO.setIdToPoliza(poliza.getIdToPoliza());
		incisoSiniestroDTO.setFechaIniVigencia(incisoReporte.getValidFrom());
		incisoSiniestroDTO.setFechaFinVigencia(incisoReporte.getValidTo());
		incisoSiniestroDTO.setValidOnMillis(incisoReporte.getValidFrom() != null ? incisoReporte.getValidFrom().getTime() : null);
		incisoSiniestroDTO.setRecordFromMillis(incisoReporte.getRecordFrom() != null ? incisoReporte.getRecordFrom().getTime() : null);
		incisoSiniestroDTO.setNoAplicaDepuracion(incisoReporte.getNoAplicaDepuracion());
		incisoSiniestroDTO.setNombreContratante(incisoReporte.getNombreAsegurado());
		
		Long incisoContinuityId = reporteCabinaService.obtenerIncisoReporteCabina(idReporte);
		incisoSiniestroDTO.setIncisoContinuityId(incisoContinuityId);
		
		Date fechaVigRealInciso = polizaSiniestroDao.obtenerFechaVigenciaInicioRealPorInciso(incisoContinuityId);
		incisoSiniestroDTO.setFechaVigenciaIniRealInciso(fechaVigRealInciso);
		
		BigDecimal idToSeccion = incisoReporte.getSeccionReporteCabina().getSeccionDTO().getIdToSeccion();
		
		//Motivo de cobranza y validacion de vigencia
		
		if (incisoReporte.getEstatusInciso() != null) {
			if (incisoReporte.getEstatusInciso().equals(IncisoReporteCabina.EstatusVigenciaInciso.VIGENTE.toString())) {
				
				incisoSiniestroDTO.setEstatus((short)IncisoSiniestroDTO.EstatusIncisoSiniestro.VIGENTE.ordinal());	
				incisoSiniestroDTO.setDescEstatus(ESTATUS_INCISO_VIGENTE);
			} else if (incisoReporte.getEstatusInciso().equals(IncisoReporteCabina.EstatusVigenciaInciso.NOVIGENTE.toString())) {
				
				incisoSiniestroDTO.setEstatus((short)IncisoSiniestroDTO.EstatusIncisoSiniestro.NO_VIGENTE.ordinal());
				incisoSiniestroDTO.setDescEstatus(ESTATUS_INCISO_VENCIDA);
			} else if (incisoReporte.getEstatusInciso().equals(IncisoReporteCabina.EstatusVigenciaInciso.CANCELADO.toString())) {
				
				incisoSiniestroDTO.setEstatus((short)IncisoSiniestroDTO.EstatusIncisoSiniestro.CANCELADO.ordinal());
				incisoSiniestroDTO.setDescEstatus(ESTATUS_INCISO_CANCELADO);
			}
		} else if (incisoReporte.getValidTo() != null) {
			incisoSiniestroDTO.setDescEstatus(getEstatusDescInciso(incisoReporte.getValidTo(), reporteCabina.getFechaHoraOcurrido()));
			if(incisoSiniestroDTO.getDescEstatus().equals(ESTATUS_INCISO_VIGENTE)){
				incisoSiniestroDTO.setEstatus((short)IncisoSiniestroDTO.EstatusIncisoSiniestro.VIGENTE.ordinal());				
			}else{
				incisoSiniestroDTO.setEstatus((short)IncisoSiniestroDTO.EstatusIncisoSiniestro.NO_VIGENTE.ordinal());
			}
		}		
		
		if (incisoSiniestroDTO.getEstatus() == EstatusVigenciaInciso.VIGENTE.ordinal()) {
			incisoSiniestroDTO.setMotivo(incisoReporte.getSituacionCobranza());
		} else {
			obtenerMotivoCobranza(incisoSiniestroDTO, poliza.getIdToPoliza().longValue(), poliza.getCotizacionDTO().getIdToCotizacion(),
					idToSeccion, incisoReporte.getNumeroInciso(), reporteCabina.getFechaHoraOcurrido());
			if (incisoSiniestroDTO.getEstatus() == EstatusVigenciaInciso.VIGENTE.ordinal()) {
				incisoReporte.setEstatusInciso(ESTATUS_INCISO_VIGENTE);
				incisoReporte.setSituacionCobranza(incisoSiniestroDTO.getMotivo());
				entidadService.save(incisoReporte);
			}
		
		}				
		
		autoInciso.setNumeroSerie(incisoReporte.getAutoIncisoReporteCabina().getNumeroSerie());
		autoInciso.setDescLineaNegocio(incisoReporte.getSeccionReporteCabina().getSeccionDTO().getNombreComercial());
		autoInciso.setDescripcionFinal(incisoReporte.getAutoIncisoReporteCabina().getDescripcionFinal());
	
		autoInciso.setNumeroMotor(incisoReporte.getAutoIncisoReporteCabina().getNumeroMotor());
		autoInciso.setModeloVehiculo(incisoReporte.getAutoIncisoReporteCabina().getModeloVehiculo());
		autoInciso.setPlaca(incisoReporte.getAutoIncisoReporteCabina().getPlaca());
		autoInciso.setNombreConductor(reporteCabina.getPersonaConductor());
		
		MarcaVehiculoDTO marca = entidadService.findById(MarcaVehiculoDTO.class, incisoReporte.getAutoIncisoReporteCabina().getMarcaId());
		
		if (marca != null) {
			autoInciso.setDescMarca(marca.getDescription());
		}
		
		if (incisoReporte.getAutoIncisoReporteCabina().getEstiloId() != null) {
			String[] arrayEstilo = StringUtils.split(incisoReporte.getAutoIncisoReporteCabina().getEstiloId(), '_');
			EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(arrayEstilo[0], arrayEstilo[1], new BigDecimal(arrayEstilo[2]));

			EstiloVehiculoDTO estilo = entidadService.findById(EstiloVehiculoDTO.class, estiloVehiculoId);
			
			if (estilo != null) {
				autoInciso.setDescEstilo(estilo.getDescripcionEstilo());
			}
		}
		incisoSiniestroDTO.setAutoInciso(autoInciso);
		
		return incisoSiniestroDTO;
	}
	
	private void obtenerMotivoCobranza(IncisoSiniestroDTO inciso, Long idToPoliza, BigDecimal idToCotizacion, BigDecimal idToSeccion, Integer numeroInciso, Date fechaHoraOcurrido){
		String motivo = obtenerDescripcionMotivoCobranza(idToPoliza, idToSeccion, numeroInciso, fechaHoraOcurrido);
		//cambiar vigencia de la poliza basada en el motivo de la situacion
		if(inciso != null && inciso.getEstatus() != null) {
			if (inciso.getEstatus().intValue() == IncisoSiniestroDTO.EstatusIncisoSiniestro.VIGENTE.ordinal() && 
				!StringUtil.isEmpty(motivo) && motivo.startsWith("N")){
				inciso.setEstatus((short)IncisoSiniestroDTO.EstatusIncisoSiniestro.NO_VIGENTE.ordinal());
				inciso.setDescEstatus(ESTATUS_INCISO_VENCIDA);
			}
			
			if (inciso.getEstatus().intValue() == IncisoSiniestroDTO.EstatusIncisoSiniestro.NO_VIGENTE.ordinal() && 
					!StringUtil.isEmpty(motivo) && motivo.startsWith("S")){
					inciso.setEstatus((short)IncisoSiniestroDTO.EstatusIncisoSiniestro.VIGENTE.ordinal());
					inciso.setDescEstatus(ESTATUS_INCISO_VIGENTE);
			}
				
		}		
		String descMotivo = StringUtil.isEmpty(motivo) ? "" : motivo.substring(2); //eliminar caracter de control
		inciso.setMotivo(descMotivo) ;	
	}
	
	
	
	@Override
	public String obtenerSituacionVigenciaInciso(Long idReporte) {
		
		ReporteCabina reporteCabina = entidadService.findById(ReporteCabina.class, idReporte);		
		IncisoReporteCabina incisoReporte = obtenerIncisoAsignadoAlReporte(idReporte);
		
		return obtenerSituacionVigenciaInciso(incisoReporte.getValidFrom(), reporteCabina.getFechaOcurrido(), 
				incisoReporte.getAutoIncisoReporteCabina().getNumeroSerie(), reporteCabina.getPoliza().getIdToPoliza());
		
	}
	
	@Override
	public String obtenerSituacionVigenciaInciso(Date validFrom, Date fechaOcurrido, String numeroSerie, BigDecimal idToPoliza	) {
		String mensaje = null;
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, idToPoliza);
		
		if (Days.daysBetween(new DateTime(validFrom),
				new DateTime(fechaOcurrido)).getDays() <= 30) {
			if (poliza.getNumeroRenovacion() == 0) {
				mensaje = MensajeDTO.MENSAJE_VIGENCIA_INCISO_SINIESTRO;
			} else {
				Map<String, Object> params = new HashMap<String, Object>();
				
				
				
				params.put("codigoProducto", StringUtils.rightPad(poliza.getCodigoProducto(),8));
				params.put("codigoTipoPoliza", StringUtils.rightPad(poliza.getCodigoTipoPoliza(),8));
				params.put("numeroPoliza", poliza.getNumeroPoliza());
				params.put("numeroRenovacion", poliza.getNumeroRenovacion() - 1);				
				
				List<PolizaDTO> polizas = entidadService.findByProperties(PolizaDTO.class, params);
		
				if (polizas != null && !polizas.isEmpty()) {
					PolizaDTO polizaAnterior = polizas.get(0);
					
					CotizacionContinuity cotizacionContinuity = entidadContinuityService.findContinuityByBusinessKey(
							CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, polizaAnterior.getCotizacionDTO().getIdToCotizacion().intValue());
					
					if (cotizacionContinuity != null) {
						List<BitemporalInciso> lstBitemporales = polizaSiniestroDao.obtenerUltimoBitemporalInciso( 
								polizaAnterior.getCotizacionDTO().getIdToCotizacion().intValue(), 
								numeroSerie);
						
						if (lstBitemporales != null && !lstBitemporales.isEmpty()) {
							BitemporalInciso bitemporalInciso = lstBitemporales.get(0);
							if (Days.daysBetween(new DateTime(fechaOcurrido), 
									new DateTime(bitemporalInciso.getValidityInterval().getInterval().getEnd().toDate())).getDays() <= 30
								&& Days.daysBetween(new DateTime(bitemporalInciso.getValidityInterval().getInterval().getEnd().toDate()), 
										new DateTime(validFrom)).getDays() >= 1)  {
								mensaje = MensajeDTO.MENSAJE_PERIODO_DESCUBIERTO_INCISO_SINIESTRO;
							}
							
						}
					}
				}
			}
			
		} 
		
		return mensaje;
	}

	@Override
	public IncisoSiniestroDTO obtenerCartaCoberturaAsignadoReporte(Long idToReporte , String idToSolicitud) {
		
		IncisoSiniestroDTO incisoSiniestroDTO					= new IncisoSiniestroDTO();
		AutoInciso autoInciso 									= new AutoInciso();
		AutoIncisoReporteCabina autoIncisoReporteCabina 		= null;
		List<AutoIncisoReporteCabina> listAutoIncisoReporte 	= null;
		List<SolicitudDataEnTramiteDTO> listSolicitudesData 	= null;
		SolicitudDataEnTramiteDTO solicitudDataEnTramiteDTO 	= null;
		
		listAutoIncisoReporte = entidadService.findByProperty( AutoIncisoReporteCabina.class, "incisoReporteCabina.seccionReporteCabina.reporteCabina.id", idToReporte );
		
		if(listAutoIncisoReporte != null && listAutoIncisoReporte.size()>0){
			autoIncisoReporteCabina = listAutoIncisoReporte.get(0);
		}
		
		autoInciso.setNumeroSerie( autoIncisoReporteCabina.getNumeroSerie());
		
		listSolicitudesData = solicitudDataEnTramiteService.findBySolicitudAndSerie( Long.parseLong(idToSolicitud), autoIncisoReporteCabina.getNumeroSerie());
		
		if(listSolicitudesData !=null && listSolicitudesData.size() >0){
			solicitudDataEnTramiteDTO = listSolicitudesData.get(0);
			
			incisoSiniestroDTO.setFechaIniVigencia( solicitudDataEnTramiteDTO.getFechaInicioVigencia());
			incisoSiniestroDTO.setFechaFinVigencia( solicitudDataEnTramiteDTO.getFechaFinVigencia() );
		}
		
		incisoSiniestroDTO.setAutoInciso(autoInciso);
		incisoSiniestroDTO.setIdToSolicitud( BigDecimal.valueOf( Long.parseLong(idToSolicitud) ));
		
		return incisoSiniestroDTO;
	}
	
	
	@Override
	@Deprecated
	public String obtenerMotivoCobranza(Long idToPoliza, BigDecimal idToSeccion, Integer numeroInciso, Date fechaReporteSiniestro){
		return polizaSiniestroDao.obtenerMotivoCobranza(idToPoliza, idToSeccion, numeroInciso, fechaReporteSiniestro);		
	}
	
	@Override
	public String obtenerDescripcionMotivoCobranza(Long idToPoliza, BigDecimal idToSeccion, Integer numeroInciso, Date fechaReporteSiniestro){
		return polizaSiniestroDao.obtenerDescripcionMotivoCobranza(idToPoliza, idToSeccion, numeroInciso, fechaReporteSiniestro);		
	}
	
	@Override
	public List<ControlDinamicoRiesgoDTO> obtenerDatosRiesgo(Long idReporteCabina, Long idCobertura) {
		
		Long incisoContinuityId = reporteCabinaService.obtenerIncisoReporteCabina(idReporteCabina);
		
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class, idReporteCabina);
		
	    List<IncisoReporteCabina> incisosReporte = entidadService.findByProperty(IncisoReporteCabina.class,
					"seccionReporteCabina.reporteCabina.id", idReporteCabina);
	    
	    return getDatosRiesgo(incisoContinuityId, idCobertura, incisosReporte.get(0).getValidFrom(),
	    		incisosReporte.get(0).getRecordFrom(), reporte.getFechaHoraOcurrido());
	}
	
	
	/**
	 * Regresa el IncisoReporteCabina Asignado al reporte, con el idReporte
	 * @param idReporte
	 * @return
	 */
	@Override
	public IncisoReporteCabina obtenerIncisoAsignadoAlReporte( Long idReporte ){
		Map<String,Object> params 						= null;
		List<IncisoReporteCabina> listIncisoReporte		= new ArrayList<IncisoReporteCabina>();
		IncisoReporteCabina incisoReporteCabina			= null;
		
		params = new HashMap<String,Object>();
		params.put( "seccionReporteCabina.reporteCabina.id", idReporte );
		listIncisoReporte = entidadService.findByProperties( IncisoReporteCabina.class, params );
		
		if( listIncisoReporte != null && !listIncisoReporte.isEmpty() ){
			incisoReporteCabina = listIncisoReporte.get(0);		
		}
		
		return incisoReporteCabina;
	}
	
	private void copiarCoberturas( List<BitemporalCoberturaSeccion> listBiCoberturaSeccion, IncisoReporteCabina incisoReporteCabina ){
		List<CoberturaReporteCabina> listCoberturaReporteCabina = new ArrayList<CoberturaReporteCabina>();
		CoberturaReporteCabina coberturaReporteCabina = null;
		
		for( BitemporalCoberturaSeccion biCoberturaSeccion : listBiCoberturaSeccion ){
			
			coberturaReporteCabina = new CoberturaReporteCabina();
			
			coberturaReporteCabina.setClaveTipoCalculo( biCoberturaSeccion.getEntidadContinuity().getCoberturaDTO().getClaveTipoCalculo() );
			coberturaReporteCabina.setClaveTipoDeducible( biCoberturaSeccion.getValue().getClaveTipoDeducible() );
			coberturaReporteCabina.setClaveTipoLimiteDeducible( biCoberturaSeccion.getValue().getClaveTipoLimiteDeducible() );
			coberturaReporteCabina.setAgrupadorTarifaId( biCoberturaSeccion.getValue().getAgrupadorTarifaId() );
			coberturaReporteCabina.setClaveAutCoaseguro( biCoberturaSeccion.getValue().getClaveAutCoaseguro() );
			coberturaReporteCabina.setClaveAutDeducible( biCoberturaSeccion.getValue().getClaveAutDeducible() );
			coberturaReporteCabina.setClaveContrato( biCoberturaSeccion.getValue().getClaveContrato() );
			coberturaReporteCabina.setClaveFacultativo( biCoberturaSeccion.getValue().getClaveFacultativo() );
			coberturaReporteCabina.setClaveObligatoriedad( biCoberturaSeccion.getValue().getClaveObligatoriedad() );
			coberturaReporteCabina.setCodigoUsuarioAutCoaseguro( biCoberturaSeccion.getValue().getCodigoUsuarioAutCoaseguro() );
			coberturaReporteCabina.setCodigoUsuarioAutDeducible( biCoberturaSeccion.getValue().getCodigoUsuarioAutDeducible() );
			coberturaReporteCabina.setCodigoUsuarioAutReaseguro( biCoberturaSeccion.getValue().getCodigoUsuarioAutReaseguro() );
			coberturaReporteCabina.setDiasSalarioMinimo( biCoberturaSeccion.getValue().getDiasSalarioMinimo() );
			coberturaReporteCabina.setFechaAutCoaSeguro( biCoberturaSeccion.getValue().getFechaAutCoaSeguro() );
			coberturaReporteCabina.setFechaAutDeducible( biCoberturaSeccion.getValue().getFechaAutDeducible() );
			coberturaReporteCabina.setFechaSolAutCoaSeguro( biCoberturaSeccion.getValue().getFechaSolAutCoaSeguro() );
			coberturaReporteCabina.setFechaSolAutDeducible( biCoberturaSeccion.getValue().getFechaSolAutDeducible() );
			coberturaReporteCabina.setNumeroAgrupacion( biCoberturaSeccion.getValue().getNumeroAgrupacion() );
			coberturaReporteCabina.setPorcentajeCoaseguro( biCoberturaSeccion.getValue().getPorcentajeCoaseguro() );
			coberturaReporteCabina.setPorcentajeDeducible( biCoberturaSeccion.getValue().getPorcentajeDeducible() );
			coberturaReporteCabina.setTarifaExtId( biCoberturaSeccion.getValue().getTarifaExtId() );
			coberturaReporteCabina.setValorCoaseguro( biCoberturaSeccion.getValue().getValorCoaseguro() );
			coberturaReporteCabina.setValorCuota( biCoberturaSeccion.getValue().getValorCuota() );
			coberturaReporteCabina.setValorDeducible( biCoberturaSeccion.getValue().getValorDeducible() );
			coberturaReporteCabina.setValorMaximoLimiteDeducible( biCoberturaSeccion.getValue().getValorMaximoLimiteDeducible() );
			coberturaReporteCabina.setValorMinimoLimiteDeducible( biCoberturaSeccion.getValue().getValorMinimoLimiteDeducible() );
			coberturaReporteCabina.setValorPrimaDiaria( biCoberturaSeccion.getValue().getValorPrimaDiaria() );
			coberturaReporteCabina.setValorPrimaNeta( biCoberturaSeccion.getValue().getValorPrimaNeta() );
			coberturaReporteCabina.setValorSumaAsegurada( biCoberturaSeccion.getValue().getValorSumaAsegurada() );
			coberturaReporteCabina.setVerAgrupadorTarifaId( biCoberturaSeccion.getValue().getVerAgrupadorTarifaId() );
			coberturaReporteCabina.setVerCargaId( biCoberturaSeccion.getValue().getVerCargaId() );
			coberturaReporteCabina.setVerTarifaExtId( biCoberturaSeccion.getValue().getVerTarifaExtId() );
			coberturaReporteCabina.setVerTarifaId( biCoberturaSeccion.getValue().getVerTarifaId() );
			coberturaReporteCabina.setValorSumaAseguradaMax( biCoberturaSeccion.getValue().getValorSumaAseguradaMax() );
			coberturaReporteCabina.setValorSumaAseguradaMin( biCoberturaSeccion.getValue().getValorSumaAseguradaMin() );
			coberturaReporteCabina.setDescripcionDeducible( biCoberturaSeccion.getValue().getDescripcionDeducible() );
			coberturaReporteCabina.setSubRamoDTO( entidadService.findById( SubRamoDTO.class, new BigDecimal(biCoberturaSeccion.getValue().getSubRamoId().longValue())) );
			coberturaReporteCabina.setCoberturaDTO( entidadService.findById( CoberturaDTO.class, biCoberturaSeccion.getContinuity().getCoberturaDTO().getIdToCobertura()) );
			coberturaReporteCabina.setIncisoReporteCabina( incisoReporteCabina );
			listCoberturaReporteCabina.add( coberturaReporteCabina );
		}
		entidadService.saveAll( listCoberturaReporteCabina );
	
	}
	
	@Override
	public boolean validacionTieneCoberturasAfectadas(Long idReporteCabina) {
		List<EstimacionCoberturaReporteCabina> list  	= null;
		boolean resultado 								= Boolean.FALSE;
		
		list = entidadService.findByProperty(EstimacionCoberturaReporteCabina.class, "coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id", idReporteCabina);
		
		if(list != null && list.size()>0){
			resultado = Boolean.TRUE;
		}
		
		return resultado;
	}
	
	@Override
	public List<String> validaCondicionEspecial(CondicionEspecial condicionEspecial,
			ReporteCabina reporteCabina, TIPO_VALIDACION_CONDICION validacion) {
		List<String> mensajes = new ArrayList<String>();
		switch(TIPO_VALIDACION_CONDICION.tipoValidacion(validacion.valor())){
			case VALIDAR_REPORTE: 
				validaReporte(condicionEspecial, reporteCabina, mensajes);
				break;
			case VALIDAR_PASE_GM:
				validaPaseGastoMedico(condicionEspecial, reporteCabina, mensajes);
				break;
			case VALIDAR_SINIESTRO:
				validaSiniestro(condicionEspecial, reporteCabina, mensajes);
				break;
		}
		return mensajes;
	}

	private void validaReporte(CondicionEspecial condicionEspecial,
			ReporteCabina reporteCabina, List<String> mensajes){
		VarAjusteCondicionEsp varAjuste;
		FactorAreaCondEsp factorAreaCondicion;
		varAjuste = obtenerVariableAjuste(condicionEspecial.getId(), CODIGO_FECHA_OCURRIDO);
		if(varAjuste != null && varAjuste.getValores().size() >0 && varAjuste.getValores().size() >0){
			try {
				Date fechaInicio = new SimpleDateFormat("dd/MM/yyyy").parse(varAjuste.getValores().get(0).getValor());
				Date fechaFin = new SimpleDateFormat("dd/MM/yyyy").parse(varAjuste.getValores().get(1).getValor());
				if(reporteCabina.getFechaOcurrido() == null || reporteCabina.getFechaOcurrido().compareTo(fechaInicio) < 0 || reporteCabina.getFechaOcurrido().compareTo(fechaFin) > 0){
					mensajes.add(MessageFormat.format(Utilerias.getMensajeRecurso(MENSAGES, "midas.siniestros.cabina.evaluacionCondicion.fechaOcurrido"),
							condicionEspecial.getNombre(), format.format(fechaInicio), format.format(fechaFin)));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		varAjuste = obtenerVariableAjuste(condicionEspecial.getId(), CODIGO_HORA_OCURRIDO);
		if(varAjuste != null && varAjuste.getValores().size() >0){
			String horaOcurridoIni = varAjuste.getValores().get(0).getValor();
			String horaOcurridoFin = varAjuste.getValores().get(1).getValor();
			if(!StringUtils.isNotEmpty(reporteCabina.getHoraOcurrido()) || reporteCabina.getHoraOcurrido().compareTo(horaOcurridoIni) < 0 ||
					reporteCabina.getHoraOcurrido().compareTo(horaOcurridoFin) > 0){
				mensajes.add(MessageFormat.format(Utilerias.getMensajeRecurso(MENSAGES, "midas.siniestros.cabina.evaluacionCondicion.horaOcurrido"), 
						condicionEspecial.getNombre(), horaOcurridoIni, horaOcurridoFin));
			}
		}
		varAjuste = obtenerVariableAjuste(condicionEspecial.getId(), CODIGO_LUGAR_OCURRIDO);
		if(varAjuste != null && varAjuste.getValores().size() >0){
			String lugarOcurrido = varAjuste.getValores().get(0).getValor();
			if(reporteCabina.getLugarOcurrido() == null || !reporteCabina.getLugarOcurrido().getEstado().getId().equalsIgnoreCase(varAjuste.getValores().get(0).getValor())){
				EstadoMidas estado = entidadService.findById(EstadoMidas.class, lugarOcurrido);
				mensajes.add(MessageFormat.format(Utilerias.getMensajeRecurso(MENSAGES, "midas.siniestros.cabina.evaluacionCondicion.lugarOcurrido"),
						condicionEspecial.getNombre(), estado.getDescripcion()));
			}
		}
		factorAreaCondicion = obtenerAreaCondEspecial(condicionEspecial.getId(), CODIGO_AREA_CABINA, CODIGO_SERVICIO);
		if(factorAreaCondicion != null){
			String tipoPrestadorServicioId = factorAreaCondicion.getValor();
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("reporteCabina.id", reporteCabina.getId());
			params.put("tipoPrestadorServicio", tipoPrestadorServicioId);
			List<GastoAjusteReporte> gastosAjuste = entidadService.findByProperties(GastoAjusteReporte.class, params);
			if(gastosAjuste.size() <= 0){
				String descripcionTipoPrestador = "";
				try{
					descripcionTipoPrestador = listadoService.getMapTipoPrestador().get(Long.parseLong(tipoPrestadorServicioId));
				}catch (Exception e){
					descripcionTipoPrestador = "";
				}
				mensajes.add(MessageFormat.format(Utilerias.getMensajeRecurso(MENSAGES, "midas.siniestros.cabina.evaluacionCondicion.servicio"),
						condicionEspecial.getNombre(), descripcionTipoPrestador));
			}
		}
	}
	
	private void validaPaseGastoMedico(CondicionEspecial condicionEspecial,
			ReporteCabina reporteCabina, List<String> mensajes){
		VarAjusteCondicionEsp varAjuste;
		varAjuste = obtenerVariableAjuste(condicionEspecial.getId(), CODIGO_PASE_MEDICO);
		if(varAjuste != null && varAjuste.getValores().size() >0){
			List<TerceroGastosMedicos> coberturasReporte = obtenerCoberturaRepGastosMedicos(reporteCabina.getId());
			Integer numeroPases = Integer.parseInt(varAjuste.getValores().get(0).getValor());
			if(coberturasReporte.size() > numeroPases){
				mensajes.add(MessageFormat.format(Utilerias.getMensajeRecurso(MENSAGES, "midas.siniestros.cabina.evaluacionCondicion.pasesMedicos"),
						condicionEspecial.getNombre(), numeroPases));
			}
		}
	}
	
	private void validaSiniestro(CondicionEspecial condicionEspecial,
			ReporteCabina reporteCabina, List<String> mensajes){
		VarAjusteCondicionEsp varAjuste;
		FactorAreaCondEsp factorAreaCondicion;
		if(autoInciso == null){
			autoInciso = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(reporteCabina.getId());
		}
		varAjuste = obtenerVariableAjuste(condicionEspecial.getId(), CODIGO_TIPO_SINIESTRO);
		if(varAjuste != null && varAjuste.getValores().size() >0){
			String causaSiniestro = varAjuste.getValores().get(0).getValor();
			if(!causaSiniestro.equalsIgnoreCase(autoInciso.getCausaSiniestro())){
				CatValorFijo valor = catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.CAUSA_SINIESTRO, causaSiniestro);
				mensajes.add(MessageFormat.format(Utilerias.getMensajeRecurso(MENSAGES, "midas.siniestros.cabina.evaluacionCondicion.tipoSiniestro"),
						condicionEspecial.getNombre(), valor.getDescripcion()));
			}
		}
		varAjuste = obtenerVariableAjuste(condicionEspecial.getId(), CODIGO_TIPO_RESPONSABILIDAD);
		if(varAjuste != null && varAjuste.getValores().size() >0){
			String tipoResposabilidad = varAjuste.getValores().get(0).getValor();
			if(!tipoResposabilidad.equalsIgnoreCase(autoInciso.getTipoResponsabilidad())){
				CatValorFijo valor = catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RESPONSABILIDAD, tipoResposabilidad);
				mensajes.add(MessageFormat.format(Utilerias.getMensajeRecurso(MENSAGES, "midas.siniestros.cabina.evaluacionCondicion.tipoResponsabilidad"),
						condicionEspecial.getNombre(), valor.getDescripcion()));
			}
		}
		varAjuste = obtenerVariableAjuste(condicionEspecial.getId(), CODIGO_EDAD_CONDUCTOR);
		if(varAjuste != null && varAjuste.getValores().size() >0){
			Integer edadMinima = Integer.parseInt(varAjuste.getValores().get(0).getValor());
			Integer edadMaxima = Integer.parseInt(varAjuste.getValores().get(1).getValor());
			if(autoInciso.getEdad() == null || autoInciso.getEdad() < edadMinima || autoInciso.getEdad() > edadMaxima){
				mensajes.add(MessageFormat.format(Utilerias.getMensajeRecurso(MENSAGES, "midas.siniestros.cabina.evaluacionCondicion.edadConductor"),
						condicionEspecial.getNombre(), edadMinima, edadMaxima));
			}
		}
		varAjuste = obtenerVariableAjuste(condicionEspecial.getId(), CODIGO_GENERO);
		if(varAjuste != null && varAjuste.getValores().size() >0){
			String genero = varAjuste.getValores().get(0).getValor();
			if(!genero.equalsIgnoreCase(autoInciso.getGenero())){
				CatValorFijo valor = catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.GENERO_PERSONA, genero);
				mensajes.add(MessageFormat.format(Utilerias.getMensajeRecurso(MENSAGES, "midas.siniestros.cabina.evaluacionCondicion.genero"),
						condicionEspecial.getNombre(), valor.getDescripcion()));
			}
		}
		varAjuste = obtenerVariableAjuste(condicionEspecial.getId(), CODIGO_TIPO_LICENCIA);
		if(varAjuste != null && varAjuste.getValores().size() >0){
			String tipoLicencia = varAjuste.getValores().get(0).getValor();
			if(!tipoLicencia.equalsIgnoreCase(autoInciso.getTipoLicencia())){
				CatValorFijo valor = catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.TIPO_LICENCIA_CONDUCIR, tipoLicencia);
				mensajes.add(MessageFormat.format(Utilerias.getMensajeRecurso(MENSAGES, "midas.siniestros.cabina.evaluacionCondicion.tipoLicencia"),
						condicionEspecial.getNombre(), valor.getDescripcion()));
			}
		}
		varAjuste = obtenerVariableAjuste(condicionEspecial.getId(), CODIGO_NUM_OCUPANTES);
		if(varAjuste != null && varAjuste.getValores().size() >0){
			Integer numeroOcupantes = Integer.parseInt(varAjuste.getValores().get(0).getValor());
			if(autoInciso.getNumOcupantes() == null || autoInciso.getNumOcupantes() > numeroOcupantes){
				mensajes.add(MessageFormat.format(Utilerias.getMensajeRecurso(MENSAGES, "midas.siniestros.cabina.evaluacionCondicion.numeroOcupantes"),
						condicionEspecial.getNombre(), numeroOcupantes));
			}
		}
		varAjuste = obtenerVariableAjuste(condicionEspecial.getId(), CODIGO_TERCERO_ASEGURADO);
		if(varAjuste != null && varAjuste.getValores().size() >0){
			if(autoInciso.getCiaSeguros() == null){
				mensajes.add(MessageFormat.format(Utilerias.getMensajeRecurso(MENSAGES, "midas.siniestros.cabina.evaluacionCondicion.terceroAsegurado"),
						condicionEspecial.getNombre()));
			}
		}
		factorAreaCondicion = obtenerAreaCondEspecial(condicionEspecial.getId(), CODIGO_AREA_CABINA, CODIGO_NUM_EVENTOS);
		if(factorAreaCondicion != null){
			Integer numeroEventos = Integer.parseInt(factorAreaCondicion.getValor());
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("condicionEspecial.id", condicionEspecial.getId());
			params.put("reporteCabina.poliza.idToPoliza", reporteCabina.getPoliza().getIdToPoliza());
			params.put("claveContrato", true);
			List<CondicionEspecialReporte> condEspecialReporte = entidadService.findByProperties(CondicionEspecialReporte.class, params);
			if(condEspecialReporte.size() > numeroEventos){
				mensajes.add(MessageFormat.format(Utilerias.getMensajeRecurso(MENSAGES, "midas.siniestros.cabina.evaluacionCondicion.numeroEventos"),
						condicionEspecial.getNombre(), numeroEventos));
			}
		}
	}
	
	private FactorAreaCondEsp obtenerAreaCondEspecial(Long condicionEspecialId, String codigoArea, String codigoFactor){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("condicionEspecial.id", condicionEspecialId);
		params.put("areaImpacto.codigo", codigoArea);
		List<AreaCondicionEspecial> areaCondicion = entidadService.findByProperties(AreaCondicionEspecial.class, params);
		if(areaCondicion.size() > 0){
			AreaCondicionEspecial areaCond = areaCondicion.get(0);
			params = new HashMap<String, Object>();
			params.put("area.id", areaCond.getId());
			params.put("factor.codigo", codigoFactor);
			List<FactorAreaCondEsp> factorAreaCondicion = entidadService.findByProperties(FactorAreaCondEsp.class, params);
			if(factorAreaCondicion.size() > 0){
				return factorAreaCondicion.get(0);
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	
	private List<TerceroGastosMedicos> obtenerCoberturaRepGastosMedicos(Long reporteCabinaId){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id", reporteCabinaId);
		return entidadService.findByProperties(TerceroGastosMedicos.class, params);
	}
	
	private VarAjusteCondicionEsp obtenerVariableAjuste(Long condicionEspecialId, String codigoVaribleAjuste){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("condicionEspecial.id", condicionEspecialId);
		params.put("variableAjuste.codigo", codigoVaribleAjuste);
		List<VarAjusteCondicionEsp> varAjuste = entidadService.findByProperties(VarAjusteCondicionEsp.class, params);
		if(varAjuste.size() > 0){
			return varAjuste.get(0);
		}else{
			return null;
		}
	}
	
	/**
	 * Borra las Condiciones Especiales asociadas a un Reporte Cabina
	 * @param idReporte
	 */
	private void desasignarCondicionesEspecialesReporte( Long idReporte ){
		List<CondicionEspecialReporte> condicionesEspecialesReporteList = new ArrayList<CondicionEspecialReporte>();
		Map<String,Object> params = null;
		
		if( idReporte != null ){
			params = new HashMap<String,Object>();
			params.put( "reporteCabina.id", idReporte );
			
			condicionesEspecialesReporteList = entidadService.findByProperties(CondicionEspecialReporte.class, params );
			for(CondicionEspecialReporte  condicionEspecialReporte:condicionesEspecialesReporteList){
				condicionEspecialReporte.setClaveContrato(false);
				condicionEspecialReporte = entidadService.save( condicionEspecialReporte );
			}
		}
	}

	public List<String> getMensajesEvaluacion() {
		return mensajesEvaluacion;
	}

	public void setAutoInciso(AutoIncisoReporteCabina autoInciso) {
		this.autoInciso = autoInciso;
	}	
	
	public void actualizarPolizaSiniestro() {

		log.info("Ejecutando tarea actualizarPolizaSiniestro");
		
		// # PROCESO VALIDACION:
		// 		1) BUSCAR QUE EXISTA POLIZA POR IDSOLICITUD-IDCOTIZACION
		//		2) TIENE POLIZA PERO NO INCISOS
		//      3) TIENE POLIZA MULTIPLES INCISOS
		//      4) HAPPY PATH : TIENE POLIZA Y UN SOLO INCISO
		//  TODOS LOS DÍAS CADA 12 HORAS: 0 0 12 1/1 * ? *
	
		List<ReporteCabina> lReporteCabina = this.polizaSiniestroDao.obtenerSiniestrosSinPoliza();
		
		// # SALVA LOG
		Long keyCron = this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(null,null,null,new Date(),"Se inicia ejecución de Cron",null,null));
		
		if( !lReporteCabina.isEmpty() ){
			
			// # SALVA LOG
			Long keyIteracion = this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(null,null,null,new Date(),"Reporte Cabina tiene: "+lReporteCabina.size()+" para procesar",null,keyCron));
			
			// # RECORRER CABINA PARA OBTENER IDTOSOLICITUD Y EL NUMERO DE SERIE
			for (ReporteCabina reporte : lReporteCabina ){
				
				Double horas = this.getDiferenciaHoras( reporte.getFechaCreacion() );

				// # OBTENER ID_COTIZACION
				Map<String, Object> paramCotizacion = new LinkedHashMap<String, Object>();
				paramCotizacion.put( "solicitudDTO.idToSolicitud",new BigDecimal(reporte.getIdToSolicitud() ));
				List<CotizacionDTO> lCotizacionDto = this.entidadService.findByProperties(CotizacionDTO.class, paramCotizacion);

				// # SALVA LOG			
				try{ this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(Long.valueOf(reporte.getIdToSolicitud()),null,null,new Date(),"IdToSolicitud", null,keyIteracion)); }catch(Exception e){ log.error("Error log 1: "+e); } 

				// # VALIDACION 1)
				if ( !lCotizacionDto.isEmpty() ){
					
					// # OBTENER LA POLIZA
					Map<String, Object> paramPoliza = new LinkedHashMap<String, Object>();
					paramPoliza.put( "cotizacionDTO.idToCotizacion", lCotizacionDto.get(0).getIdToCotizacion() );
					List<PolizaDTO> lPolizaDto = this.entidadService.findByProperties(PolizaDTO.class, paramPoliza );
					
					// # SALVA LOG			
					try{ this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(Long.valueOf(reporte.getIdToSolicitud()),null,null,new Date(),"Poliza para la cotización:"+lCotizacionDto.get(0).getIdToCotizacion(),"Registros en la poliza:"+String.valueOf(lPolizaDto.size()),keyIteracion)); }catch(Exception e){ log.error("Error log 2: "+e); }
					
					if( !lPolizaDto.isEmpty() ){
						
						// # OBTENER ID COTIZACION CONTINUITY PARA DESPUES BUSCAR EN MCOTIZACION_CONTINUITY
						Map<String, Object> paramCotContinuity = new LinkedHashMap<String, Object>(); 
						paramCotContinuity.put( "numero", lCotizacionDto.get(0).getIdToCotizacion() );
						
						CotizacionContinuity cotizacionContinuity = entidadContinuityService.findContinuityByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, lCotizacionDto.get(0).getIdToCotizacion());
						if ( cotizacionContinuity != null ) {
						
							Collection<IncisoContinuity> lIncisoCot = entidadContinuityService.findContinuitiesByParentKey(IncisoContinuity.class, IncisoContinuity.PARENT_KEY_NAME, cotizacionContinuity.getId());
							
							// # VALIDACION 1)
							if( !lIncisoCot.isEmpty() ){
								
								// # VALIDACION 3
								if( !this.validarIncisos(lIncisoCot) ){
									
									//# SALVA LOG			
									try{ this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(Long.valueOf(reporte.getIdToSolicitud()),new Long(lIncisoCot.size()),null,new Date(),"Incisos Repetidos", null,keyIteracion)); }catch(Exception e){ log.error("Error log 3: "+e); }
									
									// NOTIFICA ERROR REGLA 2
									try{ this.notificaResponsables(horas,(short) 2, reporte, lPolizaDto.get(0), reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina()); }catch(Exception e){ log.error("Error notificacion regla 2: "+e); }
									
								}else{
									
									//# SALVA LOG			
									try { this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(Long.valueOf(reporte.getIdToSolicitud()),new Long(lIncisoCot.size()),null,new Date(),"1 Inciso", null,keyIteracion)); }catch(Exception e){ log.error("Error log 4: "+e); }
									
									// # INICIA PROCESO DE ASIGNACION POLIZA
									try{
										Long idToCotizacion = new Long ( lCotizacionDto.get(0).getIdToCotizacion().toString() );
										String numeroSerie  = reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getNumeroSerie();
									
										List<BitemporalInciso> lBitemporalInciso = this.polizaSiniestroDao.obtenerIncisoCartaCobertura( idToCotizacion , numeroSerie );
									
											if ( !lBitemporalInciso.isEmpty() ){
											
												//# SALVA LOG			
												this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(Long.valueOf(reporte.getIdToSolicitud()),new Long(lIncisoCot.size()),lBitemporalInciso.get(0).getId().toString(),new Date(),"Asignacion de poliza", null,keyIteracion));

												IncisoContinuity incisoContinuity = entidadContinuityService.findContinuityByKey(IncisoContinuity.class, lBitemporalInciso.get(0).getContinuity().getId());
												
												BitemporalInciso bitemporalInciso = incisoContinuity.getBitemporalProperty().get(TimeUtils.now());
												
												if (bitemporalInciso != null) {
													
													this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(Long.valueOf(reporte.getIdToSolicitud()),new Long(lIncisoCot.size()),lBitemporalInciso.get(0).getId().toString(),new Date(),"Inicia Asiganció Poliza", null,keyIteracion));
													
													this.asignarInciso(
														reporte.getId(),
														new Long ( lPolizaDto.get(0).getIdToPoliza().toString() ),  
														incisoContinuity.getId(), 
														bitemporalInciso.getValidityInterval().getInterval().getStart().toDate(),
														bitemporalInciso.getRecordInterval().getInterval().getStart() .toDate(),
														reporte.getFechaHoraOcurrido()
													);
												}else{
													this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(Long.valueOf(reporte.getIdToSolicitud()),new Long(lIncisoCot.size()),lBitemporalInciso.get(0).getId().toString(),new Date(),"BitemporalInciso Vacio", null,keyIteracion));
												}
												
												this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(Long.valueOf(reporte.getIdToSolicitud()),new Long(lIncisoCot.size()),lBitemporalInciso.get(0).getId().toString(),new Date(),"Se ejecuto proceso de asignación Poliza, se notifica a usuario ", null,keyIteracion));
												try{ this.notificaResponsables(horas,(short) 3, reporte, lPolizaDto.get(0), reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina()); }catch(Exception e){ log.error("Error notificacion asignación poliza: "+e); }
												
											}else{
												
												//# SALVA LOG			
												this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(Long.valueOf(reporte.getIdToSolicitud()),new Long(lIncisoCot.size()),null,new Date(),"Bitemporales Vacios", null,keyIteracion));
												
												try{ this.notificaResponsables(horas,(short) 1, reporte, null, reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina()); }catch(Exception e){ log.error("Error bitemporales vacios: "+e); }
											}
											
									} catch (NumberFormatException e) {
										log.error("Number exception error metodo asignacion poliza "+e);
									} catch (Exception e) {
										log.error("Excepción general asignacion poliza "+e);
										//# SALVA LOG			
										this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(Long.valueOf(reporte.getIdToSolicitud()),null,null,new Date(),"ERROR: Excepción general asignacion poliza: "+this.imprimirTrazaError(e), null,keyIteracion));
									}
								}
								
							}else{ // # CIERRA !lIncisoCot.isEmpty()
								// NOTIFICA ERROR A REGLA 1
								try{ this.notificaResponsables(horas,(short) 1, reporte, null, reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina()); }catch(Exception e){ log.error("Error regla 1: "+e); }
							}
						
						}else{
							// NOTIFICA ERROR A REGLA 1
							try{ this.notificaResponsables(horas,(short) 1, reporte, null, reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina()); }catch(Exception e){ log.error("Error regla 1: "+e); }
						}
					}else{
						// NOTIFICA ERROR A REGLA 1
						try{ this.notificaResponsables(horas,(short) 1, reporte, null, reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina()); }catch(Exception e){ log.error("Error regla 1: "+e); }
						// # SALVA LOG			
						try{ this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(Long.valueOf(reporte.getIdToSolicitud()),null,null,new Date(),"Sin Poliza",null,keyIteracion)); }catch(Exception e){ log.error("Error log 1: "+e); }
					}
				}else{
					// NOTIFICA ERROR A REGLA 1
					try{ this.notificaResponsables(horas,(short) 1, reporte, null, reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina()); }catch(Exception e){ log.error("Error regla 1: "+e); }
					// # SALVA LOG			
					try{ this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(Long.valueOf(reporte.getIdToSolicitud()),null,null,new Date(),"Sin Solicitud",null,keyIteracion)); }catch(Exception e){ log.error("Error log 1: "+e); }
				}

			}
		}else{
			// # SALVA LOG
			this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(null,null,null,new Date(),"Reporte Cabina SIN datos a procesar",null,keyCron));
		}

	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private Long salvarLogMonitorEmision( MonitorEmisionPolizaLog monitorEmisionPolizaLog ){
		
		try{
			return (Long) this.entidadService.saveAndGetId(monitorEmisionPolizaLog);
		}catch(Exception e){
			log.error("Error salvar Log: "+e);
			return new Long(0);
		}
	}
	
	private boolean validarIncisos( Collection<IncisoContinuity> lIncisoCot ){
		
		// # AGREGAR A UN MAPA TODOS LOS NUMERO DE SERIE COMO INDICE Y VALIDAR LOS TAMAÑOS DEL LIST VS INDICE
		// # LOS MAP NO PERMITEN INDICES REPETIDOS, SI EL TAMAÑO SON DIFERENTES SE REGRESA FALSE
		
		boolean bandera = false;
		
		if( !lIncisoCot.isEmpty() ){
			
			if( lIncisoCot.size() == 1 ){
				bandera = true;
			}else{
				int tempSize = lIncisoCot.size();
				Map<Object,Object> tempMap = new HashMap<Object,Object>();
				for( IncisoContinuity lInc : lIncisoCot ){
					tempMap.put( lInc.getAutoIncisoContinuity().getBitemporalProperty().get().getValue().getNumeroSerie() , lInc.getId());
				}
				if( tempMap.size() != tempSize ){
					bandera = false;
				}else{
					bandera = true;
				}
			}
		}
		
		return bandera;
	}
	
	private void notificaResponsables(Double horas,short tipoError, ReporteCabina reporte,PolizaDTO poliza, AutoIncisoReporteCabina auto){
		
		try{
		
			switch( tipoError ){
				case 1:
					if ( horas >= TRANSCURRIDAS_24HRS & horas < TRANSCURRIDAS_48HRS){
						this.envioNotificacionesService.notificacionCartaCoberturaSinPoliza(reporte, reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina(),"");
					}else if ( horas >= TRANSCURRIDAS_48HRS & horas < TRANSCURRIDAS_72HRS){
						this.envioNotificacionesService.notificacionCartaCoberturaSinPoliza(reporte, reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina(),"_SUPERVISOR");
						this.envioNotificacionesService.notificacionCartaCoberturaSinPoliza(reporte, reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina(), "_48HRS");
					}else if (horas >= TRANSCURRIDAS_72HRS){
						this.envioNotificacionesService.notificacionCartaCoberturaSinPoliza(reporte, reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina(), "_72HRS");
					}
				break;
				case 2:
					if ( horas >= TRANSCURRIDAS_24HRS & horas <= TRANSCURRIDAS_48HRS){
						this.envioNotificacionesService.notificacionCartaCoberturaVariosIncisos(reporte, poliza, reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina(),"");
					}else if ( horas > 48){
						this.envioNotificacionesService.notificacionCartaCoberturaVariosIncisos(reporte, poliza, reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina(),"_SUPERVISOR");
					}
				break;
				case 3:
					this.envioNotificacionesService.notificacionCartaCobertura(reporte, poliza, reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina());
				break;
			}
		
		}catch(Exception e){
			log.error("Error en método general de notificaciones: "+e);
			try{ this.salvarLogMonitorEmision(new MonitorEmisionPolizaLog(Long.valueOf(reporte.getIdToSolicitud()),null,null,new Date(),"Error al enviar correo: "+this.imprimirTrazaError(e),null,null)); }catch(Exception ex){ log.error("Error log 1: "+ex); }
		}
		
	}
	
	private String imprimirTrazaError(Exception e){
		
		Writer writer = new StringWriter();
		
		try{
		
			PrintWriter printWriter = new PrintWriter(writer);
			e.printStackTrace(printWriter);
			
		}catch(Exception ex){
			log.error("Error imprimit Traza Error: "+ex);
		}
		
		return writer.toString();
		
	}
	
	/**
	 * Recibe una fecha, se la resta a la de hoy y retorna el resultado en horas
	 * @param fechaInsercion
	 * @return
	 */
	private double getDiferenciaHoras(Date fechaInsercion){
		Date hoy = new Date();

		if ( hoy.equals(fechaInsercion) ){
			return 0;
		}else{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	        
	        try{
	        	hoy = sdf.parse(sdf.format(hoy));
	        	fechaInsercion = sdf.parse(sdf.format(fechaInsercion));
	        }catch(Exception e){}

			long diferencia = hoy.getTime()- fechaInsercion.getTime();
			
			return (double)diferencia / (1000 * 60 * 60);
		}
		
	}
	
	private String getEstatusDescInciso(Date fechaFinVigencia, Date fechaOcurridoSiniestro) {
		if (fechaFinVigencia.compareTo(fechaOcurridoSiniestro) >= 0) {
			return ESTATUS_INCISO_VIGENTE;
		} else {
			return ESTATUS_INCISO_VENCIDA;
		}
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Bitemporal getCurrentBitemporal(EntidadContinuity continuity, DateTime validOn, DateTime knownOn, BigDecimal idToCotizacion ) {
		Bitemporal bitemporal = null;		
		
		bitemporal = continuity.getBitemporalProperty().get( new DateTime(validOn) , new DateTime(knownOn) );
		
		//ofal:Si no encuentra un bitemporal vigente, busca por el ultimo bitemporal conocido, aunque este no este vigente.
		if(bitemporal == null) 		
		{
			bitemporal = continuity.getBitemporalProperty().getLatestKnownAfterValidityInterval( new DateTime(validOn) , new DateTime(knownOn) );			
		}
	
		return bitemporal;
	}
	
	
	private List<ControlDinamicoRiesgoDTO> getDatosRiesgo(Long incisoContinuityId, Long coberturaId, 
			Date validOn, Date knownOn, Date fechaReporteSiniestro) {
		LogDeMidasInterfaz.log("Entrando a getDatosRiesgo1", Level.INFO, null);

		List<ControlDinamicoRiesgoDTO> controles = new ArrayList<ControlDinamicoRiesgoDTO>();

		Collection<BitemporalDatoSeccion> bitemporalesDatosSeccion = new ArrayList<BitemporalDatoSeccion>();

		Collection<BitemporalCoberturaSeccion> biCoberturaSeccionList =new ArrayList<BitemporalCoberturaSeccion>();

		try{

			BitemporalInciso biInciso =  incisoViewService.getInciso(incisoContinuityId, validOn, knownOn);

			if(biInciso != null){

				BigDecimal idToCotizacion = new BigDecimal(biInciso.getContinuity().getCotizacionContinuity().getBusinessKey());
				CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);

				List<RamoTipoPolizaDTO> ramos = cotizacion.getTipoPolizaDTO().getRamoTipoPolizaList();

				List<SubRamoDTO> subRamos = obtieneSubRamosInciso(biInciso.getContinuity().getId());

				List<ConfiguracionDatoInciso> configuracion = 
					configuracionDatoIncisoDao.getConfiguracionDatosIncisoCotAuto(ConfiguracionDatoInciso.Estatus.TODOS.getEstatus());



				Collection<SeccionIncisoContinuity> incisosSeccion = entidadContinuityService.findContinuitiesByParentKey(SeccionIncisoContinuity.class, 
						SeccionIncisoContinuity.PARENT_KEY_NAME, incisoContinuityId);

				for (SeccionIncisoContinuity seccionIncisoContinuity: incisosSeccion) {
					BitemporalSeccionInciso bitemporalSeccionInciso = (BitemporalSeccionInciso) getCurrentBitemporal(seccionIncisoContinuity, 
							new DateTime(fechaReporteSiniestro), biInciso.getRecordInterval().getInterval().getStart()/*ofal*/, idToCotizacion);

					if (bitemporalSeccionInciso != null) {
						Collection<DatoSeccionContinuity> datosSeccionContinuities = entidadContinuityService.findContinuitiesByParentKey(
								DatoSeccionContinuity.class, DatoSeccionContinuity.PARENT_KEY_NAME, 
								bitemporalSeccionInciso.getContinuity().getId());

						for (DatoSeccionContinuity datoSeccionContinuity : datosSeccionContinuities) {
							BitemporalDatoSeccion  bitemporalDatoSeccion = (BitemporalDatoSeccion) getCurrentBitemporal(
									datoSeccionContinuity, new DateTime(fechaReporteSiniestro),
									biInciso.getRecordInterval().getInterval().getStart()/*ofal*/, idToCotizacion);

							if (bitemporalDatoSeccion != null) {
								bitemporalesDatosSeccion.add(bitemporalDatoSeccion);						
							}
						}

						Collection<CoberturaSeccionContinuity> coberturasSeccion = entidadContinuityService.findContinuitiesByParentKey(
								CoberturaSeccionContinuity.class, CoberturaSeccionContinuity.PARENT_KEY_NAME, bitemporalSeccionInciso.getContinuity().getId());

						for (CoberturaSeccionContinuity coberturaSeccionContinuity : coberturasSeccion) {
							BitemporalCoberturaSeccion  bitemporalCobertura = (BitemporalCoberturaSeccion) getCurrentBitemporal(
									coberturaSeccionContinuity, new DateTime(fechaReporteSiniestro), 
									biInciso.getRecordInterval().getInterval().getStart()/*ofal*/, idToCotizacion);

							if (bitemporalCobertura != null && bitemporalCobertura.getValue().getClaveContrato() == 1) {
								biCoberturaSeccionList.add(bitemporalCobertura);						
							}
						}
					}


					validaConfiguracionDatosRiesgo(controles, configuracion, bitemporalesDatosSeccion, biCoberturaSeccionList, ramos, 
							subRamos, biInciso, bitemporalSeccionInciso, validOn, fechaReporteSiniestro, coberturaId);
				}
			}

		} catch(Exception ex) {
			ex.printStackTrace();
		}

		LogDeMidasInterfaz.log("Saliendo de getDatosRiesgo1", Level.INFO, null);
		return controles;
	}




	private void validaConfiguracionDatosRiesgo(List<ControlDinamicoRiesgoDTO> controles, 
			List<ConfiguracionDatoInciso> configuracionPrincipal, 
			Collection<BitemporalDatoSeccion> datoRiesgoCotizacion,
			Collection<BitemporalCoberturaSeccion> lstBiCobertura,
			List<RamoTipoPolizaDTO> ramos, 
			List<SubRamoDTO> subRamos,
			BitemporalInciso biInciso, 
			BitemporalSeccionInciso biSeccionInciso,
			Date validOn, Date fechaReporteSiniestro, Long coberturaId) {
		LogDeMidasInterfaz.log("Entrando a validaConfiguracionDatosRiesgo", Level.INFO, null);

		List<ConfiguracionDatoInciso> configuraciones = null;
		Collection<BitemporalDatoSeccion> datosIncisoCotAuto = null;	


		for (RamoTipoPolizaDTO ramo : ramos) {	

			if (coberturaId == null) {
				configuraciones = encontrarConfiguracion(configuracionPrincipal, BigDecimal.valueOf(0d), 
						ramo.getId().getIdtcramo(), BigDecimal.valueOf(0d), BigDecimal.valueOf(0d));
				if (configuraciones != null) {

					for (ConfiguracionDatoInciso configuracion : configuraciones) {
						datosIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, 
								biSeccionInciso.getContinuity().getSeccion().getIdToSeccion(), 
								biSeccionInciso.getContinuity().getIncisoContinuity().getNumero(), 
								BigDecimal.valueOf(0d), 
								ramo.getId().getIdtcramo(), 
								BigDecimal.valueOf(0d), BigDecimal.valueOf(0d), configuracion.getId().getIdDato());

						crearControles(controles, configuracion, datosIncisoCotAuto, 
								ramo.getRamoDTO().getDescripcion(), biInciso, validOn,fechaReporteSiniestro);		
					}
				}
			}


			//
			for (SubRamoDTO subRamo : subRamos) {
				//
				if (coberturaId == null) {
					configuraciones = encontrarConfiguracion(configuracionPrincipal, BigDecimal.valueOf(0d), 
							ramo.getId().getIdtcramo(), subRamo.getIdTcSubRamo(), BigDecimal.valueOf(0d));
					//
					if (configuraciones != null) {
						for (ConfiguracionDatoInciso configuracion : configuraciones) {
							//
							datosIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, 
									biSeccionInciso.getContinuity().getSeccion().getIdToSeccion(), 
									biSeccionInciso.getContinuity().getIncisoContinuity().getNumero(), 
									BigDecimal.valueOf(0d), ramo.getId().getIdtcramo(), 
									subRamo.getIdTcSubRamo(), BigDecimal.valueOf(0d), 
									configuracion.getId().getIdDato());
							//
							crearControles(controles, configuracion, datosIncisoCotAuto, 
									subRamo.getDescripcionSubRamo(), biInciso, validOn, fechaReporteSiniestro);	
						}
					}	
				}
			}

			//
			for (BitemporalCoberturaSeccion biCobertura: lstBiCobertura) {
				if (biCobertura.getContinuity().getCoberturaDTO() != null && 
						biCobertura.getValue().getClaveContrato().equals((short) 1)) {
					CoberturaDTO cobertura = biCobertura.getContinuity().getCoberturaDTO();

					if (coberturaId == null || (coberturaId != null && coberturaId == cobertura.getIdToCobertura().longValue())) {
						BigDecimal idSubRamo =  BigDecimal.valueOf(biCobertura.getValue().getSubRamoId());
						configuraciones = encontrarConfiguracion(configuracionPrincipal, cobertura.getIdToCobertura(), 
								ramo.getId().getIdtcramo(), idSubRamo, BigDecimal.valueOf(0d));
						if (configuraciones != null) {
							for (ConfiguracionDatoInciso configuracion : configuraciones) {
								datosIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, 
										biCobertura.getContinuity().getSeccionIncisoContinuity().getSeccion().getIdToSeccion(), 
										biCobertura.getContinuity().getSeccionIncisoContinuity().getIncisoContinuity().getNumero(), 
										cobertura.getIdToCobertura(), ramo.getId().getIdtcramo(), 
										idSubRamo, BigDecimal.valueOf(0d), configuracion.getId().getIdDato());
								crearControles(controles, configuracion, datosIncisoCotAuto, 
										cobertura.getNombreComercial(), biInciso, validOn, fechaReporteSiniestro);													
							}
						}		
					}				

				}				
			}			
		}			

		LogDeMidasInterfaz.log("Saliendo de validaConfiguracionDatosRiesgo", Level.INFO, null);
	}


	private List<SubRamoDTO> obtieneSubRamosInciso(Long incisoContinuityId){
		final String queryString = "select tc.idTcSubRamo, tc.idTcRamo, tc.codigoSubRamo, tc.descripcionSubRamo, tc.porcentajeMaxComisionRo, " +
		" tc.porcentajeMaxComisionRci, tc.porcentajeMaxComisionPrr, tc.idCuentaContable, tc.idSubCuentaContable " +
		" from MIDAS.TCSUBRAMO tc " +
		" where tc.idTcSubRamo in (select distinct mcsb.subramo_id " +
		" from MIDAS.MCOBERTURASECCIONB mcsb, MCOBERTURASECCIONC mcsc, MSECCIONINCISOC msic " +
		" where mcsb.mcoberturaseccionc_id = mcsc.id " +
		" and mcsc.mseccionincisoc_id = msic.id " +
		" and msic.mincisoc_id = "+incisoContinuityId+")";

		List<SubRamoDTO> subRamos = new ArrayList<SubRamoDTO>(1);
		Object result = entidadService.executeNativeQueryMultipleResult(queryString);
		if(result instanceof List)  {
			List<Object> listaResultados = (List<Object>) result;				
			for(Object object : listaResultados){
				Object[] singleResult = (Object[]) object;

				SubRamoDTO subRamo = new SubRamoDTO();
				try{
					BigDecimal idTcSubRamo = null;
					if(singleResult[0] instanceof BigDecimal)
						idTcSubRamo = (BigDecimal)singleResult[0];
					else if (singleResult[0] instanceof Long)
						idTcSubRamo = BigDecimal.valueOf((Long)singleResult[0]);
					else if (singleResult[0] instanceof Double)
						idTcSubRamo = BigDecimal.valueOf((Double)singleResult[0]);

					subRamo.setIdTcSubRamo(idTcSubRamo);
				}catch(Exception e){}
				try{
					RamoDTO ramoDTO = new RamoDTO();
					BigDecimal value = null;
					if(singleResult[1] instanceof BigDecimal)
						value = (BigDecimal)singleResult[1];
					else if (singleResult[1] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[1]);
					else if (singleResult[1] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[1]);

					ramoDTO.setIdTcRamo(value);
					subRamo.setRamoDTO(ramoDTO);
				}catch(Exception e){}
				try{
					BigDecimal value = null;
					if(singleResult[2] instanceof BigDecimal)
						value = (BigDecimal)singleResult[2];
					else if (singleResult[2] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[2]);
					else if (singleResult[2] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[2]);

					subRamo.setCodigoSubRamo(value);
				}catch(Exception e){}
				try{
					String value = singleResult[3].toString();
					subRamo.setDescripcionSubRamo(value);
				}catch(Exception e){}
				try{
					BigDecimal value = null;
					if(singleResult[4] instanceof BigDecimal)
						value = (BigDecimal)singleResult[4];
					else if (singleResult[4] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[4]);
					else if (singleResult[4] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[4]);

					subRamo.setPorcentajeMaximoComisionRO(value.doubleValue());
				}catch(Exception e){}
				try{
					BigDecimal value = null;
					if(singleResult[5] instanceof BigDecimal)
						value = (BigDecimal)singleResult[5];
					else if (singleResult[5] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[5]);
					else if (singleResult[5] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[5]);

					subRamo.setPorcentajeMaximoComisionRCI(value.doubleValue());
				}catch(Exception e){}
				try{
					BigDecimal value = null;
					if(singleResult[6] instanceof BigDecimal)
						value = (BigDecimal)singleResult[6];
					else if (singleResult[6] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[6]);
					else if (singleResult[6] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[6]);

					subRamo.setPorcentajeMaximoComisionPRR(value.doubleValue());
				}catch(Exception e){}
				try{
					BigDecimal value = null;
					if(singleResult[7] instanceof BigDecimal)
						value = (BigDecimal)singleResult[7];
					else if (singleResult[7] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[7]);
					else if (singleResult[7] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[7]);

					subRamo.setIdCuentaContable(value);
				}catch(Exception e){}
				try{
					BigDecimal value = null;
					if(singleResult[8] instanceof BigDecimal)
						value = (BigDecimal)singleResult[8];
					else if (singleResult[8] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[8]);
					else if (singleResult[8] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[8]);

					subRamo.setIdSubCuentaContable(value);
				}catch(Exception e){}
				subRamos.add(subRamo);
			}
		}


		return subRamos;
	}

	@SuppressWarnings("unchecked")
	private List<ConfiguracionDatoInciso> encontrarConfiguracion(List<ConfiguracionDatoInciso> configuracion, final BigDecimal idCobertura, 
			final BigDecimal idRamo, final BigDecimal idSubRamo, final BigDecimal claveDetalle){				
		Predicate predicate = new Predicate() {			
			@Override
			public boolean evaluate(Object arg0) {
				ConfiguracionDatoInciso config = (ConfiguracionDatoInciso) arg0;
				ConfiguracionDatoIncisoId id  = config.getId();
				return id.getIdTcRamo().longValue() == idRamo.longValue() && id.getIdTcSubRamo().longValue() == idSubRamo.longValue() &&
				id.getClaveDetalle().longValue() == claveDetalle.longValue() &&	id.getIdToCobertura().longValue() == idCobertura.longValue();
			}
		};		
		return (List<ConfiguracionDatoInciso>)CollectionUtils.select(configuracion, predicate);	

	}


	@SuppressWarnings("unchecked")
	private Collection<BitemporalDatoSeccion> encontrarRiesgoParaConfiguracion( Collection<BitemporalDatoSeccion> datoRiesgoCotizacion, final BigDecimal idSeccion, final Integer numInciso, 
			final BigDecimal idCobertura, final BigDecimal idRamo, final BigDecimal idSubRamo, final BigDecimal claveDetalle, final BigDecimal idDato){		
		Predicate predicate = new Predicate() {			
			@Override
			public boolean evaluate(Object arg0) {
				BitemporalDatoSeccion datoRiesgo = (BitemporalDatoSeccion) arg0;

				return datoRiesgo.getContinuity().getRamoId() == idRamo.longValue() && datoRiesgo.getContinuity().getSubRamoId() == idSubRamo.longValue() &&
				datoRiesgo.getContinuity().getClaveDetalle() == claveDetalle.longValue() && datoRiesgo.getContinuity().getDatoId() == idDato.longValue() &&
				datoRiesgo.getContinuity().getSeccionIncisoContinuity().getSeccion().getIdToSeccion() == idSeccion && 
				datoRiesgo.getContinuity().getSeccionIncisoContinuity().getIncisoContinuity().getNumero() == numInciso.longValue() && 
				datoRiesgo.getContinuity().getCoberturaId() == idCobertura.longValue();
			}
		};		
		return (Collection<BitemporalDatoSeccion>) CollectionUtils.select(datoRiesgoCotizacion, predicate);

	}

	private void crearControles(List<ControlDinamicoRiesgoDTO> controles,
			ConfiguracionDatoInciso configuracion, Collection<BitemporalDatoSeccion> datos,
			String descripcion, BitemporalInciso biInciso, Date validOn, Date fechaReporteSiniestro) {		

		ControlDinamicoRiesgoDTO control = new ControlDinamicoRiesgoDTO();
		control.setDependencia(configuracion.getDescripcionClaseRemota());
		control.setEtiqueta(configuracion.getDescripcionEtiqueta());
		control.setId(getIdControl(configuracion));
		control.setTipoControl(convertirTipoControl(configuracion.getClaveTipoControl()));
		control.setTipoValidador(convertirTipoValidador(configuracion));
		control.setVisible(true);
		control.setIdGrupoValores(configuracion.getIdGrupo());
		control.setLista(obtenerLista(control.getTipoControl(), control.getDependencia(), biInciso, 
				validOn, fechaReporteSiniestro, configuracion.getIdGrupo()));
		control.setTipoDependencia(null);
		control.setDescripcionNivel(descripcion);
		control.setIdNivel(getIdNivel(configuracion));
		control.setDeshabilitado(true);


		if (datos != null) {
			for (BitemporalDatoSeccion dato : datos) {
				if (getIdControl(configuracion).equals(getIdControl(dato))) {
					control.setValor(dato.getValue().getValor());
				}
			}
		}
		controles.add(control);		
	}

	private Map<String, String> obtenerLista(Integer tipoControl, String dependencia, BitemporalInciso biInciso, 
			Date validoEn, Date fechaReporteSiniestro, Short idGrupo) {
		Map<String, String> lista = new LinkedHashMap<String, String>();
		if(tipoControl == ControlDinamicoRiesgoDTO.TIPO_CONTROL_SELECT_CATALOGO_PROPIO 
				|| tipoControl == ControlDinamicoRiesgoDTO.TIPO_CONTROL_SELECT_VALORES_FIJOS){
			try {
				serviceLocator = ServiceLocatorP.getInstance();
				@SuppressWarnings("rawtypes")
				MidasInterfaceBaseAuto beanBase = serviceLocator.getEJB(dependencia);
				@SuppressWarnings("rawtypes")
				List optionList = null;

				if(beanBase instanceof ConfiguracionIncisoGrupoProliberAJService 
						|| beanBase instanceof ConfiguracionIncisoGrupoProliberAVService){
					EstiloVehiculoId estiloId = new EstiloVehiculoId();

					BitemporalAutoInciso bAutoInciso = (BitemporalAutoInciso) getCurrentBitemporal(biInciso.getContinuity().getAutoIncisoContinuity(), 
							new DateTime(fechaReporteSiniestro), biInciso.getRecordInterval().getInterval().getStart()/*ofal*/,
							new BigDecimal(biInciso.getContinuity().getCotizacionContinuity().getNumero()));



					CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, 
							biInciso.getContinuity().getCotizacionContinuity().getNumero());

					EstiloVehiculoDTO estiloDTO = entidadService.findById(EstiloVehiculoDTO.class,estiloId);

					TarifaAgrupadorTarifa tarifaAgrupadorTarifa = 
						tarifaAgrupadorTarifaService.getPorNegocioAgrupadorTarifaSeccion(
								negocioTarifaService.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(
										new BigDecimal(bAutoInciso.getValue().getNegocioSeccionId()),
										cotizacion.getIdMoneda()));

					NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, 
							new BigDecimal(bAutoInciso.getValue().getNegocioSeccionId()));

					optionList = beanBase.findAll(new BigDecimal[]{new BigDecimal(
							tarifaAgrupadorTarifa.getId().getIdToAgrupadorTarifa()),
							new BigDecimal(tarifaAgrupadorTarifa.getId().getIdVerAgrupadorTarifa()),
							negocioSeccion.getSeccionDTO().getIdToSeccion(), cotizacion.getIdMoneda(), 
							estiloDTO.getIdTcTipoVehiculo()});
				}else if((beanBase instanceof CatalogoValorFijoFacadeRemote) && idGrupo != null){
					int grupoTipoCarga = idGrupo;
					optionList = ((CatalogoValorFijoFacadeRemote) beanBase).findByProperty("id.idGrupoValores", grupoTipoCarga);
				} else {
					optionList = beanBase.findAll();
				}

				lista = convertCacheableDTOListToMap(optionList);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return lista;
	}

	private Map<String, String> convertCacheableDTOListToMap(List<? extends CacheableDTO> list) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (CacheableDTO cacheableDTO : list) {
			Object id = cacheableDTO.getId();
			String strId = null;
			if (id instanceof BigDecimal) {
				strId = ((BigDecimal) id).toPlainString();
			} else if(id instanceof CatalogoValorFijoId){
				CatalogoValorFijoId catId = (CatalogoValorFijoId) id;
				try{
					strId = String.valueOf(catId.getIdDato());
				}catch(Exception e){
					strId = id.toString();
				}
			}else {
				strId = id.toString();
			}
			map.put(strId, cacheableDTO.getDescription());
		}
		return map;
	}

	private String getIdControl(ConfiguracionDatoInciso configuracionDatoInciso) {
		ConfiguracionDatoIncisoId id = configuracionDatoInciso.getId();
		String del = "_";
		String controlId = id.getIdTcRamo() + del
		+ id.getIdTcSubRamo() + del + id.getIdToCobertura() + del
		+ id.getClaveDetalle() + del + id.getIdDato();
		return controlId;
	}

	private String getIdNivel(ConfiguracionDatoInciso configuracionDatoInciso) {
		ConfiguracionDatoIncisoId id = configuracionDatoInciso.getId();
		String del = "_";
		String idNivel = id.getIdTcRamo() + del
		+ id.getIdTcSubRamo() + del + id.getIdToCobertura();
		return idNivel;
	}

	private Integer convertirTipoControl(Short tipoControl) {
		int tipo = 0;

		switch (tipoControl) {
		case 1:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_SELECT_CATALOGO_PROPIO;
			break;
		case 2:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_SELECT_VALORES_FIJOS;
			break;
		case 3:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_TEXTFIELD;
			break;
		case 4:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_TEXTAREA;
			break;
		case 5:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_CHECKBOX;
		}

		return tipo;
	}

	private Integer convertirTipoValidador(ConfiguracionDatoInciso configuracionDatoInciso) {
		Integer tipoValidador = null;
		Short validadorMidas = configuracionDatoInciso.getClaveTipoValidacion();
		if (validadorMidas != null && validadorMidas > 0) {
			switch (validadorMidas) {
			case 1:
				tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_CUATRO;
				break;
			case 2:
				tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_DOS;
				break;
			case 3:
				tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_DOS;
				break;
			case 4:
				tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_CUATRO;
				break;
			case 5:
				tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO;
				break;
			}
		}
		return tipoValidador;
	}

	private String getIdControl(BitemporalDatoSeccion dato) {
		String del = "_";

		String controlId = dato.getContinuity().getRamoId() + del
		+ dato.getContinuity().getSubRamoId()+ del + dato.getContinuity().getCoberturaId() + del
		+ dato.getContinuity().getClaveDetalle() + del + dato.getContinuity().getDatoId();

		return controlId;
	}
	
	public void initialize() {
			String timerInfo = "TimerEjecucionMonitorPoliza";
			cancelarTemporizador(timerInfo);
			iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.second(0);
				expression.minute(1);
				expression.hour(0);				
				expression.dayOfMonth("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerEjecucionMonitorPoliza", false));
				
				log.info("Timer TimerEjecucionMonitorPoliza configurado");
			} catch (Exception e) {
				log.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		log.info("Cancelar Timer TimerEjecucionMonitorPoliza");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			log.error("Error al detener Timer TimerEjecucionMonitorPoliza:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		actualizarPolizaSiniestro();
	}
	
	/**
	 * Obtiene un Map con las Coberturas asociadas a un Inciso de una Póliza ademas de una bandera para saber si imprimir UMA o DSMGVDF
	 */
	@Override
	public Map<String, Object> obtenerCoberturasIncisoMap( Long incisoContinuityId, DateTime validOn, DateTime knownOn, Date fechaReporteSiniestro){
		log.debug( "PolizaSiniestroService.obtenerCoberturasInciso :: incisoContinuityId:  " + incisoContinuityId + " validOn: " + validOn + " recordFrom: " + knownOn );
		List<BitemporalCoberturaSeccion> biCoberturaSeccionList =new ArrayList<BitemporalCoberturaSeccion>();
		NegocioCobPaqSeccion negocioCobPaqSeccion;
		Map<String, Object> mapReturn = new LinkedHashMap<String, Object>();
		boolean imprimirUMA = false;

		try{

			Map<String, Object> params = new LinkedHashMap<String, Object>();

			BitemporalInciso biInciso =  incisoViewService.getInciso(incisoContinuityId, validOn.toDate(), knownOn.toDate());
			
			CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, new BigDecimal(biInciso.getContinuity().getCotizacionContinuity().getNumero()));
	
			BitemporalAutoInciso bAutoInciso = (BitemporalAutoInciso) getCurrentBitemporal(biInciso.getContinuity().getAutoIncisoContinuity(), 
					new DateTime(fechaReporteSiniestro), biInciso.getRecordInterval().getInterval().getStart()/*ofal*/, cotizacion.getIdToCotizacion());
			
			/*BitemporalCotizacion  bCotizacion = (BitemporalCotizacion) getCurrentBitemporal(biInciso.getContinuity().getCotizacionContinuity(), 
					new DateTime(fechaReporteSiniestro), null, cotizacion.getIdToCotizacion());*/			
			
			Collection<SeccionIncisoContinuity> incisosSeccion = entidadContinuityService.findContinuitiesByParentKey(SeccionIncisoContinuity.class, 
					SeccionIncisoContinuity.PARENT_KEY_NAME, incisoContinuityId);
			
			imprimirUMA = listadoService.imprimirLeyendaUMA(cotizacion.getFechaInicioVigencia());
			
			for (SeccionIncisoContinuity seccionIncisoContinuity: incisosSeccion) {
				BitemporalSeccionInciso bitemporalSeccionInciso = (BitemporalSeccionInciso) getCurrentBitemporal(seccionIncisoContinuity, 
						new DateTime(fechaReporteSiniestro), biInciso.getRecordInterval().getInterval().getStart()/*ofal*/, cotizacion.getIdToCotizacion());
			
				if (bitemporalSeccionInciso != null) {
					Collection<CoberturaSeccionContinuity> coberturasSeccion = entidadContinuityService.findContinuitiesByParentKey(
							CoberturaSeccionContinuity.class, CoberturaSeccionContinuity.PARENT_KEY_NAME, bitemporalSeccionInciso.getContinuity().getId());
				
					for (CoberturaSeccionContinuity coberturaSeccionContinuity : coberturasSeccion) {
						BitemporalCoberturaSeccion  bitemporalCobertura = (BitemporalCoberturaSeccion) getCurrentBitemporal(
								coberturaSeccionContinuity, new DateTime(fechaReporteSiniestro), 
								biInciso.getRecordInterval().getInterval().getStart()/*ofal*/, cotizacion.getIdToCotizacion());
					
						if (bitemporalCobertura != null && bitemporalCobertura.getValue().getClaveContrato() == 1) {
							biCoberturaSeccionList.add(bitemporalCobertura);						
						}
					}
				}				
			}

			Collections.sort(biCoberturaSeccionList, new Comparator<BitemporalCoberturaSeccion>() {
				@Override
				public int compare(BitemporalCoberturaSeccion n1,
						BitemporalCoberturaSeccion n2) {
					return n1.getContinuity()
					.getCoberturaDTO()
					.getNumeroSecuencia()
					.compareTo(
							n2.getContinuity().getCoberturaDTO()
							.getNumeroSecuencia());
				}
			});
			
			params = new HashMap<String, Object>();
			params.put("id.idGrupoValores", Integer.valueOf(38));
			
			List<CatalogoValorFijoDTO> posiblesDeducibles = entidadService.findByProperties(CatalogoValorFijoDTO.class, params);


			for (BitemporalCoberturaSeccion coberturaSeccion : biCoberturaSeccionList) {
				negocioCobPaqSeccion = negocioCobPaqSeccionDAO.getLimitesSumaAseguradaPorCobertura(coberturaSeccion.getContinuity().getCoberturaDTO().getIdToCobertura(),bAutoInciso.getValue().getEstadoId(), 
						bAutoInciso.getValue().getMunicipioId(), cotizacion.getIdMoneda(), bAutoInciso.getValue().getNegocioPaqueteId());

				if (negocioCobPaqSeccion != null && negocioCobPaqSeccion.getValorSumaAseguradaMax() != null && negocioCobPaqSeccion.getValorSumaAseguradaMin() != null) {
					coberturaSeccion.getValue().setValorSumaAseguradaMax(negocioCobPaqSeccion.getValorSumaAseguradaMax());
					coberturaSeccion.getValue().setValorSumaAseguradaMin(negocioCobPaqSeccion.getValorSumaAseguradaMin());
				} else {
					coberturaSeccion.getValue().setValorSumaAseguradaMax(new Double("0"));
					coberturaSeccion.getValue().setValorSumaAseguradaMin(new Double("0"));
				}

				coberturaSeccion.getValue().setDescripcionDeducible(getDescripcionDeducible(posiblesDeducibles, coberturaSeccion.getContinuity().getCoberturaDTO().getClaveTipoDeducible()));
				
				if (imprimirUMA && (coberturaSeccion.getValue().getDescripcionDeducible() != null && coberturaSeccion.getValue().getDescripcionDeducible().indexOf(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF) >= 0)){
					coberturaSeccion.getValue().setDescripcionDeducible(coberturaSeccion.getValue().getDescripcionDeducible().replace(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF,
							ImpresionesServiceImpl.TIPO_VALOR_SA_UMA));
				}
			}
		}catch( Exception ex ){
			log.error(ex);
			ex.printStackTrace();
    	}
		
		mapReturn.put("biCoberturaSeccionList", biCoberturaSeccionList);
		mapReturn.put("imprimirUMA", imprimirUMA);
		
		return mapReturn;
	}

}
	


