package mx.com.afirme.midas2.service.impl.fortimax;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fortimax.DocumentoEntidadFortimaxDao;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentoEntidadFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.service.fortimax.DocumentoEntidadFortimaxService;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class DocumentoEntidadFortimaxServiceImpl implements DocumentoEntidadFortimaxService{
	private DocumentoEntidadFortimaxDao dao;
	@Override
	public void delete(Long arg0) throws MidasException {
		dao.delete(arg0);
	}

	@Override
	public Long delete(DocumentoEntidadFortimax arg0) throws MidasException {
		return dao.delete(arg0);
	}

	@Override
	public List<DocumentoEntidadFortimax> getListaDocumentosGuardadosPorEntidad(Long arg0, String arg1) throws MidasException {
		return dao.getListaDocumentosGuardadosPorEntidad(arg0, arg1);
	}

	@Override
	public List<DocumentoEntidadFortimax> getListaDocumentosGuardadosPorEntidadYCarpeta(Long arg0, String arg1, String arg2) throws MidasException {
		return dao.getListaDocumentosGuardadosPorEntidadYCarpeta(arg0, arg1, arg2);
	}

	@Override
	public Long save(DocumentoEntidadFortimax arg0) throws MidasException {
		return dao.save(arg0);
	}
	
	/**
	 * Sincroniza Fortimax vs Base de datos consultando el servicio de documentos de fortimax y comparando con los que se tiene en base de datos,
	 * actualizando si existe o no el documento.
	 * @param idRegistro id del agente, o cliente , o entidad en question.
	 * @param nombreAplicacion nombre de la aplicacion segun la entidad, por ejemplo AGENTES, o CLIENTES, revisar tcCatalogoAplicacionFortimax columna "nombreAplicacion", NO checar columna "nombreAplicacionFortimax"
	 * @param nombreCarpeta nombre de la carpeta de la entidad 
	 */
	public void sincronizarDocumentos(Long idRegistro,String nombreAplicacion,String nombreCarpeta) throws MidasException,Exception{
		dao.sincronizarDocumentos(idRegistro,nombreAplicacion,nombreCarpeta);
	}
	/**
	 * Metodo que obtiene los documentos pendientes por subir
	 * @param idRegistro
	 * @param nombreAplicacion
	 * @param nombreCarpeta
	 * @return
	 * @throws MidasException
	 */
	public List<DocumentoEntidadFortimax> getListaDocumentosFaltantes(Long idRegistro,String nombreAplicacion,String nombreCarpeta) throws MidasException{
		return dao.getListaDocumentosFaltantes(idRegistro,nombreAplicacion,nombreCarpeta);
	}
	
	public boolean existeEstructuraPorEntidadAplicacion(Long idRegistro,String nombreAplicacion) throws MidasException{
		return dao.existeEstructuraPorEntidadAplicacion(idRegistro,nombreAplicacion);
	}
	
	public boolean existeEstructuraPorEntidadAplicacionYCarpeta(Long idRegistro,String nombreAplicacion,String nombreCarpeta) throws MidasException{
		return dao.existeEstructuraPorEntidadAplicacionYCarpeta(idRegistro,nombreAplicacion,nombreCarpeta);
	}
	
	public List<DocumentoEntidadFortimax> findByFilters(DocumentoEntidadFortimax filtro) throws MidasException{
		return dao.findByFilters(filtro);
	}
	
	public void saveDocumentosAgrupados(DocumentosAgrupados documentos, Integer numeroRegistros, String nombreDocumento, String nombreAplicacion, String nombreCarpeta)throws MidasException{
		dao.saveDocumentosAgrupados(documentos, numeroRegistros, nombreDocumento, nombreAplicacion, nombreCarpeta);
	}
	
	public String sincronizarGrupoDocumentos(String nombreDocumento, String nombreAplicacion, String nombreCarpeta, DocumentosAgrupados documentos)throws MidasException{
		return dao.sincronizarGrupoDocumentos(nombreDocumento, nombreAplicacion, nombreCarpeta, documentos);
	}
	

	@Override
	public void auditarDocumentosEntregadosAnio(Long idAgente,Long anio,String nombreAplicacion) throws MidasException {
		dao.auditarDocumentosEntregadosAnio(idAgente, anio, nombreAplicacion);
	}
	
	@EJB
	public void setDao(DocumentoEntidadFortimaxDao dao) {
		this.dao = dao;
	}
}