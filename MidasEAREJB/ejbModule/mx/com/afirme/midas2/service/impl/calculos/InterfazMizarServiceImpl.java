package mx.com.afirme.midas2.service.impl.calculos;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeFacadeRemote;
import mx.com.afirme.midas2.dao.calculos.InterfazMizarDao;
import mx.com.afirme.midas2.domain.calculos.DetalleCalculoComisiones;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.dto.calculos.DetalleSolicitudCheque;
import mx.com.afirme.midas2.service.calculos.InterfazMizarService;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class InterfazMizarServiceImpl implements InterfazMizarService{
	

	
	/**
	 * Actualiza el estatus de una solicitud de cheque de MIZAR de un calculo, este cheque representa lo que se le va a pagar en comisiones
	 * a un agente.
	 * @param cheque
	 * @throws MidasException
	 */
	@Override
	public void actualizarSolitudChequeMizar(DetalleCalculoComisiones cheque) throws MidasException {
		dao.actualizarSolitudChequeMizar(cheque);
	}
	/**
	 * Ejecuta spInsHead en SQL Server el cheque de MIZAR-Cabecera
	 * @param prestamo
	 * @throws MidasException
	 */
	public void actualizarSolitudChequeMizarPrestamo(ConfigPrestamoAnticipo prestamo) throws MidasException{
		dao.actualizarSolitudChequeMizarPrestamo(prestamo);
	}
	/**
	 * Actualiza en MIZAR los detalles de una solicitud de cheque de un calculo, es decir, una solicitud de cheque puede tener
	 * al menos un o mas detalles de solicitud de cheque, estos se actualizan su estatus por medio de un stored procedure de SQLSERVER
	 * utilizando el datasource de Mizar
	 * @param cheque
	 * @param idCalculo
	 * @throws MidasException
	 */
	@Override
	public void actualizarDetalleSolitudChequeMizar(DetalleSolicitudCheque cheque,Long idSolicitudCheque,Long idCalculo,Long claveAgente)throws MidasException {
		dao.actualizarDetalleSolitudChequeMizar(cheque,idSolicitudCheque,idCalculo,claveAgente);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void importarSolicitudesChequesAMizar(Long idCalculo) throws MidasException {
		dao.importarSolicitudesChequesAMizar(idCalculo);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void preparaSolicitudChequeMizar(DetalleCalculoComisiones detalleCalculo) {
		try {
		
			Long claveAgente = (isNotNull(detalleCalculo.getAgente()) 
					&& isNotNull(detalleCalculo.getAgente().getIdAgente()))
					? detalleCalculo.getAgente().getIdAgente() : 0L;
			
			dao.actualizarSolitudChequeMizar(detalleCalculo);
								
			List<DetalleSolicitudCheque> detallesSolicitudCheque = solicitudChequeFacade.obtenerDetallePorSolicitudCheque(detalleCalculo.getIdSolicitudCheque());
			
			for (DetalleSolicitudCheque detalleSolicitudCheque : detallesSolicitudCheque) {
					
				dao.actualizarDetalleSolitudChequeMizar(detalleSolicitudCheque, detalleCalculo.getIdSolicitudCheque()
						, detalleCalculo.getCalculoComisiones().getId(), claveAgente);
				
			}
		
		} catch (Exception ex) {
			
			throw new RuntimeException(ex);
			
		}
		
	}

	@EJB
	private InterfazMizarDao dao;
	
	@EJB
	private SolicitudChequeFacadeRemote solicitudChequeFacade;
	
}
