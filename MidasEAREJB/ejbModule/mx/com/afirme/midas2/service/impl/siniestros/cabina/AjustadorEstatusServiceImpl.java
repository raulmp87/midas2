package mx.com.afirme.midas2.service.impl.siniestros.cabina;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.siniestros.cabina.AjustadorEstatusDao;
import mx.com.afirme.midas2.domain.Coordenadas;
import mx.com.afirme.midas2.domain.siniestros.cabina.AjustadorEstatus;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas.TipoLugar;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustador.TipoDisponibilidad;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.AjustadorEstatusDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.AjustadorEstatusService;
import mx.com.afirme.midas2.service.siniestros.catalogo.serviciosiniestro.ServicioSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.serviciosiniestro.ServicioSiniestroService.ServicioSiniestroFiltro;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.ajustador.DisponibilidadAjustadorService;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.ajustador.HorarioAjustadorService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.CollectionUtils;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

@Stateless
public class AjustadorEstatusServiceImpl implements AjustadorEstatusService{

	private static final double	GRADOS_RADIANES	= 180.0;
	private static final int	SEGUNDOS_GRADOS	= 60;
	private static final double	FACTOR_MILLASNAUTICAS_MILLAS	= 1.1515;
	private static final double	FACTOR_MILLAS_MILLASNAUTICAS	= 0.8684;
	private static final double	FACTOR_MILLAS_KILOMETROS		= 1.609344;
	private static final String NO_AJUSTADOR = "Sin ajustador";

	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private ServicioSiniestroService servicioSiniestroService;
	
	@EJB
	private AjustadorEstatusDao ajustadorEstatusDao;
	
	@EJB
	private DisponibilidadAjustadorService disponibilidadAjustador;

	@EJB
	private ParametroGlobalService parametroGlobalService;
	
	@EJB
	private HorarioAjustadorService horarioAjustadorService;
	
	@EJB
	private BitacoraService bitacoraService;
	
	/**
	 * Descripciones de la base de datos para encontrar las configuraciones de las rutas
	 *
	 */
	public static enum RUTAS_CONFIG_DESCRIPTIONS{
		MAX_AJUSTADORES
	}
	
	private static final char KILOMETRO = 'K';
	private static final char MILLAS_NAUTICAS = 'N';
	private static final String CARRETERA_DIRECCION = "carretera ";
	private static final String KILOMETRO_DIRECCION = "kilometro ";
	private static final String ESTATUS_ATENCION = "A";
	private static final String ESTATUS_DISPONIBLE = "D";
	private static final int MAX_RUTAS_AJUSTADORES = 3;
	private static final String PERMISO_SIN_LIMITE_REPORTES = "FN_M2_SN_No_Limita_Maximo_Num_Reportes";

	@Override
	public String obtenerStringDireccion(LugarSiniestroMidas lugar){
		StringBuilder lugarSiniestroBuilder = new StringBuilder();
        if(lugar.getZona().equals(LugarSiniestroMidas.TipoDireccion.CD.toString())){
                        lugarSiniestroBuilder.append(lugar.getCalleNumero() + ",");
                        if(!lugar.getIdColoniaCheck()){
                            if(lugar.getColonia() != null){            
                            	lugarSiniestroBuilder.append(lugar.getColonia().getDescripcion() + ",");
                            }
                            if(lugar.getCodigoPostal() != null){
                            	lugarSiniestroBuilder.append(lugar.getCodigoPostal() + ",");
                            }
                        }else{
                                        lugarSiniestroBuilder.append(lugar.getOtraColonia() + ",");
                                        lugarSiniestroBuilder.append(lugar.getCodigoPostalOtraColonia() + ",");
                        }
                        lugarSiniestroBuilder.append(lugar.getCiudad().getDescripcion() + ",");
                        lugarSiniestroBuilder.append(lugar.getEstado().getDescripcion() + ",");
                        lugarSiniestroBuilder.append(lugar.getPais().getDescripcion());
        }else{
                        lugarSiniestroBuilder.append(CARRETERA_DIRECCION + lugar.getNombreCarretera() + ",");
                        lugarSiniestroBuilder.append(KILOMETRO_DIRECCION + lugar.getKilometro() + ",");
                        lugarSiniestroBuilder.append(lugar.getCiudad().getDescripcion() + ",");
                        lugarSiniestroBuilder.append(lugar.getEstado().getDescripcion() + ",");
                        lugarSiniestroBuilder.append(lugar.getPais().getDescripcion());
        }
        
        return lugarSiniestroBuilder.toString();
	}

	/**
	 * Guardar coordenadas para un ajustador con identificador de Seycos. 
	 * Se mantiene la última coordenada, actualizando el registro en caso de existir.
	 * @param idAjustadorSeycos ID del ajustador en Seycos
	 * @param latitud
	 * @param longitud
	 * @return
	 */
	public void guardarAjustadorEstatusSeycos(Long idAjustadorSeycos, Double latitud, Double longitud){
		guardarAjustadorEstatusMovil(idAjustadorSeycos, latitud, longitud, false);
	}
	
	public void guardarAjustadorEstatusMovil(Long idAjustador, Double latitud, Double longitud, Boolean midas){
		//convertir idAjustadorSeycos a idServicioSiniestro (MIDAS);
		List<ServicioSiniestro> ajustadores;
		if(midas){
			ajustadores = entidadService.findByProperty(ServicioSiniestro.class, "id", idAjustador);
		} else{
			ajustadores = entidadService.findByProperty(ServicioSiniestro.class, "idSeycos", idAjustador);	
		}		
		if(ajustadores != null && ajustadores.size() > 0){
			ServicioSiniestro ajustadorMidas = ajustadores.get(0);
			//desactivar coordenadas anteriores
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ajustador.id", ajustadorMidas.getId());
			params.put("estatus", 1);
			List<AjustadorEstatus> ubicaciones = 
			entidadService.findByProperties(AjustadorEstatus.class, params);
			for(AjustadorEstatus estatus : ubicaciones){
				estatus.setEstatus(0);
				entidadService.save(estatus);
			}
			//guardar/actualizar coordenadas del ajustador midas
			guardarAjustadorEstatus(ajustadorMidas.getId(), latitud, longitud);
		}
	}
	
	
	@Override
	public Long guardarAjustadorEstatus(AjustadorEstatus ajustadorEstatus){
		Long idAjustadorEstatus = null;
		if(ajustadorEstatus != null){
			idAjustadorEstatus = (Long)entidadService.saveAndGetId(ajustadorEstatus);
		}
		return idAjustadorEstatus;
	}
	
	/**
	 * Obtiene o crea el AjustadorEstatus dependiendo del id del ajustador que se le pasa como parametro,
	 * actualiza su informacion y devuelve el AjustadorEstatus listo para guardar
	 * @param idAjustador
	 * @param latitud
	 * @param longitud
	 * @return
	 */
	private AjustadorEstatus crearAjustadorEstatus(Long idAjustador, Double latitud, Double longitud){
		AjustadorEstatus ajustadorEstatus = null;
			ajustadorEstatus = new AjustadorEstatus();
			ServicioSiniestro ajustador = new ServicioSiniestro();
			ajustador.setId(idAjustador);
			ajustadorEstatus.setAjustador(ajustador);
			ajustadorEstatus.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
			ajustadorEstatus.setFechaCreacion(new Date());
		ajustadorEstatus.setCoordenadas(crearCoordenadas(latitud, longitud));
		ajustadorEstatus.setFecha(new Date());
		ajustadorEstatus.setEstatus(1); //TODO valor pendiente debido a que el desarrollo de estatus todavia no esta
		ajustadorEstatus.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		ajustadorEstatus.setFechaModificacion(new Date());
		return ajustadorEstatus;
	}
	
	/**
	 * Crea un nuevo objeto Coordenadas con la longitud y latitud correspondientes
	 * @param latitud
	 * @param longitud
	 * @return
	 */
	private Coordenadas crearCoordenadas(Double latitud, Double longitud){
		Coordenadas coordenadas = new Coordenadas();
		coordenadas.setLatitud(latitud);
		coordenadas.setLongitud(longitud);
		return coordenadas;
	}
	
	
	@Override
	public Long guardarAjustadorEstatus(Long idAjustador, Double latitud, Double longitud) {
			AjustadorEstatus ajustadorEstatus = 
				crearAjustadorEstatus(idAjustador, latitud, longitud);
			Long idAjustadorEstatus = guardarAjustadorEstatus(ajustadorEstatus);
			return idAjustadorEstatus;
	}
	

	@Override
	public List<AjustadorEstatus> obtenerListaAjustadorEstatus(Long idOficina) {
		List<AjustadorEstatus> listaAjustadoresEstatus = new ArrayList<AjustadorEstatus>();
		ServicioSiniestroFiltro filtro = new ServicioSiniestroFiltro();
		filtro.setOficinaId(idOficina);
		Integer TIPO_AJUSTADOR = new Integer(1);
		filtro.setTipoServicio(TIPO_AJUSTADOR.shortValue());
		Integer ESTATUS_ACTIVO = new Integer(1);
		filtro.setEstatus(ESTATUS_ACTIVO);
		List<ServicioSiniestro> listaAjustadores = servicioSiniestroService.buscarServicioSiniestro(filtro);
		
		for(ServicioSiniestro ajustador: listaAjustadores){
			AjustadorEstatus ajustadorEstatus = crearAjustadorEstatus(ajustador);
			if(ajustadorEstatus != null){
				listaAjustadoresEstatus.add(ajustadorEstatus);
			}
		}
		return listaAjustadoresEstatus;
	}
	
	/**
	 * Obtiene de la base de datos un AjustadorEstatus
	 * @param ajustador
	 * @return
	 */
	public AjustadorEstatus crearAjustadorEstatus(ServicioSiniestro ajustador){
		LocalDate today = LocalDate.now();	
		return crearAjustadorEstatus(ajustador, today.toDate());
	}
	
	public AjustadorEstatus crearAjustadorEstatus(ServicioSiniestro ajustador, Date date){
		AjustadorEstatus ajustadorEstatus = ajustadorEstatusDao.obtenerUltimoAjustadorEstatus(ajustador.getId(), date);
		return ajustadorEstatus;
	}

		

	@Override
	public List<AjustadorEstatusDTO> obtenerListaAjustadorEstatusDTO(Long idOficina) {
		List<AjustadorEstatusDTO> listaInfoAjustadores = new ArrayList<AjustadorEstatusDTO>();
		List<AjustadorEstatus> listaAjustadores = new ArrayList<AjustadorEstatus>();
		listaAjustadores.addAll(this.obtenerDisponibilidadAjustadorEstatus( idOficina , TipoDisponibilidad.Normal));
		listaAjustadores.addAll(this.obtenerDisponibilidadAjustadorEstatus( idOficina , TipoDisponibilidad.Guardia));
		listaAjustadores.addAll(this.obtenerDisponibilidadAjustadorEstatus( idOficina , TipoDisponibilidad.Apoyo));		
		for(AjustadorEstatus ajustador: listaAjustadores){			
			listaInfoAjustadores.add(convertirAjustadorEstatus(ajustador, null, true));
		}
		return listaInfoAjustadores;
	}
	
	/**
	 * Genera la lista de ajustadores, los ordena y marca los primeros n para que se 
	 * calcule su ruta.
	 * En caso de que no esten establecidas las coordenadas del lugar de atencion, este
	 * metodo retorna null.
	 */
	public List<AjustadorEstatusDTO> obtenerAjustadoresParaCalcularRuta(Long idReporte, Long idOficina){
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class, idReporte);
		if(reporte.getLugarAtencion() != null && reporte.getLugarAtencion().getCoordenadas() != null){
			List<AjustadorEstatusDTO> ajustadores = obtenerListaAjustadorEstatusDTO(idOficina);
			ajustadores = calcularDistancias(reporte, ajustadores);
			Collections.sort(ajustadores, new AjustadorEstatusDTO());
			int maxNumRutasAjustadores= obtenerMaxNumRutasAjustadores();
			for(int i= 0; i < ajustadores.size(); i++){
				if(i < maxNumRutasAjustadores){
					ajustadores.get(i).setCalcularRuta(true);
				}else{
					ajustadores.get(i).setCalcularRuta(false);
				}
			}
			//marcarAjustadorAsignado(ajustadores, reporte);
			return ajustadores;
		}
		return null;
	}
	
	/**
	 * En caso de que el reporte ya tenga un ajustador asignado y se encuentra en el parametro de la lista de ajustadores 
	 * marca como true el boolean estaAsignado que se encuentra dentro del DTO
	 * @param ajustadores
	 * @param reporte
	 * @return
	 */
	@SuppressWarnings("unused")
	private List<AjustadorEstatusDTO> marcarAjustadorAsignado(List<AjustadorEstatusDTO> ajustadores, ReporteCabina reporte){
		Long idAjustadorReporte = 0L;
		if(reporte.getAjustador() != null){
			idAjustadorReporte = reporte.getAjustador().getId();
		}
		for(AjustadorEstatusDTO ajustadorDTO: ajustadores){
			AjustadorEstatus ajustadorInfo = entidadService.findById(AjustadorEstatus.class, ajustadorDTO.getId());
			if(ajustadorInfo.getAjustador().getId() == idAjustadorReporte){
				ajustadorDTO.setEstaAsignado(true);
			}else{
				ajustadorDTO.setEstaAsignado(false);
			}
		}
		return ajustadores;
	}
	
	/**
	 * Calcula la distancia entre el lugar de atencion del reporte y el ajustador, y lo guarda en el DTO
	 * @param reporte
	 * @param ajustadores
	 * @return
	 */
	private  List<AjustadorEstatusDTO> calcularDistancias(ReporteCabina reporte, List<AjustadorEstatusDTO> ajustadores){
		for(AjustadorEstatusDTO ajustador: ajustadores){
			ajustador.setDistanciaSiniestroKM(
					calcularDistancia(reporte.getLugarAtencion().getCoordenadas().getLatitud(), reporte.getLugarAtencion().getCoordenadas().getLongitud(),
							Double.parseDouble(ajustador.getLatitud()), Double.parseDouble(ajustador.getLongitud()), 'K'));
		}
		return ajustadores;
	}
	
	/**
	 * Obtener el numero maximo de ajustadores para los que se va a calcular la ruta por default
	 * @return
	 */
	private int obtenerMaxNumRutasAjustadores(){
		try{
			return Integer.parseInt(parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS, 
																					  ParametroGlobalService.MAX_REPORTES_AJUSTADOR, 
																					  String.valueOf(MAX_RUTAS_AJUSTADORES)));
		}catch(Exception e){
			return MAX_RUTAS_AJUSTADORES;
		}
	}
	
	
	/**
	 * Funcion que calcula la distancia entre dos coordenadas
	 * 
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @param unit
	 * @return
	 */
	private double calcularDistancia(double lat1, double lon1, double lat2,	double lon2, char unit) {
		double theta = lon1 - lon2;
		
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * SEGUNDOS_GRADOS * FACTOR_MILLASNAUTICAS_MILLAS;
		if (unit == KILOMETRO) {
			dist = dist * FACTOR_MILLAS_KILOMETROS;
		} else if (unit == MILLAS_NAUTICAS) {
			dist = dist * FACTOR_MILLAS_MILLASNAUTICAS;
		}
		return (dist);
	}

	/**
	 * Funcion que convierte grados a radianes
	 * 
	 * @param deg
	 * @return
	 */
	private double deg2rad(double deg) {
		return (deg * Math.PI / GRADOS_RADIANES);
	}

	/**
	 * Funcion que convierte radianes a grados
	 * 
	 * @param rad
	 * @return
	 */
	private double rad2deg(double rad) {
		return (rad * GRADOS_RADIANES / Math.PI);
	}
	
	@Override
	public List<AjustadorEstatus> obtenerDisponibilidadAjustadorEstatus(Long idOficina , TipoDisponibilidad disponibilidad ){
		Integer maxReportesAjustador = Integer.parseInt(parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS, ParametroGlobalService.MAX_REPORTES_AJUSTADOR));
		List<AjustadorEstatus> listaAjustadoresEstatus = new ArrayList<AjustadorEstatus>();		
		try{
		List<ServicioSiniestro> listaAjustadoresDisponibilidad = disponibilidadAjustador.obtenerAjustadoresConDisponibilidad(disponibilidad, idOficina);		
		for(ServicioSiniestro ajustador: listaAjustadoresDisponibilidad){
			List<ReporteEstatusDTO> reportesAsignados = this.obtenerResumenReportesAsignados(ajustador.getId(), null);
 			if( reportesAsignados.size() < maxReportesAjustador || usuarioService.tienePermisoUsuarioActual(PERMISO_SIN_LIMITE_REPORTES)){
				AjustadorEstatus ajustadorEstatus = crearAjustadorEstatus(ajustador);				
				if(ajustadorEstatus != null){					
					ajustadorEstatus.setDisponibilidad(disponibilidad.toString());
					listaAjustadoresEstatus.add(ajustadorEstatus);
				}
			}
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return listaAjustadoresEstatus;
	}
	
	@Override
	public List<ServicioSiniestro> obtenerDisponibilidadAjustador(Long idOficina , TipoDisponibilidad disponibilidad ){

		Integer maxReportesAjustador = Integer.parseInt(parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS, ParametroGlobalService.MAX_REPORTES_AJUSTADOR));		
		List<ServicioSiniestro> listaFinal = new ArrayList<ServicioSiniestro>();
		try{
			List<ServicioSiniestro> listaAjustadores = disponibilidadAjustador.obtenerAjustadoresConDisponibilidad(disponibilidad, idOficina);					
			for(ServicioSiniestro ajustador: listaAjustadores){
				List<ReporteEstatusDTO> reportesAsignados = this.obtenerResumenReportesAsignados(ajustador.getId(), null);
	 			if( reportesAsignados.size() < maxReportesAjustador || usuarioService.tienePermisoUsuarioActual(PERMISO_SIN_LIMITE_REPORTES)){
	 				if(reportesAsignados.size() > 0){
						ajustador.setEstatusAsignacion(ESTATUS_ATENCION);
					}else{
						ajustador.setEstatusAsignacion(ESTATUS_DISPONIBLE);
					}
					ajustador.setTipoDisponibilidad(disponibilidad.getCodigo());
	 				listaFinal.add(ajustador);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return listaFinal;
	}
	
	/*@Override
	public List<ReporteCabina> obtenerReportesAsignados( Long idAjustador ){
		List<ReporteCabina> reportesAsignadosRetornar = new ArrayList<ReporteCabina>(); 
//		List<ReporteCabina> reportesAsignados = entidadService.findByProperty(ReporteCabina.class, "ajustador.id", idAjustador);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ajustador.id", idAjustador);
		params.put("estatus", 1);
		List<ReporteCabina> reportesAsignados = entidadService.findByProperties(ReporteCabina.class, params);
		for(ReporteCabina reporte : reportesAsignados){
			if( reporte.getFechaHoraTerminacion() == null ){
				reportesAsignadosRetornar.add(reporte);
			}
		}
		
		return reportesAsignadosRetornar;
	}*/
	
	/**
	 * Retornar un listado de ajustadores estatus (coordenadas) por oficina. 
	 * El método retornará la lista de las últimas coordenadas reportada por un ajustador activo para la oficina selccionada. 
	 * @param idOficina id de la oficina selccionada
	 * @return
	 */
	@Override
	public List<AjustadorEstatusDTO> obtenerAjustadoresPorOficina(Long idOficina){
		List<AjustadorEstatusDTO> ubicaciones = new ArrayList<AjustadorEstatusDTO>();		
		List<AjustadorEstatus> ajustadoresEstatus = ajustadorEstatusDao.obtenerEstatusAjustadores(idOficina, null);
		for(AjustadorEstatus estatus : CollectionUtils.emptyIfNull(ajustadoresEstatus)){
			ubicaciones.add(convertirAjustadorEstatus(estatus, null, true));			
		}		
		return ubicaciones;
	}
	
	/**
	 * Retornar historico de ubicaciones por ajustador. 
	 * Regresa en orden descendente (fecha creacion) las distintas ubicaciones 
	 * que ha tenido un ajustador desde una fecha dada.
	 * @param idAjustador
	 * @param desde. En caso de ser nula se retornara todo lo del día
	 * @return
	 */
	@Override
	public List<AjustadorEstatusDTO> obtenerHistoricoPorAjustador(Long idAjustador, Date desde){
		List<AjustadorEstatusDTO> ubicaciones = new ArrayList<AjustadorEstatusDTO>();		
		if(desde == null){
			LocalDate today = LocalDate.now();
			desde = today.toDate();
		}			
		List<AjustadorEstatus> historico = ajustadorEstatusDao.obtenerHistoricoPorAjustador(idAjustador, desde);
		for(AjustadorEstatus ajustadorEstatus : CollectionUtils.emptyIfNull(historico)){					
				ubicaciones.add(convertirAjustadorEstatus(ajustadorEstatus, null, true));		
		}				
		return ubicaciones;
		
	}
	
	/**
	 * Convertir una entidad de ajustador estatus a un objeto de transporte ajustadorEstatusDTO. 
	 * Se complementa el objeto con la disponibilidad, reportes, y mensajes adicionales.
	 * @param estatus. Entidad a convertir
	 * @param reporteId. Si se envia un id de reporte sera eliminado de la búsqueda de reportes  
	 * @param obtenerInfoReportes. Bandera para definir si se deben buscar los reportes asignados para el ajustador
	 * @return AjustadorEstatusDTO
	 */
	private AjustadorEstatusDTO convertirAjustadorEstatus(AjustadorEstatus estatus, Long reporteId, boolean obtenerInfoReportes){
		AjustadorEstatusDTO ajustadorDTO = new AjustadorEstatusDTO();						
		ajustadorDTO.setId(estatus.getId());
		ajustadorDTO.setIdAjustador(estatus.getAjustador().getId());
		String nombre = estatus.getAjustador().getPersonaMidas().getNombre();
		ajustadorDTO.setNombreAjustador(nombre);		
		ajustadorDTO.setLatitud(estatus.getCoordenadas().getLatitud().toString());
		ajustadorDTO.setLongitud(estatus.getCoordenadas().getLongitud().toString());
		ajustadorDTO.setFechaReporteCoordenadas(estatus.getFecha());
		ajustadorDTO.setEstatus(estatus.getEstatus().intValue() == 1 ? Boolean.TRUE : Boolean.FALSE); 
		ajustadorDTO.setEstaAsignado(Boolean.FALSE);
		TipoDisponibilidad disponibilidad = horarioAjustadorService.obtenerDisponibilidadAjustadorEnFechaHora(estatus.getAjustador(), new Date());		
		ajustadorDTO.setDisponibilidad(disponibilidad != null ? disponibilidad.getLabel() : "No identificada");	
		if(obtenerInfoReportes){
			List<ReporteEstatusDTO> reportesAsignados = obtenerResumenReportesAsignados(estatus.getAjustador().getId(), reporteId);
			if(reportesAsignados != null){
				ajustadorDTO.setNumeroReportesAsignados(reportesAsignados.size());			
				ajustadorDTO.setEstaAsignado(reportesAsignados.size() > 0 ? Boolean.TRUE : Boolean.FALSE);	
				ajustadorDTO.setReportesAsignados(reportesAsignados);
			}
		}
		return ajustadorDTO;
	}

	/**
	 * Obtener un listado de reportes que el ajustador tiene asignados. 
	 * Si <code>reporteId</code> es diferente de <code>null</code> se excluira de la busqueda 
	 * @param ajustadorId
	 * @param reporteId
	 * @return
	 */
	@Override
	public List<ReporteEstatusDTO> obtenerResumenReportesAsignados(
			Long idAjustador, Long reporteId) {		
		return ajustadorEstatusDao.obtenerReportesAsignados(idAjustador, reporteId, null);		
	}
	
	

	@Override
	public List<ReporteEstatusDTO> obtenerResumenReportes(Long idOficina) {
		List<ReporteEstatusDTO> reportesSinAsignar = new ArrayList<AjustadorEstatusService.ReporteEstatusDTO>();
		List<ReporteEstatusDTO> reportes = ajustadorEstatusDao.obtenerReportesAsignados(null, null, idOficina);
		for(ReporteEstatusDTO reporte : CollectionUtils.emptyIfNull(reportes)){
			if(reporte.getAsignadoA().equals(NO_AJUSTADOR)){
				reportesSinAsignar.add(reporte);
			}
		}
		return reportesSinAsignar;
		
		
	}

	@Override
	public ReporteEstatusDTO obtenerInformacionReporte(Long reporteId) {
		return new ReporteEstatusDTO(entidadService.findById(ReporteCabina.class, reporteId));				
	}

	@Override
	public MensajeDTO eliminarHistoricoAjustadores(Date date) {
		MensajeDTO mensaje = new MensajeDTO();
		if(date == null){
			date = new Date();
		}		
		try{
			DateTime firstDayOfMonth = new DateTime(date).dayOfMonth().withMinimumValue().withTimeAtStartOfDay();
			
			ajustadorEstatusDao.eliminarHistoricoAjustadores(firstDayOfMonth.toDate());
			String msj = "Se eliminaron los registros";
			String id = usuarioService.getUsuarioActual().getNombreUsuario().concat(" - ").concat(date.toString());
			bitacoraService.registrar(TIPO_BITACORA.MONITORES,EVENTO.ELIMINAR_HISTORIAL_AJUSTADORES, 
					id , msj, null, usuarioService.getUsuarioActual().getNombreUsuario());
			mensaje.setMensaje(msj);
			mensaje.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
		}catch(Exception ex){
			mensaje.setMensaje("Ocurrio un error al realizar la eliminación: ".concat(ex.getMessage()));
			mensaje.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
		}
		return mensaje;
	}
	
	/**
	 * Obtener informacion ajustador asignado al reporte que haya hecho contacto con el lugar de atencion (haya registrado sus coordenadas de contacto)
	 * @param reporteId
	 * @param tipoLugar
	 * @return
	 */
	public AjustadorEstatusDTO obtenerAjustadorContactoMovil(Long reporteId, TipoLugar tipoLugar){
		AjustadorEstatusDTO ajustadorDTO = null;
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class, reporteId);
		if(reporte.getAjustador() != null){
			ajustadorDTO = new AjustadorEstatusDTO();
			ajustadorDTO.setIdAjustador(reporte.getAjustador().getId());
			ajustadorDTO.setNombreAjustador(reporte.getAjustador().getPersonaMidas().getNombre());		
			ajustadorDTO.setEstaAsignado(Boolean.TRUE);
			ajustadorDTO.setEstatus(Boolean.TRUE);
			Coordenadas coords = null;
			if(tipoLugar.equals(TipoLugar.AT)){				
				if(reporte.getLugarAtencion() != null && reporte.getLugarAtencion().getCoordenadasContacto() != null){
					coords = reporte.getLugarAtencion().getCoordenadasContacto();
					ajustadorDTO.setLatitud(coords.getLatitud().toString());
					ajustadorDTO.setLongitud(coords.getLongitud().toString());
					ajustadorDTO.setFechaReporteCoordenadas(reporte.getLugarAtencion().getFechaCoordenadasContacto());
				}
			}else{
				if(reporte.getLugarOcurrido() != null && reporte.getLugarOcurrido().getCoordenadasContacto() != null){
					coords = reporte.getLugarOcurrido().getCoordenadasContacto();
					ajustadorDTO.setLatitud(coords.getLatitud().toString());
					ajustadorDTO.setLongitud(coords.getLongitud().toString());
					ajustadorDTO.setFechaReporteCoordenadas(reporte.getLugarOcurrido().getFechaCoordenadasContacto());
				}
			}
		}		
		return ajustadorDTO;
	}
	
}
