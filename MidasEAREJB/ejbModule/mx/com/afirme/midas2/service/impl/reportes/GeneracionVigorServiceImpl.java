package mx.com.afirme.midas2.service.impl.reportes;


import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.danios.reportes.reportercs.ReporteRCSDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.reportes.reporteReservas.GeneracionVigorDao;
import mx.com.afirme.midas2.domain.reportes.reporteReservas.GeneracionVigorDTO;
import mx.com.afirme.midas2.service.reportes.GeneracionVigorService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MidasException;
 
@Stateless    
public class GeneracionVigorServiceImpl implements GeneracionVigorService{
	
	private static final Logger LOG = Logger.getLogger(GeneracionVigorServiceImpl.class);
	
	private GeneracionVigorDao generacionVigorDao;
		   
	@EJB
	public void setGeneracionSesasDao(GeneracionVigorDao generacionVigorDao) {
		this.generacionVigorDao = generacionVigorDao;
	}
	
	@EJB
	private SistemaContext sistemaContext;
	
	@Resource	
	private TimerService timerService;
	
	@Override
	public int generarReporte( Date fInicial, Date fFinal, String usuario ) throws MidasException {
		
		String spName = "SEYCOS.PKG_ARCHIVOS_RESERVAS.SP_VIGOR_REPORTE";
		StoredProcedureHelper storedHelper;
		int resp = 0;
		
		try { 
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceParametro( "FINI", getStringDate(fInicial) );
			storedHelper.estableceParametro( "FFIN", getStringDate(fFinal) );
			storedHelper.estableceParametro( "USUARIO", "E" );
			storedHelper.ejecutaActualizar();			
		}
		catch (Exception e) {
			LogDeMidasEJB3.log("Error en :" + e.getMessage() + " " + e.getCause(), Level.INFO, null);
		}

		return resp;
	}
	
	private String getStringDate( Date d ) throws Exception{
		
		String strDate = null;			
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        strDate = formatter.format(d);
		
		return strDate;
	}
	
	@Override
	public InputStream exportarReporte( Date fInicial, Date fFinal) throws MidasException {
		InputStream is;
		StoredProcedureHelper storedHelper;
		StringBuilder descripcionParametros = new StringBuilder();
		
		String nombreSP = "SEYCOS.PKG_ARCHIVOS_RESERVAS.SP_VIGOR_REPORTE_EXTRACION";
		
		try {
			
			String[] nombreParametros = {"FINI", 
					"FFIN",
					"USUARIO"};
			Object[] valorParametros={fInicial,
				fFinal,
				0};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					"registro",
					"registro");
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append(", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log("Consultando reporte bases emision. SP a invocar: "+nombreSP+", Parametros: "+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log("Finaliza consulta reporte bases emision. SP invocado: "+nombreSP+", Parametros: "+descripcionParametros.toString()+
					"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
			
			byte[] bytes;
			bytes = toArrayByte(listaResultado);
			is = new ByteArrayInputStream(bytes);
	        
	        return is;
	
		} catch (Exception e) {
			
			LogDeMidasEJB3.log("Hubo un error exportarReporte VIGOR: SELECT * FROM seycos.aux_rep_vigor " + e.getMessage() + " " + e.getCause(), Level.INFO, null);
			 return null;
	    } 
	}
	
	private byte[] toArrayByte( List<ReporteRCSDTO> list ) throws Exception {
		
		byte[] bytes;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(baos);
			StringBuilder sb = new StringBuilder();
			out.writeBytes("I_MONEDA|SUBRAMO|N_POLIZAX|N_ENDOSO|FEC_EMISION|INIPOL|F_VTO|INIEND|FINEND|P_NETA|PMA_RET|PMA_CED|DIAS_DEV|DIAS_RIE|PMA_DEV_RE|" +
		       		"PMA_DEV_CE|FAC_DEV|PORC_COMI|TIPO_CAMBIO|OFSUC|I_AGENTE|LIN_NEGOCIO|SOLICITANTE|" +
		       		"ID_PRODUCTO|NOM_PRODUCTO|CODIGOTIPOPOLIZA|NOMBRECOMERCIALTIPOPOLIZA|BONIF_COMIS|DERECHOS|" +
		       		"RECARGOS|IVA|COM_AGT_PF|COM_AGT_PM|COM_REC_PF|COM_REC_PM|COM_SUP_PM|COM_SUP_PF|PRIMA_COB_TECEROS|COM_DIREC_AGT|COSTO_ADQ_ADIC\n");
			if(list != null)
				for (ReporteRCSDTO item : list) {
					sb.append(item.getRegistro()+System.getProperty("line.separator"));
				}
			out.writeBytes(sb.toString());
			bytes = baos.toByteArray();
		
		return bytes;
	}
	
	@Override
	public int programarTarea(Date fInicial, Date fFinal, String arg2)
			throws MidasException {
		
		String spName = "SEYCOS.PKG_ARCHIVOS_RESERVAS.SP_PROGRAMAR_TAREA";
		StoredProcedureHelper storedHelper = null;
		
		try { 
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceParametro( "FINI", (fInicial) );
			storedHelper.estableceParametro( "FFIN", (fFinal) );
			storedHelper.estableceParametro( "USUARIO", arg2);
			storedHelper.ejecutaActualizar();
			
		}
		catch (Exception e) {
			LogDeMidasEJB3.log("Error en :" + e.getMessage() + " " + e.getCause(), Level.INFO, null);
		}
		
		return (storedHelper != null? storedHelper.getCodigoRespuesta() : 0);
	}

	@Override
	public List<GeneracionVigorDTO> getTareasVigor(BigDecimal tipoArchivo) throws MidasException {
		
		List<GeneracionVigorDTO> listGeneracionSesas;
		
		try{			
			listGeneracionSesas = generacionVigorDao.getListTareas(tipoArchivo);
		}
		catch( SQLException e){
			listGeneracionSesas = new ArrayList<GeneracionVigorDTO>();
			LogDeMidasInterfaz.log("Excepcion en BD de GeneracionVigorServiceImpl.obtenerLog" + this, Level.WARNING, e);
			onError(e);
			
		}
		catch ( Exception e ){
			listGeneracionSesas = new ArrayList<GeneracionVigorDTO>();
			LogDeMidasInterfaz.log("Excepcion general en GeneracionVigorServiceImpl.obtenerLog" + this, Level.WARNING, e);
			onError(e);
		}		
		return listGeneracionSesas;
	}

	@Override 
	public String getFechasReporte(int tipoReporte) {
		
		StoredProcedureHelper storedHelper;
		String nombreSP = "SEYCOS.PKG_ARCHIVOS_RESERVAS.SP_GET_FECHAS_REPORTE";
		StringBuilder descripcionParametros = new StringBuilder();
		try {
			
			String[] nombreParametros = {"FINI", 
					"FFIN",
					"TIPOREPORTE"};
			Object[] valorParametros={"",
				"",
				tipoReporte};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					"registro",
					"registro");
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append(", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log("Consultando reporte RESERVAS. SP a invocar: "+nombreSP+", Parametros: "+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log("Finaliza consulta reporte RESERVAS. SP invocado: "+nombreSP+", Parametros: "+descripcionParametros.toString()+
					"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
			
			if(listaResultado!= null && listaResultado.size() > 0) {
				return listaResultado.get(0).getRegistro();
			}	        
	        return "";
	
		} catch (Exception e) {
			LogDeMidasEJB3.log("Hubo un error SEYCOS.PKG_ARCHIVOS_RESERVAS.SP_GET_FECHAS_REPORTE " + e.getMessage() + " " + e.getCause(), Level.INFO, null);
			 return "";
	    } 
	}

	@Override
	public void initTareaProgramada() {
		
		 LogDeMidasEJB3.log("-Iniciando Ejecucion Reportes RESERVAS-", Level.INFO, null);
		 String spName = "SEYCOS.PKG_ARCHIVOS_RESERVAS.SP_EJECUCION_REPORTE";
			StoredProcedureHelper storedHelper = null;
			
			try { 
				storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
				
				storedHelper.estableceParametro( "FINI", " " );
				storedHelper.estableceParametro( "FFIN", "" );
				storedHelper.estableceParametro( "USUARIO", "E" );
				storedHelper.ejecutaActualizar();
				
				LogDeMidasEJB3.log("-Terminando Ejecucion Reportes RESERVAS-", Level.INFO, null);
			}
			catch (Exception e) {
				LogDeMidasEJB3.log("Error en JECL:" + e.getMessage() + " " + e.getCause(), Level.INFO, null);
			}
		
	}
	
	public void initialize() {
		String timerInfo = "TimerGeneracionVigor";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.minute(20);
				expression.hour(2);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerGeneracionVigor", false));
				
				LOG.info("Tarea TimerGeneracionVigor configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerGeneracionVigor");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerGeneracionVigor:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		initTareaProgramada();
	}
	
}
