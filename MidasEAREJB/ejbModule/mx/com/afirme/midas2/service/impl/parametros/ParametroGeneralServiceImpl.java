package mx.com.afirme.midas2.service.impl.parametros;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.parametros.ParametroGeneralDao;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;

@Stateless
public class ParametroGeneralServiceImpl implements ParametroGeneralService {
	
	@EJB
	private ParametroGeneralDao parametroGeneralDao;
	@EJB
	private EntidadDao entidadDao;

	@Override
	public ParametroGeneralDTO findById(ParametroGeneralId id){	
		
		ParametroGeneralDTO parametroGeneral = parametroGeneralDao.findById(id);
		
		return parametroGeneral;
	}

	@Override
	public List<ParametroGeneralDTO> listAll() {
		
		return entidadDao.findAll(ParametroGeneralDTO.class);
	}
	
	@Override
	public List<ParametroGeneralDTO> listByGroup(String groupName) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("grupoParametroGeneral.descripcionGrupoParametroGral", groupName);
		List<ParametroGeneralDTO> list = entidadDao.findByProperties(ParametroGeneralDTO.class, params);
		return list;
	}

	@Override
	public void save( ParametroGeneralDTO parametroGeneral ) {
		
		entidadDao.persist(this.fillParametroGeneralDTO(parametroGeneral));
	}
	
	private ParametroGeneralDTO fillParametroGeneralDTO( ParametroGeneralDTO parametroGeneral ){
		
		ParametroGeneralDTO paramObj = this.findById( parametroGeneral.getId() );
		paramObj.setValor(parametroGeneral.getValor());
		return paramObj;		
	}

	@Override
	public ParametroGeneralDTO findByDescCode(String groupName, String paramName) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("grupoParametroGeneral.descripcionGrupoParametroGral", groupName);
		params.put("descripcionParametroGeneral", paramName);
		List<ParametroGeneralDTO> list = entidadDao.findByProperties(ParametroGeneralDTO.class, params);
		if(list != null && !list.isEmpty()){
			return list.get(0);
		}
		return null;
	}

}
