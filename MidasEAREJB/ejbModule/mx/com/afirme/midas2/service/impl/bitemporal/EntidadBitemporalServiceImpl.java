package mx.com.afirme.midas2.service.impl.bitemporal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.bitemporal.BitemporalDao;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;

import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
@SuppressWarnings("rawtypes")
public class EntidadBitemporalServiceImpl implements EntidadBitemporalService {

	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> Collection<B> findByParentBusinessKey(
			Class<C> continuityEntityClass, String parentBusinessKeyName, K parentBusinessKey) {
		return findByParentBusinessKey(continuityEntityClass, parentBusinessKeyName, parentBusinessKey, TimeUtils.now());
	}

	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> Collection<B> findByParentBusinessKey(
			Class<C> continuityEntityClass, String parentBusinessKeyName, K parentBusinessKey, DateTime validoEn) {
		return findByParentContinuityProperty(continuityEntityClass, parentBusinessKeyName, parentBusinessKey, validoEn, false);
	}

	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> Collection<B> findByParentKey(
			Class<C> continuityEntityClass, String parentKeyName, K parentKey) {
		return findByParentKey(continuityEntityClass,parentKeyName, parentKey, TimeUtils.now());
	}

	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> Collection<B> findByParentKey(
			Class<C> continuityEntityClass,String parentKeyName, K parentKey, DateTime validoEn) {
		return findByParentContinuityProperty(continuityEntityClass, parentKeyName, parentKey, validoEn, false);
	}
	
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getByBusinessKey(Class<C> continuityEntityClass, String businessKeyName, K businessKey) {
		return getByBusinessKey(continuityEntityClass, businessKeyName,businessKey, TimeUtils.getDateTime(TimeUtils.now().toDate()));
	}

	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getByBusinessKey(
			Class<C> continuityEntityClass, String businessKeyName, K businessKey, DateTime validoEn) {
		LogDeMidasInterfaz.log("Entrando a getByBusinessKey", Level.INFO, null);
		
		LogDeMidasInterfaz.log("continuityEntityClass => " + continuityEntityClass, Level.INFO, null);
		LogDeMidasInterfaz.log("businessKeyName => " + businessKeyName, Level.INFO, null);
		LogDeMidasInterfaz.log("businessKey => " + businessKey, Level.INFO, null);
		LogDeMidasInterfaz.log("validoEn => " + validoEn, Level.INFO, null);
		
		B entidad = getByContinuityProperty(continuityEntityClass, businessKeyName, businessKey, validoEn, false);
		LogDeMidasInterfaz.log("entidad => " + entidad, Level.INFO, null);
		
		return entidad;
	}

	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity,K> B getByKey(Class<C> continuityEntityClass, K key) {
		return getByKey(continuityEntityClass, key, TimeUtils.now());
	}

	@Override
	public <B extends EntidadBitemporal,C extends EntidadContinuity, K> B getByKeyOld(Class<C> continuityEntityClass, K key, DateTime validoEn) {
		return getByContinuityPropertyOld(continuityEntityClass, KEY, key, validoEn, false);
	}
	
	@Override
	public <B extends EntidadBitemporal,C extends EntidadContinuity, K> B getByKey(Class<C> continuityEntityClass, K key, DateTime validoEn) {
		return getByContinuityProperty(continuityEntityClass, KEY, key, validoEn, false);
	}
	
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getInProcessByBusinessKey(Class<C> continuityEntityClass, String businessKeyName, K businessKey, DateTime validoEn) {
		return getByContinuityProperty(continuityEntityClass, businessKeyName, businessKey, validoEn, true);
	}
	
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getInProcessByKey(Class<C> continuityEntityClass, K key, DateTime validoEn) {
		return getByContinuityProperty(continuityEntityClass, KEY, key, validoEn, true);
	}
		
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> Collection<B> findInProcessByParentKey(
			Class<C> continuityEntityClass,String parentKeyName, K parentKey, DateTime validoEn) {
		return findByParentContinuityProperty(continuityEntityClass, parentKeyName, parentKey, validoEn, true);
	}
	
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> Collection<B> findInProcessByParentBusinessKey(
			Class<C> continuityEntityClass, String parentBusinessKeyName, K parentBusinessKey, DateTime validoEn) {
		return findByParentContinuityProperty(continuityEntityClass, parentBusinessKeyName, parentBusinessKey, validoEn, true);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getNew(Class<B> bitemporalEntityClass, K parentKey) {
		
		try {
					
			B bitemporalInstance = bitemporalEntityClass.newInstance();
			
			if (parentKey != null) {
				C parentContinuity = (C) entidadContinuityService.findContinuityByKey(bitemporalInstance.getEntidadContinuity().getParentContinuity().getClass(), parentKey);
				bitemporalInstance.getEntidadContinuity().setParentContinuity(parentContinuity);
			}
			
			return bitemporalInstance;
			
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		
	}

	@Override
	public <B extends EntidadBitemporal> void remove(B bitemporalEntity, DateTime validoEn) {
		
		remove(bitemporalEntity, validoEn, null);		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <B extends EntidadBitemporal> void remove(B bitemporalEntity, DateTime validoEn, DateTime validoHasta) {
		
		if (bitemporalEntity.getEntidadContinuity() == null
				|| bitemporalEntity.getEntidadContinuity().getKey() == null) {
			throw new RuntimeException(bitemporalEntity.getEntidadContinuity().getClass() + " no puede ser nulo");
		}
		
		if(validoHasta != null)
		{
			bitemporalEntity.getEntidadContinuity().getBitemporalProperty().end(new IntervalWrapper(TimeUtils.interval(validoEn, validoHasta)), true);
			entidadContinuityDao.update(bitemporalEntity.getEntidadContinuity());			
		}
		else if(bitemporalEntity.getEntidadContinuity().getBitemporalProperty().get(validoEn) == null){
			entidadContinuityDao.remove(bitemporalEntity.getEntidadContinuity());
		}else{
			List<IntervalWrapper> intervalos = bitemporalEntity.getEntidadContinuity().getParentContinuity().getBitemporalProperty()
				.getMergedValidityIntervals();
			
			validoHasta = intervalos.get(0).getInterval().getEnd();
			
			bitemporalEntity.getEntidadContinuity().getBitemporalProperty().end(new IntervalWrapper(TimeUtils.interval(validoEn, validoHasta)), true);
			entidadContinuityDao.update(bitemporalEntity.getEntidadContinuity());
		}		
	}
	
	
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity> B saveInProcess(B bitemporalEntity, DateTime validoEn) {
		
		return saveInProcess(bitemporalEntity, validoEn, null);
		
	}
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity> B saveInProcess(B bitemporalEntity, DateTime validoEn,DateTime validoHasta) {
		
		return this.persist(bitemporalEntity, true, validoEn,validoHasta);
		
	}
	
	
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity> B saveValid(B bitemporalEntity, DateTime validoEn) {
	
		return this.persist(bitemporalEntity, false, validoEn);
	}


	@SuppressWarnings("unchecked")
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity> Object saveValidAndGetKey(B bitemporalEntity, DateTime validoEn) {
		
		Object key = null;
		
		bitemporalEntity = saveValid(bitemporalEntity, validoEn);
		
		C continuity = (C) bitemporalEntity.getEntidadContinuity();
		
		if (continuity != null && continuity.getKey() != null) {
			key = continuity.getKey();
		}
		
		return key;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity> Object saveInProcessAndGetKey(B bitemporalEntity, DateTime validoEn) {
		
		Object key = null;
		
		bitemporalEntity = saveInProcess(bitemporalEntity, validoEn);
		
		C continuity = (C) bitemporalEntity.getEntidadContinuity();
		
		if (continuity != null && continuity.getKey() != null) {
			key = continuity.getKey();
		}
		
		return key;
		
	}
	
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> void commit(Class<C> continuityEntityClass, K key) {
		
		try {
		
			C continuity = (C) entidadContinuityService.findContinuityByKey(continuityEntityClass, key);
			
			continuity.getBitemporalProperty().commitTwoPhase();
			
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
	@Override
	public <B extends EntidadBitemporal> Collection<B> listarFiltrado(Class<B> bitemporalEntityClass, Map<String,Object> params, boolean enProceso, 
			String... orderByAttributes) {
		return listarFiltrado(bitemporalEntityClass, params, TimeUtils.now(), enProceso, orderByAttributes);
	}
	
	@Override
	public <B extends EntidadBitemporal> Collection<B> listarFiltrado(Class<B> bitemporalEntityClass, Map<String,Object> params, 
			DateTime validoEn, boolean enProceso, String... orderByAttributes){
		return listarFiltrado(bitemporalEntityClass, params, validoEn, TimeUtils.now(), enProceso, orderByAttributes);
	}
	
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity> Collection<B> obtenerCancelados(
			Collection<C> continuities, Class<B> bitemporalEntityClass, DateTime validoEn, DateTime fechaDeCancelacion){
		Collection<B> collectionBitemporals =  new ArrayList<B>();
		
		for(C entidadContinuity : continuities){
			Long continuityId = (Long) entidadContinuity.getKey();
			
			Collection<B> collectionBitemporalsTemp = entidadContinuityDao.obtenerCancelados(bitemporalEntityClass, 
					continuityId, validoEn, fechaDeCancelacion);
			
			collectionBitemporals.addAll(collectionBitemporalsTemp);
		}
		
		return collectionBitemporals;
	}
	
	@Override
	public <B extends EntidadBitemporal> Collection<B> obtenerCancelados(Class<B> bitemporalEntityClass, 
			Long continuityId, DateTime validoEn, DateTime fechaDeCancelacion){
		
		Collection<B> collectionBitemporals =  null;
		collectionBitemporals = entidadContinuityDao.obtenerCancelados(bitemporalEntityClass, 
				continuityId, validoEn, fechaDeCancelacion);

		return collectionBitemporals;
	}
	
	@Override
	public <B extends EntidadBitemporal> Collection<B> obtenerCancelados(Class<B> bitemporalEntityClass, 
			DateTime validoEn, DateTime fechaDeCancelacion, Map<String, Object> params){
		Collection<B> collection = null;
		
		collection = entidadContinuityDao.obtenerCancelados(bitemporalEntityClass, 
				validoEn, fechaDeCancelacion, params);
		
		return collection;
	}
	
	@Override
	public <B extends EntidadBitemporal> Collection<B> listarFiltrado(
			Class<B> bitemporalEntityClass, Map<String,Object> params, 
			DateTime validoEn, DateTime conocidoEn, boolean enProceso, String... orderByAttributes){
		return entidadContinuityDao.listarBitemporalsFiltrado(bitemporalEntityClass, params, validoEn, conocidoEn, enProceso, orderByAttributes);
	}
	
	@Override
	public <B extends EntidadBitemporal, K, S> B prepareEndorsementBitemporalEntity(Class<B> bitemporalEntityClass, K entidadContinuityId, 
			S parentContinuityId, DateTime validoEn) {
		try {
			if(entidadContinuityId!=null){
				B entidadBitemporal = bitemporalEntityClass.newInstance();
				return getInProcessByKey(entidadBitemporal.getEntidadContinuity().getClass(), entidadContinuityId, validoEn);
			}else{
				return getNew(bitemporalEntityClass, parentContinuityId);
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
	@Override
	public <B extends EntidadBitemporal> B saveEndorsementBitemporalEntity(B entidadBitemporal, DateTime validoEn) {
		return saveInProcess(entidadBitemporal, validoEn);
	}
	
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity> C attachToParentAndSet (B bitemporalEntity,B parentBitemporal, DateTime validoEn) {
		return attachToParentAndSet(bitemporalEntity, parentBitemporal, validoEn, null);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <B extends EntidadBitemporal, C extends EntidadContinuity> C attachToParentAndSet (B bitemporalEntity,B parentBitemporal, DateTime validoEn,
			DateTime validoHasta) {
		
		if (bitemporalEntity == null || bitemporalEntity.getEntidadContinuity() == null
				|| parentBitemporal == null || parentBitemporal.getEntidadContinuity() == null) {
			throw new RuntimeException("attachToParent : las entidades no pueden ser nulas");
		}
		
		if (!bitemporalEntity.getEntidadContinuity().getParentContinuity().getClass().equals(parentBitemporal.getEntidadContinuity().getClass())) {
			throw new RuntimeException("attachToParent : " + parentBitemporal.getClass() + " no es padre de " + bitemporalEntity.getClass());	
		}
		
		bitemporalEntity.getEntidadContinuity().setParentContinuity(parentBitemporal.getEntidadContinuity());
		
		bitemporalEntity = set(bitemporalEntity, validoEn,validoHasta, true); //Aqui se setea siempre como "en proceso"
						
		return (C) bitemporalEntity.getEntidadContinuity();
		
	}
	
	@Override
	public <B extends EntidadBitemporal> B set (B bitemporalEntity, DateTime validoEn, Boolean  twoPhaseMode) {
		return set(bitemporalEntity, validoEn, null, twoPhaseMode);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <B extends EntidadBitemporal> B set (B bitemporalEntity, DateTime validoEn, DateTime validoHasta, Boolean  twoPhaseMode) {
		
		if (bitemporalEntity == null || bitemporalEntity.getEntidadContinuity() == null) {
			throw new RuntimeException("set : las entidades no pueden ser nulas");
		}
		
		if (bitemporalEntity.getEntidadContinuity().getParentContinuity() == null
				/*|| bitemporalEntity.getEntidadContinuity().getParentContinuity().getKey() == null*/) {
			throw new RuntimeException("El padre de la continuidad " + bitemporalEntity.getEntidadContinuity().getClass() + " tiene que existir");
		}
		
		if(validoHasta == null){
			 
			List<IntervalWrapper> intervalos = bitemporalEntity.getEntidadContinuity().getParentContinuity().getBitemporalProperty()
				.getCurrentMergedValidityIntervals(true);
			
			validoHasta = intervalos.get(0).getInterval().getEnd();
			 
		}
		
		bitemporalEntity.getEntidadContinuity().getBitemporalProperty()
			.set(bitemporalEntity.getEmbedded(), new IntervalWrapper(TimeUtils.interval(validoEn, validoHasta)), twoPhaseMode);
				
		return bitemporalEntity;
		
	}

	@SuppressWarnings("unchecked")
	private <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getByContinuityPropertyOld(
			Class<C> continuityEntityClass,String parameterName, K parameter, DateTime validoEn, 
			boolean getInProcess) {
        LogDeMidasInterfaz.log("Entrando a getByContinuityProperty", Level.INFO, null);
        
        LogDeMidasInterfaz.log("continuityEntityClass => " + continuityEntityClass, Level.INFO, null);
        LogDeMidasInterfaz.log("parameterName => " + parameterName, Level.INFO, null);
        LogDeMidasInterfaz.log("parameter => " + parameter, Level.INFO, null);
        LogDeMidasInterfaz.log("validoEn => " + validoEn, Level.INFO, null);
        LogDeMidasInterfaz.log("getInProcess => " + getInProcess, Level.INFO, null);
        
		try {
				Collection<C> continuities = (Collection<C>) entidadContinuityDao.findByProperty(
						continuityEntityClass, parameterName, parameter);
				
				if (continuities != null && continuities.size() > 0) {
					C continuity = continuities.iterator().next();
					entidadContinuityDao.refresh(continuity);		
					return (B) continuity.getBitemporalProperty().getInProcessOrGet(validoEn, getInProcess);
				}
		
		} catch (Exception ex) {
			LogDeMidasInterfaz.log("Error al tratar de obtener una entidad bitemporal", Level.INFO, ex);
			throw new RuntimeException(ex);
		}
		
        LogDeMidasInterfaz.log("Saliendo de getByContinuityProperty", Level.INFO, null);
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getByContinuityProperty(
			Class<C> continuityEntityClass,String parameterName, K parameter, DateTime validoEn, 
			boolean getInProcess) {
        LogDeMidasInterfaz.log("Entrando a getByContinuityProperty", Level.INFO, null);
        
        LogDeMidasInterfaz.log("continuityEntityClass => " + continuityEntityClass, Level.INFO, null);
        LogDeMidasInterfaz.log("parameterName => " + parameterName, Level.INFO, null);
        LogDeMidasInterfaz.log("parameter => " + parameter, Level.INFO, null);
        LogDeMidasInterfaz.log("validoEn => " + validoEn, Level.INFO, null);
        LogDeMidasInterfaz.log("getInProcess => " + getInProcess, Level.INFO, null);
        
		try {
				Collection<C> continuities = (Collection<C>) entidadContinuityDao.findByProperty(
						continuityEntityClass, parameterName, parameter);
				
				if (continuities != null && continuities.size() > 0) {
					C continuity = continuities.iterator().next();
					entidadContinuityDao.refresh(continuity);		
					
						try{
							Map<String, Object> params = new HashMap<String, Object>();
							//params.put("continuity."+parameterName, parameter);
							params.put("continuity.id", continuity.getKey());
							List<B> list = (List<B>) entidadContinuityDao.listarBitemporalsFiltrado(continuity.getBitemporalClass(), 
									params, validoEn, null, getInProcess);
							if(list != null && !list.isEmpty()){
								return list.get(0);
							}
							//Si no encuentra en proceso trae el valido
							if(getInProcess){
								list = (List<B>) entidadContinuityDao.listarBitemporalsFiltrado(continuity.getBitemporalClass(), 
										params, validoEn, null, !getInProcess);
								if(list != null && !list.isEmpty()){
									return list.get(0);
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					return (B) continuity.getBitemporalProperty().getInProcessOrGet(validoEn, getInProcess);
				}
		
		} catch (Exception ex) {
			LogDeMidasInterfaz.log("Error al tratar de obtener una entidad bitemporal", Level.INFO, ex);
			throw new RuntimeException(ex);
		}
		
        LogDeMidasInterfaz.log("Saliendo de getByContinuityProperty", Level.INFO, null);
		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	private <B extends EntidadBitemporal, C extends EntidadContinuity, K> Collection<B> findByParentContinuityProperty(
			Class<C> continuityEntityClass, String parameterName, K parameter, DateTime validoEn, boolean getInProcess) {
		
		Collection <B> bitemporals = new ArrayList<B>();
		
		try {
			
			Collection <C> continuities = (Collection<C>) findByProperty(continuityEntityClass, parameterName, parameter); 
			
			if(continuities != null && !continuities.isEmpty()){
				
				for (C continuity : continuities) {	
					boolean isFind = false;
					try{
						
						Map<String, Object> params = new HashMap<String, Object>();
						//params.put("continuity."+parameterName, parameter);
						params.put("continuity.id", continuity.getKey());
						List<B> list = (List<B>) entidadContinuityDao.listarBitemporalsFiltrado(continuity.getBitemporalClass(), 
								params, validoEn, null, getInProcess);
						if(list != null && !list.isEmpty() && list.get(0) != null){
							bitemporals.add(list.get(0));
							isFind = true;
						}
						/*
						if(list != null && !list.isEmpty()){
							for(B bitemporal : list){
								if(bitemporal != null){
									bitemporals.add(bitemporal);
									isFind = true;
								}
							}												
						}*/
						//Si no encuentra en proceso trae el valido
						if(getInProcess && !isFind){
							list = (List<B>) entidadContinuityDao.listarBitemporalsFiltrado(continuity.getBitemporalClass(), 
									params, validoEn, null, !getInProcess);
							
							if(list != null && !list.isEmpty() && list.get(0) != null){
								bitemporals.add(list.get(0));
								isFind = true;
							}
							/*
							if(list != null && !list.isEmpty() ){
								for(B bitemporal : list){
									if(bitemporal != null){
										bitemporals.add(bitemporal);
										isFind = true;
									}
								}
							}*/
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				
					//Si no lo encuentra por query lo trae de proceso normal
					if(!isFind){
						B item = (B) continuity.getBitemporalProperty().getInProcessOrGet(validoEn, getInProcess);
						if(item != null){
							bitemporals.add(item);
						}
					}
				}	
			}
			
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		
		return bitemporals;
	}
	
	private <C extends EntidadContinuity, K> Collection<C> findByProperty(Class<C> continuityEntityClass, String property, K value){
		return entidadContinuityDao.findByProperty(continuityEntityClass, property, value);
	}
	
	
	private  <B extends EntidadBitemporal, C extends EntidadContinuity> B persist(B bitemporalEntity, Boolean  twoPhaseMode, DateTime validoEn){
		return persist(bitemporalEntity, twoPhaseMode, validoEn, null);
	}
	
	@SuppressWarnings("unchecked")
	private  <B extends EntidadBitemporal, C extends EntidadContinuity> B persist(B bitemporalEntity, Boolean  twoPhaseMode, DateTime validoEn, DateTime validoHasta){
		try {
			if (bitemporalEntity.getEntidadContinuity() == null) {
				throw new RuntimeException(bitemporalEntity.getEntidadContinuity().getClass() + " no puede ser nulo");
			}
			
			if (bitemporalEntity.getEntidadContinuity().getParentContinuity() == null
					|| bitemporalEntity.getEntidadContinuity().getParentContinuity().getKey() == null) {
				throw new RuntimeException("El padre de la continuidad " + bitemporalEntity.getEntidadContinuity().getClass() + " tiene que existir");
			}
			
			C continuity = null;
			
			if (bitemporalEntity.getEntidadContinuity().getKey() == null) {
				
				bitemporalEntity = set(bitemporalEntity, validoEn,validoHasta, twoPhaseMode);
				
				Object key = entidadContinuityDao.persistAndReturnKey(bitemporalEntity.getEntidadContinuity());
				
				continuity = (C) entidadContinuityDao.getReference(bitemporalEntity.getEntidadContinuity().getClass(), key);
			
			} else {
							
				B bitemporalEntityClone = (B) bitemporalEntity.getClass().newInstance();
				
				bitemporalEntityClone.setEmbedded(bitemporalEntity.getEmbedded());
							
				continuity = (C) entidadContinuityDao.findByKey(bitemporalEntity.getEntidadContinuity().getClass(), 
						bitemporalEntity.getEntidadContinuity().getKey());
			
				entidadContinuityDao.refresh(continuity);
				
				bitemporalEntityClone.setEntidadContinuity(continuity);
				
				bitemporalEntityClone = set(bitemporalEntityClone, validoEn, validoHasta, twoPhaseMode);
							
				continuity = (C) entidadContinuityDao.update(bitemporalEntityClone.getEntidadContinuity());
				
			}
			
			if (twoPhaseMode) {
				return (B) continuity.getBitemporalProperty().getInProcess(validoEn);
			} else {
				return (B) continuity.getBitemporalProperty().get(validoEn);	
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}	
	}
	

	
	@Override
	public  void rollBackStatusContinuityCotizacion(Long continuityId , Integer tipo) {
	
		entidadContinuityDao.rollbackCondicionEspecial(continuityId, tipo);
	}
	
	@Override
	public void rollBackContinuity(Long continuityId){
		entidadContinuityDao.rollBackContinuity(continuityId);
	}
	
		
	private static final String KEY = "id";
	
	
	private EntidadContinuityDao entidadContinuityDao;
	private BitemporalDao bitemporalDao ;
	private EntidadContinuityService entidadContinuityService;
	
	@EJB
	public void setEntidadContinuityDao(
			EntidadContinuityDao entidadContinuityDao) {
		this.entidadContinuityDao = entidadContinuityDao;
	}
	
	@EJB
	public void setEntidadContinuityService(
			EntidadContinuityService entidadContinuityService) {
		this.entidadContinuityService = entidadContinuityService;
	}
	
	@EJB
	public void setBitemporalDao(BitemporalDao bitemporalDao) {
		this.bitemporalDao = bitemporalDao;
	}

	
	
	
}
