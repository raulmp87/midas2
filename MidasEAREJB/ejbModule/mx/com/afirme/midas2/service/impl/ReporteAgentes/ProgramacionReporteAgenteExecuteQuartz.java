package mx.com.afirme.midas2.service.impl.ReporteAgentes;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class ProgramacionReporteAgenteExecuteQuartz extends QuartzJobBean {
	
	ProgramacionReporteAgenteQuartz reporteAgenteQuartz;

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		reporteAgenteQuartz.executeTasks();
	}

	public void setReporteAgenteQuartz(
			ProgramacionReporteAgenteQuartz reporteAgenteQuartz) {
		this.reporteAgenteQuartz = reporteAgenteQuartz;
	}
}
