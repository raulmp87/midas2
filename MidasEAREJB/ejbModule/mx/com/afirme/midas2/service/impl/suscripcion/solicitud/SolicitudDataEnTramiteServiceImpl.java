package mx.com.afirme.midas2.service.impl.suscripcion.solicitud;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudDataEnTramiteDTO;
import mx.com.afirme.midas.solicitud.SolicitudFacadeRemote;
import mx.com.afirme.midas2.service.suscripcion.solicitud.SolicitudDataEnTramiteService;
import mx.com.afirme.midas2.utils.MidasXLSCreator2;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * @author jreyes
 *
 */
@Stateless
public class SolicitudDataEnTramiteServiceImpl implements SolicitudDataEnTramiteService {

	@PersistenceContext
	private EntityManager entityManager;
		
	protected SolicitudFacadeRemote solicitudFacadeRemote;
	
	
	public SolicitudFacadeRemote getSolicitudFacadeRemote() {
		return solicitudFacadeRemote;
	}

	@EJB
	public void setSolicitudFacadeRemote(SolicitudFacadeRemote solicitudFacadeRemote) {
		this.solicitudFacadeRemote = solicitudFacadeRemote;
	}

	/**
	 * Delete a persistent SolicitudDataEnTramiteDTO entity.
	 * 
	 * @param entity
	 *            SolicitudDataEnTramiteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	@Override
	public void delete(SolicitudDataEnTramiteDTO entity) {
		LogDeMidasEJB3.log("deleting SolicitudDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SolicitudDataEnTramiteDTO.class, entity
					.getIdToSolicitudDataEnTramite());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public SolicitudDataEnTramiteDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SolicitudDataEnTramiteDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SolicitudDataEnTramiteDTO instance = entityManager.find(SolicitudDataEnTramiteDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SolicitudDataEnTramiteDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SolicitudDataEnTramiteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SolicitudDataEnTramiteDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SolicitudDataEnTramiteDTO> findByProperty(String propertyName,
			Object value) {
		LogDeMidasEJB3.log("finding SolicitudDataEnTramiteDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SolicitudDataEnTramiteDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Perform an initial save of a previously unsaved SolicitudDataEnTramiteDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SolicitudDataEnTramiteDTO entity to persist
	 * @return 
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	@Override
	public SolicitudDataEnTramiteDTO save(SolicitudDataEnTramiteDTO entity) {
		LogDeMidasEJB3.log("saving SolicitudDataEnTramiteDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SolicitudDataEnTramiteDTO entity and return it or a copy of
	 * it to the sender. A copy of the SolicitudDataEnTramiteDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SolicitudDataEnTramiteDTO entity to update
	 * @return SolicitudDataEnTramiteDTO the persisted SolicitudDataEnTramiteDTO entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	@Override
	public SolicitudDataEnTramiteDTO update(SolicitudDataEnTramiteDTO entity) {
		LogDeMidasEJB3.log("updating SolicitudDataEnTramiteDTO instance", Level.INFO, null);
		try {
			SolicitudDataEnTramiteDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public void save(BigDecimal idToSolicitud, List<SolicitudDataEnTramiteDTO> datos) {
		this.deleteBySolicitud(idToSolicitud);
		for (SolicitudDataEnTramiteDTO solicitudDataEnTramiteDTO : datos) {
			solicitudDataEnTramiteDTO.setIdToSolicitud(idToSolicitud);
			this.save(solicitudDataEnTramiteDTO);
		}
		SolicitudDTO solicitud = solicitudFacadeRemote.findById(idToSolicitud);
		solicitud.setFechaModificacion(new Date());
		solicitud.setTieneCartaCobertura((short) 1);
		solicitud = solicitudFacadeRemote.update(solicitud);
	}
	
	public void deleteBySolicitud(BigDecimal idToSolicitud){
		List<SolicitudDataEnTramiteDTO> datosAntiguos = this.findByProperty("idToSolicitud", idToSolicitud);
		for (SolicitudDataEnTramiteDTO solicitudDataEnTramiteDTO : datosAntiguos) {
			this.delete(solicitudDataEnTramiteDTO);
		}
	}
	
	
	/**
	 * Find all SolicitudDataEnTramiteDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SolicitudDataEnTramiteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SolicitudDataEnTramiteDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SolicitudDataEnTramiteDTO> findBySolicitudAndSerie(Long idToSolicitud,String numeroSerie) {
		
		try {
			final String queryString = "select model from SolicitudDataEnTramiteDTO model where model.idToSolicitud= :idToSolicitud and model.serie =:serie" ; 
										
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSolicitud", idToSolicitud);
			query.setParameter("serie", numeroSerie);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by findBySolicitudAndSerie ", Level.SEVERE, re);
			throw re;
		}
	}
	
	public InputStream descargarExcelCartaCobertura(BigDecimal idToSolicitud) throws IOException{

 		MidasXLSCreator2 xlsx = new MidasXLSCreator2(
 				new String[]{"TIPO","MODELO","SERIE","EQUIPO","ADAPTACION","OBSERVACIONES","FECHA INICIA VIGENCIA","FECHA TERMINA VIGENCIA"}, new String[]{"descripcion", "modelo", "serie", "equipoEspecial", "adaptacion", "observaciones", "fechaInicioVigencia", "fechaFinVigencia"},  MidasXLSCreator2.FORMATO_XLS);

		List<SolicitudDataEnTramiteDTO> solicitudesEnTramiteDTO = this.findByProperty("idToSolicitud", idToSolicitud);
		
		List<Object> list = new ArrayList<Object>();
		
		for (SolicitudDataEnTramiteDTO object : solicitudesEnTramiteDTO) {
			list.add(object);
		}
		xlsx.iniciarProcesamientoArchivoXLS("CartaCobertura"+idToSolicitud);
		xlsx.setAutoSizeColumns(true);
		xlsx.insertarFilasArchivoXLS(list);
		HSSFWorkbook workbook = xlsx.obtieneHSSFWorkbook();
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
					
		workbook.write(bos);
		
		return new ByteArrayInputStream(bos.toByteArray());	
	}

}
