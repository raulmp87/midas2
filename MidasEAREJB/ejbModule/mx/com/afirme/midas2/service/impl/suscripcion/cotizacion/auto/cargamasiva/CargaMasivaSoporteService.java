package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.cargamasiva;

import javax.ejb.EJB;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioFacadeRemote;
import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVAFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.grupo.EstiloVehiculoGrupoFacadeRemote;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoFacadeRemote;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoFacadeRemote;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.codigopostalcolonia.CodigoPostalColoniaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaFacadeRemote;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.NegocioSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUsoDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoDao;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.seccion.dato.ConfiguracionDatoIncisoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.cobertura.CoberturaBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.CentroEmisorService;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
import mx.com.afirme.midas2.service.fuerzaventa.OficinaService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.estadodescuento.NegocioEstadoDescuentoService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionService;
import mx.com.afirme.midas2.service.negocio.tarifa.NegocioTarifaService;
import mx.com.afirme.midas2.service.negocio.zonacirculacion.negocioMunicipio.NegocioMunicipioService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAutoService;
import mx.com.afirme.midas2.service.suscripcion.emision.EmisionService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;
import mx.com.afirme.midas2.service.tarifa.TarifaAgrupadorTarifaService;

public class CargaMasivaSoporteService {
	protected EntidadService entidadService;
	protected NegocioMunicipioService municipioService;
	protected NegocioSeccionService negocioSeccionService;
	protected MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote;	
	protected NegocioTarifaService negocioTarifaService;
	protected EstiloVehiculoGrupoFacadeRemote estiloVehiculoGrupoFacade;
	protected TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService;
	protected EstiloVehiculoFacadeRemote estiloVehiculoFacade;
	protected CodigoPostalColoniaFacadeRemote codigoPostalColoniaFacadeRemote;
	protected EstadoFacadeRemote estadoFacadeRemote;
	protected CiudadFacadeRemote ciudadFacadeRemote;
	protected MunicipioFacadeRemote municipioFacadeRemote;
	protected ColoniaFacadeRemote coloniaFacadeRemote;
	protected IncisoService incisoService;
	protected CoberturaService coberturaService;
	protected ConfiguracionDatoIncisoDao configuracionDatoIncisoDao;
	protected DatoIncisoCotAutoService datoIncisoCotAutoService;
	protected CentroEmisorService centroEmisorService;
	protected NegocioSeccionDao negocioSeccionDao;
	protected CotizacionService cotizacionService;
	protected OficinaService oficinaService;
	protected CalculoService calculoService;
	protected SolicitudAutorizacionService autorizacionService;
	protected EmisionService emisionService;
	protected UsuarioService usuarioService;
	protected NegocioService negocioService;
	protected CodigoPostalIVAFacadeRemote codigoPostalIVAFacadeRemote;
	protected ClienteFacadeRemote clienteFacade;
	protected ModeloVehiculoFacadeRemote modeloVehiculoFacadeRemote;
	protected SeccionCotizacionFacadeRemote seccionCotizacionFacadeRemote;
	protected EjecutivoService ejecutivoService;
	protected AgenteMidasService agenteMidasService;
	protected ListadoService listadoService;
	protected RamoTipoPolizaFacadeRemote ramoTipoPolizaFacadeRemote;
	protected NegocioTipoUsoDao negocioTipoUsoDao;
	protected TipoUsoVehiculoFacadeRemote tipoUsoVehiculoFacadeRemote;
	protected SistemaContext sistemaContext;
	protected MedioPagoFacadeRemote medioPagoFacadeRemote;
	protected NegocioPaqueteSeccionService negocioPaqueteSeccionService;
	protected EndosoService endosoService;
	protected GeneraPlantillaReporteBitemporalService generarPlantillaReporte;
	protected NegocioEstadoDescuentoService negocioEstadoDescuentoService;
	protected CoberturaBitemporalService coberturaBitemporalService;
	protected ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoService;
	protected EntidadContinuityDao entidadContinuityDao;

	@EJB
	public void setEntidadContinuityDao(EntidadContinuityDao entidadContinuityDao) {
		this.entidadContinuityDao = entidadContinuityDao;
	}	
	
	@EJB
	public void setConfiguracionDatoIncisoBitemporalService(ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoService) {
		this.configuracionDatoIncisoService = configuracionDatoIncisoService;
	}
	
	@EJB
	public void setCoberturaBitemporalService(CoberturaBitemporalService coberturaBitemporalService) {
		this.coberturaBitemporalService = coberturaBitemporalService;
	}
	
	@EJB
	public void setNegocioEstadoDescuentoService(NegocioEstadoDescuentoService negocioEstadoDescuentoService) {
		this.negocioEstadoDescuentoService = negocioEstadoDescuentoService;
	}	
	
	@EJB
	public void setGeneraPlantillaReporteBitemporalService(GeneraPlantillaReporteBitemporalService generarPlantillaReporte) {
		this.generarPlantillaReporte = generarPlantillaReporte;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setMunicipioService(NegocioMunicipioService municipioService) {
		this.municipioService = municipioService;
	}

	@EJB
	public void setNegocioSeccionService(NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}
	
	@EJB	
	public void setMarcaVehiculoFacadeRemote(
			MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote) {
		this.marcaVehiculoFacadeRemote = marcaVehiculoFacadeRemote;
	}

	@EJB
	public void setNegocioTarifaService(
			NegocioTarifaService negocioTarifaService) {
		this.negocioTarifaService = negocioTarifaService;
	}
	
	@EJB	
	public void setEstiloVehiculoGrupoFacade(
			EstiloVehiculoGrupoFacadeRemote estiloVehiculoGrupoFacade) {
		this.estiloVehiculoGrupoFacade = estiloVehiculoGrupoFacade;
	}
	
	@EJB
	public void setTarifaAgrupadorTarifaService(
			TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService) {
		this.tarifaAgrupadorTarifaService = tarifaAgrupadorTarifaService;
	}
	
	@EJB	
	public void setEstiloVehiculoFacade(
			EstiloVehiculoFacadeRemote estiloVehiculoFacade) {
		this.estiloVehiculoFacade = estiloVehiculoFacade;
	}
	
	@EJB	
	public void setCodigoPostalColoniaFacadeRemote(
			CodigoPostalColoniaFacadeRemote codigoPostalColoniaFacadeRemote) {
		this.codigoPostalColoniaFacadeRemote = codigoPostalColoniaFacadeRemote;
	}
	
	@EJB	
	public void setEstadoFacadeRemote(
			EstadoFacadeRemote estadoFacadeRemote) {
		this.estadoFacadeRemote = estadoFacadeRemote;
	}
	
	@EJB
	public void setCiudadFacadeRemote(
			CiudadFacadeRemote ciudadFacadeRemote) {
		this.ciudadFacadeRemote = ciudadFacadeRemote;
	}
	
	@EJB
	public void setMunicipioFacadeRemote(
			MunicipioFacadeRemote municipioFacadeRemote) {
		this.municipioFacadeRemote = municipioFacadeRemote;
	}
	
	@EJB
	public void setColoniaFacadeRemote(
			ColoniaFacadeRemote coloniaFacadeRemote) {
		this.coloniaFacadeRemote = coloniaFacadeRemote;
	}

	@EJB
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}

	@EJB
	public void setCoberturaService(CoberturaService coberturaService) {
		this.coberturaService = coberturaService;
	}
	
	@EJB
	public void setConfiguracionDatoIncisoDao(ConfiguracionDatoIncisoDao configuracionDatoIncisoDao) {
		this.configuracionDatoIncisoDao = configuracionDatoIncisoDao;
	}

	@EJB
	public void setDatoIncisoCotAutoService(DatoIncisoCotAutoService datoIncisoCotAutoService) {
		this.datoIncisoCotAutoService = datoIncisoCotAutoService;
	}

	@EJB
	public void setCentroEmisorService(CentroEmisorService centroEmisorService) {
		this.centroEmisorService = centroEmisorService;
	}

	@EJB
	public void setNegocioSeccionDao(NegocioSeccionDao negocioSeccionDao) {
		this.negocioSeccionDao = negocioSeccionDao;
	}

	@EJB
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	@EJB
	public void setOficinaService(OficinaService oficinaService) {
		this.oficinaService = oficinaService;
	}

	@EJB
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

	@EJB
	public void setAutorizacionService(SolicitudAutorizacionService autorizacionService) {
		this.autorizacionService = autorizacionService;
	}

	@EJB
	public void setEmisionService(EmisionService emisionService) {
		this.emisionService = emisionService;
	}

	@EJB(beanName="UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@EJB
	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}

	@EJB
	public void setCodigoPostalIVAFacadeRemote(
			CodigoPostalIVAFacadeRemote codigoPostalIVAFacadeRemote) {
		this.codigoPostalIVAFacadeRemote = codigoPostalIVAFacadeRemote;
	}

	@EJB
	public void setClienteFacade(ClienteFacadeRemote clienteFacade) {
		this.clienteFacade = clienteFacade;
	}

	@EJB
	public void setModeloVehiculoFacadeRemote(ModeloVehiculoFacadeRemote modeloVehiculoFacadeRemote) {
		this.modeloVehiculoFacadeRemote = modeloVehiculoFacadeRemote;
	}

	@EJB
	public void setSeccionCotizacionFacadeRemote(
			SeccionCotizacionFacadeRemote seccionCotizacionFacadeRemote) {
		this.seccionCotizacionFacadeRemote = seccionCotizacionFacadeRemote;
	}

	@EJB
	public void setEjecutivoService(EjecutivoService ejecutivoService) {
		this.ejecutivoService = ejecutivoService;
	}

	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@EJB
	public void setRamoTipoPolizaFacadeRemote(RamoTipoPolizaFacadeRemote ramoTipoPolizaFacadeRemote) {
		this.ramoTipoPolizaFacadeRemote = ramoTipoPolizaFacadeRemote;
	}

	@EJB
	public void setNegocioTipoUsoDao(NegocioTipoUsoDao negocioTipoUsoDao) {
		this.negocioTipoUsoDao = negocioTipoUsoDao;
	}

	@EJB
	public void setTipoUsoVehiculoFacadeRemote(
			TipoUsoVehiculoFacadeRemote tipoUsoVehiculoFacadeRemote) {
		this.tipoUsoVehiculoFacadeRemote = tipoUsoVehiculoFacadeRemote;
	}

	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}

	@EJB
	public void setMedioPagoFacadeRemote(MedioPagoFacadeRemote medioPagoFacadeRemote) {
		this.medioPagoFacadeRemote = medioPagoFacadeRemote;
	}
	
	@EJB
	public void setNegocioPaqueteSeccionService(
			NegocioPaqueteSeccionService negocioPaqueteSeccionService) {
		this.negocioPaqueteSeccionService = negocioPaqueteSeccionService;
	}
	
	@EJB
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}

}
