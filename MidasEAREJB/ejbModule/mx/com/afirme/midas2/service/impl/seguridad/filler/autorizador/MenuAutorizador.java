package mx.com.afirme.midas2.service.impl.seguridad.filler.autorizador;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

public class MenuAutorizador {
	private List<Menu> listaMenu = null;
	
	public MenuAutorizador() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		Menu menu;
		menu = new Menu(new Integer("1"),"m1","Emision", "Menu ppal Emision", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m1_1","Autos", "Submenu Autos", null, false);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m1_2","Vida", "Submenu Vida", null, false);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("4"),"m1_3","Da�os", "Submenu Da�os", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m1_3_1","Solicitudes", "Submenu Solicitudes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("6"),"m1_3_2","Ordenes de Trabajo", "Submenu Ordenes", null, true);
		listaMenu.add(menu);
				
		menu = new Menu(new Integer("7"),"m1_3_3","Cotizaciones", "Submenu Cotizaciones", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("8"),"m1_3_3_1","Cotizaciones de P�liza", "Submenu Cotizaciones de P�liza", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("9"),"m1_3_3_2","Cotizaciones de Endoso", "Submenu Cotizaciones de Endoso", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("10"),"m1_3_4","Emisiones", "Submenu Emisiones", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("11"),"m1_3_5","Cat�logos", "Submenu Cat�logos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("12"),"m1_3_5_1","Proveedores de inspecci�n", "Submenu Proveedores de inspecci�n", null, true);
		listaMenu.add(menu);		
		
		menu = new Menu(new Integer("13"),"m6","Pendientes", "Submenu Pendientes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("14"),"m6_1","Lista de Pendientes", "Submenu Lista de Pendientes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("15"),"m7","Ayuda", "Submenu Ayuda", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("16"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("17"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
		
		return this.listaMenu;
	}
}
