package mx.com.afirme.midas2.service.impl.bonos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.bonos.ConfigBonoExcepAgenteDao;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoExcepAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.service.bonos.ConfigBonoExcepAgenteService;

@Stateless
public class ConfigBonoExcepAgenteServiceImpl implements ConfigBonoExcepAgenteService{
	private ConfigBonoExcepAgenteDao dao;
	
	@EJB
	public void setDao(ConfigBonoExcepAgenteDao dao) {
		this.dao = dao;
	}

	@Override
	public List<ConfigBonoExcepAgente> loadByConfigBono(ConfigBonos configBonos) throws Exception {
		return dao.loadByConfigBono(configBonos);
	}

}
