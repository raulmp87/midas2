package mx.com.afirme.midas2.service.impl.bitemporal.endoso.condicionesespeciales;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.negocio.condicionespecial.NegocioCondicionEspecialDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspInc;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.CondicionEspCotContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.CondicionEspIncContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.domain.negocio.condicionespecial.NegocioCondicionEspecial;
import mx.com.afirme.midas2.dto.condicionespecial.CondicionEspecialBitemporalDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.inciso.IncisoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.componente.condicionespecial.CondicionEspecialCotizacionService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;
import mx.com.afirme.midas2.service.negocio.condicionespecial.NegocioCondicionEspecialService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.RecordStatus;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class CondicionEspecialBitemporalServiceImpl implements CondicionEspecialBitemporalService {
	
	@EJB
	private EntidadContinuityService entidadContinuityService;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private NegocioCondicionEspecialService negocioCondicionEspecialService;
	
	@EJB
	private CondicionEspecialCotizacionService condicionEspecialCotizacionService;
	
	@EJB
	private EntidadBitemporalService entidadBitemporalService;
	
	@EJB
	private NegocioCondicionEspecialDao negocioCondicionEspecialDao;
	
	@EJB
	private IncisoViewService incisoViewService ;
	
	@EJB
	ListadoIncisosDinamicoService listadoIncisosDinamicoService;
	
	@EJB
	CondicionEspecialService condicionEspecialService;
	
	@EJB
	IncisoBitemporalService incisoBitemporalService;
	
	@EJB
	protected EntidadContinuityDao entidadContinuityDao;	
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	@Override
	public void asociarCondicionesDefault(BitemporalInciso inciso) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eliminarCondicion(Long idCondicionEspecial, Date validoEn) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eliminarCondicionInciso(BitemporalCondicionEspInc condicionInciso, Date validoEn) {
		// BitemporalCondicionEspInc   
		//BitemporalCondicionEspInc bitemporalCondicion = entidadBitemporalService.getInProcessByKey(CondicionEspIncContinuity.class,
			//	condicionBitemporalDTO.getIdContinuity(), TimeUtils.getDateTime(validoEn));
				
		entidadBitemporalService.remove(condicionInciso, TimeUtils.getDateTime(validoEn));		
	}
	
	@Override
	public void eliminarCondicionCotizacion(CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn) {
		
		BitemporalCondicionEspCot bitemporalCondicion = entidadBitemporalService.getInProcessByKey(CondicionEspCotContinuity.class,
				condicionBitemporalDTO.getIdContinuity(), TimeUtils.getDateTime(validoEn));
				
			entidadBitemporalService.remove(bitemporalCondicion, TimeUtils.getDateTime(validoEn));
		
		
	}
	
	
	
	@Override
	public void asociarNuevamenteCondicionCotizacion(CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn) {
		
		
		BitemporalCondicionEspCot bitemporalCondicion = entidadBitemporalService.getByKey(CondicionEspCotContinuity.class, condicionBitemporalDTO.getIdContinuity());
			//entidadBitemporalService.getInProcessByKey(CondicionEspCotContinuity.class,
				//condicionBitemporalDTO.getIdContinuity(), TimeUtils.getDateTime(validoEn));
	
		
		entidadBitemporalService.rollBackStatusContinuityCotizacion( bitemporalCondicion.getContinuity().getId(),NivelAplicacion.POLIZA.getNivel().intValue() );
		//bitemporalCondicion.setRecordStatus(RecordStatus.TO_BE_ADDED);
		//entidadBitemporalService.saveValid(bitemporalCondicion,  TimeUtils.getDateTime(validoEn) );	
	}
	

	@Override
	public void guardarCondicionCotizacion(Long cotizacionContinuityId,CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn) {
		
		BitemporalCondicionEspCot bitemporalCondicionEspCot 	= new BitemporalCondicionEspCot();
		Usuario usuario 										= usuarioService.getUsuarioActual();
		CotizacionContinuity cotContinuity 						= null;
		
		cotContinuity = entidadContinuityService.findContinuityByKey(CotizacionContinuity.class, cotizacionContinuityId);
		
		bitemporalCondicionEspCot.getContinuity().setParentContinuity(cotContinuity);
		bitemporalCondicionEspCot.getContinuity().setCondicionEspecialId(condicionBitemporalDTO.getCondicionEspecial().getId());
		
		bitemporalCondicionEspCot.getValue().setNombreUsuarioCreacion(usuario.getNombreUsuario());
		bitemporalCondicionEspCot.getValue().setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		
		entidadBitemporalService.saveInProcess(bitemporalCondicionEspCot, TimeUtils.getDateTime(validoEn));
	}
	

	

	@Override
	public void guardarCondicionesEspeciales(
			CondicionEspecialBitemporalDTO bitemporal,
			Long cotizacionContinuityId, String incisos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void guardarCondicionesObligatoriasInciso() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void guardarCondicionInciso(Long cotizacionContinuityId, Long numeroInciso, CondicionEspecial condicion, Date validoEn) {
		// TODO Auto-generated method stub
		
	}

	

	@Override
	public List<BitemporalCondicionEspCot> obtenerCondicionesCotContinuity(Long cotizacionContinuityId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesCotizacionNombreCodigo(String nombreCondicion, Long cotizacionContinuityId, Date validoEn) {
		
		List<CondicionEspecialBitemporalDTO> listCondicionesDTO 	= new ArrayList<CondicionEspecialBitemporalDTO>();
		List<CondicionEspecial> listaCondicionesEspeciales 			= new ArrayList<CondicionEspecial>();
		long idNegocio 												= 0;
		CotizacionContinuity cotContinuity 							= null;
		
		if( nombreCondicion == null){
			nombreCondicion = "";
		}
		
		cotContinuity = entidadContinuityService.findContinuityByKey(CotizacionContinuity.class, cotizacionContinuityId);
		idNegocio = condicionEspecialCotizacionService.getIdNegocio( BigDecimal.valueOf(   cotContinuity.getNumero()  ) );
		listaCondicionesEspeciales = negocioCondicionEspecialDao.buscarCondicionCodigoNombre(idNegocio, nombreCondicion, NivelAplicacion.POLIZA);
	
		
		
		if (!listaCondicionesEspeciales.isEmpty()) {

			List<CondicionEspecialBitemporalDTO> lstCondicionesAsociadas = obtenerCondicionesAsociadasCotizacion(cotizacionContinuityId, validoEn);
			
			for (CondicionEspecial condicion : listaCondicionesEspeciales) {
				
				boolean add = true;
				for (CondicionEspecialBitemporalDTO condicionAsociada : lstCondicionesAsociadas) {
					if (condicion.getId() == condicionAsociada.getCondicionEspecial().getId()) {
						add = false;
						break;
					}
				}
				
				if (add) {
					CondicionEspecialBitemporalDTO condicionDTO = new CondicionEspecialBitemporalDTO();
					condicionDTO.setCondicionEspecial(entidadService.getReference(CondicionEspecial.class, condicion.getId()));
					condicionDTO.setTipoCondicion(NivelAplicacion.POLIZA.getNivel());
					condicionDTO.setObligatoria(0);
					
					listCondicionesDTO.add(condicionDTO);
				}			
			}
		}
		
		
		return listCondicionesDTO;
	}

	@Override
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesDisponiblesCotizacion(
			Long cotizacionContinuityId, Date validoEn) {
		
		List<CondicionEspecialBitemporalDTO> listCondicionesDTO 	= new ArrayList<CondicionEspecialBitemporalDTO>();
		List<NegocioCondicionEspecial> listaCondicionesEspeciales 	= new ArrayList<NegocioCondicionEspecial>();
		long idNegocio 												= 0;
		CotizacionContinuity cotContinuity 							= null;
		
		cotContinuity = entidadContinuityService.findContinuityByKey(CotizacionContinuity.class, cotizacionContinuityId);
		idNegocio = condicionEspecialCotizacionService.getIdNegocio( BigDecimal.valueOf(   cotContinuity.getNumero()  ) );
		listaCondicionesEspeciales = negocioCondicionEspecialService.obtenerCondicionesNegocio(idNegocio,NivelAplicacion.POLIZA);
		
		listCondicionesDTO = convertNegocioCondicionEspecialToDTO(listaCondicionesEspeciales);
		

		return  validateCondicionesAsociadas(listCondicionesDTO, obtenerCondicionesAsociadasCotizacion(cotizacionContinuityId, validoEn) );
	}
	
	private Integer getObligatoriedad( Long cotizacionContinuityId , Long condicionEspecialId , NivelAplicacion nivelAplicacion){
		
		CotizacionContinuity cotContinuity 							= null;
		List<NegocioCondicionEspecial> listaCondicionesEspeciales 	= new ArrayList<NegocioCondicionEspecial>();
		long idNegocio 												= 0;
		Integer obligatoria 										= 0;
		
		
		if( NivelAplicacion.POLIZA.equals(nivelAplicacion)){
			cotContinuity = entidadContinuityService.findContinuityByKey(CotizacionContinuity.class, cotizacionContinuityId);
			idNegocio = condicionEspecialCotizacionService.getIdNegocio( BigDecimal.valueOf(   cotContinuity.getNumero()  ) );
			listaCondicionesEspeciales = negocioCondicionEspecialService.obtenerCondicionesNegocio(idNegocio,NivelAplicacion.POLIZA);
		}else if(NivelAplicacion.INCISO.equals(nivelAplicacion)){
			IncisoContinuity incisoContinuity = entidadContinuityService.findContinuityByKey(IncisoContinuity.class, cotizacionContinuityId);
			Long idToNegocio = condicionEspecialCotizacionService.getIdNegocio(new BigDecimal(incisoContinuity.getCotizacionContinuity().getNumero()));
			listaCondicionesEspeciales = negocioCondicionEspecialService.obtenerCondicionesNegocio(idToNegocio, NivelAplicacion.INCISO);
		}
		
		
		for(NegocioCondicionEspecial negocioCondicionEspecial: listaCondicionesEspeciales){
			if(negocioCondicionEspecial.getCondicionEspecial().getId() == condicionEspecialId){
				obligatoria = negocioCondicionEspecial.getObligatoria();
				break;
			}
		}
		
		return obligatoria;
	}
	
	@Override
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionEspCotInProcess(Long cotizacionContinuityId, Date validoEn){
		
		List<CondicionEspecialBitemporalDTO> resultado 							= new ArrayList<CondicionEspecialBitemporalDTO>();
		CondicionEspecialBitemporalDTO condicionDTO  							= null;
		CondicionEspecial condicionEspeccial 									= null;
		BitemporalCondicionEspCot bitemporalCondicionEspCot 					= null;
		DateTime validOn 														= new DateTime(validoEn);
		Collection <CondicionEspCotContinuity> listCondicionEspCotContinuity   	= new ArrayList<CondicionEspCotContinuity>();
		
	
		listCondicionEspCotContinuity = entidadContinuityService.findContinuitiesByParentKey(CondicionEspCotContinuity.class, 
										CondicionEspCotContinuity.PARENT_KEY_NAME,
										cotizacionContinuityId);
	
		for(CondicionEspCotContinuity condicionEspCotContinuity:listCondicionEspCotContinuity){
			bitemporalCondicionEspCot = null;
			if( condicionEspCotContinuity.getCondicionesEspecialesCotizacion().hasRecordsInProcess() ){
				bitemporalCondicionEspCot = condicionEspCotContinuity.getCondicionesEspecialesCotizacion().getInProcess(validOn);
			}
			
			if( bitemporalCondicionEspCot != null){
				condicionEspeccial = entidadService.getReference(CondicionEspecial.class, bitemporalCondicionEspCot.getContinuity().getCondicionEspecialId()) ;
				
				
				if( condicionEspeccial.getNivelAplicacion() == NivelAplicacion.POLIZA.getNivel()){
					
					if( getObligatoriedad(cotizacionContinuityId , condicionEspeccial.getId(), NivelAplicacion.POLIZA ).equals(0) ){
						condicionDTO = new CondicionEspecialBitemporalDTO();
						condicionDTO.setIdContinuity( condicionEspCotContinuity.getId());
						resultado.add(condicionDTO);
					}

				}
			}
		
		}
		
		return resultado;
	}
	
	
	
	@Override
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesAsociadasCotizacion(Long cotizacionContinuityId, Date validoEn){
		
		List<CondicionEspecialBitemporalDTO> resultado 							= new ArrayList<CondicionEspecialBitemporalDTO>();
		CondicionEspecialBitemporalDTO condicionDTO  							= null;
		CondicionEspecial condicionEspeccial 									= null;
		BitemporalCondicionEspCot bitemporalCondicionEspCot 					= null;
		DateTime validOn 														= new DateTime(validoEn);
		Collection <CondicionEspCotContinuity> listCondicionEspCotContinuity   	= new ArrayList<CondicionEspCotContinuity>();
		
	
		listCondicionEspCotContinuity = entidadContinuityService.findContinuitiesByParentKey(CondicionEspCotContinuity.class, 
				CondicionEspCotContinuity.PARENT_KEY_NAME,
				cotizacionContinuityId);

		
		for(CondicionEspCotContinuity condicionEspCotContinuity:listCondicionEspCotContinuity){
			if( condicionEspCotContinuity.getCondicionesEspecialesCotizacion().hasRecordsInProcess() ){
				bitemporalCondicionEspCot = condicionEspCotContinuity.getCondicionesEspecialesCotizacion().getInProcess(validOn);
			}else{
				bitemporalCondicionEspCot = condicionEspCotContinuity.getCondicionesEspecialesCotizacion().get(validOn);
				
			}
			
			if( bitemporalCondicionEspCot != null){
				condicionEspeccial = entidadService.getReference(CondicionEspecial.class, bitemporalCondicionEspCot.getContinuity().getCondicionEspecialId()) ;
				
				
				if( condicionEspeccial.getNivelAplicacion() == NivelAplicacion.POLIZA.getNivel()){
					condicionDTO = new CondicionEspecialBitemporalDTO();
					condicionDTO.setCondicionEspecial( condicionEspeccial );
					condicionDTO.setIdCondicionBitemporal(bitemporalCondicionEspCot.getId());
					condicionDTO.setIdContinuity( condicionEspCotContinuity.getId());
					condicionDTO.setTipoCondicion( condicionEspeccial.getNivelAplicacion());
					//condicionDTO.setObligatoria( obligatoria );
					condicionDTO.setObligatoria( getObligatoriedad(cotizacionContinuityId , condicionEspeccial.getId() ,NivelAplicacion.POLIZA ) );
					
					resultado.add(condicionDTO);
				}
			}
		
		}
		
		return resultado;
	}
	
	@Override
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesAsociadasCotizacionInProcess(Long cotizacionContinuityId, Date validoEn){
		
		List<CondicionEspecialBitemporalDTO> resultado 							= new ArrayList<CondicionEspecialBitemporalDTO>();
		CondicionEspecialBitemporalDTO condicionDTO  							= null;
		CondicionEspecial condicionEspeccial 									= null;
		BitemporalCondicionEspCot bitemporalCondicionEspCot 					= null;
		DateTime validOn 														= new DateTime(validoEn);
		Collection <CondicionEspCotContinuity> listCondicionEspCotContinuity   	= new ArrayList<CondicionEspCotContinuity>();
		
	
		listCondicionEspCotContinuity = entidadContinuityService.findContinuitiesByParentKey(CondicionEspCotContinuity.class, 
				CondicionEspCotContinuity.PARENT_KEY_NAME,
				cotizacionContinuityId);

		
		for(CondicionEspCotContinuity condicionEspCotContinuity:listCondicionEspCotContinuity){
			if( condicionEspCotContinuity.getCondicionesEspecialesCotizacion().hasRecordsInProcess() ){
				bitemporalCondicionEspCot = condicionEspCotContinuity.getCondicionesEspecialesCotizacion().getInProcess(validOn);
			}
			
			if( bitemporalCondicionEspCot != null){
				condicionEspeccial = entidadService.getReference(CondicionEspecial.class, bitemporalCondicionEspCot.getContinuity().getCondicionEspecialId()) ;
				
				
				if( condicionEspeccial.getNivelAplicacion() == NivelAplicacion.POLIZA.getNivel()){
					condicionDTO = new CondicionEspecialBitemporalDTO();
					condicionDTO.setCondicionEspecial( condicionEspeccial );
					condicionDTO.setIdCondicionBitemporal(bitemporalCondicionEspCot.getId());
					condicionDTO.setIdContinuity( condicionEspCotContinuity.getId());
					condicionDTO.setTipoCondicion( condicionEspeccial.getNivelAplicacion());
					//condicionDTO.setObligatoria( obligatoria );
					condicionDTO.setObligatoria( getObligatoriedad(cotizacionContinuityId , condicionEspeccial.getId() ,NivelAplicacion.POLIZA ) );
					
					resultado.add(condicionDTO);
				}
			}
		
		}
		
		return resultado;
	}
	
	
	@Override
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesEliminadasCotizacion(Long cotizacionContinuityId, Date validoEn){ 
		
		List<CondicionEspecialBitemporalDTO> listCondicionesDTO 		= new ArrayList<CondicionEspecialBitemporalDTO>();
		Collection<BitemporalCondicionEspCot> listaTobeEnded 			= null;
		CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO 	= null;
		CotizacionContinuity cotContinuity 								= null;
		CondicionEspecial condicionEspeccial 							= null;
		
		cotContinuity = entidadContinuityService.findContinuityByKey(CotizacionContinuity.class, cotizacionContinuityId);
		listaTobeEnded = cotContinuity.getCondicionEspecialCotContinuities().getToBeEnded( TimeUtils.getDateTime(validoEn) );
		
		for(BitemporalCondicionEspCot bitemporalCondicionEspCot :listaTobeEnded){
			
			condicionEspeccial = entidadService.getReference(CondicionEspecial.class, bitemporalCondicionEspCot.getContinuity().getCondicionEspecialId()) ;
			
			condicionEspecialBitemporalDTO = new CondicionEspecialBitemporalDTO();
			condicionEspecialBitemporalDTO.setCondicionEspecial( condicionEspeccial );
			condicionEspecialBitemporalDTO.setIdContinuity(bitemporalCondicionEspCot.getContinuity().getId());
			condicionEspecialBitemporalDTO.setTipoCondicion(condicionEspeccial.getNivelAplicacion());
			condicionEspecialBitemporalDTO.setObligatoria( getObligatoriedad( cotizacionContinuityId , condicionEspeccial.getId() , NivelAplicacion.POLIZA) );
			
			listCondicionesDTO.add(condicionEspecialBitemporalDTO);
		}

		return  listCondicionesDTO;
	}
	
	
	@Override
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesEliminadasInciso(Long incisoContinuityId, Date validoEn){ 
		
		List<CondicionEspecialBitemporalDTO> listCondicionesDTO 		= new ArrayList<CondicionEspecialBitemporalDTO>();
		Collection<BitemporalCondicionEspInc> listaTobeEnded 			= null;
		CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO 	= null;
		IncisoContinuity incisoContinuity 								= null;
		CondicionEspecial condicionEspeccial 							= null;
		
		
		incisoContinuity = entidadContinuityService.findContinuityByKey(IncisoContinuity.class, incisoContinuityId);
		
		listaTobeEnded = incisoContinuity.getCondicionEspIncContinuities().getToBeEnded( TimeUtils.getDateTime(validoEn) );
		
		for(BitemporalCondicionEspInc bitemporalCondicionEspInc :listaTobeEnded){
			
			condicionEspeccial = entidadService.getReference(CondicionEspecial.class, bitemporalCondicionEspInc.getContinuity().getCondicionEspecialId()) ;
			
			condicionEspecialBitemporalDTO = new CondicionEspecialBitemporalDTO();
			condicionEspecialBitemporalDTO.setCondicionEspecial( condicionEspeccial );
			condicionEspecialBitemporalDTO.setIdContinuity(bitemporalCondicionEspInc.getContinuity().getId());
			condicionEspecialBitemporalDTO.setTipoCondicion(condicionEspeccial.getNivelAplicacion());
			condicionEspecialBitemporalDTO.setObligatoria( getObligatoriedad( incisoContinuityId , condicionEspeccial.getId(), NivelAplicacion.INCISO) );
			
			listCondicionesDTO.add(condicionEspecialBitemporalDTO);
		}

		return  listCondicionesDTO;
	}
	
	

	
	/**
	 * Convierte las lista de Objetos NegocioCondicionEspecial en una Lista de CondicionEpecialBitemporalDTO
	 * @param listaCondicionesEspeciales
	 * @return
	 */
	private List<CondicionEspecialBitemporalDTO> convertNegocioCondicionEspecialToDTO( List<NegocioCondicionEspecial> listaCondicionesEspeciales ){
		List<CondicionEspecialBitemporalDTO> listCondicionesDTO 	= new ArrayList<CondicionEspecialBitemporalDTO>();
		CondicionEspecialBitemporalDTO condicionDTO 				= null;
		
		for(NegocioCondicionEspecial negocioCondiccionEspecial : listaCondicionesEspeciales){
			condicionDTO = new CondicionEspecialBitemporalDTO();
			condicionDTO.setCondicionEspecial( negocioCondiccionEspecial.getCondicionEspecial() );
			condicionDTO.setTipoCondicion(negocioCondiccionEspecial.getCondicionEspecial().getNivelAplicacion());
			condicionDTO.setObligatoria( negocioCondiccionEspecial.getObligatoria());
			
			listCondicionesDTO.add(condicionDTO);
		}
		
		return listCondicionesDTO ;
		
	}
	
	
	
	/**
	 * Valida que las condiciones Disponibles, no este ya asociadas a la Cotizacion
	 * @param listCondicionesDisponibles
	 * @param listCondicionesAsociadas
	 * @return
	 */
	private List<CondicionEspecialBitemporalDTO> validateCondicionesAsociadas(List<CondicionEspecialBitemporalDTO> listCondicionesDisponibles,List<CondicionEspecialBitemporalDTO> listCondicionesAsociadas){
		List<CondicionEspecialBitemporalDTO> listCondicionesRemover		= new ArrayList<CondicionEspecialBitemporalDTO>();
		
		for(CondicionEspecialBitemporalDTO condicionDisponible:listCondicionesDisponibles){
			
			for(CondicionEspecialBitemporalDTO condicionAsociada : listCondicionesAsociadas){
				
				if( condicionDisponible.getCondicionEspecial().getId() == condicionAsociada.getCondicionEspecial().getId()){
					listCondicionesRemover.add( condicionDisponible);
				}
			}
		}
		
		listCondicionesDisponibles.removeAll( listCondicionesRemover );
		
		return listCondicionesDisponibles;
	}

	@Override
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesDisponiblesInciso(
			List<CondicionEspecialBitemporalDTO> lstDisponibles, Long incisoContinuityId, Date validoEn) {
		
		List<NegocioCondicionEspecial> lstNegocioCondicion = new ArrayList<NegocioCondicionEspecial>();
		
		IncisoContinuity incisoContinuity = entidadContinuityService.findContinuityByKey(IncisoContinuity.class, incisoContinuityId);
		
		Long idToNegocio = condicionEspecialCotizacionService.getIdNegocio(new BigDecimal(incisoContinuity.getCotizacionContinuity().getNumero()));

		lstNegocioCondicion = negocioCondicionEspecialService.obtenerCondicionesNegocio(idToNegocio, NivelAplicacion.INCISO);

		
		List<CondicionEspecialBitemporalDTO> lstCondicionesAsociadas = obtenerCondicionesIncisoAsociadas(incisoContinuityId, validoEn);
		
		for (NegocioCondicionEspecial negocioCondicion : lstNegocioCondicion) {

			boolean agregar = true;
			for (CondicionEspecialBitemporalDTO condicionAsociada : lstCondicionesAsociadas) {
				
				if (negocioCondicion.getCondicionEspecial().getId() == condicionAsociada.getCondicionEspecial().getId()) {					
					agregar = false;
					break;
				} else {
					for (CondicionEspecialBitemporalDTO condicionesDisponibles : lstDisponibles) {
						if (negocioCondicion.getCondicionEspecial().getId() == condicionesDisponibles.getCondicionEspecial().getId()) {
							agregar = false;
							break;
						}
					}
				}
			}
			
			if (agregar) {
				CondicionEspecialBitemporalDTO condicionDTO = new CondicionEspecialBitemporalDTO();
				condicionDTO.setCondicionEspecial(entidadService.findById(CondicionEspecial.class, negocioCondicion.getCondicionEspecial().getId()));
				condicionDTO.setTipoCondicion(NivelAplicacion.INCISO.getNivel());
				condicionDTO.setObligatoria(0);
				
				lstDisponibles.add(condicionDTO);
			}					
		}		
		return lstDisponibles;
	}
	


	@Override
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesIncisoNombreCodigo(String nombreCondicion, Long incisoContinuityId, Date validoEn) {
		List<CondicionEspecialBitemporalDTO> condicionesResult = new ArrayList<CondicionEspecialBitemporalDTO>();
		
		IncisoContinuity incisoContinuity = entidadContinuityService.findContinuityByKey(IncisoContinuity.class, incisoContinuityId);
		
		Long idToNegocio = condicionEspecialCotizacionService.getIdNegocio(new BigDecimal(incisoContinuity.getCotizacionContinuity().getNumero()));
		
		List<CondicionEspecial> condiciones = negocioCondicionEspecialDao.buscarCondicionCodigoNombre(idToNegocio, nombreCondicion, NivelAplicacion.INCISO);		
		
		if (!condiciones.isEmpty()) {
			List<CondicionEspecialBitemporalDTO> lstCondicionesAsociadas = obtenerCondicionesIncisoAsociadas(incisoContinuityId, validoEn);
			
			for (CondicionEspecial condicion : condiciones) {
				
				boolean add = true;
				for (CondicionEspecialBitemporalDTO condicionAsociada : lstCondicionesAsociadas) {
					if (condicion.getId() == condicionAsociada.getCondicionEspecial().getId()) {
						add = false;
						break;
					}
				}
				
				if (add) {
					CondicionEspecialBitemporalDTO condicionDTO = new CondicionEspecialBitemporalDTO();
					condicionDTO.setCondicionEspecial(entidadService.getReference(CondicionEspecial.class, condicion.getId()));
					condicionDTO.setTipoCondicion(NivelAplicacion.INCISO.getNivel());
					condicionDTO.setObligatoria(0);
					
					condicionesResult.add(condicionDTO);
				}			
			}
		}
		
		
		return condicionesResult;
	}
	
	@Override
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesIncisoAsociadas( List<Long> listIncisoContinuityId, Date validoEn ) {
		
		List<CondicionEspecialBitemporalDTO> resultado 				= new ArrayList<CondicionEspecialBitemporalDTO>();
		List<CondicionEspecialBitemporalDTO> lstCondicionesDTO 		= new ArrayList<CondicionEspecialBitemporalDTO>();
		boolean add 												= Boolean.FALSE;
		
		for(Long incisoContinuityId : listIncisoContinuityId){
			lstCondicionesDTO = obtenerCondicionesIncisoAsociadas( incisoContinuityId , validoEn);
			
			if( resultado.size() == 0 ){
				resultado.addAll(lstCondicionesDTO);
			}else{
				for(CondicionEspecialBitemporalDTO  condicionBi : lstCondicionesDTO ){
					
					for( CondicionEspecialBitemporalDTO  resultadoCondicionBi : resultado){
						
						if( resultadoCondicionBi.getCondicionEspecial().getId() == condicionBi.getCondicionEspecial().getId() ){
							add = Boolean.FALSE;
							break;
						}else{
							add = Boolean.TRUE;
						}
					}
					
					if(add){
						resultado.add(condicionBi);
					}
				}
				
			}
			
			
		}

		return resultado;
	}
	
	@Override
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesIncisoAsociadasInProcess(Long incisoContinuityId, Date validoEn) {	
		List<CondicionEspecialBitemporalDTO> lstCondicionesDTO = new ArrayList<CondicionEspecialBitemporalDTO>();
		
		DateTime validOn = TimeUtils.getDateTime(validoEn);
		
		Collection<CondicionEspIncContinuity> lstCondicionContinuity = 
				entidadContinuityService.findContinuitiesByParentKey(CondicionEspIncContinuity.class, 
																	 CondicionEspIncContinuity.PARENT_KEY_NAME,
																	 incisoContinuityId);
		
		for (CondicionEspIncContinuity condicionContinuity : lstCondicionContinuity) {
			BitemporalCondicionEspInc bitemporalCondicion = null;		
			CondicionEspecial condicion = entidadService.getReference(CondicionEspecial.class, condicionContinuity.getCondicionEspecialId());
			//int obligatoria = 0;
			if (condicionContinuity.getCondEspInciso().hasRecordsInProcess()) {
				bitemporalCondicion = condicionContinuity.getCondEspInciso().getInProcess(validOn);					
			}
			
			if (bitemporalCondicion != null) {
				CondicionEspecialBitemporalDTO condicionDTO = new CondicionEspecialBitemporalDTO();
				condicionDTO.setCondicionEspecial(condicion);
				condicionDTO.setIdCondicionBitemporal(bitemporalCondicion.getId());
				condicionDTO.setIdContinuity(bitemporalCondicion.getContinuity().getId());
				condicionDTO.setTipoCondicion(NivelAplicacion.INCISO.getNivel());
				condicionDTO.setObligatoria( getObligatoriedad(incisoContinuityId , condicion.getId() , NivelAplicacion.INCISO ) );
				
				lstCondicionesDTO.add(condicionDTO);
			}
		}		
		
		return lstCondicionesDTO;
	}
	
	@Override
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesIncisoAsociadas(Long incisoContinuityId, Date validoEn) {	
		List<CondicionEspecialBitemporalDTO> lstCondicionesDTO = new ArrayList<CondicionEspecialBitemporalDTO>();
		
		DateTime validOn = TimeUtils.getDateTime(validoEn);
		
		Collection<CondicionEspIncContinuity> lstCondicionContinuity = 
				entidadContinuityService.findContinuitiesByParentKey(CondicionEspIncContinuity.class, 
																	 CondicionEspIncContinuity.PARENT_KEY_NAME,
																	 incisoContinuityId);
		
		for (CondicionEspIncContinuity condicionContinuity : lstCondicionContinuity) {
			BitemporalCondicionEspInc bitemporalCondicion = null;		
			CondicionEspecial condicion = entidadService.getReference(CondicionEspecial.class, condicionContinuity.getCondicionEspecialId());

			if (condicionContinuity.getCondEspInciso().hasRecordsInProcess()) {
				bitemporalCondicion = condicionContinuity.getCondEspInciso().getInProcess(validOn);					
			} else {
				bitemporalCondicion = condicionContinuity.getCondEspInciso().get(validOn);
			}
			
			if (bitemporalCondicion != null) {
				CondicionEspecialBitemporalDTO condicionDTO = new CondicionEspecialBitemporalDTO();
				condicionDTO.setCondicionEspecial(condicion);
				condicionDTO.setIdCondicionBitemporal(bitemporalCondicion.getId());
				condicionDTO.setIdContinuity(bitemporalCondicion.getContinuity().getId());
				condicionDTO.setTipoCondicion(NivelAplicacion.INCISO.getNivel());
				condicionDTO.setObligatoria(getObligatoriedad(incisoContinuityId, condicion.getId(),  NivelAplicacion.INCISO));
				
				lstCondicionesDTO.add(condicionDTO);
			}
		}		
		
		return lstCondicionesDTO;
	}
	
	
	@Override 
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionEspIncInProcess(Long incisoContinuityId, Date validoEn) {	
		
		List<CondicionEspecialBitemporalDTO> lstCondicionesDTO 			= new ArrayList<CondicionEspecialBitemporalDTO>();
		BitemporalCondicionEspInc bitemporalCondicionEspInc 			= null;
		Collection<CondicionEspIncContinuity> lstCondicionContinuity	= null;
		CondicionEspecial condicion 									= null;
		CondicionEspecialBitemporalDTO condicionDTO						= null;
		DateTime validOn 												= TimeUtils.getDateTime(validoEn);
		
		lstCondicionContinuity = entidadContinuityService.findContinuitiesByParentKey(CondicionEspIncContinuity.class, 
								 CondicionEspIncContinuity.PARENT_KEY_NAME,
								 incisoContinuityId);
		
		for (CondicionEspIncContinuity condicionContinuity : lstCondicionContinuity) {
			bitemporalCondicionEspInc = null;
			condicion = entidadService.getReference(CondicionEspecial.class, condicionContinuity.getCondicionEspecialId());
			if (condicionContinuity.getCondEspInciso().hasRecordsInProcess()) {
				bitemporalCondicionEspInc = condicionContinuity.getCondEspInciso().getInProcess(validOn);					
			} 
			
			if (bitemporalCondicionEspInc != null) {
				if( getObligatoriedad(incisoContinuityId , condicion.getId(), NivelAplicacion.INCISO ).equals(0) ){
					condicionDTO = new CondicionEspecialBitemporalDTO();
					condicionDTO.setIdContinuity(bitemporalCondicionEspInc.getContinuity().getId());
					lstCondicionesDTO.add(condicionDTO);
				}
			}
		}		
		
		return lstCondicionesDTO;
	}
	
	
	
	
	@Override
	public void guardarCondicionInciso(Long incisoContinuityId, CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn) {
		BitemporalCondicionEspInc bitemporalCondicion = new BitemporalCondicionEspInc();
		
		Usuario usuario = usuarioService.getUsuarioActual();
		
		IncisoContinuity incisoContinuity = entidadContinuityService.findContinuityByKey(IncisoContinuity.class, incisoContinuityId);
		
		bitemporalCondicion.getContinuity().setParentContinuity(incisoContinuity);
		bitemporalCondicion.getContinuity().setCondicionEspecialId(condicionBitemporalDTO.getCondicionEspecial().getId());
		
		bitemporalCondicion.getValue().setNombreUsuarioCreacion(usuario.getNombreUsuario());
		bitemporalCondicion.getValue().setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		
		entidadBitemporalService.saveInProcess(bitemporalCondicion, TimeUtils.getDateTime(validoEn));
		
	}
	

	@Override
	public void eliminarCondicionIncisoInclusionEndoso(CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn) {
		
		BitemporalCondicionEspInc bitemporalCondicion 							= null;
		Collection <CondicionEspIncContinuity> listCondicionEspIncContinuity 	= new ArrayList<CondicionEspIncContinuity>();
		
		listCondicionEspIncContinuity = entidadContinuityService.findContinuitiesByParentKey(CondicionEspIncContinuity.class, 
				CondicionEspIncContinuity.PARENT_KEY_NAME,
				condicionBitemporalDTO.getIdContinuity());
		
		
		for(CondicionEspIncContinuity condicionEspIncContinuity:listCondicionEspIncContinuity){
	     	   
   			if( condicionEspIncContinuity.getCondEspInciso().hasRecordsInProcess() ){
   		
   				bitemporalCondicion = condicionEspIncContinuity.getCondEspInciso().getInProcess( TimeUtils.getDateTime( validoEn) );
   				if( bitemporalCondicion.getContinuity().getCondicionEspecialId() == condicionBitemporalDTO.getCondicionEspecial().getId() ){
   					entidadBitemporalService.remove(bitemporalCondicion, TimeUtils.getDateTime(validoEn));
   					break;
   				}
   			}
           }
		
	}
	
    @Override
    public void eliminarCondicionInciso(CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn) {
          BitemporalCondicionEspInc bitemporalCondicion = entidadBitemporalService.getInProcessByKey(CondicionEspIncContinuity.class,
                      condicionBitemporalDTO.getIdContinuity(), TimeUtils.getDateTime(validoEn));
                      
          entidadBitemporalService.remove(bitemporalCondicion, TimeUtils.getDateTime(validoEn));
          
    }

	
	
    @Override
	public void eliminarCondicionIncisoBajaEndoso(CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn) {
		
		BitemporalCondicionEspInc bitemporalCondicion 							= null;
		Collection <CondicionEspIncContinuity> listCondicionEspIncContinuity 	= new ArrayList<CondicionEspIncContinuity>();
		
		listCondicionEspIncContinuity = entidadContinuityService.findContinuitiesByParentKey(CondicionEspIncContinuity.class, 
				CondicionEspIncContinuity.PARENT_KEY_NAME,
				condicionBitemporalDTO.getIdContinuity());
		
		
		for (CondicionEspIncContinuity condicionEspIncContinuity:listCondicionEspIncContinuity) {
	     	   
   			if (condicionEspIncContinuity.getCondicionEspecialId() == condicionBitemporalDTO.getCondicionEspecial().getId()) {
   		
   				bitemporalCondicion = condicionEspIncContinuity.getCondEspInciso().get( TimeUtils.getDateTime( validoEn) );
   				if (bitemporalCondicion != null) {
   					entidadBitemporalService.remove(bitemporalCondicion, TimeUtils.getDateTime(validoEn));
   					break;
   				}
   			
           }
		}
		
	}
    
    
    @Override
	public void asociarNuevamenteCondicionIncisoBajaEndoso(CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn) {
		
		BitemporalCondicionEspInc bitemporalCondicion 							= null;
		Collection <CondicionEspIncContinuity> listCondicionEspIncContinuity 	= new ArrayList<CondicionEspIncContinuity>();
		
		listCondicionEspIncContinuity = entidadContinuityService.findContinuitiesByParentKey(CondicionEspIncContinuity.class, 
				CondicionEspIncContinuity.PARENT_KEY_NAME,
				condicionBitemporalDTO.getIdContinuity());
		
		
		for (CondicionEspIncContinuity condicionEspIncContinuity:listCondicionEspIncContinuity) {
	     	   
   			if (condicionEspIncContinuity.getCondicionEspecialId() == condicionBitemporalDTO.getCondicionEspecial().getId()) {
   		
   				bitemporalCondicion = condicionEspIncContinuity.getCondEspInciso().get( TimeUtils.getDateTime( validoEn) );
   				if (bitemporalCondicion != null) {
   					
   					if( bitemporalCondicion.getRecordStatus().equals( RecordStatus.TO_BE_ADDED)){
   						entidadBitemporalService.remove(bitemporalCondicion,  TimeUtils.getDateTime(validoEn));
   					}
   					else if ( bitemporalCondicion.getRecordStatus().equals( RecordStatus.TO_BE_ENDED) ) {
   						//bitemporalCondicion.setRecordStatus(RecordStatus.TO_BE_ADDED);
   						entidadBitemporalService.rollBackStatusContinuityCotizacion( bitemporalCondicion.getContinuity().getId(),NivelAplicacion.INCISO.getNivel().intValue());
   	   					//entidadBitemporalService.saveValid(bitemporalCondicion, TimeUtils.getDateTime(validoEn));
   	   					
   					}
   					
   					
   				}
   			
           }
		}
		
	}
	
	@Override
	public void asociarCondicionesInciso(Long incisoContinuityId, Date validoEn, String nombreCondicion) {
		List<CondicionEspecialBitemporalDTO> lstCondicionesDTO = new ArrayList<CondicionEspecialBitemporalDTO>();
		
		if (nombreCondicion != null && !nombreCondicion.equals("")) {
			lstCondicionesDTO = obtenerCondicionesIncisoNombreCodigo(nombreCondicion, incisoContinuityId, validoEn);
		} else {
			lstCondicionesDTO = obtenerCondicionesDisponiblesInciso(lstCondicionesDTO, incisoContinuityId, validoEn);
		}
		
		for (CondicionEspecialBitemporalDTO condicionDTO : lstCondicionesDTO) {
			guardarCondicionInciso(incisoContinuityId, condicionDTO, validoEn);
		}
		
	}
	
	@Override
	public void eliminarCondicionesInciso(Long incisoContinuityId, Date validoEn) {
		List<CondicionEspecialBitemporalDTO> lstCondicionesDTO = obtenerCondicionesIncisoAsociadas(incisoContinuityId, validoEn);
		
		for (CondicionEspecialBitemporalDTO condicionDto: lstCondicionesDTO) {
			
			if (condicionDto.getObligatoria() == 0) {
				eliminarCondicionInciso(condicionDto, validoEn);
			}
			
		}
		
	}
	

	@Override
	public void generarCondicionesBaseInciso(BitemporalCotizacion cotizacion, DateTime validoEn) {		
				
		List<NegocioCondicionEspecial> list = negocioCondicionEspecialService.obtenerCondicionesBase(
				cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio(), NivelAplicacion.INCISO);
		
		if (list != null && !list.isEmpty()) {
			Usuario usuario = usuarioService.getUsuarioActual();		
			
			//Collection<BitemporalInciso> incisos = cotizacion.getContinuity().getIncisoContinuities().getOnlyInProcess(validoEn);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("continuity.cotizacionContinuity.id", cotizacion.getContinuity().getId());		
			List<BitemporalInciso> incisos = (List<BitemporalInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalInciso.class, 
					params, validoEn, null, true);

			for (BitemporalInciso inciso : incisos) {
				for (NegocioCondicionEspecial negocioCondicion : list) {
					
					boolean add = true;
					//Collection<BitemporalCondicionEspInc> condiciones =  inciso.getContinuity().getCondicionEspIncContinuities().getOnlyInProcess(validoEn);
					Map<String, Object> params2 = new HashMap<String, Object>();
					params.put("continuity.incisoContinuity.id", inciso.getContinuity().getId());		
					List<BitemporalCondicionEspInc> condiciones = (List<BitemporalCondicionEspInc>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalCondicionEspInc.class, 
							params2, validoEn, null, true);
				
					//Validar que la condicion especial no se encuentra existente para el inciso
					for (BitemporalCondicionEspInc condicionInciso: condiciones) {
						if (condicionInciso.getContinuity().getCondicionEspecialId() == negocioCondicion.getCondicionEspecial().getId()) {
							add = false;
						}
					}
					
					if (add) {
						BitemporalCondicionEspInc bitemporalCondicion = new BitemporalCondicionEspInc();			
						
						bitemporalCondicion.getContinuity().setParentContinuity(inciso.getContinuity());
						bitemporalCondicion.getContinuity().setCondicionEspecialId(negocioCondicion.getCondicionEspecial().getId());
						
						bitemporalCondicion.getValue().setNombreUsuarioCreacion(usuario.getNombreUsuario());
						bitemporalCondicion.getValue().setCodigoUsuarioCreacion(usuario.getNombreUsuario());
						
						entidadBitemporalService.saveInProcess(bitemporalCondicion, validoEn);
					}
					
					
				}
			}
		}
		
		
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<BitemporalCondicionEspInc> obtenerCondicionesIncisoContinuity(
			Long cotizacionContinuityId) {
		// TODO Auto-generated method stub
		List<BitemporalCondicionEspInc> bitCondicionesEspInc = new ArrayList<BitemporalCondicionEspInc>();
		List<EntidadBitemporal> listCondEspBit =(List<EntidadBitemporal>) entidadBitemporalService.findByParentKey(CondicionEspIncContinuity.class, CondicionEspIncContinuity.PARENT_KEY_NAME, cotizacionContinuityId);
		for(EntidadBitemporal con: listCondEspBit){
			bitCondicionesEspInc.add((BitemporalCondicionEspInc)con);
		}
		return bitCondicionesEspInc;
	}

	@Override
	public List<BitemporalCondicionEspInc> obtenerCondicionesIncisoCotizacion(
			Long cotizacionContinuityId, Date validoEn) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BitemporalCondicionEspCot> obtenerCondicionesNivelCotizacion(
			Long cotizacionContinuityId, Date validoEn) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<BitemporalCondicionEspCot> obtenerCondicionesNivelCotizacion(
			Long cotizacionContinuityId) {
		// TODO Auto-generated method stub
		List<BitemporalCondicionEspCot> bitCondicionesEspCot = new ArrayList<BitemporalCondicionEspCot>();
		List<EntidadBitemporal> listCondEspBit =(List<EntidadBitemporal>) entidadBitemporalService.findByParentKey(CondicionEspCotContinuity.class, CondicionEspCotContinuity.PARENT_KEY_NAME, cotizacionContinuityId);
		for(EntidadBitemporal con: listCondEspBit){
			bitCondicionesEspCot.add((BitemporalCondicionEspCot)con);
		}
		return bitCondicionesEspCot;
	}

	@Override
	public void guardarCondicionInciso(Long incidoContinuityId,
			CondicionEspecial condicion, Date validoEn) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public Collection<BitemporalCondicionEspInc> obtenerCondicionesIncisoContinuity(
			Long incisoContinuityId,DateTime validFromDT, DateTime recordFromDT) {

		Collection<BitemporalCondicionEspInc> condEspBit = new ArrayList<BitemporalCondicionEspInc>();
		
		Collection<CondicionEspIncContinuity> lstCondicionContinuity = 
			entidadContinuityService
				.findContinuitiesByParentKey(
						CondicionEspIncContinuity.class, 
						CondicionEspIncContinuity.PARENT_KEY_NAME,
						incisoContinuityId);
		
			for (CondicionEspIncContinuity condicionContinuity : lstCondicionContinuity) {
				BitemporalCondicionEspInc bitemporalCondicion = condicionContinuity.getCondEspInciso().get(validFromDT, recordFromDT);
				condEspBit.add(bitemporalCondicion);
				condicionContinuity.getIncisoContinuity().getIncisos().get(validFromDT, recordFromDT);
			}
		
		return condEspBit;
	}
	
	
	
	
    /**
     * Recupera el incisoContinuytiId en base a su IdCotizacionContinuity y fecha 
     * @param incisoABuscar
     * @param incisoContinuityId
     * @param validoEn
     * @return
     */
	@Override
     public Long getIdIncisoContinuity(int incisoABuscar , Long cotizacionContinuityId, Date validoEn){
           
           Collection<BitemporalInciso> incisos = incisoViewService.getLstIncisoByCotizacion( cotizacionContinuityId , validoEn);
           
           long inciso = 0;
           
           for(BitemporalInciso inc:incisos){
                 
                 if(inc.getContinuity().getNumero() == incisoABuscar){
                	 
                       inciso = inc.getContinuity().getId();
                       inc.getId();
                       break;
                 }
           }
           
           return inciso;
     }
	
	@Override
	public List<CondicionEspecial> obtenerCondicionesEspecialesIncisos(
			BitemporalCotizacion bitempCotizacion, DateTime validOnDT,
			DateTime recordFromDT) throws IOException {

		List<CondicionEspecial> condiciones = new ArrayList<CondicionEspecial>();
		try{
//			Collection<BitemporalInciso> bitemporalesInc = 
//			incisoBitemporalService.getIncisoList(bitempCotizacion.getContinuity()); 
			
			Collection<IncisoContinuity> incisoContinuityLst = entidadContinuityService.findContinuitiesByParentKey(
					IncisoContinuity.class, IncisoContinuity.PARENT_KEY_NAME, bitempCotizacion.getContinuity().getId());
		
			for(IncisoContinuity incisoContinuity : incisoContinuityLst) {
				BitemporalInciso bitempInciso = incisoContinuity.getIncisos().get(validOnDT, recordFromDT);
				
				if (bitempInciso != null) {
					Collection<BitemporalCondicionEspInc> bitemporalCondicionEspInc = 
						obtenerCondicionesIncisoContinuity(bitempInciso.getContinuity().getId(), validOnDT, recordFromDT);
		
					for (BitemporalCondicionEspInc item : bitemporalCondicionEspInc) {
						if( item != null ){
							CondicionEspecial condicion = condicionEspecialService.obtenerCondicion(item.getContinuity().getCondicionEspecialId());
							if (!condiciones.contains(condicion)) {
								condiciones.add(condicion);
							}
						}
					}
				}
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return condiciones;
	}
	
	@Override
	public List<CondicionEspecial> obtenerCondicionesEspecialesInciso(
			BitemporalCotizacion bitemporalCotizacion,
			String numeroInc, DateTime validOnDT,
			DateTime recordFromDT) throws IOException {

		List<CondicionEspecial> condiciones = new ArrayList<CondicionEspecial>();
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("numero", Integer.parseInt(numeroInc));
		params.put(IncisoContinuity.PARENT_KEY_NAME, bitemporalCotizacion.getContinuity().getId());

		List<IncisoContinuity> lstIncisos = entidadService.findByProperties(IncisoContinuity.class, params);
		
		if (!lstIncisos.isEmpty()) {
			Collection<BitemporalCondicionEspInc> lstCondicionBitemporal = lstIncisos.get(0).getCondicionEspIncContinuities().get(validOnDT, recordFromDT);
			
			for( BitemporalCondicionEspInc bitemporalCondicion : lstCondicionBitemporal ){
					CondicionEspecial condicion = condicionEspecialService.obtenerCondicion(bitemporalCondicion.getContinuity().getCondicionEspecialId());
					if (!condiciones.contains(condicion)) {
						condiciones.add(condicion);
					}
			}
			
		}		
		
		return condiciones;
	}
	
	@Override
	public List<CondicionEspecial> obtenerCondicionesEspecialesPoliza(
				BitemporalCotizacion bitempCotizacion, 
				DateTime validOn,
				DateTime recordFrom)
				throws IOException {

		Long cotizacionContinuityId = bitempCotizacion.getContinuity().getId();
		List<CondicionEspecial> condiciones = new ArrayList<CondicionEspecial>();

		Collection<CondicionEspCotContinuity> condicionesCotContinuities = entidadContinuityService
				.findContinuitiesByParentBusinessKey(
						CondicionEspCotContinuity.class,
						CondicionEspCotContinuity.PARENT_KEY_NAME,
						cotizacionContinuityId);

		for (CondicionEspCotContinuity item : condicionesCotContinuities) {
			BitemporalCondicionEspCot condicionEspBit = item
					.getCondicionesEspecialesCotizacion().get(validOn,
							recordFrom);

			if (condicionEspBit != null) {
				Long idCondicion = condicionEspBit.getContinuity().getCondicionEspecialId();
				condiciones.add(condicionEspecialService.obtenerCondicion(idCondicion));
			}
		}
		return condiciones;
	}


}

