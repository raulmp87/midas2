package mx.com.afirme.midas2.service.impl.catalogos;

import mx.com.afirme.midas2.domain.catalogos.GrupoVariablesModificacionPrima;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.catalogos.GrupoVariablesModificacionPrimaDao;
import mx.com.afirme.midas2.service.catalogos.GrupoVariablesModificacionPrimaService;

@Stateless
public class GrupoVariablesModificacionPrimaServiceImpl implements
		GrupoVariablesModificacionPrimaService {

	@Override
	public List<GrupoVariablesModificacionPrima> findAll() {
		return grupoDao.findAll();
	}
	
	@Override
	public GrupoVariablesModificacionPrima findById(Long id) {
		return grupoDao.findById(id);
	}
	

	private GrupoVariablesModificacionPrimaDao grupoDao;
	
	@EJB
	public void setGrupoDao(GrupoVariablesModificacionPrimaDao grupoDao) {
		this.grupoDao = grupoDao;
	}
	
	
	public GrupoVariablesModificacionPrima findByClave(Integer clave) {
		
		GrupoVariablesModificacionPrima filtroGrupoVariablesModificacionPrima = new GrupoVariablesModificacionPrima();
		
		filtroGrupoVariablesModificacionPrima.setClave(clave);
	
		List<GrupoVariablesModificacionPrima> descGrupoVariablesModificacionPrimaList =  grupoDao.findByFilters(filtroGrupoVariablesModificacionPrima);
		if (descGrupoVariablesModificacionPrimaList != null && descGrupoVariablesModificacionPrimaList.size() > 0) {
			return descGrupoVariablesModificacionPrimaList.get(0);
		}
		return null;
	}
	
	public List<GrupoVariablesModificacionPrima> findByFilters(GrupoVariablesModificacionPrima filtroGpoVarModifPrima) {
		
	if(filtroGpoVarModifPrima==null){	
		filtroGpoVarModifPrima = new GrupoVariablesModificacionPrima();
	 }
		return  grupoDao.findByFilters(filtroGpoVarModifPrima);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remove(GrupoVariablesModificacionPrima grupoVariablesModificacionPrima) {
		grupoVariablesModificacionPrima = grupoDao.getReference(grupoVariablesModificacionPrima.getId());
		grupoDao.remove(grupoVariablesModificacionPrima);
	}
	
	public GrupoVariablesModificacionPrima save(GrupoVariablesModificacionPrima grupoVariablesModificacionPrima) {
		
		if (grupoVariablesModificacionPrima.getId() != null) {
			return grupoDao.update(grupoVariablesModificacionPrima);
		} else {
			grupoDao.persist(grupoVariablesModificacionPrima);
			return grupoVariablesModificacionPrima;
		}
	}

	
}
