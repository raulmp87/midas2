package mx.com.afirme.midas2.service.impl.seguridad.filler.consultareaseguro;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

public class PaginaPermisoConsultaReaseguro {
    private List<Permiso> listaPermiso = new ArrayList<Permiso>();
    private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();

    public PaginaPermisoConsultaReaseguro(List<Permiso> listaPermiso) {
	this.listaPermiso = listaPermiso;
    }

    private Pagina nuevaPagina(String nombrePaginaJSP, String nombreAccionDo) {
	return new Pagina(new Integer("1"), nombrePaginaJSP.trim(),
		nombreAccionDo.trim(), "Descripcion pagina");
    }
    
    public List<PaginaPermiso> obtienePaginaPermisos() {
	PaginaPermiso pp;
	//***** ACERCA DE **********//
	pp = new PaginaPermiso(); 
	pp.setPagina(nuevaPagina("acerca.jsp","/MidasWeb/sistema/mensajePendiente/mostrarAcerca.do"));  
	listaPaginaPermiso.add(pp); 
	
	pp = new PaginaPermiso();
    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
    listaPaginaPermiso.add(pp);
    
    pp = new PaginaPermiso();
    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptSinReservaPendienteAcum.do"));
    listaPaginaPermiso.add(pp);
    
    pp = new PaginaPermiso();
    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptSinReservaPendienteAcumDet.do"));
    listaPaginaPermiso.add(pp);
    
    

	return this.listaPaginaPermiso;
    }


}
