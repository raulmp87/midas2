package mx.com.afirme.midas2.service.impl.jobAgentes;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import mx.com.afirme.midas2.domain.jobAgentes.TareaProgramada;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.impl.jobAgentes.ProgramacionCalculoBonosServiceImpl.conceptoEjecucionAutomatica;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionAniversarioAgenteService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class ProgramacionAniversarioAgenteServiceImpl implements ProgramacionAniversarioAgenteService{
	
	
	@EJB
	private AgenteMidasService agenteMidasService;
	@PersistenceContext private EntityManager entityManager;
	@Resource	
	private TimerService timerService;	
	@EJB
	private SistemaContext sistemaContext;
	public static final Logger LOG = Logger.getLogger(ProgramacionAniversarioAgenteServiceImpl.class);
		
	
	@Override
	public List<TareaProgramada> getTaskToDo(String conceptoEjecucionAutomatica) {
		String queryString = new String(
				" SELECT a.id,a.fechaalta " +
				" from MIDAS.toagente a " +
				" where a.fechaalta is not null " +
				" and EXTRACT(month FROM a.fechaalta) = :month " +
				" and   EXTRACT(day FROM a.fechaalta) = :day " +
				" AND a.IDSITUACIONAGENTE = (select sitAgente.ID "+
                                    " from MIDAS.toValorCatalogoAgentes sitAgente "+  
									" where sitAgente.GRUPOCATALOGOAGENTES_ID = (select tcgrupagente.ID  "+
                                                                        " from MIDAS.tcgrupocatalogoagentes tcgrupagente "+  
                                                                        " where descripcion = 'Estatus de Agente (Situacion)') "+
                                     " and sitAgente.VALOR ='AUTORIZADO')");
		Calendar today = Calendar.getInstance();
		queryString = queryString.replace(":month",today.get(Calendar.MONTH)+1+"");
		queryString = queryString.replace(":day",today.get(Calendar.DAY_OF_MONTH)+"");
		Query query = entityManager.createNativeQuery(queryString);
		@SuppressWarnings("unchecked")
		List<Object> result = query.getResultList();
		if (result != null && !result.isEmpty()) {
			List<TareaProgramada> listTask = makeListTask(result);
			return listTask;
		}
		return null;
	}

	@Override
	public void executeTasks() {

		LOG.info("Ejecutando ProgramacionAniversarioAgenteService.executeTasks()...");
		
		List<TareaProgramada> tasks=getTaskToDo(conceptoEjecucionAutomatica.BONOS.getValue());
		if (tasks != null) {
			for (TareaProgramada task : tasks) {
				Map<String, Map<String, List<String>>> mapCorreos = agenteMidasService
						.obtenerCorreos(task.getId(),
								GenericMailService.P_ANIVERSARIO,
								GenericMailService.M_CUMPLIMENTO_FECHA_ANIVERSARIO_AGENTE);
				agenteMidasService.enviarCorreo(mapCorreos, null, null,
						GenericMailService.T_FELIZ_ANIVERSARIO,null);
			}
		}
	}
	
	/**
	 * Crea la lista de Tareas Pramadas a ejecutar
	 * @param result
	 * @return
	 */
	private List<TareaProgramada> makeListTask(List<Object> result){
		List<TareaProgramada> listResultAgente = new LinkedList<TareaProgramada>();
		for (Object item : result) {
			Object[] array = (Object[])item;
			if (array[1] != null) {
				TareaProgramada tareaProgramada = new TareaProgramada();
				BigDecimal id = (BigDecimal)array[0];
				tareaProgramada.setId(id.longValue());
				tareaProgramada.setFechaEjecucion((Date)array[1]);
				listResultAgente.add(tareaProgramada);
			} 
		}
		return listResultAgente;
	}
	
	public void initialize() {
		String timerInfo = "TimerProgramacionAniversarioAgente";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0 12 * * ?
				expression.minute(0);
				expression.hour(12);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerProgramacionAniversarioAgente", false));
				
				LOG.info("Tarea TimerProgramacionAniversarioAgente configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerProgramacionAniversarioAgente");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerProgramacionAniversarioAgente:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		executeTasks();
	}
}
