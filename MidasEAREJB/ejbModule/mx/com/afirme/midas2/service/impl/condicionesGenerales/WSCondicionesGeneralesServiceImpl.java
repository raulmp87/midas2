package mx.com.afirme.midas2.service.impl.condicionesGenerales;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgCondiciones;
import mx.com.afirme.midas2.service.condicionesGenerales.WSCondicionesGeneralesService;
import mx.com.afirme.midas2.service.fortimax.FortimaxV2Service;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;


@Stateless
public class WSCondicionesGeneralesServiceImpl implements WSCondicionesGeneralesService{	

	private static final String NOMBRE_STORE = "MIDAS.PKG_CONDICIONES_GRALES.SPCONSULTACG";
	private FortimaxV2Service fortimaxV2Service;
	
	@EJB
    public void setFortimaxV2Service(FortimaxV2Service fortimaxV2Service) {
        this.fortimaxV2Service = fortimaxV2Service;
    }
    
	@Override
	public byte[] consultarCondicionesGenerales( Long idPoliza, String apSolicita ) throws Exception{
		
		try {
			
			List<CgCondiciones> list = this.obtieneLista( idPoliza, apSolicita );
			List<byte[]> listBytes = new ArrayList<byte[]>();
			
			for ( CgCondiciones item: list ){
				byte byteArray[] = fortimaxV2Service.downloadFileBP(item.getFortimaxDocNombre(), "D", item.getFortimaxDocGaveta(), Long.valueOf(item.getFortimaxDocId()));
				listBytes.add(byteArray);
			}
			
			if (listBytes.size()>0){
				return this.concatenarDocumentos(listBytes);
			}

		} catch (Exception e) {
			throw e;
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private List<CgCondiciones> obtieneLista( Long idPoliza, String apSolicita ) throws Exception{
		
		List<CgCondiciones> list = new ArrayList<CgCondiciones>();		
		StoredProcedureHelper storedHelper = null;
		
		String [] atributosDTO = { "nombre","numero_registro","fecha_inicio","fecha_fin","activo","portal","ed","fortimaxDocId","fortimaxDocNombre","fortimaxDocGaveta","fortimaxDocCarpeta" };
		String [] columnasCursor = { "NOMBRE","NUMERO_REGISTRO","FECHA_INICIO","FECHA_FIN","ACTIVO","PORTAL","ED","FORTIMAXDOC_ID","FORTIMAXDOC_NOMBRE","FORTIMAXDOC_GAVETA","FORTIMAXDOC_CARPETA" };
		
		storedHelper = new StoredProcedureHelper(NOMBRE_STORE,StoredProcedureHelper.DATASOURCE_MIDAS);
		storedHelper.estableceParametro( "pIdPoliza", idPoliza );			
		storedHelper.estableceParametro( "apsolicita", apSolicita );
		storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.domain.condicionesGenerales.CgCondiciones", atributosDTO, columnasCursor);
		
		list = (List<CgCondiciones>)storedHelper.obtieneListaResultados();
		return list;		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private byte[] concatenarDocumentos(List<byte[]> listaDocByte) throws DocumentException, IOException{

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		int pageOffset = 0;
		ArrayList master = new ArrayList();
		int f = 0;
		Document document = null;
		PdfCopy  writer = null;
		int rptsLenght = 0;
		for (byte[] rptEnTurno : listaDocByte) {
			rptsLenght += rptEnTurno.length;
			PdfReader reader = new PdfReader(rptEnTurno);
			reader.consolidateNamedDestinations();
			int n = reader.getNumberOfPages();
			List bookmarks = SimpleBookmark.getBookmark(reader);
			if (bookmarks != null) {
				if (pageOffset != 0)
					SimpleBookmark.shiftPageNumbers
					(bookmarks, pageOffset, null);
				master.addAll(bookmarks);
			}
			pageOffset += n;
			if (f == 0) {                      
				document = new Document(reader.getPageSizeWithRotation(1));                  
				writer = new PdfCopy(document, byteArrayOutputStream);
				document.open();     
			}
			PdfImportedPage page;
			for (int i = 0; i < n;){
				++i;
				page = writer.getImportedPage(reader, i);
				writer.addPage(page);
			}
			PRAcroForm form = reader.getAcroForm();
			if (form != null)
				writer.copyAcroForm(reader);
			f++;
		}
		if (!master.isEmpty())
			writer.setOutlines(master);
		
		document.close();
		byte byteArray[] = byteArrayOutputStream.toByteArray();

		return byteArray;
	}


}
