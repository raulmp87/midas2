package mx.com.afirme.midas2.service.impl.compensaciones.reportes;

// default package

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;


import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import mx.com.afirme.midas2.domain.compensaciones.CaReportesDTO;
import mx.com.afirme.midas2.service.compensaciones.reportes.ReportesCompensacionesService;
import mx.com.afirme.midas2.service.siniestros.solicitudcheque.SolicitudChequeService;
import oracle.jdbc.driver.OracleTypes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

/**
 * Facade for entity LiquidacionCompensaciones.
 * @see .LiquidacionCompensaciones
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ReportesCompensacionesServiceImpl  implements ReportesCompensacionesService {
	//property constants
	
    @PersistenceContext private EntityManager entityManager;
	

/** Log de ReportesCompensacionesServiceImpl */
private static final Logger LOG = LoggerFactory
		.getLogger(ReportesCompensacionesServiceImpl.class);
    @EJB 
	private SolicitudChequeService solicitudChequeService ;
    
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall reporteCompensacionesAdicionales;
	private SimpleJdbcCall obtenerReporte;
	
	@SuppressWarnings("unused")
	private DataSource dataSource;
	

	@Resource(name="jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {						
		//Consult agruparOrden en PKG_CA_REPORTES de MIDAS
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.reporteCompensacionesAdicionales = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("PKG_CA_REPORTES")
		.withProcedureName("reporteCompensacionesAdicionales")
		.declareParameters(
				new SqlParameter("pRamo", Types.VARCHAR),
				new SqlParameter("pFecIni", Types.DATE),
				new SqlParameter("pFecFin", Types.DATE),
				new SqlParameter("pAgente", Types.NUMERIC),
				new SqlParameter("pPromotor", Types.NUMERIC),
				new SqlParameter("pProveedor", Types.NUMERIC),
				new SqlParameter("pContratante", Types.VARCHAR),
				new SqlParameter("pGerencia", Types.NUMERIC),
				new SqlParameter("pNegocio", Types.NUMERIC),
				new SqlParameter("pGrupo", Types.NUMERIC),
				new SqlParameter("pTipoCom", Types.VARCHAR),
				new SqlParameter("pUsuario", Types.VARCHAR),
				new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
				new SqlOutParameter("pDesc_Resp", Types.VARCHAR),	
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Object>() {
					@Override
					public CaReportesDTO mapRow(ResultSet rs, int index) throws SQLException {
						CaReportesDTO ordenPagos = new CaReportesDTO();
						//ordenPagos.setId(rs.getLong("ID"));
						//ordenPagos.setTipobeneficiario(rs.getString("TIPO_BENEFICIARIO"));
						//ordenPagos.setClaveNombre(rs.getString("CLAVE_NOMBRE"));
						//ordenPagos.setImportePrima(rs.getDouble("IMPORTE_PRIMA"));
						//ordenPagos.setImporteBs(rs.getDouble("IMPORTE_BS"));
						//ordenPagos.setImporteDerpol(rs.getDouble("IMPORTE_DERPOL"));
						//ordenPagos.setImporteCumMeta(rs.getDouble("IMPORTE_CUM_META"));
						//ordenPagos.setImporteUtilidad(rs.getDouble("IMPORTE_UTILIDAD"));
						//ordenPagos.setImporteTotal(rs.getDouble("TOTAL"));
						return ordenPagos;
					}
		}));		
		
		//Consult excluirOrdenPago en PKG_CA_CONTABILIDAD de MIDAS
		this.obtenerReporte = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("PKG_CA_REPORTES")
		.withProcedureName("obtenerReporte")
		.declareParameters(
				new SqlParameter("pFechaIni", Types.DATE),
				new SqlParameter("pFechaFin", Types.DATE),
				new SqlParameter("pNomAgente", Types.VARCHAR),
				new SqlParameter("pNomPromotor", Types.VARCHAR),
				new SqlParameter("pNomProveedor", Types.VARCHAR),
				new SqlParameter("pCveAgente", Types.NUMERIC),
				new SqlParameter("pCvePromotor", Types.NUMERIC),
				new SqlParameter("pCveProveedor", Types.NUMERIC),
				new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
				new SqlOutParameter("pDesc_Resp", Types.VARCHAR),	
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Object>() {
					@Override
					public CaReportesDTO mapRow(ResultSet rs, int index) throws SQLException {
						/* IDENTIFICADOR_BEN_ID,CVE_TIPO_ENTIDAD,DOMICILIO,COLONIA,CODIGOPOSTAL,PROMOTORIA,
                    GERENCIA,EJECUTIVO,CENTROOPERACION,NUMEROCEDULA,NOMBRE_OFICINA,FECHA_TRAMITE,IMPORTE_TOTAL,
                    0 AS SALDOINICIAL, 0 AS COMGRABADASIVA, 0 AS COMEXTERNASIVA, SUBTOTAL,IVA,IVA_RETENIDO,ISR,NETO_POR_PAGAR  0 AS SALDO*/
						CaReportesDTO reportesCa = new CaReportesDTO();
						reportesCa.setIdentificador_ben_id(rs.getLong("IDENTIFICADOR_BEN_ID"));

						return reportesCa;
					}
		}));
	}
	
	@SuppressWarnings("unchecked")
	public List<CaReportesDTO> filtrarReporteCompensaciones(CaReportesDTO  param) {
		/*List<OrdenesPagoDTO> lstOrden = null;
		Map<String, Object> execute = null;
			try {
				if (param != null) {
					LOG.info("pRamo=>"+param.getRamo());
				    LOG.info("pFecIni=>"+param.getFechaInicio());
				    LOG.info("pFecFin=>"+param.getFechaFin());
				    LOG.info("pAgente=>"+param.getAgente());
				    LOG.info("pPromotor=>"+param.getPromotor());
				    LOG.info("pProveedor=>"+param.getProveedor());
				    LOG.info("pContratante=>"+param.getContratante());
				    LOG.info("pGerencia=>"+param.getGerencia());
				    LOG.info("pNegocio=>"+param.getNegocio());
				    LOG.info("pGrupo=>"+param.getGrupo());
				    LOG.info("pTipoCom=>"+param.getConcepto());
				    LOG.info("pUsuario=>"+param.getUsuario());
					MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
					sqlParameter.addValue("pRamo", param.getRamo());
					sqlParameter.addValue("pFecIni", param.getFechaInicio());
					sqlParameter.addValue("pFecFin", param.getFechaFin());
					sqlParameter.addValue("pAgente", param.getAgente());
					sqlParameter.addValue("pPromotor", param.getPromotor());
					sqlParameter.addValue("pProveedor", param.getProveedor());
					sqlParameter.addValue("pContratante", param.getContratante());
					sqlParameter.addValue("pGerencia", param.getGerencia());
					sqlParameter.addValue("pNegocio", param.getNegocio());
					sqlParameter.addValue("pGrupo", param.getGrupo());
					sqlParameter.addValue("pTipoCom", param.getConcepto());
					sqlParameter.addValue("pUsuario", param.getUsuario());
					execute = agruparOrden.execute(sqlParameter);					
					lstOrden = (List<OrdenesPagoDTO>) execute.get("pCursor");
					BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
					String pDesc_Resp = (String) execute.get("pDesc_Resp");
					LOG.info("Desc_Resp"+pDesc_Resp);
					LOG.info("idCodResp"+idCodResp);
				}else{
					LOG.error("param Regresa Vacio");
				}
			} catch (Exception e) {
				LOG.error("Excepcion general en agruparOrden", e);
			}
		return lstOrden;*/
		return  null;
	}	
	
	@SuppressWarnings("unchecked")
	public List<CaReportesDTO> filtrarReportesPagadasDevengarCa(CaReportesDTO  param) {
				
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public CaReportesDTO getCaReportesDTO(CaReportesDTO  param) {	
		return filtrarReportesPagadasDevengarCa(null).get(0);
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CaReportesDTO> filtrarReportesDerechoPolizaCa(CaReportesDTO  param){
		
		return null;
	}
	

}