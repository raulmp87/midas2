package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.inciso.configuracion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionIncisoTipoVehiculoService;

@Stateless
public class ConfiguracionIncisoTipoVehiculoServiceImpl implements
		ConfiguracionIncisoTipoVehiculoService {
	
	private TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote;

	@Override
	public List<TipoVehiculoDTO> findAll() {		
		return tipoVehiculoFacadeRemote.findAll();
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Override
	public List<TipoVehiculoDTO> findAll(BigDecimal... ids) {
		String spName="MIDAS.pkgAUT_Servicios.spAUT_TipVehPorLineaNegocio";
		String [] atributosDTO = { "id.idTcTipoVehiculo", "descripcionTipoVehiculo" };
		String [] columnasCursor = { "IDTCTIPOVEHICULO", "DESCRIPCIONTIPOVEHICULO"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdLineaNegocio", ids[0]);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO", atributosDTO, columnasCursor);
			
			return storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName,e);
		}
	}

	@Override
	public TipoVehiculoDTO findById(BigDecimal arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TipoVehiculoDTO findById(CatalogoValorFijoId arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TipoVehiculoDTO findById(double arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TipoVehiculoDTO> listRelated(Object arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public TipoVehiculoFacadeRemote getTipoVehiculoFacadeRemote() {
		return tipoVehiculoFacadeRemote;
	}
	
	@EJB
	public void setTipoVehiculoFacadeRemote(
			TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote) {
		this.tipoVehiculoFacadeRemote = tipoVehiculoFacadeRemote;
	}
	
	
}
