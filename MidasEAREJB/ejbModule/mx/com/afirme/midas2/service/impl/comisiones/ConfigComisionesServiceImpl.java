package mx.com.afirme.midas2.service.impl.comisiones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.comisiones.ConfigComisionesDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.comisiones.ConfigComCentroOperacion;
import mx.com.afirme.midas2.domain.comisiones.ConfigComEjecutivo;
import mx.com.afirme.midas2.domain.comisiones.ConfigComGerencia;
import mx.com.afirme.midas2.domain.comisiones.ConfigComMotivoEstatus;
import mx.com.afirme.midas2.domain.comisiones.ConfigComPrioridad;
import mx.com.afirme.midas2.domain.comisiones.ConfigComPromotoria;
import mx.com.afirme.midas2.domain.comisiones.ConfigComSituacion;
import mx.com.afirme.midas2.domain.comisiones.ConfigComTipoAgente;
import mx.com.afirme.midas2.domain.comisiones.ConfigComTipoPromotoria;
import mx.com.afirme.midas2.domain.comisiones.ConfigComisiones;
import mx.com.afirme.midas2.domain.comisiones.ItemsConfigComision;
import mx.com.afirme.midas2.dto.comisiones.ConfigComisionesDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.service.comisiones.ConfigComisionesService;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class ConfigComisionesServiceImpl implements ConfigComisionesService{
	private ConfigComisionesDao dao;
	@Override
	public List<ConfigComisiones> findByFilters(ConfigComisiones config)throws Exception {
		return dao.findByFilters(config);
	}

	@Override
	public List<ConfigComisionesDTO> findByFiltersView(ConfigComisionesDTO config){
		return dao.findByFiltersView(config);
	}

	@Override
	public List<ValorCatalogoAgentes> getCatalogoModoEjecucion()throws Exception {
		return dao.getCatalogoModoEjecucion();
	}

	@Override
	public List<ValorCatalogoAgentes> getCatalogoPeriodo() throws Exception {
		return dao.getCatalogoPeriodo();
	}

	@Override
	public List<CentroOperacionView> getCentroOperacionList() throws Exception {
		return dao.getCentroOperacionList();
	}

	@Override
	public List<ConfigComCentroOperacion> getCentrosOperacionPorConfiguracion(ConfigComisiones config) throws Exception {
		return dao.getCentrosOperacionPorConfiguracion(config);
	}

	@Override
	public List<EjecutivoView> getEjecutivoList() throws Exception {
		return dao.getEjecutivoList();
	}

	@Override
	public List<ConfigComEjecutivo> getEjecutivosPorConfiguracion(ConfigComisiones config) throws Exception {
		return dao.getEjecutivosPorConfiguracion(config);
	}

	@Override
	public List<GerenciaView> getGerenciaList() throws Exception {
		return dao.getGerenciaList();
	}

	@Override
	public List<ConfigComGerencia> getGerenciasPorConfiguracion(ConfigComisiones config) throws Exception {
		return dao.getGerenciasPorConfiguracion(config);
	}

	@Override
	public List<ValorCatalogoAgentes> getPrioridadList() throws Exception {
		return dao.getPrioridadList();
	}

	@Override
	public List<ConfigComPrioridad> getPrioridadesPorConfiguracion(ConfigComisiones config) throws Exception {
		return dao.getPrioridadesPorConfiguracion(config);
	}

	@Override
	public List<PromotoriaView> getPromotoriaList() throws Exception {
		return dao.getPromotoriaList();
	}

	@Override
	public List<ConfigComPromotoria> getPromotoriasPorConfiguracion(ConfigComisiones config) throws Exception {
		return dao.getPromotoriasPorConfiguracion(config);
	}

	@Override
	public List<ValorCatalogoAgentes> getSituacionList() throws Exception {
		return dao.getSituacionList();
	}

	@Override
	public List<ConfigComSituacion> getSituacionesPorConfiguracion(ConfigComisiones config) throws Exception {
		return dao.getSituacionesPorConfiguracion(config);
	}

	@Override
	public List<ValorCatalogoAgentes> getTipoDeAgenteList() throws Exception {
		return dao.getTipoDeAgenteList();
	}

	@Override
	public List<ValorCatalogoAgentes> getTipoPromotoriaList() throws Exception {
		return dao.getTipoPromotoriaList();
	}

	@Override
	public List<ConfigComTipoAgente> getTiposAgentePorConfiguracion(ConfigComisiones config) throws Exception {
		return dao.getTiposAgentePorConfiguracion(config);
	}

	@Override
	public List<ConfigComTipoPromotoria> getTiposPromotoriaPorConfiguracion(ConfigComisiones config) throws Exception {
		return dao.getTiposPromotoriaPorConfiguracion(config);
	}

	@Override
	public ConfigComisiones loadById(ConfigComisiones config) throws Exception {
		return dao.loadById(config);
	}

	@Override
	public ConfigComisiones saveCentroOperacionConfig(ConfigComisiones config,List<ConfigComCentroOperacion> list) throws Exception {
		return dao.saveCentroOperacionConfig(config, list);
	}

	@Override
	public ConfigComisiones saveConfiguration(ConfigComisiones config)throws Exception {
		return dao.saveConfiguration(config);
	}

	@Override
	public ConfigComisiones saveEjecutivoConfig(ConfigComisiones config,List<ConfigComEjecutivo> list) throws Exception {
		return dao.saveEjecutivoConfig(config, list);
	}

	@Override
	public ConfigComisiones saveGerenciaConfig(ConfigComisiones config,List<ConfigComGerencia> list) throws Exception {
		return dao.saveGerenciaConfig(config, list);
	}

	@Override
	public ConfigComisiones savePrioridadConfig(ConfigComisiones config,List<ConfigComPrioridad> list) throws Exception {
		return dao.savePrioridadConfig(config, list);
	}

	@Override
	public ConfigComisiones savePromotoriaConfig(ConfigComisiones config,List<ConfigComPromotoria> list) throws Exception {
		return dao.savePromotoriaConfig(config, list);
	}

	@Override
	public ConfigComisiones saveSituacionConfig(ConfigComisiones config,List<ConfigComSituacion> list) throws Exception {
		return dao.saveSituacionConfig(config, list);
	}

	@Override
	public ConfigComisiones saveTipoAgenteConfig(ConfigComisiones config,List<ConfigComTipoAgente> list) throws Exception {
		return dao.saveTipoAgenteConfig(config, list);
	}

	@Override
	public ConfigComisiones saveTipoPromotoriaConfig(ConfigComisiones config,List<ConfigComTipoPromotoria> list) throws Exception {
		return dao.saveTipoPromotoriaConfig(config, list);
	}
	@Override
	public List<GerenciaView> getGerenciasConCentrosOperacionExcluyentes(List<Long> configGerencias,List<Long> configCentrosOperacion){
		return dao.getGerenciasConCentrosOperacionExcluyentes(configGerencias,configCentrosOperacion);
	}
	
	@Override
	public List<EjecutivoView> getEjecutivosConGerenciasExcluyentes(List<Long> configEjecutivos,List<Long> configGerencias,List<Long> centros){
		return dao.getEjecutivosConGerenciasExcluyentes(configEjecutivos,configGerencias,centros);
	}
	
	@Override
	public List<PromotoriaView> getPromotoriasConEjecutivosExcluyentes(List<Long> configPromotorias,List<Long> configEjecutivos,List<Long> configGerencias,List<Long> centros){
		return dao.getPromotoriasConEjecutivosExcluyentes(configPromotorias,configEjecutivos,configGerencias,centros);
	}
	@Override
	public List<AgenteView> obtenerAgentesPorConfiguracion(ConfigComisiones configuracion) throws Exception{
		return dao.obtenerAgentesPorConfiguracion(configuracion);
	}
	
	@Override
	public List<Long> getCentrosOperacionPorConfiguracionView(ConfigComisiones arg0) throws Exception {
		return dao.getCentrosOperacionPorConfiguracionView(arg0);
	}

	@Override
	public List<Long> getEjecutivosPorConfiguracionView(ConfigComisiones arg0) throws Exception {
		return dao.getEjecutivosPorConfiguracionView(arg0);
	}

	@Override
	public List<Long> getGerenciasPorConfiguracionView(ConfigComisiones arg0) throws Exception {
		return dao.getGerenciasPorConfiguracionView(arg0);
	}

	@Override
	public List<Long> getPrioridadesPorConfiguracionView(ConfigComisiones arg0) throws Exception {
		return dao.getPrioridadesPorConfiguracionView(arg0);
	}

	@Override
	public List<Long> getPromotoriasPorConfiguracionView(ConfigComisiones arg0) throws Exception {
		return dao.getPromotoriasPorConfiguracionView(arg0);
	}

	@Override
	public List<Long> getSituacionesPorConfiguracionView(ConfigComisiones arg0) throws Exception {
		return dao.getSituacionesPorConfiguracionView(arg0);
	}

	@Override
	public List<Long> getTiposAgentePorConfiguracionView(ConfigComisiones arg0) throws Exception {
		return dao.getTiposAgentePorConfiguracionView(arg0);
	}

	@Override
	public List<Long> getTiposPromotoriaPorConfiguracionView(ConfigComisiones arg0) throws Exception {
		return dao.getTiposPromotoriaPorConfiguracionView(arg0);
	}
	
	@Override
	public List<ConfigComMotivoEstatus> getMotivoEstatusPorConfiguracion(ConfigComisiones config) throws Exception {
		return dao.getMotivoEstatusPorConfiguracion(config);
	}
	
	@Override
	public List<Long> getMotivoEstatusPorConfiguracionView(ConfigComisiones config) throws Exception {
		return dao.getMotivoEstatusPorConfiguracionView(config);
	}

	@Override
	public ConfigComisiones saveMotivoEstatus(ConfigComisiones config,List<ConfigComMotivoEstatus> listMotivoEstatus) throws Exception {
		return dao.saveMotivoEstatus(config, listMotivoEstatus);
	}
	@Override
	public String obtenerAgentesPorConfiguracionQuery(ConfigComisiones configuracion) throws Exception{
		return dao.obtenerAgentesPorConfiguracionQuery(configuracion);
	}
	
	//**************************************************************************************************
	
	@Override
	public List<ItemsConfigComision> getCentrosOperacionPorConfigChecked(ConfigComisiones config) throws Exception {
		return dao.getCentrosOperacionPorConfigChecked(config);
	}

	@Override
	public List<ItemsConfigComision> getEjecutivosPorConfigChecked(ConfigComisiones config) throws Exception {
		return dao.getEjecutivosPorConfigChecked(config);
	}

	@Override
	public List<ItemsConfigComision> getGerenciasPorConfigChecked(ConfigComisiones config) throws Exception {
		return dao.getGerenciasPorConfigChecked(config);
	}

	@Override
	public List<ItemsConfigComision> getMotivoEstatusPorConfigChecked(ConfigComisiones config) throws Exception {
		return dao.getMotivoEstatusPorConfigChecked(config);
	}

	@Override
	public List<ItemsConfigComision> getPrioridadesPorConfigChecked(ConfigComisiones config) throws Exception {
		return dao.getPrioridadesPorConfigChecked(config);
	}

	@Override
	public List<ItemsConfigComision> getPromotoriasPorConfigChecked(ConfigComisiones config) throws Exception {
		return dao.getPromotoriasPorConfigChecked(config);
	}

	@Override
	public List<ItemsConfigComision> getSituacionesPorConfigChecked(ConfigComisiones config) throws Exception {
		return dao.getSituacionesPorConfigChecked(config);
	}

	@Override
	public List<ItemsConfigComision> getTiposAgentePorConfigChecked(ConfigComisiones config) throws Exception {
		return dao.getTiposAgentePorConfigChecked(config);
	}

	@Override
	public List<ItemsConfigComision> getTiposPromotoriaPorConfigChecked(ConfigComisiones config) throws Exception {
		return dao.getTiposPromotoriaPorConfigChecked(config);
	}
	
	@Override
	public String obtenerAgentesCuentaAfirme() throws Exception{
		return dao.obtenerAgentesCuentaAfirme();
	}

	
	/************************************************************************************
	 * ==================================================================================
	 * 	Common methods , sets and gets
	 * ==================================================================================
	 * 
	 */
	@EJB
	public void setConfigComisionesDao(ConfigComisionesDao configComisionesDao) {
		this.dao = configComisionesDao;
	}
}
