package mx.com.afirme.midas2.service.impl.tarifa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.tarifa.TarifaAutoModifPrimaDao;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifPrima;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifPrimaId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;
import mx.com.afirme.midas2.dto.ControlDinamicoDTO;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.catalogos.TarifaAutoModifPrimaService;
import mx.com.afirme.midas2.utils.Convertidor;
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TarifaAutoModifPrimaServiceImpl extends JpaTarifaService<TarifaAutoModifPrimaId,TarifaAutoModifPrima> implements TarifaAutoModifPrimaService{
	private TarifaAutoModifPrimaDao tarifaAutoModifPrimaDao;
	private Convertidor convertidor;
	
	
	@EJB
	public void setTarifaAutoModifPrimaDao(TarifaAutoModifPrimaDao tarifaAutoModifPrimaDao) {
		this.tarifaAutoModifPrimaDao = tarifaAutoModifPrimaDao;
	}
	
	@EJB
	public void setConvertidor(Convertidor convertidor) {
		this.convertidor = convertidor;
	}

	@Override
	public List<TarifaAutoModifPrima> findByFilters(TarifaAutoModifPrima filtro) {
		if(filtro==null){
			filtro=new TarifaAutoModifPrima();
		}
		return tarifaAutoModifPrimaDao.findByFilters(filtro);
	}

	/**
	 * Se obtiene la lista de tarifas por version id
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<TarifaAutoModifPrima> findByTarifaVersionId(TarifaVersionId tarifaVersionId) {
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		Expression<Long> idConceptoExp = cb.literal(tarifaVersionId.getIdConcepto());
//		Expression<Long> idLineaNegocioExp = cb.literal(tarifaVersionId.getIdLineaNegocio());
//		Expression<Long> idMonedaExp = cb.literal(tarifaVersionId.getIdMoneda());
//		Expression<Long> idRiesgoExp = cb.literal(tarifaVersionId.getIdRiesgo());
//		Expression<Long> versionExp = cb.literal(tarifaVersionId.getVersion());
//		Expression<Long> intNullExp = cb.literal(longNull);
//		
//		CriteriaQuery<TarifaAutoModifPrima> criteriaQuery = cb.createQuery(TarifaAutoModifPrima.class);
//		Root<TarifaAutoModifPrima> root = criteriaQuery.from(TarifaAutoModifPrima.class);
////		Predicate predicado = cb.and(cb.or(cb.equal(claveExp, intNullExp),cb.equal(root.get(ColorVehiculo_.clave), filtroColorVehiculo.getClave())),
////									 cb.like(cb.upper(root.get(ColorVehiculo_.descripcion)),"%" + filtroColorVehiculo.getDescripcion() + "%"));
//		Predicate predicado = cb.or(cb.equal(root.get(TarifaAutoModifPrimaId_.idConcepto), tarifaVersionId.getIdConcepto()),
//				cb.equal(root.get(TarifaAutoModifPrimaId_.idLineaNegocio),tarifaVersionId.getIdLineaNegocio()),
//				cb.equal(root.get(TarifaAutoModifPrimaId_.idMoneda),tarifaVersionId.getIdMoneda()),
//				cb.equal(root.get(TarifaAutoModifPrimaId_.idRiesgo),tarifaVersionId.getIdRiesgo()),
//				cb.equal(root.get(TarifaAutoModifPrimaId_.version),tarifaVersionId.getVersion())
//			);
//		criteriaQuery.where(predicado);
		
		StringBuilder stm=new StringBuilder("");
		stm.append("SELECT tarifa FROM TarifaAutoModifPrima tarifa WHERE tarifa.id.idMoneda=:idMoneda ");
		stm.append(" and tarifa.id.idRiesgo=:idRiesgo ");
		stm.append(" and tarifa.id.idConcepto=:idConcepto ");
		stm.append(" and tarifa.id.version=:version ");
		Query query =entityManager.createQuery(stm.toString());
		query.setParameter("idMoneda",tarifaVersionId.getIdMoneda()).setParameter("idRiesgo",tarifaVersionId.getIdRiesgo()).setParameter("idConcepto",tarifaVersionId.getIdConcepto()).setParameter("version",tarifaVersionId.getVersion());
		return query.getResultList();
	}
	@Override
	public List<RegistroDinamicoDTO> getRegistrosDinamicosPorTarifaVersionId(
			TarifaVersionId tarifaVersionId, Class<?> vista) {
		List<TarifaAutoModifPrima> lista=findByTarifaVersionId(tarifaVersionId);
		List<RegistroDinamicoDTO> registros=convertidor.getListaRegistrosDinamicos(lista, vista);
		return registros;
	}

	@Override
	public RegistroDinamicoDTO verDetalle(TarifaVersionId tarifaVersionId, String id,String accion, Class<?> vista) {
		RegistroDinamicoDTO registro=null;
		TarifaAutoModifPrima tarifa=null;
		if(TipoAccionDTO.getAgregarModificar().equals(accion)){
			tarifa=new TarifaAutoModifPrima();
			TarifaAutoModifPrimaId idTarifa=new TarifaAutoModifPrimaId(tarifaVersionId);
			tarifa.setId(idTarifa);
			registro=convertidor.getRegistroDinamico(tarifa, vista);
			if(registro!=null){
				List<ControlDinamicoDTO> controles=registro.getControles();
			}
		}else if(TipoAccionDTO.getEliminar().equals(accion)){
			TarifaAutoModifPrimaId tarifaId=new TarifaAutoModifPrimaId(id);
			tarifa=findById(TarifaAutoModifPrima.class,tarifaId);
			registro=convertidor.getRegistroDinamico(tarifa, vista);
			if(registro!=null){
				List<ControlDinamicoDTO> controles=registro.getControles();
				//Cuando es baja, ningun campo es editable
				convertidor.setAttribute("editable", false, controles);
			}
		}else if(TipoAccionDTO.getEditar().equals(accion)){
			TarifaAutoModifPrimaId tarifaId=new TarifaAutoModifPrimaId(id);
			tarifa=findById(TarifaAutoModifPrima.class,tarifaId);
			registro=convertidor.getRegistroDinamico(tarifa, vista);
			if(registro!=null){
				List<ControlDinamicoDTO> controles=registro.getControles();
				//Cuando es cambio solo los ids no son editables
				convertidor.setAttribute("editable", false, controles,"varModifPrima.id","varModifPrima.grupo.id");
			}
		}
		return registro;
	}
	
	public TarifaAutoModifPrima getNewObject(){
		return new TarifaAutoModifPrima();
	}
}
