package mx.com.afirme.midas2.service.impl.siniestros.incentivos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.incentivos.ConfiguracionIncentivosDao;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina.EnumModoEnvio;
import mx.com.afirme.midas2.domain.siniestros.incentivos.ConfiguracionIncentivos;
import mx.com.afirme.midas2.domain.siniestros.incentivos.IncentivoAjustador;
import mx.com.afirme.midas2.domain.siniestros.incentivos.ConfiguracionIncentivos.EstatusConfiguracionIncentivo;
import mx.com.afirme.midas2.domain.siniestros.incentivos.ConfiguracionIncentivosNotificacion;
import mx.com.afirme.midas2.domain.siniestros.incentivos.IncentivoAjustador.ConceptoIncentivo;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.incentivos.IncentivoAjustadorDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.incentivos.ConfiguracionIncentivosService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EmailDestinaratios;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperReport;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;


@Stateless
public class ConfiguracionIncentivosServiceImpl implements ConfiguracionIncentivosService{

	
	public  static final Logger log = Logger.getLogger(ConfiguracionIncentivosServiceImpl.class);
	
	private static final String ARCHIVO_ADJUNTO = "Incentivos.pdf";
	
	@EJB
	private ConfiguracionIncentivosDao configuracionIncentivosDao;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	@Resource
	private javax.ejb.SessionContext sessionContext;

	@EJB
	private EnvioNotificacionesService notificacionService;

	final String[] rolesPrimerNivelAutorizacion = new String[]{"Rol_M2_Director_Indemnizacion_Operacion_Siniestros"};
	final String[] rolesSegundoNivelAutorizacion = new String[]{"Rol_M2_SubDirector_Siniestros_Autos",
			"Rol_M2_Coordinador_Cabina_Ajustes_Siniestros"};
	
	private static final String INCENTIVO_AJUSTADORES = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/incentivosAjustadores.jrxml";
	private static final String INCENTIVO_AJUSTADORES_DETALLE_GENERICO =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/incentivoAjustadorDetalleGenerico.jrxml";
	
	private static final String INCENTIVO_AJUSTADORES_DETALLE_EFECTIVO = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/incentivoAjustadorDetalleEfectivo.jrxml";
	private static final String INCENTIVO_AJUSTADORES_DETALLE_TOTALES = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/incentivoAjustadorDetalleTotales.jrxml";
	
	public List<ConfiguracionIncentivos> obtenerConfiguracionesParaBandejaAutorizacion(){
		List<ConfiguracionIncentivos> configuraciones =  this.configuracionIncentivosDao.obtenerConfiguracionesParaBandejaAutorizacion();
		this.validaAutorizaciones(configuraciones);
		return configuraciones;
	}
	
	private void validaAutorizaciones(List<ConfiguracionIncentivos> configuraciones){
		//Si se encuentran en pendiente u aprobado
		for(ConfiguracionIncentivos configuracion : configuraciones){
			if(configuracion.getEstatus() == ConfiguracionIncentivos.EstatusConfiguracionIncentivo.XAUTORIZAR ||
					configuracion.getEstatus() == ConfiguracionIncentivos.EstatusConfiguracionIncentivo.PREAPROBAD 	){
				//Si es un rol válido
				if(this.usuarioService.tieneRolUsuarioActual((String[])ArrayUtils.addAll(rolesPrimerNivelAutorizacion, rolesSegundoNivelAutorizacion))){
					//Si se encuentra en pendiente, cualquier rol valido puede autorizar
					if(configuracion.getEstatus() == ConfiguracionIncentivos.EstatusConfiguracionIncentivo.XAUTORIZAR){
						configuracion.setEsAutorizable(Boolean.TRUE);
					}else{
						//Si se encuentra pre autorizado, solamente los roles de primer nivel pueden autorizar
						if(this.usuarioService.tieneRolUsuarioActual(rolesPrimerNivelAutorizacion)){
							configuracion.setEsAutorizable(Boolean.TRUE);
						}else{
							configuracion.setEsAutorizable(Boolean.FALSE);
						}
					}
				}
			}else{
				configuracion.setEsAutorizable(Boolean.FALSE);
			}
		}
		
	}
	
	@Override
	public void autorizaConfiguracion(Long configuracionId){
		ConfiguracionIncentivos configuracion = this.entidadService.findById(ConfiguracionIncentivos.class, configuracionId);
		String nombreUsuario = this.usuarioService.getUsuarioActual().getNombreUsuario();
		if(configuracion.getEstatus() == ConfiguracionIncentivos.EstatusConfiguracionIncentivo.XAUTORIZAR){
			if(this.usuarioService.tieneRolUsuarioActual(rolesPrimerNivelAutorizacion)){
				configuracion.setEstatus(ConfiguracionIncentivos.EstatusConfiguracionIncentivo.AUTORIZADO);
				configuracion.setCodigoUsuarioPrimerNivel(nombreUsuario);
				configuracion.setFechaAutorizacionPrimerNivel(new Date());
				configuracion.setFechaModificacion(new Date());
				configuracion.setCodigoUsuarioModificacion(nombreUsuario);
			}else{
				configuracion.setEstatus(ConfiguracionIncentivos.EstatusConfiguracionIncentivo.PREAPROBAD);
				configuracion.setCodigoUsuarioSegundoNivel(nombreUsuario);
				configuracion.setFechaAutorizacionSegundoNivel(new Date());
				configuracion.setFechaModificacion(new Date());
				configuracion.setCodigoUsuarioModificacion(nombreUsuario);
			}
			this.entidadService.save(configuracion);
		}
	}
	
	@Override
	public void rechazaConfiguracion(Long configuracionId){
		ConfiguracionIncentivos configuracion = this.entidadService.findById(ConfiguracionIncentivos.class, configuracionId);
		String nombreUsuario = this.usuarioService.getUsuarioActual().getNombreUsuario();
		configuracion.setEstatus(ConfiguracionIncentivos.EstatusConfiguracionIncentivo.RECHAZADO);
		configuracion.setCodigoUsuarioRechazo(nombreUsuario);
		configuracion.setFechaDeRechazo(new Date());
		configuracion.setFechaModificacion(new Date());
		configuracion.setCodigoUsuarioModificacion(nombreUsuario);
		this.entidadService.save(configuracion);
	}
	
	

	public List<IncentivoAjustadorDTO> obtenerIncentivoPreliminar(Long configuracionId) {
		
		String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		return configuracionIncentivosDao.obtenerIncentivoPreliminar(configuracionId, codigoUsuario);
	}
	
	public List<IncentivoAjustadorDTO> obtenerIncentivos(Long configuracionId, ConceptoIncentivo concepto) {
		List<IncentivoAjustadorDTO> incentivos = new ArrayList<IncentivoAjustadorDTO>();
		
		switch (concepto) {
		case INHABIL:
			incentivos = configuracionIncentivosDao.obtenerIncentivoDiaInhabil(configuracionId);
			break;
			
		case EFECTIVO:
			incentivos = configuracionIncentivosDao.obtenerIncentivoEfectivo(configuracionId);
			break;
			
		case COMPANIA:
			incentivos = configuracionIncentivosDao.obtenerIncentivoCompanias(configuracionId);
			break;
			
		case SIPAC:
			incentivos = configuracionIncentivosDao.obtenerIncentivoSIPAC(configuracionId);
			break;
			
		case RECHAZO:
			incentivos = configuracionIncentivosDao.obtenerIncentivoRechazos(configuracionId);
			break;

		default:
			throw new IllegalArgumentException("Concepto invalido.");			
		}
		
		return incentivos;
	}
	
	
	public List<IncentivoAjustadorDTO> obtenerIncentivosAjustador(Long configuracionId) {
		return configuracionIncentivosDao.obtenerIncentivosAjustador(configuracionId);
	}
	
	@Override
	public TransporteImpresionDTO imprimirPercepcionAjustadores(Long configIncentivosId){
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		ConfiguracionIncentivos configuracion = entidadService.findById(ConfiguracionIncentivos.class, configIncentivosId);
		
		JasperReport jReport = gImpresion.getOJasperReport(INCENTIVO_AJUSTADORES);;
		JasperReport jReportIncentivoDetalleGenerico = gImpresion.getOJasperReport(INCENTIVO_AJUSTADORES_DETALLE_GENERICO);
		JasperReport jReportIncentivoDetalleEfectivo = gImpresion.getOJasperReport(INCENTIVO_AJUSTADORES_DETALLE_EFECTIVO);
		JasperReport jReportIncentivoDetalleTotales = gImpresion.getOJasperReport(INCENTIVO_AJUSTADORES_DETALLE_TOTALES);
		
		List<IncentivoAjustadorDTO> detallesTotales = obtenerIncentivosAjustador(configIncentivosId);
		BigDecimal granTotal = new BigDecimal(0.00);
		for(IncentivoAjustadorDTO detalle : detallesTotales){
			if(detalle.getMontoTotal() != null){
				granTotal = granTotal.add(detalle.getMontoTotal());
			}
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("jReportIncentivoDetalleGenerico", jReportIncentivoDetalleGenerico);
		params.put("jReportIncentivoDetalleEfectivo", jReportIncentivoDetalleEfectivo);
		params.put("jReportIncentivoDetalleTotales", jReportIncentivoDetalleTotales);
		params.put("detallesInhabiles", obtenerIncentivos(configIncentivosId, ConceptoIncentivo.INHABIL));
		params.put("detallesEfectivo", obtenerIncentivos(configIncentivosId, ConceptoIncentivo.EFECTIVO));
		params.put("detallesCompania", obtenerIncentivos(configIncentivosId, ConceptoIncentivo.COMPANIA));
		params.put("detallesSIPAC", obtenerIncentivos(configIncentivosId, ConceptoIncentivo.SIPAC));
		params.put("detallesRechazados", obtenerIncentivos(configIncentivosId, ConceptoIncentivo.RECHAZO));
		params.put("detallesTotales", detallesTotales);
		params.put("granTotal", granTotal);
		params.put("folio", configuracion.getFolio());
		params.put("oficina", configuracion.getOficina().getNombreOficina());
		params.put("periodoIni", configuracion.getPeriodoInicial());
		params.put("periodoFin", configuracion.getPeriodoFinal());
		params.put("dirAdminIndem", configuracion.getCodigoUsuarioPrimerNivel());
		
		if(configuracion.getCodigoUsuarioSegundoNivel() != null){
			Usuario usuarioSegundoNivel = usuarioService.buscarUsuarioPorNombreUsuario(configuracion.getCodigoUsuarioSegundoNivel());
			boolean tieneRolSubdirector = usuarioService.tieneRol(rolesSegundoNivelAutorizacion[0], usuarioSegundoNivel);
			if(tieneRolSubdirector){
				params.put("subdirSin", configuracion.getCodigoUsuarioSegundoNivel());
			}else{
				boolean tieneRolCoordinador = usuarioService.tieneRol(rolesSegundoNivelAutorizacion[1], usuarioSegundoNivel);
				if(tieneRolCoordinador){
					params.put("coordCabina", configuracion.getCodigoUsuarioSegundoNivel());
				}
			}
		}
		
		return gImpresion.getTImpresionDTO(jReport, new JREmptyDataSource(), params);
	}

	
	@Override
	public List<ConfiguracionIncentivos> obtenerConfiguracionesParaListadoPercepciones(){
		Map<String, Object> params = new HashMap<String,Object>();
		List<ConfiguracionIncentivos> configuraciones = 
			entidadService.findByPropertiesWithOrder(ConfiguracionIncentivos.class, params, "folio DESC");
		return configuraciones;
	}
	
	@Override
	public ConfiguracionIncentivos guardarConfiguracionIncentivos(ConfiguracionIncentivos configuracion){
		ConfiguracionIncentivosService processor = this.sessionContext.getBusinessObject(ConfiguracionIncentivosService.class);
		configuracion = processor.guardarConfiguracionIncentivosTransaction(configuracion);
		return configuracion;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ConfiguracionIncentivos guardarConfiguracionIncentivosTransaction(ConfiguracionIncentivos configuracion){
		if(configuracion.getId() == null){
			configuracion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
			configuracion.setFechaCreacion(new Date());
			configuracion.setEstatus(EstatusConfiguracionIncentivo.TRAMITE);
		}
		configuracion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		configuracion.setFechaModificacion(new Date());
		if(configuracion.getPorcentajeExcedente() != null){
			configuracion.setPorcentajeExcedente(configuracion.getPorcentajeExcedente().divide(new BigDecimal(100)));
		}
		if(configuracion.getPorcentajeRangoEfectivo() != null){
			configuracion.setPorcentajeRangoEfectivo(configuracion.getPorcentajeRangoEfectivo().divide(new BigDecimal(100)));
		}
		if(configuracion.getPorcentajeRechazo() != null){
			configuracion.setPorcentajeRechazo(configuracion.getPorcentajeRechazo().divide(new BigDecimal(100)));
		}
		
		configuracion = entidadService.save(configuracion);
		return configuracion;
	}

	
	@Override
	public ConfiguracionIncentivos obtenerConfiguracionIncentivos (Long configuracionId) {
		return entidadService.findById(ConfiguracionIncentivos.class, configuracionId);
	}	
	
	@Override
	public void preRegistroIncentivos(Long configuracionId, String incentivosConcat) {
		
		ConfiguracionIncentivosService processor = this.sessionContext.getBusinessObject(ConfiguracionIncentivosService.class);
		processor.actualizarIncentivos(configuracionId, incentivosConcat);
	}
	
	@Override
	public void actualizarIncentivos(Long configuracionId, String incentivosConcat) {
		
		configuracionIncentivosDao.preRegistroIncentivos(configuracionId, incentivosConcat);
	}
	
	@Override
	public ConfiguracionIncentivos registrarPercepcion(Long configuracionId) {
		ConfiguracionIncentivos configuracion = entidadService.findById(ConfiguracionIncentivos.class, configuracionId);
		
		configuracion.setEstatus(EstatusConfiguracionIncentivo.REGISTRADO);
		configuracion.setCodigoUsuarioModificacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
		configuracion.setFechaModificacion(new Date());
		configuracion.setFolio(entidadService.getIncrementedSequence(ConfiguracionIncentivos.SECUENCIA_FOLIO));
		entidadService.save(configuracion);
		
		return configuracion;
	}

	
	@Override
	public ConfiguracionIncentivos cancelarConfiguracion(Long configuracionId){
			ConfiguracionIncentivos configuracion = entidadService.findById(ConfiguracionIncentivos.class, configuracionId);
			configuracion.setCodigoUsuarioCancelacion(usuarioService.getUsuarioActual().getNombreUsuario());
			configuracion.setFechaDeCancelacion(new Date());
			configuracion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			configuracion.setFechaModificacion(new Date());
			configuracion.setEstatus(EstatusConfiguracionIncentivo.CANCELADO);
			configuracion = entidadService.save(configuracion);
			return configuracion;
	}
	
	
	@Override
	public List<ConfiguracionIncentivosNotificacion> obtenerHistoricoNotificaciones(Long idConfiguracion){
		List<ConfiguracionIncentivosNotificacion> notificaciones = this.entidadService.findByProperty(ConfiguracionIncentivosNotificacion.class, "configuracionIncentivo.id", idConfiguracion);
		return notificaciones;
	}
	
	@Override
	public void enviaNotificacion(Long idConfiguracion,String destinatariosStr){
		
		String[] listaDirecciones = destinatariosStr.split(";");
		EmailDestinaratios destinatariosAdicionales = new EmailDestinaratios();
		
		for(String direccionCorreo:listaDirecciones)
		{			
			destinatariosAdicionales.agregar(EnumModoEnvio.PARA, direccionCorreo, ""); 
		}	
		ConfiguracionIncentivos configuracion =  this.entidadService.findById(ConfiguracionIncentivos.class, idConfiguracion);
		
		HashMap<String, Serializable> valores = new HashMap<String, Serializable>();
		valores.put("fechaAutorizacion", this.generaFechaAmigable(configuracion.getFechaAutorizacion()));
		this.notificacionService.enviarNotificacion(
				EnvioNotificacionesService.EnumCodigo.INCENTIVO_NOTIF_AUTORIZADA.toString(), 
				valores,
				idConfiguracion.toString() , 
				destinatariosAdicionales,
				this.generarPDFAutorizacionIncentivo(idConfiguracion) );
		ConfiguracionIncentivosNotificacion notificacion = new ConfiguracionIncentivosNotificacion();
		String nombreUsuario = this.usuarioService.getUsuarioActual().getNombreUsuario();
		notificacion.setConfiguracionIncentivo(configuracion);
		notificacion.setCodigoUsuarioCreacion(nombreUsuario);
		notificacion.setUsuarioRemitente(nombreUsuario);
		notificacion.setDestinatrarios(destinatariosStr);
		notificacion.setFechaCreacion(new Date());
		configuracion.setEstatus(EstatusConfiguracionIncentivo.ENVIADO);
		this.entidadService.save(notificacion);
		
	}
	
	@Override
	public void eliminarConfiguracion(Long configuracionId) {
		ConfiguracionIncentivos configuracion = entidadService.findById(ConfiguracionIncentivos.class, configuracionId);

		if (configuracion!= null && EstatusConfiguracionIncentivo.TRAMITE == configuracion.getEstatus()) {
			List<IncentivoAjustador> incentivos = entidadService.findByProperty(IncentivoAjustador.class, "configuracionIncentivos.id", configuracionId);
			if (CollectionUtils.isNotEmpty(incentivos)) {
				entidadService.removeAll(incentivos);
			}
			
			entidadService.remove(configuracion);
		}
		
	}
	
	@Override
	public void enviaAutorizacion(Long configuracionId){
		ConfiguracionIncentivos configuracion = this.entidadService.findById(ConfiguracionIncentivos.class, configuracionId);
		configuracion.setEstatus(ConfiguracionIncentivos.EstatusConfiguracionIncentivo.XAUTORIZAR);
		this.entidadService.save(configuracion);
	}
	
	private List<ByteArrayAttachment> generarPDFAutorizacionIncentivo(Long idConfiguracion){
		List<ByteArrayAttachment> pdfAdjunto = null;
		
		try{
	        TransporteImpresionDTO pdfIncentivo =  this.imprimirPercepcionAjustadores(idConfiguracion);
	        ByteArrayAttachment adjunto = new ByteArrayAttachment(ARCHIVO_ADJUNTO, TipoArchivo.PDF, pdfIncentivo.getByteArray());
	        pdfAdjunto = new ArrayList<ByteArrayAttachment>();
	        pdfAdjunto.add(adjunto);
		}catch(Exception e){
			log.error("Error al generar el PDF de autorizacion de incentivos", e);
		}
		
		return pdfAdjunto;
	}
	
	
	
	private String generaFechaAmigable(Date fecha){
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy",new Locale("es","MX"));
		
		return sdf.format(fecha);
	}
}
