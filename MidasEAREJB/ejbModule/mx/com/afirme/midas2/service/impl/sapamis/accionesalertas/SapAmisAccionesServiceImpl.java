package mx.com.afirme.midas2.service.impl.sapamis.accionesalertas;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAcciones;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.accionesalertas.SapAmisAccionesService;

@Stateless
public class SapAmisAccionesServiceImpl implements SapAmisAccionesService{
	private EntidadService entidadService;

	@Override
	public List<SapAmisAcciones> findAll() {
		List<SapAmisAcciones> retorno = entidadService.findAll(SapAmisAcciones.class);
		return retorno;
	}

	@Override
	public SapAmisAcciones findById(long id) {
		SapAmisAcciones retorno = new SapAmisAcciones();
		if(id >= 0){
			retorno = entidadService.findById(SapAmisAcciones.class, id);
		}
		return retorno;
	}

	@Override
	public List<SapAmisAcciones> findByProperty(String propertyName, Object property) {
		List<SapAmisAcciones> emailNegocioAlertasList = new ArrayList<SapAmisAcciones>();
		if(propertyName != null && !propertyName.equals("") && property != null){
			emailNegocioAlertasList = entidadService.findByProperty(SapAmisAcciones.class, propertyName, property);
		}
		return emailNegocioAlertasList;
	}

	@Override
	public List<SapAmisAcciones> findByStatus(boolean status) {
		List<SapAmisAcciones> emailNegocioAlertas = entidadService.findByProperty(SapAmisAcciones.class, "estatus", status?0:1);
		return emailNegocioAlertas;
	}

	@Override
	public SapAmisAcciones saveObject(SapAmisAcciones sapAmisAcciones) {
		if(sapAmisAcciones != null && sapAmisAcciones.getIdSapAmisAcciones() >= 0){
			sapAmisAcciones.setIdSapAmisAcciones((Long)entidadService.saveAndGetId(sapAmisAcciones));
		}
		return sapAmisAcciones;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}