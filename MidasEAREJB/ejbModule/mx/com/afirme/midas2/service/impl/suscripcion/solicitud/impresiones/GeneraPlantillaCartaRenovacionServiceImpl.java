package mx.com.afirme.midas2.service.impl.suscripcion.solicitud.impresiones;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosCartaRenovacion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.poliza.PolizaService;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.impresiones.GeneraPlantillaCartaRenovacionService;
import mx.com.afirme.midas2.utils.ReadAndPrintXMLFile;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.fill.JRGzipVirtualizer;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;

@Stateless
public class GeneraPlantillaCartaRenovacionServiceImpl implements  GeneraPlantillaCartaRenovacionService{

	private EntidadService entidadService;
	private AgenteMidasService agenteMidasService;
	private IncisoService incisoService;
	private PolizaService polizaService;
	private RenovacionMasivaService renovacionMasivaService;
	
	public static final Long AGENTE_ESTRATEGAS_AUTOMATICA = new Long(91926);
	public static final Long AGENTE_ESTRATEGAS_AUTOMATICA_2 = new Long(92322);
	public static final Long AGENTE_ESTRATEGAS_ICV = new Long(92069);
	public static final Long AGENTE_CAMBIO_LINEA = new Long(91926);
	public static final Long AGENTE_AUTOFIN = new Long(92321);
	public static final Long AGENTE_VENTA_DIRECTA_1 = new Long(90784);
	public static final Long AGENTE_VENTA_DIRECTA_2 = new Long(90036);
	public static final Long AGENTE_VENTA_DIRECTA_3 = new Long(90835);
	public static final Long AGENTE_VENTA_DIRECTA_4 = new Long(92047);
	
	@PersistenceContext 
	private EntityManager entityManager;	
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	@EJB
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	
	@EJB
	public void setPolizaService(PolizaService polizaService) {
		this.polizaService = polizaService;
	}
	
	@EJB
	public void setRenovacionMasivaService(RenovacionMasivaService renovacionMasivaService) {
		this.renovacionMasivaService = renovacionMasivaService;
	}

	@Override
	public TransporteImpresionDTO llenarCartaRenovacion(PolizaDTO poliza, Locale locale){
		
		TransporteImpresionDTO transporte = new TransporteImpresionDTO();
		
		List<byte[]> inputByteArray = new ArrayList<byte[]>(1);		
		ByteArrayOutputStream output = new ByteArrayOutputStream();	
		DatosCartaRenovacion datosCarta = new DatosCartaRenovacion();
		
		SimpleDateFormat df2 = new SimpleDateFormat("MMMM' de 'yyyy", new java.util.Locale("ES", "MX"));
		try{
			
			String fechaTipo1 = df2.format(poliza.getCotizacionDTO().getFechaInicioVigencia());
			char[] stringArray = fechaTipo1.toCharArray();
			stringArray[0] = Character.toUpperCase(stringArray[0]);
			fechaTipo1 = new String(stringArray);
			
			datosCarta.setNombreContratante(poliza.getCotizacionDTO().getNombreContratante());
			
			datosCarta.setFechaMMMMYYYY(fechaTipo1);			
			datosCarta.setSucursal("");
			datosCarta.setBancos("Banca Afirme, Banorte o Bancomer");
			//Buscar Agente
			Agente agente = new Agente();
			try{
				agente.setId(poliza.getCotizacionDTO().getSolicitudDTO().getCodigoAgente().longValue());
				agente = agenteMidasService.loadByIdImpresiones(agente);
				datosCarta.setSucursal(agente.getPersona().getNombreCompleto());
			}catch(Exception e){
				
			}
			
			this.llenaDatosXML(datosCarta, poliza.getIdToPoliza());
			
			//Agrega cartas
			inputByteArray = anexaCartas(poliza, datosCarta, locale);
			if(inputByteArray != null && inputByteArray.size() > 0){
				pdfConcantenate(inputByteArray, output);
				transporte.setByteArray(output.toByteArray());
			}
	   }catch(Exception ex){
		 throw new RuntimeException(ex);
	   }
	   
	   return transporte;	
	}
	
	private void llenaDatosXML(DatosCartaRenovacion datosCarta, BigDecimal idToPoliza){
		try{
			//String reciboXML = polizaService.obtenerXMLPrimerReciboPoliza(idToPoliza);
			MensajeDTO mensaje = renovacionMasivaService.getXmlCartaRenovacion(idToPoliza);
			String reciboXML = "";
			if(mensaje.getMensaje() != null){
				reciboXML = mensaje.getMensaje();
			}
			reciboXML = reciboXML.replaceAll("\\r|\\n", "");
			ReadAndPrintXMLFile xmlParser = new ReadAndPrintXMLFile(reciboXML);
			
			String banco1 = xmlParser.getNode("wf:wfactura", "Extra20");
			String banco2 = xmlParser.getNode("wf:wfactura", "Extra23");
			String banco3 = xmlParser.getNode("wf:wfactura", "Extra31");
			String banco4 = xmlParser.getNode("wf:wfactura", "Descuento8");
			String fechaVenRecibo = xmlParser.getNode("wf:wfactura", "Extra12");
			
			String bancos = "";
			bancos += (banco1 != null && !banco1.isEmpty())?banco1.trim()+", ":"";
			bancos += (banco2 != null && !banco2.isEmpty())?banco2.trim()+", ":"";
			bancos += (banco3 != null && !banco3.isEmpty())?banco3.trim()+", ":"";
			bancos += (banco4 != null && !banco4.isEmpty())?banco4.trim()+", ":"";
			
			String[] lst = bancos.split(",");
			bancos = obtenerBancos(lst);
			datosCarta.setBancos(bancos);
			
			
			if(fechaVenRecibo != null && !fechaVenRecibo.isEmpty()){
			try{
				SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat df = new SimpleDateFormat("dd' de 'MMMM' de 'yyyy", new java.util.Locale("ES", "MX"));
				String fechaTipo2 = df.format(formatoDelTexto.parse(fechaVenRecibo));
				int index = fechaTipo2.indexOf("de");
				char[] stringArray = fechaTipo2.toCharArray();
				stringArray[index+3] = Character.toUpperCase(stringArray[index+3]);
				fechaTipo2 = new String(stringArray);
				datosCarta.setFechaDDMMMMYYYY(fechaTipo2);
			}catch(Exception e){
				e.printStackTrace();
			}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public String obtenerBancos(String[] lst) {
		StringBuilder bancos = new StringBuilder("");
		for(int i = 0; i < lst.length; i++){
			String banco = lst[i].trim();
			if(!banco.isEmpty()){
				if(i > 0 && i != lst.length - 2){
					bancos.append(", ");
				}else if(i == lst.length - 2){
					bancos.append(" o ");
				}				
				banco = banco.toLowerCase();
				String[] parts = banco.split(" ");
				if(parts.length > 1){
					banco = obtenerBanco(parts);
				}else{
					banco = StringUtils.capitalize(banco.trim());
				}
				bancos.append(banco);
			}
		}		
		return bancos.toString();
	}

	public String obtenerBanco(String[] parts) {
		StringBuilder banco = new StringBuilder("");
		for(int j = 0; j < parts.length; j++){
			String a = parts[j];
			banco.append(StringUtils.capitalize(a.trim())).append(" ");
		}
		return banco.toString();
	}

	@SuppressWarnings("unchecked")
	private Boolean esAgenteTipo(Agente agente, int tipo){		
		try{
			String queryString = "Select model " +
					" FROM CatalogoValorFijoDTO model  " +
					" WHERE model.id.idGrupoValores = :idGrupoValores " +
					" ";
			
			Query query = entityManager.createQuery(queryString.toString(), CatalogoValorFijoDTO.class);			

			query.setParameter("idGrupoValores", tipo);
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			List<CatalogoValorFijoDTO> valorList = (List<CatalogoValorFijoDTO>) query.getResultList();	    	
	    	
			for(CatalogoValorFijoDTO valor: valorList){
				try{
					Long idAgente = Long.parseLong(valor.getDescripcion().trim());
					if(agente.getIdAgente().equals(idAgente)){
						return true;
					}
				}catch(Exception e){
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	
	
	private List<byte[]> anexaCartas(PolizaDTO poliza, DatosCartaRenovacion datosCarta, Locale locale){
		List<byte[]> inputByteArray = new ArrayList<byte[]>(1);
		
		Agente agente = new Agente();
		try{
			agente.setId(poliza.getCotizacionDTO().getSolicitudDTO().getCodigoAgente().longValue());
			agente = agenteMidasService.loadByIdImpresiones(agente);
		}catch(Exception e){
			agente.setIdAgente(-1l);
		}

		List<IncisoCotizacionDTO>  incisos = null;
		try{
			incisos = incisoService.getIncisosPorLinea(poliza.getCotizacionDTO().getIdToCotizacion(), 
					SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_AUTOS);
			if(incisos == null || incisos.isEmpty()){
				incisos = incisoService.getIncisosPorLinea(poliza.getCotizacionDTO().getIdToCotizacion(), 
						SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_CAMIONES);
			}
		}catch(Exception e){
		}
		
		//Sucursal Venta Libre
		if((incisos == null || incisos.isEmpty()) &&
				(agente != null && agente.getPersona() != null && 
						agente.getPersona().getNombreCompleto().toUpperCase().startsWith("SUCURSAL"))){
			inputByteArray.add(imprimirCarta(datosCarta, locale,(short) 2));
		}
		
		//Estrategas
		if(this.esAgenteTipo(agente, CatalogoValorFijoDTO.IDGRUPO_AGENTE_ESTRATEGAS)){
			inputByteArray.add(imprimirCarta(datosCarta, locale,(short) 6));
		}
		
		//Autoplazo
		if(incisos != null && !incisos.isEmpty()){
			IncisoCotizacionDTO inciso = incisos.get(0);			
			datosCarta.setNombreAsegurado(inciso.getIncisoAutoCot().getNombreAseguradoUpper());
			inputByteArray.add(imprimirCarta(datosCarta, locale,(short) 1));
				
			//Certificado de Garantia Autoplazo
			if(inciso.getSeccionCotizacion() != null 
					&& !inciso.getSeccionCotizacion().getSeccionDTO().getIdToSeccion().equals(SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_CAMIONES)){
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.set(2014, 5, 1);
				if(poliza.getFechaCreacion().compareTo(gcFechaInicio.getTime()) >= 0 ){
					inputByteArray.add(imprimirCarta(datosCarta, locale,(short) 7));
				}	
			}			
		}else{
			try{
				List<SeccionDTO> seccionList = entidadService.findByProperty(SeccionDTO.class, "nombreComercial", "PICK UPS INDIVIDUAL");				
				if(polizaService.esPolizaAutoplazo(poliza.getIdToPoliza(), seccionList)){
					List<IncisoCotizacionDTO> incisoList = incisoService.getIncisos(poliza.getCotizacionDTO().getIdToCotizacion());
					IncisoCotizacionDTO inciso = incisoList.get(0);
					datosCarta.setNombreAsegurado(inciso.getIncisoAutoCot().getNombreAseguradoUpper());
					inputByteArray.add(imprimirCarta(datosCarta, locale,(short) 1));
					
					//Certificado de Garantia Autoplazo
					GregorianCalendar gcFechaInicio = new GregorianCalendar();
					gcFechaInicio.set(2014, 5, 1);
					if(poliza.getFechaCreacion().compareTo(gcFechaInicio.getTime()) >= 0 ){
						inputByteArray.add(imprimirCarta(datosCarta, locale,(short) 7));
					}
					
				}
			}catch(Exception e){
			}	
		}
		
		//Cambio de Linea
		if(this.esAgenteTipo(agente, CatalogoValorFijoDTO.IDGRUPO_AGENTE_CAMBIO_LINEA)){
			PolizaDTO polizaAnterior = entidadService.findById(PolizaDTO.class, poliza.getCotizacionDTO().getSolicitudDTO().getIdToPolizaAnterior());
			List<IncisoCotizacionDTO>  incisosAnt = null;
			incisosAnt = incisoService.getIncisosPorLinea(polizaAnterior.getCotizacionDTO().getIdToCotizacion(), 
					SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_AUTOS);
			if(incisosAnt == null || incisosAnt.isEmpty()){
				incisosAnt = incisoService.getIncisosPorLinea(polizaAnterior.getCotizacionDTO().getIdToCotizacion(), 
						SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_CAMIONES);
			}
			if(incisosAnt != null && !incisosAnt.isEmpty()){
				List<IncisoCotizacionDTO> incisoList = incisoService.getIncisos(poliza.getCotizacionDTO().getIdToCotizacion());
				for(IncisoCotizacionDTO inciso : incisoList){
					if(!inciso.getSeccionCotizacion().getId().getIdToSeccion().equals(SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_AUTOS)&&
							!inciso.getSeccionCotizacion().getId().getIdToSeccion().equals(SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_CAMIONES)){
						inputByteArray.add(imprimirCarta(datosCarta, locale,(short) 3));
						break;
					}					
				}
			}
		}
		
		//Autofin
		if(this.esAgenteTipo(agente, CatalogoValorFijoDTO.IDGRUPO_AGENTE_AUTOFIN)){
			inputByteArray.add(imprimirCarta(datosCarta, locale,(short) 4));
		}
		
		//Venta Directa
		if(this.esAgenteTipo(agente, CatalogoValorFijoDTO.IDGRUPO_AGENTE_VENTA_DIRECTA)){
			inputByteArray.add(imprimirCarta(datosCarta, locale,(short) 5));
		}
		
		return inputByteArray;
	}

	public byte[] imprimirCarta(
			DatosCartaRenovacion datosCarta, Locale locale, Short tipo) {
			
		byte[] pdfSolicitud = null;
		List<DatosCartaRenovacion> datasourceCarta = new ArrayList<DatosCartaRenovacion>(1);
		
		try {
			InputStream caratulaStream = null;
			switch(tipo){
			case 1:
				caratulaStream = this.getClass().getResourceAsStream(
				"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/CartaRenovacionAutoplazo.jasper");
				break;
			case 2:
				caratulaStream = this.getClass().getResourceAsStream(
				"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/CartaRenovacionSucursales.jasper");
				break;
			case 3:
				caratulaStream = this.getClass().getResourceAsStream(
				"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/CartaRenovacionCambioLinea.jasper");
				break;
			case 4:
				caratulaStream = this.getClass().getResourceAsStream(
				"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/CartaRenovacionAutofin.jasper");
				break;
			case 5:
				caratulaStream = this.getClass().getResourceAsStream(
				"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/CartaRenovacionVentaDirecta.jasper");
				break;
			case 6:
				caratulaStream = this.getClass().getResourceAsStream(
				"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/CartaRenovacionEstrategas.jasper");
				break;
			case 7:
				caratulaStream = this.getClass().getResourceAsStream(
				"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/GARANTIASEGUROAUTOPLAZOAUT.pdf");
				break;				
			default:
					break;
			}
			
			if(tipo.intValue() == 7){
				try {
					return IOUtils.toByteArray(caratulaStream);
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}
			
			JasperReport reporteSolicitud = (JasperReport) JRLoader.loadObject(caratulaStream);
						

			Map<String, Object> parameters = new HashMap<String, Object>();

			if (locale != null)
	    	parameters.put(JRParameter.REPORT_LOCALE, locale);
			parameters.put("P_IMAGE_LOGO", SistemaPersistencia.LOGO_CARTAS);
			
			
			parameters.put(JRParameter.REPORT_VIRTUALIZER, new JRGzipVirtualizer(2));
			datasourceCarta.add(datosCarta);
			JRBeanCollectionDataSource dsSolicitud = new JRBeanCollectionDataSource(
					datasourceCarta);

			pdfSolicitud = JasperRunManager.runReportToPdf(reporteSolicitud,
					parameters, dsSolicitud);
		} catch (JRException ex) {
			throw new RuntimeException(ex);
		}
		return pdfSolicitud;
	}	

		public TransporteImpresionDTO getFileFromDisk(String path){
			  File file = null;
			  TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO(); 
			  byte[] byteArray = null;
			  try{		 
			    file = new File(path);		    
			    byteArray = this.getBytesFromFile(file);		    
			  }catch(IOException ex){		    
			  }
			  transporteImpresionDTO.setByteArray(byteArray);
			  return transporteImpresionDTO;
		}
		
		
		private byte[] getBytesFromFile(File file) throws IOException {
			InputStream is = null;
			try {
				is = new FileInputStream(file);  
				long length = file.length();	  
				if (length > Integer.MAX_VALUE) {   
					throw new IOException("File \"" + file + 
							"\" is too large to process.");      
				}

				return IOUtils.toByteArray(is);
			} finally {
				IOUtils.closeQuietly(is);
			}
		}	
		
		@SuppressWarnings({ "rawtypes", "unchecked" })
		public void  pdfConcantenate(List inputByteArray, OutputStream outputStream){  
		      try {
		          int pageOffset = 0;    
		          int f = 0;     
		          ArrayList master = new ArrayList();
		          Document document = null;
		          PdfCopy  writer = null;
		          Iterator iterator = inputByteArray.iterator();	          
		          while(iterator.hasNext()){   
		            byte[] data = (byte[])iterator.next();
		            if(data == null || data.length == 0){
		              f++;
		              continue;
		            }
		            PdfReader reader = new PdfReader(data);
		            reader.consolidateNamedDestinations();
		            int n = reader.getNumberOfPages();
		            List bookmarks = SimpleBookmark.getBookmark(reader);
		            if (bookmarks != null) {
		              if (pageOffset != 0)
		                SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
		                master.addAll(bookmarks);
		            }	            
		            pageOffset += n;	                
		            if (f == 0) {
		              document = new Document(reader.getPageSizeWithRotation(1));
		                                  
		              writer = new PdfCopy(document, outputStream);
		             
		              document.open();           
		            }
		            PdfImportedPage page;
		            for (int i = 0; i < n; ) {
		              ++i;
		              page = writer.getImportedPage(reader, i);
		              writer.addPage(page);
		            }	  
		            PRAcroForm form = reader.getAcroForm();
		            if (form != null){
		              writer.copyAcroForm(reader);
		            }
		            f++;
		          }
		          if (!master.isEmpty()){
		            writer.setOutlines(master);
		          }
		          if(document != null){
		            document.close();
		          }
		      }
		      catch(Exception e) {	          
		      }	      
		  }	
		
	}
