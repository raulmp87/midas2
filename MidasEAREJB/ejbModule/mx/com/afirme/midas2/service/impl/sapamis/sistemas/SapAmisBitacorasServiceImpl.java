package mx.com.afirme.midas2.service.impl.sapamis.sistemas;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.sapamis.sistemas.SapAmisBitacorasDao;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatEstatusBitacora;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatOperaciones;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisBitacoras;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisBitacorasService;

/*******************************************************************************
 * Nombre Interface: 	SapAmisBitacorasServiceImpl.
 * 
 * Descripcion: 		Clase que contiene la logica para el manejo de los 
 * 						Servicios disponibles para la gestion de Bitacoras.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class SapAmisBitacorasServiceImpl implements SapAmisBitacorasService {
	private static final long serialVersionUID = 1L;
	
	@EJB private SapAmisBitacorasDao sapAmisBitacorasDao;
	@EJB private EntidadService entidadService;

	@Override
	public SapAmisBitacoras saveObject(SapAmisBitacoras sapAmisBitacoras) {
		sapAmisBitacoras.setCatEstatusBitacora(entidadService.findById(CatEstatusBitacora.class, sapAmisBitacoras.getCatEstatusBitacora().getId()));
		sapAmisBitacoras.setCatOperaciones(entidadService.findById(CatOperaciones.class, sapAmisBitacoras.getCatOperaciones().getId()));
		entidadService.save(sapAmisBitacoras);
		return sapAmisBitacoras;
	}
	
	@Override
	public List<SapAmisBitacoras> obtenerCifras(ParametrosConsulta parametrosConsulta){
		return sapAmisBitacorasDao.obtenerCifras(parametrosConsulta);
	}
}