package mx.com.afirme.midas2.service.impl.calculos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.calculos.MovimientoAgenteDao;
import mx.com.afirme.midas2.domain.calculos.MovimientoAgente;
import mx.com.afirme.midas2.dto.calculos.DetalleCalculoComisionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.calculos.MovimientoAgenteService;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class MovimientoAgenteServiceImpl implements MovimientoAgenteService{
	private MovimientoAgenteDao movimientoAgenteDao;

	@Override
	public List<MovimientoAgente> findByFilters(MovimientoAgente filtro) {
		return movimientoAgenteDao.findByFilters(filtro);
	}
	@Override
	public List<DetalleCalculoComisionesView> obtenerMovimientosPorAgentes(Long idConfig,List<AgenteView> listadoAgentes) throws MidasException{
		return movimientoAgenteDao.obtenerMovimientosPorAgentes(idConfig,listadoAgentes);
	}
	/***Common methods**/
	@EJB
	public void setMovimientoAgenteDao(MovimientoAgenteDao movimientoAgenteDao) {
		this.movimientoAgenteDao = movimientoAgenteDao;
	}
}
