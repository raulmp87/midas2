package mx.com.afirme.midas2.service.impl.tarifa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.tarifa.TarifaAutoRangoSADao;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoRangoSA;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoRangoSAId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;
import mx.com.afirme.midas2.dto.ControlDinamicoDTO;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.tarifa.TarifaAutoRangoSAService;
import mx.com.afirme.midas2.utils.Convertidor;

@Stateless
public class TarifaAutoRangoSAServiceImpl extends JpaTarifaService<TarifaAutoRangoSAId, TarifaAutoRangoSA> implements
		TarifaAutoRangoSAService {
	
	private TarifaAutoRangoSADao tarifaAutoRangoSADao;
	private Convertidor convertidor;
	
	@EJB
	public void setTarifaAutoRangoSADao(TarifaAutoRangoSADao tarifaAutoRangoSADao){
		this.tarifaAutoRangoSADao = tarifaAutoRangoSADao;
	}
	
	@EJB
	public void setConvertidor(Convertidor convertidor) {
		this.convertidor = convertidor;
	}

	@Override
	public List<TarifaAutoRangoSA> findByFilters(
			TarifaAutoRangoSA tarifaAutoRangoSA) {
		if(tarifaAutoRangoSA == null)
			tarifaAutoRangoSA = new TarifaAutoRangoSA();
		return tarifaAutoRangoSADao.findByFilters(tarifaAutoRangoSA);
	}

	@Override
	public List<TarifaAutoRangoSA> findByTarifaVersionId(
			TarifaVersionId tarifaVersionId) {
		
		return tarifaAutoRangoSADao.findByTarifaVersionId(tarifaVersionId);
	}

	@Override
	public RegistroDinamicoDTO verDetalle(
			TarifaVersionId tarifaVersionId,
			String id,
			String accion, Class<?> vista) {

		TarifaAutoRangoSA tarifa = new TarifaAutoRangoSA();
		RegistroDinamicoDTO registro = null;
		
		if(TipoAccionDTO.getAgregarModificar().equals(accion)){
			TarifaAutoRangoSAId idTarifa = new TarifaAutoRangoSAId(tarifaVersionId);
			tarifa.setId(idTarifa);
			registro = convertidor.getRegistroDinamico(tarifa, vista);
		}else if(TipoAccionDTO.getEliminar().equals(accion)){
			TarifaAutoRangoSAId tarifaId = new TarifaAutoRangoSAId(id);
			tarifa = findById(TarifaAutoRangoSA.class, tarifaId);
			registro = convertidor.getRegistroDinamico(tarifa, vista);
			if(registro!=null){
				List<ControlDinamicoDTO> controles=registro.getControles();
				convertidor.setAttribute("editable", false, controles);
			}
		}else if(TipoAccionDTO.getEditar().equals(accion)){
			TarifaAutoRangoSAId tarifaId = new TarifaAutoRangoSAId(id);
			tarifa = findById(TarifaAutoRangoSA.class, tarifaId);
			registro = convertidor.getRegistroDinamico(tarifa, vista);
			if(registro!=null){
				List<ControlDinamicoDTO> controles=registro.getControles();
				convertidor.setAttribute("editable", false, controles,
						"id.idPaquete","id.idGrupoRC","id.codigoTipoServicioVehiculo","id.valorSumaAseguradaBase","id.valorSumaAseguradaMax",
						"id.numeroToneladasMin","id.numeroToneladasMax");
			}
		}
		
		return registro;
	}

	@Override
	public List<RegistroDinamicoDTO> getRegistrosDinamicosPorTarifaVersionId(
			TarifaVersionId tarifaVersionId, Class<?> vista) {
		
		List<TarifaAutoRangoSA> tarifas = this.findByTarifaVersionId(tarifaVersionId);
		List<RegistroDinamicoDTO> registros = convertidor.getListaRegistrosDinamicos(tarifas, vista);

		return registros;
	}

	public TarifaAutoRangoSA getNewObject(){
		return new TarifaAutoRangoSA();
	}
}
