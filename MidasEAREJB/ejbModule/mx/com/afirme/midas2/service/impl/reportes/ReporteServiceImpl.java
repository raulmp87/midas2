package mx.com.afirme.midas2.service.impl.reportes;

import java.io.File;
import java.math.BigDecimal;
import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas2.service.reportes.ReporteService;

@Stateless
public class ReporteServiceImpl implements ReporteService{
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Date getUltimaFechaGeneracionDporP() {
		Date fecha = null;
		try {			
			StringBuilder queryString = new StringBuilder();
			queryString.append(" select MIDAS.PKG_DXP.fn_fecha_gen() from dual");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			
			fecha = (Date)query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fecha;
	}

	@Override
	public Integer getEjecutandoGeneracionDXP() {
		Integer ejecutandoDXP = 0;
		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append(" select MIDAS.PKG_DXP.fn_dxp_ejecutandose() from dual");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			
			Object valor = query.getSingleResult();
			
			if(valor instanceof BigDecimal){
				ejecutandoDXP = ((BigDecimal) valor).intValue();
			}else if(valor instanceof Double){
				ejecutandoDXP = ((Double) valor).intValue();
			}else if(valor instanceof Integer){
				ejecutandoDXP = ((Integer) valor).intValue();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ejecutandoDXP;
	}

	@Override
	public void generaDXP(Date pfecha) {
		try{
			
			File dxp = new File(this.getPath()+"DXPD.txt");
			
			if(dxp.delete())
			{
				System.out.println("generaDXP...si se borro archivo. " + pfecha);
			}else
				System.out.println("generaDXP...no se borro archivo. " + pfecha);
			 
			String[] atributosDTO = { "" };
			String[] columnasCursor = { ""};
			String spName = "MIDAS.PKG_DXP.stp_genera_dxp";
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
				spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pfecha", pfecha);
		
			storedHelper.estableceMapeoResultados("void.class", atributosDTO, columnasCursor);
			int resultado = storedHelper.ejecutaActualizar();
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	} 
	
	private String getPath(){
		String OSName = System.getProperty("os.name");
		String uploadFolder = "";
		if (OSName.toLowerCase().indexOf("windows")!=-1)
			uploadFolder = SistemaPersistencia.UPLOAD_FOLDER;
		else
			uploadFolder = SistemaPersistencia.LINUX_UPLOAD_FOLDER;
		
		return uploadFolder;
	}


}
