package mx.com.afirme.midas2.service.impl.negocio.cliente.grupo;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.negocio.cliente.grupo.NegocioGrupoClienteDao;
import mx.com.afirme.midas2.domain.cliente.GrupoCliente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.cliente.grupo.NegocioGrupoCliente;
import mx.com.afirme.midas2.service.negocio.cliente.grupo.NegocioGrupoClienteService;

@Stateless
public class NegocioGrupoClienteServiceImpl implements NegocioGrupoClienteService {

	@EJB
	private NegocioGrupoClienteDao negocioGrupoClienteDao;
	
	@Override
	public boolean relacionarGrupoClienteNegocio(Long idGrupo, Long idNegocio) {
		NegocioGrupoCliente negGpoCte = new NegocioGrupoCliente();
		
		//settear el grupo
		GrupoCliente grupoCliente = new GrupoCliente();
		grupoCliente.setId(idGrupo);
		negGpoCte.setGrupoClientes(grupoCliente);
		
		//settear el negocio
		Negocio negocio = new Negocio();
		negocio.setIdToNegocio(idNegocio);
		negGpoCte.setNegocio(negocio);
		try{
			List<NegocioGrupoCliente> listaNegocioGrupo = negocioGrupoClienteDao.findByFilters(negGpoCte);
			if(listaNegocioGrupo != null && listaNegocioGrupo.isEmpty()){
				negocioGrupoClienteDao.persist(negGpoCte);
			}else{
				return false;
			}
		}catch(RuntimeException e){
			throw e;
		}
		return true;
	}

	@Override
	public List<NegocioGrupoCliente> listarPorNegocio(Long idNegocio) {
		return negocioGrupoClienteDao.findByProperty("negocio.idToNegocio", idNegocio);
	}

	@Override
	public Long desasociarGrupoClienteNegocio(Long idGrupo, Long idNegocio) {
		NegocioGrupoCliente negocioGrupo = new NegocioGrupoCliente();
		negocioGrupo.setGrupoClientes(new GrupoCliente());
		negocioGrupo.getGrupoClientes().setId(idGrupo);
		Negocio negocio = new Negocio();
		negocio.setIdToNegocio(Long.valueOf(idNegocio));
		negocioGrupo.setNegocio(negocio);
		
		List<NegocioGrupoCliente> listaNegocioGrupo = negocioGrupoClienteDao.findByFilters(negocioGrupo);
		try{
			for(NegocioGrupoCliente negCte : listaNegocioGrupo){
				negocioGrupoClienteDao.remove(negCte);
			}
		}catch(RuntimeException e){
			return idNegocio;
		}
		return null;
	}

	@Override
	public Long desasociarGruposClienteNegocio(Long idNegocio) {
		NegocioGrupoCliente negocioGrupo = new NegocioGrupoCliente();
		Negocio negocio = new Negocio();
		negocio.setIdToNegocio(idNegocio);
		negocioGrupo.setNegocio(negocio);
		
		List<NegocioGrupoCliente> listaNegocioGrupo = negocioGrupoClienteDao.findByFilters(negocioGrupo);
		try{
			for(NegocioGrupoCliente negCte : listaNegocioGrupo){
				negocioGrupoClienteDao.remove(negCte);
			}
		}catch(RuntimeException e){
			return idNegocio;
		}
		return null;
	}

}
