package mx.com.afirme.midas2.service.impl.calculos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.calculos.AgentesCalculoTemporalDao;
import mx.com.afirme.midas2.domain.calculos.AgentesCalculoTemporal;
import mx.com.afirme.midas2.service.calculos.AgentesCalculoTemporalService;
import mx.com.afirme.midas2.util.MidasException;
@Stateless
public class AgentesCalculoTemporalServiceImpl implements AgentesCalculoTemporalService{
	private AgentesCalculoTemporalDao dao;
	@Override
	public void delete(Long arg0) throws MidasException {
		dao.delete(arg0);
	}

	@Override
	public List<AgentesCalculoTemporal> findByCatalogo(String arg0, Long arg1)throws MidasException {
		return dao.findByCatalogo(arg0, arg1);
	}

	@Override
	public Long getNextIdCalculo() {
		return dao.getNextIdCalculo();
	}

	@Override
	public Long save(AgentesCalculoTemporal arg0) throws MidasException {
		return dao.save(arg0);
	}

	@Override
	public Long save(Long arg0, String arg1, Long arg2) throws MidasException {
		return dao.save(arg0, arg1, arg2);
	}

	@Override
	public void saveAll(List<Long> arg0, String arg1, Long arg2)throws MidasException {
		dao.saveAll(arg0, arg1, arg2);
	}
	@EJB
	public void setDao(AgentesCalculoTemporalDao dao) {
		this.dao = dao;
	}
}
