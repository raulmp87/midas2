/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoDocumentoDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoDocumento;
import mx.com.afirme.midas2.service.compensaciones.CaTipoDocumentoService;

import org.apache.log4j.Logger;

@Stateless

public class CaTipoDocumentoServiceImpl  implements CaTipoDocumentoService {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@EJB
	private CaTipoDocumentoDao tipoDocumentocaDao;
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaTipoDocumentoServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaTipoDocumento entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoDocumento entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoDocumento entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoDocumento 	::		CaTipoDocumentoServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	tipoDocumentocaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoDocumento 	::		CaTipoDocumentoServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoDocumento 	::		CaTipoDocumentoServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaTipoDocumento entity.
	  @param entity CaTipoDocumento entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoDocumento entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoDocumento 	::		CaTipoDocumentoServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaTipoDocumento.class, entity.getId());
	        	tipoDocumentocaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoDocumento 	::		CaTipoDocumentoServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoDocumento 	::		CaTipoDocumentoServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaTipoDocumento entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoDocumento entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoDocumento entity to update
	 @return CaTipoDocumento the persisted CaTipoDocumento entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaTipoDocumento update(CaTipoDocumento entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoDocumento 	::		CaTipoDocumentoServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoDocumento result = tipoDocumentocaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoDocumento 	::		CaTipoDocumentoServiceImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoDocumento 	::		CaTipoDocumentoServiceImpl	::	update	::	ERROR	::	",re);
	        throw re;
        }
    }
    
    public CaTipoDocumento findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoDocumentoServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoDocumento instance = tipoDocumentocaDao.findById(id);//entityManager.find(CaTipoDocumento.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoDocumentoServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoDocumentoServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaTipoDocumento entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoDocumento property to query
	  @param value the property value to match
	  	  @return List<CaTipoDocumento> found by query
	 */
    public List<CaTipoDocumento> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoDocumentoServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoDocumento model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoDocumentoServiceImpl	::	findByProperty	::	FIN	::	");
			return tipoDocumentocaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoDocumentoServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoDocumento> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoDocumento> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoDocumento> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoDocumento> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaTipoDocumento entities.
	  	  @return List<CaTipoDocumento> all CaTipoDocumento entities
	 */
	public List<CaTipoDocumento> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoDocumentoServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoDocumento model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoDocumentoServiceImpl	::	findAll	::	FIN	::	");
			return tipoDocumentocaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoDocumentoServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}