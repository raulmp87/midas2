package mx.com.afirme.midas2.service.impl.jobAgentes;
import static mx.com.afirme.midas2.utils.CommonUtils.compareOnlyDatesWithoutTime;
import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;

import mx.com.afirme.midas2.domain.comisiones.ConfigComisiones;
import mx.com.afirme.midas2.domain.jobAgentes.TareaProgramada;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.calculos.CalculoComisionesService;
import mx.com.afirme.midas2.service.impl.jobAgentes.ProgramacionCalculoBonosServiceImpl.conceptoEjecucionAutomatica;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionCalculoComisionesService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MidasException;
/**
 * Servicio para la programacion de calculo de comisiones
 * @author vmhersil
 *
 */
@Stateless
public class ProgramacionCalculoComisionesServiceImpl implements ProgramacionCalculoComisionesService{
	private CalculoComisionesService calculoComisionesService;
	@Resource	
	private TimerService timerService;	
	@EJB
	private SistemaContext sistemaContext;
	public static final Logger LOG = Logger.getLogger(ProgramacionCalculoComisionesServiceImpl.class);
	
	@Override
	public void executeTasks() {
		LOG.info("Ejecutando ProgramacionCalculoComisionesService.executeTasks()...");
		List<TareaProgramada> tasks=getTaskToDo(conceptoEjecucionAutomatica.BONOS.getValue());
		Date currentDate=new Date();
		if(!isEmptyList(tasks)){
			for(TareaProgramada task:tasks){
				if(compareOnlyDatesWithoutTime(currentDate, task.getFechaEjecucion())){
					try{
						Long idConfig=task.getId();
						List<AgenteView> listaAgentesDelCalculo=calculoComisionesService.cargarAgentesPorConfiguracion(idConfig);
						//Long idCalculoTemporal=calculoComisionesService.getNextIdCalculoTemporal();
						//calculoComisionesService.guardarAgentesEnTemporal(listaAgentesDelCalculo, idCalculoTemporal);
						String queryListaAgentes=calculoComisionesService.cargarAgentesPorConfiguracionQuery(idConfig);
						ConfigComisiones configuracion=new ConfigComisiones();
						configuracion.setId(idConfig);
						calculoComisionesService.generarCalculo(configuracion,listaAgentesDelCalculo,null,queryListaAgentes);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}
	}
	/**
	 * Lista de tareas
	 * @return lista de tareas programadas
	 */
	@Override
	public List<TareaProgramada> getTaskToDo(String conceptoEjecucionAutomatica) {
		List<TareaProgramada> tasks=new ArrayList<TareaProgramada>();
		try {
			List<ConfigComisiones> listaComisiones=calculoComisionesService.obtenerConfiguracionesAutomaticasActivas();
			if(!isEmptyList(listaComisiones)){
				for(ConfigComisiones config:listaComisiones){
					if(isNotNull(config)){
						TareaProgramada task=new TareaProgramada();
						task.setId(config.getId());
						task.setFechaEjecucion(config.getFechaInicioVigencia());
						tasks.add(task);
					}
				}
			}
		} catch (MidasException e) {
			e.printStackTrace();
		}
		return tasks;
	}
	
	public void initialize() {
		String timerInfo = "TimerProgramacionCalculoComisiones";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0/1 * * * ?
				expression.minute("0/1");
				expression.hour("*");
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerProgramacionCalculoComisiones", false));
				
				LOG.info("Tarea TimerProgramacionCalculoComisiones configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerProgramacionCalculoComisiones");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerProgramacionCalculoComisiones:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		executeTasks();	
	}
	
	/**
	 * ====================================================
	 * Sets and Gets
	 * ====================================================
	 */
	@EJB
	public void setCalculoComisionesService(CalculoComisionesService calculoComisionesService) {
		this.calculoComisionesService = calculoComisionesService;
	}
}
