package mx.com.afirme.midas2.service.impl.compensaciones.negociovida;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import mx.com.afirme.midas2.dao.compensaciones.NegocioVidaDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVida;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVidaAgente;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVidaGerencia;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVidaProducto;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVidaRamo;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVidaSeccion;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVidaSubRamo;
import mx.com.afirme.midas2.domain.emision.ppct.AgenteSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.GerenciaSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.LineaNegocioSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.ProductoSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.RamoSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.SubRamoSeycos;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.compensaciones.negociovida.NegocioVidaService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Stateless
public class NegocioVidaServiceImpl extends EntidadHistoricoDaoImpl implements NegocioVidaService {

	private static final Logger LOG = LoggerFactory.getLogger(NegocioVidaServiceImpl.class);
	@PersistenceContext 
	private EntityManager entityManager;
	
	@EJB
	NegocioVidaDao negocioVidaDao;
	//private static  Long GRUPO = 13; 
	//private static  Long INDIVIDUAL = 14; 
	
	public void deleteConfiguration(NegocioVida config) throws Exception {
		if (config == null || config.getId() == null) {
			onError("Favor de proporcionar la configuracion que desea eliminar");
		}
		NegocioVida configuracion = entidadService.findById(NegocioVida.class,
				config.getId());
		if (configuracion == null) {
			onError("La configuracion con la clave " + config.getId()
					+ " no existe");
		}
		configuracion.setBajalogica(new Short("1"));// Se inactiva la
													// configuracion
		entidadService.save(configuracion);
	}
	
	@SuppressWarnings("unchecked")
	public List<NegocioVida> findByFilters(NegocioVida config) throws Exception {
		try {
			String descripcion="";
			final StringBuilder queryString = new StringBuilder("");
			queryString.append(" SELECT NEG.* FROM MIDAS.CA_NEGOCIOVIDA NEG ");
		    queryString.append(" WHERE UPPER(DESCRIPCIONNEGOCIOVIDA) LIKE  ?1 ");
		    if(config.getId()!=null){
		    	queryString.append(" AND ID = ?2 ");
		    }
		    String finalQuery=getQueryString(queryString) ;
		    Query query = entityManager.createNativeQuery(finalQuery, NegocioVida.class);
		    descripcion=config.getDescripcionnegociovida()==null?" ":config.getDescripcionnegociovida();
		    query.setParameter(1, "%"+descripcion+"%");
		    if(config.getId()!=null){
		    	query.setParameter(2, config.getId());
		    }		    
		    return query.getResultList();
		} catch (RuntimeException e) {
			LOG.error("--error findByFilters()", e);
			throw e;
		}	
	}
	
	@SuppressWarnings("unchecked")
	public List<GenericaAgentesView> getProductosVidaView(Long idParam)  throws Exception {
		LOG.info(">> getRamosPorProductosView()");
		List<GenericaAgentesView> lista= new ArrayList<GenericaAgentesView>();	
		try {
			final StringBuilder queryString = new StringBuilder("");
			queryString.append(" SELECT DP.ID_PRODUCTO AS ID, DP.ID_PRODUCTO||'-'||DP.NOM_PRODUCTO AS DESCRIPCION  ");
		    queryString.append(" FROM SEYCOS.DP_PRODUCTO DP ");
		    queryString.append(" INNER JOIN SEYCOS.DP_PRODUCTO_VER DPV ON (DP.ID_PRODUCTO = DPV.ID_PRODUCTO) ");
		    queryString.append(" AND DPV.F_TER_REG > SYSDATE ");
		    if(idParam == 13){
		    	queryString.append(" WHERE DP.CVE_T_POLIZA IN ('GPO') ");
		    }else if(idParam == 14){
		    	queryString.append(" WHERE DP.CVE_T_POLIZA IN ('INML') ");
		    	queryString.append(" AND DP.ID_PRODUCTO <> 1403 AND DP.ID_PRODUCTO <> 1407 AND DP.ID_PRODUCTO <> 1435 AND DP.ID_PRODUCTO <> 1455 AND DP.ID_PRODUCTO <> 1457 AND DP.ID_PRODUCTO <> 1460 ");
		    }else{
		    	queryString.append("WHERE DP.CVE_T_POLIZA IN ('INML', 'GPO') ");
		    }
		    queryString.append(" AND DP.ID_PRODUCTO <> 2 ");
		    queryString.append(" AND DP.ID_PRODUCTO <> 1399  AND DP.ID_PRODUCTO <> 1285 ");
		    queryString.append(" ORDER BY ID ");
		Query query=entityManager.createNativeQuery(queryString.toString(),"productoXLineaVentaView");
		query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
		lista= query.getResultList();
		LOG.info("listaSeycos>>>>>>>"+lista.size());
		} catch (RuntimeException re) {
			LOG.error("--error getProductosVidaView()", re);
			throw re;
		}
		
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	public List<GenericaAgentesView> getRamosPorProductosView(String idProducto)   throws Exception{
		LOG.info(">> getRamosPorProductosView() idProducto=>{}",idProducto);
		List<GenericaAgentesView> listaRamos = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		try {
			if(!idProducto.equals("")){//isNotNull(idProducto)){
				queryString.append(" SELECT IR.ID_RAMO_CONTABLE AS ID, IR.ID_RAMO_CONTABLE||'-'||IR.NOM_RAMO AS DESCRIPCION "); 
				queryString.append(" FROM SEYCOS.ING_RAMO IR  "); 
				queryString.append(" WHERE IR.ID_RAMO_CONTABLE IN ('13','14') ");
				Query query = entityManager.createNativeQuery(queryString.toString(),"productoXLineaVentaView");
				query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
				//query.setParameter(1,idProducto);
				listaRamos = query.getResultList();
				LOG.info("listaRamosSeycos>>>>>>>"+listaRamos.size());
				return listaRamos;
			}
			return null;
		} catch (RuntimeException re) {
			LOG.error("--error getRamosPorProductosView()", re);
			throw re;
		}

	}
	
	@SuppressWarnings("unchecked")
	public List<GenericaAgentesView> getSubramosPorRamoView(String ramos) throws Exception {
		LOG.info(">> getSubramosPorRamoView()");
		List<GenericaAgentesView> listaSubRamos = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		try {
			if(!ramos.equals("")){			
				//ramos = ramos.substring(0, ramos.length()-1);
				queryString.append(" SELECT ISR.ID_RAMO_CONTABLE AS ID, ISR.ID_RAMO_CONTABLE||'-'||ISR.NOM_SUBRAMO AS DESCRIPCION "); 
				queryString.append(" FROM SEYCOS.ING_RAMO_SUBRAMO ISR  "); 
				queryString.append(" WHERE ISR.ID_RAMO_CONTABLE IN (?1) ");			
				queryString.append(" AND ISR.ID_SUBR_CONTABLE = 01 ");
				queryString.append(" ORDER BY ID DESC ");
				Query query = entityManager.createNativeQuery(queryString.toString(),"productoXLineaVentaView");
				query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
				query.setParameter(1, ramos);
				listaSubRamos = query.getResultList();
				LOG.info("listaSubRamosSeycos>>>>>>>"+listaSubRamos.size());
				return listaSubRamos;
			}			
			return null;
		} catch (RuntimeException re) {
			LOG.error("--error getSubramosPorRamoView()", re);
			throw re;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<GenericaAgentesView> getLineasNegocioPorRamoProductoView(String productos,String ramos) throws Exception {
		LOG.info(">> getLineasNegocioPorRamoProductoView()");
		List<GenericaAgentesView> listaLineaNegocios = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		try {
			if(!productos.equals("")){
				queryString.append(" SELECT DPL.ID_LIN_NEGOCIO AS ID,DPL.ID_LIN_NEGOCIO||'-'||DPL.NOM_LINEA_NEG AS DESCRIPCION "); 
				queryString.append(" FROM SEYCOS.DP_PRODUCTO DP  "); 
				queryString.append(" INNER JOIN SEYCOS.DP_PRODUCTO_VER DPV ON (DP.ID_PRODUCTO = DPV.ID_PRODUCTO AND DPV.F_TER_REG > SYSDATE) ");			
				queryString.append(" INNER JOIN SEYCOS.DP_PR_LIN DPPL ON (DPV.ID_PRODUCTO = DPPL.ID_PRODUCTO AND DPV.ID_VERSION = DPPL.ID_VERSION)");
				queryString.append(" INNER JOIN SEYCOS.DP_LINEA_NEG DPL ON DPPL.ID_LIN_NEGOCIO = DPL.ID_LIN_NEGOCIO ");	
				queryString.append(" WHERE  DP.CVE_T_POLIZA IN ('INML','GPO') ");
				queryString.append(" AND DP.ID_PRODUCTO <> 2 ");
				queryString.append(" AND DP.ID_PRODUCTO IN (?1) ");
				queryString.append(" ORDER BY DP.ID_PRODUCTO , DPL.ID_LIN_NEGOCIO ");
				Query query = entityManager.createNativeQuery(queryString.toString(),"productoXLineaVentaView");
				query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
				query.setParameter(1, productos);
				listaLineaNegocios = query.getResultList();
				LOG.info("listaLineaNegociosSeycos>>>>>>>"+listaLineaNegocios.size());
				
				return listaLineaNegocios;
			}
			
			return null;
		} catch (RuntimeException re) {
			LOG.error("--error getSubramosPorRamoView()", re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<GenericaAgentesView> getGerenciaVidaView()  throws Exception {
		LOG.info(">> getGerenciaVidaView()");
		List<GenericaAgentesView> listaGerencia= new ArrayList<GenericaAgentesView>();	
		try {
			final StringBuilder queryString = new StringBuilder("");
			queryString.append(" SELECT GR.ID_GERENCIA AS ID, GR.ID_GERENCIA||'-'||GR.NOM_GERENCIA AS DESCRIPCION  ");
			queryString.append(" FROM SEYCOS.GERENCIA  GR  ");
		    queryString.append(" WHERE GR.B_ULT_MOD='V' ");
		    queryString.append(" AND GR.SIT_GERENCIA='A' ");
		    queryString.append(" ORDER BY GR.ID_GERENCIA ");
		Query query=entityManager.createNativeQuery(queryString.toString(),"productoXLineaVentaView");
		query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
		listaGerencia= query.getResultList();
		
		LOG.info("listaSeycos>>>>>>>"+listaGerencia.size());
		} catch (RuntimeException re) {
			LOG.error("--error getProductosVidaView()", re);
			throw re;
		}
		
		return listaGerencia;
	}
	
	@SuppressWarnings("unchecked")
	public List<DatosSeycos> getAgenteVidaView(String agente, String idGerencias)  throws Exception {
		LOG.info(">> getAgenteVidaView() idGerencias>"+idGerencias);
		List<DatosSeycos> listaAgenteSeycos= new ArrayList<DatosSeycos>();	
		try {
			agente.toUpperCase();
			final StringBuilder queryString = new StringBuilder("");
			queryString.append(" SELECT DISTINCT AGT.ID_AGENTE, AGT.ID_EMPRESA, AGT.F_SIT, PER.NOMBRE ");
			queryString.append(" FROM SEYCOS.AGENTE AGT ");
			queryString.append(" INNER JOIN SEYCOS.PERSONA PER ON AGT.ID_PERSONA = PER.ID_PERSONA ");
			queryString.append(" LEFT JOIN seycos.persona_ext pex ON pex.id_persona = agt.id_persona ");
			queryString.append(" INNER JOIN seycos.supervisoria sup ");
			queryString.append(" ON sup.id_empresa = agt.id_empresa ");
			queryString.append(" AND sup.id_supervisoria = agt.id_supervisoria ");
			queryString.append(" AND sup.b_ult_mod = 'V' ");
			queryString.append(" INNER JOIN seycos.oficina ofi ");
			queryString.append(" ON ofi.id_empresa = sup.id_empresa ");
			queryString.append(" AND ofi.id_oficina = sup.id_oficina ");
			queryString.append(" AND ofi.b_ult_mod = 'V' ");
			queryString.append(" INNER JOIN seycos.gerencia ger ");
			queryString.append(" ON ger.id_empresa = ofi.id_empresa ");
			queryString.append(" AND ger.id_gerencia = ofi.id_gerencia ");
			queryString.append(" AND ger.b_ult_mod = 'V' ");      
			queryString.append(" INNER JOIN MIDAS.TOAGENTE TOAGT ON TOAGT.IDAGENTE = AGT.ID_AGENTE ");
			queryString.append(" WHERE AGT.SIT_AGENTE = 'A' ");
			queryString.append(" AND AGT.B_ULT_MOD = 'V' ");
			queryString.append(" AND PER.NOMBRE LIKE UPPER('%" + agente.trim() + "%') ");
			if ( idGerencias != null && !idGerencias.isEmpty()){
			queryString.append(" AND ger.id_gerencia IN ("+idGerencias.trim()+") ");
			}
			
			String finalQuery=getQueryString(queryString) ;
			Query query=entityManager.createNativeQuery(finalQuery);	
			
			List<Object[]> resultList=query.getResultList();
			for (Object[] result : resultList){ 
				DatosSeycos agenteSeycos= new DatosSeycos();
				agenteSeycos.setId(new Long(((BigDecimal)result[0]).toString()));
				agenteSeycos.setIdAgente(result[0].toString());		    	  
				agenteSeycos.setIdEmpresa(result[1].toString());
				agenteSeycos.setFsit(result[2].toString());
				agenteSeycos.setNombreCompleto(result[3].toString());
				listaAgenteSeycos.add(agenteSeycos);
			}
		      
		} catch (RuntimeException e) {
			LOG.error("--error getAgenteVidaView()", e);
			throw e;
		}
		LOG.info(">> getAgenteVidaView()");
		return listaAgenteSeycos;
	}
	public List<DatosSeycos> findAgenteSeycosConPromotor(DatosSeycos datosSeycos) {
		
		return negocioVidaDao.findAgenteSeycosConPromotor(datosSeycos);
		
	}

	@SuppressWarnings("unchecked")
	public List<DatosSeycos> findAgenteSeycos(DatosSeycos datosSeycos) {
		
		LOG.info(">> findAgenteSeycos()");
		List<DatosSeycos> listaAgenteSeycos = new ArrayList<DatosSeycos>();	
		
		try {
			
			int index = 1;
			
			final StringBuilder queryString = new StringBuilder(" SELECT ");
			queryString.append(" AGE.ID_AGENTE ");
			queryString.append(" , VMEF.NOMBRE AS NOMBREAGENTE ");
			queryString.append(" , AGE.ID_EMPRESA  ");
			queryString.append(" , AGE.F_SIT ");
			queryString.append(" , AGE.ID_SUPERVISORIA ");
			queryString.append(" , VMEF.NOM_SUPERVISORIA AS NOMBRESUPERVISORIA");
			queryString.append(" FROM SEYCOS.VW_EST_FV VMEF");
			queryString.append(" INNER JOIN SEYCOS.AGENTE AGE ON (AGE.ID_AGENTE = VMEF.ID_AGENTE) ");
			queryString.append(" WHERE AGE.SIT_AGENTE = 'A' ");
			queryString.append(" AND AGE.ID_AGENTE = ? ");
			queryString.append(" AND AGE.ID_EMPRESA = ? ");
			queryString.append(" AND AGE.F_SIT = ? ");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			
			query.setParameter(index++, datosSeycos.getIdAgente());
			query.setParameter(index++, datosSeycos.getIdEmpresa());
			query.setParameter(index++, datosSeycos.getDateFsit());
			
			List<Object[]> resultList = query.getResultList();
			DatosSeycos agenteSeycos = null;
			
			for (Object[] result : resultList){ 
				agenteSeycos = new DatosSeycos();				
				agenteSeycos.setIdAgente(result[0].toString());		   
				agenteSeycos.setNombreCompleto(result[1].toString());
				agenteSeycos.setIdEmpresa(result[2].toString());
				agenteSeycos.setFsit(result[3].toString());				
				agenteSeycos.setIdSupervisoria(result[4].toString());
				agenteSeycos.setNombreSupervisoria(result[5].toString());
				listaAgenteSeycos.add(agenteSeycos);
			}
		      
		} catch (RuntimeException e) {
			LOG.error("Información del Error", e);
		}
		LOG.info(">> findAgenteSeycos()");
		return listaAgenteSeycos;
	}
	
	@SuppressWarnings("unchecked")
	public List<GerenciaSeycos> getGerenciaVida(Long gerencia) {
		LOG.info(">> getGerenciaVida()");
		List<GerenciaSeycos> listaGerenciaSeycos= new ArrayList<GerenciaSeycos>();	
		try {
			final StringBuilder queryString = new StringBuilder("");
			queryString.append(" SELECT * FROM SEYCOS.GERENCIA AGT ");
		    queryString.append(" WHERE AGT.B_ULT_MOD = 'V' ");
		    queryString.append(" AND AGT.SIT_GERENCIA = 'A' ");
		    queryString.append(" AND AGT.ID_GERENCIA = ");
		    queryString.append(gerencia);
		    String finalQuery=getQueryString(queryString) ;
		    Query query = entityManager.createNativeQuery(finalQuery, GerenciaSeycos.class);		    
		    listaGerenciaSeycos = query.getResultList();
		} catch (RuntimeException e) {
			LOG.error("--error getGerenciaVida()", e);
			throw e;
		}		
		LOG.info("<< getGerenciaVida()");
		return listaGerenciaSeycos;
	}
	
	@SuppressWarnings("unchecked")
	public List<AgenteSeycos> getAgenteVida(Long agente) {
		LOG.info(">> getAgenteVida()");
		List<AgenteSeycos> listaAgenteSeycos= new ArrayList<AgenteSeycos>();	
		try {
			final StringBuilder queryString = new StringBuilder("");
			queryString.append(" SELECT AGT.* "); 
			queryString.append(" FROM SEYCOS.AGENTE AGT"); 
		    queryString.append(" WHERE AGT.B_ULT_MOD = 'V' ");
		    queryString.append(" AND AGT.SIT_AGENTE = 'A'");
		    queryString.append(" AND AGT.ID_AGENTE = ");
		    queryString.append(agente);
		    queryString.append(" ORDER BY AGT.ID_AGENTE ");		    
		    String finalQuery=getQueryString(queryString) ;
		    Query query = entityManager.createNativeQuery(finalQuery, AgenteSeycos.class);		    
		    listaAgenteSeycos = query.getResultList();
		} catch (RuntimeException e) {
			LOG.error("--error getAgenteVida()", e);
			throw e;
		}		
		LOG.info("<< getAgenteVida()");
		return listaAgenteSeycos;
	}
	@SuppressWarnings("unchecked")
	public List<SubRamoSeycos> getSubRamoVida(Long subRamo) {
		LOG.info(">> getSubRamoVida()");
		List<SubRamoSeycos> listaSubRamoSeycos= new ArrayList<SubRamoSeycos>();	
		try {
			final StringBuilder queryString = new StringBuilder("");
			queryString.append(" SELECT * FROM SEYCOS.ING_RAMO_SUBRAMO SR  ");
		    queryString.append(" WHERE SR.ID_RAMO_CONTABLE IN (" +subRamo+ ") ");
		    String finalQuery=getQueryString(queryString) ;
		    Query query = entityManager.createNativeQuery(finalQuery, SubRamoSeycos.class);		    
		    listaSubRamoSeycos = query.getResultList();
		} catch (RuntimeException e) {
			LOG.error("--error getSubRamoVida()", e);
			throw e;
		}		
		return listaSubRamoSeycos;
	}
	
	@SuppressWarnings("unchecked")
	public List<RamoSeycos> getRamoVida(Long ramo) {
		LOG.info(">> getRamoVida()");
		List<RamoSeycos> listaRamoSeycos= new ArrayList<RamoSeycos>();	
		try {
			final StringBuilder queryString = new StringBuilder("");
			queryString.append(" SELECT * FROM SEYCOS.ING_RAMO IR ");
		    queryString.append(" WHERE IR.ID_RAMO_CONTABLE IN (" +ramo+ ") ");
		    String finalQuery=getQueryString(queryString) ;
		    Query query = entityManager.createNativeQuery(finalQuery, RamoSeycos.class);		    
		    listaRamoSeycos=query.getResultList();
		} catch (RuntimeException e) {
			LOG.error("--error getRamoVida()", e);
			throw e;
		}		
		return listaRamoSeycos;
	}
	@SuppressWarnings("unchecked")
	public List<ProductoSeycos> getProductoVida(Long producto) {
		LOG.info(">> getProductoVida()");
		List<ProductoSeycos> listaProductoSeycos= new ArrayList<ProductoSeycos>();	
		try {
			final StringBuilder queryString = new StringBuilder("");
			queryString.append(" SELECT * FROM SEYCOS.DP_PRODUCTO DP  ");
		    queryString.append(" WHERE DP.ID_PRODUCTO IN (" +producto+ ") ");
		    String finalQuery=getQueryString(queryString) ;
		    Query query = entityManager.createNativeQuery(finalQuery, ProductoSeycos.class);		    
		    listaProductoSeycos = query.getResultList();
		} catch (RuntimeException e) {
			LOG.error("--error getProductoVida()", e);
			throw e;
		}		
		return listaProductoSeycos;
	}
	
	@SuppressWarnings("unchecked")
	public List<LineaNegocioSeycos> getLineaNegocioVida(Long lineaNegocio) {
		LOG.info(">> getLineaNegocioVida()");
		List<LineaNegocioSeycos> listapProductoSeycos= new ArrayList<LineaNegocioSeycos>();	
		try {
			final StringBuilder queryString = new StringBuilder(" ");
			queryString.append(" select dpl.id_lin_negocio, dpl.nom_linea_neg, dpl.nom_cto_lin, dpl.cve_division, " );
			queryString.append(" dpl.cve_t_bien, dpv.tx_observ,dpl.cve_metodo_cont, dpl.cve_t_operacion, dpl.cve_t_dist_rea ");					
			queryString.append(" from seycos.dp_producto dp ");
			queryString.append(" inner join seycos.dp_producto_ver dpv on dp.id_producto = dpv.id_producto and dpv.f_ter_reg > sysdate ");
			queryString.append(" inner join seycos.dp_pr_lin dppl on dpv.id_producto=dppl.id_producto and dpv.id_version=dppl.id_version ");
			queryString.append(" inner join seycos.dp_linea_neg dpl on dppl.id_lin_negocio=dpl.id_lin_negocio ");
			queryString.append(" where  dp.cve_t_poliza in('INML','GPO') ");
			queryString.append(" and dp.id_producto<> 2 ");
		    queryString.append(" and dpl.id_lin_negocio in (" +lineaNegocio+ ") ");
		    String finalQuery=getQueryString(queryString) ;
		    Query query = entityManager.createNativeQuery(finalQuery);
		    List<Object[]> resultList=query.getResultList();
		      for (Object[] result : resultList){ 
		    	  LineaNegocioSeycos lineaNegocioSeycos= new LineaNegocioSeycos();
		    	  lineaNegocioSeycos.setIdLinNegocio(new Long(result[0].toString()));
		    	  lineaNegocioSeycos.setNomLineaNeg(result[1].toString());
		    	  lineaNegocioSeycos.setNomCtoLin(result[2].toString());
		    	  lineaNegocioSeycos.setCveDivision(result[3].toString());
		    	  lineaNegocioSeycos.setCveTBien(result[4].toString());
		    	  lineaNegocioSeycos.setTxObserv(result[5].toString());
		    	  lineaNegocioSeycos.setCveMetodoCont(result[6].toString());
		    	  lineaNegocioSeycos.setCveTOperacion(result[7].toString());
		    	  lineaNegocioSeycos.setCveTDistRea(result[8].toString());
		    	  listapProductoSeycos.add(lineaNegocioSeycos);
	            }
		} catch (RuntimeException e) {
			LOG.error("--error getLineaNegocioVida()", e);
			throw e;
		}		
		return listapProductoSeycos;
	}
	
	public NegocioVida saveConfiguration(NegocioVida param) {
		if (param == null) {
			throw new RuntimeException("Favor de proporcionar la configuracion a guardar");
		}
		param = entidadService.save(param);
		saveConfigRelationships(param);
		
		return param;
	}

	private void saveConfigRelationships(NegocioVida config) {
		saveAgentesConfig(config);
		saveProductoConfig(config);
		saveRamoConfig(config);
		saveSubramoConfig(config);
		saveSeccionConfig(config);
		saveGerenciaConfig(config);
	}

	private void saveProductoConfig(NegocioVida config) {

		Predicate filtro;
		List<NegocioVidaProducto> aBorrar = new ArrayListNullAware<NegocioVidaProducto>();
		List<NegocioVidaProducto> aCrear = new ArrayListNullAware<NegocioVidaProducto>();

		List<NegocioVidaProducto> actuales = entidadService.findByProperty(NegocioVidaProducto.class, "negocioVida.id",config.getId());
		List<NegocioVidaProducto> aQuedar = config.getNegocioVidaProductos();
		aQuedar.removeAll(Collections.singleton(null));

		for (NegocioVidaProducto actual : actuales) {
			filtro = getNegocioVidaProductoPredicate(actual);
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				aBorrar.add(actual);
			}
		}

		for (NegocioVidaProducto elemento : aQuedar) {
			filtro = getNegocioVidaProductoPredicate(elemento);
			if (!CollectionUtils.exists(actuales, filtro)) {
				elemento.setNegocioVida(config);
				aCrear.add(elemento);
			}
		}

		for (NegocioVidaProducto elemento : aCrear) {
			entidadService.save(elemento);
		}

		for (NegocioVidaProducto elemento : aBorrar) {
			entidadService.remove(elemento);
		}
	}

	private void saveRamoConfig(NegocioVida config) {

		Predicate filtro;
		List<NegocioVidaRamo> aBorrar = new ArrayListNullAware<NegocioVidaRamo>();
		List<NegocioVidaRamo> aCrear = new ArrayListNullAware<NegocioVidaRamo>();

		List<NegocioVidaRamo> actuales = entidadService.findByProperty(
				NegocioVidaRamo.class, "negocioVida.id", config.getId());
		List<NegocioVidaRamo> aQuedar = config.getNegocioVidaRamos();
		aQuedar.removeAll(Collections.singleton(null));

		for (NegocioVidaRamo actual : actuales) {
			filtro = getNegocioVidaRamoPredicate(actual);
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				aBorrar.add(actual);
			}
		}

		for (NegocioVidaRamo elemento : aQuedar) {
			filtro = getNegocioVidaRamoPredicate(elemento);
			if (!CollectionUtils.exists(actuales, filtro)) {
				elemento.setNegocioVida(config);
				aCrear.add(elemento);
			}
		}

		for (NegocioVidaRamo elemento : aCrear) {
			entidadService.save(elemento);
		}

		for (NegocioVidaRamo elemento : aBorrar) {
			entidadService.remove(elemento);
		}
	}

	private void saveSubramoConfig(NegocioVida config) {

		Predicate filtro;
		List<NegocioVidaSubRamo> aBorrar = new ArrayListNullAware<NegocioVidaSubRamo>();
		List<NegocioVidaSubRamo> aCrear = new ArrayListNullAware<NegocioVidaSubRamo>();

		List<NegocioVidaSubRamo> actuales = entidadService.findByProperty(NegocioVidaSubRamo.class, "negocioVida.id",config.getId());
		List<NegocioVidaSubRamo> aQuedar = config.getNegocioVidaSubRamos();
		aQuedar.removeAll(Collections.singleton(null));

		for (NegocioVidaSubRamo actual : actuales) {
			filtro = getNegocioVidaSubRamoPredicate(actual);
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				aBorrar.add(actual);
			}
		}

		for (NegocioVidaSubRamo elemento : aQuedar) {
			filtro = getNegocioVidaSubRamoPredicate(elemento);
			if (!CollectionUtils.exists(actuales, filtro)) {
				elemento.setNegocioVida(config);
				aCrear.add(elemento);
			}
		}

		for (NegocioVidaSubRamo elemento : aCrear) {
			entidadService.save(elemento);
		}

		for (NegocioVidaSubRamo elemento : aBorrar) {
			entidadService.remove(elemento);
		}
	}

	private void saveSeccionConfig(NegocioVida config) {

		Predicate filtro;
		List<NegocioVidaSeccion> aBorrar = new ArrayListNullAware<NegocioVidaSeccion>();
		List<NegocioVidaSeccion> aCrear = new ArrayListNullAware<NegocioVidaSeccion>();

		List<NegocioVidaSeccion> actuales = entidadService.findByProperty(NegocioVidaSeccion.class, "negocioVida.id",
				config.getId());
		List<NegocioVidaSeccion> aQuedar = config.getNegocioVidaSeccions();
		aQuedar.removeAll(Collections.singleton(null));

		for (NegocioVidaSeccion actual : actuales) {
			filtro = getNegocioVidaSeccionPredicate(actual);
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				aBorrar.add(actual);
			}
		}

		for (NegocioVidaSeccion elemento : aQuedar) {
			filtro = getNegocioVidaSeccionPredicate(elemento);
			if (!CollectionUtils.exists(actuales, filtro)) {
				elemento.setNegocioVida(config);
				aCrear.add(elemento);
			}
		}

		for (NegocioVidaSeccion elemento : aCrear) {
			entidadService.save(elemento);
		}
		
		for (NegocioVidaSeccion elemento : aBorrar) {
			entidadService.remove(elemento);
		}
	}

	private Predicate getNegocioVidaGerenciaPredicate(final NegocioVidaGerencia filtro) {
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getGerencia().getId().equals(((NegocioVidaGerencia) arg0).getGerencia().getId());				
			}
		};
	}
	
	private void saveAgentesConfig(NegocioVida config) {

		Predicate filtro;
		List<NegocioVidaAgente> aBorrar = new ArrayListNullAware<NegocioVidaAgente>();
		List<NegocioVidaAgente> aCrear = new ArrayListNullAware<NegocioVidaAgente>();

		List<NegocioVidaAgente> actuales = entidadService.findByProperty(NegocioVidaAgente.class,"negocioVida.id", config.getId());
		List<NegocioVidaAgente> aQuedar = config.getNegocioVidaAgentes();
		aQuedar.removeAll(Collections.singleton(null));

		for (NegocioVidaAgente actual : actuales) {
			filtro = getNegocioVidaAgentePredicate(actual);
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				aBorrar.add(actual);
			}
		}

		for (NegocioVidaAgente elemento : aQuedar) {
			filtro = getNegocioVidaAgentePredicate(elemento);
			if (!CollectionUtils.exists(actuales, filtro)) {
				elemento.setNegocioVida(config);
				aCrear.add(elemento);
			}
		}

		for (NegocioVidaAgente elemento : aCrear) {
			entidadService.save(elemento);
		}

		for (NegocioVidaAgente elemento : aBorrar) {
			entidadService.remove(elemento);
		}
	}
	
	private void saveGerenciaConfig(NegocioVida config) {

		Predicate filtro;
		List<NegocioVidaGerencia> aBorrar = new ArrayListNullAware<NegocioVidaGerencia>();
		List<NegocioVidaGerencia> aCrear = new ArrayListNullAware<NegocioVidaGerencia>();

		List<NegocioVidaGerencia> actuales = entidadService.findByProperty(NegocioVidaGerencia.class, "negocioVida.id", config.getId());
		List<NegocioVidaGerencia> aQuedar = config.getNegocioVidaGerencias();
		aQuedar.removeAll(Collections.singleton(null));

		for (NegocioVidaGerencia actual : actuales) {
			filtro = getNegocioVidaGerenciaPredicate(actual);
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				aBorrar.add(actual);
			}
		}

		for (NegocioVidaGerencia elemento : aQuedar) {
			filtro = getNegocioVidaGerenciaPredicate(elemento);
			if (!CollectionUtils.exists(actuales, filtro)) {
				elemento.setNegocioVida(config);
				aCrear.add(elemento);
			}
		}

		for (NegocioVidaGerencia elemento : aCrear) {
			entidadService.save(elemento);
		}

		for (NegocioVidaGerencia elemento : aBorrar) {
			entidadService.remove(elemento);
		}
	}
	
	private Predicate getNegocioVidaProductoPredicate(final NegocioVidaProducto filtro) {
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getProducto().getIdProducto().equals(((NegocioVidaProducto) arg0).getProducto().getIdProducto());
			}
		};
	}
	
	private Predicate getNegocioVidaRamoPredicate(final NegocioVidaRamo filtro) {
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getRamo().getIdRamoContable().equals(((NegocioVidaRamo) arg0).getRamo().getIdRamoContable());
			}
		};
	}


	private Predicate getNegocioVidaSubRamoPredicate(final NegocioVidaSubRamo filtro) {
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getSubRamo().getId().equals(((NegocioVidaSubRamo) arg0).getSubRamo().getId());
			}
		};
		
	}

	private Predicate getNegocioVidaSeccionPredicate(final NegocioVidaSeccion filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				if (arg0 == null) return false;
				return filtro.getSeccion().getIdLinNegocio().equals(((NegocioVidaSeccion) arg0).getSeccion().getIdLinNegocio());
				
			}
		};		
	}
	
	private Predicate getNegocioVidaAgentePredicate(final NegocioVidaAgente filtro) {
			return new Predicate() {		
				@Override
				public boolean evaluate(Object arg0) {
					if (arg0 == null) return false;
					return filtro.getAgenteSeycos().getId().equals(((NegocioVidaAgente) arg0).getAgenteSeycos().getId());
					
				}
			};
			
		}

	@EJB
	private EntidadService entidadService;
	private void onError(String msg) throws Exception {
		throw new Exception(msg);
	}
	@SuppressWarnings("unchecked")
	public List<GenericaAgentesView> getProductoXConfView(Long idconfiguracionVida) throws Exception {
		LOG.info(">> getProductoXConfView() idconfiguracionVida=>{}",idconfiguracionVida);
		List<GenericaAgentesView> listaRamos = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		try {
			if(idconfiguracionVida!=null){
				queryString.append(" SELECT DISTINCT DP.ID_PRODUCTO AS ID, DP.ID_PRODUCTO||'-'||DP.NOM_PRODUCTO AS DESCRIPCION  ");
			    queryString.append(" FROM SEYCOS.DP_PRODUCTO DP ");
			    queryString.append(" INNER JOIN SEYCOS.DP_PRODUCTO_VER DPV ON (DP.ID_PRODUCTO = DPV.ID_PRODUCTO) ");
			    queryString.append(" INNER JOIN MIDAS.CA_TRNEGOCIOVIDA_PRODUCTO NVP on (NVP.PRODUCTO_ID=DP.ID_PRODUCTO ) ");
			    queryString.append(" AND NVP.NEGOCIOVIDA_ID=?1 ");
			    queryString.append(" ORDER BY DP.ID_PRODUCTO ");			    
				Query query = entityManager.createNativeQuery(queryString.toString(),"productoXLineaVentaView");
				query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
				query.setParameter(1,idconfiguracionVida);
				listaRamos = asignarCheksActivos(query.getResultList());
				LOG.info("getProductoXConfView>>>>>>>"+listaRamos.size());
				return listaRamos;
			}
			return null;
		} catch (RuntimeException re) {
			LOG.error("--error getProductoXConfView()", re);
			throw re;
		}
	}	
	
	@SuppressWarnings("unchecked")
	public List<GenericaAgentesView> getRamosXConfView(Long idconfiguracionVida) throws Exception {
		LOG.info(">> getRamosXConfView() idconfiguracionVida=>{}",idconfiguracionVida);
		List<GenericaAgentesView> listaRamos = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		try {
			if(idconfiguracionVida!=null){
				queryString.append(" SELECT IR.ID_RAMO_CONTABLE AS ID, IR.ID_RAMO_CONTABLE||'-'||IR.NOM_RAMO AS DESCRIPCION "); 
				queryString.append(" FROM SEYCOS.ING_RAMO IR  "); 
			    queryString.append(" INNER JOIN  midas.CA_TRNEGOCIOVIDA_RAMO NV on(nv.RAMO_ID=IR.ID_RAMO_CONTABLE) ");
			    queryString.append(" AND NV.NEGOCIOVIDA_ID=?1 ");
			    queryString.append(" ORDER BY IR.ID_RAMO_CONTABLE ");			    
				Query query = entityManager.createNativeQuery(queryString.toString(),"productoXLineaVentaView");
				query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
				query.setParameter(1,idconfiguracionVida);
				listaRamos = asignarCheksActivos(query.getResultList());
				LOG.info("getRamosXConfView>>>>>>>"+listaRamos.size());
				return listaRamos;
			}
			return null;
		} catch (RuntimeException re) {
			LOG.error("--error getProductoXConfView()", re);
			throw re;
		}
	}	
	
	@SuppressWarnings("unchecked")
	public List<GenericaAgentesView> getSubRamosXConfView(Long idconfiguracionVida) throws Exception {
		LOG.info(">> getSubRamosXConfView() idconfiguracionVida=>{}",idconfiguracionVida);
		List<GenericaAgentesView> lista = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		try {
			if(idconfiguracionVida!=null){
				queryString.append(" SELECT ISR.ID_RAMO_CONTABLE AS ID, ISR.ID_RAMO_CONTABLE||'-'||ISR.NOM_SUBRAMO AS DESCRIPCION "); 
				queryString.append(" FROM MIDAS.CA_TRNEGOCIOVIDA_SUBRAMO NV "); 
			    queryString.append(" INNER JOIN SEYCOS.ING_RAMO_SUBRAMO ISR on(NV.ID_SUBR_CONTABLE=ISR.ID_SUBR_CONTABLE and NV.ID_RAMO_CONTABLE=ISR.ID_RAMO_CONTABLE) ");
			    queryString.append(" AND NV.NEGOCIOVIDA_ID=?1  ");
			    queryString.append(" ORDER BY ISR.ID_RAMO_CONTABLE ");			    
				Query query = entityManager.createNativeQuery(queryString.toString(),"productoXLineaVentaView");
				query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
				query.setParameter(1,idconfiguracionVida);
				lista = asignarCheksActivos(query.getResultList());
				LOG.info("getSubRamosXConfView>>>>>>>"+lista.size());
				return lista;
			}
			return null;
		} catch (RuntimeException re) {
			LOG.error("--error getSubRamosXConfView()", re);
			throw re;
		}
	}	
	
	@SuppressWarnings("unchecked")
	public List<GenericaAgentesView> getLineasNegXConfView(Long idconfiguracionVida) throws Exception {
		LOG.info(">> getProductoXConfView() idconfiguracionVida=>{}",idconfiguracionVida);
		List<GenericaAgentesView> lista = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		try {
			if(idconfiguracionVida!=null){
				queryString.append(" SELECT DPL.ID_LIN_NEGOCIO AS ID,DPL.ID_LIN_NEGOCIO||'-'||DPL.NOM_LINEA_NEG AS DESCRIPCION "); 
				queryString.append(" FROM SEYCOS.DP_PRODUCTO DP  "); 
				queryString.append(" INNER JOIN SEYCOS.DP_PRODUCTO_VER DPV ON (DP.ID_PRODUCTO = DPV.ID_PRODUCTO AND DPV.F_TER_REG > SYSDATE) ");			
				queryString.append(" INNER JOIN SEYCOS.DP_PR_LIN DPPL ON (DPV.ID_PRODUCTO = DPPL.ID_PRODUCTO AND DPV.ID_VERSION = DPPL.ID_VERSION)");
				queryString.append(" INNER JOIN SEYCOS.DP_LINEA_NEG DPL ON DPPL.ID_LIN_NEGOCIO = DPL.ID_LIN_NEGOCIO ");	
			    queryString.append(" INNER JOIN  midas.CA_TRNEGOCIOVIDA_SECCION NV on(nv.SECCION_ID=DPL.ID_LIN_NEGOCIO) ");
			    queryString.append(" AND NV.NEGOCIOVIDA_ID=?1 ");
			    queryString.append(" ORDER BY DP.ID_PRODUCTO , DPL.ID_LIN_NEGOCIO ");		    
				Query query = entityManager.createNativeQuery(queryString.toString(),"productoXLineaVentaView");
				query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
				query.setParameter(1,idconfiguracionVida);
				lista = asignarCheksActivos(query.getResultList());
				LOG.info("getProductoXConfView>>>>>>>"+lista.size());
				return lista;
			}
			return null;
		} catch (RuntimeException re) {
			LOG.error("--error getProductoXConfView()", re);
			throw re;
		}
	}	
	
	@SuppressWarnings("unchecked")
	public List<GenericaAgentesView> getGerenciaXConfView(Long idconfiguracionVida) throws Exception {
		LOG.info(">> getGerenciaXConfView() idconfiguracionVida=>{}",idconfiguracionVida);
		List<GenericaAgentesView> lista = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		try {
			if(idconfiguracionVida!=null){
				queryString.append(" SELECT GR.ID_GERENCIA AS ID, GR.ID_GERENCIA||'-'||GR.NOM_GERENCIA AS DESCRIPCION ");
				queryString.append(" FROM SEYCOS.GERENCIA  GR ");
			    queryString.append(" INNER JOIN MIDAS.CA_TRNEGOCIOVIDA_GERENCIA NV ON(NV.ID_GERENCIA = GR.ID_GERENCIA AND GR.SIT_GERENCIA = 'A' AND GR.B_ULT_MOD = 'V') ");
			    queryString.append(" AND NV.NEGOCIOVIDA_ID = ?1 ");
			    queryString.append(" ORDER BY GR.ID_GERENCIA ");			    
				Query query = entityManager.createNativeQuery(queryString.toString(),"productoXLineaVentaView");
				query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
				query.setParameter(1,idconfiguracionVida);
				lista = asignarCheksActivos(query.getResultList());
				LOG.info("getGerenciaXConfView>>>>>>>"+lista.size());
				return lista;
			}
			return null;
		} catch (RuntimeException re) {
			LOG.error("--error getGerenciaXConfView()", re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public List<GenericaAgentesView> getAgenteXConfView(Long idconfiguracionVida) throws Exception {
		LOG.info(">> getGerenciaXConfView() idconfiguracionVida=>{}",idconfiguracionVida);
		List<GenericaAgentesView> lista = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		try {
			if(idconfiguracionVida!=null){
				queryString.append(" SELECT AGT.ID_AGENTE AS ID,  PER.NOMBRE AS DESCRIPCION ");
				queryString.append(" FROM SEYCOS.AGENTE AGT ");
				queryString.append(" INNER JOIN SEYCOS.PERSONA PER ON (AGT.ID_PERSONA = PER.ID_PERSONA) ");
				queryString.append(" INNER JOIN MIDAS.CA_TRNEGOCIOVIDA_AGENTE NV ON (AGT.ID_AGENTE = NV.ID_AGENTE) ");
				queryString.append(" WHERE AGT.SIT_AGENTE = 'A' ");
			    queryString.append(" AND AGT.B_ULT_MOD = 'V' ");
				queryString.append(" AND NV.NEGOCIOVIDA_ID = ?1 ");			    
				Query query = entityManager.createNativeQuery(queryString.toString(),"productoXLineaVentaView");
				query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
				query.setParameter(1,idconfiguracionVida);
				lista = asignarCheksActivos(query.getResultList());
				LOG.info("getGerenciaXConfView>>>>>>>"+lista.size());
				return lista;
			}
			return null;
		} catch (RuntimeException re) {
			LOG.error("--error getGerenciaXConfView()", re);
			throw re;
		}
	}
	
	public List<GenericaAgentesView> asignarCheksActivos(List<GenericaAgentesView> lista){
		List<GenericaAgentesView> listaResult=new ArrayList<GenericaAgentesView> ();
		for(GenericaAgentesView object:lista){
			object.setChecado(1);
			listaResult.add(object);
		}
		return listaResult;
	}
	
	/**
	 * 
	 * @param listCompensaciones
	 * @param compensacionesDTO
	 */
	public void fechaModificacionVida(List<CaCompensacion> listCompensaciones, CompensacionesDTO compensacionesDTO){
		LOG.info(">> fechaModificacionVida()");
		
		for(CaCompensacion ca: listCompensaciones){
			if(ca.getContraprestacion()){
				compensacionesDTO.setEstatusCompensacionId(ca.getCaEstatus().getId() != null ? ca.getCaEstatus().getId().intValue() : 0);
			}else{
				compensacionesDTO.setEstatusContraprestacionId(ca.getCaEstatus().getId() != null ? ca.getCaEstatus().getId().intValue() : 0);
			}
		}
		
	}
	
}
