package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.fuerzaventa.ClienteAgenteDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HerenciaClientes;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteCarteraClientesView;
import mx.com.afirme.midas2.dto.fuerzaventa.HerenciaClientesView;
import mx.com.afirme.midas2.service.fuerzaventa.ClienteAgenteService;
import mx.com.afirme.midas2.service.impl.catalogos.EntidadHistoricoServiceImpl;
@Stateless
//@TransactionManagement(TransactionManagementType.CONTAINER)
public class ClienteAgenteServiceImpl extends EntidadHistoricoServiceImpl implements ClienteAgenteService{
	private ClienteAgenteDao clienteAgenteDao;	

	@EJB
	public void setClienteAgenteDao(ClienteAgenteDao clienteAgenteDao) {
		this.clienteAgenteDao = clienteAgenteDao;
	}

	/**
	 * Metodo para encontrar todos los Clientes relacionados con a un Agente, regresa una lista de la relacion ClienteAgentes.
	 * @param ClienteAgente debe tener asignado el idAgente en su objeto agente. 
	 * @return List<ClienteAgente> 
	 */
	@Override
	public List<ClienteAgente> findByFilters(ClienteAgente clienteAgente) throws Exception{
		return clienteAgenteDao.findByFilters(clienteAgente);
	}

	@Override
	public ClienteAgente loadById(ClienteAgente clienteAgente) throws Exception{
		return clienteAgenteDao.loadById(clienteAgente);
	}

	@Override
	public ClienteAgente saveFull(ClienteAgente clienteAgente) throws Exception {
		return clienteAgenteDao.saveFull(clienteAgente);
	}

	@Override
	public void unsubscribe(ClienteAgente clienteAgente) throws Exception {
		clienteAgenteDao.unsubscribe(clienteAgente);
	}
	
	@Override
	public ClienteAgente loadByIdCheckAgent(ClienteAgente clienteAgente){
		return clienteAgenteDao.loadByIdCheckAgent(clienteAgente);
	}

	@Override
	public void cancelarHerenciaCliente(ClienteAgente clienteAgente, Agente agenteAnterior) throws Exception{
		clienteAgenteDao.cancelarHerenciaCliente(clienteAgente, agenteAnterior);
	}

	@Override
	public void cancelarHerenciaClientes(List<ClienteAgente> clientesAgente, Agente agenteAnterior,HerenciaClientes herencia) throws Exception {
		clienteAgenteDao.cancelarHerenciaClientes(clientesAgente,agenteAnterior,herencia);
	}

	@Override
	public void heredarClientes(List<ClienteAgente> clientesAgente,Agente agenteDestino,HerenciaClientes herencia,String accion) throws Exception{
		clienteAgenteDao.heredarClientes(clientesAgente,agenteDestino,herencia,accion);
	}

	@Override
	public void heredarClientes(ClienteAgente clienteAgente, Agente agenteDestino,String accion) throws Exception{
		clienteAgenteDao.heredarClientes(clienteAgente,agenteDestino,accion);
	}
	@Override
	public void asignarCliente(ClienteAgente clienteHeredar,Agente agenteDestino,String accion) throws Exception{
		clienteAgenteDao.asignarCliente(clienteHeredar,agenteDestino,accion);
	}
	@Override
	public void desAsignarCliente(ClienteAgente clienteAgente) throws RuntimeException {
		clienteAgenteDao.desAsignarCliente(clienteAgente);
	}
	
	@Override
	public List<ClienteAgente> listarClientesAHeredar(Agente agenteAnterior, Agente agenteNuevo)throws Exception {
		return clienteAgenteDao.listarClientesAHeredar(agenteAnterior, agenteNuevo);
	}

	@Override
	public List<ClienteAgente> listarClientesAgente(Agente agente) throws Exception {
		return clienteAgenteDao.listarClientesAgente(agente);
	}
	
	@Override
	public HerenciaClientes saveHerenciaCliente(HerenciaClientes herencia) throws Exception {
		return clienteAgenteDao.saveHerenciaCliente(herencia);
	}
	
	@Override
	public List<HerenciaClientes> findHerenciasByFilters(HerenciaClientes filtro,String isAgente) throws Exception{
		return clienteAgenteDao.findHerenciasByFilters(filtro,isAgente);
	}

	@Override
	public String buscarAsociacionClienteAgente(ClienteAgente clienteAgente, Long idAgente)
			throws Exception {		
		return clienteAgenteDao.buscarAsociacionClienteAgente(clienteAgente, idAgente);
	}
	
	@Override
	public List<HerenciaClientesView> findByFiltersView(HerenciaClientes filtro,String isAgente) throws Exception {
		return clienteAgenteDao.findByFiltersView(filtro, isAgente);
	}
	
	@Override
	public HerenciaClientes obtenerHerenciaPorId(HerenciaClientes herencia) throws Exception {
		return clienteAgenteDao.obtenerHerenciaPorId(herencia);
	}
	
	@Override
	public List<ClienteAgente> consultaDetalleHerenciaClientes(Long herenciaId,Long agenteId) throws Exception {
		return clienteAgenteDao.consultaDetalleHerenciaClientes(herenciaId, agenteId);
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, String... arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void herenciaClientes(ClienteAgente clienteAgente,Agente agenteDestino,String accion)throws Exception {
		clienteAgenteDao.herenciaClientes(clienteAgente, agenteDestino, accion);
	}

	@Override
	public void herenciaClientes(List<ClienteAgente> clientes, Agente agenteDestino,HerenciaClientes herencia,String accion) throws Exception {
		clienteAgenteDao.herenciaClientes(clientes, agenteDestino, herencia, accion);		
	}
	
	@Override
	public List<ClienteAgente> findByFilters(ClienteAgente clienteAgente, String fechaHistorico) throws Exception{
		return clienteAgenteDao.findByFilters(clienteAgente, fechaHistorico);
	}
	
	@Override
	public void generarEndososCambioAgente(ClienteAgente clienteAgenteActual, Agente agenteDestino)
	{		
		clienteAgenteDao.generarEndososCambioAgente(clienteAgenteActual, agenteDestino);			
	}
	
	@Override
	public List<AgenteCarteraClientesView> findByFiltersCarteraClientesGrid(ClienteAgente clienteAgente, String historico) throws Exception {
		return clienteAgenteDao.findByFiltersCarteraClientesGrid(clienteAgente, historico);
	}	
}
