package mx.com.afirme.midas2.service.impl.bitemporal.informacionVehicular;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoEquivalenciaDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.cesvi.vinplus.Respuesta;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.poliza.diferenciasamis.DiferenciasAmis;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.informacionVehicular.InformacionVehicularBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.datosAmis.DatosAmisService;
import mx.com.afirme.midas2.service.diferenciasAmis.DiferenciasAmisService;
import mx.com.afirme.midas2.service.impl.informacionVehicular.InformacionVehicularServiceImpl;
import mx.com.afirme.midas2.service.informacionVehicular.InformacionVehicularService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

@Stateless
public class InformacionVehicularBitemporalServiceImpl implements InformacionVehicularBitemporalService {
	
	private static final Logger LOG = Logger.getLogger(InformacionVehicularBitemporalServiceImpl.class);
	@EJB
	private EstiloVehiculoFacadeRemote estiloVehiculoFacadeRemote;
	
	@EJB
	private DiferenciasAmisService diferenciasAmisService;

	@EJB
	private InformacionVehicularService informacionVehicularService;	
	
	@EJB
	private EntidadBitemporalService entidadBitemporalService;

	@EJB
	private DatosAmisService datosAmisService;
	
	private UsuarioService usuarioService;
	
	@EJB(beanName="UsuarioServiceDelegate")
    public void setUsuarioService(UsuarioService usuarioService) {
          this.usuarioService = usuarioService;
    }
	
	@EJB
	private EntidadContinuityDao entidadContinuityDao;
	
	@EJB
	protected EntidadService entidadService;
	
	private Map<String, String> validaInformacionVehicular(BitemporalCotizacion cotizacion, DateTime validoEn, PolizaDTO polizaDTO) {
		Map<String, String> mensajeInformacionVehicular = new HashMap<String, String>();

		Usuario usuario = usuarioService.getUsuarioActual();
		String icono = InformacionVehicularServiceImpl.EXITO;
		String mensaje = "";
		
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("continuity.cotizacionContinuity.id", cotizacion.getContinuity().getId());                          
        List<BitemporalInciso> incisos = (List<BitemporalInciso>) entidadContinuityDao.listarBitemporalsFiltrado(
        		BitemporalInciso.class, params, validoEn, null, false);

		for (BitemporalInciso inciso : incisos) {
			Map<String, Object> paramsAut = new HashMap<String, Object>();
			paramsAut.put("continuity.id", inciso.getContinuity().getAutoIncisoContinuity().getId());
			List<BitemporalAutoInciso> autos = (List<BitemporalAutoInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalAutoInciso.class, 
					paramsAut, validoEn, null, false);
			
			BitemporalAutoInciso auto = null;
			if(autos != null && !autos.isEmpty()){
				auto = autos.get(0);
			}
			
			if(auto != null) {
				NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, 
						new BigDecimal(auto.getValue().getNegocioSeccionId()));

				if (SeccionDTO.TIPO_VALIDACION_CESVI.compareTo(negocioSeccion.getSeccionDTO().getTipoValidacionNumSerie()) == 0 &&
						NegocioSeccion.PERMITE_CONSULTA_NUM_SERIE.compareTo(negocioSeccion.getConsultaNumSerie()) == 0) {
					LinkedList<DiferenciasAmis> listaDiferencias = new LinkedList<DiferenciasAmis>();
					List<Respuesta> vehiculosCesvi = informacionVehicularService.obtenerVin(auto.getValue().getNumeroSerie());
					if(vehiculosCesvi != null && vehiculosCesvi.size() != 0) {
						Map<Long, String> datosAmis = datosAmisService.getListarDatos();
						listaDiferencias= this.getDiferenciasInformacionVehicular(vehiculosCesvi, auto, polizaDTO, validoEn, datosAmis);
						boolean vinCorrecto = false;
						if(listaDiferencias != null && listaDiferencias.size() > 0) {
							//Guardar diferencias entre el inciso y cada uno de los vehiculos de cesvi
							for (DiferenciasAmis diferencias :listaDiferencias) {
								if(diferencias.getClaveError() != 0) {
									if(usuario != null && usuario.getNombre() != null) {
										diferencias.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
									}
									diferenciasAmisService.guardarDiferenciasAmis(diferencias);
								} else {
									vinCorrecto = true;
									break;
								}
							}
							
							if(!vinCorrecto){
								Collections.sort(listaDiferencias, new Comparator<DiferenciasAmis>() {
									@Override
									public int compare(DiferenciasAmis o1, DiferenciasAmis o2) {
										return o1.getNumeroDiferencias().compareTo(o2.getNumeroDiferencias());
									}
								});
								
								if(listaDiferencias.getFirst().getMensajeError() != null) {
									mensaje = listaDiferencias.getFirst().getMensajeError();
								}
								
								//Obtener mensaje con diferencias entre el inciso y el vehiculo mas parecido de cesvi
								if(listaDiferencias.getFirst().getClaveError() == 1 && !mensaje.isEmpty()) {
									mensaje = informacionVehicularService.getMensaje(1, auto.getValue().getNumeroSerie(), " [" + listaDiferencias.getFirst().getMensajeError() + "].");
								}
							}
						}
					} else {
						//Si no se encontro informacion del numero de serie, guardar error
						mensaje = informacionVehicularService.getMensaje(DiferenciasAmis.ERROR_VIN_NO_ENCONTRADO, auto.getValue().getNumeroSerie(), "");
						DiferenciasAmis diferenciasAmis = new DiferenciasAmis();
						diferenciasAmis = this.poblarDiferencias(diferenciasAmis, auto, polizaDTO);
						diferenciasAmis.setClaveError(DiferenciasAmis.ERROR_VIN_NO_ENCONTRADO);
						diferenciasAmis.setMensajeError(mensaje);
						if(usuario != null && usuario.getNombre() != null) {
							diferenciasAmis.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
						}
						diferenciasAmisService.guardarDiferenciasAmis(diferenciasAmis);
					}
					
					//Guardar mensaje de error para el inciso
					if(mensaje != null && !mensaje.isEmpty()) {
						icono = InformacionVehicularServiceImpl.ERROR;
						auto.getValue().setObservacionesSesa(mensaje);
						
						Map<String, Object> parametros = new LinkedHashMap<String, Object>();
						parametros.put("observacionesSesa", auto.getValue().getObservacionesSesa());
						entidadContinuityDao.updateBitemporal(BitemporalAutoInciso.class, parametros, auto.getId());
					}
				}
			}
		}
		
		mensajeInformacionVehicular.put("mensaje" ,mensaje);
		mensajeInformacionVehicular.put("icono", icono);
		
		return mensajeInformacionVehicular;
	}

	private LinkedList<DiferenciasAmis> getDiferenciasInformacionVehicular (List<Respuesta> vehiculosCesvi, BitemporalAutoInciso auto, PolizaDTO polizaDTO, DateTime validoEn, Map<Long, String> datosAmis) {
		LinkedList<DiferenciasAmis> listaDiferencias = new LinkedList<DiferenciasAmis>();

		//Determinar numero de diferencias entre el inciso y los vehiculos retornados por cesvi
		try {
			StringBuilder mensajeError = new StringBuilder("");
			for (Respuesta informacionVehicular : vehiculosCesvi) {
				Integer numeroDiferencias = 0;
				mensajeError.delete(0,mensajeError.length());
				DiferenciasAmis diferenciasAmis = new DiferenciasAmis();

				//Si cesvi retorna un resultado diferente de 1 entonces es error del numero de serie
				if(!DiferenciasAmis.VIN_SIN_ERRORES.equals(informacionVehicular.getRESULTADO())) {
					diferenciasAmis = new DiferenciasAmis();
					listaDiferencias = new LinkedList<DiferenciasAmis>();
					diferenciasAmis = this.poblarDiferencias(diferenciasAmis, auto, polizaDTO);
					diferenciasAmis.setNumeroDiferencias(0);
					diferenciasAmis.setClaveError(Integer.parseInt(informacionVehicular.getRESULTADO()));
					diferenciasAmis.setMensajeError(informacionVehicularService.getMensaje(Integer.parseInt(informacionVehicular.getRESULTADO()), diferenciasAmis.getNumeroSerie(), ""));
					listaDiferencias.add(diferenciasAmis);
					break;
				} else {
					//Si se encontro informacion del numero de serie, compararla con el vehiculo emitido
					String[] estiloId = null;
					estiloId = auto.getValue().getEstiloId().split("_");
					EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(estiloId[0], estiloId[1],
								BigDecimal.valueOf(Long.valueOf(estiloId[2]))); 
					EstiloVehiculoDTO estilo = estiloVehiculoFacadeRemote.findById(estiloVehiculoId);
					
					if (estilo != null) {
						if (datosAmis.containsValue("MARCA") && !informacionVehicular.getMARCA().equalsIgnoreCase(estilo.getMarcaVehiculoDTO().getDescripcionMarcaVehiculo() != null ?  estilo.getMarcaVehiculoDTO().getDescripcionMarcaVehiculo() : "")) {
							boolean marcaCorrecta = false;
							List<MarcaVehiculoEquivalenciaDTO> marcaVehiculoEquivalenciaDTOs = entidadService.findByProperty(MarcaVehiculoEquivalenciaDTO.class, 
									"codigoMarcaVehiculo", estilo.getMarcaVehiculoDTO().getCodigoMarcaVehiculo() != null ? estilo.getMarcaVehiculoDTO().getCodigoMarcaVehiculo() : "");
							for (MarcaVehiculoEquivalenciaDTO marcaVehiculoEquivalenciaDTO : marcaVehiculoEquivalenciaDTOs) {
								if (informacionVehicular.getMARCA().equalsIgnoreCase(marcaVehiculoEquivalenciaDTO.getDescripcionMarcaVehiculo())) {
									marcaCorrecta = true;
									break;
								}
							}
							
							if (!marcaCorrecta) {
								diferenciasAmis.setMarca(informacionVehicular.getMARCA() != null ? informacionVehicular.getMARCA() : "");
								numeroDiferencias ++;
								mensajeError.append(mensajeError.toString().isEmpty() ? "Marca" : ", Marca");
							}
						}

						if(datosAmis.containsValue("PUERTAS") && !informacionVehicular.getPUERTAS().replaceAll(" PUERTAS", "").equalsIgnoreCase(estilo.getNumeroPuertas() != null ? estilo.getNumeroPuertas().toString() : "")) {
							diferenciasAmis.setPuertas(informacionVehicular.getPUERTAS() != null ? informacionVehicular.getPUERTAS() : "");
							numeroDiferencias ++;
							mensajeError.append(mensajeError.toString().isEmpty() ? "Numero de puertas" : ", Numero de puertas");
						}

						if(datosAmis.containsValue("CILINDROS") && !informacionVehicular.getNO_CILINDRO().equalsIgnoreCase(informacionVehicularService.getModificadoresDescripcion(DiferenciasAmis.GRUPO_CILINDROS, estilo.getClaveCilindros()))) {
							diferenciasAmis.setCilindros(informacionVehicular.getNO_CILINDRO() != null ? informacionVehicular.getNO_CILINDRO() : "");
							numeroDiferencias ++;
							mensajeError.append(mensajeError.toString().isEmpty() ? "Cilindros" : ", Cilindros");
						}
							
						if(datosAmis.containsValue("COMBUSTIBLE") && !informacionVehicular.getCOMBUSTIBLE().equalsIgnoreCase(informacionVehicularService.getModificadoresDescripcion(DiferenciasAmis.GRUPO_COMBUSTIBLE, estilo.getClaveTipoCombustible()))) {
							diferenciasAmis.setCombustible(informacionVehicular.getCOMBUSTIBLE() != null ? informacionVehicular.getCOMBUSTIBLE() : "");
							numeroDiferencias ++;
							mensajeError.append(mensajeError.toString().isEmpty() ? "Combustible" : ", Combustible");
						}
					
						if(datosAmis.containsValue("INTERIORES") && !informacionVehicular.getINTERIORES().equalsIgnoreCase(informacionVehicularService.getModificadoresDescripcion(DiferenciasAmis.GRUPO_VESTIDURAS, estilo.getClaveVestidura()))) {
							diferenciasAmis.setInteriores(informacionVehicular.getINTERIORES() != null ? informacionVehicular.getINTERIORES() : "");
							numeroDiferencias ++;
							mensajeError.append(mensajeError.toString().isEmpty() ? "Interiores" : ", Interiores");
						}
							
						if(datosAmis.containsValue("PASAJEROS") && !informacionVehicular.getPASAJEROS().equalsIgnoreCase(estilo.getNumeroAsientos() != null ? estilo.getNumeroAsientos().toString() : "")) {
							diferenciasAmis.setPasajeros(informacionVehicular.getPASAJEROS() != null ? informacionVehicular.getPASAJEROS() : "");
							numeroDiferencias ++;
							mensajeError.append(mensajeError.toString().isEmpty() ? "Número de pasajeros" : ", Número de pasajeros");
						}
							
					}
					
					if(datosAmis.containsValue("ANO") && !informacionVehicular.getANO().equalsIgnoreCase(auto.getValue().getModeloVehiculo() != null ? auto.getValue().getModeloVehiculo().toString() : "")) {
						diferenciasAmis.setAnos(informacionVehicular.getANO() != null ? informacionVehicular.getANO() : "");
						numeroDiferencias ++;
						mensajeError.append(mensajeError.toString().isEmpty() ? "Modelo" : ", Modelo");
					}

					if(numeroDiferencias == 0) {
						//Si se encontro que la informacion retornada por cesvi coincide exactamente con el vehiculo emitido entonces se guardan sus claves
						if(datosAmis != null && datosAmis.size() > 0) {
							auto.getValue().setClaveAmis(informacionVehicular.getCVE_AMIS());
							auto.getValue().setClaveSesa(informacionVehicular.getCVE_SESA());
							entidadBitemporalService.saveValid(auto, validoEn);
						}
						
						diferenciasAmis = new DiferenciasAmis();
						listaDiferencias = new LinkedList<DiferenciasAmis>();
						diferenciasAmis.setClaveError(0);
						diferenciasAmis.setNumeroDiferencias(0);
						listaDiferencias.add(diferenciasAmis);
						break;
					} else {
						//Si se encontraron diferencias entre la informacion del numero de serie y el vehiculo emitido, guardar error
						diferenciasAmis = this.poblarDiferencias(diferenciasAmis, auto, polizaDTO);
						diferenciasAmis.setClaveUnica(informacionVehicular.getCLAVE_UNICA() != null ? informacionVehicular.getCLAVE_UNICA() : "");
						diferenciasAmis.setVersion(informacionVehicular.getVERSION_C() != null ? informacionVehicular.getVERSION_C() : "");
						diferenciasAmis.setNumeroDiferencias(numeroDiferencias);
						diferenciasAmis.setClaveError(DiferenciasAmis.ERROR_VIN_INFORMACION_NO_COINCIDE);
						diferenciasAmis.setMensajeError(mensajeError.toString());
						listaDiferencias.add(diferenciasAmis);
					}
				}
			}	
		} catch (Exception e) {
			LOG.error("Informacion Vehicular Individual getDiferenciasInformacionVehicular:", e);
		}

		return listaDiferencias;
	}

	private DiferenciasAmis poblarDiferencias (DiferenciasAmis diferenciasAmis, BitemporalAutoInciso auto, PolizaDTO polizaDTO) {
		BigDecimal idToCotizacion = polizaDTO.getCotizacionDTO().getIdToCotizacion();
		Integer numeroInciso = auto.getContinuity().getIncisoContinuity().getNumero();

		diferenciasAmis.setIdToPoliza(polizaDTO.getIdToPoliza());
		diferenciasAmis.setIdToCotizacion(idToCotizacion);
		diferenciasAmis.setNumeroInciso(new BigDecimal(numeroInciso));
		diferenciasAmis.setNumeroSerie(auto.getValue().getNumeroSerie() != null ? auto.getValue().getNumeroSerie() : "");
		diferenciasAmis.setFechaCreacion(new Date());
		diferenciasAmis.setEstatus(DiferenciasAmis.ESTATUS_PENDIENTE_REVISION);
		diferenciasAmis.setActivo(DiferenciasAmis.ACTIVO);
		return diferenciasAmis;
	}
	
	
	public void validaInfoVehicularByEstiloPolInd(BitemporalCotizacion cotizacionBitemporal, DateTime validoEn, 
			BigDecimal claveTipoEndoso, PolizaDTO polizaDTO) throws Exception {
		try {
			switch (claveTipoEndoso.shortValue()) {
			case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS:
				diferenciasAmisService.eliminarDiferenciasAmis(polizaDTO.getIdToPoliza());
				if (informacionVehicularService.validacionActiva(DiferenciasAmis.VALIDACION_VIN_INDIVIDUAL_ACTIVADA)) {
					this.validaInformacionVehicular(cotizacionBitemporal, validoEn, polizaDTO);
				}
				break;
			case SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA:
				diferenciasAmisService.cambiarEstatus(polizaDTO.getIdToPoliza(), DiferenciasAmis.ESTATUS_PENDIENTE_REVISION);
				break;
			case SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA:
				diferenciasAmisService.cambiarEstatus(polizaDTO.getIdToPoliza(), DiferenciasAmis.ESTATUS_CANCELADA);
				break;
			default:
				break;
			}
		} catch(Exception e){
			throw e;
		}
	}
}
