package mx.com.afirme.midas2.service.impl.enlace;

import static mx.com.afirme.midas.sistema.Utilerias.convertToUTF8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.enlace.CaseDao;
import mx.com.afirme.midas2.dao.enlace.CaseDeviationDao;
import mx.com.afirme.midas2.dto.enlace.CaseConversationDTO;
import mx.com.afirme.midas2.dto.enlace.CaseDTO;
import mx.com.afirme.midas2.dto.enlace.CaseDTO.StatusEnum;
import mx.com.afirme.midas2.dto.enlace.CaseDeviationDTO;
import mx.com.afirme.midas2.dto.enlace.CaseHistDTO;
import mx.com.afirme.midas2.dto.enlace.CaseImportantUserDTO;
import mx.com.afirme.midas2.dto.enlace.CaseManagerUserDTO;
import mx.com.afirme.midas2.dto.enlace.CaseTypeDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.enlace.CaseService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.apn.SendAPNs;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Sender;

@Stateless
public class CaseServiceImpl implements CaseService {
	
	private final Logger LOG = Logger.getLogger(CaseServiceImpl.class);
	
	@EJB
	protected EntidadService entidadService;
	@PersistenceContext
	protected EntityManager entityManager;
	@EJB
	protected CaseDao caseDao;
	@EJB
	protected EntidadDao entidadDao;
	@EJB
	protected CaseDeviationDao caseDeviationDao;
	@EJB(beanName = "UsuarioServiceDelegate")
	protected UsuarioService usuarioService;
	@EJB
	protected SistemaContext sistemaContext;
	@EJB
	protected MailService mailService;

	/**
	 * Obtiene una lista de los casos activos por usuario, es decir los casos
	 * que tengan estatus de CaseDTO.StatusEnum.NEW o
	 * CaseDTO.StatusEnum.ASSIGNED
	 * 
	 * @param createUser
	 * @return
	 */
	public List<CaseDTO> getActiveCaseByUser(String createUser) {
		Usuario usuario = usuarioService.getUsuarioActual();
		final List<CaseDTO> result;
		if (usuarioService.tieneRol("Rol_Enlace", usuario)) {
			result = caseDao.getByStatus(null, createUser,
					CaseDTO.StatusEnum.NEW.getId(),
					CaseDTO.StatusEnum.ASSIGNED.getId());
		} else {
			result = caseDao.getByStatus(createUser, null,
					CaseDTO.StatusEnum.NEW.getId(),
					CaseDTO.StatusEnum.ASSIGNED.getId());
		}
		if(CollectionUtils.isNotEmpty(result)) {
			for(CaseDTO caso : result) {
				caso.setCreateUserName(usuarioService
						.buscarUsuarioPorNombreUsuario(caso.getCreateUser())
						.getNombreCompleto());
				caso.setAssignedUserName(usuarioService
						.buscarUsuarioPorNombreUsuario(caso.getAssignedUser())
						.getNombreCompleto());
			}
		}
		return result;
	}

	/**
	 * Obtiene el caso por su identificador
	 * 
	 * @param caseId
	 * @return
	 */
	public CaseDTO findById(Long caseId) {
		return caseDao.findById(CaseDTO.class, caseId);
	}

	/**
	 * Crea un nuevo caso
	 * 
	 * @param caseTypeId
	 *            Tipo de caso
	 * @param severity
	 *            Severidad
	 * @param description
	 *            Descripcion
	 * @param createUser
	 *            Usuario
	 * @return
	 */
	public Long newCase(Long caseTypeId, Integer severity, String description, 
			String createUser) {
		final CaseDTO caso = new CaseDTO();
		caso.setCaseTypeDTO(entidadDao.findById(CaseTypeDTO.class, caseTypeId));
		caso.setCaseSeverity(severity);
		caso.setCreateDate(new Date());
		caso.setCreateUser(createUser);
		caso.setDescription(convertToUTF8(description));
		caso.setStatus(CaseDTO.StatusEnum.NEW.getId());
		caseDao.persist(caso);
		final Usuario assignedUser = assign(caso.getId());
		// El primer mensaje es el del usuario
		addMessage(caso.getId(), description, createUser);
		// El segundo lo manda el sistema
		addMessage(
				caso.getId(),
				String.format(
						"En breve nos pondremos en contacto, se te ha asignado el caso #%s, asignado a %s.",
						StringUtils.leftPad(caso.getId().toString(), 5, '0'), assignedUser.getNombreCompleto()), "SISTEMA");
		
		return caso.getId();
	}

	/**
	 * Asigna un caso para su atención dependiendo de su severidad
	 * 
	 * @param caseId
	 */
	public Usuario assign(Long caseId) {
		final CaseDTO caso = caseDao.findById(CaseDTO.class, caseId);
		final boolean isVip = getVip(caso.getCreateUser());
		if (isVip && caso.getCaseSeverity() == CaseDTO.Severity.HIGHT.getId()) {
			caso.setAssignedUser(getAssigned(CaseDTO.UserAssignedType.DIRECTOR.getId(), caso.getCaseTypeDTO().getId()));
		} else if (isVip && caso.getCaseSeverity() == CaseDTO.Severity.NORMAL.getId()) {
			caso.setAssignedUser(getAssigned(CaseDTO.UserAssignedType.MANAGER.getId(), caso.getCaseTypeDTO().getId()));
		} else {
			caso.setAssignedUser(getAssigned(CaseDTO.UserAssignedType.EMPLOYEE.getId(), caso.getCaseTypeDTO().getId()));
		}
		final Map<String, String> data = new HashMap<String, String>();
		data.put("caseId", String.valueOf(caseId));
		final String message = String.format(
				"Se le ha asignado el caso %s para su atención", caseId);
		
		sendNotification(message, caso.getCreateUser(), caso.getAssignedUser(), "assign", data);
		
		final Usuario assignedUser = usuarioService.buscarUsuarioPorNombreUsuario(caso.getAssignedUser());
		if(StringUtils.isNotBlank(assignedUser.getEmail())) {
			advertiseByMail("Enlace Afirme - Nueva Asignación", message, assignedUser.getEmail());
		}
		caseDao.update(caso);
		return usuarioService.buscarUsuarioPorNombreUsuario(caso.getAssignedUser());
	}

	/**
	 * Obtiene el usuario al que se le asignará un caso dependiendo de si tipo
	 * 
	 * @param userType
	 * @param caseTypeId
	 * @return
	 */
	public String getAssigned(Integer userType, Long caseTypeId) {
		final CaseManagerUserDTO caseManager = new CaseManagerUserDTO();
		final CaseTypeDTO caseTypeDTO = entidadDao.findById(CaseTypeDTO.class,
				caseTypeId);
		caseManager.setUserType(userType);
		caseManager.setCaseTypeDTO(caseTypeDTO);
		List<CaseManagerUserDTO> lstCaseManager = entidadDao
				.findByFilterObject(CaseManagerUserDTO.class, caseManager);
		if (CollectionUtils.isEmpty(lstCaseManager)) {
			throw new RuntimeException("No existe manager configurado");
		}
		String manager = lstCaseManager.get(0).getUserName();
		return manager;
	}

	/**
	 * Indica si el usuario está marcado como VIP
	 * 
	 * @param createUser
	 * @return
	 */
	public boolean getVip(String createUser) {
		final CaseImportantUserDTO vip = new CaseImportantUserDTO();
		vip.setUserName(createUser);
		vip.setIsVip(1);
		final List<CaseImportantUserDTO> lstVip = entidadDao
				.findByFilterObject(CaseImportantUserDTO.class, vip);
		return CollectionUtils.isNotEmpty(lstVip);
	}

	/**
	 * Cambia el caso de estatus, Nuevo a Asignado, Asignado a Cerrado
	 * 
	 * @param caseId
	 * @param newStatus
	 * @param createUser
	 */
	public void changeStatus(Long caseId, CaseDTO.StatusEnum newStatus,	String createUser) {
		final CaseDTO caso = caseDao.findById(CaseDTO.class, caseId);
		final CaseHistDTO caseHistDTO = new CaseHistDTO();
		caseHistDTO.setCaseDTO(caso);
		caseHistDTO.setCreateDate(new Date());
		caseHistDTO.setCreateUser(createUser);
		caseHistDTO.setEndStatus(newStatus.getId());
		caseHistDTO.setIniStatus(caso.getStatus());
		entidadDao.persist(caseHistDTO);
		caso.setStatus(newStatus.getId());
		caseDao.update(caso);
		final String message = String.format(
				"El caso %s ha cambiado a estatus %s", caso.getId(),
				newStatus.getName());
		addMessage(caseId, message, "SISTEMA");
		final Map<String, String> data = new HashMap<String, String>();
		data.put("caseId", String.valueOf(caseId));
		
		sendNotification(message, "SISTEMA", caso.getCreateUser(), "changeStatus", data);
	}

	/**
	 * Agrega un comentario al caso
	 * 
	 * @param caseId
	 * @param message
	 * @param createUser
	 */
	public void addMessage(Long caseId, String message, String createUser) {
		final CaseConversationDTO caseConversationDTO = new CaseConversationDTO();
		final CaseDTO caseDTO = caseDao.findById(CaseDTO.class, caseId);
		caseConversationDTO.setCaseDTO(caseDTO);
		caseConversationDTO.setCreateUser(createUser);
		caseConversationDTO.setCreateDate(new Date());
		caseConversationDTO.setMessage(convertToUTF8(message));
		entidadDao.persist(caseConversationDTO);
		final Map<String, String> data = new HashMap<String, String>();
		data.put("caseId", String.valueOf(caseId));
		if (isManager(createUser)) {
			sendNotification(message, caseDTO.getAssignedUser(), caseDTO.getCreateUser(), "newMessage", data);
		} else {
			sendNotification(message, caseDTO.getCreateUser(),	caseDTO.getAssignedUser(), "newMessage", data);
		}
	}

	/**
	 * Obtiene los diferentes tipos de casos que se pueden atender
	 * 
	 * @return
	 */
	public List<CaseTypeDTO> getCaseTypes() {
		return entidadDao.findAll(CaseTypeDTO.class);
	}

	/**
	 * Obtiene todos los comentarios de un caso
	 * 
	 * @param caseId
	 * @return
	 */
	public List<CaseConversationDTO> getMessages(Long caseId) {
		final CaseConversationDTO filter = new CaseConversationDTO();
		final CaseDTO caseDTO = caseDao.getReference(CaseDTO.class, caseId);
		filter.setCaseDTO(caseDTO);
		final List<CaseConversationDTO> result = entidadDao.findByFilterObject(CaseConversationDTO.class, filter,
				"createDate");
		// Obtiene el nombre completo del usuario que creó el mensaje
		if(CollectionUtils.isNotEmpty(result)) {
			for(final CaseConversationDTO message: result) {
				message.setCreateUserName(usuarioService
						.buscarUsuarioPorNombreUsuario(message.getCreateUser())
						.getNombreCompleto());
			}
		}
		return result;
	}

	/**
	 * Se usa para desviar la atención de un caso a otra persona
	 * 
	 * @param createUser
	 * @param deviationUser
	 * @param iniDate
	 * @param endDate
	 * @return
	 */
	public CaseDeviationDTO addDeviation(String createUser,
			String deviationUser, Date iniDate, Date endDate) {

		CaseDeviationDTO caseDeviationDTO = getDeviation(createUser);
		if (caseDeviationDTO == null) {
			caseDeviationDTO = new CaseDeviationDTO();
		}
		caseDeviationDTO.setIniDate(iniDate);
		caseDeviationDTO.setEndDate(endDate);
		caseDeviationDTO.setCreateUser(createUser);
		caseDeviationDTO.setDeviationUser(deviationUser);
		caseDeviationDTO.setCreateDate(new Date());
		return caseDeviationDao.update(caseDeviationDTO);
	}

	/**
	 * Obtiene la lista de personas a las que se le puede desviar la atención de
	 * un caso
	 * 
	 * @param createUser
	 * @return
	 */
	public List<Usuario> getDeviationUserList(String createUser) {
		final CaseManagerUserDTO filter = new CaseManagerUserDTO();
		filter.setDependsOn(createUser);
		final List<CaseManagerUserDTO> lstManager = entidadDao
				.findByFilterObject(CaseManagerUserDTO.class, filter);
		final List<Usuario> result = new ArrayList<Usuario>();
		if (CollectionUtils.isNotEmpty(lstManager)) {
			for (CaseManagerUserDTO manager : lstManager) {
				result.add(usuarioService.buscarUsuarioPorNombreUsuario(manager
						.getUserName()));
			}
		}

		return result;
	}

	/**
	 * Obtiene la desviación de un caso por ausencia
	 * 
	 * @param createUser
	 * @return
	 */
	public CaseDeviationDTO getDeviation(String createUser) {
		List<CaseDeviationDTO> lstCaseDeviation = caseDeviationDao
				.findByProperty(CaseDeviationDTO.class, "createUser",
						createUser);
		if (CollectionUtils.isEmpty(lstCaseDeviation)) {
			return null;
		} else {
			return lstCaseDeviation.get(0);
		}
	}

	/**
	 * Finaliza o cierra un caso
	 * 
	 * @param caseId
	 * @param createUser
	 */
	public void closeCase(Long caseId, String createUser) {
		changeStatus(caseId, CaseDTO.StatusEnum.CLOSED, createUser);
	}

	/**
	 * Reasigna el caso a otra persona
	 * 
	 * @param caseId
	 * @param reassignUser
	 */
	public void reassign(Long caseId, String reassignUser) {
		final CaseDTO caso = caseDao.findById(CaseDTO.class, caseId);
		final String originalUser = caso.getAssignedUser(); 
		caso.setAssignedUser(reassignUser);
		caseDao.update(caso);
		changeStatus(caseId, StatusEnum.ASSIGNED, originalUser);
		final Map<String, String> data = new HashMap<String, String>();
		data.put("caseId", String.valueOf(caseId));
		final String message = String.format(
				"Se le ha reasignado el caso %s para su atención", caseId);
		
		sendNotification(message, "SISTEMA", reassignUser, "reassign", data);
		
		final Usuario assignedUser = usuarioService.buscarUsuarioPorNombreUsuario(reassignUser);
		if(StringUtils.isNotBlank(assignedUser.getEmail())) {
			advertiseByMail("Enlace Afirme - Nueva Asignación", message, assignedUser.getEmail());
		}
	}
	
	/**
	 * Envia notificación a dispositivos Android
	 * 
	 * @param message
	 * @param senderUserName
	 * @param receiptUserName
	 * @param evento
	 * @param data
	 * @param title
	 */
	public void advertise(String message, String senderUserName, String receiptUserName, String evento, Map<String, String> data, String title){
		try {
			final List<String> registrationId = usuarioService.getRegistrationIds(receiptUserName);
			
			if(CollectionUtils.isNotEmpty(registrationId)) {
				LOG.info("<<registrationId:"+registrationId.get(0));
				final Builder builder = new Builder();
				builder.addData("message",  message);
				builder.addData("sender", senderUserName);
				builder.addData("senderName", usuarioService
						.buscarUsuarioPorNombreUsuario(senderUserName)
						.getNombreCompleto());
				builder.addData("evento", evento);
				builder.addData("title", title);
				if (data != null) {
					for (Map.Entry<String, String> entry : data.entrySet()) {
						builder.addData(entry.getKey(), entry.getValue());
					}
				}
				final Sender sender = new Sender(sistemaContext.getEnlaceMovilSenderKey());
				sender.send(builder.build(), registrationId, 2);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	/**
	 * Envia notificación a dispositivos iOS
	 * 
	 * @param message
	 * @param senderUserName
	 * @param receiptUserName
	 * @param evento
	 * @param data
	 * @param registrationId
	 * @param title
	 */
	public void advertiseAPN(String message, String senderUserName, String receiptUserName, String evento, 
			Map<String, String> data, String registrationId, String title){
		SendAPNs apn = new SendAPNs();
		try {
			String pathFile = "";
			
			if (System.getProperty("os.name").toLowerCase().indexOf("windows")!=-1) {
				pathFile = SistemaPersistencia.UPLOAD_FOLDER + sistemaContext.getNombreCertificadoAPNs();
			} else {
				pathFile = SistemaPersistencia.LINUX_UPLOAD_FOLDER + sistemaContext.getNombreCertificadoAPNs();
			}
			
			String pw = sistemaContext.getPasswordCertificadoAPNs();
			
			data.put("message",  message);
			data.put("sender", senderUserName);
			data.put("senderName", usuarioService
					.buscarUsuarioPorNombreUsuario(senderUserName)
					.getNombreCompleto());
			data.put("evento", evento);
			data.put("title", title);
			
			LOG.debug(">> registrationId:"+registrationId+"\n>> ruta:"+pathFile);
			
			apn.sendAPN(data, registrationId, pathFile, pw, message, sistemaContext.getApnsEnvironment());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	public void advertiseByMail(String title, String message, String... address){
		try {
			if(!ArrayUtils.isEmpty(address)) {
				// TODO NO SUBIR ESTE COMENTADO mailService.sendMail(Arrays.asList(address), title, message);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	public boolean isManager(String userName) {
		final Usuario usuario = usuarioService.buscarUsuarioPorNombreUsuario(userName);
		for(Rol rol : usuario.getRoles()) {
			if("Rol_Enlace".equals(rol.getDescripcion())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Valida el registrationId para saber de qué tipo de dispositivo movil es (iOS, android)
	 * 
	 * @param message
	 * @param senderUserName
	 * @param receiptUserName
	 * @param evento
	 * @param data
	 */
	public void sendNotification(String message, String senderUserName, String receiptUserName, String evento, Map<String, String> data){
		try {
			LOG.info("--> receiptUserName:"+receiptUserName+"\n--> senderUserName:"+senderUserName);
			final List<String> registrationId = usuarioService.getRegistrationIds(receiptUserName);
			
			if(CollectionUtils.isNotEmpty(registrationId)) {
				int longitudRegistrationIdiOS = Integer.parseInt(sistemaContext.getLongitudRegistrationIdIOS());
				List<String> regAndroid = new ArrayList<String>();
				
				for (String reg: registrationId){
					//validar el tipo de token para ver si la plataforma es iOS.
					if (reg.length() == longitudRegistrationIdiOS) {
						advertiseAPN(message, senderUserName, receiptUserName, evento, data, reg, "Enlace Afirme");
					} else {
						regAndroid.add(reg);
					}
				}
				
				if (CollectionUtils.isNotEmpty(regAndroid)) {
					advertise(message, senderUserName, receiptUserName, evento, data, "Enlace Afirme");
				}
				
			}
		}catch (Exception e) {
			LOG.error("--> Ocurrió un error al enviar la notificación de enlace afirme\n"+e.getMessage(), e);
		}
	}
}
