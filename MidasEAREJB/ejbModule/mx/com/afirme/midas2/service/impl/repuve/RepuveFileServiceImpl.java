package mx.com.afirme.midas2.service.impl.repuve;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;
import org.apache.commons.io.FileUtils;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveNivNci;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisEmision;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisPTT;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisRecuperacion;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisRobo;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatEstatusBitacora;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisBitacorasService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.repuve.RepuveFileService;
import mx.com.afirme.midas2.service.repuve.RepuveSisService;

/*******************************************************************************
 * Nombre Interface: 	RepuveFileServiceImpl.
 * 
 * Descripcion: 		Se utiliza para el manejo del Modulo de REPUVE.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class RepuveFileServiceImpl implements RepuveFileService{
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = Logger.getLogger(RepuveFileServiceImpl.class);
	private static final String COMPANIA = "6018";
	private static final String PATH_PREFIJO = "REPUVE/";
	private static final String PATH_PREFIJO_DOWNLOAD = PATH_PREFIJO + "Download/";
	private static final String PATH_PREFIJO_UPLOAD = PATH_PREFIJO + "Upload/";
	private static final int OPERACION_ALTA = 1;
	private static final int OPERACION_CANCELACION = 3;
	private static final String OS_NAME = "os.name";
	private static final String WINDOWS = "windows";
	private static final String MENSAJE_EXCEPCION = "Se produjo una excepcion: ";
	private static final String REPUVE_NIV_NCI_ATRIBUTE = "repuveNivNci";

	private static final Pattern p = Pattern.compile("[^A-Za-z0-9]+");
	private Matcher m;
	
	@EJB private RepuveSisService repuveSisService;
	@EJB private SapAmisUtilsService sapAmisUtilsService;
	@EJB private EntidadService entidadService;
	@EJB private SapAmisBitacorasService sapAmisBitacorasService;

	@Override
	public File generarArchivoRepuve(Date fechaOperaciones, int tipoArchivo){
		String pathMidasWeb = (System.getProperty(OS_NAME).toLowerCase().indexOf(WINDOWS)!=-1) ? (SistemaPersistencia.UPLOAD_FOLDER + PATH_PREFIJO_DOWNLOAD) : (SistemaPersistencia.LINUX_UPLOAD_FOLDER + PATH_PREFIJO_DOWNLOAD);
		File pathFile = new File(pathMidasWeb); 
		File file = new File(pathMidasWeb, this.construirNombreArchivoDescarga(tipoArchivo, fechaOperaciones));
		this.limpiarDirectorios(pathFile);
		if(this.escribirArchivo(file, tipoArchivo, fechaOperaciones)){
			return file;
		}else{
			return null;
		}
	}
	
	@Override
	public boolean procesarArchivoRepuve(File file, String nombreArchivo){
		String uploadFolder = (System.getProperty(OS_NAME).toLowerCase().indexOf(WINDOWS)!=-1) ? (SistemaPersistencia.UPLOAD_FOLDER + PATH_PREFIJO_UPLOAD) : (SistemaPersistencia.LINUX_UPLOAD_FOLDER + PATH_PREFIJO_UPLOAD);
		try{
			File destFile = new File(uploadFolder, nombreArchivo);
			FileUtils.copyFile(file, destFile);
			this.procesarArchivoRepuve(uploadFolder, nombreArchivo);
			return true;
		}catch(IOException e){
			LOG.error(MENSAJE_EXCEPCION + e.getMessage());
			return false;
		}
	}
	
	//Metodos Privados.

	private int calcularCheckSum(List<RepuveNivNci> repuveNivNciList){
		char caracter;
		int caracterAscii;
		int sumaInterior = 0;
		for(RepuveNivNci repuveNivNci: repuveNivNciList){
			for(int i=0; i < repuveNivNci.getNiv().trim().length(); i++){
				caracter = repuveNivNci.getNiv().trim().charAt(i);
				caracterAscii = (int) caracter;
				sumaInterior = sumaInterior + caracterAscii;
			}
		}
		return (sumaInterior % 499);
	}

	private String construirNombreArchivoDescarga(int banderaTipo, Date fechaOperaciones){
		String nombreArchivo = "";
		if(banderaTipo == 1){
			nombreArchivo = "AvisosRepuve_" + sapAmisUtilsService.convertToStrDate(fechaOperaciones).replace("/", "-") + ".txt";
		}else if(banderaTipo == 2){
			nombreArchivo = "ConsultasRepuve_" + sapAmisUtilsService.convertToStrDate(fechaOperaciones).replace("/", "-") + ".txt";
		}
		return nombreArchivo;
	}

	private boolean escribirArchivo(File file, int tipoArchivo, Date fechaOperaciones){
		List<RepuveNivNci> repuveNivNciList = new ArrayList<RepuveNivNci>();
		int checkSum = 0;
		ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
		parametrosConsulta.setFechaOperacionIni(fechaOperaciones);
		parametrosConsulta.setFechaOperacionFin(fechaOperaciones);
		List<RepuveSisEmision> repuveSisEmisionList = repuveSisService.obtenerEmisionPorFiltros(parametrosConsulta, 10000, 1);
		int contadorRegistros = 0;
		for(RepuveSisEmision sis : repuveSisEmisionList){
			if(this.validaNivCorrecto(sis.getRepuveNivNci().getNiv().trim())){
				repuveNivNciList.add(sis.getRepuveNivNci());
				contadorRegistros ++;
			}
		}
		List<RepuveSisRobo> repuveSisRoboList = repuveSisService.obtenerRoboPorFiltros(parametrosConsulta, 10000, 1);
		for(RepuveSisRobo sis : repuveSisRoboList){
			if(this.validaNivCorrecto(sis.getRepuveNivNci().getNiv().trim())){
				repuveNivNciList.add(sis.getRepuveNivNci());
				contadorRegistros ++;
			}
		}
		List<RepuveSisRecuperacion> repuveSisRecuperacionList = repuveSisService.obtenerRecuperacionPorFiltros(parametrosConsulta, 10000, 1);
		for(RepuveSisRecuperacion sis : repuveSisRecuperacionList){
			if(this.validaNivCorrecto(sis.getRepuveSisRobo().getRepuveNivNci().getNiv().trim())){
				repuveNivNciList.add(sis.getRepuveSisRobo().getRepuveNivNci());
				contadorRegistros ++;
			}
		}
		List<RepuveSisPTT> repuveSisPTTList  = repuveSisService.obtenerPTTPorFiltros(parametrosConsulta, 10000, 1);
		for(RepuveSisPTT sis : repuveSisPTTList){
			if(this.validaNivCorrecto(sis.getRepuveNivNci().getNiv().trim())){
				repuveNivNciList.add(sis.getRepuveNivNci());
				contadorRegistros ++;
			}
		}
		checkSum = this.calcularCheckSum(repuveNivNciList);
		if(tipoArchivo==1){
			return this.escribirArchivoAvisos(file, repuveSisEmisionList, repuveSisRoboList, repuveSisRecuperacionList, repuveSisPTTList, checkSum, contadorRegistros);
		}else if(tipoArchivo == 2){
			return this.escribirArchivoConsultas(file, repuveNivNciList, checkSum);
		}else{
			return false;	
		}
	}

	private boolean procesarArchivoRepuve(String uploadFolder, String nombreArchivo){
		File pathFile = new File((System.getProperty(OS_NAME).toLowerCase().indexOf(WINDOWS)!=-1) ? (SistemaPersistencia.UPLOAD_FOLDER + PATH_PREFIJO_UPLOAD) : (SistemaPersistencia.LINUX_UPLOAD_FOLDER + PATH_PREFIJO_UPLOAD));
		this.limpiarDirectorios(pathFile);
		FileInputStream fstream = null;
		try{
			fstream = new FileInputStream(uploadFolder + nombreArchivo);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine = "";
 			while ((strLine = br.readLine()) != null) {
				String[] strLineParts = strLine.split("\\|");
				List<RepuveNivNci> repuveNivNciList = entidadService.findByProperty(RepuveNivNci.class, "niv", strLineParts[0]);
				if(strLineParts[5].equals("1")){
					List<RepuveSisEmision> repuveSisEmisionList = entidadService.findByProperty(RepuveSisEmision.class, "", repuveNivNciList.get(0));
					for(RepuveSisEmision repuveSisEmision: repuveSisEmisionList){
						if(repuveSisEmision.getSapAmisBitacoras().getCatOperaciones().getId().intValue() == OPERACION_ALTA){
							CatEstatusBitacora catEstatusBitacora = new CatEstatusBitacora();
							if(strLineParts[1].contains("ERR")){
								catEstatusBitacora.setId(new Long(-1));
								repuveSisEmision.getSapAmisBitacoras().setCatEstatusBitacora(catEstatusBitacora);
							}else{
								catEstatusBitacora.setId(new Long(0));
								repuveSisEmision.getSapAmisBitacoras().setCatEstatusBitacora(catEstatusBitacora);
							}	
							sapAmisBitacorasService.saveObject(repuveSisEmision.getSapAmisBitacoras());
						}
					}
				}
				if(strLineParts[5].equals("2")){
					List<RepuveSisEmision> repuveSisEmisionList = entidadService.findByProperty(RepuveSisEmision.class, REPUVE_NIV_NCI_ATRIBUTE, repuveNivNciList.get(0));
					for(RepuveSisEmision repuveSisEmision: repuveSisEmisionList){
						if(repuveSisEmision.getSapAmisBitacoras().getCatOperaciones().getId().intValue() == OPERACION_CANCELACION){
							CatEstatusBitacora catEstatusBitacora = new CatEstatusBitacora();
							if(strLineParts[1].contains("ERR")){
								catEstatusBitacora.setId(new Long(-1));
								repuveSisEmision.getSapAmisBitacoras().setCatEstatusBitacora(catEstatusBitacora);
							}else{
								catEstatusBitacora.setId(new Long(0));
								repuveSisEmision.getSapAmisBitacoras().setCatEstatusBitacora(catEstatusBitacora);
							}								
							sapAmisBitacorasService.saveObject(repuveSisEmision.getSapAmisBitacoras());
						}
					}
				}
				if(strLineParts[5].equals("3")){
					List<RepuveSisRobo> repuveSisRoboList = entidadService.findByProperty(RepuveSisRobo.class, REPUVE_NIV_NCI_ATRIBUTE, repuveNivNciList.get(0));
					for(RepuveSisRobo repuveSisRobo: repuveSisRoboList){
						if(repuveSisRobo.getSapAmisBitacoras().getCatOperaciones().getId().intValue() == OPERACION_ALTA){
							CatEstatusBitacora catEstatusBitacora = new CatEstatusBitacora();
							if(strLineParts[1].contains("ERR")){
								catEstatusBitacora.setId(new Long(-1));
								repuveSisRobo.getSapAmisBitacoras().setCatEstatusBitacora(catEstatusBitacora);
							}else{
								catEstatusBitacora.setId(new Long(0));
								repuveSisRobo.getSapAmisBitacoras().setCatEstatusBitacora(catEstatusBitacora);
							}								
							sapAmisBitacorasService.saveObject(repuveSisRobo.getSapAmisBitacoras());
						}
					}					
				}
				if(strLineParts[5].equals("4")){
					List<RepuveSisRecuperacion> repuveSisRecuperacionList = entidadService.findByProperty(RepuveSisRecuperacion.class, REPUVE_NIV_NCI_ATRIBUTE, repuveNivNciList.get(0));
					for(RepuveSisRecuperacion repuveSisRecuperacion: repuveSisRecuperacionList){
						if(repuveSisRecuperacion.getSapAmisBitacoras().getCatOperaciones().getId().intValue() == OPERACION_ALTA){
							CatEstatusBitacora catEstatusBitacora = new CatEstatusBitacora();
							if(strLineParts[1].contains("ERR")){
								catEstatusBitacora.setId(new Long(-1));
								repuveSisRecuperacion.getSapAmisBitacoras().setCatEstatusBitacora(catEstatusBitacora);
							}else{
								catEstatusBitacora.setId(new Long(0));
								repuveSisRecuperacion.getSapAmisBitacoras().setCatEstatusBitacora(catEstatusBitacora);
							}								
							sapAmisBitacorasService.saveObject(repuveSisRecuperacion.getSapAmisBitacoras());
						}
					}
				}
				if(strLineParts[5].equals("5")){
					List<RepuveSisPTT> repuveSisPTTList = entidadService.findByProperty(RepuveSisPTT.class, REPUVE_NIV_NCI_ATRIBUTE, repuveNivNciList.get(0));
					for(RepuveSisPTT repuveSisPTT: repuveSisPTTList){
						if(repuveSisPTT.getSapAmisBitacoras().getCatOperaciones().getId().intValue() == OPERACION_ALTA){
							CatEstatusBitacora catEstatusBitacora = new CatEstatusBitacora();
							if(strLineParts[1].contains("ERR")){
								catEstatusBitacora.setId(new Long(-1));
								repuveSisPTT.getSapAmisBitacoras().setCatEstatusBitacora(catEstatusBitacora);
							}else{
								catEstatusBitacora.setId(new Long(0));
								repuveSisPTT.getSapAmisBitacoras().setCatEstatusBitacora(catEstatusBitacora);
							}								
							sapAmisBitacorasService.saveObject(repuveSisPTT.getSapAmisBitacoras());
						}
					}
				}
			}
			in.close();
			return true;
		}catch (Exception e){
			LOG.error(MENSAJE_EXCEPCION + e.getMessage());
			return false;
		}finally{
			try {
				fstream.close();
			} catch (IOException e) {
				LOG.error("context", e);
			}
		}
	}

	private boolean escribirArchivoAvisos(File file, List<RepuveSisEmision> repuveSisEmisionList, List<RepuveSisRobo> repuveSisRoboList, List<RepuveSisRecuperacion> repuveSisRecuperacionList, List<RepuveSisPTT> repuveSisPTTList, int checkSum, int contadorRegistros){
		String fechaArchivo = this.convertirFechaFormatoRepuve(new Date());
		try{
			if(file.exists()){
				file.delete();
			}
			if(file.createNewFile()){
				BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
				bw.write(fechaArchivo + "|" + this.obtenerHoraFormatHHMM() + "|" + contadorRegistros + "|" + COMPANIA + "|" + checkSum + "\n");
				for(RepuveSisEmision repuveSisEmision : repuveSisEmisionList){
					if(this.validaNivCorrecto(repuveSisEmision.getRepuveNivNci().getNiv().trim())){
						if(repuveSisEmision.getSapAmisBitacoras().getCatOperaciones().getId().intValue() != 3){
							bw.write(
							/*01*/        repuveSisEmision.getRepuveNivNci().getNiv().trim() 
							/*02*/+ "|" + (repuveSisEmision.getRepuveNivNci().getNci() == null ? "" : repuveSisEmision.getRepuveNivNci().getNci())  
							/*03*/+ "|" + COMPANIA 
							/*04*/+ "|1"
		                    /*05*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisEmision.getSapAmisBitacoras().getFechaOperacion()) 
		                    /*06*/+ "|" + repuveSisEmision.getNumeroPoliza() 
		                    /*07*/+ "|" + repuveSisEmision.getCatTerminosCobertura().getId() 
		                    /*08*/+ "|" + this.obtenerClaveInegi(repuveSisEmision.getCatUbicacionMunicipio().getCatUbicacionEstado().getClaveSeycos())
		                    /*09*/+ "|" + repuveSisEmision.getCatUbicacionMunicipio().getCveInegiMunicipio()
		                    /*10*/+ "|" + (repuveSisEmision.getCatTipoPersona().getId().intValue()==1?0:1)
		                    /*11*/+ "|" + repuveSisEmision.getTitular()
		                    /*12*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisEmision.getFechaVigenciaInicio())
		                    /*13*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisEmision.getFechaVigenciaFin())
		                    /*14*/+ "|" 
		                    /*15*/+ "|" 
		                    /*16*/+ "|" 
		                    /*17*/+ "|" 
		                    /*18*/+ "|" 
		                    /*19*/+ "|"
							+ "\n"
							);
						}else{
							bw.write(
							/*01*/        repuveSisEmision.getRepuveNivNci().getNiv().trim() 
							/*02*/+ "|" + (repuveSisEmision.getRepuveNivNci().getNci() == null ? "" : repuveSisEmision.getRepuveNivNci().getNci()) 
							/*03*/+ "|" + COMPANIA 
							/*04*/+ "|2"
		                    /*05*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisEmision.getSapAmisBitacoras().getFechaOperacion()) 
		                    /*06*/+ "|" + repuveSisEmision.getNumeroPoliza() 
		                    /*07*/+ "|"  
		                    /*08*/+ "|" 
		                    /*09*/+ "|" 
		                    /*10*/+ "|" 
		                    /*11*/+ "|" 
		                    /*12*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisEmision.getSapAmisBitacoras().getFechaOperacion())
		                    /*13*/+ "|"
		                    /*14*/+ "|" + repuveSisEmision.getCatMotivoCancelacion().getId()
		                    /*15*/+ "|" 
		                    /*16*/+ "|" 
		                    /*17*/+ "|" 
		                    /*18*/+ "|" 
		                    /*19*/+ "|"
							+ "\n"
							);
						}
					}
				}
				for(RepuveSisRobo repuveSisRobo : repuveSisRoboList){
					if(this.validaNivCorrecto(repuveSisRobo.getRepuveNivNci().getNiv().trim())){
						bw.write(
						/*01*/        repuveSisRobo.getRepuveNivNci().getNiv().trim()
						/*02*/+ "|" + (repuveSisRobo.getRepuveNivNci().getNci() == null ? "" : repuveSisRobo.getRepuveNivNci().getNci())
						/*03*/+ "|" + COMPANIA
						/*04*/+ "|3"
						/*05*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisRobo.getSapAmisBitacoras().getFechaOperacion())
						/*06*/+ "|" + repuveSisRobo.getNumeroPoliza()
						/*07*/+ "|"
						/*08*/+ "|" + this.obtenerClaveInegi(repuveSisRobo.getCatUbicacionMunicipio().getCatUbicacionEstado().getClaveSeycos())
						/*09*/+ "|" + repuveSisRobo.getCatUbicacionMunicipio().getCveInegiMunicipio()
						/*10*/+ "|"
						/*11*/+ "|"
						/*12*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisRobo.getFechaRobo())
						/*13*/+ "|"
						/*14*/+ "|" 
						/*15*/+ "|" + repuveSisRobo.getNumeroAveriguacionPrevia()
						/*16*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisRobo.getFechaAveriguacionPrevia())
						/*17*/+ "|" 
						/*18*/+ "|" 
						/*19*/+ "|" + repuveSisRobo.getNumeroSiniestro()
						+ "\n"
						);
					}
				}
				for(RepuveSisRecuperacion repuveSisRecuperacion : repuveSisRecuperacionList){
					if(this.validaNivCorrecto(repuveSisRecuperacion.getRepuveSisRobo().getRepuveNivNci().getNiv().trim())){
						bw.write(
						/*01*/        repuveSisRecuperacion.getRepuveSisRobo().getRepuveNivNci().getNiv().trim()
						/*02*/+ "|" + (repuveSisRecuperacion.getRepuveSisRobo().getRepuveNivNci().getNci() == null ? "" : repuveSisRecuperacion.getRepuveSisRobo().getRepuveNivNci().getNci())
						/*03*/+ "|" + COMPANIA
						/*04*/+ "|4"
						/*05*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisRecuperacion.getSapAmisBitacoras().getFechaOperacion())
						/*06*/+ "|" + repuveSisRecuperacion.getRepuveSisRobo().getNumeroPoliza()
						/*07*/+ "|"
						/*08*/+ "|" + this.obtenerClaveInegi(repuveSisRecuperacion.getCatUbicacionMunicipio().getCatUbicacionEstado().getClaveSeycos())
						/*09*/+ "|" + repuveSisRecuperacion.getCatUbicacionMunicipio().getCveInegiMunicipio()
						/*10*/+ "|"
						/*11*/+ "|"
						/*12*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisRecuperacion.getFechaRecuperacion())
						/*13*/+ "|"
						/*14*/+ "|" 
						/*15*/+ "|" + repuveSisRecuperacion.getNumeroAveriguacionPrevia()
						/*16*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisRecuperacion.getFechaAveriguacionPrevia())
						/*17*/+ "|" 
						/*18*/+ "|" 
						/*19*/+ "|" + repuveSisRecuperacion.getRepuveSisRobo().getNumeroSiniestro()
						+ "\n"
						);
					}
				}
				for(RepuveSisPTT repuveSisPTT : repuveSisPTTList){
					if(this.validaNivCorrecto(repuveSisPTT.getRepuveNivNci().getNiv().trim())){
						bw.write(
						/*01*/        repuveSisPTT.getRepuveNivNci().getNiv().trim()
						/*02*/+ "|" + (repuveSisPTT.getRepuveNivNci().getNci() == null? "" : repuveSisPTT.getRepuveNivNci().getNci())
						/*03*/+ "|" + COMPANIA
						/*04*/+ "|5"
						/*05*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisPTT.getSapAmisBitacoras().getFechaOperacion())
						/*06*/+ "|" + repuveSisPTT.getNumeroPoliza()
						/*07*/+ "|"
						/*08*/+ "|" + this.obtenerClaveInegi(repuveSisPTT.getCatUbicacionMunicipio().getCatUbicacionEstado().getClaveSeycos())
						/*09*/+ "|" + repuveSisPTT.getCatUbicacionMunicipio().getCveInegiMunicipio()
						/*10*/+ "|"
						/*11*/+ "|"
						/*12*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisPTT.getFechaPTT())
						/*13*/+ "|"
						/*14*/+ "|" 
						/*15*/+ "|" + repuveSisPTT.getNumeroAveriguacionPrevia()
						/*16*/+ "|" + this.convertirFechaFormatoRepuve(repuveSisPTT.getFechaAveriguacionPrevia())
						/*17*/+ "|" + repuveSisPTT.getPorcentajePerdida()
						/*18*/+ "|" + repuveSisPTT.getCatMotivoPTT().getId()
						/*19*/+ "|" + repuveSisPTT.getNumeroSiniestro()
						+ "\n"
						);
					}
				}
				bw.close();
				return true;
			}
			return false;
		}catch (Exception e){
			LOG.error(MENSAJE_EXCEPCION + e.getMessage());
			return false;
		}
	}
	
	private boolean escribirArchivoConsultas(File file, List<RepuveNivNci> repuveNivNciList, int checkSum){
		String fechaArchivo = this.convertirFechaFormatoRepuve(new Date());
		if(file.exists()){
			file.delete();
		}
		FileWriter fileWriter = null;
		try{
			if(file.createNewFile()){
				fileWriter = new FileWriter(file, true);
				BufferedWriter bw = new BufferedWriter(fileWriter);
				bw.write(fechaArchivo + "|" + obtenerHoraFormatHHMM() + "|" + repuveNivNciList.size() + "|" + COMPANIA + "|" + checkSum + "\n");
				for(RepuveNivNci repuveNivNci: repuveNivNciList){
					bw.write(repuveNivNci.getNiv().trim() + "|" + COMPANIA + "|" + fechaArchivo + "\n");
				}
				bw.close();
				return true;
			}else{
				return false;
			}
		}catch (Exception e){
			LOG.error(MENSAJE_EXCEPCION + e.getMessage());
			return false;
		}finally{
			try {
				fileWriter.close();
			} catch (IOException e) {
				LOG.error("context", e);
			}
		}
	}
	
	private String obtenerHoraFormatHHMM(){
	    SimpleDateFormat sm = new SimpleDateFormat("HH:mm");
	    return sm.format(new Date());
	}
	
	private String convertirFechaFormatoRepuve(Date fecha){
	    SimpleDateFormat sm = new SimpleDateFormat("dd/MM/yyyy");
	    return fecha == null ? "" : sm.format(fecha);
	}
	
	private void limpiarDirectorios(File pathFile){
		if(pathFile.exists()){
			try{
				this.delete(pathFile);
			}catch(IOException e){
				System.exit(0);
				LOG.error(MENSAJE_EXCEPCION + e.getMessage());
	        }
		}
		pathFile.mkdirs();
	}

	private void delete(File file) throws IOException{
        if(file.isDirectory()){
    		if(file.list().length==0){
    		   file.delete();
    		}else{
        	   String files[] = file.list();
        	   for (String temp : files) {
        	      File fileDelete = new File(file, temp);
        	     delete(fileDelete);
        	   }
        	   if(file.list().length==0){
           	     file.delete();
        	   }
    		}
    	}else{
    		file.delete();
    	}
    }
	
	private String obtenerClaveInegi(String claveSeycos){
		if(claveSeycos.substring(0,1).equals("0")){
			return claveSeycos.substring(1, 2);
		}else{
			return claveSeycos.substring(0, 2);
		}
	}
	
	private boolean validaNivCorrecto(String niv){
		m = p.matcher(niv);
		boolean resultado = m.find();
		return !resultado;
	}
}