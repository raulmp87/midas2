package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fuerzaventa.SuspensionAgenteDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.SuspensionAgente;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.SuspensionAgenteService;

@Stateless
public class SuspensionAgenteServiceImpl implements SuspensionAgenteService{

	private SuspensionAgenteDao dao;

	@EJB
	public void setDao(SuspensionAgenteDao dao) {
		this.dao = dao;
	}

	@Override
	public SuspensionAgente autorizaSolicitudDeSuspension(SuspensionAgente solicitudSuspension) throws Exception {
		return dao.autorizaSolicitudDeSuspension(solicitudSuspension);
	}

	@Override
	public List<SuspensionAgente> findByFilters(SuspensionAgente filtro) throws Exception {
		return dao.findByFilters(filtro);
	}

	@Override
	public SuspensionAgente guardarSolicitudDeSuspension(SuspensionAgente solicitudSuspension) throws Exception {
		
		if (solicitudSuspension.getAgente().getId() == null || solicitudSuspension.getEstatusAgtSolicitado() == null) {
			
			throw new RuntimeException("Faltan datos necesarios para la solicitud.");
						
		}
		
		if (agenteMidasService.isAgentePromotor(solicitudSuspension.getAgente().getId())) {
			
			throw new RuntimeException("El cambio de situacion no procede ya que el agente es un promotor.");
			
		}
		
		return dao.guardarSolicitudDeSuspension(solicitudSuspension);
	}

	@Override
	public SuspensionAgente loadById(Long idSolicitudSuspension) throws Exception {
		return dao.loadById(idSolicitudSuspension);
	}

	@Override
	public SuspensionAgente rechazaSolicitudDeSuspension(SuspensionAgente solicitudSuspension) throws Exception {
		return dao.rechazaSolicitudDeSuspension(solicitudSuspension);
	}

	@Override
	public List<SuspensionAgente> findByFiltersView(SuspensionAgente filtro) throws Exception {
		return dao.findByFiltersView(filtro);
	}

	@Override
	public void auditarDocumentosEntregadosSuspension(Long idSuspension, Long idAgente,	String nombreAplicacion) throws Exception {
		dao.auditarDocumentosEntregadosSuspension(idSuspension, idAgente, nombreAplicacion);		
	}

	@Override
	public List<EntregoDocumentosView> consultaEstatusDocumentosSuspension(Long idSuspension, Long idAgente) throws Exception {
		return dao.consultaEstatusDocumentosSuspension(idSuspension, idAgente);
	}

	@Override
	public List<DocumentosAgrupados> documentosFortimaxGuardadosDBSuspension(Long idSuspension, Long idAgente) throws Exception {
		return dao.documentosFortimaxGuardadosDBSuspension(idSuspension, idAgente);
	}

	@Override
	public void crearYGenerarDocumentosFortimaxSuspension(SuspensionAgente suspension)	throws Exception {
		dao.crearYGenerarDocumentosFortimaxSuspension(suspension);		
	}
	
	private AgenteMidasService agenteMidasService;
	
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
}
