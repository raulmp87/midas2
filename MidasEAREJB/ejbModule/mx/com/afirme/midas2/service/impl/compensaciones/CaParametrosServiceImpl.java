/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaParametrosDao;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCompensacion;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.service.compensaciones.CaAutorizacionService;
import mx.com.afirme.midas2.service.compensaciones.CaParametrosService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales.RAMO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless

public class CaParametrosServiceImpl  implements CaParametrosService {
	public static final String MONTOUTILIDADES = "montoUtilidades";
	public static final String PORCENTAJEPAGO = "porcentajePago";
	public static final String MONTOPAGO = "montoPago";
	public static final String PORCENTAJEAGENTE = "porcentajeAgente";
	public static final String PORCENTAJEPROMOTOR = "porcentajePromotor";
	public static final String EMISIONINTERNA = "emisionInterna";
	public static final String EMISIONEXTERNA = "emisionExterna";
	public static final String PARAMETROACTIVO = "parametroActivo";
	public static final String TOPEMAXIMO = "topeMaximo";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	public static final String COMPENSADICIOID = "caCompensacion.id";
	public static final String TIPOCOMPENSACIONID ="caTipoCompensacion.id";
	public static final String ENTIDADPERSONAID = "caEntidadPersona.id";
	
	@EJB
	private CaParametrosDao parametrosGralescaDao;
	
	@EJB
	private CaAutorizacionService caAutorizacionService;
	
	
	private static final Logger LOG = LoggerFactory.getLogger(CaParametrosServiceImpl.class);
	

    public void save(CaParametros entity) {
    	LOG.info(">> save()");
	    try {
        	parametrosGralescaDao.save(entity);
        	LOG.info("<< save()");
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
	        throw re;
        }
    }
    
    public void delete(CaParametros entity) {
    	LOG.info(">> delete()");
        try {
	    	parametrosGralescaDao.delete(entity);
	    	LOG.info("<< delete()");
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
            throw re;
        }
    }
    

    public CaParametros update(CaParametros entity) {
    	LOG.info(">> update()");
        try {
            CaParametros result = parametrosGralescaDao.update(entity);
            LOG.info("<< update()");
	        return result;
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
	        throw re;
        }
    }
    
    public CaParametros findById( Long id) {
    	LOG.info(">> findById()");
	    try {
            CaParametros instance = parametrosGralescaDao.findById(id);
            LOG.info("<< findById()");
            return instance;
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
        	return null;
        }
    }    
    

    public List<CaParametros> findByProperty(String propertyName, final Object value) {
    	LOG.info(">> findByProperty()");
		try {
			List<CaParametros> list = parametrosGralescaDao.findByProperty(propertyName, value);
			LOG.info("<< findByProperty()");
			return list;
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			return null;
		}
	}
    public CaParametros findByCompensAdicioIdTipoCompensId(Long compensAdicioId,Long tipoCompensId){
    	LOG.info(">> findByCompensAdicioIdTipoCompensId()");
    	try {			
    		CaParametros caParametros = parametrosGralescaDao.findByCompensAdicioIdTipoCompensId(compensAdicioId, tipoCompensId);
			LOG.info("<< findByCompensAdicioIdTipoCompensId()");
    		return caParametros; 
		} catch (Exception e) {
			LOG.error("Información del Error", e);
			return null;
		}
    }
	public List<CaParametros> findByMontoutilidades(Object montoutilidades) {
		return findByProperty(MONTOUTILIDADES, montoutilidades);
	}
	
	public List<CaParametros> findByPorcentajepago(Object porcentajepago) {
		return findByProperty(PORCENTAJEPAGO, porcentajepago);
	}
	
	public List<CaParametros> findByMontopago(Object montopago) {
		return findByProperty(MONTOPAGO, montopago);
	}
	
	public List<CaParametros> findByPorcentajeagente(Object porcentajeagente) {
		return findByProperty(PORCENTAJEAGENTE, porcentajeagente);
	}
	
	public List<CaParametros> findByPorcentajepromotor(Object porcentajepromotor) {
		return findByProperty(PORCENTAJEPROMOTOR, porcentajepromotor);
	}
	
	public List<CaParametros> findByEmisioninterna(Object emisioninterna) {
		return findByProperty(EMISIONINTERNA, emisioninterna);
	}
	
	public List<CaParametros> findByEmisionexterna(Object emisionexterna) {
		return findByProperty(EMISIONEXTERNA, emisionexterna);
	}
	
	public List<CaParametros> findByParametroactivo(Object parametroactivo) {
		return findByProperty(PARAMETROACTIVO, parametroactivo);
	}
		
	public List<CaParametros> findByTopemaximo(Object topemaximo) {
		return findByProperty(TOPEMAXIMO, topemaximo);
	}
	
	public List<CaParametros> findByUsuario(Object usuario) {
		return findByProperty(USUARIO, usuario);
	}
	
	public List<CaParametros> findByBorradologico(Object borradologico) {
		return findByProperty(BORRADOLOGICO, borradologico);
	}
	
	public List<CaParametros> findAll() {
		LOG.info(">> findAll()");
		try {
			List<CaParametros> list = parametrosGralescaDao.findAll();
			return list;
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			return null;
		}
	}	
	
	public List<CaParametros> findAllbyCompensadicioId(BigDecimal compensAdicioId) {
		return findByProperty(COMPENSADICIOID, compensAdicioId);
	}
	
	public CaParametros findByTipocompeidCompeIdPersoId(CaTipoCompensacion tipoCompensacion,CaCompensacion compensacion,CaEntidadPersona entidadPersona){
		LOG.info(">> findByTipocompeidCompeIdPersoId()");
		try {
			CaParametros caParametros = parametrosGralescaDao.findByTipocompeidCompeIdPersoId( tipoCompensacion,compensacion,entidadPersona);
			LOG.info("<< findByTipocompeidCompeIdPersoId()");
			return caParametros;
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			return null;
		}
	}
	
	public List<CaParametros> findAllbyCompensacionEntidadpersona(CaCompensacion compensacion,CaEntidadPersona entidadPersona){
		LOG.info(">> findAllbyCompensacionEntidadpersona()");
		try {
			List<CaParametros> list = parametrosGralescaDao.findAllbyCompensacionEntidadpersona( compensacion,entidadPersona);
			LOG.info("<< findAllbyCompensacionEntidadpersona()");
			return list;
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			return null;
		}
	}
	
	/**
	 * Busca los parametros relacionados a la Compensacion
	 * @param caCompensacion
	 * @param compensacionesDTO
	 */
	public void findParametrosForCompensacion(CaCompensacion caCompensacion, CompensacionesDTO compensacionesDTO){
		LOG.info(">> findParametrosForCompensacion()");
		
		
		if(caCompensacion != null && compensacionesDTO != null && compensacionesDTO.getCaEntidadPersona() != null){
			
			/**
			 * Carga las Autorizaciones relacionadas a la Compensacion
			 */
			caAutorizacionService.cargarAutorizaciones(caCompensacion, compensacionesDTO);
			
			List<CaParametros> listParametros = this.findAllbyCompensacionEntidadpersona(caCompensacion,compensacionesDTO.getCaEntidadPersona());		
			
			/**
			 * Carga la Configuracion
			 */
			if(caCompensacion.getContraprestacion()){
				this.cargarContraprestacion(listParametros, caCompensacion, compensacionesDTO);
			}else{
				this.cargarCompensacion(listParametros, caCompensacion, compensacionesDTO);
			}
		}
		LOG.info("<< findParametrosForCompensacion()");
	}
	
	
	/**
	 * Carga la Configuracion por Compensacion
	 * @param listParametros
	 */
	private void cargarCompensacion(List<CaParametros> listParametros, CaCompensacion caCompensacion, CompensacionesDTO compensacionesDTO ){
		
		compensacionesDTO.setBloquearConfiguracion(ConstantesCompensacionesAdicionales.NO);
		
		if (listParametros != null && !listParametros.isEmpty()) {
					
			compensacionesDTO.setBloquearConfiguracion(ConstantesCompensacionesAdicionales.SI);
			compensacionesDTO.setCompensacionId(caCompensacion.getId());			
			
			if (compensacionesDTO.getCaEntidadPersona().isConvenioEspecial()){
				compensacionesDTO.setInclusionesGral(ConstantesCompensacionesAdicionales.INCLUIRCONVENIOESPECIAL);
			}else if (compensacionesDTO.getCaEntidadPersona().isCuadernoConcurso()){
				compensacionesDTO.setInclusionesGral(ConstantesCompensacionesAdicionales.INCLUIRCUADERNO);
			}else if (compensacionesDTO.getCaEntidadPersona().isConvenioEspecial() == compensacionesDTO.getCaEntidadPersona().isCuadernoConcurso()){
				compensacionesDTO.setInclusionesGral(ConstantesCompensacionesAdicionales.INCLUIRNINGUNO);
			}
					
			for (CaParametros parametrosGralesca : listParametros) {
					
				int tipoCompensacionActual = parametrosGralesca.getCaTipoCompensacion().getId().intValue();
				
				switch (tipoCompensacionActual) {
				
					case ConstantesCompensacionesAdicionales.PORPRIMA:
						if (parametrosGralesca.getParametroActivo()) {
							compensacionesDTO.setPorPrima(parametrosGralesca.getParametroActivo());
							compensacionesDTO.setPorcePrima(parametrosGralesca.getPorcentajePago());
							compensacionesDTO.setMontoPrima(parametrosGralesca.getMontoPago());
							compensacionesDTO.setPorceAgentePrima(parametrosGralesca.getPorcentajeAgenteProveedor());
							compensacionesDTO.setPorcePromotorPrima(parametrosGralesca.getPorcentajePromotor());
							compensacionesDTO.setBaseCalculocaPrima(parametrosGralesca.getCaBaseCalculo());
							if (compensacionesDTO.getRamo().getId() == RAMO.AUTOS.getValor()){
								compensacionesDTO.setCondicionesCalcPrima(parametrosGralesca.getCaTipoCondicionCalculo());								
							}else if(compensacionesDTO.getRamo().getId() == RAMO.DANOS.getValor()){
								compensacionesDTO.setTipoProvisioncaPrima(parametrosGralesca.getCaTipoProvision()); 
							}else if(compensacionesDTO.getRamo().getId() == RAMO.VIDA.getValor()){
								compensacionesDTO.setComisionAgente(parametrosGralesca.getComisionAgente() ? 1 : 0);
								compensacionesDTO.setIvaPrima(parametrosGralesca.getIva() ? 1 : 0);
							}else if(compensacionesDTO.getRamo().getId() == RAMO.BANCA.getValor()){
								compensacionesDTO.setBonoFijoVida(parametrosGralesca.getBonoFijoVida());								
							}
						}
					break;
					
					case ConstantesCompensacionesAdicionales.PORBAJASINIESTRALIDAD:
						if (parametrosGralesca.getParametroActivo()) {
							compensacionesDTO.setPorSiniestralidad(parametrosGralesca.getParametroActivo());
							compensacionesDTO.setPorceSiniestralidad(parametrosGralesca.getPorcentajePago());
							compensacionesDTO.setPorceAgenteSiniestralidad(parametrosGralesca.getPorcentajeAgenteProveedor());
							compensacionesDTO.setPorcePromotorSiniestralidad(parametrosGralesca.getPorcentajePromotor());
							compensacionesDTO.setBaseCalculocaSiniestralidad(parametrosGralesca.getCaBaseCalculo());
							compensacionesDTO.setIvaSiniestralidad(parametrosGralesca.getIva() ? 1 : 0);
						}
					break;
					
					case ConstantesCompensacionesAdicionales.DERECHOPOLIZA:
						if (parametrosGralesca.getParametroActivo()) {
							compensacionesDTO.setPorPoliza(parametrosGralesca.getParametroActivo());
							compensacionesDTO.setPorcePoliza(parametrosGralesca.getPorcentajePago());
							compensacionesDTO.setMontoPoliza(parametrosGralesca.getMontoPago());
							compensacionesDTO.setPorceAgentePoliza(parametrosGralesca.getPorcentajeAgenteProveedor());
							compensacionesDTO.setPorcePromotorPoliza(parametrosGralesca.getPorcentajePromotor());
							compensacionesDTO.setEmisionExterna(parametrosGralesca.getEmisionExterna());
							compensacionesDTO.setEmisionInterna(parametrosGralesca.getEmisionInterna());
						}
					break;
				
				}
			}
		}
	}
	
	/**
	 * Cargar la Configuracion de Contraprestacion
	 * @param listParametros
	 * @param caCompensacion
	 * @param compensacionesDTO
	 */
	private void cargarContraprestacion(List<CaParametros> listParametros, CaCompensacion caCompensacion, CompensacionesDTO compensacionesDTO ){
		
		compensacionesDTO.setBloquearConfiguracion(ConstantesCompensacionesAdicionales.NO);
							
		if(listParametros != null && !listParametros.isEmpty()) {
				
			compensacionesDTO.setBloquearConfiguracion(ConstantesCompensacionesAdicionales.SI);
		
			compensacionesDTO.setCompensacionId(caCompensacion.getId());
				
			for (CaParametros parametrosGralesca : listParametros) {		
				
				int tipoCompensacionActual = parametrosGralesca.getCaTipoCompensacion().getId().intValue();
				
				switch (tipoCompensacionActual) {
				
					case ConstantesCompensacionesAdicionales.PORPRIMA:
						if (parametrosGralesca.getParametroActivo()) {
							compensacionesDTO.setPorPrimaContra(parametrosGralesca.getParametroActivo());
							compensacionesDTO.setPorcePrimaContra(parametrosGralesca.getPorcentajePago());
							compensacionesDTO.setMontoPrimaContra(parametrosGralesca.getMontoPago());
							compensacionesDTO.setBaseCalculocaPrimaContra(parametrosGralesca.getCaBaseCalculo());
							compensacionesDTO.setTipoMonedacaPrimaContra(parametrosGralesca.getCaTipoMoneda());
							if (compensacionesDTO.getRamo().getId() == RAMO.AUTOS.getValor()){
								compensacionesDTO.setCondicionesCalcPrimaContra(parametrosGralesca.getCaTipoCondicionCalculo());								
							}else if(compensacionesDTO.getRamo().getId() == RAMO.DANOS.getValor()){
								compensacionesDTO.setTipoProvisioncaPrima(parametrosGralesca.getCaTipoProvision()); 
							}else if(compensacionesDTO.getRamo().getId() == RAMO.VIDA.getValor()){
								compensacionesDTO.setComisionAgente(parametrosGralesca.getComisionAgente() ? 1 : 0);
								compensacionesDTO.setIvaPrima(parametrosGralesca.getIva() ? 1 : 0);
							}else if(compensacionesDTO.getRamo().getId() == RAMO.BANCA.getValor()){
								compensacionesDTO.setBonoFijoVida(parametrosGralesca.getBonoFijoVida());		
							}
						}
						break;
					
					case ConstantesCompensacionesAdicionales.PORBAJASINIESTRALIDAD:
						if (parametrosGralesca.getParametroActivo()) {
							compensacionesDTO.setPorSiniestralidadContra(parametrosGralesca.getParametroActivo());
							compensacionesDTO.setPorceSiniestralidadContra(parametrosGralesca.getPorcentajePago());
							compensacionesDTO.setMontoSiniestralidadContra(parametrosGralesca.getMontoPago());							
							compensacionesDTO.setBaseCalculocaSiniestralidadContra(parametrosGralesca.getCaBaseCalculo());
							compensacionesDTO.setTipoMonedacaSiniestralidadContra(parametrosGralesca.getCaTipoMoneda());
							compensacionesDTO.setIvaSiniestralidad(parametrosGralesca.getIva() ? 1 : 0);
						}
						break;					
					
					case ConstantesCompensacionesAdicionales.PORUTILIDAD:
						if (parametrosGralesca.getParametroActivo()) {
							compensacionesDTO.setPorUtilidad(parametrosGralesca.getParametroActivo());
							compensacionesDTO.setPorcenUtilidadesUtilidad(parametrosGralesca.getPorcentajePago());
							compensacionesDTO.setMontoUtilidadesUtilidad(parametrosGralesca.getMontoPago());
							compensacionesDTO.setMontoDeLasUtilidades(parametrosGralesca.getMontoUtilidades());
						}
						break;
						
					case ConstantesCompensacionesAdicionales.PORCUMPLIMIENTODEMETA:
						if (parametrosGralesca.getParametroActivo()) {
							compensacionesDTO.setPorCumplimientoDeMeta(parametrosGralesca.getParametroActivo());					
							compensacionesDTO.setBaseCalculoCumplimientoMeta(parametrosGralesca.getCaBaseCalculo());
							compensacionesDTO.setCaTipoMonedaCumplimientoMeta(parametrosGralesca.getCaTipoMoneda());
							compensacionesDTO.setPresupuestoAnual(parametrosGralesca.getPresupuestoAnual());
						}
						break;	
				}
			}			
		}
	}
}