package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.inciso.configuracion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.TarifaAutoProliberCombo;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionIncisoGrupoProliberAVService;
@Stateless
public class ConfiguracionIncisoGrupoProliberAVServiceImpl implements
		ConfiguracionIncisoGrupoProliberAVService {

	@Override
	public List<TarifaAutoProliberCombo> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TarifaAutoProliberCombo> findAll(BigDecimal... ids) {
		String spName="MIDAS.pkgAUT_Servicios.spAUT_GProlAVPorATLinNegMonTV";
		String [] atributosDTO = { "claveGrupoProliber"};
		String [] columnasCursor = {"claveGrupoProliber"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdAgrupadorTarifa", ids[0]);
			storedHelper.estableceParametro("pIdVerAgrupadorTarifa", ids[1]);
			storedHelper.estableceParametro("pIdLineaNegocio", ids[2]);
			storedHelper.estableceParametro("pIdMoneda", ids[3]);
			storedHelper.estableceParametro("pIdTipoVehiculo", ids[4]);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.TarifaAutoProliberCombo", atributosDTO, columnasCursor);
			
			return storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName,e);
		}
	}

	@Override
	public TarifaAutoProliberCombo findById(BigDecimal arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TarifaAutoProliberCombo findById(CatalogoValorFijoId arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TarifaAutoProliberCombo findById(double arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TarifaAutoProliberCombo> listRelated(Object arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
