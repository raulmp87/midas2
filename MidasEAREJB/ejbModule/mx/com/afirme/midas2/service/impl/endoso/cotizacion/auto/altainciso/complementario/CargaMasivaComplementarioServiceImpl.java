package mx.com.afirme.midas2.service.impl.endoso.cotizacion.auto.altainciso.complementario;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.Inciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.ExcelRowValidationMapper;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.banco.BancoMidasService;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.altainciso.complementario.CargaMasivaComplementarioService;
import mx.com.afirme.midas2.service.impl.ExcelConverter.ExcelConfig.ExcelConfigBuilder;
import mx.com.afirme.midas2.service.impl.ExcelLoader;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.util.WriterUtil;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.afirme.commons.exception.CommonException;
import com.afirme.commons.util.BeanUtils;

@Stateless
public class CargaMasivaComplementarioServiceImpl implements CargaMasivaComplementarioService {
	
	public static final String VALOR_VISA = "VISA";
	public static final String VALOR_MASTERCARD = "MASTERD CARD";
	public static final String VALOR_TC= "TARJETA DE CREDITO";
	public static final String VALOR_DOMICILIACION= "COBRANZA DOMICILIADA";
	public static final String VALOR_EFECTIVO= "EFECTIVO";
	public static final String VALOR_CUENTA_AFIRME= "CUENTA AFIRME (CREDITO O DEBITO)";
	
	private static final String MEDIO_PAGO_TC = "4";
	private static final String MEDIO_PAGO_DOMICILIACION = "8";
	private static final String MEDIO_PAGO_EFECTIVO = "15";
	private static final String MEDIO_CUENTA_AFIRME= "1386";
		
	private static final String TIPO_CUENTA_DEBITO = "DEBITO";
	private static final String TIPO_CUENTA_CUENTA_CLABE = "CUENTA CLABE";
	private static final String TIPO_CUENTA_CUENTA_BANCARIA = "CUENTA BANCARIA";
	
	private BitemporalCotizacion bitemporalCotizacion;
	private BancoMidasService bancoMidasService;
	private IncisoViewService incisoService;	
	private ListadoService listadoService;
	private ClienteFacadeRemote clienteFacadeRemote;
	private Map<String, String> bancos;
	final SimpleDateFormat creditCardFormat = new SimpleDateFormat("MM/yy");		

	@Override
	public TransporteImpresionDTO obtenerPlantilla(BigDecimal idCotizacion, DateTime fechaIniVigenciaEndoso,ListadoService listadoService) {
		
		List<BitemporalAutoInciso> bitemporalAutoIncisos = new ArrayListNullAware<BitemporalAutoInciso>();
		List<AutoInciso> autoIncisos;
		String nombreArchivo;
		String numeroPoliza;
		this.listadoService=listadoService;
		
		bitemporalAutoIncisos = obtenerAutoIncisosEndoso(idCotizacion, fechaIniVigenciaEndoso);
		
		autoIncisos = obtenerDatosPlantilla(bitemporalAutoIncisos);
		
		if(autoIncisos!=null&&autoIncisos.get(0)!=null){
			
			autoIncisos.get(0).setConductoCobro(new String[] { VALOR_TC, VALOR_DOMICILIACION, VALOR_EFECTIVO,VALOR_CUENTA_AFIRME });
			autoIncisos.get(0).setTipoTarjeta(new String[] { VALOR_VISA, VALOR_MASTERCARD });

			bancos = listadoService.getBancos();
			String[] bancosArray = bancos.values().toArray(new String[0]);
			Arrays.sort(bancosArray);
			autoIncisos.get(0).setInstitucionBancaria(bancosArray);
		}
		
		numeroPoliza = obtenerNumeroPoliza(idCotizacion);
				
		nombreArchivo = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
					"midas.movimiento.endoso.altainciso.datoscomplementarios.masivo.plantilla.nombre", numeroPoliza);
				
		ExcelExporter exporter = new ExcelExporter(AutoInciso.class);
		
		// Si se desea con menos 'formato' puede ser tambien: exportCSV
		return exporter.exportXLS(autoIncisos, nombreArchivo);    

	}

	@Override
	public TransporteImpresionDTO procesar(BigDecimal idCotizacion, BigDecimal idToControlArchivo, DateTime fechaIniVigenciaEndoso,BancoMidasService bancoMidasService,IncisoViewService incisoService,ListadoService listadoService,ClienteFacadeRemote clienteFacadeRemote) {
		
		List<BitemporalAutoInciso> bitemporalAutoIncisos = new ArrayListNullAware<BitemporalAutoInciso>();
		this.bitemporalCotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME,	idCotizacion, fechaIniVigenciaEndoso);	;
		this.bancoMidasService = bancoMidasService;
		this.incisoService=incisoService;
		this.listadoService=listadoService;
		this.clienteFacadeRemote=clienteFacadeRemote;
		Integer rowNumber = 1;
		List<AutoInciso> autoIncisos;
		ExcelLoader loader;
		Predicate itemValido;
		BitemporalAutoInciso bitAutoInciso;
		InputStream is = null;
		RuntimeException rtEx = null;
		File file = null;
		
		try {	
			
			bitemporalAutoIncisos = obtenerAutoIncisosEndoso(idCotizacion, fechaIniVigenciaEndoso);
			
			file = leerArchivo(idToControlArchivo);
			
			is = new FileInputStream(file);
						
			loader = new ExcelLoader(is, new ExcelConfigBuilder()
									.withColumnNamesToLowerCase(true)
									.withTrimColumnNames(true)
									.build());
			
			autoIncisos = procesarPlantilla(bitemporalAutoIncisos, loader);	
					
			for (AutoInciso autoInciso : autoIncisos) {
	
				itemValido = obtenerPredicadoItemValido(autoInciso);
							
				bitAutoInciso = (BitemporalAutoInciso)CollectionUtils.find(bitemporalAutoIncisos, itemValido);
							
				actualizarDatosComplementarios(bitAutoInciso.getContinuity().getId(), autoInciso, idCotizacion,loader,rowNumber);
				rowNumber++;
			}
					
			if (loader.hasErrors()) {
				
				return WriterUtil.wrapInXLSFile(loader.getErrorLog(file), "Errores");
				
			}
		
		} catch (Exception e) {
			
			rtEx = new RuntimeException(e);
			
		} finally {
			
            if (is != null) {
                    try {
                    	
                            is.close();
                            
                    } catch (IOException e) {
                    	
                    		logger.error(e.getMessage());
                            
                    }
                    
            }
            
            if (rtEx != null) throw rtEx;
                      
		}
				
		return null;
	}

	private List<AutoInciso> procesarPlantilla(final List<BitemporalAutoInciso> bitemporalAutoIncisos, ExcelLoader loader) {
		
		final SimpleDateFormat sdf = loader.getConfig().getDateFormatter();
		sdf.setLenient(false);
		creditCardFormat.setLenient(false);
				
		return loader.parseWithRowValidationMapper(new ExcelRowValidationMapper<AutoInciso>() {

			@Override
			public AutoInciso mapRow(Map<String, String> row, int rowNum) {
				
				AutoInciso autoInciso = new AutoInciso();
				Integer numeroInciso = null;
				Date fechaNacConductor = null;
				Date fechaVencimientoTarjeta = null;
								
				try {
					
					numeroInciso = Integer.parseInt(row.get("numero inciso"));
					
					autoInciso.setNumeroInciso(numeroInciso);
				
				} catch(NumberFormatException e) {
					
					throw new RuntimeException("El formato del numero de inciso no es valido");
				
				}
				
				autoInciso.setDescripcionFinal(row.get("descripcion"));  
				
				autoInciso.setNombreAsegurado(row.get("asegurado"));
				
				autoInciso.setObservacionesinciso(row.get("observaciones"));
				
				autoInciso.setNumeroSerie(row.get("numero serie"));
				
				autoInciso.setNumeroMotor(row.get("numero motor"));
				
				autoInciso.setPlaca(row.get("placa"));
								
				autoInciso.setNombreConductor(row.get("nombre conductor"));
				
				autoInciso.setPaternoConductor(row.get("ap paterno conductor"));
				
				autoInciso.setMaternoConductor(row.get("ap materno conductor"));
				
				autoInciso.setNumeroLicencia(row.get("numero licencia"));
				
				if (row.get("fecha nac conductor") != null) {
					
					try {
						
						fechaNacConductor = sdf.parse(row.get("fecha nac conductor"));
						
						autoInciso.setFechaNacConductor(fechaNacConductor);
					
					} catch (ParseException pex) {
						
						throw new RuntimeException("El formato de la fecha no es valido");
						
					}
					
				}
												
				autoInciso.setOcupacionConductor(row.get("ocupacion conductor"));
				
				if (row.get("conducto cobro") != null && !row.get("conducto cobro").isEmpty()) {
					autoInciso.setConductoCobroValue(row.get("conducto cobro"));
					
					if(row.get("conducto cobro").equals(VALOR_TC)){
						autoInciso.setIdMedioPago(MEDIO_PAGO_TC);
					}else if(row.get("conducto cobro").equals(VALOR_DOMICILIACION)){
						autoInciso.setIdMedioPago(MEDIO_PAGO_DOMICILIACION);
					}else if(row.get("conducto cobro").equals(VALOR_EFECTIVO)){
						autoInciso.setIdMedioPago(MEDIO_PAGO_EFECTIVO);
					}else if(row.get("conducto cobro").equals(VALOR_CUENTA_AFIRME)){
						autoInciso.setIdMedioPago(MEDIO_CUENTA_AFIRME);
					}
				}
				
				if(!row.get("conducto cobro").equals(VALOR_EFECTIVO)){
					autoInciso.setInstitucionBancariaValue(row.get("institucion bancaria"));
					autoInciso.setTipoTarjetaValue(row.get("tipo de tarjeta"));
					DecimalFormat df = new DecimalFormat("#");
			        df.setMaximumFractionDigits(16);
					autoInciso.setNumeroTarjetaClave(df.format(Double.parseDouble(row.get("numero de tarjeta o clabe"))));
					autoInciso.setCodigoSeguridad(row.get("codigo de seguridad"));
					
					if (row.get("fecha vencimiento") != null) {
						
						//Para obtenr la fecha recibida en le excel segun le formato que se envia
						SimpleDateFormat dateFormat = new SimpleDateFormat();
						dateFormat.setLenient(false);
						
						try {
							dateFormat.applyPattern("dd/MM/yyyy");
							autoInciso.setFechaVencimiento(dateFormat.parse(row.get("fecha vencimiento")));
						} catch (ParseException par) {
							try {
								dateFormat.applyPattern("dd-MMM-yyyy");
								autoInciso.setFechaVencimiento(dateFormat.parse(row.get("fecha vencimiento")));
							} catch (ParseException per) {
								try {
									dateFormat.applyPattern("MM-yy");
									autoInciso.setFechaVencimiento(dateFormat.parse(row.get("fecha vencimiento")));
								} catch (ParseException pex) {
									try {
										dateFormat.applyPattern("MM/yy");
										autoInciso.setFechaVencimiento(dateFormat.parse(row.get("fecha vencimiento")));	
									} catch (ParseException pars) {
										autoInciso.setFechaVencimiento(null);
									}		
								}		
							}		
						}
					}
				}
				return autoInciso;
			}

			@Override
			public void validateMapperEntry(final AutoInciso autoInciso,
					List<AutoInciso> autoIncisos) {
				
				//Se valida que el inciso mapeado se encuentre entre los incisos a complementar
				
				Predicate itemValido = obtenerPredicadoItemValido(autoInciso);
			
				if (!CollectionUtils.exists(bitemporalAutoIncisos, itemValido)) {
					
					throw new RuntimeException("El numero de inciso no es valido en este endoso");
					
				}
				
				
				//Se valida que los registros no se dupliquen (que no existan 2 renglones en el excel con el mismo numero de inciso)
				
				Predicate itemDuplicado = new Predicate() {		
					@Override
					public boolean evaluate(Object arg0) {
						AutoInciso elemento = (AutoInciso) arg0;
						
						if (autoInciso.getNumeroInciso().equals(elemento.getNumeroInciso())) {
							
							return true;
							
						}
						
						return false;
						
					}
				};
				
				if (CollectionUtils.exists(autoIncisos, itemDuplicado)) {
					
					throw new RuntimeException("Registro duplicado (mismo numero de inciso)");
					
				}
						
			}
			
		});
		
	}
	
	private void actualizarDatosComplementarios(Long autoIncisoContinuityId, AutoInciso autoInciso, BigDecimal idCotizacion, ExcelLoader loader, Integer rowNumber) {
		
		DateTime fechaInicioVigencia = obtenerFechaInicioVigencia(idCotizacion);
		
		BitemporalAutoInciso bt = new BitemporalAutoInciso();
		
		BitemporalAutoInciso bitAutoInciso = entidadBitemporalService.getInProcessByKey(AutoIncisoContinuity.class, autoIncisoContinuityId, fechaInicioVigencia);
		
		AutoIncisoContinuity autoIncisoContinuity = new AutoIncisoContinuity();
		
		IncisoContinuity incisoContinuity = new IncisoContinuity();
		
		incisoContinuity.setId(bitAutoInciso.getContinuity().getParentContinuity().getId());
		
		autoIncisoContinuity.setIncisoContinuity(incisoContinuity);
		
		autoIncisoContinuity.setId(autoIncisoContinuityId);
		
		try {
			
			BeanUtils.copyProperties(bt.getEmbedded(), bitAutoInciso.getEmbedded());
			
		} catch (CommonException e) {
			
			throw new RuntimeException(e);
			
		}
		
		bt.setId(null);
		
		bt.getValue().setNombreAsegurado(autoInciso.getNombreAsegurado());
		
		bt.getValue().setObservacionesinciso(autoInciso.getObservacionesinciso());
		
		bt.getValue().setNumeroSerie(autoInciso.getNumeroSerie());
		
		bt.getValue().setNumeroMotor(autoInciso.getNumeroMotor());
		
		bt.getValue().setPlaca(autoInciso.getPlaca());
						
		bt.getValue().setNombreConductor(autoInciso.getNombreConductor());
		
		bt.getValue().setPaternoConductor(autoInciso.getPaternoConductor());
		
		bt.getValue().setMaternoConductor(autoInciso.getMaternoConductor());
		
		bt.getValue().setNumeroLicencia(autoInciso.getNumeroLicencia());
		
		bt.getValue().setFechaNacConductor(autoInciso.getFechaNacConductor());

		bt.getValue().setOcupacionConductor(autoInciso.getOcupacionConductor());
		
		bt.getValue().setDescripcionFinal(autoInciso.getDescripcionFinal());
		
		bt.setEntidadContinuity(autoIncisoContinuity);
		
		entidadBitemporalService.saveInProcess(bt, fechaInicioVigencia);
		
		
		BitemporalInciso bitemporalInciso = incisoService.getIncisoInProcess(incisoContinuity.getId(), fechaInicioVigencia.toDate());
		
		try {
			//Para guardar el idClienteCob y el idConductoCobro
			BigDecimal idPersonaContratante = new BigDecimal(bitemporalCotizacion.getValue().getPersonaContratanteId());
			ClienteDTO cliente = null;
			ClienteGenericoDTO cuenta = null;
			
			if (idPersonaContratante != null) {
				cliente = clienteFacadeRemote.findById(idPersonaContratante,"nombreUsuario");
			}
			if (cliente != null) {
	
				bitemporalInciso.getValue().setIdClienteCob(cliente.getIdCliente());
				int conductoCobro = listadoService.getConductosCobro(cliente.getIdCliente().longValue(),Integer.parseInt(autoInciso.getIdMedioPago().toString())).size() - 1;
				bitemporalInciso.getValue().setIdConductoCobroCliente(new Long((conductoCobro > 0) ? (conductoCobro) : 1));
			}
			
			bitemporalInciso.getValue().setIdMedioPago(new BigDecimal(autoInciso.getIdMedioPago()));
			bitemporalInciso.getValue().setConductoCobro(autoInciso.getConductoCobroValue());

			String tipoCuenta = autoInciso.getTipoTarjetaValue();
			if (!autoInciso.getIdMedioPago().equals(MEDIO_PAGO_EFECTIVO)) {
				if(autoInciso.getIdMedioPago().equals(MEDIO_PAGO_DOMICILIACION)||autoInciso.getIdMedioPago().equals(MEDIO_CUENTA_AFIRME)){
					if (autoInciso.getNumeroTarjetaClave().length()==16){
						tipoCuenta = TIPO_CUENTA_DEBITO;
					}else if(autoInciso.getNumeroTarjetaClave().length()==18){
						tipoCuenta = TIPO_CUENTA_CUENTA_CLABE;
					}else{
						tipoCuenta = TIPO_CUENTA_CUENTA_BANCARIA;
					}
				}
				
				//Informacion de la tarjeta de Credito
				bitemporalInciso.getValue().setInstitucionBancaria(autoInciso.getInstitucionBancariaValue());
				bitemporalInciso.getValue().setTipoTarjeta(tipoCuenta);
				bitemporalInciso.getValue().setNumeroTarjetaClave(bancoMidasService.encriptaInformacionBancaria(autoInciso.getNumeroTarjetaClave()));
				
				if (autoInciso.getCodigoSeguridad() != null&& autoInciso.getFechaVencimiento() != null) {
					bitemporalInciso.getValue().setCodigoSeguridad(bancoMidasService.encriptaInformacionBancaria(autoInciso.getCodigoSeguridad()));
					bitemporalInciso.getValue().setFechaVencimiento(bancoMidasService.encriptaInformacionBancaria(creditCardFormat.format(autoInciso.getFechaVencimiento())));
				}
	
				//Gaurdar en ClienteCob
				try{
					if (bitemporalInciso.getValue().getIdMedioPago() != null&& bitemporalInciso.getValue().getConductoCobro() != null&& bitemporalInciso.getValue().getInstitucionBancaria() != null) {
						cuenta = clienteMigracion(bitemporalCotizacion,bitemporalInciso.getValue(), cliente);
						clienteFacadeRemote.guardarDatosCobranza(cuenta, "MIDAS", true);
					}
				}catch(Exception e){
					loader.setError(rowNumber, "Conducto Cobro", "Error al guardar el conducto de cobro: "+e.getMessage());
					logger.error("Error al guardar el conducto de cobro: "+e.getMessage());
				}
			}else{
							
				bitemporalInciso.getValue().setInstitucionBancaria(null);
				bitemporalInciso.getValue().setTipoTarjeta(null);
				bitemporalInciso.getValue().setNumeroTarjetaClave(null);				
				bitemporalInciso.getValue().setCodigoSeguridad(null);
				bitemporalInciso.getValue().setFechaVencimiento(null);
			}
	
			} catch (Exception e) {
				loader.setError(rowNumber, "Conducto Cobro", e.getMessage());
				logger.error(e.getMessage());			
		}
		entidadBitemporalService.saveInProcess(bitemporalInciso,fechaInicioVigencia);

	}
	
	private ClienteGenericoDTO clienteMigracion(BitemporalCotizacion biCotizacion,Inciso inciso, ClienteDTO clienteDto) {
		ClienteGenericoDTO cliente = new ClienteGenericoDTO();
		cliente.setIdCliente(inciso.getIdClienteCob());
		cliente.setIdConductoCobranza(inciso.getIdConductoCobroCliente());
		cliente.setNombreTarjetaHabienteCobranza(biCotizacion.getValue().getNombreContratante());
		cliente.setEmailCobranza("");
		cliente.setTelefonoCobranza("");
		cliente.setNombreCalleCobranza(clienteDto.getNombreCalle());
		cliente.setCodigoPostalCobranza(clienteDto.getCodigoPostal());
		cliente.setNombreColoniaCobranza(clienteDto.getNombreColonia());
		cliente.setIdTipoConductoCobro(inciso.getIdMedioPago().intValue());
		
		List<BancoEmisorDTO> bancos = bancoMidasService.bancosSeycos();
		Integer idBancoCobranza = null;
		for(BancoEmisorDTO banco: bancos){
			if(inciso.getInstitucionBancaria().equals(banco.getNombreBanco())){
				idBancoCobranza =  banco.getIdBanco();
			}
		}
		cliente.setIdBancoCobranza(new BigDecimal(idBancoCobranza));
		cliente.setIdTipoTarjetaCobranza(inciso.getTipoTarjeta());
		cliente.setNumeroTarjetaCobranza(bancoMidasService.desencriptaInformacionBancaria(inciso.getNumeroTarjetaClave()).trim());
		//Concidicon para cuando el conducto de cobro es del tipo tarjeta de credito
		if (inciso.getCodigoSeguridad() != null&&inciso.getFechaVencimiento()!=null) {
			cliente.setCodigoSeguridadCobranza(bancoMidasService.desencriptaInformacionBancaria(inciso.getCodigoSeguridad()).trim());
			cliente.setFechaVencimientoTarjetaCobranza(bancoMidasService.desencriptaInformacionBancaria(inciso.getFechaVencimiento()).replace("/", ""));
		}		
		cliente.setTipoPromocion("");
		cliente.setDiaPagoTarjetaCobranza(new BigDecimal(0));				
		cliente.setRfcCobranza(clienteDto.getCodigoRFC());
		
		return cliente;
	}
		
	private Predicate obtenerPredicadoItemValido(final AutoInciso autoInciso) {
		
		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				BitemporalAutoInciso elemento = (BitemporalAutoInciso) arg0;
										
				if (autoInciso.getNumeroInciso().equals(elemento.getContinuity().getParentContinuity().getNumero())) {
					
					return true;
					
				}
				
				return false;
				
			}
		};
		
	}
	
	
	private List<AutoInciso> obtenerDatosPlantilla(List<BitemporalAutoInciso> bitemporalAutoIncisos) {
		
		//Se llena un List<AutoInciso> con datos que van como default en la plantilla (numero inciso, descripcion, modelo)
		
		AutoInciso autoInciso;
		
		List<AutoInciso> autoIncisos = new ArrayListNullAware<AutoInciso>();
				
		for (BitemporalAutoInciso bitAutoInciso : bitemporalAutoIncisos) {
			
			autoInciso = new AutoInciso();
			
			autoInciso.setNumeroInciso(bitAutoInciso.getContinuity().getParentContinuity().getNumero());
			
			autoInciso.setDescripcionFinal(bitAutoInciso.getValue().getDescripcionFinal());
			
			autoInciso.setModeloVehiculo(bitAutoInciso.getValue().getModeloVehiculo());
			
			autoIncisos.add(autoInciso);
			
		}
		
		return autoIncisos;
		
	}
	
	private List<BitemporalAutoInciso> obtenerAutoIncisosEndoso(BigDecimal idCotizacion, DateTime fechaIniVigenciaEndoso) {
		
		//Se obtienen los autoincisos inprocess con sus correspondientes incisos 
		
		List<BitemporalAutoInciso> bitemporalAutoIncisos = new ArrayListNullAware<BitemporalAutoInciso>();
				
		BitemporalCotizacion cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,
				CotizacionContinuity.BUSINESS_KEY_NAME,	idCotizacion, fechaIniVigenciaEndoso);	
		
		Collection<BitemporalInciso> incisos = cotizacion.getContinuity().getIncisoContinuities().getOnlyInProcess(fechaIniVigenciaEndoso);
		
		for (BitemporalInciso inciso : incisos) {
			
			bitemporalAutoIncisos.add(inciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().getOnlyInProcess(fechaIniVigenciaEndoso));
		
		}
		
		Collections.sort(bitemporalAutoIncisos, new Comparator<BitemporalAutoInciso>() {

			@Override
			public int compare(BitemporalAutoInciso obj1,
					BitemporalAutoInciso obj2) {
				
				return obj1.getContinuity().getParentContinuity().getNumero()
					.compareTo(obj2.getContinuity().getParentContinuity().getNumero());
			}
		});
		
		return bitemporalAutoIncisos;
		
	}
	
	private String obtenerNumeroPoliza(BigDecimal idCotizacion) {
		
		try {
		
			PolizaDTO poliza = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", idCotizacion).get(0);
					
			return poliza.getNumeroPolizaFormateada();
		
		} catch (Exception ex) {
			
			return "-";
			
		}
		
	}
	
	private DateTime obtenerFechaInicioVigencia(BigDecimal idCotizacion) {
		
		//Se obtiene la fecha de inicio de vigencia del endoso a partir de la solicitud del controlendosocot
		
		ControlEndosoCot controlEndoso = endosoService.obtenerControlEndosoCotCotizacion(idCotizacion);
		
		SolicitudDTO solicitud = controlEndoso.getSolicitud();
		
		return new DateTime(solicitud.getFechaInicioVigenciaEndoso());
		
	}
	
	private File leerArchivo(BigDecimal idToControlArchivo) {
		
		String nombreCompletoArchivo = null;
		ControlArchivoDTO controlArchivoDTO = entidadService.findById(ControlArchivoDTO.class, idToControlArchivo);
		String nombreOriginalArchivo = controlArchivoDTO.getNombreArchivoOriginal();
		String extension = (nombreOriginalArchivo.lastIndexOf(".") == -1) ? "" : nombreOriginalArchivo
				.substring(nombreOriginalArchivo.lastIndexOf("."), nombreOriginalArchivo.length());
		
		if (!extension.equalsIgnoreCase(".XLS") && !extension.equalsIgnoreCase(".XLSX"))
			throw new RuntimeException(Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
											"reaseguro.edocuentacfdi.validacion.extension.diferente"));
		
		nombreCompletoArchivo = controlArchivoDTO.getIdToControlArchivo().toString() + extension;
			
		return new File(sistemaContext.getUploadFolder() + nombreCompletoArchivo);
		
	}
	
	
	@EJB
	private EntidadBitemporalService entidadBitemporalService;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private EndosoService endosoService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	private static Logger logger = Logger.getLogger(CargaMasivaComplementarioServiceImpl.class);

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
}
