package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas2.dao.fuerzaventa.CedulaSubramoDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CedulaSubramo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.service.fuerzaventa.CedulaSubramoService;

@Stateless
public class CedulaSubramoServiceImpl implements CedulaSubramoService{
	private CedulaSubramoDao cedulaSubramoDao;

	@EJB
	public void setCedulaSubramoDao(CedulaSubramoDao cedulaSubramoDao) {
		this.cedulaSubramoDao = cedulaSubramoDao;
	}

	@Override
	public List<CedulaSubramo> findByFilters(CedulaSubramo arg0) {
		return cedulaSubramoDao.findByFilters(arg0);
	}

	@Override
	public CedulaSubramo loadById(CedulaSubramo arg0) {
		return cedulaSubramoDao.loadById(arg0);
	}

	@Override
	public CedulaSubramo saveFull(CedulaSubramo arg0) throws Exception {
		return cedulaSubramoDao.saveFull(arg0);		
	}

	@Override
	public List<RamoDTO> obtenerRamos() throws Exception {
		return cedulaSubramoDao.obtenerRamos();
	}

	@Override
	public List<SubRamoDTO> obtenerSubRamosPorRamo(RamoDTO ramo) throws Exception {
		return cedulaSubramoDao.obtenerSubRamosPorRamo(ramo);
	}
	
	@Override
	public List<CedulaSubramo> obtenerSubRamosAsociadosPorCedula(ValorCatalogoAgentes tipoCedula) throws Exception {
		return cedulaSubramoDao.obtenerSubRamosAsociadosPorCedula(tipoCedula);
	}
	
	@Override
	public void guardarAsociacion(List<SubRamoDTO> listaSubRamos,ValorCatalogoAgentes tipoCedula) throws Exception {
	   cedulaSubramoDao.guardarAsociacion(listaSubRamos, tipoCedula);		
	}
	
}
