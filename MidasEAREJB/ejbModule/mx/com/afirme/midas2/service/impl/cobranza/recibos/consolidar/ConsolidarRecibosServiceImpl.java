package mx.com.afirme.midas2.service.impl.cobranza.recibos.consolidar;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.cobranza.recibos.consolidar.ConsolidarRecibosDao;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.Consolidado;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.RecibosConsolidar;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.RelRecibosConsolidado;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.cobranza.recibos.consolidar.ConsolidarRecibosService;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
@Stateless
public class ConsolidarRecibosServiceImpl implements ConsolidarRecibosService {
	private static final Logger log = Logger.getLogger(ConsolidarRecibosServiceImpl.class);
	
	protected EntidadService entidadService;
	
	@EJB
	private ConsolidarRecibosDao consolidarRecibosDao;

	
	@Override
	public List<Consolidado> getlistRecibosConsolidados(Consolidado consolidado) {
		log.trace(">> getlistRecibosAConsolidar()");
		
		List<Consolidado> listapendiente = new ArrayList<Consolidado>();
		try {
			listapendiente=  consolidarRecibosDao.obtenerRecibosConsolidados(consolidado);
		} catch (Exception e) {
			log.error("Error al mandar llamar polizaConsolidarRecibosDao.obtenerListadoRecibos ", e);
		}
		
		log.trace("<< getlistRecibosAConsolidar()");
		return listapendiente;		
	}
	@Override
	public List<RecibosConsolidar> getlistRecibosAConsolidar(RecibosConsolidar reciboConsolidar) {
		log.trace(">> getlistRecibosAConsolidar()");
		
		List<RecibosConsolidar> listapendiente = new ArrayList<RecibosConsolidar>();
		try {
			listapendiente=  consolidarRecibosDao.obtenerListadoRecibos(reciboConsolidar);
		} catch (Exception e) {
			log.error("Error al mandar llamar polizaConsolidarRecibosDao.obtenerListadoRecibos ", e);
		}
		
		log.trace("<< getlistRecibosAConsolidar()");
		return listapendiente;		
	}
	
	/**
	 * Obtener recibos consolidados.
	 */
	
	@Override
	public List<Consolidado> cancelarRecibosSeleccionados(RecibosConsolidar reciboConsolidar) {
		log.trace(">> cancelarRecibosSeleccionados()");
		
		
		List<Consolidado> listapendiente = new ArrayList<Consolidado>();
		try {
				listapendiente=  consolidarRecibosDao.cancelarRecibosSeleccionados(reciboConsolidar);
		} catch (Exception e) {
			log.error("Error al mandar llamar consolidarRecibosDao.cancelarRecibosSeleccionados ", e);
		}
		log.trace("<< cancelarRecibosSeleccionados()");
		return listapendiente;
	}
	
	@Override
	public String validarRecibosAConsolidar(String receipts) {
		StoredProcedureHelper storedHelper = null;
		String descRespuesta = "";
		try {
			storedHelper = new StoredProcedureHelper("SEYCOS.PKG_COB_RECIBOS_CONSOLIDADOS.VALID_RECEIPTS");
			storedHelper.estableceParametro("pRecibos", receipts);
			storedHelper.ejecutaActualizar();
			descRespuesta = storedHelper.getDescripcionRespuesta();
		} catch (Exception e) {
			log.error("No se pudo validar los recibos "+receipts+" Excepcion: ", e);
		}
		return descRespuesta;
	}
	
	@Override
	public List<RelRecibosConsolidado> consolidarRecibosSeleccionados(RecibosConsolidar reciboConsolidar) {
		log.trace(">> consolidarRecibosSeleccionados()");
		
		
		List<RelRecibosConsolidado> listapendiente = new ArrayList<RelRecibosConsolidado>();
		try {
			
			String valid = this.validarRecibosAConsolidar(reciboConsolidar.getIdRecibosConsolidar());
			if(StringUtils.isBlank(valid)){
				listapendiente=  consolidarRecibosDao.consolidarRecibosSeleccionados(reciboConsolidar);
			}else{
				throw new MidasException(valid);
			}
			
				
		} catch (Exception e) {
			RelRecibosConsolidado  recibosConsolidar = new RelRecibosConsolidado();
			recibosConsolidar.setTieneError(true);
			recibosConsolidar.setDescripcionError(e.getMessage());
			listapendiente.add(recibosConsolidar);
			log.error("Error al mandar llamar consolidarRecibosDao.consolidarRecibosSeleccionados ", e);
		}
		log.trace("<< consolidarRecibosSeleccionados()");
		return listapendiente;
	}

	
	@Override
public Object correrCFDConsolidado (Consolidado consolidado){
		log.trace(">> correrCFDConsolidado()");
	
	
		log.trace("<< correrCFDConsolidado()");
	return consolidarRecibosDao.correrCFDConsolidado(consolidado);
}	
	
	


}
