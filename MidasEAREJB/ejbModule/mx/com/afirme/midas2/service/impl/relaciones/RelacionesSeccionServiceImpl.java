package mx.com.afirme.midas2.service.impl.relaciones;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.relaciones.RelacionesSeccionService;

@Stateless
public class RelacionesSeccionServiceImpl implements RelacionesSeccionService {

	@Override
	public List<Paquete> obtenerPaquetesAsociados(BigDecimal idSeccion) {
		SeccionDTO seccion = seccionFacade.findById(idSeccion);
		return seccion.getPaquetes();
	}

	@Override
	public List<Paquete> obtenerPaquetesDisponibles(BigDecimal idSeccion) {
		SeccionDTO seccion = seccionFacade.findById(idSeccion);
		List<Paquete> paquetesAsociados = seccion.getPaquetes();
		
		List<Paquete> paquetes = entidadService.findAll(Paquete.class);
		
		List<Paquete> paquetesDisponibles = new ArrayList<Paquete>();
		
		for (Paquete paquete : paquetes) {
			if (!paquetesAsociados.contains(paquete)) {
				paquetesDisponibles.add(paquete);
			}
		}
		
		return paquetesDisponibles;
	}

	@Override
	public RespuestaGridRelacionDTO relacionarPaquetes(String accion, BigDecimal idSeccion, Long idPaquete) {
		
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		
		SeccionDTO seccion = seccionFacade.findById(idSeccion);
		

		Paquete paquete = entidadService.findById(Paquete.class,idPaquete);
		
		if (accion.equals("inserted") || accion.equals("updated")) {
			
			if (!seccion.getPaquetes().contains(paquete)) {
				seccion.getPaquetes().add(paquete);
			}
			
		} else if (accion.equals("deleted")) {
			if (seccion.getPaquetes().contains(paquete)) {
				seccion.getPaquetes().remove(paquete);
			}
		}
		
		seccionFacade.update(seccion);
				
		//TODO Preparar la respuesta y regresarla		
		return null;
	}

	
	private SeccionFacadeRemote seccionFacade;
	
	private EntidadService entidadService;
	
	@EJB
	public void setSeccionFacade(SeccionFacadeRemote seccionFacade) {
		this.seccionFacade = seccionFacade;
	}
	
	@EJB
	public void setCatalogoService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
}
