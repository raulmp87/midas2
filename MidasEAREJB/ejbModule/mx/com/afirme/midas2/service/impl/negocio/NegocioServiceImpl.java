package mx.com.afirme.midas2.service.impl.negocio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.NegocioDao;
import mx.com.afirme.midas2.dao.negocio.agente.NegocioAgenteDao;
import mx.com.afirme.midas2.dao.negocio.producto.NegocioProductoDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;
import mx.com.afirme.midas2.domain.negocio.bonocomision.NegocioBonoComision;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;
import mx.com.afirme.midas2.domain.negocio.cliente.grupo.NegocioGrupoCliente;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioConfiguracionDerecho;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.mediopago.NegocioMedioPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.sumaasegurada.NegocioCobSumAse;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDesc;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDescId;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionId;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioMunicipio;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.agente.NegocioAgenteService;
import mx.com.afirme.midas2.service.negocio.cliente.ClienteAsociadoJPAService;
import mx.com.afirme.midas2.service.negocio.cliente.grupo.NegocioGrupoClienteService;
import mx.com.afirme.midas2.service.negocio.estadodescuento.NegocioEstadoDescuentoService;
import mx.com.afirme.midas2.service.negocio.zonacirculacion.NegocioEstadoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

@Stateless
public class NegocioServiceImpl implements NegocioService{
	
/* declaracion de Variables
 */

	protected NegocioProductoDao negocioProductoDao;
	protected EntidadDao entidadDao;
	protected EntidadService entidadService;
	private NegocioGrupoClienteService negocioGrupoClienteService;
	protected NegocioAgenteService negocioAgenteService;
	protected UsuarioService usuarioService;
	protected NegocioEstadoService negocioEstadoService;
	protected NegocioEstadoDescuentoService negocioEstadoDescuentoService;
	private NegocioAgenteDao negocioAgenteDao;
	public  static final String  Rol_Emisor_Interno="Rol_Emisor_Interno";
	
	@EJB
	private ClienteAsociadoJPAService clienteAsociadoJPAService;
	
	@EJB
	private AgenteMidasService agenteMidasService;
	
	@EJB(beanName="UsuarioServiceDelegate")
    public void setUsuarioService(UsuarioService usuarioService) {
          this.usuarioService = usuarioService;
    }
	
	@EJB
	public void setNegocioAgenteDao(NegocioAgenteDao negocioAgenteDao) {
		this.negocioAgenteDao = negocioAgenteDao;
	}
	
/* Segmento de Metodos Sobre Escritos
 */
	@Override
	public List<Negocio> findAll() {
		return negocioDao.findAll();
	}

    @Override
	public List<Negocio> findByFilters(Negocio filtroDescNegocio) {
		Usuario usuario = usuarioService.getUsuarioActual();
		if(usuario != null){
			if(usuario.isOutsider()){
				filtroDescNegocio.setAplicaUsuarioExterno(true);
			}else{
				filtroDescNegocio.setAplicaUsuarioInterno(true);
			}
		}
		return  negocioDao.findByFilters(filtroDescNegocio);
	}

	@Override
	public Negocio findByClave(Long clave) {
		Negocio filtroNegocio = new Negocio();
		filtroNegocio.setIdToNegocio(clave);
	
		return negocioDao.findById(clave);
	}
	@Override
	public Negocio findById(Long id) {
		return negocioDao.findById(id);
	}
	
	private NegocioDao negocioDao;	
	@EJB
	public void setGrupoDao(NegocioDao negocioDao) {
		this.negocioDao = negocioDao;
	}
	
	@EJB
	public void setNegocioEstadoService(NegocioEstadoService negocioEstadoService) {
		this.negocioEstadoService = negocioEstadoService;
	}

	@EJB
	public void setNegocioEstadoDescuentoService(NegocioEstadoDescuentoService negocioEstadoDescuentoService) {
		this.negocioEstadoDescuentoService = negocioEstadoDescuentoService;
	}
	
	@Override
	public List<NegocioAgente> listarNegocioAgentes(Long id) {
		return negocioDao.listarNegocioAgentes(id);		
	}
	
	@Override
	public Long guardar(Negocio negocio, String tipoAccion) throws NegocioEJBExeption{
		Long id = null;
			// Valida que el ptcDefault no sea mayor al maximo permitido
			if (negocio.getPctDescuentoDefault() != null && negocio.getPctDescuentoDefault() > negocio.getPctMaximoDescuento()){
				throw new NegocioEJBExeption("NE_16", "El descuento default no puede ser mayor al descuento m\u00e1ximo permitido");
			}
			// Valida la fecha fin vigencia contra la fecha de vigencia del negocio
			if(negocio.getFechaFinVigencia() != null && negocio.getFechaFinVigenciaFija() != null && negocio.getFechaFinVigenciaFija().compareTo(negocio.getFechaFinVigencia()) > 0){
				throw new NegocioEJBExeption("NE_7","La Fecha fin de vigencia fija es mayor que la fecha de fin del negocio");
			}
			negocio.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			id = (Long) entidadService.saveAndGetId(negocio);
			//Relacionar estados descuentos al negocio
			if(tipoAccion != null && tipoAccion.equals(TipoAccionDTO.getAgregarModificar())){
				NegocioEstadoDescuento negocioEstadoDescuento = new NegocioEstadoDescuento();
				negocioEstadoDescuento.setNegocio(negocio);
				negocioEstadoDescuentoService.relacionarNegocio(tipoAccion, negocioEstadoDescuento);
			}
		return id;
		 
	}

	@Override
	public void cambiarEstatus(Long idToNegocio, Negocio.EstatusNegocio estatus) {
		Negocio negocio = negocioDao.getReference(idToNegocio);
		negocio.setClaveEstatus(estatus.obtenerEstatus());
		if(estatus.obtenerEstatus().equals(Negocio.EstatusNegocio.ACTIVO.getObtenerEstatus())){
			negocio.setCodigoUsuarioActivacion(usuarioService.getUsuarioActual().getNombreUsuario());
			negocio.setFechaActivacion(new Date());
		}
		negocioDao.update(negocio);
		
	}
	
	@Override
	public void cambiarTipoPersona(Long idToNegocio, short tipoPersona) {
		Negocio negocio = negocioDao.getReference(idToNegocio);
		negocio.setTipoPersona(tipoPersona);
		negocioDao.update(negocio);
		
	}

	@Override
	public List<String> activarNegocio(Long idToNegocio) {
		List<String> validaciones = this.validarNegocioParaActivacion(idToNegocio);
		if(validaciones.isEmpty()){
			cambiarEstatus(idToNegocio, Negocio.EstatusNegocio.ACTIVO);
		}
		return validaciones;
	}

	@Override
	public List<String> validarNegocioParaActivacion(Long idToNegocio) {
		List<String> validaciones = new ArrayList<String>();
		Negocio negocio = negocioDao.getReference(idToNegocio);
		//Al menos un producto negocio
		//List<NegocioProducto> productos = negocio.getNegocioProductos();
		List<NegocioProducto> productos = negocioProductoDao.findByProperty("negocio.idToNegocio", idToNegocio);
		if(productos.isEmpty()){
			validaciones.add("Se requiere al menos un producto");
		}else{
			
			for(NegocioProducto producto : productos){
				//Al menos un medio de pago
				List<NegocioMedioPago> negocioMedioPago = entidadService.findByProperty(NegocioMedioPago.class, "negocioProducto.idToNegProducto", producto.getIdToNegProducto());
				if(negocioMedioPago.isEmpty()){
					validaciones.add("Se requiere al menos un medio de pago para " + producto.getProductoDTO().getDescripcion());
				}
				
				//Al menos una forma de pago
				if(producto.getNegocioFormaPagos().isEmpty()){
					validaciones.add("Se requiere al menos una forma de pago " + 
							producto.getProductoDTO().getDescripcion());
				}
				//Al menos un tipo de poliza				
				List<NegocioTipoPoliza> tiposPoliza = producto.getNegocioTipoPolizaList();
				if(tiposPoliza.isEmpty()){
					validaciones.add("Se requiere al menos un tipo de p\u00F3liza para " + 
							producto.getProductoDTO().getDescripcion());
				}else{
					for(NegocioTipoPoliza tipoPoliza : tiposPoliza){
						//Al menos una seccion
						List<NegocioSeccion> secciones = tipoPoliza.getNegocioSeccionList();
						if(secciones.isEmpty()){
							validaciones.add("Se requiere al menos una secci\u00F3n para el tipo de p\u00F3liza " + 
									tipoPoliza.getTipoPolizaDTO().getDescripcion());
						}else{
							
							for(NegocioSeccion seccion : secciones){
								//Al menos un tipo uso

							 List<NegocioTipoUso>  negocioTipoUso = entidadService.findByProperty(NegocioTipoUso.class, "negocioSeccion.idToNegSeccion", seccion.getIdToNegSeccion());

							   if(negocioTipoUso.isEmpty()){
							  	   validaciones.add("Se requiere al menos un tipo de Uso "+ tipoPoliza.getTipoPolizaDTO().getDescripcion());
							   } else{
								   validaciones.remove(negocioTipoUso);
							   }
							
								//Al menos un agrupador tarifa
								if(seccion.getNegocioAgrupadorTarifaSeccions().isEmpty()){
									validaciones.add("Se requiere al menos un agrupador de tarifa para la secci\u00F3n " + 
											seccion.getSeccionDTO().getDescripcion());
								}
								//Al menos un paquete
								List<NegocioPaqueteSeccion> paquetes = seccion.getNegocioPaqueteSeccionList();
								if(paquetes.isEmpty()){
									validaciones.add("Se requiere al menos un paquete para la secci\u00F3n " + 
											seccion.getSeccionDTO().getDescripcion());
								}else{
									for(NegocioPaqueteSeccion paquete : paquetes){
										//Al menos una cobertura
									  if(paquete.getNegocioCobPaqSeccionList().isEmpty()){
										 validaciones.add("Se requiere al menos una cobertura dentro del paquete "    + 
												paquete.getPaquete().getDescripcion()+ ". Asociados al Producto: " + 
												paquete.getNegocioSeccion().getNegocioTipoPoliza().
												getNegocioProducto().getProductoDTO().getDescripcion()+
												" Y L\u00EDnea de Negocio " +
												paquete.getNegocioSeccion().getSeccionDTO().getNombreComercial()) ;
										}
									}
								}
							}
						}
					}
				}
			}			
		}	
		//Derechos endosos		
		if(negocio.getNegocioDerechoEndosos().isEmpty()){
			validaciones.add("Se requiere al menos un derecho de endoso");
		}
		//Derechos polizas
		if(negocio.getNegocioDerechoPolizas().isEmpty()){
			validaciones.add("Se requiere al menos un derecho de p\u00f3liza");
		}	
			
		
		return validaciones;
	}

	@Override
	public List<String> copiarNegocio(Long idNegocio, String nombreNegocio,boolean copiarEnCascada,Date fechaFinVigencia) {
		Negocio negocioCopia = null;
		List<String> validaciones = new ArrayList<String>();
		if(idNegocio != null){
			Negocio negocioBase = negocioDao.getReference(idNegocio);
			negocioCopia = new Negocio();
			//Se obtienen los valores del negocio original

			//se inicializan relaciones del negocio copia
		  if(negocioBase.getClaveEstatus().intValue()!=0){
			negocioCopia.setDescripcionNegocio(nombreNegocio);
			negocioCopia.setClaveEstatus(BigDecimal.ZERO.shortValue());
			negocioCopia.setClaveManejaTarifaBase(negocioBase.getClaveManejaTarifaBase());
			negocioCopia.setClaveManejaUdi(negocioBase.getClaveManejaUdi());
			negocioCopia.setClaveNegocio(negocioBase.getClaveNegocio());
			negocioCopia.setClavePorcentajeMontoUdi(negocioBase.getClavePorcentajeMontoUdi());
			negocioCopia.setClaveUdiIncluyeIva(negocioBase.getClaveUdiIncluyeIva());
			negocioCopia.setDiasDiferimiento(negocioBase.getDiasDiferimiento());
			negocioCopia.setDiasPlazoUsuario(negocioBase.getDiasPlazoUsuario());
			negocioCopia.setDiasGraciaEndosoSubsecuentes(negocioBase.getDiasGraciaEndosoSubsecuentes());
			negocioCopia.setDiasGraciaPoliza(negocioBase.getDiasGraciaPoliza());
			negocioCopia.setDiasGraciaPolizaSubsecuentes(negocioBase.getDiasGraciaPolizaSubsecuentes());
			negocioCopia.setDiasRetroactividad(negocioBase.getDiasRetroactividad());
			negocioCopia.setFechaCreacion(new Date());
			negocioCopia.setFechaFinVigencia(fechaFinVigencia);
			negocioCopia.setFechaInicioVigencia(negocioBase.getFechaInicioVigencia());
			negocioCopia.setFormulaDividendos(negocioBase.getFormulaDividendos());
			negocioCopia.setPctMaximoDescuento(negocioBase.getPctMaximoDescuento());
			negocioCopia.setPctUdiAgente(negocioBase.getPctUdiAgente());
			negocioCopia.setPctUdiPromotor(negocioBase.getPctUdiPromotor());
			negocioCopia.setValorUdi(negocioBase.getValorUdi());
			negocioCopia.setVersionNegocio(negocioBase.getVersionNegocio());
			negocioCopia.setPctPrimaCeder(negocioBase.getPctPrimaCeder());
			negocioCopia.setAplicaUsuarioExterno(negocioBase.getAplicaUsuarioExterno());
			negocioCopia.setAplicaUsuarioInterno(negocioBase.getAplicaUsuarioInterno());
			negocioCopia.setAplicaEnlace(negocioBase.getAplicaEnlace());
			negocioCopia.setAplicaEndosoConductoCobro(negocioBase.getAplicaEndosoConductoCobro());
			negocioCopia.setAplicaValidacionPersonaMoral(negocioBase.getAplicaValidacionPersonaMoral());
			
			//se persiste el nuevo negocio
			negocioDao.persist(negocioCopia);
			
			//Se copian los grupos de clientes asociados al Negocio base
			this.copiarGruposDeClientes(negocioBase, negocioCopia);
			//Se copian los grupos de clientes asociados al Negocio base
			this.copiarAgentes(negocioBase, negocioCopia);
			//Se copian los clientes asociados al Negocio base
			this.copiarClientes(negocioBase, negocioCopia);
			//Se copian los derechos de endosos asociados al Negocio base
			this.copiarDerechosEndoso(negocioBase,negocioCopia);			
			//Se copian los derechos de poliza asociados al Negocio base
			this.copiarDerechosPoliza(negocioBase,negocioCopia);
			//copiarProductos
			this.copiarProductos(negocioBase,negocioCopia,copiarEnCascada);
			
			//Se copian los negocio Bonos asociados al Negocio Base
			this.copiarNegocioEstado(negocioBase,negocioCopia);
			
			//Se copian Los negocios Municipios Asociados al Negocio
			this.copiarNegocioMunicipio(negocioBase,negocioCopia);
			
			//Se copian Los Negocios Bonos Asociados al Negocio.
			this.copiarNegocioBono(negocioBase, negocioCopia);		
			
			//Se copian Los negocios Estados Descuentos asociados al Negocio
			this.copiarNegocioEstadoDescuento(negocioBase,negocioCopia);
			
			//Se copian Los Negocios Renovacio Asociados al Negocio.
			try{
				this.copiarNegocioRenovacion(negocioBase, negocioCopia);
			}catch(Exception e){
				e.printStackTrace();
			}
		  }else{
			   
			  validaciones.add("No es posible Copiar Negocio con estatus de Negociando");
		  }
		}
		
		
		return validaciones;
	}
	public Negocio copiarNegocioConfiguracionDerecho(NegocioPaqueteSeccion negPaqSec, NegocioPaqueteSeccion negPaqSecCopia){
		
		Negocio negocioCopia = negPaqSecCopia.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio();
		if(negPaqSec.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getConfiguracionesDerecho() == null)
			negPaqSec = entidadService.findById(NegocioPaqueteSeccion.class, negPaqSec.getIdToNegPaqueteSeccion());
		if(negPaqSec.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getConfiguracionesDerecho().size() > 0){
			Negocio negocioBase = negPaqSec.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio();
			List<NegocioConfiguracionDerecho> copiados = new ArrayList<NegocioConfiguracionDerecho>();
			NegocioConfiguracionDerecho negocioCopiaConfiguracionDerecho = null;
			for(NegocioConfiguracionDerecho negocioConfigDerecho: negocioBase.getConfiguracionesDerecho()){
				if(negocioConfigDerecho.getPaquete().getIdToNegPaqueteSeccion().equals(negPaqSec.getIdToNegPaqueteSeccion())){
					negocioCopiaConfiguracionDerecho = new NegocioConfiguracionDerecho();
					negocioCopiaConfiguracionDerecho.setNegocio(negocioCopia);
					negocioCopiaConfiguracionDerecho.setTipoPoliza(negPaqSecCopia.getNegocioSeccion().getNegocioTipoPoliza());
					negocioCopiaConfiguracionDerecho.setSeccion(negPaqSecCopia.getNegocioSeccion());
					negocioCopiaConfiguracionDerecho.setPaquete(negPaqSecCopia);
					negocioCopiaConfiguracionDerecho.setMoneda(negocioConfigDerecho.getMoneda());
					negocioCopiaConfiguracionDerecho.setTipoDerecho(negocioConfigDerecho.getTipoDerecho());
					negocioCopiaConfiguracionDerecho.setIdToNegDerecho(negocioConfigDerecho.getIdToNegDerecho());
					negocioCopiaConfiguracionDerecho.setImporteDerecho(negocioConfigDerecho.getImporteDerecho());
					negocioCopiaConfiguracionDerecho.setImporteDefault(negocioConfigDerecho.isImporteDefault());
					negocioCopiaConfiguracionDerecho.setTipoUsoVehiculo(negocioConfigDerecho.getTipoUsoVehiculo());
					negocioCopiaConfiguracionDerecho.setEstado(negocioConfigDerecho.getEstado());
					negocioCopiaConfiguracionDerecho.setMunicipio(negocioConfigDerecho.getMunicipio());
					negocioCopiaConfiguracionDerecho.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
					negocioCopiaConfiguracionDerecho.setFechaCreacion(new Date());
					copiados.add(negocioCopiaConfiguracionDerecho);
				}
			}
			negocioCopia.setConfiguracionesDerecho(copiados);
			entidadService.save(negocioCopia);
		}
		return negocioCopia;
	}
	public Negocio copiarProductos(Negocio negocioBase, Negocio negocioCopia, boolean copiarEnCasacada){
		if(negocioBase.getNegocioProductos()== null)
			negocioBase = negocioDao.getReference(negocioBase.getIdToNegocio());
		if(negocioBase.getNegocioProductos().size() > 0){
			List<NegocioProducto> copiados = new ArrayList<NegocioProducto>();
			NegocioProducto negocioCopiaProducto = null;
			for(NegocioProducto negocioProducto: negocioBase.getNegocioProductos()){
				negocioCopiaProducto = new NegocioProducto();
				//negocioCopiaProducto = negocioProducto;
				negocioCopiaProducto.setNegocioFormaPagos(negocioProducto.getNegocioFormaPagos());
				negocioCopiaProducto.setNegocioMedioPagos(negocioProducto.getNegocioMedioPagos());
				negocioCopiaProducto.setNegocioTipoPolizaList(negocioProducto.getNegocioTipoPolizaList());
				negocioCopiaProducto.setProductoDTO(negocioProducto.getProductoDTO());
				negocioCopiaProducto.setNegocio(negocioCopia);
				
				negocioProductoDao.persist(negocioCopiaProducto);
				if(copiarEnCasacada){
					//Se copian los medios de pago asociados al Negocio base
					this.copiarMediosDePago(negocioProducto, negocioCopiaProducto);
					//Se copian los medios de pago asociados al Negocio base
					this.copiarFormasDePago(negocioProducto, negocioCopiaProducto);
					//Copiar Tipos De Poliza
					this.copiarTiposDePoliza(negocioProducto, negocioCopiaProducto, copiarEnCasacada);
				}
				copiados.add(negocioCopiaProducto);
			}
			negocioCopia.setNegocioProductos(copiados);
		}
		
		return null;
	}
	public NegocioTipoPoliza copiarSecciones(
			NegocioTipoPoliza negocioTipoPolizaBase,
			NegocioTipoPoliza negocioTipoPolizaCopia, boolean copiarEnCasacada) {
		if(negocioTipoPolizaBase.getNegocioSeccionList()== null)
			negocioTipoPolizaBase = entidadDao.getReference(NegocioTipoPoliza.class,negocioTipoPolizaBase.getIdToNegTipoPoliza());
		if(negocioTipoPolizaBase.getNegocioSeccionList().size() > 0){
			negocioTipoPolizaCopia.setNegocioSeccionList(new ArrayList<NegocioSeccion>());
			NegocioSeccion copia = null;
			for(NegocioSeccion item: negocioTipoPolizaBase.getNegocioSeccionList()){
				copia = new NegocioSeccion();
				copia.setNegocioTipoPoliza(negocioTipoPolizaCopia);
				copia.setSeccionDTO(item.getSeccionDTO());
				copia.setConsultaNumSerie(item.getConsultaNumSerie());
				entidadDao.persist(copia);
				//Copiar Tipos de Uso
				this.copiarTiposUso(item, copia);
				//Copiar Tipos de Servicio
				this.copiarTiposServicio(item, copia);
				//Copiar Agrupadores de Tarifa
				this.copiarAgrupadoresDeTarifa(item, copia);
				//Copiar Estilos de Vehiculo				
				if(copiarEnCasacada){
					copiarEstilosVehiculo(item, copia);
					//Copiar Paquetes, Coberturas y Deducibles PT y PP
					copiarPaquetes(item, copia, copiarEnCasacada);
				}
				negocioTipoPolizaCopia.getNegocioSeccionList().add(copia);
			}
		}
		return negocioTipoPolizaBase;
	}
	
	public void copiarPaquetes(
			NegocioSeccion negocioSeccionOriginal,
			NegocioSeccion negocioSeccionCopia,
			boolean copiarEnCascada){
		if(negocioSeccionOriginal.getNegocioPaqueteSeccionList() == null){
			negocioSeccionOriginal = entidadDao.getReference(NegocioSeccion.class, negocioSeccionOriginal.getIdToNegSeccion());
		}
		if(!negocioSeccionOriginal.getNegocioPaqueteSeccionList().isEmpty()){
			negocioSeccionCopia.setNegocioPaqueteSeccionList(new ArrayList<NegocioPaqueteSeccion>());
			NegocioPaqueteSeccion negCobPaqSecCopia = null;
			for(NegocioPaqueteSeccion negPaqSec : negocioSeccionOriginal.getNegocioPaqueteSeccionList()){
				negCobPaqSecCopia = new NegocioPaqueteSeccion();
				negCobPaqSecCopia.setModeloAntiguedadMax(negPaqSec.getModeloAntiguedadMax());
				negCobPaqSecCopia.setNegocioSeccion(negocioSeccionCopia);
				negCobPaqSecCopia.setPaquete(negPaqSec.getPaquete());
				negCobPaqSecCopia.setAplicaPctDescuentoEdo(negPaqSec.getAplicaPctDescuentoEdo());
				entidadDao.persist(negCobPaqSecCopia);
				if(copiarEnCascada){
					//copiar NegocioCobPaqSeccion
					copiarCoberturasPaquete(negPaqSec, negCobPaqSecCopia, copiarEnCascada);
				}
			}
		}
	}
	
	private NegocioPaqueteSeccion copiarCoberturasPaquete(
			NegocioPaqueteSeccion negPaqSecOriginal,
			NegocioPaqueteSeccion negPaqSecCopia,
			boolean copiarEnCascada){
		if(negPaqSecOriginal.getNegocioCobPaqSeccionList() == null){
			negPaqSecOriginal = entidadDao.getReference(NegocioPaqueteSeccion.class, negPaqSecOriginal.getIdToNegPaqueteSeccion());
		}
		if(!negPaqSecOriginal.getNegocioCobPaqSeccionList().isEmpty()){
			NegocioCobPaqSeccion negCobPaqSecNuevo = null;
			negPaqSecCopia.setNegocioCobPaqSeccionList(new ArrayList<NegocioCobPaqSeccion>());
			for(NegocioCobPaqSeccion negocioCobPaqSeccion : negPaqSecOriginal.getNegocioCobPaqSeccionList()){
				negCobPaqSecNuevo = new NegocioCobPaqSeccion();
				negCobPaqSecNuevo.setCiudadDTO(negocioCobPaqSeccion.getCiudadDTO());
				negCobPaqSecNuevo.setClaveTipoDeduciblePP(negocioCobPaqSeccion.getClaveTipoDeduciblePP());
				negCobPaqSecNuevo.setClaveTipoDeduciblePT(negocioCobPaqSeccion.getClaveTipoDeduciblePT());
				negCobPaqSecNuevo.setClaveTipoSumaAsegurada(negocioCobPaqSeccion.getClaveTipoSumaAsegurada());
				negCobPaqSecNuevo.setCoberturaDTO(negocioCobPaqSeccion.getCoberturaDTO());
				negCobPaqSecNuevo.setEstadoDTO(negocioCobPaqSeccion.getEstadoDTO());
				negCobPaqSecNuevo.setMonedaDTO(negocioCobPaqSeccion.getMonedaDTO());
				negCobPaqSecNuevo.setNegocioPaqueteSeccion(negPaqSecCopia);
				negCobPaqSecNuevo.setValorSumaAseguradaDefault(negocioCobPaqSeccion.getValorSumaAseguradaDefault());
				negCobPaqSecNuevo.setValorSumaAseguradaMax(negocioCobPaqSeccion.getValorSumaAseguradaMax());
				negCobPaqSecNuevo.setValorSumaAseguradaMin(negocioCobPaqSeccion.getValorSumaAseguradaMin());
				negCobPaqSecNuevo.setTipoUsoVehiculo(negocioCobPaqSeccion.getTipoUsoVehiculo());
				negCobPaqSecNuevo.setAgente(negocioCobPaqSeccion.getAgente());
				negCobPaqSecNuevo.setRenovacion(negocioCobPaqSeccion.isRenovacion());
				negCobPaqSecNuevo.setDescuento(negocioCobPaqSeccion.getDescuento());
				
				entidadDao.persist(negCobPaqSecNuevo);
				
				copiarDeducibles(negocioCobPaqSeccion, negCobPaqSecNuevo);
				copiarSumasAseguradasDiscretas(negocioCobPaqSeccion, negCobPaqSecNuevo);
				
				negPaqSecCopia.getNegocioCobPaqSeccionList().add(negCobPaqSecNuevo);
			}
		}
		return negPaqSecCopia;
	}
	
	private void copiarDeducibles(
			NegocioCobPaqSeccion original,
			NegocioCobPaqSeccion copia){
		if(original.getNegocioDeducibleCobList() == null || original.getNegocioDeducibleCobList() == null){
			original = entidadDao.getReference(NegocioCobPaqSeccion.class, original.getIdToNegCobPaqSeccion());
		}
		if(!original.getNegocioDeducibleCobList().isEmpty()){
			copia.setNegocioDeducibleCobList(new ArrayList<NegocioDeducibleCob>());
			NegocioDeducibleCob nuevo = null;
			for(NegocioDeducibleCob viejo : original.getNegocioDeducibleCobList()){
				nuevo = new NegocioDeducibleCob();
				nuevo.setClaveDefault(viejo.getClaveDefault());
				nuevo.setNegocioCobPaqSeccion(copia);
				nuevo.setNumeroSecuencia(viejo.getNumeroSecuencia());
				nuevo.setValorDeducible(viejo.getValorDeducible());
				copia.getNegocioDeducibleCobList().add(nuevo);
			}
			entidadDao.update(copia);
		}
	}
	
	private void copiarSumasAseguradasDiscretas(
			NegocioCobPaqSeccion original,
			NegocioCobPaqSeccion copia){
		if(original.getNegocioCobSumAseList() == null ){
			original = entidadDao.getReference(NegocioCobPaqSeccion.class, original.getIdToNegCobPaqSeccion());
		}
		if(!original.getNegocioCobSumAseList().isEmpty()){
			copia.setNegocioDeducibleCobList(new ArrayList<NegocioDeducibleCob>());
			NegocioCobSumAse nuevo = null;
			for(NegocioCobSumAse viejo : original.getNegocioCobSumAseList()){
				nuevo = new NegocioCobSumAse();
				nuevo.setNegocioCobPaqSeccion(copia);
				nuevo.setValorSumaAsegurada(viejo.getValorSumaAsegurada());
				nuevo.setDefaultValor(viejo.getDefaultValor());
				nuevo.setNumeroSecuencia(viejo.getNumeroSecuencia());
				copia.getNegocioCobSumAseList().add(nuevo);
			}
			entidadDao.update(copia);
		}
	}
	
	public void copiarEstilosVehiculo(NegocioSeccion negocioSeccionOriginal, NegocioSeccion negocioSeccionCopia){
		if(negocioSeccionOriginal.getNegocioEstiloVehiculoList() == null){
			negocioSeccionOriginal = entidadDao.getReference(NegocioSeccion.class, negocioSeccionOriginal.getIdToNegSeccion());
		}
		if(!negocioSeccionOriginal.getNegocioEstiloVehiculoList().isEmpty()){
			negocioSeccionCopia.setNegocioEstiloVehiculoList(new ArrayList<NegocioEstiloVehiculo>());
			NegocioEstiloVehiculo negEVCopia = null;
			for(NegocioEstiloVehiculo negocioEstiloVehiculo : negocioSeccionOriginal.getNegocioEstiloVehiculoList()){
				negEVCopia = new NegocioEstiloVehiculo();
				negEVCopia.setEstiloVehiculoDTO(negocioEstiloVehiculo.getEstiloVehiculoDTO());
				negEVCopia.setNegocioSeccion(negocioSeccionCopia);
				negocioSeccionCopia.getNegocioEstiloVehiculoList().add(negEVCopia);
			}
			entidadDao.update(negocioSeccionCopia);
		}
	}

	public NegocioSeccion copiarAgrupadoresDeTarifa(NegocioSeccion negocioSeccionBase,
			NegocioSeccion negocioSeccionCopia) {
		if(negocioSeccionBase.getNegocioAgrupadorTarifaSeccions()== null)
			negocioSeccionBase = entidadDao.getReference(NegocioSeccion.class, negocioSeccionBase.getIdToNegSeccion());
		if(negocioSeccionBase.getNegocioAgrupadorTarifaSeccions().size() > 0){
			List<NegocioAgrupadorTarifaSeccion> copiados = new ArrayList<NegocioAgrupadorTarifaSeccion>();
			NegocioAgrupadorTarifaSeccion copia = null;
			for(NegocioAgrupadorTarifaSeccion item: negocioSeccionBase.getNegocioAgrupadorTarifaSeccions()){
				copia = new NegocioAgrupadorTarifaSeccion();
				copia.setIdMoneda(item.getIdMoneda());
				copia.setNegocioSeccion(negocioSeccionCopia);
				copia.setIdToAgrupadorTarifa(item.getIdToAgrupadorTarifa());
				copia.setIdVerAgrupadorTarifa(item.getIdVerAgrupadorTarifa());
				entidadDao.persist(copia);
				copiados.add(copia);
			}
			negocioSeccionCopia.setNegocioAgrupadorTarifaSeccions(copiados);
		}
		return negocioSeccionCopia;
	}	

	public NegocioSeccion copiarTiposServicio(NegocioSeccion negocioSeccionBase,
			NegocioSeccion negocioSeccionCopia) {
		if(negocioSeccionBase.getNegocioTipoServicioList()== null)
			negocioSeccionBase = entidadDao.getReference(NegocioSeccion.class, negocioSeccionBase.getIdToNegSeccion());
		if(negocioSeccionBase.getNegocioTipoServicioList().size() > 0){
			List<NegocioTipoServicio> copiados = new ArrayList<NegocioTipoServicio>();
			NegocioTipoServicio copia = null;
			for(NegocioTipoServicio item: negocioSeccionBase.getNegocioTipoServicioList()){
				copia = new NegocioTipoServicio();
				copia.setClaveDefault(item.getClaveDefault());
				copia.setNegocioSeccion(negocioSeccionCopia);
				copia.setTipoServicioVehiculoDTO(item.getTipoServicioVehiculoDTO());
				entidadDao.persist(copia);
				copiados.add(copia);
			}
			negocioSeccionCopia.setNegocioTipoServicioList(copiados);
		}
		return negocioSeccionCopia;
	}
	
	public NegocioSeccion copiarTiposUso(NegocioSeccion negocioSeccionBase,
			NegocioSeccion negocioSeccionCopia) {
		if(negocioSeccionBase.getNegocioTipoUsoList()== null)
			negocioSeccionBase = entidadDao.getReference(NegocioSeccion.class, negocioSeccionBase.getIdToNegSeccion());
		if(negocioSeccionBase.getNegocioTipoUsoList().size() > 0){
			List<NegocioTipoUso> copiados = new ArrayList<NegocioTipoUso>();
			NegocioTipoUso copia = null;
			for(NegocioTipoUso item: negocioSeccionBase.getNegocioTipoUsoList()){
				copia = new NegocioTipoUso();
				copia.setClaveDefault(item.getClaveDefault());
				copia.setNegocioSeccion(negocioSeccionCopia);
				copia.setTipoUsoVehiculoDTO(item.getTipoUsoVehiculoDTO());
				entidadDao.persist(copia);
				copiados.add(copia);
			}
			negocioSeccionCopia.setNegocioTipoUsoList(copiados);
		}
		return negocioSeccionCopia;
	}
	
	public NegocioProducto copiarTiposDePoliza(
			NegocioProducto negocioProductoBase,
			NegocioProducto negocioProductoCopia, boolean copiarEnCasacada) {
		if(negocioProductoBase.getNegocioTipoPolizaList()== null)
			negocioProductoBase = entidadDao.getReference(NegocioProducto.class,negocioProductoBase.getIdToNegProducto());
		if(negocioProductoBase.getNegocioTipoPolizaList().size()>0){
			List<NegocioTipoPoliza> copiados = new ArrayList<NegocioTipoPoliza>();
			NegocioTipoPoliza copia = null;
			for(NegocioTipoPoliza item: negocioProductoBase.getNegocioTipoPolizaList()){
				copia = new NegocioTipoPoliza();
				copia.setNegocioProducto(negocioProductoCopia);
				copia.setTipoPolizaDTO(item.getTipoPolizaDTO());
				entidadDao.persist(copia);
				if(copiarEnCasacada){
					//Copiar Secciones /Lineas
					copiarSecciones(item, copia, copiarEnCasacada);
				}
				copiados.add(copia);				
			}
			negocioProductoCopia.setNegocioTipoPolizaList(copiados);
		}
		return negocioProductoCopia;
	}
	
	public NegocioProducto copiarFormasDePago(NegocioProducto negocioProductoBase, NegocioProducto negocioProductoCopia){
		if(negocioProductoBase.getNegocioFormaPagos()== null)
			negocioProductoBase = entidadDao.getReference(NegocioProducto.class,negocioProductoBase.getIdToNegProducto());
		if(negocioProductoBase.getNegocioFormaPagos().size()>0){
			List<NegocioFormaPago> copiados =new ArrayList<NegocioFormaPago>();
			NegocioFormaPago copia= null;
			for(NegocioFormaPago item: negocioProductoBase.getNegocioFormaPagos()){
				copia = new NegocioFormaPago();
				copia.setFormaPagoDTO(item.getFormaPagoDTO());
				copia.setNegocioProducto(negocioProductoCopia);
				entidadDao.persist(copia);
				copiados.add(copia);
			}
			negocioProductoCopia.setNegocioFormaPagos(copiados);
		}
		return negocioProductoCopia;		
	}	
	public NegocioProducto copiarMediosDePago(NegocioProducto negocioProductoBase, NegocioProducto negocioProductoCopia){
		if(negocioProductoBase.getNegocioMedioPagos()== null)
			negocioProductoBase = entidadDao.getReference(NegocioProducto.class,negocioProductoBase.getIdToNegProducto());
		if(negocioProductoBase.getNegocioMedioPagos().size()>0){
			List<NegocioMedioPago> copiados =new ArrayList<NegocioMedioPago>();
			NegocioMedioPago copia= null;
			for(NegocioMedioPago item: negocioProductoBase.getNegocioMedioPagos()){
				copia = new NegocioMedioPago();
				copia.setMedioPagoDTO(item.getMedioPagoDTO());
				copia.setNegocioProducto(negocioProductoCopia);
				entidadDao.persist(copia);
				copiados.add(copia);
			}
			negocioProductoCopia.setNegocioMedioPagos(copiados);
		}
		return negocioProductoCopia;		
	}
	public Negocio copiarDerechosPoliza(Negocio negocioBase, Negocio negocioCopia){
		
		if(negocioBase.getNegocioDerechoPolizas()== null)
			negocioBase = negocioDao.getReference(negocioBase.getIdToNegocio());
		if(negocioBase.getNegocioDerechoPolizas().size()>0){
			List<NegocioDerechoPoliza> copiados =new ArrayList<NegocioDerechoPoliza>();
			NegocioDerechoPoliza copia= null;
			for(NegocioDerechoPoliza item: negocioBase.getNegocioDerechoPolizas()){
				copia = new NegocioDerechoPoliza();
				copia.setImporteDerecho(item.getImporteDerecho());
				copia.setClaveDefault(item.getClaveDefault());
				copia.setNegocio(negocioCopia);
				copia.setActivo(item.getActivo());
				entidadDao.persist(copia);
				copiados.add(copia);
			}
			negocioCopia.setNegocioDerechoPolizas(copiados);
		}
		return negocioCopia;
		
	}		
	public Negocio copiarDerechosEndoso(Negocio negocioBase, Negocio negocioCopia){
		
		if(negocioBase.getNegocioDerechoEndosos()== null)
			negocioBase = negocioDao.getReference(negocioBase.getIdToNegocio());
		if(negocioBase.getNegocioDerechoEndosos().size()>0){
			List<NegocioDerechoEndoso> copiados =new ArrayList<NegocioDerechoEndoso>();
			NegocioDerechoEndoso copia= null;
			for(NegocioDerechoEndoso item: negocioBase.getNegocioDerechoEndosos()){
				copia = new NegocioDerechoEndoso();
				copia.setImporteDerecho(item.getImporteDerecho());
				copia.setClaveDefault(item.getClaveDefault());
				copia.setNegocio(negocioCopia);
				copia.setActivo(item.getActivo());
				copia.setEsAltaInciso(item.getEsAltaInciso());
				entidadDao.persist(copia);
				copiados.add(copia);
			}
			negocioCopia.setNegocioDerechoEndosos(copiados);
		}
		return negocioCopia;
		
	}	
	public Negocio copiarClientes(Negocio negocioBase, Negocio negocioCopia){
		
		if(negocioBase.getNegocioClientes() == null)
			negocioBase = negocioDao.getReference(negocioBase.getIdToNegocio());
		if(negocioBase.getNegocioClientes().size()>0){
			List<NegocioCliente> copiados =new ArrayList<NegocioCliente>();
			NegocioCliente copia= null;
			for(NegocioCliente item: negocioBase.getNegocioClientes()){
				copia = new NegocioCliente();
				copia.setIdCliente(item.getIdCliente());
				copia.setNegocio(negocioCopia);
				entidadDao.persist(copia);
				copiados.add(copia);
			}
			negocioCopia.setNegocioClientes(copiados);
		}
		return negocioCopia;
		
	}		
	public Negocio copiarAgentes(Negocio negocioBase, Negocio negocioCopia){
		List<NegocioAgente> negAgenteOrigen = entidadService.findByProperty(NegocioAgente.class, "negocio.idToNegocio", negocioBase.getIdToNegocio());
		
		negocioCopia.setNegocioAgentes(new ArrayList<NegocioAgente>());
		
		NegocioAgente copia= null;
		for(NegocioAgente item: negAgenteOrigen){
			copia = new NegocioAgente();
			copia.setIdTcAgente(item.getIdTcAgente());
			copia.setNegocio(negocioCopia);
			negocioCopia.getNegocioAgentes().add(copia);
		}
		
		//persistir en cascada
		negocioDao.update(negocioCopia);
		
		return negocioCopia;
		
	}	
	public Negocio copiarGruposDeClientes(Negocio negocioBase, Negocio negocioCopia){
		List<NegocioGrupoCliente> listaGposCteNeg = negocioGrupoClienteService.listarPorNegocio(negocioBase.getIdToNegocio());
		
		negocioCopia.setGrupoCliente(new ArrayList<NegocioGrupoCliente>());
		
		//se copian los grupos de clientes
		NegocioGrupoCliente grupoCopia= null;
		for(NegocioGrupoCliente grupo: listaGposCteNeg){
			grupoCopia = new NegocioGrupoCliente();
			grupoCopia.setGrupoClientes(grupo.getGrupoClientes());
			grupoCopia.setNegocio(negocioCopia);
			negocioCopia.getGrupoCliente().add(grupoCopia);
		}
		//persistir en cascada
		negocioDao.update(negocioCopia);
		
		return negocioCopia;
		
	}
	
		//Copiar NegocioEstado.
	public Negocio copiarNegocioEstado(Negocio negocioBase, Negocio negocioCopia){
			List<NegocioEstado> negocioEstado = entidadService.findByProperty(NegocioEstado.class, "negocio.idToNegocio", negocioBase.getIdToNegocio());
	   if(!negocioEstado.isEmpty()){	
			negocioCopia.setNegocioEstadoList(new ArrayList<NegocioEstado>());
			NegocioEstado copia= null;
		 for(NegocioEstado item: negocioEstado){
				copia = new NegocioEstado();
				copia.setEstadoDTO(item.getEstadoDTO());
				copia.setNegocio(negocioCopia);
				negocioCopia.getNegocioEstadoList().add(copia);
		 }
			entidadService.save(negocioCopia);
	   }
		return negocioCopia;
	 }
	
	
		//Copiar NegocioMunicipio. 
  public Negocio copiarNegocioMunicipio(Negocio negocioBase, Negocio negocioCopia){
	 List<NegocioEstado> negocioEstado = entidadService.findByProperty(NegocioEstado.class, "negocio.idToNegocio", negocioCopia.getIdToNegocio());
	 List<NegocioEstado> negocioEstadoSet = entidadService.findByProperty(NegocioEstado.class, "negocio.idToNegocio", negocioBase.getIdToNegocio());
	 if(!negocioEstado.isEmpty()){		
	   if (negocioCopia!=null){
	 		NegocioMunicipio copia= null;
		 for(NegocioEstado items: negocioEstado){
		   for(NegocioEstado estado: negocioEstadoSet){
			   if(items.getEstadoDTO().getStateId().equals(estado.getEstadoDTO().getStateId())){
				   List<NegocioMunicipio> negocioMunicipio = entidadService.findByProperty(NegocioMunicipio.class, "negocioEstado.id",estado.getId());
				     if(!negocioMunicipio.isEmpty()){ 
						    for(NegocioMunicipio item: negocioMunicipio){
								copia = new NegocioMunicipio();
								copia.setMunicipioDTO(item.getMunicipioDTO());
								copia.setNegocioEstado(entidadService.getReference(NegocioEstado.class, items.getId()));				 	
								entidadService.save(copia);
						  }   
				     }
			   }
			}
		  }
		}
	  }
	     return negocioCopia;
  }
	  

	
	
	//Copiar NegocioBono.
	public Negocio copiarNegocioBono(Negocio negocioBase, Negocio negocioCopia){
		 NegocioBonoComision negocioBono= new NegocioBonoComision();
		 NegocioBonoComision negocioBonoCopia=new NegocioBonoComision();
		 negocioBase = entidadService.findById(Negocio.class, negocioBase.getIdToNegocio());
		 if(negocioBase.getNegocioBonoComision() !=null){	 
			 negocioBono = entidadService.findById(NegocioBonoComision.class,negocioBase.getNegocioBonoComision().getId());
			 negocioBonoCopia.setNegocio(entidadService.findById(Negocio.class, negocioCopia.getIdToNegocio()));
			 negocioBonoCopia.setClaveBono(negocioBono.getClaveBono());
			 negocioBonoCopia.setClaveDerechos(negocioBono.getClaveDerechos());
			 negocioBonoCopia.setClaveIncluyeIVASobreomision(negocioBono.getClaveIncluyeIVASobreomision());
			 negocioBonoCopia.setClavePorcentajeImporteBono(negocioBono.getClavePorcentajeImporteBono());
			 negocioBonoCopia.setClavePorcentajeImporteDerechos(negocioBono.getClavePorcentajeImporteDerechos());
			 negocioBonoCopia.setClavePorcentajeImporteUDI(negocioBono.getClavePorcentajeImporteUDI());
			 negocioBonoCopia.setClaveSobreComision(negocioBono.getClaveSobreComision());
			 negocioBonoCopia.setClaveUDI(negocioBono.getClaveUDI());
			 negocioBonoCopia.setComentariosBono(negocioBono.getComentariosBono());
			 negocioBonoCopia.setComentariosDerechos(negocioBono.getComentariosDerechos());
			 negocioBonoCopia.setComentariosSobreComision(negocioBono.getComentariosSobreComision());
			 negocioBonoCopia.setComentariosUDI(negocioBono.getComentariosUDI());
			 negocioBonoCopia.setImporteBono(negocioBono.getImporteBono());
			 negocioBonoCopia.setImporteDerechos(negocioBono.getImporteDerechos());
			 negocioBonoCopia.setImporteUDI(negocioBono.getImporteUDI());
			 negocioBonoCopia.setPctAgenteBono(negocioBono.getPctAgenteBono());
			 negocioBonoCopia.setPctAgenteDerechos(negocioBono.getPctAgenteDerechos());
			 negocioBonoCopia.setPctAgenteSobreComision(negocioBono.getPctAgenteSobreComision());
			 negocioBonoCopia.setPctAgenteUDI(negocioBono.getPctAgenteUDI());
			 negocioBonoCopia.setPctDerechos(negocioBono.getPctDerechos());
			 negocioBonoCopia.setPctPromotorBono(negocioBono.getPctPromotorBono());
			 negocioBonoCopia.setPctPromotorDerechos(negocioBono.getPctPromotorDerechos());
			 negocioBonoCopia.setPctPromotorSobreComision(negocioBono.getPctPromotorSobreComision());
			 negocioBonoCopia.setPctPromotorUDI(negocioBono.getPctPromotorUDI());
			 

			 negocioBonoCopia.setPctSobreComision(negocioBono.getPctSobreComision());
			 negocioBonoCopia.setPctBono(negocioBono.getPctBono());
			 negocioBonoCopia.setPctDerechos(negocioBono.getPctDerechos());
			 negocioBonoCopia.setPctUDI(negocioBono.getPctUDI());
				
			//persistir en cascada
			entidadService.save(negocioBonoCopia);
		 }	
		return negocioCopia;
		
	}	
	
	//Copiar NegocioEstadoDescuento.
	public Negocio copiarNegocioEstadoDescuento(Negocio negocioBase, Negocio negocioCopia){
			List<NegocioEstadoDescuento> negocioEstadoDescuento = entidadService.findByProperty(NegocioEstadoDescuento.class, "negocio.idToNegocio", negocioBase.getIdToNegocio());
	   if(!negocioEstadoDescuento.isEmpty()){	
			negocioCopia.setNegocioEstadoDescuentoList(new ArrayList<NegocioEstadoDescuento>());
			NegocioEstadoDescuento copia= null;
		 for(NegocioEstadoDescuento item: negocioEstadoDescuento){
				copia = new NegocioEstadoDescuento();
				copia.setZipCode(item.getZipCode());
				copia.setZona(item.getZona());
				copia.setPctDescuento(item.getPctDescuento());
				copia.setPctDescuentoDefault(item.getPctDescuentoDefault());
				copia.setEstadoDTO(item.getEstadoDTO());
				copia.setNegocio(negocioCopia);
				negocioCopia.getNegocioEstadoDescuentoList().add(copia);
		 }
			entidadService.save(negocioCopia);
	   }
		return negocioCopia;
	 }
	
	public Negocio copiarNegocioRenovacion(Negocio negocioBase, Negocio negocioCopia){
		
		//Obtiene los negocioRenovacion
		List<NegocioRenovacion> negocioRenovacionList = entidadService.findByProperty(NegocioRenovacion.class, "id.idToNegocio", negocioBase.getIdToNegocio());
		if(negocioRenovacionList != null && !negocioRenovacionList.isEmpty()){
			for(NegocioRenovacion item : negocioRenovacionList){
				NegocioRenovacion copia = new NegocioRenovacion();
				NegocioRenovacionId copiaId = new NegocioRenovacionId();
				copiaId.setIdToNegocio(negocioCopia.getIdToNegocio());
				copiaId.setTipoRiesgo(item.getId().getTipoRiesgo());
				
				copia.setId(copiaId);
				copia.setAplicaDeducibleDanos(item.getAplicaDeducibleDanos());
				copia.setAplicaDescuentoNoSinietro(item.getAplicaDescuentoNoSinietro());
				copia.setAplicarPrimaAnterior(item.getAplicarPrimaAnterior());
				copia.setAumentarPctTarifa(item.getAumentarPctTarifa());
				copia.setDetenerNumeroIncisos(item.getDetenerNumeroIncisos());
				copia.setLimiteIncrementoTarifa(item.getLimiteIncrementoTarifa());
				copia.setLimiteSiniestros(item.getLimiteSiniestros());
				copia.setMantieneCesionComision(item.getMantieneCesionComision());
				copia.setMantieneDescuentoComercial(item.getMantieneDescuentoComercial());
				copia.setNumeroIncisos(item.getNumeroIncisos());
				copia.setOtorgaDescuentoComercial(item.getOtorgaDescuentoComercial());
				copia.setPctLimiteIncremento(item.getPctLimiteIncremento());
				copia.setPctTarifa(item.getPctTarifa());
				copia.setRenuevaPoliza(item.getRenuevaPoliza());
				copia.setSinLimiteSinietros(item.getSinLimiteSinietros());
				copia.setValorDescuentoComercial(item.getValorDescuentoComercial());
				
				entidadDao.persistAndReturnId(copia);
			}
		}
		
		//Obtiene tablas de siniestro/renovacion
		List<NegocioRenovacionDesc> listaNegocioRenovacionDesc = entidadService.findByProperty(NegocioRenovacionDesc.class, "id.idToNegocio", negocioBase.getIdToNegocio());
		if(listaNegocioRenovacionDesc != null && !listaNegocioRenovacionDesc.isEmpty()){
			for(NegocioRenovacionDesc item : listaNegocioRenovacionDesc){
				NegocioRenovacionDesc copia = new NegocioRenovacionDesc();
				NegocioRenovacionDescId copiaId = new NegocioRenovacionDescId();
				
				copiaId.setIdToNegocio(negocioCopia.getIdToNegocio());
				copiaId.setNumSiniestros(item.getId().getNumSiniestros());
				copiaId.setTipoDescuento(item.getId().getTipoDescuento());
				copiaId.setTipoRiesgo(item.getId().getTipoRiesgo());
				
				copia.setId(copiaId);
				copia.setRen1(item.getRen1());
				copia.setRen2(item.getRen2());
				copia.setRen3(item.getRen3());
				copia.setRen4(item.getRen4());
				copia.setRen5(item.getRen5());
				copia.setRen6(item.getRen6());
				
				entidadDao.persist(copia);
			}
		}
		return negocioCopia;
	}
		
	@EJB
	public void setNegocioProductoDao(
			NegocioProductoDao negocioProductoDao) {
		this.negocioProductoDao = negocioProductoDao;
	}
	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setNegocioGrupoClienteService(
			NegocioGrupoClienteService negocioGrupoClienteService) {
		this.negocioGrupoClienteService = negocioGrupoClienteService;
	}
	
	@EJB
	public void setNegocioAgenteService(NegocioAgenteService negocioAgenteService){
		this.negocioAgenteService = negocioAgenteService;
	}

	@Override
	public List<Negocio> listarNegociosNoBorrados(String arg0) {
		return negocioDao.listarNegociosNoBorrados(arg0);
	}

	@Override
	public List<Negocio> listarNegociosSinAgentes(String claveNegocio, Boolean esExterno) {
		List<Negocio> negociosLibresList = negocioDao.listarNegocioSinAgentes(claveNegocio, esExterno);
		return negociosLibresList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Negocio> listarNegociosPorAgenteUnionNegociosLibres(Integer idTcAgente, String claveNegocio){
		//Comentario integracion
		Usuario usuario = usuarioService.getUsuarioActual();
		Agente agente = entidadService.findById(Agente.class, new Long(idTcAgente));//usuarioService.getAgenteUsuarioActual();
		Boolean esExterno = null;
		if(usuario != null){
			esExterno = usuario.isOutsider();
		}
		List<Negocio> union = null;
		List<Negocio> negociosLibresList = this.listarNegociosSinAgentes(claveNegocio, esExterno);
		List<Negocio> negociosPorAgente = negocioAgenteService.listarNegociosPorAgente(idTcAgente,claveNegocio, 1, esExterno);
		if(negociosLibresList != null && negociosPorAgente != null){
			union = (List<Negocio>)CollectionUtils.union(negociosPorAgente, negociosLibresList);
			if(!union.isEmpty() && union.size() > 1){
				Collections.sort(union, 
						new Comparator<Negocio>() {				
							public int compare(Negocio n1, Negocio n2){
								return n1.getDescripcionNegocio().compareTo(n2.getDescripcionNegocio());
							}
						});
			}
		}else{
			union = new ArrayList<Negocio>();			
		}
		
		 if (agente != null && agenteMidasService.isSucursal(agente.getIdAgente().intValue())) {
			 if(!usuarioService.tieneRolUsuarioActual(Rol_Emisor_Interno)) {
				 return eliminarNegocioNoAplicaSucursal(union);
			 }
		 }
		return union;
	}

	@Override
	public List<Negocio> eliminarNegocioNoAplicaSucursal(List<Negocio> union) {
		List<Negocio> unionAplicaSucursal = new ArrayList<Negocio>(union);
		for(Negocio item: union){
			if(!item.getAplicaSucursal() ){
				unionAplicaSucursal.remove(item);		
			}			 
		}
		return unionAplicaSucursal;
	}
	
	@Override
	public boolean isAgenteTienePermitidoNegocio(Agente agente, Negocio negocio) {
		if (!negocio.isNegocioActivoVigente()) {
			//El negocio no se encuentra activo y/o vigente asi que ningun agente puede hacer uso de el.
			return false;
		}
		long totalNegocioAgente = negocioAgenteDao.getTotalNegocioAgente(negocio);
		boolean negocioSinAgentesAsignados = totalNegocioAgente == 0;
		if (negocioSinAgentesAsignados) {
			return true;
		}
		NegocioAgente negocioAgente = negocioAgenteDao.findByNegocioAndAgente(negocio, agente);
		if (negocioAgente != null) {
			return true;
		}
		return false;
	}

	@Override
	public List<String> eliminarNegocio(Long id) {
		List<String> errores = new ArrayList<String>();
		Negocio negocio = entidadDao.findById(Negocio.class, id);
		if(negocio.getClaveEstatus().shortValue() != Negocio.EstatusNegocio.CREADO.obtenerEstatus().shortValue()){
			errores.add("Solo se permite eliminar negocios con estatus de Negociando");
		}else{
			cambiarEstatus(id, Negocio.EstatusNegocio.BORRADO);
		}
		return errores;	
	}
	
	@Override
	public boolean isDuplicado(Negocio negocio) {
		Negocio filtro = new Negocio();

		boolean esIgual=false;
		List<Negocio> items = negocioDao.findByFilters(filtro);
		for(Negocio item: items){		
		 if (item.getClaveEstatus().shortValue() != Negocio.EstatusNegocio.BORRADO.obtenerEstatus().shortValue() &&
			negocio.getDescripcionNegocio().equalsIgnoreCase(item.getDescripcionNegocio())) {
			 if(negocio.getIdToNegocio()==null){
				 esIgual=true;
				 break;
			 }else{
				 if(negocio.getIdToNegocio().intValue()==item.getIdToNegocio().intValue()){
					 esIgual = false;
					 break;
				 }else{
					 esIgual=true;
					 break;
				 }
			 }
		 }else{
			 esIgual=false; 
		 }
	   }
		return esIgual;
	}	
	
	@Override 
	public boolean isDuplicado(String DescripcionNegocio) {
		Negocio filtro = new Negocio();

		boolean esIgual=false;
		List<Negocio> items = negocioDao.findByFilters(filtro);
		for(Negocio item: items){
			
	
		 if (item.getClaveEstatus().shortValue() != Negocio.EstatusNegocio.BORRADO.obtenerEstatus().shortValue() && 
		     DescripcionNegocio.equalsIgnoreCase(item.getDescripcionNegocio())) {	
			 esIgual=true;
			 break;
		 }else{
			 esIgual=false; 
		 }
		
		}
		return esIgual;
	}


	@Override
	public List<Negocio> listarNegociosPorUsuario(String claveNegocio) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	/**
	 * Lista los clientes asociados a un negocio 
	 * que esten en grupos o clientes individuales
	 * @param idToNegocio
	 * @return listado con la union de los dos tipos cientes iindividuales y grupo de clinetes
	 * @author martin
	 */
	@Override
	public List<NegocioCliente> listarClientesAsociados(Long idToNegocio) {
		List<NegocioCliente> resultAsociadosList = clienteAsociadoJPAService.listarClientesAsociados(idToNegocio);
		return resultAsociadosList;
	}
	
	/**
	 * Busca los clientes que esten repetidos
	 * @param idCliente
	 * @param unionClientes
	 * @return clientes que no esten repeditos
	 * @author martin
	 */
	public NegocioCliente getClientesFiltrados(final BigDecimal idCliente,final List<NegocioCliente> unionClientes){
		Predicate predicate = new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				NegocioCliente negocioCliente = (NegocioCliente)arg0;
				if(negocioCliente.getIdCliente().equals(idCliente)){
					return true;
				}
				return false;
			}
		};		
		return (NegocioCliente)CollectionUtils.find(unionClientes, predicate);
	}

}
