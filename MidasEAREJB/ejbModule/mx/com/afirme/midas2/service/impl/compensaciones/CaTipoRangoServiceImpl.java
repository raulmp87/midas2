/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoRangoDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoRango;
import mx.com.afirme.midas2.service.compensaciones.CaTipoRangoService;

import org.apache.log4j.Logger;

@Stateless

public class CaTipoRangoServiceImpl  implements CaTipoRangoService {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@EJB
	private CaTipoRangoDao tipoRangocaDao;

//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaTipoRangoServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaTipoRango entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoRango entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoRango entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoRango 	::		CaTipoRangoServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	tipoRangocaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoRango 	::		CaTipoRangoServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoRango 	::		CaTipoRangoServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaTipoRango entity.
	  @param entity CaTipoRango entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoRango entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoRango 	::		CaTipoRangoServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaTipoRango.class, entity.getId());
	        	tipoRangocaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoRango 	::		CaTipoRangoServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoRango 	::		CaTipoRangoServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaTipoRango entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoRango entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoRango entity to update
	 @return CaTipoRango the persisted CaTipoRango entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaTipoRango update(CaTipoRango entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoRango 	::		CaTipoRangoServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoRango result = tipoRangocaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoRango 	::		CaTipoRangoServiceImpl	::	update	::	FIN	::	");
	            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoRango 	::		CaTipoRangoServiceImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaTipoRango findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoRangoServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoRango instance = tipoRangocaDao.findById(id);//entityManager.find(CaTipoRango.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoRangoServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoRangoServiceImpl	::	findById	::	ERROR	::	",re);
	        return null;
        }
    }    
    

/**
	 * Find all CaTipoRango entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoRango property to query
	  @param value the property value to match
	  	  @return List<CaTipoRango> found by query
	 */
    public List<CaTipoRango> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoRangoServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoRango model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoRangoServiceImpl	::	findByProperty	::	FIN	::	");
			return tipoRangocaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoRangoServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoRango> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoRango> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoRango> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoRango> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaTipoRango entities.
	  	  @return List<CaTipoRango> all CaTipoRango entities
	 */
	public List<CaTipoRango> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoRangoServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoRango model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoRangoServiceImpl	::	findAll	::	FIN	::	");
			return tipoRangocaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoRangoServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}