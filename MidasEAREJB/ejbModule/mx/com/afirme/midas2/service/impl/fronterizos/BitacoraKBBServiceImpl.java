package mx.com.afirme.midas2.service.impl.fronterizos;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.domain.fronterizos.BitacoraKBB;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fronterizos.BitacoraKBBService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.apache.log4j.Logger;

@Stateless
public class BitacoraKBBServiceImpl implements BitacoraKBBService {
	
	private static final Logger LOG = Logger.getLogger(BitacoraKBBServiceImpl.class);
	
	@EJB
	protected EntidadService entidadService;
	@PersistenceContext
	private EntityManager entityManager;
	private UsuarioService usuarioService;

	@EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@Override
	public void guardarBitacora(BitacoraKBB bitacoraKBB) {
		bitacoraKBB.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		bitacoraKBB.setFechaCreacion(new Date());
		this.save(bitacoraKBB);
	}

	private void save(BitacoraKBB bitacoraKBB) {
		entidadService.save(bitacoraKBB);
	}
	
	@Override
	public Double obtenerValorComercial (BitacoraKBB bitacoraKBB) {
		Double valorComercial = null;
		
		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("select model from BitacoraKBB model ");
			sb.append("where model.idToCotizacion = :idToCotizacion ");
			sb.append("and model.numeroInciso = :numeroInciso ");
			sb.append("and trim(model.numeroSerie) = :numeroSerie ");
			sb.append("order by model.fechaCreacion desc ");

			Query query = entityManager.createQuery(sb.toString(), BitacoraKBB.class);
			query.setParameter("idToCotizacion", bitacoraKBB.getIdToCotizacion());
			query.setParameter("numeroInciso", bitacoraKBB.getNumeroInciso());
			query.setParameter("numeroSerie", bitacoraKBB.getNumeroSerie().trim());
			query.setMaxResults(1);
			@SuppressWarnings("unchecked")
			List<BitacoraKBB> resultado = query.getResultList();
			
			if (resultado != null && resultado.size() > 0) {
				valorComercial = resultado.get(0).getValorComercial().doubleValue();
				LOG.info("BitacoraKBBServiceImpl.obtieneValorComercial() --> El valor comercial del vehiculo es $ " + valorComercial);
			}
		} catch (Exception e) {
			LOG.error("BitacoraKBBServiceImpl.obtieneValorComercial() --> Ocurrio un error al consultar el valor comercial del vehiculo", e);
		}
		
		return valorComercial;
	}
}
