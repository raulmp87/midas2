/**
 * Clase que llena las opciones de Menu para el rol de reportesSiniestros
 */
package mx.com.afirme.midas2.service.impl.seguridad.filler.reportessiniestros;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

/**
 * @author andres.avalos
 *
 */
public class MenuReportesSiniestros {

	private List<Menu> listaMenu = null;
		
	public MenuReportesSiniestros() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"m7","Ayuda", "Menu Principal Ayuda", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("2"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);		
		menu = new Menu(new Integer("3"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("4"),"menu","", "Raiz del menu", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m5","Reportes", "Menu Reportes", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("6"),"m5_2","Siniestro", "Submenu Siniestro", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("7"),"m5_2_1","Reporte de gastos", "Submenu Reporte de gastos", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("8"),"m5_2_2","Reporte de movimientos contabilidad", "Submenu Reporte de movimientos contabilidad", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("9"),"m5_2_3","Reporte general de siniestros reportados", "Submenu Reporte general de siniestros reportados", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("10"),"m5_2_4","Siniestros por c�digos de movimientos (RRC_SONOR)", "Submenu Siniestros por c�digos de movimientos (RRC_SONOR)", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("11"),"m5_2_5","Reporte de siniestros terminados como indocumentado", "Submenu Reporte de siniestros terminados como indocumentado", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("12"),"m5_2_6","Reporte de siniestralidad", "Submenu Reporte de siniestralidad", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("13"),"m5_2_7","Reporte de inventario de salvamentos", "Submenu Reporte de inventario de salvamentos", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("14"),"m5_2_8","Reporte de movimientos de siniestros consolidado", "Submenu Reporte de movimientos de siniestros consolidado", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("15"),"m5_2_9","Reporte de movimientos de siniestros a detalle", "Submenu Reporte de movimientos de siniestros a detalle", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("16"),"m5_2_10","Reporte de siniestros por ajustador", "Submenu Reporte de siniestros por ajustador", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("17"),"m5_2_11","Reporte de siniestros por antig�edad", "Submenu Reporte de siniestros por antig�edad", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("18"),"m5_2_12","Reporte de siniestros sin reserva", "Submenu Reporte de siniestros sin reserva", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("19"),"m5_2_13","Reporte de siniestros terminados con reserva", "Submenu Reporte de siniestros terminados con reserva", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("20"),"m5_2_14","Reporte de venta de salvamentos", "Submenu Reporte de venta de salvamentos", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("21"),"m5_2_15","Reporte de siniestros con reserva pendiente", "Submenu Reporte de siniestros con reserva pendiente", null, true);
		listaMenu.add(menu);
		
		
		return this.listaMenu;
		
	}
	
}
