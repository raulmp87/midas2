package mx.com.afirme.midas2.service.impl.folioReexpedible;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.vigencia.VigenciaDTO;
import mx.com.afirme.midas2.dao.folioReexpedible.TcTipoFolioDAO;
import mx.com.afirme.midas2.dao.folioReexpedible.ToFolioReexpedibleDAO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.folioReexpedible.FiltroFolioReexpedible;
import mx.com.afirme.midas2.domain.folioReexpedible.TcTipoFolio;
import mx.com.afirme.midas2.domain.folioReexpedible.ToFolioReexpedible;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.folioReexpedibleService.FolioReexpedibleService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

@Stateless
public class FolioReexpedibleServiceImpl implements FolioReexpedibleService, Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(FolioReexpedibleServiceImpl.class);
	private static final String SEQUENCE_NAME_FOLIO_REEXPEDIBLE = "MIDAS.SQ_FOLIOREEXPEDIBLE";
	//private static final String REGEX_SPLIT_SPACE = "\\s+";
	
	@EJB(beanName = "UsuarioServiceDelegate")
	public UsuarioService usuarioService;

	@EJB
	private ToFolioReexpedibleDAO folioDAO;

	@EJB
	private TcTipoFolioDAO tipoFolioDAO;
	
	@EJB
	private NegocioService negocioService;
	
	@EJB
	private ListadoService listadoService;
	
	/**
	 * Solo para ambiente de DEV ya que el agente viene nulo
	 * este se setea por default.
	 */
	public static final Long CODIGO_AGENTE_BASE = Long.valueOf("90036");

	public FolioReexpedibleServiceImpl() {
	}

	@Override
	public boolean actualizarFolioReexpedble(ToFolioReexpedible folioReexpedible) {
		log.info(">>>>>>>>>>>>> Entr\u00f3 a actualizar folio reexpedible");
		boolean flag;
		try {
			folioDAO.update(folioReexpedible);
			flag = true;
			log.info("Persisti\u00f3 de manera correcta el folio.");
		} catch (RuntimeException re) {
			log.error("	::	[ERR]	::	ERROR AL PERSISTIR FOLIO	", re);
			throw re;
		}
		return flag;
	}

	@Override
	public List<FiltroFolioReexpedible> consultarFoliosReexpedibles() {
		return transformToFolio(folioDAO.findByProperty("activo", Boolean.TRUE));
	}

	@Override
	public List<TcTipoFolio> consultarTiposDeFolios() {
		return tipoFolioDAO.findAll();
	}

	@Override
	public boolean eliminarFolioReexpedible(ToFolioReexpedible folioReexpedible) {
		log.info(":  :  [ INFO ]  :  : >>>>>>>>>>> entra a la baja l\u00f3gica");
		boolean flag;
		try {
			folioDAO.eliminarToFolioReexpedible(folioReexpedible);
			flag = true;
		} catch (RuntimeException re) {
			log.error(":  :  [ ERROR ]  :  :  >>>>>>>>> RUNTIMEEXCEPTION", re);
			flag = false;
			throw (re);
		}
		return flag;
	}

	@Override
	public boolean guardarFolioReexpedible(ToFolioReexpedible to, String numeroFolios) {
		boolean flag = false;
		try {
			FiltroFolioReexpedible filter = new FiltroFolioReexpedible();
			filter.setTipoFolio(new TcTipoFolio());
			filter.getTipoFolio().setIdTipoFolio(to.getTipoFolio().getIdTipoFolio());
			List<ToFolioReexpedible> folP = new ArrayList<ToFolioReexpedible>();
			List<ToFolioReexpedible> folios = folioDAO.findByProperties(filter);
			List<Integer> rangosFolios = new ArrayList<Integer>();
			boolean bandera = false;
			Integer ultimoFolio = 0;
			if (folios.size() > 0) {
				bandera = true;
				for (ToFolioReexpedible fo : folios) {
					Long idFolioR = fo.getNumeroFolio();
					rangosFolios.add(idFolioR.intValue());
				}
				ultimoFolio = Collections.max(rangosFolios);
			}
			Integer i = Integer.parseInt(numeroFolios);
			folP.clear();
			ToFolioReexpedible toF = null;
			for(Integer x = 0; x < i; x++){
				toF = new ToFolioReexpedible();
				toF.setAgenteFacultativo(to.getAgenteFacultativo());
				toF.setAgenteVinculado(to.getAgenteVinculado());
				toF.setComentarios(to.getComentarios());
				toF.setFechaCreacion(new Date());
				toF.setFechaValidesHasta(to.getFechaValidesHasta());
				toF.setIdTipoMoneda(to.getIdTipoMoneda());
				toF.setNegocio(to.getNegocio());
				toF.setTipoFolio(new TcTipoFolio(to.getTipoFolio().getIdTipoFolio()));
				toF.setNegprod(to.getNegprod());
				toF.setNegTpoliza(to.getNegTpoliza());
				toF.setVigencia(to.getVigencia());
				
    			Long id = folioDAO.getIncrementedSequence(SEQUENCE_NAME_FOLIO_REEXPEDIBLE);
    			
    			if (id != null) {
    				if (bandera) {
    					ultimoFolio = ultimoFolio + 1;
    					if (ultimoFolio - 1 > -1) {
    						toF.setNumeroFolio(new Long(ultimoFolio));
    					}
    				} else{
    					toF.setNumeroFolio(new Long(x+1));
    				}
    				toF.setIdFolioReexpedible(id);
    				toF.setAgenteVinculado(to.getAgenteFacultativo());
    				toF.setFechaCreacion(new Date());
    				toF.setActivo(Boolean.TRUE);
    				folP.add(toF);
    			}
			}
			folioDAO.saveAll(folP);
			flag = true;
			log.info("Persisti\u00f3 de manera correcta el folio.");
		} catch (RuntimeException re) {
			log.error("	::	[ERR]	::	Error al guardar FOLIO REEXPEDIBLE 	::		FOLIOREEXPEDIBLESERVICEIMPL	::	save	::	ERROR	::	", re);
			throw re;
		}
		log.info(":  [ INFO ]  : ----------- retorna bandera: "+ flag);
		return flag;
	}
	
	private List<FiltroFolioReexpedible> transformToFolio(List<ToFolioReexpedible> folios){
		List<FiltroFolioReexpedible> filtros = new ArrayList<FiltroFolioReexpedible>();
		filtros.clear();
		if(folios != null){
    		FiltroFolioReexpedible filtro=null;
    		for(ToFolioReexpedible folio: folios){
    			filtro = new FiltroFolioReexpedible();
    			log.info("::::  [ INFO ]  :::: consulta los tipos de productos por negocio");
    			
    			filtro.setNegocio(new Negocio());
    			filtro.getNegocio().setIdToNegocio(folio.getNegocio());
    			Negocio neg = negocioService.findById(folio.getNegocio());
    			filtro.getNegocio().setDescripcionNegocio(neg.getDescripcionNegocio());
    			
    			filtro.setDescripcionProducto(listadoService.getMapNegProductoPorNegocio(folio.getNegocio()).get(folio.getNegprod()));
    			filtro.setProducto(new NegocioProducto());
    			filtro.getProducto().setIdToNegProducto(folio.getNegprod());
    			
    			BigDecimal id = new BigDecimal(folio.getNegTpoliza());
    			filtro.setDesctipcionTipoPoliza(listadoService.getMapNegTipoPolizaPorNegProducto(folio.getNegprod()).get(id));
    			filtro.setTipoPoliza(new NegocioTipoPoliza());
    			filtro.getTipoPoliza().setIdToNegTipoPoliza(id);
    			
    			filtro.setDescTipoMoneda(listadoService.getMapMonedaPorNegTipoPoliza(id).get(new BigDecimal(folio.getIdTipoMoneda())));
    			filtro.setMoneda(new MonedaDTO());
    			filtro.getMoneda().setIdTcMoneda(new Long(folio.getIdTipoMoneda()).shortValue());
    			
    			filtro.setDescVigencia(listadoService.getMapVigencias().get(new BigDecimal(folio.getVigencia())));
    			filtro.setVigencia(new VigenciaDTO());
    			filtro.getVigencia().setIdTcVigencia(new BigDecimal(folio.getVigencia()));
    			
    			filtro.setTipoFolio(new TcTipoFolio());
    			filtro.getTipoFolio().setIdTipoFolio(folio.getTipoFolio().getIdTipoFolio());
    			filtro.getTipoFolio().setClaveTipoFolio(folio.getTipoFolio().getClaveTipoFolio());
    			filtro.getTipoFolio().setDescripcion(folio.getTipoFolio().getDescripcion());
    			
    			filtro.setFolioInicio( folio.getTipoFolio().getClaveTipoFolio() + "_" + new Long(folio.getNumeroFolio()).toString());
    			
    			filtro.setFechaValidez(folio.getFechaValidesHasta());
    			filtros.add(filtro);
    		}
		}
		return filtros;
	}
	

	@Override
	public List<FiltroFolioReexpedible> consultarFolios(FiltroFolioReexpedible filtro, boolean usaFiltro) {
		List<FiltroFolioReexpedible> folios = new ArrayList<FiltroFolioReexpedible>();
		boolean bandera;
		
		bandera = ((filtro.getFolioInicioL() != null && filtro.getFolioInicioL() > 0) && (filtro.getFolioFinL() != null && filtro.getFolioFinL() > 0) && (filtro.getTipoFolio() != null && filtro.getTipoFolio().getIdTipoFolio() > 0));
		
		if(!bandera){
			bandera = ((filtro.getFolioInicio() != null && filtro.getFolioInicio().length() > 5) && (filtro.getFolioFin() != null && filtro.getFolioFin().length() > 5));
		}
		
		if(!bandera){
			bandera = (filtro.getNegocio() != null && filtro.getNegocio().getIdToNegocio() != null && filtro.getNegocio().getIdToNegocio() > 0) || 
					(filtro.getProducto() != null && filtro.getProducto().getIdToNegProducto() != null && filtro.getProducto().getIdToNegProducto() > 0) || 
					(filtro.getVigencia() != null && filtro.getVigencia().getIdTcVigencia() != null && filtro.getVigencia().getIdTcVigencia().intValue() > 0) || 
					(filtro.getMoneda() != null && filtro.getMoneda().getIdTcMoneda() > 0) || (filtro.getFechaValidez() != null);
		}
		
		if(bandera || !usaFiltro){
			folios = folioDAO.consultarFolioReexpediblesSQL(filtro);
		}
		
		return folios;
	}
	
	public Long contarRegistrosFolios(FiltroFolioReexpedible filtro, Boolean usaFiltro){
		boolean bandera;
		Long cantidadFolios = 0L;
		bandera = ((filtro.getFolioInicioL() != null && filtro.getFolioInicioL() > 0) && 
				(filtro.getFolioFinL() != null && filtro.getFolioFinL() > 0) && 
				(filtro.getTipoFolio() != null && filtro.getTipoFolio().getIdTipoFolio() > 0));
		
		if(!bandera){
			bandera = ((filtro.getFolioInicio() != null && filtro.getFolioInicio().length() > 5) && 
					(filtro.getFolioFin() != null && filtro.getFolioFin().length() > 5));
		}
		
		if(!bandera){
			bandera = (filtro.getNegocio() != null && filtro.getNegocio().getIdToNegocio() != null && filtro.getNegocio().getIdToNegocio() > 0) || 
					(filtro.getProducto() != null && filtro.getProducto().getIdToNegProducto() != null && filtro.getProducto().getIdToNegProducto() > 0) || 
					(filtro.getVigencia() != null && filtro.getVigencia().getIdTcVigencia() != null && filtro.getVigencia().getIdTcVigencia().intValue() > 0) || 
					(filtro.getMoneda() != null && filtro.getMoneda().getIdTcMoneda() > 0) || (filtro.getFechaValidez() != null);
		}
		
		if(bandera || !usaFiltro){
			cantidadFolios = folioDAO.contarRegistrosFolios(filtro);
		}
		
		return cantidadFolios;
	}
	
	@Override
	public List<ToFolioReexpedible> findByProperties(FiltroFolioReexpedible folioFiltro){
		return folioDAO.findByProperties(folioFiltro);
	}
	
	@Override
	public String vincularFolios(FiltroFolioReexpedible filtro){		
		StringBuilder stringExito = new StringBuilder();
		
		StringBuilder stringFallido = new StringBuilder();
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(new Date());
		
		GregorianCalendar calendarFolio;
		Agente agente = usuarioService.getAgenteUsuarioActual();
		if(agente == null){
			agente = new Agente();
			agente.setId(CODIGO_AGENTE_BASE);
		}
		if(agente != null){
    		if(filtro.getFolioInicio() != null && filtro.getFolioFin().trim().length() > 5 && filtro.getFolioFin() != null && filtro.getFolioFin().trim().length() > 5){
    			String[] folioInicioArray = filtro.getFolioInicio().trim().split("_");
    			String[] folioFinArray = filtro.getFolioFin().trim().split("_");
    			List<TcTipoFolio> tFolios = null;
        		if(filtro.getFolioInicio().trim().length() > 5){
        			tFolios = tipoFolioDAO.findByProperty("claveTipoFolio", folioInicioArray[0]);
        		}
        		boolean tipoFolioExiste = (tFolios != null && tFolios.size() > 0);
        		if(tipoFolioExiste){
        			if(folioInicioArray[0].equals(folioFinArray[0])){
        				List<ToFolioReexpedible> folios = folioDAO.findByProperties(filtro);
        				for(ToFolioReexpedible folio: folios){
        					if(folio.getAgenteVinculado() == folio.getAgenteFacultativo()){
        						calendarFolio = new GregorianCalendar();
        						calendarFolio.setTime(folio.getFechaValidesHasta());
        						if(calendarFolio.after(calendar)){
        							folio.setAgenteVinculado(agente.getId());
        							folioDAO.update(folio);
        							stringExito.append("EL FOLIO " + folio.getTipoFolio().getClaveTipoFolio() + "_" + folio.getNumeroFolio() + " YA SE ENCUENTRA VINCULADO.|");
        						} else {
        							stringFallido.append("EL FOLIO " + folio.getTipoFolio().getClaveTipoFolio() + "_" + folio.getNumeroFolio() + " YA SE ENCUENTRA FUERA DE VIGENCIA.|");
        						}
        					} else {
        						stringFallido.append("EL FOLIO " + folio.getTipoFolio().getClaveTipoFolio() + "_" + folio.getNumeroFolio() + " YA SE ENCUENTRA EN USO.|");
        					}
        				}			
        			} else {
        				stringFallido.append("No puede vincular tipos de folio distintos.|");
        			}
        		} else{
        			stringFallido.append("No existe el tipo de folio que quiere vincular.|");
        		}
    		} else {
    			stringFallido.append("No puede dejar vacio Folio Inicio ni Folio Fin.|");
    		}
		}
		
		String cadenaFinal = stringExito.toString() + "#" + stringFallido.toString();
		return cadenaFinal;
	}
	
	@Override
	public void obtenerFolioVenta(FiltroFolioReexpedible filtro, StringBuilder mensaje, ToFolioReexpedible to){
		List<ToFolioReexpedible> folios = null;
		
		if(mensaje == null){
			mensaje = new StringBuilder();
		}
		
		if(to == null){
			to=new ToFolioReexpedible();
		}
		
		if(filtro.getFolioInicio() != null){
    		String[] folioArray = filtro.getFolioInicio().split("_");
    		if(folioArray.length == 2){
        		List<TcTipoFolio> tFolios = null;
        		if(filtro.getFolioInicio().trim().length() > 5){
        			tFolios = tipoFolioDAO.findByProperty("claveTipoFolio", folioArray[0]);
        		}
        		boolean tipoFolioExiste = false;
        		if(tFolios != null && tFolios.size() > 0){
        			tipoFolioExiste = true;
        			folios = findByProperties(filtro);
        		}
        		log.info("::::::::::::::::      [ INFO ]      ::::::::::::::::      N\u00faMERO DE FOLIOS ENCONTRADOS " + folios);
        		if(tipoFolioExiste){
            		if(folios != null && folios.size() >= 1){
            			for(ToFolioReexpedible folio : folios){
            				to.setActivo(folio.isActivo());
            				to.setAgenteFacultativo(folio.getAgenteFacultativo());
            				to.setAgenteVinculado(folio.getAgenteVinculado());
            				to.setComentarios(folio.getComentarios());
            				to.setFechaCreacion(folio.getFechaCreacion());
            				to.setFechaValidesHasta(folio.getFechaValidesHasta());
            				to.setIdFolioReexpedible(folio.getIdFolioReexpedible());
            				to.setIdTipoMoneda(folio.getIdTipoMoneda());
            				to.setNegocio(folio.getNegocio());
            				to.setNegprod(folio.getNegprod());
            				to.setNegTpoliza(folio.getNegTpoliza());
            				to.setNumeroFolio(folio.getNumeroFolio());
            				to.setTipoFolio(folio.getTipoFolio());
            				to.setVigencia(folio.getVigencia());
            			}
            			GregorianCalendar calendarHasta = new GregorianCalendar();
            			calendarHasta.setTime(to.getFechaValidesHasta());
            			GregorianCalendar currDay = new GregorianCalendar();
            			currDay.setTime(new Date());
            			if(calendarHasta.after(currDay) || calendarHasta.equals(currDay)){
            				if(!to.isActivo()){
            					mensaje.append("El n\u00famero de folio ingresado no puede ser ocupado, ya que ha sido ocupado.");
            				}
            			} else {
            				to = null;
            				mensaje.append("El n\u00famero de folio ingresado no puede ser ocupado, ya que ha expirado.");
            			}
            		} else {
            			mensaje.append("No se encontr\u00f3 alg\u00fan folio con el n\u00famero " + filtro.getFolioInicio());
            			to = null;
            		}
        		} else {
        			mensaje.append("El tipo de folio que b\u00fasca no existe.");
        			to = null;
        		}
    		} else {
    			mensaje.append("El tipo de folio que b\u00fasca no cumple con el formato.");
    			to = null;
    		}
		}
	}
	
	@Override
	public boolean isFolioVenta(String folio){
		boolean bandera = false;
		if(folio != null){
    		FiltroFolioReexpedible filtro = new FiltroFolioReexpedible();
    		filtro.setFolioInicio(folio);
    		ToFolioReexpedible to = new ToFolioReexpedible();
    		obtenerFolioVenta(filtro, null, to);
    		if(to != null){
    			bandera=true;
    		}
		}
		return bandera;
	}

	public NegocioService getNegocioService() {
		return negocioService;
	}

	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}
}
