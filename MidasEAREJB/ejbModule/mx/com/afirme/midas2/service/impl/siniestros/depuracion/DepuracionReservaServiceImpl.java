package mx.com.afirme.midas2.service.impl.siniestros.depuracion;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTOId;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.depuracion.DepuracionReservaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ConfiguracionNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCalculoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoDocumentoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.depuracion.CoberturaDepuracionReserva;
import mx.com.afirme.midas2.domain.siniestros.depuracion.ConfiguracionDepuracionReserva;
import mx.com.afirme.midas2.domain.siniestros.depuracion.DepuracionReserva;
import mx.com.afirme.midas2.domain.siniestros.depuracion.DepuracionReservaDetalle;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones.ConfiguracionNotificacionDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.DepuracionReservaDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.MovimientoPosteriorAjusteDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.SeccionSiniestroDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.notificaciones.ConfiguracionNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.depuracion.DepuracionReservaService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EnumCodigo;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionDeducibleService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.util.MailService;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

@Stateless
public class DepuracionReservaServiceImpl implements DepuracionReservaService {
	
	private static final Logger LOG = Logger.getLogger(DepuracionReservaServiceImpl.class);
	
	private static final int	PORCENTAJE_DEPURACION_TOTAL	= 100;
	private static final Logger log = Logger.getLogger(DepuracionReservaService.class);
	private static final String ESTATUS_DEPURADO = "DE";
	private static final String ESTATUS_PENDIENTE = "PE";
	private static final int PRIMER_ELEMENTO = 0;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@EJB
	private ListadoService listadoService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private DepuracionReservaDao depuracionReservaDao;
	
	@EJB
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@EJB
	private SeccionFacadeRemote seccionFacade;
	
	@EJB
	private MailService mailService;
		
	@EJB
	private ConfiguracionNotificacionesService configService;
	
	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;
	
	@EJB
	private RecuperacionDeducibleService recuperacionDeducibleService;
	//delete for branch prod
	@EJB
	private SistemaContext sistemaContext;
	//delete for branch prod
	@Resource	
	private TimerService timerService;

	@Override
	public void asociarCobertura(Long idConfiguracion, BigDecimal idToSeccion,
			BigDecimal idToCobertura, String claveSubCalculo) {
		ConfiguracionDepuracionReserva configuracion = entidadService.findById(ConfiguracionDepuracionReserva.class, idConfiguracion);
		CoberturaDepuracionReserva coberturaDepReserva = new CoberturaDepuracionReserva();
		coberturaDepReserva.setClaveSubCalculo(claveSubCalculo);
		CoberturaDTO cobertura = entidadService.findById(CoberturaDTO.class, idToCobertura);
		coberturaDepReserva.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		coberturaDepReserva.setCobertura(cobertura);
		coberturaDepReserva.setSeccion(entidadService.findById(SeccionDTO.class, idToSeccion));
		coberturaDepReserva.setNombreCobertura(cobertura.getNombreComercial());
		coberturaDepReserva.setConfiguracion(configuracion);
		configuracion.getCoberturas().add(coberturaDepReserva);
		entidadService.save(configuracion);
	}

	@Override
	public void asociarSeccion(Long idConfiguracion, BigDecimal idToSeccion) {
		List<SeccionSiniestroDTO> seccionSiniestros = this.obtenerCoberturasDisponibles(idConfiguracion, idToSeccion);
		CoberturaDepuracionReserva coberturaDepuracion;
		ConfiguracionDepuracionReserva configuracion = entidadService.findById(ConfiguracionDepuracionReserva.class, idConfiguracion);
		for(SeccionSiniestroDTO seccionSin: seccionSiniestros){
			for(CoberturaSeccionSiniestroDTO cobertura: seccionSin.getCoberturas()){
				coberturaDepuracion = new CoberturaDepuracionReserva();
				coberturaDepuracion.setSeccion(seccionSin.getSeccion());
				coberturaDepuracion.setClaveSubCalculo(cobertura.getClaveSubCalculo());
				coberturaDepuracion.setCobertura(cobertura.getCoberturaSeccionDTO().getCoberturaDTO());
				coberturaDepuracion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
				coberturaDepuracion.setNombreCobertura(cobertura.getNombreCobertura());
				coberturaDepuracion.setConfiguracion(configuracion);
				configuracion.getCoberturas().add(coberturaDepuracion);
			}
		}
		entidadService.save(configuracion);
	}

	@Override
	public void asociarTermino(Long idConfiguracion, String claveTermino) {
		ConfiguracionDepuracionReserva configuracion = entidadService.findById(ConfiguracionDepuracionReserva.class, idConfiguracion);
		configuracion.getTermino_Ajuste().add(claveTermino);
		entidadService.save(configuracion);
	}

	@Override
	public void asociarTodasCoberturas(Long idConfiguracion) {
		eliminarTodasCoberturas(idConfiguracion);
		asociarSeccion(idConfiguracion, null);
	}

	@Override
	public void asociarTodosTerminos(Long idConfiguracion) {
		ConfiguracionDepuracionReserva configuracion = entidadService.findById(ConfiguracionDepuracionReserva.class, idConfiguracion);
		Map<String, String> terminosDisponibles = obtenerTerminosDisponibles(idConfiguracion);
		configuracion.getTermino_Ajuste().addAll(terminosDisponibles.keySet());
		entidadService.save(configuracion);
	}

	@Override
	public List<ConfiguracionDepuracionReserva> buscarConfiguraciones(
			DepuracionReservaDTO filtro) {
		return depuracionReservaDao.buscarConfiguraciones(filtro);
	}

	@Override
	public DepuracionReserva ejecutarDepuracion(String idsDepuracionDetalle) {
		Long depuracionId;
		DepuracionReservaDetalle depuracionDet = null;
		EstimacionCoberturaReporteCabina estimacionCoberturaRep;
		MovimientoCoberturaSiniestro movimientoCobertura;
		String delimiter = ",";
		String[] ids = idsDepuracionDetalle.split(delimiter);
		long registrosDepurados = 0;
		BigDecimal montoReservaDepurada = BigDecimal.ZERO;
		DepuracionReserva depuracionReserva = null;	
		
		for(String id: ids){
			depuracionId = new Long( Integer.parseInt(id) );
			depuracionDet = entidadService.findById(DepuracionReservaDetalle.class, depuracionId);
			estimacionCoberturaRep = depuracionDet.getEstimacionCobertura();
			
			BigDecimal importeMovimiento = depuracionDet.getReservaPorDepurar().multiply(new BigDecimal(-1)).setScale(2, RoundingMode.DOWN);
						
			movimientoCobertura = movimientoSiniestroService.generarMovimiento(estimacionCoberturaRep.getId(), importeMovimiento, TipoDocumentoMovimiento.ESTIMACION,
					TipoMovimiento.AJUSTE_DISMINUCION, CausaMovimiento.DEPURACION_MANUAL_RESERVA, null, null);
			
			movimientoSiniestroService.invocarInterfazContable(movimientoCobertura);
			
			if (importeMovimiento.doubleValue() == 0 || 
					depuracionDet.getDepuracionReserva().getConfiguracion().getPorcentaje() == PORCENTAJE_DEPURACION_TOTAL) {
				estimacionCoberturaRep.setDepuracionTotal(true);
				estimacionCoberturaRep.setFechaDepuracionTotal(Calendar.getInstance().getTime());
				estimacionCoberturaRep.setConfiguracionDepuracion(depuracionDet.getDepuracionReserva().getConfiguracion());	
				
				//TODO INACTIVAR DEDUCIBLE
				recuperacionDeducibleService.inactivarRecuperacion(estimacionCoberturaRep.getId(), "Por depuración de reserva"); //TODO Mandar Constante 
						
			}
			depuracionDet.setReservaDepurada(depuracionDet.getReservaPorDepurar());
			depuracionDet.setEstatus(1);
			depuracionDet.setFechaModificacion(new Date());
			depuracionDet.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			registrosDepurados++;
			if(depuracionReserva == null){
				depuracionReserva = depuracionDet.getDepuracionReserva();
			}
			montoReservaDepurada = montoReservaDepurada.add(depuracionDet.getReservaDepurada());
			entidadService.save(depuracionDet);
		}
		depuracionReserva.setRegistrosDepurados(registrosDepurados);
		depuracionReserva.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		depuracionReserva.setFechaModificacion(new Date());
		depuracionReserva.setMontoReservaDepurada(montoReservaDepurada);
		depuracionReserva.setEstatus(ESTATUS_DEPURADO);
		depuracionReserva.setFechaEjecucion(new Date());
		depuracionReserva.setCodigoUsuarioEjecucion(usuarioService.getUsuarioActual().getNombreUsuario());
		entidadService.save(depuracionReserva);		
		
		try{
			enviarMensajeOficinas(depuracionReserva);
			
			enviarMensajeConsolidado(depuracionReserva);
		}catch(Exception e){
			log.debug(e.getMessage());
		}
		
		return depuracionReserva;
	}
	
	@Override
	public DepuracionReserva ejecutarDepuracionSP(String idsDepuracionDetalle){
		Long depuracionReservaId = null;
		DepuracionReserva depuracionReserva = null;
		depuracionReservaId = depuracionReservaDao.procesarDepuracionPorSP(idsDepuracionDetalle);
		
		depuracionReserva = entidadService.findById(DepuracionReserva.class, depuracionReservaId);
		
		try{
			enviarMensajeOficinas(depuracionReserva);
			
			enviarMensajeConsolidado(depuracionReserva);
		}catch(Exception e){
			log.debug(e.getMessage());
		}
		
		return depuracionReserva;
	}
	
	private void enviarMensajeOficinas(DepuracionReserva depuracionReserva) {
		List<ByteArrayAttachment> attachment = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		
		List<Oficina> oficinasRel = this.obtenerOficinasRelacionadas(depuracionReserva.getId());
		
		for(Oficina oficina: oficinasRel){
			List<DepuracionReservaDetalle> reservasOficina = this.obtenerReservasOficina(depuracionReserva.getId(), oficina.getId());
			String titulo = "Ejecuci\u00F3n Depuraci\u00F3n de Reserva - Oficina: " + oficina.getClaveOficina() + " al " + sdf.format(depuracionReserva.getFechaEjecucion()) ;
			String cuerpoMensaje = "Se realiz\u00F3 el proceso de Depuraci\u00F3n de Reserva con " + reservasOficina.size() + " movimiento(s) para la Oficina "
			+ oficina.getClaveOficina() + "-" + oficina.getNombreOficina();
			List<String> para = new ArrayList<String>();
			try{
				if (oficina.getCorreo1() != null && !oficina.getCorreo1().equals("")) {
					para.add(oficina.getCorreo1());
				}
				
				if (oficina.getCorreo2() != null && !oficina.getCorreo2().equals("")) {
					para.add(oficina.getCorreo2());
				}


				if (!para.isEmpty()) {
					TransporteImpresionDTO transporteImpresionDTO = this.getExcelReservasOficina( reservasOficina );
					
					if (transporteImpresionDTO != null && transporteImpresionDTO.getGenericInputStream() !=  null) {
						
						ByteArrayAttachment arrayAttachment = new ByteArrayAttachment();
						byte[] bytes = IOUtils.toByteArray(transporteImpresionDTO.getGenericInputStream());
						
						arrayAttachment.setContenidoArchivo(bytes);
						arrayAttachment.setNombreArchivo("Reservas Depuradas Oficina: " +  oficina.getClaveOficina() 
								+ " " + sdf.format(depuracionReserva.getFechaEjecucion()) +  ".xls");
						arrayAttachment.setTipoArchivo(ByteArrayAttachment.TipoArchivo.XLS);
						attachment = new ArrayList<ByteArrayAttachment>();
						attachment.add(arrayAttachment);
					}
					mailService.sendMessage(para, titulo, cuerpoMensaje, "midasweb@afirme.com", null, attachment, null, null);
				}
				
			}catch(Exception e){
				log.debug(e.getMessage());
			}
		}
	}
	
	private void enviarMensajeConsolidado(DepuracionReserva depuracionReserva) {
		List<ByteArrayAttachment> attachment = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		List<String> destinatarios = new ArrayList<String>();
		TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();

		ConfiguracionNotificacionDTO configDTO = new ConfiguracionNotificacionDTO();
		configDTO.setCodigo(EnumCodigo.DEPURACION_RESERVA.toString());

		try {
			List<DepuracionReservaDetalle> reservas = obtenerReservasExportacion( depuracionReserva.getId() );
	
			ExcelExporter exporter = new ExcelExporter( DepuracionReservaDetalle.class, "DEPURADA" );
	
			transporteImpresionDTO.setGenericInputStream( exporter.export(reservas) );
			transporteImpresionDTO.setContentType("application/vnd.ms-excel");
			transporteImpresionDTO.setFileName("Reservas Depuradas: " + sdf.format(depuracionReserva.getFechaEjecucion()) + ".xls");
	
			List<ConfiguracionNotificacionCabina> configuraciones = configService.obtenerConfiguracionesPorFiltros(configDTO);
	
			if (configuraciones != null && !configuraciones.isEmpty()) {
				for (ConfiguracionNotificacionCabina config : configuraciones) {
					if (config.esActiva()) {
						List<DestinatarioConfigNotificacionCabina> destinatariosConfig = config.getDestinatariosNotificacion();
	
						destinatarios = new ArrayList<String>();
						for (DestinatarioConfigNotificacionCabina destinatario : destinatariosConfig) {
							destinatarios.add(destinatario.getCorreo());
						}
	
						if (destinatarios != null && !destinatarios.isEmpty()) {
							String titulo = "Ejecuci\u00F3n Depuraci\u00F3n de Reserva  al " + sdf.format(depuracionReserva.getFechaEjecucion()) ;
							String cuerpoMensaje = "Se realiz\u00F3 el proceso de Depuraci\u00F3n de Reserva con " + reservas.size() + " movimiento(s)";
							if (transporteImpresionDTO != null && transporteImpresionDTO.getGenericInputStream() !=  null) {
	
								ByteArrayAttachment arrayAttachment = new ByteArrayAttachment();
								byte[] bytes = IOUtils.toByteArray(transporteImpresionDTO.getGenericInputStream());
	
								arrayAttachment.setContenidoArchivo(bytes);
								arrayAttachment.setNombreArchivo("Depuracion de Reserva " + sdf.format(depuracionReserva.getFechaEjecucion()) +  ".xls");
								arrayAttachment.setTipoArchivo(ByteArrayAttachment.TipoArchivo.XLS);
								attachment = new ArrayList<ByteArrayAttachment>();
								attachment.add(arrayAttachment);
							}
							mailService.sendMessage(destinatarios, titulo, cuerpoMensaje, "midasweb@afirme.com", null, attachment, null, null);
						}					
					}
				}
			}
		}catch(Exception e){
			log.debug(e.getMessage());
		}
	}
	
	/**
	 * Crea Excel de Reservas Depuradas por Oficina
	 * @param reservasOficina
	 * @return
	 */
	private TransporteImpresionDTO getExcelReservasOficina( List<DepuracionReservaDetalle> reservasOficina ){
		
		TransporteImpresionDTO transporte =  new TransporteImpresionDTO();
		ExcelExporter exporter = null;
		
		for( DepuracionReservaDetalle detalle : reservasOficina ){
			detalle.setNumeroSiniestro( detalle.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getSiniestroCabina().getNumeroSiniestro() );
			detalle.setNumeroPolizaFormateada( detalle.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getPoliza().getNumeroPolizaFormateada() );
			detalle.setNumeroInciso( detalle.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getNumeroInciso() );
			detalle.setFolio( detalle.getEstimacionCobertura().getFolio() );
			detalle.setNombreAsegurado( detalle.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getNombreAsegurado() );
			detalle.setTerminoAjusteDesc( detalle.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getTerminoAjusteDesc() );
			detalle.setNombreConfiguracion( detalle.getDepuracionReserva().getConfiguracion().getNombre() );
			detalle.setPorcentajeDepurado( detalle.getDepuracionReserva().getConfiguracion().getPorcentaje() );
			
		}
		this.llenarNombreCoberturaYterminoAjuste(reservasOficina);
		
		exporter = new ExcelExporter( DepuracionReservaDetalle.class, "DEPURADA" );
		transporte.setGenericInputStream( exporter.export(reservasOficina) );
		transporte.setContentType("application/vnd.ms-excel");
		transporte.setFileName("Reservas Depuradas.xls");
		 
		 return transporte;
	}

	@Override
	public void eliminarCobertura(Long idConfiguracion, BigDecimal idToCobertura) {
		HashMap<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("configuracion.id", idConfiguracion);
		filtro.put("cobertura.idToCobertura", idToCobertura);
		List<CoberturaDepuracionReserva> coberturasDepuracion = entidadService.findByProperties(CoberturaDepuracionReserva.class, filtro);
		if(coberturasDepuracion.size() > 0){
			CoberturaDepuracionReserva coberturaDepuracion = coberturasDepuracion.get(0);
			entidadService.remove(coberturaDepuracion);
		}
	}

	@Override
	public void eliminarSeccion(Long idConfiguracion, BigDecimal idToSeccion) {
		HashMap<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("configuracion.id", idConfiguracion);
		filtro.put("seccion.idToSeccion", idToSeccion);
		List<CoberturaDepuracionReserva> coberturasDepuracion = entidadService.findByProperties(CoberturaDepuracionReserva.class, filtro);
		if(coberturasDepuracion.size() > 0){
			entidadService.removeAll(coberturasDepuracion);
		}
	}

	@Override
	public void eliminarTermino(Long idConfiguracion, String claveTermino) {
		ConfiguracionDepuracionReserva configuracion = entidadService.findById(ConfiguracionDepuracionReserva.class, idConfiguracion);
		configuracion.getTermino_Ajuste().remove(claveTermino);
		entidadService.save(configuracion);
	}

	@Override
	public void eliminarTodasCoberturas(Long idConfiguracion) {
		List<CoberturaDepuracionReserva> coberturasDepuracion = entidadService.findByProperty(CoberturaDepuracionReserva.class,
				"configuracion.id", idConfiguracion);
		if (coberturasDepuracion!= null && !coberturasDepuracion.isEmpty()) {
			entidadService.removeAll(coberturasDepuracion);
		}
	}

	@Override
	public void eliminarTodosTerminos(Long idConfiguracion) {
		ConfiguracionDepuracionReserva configuracion = entidadService.findById(ConfiguracionDepuracionReserva.class, idConfiguracion);
		configuracion.getTermino_Ajuste().clear();
		entidadService.save(configuracion);
	}

	@Override
	public DepuracionReserva generarDepuracion(Long idConfiguracion, boolean generacionManual) {
		ConfiguracionDepuracionReserva configuracion = entidadService.findById(ConfiguracionDepuracionReserva.class, idConfiguracion);
		String codigoUsuario = "GENERADO";
		
		if (generacionManual) {
			codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		}			
		DepuracionReserva depuracionReserva = new DepuracionReserva();
		depuracionReserva.setConfiguracion(configuracion);
		depuracionReserva.setCodigoUsuarioCreacion(codigoUsuario);
		depuracionReserva.setFechaCreacion(new Date());
		depuracionReserva.setFechaProgramacion(new Date());
		depuracionReserva.setEstatus(ESTATUS_PENDIENTE);
		Long numeroDepuracion = this.depuracionReservaDao.obtenerNumeroDeDepuracion(idConfiguracion);
		depuracionReserva.setNumeroDepuracion(numeroDepuracion);
		entidadService.save(depuracionReserva);
//		this.generarReservasSP(idConfiguracion, depuracionReserva, false);
		return depuracionReserva;
	}

	@Override
	//delete for branch prod transaction attributte
	public void generarInformacionDepuracion() {
		LOG.info("Iniciando tarea generarInformacionDepuracion()...");
		List<ConfiguracionDepuracionReserva> configuraciones = depuracionReservaDao.obtenerConfiguracionesAlDia(new Date());
		for(ConfiguracionDepuracionReserva configuracion: configuraciones){
			generarDepuracion(configuracion.getId(), false);
		}
		
	}

	@Override
	public List<DepuracionReservaDetalle> generarReservas(Long idConfiguracion,
			DepuracionReserva depuracion, boolean generacionManual) {
		DepuracionReserva depuracionAux = entidadService.findById(DepuracionReserva.class, depuracion.getId());
		List<DepuracionReservaDetalle> reservasList = new ArrayList<DepuracionReservaDetalle>();
		ConfiguracionDepuracionReserva configuracionDepuracion = entidadService.findById(ConfiguracionDepuracionReserva.class, idConfiguracion);
		String codigoUsuario = "GENERADO";
		
		if (generacionManual) {
			codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		}
		
		List<DepuracionReservaDetalle> reservas = this.obtenerReservas(depuracionAux.getId());
		for(DepuracionReservaDetalle reserva: reservas){
			entidadService.remove(reserva);
		}
		BigDecimal reservaDepuracion = BigDecimal.ZERO;
		DepuracionReservaDetalle depuracionReservaDetalle;
		//isc-joskrc
		List<EstimacionCoberturaReporteCabina> estimacionCoberturas = depuracionReservaDao.obtenerReservas(idConfiguracion);
		long numero = 1;
		for(EstimacionCoberturaReporteCabina estimacionCobertura: estimacionCoberturas){
			depuracionReservaDetalle = new DepuracionReservaDetalle();
			depuracionReservaDetalle.setEstimacionCobertura(estimacionCobertura);
			depuracionReservaDetalle.setEstatus(0);
			depuracionReservaDetalle.setCodigoUsuarioCreacion(codigoUsuario);
			depuracionReservaDetalle.setFechaCreacion(new Date());
			depuracionReservaDetalle.setNumero(numero);
			depuracionReservaDetalle.setDepuracionReserva(depuracionAux); 
			
			BigDecimal reserva = movimientoSiniestroService.obtenerReservaAfectacion(estimacionCobertura.getId(), Boolean.TRUE);
			
			depuracionReservaDetalle.setReservaActual(reserva.setScale(2, RoundingMode.DOWN));
			depuracionReservaDetalle.setReservaPorDepurar(reserva.multiply(new BigDecimal(configuracionDepuracion.getPorcentaje()).divide(new BigDecimal(PORCENTAJE_DEPURACION_TOTAL))).setScale(2, RoundingMode.DOWN));
			reservaDepuracion = reservaDepuracion.add(reserva);
			entidadService.save(depuracionReservaDetalle);
			depuracionAux.getDepuracionReservaDetalles().add(depuracionReservaDetalle);
			numero++;
			reservasList.add(depuracionReservaDetalle);
		}
		depuracionAux.setMontoReserva(reservaDepuracion);
		return reservasList;
	}
	
	@Override
	public List<DepuracionReservaDetalle> generarReservasSP(Long idConfiguracion, DepuracionReserva depuracion, boolean generacionManual){
		List<DepuracionReservaDetalle> reservaList= null;
		reservaList = depuracionReservaDao.mostrarListadoReservasParaDepurar(idConfiguracion);
		
		return reservaList;
		
	}
	
	@Override
	public String getIdsDepuraDetailReservByIdDepuracionReserva (Long idDepuracionReserva, String cadenaIds){
		String cadenaDetalles="";
		
		return cadenaDetalles;
	}
	//changes for commit
	@Override
	public List<DepuracionReservaDetalle> getListDepuratedReservsByIdDepuracionReserva (Long idDepuracionReserva, String cadenaIds){
		DepuracionReserva depuracionReserva = null;
		List<DepuracionReservaDetalle> listaReservasDepuradas = null;
		String idsDepuracionesDet = "";
		depuracionReserva = this.obtenerDepuracion(idDepuracionReserva);		  
		for(int x=0;x<depuracionReserva.getDepuracionReservaDetalles().size(); x++){
			if(depuracionReserva.getDepuracionReservaDetalles().get(x).getEstatus() == 1){
				  Long idDet = depuracionReserva.getDepuracionReservaDetalles().get(x).getId();
				  idsDepuracionesDet = idsDepuracionesDet.concat(String.valueOf(idDet)+",");
			  }
			}
		idsDepuracionesDet = this.quitLastCommaFromChainString(idsDepuracionesDet);
		listaReservasDepuradas = this.obtenerReservasDepuradasById(idsDepuracionesDet);
		return listaReservasDepuradas;
	}
	
	@Override
	public void guardarConfiguracionDepuracion(
			ConfiguracionDepuracionReserva configuracion) {
		ConfiguracionDepuracionReserva configuracionDepuracion;
		if(configuracion.getOficina() == null || configuracion.getOficina().getId() == null){
			configuracion.setOficina(null);
		}
		if(configuracion.getNegocio() == null || configuracion.getNegocio().getIdToNegocio() == null){
			configuracion.setNegocio(null);
		}
		if(configuracion.getId() != null){
			configuracionDepuracion = entidadService.findById(ConfiguracionDepuracionReserva.class, configuracion.getId());
			configuracion.setTermino_Ajuste(configuracionDepuracion.getTermino_Ajuste());
			configuracion.setFechaModificacion(new Date());
			configuracion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			
			if (!configuracion.getConfigCoberturas()) {
				eliminarTodasCoberturas(configuracion.getId());
			}
			
			if (!configuracion.getConfigTerminos()) {
				eliminarTodosTerminos(configuracion.getId());
			}
			
		}else{
			configuracion.setFechaCreacion(new Date());
			configuracion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		}
		
		
		entidadService.save(configuracion);
		
	}

	@Override
	public List<SeccionSiniestroDTO> obtenerCoberturasAsociadas(
			Long idConfiguracion) {
		BigDecimal idToSeccionAux = BigDecimal.ZERO;
		List<SeccionSiniestroDTO> seccionSiniestroList = new ArrayList<SeccionSiniestroDTO>();
		SeccionSiniestroDTO seccionSiniestroDTO = new SeccionSiniestroDTO();
		CoberturaSeccionSiniestroDTO cobSeccSiniestrodDTO;
		HashMap<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("configuracion.id", idConfiguracion);
		List<CoberturaDepuracionReserva> cobDepuracionList = entidadService.findByPropertiesWithOrder(CoberturaDepuracionReserva.class,filtro, "seccion.idToSeccion ASC");
		for(CoberturaDepuracionReserva cobDepRes:cobDepuracionList){
			if(!idToSeccionAux.equals(cobDepRes.getSeccion().getIdToSeccion())){
				if(!idToSeccionAux.equals(BigDecimal.ZERO)){
					seccionSiniestroList.add(seccionSiniestroDTO);
				}
				idToSeccionAux = cobDepRes.getSeccion().getIdToSeccion();
				seccionSiniestroDTO = new SeccionSiniestroDTO();
				seccionSiniestroDTO.setSeccion(cobDepRes.getSeccion());
			}
			cobSeccSiniestrodDTO = new CoberturaSeccionSiniestroDTO();
			CoberturaSeccionDTOId coberturaSeccionDTOId = new CoberturaSeccionDTOId(); 
			coberturaSeccionDTOId.setIdtocobertura(cobDepRes.getCobertura().getIdToCobertura());
			coberturaSeccionDTOId.setIdtoseccion(cobDepRes.getSeccion().getIdToSeccion());
			cobSeccSiniestrodDTO.setCoberturaSeccionDTO(entidadService.findById(CoberturaSeccionDTO.class, coberturaSeccionDTOId));
			seccionSiniestroDTO.getCoberturas().add(cobSeccSiniestrodDTO);	
			
			String nombreCobertura = estimacionCoberturaSiniestroService.obtenerNombreCobertura(
					cobDepRes.getCobertura(),
					cobDepRes.getCobertura().getClaveTipoCalculo(), 
					cobDepRes.getClaveSubCalculo(), null, 
					cobDepRes.getCobertura().getTipoConfiguracion());	
			
			cobSeccSiniestrodDTO.setNombreCobertura(nombreCobertura);
			if(cobDepuracionList.indexOf(cobDepRes) == cobDepuracionList.size() - 1){
				seccionSiniestroList.add(seccionSiniestroDTO);
			}
		}
		return seccionSiniestroList;
	}

	@Override
	public List<SeccionSiniestroDTO> obtenerCoberturasDisponibles(
			Long idConfiguracion, BigDecimal idToSeccion) {
		List<SeccionSiniestroDTO> seccionSiniestroList = new ArrayList<SeccionSiniestroDTO>();
		List<CoberturaSeccionSiniestroDTO> coberturaSiniestroList = new ArrayList<CoberturaSeccionSiniestroDTO>();
		SeccionSiniestroDTO seccionSiniestroDTO;
		CoberturaSeccionSiniestroDTO coberturaSecSiniestroDTO;
		HashMap<String, Object> filtro = new HashMap<String, Object>();;
		List<SeccionDTO> listSeccion = new ArrayList<SeccionDTO>();
		
		if (idToSeccion != null) {
			listSeccion = entidadService.findByProperty(SeccionDTO.class, "idToSeccion", idToSeccion);
		} else {
			listSeccion = seccionFacade.listarVigentesAutosUsables();
		}
		for(SeccionDTO seccion: listSeccion){
			filtro.clear();
			filtro.put("seccionDTO.idToSeccion", seccion.getIdToSeccion());
			List<CoberturaSeccionDTO> seccionCoberturaList = entidadService.findByPropertiesWithOrder(CoberturaSeccionDTO.class, filtro, "seccionDTO.idToSeccion ASC");
			coberturaSiniestroList = new ArrayList<CoberturaSeccionSiniestroDTO>();
			
			for (CoberturaSeccionDTO cobertura:seccionCoberturaList) {				
				filtro.clear();
				filtro.put("cveTipoCalculoCobertura", cobertura.getCoberturaDTO().getClaveTipoCalculo());
				filtro.put("tipoConfiguracion", cobertura.getCoberturaDTO().getTipoConfiguracion());
				
				List<ConfiguracionCalculoCoberturaSiniestro> configuracionList = entidadService.findByProperties(ConfiguracionCalculoCoberturaSiniestro.class, filtro);
				for(ConfiguracionCalculoCoberturaSiniestro confCalculo:configuracionList){
					filtro.clear();
					filtro.put("seccion.idToSeccion", cobertura.getSeccionDTO().getIdToSeccion());
					filtro.put("cobertura.idToCobertura", cobertura.getCoberturaDTO().getIdToCobertura());
					filtro.put("claveSubCalculo", confCalculo.getCveSubTipoCalculoCobertura());
					filtro.put("configuracion.id", idConfiguracion);
					List<CoberturaDepuracionReserva> cobDepuracionList = entidadService.findByProperties(CoberturaDepuracionReserva.class,filtro);
					if(cobDepuracionList.size() <= 0){
						coberturaSecSiniestroDTO = new CoberturaSeccionSiniestroDTO();
						coberturaSecSiniestroDTO.setCoberturaSeccionDTO(cobertura);
						if(confCalculo.getNombreCobertura() == null || "".equals(confCalculo.getNombreCobertura())){
							coberturaSecSiniestroDTO.setNombreCobertura(cobertura.getCoberturaDTO().getNombreComercial());
						}else{
							coberturaSecSiniestroDTO.setNombreCobertura(confCalculo.getNombreCobertura());
						}
						coberturaSecSiniestroDTO.setClaveSubCalculo(confCalculo.getCveSubTipoCalculoCobertura());
						coberturaSiniestroList.add(coberturaSecSiniestroDTO);						
					}
				}						
			}
			
			if (coberturaSiniestroList != null && !coberturaSiniestroList.isEmpty()) {
				seccionSiniestroDTO = new SeccionSiniestroDTO();
				seccionSiniestroDTO.setSeccion(seccion);
				seccionSiniestroDTO.setCoberturas(coberturaSiniestroList);
				seccionSiniestroList.add(seccionSiniestroDTO);		
			}
			
		}
		return seccionSiniestroList;
	}

	@Override
	public List<CoberturaSeccionSiniestroDTO> obtenerCoberturasPorSeccion() {
		List<CoberturaSeccionSiniestroDTO> listCoberturasSeccSiniestroDTO = new ArrayList<CoberturaSeccionSiniestroDTO>();
		CoberturaSeccionSiniestroDTO  coberturaSecSiniestro;
		HashMap<String, Object> filtro = new HashMap<String, Object>();
		List<SeccionDTO> listSeccion = seccionFacade.listarVigentesAutosUsables();
		for(SeccionDTO seccion: listSeccion){
			filtro.clear();
			filtro.put("seccionDTO.idToSeccion", seccion.getIdToSeccion());
			List<CoberturaSeccionDTO> listCoberturas = entidadService.findByProperties(CoberturaSeccionDTO.class,filtro);
			for(CoberturaSeccionDTO cobertura:listCoberturas){
				filtro.clear();
				filtro.put("cveTipoCalculoCobertura", cobertura.getCoberturaDTO().getClaveTipoCalculo());
				filtro.put("tipoConfiguracion", cobertura.getCoberturaDTO().getTipoConfiguracion());
				List<ConfiguracionCalculoCoberturaSiniestro> configuracionList = entidadService.findByProperties(ConfiguracionCalculoCoberturaSiniestro.class, filtro);
				
				for(ConfiguracionCalculoCoberturaSiniestro confCalculo:configuracionList){
					coberturaSecSiniestro = new CoberturaSeccionSiniestroDTO();
					coberturaSecSiniestro.setCoberturaSeccionDTO(cobertura);
					coberturaSecSiniestro.setClaveSubCalculo(seccion.getIdToSeccion() + "-" + cobertura.getCoberturaDTO().getIdToCobertura() + "-" +
							(confCalculo.getCveSubTipoCalculoCobertura() != null ? confCalculo.getCveSubTipoCalculoCobertura() : ""));
					if(confCalculo.getNombreCobertura() == null || "".equals(confCalculo.getNombreCobertura())){
						coberturaSecSiniestro.setNombreCobertura(seccion.getNombreComercial() + "-" + cobertura.getCoberturaDTO().getNombreComercial());
					}else{
						coberturaSecSiniestro.setNombreCobertura(seccion.getNombreComercial() + "-" + confCalculo.getNombreCobertura());
					}
					listCoberturasSeccSiniestroDTO.add(coberturaSecSiniestro);
				}
			}
		}
		return listCoberturasSeccSiniestroDTO;
	}

	/**
	 * 
	 * Método que obtiene los registros de Depuraciones en cualquier estatus que pertenecen a una Configuración.
	 * Hacer entidadService.findByProperty sobre DepuracionReserva con el idConfiguracion
     *
	 */
	@Override
	public List<DepuracionReserva> obtenerHistoricoConfiguracion( Long idConfiguracion ) {
		List<DepuracionReserva> listDepuracionReserva = null;
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("configuracion.id", idConfiguracion);
		listDepuracionReserva = entidadService.findByPropertiesWithOrder(DepuracionReserva.class, params, new String[]{"numeroDepuracion"});
		Collections.reverse(listDepuracionReserva);
		
		for ( DepuracionReserva depuracion : listDepuracionReserva ) {
			depuracion.setDescripcionEstatus(catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_DEPURACION, depuracion.getEstatus()).getDescripcion());
		}
		return listDepuracionReserva;
	}

	@Override
	public List<Oficina> obtenerOficinasRelacionadas(Long idDepuracion) {
		return depuracionReservaDao.obtenerOficinasRelacionadas(idDepuracion);
	}

	@Override
	public List<DepuracionReservaDetalle> obtenerReservas(Long idDepuracion) {
		List<DepuracionReservaDetalle> listDepuracionReservaDetalle = new ArrayList<DepuracionReservaDetalle>();
		listDepuracionReservaDetalle = entidadService.findByProperty(DepuracionReservaDetalle.class, "depuracionReserva.id", idDepuracion);
		this.llenarNombreCoberturaYterminoAjuste( listDepuracionReservaDetalle );
		return listDepuracionReservaDetalle;
	}
	
	public List<DepuracionReservaDetalle> obtenerReservasDepuradas(Long idDepuracion){

		HashMap<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("depuracionReserva.id", idDepuracion);
		List<DepuracionReservaDetalle> listDepuracionReservaDetalle = entidadService.findByProperties(DepuracionReservaDetalle.class, filtro);
		for(DepuracionReservaDetalle depuracionDet: listDepuracionReservaDetalle){

			BigDecimal reserva = movimientoSiniestroService.obtenerReservaAfectacion(depuracionDet.getEstimacionCobertura().getId(), Boolean.TRUE);
			
			depuracionDet.setReservaActual(reserva.setScale(2, RoundingMode.DOWN));
		}
		this.llenarNombreCoberturaYterminoAjuste( listDepuracionReservaDetalle );
		return listDepuracionReservaDetalle;
	}

	public List<DepuracionReservaDetalle> obtenerReservasDepuradasById(String listaIdsDepurados){
		//listaIdsDepurados = "4867287";
		if(listaIdsDepurados.contains(",")){
			listaIdsDepurados = quitLastCommaFromChainString(listaIdsDepurados);
		}
		
		String delimitador = ",";
		String[] listadoIdsDetalles = listaIdsDepurados.split(delimitador);
		List<DepuracionReservaDetalle> listDepuracionReservaDetalle = new ArrayList<DepuracionReservaDetalle>();
		DepuracionReservaDetalle depuracionDet =new DepuracionReservaDetalle();
		
		
		for(int x=0; x<listadoIdsDetalles.length; x++){	
			Long idDetalle = Long.valueOf(listadoIdsDetalles[x]);   
			depuracionDet = entidadService.findById(DepuracionReservaDetalle.class, idDetalle);
			if(depuracionDet != null){
				BigDecimal reserva = movimientoSiniestroService.obtenerReservaAfectacion(depuracionDet.getEstimacionCobertura().getId(), Boolean.TRUE);
				depuracionDet.setReservaActual(reserva.setScale(2, RoundingMode.DOWN));
				listDepuracionReservaDetalle.add(depuracionDet);
			}
			
		}
		
		
		return listDepuracionReservaDetalle;
	}
	
	private String quitLastCommaFromChainString(String stringChained){
		String ultimoCaracter = stringChained.substring(stringChained.length()-1);
		if (ultimoCaracter.equals(",")){
			stringChained = stringChained.substring(0, stringChained.length()-1);
		}
		
		return stringChained;
	}
	
	/**
	 * llena el Nombre de la Cobertura y la Descripcion del Termino de Ajuste
	 * @param listDepuracionReservaDetalle
	 * @return
	 */
	private List<DepuracionReservaDetalle> llenarNombreCoberturaYterminoAjuste( List<DepuracionReservaDetalle> listDepuracionReservaDetalle ){
		
		CatValorFijo valor = null;
		
		for( DepuracionReservaDetalle detalle : listDepuracionReservaDetalle ){
			
			String nombreCobertura =  estimacionCoberturaSiniestroService.obtenerNombreCobertura(
					detalle.getEstimacionCobertura().getCoberturaReporteCabina().getCoberturaDTO(),
					detalle.getEstimacionCobertura().getCoberturaReporteCabina().getClaveTipoCalculo(), 
					null, detalle.getEstimacionCobertura().getTipoEstimacion(), 
					detalle.getEstimacionCobertura().getCoberturaReporteCabina().getCoberturaDTO().getTipoConfiguracion());

			detalle.setNombreCobertura(nombreCobertura);
			valor = catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO, detalle.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getTerminoAjuste() );
			
			if (valor != null) {
				detalle.setTerminoAjusteDesc( valor.getDescripcion().toUpperCase() );
			}
			
		}
		
		return listDepuracionReservaDetalle;
	}

	@Override
	public List<DepuracionReservaDetalle> obtenerReservasOficina(
			Long idDepuracion, Long idOficina) {
		HashMap<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("depuracionReserva.id", idDepuracion);
		filtro.put("estatus", 1);
		filtro.put("estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.oficina.id", idOficina);
		return entidadService.findByProperties(DepuracionReservaDetalle.class, filtro);
	}

	@Override
	public Map<String, String> obtenerTerminosAsociados(Long idConfiguracion) {
		Map<String, String> mapa = new LinkedHashMap<String, String>();
		ConfiguracionDepuracionReserva configuracion = entidadService.findById(ConfiguracionDepuracionReserva.class, idConfiguracion);
		if(configuracion != null){
			List<String> terminosAjuste = configuracion.getTermino_Ajuste();
			CatValorFijo valor;
			for(String terminoAjuste: terminosAjuste){
				valor = catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO, terminoAjuste);
				mapa.put(terminoAjuste, valor != null ? valor.getDescripcion() : "");
			}
		}
		return mapa;
	}

	@Override
	public Map<String, String> obtenerTerminosDisponibles(Long idConfiguracion) {
		Map<String, String> terminosAsociadosDisponibles = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);
		Map<String, String> terminosAjusteAsociados = this.obtenerTerminosAsociados(idConfiguracion);
		terminosAsociadosDisponibles.keySet().removeAll(terminosAjusteAsociados.keySet());
		return terminosAsociadosDisponibles;
	}

	@Override
	public ConfiguracionDepuracionReserva cargarConfiguracionDepuracion(
			Long idConfiguracion) {
		return entidadService.findById(ConfiguracionDepuracionReserva.class, idConfiguracion);
	}

	@Override
	public MovimientoPosteriorAjusteDTO obtenerDetalleMovimientoPosterior(
			Long idMovimiento) {
		MovimientoCoberturaSiniestro movimiento = this.entidadService.findById(MovimientoCoberturaSiniestro.class, idMovimiento);
		return this.transformaMovimiento(movimiento);
	}

	@Override
	public List<MovimientoPosteriorAjusteDTO> buscarMovimientosPosteriores(
			MovimientoPosteriorAjusteDTO filtro) {
		
		if (filtro.getIdCoberturaClaveSubCalculo() != null && !filtro.getIdCoberturaClaveSubCalculo().equals("")) {
			String[] coberturaInfo = filtro.getIdCoberturaClaveSubCalculo().trim().split("-");
			
			if(coberturaInfo!=null){
				filtro.setIdToCobertura(new BigDecimal(coberturaInfo[0]));
				if (coberturaInfo.length > 1) {
					filtro.setClaveSubCalculo(coberturaInfo[1]);
				}
			}
		}
		
		
		List<MovimientoCoberturaSiniestro> movimientos = this.depuracionReservaDao.buscarMovimientosPosteriores(filtro);
		List<MovimientoPosteriorAjusteDTO> movimientosDTO = new ArrayList<MovimientoPosteriorAjusteDTO>(); 
		for(MovimientoCoberturaSiniestro movimiento : movimientos){
			MovimientoPosteriorAjusteDTO movimientoDTO = this.transformaMovimiento(movimiento);
			movimientosDTO.add(movimientoDTO);
		}
		return movimientosDTO;
	}
	
	@Override
	public MovimientoPosteriorAjusteDTO transformaMovimiento(MovimientoCoberturaSiniestro mov) {
		MovimientoPosteriorAjusteDTO dto = new MovimientoPosteriorAjusteDTO();
		dto.setMovimientoId(mov.getId());
		dto.setNombreOficina(mov.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getOficina().getNombreOficina());
		PolizaDTO poliza = mov.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getPoliza();
		if(poliza!=null){
			dto.setNumeroPoliza(poliza.getNumeroPolizaFormateada());
		}
		SiniestroCabina siniestro = mov.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getSiniestroCabina();
		if(siniestro!=null){
			dto.setNumeroSiniestro(siniestro.getNumeroSiniestro());
		}
		dto.setNumeroReporte(mov.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getNumeroReporte());
		dto.setNumeroInciso(mov.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getNumeroInciso());
		dto.setContratante(mov.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getNombreAsegurado());
		String terminoSiniestroCode = mov.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getTerminoSiniestro();
		Map<String,String> terminoSiniestroMap = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);
		dto.setTerminoSiniestroDesc(terminoSiniestroMap.get(terminoSiniestroCode));
		dto.setNombreConcepto(mov.getEstimacionCobertura().getConfiguracionDepuracion().getNombre());
		dto.setMontoAjuste(mov.getImporteMovimiento());
		dto.setFechaAjuste(mov.getFechaCreacion());
		String causaAjusteCode = mov.getCausaMovimiento(); 
	    Map<String,String> causaAjusteMap = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CAUSAS_DE_MOVIMIENTO);
		dto.setCausaAjuste(causaAjusteMap.get(causaAjusteCode));
		
		BigDecimal reservaActual = movimientoSiniestroService.obtenerReservaAfectacion(mov.getEstimacionCobertura().getId(), Boolean.TRUE);
		
		dto.setReservaActual(reservaActual);
		
		Usuario usuario = this.usuarioService.buscarUsuarioPorNombreUsuario(mov.getCodigoUsuarioCreacion());
		if (usuario != null) {
			dto.setNombreUsuario(usuario.getNombreCompleto());
		}
		
		mov.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getNombreAsegurado();
		String tipoEstimacion = mov.getEstimacionCobertura().getTipoEstimacion();
		List<ConfiguracionCalculoCoberturaSiniestro> confCalcList = this.entidadService.findByProperty(ConfiguracionCalculoCoberturaSiniestro.class, "tipoEstimacion", tipoEstimacion);
		ConfiguracionCalculoCoberturaSiniestro confCalc =  confCalcList.get(PRIMER_ELEMENTO);
		String nombreSeccion = mov.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getSeccionDTO().getNombreComercial();
		if(confCalc.getNombreCobertura()!=null){
			dto.setNombreCobertura(confCalc.getNombreCobertura() + "  -  " + nombreSeccion);
		}else{
			dto.setNombreCobertura(mov.getEstimacionCobertura().getCoberturaReporteCabina().getCoberturaDTO().getNombreComercial() + "  -  " + nombreSeccion);
		}
		dto.setOficinaDesc(mov.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getOficina().getNombreOficina());
		return dto;
	}
	
	@Override
	public List<ConfiguracionDepuracionReserva> obtenerConfiguracionesActivas() {
		return  this.entidadService.findByProperty(ConfiguracionDepuracionReserva.class, "estatus", new Integer(1));
	}
	
	@Override
	public DepuracionReserva obtenerDepuracion(Long idDepuracion) {
		return entidadService.findById(DepuracionReserva.class, idDepuracion);
	}
	
	@Override
	public  List<DepuracionReservaDetalle> obtenerReservasExportacion(Long depuracionReservaId) {
		List<DepuracionReservaDetalle> reservasList = obtenerReservas( depuracionReservaId );
		
		for( DepuracionReservaDetalle detalle : reservasList ){
			detalle.setNumeroSiniestro( detalle.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getSiniestroCabina().getNumeroSiniestro() );
			detalle.setNumeroPolizaFormateada( detalle.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getPoliza().getNumeroPolizaFormateada() );
			detalle.setNumeroInciso( detalle.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getNumeroInciso() );
			detalle.setFolio( detalle.getEstimacionCobertura().getFolio() );
			detalle.setNombreAsegurado( detalle.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getNombreAsegurado() );			
			detalle.setNombreConfiguracion( detalle.getDepuracionReserva().getConfiguracion().getNombre() );
			detalle.setPorcentajeDepurado( detalle.getDepuracionReserva().getConfiguracion().getPorcentaje() );
			detalle.setPorcentajeDepurar( detalle.getDepuracionReserva().getConfiguracion().getPorcentaje() );
		}
		
		return reservasList;
	
	}
	
	@Override
	public  List<DepuracionReservaDetalle> obtenerReservasExportacionSP(Long depuracionReservaId) {
		
		List<DepuracionReservaDetalle> reservasList = depuracionReservaDao.obtenerReservasExportacionSP( depuracionReservaId );
		
		return reservasList;
	
	}
	
	@Override
	public List<DepuracionReservaDetalle> exportarSoloDepuradosToExcel(String idsDepurados){
		List<DepuracionReservaDetalle> soloDepuradosExcel = depuracionReservaDao.getOnlyReservasDepuradasToExcel(idsDepurados);
		
		return soloDepuradosExcel;
	}
	
	public void initialize() {
		String timerInfo = "TimerDepuracionReserva";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.minute(0);
				expression.hour(0);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerDepuracionReserva", false));
				
				LOG.info("Tarea TimerDepuracionReserva configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerDepuracionReserva");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerDepuracionReserva:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		generarInformacionDepuracion();
	}

}

