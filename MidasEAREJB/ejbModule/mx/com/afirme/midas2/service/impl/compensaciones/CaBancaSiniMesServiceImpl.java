package mx.com.afirme.midas2.service.impl.compensaciones;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.sql.DataSource;

import mx.com.afirme.midas2.dao.compensaciones.CaBancaSiniMesDao;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaBancaSiniMesDaoImpl;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaSiniMes;
import mx.com.afirme.midas2.exeption.ErrorBuilder;
import mx.com.afirme.midas2.service.compensaciones.CaBancaSiniMesService;
import mx.com.afirme.midas2.service.impl.compensaciones.CaBancaPrimPagServiceImpl.ResultadoCarga;

import org.apache.log4j.Logger;
import java.io.File;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import java.io.FileInputStream;
import java.math.BigDecimal;

import org.apache.poi.ss.usermodel.Sheet;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Row;
import java.util.List;
import java.util.ArrayList;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.jdbc.datasource.DataSourceUtils;
import oracle.jdbc.internal.OracleConnection;
import oracle.sql.ArrayDescriptor;
import java.sql.CallableStatement;
import oracle.sql.ARRAY;
import oracle.jdbc.driver.OracleTypes;
import java.sql.SQLException;
import mx.com.afirme.midas2.exeption.ApplicationException;
import java.util.Date;
import java.util.Calendar;
import java.sql.Connection;
import java.text.SimpleDateFormat;


@Stateless
public class CaBancaSiniMesServiceImpl implements CaBancaSiniMesService {

	
	private static final Logger LOG = Logger.getLogger(CaBancaSiniMesServiceImpl.class);
	
	@EJB
	private CaBancaSiniMesDao caBancaSiniMesDao;
	
	private Connection con ;
	private DataSource dataSource;
	
	@Resource(name = "jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {
		this.con = DataSourceUtils.getConnection(dataSource);
	}

	public boolean cargarExcel(File fileUpload, Long BancaPrimPagId,Double mes){
		LOG.info(">> cargarExcel()");
		boolean cargar = true;
		ResultadoCarga resultado=new ResultadoCarga();
		ErrorBuilder eb= new ErrorBuilder();
		try{
			Workbook workbook = WorkbookFactory.create(new FileInputStream(fileUpload));
			Sheet sheet = workbook.getSheetAt(0);
			int numCol = 25;
			int numRow = 65000;
			Iterator<Row> iteratorRow = sheet.iterator();
			
			if(iteratorRow.hasNext()){
				iteratorRow.next();
			}
			List<CaBancaSiniMes> listSiniMes = new ArrayList<CaBancaSiniMes>();
			List<CaBancaSiniMes> listSiniMesRegistrado = this.caBancaSiniMesDao.findByProperty(CaBancaSiniMesDaoImpl.BANCA_PRIMPAG_CP_ID, BancaPrimPagId);
			
			if(!listSiniMesRegistrado.isEmpty()){
				this.caBancaSiniMesDao.deleteRecordsByProperty(CaBancaSiniMesDaoImpl.BANCA_PRIMPAG_CP_ID, BancaPrimPagId);
			}
			
			Row row;
			//Cells
			
			Cell ofsuc;
			Cell nomOfsuc;
			Cell nomGrupo;
			Cell nomCanal;
			Cell codCanal;
			Cell canalNvo;
			Cell agte;
			Cell nomAgte;
			Cell nomEjec;
			Cell codigoRamo;
			Cell nomRamo;
			Cell idCentroEmis;
			Cell poliza;
			Cell mon;
			Cell asegurado;
			Cell fecEmi;
			Cell vigDes;
			Cell vigHas;
			Cell pmaEmi;
			Cell pmaPagada;
			Cell pmaDev;
			Cell reclama;
			Cell gastos;
			Cell salvRecu;
			Cell costoSin;
			Cell porSin;
			Cell lineaNeg;

			Integer rowIndex = 0;
			
			while (iteratorRow.hasNext()){
				row = iteratorRow.next();
				
				for(int colIndex = 1; colIndex < numCol; colIndex++){
					row = sheet.getRow(colIndex);
					
					ofsuc = row.getCell(0);
					nomOfsuc = row.getCell(1);
					nomGrupo  = row.getCell(2);
					nomCanal  = row.getCell(3);
					codCanal = row.getCell(4);
					canalNvo= row.getCell(5);
					agte= row.getCell(6);
					nomAgte= row.getCell(7);
					nomEjec= row.getCell(8);
					codigoRamo= row.getCell(9);
					nomRamo= row.getCell(10);
					poliza = row.getCell(11);
					String string = poliza.toString();
					String[] parts = string.split("-");
					String idCentroEmisParts = parts[0];
					String numPolizaParts = parts[1];
					String numRenovPolParts = parts[2];
					mon= row.getCell(12);
					asegurado= row.getCell(13);
					fecEmi =row.getCell(14);
					vigDes = row.getCell(15);
					vigHas = row.getCell(16);
					pmaEmi = row.getCell(17);
					pmaPagada = row.getCell(18);
					pmaDev = row.getCell(19);
					reclama = row.getCell(20);
					gastos = row.getCell(21);
					salvRecu = row.getCell(22);
					costoSin = row.getCell(23);
					porSin = row.getCell(24);
					lineaNeg = row.getCell(25);

			 		String ofsucString = ofsuc.toString();
			 		String nomOfsucString = nomOfsuc.toString();
			 		String nomGrupoString = nomGrupo.toString();
			 		String nomCanalString = nomCanal.toString();
			 		Long codCanalString =   new Long(codCanal.toString()) ;
			 		
			 		String canalNvoString = canalNvo.toString();
			 		String agteString = agte.toString();
			 		String nomAgteString = nomAgte.toString();
			 		String nomEjecString = nomEjec.toString();
			 		String codigoRamoString = codigoRamo.toString();
			 		String nomRamoString = nomRamo.toString();
			 		String monString = mon.toString();
			 		String aseguradoString = asegurado.toString();
			 		String fecEmiString = fecEmi.toString();
			 		String vigDesString = vigDes.toString();
			 		String vigHasString = vigHas.toString();
			 		
			 		String pmaEmiString = String.valueOf(pmaEmi);
			 		Double pmaEmiDouble = Double.parseDouble(pmaEmiString);

			 		String pmaPagadaString = String.valueOf(pmaPagada);
			 		Double pmaPagadaDouble = Double.parseDouble(pmaPagadaString);

			 		String pmaDevString = String.valueOf(pmaDev);
			 		Double pmaDevDouble = Double.parseDouble(pmaDevString);

			 		String reclamaString = String.valueOf(reclama);
			 		Double reclamaDouble = Double.parseDouble(reclamaString);

			 		String gastosString = String.valueOf(gastos);
			 		Double gastosDouble = Double.parseDouble(gastosString);

			 		String salvRecuString = String.valueOf(salvRecu);
			 		Double salvRecuDouble = Double.parseDouble(salvRecuString);

			 		String costoSinString = String.valueOf(costoSin);
			 		Double costoSinDouble = Double.parseDouble(costoSinString);

			 		String porSinString = String.valueOf(porSin);
			 		Double porSinDouble = Double.parseDouble(porSinString);

			 		String lineaNegString = lineaNeg.toString();

					
					if(BancaPrimPagId !=null){
						Calendar now = Calendar.getInstance();
						int anio = now.get(Calendar.YEAR);
						String anioString = String.valueOf(anio);
						Double anioDouble = Double.parseDouble(anioString);
						
						//listSiniMes.add( new CaBancaSiniMes (ofsucString,nomOfsucString, nomGrupoString,nomCanalString, codCanalString, canalNvoString, agteString, nomAgteString, nomEjecString, secString, nomRamoString, idCentroEmisParts, numPolizaParts, numRenovPolParts, monString, aseguradoString, fecEmiString, vigDesString, vigHasString, pmaEmiDouble, pmaPagadaDouble, pmaDevDouble, reclamaDouble, gastosDouble, salvRecuDouble, costoSinDouble, porSinDouble, lineaNegString,  anioDouble, mes, BancaPrimPagId));					
						
						listSiniMes.add( new CaBancaSiniMes (ofsucString,nomOfsucString,nomGrupoString ,nomCanalString,codCanalString,canalNvoString,
					    	     agteString, nomAgteString, nomEjecString, codigoRamoString,nomRamoString, idCentroEmisParts, numPolizaParts,
					    	      numRenovPolParts, monString, aseguradoString, fecEmiString, vigDesString, vigHasString, pmaEmiDouble, pmaPagadaDouble,
					    	      pmaDevDouble, reclamaDouble, gastosDouble, salvRecuDouble, costoSinDouble,porSinDouble, lineaNegString, anioDouble,
					    	      mes, BancaPrimPagId));
						
					}
					rowIndex++;
					if(rowIndex == numRow){
						break;
					}
				//}
				//LOG.info("listSiniMes: "+listSiniMes.size());
				OracleConnection oracleCon = con.unwrap(OracleConnection.class);
				ArrayDescriptor arrayDescr = ArrayDescriptor.createDescriptor("MIDAS.CA_SINIMES_LIST",oracleCon);
				LOG.info("listSiniMes" + listSiniMes.size());
				Object[] arrayObjects = new Object[listSiniMes.size()];
				int i = 0;
				for (CaBancaSiniMes siniMes: listSiniMes){
					Object[] arrayVector = new Object[]{
						siniMes.getOfsuc(),
						siniMes.getNomOfsuc(),
						siniMes.getNomGrupo(),
						siniMes.getNomCanal(),
						siniMes.getCodCanal(),
						siniMes.getCanalNvo(),
						siniMes.getAgte(),
						siniMes.getNomAgte(),
						siniMes.getNomEjec(),
						siniMes.getCodigoRamo(),
						siniMes.getNomRamo(),
						siniMes.getIdCentroEmision(),
						siniMes.getNumPoliza(),
						siniMes.getNumRenovPoliza(),
						siniMes.getMon(),
						siniMes.getAsegurado(),
						siniMes.getFecEmi(),
						siniMes.getVigDes(),
						siniMes.getVigHas(),
						siniMes.getPmaEmi(),
						siniMes.getPmaPagada(),
						siniMes.getPmaDev(),
						siniMes.getReclama(),
						siniMes.getGastos(),
						siniMes.getSalvRecu(),
						siniMes.getCostoSin(),
						siniMes.getPorSin(),
						siniMes.getLineaNeg(),
						siniMes.getAnio(),
						siniMes.getMes(),
						siniMes.getBancaPrimpagCpId()
					};
					arrayObjects[i] = arrayVector;
					i = i + 1;
					//this.caBancaSiniMesDao.save(siniMes);
					
				}
				//PROCEDIMIENTO
				ARRAY array = new ARRAY(arrayDescr, oracleCon, arrayObjects);
				StringBuilder sb = new StringBuilder();
				sb.append(" BEGIN  " );
				sb.append(" MIDAS.PKG_CA_BANCA_CARGA.SP_CARGA_SINMES( " );
				sb.append(" CA_SINIMESLISTA => ?, ");
				sb.append(" PINSERTADOS => ?,");
				sb.append(" PACTUALIZADOS => ?);" );
				sb.append(" END; ");
				CallableStatement vectoresInsertSP = oracleCon.prepareCall(sb.toString());
				vectoresInsertSP.setArray(1,array);
				vectoresInsertSP.registerOutParameter(2,OracleTypes.NUMBER);
				vectoresInsertSP.registerOutParameter(3,OracleTypes.NUMBER);
				vectoresInsertSP.execute();
				BigDecimal insertados =(BigDecimal)vectoresInsertSP.getBigDecimal(2);
				BigDecimal actualizados =(BigDecimal)vectoresInsertSP.getBigDecimal(3);

				resultado.setInsertados(insertados);
				resultado.setActualizados(actualizados);
				}//Validacion Numeric
			}
			
		}catch(SQLException sqlEx){
			LOG.error("MIDAS.PKG_CA_BANCA_CARGA.SP_CARGA_SINMES ha fallado : guardarLista", sqlEx);
			throw new ApplicationException(eb.addFieldError("guardarCPExcelPrimasPagadas",
					"Error en la carga de Siniestralidad Mensual"+sqlEx));

		}

		catch(Exception e){
			LOG.error("Informacion del Error",e);
			fileUpload.delete();
			cargar = false;
		} finally {
			if(fileUpload != null){
				fileUpload.deleteOnExit();
			}	
		}
		LOG.info("<< cargarExcel()");
		return cargar;
	}
	

	class ResultadoCarga  {
	    private List<String> erroresList;
		private BigDecimal insertados;
	    private BigDecimal errores;
	    private BigDecimal actualizados;
	    
	    public List<String> getErroresList() {
			return erroresList;
		}
		public void setErroresList(List<String> erroresList) {
			this.erroresList = erroresList;
		}
		public BigDecimal getInsertados() {
			return insertados;
		}
		public void setInsertados(BigDecimal insertados) {
			this.insertados = insertados;
		}
		public BigDecimal getErrores() {
			return errores;
		}
		public void setErrores(BigDecimal errores) {
			this.errores = errores;
		}
		public BigDecimal getActualizados() {
			return actualizados;
		}
		public void setActualizados(BigDecimal actualizados) {
			this.actualizados = actualizados;
		}
	}

	public DataSource getDataSource() {
		return dataSource;
	}

}
