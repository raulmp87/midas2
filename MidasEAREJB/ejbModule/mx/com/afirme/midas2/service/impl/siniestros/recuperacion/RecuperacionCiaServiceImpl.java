package mx.com.afirme.midas2.service.impl.siniestros.recuperacion;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.provision.SiniestrosProvisionDao;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.RecuperacionCiaDao;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.RecuperacionDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoEstimacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoPaseAtencion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicos;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicosConductor;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCPersonas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCViajero;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.provision.MovimientoProvisionSiniestros;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CartaCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CartaCompania.EstatusCartaCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CoberturaRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ComplementoCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.OrdenCompraCartaCia;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.PaseRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.MedioRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.OrigenRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.TipoRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania.PaseAtencionCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.SeguimientoRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.EstimacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.SiniestroCabinaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.CartaCiaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.CartaCiaExclusionesDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionCiaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionRelacionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.liquidacion.LiquidacionSiniestroService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EnumCodigo;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.siniestros.provision.SiniestrosProvisionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionCiaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionProveedorService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSalvamentoService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.DateUtils.Indicador;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.utils.CommonUtils;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.Days;
import org.springframework.beans.BeanUtils;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 10-jul-2015 08:08:17 p.m.
 */

@Stateless
public class RecuperacionCiaServiceImpl extends RecuperacionServiceImpl implements RecuperacionCiaService{

	public static final Logger	log	= Logger.getLogger(RecuperacionCiaServiceImpl.class);
	
	private static final String PATTERN_REFERENCIABANCARIA = "[a-zA-Z0-9-]*";
	private static final String MOTIVO_CANCELACION_INGRESO = "Se eliminó la referencia bancaria de la Recuperación de Compañía";
	private static final String TIPO_REDOCUMENTACION_EXCLUSION = "EXCLUSION";
	private static final String NOMBRE_EXCLUSIONES_AJUSTES = "Ajustes";
	private static final String NOMBRE_EXCLUSIONES_SALVAMENTO = "Salvamento";
	private static final String NOMBRE_EXCLUSIONES_PIEZAS = "Recuperación de Piezas";
	private static final String PERDIDA_TOTAL = "Pérdida Total";
	private static final String PERDIDA_PARCIAL = "Parcial";
	
	private static final String SE_CANCELAN_LAS_CIAS = "SE_CANCELAN_LAS_CIAS";
	private static final String SE_HABILITARON_LAS_CIAS = "SE_HABILITARON_LAS_CIAS";
	private static final String SE_CAMBIA_EL_PORCENTAJE = "SE_CAMBIA_EL_PORCENTAJE";
	
	@Resource
	private javax.ejb.SessionContext sessionContext;
	
	@EJB
	private EnvioNotificacionesService envioNotificacionesService;
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;

	@EJB
	protected EstiloVehiculoFacadeRemote estiloVehiculoFacadeRemote;
	
	@EJB
	OrdenCompraService ordenCompraService;
	
	@EJB
	BitacoraService bitacoraService;
	
	@EJB 
	LiquidacionSiniestroService liquidacionSiniestroService;

	@EJB
	private SiniestroCabinaService siniestroCabinaService;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private IngresoService ingresoService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private EstimacionCoberturaSiniestroService estimacioService;
	
	@EJB 
	private RecuperacionCiaDao recuperacionCiaDao;

	@EJB
	private EnvioNotificacionesService	notificacionService;
	
	@EJB
	private RecepcionFacturaService recepcionFacturaService;	

	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;
	
	@EJB
	private SiniestrosProvisionService  siniestrosProvisionService;	
	
	@EJB
	private RecuperacionDao  recuperacionDao;
	
	@EJB
	private SiniestrosProvisionDao  siniestroProvisionDao;	
	
	@EJB
	private RecuperacionProveedorService recuperacionProveedorService;	
	
	@EJB
	private RecuperacionSalvamentoService recuperacionSalvamentoService;	
	
	private static final Logger logger = Logger.getLogger(RecuperacionCiaService.class);
	
	private static final String CARTA_CIA =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/cartaCompaniaSin.jrxml";
	private static final String CARTA_CIA_EXCLUSIONES = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/cartaCompaniaSinExcluciones.jrxml";
	private static final String CARTA_CIA_ORDENES_COMPRA = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/cartaCompaniaSinOC.jrxml";
	
	private static final Integer diasLimiteCobro = 730;

	/**
	 * <b>SE DEFINE EN DISE�O DE CANCELAR RECUPERACION COMPA�IA</b>
	 * 
	 * M�todo que realiza la Cancelaci�n de una Recuperaci�n de Compa��a
	 * 
	 * Actualizar la <b>recuperacion </b>con <b>estatus </b>CANCELADO y su �ltima
	 * Carta con <b>estatus </b>CANCELADO.
	 * 
	 * 
	 * Invocar al m�todo <b><i>super.cancelarRecuperacion</i></b> e
	 * i<b><i>ngresoService. cancelarIngresoPendientePorAplicar</i></b>
	 * 
	 * @param recuperacionId
	 */
	@Override
	public void cancelarRecuperacion(Long recuperacionId) {
		
		RecuperacionCiaService processorRecuperacionCia = this.sessionContext.getBusinessObject(RecuperacionCiaService.class);
		processorRecuperacionCia.cancelarYGuardarRecuperacionCia(recuperacionId);

		//TODO INVOCAR PROVISION AL CANCELAR LA RECUPERACION
		this.siniestrosProvisionService.revierteProvisionAlCancelarRecuperacionCia(recuperacionId);
		
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("recuperacion.id", recuperacionId);
		params.put("folio", 0);
		List<CartaCompania>  lstCartaCompania=this.entidadService.findByPropertiesWithOrder(CartaCompania.class, params, "folio desc");
		if(null!=lstCartaCompania && !lstCartaCompania.isEmpty()){
			CartaCompania cartaCia = lstCartaCompania.get(0);
			cartaCia.setEstatus(CartaCompania.EstatusCartaCompania.CANCELADO.codigo);
			cartaCia.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			cartaCia.setFechaModificacion(new Date()); 
			this.entidadService.save(cartaCia);
		}
		params.clear();
		params.put("recuperacion.id", recuperacionId);
		List<Ingreso>  ingresos=this.entidadService.findByProperties(Ingreso.class, params);
		if(null!=ingresos && ingresos.isEmpty()){
			for(Ingreso ingreso : ingresos){
				this.ingresoService.cancelarIngresoPendientePorAplicar(ingreso, "");
			}
		}
		
		
		List<OrdenCompraCartaCia>  ordenes=this.entidadService.findByProperties(OrdenCompraCartaCia.class, params);
		if(null!=ordenes && !ordenes.isEmpty()){
			for(OrdenCompraCartaCia orden : ordenes){
				this.entidadService.remove(orden);
			}
		}
		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public RecuperacionCompania cancelarYGuardarRecuperacionCia(Long recuperacionId){
		RecuperacionCompania recuperacion = this.entidadService.findById(RecuperacionCompania.class, recuperacionId);
		super.cancelarRecuperacion(recuperacion.getId(), recuperacion.getMotivoCancelacion());
		this.entidadService.save(recuperacion);
		return recuperacion;
	}
	/**
	 * <b>SE DEFINE EN DISE�O EDITAR RECUPERACION A COMPA�IAS</b>
	 * M�todo que actualiza el estatus de la carta a ELABORADA.
	 * Obtener la <b>recuperacion </b>por medio de entidadService.findById sobre
	 * RecuperacionCompania y obtener la <b>cartaCompania </b>�por medio de
	 * entidadService.findByProperties con el recuperacionId y estatus PEND_ELAB
	 * 
	 * Validar
	 * <ul>
	 * 	<li><font color="#c0c0c0"><i>Que el monto de la indemnizaci�n sea mayor a
	 * Cero</i></font> : Que se tengas Ordenes de Compra asociadas a la
	 * <b>Recuperacion</b>.</li>
	 * </ul>
	 * 
	 * <ul>
	 * 	<li><font color="#c0c0c0"><i>Que en caso que sea P�rdida Total el Monto de
	 * Salvamento sea mayor a Cero: </i></font></li>
	 * </ul>
	 *        Verificar el pase de atenci�n o cobertura asociada a la <b>recuperacion
	 * </b>e invocar al m�todo <b><i>estimacionCoberturaService.obtenerDatosEstimacion;
	 * </i></b> si el atributo tipoPerdida es Perdida total verificar que el
	 * <b>montoSalvamento </b>de la <b>recuperacion </b>sea mayo a cero
	 * 
	 * <ul>
	 * 	<li><font color="#c0c0c0"><i>En caso de que una Orden de Compra se encuentre
	 * asociada a una Recuperaci�n de Proveedor, �ste debe de estar en estatus
	 * Recuperado:</i></font></li>
	 * </ul>
	 * 
	 * <ul>
	 * 	<li><font color="#c0c0c0"><i>Que las �rdenes de Compra no se encuentren
	 * asociados a otra Recuperaci�n de Compa��a:</i></font></li>
	 * </ul>
	 *        Iterar las Ordenes de Compra y buscar que no se encuentren asociadas a
	 * otra entidad de Recuperaciond e Compa�ias
	 * 
	 * Actualizar el <b>estatus </b>a ELABORADA, actualizar la <b>fechaElaboracion
	 * </b>con la fecha actual y el <b>usuarioElaboracion </b>con el usuario actual,
	 * cambiar a null la <b>causaNoElaboracion</b>.
	 * 
	 * @param recuperacionCiaId
	 */
	
	public String validaElaborarCarta(Long recuperacionCiaId){
		StringBuilder  mensaje = new StringBuilder("");
		RecuperacionCompania recuperacion = this.entidadService.findById(RecuperacionCompania.class, recuperacionCiaId);
		CartaCompania carta = obtenerCartaPendiente(recuperacionCiaId);
		if(carta != null){
			
			List<OrdenCompraCartaCia>  ordenes = this.obtenerOrdenesCompraCia(recuperacionCiaId, Boolean.TRUE);
			
			if (CollectionUtils.isEmpty(ordenes)) {
				return "No se encuentran asociadas Ordenes de Compra a la Recuperacion";
			}
			
			if(!(recuperacion.getMontoIndemnizacion().compareTo(BigDecimal.ZERO) ==1)){
				return "El monto de la indemnizacion no es mayor a cero";
			}
			EstimacionCoberturaReporteCabina pase =null;
			List<CoberturaRecuperacion> coberturas= this.getCoberturasRecuperacion(recuperacion.getId());
			CoberturaRecuperacion cobertura= coberturas.get(0);
			List<EstimacionCoberturaReporteCabina> pases = this.getPasesRecuperacion(recuperacion.getId());
			if(null!=pases && !pases.isEmpty()){
				pase= pases.get(0);				
			}else{
				pase= this.obtieneEstimacion(cobertura.getCoberturaReporte().getId(), null);
			}
			if(null != pase && this.estimacionService.esPerdidaTotal(pase.getId())){
				BigDecimal monto =this.obtenerMontoSalvamento(pase.getId());
				if(!(monto.compareTo(BigDecimal.ZERO) ==1)){
					return "La Estimacion es de Pérdida Total, el Monto de Salvamento debe ser mayor a Cero.";
				}
			}
			
			boolean bandera =false;
			Map<String, Object> params = new HashMap<String, Object>();				
			List<RecuperacionProveedor> lstRecuperacionesPRV=null;
			List<OrdenCompraCartaCia>  lstOrdenes= null;
			if (null!=ordenes && !ordenes.isEmpty() ){
				for (OrdenCompraCartaCia orden:ordenes){					
					OrdenCompra oc = orden.getOrdenCompra();
					params.clear();
					params.put("ordenCompra.id", oc.getId());	
					StringBuilder queryWhere = new StringBuilder();	
					lstRecuperacionesPRV= entidadService.findByProperties(RecuperacionProveedor.class, params) ;
					params.clear();
					queryWhere = new StringBuilder();	
					params.put("ordenCompra.id", oc.getId());	
					params.put("recuperacion.tipo",Recuperacion.TipoRecuperacion.COMPANIA.toString());	
					queryWhere.append(" and model.recuperacion.id not in ( "+recuperacionCiaId+") "); 
					queryWhere.append(" and model.recuperacion.estatus not in ( '"+Recuperacion.EstatusRecuperacion.CANCELADO.toString()+"') "); 
					lstOrdenes= entidadService.findByColumnsAndProperties(OrdenCompraCartaCia.class, "id,ordenCompra,recuperacion", params, new HashMap<String,Object>(), queryWhere, null);
					if( null!=lstOrdenes && !lstOrdenes.isEmpty()){
						Iterator<OrdenCompraCartaCia> iteratorPases = lstOrdenes.iterator();
						mensaje.append("*La Orden de Compra No "+oc.getId()+", Esta relacionada a otra(s) Recuperacion(es) de Compañia(s): ");
					    while (iteratorPases.hasNext()) {					    	
					    	OrdenCompraCartaCia entity  = iteratorPases.next();
					    	if(null!=entity.getRecuperacion()){
					    		bandera=true;
					    		mensaje.append(entity.getRecuperacion().getNumero());
						        if (iteratorPases.hasNext()) {
						        	mensaje.append(", ");
						        }
					    	}
					    }
					    mensaje.append(". ");
					}
					if(bandera){
						return mensaje.toString();
					}
					if( null!=lstRecuperacionesPRV && !lstRecuperacionesPRV.isEmpty()){						
						Iterator<RecuperacionProveedor> iterator= lstRecuperacionesPRV.iterator();						
						mensaje.append("La Orden de Compra No ").append(oc.getId()).append(", Esta a asociada a una Recuperación de Proveedor, que no esta en estatus de Recuperado: ");
					    while (iterator.hasNext()) {
					    	RecuperacionProveedor entity  = iterator.next();
					    	if(entity.getEstatus().equalsIgnoreCase(Recuperacion.EstatusRecuperacion.RECUPERADO.toString())  || entity.getEstatus().equalsIgnoreCase(Recuperacion.EstatusRecuperacion.CANCELADO.toString())  ){
					    		continue;
					    	}
					    	bandera=true;					    	
					    	mensaje.append(entity.getNumero()).append(" [").append(this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.ESTATUS_RECUPERACION, entity.getEstatus())).append("]");
					        if (iterator.hasNext()) {
					        	mensaje.append(", ");
					        }
					    }
					}
					if(bandera){
						return mensaje.toString() ;
					}
				}
			}
		
		}else{
			return "No existe Carta Cia. Registrada";
		}
		return null;
	}
	
	private CartaCompania obtenerCartaPendiente(Long recuperacionCiaId) {
		Map<String, Object> params = new HashMap<String, Object>();	
		params.put("estatus", EstatusCartaCompania.REGISTRADO.getValue());
		params.put("recuperacion.id", recuperacionCiaId);
		List<CartaCompania>  lstCartaCompania=this.entidadService.findByPropertiesWithOrder(CartaCompania.class, params, "folio desc");
		
		if(CollectionUtils.isNotEmpty(lstCartaCompania)){
			return lstCartaCompania.get(0);
		} else {
			params = new HashMap<String, Object>();	
			params.put("estatus", EstatusCartaCompania.PENDIENTE_ELABORACION.getValue());
			params.put("recuperacion.id", recuperacionCiaId);
			lstCartaCompania=this.entidadService.findByPropertiesWithOrder(CartaCompania.class, params, "folio desc");
			
			if(CollectionUtils.isNotEmpty(lstCartaCompania)){
				return lstCartaCompania.get(0);
			}
		}
		return null;
	}
	
	@Override
	public String elaborarCarta(Long recuperacionCiaId) {
		String  mensaje = this.validaElaborarCarta(recuperacionCiaId);	
		if(!StringUtil.isEmpty(mensaje)){
			return mensaje;
		}
		RecuperacionCiaService processor = this.sessionContext.getBusinessObject(RecuperacionCiaService.class);
		processor.elaborarCartaYGuarda(recuperacionCiaId);
		this.validaSiRequiereProvisionar(null, recuperacionCiaId);
		
		// TODO PROVISIONAR GRUAS
		System.out.println("elaborarCarta - inicia provosisión ");
		// OBTENER MONTO DE GRUAS INICIAL
		EstimacionCoberturaReporteCabina estimacion = this.siniestrosProvisionService.obtenerEstimacionDeRecuperacionCia(recuperacionCiaId);
		BigDecimal totalGruas = this.siniestroProvisionDao.obtenerSumaGruasPorEstimacion( estimacion.getId() );
		this.siniestrosProvisionService.actualizaProvisionAlAsignarGruas(recuperacionCiaId,totalGruas);
		
		return null;	
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public RecuperacionCompania elaborarCartaYGuarda(Long recuperacionCiaId){
		RecuperacionCompania recuperacion = null;
		CartaCompania cartaCompania = obtenerCartaPendiente(recuperacionCiaId);		
		if (cartaCompania != null) {
			cartaCompania.setEstatus(CartaCompania.EstatusCartaCompania.ELABORADO.codigo);
			cartaCompania.setUsuarioElaboracion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			cartaCompania.setFechaElaboracion(new Date());
			cartaCompania.setCausasNoElaboracion(null);
			this.guardarCartaCia(cartaCompania, recuperacionCiaId);
			if(cartaCompania.getFolio().compareTo(CartaCompania.FOLIO_CERO) != 0  && !StringUtil.isEmpty(cartaCompania.getTipoRedocumentacion())   &&  cartaCompania.getTipoRedocumentacion().equalsIgnoreCase("EXCLUSION")  ){
				if(null!= cartaCompania.getMontoARecuperar()){
					recuperacion = this.entidadService.findById(RecuperacionCompania.class, recuperacionCiaId);
					recuperacion.setImporte(cartaCompania.getMontoARecuperar());	
					this.entidadService.save(recuperacion);
				}
			}
		}
				
		return recuperacion;
	}
	/**
	 * <b>SE DEFINE EN DISE�O EDITAR RECUPERACION A COMPA�IAS</b>
	 * 
	 * M�todo que actualiza el estatus de la carta a ENTREGADA.
	 * 
	 * Obtener la <b>recuperacion </b>por medio de entidadService.findById sobre
	 * RecuperacionCompania y obtener la <b>cartaCompania </b>�por medio de
	 * entidadService.findByProperties con el recuperacionId y estatus
	 * <b>ELABORADA</b>
	 * 
	 * 
	 * Actualizar el <b>estatus </b>a ENTREGADA, actualizar la <b>fechaEntrega </b>con
	 * la fecha actual y el <b>usuarioEntrega </b>con el usuario actual,  asignar la
	 * <b>fechaAcuse</b>
	 * 
	 * @param recuperacionCiaId
	 * @param fechaAcuse
	 */
	@Override
	public String entregaCarta(Long recuperacionCiaId, Date fechaAcuse) {
		
		
		CartaCompania carta = this.obtenerCartaActiva(recuperacionCiaId);
		if(null!=carta){
			carta.setFechaAcuse(fechaAcuse);
			carta.setEstatus(CartaCompania.EstatusCartaCompania.ENTREGADO.codigo);
		}
		this.guardarCartaCia(carta, recuperacionCiaId);
		return null;
	}
	
	@Override
	public void generarRedocumentacionProvision(CartaCompania cartaCia, Long recuperacionId, String validaExclusionRechazo){
		
		RecuperacionCiaService processor = this.sessionContext.getBusinessObject(RecuperacionCiaService.class);
		processor.generarRedocumentacion(cartaCia, recuperacionId,validaExclusionRechazo);
		this.validaSiRequiereProvisionar(cartaCia,recuperacionId);
	}
	
	private void validaSiRequiereProvisionar(CartaCompania cartaCia,Long recuperacionId){
		CartaCompania cartaActiva = this.obtenerCartaActiva(recuperacionId);
		if( cartaActiva.getMontoExclusion() != null && cartaActiva.getTipoRedocumentacion().equals("EXCLUSION") && !cartaActiva.getEstatus().equals(CartaCompania.EstatusCartaCompania.REGISTRADO.getValue()) ){
			this.siniestrosProvisionService.generaYGuardaMovimientoDeExclusionDeCartaCia(cartaActiva.getRecuperacion().getId());
			EstimacionCoberturaReporteCabina estimacion = this.siniestrosProvisionService.obtenerEstimacionDeRecuperacionCia(recuperacionId);
			this.siniestrosProvisionService.actualizaProvision(estimacion.getId(), MovimientoProvisionSiniestros.Origen.PASE);
		}
	}
	
	
	
	/**
	 * <b>SE AGREGA EN DISE�O REGISTRAR REDOCUMENTACION</b>
	 * 
	 * M�todo que guara el Rechazo o Exclusion de la Carta Actual
	 * 
	 * Invocar al m�todo obtenerCartaActiva y asignar a la variable cartaActual.
	 * 
	 * Cambiar el estatus de la cartaActual a <b>RECHAZADA</b>.
	 * 
	 * Guardar la nueva cartaCompania  con estatus <b>REGISTRADA </b>con el siguiente
	 * <b>folio </b>consecutivo.
	 * 
	 * Actualizar el monto de la recuperacion en caso de ser Exclusion.
	 * 
	 * @param cartaCia
	 * @param recuperacionId
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void generarRedocumentacion(CartaCompania cartaCia,
			Long recuperacionId, String validaExclusionRechazo) {
		RecuperacionCompania recuperacion=this.entidadService.findById(RecuperacionCompania.class, recuperacionId);
		String codigoUsuario = String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() );
		CartaCompania filter=new CartaCompania();
		filter.setFechaCreacion(null);
		filter.setFechaModificacion(null);
		filter.setRecuperacion(recuperacion);
		Long numero = 0L;
		CartaCompania cartaACtual = this.obtenerCartaActiva(recuperacionId);
		
		
		// APLICAR NUEVA FOLIO SI NO DESEA MANTENER EL ACUSE
		numero=this.entidadService.getIncrementedProperty(CartaCompania.class, "folio", filter);

		
		if(null==cartaCia.getId()){
			if(null!=cartaACtual){			
				cartaACtual.setEstatus(CartaCompania.EstatusCartaCompania.RECHAZADO.codigo);
				cartaACtual.setCodigoUsuarioModificacion(codigoUsuario);
				cartaACtual.setFechaModificacion(new Date()); 	
				this.entidadService.save(cartaACtual);
			}
			cartaCia.setCodigoUsuarioCreacion(codigoUsuario );
			cartaCia.setFechaCreacion(new Date());
			cartaCia.setEstatus(CartaCompania.EstatusCartaCompania.REGISTRADO.codigo);
			cartaCia.setConcepto(".");  
			cartaCia.setFolio(numero);

		}else{
			cartaCia.setCodigoUsuarioModificacion(codigoUsuario);
			cartaCia.setFechaModificacion(new Date());
		}
		cartaCia.setRecuperacion(recuperacion);
		if(cartaCia.getTipoRedocumentacion().equalsIgnoreCase("RECHAZO")){	
			cartaCia.setMontoExclusion(new BigDecimal(0));
			cartaCia.setMotivoExclusion(null);
			cartaCia.setMontoARecuperar(recuperacion.getImporte());
		}
		
		if( validaExclusionRechazo.equals("s") ){
			cartaCia.setEstatus(CartaCompania.EstatusCartaCompania.ENTREGADO.codigo);
			cartaCia.setFechaAcuse(cartaACtual.getFechaAcuse());
			cartaCia.setFechaImpresion(new Date());
			cartaCia.setFechaElaboracion(new Date());
			cartaCia.setFechaEntrega(cartaACtual.getFechaEntrega());
			cartaCia.setUsuarioElaboracion(codigoUsuario);
			cartaCia.setUsuarioEntrega(codigoUsuario);
			cartaCia.setCodigoUsuarioModificacion(codigoUsuario);
			recuperacion.setImporte(recuperacion.getImporte().subtract(cartaCia.getMontoExclusion()));
			this.entidadService.save(recuperacion);
			
		}else if(validaExclusionRechazo.equals("CARTAMIGRADA")){
			System.out.println("validaExclusionRechazo: "+validaExclusionRechazo);
			cartaCia.setEstatus(CartaCompania.EstatusCartaCompania.ENTREGADO.codigo);
			
			Date fechaBanderaMig = cartaCia.getFechaAcuseCartasMigradas();
			if(fechaBanderaMig == null){
	        	Date fecha = new Date();
	        	fechaBanderaMig = fecha;
			    cartaCia.setFechaAcuse(fechaBanderaMig);
			}else{
				cartaCia.setFechaAcuse(cartaCia.getFechaAcuseCartasMigradas());
			}
			cartaCia.setFechaImpresion(cartaACtual.getFechaImpresion());
			cartaCia.setFechaElaboracion(cartaACtual.getFechaElaboracion());
			cartaCia.setFechaEntrega(cartaACtual.getFechaEntrega());
			cartaCia.setUsuarioElaboracion(codigoUsuario);
			cartaCia.setUsuarioEntrega(codigoUsuario);
			cartaCia.setCodigoUsuarioModificacion(codigoUsuario);
			recuperacion.setImporte(recuperacion.getImporte().subtract(cartaCia.getMontoExclusion()));
			this.entidadService.save(recuperacion);
		}
		actualizarMonto(recuperacion, cartaCia);
		this.entidadService.save(cartaCia);
		
		
	}
	
	private void actualizarMonto(RecuperacionCompania recuperacion, CartaCompania cartaCia){
		List<Ingreso> ingresos = this.entidadService.findByProperty(Ingreso.class, "recuperacion.id", recuperacion.getId());
		if ( !ingresos.isEmpty() && cartaCia.getMontoExclusion()!=null &&  cartaCia.getMontoExclusion().compareTo(BigDecimal.ZERO)!=0 ){
			ingresos.get(0).setMonto(recuperacion.getImporte());
			this.entidadService.save(ingresos.get(0));
		}
	}
	
	/**
	 * Asociar y guardar una Carta Compania a una Recuperacion.
	 * 
	 * Obtener el ultimo folio existente de las Cartas Compania asociadas, de no
	 * existir ninguna carta se generara con el folio 0.
	 * 
	 * @param cartaCia
	 * @param recuperacionId
	 */
	@Override
	public CartaCompania guardarCartaCia(CartaCompania cartaCia,
			Long recuperacionId) {
		RecuperacionCompania recuperacion =this.entidadService.findById(RecuperacionCompania.class, recuperacionId);
		if(null==cartaCia.getId()){
			cartaCia.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			cartaCia.setFechaCreacion(new Date());
			cartaCia.setFolio(new Long(0));
			cartaCia.setEstatus(CartaCompania.EstatusCartaCompania.REGISTRADO.codigo);
			cartaCia.setRecuperacion(recuperacion);
			cartaCia.setMontoARecuperar(recuperacion.getImporte());
		}else{
			cartaCia.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			cartaCia.setFechaModificacion(new Date()); 
		}
		cartaCia.setRecuperacion(recuperacion);
		this.entidadService.save(cartaCia);
		return cartaCia;
	}
	

	
	/**
	 * M�todo que genera/actualiza una Recuperaci�n de Compa��a Manual asociando el
	 * pase de atenci�n/cobertura y las Ordenes de Compra seleccionadas .
	 * 
	 * i
	 * Invocar al m�todo <b><i>obtenerMontoSalvamento </i></b>y asignaro al
	 * montoSalvamento el objeto recuperacion.
	 * 
	 * Invocar al m�todo <b><i>obtenerOrdenesCompraCia </i></b>enviando el String de
	 * ordenes y soloSeleccionados = true.
	 * 
	 * Sumar los importes de montoARecuperar de las ordenes y asignarlo al atributo
	 * montoIndemnizacion de la recuperacion
	 * 
	 * Validar que los montos  sean correctos en las Ordenes de Compra y Salvamento.
	 * 
	 * Invocar al m�todo <b><i>validarRecuperacionCia</i></b>.
	 * Si no hay errores guardar la recuperacion como medio de Recuperacion REEMBOLSO
	 * y origen MANUAL . Calcular la fecha de Cobro en base a la RDN, sumar 2 a�os
	 * (730 d�as) a la Fecha de Ocurrido del Siniestro
	 * 
	 * 
	 * Invocar al metodo <b><i>guardarCartaCia</i></b>
	 * 
	 * Asociar las Ordenes de Compra obtenidas a la Carta Compania.
	 * 
	 * Invocar al m�todo <b><i>guardarCoberturasRecuperacion </i></b>y
	 * <b><i>guardarPasesRecuperacion  </i></b>(heredados de RecuperacionService)
	 * 
	 * @param recuperacion
	 * @param carta
	 * @param cobertura
	 * @param pase
	 * @param ordenes
	 */
	/*@Override
	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String validarRecuperacionCiaManual(RecuperacionCompania recuperacion,
			CartaCompania carta, String cobertura, String pase, String ordenes) {
		
		Map<String,Object> respuesta = new HashMap<String,Object>();
		respuesta.put("validacion", "");
		respuesta.put("recuperacion", "");
		
		 if(recuperacion.getReporteCabina()!= null &&  recuperacion.getReporteCabina().getId()!= null ){
			 ReporteCabina reporteCabina= this.entidadService.findById(ReporteCabina.class,  recuperacion.getReporteCabina().getId());
			 recuperacion.setReporteCabina(reporteCabina);
		 }
		if(null==recuperacion.getId()){
			recuperacion.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			recuperacion.setFechaCreacion(new Date());
			Long numero=this.entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO);
			recuperacion.setNumero(numero);
			recuperacion.setMedio(Recuperacion.MedioRecuperacion.REEMBOLSO.toString());
			recuperacion.setOrigen(Recuperacion.OrigenRecuperacion.MANUAL.toString());
			recuperacion.setTipoOrdenCompra(Recuperacion.TipoOrdenCompra.AFECTACION.name());
		}else{
			RecuperacionCompania recuperacionOriginal = entidadService.findById(RecuperacionCompania.class, recuperacion.getId());
			recuperacionOriginal.setPolizaCia(recuperacion.getPolizaCia());
			recuperacionOriginal.setSiniestroCia(recuperacion.getSiniestroCia());
			recuperacionOriginal.setPaseAtencionCia(recuperacion.getPaseAtencionCia());
			recuperacionOriginal.setPorcentajeParticipacion(recuperacion.getPorcentajeParticipacion());
			recuperacionOriginal.setMontoGAGrua(recuperacion.getMontoGAGrua());
			recuperacionOriginal.setReporteCabina(recuperacion.getReporteCabina());
			recuperacionOriginal.setFechaCreacion(recuperacionOriginal.getFechaCreacion());
			recuperacionOriginal.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			//recuperacionOriginal.setFechaModificacion(new Date());		
			
			recuperacion = recuperacionOriginal;
		}
		respuesta.put("recuperacion", recuperacion);
		
		//Validaciones 
		String validacion = this.validarRecuperacionCia(recuperacion);
		respuesta.put("validacion", validacion);
		
		/*if(StringUtils.isEmpty(validacion)){
			RecuperacionCiaService processor = this.sessionContext.getBusinessObject(RecuperacionCiaService.class);
			RecuperacionCompania recuperacionNueva = processor.creaYGuardaCiaManualProcessor(recuperacion,carta, cobertura, pase, ordenes);
		}*/

		/*return "";
		
	}*/
	
	@Override
	public String guardarCiaYProvisiona(RecuperacionCompania recuperacion,CartaCompania carta, String cobertura, String pase, String ordenes){
		BigDecimal totalGruasInicial = BigDecimal.ZERO;
		EstimacionCoberturaReporteCabina estimacion = null;
		if(recuperacion.getId()!=null){
			estimacion = this.siniestrosProvisionService.obtenerEstimacionDeRecuperacionCia(recuperacion.getId());
			totalGruasInicial = this.siniestroProvisionDao.obtenerSumaGruasPorEstimacion( estimacion.getId() );
		}
		RecuperacionCiaService processor = this.sessionContext.getBusinessObject(RecuperacionCiaService.class);
		String validacion = processor.guardarRecuperacionCiaManual(recuperacion, carta, cobertura, pase, ordenes);
		if(validacion == null){
			this.siniestrosProvisionService.creaMovimientosAlAsignarGruasRecuperacionCia(recuperacion.getId(), totalGruasInicial);
			estimacion = (estimacion == null)?this.siniestrosProvisionService.obtenerEstimacionDeRecuperacionCia(recuperacion.getId()):estimacion;
			this.siniestrosProvisionService.actualizaProvision(estimacion.getId(), MovimientoProvisionSiniestros.Origen.PASE);
		}
		return validacion;
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public RecuperacionCompania guardarProvisionarCiaMontos(Long recuperacionId,BigDecimal montoGrua, String ordenes,String pase,String cobertura){
		
		RecuperacionCompania recuperacion = this.entidadService.findById(RecuperacionCompania.class, recuperacionId);
		
		if( recuperacion == null ){
			
			recuperacion.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			recuperacion.setFechaCreacion(new Date());
			Long numero=this.entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO);
			recuperacion.setNumero(numero);
			recuperacion.setMedio(Recuperacion.MedioRecuperacion.REEMBOLSO.toString());
			recuperacion.setOrigen(Recuperacion.OrigenRecuperacion.MANUAL.toString());
			recuperacion.setTipoOrdenCompra(Recuperacion.TipoOrdenCompra.AFECTACION.name());
//			this.entidadService.save(recuperacion);
		}
		
		// OBTENER MONTO GRUAS PROVISIONADAS
//		EstimacionCoberturaReporteCabina estimacion = this.siniestrosProvisionService.obtenerEstimacionDeRecuperacionCia(recuperacion.getId());
//		BigDecimal totalGruas = this.siniestroProvisionDao.obtenerSumaGruasPorEstimacion( estimacion.getId() );
		
		//Calculo Importes 
		RecuperacionRelacionSiniestroDTO importes = this.calculaImportes(ordenes, recuperacion.getPorcentajeParticipacion(), 
				montoGrua, this.getIdPase(pase), this.getIdCcobertura(cobertura));
		recuperacion.setMontoGAGrua(importes.getMontoGrua());
		recuperacion.setMontoIndemnizacion(importes.getMontoIndemniza());
		recuperacion.setMontoSalvamento(importes.getMontoSalvamento());
		recuperacion.setImporte(importes.getMontoTotal());
		recuperacion.setMontoPiezas(importes.getMontoPiezas());
		recuperacion.setFechaModificacion(new Date());
		
		// INVOCAR SALVADO DE MONTOS
		recuperacion = guardarProvisionarCiaMontosProcessor(recuperacion);
		
//		this.siniestrosProvisionService.creaMovimientosAlAsignarGruasRecuperacionCia(recuperacionId, totalGruas);
		
//		siniestrosProvisionService.actualizaProvisionAlAsignarGruas( recuperacionFinalMontos.getId() , totalGruas );
		
		return recuperacion;
		
	}
	
	@Override
	public void salvarRecuperacion(RecuperacionCompania recuperacion){
		this.entidadService.save(recuperacion);
	}
	
	@Override
	public RecuperacionCompania guardarProvisionarCiaMontosProcessor(RecuperacionCompania recuperacion){
	
		
		ReporteCabina reporte = this.entidadService.findById(ReporteCabina.class, recuperacion.getReporteCabina().getId());
		if(null!=reporte && null!=reporte.getFechaOcurrido()){
			Date fecha=DateUtils.add(reporte.getFechaOcurrido(), Indicador.Days, diasLimiteCobro);
			recuperacion.setFechaLimiteCobro(fecha) ;
		}	
		//patch
		if(recuperacion.getRecuperacionOriginal() != null && recuperacion.getRecuperacionOriginal().getId() == null){
			recuperacion.setRecuperacionOriginal(null); //no tiene una recuperacion original	
		}	
		
		recuperacion =  this.entidadService.save(recuperacion);

		
		return recuperacion;
	}	
	
	@Override	
	public String guardarRecuperacionCiaManualProvision(RecuperacionCompania recuperacion,
			CartaCompania carta, String cobertura, String pase, String ordenes) {
		String msjGuardado = "";
		
		// OBTENER MONTO DE GRUAS INICIAL
		//EstimacionCoberturaReporteCabina estimacion = this.siniestrosProvisionService.obtenerEstimacionDeRecuperacionCia(recuperacion.getId());
		//BigDecimal totalGruas = this.siniestroProvisionDao.obtenerSumaGruasPorEstimacion( estimacion.getId() );
		
		//TODO CAMBIAR NOMBRE A VALIDAR
		//Map<String,Object> respuesta = this.validarRecuperacionCiaManual(recuperacion,carta, cobertura, pase, ordenes);
		
//		if(recuperacion.getReporteCabina()!= null &&  recuperacion.getReporteCabina().getId()!= null ){
//			 ReporteCabina reporteCabina= this.entidadService.findById(ReporteCabina.class,  recuperacion.getReporteCabina().getId());
//			 recuperacion.setReporteCabina(reporteCabina);
//		}
//		
//		if(null==recuperacion.getId()){
//			recuperacion.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
//			recuperacion.setFechaCreacion(new Date());
//			Long numero=this.entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO);
//			recuperacion.setNumero(numero);
//			recuperacion.setMedio(Recuperacion.MedioRecuperacion.REEMBOLSO.toString());
//			recuperacion.setOrigen(Recuperacion.OrigenRecuperacion.MANUAL.toString());
//			recuperacion.setTipoOrdenCompra(Recuperacion.TipoOrdenCompra.AFECTACION.name());
//		}else{
//			RecuperacionCompania recuperacionOriginal = entidadService.findById(RecuperacionCompania.class, recuperacion.getId());
//			recuperacionOriginal.setPolizaCia(recuperacion.getPolizaCia());
//			recuperacionOriginal.setSiniestroCia(recuperacion.getSiniestroCia());
//			recuperacionOriginal.setPaseAtencionCia(recuperacion.getPaseAtencionCia());
//			recuperacionOriginal.setPorcentajeParticipacion(recuperacion.getPorcentajeParticipacion());
//			recuperacionOriginal.setReporteCabina(recuperacion.getReporteCabina());
//			recuperacionOriginal.setFechaCreacion(recuperacionOriginal.getFechaCreacion());
//			recuperacionOriginal.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
//			recuperacionOriginal.setFechaModificacion(new Date());		
//			recuperacion = recuperacionOriginal;
//		}	
//		
//		msjGuardado = this.validarRecuperacionCia(recuperacion);
//	
//		//Validaciones 
//		if( StringUtil.isEmpty(msjGuardado) ){
//			
//			try{
//					 this.guardarRecuperacionCia(
//							recuperacion ,
//							carta, 
//							cobertura, 
//							pase, 
//							ordenes,
//							null);									
//					
//			}catch(Exception e){
//					Log.error("Error al provisionar grua. -guardarRecuperacionCiaManualProvision ", e);
//					msjGuardado = "Error al guardar la recuperación".concat(e.getMessage());			
//			}			
//		}
		return msjGuardado;	
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String guardarRecuperacionCiaManual(RecuperacionCompania recuperacion,
			CartaCompania carta, String cobertura, String pase, String ordenes) {
		 if(recuperacion.getReporteCabina()!= null &&  recuperacion.getReporteCabina().getId()!= null ){
			 ReporteCabina reporteCabina= this.entidadService.findById(ReporteCabina.class,  recuperacion.getReporteCabina().getId());
			 recuperacion.setReporteCabina(reporteCabina);
		 }
		if(null==recuperacion.getId()){
			recuperacion.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			recuperacion.setFechaCreacion(new Date());
			Long numero=this.entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO);
			recuperacion.setNumero(numero);
			recuperacion.setMedio(Recuperacion.MedioRecuperacion.REEMBOLSO.toString());
			recuperacion.setOrigen(Recuperacion.OrigenRecuperacion.MANUAL.toString());
			recuperacion.setTipoOrdenCompra(Recuperacion.TipoOrdenCompra.AFECTACION.name());
		}else{
			RecuperacionCompania recuperacionOriginal = entidadService.findById(RecuperacionCompania.class, recuperacion.getId());
			recuperacionOriginal.setPolizaCia(recuperacion.getPolizaCia());
			recuperacionOriginal.setSiniestroCia(recuperacion.getSiniestroCia());
			recuperacionOriginal.setPaseAtencionCia(recuperacion.getPaseAtencionCia());
			recuperacionOriginal.setPorcentajeParticipacion(recuperacion.getPorcentajeParticipacion());
			recuperacionOriginal.setMontoGAGrua(recuperacion.getMontoGAGrua());
			recuperacionOriginal.setMontoPiezas(recuperacion.getMontoPiezas());
			recuperacionOriginal.setReporteCabina(recuperacion.getReporteCabina());
			recuperacionOriginal.setFechaCreacion(recuperacionOriginal.getFechaCreacion());
			recuperacionOriginal.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			recuperacionOriginal.setFechaModificacion(new Date());		
			
			recuperacion = recuperacionOriginal;
		}
		//Validaciones 
		String validacion = this.validarRecuperacionCia(recuperacion,null);
		if(!StringUtils.isEmpty(validacion)){
			return validacion;
		}
		this.creaYGuardaCiaManual(recuperacion,carta, cobertura, pase, ordenes);
		return null;
	}
	
	@Override
	public  Map<String,Object> validarRecuperacionCiaManual(RecuperacionCompania recuperacion,CartaCompania carta, String cobertura, String pase, String ordenes){
		
		/*Map<String,Object> datos = new HashMap<String,Object>();
		
		if(recuperacion.getReporteCabina()!= null &&  recuperacion.getReporteCabina().getId()!= null ){
			 ReporteCabina reporteCabina= this.entidadService.findById(ReporteCabina.class,  recuperacion.getReporteCabina().getId());
			 recuperacion.setReporteCabina(reporteCabina);
		}
		if(null==recuperacion.getId()){
			recuperacion.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			recuperacion.setFechaCreacion(new Date());
			Long numero=this.entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO);
			recuperacion.setNumero(numero);
			recuperacion.setMedio(Recuperacion.MedioRecuperacion.REEMBOLSO.toString());
			recuperacion.setOrigen(Recuperacion.OrigenRecuperacion.MANUAL.toString());
			recuperacion.setTipoOrdenCompra(Recuperacion.TipoOrdenCompra.AFECTACION.name());
		}else{
			RecuperacionCompania recuperacionOriginal = entidadService.findById(RecuperacionCompania.class, recuperacion.getId());
			recuperacionOriginal.setPolizaCia(recuperacion.getPolizaCia());
			recuperacionOriginal.setSiniestroCia(recuperacion.getSiniestroCia());
			recuperacionOriginal.setPaseAtencionCia(recuperacion.getPaseAtencionCia());
			recuperacionOriginal.setPorcentajeParticipacion(recuperacion.getPorcentajeParticipacion());
			//recuperacionOriginal.setMontoGAGrua(recuperacion.getMontoGAGrua());
			recuperacionOriginal.setReporteCabina(recuperacion.getReporteCabina());
			recuperacionOriginal.setFechaCreacion(recuperacionOriginal.getFechaCreacion());
			recuperacionOriginal.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			//recuperacionOriginal.setFechaModificacion(new Date());		
			recuperacion = recuperacionOriginal;
		}	
		
		datos.put("recuperacion",recuperacion);
		
		String validacion = this.validarRecuperacionCia(recuperacion);
		datos.put("validacion",validacion);*/
		
		return null;
	}
	
	
	/*@Override
	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public RecuperacionCompania creaYGuardaCiaManualProcessor(RecuperacionCompania recuperacion,
			CartaCompania carta, String cobertura, String pase, String ordenes){
		
		//Calculo Importes 
		RecuperacionRelacionSiniestroDTO importes = this.calculaImportes(ordenes, recuperacion.getPorcentajeParticipacion(), recuperacion.getMontoGAGrua(), this.getIdPase(pase), this.getIdCcobertura(cobertura));
		recuperacion.setMontoGAGrua(importes.getMontoGrua());
		recuperacion.setMontoIndemnizacion(importes.getMontoIndemniza());
		recuperacion.setMontoSalvamento(importes.getMontoSalvamento());
		recuperacion.setImporte(importes.getMontoTotal());
		recuperacion.setMontoPiezas(importes.getMontoPiezas());
		
		ReporteCabina reporte = this.entidadService.findById(ReporteCabina.class, recuperacion.getReporteCabina().getId());
		if(null!=reporte && null!=reporte.getFechaOcurrido()){
			Date fecha=DateUtils.add(reporte.getFechaOcurrido(), Indicador.Days, diasLimiteCobro);
			recuperacion.setFechaLimiteCobro(fecha) ;
		}	
		//patch
		if(recuperacion.getRecuperacionOriginal() != null && recuperacion.getRecuperacionOriginal().getId() == null){
			recuperacion.setRecuperacionOriginal(null); //no tiene una recuperacion original	
		}	
		
		recuperacion =  this.entidadService.save(recuperacion);
		return recuperacion;
	}*/
	
	
	@Override
	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public RecuperacionCompania guardarRecuperacionCia(RecuperacionCompania recuperacion,
			CartaCompania carta, String cobertura, String pase, String ordenes, BigDecimal montoGruasProvisionado){
		
		//Calculo Importes 
		/*RecuperacionRelacionSiniestroDTO importes = this.calculaImportes(ordenes, recuperacion.getPorcentajeParticipacion(), 
				recuperacion.getMontoGAGrua(), this.getIdPase(pase), this.getIdCcobertura(cobertura));
		recuperacion.setMontoGAGrua(importes.getMontoGrua());
		recuperacion.setMontoIndemnizacion(importes.getMontoIndemniza());
		recuperacion.setMontoSalvamento(importes.getMontoSalvamento());
		recuperacion.setImporte(importes.getMontoTotal());
		recuperacion.setMontoPiezas(importes.getMontoPiezas());
		recuperacion.setFechaModificacion(new Date());
		
		ReporteCabina reporte = this.entidadService.findById(ReporteCabina.class, recuperacion.getReporteCabina().getId());
		if(null!=reporte && null!=reporte.getFechaOcurrido()){
			Date fecha=DateUtils.add(reporte.getFechaOcurrido(), Indicador.Days, diasLimiteCobro);
			recuperacion.setFechaLimiteCobro(fecha) ;
		}	
		//patch
		if(recuperacion.getRecuperacionOriginal() != null && recuperacion.getRecuperacionOriginal().getId() == null){
			recuperacion.setRecuperacionOriginal(null); //no tiene una recuperacion original	
		}	
		
		recuperacion =  this.entidadService.save(recuperacion);*/
		
		
		if(null!=carta){
			carta.setMontoARecuperar(recuperacion.getImporte());
			this.guardarCartaCia(carta, recuperacion.getId());
		}
	
		//Eliminar Coberturas y pases anteriores. 
		if(null!=recuperacion && null!=recuperacion.getId()){
			List<CoberturaRecuperacion> coberturas=this.getCoberturasRecuperacion(recuperacion.getId());
			if(null!= coberturas && !coberturas.isEmpty()){
				this.entidadService.removeAll(coberturas);
			}
		}
		
		if(!StringUtil.isEmpty(cobertura)){
 			this.guardarCoberturasRecuperacion(recuperacion.getId(), cobertura);
		}
		
		
 		if(!StringUtil.isEmpty(pase)){
 			this.guardarPasesRecuperacion (recuperacion.getId(), pase); 			
 		}
 		
		asociarOrdenesCompraRecuperacion(recuperacion,ordenes );
		
		return recuperacion;
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public RecuperacionCompania creaYGuardaCiaManual(RecuperacionCompania recuperacion,
			CartaCompania carta, String cobertura, String pase, String ordenes){
		
		//Calculo Importes 
		RecuperacionRelacionSiniestroDTO importes = this.calculaImportes(ordenes, recuperacion.getPorcentajeParticipacion(), recuperacion.getMontoGAGrua(), this.getIdPase(pase), this.getIdCcobertura(cobertura));
		recuperacion.setMontoGAGrua(importes.getMontoGrua());
		recuperacion.setMontoIndemnizacion(importes.getMontoIndemniza());
		recuperacion.setMontoSalvamento(importes.getMontoSalvamento());
		recuperacion.setImporte(importes.getMontoTotal());
		
		ReporteCabina reporte = this.entidadService.findById(ReporteCabina.class, recuperacion.getReporteCabina().getId());
		if(null!=reporte && null!=reporte.getFechaOcurrido()){
			Date fecha=DateUtils.add(reporte.getFechaOcurrido(), Indicador.Days, diasLimiteCobro);
			recuperacion.setFechaLimiteCobro(fecha) ;
		}	
		//patch
		if(recuperacion.getRecuperacionOriginal() != null && recuperacion.getRecuperacionOriginal().getId() == null){
			recuperacion.setRecuperacionOriginal(null); //no tiene una recuperacion original	
		}	
		this.entidadService.save(recuperacion);	
		if(null!=carta){
			carta.setMontoARecuperar(recuperacion.getImporte());
			this.guardarCartaCia(carta, recuperacion.getId());
		}
		//Eliminar Coberturas y pases anteriores. 
		if(null!=recuperacion && null!=recuperacion.getId()){
			List<CoberturaRecuperacion> coberturas=this.getCoberturasRecuperacion(recuperacion.getId());
			if(null!= coberturas && !coberturas.isEmpty()){
				this.entidadService.removeAll(coberturas);
			}
		}
		if(!StringUtil.isEmpty(cobertura)){
 			this.guardarCoberturasRecuperacion(recuperacion.getId(), cobertura);
 		} 		
 		if(!StringUtil.isEmpty(pase)){
 			this.guardarPasesRecuperacion (recuperacion.getId(), pase);
 		}
		asociarOrdenesCompraRecuperacion(recuperacion,ordenes );
		return recuperacion;
	}
	
	/*@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardarCoberturasRecuperacionProcessor(Long recuperacionId, String coberturasConcat){
		this.guardarCoberturasRecuperacion(recuperacionId, coberturasConcat);
	}*/	
	
	/*@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveRecuperacionCiaProcessor(RecuperacionCompania recuperacion){
		this.entidadService.save(recuperacion);	
	}*/
	
	/*@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardarCartaCiaProcessor(CartaCompania carta, Long recuperacionId){
		this.guardarCartaCia(carta, recuperacionId);
	}*/	
	
	/*@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void eliminarCoberturarPasesAtencion(RecuperacionCompania recuperacion){
		
		if(null!=recuperacion && null!=recuperacion.getId()){
			List<CoberturaRecuperacion> coberturas=this.getCoberturasRecuperacion(recuperacion.getId());
			if(null!= coberturas && !coberturas.isEmpty()){
				this.entidadService.removeAll(coberturas);
			}
		}
	}*/
	
	/*@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardarPasesRecuperacionProcessor(Long recuperacionId, String pase){
		System.out.println("test");
		this.guardarPasesRecuperacion(recuperacionId, pase);
	}*/
	
	/*@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)	
	public void asociarOrdenesCompraRecuperacionProcessor(RecuperacionCompania recuperacion,String ordenes){
		this.asociarOrdenesCompraRecuperacion(recuperacion,ordenes );
	}*/
	
	private void asociarOrdenesCompraRecuperacion(RecuperacionCompania recuperacion,String ordenes){
		if(null!=recuperacion && null!=recuperacion.getId()){
			List<OrdenCompraCartaCia>  listOC=this.obtenerOrdenesCompraCia(recuperacion.getId(), Boolean.FALSE);
			if(null!=listOC && !listOC.isEmpty()){
				this.entidadService.removeAll(listOC);
			}
		}
		List<OrdenCompraCartaCia>  listOC=this.obtenerOCestimacion(null,null, null, null, ordenes, recuperacion.getPorcentajeParticipacion(), true);
		if(null!=listOC && !listOC.isEmpty()){
			for ( OrdenCompraCartaCia oc :listOC){
				if(null==oc.getId()){
					oc.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
					oc.setFechaCreacion(new Date());				
				}else{
					oc.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
					oc.setFechaModificacion(new Date()); 
				}
				oc.setRecuperacion(recuperacion);
				this.entidadService.save(oc);
			}
		}
		
	}
	/**
	 * <b>SE DEFINE EN DISE�O DE REGISTRAR SEGUIMIENTO DE RECUPERACI�N DE
	 * COMPA��A</b>
	 * M�todo que genera y asocia un seguimiento a una Recuperaci�n de Compa��a
	 * 
	 * Obtener la <b>recuperacion </b>por medio de entidadService.findById.
	 * 
	 * Crear el objeto de SeguimientoRecuperacion con la <b>fechaCreacion </b>= fecha
	 * actual, <b>usuarioCreacion </b>= usuario actual y el <b>comentario </b>de
	 * par�metro.
	 * 
	 * Asociar el seguimiento a la <b>recuperacion</b>
	 * 
	 * @param recuperacionId
	 * @param comentario
	 */
	@Override
	public void guardarSeguimiento(Long recuperacionId, String comentario) {
		SeguimientoRecuperacion seguimientoRecuperacion = new SeguimientoRecuperacion();
		RecuperacionCompania recuperacionCompania = entidadService.findById(RecuperacionCompania.class, recuperacionId); 
		seguimientoRecuperacion.setRecuperacion(recuperacionCompania);
		seguimientoRecuperacion.setComentario(comentario);
		seguimientoRecuperacion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		seguimientoRecuperacion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		Date fechaCreacion = new Date();
		seguimientoRecuperacion.setFechaCreacion(fechaCreacion);
		seguimientoRecuperacion.setFechaModificacion(fechaCreacion);
		entidadService.save(seguimientoRecuperacion);
	}
	/**
	 * 
	 * <b>SE AGREGA EN DISE�O REGISTRAR REDOCUMENTACION</b>
	 * M�todo que retorna la carta activa.
	 * 
	 * Hacer un entidadService.findByPropertyWithOrder por el recuperacion.id =
	 * recuperacionId y ordenar por folio, retornar la carta con el folio m�s actual
	 * 
	 * @param recuperacionId
	 */
	@Override
	public CartaCompania obtenerCartaActiva(Long recuperacionId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("recuperacion.id", recuperacionId);
		List<CartaCompania>  lstCartaCompania = entidadService.findByPropertiesWithOrder(CartaCompania.class, params, "folio desc");
		if(CollectionUtils.isNotEmpty(lstCartaCompania)){
			CartaCompania carta = lstCartaCompania.get(0);//ultima carta activa
			for(CartaCompania cartaUltima :lstCartaCompania ){//obtener ultoma fecha acuse
				if(cartaUltima.getFechaAcuse()!= null){
					carta.setFechaAcuse(cartaUltima.getFechaAcuse());
					break;
				}
			}
			return carta;
		}else 
			return null;
	}


	/**
	 * 
	 * @param recuperacionCiaId
	 */
	@Override
	public CartaCompania obtenerCartaCompania(Long recuperacionCiaId) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("recuperacion.id", recuperacionCiaId);
			params.put("folio", 0);
			CartaCompania carta =  this.entidadService.findByPropertiesWithOrder(CartaCompania.class, params, "folio desc").get(0);
			return carta;
		}catch(Exception e){return null;}

	}
	/**
	 * <b>SE AGREGA EN DISE�O REGISTRAR REDOCUMENTACION</b>
	 * 
	 * Invocar al m�todo<b><i> recuperacionCiaDao.obtenerCartasRedoc</i></b>
	 * 
	 * @param recuperacionId
	 */
	@Override
	public List<CartaCompania> obtenerCartasRedoc(Long recuperacionId) {
		return this.recuperacionCiaDao.obtenerCartasRedoc(recuperacionId);
	}

	/**
	 * Obtener el importe del Salvamento para la Estimaci�n de la Recuperacion de
	 * Compa��as.
	 * 
	 * Hacer un entidadService.findByProperties sobre RecuperacionSalvamento con el
	 * <b>estimacion.id = estimacionId</b> y que el estatus = RECUPERADO. Al obtenerlo
	 * invocar al m�todo del objeto <b><i>recuperacion.obtenerVentaActiva</i></b> y
	 * obtener el atributo <b>subtotal </b>de la <b>venta</b>
	 * 
	 * @param estimacionId
	 */
	@Override
	public BigDecimal obtenerMontoSalvamento(Long estimacionId) {
		Map<String, Object> params = new HashMap<String, Object>();	
		params.put("estatus", Recuperacion.EstatusRecuperacion.RECUPERADO.toString());
		params.put("estimacion.id", estimacionId);
		List<RecuperacionSalvamento>  lstRecuperacion  =this.entidadService.findByProperties(RecuperacionSalvamento.class, params);
		BigDecimal total = BigDecimal.ZERO;
		if(null!=lstRecuperacion){
			for (RecuperacionSalvamento recuperacion : lstRecuperacion){
				if(null!= recuperacion.obtenerVentaActiva()  && null!=recuperacion.obtenerVentaActiva().getTotalVenta())
					total = total.add(recuperacion.obtenerVentaActiva().getSubtotal());
			}
		}
		return total;
	}  
	/**
	 * Invocar al m�todo <b><i>recuperacionCiaDao.obtenerOCestimacion</i></b>
	 * 
	 * @param estimacionId
	 * @param ordenesConcat
	 */
	@Override
	public List<OrdenCompraCartaCia> obtenerOCestimacion(Long recuperacionId, Long reporteCabinaId, Long coberturaId, Long estimacionId, String ordenesConcat,
			Integer porcentajeParticipacion, Boolean soloSeleccionados) {
		return this.recuperacionCiaDao.obtenerOCestimacion(recuperacionId,reporteCabinaId, coberturaId, estimacionId, ordenesConcat, porcentajeParticipacion, soloSeleccionados);
	}
	/**
	 * <b>SE AGREGA EN DISE�O EDITAR RECUPERACION COMPA�IA</b>
	 * 
	 * Hacer un entidadService.findByProperties sobre <b><i>OrdenCompraCia
	 * </i></b>usando recuperacion.id = <b>recuperacionCiaId </b>y retornar el listado
	 * 
	 * @param recuperacionCiaId
	 */
	@Override
	public List<OrdenCompraCartaCia> obtenerOrdenesCompraCia(Long recuperacionCiaId, boolean soloAfectacionReserva) {	
		RecuperacionCompania recuperacion = this.entidadService.findById(RecuperacionCompania.class, recuperacionCiaId);
		
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("recuperacion.id", recuperacionCiaId);
		if (soloAfectacionReserva) {
			params.put("ordenCompra.tipo", OrdenCompraService.TIPO_AFECTACION_RESERVA);
		}
		List<OrdenCompraCartaCia>  ordenes =this.entidadService.findByProperties(OrdenCompraCartaCia.class, params);
		for (OrdenCompraCartaCia orden:ordenes){
			orden.setSeleccionado(true);
			orden.setMontoPiezasConPorcentaje((orden.getMontoPiezasConPorcentaje()==null)?BigDecimal.ZERO:orden.getMontoPiezasConPorcentaje());
			orden.setPorcentajeParticipacion(recuperacion.getPorcentajeParticipacion());
			if(orden.getOrdenCompra()!=null && orden.getOrdenCompra().getIndemnizacion()!=null){
				orden.setIsIndemnizacion(true); 
			}else{
				orden.setIsIndemnizacion(false);
			}
			OrdenCompra  ordenCompra =this.entidadService.findById(OrdenCompra.class, orden.getOrdenCompra().getId());
			if(!StringUtil.isEmpty(ordenCompra.getDetalleConceptos())){
				String[] conceptos=  ordenCompra.getDetalleConceptos().split(",") ;
				if(conceptos.length>1){
					orden.setConceptoDesc("VARIOS.");
				}else {
					orden.setConceptoDesc(ordenCompra.getDetalleConceptos());
				}
			}
			if (StringUtils.equals(OrdenCompraService.TIPO_AFECTACION_RESERVA, ordenCompra.getTipo())) {
				orden.setTipoOcDesc("Reserva");
			} else if (StringUtils.equals(OrdenCompraService.TIPO_GASTOAJUSTE, ordenCompra.getTipo())) {
				orden.setTipoOcDesc("Gasto Ajuste Grúa");
			}
		}
		return ordenes;
	}
	/**
	 * Hacer un entidadService.findById sobre RecuperacionCompania y retornar
	 * 
	 * @param recuperacionCiaId
	 */
	@Override
	public RecuperacionCompania obtenerRecuperacionCia(Long recuperacionCiaId) {
		if(null==recuperacionCiaId){
			return null;
		}
		RecuperacionCompania recuperacionCompania= this.entidadService.findById(RecuperacionCompania.class, recuperacionCiaId);
		return recuperacionCompania;
	}
	/**
	 * <b>SE DEFINE EN DISE�O DE MOSTRAR RECUPERACIONES CIA POR VENCER</b>
	 * 
	 * Invocar al m�todo <b>recuperacionCiaDao.obtenerRecuperacionesVencer</b>
	 */
	@Override
	public List<RecuperacionCompania> obtenerRecuperacionesVencer() {
		return recuperacionCiaDao.obtenerRecuperacionesVencer();
	}
	/**

	 * @param recuperacionId
	 */
	@Override
	public List<SeguimientoRecuperacion> obtenerSeguimientos(Long recuperacionId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("recuperacion.id", recuperacionId);
	
		List<SeguimientoRecuperacion> list = entidadService.findByPropertiesWithOrder(SeguimientoRecuperacion.class, params, "fechaCreacion DESC");
		
		for(SeguimientoRecuperacion s : CollectionUtils.emptyIfNull(list)){
			Usuario usuario = usuarioService.buscarUsuarioPorNombreUsuario(s.getCodigoUsuarioCreacion());
			s.setUsuarioCreacionNombre(usuario.getNombreCompleto());

		}
		return list;
	}
	/**
	 * <b>SE DEFINE EN DISE�O DE CANCELAR RECUPERACION COMPA�IA</b>
	 * 
	 * M�todo que envia una Solicitud de Cancelaci�n al rol definido en RDN.
	 * 
	 * Actualizar la recuperacion con la <b>fechaSolCancelacion</b>, el
	 * <b>motivoCancelacion </b>y el <b>usuarioSolCancelacion. </b>Actualizar el
	 * <b>estatus </b>de la �ltima Carta a<b> PEND_CANC</b>
	 * <b>
	 * </b>Enviar un correo por medio de envioNotificacionService
	 * 
	 * @param recuperacionId
	 */
	@Override
	public void solicitarCancelacion(Long recuperacionId,String motivoCancelacion) {
		RecuperacionCompania recuperacion = this.entidadService.findById(RecuperacionCompania.class, recuperacionId);
 		recuperacion.setMotivoCancelacion(motivoCancelacion);	
		recuperacion.setFechaSolCancelacion(new Date());
		recuperacion.setUsuarioSolicitudCancelacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
		recuperacion.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
		recuperacion.setFechaModificacion(new Date());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("recuperacion.id", recuperacionId);
		params.put("folio", 0);
		List<CartaCompania>  lstCartaCompania=this.entidadService.findByPropertiesWithOrder(CartaCompania.class, params, "folio desc");
		this.entidadService.save(recuperacion);
		if(null!=lstCartaCompania && !lstCartaCompania.isEmpty()){
			CartaCompania cartaCia = lstCartaCompania.get(0);
			cartaCia.setEstatus(CartaCompania.EstatusCartaCompania.PENDIENTE_CANCELAR.codigo);
			cartaCia.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			cartaCia.setFechaModificacion(new Date()); 
			this.entidadService.save(cartaCia);
			this.notificarCancelacion(recuperacion);
		}
	}
	
	private void notificarCancelacion(RecuperacionCompania recuperacion) {
		String[] nombresRol  = {"Rol_M2_Gerente_Operaciones_Indemnizacion","Rol_M2_Coordinador_Cobro_Companias_Salvamentos"};
		EnvioNotificacionesService.EmailDestinaratios destinos = new EnvioNotificacionesService.EmailDestinaratios ();
	   // destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.PARA,recuperacion.getCorreoComprador(),prestador.getPersonaMidas().getNombre()); 
	    List<Usuario> users = usuarioService.buscarUsuariosPorNombreRol(nombresRol);
	    for(Usuario user : CollectionUtils.emptyIfNull(users)){
	    	if(!StringUtil.isEmpty(user.getEmail()))
	    		destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.PARA, user.getEmail(), user.getNombreCompleto());
	    }
	    // La configuracion ya tiene un correo dado de alta, aquí se está agregando otro en Runtime.                      
	    //Mapa con los datos a reemplazar en el template
	    HashMap<String, Serializable> data = new HashMap<String, Serializable>();        
		//data.put("secuenciaEnvio", 1);
		data.put("numero", recuperacion.getNumero());
		String nombre ="";
		if(null!=recuperacion.getCompania()  &&  null!=recuperacion.getCompania().getNombrePersona()){
			nombre =recuperacion.getCompania().getNombrePersona();
		}
		data.put("cia", nombre);
		data.put("monto", recuperacion.getImporte());
		String desc = this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.MOTIVOS_CANCELACION_INGRESO, recuperacion.getMotivoCancelacion());
		data.put("motivo", desc);
		Usuario user=usuarioService.buscarUsuarioPorNombreUsuario(recuperacion.getUsuarioSolicitudCancelacion());
		data.put("usuario",user.getNombreCompleto());
	    // Envio de la notificacion, el folio y el destino pueden ser nulos. 
	    //Cuando se desea enviar una notificacion recurrente es recomendable agregar un folio, ya que eventualmente se deberá apagar la recurrencia cuando se hayan cumplido las condiciones.
		envioNotificacionesService.enviarNotificacion(EnumCodigo.NOTIF_SOLICITUD_CANCELA_REC_CIA.toString(), data, recuperacion.getId().toString(), destinos);

	}
	/**
	 * Validar que la generaci�n de la Recuperaci�n de Compania cumpla con las RDN
	 * establecidas.
	 * 
	 * Validar quese cuenta con un Reporte de Cabina asociado, de caso contrario
	 * mandar un mensaje de error.
	 * 
	 * Validar que se tenga un Pase de Atenci�n o una Cobertura (que no genere Pase de
	 * Atenci�n) seleccionada.
	 * 
	 * Que se cuente con una Compa�ia seleccionada, un num de poliza y num de
	 * Sineistro.
	 * 
	 * Que el porcentaje se encuentre entre 1 y 100%
	 * 
	 * Que el termino de ajuste del Siniestro sea SE RECIBIO ORDEN DE CIA.
	 * Para esto buscar sobre AutoIncisoReporteCabina asociado al Reporte y que cuente
	 * con el terminoAjuste = EOC
	 * 
	 * @param recuperacionCia
	 */
	public static final String TERMINOAJUSTE_RECIBIO_ORDEN_CIA = "EOC";

	@Override
	public String validarRecuperacionCia(RecuperacionCompania recuperacionCia,Long idPaseAtencion) {
		//Validar quese cuenta con un Reporte de Cabina asociado, de caso contrario
		if(null==recuperacionCia.getReporteCabina() && null==recuperacionCia.getReporteCabina().getId()){
			return "La recuperacion debe tener un Reporte Cabina Asociado";
		}
		//Validar que se tenga un Pase de Atenci�n o una Cobertura (que no genere Pase de
				 // Atenci�n) seleccionada.
	/**TODO PENDIENTE */
		
		//Que se cuente con una Compa�ia seleccionada, un num de poliza y num de  Sineistro.
		if(null==recuperacionCia.getCompania() || null==recuperacionCia.getCompania()){
			return "Debe Seleccionar la Compa\u00f1ia Responsable.";
		}
		
		if(StringUtil.isEmpty(recuperacionCia.getPolizaCia() )){
			return "Debe Capturar el numero de Poliza Compa\u00f1ia.";
		}
		
		if(StringUtil.isEmpty(recuperacionCia.getSiniestroCia())){
			return "Debe Capturar el numero de Siniestro Compa\u00f1ia.";
		}
		
		if(null==recuperacionCia.getPorcentajeParticipacion()){
			return "Debe Capturar el Porcentaje de Participac\u00edn.";
		}
		if(recuperacionCia.getPorcentajeParticipacion()<=0   ||  recuperacionCia.getPorcentajeParticipacion()>100    ){
			return "El Porcentaje de Participac\u00edn debe ser en un Rango de 0% a 100% ";
		}
		
		
//		EstimacionCoberturaReporteCabina estimacion = this.siniestrosProvisionService.obtenerEstimacionDeRecuperacionCia(recuperacionCia.getId());
		if(idPaseAtencion==null && recuperacionCia.getId()!=null){
			EstimacionCoberturaReporteCabina estimacion = this.siniestrosProvisionService.obtenerEstimacionDeRecuperacionCia(recuperacionCia.getId());
			idPaseAtencion = estimacion.getId();
		}
		if(idPaseAtencion!=null){
			List<RecuperacionProveedor> proveedores = recuperacionDao.obtenerRecuperacionesProveedorPendientes(idPaseAtencion);
			if(proveedores!=null && proveedores.size()>0){
				return "Existen Recuperaciones de Proveedor Pendientes";
			}
			
			List<RecuperacionCompania> companiasPendientes = this.recuperacionCiaDao.obtenerRecuperacionPendientePorRecuperar(idPaseAtencion);
			if(recuperacionCia.getId()==null && companiasPendientes.size()>0){
				return "Existe una Recuperaciones de Compañia Pendiente";
			}else if(companiasPendientes!=null && !companiasPendientes.isEmpty() && companiasPendientes.size()>1){
				return "Existe una Recuperaciones de Compañia Pendiente";
			}
		}
		
		//ReporteCabina reporte = this.entidadService.findById(ReporteCabina.class, recuperacionCia.getReporteCabina().getId());
		
		//AutoIncisoReporteCabina auto=reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();
		/*if(!auto.getTerminoAjuste().equalsIgnoreCase(TERMINOAJUSTE_RECIBIO_ORDEN_CIA)){
			return "El Termio de Ajuste del Siniestro no es del tipo: "+this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO, TERMINOAJUSTE_RECIBIO_ORDEN_CIA);
		}*/
		return null;
	}
	@Override
	public EstimacionCoberturaReporteCabina obtieneEstimacion (Long idCoberturaRepCabina,Long idEstimacionCoberturaRepCabina ){
		EstimacionCoberturaReporteCabina estimacion =null;
		EstimacionCoberturaSiniestroDTO estimaDto= null;
		String tipoEstimacion = TipoEstimacion.DIRECTA.getValue();
		CoberturaReporteCabina cobertura = this.entidadService.findById(CoberturaReporteCabina.class,idCoberturaRepCabina);
		if(null==idEstimacionCoberturaRepCabina){
			 estimaDto= this.estimacionService.obtenerDatosEstimacionCobertura(cobertura.getId(),cobertura.getCoberturaDTO().getClaveTipoCalculo(),tipoEstimacion);
			 if( null!=estimaDto.getEstimacionGenerica() &&  null!= estimaDto.getEstimacionGenerica().getId()){
					estimacion=estimaDto.getEstimacionGenerica();
			}
		 }else{
			 estimacion=this.entidadService.findById(EstimacionCoberturaReporteCabina.class,idEstimacionCoberturaRepCabina);
		 }
		return estimacion;
	}
	@Override
	public RecuperacionRelacionSiniestroDTO obtenerDatosRelacionSiniestro(Long idCoberturaRepCabina,String cveSubTipoCalculo,  Long idEstimacionCoberturaRepCabina){
		RecuperacionRelacionSiniestroDTO dto = new RecuperacionRelacionSiniestroDTO();		 
		EstimacionCoberturaSiniestroDTO estimaDto= null;
		BigDecimal montoSalvamento = BigDecimal.ZERO;
		BigDecimal montoGrua= BigDecimal.ZERO;
		CoberturaReporteCabina cobertura = this.entidadService.findById(CoberturaReporteCabina.class,idCoberturaRepCabina);
		dto.setIdCoberturaRepCabina(cobertura.getId());
		EstimacionCoberturaReporteCabina estimacion = this.obtieneEstimacion(idCoberturaRepCabina, idEstimacionCoberturaRepCabina);
		if(null==estimacion){
			dto.setMensaje("Debe Seleccionar pase de Atencion");
			return dto;
		}	
		estimaDto= this.estimacionService.obtenerDatosEstimacionCobertura(cobertura.getId(),cobertura.getCoberturaDTO().getClaveTipoCalculo(),estimacion.getTipoEstimacion());
		dto.setNumeroPoliza(estimaDto.getDatosReporte().getNumeroPoliza());
		if(null!= estimaDto.getReporteCabina().getLugarOcurrido() && null!=estimaDto.getReporteCabina().getLugarOcurrido().getEstado() &&  null!=estimaDto.getReporteCabina().getLugarOcurrido().getEstado().getDescripcion())
			dto.setEstadoUbicacion(estimaDto.getReporteCabina().getLugarOcurrido().getEstado().getDescripcion());
		if(null!= estimaDto.getReporteCabina().getLugarOcurrido() && null!=estimaDto.getReporteCabina().getLugarOcurrido().getCiudad() &&  null!=estimaDto.getReporteCabina().getLugarOcurrido().getCiudad().getDescripcion())
			dto.setCiudadUbicacion(estimaDto.getReporteCabina().getLugarOcurrido().getCiudad().getDescripcion());
		if(null!= estimaDto.getReporteCabina().getLugarOcurrido() && null!=estimaDto.getReporteCabina().getLugarOcurrido().getCalleNumero())
			dto.setLugarSiniestro(estimaDto.getReporteCabina().getLugarOcurrido().getCalleNumero());
		if(null!=estimaDto.getReporteCabina().getFechaOcurrido()){
			String fechaFormat=Utilerias.cadenaDeFecha(estimaDto.getReporteCabina().getFechaOcurrido()) ;
			dto.setFechaSiniestro(estimaDto.getReporteCabina().getFechaOcurrido() );		
			dto.setFechaSiniestroStr(fechaFormat);
		}
		dto.setTipoPerdida("");
		// IndemnizacionSiniestro indemniza
		Map<String, Object> params = new HashMap<String, Object>();	
		params.put("ordenCompra.estatus", OrdenCompraService.ESTATUS_INDEMNIZACION_FINALIZADA);	
		params.put("ordenCompra.coberturaReporteCabina.id", cobertura.getId());
		if(null!=estimacion && null!=estimacion.getId()){
			params.put("ordenCompra.idTercero", estimacion.getId());
			dto.setIdEstimacionCoberturaRepCabina(estimacion.getId());
			if( this.estimacioService.esPerdidaTotal(estimacion.getId())){
				dto.setTipoPerdida(this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.TIPO_PERDIDA, "T"));			 
			}else{
				dto.setTipoPerdida(this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.TIPO_PERDIDA, "P"));
			}
		}

		/* List<IndemnizacionSiniestro>  lstIndemniza = this.entidadService.findByProperties(IndemnizacionSiniestro.class, params);
		 if(null!= lstIndemniza && !lstIndemniza.isEmpty()){
			 IndemnizacionSiniestro indemniza = lstIndemniza.get(0);
			 if(null!= indemniza.getOrdenCompraGenerada() && null!= indemniza.getOrdenCompraGenerada().getOrdenPago()) {
				if( this.liquidacionSiniestroService.tieneIndemnizacionesPT(indemniza.getOrdenCompraGenerada().getOrdenPago())){
					dto.setTipoPerdida(this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.TIPO_PERDIDA, "T"));
				}else{
					dto.setTipoPerdida(this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.TIPO_PERDIDA, "P"));;
				}
			 }

		 }*/
		//Tipo de Pérdida - Se obtiene revisando la indemnización de la Cobertura, si tiene determinación de Pérdida Total, entonces es Pérdida Total, de lo contrario es Pérdida Parcial.
		List<AutoIncisoReporteCabina> autoIncisoList =this.entidadService.findByProperty(AutoIncisoReporteCabina.class, "incisoReporteCabina.seccionReporteCabina.reporteCabina.id", estimaDto.getReporteCabina().getId());
		AutoIncisoReporteCabina autoInciso = autoIncisoList.get(0);
//		AutoIncisoReporteCabina autoInciso =estimaDto.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();
		dto.setVehiculoAsegurado(autoInciso.getDescMarca()+", "+autoInciso.getDescEstilo()+", "+autoInciso.getModeloVehiculo()+", "+autoInciso.getNumeroSerie());

		dto.setPorcParticipacion(autoInciso.getPorcentajeParticipacion());
		dto.setNoSiniestrosCia(autoInciso.getSiniestroCia());	
		dto.setNumPolizaCia(autoInciso.getPolizaCia());
		if (!StringUtils.isEmpty(autoInciso.getCiaSeguros())) {
			PrestadorServicio compania = entidadService.findById(PrestadorServicio.class, Integer.parseInt(autoInciso.getCiaSeguros()));
			dto.setCompaniaId(compania.getId().longValue());
			dto.setNombreCompania(compania.getPersonaMidas().getNombre());
		}

		if( estimacion != null){

			if (EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, estimacion.getTipoEstimacion())) {
				dto.setPersonaLesionada(((TerceroGastosMedicos) estimacion).getNombreAfectado());
				dto.setConceptoCarta(dto.getPersonaLesionada());
			} 
			
			if (EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, estimacion.getTipoEstimacion())) {
				dto.setPersonaLesionada(((TerceroRCPersonas) estimacion).getNombreAfectado());
				dto.setConceptoCarta(dto.getPersonaLesionada());
			} 
			
			if (EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, estimacion.getTipoEstimacion())) {
				dto.setPersonaLesionada(((TerceroRCViajero) estimacion).getNombreAfectado());
				dto.setConceptoCarta(dto.getPersonaLesionada());
			} 
			
			if (EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, estimacion.getTipoEstimacion())) {
				dto.setPersonaLesionada(((TerceroGastosMedicosConductor) estimacion).getNombreAfectado());
				dto.setConceptoCarta(dto.getPersonaLesionada());
			} 
			if (EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, estimacion.getTipoEstimacion())) {
				TerceroRCVehiculo terceroRCVehiculo = (TerceroRCVehiculo) estimacion;
				dto.setVehiculoTercero(terceroRCVehiculo.getMarca()+", "+terceroRCVehiculo.getEstiloVehiculo()+", "+terceroRCVehiculo.getModeloVehiculo()+", "+terceroRCVehiculo.getNumeroSerie());
				dto.setConceptoCarta(dto.getTipoPerdida()+" - "+dto.getVehiculoTercero());
			} 
			if (EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, estimacion.getTipoEstimacion())) {
				dto.setConceptoCarta(dto.getTipoPerdida()+" - "+dto.getVehiculoAsegurado());
			}

			if(null==dto.getConceptoCarta()){
				dto.setConceptoCarta("");
			}
		}
		dto.setTermioAjuste(cobertura.getIncisoReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getTerminoAjuste());

		//Calcula importes
		montoSalvamento = this.obtenerMontoSalvamento(estimacion.getId());
		
		dto.setMontoInicial(movimientoSiniestroService.obtenerMontoEstimacionAfectacion(estimacion.getId()));
		dto.setMontoSalvamento(montoSalvamento);	
		dto.setMontoGrua(montoGrua);
		dto.setMontoTotal(montoGrua.subtract(montoSalvamento));
		dto.setCartaComplemento(false);
		return dto;
	}
	
	@Override
	public RecuperacionRelacionSiniestroDTO obtenerDatosRelacionSiniestro(
			Long recuperacionId) {
		RecuperacionCompania recuperacion =this.entidadService.findById(RecuperacionCompania.class, recuperacionId);
		if(null==recuperacion){
			return null;
		}		
		Long idCobertura =null ; Long idPaseAtencion =null;
		List<CoberturaRecuperacion> lstCoberturas=this.getCoberturasRecuperacion(recuperacionId);
		List<EstimacionCoberturaReporteCabina> lstEstimacion=this.getPasesRecuperacion(recuperacionId);
		if(null!=lstCoberturas && !lstCoberturas.isEmpty()){
			CoberturaRecuperacion coberturaRecuperacion=lstCoberturas.get(0);
			idCobertura= (null!=coberturaRecuperacion.getCoberturaReporte() && null!=coberturaRecuperacion.getCoberturaReporte().getId() ) ? coberturaRecuperacion.getCoberturaReporte().getId(): null;
		}
		if(null!=lstEstimacion && !lstEstimacion.isEmpty()){
			EstimacionCoberturaReporteCabina paseAtn=lstEstimacion.get(0);
			idPaseAtencion= (null!=paseAtn && null!=paseAtn.getId() ) ? paseAtn.getId() : null;
		}
		List<OrdenCompraCartaCia> ordenes=this.obtenerOrdenesCompraCia(recuperacionId, Boolean.FALSE);
		String ordenesConcat= this.concatenaListaOrdenesCia(ordenes);		
		RecuperacionRelacionSiniestroDTO datos= this.obtenerDatosRelacionSiniestro(idCobertura, null, idPaseAtencion);
		RecuperacionRelacionSiniestroDTO importes=this.calculaImportes(ordenesConcat, recuperacion.getPorcentajeParticipacion(), recuperacion.getMontoGAGrua(), idPaseAtencion,idCobertura);
		if(null!=importes){
			datos.setMontoInicial(datos.getMontoInicial());
			datos.setMontoGrua(importes.getMontoGrua());
			datos.setMontoIndemniza(importes.getMontoIndemniza());
			datos.setMontoSalvamento(importes.getMontoSalvamento());
			datos.setMontoTotal(importes.getMontoTotal());
			// TODO : MONTO PIEZAS
		}
		
		return datos;
		
	}

	@Override
	public RecuperacionCompania inicializaRecuperacionCompania(
			RecuperacionCompania recuperacionCompania) {
		if(null ==recuperacionCompania){
			recuperacionCompania= new RecuperacionCompania();
		}
		recuperacionCompania.setEstatus(Recuperacion.EstatusRecuperacion.REGISTRADO.toString());
		recuperacionCompania.setMedio(Recuperacion.MedioRecuperacion.REEMBOLSO.toString());
		recuperacionCompania.setPorcentajeParticipacion(0);
		recuperacionCompania.setOrigen(Recuperacion.OrigenRecuperacion.MANUAL.toString());
		recuperacionCompania.setFechaCreacion(new Date());
		recuperacionCompania.setTipo(Recuperacion.TipoRecuperacion.COMPANIA.toString());
		recuperacionCompania.setActivo(Boolean.TRUE); 
		recuperacionCompania.setPorcentajeParticipacion(0);
		recuperacionCompania.setPaseAtencionCia("ORIGINAL");
		recuperacionCompania.setMontoPiezas(BigDecimal.ZERO);
		return recuperacionCompania;
	}
	
	public String concatenaListaOrdenesCia(List<OrdenCompraCartaCia> lstOrdenesCompra){
		StringBuilder concat =new StringBuilder("");
		if(null!= lstOrdenesCompra && ! lstOrdenesCompra.isEmpty()){
			Iterator<OrdenCompraCartaCia> iteratorPases = lstOrdenesCompra.iterator();
		    while (iteratorPases.hasNext()) {		
		    	OrdenCompraCartaCia oc  = iteratorPases.next();
		    	concat.append(oc.getOrdenCompra().getId());
		        if (iteratorPases.hasNext()) {
		        	concat.append(",");
		        }
		    }
		}
		return concat.toString();
	}
	@Override
	public RecuperacionRelacionSiniestroDTO calculaImportes(
			String ordenesConcat, Integer porcentajeParticipacion,
			BigDecimal montoGAGrua, Long idEstimacion, Long idCobertura) {
		RecuperacionRelacionSiniestroDTO infoImportes =new RecuperacionRelacionSiniestroDTO();
		BigDecimal montoRecuperar = BigDecimal.ZERO;
		BigDecimal total = BigDecimal.ZERO;
		BigDecimal tMontoGAGrua = BigDecimal.ZERO;
		boolean esCadenaVacia = false;
		
		// SI SOLO VIENE LA CADENA ',' INDICA QUE TODOS LOS ELEMENTOS DEL GRID SE HAN DESELECCIONADO Y 
		// DEBERA PONER LA GRUA EN 0
		esCadenaVacia = (ordenesConcat.trim().length() == 1 ? true:false);
		
		if (porcentajeParticipacion == null) {
			porcentajeParticipacion=0;
		}
		if(null==idEstimacion && null!=idCobertura){
			EstimacionCoberturaReporteCabina pase= this.obtieneEstimacion(idCobertura, idEstimacion);
			if(null!=pase && null!=pase.getId()){
				idEstimacion=pase.getId();
			}
		}
		if (montoGAGrua == null || esCadenaVacia ) {
			montoGAGrua = BigDecimal.ZERO;
		}
		BigDecimal montoSalvamento=  BigDecimal.ZERO;
		BigDecimal montoPiezas=  BigDecimal.ZERO;
		boolean calculaSalvamento= false; 
		if(!StringUtil.isEmpty(ordenesConcat)){
			List<OrdenCompraCartaCia> lstOrdenSelect=this.obtenerOCestimacion(null,null, null, null, ordenesConcat, porcentajeParticipacion, true);
			if( CollectionUtils.isNotEmpty(lstOrdenSelect)) {
				for (OrdenCompraCartaCia orden:lstOrdenSelect) {
					if(orden.getMontoPiezasConPorcentaje()!=null && orden.getMontoPiezasConPorcentaje().compareTo(BigDecimal.ZERO)!=0){
						montoPiezas = montoPiezas.add(orden.getMontoPiezasConPorcentaje());
					}
					if(orden.getIsIndemnizacion()){
						calculaSalvamento=true;
					}
					if(orden.getMontoARecuperar() != null ){
						if (StringUtils.equals(orden.getOrdenCompra().getTipo(), OrdenCompraService.TIPO_GASTOAJUSTE)) {
							tMontoGAGrua = tMontoGAGrua.add(orden.getMontoARecuperar());
						} else if (StringUtils.equals(orden.getOrdenCompra().getTipo(), OrdenCompraService.TIPO_AFECTACION_RESERVA)) {
							montoRecuperar = montoRecuperar.add(orden.getMontoARecuperar());
						}						
					}
				}
			}
			
			if(calculaSalvamento){
				//montoSalvamento=  (null!=idEstimacion) ? this.obtenerMontoSalvamento(idEstimacion) : BigDecimal.ZERO;	
				montoSalvamento = this.obtenerMontoPiezasSalvamento(idEstimacion).get("montoSalvamento");
			}
			if (tMontoGAGrua.compareTo(montoGAGrua) > 0) {
				montoGAGrua = tMontoGAGrua;
			}
			total = montoRecuperar.subtract(montoSalvamento).subtract(montoPiezas).add(montoGAGrua);
		}
		
		infoImportes.setMontoIndemniza(montoRecuperar);
		infoImportes.setMontoTotal(total);
		infoImportes.setMontoGrua(montoGAGrua);
		infoImportes.setMontoSalvamento(montoSalvamento);
		infoImportes.setMontoPiezas(montoPiezas);
		return infoImportes;

	}
	
	
	public Long getIdCcobertura(String coberturasSeleccionadas){
		Long idCobertura =null;
		if(!StringUtil.isEmpty(coberturasSeleccionadas)){			
			coberturasSeleccionadas= coberturasSeleccionadas.trim();
			String[] coberturaLst=  coberturasSeleccionadas.split("\\|"); 
			idCobertura = new Long (coberturaLst[0]);				
		}
		return idCobertura;
		
	}
	
	
	public Long getIdPase(String pasesSeleccionadas){
		Long idPaseAtencion=null;		
		if(!StringUtil.isEmpty(pasesSeleccionadas)){			
			pasesSeleccionadas= pasesSeleccionadas.trim();
			String[] paseslst=  pasesSeleccionadas.split("\\,"); 
			idPaseAtencion = new Long (paseslst[0]);
		}
		return idPaseAtencion;
	}

	
	/**
	 * Método que retorna un dto con la info de una Carta Compañía
	 * @param cartaCiaId
	 * @return
	 */
	private CartaCiaDTO obtenerInfoCarta(Long cartaCiaId) {
		CartaCiaDTO cartaCiaDTO = new CartaCiaDTO();
		
		CartaCompania carta = entidadService.findById(CartaCompania.class, cartaCiaId);
		
		RecuperacionCompania recuperacion = carta.getRecuperacion();
		
		cartaCiaDTO.setNumeroRecuperacion(recuperacion.getNumero().toString());
		
		EstimacionCoberturaReporteCabina estimacion = null;
		CoberturaReporteCabina cobertura = null;
		

		List<PaseRecuperacion> pase = entidadService.findByProperty(PaseRecuperacion.class, "coberturaRecuperacion.recuperacion.id", carta.getRecuperacion().getId());
		
		if (CollectionUtils.isNotEmpty(pase)) {
			estimacion = pase.get(0).getEstimacionCobertura();
			cobertura = estimacion.getCoberturaReporteCabina();
		}

		cartaCiaDTO.setCartaId(cartaCiaId);
		
		cartaCiaDTO.setNombreCia(recuperacion.getCompania().getNombrePersona());
		
		
		Map<String, Object> paramsSiniestro = new HashMap<String, Object>();
		paramsSiniestro.put("reporteCabina.id", recuperacion.getReporteCabina().getId());
		List<SiniestroCabina> siniestros = entidadService.findByProperties(SiniestroCabina.class, paramsSiniestro);
		SiniestroCabina siniestro = siniestros.get(0);
		
		cartaCiaDTO.setSiniestro(siniestro.getNumeroSiniestro());
		
		cartaCiaDTO.setSiniestroCia(recuperacion.getSiniestroCia());
		
		cartaCiaDTO.setPolizaCia(recuperacion.getPolizaCia());
		
		AutoIncisoReporteCabina auto = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(recuperacion.getReporteCabina().getId());;
		String descVehiculo = ((auto.getDescMarca() != null)? (auto.getDescMarca() + "-") : "") 
							+ ((auto.getDescEstilo() != null)? auto.getDescEstilo() + "-" : "")
							+ ((auto.getModeloVehiculo() != null)? auto.getModeloVehiculo() + "-" : "")
							+ ((auto.getNumeroSerie() != null)? auto.getNumeroSerie() : "");
		
		
		cartaCiaDTO.setVehiculo(descVehiculo);
		
		if (estimacion != null && EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, estimacion.getTipoEstimacion())) {
			
			TerceroRCVehiculo vehiculoTercero = (TerceroRCVehiculo) estimacion;
			String descVehiculoTercero = ((vehiculoTercero.getMarca() != null)? (vehiculoTercero.getMarca() + "-") : "") 
							+ ((vehiculoTercero.getEstiloVehiculo() != null)? vehiculoTercero.getEstiloVehiculo() + "-" : "")
							+ ((vehiculoTercero.getModeloVehiculo() != null)? vehiculoTercero.getModeloVehiculo() + "-" : "")
							+ ((vehiculoTercero.getNumeroSerie() != null)? vehiculoTercero.getNumeroSerie() : "");
			cartaCiaDTO.setVehiculoTercero(descVehiculoTercero);
		
		}
		
		if (estimacion != null && (	EnumUtil.equalsValue(estimacion.getTipoEstimacion(), TipoEstimacion.RC_PERSONA, 
				TipoEstimacion.RC_VIAJERO,	TipoEstimacion.GASTOS_MEDICOS, TipoEstimacion.GASTOS_MEDICOS_OCUPANTES))) {

			cartaCiaDTO.setLesionado(estimacion.getNombreAfectado());
		}
		
		cartaCiaDTO.setNombreCobertura(
				(EnumUtil.equalsValue(ClaveTipoCalculo.ROBO_TOTAL, cobertura.getClaveTipoCalculo()))?
						estimacionService.obtenerNombreCobertura(cobertura.getCoberturaDTO(), 
								cobertura.getClaveTipoCalculo(), null, 
								null, cobertura.getCoberturaDTO().getTipoConfiguracion()) :
									estimacionService.obtenerNombreCobertura(estimacion.getCoberturaReporteCabina().getCoberturaDTO(), 
											estimacion.getCoberturaReporteCabina().getClaveTipoCalculo(), estimacion.getCveSubCalculo(), 
											estimacion.getTipoEstimacion(), estimacion.getCoberturaReporteCabina().getCoberturaDTO().getTipoConfiguracion()));
		
			
		
		cartaCiaDTO.setFechaSiniestro(recuperacion.getReporteCabina().getFechaOcurrido());
		
		cartaCiaDTO.setTipoPase(recuperacion.getPaseAtencionCia());
		
		if (!StringUtil.isEmpty(carta.getUsuarioElaboracion())) {
			Usuario usuarioElaboracion = usuarioService.buscarUsuarioPorNombreUsuario(carta.getUsuarioElaboracion());
			
			if (usuarioElaboracion != null) {
				cartaCiaDTO.setUsuario(usuarioElaboracion.getNombreCompleto());
			}
		}
		
		
		cartaCiaDTO.setImporte(recuperacion.getImporte());
		
		cartaCiaDTO.setFolio(String.valueOf(carta.getFolio()));
		
		//TODO: Folio Anterior se ocupa el campo esComplemento en recuperacion		
		//cartaCiaDTO.setFolioAnterior()
		
		if (carta.getFolio() != 0L) {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("recuperacion.id", recuperacion.getId());
			params.put("folio", 0L);
			
			List<CartaCompania> cartaOriginal = entidadService.findByProperties(CartaCompania.class, params);
			
			if (cartaOriginal != null && !cartaOriginal.isEmpty()) {
				cartaCiaDTO.setFechaAcuse(cartaOriginal.get(0).getFechaAcuse());
			}
			
		} else {
			cartaCiaDTO.setFechaAcuse(carta.getFechaAcuse());
		}
		
		cartaCiaDTO.setMontoIndemnizacion(recuperacion.getMontoIndemnizacion());
		
		cartaCiaDTO.setMontoSalvamento(recuperacion.getMontoSalvamento());
		
		cartaCiaDTO.setMontoPiezas(recuperacion.getMontoPiezas());
		
		cartaCiaDTO.setMontoGAGrua(recuperacion.getMontoGAGrua());
		
		cartaCiaDTO.setEsRedocumentada(carta.getFolio() == 0L ? false: true);
		
		//TODO: Se ocupa el campo esComplemento en recuperacion		
		//cartaCiaDTO.setEsComplemento
		
		//se determina si la carta es de perdidatotal
		if (EnumUtil.equalsValue(estimacion.getTipoEstimacion(), TipoEstimacion.RC_VEHICULO, TipoEstimacion.DANIOS_MATERIALES)) {
			
			cartaCiaDTO.setEsPerdidaTotal(estimacionService.esPerdidaTotal(estimacion.getId()));
		}
		
		//despues de ver si es perdida total, ya que lo usa para determinar el tipo de carta que es
		cartaCiaDTO.setTipoCarta(obtenerTipoCartaCompaniaImpresion(cartaCiaDTO, estimacion, cobertura));
		
		if (recuperacion.getReporteCabina().getLugarOcurrido() != null) {
			String lugar = "";
			
			if (recuperacion.getReporteCabina().getLugarOcurrido().getCiudad() != null) {
				lugar+=recuperacion.getReporteCabina().getLugarOcurrido().getCiudad().getDescripcion();
				lugar+= '-';
			}
			
			if (recuperacion.getReporteCabina().getLugarOcurrido().getCiudad() != null) {
				lugar+=recuperacion.getReporteCabina().getLugarOcurrido().getEstado().getDescripcion();
			}			
			
			cartaCiaDTO.setLugar(lugar);
		}		
		
		cartaCiaDTO.setFechaElaboraCarta(carta.getFechaElaboracion());
		
		cartaCiaDTO.setFechaUltimoAcuse(carta.getFechaAcuse());
		
		cartaCiaDTO.setObservaciones(carta.getComentarios());
		
		if (carta.getFechaAcuse() != null) {
			cartaCiaDTO.setDiasAtraso(Days.daysBetween(TimeUtils.getDateTime(carta.getFechaAcuse()), TimeUtils.now()).getDays());
		}
		
		return cartaCiaDTO;
	}

	
	@Override
	public String asignarReferenciaBancaria(String recuperacionesConcat, String referencia) {
		String refError = "";
		StringBuilder recuperacionesResp = new StringBuilder("");
		if(referencia.matches(PATTERN_REFERENCIABANCARIA)){
			String[] recuperaciones = recuperacionesConcat.split(",");
			for(String element : recuperaciones){
				RecuperacionCompania r = entidadService.findById(RecuperacionCompania.class, Long.valueOf(element));
				CartaCompania ultimaCarta = this.obtenerCartaActiva(r.getId());
				//Se valida que la recuperacion este en estatus Registrado.
				if(r.getEstatus().equals(Recuperacion.EstatusRecuperacion.PENDIENTE.toString()) &&
				   r.getReferenciasBancarias().isEmpty() &&
				   ultimaCarta.getEstatus().equals(CartaCompania.EstatusCartaCompania.ENTREGADO.codigo)){
					r.setReferenciaBancaria(referencia);
					r.setFechaModificacion(new Date());
					r.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
					entidadService.save(r);
				}else{
					recuperacionesResp.append(r.getNumero()).append(",");
				}
			}
		}else{
			refError = "El formato de la referencia es incorrecto.";
		}
		String recuperacionResp = recuperacionesResp.toString();
		if (!recuperacionResp.equals("")){
			refError = "No fue posible asignar la referencia a las siguientes Recuperaciones: " + recuperacionResp.substring(0, recuperacionResp.length()-1) + ".";
		}
		return refError.equals("") ? null : refError;
	}
	
	@Override
	public String eliminarReferenciaBancaria(String recuperacionesConcat) {
		String refError = "";
		StringBuilder recuperacionesResp = new StringBuilder("");
		String ingresosResp = "";
		String[] recuperaciones = recuperacionesConcat.split(",");
		for(String element : recuperaciones){
			RecuperacionCompania r = entidadService.findById(RecuperacionCompania.class, Long.valueOf(element));
			//Se valida que la recuperacion este en estatus Pendiente por Recuperar.
			if(r.getEstatus().equals(Recuperacion.EstatusRecuperacion.PENDIENTE.toString()) &&
			   CommonUtils.isValid(r.getReferenciaBancaria())){
				r.setReferenciaBancaria(null);
				r.setEstatus(Recuperacion.EstatusRecuperacion.REGISTRADO.toString());
				r.setFechaModificacion(new Date());
				r.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
				try{
					Ingreso ingreso = ingresoService.obtenerIngresoActivo(r.getId());
					if (ingreso != null) {
						ingresoService.cancelarIngresoPendientePorAplicar(ingreso, MOTIVO_CANCELACION_INGRESO);
					}
					
				}catch(Exception e){
					ingresosResp += r.getNumero()+ ",";
				}
				entidadService.save(r);
			}else{
				recuperacionesResp.append(r.getNumero()).append(",");
			}
		}
		String recuperacionResp=recuperacionesResp.toString();
		if (!recuperacionResp.equals("")){
			refError = "No fue posible eliminar la referencia a las siguientes Recuperaciones: " + recuperacionResp.substring(0, recuperacionResp.length()-1) + ".\n";
		}
		if (!ingresosResp.equals("")){
			refError += "No fue posible cancelar el ingreso de las siguientes Recuperaciones: " + ingresosResp.substring(0, ingresosResp.length()-1) + ".";
		}
	return refError.equals("") ? null : refError;

	}
	
	@Override
	public List<RecuperacionCiaDTO> buscarRecuperacionesCia(RecuperacionCiaDTO recuperacionCiaFiltro) {
		List<RecuperacionCompania> recuperaciones = recuperacionCiaDao.buscarRecuperacionesCia(recuperacionCiaFiltro);
		return convertir(recuperaciones);
	}
	


	/**
	 * Método que genera el Estado de cuenta de las Compañías que no han realizado la recuperación de Compañías. 
	 * Este proceso se debe ejecutar el primer día de cada mes. Se deberá enviar  por cada Compañía un correo con un excel 
	 * con estado de cuenta agrupando  las Cartas que estan Pendientes

	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	//@Schedule( hour="*", persistent= false)
	@Schedule( hour="2", dayOfMonth="1", persistent= false)
	//@Schedule( minute="*", hour="*", dayOfMonth="*", month="*", year="*", persistent= false)
	public void generarEstadoCuenta() {

		log.info("generarEstadoCuenta");
		
		List<PrestadorServicio> companias = recuperacionCiaDao.obtenerCiasPendientePago();

		for (PrestadorServicio compania: CollectionUtils.emptyIfNull(companias)) {

			List<CartaCompania> cartas = recuperacionCiaDao.obtenerCartasCiaSinPago(Long.parseLong(String.valueOf(compania.getId())));

			List<CartaCiaDTO> cartasdto = new ArrayList<CartaCiaDTO>();

			for (CartaCompania carta: CollectionUtils.emptyIfNull(cartas)) {
				CartaCiaDTO cartadto = obtenerInfoCarta(carta.getId());
				
				cartasdto.add(cartadto);
			}
			
			ExcelExporter exporter = new ExcelExporter( CartaCiaDTO.class, CartaCiaDTO.DOCUMENTO_EDOCUENTA);
			TransporteImpresionDTO transporte = exporter.exportXLS(cartasdto, "Cartas pendientes");
			
			List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
			ByteArrayAttachment attatchment =  new ByteArrayAttachment();
			
			attatchment.setTipoArchivo(TipoArchivo.XLS);
			try {
				attatchment.setContenidoArchivo(IOUtils.toByteArray(transporte.getGenericInputStream()));
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
			attatchment.setNombreArchivo(transporte.getFileName());
			
			adjuntos.add(attatchment);			
			
			String[] nombresRol = new String[2];
			nombresRol[0] = "Rol_M2_Analista_Recuperaciones_Plaza";
			nombresRol[1] = "Rol_M2_Coordinador_Cobro_Companias_Salvamentos";
			
			EnvioNotificacionesService.EmailDestinaratios destinos = new EnvioNotificacionesService.EmailDestinaratios();

			List<Usuario> usuarios  = new ArrayList<Usuario>();

			try{
				usuarios = usuarioService.buscarUsuariosPorNombreRol(nombresRol);
				
				for (Usuario us : CollectionUtils.emptyIfNull(usuarios)) {
					if (!StringUtil.isEmpty(us.getEmail()))
						destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.CC, us.getEmail(), us.getNombreCompleto());
				}			
			}catch(Exception e){
				logger.error(e.getMessage(), e);
			}

			HashMap<String, Serializable> data = new HashMap<String, Serializable>();
		
			data.put("nombreCia", compania.getNombrePersona());			
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			String nameMonth = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, new Locale("es","ES"));
			data.put("mes",  StringUtils.capitalize(nameMonth));
			
			if (compania.getPersonaMidas().getContacto() != null && !StringUtil.isEmpty(compania.getPersonaMidas().getContacto().getCorreoPrincipal())) {
				
				destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.CC, compania.getPersonaMidas().getContacto().getCorreoPrincipal(), "Cia");
				notificacionService.enviarNotificacion(EnumCodigo.SN_RCIA_EDO_CUENTA.toString(), data, "1", destinos, adjuntos);
			} else {
				notificacionService.enviarNotificacion(EnumCodigo.SN_RCIA_EDO_CUENTA_SNCORREO.toString(), data, "1", destinos, adjuntos);
			}
		}
	}
	
	
	/**
	 * Método que generar una nueva Carta Compañía para aquellas Recuperaciones de Compañía que 
	 * necesitan ser Redocumentadas por que cuenten con más de 90 días sin Pago desde la última Fecha de Acuse.
	 * Ejecutar diariamente
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Schedule(minute="0", hour="2",  persistent= false)
	//@Schedule( minute="*", hour="*", dayOfMonth="*", month="*", year="*", persistent= false)
	@Override
	public void redocumentarCartas() {

		List<CartaCompania> cartas = recuperacionCiaDao.obtenerCartasARedoc();
		
		for (CartaCompania carta : CollectionUtils.emptyIfNull(cartas)) {
			
			carta.setEstatus(EstatusCartaCompania.POR_REDOCUMENTAR.getValue());
			carta.setFechaModificacion(new Date());
			
			entidadService.save(carta);
			
			bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.REDOCUMENTAR_CARTA, String.valueOf(carta.getId()), 
					"Se actualiza la carta: " + carta.getId() + " a estatus POR REDOCUMENTAR por job de Redocumentacion", 
					carta, "SISTEMAS");
		}		

	}
	
	/**
	 * Método que envia notificación de las Cartas Compañía pendientes a Elaborar 
	 * donde hayan pasado 15 días desde que se registró el último pago del Pase de Atención 
	 * y la Reserva sea cero. Ejecutar cada Lunes.
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	//@Schedule(minute="0", hour="1", dayOfWeek="1", persistent= false)
	//@Schedule( minute="*", hour="*", dayOfMonth="*", month="*", year="*", persistent= false)
	@Override
	public void identificarCartasAElaborar() {
		List<CartaCiaDTO> cartas = recuperacionCiaDao.obtenerCartasAElaborar();
		
		if (cartas != null && !cartas.isEmpty()) {
			for (CartaCiaDTO cartadto : CollectionUtils.emptyIfNull(cartas)) {
				CartaCompania carta = entidadService.findById(CartaCompania.class, cartadto.getCartaId());
				
				carta.setEstatus(EstatusCartaCompania.PENDIENTE_ELABORACION.getValue());
				carta.setFechaModificacion(new Date());
				entidadService.save(carta);
				
				bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.IDENTIFICA_CARTA_AELABORAR, 
						String.valueOf(carta.getId()), 
						"Se actualiza la carta: " + carta.getId() + " a estatus POR ELABORAR", 
						carta, usuarioService.getUsuarioActual() != null ? usuarioService.getUsuarioActual().getNombre() : "SISTEMAS");
			}
			
			ExcelExporter exporter = new ExcelExporter( CartaCiaDTO.class, CartaCiaDTO.DOCUMENTO_CARTA_ELABORAR);
			TransporteImpresionDTO transporte = exporter.exportXLS(cartas, "Cartas pendientes");
			
			List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
			ByteArrayAttachment attatchment =  new ByteArrayAttachment();
			
			attatchment.setTipoArchivo(TipoArchivo.XLS);
			try {
				attatchment.setContenidoArchivo(IOUtils.toByteArray(transporte.getGenericInputStream()));
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
			attatchment.setNombreArchivo(transporte.getFileName());
			
			adjuntos.add(attatchment);			
			
			String[] nombresRol = new String[2];
			nombresRol[0] = "Rol_M2_Analista_Recuperaciones_Plaza";
			nombresRol[1] = "Rol_M2_Coordinador_Cobro_Companias_Salvamentos";
			
			EnvioNotificacionesService.EmailDestinaratios destinos = new EnvioNotificacionesService.EmailDestinaratios();

			List<Usuario> usuarios  = new ArrayList<Usuario>();

			try{
				usuarios = usuarioService.buscarUsuariosPorNombreRol(nombresRol);
				
				for (Usuario us : CollectionUtils.emptyIfNull(usuarios)) {
					if (!StringUtil.isEmpty(us.getEmail()))
						destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.CC, us.getEmail(), us.getNombreCompleto());
				}			
			}catch(Exception e){
				logger.error(e.getMessage(), e);
			}			

			HashMap<String, Serializable> data = new HashMap<String, Serializable>();
			
			SimpleDateFormat sdfFecha = new SimpleDateFormat( "dd/MM/yyyy" );    
			data.put("fechaActual", sdfFecha.format(new Date()));
			
			notificacionService.enviarNotificacion(EnumCodigo.SN_RCIA_CARTAS_POR_ELABORAR.toString(), data, "1", destinos, adjuntos);
		}

	}

	
	@Override
	public BigDecimal montoTotalRecuperacionCia(Long idSiniestroCabina,
			EstatusRecuperacion estatus) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("reporteCabina.siniestroCabina.id", idSiniestroCabina);
		params.put("estatus", estatus.toString());
		BigDecimal monto  = BigDecimal.ZERO;
		List<RecuperacionCompania>  lst=this.entidadService.findByProperties(RecuperacionCompania.class, params);
		if(null!=lst && !lst.isEmpty()){
			for(RecuperacionCompania cia : lst){
				if(null!= cia && null!= cia.getImporte())
					monto= monto.add(cia.getImporte());
			}
		}
		return monto;
	}

	@Override
	public String entregarCartas(String recuperacionesConcat, Date fechaAcuse){
		String refError = "";
		StringBuilder recuperacionesResp =new StringBuilder("");
		String[] recuperaciones = recuperacionesConcat.split(",");
		for(String element : recuperaciones){
			RecuperacionCompania r = entidadService.findById(RecuperacionCompania.class, Long.valueOf(element));
			//Se valida la fecha de acuse contra la fecha de impresión
			CartaCompania ultimaCarta = this.obtenerCartaActiva(r.getId());
			if(CommonUtils.isNotNull(ultimaCarta.getFechaImpresion()) && fechaAcuse.compareTo( this.truncateFecha(ultimaCarta.getFechaImpresion()) ) >= 0){
				ultimaCarta.setFechaAcuse(fechaAcuse);
				ultimaCarta.setEstatus(CartaCompania.EstatusCartaCompania.ENTREGADO.codigo);
				ultimaCarta.setFechaEntrega(new Date());
				ultimaCarta.setUsuarioEntrega(usuarioService.getUsuarioActual().getNombreUsuario());
				ultimaCarta.setFechaModificacion(new Date());
				ultimaCarta.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
				r.setFechaModificacion(new Date());
				r.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
				entidadService.save(r);
				Ingreso ingresoActivo = obtenerIngresoActivo(r.getId());		
				if(ingresoActivo == null){
					generarIngreso(r);
				}else{
					ingresoActivo.setMonto(r.getImporte());
					entidadService.save(ingresoActivo);
				}
				
			}else{
				recuperacionesResp.append(r.getNumero()).append(",");
			}
		}
		String recuperacionResp = recuperacionesResp.toString();
		if (!recuperacionResp.equals("")){
			refError = "No fue posible asignar la fecha de acuse a las cartas de las siguientes Recuperaciones: " + recuperacionResp.substring(0, recuperacionResp.length()-1) + ".\n";
		}

		return refError.equals("") ? null : refError;
	}
	
	@Override
	public String reasignarCartas(String recuperacionesConcat, Long oficinaReasignadaId){
		StringBuilder mensajeRespuesta = new StringBuilder();
		
		String[] recuperaciones = recuperacionesConcat.split(",");
		Oficina oficina = entidadService.findById(Oficina.class, oficinaReasignadaId);
		for(String element : recuperaciones){
			RecuperacionCompania r = entidadService.findById(RecuperacionCompania.class, Long.valueOf(element));
			
			if( oficina.getId() == r.getReporteCabina().getOficina().getId() ) {
				mensajeRespuesta.append("LA RECUPERACIÓN: ")
								.append(r.getNumero())
								.append(" NO SE PUEDE REASIGNAR A SU MISMA OFICINA: "+oficina.getNombreOficina());
			}else {
				r.setOficinaReasignada(oficina);
				r.setFechaReasignacion(new Date());
				r.setUsuarioReasignacion(usuarioService.getUsuarioActual().getNombreUsuario());
				r.setFechaModificacion(new Date());
				r.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
				entidadService.save(r);
			}
		}

		return mensajeRespuesta.toString();
	}
	@Override
	public String registrarEnvioCarta(String recuperacionesConcat, Long oficinaId) {
		String refError = "";
		StringBuilder recuperacionesResp = new StringBuilder("");
		String[] recuperaciones = recuperacionesConcat.split(",");
		for(String element : recuperaciones){
			RecuperacionCompania r = entidadService.findById(RecuperacionCompania.class, Long.valueOf(element));
			CartaCompania ultimaCarta = this.obtenerCartaActiva(r.getId());
			if(CommonUtils.isNull(ultimaCarta.getFechaEnvio())){
				Oficina o = entidadService.findById(Oficina.class, oficinaId);
				if(!r.getReporteCabina().getOficina().equals(o)){
					String usuarioActual = usuarioService.getUsuarioActual().getNombreUsuario();
					Date fechaActual = new Date();
					ultimaCarta.setFechaEnvio(fechaActual);
					ultimaCarta.setUsuarioEnvio(usuarioActual);
					ultimaCarta.setOficinaRecepcion(o);
					ultimaCarta.setFechaModificacion(fechaActual);
					ultimaCarta.setCodigoUsuarioModificacion(usuarioActual);
					r.setFechaModificacion(fechaActual);
					r.setCodigoUsuarioModificacion(usuarioActual);
					entidadService.save(r);
				}else{
					recuperacionesResp.append(r.getNumero()).append(",");
				}
			}else{
				recuperacionesResp.append(r.getNumero()).append( ",");
			}
		}
		String recuperacionResp=recuperacionesResp.toString();
		if (!recuperacionResp.equals("")){
			refError = "No fue posible registrar el envío de Carta de las siguientes Recuperaciones: " + recuperacionResp.substring(0, recuperacionResp.length()-1) + ".\n";
		}

		return refError.equals("") ? null : refError;
	}
	
	@Override
	public String eliminarEnvioCarta(String recuperacionesConcat) {
		String refError = "";
		StringBuilder recuperacionesResp = new StringBuilder("");
		String[] recuperaciones = recuperacionesConcat.split(",");
		for(String element : recuperaciones){
			RecuperacionCompania r = entidadService.findById(RecuperacionCompania.class, Long.valueOf(element));
			CartaCompania ultimaCarta = this.obtenerCartaActiva(r.getId());
			if(CommonUtils.isNotNull(ultimaCarta.getFechaEnvio())){
				String usuarioActual = usuarioService.getUsuarioActual().getNombreUsuario();
				Date fechaActual = new Date();
				ultimaCarta.setFechaEnvio(null);
				ultimaCarta.setUsuarioEnvio(null);
				ultimaCarta.setOficinaRecepcion(null);
				ultimaCarta.setFechaEliminaEnvio(fechaActual);
				ultimaCarta.setUsuarioEliminaEnvio(usuarioActual);
				ultimaCarta.setFechaModificacion(fechaActual);
				ultimaCarta.setCodigoUsuarioModificacion(usuarioActual);
				r.setFechaModificacion(fechaActual);
				r.setCodigoUsuarioModificacion(usuarioActual);
				entidadService.save(r);
			}else{
				recuperacionesResp.append(r.getNumero()).append(",");
			}
		}
		String recuperacionResp=recuperacionesResp.toString();
		if (!recuperacionResp.equals("")){
			refError = "No fue posible eliminar el envío de Carta de las siguientes Recuperaciones: " + recuperacionResp.substring(0, recuperacionResp.length()-1) + ".\n";
		}

		return refError.equals("") ? null : refError;
	}
	
	@Override
	public String registrarRecepcionCarta(String recuperacionesConcat, String usuario) {
		String refError = "";
		StringBuilder recuperacionesResp = new StringBuilder("");
		String[] recuperaciones = recuperacionesConcat.split(",");
		for(String element : recuperaciones){
			RecuperacionCompania r = entidadService.findById(RecuperacionCompania.class, Long.valueOf(element));
			CartaCompania ultimaCarta = this.obtenerCartaActiva(r.getId());
			if(CommonUtils.isNull(ultimaCarta.getFechaRecepcion())){
				String usuarioActual = usuarioService.getUsuarioActual().getNombreUsuario();
				Date fechaActual = new Date();
				ultimaCarta.setFechaRecepcion(fechaActual);
				ultimaCarta.setUsuarioRecepcion(usuarioActual);
				ultimaCarta.setNombrePersonaRecepcion(usuario);
				ultimaCarta.setFechaModificacion(fechaActual);
				ultimaCarta.setCodigoUsuarioModificacion(usuarioActual);
				r.setFechaModificacion(fechaActual);
				r.setCodigoUsuarioModificacion(usuarioActual);
				entidadService.save(r);
			}else{
				recuperacionesResp.append(r.getNumero()).append(",");
			}
		}
		String recuperacionResp=recuperacionesResp.toString();
		if (!recuperacionResp.equals("")){
			refError = "No fue posible registrar la recepción de Carta de las siguientes Recuperaciones: " + recuperacionResp.substring(0, recuperacionResp.length()-1) + ".\n";
		}

		return refError.equals("") ? null : refError;
	}
	
	@Override
	public String eliminarRecepcionCarta(String recuperacionesConcat) {
		String refError = "";
		StringBuilder recuperacionesResp = new StringBuilder("");
		String[] recuperaciones = recuperacionesConcat.split(",");
		for(String element : recuperaciones){
			RecuperacionCompania r = entidadService.findById(RecuperacionCompania.class, Long.valueOf(element));
			CartaCompania ultimaCarta = this.obtenerCartaActiva(r.getId());
			if(CommonUtils.isNotNull(ultimaCarta.getFechaRecepcion())){
				String usuarioActual = usuarioService.getUsuarioActual().getNombreUsuario();
				Date fechaActual = new Date();
				ultimaCarta.setFechaRecepcion(null);
				ultimaCarta.setUsuarioRecepcion(null);
				ultimaCarta.setNombrePersonaRecepcion(null);
				ultimaCarta.setFechaEliminaRecepcion(fechaActual);
				ultimaCarta.setUsuarioEliminaRecepcion(usuarioActual);
				ultimaCarta.setFechaModificacion(fechaActual);
				ultimaCarta.setCodigoUsuarioModificacion(usuarioActual);
				r.setFechaModificacion(fechaActual);
				r.setCodigoUsuarioModificacion(usuarioActual);
				entidadService.save(r);
			}else{
				recuperacionesResp.append(r.getNumero()).append(",");
			}
		}
		String recuperacionResp=recuperacionesResp.toString();
		if (!recuperacionResp.equals("")){
			refError = "No fue posible eliminar la recepción de Carta de las siguientes Recuperaciones: " + recuperacionResp.substring(0, recuperacionResp.length()-1) + ".\n";
		}

		return refError.equals("") ? null : refError;
	}
	@Override
	public List<CartaCompania> obtenerCartasARedoc() {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public List<String> obtenerReferenciasIdentificadas()  {
		return recuperacionCiaDao.obtenerReferenciasIdentificadas();
	}

	

	/**
	 * <b>SE AGREGA EN DISEÑO REGISTRAR COMPLEMENTO</b>
	 * 
	 * Retorna si se ha realizado un ajuste de aumento al pase/afectación de la
	 * Recuperación.
	 * 
	 * Hacer un entidadService.findByProperties sobre ComplementoCompania y como
	 * parámetro la recuperacion.id = recuperacionId y activo = true.
	 * 
	 * @param recuperacionId
	 */
	@Override
	public ComplementoCompania obtenerTieneComplemento(Long recuperacionId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("recuperacion.id", recuperacionId);
		params.put("activo", true);
		ComplementoCompania complementoCompania = null;		
		List<ComplementoCompania>  lista= this.entidadService.findByProperties(ComplementoCompania.class, params);
		if(null!= lista && !lista.isEmpty()){
			complementoCompania=lista.get(0);
		}
		return complementoCompania;
	}
	@Override
	public RecuperacionCompania complementarCarta(Long recuperacionId) {
		RecuperacionCiaService processor = this.sessionContext.getBusinessObject(RecuperacionCiaService.class);
		RecuperacionCompania recuperacion = processor.complementarCartaYGuarda(recuperacionId);
		processor.invocaProvision(recuperacion.getId());
		return recuperacion;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public RecuperacionCompania complementarCartaYGuarda(Long recuperacionId){
		RecuperacionCompania recuperacionOriginal = this.entidadService.findById(RecuperacionCompania.class, recuperacionId);
		RecuperacionCompania recuperacionNueva = new RecuperacionCompania();
		ComplementoCompania complemento = this.obtenerTieneComplemento(recuperacionId);
		
		recuperacionNueva.setDiasVencimiento(recuperacionOriginal.getDiasVencimiento());
		recuperacionNueva.setPorcentajeParticipacion(recuperacionOriginal.getPorcentajeParticipacion());
		recuperacionNueva.setPolizaCia((recuperacionOriginal.getPolizaCia()));
		recuperacionNueva.setMedio(recuperacionOriginal.getMedio());
		recuperacionNueva.setReporteCabina(recuperacionOriginal.getReporteCabina());
		recuperacionNueva.setPaseAtencionCia(recuperacionOriginal.getPaseAtencionCia());		
		recuperacionNueva.setTipo(recuperacionOriginal.getTipo());
		recuperacionNueva.setSiniestroCia(recuperacionOriginal.getSiniestroCia());
		recuperacionNueva.setCompania(recuperacionOriginal.getCompania());
		recuperacionNueva.setFechaLimiteCobro(recuperacionOriginal.getFechaLimiteCobro());
		recuperacionNueva.setNumero(entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO));
		recuperacionNueva.setRecuperacionOriginal(recuperacionOriginal);
		recuperacionNueva.setFechaCreacion(new Date());
		recuperacionNueva.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		recuperacionNueva.setOrigen(Recuperacion.OrigenRecuperacion.AUTOMATICA.toString());
		recuperacionNueva.setEstatus(Recuperacion.EstatusRecuperacion.REGISTRADO.toString());
		recuperacionNueva.setEsComplemento(Boolean.TRUE);		
		
		BigDecimal montoAjuste = complemento.getMonto().multiply(new BigDecimal(recuperacionOriginal.getPorcentajeParticipacion()).divide(new BigDecimal (100)));
		
		recuperacionNueva.setImporteInicial(montoAjuste);
		recuperacionNueva.setImporte(BigDecimal.ZERO);
		recuperacionNueva.setMontoIndemnizacion(BigDecimal.ZERO);
		recuperacionNueva.setMontoGAGrua(BigDecimal.ZERO);
		recuperacionNueva.setMontoSalvamento(BigDecimal.ZERO);
		
		recuperacionNueva = entidadService.save(recuperacionNueva);
  		
		List<CoberturaRecuperacion> listaCoberturas = new ArrayList<CoberturaRecuperacion>();
		
		List<CoberturaRecuperacion> listaCoberturasOriginales = this.getCoberturasRecuperacion(recuperacionOriginal.getId());
		
		for(CoberturaRecuperacion coberturaRecup: listaCoberturasOriginales)
		{
			CoberturaRecuperacion coberturaNueva = new CoberturaRecuperacion(coberturaRecup.getClaveSubCalculo(),
					coberturaRecup.getCoberturaReporte(), recuperacionNueva, null);			
			entidadService.save(coberturaNueva);
			
			List<PaseRecuperacion> listaPases = new ArrayList<PaseRecuperacion>();
			
			for(PaseRecuperacion paseRecup : coberturaRecup.getPasesAtencion())
			{					
				PaseRecuperacion paseRecuperacion = new PaseRecuperacion(coberturaNueva, paseRecup.getEstimacionCobertura());
				
				listaPases.add(paseRecuperacion);
			}
			
			coberturaNueva.setPasesAtencion(listaPases);	
			
			listaCoberturas.add(coberturaNueva);
		}
		
		recuperacionNueva.setCoberturas(listaCoberturas);
		
		this.generarCartaOriginal(recuperacionNueva, EstatusCartaCompania.REGISTRADO);
		
		complemento.setActivo(Boolean.FALSE);
		entidadService.save(complemento);	
		
		return recuperacionNueva;
	}
	
	
	
	@Override
	public void cancelarComplemento(Long recuperacionId) {
		// TODO pendiente de definir
		
	}
	@Override
	public RecuperacionCompania regeneraRecuperacion(Long recuperacionId) {
		RecuperacionCiaService processorRecuperacionCia = this.sessionContext.getBusinessObject(RecuperacionCiaService.class);
		RecuperacionCompania recuperacionNueva = processorRecuperacionCia.regeneraYGuarda(recuperacionId);
		processorRecuperacionCia.invocaProvision(recuperacionId);
		return recuperacionNueva;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public RecuperacionCompania regeneraYGuarda(Long recuperacionId){
		RecuperacionCompania recuperacionOriginal = this.entidadService.findById(RecuperacionCompania.class, recuperacionId);
		RecuperacionCompania recuperacionNueva = new RecuperacionCompania();
		ComplementoCompania complemento = this.obtenerTieneComplemento(recuperacionId);
		
		BeanUtils.copyProperties(recuperacionOriginal,recuperacionNueva);
		recuperacionNueva.setId(null);
		recuperacionNueva.setNumero(entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO));
		recuperacionNueva.setFechaCreacion(new Date());
		recuperacionNueva.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		recuperacionNueva.setOrigen(Recuperacion.OrigenRecuperacion.AUTOMATICA.toString());
		recuperacionNueva.setEstatus(Recuperacion.EstatusRecuperacion.REGISTRADO.toString());
		
		BigDecimal montoAjuste = complemento.getMonto().multiply(new BigDecimal(recuperacionOriginal.getPorcentajeParticipacion()).divide(new BigDecimal (100)));
		
		recuperacionNueva.setImporteInicial((recuperacionOriginal.getImporteInicial() != null ? 
				recuperacionOriginal.getImporteInicial():BigDecimal.ZERO).add(montoAjuste));
		recuperacionNueva.setImporte(recuperacionOriginal.getImporte().add(montoAjuste));
		
		recuperacionNueva = entidadService.save(recuperacionNueva);
		
		List<CoberturaRecuperacion> listaCoberturas = new ArrayList<CoberturaRecuperacion>();
		
		List<CoberturaRecuperacion> listaCoberturasOriginales = this.getCoberturasRecuperacion(recuperacionOriginal.getId());
		
		for(CoberturaRecuperacion coberturaRecup: listaCoberturasOriginales)
		{
			CoberturaRecuperacion coberturaNueva = new CoberturaRecuperacion(coberturaRecup.getClaveSubCalculo(),
					coberturaRecup.getCoberturaReporte(), recuperacionNueva, null);			
			entidadService.save(coberturaNueva);
			
			List<PaseRecuperacion> listaPases = new ArrayList<PaseRecuperacion>();
			
			for(PaseRecuperacion paseRecup : coberturaRecup.getPasesAtencion())
			{					
				PaseRecuperacion paseRecuperacion = new PaseRecuperacion(coberturaNueva, paseRecup.getEstimacionCobertura());
				
				listaPases.add(paseRecuperacion);
			}
			
			coberturaNueva.setPasesAtencion(listaPases);	
			
			listaCoberturas.add(coberturaNueva);
		}   
		
		recuperacionNueva.setCoberturas(listaCoberturas);
		entidadService.save(recuperacionNueva);
			
	
		this.generarCartaOriginal(recuperacionNueva, EstatusCartaCompania.REGISTRADO);
		
		RecuperacionCiaService processorRecuperacionCia = this.sessionContext.getBusinessObject(RecuperacionCiaService.class);
		processorRecuperacionCia.cancelarRecuperacion(recuperacionOriginal.getId(), "Cancelado por Complemento"); //TODO PENDIENTE ENUM
		processorRecuperacionCia.invocaProvision(recuperacionOriginal.getId());
		List<OrdenCompraCartaCia> listaOrdenesCompra = new ArrayList<OrdenCompraCartaCia>();
		
		for (OrdenCompraCartaCia ordenCompra: CollectionUtils.
				emptyIfNull(this.obtenerOrdenesCompraCia(recuperacionOriginal.getId(), Boolean.FALSE))) {
			OrdenCompraCartaCia ordenCompraNueva = new OrdenCompraCartaCia();
			BeanUtils.copyProperties(ordenCompra,ordenCompraNueva);
			ordenCompraNueva.setId(null);
			ordenCompraNueva.setRecuperacion(recuperacionNueva);
			entidadService.save(ordenCompraNueva);
		
			listaOrdenesCompra.add(ordenCompraNueva);
		}
		    
		recuperacionNueva.setOrdenesCompra(listaOrdenesCompra);
		 		
		complemento.setActivo(false);
		entidadService.save(complemento);	
		
		return recuperacionNueva;		
	}
	
	
	
	private Boolean actualizarInfoRecuperacionPorCambioPorcentaje(Long estimacionId, Integer companiaId, String polizaCia, String numeroSiniestro, Integer porcentajeNuevo, Integer porcentajeActual) {
		List<RecuperacionCompania> recuperaciones = recuperacionCiaDao.obtenerRecuperacionesActualizables(estimacionId, companiaId);
		for (RecuperacionCompania recuperacion: CollectionUtils.emptyIfNull(recuperaciones)) {
			this.actualizaPorCambioDePorcentaje(recuperacion,estimacionId, companiaId, polizaCia, numeroSiniestro, porcentajeActual);
		}
		//TODO actualizar la provision
		if(porcentajeNuevo.intValue()!=porcentajeActual.intValue()){
			return Boolean.TRUE;
//			EstimacionCoberturaReporteCabina estimacion = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, estimacionId);
//			Long reporteCabinaId = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId();
//			this.siniestrosProvisionService.cancelaProvisionesDeCiaPorReporte(reporteCabinaId);
//			this.siniestrosProvisionService.habilitaProvisionesDeCiaPorReporte(reporteCabinaId);
		}
		return Boolean.FALSE;
	}
	
	@Override
	public RecuperacionCompania  actualizaPorCambioDePorcentaje(RecuperacionCompania recuperacion,Long estimacionId, Integer companiaId, String polizaCia, String numeroSiniestro, Integer porcentaje){
		CartaCompania cartaActual = obtenerCartaActiva(recuperacion.getId());
		
		if (recuperacion.getCompania().getId().compareTo(companiaId.intValue()) != 0 && cartaActual.getFolio().compareTo(CartaCompania.FOLIO_CERO) == 0) {
			PrestadorServicio compania = entidadService.findById(PrestadorServicio.class, companiaId);
			recuperacion.setCompania(compania);
		}
		
		recuperacion.setPolizaCia(polizaCia);
		recuperacion.setSiniestroCia(numeroSiniestro);
		
		if (recuperacion.getPorcentajeParticipacion() != null && recuperacion.getPorcentajeParticipacion().compareTo(porcentaje) != 0) {
			
			
			BigDecimal porcentajeOriginal = new BigDecimal (recuperacion.getPorcentajeParticipacion()).divide(new BigDecimal (100));
			
			BigDecimal montoIndemnizacion = BigDecimal.ZERO;			
			
			BigDecimal multiplo = new BigDecimal (porcentaje).divide(new BigDecimal (100));
			
			List<OrdenCompraCartaCia> ordenes = entidadService.findByProperty(OrdenCompraCartaCia.class, "recuperacion.id", recuperacion.getId());
			
			for (OrdenCompraCartaCia ordenCompra: CollectionUtils.emptyIfNull(ordenes)) {
				ordenCompra.setPorcentajeParticipacion(porcentaje);
				
				ordenCompra.setMontoARecuperar( ((ordenCompra.getSubtotalOC().subtract(ordenCompra.getSubTotalRecupProveedor())).multiply(multiplo)));
			
				entidadService.save(ordenCompra);
				montoIndemnizacion.add(ordenCompra.getMontoARecuperar());
			}
			
			recuperacion.setPorcentajeParticipacion(porcentaje);
			recuperacion.setMontoIndemnizacion(montoIndemnizacion);
			
			BigDecimal importeTotal = montoIndemnizacion.subtract(recuperacion.getMontoSalvamento()).add(recuperacion.getMontoGAGrua());
			BigDecimal importeExclusion = BigDecimal.ZERO;				
					
			if (cartaActual.getMontoExclusion() != null) {
				importeExclusion = cartaActual.getMontoExclusion();
				
				// INVOCAR PROVISION EN EXCLUSION
//				this.siniestrosProvisionService.
			}
			
			cartaActual.setMontoARecuperar(importeTotal.subtract(importeExclusion));

			BigDecimal importeInicialOrigen = recuperacion.getImporteInicial().divide(porcentajeOriginal);
			
			recuperacion.setImporteInicial(importeInicialOrigen.multiply(multiplo));	
			
			//Actualiza provision
			/*if(recuperacion.getId()!=null){
				RecuperacionCompania recuperacionActual = this.entidadService.findById(RecuperacionCompania.class, recuperacion.getId());
				recuperacion.setEstatusProvision(recuperacionActual.getEstatusProvision());
			}*/
			
			entidadService.save(cartaActual);
		}					
		
		entidadService.save(recuperacion);	
		return recuperacion;
	}
	
	
	@Override
	@Deprecated
	public RecuperacionCompania generaRecuperacionCiaAutomatico(EstimacionCoberturaReporteCabina estimacion, BigDecimal importe) {
		
		RecuperacionCiaService processorRecuperacionCia = this.sessionContext.getBusinessObject(RecuperacionCiaService.class);
		RecuperacionCompania recuperacion = processorRecuperacionCia.crearYSalvarRecuperacionCia(estimacion, importe);
		return recuperacion;
	}
	
	@Override
//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public RecuperacionCompania crearYSalvarRecuperacionCia(EstimacionCoberturaReporteCabina estimacion, BigDecimal importe){
		RecuperacionCompania recuperacion = new RecuperacionCompania();

		ReporteCabina reporte = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina();
		AutoIncisoReporteCabina autoInciso = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();
		PrestadorServicio compania = entidadService.findById(PrestadorServicio.class, Integer.parseInt(autoInciso.getCiaSeguros()));
		
		recuperacion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		recuperacion.setFechaCreacion(new Date());
		recuperacion.setFechaModificacion(new Date());

		recuperacion.setNumero(entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO));
		recuperacion.setMedio(MedioRecuperacion.REEMBOLSO.name());
		recuperacion.setOrigen(OrigenRecuperacion.AUTOMATICA.name());
		recuperacion.setEstatus(EstatusRecuperacion.REGISTRADO.getValue());
		recuperacion.setTipoOrdenCompra(Recuperacion.TipoOrdenCompra.AFECTACION.name());
		recuperacion.setTipo(TipoRecuperacion.COMPANIA.getValue());
		recuperacion.setReporteCabina(reporte);
		recuperacion.setCompania(compania);
		recuperacion.setPolizaCia(autoInciso.getPolizaCia());
		recuperacion.setPaseAtencionCia(PaseAtencionCompania.ORIGINAL.toString());
		recuperacion.setEsComplemento(Boolean.FALSE);
		recuperacion.setSiniestroCia(autoInciso.getSiniestroCia());
		recuperacion.setPorcentajeParticipacion(autoInciso.getPorcentajeParticipacion());
		
		BigDecimal multiplo = new BigDecimal (recuperacion.getPorcentajeParticipacion()).divide(new BigDecimal (100));

		recuperacion.setMontoGAGrua(BigDecimal.ZERO);
		recuperacion.setMontoIndemnizacion(BigDecimal.ZERO);
		recuperacion.setMontoSalvamento(BigDecimal.ZERO);
		
		recuperacion.setImporteInicial(importe.multiply(multiplo));
		recuperacion.setImporte(BigDecimal.ZERO);
		recuperacion.setFechaLimiteCobro(DateUtils.add(reporte.getFechaOcurrido(), Indicador.Days, diasLimiteCobro)) ;

		recuperacion = entidadService.save(recuperacion);	

		CoberturaRecuperacion coberturaRecuperacion=new CoberturaRecuperacion(estimacion.getCveSubCalculo(),
				estimacion.getCoberturaReporteCabina(), recuperacion, null);			
		entidadService.save(coberturaRecuperacion);

		PaseRecuperacion paseRecuperacion = new PaseRecuperacion(coberturaRecuperacion, estimacion);
		entidadService.save(paseRecuperacion);

		generarCartaOriginal(recuperacion, EstatusCartaCompania.REGISTRADO);
		
		return recuperacion;
	}
	
	//TODO Eliminar este método 
	@Override
	@Deprecated
	public RecuperacionCompania generaComplementoAutomatico(RecuperacionCompania recuperacionOriginal, BigDecimal montoAjuste) {
		RecuperacionCiaService processor = this.sessionContext.getBusinessObject(RecuperacionCiaService.class);
		RecuperacionCompania recuperacion = processor.creaYGuardaComplemento(recuperacionOriginal,montoAjuste);
		processor.invocaProvision(recuperacion.getId());
		return recuperacion;
	}
	
	
	@Override
	public RecuperacionCompania creaYGuardaComplemento(RecuperacionCompania recuperacionOriginal, BigDecimal montoAjuste){
		RecuperacionCompania recuperacion = new RecuperacionCompania();	

		recuperacion.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
		recuperacion.setFechaCreacion(new Date());

		recuperacion.setNumero(entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO));
		recuperacion.setMedio(MedioRecuperacion.REEMBOLSO.name());
		recuperacion.setOrigen(OrigenRecuperacion.AUTOMATICA.name());
		recuperacion.setEstatus(EstatusRecuperacion.REGISTRADO.getValue());
		recuperacion.setTipoOrdenCompra(Recuperacion.TipoOrdenCompra.AFECTACION.name());
		recuperacion.setTipo(TipoRecuperacion.COMPANIA.getValue());
		recuperacion.setReporteCabina(recuperacionOriginal.getReporteCabina());
		recuperacion.setCompania(recuperacionOriginal.getCompania());
		recuperacion.setPolizaCia(recuperacionOriginal.getPolizaCia());		
		recuperacion.setPaseAtencionCia(recuperacionOriginal.getPaseAtencionCia());
		recuperacion.setSiniestroCia(recuperacionOriginal.getSiniestroCia());
		
		EstimacionCoberturaReporteCabina estimacion = this.siniestrosProvisionService.obtenerEstimacionDeRecuperacionCia(recuperacionOriginal.getId());
		Integer porcentajeActual = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getPorcentajeParticipacion();
		
		recuperacion.setPorcentajeParticipacion(porcentajeActual);
		recuperacion.setRecuperacionOriginal(recuperacionOriginal);
		recuperacion.setEsComplemento(Boolean.TRUE);

		recuperacion.setMontoGAGrua(BigDecimal.ZERO);
		recuperacion.setMontoIndemnizacion(BigDecimal.ZERO);
		recuperacion.setMontoSalvamento(BigDecimal.ZERO);
		recuperacion.setImporte(BigDecimal.ZERO);
		
		BigDecimal multiplo = new BigDecimal (recuperacion.getPorcentajeParticipacion()).divide(new BigDecimal (100));
		recuperacion.setImporteInicial(montoAjuste.multiply(multiplo));
	
		recuperacion.setFechaLimiteCobro(recuperacionOriginal.getFechaLimiteCobro()) ;

		recuperacion = entidadService.save(recuperacion);	

		CoberturaRecuperacion coberturaRecuperacionOriginal = recuperacionOriginal.getCoberturas().get(0);
		CoberturaRecuperacion coberturaRecuperacion = new CoberturaRecuperacion(coberturaRecuperacionOriginal.getClaveSubCalculo(),
				coberturaRecuperacionOriginal.getCoberturaReporte(), recuperacion, null);	

		entidadService.save(coberturaRecuperacion);

		PaseRecuperacion paseRecuperacionOriginal = coberturaRecuperacionOriginal.getPasesAtencion().get(0);
		PaseRecuperacion paseRecuperacion = new PaseRecuperacion(coberturaRecuperacion, paseRecuperacionOriginal.getEstimacionCobertura() );
	
		entidadService.save(paseRecuperacion);
		
		generarCartaOriginal(recuperacion, EstatusCartaCompania.REGISTRADO);
		
		return recuperacion;
	}
	
	
	
	private void generarCartaOriginal(RecuperacionCompania recuperacion, EstatusCartaCompania estatus) {
		CartaCompania carta = new CartaCompania();
		carta.setRecuperacion(recuperacion);
		carta.setEstatus(estatus.getValue());
		carta.setFechaCreacion(new Date());
		carta.setFolio(CartaCompania.FOLIO_CERO);
		carta.setConcepto("CONCEPTO");
		carta.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual()!= null ? usuarioService.getUsuarioActual().getNombreUsuario(): "SISTEMAS");
		entidadService.save(carta);
	}

	
	@Override
	public TransporteImpresionDTO imprimirCarta(Long cartaCiaId){
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		
		JasperReport jReport = gImpresion.getOJasperReport(CARTA_CIA);
		JasperReport jReporteExclusiones = gImpresion.getOJasperReport(CARTA_CIA_EXCLUSIONES);
		JasperReport jReporteOrdenesCompra = gImpresion.getOJasperReport(CARTA_CIA_ORDENES_COMPRA);
		
		List<CartaCiaDTO> dataSourceImpresion = new ArrayList<CartaCiaDTO>();
		CartaCiaDTO infoCarta = this.obtenerInfoCarta(cartaCiaId);
		List<CartaCiaExclusionesDTO> listaExclusiones = this.generarListaCoberturasExclusiones(infoCarta);
		infoCarta.setExclusiones(listaExclusiones);
		List<OrdenCompraCartaCia> ordenesCompra = this.obtenerOrdenesCompraCiaImpresion(infoCarta.getCartaId());
		ordenesCompra.add(obtenerTotalListadoOrdenesCompra(ordenesCompra));
		infoCarta.setOrdenesCompra(ordenesCompra);
		
		dataSourceImpresion.add(infoCarta);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("rutaImagenCheckOn", SistemaPersistencia.CHECKBOX_ON);
		params.put("rutaImagenCheckOff", SistemaPersistencia.CHECKBOX_OFF);
		params.put("jReporteExclusiones", jReporteExclusiones);
		params.put("jReporteOrdenesCompra", jReporteOrdenesCompra);
		params.put("esOriginal", (infoCarta.getEsRedocumentada() )? false : true );
		
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceImpresion);
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);		
	}
	
	/**
	 * obtiene los totales 
	 * @param ordenesCompra
	 * @return
	 */
	private OrdenCompraCartaCia obtenerTotalListadoOrdenesCompra(List<OrdenCompraCartaCia> ordenesCompra){
		OrdenCompraCartaCia ordenTotal = new OrdenCompraCartaCia();
		ordenTotal.setConceptoDesc("Total");
		BigDecimal total = BigDecimal.ZERO;
		if(ordenesCompra != null){
			for(OrdenCompraCartaCia orden : ordenesCompra ){
				total = total.add((orden.getMontoARecuperar() != null)? orden.getMontoARecuperar() : BigDecimal.ZERO);
			}
		}
		ordenTotal.setMontoARecuperar(total);
		return ordenTotal;
	}
	
	/**
	 * Devuelve un String con el valor para asignarle a la carta compania para saber que tipo de carta se manda a imprimir
	 * @param cartaDTO
	 * @param estimacion
	 * @param cobertura
	 * @return
	 */
	private String obtenerTipoCartaCompaniaImpresion(CartaCiaDTO cartaDTO, EstimacionCoberturaReporteCabina estimacion,
			CoberturaReporteCabina cobertura){
		if(cartaDTO.getEsPerdidaTotal() != null && cartaDTO.getEsPerdidaTotal()){
			return CartaCiaDTO.TipoCarta.PERDIDA_TOTAL.codigo;
		}else if(cobertura != null && (EnumUtil.equalsValue(ClaveTipoCalculo.ROBO_TOTAL, cobertura.getClaveTipoCalculo()))){
			return CartaCiaDTO.TipoCarta.ROBO_TOTAL.codigo;
		}else if(estimacion != null && (EnumUtil.equalsValue(estimacion.getTipoEstimacion(), 
										TipoEstimacion.GASTOS_MEDICOS, TipoEstimacion.GASTOS_MEDICOS_OCUPANTES))){
			return CartaCiaDTO.TipoCarta.GASTOS_MEDICOS.codigo;
		}else if(estimacion != null && (EnumUtil.equalsValue(estimacion.getTipoEstimacion(), 
				TipoEstimacion.RC_BIENES, TipoEstimacion.RC_PERSONA, TipoEstimacion.RC_VEHICULO,
				TipoEstimacion.RC_VIAJERO, TipoEstimacion.DANIOS_MATERIALES))){
			return CartaCiaDTO.TipoCarta.DANOS_MATERIALES_RESP_CIVIL.codigo;
		}
		return "";
	}
	
	/**
	 * Genera el contenido de la lista de Coberturas y Exclusiones de la impresion de la carta compania
	 * segun corresponda a la carta y las coberturas que trae
	 * @param cartaCiaId
	 * @return
	 */
	private List<CartaCiaExclusionesDTO> generarListaCoberturasExclusiones(CartaCiaDTO cartaDTO){
		List<CartaCiaExclusionesDTO> listaExclusiones = new ArrayList<CartaCiaExclusionesDTO>();
		CartaCiaExclusionesDTO exclusion = null;
		
		if(cartaDTO.getTipoCarta().equals(CartaCiaDTO.TipoCarta.PERDIDA_TOTAL.codigo)){
			exclusion = new CartaCiaExclusionesDTO(
					cartaDTO.getNombreCobertura(), 
					((cartaDTO.getEsPerdidaTotal() != null && cartaDTO.getEsPerdidaTotal())? 
							"Pérdida Total"
							: "Pérdida Parcial")
							+ " - Valor Indemnizado", 
					cartaDTO.getMontoIndemnizacion());
			listaExclusiones.add(exclusion);
			exclusion = new CartaCiaExclusionesDTO(
					NOMBRE_EXCLUSIONES_SALVAMENTO, "",
					cartaDTO.getMontoSalvamento());
			listaExclusiones.add(exclusion);
			exclusion = new CartaCiaExclusionesDTO(
					"Gasto de Ajuste", "Traslado primera Grúa",
					cartaDTO.getMontoGAGrua());
			listaExclusiones.add(exclusion);
			listaExclusiones.addAll(this.obtenerExclusionesCartaCia(cartaDTO.getCartaId()));
			exclusion = 
				new CartaCiaExclusionesDTO("Total", "", calcularTotalExclusiones(listaExclusiones));
			listaExclusiones.add(exclusion);
		}
		
		else if(cartaDTO.getTipoCarta().equals(CartaCiaDTO.TipoCarta.ROBO_TOTAL.codigo)){
			exclusion = new CartaCiaExclusionesDTO(
					cartaDTO.getNombreCobertura(), "Valor Indemnizado",	cartaDTO.getMontoIndemnizacion());
			listaExclusiones.add(exclusion);
			listaExclusiones.addAll(this.obtenerExclusionesCartaCia(cartaDTO.getCartaId()));
			exclusion = 
				new CartaCiaExclusionesDTO("Total", "", calcularTotalExclusiones(listaExclusiones));
			listaExclusiones.add(exclusion);
		}
		
		else if(cartaDTO.getTipoCarta().equals(CartaCiaDTO.TipoCarta.GASTOS_MEDICOS.codigo)){
			 exclusion = new CartaCiaExclusionesDTO(
					 cartaDTO.getNombreCobertura(), 
					 "Lesionado: " + ((cartaDTO.getLesionado() != null)? cartaDTO.getLesionado() : ""), 
					 cartaDTO.getMontoIndemnizacion());
			 listaExclusiones.add(exclusion);
			 listaExclusiones.addAll(obtenerExclusionesCartaCia(cartaDTO.getCartaId()));
			 exclusion = 
					new CartaCiaExclusionesDTO("Total", "", calcularTotalExclusiones(listaExclusiones));
			listaExclusiones.add(exclusion);
		}
		
		else if(cartaDTO.getTipoCarta().equals(CartaCiaDTO.TipoCarta.DANOS_MATERIALES_RESP_CIVIL.codigo)){
			exclusion = 
				new CartaCiaExclusionesDTO(cartaDTO.getNombreCobertura(),
						(cartaDTO.getEsPerdidaTotal() != null && cartaDTO.getEsPerdidaTotal())? 
								"Pérdida Total"
								: "Pérdida Parcial" , 
						cartaDTO.getMontoIndemnizacion());
			listaExclusiones.add(exclusion);
			exclusion = 
				new CartaCiaExclusionesDTO("Gasto de Ajuste", "Traslado Primera Grúa", cartaDTO.getMontoGAGrua());
			listaExclusiones.add(exclusion);
			
			if(cartaDTO.getMontoPiezas()!=null && cartaDTO.getMontoPiezas().compareTo(BigDecimal.ZERO)!=0){
				exclusion = new CartaCiaExclusionesDTO(
						NOMBRE_EXCLUSIONES_PIEZAS, "Gasto Piezas",
						cartaDTO.getMontoPiezas());
				listaExclusiones.add(exclusion);
			}
			
			listaExclusiones.addAll(this.obtenerExclusionesCartaCia(cartaDTO.getCartaId()));
			exclusion = 
				new CartaCiaExclusionesDTO("Total", "", calcularTotalExclusiones(listaExclusiones));
			listaExclusiones.add(exclusion);
		}
		
		return listaExclusiones;
	}
	
	/**
	 * Calcula el total de la seccion de exclusiones de la carta de compania
	 * @param listaExclusiones
	 * @return
	 */
	private BigDecimal calcularTotalExclusiones(List<CartaCiaExclusionesDTO> listaExclusiones){
		BigDecimal total = BigDecimal.ZERO;
		if(listaExclusiones != null){
			for(CartaCiaExclusionesDTO exclusion : listaExclusiones){
				if(exclusion.getNombre().equals(NOMBRE_EXCLUSIONES_AJUSTES) 
						|| exclusion.getNombre().equals(NOMBRE_EXCLUSIONES_SALVAMENTO)
						|| exclusion.getNombre().equals(NOMBRE_EXCLUSIONES_PIEZAS)){
					total = total.subtract(exclusion.getMonto());
				}else{
					total = total.add(exclusion.getMonto());
				}
			}
		}
		return total;
	}
	
	/**
	 * Obtiene las exclusiones de la carta compania y las agrega a una lista para mandarlas a la impresion
	 * @param cartaCiaId
	 * @return
	 */
	private List<CartaCiaExclusionesDTO> obtenerExclusionesCartaCia(Long cartaCiaId){
		List<CartaCiaExclusionesDTO> listaExclusiones = new ArrayList<CartaCiaExclusionesDTO>();
		
		CartaCompania cartaCia = entidadService.findById(CartaCompania.class, cartaCiaId);
		RecuperacionCompania recuperacion = cartaCia.getRecuperacion();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("recuperacion.id", recuperacion.getId());
		params.put("tipoRedocumentacion", TIPO_REDOCUMENTACION_EXCLUSION);
		
		List<CartaCompania> cartas = entidadService.findByPropertiesWithOrder(CartaCompania.class, params, "fechaCreacion");
		
		if(cartas != null){
			for(CartaCompania carta : cartas){
				if(cartas.contains(cartaCia)){//si el resultado no contiene la carta, significa que se esta imprimiendo la carta 0, ya que la consulta no la trae.
					String motivo = catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.MOTIVO_EXCUSION_CARTACIA, carta.getMotivoExclusion());
					CartaCiaExclusionesDTO exclusion = 
						new CartaCiaExclusionesDTO(NOMBRE_EXCLUSIONES_AJUSTES, motivo, carta.getMontoExclusion() );
					listaExclusiones.add(exclusion);
					if(carta.getId().equals(cartaCiaId)){
						break;
					}
				}
			}
		}
		
		return listaExclusiones;
	}
	
	private List<OrdenCompraCartaCia> obtenerOrdenesCompraCiaImpresion(Long cartaCiaId){
		
		List<OrdenCompraCartaCia> LstOrdenCompraCartaCia  = new ArrayList<OrdenCompraCartaCia>();
		
		CartaCompania carta = entidadService.findById(CartaCompania.class, cartaCiaId);
		RecuperacionCompania recuperacionCia = carta.getRecuperacion();
		
		List<OrdenCompraCartaCia> ordenesComprasCia  = this.obtenerOrdenesCompraCia(recuperacionCia.getId(), Boolean.FALSE);
		
		for( OrdenCompraCartaCia ordenCia : CollectionUtils.emptyIfNull(ordenesComprasCia)) {
			ImportesOrdenCompraDTO importe =ordenCompraService.calcularImportes(ordenCia.getOrdenCompra().getId(), null, false);
			OrdenCompraCartaCia dto = new OrdenCompraCartaCia();
			
			BigDecimal subtotal= importe.getSubtotal();
			BigDecimal subtotalRecuperacion= BigDecimal.ZERO;
			if(!StringUtil.isEmpty(ordenCia.getOrdenCompra().getDetalleConceptos())){
				String[] conceptos=  ordenCia.getOrdenCompra().getDetalleConceptos().split(",") ;
				if(conceptos.length>1){
					dto.setConceptoDesc("Varios...");
				}
				else {
					dto.setConceptoDesc(ordenCia.getOrdenCompra().getDetalleConceptos());

				}
			}

			dto.setMontoARecuperar(subtotal);
			dto.setOrdenCompra(ordenCia.getOrdenCompra());
			if(ordenCia.getOrdenCompra() != null
					&& ordenCia.getOrdenCompra().getIdBeneficiario() != null){
				PrestadorServicio proveedor = 
					entidadService.findById(PrestadorServicio.class, ordenCia.getOrdenCompra().getIdBeneficiario().intValue());
				if(proveedor != null){
					dto.setNombreProveedor(proveedor.getNombrePersona());
				}
			}
			Map<String, Object> param = new HashMap<String, Object>();	
			param.put("ordenCompra.id", ordenCia.getOrdenCompra().getId());
			param.put("estatus", Recuperacion.EstatusRecuperacion.RECUPERADO.toString());
			List<RecuperacionProveedor>  lstRecuperacion  = this.entidadService.findByProperties(RecuperacionProveedor.class, param);
			for(RecuperacionProveedor recuperacion :lstRecuperacion ){
				if( recuperacion.getMedio().equalsIgnoreCase(Recuperacion.MedioRecuperacion.REEMBOLSO.toString()) ){
					if(null!=recuperacion.getMontoTotal())
						subtotalRecuperacion=subtotalRecuperacion.add(recuperacion.getSubTotal());
				}else if( recuperacion.getMedio().equalsIgnoreCase(Recuperacion.MedioRecuperacion.VENTA.toString()) ){
					if(null!=recuperacion.getMontoVenta())
						subtotalRecuperacion=subtotalRecuperacion.add(recuperacion.getSubTotalVenta());
				}
			}
			List<DocumentoFiscal> facturas = recepcionFacturaService.obtenerFacturasRegistradaByOrdenCompra(ordenCia.getOrdenCompra().getId(), DocumentoFiscal.EstatusDocumentoFiscal.PAGADA.getValue());
			DocumentoFiscal factura = null;
			if(facturas != null
					&& !facturas.isEmpty()){
				factura = facturas.get(0);
			}
			if(factura != null){
				dto.setNumeroFactura(factura.getNumeroFactura());
			}
			dto.setPorcentajeParticipacion(recuperacionCia.getPorcentajeParticipacion());
			BigDecimal porcentaje = new BigDecimal (recuperacionCia.getPorcentajeParticipacion());
			BigDecimal multiplo = porcentaje.divide(new BigDecimal (100));
			dto.setRecuperacion(null);
			dto.setSubtotalOC(subtotal);
			dto.setMontoARecuperar( ((subtotal.subtract(subtotalRecuperacion)).multiply(multiplo)));
			dto.setSubTotalRecupProveedor(subtotalRecuperacion);
			LstOrdenCompraCartaCia.add(dto);
		}
		return LstOrdenCompraCartaCia;

	}
	
	@Override
	public void notificaAjusteRecuperacion(RecuperacionCompania recuperacion, BigDecimal montoAjuste) {
		
		String[] nombresRol = new String[2];
		nombresRol[0] = "Rol_M2_Gerente_Operaciones_Indemnizacion";
		nombresRol[1] = "Rol_M2_Coordinador_Cobro_Companias_Salvamentos";
		
		PaseRecuperacion paseRecuperacion = null;
		
		List<PaseRecuperacion> lstPase = entidadService.findByProperty(PaseRecuperacion.class, 
																"coberturaRecuperacion.recuperacion.id", recuperacion.getId());
		
		if (CollectionUtils.isNotEmpty(lstPase)) {
			paseRecuperacion = lstPase.get(0);
		} else {
			return;
		}
		
		EnvioNotificacionesService.EmailDestinaratios destinos = new EnvioNotificacionesService.EmailDestinaratios();
	
		List<Usuario> usuarios  = new ArrayList<Usuario>();
	
		try{
			usuarios = usuarioService.buscarUsuariosPorNombreRol(nombresRol);
			
			for (Usuario us : CollectionUtils.emptyIfNull(usuarios)) {
				if (!StringUtil.isEmpty(us.getEmail()))
					destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.CC, us.getEmail(), us.getNombreCompleto());
			}			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}		
		
		if (destinos != null) {
			HashMap<String, Serializable> data = new HashMap<String, Serializable>();
			
			data.put("numeroRecuperacion", recuperacion.getNumero());
			data.put("numeroSiniestro", recuperacion.getReporteCabina().getSiniestroCabina() != null 
					?  recuperacion.getReporteCabina().getSiniestroCabina().getNumeroSiniestro() : null);
			data.put("cobertura", estimacionService.obtenerNombreCobertura(paseRecuperacion.getCoberturaRecuperacion().getCoberturaReporte().getCoberturaDTO(), 
					paseRecuperacion.getCoberturaRecuperacion().getCoberturaReporte().getClaveTipoCalculo(), 
					paseRecuperacion.getCoberturaRecuperacion().getClaveSubCalculo(), 
					null, paseRecuperacion.getCoberturaRecuperacion().getCoberturaReporte().getCoberturaDTO().getTipoConfiguracion()));
			data.put("pase", paseRecuperacion.getEstimacionCobertura().getFolio());
			data.put("montoAjuste", montoAjuste);		
			data.put("fechaAjuste", new SimpleDateFormat( "dd/MM/yyyy" ).format(recuperacion.getFechaCreacion()));
			
			notificacionService.enviarNotificacion(EnumCodigo.SN_GENERA_COMPLEMENTO_CIA.toString(), data, "1", destinos, null);
		}
	}
	
	
	//TODO ELIMINAR TRANSACTION
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actualizarInfoCompaniasYProvisiona(SiniestroCabinaDTO siniestroCabina, Integer porcentajeOrigen,Integer recibioOrdenCiaOrigen ){
		ReporteCabina reporte = this.entidadService.evictAndFindById(ReporteCabina.class, siniestroCabina.getReporteCabinaId());
		AutoIncisoReporteCabina autoInciso = reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();
		
		String ciaSegurosOrigen = autoInciso.getCiaSeguros();
		String polizaCiaOrigen = autoInciso.getPolizaCia();
		
		if ((siniestroCabina.getId() != null && siniestroCabina.getId() > 0 && recibioOrdenCiaOrigen != null && (recibioOrdenCiaOrigen.compareTo(0) != 0 
				|| autoInciso.getRecibidaOrdenCia().compareTo(0) != 0))) {
			RecuperacionCiaService processorRecuperacionCia = this.sessionContext.getBusinessObject(RecuperacionCiaService.class);
			Map<String, Boolean> accionesAlGuardar = processorRecuperacionCia.actualizarInfoCompanias(autoInciso, recibioOrdenCiaOrigen, ciaSegurosOrigen,polizaCiaOrigen, porcentajeOrigen);
			 Long reporteId = autoInciso.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId();
			 
			if(accionesAlGuardar.get(SE_CANCELAN_LAS_CIAS)){
				this.siniestrosProvisionService.cancelaProvisionesDeCiaPorReporte(reporteId);
			}
			if(accionesAlGuardar.get(SE_HABILITARON_LAS_CIAS)){
				this.siniestrosProvisionService.habilitaProvisionesDeCiaPorReporte(reporteId);
			}
			if(accionesAlGuardar.get(SE_CAMBIA_EL_PORCENTAJE)){
				this.siniestrosProvisionService.cancelaProvisionesDeCiaPorReporte(reporteId);
				this.siniestrosProvisionService.habilitaProvisionesDeCiaPorReporte(reporteId);
			}
		}
		
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String,Boolean> actualizarInfoCompanias(AutoIncisoReporteCabina autoInciso, Integer recibioOrdenCiaOrigen, String ciaSegurosOrigen, 
			String polizaCiaOrigen, Integer porcentajeOrigen) {

		List<EstimacionCoberturaReporteCabina> estimaciones = entidadService.findByProperty(EstimacionCoberturaReporteCabina.class,
				"coberturaReporteCabina.incisoReporteCabina.id", autoInciso.getIncisoReporteCabina().getId());
		Boolean seCancelaronLasCias = Boolean.FALSE;
		Boolean seHabilitaronLasCias = Boolean.FALSE;
		Boolean seCambiaElPorcentaje = Boolean.FALSE;
		Map<String,Boolean> accionesAlGuardarReporteMap = new HashMap<String, Boolean>();
		
		for (EstimacionCoberturaReporteCabina estimacion: CollectionUtils.emptyIfNull(estimaciones)) {
			//SI NO SE CAMBIO NADA.... ENTONCES SE VALIDA SI HUBO UN CAMBIO EN EL PORCENTAJE DE PARTICIPACION
			if (recibioOrdenCiaOrigen.equals(1) && recibioOrdenCiaOrigen.compareTo(autoInciso.getRecibidaOrdenCia())  == 0) {			
				PrestadorServicio compania = entidadService.findById(PrestadorServicio.class, Integer.parseInt(autoInciso.getCiaSeguros()));
				
				seCambiaElPorcentaje = this.actualizarInfoRecuperacionPorCambioPorcentaje(estimacion.getId(), compania.getId(), autoInciso.getPolizaCia(), 
						autoInciso.getSiniestroCia(), porcentajeOrigen,autoInciso.getPorcentajeParticipacion());
				//CUANDO ANTES ERA SI Y AHORA ES UN NO SE RECIBIO ORDEN CIA
			} else if (recibioOrdenCiaOrigen.equals(1) && recibioOrdenCiaOrigen.compareTo(autoInciso.getRecibidaOrdenCia())  != 0) {
				RecuperacionCiaService processorRecuperacionCia = this.sessionContext.getBusinessObject(RecuperacionCiaService.class);
				processorRecuperacionCia.cancelarRecuperacionesCiaAlDesabilitarCiaTercero(estimacion);
				seCancelaronLasCias = Boolean.TRUE;

			} else if (recibioOrdenCiaOrigen.equals(0) && recibioOrdenCiaOrigen.compareTo(autoInciso.getRecibidaOrdenCia())  != 0) {
				//CUANDO ANTES ERA NO Y AHORA ES UN SI SE RECIBIO ORDEN CIA
				if (!EnumUtil.equalsValue(TipoPaseAtencion.SOLO_REGISTRO, estimacion.getTipoPaseAtencion())) {					
					//TODO Poner el importe correspondiente
					BigDecimal montoDeRecuperacion = BigDecimal.ZERO;
					BigDecimal montoConsideradoAlRecuperar = this.siniestrosProvisionService.obtenerMontoRecuperacionesConsiderandoPorcentajePorEstimacion(estimacion.getId());
					BigDecimal estimacionActual = this.movimientoSiniestroService.obtenerMontoEstimacionActual(null, null, estimacion.getId(), null, null, null);
					if( montoConsideradoAlRecuperar!=null && montoConsideradoAlRecuperar.compareTo(BigDecimal.ZERO)!= 0 ){
						montoDeRecuperacion = estimacionActual.subtract(montoConsideradoAlRecuperar);
					}else{
						montoDeRecuperacion = movimientoSiniestroService.obtenerMontoEstimacionAfectacion(estimacion.getId());
					}
					crearYSalvarRecuperacionCia(estimacion,montoDeRecuperacion );
					seHabilitaronLasCias = Boolean.TRUE;
					
				}
			}
			
			estimacion.setNumeroSiniestroTercero(autoInciso.getSiniestroCia());
			if (!StringUtils.isEmpty(autoInciso.getCiaSeguros())) {
				PrestadorServicio compania = entidadService.findById(PrestadorServicio.class, Integer.parseInt(autoInciso.getCiaSeguros()));
				estimacion.setCompaniaSegurosTercero(compania);
			}
			
			entidadService.save(estimacion);			
		}
		//TODO Cambiar a provisionar segun coresponda
		
		/*
		 Long reporteId = autoInciso.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId();
		 
		if(seCancelaronLasCias){
			this.siniestrosProvisionService.cancelaProvisionesDeCiaPorReporte(reporteId);
		}
		if(seHabilitaronLasCias){
			this.siniestrosProvisionService.habilitaProvisionesDeCiaPorReporte(reporteId);
		}
		if(seCambiaElPorcentaje){
			this.siniestrosProvisionService.cancelaProvisionesDeCiaPorReporte(reporteId);
			this.siniestrosProvisionService.habilitaProvisionesDeCiaPorReporte(reporteId);
		}
		*/
		if(seCancelaronLasCias){
			accionesAlGuardarReporteMap.put(SE_CANCELAN_LAS_CIAS, Boolean.TRUE);
		}else{
			accionesAlGuardarReporteMap.put(SE_CANCELAN_LAS_CIAS, Boolean.FALSE);
		}
		if(seHabilitaronLasCias){
			accionesAlGuardarReporteMap.put(SE_HABILITARON_LAS_CIAS, Boolean.TRUE);
		}else{
			accionesAlGuardarReporteMap.put(SE_HABILITARON_LAS_CIAS, Boolean.FALSE);
		}
		if(seCambiaElPorcentaje){
			accionesAlGuardarReporteMap.put(SE_CAMBIA_EL_PORCENTAJE, Boolean.TRUE);
		}else{
			accionesAlGuardarReporteMap.put(SE_CAMBIA_EL_PORCENTAJE, Boolean.FALSE);
		}
		return accionesAlGuardarReporteMap;
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void cancelarRecuperacionesCiaAlDesabilitarCiaTercero(EstimacionCoberturaReporteCabina estimacion){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("estimacionCobertura.id", estimacion.getId());
		params.put("coberturaRecuperacion.recuperacion.tipo", TipoRecuperacion.COMPANIA.getValue());
		
		List<PaseRecuperacion> lstPase = entidadService.findByProperties(PaseRecuperacion.class, params);
		
		for (PaseRecuperacion paseRecuperacion: CollectionUtils.emptyIfNull(lstPase)) {
			if (EnumUtil.equalsValue(paseRecuperacion.getCoberturaRecuperacion().getRecuperacion().getEstatus(), EstatusRecuperacion.PENDIENTE, EstatusRecuperacion.REGISTRADO)) {
				cancelarRecuperacion(paseRecuperacion.getCoberturaRecuperacion().getRecuperacion().getId());
			}
		}
	}
	
	
	private CartaCompania obtenerUltimaCartaCompania(Long recuperacionId){
		CartaCompania ultimaCarta = null;
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("recuperacion.id", recuperacionId);
		List<CartaCompania> cartas = this.entidadService.findByPropertiesWithOrder(CartaCompania.class, params, "folio DESC");
		if(cartas!=null && !cartas.isEmpty()){
			ultimaCarta = cartas.get(0);
		}
		return ultimaCarta;
	}

	
	
	private List<RecuperacionCiaDTO> convertir(List<RecuperacionCompania> list) {
		List<RecuperacionCiaDTO> resultados = new ArrayList<RecuperacionCiaDTO>();
		for (RecuperacionCompania e : list) {
			RecuperacionCiaDTO r = new RecuperacionCiaDTO();
			r.setNumeroRecuperacion(e.getNumero());
			r.setIdRecuperacion(e.getId());
			CartaCompania ultimaCarta = this.obtenerUltimaCartaCompania(e.getId());
			if(CommonUtils.isNotNull(e.getCartas())){
				r.setFolioCarta(ultimaCarta.getFolio().toString().equals("0") ? "" : ultimaCarta.getFolio().toString());	
				r.setObservaciones(ultimaCarta.getComentarios());
			}
			r.setFechaRegistro(e.getFechaCreacion());
			r.setNombreOficina(e.getReporteCabina().getOficina().getNombreOficina());
			r.setOficinaId(e.getReporteCabina().getOficina().getId());
			r.setTipoServicioPoliza(e.getReporteCabina().getPoliza().getCotizacionDTO().getTipoCotizacion() != null &&
					e.getReporteCabina().getPoliza().getCotizacionDTO().getTipoCotizacion().equals(CotizacionDTO.TIPO_COTIZACION_SERVICIO_PUBLICO) ? 
							Recuperacion.TipoServicioPoliza.PUBLICO.toString() : 
								Recuperacion.TipoServicioPoliza.PARTICULAR.toString());
			r.setNumeroCia(e.getCompania().getId().toString());
			r.setCompania(e.getCompania().getNombrePersona());
			r.setNumeroSiniestro(e.getReporteCabina().getSiniestroCabina().getNumeroSiniestro());
			r.setReferenciaBancaria(e.getReferenciaBancaria());
			r.setEstatusDesc(catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_RECUPERACION,
					e.getEstatus()));
			r.setMonto(e.getImporte());
			r.setOficinaRecepcion(CommonUtils.isNotNull(ultimaCarta.getOficinaRecepcion())?ultimaCarta.getOficinaRecepcion().getNombreOficina(): null);
			r.setFechaEnvio(ultimaCarta.getFechaEnvio());
			r.setFechaImpresion(ultimaCarta.getFechaImpresion());
			r.setFechaRecepcion(ultimaCarta.getFechaRecepcion());
			r.setOficinaReasignacion( CommonUtils.isNotNull(e.getOficinaReasignada())?e.getOficinaReasignada().getNombreOficina(): null);
			r.setEstatusCarta(ultimaCarta.getEstatus());
			r.setUsuarioRecepcion(ultimaCarta.getNombrePersonaRecepcion());
			r.setIdRecuperacion(e.getId());
			
			r.setFechaSiniestro(e.getReporteCabina().getFechaHoraOcurrido()); //TODO se puede cambiar a fecha de creacion de siniestro
			r.setEdoOcurrencia(e.getReporteCabina().getLugarOcurrido() != null ? e.getReporteCabina().getLugarOcurrido().getEstado().getDescripcion() : null);
			r.setFechaUltimoAcuse(ultimaCarta != null ? ultimaCarta.getFechaAcuse() : null);
			r.setFechaAcuse(e.getPrimeraCarta() != null ? e.getPrimeraCarta().getFechaAcuse() : null);			
			if(e.getCoberturas().size() > 0 && e.getCoberturas().get(0).getPasesAtencion().size() > 0){
				PaseRecuperacion pase = e.getCoberturas().get(0).getPasesAtencion().get(0);
				//TODO Remove -- PARA IDENTIFICAR ERROR
				if(pase == null){
					System.out.println(" ****  PASE ESTA NULO **** ");
				}else if(pase.getCoberturaRecuperacion() == null){
					System.out.println(" ****  PASE.COBRECUPERACION ESTA NULO **** ");
				}else if(pase.getCoberturaRecuperacion().getCoberturaReporte() == null){
					System.out.println(" ****  PASE.COBRECUPERACION.COBERTURAREPORTE ESTA NULO **** ");
				}else if(pase.getCoberturaRecuperacion().getCoberturaReporte().getCoberturaDTO() == null){
					System.out.println(" ****  PASE.COBRECUPERACION.COBERTURAREPORTE.COBERTURA ESTA NULO **** ");
				}else{
					r.setCobertura(pase.getCoberturaRecuperacion().getCoberturaReporte().getCoberturaDTO().getNombreComercial());
				}					
				r.setNombreLesionado(pase.getEstimacionCobertura() != null ? pase.getEstimacionCobertura().getNombreAfectado() : "");
				r.setVehiculoTercero(pase.getEstimacionCobertura() != null && 
						pase.getEstimacionCobertura() instanceof TerceroRCVehiculo ? 
								((TerceroRCVehiculo)pase.getEstimacionCobertura()).getDescripcionVehiculo() : "");
				if(estimacioService.esPerdidaTotal(pase.getEstimacionCobertura().getId())){
					r.setTipoSiniestro(PERDIDA_TOTAL);
				}else{
					r.setTipoSiniestro(PERDIDA_PARCIAL);
				}
			}
			
			try{
				r.setUsuario(usuarioService.buscarUsuarioPorNombreUsuario(e.getCodigoUsuarioCreacion()).getNombreCompleto() + "(" + e.getCodigoUsuarioCreacion() + ")");
			}catch(Exception x){
				r.setUsuario(e.getCodigoUsuarioCreacion());
			}

			resultados.add(r);
		}

		return resultados;
	}
	
	private Date truncateFecha(Date fecha) {
		 Calendar cal = Calendar.getInstance();
	     cal.setTime(fecha);
	     cal.set(Calendar.HOUR_OF_DAY, 0);
	     cal.set(Calendar.MINUTE, 0);
	     cal.set(Calendar.SECOND, 0);
	     cal.set(Calendar.MILLISECOND, 0);
	     return cal.getTime();
	}
	
	@Override
	public Map<String,BigDecimal> obtenerMontoPiezasSalvamento(Long estimacionId){
		
		Map<String,BigDecimal> montos = new HashMap<String,BigDecimal>();
		
		montos.put("montoPiezas"    , BigDecimal.ZERO);
		montos.put("montoSalvamento", BigDecimal.ZERO);
		
	    RecuperacionCompania recuperacion = this.siniestroProvisionDao.obtenerRecuperacionCiaPorEstimacion(estimacionId);
		
		montos.put("montoPiezas", this.montoPiezas(recuperacion,estimacionId));
		montos.put("montoSalvamento", this.montoSalvamento(recuperacion,estimacionId));
		
		return montos;
	}
	
	private BigDecimal montoSalvamento(RecuperacionCompania recuperacion,Long estimacionId){
		
		BigDecimal montoSalvamentoCiaFinal = BigDecimal.ZERO;

		if(recuperacion != null
				&& recuperacion.getId() != null){
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("recuperacion.id", recuperacion.getId());
			List<CartaCompania> cartas = this.entidadService.findByPropertiesWithOrder(CartaCompania.class, params, "id desc");
			CartaCompania ultimaCarta = cartas.get(0);
			//IMPORTE RECUPERACION CIA
			if( ultimaCarta.getEstatus().equals(CartaCompania.EstatusCartaCompania.REGISTRADO.getValue()) ){
				
				RecuperacionSalvamento salvamento = this.recuperacionSalvamentoService.obtenerRecuperacionSalvamento(estimacionId);
				if( salvamento != null && salvamento.getEstatus().equals(EstatusRecuperacion.RECUPERADO.getValue()) ){
					BigDecimal montoSalvamento = this.recuperacionSalvamentoService.obtenerVentaSalvamentoActiva(salvamento.getId()).getSubtotal();
					BigDecimal montoSalvamentoConPorcentaje = this.siniestrosProvisionService.obtenerMontoConsiderandoPorcentajeParticipacion(estimacionId, montoSalvamento, null);
					montoSalvamentoCiaFinal = montoSalvamentoConPorcentaje;
				}
			}else{
				montoSalvamentoCiaFinal = ( recuperacion.getMontoSalvamento() == null ? BigDecimal.ZERO : recuperacion.getMontoSalvamento() );
			}
		}
		
		return montoSalvamentoCiaFinal;
		
	}
	
	private BigDecimal montoPiezas(RecuperacionCompania recuperacion,Long estimacionId){
		
		BigDecimal montoPiezasCia = BigDecimal.ZERO;
		BigDecimal montoPiezasRecuperadas = BigDecimal.ZERO;
		BigDecimal diferenciaMontoPiezas = BigDecimal.ZERO;
		BigDecimal montoPiezasCiaFinal = BigDecimal.ZERO;
		
		//IMPORTE RECUPERACION CIA
		
		if(recuperacion != null
				&& recuperacion.getId() != null){
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("recuperacion.id", recuperacion.getId());
			List<CartaCompania> cartas = this.entidadService.findByPropertiesWithOrder(CartaCompania.class, params, "id desc");
			CartaCompania ultimaCarta = cartas.get(0); 
			
			if( ultimaCarta.getEstatus().equals(CartaCompania.EstatusCartaCompania.REGISTRADO.getValue()) ){
				
				// OBTENER MONTO TOTAL PIEZAS EN RECUPERACION CIA
				montoPiezasCia         = this.siniestroProvisionDao.obtenerSumaPiezas( estimacionId );
				montoPiezasRecuperadas = this.recuperacionProveedorService.montoTotalRecuperacionProveedorPagadas( estimacionId );
				
				diferenciaMontoPiezas = montoPiezasRecuperadas.subtract(montoPiezasCia); // SACAR POR PORCENTAJE
				montoPiezasCiaFinal = this.siniestrosProvisionService.obtenerMontoConsiderandoPorcentajeParticipacion(estimacionId, diferenciaMontoPiezas, null);
				
			}else{
				montoPiezasCiaFinal = ( recuperacion.getMontoPiezas() == null ? BigDecimal.ZERO : recuperacion.getMontoPiezas() );
			}
		}
		
		return montoPiezasCiaFinal;		
		
	}
	
}