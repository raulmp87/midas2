package mx.com.afirme.midas2.service.impl.tarifa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoDTO;
import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.tarifa.TarifaDTO;
import mx.com.afirme.midas2.dao.tarifa.TarifaAutoProliberDao;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoProliber;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoProliberId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;
import mx.com.afirme.midas2.dto.ControlDinamicoDTO;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.wrapper.TipoServicioVehiculoComboDTO;
import mx.com.afirme.midas2.dto.wrapper.TipoUsoVehiculoComboDTO;
import mx.com.afirme.midas2.dto.wrapper.TipoVehiculoComboDTO;
import mx.com.afirme.midas2.service.tarifa.TarifaAutoProliberService;
import mx.com.afirme.midas2.utils.Convertidor;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TarifaAutoProliberServiceImpl extends JpaTarifaService<TarifaAutoProliberId, TarifaAutoProliber> implements TarifaAutoProliberService {

	@EJB
	private TarifaAutoProliberDao tarifaAutoProliberDao;
	private Convertidor convertidor;
	private TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote;
	private SeccionFacadeRemote seccionFacadeRemote;
	private TipoUsoVehiculoFacadeRemote tipoUsoVehiculoFacadeRemote;
	private TipoServicioVehiculoFacadeRemote tipoServicioVehiculoFacadeRemote;
	
	@EJB
	public void setConvertidor(Convertidor convertidor) {
		this.convertidor = convertidor;
	}
	
	@EJB
	public void setTipoVehiculoFacadeRemote(
			TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote) {
		this.tipoVehiculoFacadeRemote = tipoVehiculoFacadeRemote;
	}

	@EJB
	public void setSeccionFacadeRemote(SeccionFacadeRemote seccionFacadeRemote) {
		this.seccionFacadeRemote = seccionFacadeRemote;
	}

	@EJB
	public void setTipoUsoVehiculoFacadeRemote(
			TipoUsoVehiculoFacadeRemote tipoUsoVehiculoFacadeRemote) {
		this.tipoUsoVehiculoFacadeRemote = tipoUsoVehiculoFacadeRemote;
	}

	@EJB
	public void setTipoServicioVehiculoFacadeRemote(
			TipoServicioVehiculoFacadeRemote tipoServicioVehiculoFacadeRemote) {
		this.tipoServicioVehiculoFacadeRemote = tipoServicioVehiculoFacadeRemote;
	}

	@Override
	public List<TarifaAutoProliber> findByFilters(TarifaAutoProliber filter) {
		return tarifaAutoProliberDao.findByFilters(filter);
	}
	
	@Override
	public List<TarifaAutoProliber> findByTarifaVersionId(
			TarifaVersionId tarifaVersionId) {
		return tarifaAutoProliberDao.findByTarifaVersionId(tarifaVersionId);
	}

	@Override
	public RegistroDinamicoDTO verDetalle(
			TarifaVersionId tarifaVersionId,
			String id,
			String accion, Class<?> vista) {
		TarifaAutoProliber tarifa = new TarifaAutoProliber();
		RegistroDinamicoDTO registro = null;
		List<ControlDinamicoDTO> controles= new ArrayList<ControlDinamicoDTO>();
		BigDecimal idTcTipoVehiculo = null;
		Predicate predicate = new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				ControlDinamicoDTO control = (ControlDinamicoDTO)arg0;
				if(control != null && control.getAtributoMapeo().equalsIgnoreCase(TarifaAutoProliber.A_M_IDTCTIPOVEHICULO) && 
						control.getValor() != null && !control.getValor().isEmpty()){
					return true;
				}
				return false;
			}
		};
		
		if(TipoAccionDTO.getAgregarModificar().equals(accion)){
			TarifaAutoProliberId idTarifa = new TarifaAutoProliberId(tarifaVersionId);
			tarifa.setId(idTarifa);
			registro = convertidor.getRegistroDinamico(tarifa, vista);

			controles = getControles(registro.getControles(), tarifaVersionId.getIdRiesgo(), accion);			
			convertidor.setAttribute(TarifaDTO.ELEMENTOS_COMBO, new ArrayList<TipoUsoVehiculoDTO>(), 
					controles, TarifaAutoProliber.A_M_CODIGOTIPOUSOVEHICULO);
			convertidor.setAttribute(TarifaDTO.ELEMENTOS_COMBO, new ArrayList<TipoServicioVehiculoDTO>(), 
					controles, TarifaAutoProliber.A_M_CODIGOTIPOUSOVEHICULO);
		}else if(TipoAccionDTO.getEliminar().equals(accion) || TipoAccionDTO.getEditar().equals(accion)){
			TarifaAutoProliberId tarifaId = new TarifaAutoProliberId(id);
			tarifa = findById(TarifaAutoProliber.class, tarifaId);
			List<TipoUsoVehiculoComboDTO> tipoUsoVehiculo = getListaTipoUsoVehiculoComboDTO(idTcTipoVehiculo);
			List<TipoServicioVehiculoComboDTO> tipoServicoVehiculo = getListaTipoServicioVehiculoComboDTO(idTcTipoVehiculo);

			registro = convertidor.getRegistroDinamico(tarifa, vista);
			if(registro!=null){
				controles= getControles(registro.getControles(), tarifaVersionId.getIdRiesgo(), accion);
				
				ControlDinamicoDTO control = (ControlDinamicoDTO)CollectionUtils.find(controles, predicate);
				if(control != null){
					idTcTipoVehiculo = new BigDecimal(control.getValor());
				}
				
				if(TipoAccionDTO.getEliminar().equals(accion)){
					convertidor.setAttribute("editable", false, controles);			
				}else{
					convertidor.setAttribute("editable", false, controles, 
							TarifaAutoProliber.A_M_CLAVEGRUPOPROLIBER, TarifaAutoProliber.A_M_TONELEDASCAPACMIN,
							TarifaAutoProliber.A_M_TONELEDASCAPACMAX, TarifaAutoProliber.A_M_NUMEROREMOLQUES,
							TarifaAutoProliber.A_M_CODIGOTIPOUSOVEHICULO, TarifaAutoProliber.A_M_CODIGOTIPOUSOVEHICULO,
							TarifaAutoProliber.A_M_IDTCTIPOVEHICULO);
				}

				convertidor.setAttribute(TarifaDTO.ELEMENTOS_COMBO, 
						tipoUsoVehiculo, controles, TarifaAutoProliber.A_M_CODIGOTIPOUSOVEHICULO);
				convertidor.setAttribute(TarifaDTO.ELEMENTOS_COMBO, 
						tipoServicoVehiculo, controles, TarifaAutoProliber.A_M_CODIGOTIPOUSOVEHICULO);
			}		
		}
		registro.setControles(controles);	
		
		return registro;
	}

	@Override
	public List<RegistroDinamicoDTO> getRegistrosDinamicosPorTarifaVersionId(
			TarifaVersionId tarifaVersionId, Class<?> vista) {

		List<TarifaAutoProliber> tarifas = this.findByTarifaVersionId(tarifaVersionId);
		List<RegistroDinamicoDTO> registros = convertidor.getListaRegistrosDinamicos(tarifas, vista);
		
		BigDecimal idTcTipoVehiculo = null;
		Predicate predicate = new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				ControlDinamicoDTO control = (ControlDinamicoDTO)arg0;
				if(control != null && control.getAtributoMapeo().equalsIgnoreCase(TarifaAutoProliber.A_M_IDTCTIPOVEHICULO) && 
						control.getValor() != null && !control.getValor().isEmpty()){
					return true;
				}
				return false;
			}
		};
		
		for(RegistroDinamicoDTO registro: registros){
			if(registro!=null){
				List<TipoUsoVehiculoComboDTO> tipoUsoVehiculo = new ArrayList<TipoUsoVehiculoComboDTO>(1);
				tipoUsoVehiculo = getListaTipoUsoVehiculoComboDTO(idTcTipoVehiculo);
				
				List<ControlDinamicoDTO> controles= getControles(registro.getControles(), tarifaVersionId.getIdRiesgo(), 
						TipoAccionDTO.getAgregarModificar());

				ControlDinamicoDTO control = (ControlDinamicoDTO)CollectionUtils.find(controles, predicate);
				if(control != null){ 
					idTcTipoVehiculo = new BigDecimal(control.getValor());
				}
				convertidor.setAttribute(TarifaDTO.ELEMENTOS_COMBO, 
						tipoUsoVehiculo, controles, TarifaAutoProliber.A_M_CODIGOTIPOUSOVEHICULO);
				convertidor.setAttribute(TarifaDTO.ELEMENTOS_COMBO, 
						tipoUsoVehiculo, controles, TarifaAutoProliber.A_M_CODIGOTIPOUSOVEHICULO);
			}
		}
		
		return registros;
	}
	
	public TarifaAutoProliber getNewObject(){
		return new TarifaAutoProliber();
	}
	
	
	@Override
	public List<TipoVehiculoComboDTO> getListaTipoVehiculoComboDTO(BigDecimal idToSeccion){
		 List<TipoVehiculoComboDTO>  tipoVehiculoComboDTOList = new  ArrayList<TipoVehiculoComboDTO>();
		 
		 SeccionDTO seccionDTO = seccionFacadeRemote.findById(idToSeccion);

		 if(seccionDTO.getTipoVehiculo()!=null){			 
			 List<TipoVehiculoDTO> tipoVehiculoDTOList = tipoVehiculoFacadeRemote.findByProperty("tipoBienAutosDTO.claveTipoBien", 
					 seccionDTO.getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());
		
			 for(TipoVehiculoDTO tipoVehiculoDTO : tipoVehiculoDTOList){
				 tipoVehiculoComboDTOList.add(new TipoVehiculoComboDTO(tipoVehiculoDTO));
			 }
		 }
		 return tipoVehiculoComboDTOList;
	}

	@Override
	public List<TipoServicioVehiculoComboDTO> getListaTipoServicioVehiculoComboDTO(
			BigDecimal idTcTipoVehiculo) {
		
		List<TipoServicioVehiculoComboDTO> tipoServicioVehiculoComboDTOList = new ArrayList<TipoServicioVehiculoComboDTO>(1);
		List<TipoServicioVehiculoDTO> tipoServicioVehiculoDTOList = new ArrayList<TipoServicioVehiculoDTO>(1);
		if(idTcTipoVehiculo!=null){
			tipoServicioVehiculoDTOList = tipoServicioVehiculoFacadeRemote.findByProperty("idTcTipoVehiculo", idTcTipoVehiculo);			
		}else{
			tipoServicioVehiculoDTOList = tipoServicioVehiculoFacadeRemote.findAll();
		}
		
		for(TipoServicioVehiculoDTO tipoServicioVehiculoDTO : tipoServicioVehiculoDTOList){
			tipoServicioVehiculoComboDTOList.add(new TipoServicioVehiculoComboDTO(tipoServicioVehiculoDTO));
		}
		
		return tipoServicioVehiculoComboDTOList;
	}

	@Override
	public List<TipoUsoVehiculoComboDTO> getListaTipoUsoVehiculoComboDTO(
			BigDecimal idTcTipoVehiculo) {
		List<TipoUsoVehiculoComboDTO> tipoUsoVehiculoComboDTOList = new ArrayList<TipoUsoVehiculoComboDTO>(1);
		List<TipoUsoVehiculoDTO> tipoUsoVehiculoDTOList = new ArrayList<TipoUsoVehiculoDTO>(1);
		 
		if(idTcTipoVehiculo!=null){
			tipoUsoVehiculoDTOList = tipoUsoVehiculoFacadeRemote.findByProperty("idTcTipoVehiculo", idTcTipoVehiculo);
		}else{
			tipoUsoVehiculoDTOList = tipoUsoVehiculoFacadeRemote.findAll();
		}
		
		for(TipoUsoVehiculoDTO tipoUsoVehiculoDTO : tipoUsoVehiculoDTOList){
			tipoUsoVehiculoComboDTOList.add(new TipoUsoVehiculoComboDTO(tipoUsoVehiculoDTO));
		}
		
		return tipoUsoVehiculoComboDTOList;
	}

	private List<ControlDinamicoDTO> getControles(List<ControlDinamicoDTO> controles, Long idRiesgo,
			String accion) {
		List<ControlDinamicoDTO> ctrls=new ArrayList<ControlDinamicoDTO>(1);
		
		if(TarifaAutoProliberId.AUTO_SUSTITUTO_ROBO.equals(idRiesgo)){
			for(ControlDinamicoDTO ctrl: controles){
				String atributoMapeo = ctrl.getAtributoMapeo();

				if(atributoMapeo.equalsIgnoreCase("valorPrimaTotal")){
					ctrl.setEtiqueta("Prima Total");
				}
				
				if((atributoMapeo.equalsIgnoreCase(TarifaAutoProliber.A_M_CLAVEGRUPOPROLIBER) 
						|| atributoMapeo.equalsIgnoreCase(TarifaAutoProliber.A_M_CODIGOTIPOUSOVEHICULO)
						|| atributoMapeo.equalsIgnoreCase(TarifaAutoProliber.A_M_CODIGOTIPOSERVICIOVEHICULO))){
					ctrl.setTipoControl(TipoControl.HIDDEN.getValue());
					ctrl.setValor(" ");
				}
				
				if(atributoMapeo.equalsIgnoreCase(TarifaAutoProliber.A_M_TONELEDASCAPACMIN)
						|| atributoMapeo.equalsIgnoreCase(TarifaAutoProliber.A_M_TONELEDASCAPACMAX) 
						|| atributoMapeo.equalsIgnoreCase(TarifaAutoProliber.A_M_NUMEROREMOLQUES)){

					ctrl.setValor("0");
					ctrl.setTipoControl(TipoControl.HIDDEN.getValue());
					
				}
					
				if(atributoMapeo.equalsIgnoreCase(TarifaAutoProliber.A_M_CODIGOTIPOSERVICIOVEHICULO)){
					ctrl.setValor("TO");
				}
				ctrls.add(ctrl);
			}
		}else{
			ctrls.addAll(controles);
		}
		return ctrls;
	}	

}
