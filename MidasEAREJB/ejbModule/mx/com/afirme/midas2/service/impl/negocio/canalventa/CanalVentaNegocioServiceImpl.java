package mx.com.afirme.midas2.service.impl.negocio.canalventa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;
import mx.com.afirme.midas2.dao.negocio.canalventa.NegocioCanalVentaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.negocio.canalventa.NegocioCanalVenta;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.canalventa.CanalVentaNegocioService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.util.StringUtil;

@Stateless
public class CanalVentaNegocioServiceImpl implements CanalVentaNegocioService{
	/**
	 * 
	 */
		
	private static final long serialVersionUID = 620063385139743335L;
	
	private static final String  PROP_ENTIDAD_CANALVENTA_NEGOCIO ="negocio.idToNegocio";
	private static final String  PROP_ENTIDAD_CATALOGO_CODIGOPADRE ="catalogo.codigoPadre";
	private static final String  PROP_ENTIDAD_CATALOGO_CODIGO ="catalogo.codigo";
	private static final String  PROP_ENTIDAD_CANALVENTA_STATUS ="status";
	private static final String  PROP_ENTIDAD_CATALOGO_ACTIVO ="catalogo.activo";


	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private NegocioCanalVentaDao negocioCanalVentaDao;
	
	
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	private static final Logger LOG = Logger.getLogger(CanalVentaNegocioServiceImpl.class);

	public List<NegocioCanalVenta> getCamposPorTipoYNegocio(TIPO_CATALOGO tipo, Long idNegocio){
		LOG.debug("###getCamposPorTipoYNegocio-tipo:"+tipo.name()+",negocio:"+idNegocio);
		LOG.debug("##getCamposPorTipoYNegocio-campos sincronizados:"+negocioCanalVentaDao.sincronizarCamposCatCanalVta(tipo,idNegocio,usuarioService.getUsuarioActual().getNombreUsuario(),usuarioService.getUsuarioActual().getNombreUsuario()));
		List<NegocioCanalVenta> listaCampos = new ArrayList<NegocioCanalVenta>();
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put(PROP_ENTIDAD_CANALVENTA_NEGOCIO, idNegocio);
		params.put(PROP_ENTIDAD_CATALOGO_ACTIVO, true);
		listaCampos = entidadService.findByPropertiesWithOrder(NegocioCanalVenta.class, params,  "catalogo.codigoPadre", "catalogo.orden");
		LOG.debug("getCamposPorTipoYNegocio-listaCampos:"+listaCampos.size());
		return listaCampos;
	}

	/**
	 * Obtener la lista de NegocioCanalVenta relacionadas a un código padre
	 * @param tipo
	 * @param codigoPadre
	 * @param idNegocio
	 * @return
	 */
	public List<NegocioCanalVenta> getCamposPorTipoYNegocio(TIPO_CATALOGO tipo, String codigoPadre, BigDecimal idNegocio){

		List<NegocioCanalVenta> listaCampos = new ArrayList<NegocioCanalVenta>();
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		
		params.put(PROP_ENTIDAD_CANALVENTA_NEGOCIO, idNegocio);
		params.put(PROP_ENTIDAD_CATALOGO_ACTIVO, true);
		params.put(PROP_ENTIDAD_CATALOGO_CODIGOPADRE, codigoPadre);
		
		listaCampos = entidadService.findByProperties(NegocioCanalVenta.class, params);

		return listaCampos;
	}
	
	private NegocioCanalVenta getCampoEditado(Long idCampoEditado){
		return entidadService.findById(NegocioCanalVenta.class, idCampoEditado);
	}
	
	
	private NegocioCanalVenta editCampo(NegocioCanalVenta campo, boolean visible){
			if(StringUtil.isEmpty(campo.getCodigoUsuarioCreacion())){
				campo.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());		
				campo.setFechaCreacion(new Date());
			}
			campo.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			campo.setFechaModificacion(new Date());	
			campo.setStatus(visible);
			return campo;
	}

	

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveCamposNegocio(Long idCampoEditado) {
		if(idCampoEditado>0){
			NegocioCanalVenta campoEditado=getCampoEditado(idCampoEditado);
			entidadService.refresh(campoEditado);

			boolean esPapa=campoEditado.getCatalogo().getCodigoPadre().equals(TIPO_CATALOGO.GPO_GPOS.name());
			//si es papa
			if(esPapa){
				//se guarda la seccion completa
				LOG.debug("##saveCamposNegocio-campos guardados:"+negocioCanalVentaDao.saveSectionCamposNegocio(!campoEditado.getStatus(), 
						campoEditado.getNegocio().getIdToNegocio(), campoEditado.getCatalogo().getCodigo()));
			}
			//si es hijo
			else{
					if(validarUltimoPrendido(campoEditado) || validarPrimeroPrendido(campoEditado))
						guardarPapa(campoEditado);
					
			campoEditado=editCampo(campoEditado, !campoEditado.getStatus());
			entidadService.save(campoEditado);
			}
		}
	}
	
	
	
	private boolean validarUltimoPrendido(NegocioCanalVenta campoEditado){
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put(PROP_ENTIDAD_CANALVENTA_STATUS, true);
		params.put(PROP_ENTIDAD_CATALOGO_CODIGOPADRE, campoEditado.getCatalogo().getCodigoPadre());
		params.put(PROP_ENTIDAD_CANALVENTA_NEGOCIO, campoEditado.getNegocio().getIdToNegocio());
		params.put(PROP_ENTIDAD_CATALOGO_ACTIVO, true);
		Long campos =entidadService.findByPropertiesCount(NegocioCanalVenta.class, params);
		if(campos==1)
		{	
			return true;
		}
		return false;
	}
	
	private boolean validarPrimeroPrendido(NegocioCanalVenta campoEditado){
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put(PROP_ENTIDAD_CANALVENTA_STATUS, true);
		params.put(PROP_ENTIDAD_CATALOGO_CODIGOPADRE, campoEditado.getCatalogo().getCodigoPadre());
		params.put(PROP_ENTIDAD_CANALVENTA_NEGOCIO, campoEditado.getNegocio().getIdToNegocio());
		params.put(PROP_ENTIDAD_CATALOGO_ACTIVO, true);
		Long campos =entidadService.findByPropertiesCount(NegocioCanalVenta.class, params);
		if(campos==0)
		{	
			return true;
		}
		return false;
	}
	
	private void guardarPapa(NegocioCanalVenta campoEditado){
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put(PROP_ENTIDAD_CATALOGO_CODIGO, campoEditado.getCatalogo().getCodigoPadre());
		params.put(PROP_ENTIDAD_CANALVENTA_NEGOCIO, campoEditado.getNegocio().getIdToNegocio());
		List<NegocioCanalVenta> campos =entidadService.findByProperties(NegocioCanalVenta.class, params);
		try{
		NegocioCanalVenta campoPapa=campos.get(0);
		campoPapa=editCampo(campoPapa, !campoEditado.getStatus());
		entidadService.save(campoPapa);
		}catch(ArrayIndexOutOfBoundsException e){System.out.println("###CanalVentaNegocioServiceImpl-guardarPapa, no se pudo guardar el campo seccion");}
	}
	

	@Override
	public void saveAllCamposNegocio(Boolean visible, Long idNegocio) {
		LOG.debug("##saveAllCamposNegocio-campos guardados:"+negocioCanalVentaDao.saveAllCamposNegocio(visible,idNegocio));
	}
	
	public String getCamposPorTipoYNegocioJSON(String tipo, Long idNegocio){
		List<NegocioCanalVenta> listaCampos= new ArrayList<NegocioCanalVenta>();
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put(PROP_ENTIDAD_CATALOGO_CODIGOPADRE, tipo);
		params.put(PROP_ENTIDAD_CANALVENTA_NEGOCIO, idNegocio);
		params.put(PROP_ENTIDAD_CANALVENTA_STATUS, false);
		params.put(PROP_ENTIDAD_CATALOGO_ACTIVO, true);

		listaCampos = entidadService.findByProperties(NegocioCanalVenta.class, params);
		if(getTotalCamposPorSeccion(tipo,idNegocio)==listaCampos.size())
			listaCampos= getSeccion(tipo, idNegocio, listaCampos);
		return this.getCamposJson(listaCampos);
	}
	
	private long getTotalCamposPorSeccion(String tipo, Long idNegocio){
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put(PROP_ENTIDAD_CATALOGO_CODIGOPADRE, tipo);
		params.put(PROP_ENTIDAD_CANALVENTA_NEGOCIO, idNegocio);

		 return entidadService.findByPropertiesCount(NegocioCanalVenta.class, params);
		
	}
	
	private List<NegocioCanalVenta> getSeccion(String tipo, Long idNegocio, List<NegocioCanalVenta> listaCampos){
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put(PROP_ENTIDAD_CATALOGO_CODIGO, tipo);
		params.put(PROP_ENTIDAD_CANALVENTA_NEGOCIO, idNegocio);
		List<NegocioCanalVenta> seccion = entidadService.findByProperties(NegocioCanalVenta.class, params);
		if(seccion!=null && !seccion.isEmpty()){
			listaCampos.add(seccion.get(0));
		}
		return listaCampos;
	}
	
	private String getCamposJson(List<NegocioCanalVenta> campos){
		StringBuilder json = new StringBuilder();
		json.append("[");
		for(NegocioCanalVenta campo: campos){
			json.append(getCampoJson(campo));
			json.append(",");
		}
		if(json.length()>1)
			json.delete(json.length()-1, json.length());
		json.append("]");
		LOG.debug("##getCamposJson:"+json);
		return json.toString();
	}
	
	// METODO INTERNO QUE REGRESA CADENA JSON A PARTIR DE DTO DE NegocioCanalVenta
	private String getCampoJson(NegocioCanalVenta campo) {
		CatValorFijo cat=campo.getCatalogo();
		StringBuilder json = new StringBuilder();
		json.append("{");
		json.append("\"codigo\"").append(":").append("\"")
				.append(cat.getCodigo().toString()).append("\",");
		json.append("\"status\"").append(":").append(campo.getStatus());
		json.append("}");
		return json.toString();
	}

}
