package mx.com.afirme.midas2.service.impl.siniestros.catalogo.prestadordeservicios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.sistema.seguridad.UsuarioPrestadorServicio;
import mx.com.afirme.midas2.dao.siniestros.catalogo.PrestadorServicioDao;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaDireccionMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaFisicaMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMoralMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.log4j.Logger;

import com.asm.dto.CreatePasswordUserParameterDTO;
import com.asm.dto.CreatePasswordUserParameterDTO.PersonType;

@Stateless
public class PrestadorDeServicioServiceImpl implements  PrestadorDeServicioService{
	
	public  static final Logger log = Logger.getLogger(PrestadorDeServicioServiceImpl.class);
	
	private static final int ESTATUS_ACTIVO = 1;
	
	private static final String ROL_PROVEEDOR = "Rol_M2_Proveedor";
	
	@EJB
	private CatalogoSiniestroService catalogoService;
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private DireccionMidasService direccionService;
	
	
	@EJB
	private PrestadorServicioDao prestadorServicioDao;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private SistemaContext sistemaContext ; 


	@Override
	public List<PrestadorServicioRegistro> buscar(PrestadorServicioFiltro filtro) {
		List<PrestadorServicioRegistro> prestadores = prestadorServicioDao.buscar(filtro);
		//List<PrestadorServicio> prestadorDeServiciosList = catalogoService.buscar(PrestadorServicio.class, filtro, "persona.nombre");
		//List<PrestadorServicio> listaFiltrada = filterByTipoPrestadorServicio(prestadorDeServiciosList, filtro);
		//listaFiltrada = filterByEstado(listaFiltrada, filtro);
		return prestadores;
	}
	
	
	private List<PrestadorServicio> filterByTipoPrestadorServicio(List<PrestadorServicio> prestadorDeServiciosList, PrestadorServicioFiltro filtro){
		List<PrestadorServicio> prestadorServicioFiltered = new ArrayList<PrestadorServicio>(); 
		if(filtro.getTipoPrestadorStr()!=null && !filtro.getTipoPrestadorStr().equals("")){
			for(PrestadorServicio prestadorServicio : prestadorDeServiciosList){
				List<TipoPrestadorServicio> currentTipoPrestadorList = prestadorServicio.getTiposPrestador();
				currentTipoPrestadorList.isEmpty();
				if(filtro.getTipoPrestadorStr()!= null){
					if(!currentTipoPrestadorList.isEmpty()){
						for(TipoPrestadorServicio tipoPrestador: currentTipoPrestadorList){
							if(tipoPrestador.getNombre().equals(filtro.getTipoPrestadorStr())){
								prestadorServicioFiltered.add(prestadorServicio);
								break;
							}
						}
					}
				}else{
					prestadorServicioFiltered = prestadorDeServiciosList;
				}
			}
		}else{
			return prestadorDeServiciosList;
		}
		return prestadorServicioFiltered;
	}
	
	private List<PrestadorServicio> filterByEstado(List<PrestadorServicio> prestadorDeServiciosList, PrestadorServicioFiltro filtro){
		List<PrestadorServicio> prestadorServicioFiltered = new ArrayList<PrestadorServicio>(); 
		if(filtro.getEstado()!= null && !filtro.getEstado().equals("") ){
			for(PrestadorServicio prestadorServicio : prestadorDeServiciosList){
				List<PersonaDireccionMidas> personaDireccionList  = prestadorServicio.getPersonaMidas().getPersonaDireccion();
					personaDireccionList.isEmpty();
					if(!personaDireccionList.isEmpty()){
						for(PersonaDireccionMidas direccion: personaDireccionList){
							//String cp = direccion.getDireccion().getCodigoPostalColonia();							
							CiudadMidas ciudad = direccionService.obtenerCiudadPorColonia(direccion.getDireccion().getColonia().getId());
							if (ciudad != null) {
								String estado = ciudad.getEstado().getId();
								if(filtro.getEstado().equals(estado)){
									prestadorServicioFiltered.add(prestadorServicio);
									break;
								}
							}
							
						}
					}
				}
		}else{
			return prestadorDeServiciosList;
		}
		return prestadorServicioFiltered;
	}
	
	public PrestadorServicio buscarPrestador(Integer idPrestadorServicio){
		return entidadService.findById(PrestadorServicio.class, idPrestadorServicio);
	}


	public CatalogoSiniestroService getCatalogoService() {
		return catalogoService;
	}

	
	public void setCatalogoService(CatalogoSiniestroService catalogoService) {
		this.catalogoService = catalogoService;
	}


	public UsuarioService getUsuarioService() {
		return usuarioService;
	}


	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}


	@Override
	public List<Map<String, Object>> obtenerPrestadoresPorUsuario(
			String codigoUsuario) {
		
		List<Map<String, Object>> listaPrestadoresServicio = new ArrayList<Map<String, Object>>();
		
		Usuario usuarioActual = usuarioService.getUsuarioActual();
//		if(false){
		if(usuarioService.tieneRol(ROL_PROVEEDOR, usuarioActual)){
			//EXTERNO
			HashMap<String,Object> params = new HashMap<String,Object>();
			params.put("codigoUsuario", codigoUsuario);
			params.put("prestadorServicio.estatus", ESTATUS_ACTIVO);
			
			List<UsuarioPrestadorServicio> listaProveedoresPorUsuario = new ArrayList<UsuarioPrestadorServicio>();
			listaProveedoresPorUsuario = entidadService.findByPropertiesWithOrder(UsuarioPrestadorServicio.class, params, "prestadorServicio.personaMidas.nombre");
			for (UsuarioPrestadorServicio proveedorPorUsuario : listaProveedoresPorUsuario) {
				Map<String, Object> proveedorMap = new HashMap<String, Object>();			
				proveedorMap.put("id", proveedorPorUsuario.getPrestadorServicio().getId());
				proveedorMap.put("label", proveedorPorUsuario.getPrestadorServicio().getPersonaMidas().getNombre());
				listaPrestadoresServicio.add(proveedorMap);
			}			
		}else{
			PrestadorServicioFiltro filtro = new PrestadorServicioFiltro();
			filtro.setEstatus(ESTATUS_ACTIVO);
			
			List<PrestadorServicioRegistro> listaProveedores= this.buscar(filtro);
			for (PrestadorServicioRegistro proveedor : listaProveedores) {
				if(proveedor.getPersonaMidas()!= null ){
					Map<String, Object> proveedorMap = new HashMap<String, Object>();			
					Long idProveedor = proveedor.getId();
					proveedorMap.put("id", idProveedor);
					proveedorMap.put("label", idProveedor+" - "+proveedor.getPersonaMidas().getNombre().replaceAll("\"", "'")); 
					listaPrestadoresServicio.add(proveedorMap);
				}
			}
		}
		return listaPrestadoresServicio;
	}
	
	@Override
	public PrestadorServicio guardar(PrestadorServicio prestador){		
		 if(prestador.getId() != null){
			 entidadService.save(prestador.getPersonaMidas());
		 }
		prestador.setId(((Integer)entidadService.saveAndGetId(prestador)));		
		return prestador;
	} 
	
	
	@Override
	public UsuarioPrestadorServicio generarUsuario(Integer prestadorId){		
		UsuarioPrestadorServicio usuarioPrestador = null;		
		try{
			PrestadorServicio prestador = entidadService.findById(PrestadorServicio.class, prestadorId);
			usuarioPrestador = obtenerUsuarioPorPrestador(prestadorId);
			if(usuarioPrestador != null){
				throw new RuntimeException("El prestador ya cuenta con un usuario asociado.");
			}			
			CreatePasswordUserParameterDTO user = new CreatePasswordUserParameterDTO();
			user.setCellPhone(prestador.getPersonaMidas().getContacto().getTelCelularCompleto());
			user.setEmail(prestador.getPersonaMidas().getContacto().getCorreoPrincipal());
			if(prestador.getPersonaMidas().getTipoPersona().equals(PersonaMidas.TIPO_PERSONA.PF.toString())){
				user.setPersonType(PersonType.NATURAL);
				user.setFatherLastName(((PersonaFisicaMidas)prestador.getPersonaMidas()).getApellidoPaterno());
				user.setMotherLastName(((PersonaFisicaMidas)prestador.getPersonaMidas()).getApellidoMaterno());
				user.setFirstName(((PersonaFisicaMidas)prestador.getPersonaMidas()).getNombrePersona());			
			}else{
				user.setPersonType(PersonType.LEGAL);
				user.setFirstName(((PersonaMoralMidas)prestador.getPersonaMidas()).getNombre());				
			}
			
			//Obtener rolId para el rol de proveedor Rol_M2_Proveedor		
			Integer roleId = usuarioService.findRoleId(Integer.valueOf(sistemaContext.getMidasAppId()), ROL_PROVEEDOR);
			if(roleId == null){
				throw new RuntimeException("No está configurado el rol a utilizar por el usuario");
			}
			List<Integer> roleIds = new ArrayList<Integer>();
			roleIds.add(roleId);
			user.setRoleIds(roleIds);
			
			//crear usuario
			Usuario usuario = usuarioService.createPasswordUser(user);
			if(usuario == null){
				throw new RuntimeException("El servicio no creó un usuario.");
			}
			usuarioPrestador = new UsuarioPrestadorServicio(usuario.getNombreUsuario(), prestador);
			entidadService.save(usuarioPrestador);
		}catch(Exception ex){
			log.error("No se pudo crear el usuario para el prestador de servicio", ex);
			throw new NegocioEJBExeption("ERR_ASMCREAR: ", ex.getMessage());			
		}
		return usuarioPrestador;
	}


	@Override
	public UsuarioPrestadorServicio obtenerUsuarioPorPrestador(Integer prestadorId) {
		List<UsuarioPrestadorServicio> usuarios =  entidadService.findByProperty(
				UsuarioPrestadorServicio.class, "prestadorServicio.id", prestadorId);
		if(usuarios != null && usuarios.size() > 0){
			return usuarios.get(0);
		}
		return null;
	}


	@Override
	public void actualizarUsuarioPrestador(Integer prestadorId, String claveUsuario) {
		UsuarioPrestadorServicio usuarioPrestador = obtenerUsuarioPorPrestador(prestadorId);
		if(usuarioPrestador == null){
			throw new NegocioEJBExeption("ERR_NOUSRPRES: ", "El prestador no cuenta con usuario, se debe generar uno primero");
		}		
		if(!StringUtil.isEmpty(claveUsuario)){
			Usuario usuario = usuarioService.buscarUsuarioPorNombreUsuario(claveUsuario);
			if(usuario == null){
				throw new NegocioEJBExeption("ERR_NOUSER: ", "No existe un usuario en ASM con esa clave de usuario");
			}
			usuarioPrestador.setCodigoUsuario(claveUsuario);
			entidadService.save(usuarioPrestador);
		}else{
			entidadService.remove(usuarioPrestador);
		}				
	}

}
