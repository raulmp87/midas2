package mx.com.afirme.midas2.service.impl.bitemporal.endoso.inciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.bitemporal.endoso.inciso.IncisoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.Inciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.BitemporalDatoSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.DatoSeccionContinuity;
import mx.com.afirme.midas2.domain.catalogos.ServVehiculoLinNegTipoVeh;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.inciso.IncisoSoporteService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;

import com.anasoft.os.daofusion.bitemporal.RecordStatus;

@Stateless
public class IncisoSoporteServiceImpl implements IncisoSoporteService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public BitemporalInciso guardaIncisoBorrador(BitemporalInciso inciso, BitemporalAutoInciso autoInciso, Map<String, String> datosRiesgo,
			List<BitemporalCoberturaSeccion> coberturas, DateTime validoEn) {
		
		BitemporalSeccionInciso seccionInciso = null;
		SeccionDTO seccion = null;
		BitemporalCoberturaSeccion coberturaOriginal = null;
		List<BitemporalCoberturaSeccion> coberturasOriginales = new ArrayList<BitemporalCoberturaSeccion>();
		Boolean nuevoInciso = true;
		Boolean cambioAutoInciso = false;
		
		//Si existe un id de inciso significa que se trata de calculo de un inciso existente (valido o en proceso)
		if (inciso.getEntidadContinuity().getId() != null) {
			if(inciso.getContinuity().getIncisos().get(validoEn)!=null){ //valido
				nuevoInciso = false;
			}else{ //En proceso
				if(!inciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().getInProcess(validoEn).getValue().getNegocioSeccionId().equals(autoInciso.getValue().getNegocioSeccionId())){ //Cambio seccionInciso
					nuevoInciso = true;
					entidadContinuityDao.remove(inciso.getContinuity());
				}else{ //Se mantuvo seccionInciso
					nuevoInciso = false;
				}
			}
		}
			
		//Se guarda el inciso
		//En caso de ser un nuevo inciso se agregan datos como el numero de inciso, el numero de secuencia y la descripcion del giro asegurado
		if (nuevoInciso) {
			if (inciso.getEntidadContinuity().getNumero() == null)
				inciso.getEntidadContinuity().setNumero(getNuevoNumeroInciso(inciso.getEntidadContinuity().getParentContinuity().getId(), validoEn));
			
			if (inciso.getValue().getNumeroSecuencia() == null)
				inciso.getValue().setNumeroSecuencia(
						getNuevoNumeroSecuencia(inciso.getEntidadContinuity().getParentContinuity().getId(), validoEn));
			
			BitemporalCotizacion cotizacion = inciso.getEntidadContinuity().getParentContinuity().getBitemporalProperty().get(validoEn);
			entidadBitemporalService.attachToParentAndSet(inciso, cotizacion, validoEn);
			
			NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, new BigDecimal(autoInciso.getValue().getNegocioSeccionId()));
			seccion = negocioSeccion.getSeccionDTO();
			NegocioPaqueteSeccion negocioPaquete = entidadService.findById(NegocioPaqueteSeccion.class, autoInciso.getValue().getNegocioPaqueteId());
			autoInciso.getValue().setPaquete(negocioPaquete.getPaquete());
			//inciso.getValue().setDescripcionGiroAsegurado(seccion.getDescripcion() + " / " + autoInciso.getValue().getDescripcionFinal());
			
			inciso.getEntidadContinuity().setAutoIncisoContinuity(
					(AutoIncisoContinuity)entidadBitemporalService.attachToParentAndSet(autoInciso, inciso, validoEn));
			
			seccionInciso = entidadBitemporalService.getNew(BitemporalSeccionInciso.class, null);
			seccionInciso.getEntidadContinuity().setSeccion(seccion);
			seccionInciso.getValue().setClaveContrato((short)1);
			seccionInciso.getValue().setClaveObligatoriedad(new Short(seccion.getClaveObligatoriedad()));
			seccionInciso.getValue().setValorPrimaNeta(0D);
			seccionInciso.getValue().setValorSumaAsegurada(0D);
			
			TipoUsoVehiculoDTO tipoUsoVehiculo = entidadService.findById(TipoUsoVehiculoDTO.class, 
					BigDecimal.valueOf(autoInciso.getValue().getTipoUsoId()));
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id.idToSeccion", seccionInciso.getContinuity().getSeccion().getIdToSeccion());
			params.put("id.idTcTipoVehiculo", tipoUsoVehiculo.getIdTcTipoVehiculo());

			List<ServVehiculoLinNegTipoVeh> negTipoVeh = entidadService
					.findByProperties(ServVehiculoLinNegTipoVeh.class, params);
			if (!negTipoVeh.isEmpty()) {
				autoInciso.getValue().setTipoServicioId(
						negTipoVeh.get(0).getTipoServicioVehiculoDTO().getIdTcTipoServicioVehiculo().longValue());
			}
			
			inciso.getEntidadContinuity().getIncisoSeccionContinuities().getValue().add(
					(SeccionIncisoContinuity)entidadBitemporalService.attachToParentAndSet(seccionInciso, inciso, validoEn));
		} else {
			BitemporalAutoInciso autoIncisoOriginal = inciso.getEntidadContinuity().getAutoIncisoContinuity()
				.getBitemporalProperty().getInProcess(validoEn);
			
			if (autoInciso.getEntidadContinuity().equals(autoIncisoOriginal.getEntidadContinuity())) {
				if (!autoIncisoOriginal.getValue().equals(autoInciso.getValue())) {
					
					NegocioPaqueteSeccion negocioPaquete = entidadService.findById(NegocioPaqueteSeccion.class, autoInciso.getValue().getNegocioPaqueteId());
					autoInciso.getValue().setPaquete(negocioPaquete.getPaquete());
					
					autoInciso.setEntidadContinuity(autoIncisoOriginal.getEntidadContinuity());
					inciso.getEntidadContinuity().setAutoIncisoContinuity(
							(AutoIncisoContinuity)entidadBitemporalService.attachToParentAndSet(autoInciso, inciso, validoEn));
					cambioAutoInciso = true;
				}else if(autoInciso.getRecordStatus().equals(RecordStatus.TO_BE_ADDED)){
					cambioAutoInciso = true;
				}
			} else {
				inciso.getEntidadContinuity().setAutoIncisoContinuity(
						(AutoIncisoContinuity)entidadBitemporalService.attachToParentAndSet(autoInciso, inciso, validoEn));
			}
			
			if (inciso.getEntidadContinuity().getIncisoSeccionContinuities().getInProcess(validoEn) != null
					&& inciso.getEntidadContinuity().getIncisoSeccionContinuities().getInProcess(validoEn).iterator().hasNext()) {
				seccionInciso = inciso.getEntidadContinuity().getIncisoSeccionContinuities().getInProcess(validoEn).iterator().next();	
			} else if (inciso.getEntidadContinuity().getIncisoSeccionContinuities().get(validoEn) != null
					&& inciso.getEntidadContinuity().getIncisoSeccionContinuities().get(validoEn).iterator().hasNext()) {
				seccionInciso = inciso.getEntidadContinuity().getIncisoSeccionContinuities().get(validoEn).iterator().next();
			}
						
			if (seccionInciso != null && inciso.getEntidadContinuity().getIncisoSeccionContinuities().get(validoEn) != null
					&& inciso.getEntidadContinuity().getIncisoSeccionContinuities().get(validoEn).iterator().hasNext()) {
				//Se da rollback a todos los cambios en borrador de las coberturas
				entidadContinuityDao.rollBackCoberturas(seccionInciso.getEntidadContinuity().getId());
				
				Long seccionIncisoContinuityId = seccionInciso.getEntidadContinuity().getId();
				seccionInciso = entidadBitemporalService.getInProcessByKey(SeccionIncisoContinuity.class, seccionIncisoContinuityId, validoEn);
				
				if (seccionInciso == null) {
					seccionInciso = entidadBitemporalService.getByKey(SeccionIncisoContinuity.class, seccionIncisoContinuityId, validoEn);
				}			
				
				entidadContinuityDao.refresh(seccionInciso.getEntidadContinuity());
				
			}			
						
			coberturasOriginales = new ArrayList(entidadBitemporalService.findInProcessByParentKey(CoberturaSeccionContinuity.class, 
					CoberturaSeccionContinuity.PARENT_KEY_NAME, seccionInciso.getEntidadContinuity().getId(), validoEn));
			
//			if (coberturasOriginales == null || coberturasOriginales.size() == 0) {
//				coberturasOriginales = new ArrayList(entidadBitemporalService.findByParentKey(CoberturaSeccionContinuity.class, 
//						CoberturaSeccionContinuity.PARENT_KEY_NAME, seccionInciso.getEntidadContinuity().getId(), validoEn));
//			}
					
		}
		
		//Se agregan/actualizan los datos de riesgo de la seccion
		seccionInciso = cargaDatosRiesgoSeccion (datosRiesgo, seccionInciso);
		
		//Cambio datos riesgo
		boolean cambioDatosRiesgo = false;
		Map<Long, Boolean> map = new LinkedHashMap<Long, Boolean>();
		Collection<BitemporalDatoSeccion> lstDatoSeccionBi = entidadBitemporalService.findInProcessByParentKey(DatoSeccionContinuity.class, 
				DatoSeccionContinuity.PARENT_KEY_NAME, seccionInciso.getContinuity().getId(), validoEn);
		if(lstDatoSeccionBi != null && !lstDatoSeccionBi.isEmpty()){
			for (BitemporalDatoSeccion datoSeccion : lstDatoSeccionBi) {
				if (datoSeccion != null && datoSeccion.getRecordStatus().equals(RecordStatus.TO_BE_ADDED)) {
					//cambioDatosRiesgo = true;					
					map.put(datoSeccion.getContinuity().getCoberturaId(), true);
				}
			}
		}
		
		//Se agregan/actualizan coberturas
		for (BitemporalCoberturaSeccion cobertura : coberturas) {
			
			cobertura.getEntidadContinuity().setCoberturaDTO(
					entidadService.findById(CoberturaDTO.class, cobertura.getEntidadContinuity().getCoberturaDTO().getIdToCobertura()));
			
			if (coberturasOriginales.contains(cobertura.getEntidadContinuity().getCoberturaDTO())) {
				
				coberturaOriginal = coberturasOriginales.get(coberturasOriginales.indexOf(cobertura.getEntidadContinuity().getCoberturaDTO()));								
				if (!coberturaOriginal.getValue().equals(cobertura.getValue()) || cambioAutoInciso || map.containsKey(cobertura.getContinuity().getCoberturaDTO().getIdToCobertura().longValue())) {			
					
					if(((!coberturaOriginal.getValue().getValorSumaAsegurada().equals(cobertura.getValue().getValorSumaAsegurada()))
							&& (coberturaOriginal.getContinuity().getCoberturaDTO().getClaveFuenteSumaAsegurada().trim().equals("0")
								||	coberturaOriginal.getContinuity().getCoberturaDTO().getClaveFuenteSumaAsegurada().trim().equals("2")))
						|| (!coberturaOriginal.getValue().getClaveContrato().equals(cobertura.getValue().getClaveContrato())  
								&& cobertura.getValue().getClaveContrato().shortValue()==1) 
						|| cambioAutoInciso 
						|| map.containsKey(cobertura.getContinuity().getCoberturaDTO().getIdToCobertura().longValue()) 
						|| (coberturaOriginal.getValue().getDiasSalarioMinimo() != null && 
								!coberturaOriginal.getValue().getDiasSalarioMinimo().equals(cobertura.getValue().getDiasSalarioMinimo())) 
						|| (coberturaOriginal.getValue().getPorcentajeDeducible() != null && 
								!coberturaOriginal.getValue().getPorcentajeDeducible().equals(cobertura.getValue().getPorcentajeDeducible()))
						|| (coberturaOriginal.getValue().getValorDeducible() != null && 
								!coberturaOriginal.getValue().getValorDeducible().equals(cobertura.getValue().getValorDeducible()))								
					) {
						
							
						cobertura.setEntidadContinuity(entidadContinuityDao
								.findByKey(coberturaOriginal.getEntidadContinuity().getClass(), 
										coberturaOriginal.getEntidadContinuity().getKey()));
						
						entidadContinuityDao.refresh(cobertura.getEntidadContinuity());
						
						if (cobertura.getValue().getClaveContrato().shortValue() == 0) {
							cobertura.getValue().setValorPrimaNeta(new Double(0.0));
							cobertura.getValue().setValorPrimaDiaria(coberturaOriginal.getValue().getValorPrimaDiaria());
						}
						
						cobertura = entidadBitemporalService.saveInProcess(cobertura, validoEn);
						
						
					}else if(!coberturaOriginal.getValue().getClaveContrato().equals(cobertura.getValue().getClaveContrato())){
						cobertura.setEntidadContinuity(entidadContinuityDao
								.findByKey(coberturaOriginal.getEntidadContinuity().getClass(), 
										coberturaOriginal.getEntidadContinuity().getKey()));
						
						entidadContinuityDao.refresh(cobertura.getEntidadContinuity());
						
						if (cobertura.getValue().getClaveContrato().shortValue() == 0) {
							cobertura.getValue().setValorPrimaNeta(new Double(0.0));
							cobertura.getValue().setValorPrimaDiaria(coberturaOriginal.getValue().getValorPrimaDiaria());
						}
						cobertura = entidadBitemporalService.saveInProcess(cobertura, validoEn);
					}
				}
			} else {
				
				if (!nuevoInciso) {
					CoberturaDTO coberturaDto = cobertura.getContinuity().getCoberturaDTO();
					cobertura.setEntidadContinuity(new CoberturaSeccionContinuity());
					cobertura.getEntidadContinuity().setCoberturaDTO(coberturaDto);
				}	
				CoberturaSeccionContinuity coberturaSeccionContinuity = (CoberturaSeccionContinuity)
				entidadBitemporalService.attachToParentAndSet(cobertura, seccionInciso, validoEn);
				
				if (!nuevoInciso) {					
					cobertura = entidadBitemporalService.saveInProcess(cobertura, validoEn);
				}
				seccionInciso.getEntidadContinuity().getCoberturaSeccionContinuities().getValue().add(coberturaSeccionContinuity);				
				
			}
			
		}
		
		//Se eliminan las coberturas que ya no son requeridas
		for (BitemporalCoberturaSeccion cobertura : coberturasOriginales) {
			
			if (cobertura != null && !coberturas.contains(cobertura.getEntidadContinuity().getCoberturaDTO())) {
				entidadBitemporalService.remove(cobertura, validoEn);
			}
			
		}
		
		
		//TODO Se inserta el ARD (Implementar cuando se defina la estrategia para lograr esto)

		//Se guarda el inciso con toda su estructura para que pueda calcularse
		if (nuevoInciso) {
			Long key = (Long)entidadContinuityDao.persistAndReturnKey(inciso.getEntidadContinuity());
			inciso.setEntidadContinuity(entidadContinuityDao.getReference(inciso.getEntidadContinuity().getClass(), key));
			
		} else {
			//Se generaran nuevos bitemporales de inciso y seccionInciso para que ahi se acumulen la prima neta de las coberturas
			
			//seccionInciso = entidadBitemporalService.set(seccionInciso, validoEn,true);
			inciso.getEntidadContinuity().getIncisoSeccionContinuities().getValue().clear();
			inciso.getEntidadContinuity().getIncisoSeccionContinuities().getValue().add(
					(SeccionIncisoContinuity)entidadBitemporalService.attachToParentAndSet(seccionInciso, inciso, validoEn));
			
			
			inciso = entidadBitemporalService.set(inciso, validoEn, true);
			inciso.setEntidadContinuity(entidadContinuityDao.update(inciso.getEntidadContinuity()));
		}
		//Regresa el inciso creado
		return inciso;
	}

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public BitemporalInciso copiarInciso(Long incisoId, DateTime validoEn) {
		
		BitemporalInciso incisoOriginal = entidadBitemporalService.getInProcessByKey(IncisoContinuity.class, incisoId, validoEn);
		BitemporalInciso incisoNuevo = entidadBitemporalService.getNew(BitemporalInciso.class, null);
		
		Inciso inciso = new Inciso();
		BeanUtils.copyProperties(incisoOriginal.getValue(), inciso);
		
		incisoNuevo.setValue(inciso);

		incisoNuevo.getEntidadContinuity().setNumero(getNuevoNumeroInciso(incisoOriginal.getEntidadContinuity().getParentContinuity().getId(), validoEn));		
	
		incisoNuevo.getValue().setNumeroSecuencia(getNuevoNumeroSecuencia(incisoOriginal.getEntidadContinuity().getParentContinuity().getId(), validoEn));				
		
		BitemporalCotizacion cotizacion = incisoOriginal.getEntidadContinuity().getParentContinuity().getBitemporalProperty().get(validoEn);			
		entidadBitemporalService.attachToParentAndSet(incisoNuevo, cotizacion, validoEn);
		
		BitemporalAutoInciso autoIncisoOriginal = entidadBitemporalService.getInProcessByKey(AutoIncisoContinuity.class, incisoOriginal.getContinuity().getAutoIncisoContinuity().getKey(), validoEn);
		BitemporalAutoInciso autoIncisoNuevo = entidadBitemporalService.getNew(BitemporalAutoInciso.class, null);
		autoIncisoNuevo.setValue(autoIncisoOriginal.getValue());
		
		incisoNuevo.getEntidadContinuity().setAutoIncisoContinuity(
				(AutoIncisoContinuity)entidadBitemporalService.attachToParentAndSet(autoIncisoNuevo, incisoNuevo, validoEn));
		
		BitemporalSeccionInciso seccionIncisoOriginal = entidadBitemporalService.getInProcessByKey(SeccionIncisoContinuity.class, incisoOriginal.getContinuity().getIncisoSeccionContinuities().getInProcess(validoEn).iterator().next().getContinuity().getId(), validoEn);
		BitemporalSeccionInciso seccionIncisoNueva = entidadBitemporalService.getNew(BitemporalSeccionInciso.class, null);		
		seccionIncisoNueva.setValue(seccionIncisoOriginal.getValue());
		seccionIncisoNueva.getContinuity().setSeccion(seccionIncisoOriginal.getContinuity().getSeccion());
		
		incisoNuevo.getEntidadContinuity().getIncisoSeccionContinuities().getValue().add(
				(SeccionIncisoContinuity)entidadBitemporalService.attachToParentAndSet(seccionIncisoNueva, incisoNuevo, validoEn));
		
		List<BitemporalCoberturaSeccion> coberturasOriginales = new ArrayList(entidadBitemporalService.findInProcessByParentKey(CoberturaSeccionContinuity.class, 
				CoberturaSeccionContinuity.PARENT_KEY_NAME, seccionIncisoOriginal.getEntidadContinuity().getId(), validoEn));	
		
		Iterator<BitemporalCoberturaSeccion> itCoberturasOriginales = coberturasOriginales.iterator();
		while(itCoberturasOriginales.hasNext())
		{
			BitemporalCoberturaSeccion coberturaSeccionOriginal = itCoberturasOriginales.next();
			BitemporalCoberturaSeccion coberturaSeccionNueva = entidadBitemporalService.getNew(BitemporalCoberturaSeccion.class, null);
			coberturaSeccionNueva.setValue(coberturaSeccionOriginal.getValue());	
			coberturaSeccionNueva.getContinuity().setCoberturaDTO(coberturaSeccionOriginal.getContinuity().getCoberturaDTO());
			
			seccionIncisoNueva.getEntidadContinuity().getCoberturaSeccionContinuities().getValue().add(
					(CoberturaSeccionContinuity)entidadBitemporalService.attachToParentAndSet(coberturaSeccionNueva, seccionIncisoNueva, validoEn));
		}
		
		//Datos riesgo
		List<BitemporalDatoSeccion> datoSeccionOriginales = new ArrayList(entidadBitemporalService.findInProcessByParentKey(DatoSeccionContinuity.class, 
				DatoSeccionContinuity.PARENT_KEY_NAME, seccionIncisoOriginal.getEntidadContinuity().getId(), validoEn));
		
		Iterator<BitemporalDatoSeccion> itDatosSeccOriginales = datoSeccionOriginales.iterator();
		while(itDatosSeccOriginales.hasNext())
		{
			BitemporalDatoSeccion datoSeccionOriginal = itDatosSeccOriginales.next();
			BitemporalDatoSeccion datoSeccionNueva = entidadBitemporalService.getNew(BitemporalDatoSeccion.class, null);
			datoSeccionNueva.setValue(datoSeccionOriginal.getValue());	
			datoSeccionNueva.getContinuity().setClaveDetalle(datoSeccionOriginal.getContinuity().getClaveDetalle());
			datoSeccionNueva.getContinuity().setCoberturaId(datoSeccionOriginal.getContinuity().getCoberturaId());
			datoSeccionNueva.getContinuity().setDatoId(datoSeccionOriginal.getContinuity().getDatoId());
			datoSeccionNueva.getContinuity().setRamoId(datoSeccionOriginal.getContinuity().getRamoId());
			datoSeccionNueva.getContinuity().setSubRamoId(datoSeccionOriginal.getContinuity().getSubRamoId());
			
			seccionIncisoNueva.getEntidadContinuity().getDatoSeccionContinuities2().getValue().add(
					(DatoSeccionContinuity)entidadBitemporalService.attachToParentAndSet(datoSeccionNueva, seccionIncisoNueva, validoEn));
		}		

		
		Long key = (Long)entidadContinuityDao.persistAndReturnKey(incisoNuevo.getEntidadContinuity());
		incisoNuevo.setEntidadContinuity(entidadContinuityDao.getReference(incisoNuevo.getEntidadContinuity().getClass(), key));
		
		return incisoNuevo;
	}
	
	
	private BitemporalSeccionInciso cargaDatosRiesgoSeccion(Map<String, String> datosRiesgo, BitemporalSeccionInciso seccionInciso) {
		
		if (datosRiesgo != null) {
			String key = null;
			String value = null;
			DatoSeccionContinuity filtro = null;
			DatoSeccionContinuity datoSeccionContinuity = null;
			BitemporalDatoSeccion datoSeccion = null;
			
			List<DatoSeccionContinuity> datoSeccionContinuities = (List<DatoSeccionContinuity>)seccionInciso.getEntidadContinuity()
				.getDatoSeccionContinuities();
			
			DateTime validoEn = seccionInciso.getValidityInterval().getInterval().getStart();

			for (Entry<String, String> entry : datosRiesgo.entrySet()) {
				key = entry.getKey();
				value = entry.getValue();
				filtro = llenaDatoSeccionContinuityBusqueda(key);

				if (datoSeccionContinuities != null && datoSeccionContinuities.contains(filtro)) {
					datoSeccionContinuity =  datoSeccionContinuities.get(datoSeccionContinuities.indexOf(filtro));
					datoSeccion = datoSeccionContinuity.getBitemporalProperty().getInProcess(validoEn);
					
					if (!datoSeccion.getValue().getValor().equals(value)) {
						datoSeccion.getValue().setValor(value);
						entidadBitemporalService.set(datoSeccion, validoEn, true);
					}
					
				} else {
					datoSeccion = entidadBitemporalService.getNew(BitemporalDatoSeccion.class, null);
					datoSeccion.setEntidadContinuity(filtro);
					datoSeccion.getValue().setValor(value);
					seccionInciso.getEntidadContinuity().getDatoSeccionContinuities()
						.add((DatoSeccionContinuity)entidadBitemporalService.attachToParentAndSet(datoSeccion, seccionInciso, validoEn));
				}
				
			}
		}
		
		return seccionInciso;
		
	}
	
	
	private DatoSeccionContinuity llenaDatoSeccionContinuityBusqueda (String idDatoRiesgo) throws IllegalArgumentException{
		
		String del = "_";
		String[] idTokens = idDatoRiesgo.split(del);
		DatoSeccionContinuity datoSeccionContinuityBusqueda = new DatoSeccionContinuity();
		
		if (idTokens.length >= 5) {
			
			Long ramoId = new Long(idTokens[0]);
			Long subRamoId = new Long(idTokens[1]);
			Long coberturaId = new Long(idTokens[2]);
			Long claveDetalle = new Long(idTokens[3]);
			Long datoId = new Long(idTokens[4]);
			
			datoSeccionContinuityBusqueda.setRamoId(ramoId);
			datoSeccionContinuityBusqueda.setSubRamoId(subRamoId);
			datoSeccionContinuityBusqueda.setCoberturaId(coberturaId);
			datoSeccionContinuityBusqueda.setClaveDetalle(claveDetalle);
			datoSeccionContinuityBusqueda.setDatoId(datoId);
			
		} else {
			throw new IllegalArgumentException("Los datos recibidos no son una referencia válida a la configuración de datos de riesgo.");
		}
		
		return datoSeccionContinuityBusqueda;
	}
	
	
	private Integer getNuevoNumeroInciso(Long idCotizacion, DateTime validoEn){
		return incisoDao.getMaxIncisos(idCotizacion, validoEn) + 1;
	}
	
	private Integer getNuevoNumeroSecuencia(Long idCotizacion, DateTime validoEn){
		return incisoDao.getMaxNumeroSecuencia(idCotizacion, validoEn) + 1;
	}
	
	@EJB
	private EntidadBitemporalService entidadBitemporalService;
	
	@EJB
	private IncisoDao incisoDao;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private EntidadContinuityDao entidadContinuityDao;

}
