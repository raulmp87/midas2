package mx.com.afirme.midas2.service.impl.sapamis.otros;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;

/*******************************************************************************
 * Nombre Interface: 	RelSistemaOperacionServiceImpl.
 * 
 * Descripcion: 		Contiene la logica de servicios Utiles que se consumen
 * 						a lo largo de la ejecución de los Procesos.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class SapAmisUtilsServiceImpl implements SapAmisUtilsService{
	private static final long serialVersionUID = 1L;
	
	private static final int CODIGO_PARAMETRO_GENERAL = 136060;
	private static final int ID_GPO_PARAMETRO_GENERAL = 13;

	@EJB private static ParametroGeneralFacadeRemote parametroGeneralFacade;

	@Override
	public Date convertToDateStr(String fecha){
	    SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		Date retorno = new Date();
		try {
			retorno = sm.parse(fecha);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return retorno;
	}

	@Override
	public String convertToStrDate(Date fecha){
	    Calendar fechaInicioCalendar = Calendar.getInstance();
	    fechaInicioCalendar.setTime(fecha);
	    String mes =String.valueOf(fechaInicioCalendar.get(Calendar.MONTH) + 1);
	    String dia =String.valueOf(fechaInicioCalendar.get(Calendar.DAY_OF_MONTH));
	    mes = mes.length()<2?"0"+mes:mes;
	    dia = dia.length()<2?"0"+dia:dia;		
	    return fechaInicioCalendar.get(Calendar.YEAR) + "/" + mes + "/" + dia;
	}

	@Override
	public String[] obtenerAccesos(){
		ParametroGeneralId parametroGralId = new ParametroGeneralId();
		parametroGralId.setIdToGrupoParametroGeneral(new BigDecimal(ID_GPO_PARAMETRO_GENERAL));
		parametroGralId.setCodigoParametroGeneral(new BigDecimal(CODIGO_PARAMETRO_GENERAL));
		ParametroGeneralDTO parametroGeneral = parametroGeneralFacade.findById(parametroGralId);
		return parametroGeneral.getValor().split("\\s");
	}
}