package mx.com.afirme.midas2.service.impl.tarifa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda.MonedaTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda.MonedaTipoPolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.AgrupadorTarifaSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.NegocioSeccionDao;
import mx.com.afirme.midas2.dao.tarifa.AgrupadorTarifaDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifa;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.tarifa.AgrupadorTarifaSeccionService;

@Stateless
public class AgrupadorTarifaSeccionServiceImpl implements
		AgrupadorTarifaSeccionService {

	protected AgrupadorTarifaSeccionDao agrupadorTarifaSeccionDao;
	protected SeccionFacadeRemote seccionFacadeRemote;
	protected MonedaTipoPolizaFacadeRemote monedaTipoPolizaFacadeRemote;
	protected NegocioSeccionDao negocioSeccionDao;
	protected AgrupadorTarifaDao agrupadorTarifaDao;
	
	protected EntidadService entidadService;

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB		
	public void setAgrupadorTarifaDao(AgrupadorTarifaDao agrupadorTarifaDao) {
		this.agrupadorTarifaDao = agrupadorTarifaDao;
	}

	@EJB	
	public void setNegocioSeccionDao(NegocioSeccionDao negocioSeccionDao) {
		this.negocioSeccionDao = negocioSeccionDao;
	}

	@EJB
	public void setMonedaTipoPolizaFacadeRemote(
			MonedaTipoPolizaFacadeRemote monedaTipoPolizaFacadeRemote) {
		this.monedaTipoPolizaFacadeRemote = monedaTipoPolizaFacadeRemote;
	}

	@EJB
	public void setSeccionFacadeRemote(SeccionFacadeRemote seccionFacadeRemote) {
		this.seccionFacadeRemote = seccionFacadeRemote;
	}

	@EJB
	public void setAgrupadorTarifaSeccionDao(AgrupadorTarifaSeccionDao agrupadorTarifaSeccionDao){
		this.agrupadorTarifaSeccionDao = agrupadorTarifaSeccionDao;
	}
	
	@Override
	public List<AgrupadorTarifaSeccion> findByFilters(
			AgrupadorTarifaSeccion filter) {
		return agrupadorTarifaSeccionDao.findByFilters(filter);
	}
	
	public List<AgrupadorTarifaSeccion> findAll(){
		List<AgrupadorTarifaSeccion> lista = agrupadorTarifaSeccionDao.findAll(AgrupadorTarifaSeccion.class);
		
		return this.fillAgrupador(lista);
	}
	
	public List<AgrupadorTarifaSeccion> findByBusiness(String claveNegocio, AgrupadorTarifaSeccion filter){
		List<AgrupadorTarifaSeccion> lista = agrupadorTarifaSeccionDao.findByBusiness(claveNegocio, filter);
		
		return this.fillAgrupador(lista);
	}

	@Override
	public AgrupadorTarifaSeccion save(AgrupadorTarifaSeccion entity) {
		//TODO: Recuperar la lista de relaciones y asignar claveDefault = 0 si claveDefault de entity es 1
		if(entity.getClaveDefault() == 1){
			AgrupadorTarifaSeccion tempAgrupadorTarifaSeccion = new AgrupadorTarifaSeccion();
			tempAgrupadorTarifaSeccion.getId().setIdMoneda(entity.getId().getIdMoneda());
			tempAgrupadorTarifaSeccion.getId().setIdToSeccion(entity.getId().getIdToSeccion());
			List<AgrupadorTarifaSeccion> list = agrupadorTarifaSeccionDao.findBySeccionMoneda(entity);
			for(AgrupadorTarifaSeccion modif: list){
				modif.setClaveDefault(new Short("0"));
				agrupadorTarifaSeccionDao.update(modif);
			}
		}
		if(entity.getId() != null){
			agrupadorTarifaSeccionDao.update(entity);
		}else{
			agrupadorTarifaSeccionDao.persist(entity);
		}
		return agrupadorTarifaSeccionDao.getReference(entity.getClass(), entity.getKey());		
	}
	
	private List<AgrupadorTarifaSeccion> fillAgrupador(List<AgrupadorTarifaSeccion> toFill){
		List<AgrupadorTarifaSeccion> listaCompleta = new ArrayList<AgrupadorTarifaSeccion>();
		if(toFill!=null){
			for(AgrupadorTarifaSeccion relacion: toFill){
				StringBuilder query = new StringBuilder();
				query.append("SELECT agrupador FROM AgrupadorTarifa agrupador ")
				   .append("WHERE agrupador.id.idToAgrupadorTarifa = :idToAgrupadorTarifa ")
				   .append("ORDER BY agrupador.id.idVerAgrupadorTarifa DESC");
				
				HashMap<String, Object> params = new HashMap<String, Object>();
				
				params.put("idToAgrupadorTarifa", relacion.getId().getIdToAgrupadorTarifa());
				
				@SuppressWarnings({ "unchecked", "rawtypes" })
				List<AgrupadorTarifa> agrupador = (List)agrupadorTarifaSeccionDao.executeQueryMultipleResult(query.toString(), params);
				if(!agrupador.isEmpty())
					relacion.setAgrupadorTarifa(agrupador.get(0));
				listaCompleta.add(relacion);
			}
		}
		return listaCompleta;
	}

	@Override
	public List<AgrupadorTarifaSeccion> consultaAgrupadoresDefault(
			BigDecimal idSeccion, String claveNegocio) {
		SeccionDTO seccionDTO = entidadService.findById(SeccionDTO.class, idSeccion);
		final String query = "SELECT distinct agrupador FROM AgrupadorTarifaSeccion agrupador, AgrupadorTarifa agTarifa " +
		   					 "WHERE agrupador.id.idToSeccion = :idToSeccion  " +
		   					 "AND agrupador.id.idMoneda = :idMoneda " +
		   					"AND agrupador.claveDefault = 1 " +
		   					"AND agTarifa.id.idToAgrupadorTarifa = agrupador.id.idToAgrupadorTarifa " +
		   					"AND agTarifa.claveNegocio = :claveNegocio ";
		List<AgrupadorTarifaSeccion> agrupadorTarifaSeccions = new ArrayList<AgrupadorTarifaSeccion>();
		if(seccionDTO != null){
			List<MonedaTipoPolizaDTO> monedasPorPoliza;			
			if(seccionDTO.getTipoPolizaDTO().getMonedas()!= null && seccionDTO.getTipoPolizaDTO().getMonedas().size()>0){
				monedasPorPoliza = seccionDTO.getTipoPolizaDTO().getMonedas();
			}else{
				monedasPorPoliza = monedaTipoPolizaFacadeRemote.findByProperty("id.idToTipoPoliza", seccionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
			}
			if(monedasPorPoliza != null){
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("idToSeccion", seccionDTO.getIdToSeccion());
				params.put("claveNegocio", claveNegocio);
				for(MonedaTipoPolizaDTO monedaTipoPolizaDTO: monedasPorPoliza){
					params.put("idMoneda", monedaTipoPolizaDTO.getId().getIdMoneda());
					AgrupadorTarifaSeccion agrupadorTarifaSeccion = (AgrupadorTarifaSeccion)agrupadorTarifaSeccionDao.executeQuerySimpleResult(query, params);
					if(agrupadorTarifaSeccion != null)
						agrupadorTarifaSeccions.add(agrupadorTarifaSeccion);
				}
			}
		}
		return agrupadorTarifaSeccions;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AgrupadorTarifaSeccion> consultaAgrupadoresPosibles(
			NegocioSeccion negocioSeccion, String idMonedas) {
		final StringBuilder query = new StringBuilder("SELECT DISTINCT agrupadorSeccion.monedaDTO,agrupador.descripcionAgrupador,agrupador.descripcionVersion FROM AgrupadorTarifaSeccion agrupadorSeccion, AgrupadorTarifa  agrupador "); 
		query.append("WHERE agrupadorSeccion.id.idToSeccion = :idToSeccion ");
		query.append("AND agrupadorSeccion.id.idToAgrupadorTarifa = agrupador.id.idToAgrupadorTarifa ");
		query.append("AND agrupadorSeccion.claveDefault = 1 ");
		query.append("AND agrupador.claveEstatus IN (1,2) ");
		query.append("AND agrupadorSeccion.id.idMoneda IN (" + idMonedas + ")");
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> agrupadores = null;
		List<AgrupadorTarifaSeccion> list = new LinkedList<AgrupadorTarifaSeccion>();
		if (negocioSeccion.getIdToNegSeccion() != null){
			negocioSeccion = negocioSeccionDao.getReference(negocioSeccion.getIdToNegSeccion());
			params.put("idToSeccion", negocioSeccion.getSeccionDTO().getIdToSeccion());
			agrupadores = agrupadorTarifaSeccionDao.executeQueryMultipleResult(query.toString(), params);
		}	
		Object[] array = new Object[3];
		for(Object item : agrupadores) {
			 array = (Object[])item;
			 AgrupadorTarifaSeccion agrupadorTarifaSeccion = new AgrupadorTarifaSeccion();
			 agrupadorTarifaSeccion.setMonedaDTO((MonedaDTO)array[0]);
			 agrupadorTarifaSeccion.setSeccionDTO(negocioSeccion.getSeccionDTO());
			 agrupadorTarifaSeccion.setAgrupadorTarifa(new AgrupadorTarifa());
			 agrupadorTarifaSeccion.getAgrupadorTarifa().setDescripcionAgrupador((String)array[1]);
			 agrupadorTarifaSeccion.getAgrupadorTarifa().setDescripcionVersion((String)array[2]);
			 list.add(agrupadorTarifaSeccion);
		}
		return list;
	}
	
	/**
	 * Obtiene los agrupadores posibles y las versiones disponibles para cada agrupador
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<AgrupadorTarifaSeccion> consultaAgrupadoresPosiblesConVersiones(
			NegocioSeccion negocioSeccion, BigDecimal idMoneda, String claveNegocio) {
			final String query = "SELECT DISTINCT agrupadorSeccion FROM AgrupadorTarifaSeccion agrupadorSeccion, AgrupadorTarifa  agrupador " +
				 "WHERE agrupadorSeccion.id.idToSeccion = :idToSeccion  " +
				 "AND agrupadorSeccion.id.idMoneda = :idMoneda " +
				 "AND agrupadorSeccion.id.idToAgrupadorTarifa = agrupador.id.idToAgrupadorTarifa " +
				 "AND agrupador.claveEstatus IN (1,2) " +
				 "AND agrupador.claveNegocio = :claveNegocio ";
			Map<String, Object> params = new HashMap<String, Object>();
			
			List<AgrupadorTarifaSeccion> agrupadores = null;
			if (negocioSeccion.getIdToNegSeccion() != null){
				negocioSeccion = negocioSeccionDao.getReference(negocioSeccion.getIdToNegSeccion());
				params.put("idToSeccion", negocioSeccion.getSeccionDTO().getIdToSeccion());
				params.put("idMoneda", idMoneda);
				params.put("claveNegocio", claveNegocio);
				agrupadores = agrupadorTarifaSeccionDao.executeQueryMultipleResult(query, params);

				for (AgrupadorTarifaSeccion agrupador : agrupadores) {
					if(agrupador.getId() != null){				
						agrupador.setAgrupadorTarifa(agrupadorTarifaDao
								.consultaVersionActiva(agrupador.getId().getIdToAgrupadorTarifa()));
						
						List<AgrupadorTarifa> list = agrupadorTarifaDao
						.findByProperty(
								AgrupadorTarifa.class,
								"id.idToAgrupadorTarifa",
								agrupador.getId().getIdToAgrupadorTarifa());
						
						//Sort
						if(list != null && !list.isEmpty()){
							Collections.sort(list, 
									new Comparator<AgrupadorTarifa>() {				
										public int compare(AgrupadorTarifa n1, AgrupadorTarifa n2){
											return n2.getId().getIdVerAgrupadorTarifa().compareTo(n1.getId().getIdVerAgrupadorTarifa());
										}
									});
						}
						agrupador.setAgrupadorVersion(list);
					}
				}
			}	
		return agrupadores;
	}

}
