package mx.com.afirme.midas2.service.impl.siniestros.notificaciones;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.TareaNotificarRecurrentesService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.log4j.Logger;

@Stateless
public class TareaNotificarRecurrentesServiceImpl implements TareaNotificarRecurrentesService {
	
	private static final Logger LOG = Logger.getLogger(TareaNotificarRecurrentesServiceImpl.class);
	
	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private EnvioNotificacionesService envioNotificacionesService;
	
	@Resource	
	private TimerService timerService;
	
	public void initialize() {
		String timerInfo = "TimerNotificarRecurrentes";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.minute(0);
				expression.hour("0,12");
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerNotificarRecurrentes", false));
				
				LOG.info("Tarea TimerNotificarRecurrentes configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerNotificarRecurrentes");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerNotificarRecurrentes:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		envioNotificacionesService.notificarRecurrentes();
	}

}
