package mx.com.afirme.midas2.service.impl.seguridad;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.asm.dto.ApplicationDTO;
import com.asm.dto.CreatePasswordUserParameterDTO;
import com.asm.dto.CreatePortalUserParameterDTO;
import com.asm.dto.PageConsentDTO;
import com.asm.dto.ResendConfirmationEmailDTO;
import com.asm.dto.RoleDTO;
import com.asm.dto.SearchUserParametersDTO;
import com.asm.dto.SendResetPasswordTokenDTO;
import com.asm.dto.UserDTO;
import com.asm.dto.UserInboxEntryDTO;
import com.asm.dto.UserUpdateDTO;
import com.js.service.SystemException;

import mx.com.afirme.midas.sistema.seguridad.Menu;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.movil.ajustador.AjustadorMovil;
import mx.com.afirme.midas2.domain.sistema.seguridad.SeguridadContext;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.impl.seguridad.filler.actuario.MenuActuario;
import mx.com.afirme.midas2.service.impl.seguridad.filler.actuario.PaginaPermisoActuario;
import mx.com.afirme.midas2.service.impl.seguridad.filler.administradorcolonias.MenuAdministradorColonias;
import mx.com.afirme.midas2.service.impl.seguridad.filler.administradorcolonias.PaginaPermisoAdministradorColonias;
import mx.com.afirme.midas2.service.impl.seguridad.filler.administradorproductos.MenuAdministradorProductos;
import mx.com.afirme.midas2.service.impl.seguridad.filler.administradorproductos.PaginaPermisoAdministradorProductos;
import mx.com.afirme.midas2.service.impl.seguridad.filler.agente.MenuAgente;
import mx.com.afirme.midas2.service.impl.seguridad.filler.agente.PaginaPermisoAgente;
import mx.com.afirme.midas2.service.impl.seguridad.filler.ajustador.MenuAjustador;
import mx.com.afirme.midas2.service.impl.seguridad.filler.ajustador.PaginaPermisoAjustador;
import mx.com.afirme.midas2.service.impl.seguridad.filler.analistaadministrativo.MenuAnalistaAdministrativo;
import mx.com.afirme.midas2.service.impl.seguridad.filler.analistaadministrativo.PaginaPermisoAnalistaAdministrativo;
import mx.com.afirme.midas2.service.impl.seguridad.filler.analistaadministrativofacultativo.MenuAnalistaAdministrativoFacultativo;
import mx.com.afirme.midas2.service.impl.seguridad.filler.analistaadministrativofacultativo.PaginaPermisoAnalistaAdministrativoFacultativo;
import mx.com.afirme.midas2.service.impl.seguridad.filler.asignadorordentrabajo.MenuAsignadorOrdenTrabajo;
import mx.com.afirme.midas2.service.impl.seguridad.filler.asignadorordentrabajo.PaginaPermisoAsignadorOrdenTrabajo;
import mx.com.afirme.midas2.service.impl.seguridad.filler.asignadorsolicitud.MenuAsignadorSolicitud;
import mx.com.afirme.midas2.service.impl.seguridad.filler.asignadorsolicitud.PaginaPermisoAsignadorSolicitud;
import mx.com.afirme.midas2.service.impl.seguridad.filler.cabinero.MenuCabinero;
import mx.com.afirme.midas2.service.impl.seguridad.filler.cabinero.PaginaPermisoCabinero;
import mx.com.afirme.midas2.service.impl.seguridad.filler.consultadanios.MenuConsultaDanios;
import mx.com.afirme.midas2.service.impl.seguridad.filler.consultadanios.PaginaPermisoConsultaDanios;
import mx.com.afirme.midas2.service.impl.seguridad.filler.consultareaseguro.MenuConsultaReaseguro;
import mx.com.afirme.midas2.service.impl.seguridad.filler.consultareaseguro.PaginaPermisoConsultaReaseguro;
import mx.com.afirme.midas2.service.impl.seguridad.filler.consultasiniestros.MenuConsultaSiniestros;
import mx.com.afirme.midas2.service.impl.seguridad.filler.consultasiniestros.PaginaPermisoConsultaSiniestros;
import mx.com.afirme.midas2.service.impl.seguridad.filler.coordinadoradministrativoreaseguro.MenuCoordinadorAdministrativoReaseguro;
import mx.com.afirme.midas2.service.impl.seguridad.filler.coordinadoradministrativoreaseguro.PaginaCoordinadorAdministrativoReaseguro;
import mx.com.afirme.midas2.service.impl.seguridad.filler.coordinadoremision.MenuCoordinadorEmision;
import mx.com.afirme.midas2.service.impl.seguridad.filler.coordinadoremision.PaginaPermisoCoordinadorEmision;
import mx.com.afirme.midas2.service.impl.seguridad.filler.coordinadorsiniestrofacultativo.MenuCoordinadorSiniestrosFacultativo;
import mx.com.afirme.midas2.service.impl.seguridad.filler.coordinadorsiniestrofacultativo.PaginaPermisoCoordinadorSiniestrosFacultativo;
import mx.com.afirme.midas2.service.impl.seguridad.filler.coordinadorsiniestros.MenuCoordinadorSiniestros;
import mx.com.afirme.midas2.service.impl.seguridad.filler.coordinadorsiniestros.PaginaPermisoCoordinadorSiniestros;
import mx.com.afirme.midas2.service.impl.seguridad.filler.directorreaseguro.MenuDirectorReaseguro;
import mx.com.afirme.midas2.service.impl.seguridad.filler.directorreaseguro.PaginaPermisoDirectorReaseguro;
import mx.com.afirme.midas2.service.impl.seguridad.filler.directortecnico.MenuDirectorTecnico;
import mx.com.afirme.midas2.service.impl.seguridad.filler.directortecnico.PaginaPermisoDirectorTecnico;
import mx.com.afirme.midas2.service.impl.seguridad.filler.emisor.MenuEmisor;
import mx.com.afirme.midas2.service.impl.seguridad.filler.emisor.PaginaPermisoEmisor;
import mx.com.afirme.midas2.service.impl.seguridad.filler.gerentesiniestrofacultativo.MenuGerenteSiniestrosFacultativo;
import mx.com.afirme.midas2.service.impl.seguridad.filler.gerentesiniestrofacultativo.PaginaPermisoGerenteSiniestrosFacultativo;
import mx.com.afirme.midas2.service.impl.seguridad.filler.gerentesiniestros.MenuGerenteSiniestros;
import mx.com.afirme.midas2.service.impl.seguridad.filler.gerentesiniestros.PaginaPermisoGerenteSiniestros;
import mx.com.afirme.midas2.service.impl.seguridad.filler.mesacontrol.MenuMesaControl;
import mx.com.afirme.midas2.service.impl.seguridad.filler.mesacontrol.PaginaPermisoMesaControl;
import mx.com.afirme.midas2.service.impl.seguridad.filler.opasistentesubdirectorreaseguro.MenuOpAsistenteSubdirectorReaseguro;
import mx.com.afirme.midas2.service.impl.seguridad.filler.opasistentesubdirectorreaseguro.PaginaPermisoOpAsistenteSubdirectorReaseguro;
import mx.com.afirme.midas2.service.impl.seguridad.filler.oppagoscobrosreaseguradores.MenuOpPagosCobrosReaseguradores;
import mx.com.afirme.midas2.service.impl.seguridad.filler.oppagoscobrosreaseguradores.PaginaPermisoOpPagosCobrosReaseguradores;
import mx.com.afirme.midas2.service.impl.seguridad.filler.opreaseguroautomatico.MenuOpReaseguroAutomatico;
import mx.com.afirme.midas2.service.impl.seguridad.filler.opreaseguroautomatico.PaginaPermisoOpReaseguroAutomatico;
import mx.com.afirme.midas2.service.impl.seguridad.filler.opreasegurofacultativo.MenuOpReaseguroFacultativo;
import mx.com.afirme.midas2.service.impl.seguridad.filler.opreasegurofacultativo.PaginaPermisoOpReaseguroFacultativo;
import mx.com.afirme.midas2.service.impl.seguridad.filler.reportesdanios.MenuReportesDanios;
import mx.com.afirme.midas2.service.impl.seguridad.filler.reportesdanios.PaginaPermisoReportesDanios;
import mx.com.afirme.midas2.service.impl.seguridad.filler.reportesreaseguro.MenuReportesReaseguro;
import mx.com.afirme.midas2.service.impl.seguridad.filler.reportesreaseguro.PaginaPermisoReportesReaseguro;
import mx.com.afirme.midas2.service.impl.seguridad.filler.reportessiniestros.MenuReportesSiniestros;
import mx.com.afirme.midas2.service.impl.seguridad.filler.reportessiniestros.PaginaPermisoReportesSiniestros;
import mx.com.afirme.midas2.service.impl.seguridad.filler.sistema.MenuSistema;
import mx.com.afirme.midas2.service.impl.seguridad.filler.sistema.PaginaPermisoSistema;
import mx.com.afirme.midas2.service.impl.seguridad.filler.subdirectorreaseguro.MenuSubdirectorReaseguro;
import mx.com.afirme.midas2.service.impl.seguridad.filler.subdirectorreaseguro.PaginaPermisoSubdirectorReaseguro;
import mx.com.afirme.midas2.service.impl.seguridad.filler.supervisorsuscriptor.MenuSupervisorSuscriptor;
import mx.com.afirme.midas2.service.impl.seguridad.filler.supervisorsuscriptor.PaginaPermisoSupervisorSuscriptor;
import mx.com.afirme.midas2.service.impl.seguridad.filler.suscriptorcotizacion.MenuSuscriptorCotizacion;
import mx.com.afirme.midas2.service.impl.seguridad.filler.suscriptorcotizacion.PaginaPermisoSuscriptorCotizacion;
import mx.com.afirme.midas2.service.impl.seguridad.filler.suscriptorordentrabajo.MenuSuscriptorOrdenTrabajo;
import mx.com.afirme.midas2.service.impl.seguridad.filler.suscriptorordentrabajo.PaginaPermisoSuscriptorOrdenTrabajo;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class UsuarioServiceMock implements UsuarioService {

	private SistemaContext sistemaContext;
	private DummyFiller df;
	private AgenteMidasService agenteMidasService;
	public static final Logger LOG=Logger.getLogger(UsuarioServiceMock.class);
		
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	
	@PostConstruct
	public void initialize() {
		df = new DummyFiller();
	}
	
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	@Override
	public Usuario getUsuarioActual() {
		return SeguridadContext.getContext().getUsuario();
	}
	
	@Override
	public List<Usuario> buscarUsuariosSinRolesPorNombreRol(String nombreRol,
			String nombreUsuario, String idSesionUsuario) {
		return df.obtieneUsuariosPorRol(nombreRol);
	}

	@Override
	public List<Usuario> buscarUsuariosSinRolesPorNombreRol(
			String nombreUsuario, String idSesionUsuario, String... nombresRol) {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		for (String nombreRol : nombresRol) {
			usuarios.addAll(df.obtieneUsuariosPorRol(nombreRol));
		}
		return usuarios;
	}

	@Override
	public Usuario buscarUsuarioRegistrado(String nombreUsuario,
			String idSesionUsuario) {
		return df.obtieneUsuarioMidas(nombreUsuario);
	}

	@Override
	public boolean logOutUsuario(String nombreUsuario, String idSesionUsuario) {
		return true;
	}

	@Override
	public List<PageConsentDTO> buscarConsentimientosPorRol(int idRole) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Usuario buscarUsuarioPorId(int idUsuario) {
		return df.obtenerUsuarioPorId(idUsuario);
	}

	@Override
	public Usuario buscarUsuarioPorNombreUsuario(String nombreUsuario) {
		return df.obtieneUsuarioPorNombreUsuario(nombreUsuario);
	}

	@Override
	public List<Usuario> buscarUsuariosPorNombreRol(String... nombresRol) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private class DummyFiller {
		private List<Permiso> listaPermiso = new ArrayList<Permiso>();
		protected HashMap<String, String> mapaRolesUsuario = new HashMap<String, String>();
		protected HashMap<String, Usuario> usuariosRegistrados = new HashMap<String, Usuario>();
		protected int i = 1;
		
		public DummyFiller() {
			llenaUsuarios();
			llenaPermisos();
		}
		
		public void llenaUsuarios() {
			llenaUsuariosSistema();
		}

		private void llenaUsuariosSistema() {
			Usuario usuario = null;
			String nombreCompleto = null;
			String nombreUsuario = null;
			String roles = null;
			
			nombreUsuario = "KMGONVAZ";
			nombreCompleto = "KM Gonzalez Vazquez";
			roles = sistemaContext.getRolAgente();
			usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
			usuariosRegistrados.put(nombreUsuario, usuario);
			mapaRolesUsuario.put(nombreUsuario, roles);
					
			nombreUsuario = "SISTEMA";
			nombreCompleto = "Procesos Sistema Midas";
			roles = sistemaContext.getRolAgente();
			usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
			usuariosRegistrados.put(nombreUsuario, usuario);
			mapaRolesUsuario.put(nombreUsuario, roles);
			
			nombreUsuario = "ADMIN";
			nombreCompleto = "Administrador MIDAS";
			roles = sistemaContext.getRolAgente() + "+" +
					sistemaContext.getRolMesaControl() + "+" +
					sistemaContext.getRolCoordinadorEmi() + "+" +
					sistemaContext.getRolDirectorTecnico() + "+" +
					sistemaContext.getRolAdminProductos() + "+" +
					sistemaContext.getRolEmisor() + "+" +
					sistemaContext.getRolAsignadorOt() + "+" +
					sistemaContext.getRolSuscriptorOt() + "+" +
					sistemaContext.getRolSuscriptorCot() + "+" +
					sistemaContext.getRolAsignadorSol() + "+" +
					sistemaContext.getRolSupervisorSuscriptor() + "+" +
					sistemaContext.getRolReporteDanios() + "+" +
					sistemaContext.getRolCabinero() + "+" +
					sistemaContext.getRolAjustador() + "+" +
					sistemaContext.getRolDirectorDeOperaciones() + "+" +
					sistemaContext.getRolAnalistaAdministrativo() + "+" +
					sistemaContext.getRolAnalistaAdministrativoFacultativo() + "+" +
					sistemaContext.getRolCoordinadorSiniestros() + "+" +
					sistemaContext.getRolGerenteSiniestros() + "+" +
					sistemaContext.getRolCoordinadorSiniestrosFacultativo() + "+" +
					sistemaContext.getRolGerenteSiniestrosFacultativo() + "+" +
					sistemaContext.getRolReportesSiniestros() + "+" +
					sistemaContext.getRolGerenteReaseguro() + "+" +
					sistemaContext.getRolOpReaseguroAutomatico() + "+" +
					sistemaContext.getRolOpAsistenteSubdirectorReaseguro() + "+" +
					sistemaContext.getRolSubdirectorReaseguro() + "+" +
					sistemaContext.getRolDirectorReaseguro() + "+" +
					sistemaContext.getRolOpReaseguroAutomatico() + "+" +
					sistemaContext.getRolActuario() + "+" +
					sistemaContext.getRolOpPagosCobrosReaseguradores() + "+" + 
					sistemaContext.getRolCoordinadorAdministrativoReaseguro() + "+" +
					sistemaContext.getRolReportesReaseguro() + "+" +
					sistemaContext.getRolDirectorJuridico() + "+" +  
					sistemaContext.getRolEspAdministradorColonias();
			usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
			usuariosRegistrados.put(nombreUsuario, usuario);
			mapaRolesUsuario.put(nombreUsuario, roles);

			nombreUsuario = "MESA";
			nombreCompleto = "Mesa De Control Autos";
			roles = sistemaContext.getRolAgente() + "+" +
					sistemaContext.getRolMesaControl() + "+" +
					sistemaContext.getRolCoordinadorEmi() + "+" +
					sistemaContext.getRolDirectorTecnico() + "+" +
					sistemaContext.getRolAdminProductos() + "+" +
					sistemaContext.getRolEmisor() + "+" +
					sistemaContext.getRolAsignadorOt() + "+" +
					sistemaContext.getRolSuscriptorOt() + "+" +
					sistemaContext.getRolSuscriptorCot() + "+" +
					sistemaContext.getRolAsignadorSol() + "+" +
					sistemaContext.getRolSupervisorSuscriptor() + "+" +
					sistemaContext.getRolReporteDanios() + "+" +
					sistemaContext.getRolCabinero() + "+" +
					sistemaContext.getRolAjustador() + "+" +
					sistemaContext.getRolDirectorDeOperaciones() + "+" +
					sistemaContext.getRolAnalistaAdministrativo() + "+" +
					sistemaContext.getRolAnalistaAdministrativoFacultativo() + "+" +
					sistemaContext.getRolCoordinadorSiniestros() + "+" +
					sistemaContext.getRolGerenteSiniestros() + "+" +
					sistemaContext.getRolCoordinadorSiniestrosFacultativo() + "+" +
					sistemaContext.getRolGerenteSiniestrosFacultativo() + "+" +
					sistemaContext.getRolReportesSiniestros() + "+" +
					sistemaContext.getRolGerenteReaseguro() + "+" +
					sistemaContext.getRolOpReaseguroAutomatico() + "+" +
					sistemaContext.getRolOpAsistenteSubdirectorReaseguro() + "+" +
					sistemaContext.getRolSubdirectorReaseguro() + "+" +
					sistemaContext.getRolDirectorReaseguro() + "+" +
					sistemaContext.getRolOpReaseguroAutomatico() + "+" +
					sistemaContext.getRolActuario() + "+" +
					sistemaContext.getRolOpPagosCobrosReaseguradores() + "+" + 
					sistemaContext.getRolCoordinadorAdministrativoReaseguro() + "+" +
					sistemaContext.getRolReportesReaseguro() + "+" +
					sistemaContext.getRolMesaControlAutos() + "+" +
					sistemaContext.getRolAnalistaJuridico() + "+" +  
					sistemaContext.getRolEspAdministradorColonias();
			usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
			usuariosRegistrados.put(nombreUsuario, usuario);
			mapaRolesUsuario.put(nombreUsuario, roles);
			
			nombreUsuario = "COORAUT";
			nombreCompleto = "Coordinador Suscripcion Autos";
			roles = sistemaContext.getRolAgente() + "+" +
					sistemaContext.getRolMesaControl() + "+" +
					sistemaContext.getRolCoordinadorEmi() + "+" +
					sistemaContext.getRolDirectorTecnico() + "+" +
					sistemaContext.getRolAdminProductos() + "+" +
					sistemaContext.getRolEmisor() + "+" +
					sistemaContext.getRolAsignadorOt() + "+" +
					sistemaContext.getRolSuscriptorOt() + "+" +
					sistemaContext.getRolSuscriptorCot() + "+" +
					sistemaContext.getRolAsignadorSol() + "+" +
					sistemaContext.getRolSupervisorSuscriptor() + "+" +
					sistemaContext.getRolReporteDanios() + "+" +
					sistemaContext.getRolCabinero() + "+" +
					sistemaContext.getRolAjustador() + "+" +
					sistemaContext.getRolDirectorDeOperaciones() + "+" +
					sistemaContext.getRolAnalistaAdministrativo() + "+" +
					sistemaContext.getRolAnalistaAdministrativoFacultativo() + "+" +
					sistemaContext.getRolCoordinadorSiniestros() + "+" +
					sistemaContext.getRolGerenteSiniestros() + "+" +
					sistemaContext.getRolCoordinadorSiniestrosFacultativo() + "+" +
					sistemaContext.getRolGerenteSiniestrosFacultativo() + "+" +
					sistemaContext.getRolReportesSiniestros() + "+" +
					sistemaContext.getRolGerenteReaseguro() + "+" +
					sistemaContext.getRolOpReaseguroAutomatico() + "+" +
					sistemaContext.getRolOpAsistenteSubdirectorReaseguro() + "+" +
					sistemaContext.getRolSubdirectorReaseguro() + "+" +
					sistemaContext.getRolDirectorReaseguro() + "+" +
					sistemaContext.getRolOpReaseguroAutomatico() + "+" +
					sistemaContext.getRolActuario() + "+" +
					sistemaContext.getRolOpPagosCobrosReaseguradores() + "+" + 
					sistemaContext.getRolCoordinadorAdministrativoReaseguro() + "+" +
					sistemaContext.getRolReportesReaseguro() + "+" +
					sistemaContext.getRolCoordinadorAutos() + "+" +
					sistemaContext.getRolEspAdministradorColonias();
			usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
			usuariosRegistrados.put(nombreUsuario, usuario);
			mapaRolesUsuario.put(nombreUsuario, roles);			
			
			nombreUsuario = "SUSAUT";
			nombreCompleto = "Suscriptor Autos";
			roles = sistemaContext.getRolAgente() + "+" +
					sistemaContext.getRolMesaControl() + "+" +
					sistemaContext.getRolCoordinadorEmi() + "+" +
					sistemaContext.getRolDirectorTecnico() + "+" +
					sistemaContext.getRolAdminProductos() + "+" +
					sistemaContext.getRolEmisor() + "+" +
					sistemaContext.getRolAsignadorOt() + "+" +
					sistemaContext.getRolSuscriptorOt() + "+" +
					sistemaContext.getRolSuscriptorCot() + "+" +
					sistemaContext.getRolAsignadorSol() + "+" +
					sistemaContext.getRolSupervisorSuscriptor() + "+" +
					sistemaContext.getRolReporteDanios() + "+" +
					sistemaContext.getRolCabinero() + "+" +
					sistemaContext.getRolAjustador() + "+" +
					sistemaContext.getRolDirectorDeOperaciones() + "+" +
					sistemaContext.getRolAnalistaAdministrativo() + "+" +
					sistemaContext.getRolAnalistaAdministrativoFacultativo() + "+" +
					sistemaContext.getRolCoordinadorSiniestros() + "+" +
					sistemaContext.getRolGerenteSiniestros() + "+" +
					sistemaContext.getRolCoordinadorSiniestrosFacultativo() + "+" +
					sistemaContext.getRolGerenteSiniestrosFacultativo() + "+" +
					sistemaContext.getRolReportesSiniestros() + "+" +
					sistemaContext.getRolGerenteReaseguro() + "+" +
					sistemaContext.getRolOpReaseguroAutomatico() + "+" +
					sistemaContext.getRolOpAsistenteSubdirectorReaseguro() + "+" +
					sistemaContext.getRolSubdirectorReaseguro() + "+" +
					sistemaContext.getRolDirectorReaseguro() + "+" +
					sistemaContext.getRolOpReaseguroAutomatico() + "+" +
					sistemaContext.getRolActuario() + "+" +
					sistemaContext.getRolOpPagosCobrosReaseguradores() + "+" + 
					sistemaContext.getRolCoordinadorAdministrativoReaseguro() + "+" +
					sistemaContext.getRolReportesReaseguro() + "+" +
					sistemaContext.getRolSuscriptorAutos() + "+" +
					sistemaContext.getRolEspAdministradorColonias();
			usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
			usuariosRegistrados.put(nombreUsuario, usuario);
			mapaRolesUsuario.put(nombreUsuario, roles);					
		}
		
		private void llenaPermisos() {
			
			Permiso permiso;
			
			permiso = new Permiso(new Integer("1"),"AG","Agregar","Agregar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("2"),"AC","Actualizar","Actualizar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("3"),"BR","Borrar","Borrar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("4"),"RE","Reporte","Reporte");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("5"),"EX","Exportar","Exportar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("6"),"AD","Adjuntar","Adjuntar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("7"),"VD","VerDetalle","Ver Detalle");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("8"),"AS","Asignar","Asignar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("9"),"BU","Buscar","Buscar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("10"),"CO","Consultar","Consultar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("11"),"CT","Continuar","Continuar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("12"),"GU","Guardar","Guardar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("13"),"NV","NuevaVersion","Nueva Version");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("14"),"RE","Regresar","Regresar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("15"),"SE","Seleccionar","Seleccionar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("16"),"TE","Terminar","Terminar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("17"),"RC","rechazarCancelar","Rechazar / Cancelar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("18"),"EM","enviarEmision","Enviar a emisi�n");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("19"),"IM","imprimir","Imprimir");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("20"),"ET","emitir","Emitir");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("21"),"LI","liberar","Liberar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("22"),"AA","autoasignar","AutoAsignar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("23"),"AU","autorizar","Autorizar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("24"),"RC","rechazar","Rechazar");
			listaPermiso.add(permiso);
			
			permiso = new Permiso(new Integer("25"),"RS","resumen","Resumen");
			listaPermiso.add(permiso);		
		}
		
			
		private void llenaMenuSistema(Usuario usuario) {
			List<Menu> listaMenuSistema = new ArrayList<Menu>();
			
			MenuSistema mSistema = new MenuSistema();
			listaMenuSistema = mSistema.obtieneMenuItems();
			
			for (Menu menu : listaMenuSistema) {
				usuario.getMenus().add(menu);
			}
			
			
		}
		
		
		private void llenaMenuPorRol(Usuario usuario, Rol rol) {
			
			List<Menu> listaMenu = new ArrayList<Menu>();
					
			if (rol.getDescripcion().equals(sistemaContext.getRolCabinero())) {
				MenuCabinero mCabinero = new MenuCabinero();
				listaMenu = mCabinero.obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolAjustador())) {
				MenuAjustador mAjustador = new MenuAjustador();
				listaMenu = mAjustador.obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolOpReaseguroAutomatico())){
				MenuOpReaseguroAutomatico menuOperadorReaseguroAutomatico = new MenuOpReaseguroAutomatico();
				listaMenu = menuOperadorReaseguroAutomatico.obtieneMenuItems();
			}else if (rol.getDescripcion().equals(sistemaContext.getRolOpAsistenteSubdirectorReaseguro())){
				MenuOpAsistenteSubdirectorReaseguro menuOpAsistenteSubdirectorReaseguro = new MenuOpAsistenteSubdirectorReaseguro();
				listaMenu = menuOpAsistenteSubdirectorReaseguro.obtieneMenuItems();
			}else if (rol.getDescripcion().equals(sistemaContext.getRolSubdirectorReaseguro())){
				MenuSubdirectorReaseguro menuSubdirectorReaseguro = new MenuSubdirectorReaseguro();
				listaMenu = menuSubdirectorReaseguro.obtieneMenuItems();
			}else if (rol.getDescripcion().equals(sistemaContext.getRolDirectorReaseguro())){
				MenuDirectorReaseguro menuDirectorReaseguro = new MenuDirectorReaseguro();
				listaMenu = menuDirectorReaseguro.obtieneMenuItems();
			}else if (rol.getDescripcion().equals(sistemaContext.getRolSupervisorSuscriptor())){
				MenuSupervisorSuscriptor menuSupervisorSuscriptor = new MenuSupervisorSuscriptor();
				listaMenu = menuSupervisorSuscriptor.obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolCoordinadorSiniestros())){
				MenuCoordinadorSiniestros menuCoordinador = new MenuCoordinadorSiniestros();
				listaMenu = menuCoordinador.obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolCoordinadorSiniestrosFacultativo())){
				MenuCoordinadorSiniestrosFacultativo menuCoordinador = new MenuCoordinadorSiniestrosFacultativo();
				listaMenu = menuCoordinador.obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolGerenteSiniestros())){
				MenuGerenteSiniestros menuGerente = new MenuGerenteSiniestros();
				listaMenu = menuGerente.obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolGerenteSiniestrosFacultativo())){
				MenuGerenteSiniestrosFacultativo menuGerente = new MenuGerenteSiniestrosFacultativo();
				listaMenu = menuGerente.obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolReportesSiniestros())){
				listaMenu = new MenuReportesSiniestros().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolEmisor())){
				MenuEmisor menuEmisor = new MenuEmisor();
				listaMenu = menuEmisor.obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolAsignadorOt())){
				MenuAsignadorOrdenTrabajo menu = new MenuAsignadorOrdenTrabajo();
				listaMenu = menu.obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolSuscriptorOt())){
				MenuSuscriptorOrdenTrabajo menu = new MenuSuscriptorOrdenTrabajo();
				listaMenu = menu.obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolAsignadorSol())){
				listaMenu = new MenuAsignadorSolicitud().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolAgente())){
				listaMenu = new MenuAgente().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolSuscriptorCot())){
				MenuSuscriptorCotizacion menuSuscriptor = new MenuSuscriptorCotizacion();
				listaMenu = menuSuscriptor.obtieneMenuItems();
			}else if (rol.getDescripcion().equals(sistemaContext.getRolAnalistaAdministrativo())){
				MenuAnalistaAdministrativo mAnalistaAdministrativo = new MenuAnalistaAdministrativo();
				listaMenu = mAnalistaAdministrativo.obtieneMenuItems();
			}else if (rol.getDescripcion().equals(sistemaContext.getRolAnalistaAdministrativoFacultativo())){
				MenuAnalistaAdministrativoFacultativo mAnalistaAdministrativoFacultativo = new MenuAnalistaAdministrativoFacultativo();
				listaMenu = mAnalistaAdministrativoFacultativo.obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolEspAdministradorColonias())){
				listaMenu = new MenuAdministradorColonias().obtieneMenuItems();
			}
			
			else if (rol.getDescripcion().equals(sistemaContext.getRolMesaControl())){ //Nuevos Roles Da�os
				listaMenu = new MenuMesaControl().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolCoordinadorEmi())){
				listaMenu = new MenuCoordinadorEmision().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolDirectorTecnico())){
				listaMenu = new MenuDirectorTecnico().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolAdminProductos())){
				listaMenu = new MenuAdministradorProductos().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolReporteDanios())){
				listaMenu = new MenuReportesDanios().obtieneMenuItems();
			}	
			
			else if (rol.getDescripcion().equals(sistemaContext.getRolOpReaseguroFacultativo())){ //Nuevos roles reaseguro
				listaMenu = new MenuOpReaseguroFacultativo().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolActuario())){
				listaMenu = new MenuActuario().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolOpPagosCobrosReaseguradores())){
				listaMenu = new MenuOpPagosCobrosReaseguradores().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolCoordinadorAdministrativoReaseguro())){
				listaMenu = new MenuCoordinadorAdministrativoReaseguro().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolReportesReaseguro())){
				listaMenu = new MenuReportesReaseguro().obtieneMenuItems();
			}
			else if (rol.getDescripcion().equals(sistemaContext.getRolConsultaDanios())){ //Roles de Consulta
				listaMenu = new MenuConsultaDanios().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolConsultaSiniestros())){ 
				listaMenu = new MenuConsultaSiniestros().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolConsultaReaseguro())){
				listaMenu = new MenuConsultaReaseguro().obtieneMenuItems();
			}
			else if (rol.getDescripcion().equals(sistemaContext.getRolCoordinadorAutos())){ //Roles de Autos
				listaMenu = new MenuAgente().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolSuscriptorAutos())){ 
				listaMenu = new MenuAgente().obtieneMenuItems();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolCoordinadorAutos())){
				listaMenu = new MenuAgente().obtieneMenuItems();
			}			
			
			
			
			
					
			for (Menu menu : listaMenu) {
				usuario.getMenus().add(menu);
			}
			
		}
		
		private void llenaPaginaPermisoSistema(Usuario usuario) {
			List<PaginaPermiso> listaPaginaPermisoSistema = new ArrayList<PaginaPermiso>();
			
			PaginaPermisoSistema ppSistema = new PaginaPermisoSistema(this.listaPermiso);
			listaPaginaPermisoSistema = ppSistema.obtienePaginaPermisos();
			
			for (PaginaPermiso pp : listaPaginaPermisoSistema) {
				usuario.getPages().add(pp.getPagina());
			}
			
		}
		
		
		private void llenaPaginaPermisoPorRol(Usuario usuario, Rol rol) {
			
			List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
			
			if (rol.getDescripcion().equals(sistemaContext.getRolCabinero())) {
				PaginaPermisoCabinero ppCabinero = new PaginaPermisoCabinero(this.listaPermiso);
				listaPaginaPermiso = ppCabinero.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolAjustador())) {
				PaginaPermisoAjustador ppAjustador = new PaginaPermisoAjustador(this.listaPermiso);
				listaPaginaPermiso = ppAjustador.obtienePaginaPermisos();
			}else if (rol.getDescripcion().equals(sistemaContext.getRolOpReaseguroAutomatico())){
				PaginaPermisoOpReaseguroAutomatico opReaseguroAutomatico = new PaginaPermisoOpReaseguroAutomatico(this.listaPermiso);
				listaPaginaPermiso = opReaseguroAutomatico.obtienePaginaPermisos();
			}else if(rol.getDescripcion().equals(sistemaContext.getRolOpAsistenteSubdirectorReaseguro())){
				PaginaPermisoOpAsistenteSubdirectorReaseguro opAsistenteSubdirectorReaseguro = new PaginaPermisoOpAsistenteSubdirectorReaseguro(this.listaPermiso);
				listaPaginaPermiso = opAsistenteSubdirectorReaseguro.obtienePaginaPermisos();
			}else if (rol.getDescripcion().equals(sistemaContext.getRolSubdirectorReaseguro())){
				PaginaPermisoSubdirectorReaseguro permisoSubdirectorReaseguro = new PaginaPermisoSubdirectorReaseguro(this.listaPermiso);
				listaPaginaPermiso = permisoSubdirectorReaseguro.obtienePaginaPermisos();
			}else if (rol.getDescripcion().equals(sistemaContext.getRolDirectorReaseguro())){
				PaginaPermisoDirectorReaseguro paginaPermisoDirectorReaseguro = new PaginaPermisoDirectorReaseguro(this.listaPermiso);
				listaPaginaPermiso = paginaPermisoDirectorReaseguro.obtienePaginaPermisos();
			}else if (rol.getDescripcion().equals(sistemaContext.getRolSupervisorSuscriptor())){
				PaginaPermisoSupervisorSuscriptor paginaPermisoSupervisorSuscriptor = new PaginaPermisoSupervisorSuscriptor(this.listaPermiso);
				listaPaginaPermiso = paginaPermisoSupervisorSuscriptor.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolCoordinadorSiniestros())){
				PaginaPermisoCoordinadorSiniestros ppCoordinadorSiniestros = new PaginaPermisoCoordinadorSiniestros(this.listaPermiso);
				listaPaginaPermiso = ppCoordinadorSiniestros.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolCoordinadorSiniestrosFacultativo())){
				PaginaPermisoCoordinadorSiniestrosFacultativo ppCoordinadorSiniestrosFacultativo = new PaginaPermisoCoordinadorSiniestrosFacultativo(this.listaPermiso);
				listaPaginaPermiso = ppCoordinadorSiniestrosFacultativo.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolGerenteSiniestros())){
				PaginaPermisoGerenteSiniestros ppGerenteSiniestros = new PaginaPermisoGerenteSiniestros(this.listaPermiso);
				listaPaginaPermiso = ppGerenteSiniestros.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolGerenteSiniestrosFacultativo())){
				PaginaPermisoGerenteSiniestrosFacultativo ppGerenteSiniestrosFacultativo = new PaginaPermisoGerenteSiniestrosFacultativo(this.listaPermiso);
				listaPaginaPermiso = ppGerenteSiniestrosFacultativo.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolReportesSiniestros())){
				listaPaginaPermiso = new PaginaPermisoReportesSiniestros(this.listaPermiso).obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolEmisor())){
				listaPaginaPermiso = new PaginaPermisoEmisor(this.listaPermiso).obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolAsignadorOt())){
				listaPaginaPermiso = new PaginaPermisoAsignadorOrdenTrabajo(this.listaPermiso).obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolSuscriptorOt())){
				listaPaginaPermiso = new PaginaPermisoSuscriptorOrdenTrabajo(this.listaPermiso).obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolAsignadorSol())){
				listaPaginaPermiso = new PaginaPermisoAsignadorSolicitud(this.listaPermiso).obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolAgente())){
				listaPaginaPermiso = new PaginaPermisoAgente(this.listaPermiso).obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolSuscriptorCot())){
				listaPaginaPermiso = new PaginaPermisoSuscriptorCotizacion(this.listaPermiso).obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolAnalistaAdministrativo())){
				PaginaPermisoAnalistaAdministrativo ppAnalista = new PaginaPermisoAnalistaAdministrativo(this.listaPermiso);
				listaPaginaPermiso = ppAnalista.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolAnalistaAdministrativoFacultativo())){
				PaginaPermisoAnalistaAdministrativoFacultativo ppAnalistaFacultativo = new PaginaPermisoAnalistaAdministrativoFacultativo(this.listaPermiso);
				listaPaginaPermiso = ppAnalistaFacultativo.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolEspAdministradorColonias())){
				listaPaginaPermiso = new PaginaPermisoAdministradorColonias(this.listaPermiso).obtienePaginaPermisos();
			}
			
			else if (rol.getDescripcion().equals(sistemaContext.getRolMesaControl())){ //Nuevos Roles Da�os
				PaginaPermisoMesaControl ppDanos = new PaginaPermisoMesaControl(this.listaPermiso);
				listaPaginaPermiso = ppDanos.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolCoordinadorEmi())){
				PaginaPermisoCoordinadorEmision ppDanos = new PaginaPermisoCoordinadorEmision(this.listaPermiso);
				listaPaginaPermiso = ppDanos.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolDirectorTecnico())){
				PaginaPermisoDirectorTecnico ppDanos = new PaginaPermisoDirectorTecnico(this.listaPermiso);
				listaPaginaPermiso = ppDanos.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolAdminProductos())){
				PaginaPermisoAdministradorProductos ppDanos = new PaginaPermisoAdministradorProductos(this.listaPermiso);
				listaPaginaPermiso = ppDanos.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolReporteDanios())){
				listaPaginaPermiso = new PaginaPermisoReportesDanios(this.listaPermiso).obtienePaginaPermisos();
			}	
			
			else if (rol.getDescripcion().equals(sistemaContext.getRolOpReaseguroFacultativo())){ //Nuevos roles reaseguro
				PaginaPermisoOpReaseguroFacultativo ppReaseguro = new PaginaPermisoOpReaseguroFacultativo(this.listaPermiso);
				listaPaginaPermiso = ppReaseguro.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolActuario())){
				PaginaPermisoActuario ppReaseguro = new PaginaPermisoActuario(this.listaPermiso);
				listaPaginaPermiso = ppReaseguro.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolOpPagosCobrosReaseguradores())){
				PaginaPermisoOpPagosCobrosReaseguradores ppReaseguro = new PaginaPermisoOpPagosCobrosReaseguradores(this.listaPermiso);
				listaPaginaPermiso = ppReaseguro.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolCoordinadorAdministrativoReaseguro())){
				PaginaCoordinadorAdministrativoReaseguro ppReaseguro = new PaginaCoordinadorAdministrativoReaseguro(this.listaPermiso);
				listaPaginaPermiso = ppReaseguro.obtienePaginaPermisos();
			} else if (rol.getDescripcion().equals(sistemaContext.getRolReportesReaseguro())){
				listaPaginaPermiso = new PaginaPermisoReportesReaseguro(this.listaPermiso).obtienePaginaPermisos();
			}
	        	else if (rol.getDescripcion().equals(sistemaContext.getRolConsultaDanios())){ //Roles de Consulta
	        	    listaPaginaPermiso = new PaginaPermisoConsultaDanios(this.listaPermiso).obtienePaginaPermisos();
	        	} else if (rol.getDescripcion().equals(sistemaContext.getRolConsultaSiniestros())){ 
	        	    listaPaginaPermiso = new PaginaPermisoConsultaSiniestros(this.listaPermiso).obtienePaginaPermisos();
	        	} else if (rol.getDescripcion().equals(sistemaContext.getRolConsultaReaseguro())){
	        	    listaPaginaPermiso = new PaginaPermisoConsultaReaseguro(this.listaPermiso).obtienePaginaPermisos();
	        	}
					
			for (PaginaPermiso pp : listaPaginaPermiso) {
				usuario.getPages().add(pp.getPagina());
			}
			
		}
		
		/**
		 * Obtiene un usuario del sistema MIDAS con el/los rol(es) especificado(s)
		 * @param descripcionesRol Cadena con la(s) descripcion(es) de rol(es) separadas por '+'. Ej: rol1+rol2+rol3
		 * @return Objeto de usuario MIDAS
		 */
		public Usuario obtieneUsuarioMidas(String descripcionesRol) {
			
			Usuario usuario;
			Rol rol;
			int i = 1;
			
			String[] roles = descripcionesRol.split("\\+");
			
			usuario = new Usuario(new Integer("1"), "Usuario default", "default");
			
			for (String descripcionRol : roles) {
				rol = new Rol(new Integer(i), descripcionRol);
				llenaMenuPorRol(usuario, rol);
				llenaPaginaPermisoPorRol(usuario, rol);
				usuario.getRoles().add(rol);
				//TEMPORALMENTE. El nombre de usuario para el rol de agente es usado para recuperar la lista de agentes, 
				//necesaria para registar una solicitud. Este nombre vendr� poblado de SEYCOS.
				if (rol.getDescripcion().equals(sistemaContext.getRolAgente())) {
					usuario.setNombreUsuario("KMGONVAZ");
				} else if (rol.getDescripcion().equals(sistemaContext.getRolSuscriptorOt())) {
					usuario.setNombreUsuario("ARMARTOR");//Arturo Martinez
				} else if (rol.getDescripcion().equals(sistemaContext.getRolSuscriptorCot())) {
					usuario.setNombreUsuario("CLGARROM");//Clara Garc�a
				}if (rol.getDescripcion().equals(sistemaContext.getRolCoordinadorSiniestros())) {
					usuario.setId(new Integer(54));
				}
				
				usuario.setNombre(descripcionesRol);
				i++;
			}
				
			return usuario;
			
		}
		
		public Usuario obtieneUsuarioPorId(Integer id){
			Usuario usr = obtieneUsuarioMidas(sistemaContext.getRolCabinero(),1,"","","");
			
			return usr;
			//return null;
		}
			
		
		private Usuario obtieneUsuarioMidas(String descripcionesRol, Integer id, String name, String userName, String email) {
			Usuario usuario;
			Rol rol;
			int i = 1;
			
			String[] roles = descripcionesRol.split("\\+");
			
			usuario = new Usuario(id, name, userName, email);
			
			for (String descripcionRol : roles) {
				rol = new Rol(new Integer(i), descripcionRol);
				llenaMenuSistema(usuario);
				llenaPaginaPermisoSistema(usuario);
				llenaMenuPorRol(usuario, rol);
				llenaPaginaPermisoPorRol(usuario, rol);
				usuario.getRoles().add(rol);
				i++;
			}
			return usuario;
		}
		
		public Usuario obtieneUsuarioPorid(Integer idUsuario,String rol){
			Usuario user = new Usuario();
			List<Usuario> listaUsuarios = obtieneUsuariosPorRol(rol);
			for(Usuario userLista: listaUsuarios){
				if(userLista.getId().compareTo(idUsuario) == 0){
					user = userLista;
				}
			}
			return user;
		}
		
		public Usuario obtieneUsuarioPorNombreUsuario(String nombreUsuario){
			
			nombreUsuario = nombreUsuario.toUpperCase().trim();
			
			Usuario usuarioRegistrado =  usuariosRegistrados.get(nombreUsuario);
			
			if (usuarioRegistrado != null) {
			
				Usuario usr = obtieneUsuarioMidas(mapaRolesUsuario.get(nombreUsuario),
						usuarioRegistrado.getId(), usuarioRegistrado.getNombre(), usuarioRegistrado.getNombreUsuario(), usuarioRegistrado.getEmail());
				
				return usr;
			}
			
			return null;
		}
		
		public List<Usuario> obtieneUsuariosPorRol(String rol){
			List<Usuario> usuarios = new ArrayList<Usuario>();
			
			Collection<Usuario> coleccionUsuarios =  usuariosRegistrados.values();
			
			Iterator<Usuario> itr = coleccionUsuarios.iterator();
			
			while(itr.hasNext()) {
			
			    Usuario usuario = itr.next(); 
			    String descripcionesRol = mapaRolesUsuario.get(usuario.getNombreUsuario());
			    String[] roles = descripcionesRol.split("\\+");
			    
			    for (String descripcionRol : roles) {
			    	if (descripcionRol.equals(rol)) {
			    		
			    		Usuario usr = obtieneUsuarioMidas(descripcionesRol,
			    				usuario.getId(), usuario.getNombre(), usuario.getNombreUsuario(), usuario.getEmail());
			    		
			    		usuarios.add(usr);
			    		break;
			    	}
			    }
			} 
			
			return usuarios;
		}
		
		public List<Usuario> getCoordinadoresSiniestro(){
			List<Usuario> coordinadoresSiniestro = null;
			List<Usuario> coordinadoresFacultativos = null;
			
			coordinadoresSiniestro = this.obtieneUsuariosPorRol(sistemaContext.getRolCoordinadorSiniestros());
			coordinadoresFacultativos = this.obtieneUsuariosPorRol(sistemaContext.getRolCoordinadorSiniestrosFacultativo());
			
			for(Usuario coordinador : coordinadoresFacultativos){
				if(! coordinadoresSiniestro.contains(coordinador)){
					coordinadoresSiniestro.add(coordinador);
				}
			}		
			
			return coordinadoresSiniestro;
		}
		
		public Usuario obtenerUsuarioPorId(Integer idUsuario){
			Usuario usuario = new Usuario();
					
			Object[] usuarios = usuariosRegistrados.values().toArray();		
					
			for(Object userItem : usuarios){
				Usuario user = (Usuario)userItem;
				if(user.getId().compareTo(idUsuario) == 0){
					usuario = user;
					break;
				}
			}
			
			return usuario;		
		}
		
		public List<Usuario> obtieneUsuariosPorNombreCompleto(String nombre){
			List<Usuario> usuarios = new ArrayList<Usuario>();
			
			Collection<Usuario> coleccionUsuarios =  usuariosRegistrados.values();
			
			Iterator<Usuario> itr = coleccionUsuarios.iterator();
			
			while(itr.hasNext()) {
			
			    Usuario usuario = itr.next(); 
			    if(usuario != null && usuario.getNombreCompleto() != null &&
			    		usuario.getNombreCompleto().toUpperCase().trim().contains(nombre.toUpperCase().trim())){
			    	
			    	String descripcionesRol = mapaRolesUsuario.get(usuario.getNombreUsuario());
			    	Usuario usr = obtieneUsuarioMidas(descripcionesRol,
		    				usuario.getId(), usuario.getNombre(), usuario.getNombreUsuario(), usuario.getEmail());
		    		
		    		usuarios.add(usr);
			    }
			}
			return usuarios;
		}
		
	}

	@Override
	public boolean tieneRol(String nombreRol, Usuario usuario) {
		return true;
	}
	
	@Override
	public boolean tieneRolUsuarioActual(String nombreRol) {
		return true;
	}

	@Override
	public void setUsuarioActual(Usuario usuario) {
		SeguridadContext.getContext().setUsuario(usuario);
	}

	@Override
	public boolean tienePermisoUsuarioActual(String nombrePermiso) {
		LOG.error("Iniciando tienePermisoUsuarioActual en UsuarioServiceMock para el permiso: "+nombrePermiso);
		if (nombrePermiso.equals("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			return false;
		} else if (nombrePermiso.equals("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
			return false;
		} else if (nombrePermiso.equals("FN_M2_General_Visibilidad_Agente_Todos")) {
			return false;
		} else if (nombrePermiso.equals("FN_M2_SN_No_Permite_Afectacion_Reserva")){
			return false;
		}
		return true;
	}

	@Override
	public boolean tienePermisoUsuario(String nombrePermiso, Usuario usuario) {
		return true;
	}
	
	@Override
	public boolean tienePermisoUsuario(String[] nombrePermisos, Usuario usuario) {
		return true;
	}

	@Override
	public boolean tieneRolUsuarioActual(String[] nombreRoles) {
		return true;
	}

	@Override
	public boolean tieneRol(String[] nombreRoles, Usuario usuario) {
		return true;
	}

	@Override
	public boolean tienePermisoUsuarioActual(String[] nombrePermisos) {
		for(String permiso : nombrePermisos){
			if(!tienePermisoUsuarioActual(permiso)){
				return false;
			};
		}
		return true;
	}
	
	@Override
	public Agente getAgenteUsuarioActual() {
		Usuario usuarioActual = getUsuarioActual();
		LOG.info("Iniciando getAgenteUsuarioActual para buscar el agente del usuario: "+usuarioActual.getNombreUsuario());
		Agente agente = agenteMidasService.findByCodigoUsuario(usuarioActual.getNombreUsuario());
		LOG.error("Agente encontrado: "+((agente!=null)?agente.getPersona().getNombreCompleto():" el Usuario no contiene agente Asignado"));
		boolean obligatorio = tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")
				|| tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual");
		LOG.info("Variable obligatorio: "+(obligatorio ? "true" : "false"));
		if (agente == null && obligatorio) {
			throw new NegocioEJBExeption("_","El usuario no tiene un agente autorizado asignado.");
		}
		return agente;
	}
	
	@Override
	public List<Usuario> buscarUsuarioPorNombreCompleto(String nombreUsuario) {
		return df.obtieneUsuariosPorNombreCompleto(nombreUsuario);
	}

	@Override
	public Usuario login(LoginParameter parameter) {
		return buscarUsuarioRegistrado(parameter.getUsuario(), null);
	}
	
	@Override
	public AjustadorMovil getAjustadorMovilUsuarioActual() {
		AjustadorMovil ajustadorMovil = new AjustadorMovil();
		ajustadorMovil.setId(1l);
		ajustadorMovil.setCodigoUsuario("M2AJUSTA");
		ajustadorMovil.setIdSeycos(3634l);
		return ajustadorMovil;
	}

	@Override
	public AjustadorMovil getAjustadorMovilUsuario(Usuario usuario) {
		return getAjustadorMovilUsuarioActual();
	}

	@Override
	public AjustadorMovil getAjustadorMovilMidasUsuarioActual() {
		AjustadorMovil ajustadorMovil = new AjustadorMovil();
		ajustadorMovil.setId(1l);
		ajustadorMovil.setCodigoUsuario("M2AJUSTA");
		ajustadorMovil.setIdSeycos(3634l);
		return ajustadorMovil;
	}
	
	@Override
	public AjustadorMovil getAjustadorMovilMidasUsuario(Usuario usuario) {
		return getAjustadorMovilMidasUsuarioActual();
	}
	@Override
	public AjustadorMovil getAjustadorMovilMidasUsuario(Long ajustadorId) {
		return getAjustadorMovilMidasUsuarioActual();
	}

	@Override
	public Usuario buscarUsuarioRegistrado(String token) {
		return df.obtieneUsuarioMidas("");
	}

	@Override
	public void deshabilitarToken(String token) {	
	}

	@Override
	@Deprecated
	public List<String> getRegistrationIds(String nombreUsuario) {
		return new ArrayList<String>();
	}
	
	@Override
	public List<String> getRegistrationIds(String nombreUsuario, String deviceApplicationId) {
		return null;
	}
	
	@Override
	public String getRegistrationId(String nombreUsuario, String deviceApplicationId, String deviceUuid) {
		return null;
	}
	
	@Override
	public Usuario createPortalUser(CreatePortalUserParameterDTO parameter) {
		return null;
	}

	@Override
	public void resendConfirmationEmail(ResendConfirmationEmailDTO resendConfirmationEmail) {
		return;
	}
	
	@Override
	public void resendConfirmationEmail(Integer userId) {
		return;
	}


	@Override
	public int confirmEmail(String confirmationCode) {
		return 1;		
	}

	@Override
	public void changePassword(String oldPassword, String newPassword) {
		return;
	}
	
	@Override
	public void resetPassword(String token, String newPassword) {
		return;
	}

	@Override
	public void sendResetPasswordToken(Integer userId) {
		return;		
	}
	
	@Override
	public void sendResetPasswordToken(SendResetPasswordTokenDTO sendResetPasswordToken) {
		return;		
	}
		
	@Override
	public void updateUser(UserUpdateDTO userUpdate) {
		return;
	}

	@Override
	public void changePassword2(String newPassword) {
		return;		
	}

	@Override
	public void activateDeactivateUser(Integer userId, Boolean active,
			Integer administratorUserId) {
		return;
	}

	@Override
	public List<UserInboxEntryDTO> getUserList(
			SearchUserParametersDTO searchParameters) {
		return new ArrayList<UserInboxEntryDTO>();
	}

	@Override
	public Usuario createPasswordUser(CreatePasswordUserParameterDTO parameter) {
		return null;
	}

	@Override
	public Integer findUserIdByUsername(String username) {
		return null;
	}
	
	@Override
	public Integer findRoleId(Integer applicationId, String roleName) {
		return null;
	}

	@Override
	public boolean isUserNeedsToSetPassword(Integer userId) {
		return false;
	}

	@Override
	public String getResetPasswordUrl(Integer userId) {
		return "";
	}

	@Override
	public Agente getAgenteUsuarioActual(Usuario usuarioActual) {
		Agente agente = agenteMidasService.findByCodigoUsuario(usuarioActual.getNombreUsuario());
		boolean obligatorio = tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")
				|| tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual");
		if (agente == null && obligatorio) {
			throw new NegocioEJBExeption("_","El usuario no tiene un agente autorizado asignado..");
		}
		return agente;
	}

	@Override
	public boolean isUserHasAccessToApplication(Integer userId,
			Integer applicationId) {
		return true;
	}

	@Override
	public boolean isUserPortalUser(Integer userId) {
		return true;
	}

	@Override
	public List<ApplicationDTO> getUserApplications(Integer userId) {
		return new ArrayList<ApplicationDTO>();
	}

	@Override
	public boolean isTokenActive(String token) {
		return true;
	}
	
	@Override
	public void validateToken( String token ) throws SystemException{
		
	}

	@Override
	public UserDTO getUserDTOById(Integer userId) {
		UserDTO usuario = new UserDTO();
		
		usuario.setUserId(1);
		
		return usuario;
		
		
	}

	@Override
	public List<Usuario> buscarUsuariosPorNombreRolSimple(String... arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void updateMidasUser(UserDTO updateUser, UserDTO administrator,
			List<RoleDTO> roleList) {
		return;
	}
	
	@Override
	public RoleDTO getRoleDTOById(int id) {
		
		return new RoleDTO();
		
	}

}
