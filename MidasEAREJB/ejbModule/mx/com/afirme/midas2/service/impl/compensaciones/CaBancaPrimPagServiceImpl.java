package mx.com.afirme.midas2.service.impl.compensaciones;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import mx.com.afirme.midas2.dao.compensaciones.CaBancaPrimPagCPDao;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaPrimPagCP;
import mx.com.afirme.midas2.domain.compensaciones.CaBitacora;
import mx.com.afirme.midas2.domain.compensaciones.CargaBancaPrimPag;
import mx.com.afirme.midas2.domain.compensaciones.CargaBancaSiniMes;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.exeption.ErrorBuilder;
import mx.com.afirme.midas2.service.compensaciones.CaBancaPrimPagCPService;
import mx.com.afirme.midas2.service.compensaciones.CaBancaPrimPagService;
import mx.com.afirme.midas2.service.compensaciones.CaBitacoraService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import oracle.jdbc.driver.OracleTypes;
import oracle.jdbc.internal.OracleConnection;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;


@Stateless
public class CaBancaPrimPagServiceImpl implements CaBancaPrimPagService{

    
    private static final Logger LOG = Logger.getLogger(CaBancaPrimPagServiceImpl.class);
    
    
    private UsuarioService usuarioService;
    
    @EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
    @EJB
    private CaBancaPrimPagCPDao caBancaPrimPagCPDao;
    
    @EJB
    private CaBancaPrimPagCPService caBancaService;
    
    @EJB
    private CaBitacoraService bitacoraService;
    
    private Connection con ;
    private DataSource dataSource;
    
    @Resource(name = "jdbc/MidasDataSource")
    public void setDataSource(DataSource dataSource) {
        this.con = DataSourceUtils.getConnection(dataSource);
    }
    
    
    public void procesarArchivos(CaBancaPrimPagCP caBancaPrimPagCP,File filePrimasPagadas,File fileSiniestralidadMes,Double mes){
        guardarConfiguracionCP(caBancaPrimPagCP);
        LOG.info(caBancaPrimPagCP.getId());
        Object[] cargaBancaPrimPag = procesarFilePrimasPagadas(filePrimasPagadas,mes);
        Object[] cargaBancaSiniMes = procesarFileSiniestralidadMes(fileSiniestralidadMes,mes);
        guardarPrimasPagSiniMes(cargaBancaPrimPag, cargaBancaSiniMes,caBancaPrimPagCP.getId());
        
    }
    
    public void guardarConfiguracionCP(CaBancaPrimPagCP caBancaPrimPagCP){
        LOG.info(">> guardarConfiguracion()");
        BigDecimal IdReturn = null;
        CaBitacora caBitacora = new CaBitacora();
        caBancaPrimPagCP.setUsuario(usuarioService.getUsuarioActual().getNombreUsuario());
        try{
        	IdReturn = caBancaService.findByAnioMes(caBancaPrimPagCP.getAnio(), caBancaPrimPagCP.getMes());
        	if (IdReturn !=null){
        		try{
        		DateFormat formatter  = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    			Date date = new Date();
    			String fecha = formatter .format(date);
    			Date modificacion = formatter.parse(fecha);
        		caBancaPrimPagCP.setId((IdReturn.longValue()));
        		caBancaPrimPagCP.setCostonetosinaut(caBancaPrimPagCP.getCostonetosinaut());
        		caBancaPrimPagCP.setCostonetosindan(caBancaPrimPagCP.getCostonetosindan());
        		caBancaPrimPagCP.setPrimasretendevenaut(caBancaPrimPagCP.getPrimasretendevenaut());
        		caBancaPrimPagCP.setPrimasretendevendan(caBancaPrimPagCP.getPrimasretendevendan());
        		caBancaPrimPagCP.setAnio(caBancaPrimPagCP.getAnio());
        		caBancaPrimPagCP.setMes(caBancaPrimPagCP.getMes());
        		caBancaPrimPagCP.setModificacion(modificacion);
        		caBancaPrimPagCP.setUsuario(usuarioService.getUsuarioActual().getNombreUsuario());
        		caBancaPrimPagCPDao.update(caBancaPrimPagCP);
        		caBitacora.setFecha(caBancaPrimPagCP.getModificacion());
        		caBitacora.setUsuario(caBancaPrimPagCP.getUsuario());
        		caBitacora.setMovimiento("ACTUALIZACION DE CONFIGURACION");
        		caBitacora.setConfgiracionBancaCalculoId(caBancaPrimPagCP.getId());
        		bitacoraService.save(caBitacora);
        		
        		}catch(ParseException e){
        			e.printStackTrace();
        		}
        	}
        	else{
        		
            caBancaPrimPagCPDao.save(caBancaPrimPagCP);
            caBitacora.setFecha(caBancaPrimPagCP.getModificacion());
    		caBitacora.setUsuario(caBancaPrimPagCP.getUsuario());
    		caBitacora.setMovimiento("GUARDADO DE CONFIGURACION");
    		caBitacora.setConfgiracionBancaCalculoId(caBancaPrimPagCP.getId());
    		bitacoraService.save(caBitacora);
        	}
        }catch(RuntimeException re){
            LOG.info("Informacion del Error : " + re);
            throw re;
        }
    }
    
    public Object[] procesarFileSiniestralidadMes(File fileUpload,Double mes){
        LOG.info(">> procesarFileSiniestralidadMes");
        List<CargaBancaSiniMes> listSiniMes = new ArrayList<CargaBancaSiniMes>();
        Object[] arrayObjects = null;
        try{
            Workbook workbook = WorkbookFactory.create(new FileInputStream(fileUpload));
            Sheet sheet = workbook.getSheetAt(0);
            int numCol = 25;
            int numRow = 65000;
            Iterator<Row> iteratorRow = sheet.iterator();
            if(iteratorRow.hasNext()){
                iteratorRow.next();
            }
            Row row;
            Cell ofsuc;
            Cell nomOfsuc;
            Cell nomGrupo;
            Cell nomCanal;
            Cell codCanal;
            Cell canalNvo;
            Cell agte;
            Cell nomAgte;
            Cell nomEjec;
            Cell sec;
            Cell nomRamo;
            Cell poliza;
            Cell mon;
            Cell asegurado;
            Cell fecEmi;
            Cell vigDes;
            Cell vigHas;
            Cell pmaEmi;
            Cell pmaPagada;
            Cell pmaDev;
            Cell reclama;
            Cell gastos;
            Cell salvRecu;
            Cell costoSin;
            Cell porSin;
            Cell lineaNeg;

            Integer rowIndex = 0;

            while(iteratorRow.hasNext()){
                row = iteratorRow.next();
            }

            for(int colIndex = 1; colIndex <numCol; colIndex++){
                row = sheet.getRow(colIndex);
                if ( StringUtils.isBlank(String.valueOf(row.getCell(colIndex))) == true ) {
                    throw new Exception("Celdas vacias encontradas, omitiendo... ");
                } 
                    else {                        
                    ofsuc = row.getCell(0);
                    nomOfsuc = row.getCell(1);
                    nomGrupo  = row.getCell(2);
                    nomCanal  = row.getCell(3);
                    codCanal = row.getCell(4);
                    LOG.info("Print this: "+ ofsuc + " codCanal: " + codCanal.toString());
                    canalNvo= row.getCell(5);
                    agte= row.getCell(6);
                    nomAgte= row.getCell(7);
                    nomEjec= row.getCell(8);
                    sec= row.getCell(9);
                    nomRamo= row.getCell(10);
                    poliza = row.getCell(11);
                    String string = poliza.toString();
                    String[] parts = string.split("-");
                    String idCentroEmisParts = parts[0];
                    String numPolizaParts = parts[1];
                    String numRenovPolParts = parts[2];
                    mon= row.getCell(12);
                    asegurado= row.getCell(13);
                    fecEmi =row.getCell(14);
                    vigDes = row.getCell(15);
                    vigHas = row.getCell(16);
                    pmaEmi = row.getCell(17);
                    pmaPagada = row.getCell(18);
                    pmaDev = row.getCell(19);
                    reclama = row.getCell(20);
                    gastos = row.getCell(21);
                    salvRecu = row.getCell(22);
                    costoSin = row.getCell(23);
                    porSin = row.getCell(24);
                    lineaNeg = row.getCell(25);
                    
                     String ofsucString = ofsuc.toString();
                     String nomOfsucString = nomOfsuc.toString();
                     String nomGrupoString = nomGrupo.toString();
                     String nomCanalString = nomCanal.toString();
                     String codCanalString = codCanal.toString();
                     String canalNvoString = canalNvo.toString();
                     String agteString = agte.toString();
                     String nomAgteString = nomAgte.toString();
                     String nomEjecString = nomEjec.toString();
                     String secString = sec.toString();
                     String nomRamoString = nomRamo.toString();
                     String monString = mon.toString();
                     String aseguradoString = asegurado.toString();
                     String fecEmiString = fecEmi.toString();
                     String vigDesString = vigDes.toString();
                     String vigHasString = vigHas.toString();
                     String pmaEmiString = String.valueOf(pmaEmi);
                     Double pmaEmiDouble = Double.parseDouble(pmaEmiString);
                     String pmaPagadaString = String.valueOf(pmaPagada);
                     Double pmaPagadaDouble = Double.parseDouble(pmaPagadaString);
                     String pmaDevString = String.valueOf(pmaDev);
                     Double pmaDevDouble = Double.parseDouble(pmaDevString);
                     String reclamaString = String.valueOf(reclama);
                     Double reclamaDouble = Double.parseDouble(reclamaString);
                     String gastosString = String.valueOf(gastos);
                     Double gastosDouble = Double.parseDouble(gastosString);
                     String salvRecuString = String.valueOf(salvRecu);
                     Double salvRecuDouble = Double.parseDouble(salvRecuString);
                     String costoSinString = String.valueOf(costoSin);
                     Double costoSinDouble = Double.parseDouble(costoSinString);
                     String porSinString = String.valueOf(porSin);
                     Double porSinDouble = Double.parseDouble(porSinString);
                     String lineaNegString = lineaNeg.toString();

                    
                        Calendar now = Calendar.getInstance();
                        int anio = now.get(Calendar.YEAR);
                        String anioString = String.valueOf(anio);
                        Double anioDouble = Double.parseDouble(anioString);

                       listSiniMes.add( new CargaBancaSiniMes (ofsucString,nomOfsucString, nomGrupoString,nomCanalString, codCanalString, canalNvoString, agteString, nomAgteString, nomEjecString, secString, nomRamoString, idCentroEmisParts, numPolizaParts, numRenovPolParts, monString, aseguradoString, fecEmiString, vigDesString, vigHasString, pmaEmiDouble, pmaPagadaDouble, pmaDevDouble, reclamaDouble, gastosDouble, salvRecuDouble, costoSinDouble, porSinDouble, lineaNegString,  anioDouble, mes, null));
                
                    rowIndex++;
                    if(rowIndex == numRow){
                        break;
                    }
                    arrayObjects = new Object[listSiniMes.size()];
                    int i = 0;
                    for(CargaBancaSiniMes siniMes: listSiniMes){
                        Object[] arrayVector = new Object[]{
                        siniMes.getOfsuc(),
                        siniMes.getNomOfsuc(),
                        siniMes.getNomGrupo(),
                        siniMes.getNomCanal(),
                        siniMes.getCodCanal(),
                        siniMes.getCanalNvo(),
                        siniMes.getAgte(),
                        siniMes.getNomAgte(),
                        siniMes.getNomEjec(),
                        siniMes.getSec(),
                        siniMes.getNomRamo(),
                        siniMes.getIdCentroEmision(),
                        siniMes.getNumPoliza(),
                        siniMes.getNumRenovPoliz(),
                        siniMes.getMon(),
                        siniMes.getAsegurado(),
                        siniMes.getFecEmi(),
                        siniMes.getVigDes(),
                        siniMes.getVigHas(),
                        siniMes.getPmaEmi(),
                        siniMes.getPmaPagada(),
                        siniMes.getPmaDev(),
                        siniMes.getReclama(),
                        siniMes.getGastos(),
                        siniMes.getSalvRecu(),
                        siniMes.getCostoSin(),
                        siniMes.getPorSin(),
                        siniMes.getLineaNeg(),
                        siniMes.getAnio(),
                        siniMes.getMes(),
                        siniMes.getBancaPrimpagCpId()
                        };
                        arrayObjects[i] = arrayVector;
                        i = i +1;
                        
                    }
                    }//end validation blank
            }// end for

        }catch(Exception e){
            LOG.info("Informacion del Error : "+ e);
            fileUpload.delete();
        } finally{
            if(fileUpload != null){
                fileUpload.deleteOnExit();
            }
        }
        LOG.info("<< procesarFileSiniestralidadMes()");
        return arrayObjects;
    }
    
    public Object[] procesarFilePrimasPagadas(File fileUpload,Double mes){
        LOG.info(">> procesarFilePrimasPagadas");
        List<CargaBancaPrimPag> listPrimPag = new ArrayList<CargaBancaPrimPag>();
        Object[] arrayObjects = null;
        try{
            Workbook workbook = WorkbookFactory.create(new FileInputStream(fileUpload));
            Sheet sheet = workbook.getSheetAt(0);
            int numCol = 20;
            int numRow = 65000;
            Iterator<Row> iteratorRow = sheet.iterator();
            if(iteratorRow.hasNext()){
                iteratorRow.next();
            }
            Row row;
            Cell ofsuc;
            Cell nomCanal;
            Cell codCanal;
            Cell canalNvo;
            Cell nomGrupo;
            Cell agte;
            Cell idAgente;
            Cell nomAgte;
            Cell nomEjec;
            Cell nomRamo;
            Cell poliza;
            Cell sec;
            Cell mon;
            Cell asegurado;
            Cell fecEmi;
            Cell vigDes;
            Cell vigHas;
            Cell lineaNeg;
            Cell pmaEmi;
            Cell pmaPagada;
            Cell ordimp;

            Integer rowIndex = 0;

            while(iteratorRow.hasNext()){
                row = iteratorRow.next();
            }

            for(int colIndex = 1; colIndex <numCol; colIndex++){
                row = sheet.getRow(colIndex);
                if ( StringUtils.isBlank(String.valueOf(row.getCell(colIndex))) == true ) {
                    throw new Exception("Celdas vacias encontradas, omitiendo... ");
                } 
                    else {   
                    ofsuc = row.getCell(0);
                    nomCanal = row.getCell(1);
                    codCanal = row.getCell(2);
                    canalNvo = row.getCell(3);
                    nomGrupo = row.getCell(4);
                    agte = row.getCell(5);
                    idAgente = row.getCell(6);
                    nomAgte = row.getCell(7);
                    nomEjec = row.getCell(8);
                    nomRamo = row.getCell(9);
                    poliza = row.getCell(10);
                    String string = poliza.toString();
                    String[] parts = string.split("-");
                    String idCentroEmisParts = parts[0];
                    String numPolizaParts = parts[1];
                    String numRenovPolParts = parts[2];
                    sec = row.getCell(11);
                    mon = row.getCell(12);
                    asegurado = row.getCell(13);
                    fecEmi = row.getCell(14);
                    vigDes = row.getCell(15);
                    vigHas = row.getCell(16);
                    lineaNeg = row.getCell(17);
                    pmaEmi = row.getCell(18);
                    pmaPagada = row.getCell(19);
                    ordimp = row.getCell(20);
                    
                    String ofsucString = ofsuc.toString();
                    String nomCanalString = nomCanal.toString();
                    String codCanalString = codCanal.toString();
                    String canalNvoString = canalNvo.toString();
                    String nomGrupoString = nomGrupo.toString();
                    String agteString = agte.toString();
                    String idAgenteString = idAgente.toString();
                    String nomAgteString = nomAgte.toString();
                    String nomEjecString = nomEjec.toString();
                    String nomRamoString = nomRamo.toString();
                    String secString = sec.toString();
                    String monString = mon.toString();
                    String aseguradoString = asegurado.toString();
                    String fecEmiString = fecEmi.toString();
                    String vigDesString = vigDes.toString();
                    String vigHasString = vigHas.toString();
                    String lineaNegString = lineaNeg.toString();
                    String pmaEmiString = String.valueOf(pmaEmi);
                    Double pmaEmiDouble = Double.parseDouble(pmaEmiString);
                    String pmaPagadaString = String.valueOf(pmaPagada);
                    Double pmaPagadaDouble =Double.parseDouble(pmaPagadaString);
                    String ordimpString = ordimp.toString();

                        Calendar now = Calendar.getInstance();
                        int anio = now.get(Calendar.YEAR);
                        String anioString = String.valueOf(anio);
                        Double anioDouble = Double.parseDouble(anioString);

                        listPrimPag.add(new    CargaBancaPrimPag(ofsucString, nomCanalString, codCanalString, canalNvoString,  nomGrupoString, agteString , idAgenteString, nomAgteString , nomEjecString,
                                 nomRamoString, idCentroEmisParts, numPolizaParts,numRenovPolParts, secString, monString,aseguradoString, fecEmiString,vigDesString,vigHasString,
                                 lineaNegString, pmaEmiDouble ,pmaPagadaDouble , ordimpString , anioDouble , mes , null));
                
                    rowIndex++;
                    if(rowIndex == numRow){
                        break;
                    }
                    arrayObjects = new Object[listPrimPag.size()];
                    int i = 0;
                    for(CargaBancaPrimPag primPag: listPrimPag){
                        Object[] arrayVector = new Object[]{
                            primPag.getOfsuc(),
                            primPag.getNomCanal(),
                            primPag.getCodCanal(),
                            primPag.getCanalNvo(),
                            primPag.getNomGrupo(),
                            primPag.getAgte(),
                            primPag.getIdAgente(),
                            primPag.getNomAgte(),
                            primPag.getNomEjec(),
                            primPag.getNomRamo(),
                            primPag.getIdCentroEmision(),
                            primPag.getNumPoliza(),
                            primPag.getNumRenovPoliz(),
                            primPag.getSec(),
                            primPag.getMon(),
                            primPag.getAsegurado(),
                            primPag.getFecEmi(),
                            primPag.getVigDes(),
                            primPag.getVigHas(),
                            primPag.getLineaNeg(),
                            primPag.getPmaEmi(),
                            primPag.getPmaPagada(),
                            primPag.getOrdimp(),
                            primPag.getAnio(),
                            primPag.getMes(),
                            primPag.getBancaPrimpagCpId()
                        };
                        arrayObjects[i] = arrayVector;
                        i = i +1;
                        
                    }
                    }//end validation blank
            }// end for

        }catch(Exception e){
            LOG.info("Informacion del Error : "+ e);
            fileUpload.delete();
        } finally{
            if(fileUpload != null){
                fileUpload.deleteOnExit();
            }
        }
        LOG.info("<< procesarFilePrimasPagadas()");
        return arrayObjects;
    }

    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void guardarPrimasPagSiniMes(Object[] listPrimPag,Object[] listSiniMes,Long caBancaPrimPagCP){
        ErrorBuilder eb = new ErrorBuilder();
        try{
            
            OracleConnection oracleCon = con.unwrap(OracleConnection.class);
            ArrayDescriptor arrayDescrPrimPag = ArrayDescriptor.createDescriptor("MIDAS.CA_COSTOPRIMAS_LIST", oracleCon);
            ArrayDescriptor arrayDescrSiniMes = ArrayDescriptor.createDescriptor("MIDAS.CA_SINIMES_LIST", oracleCon);
            ARRAY arrayPrimPag = new ARRAY(arrayDescrPrimPag, oracleCon, listPrimPag);
            ARRAY arraySiniMes = new ARRAY(arrayDescrSiniMes, oracleCon, listSiniMes);
            StringBuilder sb = new StringBuilder();
            sb.append(" BEGIN  " );
            sb.append(" MIDAS.PKG_CA_BANCA_CARGA.SP_CARGA_COSPRIM( " );
            sb.append(" CA_COSTOPRIMASLISTA => ?, ");
            sb.append(" CA_SINIMESLISTA => ?, ");
            sb.append(" pConfiguracion_Id => ?,");
            sb.append(" pId_Cod_Resp => ?,");
            sb.append(" pDesc_Resp => ?);" );
            sb.append(" END; ");
            LOG.info("Procedimiento a ejecutar : " + sb.toString());
            CallableStatement vectoresInsertSP = oracleCon.prepareCall(sb.toString());
            vectoresInsertSP.setArray(1, arrayPrimPag);
            vectoresInsertSP.setArray(2 ,arraySiniMes );
            vectoresInsertSP.setInt(3, caBancaPrimPagCP.intValue());
            vectoresInsertSP.registerOutParameter(4,OracleTypes.NUMBER);
            vectoresInsertSP.registerOutParameter(5,OracleTypes.VARCHAR);
            vectoresInsertSP.execute();
            BigDecimal codigoRespuestaSiniMes =(BigDecimal)vectoresInsertSP.getBigDecimal(4);
            String descripcionRespuestaSiniMes = vectoresInsertSP.getString(5);
            int numError = 1;
            BigDecimal errorBD = new BigDecimal(String.valueOf(numError));
            
            if(codigoRespuestaSiniMes == errorBD ){
                throw new Exception("Error al guardar los datos : "+ descripcionRespuestaSiniMes);
            }
            
            
        }catch(SQLException sqlEx){
            throw new ApplicationException(eb.addFieldError("guardarSiniestralidadMes",
                    "Error en la carga de Tarifas"+sqlEx));
        }catch(Exception e){
            LOG.info("Informacion del Error : " + e);
        }
    }
    
    class ResultadoCarga  {
        private List<String> erroresList;
        private BigDecimal insertados;
        private BigDecimal errores;
        private BigDecimal actualizados;
        
        public List<String> getErroresList() {
            return erroresList;
        }
        public void setErroresList(List<String> erroresList) {
            this.erroresList = erroresList;
        }
        public BigDecimal getInsertados() {
            return insertados;
        }
        public void setInsertados(BigDecimal insertados) {
            this.insertados = insertados;
        }
        public BigDecimal getErrores() {
            return errores;
        }
        public void setErrores(BigDecimal errores) {
            this.errores = errores;
        }
        public BigDecimal getActualizados() {
            return actualizados;
        }
        public void setActualizados(BigDecimal actualizados) {
            this.actualizados = actualizados;
        }
    }

    public DataSource getDataSource() {
        return dataSource;
    }


	

}
