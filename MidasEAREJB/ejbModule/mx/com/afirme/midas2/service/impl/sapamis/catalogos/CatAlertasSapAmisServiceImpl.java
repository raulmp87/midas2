package mx.com.afirme.midas2.service.impl.sapamis.catalogos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatAlertasSapAmis;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatAlertasSapAmisService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatSistemasService;

@Stateless
public class CatAlertasSapAmisServiceImpl implements CatAlertasSapAmisService{
	private static final long serialVersionUID = 1L;
	
	private EntidadService entidadService;
	private CatSistemasService catSistemasService;
	
	@Override
	public CatAlertasSapAmis completeObject(CatAlertasSapAmis catAlertasSapAmis) {
		CatAlertasSapAmis retorno = new CatAlertasSapAmis(); 
		if(catAlertasSapAmis != null){
			if(catAlertasSapAmis.getId().intValue() < 1){
				retorno = this.findIdByAttributes(catAlertasSapAmis);
			}else{
				if(!validateAttributes(catAlertasSapAmis)){
					retorno = this.findById(catAlertasSapAmis.getId());
				}
			}
		}else{
			retorno.setId(new Long(-1));
		}
		return retorno;
	}

	private CatAlertasSapAmis findById(long id) {
		CatAlertasSapAmis retorno = new CatAlertasSapAmis();
		if(id >= 0){
			retorno = entidadService.findById(CatAlertasSapAmis.class, id);
		}
		return retorno;
	}
	
	private boolean validateAttributes(CatAlertasSapAmis catAlertasSapAmis){
		boolean retorno = catAlertasSapAmis.getDescCatAlertasSapAmis() != null && !catAlertasSapAmis.getDescCatAlertasSapAmis().equals("");
		if(retorno){
			retorno = catAlertasSapAmis.getCatSistemas() != null;
		}
		return retorno;
	}

	private CatAlertasSapAmis findIdByAttributes(CatAlertasSapAmis catAlertasSapAmis) {
		if(validateAttributes(catAlertasSapAmis)){
			catAlertasSapAmis.setCatSistemas(catSistemasService.completeObject(catAlertasSapAmis.getCatSistemas()));
			if(catAlertasSapAmis.getCatSistemas().getId().intValue() > 0){
				Map<String,Object> parametros = new HashMap<String,Object>();
				parametros.put("descCatAlertasSapAmis", catAlertasSapAmis.getDescCatAlertasSapAmis());
				parametros.put("catSistemas", catAlertasSapAmis.getCatSistemas());
				List<CatAlertasSapAmis> catAlertasSapAmisList = entidadService.findByProperties(CatAlertasSapAmis.class, parametros);
				if(!catAlertasSapAmisList.isEmpty()){
					catAlertasSapAmis.setId(catAlertasSapAmisList.get(0).getId());
				}else{
					catAlertasSapAmis.setId(new Long(0));
				}
			}
		}else{
			catAlertasSapAmis.setId(new Long(-1));
		}
		return catAlertasSapAmis;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public CatAlertasSapAmis findByDesc(String desc) {
		CatAlertasSapAmis retorno = new CatAlertasSapAmis();
		if(desc != null && !desc.equals("")){
			List<CatAlertasSapAmis> list = entidadService.findByProperty(CatAlertasSapAmis.class, "descCatAlertasSapAmis", desc);
			if(list.isEmpty()){
				retorno = list.get(0);
			}else{
				retorno.setId(new Long(0));
				retorno.setDescCatAlertasSapAmis(desc);
			}
		}
		return retorno;
	}

	@EJB
	public void setCatSistemasService(CatSistemasService catSistemasService) {
		this.catSistemasService = catSistemasService;
	}
}