package mx.com.afirme.midas2.service.impl.tarea;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.GrupoParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.pkg.PkgAutGeneralesDao;
import mx.com.afirme.midas2.dao.tarea.EmisionPendienteDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente.ClaveTipoEndoso;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente.EstatusRegistro;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente.TipoEmision;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cancelacion.CancelacionBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoPolizaService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.MovimientoEndosoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;
import mx.com.afirme.midas2.service.tarea.EmisionPendienteService;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class EmisionPendienteServiceImpl implements EmisionPendienteService {
	
	@EJB
	private GrupoParametroGeneralFacadeRemote grupoParametroGeneral;
	
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneral;
	
	private static final String PROCESOS_AUTOMATICOS = "Procesos Automaticos";
	
	private static final String COD_PARAM_CANC_POL_DANIOS = "0001";
	
	private static final String COD_PARAM_CANC_END_DANIOS = "0002";
	
	private static final String COD_PARAM_CANC_POL_AUTOS = "0051";
	
	private static final String COD_PARAM_CANC_END_AUTOS = "0052";	
	
	@Resource	
	private TimerService timerService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	public static final Logger LOG = Logger.getLogger(EmisionPendienteServiceImpl.class);
			
	private boolean habilitado(String codigoProceso){
		boolean habilitado = true;					
		List<ParametroGeneralDTO> parametros =  parametroGeneral.findByProperty(
				"grupoParametroGeneral.descripcionGrupoParametroGral", PROCESOS_AUTOMATICOS);		
		for(ParametroGeneralDTO parametro : parametros){
			if(parametro.getId().getCodigoParametroGeneral().toString().equals(
					parametro.getId().getIdToGrupoParametroGeneral().toString().concat(codigoProceso))){ //generar cadena que representa el codigo el parametro
				habilitado = Integer.valueOf(parametro.getValor()).intValue() == 1 ? true : false;
				break;
			}
		}
		return habilitado;		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesaBajaIncisoPerdidaTotalAutos() {
		LOG.info("Ejecutando tarea procesaBajaIncisoPerdidaTotalAutos...");
		procesaEmisionesPendientes(TipoEmision.BAJA_INCISO_PERDIDA_TOTAL_AUTOS);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesaCancelacionEndosoAutos(int dias) {
		LOG.info("Ejecutando tarea procesaCancelacionEndosoAutos...");
		Date fechaActual = Calendar.getInstance().getTime();
		Date fecha = restaDias(fechaActual, dias);		
		if(habilitado(COD_PARAM_CANC_END_AUTOS)){
			//procesaEmisionesPendientes(TipoEmision.CANC_END_AUTOS, fecha);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesaCancelacionEndosoDanios() {
		procesaEmisionesPendientes(TipoEmision.CANC_END_DANIOS);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesaCancelacionPolizaAutos(int dias) {
		LOG.info("Ejecutando tarea procesaCancelacionPolizaAutos...");
		Date fechaActual = Calendar.getInstance().getTime();
		Date fecha = restaDias(fechaActual, dias);		
		if(habilitado(COD_PARAM_CANC_POL_AUTOS)){
			//procesaEmisionesPendientes(TipoEmision.CANC_POL_AUTOS, fecha);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesaCancelacionPolizaDanios() {
		procesaEmisionesPendientes(TipoEmision.CANC_POL_DANIOS);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesaRehabilitacionEndosoAutos() {
		LOG.info("Ejecutando tarea procesaRehabilitacionEndosoAutos...");
		procesaEmisionesPendientes(TipoEmision.REHAB_END_AUTOS);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesaRehabilitacionEndosoDanios() {
		procesaEmisionesPendientes(TipoEmision.REHAB_END_DANIOS);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesaRehabilitacionIncisoAutos() {
		LOG.info("Ejecutando tarea procesaRehabilitacionIncisoAutos...");
		procesaEmisionesPendientes(TipoEmision.REHAB_INCISO_AUTOS);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesaRehabilitacionPolizaAutos() {
		LOG.info("Ejecutando tarea procesaRehabilitacionPolizaAutos...");
		procesaEmisionesPendientes(TipoEmision.REHAB_POL_AUTOS);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesaRehabilitacionPolizaDanios() {
		procesaEmisionesPendientes(TipoEmision.REHAB_POL_DANIOS);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesaCancelacionIncisoAutos() {
		LOG.info("Ejecutando tarea procesaCancelacionIncisoAutos...");
		procesaEmisionesPendientes(TipoEmision.CANC_INCISO_AUTOS);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long obtenerTotalPaginacionCancelacionesPendientesAutos(
			EmisionPendiente filtro) 
	{
		Long total = 0L;

		// Se actualiza la tabla emisiones pendientes

		// Esto es para evitar la invocacion del SP de Seycos cuando no sea una
		// busqueda explicita
		if (filtro != null && filtro.getAuxiliarFlag() != null
				&& filtro.getAuxiliarFlag().trim().equals("1")) {
			if (filtro.getTipoEmision() == null) {
				pkgAutGeneralesDao.updateEmisionPendiente(
						TipoEmision.CANC_POL_AUTOS.getTipoEmision(), false,
						filtro.getFechaValidacion());
				pkgAutGeneralesDao.updateEmisionPendiente(TipoEmision.CANC_END_AUTOS.getTipoEmision(),
						false, filtro.getFechaValidacion());
			} else {
				pkgAutGeneralesDao.updateEmisionPendiente(
						filtro.getTipoEmision(), false,
						filtro.getFechaValidacion());
			}
		}

		total += emisionPendienteDao
				.obtenerTotalPaginacionCancelacionesPendientesAutos(filtro);

		return total;
	}
	
	@Override
	public List<EmisionPendiente> buscarCancelacionesPendientesAutos(EmisionPendiente filtro) {
		if(filtro == null) {
			return null;
		}
		return emisionPendienteDao.buscarCancelacionesPendientesAutos(filtro);
	}
	
	@Override
	public TransporteImpresionDTO getExcelCancelacionesPendientesAutos(EmisionPendiente filtro) {
		
		List<EmisionPendiente> emisionesPendientes = this.buscarCancelacionesPendientesAutos(filtro);
		
		return impresionesService.getExcel(emisionesPendientes, 
				"midas.endoso.previo.emisionpendiente.cancelacion.plantilla.archivo.nombre",ConstantesReporte.TIPO_XLS);
		
	}
	
	
	@Override
	public void actualizaBloqueo (Long emisionPendienteId, Short bloqueo) {
		
		EmisionPendiente emisionPendiente = entidadService.findById(EmisionPendiente.class, emisionPendienteId);
		
		if (emisionPendiente != null && bloqueo != null) {
			
			emisionPendiente.setBloqueoEmision(bloqueo);
			
			entidadService.save(emisionPendiente);
			
		}
		
		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void emitePendiente (Long emisionPendienteId) {
		EmisionPendiente emisionPendiente = entidadService.findById(EmisionPendiente.class, emisionPendienteId);
		procesaEmisionPendiente (emisionPendiente);
	}
	
	private void procesaEmisionPendiente (EmisionPendiente emisionPendiente) {
		//Aqui se asume que es una emision pendiente con estatus Registro 0 (SIN PROCESAR)
		
		//Solo se procesa si la emision no ha sido bloqueada por el usuario
		if (emisionPendiente.getBloqueoEmision().shortValue() == 0) {
		
			EstatusRegistro nuevoEstatusRegistro = null;
			
			TipoEmision tipoEmision = EmisionPendiente.tipoEmisionMap.get(emisionPendiente.getTipoEmision());
			
			try {
				actualizaEstatus(emisionPendiente, EstatusRegistro.EN_PROCESO);
				
				this.removeInProcessForEndosoAutomatico(emisionPendiente.getIdToPoliza());
				
				//Se procesa la emision pendiente
				switch (tipoEmision) {
					case BAJA_INCISO_PERDIDA_TOTAL_AUTOS: {
						emiteBajaIncisoPerdidaTotalAutos(emisionPendiente);
						break;
					}
					case CANC_POL_AUTOS: {
						emiteCancelacionPolizaAutos(emisionPendiente);
						break;						
					}
					case REHAB_POL_AUTOS: {
						emiteRehabilitacionPolizaAutos(emisionPendiente);
						break;
					}
					case CANC_INCISO_AUTOS: {
						//Tiene el mismo comportamiento que Baja de inciso por Perdida Total
						emiteBajaIncisoPerdidaTotalAutos(emisionPendiente);
						break;
					}
					case REHAB_INCISO_AUTOS: {
						emiteRehabilitacionInciso(emisionPendiente);
						break;
					}	
					case CANC_END_AUTOS: {
						emiteCancelacionEndosoAutos(emisionPendiente);
						break;
					}
					case REHAB_END_AUTOS: {
						emiteRehabilitacionEndosoAutos(emisionPendiente);
						break;
					}
				}
				
				nuevoEstatusRegistro = EstatusRegistro.PROCESADO;
				
			} catch (Exception ex) {
				//TODO Implementar envio de correo para notificar el error
				nuevoEstatusRegistro = EstatusRegistro.EMISION_FALLO;
			} finally {
				actualizaEstatus(emisionPendiente, nuevoEstatusRegistro);
			}
		}
		
	}

	private void procesaEmisionesPendientes(TipoEmision tipoEmision) {
		procesaEmisionesPendientes(tipoEmision, new Date());
	}
	
	private void procesaEmisionesPendientes(TipoEmision tipoEmision, Date date) {
		
		//Se actualiza la tabla emisiones pendientes
		if (tipoEmision.getTipoEmision() != TipoEmision.BAJA_INCISO_PERDIDA_TOTAL_AUTOS.getTipoEmision()) {
			pkgAutGeneralesDao.updateEmisionPendiente(tipoEmision.getTipoEmision(), true);
		}	
		
		//Obtiene de la tabla temporal todos los registros con estatus SIN_PROCESAR
		EmisionPendiente filtro = new EmisionPendiente();
		filtro.setTipoEmision(tipoEmision.getTipoEmision());
		filtro.setFechaValidacion(new Date());
		filtro.setBloqueoEmision((short)0); //no bloqueadas
		
		List<EmisionPendiente> emisionesPendientes = emisionPendienteDao.buscarEmisionesPendientes(filtro);		
		for (EmisionPendiente emisionPendiente : emisionesPendientes) {
			procesaEmisionPendiente (emisionPendiente);
		}
		
	}
	
	private void actualizaEstatus(EmisionPendiente emisionPendiente, EstatusRegistro estatusRegistro) {
		emisionPendiente.setEstatusRegistro(estatusRegistro.getEstatusRegistro());
		emisionPendiente.setBloqueoEmision((short)0);
		entidadService.save(emisionPendiente);
	}
	
	
	@SuppressWarnings("unused")
	private void emiteBajaIncisoPerdidaTotalAutos(EmisionPendiente emisionPendiente) {
		
		short claveMotivoEndoso = (emisionPendiente.getMigrada() == EmisionPendiente.POLIZA_MIGRADA && emisionPendiente
				.getClaveMotivoEndoso() != null) ? emisionPendiente
				.getClaveMotivoEndoso()
				: SolicitudDTO.CVE_MOTIVO_ENDOSO_PERDIDA_TOTAL;				
		
		Boolean emiteRecibos = (emisionPendiente.getMigrada() == EmisionPendiente.POLIZA_MIGRADA)?false:true;
		
		DateTime validoEn = TimeUtils.getDateTime(emisionPendiente.getFechaInicioVigencia());
		BitemporalCotizacion cotizacion = endosoService.getCotizacionEndosoBajaIncisoPerdidaTotal(emisionPendiente.getIdToPoliza(), 
				emisionPendiente.getFechaInicioVigencia(), TipoAccionDTO.getNuevoEndosoCot(),claveMotivoEndoso, emisionPendiente.getMigrada());
		
		//Si hay mas de un inciso se emite como Baja de Inciso por Perdida Total
		if (cotizacion.getEntidadContinuity().getIncisoContinuities().get(validoEn).size() > 1) {
			
			if (emisionPendiente.getNumeroInciso() == null
					|| emisionPendiente.getNumeroInciso().isEmpty()) {
				throw new RuntimeException("No hay inciso al cual dar de Baja");
			}

			String[] numerosIncisos = null;
			String cadenaIncisos = emisionPendiente.getNumeroInciso().replaceAll(" ", "");
			numerosIncisos = cadenaIncisos.split(",");
		
			String[] incisoContinuitiesString = null;
			StringBuffer continuidadesIds = new StringBuffer();
			String continuidadesIdSplit = null;
			for (String numeroIncisoString : numerosIncisos) 
			{
				Map<String, Object> parametros = new LinkedHashMap<String, Object>();
				parametros.put("cotizacionContinuity.id", cotizacion
						.getEntidadContinuity().getId());
				parametros.put("numero", Integer.valueOf(numeroIncisoString));
				List<IncisoContinuity> incisoContinuities = entidadService
						.findByProperties(IncisoContinuity.class, parametros);

				if (incisoContinuities != null
						&& incisoContinuities.size() > 0
						&& incisoContinuities.get(0).getBitemporalProperty()
								.get(validoEn) != null) 
				{
					continuidadesIds.append(incisoContinuities.get(0).getId().toString());
					continuidadesIds.append(",");
				}
			}
			
			if(continuidadesIds!=null){
				continuidadesIdSplit = continuidadesIds.toString().substring(0,continuidadesIds.toString().lastIndexOf(","));
				incisoContinuitiesString = continuidadesIdSplit.split(",");
				if (incisoContinuitiesString != null
						&& incisoContinuitiesString.length > 0) {
	
					endosoService.guardaCotizacionEndosoBajaInciso(
							incisoContinuitiesString,
							cotizacion);
					
					calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
							validoEn);
					emisionEndosoBitemporalService.emiteEndoso(cotizacion.getEntidadContinuity().getId(), validoEn, 
							SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO, emiteRecibos);
	
				}
			} else {
				throw new RuntimeException("No hay inciso al cual dar de Baja");
			}
			
		} else {//De lo contrario se emite como cancelacion de Poliza
			emiteCancelacionPolizaAutos(emisionPendiente);
		}		
	}
	
	private void emiteCancelacionPolizaAutos(EmisionPendiente emisionPendiente) {

		short claveMotivoEndoso = (emisionPendiente.getMigrada() == EmisionPendiente.POLIZA_MIGRADA && emisionPendiente
				.getClaveMotivoEndoso() != null) ? emisionPendiente
				.getClaveMotivoEndoso()
				: SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO;
				
		Boolean emiteRecibos = (emisionPendiente.getMigrada() == EmisionPendiente.POLIZA_MIGRADA)?false:true;

		BitemporalCotizacion cotizacion = null;

		DateTime validoEn = TimeUtils.getDateTime(emisionPendiente
				.getFechaInicioVigencia());

		cotizacion = cancelacionBitemporalService
				.guardaCotizacionCancelacionPolizaAutomatico(
						emisionPendiente.getIdToPoliza(),
						emisionPendiente.getNumeroEndoso().shortValue(),
						emisionPendiente.getFechaInicioVigencia(),
						SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA,
						claveMotivoEndoso,
						emisionPendiente.getMigrada());

		emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity()
				.getId(), validoEn,
				SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA,emiteRecibos);
	}   
	
	private void emiteRehabilitacionPolizaAutos(EmisionPendiente emisionPendiente) {
		
		short claveMotivoEndoso = (emisionPendiente.getMigrada() == EmisionPendiente.POLIZA_MIGRADA && emisionPendiente
				.getClaveMotivoEndoso() != null) ? emisionPendiente
				.getClaveMotivoEndoso()
				: SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA;
				
				Boolean emiteRecibos = (emisionPendiente.getMigrada() == EmisionPendiente.POLIZA_MIGRADA)?false:true;
		
		endosoPolizaService.guardaCotizacionEndosoRehabilitacionPolizaAutomatica(
				emisionPendiente.getIdToPoliza(), emisionPendiente.getFechaInicioVigencia(), TipoAccionDTO.getNuevoEndosoCot(),
				claveMotivoEndoso, emisionPendiente.getMigrada());
		
		DateTime validoEn = TimeUtils.getDateTime(emisionPendiente.getFechaInicioVigencia());
		
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, emisionPendiente.getIdToPoliza());
		
		BitemporalCotizacion cotizacion = entidadBitemporalService
			.getInProcessByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, 
					poliza.getCotizacionDTO().getIdToCotizacion(), validoEn);
		
		calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
				validoEn);
		
		emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(), validoEn,
				SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA,emiteRecibos);		
	}
	
	@SuppressWarnings("unused")
	private void emiteRehabilitacionInciso(EmisionPendiente emisionPendiente) {  
		
		short claveMotivoEndoso = (emisionPendiente.getMigrada() == EmisionPendiente.POLIZA_MIGRADA && emisionPendiente
				.getClaveMotivoEndoso() != null) ? emisionPendiente
				.getClaveMotivoEndoso()
				: SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS;
				
		Boolean emiteRecibos = (emisionPendiente.getMigrada() == EmisionPendiente.POLIZA_MIGRADA)?false:true;
		
		DateTime validoEn = TimeUtils.getDateTime(emisionPendiente.getFechaInicioVigencia());
		BitemporalCotizacion cotizacion = endosoService.getCotizacionEndosoRehabilitacionInciso(emisionPendiente.getIdToPoliza(),
				emisionPendiente.getFechaInicioVigencia(),TipoAccionDTO.getNuevoEndosoCot(), claveMotivoEndoso);		
		
		List<BitemporalInciso> listaIncisosCotizacion = new ArrayList<BitemporalInciso>(1);
		listaIncisosCotizacion = listadoIncisosDinamicoService.buscarIncisosFiltrado(new IncisoCotizacionDTO(), SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS, emisionPendiente.getIdToPoliza().longValue(), validoEn.toDate(), 0);
		
		List<BitemporalInciso> listaIncisosARehabilitar = new ArrayList<BitemporalInciso>();
		if (emisionPendiente.getNumeroInciso() == null
				|| emisionPendiente.getNumeroInciso().isEmpty()) {
			throw new RuntimeException("No hay inciso al cual Rehabilitar");
		}

		String[] numerosIncisos = null;
		String[] incisoContinuitiesString = null;
		String cadenaIncisos = emisionPendiente.getNumeroInciso().replaceAll(" ", "");
		numerosIncisos = cadenaIncisos.split(",");
	
	
		StringBuffer continuidadesIds = new StringBuffer();
		String continuidadesIdSplit = null;
		for (String numeroIncisoString : numerosIncisos) 
		{
			Iterator<BitemporalInciso> itListaIncisosCotizacion = listaIncisosCotizacion.iterator();
			while(itListaIncisosCotizacion.hasNext())
			{
				BitemporalInciso bitemporalInciso = itListaIncisosCotizacion.next();
				if(bitemporalInciso.getContinuity().getNumero() == Integer.valueOf(numeroIncisoString))
				{
					continuidadesIds.append(bitemporalInciso.getContinuity().getId().toString());
					continuidadesIds.append(",");
					listaIncisosARehabilitar.add(bitemporalInciso);				
					break;				
				}
			}			
		}
	
		if(continuidadesIds.length()<0){
			continuidadesIdSplit = continuidadesIds.toString().substring(0,continuidadesIds.toString().lastIndexOf(","));
			incisoContinuitiesString = continuidadesIdSplit.split(",");
			endosoService.guardaCotizacionEndosoRehabilitacionInciso(incisoContinuitiesString, listaIncisosARehabilitar, cotizacion);
			calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
					validoEn);
			emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(), validoEn, SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS, emiteRecibos);
		}
	}
	
	private void emiteCancelacionEndosoAutos(EmisionPendiente emisionPendiente) {  
		
		short claveMotivoEndoso = (emisionPendiente.getMigrada() == EmisionPendiente.POLIZA_MIGRADA && emisionPendiente
				.getClaveMotivoEndoso() != null) ? emisionPendiente
				.getClaveMotivoEndoso()
				: SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO;
				
		Boolean emiteRecibos = (emisionPendiente.getMigrada() == EmisionPendiente.POLIZA_MIGRADA)?false:true;
		int numeroEndoso = 0;
		short esMigrada = ((!emiteRecibos)?(short)1:(short)0);
		if(esMigrada == EmisionPendiente.POLIZA_MIGRADA){
			numeroEndoso = (int)(emisionPendiente.getNumeroEndoso()%1000);
		}else{
			numeroEndoso = emisionPendiente.getNumeroEndoso().intValue();
		}
		
		DateTime validoEn = TimeUtils.getDateTime(emisionPendiente.getFechaInicioVigencia());
		
		PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, emisionPendiente.getIdToPoliza());
		
		BitemporalCotizacion cotizacion = endosoService.prepareCotizacionEndosoCancelacion(polizaDTO, 
											SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO,
											validoEn, 
											TipoAccionDTO.getNuevoEndosoCot(), new Long(numeroEndoso), claveMotivoEndoso, esMigrada);		
		
		endosoService.guardaCotizacionCancelacionEndoso(cotizacion, validoEn, emisionPendiente.getIdToPoliza(),
				 (short)numeroEndoso, TipoAccionDTO.getNuevoEndosoCot(), claveMotivoEndoso);
		calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
				validoEn);
		
		emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(), validoEn, claveMotivoEndoso, emiteRecibos);
		
	}
	
	private void emiteRehabilitacionEndosoAutos(EmisionPendiente emisionPendiente) {  
		
		short claveMotivoEndoso = (emisionPendiente.getMigrada() == EmisionPendiente.POLIZA_MIGRADA && emisionPendiente
				.getClaveMotivoEndoso() != null) ? emisionPendiente
				.getClaveMotivoEndoso()
				: SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO;
				
		Boolean emiteRecibos = (emisionPendiente.getMigrada() == EmisionPendiente.POLIZA_MIGRADA)?false:true;
		
		short esMigrada = ((!emiteRecibos)?(short)1:(short)0);
		
		DateTime validoEn = TimeUtils.getDateTime(emisionPendiente.getFechaInicioVigencia());
		
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, emisionPendiente.getIdToPoliza());
		
		BitemporalCotizacion cotizacion = endosoService.prepareCotizacionEndosoCancelacion(poliza, 
											SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO,
											validoEn, 
											TipoAccionDTO.getNuevoEndosoCot(), emisionPendiente.getNumeroEndoso(), claveMotivoEndoso, esMigrada);		
		
		
		endosoService.guardaCotizacionRehabilitacionEndoso(cotizacion, emisionPendiente.getIdToPoliza(), 
				validoEn, emisionPendiente.getNumeroEndoso().shortValue(), TipoAccionDTO.getNuevoEndosoCot(), claveMotivoEndoso); 
		
		calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
				validoEn);
		
		emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(), validoEn, SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO, emiteRecibos);
		
	}
	
	
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void removeInProcessForEndosoAutomatico(BigDecimal polizaId) {		

		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, polizaId);
		Map<String, Object> values = new LinkedHashMap<String, Object>();
        values.put("cotizacionId", poliza.getCotizacionDTO().getIdToCotizacion());
    
        List<ControlEndosoCot> controlEndosos = entidadService.findByProperties(ControlEndosoCot.class,values);
		ControlEndosoCot controlEndoso = null;
		if(controlEndosos !=null && !controlEndosos.isEmpty()){
			controlEndoso = Collections.max(controlEndosos, new Comparator<ControlEndosoCot>() {
				public int compare(ControlEndosoCot e1, ControlEndosoCot e2){
					return e1.getId().compareTo(e2.getId());
				} 
			});
		}
		
		if(controlEndoso!=null && controlEndoso.getClaveEstatusCot().shortValue() != CotizacionDTO.ESTATUS_COT_EMITIDA){
			 movimientoEndosoBitemporalService.borrarMovimientos(controlEndoso);
				entidadService.remove(controlEndoso);		
		}
	
		//entidadContinuityDao.rollBackContinuity(cotizaciones.get(0).getId());
			
	}	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesaEndosoMigracion(Long idCotizacion, String claveTipoEndoso)
	{
		
		PolizaDTO poliza = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", new BigDecimal(idCotizacion)).get(0);
		
		Short tipoEmision = null;
				
		if(claveTipoEndoso.equalsIgnoreCase(ClaveTipoEndoso.CLAVETIPOENDOSO_CAP.getClaveTipoEndoso())||claveTipoEndoso.equalsIgnoreCase(ClaveTipoEndoso.CLAVETIPOENDOSO_CPP.getClaveTipoEndoso())){
			tipoEmision = EmisionPendiente.TipoEmision.CANC_POL_AUTOS.getTipoEmision();
		}
		else if (claveTipoEndoso.equalsIgnoreCase(ClaveTipoEndoso.CLAVETIPOENDOSO_REPP.getClaveTipoEndoso())||claveTipoEndoso.equalsIgnoreCase(ClaveTipoEndoso.CLAVETIPOENDOSO_REAP.getClaveTipoEndoso())){
			tipoEmision = EmisionPendiente.TipoEmision.REHAB_POL_AUTOS.getTipoEmision();
		}
		else if (claveTipoEndoso.equalsIgnoreCase(ClaveTipoEndoso.CLAVETIPOENDOSO_BIN.getClaveTipoEndoso())||claveTipoEndoso.equalsIgnoreCase(ClaveTipoEndoso.CLAVETIPOENDOSO_CAI.getClaveTipoEndoso())){
			tipoEmision = EmisionPendiente.TipoEmision.BAJA_INCISO_PERDIDA_TOTAL_AUTOS.getTipoEmision();
		}
		else if (claveTipoEndoso.equalsIgnoreCase(ClaveTipoEndoso.CLAVETIPOENDOSO_CAE.getClaveTipoEndoso())||claveTipoEndoso.equalsIgnoreCase(ClaveTipoEndoso.CLAVETIPOENDOSO_CEP.getClaveTipoEndoso())){
			tipoEmision = EmisionPendiente.TipoEmision.CANC_END_AUTOS.getTipoEmision();
		}
		else if (claveTipoEndoso.equalsIgnoreCase(ClaveTipoEndoso.CLAVETIPOENDOSO_REAE.getClaveTipoEndoso())){
			tipoEmision = EmisionPendiente.TipoEmision.REHAB_END_AUTOS.getTipoEmision();
		}
		else if (claveTipoEndoso.equalsIgnoreCase(ClaveTipoEndoso.CLAVETIPOENDOSO_REIC.getClaveTipoEndoso())){
			tipoEmision = EmisionPendiente.TipoEmision.REHAB_INCISO_AUTOS.getTipoEmision();
		}
		
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("idToPoliza", poliza.getIdToPoliza());
		params.put("migrada", EmisionPendiente.POLIZA_MIGRADA);
		params.put("estatusRegistro",EstatusRegistro.SIN_PROCESAR.getEstatusRegistro());		
		params.put("tipoEmision", tipoEmision);		
		
		EmisionPendiente emisionPendiente = entidadService.findByProperties(EmisionPendiente.class, params).get(0);
		
		this.removeInProcessForEndosoAutomatico(emisionPendiente.getIdToPoliza());
		
		EstatusRegistro nuevoEstatusRegistro = null;
		
		try {
			
			actualizaEstatus(emisionPendiente, EstatusRegistro.EN_PROCESO);
		
			if(tipoEmision == EmisionPendiente.TipoEmision.CANC_POL_AUTOS.getTipoEmision())
			{
				emiteCancelacionPolizaAutos(emisionPendiente);			
			}
			else if(tipoEmision == EmisionPendiente.TipoEmision.REHAB_POL_AUTOS.getTipoEmision())
			{
				emiteRehabilitacionPolizaAutos(emisionPendiente);			
			}
			else if(tipoEmision == EmisionPendiente.TipoEmision.CANC_END_AUTOS.getTipoEmision())
			{
				emiteCancelacionEndosoAutos(emisionPendiente);			
			}
			else if(tipoEmision == EmisionPendiente.TipoEmision.BAJA_INCISO_PERDIDA_TOTAL_AUTOS.getTipoEmision())
			{
				emiteBajaIncisoPerdidaTotalAutos(emisionPendiente);			
			}
			else if(tipoEmision == EmisionPendiente.TipoEmision.REHAB_END_AUTOS.getTipoEmision())
			{
				emiteRehabilitacionEndosoAutos(emisionPendiente);			
			}
			else if(tipoEmision == EmisionPendiente.TipoEmision.REHAB_INCISO_AUTOS.getTipoEmision())
			{
				emiteRehabilitacionInciso(emisionPendiente);			
			}
			nuevoEstatusRegistro = EstatusRegistro.PROCESADO;
		
		} catch (Exception ex) {
			
			nuevoEstatusRegistro = EstatusRegistro.EMISION_FALLO;
			throw new RuntimeException(ex);
			
		} finally {
			actualizaEstatus(emisionPendiente, nuevoEstatusRegistro);
		}
	}
	
	@Override
	public Map<String, Long> monitorearAvanceEmisionPendiente(EmisionPendiente filtro)
	{
		Map<String, Long> cifras = new HashMap<String, Long>();
		
		Long totalRegistros = 0L;
		Long registrosPendientesProcesar = 0L;
		Long registrosProcesadosExito = 0L;
		Long registrosProcesadosFallo = 0L;
		
		List<Object[]> resultados = null;
		resultados = emisionPendienteDao.monitorearEmisionPendiente(filtro);
		
		for (Object[] result : resultados) 
		{
			short estatus = (Short)result[0];
			long cantidadRegistros = (Long)result[1];
			
			totalRegistros = totalRegistros + cantidadRegistros;
			
			if(estatus == EmisionPendiente.EstatusRegistro.SIN_PROCESAR.getEstatusRegistro())
			{
				registrosPendientesProcesar += cantidadRegistros;				
			}
			else if(estatus == EmisionPendiente.EstatusRegistro.PROCESADO.getEstatusRegistro())
			{
				registrosProcesadosExito += cantidadRegistros;
				
			}else if(estatus == EmisionPendiente.EstatusRegistro.EMISION_FALLO.getEstatusRegistro())
			{
				registrosProcesadosFallo += cantidadRegistros;				
			}		    
		} 
		
		cifras.put("TOTAL_REGISTROS", totalRegistros);
		cifras.put("SIN_PROCESAR", registrosPendientesProcesar);
		cifras.put("PROCESADO_EXITO", registrosProcesadosExito);
		cifras.put("PROCESADO_FALLO", registrosProcesadosFallo);
		
		return cifras;		
	}	
	
	public void generarEndosoPerdidaTotalAutomatico(BigDecimal idToPoliza, String tipoIndemnizacion, Date fechaInicioVigencia, 
			IncisoContinuity incisoContinuity, Long idSiniestroPT)
	{
		Short tipoEndoso; 
		
		//se obtiene la poliza
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class,
				idToPoliza);
		
		tipoEndoso = endosoService.getCotizacionEndosoPerdidaTotal(idToPoliza, fechaInicioVigencia, null);
		  
		//Nota: el tipo de endoso no se proporciona al servicio, para establecer la bandera bloquearFlujo en false.
		BitemporalCotizacion cotizacion = endosoService.prepareCotizacionEndosoPerdidaTotal(idToPoliza, fechaInicioVigencia, 
				null, tipoIndemnizacion, TipoAccionDTO.getNuevoEndosoCot(), incisoContinuity);
		
		String[] continuitiesStringIds ={String.valueOf(incisoContinuity.getId())};
		
		if(tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL)
		{
			endosoService.guardaCotizacionEndosoBajaInciso(continuitiesStringIds, cotizacion);
			
		}else if(tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL)
		{
			endosoPolizaService.guardaCotizacionEndosoCancelacionPolizaPTAutomatico(cotizacion,continuitiesStringIds,cotizacion.getValue().getSolicitud().getIdToSolicitud(), 
					fechaInicioVigencia);
		}
				
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndosoPerdidaTotal(cotizacion.getContinuity().getId(),
				TimeUtils.getDateTime(fechaInicioVigencia),cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue(),
				idSiniestroPT);		
		
	}
	
	public void generarEndosoDesagrupacionRecibosAutomatico(BigDecimal idToPoliza, Date fechaInicioVigencia, IncisoContinuity incisoContinuity)
	{
		//se obtiene la poliza
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class,
				idToPoliza);
				
		endosoService.getCotizacionEndosoDesagrupacionRecibos(idToPoliza, 
				fechaInicioVigencia,SolicitudDTO.CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS);
		
		BitemporalCotizacion cotizacion = endosoService.prepareCotizacionEndosoDesagrupacionRecibos(idToPoliza, 
				fechaInicioVigencia,TipoAccionDTO.getNuevoEndosoCot());
		
		BitemporalAutoInciso bitemporalAutoInciso = incisoViewService.getAutoIncisoInProcess(incisoViewService.getIncisoInProcess(incisoContinuity.getId(), 
				fechaInicioVigencia), fechaInicioVigencia);
		
		endosoService.guardaCotizacionEndosoDesagrupacionRecibosAutomatica(poliza, cotizacion, incisoContinuity, bitemporalAutoInciso);
		
		EndosoDTO endosoDesagrupacion = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),
				TimeUtils.getDateTime(fechaInicioVigencia),cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());		
	}
		
	private Date restaDias(Date fechaActual, int diferenciaEnDias){	
		long tiempoActual = fechaActual.getTime();
		long unDia = diferenciaEnDias * 24 * 60 * 60 * 1000;
		return new Date(tiempoActual - unDia);		
	}
	
	public void initialize() {
		String timerInfo = "TimerEmiteEndososBajaPerdidaTotal";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0 2 * * ?
				expression.minute(0);
				expression.hour(2);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerEmiteEndososBajaPerdidaTotal", false));
				
				LOG.info("Tarea TimerEmiteEndososBajaPerdidaTotal configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerEmiteEndososBajaPerdidaTotal");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerEmiteEndososBajaPerdidaTotal:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		procesaBajaIncisoPerdidaTotalAutos();
	}
		
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private EndosoService endosoService;
	
	@EJB
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	
	@EJB
	private CancelacionBitemporalService cancelacionBitemporalService;
	
	@EJB
	private EndosoPolizaService endosoPolizaService;
	
	@EJB
	private EntidadBitemporalService entidadBitemporalService;
	
	@EJB
	private ListadoIncisosDinamicoService listadoIncisosDinamicoService;
	
	@EJB
	private CalculoService calculoService;
	
	@EJB 
	private PkgAutGeneralesDao pkgAutGeneralesDao;
	
	@EJB
	private EmisionPendienteDao emisionPendienteDao;
	
	@EJB
	protected EntidadContinuityDao entidadContinuityDao;	
	
	@EJB
	protected MovimientoEndosoBitemporalService movimientoEndosoBitemporalService;
	
	@EJB
	private ImpresionesService impresionesService;
	
	@EJB
    private IncisoViewService incisoViewService;
}
