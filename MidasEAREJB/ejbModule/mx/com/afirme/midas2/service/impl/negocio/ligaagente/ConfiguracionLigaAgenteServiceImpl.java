package mx.com.afirme.midas2.service.impl.negocio.ligaagente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.sistema.seguridad.AtributoUsuario;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.negocio.ligaagente.ConfiguracionLigaAgenteDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.ligaagente.ConfiguracionLigaAgente;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina.EnumModoEnvio;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.negocio.ligaagente.ConfigLigaAgenteDTO;
import mx.com.afirme.midas2.dto.negocio.ligaagente.MonitoreoLigaAgenteDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.ligaagente.ConfiguracionLigaAgenteService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService.LoginParameter;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EmailDestinaratios;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;

import org.apache.log4j.Logger;

import com.asm.service.ASMConstants;

@Stateless
public class ConfiguracionLigaAgenteServiceImpl implements ConfiguracionLigaAgenteService{
	
	private static final Logger log = Logger.getLogger(ConfiguracionLigaAgenteServiceImpl.class);
	
	public static final String FN_M2_Monitorear_Todos_Los_Agentes = "FN_M2_Monitorear_Todos_Los_Agentes";
	
	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private ConfiguracionLigaAgenteDao configuracionLigaAgenteDao;
	
	@EJB
	private EnvioNotificacionesService notificacionService;
	
	@EJB
	private ParametroGlobalService parametroGlobalService;
	
	@EJB
	private GenerarPlantillaReporte generarPlantillaReporte;
	
	@Override
	public List<ConfigLigaAgenteDTO> buscarConfiguracionesLigasAgentes(ConfigLigaAgenteDTO cfgLigaFiltro){
		List<ConfigLigaAgenteDTO> cfgsLigasAgentes = new ArrayList<ConfigLigaAgenteDTO>();
		
		cfgsLigasAgentes = configuracionLigaAgenteDao.buscarConfiguracionesLigasAgentes(cfgLigaFiltro);
//		crearConfiguracionesDummy();
		
		return cfgsLigasAgentes;
	}
	
	@Override
	public List<MonitoreoLigaAgenteDTO> buscarMonitoreoLigasAgentes(MonitoreoLigaAgenteDTO monitorLigaFiltro){
		return configuracionLigaAgenteDao.buscarMonitoreoLigasAgentes(monitorLigaFiltro);
	}
	
	@Override
	public List<MonitoreoLigaAgenteDTO> buscarMonitoreoLigasAgentesPaginado(MonitoreoLigaAgenteDTO monitorLigaFiltro){
		return configuracionLigaAgenteDao.buscarMonitoreoLigasAgentesPaginado(monitorLigaFiltro);
	}
	
	@Override
	public MonitoreoLigaAgenteDTO contarResultadoMonitoreoLigasAgentes(MonitoreoLigaAgenteDTO monitorLigaFiltro){
		return configuracionLigaAgenteDao.buscarMonitoreoLigasAgentesContador(monitorLigaFiltro);
	}
	
	@Override
	public ConfiguracionLigaAgente guardarConfiguracionLigaAgente(ConfiguracionLigaAgente configuracion){
		if(configuracion != null){
			if(configuracion.getId() == null
					&& existeConfiguracionAgenteNegocio(configuracion)){
				throw new NegocioEJBExeption("LigaAgenteEx","Ya existe una liga para el agente y el negocio elegidos");
			}
			String usuario = usuarioService.getUsuarioActual().getNombreUsuario();
			if(configuracion.getId() == null){
				configuracion.setCodigoUsuarioCreacion(usuario);
				configuracion.setFechaCreacion(new Date());
				configuracion.setToken(UUID.randomUUID().toString());
			}
			if(configuracion.getAgente() != null
					&& configuracion.getAgente().getId() != null){
				Agente agente = entidadService.findById(Agente.class, configuracion.getAgente().getId());
				configuracion.setAgente(agente);
			}
			configuracion.setCodigoUsuarioModificacion(usuario);
			configuracion.setFechaModificacion(new Date());
			configuracion = entidadService.save(configuracion);
		}
		return configuracion;
	}
	
	/**
	 * valida si existe una configuracion con el mismo negocio y agente que el que se desea crear
	 * @param configuracion
	 * @return
	 */
	private boolean existeConfiguracionAgenteNegocio(ConfiguracionLigaAgente configuracion){
		boolean existeConfiguracion = false;
		ConfigLigaAgenteDTO filtro = new ConfigLigaAgenteDTO();
		List<ConfigLigaAgenteDTO> resultados = null;
		
		filtro.setIdNegocio(configuracion.getNegocio().getIdToNegocio());
		filtro.setIdAgente(configuracion.getAgente().getId());
		
		resultados = this.buscarConfiguracionesLigasAgentes(filtro);
		if(resultados != null
				&& !resultados.isEmpty()){
			existeConfiguracion = true;
		}
		
		return existeConfiguracion;
	}
	
	@Override
	public ConfiguracionLigaAgente generarLigaAgente(Long idConfiguracion, String ipAddress){
		ConfiguracionLigaAgente config = null;
		
		if(idConfiguracion != null){
			config = entidadService.findById(ConfiguracionLigaAgente.class, idConfiguracion);
			if(config != null){
				StringBuilder liga = new StringBuilder();
				String url = parametroGlobalService.obtenerValorPorIdParametro(
								ParametroGlobalService.AP_MIDAS, ParametroGlobalService.LIGA_AGENTE_URL);
				
				Usuario usuario = obtenerUsuarioLigasAgentes(ipAddress); 
				
				liga.append(url);
				liga.append("/MidasWeb/suscripcion/cotizacion/auto/agentes/mostrarCotizadorLigaAgente.action?token=");
				liga.append(usuario.getToken());
				liga.append("&userAccessId=");
				liga.append(usuario.getIdSesionUsuario());
				liga.append("&clati=");
				liga.append(config.getToken());
				
				config.setLiga(liga.toString());
				config.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
				config.setFechaModificacion(new Date());
			}
		}
		config = entidadService.save(config);
		return config;
	}	
	
	/**
	 * Metodo para autenticar el usuario de ligas agentes en la aplicacion.
	 * @return
	 */
	private Usuario obtenerUsuarioLigasAgentes(String ipAddress){
		LoginParameter loginParameter = new LoginParameter();
		loginParameter.setIpAddress(ipAddress);

		String username = parametroGlobalService.obtenerValorPorIdParametro(
				ParametroGlobalService.AP_MIDAS, ParametroGlobalService.LIGA_AGENTE_USUARIO);
		String password = parametroGlobalService.obtenerValorPorIdParametro(
				ParametroGlobalService.AP_MIDAS, ParametroGlobalService.LIGA_AGENTE_PASSWORD);
		loginParameter.setUsuario(username);
		loginParameter.setPassword(password);
		loginParameter.setApplicationId(ParametroGlobalService.AP_MIDAS);
		Usuario usuario = usuarioService.login(loginParameter);
		
		if (usuario == null)
		{
			throw new RuntimeException("Error al intentar autenticar usuario ligas agentes");			
		}
				
		for(AtributoUsuario atributo:usuario.getListaAtributos())
		{
			if(atributo.getNombre().equalsIgnoreCase(ASMConstants.USER_ACCESS_ID))
			{
				usuario.setIdSesionUsuario(String.valueOf(atributo.getValor()));
				break;
			}
		}		
		
		return usuario;
	}
	
	@Override
	public void enviarLigaAgente(Long idConfiguracion){
		if(idConfiguracion != null){
			ConfiguracionLigaAgente configLiga = entidadService.findById(ConfiguracionLigaAgente.class, idConfiguracion);
			if(configLiga != null){
				// NOTIFICACION DE CORREO
		        EmailDestinaratios destinatarios = new EmailDestinaratios();
		        String nombreCompleto = configLiga.getAgente().getPersona().getNombreCompleto();
		        if(nombreCompleto != null){
		        	nombreCompleto = nombreCompleto.replaceAll("\\W"," "); }
		        destinatarios.agregar(EnumModoEnvio.PARA, configLiga.getCorreo() , nombreCompleto ); 
		        
		        HashMap<String, Serializable> notificacion = new HashMap<String, Serializable>();
		        
		        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy",new Locale("es","MX"));			//FECHA
		        String fechaCreacionConFormato = sdf.format(configLiga.getFechaCreacion());
		        StringBuilder infoConfiguracion = new StringBuilder();										//INFO CONFIGURACION
		        infoConfiguracion.append(configLiga.getId()).append(" - ").append(configLiga.getNombre());
		        StringBuilder infoAgente = new StringBuilder();
		        infoAgente.append(configLiga.getAgente().getId()).append(" - ").							//INFO AGENTE
		        	append(configLiga.getAgente().getIdAgente()).append(" - ").
		        	append(configLiga.getAgente().getPersona().getNombreCompleto());
		        StringBuilder infoNegocio = new StringBuilder();											//INFO NEGOCIO
		        infoNegocio.append(configLiga.getNegocio().getIdToNegocio()).append(" - ").
		        	append(configLiga.getNegocio().getDescripcionNegocio());
		        
				notificacion.put("fechaCreacion"  , fechaCreacionConFormato );
				notificacion.put("liga" , configLiga.getLiga());
				notificacion.put("infoconfiguracion", infoConfiguracion);
				notificacion.put("infoagente", infoAgente);
				notificacion.put("infonegocio", infoNegocio);
		        
		        this.notificacionService.enviarNotificacion(
						EnvioNotificacionesService.EnumCodigo.ENVIO_LIGA_AGENTE.toString() , 
						notificacion , 
						configLiga.getId().toString(), destinatarios,
						null);
			}
		}
	}
	
	@Override
	public boolean tienePermisoMonitorearTodosLosAgentes(){
		boolean tienePermiso = false;
		tienePermiso = usuarioService.tienePermisoUsuarioActual(FN_M2_Monitorear_Todos_Los_Agentes);
		return tienePermiso;
	}
	
	@Override
	public void enviarNotificacionCreacionCotizacion(Long configuracionId, BigDecimal idToCotizacion, String nombreProspecto, 
			String correoProspecto, String telefonoProspecto, Locale locale){
		if(configuracionId != null){
			ConfiguracionLigaAgente configLiga = entidadService.findById(ConfiguracionLigaAgente.class, configuracionId);
			if(configLiga != null){
				// NOTIFICACION DE CORREO
		        EmailDestinaratios destinatarios = new EmailDestinaratios();
		        String nombreCompleto = configLiga.getAgente().getPersona().getNombreCompleto();
		        if(nombreCompleto != null){
		        	nombreCompleto = nombreCompleto.replaceAll("\\W"," "); }
		        destinatarios.agregar(EnumModoEnvio.PARA, configLiga.getCorreo() , nombreCompleto); 
		        
		        HashMap<String, Serializable> notificacion = new HashMap<String, Serializable>();
		        
		        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy",new Locale("es","MX"));			//FECHA
		        String fechaCreacionConFormato = sdf.format(new Date());
		        StringBuilder infoConfiguracion = new StringBuilder();										//INFO CONFIGURACION
		        infoConfiguracion.append(configLiga.getId()).append(" - ").append(configLiga.getNombre());
		        StringBuilder infoAgente = new StringBuilder();
		        infoAgente.append(configLiga.getAgente().getId()).append(" - ").							//INFO AGENTE
		        	append(configLiga.getAgente().getIdAgente()).append(" - ").
		        	append(configLiga.getAgente().getPersona().getNombreCompleto());
		        StringBuilder infoNegocio = new StringBuilder();											//INFO NEGOCIO
		        infoNegocio.append(configLiga.getNegocio().getIdToNegocio()).append(" - ").
		        	append(configLiga.getNegocio().getDescripcionNegocio());
		        
				notificacion.put("fechaCreacion"  , fechaCreacionConFormato );
				notificacion.put("idtocotizacion", idToCotizacion);
				notificacion.put("liga" , configLiga.getLiga());
				notificacion.put("infoconfiguracion", infoConfiguracion);
				notificacion.put("infoagente", infoAgente);
				notificacion.put("infonegocio", infoNegocio);
				notificacion.put("nombreProspecto", nombreProspecto);
				notificacion.put("correoProspecto", correoProspecto);
				notificacion.put("telefonoProspecto", telefonoProspecto);
		        
		        this.notificacionService.enviarNotificacion(
						EnvioNotificacionesService.EnumCodigo.COTIZACION_LIGA_AGENTE.toString() , 
						notificacion , 
						configLiga.getId().toString(), destinatarios,
						generarPDFCotizacion(idToCotizacion, locale));
			}
		}
	}
	
	@Override
	public void enviarCotizacionLigaAgenteAlProspecto(Long configuracionId, BigDecimal idToCotizacion, String nombreContratante, String correoContratante, Locale locale){
		StringBuilder info = new StringBuilder();
		info.append("----->configuracionId:").append(configuracionId).append(" idToCotizacion:").append(idToCotizacion).append(" nombreContratante:").append(nombreContratante).append("correoContratante").append(correoContratante);
		log.info(info);
		if(configuracionId != null){
			ConfiguracionLigaAgente configLiga = entidadService.findById(ConfiguracionLigaAgente.class, configuracionId);
			if(configLiga != null){
				// NOTIFICACION DE CORREO
		        EmailDestinaratios destinatarios = new EmailDestinaratios();
		        if(nombreContratante != null){
		        	nombreContratante = nombreContratante.replaceAll("\\W"," "); }
		        
		        destinatarios.agregar(EnumModoEnvio.PARA, correoContratante , nombreContratante ); 
		        
		        HashMap<String, Serializable> notificacion = new HashMap<String, Serializable>();
		        
				notificacion.put("nombreProspecto", nombreContratante);
		        
		        this.notificacionService.enviarNotificacion(
						EnvioNotificacionesService.EnumCodigo.CARATULA_CLIENTE_LIGA_AGENTE.toString() , 
						notificacion , 
						configLiga.getId().toString(), destinatarios,
						generarPDFCotizacion(idToCotizacion, locale));
			}
		}
	}

	public List<ByteArrayAttachment> generarPDFCotizacion(BigDecimal idCotizacion, Locale locale){
		List<ByteArrayAttachment> pdfAdjunto = null;
		final Boolean IMPRIMIR_CARATULA_COTIZACION = Boolean.TRUE;
		final Boolean IMPRIMIR_INCISOS = Boolean.TRUE;
		final String ARCHIVO_ADJUNTO = "Cotizacion.pdf";
		try{
			// Generando impresion de la cotizacion
	        TransporteImpresionDTO pdfCotizacion =  generarPlantillaReporte.imprimirCotizacion(idCotizacion,locale,IMPRIMIR_CARATULA_COTIZACION,IMPRIMIR_INCISOS,false);
	        ByteArrayAttachment adjunto = new ByteArrayAttachment(ARCHIVO_ADJUNTO, TipoArchivo.PDF, pdfCotizacion.getByteArray());
	        pdfAdjunto = new ArrayList<ByteArrayAttachment>();
	        pdfAdjunto.add(adjunto);
		}catch(Exception e){
			log.error("Error al generar la cotizacion para el cliente de ligas de agente", e);
		}
		
		return pdfAdjunto;
	}
	
}
