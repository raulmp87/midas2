package mx.com.afirme.midas2.service.impl.calculos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.calculos.CalculoComisionesDao.EstatusCalculoComisiones;
import mx.com.afirme.midas2.dao.calculos.DetalleCalculoComisionesDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.calculos.CalculoComisiones;
import mx.com.afirme.midas2.domain.calculos.DetalleCalculoComisiones;
import mx.com.afirme.midas2.dto.calculos.DetalleCalculoComisionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.calculos.DetalleCalculoComisionesService;
import mx.com.afirme.midas2.util.MidasException;
@Stateless
public class DetalleCalculoComisionesServiceImpl extends EntidadDaoImpl implements DetalleCalculoComisionesService{
	private DetalleCalculoComisionesDao dao;
	@Override
	public void deleteDetallePorCalculo(Long arg0) throws MidasException {
		dao.deleteDetallePorCalculo(arg0);
	}

	@Override
	public void deleteDetallePorCalculo(CalculoComisiones arg0)throws MidasException {
		dao.deleteDetallePorCalculo(arg0);
	}

	@Override
	public List<DetalleCalculoComisiones> findByFilters(DetalleCalculoComisiones arg0) {
		return dao.findByFilters(arg0);
	}

	@Override
	public List<DetalleCalculoComisiones> listarDetalleDeCalculo(Long arg0)throws MidasException {
		return dao.listarDetalleDeCalculo(arg0);
	}

	@Override
	public List<DetalleCalculoComisiones> listarDetalleDeCalculo(CalculoComisiones arg0) throws MidasException {
		return dao.listarDetalleDeCalculo(arg0);
	}

	@Override
	public DetalleCalculoComisiones loadById(Long arg0) throws MidasException {
		return dao.loadById(arg0);
	}

	@Override
	public DetalleCalculoComisiones loadById(DetalleCalculoComisiones arg0)throws MidasException {
		return dao.loadById(arg0);
	}

	@Override
	public List<DetalleCalculoComisiones> obtenerDetalleCalculoPorConfiguracion(Long arg0, List<AgenteView> arg1) {
		return dao.obtenerDetalleCalculoPorConfiguracion(arg0, arg1);
	}

	@Override
	public void rehabilitarDetallePorCalculo(Long arg0) throws MidasException {
		dao.rehabilitarDetallePorCalculo(arg0);
	}

	@Override
	public void rehabilitarDetallePorCalculo(CalculoComisiones arg0)throws MidasException {
		dao.rehabilitarDetallePorCalculo(arg0);
	}

	@Override
	public Long saveListDetalleCalculoComisiones(List<DetalleCalculoComisiones> arg0) throws MidasException {
		return dao.saveListDetalleCalculoComisiones(arg0);
	}
	

	@Override
	public void generarDetalleCalculoComisiones(Long idConfigComisiones,List<AgenteView> agentes,Long idCalculoTemporal,Long idCalculo) throws MidasException{
		dao.generarDetalleCalculoComisiones(idConfigComisiones, agentes, idCalculoTemporal,idCalculo);
	}
	
	@Override
	public void actualizarDetallePorCalculo(Long idCalculo,EstatusCalculoComisiones estatus) throws MidasException {
		dao.actualizarDetallePorCalculo(idCalculo,estatus);
	}

	@Override
	public void actualizarDetallePorCalculo(CalculoComisiones calculo,EstatusCalculoComisiones estatus) throws MidasException {
		dao.actualizarDetallePorCalculo(calculo, estatus);
	}

	@Override
	public DetalleCalculoComisiones obtenerDetallePorSolicitudCheque(Long idSolicitud) throws MidasException {
		return dao.obtenerDetallePorSolicitudCheque(idSolicitud);
	}
	
	/**
	 * Obtiene el detalle de un calculo de comisiones para comparar por parte de midas.
	 * @param idCalculo
	 * @return
	 * @throws MidasException
	 */
	public List<DetalleCalculoComisiones> listarDetalleCalculoMidasDiferencia(Long idCalculo) throws MidasException{
		return dao.listarDetalleCalculoMidasDiferencia(idCalculo);
	}
	/**
	 * Obtiene el detalle de un calculo de comisiones para comparar por parte de mizar.
	 * @param idCalculo
	 * @return
	 * @throws MidasException
	 */
	public List<DetalleCalculoComisiones> listarDetalleCalculoMizarDiferencia(Long idCalculo) throws MidasException{
		return dao.listarDetalleCalculoMizarDiferencia(idCalculo);
	}
	/**
	 * Obtiene las diferencias de importes entre Midas y Mizar.
	 * @param idCalculo
	 * @return
	 * @throws MidasException
	 */
	public List<DetalleCalculoComisiones> listarDiferenciasMidasMizar(Long idCalculo) throws MidasException{
		return dao.listarDiferenciasMidasMizar(idCalculo);
	}
	
	@Override
	public List<DetalleCalculoComisionesView> listarReporteComisionAgente(Long idCalculo) throws MidasException {
		return dao.listarReporteComisionAgente(idCalculo);
	}
	
	/**
	 *  =====================================================
	 *	Sets and gets
	 *  =====================================================
	 */
	@EJB
	public void setDao(DetalleCalculoComisionesDao dao) {
		this.dao = dao;
	}
}