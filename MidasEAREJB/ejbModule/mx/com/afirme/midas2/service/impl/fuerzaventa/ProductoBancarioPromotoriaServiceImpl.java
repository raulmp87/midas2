package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fuerzaventa.ProductoBancarioPromotoriaDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancarioPromotoria;
import mx.com.afirme.midas2.service.fuerzaventa.ProductoBancarioPromotoriaService;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class ProductoBancarioPromotoriaServiceImpl implements ProductoBancarioPromotoriaService{
	private ProductoBancarioPromotoriaDao dao;
	
	@Override
	public void delete(Long id) throws Exception {
		dao.delete(id);
	}

	@Override
	public List<ProductoBancarioPromotoria> findByFilters(ProductoBancarioPromotoria filtro) {
		return dao.findByFilters(filtro);
	}

	@Override
	public List<ProductoBancarioPromotoria> findByPromotoria(Long idPromotoria) {
		return dao.findByPromotoria(idPromotoria);
	}

	@Override
	public ProductoBancarioPromotoria saveProducto(ProductoBancarioPromotoria producto)throws Exception {
		return dao.saveProducto(producto);
	}
	
	@Override
	public void saveListProductoBanc(List<ProductoBancarioPromotoria> lista)
	throws Exception {
		dao.saveListProductoBanc(lista);
	}
	
	@Override
	public List<ProductoBancarioPromotoria> findByPromotoria(Long idPromotoria, String fechaHistorico, String tipoAccion) {
		// TODO Auto-generated method stub
		return dao.findByPromotoria(idPromotoria, fechaHistorico, tipoAccion);
	}
	/*********************************************************************************************
	 *  Sets & gets
	 ********************************************************************************************/
	@EJB
	public void setDao(ProductoBancarioPromotoriaDao dao) {
		this.dao = dao;
	}


}
