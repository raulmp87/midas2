package mx.com.afirme.midas2.service.impl.bitemporal.poliza.inciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.DescuentoPorVolumenAuto;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class IncisoViewServiceImpl implements IncisoViewService {

	public static List<CatalogoValorFijoDTO> posiblesDeducibles= null;
	public static  BigDecimal IDCOBERTURARC_USA_CANADA = new BigDecimal(2640);
	public static BigDecimal idCoberturaRC_USA_CANADA_FRONT = new BigDecimal(3840);
	public static BigDecimal idCoberturaRC_USA_CANADA_CAM = new BigDecimal(2850);
	public static  Short CLAVECONTRATO = (short) 1;
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	public static final short CVE_ESTATUS_INCISO_VIGENTE = 1;
	public static final short CVE_ESTATUS_INCISO_CANCELADO= 2;

	@Override
	public BitemporalCotizacion getCotizacion(Long cotizacionId, Date validoEn) {
		BitemporalCotizacion biCotizacion = entidadBitemporalService.
		getByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME,
				cotizacionId, getDateTime(validoEn));

		if (biCotizacion != null) {
			fillCotizacion(biCotizacion, validoEn);
		}		

		return  biCotizacion;
	}

	@Override
	public BitemporalCotizacion getCotizacionByContinuity(Long cotizacionContinuityId, Date validoEn) {
		Date recordFrom = null;
		BitemporalCotizacion bc = getCotizacionByContinuity(cotizacionContinuityId, validoEn, recordFrom);
		return bc;
	}

	@Override
	public BitemporalCotizacion getCotizacionByContinuity(Long cotizacionContinuityId, Date validoEn, 
			Date recordFrom) {
		BitemporalCotizacion bc = entidadBitemporalService.getByKey(
				CotizacionContinuity.class, cotizacionContinuityId, getDateTime(validoEn));

		if (bc == null && recordFrom != null) {
			CotizacionContinuity cc = entidadContinuityService.findContinuityByKey(
					CotizacionContinuity.class, cotizacionContinuityId);
			bc = cc.getCotizaciones().get(new DateTime(validoEn.getTime()) , new DateTime(recordFrom.getTime()));
		}

		return  bc;
	}

	/** getInciso1 */
	@Override
	public BitemporalInciso getInciso(Long incisoContinuityId, Date validoEn) {
		LogDeMidasInterfaz.log("Entrando a getInciso1", Level.INFO, null);
		Date recordFrom = null;
		Short claveTipoEndoso = null;
		BitemporalInciso bi = getInciso(incisoContinuityId, validoEn, recordFrom, claveTipoEndoso);
		LogDeMidasInterfaz.log("Saliendo de getInciso1", Level.INFO, null);
		return bi;
	}

	/** getInciso1a */
	@Override
	public BitemporalInciso getInciso(Long incisoContinuityId, Date validoEn, Date recordFrom) {
		LogDeMidasInterfaz.log("Entrando a getInciso1a", Level.INFO, null);

		Short claveTipoEndoso = null;
		BitemporalInciso bi = getInciso(incisoContinuityId, validoEn, recordFrom, claveTipoEndoso);

		LogDeMidasInterfaz.log("Saliendo de getInciso1a", Level.INFO, null);
		return bi;
	}

	/** getInciso2 */
	@Override
	public BitemporalInciso getInciso(Long incisoContinuityId, Date validoEn, Date recordFrom, Short claveTipoEndoso) {
		LogDeMidasInterfaz.log("Entrando a getInciso2", Level.INFO, null);
		//return entidadBitemporalService.getByKey(IncisoContinuity.class, incisoContinuityId, getDateTime(validoEn));
		Long validoEnMillis = validoEn != null ? validoEn.getTime() : null;
		BitemporalInciso bi = null;
		if (recordFrom == null) {
			bi = getInciso(incisoContinuityId, validoEn, validoEnMillis);
		}
		if (bi == null && recordFrom != null) {
			bi = getInciso(incisoContinuityId, validoEn, recordFrom, validoEnMillis, claveTipoEndoso);
		}

		LogDeMidasInterfaz.log("Saliendo de getInciso2", Level.INFO, null);
		return bi;
	}

	/** getInciso3 */
	private BitemporalInciso getInciso(Long incisoContinuityId, Date validoEn,  Long validoEnMillisegundos) {
		LogDeMidasInterfaz.log("Entrando a getInciso3", Level.INFO, null);		
		Date recordFrom = null;
		Short claveTipoEndoso = null;
		BitemporalInciso bi = getInciso(incisoContinuityId, validoEn, recordFrom, validoEnMillisegundos, claveTipoEndoso);
		LogDeMidasInterfaz.log("Saliendo de getInciso3", Level.INFO, null);
		return bi;
	}

	/**
	 * getInciso4
	 * @param incisoContinuityId
	 * @param validoEn
	 * @param validoEnMillisegundos
	 * @return
	 */
	private BitemporalInciso getInciso(Long incisoContinuityId, Date validoEn, Date recordFrom, 
			Long validoEnMillisegundos, Short claveTipoEndoso) {

		DateTime validoEnDateTime = null;
		BitemporalInciso bi = null;

		if (validoEnMillisegundos != null) {
			validoEnDateTime = new DateTime(validoEnMillisegundos);
		} else {
			validoEnDateTime = getDateTime(validoEn) ;
		}

		if (recordFrom == null) {
			bi = entidadBitemporalService.getByKey(IncisoContinuity.class, 
					incisoContinuityId, validoEnDateTime);
		}		

		if (bi == null && recordFrom != null) {
			IncisoContinuity ic = entidadContinuityService.findContinuityByKey(IncisoContinuity.class, incisoContinuityId);
			bi = ic.getIncisos().get(validoEnDateTime, new DateTime(recordFrom));
		}

		// Cancelados
		if (bi == null && recordFrom != null && 
				(claveTipoEndoso != null && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION)) {
			Long bi_continuitiId = incisoContinuityId;
			Collection<BitemporalInciso> collectionBI = entidadBitemporalService.obtenerCancelados(
					BitemporalInciso.class, bi_continuitiId, validoEnDateTime, new DateTime(recordFrom));
			if (!collectionBI.isEmpty()) {
				bi = collectionBI.iterator().next();
			} 
		}

		return bi; 
	}

	@Override
	public BitemporalAutoInciso getAutoInciso(Long incisoContinuityId, Date validoEn){
		BitemporalAutoInciso bai = null;

		Date recordFrom = null;
		Short claveTipoEndoso = null;
		bai = getAutoInciso(incisoContinuityId, validoEn, recordFrom, claveTipoEndoso);

		return bai;
	}

	@Override
	public BitemporalAutoInciso getAutoInciso(Long incisoContinuityId, Date validoEn, Date recordFrom, 
			Short claveTipoEndoso) {

		BitemporalAutoInciso biAutoInciso = null;

		BitemporalInciso biInciso = getInciso(incisoContinuityId, validoEn, recordFrom, null, claveTipoEndoso);		

		if (biInciso != null) {
			biAutoInciso = getAutoInciso(biInciso, validoEn, recordFrom, claveTipoEndoso);			
			fillAutoInciso(biAutoInciso);	
		}	

		return biAutoInciso;
	}

	@Override
	public Collection<BitemporalInciso> getLstIncisoByCotizacion(Long cotizacionContinuityId, Date validoEn) {

		Collection<BitemporalInciso> biIncisos = entidadBitemporalService.findByParentKey(IncisoContinuity.class, 
				IncisoContinuity.PARENT_KEY_NAME, cotizacionContinuityId, getDateTime(validoEn));

		for (Iterator<BitemporalInciso> it = biIncisos.iterator(); it.hasNext();) {
			BitemporalInciso biInciso = (BitemporalInciso) it.next();
			if (biInciso != null) {
				fillLstInciso(biInciso, validoEn);
			} else {
				it.remove();
			}
		}		
		return biIncisos;
	}

	@Override
	public Collection<BitemporalInciso> getLstIncisoCanceladosByCotizacion(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, 
			Date validoEn, Date recordFrom, Boolean esConteo) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity." + IncisoContinuity.PARENT_KEY_NAME, cotizacionContinuityId);

		boolean cancelado = false;

		BitemporalCotizacion cotizacion = getCotizacionByContinuity(cotizacionContinuityId, validoEn, recordFrom);

		if (cotizacion == null) {
			Collection<BitemporalCotizacion> collectionBI = 
				entidadBitemporalService.obtenerCancelados(BitemporalCotizacion.class, cotizacionContinuityId, 
						new DateTime(validoEn), new DateTime(recordFrom));
			if (!collectionBI.isEmpty()) {
				cotizacion = collectionBI.iterator().next();	
			}
		}

		Collection<BitemporalInciso> biIncisos = null;

		/*biIncisos = entidadBitemporalService.listarFiltrado(BitemporalInciso.class, params, new DateTime(validoEn), 
				new DateTime(recordFrom), new String[0]);

		if (biIncisos == null || biIncisos.isEmpty()) {
			Collection<IncisoContinuity> collectionIC = entidadContinuityService.findContinuitiesByParentKey(
					IncisoContinuity.class, IncisoContinuity.PARENT_KEY_NAME, cotizacionContinuityId);

			if (!collectionIC.isEmpty()) { 
			biIncisos  = entidadBitemporalService.obtenerCancelados(collectionIC, BitemporalInciso.class, 
																new DateTime(validoEn), new DateTime(recordFrom));
			cancelado = true;
			}	
		}		*/	

		List<PolizaDTO> lstPoliza = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", 
				new BigDecimal(cotizacion.getContinuity().getNumero()));

		biIncisos = listadoIncisosDinamicoService.buscarIncisosFiltrado(filtros, (short) 0,
				lstPoliza.get(0).getIdToPoliza().longValue(), validoEn, recordFrom, true, true,0);		


		if (biIncisos == null || biIncisos.isEmpty()) {
			biIncisos = listadoIncisosDinamicoService.buscarIncisosFiltrado(filtros, 
					SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS, lstPoliza.get(0).getIdToPoliza().longValue(), null,0);
		}

		if(!esConteo){
			Short numeroEndoso= this.getNumeroEndoso(lstPoliza.get(0).getCotizacionDTO().getIdToCotizacion().longValue(), recordFrom);
	
			if (numeroEndoso == null) {
				numeroEndoso = (short) 0;
			}				
	
	
			for (Iterator<BitemporalInciso> it = biIncisos.iterator(); it.hasNext();) {
				BitemporalInciso biInciso = (BitemporalInciso) it.next();
				if (biInciso != null) {
					if (cancelado) {
						biInciso.getValue().setEstatus(CVE_ESTATUS_INCISO_CANCELADO);
					} else {
						biInciso.getValue().setEstatus(CVE_ESTATUS_INCISO_VIGENTE);
					}
					biInciso.getValue().setPrimaTotal(calculoService.resumenCostosIncisoPoliza(lstPoliza.get(0).getIdToPoliza(), new BigDecimal(biInciso.getContinuity().getNumero()), numeroEndoso).getPrimaTotal());
					//fillLstIncisoCancelado(biInciso, validoEn, recordFrom);
				} else {
					it.remove();
				}
			}
		}
		return biIncisos;
	}

	@Override
	public Collection<BitemporalInciso> getLstIncisosCanceladosTotales(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, 
			Date validoEn, Date recordFrom, Boolean esConteo) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity." + IncisoContinuity.PARENT_KEY_NAME, cotizacionContinuityId);	

		Collection<BitemporalInciso> biIncisos = new ArrayList<BitemporalInciso>();
		Collection<BitemporalInciso> biIncisosCancelados = null;

		BitemporalCotizacion cotizacion = getCotizacionByContinuity(cotizacionContinuityId, validoEn, recordFrom);

		if (cotizacion == null) {
			Collection<BitemporalCotizacion> collectionBI = 
				entidadBitemporalService.obtenerCancelados(BitemporalCotizacion.class, cotizacionContinuityId, 
						new DateTime(validoEn), new DateTime(recordFrom));
			if (!collectionBI.isEmpty()) {
				cotizacion = collectionBI.iterator().next();	
			}
		}

		List<PolizaDTO> lstPoliza = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", new BigDecimal(cotizacion.getContinuity().getNumero()));

		if (lstPoliza!= null & !lstPoliza.isEmpty()) {
			biIncisosCancelados = listadoIncisosDinamicoService.buscarIncisosFiltrado(filtros, 
					SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS,
					lstPoliza.get(0).getIdToPoliza().longValue(), validoEn, recordFrom, true,0);		
			//biIncisosCancelados = listadoIncisosDinamicoService.buscarIncisosFiltrado(filtros, 
			//	SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS, lstPoliza.get(0).getIdToPoliza().longValue(), null);
			if(!esConteo){
				Short numeroEndoso= this.getNumeroEndoso(lstPoliza.get(0).getCotizacionDTO().getIdToCotizacion().longValue(), recordFrom);
	
				if (numeroEndoso == null) {
					numeroEndoso = (short) 0;
				}
	
				if (biIncisosCancelados != null && !biIncisosCancelados.isEmpty()) {
					for (Iterator<BitemporalInciso> it = biIncisosCancelados.iterator(); it.hasNext();) {
						BitemporalInciso biInciso = (BitemporalInciso) it.next();
						if (biInciso != null) {
							biInciso.getValue().setEstatus(CVE_ESTATUS_INCISO_CANCELADO);
							//fillLstIncisoCancelado(biInciso, validoEn, recordFrom);
							biInciso.getValue().setPrimaTotal(calculoService.resumenCostosIncisoPoliza(lstPoliza.get(0).getIdToPoliza(), new BigDecimal(biInciso.getContinuity().getNumero()), numeroEndoso).getPrimaTotal());
						} else {
							it.remove();
						}
					}
					biIncisos.addAll(biIncisosCancelados);
				}
			}
		}

		return biIncisos;
	}
	/**
	 * Regresar las coberturas de un Inciso con una sola Secci�n
	 * @param cotizacionId
	 * @return Collection<BitemporalCoberturaSeccion>
	 */
	@Override
	public List<BitemporalCoberturaSeccion> getLstCoberturasByInciso(Long incisoContinuityId, Date validoEn) {
		return 	getLstCoberturasByInciso(incisoContinuityId, validoEn, false);
	}

	@Override
	public List<BitemporalCoberturaSeccion> getLstCoberturasByInciso(Long incisoContinuityId, Date validoEn, boolean inProcess) {
		return 	getLstCoberturasByInciso(incisoContinuityId,  TimeUtils.getDateTime(validoEn), null, inProcess);
	}

	@Override
	public List<BitemporalCoberturaSeccion> getLstCoberturasByInciso(Long incisoContinuityId, DateTime validoEn, 
			DateTime recordFrom, boolean inProcess) {
		return getLstCoberturasByInciso(incisoContinuityId, validoEn, recordFrom, null, inProcess);
	}

	@Override
	public List<BitemporalCoberturaSeccion> getLstCoberturasByInciso(Long incisoContinuityId, DateTime validoEn, 
			DateTime recordFrom, Short claveTipoEndoso, boolean inProcess) {

		List<BitemporalCoberturaSeccion> biCoberturaSeccionList = new ArrayList<BitemporalCoberturaSeccion>();
		NegocioCobPaqSeccion negocioCobPaqSeccion;
		Map<String, Object> params = new LinkedHashMap<String, Object>();

		BitemporalInciso biInciso = null;

		if (inProcess) {
			biInciso = entidadBitemporalService.getInProcessByKey(IncisoContinuity.class, incisoContinuityId, validoEn);			
		} else if (recordFrom == null) {
			biInciso = getInciso(incisoContinuityId, validoEn.toDate());
		}

		if (biInciso == null && recordFrom != null) {
			biInciso = getInciso(incisoContinuityId, validoEn.toDate(), recordFrom.toDate(), EndosoDTO.TIPO_ENDOSO_CANCELACION);
		}

		BitemporalAutoInciso bAutoInciso = null;

		if (inProcess) {
			bAutoInciso = biInciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().getInProcess(validoEn);			
		} else if (recordFrom == null) {
			bAutoInciso = getAutoInciso(biInciso, validoEn.toDate());
		}

		if (bAutoInciso == null && recordFrom != null) {
			bAutoInciso = getAutoInciso(biInciso, validoEn.toDate(), recordFrom.toDate(), EndosoDTO.TIPO_ENDOSO_CANCELACION);
		}

		BitemporalCotizacion  bCotizacion = null;

		if (inProcess) {
			bCotizacion = biInciso.getContinuity().getCotizacionContinuity().getCotizaciones().getInProcess(validoEn);
		} else if (recordFrom == null) {
			bCotizacion = biInciso.getContinuity().getCotizacionContinuity().getCotizaciones().get(validoEn);
		}

		if(bCotizacion == null && recordFrom != null){
			bCotizacion = biInciso.getContinuity().getCotizacionContinuity().getCotizaciones().get(new DateTime(validoEn), new DateTime(recordFrom));
		}

		if (bCotizacion == null && recordFrom != null) {
			Collection<BitemporalCotizacion> collectionCot = entidadBitemporalService.obtenerCancelados(
					BitemporalCotizacion.class, biInciso.getContinuity().getCotizacionContinuity().getId(), validoEn, recordFrom);
			if (!collectionCot.isEmpty()) {
				bCotizacion = collectionCot.iterator().next();	
			}	
		}

		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, new BigDecimal(bCotizacion.getContinuity().getNumero()));

		Collection<BitemporalSeccionInciso> lstSeccion = null;

		if (inProcess) {
			lstSeccion = biInciso.getContinuity().getIncisoSeccionContinuities().getInProcess(validoEn);			
		} else if (recordFrom == null) {
			lstSeccion = biInciso.getContinuity().getIncisoSeccionContinuities().get(validoEn);
		}

		if ((lstSeccion == null || lstSeccion.isEmpty()) && recordFrom != null ) {
			lstSeccion = biInciso.getContinuity().getIncisoSeccionContinuities().get(validoEn, recordFrom);

			if ((lstSeccion == null || lstSeccion.isEmpty())) {
				Collection<SeccionIncisoContinuity> collCS = entidadContinuityService.findContinuitiesByParentKey(
						SeccionIncisoContinuity.class, SeccionIncisoContinuity.PARENT_KEY_NAME, biInciso.getContinuity().getId());

				if (!collCS.isEmpty()) {
					lstSeccion = entidadBitemporalService.obtenerCancelados(collCS, BitemporalSeccionInciso.class, validoEn, recordFrom);	
				}
			}			
		}

		Collection<BitemporalCoberturaSeccion> lstBiCoberturaSeccion = null;

		for (BitemporalSeccionInciso biSeccionInciso : lstSeccion) {

			if (inProcess) {
				lstBiCoberturaSeccion = biSeccionInciso.getContinuity().getCoberturaSeccionContinuities().getInProcess(validoEn);			
			} else if (recordFrom == null) {
				lstBiCoberturaSeccion = biSeccionInciso.getContinuity().getCoberturaSeccionContinuities().get(validoEn);
			}

			if ((lstBiCoberturaSeccion == null || lstBiCoberturaSeccion.isEmpty()) && recordFrom != null ) {
				lstBiCoberturaSeccion = biSeccionInciso.getContinuity().getCoberturaSeccionContinuities().get(validoEn, recordFrom);

				if ((lstBiCoberturaSeccion == null || lstBiCoberturaSeccion.isEmpty())) {
					Collection<CoberturaSeccionContinuity> collCS = entidadContinuityService.findContinuitiesByParentKey(
							CoberturaSeccionContinuity.class, CoberturaSeccionContinuity.PARENT_KEY_NAME, biSeccionInciso.getContinuity().getId());

					if (!collCS.isEmpty()) {
						lstBiCoberturaSeccion = entidadBitemporalService.obtenerCancelados(collCS, BitemporalCoberturaSeccion.class, 
								validoEn, recordFrom);	
					}
				}				
			}
		}

		biCoberturaSeccionList = new ArrayList<BitemporalCoberturaSeccion>(lstBiCoberturaSeccion);

		Collections.sort(biCoberturaSeccionList, new Comparator<BitemporalCoberturaSeccion>() {
			@Override
			public int compare(BitemporalCoberturaSeccion n1,
					BitemporalCoberturaSeccion n2) {
				return n1.getContinuity()
				.getCoberturaDTO()
				.getNumeroSecuencia()
				.compareTo(
						n2.getContinuity().getCoberturaDTO()
						.getNumeroSecuencia());
			}
		});	

		if (posiblesDeducibles == null) {
			params.put("id.idGrupoValores", Integer.valueOf(38));
			posiblesDeducibles = entidadService.findByProperties(CatalogoValorFijoDTO.class, params);
		}

		for (BitemporalCoberturaSeccion coberturaSeccion : biCoberturaSeccionList) {
			negocioCobPaqSeccion = coberturaService.getLimitesSumaAseguradaPorCobertura(coberturaSeccion.getContinuity().getCoberturaDTO().getIdToCobertura(), bAutoInciso.getValue().getNegocioPaqueteId(),
					bAutoInciso.getValue().getEstadoId(),bAutoInciso.getValue().getMunicipioId(), cotizacion.getIdMoneda(),
					new BigDecimal(bAutoInciso.getValue().getTipoUsoId()), cotizacion.getSolicitudDTO().getCodigoAgente(),
					(cotizacion.getSolicitudDTO().getEsRenovacion() == null 
							|| cotizacion.getSolicitudDTO().getEsRenovacion() == 0)? false : true);

			if (negocioCobPaqSeccion != null && negocioCobPaqSeccion.getValorSumaAseguradaMax() != null && negocioCobPaqSeccion.getValorSumaAseguradaMin() != null) {
				coberturaSeccion.getValue().setValorSumaAseguradaMax(negocioCobPaqSeccion.getValorSumaAseguradaMax());
				coberturaSeccion.getValue().setValorSumaAseguradaMin(negocioCobPaqSeccion.getValorSumaAseguradaMin());
			} else {
				coberturaSeccion.getValue().setValorSumaAseguradaMax(new Double("0"));
				coberturaSeccion.getValue().setValorSumaAseguradaMin(new Double("0"));
			}

			coberturaSeccion.getValue().setDescripcionDeducible(getDescripcionDeducible(coberturaSeccion.getContinuity().getCoberturaDTO().getClaveTipoDeducible()));
		}

		return biCoberturaSeccionList;
	}



	@Override
	public Boolean infoConductorEsRequerido(Long incisoContinuityId, Date validoEn) {

		return infoConductorEsRequerido(incisoContinuityId, validoEn, false);
	}

	public Boolean infoConductorEsRequerido(Long incisoContinuityId, Date validoEn, Boolean getInProcess) {
		return infoConductorEsRequerido(incisoContinuityId, TimeUtils.getDateTime(validoEn), null, getInProcess);
	}

	@Override
	public Boolean infoConductorEsRequerido(Long incisoContinuityId, DateTime validoEn, DateTime recordFrom, Boolean getInProcess) {

		BitemporalInciso biInciso = null;
		if (getInProcess) {
			biInciso = entidadBitemporalService.getInProcessByKey(IncisoContinuity.class, incisoContinuityId, validoEn);			
		} else if (recordFrom == null) {
			biInciso = getInciso(incisoContinuityId, validoEn.toDate());
		} 
		if (biInciso == null && recordFrom != null) {
			biInciso = getInciso(incisoContinuityId, validoEn.toDate(), recordFrom.toDate(), EndosoDTO.TIPO_ENDOSO_CANCELACION);
		}

		Boolean requerido = false;		
		//Validar si la cobertura esta presente y esta contratada en el inciso, en caso de cumplirse lo anterior (requerido = true)

		Collection<BitemporalSeccionInciso> lstSeccion = null;

		if (getInProcess) {
			lstSeccion = biInciso.getContinuity().getIncisoSeccionContinuities().getInProcess(validoEn);			
		} else if (recordFrom == null) {
			lstSeccion = biInciso.getContinuity().getIncisoSeccionContinuities().get(validoEn);
		}

		if (lstSeccion == null && recordFrom != null) {
			lstSeccion = biInciso.getContinuity().getIncisoSeccionContinuities().get(validoEn, recordFrom);
		}

		if ((lstSeccion == null || lstSeccion.isEmpty()) && recordFrom != null) {

			lstSeccion = biInciso.getContinuity().getIncisoSeccionContinuities().get(validoEn, recordFrom);

			if ((lstSeccion == null || lstSeccion.isEmpty())) {
				Collection<SeccionIncisoContinuity> collSIC = entidadContinuityService.findContinuitiesByParentKey(
						SeccionIncisoContinuity.class, SeccionIncisoContinuity.PARENT_KEY_NAME, biInciso.getContinuity().getId());

				if (!collSIC.isEmpty()) {
					lstSeccion = entidadBitemporalService.obtenerCancelados(collSIC, BitemporalSeccionInciso.class, validoEn, recordFrom);	
				}	
			}

		}

		for (BitemporalSeccionInciso biSeccionInciso : lstSeccion) {

			Collection<BitemporalCoberturaSeccion> lstBiCoberturaSeccion = null;

			if (getInProcess) {
				lstBiCoberturaSeccion = biSeccionInciso.getContinuity().getCoberturaSeccionContinuities().getInProcess(validoEn);			
			} else if (recordFrom == null) {
				lstBiCoberturaSeccion = biSeccionInciso.getContinuity().getCoberturaSeccionContinuities().get(validoEn);
			}

			if (lstBiCoberturaSeccion == null && recordFrom != null) {
				lstBiCoberturaSeccion = biSeccionInciso.getContinuity().getCoberturaSeccionContinuities().get(validoEn, recordFrom);
			}

			if ((lstBiCoberturaSeccion == null || lstBiCoberturaSeccion.isEmpty()) && recordFrom != null) {

				lstBiCoberturaSeccion = biSeccionInciso.getContinuity().getCoberturaSeccionContinuities().get(validoEn, recordFrom);

				if ((lstBiCoberturaSeccion == null || lstBiCoberturaSeccion.isEmpty())) {
					Collection<CoberturaSeccionContinuity> collCS = entidadContinuityService.findContinuitiesByParentKey(
							CoberturaSeccionContinuity.class, CoberturaSeccionContinuity.PARENT_KEY_NAME, biInciso.getContinuity().getId());

					if (!collCS.isEmpty()) {
						lstBiCoberturaSeccion = entidadBitemporalService.obtenerCancelados(collCS, BitemporalCoberturaSeccion.class, 
								validoEn, recordFrom);	
					}
				}				
			}
			ParametroGeneralId idParametro=new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_COBERTURA_RC_USA,new BigDecimal(920010));
			ParametroGeneralDTO parametro=parametroGeneralFacade.findById(idParametro);
			String[] valoresParametros=null;
			if(parametro !=null && parametro.getValor()!=null){
				 valoresParametros=parametro.getValor().split(UtileriasWeb.SEPARADOR_COMA);	
			}
			for (BitemporalCoberturaSeccion biCoberturaSeccion : lstBiCoberturaSeccion) {
				for( int i = 0; valoresParametros!=null && i <= valoresParametros.length - 1; i++)
				{
					if( StringUtils.isNumeric(valoresParametros[i]) ){
						BigDecimal idCobertura= new  BigDecimal(valoresParametros[i]);
						if(biCoberturaSeccion.getContinuity().getCoberturaDTO().getIdToCobertura().equals(idCobertura) &&
								biCoberturaSeccion.getValue().getClaveContrato().equals((short) CLAVECONTRATO	))
						{
							requerido = true;
							break;
						}
					}
				}
				if(requerido){
					break;
				}
			}
		}
		return requerido;
	}

	@Override
	public Long getNumeroIncisos(Long idToCotizacion, DateTime validFromDT, DateTime recordFromDT) {
		Long numIncisos = 0L;

		//Collection<BitemporalInciso> lstIncisos = new ArrayList<BitemporalInciso>();
		CotizacionContinuity cotizacionContinuity = entidadContinuityService.findContinuityByBusinessKey(
				CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, idToCotizacion);

		
		numIncisos = listadoIncisosDinamicoService.getNumeroIncisos(cotizacionContinuity.getId(), validFromDT, recordFromDT, false);
		if(numIncisos.equals(0L)){
			numIncisos = listadoIncisosDinamicoService.getNumeroIncisos(cotizacionContinuity.getId(), validFromDT, recordFromDT, true);
		}
		/*
		BitemporalCotizacion biCotizacion = cotizacionContinuity.getCotizaciones().get(validFromDT, recordFromDT);

		if (biCotizacion == null) {
			Collection<BitemporalCotizacion> collectionBI = 
				entidadBitemporalService.obtenerCancelados(BitemporalCotizacion.class, cotizacionContinuity.getId(), 
						validFromDT, recordFromDT);
			if (!collectionBI.isEmpty()) {
				biCotizacion = collectionBI.iterator().next();	
			}
		}

		lstIncisos = biCotizacion.getContinuity().getIncisoContinuities().get(validFromDT, recordFromDT);
		 

		// Cancelados
		if (lstIncisos.isEmpty() && recordFromDT != null) {
			Collection<IncisoContinuity> collectionSIC = entidadContinuityService.findContinuitiesByParentKey(IncisoContinuity.class, 
					IncisoContinuity.PARENT_KEY_NAME, cotizacionContinuity.getId());
			if (!collectionSIC.isEmpty()) {
				//SeccionIncisoContinuity sic = collectionSIC.iterator().next(); 
				//Long bsi_continuitiId = sic.getId();
				lstIncisos = entidadBitemporalService.obtenerCancelados(collectionSIC, BitemporalInciso.class, 
						validFromDT, recordFromDT);	
			}
		}

		if (lstIncisos != null & !lstIncisos.isEmpty()) {
			numIncisos =  new Long(lstIncisos.size());
		}*/

		return numIncisos;		
	}


	@Override
	public Short getNumeroEndoso(Long idToCotizacion, Date recordFrom) {

		Short numeroEndoso = null;

		Map<String, Object> values = new LinkedHashMap<String, Object>();
		values.put("cotizacionId", idToCotizacion);
		values.put("claveEstatusCot", 16);
		values.put("recordFrom", recordFrom);

		List<ControlEndosoCot> controlEndosos = entidadService.findByProperties(ControlEndosoCot.class,values);
		ControlEndosoCot controlEndoso = null;
		if(controlEndosos !=null && !controlEndosos.isEmpty()){
			controlEndoso = Collections.max(controlEndosos, new Comparator<ControlEndosoCot>() {
				public int compare(ControlEndosoCot e1, ControlEndosoCot e2){
					return e1.getId().compareTo(e2.getId());
				} 
			});
		}

		if (controlEndoso != null) {
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("idToCotizacion", controlEndoso.getCotizacionId());
			parametros.put("recordFrom", controlEndoso.getRecordFrom());

			List<EndosoDTO> listaEndosos = entidadService.findByProperties(EndosoDTO.class, parametros);

			if (!listaEndosos.isEmpty()) {
				numeroEndoso = listaEndosos.get(0).getId().getNumeroEndoso();
			}
		}

		if (numeroEndoso == null)  {
			numeroEndoso = (short) 0;
		}		

		return numeroEndoso;
	}


	private String getDescripcionDeducible(String tipoDeducible) {
		String descripcion = "";
		int valorTipoDeducible;
		if (tipoDeducible != null) {
			valorTipoDeducible = Integer.valueOf(tipoDeducible);

			for (CatalogoValorFijoDTO valor: posiblesDeducibles) {
				if (valor.getId().getIdDato() == valorTipoDeducible) {
					descripcion = valor.getDescripcion();					
					break;
				}
			}
		}
		return descripcion;
	}

	private void fillCotizacion(BitemporalCotizacion biCotizacion, Date validoEn) {
		biCotizacion.getValue().setDescuentoVolumen(getDescuentoPorVolumenaNivelPoliza(biCotizacion, validoEn));
	}

	private void fillAutoInciso(BitemporalAutoInciso biAutoInciso) {
		try{
		biAutoInciso.getValue().setNombreMunicipio(
				entidadService.findById(MunicipioDTO.class, biAutoInciso.getValue().getMunicipioId()).getDescription());

		biAutoInciso.getValue().setNombreEstado(
				entidadService.findById(EstadoDTO.class, biAutoInciso.getValue().getEstadoId()).getDescription());		

		//Cambio para polizas migradas
		
		if (biAutoInciso.getValue().getTipoUsoId() != null) {			
		
			TipoUsoVehiculoDTO tipoUso = entidadService.findById(TipoUsoVehiculoDTO.class, new BigDecimal(biAutoInciso.getValue().getTipoUsoId()));
			
			if (tipoUso != null) {
				biAutoInciso.getValue().setDescTipoUso(tipoUso.getDescription());
			}
		}
		
		if (biAutoInciso.getValue().getMarcaId() != null) {
			MarcaVehiculoDTO marcaVehiculo = entidadService.findById(MarcaVehiculoDTO.class, biAutoInciso.getValue().getMarcaId());
					
			if (marcaVehiculo != null) {
				biAutoInciso.getValue().setDescMarca(marcaVehiculo.getDescription());
			}
		}

		if (biAutoInciso.getValue().getEstiloId() != null) {
			EstiloVehiculoId estiloId = getEstiloVehiculoId(biAutoInciso.getValue().getEstiloId());
			
			if (estiloId != null) {
				EstiloVehiculoDTO estiloVehiculo = entidadService.findById(EstiloVehiculoDTO.class, estiloId);
				
				if (estiloVehiculo != null) {
					biAutoInciso.getValue().setDescEstilo(estiloVehiculo.getDescription());
				}
			}
			
		}		

		NegocioSeccion negocio = entidadService.findById(NegocioSeccion.class,  new BigDecimal(biAutoInciso.getValue().getNegocioSeccionId()));

		biAutoInciso.getValue().setDescLineaNegocio(negocio.getSeccionDTO().getNombreComercial());
		}catch(Exception e){
			
			System.out.println("Ocurrio un Error al Tratar de obtener la informacion del Autoinciso con el continuity  "+biAutoInciso.getContinuity().getId() );
		}

	}

	private void fillLstInciso(BitemporalInciso biInciso, Date validoEn) {
		BitemporalAutoInciso biAutoInciso = getAutoInciso(biInciso, validoEn);
		biInciso.getValue().setAutoInciso(biAutoInciso.getValue());
	}

	private void fillLstIncisoCancelado(BitemporalInciso biInciso, Date validoEn, Date recordFrom) {
		BitemporalAutoInciso biAutoInciso = getAutoInciso(biInciso, validoEn, recordFrom, EndosoDTO.TIPO_ENDOSO_CANCELACION);
		biInciso.getValue().setAutoInciso(biAutoInciso.getValue());
	}

	private BitemporalAutoInciso getAutoInciso(BitemporalInciso biInciso, Date validoEn) {
		Date recordFrom = null;
		Short claveTipoEndoso = null;

		return getAutoInciso(biInciso, validoEn, recordFrom, claveTipoEndoso);
	}

	private BitemporalAutoInciso getAutoInciso(BitemporalInciso biInciso, Date validoEn, Date recordFrom, 
			Short claveTipoEndoso) {

		AutoIncisoContinuity aic = biInciso.getContinuity().getAutoIncisoContinuity();
		BitemporalAutoInciso bai = null;
		if (recordFrom == null) {
			bai = aic.getBitemporalProperty().get(getDateTime(validoEn));
		}

		if (bai == null && recordFrom != null) {
			bai = aic.getBitemporalProperty().get(new DateTime(validoEn), new DateTime(recordFrom));
		}
		// Cancelados
		if (bai == null && recordFrom != null && claveTipoEndoso != null && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION) {
			Long autoIncisoContinuityId = aic.getId();
			Collection<BitemporalAutoInciso> collectionBAI = entidadBitemporalService.obtenerCancelados(
					BitemporalAutoInciso.class, autoIncisoContinuityId, new DateTime(validoEn), 
					new DateTime(recordFrom));
			if (!collectionBAI.isEmpty()) {
				bai = collectionBAI.iterator().next();	
			}
		}

		return bai;
	}

	private DateTime getDateTime(Date date) {
		DateTime dateTime = TimeUtils.now();

		if (date != null) {
			dateTime = TimeUtils.getDateTime(date);
		}
		return dateTime;
	}

	private EstiloVehiculoId getEstiloVehiculoId(String estiloId) {
		EstiloVehiculoId estiloVehiculoId = null;
		String[] arrayEstilo = StringUtils.split(estiloId, '_');
		
		if (arrayEstilo.length >= 3) {
			estiloVehiculoId = new EstiloVehiculoId(arrayEstilo[0], arrayEstilo[1], new BigDecimal(arrayEstilo[2]));
		}
		
		return estiloVehiculoId;		
	}

	private Double getDescuentoPorVolumenaNivelPoliza(BitemporalCotizacion biCotizacion, Date validoEn) {
		List<DescuentoPorVolumenAuto> descuentoPorVolumenAutoList =  entidadService.findAll(DescuentoPorVolumenAuto.class);
		Collection<BitemporalInciso> incisoCotizacionList = getLstIncisoByCotizacion(biCotizacion.getContinuity().getId(), validoEn);

		Double descuento = new Double(0.0);
		if (incisoCotizacionList != null && incisoCotizacionList.size() > 0) {
			for(DescuentoPorVolumenAuto item : descuentoPorVolumenAutoList) {
				if (incisoCotizacionList.size() > item.getMinTotalSubsections().intValue() && incisoCotizacionList.size() < item.getMaxTotalSubsections().intValue()){
					descuento = item.getDefaultDiscountVolumen();
					break;
				}
			}
		}
		return descuento;
	}

	@Override
	public BitemporalCotizacion getCotizacionInProcess(BitemporalInciso biInciso, Date validoEn) {

		BitemporalCotizacion biCotizacion = biInciso.getContinuity().getCotizacionContinuity().getCotizaciones().getInProcess(TimeUtils.getDateTime(validoEn));

		return biCotizacion;
	}	

	@Override
	public BitemporalInciso getIncisoInProcess(Long incisoContinuityId, Date validoEn) {
		BitemporalInciso biInciso = entidadBitemporalService.getInProcessByKey(IncisoContinuity.class, incisoContinuityId
				, TimeUtils.getDateTime(validoEn));

		return biInciso;
	}


	@Override
	public BitemporalAutoInciso getAutoIncisoInProcess(BitemporalInciso biInciso, Date validoEn) {

		//BitemporalAutoInciso biAutoInciso = biInciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().getInProcess(TimeUtils.getDateTime(validoEn));
		Long autoIncisoContinuityId = null;
		if(biInciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().getInProcess(TimeUtils.getDateTime(validoEn))!=null){
			autoIncisoContinuityId =  biInciso.getContinuity().getAutoIncisoContinuity().getId();
		}
		BitemporalAutoInciso biAutoInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalAutoInciso.class, autoIncisoContinuityId, biInciso.getContinuity().getId(), TimeUtils.getDateTime(validoEn));
		BitemporalAutoInciso biAutoIncisoEditable = (BitemporalAutoInciso) biAutoInciso.copyWith(new IntervalWrapper(TimeUtils.from(TimeUtils.getDateTime(validoEn))), true);
		biAutoIncisoEditable.setId(null);
		return biAutoIncisoEditable;
	}

	@Override
	public Collection<BitemporalSeccionInciso> getLstSeccionIncisoInProcess(BitemporalInciso biInciso, Date validoEn) {
		Collection<BitemporalSeccionInciso> lstBiSeccionInciso = biInciso.getContinuity().getIncisoSeccionContinuities().
		getInProcess(TimeUtils.getDateTime(validoEn));

		return lstBiSeccionInciso;
	}
	
	@Override
	public Long getMaxNumeroSecuencia(Long idToCotizacion, DateTime validFromDT, DateTime recordFromDT) {

		Long numIncisos = 0L;

		Collection<BitemporalInciso> lstIncisos = new ArrayList<BitemporalInciso>();
		CotizacionContinuity cotizacionContinuity = entidadContinuityService.findContinuityByBusinessKey(
				CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, idToCotizacion);

		BitemporalCotizacion biCotizacion = cotizacionContinuity.getCotizaciones().get(validFromDT, recordFromDT);

		if (biCotizacion == null) {
			Collection<BitemporalCotizacion> collectionBI = 
				entidadBitemporalService.obtenerCancelados(BitemporalCotizacion.class, cotizacionContinuity.getId(), 
						validFromDT, recordFromDT);
			if (!collectionBI.isEmpty()) {
				biCotizacion = collectionBI.iterator().next();	
			}
		}

		lstIncisos = biCotizacion.getContinuity().getIncisoContinuities().get(validFromDT, recordFromDT);


		// Cancelados
		if (lstIncisos.isEmpty() && recordFromDT != null) {
			Collection<IncisoContinuity> collectionSIC = entidadContinuityService.findContinuitiesByParentKey(IncisoContinuity.class, 
					IncisoContinuity.PARENT_KEY_NAME, biCotizacion.getContinuity().getId());
			if (!collectionSIC.isEmpty()) {
				//SeccionIncisoContinuity sic = collectionSIC.iterator().next(); 
				//Long bsi_continuitiId = sic.getId();
				lstIncisos = entidadBitemporalService.obtenerCancelados(collectionSIC, BitemporalInciso.class, 
						validFromDT, recordFromDT);	
			}
		}

		if (lstIncisos != null & !lstIncisos.isEmpty()) {
			ArrayList<BitemporalInciso> list = new ArrayList<BitemporalInciso>(1);
			list.addAll(lstIncisos);
			for(BitemporalInciso item : list){
				if(item.getValue().getNumeroSecuencia().longValue() > numIncisos){
					numIncisos =  item.getValue().getNumeroSecuencia().longValue();
				}
			}
		}

		return numIncisos;	
	}


	@EJB
	private EntidadBitemporalService entidadBitemporalService;	

	@EJB
	private EntidadService entidadService;


	@EJB
	private EntidadContinuityService entidadContinuityService;

	@EJB
	private ListadoIncisosDinamicoService listadoIncisosDinamicoService;

	@EJB
	private CalculoService calculoService;
	
	@EJB
	protected EntidadContinuityDao entidadContinuityDao;
	
	@EJB
	private CoberturaService coberturaService;

	public void setEntidadBitemporalService(EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}	

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}	

	public void setCoberturaService(CoberturaService coberturaService) {
		this.coberturaService = coberturaService;
	}

	public void setEntidadContinuityService(EntidadContinuityService entidadContinuityService){
		this.entidadContinuityService = entidadContinuityService;
	}

	public void setListadoIncisosDinamicoService(
			ListadoIncisosDinamicoService listadoIncisosDinamicoService) {
		this.listadoIncisosDinamicoService = listadoIncisosDinamicoService;
	}

	public CalculoService getCalculoService() {
		return calculoService;
	}

	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}	

}
