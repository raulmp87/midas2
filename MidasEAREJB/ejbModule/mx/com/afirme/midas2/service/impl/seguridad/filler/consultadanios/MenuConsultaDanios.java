package mx.com.afirme.midas2.service.impl.seguridad.filler.consultadanios;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

public class MenuConsultaDanios {
    	private List<Menu> listaMenu = null;
	
	public MenuConsultaDanios() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"m1","Emision", "Menu ppal Emision", null, true);
		listaMenu.add(menu);				
		
		menu = new Menu(new Integer("2"),"m1_1","Autos", "Submenu Autos", null, false);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m1_2","Vida", "Submenu Vida", null, false);
		listaMenu.add(menu);
						
		menu = new Menu(new Integer("4"),"m1_3","Da�os", "Submenu Da�os", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m1_3_4","Emisiones", "Submenu Emisiones", null, true);
		listaMenu.add(menu);				
		
		menu = new Menu(new Integer("6"),"m7","Ayuda", "Submenu Ayuda", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("7"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("8"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);		
				
		return this.listaMenu;
		
	}
}
