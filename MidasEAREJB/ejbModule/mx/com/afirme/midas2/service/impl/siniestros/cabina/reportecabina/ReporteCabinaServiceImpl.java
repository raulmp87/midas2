package mx.com.afirme.midas2.service.impl.siniestros.cabina.reportecabina;

import static mx.com.afirme.midas2.utils.CommonUtils.isNullOrEmpty;
import static mx.com.afirme.midas2.utils.CommonUtils.isNullOrZero;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.cabina.reportecabina.ReporteCabinaDao;
import mx.com.afirme.midas2.domain.Coordenadas;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.personadireccion.PaisMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.BitacoraEventoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.BitacoraEventoSiniestro.TipoEvento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoEstimacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina.EstatusReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroDanosMateriales;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicos;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicosConductor;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCBienes;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCPersonas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCViajero;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.cita.CitaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas.TipoLugar;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.dto.impresiones.CoberturaImporteDTO;
import mx.com.afirme.midas2.dto.impresiones.ImpresionReporteCabinaDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.BitacoraEventoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ComplementoReporteDTO;
import mx.com.afirme.midas2.dto.siniestros.CreacionReporteDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosReporteSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.EstimacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ReporteSiniestroRoboDTO;
import mx.com.afirme.midas2.dto.siniestros.TerceroDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.ReporteSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.SiniestroCabinaDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.sipac.ReporteLayoutSipacService;
import mx.com.afirme.midas2.service.sistema.EventoService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.utils.CommonUtils;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.js.util.StringUtil;

/**
 * @author User
 * 
 */
@Stateless
public class ReporteCabinaServiceImpl implements ReporteCabinaService {
	
	private static final String FN_NO_AFECTACION_RESERVA = "FN_M2_SN_No_Permite_Afectacion_Reserva";

	private static final int	LONG_LARGA_ANIO	= 4;

	private static final int	LONG_CORTA_ANIO	= 2;
	
	private static final Logger log = Logger.getLogger(ReporteCabinaServiceImpl.class);

	@EJB
	private EntidadService entidadService;

	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	@Resource
	private Validator validator;

	@EJB
	private ReporteCabinaDao reporteCabinaDao;

	@EJB
	private SiniestroCabinaService siniestroCabinaService;

	@EJB
	private EventoService eventoService;

	@EJB
	private PolizaSiniestroService polizaSiniestroService;

	@EJB
	private EnvioNotificacionesService notificacionesService;

	@EJB
	private ReporteCabinaService reporteCabinaService;

	@EJB
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;

	@EJB
	private ListadoService listadoService;
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;
	
	@EJB
	private IncisoViewService incisoViewService;
	
	@EJB
	private ParametroGlobalService parametroGlobalService;
	
	@EJB
	private ReporteLayoutSipacService reporteLayoutService;	

	@Override
	public ReporteCabina buscarReporte(Long numReporte) {		
		return entidadService.findById(ReporteCabina.class, numReporte);
	}

	@Override
	public ReporteCabina buscarReporte(String nsOficina, String nsConsecutivoR,
			String nsAnio) {

		if (isNullOrEmpty(nsOficina) && isNullOrEmpty(nsConsecutivoR) && isNullOrEmpty(nsAnio)) {
			return null;
		}
		HashMap<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("claveOficina", nsOficina);
		filtro.put("consecutivoReporte", nsConsecutivoR);
		filtro.put("anioReporte", nsAnio);

		List<ReporteCabina> list = entidadService.findByProperties(
				ReporteCabina.class, filtro);		
		if (!list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}
	
	@Override
	public ReporteCabina buscarReportePorSiniestro(String nsOficina, String nsConsecutivoR,
			String nsAnio) {

		if (isNullOrEmpty(nsOficina) && isNullOrEmpty(nsConsecutivoR) && isNullOrEmpty(nsAnio)) {
			return null;
		}

		HashMap<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("claveOficina", nsOficina);
		filtro.put("consecutivoReporte", nsConsecutivoR);
		filtro.put("anioReporte", nsAnio);

		List<SiniestroCabina> list = entidadService.findByProperties(
				SiniestroCabina.class, filtro);		
		if (!list.isEmpty()) {
			return list.get(0).getReporteCabina();
		}
		return null;
	}

	@Override
	public void salvarCita(CitaReporteCabina cita, Long numReporte) {
		ReporteCabina reporte = buscarReporte(numReporte);
		Boolean nuevaCita = Boolean.FALSE;

		if (reporte.getCitaReporteCabina() == null) {
			nuevaCita = Boolean.TRUE;
		}
		reporte.setCitaReporteCabina(cita);
		entidadService.save(reporte);

		if (nuevaCita) {
			guardarEvento(
					reporte.getId(),
					TipoEvento.EVENTO_4,
					"Se programa cita " + cita.getHoraCita() + "/"
							+ cita.getFechaCita());
		} else {
			guardarEvento(
					reporte.getId(),
					TipoEvento.EVENTO_5,
					"Se reprograma cita " + cita.getHoraCita() + "/"
							+ cita.getFechaCita());
		}

	}

	@Override
	public LugarSiniestroMidas mostrarLugarSiniestro(Long idReporte,
			TipoLugar tipo) {
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class,
				idReporte);
		if (tipo == TipoLugar.AT) {
			return reporte.getLugarAtencion();
		}
		return reporte.getLugarOcurrido();
	}

	@Override
	public void guardarLugarSiniestro(Long idReporte,
			LugarSiniestroMidas lugar, TipoLugar tipo) {
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class,
				idReporte);
		lugar.setTipo(tipo.toString());
		if (lugar.getTipo().equals(TipoLugar.AT.toString())) {
			reporte.setLugarAtencion(lugar);
		} else {
			reporte.setLugarOcurrido(lugar);
		}
		reporte.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual()
				.getNombreUsuario());
		reporte.setFechaModificacion(new Date());
		entidadService.save(lugar);
		if (lugar.getTipo().equals(TipoLugar.OC.toString())) {
			this.reporteLayoutService.actualizaLugarOcurrido(reporte);
		}
		
	}

	@Override
	public Long guardarLugarSiniestro(Long idReporte, String nombre,
			String lada, String telefono, LugarSiniestroMidas lugarCapturado,
			TipoLugar tipo) {
		Set<ConstraintViolation<LugarSiniestroMidas>> constraintViolations = validator
				.validate(lugarCapturado);
		Boolean isNuevo = Boolean.FALSE;

		if (constraintViolations.size() > 0) {
			throw new ConstraintViolationException(
					new HashSet<ConstraintViolation<?>>(constraintViolations));
		}
		ReporteCabina reporte = null;
		if (isNullOrZero(idReporte)) {
			// Si no existe, se crea el reporte
			reporte = new ReporteCabina();
			reporte.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual()
					.getNombreUsuario());
			reporte.setFechaCreacion(new Date());
			reporte.setPersonaReporta(nombre); // Se actualiza la informacion
			reporte.setTelefono(telefono);
			reporte.setLadaTelefono(lada);
		} else { // De lo contrario se busca
			reporte = entidadService.findById(ReporteCabina.class, idReporte);
		}

		if (tipo.equals(TipoLugar.AT)) {
			if (reporte.getLugarAtencion() == null) {
				isNuevo = Boolean.TRUE;
			}
		} else if (tipo.equals(TipoLugar.OC)) {
			if (reporte.getLugarOcurrido() == null) {
				isNuevo = Boolean.TRUE;
			}
		}

		lugarCapturado.setTipo(tipo.toString());
		LugarSiniestroMidas lugarGuardar = null;
		if (lugarCapturado.getTipo().equals(TipoLugar.AT.toString())) {
			LugarSiniestroMidas lugarBD = reporte.getLugarAtencion();
			lugarGuardar = cargarInformacionLugarSiniestro(lugarBD,
					lugarCapturado);
			reporte.setLugarAtencion(lugarGuardar);
			if (reporte.getLugarOcurrido() == null){
				reporte.setLugarOcurrido(copiarLugar(lugarCapturado));

			}
		} else {
			LugarSiniestroMidas lugarBD = reporte.getLugarOcurrido();
			lugarGuardar = cargarInformacionLugarSiniestro(lugarBD,
					lugarCapturado);
			reporte.setLugarOcurrido(lugarGuardar);
		}
		Integer EN_PROCESO = new Integer(1);
		reporte.setEstatus(EN_PROCESO);
		reporte.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual()
				.getNombreUsuario());
		reporte.setFechaModificacion(new Date());
		Long idReporteAux = (Long) entidadService.saveAndGetId(reporte);

		if (isNuevo) {
			if (tipo.equals(TipoLugar.AT)) {
				guardarEvento(idReporteAux, TipoEvento.EVENTO_8,
						"Se Registro el Lugar PENDIENTE");
			} else if (tipo.equals(TipoLugar.OC)) {
				guardarEvento(idReporteAux, TipoEvento.EVENTO_10,
						"Se Modifico el Lugar a PENDIENTE");
			}

		} else {
			if (tipo.equals(TipoLugar.AT)) {
				guardarEvento(idReporteAux, TipoEvento.EVENTO_9,
						"Se Registro el Lugar PENDIENTE");
			} else if (tipo.equals(TipoLugar.OC)) {
				guardarEvento(idReporteAux, TipoEvento.EVENTO_11,
						"Se Modifico el Lugar a PENDIENTE");
			}
		}

		return idReporteAux;
	}

	/**
	 * Carga la información capturada en la interfaz al lugar midas que se va a
	 * persistir
	 * 
	 * @param lugarBD
	 *            informacion del lugar contenido en el reporte que estaba en la
	 *            base de datos
	 * @param lugarCapturado
	 *            informacion del lugar que capturo el usuario
	 * @return lugar con la información a persistir
	 */
	private LugarSiniestroMidas cargarInformacionLugarSiniestro(
			LugarSiniestroMidas lugarBD, LugarSiniestroMidas lugarCapturado) {
		LugarSiniestroMidas lugarGuardar = lugarBD;
		if (lugarGuardar == null || lugarGuardar.getId() == 0L) {
			lugarGuardar = lugarCapturado;
		} else {
			lugarGuardar.setCalleNumero(lugarCapturado.getCalleNumero());
			lugarGuardar.setCiudad(lugarCapturado.getCiudad());
			lugarGuardar.setCodigoPostalOtraColonia(lugarCapturado
					.getCodigoPostalOtraColonia());
			lugarGuardar.setEstado(lugarCapturado.getEstado());
			lugarGuardar.setKilometro(lugarCapturado.getKilometro());
			lugarGuardar
					.setNombreCarretera(lugarCapturado.getNombreCarretera());
			lugarGuardar.setOtraColonia(lugarCapturado.getOtraColonia());
			lugarGuardar.setPais(lugarCapturado.getPais());
			lugarGuardar.setReferencia(lugarCapturado.getReferencia());
			lugarGuardar.setTipoCarretera(lugarCapturado.getTipoCarretera());
			lugarGuardar.setZona(lugarCapturado.getZona());
			lugarGuardar.setCoordenadas(lugarCapturado.getCoordenadas());
		}
		if (lugarCapturado.getIdColoniaCheck()
				|| lugarCapturado.getZona().equals("CA")) {
			lugarGuardar.setCodigoPostal("99999");
			lugarGuardar.setColonia(entidadService.findById(ColoniaMidas.class,
					"99999-1"));
		} else {
			lugarGuardar.setCodigoPostal(lugarCapturado.getCodigoPostal());
			lugarGuardar.setColonia(lugarCapturado.getColonia());
		}
		return lugarGuardar;
	}

	@Override
	public void salvarReporte(ReporteCabina reporteCabina) {	
		// Es al registrar un nuevo reporte de siniestro,
		// con la información minima requerida Reporta, Teléfono y lugar de
		// atención.
		ReporteCabina reporteGuardado = null;
		Usuario usuario = usuarioService.getUsuarioActual();
		String codigoUsuario = usuario.getNombreUsuario();		
		boolean seAsignoAjustador = false;
		boolean esNuevo = false;
		
		if (reporteCabina.getCodigoUsuarioCreacion() == null) {
			reporteCabina.setCodigoUsuarioCreacion("");
			reporteCabina.setCodigoUsuarioCreacion(codigoUsuario);
		} else if (reporteCabina.getCodigoUsuarioCreacion().isEmpty()) {
			reporteCabina.setCodigoUsuarioCreacion(codigoUsuario);
		}
		reporteCabina.setCodigoUsuarioModificacion(codigoUsuario);
		//posible error de creación en consecutivo_reporte. Si no tiene ID reporte entonces es nuevo, asignarle un consecutivo
		if (reporteCabina.getId() == null){
			reporteCabina.setConsecutivoReporte(this.obtenerConsecutivoReporte(reporteCabina));
		}
		/*
		if (reporteCabina.getConsecutivoReporte() == null
				|| reporteCabina.getConsecutivoReporte().isEmpty()
				|| !reporteCabina.getConsecutivoReporte().matches("[0-9]*")) {
			reporteCabina.setConsecutivoReporte(this
					.obtenerConsecutivoReporte(reporteCabina));
		}*/

		String numeroReporte = this.construirNumeroReporte(reporteCabina);

		if (!reporteCabina.getNumeroReporte().equals(numeroReporte)
				|| reporteCabina.getNumeroReporte().isEmpty()
				|| reporteCabina.getNumeroReporte().equals("")) {
			reporteCabina.setNumeroReporte(numeroReporte);			
		}
		if (reporteCabina.getId() == null) {
			reporteCabina.setFechaHoraReporte(new Date());
		}

		if (CommonUtils.isNullOrEmpty(reporteCabina.getAnioReporte())){
			reporteCabina.setAnioReporte(obtenerAnioActual());
		}
		
		if (reporteCabina.getEstatus() == null) {
			reporteCabina.setEstatus(EstatusReporteCabina.TRAMITE_REPORTE.getValue());
		}
			

		String apellidoPaterno = (usuario.getApellidoPaterno() == null) ? ""
				: usuario.getApellidoPaterno();
		String apellidoMaterno = (usuario.getApellidoMaterno() == null) ? ""
				: usuario.getApellidoMaterno();
		reporteCabina.setAtendidoPor(usuario.getNombre() + " "
				+ apellidoPaterno + " " + apellidoMaterno);

		Oficina oficina = entidadService.findById(Oficina.class, reporteCabina
				.getOficina().getId());
		reporteCabina.setOficina(oficina);
		reporteCabina.setClaveOficina(oficina.getClaveOficina());

		if (reporteCabina.getAjustador() != null && reporteCabina.getAjustador().getId() != null) {
			ServicioSiniestro ajustador = entidadService.findById(
					ServicioSiniestro.class, reporteCabina.getAjustador()
							.getId());
			reporteCabina.setAjustador(ajustador);
		} else {
			reporteCabina.setAjustador(null);
		}
		
		//determinar fecha de asignacion de ajustador
		if(reporteCabina.getId() != null){
			reporteGuardado = entidadService.findById(ReporteCabina.class, reporteCabina.getId());
			reporteCabina.setOficina(reporteGuardado.getOficina());
			reporteCabina.setClaveOficina(reporteGuardado.getClaveOficina());
			reporteCabina.setConsecutivoReporte(reporteGuardado.getConsecutivoReporte());
			reporteCabina.setAnioReporte(reporteGuardado.getAnioReporte());
			if((reporteGuardado.getAjustador() == null || reporteGuardado.getAjustador().getId() == null) && 
					(reporteCabina.getAjustador() != null && reporteCabina.getAjustador().getId() != null)){
				reporteCabina.setFechaHoraAsignacion(new Date());
				seAsignoAjustador = true;
			}else if((reporteGuardado.getAjustador() != null && reporteGuardado.getAjustador().getId() != null) && 
					(reporteCabina.getAjustador() != null && reporteCabina.getAjustador().getId() != null)){ 				
				if(reporteGuardado.getAjustador().getId().longValue() != reporteCabina.getAjustador().getId().longValue()){
					guardarEvento(reporteCabina.getId(), TipoEvento.EVENTO_7,
						"Se modifico ajustador por :"
								+ reporteCabina.getAjustador()
										.getPersonaMidas().getNombre());			
					eventoService.guardarEvento("reporteSiniestroAsignado",
							"{\"reporteSiniestroId\":" + reporteCabina.getId() + ", \"ajustadorId\":"+reporteCabina.getAjustador().getIdAjustador().toString()
								+", \"oldAjustadorId\":" + reporteGuardado.getAjustador().getId().toString() + "}");
				}
			}			
		}else{
			esNuevo = true;
			if(reporteCabina.getAjustador() != null && reporteCabina.getAjustador().getId() != null){
				reporteCabina.setFechaHoraAsignacion(new Date());
				seAsignoAjustador = true;
			}
		}
	
		entidadService.save(reporteCabina);		
		validateBitacoraAjustador(reporteCabina, esNuevo, seAsignoAjustador);

	}

	@Override
	public void salvarInformacionAdicional(Long idReporte,
			String informacionAdicional) {
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class,
				idReporte);
		reporte.setInformacionAdicional(informacionAdicional);
		entidadService.save(reporte);
		guardarEvento(
				idReporte,
				TipoEvento.EVENTO_12,
				"Se guardo Informacion adicional del reporte :"
						+ reporte.getNumeroReporte());
	}

	@Override
	public void salvarDeclaracionSiniestro(Long idReporte,
			String declaracionSiniestro) {
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class,
				idReporte);
		reporte.setDeclaracionTexto(declaracionSiniestro);
		entidadService.save(reporte);
	}
	
	@Override
	public Long contarReporteSiniestro(
			ReporteSiniestroDTO reporteSiniestroDTO) {
		return reporteCabinaDao.contarReporteSiniestro(reporteSiniestroDTO);
	}

	@Override
	public List<ReporteSiniestroDTO> buscarReportes(
			ReporteSiniestroDTO reporteSiniestroDTO) {
		List<ReporteSiniestroDTO> listReporteCabina = reporteCabinaDao
				.buscarReporteSiniestro(reporteSiniestroDTO);	
		return listReporteCabina;
	}

	@Override
	public String obtenerConsecutivoReporte(ReporteCabina reporteCabina) {
		return reporteCabinaDao.obtenerConsecutivoReporte(reporteCabina);
	}

	public String construirNumeroReporte(ReporteCabina reporteCabina) {
		String numeroReporte = "";
		Oficina oficina = entidadService.findById(Oficina.class, reporteCabina
				.getOficina().getId());
		numeroReporte += oficina.getClaveOficina();
		numeroReporte += "-";
		numeroReporte += reporteCabina.getConsecutivoReporte();
		numeroReporte += "-";
		numeroReporte += obtenerAnioActual();

		return numeroReporte;
	}

	private String obtenerAnioActual() {
		String date = obtenerFechaActual();
		String anio = date.substring(date.length() - LONG_CORTA_ANIO, date.length());
		return anio;
	}

	@SuppressWarnings("unused")
	private String obtenerAnioActualCompleto() {
		String date = obtenerFechaActual();
		String anio = date.substring(date.length() - LONG_LARGA_ANIO, date.length());
		return anio;
	}

	private String obtenerFechaActual() {
		DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String date = sdf.format(new Date());
		return date;
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public ReporteCabinaDao getReporteCabinaDao() {
		return reporteCabinaDao;
	}

	public void setReporteCabinaDao(ReporteCabinaDao reporteCabinaDao) {
		this.reporteCabinaDao = reporteCabinaDao;
	}

	@Override
	public Oficina obtenerOficinaReporte(ReporteCabina reporteCabina) {
		return entidadService.findById(Oficina.class, reporteCabina
				.getOficina().getId());
	}

	@Override
	public List<BitacoraEventoSiniestroDTO> buscarEventos(
			BitacoraEventoSiniestroDTO bitacoraEventoSiniestroDTO,
			Long idReporte) {
		List<BitacoraEventoSiniestroDTO> bitacora = new ArrayList<BitacoraEventoSiniestroDTO>();
		bitacora = reporteCabinaDao.buscarEventos(bitacoraEventoSiniestroDTO,
				idReporte);
		
		//Se ordena en este punto debido a que la bitacora es llenada invocando diferentes fuentes.
		//ordenado por Fecha/Hora de forma descendente
		Collections.sort(bitacora, new Comparator<BitacoraEventoSiniestroDTO>() {
	        private DateFormat f = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	        @Override
	        public int compare(BitacoraEventoSiniestroDTO o1, BitacoraEventoSiniestroDTO o2) {
	            try {
	                return f.parse(o2.getFechaHora()).compareTo(f.parse(o1.getFechaHora()));
	            } catch (ParseException e) {
	                throw new IllegalArgumentException(e);
	            }
	        }
	    });
		
		return bitacora;
	}

	@Override
	public void guardarEvento(Long idReporte, TipoEvento tipoEvento,
			String descripcion) {
		BitacoraEventoSiniestro bitacora = new BitacoraEventoSiniestro();
		bitacora.setFechaCreacion(new Date());
		bitacora.setFechaModificacion(new Date());
		bitacora.setCodigoUsuarioCreacion(String.valueOf(usuarioService
				.getUsuarioActual().getNombreUsuario()));
		bitacora.setDescripcion(descripcion);
		bitacora.setIdEvento(tipoEvento.toString());
		bitacora.setReporteCabina(this.buscarReporte(idReporte));
		entidadService.save(bitacora);
	}

	@Override
	public Long obtenerIncisoReporteCabina(Long id) {
		return reporteCabinaDao.obtenerIncisoReporteCabina(id);
	}

	private void validateBitacoraAjustador(ReporteCabina reporteCabina, boolean esNuevo, boolean seAsignoAjustador) {		
		Long idReporte = reporteCabina.getId();
		if (esNuevo) {
			guardarEvento(reporteCabina.getId(), TipoEvento.EVENTO_3,
					"Se guardo el Reporte " + reporteCabina.getNumeroReporte());
			notificacionesService
					.notificacionCabinaCreacionReporte(reporteCabina);			
		} 
		
		if(seAsignoAjustador){
			eventoService.guardarEvento("reporteSiniestroAsignado",
				"{\"reporteSiniestroId\":" + idReporte.toString() + ", \"ajustadorId\":"+reporteCabina.getAjustador().getIdAjustador().toString()
					+", \"oldAjustadorId\":null}");
			notificacionesService
				.notificacionCabinaAsignacionAjustador(reporteCabina);
			guardarEvento(idReporte, TipoEvento.EVENTO_6,
					"Se asigno ajustador :"
					+ reporteCabina.getAjustador()
							.getPersonaMidas().getNombre());
		}	
	}

	@SuppressWarnings("unused")
	@Deprecated
	private List<ReporteSiniestroDTO> transformReporteCabinaToReporteSiniestrDTO(
			List<ReporteCabina> listReporteCabina) {
		List<ReporteSiniestroDTO> listaReporteSiniestroDTO = new ArrayList<ReporteSiniestroDTO>();
		ReporteSiniestroDTO reporteSiniestroDTO = null;
		Map<String, Object> params = null;
		List<IncisoReporteCabina> listIncisoReporteCabina = null;
		List<AutoIncisoReporteCabina> listAutoIncisoReporteCabina = null;
		List<SiniestroCabina> listSiniestroCabina = null;
		IncisoReporteCabina incisoReporteCabina = null;
		AutoIncisoReporteCabina autoIncisoReporteCabina = null;
		SiniestroCabina siniestroCabina = null;

		for (ReporteCabina reporteCabina : listReporteCabina) {
			reporteSiniestroDTO = new ReporteSiniestroDTO();
			reporteSiniestroDTO.setReporteCabinaId(reporteCabina.getId());
			reporteSiniestroDTO.setNombreOficina(reporteCabina.getOficina()
					.getNombreOficina());
			reporteSiniestroDTO.setOficinaId(reporteCabina.getOficina().getId());
			reporteSiniestroDTO.setFechaSiniestro(reporteCabina
					.getFechaOcurrido());
			reporteSiniestroDTO.setNumeroReporte(reporteCabina
					.getNumeroReporte());
			reporteSiniestroDTO
					.setClaveOficina(reporteCabina.getClaveOficina());
			reporteSiniestroDTO.setConsecutivoReporte(reporteCabina
					.getConsecutivoReporte());
			reporteSiniestroDTO.setAnioReporte(reporteCabina.getAnioReporte());
			reporteSiniestroDTO.setNombrePersona(reporteCabina.getPersonaReporta());
			reporteSiniestroDTO.setNombreConductor(reporteCabina.getPersonaConductor());
			reporteSiniestroDTO.setEstatus(reporteCabina.getEstatus());
			
			if (reporteSiniestroDTO.getEstatus() != null) {
				CatValorFijo valor = catalogoGrupoValorService.obtenerValorPorCodigo(
							CatGrupoFijo.TIPO_CATALOGO.ESTATUS_REPORTE_CABINA, reporteSiniestroDTO.getEstatus().toString());
				if (valor != null) {
					reporteSiniestroDTO.setEstatusDesc(valor.getDescripcion());
				}
			}
			

			if (reporteCabina.getPoliza() != null) {
				reporteSiniestroDTO.setNumeroPoliza(reporteCabina.getPoliza()
						.getNumeroPolizaFormateada());
			}

			params = new HashMap<String, Object>();
			params.put("seccionReporteCabina.reporteCabina.id",
					reporteCabina.getId());
			listIncisoReporteCabina = entidadService.findByProperties(
					IncisoReporteCabina.class, params);

			if (listIncisoReporteCabina != null
					&& !listIncisoReporteCabina.isEmpty()) {
				incisoReporteCabina = listIncisoReporteCabina.get(0);
			} else {
				incisoReporteCabina = null;
			}

			if (incisoReporteCabina != null) {
				reporteSiniestroDTO.setNombreAsegurado(incisoReporteCabina
						.getNombreAsegurado());
				reporteSiniestroDTO.setNumeroInciso(incisoReporteCabina
						.getNumeroInciso());

				params = new HashMap<String, Object>();
				params.put("incisoReporteCabina.id",
						incisoReporteCabina.getId());
				listAutoIncisoReporteCabina = entidadService.findByProperties(
						AutoIncisoReporteCabina.class, params);

				if (listAutoIncisoReporteCabina != null
						&& !listAutoIncisoReporteCabina.isEmpty()) {
					autoIncisoReporteCabina = listAutoIncisoReporteCabina
							.get(0);
					reporteSiniestroDTO.setNumeroSerie(autoIncisoReporteCabina
							.getNumeroSerie());
				}
			}

			params = new HashMap<String, Object>();
			params.put("reporteCabina.id", reporteCabina.getId());
			listSiniestroCabina = entidadService.findByProperties(
					SiniestroCabina.class, params);

			if (listSiniestroCabina != null && !listSiniestroCabina.isEmpty()) {
				siniestroCabina = listSiniestroCabina.get(0);
				reporteSiniestroDTO.setNumeroSiniestro(siniestroCabina
						.getNumeroSiniestro());
				reporteSiniestroDTO.setSiniestroCabinaId(siniestroCabina.getId());
			}

			listaReporteSiniestroDTO.add(reporteSiniestroDTO);

		}

		return listaReporteSiniestroDTO;
	}

	/**
	 * Método que regresa los datos generales de un reporte de cabina. Estos
	 * datos se utilizan en muchas pantallas.
	 * 
	 * @param idReporteCabina
	 * @return
	 */
	@Override
	public DatosReporteSiniestroDTO obtenerDatosReporte(Long idReporteCabina) {
		DatosReporteSiniestroDTO dto = new DatosReporteSiniestroDTO();
		ReporteCabina reporteCabina = this.entidadService.findById(
				ReporteCabina.class, idReporteCabina);
		dto.setNumeroReporte(reporteCabina.getNumeroReporte());
		dto.setReporteCabinaId(reporteCabina.getId());
		List<SiniestroCabina> siniestrosCabina = this.entidadService
				.findByProperty(SiniestroCabina.class, "reporteCabina.id",
						idReporteCabina);
		if (siniestrosCabina.size() > 0) {
			SiniestroCabina siniestroCabina = siniestrosCabina.get(0);
			dto.setNumeroSiniestro(siniestroCabina.getNumeroSiniestro());
		}
		PolizaDTO p = reporteCabina.getPoliza();
		if( p != null ){
			PolizaDTO poliza = this.entidadService.findById(
					PolizaDTO.class,p.getIdToPoliza());
			dto.setNumeroPoliza(poliza.getNumeroPolizaFormateada());
		}
		/*Integer numeroInciso = reporteCabina.getSeccionReporteCabina()
				.getIncisoReporteCabina().getNumeroInciso();*/
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("seccionReporteCabina.reporteCabina.id", idReporteCabina);
		
		List<IncisoReporteCabina> lstIncisos = entidadService.findByProperties(IncisoReporteCabina.class, params);
		
		if (lstIncisos!= null && !lstIncisos.isEmpty()) {
			dto.setNumeroInciso(lstIncisos.get(0).getNumeroInciso());
		}
		
		return dto;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteCabina(Long idReporteCabina) {

		GeneradorImpresion gImpresion = new GeneradorImpresion();
		ImpresionReporteCabinaDTO impReporte = new ImpresionReporteCabinaDTO();
		List<ImpresionReporteCabinaDTO> dataSourceReporte = new ArrayList<ImpresionReporteCabinaDTO>();
		List<LugarSiniestroMidas> dataSourceLugarOcurrido = new ArrayList<LugarSiniestroMidas>();
		List<LugarSiniestroMidas> dataSourceLugarAtencion = new ArrayList<LugarSiniestroMidas>();
		impReporte = this.cargarImpresionReporteCabinaDTO(idReporteCabina);

		// Compilado de jReports y generación del .jasper
		String jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/reporteSiniestroCabinav2.jrxml";
		JasperReport jReport = gImpresion.getOJasperReport(jrxml);

		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/reporteSiniestroCabina_subreport1.jrxml";
		JasperReport jVehiculos = gImpresion.getOJasperReport(jrxml);

		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/reporteSiniestroCabina_subreport2.jrxml";
		JasperReport jMedicos = gImpresion.getOJasperReport(jrxml);

		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/reporteSiniestroCabina_subreport3.jrxml";
		JasperReport jBienes = gImpresion.getOJasperReport(jrxml);
		
		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/reporteSiniestroCabina_subLugarCiudad.jrxml";
		JasperReport jLugarCiudad = gImpresion.getOJasperReport(jrxml);

		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/reporteSiniestroCabina_subLugarCarretera.jrxml";
		JasperReport jLugarCarretera = gImpresion.getOJasperReport(jrxml);
		
		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/reporteSiniestroCabina_subEstimaciones.jrxml";
		JasperReport jAfectaciones = gImpresion.getOJasperReport(jrxml);
		
		List<TerceroDTO> DSVehiculos = impReporte.getPasesVehiculos();
		List<TerceroDTO> DSMedicos = impReporte.getPasesMedicos();
		List<TerceroDTO> DSBienes = impReporte.getPasesBienes();
		List<CoberturaImporteDTO> DSAfectaciones = impReporte.getAfectaciones();

		if (DSVehiculos.size() == 0) {
			DSVehiculos = null;
		}
		if (DSMedicos.size() == 0) {
			DSMedicos = null;
		}
		if (DSBienes.size() == 0) {
			DSBienes = null;
		}
		if (DSAfectaciones.size() == 0) {
			DSAfectaciones = null;
		}
		

		// Se definen los parámetros a mapear en el jrxml
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", "/mx/com/afirme/midas2/service/impl/impresiones/img/logo_pasesatencion.png");

		params.put("jVehiculos", jVehiculos);
		params.put("pasesVehiculos", DSVehiculos);

		params.put("jMedicos", jMedicos);
		params.put("pasesMedicos", DSMedicos);

		params.put("jBienes", jBienes);
		params.put("pasesBienes", DSBienes);
		
		params.put("jLugarCiudad", jLugarCiudad);
		params.put("jLugarCarretera", jLugarCarretera);
		params.put("jAfectaciones", jAfectaciones);
		params.put("afectaciones", DSAfectaciones);		

		dataSourceReporte.add(impReporte);		
		
		dataSourceLugarAtencion.add(impReporte.getReporteCabina().getLugarAtencion());		
		JRBeanCollectionDataSource collLugarAtencion = new JRBeanCollectionDataSource(dataSourceLugarAtencion);
		params.put("dsLugarAtencion", collLugarAtencion); 
		
		dataSourceLugarOcurrido.add(impReporte.getReporteCabina().getLugarOcurrido());		
		JRBeanCollectionDataSource collLugarOcurrido = new JRBeanCollectionDataSource(dataSourceLugarOcurrido);
		params.put("dsLugarOcurrido", collLugarOcurrido); 
		
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceReporte);
		
		// Rellena el reporte con los datos y exporta el PDF
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource,params);
	}

	private ImpresionReporteCabinaDTO cargarImpresionReporteCabinaDTO(
			Long idReporteCabina) {
		ImpresionReporteCabinaDTO impReporte = new ImpresionReporteCabinaDTO();
		try {
			ReporteCabina reporte = null;
			IncisoSiniestroDTO incisoSiniestro = new IncisoSiniestroDTO();
			List<EstimacionCoberturaReporteCabina> estimacionLst = new ArrayList<EstimacionCoberturaReporteCabina>();
			List<CoberturaReporteCabina> coberturasLst = new ArrayList<CoberturaReporteCabina>();			
			List<TerceroDTO> pasesVehiculos = new ArrayList<TerceroDTO>();
			List<TerceroDTO> pasesMedicos = new ArrayList<TerceroDTO>();
			List<TerceroDTO> pasesBienes = new ArrayList<TerceroDTO>();
			EstimacionCoberturaSiniestroDTO dto = new EstimacionCoberturaSiniestroDTO();

			// Se cargan los datos
			reporte = this.buscarReporte(idReporteCabina);
			String estatusReporte = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_REPORTE_CABINA).get(reporte.getEstatus().toString());
			impReporte.setEstatusReporte(estatusReporte); 
			impReporte.setReporteCabina(reporte);		
			impReporte.getReporteCabina().setNumeroReporte(reporte.getNumeroReporte());
			impReporte.setHoraDeImpresion("");
			impReporte.setUsuario(usuarioService.getUsuarioActual());		
			
			incisoSiniestro.setFechaReporteSiniestro(reporte.getFechaOcurrido());
			
			if (reporte.getPoliza() != null) {
				try {
					SiniestroCabinaDTO siniestro = siniestroCabinaService.obtenerSiniestroCabina(idReporteCabina);
					impReporte.setSiniestroCabina(siniestro);
					String idReporteCabinaStr = "incisoReporteCabina.seccionReporteCabina.reporteCabina.id";
					List<AutoIncisoReporteCabina> autoIncisoLst = entidadService
							.findByProperty(AutoIncisoReporteCabina.class,
									idReporteCabinaStr, reporte.getId());
					
					if (autoIncisoLst != null && !autoIncisoLst.isEmpty()) {
						String causaSiniestro = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.CAUSA_SINIESTRO).get(autoIncisoLst.get(0).getCausaSiniestro());
						
						impReporte.setCausaSiniestro(causaSiniestro);
					}
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("incisoReporteCabina.seccionReporteCabina.reporteCabina.id", reporte.getId());
					params.put("siniestrado", true);
					coberturasLst =  entidadService.findByProperties(CoberturaReporteCabina.class, params);
					
					for (CoberturaReporteCabina cobertura : coberturasLst) {
						CoberturaImporteDTO afectacion = new CoberturaImporteDTO();
						BigDecimal importe = movimientoSiniestroService.obtenerMontoEstimacionCobertura(cobertura.getId(), null);
						afectacion.setNombreCobertura(cobertura.getCoberturaDTO().getNombreComercial());
						if (importe != null) {
							afectacion.setImporteCobertura(importe.doubleValue());
						}
						
						impReporte.getAfectaciones().add(afectacion);
					}


					String property = "coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id";
					estimacionLst = entidadService.findByProperty(EstimacionCoberturaReporteCabina.class, property, reporte.getId());

					if (estimacionLst.size() > 0) {

						for (EstimacionCoberturaReporteCabina estimacion : estimacionLst) {
							TerceroDTO terceroDTO = this.cargarDetalletTercerDTO(estimacion);
							if (terceroDTO != null) {
								if (terceroDTO.getEsRCV()) {
									pasesVehiculos.add(terceroDTO);
								}
								
								if (terceroDTO.getEsGME() || terceroDTO.getEsGMC() || terceroDTO.getEsRCJ() || terceroDTO.getEsRCP()) {
									pasesMedicos.add(terceroDTO);
								}
								if (terceroDTO.getEsRCB()) {
									pasesBienes.add(terceroDTO);
								}
							}	
								
						}
					}

					impReporte.setPasesVehiculos(pasesVehiculos);
					impReporte.setPasesBienes(pasesBienes);
					impReporte.setPasesMedicos(pasesMedicos);

					impReporte.setEstimacionCoberturaSiniestroDTO(dto);
					incisoSiniestro = polizaSiniestroService.obtenerDatosIncisoAsignadoReporte(reporte.getId());
					impReporte.setIncisoSiniestro(incisoSiniestro);
				} catch(Exception e) {
					e.getMessage();
				}
			}
			
		
			

		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}

		return impReporte;
	}
	


	@SuppressWarnings("unused")
	private TerceroDTO cargarDetalletTercerDTO(EstimacionCoberturaReporteCabina estimacion) {
		TerceroDTO terceroDTO = new TerceroDTO();

		terceroDTO.setNombreCobertura(estimacionCoberturaSiniestroService.obtenerNombreCobertura(
				estimacion.getCoberturaReporteCabina().getCoberturaDTO(), estimacion.getCoberturaReporteCabina().getClaveTipoCalculo(), 
				estimacion.getCveSubCalculo(), estimacion.getTipoEstimacion(), estimacion.getCoberturaReporteCabina().getCoberturaDTO().getTipoConfiguracion()));
		
		terceroDTO.setEstimacionActual(movimientoSiniestroService.obtenerMontoEstimacionAfectacion(estimacion.getId()));
		
		if (EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, estimacion.getTipoEstimacion())) {
			TerceroDanosMateriales tercero = new TerceroDanosMateriales();
			tercero = entidadService.findById(TerceroDanosMateriales.class, estimacion.getId());
			
			terceroDTO.setEsDMA(true);
			PrestadorServicio taller = tercero.getTaller();
			PrestadorServicio ciaSeguros = tercero.getCompaniaSegurosTercero();
			terceroDTO.setNombreTercero(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getNombreAsegurado());
			//terceroDTO.setColor(autoIncisoReporteCabina.getColor());

			if (taller != null) {
				tercero.getTaller().getNombrePersona();
			} else if (ciaSeguros != null) {
				tercero.getCompaniaSegurosTercero().getNombrePersona();
			}
		} else if (EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, estimacion.getTipoEstimacion())) {
			TerceroGastosMedicosConductor tercero = new TerceroGastosMedicosConductor();
			tercero = entidadService.findById(TerceroGastosMedicosConductor.class, estimacion.getId());
			
			terceroDTO.setEsGMC(true);
			terceroDTO.setNombreTercero(tercero.getNombreAfectado());
			
			PrestadorServicio medico = tercero.getMedico();
			PrestadorServicio hospitalPs = tercero.getHospital();
			PrestadorServicio ciaSeguros = tercero.getCompaniaSegurosTercero();

			if (medico != null) {
				terceroDTO.setNombreMedico(tercero.getMedico().getNombrePersona());
				terceroDTO.setClaveMedico(tercero.getMedico().getId().toString());
			} 
			if (hospitalPs != null) {
				terceroDTO.setNombreHospital(tercero.getHospital().getNombrePersona());
				terceroDTO.setClaveHospital(tercero.getHospital().getId().toString());
			} else if (tercero.getOtroHospital() != null) {
				terceroDTO.setNombreHospital(tercero.getOtroHospital());
			}
			
		} else if (EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, estimacion.getTipoEstimacion())) {
			TerceroGastosMedicos tercero = new TerceroGastosMedicos();
			tercero = entidadService.findById(TerceroGastosMedicos.class, estimacion.getId());
			
			terceroDTO.setEsGME(true);
			terceroDTO.setNombreTercero(tercero.getNombreAfectado());
			
			PrestadorServicio medico = tercero.getMedico();
			PrestadorServicio hospitalPs = tercero.getHospital();
			PrestadorServicio ciaSeguros = tercero.getCompaniaSegurosTercero();

			if (medico != null) {
				terceroDTO.setNombreMedico(tercero.getMedico().getNombrePersona());
				terceroDTO.setClaveMedico(tercero.getMedico().getId().toString());
			} 
			if (hospitalPs != null) {
				terceroDTO.setNombreHospital(tercero.getHospital().getNombrePersona());
				terceroDTO.setClaveHospital(tercero.getHospital().getId().toString());
			} else if (tercero.getOtroHospital() != null) {
				terceroDTO.setNombreHospital(tercero.getOtroHospital());
			}
			
		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, estimacion.getTipoEstimacion())) {
			TerceroRCViajero tercero = new TerceroRCViajero();
			tercero = entidadService.findById(TerceroRCViajero.class,estimacion.getId());
			
			terceroDTO.setEsRCJ(true);
			terceroDTO.setNombreTercero(tercero.getNombreAfectado());
			
			PrestadorServicio medico = tercero.getMedico();
			PrestadorServicio hospitalPs = tercero.getHospital();
			PrestadorServicio ciaSeguros = tercero.getCompaniaSegurosTercero();

			if (medico != null) {
				terceroDTO.setNombreMedico(tercero.getMedico().getNombrePersona());
				terceroDTO.setClaveMedico(tercero.getMedico().getId().toString());
			} 
			if (hospitalPs != null) {
				terceroDTO.setNombreHospital(tercero.getHospital().getNombrePersona());
				terceroDTO.setClaveHospital(tercero.getHospital().getId().toString());
			} else if (tercero.getOtroHospital() != null) {
				terceroDTO.setNombreHospital(tercero.getOtroHospital());
			}

		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, estimacion.getTipoEstimacion())) {
			TerceroRCBienes tercero = new TerceroRCBienes();
			tercero = entidadService.findById(TerceroRCBienes.class, estimacion.getId());
			
			terceroDTO.setEsRCB(true);
			terceroDTO.setNombreTercero(tercero.getNombreAfectado());

			PrestadorServicio reponsableReparacion = tercero.getResponsableReparacion();
			PrestadorServicio ciaSeguros = tercero.getCompaniaSegurosTercero();
			
			CatValorFijo valor = catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.TIPOS_DE_BIEN, tercero.getTipoBien());
			if (valor != null) {
				terceroDTO.setTipoBien(valor.getDescripcion());
			}
			
			PaisMidas pais = tercero.getPais();
			EstadoMidas estado = tercero.getEstado();
			CiudadMidas municipio = tercero.getMunicipio();

			if (pais != null) {
				terceroDTO.setPais(pais.getDescripcion());
			}

			if (estado != null) {
				terceroDTO.setEstado(estado.getDescripcion());
			}
 
			if (municipio != null) {
				terceroDTO.setMunicipio(municipio.getDescripcion());
			}

			terceroDTO.setDomicilio(tercero.getUbicacion());			
			

		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, estimacion.getTipoEstimacion())) {
			TerceroRCPersonas tercero = new TerceroRCPersonas();
			tercero = entidadService.findById(TerceroRCPersonas.class, estimacion.getId());
			
			terceroDTO.setNombreTercero(tercero.getNombreAfectado());
			terceroDTO.setEsRCP(true);
			
			PrestadorServicio medico = tercero.getMedico();
			PrestadorServicio hospitalPs = tercero.getHospital();
			PrestadorServicio ciaSeguros = tercero.getCompaniaSegurosTercero();

			if (medico != null) {
				terceroDTO.setNombreMedico(tercero.getMedico().getNombrePersona());
				terceroDTO.setClaveMedico(tercero.getMedico().getId().toString());
			} 
			if (hospitalPs != null) {
				terceroDTO.setNombreHospital(tercero.getHospital().getNombrePersona());
				terceroDTO.setClaveHospital(tercero.getHospital().getId().toString());
			} else if (tercero.getOtroHospital() != null) {
				terceroDTO.setNombreHospital(tercero.getOtroHospital());
			}

		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, estimacion.getTipoEstimacion())) {
			TerceroRCVehiculo tercero = new TerceroRCVehiculo();
			tercero = entidadService.findById(TerceroRCVehiculo.class, estimacion.getId());
			
			terceroDTO.setEsRCV(true);	
			terceroDTO.setNombreTercero(tercero.getNombreAfectado());
			
			terceroDTO.setModelo(tercero.getModeloVehiculo().toString());
			String color = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.COLOR).get(tercero.getColor().toString());
			terceroDTO.setColor(color);
			terceroDTO.setEstilo(tercero.getEstiloVehiculo());
			terceroDTO.setMarca(tercero.getMarca());
			terceroDTO.setPlacas(tercero.getPlacas());
			terceroDTO.setNombreTercero(tercero.getNombreAfectado());
			terceroDTO.setNumeroSerie(tercero.getNumeroSerie());
			
		}

		return terceroDTO;
	}

	public SiniestroCabinaService getSiniestroCabinaService() {
		return siniestroCabinaService;
	}

	public void setSiniestroCabinaService(
			SiniestroCabinaService siniestroCabinaService) {
		this.siniestroCabinaService = siniestroCabinaService;
	}

	public PolizaSiniestroService getPolizaSiniestroService() {
		return polizaSiniestroService;
	}

	public void setPolizaSiniestroService(
			PolizaSiniestroService polizaSiniestroService) {
		this.polizaSiniestroService = polizaSiniestroService;
	}

	/**
	 * Obtiene Reportes de Siniestrosa de cobertura tipo Robo RT con
	 * estimacionCobertura
	 * 
	 * @param ReporteSiniestroRoboDTO
	 */
	@Override
	public List<ReporteSiniestroRoboDTO> buscarReportesRobo(
			ReporteSiniestroRoboDTO reporteSiniestroDTO) {
		return reporteCabinaDao.buscarReportesRobo(reporteSiniestroDTO);
	}

	public boolean validarGuardadoFechaOcurrido(ReporteCabina reporteCabina) {
		ReporteCabina reporteGuardado = null;
		Long idReporte = reporteCabina.getId();
		if (idReporte != null) {
			reporteGuardado = entidadService.findById(ReporteCabina.class,
					idReporte);
		}

		if (reporteGuardado != null) {
			if(reporteGuardado.getFechaOcurrido() != null && 
					!reporteGuardado.getFechaOcurrido().equals(reporteCabina.getFechaOcurrido())){
				return Boolean.FALSE;
				}
			if(!StringUtil.isEmpty(reporteGuardado.getHoraOcurrido()) && 
					!reporteGuardado.getHoraOcurrido().equals(reporteCabina.getHoraOcurrido())){
				return Boolean.FALSE;
			}
			}
		return Boolean.TRUE;		
	}

	private LugarSiniestroMidas copiarLugar(LugarSiniestroMidas lugarOrigen){
		LugarSiniestroMidas lugarDestino = null;
		if (lugarOrigen != null){
			lugarDestino = new LugarSiniestroMidas();
			lugarDestino.setTipo(lugarOrigen.getTipo().equals(TipoLugar.AT.toString())?TipoLugar.OC.toString():TipoLugar.AT.toString());
			lugarDestino.setCalleNumero(lugarOrigen.getCalleNumero());
			lugarDestino.setCiudad(lugarOrigen.getCiudad());
			lugarDestino.setCodigoPostalOtraColonia(lugarOrigen
					.getCodigoPostalOtraColonia());
			lugarDestino.setEstado(lugarOrigen.getEstado());
			lugarDestino.setKilometro(lugarOrigen.getKilometro());
			lugarDestino
					.setNombreCarretera(lugarOrigen.getNombreCarretera());
			lugarDestino.setOtraColonia(lugarOrigen.getOtraColonia());
			lugarDestino.setPais(lugarOrigen.getPais());
			lugarDestino.setReferencia(lugarOrigen.getReferencia());
			lugarDestino.setTipoCarretera(lugarOrigen.getTipoCarretera());
			lugarDestino.setZona(lugarOrigen.getZona());
			lugarDestino.setCoordenadas(lugarOrigen.getCoordenadas());
			if (lugarOrigen.getIdColoniaCheck()
					|| lugarOrigen.getZona().equals("CA")) {
				lugarDestino.setCodigoPostal("99999");
				lugarDestino.setColonia(entidadService.findById(ColoniaMidas.class,
						"99999-1"));
			} else {
				lugarDestino.setCodigoPostal(lugarOrigen.getCodigoPostal());
				lugarDestino.setColonia(lugarOrigen.getColonia());
			}
		}

		return lugarDestino;
	}
	
	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public EstimacionCoberturaSiniestroService getEstimacionCoberturaSiniestroService() {
		return estimacionCoberturaSiniestroService;
	}

	public void setEstimacionCoberturaSiniestroService(
			EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService) {
		this.estimacionCoberturaSiniestroService = estimacionCoberturaSiniestroService;
	}

	public ReporteCabinaService getReporteCabinaService() {
		return reporteCabinaService;
	}

	public void setReporteCabinaService(
			ReporteCabinaService reporteCabinaService) {
		this.reporteCabinaService = reporteCabinaService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Override
	public FechaHoraDTO generarFechaHora(Long idReporteCabina,
			TIPO_FECHA_HORA tipo, String isEditable, Date fechaEditable) {
		FechaHoraDTO fechaHoraDTO = new FechaHoraDTO();
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class, idReporteCabina);
		Date fecha = new Date();
		
		if(fechaEditable != null)
			fecha = fechaEditable;
		
		if(reporte != null){
			switch(tipo){
				case CONTACTO:
					if(reporte.getFechaHoraContacto() != null && isEditable.equals("NO")){
						fechaHoraDTO.setDescripcion("Ya existe una fecha y hora contacto generada para este reporte");
					}else if(reporte.getFechaHoraAsignacion() == null){
						fechaHoraDTO.setDescripcion("Se requiere previamente una fecha y hora de asignaci\u00F3n");					
					}else{
						reporte.setFechaHoraContacto(fecha);		
						if(isEditable.equals("NO"))
							notificacionesService.notificacionCabinaArriboAjustador(reporte);						
					} 
					break;
				case TERMINACION:
					if(reporte.getFechaHoraTerminacion() != null && isEditable.equals("NO")){
						fechaHoraDTO.setDescripcion("Ya existe una fecha y hora de terminaci\u00F3n para este reporte");						
					}else if(reporte.getFechaHoraContacto() == null){
						fechaHoraDTO.setDescripcion("Se requiere previamente una fecha y hora de contacto");
					}else{
						reporte.setFechaHoraTerminacion(fecha);							
					}
					break;
				default:
					fecha = null;
					break;
			}
			if(fecha != null && StringUtil.isEmpty(fechaHoraDTO.getDescripcion())){
				entidadService.save(reporte);
				DateFormat sdf = new SimpleDateFormat("HH:mm");
				fechaHoraDTO.setHora(sdf.format(fecha));
				sdf = new SimpleDateFormat("dd/MM/yyyy");
				fechaHoraDTO.setFecha(sdf.format(fecha));
			}
		}
		return fechaHoraDTO;
	}

	@Override
	public FechaHoraDTO validarExistenciaFechaHora(Long idReporteCabina,
			TIPO_FECHA_HORA tipo) {
		FechaHoraDTO fechaHoraDTO = new FechaHoraDTO();
		ReporteCabina reporteCabina = entidadService.findById(ReporteCabina.class, idReporteCabina);
		switch(tipo){
			case OCURRIDO:
				if(reporteCabina.getFechaOcurrido() != null){
					DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					fechaHoraDTO.setFecha(sdf.format(reporteCabina.getFechaOcurrido()));					
				}
				
				if(!StringUtil.isEmpty(reporteCabina.getHoraOcurrido())){					
					fechaHoraDTO.setHora(reporteCabina.getHoraOcurrido());
				}				
				break;		
			case ASIGNACION:
			case REPORTE:
			case TERMINACION:			
			default:
				break;
		}		
		return fechaHoraDTO;
	}
	

	/**
	 * Validar si el usuario actual tiene permitido editar un reporte convertido a siniestro debido a su rol (SPV)
	 * @param reporte
	 * @return
	 */
	@Override
	public Boolean noEsEditableParaElRol(ReporteCabina reporte){
		//SPV CABIN Validacion lectura una vez convertido a siniestro
		if(usuarioService.tienePermisoUsuarioActual(FN_NO_AFECTACION_RESERVA) && reporte != null && reporte.getSiniestroCabina() != null && 
				!StringUtil.isEmpty(reporte.getSiniestroCabina().getNumeroSiniestro()) && 
				!reporte.getSiniestroCabina().getNumeroSiniestro().equals("--")){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	@Override
	public CreacionReporteDTO generarReporte(Long continuityId, String codigoOficina, String personaReporte, Date fechaOcurrido, String horaOcurrido,
			String lada, String telefono, String personaConductor, String usuario ) {
		
		CreacionReporteDTO reporteDto = new CreacionReporteDTO();
		
		//GENERAR SESIO USUARIO
		Usuario usuarioWs = this.generarUsuarioWS(usuario);
		usuario = usuarioWs.getNombreUsuario();
		
		ReporteCabina reporte = new ReporteCabina();
	
		reporte.setFechaOcurrido(fechaOcurrido);
		reporte.setHoraOcurrido(horaOcurrido);
		
		if (continuityId != null) {
			BitemporalInciso biInciso = incisoViewService.getInciso(continuityId, reporte.getFechaHoraOcurrido(), reporte.getFechaHoraOcurrido() );
			
			if (biInciso == null) {
				reporteDto.setEstatus((short)1);
				reporteDto.setDescripcionError("El inciso no es vigente");
				return reporteDto;
			}
		}
		
		//Oficina oficina = entidadService.findById(Oficina.class, oficinaId);
		Oficina oficina = null;
		List<Oficina> lOficina = entidadService.findByProperty(Oficina.class, "claveOficina", codigoOficina);
		
		if (  lOficina.isEmpty() ) {
			reporteDto.setEstatus((short)1);
			reporteDto.setDescripcionError("Oficina no existe");
			return reporteDto;
		}else{
			oficina = lOficina.get(0);
		}
		
		DateTime fechaHoraOcurridoDT = new DateTime(fechaOcurrido);
		
		reporte.setOficina(oficina);		
		reporte.setClaveOficina(oficina.getClaveOficina());
		
		reporte.setConsecutivoReporte(this.obtenerConsecutivoReporte(reporte));
		
		String aniostr = String.valueOf(fechaHoraOcurridoDT.getYear());
		String anio = aniostr.substring(aniostr.length() - LONG_CORTA_ANIO, aniostr.length());
		reporte.setAnioReporte(anio);		
		
		reporte.setLadaTelefono(lada);
		reporte.setTelefono(telefono);
		reporte.setPersonaConductor(personaConductor);
		reporte.setPersonaReporta(personaReporte);
		
		reporte.setFechaHoraReporte(new Date());		
		reporte.setFechaCreacion(new Date());
		reporte.setCodigoUsuarioCreacion(usuario);
		reporte.setEstatus(EstatusReporteCabina.TRAMITE_REPORTE.getValue());
		
		reporte = entidadService.save(reporte);
		
		if (continuityId != null) {
			String mensaje = polizaSiniestroService.asignarIncisoReporte(reporte.getId(), continuityId, reporte.getFechaHoraOcurrido(), usuario);
			
			if (!StringUtils.isEmpty(mensaje)) {
				reporteDto.setEstatus((short)1);
				reporteDto.setDescripcionError("Error al crear el registro");
				return reporteDto;
			}			
		}
		
		reporteDto.setEstatus((short)0);
		reporteDto.setReporteId(reporte.getId());
		reporteDto.setNoReporteSiniestro(reporte.getNumeroReporte());
		
		
		return reporteDto;
		
	}
	
	@Override
	public void depurarReservaReporte(Long reporteId) {
		
		BigDecimal reservaReporte = movimientoSiniestroService.obtenerReservaReporte(reporteId, Boolean.TRUE);
		
		if (reservaReporte.compareTo(BigDecimal.ZERO) != 0) {
			List<EstimacionCoberturaReporteCabina> estimaciones = entidadService.findByProperty(
					EstimacionCoberturaReporteCabina.class, "coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id", reporteId);
			
			for (EstimacionCoberturaReporteCabina estimacion: CollectionUtils.emptyIfNull(estimaciones)) {
				BigDecimal reservaPendiente = movimientoSiniestroService.obtenerReservaAfectacion(estimacion.getId(), Boolean.TRUE);
				
				if (reservaPendiente.compareTo(BigDecimal.ZERO) != 0) {
					movimientoSiniestroService.generarAjusteReserva(estimacion.getId(), BigDecimal.ZERO, 
							CausaMovimiento.DEPURACION_MANUAL_RESERVA, usuarioService.getUsuarioActual().getNombreUsuario());
					reporteCabinaService.guardarEvento(reporteId, TipoEvento.EVENTO_51, 
							"Por cambio de estatus del reporte se realiza depuracion de reserva del pase con folio " + estimacion.getFolio() + " para la cobertura de " 
							+ estimacionCoberturaSiniestroService.obtenerNombreCobertura(estimacion.getCoberturaReporteCabina().getCoberturaDTO(), estimacion.getCoberturaReporteCabina().getClaveTipoCalculo(), estimacion.getCveSubCalculo(), estimacion.getTipoEstimacion(), estimacion.getCoberturaReporteCabina().getCoberturaDTO().getTipoConfiguracion()));
				}				
			}			
		}			
	}

	@Override
	public String sincronizarReporteWS( ComplementoReporteDTO complementoReporteDto) {
		
		String mensajeWS = null, usuario = "", mensaje = "";
		
		if( complementoReporteDto != null ){
			
			// BUSCAR USUARIO EN ASM
			Usuario usuarioWs = this.generarUsuarioWS(complementoReporteDto.getUsuario());
			usuario = usuarioWs.getNombreUsuario();
			
			ReporteCabina reporte = null;
			
			// VALIDAR QUE EL REPORTE EXISTA
			if( complementoReporteDto.getIdReporte() != null ){
				
				reporte = this.entidadService.findById(ReporteCabina.class, complementoReporteDto.getIdReporte() );
				
				if( reporte != null && reporte.getClaveOficina().equals(parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS, ParametroGlobalService.SPV_CODIGO_OFICINA  )) ) {
					
					// VALIDAR QUE LA FECHA DE ASIGNACION NO SEA MENOR A LA FECHA DE REPORTE
					if ( !this.validarFechaReporteAsignacionSpv( reporte.getFechaHoraOcurrido(),complementoReporteDto.getFechaHoraAsignacion() ) ) {
						
						if( !polizaSiniestroService.validacionTieneCoberturasAfectadas(reporte.getId())){
							
							try {
								
								ServicioSiniestro ajustador = this.entidadService.findById(ServicioSiniestro.class, complementoReporteDto.getAjustadorId());
								if( ajustador == null ){
									return mensajeWS = " 06 Error: el ajustador no existe ";
								}else{
	
									if ( complementoReporteDto.getIncisoContinuityId() != null && complementoReporteDto.getIncisoContinuityId() != 0 ) {
										
										BitemporalInciso biInciso = incisoViewService.getInciso(complementoReporteDto.getIncisoContinuityId(), reporte.getFechaHoraOcurrido(), reporte.getFechaHoraOcurrido() );
										
										if (biInciso == null) {
											mensajeWS = "La póliza no se puede asignar al reporte, validar la vigencia de la misma ";
										}else{
											
											if( reporte.getPoliza() != null ){
												
												// VALIDAR NO SEAN INCISOS DIFERENTES
												BigDecimal idtoPoliza = reporte.getPoliza().getIdToPoliza();
												Integer cotizacion =  biInciso.getContinuity().getCotizacionContinuity().getNumero();
												
												List<PolizaDTO> listPoliza = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", new BigDecimal(cotizacion));
												
												if( listPoliza != null && listPoliza.size() >0){
													PolizaDTO poliza = listPoliza.get(0);
													if( poliza.getIdToPoliza().compareTo(idtoPoliza) != 0 ){
														mensajeWS = " 07 Error: el inciso de la póliza enviada no coincide con la asignada previamente ";
													}
												}
												
											}else{
												
												// ASIGNAR INCISO AL REPORTE
												mensaje = polizaSiniestroService.asignarIncisoReporte(reporte.getId(), complementoReporteDto.getIncisoContinuityId(), reporte.getFechaHoraOcurrido(), usuario);
												
												if (!StringUtils.isEmpty(mensaje)) {
													mensajeWS = mensaje;
												}
												
											}
											
										}
										
									}// cierra if ( complementoReporteDto.getIncisoContinuityId()
									
								}
									
								// ASIGNAR AJUSTADOR
								reporte.setAjustador(ajustador);
								reporte.setFechaHoraAsignacion(complementoReporteDto.getFechaHoraAsignacion());
									
								// TODO LUGAR DE SINIESTROS
								this.entidadService.save(reporte);
								
								if( !StringUtil.isEmpty(mensajeWS) ) {
									mensajeWS += " | Datos de ajustador y fecha sincronizados con éxito";
								}else {
									mensajeWS = " Reporte sincronizado con éxito";
								}
								
							} catch (Exception e) {
								log.error("sincronizarReporteWS - Error al asignar poliza y salvar ajustador", e);
								mensajeWS = "05 Error: no se puede asignar el inciso a la póliza";
							}
						}else{
							mensajeWS = "04 Error: La Póliza actual del Siniestro cuenta con coberturas afectadas";
						}
						
					}else {
						mensajeWS = "08 Error: la fecha de asignación de ajustador es menor a la fecha del reporte";
					}
					
				}else{
					mensajeWS = "03 Error: el id de reporte enviado NO existe o no pertenece a la oficina de SPV";
				}
				
			}else{
				mensajeWS = "02 Error: el ID de reporte cabina no puede ser nulo";
			}
			
		}else{
			mensajeWS = "01 Error: el objeto no contiene datos";
		}
		
		
		return mensajeWS;
	}
	
	
	private Usuario generarUsuarioWS(String usuario){
		//BUSCAR USUARIO
		
		Usuario usuarioWs = this.usuarioService.buscarUsuarioPorNombreUsuario(usuario.trim());
		if ( usuarioWs == null ){
			//usuarioWs = new Usuario(); // DESCOMENTAR EN DESARROLLO
			//usuarioWs.setNombreUsuario("SYSTEM"); // DESCOMENTAR EN DESARROLLO
		}else{
			usuarioWs.getNombreUsuario();
		}
		
		return usuarioWs;
	}

	@Override
	@Deprecated
	public void registrarContacto(Long reporteId, Date fecha, Long ajustadorId) {
		this.registrarContacto(reporteId, fecha, ajustadorId, null);
	}

	@Override
	@Deprecated
	public void registrarContacto(Long reporteId, Date fecha, Long ajustadorId,
			Coordenadas coordenadas) {
		ServicioSiniestro ajustador = entidadService.findById(ServicioSiniestro.class, ajustadorId);
		if(ajustador != null){
			StringBuilder str = new StringBuilder("Registro de contacto por parte de ajustador ");
			str.append(ajustadorId).append(" - ").append(ajustador.getPersonaMidas().getNombre()).
				append(". Fecha de contacto: ").append(DateUtils.toString(fecha, "dd/MM/yyyy HH:mm:ss")).append(". ");
			if(coordenadas != null){
				str.append(" Ubicacion: Lat ").append(coordenadas.getLatitud()).append(", Long ").append(coordenadas.getLongitud()).append(".");
				//registrar coordenadas en lugar de atencion
				ReporteCabina reporte = entidadService.findById(ReporteCabina.class, reporteId);
				LugarSiniestroMidas atencion = reporte.getLugarAtencion();
				if(atencion != null){
					atencion.setCoordenadasContacto(coordenadas);
					atencion.setFechaCoordenadasContacto(new Date());
					entidadService.save(reporte);
				}
			}			
			guardarEvento(reporteId, TipoEvento.EVENTO_52, str.toString());
		}			
	}
	
	/**
	 * Registrar en bitacora de reporte el contacto con quien reporta el siniestro por parte del ajustador
	 * @param reporteId id del reporte
	 * @param fecha fecha hora de contacto
	 * @param ajustadorId id del ajustador
	 * @param tipo TIPO_FECHA_HORA que esta reportando el ajustador - ENTERADO, CONTACTO
	 */
	public void registrarEventoAjustador(Long reporteId, Date fecha, Long ajustadorId, TIPO_FECHA_HORA tipo){
		this.registrarEventoAjustador(reporteId, fecha, ajustadorId, tipo, null);
	}
	
	
	/**
	 * Registrar en bitacora de reporte el contacto con quien reporta el siniestro por parte del ajustador
	 * @param reporteId id del reporte
	 * @param fecha fecha hora de contacto
	 * @param ajustadorId id del ajustador
	 * @param tipo TIPO_FECHA_HORA que esta reportando el ajustador - ENTERADO, CONTACTO
	 */
	public void registrarEventoAjustador(Long reporteId, Date fecha, Long ajustadorId, TIPO_FECHA_HORA tipo, Coordenadas coordenadas){
		ServicioSiniestro ajustador = entidadService.findById(ServicioSiniestro.class, ajustadorId);
		if(ajustador != null){
			StringBuilder str = new StringBuilder("Registro de ");
			str.append(tipo.toString());
			str.append("  de ajustador ").append(ajustadorId).append(" - ").append(ajustador.getPersonaMidas().getNombre()).
			append(". Fecha: ").append(DateUtils.toString(fecha, "dd/MM/yyyy HH:mm:ss")).append(". ");
			switch(tipo){			
				case CONTACTO:									
					if(coordenadas != null){
						str.append(" Ubicacion: Lat ").append(coordenadas.getLatitud()).append(", Long ")
							.append(coordenadas.getLongitud()).append(".");
						//registrar coordenadas en lugar de atencion
						ReporteCabina reporte = entidadService.findById(ReporteCabina.class, reporteId);
						LugarSiniestroMidas atencion = reporte.getLugarAtencion();
						if(atencion != null){
							atencion.setCoordenadasContacto(coordenadas);
							atencion.setFechaCoordenadasContacto(new Date());
							entidadService.save(reporte);
						}
					}			
					generarFechaHora(reporteId, tipo, "NO",null);
					break;			
				default:
					break;
			}
			guardarEvento(reporteId, TipoEvento.EVENTO_52, str.toString());
		}		
	}
	
	/***
	 * Valida que la fecha de asignacion no sea mayor a la fecha de ocurrido el reporte.
	 * @param fechaReporte
	 * @param fechaAsignaAjustador
	 * @return
	 */
	private boolean validarFechaReporteAsignacionSpv(Date fechaReporteOcurrido, Date fechaAsignaAjustador) {
		
		boolean isAjustadorFechaMayor = false;
		
		if( fechaReporteOcurrido != null && fechaAsignaAjustador != null ) {
			if( fechaAsignaAjustador.compareTo(fechaReporteOcurrido) > 0 ) {
				isAjustadorFechaMayor = true;
			}
		}
		
		return isAjustadorFechaMayor;
	}
	
	
	/**
	 * Regresa una lista de reportecabina que está en atención por un ajustador pero que aun
	 * no tiene hora de contacto, es decir el ajustador aun no llega al lugar del siniestro
	 * @param idAjustador
	 * @return
	 */
	public List<ReporteCabina> buscarReportesCabinaSinContactoPorAjustador(Long idAjustador) {
		final Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("ajustador.id", idAjustador);
		filtro.put("fechaHoraContacto", null);
		return entidadService.findByProperties(
				ReporteCabina.class, filtro);		

	}
	
	@Override
	public String buscarNombreAseguradoAutos(BigDecimal idToPoliza, Integer numeroInciso, Date fechaValidez){
		return reporteCabinaDao.buscarNombreAseguradoAutos(idToPoliza, numeroInciso, fechaValidez);
	}

}
