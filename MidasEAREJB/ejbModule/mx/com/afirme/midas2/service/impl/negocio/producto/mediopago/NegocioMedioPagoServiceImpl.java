package mx.com.afirme.midas2.service.impl.negocio.producto.mediopago;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.mediopago.NegocioMedioPago;
import mx.com.afirme.midas2.dto.negocio.producto.mediopago.RelacionesNegocioMedioPagoDTO;
import mx.com.afirme.midas2.service.negocio.producto.mediopago.NegocioMedioPagoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

@Stateless
public class NegocioMedioPagoServiceImpl implements NegocioMedioPagoService {

	@Override
	public RelacionesNegocioMedioPagoDTO getRelationLists(NegocioProducto negocioProducto) {
		RelacionesNegocioMedioPagoDTO relacionesNegocioMedioPagoDTO = new RelacionesNegocioMedioPagoDTO();
		//Listado asociadas
		List<NegocioMedioPago> negocioMedioPagoAsociadasList = entidadDao.findByProperty(NegocioMedioPago.class, "negocioProducto.idToNegProducto", negocioProducto.getIdToNegProducto());
		//Listado posibles
		List<MedioPagoDTO> posibles =  negocioProducto.getProductoDTO().getMediosPago();
		//Listado disponibles
		List<NegocioMedioPago> negocioMedioPagoDisponiblesList = new ArrayList<NegocioMedioPago>();
		
		List<MedioPagoDTO> medioPagoDTOAsociadasList = new ArrayList<MedioPagoDTO>();
		
		for(NegocioMedioPago item : negocioMedioPagoAsociadasList){
			medioPagoDTOAsociadasList.add(item.getMedioPagoDTO());
		}
		
		for(MedioPagoDTO item : posibles){
			if(!medioPagoDTOAsociadasList.contains(item)){
				NegocioMedioPago negocioMedioPago = new NegocioMedioPago();
				negocioMedioPago.setMedioPagoDTO(item);
				negocioMedioPago.setNegocioProducto(negocioProducto);
				negocioMedioPagoDisponiblesList.add(negocioMedioPago);
			}
		}
		
		relacionesNegocioMedioPagoDTO.setAsociadas(negocioMedioPagoAsociadasList);
		relacionesNegocioMedioPagoDTO.setDisponibles(negocioMedioPagoDisponiblesList);
		
		return relacionesNegocioMedioPagoDTO;
	}

	@Override
	public void relacionarNegocioMedioPago(String accion, NegocioMedioPago negocioMedioPago) {
		negocioMedioPago.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		entidadDao.executeActionGrid(accion, negocioMedioPago);
	}
	
	private EntidadDao entidadDao;
	protected UsuarioService usuarioService;
	
	@EJB(beanName="UsuarioServiceDelegate")
    public void setUsuarioService(UsuarioService usuarioService) {
          this.usuarioService = usuarioService;
    }	
	
	@EJB
	public void setEntidadDao(EntidadDao catalogoDato) {
		this.entidadDao = catalogoDato;
	}

}
