package mx.com.afirme.midas2.service.impl.tarea;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.tarea.EmisionPendienteService;
import mx.com.afirme.midas2.service.tarea.TareaEmiteEndososCancelacionPolizaAutoService;

import org.apache.log4j.Logger;

@Stateless
public class TareaEmiteEndososCancelacionPolizaAutoServiceImpl implements TareaEmiteEndososCancelacionPolizaAutoService {
	
	public static final Logger LOG = Logger.getLogger(TareaEmiteEndososCancelacionPolizaAutoServiceImpl.class);
	
	@Resource	
	private TimerService timerService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private EmisionPendienteService emisionPendienteService;
	
	public void initialize() {
		String timerInfo = "TimerEmiteEndososCancelacionPolizaAuto";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0 2 * * ?
				expression.minute(0);
				expression.hour(2);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerEmiteEndososCancelacionPolizaAuto", false));
				
				LOG.info("Tarea TimerEmiteEndososCancelacionPolizaAuto configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerEmiteEndososCancelacionPolizaAuto");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerEmiteEndososCancelacionPolizaAuto:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		emisionPendienteService.procesaCancelacionPolizaAutos(1);
	}

}
