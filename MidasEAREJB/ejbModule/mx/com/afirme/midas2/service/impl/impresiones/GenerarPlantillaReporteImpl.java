package mx.com.afirme.midas2.service.impl.impresiones;
import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.afirme.insurance.services.PrintReport.PrintReport;
import com.afirme.insurance.services.PrintReport.PrintReportServiceLocator;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.cobranza.RecibosView;
import mx.com.afirme.midas2.domain.compensaciones.OrdenPagosDetalle;
import mx.com.afirme.midas2.domain.compensaciones.OrdenesPagoDTO;
import mx.com.afirme.midas2.domain.compensaciones.ReporteAutorizarOrdenPago;
import mx.com.afirme.midas2.domain.compensaciones.ReporteDetallePrimas;
import mx.com.afirme.midas2.domain.compensaciones.ReporteFiltrosDTO;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;
import mx.com.afirme.midas2.domain.negocio.bonocomision.NegocioBonoComision;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacion;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.ReporteEfectividadEntrega;
import mx.com.afirme.midas2.domain.provisiones.ProvisionImportesRamo;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionNegocioDescripcionDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.ReporteDetalleProvisionView;
import mx.com.afirme.midas2.dto.fuerzaventa.ReporteProvisionView;
import mx.com.afirme.midas2.dto.impresiones.ContenedorDatosImpresion;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaCotizacion;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaInciso;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaPoliza;
import mx.com.afirme.midas2.dto.impresiones.DatosCoberturasDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosCotizacionDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosDesglosePagosDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosIncisoDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosLineasDeNegocioDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosNegocioDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosPolizaDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosProductosDeNegocioDetalleDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosRecibosProveedores;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotMovilVidaDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteBonosGerenciaView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteContabilidadMMView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteDatosView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteEstatusEntregaFactura;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteIngresosView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgentePrimaEmitidaVsPriamaPagadaDTO;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgentePrimaPagadaPorRamoView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteTopAgenteView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgentesDetalleBonoView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgentesReporteProduccion;
import mx.com.afirme.midas2.dto.reportesAgente.DatosDetPrimas;
import mx.com.afirme.midas2.dto.reportesAgente.DatosEdoctaAgenteAcumuladoView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosEdoctaAgenteProduccion;
import mx.com.afirme.midas2.dto.reportesAgente.DatosEdoctaAgenteSiniestralidad;
import mx.com.afirme.midas2.dto.reportesAgente.DatosHonorariosAgentes;
import mx.com.afirme.midas2.dto.reportesAgente.DatosReporteCalculoBonoMensualDTO;
import mx.com.afirme.midas2.dto.reportesAgente.DatosReportePreviewBonos;
import mx.com.afirme.midas2.dto.reportesAgente.DatosVentasProduccion;
import mx.com.afirme.midas2.dto.reportesAgente.DatosVentasProduccionAux;
import mx.com.afirme.midas2.dto.reportesAgente.DetPrimasResumen;
import mx.com.afirme.midas2.dto.reportesAgente.ReporteGlobalBonoYComisionesDTO;
import mx.com.afirme.midas2.dto.reportesAgente.RetencionImpuestosReporteView;
import mx.com.afirme.midas2.dto.reportesAgente.SaldosVsContabilidadView;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasistemasEnvio;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraEmision;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraSiniestros;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.compensaciones.LiquidacionCompensacionesService;
import mx.com.afirme.midas2.service.compensaciones.OrdenPagosService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionNegocioAutosService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.reportes.MidasBaseReporteService;
import mx.com.afirme.midas2.service.reportes.MidasBaseReporte.MidasBaseReporteExcel;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.MidasException;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.utils.MailServiceSupport;
import mx.com.afirme.midas2.wsClient.sapamis.alerta.AlertaPortProxy;
import mx.com.afirme.midas2.wsClient.sapamis.alerta.RespuestaAlerta;
import mx.com.afirme.midas2.wsClient.sapamis.alerta.RespuestaDetalleAlerta;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;
import net.sf.jasperreports.engine.fill.JRGzipVirtualizer;

@Stateless
public class GenerarPlantillaReporteImpl implements GenerarPlantillaReporte{
	
	public static final Logger LOG = Logger.getLogger(GenerarPlantillaReporteImpl.class);
	public static final String KEY_LOGO_IMG = "P_IMAGE_LOGO";
	public static final String KEY_SIGNATURES = "P_SIGNATURES";
	public static final String SALDO_INICIAL = "SALDO INICIAL";
	public static final String AGAUT = "AGAUT";
	public static final String IMGHEADER = "imgHeader";
	public static final String UR_JRXML = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/";
	public static final String UR_JRXML_SO = "/mx/com/afirme/midas2/service/impl/movil/cotizador/enlaceafirme/jrxml";
	public static final int MAX_GZIPVIRTUALIZER = 10;
	private ListadoService listadoService;
	private Long idToNegocio;
	private AgenteMidasService agenteMidasService;
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	public static final String ESTATUS_REPORTE_COMPLETADO = "0";
	public static final String ESTATUS_REPORTE_EN_PROCESO = "1";
	
	private static final String TRUE = "true";
	
	private static final String REPORT_NAME_DETALLEPRIMAS = "reporte_Detalle_Primas.";
	private static final String REPORT_NAME_DETALLEPRIMAS_RETENCIONIMPUESTOS = "Reporte Impuestos Estatales.";
	
	private static final String CONTENT_TYPE_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	
	@EJB
	protected ParametroGeneralService parametroGeneralService;
	@EJB
	protected LiquidacionCompensacionesService liquiuLiquidacionCompensacionesService;
	@EJB
	protected GenerarPlantillaReporte generarPlantillaReporteService;
	
	public byte[] generarReporte(JasperReport configuracionReporte, 
			Map<String, Object> parametros, JRBeanCollectionDataSource colleccionObjeto){
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try{
			JasperPrint jasperPrint = JasperFillManager.fillReport(configuracionReporte, parametros, colleccionObjeto);
			JasperExportManager.exportReportToPdfStream(jasperPrint, out);
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}
		return out.toByteArray();		
	}
	
	public byte [] generarReportehtml(JasperReport jasperReport, Map<String, Object> parametros, JRBeanCollectionDataSource colleccionObjeto){		
		ByteArrayOutputStream out = new ByteArrayOutputStream();		
		try{
			
			JasperPrint print = JasperFillManager.fillReport(jasperReport, parametros, colleccionObjeto);
			JRHtmlExporter htmlExporter = new JRHtmlExporter();
            htmlExporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            htmlExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);            
            htmlExporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, obtenerValor());            
            htmlExporter.setParameter(JRHtmlExporterParameter.IS_OUTPUT_IMAGES_TO_DIR, Boolean.FALSE);
            //htmlExporter.setParameter(JRHtmlExporterParameter.ZOOM_RATIO, ZOOM_2X);
            htmlExporter.exportReport();
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}
		return out.toByteArray();
	}
	
	public String obtenerValor () {
        ParametroGeneralId id = new ParametroGeneralId();
        id.setIdToGrupoParametroGeneral(new BigDecimal(13));
        id.setCodigoParametroGeneral(new BigDecimal(131171));
        ParametroGeneralDTO parametro = parametroGeneralService.findById(id);
        LOG.info("parametro.getValor() := " + parametro.getValor());
        return parametro.getValor();
    }

	public void  pdfConcantenate(List<byte[]> inputByteArray, OutputStream outputStream){  
	      try {
	          int pageOffset = 0;    
	          int f = 0;     
	          ArrayList master = new ArrayList(1);
	          Document document = null;
	          PdfCopy  writer = null;	          
	          // we create a reader for a certain document	          
	          Iterator<byte[]> iterator = inputByteArray.iterator();	          
	          while(iterator.hasNext()){   
	            byte[] data = (byte[])iterator.next();
	            if(data == null || data.length == 0){
	              f++;
	              continue;
	            }
	            
	            PdfReader reader = new PdfReader(data);
	            reader.consolidateNamedDestinations();
	            // we retrieve the total number of pages
	            int n = reader.getNumberOfPages();
	            List bookmarks = SimpleBookmark.getBookmark(reader);
	            if (bookmarks != null) {
	              if (pageOffset != 0)
	                SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
	                master.addAll(bookmarks);
	            }	            
	            pageOffset += n;	                
	            if (f == 0) {
	              // step 1: creation of a document-object
	              document = new Document(reader.getPageSizeWithRotation(1));
	              // step 2: we create a writer that listens to the document                          
	              writer = new PdfCopy(document, outputStream);
	              // step 3: we open the document
	              document.open();           
	            }	              
	            // step 4: we add content
	            PdfImportedPage page;
	            for (int i = 0; i < n; ) {
	              ++i;
	              page = writer.getImportedPage(reader, i);
	              writer.addPage(page);
	            }	  
	            PRAcroForm form = reader.getAcroForm();
	            if (form != null){
	              writer.copyAcroForm(reader);
	            }
	            f++;
	          }
	          if (!master.isEmpty()){
	            writer.setOutlines(master);
	          }
	          // step 5: we close the document
	          if(document != null){
	            document.close();
	          }
	      }
	      catch(Exception e) {	      
	    	  LOG.error(e.getMessage(), e); 
	      }	      
	  }
	
	@Override
	public TransporteImpresionDTO imprimirListadoExcepciones(String cveNegocio, Locale locale){
		TransporteImpresionDTO transporte = new TransporteImpresionDTO();		
		byte[] reporte = null;
		Map<String, Object> parametros = null;
		List<? extends ExcepcionSuscripcionNegocioDescripcionDTO> lista = null;
		JasperReport reporteExcepciones = null;
		JRBeanCollectionDataSource excepciones = null;
		try{			
			lista = excepcionAutoService.listarExcepciones(cveNegocio);
			if(cveNegocio.equals(Negocio.CLAVE_NEGOCIO_AUTOS)){
				reporteExcepciones = getJasperReport(
						UR_JRXML+"listadoExcepcionesAutos.jrxml");	
				excepciones = new JRBeanCollectionDataSource(lista);
			}else{
				throw new RuntimeException("Se necesita especificar el tipo de excepciones a imprimir (autos, da�os, etc)");
			}
			parametros = new HashMap<String, Object>(1);
			if(locale != null){
				parametros.put(JRParameter.REPORT_LOCALE, locale);
			}
		 	parametros.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_SEGUROS_AFIRME);		 	
		 	reporte = generarReporte(reporteExcepciones, parametros, excepciones);
		 	transporte.setByteArray(reporte);
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}
		return transporte;		
	}


	@Override	
	public TransporteImpresionDTO imprimirCotizacion(
			BigDecimal idToCotizacion, Locale locale, boolean isOutsider) {
		return this.imprimirCotizacion(idToCotizacion, locale, true, true,isOutsider);
	}
	
	@Override	
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToCotizacion, BigDecimal idToPoliza, 
			Locale locale) {
		return this.imprimirPoliza(idToCotizacion, idToPoliza, locale, true, true, true, true, true);
	}
	
	public byte[] imprimirCotizacion(
			DatosCaratulaCotizacion datosCaratulaCotizacion, Locale locale, Long idToLineaNegocio) {
		List<DatosCotizacionDTO> datasourceCotizacion = new ArrayList<DatosCotizacionDTO>(1);
		List<DatosLineasDeNegocioDTO> datasourceLineasNegocio = datosCaratulaCotizacion
				.getDatasourceLineasNegocio();
		List<DatosDesglosePagosDTO> datasourceDesglosePagos = datosCaratulaCotizacion
				.getDatasourceDesglosePagos();
		byte[] pdfCotizacion = null;

		try {
			JasperReport reporteCotizacion = getJasperReport(
					UR_JRXML+"CaratulaCotizacionM2.jrxml");
			
			JasperReport reporteLineas = getJasperReport(
					UR_JRXML+"DesgloseLineasM2.jrxml");
			
			JasperReport reporteEsquemaPago = getJasperReport(
					UR_JRXML+"DesglosePagosCotizacionM2.jrxml");

			Map<String, Object> parameters = new HashMap<String, Object>(1);
			if (locale != null)
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			parameters.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_SEGUROS_AFIRME);
			parameters.put("P_SIGNATURE",
					SistemaPersistencia.FIRMA_FUNCIONARIO);
			parameters.put("lineasDataSource", datasourceLineasNegocio);
			parameters.put("lineasReport", reporteLineas);
			if(idToLineaNegocio != /*01830*/1682) {
				parameters.put("dataSourceEsquemaPago", datasourceDesglosePagos);
				parameters.put("reporteEsquemaPago", reporteEsquemaPago);
			}
			parameters.put(JRParameter.REPORT_VIRTUALIZER, new JRGzipVirtualizer(2));
			datasourceCotizacion.add(datosCaratulaCotizacion
					.getDatosCotizacionDTO());
			JRBeanCollectionDataSource dsCotizacion = new JRBeanCollectionDataSource(
					datasourceCotizacion);

			pdfCotizacion = JasperRunManager.runReportToPdf(reporteCotizacion,
					parameters, dsCotizacion);
		} catch (JRException ex) {
			throw new RuntimeException(ex);
		}
		return pdfCotizacion;
	}		

	public byte[] imprimirInciso(DatosCaratulaInciso inciso,
			Locale locale, CotizacionDTO cotizacion) {
		byte[] pdfInciso = null;
		SolicitudDTO solicitud = cotizacion.getSolicitudDTO()!=null?cotizacion.getSolicitudDTO():new SolicitudDTO();
		Negocio negocio = solicitud.getNegocio()!=null?solicitud.getNegocio():new Negocio();
		String folio = negocio.getAplicaEnlace()?cotizacion.getFolio():new String();
		boolean imprimirUMA = listadoService.imprimirLeyendaUMA(inciso.getDataSourceIncisos().getFechaInicioVigencia());

		try {
			JasperReport reporteInciso = getJasperReport(
					UR_JRXML+"CaratulaIncisoM2.jrxml");
			JasperReport reporteCoberturas = getJasperReport(
					UR_JRXML+"DesgloseCoberturasM2.jrxml");
			Map<String, Object> parameters = new HashMap<String, Object>(1);
			List<DatosIncisoDTO> reporteDatasourceInciso = new ArrayList<DatosIncisoDTO>(1);			
			reporteDatasourceInciso.add(inciso.getDataSourceIncisos());
			if (locale != null)
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			parameters.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_SEGUROS_AFIRME);
			parameters.put(KEY_SIGNATURES, SistemaPersistencia.FIRMA_FUNCIONARIO);
			parameters.put("coberturasDataSource",
					inciso.getDataSourceCoberturas());
			parameters.put("coberturasReport", reporteCoberturas);
			parameters.put("folioCalca", folio);
			parameters.put("imprimirUMA", imprimirUMA);
			
			if (idToNegocio!=null){
				parameters.put("numeroTelefonoSPV",listadoService.getSpvData(idToNegocio).get("numeroTelefonoSPV"));
				parameters.put("spvNumberDF",listadoService.getSpvData(idToNegocio).get("spvNumberDF"));
				parameters.put("esServicioPublico",listadoService.getSpvData(idToNegocio).get("esServicioPublico"));
			}
			
			parameters.put(JRParameter.REPORT_VIRTUALIZER, new JRGzipVirtualizer(2));			
			JRBeanCollectionDataSource dsInciso = new JRBeanCollectionDataSource(
					reporteDatasourceInciso);
			pdfInciso = JasperRunManager.runReportToPdf(reporteInciso,
					parameters, dsInciso);
		} catch (JRException ex) {
			throw new RuntimeException(ex);
		}
		return pdfInciso;
	}
	
	public byte[] imprimirInciso(List<DatosCaratulaInciso> incisosList,
			Locale locale) {
		byte[] pdfInciso = null;

		try {
			JasperReport reporteInciso = getJasperReport(
					UR_JRXML+"CaratulaIncisoM2.jrxml");
			JasperReport reporteCoberturas = getJasperReport(
					UR_JRXML+"DesgloseCoberturasM2.jrxml");
			Map<String, Object> parameters = new HashMap<String, Object>(1);
			List<DatosIncisoDTO> reporteDatasourceInciso = new ArrayList<DatosIncisoDTO>(1);
			for (DatosCaratulaInciso dataSourceInciso : incisosList) {
				dataSourceInciso.getDataSourceIncisos().setCoberturas(
						dataSourceInciso.getDataSourceCoberturas());
				reporteDatasourceInciso.add(dataSourceInciso
						.getDataSourceIncisos());
				if (locale != null)
					parameters.put(JRParameter.REPORT_LOCALE, locale);
				parameters.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_SEGUROS_AFIRME);
				parameters.put(KEY_SIGNATURES, SistemaPersistencia.FIRMA_FUNCIONARIO);
				parameters.put("coberturasDataSource",
						dataSourceInciso.getDataSourceCoberturas());
				parameters.put("coberturasReport", reporteCoberturas);
				
				if (idToNegocio!=null){
					parameters.put("numeroTelefonoSPV",listadoService.getSpvData(idToNegocio).get("numeroTelefonoSPV"));
					parameters.put("spvNumberDF",listadoService.getSpvData(idToNegocio).get("spvNumberDF"));
					parameters.put("esServicioPublico",listadoService.getSpvData(idToNegocio).get("esServicioPublico"));
				}
				
				parameters.put(JRParameter.REPORT_VIRTUALIZER, new JRGzipVirtualizer(2));
			}
			JRBeanCollectionDataSource dsInciso = new JRBeanCollectionDataSource(
					reporteDatasourceInciso);

			pdfInciso = JasperRunManager.runReportToPdf(reporteInciso,
					parameters, dsInciso);
		} catch (JRException ex) {
			throw new RuntimeException(ex);
		}
		return pdfInciso;
	}
	
	public byte[] imprimirPoliza(DatosCaratulaPoliza datosCaratulaPoliza,
			Locale locale) {

		List<DatosPolizaDTO> datasourcePoliza = new ArrayList<DatosPolizaDTO>(1);
		List<DatosLineasDeNegocioDTO> datasourceLineasNegocio = datosCaratulaPoliza
				.getDatasourceLineasNegocio();
		byte[] pdfPoliza = null;

		JasperReport reportePoliza = getJasperReport(
				UR_JRXML+"CaratulaPolizaM2.jrxml");
		JasperReport reporteLineas = getJasperReport(
				UR_JRXML+"DesgloseLineasM2.jrxml");

		Map<String, Object> parameters = new HashMap<String, Object>(1);
		if (locale != null)
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		parameters.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_SEGUROS_AFIRME);
		parameters.put(KEY_SIGNATURES,
				SistemaPersistencia.FIRMA_FUNCIONARIO);
		parameters.put("lineasDataSource", datasourceLineasNegocio);
		parameters.put("lineasReport", reporteLineas);	
		parameters.put(JRParameter.REPORT_VIRTUALIZER, new JRGzipVirtualizer(2));

		datasourcePoliza.add(datosCaratulaPoliza.getDatosPolizaDTO());
		JRBeanCollectionDataSource dsPoliza = new JRBeanCollectionDataSource(
				datasourcePoliza);
		pdfPoliza = generarReporte(reportePoliza, parameters, dsPoliza);
		
		return pdfPoliza;
	}
	
	public PrintReport getServicioImpresionED(){
		try {
			PrintReportServiceLocator locator = new PrintReportServiceLocator(
					SistemaPersistencia.URL_WS_IMPRESION, SistemaPersistencia.PUERTO_WS_IMPRESION);						
			return locator.getPrintReport();		
		} catch (Exception e) {
			throw new RuntimeException("WebService de recibos no disponible");
		}
	}
	
	public byte[] getNationalUnity(String idToCotizacion, int idLinea, int idInciso){
		byte[] pdf = null;
		try{
			pdf = this.getServicioImpresionED().getNationalUnityCertificateNew(idToCotizacion, 
					idLinea, idInciso, "application/pdf");
		}catch(Exception ex){
			LOG.error(ex.getMessage(), ex);
		}
		return pdf;
	}
	
	@Override
	public TransporteImpresionDTO imprimirCotizacion(BigDecimal idToCotizacion,
			Locale locale, boolean caratula, int incisoIncial, int incisoFinal, boolean isOutsider) {
		return this.imprimirCotizacion(idToCotizacion, locale, caratula, false, incisoIncial, incisoFinal,isOutsider);
	}
	
	@Override
	public TransporteImpresionDTO imprimirCotizacion(BigDecimal idToCotizacion,
			Locale locale, boolean caratula, boolean todosLosIncisos, boolean isOutsider) {
		return this.imprimirCotizacion(idToCotizacion, locale, caratula, todosLosIncisos, 0, 0,isOutsider);
	}
	
	@Override
	public TransporteImpresionDTO imprimirCotizacion(BigDecimal idToCotizacion,
			Locale locale, boolean caratula, boolean todosLosIncisos, boolean isMovil, String email, boolean isOutsider) {
		if( isMovil ){
			return this.imprimirCotizacionMovil(idToCotizacion, locale, caratula, todosLosIncisos, 0, 0,email);
		} else {
			return this.imprimirCotizacion(idToCotizacion, locale, caratula, todosLosIncisos, 0, 0,isOutsider);
		}
	}
	
	private TransporteImpresionDTO imprimirCotizacion(BigDecimal idToCotizacion,
			Locale locale, boolean caratula, boolean todosLosIncisos,
			int incisoInicial, int incisoFinal, boolean isOutsider) {
		
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class,idToCotizacion);
		idToNegocio=cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio();
		
		DatosCaratulaCotizacion datosCaratulaCotizacion = null;
		TransporteImpresionDTO transporte = new TransporteImpresionDTO();	
		List<byte[]> inputByteArray = new ArrayList<byte[]>(1);		
		ByteArrayOutputStream output = new ByteArrayOutputStream();	
		List<DatosCaratulaInciso> dataSourceIncisos = new ArrayList<DatosCaratulaInciso>(1);
		try{
			long start = System.currentTimeMillis();
			LOG.info("Entrando llenarCotizacionDTO() impresion de cotizacion : " + idToCotizacion + " idToNegocio = " + idToNegocio);  
			if(caratula){
				datosCaratulaCotizacion = impresionesService.llenarCotizacionDTO(idToCotizacion);
				inputByteArray.add(imprimirCotizacion(datosCaratulaCotizacion, locale, idToNegocio));
			}else{
				datosCaratulaCotizacion = new DatosCaratulaCotizacion();
				datosCaratulaCotizacion.setContenedorDatosImpresion(new ContenedorDatosImpresion());
				datosCaratulaCotizacion.setCotizacionDTO(entidadService.findById(CotizacionDTO.class, idToCotizacion));
			}
			LOG.info("Saliendo llenarCotizacionDTO() impresion de cotizacion : " + idToCotizacion);
			
			start = System.currentTimeMillis();
			LOG.info("Entrando llenarIncisoDTO() impresion de cotizacion : " + idToCotizacion);  
			if(todosLosIncisos){
				dataSourceIncisos = impresionesService.llenarIncisoDTO(datosCaratulaCotizacion.getCotizacionDTO(),"", datosCaratulaCotizacion.getContenedorDatosImpresion());				
			}else if(incisoInicial > 0 && incisoFinal > 0 && incisoFinal >= incisoInicial){
				dataSourceIncisos = impresionesService.llenarIncisoDTO(datosCaratulaCotizacion.getCotizacionDTO(),"", incisoInicial, incisoFinal, datosCaratulaCotizacion.getContenedorDatosImpresion());
			}
			LOG.info("Saliendo llenarIncisoDTO() impresion de cotizacion : " + idToCotizacion);
			
			start = System.currentTimeMillis();
			LOG.info("Entrando preparar PDF impresion de cotizacion : " + idToCotizacion);  
			if(!dataSourceIncisos.isEmpty()){			
				for(DatosCaratulaInciso inciso : dataSourceIncisos){
					inputByteArray.add(imprimirInciso(inciso, locale,cotizacion));
				}
			}			
			pdfConcantenate(inputByteArray, output);
			transporte.setByteArray(output.toByteArray());
			LOG.info("Saliendo preparar PDF impresion de cotizacion : " + idToCotizacion);  
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}
		
		transporte.setFileName( "Cotizacion_" + idToCotizacion + ".pdf" );
		transporte.setContentType( ConstantesReporte.TIPO_PDF );
		
		return transporte;
	}
	
	private TransporteImpresionDTO imprimirCotizacionMovil( BigDecimal idToCotizacion, Locale locale, boolean caratula, 
				boolean todosLosIncisos, int incisoInicial, int incisoFinal, String email) {
		
		TransporteImpresionDTO transporte = new TransporteImpresionDTO();
		List<DatosCaratulaInciso> dataSourceIncisos = new ArrayList<DatosCaratulaInciso>(1);
		DatosCaratulaCotizacion datosCaratulaCotizacion = new DatosCaratulaCotizacion();
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class,idToCotizacion);
		
		SolicitudDTO solicitud = cotizacion.getSolicitudDTO();
		Agente agenteRemp = new Agente();
		agenteRemp.setId(solicitud.getCodigoAgente().longValue());
		agenteRemp = agenteMidasService.loadById(agenteRemp);
		
		Agente agente = (solicitud.getAgente() != null)?solicitud.getAgente():agenteRemp;
		idToNegocio=cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio();
		datosCaratulaCotizacion.setCotizacionDTO(cotizacion);
		datosCaratulaCotizacion.setContenedorDatosImpresion(new ContenedorDatosImpresion());
		
		try{
			datosCaratulaCotizacion = impresionesService.llenarCotizacionDTO(idToCotizacion);
//			inputByteArray.add(imprimirCotizacion(datosCaratulaCotizacion, locale));

			if(todosLosIncisos){
				dataSourceIncisos = impresionesService.llenarIncisoDTO(cotizacion,"", datosCaratulaCotizacion.getContenedorDatosImpresion());				
			}else if(incisoInicial > 0 && incisoFinal > 0 && incisoFinal >= incisoInicial){
				dataSourceIncisos = impresionesService.llenarIncisoDTO(cotizacion,"", incisoInicial, incisoFinal, datosCaratulaCotizacion.getContenedorDatosImpresion());
			}
  
			if(!dataSourceIncisos.isEmpty()){
				int index = 0;
				for(DatosCaratulaInciso inciso : dataSourceIncisos){
					byte[] byteArr = imprimirCaratulaMovil(inciso, locale, agente, index,email);
					if(byteArr!=null){
						transporte.setByteArray(byteArr);
					}
					index++;
				}
			}

			transporte.setContentType(ConstantesReporte.TIPO_TXT_UTF8);
			transporte.setFileName("Cotizacion_Seguro_Obligatorio.txt");
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}
		return transporte;
	}
	
	private byte[] imprimirCaratulaMovil(DatosCaratulaInciso inciso, Locale locale, Agente agente, int index, String email) {

		byte[] certificado = null;
		String caratulaM2 = UR_JRXML_SO + "/Caratula_M2.jrxml";
		String desgloseM2 = UR_JRXML_SO + "/DesgloseCoberturasM2.jrxml";
		Map<String, Object> parameters = new HashMap<String, Object>(1);
		List<DatosIncisoDTO> reporteDatasourceInciso = new ArrayList<DatosIncisoDTO>(1);
		DatosIncisoDTO datosInciso = inciso.getDataSourceIncisos();
		String observaciones = datosInciso.getObservaciones();
		IncisoCotizacionDTO incisoCot = inciso.getListadoIncisos().get(index);
		CotizacionDTO cotizacion = incisoCot.getCotizacionDTO();
		IncisoAutoCot incisoAutoCot = incisoCot.getIncisoAutoCot();
		String nombreAsegurado = incisoAutoCot.getNombreAsegurado();
		nombreAsegurado = nombreAsegurado==null?cotizacion.getNombreContratante():nombreAsegurado;

		try {
			// Por cada uno de estos DatosIncisoDTO se hará una página
			reporteDatasourceInciso.add(datosInciso);

			InputStream caratulaStream = this.getClass().getResourceAsStream(caratulaM2);
			InputStream desgloseCoberturasStream = this.getClass().getResourceAsStream(desgloseM2);
			
			JasperReport reporteInciso = JasperCompileManager.compileReport(caratulaStream);
			JasperReport reporteCoberturas = JasperCompileManager.compileReport(desgloseCoberturasStream);

			JRBeanCollectionDataSource dsInciso = new JRBeanCollectionDataSource( reporteDatasourceInciso);
			Persona personaAgent = agente.getPersona();
			String emailAgente = personaAgent.getEmail()==null?"":personaAgent.getEmail();

			if (locale != null)
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			
			// Estas son las coberturas para cada inciso
			List<DatosCoberturasDTO> dcDTOList = inciso.getDataSourceCoberturas();
			parameters.put("coberturasDataSource", dcDTOList);
			parameters.put("coberturasReport", reporteCoberturas);
			parameters.put("idAgente", agente.getIdAgente());
			parameters.put("telCelularAgente",(personaAgent!=null)?personaAgent.getTelefonoCelular():"");
			parameters.put("title", "CERTIFICADO DE SEGURO PARA\n AUTOMOVILES INDIVIDUALES");
			parameters.put("subtitle", "Cotización No. ");
			parameters.put("numeroTelefonoSPV", listadoService.getSpvData(idToNegocio).get("numeroTelefonoSPV"));
			parameters.put(JRParameter.REPORT_VIRTUALIZER,new JRGzipVirtualizer(2));
			parameters.put("observaciones", observaciones);
			parameters.put("noFolio", cotizacion.getFolio());
			parameters.put("nombreAsegurado", nombreAsegurado);
			parameters.put("e_mail", email);
			//parameters.put("emailAgente", emailAgente);

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			JasperPrint jasperPrint = JasperFillManager.fillReport(reporteInciso, parameters, dsInciso);
			JRTextExporter rtfExporter = new JRTextExporter();
			rtfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			rtfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,outputStream);
			rtfExporter.setParameter(JRTextExporterParameter.CHARACTER_WIDTH, new Float(7));
			rtfExporter.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT, new Float(10));
			rtfExporter.exportReport();
			
			certificado = outputStream.toByteArray();
		} catch (JRException ex) {
			throw new RuntimeException(ex.getMessage(), ex);
		}
		return certificado;
	}
	
	@Override
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToCotizacion,
			BigDecimal idToPoliza, Locale locale, boolean caratula,
			boolean recibo, boolean incisos, boolean certificadoRC, boolean anexosInciso){
		return this.imprimirPoliza(idToCotizacion, idToPoliza, locale, caratula, recibo, 
				incisos, 0, 0, certificadoRC, anexosInciso);
	}
	
	@Override
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToCotizacion,
			BigDecimal idToPoliza, Locale locale, boolean caratula,
			boolean recibo, boolean certificadoRC, boolean anexosInciso, 
			int incisoInicial, int incisoFinal){
		return this.imprimirPoliza(idToCotizacion, idToPoliza, locale, caratula, recibo, 
				false, incisoInicial, incisoFinal, certificadoRC, anexosInciso);
	}

	private TransporteImpresionDTO imprimirPoliza(BigDecimal idToCotizacion,
			BigDecimal idToPoliza, Locale locale, boolean caratula,
			boolean recibo, boolean todosIncisos, int incisoInicial,
			int incisoFinal, boolean certificadoRC, boolean anexosInciso) {
		String llaveFiscal = null;		
		byte[] reciboFiscal = null;
		byte[] nationalUnity = null;
		DatosCaratulaPoliza datosCaratulaPoliza = null;
		TransporteImpresionDTO transporte = new TransporteImpresionDTO();
		ByteArrayOutputStream output = new ByteArrayOutputStream();		
		List<byte[]> inputByteArray = new ArrayList<byte[]>();		
		List<DatosCaratulaInciso> dataSourceIncisos = new ArrayList<DatosCaratulaInciso>();
		
		try{			
			
			//
			// Caratula
			//			
			if(caratula){
				datosCaratulaPoliza = impresionesService.llenarPolizaDTO(idToPoliza, incisoInicial,incisoFinal);
				inputByteArray.add(imprimirPoliza(datosCaratulaPoliza, locale));
			}else{
				datosCaratulaPoliza = new DatosCaratulaPoliza();
				datosCaratulaPoliza.setContenedorDatosImpresion(new ContenedorDatosImpresion());
				datosCaratulaPoliza.setPolizaDTO(entidadService.findById(PolizaDTO.class, idToPoliza));
			}
			
			datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO().setIdToCotizacion(idToCotizacion);
			
			//
			// Recibos
			//			
			if(recibo){
				reciboFiscal = impresionRecibosService.getReciboFiscal(llaveFiscal);
				if(reciboFiscal != null){
					inputByteArray.add(reciboFiscal);	
				}
			}
			
			//
			// Incisos
			//			
			if(todosIncisos){
				dataSourceIncisos = impresionesService.llenarIncisoDTO(
						datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO(), datosCaratulaPoliza.getPolizaDTO().getNumeroPolizaFormateada(), 
						datosCaratulaPoliza.getContenedorDatosImpresion());
			}else if(incisoInicial > 0 && incisoFinal > 0 && incisoFinal >= incisoInicial){
				dataSourceIncisos = impresionesService.llenarIncisoDTO(
						datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO(), datosCaratulaPoliza.getPolizaDTO().getNumeroPolizaFormateada(), 
						incisoInicial, incisoFinal, datosCaratulaPoliza.getContenedorDatosImpresion());
			}
			
			//
			// Incisos y certificados
			//			
			if(!dataSourceIncisos.isEmpty()){		
				for(DatosCaratulaInciso inciso : dataSourceIncisos){
					inputByteArray.add(imprimirInciso(inciso, locale,datosCaratulaPoliza.getPolizaDTO().getCotizacionDTO()));					
					//national unity
					if(certificadoRC && inciso.getDataSourceIncisos().isNationalUnity()){
						nationalUnity = this.getNationalUnity(idToCotizacion.multiply(BigDecimal.valueOf(-1)).toString(), 
								Integer.valueOf(inciso.getDataSourceIncisos().getIdSeccion()), 
								Integer.valueOf(inciso.getDataSourceIncisos().getIdInciso()));
						if(nationalUnity != null){
							inputByteArray.add(nationalUnity);
						}
					}
				}		
			}
			
			pdfConcantenate(inputByteArray, output);
			transporte.setByteArray(output.toByteArray());			
		}
		catch(Exception e){
			throw new RuntimeException(e);
		}
		return transporte;		
	}
	
	/**
	 * Imprime la configuracion de un negocio
	 * @param negocioDatos
	 * @param locale
	 * @return
	 * @throws JRException 
	 * @autor martin
	 */
	private byte[] imprimirConfiguracionNegocio(DatosNegocioDTO negocioDatos, Locale locale) throws JRException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<byte[]> inputByteArray = new ArrayList<byte[]>();
		ByteArrayOutputStream pdfDConfiguracionNegocio =  new ByteArrayOutputStream();
			inputByteArray.add(imprimirCaratulaNegocio(parameters, locale, negocioDatos));
			inputByteArray.add(imprimirNegocioProducto(parameters, locale, negocioDatos.getDatosProductosDeNegocioDetalleDTOs()));
			inputByteArray.add(imprimirNegocioCliente(parameters, locale, negocioDatos.getNegocioClientes()));
			inputByteArray.add(imprimirNegocioAgente(parameters, locale, negocioDatos.getNegocioAgentes()));
			inputByteArray.add(imprimirNegocioParametrosGenerales(parameters, locale, negocioDatos));
			inputByteArray.add(imprimirNegocioBonosYcomisiones(parameters, locale, negocioDatos.getNegocioBonoComisions()));
			inputByteArray.add(imprimirNegocioZonaCirculacion(parameters, locale, negocioDatos.getEstados()));
			inputByteArray.add(imprimirNegocioRiesgosNormales(parameters, locale, negocioDatos));
			inputByteArray.add(imprimirCompensaciones(parameters, locale, negocioDatos));
			
			inputByteArray.add(imprimirRecuotificacion(parameters, locale, negocioDatos.getNegocioRecuotificacion()));

			pdfConcantenate(inputByteArray, pdfDConfiguracionNegocio);
		return pdfDConfiguracionNegocio.toByteArray();
	}
	
	private byte[] imprimirNegocioRiesgosNormales(Map<String, Object> parameters,
			Locale locale, DatosNegocioDTO negocioDatos) throws JRException {
		parameters.clear();
		List<NegocioRenovacion> dataSourceRiesgosNormales = new LinkedList<NegocioRenovacion>();
		dataSourceRiesgosNormales = negocioDatos.getNegocioRenovacions();

		JasperReport reporteNegocioInfoGeneral_condicionesDeRenovacion_riesgosNormales = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_riesgosNormales.jrxml");

		JasperReport reporteNegocioInfoGeneral_condicionesDeRenovacion_riesgosNormales_tablaDescuento = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_riesgosNormales_tablaDescuento.jrxml");

		JasperReport reporteNegocioInfoGeneral_condicionesDeRenovacion_riesgosNormales_tablaDescuento_danios = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_riesgosNormales_tablaDescuento_danios.jrxml");
		if (locale != null) {
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		parameters.put("renovacionDescuentosList",
				negocioDatos.getRenovacionDescuentosList());
		parameters
				.put("negocioInfoGeneral_condicionesDeRenovacion_riesgosNormales",
						reporteNegocioInfoGeneral_condicionesDeRenovacion_riesgosNormales);
		parameters
				.put("negocioInfoGeneral_condicionesDeRenovacion_riesgosNormales_tablaDescuento",
						reporteNegocioInfoGeneral_condicionesDeRenovacion_riesgosNormales_tablaDescuento);
		parameters
				.put("negocioInfoGeneral_condicionesDeRenovacion_riesgosNormales_tablaDescuento_danios",
						reporteNegocioInfoGeneral_condicionesDeRenovacion_riesgosNormales_tablaDescuento_danios);
		JRBeanCollectionDataSource dsRiesgosNormales = new JRBeanCollectionDataSource(
				dataSourceRiesgosNormales);
		
		return (JasperRunManager
				.runReportToPdf(
						reporteNegocioInfoGeneral_condicionesDeRenovacion_riesgosNormales,
						parameters, dsRiesgosNormales));
	}
	
	private byte[] imprimirRecuotificacion(Map<String, Object> parameters,
			Locale locale, NegocioRecuotificacion negocioRecuotificacion) throws JRException {
		parameters.clear();
		List<NegocioRecuotificacion> dataSourceNegocioRecuotificacion = null;
		if(negocioRecuotificacion!=null){
			dataSourceNegocioRecuotificacion= new LinkedList<NegocioRecuotificacion>();
			dataSourceNegocioRecuotificacion.add(negocioRecuotificacion);
		}
		JasperReport reporteNegocioInfoGeneral_recuotificacion = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_recuotificacion.jrxml");
		System.out.println("reporteNegocioInfoGeneral_recuotificacion ok");

		JasperReport reporteNegocioInfoGeneral_recuotifAutmvers = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_recuotif_Autm_vers.jrxml");
		System.out.println("reporteNegocioInfoGeneral_recuotifAutmvers ok");

		JasperReport reporteNegocioInfoGeneral_recuotifAutmVerProgs = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_recuotif_Autm_vers_progs.jrxml");

		JasperReport reporteNegocioInfoGeneral_recuotifAutmVerProgRecs = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_recuotif_Autm_vers_prog_recs.jrxml");
		System.out.println("reporteNegocioInfoGeneral_recuotifAutmVerProgRecs ok");

		if (locale != null) {
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		parameters.put("negocioInfoGeneral_recuotificacion",
				reporteNegocioInfoGeneral_recuotificacion);
		parameters.put("negocioInfoGeneral_recuotif_Autm_vers",
				reporteNegocioInfoGeneral_recuotifAutmvers);
		parameters.put("negocioInfoGeneral_recuotif_Autm_vers_progs",
				reporteNegocioInfoGeneral_recuotifAutmVerProgs);
		parameters.put("negocioInfoGeneral_recuotif_Autm_vers_prog_recs",
				reporteNegocioInfoGeneral_recuotifAutmVerProgRecs);
		
		
		JRBeanCollectionDataSource dsProductos = new JRBeanCollectionDataSource(
				dataSourceNegocioRecuotificacion);
		System.out.println("dsProductos ok");

		return (JasperRunManager.runReportToPdf(
				reporteNegocioInfoGeneral_recuotificacion, parameters, dsProductos));
		
	}
	private byte[] imprimirCompensaciones(Map<String, Object> parameters,
			Locale locale, DatosNegocioDTO negocioDatos) throws JRException {
		parameters.clear();
		List<DatosNegocioDTO> dsdDatosNegocioDTOs = new LinkedList<DatosNegocioDTO>();
		dsdDatosNegocioDTOs.add(negocioDatos);
       
		JasperReport compensaciones_reporteNegocioInfoGeneral = getJasperReport(UR_JRXML + "compensacionesInfoGeneral.jrxml");
		JasperReport reporte_Parametros_Compensaciones = getJasperReport(UR_JRXML + "compensacionesInfoGeneralGrid.jrxml");
		JasperReport reporte_Parametros_Compensaciones_bajaSiniestralidad = getJasperReport(UR_JRXML + "compensacionesInfoGeneralGrid_BS.jrxml");
		boolean regresarHoja = false;
		if (locale != null) {
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		LOG.info("NUMERO RE REGISTROS ="+ negocioDatos.getReportePrima_poliza());
		
		parameters.put("invocando_JRXML_Principal",compensaciones_reporteNegocioInfoGeneral);
		if(negocioDatos.getReportePrima_poliza().size() >0 && negocioDatos.getReportePrima_poliza() != null){
			parameters.put("parametrosNegocio_CompensacionesList",negocioDatos.getReportePrima_poliza());
			parameters.put("reporte_parametrosNegocio_Compensaciones",reporte_Parametros_Compensaciones);
			regresarHoja=true;
		}
		if(negocioDatos.getReporteBs().size() > 0 && negocioDatos.getReporteBs()!= null){
			parameters.put("parametrosNegocio_CompensacionesList_BS",negocioDatos.getReporteBs());
			parameters.put("reporte_parametrosNegocio_Compensaciones_bajaSinestralidad",reporte_Parametros_Compensaciones_bajaSiniestralidad);
			regresarHoja=true;
		} 
		if(regresarHoja){
			JRBeanCollectionDataSource dsCollection_caParametros = new JRBeanCollectionDataSource(
					dsdDatosNegocioDTOs);
	
			return (JasperRunManager.runReportToPdf(
					compensaciones_reporteNegocioInfoGeneral,
					parameters, dsCollection_caParametros));
		}else{
			return null;
		}
	}
		
	private byte[] imprimirNegocioZonaCirculacion(
			Map<String, Object> parameters, Locale locale,
			List<NegocioEstado> dataSourceZonaCirculacion) throws JRException {
		parameters.clear();
		JasperReport reporteNegocioInfoGeneral_zonaCirculacion = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_zonaCirculacion.jrxml");

		JasperReport reporteNegocioInfoGeneral_zonaCirculacion_municipio = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_zonaCirculacion_municipio.jrxml");
		if (locale != null) {
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		parameters.put("negocioInfoGeneral_zonaCirculacion",
				reporteNegocioInfoGeneral_zonaCirculacion);
		parameters.put("negocioInfoGeneral_zonaCirculacion_municipio",
				reporteNegocioInfoGeneral_zonaCirculacion_municipio);
		JRBeanCollectionDataSource dsZonaCirculacion = new JRBeanCollectionDataSource(
				dataSourceZonaCirculacion);
		return (JasperRunManager.runReportToPdf(
				reporteNegocioInfoGeneral_zonaCirculacion, parameters,
				dsZonaCirculacion));
	}

	private byte[] imprimirNegocioBonosYcomisiones(
			Map<String, Object> parameters, Locale locale,
			List<NegocioBonoComision> dataSourceBonosYcomisiones)
			throws JRException {
		parameters.clear();
		JasperReport reporteNegocioInfoGeneral_bonosYcomisiones = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_bonosYcomisiones.jrxml");
		if (locale != null) {
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		parameters.put("negocioInfoGeneral_bonosYcomisiones",
				reporteNegocioInfoGeneral_bonosYcomisiones);
		JRBeanCollectionDataSource dsBonosYcomisiones = new JRBeanCollectionDataSource(
				dataSourceBonosYcomisiones);
		return (JasperRunManager.runReportToPdf(
				reporteNegocioInfoGeneral_bonosYcomisiones, parameters,
				dsBonosYcomisiones));
	}

	private byte[] imprimirNegocioParametrosGenerales(
			Map<String, Object> parameters, Locale locale,
			DatosNegocioDTO negocioDatos) throws JRException {
		parameters.clear();
		List<DatosNegocioDTO> dataSourceParametrosGenerales = new LinkedList<DatosNegocioDTO>();
		dataSourceParametrosGenerales.add(negocioDatos);
		
		JasperReport reporteNegocioInfoGeneral_parametrosGenerales= getJasperReport(
				UR_JRXML+"negocioInfoGeneral_parametrosGenerales.jrxml");

		JasperReport reporteNegocioInfoGeneral_parametrosGenerales_derechosEndoso = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_parametrosGenerales_derechosEndoso.jrxml");
		
		JasperReport reporteNegocioInfoGeneral_parametrosGenerales_derechosPoliza = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_parametrosGenerales_derechosPoliza.jrxml");
		
		if (locale != null) {
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		parameters.put("negocio", negocioDatos.getNegocio());
		parameters.put("negocioDerechoEndosos",negocioDatos.getNegocioDerechoEndosos());
		parameters.put("negocioDerechoPolizas", negocioDatos.getNegocioDerechoPolizas());
		parameters.put("negocioInfoGeneral_parametrosGenerales",
				reporteNegocioInfoGeneral_parametrosGenerales);
		parameters.put("negocioInfoGeneral_parametrosGenerales_derechosEndoso",
				reporteNegocioInfoGeneral_parametrosGenerales_derechosEndoso);
		parameters.put("negocioInfoGeneral_parametrosGenerales_derechosPoliza",
				reporteNegocioInfoGeneral_parametrosGenerales_derechosPoliza);
		JRBeanCollectionDataSource dsParametrosGenerales = new JRBeanCollectionDataSource(
				dataSourceParametrosGenerales);
		return (JasperRunManager.runReportToPdf(
				reporteNegocioInfoGeneral_parametrosGenerales, parameters,
				dsParametrosGenerales));
	}

	private byte[] imprimirNegocioAgente(Map<String, Object> parameters,
			Locale locale, List<NegocioAgente> dataSourceAgentes)
			throws JRException {
		parameters.clear();
		JasperReport reporteNegocioInfoGeneral_agentes = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_agentes.jrxml");
		if (locale != null) {
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		parameters.put("negocioInfoGeneral_agentes",
				reporteNegocioInfoGeneral_agentes);
		JRBeanCollectionDataSource dsAgentes = new JRBeanCollectionDataSource(
				dataSourceAgentes);
		return (JasperRunManager.runReportToPdf(
				reporteNegocioInfoGeneral_agentes, parameters, dsAgentes));
	}
	
	private byte[] imprimirNegocioCliente(Map<String, Object> parameters,
			Locale locale, List<NegocioCliente> dataSourceClientes)
			throws JRException {
		parameters.clear();
		JasperReport reporteNegocioInfoGeneral_clientes = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_clientes.jrxml");
		if (locale != null) {
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		parameters.put("negocioInfoGeneral_clientes",
				reporteNegocioInfoGeneral_clientes);
		JRBeanCollectionDataSource dsClientes = new JRBeanCollectionDataSource(
				dataSourceClientes);
		return (JasperRunManager.runReportToPdf(
				reporteNegocioInfoGeneral_clientes, parameters, dsClientes));
	}

	private byte[] imprimirNegocioProducto(Map<String, Object> parameters,
			Locale locale,
			List<DatosProductosDeNegocioDetalleDTO> dataSourceProductos)
			throws JRException {
		parameters.clear();
		
		JasperReport reporteNegocioInfoGeneral_productos = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_productos.jrxml");

		JasperReport reporteNegocioInfoGeneral_condicionesCobranza = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_productos_condicionesCobranza.jrxml");

		JasperReport reporteNegocioInfoGeneral_condicionesCobranza_formasDePago = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_productos_condicionesCobranza_formasDePago.jrxml");

		JasperReport reporteNegocioInfoGeneral_condicionesCobranza_medioDePago = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_productos_condicionesCobranza_medioDePago.jrxml");

		JasperReport reporteNegocioInfoGeneral_tipoPoliza = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_tipoPolizas.jrxml");

		JasperReport reporteNegocioInfoGeneral_lineasNegocio = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_lineasDeNegocio.jrxml");

		JasperReport reporteNegocioInfoGeneral_lineasNegocio_tipoUso = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_lineasDeNegocio_tipoUso.jrxml");

		JasperReport reporteNegocioInfoGeneral_lineasNegocio_tipoServicio = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_lineasDeNegocio_tipoServicio.jrxml");

		JasperReport reporteNegocioInfoGeneral_lineasNegocio_estilo = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_lineasDeNegocio_estilo.jrxml");

		JasperReport reporteNegocioInfoGeneral_paquetes = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_paquetes.jrxml");

		JasperReport reporteNegocioInfoGeneral_paquetes_incluidos = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_paquetes_paquetesDetalle.jrxml");

		JasperReport reporteNegocioInfoGeneral_coberturas = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_coberturas.jrxml");

		JasperReport reporteNegocioInfoGeneral_coberturas_detalle = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_coberturas_detalle.jrxml");

		JasperReport reporteNegocioInfoGeneral_tarifas = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_tarifas.jrxml");

		JasperReport reporteNegocioInfoGeneral_tarifas_detalle = getJasperReport(
				UR_JRXML+"negocioInfoGeneral_tarifas_detalle.jrxml");

		if (locale != null) {
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		parameters.put("negocioInfoGeneral_productos",
				reporteNegocioInfoGeneral_productos);
		parameters.put("negocioInfoGeneral_tiposPoliza",
				reporteNegocioInfoGeneral_tipoPoliza);
		parameters.put("negocioInfoGeneral_lineasNegocio",
				reporteNegocioInfoGeneral_lineasNegocio);
		parameters.put("negocioInfoGeneral_lineasNegocio_tipoUso",
				reporteNegocioInfoGeneral_lineasNegocio_tipoUso);
		parameters.put("negocioInfoGeneral_lineasNegocio_tipoServicio",
				reporteNegocioInfoGeneral_lineasNegocio_tipoServicio);
		parameters.put("negocioInfoGeneral_lineasNegocio_estilo",
				reporteNegocioInfoGeneral_lineasNegocio_estilo);
		parameters.put("negocioInfoGeneral_paquetes",
				reporteNegocioInfoGeneral_paquetes);
		parameters.put("negocioInfoGeneral_paquetes_incluidos",
				reporteNegocioInfoGeneral_paquetes_incluidos);
		parameters.put("negocioInfoGeneral_coberturas",
				reporteNegocioInfoGeneral_coberturas);
		parameters.put("negocioInfoGeneral_coberturas_detalle",
				reporteNegocioInfoGeneral_coberturas_detalle);
		parameters.put("negocioInfoGeneral_tarifas",
				reporteNegocioInfoGeneral_tarifas);
		parameters.put("negocioInfoGeneral_tarifas_detalle",
				reporteNegocioInfoGeneral_tarifas_detalle);
		parameters.put("negocioInfoGeneral_condicionesCobranza",
				reporteNegocioInfoGeneral_condicionesCobranza);
		parameters.put("negocioInfoGeneral_condicionesCobranza_formasDePago",
				reporteNegocioInfoGeneral_condicionesCobranza_formasDePago);
		parameters.put("negocioInfoGeneral_condicionesCobranza_medioDePago",
				reporteNegocioInfoGeneral_condicionesCobranza_medioDePago);
		JRGzipVirtualizer gzipVirtualizer = new JRGzipVirtualizer(MAX_GZIPVIRTUALIZER);
		parameters.put(JRParameter.REPORT_VIRTUALIZER, gzipVirtualizer);
		JRBeanCollectionDataSource dsProductos = new JRBeanCollectionDataSource(
				dataSourceProductos);
		return (JasperRunManager.runReportToPdf(
				reporteNegocioInfoGeneral_productos, parameters, dsProductos));
	}

	private byte[] imprimirCaratulaNegocio(Map<String, Object> parameters,
			Locale locale, DatosNegocioDTO negocio) throws JRException {
		List<DatosNegocioDTO> datasourceNegocio = new ArrayList<DatosNegocioDTO>(1);
		datasourceNegocio.add(negocio);
		JasperReport reporteNegocioInfoGeneral = getJasperReport(
				UR_JRXML+"negocioInfoGeneral.jrxml");
		if (locale != null) {
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		parameters.put(IMGHEADER, SistemaPersistencia.LOGO_AFIRME);
		parameters.put("usuarioActivacion",negocio.getUsuarioCreador());
		parameters.put("reporteNegocioInfoGeneral", reporteNegocioInfoGeneral);
		JRBeanCollectionDataSource dsNegocio = new JRBeanCollectionDataSource(
				datasourceNegocio);
		return (JasperRunManager.runReportToPdf(reporteNegocioInfoGeneral,
				parameters, dsNegocio));
	}
	
	@Override
	public TransporteImpresionDTO imprimirConfiguracionNegocio(Long idToNegocio, Locale locale) {
		TransporteImpresionDTO transporteImpresionDTO = null;
			DatosNegocioDTO negocioDatos = impresionesService.llenarNegocioDTO(idToNegocio);
			if (negocioDatos != null) {
				transporteImpresionDTO = new TransporteImpresionDTO();
				try {
					transporteImpresionDTO.setByteArray(imprimirConfiguracionNegocio(negocioDatos, locale));
				} catch (JRException e) {
					LOG.error(e.getMessage(), e);
				}
			}
		return transporteImpresionDTO;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteEstadoDeCuentaAgente(
			Long idEmpresa, Long idAgente, Long anioMes,Boolean guiaLectura, Boolean detalle, Boolean mostrarColAdicionales,Boolean afirmeComunica,String afirmeComunicaText, Boolean tieneRole) {
		TransporteImpresionDTO transporteImpresionDTO = null;
		List<DatosEdoctaAgenteAcumuladoView> acumuladoViews = new ArrayList<DatosEdoctaAgenteAcumuladoView>(1);
		 acumuladoViews = impresionesService.llenarDatosEdoctaAgente(idEmpresa, idAgente, anioMes);
		if (acumuladoViews != null) {
			Map<String,Object> params = new HashMap<String, Object>(1);
			try {
				final String anio = anioMes.toString().substring(0, 4);
				final String mes = anioMes.toString().substring(4, 6);
				String month = MailServiceSupport.monthsArray[Integer.parseInt(mes) - 1];
				params.put("anioMes", month.toUpperCase() + " " + anio);
//				if (acumuladoViews.get(0).getMovimientos() != null
//						&& acumuladoViews.get(0).getMovimientos().get(0)
//								.getCierremes() != null) {
//					params.put("cierremes", acumuladoViews.get(0)
//							.getMovimientos().get(0).getCierremes());
//				} else {
//					params.put("cierremes", new Double(0.0));					
//				}
				params.put("cierremes", acumuladoViews.get(0).getSaldoFinal().doubleValue());
				if (detalle) {
					JasperReport edoctaMovimientosDetalle = getJasperReport(UR_JRXML+"edoctaAfirme_movimientos.jrxml");
					params.put("edoctaAfirme_movimientos",edoctaMovimientosDetalle);
					
					
				}
				if (guiaLectura) {
					LOG.trace("1337: Se accedio al archivo del Reporte de guia de lectura");
					JasperReport guiaLecturaReport = getJasperReport(UR_JRXML+"guiaDeLecturaAgentes.jrxml");
					params.put("edoctaAfirme_guiaLectura",guiaLecturaReport);
					LOG.trace("1337: Se agrego reporte a params");
				}
				if (afirmeComunica) {
					LOG.trace("1337: Se accedio al archivo del Reporte afirme comunica");
					JasperReport afirmeComunicaReport = getJasperReport(UR_JRXML+"afirmeComunica.jrxml");
					params.put("edoctaAfirme_afirmeComunica",afirmeComunicaReport);
					params.put("afirmeComunicaText",afirmeComunicaText);
				}
				
				if (mostrarColAdicionales != null) {
					Boolean MostrarColAdicionalesReport;
					MostrarColAdicionalesReport = mostrarColAdicionales;
					
					if (MostrarColAdicionalesReport){
						params.put("MostrarColAdicionales", false);
					}else{
						params.put("MostrarColAdicionales", true);
					}
					
					LOG.debug("Juan:" + mostrarColAdicionales.toString() + " " + MostrarColAdicionalesReport.toString());
				}
				
				DatosEdoctaAgenteProduccion produccion = new DatosEdoctaAgenteProduccion();
				 produccion = impresionesService.llenarDatosEdoctaAgenteProduccion(idAgente, anio, mes);
				if (produccion != null) {
					params.put("produccion", produccion);
				}
				Calendar fechaInicioMes = Calendar.getInstance();
				Calendar fechaFinMes = Calendar.getInstance();
				Calendar fechaInicioAnio = Calendar.getInstance();
				Calendar fechaFinAnio = Calendar.getInstance();
				Calendar fechaUltimoAnioInicio = Calendar.getInstance();
				Calendar fechaUltimoAnioFin = Calendar.getInstance();
				fechaInicioMes.set(Calendar.MONTH, (Integer.parseInt(mes)-1));
				fechaInicioMes.set(Calendar.YEAR, Integer.parseInt(anio));
				fechaInicioMes.set(Calendar.DAY_OF_MONTH, 1); fechaInicioMes.getTime();
				fechaFinMes.set(Calendar.MONTH, (Integer.parseInt(mes)-1));
				fechaFinMes.set(Calendar.YEAR, Integer.parseInt(anio));
				fechaFinMes.set(Calendar.DAY_OF_MONTH,Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)); fechaFinMes.getTime();
				fechaInicioAnio.set(Calendar.MONTH, 0);
				fechaInicioAnio.set(Calendar.DAY_OF_MONTH, 1);
				fechaUltimoAnioInicio.set(Calendar.YEAR, (Integer.parseInt(anio)-1));
				//Prima Neta Pagada
				ValorCatalogoAgentes valor = valorCatalogoAgentesService.obtenerElementoEspecifico("Produccion Sobre ...","Prima Neta Emitida");
				DatosEdoctaAgenteSiniestralidad siniestralidad = new DatosEdoctaAgenteSiniestralidad();
				 siniestralidad = impresionesService.llenarDatosEdocataAgenteSiniestralidad(idAgente,anio,mes,valor.getId(),"siniestralidad", fechaInicioAnio.getTime(),fechaFinAnio.getTime(),fechaUltimoAnioInicio.getTime(),fechaUltimoAnioFin.getTime());
				if (siniestralidad != null) {
					params.put("siniestralidad", siniestralidad);
				}
			
			} catch (JRException e1) {
				LOG.error(e1.getMessage(), e1);
				return null;
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
			transporteImpresionDTO = new TransporteImpresionDTO();
			try {
				//se agrega parametros para mostrar u ocultar la seccion INFORMACION DE PRODUCTIVIDAD
				if(tieneRole){
					params.put("MOSTRAR_SECCION_PRODUCCION", true);
					params.put("MOSTRAR_SECCION_SINIESTRALIDAD", true);
				}else{
					params.put("MOSTRAR_SECCION_PRODUCCION", mostrarSeccionReporteEdocta("SECCION PRODUCCION"));
					params.put("MOSTRAR_SECCION_SINIESTRALIDAD", mostrarSeccionReporteEdocta("SECCION SINIESTRALIDAD"));
				}
				transporteImpresionDTO.setByteArray(imprimirReporteAgenteGeneric(acumuladoViews, new Locale("es", "MX"), "edoctaAfirme.jrxml", params));
			} catch (JRException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return transporteImpresionDTO;
	}
	
	public TransporteImpresionDTO imprimirReporteDetalleAlertasLineaSAP(String consultaNuemeroSerie, String usuario, String password) {
		
		TransporteImpresionDTO transporteImpresionDTO = null;
		Map<String,Object> params = new HashMap<String, Object>(1);
		
		List<RespuestaDetalleAlerta> encabezadoAlertas = new ArrayList<RespuestaDetalleAlerta>(1);
		RespuestaDetalleAlerta respuestaLineaImprimir = new RespuestaDetalleAlerta();
		List<RespuestaAlerta> arregloEncabezadoReporte = new ArrayList<RespuestaAlerta>(1);
		try{
			
			if(consultaNuemeroSerie!= null){
				AlertaPortProxy proxiAlerta = new AlertaPortProxy();
				respuestaLineaImprimir = proxiAlerta.detalleAlerta(usuario,
						password, 
						 consultaNuemeroSerie);
				
				encabezadoAlertas.add(respuestaLineaImprimir);
				
				 arregloEncabezadoReporte = proxiAlerta.consultaAlerta(usuario,
						password, 
						 consultaNuemeroSerie);
			}
			
		}catch(Exception ex){
			LOG.error(ex.getMessage(), ex);
		}
			
		params.put("consultaAlerta",arregloEncabezadoReporte);
		
		params.put("cesvi_anio",respuestaLineaImprimir.getCesvi().getAnio());
		params.put("cesvi_complemento",respuestaLineaImprimir.getCesvi().getComplemento());
		params.put("cesvi_estatus",respuestaLineaImprimir.getCesvi().getEstatus());
		params.put("cesvi_linea",respuestaLineaImprimir.getCesvi().getLinea());
		params.put("cesvi_marca",respuestaLineaImprimir.getCesvi().getMarca());
		params.put("cesvi_mensaje",respuestaLineaImprimir.getCesvi().getMensaje());
		params.put("cesvi_mensajeEstatus",respuestaLineaImprimir.getCesvi().getMensajeEstatus());
		params.put("cesvi_motor",respuestaLineaImprimir.getCesvi().getMotor());
		params.put("cesvi_submarca",respuestaLineaImprimir.getCesvi().getSubmarca());
		params.put("cesvi_tipoVehiculo",respuestaLineaImprimir.getCesvi().getTipoVehiculo());
		params.put("cesvi_vin",respuestaLineaImprimir.getCesvi().getVin());
		
		JasperReport sapAmisPlantillaConsultaJasper = getJasperReport(
				UR_JRXML+"sapAmisConsultaAlertas.jrxml");
		params.put("sapAmisPlantillaConsulta",sapAmisPlantillaConsultaJasper);
		
		JasperReport sapAmisCesviJasper = getJasperReport(
				UR_JRXML+"sapAmisCesviLinea_detalle.jrxml");
		params.put("sapAmisCesviLineaJasper",sapAmisCesviJasper);
		
		JasperReport sapAmisCiiJasper = getJasperReport(
				UR_JRXML+"sapAmisCiiLinea_detalle.jrxml");
		params.put("sapAmisCiiLineaJasper",sapAmisCiiJasper);
		
		JasperReport sapAmisEmisionJasper = getJasperReport(
				UR_JRXML+"sapAmisEmisionLinea_detalle.jrxml");
		params.put("sapAmisEmisionLineaJasper",sapAmisEmisionJasper);
		
		JasperReport sapAmisOcraJasper = getJasperReport(
				UR_JRXML+"sapAmisOcraLinea_detalle.jrxml");
		params.put("sapAmisOcraLineaJasper",sapAmisOcraJasper);
		
		JasperReport sapAmisPrevencionJasper = getJasperReport(
				UR_JRXML+"sapAmisPrevencionLinea_detalle.jrxml");
		params.put("sapAmisPrevencionLineaJasper",sapAmisPrevencionJasper);
		
		JasperReport sapAmisPtsJasper = getJasperReport(
				UR_JRXML+"sapAmisPtsLinea_detalle.jrxml");
		params.put("sapAmisPtsLineaJasper",sapAmisPtsJasper);
		
		JasperReport sapAmisScdJasper = getJasperReport(
				UR_JRXML+"sapAmisScdLinea_detalle.jrxml");
		params.put("sapAmisScdLineaJasper",sapAmisScdJasper);
		
		JasperReport sapAmisSiniestroJasper = getJasperReport(
				UR_JRXML+"sapAmisSiniestroLinea_detalle.jrxml");
		params.put("sapAmisSiniestroLineaJasper",sapAmisSiniestroJasper);
		
		JasperReport sapAmisSipacJasper = getJasperReport(
				UR_JRXML+"sapAmisSipacLinea_detalle.jrxml");
		params.put("sapAmisSipacLineaJasper",sapAmisSipacJasper);
		
		JasperReport sapAmisValuacionJasper = getJasperReport(
				UR_JRXML+"sapAmisValuacionLinea_detalle.jrxml");
		params.put("sapAmisValuacionLineaJasper",sapAmisValuacionJasper);
		
		transporteImpresionDTO = new TransporteImpresionDTO();
		try {
			// Modificar
			transporteImpresionDTO
					.setByteArray(imprimirReporteDetAlertLineaSap(   
							encabezadoAlertas, new Locale("es", "MX"),
							"detAlertLineaSap.jrxml",params));
		} catch (JRException e) {
			LOG.error(e.getMessage(), e);
		}
		
		return transporteImpresionDTO;
	}

	public TransporteImpresionDTO imprimirReporteDetalleAlertasSAP(String idEncabezadoAlertas) {
		
		TransporteImpresionDTO transporteImpresionDTO = null;
		Map<String,Object> params = new HashMap<String, Object>(1);
		
		List<SapAlertasistemasEnvio> encabezadoAlertas = new ArrayList<SapAlertasistemasEnvio>(1);
		encabezadoAlertas = impresionesService.obtenerDetalleAlertsBitacora(idEncabezadoAlertas);
	
		/*Bloque donde se define agrega el Jasper para el Subreporte de Alertas Cesvi*/
		JasperReport sapAmisCesviJasper = getJasperReport(
				UR_JRXML+"sapAmisCesvi_detalle.jrxml");
		params.put("sapAmisCesviJasper",sapAmisCesviJasper);
		
		/*Bloque donde se define agrega el Jasper para el Subreporte de Alertas Cesvi*/
		JasperReport sapAmisCiiJasper = getJasperReport(
				UR_JRXML+"sapAmisCii_detalle.jrxml");
		params.put("sapAmisCiiJasper",sapAmisCiiJasper);
		
		/*Bloque donde se define agrega el Jasper para el Subreporte de Alertas Emision*/
		JasperReport sapAmisEmisionJasper = getJasperReport(
				UR_JRXML+"sapAmisEmision_detalle.jrxml");
		params.put("sapAmisEmisionJasper",sapAmisEmisionJasper);
		
		/*Bloque donde se define agrega el Jasper para el Subreporte de Alertas Ocra*/
		JasperReport sapAmisOcraJasper = getJasperReport(
				UR_JRXML+"sapAmisOcra_detalle.jrxml");
		params.put("sapAmisOcraJasper",sapAmisOcraJasper);
		
		/*Bloque donde se define agrega el Jasper para el Subreporte de Alertas Prevencion*/
		JasperReport sapAmisPrevencionJasper = getJasperReport(
				UR_JRXML+"sapAmisPrevencion_detalle.jrxml");
		params.put("sapAmisPrevencionJasper",sapAmisPrevencionJasper);
		
		/*Bloque donde se define agrega el Jasper para el Subreporte de Alertas Pts*/
		JasperReport sapAmisPtsJasper = getJasperReport(
				UR_JRXML+"sapAmisPts_detalle.jrxml");
		params.put("sapAmisPtsJasper",sapAmisPtsJasper);
		
		/*Bloque donde se define agrega el Jasper para el Subreporte de Alertas Scd*/
		JasperReport sapAmisScdJasper = getJasperReport(
				UR_JRXML+"sapAmisScd_detalle.jrxml");
		params.put("sapAmisScdJasper",sapAmisScdJasper);
		
		/*Bloque donde se define agrega el Jasper para el Subreporte de Alertas Siniestro*/
		JasperReport sapAmisSiniestroJasper = getJasperReport(
				UR_JRXML+"sapAmisSiniestro_detalle.jrxml");
		params.put("sapAmisSiniestroJasper",sapAmisSiniestroJasper);
		
		/*Bloque donde se define agrega el Jasper para el Subreporte de Alertas Sipac*/
		JasperReport sapAmisSipacJasper = getJasperReport(
				UR_JRXML+"sapAmisSipac_detalle.jrxml");
		params.put("sapAmisSipacJasper",sapAmisSipacJasper);
		
		/*Bloque donde se define agrega el Jasper para el Subreporte de Alertas Valuacion*/
		JasperReport sapAmisValuacionJasper = getJasperReport(
				UR_JRXML+"sapAmisValuacion_detalle.jrxml");
		params.put("sapAmisValuacionJasper",sapAmisValuacionJasper);		 
		
		transporteImpresionDTO = new TransporteImpresionDTO();
		try {  
			// Modificar
			transporteImpresionDTO
					.setByteArray(imprimirReporteDetAlertBitacoraSap(
							encabezadoAlertas, new Locale("es", "MX"),
							"detAlertBitacoraSap.jrxml",params));
		} catch (JRException e) {
			LOG.error(e.getMessage(), e);
		}
		
		return transporteImpresionDTO;
	}
	
	public TransporteImpresionDTO imprimirBitacoraEmisionSAP(String bitacoraPoliza ,String bitacoraVin ,String bitacoraFechaEnvio,
		String estatusEnvio,String cesvi,String cii,String emision,String ocra,String prevencion,String pt,String csd,
		String siniestro, String sipac , String valuacion) {
		
		TransporteImpresionDTO transporteImpresionDTO = null;
		
		List<SapAmisBitacoraEmision> bitacoraEmision =  new ArrayList<SapAmisBitacoraEmision>(1); 
		
		bitacoraEmision = impresionesService.obtenerBitacoraEmision(bitacoraPoliza , bitacoraVin , bitacoraFechaEnvio,
				 estatusEnvio, cesvi, cii, emision, ocra, prevencion, pt, csd,
				 siniestro,  sipac ,  valuacion);		
		try {
			if(!bitacoraEmision.isEmpty()){
				transporteImpresionDTO = new TransporteImpresionDTO();
				transporteImpresionDTO = impresionesService.getExcel(bitacoraEmision, 
						"midas.impresion.sapamis.bitacoras.emision.reportebitacora.archivo.nombre",ConstantesReporte.TIPO_XLS);		
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		return transporteImpresionDTO;
	}
	
	
	public TransporteImpresionDTO imprimirBitacoraSiniestrosSAP(String bitacoraPoliza ,String bitacoraVin ,String bitacoraFechaEnvio,
			String estatusEnvio,String cesvi,String cii,String emision,String ocra,String prevencion,String pt,String csd,
			String siniestro, String sipac , String valuacion) {
			
			TransporteImpresionDTO transporteImpresionDTO = null;
			
			List<SapAmisBitacoraSiniestros> bitacoraEmision =  new ArrayList<SapAmisBitacoraSiniestros>(1); 
			
			bitacoraEmision = impresionesService.obtenerBitacoraSiniestro(bitacoraPoliza , bitacoraVin , bitacoraFechaEnvio,
					 estatusEnvio, cesvi, cii, emision, ocra, prevencion, pt, csd,
					 siniestro,  sipac ,  valuacion);
			
			try {
				if(!bitacoraEmision.isEmpty()){
					transporteImpresionDTO = new TransporteImpresionDTO();
					transporteImpresionDTO = impresionesService.getExcel(bitacoraEmision, 
							"midas.impresion.sapamis.bitacoras.siniestro.reportebitacora.archivo.nombre",ConstantesReporte.TIPO_XLS);
				}

			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
			
			return transporteImpresionDTO;
		}
	
//	private byte[] imprimirReporteProvision(List<ReporteProvisionesView> datosAgentes,
//			Locale locale,int reporteAimprimir) throws JRException {
//
//		Map<String, Object> parameters = new HashMap<String, Object>();
//		ByteArrayOutputStream pdfReporte =  new ByteArrayOutputStream();
//		List<byte[]> inputByteArray = new ArrayList<byte[]>();
//		InputStream reporteCargosAgente = null;
//		reporteCargosAgente = this
//					.getClass()
//					.getResourceAsStream(
//							UR_JRXML+"reporteProvisionToPDF.jrxml");
//		
//		JasperReport reporte = JasperCompileManager
//				.compileReport(reporteCargosAgente);
//		if (locale != null) {
//			parameters.put(JRParameter.REPORT_LOCALE, locale);
//		}
//		parameters.put(IMGHEADER, SistemaPersistencia.LOGO_AFIRME);
//		parameters.put("reporte", reporte);
//		JRGzipVirtualizer gzipVirtualizer = new JRGzipVirtualizer(MAX_GZIPVIRTUALIZER);
//		parameters.put(JRParameter.REPORT_VIRTUALIZER, gzipVirtualizer);
//		JRBeanCollectionDataSource dsAgente = new JRBeanCollectionDataSource(
//				datosAgentes);
//		inputByteArray.add(JasperRunManager.runReportToPdf(reporte,
//				parameters, dsAgente));
//		pdfConcantenate(inputByteArray, pdfReporte);
//		return pdfReporte.toByteArray();
//	}

	@Override
	public TransporteImpresionDTO imprimirReporteAgenteIngresos(String anio,
			String mes, Long rangoInicio, Long rangoFin,
			Integer tipoReporte, Locale locale) {
		List<DatosAgenteIngresosView> datosAgentes = impresionesService
				.llenarDatosAgenteIngresos(anio, mes, rangoInicio, rangoFin, tipoReporte);
		TransporteImpresionDTO transporteImpresionDTO = null;
		if (datosAgentes != null && !datosAgentes.isEmpty()) {
			transporteImpresionDTO = new TransporteImpresionDTO();
			try {
				transporteImpresionDTO
						.setByteArray(imprimirReporteAgenteGeneric(
								datosAgentes, locale,
								"reporteAgenteIngresosToExcel.jrxml",null));
			} catch (JRException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		if (transporteImpresionDTO != null && transporteImpresionDTO.getByteArray() != null) {
			return transporteImpresionDTO;
		}
		return null;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteAgenteIngresosToExcel(String anio, String mes, Long rangoInicio, Long rangoFin,Integer tipoReporte, Locale locale, String formatoArchivo) throws MidasException{
		try{
			List<DatosAgenteIngresosView> listaDatosAgente = impresionesService.llenarDatosAgenteIngresos(anio, mes, rangoInicio, rangoFin,tipoReporte);
			if(isNull(listaDatosAgente) || isEmptyList(listaDatosAgente)){
				onError("No se encontraron datos");
			}
			TransporteImpresionDTO reporte = new TransporteImpresionDTO();
			if(TipoSalidaReportes.TO_EXCEL.getValue().equals(formatoArchivo)){
				 reporte = impresionesService.getExcel(listaDatosAgente,DatosAgenteIngresosView.PLANTILLA_NAME,ConstantesReporte.TIPO_XLS);
			}else if(TipoSalidaReportes.TO_XLSX.getValue().equals(formatoArchivo)){
				 reporte = impresionesService.getExcel(listaDatosAgente,DatosAgenteIngresosView.PLANTILLA_NAME,ConstantesReporte.TIPO_XLSX);
			}else{
				 reporte = impresionesService.getExcel(listaDatosAgente,DatosAgenteIngresosView.PLANTILLA_NAME,ConstantesReporte.TIPO_CSV);
			}
			if (reporte != null && reporte.getByteArray() != null) {
				return reporte;
			}
		}catch(Exception e){
			onError("Ha ocurrido un error inesperado al generar el reporte, causado por:"+e.getMessage());
		}
		return null;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteAgentePrimaPagadaPorRamoToExcel(
			List<Agente> agenteList, Date fechaInicial, Date fechaCorteFin,
		    List<CentroOperacion> centroOperacionesSeleccionados, List<Ejecutivo> ejecutivosSeleccionados,
			List<Gerencia> gerenciasSeleccionadas,List<Promotoria> promotoriasSeleccionadas,
			Integer tipoReporte, Locale locale, String formatoArchivo ) throws MidasException{
		try{	
			List<DatosAgentePrimaPagadaPorRamoView> listaDatosAgente = new ArrayList<DatosAgentePrimaPagadaPorRamoView>();
			listaDatosAgente = impresionesService
					.llenarDatosAgentePrimaPagadaPorRamo(agenteList,  fechaInicial,
							 fechaCorteFin, centroOperacionesSeleccionados,
							 ejecutivosSeleccionados,  gerenciasSeleccionadas,
							 promotoriasSeleccionadas,  tipoReporte);
			if(isNull(listaDatosAgente)|| isEmptyList(listaDatosAgente)){
				onError("No se encontraron datos");
			}
			TransporteImpresionDTO reporte = new TransporteImpresionDTO();  
			
			if(TipoSalidaReportes.TO_EXCEL.getValue().equals(formatoArchivo)){
				reporte = impresionesService
							.getExcel(listaDatosAgente,DatosAgentePrimaPagadaPorRamoView.PLANTILLA_NAME,ConstantesReporte.TIPO_XLS);
			}else if(TipoSalidaReportes.TO_XLSX.getValue().equals(formatoArchivo)){
				reporte = impresionesService
				.getExcel(listaDatosAgente, DatosAgentePrimaPagadaPorRamoView.PLANTILLA_NAME,ConstantesReporte.TIPO_XLSX);
			}else{
				reporte = impresionesService
							.getExcel(listaDatosAgente, DatosAgentePrimaPagadaPorRamoView.PLANTILLA_NAME,ConstantesReporte.TIPO_CSV);
			}
			if (reporte != null && reporte.getByteArray() != null) {
				return reporte;
			}
		}catch(Exception e){
			onError("Ha ocurrido un error inesperado al generar el reporte, causado por:"+e.getMessage());
		}
		return null;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteAgenteTopAgenteToExcel (
			List<Agente> listaAgentes,
			List<CentroOperacion> centroOperacionesSeleccionados, List<Ejecutivo> ejecutivosSeleccionados,
			List<Gerencia> gerenciasSeleccionadas,List<Promotoria> promotoriasSeleccionadas,
			String anio,String mes,Integer tipoReporte,Integer topAgente, Locale locale, String formatoArchivo,Date fechaInicio, Date fechaFin ) {
		List<DatosAgenteTopAgenteView> listaDatosAgente = impresionesService
				.llenarDatosAgenteTopAgente(listaAgentes,centroOperacionesSeleccionados,
						ejecutivosSeleccionados,gerenciasSeleccionadas,promotoriasSeleccionadas,
						anio, mes, tipoReporte,topAgente,fechaInicio,fechaFin);
		Map<String,Object> params = new HashMap<String, Object>();
		String[] monthsArray = new DateFormatSymbols(new Locale("es", "MX")).getMonths();
		params.put("anio",Integer.parseInt(anio)); 
		params.put("mes",monthsArray[Integer.parseInt(mes)-1]);
		if(listaDatosAgente!=null && !listaDatosAgente.isEmpty()){
			params.put("totalCiaMesAct",listaDatosAgente.get(0).getTotalCiaMesAct());
			params.put("totalCiaMesAnt",listaDatosAgente.get(0).getTotalCiaMesAnt());
			params.put("totalCiaAcumAnioAct",listaDatosAgente.get(0).getTotalCiaAcumAnioAct());
			params.put("totalCiaAcumAnioAnt",listaDatosAgente.get(0).getTotalCiaAcumAnioAnt());
			params.put("totalAcumAgentes",listaDatosAgente.get(0).getTotalAcumAgentes());
			params.put("totalAcumProm",listaDatosAgente.get(0).getTotalAcumProm());
		}
		try {
			if(TipoSalidaReportes.TO_EXCEL.getValue().equals(formatoArchivo)){
				return impresionesService.getExcel(listaDatosAgente, DatosAgenteTopAgenteView.PLANTILLA_NAME,params,ConstantesReporte.TIPO_XLS);
			}else if(TipoSalidaReportes.TO_XLSX.getValue().equals(formatoArchivo)){
				return impresionesService.getExcel(listaDatosAgente, DatosAgenteTopAgenteView.PLANTILLA_NAME,params,ConstantesReporte.TIPO_XLSX);
			}else{
				return impresionesService.getExcel(listaDatosAgente, DatosAgenteTopAgenteView.PLANTILLA_NAME,params,ConstantesReporte.TIPO_CSV);
			}
		} catch(RuntimeException e) {
			LOG.error(e.getMessage(), e);
		} 
		 return null;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteAgentePrimaPagadaPorRamo(
			List<AgenteView> listaAgentes, Date fechaCorteInicio,
			Date fechaCorteFin, Integer tipoReporte, Locale locale) {
		List<DatosAgentePrimaPagadaPorRamoView> datosAgentes = impresionesService
				.llenarDatosAgentePrimaPagadaPorRamo(null,
						fechaCorteInicio, fechaCorteFin ,null,null,null,null, tipoReporte);
		TransporteImpresionDTO transporteImpresionDTO = null;
		if (datosAgentes != null && !datosAgentes.isEmpty()) {
			transporteImpresionDTO = new TransporteImpresionDTO();
			try {
				transporteImpresionDTO
						.setByteArray(imprimirReporteAgenteGeneric(
								datosAgentes, locale,
								"reporteAgentePrimaPagadaPorRamoToExcel.jrxml",null));
			} catch (JRException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return transporteImpresionDTO;
	}
	
	private <C> byte[] imprimirReporteDetAlertLineaSap(
			List<C> datosAlertas, Locale locale,
			String nameReport,Map<String,Object> parametersGeneric) throws JRException{
		
		Map<String, Object> parameters = new HashMap<String, Object>(1);
		ByteArrayOutputStream pdfReporteAlertas =  new ByteArrayOutputStream();
		List<byte[]> inputByteArray = new ArrayList<byte[]>(1);
		
		JasperReport reporteAlertasInfo = getJasperReport(
				UR_JRXML + nameReport);
		
		if (locale != null) {
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		if (parametersGeneric != null && !parametersGeneric.isEmpty()) {
			for (Entry<String, Object> param : parametersGeneric.entrySet()) {
				parameters.put(param.getKey(), parametersGeneric.get(param.getKey()));
			}
		}
		parameters.put(IMGHEADER, SistemaPersistencia.LOGO_AFIRME);
		
		/*Esta pendiente de verificar donde se utiliza*/
		//parameters.put("reporteAlertasGeneral", reporteAlertasInfo);
		
		JRGzipVirtualizer gzipVirtualizer = new JRGzipVirtualizer(MAX_GZIPVIRTUALIZER);
		parameters.put(JRParameter.REPORT_VIRTUALIZER, gzipVirtualizer);
		
		JRBeanCollectionDataSource dsAlertas = new JRBeanCollectionDataSource(
				datosAlertas);
		
		inputByteArray.add(JasperRunManager.runReportToPdf(reporteAlertasInfo,
				parameters, dsAlertas));
		pdfConcantenate(inputByteArray, pdfReporteAlertas);
		
		return pdfReporteAlertas.toByteArray();
	}
	
	private <C> byte[] imprimirReporteDetAlertBitacoraSap(
			List<C> datosAlertas, Locale locale,
			String nameReport,Map<String,Object> parametersGeneric) throws JRException{
		
		Map<String, Object> parameters = new HashMap<String, Object>(1);
		ByteArrayOutputStream pdfReporteAlertas =  new ByteArrayOutputStream();
		List<byte[]> inputByteArray = new ArrayList<byte[]>(1);
				
		JasperReport reporteAlertasInfo = getJasperReport(
				UR_JRXML + nameReport);
		
		if (locale != null) {
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		if (parametersGeneric != null && !parametersGeneric.isEmpty()) {
			for (Entry<String, Object> param : parametersGeneric.entrySet()) {
				parameters.put(param.getKey(), parametersGeneric.get(param.getKey()));
			}
		}
		parameters.put(IMGHEADER, SistemaPersistencia.LOGO_AFIRME);
		
		/*Esta pendiente de verificar donde se utiliza*/
		//parameters.put("reporteAlertasGeneral", reporteAlertasInfo);
		
		JRGzipVirtualizer gzipVirtualizer = new JRGzipVirtualizer(MAX_GZIPVIRTUALIZER);
		parameters.put(JRParameter.REPORT_VIRTUALIZER, gzipVirtualizer);
		
		JRBeanCollectionDataSource dsAlertas = new JRBeanCollectionDataSource(
				datosAlertas);
		
		inputByteArray.add(JasperRunManager.runReportToPdf(reporteAlertasInfo,
				parameters, dsAlertas));
		pdfConcantenate(inputByteArray, pdfReporteAlertas);
		
		return pdfReporteAlertas.toByteArray();
	}
	

	private <C> byte[] imprimirReporteAgenteGeneric(
			List<C> datosAgentes, Locale locale,
			String nameReport,Map<String,Object> parametersGeneric) throws JRException {
		Map<String, Object> parameters = new HashMap<String, Object>(1);
		ByteArrayOutputStream pdfReporteAgente =  new ByteArrayOutputStream();
		List<byte[]> inputByteArray = new ArrayList<byte[]>(1);
		JasperReport reporteAgenteInfo = getJasperReport(UR_JRXML + nameReport);
		if (locale != null) {
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		if (parametersGeneric != null && !parametersGeneric.isEmpty()) {
			for (Entry<String, Object> param : parametersGeneric.entrySet()) {
				parameters.put(param.getKey(), parametersGeneric.get(param.getKey()));
			}
		}
		
		if (nameReport.equals("edoctaAfirme.jrxml")){
			parameters.put(IMGHEADER, SistemaPersistencia.LOGO_ESTADODECUENTA);
			LOG.info("1337: Se cargo logo estado de cuenta");
		}else{
			parameters.put(IMGHEADER, SistemaPersistencia.LOGO_AFIRME);
			LOG.info("1337: Se cargo logo afirme");
		}
		
		parameters.put("reporteAgenteInfoGeneral", reporteAgenteInfo);
		JRGzipVirtualizer gzipVirtualizer = new JRGzipVirtualizer(MAX_GZIPVIRTUALIZER);
		parameters.put(JRParameter.REPORT_VIRTUALIZER, gzipVirtualizer);
		JRBeanCollectionDataSource dsAgente = new JRBeanCollectionDataSource(
				datosAgentes);
		inputByteArray.add(JasperRunManager.runReportToPdf(reporteAgenteInfo,
				parameters, dsAgente));
		pdfConcantenate(inputByteArray, pdfReporteAgente);
		return pdfReporteAgente.toByteArray();
	}
	@Override
	public TransporteImpresionDTO imprimirReporteAgenteTopAgente(
			List<AgenteView> listaAgentes, String anio, String mes,
			Integer tipoReporte,Integer topAgente, Locale locale) {
		List<DatosAgenteTopAgenteView> datosAgentes = impresionesService
				.llenarDatosAgenteTopAgente(null,null,null,null,null, anio, mes,
						tipoReporte, topAgente,null,null);
		TransporteImpresionDTO transporteImpresionDTO = null;
		if (datosAgentes != null && !datosAgentes.isEmpty()) {
			transporteImpresionDTO = new TransporteImpresionDTO();
			try {
				transporteImpresionDTO
						.setByteArray(imprimirReporteAgenteGeneric(
								datosAgentes, locale,
								"reporteAgenteTopAgenteToExcel.jrxml",null));
			} catch (JRException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		if (transporteImpresionDTO != null && transporteImpresionDTO.getByteArray() != null) {
			return transporteImpresionDTO;
		}
		return null;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteAgenteDatosAgenteToExcel(
			Date fechaInicial, Date fechaFinal, Long idCentroOperacion,
			Long idGerencia, Long idEjecutivo, Long idPromotoria,
			Long rangoInicio, Long rangoFin, Locale locale, String formatoArchivo) {
		List<DatosAgenteDatosView> datosAgentes = impresionesService
				.llenarDatosAgenteDatosAgente(fechaInicial, fechaFinal,
						idCentroOperacion, idGerencia, idEjecutivo,
						idPromotoria, rangoInicio, rangoFin);
		if (datosAgentes != null) {
			if(formatoArchivo.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
				return impresionesService
					.getExcel(datosAgentes, DatosAgenteDatosView.PLANTILLA_NAME,
						ConstantesReporte.TIPO_XLS);
			}else if(formatoArchivo.equals(TipoSalidaReportes.TO_XLSX.getValue())){
				return impresionesService
					.getExcel(datosAgentes,DatosAgenteDatosView.PLANTILLA_NAME,
					ConstantesReporte.TIPO_XLSX);
			}else{
				return impresionesService
					.getExcel(datosAgentes,DatosAgenteDatosView.PLANTILLA_NAME,
						ConstantesReporte.TIPO_CSV);
			}
		}
		return null;
	}
	
	@Override
	public TransporteImpresionDTO imprimirReporteDetPrimasRetencionImpuestosExcel(RetencionImpuestosReporteView filtro, String formatoArchivo) {
		final TransporteImpresionDTO result = new TransporteImpresionDTO();
		final ExcelExporter excelExporter = new ExcelExporter(RetencionImpuestosReporteView.class, new SXSSFWorkbook());
		
		try{
			if(!generarPlantillaReporteService.getValorParameter(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_REPORTE_AGENTES, ParametroGeneralDTO.CODIGO_PARAM_GENERAL_REPORTE_AGENTE_DETALLE_PRIMAS).equals(ESTATUS_REPORTE_EN_PROCESO)) {
				generarPlantillaReporteService.actualizarparametroReporte(ESTATUS_REPORTE_EN_PROCESO);
				
				List<RetencionImpuestosReporteView> listaPrimasDetalleRetencionImp = impresionesService.llenarDatosDetallePrimaRetencionImpuestos(filtro);
				
				if (listaPrimasDetalleRetencionImp == null || listaPrimasDetalleRetencionImp.isEmpty()) {
					throw new RuntimeException("No se obtuvieron agentes con los criterios de busqueda seleccionados.");
				}
				
				if(formatoArchivo.equals(TipoSalidaReportes.TO_XLSX.getValue())){
					result.setGenericInputStream(excelExporter.export(listaPrimasDetalleRetencionImp));
				} else {
					result.setGenericInputStream(excelExporter.export(listaPrimasDetalleRetencionImp));
				}
				
				result.setContentType(CONTENT_TYPE_XLSX);
				result.setFileName(REPORT_NAME_DETALLEPRIMAS_RETENCIONIMPUESTOS + TipoSalidaReportes.TO_XLSX.getValue());
			} else {
				throw new RuntimeException("El reporte ya esta siendo generado.");
			}
		} finally {
			generarPlantillaReporteService.actualizarparametroReporte(ESTATUS_REPORTE_COMPLETADO);
		}
		return result;
	}
	
	@Override
	public TransporteImpresionDTO imprimirReporteDetPrimasExcel(CentroOperacion centroOperacion, Gerencia gerencia, Ejecutivo ejecutivo,Promotoria promotoria, ValorCatalogoAgentes tipoAgentes, String fecha1, String fecha2, Agente agente, String formatoArchivo) {		
		final TransporteImpresionDTO result = new TransporteImpresionDTO();
		final ExcelExporter excelExporter = new ExcelExporter(DatosDetPrimas.class, new SXSSFWorkbook());

		try{
			if(!generarPlantillaReporteService.getValorParameter(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_REPORTE_AGENTES, ParametroGeneralDTO.CODIGO_PARAM_GENERAL_REPORTE_AGENTE_DETALLE_PRIMAS).equals(ESTATUS_REPORTE_EN_PROCESO)) {
				generarPlantillaReporteService.actualizarparametroReporte(ESTATUS_REPORTE_EN_PROCESO);

				LOG.debug("--imprimirReporteDetPrimasExcel() Obteniendo lista");
				final List<DatosDetPrimas> listaPreviewBonos =  impresionesService.llenarDatosDetPrimas( centroOperacion,  gerencia,  ejecutivo,promotoria,  tipoAgentes,  fecha1,  fecha2,  agente);

				if (listaPreviewBonos == null || listaPreviewBonos.isEmpty()) {
					throw new RuntimeException("No se obtuvieron agentes con los criterios de busqueda seleccionados.");
				}

				LOG.debug("--imprimirReporteDetPrimasExcel() Exportando documento");
				if(formatoArchivo.equals(TipoSalidaReportes.TO_XLSX.getValue())){
					result.setByteArray(IOUtils.toByteArray(excelExporter.export(listaPreviewBonos)));
				}else{
					result.setByteArray(IOUtils.toByteArray(excelExporter.exportCSV(listaPreviewBonos)));
				}
				
				result.setContentType(CONTENT_TYPE_XLSX);
				result.setFileName(REPORT_NAME_DETALLEPRIMAS + TipoSalidaReportes.TO_XLSX.getValue());
			
				this.enviarNotificacionGenRepDetPrimas();
			} else {
				throw new RuntimeException("El reporte ya esta siendo generado.");
			}
		} catch (IOException IOE) {
			String msg = "Ha ocurrido un error al intentar generar el Reporte Detalle de Primas de Impuestos Estatales, favor de intentar m&aacute;s tarde";
			LOG.error(msg, IOE);
			throw new RuntimeException(msg);
		} finally {
			generarPlantillaReporteService.actualizarparametroReporte(ESTATUS_REPORTE_COMPLETADO);
		}
		return result;
	}

	@Override
	public TransporteImpresionDTO imprimirReportePreviewBonosExcel(Long idCentroOperacion, Long idGerencia, Long idEjecutivo,
			Long idPromotoria, ValorCatalogoAgentes tipoAgentes, String mes, String anio, Agente agente, ConfigBonos bono, Long idCalculo, String formatoArchivo) {

		List<DatosReportePreviewBonos> listaDatosPreviewBonos = impresionesService
		.llenarDatosPreviewBonos( idCentroOperacion,  idGerencia,  idEjecutivo,
				 idPromotoria,  tipoAgentes,  anio,  mes,  agente,  bono, idCalculo);
		if(listaDatosPreviewBonos!=null){
			if(formatoArchivo.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
				return impresionesService.getExcel(listaDatosPreviewBonos, DatosReportePreviewBonos.PLANTILLA_NAME,
						ConstantesReporte.TIPO_XLS);
			}else if(formatoArchivo.equals(TipoSalidaReportes.TO_XLSX.getValue())){
				return impresionesService.getExcel(listaDatosPreviewBonos, DatosReportePreviewBonos.PLANTILLA_NAME,
						ConstantesReporte.TIPO_XLSX);
			}else{
				return impresionesService.getExcel(listaDatosPreviewBonos, DatosReportePreviewBonos.PLANTILLA_NAME,
						ConstantesReporte.TIPO_CSV);
			}
		}
		return null;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteAgenteBonosGerenciaToExcel(
			String anio, String mes, String mesFin, Long idCentroOperacion, Long idGerencia,
			Long idEjecutivo, Long idPromotoria, Long idAgente,Long clasificacionAgente, Locale locale, String formatoArchivo) {
		List<DatosAgenteBonosGerenciaView> datosagentes = impresionesService
				.llenarDatosAgenteBonosGerencia(anio, mes, mesFin, idCentroOperacion,
						idGerencia, idEjecutivo, idPromotoria, idAgente, clasificacionAgente);
		if (datosagentes != null) {
			if(formatoArchivo.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
				return impresionesService.
					getExcel(datosagentes, DatosAgenteBonosGerenciaView.PLANTILLA_NAME,
							ConstantesReporte.TIPO_XLS);
			}else if(formatoArchivo.equals(TipoSalidaReportes.TO_XLSX.getValue())){
				return impresionesService.
				getExcel(datosagentes,DatosAgenteBonosGerenciaView.PLANTILLA_NAME,
						ConstantesReporte.TIPO_XLSX);
			}else{
				return impresionesService.
					getExcel(datosagentes, DatosAgenteBonosGerenciaView.PLANTILLA_NAME,
							ConstantesReporte.TIPO_CSV);
			}
		}
		return null;
	}
	
	//TransporteImpresionDTO
	@Override 
	public InputStream imprimirReporteAgenteContabilidadMMToExcel(
			String anioInicio, String mesInicio, String anioFin,
			String mesFin, Long idCentroOperacion, Long idGerencia,
			Long idEjecutivo, Long idPromotoria, Long idAgente, Locale locale) {
		try {
		InputStream file = null;
		List<DatosAgenteContabilidadMMView> datosagentes = impresionesService
				.llenarDatosAgenteContabilidadMM(anioInicio, mesInicio,
						anioFin, mesFin, idCentroOperacion, idGerencia,
						idEjecutivo, idPromotoria, idAgente);
		if (datosagentes != null && !datosagentes.isEmpty()) {
//			List<Map<String,Object>> params = new LinkedList<Map<String,Object>>();
//			List<DatosAgenteBonosGerenciaView> datosagentes1 = impresionesService
//			.llenarDatosAgenteBonosGerencia(anioFin, idCentroOperacion,
//					idGerencia, idEjecutivo, idPromotoria, idAgente);
//			JRBeanCollectionDataSource c1 = new JRBeanCollectionDataSource(
//					datosagentes1);
//			Map<String,Object> dataSource2 = new HashMap<String, Object>();
//			dataSource2.put("dataSource", c1);
//			params.add(dataSource2);
//			return impresionesService.getExcel(datosagentes, params, "midas.agente.reporte.agente.contabilidadMM.archivo.nombre","midas.agente.reporte.agente.bonosgerencia.archivo.nombre");
//**********************************************************************			
//			return impresionesService.getExcel(datosagentes, "midas.agente.reporte.agente.contabilidadMM.archivo.nombre",ConstantesReporte.TIPO_CSV);
//**********************************************************************
				MidasBaseReporteExcel sheet = new MidasBaseReporteExcel();
				sheet.setBookName("contabilidadMidas");
				sheet.setDataSource(datosagentes);
				file =  baseReporteService.generateExcelVeryLong(sheet);
				return file;
		}
		} catch (RuntimeException e) {
			LOG.error(e.getMessage(), e);
			System.gc();
		} catch (FileNotFoundException e) {
			LOG.error(e.getMessage(), e);
			System.gc();
		} catch (InvalidFormatException e) {
			LOG.error(e.getMessage(), e);
			System.gc();
		} catch (IllegalAccessException e) {
			LOG.error(e.getMessage(), e);
			System.gc();
		} catch (InvocationTargetException e) {
			LOG.error(e.getMessage(), e);
			System.gc();
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			System.gc();
		}
		finally {
			System.gc();
		}
	return null;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteDetalleBonosExcel(Long idAgente, Date fechaInicial, Date fechaFinal, String formatoArchivo) {
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		List<DatosAgentesDetalleBonoView> listaDetallePrimas = impresionesService
			.llenarDatosDetBonos(idAgente,  f.format(fechaInicial), f.format(fechaFinal));
			if (listaDetallePrimas!=null) {
				if(formatoArchivo.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
					return impresionesService.getExcel(listaDetallePrimas, DatosAgentesDetalleBonoView.PLANTILLA_NAME,
							ConstantesReporte.TIPO_XLS);
				}else if(formatoArchivo.equals(TipoSalidaReportes.TO_XLSX.getValue())){
					return impresionesService.getExcel(listaDetallePrimas, DatosAgentesDetalleBonoView.PLANTILLA_NAME,
							ConstantesReporte.TIPO_XLSX);
				}else{
					return impresionesService.getExcel(listaDetallePrimas, DatosAgentesDetalleBonoView.PLANTILLA_NAME,
							ConstantesReporte.TIPO_CSV);
				}
			}
			return  null;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteAgenteCalculoBonoPagado(Long idBeneficiario, String tipoUsuario, 
			Long idCalculoBonos)
	{
		return this.imprimirReporteAgenteCalculoBonoMensual(idBeneficiario, tipoUsuario, idCalculoBonos,null,null, null);
	}
	
	@Override
	public TransporteImpresionDTO imprimirReporteAgenteCalculoBonoMensual(Long idBeneficiario, String tipoUsuario, 
			String anio, String mes, Long tipoBeneficiario)
	{
		return this.imprimirReporteAgenteCalculoBonoMensual(idBeneficiario, tipoUsuario, null, anio, mes, tipoBeneficiario);
	}
	
	private TransporteImpresionDTO imprimirReporteAgenteCalculoBonoMensual(Long idBeneficiario, String tipoUsuario, 
			Long idCalculoBonos, String anio, String mes, Long tipoBeneficiario)
	{
		TransporteImpresionDTO reporte = null;
		List<DatosReporteCalculoBonoMensualDTO> listaBonos = new ArrayList<DatosReporteCalculoBonoMensualDTO>();		
		
		listaBonos = impresionesService.llenarDatosReporteCalculoBonoMensual(idBeneficiario, idCalculoBonos, 
				anio, mes, tipoBeneficiario);		
		
		if (listaBonos != null && !listaBonos.isEmpty()) {
			
			DatosReporteCalculoBonoMensualDTO primerRegistro = listaBonos.get(0);
			
			Map<String,Object> params = new HashMap<String,Object>();
			
			params.put("ID_BENEFICIARIO", primerRegistro.getIdBeneficiario());
			params.put("NOMBRE_BENEFICIARIO", primerRegistro.getNombreBeneficiario());
			params.put("TIPO_USUARIO", (tipoUsuario != null && !tipoUsuario.isEmpty()) ? tipoUsuario: GenerarPlantillaReporte.TIPO_USUARIO_AGENTE);
			params.put("PERIODO_FECHAPAGO", primerRegistro.getPeriodoFechaPago());
			
			reporte = impresionesService.getExcel(listaBonos,DatosReporteCalculoBonoMensualDTO.PLANTILLA_NAME,
					params,ConstantesReporte.TIPO_PDF);			
		}
		
		return reporte;
	}
	
	@Override
	public InputStream  imprimirReporteProduccion(Date fechaInicio, Date fechaFin) {
		try {
				final String fileName = "reporteProduccion_"
					+ Utilerias.cadenaDeFecha(fechaInicio, "dd-MM-yyyy")
					+ "_" + Utilerias.cadenaDeFecha(fechaFin, "dd-MM-yyyy");
				InputStream file = null;
				file = baseReporteService.findFile(fileName+".xls.zip");
				if (file == null) {
					List<DatosAgentesReporteProduccion> listaProduccion = impresionesService
					.llenarDatosReporteProd(fechaInicio,  fechaFin);
					if(listaProduccion!=null && !listaProduccion.isEmpty()){ 
						MidasBaseReporteExcel sheet = new MidasBaseReporteExcel();
						sheet.setBookName(fileName);
						sheet.setDataSource(listaProduccion);
						file =  baseReporteService.generateExcelVeryLong(sheet);
						return file;
					}
				}
				return file;
			} catch (RuntimeException e) {
				LOG.error(e.getMessage(), e);
				System.gc();
			} catch (FileNotFoundException e) {
				LOG.error(e.getMessage(), e);
				System.gc();
			} catch (InvalidFormatException e) {
				LOG.error(e.getMessage(), e);
				System.gc();
			} catch (IllegalAccessException e) {
				LOG.error(e.getMessage(), e);
				System.gc();
			} catch (InvocationTargetException e) {
				LOG.error(e.getMessage(), e);
				System.gc();
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
				System.gc();
			}
			finally {
				System.gc();
			}
		return null;
	}

	@Override
	public TransporteImpresionDTO imprimirReporePrimaPagadaVsPrimaEmitida(
			String fechaCorteInicio, String fechaCorteFin, Long centroOperacion, 
			Long gerencia, Long ejecutivo, Long promotoria, Long agente, String formatoArchivo) {
//		if(Integer.parseInt(mes)<10){
//			mes = "0"+mes;
//		}
//		String fecha1 = anio+mes;
		List<DatosAgentePrimaEmitidaVsPriamaPagadaDTO> listaPrimaPagVSPrimaEmit = impresionesService
		.llenarDatosRepPrimaEmitidaVsPrmaPagada(fechaCorteInicio, fechaCorteFin, centroOperacion, gerencia, ejecutivo, promotoria, agente);
		if(listaPrimaPagVSPrimaEmit != null && !listaPrimaPagVSPrimaEmit.isEmpty()){
		if(formatoArchivo.equals(TipoSalidaReportes.TO_EXCEL.getValue())){	
			return impresionesService.getExcel(listaPrimaPagVSPrimaEmit,
					DatosAgentePrimaEmitidaVsPriamaPagadaDTO.PLANTILLA_NAME, ConstantesReporte.TIPO_XLS);
		}else if(formatoArchivo.equals(TipoSalidaReportes.TO_XLSX.getValue())){	
			return impresionesService.getExcel(listaPrimaPagVSPrimaEmit,
					DatosAgentePrimaEmitidaVsPriamaPagadaDTO.PLANTILLA_NAME, ConstantesReporte.TIPO_XLSX);
		}else{
			return impresionesService.getExcel(listaPrimaPagVSPrimaEmit,
					DatosAgentePrimaEmitidaVsPriamaPagadaDTO.PLANTILLA_NAME, ConstantesReporte.TIPO_CSV);
			}
		}
		return null;
	}
	
	@Override
	public TransporteImpresionDTO imprimirRepGlobalComisionYbono(Long idAgente, Date fecha, Long idBono, Boolean bandera){
		
		List<ReporteGlobalBonoYComisionesDTO> listaRepGlobalComisionObono = impresionesService
		.llenarDatosReporteGlobalComisionesY_Bonos(idAgente, fecha, idBono, bandera);
		if(listaRepGlobalComisionObono != null && !listaRepGlobalComisionObono.isEmpty()){
			Map<String, Object> parameters = new HashMap<String, Object>(1);
			parameters.put("bandera",bandera);
			return impresionesService.getExcel(listaRepGlobalComisionObono,
					ReporteGlobalBonoYComisionesDTO.PLANTILLA_NAME, parameters,ConstantesReporte.TIPO_XLS);
		}
		return null;
	}
	
	@Override
	public TransporteImpresionDTO imprimirReporteDetalleProvision(String anio,String mes,ProvisionImportesRamo filtro, String formatoArchivo) {
		List<ReporteDetalleProvisionView> listaImportesRamo;
		try {
			listaImportesRamo = impresionesService.reporteDetalleProvisiones(anio,mes,filtro);
			if(listaImportesRamo!=null){
				if(formatoArchivo.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
					return impresionesService.getExcel(listaImportesRamo, ReporteDetalleProvisionView.PLANTILLA_NAME,ConstantesReporte.TIPO_XLS);
				}else if(formatoArchivo.equals(TipoSalidaReportes.TO_XLSX.getValue())){
					return impresionesService.getExcel(listaImportesRamo, ReporteDetalleProvisionView.PLANTILLA_NAME,ConstantesReporte.TIPO_XLSX);
				}else{
					return impresionesService.getExcel(listaImportesRamo, ReporteDetalleProvisionView.PLANTILLA_NAME,ConstantesReporte.TIPO_CSV);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}		
			
		return null;
	}	

	@Override
	public TransporteImpresionDTO imprimirReciboHonorarios(BigDecimal solicitudChequeId) {
		
		TransporteImpresionDTO transporte = new TransporteImpresionDTO();		
		byte[] reporte = null;
		Map<String, Object> parametros = null;
		List<DatosHonorariosAgentes> lista = new ArrayList<DatosHonorariosAgentes>(1);
		JasperReport honorarios = null;
		JRBeanCollectionDataSource datasource = null;
		try{			
			lista = impresionesService.llenarDatosHonorarios(solicitudChequeId);
			if(lista!=null){
				honorarios = getJasperReport(UR_JRXML+"honorariosAgentes.jrxml");	
				datasource = new JRBeanCollectionDataSource(lista);
				parametros = new HashMap<String, Object>();
				Locale locale = new Locale("es","MX");
				if(locale != null){
					parametros.put(JRParameter.REPORT_LOCALE, locale);
				}
			 	parametros.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_AFIRME);		 	
			 	reporte = generarReporte(honorarios, parametros, datasource);
			 	transporte.setByteArray(reporte);
			}else{
				return null;
			}
			
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}
		return transporte;
	}
	
	////generar pdf recibo orden pagos CA///
	public byte[] imprimirReporteAgenteDetalle(List<OrdenPagosDetalle> listaOrdenPagosDetalle) {
		byte[] pdfFacturacionDetalle = null;
		try {			
			JasperReport  impresionReporteAgentesDetalle = getJasperReport(
					UR_JRXML+"reporteAgente.jrxml");
			Map<String, Object> parameters = new HashMap<String, Object>(1);
			JRBeanCollectionDataSource datasource = new JRBeanCollectionDataSource(listaOrdenPagosDetalle);
			/*if(datosFacturacionDetalle != null){
				datosFacturacionDetalle.getNumero_poliza();*/
				parameters.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_AFIRME);
				/*parameters.put("poliza", datosFacturacionDetalle.getNumero_poliza());
				parameters.put("contratante", datosFacturacionDetalle.getContratante().toString());
				parameters.put("endoso", datosFacturacionDetalle.getEndoso());
				parameters.put("prima_base", datosFacturacionDetalle.getPrima_base());
				parameters.put("importe_prima_base", datosFacturacionDetalle.getImporte_prima_base());
				parameters.put("importePrima", datosFacturacionDetalle.getImporte_prima());
				parameters.put("porcentaje_comp_prima", datosFacturacionDetalle.getPorcentaje_comp_prima());
				parameters.put("comp_derecho_poliza", datosFacturacionDetalle.getComp_derecho_poliza());
				parameters.put("comp_baja_siniestralidad", datosFacturacionDetalle.getComp_baja_siniestralidad());
				parameters.put("total_compensacion", datosFacturacionDetalle.getTotal_compensacion());
			}*/
			pdfFacturacionDetalle = JasperRunManager.runReportToPdf(impresionReporteAgentesDetalle, parameters,datasource);
			
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
		return pdfFacturacionDetalle;
	}
	
	public byte[] imprimirReciboHonorariosCompensaciones(DatosHonorariosAgentes datosHonorariosAgentes) {
		byte[] pdfHonorariosCompensaciones = null;
		LOG.info("ntrando a imprimir Recibos de Honorarios"+ datosHonorariosAgentes.getNombre());
		try {			
			JasperReport  impresionHonorariosCompensaciones = getJasperReport(
					UR_JRXML+"honorariosCompensacionesAdicionales.jrxml");
			Map<String, Object> parameters = new HashMap<String, Object>(1);
			if(datosHonorariosAgentes != null){
				parameters.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_AFIRME);
				
				
				parameters.put("nombre", datosHonorariosAgentes.getNombre().toString());
	     		parameters.put("id_agente", datosHonorariosAgentes.getIdAgente());
				parameters.put("imp_base", datosHonorariosAgentes.getImporteBase());
				parameters.put("imp_iva", datosHonorariosAgentes.getImporteIva());
				//parameters.put("imp_subtotal", datosHonorariosAgentes.getImp_subtotal());
				parameters.put("imp_ret_isr", datosHonorariosAgentes.getImporteIsrRetenido());
				parameters.put("imp_ret_iva", datosHonorariosAgentes.getImporteIvaRetenido());
				parameters.put("imp_total", datosHonorariosAgentes.getImporteTotal());
				parameters.put("imp_gravable", datosHonorariosAgentes.getImporteGravable());
				parameters.put("imp_exento", datosHonorariosAgentes.getImporteExento());
				parameters.put("imp_total_Letra", datosHonorariosAgentes.getImporteTotalCLetra());
				parameters.put("folio", datosHonorariosAgentes.getOrigen());
                parameters.put("tipoBeneficiario", datosHonorariosAgentes.getNombreGerencia());
				
     		}
			pdfHonorariosCompensaciones = JasperRunManager.runReportToPdf(impresionHonorariosCompensaciones, parameters);
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
		return pdfHonorariosCompensaciones;
	}
	
	
	public byte[] imprimirReciboHonorariosComp(DatosHonorariosAgentes datosHonorariosAgentes) {
		byte[] pdfHonorariosCompensaciones = null;
		try {			
			JasperReport  impresionHonorariosCompensaciones = getJasperReport(
					UR_JRXML+"honorariosCompAdicionales.jrxml");
			Map<String, Object> parameters = new HashMap<String, Object>(1);
			if(datosHonorariosAgentes != null){
				parameters.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_AFIRME);
		    	parameters.put("nombre", datosHonorariosAgentes.getNombre().toString());				
	     		parameters.put("id_agente", datosHonorariosAgentes.getIdAgente());
				//parameters.put("folio", datosHonorariosAgentes.getOrigen());
				parameters.put("imp_total_Letra", datosHonorariosAgentes.getImporteTotalCLetra());
				parameters.put("imp_gravable", datosHonorariosAgentes.getImporteGravable());
				parameters.put("imp_exento", datosHonorariosAgentes.getImporteExento());
				parameters.put("imp_iva", datosHonorariosAgentes.getImporteIva());
				parameters.put("imp_ret_isr", datosHonorariosAgentes.getImporteIsrRetenido());
				parameters.put("imp_ret_iva", datosHonorariosAgentes.getImporteIvaRetenido());
				parameters.put("imp_total", datosHonorariosAgentes.getImporteTotal());
                parameters.put("tipoBeneficiario", datosHonorariosAgentes.getNombreGerencia());
               
     		}
			pdfHonorariosCompensaciones = JasperRunManager.runReportToPdf(impresionHonorariosCompensaciones, parameters);
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
		return pdfHonorariosCompensaciones;
	}
	
    public TransporteImpresionDTO  imprimirReporteAutorizacionOrdenpago(String idLiquidacion){
     OrdenesPagoDTO ordenesPagoDTO =  liquiuLiquidacionCompensacionesService.getReporteAutorizarOrdenPagos(idLiquidacion);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("P_IMAGE_LOGO",SistemaPersistencia.AFIRME_SEGUROS);	
		params.put("totalComisiones",ordenesPagoDTO.getSumatoria());			
		List<ReporteAutorizarOrdenPago> lista  = ordenesPagoDTO.getReporteAutorizarOrdenPagos();		
		return impresionesService.getExcel(lista, "midas.impresion.compensacionadicionales.reportePreviewCompensaciones",params,ConstantesReporte.TIPO_XLS);
	}
	
	
	@Override
	public TransporteImpresionDTO imprimirReporteProvision(String anio,String mes,ProvisionImportesRamo filtro, String formatoArchivo) {
		List<ReporteProvisionView> listaImportesRamo;
		try {
			listaImportesRamo = impresionesService.reporteProvisiones(anio, mes, filtro);
			if(listaImportesRamo!=null){
				if(formatoArchivo.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
					return impresionesService.
						getExcel(listaImportesRamo, ReporteProvisionView.PLANTILLA_NAME,ConstantesReporte.TIPO_XLS);
				}else if(formatoArchivo.equals(TipoSalidaReportes.TO_XLSX.getValue())){
					return impresionesService.
					getExcel(listaImportesRamo, ReporteProvisionView.PLANTILLA_NAME,ConstantesReporte.TIPO_XLSX);
			}else{
					return impresionesService.
					getExcel(listaImportesRamo, ReporteProvisionView.PLANTILLA_NAME,ConstantesReporte.TIPO_CSV);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}		
			
		return null;
	}
	
	@Override
	public TransporteImpresionDTO imprimirReporteDetallePrimas(String anio, String mes, Agente agente, String tipoSalidaArchivo) throws JRException {		
		
		List<DatosDetPrimas> listaDetPrimas = impresionesService.llenarDatosDetallePrimasReporte(anio,  mes,  agente);
		
		if(listaDetPrimas.isEmpty()){
			return null;
		}
		
		for(DatosDetPrimas list:listaDetPrimas){
    		if(list!=null && list.getContratante()!=null
    			&& list.getContratante().equalsIgnoreCase(SALDO_INICIAL)){
//    			list.setTotalpormovimiento(list.getImp_comis_agte());
    			list.setImporteTotalPorMovimiento(list.getImporteComisionAgente());
    			/*Comentado por Hileab, en base a una investigacion realizada para determinar que Regla de Negocio (RN) sustenta esta sección de 
                código, y al no encontrarse alguna se procede a comentar, se valido esto con Itzel Maria Aguilar Rodriguez y Christian Ezequiel Avalos Razo*/
				/*if(list.getContratante().equalsIgnoreCase("PRESTAMO") || list.getContratante().equalsIgnoreCase("PAGARE")){				
						list.setTotalpormovimiento(list.getTotalporpoliza());
				}*/
    		}
		}
		
		
		Collection<DetPrimasResumen> listaDetPrimasResumen = this.generarSeccionResumen(listaDetPrimas);
		
		DatosDetPrimas primerRegistro = listaDetPrimas.get(0);
	    boolean esSaldoIncial = false;
		if(primerRegistro.getContratante().equalsIgnoreCase(SALDO_INICIAL)){
			//Nota: se remueve el primer registro que corresponde al saldo inicial para tratarlo diferente.
			listaDetPrimas.remove(0);
			esSaldoIncial = true;
		}	
		if(!listaDetPrimas.isEmpty()){
			this.calcularColsComisAcumPorDiaYTotalPorMov(listaDetPrimas);
		}
		
        if(esSaldoIncial)
        {
//        	primerRegistro.setComis_acum_dia((primerRegistro.getTotalporpoliza()!=null)?primerRegistro.getTotalporpoliza():0);
        	primerRegistro.setImporteComisionAcumuladaDiaCorte(primerRegistro.getImporteTotalPorPoliza() != null ? primerRegistro.getImporteTotalPorPoliza() : BigDecimal.ZERO);
//    		primerRegistro.setTotalpormovimiento(primerRegistro.getTotalporpoliza());
//    		primerRegistro.setTotalpormovimiento(primerRegistro.getImp_comis_agte()+primerRegistro.getIni_iva()+primerRegistro.getIni_iva_ret()+primerRegistro.getIni_isr());
//        	Double imp_comis_agte =(primerRegistro.getImp_comis_agte()!=null)?primerRegistro.getImp_comis_agte():0;
        	BigDecimal importeComisionAgente = (primerRegistro.getImporteComisionAgente() != null ? primerRegistro.getImporteComisionAgente() : BigDecimal.ZERO);
//        	BigDecimal ini_iva =(primerRegistro.getIni_iva()!=null)?primerRegistro.getIni_iva():0;
        	BigDecimal importeIvaInicial = (primerRegistro.getImporteInicialIva() != null ? primerRegistro.getImporteInicialIva() : BigDecimal.ZERO);
//        	BigDecimal ini_iva_ret =(primerRegistro.getIni_iva_ret()!=null)?primerRegistro.getIni_iva_ret():0;
        	BigDecimal importeIvaRetenidoInicial = (primerRegistro.getImporteInicialIvaRetenido() != null ? primerRegistro.getImporteInicialIvaRetenido() : BigDecimal.ZERO);
//			BigDecimal ini_isr = (primerRegistro.getIni_isr()!=null)?primerRegistro.getIni_isr():0;
			BigDecimal importeIsrInicial = (primerRegistro.getImporteInicialIsr() != null ? primerRegistro.getImporteInicialIsr() : BigDecimal.ZERO);
//			BigDecimal ini_ret_est = (primerRegistro.getIni_ret_est()!=null)?primerRegistro.getIni_ret_est():0;
			BigDecimal importeEstatalRetenidoInicial = (primerRegistro.getImporteInicialEstatalRetenido() != null ? primerRegistro.getImporteInicialEstatalRetenido() : BigDecimal.ZERO);
//    		primerRegistro.setTotalpormovimiento(imp_comis_agte+ini_iva+ini_iva_ret+ini_isr);
			BigDecimal importeTotalPorMoimiviento = importeComisionAgente.add(importeIvaInicial.add(importeIvaRetenidoInicial.add(importeIsrInicial)));
			primerRegistro.setImporteTotalPorMovimiento(importeTotalPorMoimiviento);
			System.out.println(importeComisionAgente.toString() + " + " + importeIvaInicial.toString() + " + " + importeIvaRetenidoInicial.toString() + " + " + importeIsrInicial.toString() + " = " + importeTotalPorMoimiviento.toString());
//    		primerRegistro.setImp_iva_acred(ini_iva);
			primerRegistro.setImporteIvaAcreditado(importeIvaInicial);
//    		primerRegistro.setImp_iva_ret(ini_iva_ret);
			primerRegistro.setImporteIvaRetenido(importeIvaRetenidoInicial);
//    		primerRegistro.setImp_isr(ini_isr);
    		primerRegistro.setImporteIsr(importeIsrInicial);
//    		primerRegistro.setImp_ret_est(ini_ret_est);
    		primerRegistro.setImporteEstatalRetenido(importeEstatalRetenidoInicial);
    		
    		listaDetPrimas.add(0, primerRegistro);
        }				
            	
				
		Map<String,Object> params = this.llenarDatosHeaderRepDetallePrimas(agente, listaDetPrimasResumen, anio, mes);		
		params.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_AFIRME);
		
		if(tipoSalidaArchivo.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
		return impresionesService.getExcel(listaDetPrimas,
				DatosDetPrimas.PLANTILLA_NAME, params,ConstantesReporte.TIPO_XLS);
		}else{
			return impresionesService.getExcel(listaDetPrimas,
					DatosDetPrimas.PLANTILLA_NAME, params,ConstantesReporte.TIPO_PDF);
		}
	}
	
	private Map<String, Object> llenarDatosHeaderRepDetallePrimas(Agente agente, Collection<DetPrimasResumen> listaDetPrimasResumen, String anio, String mes) throws JRException {
		Map<String, Object> paramsHeader = new HashMap<String, Object>(1);
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
		List<Domicilio> dom = entidadService.findByProperty(Domicilio.class, "idPersona", agente.getPersona().getIdPersona());
		
		Domicilio domicilio =null;
		if(!dom.isEmpty()){
			domicilio =dom.get(0);
		}
		
//		Domicilio domicilio = agente.getPersona().getDomicilios().get(0);

		paramsHeader.put("NUMERO_AGENTE", agente.getIdAgente());
		paramsHeader.put("ESTATUS_AGENTE", agente.getTipoSituacion().getValor());
		paramsHeader.put("NOMBRE_AGENTE", agente.getPersona().getNombreCompleto());
		paramsHeader.put("NUMERO_FIANZA", agente.getNumeroFianza());
		paramsHeader.put("VENCIMIENTO_FIANZA", f.format(agente.getFechaVencimientoFianza()));
		paramsHeader.put("NUMERO_CEDULA", agente.getNumeroCedula());
		paramsHeader.put("VENCIMIENTO_CEDULA", f.format(agente.getFechaVencimientoCedula()));
		paramsHeader.put("CENTRO_OPERACION", agente.getPromotoria()
				.getEjecutivo().getGerencia().getCentroOperacion()
				.getDescripcion());
		paramsHeader.put("GERENCIA", agente.getPromotoria().getEjecutivo()
				.getGerencia().getDescripcion());
		
		String[] monthsArray = new DateFormatSymbols(new Locale("es","MX")).getMonths();
		
		paramsHeader.put("MES_ANIO_DESC", monthsArray[Integer.valueOf(mes)-1] + " " + anio);
		paramsHeader.put("RFC", agente.getPersona().getRfc());
		if(domicilio!=null){
			paramsHeader.put("DOMICILIO", domicilio.getCalleNumero() + " Col. "
							+ domicilio.getNombreColonia() + " C.P. "
							+ domicilio.getCodigoPostal() + " "
							+ domicilio.getCiudad() + ", " + domicilio.getEstado());
		}else{
			paramsHeader.put("DOMICILIO"," "); 
		}
		paramsHeader.put("EJECUTIVO", agente.getPromotoria().getEjecutivo()
				.getPersonaResponsable().getNombreCompleto());
		paramsHeader.put("PROMOTORIA", agente.getPromotoria().getIdPromotoria()
				+ " " + agente.getPromotoria().getDescripcion());	
	
		paramsHeader.put("DETALLE_DATASOURCE", listaDetPrimasResumen);
		paramsHeader.put("IMAGE_LOGO", SistemaPersistencia.LOGO_AFIRME);
		
        JasperReport detallePrimasAgenteResumen =  getJasperReport(
        		UR_JRXML+"detallePrimasAgenteResumen.jrxml");
        paramsHeader.put("DETALLE_PRIMAS_AGENTE_RESUMEN", detallePrimasAgenteResumen);

		return paramsHeader;
	}
	
	private void calcularColsComisAcumPorDiaYTotalPorMov(List<DatosDetPrimas> listaDetPrimas){
		BigDecimal sumatoria = BigDecimal.ZERO;
		Long idConceptoRAnterior = Long.valueOf("4");
		//Se inicializa con objeto dummy
		DatosDetPrimas registroAnterior = new DatosDetPrimas(); 
		
		for(DatosDetPrimas registro: listaDetPrimas)
		{
			String ultimoDia = "0";
//			if(registro.getNum_poliza != null && !registro.getNum_poliza().isEmpty())
			if(registro.getNumeroPoliza() != null && !registro.getNumeroPoliza().isEmpty())
			{
				if(ultimoDia.equalsIgnoreCase(registro.getDiaCorte()))
				{					
//					sumatoria += (registro.getTotalporpoliza()!=null)?registro.getTotalporpoliza():0;
					if(registro.getImporteTotalPorPoliza() != null) {
						sumatoria = sumatoria.add(registro.getImporteTotalPorPoliza());
					}
//					registro.setComis_acum_dia(sumatoria);										
					registro.setImporteComisionAcumuladaDiaCorte(sumatoria);
				}
				else
				{
					ultimoDia = registro.getDiaCorte();
//					registro.setComis_acum_dia((registro.getTotalporpoliza()!=null)?registro.getTotalporpoliza():0);
					registro.setImporteComisionAcumuladaDiaCorte(registro.getImporteTotalPorPoliza() != null ? registro.getImporteTotalPorPoliza() : BigDecimal.ZERO);
//					if(registroAnterior.getNum_poliza() != null && !registroAnterior.getNum_poliza().isEmpty())
					if(registroAnterior.getNumeroPoliza() != null && !registroAnterior.getNumeroPoliza().isEmpty())
					{
//						registroAnterior.setTotalpormovimiento(sumatoria);						
						registroAnterior.setImporteTotalPorMovimiento(sumatoria);
					}
					
//					sumatoria = (registro.getTotalporpoliza()!=null)?registro.getTotalporpoliza():0;								
					sumatoria = (registro.getImporteTotalPorPoliza() != null ? registro.getImporteTotalPorPoliza() : BigDecimal.ZERO);
				}				
			}
			else
			{
				//if(!ultimoDia.equalsIgnoreCase(registro.getDia_Str())) 
				//if(!ultimoDia.equalsIgnoreCase(registro.getDiaCorte()) || registro.getContratante().equalsIgnoreCase("NUESTRO PAGO DE COMISIONES"))
				//&& registro.getCve_c_a().equalsIgnoreCase("C")
//				if(!ultimoDia.equalsIgnoreCase(registro.getDiaCorte()) || (registro.getCve_t_cpto_a_c_o().equalsIgnoreCase(AGAUT) && registro.getId_concepto().equals(idConceptoRAnterior) ) )  
					if(!ultimoDia.equalsIgnoreCase(registro.getDiaCorte()) || (registro.getTipoConceptoOrigen().equalsIgnoreCase(AGAUT) && registro.getIdConcepto().equals(idConceptoRAnterior) ) )
				{
					//if(registroAnterior.getContratante()!=null && !registroAnterior.getContratante().equalsIgnoreCase("NUESTRO PAGO DE COMISIONES")){
					//&& registroAnterior.getCve_c_a().equalsIgnoreCase("C")
//					if(registroAnterior.getContratante()!=null && !(registroAnterior.getCve_t_cpto_a_c_o().equalsIgnoreCase(AGAUT) && registroAnterior.getId_concepto().equals(idConceptoRAnterior) )){
						if(registroAnterior.getContratante()!=null && !(registroAnterior.getTipoConceptoOrigen().equalsIgnoreCase(AGAUT) && registroAnterior.getIdConcepto().equals(idConceptoRAnterior) )){
//						registroAnterior.setTotalpormovimiento(sumatoria);
						registroAnterior.setImporteTotalPorMovimiento(sumatoria);
					}					
					ultimoDia = registro.getDiaCorte();
				}
				BigDecimal importeTotalPorMovimiento = BigDecimal.ZERO;
				importeTotalPorMovimiento = registro.getImporteComisionAgente().add(registro.getImporteIvaAcreditado().add(registro.getImporteIvaRetenido().add(registro.getImporteIsr())));
//				registro.setTotalpormovimiento(registro.getImp_comis_agte()+registro.getImp_iva_acred()+registro.getImp_iva_ret()+registro.getImp_isr());
				registro.setImporteTotalPorMovimiento(importeTotalPorMovimiento);
//				registro.setTotalporpoliza(0.0);
				registro.setImporteTotalPorPoliza(new BigDecimal(0.0));
//				registro.setComis_acum_dia(0.0);
				registro.setImporteComisionAcumuladaDiaCorte(new BigDecimal(0.0));
//				sumatoria=0.0;
				sumatoria = new BigDecimal(0.0);
				
				//HJC: Cambio solicitado por Itzel, Se añade el siguiente código para garantizar que el pago de comisiones tambien muestre valores en las columnas "Total por póliza" y "Comisión Acumulada al Día"
				//if(registro.getContratante().equalsIgnoreCase("NUESTRO PAGO DE COMISIONES")){
				//&& registro.getCve_c_a().equalsIgnoreCase("C")
//				if(registro.getCve_t_cpto_a_c_o().equalsIgnoreCase(AGAUT) && registro.getId_concepto().equals(idConceptoRAnterior) ){					
					if(registro.getTipoConceptoOrigen().equalsIgnoreCase(AGAUT) && registro.getIdConcepto().equals(idConceptoRAnterior) ){
//					registro.setTotalporpoliza(registro.getTotalpormovimiento());
					registro.setImporteTotalPorPoliza(registro.getImporteTotalPorMovimiento());
//					registro.setComis_acum_dia(registro.getTotalpormovimiento());
					registro.setImporteComisionAcumuladaDiaCorte(registro.getImporteTotalPorMovimiento());
				}
			}
			
			registroAnterior = registro;
		}		
		
		//establece el valor al ultimo registro.
//		if(registroAnterior.getNum_poliza() != null && !registroAnterior.getNum_poliza().isEmpty()){									
		if(registroAnterior.getNumeroPoliza() != null && !registroAnterior.getNumeroPoliza().isEmpty()){
//				registroAnterior.setTotalpormovimiento(sumatoria);			
				registroAnterior.setImporteTotalPorMovimiento(sumatoria);
		}
		else{
			//if(registroAnterior.getContratante().equalsIgnoreCase("NUESTRO PAGO DE COMISIONES")){
			//&& registroAnterior.getCve_c_a().equalsIgnoreCase("C")
//			if(registroAnterior.getCve_t_cpto_a_c_o().equalsIgnoreCase(AGAUT) && registroAnterior.getId_concepto().equals(idConceptoRAnterior) ){
			if(registroAnterior.getTipoConceptoOrigen().equalsIgnoreCase(AGAUT) && registroAnterior.getIdConcepto().equals(idConceptoRAnterior) ){
//				registroAnterior.setTotalpormovimiento(registroAnterior.getImp_comis_agte()+registroAnterior.getImp_iva_acred()+registroAnterior.getImp_iva_ret()+registroAnterior.getImp_isr());
				BigDecimal importeTotalPorMovimiento = registroAnterior.getImporteComisionAgente().add(registroAnterior.getImporteIvaAcreditado().add(registroAnterior.getImporteIvaRetenido().add(registroAnterior.getImporteIsr())));
				registroAnterior.setImporteTotalPorMovimiento(importeTotalPorMovimiento);
			}else{
//				registroAnterior.setTotalpormovimiento((registroAnterior.getTotalporpoliza()!=null)?registroAnterior.getTotalporpoliza():0);
				registroAnterior.setImporteTotalPorMovimiento(registroAnterior.getImporteTotalPorPoliza() != null ? registroAnterior.getImporteTotalPorPoliza() : BigDecimal.ZERO);
			}
		}		
	}
	
	private Collection<DetPrimasResumen> generarSeccionResumen(List<DatosDetPrimas> listaDetPrimas)
	{
		Map<String, DetPrimasResumen> resumen = new LinkedHashMap<String, DetPrimasResumen>(); 			
		
		for(DatosDetPrimas registro: listaDetPrimas)
		{
//			String keyRamo = registro.getDescripcionramo();
			String keyRamo = registro.getRamo();
			if(resumen.containsKey(keyRamo))
			{
//				resumen.get(keyRamo).agregarMovimiento((registro.getTotalporpoliza()!=null)?registro.getTotalporpoliza():0);
				Double movimiento = new Double(0.0);
				if(registro.getImporteTotalPorPoliza() != null) {
					movimiento = registro.getImporteTotalPorPoliza().doubleValue();
				}
				resumen.get(keyRamo).agregarMovimiento(movimiento);
			}
			else
			{				
				DetPrimasResumen registroResumen = new DetPrimasResumen();
				registroResumen.setConcepto(keyRamo);					
 				if(registroResumen.getConcepto().equalsIgnoreCase(SALDO_INICIAL)){
//					registroResumen.setAbono(registro.getImp_comis_agte()+registro.getIni_iva()+registro.getIni_iva_ret()+registro.getIni_isr());
// 					Double imp_comis_agte =(registro.getImp_comis_agte()!=null)?registro.getImp_comis_agte():0; 
 					BigDecimal importeComisionAgente =(registro.getImporteComisionAgente() != null ? registro.getImporteComisionAgente() : BigDecimal.ZERO);
// 					BigDecimal ini_iva = (registro.getIni_iva()!=null)?registro.getIni_iva():0;
 					BigDecimal importeIvaInicial = (registro.getImporteInicialIva()!= null  ? registro.getImporteInicialIva() : BigDecimal.ZERO);
// 					BigDecimal ini_iva_ret =(registro.getIni_iva_ret()!=null)?registro.getIni_iva_ret():0;
 					BigDecimal importeIvaRetenidoInicial =(registro.getImporteInicialIvaRetenido() != null ? registro.getImporteInicialIvaRetenido() : BigDecimal.ZERO);
// 					BigDecimal ini_isr = (registro.getIni_isr()!=null)?registro.getIni_isr():BigDecimal.ZERO;
 					BigDecimal importeIsrInicial = (registro.getImporteInicialIsr() != null ? registro.getImporteInicialIsr() : BigDecimal.ZERO);
// 					BigDecimal ini_ret_est = (registro.getIni_ret_est()!=null)?registro.getIni_ret_est():BigDecimal.ZERO;
 					BigDecimal importeEstatalRetenidoInicial = (registro.getImporteInicialEstatalRetenido() != null ? registro.getImporteInicialIvaRetenido() : BigDecimal.ZERO);
//					registroResumen.agregarMovimiento(imp_comis_agte+ini_iva+ini_iva_ret+ini_isr+ini_ret_est);
 					Double movimiento = importeComisionAgente.add(importeIvaInicial.add(importeIvaRetenidoInicial.add(importeIsrInicial.add(importeEstatalRetenidoInicial)))).doubleValue();
 					
					registroResumen.agregarMovimiento(movimiento);
				}else{
//					registroResumen.agregarMovimiento((registro.getTotalporpoliza()!=null)?registro.getTotalporpoliza():0);
					registroResumen.agregarMovimiento(registro.getImporteTotalPorPoliza() != null ? registro.getImporteTotalPorPoliza().doubleValue() : 0);
				}
				resumen.put(keyRamo, registroResumen);
			}			
		}
	    
		return resumen.values();
	}
	
	@Override
	public TransporteImpresionDTO imprimirReporteRecibosProveedores(String mesAnio) 
		throws JRException	{
		Map<String, Object> params = new LinkedHashMap<String, Object>(1);
		params.put("periodo", mesAnio);
		List<DatosRecibosProveedores> listaRecibosProv = entidadService.findByProperties(DatosRecibosProveedores.class, params);
		if(listaRecibosProv != null && !listaRecibosProv.isEmpty())
		{
			return impresionesService.getExcel(listaRecibosProv, 
					DatosRecibosProveedores.PLANTILLA_NAME,ConstantesReporte.TIPO_XLS);			
		}
		else
		{
			return null;
		}		
	}

	@Override
	public TransporteImpresionDTO imprimirReporteAgenteContabilidadMizar(
			String anioInicio, String mesInicio,String anioFin, String mesFin,
			Long idCentroOperacion, Long idGerencia, Long idEjecutivo,
			Long idPromotoria, Long idAgente, Locale locale) {
		
		List<DatosAgenteContabilidadMMView> datosagentes = impresionesService
		.llenarDatosAgenteContabilidadMizar(anioInicio, mesInicio,
				anioFin, mesFin, idCentroOperacion, idGerencia,
				idEjecutivo, idPromotoria, idAgente);
		if (datosagentes != null) {
			return impresionesService.getExcel(datosagentes, 
					DatosAgenteContabilidadMMView.PLANTILLA_NAME,ConstantesReporte.TIPO_CSV);
		}
		return null;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteSaldosVsContabilidad(List<SaldosVsContabilidadView> datosSaldosVsContabilidad, String formatoSalida) {
//		List<SaldosVsContabilidadView> datosSaldosVsContabilidad = new ArrayList<SaldosVsContabilidadView>();
//		datosSaldosVsContabilidad = impresionesService.llenarDatosReporteSaldosVsContabilidad(fechaCorte);
		if(datosSaldosVsContabilidad != null && !datosSaldosVsContabilidad.isEmpty()){
			if(formatoSalida.equals(TipoSalidaReportes.TO_XLSX.getValue())){
				LogDeMidasInterfaz.log("datosSaldosVsContabilidad: "+datosSaldosVsContabilidad.size()+"entrando a generar excel..." + this, Level.WARNING, null);
				return impresionesService.getExcel(datosSaldosVsContabilidad, 
						SaldosVsContabilidadView.PLANTILLA_NAME, ConstantesReporte.TIPO_XLSX);
			}else{
				LogDeMidasInterfaz.log("entrando a generar texto..." + this, Level.WARNING, null);
				return impresionesService.getExcel(datosSaldosVsContabilidad, 
						SaldosVsContabilidadView.PLANTILLA_NAME, ConstantesReporte.TIPO_CSV);	
			}
		}
		return null;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteEfectividadEntrega(List<ReporteEfectividadEntrega> listDataSource,
				Map<String, Object> params){
		return impresionesService.getReporteEfectividad(listDataSource, params);
	}
	
	private Boolean mostrarSeccionReporteEdocta (String seccion){
	
		ValorCatalogoAgentes seccionAmostrar = new ValorCatalogoAgentes(); 
		try {
			seccionAmostrar = valorCatalogoAgentesService.obtenerElementoEspecifico("Mostrar Secciones Estado de Cuenta", seccion);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
		if (seccionAmostrar!=null && seccionAmostrar.getId()!=null){
			return (Integer.parseInt(seccionAmostrar.getClave()) ==0)?false:true;
		}
		return false;
	}
	
	public byte[] imprimirCotizacionMovilVida(ResumenCotMovilVidaDTO resumenCotMovilVidaDTO) {
		byte[] pdfCotizacionVida = null;
		try {
			
			JasperReport  impresionCotizacionMovilVida = getJasperReport(
					"/mx/com/afirme/vida/service/impl/impresiones/jrxml/CotizacionMovilVida.jrxml");
			Map<String, Object> parameters = new HashMap<String, Object>(1);
			if(resumenCotMovilVidaDTO != null)			
			parameters.put("ENCABEZADO_AFIRME", SistemaPersistencia.ENCABEZADO_AFIRME);
			parameters.put("COBERTURA_BAS_PLAT", SistemaPersistencia.COBERTURA_BAS_PLAT);
			parameters.put("DESCRIPCIONESVIDA", SistemaPersistencia.DESCRIPCIONESVIDA);
			parameters.put("NOMBRE_PROSPECTO", resumenCotMovilVidaDTO.getCotizacionMovil().getNombreProspecto() == null || 
					resumenCotMovilVidaDTO.getCotizacionMovil().getNombreProspecto().trim().isEmpty() ? 
							"CLIENTE" : resumenCotMovilVidaDTO.getCotizacionMovil().getNombreProspecto());
			parameters.put("EMAIL_PROSPECTO", regresaObjectEsNuloParametro(resumenCotMovilVidaDTO.getCotizacionMovil().getEmailProspecto(), ""));
			parameters.put("TELEFONO_PROSPECTO", regresaObjectEsNuloParametro(resumenCotMovilVidaDTO.getCotizacionMovil().getTelefonoProspecto(), ""));
			parameters.put("EDAD", resumenCotMovilVidaDTO.getEdad().toString());
			
			parameters.put("SUMA_ASEGURADA", UtileriasWeb.formatoMoneda(resumenCotMovilVidaDTO.getCotizacionMovil().getSumaasegurada()));
			parameters.put("PRIMA_AMPLIA", UtileriasWeb.formatoMoneda(resumenCotMovilVidaDTO.getCotizacionMovil().getTarifaPlatino()));
			parameters.put("PRIMA_LIMITADA", UtileriasWeb.formatoMoneda(resumenCotMovilVidaDTO.getCotizacionMovil().getTarifaBasica()));
			parameters.put("VALOR_ANUAL_BASICO", resumenCotMovilVidaDTO.getCotizacionMovil().getTarifaBasica().toString());
			parameters.put("VALOR_ANUAL_PLATINO", resumenCotMovilVidaDTO.getCotizacionMovil().getTarifaPlatino().toString());
			parameters.put("MENSUALBASICO",resumenCotMovilVidaDTO.getMensualBasico().toString());
			parameters.put("TRIMESTRALBASICO",resumenCotMovilVidaDTO.getTrimestralBasico().toString());
			parameters.put("SEMESTRALBASICO",resumenCotMovilVidaDTO.getSemestralBasico().toString());
			parameters.put("MENSUALPLATINO",resumenCotMovilVidaDTO.getMensualPlatino().toString());
			parameters.put("TRIMESTRALPLATINO",resumenCotMovilVidaDTO.getTrimestralPlatino().toString());
			parameters.put("SEMESTRALPLATINO",resumenCotMovilVidaDTO.getSemestralPlatino().toString());
			parameters.put("DERECHOPOLIZA",resumenCotMovilVidaDTO.getDerechoPoiza().toString());
			
			pdfCotizacionVida = JasperRunManager.runReportToPdf(impresionCotizacionMovilVida, parameters);
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
		return pdfCotizacionVida;
	}
	
	
	/**
	 * 
	 */
	@Override
	public TransporteImpresionDTO imprimirReporteAgenteVentasProduccion(
			String anio, String mesInicio, String mesFin, Integer crecimiento,  Long idCentroOperacion, Long idGerencia,
			Long idEjecutivo, Long idPromotoria, Long idAgente, Locale locale, String formatoArchivo) {

		Map<String,Object>params = new HashMap<String, Object>(1);
		List<DatosVentasProduccion> datosagentes = impresionesService
		.llenarDatosVentasProduccion(anio, mesInicio, mesFin, idCentroOperacion, idGerencia, idEjecutivo, idPromotoria, idAgente);
		
		if (datosagentes != null) {
			if(formatoArchivo.equals(TipoSalidaReportes.TO_XLSX.getValue())){
				
				JasperReport periodoAnterior = getJasperReport(
						UR_JRXML+"SubreporteVentasProduccion.jrxml");
				JasperReport periodoActual = getJasperReport(
						UR_JRXML+"subrpteVentasProduccionDet1.jrxml");
					params.put("periodoAnterior",periodoAnterior);
					params.put("periodoActual",periodoActual);
					params.put("datosagentes",datosagentes);
					params.put("mesFin",Integer.parseInt(mesFin));
					params.put("mesInicio",Integer.parseInt(mesInicio));
					params.put("periodo",Integer.parseInt(anio));
					params.put("crecimiento",crecimiento);
					params.put("pctGlobal",getPctRepVentasProduccion(datosagentes, crecimiento));
				return impresionesService.
					getExcel(getCre_decre_repVentasProd(datosagentes, crecimiento,Integer.parseInt(mesInicio),Integer.parseInt(mesFin)),
							DatosVentasProduccion.PLANTILLA_NAME,params,ConstantesReporte.TIPO_XLSX);
			}
		}
		return null;
	}
	
	private BigDecimal getPctRepVentasProduccion(List<DatosVentasProduccion> datosagentes, Integer crecimiento){
		
		BigDecimal totalPorProducto = BigDecimal.ZERO; 
		BigDecimal paTotalPorProducto = BigDecimal.ZERO;
		BigDecimal pct = BigDecimal.ZERO;
		if(crecimiento==1){
			for(DatosVentasProduccion obj : datosagentes){
					BigDecimal a = BigDecimal.ZERO;  
					BigDecimal b = BigDecimal.ZERO; 
					a = a.add((null!=obj.getPrimaPagadaEne())?obj.getPrimaPagadaEne(): BigDecimal.ZERO);
					a = a.add((null!=obj.getPrimaPagadaFeb())?obj.getPrimaPagadaFeb(): BigDecimal.ZERO);
					a = a.add((null!=obj.getPrimaPagadaMar())?obj.getPrimaPagadaMar(): BigDecimal.ZERO);
					a = a.add((null!=obj.getPrimaPagadaAbr())?obj.getPrimaPagadaAbr(): BigDecimal.ZERO);
					a = a.add((null!=obj.getPrimaPagadaMay())?obj.getPrimaPagadaMay(): BigDecimal.ZERO);
					a = a.add((null!=obj.getPrimaPagadaJun())?obj.getPrimaPagadaJun(): BigDecimal.ZERO);
					a = a.add((null!=obj.getPrimaPagadaJul())?obj.getPrimaPagadaJul(): BigDecimal.ZERO);
					a = a.add((null!=obj.getPrimaPagadaAgo())?obj.getPrimaPagadaAgo(): BigDecimal.ZERO);
					a = a.add((null!=obj.getPrimaPagadaSep())?obj.getPrimaPagadaSep(): BigDecimal.ZERO);
					a = a.add((null!=obj.getPrimaPagadaOct())?obj.getPrimaPagadaOct(): BigDecimal.ZERO);
					a = a.add((null!=obj.getPrimaPagadaNov())?obj.getPrimaPagadaNov(): BigDecimal.ZERO);
					a = a.add((null!=obj.getPrimaPagadaDic())?obj.getPrimaPagadaDic(): BigDecimal.ZERO);
					
					b = b.add((null!=obj.getPaPrimaPagadaEne())?obj.getPaPrimaPagadaEne():BigDecimal.ZERO);
					b = b.add((null!=obj.getPaPrimaPagadaFeb())?obj.getPaPrimaPagadaFeb():BigDecimal.ZERO);
					b = b.add((null!=obj.getPaPrimaPagadaMar())?obj.getPaPrimaPagadaMar():BigDecimal.ZERO);
					b = b.add((null!=obj.getPaPrimaPagadaAbr())?obj.getPaPrimaPagadaAbr():BigDecimal.ZERO);
					b = b.add((null!=obj.getPaPrimaPagadaMay())?obj.getPaPrimaPagadaMay():BigDecimal.ZERO);
					b = b.add((null!=obj.getPaPrimaPagadaJun())?obj.getPaPrimaPagadaJun():BigDecimal.ZERO);
					b = b.add((null!=obj.getPaPrimaPagadaJul())?obj.getPaPrimaPagadaJul():BigDecimal.ZERO);
					b = b.add((null!=obj.getPaPrimaPagadaAgo())?obj.getPaPrimaPagadaAgo():BigDecimal.ZERO);
					b = b.add((null!=obj.getPaPrimaPagadaSep())?obj.getPaPrimaPagadaSep():BigDecimal.ZERO);
					b = b.add((null!=obj.getPaPrimaPagadaOct())?obj.getPaPrimaPagadaOct():BigDecimal.ZERO);
					b = b.add((null!=obj.getPaPrimaPagadaNov())?obj.getPaPrimaPagadaNov():BigDecimal.ZERO);
					b = b.add((null!=obj.getPaPrimaPagadaDic())?obj.getPaPrimaPagadaDic():BigDecimal.ZERO);
					
					 totalPorProducto =totalPorProducto.add(a); 
					 paTotalPorProducto = paTotalPorProducto.add(b);
				}
			pct = (totalPorProducto.divide(paTotalPorProducto, 2).subtract(BigDecimal.ONE));
			return pct;
			}
		return null;
	}
	private List<DatosVentasProduccionAux> getCre_decre_repVentasProd(List<DatosVentasProduccion> datosagentes, Integer crecimiento,Integer mesIncio, Integer MesFin ){
		
		List<DatosVentasProduccionAux> lista = new ArrayList<DatosVentasProduccionAux>();
		for(DatosVentasProduccion obj : datosagentes){
			DatosVentasProduccionAux obj2 = new DatosVentasProduccionAux();
			BigDecimal a = BigDecimal.ZERO;  
			BigDecimal b = BigDecimal.ZERO; 
				if(1>=mesIncio && 1<=MesFin){
					if(crecimiento==1){
							a = a.add((null!=obj.getPrimaPagadaEne())?obj.getPrimaPagadaEne(): BigDecimal.ZERO);
							b = b.add((null!=obj.getPaPrimaPagadaEne())?obj.getPaPrimaPagadaEne():BigDecimal.ZERO);
					}else{
						obj2.setImporteEne((null!=obj.getPrimaPagadaEne()?obj.getPrimaPagadaEne():BigDecimal.ZERO).subtract((null != obj.getPaPrimaPagadaEne())?obj.getPaPrimaPagadaEne():BigDecimal.ZERO));
						if(null!=obj.getPrimaPagadaEne()&& obj.getPrimaPagadaEne().compareTo(BigDecimal.ZERO) !=0  && null!= obj.getPaPrimaPagadaEne() && obj.getPaPrimaPagadaEne().compareTo(BigDecimal.ZERO)!=0){
							obj2.setPorcentajeEne(obj.getPrimaPagadaEne().divide(obj.getPaPrimaPagadaEne(), 2, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
						}
					}
				}
				if(2>=mesIncio && 2<=MesFin){
					if(crecimiento==1){
							a = a.add((null!=obj.getPrimaPagadaFeb())?obj.getPrimaPagadaFeb():BigDecimal.ZERO);
							b = b.add((null!=obj.getPaPrimaPagadaFeb())?obj.getPaPrimaPagadaFeb():BigDecimal.ZERO);
					}else{
						obj2.setImporteFeb((null != obj.getPrimaPagadaFeb()?obj.getPrimaPagadaFeb(): BigDecimal.ZERO).subtract((null!=obj.getPaPrimaPagadaFeb()?obj.getPaPrimaPagadaFeb(): BigDecimal.ZERO)));
						if(null!=obj.getPrimaPagadaFeb() && obj.getPrimaPagadaFeb().compareTo(BigDecimal.ZERO)!=0 && null!= obj.getPaPrimaPagadaFeb() && obj.getPaPrimaPagadaFeb().compareTo(BigDecimal.ZERO)!=0){
							obj2.setPorcentajeFeb(obj.getPrimaPagadaFeb().divide(obj.getPaPrimaPagadaFeb(), 2, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
						}
					}
				}
				if(3>=mesIncio && 3<=MesFin){
					if(crecimiento==1){
							a = a.add((null!=obj.getPrimaPagadaMar())?obj.getPrimaPagadaMar():BigDecimal.ZERO);
							b = b.add((null!=obj.getPaPrimaPagadaMar())?obj.getPaPrimaPagadaMar():BigDecimal.ZERO);
					}else{
						obj2.setImporteMar((null!=obj.getPrimaPagadaMar()?obj.getPrimaPagadaMar():BigDecimal.ZERO).subtract((null!=obj.getPaPrimaPagadaMar()?obj.getPaPrimaPagadaMar(): BigDecimal.ZERO)));
						if(null!=obj.getPrimaPagadaMar() && obj.getPrimaPagadaMar().compareTo(BigDecimal.ZERO)!=0 && null!= obj.getPaPrimaPagadaMar() && obj.getPaPrimaPagadaMar().compareTo(BigDecimal.ZERO)!=0){
							obj2.setPorcentajeMar(obj.getPrimaPagadaMar().divide(obj.getPaPrimaPagadaMar(), 2, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
						}
					}
				}
				if(4>=mesIncio && 4<=MesFin){
					if(crecimiento==1){
							a = a.add((null!=obj.getPrimaPagadaAbr())?obj.getPrimaPagadaAbr():BigDecimal.ZERO);
							b = b.add((null!=obj.getPaPrimaPagadaAbr())?obj.getPaPrimaPagadaAbr():BigDecimal.ZERO);
					}else{
						obj2.setImporteAbr((null!=obj.getPrimaPagadaAbr()?obj.getPrimaPagadaAbr(): BigDecimal.ZERO).subtract(null!=obj.getPaPrimaPagadaAbr()? obj.getPaPrimaPagadaAbr():BigDecimal.ZERO));
						if(null !=  obj.getPaPrimaPagadaAbr()&& obj.getPaPrimaPagadaAbr().compareTo(BigDecimal.ZERO)!=0 &&  null != obj.getPrimaPagadaAbr()&& obj.getPrimaPagadaAbr().compareTo(BigDecimal.ZERO)!=0){
							obj2.setPorcentajeAbr(obj.getPrimaPagadaAbr().divide(obj.getPaPrimaPagadaAbr(), 2, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
						}
					}
				}
				if(5>=mesIncio && 5<=MesFin){
					if(crecimiento==1){
							a = a.add((null!=obj.getPrimaPagadaMay())?obj.getPrimaPagadaMay():BigDecimal.ZERO);
							b = b.add((null!=obj.getPaPrimaPagadaMay())?obj.getPaPrimaPagadaMay():BigDecimal.ZERO);
					}else{
						obj2.setImporteMay((null!=obj.getPrimaPagadaMay()?obj.getPrimaPagadaMay():BigDecimal.ZERO).subtract((null!=obj.getPaPrimaPagadaMay())?obj.getPaPrimaPagadaMay(): BigDecimal.ZERO));
						if(null!=obj.getPrimaPagadaMay() && obj.getPrimaPagadaMay().compareTo(BigDecimal.ZERO)!=0 && null!= obj.getPaPrimaPagadaMay() && obj.getPaPrimaPagadaMay().compareTo(BigDecimal.ZERO)!=0){	
							obj2.setPorcentajeMay(obj.getPrimaPagadaMay().divide(obj.getPaPrimaPagadaMay(), 2, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
						}
					}
				}
				if(6>=mesIncio && 6<=MesFin){
					if(crecimiento==1){
							a = a.add((null!=obj.getPrimaPagadaJun())?obj.getPrimaPagadaJun():BigDecimal.ZERO);
							b = b.add((null!=obj.getPaPrimaPagadaJun())?obj.getPaPrimaPagadaJun():BigDecimal.ZERO);
					}else{
						obj2.setImporteJun((null!=obj.getPrimaPagadaJun()?obj.getPrimaPagadaJun():BigDecimal.ZERO).subtract((null!=obj.getPaPrimaPagadaJun()?obj.getPaPrimaPagadaJun():BigDecimal.ZERO)));
						if(null!=obj.getPaPrimaPagadaJun() && obj.getPaPrimaPagadaJun().compareTo(BigDecimal.ZERO)!=0 && null!= obj.getPrimaPagadaJun() && obj.getPrimaPagadaJun().compareTo(BigDecimal.ZERO)!=0){
							obj2.setPorcentajeJun(obj.getPrimaPagadaJun().divide(obj.getPaPrimaPagadaJun(), 2, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
						}
					}
				}
				if(7>=mesIncio && 7<=MesFin){
					if(crecimiento==1){
							a = a.add((null!=obj.getPrimaPagadaJul())?obj.getPrimaPagadaJul():BigDecimal.ZERO);
							b = b.add((null!=obj.getPaPrimaPagadaJul())?obj.getPaPrimaPagadaJul():BigDecimal.ZERO);
					}else{
						obj2.setImporteJul((null!=obj.getPrimaPagadaJul()?obj.getPrimaPagadaJul():BigDecimal.ZERO).subtract((null!=obj.getPaPrimaPagadaJul()?obj.getPaPrimaPagadaJul():BigDecimal.ZERO)));
						if(null!= obj.getPrimaPagadaJul() && obj.getPrimaPagadaJul().compareTo(BigDecimal.ZERO)!=0 && null!= obj.getPaPrimaPagadaJul() && obj.getPaPrimaPagadaJul().compareTo(BigDecimal.ZERO)!=0){
							obj2.setPorcentajeJul(obj.getPrimaPagadaJul().divide(obj.getPaPrimaPagadaJul(), 2, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
						}
					}
				}
				if(8>=mesIncio && 8<=MesFin){
					if(crecimiento==1){
							a = a.add((null!=obj.getPrimaPagadaAgo())?obj.getPrimaPagadaAgo():BigDecimal.ZERO);
							b = b.add((null!=obj.getPaPrimaPagadaAgo())?obj.getPaPrimaPagadaAgo():BigDecimal.ZERO);
					}else{
						obj2.setImporteAgo((null!=obj.getPrimaPagadaAgo() ? obj.getPrimaPagadaAgo(): BigDecimal.ZERO).subtract((null!= obj.getPaPrimaPagadaAgo() ?obj.getPaPrimaPagadaAgo() : BigDecimal.ZERO)));
						if(null != obj.getPrimaPagadaAgo() && obj.getPrimaPagadaAgo().compareTo(BigDecimal.ZERO)!=0 && null!= obj.getPaPrimaPagadaAgo() && obj.getPaPrimaPagadaAgo().compareTo(BigDecimal.ZERO)!=0){
							obj2.setPorcentajeAgo(obj.getPrimaPagadaAgo().divide(obj.getPaPrimaPagadaAgo(), 2, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
						}
					}
				}
				if(9>=mesIncio && 9<=MesFin){
					if(crecimiento==1){
							a = a.add((null!=obj.getPrimaPagadaSep())?obj.getPrimaPagadaSep():BigDecimal.ZERO);
							b = b.add((null!=obj.getPaPrimaPagadaSep())?obj.getPaPrimaPagadaSep():BigDecimal.ZERO);
					}else{
						obj2.setImporteSep((null!=obj.getPrimaPagadaSep()?obj.getPrimaPagadaSep():BigDecimal.ZERO).subtract((null!=obj.getPaPrimaPagadaSep()?obj.getPaPrimaPagadaSep() : BigDecimal.ZERO)));
						if(null!= obj.getPrimaPagadaSep() && obj.getPrimaPagadaSep().compareTo(BigDecimal.ZERO)!=0 && null!= obj.getPaPrimaPagadaSep() && obj.getPaPrimaPagadaSep().compareTo(BigDecimal.ZERO)!=0){
							obj2.setPorcentajeSep(obj.getPrimaPagadaSep().divide(obj.getPaPrimaPagadaSep(), 2, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
						}
					}
				}
				if(10>=mesIncio && 10<=MesFin){
					if(crecimiento==1){
							a = a.add((null!=obj.getPrimaPagadaOct())?obj.getPrimaPagadaOct():BigDecimal.ZERO);
							b = b.add((null!=obj.getPaPrimaPagadaOct())?obj.getPaPrimaPagadaOct():BigDecimal.ZERO);
					}else{
						obj2.setImporteOct((null!=obj.getPrimaPagadaOct()?obj.getPrimaPagadaOct() : BigDecimal.ZERO).subtract(null!=obj.getPaPrimaPagadaOct()? obj.getPaPrimaPagadaOct() : BigDecimal.ZERO));
						if(null!= obj.getPrimaPagadaOct() && obj.getPrimaPagadaOct().compareTo(BigDecimal.ZERO)!=0 && null!=obj.getPaPrimaPagadaOct() && obj.getPaPrimaPagadaOct().compareTo(BigDecimal.ZERO)!=0 ){
							obj2.setPorcentajeOct(obj.getPrimaPagadaOct().divide(obj.getPaPrimaPagadaOct(), 2, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
						}
					}
				}
				if(11>=mesIncio && 11<=MesFin){
					if(crecimiento==1){
							a = a.add((null!=obj.getPrimaPagadaNov())?obj.getPrimaPagadaNov():BigDecimal.ZERO);
							b = b.add((null!=obj.getPaPrimaPagadaNov())?obj.getPaPrimaPagadaNov():BigDecimal.ZERO);
					}else{
						obj2.setImporteNov((null!=obj.getPrimaPagadaNov()?obj.getPrimaPagadaNov():BigDecimal.ZERO).subtract((null!=obj.getPaPrimaPagadaNov()?obj.getPaPrimaPagadaNov():BigDecimal.ZERO)));
						if(null!=obj.getPrimaPagadaNov() && obj.getPrimaPagadaNov().compareTo(BigDecimal.ZERO)!=0 && null!=obj.getPaPrimaPagadaNov() && obj.getPaPrimaPagadaNov().compareTo(BigDecimal.ZERO)!=0){
							obj2.setPorcentajeNov(obj.getPrimaPagadaNov().divide(obj.getPaPrimaPagadaNov(), 2, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
						}
					}
				}
				if(12>=mesIncio && 12<=MesFin){
					if(crecimiento==1){
							a = a.add((null!=obj.getPrimaPagadaDic())?obj.getPrimaPagadaDic():BigDecimal.ZERO);
							b = b.add((null!=obj.getPaPrimaPagadaDic())?obj.getPaPrimaPagadaDic():BigDecimal.ZERO);
					}else{
						obj2.setImporteDic((null!=obj.getPrimaPagadaDic()?obj.getPrimaPagadaDic():BigDecimal.ZERO).subtract(null!=obj.getPaPrimaPagadaDic()?obj.getPaPrimaPagadaDic():BigDecimal.ZERO));
						if(null != obj.getPrimaPagadaDic() && obj.getPrimaPagadaDic().compareTo(BigDecimal.ZERO)!=0 && null!=obj.getPaPrimaPagadaDic() && obj.getPaPrimaPagadaDic().compareTo(BigDecimal.ZERO)!=0){
							obj2.setPorcentajeDic(obj.getPrimaPagadaDic().divide(obj.getPaPrimaPagadaDic(), 2, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
						}
					}
				}
				obj2.setProducto(obj.getProducto());
			if(crecimiento==1){
				BigDecimal c = BigDecimal.ZERO;
				if(null!=a && null!=b){
					if (a.compareTo(c)!= 0 && b.compareTo(c)!= 0){
					 	c = (a.divide(b, 2, RoundingMode.HALF_UP)).subtract(BigDecimal.ONE);
					}
				}
				BigDecimal importeGlobal = a.subtract(b);
				obj2.setImporteGlobal(importeGlobal);
				obj2.setPorcentajeGlobal(c);
			}
			lista.add(obj2);
		}
		return lista;
	}
	
	public static Object regresaObjectEsNuloParametro(Object parametro,
			Object retorno) {
		if (parametro == null)
			return retorno;
		else
			return parametro;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteEstatusEntregaFacturaExcel(
				DatosAgenteEstatusEntregaFactura datos){
		List<DatosAgenteEstatusEntregaFactura> list = impresionesService.llenarDatosEstatusEntregaFactura(datos);
		if(list.isEmpty()){
			return null;
		}
		
		return impresionesService.getExcel(list,
				DatosAgenteEstatusEntregaFactura.PLANTILLA_NAME, null,ConstantesReporte.TIPO_XLSX);
	}
	
	
	public byte[] imprimirFormatoRecibo(HashMap<String, Object> parametros) {
		/*	
		byte[] reporte = null;
		JasperReport datosPagoTC = null;
		JRBeanCollectionDataSource datasource;// = new JRBeanCollectionDataSource(new ArrayList());
		
		try{			
			
				//System.out.println("LLEGANDO PARAMeTROS: "+lista.size());

				datosPagoTC = getJasperReport(UR_JRXML+"Datos_PagoTC.jrxml");
                List<String>lista=new ArrayList<String>();
				
				//System.out.println("paso elxml ");
				
				
				datasource = new JRBeanCollectionDataSource(lista);
				
				//System.out.println("paso el datasource ");
					
				//parametros = new HashMap<String, Object>();
			//	parametros.put("importe", "Valentin Santos");
				
				//System.out.println("paso la carga de parametros  ");
				
				Locale locale = new Locale("es","MX");
				if(locale != null){
					parametros.put(JRParameter.REPORT_LOCALE, locale);
				}
			 	parametros.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_AFIRME);
			 	//System.out.println("antes de genera el reporte ");	 	
			 	reporte = generarReporte(datosPagoTC, parametros, datasource);
			 	//reporte =JasperRunManager.runReportToPdf(datosPagoTC, parametros);
			 	//System.out.println("paso genera reporte ");
			 	//transporte.setByteArray(reporte);
			
			
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}
		return reporte;*/
		
		//TransporteImpresionDTO transporte = new TransporteImpresionDTO();
		byte [] reporte = null;
		//Map<String, Object> parametros = null;
		List<RecibosView> lista = new ArrayList<RecibosView>(1);
		RecibosView x = new RecibosView();
		            x.setComercio(" ");
		lista.add(x);
		//lista.add(RecibosView.getComercio());
		JasperReport reciboPortalTC = null;
		JRBeanCollectionDataSource datasource = null;
		try{			
			//lista = impresionesService.llenarDatosHonorarios(anio, mes, idAgente, anioMesFin);
			//if(lista!=null){
				reciboPortalTC = getJasperReport(UR_JRXML+"Datos_PagoTC.jrxml");	
				datasource = new JRBeanCollectionDataSource(lista);
				//parametros = new HashMap<String, Object>();
				Locale locale = new Locale("es","MX");
				if(locale != null){
					parametros.put(JRParameter.REPORT_LOCALE, locale);
				}
			 	parametros.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_AFIRME);		 	
			 	//reporte = generarReporte(honorarios, parametros, datasource);
			 	reporte = generarReportehtml(reciboPortalTC, parametros, datasource);
			 	//transporte.setByteArray(reporte);
			//}else{
				//return null;
			//}				
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}		
		return reporte;
	}
	
	public byte[] formatoReciboPdf(HashMap<String, Object> parametros) {
		
		byte [] reportePdf = null;

		List<RecibosView> lista = new ArrayList<RecibosView>(1);
		RecibosView x = new RecibosView();
		            x.setComercio(" ");
		lista.add(x);
		JasperReport reciboPortalTC = null;
		JRBeanCollectionDataSource datasource = null;
		try{			
				reciboPortalTC = getJasperReport(UR_JRXML+"Datos_PagoTC.jrxml");	
				datasource = new JRBeanCollectionDataSource(lista);
				Locale locale = new Locale("es","MX");
				if(locale != null){
					parametros.put(JRParameter.REPORT_LOCALE, locale);
				}
			 	parametros.put(KEY_LOGO_IMG, SistemaPersistencia.LOGO_AFIRME);		 	
			 	reportePdf = generarReporte(reciboPortalTC, parametros, datasource);
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}		
		return reportePdf;
	}
	
	private JasperReport getJasperReport(String plantilla){
		InputStream reportInputStream = this.getClass()
		.getResourceAsStream(plantilla);
		JasperReport jasperReport = null;
		try {
			jasperReport = JasperCompileManager
			.compileReport(reportInputStream);
		} catch (JRException e) {
			LOG.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		return jasperReport;
	}

	//@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	//@Schedule(minute="*/5", hour="*", persistent=false)
	public void actualizaXMLCLOB(){
		try{
			impresionRecibosService.getServicioImpresionED().getDigitalBill("", "LoadMassive");
		}catch(Exception ex){
			LOG.error("Error to save massive xmlClob",ex);
		}
	}

	//@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	//@Schedule(minute="*/15", hour="*", persistent=false)
	public void deleteMasivoXMLPendiente(){
		try{
			impresionRecibosService.getServicioImpresionED().getDigitalBill("", "DeleteMassive");
		}catch(Exception ex){
			LOG.error("Error to delete massive xml to server", ex);
		}
	}
	
	private void enviarNotificacionGenRepDetPrimas()
	{		
		String banderaNotificar = parametroGlobalService.obtenerValorPorIdParametro(
				ParametroGlobalService.AP_MIDAS,
				ParametroGlobalService.NOTIFICAR_REP_DETALLE_PRIMAS_GEN_MIDAS);
		
		LOG.info("banderaNotificar = " + banderaNotificar);
		
		if(banderaNotificar != null && !banderaNotificar.isEmpty() 
				&& banderaNotificar.equalsIgnoreCase(TRUE))
		{
			String keyMensajeNotificacion = "midas.agentes.reportes.detalleprimas.audit.correo.cuerpo";					
			
			Usuario usuarioActual = usuarioService.getUsuarioActual();
			Date fechaGeneracion = new Date();		
			
			List<String> direcciones = new ArrayList<String>();
			direcciones.add("hileab.juarez@afirme.com");
									
				mailService.sendMail(direcciones, Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, "midas.agentes.reportes.detalleprimas.audit.correo.titulo"), 
						Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, keyMensajeNotificacion,							
								usuarioActual.getNombreUsuario(),
								fechaGeneracion),
						null, 
						Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.agentes.reportes.detalleprimas.audit.correo.titulo"),
						Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.agentes.reportes.detalleprimas.audit.correo.saludo"));
			
		}						
	}
	
	private ExcepcionSuscripcionNegocioAutosService excepcionAutoService = null;
	
	private ImpresionesService impresionesService = null;
	
	private EntidadService entidadService = null;
	
	
	@EJB
	private MidasBaseReporteService baseReporteService;
	
	@EJB
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	@EJB
	private ImpresionRecibosService impresionRecibosService;
	
	@EJB
	private ParametroGlobalService parametroGlobalService;
	
	@EJB
	private MailService mailService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;	
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	
	
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	@EJB
	public void setImpresionesService(ImpresionesService impresionesService) {
		this.impresionesService = impresionesService;
	}

	@EJB
	public void setExcepcionAutoService(
			ExcepcionSuscripcionNegocioAutosService excepcionAutoService) {
		this.excepcionAutoService = excepcionAutoService;
	}
	
	
	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Override
	public TransporteImpresionDTO imprimirReporteResumenDetalleBonosExcel(
			Long arg0) {
		return null;
	}
	@EJB
	OrdenPagosService ordenPagosService;
	
	@EJB
	public void setParametroGeneralFacade(ParametroGeneralFacadeRemote parametroGeneralFacade){
		this.parametroGeneralFacade = parametroGeneralFacade;
	}


	@Override
	public TransporteImpresionDTO imprimirReporteDetallePrimasComp(ReporteFiltrosDTO arg0) {
		Map<String,Object> dates = new HashMap<String, Object>();
		Map<String,Object> params = new HashMap<String, Object>();
		List<ReporteDetallePrimas> lista  = new ArrayList<ReporteDetallePrimas>();	
		try{
			dates=liquiuLiquidacionCompensacionesService.getReporteDetallePrimas(arg0);
			params.put("P_IMAGE_LOGO",SistemaPersistencia.AFIRME_SEGUROS);
			params.put("NUMERO_AGENTE",dates.get("pIdentificadorBenId")+"");
			params.put("NOMBRE_AGENTE", dates.get("pNomBeneficiario"));
			params.put("CENTRO_OPERACION", dates.get("pCentroOperacion"));
			params.put("GERENCIA", dates.get("pGerencia"));
			params.put("ESTATUS_AGENTE",dates.get("estatusAgente"));
			params.put("RFC", dates.get("pRfcBeneficiario"));
			params.put("DOMICILIO", dates.get("pDomicilio"));
			params.put("EJECUTIVO", dates.get("pEjectutivo"));
			params.put("PROMOTORIA", dates.get("pPromotoria"));
			params.put("NUMERO_CEDULA", dates.get("pNumCedula"));
			params.put("VENCIMIENTO_CEDULA", dates.get("pVenceCedula"));
			lista=(ArrayList<ReporteDetallePrimas>) dates.get("pCursor");
			return impresionesService.getExcel(lista,"midas.impresion.compensacionadicionales.reporteDetallePrimas",params,ConstantesReporte.TIPO_XLS);
		}catch(Exception ex){
			ex.getStackTrace();
		}
		return null;
	}
	
	public String getValorParameter(BigDecimal idToGrupoParamGen, BigDecimal codigoParma) {		
		String parameter = parametroGeneralFacade.findById(new ParametroGeneralId(idToGrupoParamGen, codigoParma), true).getValor();		
		if(parameter!=null){
			return parameter;
		}
		return null;
	}
	
	public void actualizarparametroReporte(String estatus) {
		impresionesService.actualizarEstatusReporteDetPrimas(estatus);
	}

}