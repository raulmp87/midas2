/**
 * 
 */
package mx.com.afirme.midas2.service.impl.siniestros.catalogo.solicitudsuspensionservicio;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudsuspensionservicio.SolicitudSuspensionServicio;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.solicitudsuspensionservicio.CatSolicitudSuspensionServicioService;

import org.apache.log4j.Logger;




/**
 * @author simavera
 *
 */
@Stateless
public class CatSolicitudSuspensionServicioServiceImpl implements CatSolicitudSuspensionServicioService{

	@EJB
    private CatalogoSiniestroService catalogoService;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	@Resource
	private Validator validator;
	
	private static Logger log = Logger.getLogger("CatSolicitudSuspensionServicioServiceImpl");
    @Override
    public List<SolicitudSuspensionServicio> buscar(SolicitudSuspensionServicioFiltro filtro) {
          List<SolicitudSuspensionServicio> listSuspensionServicios = catalogoService.buscar(SolicitudSuspensionServicio.class, filtro);
          return listSuspensionServicios;
    }

    public SolicitudSuspensionServicio getById( Integer idSuspensionServicio ){
    	SolicitudSuspensionServicio solicitudSuspensionServicio = null;    	
    	try{
	    	solicitudSuspensionServicio = catalogoService.obtener( SolicitudSuspensionServicio.class, idSuspensionServicio );	    		    	
    	}catch( Exception ex ){
    		log.error("Ocurri un error en la busqueda, ERROR::  " + ex.getMessage() );
    	}
    	return solicitudSuspensionServicio;
    }

    public void save( SolicitudSuspensionServicio solicitudSuspensionServicio ){    
    	Set<ConstraintViolation<SolicitudSuspensionServicio>> constraintViolations = 
    		validator.validate(solicitudSuspensionServicio);
		if(constraintViolations.size() > 0) {
			throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(constraintViolations));
		}		  
    	Long id = catalogoService.salvarObtenerId( solicitudSuspensionServicio );
    	solicitudSuspensionServicio.setId(id);
    }    

    @Override
    public String findCurrentUser(){
    	return String.valueOf(  usuarioService.getUsuarioActual().getId() );
    }
    
    
    public CatalogoSiniestroService getCatalogoService() {
          return catalogoService;
    }

    
    public void setCatalogoService(CatalogoSiniestroService catalogoService) {
          this.catalogoService = catalogoService;
    }

	/**
	 * @return the entidadService
	 */
	public EntidadService getEntidadService() {
		return entidadService;
	}

	/**
	 * @param entidadService the entidadService to set
	 */
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	/**
	 * @return the usuarioService
	 */
	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	/**
	 * @param usuarioService the usuarioService to set
	 */
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
}
