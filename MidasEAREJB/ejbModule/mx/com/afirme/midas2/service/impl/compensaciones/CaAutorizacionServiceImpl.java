/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaAutorizacionDao;
import mx.com.afirme.midas2.domain.compensaciones.CaAutorizacion;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.service.compensaciones.CaAutorizacionService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;

import org.apache.log4j.Logger;

@Stateless

public class CaAutorizacionServiceImpl  implements CaAutorizacionService {

	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	
	@EJB
	private CaAutorizacionDao caAutorizacionDao;
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaAutorizacionServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaAutorizacion entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaAutorizacion entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaAutorizacion entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaAutorizacion 	::		CaAutorizacionServiceImpl	::	save	::	INICIO	::	");
	        try {
//            entityManager.persist(entity);
	        	caAutorizacionDao.save(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaAutorizacion 	::		CaAutorizacionServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaAutorizacion 	::		CaAutorizacionServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaAutorizacion entity.
	  @param entity CaAutorizacion entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaAutorizacion entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaAutorizacion 	::		CaAutorizacionServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaAutorizacion.class, entity.getId());
//            entityManager.remove(entity);
        	caAutorizacionDao.delete(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaAutorizacion 	::		CaAutorizacionServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaAutorizacion 	::		CaAutorizacionServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaAutorizacion entity and return it or a copy of it to the sender. 
	 A copy of the CaAutorizacion entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaAutorizacion entity to update
	 @return CaAutorizacion the persisted CaAutorizacion entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaAutorizacion update(CaAutorizacion entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaAutorizacion 	::		CaAutorizacionServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaAutorizacion result = caAutorizacionDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaAutorizacion 	::		CaAutorizacionServiceImpl	::	update	::	FIN	::	");
	        return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaAutorizacion 	::		CaAutorizacionServiceImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaAutorizacion findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaAutorizacionServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaAutorizacion instance = caAutorizacionDao.findById(id);//entityManager.find(CaAutorizacion.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaAutorizacionServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaAutorizacionServiceImpl	::	findById	::	ERROR	::	",re);
	            throw re;
        }
    }    
    

/**
	 * Find all CaAutorizacion entities with a specific property value.  
	 
	  @param propertyName the name of the CaAutorizacion property to query
	  @param value the property value to match
	  	  @return List<CaAutorizacion> found by query
	 */
    public List<CaAutorizacion> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaAutorizacionServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaAutorizacion model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
				
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaAutorizacionServiceImpl	::	findByProperty	::	FIN	::	");
			return caAutorizacionDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaAutorizacionServiceImpl	::	findByProperty	::	ERROR	::	",re);
			throw re;
		}
	}
    public List<CaAutorizacion> findByCompensacionid(Long idCompensacion
	){
    	LOGGER.info("	::	[INF]	::	Buscando por CompensacionId 	::		CaAutorizacionServiceImpl	::	findByCompensacionid	::	INICIO	::	");    	
        try {
        LOGGER.info("	::	[INF]	::	Se busco por CompensacionId 	::		CaAutorizacionServiceImpl	::	findByCompensacionid	::	FIN	::	");
        return caAutorizacionDao.findByCompensacionid(idCompensacion);
    } catch (RuntimeException re) {
    	LOGGER.error("	::	[ERR]	::	Error al buscar por CompensacionId 	::		CaAutorizacionServiceImpl	::	findByCompensacionid	::	ERROR	::	",re);
            throw re;
    }
    }
	public List<CaAutorizacion> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaAutorizacion> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaAutorizacion> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaAutorizacion entities.
	  	  @return List<CaAutorizacion> all CaAutorizacion entities
	 */
	public List<CaAutorizacion> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaAutorizacionServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaAutorizacion model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaAutorizacionServiceImpl	::	findAll	::	FIN	::	");
			return caAutorizacionDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaAutorizacionServiceImpl	::	findAll	::	ERROR	::	",re);
			throw re;
		}
	}
	public List<CaAutorizacion> finByCompensidAndTipoautoid(Long compensId, Long tipoAutoId){
		LOGGER.info("	::	[INF]	::	Buscando por CompensId & TipoAutorizacionId 	::		CaAutorizacionServiceImpl	::	finByCompensidAndTipoautoid	::	INICIO	::	");
		try {
		LOGGER.info("	::	[INF]	::	Se busco por CompensId & TipoAutorizacionId 	::		CaAutorizacionServiceImpl	::	finByCompensidAndTipoautoid	::	FIN	::	");
		return caAutorizacionDao.finByCompensidAndTipoautoid(compensId, tipoAutoId);
	} catch (RuntimeException re) {
		LOGGER.error("	::	[ERR]	::	Error al buscar por CompensId & TipoAutorizacionId 	::		CaAutorizacionServiceImpl	::	finByCompensidAndTipoautoid	::	ERROR	::	",re);
		throw re;
		}
	}
	public List<CaAutorizacion> finByCompensidTiposautosid(Long compensId, Long ...tipos){
		LOGGER.info("	::	[INF]	::	Buscando por CompensId & TiposAutorizacionId 	::		CaAutorizacionServiceImpl	::	finByCompensidTiposautosid	::	INICIO	::	");
		try {
		LOGGER.info("	::	[INF]	::	Se busco por CompensId & TiposAutorizacionId 	::		CaAutorizacionServiceImpl	::	finByCompensidTiposautosid	::	FIN	::	");
		return caAutorizacionDao.finByCompensidTiposautosid(compensId, tipos);
	} catch (RuntimeException re) {
		LOGGER.error("	::	[ERR]	::	Error al buscar por CompensId & TiposAutorizacionId 	::		CaAutorizacionServiceImpl	::	finByCompensidTiposautosid	::	ERROR	::	",re);
		throw re;
		}
	}
	
	public void cargarAutorizaciones(CaCompensacion caCompensacion, CompensacionesDTO compensacionesDTO){
		LOGGER.info(">> cargarAutorizaciones()");
		List<CaAutorizacion> caAutorizacionesVer = this.findByCompensacionid(caCompensacion.getId());
		
		List<Long> listCaTipoAutorizacion = new ArrayList<Long>();
		for (CaAutorizacion caAutorizacion : caAutorizacionesVer) {
			if(caAutorizacion.getCaEstatus().getId().longValue() == ConstantesCompensacionesAdicionales.ESTATUS_APROBADO){
				listCaTipoAutorizacion.add(caAutorizacion.getCaTipoAutorizacion().getId());
			}
		}
		
		compensacionesDTO.setListTipoAutorizacionId(listCaTipoAutorizacion);
		LOGGER.info("<< cargarAutorizaciones()");
	}
	
}