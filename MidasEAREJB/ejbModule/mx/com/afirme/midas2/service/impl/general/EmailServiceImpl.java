package mx.com.afirme.midas2.service.impl.general;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.general.Email;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.general.EmailService;

@Stateless
public class EmailServiceImpl implements EmailService{
	private EntidadService entidadService;

	@Override
	public List<Email> findAll() {
		List<Email> retorno = entidadService.findAll(Email.class);
		return retorno;
	}

	@Override
	public Email findById(Long arg0) {
		Email retorno = new Email();
		if(arg0 != null && arg0 >= 0){
			retorno = entidadService.findById(Email.class, arg0);
		}
		return retorno;
	}

	@Override
	public List<Email> findByProperty(String arg0, Object arg1) {
		List<Email> emailList = new ArrayList<Email>();
		if(arg0 != null && !arg0.equals("") && arg1 != null){
			emailList = entidadService.findByProperty(Email.class, arg0, arg1);
		}
		return emailList;
	}

	@Override
	public List<Email> findByStatus(boolean arg0) {
		List<Email> email = entidadService.findByProperty(Email.class, "estatus", arg0?0:1);
		return email;
	}

	@Override
	public Email saveObject(Email arg0) {
		arg0.setIdEmail(arg0.getIdEmail());
		if(arg0 != null && arg0.getIdEmail() >= 0){
			arg0.setIdEmail((Long)entidadService.saveAndGetId(arg0));
		}
		return arg0;
	}
	
	private boolean validateAttributes(Email arg0){
		boolean retorno = true;
		if(retorno){
			retorno = arg0.getEmail() != null && !arg0.getEmail().equals("");
		}
		return retorno;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public Email findByDesc(String arg0) {
		Email retorno = new Email();
		if(arg0 != null && !arg0.equals("")){
			List<Email> list = entidadService.findByProperty(Email.class, "email", arg0);
			if(list.size() > 0){
				retorno = list.get(0);
			}else{
				retorno.setIdEmail(new Long(0));
				retorno.setEmail(arg0);
			}
		}
		return retorno;
	}
}
