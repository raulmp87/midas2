package mx.com.afirme.midas2.service.impl.reportes.historial.cobranza;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.reportes.historial.cobranza.ReporteHistorialCobranzaDAO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.ReporteHistorialCobranzaDTO;
import mx.com.afirme.midas2.service.reportes.historial.cobranza.ReporteHistorialCobranzaService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

@Stateless
public class ReporteHistorialCobranzaServiceImpl implements Serializable, ReporteHistorialCobranzaService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ReporteHistorialCobranzaDAO dao;

	@EJB
	public void setDao(ReporteHistorialCobranzaDAO dao) {
		this.dao = dao;
	}

	@Override
	public List<ReporteHistorialCobranzaDTO> consultarHistCobranzaPorAgente(
			Long idAgente) {
		List<ReporteHistorialCobranzaDTO> resultados = new ArrayList<ReporteHistorialCobranzaDTO>();
		if(idAgente != null){
			resultados = dao.consultarHistPorAgente(idAgente);
		}
		return resultados;
	}

	@Override
	public TransporteImpresionDTO generarReporteCobranza(
			List<ReporteHistorialCobranzaDTO> dataSource, String nombreLibro)
			throws IllegalArgumentException, FileNotFoundException,
			InvalidFormatException, IllegalAccessException,
			InvocationTargetException, IOException {
		ExcelExporter exporter = new ExcelExporter(ReporteHistorialCobranzaDTO.class);
		return exporter.exportXLS(dataSource, nombreLibro);
	}

}
