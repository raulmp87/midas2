package mx.com.afirme.midas2.service.impl.siniestros.expedientejuridico;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.dao.siniestros.expedientesjuridicos.ExpedienteJuridicoDAO;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCalculoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.ClaveSubCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.siniestros.expedientejuridico.ComentarioJuridico;
import mx.com.afirme.midas2.domain.siniestros.expedientejuridico.ExpedienteJuridico;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.dto.expedientejuridico.ExpedienteJuridicoDTO;
import mx.com.afirme.midas2.dto.expedientejuridico.ExpedienteJuridicoExportableDTO;
import mx.com.afirme.midas2.dto.siniestros.AfectacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.ReporteSiniestroDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.expedientejuridico.ExpedienteJuridicoService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionCiaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionCruceroJuridicaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionDeducibleService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionProveedorService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSalvamentoService;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.StringUtil;



/**
 * invocar expedienteJuridicoDAO.buscarExpedientes
 * y regresr la lista
 * @author Israel
 * @version 1.0
 * @created 06-ago.-2015 10:30:09 a. m.
 */
@Stateless
public class ExpedienteJuridicoServiceImpl implements ExpedienteJuridicoService{

	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private ExpedienteJuridicoDAO expedienteJuridicoDAO ; 
	
	@EJB
	private ListadoService listadoService;
	
	@EJB
	private ReporteCabinaService reporteCabinaService;
	
//	@EJB
//	private PolizaSiniestroService polizaSiniestroService;
	
	@EJB
    private ClienteFacadeRemote clienteFacadeRemote;
	
	@EJB
	private EstimacionCoberturaSiniestroService estimacionService;
	
	@EJB
	private MovimientoSiniestroService movimientoService;
	
	@EJB
	private  RecuperacionProveedorService recuperacionProveedorService;
	
	@EJB
	private RecuperacionCiaService recuperacionCiaService;
	
	@EJB
	private RecuperacionDeducibleService recuperacionDeducibleService;
	
	@EJB
	private RecuperacionCruceroJuridicaService recuperacionCruceroService;
	
	@EJB
	private RecuperacionSalvamentoService recuperacionSalvamentoService;
	
	
	
	/**
	 * Validar si se tiene valor en comentarioProveedor y generar un objeto
	 * comentarioJuridico y realizar la misma operacion para comentarioAfirme.
	 * 
	 * Llenar los campos de control
	 * 
	 * Agregar lo(s) objetos creado(s) a la lista de comentarios de expedienteJuridico
	 * 
	 * @param expedienteJuridico
	 * @param comentarioAfirme
	 * @param comentarioProveedor
	 */
	@Override
	public void crearComentarios(ExpedienteJuridico expedienteJuridico, String comentarioAfirme, String comentarioProveedor){
		String currentUser = String.valueOf(usuarioService.getUsuarioActual().getNombreUsuario());
		if(!StringUtil.isEmpty(comentarioAfirme)){
			ComentarioJuridico comentarioJuridicoAfirme = new ComentarioJuridico();
			comentarioJuridicoAfirme.setComentario(comentarioAfirme);
			comentarioJuridicoAfirme.setTipo(ComentarioJuridico.Tipo.AFIRME.getValue());
			comentarioJuridicoAfirme.setCodigoUsuarioCreacion(currentUser);
			comentarioJuridicoAfirme.setFechaCreacion(new Date());
			comentarioJuridicoAfirme.setCodigoUsuarioModificacion(currentUser);
			comentarioJuridicoAfirme.setExpedienteJuridico(expedienteJuridico);
			this.entidadService.save(comentarioJuridicoAfirme);
		}
		if(!StringUtil.isEmpty(comentarioProveedor)){
			ComentarioJuridico comentarioJuridicoProveedor = new ComentarioJuridico();
			comentarioJuridicoProveedor.setComentario(comentarioProveedor);
			comentarioJuridicoProveedor.setTipo(ComentarioJuridico.Tipo.PROVEEDOR.getValue());
			comentarioJuridicoProveedor.setCodigoUsuarioCreacion(currentUser);
			comentarioJuridicoProveedor.setFechaCreacion(new Date());
			comentarioJuridicoProveedor.setCodigoUsuarioModificacion(currentUser);
			comentarioJuridicoProveedor.setExpedienteJuridico(expedienteJuridico);
			this.entidadService.save(comentarioJuridicoProveedor);
		}
	}

	/**
	 * Crear una lista de expotables por cada registro
	 * 
	 * @param expedientes
	 */
	@Override
	public List<ExpedienteJuridicoExportableDTO> crearExpedientesExportables(List<ExpedienteJuridico> expedientes){
		List<ExpedienteJuridicoExportableDTO> exportablesDTO = new ArrayList<ExpedienteJuridicoExportableDTO>();
		expedientes = agregarDescripciones(expedientes);
		for(ExpedienteJuridico expediente : expedientes){
			ExpedienteJuridicoExportableDTO dto = new ExpedienteJuridicoExportableDTO();
			ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
			String noSiniestro = expediente.getNumeroSiniestro();
			String[] siniestroArray = noSiniestro.split("-");
			String anio = siniestroArray[2].trim();
			reporteSiniestroDTO.setNumeroSiniestro(noSiniestro);
			List<ReporteSiniestroDTO> reportesSiniestro = this.reporteCabinaService.buscarReportes(reporteSiniestroDTO);
			ReporteCabina reporteCabina = null;
			if(reportesSiniestro != null && reportesSiniestro.size()>0){
				reporteCabina = this.entidadService.findById(ReporteCabina.class, reportesSiniestro.get(0).getReporteCabinaId());
			}
			dto.setNoSinietro(expediente.getNumeroSiniestro());
			dto.setAnioSiniestro(anio);
			dto.setReporteCabina(reporteCabina.getNumeroReporte());
			dto.setPoliza(reporteCabina.getPoliza().getNumeroPolizaFormateada());
			dto.setInciso(reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getDescripcionFinal());
			
			
			ClienteGenericoDTO clienteGenericoDTO = new ClienteGenericoDTO();
			
			clienteGenericoDTO.setIdCliente(new BigDecimal(reporteCabina.getPersonaContratanteId()));
			
			try {
				clienteGenericoDTO = clienteFacadeRemote.loadById(clienteGenericoDTO);
			} catch (Exception e) {
				e.printStackTrace();
			}
			dto.setContratante(clienteGenericoDTO.getNombreCompleto());
			dto.setTelContratante(clienteGenericoDTO.getTelefono());
			dto.setConductor(reporteCabina.getPersonaConductor());
			dto.setFechaOcurrido(reporteCabina.getFechaOcurrido());
			dto.setFechaReportado(reporteCabina.getFechaHoraReporte());
			dto.setFechaTurnoJuridico(expediente.getFechaTurno());
			dto.settVehicAseg(reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getDescripcionFinal());
			dto.setModelo(reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getModeloVehiculo().toString());
			dto.setSerie(reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getNumeroSerie());
			dto.setPlacas(reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getPlaca());
			
			Map<String, String> catCausaSiniestro = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CAUSA_SINIESTRO);
			dto.setCausaSin(catCausaSiniestro.get(reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getCausaSiniestro()));
			
			Map<String, String> catTipoResponsabilidad = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RESPONSABILIDAD);
			dto.setResponsabilidad(catTipoResponsabilidad.get(reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getTipoResponsabilidad()));
			
			Map<String, String> catTerminoSiniestro = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);
			dto.setTerminoAjuste(catTerminoSiniestro.get(reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getTipoResponsabilidad()));
			
			dto.setDescripcionSiniestro(reporteCabina.getDeclaracionTexto());
			dto.setEstatusJuridico(expediente.getEstatusJuridicoDesc());
			
			dto.setComentariosJuridico(this.concatenaComentarios(expediente.getComentarios()));
			
			
			
			//Se obtienen las estimaciones
			List<AfectacionCoberturaSiniestroDTO> coberturasDTO =  this.estimacionService.obtenerCoberturasAfectacion(reporteCabina.getId());
			for(AfectacionCoberturaSiniestroDTO coberturaDTO : coberturasDTO){
				Long reporteCabinaId = reporteCabina.getId();
				Long coberturaId = coberturaDTO.getCoberturaReporteCabina().getId();
				String cveTipoCalculo = coberturaDTO.getCoberturaReporteCabina().getClaveTipoCalculo();
				ConfiguracionCalculoCoberturaSiniestro configuracion = coberturaDTO.getConfiguracionCalculoCobertura(); 
				String cveSubCalculo = (configuracion!=null)?configuracion.getCveSubTipoCalculoCobertura():null;
				
				BigDecimal montoEstimacion = this.movimientoService.obtenerMontoEstimacionCobertura(coberturaId, cveSubCalculo);					
				BigDecimal pago = this.movimientoService.obtenerMontoPagosCobertura(coberturaId, cveSubCalculo);
				
				if(EnumUtil.equalsValue(ClaveTipoCalculo.DANIOS_MATERIALES, cveTipoCalculo)){					
					dto.setEstFinalDM(montoEstimacion.toString());					
					dto.setPagosDM(pago.toString());
				}else if(EnumUtil.equalsValue(ClaveTipoCalculo.RESPONSABILIDAD_CIVIL, cveTipoCalculo) && EnumUtil.equalsValue(ClaveSubCalculo.RC_VEHICULO, cveSubCalculo)){
					dto.setEstFinalRCVehiculos(montoEstimacion.toString());					
					dto.setPagosRCVehiculo(pago.toString());
					dto.settVehiculoTercero(this.concatenaNombreVerhiculoTercero(coberturaDTO.getCoberturaReporteCabina()));
					dto.setCompaniaTerceroResp(this.concatenaCompaniasTerceroVehiculos(coberturaDTO.getCoberturaReporteCabina()));
				}else if(EnumUtil.equalsValue(ClaveTipoCalculo.RESPONSABILIDAD_CIVIL, cveTipoCalculo) && EnumUtil.equalsValue(ClaveSubCalculo.RC_PERSONA, cveSubCalculo)){
					dto.setEstFinalRCPersonas(montoEstimacion.toString());			
					dto.setPagosRCPersonas(pago.toString());
				}else if(EnumUtil.equalsValue(ClaveTipoCalculo.RESPONSABILIDAD_CIVIL, cveTipoCalculo) && EnumUtil.equalsValue(ClaveSubCalculo.RC_BIENES, cveSubCalculo)){
					dto.setEstFinalRCBienes(montoEstimacion.toString());
					dto.setPagosRCBienes(pago.toString());
				}else if(EnumUtil.equalsValue(ClaveTipoCalculo.GASTOS_MEDICOS, cveTipoCalculo) ){				
					dto.setEstFinalGM(montoEstimacion.toString());
					dto.setPagosGM(pago.toString());
				}else if(EnumUtil.equalsValue(ClaveTipoCalculo.ROBO_TOTAL, cveTipoCalculo) ){					
					dto.setEstFinalRT(montoEstimacion.toString());
					dto.setPagosRT(pago.toString());
				}else if(EnumUtil.equalsValue(ClaveTipoCalculo.RESP_CIVIL_VIAJERO, cveTipoCalculo) ){
					dto.setEstFinalRCViajero(montoEstimacion.toString());
					dto.setPagosRCViajero(pago.toString());
				}else if(EnumUtil.equalsValue(ClaveTipoCalculo.ACCIDENTES_AUT_CONDUCTOR, cveTipoCalculo)){			
					dto.setEstFinalAAC(montoEstimacion.toString());
					dto.setPagosAAC(pago.toString());
				}else if(EnumUtil.equalsValue(ClaveTipoCalculo.ADAPTACIONES_CONVERSIONES, cveTipoCalculo)){					
					dto.setEstFinalAYC(montoEstimacion.toString());
					dto.setPagosAYC(pago.toString());
				}else if(EnumUtil.equalsValue(ClaveTipoCalculo.EQUIPO_ESPECIAL, cveTipoCalculo) ){					
					dto.setEstFinalEE(montoEstimacion.toString());
					dto.setPagosEE(pago.toString());
				}
				
				BigDecimal montoRecuperacionProveedor = this.recuperacionProveedorService.montoTotalRecuperacionProveedor(reporteCabina.getSiniestroCabina().getId(), EstatusRecuperacion.RECUPERADO);
				dto.setRecuperacionesProveedor(montoRecuperacionProveedor.toString());	
				
				BigDecimal montoRecuperacionCiaRec = this.recuperacionCiaService.montoTotalRecuperacionCia(reporteCabina.getSiniestroCabina().getId(), EstatusRecuperacion.RECUPERADO);
				dto.setRecuperacionesCompanias(montoRecuperacionCiaRec.toString());
				
				BigDecimal montoRecuperacionCiaPend = this.recuperacionCiaService.montoTotalRecuperacionCia(reporteCabina.getSiniestroCabina().getId(), EstatusRecuperacion.RECUPERADO);
				dto.setRecuperacionesPendCompanias(montoRecuperacionCiaPend.toString());
				
				BigDecimal montoRecuperacionDeducible = this.recuperacionDeducibleService.obtenerMontoRecuperacionDeducible(reporteCabina.getSiniestroCabina().getId(), EstatusRecuperacion.RECUPERADO);
				dto.setRecuperacionesPendCompanias(montoRecuperacionDeducible.toString());
				
				BigDecimal montoRecuperacionCrucero = this.recuperacionCruceroService.montoTotalRecuperacionCrucero(reporteCabina.getSiniestroCabina().getId(), EstatusRecuperacion.RECUPERADO);
				dto.setRecuperacionesPendCompanias(montoRecuperacionCrucero.toString());
				
				BigDecimal montoRecuperacionSalvamento = this.recuperacionSalvamentoService.obtenerMontoRecuperacionSalvamento(reporteCabina.getSiniestroCabina().getId(), EstatusRecuperacion.RECUPERADO);
				dto.setRecuperacionesSalvamento(montoRecuperacionSalvamento.toString());
				
				BigDecimal montoIvasRecuperacionSalvamento = this.recuperacionSalvamentoService.obtenerIvaRecuperacionSalvamento(reporteCabina.getSiniestroCabina().getId(), EstatusRecuperacion.RECUPERADO);
				dto.setImpIngSalvam(montoIvasRecuperacionSalvamento.toString());
				
				dto.setCiudad(reporteCabina.getLugarOcurrido().getCiudad().getDescripcion());
				dto.setEstado(reporteCabina.getLugarOcurrido().getEstado().getDescripcion());
				dto.setAjustador(reporteCabina.getAjustador().getNombrePersonaAjustador());
				dto.setMotivoTurno(expediente.getMotivoTurnoDesc());
				dto.setNumExpedienteExterno(expediente.getExpedienteProveedor());
				ServicioSiniestro abogado = expediente.getAbogado(); 
				dto.setTipoAbogado((abogado!=null)?abogado.getAmbitoDesc():null);
				dto.setCveAbogado((abogado!=null)?abogado.getId().toString():null );
				dto.setFechaConclusionLegal(expediente.getFechaConclusionLegal());
				dto.setFechaConclusionAfirme(expediente.getFechaConclusionAfirme());
				dto.setTipoConclusion(expediente.getTipoConclusionDesc());
				dto.setbVehicDetenido(expediente.getVehiculoDetenidoDesc());
				dto.setEstatusVehiculo(expediente.getEstatusVehiculoDesc());
				dto.setFechaLiberacionVehic(expediente.getFechaLiberacionVehiculo());
//				dto.setImpIngSalvam();
			}
			exportablesDTO.add(dto);
		}
		
		return exportablesDTO;
	}
	
	
	private String concatenaNombreVerhiculoTercero(CoberturaReporteCabina coberturaReporteCabina) {
		StringBuilder textoConcat = new StringBuilder();
		for(EstimacionCoberturaReporteCabina estimacion :  coberturaReporteCabina.getlEstimacionCoberturaReporteCabina()){
			if(estimacion instanceof TerceroRCVehiculo){
				TerceroRCVehiculo terceroRCVehiculo = (TerceroRCVehiculo)estimacion; 
				textoConcat.append(terceroRCVehiculo.getMarca()+",");
			}
		}
		return textoConcat.toString();
	}
	
	private String concatenaCompaniasTerceroVehiculos(CoberturaReporteCabina coberturaReporteCabina) {
		StringBuilder textoConcat = new StringBuilder();
		for(EstimacionCoberturaReporteCabina estimacion :  coberturaReporteCabina.getlEstimacionCoberturaReporteCabina()){
			if(estimacion instanceof TerceroRCVehiculo){
				TerceroRCVehiculo terceroRCVehiculo = (TerceroRCVehiculo)estimacion; 
				PrestadorServicio compania = terceroRCVehiculo.getCompaniaSegurosTercero();
				if(compania!=null){
					textoConcat.append(compania.getNombrePersona()+",");
				}
			}
		}
		return textoConcat.toString();
	}

	private String concatenaComentarios(List<ComentarioJuridico> comentarios){
		StringBuilder comentariosConcat = new StringBuilder();
		if(comentarios!=null){
			for(ComentarioJuridico comentario : comentarios){
				comentariosConcat.append(comentario.getTipo()+" - "+comentario.getComentario()+",");
			}
		}
		return comentariosConcat.toString();
	}

	/**
	 * 
	 * @param expedienteJuridicoDTO
	 */
	private List<ExpedienteJuridico> agregarDescripciones(List<ExpedienteJuridico>  expedientes){
		Map<String,String> motivoTurno = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.JURIDICO_MOTIVO_TURNO);
		Map<String,String> estatusVehiculo = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.JURIDICO_ESTATUS_VEHICULO);
		Map<String,String> tipoConclusion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.JURIDICO_TIPO_CONCLUSION);
		Map<String,String> estatusJuridico = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_JURIDICO);
		for(ExpedienteJuridico expediente : expedientes){
			expediente.setMotivoTurnoDesc(motivoTurno.get(expediente.getMotivoTurno()));
			expediente.setVehiculoDetenidoDesc((expediente.getVehiculoDetenido().intValue()==1)?"SI":"NO");
			if(expediente.getEstatusVehiculo()!=null){
				expediente.setEstatusVehiculoDesc(estatusVehiculo.get(expediente.getEstatusVehiculo()));
			}
			if(expediente.getAbogado()!=null){
				switch (expediente.getAbogado().getAmbitoPrestadorServicio().intValue()) {
				case 1:
					expediente.getAbogado().setAmbitoDesc(ServicioSiniestro.Ambito.INTERNO.toString());
				break;
				
				case 2:
					expediente.getAbogado().setAmbitoDesc(ServicioSiniestro.Ambito.EXTERNO.toString());
				break;
	
				default:
					break;
			}
			}
			if(expediente.getTipoConclusion()!=null){
				expediente.setTipoConclusionDesc(tipoConclusion.get(expediente.getTipoConclusion()));
			}
			if(expediente.getEstatusJuridico()!=null){
				expediente.setEstatusJuridicoDesc(estatusJuridico.get(expediente.getEstatusJuridico()));
			}
			
		}
		return expedientes;
	}

	/**
	 * invocar expedienteJuridicoDAO.buscarExpedientes
	 * y retornar la respuesta
	 * 
	 * @param expedienteJuridicoDTO
	 */
	@Override
	public List<ExpedienteJuridico> buscarExpedientes(ExpedienteJuridicoDTO expedienteJuridicoDTO){
		List<ExpedienteJuridico> expedientes = this.expedienteJuridicoDAO.buscarExpedientes(expedienteJuridicoDTO); 
		return this.agregarDescripciones(expedientes);
	}

	/**
	 * acompletar los campos de control
	 * invocarCrearComentarios
	 * y ejecutar el entidadService.save
	 * 
	 * @param expedienteJuridico
	 * @param comentarioAfirme
	 * @param comentarioProveedor
	 */
	@Override
	public ExpedienteJuridico guardar(ExpedienteJuridico expedienteJuridico, String comentarioAfirme, String comentarioProveedor){
		String currentUser = String.valueOf(usuarioService.getUsuarioActual().getNombreUsuario());
		if(expedienteJuridico.getId()==null){
			expedienteJuridico.setFechaCreacion(new Date());
			expedienteJuridico.setCodigoUsuarioCreacion(currentUser);
			expedienteJuridico.setCodigoUsuarioModificacion(currentUser);
		}else{
			ExpedienteJuridico expedienteJuridicoActual = this.entidadService.findById(ExpedienteJuridico.class, expedienteJuridico.getId());
			expedienteJuridico.setFechaCreacion(expedienteJuridicoActual.getFechaCreacion());
			expedienteJuridico.setCodigoUsuarioCreacion(expedienteJuridicoActual.getCodigoUsuarioCreacion());
			expedienteJuridico.setCodigoUsuarioModificacion(expedienteJuridicoActual.getCodigoUsuarioModificacion());
			expedienteJuridico.setFechaModificacion(expedienteJuridicoActual.getFechaModificacion());
			if(expedienteJuridico.getEstatusJuridico().equals(ExpedienteJuridico.EstatusJuridico.CANCELADO.getValue())){
				expedienteJuridico.setFechaCancelacion(new Date());
			}
		}
		this.crearComentarios(expedienteJuridico, comentarioAfirme, comentarioProveedor);
		if(expedienteJuridico.getAbogado()!=null && expedienteJuridico.getAbogado().getId()==null){
			expedienteJuridico.setAbogado(null);
		}
		this.entidadService.save(expedienteJuridico);
		return expedienteJuridico;
	}
	
	@Override
	public List<ComentarioJuridico> obtenerComentarios(Long idExpedienteJuridico){
		List<ComentarioJuridico> comentarios = null;
		if(idExpedienteJuridico!=null){
			List<ExpedienteJuridico> expJuridicoList = this.entidadService.findByProperty(ExpedienteJuridico.class, "id", idExpedienteJuridico);
			if(expJuridicoList!=null && expJuridicoList.size()>0){
				ExpedienteJuridico expJuridico = expJuridicoList.get(0);
				comentarios = expJuridico.getComentarios();
			}
			
		}else{
			comentarios = new ArrayList<ComentarioJuridico>();
		}
		this.acompletaDescripcionEnComentarios(comentarios);
		return comentarios;
	}
	
	
	private void acompletaDescripcionEnComentarios(List<ComentarioJuridico> comentarios){
		for(ComentarioJuridico comentario: comentarios){
			if(comentario.getTipo().equals(ComentarioJuridico.Tipo.AFIRME.getValue())){
				comentario.setTipoDesc(ComentarioJuridico.Tipo.AFIRME.toString());
			}else if(comentario.getTipo().equals(ComentarioJuridico.Tipo.PROVEEDOR.getValue())){
				comentario.setTipoDesc(ComentarioJuridico.Tipo.PROVEEDOR.toString());
			}
		}
		
	}

}