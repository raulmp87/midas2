package mx.com.afirme.midas2.service.impl.sapamis.procesos;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.sapamis.procesos.SapAmisEjecucionesSTPDao;
import mx.com.afirme.midas2.service.sapamis.procesos.SapAmisEjecucionesSTPService;

@Stateless
public class SapAmisEjecucionesSTPServiceImpl implements SapAmisEjecucionesSTPService{
	private static final long serialVersionUID = 1L;
	
	private SapAmisEjecucionesSTPDao sapAmisEjecucionesSTPDao;

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void emisionPoblado(Date fechaInicial, Date fechaFinal) {
		sapAmisEjecucionesSTPDao.emisionPoblado(fechaInicial, fechaFinal);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void siniestrosPoblado(Date fechaInicial, Date fechaFinal) {
		sapAmisEjecucionesSTPDao.siniestrosPoblado(fechaInicial, fechaFinal);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void rechazosPoblado(Date fechaInicial, Date fechaFinal) {
		sapAmisEjecucionesSTPDao.rechazosPoblado(fechaInicial, fechaFinal);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void pttPoblado(Date fechaInicial, Date fechaFinal) {
		sapAmisEjecucionesSTPDao.pttPoblado(fechaInicial, fechaFinal);
	}

	@Override
	public void salvamentoPoblado(Date fechaInicial, Date fechaFinal) {
		sapAmisEjecucionesSTPDao.salvamentoPoblado(fechaInicial, fechaFinal);
	}
	
	@Override
	public void roboPobladoREPUVE(Date fechaInicial, Date fechaFinal) {
		sapAmisEjecucionesSTPDao.roboPobladoREPUVE(fechaInicial, fechaFinal);
	}
	
	@Override
	public void recuperacionPobladoREPUVE(Date fechaInicial, Date fechaFinal) {
		sapAmisEjecucionesSTPDao.recuperacionPobladoREPUVE(fechaInicial, fechaFinal);
	}

	@EJB
	public void setSapAmisEjecucionesSTPDao(SapAmisEjecucionesSTPDao sapAmisEjecucionesSTPDao) {
		this.sapAmisEjecucionesSTPDao = sapAmisEjecucionesSTPDao;
	}
}
