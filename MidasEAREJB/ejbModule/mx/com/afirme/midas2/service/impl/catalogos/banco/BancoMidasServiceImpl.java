package mx.com.afirme.midas2.service.impl.catalogos.banco;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas2.domain.catalogos.banco.BancoMidas;
import mx.com.afirme.midas2.domain.catalogos.banco.ConfigCuentaMidas;
import mx.com.afirme.midas2.domain.catalogos.banco.CuentaBancoMidas;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.DetalleCargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.DetalleCargaMasivaIndAutoCot;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.banco.BancoMidasService;


@Stateless
public class BancoMidasServiceImpl implements BancoMidasService {

	@EJB
	EntidadService entidadService;
	
	@EJB
	private BancoEmisorFacadeRemote bancoEmisorFacadeRemote;
	
	@Override
	public List<BancoMidas> obtenerBancos() {
		return entidadService.findByFilterObject(BancoMidas.class, null, "nombre");		
	}

	@Override
	public List<CuentaBancoMidas> obtenerCuentas() {
		return entidadService.findByFilterObject(CuentaBancoMidas.class, null, "descripcion");		
	}

	@Override
	public List<CuentaBancoMidas> obtenerCuentas(Long bancoId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("bancoMidas.id", bancoId);
		return entidadService.findByPropertiesWithOrder(CuentaBancoMidas.class, params, "descripcion");	
	}

	@Override
	public List<ConfigCuentaMidas> obtenerConfiguracionCuentas(Long idCuenta) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("cuenta.id", idCuenta);
		return entidadService.findByPropertiesWithOrder(ConfigCuentaMidas.class, params, "orden");	
	}

	@Override
	public ConfigCuentaMidas obtenerConfiguracionCuenta(Long idCuenta) {
		List<ConfigCuentaMidas> configs = obtenerConfiguracionCuentas(idCuenta);
		if(configs != null && configs.size() > 0){
			return configs.get(0);
		}
		return null;
	}
	
	@Override
	public List<DetalleCargaMasivaAutoCot> encriptarCuentasBancariasCargaMasiva(List<DetalleCargaMasivaAutoCot> detalleCargaMasivaList){
		
		for(DetalleCargaMasivaAutoCot detCargaMasiva : detalleCargaMasivaList){
			
			detCargaMasiva.setNumeroTarjetaClave(bancoEmisorFacadeRemote.encriptaDatos(detCargaMasiva.getNumeroTarjetaClave()));
			detCargaMasiva.setCodigoSeguridad(bancoEmisorFacadeRemote.encriptaDatos(detCargaMasiva.getCodigoSeguridad()));
			detCargaMasiva.setFechaVencimiento(bancoEmisorFacadeRemote.encriptaDatos(detCargaMasiva.getFechaVencimiento()));
		}

		return detalleCargaMasivaList;
	}
	
	@Override
	public DetalleCargaMasivaIndAutoCot encriptarCuentasBancariasCargaMasivaInd(DetalleCargaMasivaIndAutoCot detalleCargaMasivaInd){
		detalleCargaMasivaInd.setNumeroTarjetaClave(bancoEmisorFacadeRemote.encriptaDatos(detalleCargaMasivaInd.getNumeroTarjetaClave()));
		detalleCargaMasivaInd.setCodigoSeguridad(bancoEmisorFacadeRemote.encriptaDatos(detalleCargaMasivaInd.getCodigoSeguridad()));
		detalleCargaMasivaInd.setFechaVencimiento(bancoEmisorFacadeRemote.encriptaDatos(detalleCargaMasivaInd.getFechaVencimiento()));

		return detalleCargaMasivaInd;
	}
	
	@Override
	public String encriptaInformacionBancaria(String valorDesencriptado){
		if(valorDesencriptado!=null){
			valorDesencriptado = bancoEmisorFacadeRemote.encriptaDatos(valorDesencriptado);
		}
		return valorDesencriptado;
	}

	@Override
	public String desencriptaInformacionBancaria(String valorDesencriptado){
		if(valorDesencriptado!=null){
			valorDesencriptado = bancoEmisorFacadeRemote.desEncriptaDatos(valorDesencriptado);
			
		}
		return valorDesencriptado;
	}
	
	@Override
	public List<BancoEmisorDTO> bancosSeycos(){
		return bancoEmisorFacadeRemote.findAll();
	}
}
