package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.service.fuerzaventa.DocumentoAgenteService;
import mx.com.afirme.midas2.dto.fuerzaventa.ConfiguracionAgenteDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.GuiaHonorariosAgenteView;
import mx.com.afirme.midas2.dao.fuerzaventa.DocumentoAgenteDao;

@Stateless
public class DocumentoAgenteServiceImpl implements DocumentoAgenteService {
	
	@EJB 
	private DocumentoAgenteDao dao;
	
	public List<GuiaHonorariosAgenteView> obtenerGuiasHonorarios(ConfiguracionAgenteDTO configuracion, String anioMes) {
		return dao.obtenerGuiasHonorarios(configuracion, anioMes);
	}
	public List<GuiaHonorariosAgenteView> obtenerGuiasHonorariosPorIds(List<Long> ids) {
		return dao.obtenerGuiasHonorariosPorIds(ids);
	}
	
}