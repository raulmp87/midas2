/**
 * Clase que llena las opciones de Menu para el rol de Cabinero
 */
package mx.com.afirme.midas2.service.impl.seguridad.filler.opreasegurofacultativo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

/**
 * @author andres.avalos
 *
 */
public class MenuOpReaseguroFacultativo {

	private List<Menu> listaMenu = null;
		
	public MenuOpReaseguroFacultativo() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"menu","", "Raiz del menu", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m3","Reaseguro", "Menu ppal Reaseguro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m3_1","Reaseguro", "Catalogos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("4"),"m3_1_1","Reaseguro", "Reasegurador-Corredor", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m3_1_2","Reaseguro", "Contacto", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("6"),"m3_1_3","Reaseguro", "Cuenta Banco", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("7"),"m3_2","Reaseguro", "Contratos Proporcionales", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("12"),"m3_2_2","Reaseguro", "Administraci�n de Facultativo", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("13"),"m3_2_2_1","Reaseguro", "Cotizaci�n Negociaci�n", "/MidasWeb/sistema/configuracion/listar.do|contenido|cargandoTree('configuracionFacultativa0','treeboxbox_tree')", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("14"),"m3_2_2_2","Reaseguro", "Cotizaci�n Autoriza", "/MidasWeb/sistema/configuracion/listar.do|contenido|cargandoTree('configuracionFacultativa1','treeboxbox_tree')", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("15"),"m3_2_2_3","Reaseguro", "Contratos Autorizados", "/MidasWeb/sistema/configuracion/listar.do|contenido|cargandoTree('configuracionFacultativa2','treeboxbox_tree')", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("16"),"m3_2_2_4","Reaseguro", "Solicitud Cancelados", "/MidasWeb/sistema/configuracion/listar.do|contenido|cargandoTree('configuracionFacultativa3','treeboxbox_tree')", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("17"),"m3_2_2_5","Reaseguro", "Cancelados", "/MidasWeb/sistema/configuracion/listar.do|contenido|cargandoTree('configuracionFacultativa4','treeboxbox_tree')','treeboxbox_tree')", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("36"),"m3_6","Reaseguro", "Estados de Cuenta", "/MidasWeb/reaseguro/estadoscuenta/mostrarPrincipalEstadosCuenta.do|contenido|dhx_init_tabbars()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("63"),"m7","Ayuda", "Ayuda MIDAS", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("64"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("65"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
		
		return this.listaMenu;
		
	}
	
}
