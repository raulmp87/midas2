package mx.com.afirme.midas2.threads;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.calculos.CalculoBonosService;
import mx.com.afirme.midas2.service.calculos.CalculoComisionesService;


public class MailThreadSupport implements Runnable{
	private static final MailThreadSupport mailSupport = new MailThreadSupport();
	private Object service;
	private Long id;
	private List<Long> ids;
	private Long idProceso;
	private Long idMovimiento;
	private String mensaje;
	private String tituloMensaje;
	private int tipoTemplate;
	private String methodToExcecute;
	private Map<String, Object> genericParams;
	private String formatoSalida;
	
	private MailThreadSupport() {

	}
	
	@Override
	public void run() {
		castInterface();
	}
	
	/**
	 * Obtiene el tipo de la interface
	 * para luego ejecutar el metodo
	 */
	private  void castInterface() {
		try {
			if (getService() instanceof CalculoComisionesService) {
				CalculoComisionesService service = (CalculoComisionesService) getService();
				executeMethod(service,id, idProceso, idMovimiento, mensaje, tipoTemplate);
			}else if (getService() instanceof CalculoBonosService) {
				CalculoBonosService service = (CalculoBonosService)getService();
				executeMethod(service, id, idProceso, idMovimiento, mensaje, tipoTemplate);
			}else if (getService() instanceof ConfigBonosService) {
				ConfigBonosService service = (ConfigBonosService)getService();
				executeMethod(service,ids,genericParams,formatoSalida);
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}
	
	private <I> void executeMethod(I service, Object ...args){
		try {
			if (methodToExcecute != null) {
				getMethod(methodToExcecute).invoke(service,args);
			}else {
				throw new RuntimeException("No existe un metodo para ejecutar");
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Obtiene el metodo que se tiene que ejecutar
	 * @param methodName
	 * @return
	 */
	public Method getMethod(String methodName) {
		Method[] methods = getService().getClass().getDeclaredMethods();
		for (Method method : methods) {
				if (method.getName().equals(methodName)) {
					return method;
				}
		}
		return null;
	}
	public static MailThreadSupport getInstance(){
		return mailSupport;
	}
	
	public Object getService() {
		return service;
	}
	
	public void setService(Object service) {
		this.service = service;
	}

	public Long getIdProceso() {
		return idProceso;
	}

	public void setIdProceso(Long idProceso) {
		this.idProceso = idProceso;
	}

	public Long getIdMovimiento() {
		return idMovimiento;
	}

	public void setIdMovimiento(Long idMovimiento) {
		this.idMovimiento = idMovimiento;
	}

	public int getTipoTemplate() {
		return tipoTemplate;
	}

	public void setTipoTemplate(int tipoTemplate) {
		this.tipoTemplate = tipoTemplate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getTituloMensaje() {
		return tituloMensaje;
	}

	public void setTituloMensaje(String tituloMensaje) {
		this.tituloMensaje = tituloMensaje;
	}

	public String getMethodToExcecute() {
		return methodToExcecute;
	}

	public void setMethodToExcecute(String methodToExcecute) {
		this.methodToExcecute = methodToExcecute;
	}

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	public Map<String, Object> getGenericParams() {
		return genericParams;
	}

	public void setGenericParams(Map<String, Object> genericParams) {
		this.genericParams = genericParams;
	}

	public String getFormatoSalida() {
		return formatoSalida;
	}

	public void setFormatoSalida(String formatoSalida) {
		this.formatoSalida = formatoSalida;
	}
	
}
