package mx.com.afirme.vida.service.impl.tarifa;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.exeption.ErrorBuilder;
import mx.com.afirme.midas2.utils.LeerArchivo;
import mx.com.afirme.vida.domain.movil.cotizador.TarifasMovilVidaDTO;
import mx.com.afirme.vida.service.tarifa.TarifaMovilVidaService;

/**
 * Facade for entity TarifasMovilVidaDTO.
 * 
 * 
 * 
 */
@Stateless
public class TarifaMovilVidaServiceImpl extends EntidadHistoricoDaoImpl  implements TarifaMovilVidaService {
	// property constants
	public static final String CLAVEAGENTE = "claveagente";
	public static final String NOMBRE = "nombre";
	public static final String PORCENTAJE = "porcentaje";
	public String respuestaMensaje="";
	@PersistenceContext
	private EntityManager entityManager;
	private List<String> registrosAfectados = new ArrayList<String>();
	private List<Map<String,String>>cargaMasiva = new ArrayList<Map<String,String>>();
	private Map<String, String> obtenerDato;

	public List<String> cargaMasiva(String archivo) throws Exception {
		int cont = 0;
		int registrosNoInsertados = 0;
		int registrosInsertados = 0;
		cargaMasiva.addAll(LeerArchivo.leer(archivo));
		registrosAfectados.clear();
		String claveAgente;
		for (Map<String, String> iterador : cargaMasiva) {
			try {
				TarifasMovilVidaDTO tarifasMovilVidaDTO = new TarifasMovilVidaDTO();			
				obtenerDato = new HashMap<String, String>();
				obtenerDato.putAll(cargaMasiva.get(cont));
				LogDeMidasEJB3.log("cargaMasiva="+cargaMasiva, Level.INFO, null);
				LogDeMidasEJB3.log("EDADMAX="+obtenerDato.get("EDADMAXIMA").trim(), Level.INFO, null);
				tarifasMovilVidaDTO.setEdadMaxima(new Short(obtenerDato.get("EDADMAXIMA").trim()));
				tarifasMovilVidaDTO.setEdadMinima(new Short(obtenerDato.get("EDADMINIMA").trim()));
				tarifasMovilVidaDTO.setSumaAsegurada(new BigDecimal(obtenerDato.get("SUMAASEGURADA").trim()));
				tarifasMovilVidaDTO.setTarifaBasica(new BigDecimal(obtenerDato.get("TARIFABASICA").trim()));
				tarifasMovilVidaDTO.setTarifaPlatino(new BigDecimal(obtenerDato.get("TARIFAPLATINO").trim()));
				String activo=obtenerDato.get("ACTIVO").trim();
				tarifasMovilVidaDTO.setBajalogica(Short.parseShort(activo));
				save(tarifasMovilVidaDTO);
				registrosInsertados++;
				cont++;
				/**/

			} catch (Exception e) {
				e.toString();
				e.printStackTrace();
				String error = e.toString().replace("java.lang.Exception:", "");
				registrosNoInsertados++;
				cont++;

				registrosAfectados.add("Registro " + String.valueOf(cont) + " "
						+ error);
			}
		}
		registrosAfectados.add(String.valueOf(registrosInsertados));
		registrosAfectados.add(String.valueOf(registrosNoInsertados));
		cargaMasiva.clear();
		return registrosAfectados;
	}
	@Override
	public void delete(TarifasMovilVidaDTO arg0) {
		// TODO Apéndice de método generado automáticamente
		
	}
	@Override
	public boolean existeTarifaMovilVida(String property,Object value){
		List<TarifasMovilVidaDTO> tarifasMovilVidaDTOTmpList=null;
		tarifasMovilVidaDTOTmpList= findByProperty(property, value);
		if(tarifasMovilVidaDTOTmpList.isEmpty()){
		return false;
	}
		else{
			return true;
		}
	}
	@Override
	public List<TarifasMovilVidaDTO> findAll() {
		LogDeMidasEJB3.log("finding all TarifasMovilVidaDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from TarifasMovilVidaDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all TarifasMovilVidaDTO failed", Level.SEVERE, re);
			throw re;
		}
	}
	@Override
	public List<TarifasMovilVidaDTO> findByFiltersDiferentID(
			TarifasMovilVidaDTO filter) {
		LogDeMidasEJB3.log("findTarifasByFilters  TarifasMovilVidaDTO instances", Level.INFO,
				null);
		List<TarifasMovilVidaDTO>  lista=new ArrayList<TarifasMovilVidaDTO>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		try{
			queryString.append(" select distinct * ");
			queryString.append(" from MIDAS.TCTARIFAMOVILVIDA entidad ");
			queryString.append(" where ");
			if(isNotNull(filter)){
				int index=1;
					if(filter.getIdTarifaMovilVida()!=null){
						addCondition(queryString, " entidad.IDTCTARIFAMOVILVIDA!=? ");
						params.put(index,filter.getIdTarifaMovilVida());
						index++;					
					}
					if(filter.getEdadMaxima()!=null){
						addCondition(queryString, " entidad.EDADMAX=? ");
						params.put(index, filter.getEdadMaxima());
						index++;					
					}
					if(filter.getEdadMinima()!=null){
						addCondition(queryString, " entidad.EDADMIN=? ");
						params.put(index, filter.getEdadMinima());
						index++;					
					}
					if(filter.getSumaAsegurada()!=null){
						addCondition(queryString, " entidad.SA=? ");
						params.put(index, filter.getSumaAsegurada());
						index++;					
					}
					if(filter.getTarifaBasica()!=null){
						addCondition(queryString, " entidad.TARIFABASICA=? ");
						params.put(index, filter.getTarifaBasica());
						index++;					
					}
					if(filter.getTarifaPlatino()!=null){
						addCondition(queryString, " entidad.TARIFAPLATINO=? ");
						params.put(index, filter.getTarifaPlatino());
						index++;					
					}
				
				if(params.isEmpty()){
					int lengthWhere="where ".length();
					queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
				}
				String finalQuery=getQueryString(queryString)+" order by entidad.IDTCTARIFAMOVILVIDA desc ";
				Query query=entityManager.createNativeQuery(finalQuery,TarifasMovilVidaDTO.class);
				if(!params.isEmpty()){
					for(Integer key:params.keySet()){
						query.setParameter(key,params.get(key));
					}
				}
				//query.setMaxResults(100);
				lista=query.getResultList();
			}
		
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("findByFilters all  TarifasMovilVidaDTO failed", Level.SEVERE, re);
			throw re;
		}
		return lista;
	}
	public List<TarifasMovilVidaDTO> findByFilters(TarifasMovilVidaDTO filter){
		LogDeMidasEJB3.log("findTarifasByFilters  TarifasMovilVidaDTO instances", Level.INFO,
				null);
		List<TarifasMovilVidaDTO>  lista=new ArrayList<TarifasMovilVidaDTO>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		try{
			queryString.append(" select distinct * ");
			queryString.append(" from MIDAS.TCTARIFAMOVILVIDA entidad ");
			queryString.append(" where ");
			if(isNotNull(filter)){
				int index=1;
					if(filter.getIdTarifaMovilVida()!=null){
						addCondition(queryString, " entidad.IDTCTARIFAMOVILVIDA=? ");
						params.put(index,filter.getIdTarifaMovilVida());
						index++;					
					}
					if(filter.getEdadMaxima()!=null){
						addCondition(queryString, " entidad.EDADMAX=? ");
						params.put(index, filter.getEdadMaxima());
						index++;					
					}
					if(filter.getEdadMinima()!=null){
						addCondition(queryString, " entidad.EDADMIN=? ");
						params.put(index, filter.getEdadMinima());
						index++;					
					}
					if(filter.getSumaAsegurada()!=null){
						addCondition(queryString, " entidad.SA=? ");
						params.put(index, filter.getSumaAsegurada());
						index++;					
					}
					if(filter.getTarifaBasica()!=null){
						addCondition(queryString, " entidad.TARIFABASICA=? ");
						params.put(index, filter.getTarifaBasica());
						index++;					
					}
					if(filter.getTarifaPlatino()!=null){
						addCondition(queryString, " entidad.TARIFAPLATINO=? ");
						params.put(index, filter.getTarifaPlatino());
						index++;					
					}
					if(filter.getBajalogica()!=null){
						addCondition(queryString, " entidad.BAJALOGICA=? ");
						params.put(index, filter.getBajalogica());
						index++;					
					}
				
				if(params.isEmpty()){
					int lengthWhere="where ".length();
					queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
				}
				String finalQuery=getQueryString(queryString)+" order by entidad.IDTCTARIFAMOVILVIDA desc ";
				Query query=entityManager.createNativeQuery(finalQuery,TarifasMovilVidaDTO.class);
				if(!params.isEmpty()){
					for(Integer key:params.keySet()){
						query.setParameter(key,params.get(key));
					}
				}
				query.setMaxResults(100);
				lista=query.getResultList();
			}
		
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("findByFilters all  TarifasMovilVidaDTO failed", Level.SEVERE, re);
			throw re;
	}
		return lista;
		
	}
	@Override
	public TarifasMovilVidaDTO findById(Long id) {
		LogDeMidasEJB3.log("finding TarifasMovilVidaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TarifasMovilVidaDTO instance = entityManager.find(
					TarifasMovilVidaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed TarifasMovilVidaDTO", Level.SEVERE, re);
			throw re;
	}
	}
	@Override
	public List<TarifasMovilVidaDTO> findByProperty(String propertyName,
	final Object value)  {
		LogDeMidasEJB3.log("finding TarifasMovilVidaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TarifasMovilVidaDTO model where  model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property TarifasMovilVidaDTO name failed", Level.SEVERE, re);
			throw re;
	}
	}
	@Override
	public List<TarifasMovilVidaDTO> findByPropertyActivos(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TarifasMovilVidaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TarifasMovilVidaDTO model where  model.bajalogica=1 and model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
	}
	}
	@Override
	public TarifasMovilVidaDTO save(TarifasMovilVidaDTO entity) {
		LogDeMidasEJB3.log("saving TarifasMovilVidaDTO instance", Level.INFO, null);
		try {
			entity.setFechaRegistro(new Date());
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save  TarifasMovilVidaDTO successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save TarifasMovilVidaDTO failed", Level.SEVERE, re);
			throw re;
		}
		return entity;
	}
	@Override
	public String update(TarifasMovilVidaDTO entity) {
		LogDeMidasEJB3.log("updating TarifasMovilVidaDTO instance", Level.INFO, null);
		try {
			ErrorBuilder eb = new ErrorBuilder();
			List<TarifasMovilVidaDTO> lista=findByFiltersDiferentID(entity);
			if(lista.isEmpty()){
				entity.setFechaRegistro(new Date());			
				TarifasMovilVidaDTO result = entityManager.merge(entity);
				LogDeMidasEJB3.log("update successful", Level.INFO, null);
				respuestaMensaje= "Actualizacion Exitosa";
			}
			else{
				respuestaMensaje= "duplicado";
			}
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
		return respuestaMensaje;
	}
	public TarifasMovilVidaDTO findFechaNacimientoTarifaMovilVida(Date FechaNacimiento){
		List<TarifasMovilVidaDTO>  lista=new ArrayList<TarifasMovilVidaDTO>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		TarifasMovilVidaDTO tarifasMovilVida;
		try{
			final StringBuilder queryString=new StringBuilder("");
				queryString.append(" select distinct * ");
				queryString.append(" from MIDAS.TCTARIFAMOVILVIDA entidad ");
				queryString.append(" where ");
				if(isNotNull(FechaNacimiento)){
					int index=1;
						if(FechaNacimiento!=null){
							addCondition(queryString, " entidad.EDADMIN<=? ");
							params.put(index, obtenerEdad(FechaNacimiento));
							index++;					
							addCondition(queryString, " entidad.EDADMAX>=? ");
							params.put(index, obtenerEdad(FechaNacimiento));
							index++;
						}
					if(params.isEmpty()){
						int lengthWhere="where ".length();
						queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
					}
					String finalQuery=getQueryString(queryString)+" order by entidad.IDTCTARIFAMOVILVIDA desc ";
					Query query=entityManager.createNativeQuery(finalQuery,TarifasMovilVidaDTO.class);
					if(!params.isEmpty()){
						for(Integer key:params.keySet()){
							query.setParameter(key,params.get(key));
						}
					}
					lista=query.getResultList();
				}
				tarifasMovilVida=lista.get(0);
			}
		catch(Exception e){
			tarifasMovilVida=null;
			e.printStackTrace();
		}
		return tarifasMovilVida;
	}


	public static Integer obtenerEdad(Date birthday)
	{
	    GregorianCalendar today = new GregorianCalendar();
	    GregorianCalendar bday = new GregorianCalendar();
	    GregorianCalendar bdayThisYear = new GregorianCalendar();

	    bday.setTime(birthday);
	    bdayThisYear.setTime(birthday);
	    bdayThisYear.set(Calendar.YEAR, today.get(Calendar.YEAR));

	    Integer age = today.get(Calendar.YEAR) - bday.get(Calendar.YEAR);

	    if(today.getTimeInMillis() < bdayThisYear.getTimeInMillis())
	        age--;

	    return age;
	}
}