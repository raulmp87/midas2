package mx.com.afirme.vida.service.impl.asistencia;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.utils.SimpleVerifier;
import mx.com.afirme.vida.dao.asistencia.AsistenciaVidaDao;
import mx.com.afirme.vida.domain.asistencia.AsistenciaVida;
import mx.com.afirme.vida.domain.asistencia.AsistenciaVida.AsistenciaVidaEstatus;
import mx.com.afirme.vida.service.asistencia.AsistenciaVidaService;
import mx.com.afirme.vida.dto.asistencia.AsistenciaVidaFiltro;
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.InteractiveCallback;
import ch.ethz.ssh2.KnownHosts;
import ch.ethz.ssh2.SFTPv3Client;
import ch.ethz.ssh2.SFTPv3FileHandle;

@Stateless
public class AsistenciaVidaServiceImpl extends AsistenciaVidaSoporteService implements AsistenciaVidaService {

	
	@PersistenceContext
	private EntityManager entityManager;

	@EJB
	private AsistenciaVidaDao asistenciaVidaDao;
	
	//public static final Logger LOG = Logger.getLogger(AsistenciaVidaServiceImpl.class);

	@Override
	public List<AsistenciaVida> findAll() {
		LogDeMidasEJB3.log("finding all AsistenciaVida instances", Level.INFO, null);
		try {
			final String queryString = "select model from AsistenciaVida model order by model.id ASC";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all AsistenciaVida failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public List<AsistenciaVida> findByFilters(AsistenciaVidaFiltro filter) {
		LogDeMidasEJB3.log("findByFilters  AsistenciaVida instances", Level.INFO, null);
		return asistenciaVidaDao.buscarEnvioProveedor(filter);

	}	

	@Override
	public AsistenciaVida findById(Long id) {
		LogDeMidasEJB3.log("finding AsistenciaVida instance with id: " + id, Level.INFO, null);
		try {
			return entityManager.find(AsistenciaVida.class, id);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed AsistenciaVida", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public List<AsistenciaVida> findByProperty(String propertyName, Object value) {
		LogDeMidasEJB3.log("finding AsistenciaVida instance with property: " + propertyName + ", value: " + value,
				Level.INFO, null);
		try {
			final String queryString = "select model from AsistenciaVida model where  model." + propertyName
					+ "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property AsistenciaVida name failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public AsistenciaVida save(AsistenciaVida entity) {
		LogDeMidasEJB3.log("saving AsistenciaVida instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save  AsistenciaVida successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save AsistenciaVida failed", Level.SEVERE, re);
			throw re;
		}
		return entity;
	}

	@Override
	public void update(AsistenciaVida entity) {
		LogDeMidasEJB3.log("updating AsistenciaVida instance", Level.INFO, null);

		try {
			entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public List<AsistenciaVida> findBatchesPendientes() {
		LogDeMidasEJB3.log("findBatchesPendientes AsistenciaVida instance", Level.INFO, null);
		try {
			final String queryString = "select model from AsistenciaVida model where  model.estatus = :estatus";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("estatus", AsistenciaVida.AsistenciaVidaEstatus.POR_ENVIAR);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("findBatchesPendientes AsistenciaVida name failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public AsistenciaVida setEnviado(AsistenciaVida entity) {
		LogDeMidasEJB3.log("updating AsistenciaVida instance", Level.INFO, null);
		AsistenciaVida result;
		try {
			entity.setEstatus(AsistenciaVidaEstatus.ENVIADO);
			result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
		return result;
	}

	public void addCondition(StringBuilder queryString, String conditional) {
		if (!queryString.toString().contains("where")) {
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}

	@Override
	public String guardaArchivo(AsistenciaVida entity) {
		LogDeMidasEJB3.log("enviarProveedor instance", Level.INFO, null);
		String mensaje = null;

		Connection conn = null;
		SFTPv3Client client = null;

		try {
			String directory = sistemaContext.getEnvioProvAsisVidaDirectory();
			String remoteDirectory = directory;

			conn = this.getConnectionSFTPAuth();

			LogDeMidasEJB3.log("Inicia envio de Archivos a Proveedor", Level.INFO, null);
			client = new SFTPv3Client(conn);


			try {
				enviarArchivoSFTP(client, entity, remoteDirectory + "/" + entity.getNombreArchivo() + ".txt");
			} catch (Exception e) {
				LogDeMidasEJB3.log("Error al invocar enviarArchivoSFTP", Level.SEVERE, e);
				mensaje = e.getMessage();
			}

			LogDeMidasEJB3.log("Termina envio de Archivos a Proveedor", Level.INFO, null);
			client.close();
			conn.close();
		} catch (java.net.ConnectException e) {
			LogDeMidasEJB3.log("Error de conexión al guardar el archivo", Level.SEVERE, e);
			mensaje = e.getMessage();
		} catch (IOException e) {
			LogDeMidasEJB3.log("Error al escribir archivo", Level.SEVERE, e);
			mensaje = e.getMessage();
		} catch (Exception e) {
			LogDeMidasEJB3.log(e.getMessage(), Level.SEVERE, e);
			mensaje = e.getMessage();
		} finally {
			if (client != null) {
				client.close();
			}

			if (conn != null) {
				conn.close();
			}

		}
		return mensaje;
	}

	private Connection getConnectionSFTPAuth() throws IOException {
		File home = new File(System.getProperty("user.home"));
		String serverAddress = sistemaContext.getEnvioProvAsisVidaFTP();
		String username = sistemaContext.getEnvioProvAsisVidaUser();
		final String password = sistemaContext.getEnvioProvAsisVidaPassword();
		String privateKeyPath = null;
		int connectTimeout = 30000;
		int kexTimeout = 30000;


		File knownHosts = new File("~/.ssh/known_hosts");
		File knownHosts2 = new File("/root/.ssh/known_hosts");
		File knownHosts3 = new File(home, ".ssh/known_hosts");
		File knownHosts4 = null;

		try {
			Properties prop = new Properties();
			// load a properties file from class path, inside static method
			InputStream p = AsistenciaVidaServiceImpl.class.getClassLoader().getResourceAsStream(
					"MidasSistemaVida.properties");
			if (p == null) {
				p = new FileInputStream("MidasSistemaVida.properties");
			}
			prop.load(p);

			// get the property value and print it out
			String pathProp = prop.getProperty("midas.sistema.envioProveedor.knowhost");
			if (pathProp != null) {
				knownHosts4 = new File(pathProp);
			}
		} catch (Exception ex) {
			LogDeMidasEJB3.log(ex.getMessage(), Level.SEVERE, ex);
		}
		KnownHosts database = new KnownHosts();

		/* Load known_hosts file into in-memory database */

		if (knownHosts != null && knownHosts.exists()) {
			database.addHostkeys(knownHosts);
		} else if (knownHosts2 != null && knownHosts2.exists()) {
			database.addHostkeys(knownHosts2);
		} else if (knownHosts3 != null && knownHosts3.exists()) {
			database.addHostkeys(knownHosts3);
		} else if (knownHosts4 != null && knownHosts4.exists()) {
			database.addHostkeys(knownHosts4);
		} else {
			LogDeMidasEJB3.log("File /root/.ssh/known_hosts not found.", Level.INFO, null);
		}

		Connection conn = new Connection(serverAddress);
		/* Now connect and use the SimpleVerifier */

		conn.connect(new SimpleVerifier(database), connectTimeout, kexTimeout);

		/*
		 * Authenticate. If you get an IOException saying something like
		 * "Authentication method password not supported by the server at this
		 * stage." then please check the FAQ.
		 */

		boolean triedCustomPublicKey = false;
		boolean triedStandardDSAPublicKey = false;
		boolean triedStandardRSAPublicKey = false;
		boolean triedPassword = false;
		boolean triedPasswordInteractive = false;

		boolean isAuthenticated = false;
		if (privateKeyPath != null) {
			triedCustomPublicKey = true;
			isAuthenticated = conn.authenticateWithPublicKey(username, new File(privateKeyPath), password);
		} else {

			try {
				triedStandardDSAPublicKey = true;
				isAuthenticated = conn.authenticateWithPublicKey(username, new File(home, ".ssh/id_dsa"), password);
			} catch (IOException ex) {
				// dsa key probably can't be found
				LogDeMidasEJB3.log("dsa key probably can't be found", Level.SEVERE, ex);
			}
			if (!isAuthenticated) {
				try {
					triedStandardRSAPublicKey = true;
					isAuthenticated = conn.authenticateWithPublicKey(username, new File(home, ".ssh/id_rsa"), password);
				} catch (IOException ex) {
					// rsa key probably can't be found
					LogDeMidasEJB3.log("rsa key probably can't be found", Level.SEVERE, ex);
				}
			}
		}

		if (!isAuthenticated) {
			try {
				triedPassword = true;
				isAuthenticated = conn.authenticateWithPassword(username, password);
			} catch (IOException ex) {
				// Password authentication probably not supported
				LogDeMidasEJB3.log("Password authentication probably not supported", Level.SEVERE, ex);
			}
			if (!isAuthenticated) {
				try {
					triedPasswordInteractive = true;
					InteractiveCallback icb = new InteractiveCallback() {

						public String[] replyToChallenge(String name, String instruction, int numPrompts,
								String[] prompt, boolean[] echo) throws Exception {
							String[] responses = new String[numPrompts];
							for (int x = 0; x < numPrompts; x++) {
								responses[x] = password;
							}
							return responses;
						}
					};
					isAuthenticated = conn.authenticateWithKeyboardInteractive(username, icb);
				} catch (IOException ex) {
					// Password interactive probably not supported
					LogDeMidasEJB3.log("Password interactive probably not supported", Level.SEVERE, ex);
				}
			}
		}

		if (!isAuthenticated) {
			throw new IOException("Authentication failed.  Tried \n"
					+ (triedCustomPublicKey ? "\tpublic key using " + privateKeyPath + "\n" : "")
					+ (triedStandardDSAPublicKey ? "\tpublic key using ~/.ssh/id_dsa\n" : "")
					+ (triedStandardRSAPublicKey ? "\tpublic key using ~/.ssh/id_rsa\n" : "")
					+ (triedPassword ? "\tpassword\n" : "")
					+ (triedPasswordInteractive ? "\tpassword interactive" : ""));
		}

		return conn;
	}

	private void enviarArchivoSFTP(SFTPv3Client client, AsistenciaVida entity, String nombreArchivo) throws IOException {
		try{
			client.rm(nombreArchivo);
		}catch(Exception e){
		}
		
		SFTPv3FileHandle handle = client.createFile(nombreArchivo);
		byte[] buffer = new byte[1024];
		int offset = 0;

		buffer = (entity.getRecordHeader().append("\r\n")).toString().getBytes("UTF-8");
		client.write(handle, offset, buffer, 0, buffer.length);
		offset += buffer.length;
		for (int i = 0; i < entity.getAsegurados().size(); i++) {
			buffer = (entity.getAsegurados().get(i).getRecord().append("\r\n")).toString().getBytes();
			client.write(handle, offset, buffer, 0, buffer.length);
			offset += buffer.length;
			entity.getAsegurados().get(i).setEstatus(AsistenciaVidaEstatus.ENVIADO);
			entity.getAsegurados().get(i).setFhEnvio(new Date());

		}
		setEnviado(entity);
		client.closeFile(handle);
		if (handle.isClosed()) {
			LogDeMidasEJB3.log("Closed", Level.INFO, null);
		}
	}

	@Override
	@Schedule(hour="9", persistent=false)	
	public void initialize() {
		LogDeMidasEJB3.log("Iniciando ejecucion automatizada de Envío de Asistencias a Proveedor", Level.INFO, null);
		List<AsistenciaVida> asistenciaVidaList = new ArrayList<AsistenciaVida>();
		asistenciaVidaList = findBatchesPendientes();
		for(int i=0; i < asistenciaVidaList.size(); i++){
			guardaArchivo(asistenciaVidaList.get(i));
			setEnviado(asistenciaVidaList.get(i));
		}
	
	}
	
}
