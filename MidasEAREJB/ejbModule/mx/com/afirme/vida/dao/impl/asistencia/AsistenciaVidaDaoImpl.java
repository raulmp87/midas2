package mx.com.afirme.vida.dao.impl.asistencia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.vida.dao.asistencia.AsistenciaVidaDao;
import mx.com.afirme.vida.domain.asistencia.AsistenciaVida;
import mx.com.afirme.vida.dto.asistencia.AsistenciaVidaFiltro;

@Stateless
public class AsistenciaVidaDaoImpl extends EntidadDaoImpl implements AsistenciaVidaDao{

	@Override
	public List<AsistenciaVida> buscarEnvioProveedor(AsistenciaVidaFiltro filtro) {
		StringBuilder queryString = new StringBuilder();
		List<HashMap> params 	= new ArrayList<HashMap>();
		List<AsistenciaVida> resultList;
		Query query = null;

		queryString.append( " SELECT e FROM AsistenciaVida AS e WHERE 1=1 " );

		if(filtro.getEstatus() != null){
			queryString.append(" AND e.estatus = :estatus");
			Utilerias.agregaHashLista(params, "estatus", filtro.getEstatus());
		}

		if(filtro.getFechaRecepcionDesde() != null){
			queryString.append(" AND e.fRecepcion >= :fRecepcionDesde");
			Utilerias.agregaHashLista(params, "fRecepcionDesde", filtro.getFechaRecepcionDesde());
		}		
		
		if(filtro.getFechaRecepcionHasta() != null){
			queryString.append(" AND e.fRecepcion <= :fRecepcionHasta");
			Utilerias.agregaHashLista(params, "fRecepcionHasta", filtro.getFechaRecepcionHasta());
		}		
		
		if(filtro.getIdBatch() != null){
			queryString.append(" AND e.id = :id");
			Utilerias.agregaHashLista(params, "id", filtro.getIdBatch());
		}		

		queryString.append(" ORDER BY e.id ASC");
		query = this.entityManager.createQuery(queryString.toString());
		Utilerias.estableceParametrosQuery(query, params);

		resultList = query.getResultList();
		return resultList;
	}

}
