package mx.com.afirme.midas.endoso.cancelacion;


import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity EndosoCancelableDTO.
 * @see .EndosoCancelableDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class EndosoCancelableFacade  implements EndosoCancelableFacadeRemote {
	//property constants
	public static final String ESTATUS_REGISTRO = "estatusRegistro";
	public static final String VALOR_PRIMA_NETA = "valorPrimaNeta";
	public static final String VALOR_BONIFICACION_COMISION = "valorBonificacionComision";
	public static final String VALOR_BONIF_COM_RECARGO_PAGO_FRACCIONADO = "valorBonifComRecargoPagoFraccionado";
	public static final String VALOR_DERECHOS = "valorDerechos";
	public static final String VALOR_RECARGO_PAGO_FRACCIONADO = "valorRecargoPagoFraccionado";
	public static final String VALOR_IVA = "valorIva";
	public static final String VALOR_PRIMA_TOTAL = "valorPrimaTotal";
	public static final String VALOR_COMISION = "valorComision";
	public static final String VALOR_COMISION_RECARGO_PAGO_FRACCIONADO = "valorComisionRecargoPagoFraccionado";





    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved EndosoCancelableDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EndosoCancelableDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EndosoCancelableDTO entity) {
    				LogUtil.log("saving EndosoCancelableDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent EndosoCancelableDTO entity.
	  @param entity EndosoCancelableDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EndosoCancelableDTO entity) {
    				LogUtil.log("deleting EndosoCancelableDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(EndosoCancelableDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved EndosoCancelableDTO entity and return it or a copy of it to the sender. 
	 A copy of the EndosoCancelableDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EndosoCancelableDTO entity to update
	 @return EndosoCancelableDTO the persisted EndosoCancelableDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public EndosoCancelableDTO update(EndosoCancelableDTO entity) {
    				LogUtil.log("updating EndosoCancelableDTO instance", Level.INFO, null);
	        try {
            EndosoCancelableDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public EndosoCancelableDTO findById( EndosoCancelableId id) {
    				LogUtil.log("finding EndosoCancelableDTO instance with id: " + id, Level.INFO, null);
	        try {
            EndosoCancelableDTO instance = entityManager.find(EndosoCancelableDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all EndosoCancelableDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EndosoCancelableDTO property to query
	  @param value the property value to match
	  	  @return List<EndosoCancelableDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<EndosoCancelableDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding EndosoCancelableDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from EndosoCancelableDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	public List<EndosoCancelableDTO> findByEstatusRegistro(Object estatusRegistro
	) {
		return findByProperty(ESTATUS_REGISTRO, estatusRegistro
		);
	}
	
	public List<EndosoCancelableDTO> findByValorPrimaNeta(Object valorPrimaNeta
	) {
		return findByProperty(VALOR_PRIMA_NETA, valorPrimaNeta
		);
	}
	
	public List<EndosoCancelableDTO> findByValorBonificacionComision(Object valorBonificacionComision
	) {
		return findByProperty(VALOR_BONIFICACION_COMISION, valorBonificacionComision
		);
	}
	
	public List<EndosoCancelableDTO> findByValorBonifComRecargoPagoFraccionado(Object valorBonifComRecargoPagoFraccionado
	) {
		return findByProperty(VALOR_BONIF_COM_RECARGO_PAGO_FRACCIONADO, valorBonifComRecargoPagoFraccionado
		);
	}
	
	public List<EndosoCancelableDTO> findByValorDerechos(Object valorDerechos
	) {
		return findByProperty(VALOR_DERECHOS, valorDerechos
		);
	}
	
	public List<EndosoCancelableDTO> findByValorRecargoPagoFraccionado(Object valorRecargoPagoFraccionado
	) {
		return findByProperty(VALOR_RECARGO_PAGO_FRACCIONADO, valorRecargoPagoFraccionado
		);
	}
	
	public List<EndosoCancelableDTO> findByValorIva(Object valorIva
	) {
		return findByProperty(VALOR_IVA, valorIva
		);
	}
	
	public List<EndosoCancelableDTO> findByValorPrimaTotal(Object valorPrimaTotal
	) {
		return findByProperty(VALOR_PRIMA_TOTAL, valorPrimaTotal
		);
	}
	
	public List<EndosoCancelableDTO> findByValorComision(Object valorComision
	) {
		return findByProperty(VALOR_COMISION, valorComision
		);
	}
	
	public List<EndosoCancelableDTO> findByValorComisionRecargoPagoFraccionado(Object valorComisionRecargoPagoFraccionado
	) {
		return findByProperty(VALOR_COMISION_RECARGO_PAGO_FRACCIONADO, valorComisionRecargoPagoFraccionado
		);
	}
	
	
	/**
	 * Find all EndosoCancelableDTO entities.
	  	  @return List<EndosoCancelableDTO> all EndosoCancelableDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<EndosoCancelableDTO> findAll(
		) {
					LogUtil.log("finding all EndosoCancelableDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from EndosoCancelableDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	public boolean notificaCancelacion(EndosoCancelableDTO endoso) {
		try {
			
			LogDeMidasEJB3.log("EndosoCancelableFacade.notificaCancelacion->" +
					" Guardando el registro del endoso cancelable en la tabla temporal..." +
					"idPoliza=" + endoso.getId().getIdToPoliza() + ", numeroEndoso=" + endoso.getId().getNumeroEndoso() + 
					"Fecha de Inicio de Vigencia" + endoso.getFechaInicioVigencia(), Level.INFO, null);

			List<EndosoCancelableDTO> endosoExisteList = findByProperty("id", endoso.getId());
			
			if (endosoExisteList == null || endosoExisteList.isEmpty()) {
				save(endoso);
			}
			
			return true;
		
		}  catch (RuntimeException re) {
			LogUtil.log("EndosoCancelableFacade.notificaCancelacion fallo", Level.SEVERE, re);
			throw re;
		}
	}
	
}