package mx.com.afirme.midas.endoso.cancelacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoFacade;
import mx.com.afirme.midas.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudFacadeRemote;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;



@Stateless
public class CancelacionEndosoFacade extends EndosoFacade implements CancelacionEndosoFacadeRemote {
	
	@EJB
	SolicitudFacadeRemote solicitud;
	@EJB
	CotizacionFacadeRemote cotizacion;
	@EJB
	EndosoFacadeRemote endosoFacade;
	
	@PersistenceContext
	private EntityManager entityManager;	
	
	@SuppressWarnings("unchecked")
	public List<EndosoDTO> listarEndososCancelables(BigDecimal idToPoliza) {
				
		LogDeMidasEJB3.log("CancelacionEndosoFacade.listarEndososCancelables: "
				+ " idToPoliza: " + idToPoliza, Level.INFO, null);
		try {

			StringBuffer sb =  new StringBuffer();
			sb.append("select model from EndosoDTO model where ");
			sb.append("model.claveTipoEndoso in (2) ");
			sb.append("and model.cancela is null ");
			sb.append("and model.id.numeroEndoso > :numeroEndoso ");
			sb.append("and model.id.idToPoliza = :idToPoliza ");
			sb.append("order by model.id.numeroEndoso");
						
			Query query = entityManager.createQuery(sb.toString());
			query.setParameter("idToPoliza", idToPoliza);
			query.setParameter("numeroEndoso", endosoFacade.buscarNumeroEndosoUltimoCFP(idToPoliza));
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("CancelacionEndosoFacade.listarEndososCancelables fallo", Level.SEVERE, re);
			throw re;
		}
	
	}
	
	@SuppressWarnings("unchecked")
	public List<EndosoDTO> listarEndososCancelablesPoliza(BigDecimal idToPoliza) {
				
		LogDeMidasEJB3.log("CancelacionEndosoFacade.listarEndososCancelablesPoliza: "
				+ " idToPoliza: " + idToPoliza, Level.INFO, null);
		try {

			StringBuffer sb =  new StringBuffer();
			sb.append("select model from EndosoDTO model where ");
			sb.append("model.claveTipoEndoso in (2,3) ");
			sb.append("and model.cancela is null ");
			sb.append("and model.id.numeroEndoso > :numeroEndoso ");
			sb.append("and model.id.idToPoliza = :idToPoliza ");
			sb.append("order by model.id.numeroEndoso");
						
			Query query = entityManager.createQuery(sb.toString());
			query.setParameter("idToPoliza", idToPoliza);
			query.setParameter("numeroEndoso", endosoFacade.buscarNumeroEndosoUltimoCFP(idToPoliza));
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("CancelacionEndosoFacade.listarEndososCancelablesPoliza fallo", Level.SEVERE, re);
			throw re;
		}
	
	}

	@SuppressWarnings("unchecked")
	public List<EndosoDTO> listarPosiblesEndososCancelables(BigDecimal idToPoliza, Short numeroEndoso) {
		LogDeMidasEJB3.log("CancelacionEndosoFacade.listarPosiblesEndososCancelables: "
				+ " idToPoliza: " + idToPoliza + ", numeroEndoso: " + numeroEndoso, Level.INFO, null);
		try {

			StringBuffer sb =  new StringBuffer();
			sb.append("select model from EndosoDTO model where ");
			sb.append("model.id.idToPoliza = :idToPoliza ");
			sb.append("and model.id.numeroEndoso > :numeroEndoso ");
			sb.append("and model.claveTipoEndoso not in (4,5,7,8) "); //Diferentes a: Canc. Poliza, Rehab. Poliza, CE y RE
			sb.append("order by model.id.numeroEndoso");
									
			Query query = entityManager.createQuery(sb.toString());
			query.setParameter("idToPoliza", idToPoliza);
			query.setParameter("numeroEndoso", numeroEndoso);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("CancelacionEndosoFacade.listarPosiblesEndososCancelables fallo", Level.SEVERE, re);
			throw re;
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public CotizacionDTO copiaCotizacion(EndosoDTO ultimoEndoso,short motivo,String usuarioCreacion) {
		
		BigDecimal tipoEndoso = SistemaPersistencia.ENDOSO_DE_MODIFICACION;
		
		CotizacionDTO cotizacionOrigen = cotizacion.findById(ultimoEndoso.getIdToCotizacion());
				
		SolicitudDTO solicitudDTO = copiaSolicitud(
				ultimoEndoso.getId().getIdToPoliza(),
				usuarioCreacion,
				ultimoEndoso.getFechaInicioVigencia(),
				cotizacionOrigen,
				tipoEndoso,
				null,
				Short.valueOf(SistemaPersistencia.SOLICITUD_ENDOSO));
		solicitud.save(solicitudDTO);
		LogDeMidasEJB3.log("se copio la solicitud", Level.INFO, null);
		CotizacionDTO cotizacionDTO = creaCopiaCotizacion(solicitudDTO, usuarioCreacion,cotizacionOrigen,motivo);
		cotizacionDTO.setSolicitudDTO(solicitudDTO);
		LogDeMidasEJB3.log("se copio la cotizacion", Level.INFO, null);			
		cotizacionDTO = cotizacion.save(cotizacionDTO);
		LogDeMidasEJB3.log("cotizacion guardada exitosamente IdToCotizacion="+cotizacionDTO.getIdToCotizacion(), Level.INFO, null);
		entityManager.merge(cotizacionDTO);			
		copiaRelacionesCotizacion(cotizacionOrigen, cotizacionDTO);
		
		return cotizacionDTO;
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public EndosoDTO creaEndosoCE(CotizacionDTO cotizacionEndosoCE, EndosoDTO endosoACancelar, EndosoDTO ultimoEndoso, 
			Double ivaCotizacion, boolean existeReciboPagado, List<ReciboDTO> recibos, EndosoIDTO endosoIDTO, boolean esRehabilitacion, Short grupo) {
		EndosoDTO endosoCE = null;
		try {
			
			short tipoEndoso = (esRehabilitacion ? SistemaPersistencia.TIPO_ENDOSO_RE : SistemaPersistencia.TIPO_ENDOSO_CE);
			
			if(endosoACancelar.getCotizacionDTO() == null){
				endosoACancelar.setCotizacionDTO(
						cotizacion.findById(endosoACancelar.getIdToCotizacion()));
			}
			
	//		-Se calcula el numero para el nuevo endoso
			Short numeroUltimoEndoso = ultimoEndoso.getId().getNumeroEndoso();
			Short numeroEndosoCE = Short.parseShort((numeroUltimoEndoso + 1) + "");
			
	//		-Se agrega el nuevo endoso a la Base de Datos
	//		-Se agrega el tipo de endoso CE (7) al campo clavetipoendoso		
			insertEndosoDeCotizacion(cotizacionEndosoCE.getIdToCotizacion(), endosoACancelar.getId().getIdToPoliza(),numeroEndosoCE,
					tipoEndoso, 1D);
			
			endosoCE = getUltimoEndoso(endosoACancelar.getId().getIdToPoliza());
			
//			-Se agrega numeroendosoACancelar al campo cancela
			endosoCE.setCancela(endosoACancelar.getId().getNumeroEndoso());
//			-Se agrega el grupo (el numero de endoso original, que provoco que se cancelara este endoso)
			endosoCE.setGrupo(grupo);
			
			endosoCE.setCotizacionDTO(cotizacionEndosoCE);
			
			//Dias por devengar
			double diasPorDevengar = Utilerias.obtenerDiasEntreFechas(
					endosoCE.getFechaInicioVigencia(),
					endosoCE.getFechaFinVigencia());				
			endosoCE.setFactorAplicacion(diasPorDevengar/365);		
			
			EndosoDTO endosoCero = findById(new EndosoId(endosoACancelar.getId().getIdToPoliza(),(short)0));							
			
			endosoCE = emitir(endosoCE, ivaCotizacion, diasPorDevengar, existeReciboPagado, recibos, ultimoEndoso, endosoIDTO,
					endosoCero.getFechaInicioVigencia(), endosoACancelar);
			
			actualizarEndosoEmitido(endosoCE, endosoIDTO, ultimoEndoso,ivaCotizacion);
		
			
			
		} catch (RuntimeException re) {	
			LogDeMidasEJB3.log("CancelacionEndosoFacade.creaEndoso fallo", Level.SEVERE, re);
			throw re;
		} catch (Exception e) {	
			LogDeMidasEJB3.log("CancelacionEndosoFacade.creaEndoso fallo", Level.SEVERE, e);
			try {
				throw e;
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}	
		
		return endosoCE;		
	}
	
	public EndosoDTO obtieneEndosoAnterior (EndosoId id) {
		LogDeMidasEJB3.log("CancelacionEndosoFacade.obtieneEndosoAnterior: "
				+ " idToPoliza: " + id.getIdToPoliza() + ", numeroEndoso: " + id.getNumeroEndoso(), Level.INFO, null);
		try {

			StringBuffer sb =  new StringBuffer();
			sb.append("select model from EndosoDTO model where ");
			sb.append("model.id.idToPoliza = :idToPoliza ");
			sb.append("and model.id.numeroEndoso < :numeroEndoso ");
			sb.append("order by model.id.numeroEndoso desc");
									
			Query query = entityManager.createQuery(sb.toString());
			query.setParameter("idToPoliza", id.getIdToPoliza());
			query.setParameter("numeroEndoso", id.getNumeroEndoso());
			query.setMaxResults(1);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
						
			return (EndosoDTO) query.getSingleResult();
					
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("CancelacionEndosoFacade.obtieneEndosoAnterior fallo", Level.SEVERE, re);
			throw re;
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void copiaIncisoConRelaciones (CotizacionDTO cotizacionOrigen, CotizacionDTO cotizacionDestino, BigDecimal numeroInciso) {
		copiaRelacionesCotizacionInciso(cotizacionOrigen, cotizacionDestino, numeroInciso);
	}

}
