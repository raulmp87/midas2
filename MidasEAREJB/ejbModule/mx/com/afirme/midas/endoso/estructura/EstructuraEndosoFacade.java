package mx.com.afirme.midas.endoso.estructura;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless
public class EstructuraEndosoFacade implements EstructuraEndosoFacadeRemote {

	/**
	 * Deshace la generacion de la estructura de un endoso a partir de una poliza o endoso anterior
	 * @param idToPoliza Id de la poliza
	 * @param numeroEndoso Numero del endoso de la poliza
	 * @param nombreUsuario Nombre del usuario logueado
	 * @return true si se deshizo correctamente la estructura del endoso
	 * @throws Exception
	 */
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public boolean deshaceEstructuraEndoso(BigDecimal idToPoliza, int numeroEndoso, String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			
			LogDeMidasEJB3.log("Entrando a EstructuraEndosoFacade.deshaceEstructuraEndoso..." + this, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.pkgDAN_Generales.spDAN_BorraEstructuraEndoso", StoredProcedureHelper.DATASOURCE_MIDAS);
					
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pNumeroEndoso", numeroEndoso);
			
			int result = storedHelper.ejecutaActualizar();
			
			LogDeMidasEJB3.log("Saliendo de EstructuraEndosoFacade.deshaceEstructuraEndoso..." + this, Level.INFO, null);
			
			if (result == 1) {
				return true;
			} 
			
			return false;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"MIDAS.pkgDAN_Generales.spDAN_BorraEstructuraEndoso", int.class, codErr, descErr);
			
			LogDeMidasEJB3.log("Excepcion en BD de EstructuraEndosoFacade.deshaceEstructuraEndoso..." + this, Level.WARNING, null);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en EstructuraEndosoFacade.deshaceEstructuraEndoso..." + this, Level.WARNING, null);
			throw e;
		}
	}

	/**
	 * Genera la estructura de un endoso a partir de una poliza o endoso anterior
	 * @param idToCotizacion Id de la cotizacion
	 * @param idToPoliza Id de la poliza
	 * @param numeroEndoso Numero del nuevo endoso de la poliza
	 * @param tipoEndoso Tipo de endoso
	 * @param idToCotizacionAnterior Id de la cotizacion anterior
	 * @param nombreUsuario Nombre del usuario logueado
	 * @return true si la estructura se genero correctamente
	 * @throws Exception
	 */
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public boolean generaEstructuraEndoso(BigDecimal idToCotizacion,
			BigDecimal idToPoliza, int numeroEndoso, Short tipoEndoso,
			BigDecimal idToCotizacionAnterior, String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			
			LogDeMidasEJB3.log("Entrando a EstructuraEndosoFacade.generaEstructuraEndoso..." + this, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.pkgDAN_Generales.spDAN_GeneraEstructuraEndoso", StoredProcedureHelper.DATASOURCE_MIDAS);
					
			storedHelper.estableceParametro("pIdToCotizacion", idToCotizacion);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pNumeroEndoso", numeroEndoso);
			storedHelper.estableceParametro("pClaveTipoEndoso", tipoEndoso);
			storedHelper.estableceParametro("pIdToCotizacionAnterior", idToCotizacionAnterior);
			
			int result = storedHelper.ejecutaActualizar();
			
			LogDeMidasEJB3.log("Saliendo de EstructuraEndosoFacade.generaEstructuraEndoso..." + this, Level.INFO, null);
			
			if (result == 1) {
				return true;
			} 
			
			return false;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"MIDAS.pkgDAN_Generales.spDAN_GeneraEstructuraEndoso", int.class, codErr, descErr);
			
			LogDeMidasEJB3.log("Excepcion en BD de EstructuraEndosoFacade.generaEstructuraEndoso..." + this, Level.WARNING, null);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en EstructuraEndosoFacade.generaEstructuraEndoso..." + this, Level.WARNING, null);
			throw e;
		}
	}

}
