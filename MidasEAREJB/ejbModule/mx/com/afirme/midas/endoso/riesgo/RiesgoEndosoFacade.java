package mx.com.afirme.midas.endoso.riesgo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity RiesgoEndosoDTO.
 * 
 * @see .RiesgoEndosoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class RiesgoEndosoFacade implements RiesgoEndosoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved RiesgoEndosoDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            RiesgoEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RiesgoEndosoDTO entity) {
		LogUtil.log("saving RiesgoEndosoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent RiesgoEndosoDTO entity.
	 * 
	 * @param entity
	 *            RiesgoEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoEndosoDTO entity) {
		LogUtil.log("deleting RiesgoEndosoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(RiesgoEndosoDTO.class, entity
					.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved RiesgoEndosoDTO entity and return it or a copy
	 * of it to the sender. A copy of the RiesgoEndosoDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            RiesgoEndosoDTO entity to update
	 * @return RiesgoEndosoDTO the persisted RiesgoEndosoDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoEndosoDTO update(RiesgoEndosoDTO entity) {
		LogUtil.log("updating RiesgoEndosoDTO instance", Level.INFO, null);
		try {
			RiesgoEndosoDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RiesgoEndosoDTO findById(RiesgoEndosoId id) {
		LogUtil.log("finding RiesgoEndosoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			RiesgoEndosoDTO instance = entityManager.find(
					RiesgoEndosoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoEndosoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoEndosoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoEndosoDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding RiesgoEndosoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoEndosoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoEndosoDTO entities.
	 * 
	 * @return List<RiesgoEndosoDTO> all RiesgoEndosoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoEndosoDTO> findAll() {
		LogUtil.log("finding all RiesgoEndosoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoEndosoDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void insertRiesgoEndosoDePoliza(BigDecimal idToCotizacion, BigDecimal idToPoliza, int numeroEndoso,short tipoEndoso,
			BigDecimal idToCotizacionAnterior){
		String queryString = "";
		if (numeroEndoso == 0){
			queryString ="INSERT INTO MIDAS.toRiesgoEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, idToCobertura, idToRiesgo, idTcSubRamo, valorSumaAsegurada, "+
			" valorCoaseguro, valorDeducible, valorCuota, valorPrimaNeta, valorRecargoPagoFrac, valorDerechos, valorBonifComision, valorBonifComRecPagoFrac,  "+
			" valorIVA, valorPrimaTotal, porcentajeComision, valorComision, valorComisionFinal, valorComisionRecPagoFrac, valorComFinalRecPagoFrac) "+
			" SELECT riesgopol.idToPoliza, 0, riesgopol.numeroInciso, riesgopol.idToSeccion, riesgopol.idToCobertura, riesgopol.idToRiesgo, riesgopol.idTcSubRamo, "+
			" riesgopol.valorSumaAsegurada, riesgopol.valorCoaseguro, riesgopol.valorDeducible, riesgopol.valorCuota, riesgopol.valorPrimaNeta, riesgopol.valorRecargoPagoFrac,"+
			" riesgopol.valorDerechos, riesgopol.valorBonifComision, riesgopol.valorBonifComRecPagoFrac, riesgopol.valorIVA, riesgopol.valorPrimaTotal, riesgopol.porcentajeComision,"+
			" riesgopol.valorComision, riesgopol.valorComisionFinal, riesgopol.valorComisionRecPagoFrac, riesgopol.valorComFinalRecPagoFrac "+
			" FROM MIDAS.toRiesgoPol riesgopol "+
			" WHERE riesgopol.idToPoliza = "+idToPoliza;							
		}else if(tipoEndoso == 4 || tipoEndoso == 5 || tipoEndoso == 6){	//CANCELACION , REHABILITACION o CAMBIO FORMA DE PAGO
			queryString ="INSERT INTO MIDAS.toRiesgoEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, idToCobertura, idToRiesgo, idTcSubRamo, valorSumaAsegurada, "+
			" valorCoaseguro, valorDeducible, valorCuota, valorPrimaNeta, valorRecargoPagoFrac, valorDerechos, valorBonifComision, valorBonifComRecPagoFrac,  "+
			" valorIVA, valorPrimaTotal, porcentajeComision, valorComision, valorComisionFinal, valorComisionRecPagoFrac, valorComFinalRecPagoFrac) "+
			" SELECT "+idToPoliza+", "+numeroEndoso+", riesgocot.numeroInciso, riesgocot.idToSeccion, riesgocot.idToCobertura, riesgocot.idToRiesgo, riesgocot.idTcSubRamo, "+
			" riesgocot.valorSumaAsegurada, riesgocot.valorCoaseguro, riesgocot.valorDeducible, coberturacot.valorcuota, riesgocot.valorPrimaNeta, 0,"+
			" 0, 0, 0, 0, 0, 0,"+
			" 0, 0, 0, 0 "+
			" FROM MIDAS.toriesgocot riesgocot ,MIDAS.tocoberturacot coberturacot, MIDAS.toseccioncot seccioncot "+
			" WHERE riesgocot.idToCotizacion =  "+idToCotizacion+ 
			" and coberturacot.idtocotizacion = riesgocot.idtocotizacion "+
			" and coberturacot.numeroinciso = riesgocot.numeroinciso "+
			" and coberturacot.idtoseccion = riesgocot.idtoseccion "+
			" and coberturacot.idtocobertura = riesgocot.idtocobertura "+
			" and coberturacot.clavecontrato =1 "+
			" and seccioncot.idtocotizacion = coberturacot.idtocotizacion "+
			" and seccioncot.numeroinciso = coberturacot.numeroinciso "+
			" and seccioncot.idtoseccion = coberturacot.idtoseccion "+
			" and seccioncot.clavecontrato =1 "+
			" and riesgocot.clavecontrato =1 ";	
		} else if (tipoEndoso == SistemaPersistencia.TIPO_ENDOSO_CE || tipoEndoso == SistemaPersistencia.TIPO_ENDOSO_RE) {
			queryString ="INSERT INTO MIDAS.toRiesgoEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, idToCobertura, idToRiesgo, idTcSubRamo, valorSumaAsegurada, "+
			" valorCoaseguro, valorDeducible, valorCuota, valorPrimaNeta, valorRecargoPagoFrac, valorDerechos, valorBonifComision, valorBonifComRecPagoFrac,  "+
			" valorIVA, valorPrimaTotal, porcentajeComision, valorComision, valorComisionFinal, valorComisionRecPagoFrac, valorComFinalRecPagoFrac) "+
			//Riesgos actuales
			" SELECT "+idToPoliza+", "+numeroEndoso+", riesgocot.numeroInciso, riesgocot.idToSeccion, riesgocot.idToCobertura, riesgocot.idToRiesgo, riesgocot.idTcSubRamo, "+
			" riesgocot.valorSumaAsegurada, riesgocot.valorCoaseguro, riesgocot.valorDeducible, coberturacot.valorcuota, riesgocot.valorPrimaNeta, 0,"+
			" 0, 0, 0, 0, 0, 0,"+
			" 0, 0, 0, 0 "+
			" FROM MIDAS.toMovimientoCotEnd movimientocotend, MIDAS.toCoberturaCot coberturacot, MIDAS.toRiesgoCot riesgocot"+
			" WHERE movimientocotend.idToCotizacion = "+idToCotizacion+
			" AND   movimientocotend.numeroSubinciso = 0 "+
			" AND   coberturacot.idToCotizacion = movimientocotend.idToCotizacion "+ 
			" AND   coberturacot.numeroInciso = movimientocotend.numeroInciso "+
			" AND   coberturacot.idToSeccion = movimientocotend.idToSeccion "+
			" AND   coberturacot.idToCobertura = movimientocotend.idToCobertura "+
			" AND   riesgocot.idToCotizacion = coberturacot.idToCotizacion "+
			" AND   riesgocot.numeroInciso = coberturacot.numeroInciso "+
			" AND   riesgocot.idToSeccion = coberturacot.idToSeccion "+
			" AND   riesgocot.idToCobertura = coberturacot.idToCobertura " + 
			
			" UNION " + 
			//Riesgos eliminados
		    " SELECT " + idToPoliza + ", " + numeroEndoso + ", riesgocot.numeroInciso, riesgocot.idToSeccion, riesgocot.idToCobertura, riesgocot.idToRiesgo, riesgocot.idTcSubRamo, "+
			" riesgocot.valorSumaAsegurada, riesgocot.valorCoaseguro, riesgocot.valorDeducible, coberturacot.valorcuota, riesgocot.valorPrimaNeta * -1, 0,"+
			" 0, 0, 0, 0, 0, 0,"+
			" 0, 0, 0, 0 "+
			" FROM MIDAS.toRiesgoCot riesgocot, MIDAS.toCoberturaCot coberturacot " +
			" inner join MIDAS.toseccioncot seccioncot on seccioncot.idtocotizacion = coberturacot.idtocotizacion and seccioncot.numeroinciso = coberturacot.numeroinciso and seccioncot.idtoseccion = coberturacot.idtoseccion "+
			" WHERE riesgocot.idToCotizacion =  "+ idToCotizacionAnterior  +
			" AND   riesgocot.idToCotizacion = coberturacot.idToCotizacion "+
			" AND   riesgocot.numeroInciso = coberturacot.numeroInciso  "+
			" AND   riesgocot.idToSeccion = coberturacot.idToSeccion  "+
			" AND   riesgocot.idToCobertura = coberturacot.idToCobertura "+
			" AND coberturacot.clavecontrato = 1 " +
			" and seccioncot.clavecontrato = 1 " +
			" and riesgocot.clavecontrato = 1 "+
			" AND riesgocot.idtoriesgo  "+
			"    not in ( "+
			"        SELECT  idtoriesgo "+
			"        FROM MIDAS.toRiesgoCot  "+
			"        WHERE idtocotizacion =  " + idToCotizacion +
			"        and numeroInciso = riesgocot.numeroInciso "+
			"        and idToSeccion = riesgocot.idToSeccion "+
			"        and idToCobertura = riesgocot.idToCobertura "+
			"        and idtoriesgo = riesgocot.idtoriesgo "+
			"    ) ";
			 
			
			
			
		} else{
			queryString ="INSERT INTO MIDAS.toRiesgoEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, idToCobertura, idToRiesgo, idTcSubRamo, valorSumaAsegurada, "+
			" valorCoaseguro, valorDeducible, valorCuota, valorPrimaNeta, valorRecargoPagoFrac, valorDerechos, valorBonifComision, valorBonifComRecPagoFrac,  "+
			" valorIVA, valorPrimaTotal, porcentajeComision, valorComision, valorComisionFinal, valorComisionRecPagoFrac, valorComFinalRecPagoFrac) "+
			" SELECT "+idToPoliza+", "+numeroEndoso+", riesgocot.numeroInciso, riesgocot.idToSeccion, riesgocot.idToCobertura, riesgocot.idToRiesgo, riesgocot.idTcSubRamo, "+
			" riesgocot.valorSumaAsegurada, riesgocot.valorCoaseguro, riesgocot.valorDeducible, coberturacot.valorcuota, riesgocot.valorPrimaNeta, 0,"+
			" 0, 0, 0, 0, 0, 0,"+
			" 0, 0, 0, 0 "+
			" FROM MIDAS.toMovimientoCotEnd movimientocotend, MIDAS.toCoberturaCot coberturacot, MIDAS.toRiesgoCot riesgocot"+
			" WHERE movimientocotend.idToCotizacion = "+idToCotizacion+
			" AND   movimientocotend.numeroSubinciso = 0 "+
			" AND   coberturacot.idToCotizacion = movimientocotend.idToCotizacion "+ 
			" AND   coberturacot.numeroInciso = movimientocotend.numeroInciso "+
			" AND   coberturacot.idToSeccion = movimientocotend.idToSeccion "+
			" AND   coberturacot.idToCobertura = movimientocotend.idToCobertura "+
			" AND   riesgocot.idToCotizacion = coberturacot.idToCotizacion "+
			" AND   riesgocot.numeroInciso = coberturacot.numeroInciso "+
			" AND   riesgocot.idToSeccion = coberturacot.idToSeccion "+
			" AND   riesgocot.idToCobertura = coberturacot.idToCobertura ";			
		}
	
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();			
			
	}
	
	@SuppressWarnings("unchecked")
	public List<RiesgoEndosoDTO> listarFiltrado(RiesgoEndosoId id){
		try {
			String queryString = "select model from RiesgoEndosoDTO as model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (id == null)
				return null;		
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToPoliza", id.getIdToPoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.numeroEndoso", id.getNumeroEndoso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.numeroInciso", id.getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToSeccion", id.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToCobertura", id.getIdToCobertura());		
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToRiesgo", id.getIdToRiesgo());				
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return query.getResultList();			
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find RiesgoEndosoDTO failed", Level.SEVERE,
					re);
			throw re;
		}
	}
}