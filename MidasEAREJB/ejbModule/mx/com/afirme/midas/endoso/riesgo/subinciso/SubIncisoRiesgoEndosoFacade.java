package mx.com.afirme.midas.endoso.riesgo.subinciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity SubIncisoRiesgoEndosoDTO.
 * 
 * @see .SubIncisoRiesgoEndosoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SubIncisoRiesgoEndosoFacade implements
		SubIncisoRiesgoEndosoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SubIncisoRiesgoEndosoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SubIncisoRiesgoEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubIncisoRiesgoEndosoDTO entity) {
		LogUtil.log("saving SubIncisoRiesgoEndosoDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SubIncisoRiesgoEndosoDTO entity.
	 * 
	 * @param entity
	 *            SubIncisoRiesgoEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubIncisoRiesgoEndosoDTO entity) {
		LogUtil.log("deleting SubIncisoRiesgoEndosoDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(SubIncisoRiesgoEndosoDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SubIncisoRiesgoEndosoDTO entity and return it
	 * or a copy of it to the sender. A copy of the SubIncisoRiesgoEndosoDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SubIncisoRiesgoEndosoDTO entity to update
	 * @return SubIncisoRiesgoEndosoDTO the persisted SubIncisoRiesgoEndosoDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubIncisoRiesgoEndosoDTO update(SubIncisoRiesgoEndosoDTO entity) {
		LogUtil.log("updating SubIncisoRiesgoEndosoDTO instance", Level.INFO,
				null);
		try {
			SubIncisoRiesgoEndosoDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SubIncisoRiesgoEndosoDTO findById(SubIncisoRiesgoEndosoId id) {
		LogUtil.log("finding SubIncisoRiesgoEndosoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SubIncisoRiesgoEndosoDTO instance = entityManager.find(
					SubIncisoRiesgoEndosoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubIncisoRiesgoEndosoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SubIncisoRiesgoEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubIncisoRiesgoEndosoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoRiesgoEndosoDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding SubIncisoRiesgoEndosoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SubIncisoRiesgoEndosoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubIncisoRiesgoEndosoDTO entities.
	 * 
	 * @return List<SubIncisoRiesgoEndosoDTO> all SubIncisoRiesgoEndosoDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoRiesgoEndosoDTO> findAll() {
		LogUtil.log("finding all SubIncisoRiesgoEndosoDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from SubIncisoRiesgoEndosoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void insertSubIncisoRiesgoEndosoDeCotizacion(BigDecimal idToCotizacion, BigDecimal idToPoliza, int numeroEndoso, double diasPorDevengar,short tipoEndoso){
		String queryString = "";
		if (numeroEndoso ==0){
			queryString ="INSERT INTO MIDAS.toSubincisoRgoEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, numeroSubinciso, idToCobertura, "+
			" idtoriesgo, idTcSubramo, valorSumaAsegurada, valorPrimaNeta) "+
			" SELECT subincisorgopol.idToPoliza, 0, subincisorgopol.numeroInciso, subincisorgopol.idToSeccion, subincisorgopol.numeroSubinciso, "+
			" subincisorgopol.idToCobertura, subincisorgopol.idToRiesgo,  "+
			" subincisorgopol.idTcSubramo, subincisorgopol.valorSumaAsegurada, subincisorgopol.valorPrimaNeta * ("+ diasPorDevengar+ "/365)"+
			" FROM MIDAS.toSubincisoRgoPol subincisorgopol "+
			" WHERE subincisorgopol.idToPoliza = "+idToPoliza;							
		}else if(tipoEndoso == 4 || tipoEndoso == 5 || tipoEndoso == 6){	//CANCELACION , REHABILITACION o CAMBIO FORMA DE PAGO
			queryString ="INSERT INTO MIDAS.toSubincisoRgoEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, numeroSubinciso, idToCobertura, "+
			" idtoriesgo, idTcSubramo, valorSumaAsegurada, valorPrimaNeta) "+
			" SELECT "+idToPoliza+", "+numeroEndoso+", detprirgocot.numeroInciso, detprirgocot.idToSeccion, detprirgocot.numeroSubinciso, "+
			" detprirgocot.idToCobertura, detprirgocot.idToRiesgo,  "+
			" riesgocot.idTcSubramo, 0, 0"+
			" FROM MIDAS.toDetPrimaRiesgoCot detprirgocot, MIDAS.toRiesgoCot riesgocot ,MIDAS.tocoberturacot coberturacot, MIDAS.toseccioncot seccioncot"+
			" WHERE detprirgocot.idToCotizacion  = "+idToCotizacion+
			" AND   riesgocot.idToCotizacion = detprirgocot.idToCotizacion "+
			" AND   riesgocot.numeroInciso = detprirgocot.numeroInciso "+
			" AND   riesgocot.idToSeccion = detprirgocot.idToSeccion "+ 
			" AND   riesgocot.idToCobertura = detprirgocot.idToCobertura "+
			" AND   riesgocot.idToRiesgo = detprirgocot.idToRiesgo "+			
			" and coberturacot.idtocotizacion = riesgocot.idtocotizacion "+
			" and coberturacot.numeroinciso = riesgocot.numeroinciso "+
			" and coberturacot.idtoseccion = riesgocot.idtoseccion "+
			" and coberturacot.idtocobertura = riesgocot.idtocobertura "+
			" and coberturacot.clavecontrato =1 "+
			" and seccioncot.idtocotizacion = coberturacot.idtocotizacion "+
			" and seccioncot.numeroinciso = coberturacot.numeroinciso "+
			" and seccioncot.idtoseccion = coberturacot.idtoseccion "+
			" and seccioncot.clavecontrato =1 "+
			" and riesgocot.clavecontrato =1 ";				
		}else{
			queryString ="INSERT INTO MIDAS.toSubincisoRgoEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, numeroSubinciso, idToCobertura, "+
			" idtoriesgo, idTcSubramo, valorSumaAsegurada, valorPrimaNeta) "+
			" SELECT "+idToPoliza+", "+numeroEndoso+", movimientocotend.numeroInciso, movimientocotend.idToSeccion, movimientocotend.numeroSubinciso, "+
			" movimientocotend.idToCobertura, detprirgocot.idToRiesgo,  "+
			" riesgocot.idTcSubramo, riesgocot.valorSumaAsegurada, detprirgocot.valorPrimaNeta * ("+ diasPorDevengar+ "/365)"+
			" FROM MIDAS.toMovimientoCotEnd movimientocotend, MIDAS.toDetPrimaRiesgoCot detprirgocot, MIDAS.toRiesgoCot riesgocot "+
			" WHERE movimientocotend.idToCotizacion = "+idToCotizacion+
			" AND   detprirgocot.idToCotizacion = movimientocotend.idToCotizacion "+ 
			" AND   detprirgocot.numeroInciso = movimientocotend.numeroInciso "+
			" AND   detprirgocot.idToSeccion = movimientocotend.idToSeccion "+
			" AND   detprirgocot.numeroSubinciso = movimientocotend.numeroSubinciso "+
			" AND   detprirgocot.idToCobertura = movimientocotend.idToCobertura "+
			" AND   riesgocot.idToCotizacion = detprirgocot.idToCotizacion "+
			" AND   riesgocot.numeroInciso = detprirgocot.numeroInciso "+
			" AND   riesgocot.idToSeccion = detprirgocot.idToSeccion "+ 
			" AND   riesgocot.idToCobertura = detprirgocot.idToCobertura "+
			" AND   riesgocot.idToRiesgo = detprirgocot.idToRiesgo " +
			" GROUP BY "+idToPoliza+", "+numeroEndoso+", movimientocotend.numeroInciso, movimientocotend.idToSeccion, movimientocotend.numeroSubinciso, "+
			" movimientocotend.idToCobertura, detprirgocot.idToRiesgo,  "+
			" riesgocot.idTcSubramo, riesgocot.valorSumaAsegurada, detprirgocot.valorPrimaNeta * ("+ diasPorDevengar+ "/365)";
		}

		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();			
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SubIncisoRiesgoEndosoDTO> listarFiltrado(SubIncisoRiesgoEndosoDTO subIncisoRiesgoEndosoDTO) {
		
		try {
		        String queryString = "select model from SubIncisoRiesgoEndosoDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (subIncisoRiesgoEndosoDTO == null)
				return null;
			else
				if (subIncisoRiesgoEndosoDTO.getId() == null)
				    subIncisoRiesgoEndosoDTO.setId(new SubIncisoRiesgoEndosoId()); // Avoid Null Pointer Exception
						
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToPoliza", subIncisoRiesgoEndosoDTO.getId().getIdToPoliza(), "idToPoliza");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.numeroEndoso", subIncisoRiesgoEndosoDTO.getId().getNumeroEndoso(), "numeroEndoso");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.numeroInciso", subIncisoRiesgoEndosoDTO.getId().getNumeroInciso(), "numeroInciso");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToSeccion", subIncisoRiesgoEndosoDTO.getId().getIdToSeccion(), "idToSeccion");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.numeroSubInciso", subIncisoRiesgoEndosoDTO.getId().getNumeroSubInciso(), "numeroSubInciso");
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				
			}   	
		    	query = entityManager.createQuery(queryString);
    			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
    			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
    			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
}