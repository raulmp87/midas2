package mx.com.afirme.midas.endoso;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoFacadeRemote;
import mx.com.afirme.midas.endoso.recibo.ReciboInterfazServiciosRemote;
import mx.com.afirme.midas.endoso.rehabilitacion.EndosoRehabilitableDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.usuario.LogUtil;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity EndosoDTO.
 * 
 * @see .EndosoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class EndosoFacade extends EndosoSoporteEmision implements EndosoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	@Resource
	private SessionContext context;	
	
	@EJB
	private SoporteReaseguroFacadeRemote soporteReaseguroFacade;
	
	@EJB
	private MovimientoCotizacionEndosoFacadeRemote movimientoCotEndFacade;
	
	@EJB
	private CotizacionFacadeRemote cotizacionFacade;
	
	@EJB
	private ReciboInterfazServiciosRemote reciboInterfazFacade;
	
	@EJB
	private EntidadService entidadService;
	
	
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	/**
	 * Perform an initial save of a previously unsaved EndosoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            EndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public EndosoDTO save(EndosoDTO entity) {
		LogUtil.log("saving EndosoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent EndosoDTO entity.
	 * 
	 * @param entity
	 *            EndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(EndosoDTO entity) {
		LogUtil.log("deleting EndosoDTO instance", Level.INFO, null);
		try {
			entity = entityManager
					.getReference(EndosoDTO.class, entity.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved EndosoDTO entity and return it or a copy of it
	 * to the sender. A copy of the EndosoDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            EndosoDTO entity to update
	 * @return EndosoDTO the persisted EndosoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public EndosoDTO update(EndosoDTO entity) {
		LogUtil.log("updating EndosoDTO instance", Level.INFO, null);
		try {
			EndosoDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public EndosoDTO findById(EndosoId id) {
		LogUtil.log("finding EndosoDTO instance with id: " + id, Level.INFO,
				null);
		try {
			EndosoDTO instance = entityManager.find(EndosoDTO.class, id);
			entityManager.refresh(instance);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all EndosoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the EndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<EndosoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<EndosoDTO> findByProperty(String propertyName,
			final Object value, boolean ascendingOrder) {
		LogUtil.log("finding EndosoDTO instance with property: " + propertyName
				+ ", value: " + value, Level.INFO, null);
		try {
			String queryString = "select model from EndosoDTO model where model."
					+ propertyName + "= :propertyValue order by model.id.numeroEndoso ";
			if(ascendingOrder) {
				queryString += "asc";
			} else {
				queryString += "desc";
			}
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
//	@SuppressWarnings("unchecked")
//	public List<EndosoDTO> findByPropertyWhitDescriptions(String propertyName,
//			final Object value, boolean ascendingOrder) {
//
//			List<EndosoDTO> list = this.findByProperty(propertyName, value, ascendingOrder);
//			
//			int index=0;
//			for(EndosoDTO item : list){
//				list.get(index).setDescripcionNumeroEndoso(this.getNumeroEndoso(item.getId().getNumeroEndoso().intValue()));
//				if(item.getId().getNumeroEndoso()>0){
//					Map<String, Object> values = new LinkedHashMap<String, Object>();
//					values.put("cotizacionId", item.getIdToCotizacion().longValue());
//					values.put("recordFrom", item.getRecordFrom());
//					List<ControlEndosoCot> controlEndosoCotList = (List<ControlEndosoCot>)entidadService.findByProperties(ControlEndosoCot.class,values);
//					if(!controlEndosoCotList.isEmpty()&&controlEndosoCotList.get(0)!=null){
//						ControlEndosoCot controlEndosoCot = controlEndosoCotList.get(0);
//						list.get(index).setDescripcionTipoEndoso(this.getDescripcionClaveTipoEndoso(controlEndosoCot.getSolicitud()));
//						list.get(index).setDescripcionMotivoEndoso(this.getDescripcionClaveMotivoEndoso(controlEndosoCot.getSolicitud()));
//					}
//					
//				}else{
//					list.get(index).setDescripcionTipoEndoso("");
//					list.get(index).setDescripcionMotivoEndoso("");
//				}
//				index++;
//			}
//			return list;
//	}
	
	public String getNumeroEndoso(Integer numeroEndoso){
		if (numeroEndoso == 0)
			return "Poliza Original";
		else
			return numeroEndoso.toString();
	}
	
	public String getDescripcionClaveTipoEndoso(SolicitudDTO solicitudDTO){

		String result = "";
		if(solicitudDTO.getClaveTipoEndoso() != null) {
			int tipoEndoso = solicitudDTO.getClaveTipoEndoso().intValueExact();
			if (tipoEndoso == 0)
				result = "";
			else{
				switch(tipoEndoso){
					case 5:
						result = "ALTA DE INCISO";
						break;
					case 6:
						result = "BAJA DE INCISO";
						break;
					case 7:
						result = "CAMBIO DE DATOS";
						break;
					case 8:
						result = "CAMBIO DE AGENTE";
						break;
					case 9:
						result = "CAMBIO DE FORMA DE PAGO";
						break;
					case 10:
						result = "CANCELACI\u00D3N DE ENDOSO";
						break;
					case 11:
						result = "CANCELACI\u00D3N DE P\u00D3LIZA";
						break;
					case 12:
						result = "EXTENSI\u00D3N DE VIGENCIA";
						break;
					case 13:
						result = "INCLUSI\u00D3N DE ANEXO";
						break;
					case 14:
						result = "INCLUSI\u00D3N DE TEXTO";
						break;
					case 15:
						result = "DE MOVIMIENTOS";
						break;
					case 16:
						result = "REHABILITACI\u00D3N DE INCISOS";
						break;
					case 17:
						result = "REHABILITACI\u00D3N DE ENDOSO DE CANCELACI\u00D3N DE ENDOSO";
						break;
					case 18:
						result = "REHABILITACI\u00D3N DE P\u00D3LIZA";
						break;
					case 19:
						result = "AJUSTE DE PRIMA";
						break;
					case 20:
						result = "EXCLUSI\u00D3N DE TEXTO";
						break;
					case 21:
						result = "CAMBIO DE IVA";
						break;
					case 22:
						result = "INCLUSI\u00D3N DE CONDICIONES ESPECIALES";
						break;
					case 23:
						result = "BAJA DE CONDICIONES ESPECIALES";
						break;
					case 24:
						result = "BAJA DE INCISO PERDIDA TOTAL"; 
						break;
					case 25:
						result = "CANCELACION DE POLIZA PERDIDA TOTAL";
						break;
					case 26:
						result = "DESAGRUPACION DE RECIBOS";
						break;
					case 27:
						result = "CAMBIO DE CONDUCTO DE COBRO";
						break;	
					default:
						result = "No disponible";
						break;
				}
			}
		}
		return result;
	}
	
	public String getDescripcionClaveMotivoEndoso(SolicitudDTO solicitudDTO){
		String result = "";
		if(solicitudDTO.getClaveMotivoEndoso()!=null) {
			int motivoEndoso = solicitudDTO.getClaveMotivoEndoso();
			if (motivoEndoso == 0)
				result = "";
			else{
				switch(motivoEndoso){
					case 8:
						result = "POR COMPETENCIA";
						break;
					case 9:
						result = "VENTA DE UNIDAD";
						break;
					case 10:
						result = "PERDIDA TOTAL";
						break;
					case 11:
						result = "ROBO TOTAL";
						break;
					case 12:
						result = "MAL SERVICIO SINIESTRO";
						break;
					case 13:
						result = "MAL SERVICIO COBRANZA";
						break;
					case 14:
						result = "MAL SERVICIO VENTAS";
						break;
					case 15:
						result = "CAMBIO DE AGENTE";
						break;
					case 16:
						result = "SE FUE A VENTA DIRECTA";
						break;
					case 17:
						result = "ALTA SINIESTRALIDAD";
						break;
					case 1:
					case 18:
						result = "INTERES PERSONAL DEL CLIENTE";
						break;
					case 19:
						result = "CAMBIO DE P\u00D3LIZA";
						break;
					case 20:
						result = "DUPLICIDAD";
						break;
					case 7:
					case 21:
						result = "POR FALTA DE PAGO";
						break;
					case 22:
						result = "PAGO DA\u00D1OS SUSTITUCION PERDIDA TOTAL"; 
						break;						
					default:
						result = "NO DISPONIBLE";
						break;
				}
			}
		}
		return result;
	}

	/**
	 * Find all EndosoDTO entities.
	 * 
	 * @return List<EndosoDTO> all EndosoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<EndosoDTO> findAll() {
		LogUtil.log("finding all EndosoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from EndosoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public EndosoDTO getUltimoEndoso(BigDecimal idToPoliza) {
		LogUtil.log("getUltimoEndoso", Level.INFO, null);
		try {
			final String queryString = "select model from EndosoDTO model " +
					"where model.id.idToPoliza = :idToPoliza and model.id.numeroEndoso = (" +
					"select max(endoso.id.numeroEndoso) from EndosoDTO endoso " +
					"where endoso.id.idToPoliza = :idToPoliza)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToPoliza", idToPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List result = query.getResultList();
			if(!result.isEmpty()) {
				return (EndosoDTO) result.get(0);
			} else {
				return null;
			}
		} catch (RuntimeException re) {
			LogUtil.log("getUltimoEndoso failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public BigDecimal obtenerCantidadEndososPoliza(BigDecimal idToPoliza){
		LogDeMidasEJB3.log("Contando cantidad de endosos de la p�liza: "+idToPoliza, Level.INFO, null);
		try {
			final String queryString = "select count (model.id.numeroEndoso) from EndosoDTO model where model.id.idToPoliza = :idToPoliza";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToPoliza", idToPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			Long cantidadEndosos = (Long) query.getSingleResult();
			if (cantidadEndosos == null)
				cantidadEndosos = Long.valueOf("0");
			BigDecimal cantidad = new BigDecimal(cantidadEndosos);
			return cantidad;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("FALL� la cuenta de endosos de la p�liza: "+idToPoliza, Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public EndosoDTO getPenultimoEndoso(BigDecimal idToPoliza) {
		LogUtil.log("getPenultimoEndoso", Level.INFO, null);
		try {
			String queryString = "select model from EndosoDTO model " +
				"where model.id.idToPoliza = :idToPoliza and model.id.numeroEndoso = (" +
				"select max(endoso.id.numeroEndoso) from EndosoDTO endoso " +
				"where endoso.id.idToPoliza = :idToPoliza)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToPoliza", idToPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List result = query.getResultList();
			if(!result.isEmpty()) {
				EndosoDTO  endosoDTO = (EndosoDTO) result.get(0);
				queryString = "";
				queryString =  "select model from EndosoDTO model " +
				"where model.id.idToPoliza = :idToPoliza and model.id.numeroEndoso = :numeroEndoso ";
				query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				query.setParameter("idToPoliza", idToPoliza);
				query.setParameter("numeroEndoso", endosoDTO.getId().getNumeroEndoso() -1);
				result = query.getResultList();
				if(!result.isEmpty()) {
					return (EndosoDTO) result.get(0);
				}else{
					return null;
				}				
			} else {
				return null;
			}
		} catch (RuntimeException re) {
			LogUtil.log("getPenultimoEndoso failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public EndosoDTO obtenerEndoso(BigDecimal idToPoliza,Short numeroEndoso) {
		LogUtil.log("buscando Endoso "+numeroEndoso+"de la poliza: "+idToPoliza, Level.INFO, null);
		EndosoDTO  endosoDTO = null;
		try {
			if(numeroEndoso == null)
				numeroEndoso = new Short((short)0);
			String queryString = "select model from EndosoDTO model where model.id.idToPoliza = :idToPoliza and model.id.numeroEndoso = :numeroEndoso";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToPoliza", idToPoliza);
			query.setParameter("numeroEndoso", numeroEndoso);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<EndosoDTO> result = query.getResultList();
			if(!result.isEmpty()) {
				endosoDTO = (EndosoDTO) result.get(0);
			} else {
				return null;
			}
		} catch (RuntimeException re) {
			LogUtil.log("getPenultimoEndoso failed", Level.SEVERE, re);
			throw re;
		}
		return endosoDTO;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<String, String> emitirEndoso(CotizacionDTO cotizacionDTO,Double ivaCotizacion, boolean existeReciboPagado, List<ReciboDTO> recibos, EndosoIDTO endosoIDTO) {
		
		Map<String, String> mensaje = new HashMap<String, String>();
		try{
			PolizaDTO polizaDTO = poliza.findById(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
			EndosoDTO ultimoEndoso = getUltimoEndoso(polizaDTO.getIdToPoliza());

			int numeroEndoso = ultimoEndoso.getId().getNumeroEndoso().intValue() + 1;
			
			short tipoEndoso = obtieneClaveTipoEndoso (cotizacionDTO.getIdToCotizacion(), 
					cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue());		
			
			insertEndosoDeCotizacion(cotizacionDTO.getIdToCotizacion(),polizaDTO.getIdToPoliza(),numeroEndoso,tipoEndoso, 1D);
			EndosoDTO endosoEmitido = getUltimoEndoso(polizaDTO.getIdToPoliza());
			endosoEmitido.setCotizacionDTO(cotizacionDTO);
			//Dias por devengar
			double diasPorDevengar = Utilerias.obtenerDiasEntreFechas(
					endosoEmitido.getFechaInicioVigencia(),
					endosoEmitido.getFechaFinVigencia());

			endosoEmitido.setCotizacionDTO(cotizacionDTO);
			endosoEmitido.setFactorAplicacion(diasPorDevengar/365);
			endosoIDTO.setIdPoliza(polizaDTO.getIdToPoliza());
			endosoIDTO.setNumeroEndoso((short)numeroEndoso);
			EndosoDTO endosoCero = findById(new EndosoId(polizaDTO.getIdToPoliza(),(short)0));				
			endosoEmitido  = emitir(endosoEmitido,ivaCotizacion,diasPorDevengar, existeReciboPagado, recibos, ultimoEndoso, endosoIDTO,endosoCero.getFechaInicioVigencia());
			ultimoEndoso.setCotizacionDTO(cotizacion.findById(ultimoEndoso.getIdToCotizacion()));
			actualizarEndosoEmitido(endosoEmitido, endosoIDTO, ultimoEndoso, ivaCotizacion);
			if(endosoEmitido.getClaveTipoEndoso() == TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO)
				actualizaEndosoCFP(endosoEmitido, ultimoEndoso,ivaCotizacion);
			mensaje.put("icono", "30");//Exito
			mensaje.put("mensaje", "El endoso se emiti\u00f3 correctamente.</br> El n\u00famero de  P\u00f3liza es: ");	
			mensaje.put("idpoliza", endosoEmitido.getId().getIdToPoliza().toString());
			mensaje.put("noendoso", endosoEmitido.getId().getNumeroEndoso().toString());
	
			return mensaje;
		} catch (RuntimeException re) {	
			context.setRollbackOnly();
			LogDeMidasEJB3.log("emitirEndoso failed", Level.SEVERE, re);
			mensaje.put("icono", "10");//Error
			mensaje.put("mensaje", "Error al generar el Endoso.");
			return mensaje;
		} catch (Exception e){
			context.setRollbackOnly();
			LogDeMidasEJB3.log("emitirEndoso failed", Level.SEVERE, e);
			mensaje.put("icono", "10");//Error
			mensaje.put("mensaje", "Error al generar el Endoso.");
			return mensaje;
		}
	}
	
	protected void actualizarEndosoEmitido(EndosoDTO endosoEmitido, EndosoIDTO endosoIDTO, EndosoDTO ultimoEndoso, double ivaCotizacion){
		//Se valida el tipo de cancelacion
		//para determinar el factor que se actualizara.
		//si es cancelacion de poliza o CE o RE y se cancelo en base a recibos:
		if((endosoEmitido.getClaveTipoEndoso().shortValue() == TIPO_ENDOSO_CANCELACION
				|| endosoEmitido.getClaveTipoEndoso().shortValue() == TIPO_ENDOSO_REHABILITACION
				|| endosoEmitido.getClaveTipoEndoso().shortValue() == SistemaPersistencia.TIPO_ENDOSO_RE
				|| endosoEmitido.getClaveTipoEndoso().shortValue() == SistemaPersistencia.TIPO_ENDOSO_CE) 
			&& endosoIDTO.getCalculo().equals(TIPO_CALCULO_SEYCOS)){
			//Dias por devengar
			double diasVigenciaEndosoActual = Utilerias.obtenerDiasEntreFechas(
					endosoEmitido.getFechaInicioVigencia(),
					endosoEmitido.getFechaFinVigencia());			
			double diasVigenciaEndosoAnterior = Utilerias.obtenerDiasEntreFechas(
					ultimoEndoso.getFechaInicioVigencia(),
					ultimoEndoso.getFechaFinVigencia());			
			BigDecimal factorEndoso =  BigDecimal.valueOf((diasVigenciaEndosoActual / diasVigenciaEndosoAnterior) * (endosoIDTO.getPrimaNeta()/endosoEmitido.getValorPrimaNeta()));
				
			//Factor Ajuste = Prima a Cancelar en base a Recibos / Prima a Cancelar en base a D�as No Devengados
			BigDecimal factorAjuste = BigDecimal.valueOf(endosoIDTO.getPrimaNeta() / endosoEmitido.getValorPrimaNeta());
			
			BigDecimal factorAjusteRPF = BigDecimal.ONE;
			if(endosoEmitido.getValorRecargoPagoFrac() != null && 
					endosoEmitido.getValorRecargoPagoFrac().doubleValue() != 0){
				factorAjusteRPF = BigDecimal.valueOf(endosoIDTO.getRecargoPF().doubleValue() / endosoEmitido.getValorRecargoPagoFrac().doubleValue());
			}
			
			aplicaFactorAjusteRiesgos(factorAjuste.abs().doubleValue(), factorAjusteRPF.abs().doubleValue(), endosoEmitido, ivaCotizacion);
			acumulaCoberturas(endosoEmitido);
			acumulaEndoso(endosoEmitido);
			//se actualiza el endoso con el factor
			endosoEmitido.setFactorAplicacion(factorEndoso.abs().doubleValue());
			
			//25/02/2011. Se agrega aplicaci�n del factor de ajuste al soporte de reaseguro para cuadrar emision - reaseguro
			soporteReaseguroFacade.aplicarFactorAjusteSoporteReaseguro(endosoEmitido, factorAjuste);
			
		}
		update(endosoEmitido);		
	}

	public void insertEndosoDePoliza(BigDecimal idToPoliza,int numeroEndoso, short tipoEndoso, double factorAplicacion){
		String queryString ="INSERT INTO MIDAS.toEndoso (idToPoliza, idToCotizacion, numeroEndoso, claveTipoEndoso, fechaInicioVigencia, fechaFinVigencia, valorPrimaNeta, "+
				" valorRecargoPagoFrac, valorDerechos, porcentajeBonifComision, valorBonifComision, valorIVA, valorPrimaTotal, valorBonifComRecPagoFrac, "+
				" valorComision, valorComisionRecPagoFrac, tipoCambio, llaveFiscal, fechaCreacion,factorAplicacion ) "+
				" SELECT poliza.idToPoliza, cotizacion.idToCotizacion, "+numeroEndoso+", "+tipoEndoso+", cotizacion.fechaInicioVigencia, cotizacion.fechaFinVigencia, "+
				" 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, cotizacion.tipoCambio, null, sysdate, "+factorAplicacion+
				" FROM MIDAS.toPoliza poliza, MIDAS.toCotizacion cotizacion "+
				" WHERE poliza.idToPoliza ="+idToPoliza +
				" AND   cotizacion.IdToCotizacion = poliza.idToCotizacion";
						
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();			
	}
	public void insertEndosoDeCotizacion(BigDecimal idToCotizacion,BigDecimal idToPoliza, int numeroEndoso, short tipoEndoso, double factorAplicacion){
		String queryString ="INSERT INTO MIDAS.toEndoso (idToPoliza, idToCotizacion, numeroEndoso, claveTipoEndoso, fechaInicioVigencia, fechaFinVigencia, valorPrimaNeta, "+
		" valorRecargoPagoFrac, valorDerechos, porcentajeBonifComision, valorBonifComision, valorIVA, valorPrimaTotal, valorBonifComRecPagoFrac, "+
		" valorComision, valorComisionRecPagoFrac, tipoCambio, llaveFiscal, fechaCreacion, factorAplicacion) "+
		" SELECT "+ idToPoliza +", cotizacion.idToCotizacion, "+numeroEndoso+", "+tipoEndoso+", cotizacion.fechaInicioVigencia, cotizacion.fechaFinVigencia, "+
		" 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, cotizacion.tipoCambio, null, sysdate, "+ factorAplicacion+
		" FROM MIDAS.toCotizacion cotizacion "+
		" WHERE cotizacion.IdToCotizacion =" + idToCotizacion;
				
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();	
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void emiteEndosoCancelacionAutomatica(
			CotizacionDTO cotizacionOrigen, String usuarioCreacion,
			Double ivaCotizacion, boolean existeReciboPagado, List<ReciboDTO> recibos, 
			EndosoIDTO endosoIDTO) {
		try{		
		
			SolicitudDTO solicitudDTO = copiaSolicitud(
					endosoIDTO.getIdPoliza(),
					usuarioCreacion,
					endosoIDTO.getFechaInicioVigencia(),
					cotizacionOrigen,
					SistemaPersistencia.ENDOSO_DE_CANCELACION,
					SistemaPersistencia.MOTIVO_CANCELACION_AUTOMATICA_POR_FALTA_DE_PAGO,
					Short.valueOf(SistemaPersistencia.SOLICITUD_ENDOSO));
			solicitud.save(solicitudDTO);
			LogDeMidasEJB3.log("se copio la solicitud", Level.INFO, null);
			CotizacionDTO cotizacionDTO = creaCopiaCotizacion(solicitudDTO, usuarioCreacion,cotizacionOrigen,SistemaPersistencia.MOTIVO_CANCELACION_AUTOMATICA_POR_FALTA_DE_PAGO);
			LogDeMidasEJB3.log("se copio la cotizacion", Level.INFO, null);			
			cotizacionDTO.setSolicitudDTO(solicitudDTO);
			cotizacionDTO = cotizacion.save(cotizacionDTO);
			LogDeMidasEJB3.log("cotizacion guardada exitosamente IdToCotizacion="+cotizacionDTO.getIdToCotizacion(), Level.INFO, null);
			entityManager.merge(cotizacionDTO);			
			copiaRelacionesCotizacion(cotizacionOrigen, cotizacionDTO);

			PolizaDTO polizaDTO = poliza.findById(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
			EndosoDTO ultimoEndoso = getUltimoEndoso(polizaDTO.getIdToPoliza());
			
			
			if(ultimoEndoso.getClaveTipoEndoso() == TIPO_ENDOSO_CANCELACION){
				throw new RuntimeException("El endoso " + endosoIDTO.getIdPoliza() + "|" + endosoIDTO.getNumeroEndoso() + " no se puede cancelar debido a su estatus");
			}
			
			int numeroEndoso = ultimoEndoso.getId().getNumeroEndoso().intValue() + 1;
			
			short tipoEndoso = obtieneClaveTipoEndoso (cotizacionDTO.getIdToCotizacion(), 
					cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue());		

			insertEndosoDeCotizacion(cotizacionDTO.getIdToCotizacion(),polizaDTO.getIdToPoliza(),numeroEndoso,tipoEndoso,1D);
			EndosoDTO endosoEmitido = getUltimoEndoso(polizaDTO.getIdToPoliza());
			endosoEmitido.setCotizacionDTO(cotizacionDTO);
			//Dias por devengar
			double diasPorDevengar = Utilerias.obtenerDiasEntreFechas(
					endosoEmitido.getFechaInicioVigencia(),
					endosoEmitido.getFechaFinVigencia());				
			endosoEmitido.setFactorAplicacion(diasPorDevengar/365);			
			EndosoDTO endosoCero = findById(new EndosoId(polizaDTO.getIdToPoliza(),(short)0));				
			endosoEmitido = emitir(endosoEmitido,ivaCotizacion, diasPorDevengar, existeReciboPagado, recibos, ultimoEndoso, endosoIDTO, endosoCero.getFechaInicioVigencia());
			ultimoEndoso.setCotizacionDTO(cotizacion.findById(ultimoEndoso.getIdToCotizacion()));
			endosoIDTO.setCalculo(TIPO_CALCULO_SEYCOS);
			actualizarEndosoEmitido(endosoEmitido, endosoIDTO, ultimoEndoso,ivaCotizacion);
			LogDeMidasEJB3.log("se emito el endoso de cancelacion automatica numeroEndoso="+endosoEmitido.getId().getNumeroEndoso(), Level.INFO, null);
			
		} catch (RuntimeException re) {	
			context.setRollbackOnly();
			LogDeMidasEJB3.log("emitirEndoso Cancelacion fallo", Level.SEVERE, re);
		} catch (Exception e) {	
			context.setRollbackOnly();
			LogDeMidasEJB3.log("emitirEndoso Cancelacion fallo", Level.SEVERE, e);
		}
	}
	protected void copiaRelacionesCotizacion(CotizacionDTO cotizacionOrigen, CotizacionDTO cotizacionDestino){
		
		String queryString = copiarDocumentosDigitales(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();				
		LogDeMidasEJB3.log("se copiaron los documentos digitales", Level.INFO, null);

		queryString = copiarDocumentosCotizacion(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();				
		LogDeMidasEJB3.log("se copiaron los documentos anexos de la cotizacion", Level.INFO, null);
		
		queryString = copiarDocumentosReaseguro(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();				
		LogDeMidasEJB3.log("se copiaron los documentos anexos de reaseguro", Level.INFO, null);		

		queryString = copiarTextosAdicionales(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();				
		LogDeMidasEJB3.log("se copiaron los textos  adicionales", Level.INFO, null);
		
		queryString = copiarIncisos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();				
		LogDeMidasEJB3.log("se copiaron los incisos", Level.INFO, null);
				
		queryString = copiarSecciones(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();	
		LogDeMidasEJB3.log("se copiaron las secciones", Level.INFO, null);
		
		queryString = copiarCoberturas(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();	
		LogDeMidasEJB3.log("se copiaron las coberturas", Level.INFO, null);
		
		queryString = copiarRiesgos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();			
		LogDeMidasEJB3.log("se copiaron los riesgos", Level.INFO, null);
		
		queryString = copiarAgrupaciones(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();		
		LogDeMidasEJB3.log("se copiaron las agrupaciones", Level.INFO, null);
		
		queryString = copiarsubIncisos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();	
		LogDeMidasEJB3.log("se copiaron los subincisos", Level.INFO, null);
		
		queryString = copiarDetallePrimaCobertura(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los detalles de prima cobertura", Level.INFO, null);
		
		queryString = copiarDetallePrimaRiesgo(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();		
		LogDeMidasEJB3.log("se copiaron los detalles de prima riesgo", Level.INFO, null);
		
		queryString = copiarComisiones(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();		
		LogDeMidasEJB3.log("se copiaron las comisiones", Level.INFO, null);

		queryString = copiarDatosIncisos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();		
		LogDeMidasEJB3.log("se copiaron los datos de inciso", Level.INFO, null);
		
		queryString = copiarAumentos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();			
		LogDeMidasEJB3.log("se copiaron los aumentos", Level.INFO, null);

		queryString = copiarRecargos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los recargos", Level.INFO, null);

		queryString = copiarDescuentos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los descuentos", Level.INFO, null);		
	}
	
	protected void copiaRelacionesCotizacionInciso(CotizacionDTO cotizacionOrigen, CotizacionDTO cotizacionDestino, 
			BigDecimal numeroInciso){
		String queryString = copiarIncisos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), numeroInciso);
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();				
		LogDeMidasEJB3.log("se copiaron los incisos", Level.INFO, null);
				
		queryString = copiarSecciones(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), numeroInciso);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();	
		LogDeMidasEJB3.log("se copiaron las secciones", Level.INFO, null);
		
		queryString = copiarCoberturas(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), numeroInciso);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();	
		LogDeMidasEJB3.log("se copiaron las coberturas", Level.INFO, null);
		
		queryString = copiarRiesgos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), numeroInciso);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();			
		LogDeMidasEJB3.log("se copiaron los riesgos", Level.INFO, null);
				
		queryString = copiarsubIncisos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), numeroInciso);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();	
		LogDeMidasEJB3.log("se copiaron los subincisos", Level.INFO, null);
		
		queryString = copiarDetallePrimaCobertura(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), numeroInciso);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los detalles de prima cobertura", Level.INFO, null);
		
		queryString = copiarDetallePrimaRiesgo(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), numeroInciso);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();		
		LogDeMidasEJB3.log("se copiaron los detalles de prima riesgo", Level.INFO, null);
		
		queryString = copiarDatosIncisos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), numeroInciso);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();		
		LogDeMidasEJB3.log("se copiaron los datos de inciso", Level.INFO, null);
		
		queryString = copiarAumentos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), numeroInciso);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();			
		LogDeMidasEJB3.log("se copiaron los aumentos", Level.INFO, null);

		queryString = copiarRecargos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), numeroInciso);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los recargos", Level.INFO, null);

		queryString = copiarDescuentos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion(), numeroInciso);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los descuentos", Level.INFO, null);		
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void emiteEndosoRehabilitacion(CotizacionDTO cotizacionOrigen,
			String usuarioCreacion, Double ivaCotizacion, EndosoRehabilitableDTO endosoaProcesar) {
		try{		
		
			SolicitudDTO solicitudDTO = copiaSolicitud(endosoaProcesar.getId()
					.getIdToPoliza(), usuarioCreacion, endosoaProcesar
					.getFechaInicioVigencia(), cotizacionOrigen,
					SistemaPersistencia.ENDOSO_DE_REHABILITACION,
					null,Short.valueOf(SistemaPersistencia.SOLICITUD_ENDOSO));
			solicitud.save(solicitudDTO);
			LogDeMidasEJB3.log("se copio la solicitud", Level.INFO, null);
			CotizacionDTO cotizacionDTO = creaCopiaCotizacion(solicitudDTO, usuarioCreacion,cotizacionOrigen,(short)1);//temporal el numero 1
			LogDeMidasEJB3.log("se copio la cotizacion", Level.INFO, null);			
			cotizacionDTO.setSolicitudDTO(solicitudDTO);
			cotizacionDTO = cotizacion.save(cotizacionDTO);
			LogDeMidasEJB3.log("cotizacion guardada exitosamente IdToCotizacion="+cotizacionDTO.getIdToCotizacion(), Level.INFO, null);
			entityManager.merge(cotizacionDTO);			
			copiaRelacionesCotizacion(cotizacionOrigen, cotizacionDTO);
			
			PolizaDTO polizaDTO = poliza.findById(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
			EndosoDTO ultimoEndoso = getUltimoEndoso(polizaDTO.getIdToPoliza());
			
			if(ultimoEndoso.getClaveTipoEndoso() == TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO){
				throw new RuntimeException("El endoso " + endosoaProcesar.getId().getIdToPoliza() + "|" + endosoaProcesar.getId().getNumeroEndoso() + " no se puede rehabilitar debido a su estatus");
			}
			int numeroEndoso = ultimoEndoso.getId().getNumeroEndoso().intValue() + 1;
			
			short tipoEndoso = obtieneClaveTipoEndoso (cotizacionDTO.getIdToCotizacion(), 
					cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue());		

			insertEndosoDeCotizacion(cotizacionDTO.getIdToCotizacion(),polizaDTO.getIdToPoliza(),numeroEndoso,tipoEndoso,1D);
			EndosoDTO endosoEmitido = getUltimoEndoso(polizaDTO.getIdToPoliza());
			endosoEmitido.setCotizacionDTO(cotizacionDTO);
			//Dias por devengar
			double diasPorDevengar = Utilerias.obtenerDiasEntreFechas(
					endosoEmitido.getFechaInicioVigencia(),
					endosoEmitido.getFechaFinVigencia());	

			endosoEmitido.setFactorAplicacion(diasPorDevengar/365);
			EndosoDTO endosoCero = findById(new EndosoId(polizaDTO.getIdToPoliza(),(short)0));	
			EndosoIDTO endosoIDTO = poblarEndosoInterfaz(endosoaProcesar);
			endosoEmitido = emitir(endosoEmitido,ivaCotizacion, diasPorDevengar, false, null, ultimoEndoso, endosoIDTO, endosoCero.getFechaInicioVigencia());
			
			//Se ajusta la emisi�n a lo requerido por Seycos
			endosoIDTO.setCalculo(TIPO_CALCULO_SEYCOS);
			actualizarEndosoEmitido(endosoEmitido, endosoIDTO, ultimoEndoso,ivaCotizacion);
			//
			
			LogDeMidasEJB3.log("se emito el endoso de rehabilitacion numeroEndoso="+endosoEmitido.getId().getNumeroEndoso(), Level.INFO, null);
			
		} catch (RuntimeException re) {	
			context.setRollbackOnly();
			LogDeMidasEJB3.log("emitirEndoso rehabilitacion fallo", Level.SEVERE, re);
		}
	}
	
	private EndosoIDTO poblarEndosoInterfaz (EndosoRehabilitableDTO endosoaProcesar){
		EndosoIDTO endosoIDTO = new EndosoIDTO();
		endosoIDTO.setIdPoliza(endosoaProcesar.getId().getIdToPoliza());
		endosoIDTO.setNumeroEndoso(endosoaProcesar.getId().getNumeroEndoso());
		endosoIDTO.setBonificacionComision(endosoaProcesar.getValorBonifComision());
		endosoIDTO.setBonificacionComisionRPF(endosoaProcesar.getValorBonifComisionRPF());
		endosoIDTO.setComision(endosoaProcesar.getValorComision());
		endosoIDTO.setComisionRPF(endosoaProcesar.getValorComisionRPF());
		endosoIDTO.setDerechos(endosoaProcesar.getValorDerechos());
		endosoIDTO.setFechaInicioVigencia(endosoaProcesar.getFechaInicioVigencia());
		endosoIDTO.setIva(endosoaProcesar.getValorIVA());
		endosoIDTO.setPrimaNeta(endosoaProcesar.getValorPrimaNeta());
		endosoIDTO.setPrimaTotal(endosoaProcesar.getValorPrimaTotal());
		endosoIDTO.setRecargoPF(endosoaProcesar.getValorRecargoPagoFrac());
		return endosoIDTO;
	}
	/**
	 * Obtiene las diferencias en cotizaciones de endoso
	 * @param idToCotizacion Id de la cotizacion
	 * @param nombreUsuario Nombre del usuario logueado
	 * @return Listado con las diferencias en cotizaciones de endoso
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<DiferenciaCotizacionEndosoDTO> obtenerDiferenciasCotizacionEndoso(
			BigDecimal idToCotizacion, String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			
			LogDeMidasEJB3.log("Entrando a EndosoFacade.obtenerDiferenciasCotizacionEndoso..." + this, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.pkgDAN_Generales.spDAN_DiferenciaCotizacion", StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper
			.estableceMapeoResultados(
					DiferenciaCotizacionEndosoDTO.class.getCanonicalName(),
										
					"idToSeccion," +
					"idToCobertura," +          
		           	"originalSumaAsegurada," +
		           	"originalCuota," +   
		           	"originalPrimaNeta," +
		           	"nuevaSumaAsegurada," +
		           	"nuevaCuota," +
		           	"nuevaPrimaNeta," +
		           	"diferenciaSumaAsegurada," +
		           	"diferenciaCuota," +    
		           	"diferenciaPrimaNeta",
					
		           	"idToSeccion," +
					"idToCobertura," +          
		           	"originalSumaAsegurada," +
		           	"originalCuota," +   
		           	"originalPrimaNeta," +
		           	"nuevaSumaAsegurada," +
		           	"nuevaCuota," +
		           	"nuevaPrimaNeta," +
		           	"diferenciaSumaAsegurada," +
		           	"diferenciaCuota," +    
		           	"diferenciaPrimaNeta");
			
			storedHelper.estableceParametro("pIdToCotizacion", idToCotizacion);
									
			List<DiferenciaCotizacionEndosoDTO> diferenciaCotizacionEndosoDTOList = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log("Saliendo de EndosoFacade.obtenerDiferenciasCotizacionEndoso..." + this, Level.INFO, null);
			
			return diferenciaCotizacionEndosoDTOList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"MIDAS.pkgDAN_Generales.spDAN_DiferenciaCotizacion", DiferenciaCotizacionEndosoDTO.class, codErr, descErr);
			
			LogDeMidasEJB3.log("Excepcion en BD de EndosoFacade.obtenerDiferenciasCotizacionEndoso..." + this, Level.WARNING, null);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en EndosoFacade.obtenerDiferenciasCotizacionEndoso..." + this, Level.WARNING, null);
			throw e;
		}
	}
	
	public EndosoDTO buscarPorCotizacion(BigDecimal idToCotizacion) {
		EndosoDTO endosoDTO = null;
		List<EndosoDTO> endosoDTOs = 
			findByProperty("idToCotizacion", idToCotizacion, true);
		if (endosoDTOs.size() == 1) {
			endosoDTO = endosoDTOs.get(0);
		} else if (endosoDTOs.size() > 1) {
			String errorMsg = "Existe mas de un endoso para un idToCotizacion: " + idToCotizacion;
			LogDeMidasEJB3.log("Excepcion en BD de EndosoFacade.obtenerDiferenciasCotizacionEndoso..." + this, Level.WARNING, null);
			throw new RuntimeException(errorMsg);
		}
		return endosoDTO;
	}
	
	/**
	 * Regresa el numero de Endoso del ultimo Endoso de Tipo Cambio de Forma de Pago en la P�liza
	 * @param idPoliza Id de la p�liza
	 * @return El numero de Endoso del ultimo Endoso de Tipo Cambio de Forma de Pago en la P�liza, en caso contrario devuelve 0
	 */
	@SuppressWarnings("unchecked")
	public Short buscarNumeroEndosoUltimoCFP(BigDecimal idPoliza) {
		
		LogDeMidasEJB3.log("EndosoFacade.buscarNumeroEndosoUltimoCFP: "
				+ " idPoliza: " + idPoliza, Level.INFO, null);
		try {

			StringBuffer sb =  new StringBuffer();
			sb.append("select model from EndosoDTO model where ");
			sb.append("model.id.idToPoliza = :idToPoliza ");
			sb.append("and model.claveTipoEndoso = 6 "); //Cambio Forma de Pago
			
			sb.append("order by model.id.numeroEndoso desc");
						
			Query query = entityManager.createQuery(sb.toString());
			query.setParameter("idToPoliza", idPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			List result = query.getResultList();
			if(!result.isEmpty()) {
				return ((EndosoDTO) (result.get(0))).getId().getNumeroEndoso();
			}else{
				return new Short("0");
			}			
			
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("EndosoFacade.buscarNumeroEndosoUltimoCFP fallo", Level.SEVERE, re);
			throw re;
		}
		
	}

	public Integer calculaTipoEndoso(CotizacionDTO cotizacionDTO) {
		 Integer tipoEndoso = new Integer(2);
		 if(cotizacionDTO != null && cotizacionDTO.getSolicitudDTO() != null){
			 if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CANCELACION){
				 tipoEndoso = Integer.valueOf(ConstantesCotizacion.TIPO_ENDOSO_CANCELACION);
			 }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == ConstantesCotizacion.SOLICITUD_ENDOSO_DE_REHABILITACION){
				 tipoEndoso = Integer.valueOf(ConstantesCotizacion.TIPO_ENDOSO_REHABILITACION);
			 }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
				 tipoEndoso = Integer.valueOf(ConstantesCotizacion.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO);
			 }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == ConstantesCotizacion.SOLICITUD_ENDOSO_DE_DECLARACION){
				 tipoEndoso = Integer.valueOf(ConstantesCotizacion.TIPO_ENDOSO_AUMENTO);
			 }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == ConstantesCotizacion.SOLICITUD_ENDOSO_DE_MODIFICACION){
					MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
					dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
					BigDecimal primaNetaCotizacion = BigDecimal.ZERO;
					List<MovimientoCotizacionEndosoDTO> movimientos = movimientoCotEndFacade.listarFiltrado(dto);
					for (MovimientoCotizacionEndosoDTO movimiento : movimientos) {
						if (movimiento.getIdToCobertura() != null && movimiento.getIdToCobertura().intValue() >0)
							primaNetaCotizacion = primaNetaCotizacion.add(movimiento.getValorDiferenciaPrimaNeta());
					}
					if(primaNetaCotizacion.doubleValue() == 0){
						tipoEndoso = Integer.valueOf(ConstantesCotizacion.TIPO_ENDOSO_CAMBIO_DATOS);
					}else if(primaNetaCotizacion.doubleValue() > 0){
						tipoEndoso = Integer.valueOf(ConstantesCotizacion.TIPO_ENDOSO_AUMENTO);
					}else if(primaNetaCotizacion.doubleValue() < 0){
						tipoEndoso = Integer.valueOf(ConstantesCotizacion.TIPO_ENDOSO_DISMINUCION);
					}
			 }
		 }else if(cotizacionDTO != null && cotizacionDTO.getIdToCotizacion() != null){
			 cotizacionDTO = cotizacionFacade.findById(cotizacionDTO.getIdToCotizacion());
			 tipoEndoso = calculaTipoEndoso(cotizacionDTO);
		 }
		 
		 return tipoEndoso;
	}
	
	public boolean primerReciboPagado(EndosoDTO endoso,String nombreUsuario) throws Exception{
		List<ReciboDTO> recibos = reciboInterfazFacade.consultaRecibos(endoso.getId().getIdToPoliza(),nombreUsuario);
		
		for(ReciboDTO recibo : recibos){
			if(recibo.getNumeroEndoso().equals(endoso.getId().getNumeroEndoso().toString()) &&
			   recibo.getSituacion().trim().equals(ConstantesCotizacion.RECIBO_PAGADO))
				return true;
		}
		return false;
	}
	
	
	@SuppressWarnings("unchecked")
	public EndosoDTO getEndosoByPolizaCotizacion(BigDecimal idToPoliza, BigDecimal idToCotizacion) {
		EndosoDTO result = new EndosoDTO();
		LogUtil.log("getEndosoByPolizaCotizacion", Level.INFO, null);
		try {
			final String queryString = "select model from EndosoDTO model " +
					" where model.id.idToPoliza = :idToPoliza " +
					" and model.idToCotizacion =:idToCotizacion " +
					" and model.id.numeroEndoso = 0 ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToPoliza", idToPoliza);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List queryResult = query.getResultList();
			if(!queryResult.isEmpty()) {
				result = (EndosoDTO) queryResult.get(0);
			}

		} catch (RuntimeException re) {
			LogUtil.log("getEndosoByPolizaCotizacion failed", Level.SEVERE, re);
			throw re;
		}
		return result;
	}
}