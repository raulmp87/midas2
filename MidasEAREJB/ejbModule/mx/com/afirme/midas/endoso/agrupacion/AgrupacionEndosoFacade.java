package mx.com.afirme.midas.endoso.agrupacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity AgrupacionEndosoDTO.
 * 
 * @see .AgrupacionEndosoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class AgrupacionEndosoFacade implements AgrupacionEndosoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved AgrupacionEndosoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            AgrupacionEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(AgrupacionEndosoDTO entity) {
		LogUtil.log("saving AgrupacionEndosoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent AgrupacionEndosoDTO entity.
	 * 
	 * @param entity
	 *            AgrupacionEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(AgrupacionEndosoDTO entity) {
		LogUtil.log("deleting AgrupacionEndosoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(AgrupacionEndosoDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved AgrupacionEndosoDTO entity and return it or a
	 * copy of it to the sender. A copy of the AgrupacionEndosoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            AgrupacionEndosoDTO entity to update
	 * @return AgrupacionEndosoDTO the persisted AgrupacionEndosoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public AgrupacionEndosoDTO update(AgrupacionEndosoDTO entity) {
		LogUtil.log("updating AgrupacionEndosoDTO instance", Level.INFO, null);
		try {
			AgrupacionEndosoDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public AgrupacionEndosoDTO findById(AgrupacionEndosoId id) {
		LogUtil.log("finding AgrupacionEndosoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			AgrupacionEndosoDTO instance = entityManager.find(
					AgrupacionEndosoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all AgrupacionEndosoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the AgrupacionEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<AgrupacionEndosoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<AgrupacionEndosoDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding AgrupacionEndosoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from AgrupacionEndosoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all AgrupacionEndosoDTO entities.
	 * 
	 * @return List<AgrupacionEndosoDTO> all AgrupacionEndosoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<AgrupacionEndosoDTO> findAll() {
		LogUtil.log("finding all AgrupacionEndosoDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from AgrupacionEndosoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
											   
	public void insertAgrupacionEndosoDePoliza(BigDecimal idToCotizacion, BigDecimal idToPoliza, int numeroEndoso, double diasPorDevengar,short tipoEndoso){
		String queryString = "";
		if (numeroEndoso == 0){
			queryString ="INSERT INTO MIDAS.toAgrupacionEnd (idToPoliza, numeroEndoso, numeroAgrupacion, claveTipoAgrupacion, idToSeccion, valorSumaAsegurada, valorCuota,valorPrimaNeta) "+
			" SELECT agrupacionpol.idToPoliza, 0, agrupacionpol.numeroAgrupacion, agrupacionpol.claveTipoAgrupacion, agrupacionpol.idToSeccion, "+
			" agrupacionpol.valorSumaAsegurada, agrupacionpol.valorCuota, agrupacionpol.valorPrimaNeta * ("+diasPorDevengar+"/365)"+
			" FROM MIDAS.toAgrupacionPol agrupacionpol WHERE agrupacionpol.idToPoliza = "+idToPoliza;							
		}else if(tipoEndoso == 4 || tipoEndoso == 5 || tipoEndoso == 6){	//CANCELACION , REHABILITACION o CAMBIO FORMA DE PAGO
			queryString ="INSERT INTO MIDAS.toAgrupacionEnd (idToPoliza, numeroEndoso, numeroAgrupacion, claveTipoAgrupacion, idToSeccion, valorSumaAsegurada, valorCuota,valorPrimaNeta) "+
			" SELECT "+idToPoliza+", "+numeroEndoso+", agrupacioncot.numeroAgrupacion, agrupacioncot.claveTipoAgrupacion, agrupacioncot.idToSeccion, "+
			" agrupacioncot.valorSumaAsegurada, agrupacioncot.valorCuota, agrupacioncot.valorPrimaNeta * ("+diasPorDevengar+"/365)"+
			" FROM MIDAS.toagrupacioncot agrupacioncot WHERE agrupacioncot.idToCotizacion = "+idToCotizacion;							
		}else{
			queryString ="INSERT INTO MIDAS.toAgrupacionEnd (idToPoliza, numeroEndoso, numeroAgrupacion, claveTipoAgrupacion, idToSeccion, valorSumaAsegurada, valorCuota,valorPrimaNeta) "+
			" SELECT "+idToPoliza+", "+numeroEndoso+", agrupacioncot.numeroAgrupacion, agrupacioncot.claveTipoAgrupacion, agrupacioncot.idToSeccion, "+
			" agrupacioncot.valorSumaAsegurada, agrupacioncot.valorCuota, agrupacioncot.valorPrimaNeta * ("+diasPorDevengar+"/365)"+
			" FROM MIDAS.toagrupacioncot agrupacioncot WHERE agrupacioncot.idToCotizacion = "+idToCotizacion;				
		}
		
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();				
	}
	@SuppressWarnings("unchecked")
	public List<AgrupacionEndosoDTO> listarFiltrado(AgrupacionEndosoId id){
		try {
			String queryString = "select model from AgrupacionEndosoDTO as model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (id == null)
				return null;		
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToPoliza", id.getIdToPoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.numeroEndoso", id.getNumeroEndoso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.numeroAgrupacion", id.getNumeroAgrupacion());
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"listarFiltrado CoberturaEndosoDTO failed",
					Level.SEVERE, re);
			throw re;
		}			
	}	
}