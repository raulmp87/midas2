package mx.com.afirme.midas.endoso;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.endoso.EndosoCoberturaDTO;
import mx.com.afirme.midas.interfaz.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;

@Stateless
public class EndosoInterfazServicios implements EndosoInterfazServiciosRemote{

	@EJB
	private EndosoFacadeRemote endosoFacadeInterfaz;
	
	public List<EndosoIDTO> emiteEndoso(String numPolizaEndoso,String nombreUsuario) throws Exception {
		return endosoFacadeInterfaz.emiteEndoso(numPolizaEndoso, nombreUsuario);
	}

	public List<EndosoCoberturaDTO> obtieneCoberturasAgrupadas(EndosoId id,String nombreUsuario) throws Exception {
		return obtieneCoberturasAgrupadas(id, nombreUsuario);
	}

	public List<EndosoIDTO> obtieneListaCancelables(Date fechaConsulta,
			String nombreUsuario) throws Exception {
		return endosoFacadeInterfaz.obtieneListaCancelables(fechaConsulta, nombreUsuario);
	}

	public List<EndosoIDTO> obtieneListaRehabilitables(Date fechaConsulta,
			String nombreUsuario) throws Exception {
		return endosoFacadeInterfaz.obtieneListaRehabilitables(fechaConsulta, nombreUsuario);
	}

	public EndosoIDTO validaEsCancelable(String numPolizaEndoso,
			String nombreUsuario, Date fechaCancelacion, Integer tipoEndoso)throws Exception {
		return endosoFacadeInterfaz.validaEsCancelable(numPolizaEndoso, nombreUsuario, fechaCancelacion, tipoEndoso);
	}

}
