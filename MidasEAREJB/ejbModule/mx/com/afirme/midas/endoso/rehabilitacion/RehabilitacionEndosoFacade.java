package mx.com.afirme.midas.endoso.rehabilitacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoFacade;
import mx.com.afirme.midas.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless
public class RehabilitacionEndosoFacade extends EndosoFacade implements	RehabilitacionEndosoFacadeRemote {

	@EJB
	EndosoFacadeRemote endosoFacade;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<EndosoDTO> listarEndososRehabilitables(BigDecimal idToPoliza) {
		
		LogDeMidasEJB3.log("RehabilitacionEndosoFacade.listarEndososRehabilitables: "
				+ " idToPoliza: " + idToPoliza, Level.INFO, null);
		try {

			StringBuffer sb =  new StringBuffer();
			sb.append("select model from EndosoDTO model where ");
			sb.append("model.grupo is not null ");
			sb.append("and model.claveTipoEndoso not in (7,8) ");
			sb.append("and model.id.idToPoliza = :idToPoliza ");
			sb.append("and model.id.numeroEndoso > :numeroEndoso ");
			sb.append("order by model.grupo, model.id.numeroEndoso");
						
			Query query = entityManager.createQuery(sb.toString());
			query.setParameter("idToPoliza", idToPoliza);
			query.setParameter("numeroEndoso", endosoFacade.buscarNumeroEndosoUltimoCFP(idToPoliza));
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<EndosoDTO> endosos = query.getResultList();
			
			Short grupoIterando = 0;
			List<EndosoDTO> endososRehabilitables = new ArrayList<EndosoDTO>();
			
			for (EndosoDTO endoso : endosos) {
				if (endoso.getGrupo().compareTo(grupoIterando) != 0) {
					endososRehabilitables.add(endoso);
					grupoIterando = endoso.getGrupo();	
				}
			}
			
			return endososRehabilitables;
			 
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("RehabilitacionEndosoFacade.listarEndososRehabilitables fallo", Level.SEVERE, re);
			throw re;
		}
		
	}

}
