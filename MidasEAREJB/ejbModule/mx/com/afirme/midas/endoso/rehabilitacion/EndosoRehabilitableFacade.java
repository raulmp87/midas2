package mx.com.afirme.midas.endoso.rehabilitacion;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity EndosoRehabilitableDTO.
 * 
 * @see mx.com.afirme.midas.endoso.rehabilitacion.EndosoRehabilitableDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
//@WebService(endpointInterface="mx.com.afirme.midas.endoso.rehabilitacion.EndosoRehabilitableWSEndPoint")
public class EndosoRehabilitableFacade implements EndosoRehabilitableFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved EndosoRehabilitableDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            EndosoRehabilitableDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(EndosoRehabilitableDTO entity) {
		LogUtil.log("saving EndosoRehabilitableDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			entityManager.flush();
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent EndosoRehabilitableDTO entity.
	 * 
	 * @param entity
	 *            EndosoRehabilitableDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(EndosoRehabilitableDTO entity) {
		LogUtil
				.log("deleting EndosoRehabilitableDTO instance", Level.INFO,
						null);
		try {
			entity = entityManager.getReference(EndosoRehabilitableDTO.class,
					entity.getId());
			entityManager.remove(entity);
			entityManager.flush();
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved EndosoRehabilitableDTO entity and return it or
	 * a copy of it to the sender. A copy of the EndosoRehabilitableDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            EndosoRehabilitableDTO entity to update
	 * @return EndosoRehabilitableDTO the persisted EndosoRehabilitableDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public EndosoRehabilitableDTO update(EndosoRehabilitableDTO entity) {
		LogUtil
				.log("updating EndosoRehabilitableDTO instance", Level.INFO,
						null);
		try {
			EndosoRehabilitableDTO result = entityManager.merge(entity);
			entityManager.flush();
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public EndosoRehabilitableDTO findById(EndosoRehabilitableId id) {
		LogUtil.log("finding EndosoRehabilitableDTO instance with id: " + id,
				Level.INFO, null);
		try {
			EndosoRehabilitableDTO instance = entityManager.find(
					EndosoRehabilitableDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all EndosoRehabilitableDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the EndosoRehabilitableDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<EndosoRehabilitableDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<EndosoRehabilitableDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding EndosoRehabilitableDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from EndosoRehabilitableDTO model where model."
					+ propertyName + "= :propertyValue"
					+ " order by model.id.idToPoliza, model.id.numeroEndoso";
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all EndosoRehabilitableDTO entities.
	 * 
	 * @return List<EndosoRehabilitableDTO> all EndosoRehabilitableDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<EndosoRehabilitableDTO> findAll() {
		LogUtil.log("finding all EndosoRehabilitableDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from EndosoRehabilitableDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}


	public boolean notificaRehabilitacion(EndosoRehabilitableDTO endoso) {
		
		try {
			
			LogDeMidasEJB3.log("EndosoRehabilitableFacade.notificaRehabilitacion->" +
					" Guardando el registro del endoso rehabilitado en la tabla temporal..." +
					"idPoliza=" + endoso.getId().getIdToPoliza() + ", numeroEndoso=" + endoso.getId().getNumeroEndoso() + 
					"Fecha de Inicio de Vigencia" + endoso.getFechaInicioVigencia(), Level.INFO, null);

			List<EndosoRehabilitableDTO> endosoExisteList = findByProperty("id", endoso.getId());
			
			if (endosoExisteList == null || endosoExisteList.isEmpty()) {
				save(endoso);
			}
			else{
				//21/12/2010. JLAB, si el registro ya existe, se valida el estatus, ya que se debe reemplazar por el nuevo registro, para procesarlo posteriormente
				EndosoRehabilitableDTO endosoRegistrado = endosoExisteList.get(0);
				//Si el estatus no es "En proceso", se reemplaza el registro.
				if(endosoRegistrado.getEstatusRegistro().compareTo(EndosoRehabilitableDTO.ESTATUS_REGISTRO_EN_PROCESO) != 0){
					delete(endosoRegistrado);
					save(endoso);
				}
			}
			
			return true;
		
		}  catch (RuntimeException re) {
			LogUtil.log("EndosoRehabilitableFacade.notificaRehabilitacion fallo", Level.SEVERE, re);
			throw re;
		}
	}

}