package mx.com.afirme.midas.endoso.subinciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity SubIncisoEndosoDTO.
 * 
 * @see .SubIncisoEndosoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SubIncisoEndosoFacade implements SubIncisoEndosoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SubIncisoEndosoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SubIncisoEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubIncisoEndosoDTO entity) {
		LogUtil.log("saving SubIncisoEndosoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SubIncisoEndosoDTO entity.
	 * 
	 * @param entity
	 *            SubIncisoEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubIncisoEndosoDTO entity) {
		LogUtil.log("deleting SubIncisoEndosoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SubIncisoEndosoDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SubIncisoEndosoDTO entity and return it or a
	 * copy of it to the sender. A copy of the SubIncisoEndosoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SubIncisoEndosoDTO entity to update
	 * @return SubIncisoEndosoDTO the persisted SubIncisoEndosoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubIncisoEndosoDTO update(SubIncisoEndosoDTO entity) {
		LogUtil.log("updating SubIncisoEndosoDTO instance", Level.INFO, null);
		try {
			SubIncisoEndosoDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SubIncisoEndosoDTO findById(SubIncisoEndosoId id) {
		LogUtil.log("finding SubIncisoEndosoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SubIncisoEndosoDTO instance = entityManager.find(
					SubIncisoEndosoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubIncisoEndosoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SubIncisoEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubIncisoEndosoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoEndosoDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding SubIncisoEndosoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SubIncisoEndosoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubIncisoEndosoDTO entities.
	 * 
	 * @return List<SubIncisoEndosoDTO> all SubIncisoEndosoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoEndosoDTO> findAll() {
		LogUtil.log("finding all SubIncisoEndosoDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from SubIncisoEndosoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void insertSubIncisoEndosoDePoliza(BigDecimal idToCotizacion ,BigDecimal idToPoliza, int numeroEndoso, double diasPorDevengar, short tipoEndoso){
		String queryString = "";	
		if (numeroEndoso == 0){
			queryString ="INSERT INTO MIDAS.toSubincisoEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, numeroSubinciso, valorSumaAsegurada, valorPrimaNeta) "+
			" SELECT subincisopol.idToPoliza, 0, subincisopol.numeroInciso, subincisopol.idToSeccion, subincisopol.numeroSubinciso, "+
			" subincisopol.valorSumaAsegurada, subincisopol.valorPrimaNeta *("+diasPorDevengar+" /365 ) "+
			" FROM MIDAS.toSubincisoPol subincisopol WHERE subincisopol.idToPoliza = "+idToPoliza;				
		}else if(tipoEndoso == 4 || tipoEndoso == 5 || tipoEndoso == 6){	//CANCELACION , REHABILITACION o CAMBIO FORMA DE PAGO		
			queryString ="INSERT INTO MIDAS.toSubincisoEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, numeroSubinciso, valorSumaAsegurada, valorPrimaNeta) "+
			" SELECT "+idToPoliza+", "+numeroEndoso+", subincisocot.numeroInciso, subincisocot.idToSeccion, subincisocot.numeroSubinciso, "+
			" subincisocot.valorSumaAsegurada, 0"+
			" from MIDAS.tosubincisocot subincisocot, MIDAS.toseccioncot seccioncot "+
			" where subincisocot.idtocotizacion = "+idToCotizacion+
			" and seccioncot.idtocotizacion = subincisocot.idtocotizacion "+
			" and seccioncot.numeroinciso = subincisocot.numeroinciso "+
			" and seccioncot.idtoseccion = subincisocot.idtoseccion "+
			" and seccioncot.clavecontrato =1 ";		
		}else{
			queryString ="INSERT INTO MIDAS.toSubincisoEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, numeroSubinciso, valorSumaAsegurada, valorPrimaNeta) "+
			" SELECT distinct "+idToPoliza+", "+numeroEndoso+", subincisocot.numeroInciso, subincisocot.idToSeccion, subincisocot.numeroSubinciso, "+
			" subincisocot.valorSumaAsegurada, 0 "+
			" FROM MIDAS.toSubincisoCot subincisocot , MIDAS.toMovimientoCotEnd movimientocotend "+
			" WHERE subincisocot.idToCotizacion = "+idToCotizacion+		
			" AND movimientocotend.idToCotizacion = subincisocot.idToCotizacion "+  
			" AND movimientocotend.numeroinciso = subincisocot.numeroinciso "+
			" AND movimientocotend.idtoseccion = subincisocot.idtoseccion "+
			" AND movimientocotend.numerosubinciso = subincisocot.numerosubinciso "+ 			
			" AND movimientocotend.clavetipomovimiento in (11,12,13)";			
		}
		
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();			
	}
	@SuppressWarnings("unchecked")
	public List<SubIncisoEndosoDTO> listarFiltrado(SubIncisoEndosoId id){
		try {
			String queryString = "select model from CoberturaEndosoDTO as model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (id == null)
				return null;		
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToPoliza", id.getIdToPoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.numeroEndoso", id.getNumeroEndoso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.numeroInciso", id.getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToSeccion", id.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.numeroSubInciso", id.getNumeroSubInciso());			
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"listarFiltrado CoberturaEndosoDTO failed",
					Level.SEVERE, re);
			throw re;
		}			
	}	
}