package mx.com.afirme.midas.endoso.cobertura;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity CoberturaEndosoDTO.
 * 
 * @see .CoberturaEndosoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CoberturaEndosoFacade implements CoberturaEndosoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved CoberturaEndosoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CoberturaEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CoberturaEndosoDTO entity) {
		LogUtil.log("saving CoberturaEndosoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent CoberturaEndosoDTO entity.
	 * 
	 * @param entity
	 *            CoberturaEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoberturaEndosoDTO entity) {
		LogUtil.log("deleting CoberturaEndosoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(CoberturaEndosoDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved CoberturaEndosoDTO entity and return it or a
	 * copy of it to the sender. A copy of the CoberturaEndosoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CoberturaEndosoDTO entity to update
	 * @return CoberturaEndosoDTO the persisted CoberturaEndosoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoberturaEndosoDTO update(CoberturaEndosoDTO entity) {
		LogUtil.log("updating CoberturaEndosoDTO instance", Level.INFO, null);
		try {
			CoberturaEndosoDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CoberturaEndosoDTO findById(CoberturaEndosoId id) {
		LogUtil.log("finding CoberturaEndosoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			CoberturaEndosoDTO instance = entityManager.find(
					CoberturaEndosoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoberturaEndosoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoberturaEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoberturaEndosoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaEndosoDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding CoberturaEndosoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from CoberturaEndosoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoberturaEndosoDTO entities.
	 * 
	 * @return List<CoberturaEndosoDTO> all CoberturaEndosoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaEndosoDTO> findAll() {
		LogUtil.log("finding all CoberturaEndosoDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from CoberturaEndosoDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void insertCoberturaEndosoDePoliza(BigDecimal idToCotizacion, BigDecimal idToPoliza, int numeroEndoso,short tipoEndoso, 
			BigDecimal idToCotizacionAnterior){
		String queryString = "";
		if (numeroEndoso == 0){
			queryString ="INSERT INTO MIDAS.toCoberturaEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, idToCobertura, idTcSubRamo, valorSumaAsegurada, valorCoaseguro,valorDeducible, "+
			" valorCuota, valorPrimaNeta, valorRecargoPagoFrac, valorDerechos, valorBonifComision, valorBonifComRecPagoFrac,  "+
			" valorIVA, valorPrimaTotal, porcentajeComision, valorComision, valorComisionFinal, valorComisionRecPagoFrac, valorComFinalRecPagoFrac, numeroAgrupacion) "+
			" SELECT coberturapol.idToPoliza, 0, coberturapol.numeroInciso, coberturapol.idToSeccion, coberturapol.idToCobertura, coberturapol.idTcSubRamo, "+
			" coberturapol.valorSumaAsegurada, coberturapol.valorCoaseguro, coberturapol.valorDeducible, coberturapol.valorCuota, coberturapol.valorPrimaNeta, "+
			" coberturapol.valorRecargoPagoFrac, coberturapol.valorDerechos, coberturapol.valorBonifComision, coberturapol.valorBonifComRecPagoFrac, coberturapol.valorIVA, coberturapol.valorPrimaTotal,"+
			" coberturapol.porcentajeComision, coberturapol.valorComision, coberturapol.valorComisionFinal, coberturapol.valorComisionRecPagoFrac, coberturapol.valorComFinalRecPagoFrac, "+
			" coberturapol.numeroAgrupacion FROM MIDAS.toCoberturaPol coberturapol "+
			" WHERE coberturapol.idToPoliza = "+idToPoliza;								
		}else if(tipoEndoso == 4 || tipoEndoso == 5 || tipoEndoso == 6){	//CANCELACION , REHABILITACION o CAMBIO FORMA DE PAGO
			queryString ="INSERT INTO MIDAS.toCoberturaEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, idToCobertura, idTcSubRamo, valorSumaAsegurada, valorCoaseguro,valorDeducible, "+
			" valorCuota, valorPrimaNeta, valorRecargoPagoFrac, valorDerechos, valorBonifComision, valorBonifComRecPagoFrac,  "+
			" valorIVA, valorPrimaTotal, porcentajeComision, valorComision, valorComisionFinal, valorComisionRecPagoFrac, valorComFinalRecPagoFrac, numeroAgrupacion) "+
			" SELECT "+idToPoliza+", "+numeroEndoso+", coberturacot.numeroInciso, coberturacot.idToSeccion, coberturacot.idToCobertura, coberturacot.idTcSubRamo, "+
			" coberturacot.valorSumaAsegurada, coberturacot.valorCoaseguro, coberturacot.valorDeducible, coberturacot.valorCuota, coberturacot.valorPrimaNeta, "+
			" 0, 0, 0, 0, 0, 0,"+
			" 0, 0, 0, 0, 0, coberturacot.numeroAgrupacion "+
		    " FROM MIDAS.tocoberturacot coberturacot, "+
	        " MIDAS.toseccioncot seccioncot "+
			" WHERE coberturacot.idtocotizacion = " +  idToCotizacion +
		    " and coberturacot.clavecontrato = 1 "+
		    " and seccioncot.idtocotizacion = coberturacot.idtocotizacion "+
		    " and seccioncot.numeroinciso = coberturacot.numeroinciso "+
		    " and seccioncot.idtoseccion = coberturacot.idtoseccion "+
		    " and seccioncot.clavecontrato = 1 ";			
		} else if (tipoEndoso == SistemaPersistencia.TIPO_ENDOSO_CE || tipoEndoso == SistemaPersistencia.TIPO_ENDOSO_RE) {
			queryString ="INSERT INTO MIDAS.toCoberturaEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, idToCobertura, idTcSubRamo, valorSumaAsegurada, valorCoaseguro,valorDeducible, "+
			" valorCuota, valorPrimaNeta, valorRecargoPagoFrac, valorDerechos, valorBonifComision, valorBonifComRecPagoFrac,  "+
			" valorIVA, valorPrimaTotal, porcentajeComision, valorComision, valorComisionFinal, valorComisionRecPagoFrac, valorComFinalRecPagoFrac, numeroAgrupacion) "+
			
			//Contratados
			" SELECT "+idToPoliza+", "+numeroEndoso+", coberturacot.numeroInciso, coberturacot.idToSeccion, coberturacot.idToCobertura, coberturacot.idTcSubRamo, "+
			" coberturacot.valorSumaAsegurada, coberturacot.valorCoaseguro, coberturacot.valorDeducible, coberturacot.valorCuota, coberturacot.valorPrimaNeta, "+
			" 0, 0, 0, 0, 0, 0,"+
			" 0, 0, 0, 0, 0, coberturacot.numeroAgrupacion "+
		    " FROM MIDAS.tomovimientocotend movimientocotend " +
		    "INNER JOIN MIDAS.tocoberturacot coberturacot on" +
		    "  coberturacot.idtocotizacion = movimientocotend.idtocotizacion" +
		    "  and coberturacot.numeroinciso = movimientocotend.numeroinciso" +
		    "  and coberturacot.idtoseccion = movimientocotend.idtoseccion" +
		    "  and coberturacot.idtocobertura = movimientocotend.idtocobertura " +
		    "INNER JOIN MIDAS.toseccioncot seccioncot on" +
		    "  seccioncot.idtocotizacion = coberturacot.idtocotizacion" +
		    "  and seccioncot.numeroinciso = coberturacot.numeroinciso" +
		    "  and seccioncot.idtoseccion = coberturacot.idtoseccion  "+
			" WHERE movimientocotend.idtocotizacion = " +idToCotizacion+  
		    " and coberturacot.clavecontrato = 1 "+
		    " and seccioncot.clavecontrato = 1 " +
			
			" UNION " +
			//Descontratados
			" SELECT "+idToPoliza+", "+numeroEndoso+", coberturacot.numeroInciso, coberturacot.idToSeccion, coberturacot.idToCobertura, coberturacot.idTcSubRamo, "+
			" coberturacot.valorSumaAsegurada, coberturacot.valorCoaseguro, coberturacot.valorDeducible, coberturacot.valorCuota, coberturacot.valorPrimaNeta * -1, "+
			" 0, 0, 0, 0, 0, 0,"+
			" 0, 0, 0, 0, 0, coberturacot.numeroAgrupacion "+
		    " FROM MIDAS.tomovimientocotend movimientocotend, "+
	        " MIDAS.tocoberturacot coberturacot, MIDAS.toseccioncot seccioncot, "+
	        " MIDAS.tocoberturacot coberturacotanterior,"+
            " MIDAS.toseccioncot seccioncotanterior "+
			
            " WHERE movimientocotend.idtocotizacion = " +idToCotizacion+
		    " and coberturacot.idtocotizacion = movimientocotend.idtocotizacion " +  

		    " and coberturacot.numeroinciso = movimientocotend.numeroinciso "+
		    " and coberturacot.idtoseccion = movimientocotend.idtoseccion "+
		    " and coberturacot.idtocobertura = movimientocotend.idtocobertura "+ 
		    
		    " and seccioncot.idtocotizacion = coberturacot.idtocotizacion "+
		    " and seccioncot.numeroinciso = coberturacot.numeroinciso "+
		    " and seccioncot.idtoseccion = coberturacot.idtoseccion "+
		    
		    " AND coberturacotanterior.idtocotizacion ="+ idToCotizacionAnterior  +
            
		    " AND coberturacot.numeroinciso = coberturacotanterior.numeroinciso"+
		    " AND coberturacot.idtoseccion = coberturacotanterior.idtoseccion"+
		    " AND coberturacot.idtocobertura = coberturacotanterior.idtocobertura"+ 
            
		    " AND seccioncotanterior.idtocotizacion = coberturacotanterior.idtocotizacion"+
		    " AND seccioncotanterior.numeroinciso = coberturacotanterior.numeroinciso"+
		    " AND seccioncotanterior.idtoseccion = coberturacotanterior.idtoseccion "+                               
                          
		    " AND (coberturacot.clavecontrato = 0 OR seccioncot.clavecontrato = 0)"+
            
		    " AND seccioncotanterior.clavecontrato = 1"+ 
		    " AND coberturacotanterior.clavecontrato = 1" +
		    
		    " UNION " +
		    //Eliminados de la BD  
			" SELECT "+idToPoliza+", "+numeroEndoso+", coberturacot.numeroInciso, coberturacot.idToSeccion, coberturacot.idToCobertura, coberturacot.idTcSubRamo, "+
			" coberturacot.valorSumaAsegurada, coberturacot.valorCoaseguro, coberturacot.valorDeducible, coberturacot.valorCuota, coberturacot.valorPrimaNeta * -1, "+
			" 0, 0, 0, 0, 0, 0,"+
			" 0, 0, 0, 0, 0, coberturacot.numeroAgrupacion " +
			" FROM MIDAS.tocoberturacot coberturacot " +
			" inner join MIDAS.toseccioncot seccioncot on seccioncot.idtocotizacion = coberturacot.idtocotizacion and seccioncot.numeroinciso = coberturacot.numeroinciso and seccioncot.idtoseccion = coberturacot.idtoseccion "+
			" WHERE coberturacot.idtocotizacion = " + idToCotizacionAnterior  +
			" and coberturacot.clavecontrato = 1 " +
			" and seccioncot.clavecontrato = 1 "+
			" and coberturacot.idtocobertura "+
			" 	not in ("+
			"        SELECT  idtocobertura"+
			"        FROM MIDAS.tocoberturacot "+
			"        WHERE idtocotizacion = " + idToCotizacion +
			"        and numeroInciso = coberturacot.numeroInciso"+
			"        and idToSeccion = coberturacot.idToSeccion"+
			"        and idToCobertura = coberturacot.idToCobertura"+
			"    )";
		    
		    
		    
		} else{
			queryString ="INSERT INTO MIDAS.toCoberturaEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, idToCobertura, idTcSubRamo, valorSumaAsegurada, valorCoaseguro,valorDeducible, "+
			" valorCuota, valorPrimaNeta, valorRecargoPagoFrac, valorDerechos, valorBonifComision, valorBonifComRecPagoFrac,  "+
			" valorIVA, valorPrimaTotal, porcentajeComision, valorComision, valorComisionFinal, valorComisionRecPagoFrac, valorComFinalRecPagoFrac, numeroAgrupacion) "+
			" SELECT "+idToPoliza+", "+numeroEndoso+", coberturacot.numeroInciso, coberturacot.idToSeccion, coberturacot.idToCobertura, coberturacot.idTcSubRamo, "+
			" coberturacot.valorSumaAsegurada, coberturacot.valorCoaseguro, coberturacot.valorDeducible, coberturacot.valorCuota, coberturacot.valorPrimaNeta, "+
			" 0, 0, 0, 0, 0, 0,"+
			" 0, 0, 0, 0, 0, coberturacot.numeroAgrupacion "+
		    " FROM MIDAS.tomovimientocotend movimientocotend, "+
	        " MIDAS.tocoberturacot coberturacot, MIDAS.toseccioncot seccioncot "+
			" WHERE movimientocotend.idtocotizacion = " +idToCotizacion+
		    " and coberturacot.idtocotizacion = movimientocotend.idtocotizacion " +  
		    " and coberturacot.numeroinciso = movimientocotend.numeroinciso "+
		    " and coberturacot.idtoseccion = movimientocotend.idtoseccion "+
		    " and coberturacot.idtocobertura = movimientocotend.idtocobertura "+  
		    " and coberturacot.clavecontrato = 1 "+
		    " and seccioncot.idtocotizacion = coberturacot.idtocotizacion "+
		    " and seccioncot.numeroinciso = coberturacot.numeroinciso "+
		    " and seccioncot.idtoseccion = coberturacot.idtoseccion "+
		    " and seccioncot.clavecontrato = 1 ";			
		}

		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();			
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaEndosoDTO> obtenerCoberturasPrimerRiesgoLUC(BigDecimal idToPoliza, Short numEndoso) {
	    if(idToPoliza==null || numEndoso==null){
		return null;
	    }
		LogUtil.log("finding all CoberturaEndosoDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from CoberturaEndosoDTO model where  " +
					"model.id.idToPoliza=:idPol and model.id.numeroEndoso=:numEnd and model.numeroAgrupacion >0";
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idPol",  idToPoliza);
			query.setParameter("numEnd", numEndoso);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}	
	@SuppressWarnings("unchecked")
	public List<CoberturaEndosoDTO> listarFiltrado(CoberturaEndosoId id){
		try {
			String queryString = "select model from CoberturaEndosoDTO as model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (id == null)
				return null;		
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToPoliza", id.getIdToPoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.numeroEndoso", id.getNumeroEndoso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.numeroInciso", id.getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToSeccion", id.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToCobertura", id.getIdToCobertura());
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"listarFiltrado CoberturaEndosoDTO failed",
					Level.SEVERE, re);
			throw re;
		}			
	}
	
	
	public int obtenerMaxNumeroCoberturaEnd(BigDecimal idToPoliza, BigDecimal numeroInciso) {
	    if(idToPoliza==null){
		return 0;
	    }
		LogUtil.log("finding all CoberturaEndosoDTO instances", Level.INFO,
				null);
		try {
			String queryString = "select max(model.id.numeroEndoso) from CoberturaEndosoDTO model where  " +
					"model.id.idToPoliza=:idPol";
			if(numeroInciso != null){
				queryString = queryString + " and model.id.numeroInciso=:numeroInciso ";
			}
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idPol",  idToPoliza);
			if(numeroInciso != null){
				query.setParameter("numeroInciso",  numeroInciso);
			}

			Object object = query.getSingleResult();
			int numeroEndoso = 0;
			if(object != null){
				if(object instanceof BigDecimal){
					BigDecimal valor = (BigDecimal) object;
					numeroEndoso = valor.intValue();
				}else if(object instanceof Double){
					Double valor = (Double) object;
					numeroEndoso = valor.intValue();
				}else if(object instanceof Integer){
					Integer valor = (Integer) object;
					numeroEndoso = valor.intValue();
				} else if(object instanceof Short){
					Short valor = (Short)object;
					numeroEndoso = valor.intValue();
				}
				
			}
			return numeroEndoso;
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
}