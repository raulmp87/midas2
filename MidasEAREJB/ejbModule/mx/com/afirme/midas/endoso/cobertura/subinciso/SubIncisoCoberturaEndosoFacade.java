package mx.com.afirme.midas.endoso.cobertura.subinciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity SubIncisoCoberturaEndosoDTO.
 * 
 * @see .SubIncisoCoberturaEndosoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SubIncisoCoberturaEndosoFacade implements
		SubIncisoCoberturaEndosoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * SubIncisoCoberturaEndosoDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            SubIncisoCoberturaEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubIncisoCoberturaEndosoDTO entity) {
		LogUtil.log("saving SubIncisoCoberturaEndosoDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SubIncisoCoberturaEndosoDTO entity.
	 * 
	 * @param entity
	 *            SubIncisoCoberturaEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubIncisoCoberturaEndosoDTO entity) {
		LogUtil.log("deleting SubIncisoCoberturaEndosoDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					SubIncisoCoberturaEndosoDTO.class, entity.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SubIncisoCoberturaEndosoDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * SubIncisoCoberturaEndosoDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            SubIncisoCoberturaEndosoDTO entity to update
	 * @return SubIncisoCoberturaEndosoDTO the persisted
	 *         SubIncisoCoberturaEndosoDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubIncisoCoberturaEndosoDTO update(SubIncisoCoberturaEndosoDTO entity) {
		LogUtil.log("updating SubIncisoCoberturaEndosoDTO instance",
				Level.INFO, null);
		try {
			SubIncisoCoberturaEndosoDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SubIncisoCoberturaEndosoDTO findById(SubIncisoCoberturaEndosoId id) {
		LogUtil.log("finding SubIncisoCoberturaEndosoDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			SubIncisoCoberturaEndosoDTO instance = entityManager.find(
					SubIncisoCoberturaEndosoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubIncisoCoberturaEndosoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SubIncisoCoberturaEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubIncisoCoberturaEndosoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoCoberturaEndosoDTO> findByProperty(
			String propertyName, final Object value) {
		LogUtil.log(
				"finding SubIncisoCoberturaEndosoDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SubIncisoCoberturaEndosoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubIncisoCoberturaEndosoDTO entities.
	 * 
	 * @return List<SubIncisoCoberturaEndosoDTO> all SubIncisoCoberturaEndosoDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoCoberturaEndosoDTO> findAll() {
		LogUtil.log("finding all SubIncisoCoberturaEndosoDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from SubIncisoCoberturaEndosoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void insertSubIncisoCoberturaEndosoDePoliza(BigDecimal idToCotizacion, BigDecimal idToPoliza, int numeroEndoso, double diasPorDevengar,short tipoEndoso){
		String queryString = "";
		if (numeroEndoso == 0){
			queryString ="INSERT INTO MIDAS.toSubincisoCobEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, numeroSubinciso, idToCobertura,  "+
			" idTcSubramo, valorSumaAsegurada, valorPrimaNeta)  "+
			" SELECT subincisocobpol.idToPoliza, 0, subincisocobpol.numeroInciso, subincisocobpol.idToSeccion, subincisocobpol.numeroSubinciso, "+
			" subincisocobpol.idToCobertura, "+
			" subincisocobpol.idTcSubramo, subincisocobpol.valorSumaAsegurada, subincisocobpol.valorPrimaNeta *("+ diasPorDevengar +" /365) "+
			" FROM MIDAS.toSubincisoCobPol subincisocobpol "+
			" WHERE subincisocobpol.idToPoliza = "+idToPoliza;				
		}else if(tipoEndoso == 4 || tipoEndoso == 5 || tipoEndoso == 6){	//CANCELACION , REHABILITACION o CAMBIO FORMA DE PAGO
			queryString ="INSERT INTO MIDAS.toSubincisoCobEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, numeroSubinciso, idToCobertura,  "+
			" idTcSubramo, valorSumaAsegurada, valorPrimaNeta)  "+
			" SELECT "+idToPoliza+", "+numeroEndoso+", detpricobcot.numeroInciso, detpricobcot.idToSeccion, detpricobcot.numeroSubinciso, "+
			" detpricobcot.idToCobertura, "+
			" coberturacot.idTcSubramo, coberturacot.valorSumaAsegurada, coberturacot.valorPrimaNeta "+
			" FROM MIDAS.toDetPrimaCoberturaCot detpricobcot, MIDAS.toCoberturaCot coberturacot , MIDAS.toseccioncot seccioncot"+
			" WHERE detpricobcot.idToCotizacion = "+idToCotizacion+
			" AND   coberturacot.idToCotizacion = detpricobcot.idToCotizacion "+
			" AND   coberturacot.numeroInciso = detpricobcot.numeroInciso "+
			" AND   coberturacot.idToSeccion = detpricobcot.idToSeccion "+ 
			" AND   coberturacot.idToCobertura = detpricobcot.idToCobertura "+
			" AND   coberturacot.clavecontrato = 1 "+
			" and seccioncot.idtocotizacion = coberturacot.idtocotizacion "+
			" and seccioncot.numeroinciso = coberturacot.numeroinciso "+
			" and seccioncot.idtoseccion = coberturacot.idtoseccion "+
			" and seccioncot.clavecontrato =1 ";			
		}else{
			queryString ="INSERT INTO MIDAS.toSubincisoCobEnd (idToPoliza, numeroEndoso, numeroInciso, idToSeccion, numeroSubinciso, idToCobertura,  "+
			" idTcSubramo, valorSumaAsegurada, valorPrimaNeta)  "+
			" SELECT "+idToPoliza+", "+numeroEndoso+", movimientocotend.numeroInciso, movimientocotend.idToSeccion, movimientocotend.numeroSubinciso, "+
			" movimientocotend.idToCobertura, "+
			" coberturacot.idTcSubramo,coberturacot.valorSumaAsegurada , detpricobcot.valorPrimaNeta *("+ diasPorDevengar +" /365) "+
			" FROM MIDAS.toMovimientoCotEnd movimientocotend " +
			"inner join MIDAS.toDetPrimaCoberturaCot detpricobcot on " +
			"  detpricobcot.idToCotizacion = movimientocotend.idToCotizacion  AND " +
			"  detpricobcot.numeroInciso = movimientocotend.numeroInciso  AND " +
			"  detpricobcot.idToSeccion = movimientocotend.idToSeccion  AND " +
			"  detpricobcot.numeroSubinciso = movimientocotend.numeroSubinciso  AND " +
			"  detpricobcot.idToCobertura = movimientocotend.idToCobertura " +
			"inner join MIDAS.toCoberturaCot coberturacot on " +
			"  coberturacot.idToCotizacion = detpricobcot.idToCotizacion  AND " +
			"  coberturacot.numeroInciso = detpricobcot.numeroInciso  AND " +
			"  coberturacot.idToSeccion = detpricobcot.idToSeccion  AND " +
			"  coberturacot.idToCobertura = detpricobcot.idToCobertura " +
			"WHERE" +
			"  movimientocotend.idToCotizacion ="+idToCotizacion+
			"and movimientocotend.numerosubinciso <> 0" +
			"GROUP BY "+idToPoliza+", "+numeroEndoso+", movimientocotend.numeroInciso," +
			"movimientocotend.idToSeccion, movimientocotend.numeroSubinciso," +
			"movimientocotend.idToCobertura,  coberturacot.idTcSubramo,coberturacot.valorSumaAsegurada ," +
			"detpricobcot.valorPrimaNeta *("+ diasPorDevengar +" /365) " ;
		}

		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();			
		
	}

	@SuppressWarnings("unchecked")
	public List<SubIncisoCoberturaEndosoDTO> listarFiltrado(SubIncisoCoberturaEndosoId id){
		try {
			String queryString = "select model from SubIncisoCoberturaEndosoDTO as model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (id == null)
				return null;		
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToPoliza", id.getIdToPoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.numeroEndoso", id.getNumeroEndoso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.numeroInciso", id.getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToSeccion", id.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToCobertura", id.getIdToCobertura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.numeroSubInciso", id.getNumeroSubInciso());			
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"listarFiltrado CoberturaEndosoDTO failed",
					Level.SEVERE, re);
			throw re;
		}			
	}

}