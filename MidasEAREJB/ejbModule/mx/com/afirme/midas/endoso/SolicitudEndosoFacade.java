package mx.com.afirme.midas.endoso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.solicitud.SolicitudDTO;

@Stateless
public class SolicitudEndosoFacade implements SolicitudEndosoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;
	/**
	 * Perform an initial save of a previously unsaved SolicitudDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SolicitudDTO entity to persist
	 * @return 
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public SolicitudDTO save(SolicitudDTO entity) {
		LogDeMidasEJB3.log("saving SolicitudDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SolicitudEndosoDTO entity.
	 * 
	 * @param entity
	 *            SolicitudEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SolicitudDTO entity) {
		LogDeMidasEJB3.log("deleting SolicitudDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SolicitudDTO.class, entity
					.getIdToSolicitud());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SolicitudEndosoDTO entity and return it or a copy of
	 * it to the sender. A copy of the SolicitudEndosoDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SolicitudEndosoDTO entity to update
	 * @return SolicitudEndosoDTO the persisted SolicitudEndosoDTO entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SolicitudDTO update(SolicitudDTO entity) {
		LogDeMidasEJB3.log("updating SolicitudDTO instance", Level.INFO, null);
		try {
			SolicitudDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SolicitudDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SolicitudDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SolicitudDTO instance = entityManager.find(SolicitudDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SolicitudEndosoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SolicitudEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SolicitudEndosoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SolicitudDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SolicitudDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SolicitudEndosoDTO entities.
	 * 
	 * @return List<SolicitudEndosoDTO> all SolicitudEndosoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> findAll() {
		LogDeMidasEJB3.log("finding all SolicitudDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from SolicitudDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> findByUserId(String id) {
		LogDeMidasEJB3.log("finding SolicitudDTO instances with user id: " + id, Level.INFO,
				null);
		try {
			String queryString = "select model from SolicitudDTO model where model.codigoUsuarioCreacion = :id";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("id", String.format("%1$-8s", id));
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> findByEstatus(Short claveEstatus, String userId) {
		LogDeMidasEJB3.log("finding SolicitudDTO instances with user id: " + userId + " and claveEstatus: " + claveEstatus, Level.INFO,
				null);
		try {
			String queryString = "select model from SolicitudDTO model where model.claveEstatus = :claveEstatus and model.codigoUsuarioCreacion = :id";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("claveEstatus", claveEstatus);
			query.setParameter("id", String.format("%1$-8s", userId));
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
}
