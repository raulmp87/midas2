package mx.com.afirme.midas.endoso;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.ejb.EJB;

import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVAFacadeRemote;
import mx.com.afirme.midas.consultas.gastoexpedicion.GastoExpedicionDTO;
import mx.com.afirme.midas.consultas.gastoexpedicion.GastoExpedicionFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionId;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.endoso.agrupacion.AgrupacionEndosoFacadeRemote;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoDTO;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoFacadeRemote;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoId;
import mx.com.afirme.midas.endoso.cobertura.subinciso.SubIncisoCoberturaEndosoFacadeRemote;
import mx.com.afirme.midas.endoso.riesgo.RiesgoEndosoDTO;
import mx.com.afirme.midas.endoso.riesgo.RiesgoEndosoFacadeRemote;
import mx.com.afirme.midas.endoso.riesgo.RiesgoEndosoId;
import mx.com.afirme.midas.endoso.riesgo.subinciso.SubIncisoRiesgoEndosoFacadeRemote;
import mx.com.afirme.midas.endoso.subinciso.SubIncisoEndosoFacadeRemote;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoFacadeRemote;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudFacadeRemote;

public class EndosoSoporteEmision {

	@EJB
	protected MovimientoCotizacionEndosoFacadeRemote movimientoCotizacion;
	@EJB
	protected CoberturaCotizacionFacadeRemote coberturaCotizacion;
	@EJB
	protected ParametroGeneralFacadeRemote parametroGeneral;
	@EJB
	protected GastoExpedicionFacadeRemote gastoExpedicion;
	@EJB
	protected CoberturaEndosoFacadeRemote coberturaEndoso;
	@EJB
	protected RiesgoEndosoFacadeRemote riesgoEndoso;
	@EJB
	protected SubIncisoEndosoFacadeRemote subIncisoEndoso;
	@EJB
	protected SubIncisoCoberturaEndosoFacadeRemote subIncisoCoberturaEndoso;
	@EJB
	protected SubIncisoRiesgoEndosoFacadeRemote subIncisoRiesgoEndoso;
	@EJB
	protected AgrupacionEndosoFacadeRemote agrupacionEndoso;	
	@EJB
	protected AgrupacionCotFacadeRemote agrupacionCot;
	@EJB
	protected RiesgoCotizacionFacadeRemote riesgoCotizacion;	
	@EJB
	protected PolizaFacadeRemote poliza;
	@EJB
	protected CotizacionFacadeRemote cotizacion;
	@EJB
	protected ComisionCotizacionFacadeRemote comisionCotizacion;	
	@EJB
	protected SolicitudFacadeRemote solicitud;
	@EJB
	protected EndosoFacadeRemote endoso;	
	@EJB
	protected CodigoPostalIVAFacadeRemote codigoPostalIVAFacadeRemote;
	@EJB
	protected ControlArchivoFacadeRemote controlArchivoFacadeRemote;	
	@EJB
	protected DocumentoDigitalCotizacionFacadeRemote documentoDigitalCotizacionFacadeRemote;
	@EJB
	protected DocAnexoCotFacadeRemote docAnexoCotFacadeRemote;
	@EJB
	protected DocumentoAnexoReaseguroCotizacionFacadeRemote documentoAnexoReaseguroCotizacionFacadeRemote;
	@EJB
	protected TexAdicionalCotFacadeRemote  texAdicionalCotFacadeRemote;
	// Constantes de tipo de solicitud de endoso;
	protected final short SOLICITUD_TIPO_ENDOSO_CANCELACION = 1;
	protected final short SOLICITUD_TIPO_ENDOSO_REHABILITACION = 2;
	protected final short SOLICITUD_TIPO_ENDOSO_MODIFICACION = 3;
	protected final short SOLICITUD_TIPO_ENDOSO_DECLARACION = 4;
	protected final short SOLICITUD_TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO = 5;
	
	// Constantes de tipo de endoso;
	protected final short TIPO_ENDOSO_CANCELACION = 4;
	protected final short TIPO_ENDOSO_REHABILITACION = 5;
	protected final short TIPO_ENDOSO_AUMENTO = 2;
	protected final short TIPO_ENDOSO_DISMINUCION = 3;
	protected final short TIPO_ENDOSO_CAMBIO_DATOS = 1;
	protected final short TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO = 6;
	
	//Constantes para el tipo de Calculo
	protected final String TIPO_CALCULO_SEYCOS = "S";
	protected final String TIPO_CALCULO_MIDAS = "M";
	
	// Constantes para tipos de comision
	private static final short TIPO_RO = 1;
	private static final short TIPO_RCI = 2;
	private static final short TIPO_PRR = 3;
	
	// Constantes para tipos de agrupoacion
	private static final short TIPO_PRIMER_RIESGO = 1;
	
	// Constantes para estatus de recibos
	private static final String RECIBO_PAGADO = "PAGADO";			
	
	private double valorPrimaNetaEndoso;
	private double valorRecargoPagoFraccionadoEndoso;
	private double valorDerechosEndoso;
	private double valorBonificacionComisionEndoso;
	private double valorIVAEndoso;
	private double valorPrimaTotalEndoso;
	private double valorBonificacionComisionRPFEndoso;
	private double valorComisionEndoso;
	private double valorComisionRPFEndoso;
	
	protected EndosoDTO emitir(EndosoDTO endosoEmitido, Double ivaCotizacion,
			double diasPorDevengar, boolean existeReciboPagado, List<ReciboDTO> recibos,
			EndosoDTO ultimoEndoso, EndosoIDTO endosoIDTO, Date fechaInicioVigenciaPoliza) {
		
		EndosoDTO endosoACancelarRehabilitar = null;
		if((endosoEmitido.getClaveTipoEndoso().shortValue() == TIPO_ENDOSO_CANCELACION ||
				endosoEmitido.getClaveTipoEndoso().shortValue() == TIPO_ENDOSO_REHABILITACION)){
			
			endosoACancelarRehabilitar = endoso.findById(
					new EndosoId(endosoEmitido.getId().getIdToPoliza(), (short)0));
			
			endosoACancelarRehabilitar.setCotizacionDTO(
					cotizacion.findById(
							endosoACancelarRehabilitar.getIdToCotizacion()));
		}
		return emitir(endosoEmitido, ivaCotizacion, diasPorDevengar, existeReciboPagado, recibos, 
				      ultimoEndoso, endosoIDTO, fechaInicioVigenciaPoliza, endosoACancelarRehabilitar);
	}
	
	protected EndosoDTO emitir(EndosoDTO endosoEmitido, Double ivaCotizacion,
			double diasPorDevengar, boolean existeReciboPagado, List<ReciboDTO> recibos,
			EndosoDTO ultimoEndoso, EndosoIDTO endosoIDTO, Date fechaInicioVigenciaPoliza, EndosoDTO endosoACancelarRehabilitar) {
		
		agrupacionEndoso.insertAgrupacionEndosoDePoliza(endosoEmitido
				.getIdToCotizacion(), endosoEmitido.getId().getIdToPoliza(), endosoEmitido
				.getId().getNumeroEndoso().intValue(), diasPorDevengar,endosoEmitido.getClaveTipoEndoso());
		subIncisoEndoso.insertSubIncisoEndosoDePoliza(endosoEmitido
				.getIdToCotizacion(), endosoEmitido.getId().getIdToPoliza(), endosoEmitido
				.getId().getNumeroEndoso().intValue(), diasPorDevengar,endosoEmitido.getClaveTipoEndoso());
		coberturaEndoso.insertCoberturaEndosoDePoliza(endosoEmitido
				.getIdToCotizacion(), endosoEmitido.getId().getIdToPoliza(), endosoEmitido
				.getId().getNumeroEndoso().intValue(),endosoEmitido.getClaveTipoEndoso(), ultimoEndoso.getIdToCotizacion());
		subIncisoCoberturaEndoso.insertSubIncisoCoberturaEndosoDePoliza(endosoEmitido
				.getIdToCotizacion(), endosoEmitido.getId().getIdToPoliza(), endosoEmitido
				.getId().getNumeroEndoso().intValue(), diasPorDevengar,endosoEmitido.getClaveTipoEndoso());
		riesgoEndoso.insertRiesgoEndosoDePoliza(endosoEmitido
				.getIdToCotizacion(), endosoEmitido.getId().getIdToPoliza(), endosoEmitido
				.getId().getNumeroEndoso().intValue(),endosoEmitido.getClaveTipoEndoso(), ultimoEndoso.getIdToCotizacion());
		subIncisoRiesgoEndoso.insertSubIncisoRiesgoEndosoDeCotizacion(endosoEmitido
				.getIdToCotizacion(), endosoEmitido.getId().getIdToPoliza(), endosoEmitido
				.getId().getNumeroEndoso().intValue(), diasPorDevengar,endosoEmitido.getClaveTipoEndoso());	
		calcularRiesgos(endosoEmitido.getCotizacionDTO(), endosoEmitido,
				ivaCotizacion, diasPorDevengar, existeReciboPagado, recibos,
				ultimoEndoso, fechaInicioVigenciaPoliza, endosoACancelarRehabilitar,endosoIDTO);
		acumulaCoberturas(endosoEmitido);
		acumulaEndoso(endosoEmitido);
		
		return endosoEmitido;
	}

	protected void aplicaFactorAjusteRiesgos(double factorAjustePrima,double factorAjusteRPF, EndosoDTO endosoDTO,double ivaCotizacion){
		valorPrimaNetaEndoso= 0D;
		valorRecargoPagoFraccionadoEndoso= 0D;
		valorDerechosEndoso = 0D;
		valorBonificacionComisionEndoso = 0D;
		valorIVAEndoso = 0D;
		valorPrimaTotalEndoso= 0D;
		valorBonificacionComisionRPFEndoso = 0D;
		valorComisionEndoso = 0D;
		valorComisionRPFEndoso = 0D;	
		RiesgoEndosoId id0 = new RiesgoEndosoId();
		id0.setIdToPoliza(endosoDTO.getId().getIdToPoliza());
		id0.setNumeroEndoso(endosoDTO.getId().getNumeroEndoso());

		List<RiesgoEndosoDTO> riesgoEndosoDTOs = riesgoEndoso.listarFiltrado(id0);
		
		for(RiesgoEndosoDTO riesgoEndosoDTO: riesgoEndosoDTOs){
			riesgoEndosoDTO.setValorPrimaNeta(riesgoEndosoDTO.getValorPrimaNeta() * factorAjustePrima);
			riesgoEndosoDTO.setValorComision(riesgoEndosoDTO.getValorComision() * factorAjustePrima);
			riesgoEndosoDTO.setValorRecargoPagoFrac(riesgoEndosoDTO.getValorRecargoPagoFrac() * factorAjusteRPF);		
			riesgoEndosoDTO.setValorComisionRecPagoFrac(riesgoEndosoDTO.getValorComisionRecPagoFrac() * factorAjusteRPF);
			riesgoEndosoDTO.setValorCoaseguro(riesgoEndosoDTO.getValorCoaseguro() * factorAjustePrima);
			riesgoEndosoDTO.setValorDeducible(riesgoEndosoDTO.getValorDeducible() * factorAjustePrima);
			riesgoEndosoDTO.setValorBonifComision(riesgoEndosoDTO.getValorBonifComision() * factorAjustePrima);
			riesgoEndosoDTO.setValorBonifComRecPagoFrac(riesgoEndosoDTO.getValorBonifComRecPagoFrac() * factorAjusteRPF);
			riesgoEndosoDTO.setValorComisionFinal(riesgoEndosoDTO.getValorComision()  - riesgoEndosoDTO.getValorBonifComision());
			riesgoEndosoDTO.setValorComFinalRecPago( riesgoEndosoDTO.getValorComisionRecPagoFrac() -riesgoEndosoDTO.getValorBonifComRecPagoFrac());			
			Double iva = (riesgoEndosoDTO.getValorPrimaNeta() + riesgoEndosoDTO.getValorRecargoPagoFrac() +riesgoEndosoDTO.getValorDerechos() 
			- riesgoEndosoDTO.getValorBonifComision() -riesgoEndosoDTO.getValorBonifComRecPagoFrac())* ivaCotizacion/100D; 			
			riesgoEndosoDTO.setValorIVA(iva);
			Double primaTotal = riesgoEndosoDTO.getValorPrimaNeta() + riesgoEndosoDTO.getValorRecargoPagoFrac() +riesgoEndosoDTO.getValorDerechos()
			- riesgoEndosoDTO.getValorBonifComision() -riesgoEndosoDTO.getValorBonifComRecPagoFrac();			
			riesgoEndosoDTO.setValorPrimaTotal(primaTotal+ riesgoEndosoDTO.getValorIVA());

			//Acumula a Nivel Endoso
			valorPrimaNetaEndoso += riesgoEndosoDTO.getValorPrimaNeta();
			valorRecargoPagoFraccionadoEndoso += riesgoEndosoDTO.getValorRecargoPagoFrac();
			valorDerechosEndoso += riesgoEndosoDTO.getValorDerechos();
			valorBonificacionComisionEndoso += riesgoEndosoDTO.getValorBonifComision();
			valorIVAEndoso += riesgoEndosoDTO.getValorIVA();
			valorPrimaTotalEndoso += riesgoEndosoDTO.getValorPrimaTotal();
			valorBonificacionComisionRPFEndoso += riesgoEndosoDTO.getValorBonifComRecPagoFrac();
			valorComisionEndoso += riesgoEndosoDTO.getValorComision();
			valorComisionRPFEndoso += riesgoEndosoDTO.getValorComisionRecPagoFrac();

			riesgoEndoso.update(riesgoEndosoDTO);
		}
	}
		
	protected void calcularRiesgos(CotizacionDTO cotizacionDTO,
			EndosoDTO endosoEmitido, Double ivaCotizacion,
			double diasPorDevengar, boolean existeReciboPagado, List<ReciboDTO> recibos,
			EndosoDTO ultimoEndoso, Date fechaInicioVigenciaPoliza, EndosoDTO endosoACancelarRehabilitar, EndosoIDTO endosoIDTO) {
		boolean negativo = false;
		valorPrimaNetaEndoso= 0D;
		valorRecargoPagoFraccionadoEndoso= 0D;
		valorDerechosEndoso = 0D;
		valorBonificacionComisionEndoso = 0D;
		valorIVAEndoso = 0D;
		valorPrimaTotalEndoso= 0D;
		valorBonificacionComisionRPFEndoso = 0D;
		valorComisionEndoso = 0D;
		valorComisionRPFEndoso = 0D;		
		
		try {
		
		if(cotizacionDTO.getTipoPolizaDTO().getProductoDTO() != null && 
				cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getClaveAjusteVigencia().intValue() == 0){
			diasPorDevengar = 365d;
		}			
		
		Double primaNetaCotizacion = cotizacion.getPrimaNetaCotizacion(cotizacionDTO.getIdToCotizacion());	
		Double primaNetaCotizacionBase = cotizacion.getPrimaNetaCotizacion(ultimoEndoso.getIdToCotizacion());
		primaNetaCotizacion = (primaNetaCotizacion - primaNetaCotizacionBase) * (diasPorDevengar / 365D );
		
		if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()==SOLICITUD_TIPO_ENDOSO_CANCELACION){
			primaNetaCotizacion = primaNetaCotizacionBase * (diasPorDevengar / 365D ) * -1D;
		}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()==SOLICITUD_TIPO_ENDOSO_REHABILITACION){
			primaNetaCotizacion = primaNetaCotizacionBase * (diasPorDevengar / 365D );
		}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()== SOLICITUD_TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO){
			primaNetaCotizacion = cotizacion.getPrimaNetaCotizacion(cotizacionDTO.getIdToCotizacion()) * (diasPorDevengar / 365D );	
		} else if ((endosoEmitido.getClaveTipoEndoso().shortValue() == SistemaPersistencia.TIPO_ENDOSO_CE
				|| endosoEmitido.getClaveTipoEndoso().shortValue() == SistemaPersistencia.TIPO_ENDOSO_RE)
				&& primaNetaCotizacion < 0) {
			negativo = true;
			primaNetaCotizacion = primaNetaCotizacion * -1D;
		}
		
		//Se obtienen los gastos de expedicion y recargo por pago fraccionado
		Double derechos = 0D;
		GastoExpedicionDTO gasto = null;	
		gasto = gastoExpedicion.getGastoExpedicion(cotizacionDTO.getIdMoneda(), BigDecimal.valueOf(primaNetaCotizacion));
		
		if (negativo) {
			primaNetaCotizacion = primaNetaCotizacion * -1D;
		}
		
		if (primaNetaCotizacion > 0
				&& (endosoEmitido.getClaveTipoEndoso() == TIPO_ENDOSO_AUMENTO)) {	
			derechos = gasto.getGastoExpedicionEndosoAumento();				
		} else if (endosoEmitido.getClaveTipoEndoso() == TIPO_ENDOSO_REHABILITACION) {
			//Se cobran justamente los derechos que se regresaron en el endoso de cancelación previo	
			derechos = ultimoEndoso.getValorDerechos() * -1D; 

			//Se cobran justamente el RPF que se regresó en el endoso de cancelación previo (prorrateado, aunque siempre deberían coincidir las fechas de vigencia)
			double diasEndosoRehabilitacion = Utilerias.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
			double diasEndosoCancelacion = Utilerias.obtenerDiasEntreFechas(ultimoEndoso.getFechaInicioVigencia(), ultimoEndoso.getFechaFinVigencia());			
			double factor = diasEndosoRehabilitacion/diasEndosoCancelacion;	//En teoría este factor siempre debe ser igual a 1.		
			cotizacionDTO.setMontoRecargoPagoFraccionado(ultimoEndoso.getValorRecargoPagoFrac() * factor * -1D);
			cotizacionDTO.setMontoComisionPagoFraccionado(ultimoEndoso.getValorComisionRPF() * factor * -1D);
			cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(ultimoEndoso.getValorBonifComisionRPF() * factor * -1D);						
		} else if (endosoEmitido.getClaveTipoEndoso().shortValue() == SistemaPersistencia.TIPO_ENDOSO_CE) {						
			//Se obtienen los derechos a cancelar
			//Los derechos se regresan siempre y cuando se cumpla cualquiera de las siguientes condiciones: 
			//		1) La fecha de inicio de vigencia del endoso de cancelación sea igual o menor a la fecha de inicio de vigencia del endoso a cancelar
			//		2) No se haya pagado el primer recibo del endoso a cancelar
			boolean primerReciboPagado = primerReciboPagado(recibos, endosoACancelarRehabilitar);
			boolean regresarDerechos = regresarDerechosEndoso(endosoACancelarRehabilitar.getFechaInicioVigencia(), endosoEmitido.getFechaInicioVigencia(), primerReciboPagado);
			if(regresarDerechos)
				derechos += endosoACancelarRehabilitar.getValorDerechos() * -1D;	
			
			double diasEndosoCancelacion = Utilerias.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());																					

			//Se calculan el RPF y la bonificación de comisión por RPF a cancelar
			//Se acumulan los montos de todos los endosos de la póliza
			double diasEndoso = Utilerias.obtenerDiasEntreFechas(endosoACancelarRehabilitar.getFechaInicioVigencia(), endosoACancelarRehabilitar.getFechaFinVigencia());			
			double factor = diasEndosoCancelacion/diasEndoso;			
			factor = factor > 1 ? 1 : factor; //es necesario igualar el factor a 1 para no regresar más RPF de los emitidos
									
			double montoRPF = endosoACancelarRehabilitar.getValorRecargoPagoFrac() * factor;
			double montoComisionRFP = endosoACancelarRehabilitar.getValorComisionRPF() * factor;
			double montoBonificacionComisionRFP = endosoACancelarRehabilitar.getValorBonifComisionRPF() * factor;

			cotizacionDTO.setMontoRecargoPagoFraccionado(montoRPF * -1D);
			cotizacionDTO.setMontoComisionPagoFraccionado(montoComisionRFP * -1D);
			cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(montoBonificacionComisionRFP * -1D);							
		} else if (endosoEmitido.getClaveTipoEndoso().shortValue() == SistemaPersistencia.TIPO_ENDOSO_RE) {										
			//Se cobran justamente los derechos que se regresaron en el endoso de cancelación previo
			EndosoId id = new EndosoId(endosoACancelarRehabilitar.getId().getIdToPoliza(), endosoACancelarRehabilitar.getCancela());
			EndosoDTO endosoCancelacion = endoso.findById(id);
			derechos = endosoCancelacion.getValorDerechos() * -1D; 

			//Se cobran justamente el RPF que se regresó en el endoso de cancelación previo (prorrateado)
			double diasEndosoRehabilitacion = Utilerias.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
			double diasEndosoCancelacion = Utilerias.obtenerDiasEntreFechas(endosoCancelacion.getFechaInicioVigencia(), endosoCancelacion.getFechaFinVigencia());			
			double factor = diasEndosoRehabilitacion/diasEndosoCancelacion;		
			cotizacionDTO.setMontoRecargoPagoFraccionado(endosoCancelacion.getValorRecargoPagoFrac() * factor * -1D);
			cotizacionDTO.setMontoComisionPagoFraccionado(endosoCancelacion.getValorComisionRPF() * factor * -1D);
			cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(endosoCancelacion.getValorBonifComisionRPF() * factor * -1D);			
		} else if (endosoEmitido.getClaveTipoEndoso() == TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO) {			
			BigDecimal idToPoliza = cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada();
			List<EndosoDTO> endosos = endoso.findByProperty("id.idToPoliza", idToPoliza, false);
																														
			//Se elimina de la lista el endoso que se está emitiendo en este momento.
			endosos.remove(0);
			for(EndosoDTO endoso : endosos){							
				//Se obtienen los derechos a cobrar (Todos los derechos no pagados en cualquier endoso)									
				boolean primerReciboPagado = primerReciboPagado(recibos, endoso);								
				if(!primerReciboPagado)
					derechos += endoso.getValorDerechos();
																
				//Se consideran los endosos de la póliza a partir del último endoso de CFP
				if(endoso.getClaveTipoEndoso().compareTo(TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO) == 0)
					break;
			}			
		} else if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == SOLICITUD_TIPO_ENDOSO_CANCELACION) {									
				BigDecimal idToPoliza = cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada();
//				List<EndosoDTO> endosos = endoso.findByProperty("id.idToPoliza", idToPoliza, false);
				
				EndosoDTO endosoCero = endoso.findById(new EndosoId(idToPoliza, endoso.buscarNumeroEndosoUltimoCFP(idToPoliza)));
				double diasEndosoCancelacion = Utilerias.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());																		
				double montoRPF = 0;
				double montoComisionRFP = 0;
				double montoBonificacionComisionRFP = 0;
				boolean primerReciboPagado = primerReciboPagado(recibos, endosoCero);					
				boolean regresarDerechos = regresarDerechosEndoso(endosoCero.getFechaInicioVigencia(), cotizacionDTO.getFechaInicioVigencia(), primerReciboPagado);
				
				if(!existeReciboPagado && regresarDerechos && endosoCero.getId().getNumeroEndoso() > 0){
					derechos = endosoIDTO.getDerechos() * -1D;
					cotizacionDTO.setMontoRecargoPagoFraccionado(endosoIDTO.getRecargoPF() * -1D);
					cotizacionDTO.setMontoComisionPagoFraccionado(endosoIDTO.getComisionRPF() * -1D);
					cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(endosoIDTO.getBonificacionComisionRPF() * -1D);
				}else{
					//Se obtienen los derechos a cancelar
					//Los derechos se regresan siempre y cuando se cumpla cualquiera de las siguientes condiciones: 
					//		1) La fecha de inicio de vigencia del endoso de cancelación sea igual o menor a la fecha de inicio de vigencia del endoso a cancelar
					//		2) No se haya pagado el primer recibo del endoso a cancelar				
					if(regresarDerechos)
						derechos = endosoCero.getValorDerechos() * -1D;
					
					
					//Se calculan el RPF y la bonificación de comisión por RPF a cancelar
					//Se acumulan los montos de todos los endosos de la póliza
					double diasEndoso = Utilerias.obtenerDiasEntreFechas(endosoCero.getFechaInicioVigencia(), endosoCero.getFechaFinVigencia());			
					double factor = diasEndosoCancelacion/diasEndoso;			
					factor = factor > 1 ? 1 : factor; //es necesario igualar el factor a 1 para no regresar más RPF de los emitidos
										
					montoRPF = endosoCero.getValorRecargoPagoFrac() * factor;
					montoComisionRFP = endosoCero.getValorComisionRPF() * factor;
					montoBonificacionComisionRFP = endosoCero.getValorBonifComisionRPF() * factor;
					
					cotizacionDTO.setMontoRecargoPagoFraccionado(montoRPF * -1D);
					cotizacionDTO.setMontoComisionPagoFraccionado(montoComisionRFP * -1D);
					cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(montoBonificacionComisionRFP * -1D);																			
					
				}
		}

		if(ivaCotizacion==null){
		    throw new RuntimeException("No se pudo encontrar el valor para el IVA");
		}

		Double primaNetaRiesgoBase = 0D;

		RiesgoEndosoId id0 = new RiesgoEndosoId();
		id0.setIdToPoliza(ultimoEndoso.getId().getIdToPoliza());
		id0.setNumeroEndoso(endosoEmitido.getId().getNumeroEndoso());

		List<RiesgoEndosoDTO> riesgoEndosoDTOs = riesgoEndoso.listarFiltrado(id0);
		
		boolean riesgoEliminado = false;		
		for(RiesgoEndosoDTO riesgoEndosoDTO: riesgoEndosoDTOs){
			
			RiesgoCotizacionDTO riesgoBase = new RiesgoCotizacionDTO();
			RiesgoCotizacionId id1 = new RiesgoCotizacionId();
			id1.setIdToCotizacion(ultimoEndoso.getIdToCotizacion());
			id1.setNumeroInciso(riesgoEndosoDTO.getId().getNumeroInciso());
			id1.setIdToSeccion(riesgoEndosoDTO.getId().getIdToSeccion());
			id1.setIdToCobertura(riesgoEndosoDTO.getId().getIdToCobertura());
			id1.setIdToRiesgo(riesgoEndosoDTO.getId().getIdToRiesgo());
			
			riesgoBase.setId(id1);
			
			riesgoBase = riesgoCotizacion.findById(id1);
			id1.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
			
			RiesgoCotizacionDTO riesgo = new RiesgoCotizacionDTO();
			riesgo = riesgoCotizacion.findById(id1);
			if (riesgo == null){
				riesgo = riesgoBase;
				riesgoEliminado = true;
			}else{
				riesgoEliminado = false;
			}
							
			if (riesgoBase != null)
				primaNetaRiesgoBase = riesgoBase.getValorPrimaNeta();
			
			ComisionCotizacionDTO comision = obtenerComision(riesgo, cotizacionDTO, endosoEmitido, endosoACancelarRehabilitar);
			
			actualizarPrimaNetaComisionRiesgo(riesgoEndosoDTO, riesgo, riesgoBase, 
					primaNetaRiesgoBase, comision, endosoEmitido, diasPorDevengar, riesgoEliminado);

			actualizarRecargoPagoFraccionado(riesgoEndosoDTO, cotizacionDTO, endosoEmitido, 
					endosoACancelarRehabilitar, primaNetaCotizacion);
			
			//valorCoaseguro = valorPrimaNeta * porcentajeCoaseguro / 100
			riesgoEndosoDTO.setValorCoaseguro(riesgoEndosoDTO.getValorPrimaNeta() * riesgo.getPorcentajeCoaseguro() / 100);
			//valorDeducible = valorPrimaNeta * porcentajeDeducible / 100
			riesgoEndosoDTO.setValorDeducible(riesgoEndosoDTO.getValorPrimaNeta() * riesgo.getPorcentajeDeducible() / 100);
			//valorCuota = valorPrimaNeta / valorSumaAsegurada
			if(riesgo.getValorSumaAsegurada() == 0){
				riesgoEndosoDTO.setValorCuota(0D);
			}else{
				riesgoEndosoDTO.setValorCuota(riesgoEndosoDTO.getValorPrimaNeta()/riesgo.getValorSumaAsegurada());
			}
			//derechos = derechos poliza * valorPrimaNetaCobertura / primaNetaCotizacion 
			if(derechos == 0D){
				riesgoEndosoDTO.setValorDerechos(derechos);
			}else{
				riesgoEndosoDTO.setValorDerechos(derechos * (riesgoEndosoDTO.getValorPrimaNeta()/ primaNetaCotizacion ));
	
			}
			//valorBonifComision =  getValorComision * porcentajeBonifComision/100
			riesgoEndosoDTO.setValorBonifComision(riesgoEndosoDTO.getValorComision() * cotizacionDTO.getPorcentajebonifcomision()/100);
			//valorComisionFinal = valorComision - valorBonifComision
			riesgoEndosoDTO.setValorComisionFinal(riesgoEndosoDTO.getValorComision() - riesgoEndosoDTO.getValorBonifComision());			
			//valorIVA = (primaNeta del riesgo + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac) * porcentaje de IVA
			Double iva = (riesgoEndosoDTO.getValorPrimaNeta() + riesgoEndosoDTO.getValorRecargoPagoFrac() +riesgoEndosoDTO.getValorDerechos() 
						- riesgoEndosoDTO.getValorBonifComision() -riesgoEndosoDTO.getValorBonifComRecPagoFrac())* ivaCotizacion/100D; 
			riesgoEndosoDTO.setValorIVA(iva);
			//valorPrimaTotal = primaNeta del riesgo + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac + IVA
			Double primaTotal = riesgoEndosoDTO.getValorPrimaNeta() + riesgoEndosoDTO.getValorRecargoPagoFrac() +riesgoEndosoDTO.getValorDerechos()
								- riesgoEndosoDTO.getValorBonifComision() -riesgoEndosoDTO.getValorBonifComRecPagoFrac();
			riesgoEndosoDTO.setValorPrimaTotal(primaTotal + riesgoEndosoDTO.getValorIVA());			

			//se actualiza el valor de la PN
			riesgoEndosoDTO.setValorPrimaNeta(riesgoEndosoDTO.getValorPrimaNeta() - riesgoEndosoDTO.getValorBonifComision());
			
			//Acumula a Nivel Endoso
			valorPrimaNetaEndoso += riesgoEndosoDTO.getValorPrimaNeta();
			valorRecargoPagoFraccionadoEndoso += riesgoEndosoDTO.getValorRecargoPagoFrac();
			valorDerechosEndoso += riesgoEndosoDTO.getValorDerechos();
			valorBonificacionComisionEndoso += riesgoEndosoDTO.getValorBonifComision();
			valorIVAEndoso += riesgoEndosoDTO.getValorIVA();
			valorPrimaTotalEndoso += riesgoEndosoDTO.getValorPrimaTotal();
			valorBonificacionComisionRPFEndoso += riesgoEndosoDTO.getValorBonifComRecPagoFrac();
			valorComisionEndoso += riesgoEndosoDTO.getValorComision();
			valorComisionRPFEndoso += riesgoEndosoDTO.getValorComisionRecPagoFrac();
			
			riesgoEndoso.update(riesgoEndosoDTO);
		}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	private void actualizarRecargoPagoFraccionado(RiesgoEndosoDTO riesgoEndosoDTO,CotizacionDTO cotizacionDTO,
			EndosoDTO endosoEmitido,EndosoDTO endosoACancelarRehabilitar,double primaNetaCotizacion){
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == SOLICITUD_TIPO_ENDOSO_CANCELACION ||
				cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == SOLICITUD_TIPO_ENDOSO_REHABILITACION ||
				endosoEmitido.getClaveTipoEndoso().shortValue() == SistemaPersistencia.TIPO_ENDOSO_CE ||
				endosoEmitido.getClaveTipoEndoso().shortValue() == SistemaPersistencia.TIPO_ENDOSO_RE){
			//TODO se deben recuperar los datos del endoso que se está cancelando.
			//Para cancelacion de póliza, se puede obtener de la variable "ultimoEndoso"
			riesgoEndosoDTO.setValorRecargoPagoFrac(cotizacionDTO.getMontoRecargoPagoFraccionado()*(riesgoEndosoDTO.getValorPrimaNeta()/primaNetaCotizacion));
			//valorComisionRecPagoFrac = valorRecargoPagoFrac * porcentajeComision/100
//				riesgoEndosoDTO.setValorComisionRecPagoFrac(cotizacionDTO.getMontoComisionPagoFraccionado()*(riesgoEndosoDTO.getValorPrimaNeta()/primaNetaCotizacion));
			/*
			 * 05/08/2011. se calcula multiplicando el RPF por el porcentaje de comisión
			 */
			riesgoEndosoDTO.setValorComisionRecPagoFrac(
					riesgoEndosoDTO.getValorRecargoPagoFrac().doubleValue()*riesgoEndosoDTO.getPorcentajeComision().doubleValue()/100d);
			//valorBonifComRecPagoFrac = ValorComisionRecPagoFrac * porcentajeBonifComision/100
//				riesgoEndosoDTO.setValorBonifComRecPagoFrac(cotizacionDTO.getMontoBonificacionComisionPagoFraccionado()*(riesgoEndosoDTO.getValorPrimaNeta()/primaNetaCotizacion));
			/*
			 * 05/08/2011. la bonificación de RPF se debe calcular multiplicando la comisión de RPF (previamente calculada) por el porcentaje de bonificación
			 */
			riesgoEndosoDTO.setValorBonifComRecPagoFrac(riesgoEndosoDTO.getValorComisionRecPagoFrac().doubleValue()*
					endosoACancelarRehabilitar.getCotizacionDTO().getPorcentajebonifcomision().doubleValue()/100d);
		}else{
			//valorRecargoPagoFrac = valorPrimaNeta * PorcentajeRPF/100			
			riesgoEndosoDTO.setValorRecargoPagoFrac(riesgoEndosoDTO.getValorPrimaNeta() * cotizacionDTO.getPorcentajePagoFraccionado() / 100);
			//valorComisionRecPagoFrac = valorRecargoPagoFrac * porcentajeComision/100
			riesgoEndosoDTO.setValorComisionRecPagoFrac(riesgoEndosoDTO.getValorRecargoPagoFrac() * riesgoEndosoDTO.getPorcentajeComision()/100);
			//valorBonifComRecPagoFrac = ValorComisionRecPagoFrac * porcentajeBonifComision/100
			riesgoEndosoDTO.setValorBonifComRecPagoFrac(riesgoEndosoDTO.getValorComisionRecPagoFrac() * cotizacionDTO.getPorcentajebonifcomision()/100);
		}
		
		//valorComFinalRecPagoFrac = valorComisionRecPagoFrac - valorBonifComRecPagoFac
		riesgoEndosoDTO.setValorComFinalRecPago(riesgoEndosoDTO.getValorComisionRecPagoFrac() - riesgoEndosoDTO.getValorBonifComRecPagoFrac());
	}
	
	private void actualizarPrimaNetaComisionRiesgo(RiesgoEndosoDTO riesgoEndosoDTO,RiesgoCotizacionDTO riesgo,
			RiesgoCotizacionDTO riesgoBase, Double primaNetaRiesgoBase,
			ComisionCotizacionDTO comision,
			EndosoDTO endosoEmitido,double diasPorDevengar,boolean riesgoEliminado){
		if(endosoEmitido.getClaveTipoEndoso() == TIPO_ENDOSO_CANCELACION) {
			riesgoEndosoDTO.setValorPrimaNeta(((riesgo.getValorPrimaNeta()) * (diasPorDevengar / 365D ) * -1D));
		}else if(endosoEmitido.getClaveTipoEndoso() == TIPO_ENDOSO_REHABILITACION || endosoEmitido.getClaveTipoEndoso() == TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO){
			riesgoEndosoDTO.setValorPrimaNeta(riesgo.getValorPrimaNeta()* (diasPorDevengar / 365D ));
		}else{
			if(riesgoEliminado){
				riesgoEndosoDTO.setValorPrimaNeta(((riesgo.getValorPrimaNeta()) * (diasPorDevengar / 365D ) * -1D));
			}else{
				boolean riesgoEndosoAnteriorContratado = false;
				if(riesgoBase != null){
					riesgoEndosoAnteriorContratado = 
						(riesgoBase.getCoberturaCotizacionDTO().getSeccionCotizacionDTO().getClaveContrato().shortValue() == SistemaPersistencia.CONTRATADO
						&& riesgoBase.getCoberturaCotizacionDTO().getClaveContrato().shortValue() == SistemaPersistencia.CONTRATADO
						&& riesgoBase.getClaveContrato().shortValue() == SistemaPersistencia.CONTRATADO);						
				}
	
				boolean riesgoEndosoActualContratado = 
					(riesgo.getCoberturaCotizacionDTO().getSeccionCotizacionDTO().getClaveContrato().shortValue() == SistemaPersistencia.CONTRATADO
					&& riesgo.getCoberturaCotizacionDTO().getClaveContrato().shortValue() == SistemaPersistencia.CONTRATADO
					&& riesgo.getClaveContrato().shortValue() == SistemaPersistencia.CONTRATADO);			

				if(!riesgoEndosoActualContratado && riesgoEndosoAnteriorContratado){// Baja de riesgo 
					riesgoEndosoDTO.setValorPrimaNeta(((riesgo.getValorPrimaNeta()) * (diasPorDevengar / 365D ) * -1D));
				}else if (riesgoEndosoActualContratado && !riesgoEndosoAnteriorContratado ){ //Alta de riesgo
					riesgoEndosoDTO.setValorPrimaNeta(((riesgo.getValorPrimaNeta()) * (diasPorDevengar / 365D )));
				}else{
					riesgoEndosoDTO.setValorPrimaNeta((riesgo.getValorPrimaNeta() - primaNetaRiesgoBase) * (diasPorDevengar / 365D ));
				}
			
			}
		}
		
		riesgoEndosoDTO.setPorcentajeComision(comision.getPorcentajeComisionCotizacion().doubleValue());
		//valorComision = primaNeta del riesgo * porcentajeComision/100
		riesgoEndosoDTO.setValorComision(riesgoEndosoDTO.getValorPrimaNeta()*riesgoEndosoDTO.getPorcentajeComision()/100);
	}
	
	private ComisionCotizacionDTO obtenerComision(RiesgoCotizacionDTO riesgo,CotizacionDTO cotizacionDTO,
			EndosoDTO endosoEmitido,EndosoDTO endosoACancelarRehabilitar){
		List<ParametroGeneralDTO> parametrosLimiteSA = parametroGeneral.findByProperty("id.codigoParametroGeneral", BigDecimal.valueOf(30010D));
		Double limiteSA = Double.valueOf(parametrosLimiteSA.get(0).getValor());
		
		Double tipoCambioVal = cotizacionDTO.getTipoCambio();
		
		AgrupacionCotDTO agrupacion = agrupacionCot.buscarPorCotizacion(cotizacionDTO.getIdToCotizacion(),TIPO_PRIMER_RIESGO);
		
		ComisionCotizacionId id = new ComisionCotizacionId();
		id.setIdTcSubramo(riesgo.getIdTcSubramo());
		
		//15/08/2011. Para endosos de cancelación y rehabilitación (póliza y endoso) se debe usar la misma comisión del endoso a cancelar ó rehabilitar.
		if(endosoEmitido.getClaveTipoEndoso().shortValue() == TIPO_ENDOSO_CANCELACION ||
				endosoEmitido.getClaveTipoEndoso().shortValue() == TIPO_ENDOSO_REHABILITACION ||
				endosoEmitido.getClaveTipoEndoso().shortValue() == SistemaPersistencia.TIPO_ENDOSO_CE ||
				endosoEmitido.getClaveTipoEndoso().shortValue() == SistemaPersistencia.TIPO_ENDOSO_RE){
			id.setIdToCotizacion(endosoACancelarRehabilitar.getIdToCotizacion());
		}
		else{
			id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
		}
		
		ComisionCotizacionDTO comision = null;
		Double sumaAseguradaIncendio = 0D;
		if(riesgo.getIdTcSubramo().intValue() == SistemaPersistencia.INCENDIO){
			limiteSA = limiteSA/tipoCambioVal;
			// Se obtiene la SA de Incendio por Inciso
			if (endosoEmitido.getClaveTipoEndoso().shortValue() == SistemaPersistencia.TIPO_ENDOSO_CE
					|| endosoEmitido.getClaveTipoEndoso().shortValue() == SistemaPersistencia.TIPO_ENDOSO_RE) {
				
				sumaAseguradaIncendio = coberturaCotizacion
					.obtenerSACoberturasBasicasIncendioPorCotizacion(endosoACancelarRehabilitar.getIdToCotizacion());
			
			} else {
				
				sumaAseguradaIncendio = coberturaCotizacion
					.obtenerSACoberturasBasicasIncendioPorCotizacion(cotizacionDTO.getIdToCotizacion());
			
			}
								
			if (agrupacion != null) {
				if (riesgo.getCoberturaCotizacionDTO().getNumeroAgrupacion() == agrupacion.getId().getNumeroAgrupacion()) {
					id.setTipoPorcentajeComision(TIPO_PRR);
				}else{
					id.setTipoPorcentajeComision(TIPO_RO);
				}
			} else {
				if (sumaAseguradaIncendio < limiteSA) {
					id.setTipoPorcentajeComision(TIPO_RO);
				} else if (sumaAseguradaIncendio >= limiteSA) {
					id.setTipoPorcentajeComision(TIPO_RCI);
				}else{
					id.setTipoPorcentajeComision(TIPO_RO);	
				}
			}				
		}else{
			id.setTipoPorcentajeComision(TIPO_RO);
		}
		comision = comisionCotizacion.findById(id);
		
		return comision;
	}
	
	protected void actualizaEndosoCFP(EndosoDTO endosoEmitido, EndosoDTO endosoAnterior, Double ivaCotizacion){
		valorPrimaNetaEndoso= 0D;
		valorRecargoPagoFraccionadoEndoso= 0D;
		valorDerechosEndoso = 0D;
		valorBonificacionComisionEndoso = 0D;
		valorIVAEndoso = 0D;
		valorPrimaTotalEndoso= 0D;
		valorBonificacionComisionRPFEndoso = 0D;
		valorComisionEndoso = 0D;
		valorComisionRPFEndoso = 0D;	
		
		RiesgoEndosoId id = new RiesgoEndosoId();
		id.setIdToPoliza(endosoEmitido.getId().getIdToPoliza());
		id.setNumeroEndoso(endosoEmitido.getId().getNumeroEndoso());
		
		List<RiesgoEndosoDTO> riesgoEndosoEmitido = riesgoEndoso.listarFiltrado(id);
		
		for(RiesgoEndosoDTO riesgoEmido: riesgoEndosoEmitido){
			
			RiesgoEndosoDTO riesgoBase = new RiesgoEndosoDTO();
			RiesgoEndosoId id1 = new RiesgoEndosoId();
			id1.setIdToPoliza(riesgoEmido.getId().getIdToPoliza());
			id1.setNumeroEndoso(endosoAnterior.getId().getNumeroEndoso());
			id1.setNumeroInciso(riesgoEmido.getId().getNumeroInciso());
			id1.setIdToSeccion(riesgoEmido.getId().getIdToSeccion());
			id1.setIdToCobertura(riesgoEmido.getId().getIdToCobertura());
			id1.setIdToRiesgo(riesgoEmido.getId().getIdToRiesgo());
			
			riesgoBase = riesgoEndoso.findById(id1);
			
			if(riesgoBase != null){
				//riesgoEmido.setValorPrimaNeta(riesgoEmido.getValorPrimaNeta() - (riesgoBase.getValorPrimaNeta() * endosoEmitido.getFactorAplicacion()));
				riesgoEmido.setValorPrimaNeta(0D);
				riesgoEmido.setValorRecargoPagoFrac(riesgoEmido.getValorRecargoPagoFrac() - (riesgoBase.getValorRecargoPagoFrac() * endosoEmitido.getFactorAplicacion()));
				//riesgoEmido.setValorDerechos(riesgoEmido.getValorDerechos() - riesgoBase.getValorDerechos());
				riesgoEmido.setValorDerechos(0D);
				riesgoEmido.setValorBonifComision(riesgoEmido.getValorBonifComision() - (riesgoBase.getValorBonifComision() * endosoEmitido.getFactorAplicacion()));
				//riesgoEmido.setValorIVA(riesgoEmido.getValorIVA() - (riesgoBase.getValorIVA() * endosoEmitido.getFactorAplicacion()));
				riesgoEmido.setValorIVA((riesgoEmido.getValorPrimaNeta() + riesgoEmido.getValorRecargoPagoFrac() +riesgoEmido.getValorDerechos() 
						- riesgoEmido.getValorBonifComision() -riesgoEmido.getValorBonifComRecPagoFrac())* ivaCotizacion/100D); 
				
				Double primaTotal = riesgoEmido.getValorPrimaNeta() + riesgoEmido.getValorRecargoPagoFrac() +riesgoEmido.getValorDerechos()
				- riesgoEmido.getValorBonifComision() -riesgoEmido.getValorBonifComRecPagoFrac();
				
				riesgoEmido.setValorPrimaTotal(primaTotal + riesgoEmido.getValorIVA());						
				//riesgoEmido.setValorPrimaTotal(riesgoEmido.getValorPrimaTotal() - (riesgoBase.getValorPrimaTotal() * endosoEmitido.getFactorAplicacion()));
				riesgoEmido.setValorBonifComRecPagoFrac(riesgoEmido.getValorBonifComRecPagoFrac() - (riesgoBase.getValorBonifComRecPagoFrac() * endosoEmitido.getFactorAplicacion()));
				riesgoEmido.setValorComision(0D);
				riesgoEmido.setValorComisionRecPagoFrac(riesgoEmido.getValorComisionRecPagoFrac() - (riesgoBase.getValorComisionRecPagoFrac() * endosoEmitido.getFactorAplicacion()));
				
				//Acumula a Nivel Endoso
				valorPrimaNetaEndoso += riesgoEmido.getValorPrimaNeta();
				valorRecargoPagoFraccionadoEndoso += riesgoEmido.getValorRecargoPagoFrac();
				valorDerechosEndoso += riesgoEmido.getValorDerechos();
				valorBonificacionComisionEndoso += riesgoEmido.getValorBonifComision();
				valorIVAEndoso += riesgoEmido.getValorIVA();
				valorPrimaTotalEndoso += riesgoEmido.getValorPrimaTotal();
				valorBonificacionComisionRPFEndoso += riesgoEmido.getValorBonifComRecPagoFrac();
				valorComisionEndoso += riesgoEmido.getValorComision();
				valorComisionRPFEndoso += riesgoEmido.getValorComisionRecPagoFrac();
				
				riesgoEndoso.update(riesgoEmido);				
			}
		}
		acumulaCoberturas(endosoEmitido);
		acumulaEndoso(endosoEmitido);
	}
	protected void acumulaCoberturas(EndosoDTO ultimoEndoso){
		CoberturaEndosoId id = new CoberturaEndosoId();
		id.setIdToPoliza(ultimoEndoso.getId().getIdToPoliza());
		id.setNumeroEndoso(ultimoEndoso.getId().getNumeroEndoso());
		
		List<CoberturaEndosoDTO> coberturas = coberturaEndoso.listarFiltrado(id);
		
		double valorRPF;
		double valorDerechos;
		double valorBonificacionComis;
		double valorBonificacionComisRFP;
		double valorIVA;
		double valorPrimaNetaTotal;
		double valorPrimaNeta;
		double porcentajeComision;
		double valorComision;
		double valorComisionFinal;
		double valorComisionRPF;
		double valorComisionFinalRPF;	
		
		for(CoberturaEndosoDTO cobertura: coberturas){
			valorRPF = 0D;
			valorDerechos = 0D;
			valorBonificacionComis = 0D;	
			valorBonificacionComisRFP = 0D;
			valorIVA = 0D;
			valorPrimaNetaTotal = 0d;
			valorPrimaNeta = 0d;
			porcentajeComision = 0D;
			valorComision = 0D;
			valorComisionFinal = 0D;
			valorComisionRPF = 0D;
			valorComisionFinalRPF = 0D;
			
			CoberturaCotizacionId id2 = new CoberturaCotizacionId();
			id2.setIdToCotizacion(ultimoEndoso.getIdToCotizacion());
			id2.setIdToCobertura(cobertura.getId().getIdToCobertura());
			id2.setIdToSeccion(cobertura.getId().getIdToSeccion());
			id2.setNumeroInciso(cobertura.getId().getNumeroInciso());
			
			CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
			coberturaCotizacionDTO.setId(id2);
			
			coberturaCotizacionDTO = coberturaCotizacion.findById(id2);
			
			RiesgoEndosoId id3  = new RiesgoEndosoId();
			id3.setIdToPoliza(cobertura.getId().getIdToPoliza());
			id3.setNumeroEndoso(cobertura.getId().getNumeroEndoso());
			id3.setNumeroInciso(cobertura.getId().getNumeroInciso());
			id3.setIdToSeccion(cobertura.getId().getIdToSeccion());
			id3.setIdToCobertura(cobertura.getId().getIdToCobertura());
			
			List<RiesgoEndosoDTO> riesgos = riesgoEndoso.listarFiltrado(id3);
			
			for(RiesgoEndosoDTO riesgo : riesgos){
				valorRPF += riesgo.getValorRecargoPagoFrac();
				valorDerechos += riesgo.getValorDerechos();
				valorBonificacionComis += riesgo.getValorBonifComision();
				valorBonificacionComisRFP += riesgo.getValorBonifComRecPagoFrac();
				valorIVA += riesgo.getValorIVA();
				valorPrimaNetaTotal += riesgo.getValorPrimaTotal();
				porcentajeComision = riesgo.getPorcentajeComision();
				valorComision += riesgo.getValorComision();
				valorComisionFinal += riesgo.getValorComisionFinal();
				valorComisionRPF += riesgo.getValorComisionRecPagoFrac();
				valorComisionFinalRPF += riesgo.getValorComFinalRecPago();
				valorPrimaNeta += riesgo.getValorPrimaNeta();
			}
			// valorCoaseguro = valorPrimaNeta * porcentajeCoaseguro /100	
			if(coberturaCotizacionDTO != null){
				cobertura.setValorCoaseguro(cobertura.getValorPrimaNeta() * coberturaCotizacionDTO.getPorcentajeCoaseguro() / 100D);
			// valorDeducible = valorPrimaNeta * porcentajeDeducible /100
				cobertura.setValorDeducible(cobertura.getValorPrimaNeta() * coberturaCotizacionDTO.getPorcentajeDeducible() / 100D);
			}
			cobertura.setValorRecargoPagoFrac(valorRPF);
			cobertura.setValorBonifComision(valorBonificacionComis);
			cobertura.setValorBonifComRecPagoFrac(valorBonificacionComisRFP);
			cobertura.setValorIVA(valorIVA);
			cobertura.setValorDerechos(valorDerechos);
			cobertura.setValorPrimaTotal(valorPrimaNetaTotal);
			cobertura.setPorcentajeComision(porcentajeComision);
			cobertura.setValorComision(valorComision);
			cobertura.setValorComisionFinal(valorComisionFinal);
			cobertura.setValorComisionRecPagoFrac(valorComisionRPF);
			cobertura.setValorComisionFinalRecPagoFrac(valorComisionFinalRPF);
			cobertura.setValorPrimaNeta(valorPrimaNeta);
			coberturaEndoso.update(cobertura);
		}
		
	}
	protected void acumulaEndoso(EndosoDTO ultimoEndoso){
		ultimoEndoso.setValorPrimaNeta(valorPrimaNetaEndoso);
		ultimoEndoso.setValorRecargoPagoFrac(valorRecargoPagoFraccionadoEndoso);
		ultimoEndoso.setValorDerechos(valorDerechosEndoso);
		ultimoEndoso.setValorBonifComision(valorBonificacionComisionEndoso);
		ultimoEndoso.setValorIVA(valorIVAEndoso);
		ultimoEndoso.setValorPrimaTotal(valorPrimaTotalEndoso);
		ultimoEndoso.setValorBonifComisionRPF(valorBonificacionComisionRPFEndoso);
		ultimoEndoso.setValorComision(valorComisionEndoso);
		ultimoEndoso.setValorComisionRPF(valorComisionRPFEndoso);
	}
	protected Short obtieneClaveTipoEndoso(BigDecimal idCotizacion, Short claveTipoEndosoSolicitud) {
		Short tipoEndoso = null;
		
		if (claveTipoEndosoSolicitud.equals(this.SOLICITUD_TIPO_ENDOSO_CANCELACION)) {
			tipoEndoso = this.TIPO_ENDOSO_CANCELACION;
		} else if (claveTipoEndosoSolicitud.equals(this.SOLICITUD_TIPO_ENDOSO_REHABILITACION)) {
			tipoEndoso = this.TIPO_ENDOSO_REHABILITACION;
		} else if (claveTipoEndosoSolicitud.equals(this.SOLICITUD_TIPO_ENDOSO_DECLARACION)) {
			tipoEndoso = this.TIPO_ENDOSO_AUMENTO;
		} else if (claveTipoEndosoSolicitud.equals(this.SOLICITUD_TIPO_ENDOSO_MODIFICACION)) {
			
			BigDecimal diferenciaPrimaNeta = movimientoCotizacion.obtenerDiferenciaPrimaNeta(idCotizacion);
			if (diferenciaPrimaNeta == null || diferenciaPrimaNeta.intValue() == 0){
				tipoEndoso = this.TIPO_ENDOSO_CAMBIO_DATOS;
			}else if (diferenciaPrimaNeta.intValue() > 0) {
				tipoEndoso = this.TIPO_ENDOSO_AUMENTO;
			} else if (diferenciaPrimaNeta.intValue() < 0) {
				tipoEndoso = this.TIPO_ENDOSO_DISMINUCION;
			} 
		}else if(claveTipoEndosoSolicitud.equals(this.SOLICITUD_TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO)){
			tipoEndoso = this.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO;
		}else {
			tipoEndoso = this.TIPO_ENDOSO_CAMBIO_DATOS;
		}
		
		return tipoEndoso;
		
	}

	protected SolicitudDTO copiaSolicitud(BigDecimal idToPoliza,
			String usuarioCreacion, Date fechaIniVigEnd,
			CotizacionDTO cotizacionOrigen, BigDecimal tipoEndoso,Short motivoEndoso,Short tipoSolicitud) {
		SolicitudDTO solicitudDTO = new SolicitudDTO();
		SolicitudDTO solicitudOrigen = cotizacionOrigen.getSolicitudDTO();
		solicitudDTO.setApellidoMaterno(solicitudOrigen.getApellidoMaterno());
		solicitudDTO.setApellidoPaterno(solicitudOrigen.getApellidoPaterno());
		solicitudDTO.setNombrePersona(solicitudOrigen.getNombrePersona());
		solicitudDTO.setClaveTipoPersona(solicitudOrigen.getClaveTipoPersona());

		solicitudDTO.setClaveEstatus(solicitudOrigen.getClaveEstatus());
		solicitudDTO.setClaveOpcionEmision(solicitudOrigen.getClaveOpcionEmision());
		solicitudDTO.setClaveTipoEndoso(tipoEndoso);
		solicitudDTO.setClaveTipoSolicitud(tipoSolicitud);
		solicitudDTO.setCodigoAgente(solicitudOrigen.getCodigoAgente());
		solicitudDTO.setCodigoUsuarioCreacion(usuarioCreacion);
		solicitudDTO.setCodigoUsuarioModificacion(usuarioCreacion);
		solicitudDTO.setEmailContactos(solicitudOrigen.getEmailContactos());
		solicitudDTO.setFechaCreacion(new Date());
		solicitudDTO.setFechaInicioVigenciaEndoso(fechaIniVigEnd);
		solicitudDTO.setFechaModificacion(new Date());
		solicitudDTO.setIdToPolizaAnterior(solicitudOrigen.getIdToPolizaAnterior());
		solicitudDTO.setIdToPolizaEndosada(idToPoliza);
		solicitudDTO.setProductoDTO(solicitudOrigen.getProductoDTO());
		solicitudDTO.setTelefonoContacto(solicitudOrigen.getTelefonoContacto());	
		solicitudDTO.setClaveMotivoEndoso(motivoEndoso);

		solicitudDTO.setNombreAgente(solicitudOrigen.getNombreAgente());
		solicitudDTO.setNombreOficinaAgente(solicitudOrigen
				.getNombreOficinaAgente());
		solicitudDTO.setIdOficina(solicitudOrigen.getIdOficina());
		solicitudDTO.setCodigoEjecutivo(solicitudOrigen.getCodigoEjecutivo());
        solicitudDTO.setNombreEjecutivo(solicitudOrigen.getNombreEjecutivo());
        solicitudDTO.setNombreOficinaAgente(solicitudOrigen.getNombreOficinaAgente());
        solicitudDTO.setNombreOficina(solicitudOrigen.getNombreOficina());
		return solicitudDTO;
	}

	protected CotizacionDTO creaCopiaCotizacion(SolicitudDTO solicitud, String usuarioCreacion,CotizacionDTO cotizacionOrigen, Short motivoEndoso) {
		
		CotizacionDTO cotizacionDestino = new CotizacionDTO();
		
		cotizacionDestino.setTipoPolizaDTO(cotizacionOrigen.getTipoPolizaDTO());
		cotizacionDestino.setSolicitudDTO(solicitud); //Se le asigna la nueva solicitud de endoso
		cotizacionDestino.setDireccionCobroDTO(cotizacionOrigen.getDireccionCobroDTO());
		cotizacionDestino.setIdMoneda(cotizacionOrigen.getIdMoneda());
		cotizacionDestino.setIdFormaPago(cotizacionOrigen.getIdFormaPago());
		cotizacionDestino.setFechaInicioVigencia(solicitud.getFechaInicioVigenciaEndoso());
		cotizacionDestino.setFechaFinVigencia(cotizacionOrigen.getFechaFinVigencia());
		cotizacionDestino.setNombreEmpresaContratante(cotizacionOrigen.getNombreEmpresaContratante());
		cotizacionDestino.setNombreEmpresaAsegurado(cotizacionOrigen.getNombreEmpresaAsegurado());
		cotizacionDestino.setCodigoUsuarioOrdenTrabajo(null);
		cotizacionDestino.setCodigoUsuarioCotizacion(usuarioCreacion); //Usuario al que se le asigna la cotización de endoso
		cotizacionDestino.setFechaCreacion(new Date());
		cotizacionDestino.setCodigoUsuarioCreacion(usuarioCreacion);  //Va el usuario que esta creando la nueva cotizacion
		cotizacionDestino.setFechaModificacion(new Date());
		cotizacionDestino.setCodigoUsuarioModificacion(usuarioCreacion);
		cotizacionDestino.setClaveEstatus(cotizacionOrigen.getClaveEstatus());
		cotizacionDestino.setPorcentajebonifcomision(cotizacionOrigen.getPorcentajebonifcomision());
		cotizacionDestino.setIdToPersonaContratante(cotizacionOrigen.getIdToPersonaContratante());
		cotizacionDestino.setIdToPersonaAsegurado(cotizacionOrigen.getIdToPersonaAsegurado());
		cotizacionDestino.setIdMedioPago(cotizacionOrigen.getIdMedioPago());
		cotizacionDestino.setClaveAutRetroacDifer(null);
		cotizacionDestino.setClaveAutVigenciaMaxMin(null);
		cotizacionDestino.setDiasGracia(cotizacionOrigen.getDiasGracia());
		cotizacionDestino.setFolioPolizaAsociada(cotizacionOrigen.getFolioPolizaAsociada());
		cotizacionDestino.setClaveAutoEmisionDocOperIlicitas(null);
		cotizacionDestino.setNombreAsegurado(cotizacionOrigen.getNombreAsegurado());
		cotizacionDestino.setNombreContratante(cotizacionOrigen.getNombreContratante());
		cotizacionDestino.setClaveMotivoEndoso(motivoEndoso);
		cotizacionDestino.setTipoCambio(cotizacionOrigen.getTipoCambio());
		cotizacionDestino.setPorcentajePagoFraccionado(cotizacionOrigen.getPorcentajePagoFraccionado());
		cotizacionDestino.setPorcentajeIva(cotizacionOrigen.getPorcentajeIva());
		cotizacionDestino.setTipoNegocioDTO(cotizacionOrigen.getTipoNegocioDTO());
		
		cotizacionDestino.setClaveAutorizacionDiasGracia(cotizacionOrigen.getClaveAutorizacionDiasGracia());
				
		return cotizacionDestino;
		
	}	
	
	public String copiarDocumentosDigitales(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = " INSERT INTO MIDAS.todocdigitalcot (idtodocdigitalcot, idtocotizacion, idtocontrolarchivo,"
				+ "fechacreacion, codigousuariocreacion, nombreusuariocreacion,fechamodificacion, codigousuariomodificacion,nombreusuariomodificacion)"
				+ " SELECT MIDAS.IDTODOCDIGITALCOT_SEQ.NEXTVAL,"
				+ idToCotizacion
				+ ", idtocontrolarchivo,"
				+ " fechacreacion, codigousuariocreacion, nombreusuariocreacion, fechamodificacion, codigousuariomodificacion, nombreusuariomodificacion"
				+ " FROM MIDAS.todocdigitalcot where idtocotizacion = "
				+ idToCotizacionBase;

		return queryString;
	}

	public String copiarDocumentosCotizacion(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "INSERT INTO MIDAS.todocanexocot (idtocotizacion, idtocontrolarchivo, fechacreacion,codigousuariocreacion, nombreusuariocreacion, fechamodificacion,"
				+ "codigousuariomodificacion, nombreusuariomodificacion,descripciondocumentoanexo, claveobligatoriedad, claveseleccion,orden, clavetipo, idtocobertura) "
				+ " SELECT "
				+ idToCotizacion
				+ ", idtocontrolarchivo, fechacreacion,codigousuariocreacion, nombreusuariocreacion, fechamodificacion,"
				+ " codigousuariomodificacion, nombreusuariomodificacion,descripciondocumentoanexo, claveobligatoriedad, claveseleccion, orden, clavetipo, idtocobertura "
				+ " FROM MIDAS.todocanexocot where idtocotizacion = "
				+ idToCotizacionBase;
		return queryString;
	}

	public String copiarDocumentosReaseguro(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "INSERT INTO MIDAS.todocanexoreasegurocot (idtocotizacion, idtocontrolarchivo, numerosecuencia, descripciondocumentoanexo, fechacreacion,"
				+ "  codigousuariocreacion, nombreusuariocreacion, fechamodificacion, codigousuariomodificacion, nombreusuariomodificacion)"
				+ " SELECT "
				+ idToCotizacion
				+ ", idtocontrolarchivo, numerosecuencia, descripciondocumentoanexo, fechacreacion, codigousuariocreacion,"
				+ " nombreusuariocreacion, fechamodificacion, codigousuariomodificacion, nombreusuariomodificacion FROM MIDAS.todocanexoreasegurocot "
				+ " WHERE idtocotizacion = " + idToCotizacionBase;
		return queryString;
	}

	public String copiarTextosAdicionales(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "INSERT INTO MIDAS.totexadicionalcot (idtotexadicionalcot, idtocotizacion, descripciontexto, claveautorizacion, codigousuarioautorizacion, fechacreacion,"
				+ " codigousuariocreacion, nombreusuariocreacion, fechamodificacion, codigousuariomodificacion, nombreusuariomodificacion, numerosecuencia, fechasolicitudautorizacion, fechaautorizacion)"
				+ " SELECT MIDAS.IDTOTEXADICIONALCOT_SEQ.nextval, "
				+ idToCotizacion
				+ ", descripciontexto,claveautorizacion, codigousuarioautorizacion, fechacreacion,"
				+ " codigousuariocreacion, nombreusuariocreacion, fechamodificacion, codigousuariomodificacion, nombreusuariomodificacion, numerosecuencia, fechasolicitudautorizacion, fechaautorizacion"
				+ " FROM MIDAS.totexadicionalcot WHERE idtocotizacion = "
				+ idToCotizacionBase;
		return queryString;
	}

	public String copiarIncisos(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase, BigDecimal numeroInciso) {
		String queryString = "insert into MIDAS.toincisocot (idtocotizacion, numeroinciso, idtodireccioninciso, valorprimaneta, claveestatusinspeccion,"
				+ " clavetipoorigeninspeccion, clavemensajeinspeccion, codigousuarioestinspeccion, claveautinspeccion, fechaestatusinspeccion,"
				+ " fechasolautinspeccion, fechaautinspeccion) "
				+ " select "
				+ idToCotizacion
				+ " , incisocot.numeroinciso, incisocot.idtodireccioninciso, incisocot.valorprimaneta, incisocot.claveestatusinspeccion, incisocot.clavetipoorigeninspeccion, incisocot.clavemensajeinspeccion,"
				+ " incisocot.codigousuarioestinspeccion, incisocot.claveautinspeccion, incisocot.fechaestatusinspeccion,incisocot.fechasolautinspeccion, incisocot.fechaautinspeccion "
				+ " from MIDAS.toincisocot incisocot  where incisocot.idtocotizacion = "
				+ idToCotizacionBase;
		if (numeroInciso != null) {
			queryString += " and incisocot.numeroinciso = " + numeroInciso;
		}
		
		return queryString;
	}

	public String copiarSecciones(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase, BigDecimal numeroInciso) {
		String queryString = "insert into MIDAS.toseccioncot (idtocotizacion, numeroinciso, idtoseccion, valorprimaneta, valorsumaasegurada,"
				+ " claveobligatoriedad, clavecontrato) "
				+ " select "
				+ idToCotizacion
				+ " , seccioncot.numeroinciso, seccioncot.idtoseccion, seccioncot.valorprimaneta, seccioncot.valorsumaasegurada, seccioncot.claveobligatoriedad, seccioncot.clavecontrato "
				+ " from MIDAS.toseccioncot seccioncot where seccioncot.idtocotizacion = "
				+ idToCotizacionBase;
		
		if (numeroInciso != null) {
			queryString += " and seccioncot.numeroinciso = " + numeroInciso;
		}
		
		return queryString;
	}

	public String copiarCoberturas(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase, BigDecimal numeroInciso) {

		String queryString = "insert into MIDAS.tocoberturacot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura, idtcsubramo,"
				+ " valorprimaneta, valorsumaasegurada, valorcoaseguro, valordeducible, codigousuarioautreaseguro, "
				+ " claveobligatoriedad, clavecontrato, valorcuota, porcentajecoaseguro, claveautcoaseguro, "
				+ " codigousuarioautcoaseguro, porcentajededucible, claveautdeducible, codigousuarioautdeducible, numeroagrupacion, "
				+ " fechasolautcoaseguro, fechaautcoaseguro, fechasolautdeducible, fechaautdeducible, clavetipodeducible, "
				+ " clavetipolimitededucible, valorminimolimitededucible, valormaximolimitededucible) "
				+ " select "
				+ idToCotizacion
				+ " , coberturacot.numeroinciso, coberturacot.idtoseccion, coberturacot.idtocobertura, coberturacot.idtcsubramo,"
				+ " coberturacot.valorprimaneta, coberturacot.valorsumaasegurada, coberturacot.valorcoaseguro, coberturacot.valordeducible, coberturacot.codigousuarioautreaseguro,"
				+ " coberturacot.claveobligatoriedad, coberturacot.clavecontrato, coberturacot.valorcuota, coberturacot.porcentajecoaseguro, coberturacot.claveautcoaseguro,"
				+ " coberturacot.codigousuarioautcoaseguro, coberturacot.porcentajededucible, coberturacot.claveautdeducible, coberturacot.codigousuarioautdeducible, numeroagrupacion, "
				+ " coberturacot.fechasolautcoaseguro, coberturacot.fechaautcoaseguro, coberturacot.fechasolautdeducible, coberturacot.fechaautdeducible, coberturacot.clavetipodeducible,"
				+ " coberturacot.clavetipolimitededucible, coberturacot.valorminimolimitededucible, coberturacot.valormaximolimitededucible"
				+ " from MIDAS.tocoberturacot coberturacot where coberturacot.idtocotizacion ="
				+ idToCotizacionBase;

		if (numeroInciso != null) {
			queryString += " and coberturacot.numeroinciso = " + numeroInciso;
		}
		
		return queryString;
	}

	public String copiarRiesgos(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase, BigDecimal numeroInciso) {
		String queryString = "insert into MIDAS.toriesgocot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura, idtoriesgo, "
				+ " idtcsubramo, valorprimaneta, valorsumaasegurada, valorcoaseguro, valordeducible, "
				+ " valordescuento, valorrecargo, valoraumento, porcentajecoaseguro, claveautcoaseguro, "
				+ " codigousuarioautcoaseguro, porcentajededucible, claveautdeducible, codigousuarioautdeducible, claveobligatoriedad, "
				+ " clavecontrato, valorcuotab, valorcuotaardt, valorprimanetaardt, valorprimanetab,  "
				+ " fechasolautcoaseguro, fechaautcoaseguro, fechasolautdeducible, fechaautdeducible, clavetipodeducible,  "
				+ " clavetipolimitededucible, valorminimolimitededucible, valormaximolimitededucible) "
				+ " select "
				+ idToCotizacion
				+ " ,riesgocot.numeroinciso, riesgocot.idtoseccion, riesgocot.idtocobertura, riesgocot.idtoriesgo,"
				+ " riesgocot.idtcsubramo, riesgocot.valorprimaneta, riesgocot.valorsumaasegurada, riesgocot.valorcoaseguro, riesgocot.valordeducible,"
				+ " riesgocot.valordescuento, riesgocot.valorrecargo, riesgocot.valoraumento, riesgocot.porcentajecoaseguro, riesgocot.claveautcoaseguro,"
				+ " riesgocot.codigousuarioautcoaseguro, riesgocot.porcentajededucible, riesgocot.claveautdeducible, riesgocot.codigousuarioautdeducible, riesgocot.claveobligatoriedad, "
				+ " riesgocot.clavecontrato, riesgocot.valorcuotab, riesgocot.valorcuotaardt, riesgocot.valorprimanetaardt, riesgocot.valorprimanetab,"
				+ " riesgocot.fechasolautcoaseguro, riesgocot.fechaautcoaseguro, riesgocot.fechasolautdeducible, riesgocot.fechaautdeducible, riesgocot.clavetipodeducible,"
				+ " riesgocot.clavetipolimitededucible, riesgocot.valorminimolimitededucible, riesgocot.valormaximolimitededucible"
				+ " from MIDAS.toriesgocot riesgocot where riesgocot.idtocotizacion ="
				+ idToCotizacionBase;

		if (numeroInciso != null) {
			queryString += " and riesgocot.numeroinciso = " + numeroInciso;
		}
		
		return queryString;
	}
	
	public String copiarAgrupaciones(BigDecimal idToCotizacion ,BigDecimal idToCotizacionBase) {
		String queryString = "INSERT INTO MIDAS.toagrupacioncot (idToCotizacion, numeroAgrupacion, claveTipoAgrupacion, idToSeccion, valorSumaAsegurada, valorCuota, valorPrimaNeta) "
				+ " SELECT "
				+ idToCotizacion
				+ ", agrupacioncot.numeroAgrupacion, agrupacioncot.claveTipoAgrupacion, agrupacioncot.idToSeccion, "
				+ " agrupacioncot.valorSumaAsegurada, agrupacioncot.valorCuota, agrupacioncot.valorPrimaNeta "
				+ " FROM MIDAS.toagrupacioncot agrupacioncot WHERE agrupacioncot.idToCotizacion = "
				+ idToCotizacionBase;

		return queryString;
	}	
	
	public String copiarsubIncisos(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase, BigDecimal numeroInciso) {
		String queryString = "insert into MIDAS.tosubincisocot (idtocotizacion, numeroinciso, idtoseccion, numerosubinciso, claveautreaseguro, "
				+ " codigousuarioautreaseguro, descripcionsubinciso, valorsumaasegurada, claveestatusdeclaracion) "
				+ " SELECT "
				+ idToCotizacion
				+ ", subincisocot.numeroinciso, subincisocot.idtoseccion, subincisocot.numerosubinciso, subincisocot.claveautreaseguro, "
				+ " subincisocot.codigousuarioautreaseguro, subincisocot.descripcionsubinciso, subincisocot.valorsumaasegurada, subincisocot.claveestatusdeclaracion "
				+ " FROM MIDAS.tosubincisocot subincisocot WHERE subincisocot.idToCotizacion = "
				+ idToCotizacionBase;

		if (numeroInciso != null) {
			queryString += " and subincisocot.numeroinciso = " + numeroInciso;
		}
		
		return queryString;
	}

	public String copiarDetallePrimaCobertura(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase, BigDecimal numeroInciso) {
		String queryString = "insert into MIDAS.todetprimacoberturacot (numerosubinciso, idtocotizacion, numeroinciso, idtoseccion, idtocobertura,"
				+ " valorprimanetab, valorprimanetaardt, valorprimaneta, valorcuotab, valorcuotaardt, "
				+ " valorcuotaardv, valorcuota)"
				+ " SELECT detprima.numerosubinciso, "
				+ idToCotizacion
				+ ", detprima.numeroinciso, detprima.idtoseccion, detprima.idtocobertura, "
				+ " detprima.valorprimanetab, detprima.valorprimanetaardt, detprima.valorprimaneta, detprima.valorcuotab, detprima.valorcuotaardt, "
				+ " detprima.valorcuotaardv, detprima.valorcuota "
				+ " FROM MIDAS.todetprimacoberturacot detprima WHERE detprima.idToCotizacion = "
				+ idToCotizacionBase;

		if (numeroInciso != null) {
			queryString += " and detprima.numeroinciso = " + numeroInciso;
		}
		
		return queryString;
	}

	public String copiarDetallePrimaRiesgo(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase, BigDecimal numeroInciso) {
		String queryString = "insert into MIDAS.todetprimariesgocot (numerosubinciso, idtocotizacion, numeroinciso, idtoseccion, idtocobertura, idtoriesgo,"
				+ " valorsumaasegurada, valorprimanetab, valorprimanetaardt, valorprimaneta, valorcuotab, valorcuotaardt, "
				+ " valorcuotaardv, valorcuota)"
				+ " SELECT detprima.numerosubinciso, "
				+ idToCotizacion
				+ ", detprima.numeroinciso, detprima.idtoseccion, detprima.idtocobertura, detprima.idtoriesgo,"
				+ " detprima.valorsumaasegurada, detprima.valorprimanetab, detprima.valorprimanetaardt, detprima.valorprimaneta, detprima.valorcuotab, detprima.valorcuotaardt, "
				+ " detprima.valorcuotaardv, detprima.valorcuota "
				+ " FROM MIDAS.todetprimariesgocot detprima WHERE detprima.idToCotizacion = "
				+ idToCotizacionBase;

		if (numeroInciso != null) {
			queryString += " and detprima.numeroinciso = " + numeroInciso;
		}
		
		return queryString;
	}

	protected String copiarComisiones(BigDecimal idToCotizacion, BigDecimal idToCotizacionBase) {
		String query = "INSERT INTO MIDAS.tocomisioncot c1 " +
				"(c1.idtocotizacion, c1.idtcsubramo, " +
				"c1.valorcomisioncotizacion, c1.claveautcomision, " +
				"c1.codigousuarioautcomision, " +
				"c1.clavetipoporcentajecomision, " +
				"c1.porcentajecomisiondefault, " +
				"c1.porcentajecomisioncotizacion, " +
				"c1.fechasolautcomision, c1.fechaautcomision) " +
				"SELECT "+idToCotizacion+", c2.idtcsubramo, " +
				"c2.valorcomisioncotizacion, c2.claveautcomision, " +
				"c2.codigousuarioautcomision, " +
				"c2.clavetipoporcentajecomision, " +
				"c2.porcentajecomisiondefault, " +
				"c2.porcentajecomisioncotizacion, " +
				"c2.fechasolautcomision, c2.fechaautcomision " +
				"FROM MIDAS.tocomisioncot c2 " +
				"WHERE c2.idtocotizacion = "+idToCotizacionBase;

		return query;
	}
	     
	protected String copiarDatosIncisos(BigDecimal idToCotizacion, BigDecimal idToCotizacionBase, BigDecimal numeroInciso) {
		String queryString = "INSERT INTO MIDAS.todatoincisocot d1 " +
				"(d1.idtocotizacion, d1.numeroinciso, d1.idtoseccion, " +
				"d1.idtocobertura, d1.idtoriesgo, d1.numerosubinciso, d1.idtcramo, " +
				"d1.idtcsubramo, d1.clavedetalle, d1.iddato, d1.valor) " +
				"SELECT "+idToCotizacion+", d2.numeroinciso, d2.idtoseccion, " +
				"d2.idtocobertura, d2.idtoriesgo, d2.numerosubinciso, d2.idtcramo, " +
				"d2.idtcsubramo, d2.clavedetalle, d2.iddato, d2.valor " +
				"FROM MIDAS.todatoincisocot d2 " +
				"WHERE d2.idtocotizacion = "+idToCotizacionBase;
		
		if (numeroInciso != null) {
			queryString += " and d2.numeroinciso = " + numeroInciso;
		}
		
		
		return queryString;
	}	

	protected String copiarAumentos(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase, BigDecimal numeroInciso) {
		String queryString = "INSERT INTO MIDAS.toaumentoriesgocot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura,"
				+ " idtoriesgo, idtoaumentovario, claveautorizacion, codigousuarioautorizacion, valoraumento, claveobligatoriedad, "
				+ " clavecontrato, clavecomercialtecnico, clavenivel, fechasolicitudautorizacion, fechaautorizacion) "
				+ " select "
				+ idToCotizacion
				+ " ,riesgocot.numeroinciso, riesgocot.idtoseccion, riesgocot.idtocobertura,"
				+ " riesgocot.idtoriesgo, riesgocot.idtoaumentovario, riesgocot.claveautorizacion, riesgocot.codigousuarioautorizacion, riesgocot.valoraumento, riesgocot.claveobligatoriedad,"
				+ " riesgocot.clavecontrato, riesgocot.clavecomercialtecnico, riesgocot.clavenivel, riesgocot.fechasolicitudautorizacion, riesgocot.fechaautorizacion"
				+ " from MIDAS.toaumentoriesgocot riesgocot where riesgocot.idtocotizacion = "
				+ idToCotizacionBase;

		if (numeroInciso != null) {
			queryString += " and riesgocot.numeroinciso = " + numeroInciso;
		}
		
		return queryString;
	}
	
	protected String copiarRecargos(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase, BigDecimal numeroInciso) {
		String queryString = "INSERT INTO MIDAS.torecargoriesgocot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura,"
				+ " idtoriesgo, idtorecargovario, claveautorizacion, codigousuarioautorizacion, valorrecargo, claveobligatoriedad, "
				+ " clavecontrato, clavecomercialtecnico, clavenivel, fechasolicitudautorizacion, fechaautorizacion) "
				+ " select "
				+ idToCotizacion
				+ " ,riesgocot.numeroinciso, riesgocot.idtoseccion, riesgocot.idtocobertura,"
				+ " riesgocot.idtoriesgo, riesgocot.idtorecargovario, riesgocot.claveautorizacion, riesgocot.codigousuarioautorizacion, riesgocot.valorrecargo, riesgocot.claveobligatoriedad,"
				+ " riesgocot.clavecontrato, riesgocot.clavecomercialtecnico, riesgocot.clavenivel, riesgocot.fechasolicitudautorizacion, riesgocot.fechaautorizacion"
				+ " from MIDAS.torecargoriesgocot riesgocot where riesgocot.idtocotizacion = "
				+ idToCotizacionBase;

		if (numeroInciso != null) {
			queryString += " and riesgocot.numeroinciso = " + numeroInciso;
		}
		
		return queryString;
	}	

	protected String copiarDescuentos(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase, BigDecimal numeroInciso) {
		String queryString = "INSERT INTO MIDAS.todescuentoriesgocot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura,"
				+ " idtoriesgo, idtodescuentovario, claveautorizacion, codigousuarioautorizacion, valordescuento, claveobligatoriedad, "
				+ " clavecontrato, clavecomercialtecnico, clavenivel, fechasolicitudautorizacion, fechaautorizacion) "
				+ " select "
				+ idToCotizacion
				+ " ,riesgocot.numeroinciso, riesgocot.idtoseccion, riesgocot.idtocobertura,"
				+ " riesgocot.idtoriesgo, riesgocot.idtodescuentovario, riesgocot.claveautorizacion, riesgocot.codigousuarioautorizacion, riesgocot.valordescuento, riesgocot.claveobligatoriedad,"
				+ " riesgocot.clavecontrato, riesgocot.clavecomercialtecnico, riesgocot.clavenivel, riesgocot.fechasolicitudautorizacion, riesgocot.fechaautorizacion"
				+ " from MIDAS.todescuentoriesgocot riesgocot where riesgocot.idtocotizacion = "
				+ idToCotizacionBase;

		if (numeroInciso != null) {
			queryString += " and riesgocot.numeroinciso = " + numeroInciso;
		}
		
		return queryString;
	}	
	
	//Indica si se deben regresar los derechos de un endoso durante la cancelación
	//Los derechos se regresan siempre y cuando la fecha de inicio de vigencia del endoso de cancelación
	//sea igual a la fecha de inicio de vigencia del endoso a cancelar
	private boolean regresarDerechosEndoso(Date fechaInicioVigenciaEndosoACancelar, Date fechaInicioVigenciaEndosoCancelacion, boolean primerReciboPagado){
		if(fechaInicioVigenciaEndosoACancelar.compareTo(fechaInicioVigenciaEndosoCancelacion)==0 ||
		   fechaInicioVigenciaEndosoACancelar.compareTo(fechaInicioVigenciaEndosoCancelacion)>0 ||
		  !primerReciboPagado)
			return true;
		else 
			return false;		
	}
	
	//Indica si el primer recibo del endoso ya está pagado
	private boolean primerReciboPagado(List<ReciboDTO> recibos, EndosoDTO endoso){		
		for(ReciboDTO recibo : recibos){
			if(recibo.getNumeroEndoso().equals(endoso.getId().getNumeroEndoso().toString()) &&
			   recibo.getSituacion().trim().equals(RECIBO_PAGADO)){
				return true;				
			}
		}
		return false;
	}
	    
	/**
	 * Agrega dias a una fecha
	 * @param Date fecha
	 * @param int dias
	 * @return Date fechaIncrementada
	 */	
	public Date sumaDias(Date fecha, int dias) {
		
		Calendar fechaC = Calendar.getInstance();
		fechaC.setTime(fecha);
		fechaC.set(GregorianCalendar.HOUR_OF_DAY, 0);
		fechaC.set(GregorianCalendar.MINUTE, 0);
		fechaC.set(GregorianCalendar.SECOND, 0);
		fechaC.set(GregorianCalendar.MILLISECOND, 0);	
		
		fechaC.add(Calendar.DATE, dias);

		return fechaC.getTime();
	}
	public static Double obtenerDiasEntreFechas(Date fechaInicial, Date fechaFinal){

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        
        try{
            fechaInicial = sdf.parse(sdf.format(fechaInicial));
            fechaFinal = sdf.parse(sdf.format(fechaFinal));
        }catch(Exception e){}

		long diferencia = fechaFinal.getTime()- fechaInicial.getTime();
		
		double dias = (double)diferencia / (1000 * 60 * 60 * 24);
		
		return dias;
		
	}	
}
