package mx.com.afirme.midas.siniestro.documentos;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity DocumentoSiniestroDTO.
 * @see .DocumentoSiniestroDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DocumentoSiniestroFacade  implements DocumentoSiniestroFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DocumentoSiniestroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocumentoSiniestroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocumentoSiniestroDTO entity) {
    				LogUtil.log("saving DocumentoSiniestroDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DocumentoSiniestroDTO entity.
	  @param entity DocumentoSiniestroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocumentoSiniestroDTO entity) {
    				LogUtil.log("deleting DocumentoSiniestroDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DocumentoSiniestroDTO.class, entity.getIdToDocumentoSiniestro());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DocumentoSiniestroDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocumentoSiniestroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocumentoSiniestroDTO entity to update
	 @return DocumentoSiniestroDTO the persisted DocumentoSiniestroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DocumentoSiniestroDTO update(DocumentoSiniestroDTO entity) {
    				LogUtil.log("updating DocumentoSiniestroDTO instance", Level.INFO, null);
	        try {
            DocumentoSiniestroDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DocumentoSiniestroDTO findById( BigDecimal id) {
    				LogUtil.log("finding DocumentoSiniestroDTO instance with id: " + id, Level.INFO, null);
	        try {
            DocumentoSiniestroDTO instance = entityManager.find(DocumentoSiniestroDTO.class, id);
            entityManager.refresh(instance);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DocumentoSiniestroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocumentoSiniestroDTO property to query
	  @param value the property value to match
	  	  @return List<DocumentoSiniestroDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DocumentoSiniestroDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding DocumentoSiniestroDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DocumentoSiniestroDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			

	/**
	 * Find all DocumentoSiniestroDTO entities.
	  	  @return List<DocumentoSiniestroDTO> all DocumentoSiniestroDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DocumentoSiniestroDTO> findAll(
		) {
					LogUtil.log("finding all DocumentoSiniestroDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from DocumentoSiniestroDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public DocumentoSiniestroDTO obtenDocumentoSiniestro(BigDecimal idReporteSiniestro, BigDecimal idTipoDocumentoSiniestro) {
		LogDeMidasEJB3.log("obtenDocumentoSiniestro Consulta", Level.INFO, null);
		try {
			final String queryString = "select model.idToDocumentoSiniestro from DocumentoSiniestroDTO model" +
									" where model.tipoDocumentoSiniestroDTO.idTcTipoDocumentoSiniestro= :idTipoDocumentoSiniestro" +
									" and model.reporteSiniestroDTO.idToReporteSiniestro= :idReporteSiniestro";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTipoDocumentoSiniestro", idTipoDocumentoSiniestro);
			query.setParameter("idReporteSiniestro", idReporteSiniestro);
			List resultados = query.getResultList();
			BigDecimal idDocumentoSiniestro= new BigDecimal(0);
			for (Object resultado : resultados) {
				idDocumentoSiniestro = (BigDecimal) resultado;
			}
			return findById(idDocumentoSiniestro);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenDocumentoSiniestro failed", Level.SEVERE, re);
			throw re;
		}
	}
	
}