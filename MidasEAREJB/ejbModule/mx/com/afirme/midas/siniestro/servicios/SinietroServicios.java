package mx.com.afirme.midas.siniestro.servicios;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableDTO;
import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableFacadeRemote;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDTO;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeFacadeRemote;

@Stateless
public class SinietroServicios implements SinietroServiciosRemote {

	
	@EJB
	private AsientoContableFacadeRemote asientoContableFacade;
	
	
	@EJB
	private SolicitudChequeFacadeRemote solicitudChequeFacade;
	
	
	public List<AsientoContableDTO> contabilizaMovimientos(String idObjetoContable, String claveTransaccionContable,String nombreUsuario){
		List<AsientoContableDTO> result = new ArrayList<AsientoContableDTO>();
		try {
			result =  asientoContableFacade.contabilizaMovimientos(idObjetoContable, claveTransaccionContable, nombreUsuario);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public BigDecimal solicitaCheque (SolicitudChequeDTO solicitudCheque, String nombreUsuario){
		BigDecimal result = null;
		try {
			result = solicitudChequeFacade.solicitaCheque(solicitudCheque, nombreUsuario);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public SolicitudChequeDTO consultaEstatusSolicitudCheque (BigDecimal idPago, String nombreUsuario){
		SolicitudChequeDTO result = null;
		try {
			result = solicitudChequeFacade.consultaEstatusSolicitudCheque(idPago, nombreUsuario);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public String cancelaSolicitudCheque (BigDecimal idPago, String nombreUsuario){
		String result = null;
		try {
			result = solicitudChequeFacade.cancelaSolicitudCheque(idPago, nombreUsuario);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
