package mx.com.afirme.midas.siniestro.salvamento;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity SalvamentoSiniestro.
 * 
 * @see .SalvamentoSiniestro
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SalvamentoSiniestroFacade implements
		SalvamentoSiniestroFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SalvamentoSiniestro
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SalvamentoSiniestro entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SalvamentoSiniestroDTO entity) {
		LogUtil.log("saving SalvamentoSiniestro instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SalvamentoSiniestro entity.
	 * 
	 * @param entity
	 *            SalvamentoSiniestro entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SalvamentoSiniestroDTO entity) {
		LogUtil.log("deleting SalvamentoSiniestro instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SalvamentoSiniestroDTO.class,
					entity.getIdToSalvamentoSiniestro());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SalvamentoSiniestro entity and return it or a
	 * copy of it to the sender. A copy of the SalvamentoSiniestro entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SalvamentoSiniestro entity to update
	 * @return SalvamentoSiniestro the persisted SalvamentoSiniestro entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SalvamentoSiniestroDTO update(SalvamentoSiniestroDTO entity) {
		LogUtil.log("updating SalvamentoSiniestro instance", Level.INFO, null);
		try {
			SalvamentoSiniestroDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SalvamentoSiniestroDTO findById(BigDecimal id) {
		LogUtil.log("finding SalvamentoSiniestro instance with id: " + id,
				Level.INFO, null);
		try {
			SalvamentoSiniestroDTO instance = entityManager.find(
					SalvamentoSiniestroDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SalvamentoSiniestro entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SalvamentoSiniestro property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<SalvamentoSiniestro> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SalvamentoSiniestroDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding SalvamentoSiniestro instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SalvamentoSiniestroDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SalvamentoSiniestro entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<SalvamentoSiniestro> all SalvamentoSiniestro entities
	 */
	@SuppressWarnings("unchecked")
	public List<SalvamentoSiniestroDTO> findAll() {
		LogUtil.log("finding all SalvamentoSiniestro instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from SalvamentoSiniestroDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find all SalvamentoSiniestro entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<SalvamentoSiniestro> all SalvamentoSiniestro entities
	 */
	@SuppressWarnings("unchecked")
	public List<SalvamentoSiniestroDTO> findByNotDeletedStatus(ReporteSiniestroDTO reporteSiniestroDTO) {
		LogUtil.log("finding all not deleted SalvamentoSiniestro instances", Level.INFO,
				null);
		try {
			String queryString = "select model from SalvamentoSiniestroDTO model";
			String sWhere = " where ";
			sWhere += "model.estatusInterno <> :estatusInterno and model.reporteSiniestro.idToReporteSiniestro=:idToReporteSiniestro";
			
			Query query = entityManager.createQuery(queryString + sWhere);
			query.setParameter("estatusInterno", SalvamentoSiniestroDTO.ESTATUS_ELIMINADO);
			query.setParameter("idToReporteSiniestro", reporteSiniestroDTO.getIdToReporteSiniestro());
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all not deleted failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find all SalvamentoSiniestro entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<SalvamentoSiniestro> all SalvamentoSiniestro entities
	 */
	@SuppressWarnings("unchecked")
	public List<SalvamentoSiniestroDTO> getSalvamentosPorReporteYEstatus(ReporteSiniestroDTO reporteSiniestroDTO,Object... params) {
		LogUtil.log("finding all getSalvamentosPorReporteYEstatus SalvamentoSiniestro instances", Level.INFO,
				null);
		String valoresEstatus = new String();
		valoresEstatus = obtenerValoresEstatus(params);
		
		if (valoresEstatus.trim().length()>0){
			valoresEstatus = valoresEstatus.substring(0, valoresEstatus.length()-1);
		}
		try {
			String queryString = "select model from SalvamentoSiniestroDTO model";
			String sWhere = " where ";
			sWhere += "model.salvamentoEstatus.idTcSalvamentoEstatus in (" 
					+ valoresEstatus + ") and model.reporteSiniestro.idToReporteSiniestro=:idToReporteSiniestro";
			
			Query query = entityManager.createQuery(queryString + sWhere);
			query.setParameter("idToReporteSiniestro", reporteSiniestroDTO.getIdToReporteSiniestro());
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all getSalvamentosPorReporteYEstatus failed", Level.SEVERE, re);
			throw re;
		}
	}

	public String obtenerValoresEstatus(Object[] params) {
		StringBuilder valoresEstatus = new StringBuilder();
		for (Object estatus : params) {
			valoresEstatus.append(String.valueOf(estatus)).append(","); 
		}
		return valoresEstatus.toString();
	}
	
}