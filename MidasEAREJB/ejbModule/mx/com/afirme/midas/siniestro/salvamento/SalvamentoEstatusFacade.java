package mx.com.afirme.midas.siniestro.salvamento;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity SalvamentoEstatus.
 * 
 * @see .SalvamentoEstatus
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SalvamentoEstatusFacade implements
		SalvamentoEstatusFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SalvamentoEstatus
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SalvamentoEstatus entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SalvamentoEstatusDTO entity) {
		LogUtil.log("saving SalvamentoEstatus instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SalvamentoEstatus entity.
	 * 
	 * @param entity
	 *            SalvamentoEstatus entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SalvamentoEstatusDTO entity) {
		LogUtil.log("deleting SalvamentoEstatus instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SalvamentoEstatusDTO.class,
					entity.getIdTcSalvamentoEstatus());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SalvamentoEstatus entity and return it or a
	 * copy of it to the sender. A copy of the SalvamentoEstatus entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SalvamentoEstatus entity to update
	 * @return SalvamentoEstatus the persisted SalvamentoEstatus entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SalvamentoEstatusDTO update(SalvamentoEstatusDTO entity) {
		LogUtil.log("updating SalvamentoEstatus instance", Level.INFO, null);
		try {
			SalvamentoEstatusDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SalvamentoEstatusDTO findById(BigDecimal id) {
		LogUtil.log("finding SalvamentoEstatus instance with id: " + id,
				Level.INFO, null);
		try {
			SalvamentoEstatusDTO instance = entityManager.find(
					SalvamentoEstatusDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SalvamentoEstatus entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SalvamentoEstatus property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<SalvamentoEstatus> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SalvamentoEstatusDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding SalvamentoEstatus instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SalvamentoEstatusDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SalvamentoEstatus entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<SalvamentoEstatus> all SalvamentoEstatus entities
	 */
	@SuppressWarnings("unchecked")
	public List<SalvamentoEstatusDTO> findAll() {
		LogUtil.log("finding all SalvamentoEstatus instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from SalvamentoEstatusDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SalvamentoEstatusDTO> findNotSaled() {
		LogUtil.log("finding all SalvamentoEstatus instances", Level.INFO,
				null);
		try {
			String queryString = "select model from SalvamentoEstatusDTO model"
								+ " where model.idTcSalvamentoEstatus<>" + SalvamentoEstatusDTO.ESTATUS_VENDIDO;
			System.out.print("Si refresco");
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
}