package mx.com.afirme.midas.siniestro.cabina;
// default package

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroLogDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroLogFacadeRemote;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroLogId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Facade for entity ReporteSiniestroLogDTO.
 * @see .ReporteSiniestroLogDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ReporteSiniestroLogFacade  implements ReporteSiniestroLogFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ReporteSiniestroLogDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ReporteSiniestroLogDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ReporteSiniestroLogDTO entity) {
    				LogDeMidasEJB3.log("saving ReporteSiniestroLogDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ReporteSiniestroLogDTO entity.
	  @param entity ReporteSiniestroLogDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ReporteSiniestroLogDTO entity) {
    				LogDeMidasEJB3.log("deleting ReporteSiniestroLogDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ReporteSiniestroLogDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ReporteSiniestroLogDTO entity and return it or a copy of it to the sender. 
	 A copy of the ReporteSiniestroLogDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ReporteSiniestroLogDTO entity to update
	 @return ReporteSiniestroLogDTO the persisted ReporteSiniestroLogDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ReporteSiniestroLogDTO update(ReporteSiniestroLogDTO entity) {
    				LogDeMidasEJB3.log("updating ReporteSiniestroLogDTO instance", Level.INFO, null);
	        try {
            ReporteSiniestroLogDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ReporteSiniestroLogDTO findById( ReporteSiniestroLogId id) {
    				LogDeMidasEJB3.log("finding ReporteSiniestroLogDTO instance with id: " + id, Level.INFO, null);
	        try {
            ReporteSiniestroLogDTO instance = entityManager.find(ReporteSiniestroLogDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ReporteSiniestroLogDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReporteSiniestroLogDTO property to query
	  @param value the property value to match
	  	  @return List<ReporteSiniestroLogDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ReporteSiniestroLogDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ReporteSiniestroLogDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ReporteSiniestroLogDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ReporteSiniestroLogDTO entities.
	  	  @return List<ReporteSiniestroLogDTO> all ReporteSiniestroLogDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReporteSiniestroLogDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ReporteSiniestroLogDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ReporteSiniestroLogDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}