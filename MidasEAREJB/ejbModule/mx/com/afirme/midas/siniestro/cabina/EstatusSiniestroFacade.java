package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.siniestro.cabina.EstatusSiniestroDTO;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity EstatusSiniestro.
 * 
 * @see .EstatusSiniestro
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class EstatusSiniestroFacade implements
		EstatusSiniestroFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved EstatusSiniestro
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            EstatusSiniestro entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(EstatusSiniestroDTO entity) {
		LogUtil.log("saving EstatusSiniestro instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent EstatusSiniestro entity.
	 * 
	 * @param entity
	 *            EstatusSiniestro entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(EstatusSiniestroDTO entity) {
		LogUtil.log("deleting EstatusSiniestro instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(EstatusSiniestroDTO.class,
					entity.getIdTcEstatusSiniestro());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved EstatusSiniestro entity and return it or a
	 * copy of it to the sender. A copy of the EstatusSiniestro entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            EstatusSiniestro entity to update
	 * @return EstatusSiniestro the persisted EstatusSiniestro entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public EstatusSiniestroDTO update(EstatusSiniestroDTO entity) {
		LogUtil.log("updating EstatusSiniestro instance", Level.INFO, null);
		try {
			EstatusSiniestroDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public EstatusSiniestroDTO findById(BigDecimal id) {
		LogUtil.log("finding EstatusSiniestro instance with id: " + id,
				Level.INFO, null);
		try {
			EstatusSiniestroDTO instance = entityManager.find(
					EstatusSiniestroDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all EstatusSiniestro entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the EstatusSiniestro property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<EstatusSiniestro> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<EstatusSiniestroDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding EstatusSiniestro instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from EstatusSiniestroDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all EstatusSiniestro entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<EstatusSiniestro> all EstatusSiniestro entities
	 */
	@SuppressWarnings("unchecked")
	public List<EstatusSiniestroDTO> findAll() {
		LogUtil.log("finding all EstatusSiniestro instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from EstatusSiniestroDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
}