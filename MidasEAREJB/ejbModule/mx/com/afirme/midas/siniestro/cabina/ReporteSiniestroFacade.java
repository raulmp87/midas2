package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ReporteSiniestroDTO.
 * @see .ReporteSiniestroDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ReporteSiniestroFacade  implements ReporteSiniestroFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	@EJB
	private PolizaFacadeRemote polizaFacade;
		/**
	 Perform an initial save of a previously unsaved ReporteSiniestroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ReporteSiniestroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public ReporteSiniestroDTO save(ReporteSiniestroDTO entity) {
    				LogDeMidasEJB3.log("saving ReporteSiniestroDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
            			
            return entity;
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ReporteSiniestroDTO entity.
	  @param entity ReporteSiniestroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public void delete(ReporteSiniestroDTO entity) {
    				LogDeMidasEJB3.log("deleting ReporteSiniestroDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ReporteSiniestroDTO.class, entity.getIdToReporteSiniestro());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ReporteSiniestroDTO entity and return it or a copy of it to the sender. 
	 A copy of the ReporteSiniestroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ReporteSiniestroDTO entity to update
	 @return ReporteSiniestroDTO the persisted ReporteSiniestroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public ReporteSiniestroDTO update(ReporteSiniestroDTO entity) {
    				LogDeMidasEJB3.log("updating ReporteSiniestroDTO instance", Level.INFO, null);
	        try {
            ReporteSiniestroDTO result = entityManager.merge(entity);
            LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ReporteSiniestroDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding ReporteSiniestroDTO instance with id: " + id, Level.INFO, null);
	        try {
            ReporteSiniestroDTO instance = entityManager.find(ReporteSiniestroDTO.class, id);
			if(instance != null){
				if(instance.getNumeroPoliza() != null){
					try{
						instance.setPolizaDTO(polizaFacade.findById(instance.getNumeroPoliza()));
					}catch (Exception e){}							
				}
			}            
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ReporteSiniestroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReporteSiniestroDTO property to query
	  @param value the property value to match
	  	  @return List<ReporteSiniestroDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ReporteSiniestroDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ReporteSiniestroDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ReporteSiniestroDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ReporteSiniestroDTO entities.
	  	  @return List<ReporteSiniestroDTO> all ReporteSiniestroDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReporteSiniestroDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ReporteSiniestroDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ReporteSiniestroDTO model where model.reporteEstatus.idTcReporteEstatus <> "+ReporteSiniestroDTO.PENDIENTE_CERRADO +" order by model.fechaHoraReporte desc";
			System.out.println(queryString);
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ReporteSiniestroDTO> listarFiltrado(
			ReporteSiniestroDTO reporteSiniestroDTO) {
		LogDeMidasEJB3.log("listarFiltrado ReporteSiniestroDTO instances", Level.INFO, null);
		try {
			String queryString = "select model from ReporteSiniestroDTO model";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (reporteSiniestroDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "numeroReporte", reporteSiniestroDTO
							.getNumeroReporte());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "idCiudad", reporteSiniestroDTO.getIdCiudad());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "nombrePersonaReporta", reporteSiniestroDTO
							.getNombrePersonaReporta());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "telefonoPersonaReporta", reporteSiniestroDTO
							.getTelefonoPersonaReporta());
			//Si vienen datos entonces se agrega el filtro de fecha
			if(reporteSiniestroDTO.getFechaHoraReporte()!=null){
				GregorianCalendar fechaReporte = new GregorianCalendar();
				fechaReporte.setTimeInMillis(reporteSiniestroDTO.getFechaHoraReporte().getTime());
				//Se genera la fecha inicial
				fechaReporte.set(Calendar.HOUR_OF_DAY, 0);
				fechaReporte.set(Calendar.MINUTE, 0);
				fechaReporte.set(Calendar.SECOND, 0);
				Timestamp fechaInicial = new Timestamp(fechaReporte.getTimeInMillis());
				//Se genera la fecha final
				fechaReporte.add(Calendar.DATE, 1);
				fechaReporte.add(Calendar.SECOND, -1);
				Timestamp fechaFinal = new Timestamp(fechaReporte.getTimeInMillis());
				//Se agrega el texto del filtro
				if (Utilerias.esAtributoQueryValido(sWhere))
					sWhere = sWhere.concat(" and ");
				sWhere = sWhere.concat("model.fechaHoraReporte BETWEEN :fechaInicio and :fechaFinal");
				Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicio", fechaInicial);
				Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"fechaFinal", fechaFinal);
			}
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			queryString = queryString.concat(" order by model.fechaHoraReporte desc");
			System.out.println(queryString);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}	

	/**
	 * A utilizarse para el caso de uso de Listar Reportes de Siniestro
	 * @param filtro
	 * @return
	 */
	public List<ReporteSiniestroDTO> listarFiltrado(ReporteSiniestroFiltroDTO filtro){
		
		return null;
	}
	
	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ReporteSiniestroDTO> listarReportesAbiertos() {
			LogDeMidasEJB3.log("consult listarReporteAbiertos", Level.INFO, null);
			List<ReporteSiniestroDTO> reportes = null;
			try {
				final String queryString = "select r from ReporteSiniestroDTO r where r.reporteEstatus.idTcReporteEstatus <> 25";
				Query query = entityManager.createQuery(queryString);
				reportes = query.getResultList();
				if(reportes != null){
					for(ReporteSiniestroDTO reporte: reportes){
						if(reporte.getNumeroPoliza() != null){
							try{
								reporte.setPolizaDTO(polizaFacade.findById(reporte.getNumeroPoliza()));
							}catch (Exception e){}							
						}
					}
				}
				return reportes;
			} catch (RuntimeException re) {
				LogDeMidasEJB3.log("consult listarReporteAbiertos failed", Level.SEVERE, re);
				throw re;
			}
	}
	
	@SuppressWarnings("unchecked")
	public List<ReporteSiniestroDTO> listarFiltradoPorNumPoliza(String idPolizas){
		LogDeMidasEJB3.log("consult listarFiltradoPorNumPoliza", Level.INFO, null);
		try {
			if (idPolizas==null||idPolizas.trim().length()==0)return null;
			final String queryString = "select model from ReporteSiniestroDTO model where model.numeroPoliza IN ("+idPolizas+") order by model.fechaHoraReporte desc";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("consult listarFiltradoPorNumPoliza failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ReporteSiniestroDTO> reportesSinInformePreliminar(){
		LogDeMidasEJB3.log("consult reportesSinInformePreliminar", Level.INFO, null);
		try {
			
			StringBuilder sb = new StringBuilder();		
			sb.append("select  ");
			sb.append("		model.* ");
			sb.append("from ");
			sb.append("		MIDAS.ToReporteSiniestro model ");
			sb.append("where ");
			sb.append("		model.idTcReporteEstatus in (");
			sb.append(ReporteSiniestroDTO.PENDIENTE_ASIGNAR_AJUSTADOR);
			sb.append(",");
			sb.append(ReporteSiniestroDTO.PENDIENTE_CONFIRMAR_AJUSTADOR);
			sb.append(",");
			sb.append(ReporteSiniestroDTO.PENDIENTE_VALIDAR_INFORME_PRELIMINAR);
			sb.append(")");
			sb.append("		and (trunc(sysdate) - trunc (model.fechaHoraReporte)) > 2 ");
			
			
			LogDeMidasEJB3.log(sb.toString(), Level.WARNING, null);			
			final String queryString = sb.toString();						
			
			Query query = entityManager.createNativeQuery(queryString, ReporteSiniestroDTO.class);
			return  query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("consult reportesSinInformePreliminar failed", Level.SEVERE, re);
			throw re;
		}
	}
}