package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;

/**
 * @author smvr
 *
 */
@Stateless
public class ConfigCorreoTipoNegocioFacade implements ConfigCorreoTipoNegocioFacadeRemote{

	@PersistenceContext
	private EntityManager entityManager;
	public static final String IDTCTIPONEGOCIO = "idTcTipoNegocio";
	
	
	public List<TipoNegocioDTO> findAll() {
		LogDeMidasEJB3.log( "finding all TipoNegocioDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from TipoNegocioDTO model " +
					"order by model.codigoTipoNegocio";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log( "find all failed" + re.getMessage(), Level.SEVERE, null );
			throw re;
		}
	}

	public List<TipoNegocioDTO> listarFiltrado(TipoNegocioDTO tipoNegocioDTO) {
		
		LogDeMidasEJB3.log( "finding all TipoNegocioDTO instances", Level.INFO, null);
		try {
			
			String queryString = "select model from TipoNegocioDTO model where model."
					+ IDTCTIPONEGOCIO + "= :propertyValue " +
					"order by model.codigoTipoNegocio";
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", tipoNegocioDTO.getIdTcTipoNegocio());
			
			if (tipoNegocioDTO == null) {
				return null;
			}
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();

		} catch (RuntimeException re) {
			
			LogDeMidasEJB3.log( "find all failed" + re.getMessage(), Level.SEVERE, null );
			throw re;
		}
	}
	
	public void save(ConfigCorreoTipoNegociooDTO entity) {
		LogDeMidasEJB3.log( "saving ConfigCorreoTipoNegociooDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log( "save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log( "save failed" + re.getMessage(), Level.SEVERE, null );
			throw re;
		}
	}
	
	public ConfigCorreoTipoNegociooDTO findById(BigDecimal idConfigTipoNegocio) {
		LogDeMidasEJB3.log( "finding ConfigCorreoTipoNegociooDTO instance with id: " + idConfigTipoNegocio, Level.INFO, null);
		try {
			ConfigCorreoTipoNegociooDTO instance = entityManager.find(ConfigCorreoTipoNegociooDTO.class, idConfigTipoNegocio);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log( "findById failed" + re.getMessage(), Level.SEVERE, null );
			throw re;
		}
	}
	
	public List<ConfigCorreoTipoNegociooDTO> findByTipoNegocio(BigDecimal idTcTipoNegocio) {
		LogDeMidasEJB3.log( "finding ConfigCorreoTipoNegociooDTO instance with id: " + idTcTipoNegocio, Level.INFO, null);
		try {

			String queryString = "select model from ConfigCorreoTipoNegociooDTO model where model." + IDTCTIPONEGOCIO + "= :propertyValue ";
		
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", idTcTipoNegocio);
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log( "findByTipoNegocio failed" + re.getMessage(), Level.SEVERE, null );
			throw re;
		}
	}
	
	public ConfigCorreoTipoNegociooDTO update(ConfigCorreoTipoNegociooDTO entity) {
			LogDeMidasEJB3.log( "updating ConfigCorreoTipoNegociooDTO instance", Level.INFO, null);
	        try {
	        	ConfigCorreoTipoNegociooDTO result = entityManager.merge(entity);
            	LogDeMidasEJB3.log( "update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        		LogDeMidasEJB3.log( "findByTipoNegocio failed" + re.getMessage(), Level.SEVERE, null );
	            throw re;
        }
	}
	
	public void delete(ConfigCorreoTipoNegociooDTO entity) {
		LogDeMidasEJB3.log( "deleting ConfigCorreoTipoNegociooDTO instance", Level.INFO, null);
        try {
	    	entity = entityManager.getReference(ConfigCorreoTipoNegociooDTO.class, entity.getIdConfigTipoNegocio());
	        entityManager.remove(entity);
	        LogDeMidasEJB3.log( "delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
        		LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
            throw re;
    }
	}
}
