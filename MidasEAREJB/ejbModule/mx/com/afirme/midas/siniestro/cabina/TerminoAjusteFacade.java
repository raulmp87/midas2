package mx.com.afirme.midas.siniestro.cabina;
// default package

import mx.com.afirme.midas.siniestro.cabina.TerminoAjusteDTO;
import mx.com.afirme.midas.siniestro.cabina.TerminoAjusteFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Facade for entity TerminoAjusteDTO.
 * @see .TerminoAjusteDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class TerminoAjusteFacade  implements TerminoAjusteFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved TerminoAjusteDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TerminoAjusteDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TerminoAjusteDTO entity) {
    				LogDeMidasEJB3.log("saving TerminoAjusteDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent TerminoAjusteDTO entity.
	  @param entity TerminoAjusteDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TerminoAjusteDTO entity) {
    				LogDeMidasEJB3.log("deleting TerminoAjusteDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(TerminoAjusteDTO.class, entity.getIdTcTerminoAjuste());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved TerminoAjusteDTO entity and return it or a copy of it to the sender. 
	 A copy of the TerminoAjusteDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TerminoAjusteDTO entity to update
	 @return TerminoAjusteDTO the persisted TerminoAjusteDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public TerminoAjusteDTO update(TerminoAjusteDTO entity) {
    				LogDeMidasEJB3.log("updating TerminoAjusteDTO instance", Level.INFO, null);
	        try {
            TerminoAjusteDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public TerminoAjusteDTO findById(BigDecimal id) {
    				LogDeMidasEJB3.log("finding TerminoAjusteDTO instance with id: " + id, Level.INFO, null);
	        try {
            TerminoAjusteDTO instance = entityManager.find(TerminoAjusteDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all TerminoAjusteDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TerminoAjusteDTO property to query
	  @param value the property value to match
	  	  @return List<TerminoAjusteDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<TerminoAjusteDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding TerminoAjusteDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from TerminoAjusteDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all TerminoAjusteDTO entities.
	  	  @return List<TerminoAjusteDTO> all TerminoAjusteDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TerminoAjusteDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all TerminoAjusteDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from TerminoAjusteDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}