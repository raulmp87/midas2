package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity SubEstatusSiniestro.
 * 
 * @see .SubEstatusSiniestro
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SubEstatusSiniestroFacade implements
		SubEstatusSiniestroFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SubEstatusSiniestro
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SubEstatusSiniestro entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubEstatusSiniestroDTO entity) {
		LogUtil.log("saving SubEstatusSiniestro instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SubEstatusSiniestro entity.
	 * 
	 * @param entity
	 *            SubEstatusSiniestro entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubEstatusSiniestroDTO entity) {
		LogUtil.log("deleting SubEstatusSiniestro instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SubEstatusSiniestroDTO.class,
					entity.getIdTcSubEstatusSiniestro());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SubEstatusSiniestro entity and return it or a
	 * copy of it to the sender. A copy of the SubEstatusSiniestro entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SubEstatusSiniestro entity to update
	 * @return SubEstatusSiniestro the persisted SubEstatusSiniestro entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubEstatusSiniestroDTO update(SubEstatusSiniestroDTO entity) {
		LogUtil.log("updating SubEstatusSiniestro instance", Level.INFO, null);
		try {
			SubEstatusSiniestroDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SubEstatusSiniestroDTO findById(BigDecimal id) {
		LogUtil.log("finding SubEstatusSiniestro instance with id: " + id,
				Level.INFO, null);
		try {
			SubEstatusSiniestroDTO instance = entityManager.find(
					SubEstatusSiniestroDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubEstatusSiniestro entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SubEstatusSiniestro property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<SubEstatusSiniestro> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SubEstatusSiniestroDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding SubEstatusSiniestro instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SubEstatusSiniestroDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubEstatusSiniestro entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<SubEstatusSiniestro> all SubEstatusSiniestro entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubEstatusSiniestroDTO> findAll() {
		LogUtil.log("finding all SubEstatusSiniestro instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from SubEstatusSiniestroDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
}