package mx.com.afirme.midas.siniestro.cabina;
// default package


import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.siniestro.cabina.CoordinadorZonaDTO;
import mx.com.afirme.midas.siniestro.cabina.CoordinadorZonaFacadeRemote;
import mx.com.afirme.midas.siniestro.cabina.CoordinadorZonaId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity CoordinadorZonaDTO.
 * @see .CoordinadorZonaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class CoordinadorZonaFacade  implements CoordinadorZonaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved CoordinadorZonaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CoordinadorZonaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CoordinadorZonaDTO entity) {
    				LogDeMidasEJB3.log("saving CoordinadorZonaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CoordinadorZonaDTO entity.
	  @param entity CoordinadorZonaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CoordinadorZonaDTO entity) {
    				LogDeMidasEJB3.log("deleting CoordinadorZonaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(CoordinadorZonaDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CoordinadorZonaDTO entity and return it or a copy of it to the sender. 
	 A copy of the CoordinadorZonaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CoordinadorZonaDTO entity to update
	 @return CoordinadorZonaDTO the persisted CoordinadorZonaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CoordinadorZonaDTO update(CoordinadorZonaDTO entity) {
    				LogDeMidasEJB3.log("updating CoordinadorZonaDTO instance", Level.INFO, null);
	        try {
            CoordinadorZonaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public CoordinadorZonaDTO findById( CoordinadorZonaId id) {
    				LogDeMidasEJB3.log("finding CoordinadorZonaDTO instance with id: " + id, Level.INFO, null);
	        try {
            CoordinadorZonaDTO instance = entityManager.find(CoordinadorZonaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all CoordinadorZonaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the CoordinadorZonaDTO property to query
	  @param value the property value to match
	  	  @return List<CoordinadorZonaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<CoordinadorZonaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding CoordinadorZonaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from CoordinadorZonaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all CoordinadorZonaDTO entities.
	  	  @return List<CoordinadorZonaDTO> all CoordinadorZonaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CoordinadorZonaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all CoordinadorZonaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from CoordinadorZonaDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}