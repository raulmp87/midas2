package mx.com.afirme.midas.siniestro.cabina;
// default package

import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Facade for entity ReporteEstatusDTO.
 * @see .ReporteEstatusDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ReporteEstatusFacade  implements ReporteEstatusFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ReporteEstatusDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ReporteEstatusDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ReporteEstatusDTO entity) {
    				LogDeMidasEJB3.log("saving ReporteEstatusDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ReporteEstatusDTO entity.
	  @param entity ReporteEstatusDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ReporteEstatusDTO entity) {
    				LogDeMidasEJB3.log("deleting ReporteEstatusDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ReporteEstatusDTO.class, entity.getIdTcReporteEstatus());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ReporteEstatusDTO entity and return it or a copy of it to the sender. 
	 A copy of the ReporteEstatusDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ReporteEstatusDTO entity to update
	 @return ReporteEstatusDTO the persisted ReporteEstatusDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ReporteEstatusDTO update(ReporteEstatusDTO entity) {
    				LogDeMidasEJB3.log("updating ReporteEstatusDTO instance", Level.INFO, null);
	        try {
            ReporteEstatusDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ReporteEstatusDTO findById( Byte id) {
    				LogDeMidasEJB3.log("finding ReporteEstatusDTO instance with id: " + id, Level.INFO, null);
	        try {
            ReporteEstatusDTO instance = entityManager.find(ReporteEstatusDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ReporteEstatusDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReporteEstatusDTO property to query
	  @param value the property value to match
	  	  @return List<ReporteEstatusDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ReporteEstatusDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ReporteEstatusDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ReporteEstatusDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ReporteEstatusDTO entities.
	  	  @return List<ReporteEstatusDTO> all ReporteEstatusDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReporteEstatusDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ReporteEstatusDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ReporteEstatusDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}