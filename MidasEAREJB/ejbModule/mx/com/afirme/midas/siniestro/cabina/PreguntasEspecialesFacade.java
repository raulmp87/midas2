package mx.com.afirme.midas.siniestro.cabina;
// default package

import mx.com.afirme.midas.siniestro.cabina.PreguntasEspecialesDTO;
import mx.com.afirme.midas.siniestro.cabina.PreguntasEspecialesFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Facade for entity PreguntasEspecialesDTO.
 * @see .PreguntasEspecialesDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class PreguntasEspecialesFacade  implements PreguntasEspecialesFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved PreguntasEspecialesDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PreguntasEspecialesDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(PreguntasEspecialesDTO entity) {
    				LogDeMidasEJB3.log("saving PreguntasEspecialesDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent PreguntasEspecialesDTO entity.
	  @param entity PreguntasEspecialesDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PreguntasEspecialesDTO entity) {
    				LogDeMidasEJB3.log("deleting PreguntasEspecialesDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(PreguntasEspecialesDTO.class, entity.getIdToReporteSiniestro());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved PreguntasEspecialesDTO entity and return it or a copy of it to the sender. 
	 A copy of the PreguntasEspecialesDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PreguntasEspecialesDTO entity to update
	 @return PreguntasEspecialesDTO the persisted PreguntasEspecialesDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public PreguntasEspecialesDTO update(PreguntasEspecialesDTO entity) {
    				LogDeMidasEJB3.log("updating PreguntasEspecialesDTO instance", Level.INFO, null);
	        try {
            PreguntasEspecialesDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public PreguntasEspecialesDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding PreguntasEspecialesDTO instance with id: " + id, Level.INFO, null);
	        try {
            PreguntasEspecialesDTO instance = entityManager.find(PreguntasEspecialesDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all PreguntasEspecialesDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PreguntasEspecialesDTO property to query
	  @param value the property value to match
	  	  @return List<PreguntasEspecialesDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<PreguntasEspecialesDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding PreguntasEspecialesDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from PreguntasEspecialesDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all PreguntasEspecialesDTO entities.
	  	  @return List<PreguntasEspecialesDTO> all PreguntasEspecialesDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<PreguntasEspecialesDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all PreguntasEspecialesDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from PreguntasEspecialesDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}