package mx.com.afirme.midas.siniestro.cabina;
// default package


import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.siniestro.cabina.DatosTransportistaDTO;
import mx.com.afirme.midas.siniestro.cabina.DatosTransportistaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity DatosTransportistaDTO.
 * @see .DatosTransportistaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DatosTransportistaFacade  implements DatosTransportistaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DatosTransportistaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DatosTransportistaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DatosTransportistaDTO entity) {
    				LogDeMidasEJB3.log("saving DatosTransportistaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DatosTransportistaDTO entity.
	  @param entity DatosTransportistaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DatosTransportistaDTO entity) {
    				LogDeMidasEJB3.log("deleting DatosTransportistaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DatosTransportistaDTO.class, entity.getIdToReporteSiniestro());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DatosTransportistaDTO entity and return it or a copy of it to the sender. 
	 A copy of the DatosTransportistaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DatosTransportistaDTO entity to update
	 @return DatosTransportistaDTO the persisted DatosTransportistaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DatosTransportistaDTO update(DatosTransportistaDTO entity) {
    				LogDeMidasEJB3.log("updating DatosTransportistaDTO instance", Level.INFO, null);
	        try {
            DatosTransportistaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DatosTransportistaDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding DatosTransportistaDTO instance with id: " + id, Level.INFO, null);
	        try {
            DatosTransportistaDTO instance = entityManager.find(DatosTransportistaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DatosTransportistaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DatosTransportistaDTO property to query
	  @param value the property value to match
	  	  @return List<DatosTransportistaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DatosTransportistaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding DatosTransportistaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DatosTransportistaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all DatosTransportistaDTO entities.
	  	  @return List<DatosTransportistaDTO> all DatosTransportistaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DatosTransportistaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all DatosTransportistaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from DatosTransportistaDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}