package mx.com.afirme.midas.siniestro.cabina;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacade;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless
public class PolizaDummyFacade extends CotizacionFacade implements PolizaDummyFacadeRemote{
	@PersistenceContext private EntityManager entityManager;	
	

	public List<PolizaDummyDTO> listarFiltrado(PolizaDummyDTO polizaDTO) {
		List<CotizacionDTO> lista = super.listarFiltrado(polizaDTO);
		return convertirLista(lista);
	}

	public PolizaDummyDTO obtenerPoliza(BigDecimal id) {
		PolizaDummyDTO polizaDummyDTO = new PolizaDummyDTO();
//		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionDTO cotizacionDTO  = this.findById(id);
		if(cotizacionDTO != null){
			polizaDummyDTO = castPolizaCotizacion(cotizacionDTO);
		} 
//		return (PolizaDummyDTO)this.findById(id);
		return polizaDummyDTO;
	}

	
	@SuppressWarnings("unchecked")
	public List<RiesgoPolizaDummyDTO> listarRiesgosPoliza(PolizaDummyDTO poliza){
		StringBuffer queryString = new StringBuffer();
		List<RiesgoCotizacionDTO> riesgosCotizacion = null;
		List<RiesgoPolizaDummyDTO> riesgosPoliza = new ArrayList<RiesgoPolizaDummyDTO>();
		
		try{
			queryString.append("select model from RiesgoCotizacionDTO model ");
			queryString.append("where model.id.idToCotizacion = :idToCotizacion ");
			
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("idToCotizacion", poliza.getIdToCotizacion());
			
			riesgosCotizacion = query.getResultList();

			//Invocando propiedades lazy que necesitaran que est�n cargadas en la capa web
			for(RiesgoCotizacionDTO riesgo : riesgosCotizacion){
				riesgo.getRiesgoCoberturaDTO().getCoberturaSeccionDTO().getSeccionDTO();
				riesgo.getRiesgoCoberturaDTO().getCoberturaSeccionDTO().getCoberturaDTO();
				riesgosPoliza.add((RiesgoPolizaDummyDTO)riesgo);				
			}			
			return riesgosPoliza;	
			
		}catch(RuntimeException exc){
			exc.printStackTrace();
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, exc);
			throw exc;			
		}
		

	}
	
	private List<PolizaDummyDTO> convertirLista(List<CotizacionDTO> lista){
		if(lista == null){
			return null;
		}
		
		List<PolizaDummyDTO> listaPolizas = new ArrayList<PolizaDummyDTO>();		
		
		for(CotizacionDTO cotizacion : lista){
			listaPolizas.add((PolizaDummyDTO)cotizacion);
		}
		
		return listaPolizas;
	}
	
	private PolizaDummyDTO castPolizaCotizacion(CotizacionDTO cotizacionDTO){
		PolizaDummyDTO polizaDummyDTO = new PolizaDummyDTO();
//		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		polizaDummyDTO.setClaveEstatus(cotizacionDTO.getClaveEstatus());
//		polizaDummyDTO.setClaveOficina(cotizacionDTO.getClaveOficina());
		polizaDummyDTO.setCodigoUsuarioCotizacion(cotizacionDTO.getCodigoUsuarioCotizacion());
		polizaDummyDTO.setCodigoUsuarioCreacion(cotizacionDTO.getCodigoUsuarioCreacion());
		polizaDummyDTO.setCodigoUsuarioOrdenTrabajo(cotizacionDTO.getCodigoUsuarioOrdenTrabajo());
//		polizaDummyDTO.setDescripcionProducto(cotizacionDTO.getDescripcionProducto());
//		polizaDummyDTO.setDescripcionSubRamo(cotizacionDTO.getDescripcionSubRamo());
//				polizaDummyDTO.setDireccionAseguradoDTO(cotizacionDTO.getDireccionAseguradoDTO());
		polizaDummyDTO.setDireccionCobroDTO(cotizacionDTO.getDireccionCobroDTO());
//				polizaDummyDTO.setDireccionContratanteDTO(cotizacionDTO.getDireccionContratanteDTO());
		polizaDummyDTO.setFechaCreacion(cotizacionDTO.getFechaCreacion());
//		polizaDummyDTO.setFechaEmision(cotizacionDTO.getFechaEmision());
		polizaDummyDTO.setFechaFinVigencia(cotizacionDTO.getFechaFinVigencia());
		polizaDummyDTO.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
		polizaDummyDTO.setFechaModificacion(cotizacionDTO.getFechaModificacion());
//		polizaDummyDTO.setFechaPago(cotizacionDTO.getFechaPago());
//		polizaDummyDTO.setFormaPago(cotizacionDTO.getFormaPago());
		polizaDummyDTO.setIdFormaPago(cotizacionDTO.getIdFormaPago());
		polizaDummyDTO.setIdMedioPago(cotizacionDTO.getIdMedioPago());
		polizaDummyDTO.setIdMoneda(cotizacionDTO.getIdMoneda());
		polizaDummyDTO.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
		polizaDummyDTO.setIncisoCotizacionDTOs(cotizacionDTO.getIncisoCotizacionDTOs());
		polizaDummyDTO.setNombreEmpresaAsegurado(cotizacionDTO.getNombreEmpresaAsegurado());
		polizaDummyDTO.setNombreEmpresaContratante(cotizacionDTO.getNombreEmpresaContratante());
//		polizaDummyDTO.setNumeroPoliza(cotizacionDTO.getNumeroPoliza());
//				polizaDummyDTO.setPersonaAseguradoDTO(cotizacionDTO.getPersonaAseguradoDTO());
//				polizaDummyDTO.setPersonaContratanteDTO(cotizacionDTO.getPersonaContratanteDTO());
//		polizaDummyDTO.setSaldoPendiente(cotizacionDTO.getSaldoPendiente());
//		polizaDummyDTO.setSaldoVencido(cotizacionDTO.getSaldoVencido());
		polizaDummyDTO.setSolicitudDTO(cotizacionDTO.getSolicitudDTO());
//		polizaDummyDTO.setTipoNegocio(cotizacionDTO.getTipoNegocio());
		polizaDummyDTO.setTipoPolizaDTO(cotizacionDTO.getTipoPolizaDTO());
//		polizaDummyDTO.setUltimoReciboPagado(cotizacionDTO.getUltimoReciboPagado());
		
		
		return polizaDummyDTO;
	}

}
