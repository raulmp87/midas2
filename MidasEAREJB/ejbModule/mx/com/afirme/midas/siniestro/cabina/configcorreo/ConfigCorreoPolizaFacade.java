/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;


/**
 * @author smvr
 *
 */
@Stateless
public class ConfigCorreoPolizaFacade implements ConfigCorreoPolizaFacadeRemote{

	@PersistenceContext
	private EntityManager entityManager;
	public static final String IDCONFIGPOLIZA = "idConfigPoliza";
	
	
	public List<ConfigCorreoPolizaDTO> findAll() {
		
		LogDeMidasEJB3.log( "finding all ConfigCorreoPolizaDTO instances", Level.INFO, null);
		List<ConfigCorreoPolizaDTO> list;
		try {
			
			final String queryString = "select model from ConfigCorreoPolizaDTO model " +
					"order by model.codigoProducto, model.codigoTipoPoliza, model.numeroPoliza, model.numeroRenovacion";
			Query query = entityManager.createQuery(queryString);
			query.setHint( QueryHints.REFRESH, HintValues.TRUE );
			list = query.getResultList();
			return list;
			
		} catch ( RuntimeException re ) {
			LogDeMidasEJB3.log( "find all failed" + re.getMessage(), Level.SEVERE, null );
			throw re;
		}
	}
	
	public List<ConfigCorreoPolizaDTO> listarFiltrado( String numeroPoliza ) {
		
		LogDeMidasEJB3.log( "finding all ConfigCorreoPolizaDTO instances", Level.INFO, null);
		List<ConfigCorreoPolizaDTO> list = null;
		try {
			Query query;
			String queryString = "select model from ConfigCorreoPolizaDTO model ";
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			String sWhere = "";
			if( numeroPoliza !=null && numeroPoliza.length()>0 ){			
				
				String[] numeroDePolizaArray = numeroPoliza.split( "-" );
		        String codigoProducto = numeroDePolizaArray[0].substring( 0, 2 );
		        String codigoTipoPoliza = numeroDePolizaArray[0].substring( 2, 4 );
		        Integer numPoliza = new Integer( numeroDePolizaArray[1] );
		        Integer numeroRenovacion = new Integer( numeroDePolizaArray[2] );
				
			
				sWhere = Utilerias.agregaParametroQuery( listaParametrosValidos,sWhere, "codigoProducto",String.format("%1$-8s", codigoProducto) );
				sWhere = Utilerias.agregaParametroQuery( listaParametrosValidos,sWhere, "codigoTipoPoliza",String.format("%1$-8s", codigoTipoPoliza) );
				sWhere = Utilerias.agregaParametroQuery( listaParametrosValidos,sWhere, "numeroPoliza",numPoliza );
				sWhere = Utilerias.agregaParametroQuery( listaParametrosValidos,sWhere, "numeroRenovacion",numeroRenovacion );
				
				
				if ( Utilerias.esAtributoQueryValido(sWhere) ){
					queryString = queryString.concat( " where " ).concat( sWhere );
				}
				
				queryString = queryString.concat( " order by model.fechaModificacion desc" );
				
				query = entityManager.createQuery( queryString );
				Utilerias.estableceParametrosQuery( query, listaParametrosValidos );
				query.setHint( QueryHints.REFRESH, HintValues.TRUE );
				
				list = query.getResultList();
				
			} 
		}catch ( RuntimeException re ) {
			LogDeMidasEJB3.log( "find all failed" + re.getMessage(), Level.SEVERE, null );
			throw re;
		}
		return list;
	}
	
	public void save( ConfigCorreoPolizaDTO entity ) {
		
		LogDeMidasEJB3.log( "saving ConfigCorreoPolizaDTO instance", Level.INFO, null);
		try {
			
			entityManager.persist( entity );
			LogDeMidasEJB3.log( "save successful" , Level.INFO, null);
			
		} catch ( RuntimeException re ) {
			LogDeMidasEJB3.log( "save failed" + re.getMessage(), Level.SEVERE, null );
			throw re;
		}
	}
	
	public ConfigCorreoPolizaDTO findById( BigDecimal idConfigPoliza ) {
		
		LogDeMidasEJB3.log( "finding ConfigCorreoTipoNegociooDTO instance with id: " + idConfigPoliza , Level.INFO, null);
		try {
			
			ConfigCorreoPolizaDTO instance = entityManager.find( ConfigCorreoPolizaDTO.class, idConfigPoliza );
			return instance;
			
		} catch ( RuntimeException re ) {
			LogDeMidasEJB3.log( "find failed" + re.getMessage(), Level.SEVERE, null );
			throw re;
		}
	}
	
	public ConfigCorreoPolizaDTO update( ConfigCorreoPolizaDTO entity ) {
		
			LogDeMidasEJB3.log( "updating ConfigCorreoPolizaDTO instance" , Level.INFO, null);
	        try {
	        	
	        	ConfigCorreoPolizaDTO result = entityManager.merge( entity );
            	LogDeMidasEJB3.log( "update successful" , Level.INFO, null);
	            return result;
	            
        } catch ( RuntimeException re ) {
        		LogDeMidasEJB3.log( "update failed" + re.getMessage(), Level.SEVERE, null );
	            throw re;
        }
	}
	
	public void delete( ConfigCorreoPolizaDTO entity ) {
		
		LogDeMidasEJB3.log( "deleting ConfigCorreoPolizaDTO instance" , Level.INFO, null);
        try {
        	
	    	entity = entityManager.getReference( ConfigCorreoPolizaDTO.class, entity.getIdConfigPoliza() );
	        entityManager.remove( entity );
	        LogDeMidasEJB3.log( "delete successful" , Level.INFO, null);
	        
        } catch ( RuntimeException re ) {
        		LogDeMidasEJB3.log( "delete failed" + re.getMessage(), Level.SEVERE, null );
            throw re;
        }
	}
}
