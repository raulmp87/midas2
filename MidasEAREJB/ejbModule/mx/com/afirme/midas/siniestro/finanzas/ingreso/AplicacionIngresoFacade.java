package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity Toaplicacioningreso.
 * 
 * @see mx.com.afirme.midas.catalogos.codigo.Toaplicacioningreso
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class AplicacionIngresoFacade implements
		AplicacionIngresoFacadeRemote {
	// property constants
	public static final String OBSERVACIONAPLICACION = "observacionaplicacion";
	public static final String REFERENCIASAPLICACION = "referenciasaplicacion";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved AplicacionIngresoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            AplicacionIngresoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public AplicacionIngresoDTO save(AplicacionIngresoDTO entity) {
		LogUtil.log("saving AplicacionIngresoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
		return entity;
	}

	/**
	 * Delete a persistent AplicacionIngresoDTO entity.
	 * 
	 * @param entity
	 *            AplicacionIngresoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(AplicacionIngresoDTO entity) {
		LogUtil.log("deleting AplicacionIngresoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(AplicacionIngresoDTO.class,
					entity.getIdToAplicacionIngreso());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved AplicacionIngresoDTO entity and return it or a
	 * copy of it to the sender. A copy of the AplicacionIngresoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            AplicacionIngresoDTO entity to update
	 * @return AplicacionIngresoDTO the persisted AplicacionIngresoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public AplicacionIngresoDTO update(AplicacionIngresoDTO entity) {
		LogUtil.log("updating AplicacionIngresoDTO instance", Level.INFO, null);
		try {
			AplicacionIngresoDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public AplicacionIngresoDTO findById(BigDecimal id) {
		LogUtil.log("finding AplicacionIngresoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			AplicacionIngresoDTO instance = entityManager.find(
					AplicacionIngresoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all AplicacionIngresoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the AplicacionIngresoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<AplicacionIngresoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<AplicacionIngresoDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding AplicacionIngresoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from AplicacionIngresoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<AplicacionIngresoDTO> findByObservacionaplicacion(Object observacionaplicacion) {
		return findByProperty(OBSERVACIONAPLICACION, observacionaplicacion);
	}

	public List<AplicacionIngresoDTO> findByReferenciasaplicacion(Object referenciasaplicacion) {
		return findByProperty(REFERENCIASAPLICACION, referenciasaplicacion);
	}

	/**
	 * Find all Toaplicacioningreso entities.
	 * 
	 * @return List<Toaplicacioningreso> all AplicacionIngresoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<AplicacionIngresoDTO> findAll() {
		LogUtil.log("finding all Toaplicacioningreso instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from AplicacionIngresoDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	public AplicacionIngresoDTO buscarAplicarIngresoPorAT(BigDecimal idToAutorizacionTecnica,Short estatus){
		LogDeMidasEJB3.log("buscarAplicarIngreso instance", Level.INFO, null);
		try {
			StringBuilder sb = new StringBuilder();		
			sb.append("select  ");
			sb.append("		model ");
			sb.append("from ");
			sb.append("		AplicacionIngresoDTO model ");
			sb.append("where ");
			sb.append("		model.autorizacionTecnicaDTO.idToAutorizacionTecnica = :idToAutorizacionTecnica ");
			sb.append("		and model.estatus <> :estatus ");
			
			final String queryString = sb.toString();
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToAutorizacionTecnica", idToAutorizacionTecnica);
			query.setParameter("estatus", estatus);
			
			Object obj = query.getSingleResult();
			if(obj != null ){
				return (AplicacionIngresoDTO)obj;
			}else{
				return null;
			}
			
		}catch(NoResultException nre){
			return null;
		}catch (NonUniqueResultException nure) {
			return null;
		}catch (RuntimeException re) {
			LogDeMidasEJB3.log("buscarAplicarIngreso failed", Level.SEVERE, re);
			throw re;
		}	
	}
	
	@SuppressWarnings("unchecked")
	public List<AplicacionIngresoDTO> buscarAplicarIngresoPorEstatus(Short estatus){
		LogDeMidasEJB3.log("buscarAplicarIngresoPorEstatus instance", Level.INFO, null);
		try {
			StringBuilder sb = new StringBuilder();		
			sb.append("select  ");
			sb.append("		model ");
			sb.append("from ");
			sb.append("		AplicacionIngresoDTO model ");
			sb.append("where ");
			sb.append("		model.estatus = :estatus ");
			
			final String queryString = sb.toString();
			Query query = entityManager.createQuery(queryString);
			query.setParameter("estatus", estatus);
			
			return query.getResultList();
			
		}catch (RuntimeException re) {
			LogDeMidasEJB3.log("buscarAplicarIngresoPorEstatus failed", Level.SEVERE, re);
			throw re;
		}	
	}
	
	@SuppressWarnings("unchecked")
	public List<AplicacionIngresoDTO> buscarAplicarIngresoPendientes(){
		LogDeMidasEJB3.log("buscarAplicarIngresoPendientes instance", Level.INFO, null);
		try {
			
			StringBuilder sb = new StringBuilder();		
			
			sb.append("select ");
			sb.append("  distinct ai.* ");
			sb.append("from ");
			sb.append("  MIDAS.toaplicacioningreso ai join MIDAS.tocontabilidadremesa cr ");
			sb.append("    on ai.idtoaplicacioningreso =  cr.idtoaplicacioningreso ");		
			sb.append("where ");	
			sb.append("    cr.idpolizacontable is null ");		
			
			LogDeMidasEJB3.log(sb.toString(), Level.WARNING, null);			
			final String queryString = sb.toString();						
			
			Query query = entityManager.createNativeQuery(queryString, AplicacionIngresoDTO.class);
			List lista = query.getResultList();
			return lista;		
			
		}catch (RuntimeException re) {
			LogDeMidasEJB3.log("buscarAplicarIngresoPendientes failed", Level.SEVERE, re);
			throw re;
		}	
	}

}