package mx.com.afirme.midas.siniestro.finanzas.conveniofiniquito;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity ConvenioFiniquitoDTO.
 * 
 * @see .ConvenioFiniquitoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ConvenioFiniquitoFacade implements ConvenioFiniquitoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved ConvenioFiniquitoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ConvenioFiniquitoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public ConvenioFiniquitoDTO save(ConvenioFiniquitoDTO entity) {
		LogDeMidasEJB3.log("saving ConvenioFiniquitoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ConvenioFiniquitoDTO entity.
	 * 
	 * @param entity
	 *            ConvenioFiniquitoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ConvenioFiniquitoDTO entity) {
		LogDeMidasEJB3.log("deleting ConvenioFiniquitoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(ConvenioFiniquitoDTO.class, entity
					.getIdToConvenioFiniquito());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ConvenioFiniquitoDTO entity and return it or a copy
	 * of it to the sender. A copy of the ConvenioFiniquitoDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            ConvenioFiniquitoDTO entity to update
	 * @return ConvenioFiniquitoDTO the persisted ConvenioFiniquitoDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ConvenioFiniquitoDTO update(ConvenioFiniquitoDTO entity) {
		try {
			ConvenioFiniquitoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ConvenioFiniquitoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding ConvenioFiniquitoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			ConvenioFiniquitoDTO instance = entityManager
					.find(ConvenioFiniquitoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ConvenioFiniquitoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ConvenioFiniquitoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ConvenioFiniquitoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ConvenioFiniquitoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding ConvenioFiniquitoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ConvenioFiniquitoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ConvenioFiniquitoDTO entities.
	 * 
	 * @return List<ConvenioFiniquitoDTO> all ConvenioFiniquitoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConvenioFiniquitoDTO> findAll() {
		LogDeMidasEJB3.log("finding all ConvenioFiniquitoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from ConvenioFiniquitoDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	public ConvenioFiniquitoDTO obtenerConvenioCancelar(BigDecimal idToReporteSiniestro, BigDecimal idToIndemnizacion,Short estatus) {
		LogDeMidasEJB3.log("finding all ConvenioFiniquitoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from ConvenioFiniquitoDTO model where reporteSiniestro.idToReporteSiniestro=:idToReporteSiniestro and " +
					" and indemnizacion.idToIndemnizacion= :idToIndemnizacion" +
					" and estatus=:estatus";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			query.setParameter("idToIndemnizacion", idToIndemnizacion);
			query.setParameter("estatus", estatus);
			return (ConvenioFiniquitoDTO)query.getSingleResult();
		} catch (NoResultException nre){
			return null;
		} catch (NonUniqueResultException nure){
			return null;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}