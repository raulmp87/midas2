package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity ConceptoGasto.
 * 
 * @see .ConceptoGasto
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ConceptoGastoFacade implements ConceptoGastoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved ConceptoGasto entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ConceptoGasto entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ConceptoGastoDTO entity) {
		LogDeMidasEJB3.log("saving ConceptoGastoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ConceptoGasto entity.
	 * 
	 * @param entity
	 *            ConceptoGasto entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ConceptoGastoDTO entity) {
		LogDeMidasEJB3.log("deleting ConceptoGastoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(ConceptoGastoDTO.class, entity
					.getIdTcConceptoGasto());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ConceptoGasto entity and return it or a copy
	 * of it to the sender. A copy of the ConceptoGasto entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            ConceptoGasto entity to update
	 * @return ConceptoGasto the persisted ConceptoGasto entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ConceptoGastoDTO update(ConceptoGastoDTO entity) {
		LogDeMidasEJB3.log("updating ConceptoGastoDTO instance", Level.INFO, null);
		try {
			ConceptoGastoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ConceptoGastoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding ConceptoGastoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			ConceptoGastoDTO instance = entityManager
					.find(ConceptoGastoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ConceptoGasto entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ConceptoGasto property to query
	 * @param value
	 *            the property value to match
	 * @return List<ConceptoGasto> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ConceptoGastoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding ConceptoGastoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ConceptoGastoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ConceptoGasto entities.
	 * 
	 * @return List<ConceptoGasto> all ConceptoGasto entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConceptoGastoDTO> findAll() {
		LogDeMidasEJB3.log("finding all ConceptoGastoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from ConceptoGastoDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}