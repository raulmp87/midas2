package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
/**
 * Facade for entity PrestadorServicios.
 * 
 * @see .PrestadorServicios
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class PrestadorServiciosFacade implements PrestadorServiciosFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved PrestadorServicios
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            PrestadorServicios entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(PrestadorServiciosDTO entity) {
		LogDeMidasEJB3.log("saving PrestadorServiciosDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent PrestadorServicios entity.
	 * 
	 * @param entity
	 *            PrestadorServicios entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(PrestadorServiciosDTO entity) {
		LogDeMidasEJB3.log("deleting PrestadorServiciosDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(PrestadorServiciosDTO.class,
					entity.getIdTcPrestadorServicios());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved PrestadorServicios entity and return it or a
	 * copy of it to the sender. A copy of the PrestadorServicios entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            PrestadorServicios entity to update
	 * @return PrestadorServicios the persisted PrestadorServicios entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public PrestadorServiciosDTO update(PrestadorServiciosDTO entity) {
		LogDeMidasEJB3.log("updating PrestadorServiciosDTO instance", Level.INFO, null);
		try {
			PrestadorServiciosDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public PrestadorServiciosDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding PrestadorServiciosDTO instance with id: " + id,
				Level.INFO, null);
		try {
			PrestadorServiciosDTO instance = entityManager.find(
					PrestadorServiciosDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all PrestadorServicios entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the PrestadorServicios property to query
	 * @param value
	 *            the property value to match
	 * @return List<PrestadorServicios> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<PrestadorServiciosDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding PrestadorServiciosDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from PrestadorServiciosDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all PrestadorServicios entities.
	 * 
	 * @return List<PrestadorServicios> all PrestadorServicios entities
	 */
	@SuppressWarnings("unchecked")
	public List<PrestadorServiciosDTO> findAll() {
		LogDeMidasEJB3.log("finding all PrestadorServiciosDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from PrestadorServiciosDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}