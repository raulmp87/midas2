package mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica;
// default package

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDTO;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosInterfazFacadeRemote;
import mx.com.afirme.midas.siniestro.finanzas.pagos.SoportePagosDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity AutorizacionTecnica.
 * @see .AutorizacionTecnica
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class AutorizacionTecnicaFacade  implements AutorizacionTecnicaFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
    @EJB
    private PrestadorServiciosInterfazFacadeRemote prestadorServicios;
	
		/**
	 Perform an initial save of a previously unsaved AutorizacionTecnica entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AutorizacionTecnica entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AutorizacionTecnicaDTO entity) {
    	LogDeMidasEJB3.log("saving AutorizacionTecnica instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
	        	LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent AutorizacionTecnica entity.
	  @param entity AutorizacionTecnica entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AutorizacionTecnicaDTO entity) {
    	LogDeMidasEJB3.log("deleting AutorizacionTecnica instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(AutorizacionTecnicaDTO.class, entity.getIdToAutorizacionTecnica());
            entityManager.remove(entity);
            LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
	        	LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved AutorizacionTecnica entity and return it or a copy of it to the sender. 
	 A copy of the AutorizacionTecnica entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AutorizacionTecnica entity to update
	 @return AutorizacionTecnica the persisted AutorizacionTecnica entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public AutorizacionTecnicaDTO update(AutorizacionTecnicaDTO entity) {
    	LogDeMidasEJB3.log("updating AutorizacionTecnica instance", Level.INFO, null);
	        try {
            AutorizacionTecnicaDTO result = entityManager.merge(entity);
            LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public AutorizacionTecnicaDTO findById( BigDecimal id) {
    	LogDeMidasEJB3.log("finding AutorizacionTecnica instance with id: " + id, Level.INFO, null);
	        try {
            AutorizacionTecnicaDTO instance = entityManager.find(AutorizacionTecnicaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all AutorizacionTecnica entities with a specific property value.  
	 
	  @param propertyName the name of the AutorizacionTecnica property to query
	  @param value the property value to match
	  	  @return List<AutorizacionTecnica> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<AutorizacionTecnicaDTO> findByProperty(String propertyName, final Object value) {
    	LogDeMidasEJB3.log("finding AutorizacionTecnica instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from AutorizacionTecnicaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all AutorizacionTecnica entities.
	  	  @return List<AutorizacionTecnica> all AutorizacionTecnica entities
	 */
	@SuppressWarnings("unchecked")
	public List<AutorizacionTecnicaDTO> findAll(
		) {
		LogDeMidasEJB3.log("finding all AutorizacionTecnica instances", Level.INFO, null);
			try {
			final String queryString = "select model from AutorizacionTecnicaDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<AutorizacionTecnicaDTO> listarAutorizacionesPorEstatus(BigDecimal idToReporteSiniestro, short estatus){
		LogDeMidasEJB3.log("finding all listarAutorizacionesPorAutorizar instances", Level.INFO, null);
		try {
			final String queryString = 
				"select model from AutorizacionTecnicaDTO model where model.reporteSiniestroDTO.idToReporteSiniestro = :idtoreportesiniestro and model.estatus = :estatus";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idToReporteSiniestro);
			query.setParameter("estatus", estatus);
			
			return query.getResultList();
		} catch (RuntimeException re) {
		LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<AutorizacionTecnicaDTO> listarAutorizacionesPorCancelar(BigDecimal idToReporteSiniestro){
		LogDeMidasEJB3.log("finding all listarAutorizacionesPorCancelar instances", Level.INFO, null);
		try {
			final String queryString = 
				"select model from AutorizacionTecnicaDTO model where model.reporteSiniestroDTO.idToReporteSiniestro = :idtoreportesiniestro and model.estatus <> :estatus";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idToReporteSiniestro);
			query.setParameter("estatus", AutorizacionTecnicaDTO.ESTATUS_CANCELADA);
			
			return query.getResultList();
		} catch (RuntimeException re) {
		LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}		
	}	
	
	public AutorizacionTecnicaDTO obtenerAutorizacionIndemnizacion(BigDecimal idToReporteSiniestro,BigDecimal idToIndemnizacion){
		LogDeMidasEJB3.log("finding obtenerAutorizacionIndemnizacion instances", Level.INFO, null);
		try {
			final String queryString = 
				"select model from AutorizacionTecnicaDTO model where model.reporteSiniestroDTO.idToReporteSiniestro = :idtoreportesiniestro and model.indemnizacionDTO.idToIndemnizacion = :idToIndemnizacion and model.estatus <> :cancelado";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idToReporteSiniestro);
			query.setParameter("idToIndemnizacion", idToIndemnizacion);
			query.setParameter("cancelado", AutorizacionTecnicaDTO.ESTATUS_CANCELADA);
			
			Object obj = query.getSingleResult();
			if(obj != null ){
				return (AutorizacionTecnicaDTO)obj;
			}else{
				return null;
			}
		}catch(NoResultException nre){
			return null;
		}catch (NonUniqueResultException nure) {
			return null;
		}catch (RuntimeException re) {
			LogDeMidasEJB3.log("find obtenerAutorizacionIndemnizacion failed", Level.SEVERE, re);
			throw re;
		}		
	}	
	
	
	public AutorizacionTecnicaDTO obtenerAutorizacionIngreso(BigDecimal idToReporteSiniestro,BigDecimal idToIngresoSiniestro){
		LogDeMidasEJB3.log("finding obtenerAutorizacionIngreso instances", Level.INFO, null);
		try {
			final String queryString = 
				"select model from AutorizacionTecnicaDTO model where model.reporteSiniestroDTO.idToReporteSiniestro = :idtoreportesiniestro and model.ingresoSiniestroDTO.idToIngresoSiniestro = :idToIngresoSiniestro and model.estatus <> :cancelado";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idToReporteSiniestro);
			query.setParameter("idToIngresoSiniestro", idToIngresoSiniestro);
			query.setParameter("cancelado", AutorizacionTecnicaDTO.ESTATUS_CANCELADA);
			
			Object obj = query.getSingleResult();
			if(obj != null ){
				return (AutorizacionTecnicaDTO)obj;
			}else{
				return null;
			}
		}catch(NoResultException nre){
			return null;
		}catch (NonUniqueResultException nure) {
			return null;
		}catch (RuntimeException re) {
		LogDeMidasEJB3.log("find obtenerAutorizacionIngreso failed", Level.SEVERE, re);
			throw re;
		}		
	}	
	
	
	public AutorizacionTecnicaDTO obtenerAutorizacionGasto(BigDecimal idToReporteSiniestro,BigDecimal idToGastoSiniestro){
		LogDeMidasEJB3.log("finding obtenerAutorizacionGasto instances", Level.INFO, null);
		try {
			final String queryString = 
				"select model from AutorizacionTecnicaDTO model where model.reporteSiniestroDTO.idToReporteSiniestro = :idtoreportesiniestro and model.gastoSiniestroDTO.idToGastoSiniestro = :idToGastoSiniestro and model.estatus <> :cancelado";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idToReporteSiniestro);
			query.setParameter("idToGastoSiniestro", idToGastoSiniestro);
			query.setParameter("cancelado", AutorizacionTecnicaDTO.ESTATUS_CANCELADA);
			
			Object obj = query.getSingleResult();
			if(obj != null ){
				return (AutorizacionTecnicaDTO)obj;
			}else{
				return null;
			}
		}catch(NoResultException nre){
			return null;
		}catch (NonUniqueResultException nure) {
			return null;
		}catch (RuntimeException re) {
		LogDeMidasEJB3.log("find obtenerAutorizacionGasto failed", Level.SEVERE, re);
			throw re;
		}		
	}	
	@SuppressWarnings("unchecked")
	public List<AutorizacionTecnicaDTO> listarAutorizacionesGastosFiltrado(
			SoportePagosDTO soportePagosDTO) {
		LogDeMidasEJB3.log("finding all listarAutorizaciones instances", Level.INFO, null);
		try {	
			String queryString = 
				"select model from AutorizacionTecnicaDTO model ";
			String sWhere = " model.estatus = 1 and model.gastoSiniestroDTO.idToGastoSiniestro is not null "+
							" and model.idToAutorizacionTecnica not in (select detalle.autorizacionTecnicaDTO.idToAutorizacionTecnica from DetalleOrdenPagoDTO detalle) ";
			Query query;
			List<AutorizacionTecnicaDTO> autorizaciones = null;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();						

			if (soportePagosDTO == null)
				return null;
			
			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getNumeroReporte()))
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "reporteSiniestroDTO.numeroReporte", soportePagosDTO.getNumeroReporte());

			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getNombreAsegurado()))
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "reporteSiniestroDTO.nombreAsegurado", soportePagosDTO.getNombreAsegurado());

			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getIdPrestadorServicios()))
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "gastoSiniestroDTO.idTcPrestadorServicios", soportePagosDTO.getIdPrestadorServicios());
			
			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getIdMoneda()))
				sWhere += " and model.reporteSiniestroDTO.numeroPoliza in (Select poliza.idToPoliza from PolizaDTO poliza where poliza.cotizacionDTO.idMoneda = "+soportePagosDTO.getIdMoneda()+")";

			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getIdsAutorizaciones())){
				sWhere += " and model.idToAutorizacionTecnica in ("+soportePagosDTO.getIdsAutorizaciones()+") ";
			}

			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getFechaDesdeString()) && Utilerias.esAtributoQueryValido(soportePagosDTO.getFechaHastaString())){
				sWhere += " and model.fechaAutorizacion BETWEEN :desde AND :hasta order by model.fechaAutorizacion ";
			}
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getFechaDesdeString()) && Utilerias.esAtributoQueryValido(soportePagosDTO.getFechaHastaString())){
				try{
					DateFormat f = new SimpleDateFormat("dd/MM/yyyy");				
					GregorianCalendar gcFechaInicio = new GregorianCalendar();
					gcFechaInicio.setTime(f.parse(soportePagosDTO.getFechaDesdeString()));
					gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
					gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
					gcFechaInicio.set(GregorianCalendar.SECOND, 0);
					gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);				
					
					GregorianCalendar gcFechaFin = new GregorianCalendar();
					gcFechaFin.setTime(f.parse(soportePagosDTO.getFechaHastaString()));
					gcFechaFin.add(GregorianCalendar.DATE, 1);
					gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
	
					System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
					System.out.println("Poliza: Rango de Fecha fin ->" + gcFechaFin.getTime());
					
					query.setParameter("desde", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
					query.setParameter("hasta", gcFechaFin.getTime(), TemporalType.TIMESTAMP);
				}catch (ParseException p){return null;}
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			autorizaciones = query.getResultList();
			if(autorizaciones != null){
				for(AutorizacionTecnicaDTO autorizacion: autorizaciones){
					try {
						PrestadorServiciosDTO prestador = prestadorServicios.detallePrestador(autorizacion.getGastoSiniestroDTO().getIdTcPrestadorServicios(), autorizacion.getCodigoUsuarioCreacion());
						if(prestador != null)
							autorizacion.getGastoSiniestroDTO().setNombrePrestadorServicios(prestador.getNombrePrestador());
					} catch (Exception e) {
					}
				}
			}
			return autorizaciones; 		
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}		
	}

}