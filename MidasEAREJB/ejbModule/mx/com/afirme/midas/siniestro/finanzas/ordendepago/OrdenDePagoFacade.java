package mx.com.afirme.midas.siniestro.finanzas.ordendepago;
// default package

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDTO;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosInterfazFacadeRemote;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.pagos.SoportePagosDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity OrdenDePagoDTO.
 * 
 * @see .OrdenDePagoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class OrdenDePagoFacade extends SoporteOrdenDePago implements OrdenDePagoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;
	@Resource
	private SessionContext context;	
	
    @EJB
    private PrestadorServiciosInterfazFacadeRemote prestadorServicios;	

	/**
	 * Perform an initial save of a previously unsaved OrdenDePagoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            OrdenDePagoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public OrdenDePagoDTO save(OrdenDePagoDTO entity) {
		LogUtil.log("saving OrdenDePagoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent OrdenDePagoDTO entity.
	 * 
	 * @param entity
	 *            OrdenDePagoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(OrdenDePagoDTO entity) {
		LogUtil.log("deleting OrdenDePagoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(OrdenDePagoDTO.class, entity
					.getIdToOrdenPago());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved OrdenDePagoDTO entity and return it or a copy of it
	 * to the sender. A copy of the OrdenDePagoDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            OrdenDePagoDTO entity to update
	 * @return OrdenDePagoDTO the persisted OrdenDePagoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public OrdenDePagoDTO update(OrdenDePagoDTO entity) {
		LogUtil.log("updating OrdenDePagoDTO instance", Level.INFO, null);
		try {
			OrdenDePagoDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public OrdenDePagoDTO findById(BigDecimal id) {
		LogUtil.log("finding OrdenDePagoDTO instance with id: " + id, Level.INFO,
				null);
		try {
			OrdenDePagoDTO instance = entityManager.find(OrdenDePagoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all OrdenDePagoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the OrdenDePagoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<OrdenDePagoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<OrdenDePagoDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding OrdenDePagoDTO instance with property: " + propertyName
				+ ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from OrdenDePagoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all OrdenDePagoDTO entities.
	 * 
	 * @return List<OrdenDePagoDTO> all OrdenDePagoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<OrdenDePagoDTO> findAll() {
		LogUtil.log("finding all OrdenDePagoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from OrdenDePagoDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find all OrdenDePagoDTO entities.
	 * 
	 * @return List<OrdenDePagoDTO> all OrdenDePagoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<OrdenDePagoDTO> obtenerOrdenesDePagoXReporteSiniestro(BigDecimal idToReporteSiniestro) {
		LogUtil.log("finding all OrdenDePagoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from OrdenDePagoDTO model where model.autorizacionTecnica.reporteSiniestroDTO.idToReporteSiniestro = :valorReporteSiniestro and model.autorizacionTecnica.ingresoSiniestroDTO is NULL";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("valorReporteSiniestro", idToReporteSiniestro);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Obtiene las ordenes de pago individuales, esto es las que no son ordenes
	 * de pago agrupadas
	 * 
	 * @return List<OrdenDePagoDTO> OrdenDePagoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<OrdenDePagoDTO> obtenerOrdenesDePagoXReporteSiniestroIndividual(BigDecimal idToReporteSiniestro) {
		LogUtil.log("finding all OrdenDePagoDTO instances", Level.INFO, null);
		try {
			final String queryString = " SELECT model "
					+ " FROM OrdenDePagoDTO model "
					+ " WHERE model.autorizacionTecnica.reporteSiniestroDTO.idToReporteSiniestro = :valorReporteSiniestro "
					+ " AND model.autorizacionTecnica.ingresoSiniestroDTO IS NULL ";			 
			Query query = entityManager.createQuery(queryString);
			query.setParameter("valorReporteSiniestro", idToReporteSiniestro);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public OrdenDePagoDTO obtenerOrdenPorIdAutorizacionTecnica(BigDecimal idAutorizacionTecnica){
		OrdenDePagoDTO ordenDePagoDTO = null;
		List<OrdenDePagoDTO> resultSet  = null;
		
		LogUtil.log("obtenerOrdenPorIdAutorizacionTecnica", Level.INFO, null);
		try {
			final String queryString = 
				"select model from OrdenDePagoDTO model where model.autorizacionTecnica.idToAutorizacionTecnica = :idAutorizacionTecnica";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idAutorizacionTecnica", idAutorizacionTecnica);
			resultSet = query.getResultList();
			
			if(resultSet != null && resultSet.size() > 0){
				ordenDePagoDTO = resultSet.get(0);
			}				
		}catch(NoResultException nre){
			LogUtil.log("No hay instancias del objeto OrdenDePagoDTO para la AT = " + idAutorizacionTecnica, Level.WARNING, null);
		}catch (NonUniqueResultException nure) {
			LogUtil.log("No hay instancias del objeto OrdenDePagoDTO para la AT = " + idAutorizacionTecnica, Level.WARNING, null);
		}catch (RuntimeException re) {
			LogUtil.log("obtenerOrdenPorIdAutorizacionTecnica failed", Level.SEVERE, re);
			throw re;
		}	
		
		return ordenDePagoDTO;
	}
	
//	@Override
//	public void actualizaOrdenCheque(BigDecimal idOrdenPago, String estatusSolicitud) throws Exception{
//		if(estatusSolicitud != null && idOrdenPago != null){
//			if(estatusSolicitud.equalsIgnoreCase("PROCESO")){
//				LogDeMidasEJB3.log("La solicitud esta en proceso : " + idOrdenPago.toString(), Level.INFO, null);
//			}else if(estatusSolicitud.equalsIgnoreCase("EN SOLICITUD MIZAR")){
//				LogDeMidasEJB3.log("La solicitud fue requerida a MIZAR : " + idOrdenPago.toString(), Level.INFO, null);
//			}else if(estatusSolicitud.equalsIgnoreCase("CANCELADO")){
//				OrdenDePagoDTO entity = null;
//				
//				try{
//					entity = findById(idOrdenPago);
//					if(entity!= null){
//						entity.setEstatus(OrdenDePagoDTO.ESTATUS_CANCELADA);
//						this.update(entity);
//						super.completarOrdenDePago(entity, estatusSolicitud);
//					}
//				}catch(RuntimeException ex){
//					LogDeMidasEJB3.log("Error en actualizaOrdenCheque, actualizar estatus" + estatusSolicitud, Level.WARNING, ex);
//				}				
//			}else if(estatusSolicitud.equalsIgnoreCase("TERMINADO")){
//				OrdenDePagoDTO entity = null;
//				
//				try{
//					entity = findById(idOrdenPago);
//					if(entity!= null){
//						entity.setEstatus(OrdenDePagoDTO.ESTATUS_PAGADA);
//						this.update(entity);
//						super.completarOrdenDePago(entity, estatusSolicitud);
//					}
//				}catch (Exception e) {
//					LogDeMidasEJB3.log(e.getMessage(), Level.WARNING, e);
//					throw e;
//				}								
//			}else{
//				LogDeMidasEJB3.log("Estatus de solicitud desconocido : " + estatusSolicitud, Level.INFO, null);
//			}
//		}		
//	}
	
	
	public OrdenDePagoDTO obtenerOrdenNoCanceladaPorIdAutorizacionTecnica(BigDecimal idAutorizacionTecnica){
		@SuppressWarnings("unused")
		OrdenDePagoDTO ordenDePagoDTO = null;
		@SuppressWarnings("unused")
		List<OrdenDePagoDTO> resultSet  = null;
		
		LogUtil.log("obtenerOrdenPorIdAutorizacionTecnica", Level.INFO, null);
		try {
			final String queryString = 
				"select model from OrdenDePagoDTO model where model.autorizacionTecnica.idToAutorizacionTecnica = :idAutorizacionTecnica and model.estatus <> :cancelada";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idAutorizacionTecnica", idAutorizacionTecnica);
			query.setParameter("cancelada", OrdenDePagoDTO.ESTATUS_CANCELADA);
			
			Object obj = query.getSingleResult();
			if(obj != null ){
				return (OrdenDePagoDTO)obj;
			}else{
				return null;
			}
		}catch(NoResultException nre){
			return null;
		}catch (NonUniqueResultException nure) {
			return null;
		}catch (RuntimeException re) {
		LogDeMidasEJB3.log("find obtenerAutorizacionIngreso failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public OrdenDePagoDTO buscaPrimerRegistro() {
		LogDeMidasEJB3.log("entrando a OrdenDePagoFacade.buscaPrimerRegistro", Level.INFO, null);
		try {
			final String queryString = "select model from OrdenDePagoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setFirstResult(0);
			query.setMaxResults(1);
						
			List<OrdenDePagoDTO> resultList = query.getResultList();
			
			if (resultList != null && resultList.size() > 0) {
				return resultList.get(0);
			}
			return null;
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("OrdenDePagoFacade.buscaPrimerRegistro fallo", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DetalleOrdenPagoDTO> listarOrdenesDePagoFiltrado(
			SoportePagosDTO soportePagosDTO) {
		LogDeMidasEJB3.log("finding all listarOrdenesDePago instances", Level.INFO, null);
		try {	
			String queryString = 
				"select model from DetalleOrdenPagoDTO model ";
			String sWhere = " model.autorizacionTecnicaDTO.gastoSiniestroDTO.idToGastoSiniestro is not null ";
			Query query;
			List<DetalleOrdenPagoDTO> ordenesDePago = null;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();						

			if (soportePagosDTO == null) {
				return null;
			}
			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getEstatus())){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "ordenDePagoDTO.estatus",Short.valueOf(soportePagosDTO.getEstatus()));
			}else{
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "ordenDePagoDTO.estatus",Short.valueOf("0"));
			}
				
			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getNumeroOrdenDePago())) {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "ordenDePagoDTO.idToOrdenPago",Utilerias.regresaBigDecimal(soportePagosDTO.getNumeroOrdenDePago()));
			}
				
			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getNumeroReporte())) {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "autorizacionTecnicaDTO.reporteSiniestroDTO.numeroReporte", soportePagosDTO.getNumeroReporte());
			}

			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getNombreAsegurado())) {
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "autorizacionTecnicaDTO.reporteSiniestroDTO.nombreAsegurado", soportePagosDTO.getNombreAsegurado());
			}

			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getIdPrestadorServicios())) {
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "autorizacionTecnicaDTO.gastoSiniestroDTO.idTcPrestadorServicios", soportePagosDTO.getIdPrestadorServicios());
			}
			
			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getIdMoneda())) {
				sWhere += " and model.autorizacionTecnicaDTO.reporteSiniestroDTO.numeroPoliza in (Select poliza.idToPoliza from PolizaDTO poliza where poliza.cotizacionDTO.idMoneda = "+soportePagosDTO.getIdMoneda()+")";
			}

			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getFechaDesdeString()) && Utilerias.esAtributoQueryValido(soportePagosDTO.getFechaHastaString())) {
				sWhere += " and model.ordenDePagoDTO.fechaPago BETWEEN :desde AND :hasta order by model.ordenDePagoDTO.fechaPago";
			}
			

			if (Utilerias.esAtributoQueryValido(sWhere)) {
				queryString = queryString.concat(" where ").concat(sWhere);
			}
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if(Utilerias.esAtributoQueryValido(soportePagosDTO.getFechaDesdeString()) && Utilerias.esAtributoQueryValido(soportePagosDTO.getFechaHastaString())){
				try{
					DateFormat f = new SimpleDateFormat("dd/MM/yyyy");				
					GregorianCalendar gcFechaInicio = new GregorianCalendar();
					gcFechaInicio.setTime(f.parse(soportePagosDTO.getFechaDesdeString()));
					gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
					gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
					gcFechaInicio.set(GregorianCalendar.SECOND, 0);
					gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);				
					
					GregorianCalendar gcFechaFin = new GregorianCalendar();
					gcFechaFin.setTime(f.parse(soportePagosDTO.getFechaHastaString()));
					gcFechaFin.add(GregorianCalendar.DATE, 1);
					gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
	
					System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
					System.out.println("Poliza: Rango de Fecha fin ->" + gcFechaFin.getTime());
					
					query.setParameter("desde", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
					query.setParameter("hasta", gcFechaFin.getTime(), TemporalType.TIMESTAMP);
				}catch (ParseException p){return null;}
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			ordenesDePago = query.getResultList();
			if(ordenesDePago != null){
				for(DetalleOrdenPagoDTO ordenDePago: ordenesDePago){
					try {
						PrestadorServiciosDTO prestador = prestadorServicios.detallePrestador(ordenDePago.getAutorizacionTecnicaDTO().getGastoSiniestroDTO().getIdTcPrestadorServicios(), ordenDePago.getAutorizacionTecnicaDTO().getCodigoUsuarioCreacion());
						if(prestador != null) {
							ordenDePago.getAutorizacionTecnicaDTO().getGastoSiniestroDTO().setNombrePrestadorServicios(prestador.getNombrePrestador());
						}
					} catch (Exception e) {
					}
				}
			}
			return ordenesDePago; 		
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}	
	}

	/**
	 * Permite genererar una orden de pago apartir de 1 o n Autorizaciones Tecnicas
	 * 
	 * @param usuario
	 *            String clave de usuario que solicita la orden de pago
	 * @param autorizaciones
	 *            BigDecimal[] autorizaciones que se desean agrupar
	 *            en una orden de pago
	 * @return mapa
	 * 			  Map<String, String> mapa con la informacion generada del resultado de
	 * 			  la operacion solicitada las llaves para acceder al mapa son:
	 * 			  idToOrdenDePago: El ID generado por la orden de pago
	 * 			  mensaje: Mensaje del resultado de la transaccion           	              
	 * @throws RuntimeException
	 *             when the operation fails
	 */	
	public Map<String, String> generarOrdenDePago(String usuario,
			BigDecimal... autorizaciones) {
		Map<String, String> resultado = new HashMap<String, String>();
		try {
			if (autorizaciones != null && autorizaciones.length > 0) {
				List<AutorizacionTecnicaDTO> autorizacionesDTO = cargarAutorizaciones(autorizaciones);
				try{
					sonAutorizacionesValidas(autorizacionesDTO);

					OrdenDePagoDTO ordenDePagoDTO = new OrdenDePagoDTO();
					ordenDePagoDTO.setEstatus(OrdenDePagoDTO.ESTATUS_PENDIENTE);

					// contabilizarOrdenDePago
					//
						
				}catch (SystemException e){
					resultado.put("mensaje", "Error al generar la orden de pago: " + e.getCause());
					return resultado;
				}
				
			} else {
				resultado.put("mensaje",
							 "Error: Las autorizaciones tecnicas proporcionadas no son validas");
			}

		} catch (RuntimeException re) {	
			context.setRollbackOnly();
			LogDeMidasEJB3.log("generarOrdenDePago failed", Level.SEVERE, re);
			resultado.put("mensaje", "Error al generar el Endoso.");
			return resultado;
		} catch (Exception e){
			context.setRollbackOnly();
			LogDeMidasEJB3.log("generarOrdenDePago failed", Level.SEVERE, e);
			resultado.put("mensaje", "Error al generar el Endoso.");
			return resultado;
		}
		return resultado;
	}

	public List<DetalleOrdenPagoDTO> buscarDetallePorAutorizacionTecnica(
			BigDecimal idToAutorizacionTecnica) {
		LogUtil.log(
				"finding DetalleOrdendePagoDTO instance with property: idToAutorizacionTecnica"
						+ ", value: " + idToAutorizacionTecnica, Level.INFO,
				null);
		try {
			List<DetalleOrdenPagoDTO> detalleList = detalleOrdenDePago
					.findByProperty(
							"autorizacionTecnicaDTO.idToAutorizacionTecnica",
							idToAutorizacionTecnica);
			return detalleList;
		} catch (RuntimeException re) {
			LogUtil.log("find DetalleOrdendePagoDTO failed", Level.SEVERE, re);
			throw re;
		}
	}
	public void agregarDetalleOrdenDePago(DetalleOrdenPagoDTO detalle){
		LogUtil.log("saving DetalleOrdendePagoDTO instance ", Level.INFO,null);
		try {
			detalleOrdenDePago.save(detalle);
		} catch (RuntimeException re) {
			LogUtil.log("save DetalleOrdendePagoDTO instance failed", Level.SEVERE, re);
			throw re;
		}		
	}
}