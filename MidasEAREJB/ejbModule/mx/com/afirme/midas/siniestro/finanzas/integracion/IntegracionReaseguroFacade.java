package mx.com.afirme.midas.siniestro.finanzas.integracion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless
public class IntegracionReaseguroFacade implements	IntegracionReaseguroFacadeRemote {
	
	@PersistenceContext
	private EntityManager entityManager;

	public List<IntegracionReaseguroDTO> getReservaDetalleInicial(BigDecimal idToReservaEstimada){
		LogDeMidasEJB3.log("invocando distribuirReservaInicial ", Level.INFO, null);
		
		List<IntegracionReaseguroDTO> listaDetalle = new ArrayList<IntegracionReaseguroDTO>(); 

		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("  reservaDetalle.id.idtoreportesiniestro, ");
		sb.append("  reservaDetalle.id.idtocobertura, ");
		sb.append("  reservaDetalle.id.idtoseccion, ");
		sb.append("  reservaDetalle.id.numeroinciso, ");
		sb.append("  reservaDetalle.id.numerosubinciso, ");		
		sb.append("  sum(reservaDetalle.estimacion) ");
		sb.append("from ");
		sb.append("  ReservaDetalleDTO reservaDetalle ");
		sb.append("where ");
		sb.append("  reservaDetalle.id.idtoreservaestimada = :idtoreservaestimada ");
		sb.append("group by ");
		sb.append("  reservaDetalle.id.idtoreportesiniestro, ");
		sb.append("  reservaDetalle.id.idtocobertura, ");
		sb.append("  reservaDetalle.id.idtoseccion, ");
		sb.append("  reservaDetalle.id.numeroinciso, ");
		sb.append("  reservaDetalle.id.numerosubinciso ");				

		Query query = entityManager.createQuery(sb.toString());
		query.setParameter("idtoreservaestimada", idToReservaEstimada);
		List<?> resultSet = query.getResultList();
		
		for(Object record : resultSet){			
			Object[] detallesReserva = (Object[])record;
			
			IntegracionReaseguroDTO integracionReasesuroDTO = new IntegracionReaseguroDTO();
			
			integracionReasesuroDTO.setIdToReporteSiniestro((BigDecimal)detallesReserva[0]);
			integracionReasesuroDTO.setIdToCobertura((BigDecimal)detallesReserva[1]);					
			integracionReasesuroDTO.setIdToSeccion((BigDecimal)detallesReserva[2]);
			integracionReasesuroDTO.setNumeroInciso((BigDecimal)detallesReserva[3]);
			integracionReasesuroDTO.setNumeroSubInciso((BigDecimal)detallesReserva[4]);						
			integracionReasesuroDTO.setMontoSiniestro((Double)detallesReserva[5]);
			integracionReasesuroDTO.setIdMovimientoSiniestro(idToReservaEstimada);
			
			listaDetalle.add(integracionReasesuroDTO);
		}
					
		return listaDetalle;		
	}
	
	public List<IntegracionReaseguroDTO> getReservaDetalle(BigDecimal idToReservaEstimada){
		LogDeMidasEJB3.log("invocando distribuirReservaInicial ", Level.INFO, null);
		
		List<IntegracionReaseguroDTO> listaDetalle = new ArrayList<IntegracionReaseguroDTO>(); 

		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("  reservaDetalle.id.idtoreportesiniestro, ");
		sb.append("  reservaDetalle.id.idtocobertura, ");
		sb.append("  reservaDetalle.id.idtoseccion, ");
		sb.append("  reservaDetalle.id.numeroinciso, ");
		sb.append("  reservaDetalle.id.numerosubinciso, ");		
		sb.append("  sum(reservaDetalle.ajusteMas), ");
		sb.append("  sum(reservaDetalle.ajusteMenos) ");
		sb.append("from ");
		sb.append("  ReservaDetalleDTO reservaDetalle ");
		sb.append("where ");
		sb.append("  reservaDetalle.id.idtoreservaestimada = :idtoreservaestimada ");
		sb.append("group by ");
		sb.append("  reservaDetalle.id.idtoreportesiniestro, ");
		sb.append("  reservaDetalle.id.idtocobertura, ");
		sb.append("  reservaDetalle.id.idtoseccion, ");
		sb.append("  reservaDetalle.id.numeroinciso, ");
		sb.append("  reservaDetalle.id.numerosubinciso ");				

		Query query = entityManager.createQuery(sb.toString());
		query.setParameter("idtoreservaestimada", idToReservaEstimada);
		List<?> resultSet = query.getResultList();
		
		for(Object record : resultSet){			
			Object[] detallesReserva = (Object[])record;
			
			IntegracionReaseguroDTO integracionReasesuroDTO = new IntegracionReaseguroDTO();			
			integracionReasesuroDTO.setIdToReporteSiniestro((BigDecimal)detallesReserva[0]);
			integracionReasesuroDTO.setIdToCobertura((BigDecimal)detallesReserva[1]);
			integracionReasesuroDTO.setIdToSeccion((BigDecimal)detallesReserva[2]);
			integracionReasesuroDTO.setNumeroInciso((BigDecimal)detallesReserva[3]);
			integracionReasesuroDTO.setNumeroSubInciso((BigDecimal)detallesReserva[4]);										
			integracionReasesuroDTO.setAjusteDeMas((Double)detallesReserva[5]);
			integracionReasesuroDTO.setAjusteDeMenos((Double)detallesReserva[6]);
			
			double diferenciaAjustes = 
				integracionReasesuroDTO.getAjusteDeMas().doubleValue() - integracionReasesuroDTO.getAjusteDeMenos().doubleValue();  
			
			integracionReasesuroDTO.setMontoSiniestro(new Double(diferenciaAjustes));
			integracionReasesuroDTO.setIdMovimientoSiniestro(idToReservaEstimada);
			
			listaDetalle.add(integracionReasesuroDTO);					
		}
					
		return listaDetalle;		
	}
	
	public List<IntegracionReaseguroDTO> getDistribucionReaseguro(BigDecimal idToReporteSiniestro) {
		LogDeMidasEJB3.log("getDistribucionReaseguro ", Level.INFO, null);
		List<IntegracionReaseguroDTO> listaDetalle = new ArrayList<IntegracionReaseguroDTO>();
		
		Double totalSumaAsegurada = this.getTotalSumaAseguradaPorReporte(idToReporteSiniestro);
		
		StringBuilder sb = new StringBuilder();				
		sb.append("select ");
		sb.append("  riesgoAfectado.id.idtopoliza, ");
		sb.append("  riesgoAfectado.id.numeroinciso, ");
		sb.append("  riesgoAfectado.id.numerosubinciso, ");
		sb.append("	 riesgoAfectado.id.idtocobertura, ");
		sb.append("	 riesgoAfectado.id.idtoseccion, ");		
		sb.append("  sum(riesgoAfectado.sumaAsegurada) ");
		sb.append("from ");
		sb.append("  RiesgoAfectadoDTO riesgoAfectado ");
		sb.append("where ");
		sb.append("  riesgoAfectado.id.idtoreportesiniestro = :idtoreportesiniestro ");
		sb.append("group by         ");
		sb.append("  riesgoAfectado.id.idtopoliza, ");
		sb.append("  riesgoAfectado.id.numeroinciso, ");
		sb.append("  riesgoAfectado.id.numerosubinciso, ");
		sb.append("  riesgoAfectado.id.idtocobertura, ");
		sb.append("	 riesgoAfectado.id.idtoseccion ");
		
		try {
			final String queryString = sb.toString();
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idToReporteSiniestro);
									
			List<?> resultSet = query.getResultList();
			
			for(Object record : resultSet){			
				Object[] detallesReserva = (Object[])record;
				
				IntegracionReaseguroDTO integracionReasesuroDTO = new IntegracionReaseguroDTO();			
				integracionReasesuroDTO.setIdToReporteSiniestro(idToReporteSiniestro);
				integracionReasesuroDTO.setIdToPoliza((BigDecimal)detallesReserva[0]);
				integracionReasesuroDTO.setNumeroInciso((BigDecimal)detallesReserva[1]);
				integracionReasesuroDTO.setNumeroSubInciso((BigDecimal)detallesReserva[2]);				
				integracionReasesuroDTO.setIdToCobertura((BigDecimal)detallesReserva[3]);
				integracionReasesuroDTO.setIdToSeccion((BigDecimal)detallesReserva[4]);
				
				Double sumaAsegurada = (Double)detallesReserva[5];
				integracionReasesuroDTO.setMontoSiniestro(sumaAsegurada);
				
				double porcentajeSumaAsegurada = (sumaAsegurada.doubleValue() / totalSumaAsegurada.doubleValue());
												
				integracionReasesuroDTO.setPorcentajeSumaAsegurada(new Double(porcentajeSumaAsegurada));
				listaDetalle.add(integracionReasesuroDTO);					
			}
																						
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getDistribucionReaseguro failed", Level.SEVERE, re);
			throw re;
		}
		
		return listaDetalle;
	}

	public Double getTotalSumaAseguradaPorReporte(BigDecimal idToReporteSiniestro) {
		LogDeMidasEJB3.log("getTotalSumaAseguradaPorReporte Consulta", Level.INFO, null);
		
		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("  sum(riesgoAfectado.sumaAsegurada) ");
		sb.append("from ");
		sb.append("  RiesgoAfectadoDTO riesgoAfectado ");
		sb.append("where  ");
		sb.append("  riesgoAfectado.id.idtoreportesiniestro = :idtoreportesiniestro ");
		
		try {
			final String queryString = sb.toString();
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idToReporteSiniestro);
									
			return (Double) query.getSingleResult();			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getTotalSumaAseguradaPorReporte failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public List<IntegracionReaseguroDTO> getDistribucionDeIndemnizacion(BigDecimal idToIndemnizacion){
		LogDeMidasEJB3.log("invocando getDistribucionDeIndemnizacion ", Level.INFO, null);
		
		List<IntegracionReaseguroDTO> listaDetalle = new ArrayList<IntegracionReaseguroDTO>(); 

		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("  indemnizacion.id.idToReporteSiniestro, ");
		sb.append("  indemnizacion.id.idToCobertura, ");
		sb.append("  indemnizacion.id.idToPoliza, ");
		sb.append("  indemnizacion.id.idToSeccion, ");
		sb.append("  indemnizacion.id.numeroInciso, ");
		sb.append("  indemnizacion.id.numeroSubinciso, ");		
		sb.append("  sum(indemnizacion.montoPago), ");
		sb.append("  sum(indemnizacion.deducible), ");
		sb.append("  sum(indemnizacion.coaseguro) ");
		sb.append("from ");
		sb.append("  IndemnizacionRiesgoCoberturaDTO indemnizacion ");
		sb.append("where ");
		sb.append("  indemnizacion.id.idToIndemnizacion = :idtoindemnizacion ");
		sb.append("group by ");
		sb.append("  indemnizacion.id.idToReporteSiniestro, ");
		sb.append("  indemnizacion.id.idToCobertura, ");
		sb.append("  indemnizacion.id.idToPoliza, ");
		sb.append("  indemnizacion.id.idToSeccion, ");
		sb.append("  indemnizacion.id.numeroInciso, ");
		sb.append("  indemnizacion.id.numeroSubinciso ");		

		Query query = entityManager.createQuery(sb.toString());
		query.setParameter("idtoindemnizacion", idToIndemnizacion);
		List<?> resultSet = query.getResultList();
		
		for(Object record : resultSet){			
			Object[] detallesReserva = (Object[])record;
			IntegracionReaseguroDTO integracionReasesuroDTO = new IntegracionReaseguroDTO();			
			integracionReasesuroDTO.setIdToReporteSiniestro((BigDecimal)detallesReserva[0]);
			integracionReasesuroDTO.setIdToCobertura((BigDecimal)detallesReserva[1]);			
			integracionReasesuroDTO.setIdToPoliza((BigDecimal)detallesReserva[2]);
			integracionReasesuroDTO.setIdToSeccion((BigDecimal)detallesReserva[3]);
			integracionReasesuroDTO.setNumeroInciso((BigDecimal)detallesReserva[4]);
			integracionReasesuroDTO.setNumeroSubInciso((BigDecimal)detallesReserva[5]);										
			integracionReasesuroDTO.setMontoSiniestro((Double)detallesReserva[6] + (Double)detallesReserva[7] + (Double)detallesReserva[8]);						
			integracionReasesuroDTO.setIdMovimientoSiniestro(idToIndemnizacion);
			
			listaDetalle.add(integracionReasesuroDTO);					
		}
					
		return listaDetalle;		
	}	
	
	public List<IntegracionReaseguroDTO> getDistribucionDeDeducibleIndemnizacion(BigDecimal idToIndemnizacion){
		LogDeMidasEJB3.log("invocando getDistribucionDeDeducibleIndemnizacion ", Level.INFO, null);
		
		List<IntegracionReaseguroDTO> listaDetalle = new ArrayList<IntegracionReaseguroDTO>(); 

		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("  indemnizacion.id.idToReporteSiniestro, ");
		sb.append("  indemnizacion.id.idToCobertura, ");
		sb.append("  indemnizacion.id.idToPoliza, ");
		sb.append("  indemnizacion.id.idToSeccion, ");
		sb.append("  indemnizacion.id.numeroInciso, ");
		sb.append("  indemnizacion.id.numeroSubinciso, ");		
		sb.append("  sum(indemnizacion.deducible) ");
		sb.append("from ");
		sb.append("  IndemnizacionRiesgoCoberturaDTO indemnizacion ");
		sb.append("where ");
		sb.append("  indemnizacion.id.idToIndemnizacion = :idtoindemnizacion ");
		sb.append("group by ");
		sb.append("  indemnizacion.id.idToReporteSiniestro, ");
		sb.append("  indemnizacion.id.idToCobertura, ");
		sb.append("  indemnizacion.id.idToPoliza, ");
		sb.append("  indemnizacion.id.idToSeccion, ");
		sb.append("  indemnizacion.id.numeroInciso, ");
		sb.append("  indemnizacion.id.numeroSubinciso ");		
		
		try{
			Query query = entityManager.createQuery(sb.toString());
			query.setParameter("idtoindemnizacion", idToIndemnizacion);
			List<?> resultSet = query.getResultList();
			
			for(Object record : resultSet){			
				Object[] detallesReserva = (Object[])record;
				IntegracionReaseguroDTO integracionReasesuroDTO = new IntegracionReaseguroDTO();			
				integracionReasesuroDTO.setIdToReporteSiniestro((BigDecimal)detallesReserva[0]);
				integracionReasesuroDTO.setIdToCobertura((BigDecimal)detallesReserva[1]);			
				integracionReasesuroDTO.setIdToPoliza((BigDecimal)detallesReserva[2]);
				integracionReasesuroDTO.setIdToSeccion((BigDecimal)detallesReserva[3]);
				integracionReasesuroDTO.setNumeroInciso((BigDecimal)detallesReserva[4]);
				integracionReasesuroDTO.setNumeroSubInciso((BigDecimal)detallesReserva[5]);										
				integracionReasesuroDTO.setMontoSiniestro((Double)detallesReserva[6]);						
				integracionReasesuroDTO.setIdMovimientoSiniestro(idToIndemnizacion);
				
				listaDetalle.add(integracionReasesuroDTO);					
			}
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getDistribucionDeDeducibleIndemnizacion failed", Level.SEVERE, re);
			throw re;
		}
					
		return listaDetalle;		
	}	
	
	public List<IntegracionReaseguroDTO> getDistribucionDeCoaseguroIndemnizacion(BigDecimal idToIndemnizacion){
		LogDeMidasEJB3.log("invocando getDistribucionDeCoaseguroIndemnizacion ", Level.INFO, null);
		
		List<IntegracionReaseguroDTO> listaDetalle = new ArrayList<IntegracionReaseguroDTO>(); 

		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("  indemnizacion.id.idToReporteSiniestro, ");
		sb.append("  indemnizacion.id.idToCobertura, ");
		sb.append("  indemnizacion.id.idToPoliza, ");
		sb.append("  indemnizacion.id.idToSeccion, ");
		sb.append("  indemnizacion.id.numeroInciso, ");
		sb.append("  indemnizacion.id.numeroSubinciso, ");		
		sb.append("  sum(indemnizacion.coaseguro) ");
		sb.append("from ");
		sb.append("  IndemnizacionRiesgoCoberturaDTO indemnizacion ");
		sb.append("where ");
		sb.append("  indemnizacion.id.idToIndemnizacion = :idtoindemnizacion ");
		sb.append("group by ");
		sb.append("  indemnizacion.id.idToReporteSiniestro, ");
		sb.append("  indemnizacion.id.idToCobertura, ");
		sb.append("  indemnizacion.id.idToPoliza, ");
		sb.append("  indemnizacion.id.idToSeccion, ");
		sb.append("  indemnizacion.id.numeroInciso, ");
		sb.append("  indemnizacion.id.numeroSubinciso ");		
		
		try{
			Query query = entityManager.createQuery(sb.toString());
			query.setParameter("idtoindemnizacion", idToIndemnizacion);
			List<?> resultSet = query.getResultList();
			
			for(Object record : resultSet){			
				Object[] detallesReserva = (Object[])record;
				IntegracionReaseguroDTO integracionReasesuroDTO = new IntegracionReaseguroDTO();			
				integracionReasesuroDTO.setIdToReporteSiniestro((BigDecimal)detallesReserva[0]);
				integracionReasesuroDTO.setIdToCobertura((BigDecimal)detallesReserva[1]);			
				integracionReasesuroDTO.setIdToPoliza((BigDecimal)detallesReserva[2]);
				integracionReasesuroDTO.setIdToSeccion((BigDecimal)detallesReserva[3]);
				integracionReasesuroDTO.setNumeroInciso((BigDecimal)detallesReserva[4]);
				integracionReasesuroDTO.setNumeroSubInciso((BigDecimal)detallesReserva[5]);										
				integracionReasesuroDTO.setMontoSiniestro((Double)detallesReserva[6]);						
				integracionReasesuroDTO.setIdMovimientoSiniestro(idToIndemnizacion);
				
				listaDetalle.add(integracionReasesuroDTO);					
			}
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getDistribucionDeCoaseguroIndemnizacion failed", Level.SEVERE, re);
			throw re;
		}
					
		return listaDetalle;		
	}	
}
