package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;
// default package

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ContabilidadSiniestro.
 * @see .ContabilidadSiniestro
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ContabilidadSiniestroFacade  implements ContabilidadSiniestroFacadeRemote {
    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ContabilidadSiniestro entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ContabilidadSiniestro entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ContabilidadSiniestroDTO entity) {
    				LogDeMidasEJB3.log("saving ContabilidadSiniestro instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ContabilidadSiniestro entity.
	  @param entity ContabilidadSiniestro entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ContabilidadSiniestroDTO entity) {
    				LogDeMidasEJB3.log("deleting ContabilidadSiniestro instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ContabilidadSiniestroDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ContabilidadSiniestro entity and return it or a copy of it to the sender. 
	 A copy of the ContabilidadSiniestro entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ContabilidadSiniestro entity to update
	 @return ContabilidadSiniestro the persisted ContabilidadSiniestro entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ContabilidadSiniestroDTO update(ContabilidadSiniestroDTO entity) {
    				LogDeMidasEJB3.log("updating ContabilidadSiniestro instance", Level.INFO, null);
	        try {
            ContabilidadSiniestroDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ContabilidadSiniestroDTO findById( ContabilidadSiniestroId id) {
    				LogDeMidasEJB3.log("finding ContabilidadSiniestro instance with id: " + id, Level.INFO, null);
	        try {
            ContabilidadSiniestroDTO instance = entityManager.find(ContabilidadSiniestroDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    
    /**
	 * Find all ContabilidadSiniestro entities with a specific property value.  
	 
	  @param propertyName the name of the ContabilidadSiniestro property to query
	  @param value the property value to match
	  	  @return List<ContabilidadSiniestro> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ContabilidadSiniestroDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ContabilidadSiniestro instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ContabilidadSiniestroDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ContabilidadSiniestro entities.
	  	  @return List<ContabilidadSiniestro> all ContabilidadSiniestro entities
	 */
	@SuppressWarnings("unchecked")
	public List<ContabilidadSiniestroDTO> findAll() {
					LogDeMidasEJB3.log("finding all ContabilidadSiniestroDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ContabilidadSiniestroDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Devuelve una lista con los ids de los movimientos que aun no han sido registrados
	 * en la contabilidad de Seycos
	 * @return Una lista de ids de movimientos no registrados en la contabilidad de Seycos
	 */
	@SuppressWarnings("unchecked")
	public List<ContabilidadSiniestroId> buscarMovimientosNoContabilizados() {
		try {
			LogDeMidasEJB3.log("Buscando movimientos no registrados en la contabilidad de Seycos", Level.INFO, null);			
			StringBuffer sb = new StringBuffer();
			
			sb.append("Select distinct(model.id)");
			sb.append(" from ContabilidadSiniestroDTO model");
			sb.append(" where model.idPolizaContable is null");
			
			Query query = entityManager.createQuery(sb.toString());
			
			return query.getResultList();
			
		} catch (Exception re) {
			re.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Actualiza los movimientos de siniestro como contabilizados
	 * @param contabilidadSiniestroId Id del movimiento de siniestro cuyos registros se desean marcar como contabilizados
	 * @param estatusContabilizado Estatus de movimiento contabilizado
	 */
	public void actualizarMovimientosContabilizados(ContabilidadSiniestroId contabilidadSiniestroId, int estatusContabilizado) {
		
		try {
			LogDeMidasEJB3.log("Actualizando movimientos de siniestros", Level.INFO, null);			
			StringBuffer sb = new StringBuffer();
			
			sb.append("update ContabilidadSiniestroDTO model set model.idPolizaContable= :estatusContabilizado ");
			sb.append(" where model.id.idToReporteSiniestro = :idReporteSiniestro");
			sb.append(" and model.id.idTcMovimientoSiniestro = :idMovimientoSiniestro");
			sb.append(" and model.id.idRegistro = :idRegistro");
			
			Query query = entityManager.createQuery(sb.toString());
			query.setParameter("estatusContabilizado", estatusContabilizado);
			query.setParameter("idReporteSiniestro", contabilidadSiniestroId.getIdToReporteSiniestro());
			query.setParameter("idMovimientoSiniestro", contabilidadSiniestroId.getIdTcMovimientoSiniestro());
			query.setParameter("idRegistro", contabilidadSiniestroId.getIdRegistro());
			
			query.executeUpdate();
			LogDeMidasEJB3.log("Actualizo movimientos de siniestros", Level.INFO, null);
		} catch (Exception re) {
			re.printStackTrace();
		}
	}
	
}