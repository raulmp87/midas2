package mx.com.afirme.midas.siniestro.finanzas;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ReservaDetalleDTO.
 * 
 * @see .ReservaDetalleDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ReservaDetalleFacade implements ReservaDetalleFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved ReservaDetalleDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ReservaDetalleDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ReservaDetalleDTO entity) {
		LogDeMidasEJB3.log("saving ReservaDetalleDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ReservaDetalleDTO entity.
	 * 
	 * @param entity
	 *            ReservaDetalleDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ReservaDetalleDTO entity) {
		LogDeMidasEJB3.log("deleting ReservaDetalleDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(ReservaDetalleDTO.class, entity
					.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ReservaDetalleDTO entity and return it or a copy
	 * of it to the sender. A copy of the ReservaDetalleDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            ReservaDetalleDTO entity to update
	 * @return ReservaDetalleDTO the persisted ReservaDetalleDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ReservaDetalleDTO update(ReservaDetalleDTO entity) {
		LogDeMidasEJB3.log("updating ReservaDetalleDTO instance", Level.INFO, null);
		try {
			ReservaDetalleDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ReservaDetalleDTO findById(ReservaDetalleId id) {
		LogDeMidasEJB3.log("finding ReservaDetalleDTO instance with id: " + id,
				Level.INFO, null);
		try {
			ReservaDetalleDTO instance = entityManager.find(ReservaDetalleDTO.class,
					id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ReservaDetalleDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ReservaDetalleDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ReservaDetalleDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ReservaDetalleDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding ReservaDetalleDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ReservaDetalleDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ReservaDetalleDTO entities.
	 * 
	 * @return List<ReservaDetalleDTO> all ReservaDetalleDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReservaDetalleDTO> findAll() {
		LogDeMidasEJB3.log("finding all ReservaDetalleDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from ReservaDetalleDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ReservaDetalleDTO> listarDetalleReserva(BigDecimal idReserva) {
		LogDeMidasEJB3.log("listarDetalleReserva ReservaDetalleDTO instances", Level.INFO, null);
		try {
			final String queryString = 
				"select model from ReservaDetalleDTO model where model.id.idtoreservaestimada= :idReserva and model.estatus = 1 " +
				"order by model.id.numeroinciso, model.id.numerosubinciso, model.id.idtoseccion, model.id.idtocobertura, model.id.idtoriesgo ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idReserva", idReserva);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarDetalleReserva failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Double sumaReserva(BigDecimal idReserva) {
		LogDeMidasEJB3.log("sumaReserva Consulta", Level.INFO, null);
		try {
			final String queryString = "select SUM(model.estimacion) from ReservaDetalleDTO model where model.id.idtoreservaestimada= :idReserva and model.estimacion > 0 and model.estatus = 1";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idReserva", idReserva);
			List resultados = query.getResultList();
			Double valorSumaAseguradaReporte= new Double(0);
			
			for (Object resultado : resultados) {
				if(resultado != null){
					valorSumaAseguradaReporte = (Double) resultado;
				}
			}
			return valorSumaAseguradaReporte;
		}catch(NoResultException nre){
			return new Double(0);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("sumaReserva failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<ReservaDetalleDTO> listarReservaDetalle(BigDecimal idReserva) {
		LogDeMidasEJB3.log("listarDetalleReserva ReservaDetalleDTO instances", Level.INFO, null);
		try {
			final String queryString = 
				"select model from ReservaDetalleDTO model where model.id.idtoreservaestimada= :idReserva " +
				"order by model.id.numeroinciso, model.id.numerosubinciso, model.id.idtoseccion, model.id.idtocobertura, model.id.idtoriesgo ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idReserva", idReserva);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarDetalleReserva failed", Level.SEVERE, re);
			throw re;
		}
	}	
	
	public Double diferenciaDeReserva(BigDecimal idToReporteSiniestro) {
		LogDeMidasEJB3.log("diferenciaDeReserva instances", Level.INFO, null);
		try {
			Double diferencia = 0.0;
			StringBuilder buffer = new StringBuilder();
			buffer.append("select ");
			buffer.append("		sum(model.estimacion),sum(model.ajusteMas),sum(model.ajusteMenos) ");
			buffer.append("from ");
			buffer.append("		ReservaDetalleDTO model ");
			buffer.append("where  ");
			buffer.append("		model.id.idtoreportesiniestro= :idToReporteSiniestro ");
			buffer.append(" 	and model.estatus = :estatus ");
			
			Query query = entityManager.createQuery(buffer.toString());	
				
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			query.setParameter("estatus", ReservaDetalleDTO.ACTIVO);
			
			List<?> resultSet = query.getResultList();
				
				Object record = (Object)resultSet.get(0);
				Object[] valor = (Object[])record;
				diferencia =  ((Double)valor[0]+(Double)valor[1])-(Double)valor[2];
			
			return diferencia;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("diferenciaDeReserva failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	public Double obtenerSumaReservaInicial(BigDecimal idToReporteSiniestro) {
		LogDeMidasEJB3.log("obtenerSumaReservaInicial instances", Level.INFO, null);
		try {
			StringBuilder buffer = new StringBuilder();
			buffer.append("select ");
			buffer.append("		sum(model.estimacion)");
			buffer.append("from ");
			buffer.append("		ReservaDetalleDTO model ");
			buffer.append("where  ");
			buffer.append("		model.id.idtoreportesiniestro= :idToReporteSiniestro ");
			buffer.append(" 	and model.reservaDTO.tipoajuste = 0 ");
			
			Query query = entityManager.createQuery(buffer.toString());	
				
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			
			Object obj = query.getSingleResult();
			if(obj != null ){
				return (Double)obj;
			}else{
				return new Double(0.0);
			}
			
		}catch(NoResultException nre){
			return new Double(0.0);
		}catch (NonUniqueResultException nure) {
			return new Double(0.0);
		}catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenerSumaReservaInicial failed", Level.SEVERE, re);
			throw re;
		}
	}
}