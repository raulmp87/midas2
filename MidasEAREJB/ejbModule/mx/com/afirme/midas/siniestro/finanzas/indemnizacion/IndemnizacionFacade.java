package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity IndemnizacionDTO.
 * @see .IndemnizacionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class IndemnizacionFacade  implements IndemnizacionFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved IndemnizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity IndemnizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public IndemnizacionDTO save(IndemnizacionDTO entity) {
    				LogUtil.log("saving IndemnizacionDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
	    return entity;
    }
    
    /**
	 Delete a persistent IndemnizacionDTO entity.
	  @param entity IndemnizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(IndemnizacionDTO entity) {
    				LogUtil.log("deleting IndemnizacionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(IndemnizacionDTO.class, entity.getIdToIndemnizacion());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved IndemnizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the IndemnizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity IndemnizacionDTO entity to update
	 @return IndemnizacionDTO the persisted IndemnizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public IndemnizacionDTO update(IndemnizacionDTO entity) {
    				LogUtil.log("updating IndemnizacionDTO instance", Level.INFO, null);
	        try {
            IndemnizacionDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public IndemnizacionDTO findById( BigDecimal id) {
    				LogUtil.log("finding IndemnizacionDTO instance with id: " + id, Level.INFO, null);
	        try {
            IndemnizacionDTO instance = entityManager.find(IndemnizacionDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all IndemnizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the IndemnizacionDTO property to query
	  @param value the property value to match
	  	  @return List<IndemnizacionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<IndemnizacionDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding IndemnizacionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from IndemnizacionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all IndemnizacionDTO entities.
	  	  @return List<IndemnizacionDTO> all IndemnizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<IndemnizacionDTO> findAll(
		) {
					LogUtil.log("finding all IndemnizacionDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from IndemnizacionDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public IndemnizacionDTO obtenIndemnizacionPorAutorizar(BigDecimal idReporteSiniestro) {
		LogDeMidasEJB3.log("obtenIndemnizacionPorAutorizar Consulta", Level.INFO, null);
		try {
			final String queryString = "select model.idToIndemnizacion from IndemnizacionDTO model" +
									" where model.estatusFinanzasDTO.idTcEstatusfinanzas= :idEstatusFinanzas" +
									" and model.reporteSiniestroDTO.idToReporteSiniestro= :idReporteSiniestro";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idEstatusFinanzas", EstatusFinanzasDTO.POR_AUTORIZAR);
			query.setParameter("idReporteSiniestro", idReporteSiniestro);
			List resultados = query.getResultList();
			BigDecimal idToIndemnizacion= new BigDecimal(0);
			for (Object resultado : resultados) {
				idToIndemnizacion = (BigDecimal) resultado;
			}
			return findById(idToIndemnizacion);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenDocumentoSiniestro failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<IndemnizacionDTO> buscaPagosParciales(BigDecimal idReporteSiniestro) {
		LogUtil.log("buscaPagosParciales consulta", Level.INFO, null);
		try {
			final String queryString = "select model from IndemnizacionDTO model" +
				" where model.variosPagos= :pagoParcial" +
				" and model.reporteSiniestroDTO.idToReporteSiniestro= :idReporteSiniestro";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("pagoParcial", new Boolean(true));
			query.setParameter("idReporteSiniestro", idReporteSiniestro);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean tienePagosParciales(BigDecimal idReporteSiniestro) {
		LogDeMidasEJB3.log("tienePagosParciales Consulta", Level.INFO, null);
		boolean pagosParciales=false;
		try {
			final String queryString = "select model.idToIndemnizacion from IndemnizacionDTO model" +
									" where model.reporteSiniestroDTO.idToReporteSiniestro= :idReporteSiniestro" +
									" and model.variosPagos= :pagoParcial" +
									" and model.estatusFinanzasDTO.idTcEstatusfinanzas<> :idEstatusFinanzas";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idReporteSiniestro", idReporteSiniestro);
			query.setParameter("idEstatusFinanzas", EstatusFinanzasDTO.CANCELADO);
			query.setParameter("pagoParcial", new Boolean(true));
			List resultados = query.getResultList();
			if(resultados.size() > 0){
				pagosParciales = true;
			}
			return pagosParciales;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenDocumentoSiniestro failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<IndemnizacionDTO> obtenIndemnizacionPorEstatusNOCancelado(ReporteSiniestroDTO reporteSiniestroDTO) {
		LogDeMidasEJB3.log("obtenIndemnizacionPorEstatusNOCancelado Consulta", Level.INFO, null);
		try {
			final String queryString = "select model from IndemnizacionDTO model" +
									" where model.estatusFinanzasDTO.idTcEstatusfinanzas<> :idTcEstatusfinanzas" +
									" and model.reporteSiniestroDTO.idToReporteSiniestro= :idToReporteSiniestro";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTcEstatusfinanzas", EstatusFinanzasDTO.CANCELADO);
			query.setParameter("idToReporteSiniestro", reporteSiniestroDTO.getIdToReporteSiniestro());
			List<IndemnizacionDTO> result = query.getResultList();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenIndemnizacionPorEstatusNOCancelado failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
		public IndemnizacionDTO findLastPaymentByReporteSiniestro(
			ReporteSiniestroDTO reporteSiniestroDTO) {
		LogUtil.log("finding all IndemnizacionDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from IndemnizacionDTO model " +
					" where model.reporteSiniestro.idToReporteSiniestro=:idToReporteSiniestro " +
					" ";
			Query query = entityManager.createQuery(queryString);
			return (IndemnizacionDTO)query.getSingleResult();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
		
	
	public IndemnizacionDTO obtenerUltimoPagoUnicoPago(BigDecimal idReporteSiniestro,Short statusUnicoPago,Short statusUltimoPago) {
		LogDeMidasEJB3.log("obtenerUltimoPagoUnicoPago Consulta", Level.INFO, null);
		try {
			final String queryString = "select model from IndemnizacionDTO model" +
									" where model.reporteSiniestroDTO.idToReporteSiniestro= :idReporteSiniestro" +
									" and ((model.variosPagos= :soloPago and model.estatusFinanzasDTO.idTcEstatusfinanzas = :statusUnicoPago)" +
									" or (model.variosPagos= :pagoParcial and model.ultimoPago = :pagoUltimo and model.estatusFinanzasDTO.idTcEstatusfinanzas = :statusUltimoPago))";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idReporteSiniestro", idReporteSiniestro);
			query.setParameter("soloPago", new Boolean(false));
			query.setParameter("pagoParcial", new Boolean(true));
			query.setParameter("pagoUltimo", new Boolean(true));
			query.setParameter("statusUnicoPago", statusUnicoPago);
			query.setParameter("statusUltimoPago", statusUltimoPago);
			Object obj = query.getSingleResult();
			if(obj != null ){
				return (IndemnizacionDTO)obj;
			}else{
				return null;
			}
		}catch(NoResultException nre){
			return null;
		}catch (NonUniqueResultException nure) {
			return null;
		}catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenerUltimoPagoUnicoPago failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<IndemnizacionDTO> obtenerVariosPagos() {
		LogDeMidasEJB3.log("obtenerVariosPagos Consulta", Level.INFO, null);
		try {
			final String queryString = "select model from IndemnizacionDTO model" +
									" where model.variosPagos= :pagoParcial and model.reporteSiniestroDTO.reporteEstatus.idTcReporteEstatus <> 25 " ;
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("pagoParcial", new Boolean(true));
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenerVariosPagos failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	//Renombrar a buscarIndemnizacionesPendientes
	@SuppressWarnings("unchecked")
	public List<IndemnizacionDTO> getIndemnizacionPorReporteYEstatus(BigDecimal idReporteSiniestro,Object... params){
		LogDeMidasEJB3.log("getIndemnizacionPorReporteYEstatus instance with idReporeSiniestro: "+ idReporteSiniestro, Level.INFO, null);
		try {
			StringBuilder sb = new StringBuilder();		
			sb.append("select  ");
			sb.append("		model ");
			sb.append("from ");
			sb.append("		IndemnizacionDTO model ");
			sb.append("where ");
			sb.append("		model.reporteSiniestroDTO.idToReporteSiniestro = :idtoreportesiniestro ");
			sb.append("		and model.estatusFinanzasDTO.idTcEstatusfinanzas not in (");
			sb.append(EstatusFinanzasDTO.PAGADO);
			sb.append(", ");
			sb.append(EstatusFinanzasDTO.CANCELADO);
			sb.append(") ");
			
			final String queryString = sb.toString();
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idReporteSiniestro);
			
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getIndemnizacionPorReporteYEstatus failed", Level.SEVERE, re);
			throw re;
		}		
	}
	
	public IndemnizacionDTO obtenerUltimoPago(BigDecimal idToReporteSiniestro) {
		LogDeMidasEJB3.log("obtenerUltimoPago Consulta", Level.INFO, null);
		try {
			
			StringBuilder sb = new StringBuilder();		
			sb.append("select  ");
			sb.append("		model ");
			sb.append("from ");
			sb.append("		IndemnizacionDTO model ");
			sb.append("where ");
			sb.append("		model.reporteSiniestroDTO.idToReporteSiniestro = :idToReporteSiniestro ");
			sb.append("		and model.ultimoPago = :ultimoPago ");
			sb.append("		and model.estatusFinanzasDTO.idTcEstatusfinanzas <> ");
			sb.append(EstatusFinanzasDTO.CANCELADO);
			
			final String queryString = sb.toString();
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			query.setParameter("ultimoPago", new Boolean(true));
			
			Object obj = query.getSingleResult();
			if(obj != null ){
				return (IndemnizacionDTO)obj;
			}else{
				return null;
			}
		}catch(NoResultException nre){
			return null;
		}catch (NonUniqueResultException nure) {
			return null;
		}catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenerUltimoPago failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public IndemnizacionDTO obtenerUltimoPagoGenerado(BigDecimal idToReporteSiniestro) {
		LogDeMidasEJB3.log("obtenerUltimoPagoGenerado Consulta", Level.INFO, null);
		try {
			
			StringBuilder sb = new StringBuilder();		
			sb.append("select  ");
			sb.append("		model ");
			sb.append("from ");
			sb.append("		IndemnizacionDTO model ");
			sb.append("where ");
			sb.append("		model.reporteSiniestroDTO.idToReporteSiniestro = :idToReporteSiniestro ");
			sb.append("order by ");
			sb.append("		model.fechacreacion desc");
					
			final String queryString = sb.toString();
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			
			List<IndemnizacionDTO> lista = query.getResultList();;
			if(lista != null ){
				return (IndemnizacionDTO)lista.get(0);
			}else{
				return null;
			}
		}catch(NoResultException nre){
			return null;
		}catch (NonUniqueResultException nure) {
			return null;
		}catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenerUltimoPagoGenerado failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public Double getMontoReclamado(BigDecimal idToPoliza) {
		LogDeMidasEJB3.log("getMontoReclamado Consulta", Level.INFO, null);
		try {
			Double resultado = 0D;
			final String queryString = "SELECT     SUM (NVL (det.montopago, 0)) "+
								       "  + SUM (NVL (det.deducible, 0)) "+
								       "  + SUM (NVL (det.coaseguro, 0)) AS montoreclamado "+
								   " FROM midas.toindemnizacionriesgocobertura det, "+
								   "      midas.toindemnizacion ind, "+
								   "      midas.toautorizaciontecnica aut, "+
								   "      midas.toordendepago ord, "+
								   "      midas.toreportesiniestro rs "+
								   " WHERE det.idtoindemnizacion = ind.idtoindemnizacion "+
								   "  AND ind.idtoreportesiniestro = rs.idtoreportesiniestro "+
								   "  AND aut.idtoindemnizacion = ind.idtoindemnizacion "+
								   "  AND aut.idtoautorizaciontecnica = ord.idtoautorizaciontecnica "+
								   "  AND ord.estatus = 1 "+
								   "  AND ind.idtcestatus = 6 "+
								   "  AND det.idtopoliza  = "+idToPoliza +
								" GROUP BY det.idtopoliza ";
			Query query = entityManager.createNativeQuery(queryString);
			try{
				Object result = query.getSingleResult();
				if(result instanceof List)  {
					if(((List) result).get(0) != null) {
						resultado = (Double)((BigDecimal)((List)result).get(0)).doubleValue();
					}
				} else if (result instanceof BigDecimal) {
					resultado = (Double)((BigDecimal)result).doubleValue();
				}					
			}catch (javax.persistence.NoResultException nre){}
		
			return resultado;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenDocumentoSiniestro failed", Level.SEVERE, re);
			throw re;
		}
	}
}