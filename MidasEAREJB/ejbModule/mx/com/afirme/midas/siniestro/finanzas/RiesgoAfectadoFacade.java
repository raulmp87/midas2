package mx.com.afirme.midas.siniestro.finanzas;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity RiesgoAfectadoDTO.
 * 
 * @see .RiesgoAfectadoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class RiesgoAfectadoFacade implements RiesgoAfectadoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved RiesgoAfectadoDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            RiesgoAfectadoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RiesgoAfectadoDTO entity) {
		LogDeMidasEJB3.log("saving RiesgoAfectadoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent RiesgoAfectadoDTO entity.
	 * 
	 * @param entity
	 *            RiesgoAfectadoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoAfectadoDTO entity) {
		LogDeMidasEJB3.log("deleting RiesgoAfectadoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(RiesgoAfectadoDTO.class, entity
					.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved RiesgoAfectadoDTO entity and return it or a copy
	 * of it to the sender. A copy of the RiesgoAfectadoDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            RiesgoAfectadoDTO entity to update
	 * @return RiesgoAfectadoDTO the persisted RiesgoAfectadoDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoAfectadoDTO update(RiesgoAfectadoDTO entity) {
		LogDeMidasEJB3.log("updating RiesgoAfectadoDTO instance", Level.INFO, null);
		try {
			RiesgoAfectadoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RiesgoAfectadoDTO findById(RiesgoAfectadoId id) {
		LogDeMidasEJB3.log("finding RiesgoAfectadoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			RiesgoAfectadoDTO instance = entityManager.find(RiesgoAfectadoDTO.class,
					id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoAfectadoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoAfectadoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoAfectadoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoAfectadoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding RiesgoAfectadoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoAfectadoDTO model where model.estatus = 1 and  model."
					+ propertyName + "= :propertyValue " +
					"order by model.id.numeroinciso, model.id.numerosubinciso, model.id.idtoseccion, model.id.idtocobertura, model.id.idtoriesgo ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoAfectadoDTO entities.
	 * 
	 * @return List<RiesgoAfectadoDTO> all RiesgoAfectadoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoAfectadoDTO> findAll() {
		LogDeMidasEJB3.log("finding all RiesgoAfectadoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoAfectadoDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<RiesgoAfectadoDTO> listarFiltrado(RiesgoAfectadoId riesgoAfectadoId) {
		try {
			String queryString = "select model from RiesgoAfectadoDTO model where";
			queryString += " model.estatus = 1 and model.id.idtoreportesiniestro = :idValor order by model.id.numeroinciso, model.id.numerosubinciso, model.id.idtoseccion, model.id.idtocobertura, model.id.idtoriesgo ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idValor", riesgoAfectadoId.getIdtoreportesiniestro());
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Double sumaAseguradaPorReporte(BigDecimal idReporteSiniestro) {
		LogDeMidasEJB3.log("sumaAseguradaPorReporte Consulta", Level.INFO, null);
		try {
			final String queryString = "select SUM(model.sumaAsegurada) from RiesgoAfectadoDTO model where model.estatus = 1 and model.id.idtoreportesiniestro= :idReporteSiniestro";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idReporteSiniestro", idReporteSiniestro);
			List resultados = query.getResultList();
			Double valorSumaAseguradaReporte= new Double(0);
			for (Object resultado : resultados) {
				valorSumaAseguradaReporte = (Double) resultado;
			}
			return valorSumaAseguradaReporte;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("sumaAseguradaPorReporte failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SubRamoDTO> getSubramosFromReservaEstimada(BigDecimal idToReservaEstimada){
		LogDeMidasEJB3.log("getSubramosFromReservaEstimada Consulta", Level.INFO, null);
		
		StringBuilder sb = new StringBuilder();		
		sb.append("select distinct ");
		sb.append("    coberturaDTO.subRamoDTO ");
		sb.append("from ");
		sb.append("    ReservaDetalleDTO reservaDTO, ");
		sb.append("    CoberturaDTO coberturaDTO ");
		sb.append("where ");
		sb.append("		model.estatus = 1 ");
		sb.append("     and reservaDTO.id.idtoreservaestimada =  :idtoreservaestimada ");
		sb.append("		and coberturaDTO.idToCobertura = reservaDTO.riesgoAfectadoDTO.coberturaDTO.idToCobertura ");   				
							
		try {
			final String queryString = sb.toString();
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreservaestimada", idToReservaEstimada);
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getSubramosFromReservaEstimada failed", Level.SEVERE, re);
			throw re;
		}				
	}
	
	public Double getSumaAseguradaPorReporteYSubramo(BigDecimal idReporteSiniestro, BigDecimal idSubramo) {
		LogDeMidasEJB3.log("getSumaAseguradaPorReporteYSubramo Consulta", Level.INFO, null);
		
		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("    sum(riesgoAfectado.sumaAsegurada) ");
		sb.append("from ");
		sb.append("    RiesgoAfectadoDTO riesgoAfectado, ");
		sb.append("    CoberturaDTO cobertura ");
		sb.append("where  ");
		sb.append("		model.estatus = 1 ");
		sb.append("		and riesgoAfectado.id.idtoreportesiniestro = :idtoreportesiniestro ");
		sb.append("		and cobertura.idToCobertura = riesgoAfectado.coberturaDTO.idToCobertura ");
		sb.append("		and cobertura.subRamoDTO.idTcSubRamo = :idtcsubramo ");		
		
		try {
			final String queryString = sb.toString();
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idReporteSiniestro);
			query.setParameter("idtcsubramo", idSubramo);
									
			return (Double) query.getSingleResult();			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getSumaAseguradaPorReporteYSubramo failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public Double getPorcentajeSumaAseguradaPorReporteYSubramo(BigDecimal idReporteSiniestro, BigDecimal idSubramo) {
		LogDeMidasEJB3.log("getPorcentajeSumaAseguradaPorReporteYSubramo Consulta", Level.INFO, null);
		Double sumaAseguradaReporte = null;
		Double sumaAseguradaSubramo = null;
				
		try {

			sumaAseguradaReporte = this.sumaAseguradaPorReporte(idReporteSiniestro);
			sumaAseguradaSubramo = this.getSumaAseguradaPorReporteYSubramo(idReporteSiniestro, idSubramo);
			
			if((sumaAseguradaReporte != null) && (sumaAseguradaSubramo != null)){
				return new Double((sumaAseguradaSubramo.doubleValue()/sumaAseguradaReporte.doubleValue()) * 100); 
			}else{
				return new Double(0);
			}
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getPorcentajeSumaAseguradaPorReporteYSubramo failed", Level.SEVERE, re);
			throw re;
		}
	}	
	
	public BigDecimal getIncisoPorReportePoliza(BigDecimal idToReporteSiniestro, BigDecimal idToPoliza) {
		LogDeMidasEJB3.log("getIncisoPorReportePoliza Consulta", Level.INFO, null);
		
		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("    distinct model.id.numeroinciso ");
		sb.append("from ");
		sb.append("    RiesgoAfectadoDTO model ");
		sb.append("where  ");
		sb.append("		model.estatus = 1 ");
		sb.append("		and model.id.idtoreportesiniestro = :idToReporteSiniestro ");
		sb.append("		and model.id.idtopoliza = :idToPoliza ");
		
		try {
			final String queryString = sb.toString();
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			query.setParameter("idToPoliza", idToPoliza);
									
			Object obj = query.getSingleResult();
			if(obj != null ){
				return (BigDecimal)obj;
			}else{
				return new BigDecimal(-1);
			}
		
		}catch(NoResultException nre){
			return new BigDecimal(-1);
		}catch (NonUniqueResultException nure) {
			return new BigDecimal(-1);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getIncisoPorReportePoliza failed", Level.SEVERE, re);
			throw re;
		}
	}	
	
	@SuppressWarnings("unchecked")
	public List<RiesgoAfectadoId> getSubIncisoSeccionPorReportePoliza(BigDecimal idToReporteSiniestro, BigDecimal idToPoliza,BigDecimal numeroInciso) {
		LogDeMidasEJB3.log("getSubIncisoSeccionPorReportePoliza Consulta", Level.INFO, null);
		
		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("    distinct model.id.numerosubinciso,model.id.idtoseccion ");
		sb.append("from ");
		sb.append("    RiesgoAfectadoDTO model ");
		sb.append("where  ");
		sb.append("		model.estatus = 1 ");
		sb.append("	   and model.id.idtoreportesiniestro = :idToReporteSiniestro ");
		sb.append("	   and model.id.idtopoliza = :idToPoliza ");
		sb.append("	   and model.id.numeroinciso = :numeroInciso ");
		List<RiesgoAfectadoId> listaFinal = new ArrayList<RiesgoAfectadoId>();
		try {
			final String queryString = sb.toString();
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			query.setParameter("idToPoliza", idToPoliza);
			query.setParameter("numeroInciso", numeroInciso);
									
			List<Object> obj = query.getResultList();
			
			for(Object valor:obj){
				Object[] valoresReal = (Object[])valor;
				RiesgoAfectadoId riesgoAfectadoId = new RiesgoAfectadoId();
				riesgoAfectadoId.setNumerosubinciso(new BigDecimal(valoresReal[0].toString()));
				riesgoAfectadoId.setIdtoseccion(new BigDecimal(valoresReal[1].toString()));
				listaFinal.add(riesgoAfectadoId);
			}
			
			
		
		}catch (RuntimeException re) {
			LogDeMidasEJB3.log("getSubIncisoSeccionPorReportePoliza failed", Level.SEVERE, re);
			throw re;
		}
		 return listaFinal;
	}	
	
	@SuppressWarnings("unchecked")
	public List<RiesgoAfectadoDTO> getCoberturasRiesgoPorReporte(BigDecimal idToReporteSiniestro) {
		LogDeMidasEJB3.log("getSubIncisoSeccionPorReportePoliza Consulta", Level.INFO, null);
		
		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("    model ");
		sb.append("from ");
		sb.append("    RiesgoAfectadoDTO model ");
		sb.append("where  ");
		sb.append("	   model.id.idtoreportesiniestro = :idToReporteSiniestro ");
		sb.append("order by ");
		sb.append("	   model.id.numeroinciso, model.id.numerosubinciso, model.id.idtoseccion, model.id.idtocobertura, model.id.idtoriesgo ");
		 
		@SuppressWarnings("unused")
		List<RiesgoAfectadoId> listaFinal = new ArrayList<RiesgoAfectadoId>();
		try {
			final String queryString = sb.toString();
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);

			return query.getResultList();
			
		}catch (RuntimeException re) {
			LogDeMidasEJB3.log("getSubIncisoSeccionPorReportePoliza failed", Level.SEVERE, re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public List<RiesgoAfectadoDTO> getCoberturasRiesgoPorReporte(BigDecimal idToReporteSiniestro, Byte estatus) {
		LogDeMidasEJB3.log("getSubIncisoSeccionPorReportePoliza Consulta", Level.INFO, null);
		
		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("    model ");
		sb.append("from ");
		sb.append("    RiesgoAfectadoDTO model ");
		sb.append("where  ");
		sb.append("	   model.id.idtoreportesiniestro = :idToReporteSiniestro ");
		sb.append("	   and model.estatus = :estatus ");
		sb.append("order by ");
		sb.append("	   model.id.numeroinciso, model.id.numerosubinciso, model.id.idtoseccion, model.id.idtocobertura, model.id.idtoriesgo ");
		 
		@SuppressWarnings("unused")
		List<RiesgoAfectadoId> listaFinal = new ArrayList<RiesgoAfectadoId>();
		try {
			final String queryString = sb.toString();
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			query.setParameter("estatus", estatus);

			return query.getResultList();
			
		}catch (RuntimeException re) {
			LogDeMidasEJB3.log("getSubIncisoSeccionPorReportePoliza failed", Level.SEVERE, re);
			throw re;
		}
	}
}