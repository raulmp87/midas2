package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;

import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity IngresoSiniestro.
 * 
 * @see .IngresoSiniestro
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class IngresoSiniestroFacade implements IngresoSiniestroFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved IngresoSiniestro entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            IngresoSiniestro entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public IngresoSiniestroDTO save(IngresoSiniestroDTO entity) {
		LogDeMidasEJB3.log("saving IngresoSiniestroDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);			
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent IngresoSiniestro entity.
	 * 
	 * @param entity
	 *            IngresoSiniestro entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(IngresoSiniestroDTO entity) {
		LogDeMidasEJB3.log("deleting IngresoSiniestroDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(IngresoSiniestroDTO.class, entity
					.getIdToIngresoSiniestro());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved IngresoSiniestro entity and return it or a
	 * copy of it to the sender. A copy of the IngresoSiniestro entity parameter
	 * is returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            IngresoSiniestro entity to update
	 * @return IngresoSiniestro the persisted IngresoSiniestro entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public IngresoSiniestroDTO update(IngresoSiniestroDTO entity) {
		LogDeMidasEJB3.log("updating IngresoSiniestroDTO instance", Level.INFO, null);
		try {
			IngresoSiniestroDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public IngresoSiniestroDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding IngresoSiniestroDTO instance with id: " + id,
				Level.INFO, null);
		try {
			IngresoSiniestroDTO instance = entityManager.find(
					IngresoSiniestroDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all IngresoSiniestro entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the IngresoSiniestro property to query
	 * @param value
	 *            the property value to match
	 * @return List<IngresoSiniestro> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<IngresoSiniestroDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding IngresoSiniestroDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from IngresoSiniestroDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all IngresoSiniestro entities.
	 * 
	 * @return List<IngresoSiniestro> all IngresoSiniestro entities
	 */
	@SuppressWarnings("unchecked")
	public List<IngresoSiniestroDTO> findAll() {
		LogDeMidasEJB3.log("finding all IngresoSiniestro instances", Level.INFO, null);
		try {
			final String queryString = "select model from IngresoSiniestroDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all IngresoSiniestro entities which are part of a Reporte Siniestro.
	 * 
	 * @return List<IngresoSiniestro> all IngresoSiniestro entities
	 */		
	@SuppressWarnings("unchecked")
	public List<IngresoSiniestroDTO> findByIdReporteSiniestro(BigDecimal idReporteSiniestro){
		LogDeMidasEJB3.log("finding IngresoSiniestroDTO instance with idReporeSiniestro: "
				+ idReporteSiniestro, Level.INFO, null);
		try {
			final String queryString = 
				"select model from IngresoSiniestroDTO model where model.reporteSiniestroDTO.idToReporteSiniestro = :idtoreportesiniestro";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idReporteSiniestro);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<IngresoSiniestroDTO> listarIngresosPendientes(){
		LogDeMidasEJB3.log("finding listarIngresosPendientes", Level.INFO, null);
		try {
			final String queryString = 
				"select model from IngresoSiniestroDTO model where model.estatusFinanzas.idTcEstatusfinanzas not in (4,6) and model.reporteSiniestroDTO.reporteEstatus.idTcReporteEstatus <> 25";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("finding listarIngresosPendientes failed", Level.SEVERE, re);
			throw re;
		}		
	}
	@SuppressWarnings("unchecked")
	public List<IngresoSiniestroDTO> getIngresosPorReporteYEstatus(BigDecimal idReporteSiniestro,Object... params){
		LogDeMidasEJB3.log("getIngresosPorReporteYEstatus instance with idReporeSiniestro: "+ idReporteSiniestro, Level.INFO, null);
		try {
			StringBuilder sb = new StringBuilder();		
			sb.append("select  ");
			sb.append("		model ");
			sb.append("from ");
			sb.append("		IngresoSiniestroDTO model ");
			sb.append("where ");
			sb.append("		model.reporteSiniestroDTO.idToReporteSiniestro = :idtoreportesiniestro ");
			sb.append("		and model.estatusFinanzas.idTcEstatusfinanzas not in (");
			sb.append(EstatusFinanzasDTO.PAGADO);
			sb.append(", ");
			sb.append(EstatusFinanzasDTO.CANCELADO);
			sb.append(") ");
			
			final String queryString = sb.toString();
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idReporteSiniestro);
			
			
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getIngresosPorReporteYEstatus failed", Level.SEVERE, re);
			throw re;
		}		
	}
	
	private boolean existenCriteriosDefinidos(IngresoSiniestroFiltroDTO criteriosDeFiltrado){
		boolean result = false;
		
		result = criteriosDeFiltrado.getNumeroReporte() != null;
		result = result || (criteriosDeFiltrado.getFechaIngreso() != null);
		result = result || (criteriosDeFiltrado.getIdConceptoIngreso() != null);
		result = result || (criteriosDeFiltrado.getMonto() != null);
		
		return result;
	}
	
	private String geneararCondicionesDeFiltrado(IngresoSiniestroFiltroDTO criteriosDeFiltrado){
		StringBuilder sb = new StringBuilder();
		boolean agregarAnd = false;
		
		if(criteriosDeFiltrado.getNumeroReporte() != null && !StringUtils.isBlank(criteriosDeFiltrado.getNumeroReporte())){
			sb.append("tors.numeroreporte like '%" + criteriosDeFiltrado.getNumeroReporte() + "%' ");
			agregarAnd = true;
		}
		
		if(criteriosDeFiltrado.getMonto() != null){
			if(agregarAnd){
				sb.append(" and ");
			}
			
			sb.append("trunc(tois.monto) = trunc(" + criteriosDeFiltrado.getMonto().toString() + ") ");
			agregarAnd = true;
		}
		
		if(criteriosDeFiltrado.getIdConceptoIngreso() != null){
			if(agregarAnd){
				sb.append(" and ");
			}
			
			sb.append("tois.idtcconceptoingreso = " + criteriosDeFiltrado.getIdConceptoIngreso().toString());
			agregarAnd = true;
		}
		
		if(criteriosDeFiltrado.getFechaIngreso() != null){
			if(agregarAnd){
				sb.append(" and ");
			}
			
			sb.append("trunc(tois.fechacobro) = to_date('" + criteriosDeFiltrado.getFechaIngreso() + "', 'dd/mm/yyyy') ");
		}
		sb.append(" and  tois.idtcestatus in (");
		sb.append(EstatusFinanzasDTO.AUTORIZADO);
		sb.append(",");
		sb.append(EstatusFinanzasDTO.PAGADO);
		sb.append(" )");
		return sb.toString();
	}
	
	@SuppressWarnings("unchecked")
	public List<IngresoSiniestroDTO> listarIngresosPorAplicar(IngresoSiniestroFiltroDTO criteriosDeFiltrado){
		LogDeMidasEJB3.log("listarIngresosPorAplicar instance with filtro: " + criteriosDeFiltrado.toString(), Level.INFO, null);
		List<IngresoSiniestroDTO> resultSet = null;
		String condicionesFiltrado = "";

		try {
			StringBuilder sb = new StringBuilder();					
			sb.append("select ");
			sb.append("  tois.* ");
			sb.append("from ");
			sb.append("  MIDAS.toingresosiniestro tois join MIDAS.toreportesiniestro tors ");
			sb.append("    on tois.idtoreportesiniestro = tors.idtoreportesiniestro ");			
			
			if(this.existenCriteriosDefinidos(criteriosDeFiltrado)){
				sb.append("where ");
				condicionesFiltrado = this.geneararCondicionesDeFiltrado(criteriosDeFiltrado);
				sb.append(condicionesFiltrado);
			}
									
			sb.append(" order by tois.idtoingresosiniestro");
			
			LogDeMidasEJB3.log(sb.toString(), Level.WARNING, null);			
			final String queryString = sb.toString();						
			
			Query query = entityManager.createNativeQuery(queryString, IngresoSiniestroDTO.class);
			
			resultSet = query.getResultList();			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(criteriosDeFiltrado.toString(), Level.WARNING, re);
			LogDeMidasEJB3.log("listarIngresosPorAplicar failed", Level.SEVERE, re);			
			throw re;
		}		

		return resultSet;
	}
	
	@SuppressWarnings("unchecked")
	public List<IngresoSiniestroDTO> getIngresosNoCancelados(BigDecimal idReporteSiniestro){
		LogDeMidasEJB3.log("getIngresosPagados instance with idReporeSiniestro: "+ idReporteSiniestro, Level.INFO, null);
		try {
			StringBuilder sb = new StringBuilder();		
			sb.append("select  ");
			sb.append("		model ");
			sb.append("from ");
			sb.append("		IngresoSiniestroDTO model ");
			sb.append("where ");
			sb.append("		model.reporteSiniestroDTO.idToReporteSiniestro = :idtoreportesiniestro ");
			sb.append("		and model.estatusFinanzas.idTcEstatusfinanzas <> :idTcEstatusfinanzas ");
			
			final String queryString = sb.toString();
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idReporteSiniestro);
			query.setParameter("idTcEstatusfinanzas", EstatusFinanzasDTO.CANCELADO);
			
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getIngresosPagados failed", Level.SEVERE, re);
			throw re;
		}
	}
}