package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity IndemnizacionRiesgoCoberturaDTO.
 * @see .IndemnizacionRiesgoCoberturaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class IndemnizacionRiesgoCoberturaFacade  implements IndemnizacionRiesgoCoberturaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved IndemnizacionRiesgoCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity IndemnizacionRiesgoCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(IndemnizacionRiesgoCoberturaDTO entity) {
    				LogUtil.log("saving IndemnizacionRiesgoCoberturaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent IndemnizacionRiesgoCoberturaDTO entity.
	  @param entity IndemnizacionRiesgoCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(IndemnizacionRiesgoCoberturaDTO entity) {
    				LogUtil.log("deleting IndemnizacionRiesgoCoberturaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(IndemnizacionRiesgoCoberturaDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved IndemnizacionRiesgoCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the IndemnizacionRiesgoCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity IndemnizacionRiesgoCoberturaDTO entity to update
	 @return IndemnizacionRiesgoCoberturaDTO the persisted IndemnizacionRiesgoCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public IndemnizacionRiesgoCoberturaDTO update(IndemnizacionRiesgoCoberturaDTO entity) {
    				LogUtil.log("updating IndemnizacionRiesgoCoberturaDTO instance", Level.INFO, null);
	        try {
            IndemnizacionRiesgoCoberturaDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public IndemnizacionRiesgoCoberturaDTO findById( IndemnizacionRiesgoCoberturaId id) {
    				LogUtil.log("finding IndemnizacionRiesgoCoberturaDTO instance with id: " + id, Level.INFO, null);
	        try {
            IndemnizacionRiesgoCoberturaDTO instance = entityManager.find(IndemnizacionRiesgoCoberturaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all IndemnizacionRiesgoCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the IndemnizacionRiesgoCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<IndemnizacionRiesgoCoberturaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<IndemnizacionRiesgoCoberturaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding IndemnizacionRiesgoCoberturaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from IndemnizacionRiesgoCoberturaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all IndemnizacionRiesgoCoberturaDTO entities.
	  	  @return List<IndemnizacionRiesgoCoberturaDTO> all IndemnizacionRiesgoCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<IndemnizacionRiesgoCoberturaDTO> findAll(
		) {
					LogUtil.log("finding all IndemnizacionRiesgoCoberturaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from IndemnizacionRiesgoCoberturaDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	public Double getTotalCoaseguroPorIdAutorizacionTecnica(BigDecimal idToIndemnizacion) {
		LogUtil.log("getTotalCoaseguroPorIdAutorizacionTecnica IndemnizacionRiesgoCoberturaDTO instances", Level.INFO, null);
		
		try {
			final String queryString = 
				"select sum(model.coaseguro) from IndemnizacionRiesgoCoberturaDTO model where model.indemnizacionDTO.idToIndemnizacion = :idToIndemnizacion";
			
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idToIndemnizacion", idToIndemnizacion);
				
				return (Double) query.getSingleResult();
		} catch (RuntimeException re) {
					LogUtil.log("getTotalCoaseguroPorIdAutorizacionTecnica failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Double getTotalDeduciblePorIdAutorizacionTecnica(BigDecimal idToIndemnizacion) {
		LogUtil.log("getTotalCoaseguroPorIdAutorizacionTecnica IndemnizacionRiesgoCoberturaDTO instances", Level.INFO, null);
		
		try {
			final String queryString = 
				"select sum(model.deducible) from IndemnizacionRiesgoCoberturaDTO model where model.indemnizacionDTO.idToIndemnizacion = :idToIndemnizacion";
			
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idToIndemnizacion", idToIndemnizacion);
				
				return (Double) query.getSingleResult();
		} catch (RuntimeException re) {
					LogUtil.log("getTotalCoaseguroPorIdAutorizacionTecnica failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Double obtenerMontoTotalIndeminizacionCobertura(BigDecimal idToPoliza, BigDecimal numeroInciso,
			BigDecimal numeroSubInciso, BigDecimal idToSeccion, BigDecimal idToCobertura){
		LogUtil.log("obtenerMontoTotalIndeminizacionCobertura for ids instances", Level.INFO, null);
		List sumaMontos = new ArrayList();
		try {
			String queryString = "select sum(model.montoPago), sum(model.deducible) from IndemnizacionRiesgoCoberturaDTO model ";
			String sWhere = "";
			Query query;

			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToPoliza", idToPoliza, "idToPoliza");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.numeroInciso", numeroInciso, "numeroInciso");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.numeroSubinciso", numeroSubInciso, "numeroSubInciso");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToSeccion", idToSeccion, "idToSeccion");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToCobertura", idToCobertura, "idToCobertura");
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "indemnizacionDTO.estatusFinanzasDTO.idTcEstatusfinanzas", EstatusFinanzasDTO.PAGADO, "idTcEstatusfinanzas");
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			sumaMontos = query.getResultList();
			
			if (sumaMontos==null || sumaMontos.size()<=0 || sumaMontos.isEmpty()){
				return new Double("0.0");
			}else{
				if (sumaMontos.get(0)!=null){
					Object[] objs = (Object[])sumaMontos.get(0); 
					if (objs!=null && objs.length>0 && objs[0]!=null && objs[1]!=null){
						return (Double)objs[0] + (Double)objs[1];
					}else{
						return new Double("0.0");
					}
				}else 
					return new Double("0.0");
			}
		} catch (RuntimeException re) {
					LogUtil.log("obtenerMontoTotalIndeminizacionCobertura failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public Double[] sumaPagosParcialesAgregar(BigDecimal idToReporteSiniestro) {
		LogUtil.log("sumaPagosParcialesAgregar consulta", Level.INFO, null);
		try {
			
			StringBuilder sb = new StringBuilder();		
			sb.append(" select  ");
			sb.append("		sum(model.montoPago),sum(model.deducible),sum(model.coaseguro) ");
			sb.append(" from ");
			sb.append("		IndemnizacionRiesgoCoberturaDTO model ");
			sb.append(" where ");
			sb.append("		model.indemnizacionDTO.reporteSiniestroDTO.idToReporteSiniestro = :idToReporteSiniestro ");
			sb.append("		and model.indemnizacionDTO.estatusFinanzasDTO.idTcEstatusfinanzas <>  ");
			sb.append(EstatusFinanzasDTO.CANCELADO);
			sb.append(" group by ");
			sb.append("		model.id.numeroInciso,model.id.numeroSubinciso,model.id.idToSeccion,model.id.idToCobertura,model.id.idToRiesgo");
			sb.append(" order by ");
			sb.append("		model.id.numeroInciso,model.id.numeroSubinciso,model.id.idToSeccion,model.id.idToCobertura,model.id.idToRiesgo");
			
			
			Query query = entityManager.createQuery(sb.toString());
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			List<?> resultSet = query.getResultList();
			Double[] listaFinal = new Double[resultSet.size()];
			
			for(int i=0; i < resultSet.size() ; i++){			
				
				Object record = (Object)resultSet.get(i);
				Object[] valor = (Object[])record;
				listaFinal[i] = (Double)valor[0]+(Double)valor[1]+(Double)valor[2];
			}
			return listaFinal;
		} catch (RuntimeException re) {
			LogUtil.log("sumaPagosParcialesAgregar failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public Double[] sumaPagosParcialesModificar(BigDecimal idToReporteSiniestro,BigDecimal idToIndemnizacion) {
		LogUtil.log("sumaPagosParcialesModificar consulta", Level.INFO, null);
		try {
			
			StringBuilder sb = new StringBuilder();		
			sb.append(" select  ");
			sb.append("		sum(model.montoPago),sum(model.deducible),sum(model.coaseguro) ");
			sb.append(" from ");
			sb.append("		IndemnizacionRiesgoCoberturaDTO model ");
			sb.append(" where ");
			sb.append("		model.indemnizacionDTO.reporteSiniestroDTO.idToReporteSiniestro = :idToReporteSiniestro ");
			sb.append("		and model.id.idToIndemnizacion <> :idToIndemnizacion ");
			sb.append("		and model.indemnizacionDTO.estatusFinanzasDTO.idTcEstatusfinanzas <>  ");
			sb.append(EstatusFinanzasDTO.CANCELADO);
			sb.append(" group by ");
			sb.append("		model.id.numeroInciso,model.id.numeroSubinciso,model.id.idToSeccion,model.id.idToCobertura,model.id.idToRiesgo");
			sb.append(" order by ");
			sb.append("		model.id.numeroInciso,model.id.numeroSubinciso,model.id.idToSeccion,model.id.idToCobertura,model.id.idToRiesgo");
			
			Query query = entityManager.createQuery(sb.toString());
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			query.setParameter("idToIndemnizacion", idToIndemnizacion);
			List<?> resultSet = query.getResultList();
			Double[] listaFinal = new Double[resultSet.size()];
			
			for(int i=0; i < resultSet.size() ; i++){			
				
				Object record = (Object)resultSet.get(i);
				Object[] valor = (Object[])record;
				listaFinal[i] = (Double)valor[0]+(Double)valor[1]+(Double)valor[2];
			}
			return listaFinal;
		} catch (RuntimeException re) {
			LogUtil.log("sumaPagosParcialesModificar failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	public Double sumaPerdidaDeterminadaPorReporte(BigDecimal idToReporteSiniestro) {
		LogUtil.log("sumaPerdidaDeterminadaPorReporte consulta", Level.INFO, null);
		try {
			
			StringBuilder sb = new StringBuilder();		
			sb.append(" select  ");
			sb.append("		sum(model.montoPago),sum(model.deducible),sum(model.coaseguro) ");
			sb.append(" from ");
			sb.append("		IndemnizacionRiesgoCoberturaDTO model ");
			sb.append(" where ");
			sb.append("		model.indemnizacionDTO.reporteSiniestroDTO.idToReporteSiniestro = :idToReporteSiniestro ");
			sb.append("		and model.indemnizacionDTO.estatusFinanzasDTO.idTcEstatusfinanzas =  ");
			sb.append(EstatusFinanzasDTO.PAGADO);
			
			
			Query query = entityManager.createQuery(sb.toString());
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			List<?> resultSet = query.getResultList();
			Double sumaTotalPerdidaDeterminada = new Double(0.0);
			
			if(resultSet.size() > 0){
				Object record = (Object)resultSet.get(0);
				Object[] valor = (Object[])record;
				if(valor[0] != null){
					sumaTotalPerdidaDeterminada = (Double)valor[0] + (Double)valor[1] + (Double)valor[2];
				}
			}
			
			return sumaTotalPerdidaDeterminada;
		} catch (RuntimeException re) {
			LogUtil.log("sumaPerdidaDeterminadaPorReporte failed", Level.SEVERE, re);
			throw re;
		}
	}
}