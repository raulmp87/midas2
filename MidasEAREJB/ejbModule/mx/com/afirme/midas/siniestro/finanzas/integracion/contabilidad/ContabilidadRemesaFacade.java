package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.siniestro.finanzas.ingreso.AplicacionIngresoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ContabilidadRemesa.
 * 
 * @see .ContabilidadRemesa
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ContabilidadRemesaFacade implements ContabilidadRemesaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved ContabilidadRemesa
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            ContabilidadRemesa entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ContabilidadRemesaDTO entity) {
		LogDeMidasEJB3.log("saving ContabilidadRemesa instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ContabilidadRemesa entity.
	 * 
	 * @param entity
	 *            ContabilidadRemesa entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ContabilidadRemesaDTO entity) {
		LogDeMidasEJB3.log("deleting ContabilidadRemesa instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(ContabilidadRemesaDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ContabilidadRemesa entity and return it or a
	 * copy of it to the sender. A copy of the ContabilidadRemesa entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            ContabilidadRemesa entity to update
	 * @return ContabilidadRemesa the persisted ContabilidadRemesa entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ContabilidadRemesaDTO update(ContabilidadRemesaDTO entity) {
		LogDeMidasEJB3.log("updating ContabilidadRemesa instance", Level.INFO, null);
		try {
			ContabilidadRemesaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ContabilidadRemesaDTO findById(ContabilidadRemesaId id) {
		LogDeMidasEJB3.log(	"finding ContabilidadRemesa instance with id: " + id,	Level.INFO, null);
		try {
			ContabilidadRemesaDTO instance = entityManager.find(ContabilidadRemesaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ContabilidadRemesa entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ContabilidadRemesa property to query
	 * @param value
	 *            the property value to match
	 * @return List<ContabilidadRemesa> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ContabilidadRemesaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding ContabilidadRemesa instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ContabilidadRemesa model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ContabilidadRemesa entities.
	 * 
	 * @return List<ContabilidadRemesa> all ContabilidadRemesa entities
	 */
	@SuppressWarnings("unchecked")
	public List<ContabilidadRemesaDTO> findAll() {
		LogDeMidasEJB3.log("finding all ContabilidadRemesa instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from ContabilidadRemesa model";
			Query query = entityManager.createQuery(queryString);
			
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ContabilidadRemesaDTO> buscarMovimientosPendientesActualizar(BigDecimal idToAplicacionIngreso){
		LogDeMidasEJB3.log("buscarMovimientosPendientesActualizar instance", Level.INFO, null);
		try {
			StringBuilder sb = new StringBuilder();		
			
			sb.append("select  ");
			sb.append("		model ");
			sb.append("from ");
			sb.append("		ContabilidadRemesaDTO model ");
			sb.append("where ");
			sb.append("		model.aplicacionIngresoDTO.idToAplicacionIngreso = :idToAplicacionIngreso ");
			sb.append("		and model.idPolizaContable is null ");
			
			final String queryString = sb.toString();
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToAplicacionIngreso", idToAplicacionIngreso);
			
			return query.getResultList();
			
		}catch (RuntimeException re) {
			LogDeMidasEJB3.log("buscarMovimientosPendientesActualizar failed", Level.SEVERE, re);
			throw re;
		}	
	}
}