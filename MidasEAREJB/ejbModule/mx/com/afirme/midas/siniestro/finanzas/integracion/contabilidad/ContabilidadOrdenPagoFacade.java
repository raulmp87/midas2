package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

//default package

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
* Facade for entity ContabilidadOrdenPago.
* 
* @see .ContabilidadOrdenPago
* @author MyEclipse Persistence Tools
*/
@Stateless
public class ContabilidadOrdenPagoFacade implements
		ContabilidadOrdenPagoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved ContabilidadOrdenPago
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            ContabilidadOrdenPago entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ContabilidadOrdenPagoDTO entity) {
		LogDeMidasEJB3.log("saving ContabilidadOrdenPagoDTO instance", Level.INFO,	null);
		
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ContabilidadOrdenPago entity.
	 * 
	 * @param entity
	 *            ContabilidadOrdenPago entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ContabilidadOrdenPagoDTO entity) {
		LogDeMidasEJB3.log("deleting ContabilidadOrdenPagoDTO instance", Level.INFO, null);
		
		try {
			entity = entityManager.getReference(ContabilidadOrdenPagoDTO.class,	entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ContabilidadOrdenPago entity and return it or
	 * a copy of it to the sender. A copy of the ContabilidadOrdenPago entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            ContabilidadOrdenPago entity to update
	 * @return ContabilidadOrdenPago the persisted ContabilidadOrdenPago entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ContabilidadOrdenPagoDTO update(ContabilidadOrdenPagoDTO entity) {
		LogDeMidasEJB3.log("updating ContabilidadOrdenPagoDTO instance", Level.INFO, null);
		
		try {
			ContabilidadOrdenPagoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ContabilidadOrdenPagoDTO findById(ContabilidadOrdenPagoId id) {
		LogDeMidasEJB3.log("finding ContabilidadOrdenPagoDTO instance with id: " + id, Level.INFO, null);
		
		try {
			ContabilidadOrdenPagoDTO instance = entityManager.find(
					ContabilidadOrdenPagoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ContabilidadOrdenPago entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ContabilidadOrdenPago property to query
	 * @param value
	 *            the property value to match
	 * @return List<ContabilidadOrdenPago> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ContabilidadOrdenPagoDTO> findByProperty(String propertyName, final Object value) {
		LogDeMidasEJB3.log(	"finding ContabilidadOrdenPagoDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = 
				"select model from ContabilidadOrdenPagoDTO model where model."
					+ propertyName + "= :propertyValue";
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ContabilidadOrdenPago entities.
	 * 
	 * @return List<ContabilidadOrdenPago> all ContabilidadOrdenPago entities
	 */
	@SuppressWarnings("unchecked")
	public List<ContabilidadOrdenPagoDTO> findAll() {
		LogDeMidasEJB3.log("finding all ContabilidadOrdenPago instances", Level.INFO, null);
		
		try {
			final String queryString = "select model from ContabilidadOrdenPagoDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
}