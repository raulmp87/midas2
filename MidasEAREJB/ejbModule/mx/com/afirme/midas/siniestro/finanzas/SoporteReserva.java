package mx.com.afirme.midas.siniestro.finanzas;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;

import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionFacadeRemote;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaFacadeRemote;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaId;

public class SoporteReserva {
	@EJB 
	RiesgoAfectadoFacadeRemote ejbRiesgoAfectado;
	@EJB 
	ReservaDetalleFacadeRemote ejbReservaDetalle;
	@EJB
	IndemnizacionFacadeRemote ejbIndemnizacion;
	@EJB
	IndemnizacionRiesgoCoberturaFacadeRemote ejbIndemnizacionRiesgoCobertura;

	protected void actualizarRiesgosAfectados(List<RiesgoAfectadoDTO> riesgosAfectados){
		for(RiesgoAfectadoDTO riesgoAfectado : riesgosAfectados){
			RiesgoAfectadoId riesgoAfecatdoId = riesgoAfectado.getId(); 
			RiesgoAfectadoDTO refRiesgoAfectado = ejbRiesgoAfectado.findById(riesgoAfecatdoId);
			refRiesgoAfectado.setEstatus(RiesgoAfectadoDTO.INACTIVO);
			ejbRiesgoAfectado.update(refRiesgoAfectado);
		}				
	}	
	protected void guardarDetalleDeReserva(BigDecimal idtoreservaestimada,List<ReservaDetalleDTO> reservaDetalle){
		for(ReservaDetalleDTO entity : reservaDetalle){
			ReservaDetalleId id = entity.getId();
			id.setIdtoreservaestimada(idtoreservaestimada);
			ejbReservaDetalle.save(entity);
		}
	}
	
	protected void eliminaDetalleDeReserva(List<ReservaDetalleDTO> ListaReservaDetalle){
		for(ReservaDetalleDTO reservaDetalle : ListaReservaDetalle){
			ejbReservaDetalle.delete(reservaDetalle);
		}				
	}	
	
	protected IndemnizacionDTO guardarIndemnizacion(IndemnizacionDTO indemnizacionDTO){
		return ejbIndemnizacion.save(indemnizacionDTO);
		
	}
	
	protected void guardarDetalleIndemnizacion(IndemnizacionDTO indemnizacionDTO,List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion){
		for(IndemnizacionRiesgoCoberturaDTO entity : listaDetalleIndemnizacion){
			IndemnizacionRiesgoCoberturaId id = entity.getId();
			id.setIdToIndemnizacion(indemnizacionDTO.getIdToIndemnizacion());
			entity.setIndemnizacionDTO(indemnizacionDTO);
			ejbIndemnizacionRiesgoCobertura.save(entity);
		}
	}
	
	protected IndemnizacionDTO modificarIndemnizacion(IndemnizacionDTO indemnizacionDTO){
		return ejbIndemnizacion.update(indemnizacionDTO);
		
	}
	
	protected void eliminarIndemnizacion(IndemnizacionDTO indemnizacionDTO){
		ejbIndemnizacion.delete(indemnizacionDTO);
	}
	
	protected void eliminarDetalleIndemnizacion(List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion){
		for(IndemnizacionRiesgoCoberturaDTO entity : listaDetalleIndemnizacion){
			ejbIndemnizacionRiesgoCobertura.delete(entity);
		}
	}
	
	protected void modificarDetalleIndemnizacion(List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion){
		for(IndemnizacionRiesgoCoberturaDTO entity : listaDetalleIndemnizacion){
			ejbIndemnizacionRiesgoCobertura.update(entity);
		}
	}
	
	protected List<IndemnizacionRiesgoCoberturaDTO> buscarDetalleIndemnizacion(IndemnizacionDTO indemnizacionDTO){
		List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion = ejbIndemnizacionRiesgoCobertura.findByProperty("indemnizacionDTO", indemnizacionDTO);
		return listaDetalleIndemnizacion;
	}
}
