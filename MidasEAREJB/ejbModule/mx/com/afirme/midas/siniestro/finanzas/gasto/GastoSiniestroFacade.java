package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity GastoSiniestro.
 * 
 * @see .GastoSiniestro
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class GastoSiniestroFacade implements GastoSiniestroFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved GastoSiniestro entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            GastoSiniestro entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(GastoSiniestroDTO entity) {
		LogDeMidasEJB3.log("saving GastoSiniestroDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent GastoSiniestro entity.
	 * 
	 * @param entity
	 *            GastoSiniestro entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(GastoSiniestroDTO entity) {
		LogDeMidasEJB3.log("deleting GastoSiniestroDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(GastoSiniestroDTO.class, entity
					.getIdToGastoSiniestro());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved GastoSiniestro entity and return it or a copy
	 * of it to the sender. A copy of the GastoSiniestro entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            GastoSiniestro entity to update
	 * @return GastoSiniestro the persisted GastoSiniestro entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public GastoSiniestroDTO update(GastoSiniestroDTO entity) {
		LogDeMidasEJB3.log("updating GastoSiniestroDTO instance", Level.INFO, null);
		try {
			GastoSiniestroDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public GastoSiniestroDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding GastoSiniestroDTO instance with id: " + id,
				Level.INFO, null);
		try {
			GastoSiniestroDTO instance = entityManager.find(GastoSiniestroDTO.class,
					id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all GastoSiniestro entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the GastoSiniestro property to query
	 * @param value
	 *            the property value to match
	 * @return List<GastoSiniestro> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<GastoSiniestroDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding GastoSiniestroDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from GastoSiniestroDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all GastoSiniestro entities.
	 * 
	 * @return List<GastoSiniestro> all GastoSiniestro entities
	 */
	@SuppressWarnings("unchecked")
	public List<GastoSiniestroDTO> findAll() {
		LogDeMidasEJB3.log("finding all GastoSiniestroDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from GastoSiniestroDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	/**
	 * Find all GastoSiniestro entities which are part of a Reporte Siniestro.
	 * 
	 * @return List<GastoSiniestro> all GastoSiniestro entities
	 */		
	@SuppressWarnings("unchecked")
	public List<GastoSiniestroDTO> findByIdReporteSiniestro(BigDecimal idReporteSiniestro){
		LogDeMidasEJB3.log("finding GastoSiniestroDTO instance with idReporeSiniestro: "
				+ idReporteSiniestro, Level.INFO, null);
		try {
			final String queryString = 
				"select model from GastoSiniestroDTO model where model.reporteSiniestroDTO.idToReporteSiniestro = :idtoreportesiniestro";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idReporteSiniestro);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<GastoSiniestroDTO> listarGastosPendientes(){
		LogDeMidasEJB3.log("finding listarGastosPendientes", Level.INFO, null);
		try {
			final String queryString = 
				"select model from GastoSiniestroDTO model where model.estatusFinanzas.idTcEstatusfinanzas not in (4,6) and model.reporteSiniestroDTO.reporteEstatus.idTcReporteEstatus <> 25";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("finding listarGastosPendientes failed", Level.SEVERE, re);
			throw re;
		}		
	}
	
	//Falta cambiar el nombre a la funcion por el nombre de listarGastosPendientes
	@SuppressWarnings("unchecked")
	public List<GastoSiniestroDTO> getGastosPorReporteYEstatus(BigDecimal idReporteSiniestro,Object... params){
		LogDeMidasEJB3.log("getGastosPorReporteYEstatus instance with idReporeSiniestro: " + idReporteSiniestro, Level.INFO, null);
		try {
			
			StringBuilder sb = new StringBuilder();		
			sb.append("select  ");
			sb.append("		model ");
			sb.append("from ");
			sb.append("		GastoSiniestroDTO model ");
			sb.append("where ");
			sb.append("		model.reporteSiniestroDTO.idToReporteSiniestro = :idtoreportesiniestro ");
			sb.append("		and model.estatusFinanzas.idTcEstatusfinanzas not in (");
			sb.append(EstatusFinanzasDTO.PAGADO);
			sb.append(", ");
			sb.append(EstatusFinanzasDTO.CANCELADO);
			sb.append(") ");
			
			final String queryString = sb.toString();
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idReporteSiniestro);
//			query.setParameter("estatus", valoresEstatus);
			
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getGastosPorReporteYEstatus failed", Level.SEVERE, re);
			throw re;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<GastoSiniestroDTO> getGastosNoCancelados(BigDecimal idReporteSiniestro){
		LogDeMidasEJB3.log("getGastosPagados instance with idReporeSiniestro: " + idReporteSiniestro, Level.INFO, null);
		try {
			
			StringBuilder sb = new StringBuilder();		
			sb.append("select  ");
			sb.append("		model ");
			sb.append("from ");
			sb.append("		GastoSiniestroDTO model ");
			sb.append("where ");
			sb.append("		model.reporteSiniestroDTO.idToReporteSiniestro = :idtoreportesiniestro ");
			sb.append("		and model.estatusFinanzas.idTcEstatusfinanzas <> :idTcEstatusfinanzas ");
			
			final String queryString = sb.toString();
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtoreportesiniestro", idReporteSiniestro);
			query.setParameter("idTcEstatusfinanzas", EstatusFinanzasDTO.CANCELADO);
			
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getGastosPagados failed", Level.SEVERE, re);
			throw re;
		}
	}

}