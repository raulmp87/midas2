package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity ConceptoIngreso.
 * 
 * @see .ConceptoIngreso
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ConceptoIngresoFacade implements ConceptoIngresoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved ConceptoIngreso entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ConceptoIngreso entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ConceptoIngresoDTO entity) {
		LogDeMidasEJB3.log("saving ConceptoIngresoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ConceptoIngreso entity.
	 * 
	 * @param entity
	 *            ConceptoIngreso entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ConceptoIngresoDTO entity) {
		LogDeMidasEJB3.log("deleting ConceptoIngresoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(ConceptoIngresoDTO.class, entity
					.getIdTcConceptoIngreso());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ConceptoIngreso entity and return it or a copy
	 * of it to the sender. A copy of the ConceptoIngreso entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            ConceptoIngreso entity to update
	 * @return ConceptoIngreso the persisted ConceptoIngreso entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ConceptoIngresoDTO update(ConceptoIngresoDTO entity) {
		LogDeMidasEJB3.log("updating ConceptoIngresoDTO instance", Level.INFO, null);
		try {
			ConceptoIngresoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ConceptoIngresoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding ConceptoIngresoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			ConceptoIngresoDTO instance = entityManager.find(
					ConceptoIngresoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ConceptoIngreso entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ConceptoIngreso property to query
	 * @param value
	 *            the property value to match
	 * @return List<ConceptoIngreso> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ConceptoIngresoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding ConceptoIngresoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ConceptoIngresoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ConceptoIngreso entities.
	 * 
	 * @return List<ConceptoIngreso> all ConceptoIngreso entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConceptoIngresoDTO> findAll() {
		LogDeMidasEJB3.log("finding all ConceptoIngreso instances", Level.INFO, null);
		try {
			final String queryString = "select model from ConceptoIngresoDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
}