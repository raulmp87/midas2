package mx.com.afirme.midas.siniestro.finanzas;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity EstatusFinanzasDTO.
 * @see .EstatusFinanzasDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class EstatusFinanzasFacade  implements EstatusFinanzasFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved EstatusFinanzasDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EstatusFinanzasDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EstatusFinanzasDTO entity) {
    				LogUtil.log("saving EstatusFinanzasDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent EstatusFinanzasDTO entity.
	  @param entity EstatusFinanzasDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EstatusFinanzasDTO entity) {
    				LogUtil.log("deleting EstatusFinanzasDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(EstatusFinanzasDTO.class, entity.getIdTcEstatusfinanzas());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved EstatusFinanzasDTO entity and return it or a copy of it to the sender. 
	 A copy of the EstatusFinanzasDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EstatusFinanzasDTO entity to update
	 @return EstatusFinanzasDTO the persisted EstatusFinanzasDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public EstatusFinanzasDTO update(EstatusFinanzasDTO entity) {
    				LogUtil.log("updating EstatusFinanzasDTO instance", Level.INFO, null);
	        try {
            EstatusFinanzasDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public EstatusFinanzasDTO findById( Short id) {
    				LogUtil.log("finding EstatusFinanzasDTO instance with id: " + id, Level.INFO, null);
	        try {
            EstatusFinanzasDTO instance = entityManager.find(EstatusFinanzasDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all EstatusFinanzasDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EstatusFinanzasDTO property to query
	  @param value the property value to match
	  	  @return List<EstatusFinanzasDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<EstatusFinanzasDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding EstatusFinanzasDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from EstatusFinanzasDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all EstatusFinanzasDTO entities.
	  	  @return List<EstatusFinanzasDTO> all EstatusFinanzasDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<EstatusFinanzasDTO> findAll(
		) {
					LogUtil.log("finding all EstatusFinanzasDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from EstatusFinanzasDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}