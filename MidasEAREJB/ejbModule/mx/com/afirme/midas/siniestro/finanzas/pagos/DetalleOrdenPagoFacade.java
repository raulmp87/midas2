package mx.com.afirme.midas.siniestro.finanzas.pagos;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.siniestro.finanzas.ordendepago.DetalleOrdenPagoDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.DetalleOrdenPagoFacadeRemote;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity DetalleOrdenPagoDTO.
 * 
 * @see .DetalleOrdenPagoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class DetalleOrdenPagoFacade implements
		DetalleOrdenPagoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved DetalleOrdenPagoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            DetalleOrdenPagoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DetalleOrdenPagoDTO entity) {
		LogUtil.log("saving DetalleOrdenPagoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent DetalleOrdenPagoDTO entity.
	 * 
	 * @param entity
	 *            DetalleOrdenPagoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DetalleOrdenPagoDTO entity) {
		LogUtil.log("deleting DetalleOrdenPagoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(DetalleOrdenPagoDTO.class,
					entity.getIdToDetalleOrdenPago());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved DetalleOrdenPagoDTO entity and return it or a
	 * copy of it to the sender. A copy of the DetalleOrdenPagoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            DetalleOrdenPagoDTO entity to update
	 * @return DetalleOrdenPagoDTO the persisted DetalleOrdenPagoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DetalleOrdenPagoDTO update(DetalleOrdenPagoDTO entity) {
		LogUtil.log("updating DetalleOrdenPagoDTO instance", Level.INFO, null);
		try {
			DetalleOrdenPagoDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DetalleOrdenPagoDTO findById(BigDecimal id) {
		LogUtil.log("finding DetalleOrdenPagoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			DetalleOrdenPagoDTO instance = entityManager.find(
					DetalleOrdenPagoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DetalleOrdenPagoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the DetalleOrdenPagoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DetalleOrdenPagoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<DetalleOrdenPagoDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding DetalleOrdenPagoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from DetalleOrdenPagoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DetalleOrdenPagoDTO entities.
	 * 
	 * @return List<DetalleOrdenPagoDTO> all DetalleOrdenPagoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DetalleOrdenPagoDTO> findAll() {
		LogUtil.log("finding all DetalleOrdenPagoDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from DetalleOrdenPagoDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}