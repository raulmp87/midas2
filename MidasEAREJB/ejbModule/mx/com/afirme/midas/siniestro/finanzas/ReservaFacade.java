package mx.com.afirme.midas.siniestro.finanzas;

// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity ReservaDTO.
 * 
 * @see .ReservaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ReservaFacade extends SoporteReserva implements ReservaFacadeRemote {	
	@Resource
	private SessionContext context;
		
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved ReservaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ReservaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public ReservaDTO save(ReservaDTO entity) {
		LogDeMidasEJB3.log("saving ReservaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			entityManager.flush();
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ReservaDTO entity.
	 * 
	 * @param entity
	 *            ReservaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ReservaDTO entity) {
		LogDeMidasEJB3.log("deleting ReservaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(ReservaDTO.class, entity
					.getIdtoreservaestimada());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ReservaDTO entity and return it or a copy of
	 * it to the sender. A copy of the ReservaDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ReservaDTO entity to update
	 * @return ReservaDTO the persisted ReservaDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ReservaDTO update(ReservaDTO entity) {
		LogDeMidasEJB3.log("updating ReservaDTO instance", Level.INFO, null);
		try {
			ReservaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ReservaDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding ReservaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			ReservaDTO instance = entityManager.find(ReservaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ReservaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ReservaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ReservaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ReservaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding ReservaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ReservaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ReservaDTO entities.
	 * 
	 * @return List<ReservaDTO> all ReservaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReservaDTO> findAll() {
		LogDeMidasEJB3
				.log("finding all ReservaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from ReservaDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ReservaDTO> listarPorFechaEstimacion(BigDecimal idReporteSiniestro) {
		LogDeMidasEJB3.log("listarPorFechaEstimacion ReservaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from ReservaDTO model where model.reporteSiniestroDTO.idToReporteSiniestro= :idReporte" 
									+ " order by model.fechaestimacion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idReporte", idReporteSiniestro);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarPorFechaEstimacion failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Obtiene el registro de la reserva estimana mas actual de un reporte de siniestro.
	 * 
	 * @param idToReporteSiniestro
	 * 			Represnta el numero de reporte del siniestro
	 * @param autorizada
	 * 			Indica si recupera la reserva que ya ha sido autorizada (True)
	 * @return ReservaDTO 
	 */	
	@SuppressWarnings("unchecked")
	public List<ReservaDTO> obtenerUltimaReserva(BigDecimal idToReporteSiniestro,
			Boolean autorizada) {
		LogDeMidasEJB3.log("", Level.INFO, null);
		StringBuilder buffer = new StringBuilder();

		buffer.append("select model ");
		buffer.append("from ");
		buffer.append("	   ReservaDTO model ");
		buffer.append("where  ");
		buffer.append("	   model.reporteSiniestroDTO.idToReporteSiniestro = :idToReporteSiniestro ");
		buffer.append("and model.autorizada = :autorizada  ");
		buffer.append("and model.fechaestimacion = ( ");
		buffer.append("		select  ");
		buffer.append("			max(innerModel.fechaestimacion) ");
		buffer.append("		from  ");
		buffer.append("			ReservaDTO innerModel ");
		buffer.append("		where " ); 
		buffer.append("			innerModel.reporteSiniestroDTO.idToReporteSiniestro = :idToReporteSiniestro ");
		buffer.append("		and innerModel.autorizada = :autorizada  ");
		buffer.append(")");

		try {
			Query query = entityManager.createQuery(buffer.toString());			
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			query.setParameter("autorizada", autorizada);
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public ReservaDTO obtenerUltimaReservaCreada(BigDecimal idToReporteSiniestro) {
		LogDeMidasEJB3.log("obtenerUltimaReservaCreada", Level.INFO, null);
		StringBuilder buffer = new StringBuilder();
		ReservaDTO reservaDTO = null;
		buffer.append("select ");
		buffer.append("		model ");
		buffer.append("from ");
		buffer.append("		ReservaDTO model ");
		buffer.append("where  ");
		buffer.append("		model.reporteSiniestroDTO.idToReporteSiniestro = :idToReporteSiniestro ");
		buffer.append("Order by  ");
		buffer.append("		model.fechaestimacion desc ");

		try {
			Query query = entityManager.createQuery(buffer.toString());			
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			List listaResultado =  query.getResultList();
			if(listaResultado != null && listaResultado.size() > 0){
				reservaDTO = (ReservaDTO)listaResultado.get(0);
			}
			return reservaDTO;
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenerUltimaReservaCreada failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public ReservaDTO obtenReservaEstimacionInicial(BigDecimal idReporteSiniestro) {
		LogDeMidasEJB3.log("obtenReservaEstimacionInicial Consulta", Level.INFO, null);
		try {
			final String queryString = "select model.idtoreservaestimada from ReservaDTO model" +
									" where  model.reporteSiniestroDTO.idToReporteSiniestro= :idReporteSiniestro" +
									" and model.tipoajuste = 0";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idReporteSiniestro", idReporteSiniestro);
			List resultados = query.getResultList();
			BigDecimal idReserva= new BigDecimal(0);
			for (Object resultado : resultados) {
				idReserva = (BigDecimal) resultado;
			}
			return findById(idReserva);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenDocumentoSiniestro failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ReservaDTO ajustarReservayRiesgosAfectados(List<RiesgoAfectadoDTO> riesgosAfectados, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle){
		ReservaDTO refReservaDTO  = null;
		
		try{
			super.actualizarRiesgosAfectados(riesgosAfectados);
			refReservaDTO = this.save(reserva);			
			super.guardarDetalleDeReserva(refReservaDTO.getIdtoreservaestimada(),reservaDetalle);
		}catch(Exception ex){
			context.setRollbackOnly();
			LogUtil.log("ajustarReservayRiesgosAfectados failed", Level.SEVERE, ex);			
		}

		return refReservaDTO;
	}			
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean ajustarEstimacionInicialyRiesgosAfectados(List<RiesgoAfectadoDTO> riesgosAfectados, List<ReservaDetalleDTO> reservaDetalle){
		boolean result = false;
		
		try{
			super.actualizarRiesgosAfectados(riesgosAfectados);
			super.eliminaDetalleDeReserva(reservaDetalle);
			result = true;
		}catch(Exception ex){
			context.setRollbackOnly();
			LogUtil.log("ajustarReservayRiesgosAfectados failed", Level.SEVERE, ex);	
			return result;
		}

		return result;
	}		
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ReservaDTO ajustarReservaeIndemnizacion(IndemnizacionDTO indemnizacionDTO, List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle){
		ReservaDTO refReservaDTO  = null;
		IndemnizacionDTO refIndemnizacionDTO = null;
		try{
			refReservaDTO = this.save(reserva);			
			super.guardarDetalleDeReserva(refReservaDTO.getIdtoreservaestimada(),reservaDetalle);
			refIndemnizacionDTO = super.guardarIndemnizacion(indemnizacionDTO);
			super.guardarDetalleIndemnizacion(refIndemnizacionDTO, listaDetalleIndemnizacion);
		}catch(Exception ex){
			context.setRollbackOnly();
			LogUtil.log("ajustarReservayRiesgosAfectados failed", Level.SEVERE, ex);			
		}

		return refReservaDTO;
	}	
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ReservaDTO ajustarReservaeModificacionIndemnizacion(IndemnizacionDTO indemnizacionDTO, List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle){
		ReservaDTO refReservaDTO  = null;
		try{
			refReservaDTO = this.save(reserva);			
			super.guardarDetalleDeReserva(refReservaDTO.getIdtoreservaestimada(),reservaDetalle);
			super.modificarIndemnizacion(indemnizacionDTO);
			super.modificarDetalleIndemnizacion(listaDetalleIndemnizacion);
		}catch(Exception ex){
			context.setRollbackOnly();
			LogUtil.log("ajustarReservayRiesgosAfectados failed", Level.SEVERE, ex);			
		}

		return refReservaDTO;
	}	
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ReservaDTO ajustarReservaCancelarIndemnizacion(IndemnizacionDTO indemnizacionDTO, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle){
		ReservaDTO refReservaDTO  = null;
		try{
			refReservaDTO = this.save(reserva);			
			super.guardarDetalleDeReserva(refReservaDTO.getIdtoreservaestimada(),reservaDetalle);
			super.modificarIndemnizacion(indemnizacionDTO);
		}catch(Exception ex){
			context.setRollbackOnly();
			LogUtil.log("ajustarReservayRiesgosAfectados failed", Level.SEVERE, ex);			
		}

		return refReservaDTO;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ReservaDTO ajustarReservaEliminarIndemnizacion(IndemnizacionDTO indemnizacionDTO, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle){
		ReservaDTO refReservaDTO  = null;
		try{
			refReservaDTO = this.save(reserva);			
			super.guardarDetalleDeReserva(refReservaDTO.getIdtoreservaestimada(),reservaDetalle);
			List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion =super.buscarDetalleIndemnizacion(indemnizacionDTO);
			super.eliminarDetalleIndemnizacion(listaDetalleIndemnizacion);
			super.eliminarIndemnizacion(indemnizacionDTO);
		}catch(Exception ex){
			context.setRollbackOnly();
			LogUtil.log("ajustarReservayRiesgosAfectados failed", Level.SEVERE, ex);			
		}

		return refReservaDTO;
	}
}