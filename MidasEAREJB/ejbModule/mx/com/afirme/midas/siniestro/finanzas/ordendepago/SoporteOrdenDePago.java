package mx.com.afirme.midas.siniestro.finanzas.ordendepago;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaFacadeRemote;
import mx.com.afirme.midas.sistema.SystemException;

public class SoporteOrdenDePago {	
    @EJB
    protected AutorizacionTecnicaFacadeRemote autorizacionTecnica;
    @EJB
    protected DetalleOrdenPagoFacadeRemote detalleOrdenDePago;
    @EJB
    protected PolizaFacadeRemote poliza;
    
	protected boolean sonAutorizacionesValidas(List<AutorizacionTecnicaDTO> autorizaciones) throws SystemException{
		boolean sonValidas = Boolean.TRUE;

		List<DetalleOrdenPagoDTO> detalleList;
		double validadorPrestadorDeServicios = 0D;
		double validadorMoneda = 0D;
		PolizaDTO polizaDTO = null;
		for(AutorizacionTecnicaDTO autorizacion: autorizaciones){
			
			validadorPrestadorDeServicios += autorizacion.getGastoSiniestroDTO().getIdTcPrestadorServicios().doubleValue();
			
			detalleList = detalleOrdenDePago.findByProperty("autorizacionTecnicaDTO.idToAutorizacionTecnica", autorizacion.getIdToAutorizacionTecnica());
			
			if (detalleList != null && detalleList.size() > 0) 
				throw new SystemException("La Autorización Técnica No. "
						+ autorizacion.getIdToAutorizacionTecnica()
						+ " se encuentra asignada a la Orden de Pago No. "
						+ detalleList.get(0).getOrdenDePagoDTO()
								.getIdToOrdenPago());

			if(autorizacion.getGastoSiniestroDTO() ==  null )
				throw new SystemException("La Autorización Técnica No. "
						+ autorizacion.getIdToAutorizacionTecnica()
						+ " no esta asociada a un concepto de gastos. "
						+ detalleList.get(0).getOrdenDePagoDTO()
								.getIdToOrdenPago());

			polizaDTO = poliza.findById(autorizacion.getGastoSiniestroDTO().getReporteSiniestroDTO().getNumeroPoliza());
			if(polizaDTO != null)
				validadorMoneda += polizaDTO.getCotizacionDTO().getIdMoneda().doubleValue();
				
		}
		
		if(validadorPrestadorDeServicios/autorizaciones.size() != autorizaciones.get(0).getGastoSiniestroDTO().getIdTcPrestadorServicios().doubleValue())
			throw new SystemException("Solo se deben agrupar autorizaciones "
					+ " del mismo prestador de servicios. ");
		
		if(polizaDTO != null)
			if(validadorMoneda/autorizaciones.size() != polizaDTO.getCotizacionDTO().getIdMoneda().doubleValue())
				throw new SystemException("Solo se deben agrupar autorizaciones "
						+ " de la misma moneda. ");
			
		return sonValidas;
	}

	protected List<AutorizacionTecnicaDTO> cargarAutorizaciones(
			BigDecimal... autorizaciones) {
		List<AutorizacionTecnicaDTO> autorizacionesList = new ArrayList<AutorizacionTecnicaDTO>();
		for (BigDecimal idToAutorizacion : autorizaciones) {
			autorizacionesList.add(autorizacionTecnica
					.findById(idToAutorizacion));
		}
		return autorizacionesList;
	}
}
