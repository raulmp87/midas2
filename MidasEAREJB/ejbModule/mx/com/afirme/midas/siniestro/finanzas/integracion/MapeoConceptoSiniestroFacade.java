package mx.com.afirme.midas.siniestro.finanzas.integracion;
// default package


import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity MapeoConceptoSiniestro.
 * @see .MapeoConceptoSiniestro
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class MapeoConceptoSiniestroFacade  implements MapeoConceptoSiniestroFacadeRemote {
    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved MapeoConceptoSiniestro entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MapeoConceptoSiniestro entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(MapeoConceptoSiniestroDTO entity) {
    	LogDeMidasEJB3.log("saving MapeoConceptoSiniestro instance", Level.INFO, null);
	        try {
	        	entityManager.persist(entity);
	        	LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
	        	LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
	        }
    }
    
    /**
	 Delete a persistent MapeoConceptoSiniestro entity.
	  @param entity MapeoConceptoSiniestro entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MapeoConceptoSiniestroDTO entity) {
    	LogDeMidasEJB3.log("deleting MapeoConceptoSiniestro instance", Level.INFO, null);
	        try {
	        	entity = entityManager.getReference(MapeoConceptoSiniestroDTO.class, entity.getId());
	        	entityManager.remove(entity);
	        	LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
	        	LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
	        }
    }
    
    /**
	 Persist a previously saved MapeoConceptoSiniestro entity and return it or a copy of it to the sender. 
	 A copy of the MapeoConceptoSiniestro entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MapeoConceptoSiniestro entity to update
	 @return MapeoConceptoSiniestro the persisted MapeoConceptoSiniestro entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public MapeoConceptoSiniestroDTO update(MapeoConceptoSiniestroDTO entity) {
    	LogDeMidasEJB3.log("updating MapeoConceptoSiniestro instance", Level.INFO, null);
	        try {
	        	MapeoConceptoSiniestroDTO result = entityManager.merge(entity);
	        	LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
	        } catch (RuntimeException re) {
	        	LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
	        }
    }
    
    public MapeoConceptoSiniestroDTO findById( MapeoConceptoSiniestroId id) {
    	LogDeMidasEJB3.log("finding MapeoConceptoSiniestro instance with id: " + id, Level.INFO, null);
	    try {
	            MapeoConceptoSiniestroDTO instance = entityManager.find(MapeoConceptoSiniestroDTO.class, id);
	            return instance;
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	        throw re;
        }
    }        

/**
	 * Find all MapeoConceptoSiniestro entities with a specific property value.  
	 
	  @param propertyName the name of the MapeoConceptoSiniestro property to query
	  @param value the property value to match
	  	  @return List<MapeoConceptoSiniestro> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<MapeoConceptoSiniestroDTO> findByProperty(String propertyName, final Object value) {
    	LogDeMidasEJB3.log("finding MapeoConceptoSiniestro instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from MapeoConceptoSiniestro model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all MapeoConceptoSiniestro entities.
	  	  @return List<MapeoConceptoSiniestro> all MapeoConceptoSiniestro entities
	 */
	@SuppressWarnings("unchecked")
	public List<MapeoConceptoSiniestroDTO> findAll() {
		LogDeMidasEJB3.log("finding all MapeoConceptoSiniestro instances", Level.INFO, null);
			try {
			final String queryString = "select model from MapeoConceptoSiniestro model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public MapeoConceptoSiniestroDTO getMapeoByIdConcepto(BigDecimal idConcepto, BigDecimal concepto){
		MapeoConceptoSiniestroDTO mapeoConceptoSiniestroDTO = null;
		
    	LogDeMidasEJB3.log("finding MapeoConceptoSiniestro instance with getMapeoByIdConcepto", Level.INFO, null);
		try {
			final String queryString = 
				"select model from MapeoConceptoSiniestroDTO model where model.id.idConcepto = :idConcepto and model.id.concepto = :concepto"; 
		
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idConcepto", idConcepto);
			query.setParameter("concepto", concepto);
			
			List<MapeoConceptoSiniestroDTO> resultSet = query.getResultList();
			
			if(resultSet != null && resultSet.size() > 0){
				mapeoConceptoSiniestroDTO = resultSet.get(0);
			}
			
			return mapeoConceptoSiniestroDTO;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by getMapeoByIdConcepto failed", Level.SEVERE, re);
			throw re;
		}			
	}	
}