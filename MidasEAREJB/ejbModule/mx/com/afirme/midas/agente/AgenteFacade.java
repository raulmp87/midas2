package mx.com.afirme.midas.agente;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity AgenteDTO.
 * 
 * @see .AgenteDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class AgenteFacade implements AgenteFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved AgenteDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            AgenteDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(AgenteDTO entity) {
		LogDeMidasEJB3.log("saving AgenteDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent AgenteDTO entity.
	 * 
	 * @param entity
	 *            AgenteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(AgenteDTO entity) {
		LogDeMidasEJB3.log("deleting AgenteDTO instance", Level.INFO, null);
		try {
			entity = entityManager
					.getReference(AgenteDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved AgenteDTO entity and return it or a copy of it
	 * to the sender. A copy of the AgenteDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            AgenteDTO entity to update
	 * @return AgenteDTO the persisted AgenteDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public AgenteDTO update(AgenteDTO entity) {
		LogDeMidasEJB3.log("updating AgenteDTO instance", Level.INFO, null);
		try {
			AgenteDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public AgenteDTO findById(AgenteDTO id) {
		LogDeMidasEJB3.log("finding AgenteDTO instance with id: " + id,
				Level.INFO, null);
		try {
			AgenteDTO instance = this.findByProperty("idTcAgente", id.getIdTcAgente()).get(0);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all AgenteDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the AgenteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<AgenteDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<AgenteDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding AgenteDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from AgenteDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all AgenteDTO entities.
	 * 
	 * @return List<AgenteDTO> all AgenteDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<AgenteDTO> findAll() {
		LogDeMidasEJB3.log("finding all AgenteDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from AgenteDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public AgenteDTO findById(BigDecimal id) {
		AgenteDTO agenteDTO = new AgenteDTO();
		agenteDTO.setIdTcAgente(id.intValue());
		return findById(agenteDTO);
	}

	public AgenteDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public AgenteDTO findById(double id) {
		AgenteDTO agenteDTO = new AgenteDTO();
		agenteDTO.setIdTcAgente(Integer.parseInt(id + ""));
		return findById(agenteDTO);
	}

	public List<AgenteDTO> listRelated(Object id) {
		return findAll();
	}
	
	@SuppressWarnings("unchecked")
	public List<AgenteDTO> listarAgentesBusqueda(String nombre){
			StringBuffer queryString = new StringBuffer(); 
			queryString.append("SELECT  model.idtcagente, model.nombre ");
			queryString.append(" FROM MIDAS.vnagente model  ");
			queryString.append(" WHERE UPPER (model.nombre) LIKE '%"+nombre.toUpperCase()+"%' ");
								 
			Query query = entityManager.createNativeQuery(queryString.toString());
			Object result  = query.getResultList();
			List<AgenteDTO> list = new ArrayList<AgenteDTO>(1);
			if(result instanceof List)  {
				List<Object> listaResultados = (List<Object>) result;
				BigDecimal idTcAgente = null;
				AgenteDTO agente = null;
				for(Object object : listaResultados){
					Object[] singleResult = (Object[]) object;
					
					if(singleResult[0] instanceof BigDecimal)
						idTcAgente = (BigDecimal)singleResult[0];
					else if (singleResult[0] instanceof Long)
						idTcAgente = BigDecimal.valueOf((Long)singleResult[0]);
					else if (singleResult[0] instanceof Double)
						idTcAgente = BigDecimal.valueOf((Double)singleResult[0]);
					
					agente = findById(idTcAgente);
					list.add(agente);
				}
			}
			return list;
	}
}