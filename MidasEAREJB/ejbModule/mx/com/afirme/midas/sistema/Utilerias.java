/**
 * 
 */
package mx.com.afirme.midas.sistema;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.logging.Level;
import java.math.RoundingMode;
import java.text.DecimalFormat;








import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;

/**
 * @author Christian Ceballos
 * @since 13 de julio de 2009
 */
public final class Utilerias {
	
	public static BigDecimal regresaBigDecimal(String parametro){
		BigDecimal bigDecimal = new BigDecimal(parametro);
		return bigDecimal;
	}
	
	/**
	 * Agrega a una lista un nuevo hashmap con el par formato <clave,valor> que
	 * identifican a las cariables recibidas como parametro
	 * 
	 * @author Christian Ceballos
	 * @since 13 de julio de 2009
	 */
	@SuppressWarnings("unchecked")
	public static String agregaHashLista(List<HashMap> lista, String clave,
			Object valor) {
		if (lista == null || clave == null)
			return null;
		HashMap hash = new HashMap();
		hash.put("PARAMETRO", clave);
		hash.put("VALOR", valor);
		lista.add(hash);
		return null;
	}
	/**
	 * Metodo encargado de armar el string que representa a los parametros
	 * validos para formar el query y ademas los registra en una lista de
	 * HashMap para despues poder asignar sus valores
	 * 
	 * @author Christian Ceballos
	 * @since 13 de julio de 2009
	 * 
	 * @param parametrosValidos
	 * @param parametrosQuery
	 * @param nuevoParametro
	 * @return Un String que representa la condicion por la que vamos a filtrar
	 */
	@SuppressWarnings("unchecked")
	public static String agregaParametroQuery(List<HashMap> parametrosValidos,
			String parametrosQuery, String nuevoParametro,
			Object valorNuevoParametro) {

	
		return agregaParametroQuery(parametrosValidos, parametrosQuery, nuevoParametro, valorNuevoParametro, true);
	}
	
	
	
	/**
	 * Metodo encargado de armar el string que representa a los parametros
	 * validos para formar el query y ademas los registra en una lista de
	 * HashMap para despues poder asignar sus valores
	 * 
	 * @author Christian Ceballos
	 * @since 13 de julio de 2009
	 * 
	 * @param parametrosValidos
	 * @param parametrosQuery
	 * @param nuevoParametro
	 * @return Un String que representa la condicion por la que vamos a filtrar
	 */
	@SuppressWarnings("unchecked")
	public static String agregaParametroQuery(List<HashMap> parametrosValidos,
			String parametrosQuery, String nuevoParametro,
			Object valorNuevoParametro, Boolean incluyeModelo) {
		if (!esAtributoQueryValido(valorNuevoParametro))
			return parametrosQuery;
		if (esAtributoQueryValido(parametrosQuery))
			parametrosQuery = parametrosQuery.concat(" and ");
		if(incluyeModelo){
			parametrosQuery = parametrosQuery.concat("model.").concat(nuevoParametro).concat(" = :");
		}else{
			parametrosQuery = parametrosQuery.concat(nuevoParametro).concat(" = :");
		}
		if (nuevoParametro.indexOf(".")!= -1)
			nuevoParametro = nuevoParametro.substring(0,nuevoParametro.indexOf("."))+nuevoParametro.substring(nuevoParametro.indexOf(".")+1);
		if (nuevoParametro.indexOf(".")!= -1)
			nuevoParametro = nuevoParametro.substring(0,nuevoParametro.indexOf("."))+nuevoParametro.substring(nuevoParametro.indexOf(".")+1);
		if (nuevoParametro.indexOf(".")!= -1)
			nuevoParametro = nuevoParametro.substring(0,nuevoParametro.indexOf("."))+nuevoParametro.substring(nuevoParametro.indexOf(".")+1);
		
		parametrosQuery = parametrosQuery.concat(nuevoParametro);
		agregaHashLista(parametrosValidos, nuevoParametro, valorNuevoParametro);
	
		return parametrosQuery;
	}

	/**
	 * Metodo encargado de armar el string que representa a los parametros
	 * validos para formar el query y ademas los registra en una lista de
	 * HashMap para despues poder asignar sus valores. El operador de
	 * comparaci�n es "like".
	 * 
	 * @author Mario Gonzalez
	 * @since 18 de julio de 2009
	 * 
	 * @param parametrosValidos
	 * @param parametrosQuery
	 * @param nuevoParametro
	 * @return Un String que representa la condicion por la que vamos a filtrar
	 */
	@SuppressWarnings("unchecked")
	public static String agregaParametroQueryLike(
			List<HashMap> parametrosValidos, String parametrosQuery,
			String nuevoParametro, Object valorNuevoParametro) {
		if (!esAtributoQueryValido(valorNuevoParametro))
			return parametrosQuery;
		if (esAtributoQueryValido(parametrosQuery))
			parametrosQuery = parametrosQuery.concat(" and ");
		parametrosQuery = parametrosQuery.concat("UPPER(model.").concat(
				nuevoParametro).concat(") like :");		
		if (nuevoParametro.indexOf(".")!= -1)
			nuevoParametro = nuevoParametro.substring(0,nuevoParametro.indexOf("."))+nuevoParametro.substring(nuevoParametro.indexOf(".")+1);
		if (nuevoParametro.indexOf(".")!= -1)
			nuevoParametro = nuevoParametro.substring(0,nuevoParametro.indexOf("."))+nuevoParametro.substring(nuevoParametro.indexOf(".")+1);
		parametrosQuery = parametrosQuery.concat(nuevoParametro);
		
		String valorLike = "%".concat(valorNuevoParametro.toString().toUpperCase()).concat("%");
		agregaHashLista(parametrosValidos, nuevoParametro, valorLike);

		return parametrosQuery;
	}
	
	/**
	 * Metodo encargado de armar el string que representa a los parametros
	 * validos para formar el query y ademas los registra en una lista de
	 * HashMap para despues poder asignar sus valores.
	 * Igual que agregaParametroQuery pero con un parametro extra ("nombreParametro")
	 * que asigna el nombre del parametro del query, en lugar de asignarlo automaticamente.
	 * 
	 * @author Mario Antonio Gonzalez Galvan
	 * @since 3 de agosto de 2009
	 * 
	 * @param parametrosValidos
	 * @param parametrosQuery
	 * @param nuevoParametro
	 * @param nombreParametro
	 * @return Un String que representa la condicion por la que vamos a filtrar
	 */
	@SuppressWarnings("unchecked")
	public static String agregaParametroQuery(List<HashMap> parametrosValidos,
			String parametrosQuery, String nuevoParametro,
			Object valorNuevoParametro, String nombreParametro) {
		if (!esAtributoQueryValido(valorNuevoParametro))
			return parametrosQuery;
		if (esAtributoQueryValido(parametrosQuery))
			parametrosQuery = parametrosQuery.concat(" and ");
		parametrosQuery = parametrosQuery.concat("model.").concat(nuevoParametro).concat(" = :").concat(nombreParametro);
		agregaHashLista(parametrosValidos, nombreParametro, valorNuevoParametro);
	
		return parametrosQuery;
	}

	/**
	 * Metodo encargado de armar el string que representa a los parametros
	 * validos para formar el query y ademas los registra en una lista de
	 * HashMap para despues poder asignar sus valores. El operador de
	 * comparaci�n es "like".
	 * Igual que agregaParametroQueryLike pero con un parametro extra ("nombreParametro")
	 * que asigna el nombre del parametro del query, en lugar de asignarlo automaticamente.
	 * 
	 * @author Mario Gonzalez
	 * @since 3 de agosto de 2009
	 * 
	 * @param parametrosValidos
	 * @param parametrosQuery
	 * @param nuevoParametro
	 * @param nombreParametro
	 * @return Un String que representa la condicion por la que vamos a filtrar
	 */
	@SuppressWarnings("unchecked")
	public static String agregaParametroQueryLike(
			List<HashMap> parametrosValidos, String parametrosQuery,
			String nuevoParametro, Object valorNuevoParametro, String nombreParametro) {
		if (!esAtributoQueryValido(valorNuevoParametro))
			return parametrosQuery;
		if (esAtributoQueryValido(parametrosQuery))
			parametrosQuery = parametrosQuery.concat(" and ");

		parametrosQuery = parametrosQuery.concat("UPPER(model.").concat(
				nuevoParametro).concat(") like :").concat(nombreParametro);
		String valorLike = "%".concat(valorNuevoParametro.toString().toUpperCase()).concat("%");
		agregaHashLista(parametrosValidos, nombreParametro, valorLike);

		return parametrosQuery;
	}
	
	/**
	 * Comprueba si un objeto es de tipo String
	 * 
	 * @author Christian Ceballos
	 * @since 16 de julio de 2009
	 * @param valor
	 * @return
	 */
	public static boolean esString(Object valor) {
		return (valor instanceof String);
	}

	/**
	 * Establece los parametros a un query mediante una lista que contiene lo
	 * valores validos
	 * 
	 * @author Christian Ceballos
	 * @since 13 de julio de 2009
	 * 
	 * @param query
	 * @param listaParametrosValidos
	 * @return null
	 */
	@SuppressWarnings("unchecked")
	public static String estableceParametrosQuery(Query query,
			List<HashMap> listaParametrosValidos) {
		for (int indiceLista = 0; indiceLista < listaParametrosValidos.size(); indiceLista++) {
			HashMap hash = (HashMap) listaParametrosValidos.get(indiceLista);
			query.setParameter(hash.get("PARAMETRO").toString(), hash
					.get("VALOR"));
		}
		return null;
	}

	/**
	 * Valida si un atributo que se quiere agregar a un query es nulo, numerico
	 * o tiene una longitud de caracteres mayor a cero
	 * 
	 * @author Christian Ceballos
	 * @since 13 de julio de 2009
	 * 
	 * @param atributo
	 * @return un booleano indicando si el atributo que se pasa como parametros
	 *         es valido para un query
	 */
	public static boolean esAtributoQueryValido(Object atributo) {
		if (atributo == null) {
			return false;
		}
		if (esString(atributo)) {
			return atributo.toString().trim().length() > 0;
		}
		return true;
	}
	
	public static boolean fechaEntreRango(Date fechaComparada, Date fechaInicial, Date fechaFinal){
    	boolean respuesta = false;
    	fechaComparada = removerHorasMinutosSegundos(fechaComparada);
    	fechaInicial = removerHorasMinutosSegundos(fechaInicial);
    	fechaFinal = removerHorasMinutosSegundos(fechaFinal);
	 	if (fechaComparada.compareTo(fechaInicial) >= 0 && fechaComparada.compareTo(fechaFinal) <= 0 )  
	 		respuesta = true;
    	return respuesta;
    }
    
    public static Date removerHorasMinutosSegundos(Date fecha){
    	Calendar calendarFechaModificada = Calendar.getInstance();
    	calendarFechaModificada.setTime(fecha);
    	calendarFechaModificada.set(Calendar.HOUR_OF_DAY, 0);
    	calendarFechaModificada.set(Calendar.MINUTE, 0);
    	calendarFechaModificada.set(Calendar.SECOND, 0);
    	calendarFechaModificada.set(Calendar.MILLISECOND, 0);
    	return calendarFechaModificada.getTime();
    }
    
    
    public static Date obtenerFechaDeCadena(String fechaFormateada){
		
    	SimpleDateFormat sdf = new SimpleDateFormat(SistemaPersistencia.FORMATO_FECHA);
    	
    	try {
			return sdf.parse(fechaFormateada.trim());
		} catch (ParseException e) {
			LogDeMidasEJB3.log("Fallo al formatear la fecha " + fechaFormateada + " con el formato " 
					+ SistemaPersistencia.FORMATO_FECHA + ". Se retornara la fecha actual.", Level.WARNING, null);
		}
    	
    	return new Date();	
    }
    
    public static String cadenaDeFecha(Date fecha){
		
    	SimpleDateFormat sdf = new SimpleDateFormat(SistemaPersistencia.FORMATO_FECHA);
    	
    	try {
			return sdf.format(fecha);
		} catch (Exception e) {
			LogDeMidasEJB3.log("Fallo al formatear la fecha " + fecha + " con el formato " 
					+ SistemaPersistencia.FORMATO_FECHA + ". Se retornara la fecha actual.", Level.WARNING, null);
		}
    	
    	return fecha.toString();	
    }    
    
    public static String cadenaDeFecha(Date fecha,String formato){
    	if(formato != null) {
    		SimpleDateFormat sdf = new SimpleDateFormat(formato);
    		return sdf.format(fecha);
    	}else{
    		return cadenaDeFecha(fecha);
    	}
    }
    
    public static Date getDateFromString(String fecha, String formato){
    	if(formato != null){
    		try{
        		SimpleDateFormat sdf = new SimpleDateFormat(formato);
        		return sdf.parse(fecha.trim());	
    		}catch(ParseException e){
    			return new Date();
    		}
    	}else{
    		return obtenerFechaDeCadena(fecha);
    	}
    }
    
    
    public enum FormaPago{
    	MENSUAL(0){
			public int obtenerUltimaSuscripcionAnual() {
				return 12;
			}
    	},
    	TRIMESTRAL(1){
			public int obtenerUltimaSuscripcionAnual() {
				return 4;
			}
    	},SEMESTRAL(2){
			public int obtenerUltimaSuscripcionAnual() {
				return 2;
			}
    	},ANUAL(3){
			public int obtenerUltimaSuscripcionAnual() {
				return 1;
			}
    	};
    	
    	int idFormaPago;
		FormaPago(int idFormaPago){
			this.idFormaPago = idFormaPago;
		}
		public abstract int obtenerUltimaSuscripcionAnual();
    }
    
    public static FormaPago obtenerFormaPagoContratos(int idFormaPago){
    	FormaPago formaPago = null;
    	switch(idFormaPago){
	    	case 0:
	    		formaPago = FormaPago.MENSUAL;
	    		break;
	    	case 1:
	    		formaPago = FormaPago.TRIMESTRAL;
	    		break;
	    	case 2:
	    		formaPago = FormaPago.SEMESTRAL;
	    		break;
	    	case 3:
	    		formaPago = FormaPago.ANUAL;
	    		break;
    	}
    	return formaPago;
    }
    
    public static BigDecimal obtenerBigDecimal(Object valor){
		BigDecimal resultado = null;
		if(valor != null){
			if(valor instanceof BigDecimal)
				resultado = (BigDecimal) valor;
			else if(valor instanceof Integer)
				resultado = new BigDecimal ((Integer) valor);
			else if(valor instanceof Float)
				resultado = new BigDecimal ((Float) valor);
			else if(valor instanceof Double)
				resultado = new BigDecimal ((Double) valor);
			else if(valor instanceof Long)
				resultado = new BigDecimal ((Long) valor);
			else{
				try{
					resultado = new BigDecimal (valor.toString());
				}catch(Exception e){}
			}
		}
		return resultado;
	}
    
    public static Long obtenerLong(Object valor){
		Long resultado = null;
		if(valor != null){
			if(valor instanceof BigDecimal)
				resultado = ((BigDecimal) valor).longValue();
			else if(valor instanceof Integer)
				resultado = new Long ((Integer) valor);
			else if(valor instanceof Float)
				resultado = ((Float) valor).longValue();
			else if(valor instanceof Double)
				resultado = ((Double) valor).longValue();
			else if(valor instanceof Long)
				resultado = (Long) valor;
			else{
				try{
					resultado = new Long (valor.toString());
				}catch(Exception e){}
			}
		}
		return resultado;
	}
    
    public static Integer obtenerInteger(Object valor){
		Integer resultado = null;
		if(valor != null){
			if(valor instanceof BigDecimal)
				resultado = ((BigDecimal) valor).intValue();
			else if(valor instanceof Integer)
				resultado = ((Integer) valor);
			else if(valor instanceof Float)
				resultado = ((Float) valor).intValue();
			else if(valor instanceof Double)
				resultado = ((Double) valor).intValue();
			else if(valor instanceof Long)
				resultado = ((Long) valor).intValue();
			else{
				try{
					resultado = new Integer(valor.toString());
				}catch(Exception e){}
			}
		}
		return resultado;
	}
    
    public static Date obtenerDate(Object valor){
		Date resultado = null;
		if(valor != null){
			if(valor instanceof Date)
				resultado = ((Date) valor);
			else if(valor instanceof java.sql.Date)
				resultado = new Date(((java.sql.Date) valor).getTime());
		}
		return resultado;
	}
    
    public static String obtenerNumeroPoliza(PolizaDTO polizaDTO){
    	String numeroPoliza = "";

    	if(polizaDTO != null){
			numeroPoliza += polizaDTO.getCodigoProducto();
			numeroPoliza += polizaDTO.getCodigoTipoPoliza();
			numeroPoliza += "-";
			numeroPoliza += llenarIzquierda(polizaDTO.getNumeroPoliza().toString(),
					"0", 6);
			numeroPoliza += "-";
			numeroPoliza += llenarIzquierda(polizaDTO.getNumeroRenovacion().toString(), "0", 2);
    	}
		return numeroPoliza;
    }
    
    public static String llenarIzquierda(String original, String caracter,int cantidadTotal) {
		StringBuilder modificado = new StringBuilder("");
		for (int i = 0; i < cantidadTotal - original.length(); i++) {
			modificado.append(caracter);
		}
		modificado.append(original);
		return modificado.toString();
	}
    
    public static String calcularDescripcionTipoEndoso(EndosoDTO endosoDTO){
    	String descripcionEndoso = "";
    	switch(endosoDTO.getClaveTipoEndoso().shortValue()){
    		case SistemaPersistencia.TIPO_ENDOSO_AUMENTO:
    			descripcionEndoso = "Aumento";
    			break;
    		case SistemaPersistencia.TIPO_ENDOSO_DISMINUCION:
    			descripcionEndoso = "Disminuci�n";
    			break;
    		case SistemaPersistencia.TIPO_ENDOSO_CAMBIO_DATOS:
    			descripcionEndoso = "Cambio de datos";
    			break;
    		case SistemaPersistencia.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO:
    			descripcionEndoso = "Cambio de forma de pago";
    			break;
    		case SistemaPersistencia.TIPO_ENDOSO_CANCELACION:
    			descripcionEndoso = "Cancelaci�n";
    			break;
    		case SistemaPersistencia.TIPO_ENDOSO_CE:
    			descripcionEndoso = "Cancelaci�n de endoso";
    			break;
    		case SistemaPersistencia.TIPO_ENDOSO_RE:
    			descripcionEndoso = "Rehabilitaci�n de endoso";
    			break;
    		case SistemaPersistencia.TIPO_ENDOSO_REHABILITACION:
    			descripcionEndoso = "Rehabilitaci�n";
    			break;
    			
    	}
    	return descripcionEndoso;
    }
    
    public static String obtenerDescripcionMoneda(int idMoneda){
		 String moneda = null;
		 if(idMoneda == MonedaDTO.MONEDA_DOLARES)
			 moneda = "DOLARES";
		 else if((idMoneda == MonedaDTO.MONEDA_PESOS))
			 moneda = "PESOS";
		 else
			 moneda = "";
		 
		 return moneda;
	}
    
    /**
	 * Calcula la diferencia de dias entre 2 fechas y
	 * Obtiene el factor de esas fechas
	 * el factor es obtenido en base a un anio 365 dias
	 * @param Date fechaInicial
	 * @param Date fechaFinal
	 * @return double factor
	 */	
	public static BigDecimal getFactorVigencia(Date fechaInicial, Date fechaFinal) {
		BigDecimal factorVigencia = BigDecimal.ZERO;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        
        try{
            fechaInicial = sdf.parse(sdf.format(fechaInicial));
            fechaFinal = sdf.parse(sdf.format(fechaFinal));
        }catch(Exception e){}

        long diferencia = fechaFinal.getTime()- fechaInicial.getTime();
        
        double diferenciaEnDias = (double)diferencia / (1000 * 60 * 60 * 24);

		factorVigencia = BigDecimal.valueOf(diferenciaEnDias /365);
	  	
		return factorVigencia;
	}
	
	public static double obtenerDiasEntreFechas(Date fechaInicial, Date fechaFinal){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        
        try{
            fechaInicial = sdf.parse(sdf.format(fechaInicial));
            fechaFinal = sdf.parse(sdf.format(fechaFinal));
        }catch(Exception e){}

		long diferencia = fechaFinal.getTime()- fechaInicial.getTime();
		
		double dias = (double)diferencia / (1000 * 60 * 60 * 24);
		
		return dias;
		
	}
	
	/**
	 * Obtiene un mensaje identificado por una clave de un archivo de recursos
	 * 
	 * @param recurso
	 * @param clave
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static synchronized String getMensajeRecurso(String recurso,String clave) {
		ResourceBundle objlResourceBundle = getRecurso(recurso);
		Enumeration objlEnum = null;
		boolean blExist = false;
		String slMessage = null;
		for (objlEnum = objlResourceBundle.getKeys(); objlEnum!= null && objlEnum.hasMoreElements() && !blExist;) {
			if (clave.equalsIgnoreCase((String) objlEnum.nextElement())) {
				blExist = true;
			}
		}

		if (blExist) {
			slMessage = objlResourceBundle.getString(clave);
		}
		return slMessage;
	}
	
	/**
	 * Regresa la cadena de texto referida por el key en el archivo de recursos
	 * 
	 * @param recurso
	 *            nombre del archivo de recursos
	 * @param key
	 *            Clave del texto en el archivo de recursos
	 * @param params
	 *            Argumentos del texto si los hay
	 * @return Texto definido en el archivo de recursos
	 */
	public static String getMensajeRecurso(String recurso, String key,Object... params) {
		ResourceBundle resourceBundle = getRecurso(recurso);
		String strTextL = "";
		try {
			strTextL = resourceBundle.getString(key);
			if (params != null && params.length > 0) {
				MessageFormat messageFormat = new MessageFormat(strTextL);
				strTextL = messageFormat.format(params);
			}
		} catch (MissingResourceException e) {
			e.printStackTrace();
		}
		return strTextL;
	}
	
	
	/**
	 * Agrega un numero de a�os determinado a una Fecha
	 * @param fechaOrigen Fecha original
	 * @param numeroAnios Numero de a�os a agregar
	 * @return La nueva fecha con los a�os agregados
	 */
	public static Date agregaAniosAFecha(Date fechaOrigen, int numeroAnios) {
		return calculaNuevaFecha(fechaOrigen, Calendar.YEAR, numeroAnios);
	}
	
	public static Date agregaDiasAFecha(Date fechaOrigen, int numeroDias) {
		return calculaNuevaFecha(fechaOrigen, Calendar.DAY_OF_YEAR, numeroDias);
	}	
	
	
	private static Date calculaNuevaFecha (Date fechaOrigen, int tipoElementoFecha,  int unidadesAgregadas) {
		
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(fechaOrigen);
				
		cal.add(tipoElementoFecha, unidadesAgregadas);
		
		return cal.getTime();
		
	}
	
	

	/**
	 * Obtiene un archivo de recursos
	 * 
	 * @param recurso
	 * @return
	 */
	private static synchronized ResourceBundle getRecurso(String recurso) {
		ResourceBundle objlResource = null;
		objlResource = ResourceBundle.getBundle(recurso);
		return objlResource;
	}
	//********************************************
	private static final String[] UNIDADES = { "", "UN ", "DOS ", "TRES ",
           "CUATRO ", "CINCO ", "SEIS ", "SIETE ", "OCHO ", "NUEVE ", "DIEZ ",
           "ONCE ", "DOCE ", "TRECE ", "CATORCE ", "QUINCE ", "DIECISEIS",
           "DIECISIETE", "DIECIOCHO", "DIECINUEVE", "VEINTE" };

   private static final String[] DECENAS = { "VENTI", "TREINTA ", "CUARENTA ",
           "CINCUENTA ", "SESENTA ", "SETENTA ", "OCHENTA ", "NOVENTA ",
           "CIEN " };

   private static final String[] CENTENAS = { "CIENTO ", "DOSCIENTOS ",
           "TRESCIENTOS ", "CUATROCIENTOS ", "QUINIENTOS ", "SEISCIENTOS ",
           "SETECIENTOS ", "OCHOCIENTOS ", "NOVECIENTOS " };

   /**
    * Convierte a letras un numero de la forma $123,456.32
    * 
    * @param number
    *            Numero en representacion texto
    * @throws NumberFormatException
    *             Si valor del numero no es valido (fuera de rango o )
    * @return Numero en letras
    */
   public static String convertNumberToLetter(String number) throws NumberFormatException {
       
	   return convertNumberToLetter(Double.parseDouble(number));
	   
   }

   /**
    * Convierte un numero en representacion numerica a uno en representacion de
    * texto. El numero es valido si esta entre 0 y 999'999.999
    * 
    * @param number
    *            Numero a convertir
    * @return Numero convertido a texto
    * @throws NumberFormatException
    *             Si el numero esta fuera del rango
    */
   public static String convertNumberToLetter(double doubleNumber)
           throws NumberFormatException {

       StringBuilder converted = new StringBuilder();

       String patternThreeDecimalPoints = "#.###";

       DecimalFormat format = new DecimalFormat(patternThreeDecimalPoints);
       format.setRoundingMode(RoundingMode.DOWN);

       // formateamos el numero, para ajustarlo a el formato de tres puntos
       // decimales
       String formatedDouble = format.format(doubleNumber);
       doubleNumber = Double.parseDouble(formatedDouble);

       // Validamos que sea un numero legal
       if (doubleNumber > 999999999)
           throw new NumberFormatException(
                   "El numero es mayor de 999'999.999, "
                           + "no es posible convertirlo");

       if (doubleNumber < 0)
           throw new NumberFormatException("El numero debe ser positivo");

       String splitNumber[] = String.valueOf(doubleNumber).replace('.', '#')
               .split("#");

       // Descompone el trio de millones
       int millon = Integer.parseInt(String.valueOf(getDigitAt(splitNumber[0],
               8))
               + String.valueOf(getDigitAt(splitNumber[0], 7))
               + String.valueOf(getDigitAt(splitNumber[0], 6)));
       if (millon == 1)
           converted.append("UN MILLON ");
       else if (millon > 1)
           converted.append(convertNumber(String.valueOf(millon))
                   + "MILLONES ");

       // Descompone el trio de miles
       int miles = Integer.parseInt(String.valueOf(getDigitAt(splitNumber[0],
               5))
               + String.valueOf(getDigitAt(splitNumber[0], 4))
               + String.valueOf(getDigitAt(splitNumber[0], 3)));
       if (miles == 1)
           converted.append("MIL ");
       else if (miles > 1)
           converted.append(convertNumber(String.valueOf(miles)) + "MIL ");

       // Descompone el ultimo trio de unidades
       int cientos = Integer.parseInt(String.valueOf(getDigitAt(
               splitNumber[0], 2))
               + String.valueOf(getDigitAt(splitNumber[0], 1))
               + String.valueOf(getDigitAt(splitNumber[0], 0)));
       if (cientos == 1)
           converted.append("UN");

       if (millon + miles + cientos == 0)
           converted.append("CERO");
       if (cientos > 1)
           converted.append(convertNumber(String.valueOf(cientos)));
       // Descompone los centavos
       int centavos = Integer.parseInt(String.valueOf(getDigitAt(
               splitNumber[1], 2))
               + String.valueOf(getDigitAt(splitNumber[1], 1))
               + String.valueOf(getDigitAt(splitNumber[1], 0)));
//       if (centavos == 1)
//           converted.append(" CON UN CENTAVO");
//       else if (centavos > 1)
//           converted.append(" CON " + convertNumber(String.valueOf(centavos))
//                   + "CENTAVOS");
       if(centavos!=0){
    	   converted.append(" PESOS "+centavos+"/100");
       }else{
    	   converted.append(" PESOS 0/100");
       }
       converted.append(" M.N.");
       return converted.toString();
   }

   public static String convertNumberToLetterPorciento(double doubleNumber)
   throws NumberFormatException {

StringBuilder converted = new StringBuilder();

String patternThreeDecimalPoints = "#.###";

DecimalFormat format = new DecimalFormat(patternThreeDecimalPoints);
format.setRoundingMode(RoundingMode.DOWN);

// formateamos el numero, para ajustarlo a el formato de tres puntos
// decimales
String formatedDouble = format.format(doubleNumber);
doubleNumber = Double.parseDouble(formatedDouble);

// Validamos que sea un numero legal
if (doubleNumber > 999999999)
   throw new NumberFormatException(
           "El numero es mayor de 999'999.999, "
                   + "no es posible convertirlo");

if (doubleNumber < 0)
   throw new NumberFormatException("El numero debe ser positivo");

String splitNumber[] = String.valueOf(doubleNumber).replace('.', '#')
       .split("#");

// Descompone el trio de millones
int millon = Integer.parseInt(String.valueOf(getDigitAt(splitNumber[0],
       8))
       + String.valueOf(getDigitAt(splitNumber[0], 7))
       + String.valueOf(getDigitAt(splitNumber[0], 6)));
if (millon == 1)
   converted.append("UN MILLON ");
else if (millon > 1)
   converted.append(convertNumber(String.valueOf(millon))
           + "MILLONES ");

// Descompone el trio de miles
int miles = Integer.parseInt(String.valueOf(getDigitAt(splitNumber[0],
       5))
       + String.valueOf(getDigitAt(splitNumber[0], 4))
       + String.valueOf(getDigitAt(splitNumber[0], 3)));
if (miles == 1)
   converted.append("MIL ");
else if (miles > 1)
   converted.append(convertNumber(String.valueOf(miles)) + "MIL ");

// Descompone el ultimo trio de unidades
int cientos = Integer.parseInt(String.valueOf(getDigitAt(
       splitNumber[0], 2))
       + String.valueOf(getDigitAt(splitNumber[0], 1))
       + String.valueOf(getDigitAt(splitNumber[0], 0)));
if (cientos == 1)
   converted.append("UN");

if (millon + miles + cientos == 0)
   converted.append("CERO");
if (cientos > 1)
   converted.append(convertNumber(String.valueOf(cientos)));

converted.append(" PORCIENTO");

// Descompone los centavos
//int centavos = Integer.parseInt(String.valueOf(getDigitAt(
//       splitNumber[1], 2))
//       + String.valueOf(getDigitAt(splitNumber[1], 1))
//       + String.valueOf(getDigitAt(splitNumber[1], 0)));
////if (centavos == 1)
////   converted.append(" CON UN CENTAVO");
////else if (centavos > 1)
////   converted.append(" CON " + convertNumber(String.valueOf(centavos))
////           + "CENTAVOS");
//if(centavos!=0){
//   converted.append(" "+centavos+"/100");
//}else{
//   converted.append(" 0/100");
//}
return converted.toString();
}

   /**
    * Convierte los trios de numeros que componen las unidades, las decenas y
    * las centenas del numero.
    * 
    * @param number
    *            Numero a convetir en digitos
    * @return Numero convertido en letras
    */
   private static String convertNumber(String number) {

       if (number.length() > 3)
           throw new NumberFormatException(
                   "La longitud maxima debe ser 3 digitos");

       // Caso especial con el 100
       if (number.equals("100")) {
           return "CIEN";
       }

       StringBuilder output = new StringBuilder();
       if (getDigitAt(number, 2) != 0)
           output.append(CENTENAS[getDigitAt(number, 2) - 1]);

       int k = Integer.parseInt(String.valueOf(getDigitAt(number, 1))
               + String.valueOf(getDigitAt(number, 0)));

       if (k <= 20)
           output.append(UNIDADES[k]);
       else if (k > 30 && getDigitAt(number, 0) != 0)
           output.append(DECENAS[getDigitAt(number, 1) - 2] + "Y "
                   + UNIDADES[getDigitAt(number, 0)]);
       else
           output.append(DECENAS[getDigitAt(number, 1) - 2]
                   + UNIDADES[getDigitAt(number, 0)]);

       return output.toString();
   }

   /**
    * Retorna el digito numerico en la posicion indicada de derecha a izquierda
    * 
    * @param origin
    *            Cadena en la cual se busca el digito
    * @param position
    *            Posicion de derecha a izquierda a retornar
    * @return Digito ubicado en la posicion indicada
    */
   private static int getDigitAt(String origin, int position) {
       if (origin.length() > position && position >= 0)
           return origin.charAt(origin.length() - position - 1) - 48;
       return 0;
   }
   
   public static String formatoMoneda(Double cantidad) {
		NumberFormat formateador = new DecimalFormat("$#,##0.00");
		return cantidad != null ? formateador.format(cantidad) : null;
	}
	
	public static String formatoMoneda(BigDecimal cantidad) {
		if (cantidad != null) {
			return formatoMoneda(cantidad.doubleValue());
		} else {
			return null;
		}
	}
	
	/**
	 * Canviar encode a UFT-8 de un string.
	 * @param texto
	 * @return
	 */
	public static String convertToUTF8(String text) {
        String textNew = null;
        try {
        	if(text!=null && !text.isEmpty()){
        		textNew = new String(text.getBytes("ISO-8859-1"), "UTF-8");
        	}
        } catch (java.io.UnsupportedEncodingException e) {
            return textNew;
        }
        
        return textNew;
    }

	public static String withoutCRLF(File file){
		String inoneLine = "";
		FileInputStream fis = null;
		try {   
			StringBuilder strbuilder = new StringBuilder();
			fis = new FileInputStream(file);		    
		    InputStreamReader inputStreamReader = new InputStreamReader((InputStream)fis);
		    BufferedReader br = new BufferedReader(inputStreamReader);
		    String line = null;

		    while ((line = br.readLine()) != null) {
		    	strbuilder.append(line);
		    }   
		    
		    inoneLine = strbuilder.toString();
		    
		    //se cambia el tipo de encoding para corregir error del sello digital.
		    inoneLine = new String(inoneLine.getBytes(), "UTF-8");
		    		   
		} catch(FileNotFoundException e) {
			System.err.print(e + "\n");
		} catch (Exception e) {
			System.err.print(e + "\n");
		} finally {
			if(fis != null) { 
				try {
					fis.close();
				} catch(Exception e ) {
					System.err.print(e + "\n");
				}
			}
		}
		
		return ((inoneLine == null) ? "" : inoneLine);
	}
}
