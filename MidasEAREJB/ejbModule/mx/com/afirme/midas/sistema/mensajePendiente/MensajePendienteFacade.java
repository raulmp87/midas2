package mx.com.afirme.midas.sistema.mensajePendiente;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.mensajePendiente.MensajePendienteDTO;

/**
 * Facade for entity MensajePendienteDTO.
 * 
 * @see .MensajePendienteDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class MensajePendienteFacade implements MensajePendienteFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved MensajePendienteDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            MensajePendienteDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(MensajePendienteDTO entity) {
		LogDeMidasEJB3.log("saving MensajePendienteDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent MensajePendienteDTO entity.
	 * 
	 * @param entity
	 *            MensajePendienteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(MensajePendienteDTO entity) {
		LogDeMidasEJB3.log("deleting MensajePendienteDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(MensajePendienteDTO.class,
					entity.getIdToMensajePendiente());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved MensajePendienteDTO entity and return it or a
	 * copy of it to the sender. A copy of the MensajePendienteDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            MensajePendienteDTO entity to update
	 * @return MensajePendienteDTO the persisted MensajePendienteDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public MensajePendienteDTO update(MensajePendienteDTO entity) {
		LogDeMidasEJB3.log("updating MensajePendienteDTO instance", Level.INFO,
				null);
		try {
			MensajePendienteDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public MensajePendienteDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding MensajePendienteDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			MensajePendienteDTO instance = entityManager.find(
					MensajePendienteDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all MensajePendienteDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the MensajePendienteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<MensajePendienteDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<MensajePendienteDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding MensajePendienteDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from MensajePendienteDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all MensajePendienteDTO entities.
	 * 
	 * @return List<MensajePendienteDTO> all MensajePendienteDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<MensajePendienteDTO> findAll() {
		LogDeMidasEJB3.log("finding all MensajePendienteDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from MensajePendienteDTO model";
			Query query = entityManager.createQuery(queryString);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<MensajePendienteDTO> listarPendientesDanios(String idsRoles) {
		LogDeMidasEJB3.log("finding MensajePendienteDTO instances with idRol: "
				+ idsRoles + " and estatus: 0 and modulo: 0", Level.INFO, null);
		try {
			final String queryString = "select model from MensajePendienteDTO model where model.idRol in ("
					+ idsRoles + ") and model.estatus = 0 and model.modulo = 0";
			Query query = entityManager.createQuery(queryString);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarPendientesDanios failed", Level.SEVERE,
					re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<MensajePendienteDTO> listarPendientesReaseguro(String idsRoles) {
		LogDeMidasEJB3.log("finding MensajePendienteDTO instances with idRol: "
				+ idsRoles + " and estatus: 0 and modulo: 1", Level.INFO, null);
		try {
			final String queryString = "select model from MensajePendienteDTO model where model.idRol in ("
					+ idsRoles + ") and model.estatus = 0 and model.modulo = 1";
			Query query = entityManager.createQuery(queryString);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarPendientesReaseguro failed",
					Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<MensajePendienteDTO> listarPendientesSiniestros(String idsRoles) {
		LogDeMidasEJB3.log("finding MensajePendienteDTO instances with idRol: "
				+ idsRoles + " and estatus: 0 and modulo: 2", Level.INFO, null);
		try {
			final String queryString = "select model from MensajePendienteDTO model where model.idRol in ("
					+ idsRoles + ") and model.estatus = 0 and model.modulo = 2";
			Query query = entityManager.createQuery(queryString);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarPendientesSiniestros failed",
					Level.SEVERE, re);
			throw re;
		}
	}
}