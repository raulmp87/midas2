/**
 *
 */
package mx.com.afirme.midas.sistema;

import org.xml.sax.SAXException;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;

/**
 * With this class you can read a valid scheme of XML input.
 * And create any number of  XMLNodeReader instances for each node-name
 * that exists in the XML input.
 *
 * @author 0jsevrod <p>Jos&eacute; Miguel Sevilla rodr&iacut;guez</p>
 * @since 2017-08-08 <p>year-month-day</p>
 *
 * <address>
 *     <a href="mailto:0jose.sevilla@afirme.com>e-mail</a>
 * </address>
 */
public class XMLNodeReader{

    private static final String UTF8     = "UTF-8";

    private String  name             = "";
    private String  attribute        = "";
    private String  textBetweenTags  = "";
    private String  valueOfAttribute = "";
    private String  fileName         = "";
    private String  encoding         = UTF8;
    private String  warning         = "This is the last node read.";
    private File    xmlTmpFile           = null;
    private Boolean wasCreatedXmltmpFile =  Boolean.FALSE;
    
    public XMLNodeReader(){
        super();
    }
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((attribute == null) ? 0 : attribute.hashCode());
		result = prime * result
				+ ((encoding == null) ? 0 : encoding.hashCode());
		result = prime * result
				+ ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((textBetweenTags == null) ? 0 : textBetweenTags.hashCode());
		result = prime
				* result
				+ ((valueOfAttribute == null) ? 0 : valueOfAttribute.hashCode());
		result = prime * result + ((warning == null) ? 0 : warning.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XMLNodeReader other = (XMLNodeReader) obj;
		if (attribute == null) {
			if (other.attribute != null)
				return false;
		} else if (!attribute.equals(other.attribute))
			return false;
		if (encoding == null) {
			if (other.encoding != null)
				return false;
		} else if (!encoding.equals(other.encoding))
			return false;
		if (fileName == null) {
			if (other.fileName != null)
				return false;
		} else if (!fileName.equals(other.fileName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (textBetweenTags == null) {
			if (other.textBetweenTags != null)
				return false;
		} else if (!textBetweenTags.equals(other.textBetweenTags))
			return false;
		if (valueOfAttribute == null) {
			if (other.valueOfAttribute != null)
				return false;
		} else if (!valueOfAttribute.equals(other.valueOfAttribute))
			return false;
		if (warning == null) {
			if (other.warning != null)
				return false;
		} else if (!warning.equals(other.warning))
			return false;
		return true;
	}
    
    @Override
    public String toString() {
        return "XMLNodeReader{" +
                "name='" + name + '\'' +
                ", attribute='" + attribute + '\'' +
                ", textBetweenTags='" + textBetweenTags + '\'' +
                ", valueOfAttribute='" + valueOfAttribute + '\'' +
                ", fileName='" + fileName + '\'' +
                ", encoding='" + encoding + '\'' +
                ", warning='" + warning + '\'' +
                ", wasCreatedXmltmpFile='" + wasCreatedXmltmpFile + '\'' +
                '}';
    }

    public File getXmlTmpFile() {
	    return xmlTmpFile;
	}
    
    public void setXmlTmpFile(File xmlTmpFile) {
       this.xmlTmpFile = xmlTmpFile;
																										 
    }

    public Boolean getWasCreatedXmltmpFile() {
        return wasCreatedXmltmpFile;
    }

    public void setWasCreatedXmltmpFile(Boolean wasCreatedXmltmpFile) {
        this.wasCreatedXmltmpFile = wasCreatedXmltmpFile;
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTagText() {
        return this.textBetweenTags;
    }

    public String getNodeText(){
        return this.textBetweenTags;
    }

    public void setNodeText(String text){
        this.textBetweenTags = text;
    }
    public void setTagText(String text){
        this.textBetweenTags = textBetweenTags;
    }

    public String getAttributeValue(){
        return this.valueOfAttribute;
    }

    public void setAttributeValue(String value){
        this.valueOfAttribute = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }

    public static class Document {

    	public static XMLNodeReader xmlnr = null;

        public Document(XMLNodeReader xmlnr) {
            super();
            this.xmlnr = xmlnr;
        }

        private static XMLNodeReader read(String fileName, String nodeOrTagName, String attributeName) throws Exception {
            if(!theNameEndsWithDotxml(fileName)){
                String name = "XMLNodeReader.Document.read(java.lio.File, java.lang.String, java.lang.String)";
                throw new Exception(String.format("Try with the instance method %s", name));
            }
            xmlnr.setFileName(fileName);
            process(convert(toFileInputStream(fileName)), nodeOrTagName, attributeName);
            return xmlnr;
        }

        public static XMLNodeReader read(File file, String nodeOrTagName, String attributeName) {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                System.err.println(e);
            }
            xmlnr.setFileName(file.getAbsolutePath());
            if(!theNameEndsWithDotxml(file.getAbsolutePath())){
                System.out.println("Trying to convert the instance method parameter named file.");
                fis = createXmlFile(file);
            }

            process(convert(fis), nodeOrTagName, attributeName);
            if(xmlnr.getWasCreatedXmltmpFile() && xmlnr.getXmlTmpFile().exists()){
                xmlnr.getXmlTmpFile().deleteOnExit();
            }
            return xmlnr;
        }

        private static FileInputStream toFileInputStream(Object object){
            FileInputStream fis = null;
            try {
                if(object instanceof String){
                    fis = new FileInputStream(new File(String.valueOf(object)));
                } else if(object instanceof File){
                    fis = new FileInputStream(((File) object));
                }
            } catch(FileNotFoundException e) {
                System.err.println(e);
            }
            return fis;
        }

        private static InputSource convert(FileInputStream fistream){
            Reader r = new InputStreamReader(fistream);
            InputSource isrc = new InputSource(r);

            isrc.setEncoding("UTF-8");
            return isrc;
        }

        private static Boolean theNameEndsWithDotxml(String name) {
            if(name == null){
                System.err.println("No file.");
            }
            return name.endsWith(".xml");
        }

        private static FileInputStream createXmlFile(File srcFile){
            File newXmllFile = new File("xmlfile" + Math.random() + ".xml");
            newXmllFile.setReadable(true);
            newXmllFile.setWritable(true);
            BufferedWriter bffw = null;
            BufferedReader bffr = null;
            try {
            	newXmllFile.createNewFile();
                bffr = new BufferedReader(new FileReader(srcFile));
                bffw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newXmllFile), xmlnr.encoding));
                String line = "";
                while((line = bffr.readLine()) != null){
                    bffw.write(line);
                    bffw.append('\n');
                }

                bffw.flush();
                bffw.close();
                bffr.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if(bffw != null) { try { bffw.flush(); bffw.close(); } catch(IOException e) { System.out.println(e); } }
                if(bffr != null) { try { bffr.close(); } catch(IOException e) { System.out.println(e); } }
            }

            System.out.println("The new path-file-name is ");
            System.out.println(newXmllFile.getAbsolutePath());
            xmlnr.setFileName(newXmllFile.getAbsolutePath());
            FileInputStream fos = null;
            try{
                fos = new FileInputStream(newXmllFile);
                xmlnr.setXmlTmpFile(newXmllFile);
                xmlnr.setWasCreatedXmltmpFile(Boolean.TRUE);
            } catch (IOException e) {
                System.err.println(e);
            }
            return fos;
        }

        protected static void process(InputSource isrc, final String nodeOrTagName, final String attributeName){
            try {

                SAXParserFactory factory = SAXParserFactory.newInstance();
                SAXParser saxParser = factory.newSAXParser();

                DefaultHandler defhandler = new DefaultHandler() {

                    boolean foundNodeName = false;

                    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

                        if (qName.equalsIgnoreCase(nodeOrTagName)) {
                            xmlnr.setName(nodeOrTagName);
                            xmlnr.setAttribute(attributeName);
                            xmlnr.setAttributeValue(attributes.getValue(attributeName));
                            foundNodeName = true;
                        }
                    }

                    public void endElement(String uri, String localName, String qName) throws SAXException {
                    }

                    public void characters(char ch[], int start, int length) throws SAXException {

                        if (foundNodeName) {
                            xmlnr.setTagText(new String(ch));
                            foundNodeName = false;
                        }
                    }
                };

                saxParser.parse(isrc, defhandler);
            } catch(SAXException e){ System.err.println(e.getMessage());}
              catch(ParserConfigurationException pe){ System.err.println(pe.getMessage());}
              catch(IOException ioe) { System.err.println(ioe.getMessage());}
        }
    }

}
