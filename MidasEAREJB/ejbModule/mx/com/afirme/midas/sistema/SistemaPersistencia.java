package mx.com.afirme.midas.sistema;

import java.math.BigDecimal;

import mx.com.afirme.midas.sistema.entorno.Entorno;

public class SistemaPersistencia {

	public static final String ESTATUS_DE_COTIZACIONES = "(10, 11, 13, 14, 15, 16, 18, 17, 19) ";
	public static final String SOLICITUD_ENDOSO = "2";
	public static final String SOLICITUD_POLIZA = "0";
	public static final Short CLAVE_SOLICITUD_ENDOSO = (short) 2;
	public static final Short CLAVE_SOLICITUD_POLIZA = (short) 1;
	public static final Short SOLICITUD_EN_PROCESO_AUTORIZACION = (short) 3;
	public static final Short COTIZACION_ASIGNADA = new Short("11");
	public static final Short COTIZACION_EN_PROCESO = new Short("10");
	public static final Short ESTATUS_COT_EMITIDA = new Short("16");	
	public static final Short COTIZACION_EN_PROCESO_AUTORIZACION = new Short("17");	
	public static final Short ODT_EN_PROCESO = new Short("0");
	public static final Short ODT_ASIGNADA = new Short("1");
	public static final BigDecimal ENDOSO_DE_CANCELACION = BigDecimal.ONE;
	public static final BigDecimal ENDOSO_DE_REHABILITACION = new BigDecimal(2D);
	public static final BigDecimal ENDOSO_DE_MODIFICACION = new BigDecimal(3D);
	public static final BigDecimal ENDOSO_DE_CAMBIO_FORMA_PAGO = new BigDecimal(5D);
	public static final short MOTIVO_CANCELACION_AUTOMATICA_POR_FALTA_DE_PAGO = 7;
	public static final short CONTRATADO = 1;
	public static final short NO_CONTRATADO = 0;

	public static final String CLAVE_MENSAJE_EXITO = "30";
	public static final String CLAVE_MENSAJE_INFORMACION = "20";
	public static final String CLAVE_MENSAJE_ERROR = "10";
	
	// '1:Cambio de Datos, 2:Aumento, 3:Disminución, 4:Cancelación,
	// 5:Rehabilitación', 6:Cambio Forma de Pago, 7:Cancelacion de endoso, 8:Rehabilitacion de endoso;
	public static final short TIPO_ENDOSO_REHABILITACION = 5;
	public static final short TIPO_ENDOSO_CE = 7;
	public static final short TIPO_ENDOSO_RE = 8;
	
	public static final short TIPO_ENDOSO_CANCELACION = 4;
	public static final short TIPO_ENDOSO_AUMENTO = 2;
	public static final short TIPO_ENDOSO_DISMINUCION = 3;
	public static final short TIPO_ENDOSO_CAMBIO_DATOS = 1;
	public static final short TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO = 6;

	public static final BigDecimal CODIGO_TIPO_NEGOCIO_FACULTATIVO_CONTROL_RECLAMOS=new BigDecimal("97");
	public static final BigDecimal CODIGO_TIPO_NEGOCIO_FACULTATIVO=new BigDecimal("98");
	public static final BigDecimal CODIGO_TIPO_NEGOCIO_NORMAL=BigDecimal.valueOf(99D);
	
	public static final short CLAVE_FORMATO_EDO_CTA_AUTOMATICOS = (short)3;
	public static final short CLAVE_FORMATO_EDO_CTA_AUTOMATICOS_REPORTETRIMESTRAL = (short)2;
	
	public static final BigDecimal RENOVACION_MOVIMIENTO_ASIGNACION_SUSCRIPTOR = BigDecimal.valueOf(10);
	public static final BigDecimal RENOVACION_MOVIMIENTO_POLIZA_EMITIDA = BigDecimal.valueOf(50);
	
	//Constantes para Estatus
	public static final Short ESTATUS_AUTORIZACION_NO_REQUERIDA = (short)0;
	public static final Short ESTATUS_AUTORIZACION_REQUERIDA = (short)1;
	public static final Short ESTATUS_AUTORIZADA = (short)7;
	public static final Short ESTATUS_RECHAZADA = (short)8;
	
	
	public static final String ARCHIVO_RECURSOS = "mx.com.afirme.midas.danios.reportes.comun.Reportes";
	
	public static final String RECURSOS_FORTIMAX = "mx.com.afirme.midas2.ws.fortimax.ConstantesFortimax";
	
	public static final String OCRA_CONFIG = "mx.com.afirme.midas2.wsClient.ocra.properties.ocraConfig";
	
	public static final String RECURSO_MENSAJES_EXCEPCIONES="mx.com.afirme.midas2.globalExceptionMessages";
	//Constantes para catalogo de valores fijo
	public static final int CLAVE_TIPO_CALCULO_EMISION=22;
	public static final int CLAVE_TIPO_CALCULO_CANCELACION=22;
	public static final int CLAVE_UNIDAD_VIGENCIA=23;
	public static final int CLAVE_TIPO_SUMA_ASEGURADA=24;
	public static final int CLAVE_TIPO_COASEGURO=25;
	public static final int CLAVE_TIPO_DEDUCIBLE=25;
	public static final int CLAVE_TIPO_LIMITE_COASEGURO=25;
	public static final int CLAVE_TIPO_LIMITE_DEDUCIBLE=25;
	public static final int GRUPO_CLAVE_BIEN_SECCION=26;
	public static final int GRUPO_CLAVE_OBLIGATORIEDAD=27;
	public static final int GRUPO_CLAVE_ESTATUS_PRODUCTO=32;
	public static final int GRUPO_CLAVE_TIPO_DEDUCIBLE=38;
	public static final int GRUPO_CLAVE_TIPO_LIMITE_DEDUCIBLE=39;
	
	public static final String UPLOAD_FOLDER="c:/MidasWeb/ArchivosAnexos/";//TODO Implementar variables de entorno. Entorno.obtenerVariable("midas.sistema.uploadFolder","c:/MidasWeb/ArchivosAnexos/");
	public static final String LINUX_UPLOAD_FOLDER="/apps/profile1/MIDASPDF/";//TODO Implementar variables de entorno. Entorno.obtenerVariable("midas.sistema.linux.uploadFolder","/apps/profile2/MIDASPDF/");//profile1 para azkaban profile2 para qa y produccion
		
	//constantes para c�digo de par�metros generales.
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_IMPRESION_RCFUNCIONARIOS_PRODUCTO = new BigDecimal(10070d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_IMPRESION_RCFUNCIONARIOS_TIPO_POLIZA = new BigDecimal(60010d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_IMPRESION_RCMEDICOS_TIPO_POLIZA = new BigDecimal(60020d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_IMPRESION_RC_PROFESIONAL_MEDICOS_TIPO_POLIZA = new BigDecimal(60030d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_IMPRESION_PAQ_EMPRESARIAL= new BigDecimal(10010d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_IMPRESION_TRANSPORTES = new BigDecimal(10020d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_PAQ_FAMILIAR= new BigDecimal(10030d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_INCENDIO= new BigDecimal(10040d);
	
	
	
	
	
	
	public static final String USUARIO_SISTEMA = "SISTEMA";
	public static final Short DOCUMENTO_ANEXO_OBLIGATORIO =(short)3;
	public static final Short DOCUMENTO_ANEXO_OPCIONAL = (short)0;
	
	public static final short PERMITE_SUBINCISOS = (short)1;
	public static final short NO_PERMITE_SUBINCISOS = (short)0;
	
	public static final String CLAVE_SUMA_ASEGURADA_BASICA="1";
	public static final String CLAVE_SUMA_ASEGURADA_AMPARADA="2";
	public static final String CLAVE_SUMA_ASEGURADA_SUBLIMITE="3";
	
	public static final BigDecimal REGISTRO_TIPO_POLIZA = new BigDecimal(1D);
	public static final BigDecimal REGISTRO_TIPO_INCISO = new BigDecimal(3D);
	public static final BigDecimal REGISTRO_TIPO_COBERTURA = new BigDecimal(4D);
	
	public static final BigDecimal CENTRO_EMISOR_DEFAULT = new BigDecimal(1D);
	
	public static final short TIPO_PRIMER_RIESGO = 1;
	
	public static final int INCENDIO = 1;
	
	public static final int MONEDA_PESOS = 484;
	
	
	// Constantes para tipos de comision
	public static final short TIPO_RO = 1;
	public static final short TIPO_RCI = 2;
	public static final short TIPO_PRR = 3;
	
	
	//Constante para Registar las estadisticas
	public static final int SE_CREA_SOLICITUD=1;
	public static final int SE_ASIGNA_SOL_A_ODT=2;
	public static final int SE_ASIGNA_ODT_A_COT=3;
	public static final int SE_ASIGNA_COT_A_EMISION=4;
	public static final int SE_EMITE_COTIZACION=5;
	public static final int SE_ASIGNA_SOL_A_COT=6;
	
	
	public static final long TIEMPO_INICIAR_DISTRIBUIR_POLIZA = 300000L;
	
	
	
	//EMISION
	
	//Constantes para renovacion
	public static final short ES_RENOVACION = 1;
	public static final short NO_ES_RENOVACION = 0;
	
	public static final String CLAVES_AUTORIZADA_RECHAZADA = "78";
	
	public static final String FORMATO_FECHA= "dd/MM/yyyy";
	public static final String FORMATO_FECHA_ORACLE= "yyyy-MM-dd HH:mm:ss.SSS";
	
	//REPORTES	
	public static final String LOGO_SEGUROS="/img/reportes/logo_seguros_11.gif";
	public static final String DUPLICADO="/img/reportes/duplicado.gif";
	public static final String FIRMA_FUNCIONARIO_AUTOS ="/img/reportes/firmas.jpg";
	
	public static final String AFIRME_SEGUROS="/mx/com/afirme/midas2/service/impl/impresiones/img/logo_AS.jpg";
	public static final String TABLA_CONTRATO_AGENTE="/mx/com/afirme/midas2/service/impl/impresiones/img/tablaContrato.jpg";
	public static final String FIRMA_FUNCIONARIO ="/mx/com/afirme/midas2/service/impl/impresiones/img/firmas.jpg";
	public static final String LOGO_SEGUROS_AFIRME ="/mx/com/afirme/midas2/service/impl/impresiones/img/logo_seguros_11.gif";
	public static final String NUEVO_LOGO_SEGUROS_AFIRME ="/mx/com/afirme/midas2/service/impl/impresiones/img/Logo_AFIRME_Seguros.png";
	public static final String LOGO_AFIRME ="/mx/com/afirme/midas2/service/impl/impresiones/img/logo_afirme.png";
	public static final String LOGO_ESTADODECUENTA ="/mx/com/afirme/midas2/service/impl/impresiones/img/logo_estadodecuenta.png";
	public static final String LOGO_CARTAS ="/mx/com/afirme/midas2/service/impl/impresiones/img/logo_cartas.png";
	public static final String FRENTE_AUTO_IMG ="/mx/com/afirme/midas2/service/impl/impresiones/img/frente.jpg";
	public static final String DETRAS_AUTO_IMG ="/mx/com/afirme/midas2/service/impl/impresiones/img/detras.jpg";
	public static final String IZQUIERDA_AUTO_IMG ="/mx/com/afirme/midas2/service/impl/impresiones/img/izquierda.jpg";
	public static final String DERECHA_AUTO_IMG ="/mx/com/afirme/midas2/service/impl/impresiones/img/derecha.jpg";
	public static final String LOGO_AFIRME_SEGUROS_INVERTIDO ="/mx/com/afirme/vida/service/impl/impresiones/img/afirme_seguros_invertido.png";
	                                                           
	//COMPONENTESGRAFICOS
	public static final String RADIO_BUTTON_ON 	= "/mx/com/afirme/midas2/service/impl/impresiones/img/radio_on.gif";
	public static final String RADIO_BUTTON_OFF = "/mx/com/afirme/midas2/service/impl/impresiones/img/radio_off.gif";
	public static final String CHECKBOX_ON 		= "/mx/com/afirme/midas2/service/impl/impresiones/img/check_on.gif";
	public static final String CHECKBOX_OFF		= "/mx/com/afirme/midas2/service/impl/impresiones/img/check_off.gif";
	public static final String PIXEL_IMG 		= "/mx/com/afirme/midas2/service/impl/impresiones/img/pixel.gif";
	public static final String LINE_GREEN       = "/mx/com/afirme/midas2/service/impl/impresiones/img/line_green.gif";
	
	//public static final String URL_WS_IMPRESION = Entorno.obtenerVariable("midas.ws.impresion.url","172.20.73.13");
	public static final String URL_WS_IMPRESION = Entorno.obtenerVariable("midas.ws.impresion.url","172.30.5.33");
	public static final String PUERTO_WS_IMPRESION = Entorno.obtenerVariable("midas.ws.impresion.puerto","45120");
	
	public static final String URL_WS_IMPRESION_FACTURA = Entorno.obtenerVariable("midas.wFacturaService.url","http://172.30.5.33:45120/WFacturaServices/services/WFacturaService?wsdl");
	
	///COTIZACION VIDA//
	public static final String ENCABEZADO_AFIRME="/mx/com/afirme/midas2/service/impl/impresiones/img/logoAfirme.png";
	public static final String COBERTURA_BAS_PLAT="/mx/com/afirme/midas2/service/impl/impresiones/img/coberturaBas_Plat.png";
	public static final String DESCRIPCIONESVIDA="/mx/com/afirme/midas2/service/impl/impresiones/img/descripcionesVida.png";
	
}
