package mx.com.afirme.midas.sistema.reporte;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.commons.lang.StringUtils;

public abstract class MidasBaseReporte {

	public static final int MAXIM_ROWS = 65000;
	
	/**
	 * Este metodo permite obtener un Reporte Compilado apartir de una plantilla
	 * JRXML localizada en el algun paquete del proyecto.
	 * 
	 * @param String
	 *            sourceFileName es la plantilla que se pretende compilar
	 * @return JasperReport el reporte Jasper compilado
	 */	
	protected JasperReport getJasperReport(String sourceFileName) {
		JasperReport jasperReport = null;
		try {
			// InputStream input = new FileInputStream(new
			// File(sourceFileName));
			InputStream input = MidasBaseReporte.class
					.getResourceAsStream(sourceFileName);
			JasperDesign design = JRXmlLoader.load(input);
			jasperReport = JasperCompileManager.compileReport(design);
		} catch (JRException e) {
			mostrarErrorGeneral(e);
		}
		return jasperReport;
	}
	
	/**
	 * Genera un documento pdf, xls ó rtf a partir de una plantilla jrxml, un conjunto de parámetros y la lista de objetos que conforma
	 * el contenido del reporte; y lo devuelve como un arreglo de bytes
	 * @param tipoReporte. Daos permitidos: "application/pdf", "application/vnd.ms-excel", "application/vnd.ms-word". Si recibe null o ""
	 * se utiliza el valor por default, que es: "application/pdf"
	 * @param sourceFileName. nombre completo de la plantilla JRXML, incluyendo su ubicación dentro de los paquetes fuente del proyecto.
	 * @param parametrosReporte. Mapa de parámetros que se utilizarán para poblar el reporte.
	 * @param listaObjetos. Lista de objetos con los cuales se poblará el contenido del reporte.
	 * @return byte[] arreglo de bytes que corresponde al reporte.
	 * @throws JRException.
	 */
	public byte[] generaReporte(String tipoReporte,String sourceFileName,Map<String,Object> parametrosReporte,List<Object> listaObjetos) throws JRException{
		InputStream input = MidasBaseReporte.class.getResourceAsStream(sourceFileName);
		JasperDesign design = JRXmlLoader.load(input);
		JasperReport jasperReport = JasperCompileManager.compileReport(design);
		
		JasperPrint jasperPrint;
		if (listaObjetos == null)
			jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosReporte);
		else{
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(listaObjetos);
			jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosReporte, dataSource);
		}

		if (StringUtil.isEmpty(tipoReporte))
			tipoReporte = ConstantesReporte.TIPO_PDF;
		if (tipoReporte.equals(ConstantesReporte.TIPO_PDF)) {
			return this.generarReportePDF(jasperPrint);
		} else if (tipoReporte.equals(ConstantesReporte.TIPO_XLS)) {
			return this.generarReporteXLS(jasperPrint);
		} else if (tipoReporte.equals(ConstantesReporte.TIPO_DOC)) {
			return this.generarReporteRTF(jasperPrint);
		}
		else throw new JRException("tipo de reporte no válido.");
	}

	/**
	 * Este metodo genera un reporte apartir de una platilla JRXML y lo regresa
	 * como un arreglo de bites
	 * 
	 * @param String
	 *            reportType es el tipo de reporte que se pretende generar,
	 *            puden ser PDF, RTF o XLS
	 * @param JasperReport
	 *            jasperReport es el reporte compilado que se tranformara
	 * @param Map
	 *            <?, ?> parameters son los parametros que necesita la platilla
	 * @param JRDataSource
	 *            jrDataSource es el dataSource de la plantilla
	 * @return JasperReport el reporte Jasper compilado
	 */
	public byte[] generaReporte(String reportType, JasperReport jasperReport,
			Map<?, ?> parameters, JRDataSource jrDataSource) throws JRException {
		JasperPrint jasperPrint;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, jrDataSource);
			if (reportType.equals(ConstantesReporte.TIPO_PDF)) {
				return this.generarReportePDF(jasperPrint);
			} else if (reportType.equals(ConstantesReporte.TIPO_XLS)) {
				return this.generarReporteXLS(jasperPrint);
			} else if (reportType.equals(ConstantesReporte.TIPO_DOC)) {
				return this.generarReporteRTF(jasperPrint);
			}
			return null;
		} catch (JRException e) {
			mostrarErrorGeneral(e);
			throw e;
		}
	}
	
	public byte[] generaReporte(String reportType, JasperPrint jasperPrint) throws JRException {
		try {
			if (reportType.equals(ConstantesReporte.TIPO_PDF)) {
				return this.generarReportePDF(jasperPrint);
			} else if (reportType.equals(ConstantesReporte.TIPO_XLS)) {
				return this.generarReporteXLS(jasperPrint);
			} else if (reportType.equals(ConstantesReporte.TIPO_DOC)) {
				return this.generarReporteRTF(jasperPrint);
			}
			return null;
		} catch (JRException e) {
			mostrarErrorGeneral(e);
			throw e;
		}
	}

	
	public byte[] generaReporte(String reportType, List<JasperPrint> jasperPrint) throws JRException {
		try {
			if (reportType.equals(ConstantesReporte.TIPO_XLS)) {
				return this.generarReporteXLS(jasperPrint);
			} 
			return null;
		} catch (JRException e) {
			mostrarErrorGeneral(e);
			throw e;
		}
	}

	/**
	 * Este metodo genera un reporte apartir de una platilla JRXML y lo regresa
	 * como un arreglo de bites
	 * 
	 * @param String
	 *            reportType es el tipo de reporte que se pretende generar,
	 *            puden ser PDF, RTF o XLS
	 * @param JasperReport
	 *            jasperReport es el reporte compilado que se tranformara
	 * @param Map
	 *            <?, ?> parameters son los parametros que necesita la platilla
	 * @param Connection
	 *            connection es la conexion que utilizara la plantilla
	 * @return JasperReport el reporte Jasper compilado
	 */
	public byte[] generaReporte(String reportType, JasperReport jasperReport,
			Map<?, ?> parameters, Connection connection) throws JRException {
		try{
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, connection);
			if (reportType.equals(ConstantesReporte.TIPO_PDF)) {
				return this.generarReportePDF(jasperPrint);
			} else if (reportType.equals(ConstantesReporte.TIPO_XLS)) {
				return this.generarReporteXLS(jasperPrint);
			} else if (reportType.equals(ConstantesReporte.TIPO_DOC)) {
				return this.generarReporteRTF(jasperPrint);
			}
			return null;
		}catch(Exception e){
			System.out.println("Error en sistema.MidasBaseReporte.generaReporte ==> " + e.getMessage());
			return null;
		}
	}

	/**
	 * Este metodo genera un reporte apartir de una platilla JRXML y lo regresa
	 * como un arreglo de bites
	 * 
	 * @param String
	 *            reportType es el tipo de reporte que se pretende generar,
	 *            puden ser PDF, RTF o XLS
	 * @param JasperReport
	 *            jasperReport es el reporte compilado que se tranformara
	 * @param Map
	 *            <?, ?> parameters son los parametros que necesita la platilla
	 * @param JRResultSetDataSource
	 *            set es el resultSet con datos que se utilizaran para pintar el
	 *            contenido de la plantilla
	 * @return JasperReport el reporte Jasper compilado
	 */
	public byte[] generaReporte(String reportType, JasperReport jasperReport,
			Map<?, ?> parameters, JRResultSetDataSource set) throws JRException {
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
				parameters, set);
		if (reportType.equals(ConstantesReporte.TIPO_PDF)) {
			return this.generarReportePDF(jasperPrint);
		} else if (reportType.equals(ConstantesReporte.TIPO_XLS)) {
			return this.generarReporteXLS(jasperPrint);
		} else if (reportType.equals(ConstantesReporte.TIPO_DOC)) {
			return this.generarReporteRTF(jasperPrint);
		}
		return null;
	}

	/**
	 * Este metodo genera un reporte apartir de una platilla JRXML y lo regresa
	 * como un arreglo de bites
	 * 
	 * @param String
	 *            reportType es el tipo de reporte que se pretende generar,
	 *            puden ser PDF, RTF o XLS
	 * @param JasperReport
	 *            jasperReport es el reporte compilado que se tranformara
	 * @param Map
	 *            <?, ?> parameters son los parametros que necesita la platilla
	 * @param JRBeanCollectionDataSource
	 *            dataSource es el Arreglo de Beans que se utilizaran para
	 *            pintar el contenido de la plantilla
	 * @return JasperReport el reporte Jasper compilado
	 */
	public byte[] generaReporte(String reportType, JasperReport jasperReport,
			Map<?, ?> parameters, JRBeanCollectionDataSource dataSource)
			throws JRException {
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
				parameters, dataSource);
		if (reportType.equals(ConstantesReporte.TIPO_PDF)) {
			Logger.getAnonymousLogger().log(Level.INFO ,"[MidasBaseReporte.generaReporte] formato del reporte "+reportType);
			return this.generarReportePDF(jasperPrint);
		} else if (reportType.equals(ConstantesReporte.TIPO_XLS)) {
			Logger.getAnonymousLogger().log(Level.INFO ,"[MidasBaseReporte.generaReporte] formato del reporte "+reportType);
			return this.generarReporteXLS(jasperPrint);
		} else if (reportType.equals(ConstantesReporte.TIPO_DOC)) {
			Logger.getAnonymousLogger().log(Level.INFO ,"[MidasBaseReporte.generaReporte] formato del reporte "+reportType);
			return this.generarReporteRTF(jasperPrint);
		}  else if (reportType.equals(ConstantesReporte.TIPO_TXT)) {
			Logger.getAnonymousLogger().log(Level.INFO ,"[MidasBaseReporte.generaReporte] formato del reporte "+reportType);
			return this.generarReporteTXT(jasperPrint);
		} else if (reportType.equals(ConstantesReporte.TIPO_CSV)) {
			Logger.getAnonymousLogger().log(Level.INFO ,"[MidasBaseReporte.generaReporte] formato del reporte "+reportType);
			return this.generarReporteCSV(jasperPrint);
		} else if (reportType.equals(ConstantesReporte.TIPO_XLSX)) {
			Logger.getAnonymousLogger().log(Level.INFO ,"[MidasBaseReporte.generaReporte] formato del reporte "+ConstantesReporte.TIPO_XLSX);
			return this.generarReporteXLSX(jasperPrint);
		} else if (reportType.equals(ConstantesReporte.TIPO_TXT_MOVIL)) {
			Logger.getAnonymousLogger().log(Level.INFO ,"[MidasBaseReporte.generaReporte] formato del reporte "+reportType);
			return this.generarReporteTXT(jasperPrint, new Float(5), new Float(5));
	    }
		return null;
	}
	/**
	 * Este metodo genera un reporte con multiples hojas apartir de una platilla JRXML y lo regresa
	 * cada hoja necesita un JRBeanCollectionDataSource el cual se le pasa en el mapa de los parametros con el nombre
	 * de "dataSource" si no lo ponen asi, no va funcionar
	 * tambien necesita que los reportes que van a generar tenga activada la opcion 
	 * ignore pagitation
	 * como un arreglo de bites
	 * @param reportType
	 * @param parameters
	 * @param dataSource
	 * @param jasperReport
	 * @return
	 * @throws JRException
	 * @autor martin
	 */
	@SuppressWarnings("unchecked")
	public byte[] generaReporte(String reportType,
			JRBeanCollectionDataSource dataSource,
			List<Map<String, Object>> parameters, JasperReport[] jasperReport)
			throws JRException {
		// Obtiene el primer reporte
		JasperPrint firstSheet = JasperFillManager.fillReport(jasperReport[0],
				parameters.get(0), dataSource);
		String[] sheetNames = new String[jasperReport.length];
		// Obtiene el nombre del primer reporte
		sheetNames[0] = firstSheet.getName();
		// Obtiene el resto de los reportes y su dataSource
		for (int x = 1; x < jasperReport.length; x++) {
			JasperPrint jasperPrint = JasperFillManager.fillReport(
					jasperReport[x], parameters.get(x-1),(JRBeanCollectionDataSource)parameters.get(x-1).get("dataSource"));
			jasperPrint.getName();
			if (StringUtils.isNotBlank(jasperPrint.getName())) {
				sheetNames[x] = jasperPrint.getName();	
			}
			List<JRPrintPage> pages = new ArrayList<JRPrintPage>(
					jasperPrint.getPages());
			int i = firstSheet.getPages().size();
			// Junta todas las hojas en el primer reporte
			for (int count = 0; count < pages.size(); count++) {
				firstSheet.addPage(i, (JRPrintPage) pages.get(count));
				i++;
			}
		}
		if (reportType.equals(ConstantesReporte.TIPO_PDF)) {
			return this.generarReportePDF(firstSheet);
		} else if (reportType.equals(ConstantesReporte.TIPO_XLS)) {
			return this.generarReporteXLS(firstSheet,sheetNames);
		} else if (reportType.equals(ConstantesReporte.TIPO_DOC)) {
			return this.generarReporteRTF(firstSheet);
		}else if (reportType.equals(ConstantesReporte.TIPO_TXT)) {
			return this.generarReporteTXT(firstSheet);
		}
		return null;
	}
	
	protected byte[] generarReportePDF(JasperPrint jasperPrint)
			throws JRException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRPdfExporter pdfExporter = new JRPdfExporter();
		pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
				outputStream);
		pdfExporter.exportReport();
		return outputStream.toByteArray();
	}
	
	protected byte[] generarReporteXLS(JasperPrint jasperPrint,String[] sheetNames) throws JRException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRXlsExporter xlsExporter = new JRXlsExporter();
		xlsExporter.setParameter(JRXlsExporterParameter.SHEET_NAMES, sheetNames);
		xlsExporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, 65000);
		xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		xlsExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,outputStream);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.TRUE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,Boolean.TRUE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED, Boolean.TRUE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,  Boolean.TRUE);
		return outputStream.toByteArray();
	}

	protected byte[] generarReporteXLS(JasperPrint jasperPrint) throws JRException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRXlsExporter xlsExporter = new JRXlsExporter();
		xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		xlsExporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, 65000);
		xlsExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,outputStream);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.FALSE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,Boolean.FALSE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED, Boolean.TRUE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,  Boolean.TRUE);
		//xlsExporter.setParameter(JRXlsAbstractExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);//se agrega este parametro para que detecte automaticamnte el tipo de la celda
		xlsExporter.exportReport();
		return outputStream.toByteArray();
	}
	
	protected byte[] generarReporteXLS(List<JasperPrint> listJasperPrint) 
		throws JRException{
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRXlsExporter xlsExporter = new JRXlsExporter();
		xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, listJasperPrint);
		xlsExporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, MAXIM_ROWS);
		xlsExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,outputStream);
		xlsExporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE); 
		xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, listJasperPrint);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.FALSE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,Boolean.FALSE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED, Boolean.TRUE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,  Boolean.TRUE);
		xlsExporter.exportReport();
		return outputStream.toByteArray();
	}
	
	protected byte[] generarReporteCSV(JasperPrint jasperPrint) throws JRException{
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRCsvExporter csvExporter = new JRCsvExporter();
		csvExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		csvExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
		csvExporter.exportReport();
		return outputStream.toByteArray();
		
	}

	protected byte[] generarReporteRTF(JasperPrint jasperPrint)
			throws JRException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRRtfExporter rtfExporter = new JRRtfExporter();
		rtfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		rtfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
				outputStream);
		rtfExporter.exportReport();
		return outputStream.toByteArray();
	}
	
	protected byte[] generarReporteTXT(JasperPrint jasperPrint)
			throws JRException {
		byte[] outputStream;
		outputStream = generarReporteTXT(jasperPrint,new Float(7),new Float(10));
		return outputStream;
	}
	protected byte[] generarReporteTXT(JasperPrint jasperPrint,Float CHARACTER_WIDTH,Float CHARACTER_HEIGHT)
			throws JRException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRTextExporter rtfExporter = new JRTextExporter();
		rtfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		rtfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
				outputStream);
		rtfExporter.setParameter(JRTextExporterParameter.CHARACTER_WIDTH, CHARACTER_WIDTH);
		rtfExporter.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT, CHARACTER_HEIGHT);
		rtfExporter.exportReport();
		return outputStream.toByteArray();
	}
	
	protected byte[] generarReporteXLSX(JasperPrint jasperPrint) throws JRException {
		Logger.getAnonymousLogger().log(Level.INFO,"Entrando al metodo generarReporteXLSX");
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRXlsxExporter xlsxExporter = new JRXlsxExporter();
		xlsxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		xlsxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,outputStream);
		xlsxExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		xlsxExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.FALSE);
		xlsxExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,Boolean.TRUE);
		xlsxExporter.setParameter(JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED, Boolean.TRUE);
		xlsxExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		xlsxExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,  Boolean.TRUE);
//		xlsxExporter.setParameter(JRXlsAbstractExporterParameter.IS_DETECT_CELL_TYPE, Boolean.FALSE);//se agrega este parametro para que detecte automaticamnte el tipo de la celda
		xlsxExporter.exportReport();
		return outputStream.toByteArray();
	}
	
	protected void mostrarErrorGeneral(Exception e){  
		Logger.getAnonymousLogger().log(Level.WARNING,"Error to generate report... producer JRException ", e);
	}
}