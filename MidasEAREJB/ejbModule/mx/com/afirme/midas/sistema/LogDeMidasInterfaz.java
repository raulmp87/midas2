package mx.com.afirme.midas.sistema;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LogDeMidasInterfaz {

private static final Logger logger;
	
	static {
		logger = Logger.getLogger("InterfazPU");
		logger.setLevel(Level.ALL);
	}
	
	public static void log(String info, Level level, Throwable ex) {
    	logger.log(level, info, ex);
    }
    
    public static Logger getLogger() {
    	return logger;
    }
	
}
