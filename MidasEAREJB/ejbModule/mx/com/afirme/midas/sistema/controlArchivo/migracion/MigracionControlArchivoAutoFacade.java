package mx.com.afirme.midas.sistema.controlArchivo.migracion;

// default package

import java.util.List;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.controlArchivo.migracion.MigracionControlArchivoAutoDTO.Estatus;


/**
 * Facade for entity MigracionControlArchivoAutoDTO.
 * 
 * @see .MigracionControlArchivoAutoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class MigracionControlArchivoAutoFacade implements MigracionControlArchivoAutoFacadeRemote {
	
	
	// property constants
	public static final String ESTATUS = "estatus";
	
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * MigracionControlArchivoAutoDTO entity. All subsequent persist actions of this
	 * entity should use the #update() method.
	 * 
	 * @param entity
	 *            MigracionControlArchivoAutoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(MigracionControlArchivoAutoDTO entity) {
			entityManager.persist(entity);
	}

	/**
	 * Delete a persistent MigracionControlArchivoAutoDTO entity.
	 * 
	 * @param entity
	 *            MigracionControlArchivoAutoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(MigracionControlArchivoAutoDTO entity) {
		entity = entityManager.getReference(
				MigracionControlArchivoAutoDTO.class, entity.getId());
		entityManager.remove(entity);
	}

	/**
	 * Persist a previously saved MigracionControlArchivoAutoDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * MigracionControlArchivoAutoDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            MigracionControlArchivoAutoDTO entity to update
	 * @return MigracionControlArchivoAutoDTO the persisted
	 *         MigracionControlArchivoAutoDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public MigracionControlArchivoAutoDTO update(MigracionControlArchivoAutoDTO entity) {
		MigracionControlArchivoAutoDTO result = entityManager.merge(entity);
		return result;
	}

	public MigracionControlArchivoAutoDTO findById(Long id) {
		MigracionControlArchivoAutoDTO instance = entityManager.find(
				MigracionControlArchivoAutoDTO.class, id);
		return instance;
	}

	/**
	 * Find all MigracionControlArchivoAutoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the MigracionControlArchivoAutoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<MigracionControlArchivoAutoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<MigracionControlArchivoAutoDTO> findByProperty(String propertyName, final Object value) {
		final String queryString = "select model from MigracionControlArchivoAutoDTO model where model."
			+ propertyName + "= :propertyValue";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("propertyValue", value);
		return query.getResultList();
	}

	public List<MigracionControlArchivoAutoDTO> findByEstatus(Estatus estatus) {
		return findByProperty(ESTATUS, estatus.getValor());
	}

	/**
	 * Find all MigracionControlArchivoAutoDTO entities.
	 * 
	 * @return List<MigracionControlArchivoAutoDTO> all MigracionControlArchivoAutoDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<MigracionControlArchivoAutoDTO> findAll() {
		final String queryString = "select model from MigracionControlArchivoAutoDTO model";
		Query query = entityManager.createQuery(queryString);
		return query.getResultList();
	}
	
	

}