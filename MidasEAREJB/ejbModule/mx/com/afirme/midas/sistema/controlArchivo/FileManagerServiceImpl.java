package mx.com.afirme.midas.sistema.controlArchivo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.controlArchivo.migracion.MigracionControlArchivoAutoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.migracion.MigracionControlArchivoAutoDTO.Estatus;
import mx.com.afirme.midas.sistema.controlArchivo.migracion.MigracionControlArchivoAutoFacadeRemote;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.fortimax.FortimaxV2Service;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;

import com.afirme.webservice.client.BusquedaService;
import com.afirme.webservice.client.BusquedaServiceLocator;
import com.afirme.webservice.client.BusquedaServicePortType;
import com.afirme.webservice.client.DocumentoService;
import com.afirme.webservice.client.DocumentoServiceLocator;
import com.afirme.webservice.client.DocumentoServicePortType;
import com.afirme.webservice.client.ExpedienteService;
import com.afirme.webservice.client.ExpedienteServiceLocator;
import com.afirme.webservice.client.ExpedienteServicePortType;
import com.afirme.webservice.client.LoginService;
import com.afirme.webservice.client.LoginServiceLocator;
import com.afirme.webservice.client.LoginServicePortType;
import com.afirme.webservice.client.MakeZipService;
import com.afirme.webservice.client.MakeZipServiceLocator;
import com.afirme.webservice.client.MakeZipServicePortType;

@Stateless
public class FileManagerServiceImpl implements FileManagerService {
	
	
	/** Log de FileRepositoryServiceImpl */
	private static final Logger LOG = Logger.getLogger(FileManagerServiceImpl.class);
	
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	private DocumentoServicePortType documentoService;
	private LoginServicePortType loginService;
	private BusquedaServicePortType busquedaService;
	private MakeZipServicePortType makeZipService;
	private ExpedienteServicePortType expedienteService;
	
	@EJB
	private MigracionControlArchivoAutoFacadeRemote migracionControlArchivoFacade;
	@EJB
	private ControlArchivoFacadeRemote controlArchivoFacade;
	@EJB
	private FortimaxV2Service fortimaxV2Service;	
		
	@PostConstruct
	public void init() {
		try {
			final String urlWS = parametroGeneralFacade.findById(
					new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX,PARAMETRO_URL), true).getValor();
			final DocumentoService serviceLocator = new DocumentoServiceLocator();
			URL url = new URL(urlWS + WSDL_DOCUMENTO);
			documentoService = serviceLocator.getDocumentoServiceHttpSoap12Endpoint(url);
			
			final LoginService loginServiceLocator = new LoginServiceLocator();
			url = new URL(urlWS+ WSDL_LOGIN);
			loginService = loginServiceLocator.getLoginServiceHttpSoap12Endpoint(url);
			
			final BusquedaService busquedaServiceLocator = new BusquedaServiceLocator();
			url = new URL(urlWS + WSDL_BUSQUEDA);
			busquedaService = busquedaServiceLocator.getBusquedaServiceHttpSoap12Endpoint(url);
			
			final MakeZipService makeZipServiceLocator = new MakeZipServiceLocator();
			url = new URL(urlWS + WSDL_MAKEZIP);
			makeZipService = makeZipServiceLocator.getMakeZipServiceHttpSoap12Endpoint(url);
			
			final ExpedienteService expedienteServiceLocator = new ExpedienteServiceLocator();
			url = new URL(urlWS + WSDL_EXPEDIENTE);
			expedienteService = expedienteServiceLocator.getExpedienteServiceHttpSoap12Endpoint(url);
			
		} catch(Exception e) {
			LOG.error("-- FortimaxServiceImpl()", e);
		}
	}
	
	
	/**
	 * Sube un archivo a fortimax, si fortimax no estï¿½ disponible lo sube al filesystem y crea un
	 * registro para intentar migrar despues
	 */
	public boolean uploadFile(String fileName, String idControlArchivo, byte[] fileData) {
		return uploadFile(fileName, idControlArchivo, fileData, true);
	}
	
	public boolean uploadFile(String fileName, String idControlArchivo, byte[] fileData, boolean highAvailability) {
		
		String fileControlName = getFileName(fileName, idControlArchivo);
		try {
			LOG.info("--uploadFile() {}"+fileName);
			
			final String gaveta = parametroGeneralFacade.findById(
					new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX, PARAMETRO_GAVETA_AUTOS), true).getValor();
			final String[] expediente = {parametroGeneralFacade.findById(
					new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX, PARAMETRO_EXPEDIENTE), true).getValor()};
			final String carpeta = parametroGeneralFacade.findById(
					new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX,PARAMETRO_CARPETA_AUTOS), true).getValor();
			
			String switchFortimax = parametroGeneralFacade.findById(
					new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX,PARAMETRO_SWITCH_FORTIMAX), true).getValor();			
			
			boolean result = false;
			if(switchFortimax == null || Integer.parseInt(switchFortimax) == 1){
				final String[] response = documentoService.uploadFileNodo(fileControlName, fileData, gaveta, expediente, carpeta);
				LOG.info("--uploadFile() Resultado ["+response[0]+"] ["+response[1]+"] ["+response[2]+"]");
				result = BooleanUtils.toBoolean(response[0]);
				System.out.println("### uploadFile-result: "+result);
			}
			if (!result) {
				throw new Exception(String.format("No se pudo cargar el archivo [%s] en fortimax", fileName));
			}else{
				try{
					String valor = parametroGeneralFacade.findById(
						new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX,PARAMETRO_CREA_FILESYSTEM), true).getValor();
					System.out.println("### uploadFile-valor: "+valor);

					if(valor != null && Integer.parseInt(valor) == 1){
						uploadFileToDisk(fileControlName, fileData,false,idControlArchivo);
					}
				}catch(Exception e){
					LOG.info("--uploadFile() Fallo Filesystem");
				}
			}
			return result;
		} catch(Exception e){
			LOG.error("-- uploadFile()", e);
			return highAvailability ? uploadFileToDisk(fileControlName, fileData,true,idControlArchivo) : false; 
		}
	
	}
	
	public FileManagerResponse uploadFileFortimax(String fileName, String idControlArchivo, byte[] fileData, Map<String,Object> parametros){		
		boolean highAvailability = (Boolean) parametros.get("highAvailability");
		String fileControlName = getFileName(fileName, idControlArchivo);
		String expediente = parametros.get("expediente").toString();
		String tipo = parametros.get("tipo").toString();
		FileManagerResponse fileManagerResponse = null;
		boolean result = false ;
		String nodo;
		
		String switchFortimax = parametroGeneralFacade.findById(
				new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX,PARAMETRO_SWITCH_FORTIMAX), true).getValor();
		
		BigDecimal idGaveta = new BigDecimal(parametroGeneralFacade.findById(
				new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX, PARAMETRO_TIPO_ESTRUCTURA), true).getValor());
		
		final String estructura = parametroGeneralFacade.findById(
				new ParametroGeneralId(GRUPO_PARAMETRO_ESTRUCTURA_FORTIMAX, idGaveta.add(new BigDecimal(tipo))), true).getValor();
		
		final String campos = parametroGeneralFacade.findById(
				new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX, PARAMETRO_CAMPOS_ESTRUCTURA), true).getValor();
				
		final String[] data = estructura.split("\\|");
		final String gaveta = data[0];
		final String carpeta = data[1];
		
		final String[] datos = campos.split("\\|");
		final String id = datos[0];
		final String referencia = datos[1];
		boolean cargarArchivo = false;
		
		if(switchFortimax == null || Integer.parseInt(switchFortimax) == 1){
			try {
				LOG.info("--uploadFile() {}"+fileName);
				final String[] resultadoCE = expedienteService.getExpedientes(gaveta, new String[]{id}, new String[]{expediente}); 
				LOG.info("--getExpedientes() Resultado [{}] "+ resultadoCE[0]);
				
				if(resultadoCE[0]==null){
					
					final String[] resultadoGE = expedienteService.generateExpediente(parametroGeneralFacade.findById(
							new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX,PARAMETRO_USUARIO), true).getValor(),
							gaveta, gaveta, new String[]{id,referencia}, new String[]{expediente,"EXP "+expediente+""}, true);
					LOG.info("--createExpediente() Resultado [{}] [{}] [{}] "+ resultadoGE[0]+" "+ resultadoGE[1]+" "+resultadoGE[2]);
					cargarArchivo = BooleanUtils.toBoolean(resultadoGE[0]);
				}else{
					cargarArchivo = true;
				}
				
				if(cargarArchivo){
					final String[] response = documentoService.uploadFileNodo(fileControlName, fileData, gaveta, new String[]{expediente}, carpeta);
					LOG.info("--uploadFile() Resultado ["+response[0]+"] ["+response[1]+"] ["+response[2]+"]");
					result = BooleanUtils.toBoolean(response[0]);
					nodo = response[1];
					
					System.out.println("### uploadFile-result: "+result);
					if (!result) {
						throw new Exception(String.format("No se pudo cargar el archivo [%s] en fortimax", fileName));
					}else{
						 fileManagerResponse = new FileManagerResponse(result,nodo);				
						}
					return fileManagerResponse;
				}else{
					throw new Exception(String.format("No se pudo generar el expediente", expediente));
					}
				}catch(Exception e){
					LOG.error("-- uploadFile() file system", e);
					result = highAvailability ? uploadFileToDisk(fileControlName, fileData,true,idControlArchivo) : false; 
					return new FileManagerResponse(result,null);  	
				}
		}else{
			uploadFileToDisk(fileControlName, fileData,true,idControlArchivo);
			return new FileManagerResponse(result,null); 
		}	
	}
	
	public void crearExpedienteFortimax(String fortimaxIDFile, String nombre, String gaveta) {
		String[] fieldValues = new String[2];
		fieldValues[0]=fortimaxIDFile;
		fieldValues[1]=nombre;
		
		String[] strReturns;
		try {
			strReturns = fortimaxV2Service.generateExpedient(gaveta, fieldValues);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		if (strReturns[0].equals("false")) {
			throw new RuntimeException("No se pudo crear el expediente en Fortimax para el Documento Anexo.");
		}
	}
	
	public void cargarArchivoFortimax(String fileName, String fortimaxIDFile, TransporteImpresionDTO transporteImpresionDTO, String gaveta, String carpeta){
		try {
			fortimaxV2Service.uploadFile(fileName, transporteImpresionDTO, gaveta, new String[]{fortimaxIDFile}, carpeta);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public boolean uploadFileToDisk(String fileName, byte[] fileData, boolean migrar,String idControlArchivo) {
		
		try {
			String OSName = System.getProperty("os.name");
			String uploadFolder;
			if (OSName.toLowerCase().indexOf("windows")!=-1) {
				uploadFolder = SistemaPersistencia.UPLOAD_FOLDER;
			} else {
				uploadFolder = SistemaPersistencia.LINUX_UPLOAD_FOLDER;
			}
			final File dir = new File(uploadFolder);
			final File file = new File(dir, fileName);
			FileUtils.writeByteArrayToFile(file, fileData);
			
			// Guarda el registro en la tabla MigracionControlArchivo para su tratamiento posterior
			if(migrar){
				final ControlArchivoDTO controlArchivo = controlArchivoFacade.findById(new BigDecimal(idControlArchivo));
				final MigracionControlArchivoAutoDTO migracionControlArchivoDTO = new MigracionControlArchivoAutoDTO();
				migracionControlArchivoDTO.setControlArchivo(controlArchivo);
				migracionControlArchivoDTO.setEstatus(Estatus.PENDIENTE.getValor());
				migracionControlArchivoDTO.setFechaEstatus(new Date());
				migracionControlArchivoDTO.setDetalleEstatus("No se pudo cargar el archivo en fortimax");
				migracionControlArchivoFacade.save(migracionControlArchivoDTO);
			}
			return true;
		} catch (Exception e) {
			LOG.error("-- uploadFileToDisk()", e);
			return false;
		}
	}

	/**
	 * Descarga un archivo desde el sistema de archivos o desde fortimax
	 * @param fileName
	 * @return
	 */
	public byte[] downloadFile(String fileName, String idControlArchivo) {
		// Primero busca en el sistema de archivo
		
		byte[] result = downloadFileFromDisk(fileName);
		if(ArrayUtils.isEmpty(result)) {
			try {
				// Si no se encuentra se busca en fortimax
				final String nodo = !org.apache.commons.lang.StringUtils.isBlank(idControlArchivo) ? idControlArchivo :getNodo(fileName);
				final String token = getToken(60);
				result = makeZipService.descargaBytesPagina(token, nodo, 0); 
				if( result == null) {
					throw new Exception(String.format("No se encontr� el archivo [%s] en el repositorio, " +
							"ni en el sistema de archivos del servidor", fileName));
					}
				} catch (Exception e) {
					LOG.error("-- downloadFile()", e);
					}
			}
		return result;
		}
	
	/**
	 * Descarga un archivo desde el file system
	 * @param fileName
	 * @return
	 */
	public byte[] downloadFileFromDisk(String fileName) {
		InputStream input = null;
		try {
			final String uploadFolder;
			if (System.getProperty("os.name").toLowerCase().indexOf("windows")!= -1) {
				uploadFolder = SistemaPersistencia.UPLOAD_FOLDER;
			} else {
				uploadFolder = SistemaPersistencia.LINUX_UPLOAD_FOLDER; 
			}
			final File file = new File(uploadFolder + fileName);
			input = new FileInputStream(file);
			return IOUtils.toByteArray(input);
		} catch(FileNotFoundException e) {
			LOG.error("-- dowloadFileFromDisk() - No se encontro el archivo en el filesystem, se buscara en Fortimax");
			return null;
		} catch (Exception e) {
			LOG.error("-- dowloadFileFromDisk()", e);
			return null;
		} finally {
			IOUtils.closeQuietly(input);
		}
	}
	
	private String getToken(long duration) {
		try {
			final String user = parametroGeneralFacade.findById(
					new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX,PARAMETRO_USUARIO), true).getValor();
			final String password = parametroGeneralFacade.findById(
					new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX,PARAMETRO_PASSWORD), true).getValor();
			
			final String[] response = loginService.login(user, password, false, duration);
			final String result = response[0];
			/*
			if(result == null) {
				LOG.info("-- getLoginToken() {} - {} : user[{}] password[{}] ", response[1], response[2],user, password);
			}*/
			return result;
		} catch (Exception e) {
			LOG.error("-- getLoginToken()", e);
			return null;
		}
	}
	
	private String getNodo(String fileName) {
		try {
			final String token = getToken(60);
			final String gaveta = parametroGeneralFacade.findById(
					new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX, PARAMETRO_GAVETA_AUTOS), true).getValor();
			final String nombreCampo = parametroGeneralFacade.findById(
					new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX,PARAMETRO_FIELD_NAME), true).getValor();
			final String valorCampo = parametroGeneralFacade.findById(
					new ParametroGeneralId(GRUPO_PARAMETRO_FORTIMAX, PARAMETRO_EXPEDIENTE), true).getValor();
			// En fortimax los archivos se guardan sin extension, hay que quitarsela antes de buscar
			int indexOfDot = fileName.lastIndexOf(".");
			fileName = fileName.substring(0, indexOfDot > 0 ? indexOfDot : fileName.length());
			final String[] respuesta = busquedaService.getNodo(token, gaveta, nombreCampo, valorCampo, fileName, CLAVE_DOCUMENTO);
			// El nodo es el string hasta el primer espacio en blanco
			final String result = respuesta[0].substring(0, respuesta[0].indexOf(" "));
			return result;
		} catch (Exception e) {
			LOG.error("-- getNodo()", e);
			return null;
		}	

	}
	
	private String getFileName(String fileName, String idToControlArchivo){
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		controlArchivoDTO.setNombreArchivoOriginal(fileName);
		controlArchivoDTO.setIdToControlArchivo(new BigDecimal(idToControlArchivo));		
		return controlArchivoDTO.obtenerNombreArchivoFisico();
	}
	
	public String getFileName(ControlArchivoDTO controlArchivoDTO) {
		return controlArchivoDTO.obtenerNombreArchivoFisico();
	}
	
	@EJB
	public void setParametroGeneralFacade(ParametroGeneralFacadeRemote parametroGeneralFacade){
		this.parametroGeneralFacade = parametroGeneralFacade;
	}
	
	public boolean uploadFileCG(String fileName, byte[] fileData, boolean highAvailability) {
		try {
			LOG.info("--uploadFileCG() {}"+fileName);
			
			final String gaveta = this.traerParametroGral(GRUPO_PARAMETRO_FORTIMAX, PARAMETRO_GAVETA_CG); 
			final String[] expediente = {this.traerParametroGral(GRUPO_PARAMETRO_FORTIMAX, PARAMETRO_EXPEDIENTE_DANOS)};
			final String carpeta = this.traerParametroGral(GRUPO_PARAMETRO_FORTIMAX,PARAMETRO_CARPETA_DANOS);
			final String[] response = documentoService.uploadFile(fileName, fileData, gaveta, expediente, carpeta);
			//LOG.debug("--uploadFileCG() Resultado [{}] [{}] [{}]", response[0], response[1], response[2]);
			boolean result = BooleanUtils.toBoolean(response[0]);
			if (!result) {
				throw new Exception(String.format("No se pudo cargar el archivo [%s] en fortimax", fileName));
			}
			return result;
		} catch(Exception e){
			LOG.error("-- uploadFileCG()", e);	
			return false; 
		}	
	}
	
	public byte[] downloadFileCG(String fileName) {
		try {
			final String token = getToken(60);
			final String nodo = getNodoCG(fileName);
			final byte[] byteArray = makeZipService.descargaBytesPagina(token, nodo, 0); 
			if( byteArray == null) {
				throw new Exception(String.format("No se encontrï¿½ el archivo [%s] en el repositorio, ", fileName));
			}
			return byteArray;
		} catch (Exception e) {
			LOG.error("-- downloadFileCG()", e);
			return null;
		}
	}

	private String getNodoCG(String fileName) {
		try {
			final String token = getToken(60);
			final String gaveta = this.traerParametroGral(GRUPO_PARAMETRO_FORTIMAX, PARAMETRO_GAVETA_CG); 
			final String expediente = this.traerParametroGral(GRUPO_PARAMETRO_FORTIMAX, PARAMETRO_EXPEDIENTE_DANOS);
			final String fieldName = this.traerParametroGral(GRUPO_PARAMETRO_FORTIMAX, PARAMETRO_FIELDNAME_DANOS);
			final String[] respuesta = busquedaService.getNodo(token, gaveta, fieldName, expediente, fileName, CLAVE_DOCUMENTO);
			final String result = respuesta[0].substring(0, respuesta[0].indexOf(" "));
			return result;
		} catch (Exception e) {
			LOG.error("-- getNodoCG()", e);
			return null;
		}
	}
	
	public String traerParametroGral( BigDecimal grupo, BigDecimal param ){
		
		try {
			return parametroGeneralFacade.findById(
					new ParametroGeneralId(grupo, param), true).getValor();
		} catch (Exception e) {
			LOG.error("-- obtenerParametroGral()", e);
			return null;
		}
	}

}
