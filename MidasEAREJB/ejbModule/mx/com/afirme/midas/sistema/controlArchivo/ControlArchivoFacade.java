package mx.com.afirme.midas.sistema.controlArchivo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ControlArchivoDTO.
 * 
 * @see .ControlArchivoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ControlArchivoFacade implements ControlArchivoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private FileManagerService fileManagerService;

	/**
	 * Perform an initial save of a previously unsaved ControlArchivoDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ControlArchivoDTO entity to persist
	 * @return
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public ControlArchivoDTO save(ControlArchivoDTO entity) {
		LogDeMidasEJB3.log("saving ControlArchivoDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ControlArchivoDTO entity.
	 * 
	 * @param entity
	 *            ControlArchivoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ControlArchivoDTO entity) {
		LogDeMidasEJB3.log("deleting ControlArchivoDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(ControlArchivoDTO.class, entity
					.getIdToControlArchivo());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ControlArchivoDTO entity and return it or a
	 * copy of it to the sender. A copy of the ControlArchivoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            ControlArchivoDTO entity to update
	 * @return ControlArchivoDTO the persisted ControlArchivoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ControlArchivoDTO update(ControlArchivoDTO entity) {
		LogDeMidasEJB3.log("updating ControlArchivoDTO instance", Level.INFO,
				null);
		try {
			ControlArchivoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ControlArchivoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding ControlArchivoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			ControlArchivoDTO instance = entityManager.find(
					ControlArchivoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ControlArchivoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ControlArchivoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ControlArchivoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ControlArchivoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding ControlArchivoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ControlArchivoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ControlArchivoDTO entities.
	 * 
	 * @return List<ControlArchivoDTO> all ControlArchivoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ControlArchivoDTO> findAll() {
		LogDeMidasEJB3.log("finding all ControlArchivoDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from ControlArchivoDTO model";
			Query query = entityManager.createQuery(queryString);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * 
	 * @param in
	 * @param out
	 */
	public void copy(ControlArchivoDTO in, ControlArchivoDTO out) {
		String fileInName = in.getNombreArchivoOriginal();
		byte[] origin = fileManagerService.downloadFile(fileInName, in.getIdToControlArchivo().toString());
		final String extension = (fileInName .lastIndexOf(".") == -1) ? "" : fileInName
				.substring(fileInName.lastIndexOf("."), fileInName.length());
		fileManagerService.uploadFile(out.getIdToControlArchivo().intValue() + extension, out.getIdToControlArchivo().toString(), origin);
	}

}