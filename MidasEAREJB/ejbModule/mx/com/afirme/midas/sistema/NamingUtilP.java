package mx.com.afirme.midas.sistema;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.SystemException;

/**
 * This class specializes in common JNDI utilities.
 * 
 * @author Marsil Benavides @ mantenimeinto Equipo MIDAS
 * @version 1.00, June 03, 2008
 */
public class NamingUtilP {

	// private static final Logger LOG = Logger.getLogger(NamingUtil.class);

	private static final String[] PREFIX_LIST = new String[] { "ejbLocal:", "",
			"java:", "java:comp/env/"};

	private static final NamingUtilP INSTANCE = new NamingUtilP();

	private final Map<String, String> statistics;

	private Context context = null;

	private NamingUtilP() {
		this.statistics = new HashMap<String, String>();
	}


	private void initializeContext() throws SystemException {
		NamingUtilP.closeQuietly(this.context);
		try {
			
//			Hashtable env = new Hashtable();
//			env.put(Context.INITIAL_CONTEXT_FACTORY,"com.ibm.websphere.naming.WsnInitialContextFactory");
//			env.put(Context.PROVIDER_URL,"corbaloc:iiop:192.168.1.206:2809");
//			this.context = new InitialContext(env);
			
			this.context = new InitialContext();
		} catch (NamingException nException) {
			throw new SystemException("No es posible inicializar el contexto.");
		} // End of try/catch
		// LogDeMidasWeb("Context Initialized to: " + this.context);
	}

	/**
	 * Close a <code>Context</code>, avoid closing if null and hide any
	 * NamingException that occur.
	 * 
	 * @param context
	 *            The <code>Context</code> instance to be closed.
	 */
	public static void closeQuietly(Context context) {
		if (context != null) {
			try {
				context.close();
			} catch (NamingException nException) {
				LogDeMidasEJB3.log("no se puede cerrar el contexto: "
						+ NamingUtilP.getEnvironment(context), Level.SEVERE,
						null);
			} // End of try/catsh
		} // End of if
	}

	public static Map<?, ?> getEnvironment(Context context) {
		Map<?, ?> environment = null;
		try {
			environment = context.getEnvironment();
		} catch (NamingException nException) {
			LogDeMidasEJB3.log(
					"No se puede obtener el entorno para el contexto: "
							+ context, Level.SEVERE, null);
		} // End of try/catch
		return environment;
	}

	/**
	 * Retrieves the requested named object.
	 * 
	 * @param reference
	 *            The name of the object to look up.
	 * @return The object bound to <code>reference</code>.
	 * @throws SystemException
	 *             If an unexpected naming exception is encountered.
	 */
	public static Object lookup(String reference) throws SystemException {
		Object namedObject = null;
		NamingUtilP namingUtil = NamingUtilP.INSTANCE;
		String foundPreviouslyInPrefix = namingUtil.statistics.get(reference);

		if (namingUtil.context == null) {
			namingUtil.initializeContext();
		} // End of if

		if ((foundPreviouslyInPrefix != null) && (foundPreviouslyInPrefix.trim().length() > 0)) {
			try {
				namedObject = namingUtil.context.lookup(foundPreviouslyInPrefix
						+ reference);
			} catch (NamingException nException) {
				throw new SystemException(
						"Indispuesto para buscar objetos EJB \"" + namedObject
								+ "\".");
			} // End of try/catch
		} else {
			for (int i = 0; i < NamingUtilP.PREFIX_LIST.length; i++) {
				try {
					namedObject = namingUtil.context
							.lookup(NamingUtilP.PREFIX_LIST[i] + reference);
					namingUtil.statistics.put(reference,
							NamingUtilP.PREFIX_LIST[i]);
					break;
				} catch (NamingException nException) {
					LogDeMidasEJB3.log(" No conexion a EJB con prefix \""
							+ NamingUtilP.PREFIX_LIST[i] + "\".", Level.INFO,
							null);
				} // End of try/catch
			} // End of for
		} // End of if/else

		if (namedObject == null) {
			namingUtil.statistics.remove(reference);
			throw new SystemException("No disponible la busqueda del objeto \""
					+ reference + "\".");
		} // End of if

 		return namedObject;
	}

	public static void close(Context context) {
		if (context != null) {
			try {
				context.close();
			} catch (NamingException nException) {
				LogDeMidasEJB3.log("Unable to close context: "
						+ NamingUtilP.getEnvironment(context), Level.INFO, null);
			} // End of try/catch
		} // End of if
	}
}
