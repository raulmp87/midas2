package mx.com.afirme.midas.sistema;

import javax.rmi.PortableRemoteObject;
import javax.transaction.SystemException;

import com.js.util.NamingUtil;

import mx.com.afirme.midas.ambiente.Config;

/**
 * This class will retrieve requested resources.
 * 
 * @author Grupo de Desarrollo MIDAS
 * @version 1.00, March 31, 2009
 */
public final class ServiceLocatorP {

	private static final ServiceLocatorP INSTANCE = new ServiceLocatorP();

	/**
	 * Retrieves the <code>ServiceLocator</code> singleton instance.
	 * 
	 * @return The <code>ServiceLocator</code> singleton instance.
	 */
	public static ServiceLocatorP getInstance() {
		return ServiceLocatorP.INSTANCE;
	}

	private ServiceLocatorP() {
		// Prevents Instantiation.
	}

	/**
	 * Retrieves the requested Enterprise Java Bean associated to the given
	 * <code>reference</code>.
	 * 
	 * @param theInterface
	 *            The interface of the SSB.
	 * @return The stateless session bean implementation
	 * @throws SystemException
	 *             If the ejb could not be located.
	 */
	
	@SuppressWarnings("unchecked")
	public <T extends Object> T getEJB(Class<T> theInterface)
			throws SystemException {
		T service = null;
		if (System.getProperty("java.vendor").startsWith("IBM Corporation")) {
			service = (T) NamingUtilP.lookup(theInterface.getName());
			service = (T) PortableRemoteObject.narrow(service, theInterface);
		} else {
			String simpleName = theInterface.getSimpleName();
			simpleName = simpleName.replace("Remote", "Bean");
			return (T) NamingUtilP.lookup(simpleName);
		} // End of if/else
		return service;
	}

	/**
	 * Retrieves the requested Enterprise Java Bean associated to the given
	 * <code>reference</code>.
	 * 
	 * @param theInterface
	 *            name of The interface of the SSB.
	 * @return The stateless session bean implementation
	 * @throws SystemException
	 *             If the ejb could not be located.
	 * @throws ClassNotFoundException
	 *             If the class could not be generated.
	 */

	@SuppressWarnings("unchecked")
	public <T extends Object> T getEJB(String theInterface)
			throws SystemException, ClassNotFoundException {

		Class remoteClass = Class.forName(theInterface);

		return (T) this.getEJB(remoteClass);
	}

	@SuppressWarnings("finally")
	public String obtenerVariableEntorno(String nombreVariable) {
		String myVariable = null;
		try {
			Config config = (Config) NamingUtil.lookup("MyConstants");
			myVariable = (String) config.getAttribute(nombreVariable);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return myVariable;
		}
	}	
}
