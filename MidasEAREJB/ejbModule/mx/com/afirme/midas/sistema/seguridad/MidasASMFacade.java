package mx.com.afirme.midas.sistema.seguridad;

import java.util.List;
import java.util.logging.Level;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.asm.dto.PageConsentDTO;
import com.asm.dto.UserDTO;
import com.asm.ejb.ASMRemote;
import com.asm.ejb.RoleRemote;
import com.asm.web.webservice.ASMWSC;
import com.asm.web.webservice.RoleWSC;
import com.js.service.SystemException;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

/**
 * @deprecated desde Midas 2.0. Utilizar {@link mx.com.afirme.midas2.service.seguridad.UsuarioService}
 */
@Deprecated
@Stateless
public class MidasASMFacade implements MidasASMFacadeRemote {

	private RoleRemote roleRemote;
	private ASMRemote asmRemote;
	@EJB
	private SistemaContext sistemaContext;


	@PostConstruct
	private void init() {
		this.asmRemote = new ASMWSC(sistemaContext.getAsmWscContext());
		this.roleRemote = new RoleWSC(sistemaContext.getAsmWscContext());
	}
		
	/**
	 * Metodo que hace logout en ASM a un usuario especifico
	 * 
	 * @param nombreUsuario
	 *            nombre del usuario que se desea hacer logout
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return true si fue exitoso, false si no fue exitoso
	 * 
	 */
	public boolean logOutUsuario(String nombreUsuario, int idAccesoUsuario) {
		try {
			return asmRemote.setLogoutUser(nombreUsuario, idAccesoUsuario);
			// return true;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("fallo al logOut del Usuario:" + nombreUsuario
					+ " con el ID:" + idAccesoUsuario, Level.SEVERE, re);
			throw re;

		} catch (SystemException e) {
			LogDeMidasEJB3.log("fallo al logOut del Usuario:" + nombreUsuario
					+ " con el ID:" + idAccesoUsuario, Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Metodo que bloquea un usuario deacuerdo al IdUsuario
	 * 
	 * @param idUsuario
	 *            idUsuario que se pretende bloquear
	 */
	public void bloqueaAcceso(int idUsuario) {
		try {
			asmRemote.blockAccess(idUsuario);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("fallo al bloquear el acceso del Id Usuario:"
					+ idUsuario, Level.SEVERE, re);
			throw re;
		} catch (SystemException e) {
			LogDeMidasEJB3.log("fallo al bloquear el acceso del Id Usuario:"
					+ idUsuario, Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Metodo que obtiene una lista de Consentimientos por pagina de acuerdo a
	 * un idRole especifico
	 * 
	 * @param idRol
	 *            id del rol que se desea consultar
	 * @return List<PageConsentDTO> Lista de consentimientos por pagina que
	 *         corresponden al id del rol solicitado
	 */
	public List<PageConsentDTO> obtieneListaConsentimientos(int idRole) {
		try {
			return roleRemote.getPageConsentList(idRole);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"fallo al obtener la lisa de consentimientos del roleId:"
							+ idRole, Level.SEVERE, re);
			throw re;
		} catch (SystemException e) {
			LogDeMidasEJB3.log(
					"fallo al obtener la lisa de consentimientos del roleId:"
							+ idRole, Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Metodo que obtiene un Usuario Registrado por nombre de usuario especifico
	 * 
	 * @param nombreUsuario
	 *            nombre del usuario que se desea consultar
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return UserDTO usuario solicitado
	 * 
	 */
	public UserDTO obtieneUsuarioRegistrado(String nombreUsuario,
			int idAccesoUsuario) {
		try {
			return asmRemote.getLoggedUser(nombreUsuario, idAccesoUsuario);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("fallo al obtener el Usuario Registrado:"
					+ nombreUsuario, Level.SEVERE, re);
			throw re;
		} catch (SystemException e) {
			LogDeMidasEJB3.log("fallo al obtener el Usuario Registrado:"
					+ nombreUsuario, Level.SEVERE, e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			LogDeMidasEJB3.log("fallo al obtener el Usuario Registrado:"
					+ nombreUsuario, Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}
	/**
	 * Metodo que obtiene un Usuario Registrado por nombre de usuario especifico
	 * 
	 * @param nombreUsuario
	 *            nombre del usuario que se desea consultar
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return UserDTO usuario solicitado
	 * 
	 */
	public UserDTO obtieneUsuario(String nombreUsuario) {
		try {
			return asmRemote.getUser(nombreUsuario, UserDTO.class);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("fallo al obtener el Usuario"
					+ nombreUsuario, Level.SEVERE, re);
			throw re;
		} catch (SystemException e) {
			LogDeMidasEJB3.log("fallo al obtener el Usuario"
					+ nombreUsuario, Level.SEVERE, e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			LogDeMidasEJB3.log("fallo al obtener el Usuario"
					+ nombreUsuario, Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Metodo que obtiene un Usuario Registrado por nombre de usuario especifico
	 * 
	 * @param nombreUsuario
	 *            nombre del usuario que se desea consultar
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return UserDTO usuario solicitado
	 * 
	 */
	public UserDTO obtieneUsuario(int idUsuario) {
		try {
			return asmRemote.getUser(idUsuario, UserDTO.class);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("fallo al obtener el idUsuario"
					+ idUsuario, Level.SEVERE, re);
			throw re;
		} catch (SystemException e) {
			LogDeMidasEJB3.log("fallo al obtener el idUsuario"
					+ idUsuario, Level.SEVERE, e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			LogDeMidasEJB3.log("fallo al obtener el idUsuario"
					+ idUsuario, Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}	
	/**
	 * Metodo que obtiene un Usuario sin roles por un id de usuario especifico
	 * 
	 * @param idUsuario
	 *            id del usuario que se desea consultar
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return UserDTO usuario solicitado
	 * 
	 */
	public UserDTO obtieneUsuarioSinRolesPorIdUsuario(int idUsuario,
			int idAccesoUsuario) {
		try {
			return asmRemote.getUserWithOutRolesByUserId(idUsuario,
					idAccesoUsuario);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("fallo al obtener el Usuario sin roles:"
					+ idUsuario, Level.SEVERE, re);
			throw re;
		} catch (SystemException e) {
			LogDeMidasEJB3.log("fallo al obtener el Usuario sin roles:"
					+ idAccesoUsuario, Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Metodo que obtiene un Usuario sin roles por un nombre de usuario
	 * especifico
	 * 
	 * @param nombreUsuario
	 *            nombre del usuario que se desea consultar
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return UserDTO usuario solicitado
	 * 
	 */
	public UserDTO obtieneUsuarioSinRolesPorNombreUsuario(String nombreUsuario,
			int idAccesoUsuario) {
		try {
			return asmRemote.getUserWithOutRolesByUserName(nombreUsuario,
					idAccesoUsuario);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"fallo al obtener el Usuario sin roles por nombre: "
							+ nombreUsuario, Level.SEVERE, re);
			throw re;
		} catch (SystemException e) {
			LogDeMidasEJB3.log(
					"fallo al obtener el Usuario sin roles por nombre: "
							+ nombreUsuario, Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Metodo que obtiene una lista de Usuarios sin roles por un id de rol
	 * especifico
	 * 
	 * @param idRol
	 *            id del rol que se desea consultar
	 * @param nombreUsuario
	 *            nombre del usuario que desea hacer la peticion
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return List<UserDTO> Lista de usuarios que corresponden al id del rol
	 *         solicitado
	 */
	public List<UserDTO> obtieneUsuariosSinRolesPorIdRol(int idRol,
			String nombreUsuario, int idAccesoUsuario) {
		try {
			return asmRemote.getUsersWithOutRolesByRoleId(idRol, nombreUsuario,
					idAccesoUsuario);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"fallo al obtener el Usuario sin roles por idRol: " + idRol
							+ " Nombre Usuario: " + nombreUsuario,
					Level.SEVERE, re);
			throw re;
		} catch (SystemException e) {
			LogDeMidasEJB3.log(
					"fallo al obtener el Usuario sin roles por idRol: " + idRol
							+ " Nombre Usuario: " + nombreUsuario,
					Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Metodo que obtiene una lista de Usuarios sin roles por un nombre de rol
	 * especifico
	 * 
	 * @param nombreRol
	 *            nombre del rol que se desea consultar
	 * @param nombreUsuario
	 *            nombre del usuario que desea hacer la peticion
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return List<UserDTO> Lista de usuarios que corresponden al nombre del
	 *         rol solicitados
	 */
	public List<UserDTO> obtieneUsuariosSinRolesPorNombreRol(String nombreRol,
			String nombreUsuario, int idAccesoUsuario) {
		try {
			return asmRemote.getUsersWithOutRolesByRoleName(nombreRol,
					nombreUsuario, idAccesoUsuario);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"fallo al obtener el Usuario sin roles por nombre rol: "
							+ nombreRol + " Nombre Usuario: " + nombreUsuario,
					Level.SEVERE, re);
			throw re;
		} catch (SystemException e) {
			LogDeMidasEJB3.log(
					"fallo al obtener el Usuario sin roles por nombre rol: "
							+ nombreRol + " Nombre Usuario: " + nombreUsuario,
					Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}

}
