package mx.com.afirme.midas.sistema.log;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity LogOperacionDTO.
 * 
 * @see mx.com.afirme.midas.sistema.log.LogOperacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class LogOperacionFacade implements LogOperacionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved LogOperacionDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            LogOperacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(LogOperacionDTO entity) {
		LogUtil.log("saving LogOperacionDTO instance", Level.INFO, null);
		try {
			//Se agrega la fecha y hora que se registra en la bit�cora
			entity.setFecha(new Timestamp(new Date().getTime()));
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent LogOperacionDTO entity.
	 * 
	 * @param entity
	 *            LogOperacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(LogOperacionDTO entity) {
		LogUtil.log("deleting LogOperacionDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(LogOperacionDTO.class, entity
					.getIdtooperacionlog());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Encuentra todos los registros de log que deben ser 
	 * transferidos al historico.
	 * @param maximoRegistros Numero maximo de registros que deben permanecer en el log. Si se sobrepasa este numero, se tienen que 
	 * respaldar en el historico
	 * @return List<LogOperacionDTO> Los registros de log que deben ser
	 *  transferidos al historico
	 */
	@SuppressWarnings("unchecked")
	public List<LogOperacionDTO> buscarRegistrosParaRespaldo(String maximoRegistros) {
		try {
			/*
			 * La politica original ha sido establecida en mandar a respaldar
			 * los registros de la bit�cora cuando �stos excedan los 100,000
			 * registros 
			 */
						
			Long numeroRegistros = new Long(0);
			StringBuffer sb = new StringBuffer();
			
			sb.append("Select count(model.idtooperacionlog) from LogOperacionDTO model");
			
			Query query = entityManager.createQuery(sb.toString());
			
			numeroRegistros = (Long)query.getSingleResult();
			
			
			if (numeroRegistros.compareTo(new Long(maximoRegistros)) >= 0) {
				sb = new StringBuffer();
				sb.append("Select model from LogOperacionDTO model");
				
				query = entityManager.createQuery(sb.toString());
				query.setMaxResults(Integer.parseInt(maximoRegistros));
				//Obliga al persistence context a refrescar su cache con los datos del query en la BD
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
			}
		} catch (Exception re) {
			re.printStackTrace();
		}
		return null;
	}

	

}