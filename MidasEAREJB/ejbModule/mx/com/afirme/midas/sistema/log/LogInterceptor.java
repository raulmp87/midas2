package mx.com.afirme.midas.sistema.log;

import java.util.logging.Level;

import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.naming.Context;
import javax.naming.InitialContext;

import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.usuario.LogUtil;

public class LogInterceptor {

	private static final String ACCION_GUARDAR = "GUARDAR";
	private static final String ACCION_BORRAR = "BORRAR";
	private static final String ACCION_MODIFICAR = "MODIFICAR";
	
	@EJB
	private LogOperacionFacadeRemote logFacade;
	
	
	 public LogInterceptor() {
     }
 
	 
	 private String obtieneTipoAccion (String nombreMetodo) {
		 
		 if (nombreMetodo.equals("save")) {
			 return ACCION_GUARDAR;
		 } else if (nombreMetodo.equals("update")) {
			 return ACCION_MODIFICAR;
		 } else if (nombreMetodo.equals("delete")) {
			 return ACCION_BORRAR;
		 } else {
			 return ACCION_MODIFICAR;
		 }
	 }
	 
	private LogOperacionDTO obtieneObjetoLog(Object objeto, String tipoAccion) {
		
		LogOperacionDTO logOperacionDTO = new LogOperacionDTO();
		
		if (objeto.getClass().getSimpleName().equals("ContratoPrimerExcedenteDTO")) {
       	 
       	 ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO = (ContratoPrimerExcedenteDTO) objeto;
       	 
       	 logOperacionDTO.setIdregistro(contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente());
       	 logOperacionDTO.setNombreentidad("ContratoPrimerExcedenteDTO");
       	 logOperacionDTO.setNombretablaentidad("TMCONTRATOPRIMEREXCEDENTE");
       	 logOperacionDTO.setNombreusuario(contratoPrimerExcedenteDTO.getNombreUsuarioLog());
       	 logOperacionDTO.setTipoaccion(tipoAccion);
       	 logOperacionDTO.setTipoentidad("Contrato de Reaseguro");
       	 
        } else if (objeto.getClass().getSimpleName().equals("ContratoCuotaParteDTO")) {
	       	 
        	ContratoCuotaParteDTO contratoCuotaParteDTO = (ContratoCuotaParteDTO) objeto;
	       	 
	       	logOperacionDTO.setIdregistro(contratoCuotaParteDTO.getIdTmContratoCuotaParte());
	       	logOperacionDTO.setNombreentidad("ContratoCuotaParteDTO");
	       	logOperacionDTO.setNombretablaentidad("TMCONTRATOCUOTAPARTE");
	       	logOperacionDTO.setNombreusuario(contratoCuotaParteDTO.getNombreUsuarioLog());
	       	logOperacionDTO.setTipoaccion(tipoAccion);
	       	logOperacionDTO.setTipoentidad("Contrato de Reaseguro");
	       	 
	    } else if (objeto.getClass().getSimpleName().equals("ContratoFacultativoDTO")) {
	       	 
	    	ContratoFacultativoDTO contratoFacultativoDTO = (ContratoFacultativoDTO) objeto;
	       	 
	       	logOperacionDTO.setIdregistro(contratoFacultativoDTO.getIdTmContratoFacultativo());
	       	logOperacionDTO.setNombreentidad("ContratoFacultativoDTO");
	       	logOperacionDTO.setNombretablaentidad("TMCONTRATOFACULTATIVO");
	       	logOperacionDTO.setNombreusuario(contratoFacultativoDTO.getNombreUsuarioLog());
	       	logOperacionDTO.setTipoaccion(tipoAccion);
	       	logOperacionDTO.setTipoentidad("Contrato de Reaseguro");
	       	 
	    } else if (objeto.getClass().getSimpleName().equals("ReporteSiniestroDTO")) {
	       	 
	    	ReporteSiniestroDTO reporteSiniestroDTO = (ReporteSiniestroDTO) objeto;
	       	 
			logOperacionDTO.setIdregistro(reporteSiniestroDTO.getIdToReporteSiniestro());
			logOperacionDTO.setNombreentidad("ReporteSiniestroDTO");
			logOperacionDTO.setNombretablaentidad("TOREPORTESINIESTRO");
			logOperacionDTO.setNombreusuario(reporteSiniestroDTO.getNombreUsuarioLog());
			logOperacionDTO.setTipoaccion(tipoAccion);
			logOperacionDTO.setTipoentidad("Reporte Siniestros");
	       	 
	    } else if (objeto.getClass().getSimpleName().equals("CotizacionDTO")) {
	       	 
	    	CotizacionDTO cotizacionDTO = (CotizacionDTO) objeto;
	       	 
	       	logOperacionDTO.setIdregistro(cotizacionDTO.getIdToCotizacion());
	       	logOperacionDTO.setNombreentidad("CotizacionDTO");
	       	logOperacionDTO.setNombretablaentidad("TOCOTIZACION");
	       	logOperacionDTO.setNombreusuario(cotizacionDTO.getNombreUsuarioLog());
	       	logOperacionDTO.setTipoaccion(tipoAccion);
	       	logOperacionDTO.setTipoentidad("Cotizacion");
	       	 
	    }
		
		
		
		return logOperacionDTO;
		
	}
	 
	 
	 
	 
 @AroundInvoke
 public Object doLog(InvocationContext invCtx) throws Exception {
	 Object result = null;
	 try {
		 
		result = invCtx.proceed();
        
        Object dtoObject = invCtx.getParameters()[0];
        LogOperacionDTO logOperacionDTO = obtieneObjetoLog (dtoObject, obtieneTipoAccion(invCtx.getMethod().getName()));
         
		logFacade.save(logOperacionDTO);
	   
         
             
     } catch (Exception ex) {
    	 LogUtil.log("Fallo al registrar el Log, posible causa de fallo en la persistencia!!", Level.SEVERE, null);
    	 throw ex;
     } 
    
     return result;
     
 }

	
	
}
