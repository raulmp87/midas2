package mx.com.afirme.midas.sistema.log.historico;

import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity LogOperacionHistDTO.
 * 
 * @see mx.com.afirme.midas.sistema.log.historico.LogOperacionHistDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class LogOperacionHistFacade implements LogOperacionHistFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved LogOperacionHistDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            LogOperacionHistDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(LogOperacionHistDTO entity) {
		LogUtil.log("saving LogOperacionHistDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	
}