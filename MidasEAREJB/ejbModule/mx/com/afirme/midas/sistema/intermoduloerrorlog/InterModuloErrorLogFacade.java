package mx.com.afirme.midas.sistema.intermoduloerrorlog;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity Tointermoderrorlog.
 * 
 * @see mx.com.afirme.midas.sistema.intermoduloerrorlog.Tointermoderrorlog
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class InterModuloErrorLogFacade implements InterModuloErrorLogFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved Tointermoderrorlog
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Tointermoderrorlog entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(InterModuloErrorLogDTO entity) {
		LogUtil.log("saving Tointermoderrorlog instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent InterModuloErrorLogDTO entity.
	 * 
	 * @param entity
	 *            InterModuloErrorLogDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(InterModuloErrorLogDTO entity) {
		LogUtil.log("deleting InterModuloErrorLogDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(InterModuloErrorLogDTO.class,
					entity.getIdToInterModuloErrorLog());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved InterModuloErrorLogDTO entity and return it or a
	 * copy of it to the sender. A copy of the InterModuloErrorLogDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            InterModuloErrorLogDTO entity to update
	 * @return InterModuloErrorLogDTO the persisted InterModuloErrorLogDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public InterModuloErrorLogDTO update(InterModuloErrorLogDTO entity) {
		LogUtil.log("updating InterModuloErrorLogDTO instance", Level.INFO, null);
		try {
			InterModuloErrorLogDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public InterModuloErrorLogDTO findById(BigDecimal id) {
		LogUtil.log("finding InterModuloErrorLogDTO instance with id: " + id,
				Level.INFO, null);
		try {
			InterModuloErrorLogDTO instance = entityManager.find(
					InterModuloErrorLogDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all InterModuloErrorLogDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the InterModuloErrorLogDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<InterModuloErrorLogDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<InterModuloErrorLogDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding InterModuloErrorLogDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from InterModuloErrorLogDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all InterModuloErrorLogDTO entities.
	 * 
	 * @return List<InterModuloErrorLogDTO> all InterModuloErrorLogDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<InterModuloErrorLogDTO> findAll() {
		LogUtil.log("finding all InterModuloErrorLogDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from InterModuloErrorLogDTO model";
			Query query = entityManager.createQuery(queryString);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<InterModuloErrorLogDTO> listarFiltrado(InterModuloErrorLogDTO entity) {
		try {
			LogDeMidasEJB3.log("entrando a listarFiltrado(InterModuloErrorLogDTO)", Level.FINE, null);
			String queryString = "select model from InterModuloErrorLogDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "comentariosAdicionales", entity.getComentariosAdicionales());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "descripcionError", entity.getDescripcionError());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "excepcion", entity.getExcepcion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "fechaHora", entity.getFechaHora());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToInterModuloErrorLog", entity.getIdToInterModuloErrorLog());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "metodoCapturaError", entity.getMetodoCapturaError());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "metodoOrigenError", entity.getMetodoOrigenError());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "moduloCapturaError", entity.getModuloCapturaError());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "moduloOrigenError", entity.getModuloOrigenError());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombreUsuario", entity.getNombreUsuario());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "objetoCapturaError", entity.getObjetoCapturaError());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "objetoOrigenError", entity.getObjetoOrigenError());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "paramMetodoOrigenError", entity.getParamMetodoOrigenError());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "retornoMetodoorigenError", entity.getRetornoMetodoorigenError());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("error en listarFiltrado(InterModuloErrorLogDTO)", Level.SEVERE, re);
			throw re;
		}finally{
			LogDeMidasEJB3.log("saliendo de listarFiltrado(InterModuloErrorLogDTO)", Level.FINE, null);
		}
	}
}