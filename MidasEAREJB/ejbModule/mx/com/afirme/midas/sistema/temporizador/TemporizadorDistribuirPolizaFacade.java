package mx.com.afirme.midas.sistema.temporizador;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroFacadeRemote;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroServicios;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroServiciosRemote;
import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableDTO;
import mx.com.afirme.midas.reaseguro.distribucion.DistribucionReaseguroServicios;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroFacadeRemote;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.eclipse.persistence.exceptions.TransactionException;

@Stateless
public class TemporizadorDistribuirPolizaFacade implements TemporizadorDistribuirPolizaFacadeRemote {
	public static final String CLAVE_TIPO_TAREA = "tipoTarea";
	public static final String IDENTIFICADOR_TIMER_DISTRIBUCION = "timerDistribucionpoliza";
	@EJB
	private MovimientoReaseguroServiciosRemote movimientoReaseguroServicios;
	@EJB
	private LineaSoporteReaseguroFacadeRemote lineaSoporteReaseguroFacade;
	
	@EJB
	private MovimientoReaseguroFacadeRemote movimientoReaseguroFacade;
	
	/**
	 * Inicia el temporizador
	 * @param tiempoIniciar Tiempo para que se inicie el temporizador (en milisegundos)
	 * @param soporteReaseguroDTO Este parametro es el que se le va a mandar el timer para
	 * como parametro info se espera que este objeto tenga establecido estos dos valores:
	 * idToPoliza y numeroEndoso.
	 */
	public void iniciarTemporizador(long tiempoIniciar, SoporteReaseguroDTO soporteReaseguroDTO) {
		if(sistemaContext.getTimerActivo()) {
			if (soporteReaseguroDTO == null) {
				throw new IllegalArgumentException("soporteReaseguroDTO no puede ser nulo.");
			}
			
			HashMap<String, Object> infoMap = new HashMap<String, Object>();
			infoMap.put("info", soporteReaseguroDTO); //Esto se hace para poder guardar en este HashMap mas adelante el numero de intentos.
			infoMap.put("intentos", 0);
			//Se agrega un identificador para indicar que se trata de un timer de distribuci�n de p�lizas.
			infoMap.put(CLAVE_TIPO_TAREA, IDENTIFICADOR_TIMER_DISTRIBUCION);
			timerService.createTimer(tiempoIniciar, infoMap);
			LogDeMidasEJB3.log("Temporizador para Distribuir POLIZA (createTimer) tiempoIniciar:("+tiempoIniciar+") idToPoliza:"+ soporteReaseguroDTO.getIdToPoliza() +" idToSoporteReaseguro: "+ soporteReaseguroDTO.getIdToSoporteReaseguro() +", timerService.getTimers().size(): " + timerService.getTimers().size(),
					Level.INFO, null);
		}		

	}
	
	/**
	 * Inicia el temporizador
	 * @param tiempoIniciar Tiempo para que se inicie el temporizador (en milisegundos)
	 * @param idToSoporteReaseguro Este parametro es el id del soporte que se distribuir�.
	 */
	public void iniciarTemporizador(long tiempoIniciar, BigDecimal idToSoporteReaseguro) {
		if(sistemaContext.getTimerActivo()) {
			if (idToSoporteReaseguro == null) {
				throw new IllegalArgumentException("idToSoporteReaseguro no puede ser nulo. idToSoporteReaseguro: "+idToSoporteReaseguro);
			}
			
			HashMap<String, Object> infoMap = new HashMap<String, Object>();
			infoMap.put("idToSoporteReaseguro", idToSoporteReaseguro); //Esto se hace para poder guardar en este HashMap mas adelante el numero de intentos.
			infoMap.put("intentos", 0);
			//Se agrega un identificador para indicar que se trata de un timer de distribuci�n de p�lizas.
			infoMap.put(CLAVE_TIPO_TAREA, IDENTIFICADOR_TIMER_DISTRIBUCION);
			timerService.createTimer(tiempoIniciar, infoMap);
			LogDeMidasEJB3.log("Temporizador para Distribuir POLIZA (createTimer) tiempoIniciar:("+tiempoIniciar+") idToSoporteReaseguro: "+ idToSoporteReaseguro +", timerService.getTimers().size(): " + timerService.getTimers().size(),
					Level.INFO, null);
		}		

	}
	
	/**
	 * Detiene los temporizadores de distribuci�n de p�lizas.
	 */
	@SuppressWarnings("unchecked")
	public void detenerTemporizadoresDistribucionPolizas() {
		try{
			LogDeMidasEJB3.log("Se intentar� detener los temporizadores para Distribuir p�lizas. timerService.getTimers().size(): " + timerService.getTimers().size(),Level.INFO, null);
			HashMap<String, Object> infoMap = new HashMap<String, Object>();
			
			int timersDistribucionPolizas = 0;
			for (Object obj : timerService.getTimers()) {
				Timer timer = (Timer)obj;
				
				Object infoTimer = timer.getInfo();
				
				if(infoTimer instanceof HashMap){
					infoMap = (HashMap<String, Object>) timer.getInfo();
					
					Object tipoTareaObj = infoMap.get(CLAVE_TIPO_TAREA);
					if(tipoTareaObj != null && tipoTareaObj instanceof String){
						String tipoTareaStr = (String) tipoTareaObj;
						if(tipoTareaStr.equalsIgnoreCase(IDENTIFICADOR_TIMER_DISTRIBUCION)){
							timer.cancel();
							timersDistribucionPolizas++;
						}
					}
				}
			}
	
			LogDeMidasEJB3.log("Se detuvieron "+timersDistribucionPolizas+ " timer(s) de distribuci�n de p�liza.", Level.INFO, null);
		}catch(RuntimeException re){
			LogDeMidasEJB3.log("Ocurri� un error al intentar detener los timers de distribuci�n de p�liza.", Level.SEVERE, re);
		}

	}
	
	@SuppressWarnings("unchecked")
	@Timeout
	public void timeOut(Timer timer) {
		SoporteReaseguroDTO soporteReaseguroDTO = null;
		HashMap<String, Object> infoMap = null;
		int intentos = 0;
		BigDecimal idToSoporteReaseguro;
		
		try {
			infoMap = (HashMap<String, Object>) timer.getInfo();
			soporteReaseguroDTO = (SoporteReaseguroDTO) infoMap.get("info");
			intentos = (Integer) infoMap.get("intentos"); //Obtener el numero de intentos que se han realizado y sumar el actual
			intentos++;
			
			if(soporteReaseguroDTO == null){//Se lanz� el timer usando s�lo el idToSoporteReaseguro.
				if(infoMap.get("idToSoporteReaseguro") != null){
					idToSoporteReaseguro = (BigDecimal)infoMap.get("idToSoporteReaseguro");
					soporteReaseguroDTO = entityManager.find(SoporteReaseguroDTO.class, idToSoporteReaseguro);
				}
				else{
					throw new RuntimeException("No se recibi� informaci�n de soporte reaseguro para la distribuci�n.");
				}
			}
			else{
				idToSoporteReaseguro = soporteReaseguroDTO.getIdToSoporteReaseguro();
			}

			BigDecimal cantidadMovimientosNotificados = movimientoReaseguroFacade.obtenerCantidadMovimientosReaseguro(soporteReaseguroDTO.getIdToPoliza(), soporteReaseguroDTO.getNumeroEndoso(), "contabilizado", "1");
			if (cantidadMovimientosNotificados != null && cantidadMovimientosNotificados.compareTo(BigDecimal.ZERO) == 0){
				LogDeMidasEJB3.log("Temporizador para Distribuir POLIZA (timeOut) INICIA Intento:("+intentos+") idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza() +" , numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso()+" , idToSoporteReaseguro: "+ soporteReaseguroDTO.getIdToSoporteReaseguro() +" , timerService.getTimers().size(): " + timerService.getTimers().size(),Level.INFO, null);
				int cantidadLineasDistribuidas = 0;
				Object estatusLineaSoporteObject = null;
				
				//10/01/2010 Consulta de las lineas pendientes por distribuir, en lugar de acceder a ellas a traves de la lista EAGER.
				String queryString = "select lsr.idtmlineasoportereaseguro from MIDAS.tmlineasoportereaseguro lsr where estatuslineasoporte in ("+
					LineaSoporteReaseguroDTO.EMITIDO_SIN_DISTRIBUIR+","+LineaSoporteReaseguroDTO.EMITIDO_DISTRIBUCION_MASIVA+") and idtosoportereaseguro = "+idToSoporteReaseguro;
				Query query = entityManager.createNativeQuery(queryString);
				List<Object> listaResultObject = query.getResultList();
				List<BigDecimal> listaIdTmLineasPendientes = new ArrayList<BigDecimal>();
				boolean busquedaPorLista = true;
				if(listaResultObject != null){
					for(Object obj : listaResultObject){
						listaIdTmLineasPendientes.add(Utilerias.obtenerBigDecimal(obj));
					}
					cantidadLineasDistribuidas = lineaSoporteReaseguroFacade.contarLineaSoporteReaseguroPorPropiedad(soporteReaseguroDTO.getIdToSoporteReaseguro(), 
							"estatusLineaSoporte", LineaSoporteReaseguroDTO.EMITIDO_Y_DISTRIBUIDO, true);
				}
				else{
					busquedaPorLista = false;
				}	
				
				for(LineaSoporteReaseguroDTO lineaSoporteReaseguro : soporteReaseguroDTO.getLineaSoporteReaseguroDTOs()){
					if(lineaSoporteReaseguro != null){
						if(busquedaPorLista){
							//Se omite la consulta del estatus
							//Si la linea est� en la lista de pendientes por distribuir, se manda distribuir.
							if(listaIdTmLineasPendientes.contains(lineaSoporteReaseguro.getId().getIdTmLineaSoporteReaseguro())){
								listaIdTmLineasPendientes.remove(lineaSoporteReaseguro.getId().getIdTmLineaSoporteReaseguro());
								cantidadLineasDistribuidas = distribuirLinea(lineaSoporteReaseguro, cantidadLineasDistribuidas);
							}
						}
						else{//Si la b�squeda no ser� a trav�s de la lista, se realiza por cada linea una consulta (mas tardado)
							estatusLineaSoporteObject = lineaSoporteReaseguroFacade.obtenerCampoLineaSoporte(lineaSoporteReaseguro.getId().getIdTmLineaSoporteReaseguro(), "estatusLineaSoporte"); 
							Integer estatusLineaSoporte = estatusLineaSoporteObject!=null?((BigDecimal)estatusLineaSoporteObject).intValue():0;
							if(estatusLineaSoporte != null && estatusLineaSoporte.intValue() == LineaSoporteReaseguroDTO.EMITIDO_Y_DISTRIBUIDO)
								cantidadLineasDistribuidas += 1;
							else{
								cantidadLineasDistribuidas = distribuirLinea(lineaSoporteReaseguro, cantidadLineasDistribuidas);
							}
						}
					}
//					Runtime.getRuntime().gc(); Se comenta la invocaci�n al garbageCollector para reducir el tiempo de la distribuci�n
				}
				//Una vez distribuida la poliza, se procede a contabilizarla
				String identificador = soporteReaseguroDTO.getIdToPoliza() + "|" + soporteReaseguroDTO.getNumeroEndoso();
				String claveModulo = MovimientoReaseguroServicios.CLAVE_MODULO_EMISION;
				int cantidadLineasSoporte = lineaSoporteReaseguroFacade.contarLineaSoporteReaseguroPorPropiedad(soporteReaseguroDTO.getIdToSoporteReaseguro(), 
						"id.idToSoporteReaseguro", soporteReaseguroDTO.getIdToSoporteReaseguro(), true);
				if (cantidadLineasDistribuidas == cantidadLineasSoporte){
					LogDeMidasEJB3.log("Se distribuyeron exitosamente "+cantidadLineasSoporte+" lineas del soporte: "+soporteReaseguroDTO.getIdToSoporteReaseguro()+", cotizacion: "+soporteReaseguroDTO.getIdToCotizacion()+"Poliza: "+ soporteReaseguroDTO.getIdToPoliza()+", numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso(), Level.INFO, null);
					cantidadMovimientosNotificados = movimientoReaseguroFacade.obtenerCantidadMovimientosReaseguro(soporteReaseguroDTO.getIdToPoliza(), soporteReaseguroDTO.getNumeroEndoso(), "contabilizado", "1");
					//si no se han notificado movimientos
					if (cantidadMovimientosNotificados != null && cantidadMovimientosNotificados.compareTo(BigDecimal.ZERO) == 0){
						LogDeMidasEJB3.log("Se procede a contabilizar los movimientos para el idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza()+", numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso(), Level.INFO, null);
						List<AsientoContableDTO> asientos = null;
						try{
							asientos = movimientoReaseguroServicios.registrarContabilidad(identificador, claveModulo);
							LogDeMidasEJB3.log("La Notificaci�n a Contabilidad fue Exitosa bajo la referencia : " + identificador ,Level.INFO, null);
						}catch(Exception e){
							LogDeMidasEJB3.log("NO SE PUDIERON CONTABILIZAR LOS MOVIMIENTOS PARA idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza()+", numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso()+"LA DISTRIBUCION FUE EXITOSA.", Level.INFO, null);
						}
						if(asientos != null && !asientos.isEmpty()){
							try{
								movimientoReaseguroFacade.notificarMovimientosContabilidadReaseguro(soporteReaseguroDTO.getIdToPoliza(), soporteReaseguroDTO.getNumeroEndoso(), new Date(), claveModulo+"-"+identificador);
								LogDeMidasEJB3.log("Se contabilizaron satisfactoriamente los movimientos para el idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza()+", numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso(), Level.INFO, null);
							}catch(Exception e){
								LogDeMidasEJB3.log("OCURRI� UN ERROR AL NOTIFICAR lA CONTABILIDAD PARA LOS MOVIMIENTOS DEL idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza()+", numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso()+"LA DISTRIBUCION FUE EXITOSA.", Level.INFO, null);
							}
						} else{
							LogDeMidasEJB3.log("La Contabilidad No retorno ningun asiento contable para acuse de recibo de Contabilidad bajo la referencia: " + identificador ,Level.SEVERE, null);
							LogDeMidasEJB3.log("SE OMITE LA ACTUALIZACION DEL ESTATUS DE LOS MOVIMIENTOS DE LA POLIZA: "+ soporteReaseguroDTO.getIdToPoliza()+", NUMEROENDOSO: "+soporteReaseguroDTO.getNumeroEndoso() ,Level.SEVERE, null);
						}
					} else{//ya se realiz� la notificaci�n a contabilidad, no se hace nada
						LogDeMidasEJB3.log("Se encontraron "+cantidadMovimientosNotificados+" movimientos contabilizados, se omite la contabilidad de los movimientos para el idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza()+", numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso(), Level.INFO, null);
					}
				}else{
					String error = "FALLA la distribucion del soporteReaseguro: "+soporteReaseguroDTO.getIdToSoporteReaseguro()+",idToCotizacion: "+
						soporteReaseguroDTO.getIdToCotizacion()+", idToPoliza: "+soporteReaseguroDTO.getIdToPoliza()+", numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso()+"." +
								" Se registraron movimientos de "+cantidadLineasDistribuidas+" LineaSoporteReaseguro pero el soporte tiene "+cantidadLineasSoporte+" LineaSoporteReaseguro. NO SE REALIZO LA CONTABILIDAD CON SEYCOS.";
					LogDeMidasEJB3.log(error, Level.SEVERE, null);
				}
				LogDeMidasEJB3.log("Temporizador para Distribuir POLIZA (timeOut) TERMINA Intento:("+intentos+") idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza() +" , numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso()+" , idToSoporteReaseguro: "+ soporteReaseguroDTO.getIdToSoporteReaseguro() +" , timerService.getTimers().size(): " + timerService.getTimers().size(),Level.INFO, null);
			}else{
				LogDeMidasEJB3.log("FALLA la distribucion del soporteReaseguro: "+soporteReaseguroDTO.getIdToSoporteReaseguro()+",idToCotizacion: "+soporteReaseguroDTO.getIdToCotizacion()+", idToPoliza: "+soporteReaseguroDTO.getIdToPoliza()+", numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso()+". Ya existen "+cantidadMovimientosNotificados+" movimientos contabilizados para esta p�liza. NO SE REALIZO LA DISTRIBUCION NI CONTABILIDAD CON SEYCOS.", Level.SEVERE, null);
			}
		}
		catch(TransactionException re){
			LogDeMidasEJB3.log("Temporizador para Distribuir POLIZA (timeOut) FALLA en Intento:("+intentos+" de "+TemporizadorReaseguro.NUMERO_INTENTOS_MAXIMO+")  idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza() +" , numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso()+" , idToSoporteReaseguro: "+ soporteReaseguroDTO.getIdToSoporteReaseguro() +" , timerService.getTimers().size(): " + timerService.getTimers().size(),Level.SEVERE, re);

			//Verificar si se debe seguir intentando distribuir esta poliza
			if (intentos < TemporizadorReaseguro.NUMERO_INTENTOS_MAXIMO) {
				if(sistemaContext.getTimerActivo()) {
					infoMap.put("intentos", intentos); //Actualizar en el map el numero de intentos.
					timerService.createTimer(TemporizadorReaseguro.INTERVALO_TIEMPO, infoMap);
					LogDeMidasEJB3.log("Temporizador para Distribuir POLIZA INTENTO:("+intentos+") (createTimer) tiempoIniciar:("+TemporizadorReaseguro.INTERVALO_TIEMPO+") idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza() +" , numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso()+" , idToSoporteReaseguro: "+ soporteReaseguroDTO.getIdToSoporteReaseguro() +" , timerService.getTimers().size(): " + timerService.getTimers().size(),Level.INFO, null);
				}				
			}
			else {
				//TODO:Mandar email
			}
		}
		catch (Exception ex) {
			LogDeMidasEJB3.log("Temporizador para Distribuir POLIZA (timeOut) FALLA en Intento:("+intentos+" de "+TemporizadorReaseguro.NUMERO_INTENTOS_MAXIMO+")  idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza() +" , numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso()+" , idToSoporteReaseguro: "+ soporteReaseguroDTO.getIdToSoporteReaseguro() +" , timerService.getTimers().size(): " + timerService.getTimers().size(),Level.SEVERE, ex);

			//Verificar si se debe seguir intentando distribuir esta poliza
			if (intentos < TemporizadorReaseguro.NUMERO_INTENTOS_MAXIMO) {
				if(sistemaContext.getTimerActivo()) {
					infoMap.put("intentos", intentos); //Actualizar en el map el numero de intentos.
					timerService.createTimer(TemporizadorReaseguro.INTERVALO_TIEMPO, infoMap);
					LogDeMidasEJB3.log("Temporizador para Distribuir POLIZA INTENTO:("+intentos+") (createTimer) tiempoIniciar:("+TemporizadorReaseguro.INTERVALO_TIEMPO+") idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza() +" , numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso()+" , idToSoporteReaseguro: "+ soporteReaseguroDTO.getIdToSoporteReaseguro() +" , timerService.getTimers().size(): " + timerService.getTimers().size(),
							Level.INFO, null);	
				}
			}
			else {
				//TODO:Mandar email
			}
		}
	}

	private int distribuirLinea(LineaSoporteReaseguroDTO lineaSoporteReaseguro,int cantidadLineasDistribuidas){
		distribucionReaseguroServicios.distribuirLineaSoporteReaseguro(lineaSoporteReaseguro);
		Object estatusLineaSoporteObject = lineaSoporteReaseguroFacade.obtenerCampoLineaSoporte(lineaSoporteReaseguro.getId().getIdTmLineaSoporteReaseguro(), "estatusLineaSoporte");
		Integer estatusLineaSoporte = estatusLineaSoporteObject!=null?Utilerias.obtenerInteger(estatusLineaSoporteObject):0;
		if(estatusLineaSoporte != null && estatusLineaSoporte.intValue() == LineaSoporteReaseguroDTO.EMITIDO_Y_DISTRIBUIDO)
			cantidadLineasDistribuidas += 1;
		return cantidadLineasDistribuidas;
	}

	@PersistenceContext
	EntityManager entityManager;
	
	private TimerService timerService;
	private DistribucionReaseguroServicios distribucionReaseguroServicios;

	@Resource 
	public void setTimerService(TimerService timerService) {
		this.timerService = timerService;
	}

	@EJB
	public void setDistribucionReaseguroServicios(
			DistribucionReaseguroServicios distribucionReaseguroServicios) {
		this.distribucionReaseguroServicios = distribucionReaseguroServicios;
	}
	
	private SistemaContext sistemaContext;
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}

}