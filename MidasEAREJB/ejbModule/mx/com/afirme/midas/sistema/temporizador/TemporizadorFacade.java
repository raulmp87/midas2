package mx.com.afirme.midas.sistema.temporizador;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.log.LogOperacionDTO;
import mx.com.afirme.midas.sistema.log.LogOperacionFacadeRemote;
import mx.com.afirme.midas.sistema.log.historico.LogOperacionHistDTO;
import mx.com.afirme.midas.sistema.log.historico.LogOperacionHistFacadeRemote;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class TemporizadorFacade implements TemporizadorFacadeRemote {

	@Resource TimerService timerService;
	@EJB LogOperacionFacadeRemote logFacade;
	@EJB LogOperacionHistFacadeRemote logHistFacade;
	private SistemaContext sistemaContext;
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	
	/**
	 * Detiene el temporizador
	 */
	public void detenerTemporizador() {
		Timer timer;
		for (Object obj : timerService.getTimers()) {
			timer = (Timer) obj;
			timer.cancel();
		}

	}
	
	/**
	 * Inicia el temporizador
	 * @param tiempoIniciar Tiempo para que se inicie el temporizador (en milisegundos)
	 * @param tiempoIntervalo Intervalo de tiempo entre cada ejecucion del evento TimeOut del temporizador (en milisegundos)
	 * @param maximoRegistros Numero maximo de registros que deben permanecer en el log. Si se sobrepasa este numero, se tienen que 
	 * respaldar en el historico
	 */
	public void iniciarTemporizador(long tiempoIniciar, long tiempoIntervalo, String maximoRegistros) {
		if(sistemaContext.getTimerActivo()) {
			timerService.createTimer(tiempoIniciar, tiempoIntervalo, maximoRegistros);
		}
	}
	
	
	@Timeout
	public void timeOut(Timer timer) {
		
		try {

			String maximoRegistros = (String) timer.getInfo();
			List<LogOperacionDTO> registrosARespaldar = logFacade.buscarRegistrosParaRespaldo(maximoRegistros);
			
			//Si existen registros en la bit�cora de Midas para ser respaldados
			if(registrosARespaldar != null) {
			
				LogOperacionHistDTO registroHistorico = null;
				
				for (LogOperacionDTO registro : registrosARespaldar) {
					registroHistorico = new LogOperacionHistDTO();
					registroHistorico.setIdtooperacionloghist(registro.getIdtooperacionlog());
					registroHistorico.setTipoaccion(registro.getTipoaccion());
					registroHistorico.setIdregistro(registro.getIdregistro());
					registroHistorico.setNombreusuario(registro.getNombreusuario());
					registroHistorico.setTipoentidad(registro.getTipoentidad());
					registroHistorico.setNombreentidad(registro.getNombreentidad());
					registroHistorico.setNombretablaentidad(registro.getNombretablaentidad());
					registroHistorico.setFecha(registro.getFecha());
					
					logHistFacade.save(registroHistorico);
					logFacade.delete(registro);
				}
				
				LogDeMidasEJB3.log("Se respaldaron " + registrosARespaldar.size() + " registros de la bit�cora de Midas", Level.INFO, null);
				
			} else {
				
				LogDeMidasEJB3.log("Se revis� la bit�cora de Midas y no hay registros a ser respaldados", Level.INFO, null);
			}
		
		} catch (Exception ex) {
			LogDeMidasEJB3.log("Error al respaldar registros de la bit�cora al Hist�rico", Level.WARNING, null);
			ex.printStackTrace();
		} finally {
			limpiaLogErrorCalculosRiesgo();
		}
		
		
	}
	
	private void limpiaLogErrorCalculosRiesgo() {
		Connection con = null;
		PreparedStatement  ps = null;
		LogDeMidasEJB3.log("Limpiando el log de Errores de Calculos Riesgo...", Level.INFO, null);
		
		try {
			Context ctx = new InitialContext();
	        if(ctx == null ) {
				throw new Exception("No Contexto");
	        }
	        
	        DataSource ds = (DataSource)ctx.lookup("jdbc/MidasDataSource");

	        if (ds != null) {
	        	con = ds.getConnection();
	        }
	        
	        GregorianCalendar gcFechaLimite = new GregorianCalendar();
			gcFechaLimite.add(GregorianCalendar.DATE, -7);
			gcFechaLimite.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFechaLimite.set(GregorianCalendar.MINUTE, 0);
			gcFechaLimite.set(GregorianCalendar.SECOND, 0);
			gcFechaLimite.set(GregorianCalendar.MILLISECOND, 0);
			 
	        String sqlQuery = "DELETE MIDAS.TLERRORCALCULORIESGO WHERE FECHAHORA < ?";
	        
	        ps = con.prepareStatement(sqlQuery);
	        ps.setTimestamp(1, new java.sql.Timestamp(gcFechaLimite.getTimeInMillis()));
	        ps.execute();    
	        
	        LogDeMidasEJB3.log("Se eliminaron los registros del log de Errores de Calculos Riesgo con fecha de registro anterior a " + 
	        		gcFechaLimite.getTime(), Level.INFO, null);
	        
		} catch (Exception e) {
			LogDeMidasEJB3.log("Hubo un error al limpiar el log de Errores de Calculos Riesgo", Level.WARNING, e);
	    } finally{
	    	DbUtils.closeQuietly(con);
	    	DbUtils.closeQuietly(ps);
	    }
	}
	
	

}
