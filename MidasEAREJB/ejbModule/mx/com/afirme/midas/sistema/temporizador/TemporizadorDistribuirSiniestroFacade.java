package mx.com.afirme.midas.sistema.temporizador;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;

import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroServiciosRemote;
import mx.com.afirme.midas.reaseguro.distribucion.DistribucionReaseguroServicios;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class TemporizadorDistribuirSiniestroFacade implements TemporizadorDistribuirSiniestroFacadeRemote {
	
	@EJB
	private MovimientoReaseguroServiciosRemote movimientoReaseguroServicios;	
	
	/**
	 * Inicia el temporizador
	 * @param tiempoIniciar Tiempo para que se inicie el temporizador (en milisegundos)
	 * @param soporteReaseguroDTO Este parametro es el que se le va a mandar el timer para
	 * como parametro info se espera que este objeto tenga establecido estos dos valores:
	 * idToPoliza y numeroEndoso.
	 */
	public void iniciarTemporizador(long tiempoIniciar, BigDecimal idToPoliza,
			Integer numeroEndoso, BigDecimal idToSeccion,
			BigDecimal idToCobertura, Integer numeroInciso,
			Integer numeroSubInciso, Integer conceptoMovimiento,
			BigDecimal idToMoneda, Date fechaMovimiento,
			BigDecimal idMovimientoSiniestro, BigDecimal montoMovimiento,
			Integer tipoMovimiento, BigDecimal idToReporteSiniestro,
			BigDecimal idToDistribucionMovSiniestro) {
		
		if(sistemaContext.getTimerActivo()) {
			HashMap<String, Object> infoMap = new HashMap<String, Object>();
			HashMap<String, Object> parametros = new HashMap<String, Object>();
			
			parametros.put("tiempoIniciar", tiempoIniciar);
			parametros.put("idToPoliza", idToPoliza);
			parametros.put("numeroEndoso", numeroEndoso);
			parametros.put("idToSeccion", idToSeccion);
			parametros.put("idToCobertura", idToCobertura);
			parametros.put("numeroInciso", numeroInciso);
			parametros.put("numeroSubInciso", numeroSubInciso);
			parametros.put("conceptoMovimiento", conceptoMovimiento);
			parametros.put("idToMoneda", idToMoneda);
			parametros.put("fechaMovimiento", fechaMovimiento);
			parametros.put("idMovimientoSiniestro", idMovimientoSiniestro);
			parametros.put("montoMovimiento", montoMovimiento);
			parametros.put("tipoMovimiento", tipoMovimiento);
			parametros.put("idToReporteSiniestro", idToReporteSiniestro);
			parametros.put("idToDistribucionMovSiniestro", idToDistribucionMovSiniestro);
			
			infoMap.put("info", parametros); //Esto se hace para poder guardar en este HashMap mas adelante el numero de intentos.
			infoMap.put("intentos", 0);
			
			timerService.createTimer(tiempoIniciar, infoMap);
			LogDeMidasEJB3.log("Temporizador para Distribuir SINIESTRO (createTimer) tiempoIniciar:("+tiempoIniciar+") idToReporteSiniestro:"+ idToReporteSiniestro +" idconceptoMovimietno: "+ conceptoMovimiento +", id generico idMovimientoSiniestro: "+ idMovimientoSiniestro +"  idtimerService.getTimers().size(): " + timerService.getTimers().size(),
					Level.INFO, null);	
		}
	}
	
	@SuppressWarnings("unchecked")
	@Timeout
	public void timeOut(Timer timer) {
		
	
		HashMap<String, Object> infoMap = null;
		int intentos = 0;
		
		try {
			infoMap = (HashMap<String, Object>) timer.getInfo();
			intentos = (Integer) infoMap.get("intentos"); //Obtener el numero de intentos que se han realizado y sumar el actual
			intentos++;
			
			HashMap<String, Object> parametros = (HashMap<String, Object>) infoMap.get("info");
			BigDecimal idToPoliza = (BigDecimal) parametros.get("idToPoliza");
			Integer numeroEndoso = (Integer) parametros.get("numeroEndoso"); 
			BigDecimal idToSeccion = (BigDecimal) parametros.get("idToSeccion");
			BigDecimal idToCobertura = (BigDecimal) parametros.get("idToCobertura"); 
			Integer numeroInciso = (Integer) parametros.get("numeroInciso");
			Integer numeroSubInciso = (Integer) parametros.get("numeroSubInciso");
			Integer conceptoMovimiento = (Integer) parametros.get("conceptoMovimiento");
			BigDecimal idToMoneda = (BigDecimal) parametros.get("idToMoneda");
			Date fechaMovimiento = (Date) parametros.get("fechaMovimiento");
			BigDecimal idMovimientoSiniestro = (BigDecimal) parametros.get("idMovimientoSiniestro"); 
			BigDecimal montoMovimiento = (BigDecimal) parametros.get("montoMovimiento");
			Integer tipoMovimiento = (Integer) parametros.get("tipoMovimiento");
			BigDecimal idToReporteSiniestro = (BigDecimal) parametros.get("idToReporteSiniestro");
			BigDecimal idToDistribucionMovSiniestro = (BigDecimal) parametros.get("idToDistribucionMovSiniestro");
			
//			LogDeMidasEJB3.log("Distribuci�n Movimiento Siniestro Id: " + distribucionMovSiniestroDTO.getIdMovimientoSiniestro() ,Level.INFO, null);

			LogDeMidasEJB3.log("INICIA Temporizador a Distribuir SINIESTRO (timeOut) idToReporteSiniestro:"+ idToReporteSiniestro +" idconceptoMovimietno: "+ conceptoMovimiento +", id generico idMovimientoSiniestro: "+ idMovimientoSiniestro +"  idtimerService.getTimers().size(): " + timerService.getTimers().size(),
					Level.INFO, null);		
			
			
			List<MovimientoReaseguroDTO> listaMovimientos = 
				distribucionReaseguroServicios.distribuirMontoSiniestro(idToPoliza, 
					numeroEndoso, idToSeccion, idToCobertura, 
					numeroInciso, numeroSubInciso, conceptoMovimiento, 
					idToMoneda, fechaMovimiento, idMovimientoSiniestro, 
					montoMovimiento, tipoMovimiento, idToReporteSiniestro, 
					idToDistribucionMovSiniestro);
			
			
			/*
			 * Contabilizar   
			 * IDTOPOLIZA | IDTOENDOSO | IDTOREPORTESINIESTRO | IDTCCONCEPTOMOVIMIENTO  | IDTOMOVIMIENTO | IDTOCOBERTURA | IDTOSECCION
			 */
			StringBuffer idBuffer = new StringBuffer();
			idBuffer.append(idToPoliza.toPlainString());
			idBuffer.append("|");
			idBuffer.append(numeroEndoso);
			idBuffer.append("|");
			idBuffer.append(idToReporteSiniestro.toPlainString());
			idBuffer.append("|");
			idBuffer.append(conceptoMovimiento);
			idBuffer.append("|");
			idBuffer.append(idMovimientoSiniestro.toPlainString());
			idBuffer.append("|");
			idBuffer.append(idToCobertura);
			idBuffer.append("|");
			idBuffer.append(idToSeccion);

			/*
			 * 31/05/2010. Jose Luis Arellano. Se agreg� validaci�n para el campo "conceptoMovimiento", ya que en caso de tener un valor de 9 (Gastos de ajuste)
			 * se debe enviar la clave "REASEGURO-GAS" en vez de "REASEGURO-SIN"
			 * 28/07/2010. Jose Luis. Se debe incluir "REASEGURO-GAS" para la cancelaci�n del gasto.
			 */
			String claveModulo ;
			if(conceptoMovimiento.intValue() == 9 || conceptoMovimiento.intValue() == 24)
				claveModulo = "REASEGURO-GAS";
			else
				claveModulo = "REASEGURO-SIN";
			LogDeMidasEJB3.log("Se prepara para invocar la contabilidad de Movimientos Reaseguro de Siniestros del IdToReporteSiniestro: " + 
					idToReporteSiniestro + " para la Poliza: " +
					idToPoliza + " y la idToCobertura: " + idToCobertura+" claveModulo: "+claveModulo, 
					Level.INFO, null);
			LogDeMidasEJB3.log("Bajo la referencia : " + idBuffer.toString() ,Level.INFO, null);
			
			try {
				movimientoReaseguroServicios.registrarContabilidad(
						idBuffer.toString(), claveModulo, listaMovimientos);
				LogDeMidasEJB3.log("La Contabilidad de Reaseguro en Siniestro fue Exitosa bajo la referencia : " + idBuffer.toString() ,Level.INFO, null);
			} catch (Exception e) {
				LogDeMidasEJB3.log("La Contabilidad de Reaseguro en Siniestro FALL� bajo la referencia : " + idBuffer.toString() ,Level.SEVERE, e);
			}
			
		}
		catch (Exception ex) {

			LogDeMidasEJB3.log("Temporizador para Distribuir SINIESTRO (timeOut) FALLA en Intento:("+intentos+" de "+TemporizadorReaseguro.NUMERO_INTENTOS_MAXIMO+")  idtimerService.getTimers().size(): " + timerService.getTimers().size(),
	                   Level.SEVERE, ex);			
			
			//Verificar si se debe seguir intentando distribuir
			if (intentos < TemporizadorReaseguro.NUMERO_INTENTOS_MAXIMO) {
				if(sistemaContext.getTimerActivo()) {
					infoMap.put("intentos", intentos); //Actualizar en el map el numero de intentos.
					timerService.createTimer(TemporizadorReaseguro.INTERVALO_TIEMPO, infoMap);
	
					LogDeMidasEJB3.log("Temporizador para Distribuir SINIESTRO (createTimer) tiempoIniciar:("+TemporizadorReaseguro.INTERVALO_TIEMPO+") INTENTO N� "+ intentos +"   idtimerService.getTimers().size(): " + timerService.getTimers().size(),
							Level.INFO, null);	
				}
			}
			else {
				//TODO:Mandar email
			}
		}
	}
	
	
	private TimerService timerService;
	private DistribucionReaseguroServicios distribucionReaseguroServicios;

	@Resource 
	public void setTimerService(TimerService timerService) {
		this.timerService = timerService;
	}
	
	@EJB
	public void setDistribucionReaseguroServicios(
			DistribucionReaseguroServicios distribucionReaseguroServicios) {
		this.distribucionReaseguroServicios = distribucionReaseguroServicios;
	}
	
	private SistemaContext sistemaContext;
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}

}
