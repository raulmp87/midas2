package mx.com.afirme.midas.sistema.temporizador;

import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;

import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableDTO;
import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableFacadeRemote;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeFacadeRemote;
import mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad.ContabilidadSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad.ContabilidadSiniestroFacadeRemote;
import mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad.ContabilidadSiniestroId;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoFacadeRemote;
import mx.com.afirme.midas.siniestro.servicios.SinietroServiciosRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class TemporizadorContabilizarSiniestrosFacade implements
		TemporizadorContabilizarSiniestrosFacadeRemote {

	@Resource TimerService timerService;
	@EJB ContabilidadSiniestroFacadeRemote contabilidadSiniestroFacade;
	@EJB OrdenDePagoFacadeRemote ordenPagoFacade;
	
	private AsientoContableFacadeRemote asientoContableFacade;
//	private SinietroServiciosRemote sinietroServiciosRemote;
//	private SolicitudChequeFacadeRemote solicitudChequeFacade;
	
	private SistemaContext sistemaContext;
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	
	public void detenerTemporizador() {
		Timer timer;
		for (Object obj : timerService.getTimers()) {
			timer = (Timer) obj;
			timer.cancel();
		}

	}

	public void iniciarTemporizador(long tiempoIniciar, long tiempoIntervalo) {
		if(sistemaContext.getTimerActivo()) {
			timerService.createTimer(tiempoIniciar, tiempoIntervalo, "PROCESO_NOCTURNO_CONTABILIZAR");
		}		
	}

	@Timeout
	public void timeOut(Timer timer) {
		
		try {
			
			System.out.println("Entro en el timer de contabilizar Siniestros...");
			
			//localizaEJBInterfaz();
			
			/**
			 * 
			 * obtener un listado con los movimientos que no fueron contabilizados
			 * distinct tocontabilidadsiniestro.idtoreportesiniestro donde tocontabilidadsiniestro.idpolizacontable sea null 
			 * 
			 * con cada idtoreportesiniestro mandar llamar la interfaz de contabilidad
			 *  
			 * Si regresa el cursor con los movimientos
			 * actualizar los registros con ese idtoreportesiniestro seteando idpolizacontable a 1 (ContabilidadSiniestroDTO.ESTATUS_CONTABILIZADO)
			 * 
			 * 
			 */
			
			//Obtener un listado con los movimientos que no fueron contabilizados
			List<ContabilidadSiniestroId> contabilidadSiniestroIdList = contabilidadSiniestroFacade.buscarMovimientosNoContabilizados();
			String idMovimiento = null;
			if (contabilidadSiniestroIdList != null) {
				for (ContabilidadSiniestroId contabilidadSiniestroId : contabilidadSiniestroIdList) {
					
					try {
						//Se manda a hacer la contabilidad en Seycos    idReporte|idMovimiento|idRegistro
						
						idMovimiento = 	contabilidadSiniestroId.getIdToReporteSiniestro() + "|" + 
										contabilidadSiniestroId.getIdTcMovimientoSiniestro() + "|" + 
										contabilidadSiniestroId.getIdRegistro() + "|" + 
										contabilidadSiniestroId.getCveConceptoMov();
						if(asientoContableFacade == null){
							asientoContableFacade = ServiceLocatorP.getInstance().getEJB(AsientoContableFacadeRemote.class);
						}
						List<AsientoContableDTO> asientoContableList = asientoContableFacade.contabilizaMovimientos(
								idMovimiento, "SINIESTRO", "TIMER");
						
						//Si se contabilizaron los movimientos en Seycos
						if (asientoContableList != null && asientoContableList.size() > 0) {
							//Se actualizan los movimientos de siniestros como contabilizados
							contabilidadSiniestroFacade.actualizarMovimientosContabilizados(
									contabilidadSiniestroId, ContabilidadSiniestroDTO.ESTATUS_CONTABILIZADO);
						}
					} catch (Exception ex) { //SQLException Se trata de manejar si regresa cerrado el cursor (no regreso movimientos)
						if (ex.getMessage().lastIndexOf("closed") == -1 && ex.getMessage().lastIndexOf("cerrado") == -1) {
							//TODO: Quitar este comentario cuando se corrija el SP y 
							//no regrese un cursor con nombre de columna NULL (en caso de no haber movimientos)
							//throw ex;
						}
					}
					
				}
			}
			
		
		} catch (Exception ex) {
			LogDeMidasEJB3.log("Error en la ejecucion del proceso de contabilizar movimientos de Siniestros.", Level.WARNING, null);
			ex.printStackTrace();
		} finally {
			//Se ejecuta el proceso para revisar y actualizar los estatus de las solicitudes de cheque
//			actualizaEstatusSolicitudCheque();
		}
		
		
	}
	
	
	private void actualizaEstatusSolicitudCheque() {
		
		/**
		 * consultar las ordenes de pago con estatus 0 (pendiente)
		 * 
		 * de cada una de esas ordenes:
		 *  -obtener su idToOrdenPago y su idSolicitudCheque
		 *    
		 *  -llamar a la interfaz de consultarEstatus de solicitud de cheque (parametro: idToOrdenPago)
		 * 
		 *  -Con el string resultante (el estatus actual) pasarlo como parametro al metodo ordenPagoFacade.actualizaOrdenCheque
		 * 
		 * 
		 */
		
		try { 
			String estatusActual = null;
			List<OrdenDePagoDTO> ordenPagoList = ordenPagoFacade.findByProperty("estatus", OrdenDePagoDTO.ESTATUS_PENDIENTE);
			
			if (ordenPagoList != null) {
				for (OrdenDePagoDTO ordenPago : ordenPagoList) {
					
					LogDeMidasEJB3.log("Consultando estatus actual de la solicitud de cheque con id de Orden de pago: " + 
							ordenPago.getIdToOrdenPago() + " e id de Solicitud de cheque: " + ordenPago.getIdSolicitudCheque(),
							Level.INFO, null);
					
					//TODO se comenta la llamada al metodo debido a que ya no se va a usar este metodo para actualizar las ordenes de pago
//					estatusActual = solicitudChequeFacade.consultaEstatusSolicitudCheque(ordenPago.getIdToOrdenPago(), "TIMER");
					
					if (estatusActual != null) {
						//TODO se comenta la llamada al metodo debido a que se movio la logica de este proceso a un DN
//						ordenPagoFacade.actualizaOrdenCheque(ordenPago.getIdToOrdenPago(), estatusActual);
						
						LogDeMidasEJB3.log("Ejecucion exitosa en la Actualizacion de estatus la solicitud de cheque con id de Orden de pago: " + 
								ordenPago.getIdToOrdenPago() + " e id de Solicitud de cheque: " + ordenPago.getIdSolicitudCheque(),
								Level.INFO, null);
						
					}
					
				}
			}
			
		} catch (Exception ex) {
			LogDeMidasEJB3.log("Error en la ejecucion del proceso de actualizar estatus a las ordenes de " +
					"cheque de Siniestros.", Level.WARNING, null);
			ex.printStackTrace();
		}
		
	}
	
	
	private void localizaEJBInterfaz() throws Exception {
		try {
			ServiceLocatorP serviceLocatorP = ServiceLocatorP.getInstance();
			asientoContableFacade = serviceLocatorP.getEJB(AsientoContableFacadeRemote.class);
//			solicitudChequeFacade = serviceLocatorP.getEJB(SolicitudChequeFacadeRemote.class);
						
		} catch (Exception e) {
			
			throw new Exception("EJBs de Interfaz no disponibles...");
		
		}
		LogDeMidasEJB3.log("beans Remotos instanciados", Level.FINEST, null);
	}
	
	
}
