package mx.com.afirme.midas.sistema.temporizador;

import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.reaseguro.distribucion.DistribucionReaseguroServicios;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class TemporizadorDistribuirPrimaFacade implements TemporizadorDistribuirPrimaFacadeRemote {

	@Resource 
	private TimerService timerService;
	
	@EJB
	private DistribucionReaseguroServicios distribucionReaseguroServicios;
	
	private SistemaContext sistemaContext;
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	
	/**
	 * Detiene el temporizador
	 */
	public void detenerTemporizador() {
		Timer timer;
		for (Object obj : timerService.getTimers()) {
			timer = (Timer) obj;
			timer.cancel();
		}

	}
	
	/**
	 * Inicia el temporizador
	 * @param tiempoIniciar Tiempo para que se inicie el temporizador (en milisegundos)
	 */
	public void iniciarTemporizador(long tiempoIniciar) {
		//Este timer inicio como un timer que iba a ser repetitivo termino
		//siendo un timer de una sola vez. Por lo tanto tiempoIntervalo ya no se utiliza
		//Lo correcto seria cambiar la firma.
		if(sistemaContext.getTimerActivo()) {
			timerService.createTimer(tiempoIniciar, null);
			LogDeMidasEJB3.log("Temporizador para Distribuir MASIVAMENTE POLIZAS (createTimer) tiempoIniciar:("+tiempoIniciar+", timerService.getTimers().size(): " + timerService.getTimers().size(),
					Level.INFO, null);
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Timeout
	public void timeOut(Timer timer) {
		try {

			LogDeMidasEJB3.log("Temporizador para Distribuir MASIVAMENTE POLIZAS (timeOut) INICIA, timerService.getTimers().size(): " + timerService.getTimers().size(),
					Level.INFO, null);			
			distribucionReaseguroServicios.distribuirPrimas();
			LogDeMidasEJB3.log("Temporizador para Distribuir MASIVAMENTE POLIZAS (timeOut) TERMINA, timerService.getTimers().size(): " + timerService.getTimers().size(),
					Level.INFO, null);			

		}
		catch (Exception ex) {
			//LogDeMidasEJB3.log("Fallo la distribucion masiva.", Level.SEVERE, ex);
			LogDeMidasEJB3.log("Temporizador para Distribuir MASIVAMENTE POLIZAS (timeOut) FALL�, YA NO HABRA INTENTOS timerService.getTimers().size(): " + timerService.getTimers().size(),
					Level.SEVERE, ex);			
			
		}
	}

}
