package mx.com.afirme.midas.sistema.temporizador;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;

import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoFacadeRemote;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaFacadeRemote;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.contratos.linea.LineaFacadeRemote;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroServiciosRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
 
@Stateless
public class TemporizadorEstadoCuentaVigenteFacade implements TemporizadorEstadoCuentaVigenteRemote {

	@Resource TimerService timerService;
	@EJB EstadoCuentaFacadeRemote estadoCuentaFacade;
	@EJB LineaFacadeRemote lineaFacade;
	@EJB ContratoFacultativoFacadeRemote contratoFacultativoFacadeRemote;
	
	@EJB
	private MovimientoReaseguroServiciosRemote movimientoReaseguroServicios;
	
	private SistemaContext sistemaContext;
 	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	 
	/**
	 * Detiene el temporizador
	 */
	public void detenerTemporizador() {
		Timer timer;
		for (Object obj : timerService.getTimers()) {
			timer = (Timer) obj;
			timer.cancel();
		}

	}
 	 
	/**
	 * Inicia el temporizador
	 * @param tiempoIniciar Tiempo para que se inicie el temporizador (en milisegundos)
	 * @param tiempoIntervalo Intervalo de tiempo entre cada ejecucion del evento TimeOut del temporizador (en milisegundos)
	 */   
	public void iniciarTemporizador(long tiempoIniciar, long tiempoIntervalo) {
		if(sistemaContext.getTimerActivo()) {
			timerService.createTimer(tiempoIniciar, tiempoIntervalo);
		}
	}
	
	
	@Timeout
	public void timeOut(Timer timer) {
		LogDeMidasEJB3.log("comienza la creacion de estados de cuenta por lineas vigentes hora: "+ new Date(),Level.INFO, null);
	 	try { 
			Date fecha = new Date();
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				fecha =  sdf.parse("24/12/2009");
		 	} catch (ParseException e) {
				e.printStackTrace();
			}
 			List<LineaDTO> lineaList = estadoCuentaFacade.obtenerLineasAutorizadas(fecha);
 			List<ContratoFacultativoDTO> contratoFacultativoList = estadoCuentaFacade.obtenerContratoAutorizadasFacultativo(fecha);
 			//Si existen lineas vigentes crea estados de cuenta
			if(lineaList != null) {
				LogDeMidasEJB3.log("Comienza la creacion de estados de cuenta " + new Date(), Level.INFO, null);
		 	 	for (LineaDTO lineaDTO : lineaList) {
					estadoCuentaFacade.crearEstadosCuentaLinea(lineaDTO);
					lineaDTO.setEstadosCuentaCreados(1);
					lineaFacade.update(lineaDTO);
				}
	 			LogDeMidasEJB3.log("Se crearon estados de cuenta " + new Date(), Level.INFO, null);
	  		} else {
	 			LogDeMidasEJB3.log("No hay lineas vigentes para crear estados de cuenta", Level.INFO, null);
			}
			if(contratoFacultativoList != null){
				LogDeMidasEJB3.log("Comienza la creacion de estados de cuenta " + new Date(), Level.INFO, null);
				for(ContratoFacultativoDTO contratoFacultativoDTO : contratoFacultativoList){
					estadoCuentaFacade.crearEstadosCuentaFacultativo(contratoFacultativoDTO);
					contratoFacultativoDTO.setEstadosCuentaCreados(1);
					contratoFacultativoFacadeRemote.update(contratoFacultativoDTO);
			 	}
			 	LogDeMidasEJB3.log("Se crearon estados de cuenta facultativo " + new Date(), Level.INFO, null);
 			}else{
				LogDeMidasEJB3.log("No hay contratos vigentes facultativos para crear estados de cuenta", Level.INFO, null);
			}
			
			
			/*
			 * Se agrega tambi�n un proceso para recolectar movimientos de remesas que no han sido contabilizadas
			 * y les da una nueva oportunidad de ser contabilizadas.
			 * TODO:REMESA habilitar llamada a contabilizarMovimientosIngresosPendientes()
			 */
			//contabilizarMovimientosIngresosPendientes();
			
	 	} catch (Exception ex) {
			LogDeMidasEJB3.log("Error al generar los estados de cuenta", Level.WARNING, null);
			ex.printStackTrace();
		}
	}
	
	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private void contabilizarMovimientosIngresosPendientes(){
		try{
			List<String> listaPendientes = 
				movimientoReaseguroServicios.obtenerMovimientosIngresosPendientesPorContabilizar();
			
			movimientoReaseguroServicios.registrarContabilidad(listaPendientes, "MIDAS");			
		}catch(Exception ex){
			LogDeMidasEJB3.log("Error al generar los estados de cuenta", Level.WARNING, null);
			ex.printStackTrace();			
		}	
	}

}
