package mx.com.afirme.midas.poliza.cobertura.subinciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity SubIncisoCoberturaPolizaDTO.
 * 
 * @see .SubIncisoCoberturaPolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SubIncisoCoberturaPolizaFacade implements
		SubIncisoCoberturaPolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * SubIncisoCoberturaPolizaDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            SubIncisoCoberturaPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubIncisoCoberturaPolizaDTO entity) {
		LogUtil.log("saving SubIncisoCoberturaPolizaDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SubIncisoCoberturaPolizaDTO entity.
	 * 
	 * @param entity
	 *            SubIncisoCoberturaPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubIncisoCoberturaPolizaDTO entity) {
		LogUtil.log("deleting SubIncisoCoberturaPolizaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					SubIncisoCoberturaPolizaDTO.class, entity.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SubIncisoCoberturaPolizaDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * SubIncisoCoberturaPolizaDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            SubIncisoCoberturaPolizaDTO entity to update
	 * @return SubIncisoCoberturaPolizaDTO the persisted
	 *         SubIncisoCoberturaPolizaDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubIncisoCoberturaPolizaDTO update(SubIncisoCoberturaPolizaDTO entity) {
		LogUtil.log("updating SubIncisoCoberturaPolizaDTO instance",
				Level.INFO, null);
		try {
			SubIncisoCoberturaPolizaDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SubIncisoCoberturaPolizaDTO findById(SubIncisoCoberturaPolizaId id) {
		LogUtil.log("finding SubIncisoCoberturaPolizaDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			SubIncisoCoberturaPolizaDTO instance = entityManager.find(
					SubIncisoCoberturaPolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubIncisoCoberturaPolizaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SubIncisoCoberturaPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubIncisoCoberturaPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoCoberturaPolizaDTO> findByProperty(
			String propertyName, final Object value) {
		LogUtil.log(
				"finding SubIncisoCoberturaPolizaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SubIncisoCoberturaPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubIncisoCoberturaPolizaDTO entities.
	 * 
	 * @return List<SubIncisoCoberturaPolizaDTO> all SubIncisoCoberturaPolizaDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoCoberturaPolizaDTO> findAll() {
		LogUtil.log("finding all SubIncisoCoberturaPolizaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from SubIncisoCoberturaPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void insertSubIncisoCoberturaPolizaPorCotizacion(BigDecimal idToCotizacion,BigDecimal idToPoliza){
		String queryString ="INSERT INTO MIDAS.toSubincisoCobPol (idToPoliza, numeroInciso, idToSeccion, numeroSubinciso, idToCobertura, idTcSubramo, valorSumaAsegurada, valorPrimaNeta) "+
				" SELECT "+idToPoliza+", detpricobcot.numeroInciso, detpricobcot.idToSeccion, detpricobcot.numeroSubinciso, detpricobcot.idToCobertura, "+
				" coberturacot.idTcSubramo,DECODE(seccion.claveBienSeccion,1,DECODE(subincisocot.valorsumaasegurada, null, coberturacot.valorsumaasegurada, subincisocot.valorsumaasegurada), coberturacot.valorsumaasegurada) valorsumaasegurada, "+
				"  detpricobcot.valorPrimaNeta "+
				" FROM MIDAS.todetprimacoberturacot detpricobcot, MIDAS.tocoberturacot coberturacot, MIDAS.toseccioncot seccioncot, MIDAS.tosubincisocot subincisocot, MIDAS.toseccion seccion "+
			    " WHERE detpricobcot.idtocotizacion = "+idToCotizacion+
			    " AND seccion.idtoseccion = detpricobcot.idtoseccion "+
			    " AND coberturacot.idtocobertura = detpricobcot.idtocobertura "+
			    " AND coberturacot.clavecontrato = 1 "+
			    " AND coberturacot.idtocotizacion = detpricobcot.idtocotizacion "+
			    " AND coberturacot.numeroinciso = detpricobcot.numeroinciso "+
			    " AND coberturacot.idtoseccion = detpricobcot.idtoseccion "+
			    " AND seccioncot.idtocotizacion = coberturacot.idtocotizacion "+
			    " AND seccioncot.numeroinciso = coberturacot.numeroinciso "+ 
			    " AND seccioncot.idtoseccion = coberturacot.idtoseccion "+
			    " AND seccioncot.clavecontrato = 1 "+
			    " AND subincisocot.idtocotizacion (+) = detpricobcot.idtocotizacion "+ 
			    " AND subincisocot.numeroinciso (+) = detpricobcot.numeroinciso "+
			    " AND subincisocot.idtoseccion (+) = detpricobcot.idtoseccion " +
			    " AND subincisocot.numerosubinciso (+) = detpricobcot.numerosubinciso "; 
						
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();		
	}
}