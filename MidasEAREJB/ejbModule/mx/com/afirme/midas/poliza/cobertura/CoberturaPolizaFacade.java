package mx.com.afirme.midas.poliza.cobertura;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity CoberturaPolizaDTO.
 * 
 * @see .CoberturaPolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CoberturaPolizaFacade implements CoberturaPolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved CoberturaPolizaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CoberturaPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public CoberturaPolizaDTO save(CoberturaPolizaDTO entity) {
		LogUtil.log("saving CoberturaPolizaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent CoberturaPolizaDTO entity.
	 * 
	 * @param entity
	 *            CoberturaPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoberturaPolizaDTO entity) {
		LogUtil.log("deleting CoberturaPolizaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(CoberturaPolizaDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved CoberturaPolizaDTO entity and return it or a
	 * copy of it to the sender. A copy of the CoberturaPolizaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CoberturaPolizaDTO entity to update
	 * @return CoberturaPolizaDTO the persisted CoberturaPolizaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoberturaPolizaDTO update(CoberturaPolizaDTO entity) {
		LogUtil.log("updating CoberturaPolizaDTO instance", Level.INFO, null);
		try {
			CoberturaPolizaDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CoberturaPolizaDTO findById(CoberturaPolizaId id) {
		LogUtil.log("finding CoberturaPolizaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			CoberturaPolizaDTO instance = entityManager.find(
					CoberturaPolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoberturaPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoberturaPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoberturaPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaPolizaDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding CoberturaPolizaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from CoberturaPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoberturaPolizaDTO entities.
	 * 
	 * @return List<CoberturaPolizaDTO> all CoberturaPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaPolizaDTO> findAll() {
		LogUtil.log("finding all CoberturaPolizaDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from CoberturaPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void insertCoberturaPolizaPorCotizacion(BigDecimal idToCotizacion,BigDecimal idToPoliza){
		String queryString ="INSERT INTO MIDAS.toCoberturaPol (idToPoliza, numeroInciso, idToSeccion, idToCobertura, idTcSubRamo, valorSumaAsegurada, valorCoaseguro, valorDeducible, "+
						" valorCuota, valorPrimaNeta, valorRecargoPagoFrac, valorDerechos, valorBonifComision, valorBonifComRecPagoFrac, "+
						" valorIVA, valorPrimaTotal, porcentajeComision, valorComision, valorComisionFinal, valorComisionRecPagoFrac, "+
						" valorComFinalRecPagoFrac, numeroAgrupacion, claveEstatus) "+
						" SELECT "+idToPoliza+", coberturacot.numeroInciso, coberturacot.idToSeccion, coberturacot.idToCobertura, coberturacot.idTcSubRamo, coberturacot.valorSumaAsegurada, "+
						" coberturacot.valorCoaseguro, coberturacot.valorDeducible, coberturacot.valorCuota, coberturacot.valorPrimaNeta, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, coberturacot.numeroAgrupacion, 1 "+
						" FROM MIDAS.toCoberturaCot coberturacot, MIDAS.toSeccionCot seccioncot "+
						" WHERE coberturacot.idToCotizacion = "+idToCotizacion+
						" AND   coberturacot.claveContrato = 1"+
					    " AND seccioncot.idtocotizacion = coberturacot.idtocotizacion "+
					    " AND seccioncot.numeroinciso = coberturacot.numeroinciso "+
					    " AND seccioncot.idtoseccion = coberturacot.idtoseccion "+						
						" AND   seccioncot.idToSeccion = coberturacot.idToSeccion"+
						" AND   seccioncot.claveContrato = 1";
						
							
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();		
	}

	@SuppressWarnings("unchecked")
	public Double obtenerPrimaNetaPoliza(BigDecimal idToPoliza) {
		LogDeMidasEJB3.log(
				"obtenerPrimaNetaPoliza from CoberturaPolizaDTO instance",
				Level.INFO, null);
		String queryString = "";
		try {
			Double primaNeta = 0D;
			if (idToPoliza == null)
				return primaNeta;

			queryString = "select SUM(VALORPRIMANETA) as VALORPRIMANETA from MIDAS.TOCOBERTURAPOL where idtopoliza ="+idToPoliza;
			Query query = entityManager.createNativeQuery(queryString);
			Object result = query.getSingleResult();
			if (result instanceof List) {
				primaNeta = (Double) ((BigDecimal) ((List) result).get(0))
						.doubleValue();
			} else if (result instanceof BigDecimal) {
				primaNeta = (Double) ((BigDecimal) result).doubleValue();
			}
			return primaNeta;
		} catch (RuntimeException re) {
			re.printStackTrace();
			LogDeMidasEJB3.log("obtenerPrimaNetaPoliza failed", Level.SEVERE,
					re);
			throw re;
		}
	}	
}