package mx.com.afirme.midas.poliza.subinciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity SubIncisoPolizaDTO.
 * 
 * @see .SubIncisoPolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SubIncisoPolizaFacade implements SubIncisoPolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SubIncisoPolizaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SubIncisoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubIncisoPolizaDTO entity) {
		LogUtil.log("saving SubIncisoPolizaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SubIncisoPolizaDTO entity.
	 * 
	 * @param entity
	 *            SubIncisoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubIncisoPolizaDTO entity) {
		LogUtil.log("deleting SubIncisoPolizaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SubIncisoPolizaDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SubIncisoPolizaDTO entity and return it or a
	 * copy of it to the sender. A copy of the SubIncisoPolizaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SubIncisoPolizaDTO entity to update
	 * @return SubIncisoPolizaDTO the persisted SubIncisoPolizaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubIncisoPolizaDTO update(SubIncisoPolizaDTO entity) {
		LogUtil.log("updating SubIncisoPolizaDTO instance", Level.INFO, null);
		try {
			SubIncisoPolizaDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SubIncisoPolizaDTO findById(SubIncisoPolizaId id) {
		LogUtil.log("finding SubIncisoPolizaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SubIncisoPolizaDTO instance = entityManager.find(
					SubIncisoPolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubIncisoPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SubIncisoPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubIncisoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoPolizaDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding SubIncisoPolizaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SubIncisoPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubIncisoPolizaDTO entities.
	 * 
	 * @return List<SubIncisoPolizaDTO> all SubIncisoPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoPolizaDTO> findAll() {
		LogUtil.log("finding all SubIncisoPolizaDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from SubIncisoPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void insertSubIncisoPolizaPorCotizacion(BigDecimal idToCotizacion,BigDecimal idToPoliza){
		String queryString ="INSERT INTO MIDAS.toSubincisoPol (idToPoliza, numeroInciso, idToSeccion, numeroSubinciso,valorSumaAsegurada, valorPrimaNeta, claveestatus) "+
			" SELECT "+idToPoliza+", subincisocot.numeroInciso, subincisocot.idToSeccion, subincisocot.numeroSubinciso, subincisocot.valorSumaAsegurada,  0, 1 "+
			" FROM MIDAS.tosubincisocot subincisocot, "+
			" MIDAS.toseccioncot seccioncot "+
			" WHERE subincisocot.idtocotizacion ="+idToCotizacion+
			" AND subincisocot.idtocotizacion = seccioncot.idtocotizacion "+
			" AND subincisocot.numeroinciso = seccioncot.numeroinciso "+
			" AND subincisocot.idtoseccion = seccioncot.idtoseccion "+
			" and seccioncot.clavecontrato = 1 ";
	      
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();				
	}
}