package mx.com.afirme.midas.poliza.riesgo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity RiesgoPolizaDTO.
 * 
 * @see .RiesgoPolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class RiesgoPolizaFacade implements RiesgoPolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved RiesgoPolizaDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            RiesgoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public RiesgoPolizaDTO save(RiesgoPolizaDTO entity) {
		LogUtil.log("saving RiesgoPolizaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent RiesgoPolizaDTO entity.
	 * 
	 * @param entity
	 *            RiesgoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoPolizaDTO entity) {
		LogUtil.log("deleting RiesgoPolizaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(RiesgoPolizaDTO.class, entity
					.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved RiesgoPolizaDTO entity and return it or a copy
	 * of it to the sender. A copy of the RiesgoPolizaDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            RiesgoPolizaDTO entity to update
	 * @return RiesgoPolizaDTO the persisted RiesgoPolizaDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoPolizaDTO update(RiesgoPolizaDTO entity) {
		LogUtil.log("updating RiesgoPolizaDTO instance", Level.INFO, null);
		try {
			RiesgoPolizaDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RiesgoPolizaDTO findById(RiesgoPolizaId id) {
		LogUtil.log("finding RiesgoPolizaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			RiesgoPolizaDTO instance = entityManager.find(
					RiesgoPolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoPolizaDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding RiesgoPolizaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoPolizaDTO entities.
	 * 
	 * @return List<RiesgoPolizaDTO> all RiesgoPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoPolizaDTO> findAll() {
		LogUtil.log("finding all RiesgoPolizaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void insertRiesgoPolizaPorCotizacion(BigDecimal idToCotizacion, BigDecimal idToPoliza){
		String queryString ="INSERT INTO MIDAS.toRiesgoPol (idToPoliza, numeroInciso, idToSeccion, idToCobertura, idToRiesgo, idTcSubRamo, valorSumaAsegurada, valorCoaseguro, "+
				" valorDeducible, valorCuota, valorPrimaNeta, valorRecargoPagoFrac, valorDerechos, valorBonifComision, valorBonifComRecPagoFrac, "+
				" valorIVA, valorPrimaTotal, porcentajeComision, valorComision, valorComisionFinal, valorComisionRecPagoFrac, valorComFinalRecPagoFrac, "+
				" claveEstatus) "+
				" SELECT "+idToPoliza+", riesgocot.numeroInciso, riesgocot.idToSeccion, riesgocot.idToCobertura, riesgocot.idToRiesgo, riesgocot.idTcSubRamo, "+
				" case when riesgocot.valorsumaasegurada <= 0 then coberturacot.valorsumaasegurada else riesgocot.valorsumaasegurada end,"+ 
				" riesgocot.valorCoaseguro, riesgocot.valorDeducible, "+
				" case when riesgocot.valorsumaasegurada <= 0 then riesgocot.valorprimaneta /coberturacot.valorsumaasegurada else riesgocot.valorprimaneta /riesgocot.valorsumaasegurada end,"+ 
				" riesgocot.valorPrimaNeta, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 "+
				" FROM MIDAS.toriesgocot riesgocot, MIDAS.toseccioncot seccioncot, MIDAS.tocoberturacot coberturacot"+
				" WHERE riesgocot.idToCotizacion ="+idToCotizacion+
				" AND   riesgocot.claveContrato = 1 "+
				" AND   coberturacot.idToCobertura = riesgocot.idToCobertura"+
				" AND   coberturacot.claveContrato = 1 "+
			    " AND coberturacot.idtocotizacion = riesgocot.idtocotizacion "+
			    " AND coberturacot.numeroinciso = riesgocot.numeroinciso "+
			    " AND coberturacot.idtoseccion = riesgocot.idtoseccion "+
			    " AND seccioncot.idtocotizacion = riesgocot.idtocotizacion "+
			    " AND seccioncot.numeroinciso = riesgocot.numeroinciso "+		    
				" AND   seccioncot.idToSeccion = riesgocot.idToSeccion "+
				" AND   seccioncot.claveContrato = 1 ";
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();			
	}
	
	@SuppressWarnings("unchecked")
	public List<RiesgoPolizaDTO> listarFiltrado(RiesgoPolizaId id){
		try {		
			String queryString = "select model from RiesgoPolizaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (id == null)
				return null;		
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToPoliza", id.getIdToPoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.numeroInciso", id.getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToSeccion", id.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToCobertura", id.getIdToCobertura());	
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToRiesgo", id.getIdToRiesgo());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		}catch (RuntimeException re){
			LogDeMidasEJB3.log("listarFiltrado failed", Level.SEVERE, re);
			throw re;			
		}			
	}
}
