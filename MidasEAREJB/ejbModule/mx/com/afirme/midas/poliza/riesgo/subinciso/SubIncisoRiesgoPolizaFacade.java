package mx.com.afirme.midas.poliza.riesgo.subinciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity SubIncisoRiesgoPolizaDTO.
 * 
 * @see .SubIncisoRiesgoPolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SubIncisoRiesgoPolizaFacade implements
		SubIncisoRiesgoPolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SubIncisoRiesgoPolizaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SubIncisoRiesgoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubIncisoRiesgoPolizaDTO entity) {
		LogUtil.log("saving SubIncisoRiesgoPolizaDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SubIncisoRiesgoPolizaDTO entity.
	 * 
	 * @param entity
	 *            SubIncisoRiesgoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubIncisoRiesgoPolizaDTO entity) {
		LogUtil.log("deleting SubIncisoRiesgoPolizaDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(SubIncisoRiesgoPolizaDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SubIncisoRiesgoPolizaDTO entity and return it
	 * or a copy of it to the sender. A copy of the SubIncisoRiesgoPolizaDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SubIncisoRiesgoPolizaDTO entity to update
	 * @return SubIncisoRiesgoPolizaDTO the persisted SubIncisoRiesgoPolizaDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubIncisoRiesgoPolizaDTO update(SubIncisoRiesgoPolizaDTO entity) {
		LogUtil.log("updating SubIncisoRiesgoPolizaDTO instance", Level.INFO,
				null);
		try {
			SubIncisoRiesgoPolizaDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SubIncisoRiesgoPolizaDTO findById(SubIncisoRiesgoPolizaId id) {
		LogUtil.log("finding SubIncisoRiesgoPolizaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SubIncisoRiesgoPolizaDTO instance = entityManager.find(
					SubIncisoRiesgoPolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubIncisoRiesgoPolizaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SubIncisoRiesgoPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubIncisoRiesgoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoRiesgoPolizaDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding SubIncisoRiesgoPolizaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SubIncisoRiesgoPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubIncisoRiesgoPolizaDTO entities.
	 * 
	 * @return List<SubIncisoRiesgoPolizaDTO> all SubIncisoRiesgoPolizaDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoRiesgoPolizaDTO> findAll() {
		LogUtil.log("finding all SubIncisoRiesgoPolizaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from SubIncisoRiesgoPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void insertSubIncisoRiesgoPolizaPorCotizacion(BigDecimal idToCotizacion, BigDecimal idToPoliza){
		String queryString ="INSERT INTO MIDAS.toSubincisoRgoPol (idToPoliza, numeroInciso, idToSeccion, numeroSubinciso, idToCobertura, idToRiesgo, "+
				" idTcSubramo, valorSumaAsegurada, valorPrimaNeta)"+
				" SELECT "+idToPoliza+", detprirgocot.numeroInciso, detprirgocot.idToSeccion, detprirgocot.numeroSubinciso, detprirgocot.idToCobertura, "+
				" detprirgocot.idToRiesgo, coberturacot.idTcSubramo, detprirgocot.valorSumaAsegurada, detprirgocot.valorPrimaNeta "+
				" FROM MIDAS.toDetPrimaRiesgoCot detprirgocot, MIDAS.toSeccionCot seccioncot, MIDAS.toCoberturaCot coberturacot, MIDAS.toSubincisoCot subincisocot, MIDAS.toriesgocot riesgocot"+
			    " WHERE riesgocot.idtocotizacion = "+idToCotizacion+
			      " AND riesgocot.idtocotizacion = detprirgocot.idtocotizacion "+
			      " AND riesgocot.numeroinciso = detprirgocot.numeroinciso "+ 
			      " AND riesgocot.idtoseccion = detprirgocot.idtoseccion "+
			      " AND riesgocot.idtocobertura = detprirgocot.idtocobertura "+
			      " AND riesgocot.idtoriesgo = detprirgocot.idtoriesgo "+
			      " AND riesgocot.clavecontrato = 1 "+
			      " AND coberturacot.idtocotizacion = riesgocot.idtocotizacion "+
			      " AND coberturacot.numeroinciso = riesgocot.numeroinciso "+
			      " AND coberturacot.idtoseccion = riesgocot.idtoseccion "+
			      " AND coberturacot.idtocobertura = riesgocot.idtocobertura "+
			      " AND coberturacot.clavecontrato = 1 "+
			      " AND seccioncot.idtocotizacion = coberturacot.idtocotizacion "+
			      " AND seccioncot.numeroinciso = coberturacot.numeroinciso "+
			      " AND seccioncot.idtoseccion = coberturacot.idtoseccion "+
			      " AND seccioncot.clavecontrato = 1 "+
			      " AND subincisocot.idtocotizacion(+) = detprirgocot.idtocotizacion "+
			      " AND subincisocot.numeroinciso(+) = detprirgocot.numeroinciso "+
			      " AND subincisocot.idtoseccion(+) = detprirgocot.idtoseccion "+
			      " AND subincisocot.numerosubinciso(+) = detprirgocot.numerosubinciso";
				
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();		
	}
}