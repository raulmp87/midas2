package mx.com.afirme.midas.poliza.folio;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.poliza.folio.FolioPolizaDTO;
import mx.com.afirme.midas.poliza.folio.FolioPolizaFacadeRemote;
import mx.com.afirme.midas.poliza.folio.FolioPolizaId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity FolioPolizaDTO.
 * 
 * @see .FolioPolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class FolioPolizaFacade implements FolioPolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved FolioPolizaDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            FolioPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(FolioPolizaDTO entity) {
		LogUtil.log("saving FolioPolizaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent FolioPolizaDTO entity.
	 * 
	 * @param entity
	 *            FolioPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(FolioPolizaDTO entity) {
		LogUtil.log("deleting FolioPolizaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(FolioPolizaDTO.class, entity
					.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved FolioPolizaDTO entity and return it or a copy
	 * of it to the sender. A copy of the FolioPolizaDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            FolioPolizaDTO entity to update
	 * @return FolioPolizaDTO the persisted FolioPolizaDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public FolioPolizaDTO update(FolioPolizaDTO entity) {
		LogUtil.log("updating FolioPolizaDTO instance", Level.INFO, null);
		try {
			FolioPolizaDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public FolioPolizaDTO findById(FolioPolizaId id) {
		LogUtil.log("finding FolioPolizaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			FolioPolizaDTO instance = entityManager.find(FolioPolizaDTO.class,
					id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all FolioPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the FolioPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<FolioPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<FolioPolizaDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding FolioPolizaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from FolioPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all FolioPolizaDTO entities.
	 * 
	 * @return List<FolioPolizaDTO> all FolioPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<FolioPolizaDTO> findAll() {
		LogUtil.log("finding all FolioPolizaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from FolioPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Integer getSiguienteNumeroPoliza(String codigoProducto,
			String codigoTipoPoliza) {
		LogDeMidasEJB3.log("siguiente poliza", Level.INFO, null);
		try {
			final String queryString = "select max(folios.id.numeroPoliza) from FolioPolizaDTO folios where folios.id.codigoProducto = :codigoProducto and folios.id.codigoTipoPoliza = :codigoTipoPoliza";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("codigoProducto", String.format("%1$-8s", codigoProducto));
			query.setParameter("codigoTipoPoliza", String.format("%1$-8s", codigoTipoPoliza));
			Integer numPoliza = (Integer) query.getSingleResult();
			if (numPoliza == null)
				numPoliza = Integer.valueOf(1);
			else{
				numPoliza=numPoliza+1;
			}
			return numPoliza;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("count failed", Level.SEVERE, re);
			throw re;
		}
	}
}