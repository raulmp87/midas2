package mx.com.afirme.midas.poliza;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.endoso.EndosoInterfazServiciosRemote;
import mx.com.afirme.midas.endoso.recibo.ReciboInterfazServiciosRemote;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.poliza.renovacion.SeguimientoRenovacionDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.emision.conductoresAdicionales.PolizaAutoSeycos;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasivaDet;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

/**
 * Facade for entity PolizaDTO.
 * 
 * @see .PolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class PolizaFacade extends PolizaSoporteEmision implements PolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	@Resource
	private SessionContext context;
	
	@EJB
	private ReciboInterfazServiciosRemote reciboInterfazFacade;
	
	@EJB
	private EndosoFacadeRemote endosoFacade;
	
	private UsuarioService usuarioService;
	
	public static final BigDecimal ID_PARAMETRO_GRUPO_TIPO_POLIZA = new BigDecimal("30");
	public static final BigDecimal ID_PARAMETRO_GRUPO_TIPO_POLIZA_CASA = new BigDecimal("300010");
	
	@EJB(beanName = "UsuarioServiceDelegate") 
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	/**
	 * Perform an initial save of a previously unsaved PolizaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            PolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public PolizaDTO save(PolizaDTO entity) {
		LogDeMidasEJB3.log("saving PolizaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent PolizaDTO entity.
	 * 
	 * @param entity
	 *            PolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(PolizaDTO entity) {
		LogDeMidasEJB3.log("deleting PolizaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(PolizaDTO.class, entity
					.getIdToPoliza());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved PolizaDTO entity and return it or a copy of it
	 * to the sender. A copy of the PolizaDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            PolizaDTO entity to update
	 * @return PolizaDTO the persisted PolizaDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public PolizaDTO update(PolizaDTO entity) {
		LogDeMidasEJB3.log("updating PolizaDTO instance", Level.INFO, null);
		try {
			PolizaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public PolizaDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding PolizaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			PolizaDTO instance = entityManager.find(PolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all PolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the PolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<PolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<PolizaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding PolizaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from PolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all PolizaDTO entities.
	 * 
	 * @return List<PolizaDTO> all PolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<PolizaDTO> findAll() {
		LogDeMidasEJB3.log("finding all PolizaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from PolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<PolizaDTO> buscarFiltrado(PolizaDTO polizaDTO) {
		try {		
			String queryString = "select model from PolizaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (polizaDTO == null)
				return null;
			if(polizaDTO.getCodigoProducto()!= null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoProducto",String.format("%1$-8s", polizaDTO.getCodigoProducto()));
			if(polizaDTO.getCodigoTipoPoliza()!= null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoTipoPoliza",String.format("%1$-8s", polizaDTO.getCodigoTipoPoliza()));
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroPoliza",polizaDTO.getNumeroPoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroRenovacion",polizaDTO.getNumeroRenovacion());
			if(polizaDTO.getCodigoProductoAsoc()!= null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoProductoAsoc",String.format("%1$-8s", polizaDTO.getCodigoProductoAsoc()));
			if(polizaDTO.getNumeroPolizaSeycos()!= null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroPolizaSeycos",polizaDTO.getNumeroPolizaSeycos());
			if(polizaDTO.getCodigoTipoPolizaAsoc()!= null)
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoTipoPolizaAsoc",String.format("%1$-8s", polizaDTO.getCodigoTipoPolizaAsoc()));
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroPolizaAsoc",polizaDTO.getNumeroPolizaAsoc());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroRenovacionAsoc",polizaDTO.getNumeroRenovacionAsoc());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToPoliza",polizaDTO.getIdToPoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveEstatus",polizaDTO.getClaveEstatus());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "codigoUsuarioCreacion",polizaDTO.getCodigoUsuarioCreacion());
			
			if (polizaDTO.getFechaCreacion() != null) {
				if (Utilerias.esAtributoQueryValido(sWhere)) {
			        sWhere = sWhere.concat(" and ");
			     }
			     sWhere += " model.fechaCreacion BETWEEN :start AND :end ";
			}
			
			
			if(polizaDTO.getCotizacionDTO() != null){
				
				if (polizaDTO.getCotizacionDTO().getSolicitudDTO() != null 
						&& polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoAgente() != null) {
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "cotizacionDTO.solicitudDTO.codigoAgente", polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoAgente());
				}
				
				if(polizaDTO.getCotizacionDTO().getIdToCotizacion() != null)
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "cotizacionDTO.idToCotizacion",polizaDTO.getCotizacionDTO().getIdToCotizacion());
				
				
				if(!StringUtils.isBlank(polizaDTO.getCotizacionDTO().getNombreAsegurado())) {
					
					if (Utilerias.esAtributoQueryValido(sWhere)){
						sWhere = sWhere.concat(" and ");
					}
					
					sWhere +=" UPPER(model.cotizacionDTO.nombreAsegurado) like '"+polizaDTO.getCotizacionDTO().getNombreAsegurado()+"%' and model.cotizacionDTO.claveEstatus = 16 ";
				}	
				if(polizaDTO.getCotizacionDTO().getFechaInicioVigencia() != null &&
						polizaDTO.getCotizacionDTO().getFechaFinVigencia() != null){
					
					if (Utilerias.esAtributoQueryValido(sWhere)){
						sWhere = sWhere.concat(" and ");
					}
					
					sWhere += " model.cotizacionDTO.fechaFinVigencia BETWEEN :desde AND :hasta order by model.cotizacionDTO.fechaFinVigencia";
				}
			}				
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if (polizaDTO.getFechaCreacion() != null) {
				
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(polizaDTO.getFechaCreacion());
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
				
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(gcFechaInicio.getTime());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				
				System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
				System.out.println("Poliza: Rango de Fecha fin ->" + gcFechaFin.getTime());
				
				query.setParameter("start", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
				query.setParameter("end", gcFechaFin.getTime(), TemporalType.TIMESTAMP);
			}
			if (polizaDTO.getCotizacionDTO() != null
					&& polizaDTO.getCotizacionDTO().getFechaInicioVigencia() != null
					&& polizaDTO.getCotizacionDTO().getFechaFinVigencia() != null) {

				System.out.println("Poliza: Fecha Fin Vigencia desde  ->" + polizaDTO.getCotizacionDTO().getFechaInicioVigencia());
				System.out.println("Poliza: Fecha Fin Vigencia hasta ->" + polizaDTO.getCotizacionDTO().getFechaFinVigencia());

				query.setParameter("desde", polizaDTO.getCotizacionDTO()
						.getFechaInicioVigencia(), TemporalType.TIMESTAMP);
				query.setParameter("hasta", polizaDTO.getCotizacionDTO()
						.getFechaFinVigencia(), TemporalType.TIMESTAMP);
			}
			
			if(polizaDTO.getPrimerRegistroACargar() != null && polizaDTO.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(polizaDTO.getPrimerRegistroACargar().intValue());
				query.setMaxResults(polizaDTO.getNumeroMaximoRegistrosACargar().intValue());
			}
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
			
        } catch (RuntimeException re) {
				LogDeMidasEJB3.log("find in PolizaDTO failed", Level.SEVERE, re);
		    throw re;
		}			
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<String, String> emitirPoliza(BigDecimal idToCotizacion,PolizaDTO polizaDTO,Double ivaCotizacion) {
		Map<String, String> mensaje = new HashMap<String, String>();
		try{	
			poblarPolizaDTO(idToCotizacion, polizaDTO);		
			save(polizaDTO);
			emitir(idToCotizacion, polizaDTO,ivaCotizacion);
			entityManager.merge(polizaDTO);
			if (polizaDTO.getCotizacionDTO().getSolicitudDTO()
					.getEsRenovacion() != null
					&& polizaDTO.getCotizacionDTO().getSolicitudDTO()
							.getEsRenovacion().intValue() == 1){
				SeguimientoRenovacionDTO seguimientoRenovacionDTO = new SeguimientoRenovacionDTO();
				seguimientoRenovacionDTO.setDescripcionMovimiento("La renovación se emitió correctamente");
				seguimientoRenovacionDTO.setTipoMovimiento(SistemaPersistencia.RENOVACION_MOVIMIENTO_POLIZA_EMITIDA);
				seguimientoRenovacionDTO.setFechaMovimiento(new Date());
				renovacionPolizaFacadeRemote.agregarDetalleSeguimiento(polizaDTO.getIdToPoliza(), seguimientoRenovacionDTO);
			}
			mensaje.put("icono", "30");//Exito
			mensaje.put("mensaje", "La p\u00f3liza se emiti\u00f3 correctamente.</br> El n\u00famero de  P\u00f3liza es: ");	
			mensaje.put("idpoliza", polizaDTO.getIdToPoliza().toString());
			mensaje.put("nopoliza", polizaDTO.getNumeroPoliza().toString());
			return mensaje;
		} catch (RuntimeException re) {	
			context.setRollbackOnly();
			LogDeMidasEJB3.log("emitirPoliza failed", Level.SEVERE, re);
			mensaje.put("icono", "10");//Error
			mensaje.put("mensaje", "Error al generar la P\u00f3liza.");
			return mensaje;
		}
		
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<PolizaDTO> selectPolizasByEstatus(short cveEstatus){
		Query query;
		String queryString = "select model from PolizaDTO model ";
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		String sWhere = "";
		if(cveEstatus ==1 || cveEstatus ==0){
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, 
					"claveEstatus",cveEstatus);
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
			
		}else{
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<PolizaDTO> selectPolizasByDatosNumPolizaFormato(String numPoliza){
		Query query;
		String queryString = "select model from PolizaDTO model ";
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		String sWhere = "";
		if( numPoliza !=null && numPoliza.length()>0){
			
			String[] numeroDePolizaArray = numPoliza.split("-");
	        String codigoProducto = numeroDePolizaArray[0].substring(0, 2);
	        String codigoTipoPoliza = numeroDePolizaArray[0].substring(2, 4);
	        Integer numeroPoliza = new Integer(numeroDePolizaArray[1]);
	        Integer numeroRenovacion = new Integer(numeroDePolizaArray[2]);
			
		
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoProducto",String.format("%1$-8s", codigoProducto));
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoTipoPoliza",String.format("%1$-8s", codigoTipoPoliza));
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroPoliza",numeroPoliza);
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroRenovacion",numeroRenovacion);
			
			
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
			}
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
			
		}else{
			return null;
		}
		
	}
	
	

	@SuppressWarnings("unchecked")
	public PolizaDTO buscaPrimerRegistro() {
		LogDeMidasEJB3.log("entrando a PolizaFacade.buscaPrimerRegistro", Level.INFO, null);
		try {
			final String queryString = "select model from PolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setFirstResult(0);
			query.setMaxResults(1);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<PolizaDTO> resultList = query.getResultList();
			
			if (resultList != null && resultList.size() > 0) {
				return resultList.get(0);
			}
			return null;
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("PolizaFacade.buscaPrimerRegistro fallo", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public Long obtenerTotalFiltrado(PolizaDTO polizaDTO) {
		try {	
			
			LogDeMidasEJB3.log("Entra a PolizaFacade.obtenerTotalFiltrado", Level.INFO, null);
			
			String queryString = "select count(model) from PolizaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (polizaDTO == null)
				return null;
			if(polizaDTO.getCodigoProducto()!= null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoProducto",String.format("%1$-8s", polizaDTO.getCodigoProducto()));
			if(polizaDTO.getCodigoTipoPoliza()!= null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoTipoPoliza",String.format("%1$-8s", polizaDTO.getCodigoTipoPoliza()));
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroPoliza",polizaDTO.getNumeroPoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroRenovacion",polizaDTO.getNumeroRenovacion());
			if(polizaDTO.getCodigoProductoAsoc()!= null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoProductoAsoc",String.format("%1$-8s", polizaDTO.getCodigoProductoAsoc()));
			if(polizaDTO.getNumeroPolizaSeycos()!= null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroPolizaSeycos",polizaDTO.getNumeroPolizaSeycos());
			if(polizaDTO.getCodigoTipoPolizaAsoc()!= null)
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoTipoPolizaAsoc",String.format("%1$-8s", polizaDTO.getCodigoTipoPolizaAsoc()));
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroPolizaAsoc",polizaDTO.getNumeroPolizaAsoc());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroRenovacionAsoc",polizaDTO.getNumeroRenovacionAsoc());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToPoliza",polizaDTO.getIdToPoliza());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "codigoUsuarioCreacion",polizaDTO.getCodigoUsuarioCreacion());
			
			if (polizaDTO.getFechaCreacion() != null) {
				if (Utilerias.esAtributoQueryValido(sWhere)) {
			        sWhere = sWhere.concat(" and ");
			     }
			     sWhere += " model.fechaCreacion BETWEEN :start AND :end ";
			}
			
			
			if(polizaDTO.getCotizacionDTO() != null){
				if (polizaDTO.getCotizacionDTO().getSolicitudDTO() != null 
						&& polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoAgente() != null) {
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "cotizacionDTO.solicitudDTO.codigoAgente", polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoAgente());
				}
				
				if(polizaDTO.getCotizacionDTO().getIdToCotizacion() != null)
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "cotizacionDTO.idToCotizacion",polizaDTO.getCotizacionDTO().getIdToCotizacion());
				if (!StringUtils.isBlank(polizaDTO.getCotizacionDTO().getNombreAsegurado())) {
					if (Utilerias.esAtributoQueryValido(sWhere)){
						sWhere = sWhere.concat(" and ");
					}
					sWhere +=" UPPER(model.cotizacionDTO.nombreAsegurado) like '"+polizaDTO.getCotizacionDTO().getNombreAsegurado()+"%' and model.cotizacionDTO.claveEstatus = 16 ";
				}
				
			}			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if (polizaDTO.getFechaCreacion() != null) {
				
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(polizaDTO.getFechaCreacion());
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
				
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(gcFechaInicio.getTime());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				
				query.setParameter("start", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
				query.setParameter("end", gcFechaFin.getTime(), TemporalType.TIMESTAMP);
			}
			
			return (Long)query.getSingleResult();
			
        } catch (RuntimeException re) {
			LogDeMidasEJB3.log("PolizaFacade.obtenerTotalFiltrado fallo", Level.SEVERE, re);
		    throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<PolizaDTO> selectPolizasFacultativo(){
		Query query;
		String queryString = "select model from PolizaDTO model where model.cotizacionDTO.tipoNegocioDTO.idTcTipoNegocio in (3,4)";
		query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();	
	}
	
	public boolean existeReciboPagado(BigDecimal idToPoliza) throws Exception{	
		PolizaDTO polizaDTO = findById(idToPoliza);
		List<ReciboDTO> recibosPolizaOriginal = reciboInterfazFacade.consultaRecibos(polizaDTO.getIdToPoliza(),(short)0,polizaDTO.getCodigoUsuarioCreacion());
		ReciboDTO ultimoReciboEmitido= null;
		boolean existeReciboPagado = false;
		for(ReciboDTO reciboDTO:recibosPolizaOriginal){
			if (reciboDTO.getSituacion().trim().equals(ConstantesCotizacion.RECIBO_EMITIDO)){// se valida el primer recibo no pagado emitido
				if (ultimoReciboEmitido == null)
					ultimoReciboEmitido = reciboDTO;
			}else if (reciboDTO.getSituacion().trim().equals(ConstantesCotizacion.RECIBO_PAGADO)){
				existeReciboPagado = true;
			}
		}
		return existeReciboPagado;
	}
	
	//Actualiza en cotizacion los montos de RFP y bonificación de comisión de RFP , colocando los acumulados de todos los endosos 
	//de una póliza particular, prorrateados de acuerdo a los dáas por devengar dados como parámetro. 
	//(Se toman en cuenta todos los endosos a partir del último endoso de cambio de forma de pago emitido).
	public CotizacionDTO setMontosRFPAcumulados(BigDecimal idToPoliza,double diasPorDevengar, CotizacionDTO cotizacion){
		List<EndosoDTO> endosos = endosoFacade.findByProperty("id.idToPoliza", idToPoliza,false);		
		
		double montoRPF = 0;
		double montoComisionRFP = 0;
		double montoBonificacionComisionRFP = 0;
				
		//suma los montos de todos los endosos a partir del último endoso de cambio de forma de pago emitido
		for(EndosoDTO endoso : endosos){			
			//Se obtiene el factor de vigencia
			double diasEndoso = Utilerias.obtenerDiasEntreFechas(endoso.getFechaInicioVigencia(), 
																	endoso.getFechaFinVigencia());			
			double factor = diasPorDevengar/diasEndoso;			
			factor = factor > 1 ? 1 : factor; //es necesario igualar el factor a 1 para no regresar más RPF de los emitidos
			
			montoRPF += endoso.getValorRecargoPagoFrac() * factor;
			montoComisionRFP += endoso.getValorBonifComisionRPF() * factor;
			montoBonificacionComisionRFP += endoso.getValorBonifComisionRPF() * factor;
			
			if(endoso.getClaveTipoEndoso().compareTo(ConstantesCotizacion.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO) == 0)
				break;
		}
		
		cotizacion.setMontoRecargoPagoFraccionado(montoRPF);
		cotizacion.setMontoComisionPagoFraccionado(montoComisionRFP);
		cotizacion.setMontoBonificacionComisionPagoFraccionado(montoBonificacionComisionRFP);
		
		return cotizacion;
	}
	
	
	/**
	 * Genera los recibos de la póliza recibida, con el número de endoso recibido.
	 * El nombre del archivo queda configurado como 'Recibo'+Id de la poliza+numeroEndoso y la extensión PDF.
	 * @param polizaDTO, la póliza cuyos recibos se desean imprimir.
	 * @param Integer numeroEndoso, el endoso cuyos recibos se desean imprimir.
	 * @return true, si la operación es exitosa, false de lo contrario.
	 */
	public boolean generarRecibosEndoso(PolizaDTO polizaDTO, Short numeroEndoso){
		String nombreUsuario = polizaDTO.getCodigoUsuarioCreacion();
		boolean result = true;
		boolean emisionGeneroExcepcion = false;
		if (numeroEndoso == null)
			numeroEndoso = new Short((short)0);
		EndosoId endosoId = new EndosoId();
		endosoId.setIdToPoliza(polizaDTO.getIdToPoliza());
		endosoId.setNumeroEndoso(numeroEndoso);
		EndosoDTO endosoDTO = null;
		try{
			endosoDTO = endosoFacade.findById(endosoId);
			if (endosoDTO == null)
				throw new Exception();
			//Si no se han generado los recibos, se invoca el procedimiento para generarlos.
			List<ReciboDTO> reciboDTOList = null;
			EndosoIDTO endosoIDTO = null;
			if (endosoDTO.getLlaveFiscal() == null || endosoDTO.getLlaveFiscal().trim() == ""){
				try{
					
					LogDeMidasEJB3.log("PolizaFacade.generarRecibosEndoso : Ejecutando notificacion de Emision a Seycos con id de Poliza = " + 
							polizaDTO.getIdToPoliza() + " Endoso Numero= "+numeroEndoso, Level.INFO, null);
					if (numeroEndoso > 0){
						endosoIDTO = endosoInterfazServicios.emiteEndoso(endosoDTO.getId().getIdToPoliza().toString()+ "|" +endosoDTO.getId().getNumeroEndoso(), polizaDTO.getCodigoUsuarioCreacion()).get(0);
					}else{
						reciboDTOList = reciboInterfazFacade.emiteRecibosPoliza(polizaDTO.getIdToPoliza(), nombreUsuario);
					}

				}catch (Exception e){
					emisionGeneroExcepcion = true;
					LogDeMidasEJB3.log("PolizaFacade.generarRecibosEndoso : Excepcion en notificacion de Emision a Seycos con id de Poliza = " + 
							polizaDTO.getIdToPoliza() + " Endoso Numero= "+numeroEndoso, Level.WARNING, e);
					
					LogDeMidasEJB3.log("PolizaFacade.generarRecibosEndoso : Ejecutando reciboInterfazFacade.consultaRecibos con id de Poliza = " + 
							polizaDTO.getIdToPoliza()  + " Endoso Numero= "+numeroEndoso, Level.INFO, null);
					//FIXME Implementar busqueda de recibos por poliza-numeroEndoso
					reciboDTOList = reciboInterfazFacade.consultaRecibos(polizaDTO.getIdToPoliza(), numeroEndoso, nombreUsuario);				
				}
				if(emisionGeneroExcepcion){
					if (reciboDTOList != null && !reciboDTOList.isEmpty()) {
						reciboInterfazFacade.generarRecibos();
						//Se le agrega al endoso la llave fiscal de los recibos
						endosoDTO.setLlaveFiscal(reciboDTOList.get(0).getLlaveFiscal());
						endosoDTO = endosoFacade.update(endosoDTO);
					}					
				}else{
					if(numeroEndoso > 0){
						if (endosoIDTO != null){
							reciboInterfazFacade.generarRecibos();
							if (endosoDTO != null) {
								endosoDTO.setLlaveFiscal(endosoIDTO.getLlaveFiscal());
								endosoDTO = endosoFacade.update(endosoDTO);
							}						
						}
					}else{
						if (reciboDTOList != null && !reciboDTOList.isEmpty()) {
							reciboInterfazFacade.generarRecibos();
							//Se le agrega al endoso la llave fiscal de los recibos
							endosoDTO.setLlaveFiscal(reciboDTOList.get(0).getLlaveFiscal());
							endosoDTO = endosoFacade.update(endosoDTO);
						}	
					}					
				}
			}
			
	
//			byte[] archivoPDF = reciboInterfazFacade.obtieneFactura(endosoDTO.getLlaveFiscal());
//			// Cesar Ayma solicita que no se guarden los recibos 20100810
//			
//			if(archivoPDF != null && archivoPDF.length > 0){
//				//Validaci�n del documento
//	            try {
//	            	PdfReader reader = new PdfReader(archivoPDF);
////					@SuppressWarnings("unused")
//					PdfStamper stamper = new PdfStamper(reader, new ByteArrayOutputStream());
//					stamper.getAcroFields();
//					stamper.getMoreInfo();
//					stamper.getSignatureAppearance();
//					stamper.getWriter();
//					result = true;
//				} catch (Exception e) {
//					//Si se genera un DocumentException, el archivo estó corrupto, se debe borrar e importar nuevamente del WebService.
//					
//					result = false;
//				}
//			}
//			else{
//				result = false;
//			}

		} catch(Exception e){
			result = false;
		}
		return result;
	}
	
	
	private EndosoInterfazServiciosRemote endosoInterfazServicios;
	
	@EJB
	public void setEndosoInterfazServicios(
			EndosoInterfazServiciosRemote endosoInterfazServicios) {
		this.endosoInterfazServicios = endosoInterfazServicios;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> buscarFiltrado(PolizaDTO polizaDTO, Date fechaInicial, Date fechaFinal) {
		
		Query query = getQueryPolizasSimple(polizaDTO, fechaInicial, fechaFinal, false);
		
		if(polizaDTO.getPrimerRegistroACargar() != null && polizaDTO.getNumeroMaximoRegistrosACargar() != null) {
			query.setFirstResult(polizaDTO.getPrimerRegistroACargar().intValue());
			query.setMaxResults(polizaDTO.getNumeroMaximoRegistrosACargar().intValue());
		}
		
		return query.getResultList();
	}
	
	@Override
	public Long obtenerTotalPaginacion(PolizaDTO polizaDTO, Date fechaInicial, Date fechaFinal) {
		
		Query query = getQueryPolizasSimple(polizaDTO, fechaInicial, fechaFinal, true);
		
		BigDecimal total = null;
		Object singleResult = query.getSingleResult();
		if(singleResult instanceof BigDecimal)
			total = (BigDecimal)singleResult;
		else if (singleResult instanceof Long)
			total = BigDecimal.valueOf((Long)singleResult);
		else if (singleResult instanceof Double)
			total = BigDecimal.valueOf((Double)singleResult);

		if (total != null) {
			return total.longValue();
		}		
		return 0l;
	}
	
	private Query getQueryPolizasSimple(PolizaDTO polizaDTO, Date fechaInicial, Date fechaFinal, Boolean esConteo) {
		
		Query query = null;
		
		Map<Integer,Object>params = new HashMap<Integer, Object>();
		int index=1;
		
		String queryString = "";
		String sWhere = "";
		String sGroup = "";
		String sHaving = "";
		String sOrder = "";
		
		if (!esConteo) {
			queryString +="SELECT distinct t0.IDTOPOLIZA, T0.CODIGOPRODUCTO, T0.CODIGOTIPOPOLIZA, T0.NUMEROPOLIZA, T0.NUMERORENOVACION, T0.CVEPOLIZASEYCOS," +
					" T0.IDTOCOTIZACION, T0.FECHACREACION, T0.CLAVEESTATUS, T8.CLAVEAPLICAFLOTILLAS, count(t4.id) ";
		} else {
			queryString +=" SELECT SUM(COUNT(DISTINCT(t0.IDTOPOLIZA))) ";
		}
		
		queryString += " FROM  MIDAS.TOPOLIZA t0, MIDAS.TOSOLICITUD t1, MIDAS.MCOTIZACIONC t2, MIDAS.TOCOTIZACION T7, MIDAS.TOTIPOPOLIZA T8, MIDAS.MINCISOC T4 ";
		
		sWhere += " t1.IDTOSOLICITUD = t0.IDTOSOLICITUD " +
		" AND t2.NUMERO = t0.IDTOCOTIZACION "; 		
		sWhere += " AND t0.IDTOCOTIZACION = t7.IDTOCOTIZACION " +
		" AND T7.IDTOTIPOPOLIZA = T8.IDTOTIPOPOLIZA " +
		" AND t2.ID = t4.MCOTIZACIONC_ID ";
		
		if ((polizaDTO.getNumeroSerie() != null && !polizaDTO.getNumeroSerie().isEmpty())
				|| (polizaDTO.getIdToNegSeccion() != null)
		) {			
			queryString += ", MIDAS.MAUTOINCISOC T5, MIDAS.MAUTOINCISOB T6 ";
			sWhere += " AND t4.ID = t5.MINCISOC_ID " +
			" AND T5.ID = T6.MAUTOINCISOC_ID ";			
		}
		
		if(!esConteo){
			sGroup = " GROUP BY t0.IDTOPOLIZA, T0.CODIGOPRODUCTO, T0.CODIGOTIPOPOLIZA, T0.NUMEROPOLIZA, T0.NUMERORENOVACION, T0.CVEPOLIZASEYCOS," +
			" T0.IDTOCOTIZACION, T0.FECHACREACION, T0.CLAVEESTATUS, T8.CLAVEAPLICAFLOTILLAS ";			
		}else{
			sGroup = " GROUP BY t0.IDTOPOLIZA ";			
		}

		
		sOrder = " ORDER BY T0.NUMEROPOLIZA ";
			
				
		/* Seccion Filtros */
		
		if(polizaDTO.getNumeroPoliza()!=null){
			sWhere += " and T0.NUMEROPOLIZA = ?";
			
			params.put(index,polizaDTO.getNumeroPoliza());
			index++;
		}

		if(polizaDTO.getNumeroPolizaSeycos()!=null){
			sWhere += " and T0.NUMPOLIZASEYCOS = ?";
			
			params.put(index,polizaDTO.getNumeroPolizaSeycos());
			index++;			
		}
			
		if (fechaInicial != null) {
			sWhere += " and T0.fechaCreacion > ?";
			
			GregorianCalendar gcFechaInicio = new GregorianCalendar();
			gcFechaInicio.setTime(fechaInicial);
			gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
			gcFechaInicio.set(GregorianCalendar.SECOND, 0);
			gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
			
			params.put(index,new Timestamp(gcFechaInicio.getTime().getTime()));
			index++;
		}
		
		if (fechaFinal != null) {
			sWhere += " and T0.fechaCreacion <= ?";
			
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(fechaFinal);
			gcFechaFin.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFechaFin.set(GregorianCalendar.MINUTE, 0);
			gcFechaFin.set(GregorianCalendar.SECOND, 0);
			gcFechaFin.set(GregorianCalendar.MILLISECOND, 0);
			gcFechaFin.add(GregorianCalendar.DATE, 1);
			gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
			
			params.put(index,new Timestamp(gcFechaFin.getTime().getTime()));
			index++;			
		}
		
		asignarAgenteFiltro(polizaDTO);
		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			Promotoria promotoria = agenteUsuarioActual.getPromotoria();
			String agenteSubquery = " and T1.codigoAgente in (select a.id from MIDAS.TOAGENTE a where a.IDPROMOTORIA = " + promotoria.getId() + ")";
			sWhere += agenteSubquery;
		}
				
		if(polizaDTO.getCotizacionDTO() != null){

			
			if (polizaDTO.getCotizacionDTO().getSolicitudDTO() != null) {
				
				if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoAgente() != null){
					sWhere += " AND T1.CODIGOAGENTE = ?";
					
					params.put(index,polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoAgente());
					index++;						
				}
				
				if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio()!= null){					
					if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio()!= null){
						sWhere += " AND T1.NEGOCIO_ID = ?";
						
						params.put(index,polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio());
						index++;
					}
				}
			}
				
			if(polizaDTO.getCotizacionDTO().getFolio() != null && polizaDTO.getCotizacionDTO().getFolio().trim().length() > 0){
				sWhere += " AND T7.FOLIO LIKE UPPER('%" + polizaDTO.getCotizacionDTO().getFolio() + "%') "; 
			}
			
			if(polizaDTO.getCotizacionDTO().getIdToPersonaContratante() != null) {
				sWhere +=" and T7.IDTOPERSONACONTRATANTE = ? ";
				
				params.put(index,polizaDTO.getCotizacionDTO().getIdToPersonaContratante());
				index++;
			}

			if(!StringUtils.isBlank(polizaDTO.getCotizacionDTO().getNombreContratante())) {
				sWhere +=" and T7.NOMBRECONTRATANTE like '"+polizaDTO.getCotizacionDTO().getNombreContratante()+"%' ";
			}	
			
			
			if(polizaDTO.getCotizacionDTO().getConflictoNumeroSerie() != null && polizaDTO.getCotizacionDTO().getConflictoNumeroSerie()){
				sWhere += " and T7.CONFLICTONUMEROSERIE = 1";
			}
			
			if(polizaDTO.getCotizacionDTO().getTipoPolizaDTO() != null && polizaDTO.getCotizacionDTO().getTipoPolizaDTO().getClaveAplicaFlotillas() != null
					&& polizaDTO.getCotizacionDTO().getTipoPolizaDTO().getClaveAplicaFlotillas().compareTo(2) < 0){
				
				sWhere += " and T8.CLAVEAPLICAFLOTILLAS = ?";
				
				params.put(index,polizaDTO.getCotizacionDTO().getTipoPolizaDTO().getClaveAplicaFlotillas());
				index++;
			}
				
		}
		
		if(polizaDTO.getNumeroSerie()!=null && !polizaDTO.getNumeroSerie().isEmpty()){
			sWhere += " and T6.NUMEROSERIE = ?";
			
			params.put(index,polizaDTO.getNumeroSerie().toUpperCase());
			index++;
		}		
		
		/* Fin Seccion Filtros*/
		if (polizaDTO.getLimiteInferiorIncisos() != null && polizaDTO.getLimiteSuperiorIncisos() != null
				&& polizaDTO.getLimiteSuperiorIncisos() >= polizaDTO.getLimiteInferiorIncisos()) {
			
			sHaving += " HAVING count(t4.id) >= ? AND count(t4.id) <= ? ";
			
			params.put(index,polizaDTO.getLimiteInferiorIncisos());
			index++;
			params.put(index,polizaDTO.getLimiteSuperiorIncisos());
			index++;
		}		
		
		if (Utilerias.esAtributoQueryValido(sWhere)) {
			if(!esConteo){
				queryString = queryString.concat(" WHERE ").concat(sWhere).concat(sGroup).concat(sHaving).concat(sOrder);
			}else{
				queryString = queryString.concat(" WHERE ").concat(sWhere).concat(sGroup).concat(sHaving);
			}
		}
		
		query = entityManager.createNativeQuery(queryString);
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}			
		
		System.out.print(query.toString());
		
		return query;
		
	}	
	
	@SuppressWarnings("rawtypes")
	private Query getQueryPolizas(PolizaDTO polizaDTO, Date fechaInicial, Date fechaFinal, Boolean esConteo) {
		
		Query query = null;
		
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		String queryString = "";
		String sWhere = "";
		String sGroup = "";
		String sHaving = "";
		String sOrder = "";
		
		if (!esConteo) {
			queryString +="SELECT distinct model, COUNT(distinct incisob.continuity) FROM PolizaDTO model ";
		} else {
			queryString +="SELECT SUM(COUNT(DISTINCT model.idToPoliza)) FROM PolizaDTO model ";
		}
		
		//queryString = "SELECT distinct model" + sConteoIncisos + " FROM PolizaDTO model ";
		

			
		queryString += "JOIN model.cotizacionDTO cotizacion "+
					"JOIN cotizacion.solicitudDTO solicitud "+
					"JOIN solicitud.negocio negocio "+
					"JOIN cotizacion.cotizacionContinuity cotizacionC "+
					"JOIN cotizacionC.incisoContinuities incisoC "+
					"JOIN incisoC.incisos incisoB ";
		
		if ((polizaDTO.getNumeroSerie() != null && !polizaDTO.getNumeroSerie().isEmpty())
				|| (polizaDTO.getIdToNegSeccion() != null)) {
			
			queryString += " " +
					"left JOIN incisoC.autoIncisoContinuity incisos " +
					"left JOIN incisos.autoIncisos incisoAuto ";
			
		}
				
		sWhere += " ((model.claveEstatus = 1 and incisoB.validityInterval.from <= (" +
		"	CASE WHEN :validOn < cotizacion.fechaInicioVigencia THEN cotizacion.fechaInicioVigencia ELSE :validOn END) " +
		/*" and incisoB.validityInterval.to > (" +
		"	CASE WHEN :validOn < cotizacion.fechaInicioVigencia THEN cotizacion.fechaInicioVigencia ELSE :validOn END) " +*/
		" and :endOfTimes = incisoB.recordInterval.to)" +
		" or ( model.claveEstatus = 0 and " +
		" incisoB.recordInterval.to IN (SELECT MAX (control.recordFrom) FROM ControlEndosoCot control " +
		" WHERE control.cotizacionId = cotizacion.idToCotizacion)" +
		" )) ";
		
		
		GregorianCalendar gcFechaActual = new GregorianCalendar();
		gcFechaActual.setTime(new Date());
		gcFechaActual.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gcFechaActual.set(GregorianCalendar.MINUTE, 0);
		gcFechaActual.set(GregorianCalendar.SECOND, 0);
		gcFechaActual.set(GregorianCalendar.MILLISECOND, 0);
		
		Utilerias.agregaHashLista(listaParametrosValidos, "validOn", new Timestamp(gcFechaActual.getTime().getTime()));
		
		GregorianCalendar gcFechaFinTiempos = new GregorianCalendar();
		gcFechaFinTiempos.set(GregorianCalendar.DAY_OF_MONTH, 1);
		gcFechaFinTiempos.set(GregorianCalendar.MONTH, 0);
		gcFechaFinTiempos.set(GregorianCalendar.YEAR, 4000);
		gcFechaFinTiempos.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gcFechaFinTiempos.set(GregorianCalendar.MINUTE, 0);
		gcFechaFinTiempos.set(GregorianCalendar.SECOND, 0);
		gcFechaFinTiempos.set(GregorianCalendar.MILLISECOND, 0);
		
		Utilerias.agregaHashLista(listaParametrosValidos, "endOfTimes", new Timestamp(gcFechaFinTiempos.getTime().getTime()));
		
		sGroup = " GROUP BY model";
				
		/* Seccion Filtros */
		
		if (polizaDTO.getLimiteInferiorIncisos() != null && polizaDTO.getLimiteSuperiorIncisos() != null
				&& polizaDTO.getLimiteSuperiorIncisos() >= polizaDTO.getLimiteInferiorIncisos()) {
			
			sHaving += " HAVING COUNT(incisob) >= :limiteInferior AND COUNT(incisob) <= :limiteSuperior ";
			
			Utilerias.agregaHashLista(listaParametrosValidos, "limiteInferior", polizaDTO.getLimiteInferiorIncisos());
			Utilerias.agregaHashLista(listaParametrosValidos, "limiteSuperior", polizaDTO.getLimiteSuperiorIncisos());
		}
				
				
		if(polizaDTO.getNumeroSerie()!=null && !polizaDTO.getNumeroSerie().isEmpty()){
			sWhere += " and UPPER(incisoAuto.value.numeroSerie) = :numeroSerie";
			Utilerias.agregaHashLista(listaParametrosValidos, "numeroSerie", polizaDTO.getNumeroSerie().toUpperCase());
		}
		
		if(polizaDTO.getIdToNegSeccion() != null){
			sWhere += " and incisoAuto.value.negocioSeccionId = :negocioSeccionId ";
			Utilerias.agregaHashLista(listaParametrosValidos, "negocioSeccionId", polizaDTO.getIdToNegSeccion());
		}
				
		if(polizaDTO.getCodigoProducto()!= null)
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoProducto",String.format("%1$-8s", polizaDTO.getCodigoProducto()));
		if(polizaDTO.getCodigoTipoPoliza()!= null)
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoTipoPoliza",String.format("%1$-8s", polizaDTO.getCodigoTipoPoliza()));
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroPoliza",polizaDTO.getNumeroPoliza());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroRenovacion",polizaDTO.getNumeroRenovacion());
		if(polizaDTO.getCodigoProductoAsoc()!= null)
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoProductoAsoc",String.format("%1$-8s", polizaDTO.getCodigoProductoAsoc()));
		if(polizaDTO.getNumeroPolizaSeycos()!= null)
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroPolizaSeycos",polizaDTO.getNumeroPolizaSeycos());
		if(polizaDTO.getCodigoTipoPolizaAsoc()!= null)
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoTipoPolizaAsoc",String.format("%1$-8s", polizaDTO.getCodigoTipoPolizaAsoc()));
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroPolizaAsoc",polizaDTO.getNumeroPolizaAsoc());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroRenovacionAsoc",polizaDTO.getNumeroRenovacionAsoc());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToPoliza",polizaDTO.getIdToPoliza());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveEstatus",polizaDTO.getClaveEstatus());
		sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "codigoUsuarioCreacion",polizaDTO.getCodigoUsuarioCreacion());
		
		if (fechaInicial != null) {
			sWhere += " and model.fechaCreacion > :fechaInicial";
			
			GregorianCalendar gcFechaInicio = new GregorianCalendar();
			gcFechaInicio.setTime(fechaInicial);
			gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
			gcFechaInicio.set(GregorianCalendar.SECOND, 0);
			gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
			
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaInicial", new Timestamp(gcFechaInicio.getTime().getTime()));
		}
		
		if (fechaFinal != null) {
			sWhere += " and model.fechaCreacion <= :fechaFinal";
			
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(fechaFinal);
			gcFechaFin.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFechaFin.set(GregorianCalendar.MINUTE, 0);
			gcFechaFin.set(GregorianCalendar.SECOND, 0);
			gcFechaFin.set(GregorianCalendar.MILLISECOND, 0);
			gcFechaFin.add(GregorianCalendar.DATE, 1);
			gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
			
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaFinal", new Timestamp(gcFechaFin.getTime().getTime()));
		}
		
		asignarAgenteFiltro(polizaDTO);
		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			Promotoria promotoria = agenteUsuarioActual.getPromotoria();
			String agenteSubquery = " and solicitud.codigoAgente in (select a.id from Agente a where a.promotoria.id = " + promotoria.getId() + ")";
			sWhere += agenteSubquery;
		}
				
		if(polizaDTO.getCotizacionDTO() != null){

			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "cotizacion.claveEstatus", CotizacionDTO.ESTATUS_COT_EMITIDA, false);
			
			if (polizaDTO.getCotizacionDTO().getSolicitudDTO() != null) {
				
				if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoAgente() != null){
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitud.codigoAgente", polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoAgente(),false);						
				}
				if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio()!= null){
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "negocio.claveNegocio", polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getClaveNegocio(),false);
					
					if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio()!= null){
						sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "negocio.idToNegocio", polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio(),false);
					}
					if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO()!= null){
						if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getIdToProducto()!= null){
							sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitud.productoDTO.idToProducto", polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getIdToProducto(),false);
						}							
					}
				}
				
				if(polizaDTO.getCotizacionDTO().getFolio() != null && polizaDTO.getCotizacionDTO().getFolio().trim().length() > 0){
					sWhere += " AND UPPER(cotizacion.folio) LIKE UPPER('%" + polizaDTO.getCotizacionDTO().getFolio() + "%') "; 
				}
			}
			
			if(polizaDTO.getCotizacionDTO().getIdToCotizacion() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "cotizacion.idToCotizacion",polizaDTO.getCotizacionDTO().getIdToCotizacion(),false);
			
			
			if(!StringUtils.isBlank(polizaDTO.getCotizacionDTO().getNombreAsegurado())) {
				sWhere +=" and UPPER(cotizacion.nombreAsegurado) like '"+polizaDTO.getCotizacionDTO().getNombreAsegurado()+"%' ";
			}	

			if(!StringUtils.isBlank(polizaDTO.getCotizacionDTO().getNombreContratante())) {
				sWhere +=" and UPPER(cotizacion.nombreContratante) like '"+polizaDTO.getCotizacionDTO().getNombreContratante()+"%' ";
			}	
			
			if(polizaDTO.getCotizacionDTO().getFechaInicioVigencia() != null 
					&& polizaDTO.getCotizacionDTO().getFechaFinVigencia() != null) {
				
				sWhere += " and (cotizacion.fechaFinVigencia BETWEEN :desde AND :hasta)";
				
				GregorianCalendar gcFechaDesde = new GregorianCalendar();
				gcFechaDesde.setTime(polizaDTO.getCotizacionDTO().getFechaInicioVigencia());
				gcFechaDesde.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaDesde.set(GregorianCalendar.MINUTE, 0);
				gcFechaDesde.set(GregorianCalendar.SECOND, 0);
				gcFechaDesde.set(GregorianCalendar.MILLISECOND, 0);
				
				Utilerias.agregaHashLista(listaParametrosValidos, "desde", new Timestamp(gcFechaDesde.getTime().getTime()));
				
				GregorianCalendar gcFechaHasta = new GregorianCalendar();
				gcFechaHasta.setTime(polizaDTO.getCotizacionDTO().getFechaFinVigencia());
				gcFechaHasta.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaHasta.set(GregorianCalendar.MINUTE, 0);
				gcFechaHasta.set(GregorianCalendar.SECOND, 0);
				gcFechaHasta.set(GregorianCalendar.MILLISECOND, 0);
				
				Utilerias.agregaHashLista(listaParametrosValidos, "hasta", new Timestamp(gcFechaHasta.getTime().getTime()));
				
				sOrder += " ORDER BY model.cotizacionDTO.fechaFinVigencia ";
				
			} else {
				sOrder += " ORDER BY model.fechaCreacion DESC ";
			}
			
			if(polizaDTO.getCotizacionDTO().getConflictoNumeroSerie() != null && polizaDTO.getCotizacionDTO().getConflictoNumeroSerie()){
				sWhere += " and model.cotizacionDTO.conflictoNumeroSerie = 1";
			}
			
			if(polizaDTO.getCotizacionDTO().getTipoPolizaDTO() != null && polizaDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "cotizacion.tipoPolizaDTO.idToTipoPoliza",polizaDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza(), false);
			}
			
			if(polizaDTO.getCotizacionDTO().getTipoPolizaDTO() != null && polizaDTO.getCotizacionDTO().getTipoPolizaDTO().getClaveAplicaFlotillas() != null
					&& polizaDTO.getCotizacionDTO().getTipoPolizaDTO().getClaveAplicaFlotillas().compareTo(2) < 0){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "cotizacion.tipoPolizaDTO.claveAplicaFlotillas",polizaDTO.getCotizacionDTO().getTipoPolizaDTO().getClaveAplicaFlotillas(),false);
			}
			
			if(polizaDTO.getCotizacionDTO().getIdToPersonaContratante() != null)
			{
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "cotizacion.idToPersonaContratante",polizaDTO.getCotizacionDTO().getIdToPersonaContratante(),false);
			}
				
		}
		
		/* Fin Seccion Filtros*/
		
		if (Utilerias.esAtributoQueryValido(sWhere)) {
			queryString = queryString.concat(" WHERE ").concat(sWhere).concat(sGroup).concat(sHaving);
			
		
		}
		
		query = entityManager.createQuery(queryString);
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
//		if (esConteo) {
//		 query.setMaxResults(1);
//		}
		
		return query;
		
	}
	
	
	/**
	 * Limita el filtro a un agente en caso de ser necesario de acuerdo a los permisos del usuario.
	 * @param polizaDTO
	 */
	private void asignarAgenteFiltro(PolizaDTO polizaDTO) {
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			inicializarSolicitud(polizaDTO);
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			SolicitudDTO solicitud = polizaDTO.getCotizacionDTO().getSolicitudDTO();
			solicitud.setCodigoAgente(BigDecimal.valueOf(agenteUsuarioActual.getId()));
		}
	}
	
	private void inicializarSolicitud(PolizaDTO polizaDTO) {
		if (polizaDTO == null) {
			polizaDTO = new PolizaDTO();
		}
		
		if (polizaDTO.getCotizacionDTO() == null) {
			polizaDTO.setCotizacionDTO(new CotizacionDTO());
		}
		
		if (polizaDTO.getCotizacionDTO().getSolicitudDTO() == null) {
			polizaDTO.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
		}
	}

	///
	private Query getQueryFiltradoRenovacion (PolizaDTO polizaDTO, Date fechaInicial, Date fechaFinal, Boolean isCount, Boolean isFixEndoso,
			Boolean exclusionRenovacion) {
		try {
			
			String queryString = "";
			
			String queryStringSelect = "";
			
			if(isCount){
				queryStringSelect = "SELECT COUNT(DISTINCT t0.IDTOPOLIZA)";
			}else{
				queryStringSelect = "SELECT DISTINCT t0.IDTOPOLIZA, t0.IDTOSOLICITUD, t0.IDTOCOTIZACION, " +
				"t0.CODIGOPRODUCTO, t0.CODIGOTIPOPOLIZA, t0.NUMEROPOLIZA, t0.NUMERORENOVACION, " +
				"t10.NOMBRECONTRATANTE, MAX(t10.FECHAFINVIGENCIA), t0.CLAVEESTATUS, t2.DESCRIPCIONNEGOCIO," +
				"t4.DESCRIPCIONPRODUCTO, t5.DESCRIPCIONTIPOPOLIZA, AGENTE.DESCRIPCIONEJECUTIVO, t3.NOMBREAGENTE,AGENTE.* ";	
			}
			
			String sWhere = "";
			
			if (polizaDTO == null)
				return null;
			
			String filtroGerencia = "";
			if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getAgente() != null){
				if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getAgente().getPromotoria() != null &&
						polizaDTO.getCotizacionDTO().getSolicitudDTO().getAgente().getPromotoria().getEjecutivo() != null &&
						polizaDTO.getCotizacionDTO().getSolicitudDTO().getAgente().getPromotoria().getEjecutivo().getGerencia() != null &&
						polizaDTO.getCotizacionDTO().getSolicitudDTO().getAgente().getPromotoria().getEjecutivo().getGerencia().getId() != null){
					filtroGerencia = " WHERE a5.ID = " + polizaDTO.getCotizacionDTO().getSolicitudDTO().getAgente().getPromotoria().getEjecutivo().getGerencia().getId() + " ";
				}
			}
			if(polizaDTO.getCotizacionDTO() != null &&
					polizaDTO.getCotizacionDTO().getSolicitudDTO() != null &&
					polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoEjecutivo() != null){
				if(filtroGerencia.equals("")){
					filtroGerencia += " WHERE ";
				}else{
					filtroGerencia += " AND ";
				}
				filtroGerencia += " a4.ID = " + polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoEjecutivo();						
			}
			
			String ordenRenovacionTbl = "";
			if(polizaDTO.getIdToOrdenRenovacion() != null && polizaDTO.getIdToOrdenRenovacion().compareTo(BigDecimal.ZERO)>0){
				ordenRenovacionTbl = "MIDAS.TOORDENRENOVACIONMASIVADET t12, ";
			}
			
			if((polizaDTO.getNumeroSerie()!=null && !polizaDTO.getNumeroSerie().isEmpty()) || polizaDTO.getIdToNegSeccion() != null){
				if(!isCount){
					queryStringSelect += ", t6.NUMEROSERIE, t8.DESCRIPCIONSECCION, t0.FECHACREACION, t11.NOMBRE ";
				}
				queryString +=  "FROM MIDAS.MINCISOC MIC " +
						"	LEFT OUTER JOIN MIDAS.MAUTOINCISOC MAC ON MAC.MINCISOC_ID = MIC.id " +
						"	LEFT OUTER JOIN MIDAS.MAUTOINCISOB t6 ON t6.MAUTOINCISOC_ID = MAC.id, " +
						""+ordenRenovacionTbl+"MIDAS.TCCENTROEMISOR t11, MIDAS.MCOTIZACIONB t10, " +
						"MIDAS.MCOTIZACIONC t9, MIDAS.TOSECCION t8, MIDAS.TONEGSECCION t7, MIDAS.TOTIPOPOLIZA t5, MIDAS.TOPRODUCTO t4, " +
						"MIDAS.TONEGOCIO t2, MIDAS.TOCOTIZACION t1, MIDAS.TOPOLIZA t0, MIDAS.TOSOLICITUD t3 " +
						"INNER JOIN (SELECT a1.ID, a1.IDAGENTE, a3.ID as IDPROMOTORIA, " +
						" a7.NOMBRE AS DESCRIPCIONEJECUTIVO, " +
						"a3.DESCRIPCION as DESCRIPCIONPROMOTORIA, a5.DESCRIPCION as DESCRIPCIONGERENCIA " +
						"FROM MIDAS.TOAGENTE a1 " +
						"LEFT OUTER JOIN MIDAS.TOPROMOTORIA a3 ON a3.ID = a1.IDPROMOTORIA " +
						"LEFT OUTER JOIN MIDAS.TOEJECUTIVO a4 ON a4.ID = a3.EJECUTIVO_ID " +
						"LEFT OUTER JOIN MIDAS.TOGERENCIA a5 ON a5.ID = a4.GERENCIA_ID " +
						"LEFT OUTER JOIN SEYCOS.PERSONA a7 ON a7.ID_PERSONA = a4.IDPERSONA " +
						" " + filtroGerencia +						
						")AGENTE ON AGENTE.ID = t3.CODIGOAGENTE";
			}else{
				if(!isCount){
					queryStringSelect += ", INDIVIDUALES.NUMEROSERIE, INDIVIDUALES.DESCRIPCIONSECCION, t0.FECHACREACION, t11.NOMBRE ";
				}
				queryString +=   "FROM "+ordenRenovacionTbl+"MIDAS.TCCENTROEMISOR t11, MIDAS.MCOTIZACIONB t10, MIDAS.MCOTIZACIONC t9, MIDAS.TOTIPOPOLIZA t5, MIDAS.TOPRODUCTO t4, " +
				"MIDAS.TONEGOCIO t2, MIDAS.TOCOTIZACION t1, " + 						
				"MIDAS.TOPOLIZA t0, MIDAS.TOSOLICITUD t3 " +
				"INNER JOIN (SELECT a1.ID, a1.IDAGENTE, a3.ID as IDPROMOTORIA, " +
				" a7.NOMBRE AS DESCRIPCIONEJECUTIVO, " +
				"a3.DESCRIPCION as DESCRIPCIONPROMOTORIA, a5.DESCRIPCION as DESCRIPCIONGERENCIA " +
				"FROM MIDAS.TOAGENTE a1 " +
				"LEFT OUTER JOIN MIDAS.TOPROMOTORIA a3 ON a3.ID = a1.IDPROMOTORIA " + 
				"LEFT OUTER JOIN MIDAS.TOEJECUTIVO a4 ON a4.ID = a3.EJECUTIVO_ID " + 
				"LEFT OUTER JOIN MIDAS.TOGERENCIA a5 ON a5.ID = a4.GERENCIA_ID " + 
				"LEFT OUTER JOIN SEYCOS.PERSONA a7 ON a7.ID_PERSONA = a4.IDPERSONA " +
				" " + filtroGerencia +
				")AGENTE ON AGENTE.ID = t3.CODIGOAGENTE " +
				" LEFT JOIN ( " +
				" SELECT b1.IDTOCOTIZACION AS ID,  b2.NUMEROSERIE ,  b5.DESCRIPCIONSECCION " +
				" FROM MIDAS.TOCOTIZACION b1, MIDAS.TOINCISOAUTOCOT b2, MIDAS.TOTIPOPOLIZA b3 , MIDAS.TONEGSECCION b4, MIDAS.TOSECCION b5 " +
				" WHERE b1.IDTOCOTIZACION = b2.COTIZACION_ID " +
				" AND b1.IDTOTIPOPOLIZA = b3.IDTOTIPOPOLIZA " +
				" AND b3.CLAVEAPLICAFLOTILLAS = 0 " +
				" AND b2.NEGOCIOSECCION_ID = b4.IDTONEGSECCION " +
				" AND b4.IDTOSECCION = b5.IDTOSECCION " +
				" ) INDIVIDUALES ON INDIVIDUALES.ID = t3.IDTOSOLICITUD ";
			}
			
			queryString = queryStringSelect + " " + queryString;
			
			if(polizaDTO.getNumeroSerie()!=null && !polizaDTO.getNumeroSerie().isEmpty()){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " UPPER(t6.NUMEROSERIE) = '" +  polizaDTO.getNumeroSerie().toUpperCase() + "'";
			}
			if(polizaDTO.getIdToNegSeccion() !=null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " t6.NEGOCIOSECCION_ID = " + polizaDTO.getIdToNegSeccion();
			}
			if(polizaDTO.getNumeroPoliza() !=null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " t0.NUMEROPOLIZA = " + polizaDTO.getNumeroPoliza();
			}
			if(polizaDTO.getIdCentroEmisor() !=null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " t0.IDTCCENTROEMISOR = " + polizaDTO.getIdCentroEmisor();
			}
			if(polizaDTO.getCodigoProducto() !=null && !polizaDTO.getCodigoProducto().isEmpty()){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " t0.CODIGOPRODUCTO = " + polizaDTO.getCodigoProducto();
			}
			if(polizaDTO.getCodigoTipoPoliza() !=null && !polizaDTO.getCodigoTipoPoliza().isEmpty()){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " t0.CODIGOTIPOPOLIZA = " + polizaDTO.getCodigoTipoPoliza();
			}
			if(polizaDTO.getNumeroRenovacion() !=null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " t0.NUMERORENOVACION = " + polizaDTO.getNumeroRenovacion();
			}
			if(polizaDTO.getClaveEstatus() !=null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " t0.CLAVEESTATUS = " + polizaDTO.getClaveEstatus();
			}
			
			asignarAgenteFiltro(polizaDTO);
			
			if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
				Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
				Promotoria promotoria = agenteUsuarioActual.getPromotoria();
				String agenteSubquery = " t3.codigoAgente in (select asub.id from MIDAS.TOAGENTE asub where asub.idpromotoria  = " + promotoria.getId() + ")";
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += agenteSubquery;
			}
			
			if(polizaDTO.getCotizacionDTO() != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " t1.CLAVEESTATUS = " + CotizacionDTO.ESTATUS_COT_EMITIDA;
				
				if (polizaDTO.getCotizacionDTO().getSolicitudDTO() != null) {
					
					if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoAgente() != null){
						if(sWhere.equals("")){
							sWhere += "";
						}else{
							sWhere += " and ";
						}
						sWhere += " t3.CODIGOAGENTE = " + polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoAgente();						
					}
					if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getAgente() != null){
						if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getAgente().getIdAgente() != null){
							if(sWhere.equals("")){
								sWhere += "";
							}else{
								sWhere += " and ";
							}
							sWhere += " AGENTE.IDAGENTE = " + polizaDTO.getCotizacionDTO().getSolicitudDTO().getAgente().getIdAgente();
						}
						if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getAgente().getPromotoria() != null &&
								polizaDTO.getCotizacionDTO().getSolicitudDTO().getAgente().getPromotoria().getId() != null){
							if(sWhere.equals("")){
								sWhere += "";
							}else{
								sWhere += " and ";
							}
							sWhere += " AGENTE.IDPROMOTORIA = " + polizaDTO.getCotizacionDTO().getSolicitudDTO().getAgente().getPromotoria().getId() + " ";
						}
					}
					if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio()!= null){
						if(sWhere.equals("")){
							sWhere += "";
						}else{
							sWhere += " and ";
						}
						sWhere += " t2.CLAVENEGOCIO = '" + polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getClaveNegocio() + "'";
						
						if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio()!= null){
							if(sWhere.equals("")){
								sWhere += "";
							}else{
								sWhere += " and ";
							}
							sWhere += " t3.NEGOCIO_ID = " + polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio();
						}
						if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO()!= null){
							if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getIdToProducto()!= null){
								if(sWhere.equals("")){
									sWhere += "";
								}else{
									sWhere += " and ";
								}
								sWhere += " t3.IDTOPRODUCTO = " + polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getIdToProducto();
							}							
						}
					}
				}


				if(!StringUtils.isBlank(polizaDTO.getCotizacionDTO().getNombreContratante())) {
					if(sWhere.equals("")){
						sWhere += "";
					}else{
						sWhere += " and ";
					}
					sWhere +=" UPPER(t10.NOMBRECONTRATANTE) LIKE '"+polizaDTO.getCotizacionDTO().getNombreContratante()+"%' ";
				}	
				
				if(polizaDTO.getCotizacionDTO().getFechaFinVigencia() != null){
					if(sWhere.equals("")){
						sWhere += "";
					}else{
						sWhere += " and ";
					}
					sWhere += " t10.fechaFinVigencia >= ? ";
				}
				
				if(polizaDTO.getCotizacionDTO().getFechaFinVigenciaHasta() != null){
					if(sWhere.equals("")){
						sWhere += "";
					}else{
						sWhere += " and ";
					}
					sWhere += " t10.fechaFinVigencia <= ? ";
				}
				
				if(polizaDTO.getFechaCreacion() != null){
					if(sWhere.equals("")){
						sWhere += "";
					}else{
						sWhere += " and ";
					}
					sWhere += " t0.fechaCreacion >= ? ";
				}
				
				if(polizaDTO.getFechaCreacionHasta() != null){
					if(sWhere.equals("")){
						sWhere += "";
					}else{
						sWhere += " and ";
					}
					sWhere += " t0.fechaCreacion <= ? ";
				}
				
				if(polizaDTO.getCotizacionDTO().getTipoPolizaDTO() != null && polizaDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza() != null){
					if(sWhere.equals("")){
						sWhere += "";
					}else{
						sWhere += " and ";
					}
					sWhere += " t1.IDTOTIPOPOLIZA = " + polizaDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza();
				}
					
			}
			
			if(polizaDTO.getIdToOrdenRenovacion() != null && polizaDTO.getIdToOrdenRenovacion().compareTo(BigDecimal.ZERO)>0){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += "  t0.IDTOPOLIZA = t12.IDTOPOLIZARENOVADA and  t12.IDTOORDENRENOVACION = " + polizaDTO.getIdToOrdenRenovacion();
			}
						
			
			if((polizaDTO.getNumeroSerie()!=null && !polizaDTO.getNumeroSerie().isEmpty()) || polizaDTO.getIdToNegSeccion() != null){
				sWhere += " AND t1.IDTOCOTIZACION = t0.IDTOCOTIZACION " +
						" AND t3.IDTOSOLICITUD = t1.IDTOSOLICITUD  " +
						"AND t2.IDTONEGOCIO = t3.NEGOCIO_ID  " +
						"AND t4.IDTOPRODUCTO = t3.IDTOPRODUCTO  " +
						"AND t5.IDTOTIPOPOLIZA = t1.IDTOTIPOPOLIZA " +
						"AND MIC.MCOTIZACIONC_ID = t9.ID " +
						"AND t7.IDTONEGSECCION = t6.NEGOCIOSECCION_ID  " +
						"AND t8.IDTOSECCION = t7.IDTOSECCION  " +						
						"AND t9.ID = t10.MCOTIZACIONC_ID " +
						"AND t9.NUMERO = t1.IDTOCOTIZACION " +
						"AND t10.RECORDTO = DATE'4000-01-01' " +
						"AND t10.FECHAFINVIGENCIA > ? " +
						"AND t11.ID = t0.IDTCCENTROEMISOR "; 
				if(!isCount){
					sWhere += "	GROUP BY t0.IDTOPOLIZA, t0.IDTOSOLICITUD, t0.IDTOCOTIZACION, t0.CODIGOPRODUCTO, t0.CODIGOTIPOPOLIZA, " +
						"t0.NUMEROPOLIZA, t0.NUMERORENOVACION, t10.NOMBRECONTRATANTE, t0.CLAVEESTATUS, t2.DESCRIPCIONNEGOCIO, " +
						"t4.DESCRIPCIONPRODUCTO, t5.DESCRIPCIONTIPOPOLIZA, t3.NOMBREEJECUTIVO, t3.NOMBREOFICINA, " +
						"t3.NOMBREAGENTE, AGENTE.ID, AGENTE.IDAGENTE, AGENTE.IDPROMOTORIA, AGENTE.DESCRIPCIONEJECUTIVO, " +
						"AGENTE.DESCRIPCIONPROMOTORIA, AGENTE.DESCRIPCIONGERENCIA , " +
						"t6.NUMEROSERIE, t8.DESCRIPCIONSECCION, t0.FECHACREACION, t11.NOMBRE ";
				}
			}else{
				sWhere += " AND t1.IDTOCOTIZACION = t0.IDTOCOTIZACION " +
				"AND t3.IDTOSOLICITUD = t1.IDTOSOLICITUD  " +
				"AND t2.IDTONEGOCIO = t3.NEGOCIO_ID  " +
				"AND t4.IDTOPRODUCTO = t3.IDTOPRODUCTO  " +
				"AND t5.IDTOTIPOPOLIZA = t1.IDTOTIPOPOLIZA " +
				"AND t9.ID = t10.MCOTIZACIONC_ID " +
				"AND t9.NUMERO = t1.IDTOCOTIZACION " +
				"AND t10.RECORDTO = DATE'4000-01-01' " +
				"AND t10.FECHAFINVIGENCIA > ? " +
				"AND t11.ID = t0.IDTCCENTROEMISOR ";
				if(!isCount){
					sWhere +=  " GROUP BY t0.IDTOPOLIZA, t0.IDTOSOLICITUD, t0.IDTOCOTIZACION, t0.CODIGOPRODUCTO, t0.CODIGOTIPOPOLIZA, " +
				"t0.NUMEROPOLIZA, t0.NUMERORENOVACION, t10.NOMBRECONTRATANTE, t0.CLAVEESTATUS, t2.DESCRIPCIONNEGOCIO, " +
				"t4.DESCRIPCIONPRODUCTO, t5.DESCRIPCIONTIPOPOLIZA, t3.NOMBREEJECUTIVO, " + 
				"t3.NOMBREAGENTE, AGENTE.ID, AGENTE.IDAGENTE, AGENTE.IDPROMOTORIA, AGENTE.DESCRIPCIONEJECUTIVO, " +
				"AGENTE.DESCRIPCIONPROMOTORIA, AGENTE.DESCRIPCIONGERENCIA , " +
				"INDIVIDUALES.NUMEROSERIE, INDIVIDUALES.DESCRIPCIONSECCION, t0.FECHACREACION, t11.NOMBRE ";
				}
			}
			 
			
			queryString = queryString.concat(" where ").concat(sWhere);
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			
			int pos = 1;
			if(polizaDTO.getCotizacionDTO() != null && polizaDTO.getCotizacionDTO().getFechaFinVigencia() != null){
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(polizaDTO.getCotizacionDTO().getFechaFinVigencia());
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
			
				query.setParameter(pos,gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
				pos++;
			}
			
			if(polizaDTO.getCotizacionDTO() != null && polizaDTO.getCotizacionDTO().getFechaFinVigenciaHasta() != null){
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(polizaDTO.getCotizacionDTO().getFechaFinVigenciaHasta());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				query.setParameter(pos,gcFechaFin.getTime(), TemporalType.TIMESTAMP);
				pos++;
			}
			
			if(polizaDTO.getFechaCreacion() != null){
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(polizaDTO.getFechaCreacion());
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
			
				query.setParameter(pos,gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
				pos++;
			}
			
			if(polizaDTO.getFechaCreacionHasta() != null){
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(polizaDTO.getFechaCreacionHasta());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				query.setParameter(pos,gcFechaFin.getTime(), TemporalType.TIMESTAMP);
				pos++;
			}
			
			query.setParameter(pos, new Date(), TemporalType.TIMESTAMP);
			
			
			if(polizaDTO.getPrimerRegistroACargar() != null && polizaDTO.getNumeroMaximoRegistrosACargar() != null){
				query.setFirstResult(polizaDTO.getPrimerRegistroACargar());
				query.setMaxResults(polizaDTO.getNumeroMaximoRegistrosACargar());
			}
			
			
			return query;
			
        } catch (RuntimeException re) {
				LogDeMidasEJB3.log("find in PolizaDTO failed", Level.SEVERE, re);
		    throw re;
		}			

	}
	
	@Override
	public Long obtieneTotalFiltradoRenovacion(PolizaDTO polizaDTO, Date fechaInicial, Date fechaFinal, Boolean isCount, 
			Boolean exclusionRenovacion) {
		Long total = (long) 0;
		Query query = this.getQueryFiltradoRenovacion(polizaDTO, fechaInicial, fechaFinal, isCount, false, exclusionRenovacion);
		try{
			Object object = query.getSingleResult();
			BigDecimal numero = BigDecimal.ZERO;
			if(object instanceof BigDecimal)
				numero = (BigDecimal) object;
			else if (object instanceof Long)
				numero = BigDecimal.valueOf((Long)object);
			else if (object instanceof Double)
				numero = BigDecimal.valueOf((Double)object);
			
			total = numero.longValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return total;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PolizaDTO> buscarFiltradoRenovacion(PolizaDTO polizaDTO, Date fechaInicial, Date fechaFinal, Boolean isCount, 
			Boolean isFixEndoso, Boolean exclusionRenovacion) {
	
		Query query = this.getQueryFiltradoRenovacion(polizaDTO, fechaInicial, fechaFinal, isCount, isFixEndoso, exclusionRenovacion);
		Object result  = query.getResultList();
		
		List<PolizaDTO> list = new ArrayList<PolizaDTO>(1);
		if(result instanceof List)  {
		List<Object> listaResultados = (List<Object>) result;				
		for(Object object : listaResultados){
			Object[] singleResult = (Object[]) object;
			
			PolizaDTO poliza = new PolizaDTO();
			CotizacionDTO cotizacion = new CotizacionDTO();
			SolicitudDTO solicitud = new SolicitudDTO();

			BigDecimal idToPoliza = null;
			if(singleResult[0] instanceof BigDecimal)
				idToPoliza = (BigDecimal)singleResult[0];
			else if (singleResult[0] instanceof Long)
				idToPoliza = BigDecimal.valueOf((Long)singleResult[0]);
			else if (singleResult[0] instanceof Double)
				idToPoliza = BigDecimal.valueOf((Double)singleResult[0]);
			poliza.setIdToPoliza(idToPoliza);
			
			try{
				BigDecimal idToCotizacion = null;
				if(singleResult[2] instanceof BigDecimal)
					idToCotizacion = (BigDecimal)singleResult[2];
				else if (singleResult[2] instanceof Long)
					idToCotizacion = BigDecimal.valueOf((Long)singleResult[2]);
				else if (singleResult[2] instanceof Double)
					idToCotizacion = BigDecimal.valueOf((Double)singleResult[2]);
				cotizacion.setIdToCotizacion(idToCotizacion);
			}catch(Exception e){}
			
			try{
			String codigoProducto = singleResult[3].toString();
			poliza.setCodigoProducto(codigoProducto);
			}catch(Exception e){}
			
			try{
			String codigoTipoPoliza = singleResult[4].toString();
			poliza.setCodigoTipoPoliza(codigoTipoPoliza);
			}catch(Exception e){}
			
			try{
			Integer numeroPoliza = null;
			if(singleResult[5] instanceof Integer)
				numeroPoliza = (Integer)singleResult[5];
			else 
				numeroPoliza = Integer.valueOf(singleResult[5].toString());
			poliza.setNumeroPoliza(numeroPoliza);
			}catch(Exception e){}
			
			try{
			Integer numeroRenovacion = null;
			if(singleResult[6] instanceof Integer)
				numeroRenovacion = (Integer)singleResult[6];
			else 
				numeroRenovacion = Integer.valueOf(singleResult[6].toString());
			poliza.setNumeroRenovacion(numeroRenovacion);
			}catch(Exception e){}
			
			try{
			String nombreContratante = singleResult[7].toString();
			cotizacion.setNombreContratante(nombreContratante);
			}catch(Exception e){}
			
			//
			try{
			String numeroSerie = singleResult[21].toString();
			poliza.setNumeroSerie(numeroSerie);
			}catch(Exception e){}
			
			SeccionDTO seccion = new SeccionDTO();
			try{
			String descripcionSeccion = singleResult[22].toString();					
			seccion.setDescripcion(descripcionSeccion);
			}catch(Exception e){}
			
			try{
			Date fechaFinVigencia = (Date) singleResult[8];
			cotizacion.setFechaFinVigencia(fechaFinVigencia);
			}catch(Exception e){}
			
			try{
			Short claveEstatus = Short.valueOf(singleResult[9].toString());
			poliza.setClaveEstatus(claveEstatus);
			}catch(Exception e){}
			
			Negocio negocio = new Negocio();
			try{
			String descripcionNegocio = singleResult[10].toString();					
			negocio.setDescripcionNegocio(descripcionNegocio);
			}catch(Exception e){}
			
			ProductoDTO producto = new ProductoDTO();
			try{
			String descripcionProducto = singleResult[11].toString();					
			producto.setDescripcion(descripcionProducto);
			}catch(Exception e){}
			
			TipoPolizaDTO tipoPoliza = new TipoPolizaDTO();
			try{
			String descripcionTipoPoliza = singleResult[12].toString();					
			tipoPoliza.setDescripcion(descripcionTipoPoliza);
			}catch(Exception e){}
			
			Agente agente = new Agente();
			Promotoria promotoria = new Promotoria();
			Ejecutivo ejecutivo = new Ejecutivo();
			Gerencia gerencia = new Gerencia();
			try{
			String descripcionGerencia = singleResult[20].toString();
			gerencia.setDescripcion(descripcionGerencia);
			}catch(Exception e){}
			ejecutivo.setGerencia(gerencia);
			promotoria.setEjecutivo(ejecutivo);
			
			try{
			String nombreEjecutivo = singleResult[13].toString();
			solicitud.setNombreEjecutivo(nombreEjecutivo);
			}catch(Exception e){}
			
			try{
			String descripcionPromotoria = singleResult[19].toString();
			promotoria.setDescripcion(descripcionPromotoria);
			}catch(Exception e){}
			
			try{
			String nombreAgente = singleResult[14].toString();
			solicitud.setNombreAgente(nombreAgente);
			}catch(Exception e){}
			
			try{
			Long idAgente = Long.valueOf(singleResult[16].toString());
			agente.setIdAgente(idAgente);
			}catch(Exception e){}
		
			try{
				Date fechaCreacion = (Date)singleResult[23];					
				poliza.setFechaCreacion(fechaCreacion);
			}catch(Exception e){}
			
			try{
				String centroEmisorDescripcion = singleResult[24].toString();					
				cotizacion.setCentroEmisorDescripcion(centroEmisorDescripcion);
			}catch(Exception e){}
			
			agente.setPromotoria(promotoria);
			cotizacion.setSeccionDTO(seccion);
			solicitud.setAgente(agente);
			cotizacion.setTipoPolizaDTO(tipoPoliza);
			solicitud.setProductoDTO(producto);
			solicitud.setNegocio(negocio);
			cotizacion.setSolicitudDTO(solicitud);
			poliza.setCotizacionDTO(cotizacion);
			list.add(poliza);
		}
		}
		return list;
	}

	@Override
	public PolizaDTO find(String codigoProducto, String codigoTipoPoliza,
			Integer numeroPoliza, Integer numeroRenovacion) {
		String jpql = "select m from PolizaDTO m where trim(m.codigoProducto) = :codigoProducto " +
				"and trim(m.codigoTipoPoliza) = :codigoTipoPoliza and m.numeroPoliza = :numeroPoliza " +
				"and m.numeroRenovacion = :numeroRenovacion";
		TypedQuery<PolizaDTO> query = entityManager.createQuery(jpql, PolizaDTO.class);
		query
			.setParameter("codigoProducto", codigoProducto)
			.setParameter("codigoTipoPoliza", codigoTipoPoliza)
			.setParameter("numeroPoliza", numeroPoliza)
			.setParameter("numeroRenovacion", numeroRenovacion);
		List<PolizaDTO> list = query.getResultList();
		if (list.size() == 0) {
			return null;
		}
		
		if (list.size() == 1) {
			return list.get(0);
		}
		throw new RuntimeException("Regreso mas de uno.");
	}
	
	@Override
	public PolizaDTO find(String clavePolizaSeycos) {
		String jpql = "select m from PolizaDTO m where m.clavePolizaSeycos = :clavePolizaSeycos";
		TypedQuery<PolizaDTO> query = entityManager.createQuery(jpql,
				PolizaDTO.class);
		query.setParameter("clavePolizaSeycos", clavePolizaSeycos);

		List<PolizaDTO> list = query.getResultList();
		if (list.size() == 0) {
			return null;
		}

		if (list.size() == 1) {
			return list.get(0);
		}
		throw new RuntimeException("Regreso mas de uno.");
	}
	

	@Override
	public PolizaDTO find(NumeroPolizaCompleto numeroPolizaCompleto) {
		if (numeroPolizaCompleto == null) {
			throw new IllegalArgumentException("numeroPolizaCompleto no puede ser nulo.");
		}		
		
		PolizaDTO poliza;		
		if (numeroPolizaCompleto instanceof NumeroPolizaCompletoSeycos) {
			NumeroPolizaCompletoSeycos numeroPolizaCompletoSeycos = (NumeroPolizaCompletoSeycos) numeroPolizaCompleto;			
			poliza = find(numeroPolizaCompletoSeycos);
		} else if (numeroPolizaCompleto instanceof NumeroPolizaCompletoMidas) {
			NumeroPolizaCompletoMidas numeroPolizaCompletoMidas = (NumeroPolizaCompletoMidas) numeroPolizaCompleto;
			poliza = find(numeroPolizaCompletoMidas);
		} else {
			throw new IllegalArgumentException("Tipo de NumeroPolizaCompleto desconocido");
		}

		return poliza;
	}
	
	@Override
	public PolizaDTO find(NumeroPolizaCompletoMidas numeroPolizaCompletoMidas) {
		if (numeroPolizaCompletoMidas == null) {
			throw new IllegalArgumentException("numeroPolizaCompletoMidas no puede ser nulo.");
		}
		
		return find(numeroPolizaCompletoMidas.getCodigoProducto(), numeroPolizaCompletoMidas.getCodigoTipoPoliza(), numeroPolizaCompletoMidas.getNumeroPoliza(), numeroPolizaCompletoMidas.getNumeroRenovacion());
	}

	@Override
	public PolizaDTO find(NumeroPolizaCompletoSeycos numeroCompletoSeycos) {
		if (numeroCompletoSeycos == null) {
			throw new IllegalArgumentException("numeroCompletoSeycos no puede ser nulo.");
		}
		
		if (numeroCompletoSeycos.getOrigenNumeroPolizaCompletoMidas() != null) {
			return find(numeroCompletoSeycos.getOrigenNumeroPolizaCompletoMidas());
		}
		
		return find(numeroCompletoSeycos.toString());
	}
	
	@Override
	public boolean esPolizaAutoplazo(BigDecimal idToPoliza, List<SeccionDTO> seccionList){
		Query query;
		String queryString = "SELECT p.* FROM MIDAS.topoliza p, MIDAS.tocotizacion c, MIDAS.toseccioncot s,  ";
		queryString = queryString.concat(" (select decode(level,1, '%Arrendadora Afirme%',2, '%Banca Afirme%') n from dual connect by level <=2) l ");
		queryString = queryString.concat(" WHERE p.idToPoliza = " + idToPoliza);
		queryString = queryString.concat(" and p.idtocotizacion = c.idtocotizacion ");
		queryString = queryString.concat(" and c.idtocotizacion = s.idtocotizacion ");
		queryString = queryString.concat(" and upper(c.nombreContratante) LIKE upper(l.n) ");
		
		if(seccionList != null && !seccionList.isEmpty()){
			queryString = queryString.concat(" and s.idtoseccion in ( " + obtenerIds(seccionList) + ") ");
		}
		
		query = entityManager.createNativeQuery(queryString, PolizaDTO.class);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		if(query.getResultList().size() > 0){
			return true;
		}
		return false;
	}
	
	public String obtenerIds(List<SeccionDTO> seccionList) {
		StringBuilder ids = new StringBuilder("");
		for(SeccionDTO seccion : seccionList){
			if(!ids.toString().isEmpty()) ids.append(",");
			ids.append(seccion.getIdToSeccion());
		}		
		return ids.toString();
	}

	@Override
	public BigDecimal obtenerNumeroCotizacionSeycos(BigDecimal idToPoliza){
		Query query;
		String queryString = "SELECT id_seycos  ";
		queryString = queryString.concat(" FROM seycos.conv_midas_seycos ");
		queryString = queryString.concat(" WHERE tipo = 'POLIZA' ");
		queryString = queryString.concat("  AND id_midas = " + idToPoliza +"||'|0' ");
		
		query = entityManager.createNativeQuery(queryString);
		
		BigDecimal total = null;
		Object singleResult = query.getSingleResult();
		if(singleResult instanceof BigDecimal)
			total = (BigDecimal)singleResult;
		else if (singleResult instanceof Long)
			total = BigDecimal.valueOf((Long)singleResult);
		else if (singleResult instanceof Double)
			total = BigDecimal.valueOf((Double)singleResult);
		
		return total;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String obtenerXMLPrimerReciboPoliza(BigDecimal idToPoliza){
		
		String xml = null;
		BigDecimal idCotizacionSeycos = obtenerNumeroCotizacionSeycos(idToPoliza);
		
		Query query;
		String queryString = "select wf.xmlclob  ";
		queryString = queryString.concat(" from seycos.pol_recibo pr ");
		queryString = queryString.concat(" inner join wfactura.wfacturas wf on wf.serie = pr.serie_folio_fiscal  ");
		queryString = queryString.concat(" and  wf.folio = pr.num_folio_fiscal ");
		queryString = queryString.concat(" where pr.id_cotizacion   =  " + idCotizacionSeycos + "");
		queryString = queryString.concat(" and pr.num_exhibicion   = 1  ");
		queryString = queryString.concat(" and pr.id_prog_pago     = 1 ");
		queryString = queryString.concat(" and pr.id_version_rbo   = 1 ");
		queryString = queryString.concat(" order by wf.id ");
		
		query = entityManager.createNativeQuery(queryString);
		
		Object result  = query.getResultList();
		
		if(result instanceof List)  {
			List<Object> listaResultados = (List<Object>) result;				
			if(listaResultados != null && listaResultados.size() > 0){
				Object singleResult = listaResultados.get(0);
				xml = singleResult.toString();
			}
		}
		
		return xml;
	}
	
	@Override
	public boolean esPolizaCambioIVA(BigDecimal idToPoliza){
		
		try{
			BigDecimal idCotizacionSeycos = obtenerNumeroCotizacionSeycos(idToPoliza);
		
			Query query;
			String queryString = "SELECT ID_COTIZACION  ";
			queryString = queryString.concat(" FROM seycos.PASO_CAMBIOIVA2014 ");
			queryString = queryString.concat(" WHERE ID_COTIZACION = " + idCotizacionSeycos +" ");
		
			query = entityManager.createNativeQuery(queryString);
		
			if(query.getResultList().size() > 0){
				return true;
			}
		}catch(Exception e){
		}
		return false;
	}
	
	@Override
	public boolean esPolizaCasa(BigDecimal idToPoliza) {
		final PolizaDTO poliza = findById(idToPoliza);
		final String producto = poliza.getCodigoProducto();
		final String tipoPoliza = poliza.getCodigoTipoPoliza();
		final ParametroGeneralDTO parametro = parametroGeneral
				.findById(new ParametroGeneralId(
						ID_PARAMETRO_GRUPO_TIPO_POLIZA,
						ID_PARAMETRO_GRUPO_TIPO_POLIZA_CASA));
		final List<String> tipoPolizaProductoCasa = Arrays.asList(parametro.getValor().split(","));
		if(tipoPolizaProductoCasa.contains(producto + tipoPoliza)) {
			return true;
		}
		return false;
	}
	
	@Override
	public BigDecimal obtenerIdDoctoVersSeycos(BigDecimal idToPoliza, BigDecimal numeroEndoso){
		Query query;
		String queryString = "SELECT id_seycos  ";
		queryString = queryString.concat(" FROM seycos.conv_midas_seycos ");
		queryString = queryString.concat(" WHERE tipo = 'ENDOSO' ");
		queryString = queryString.concat("  AND id_midas = ? ");
		
		query = entityManager.createNativeQuery(queryString);
		
		query.setParameter(1, idToPoliza +"|"+numeroEndoso);
		
		BigDecimal total = null;
		
		try {
			total = (BigDecimal) query.getSingleResult();
		} catch(Exception e){
			LogDeMidasEJB3.log("No se pudo obtener el idDoctoVers de la p\u00F3liza "+ idToPoliza +" y endoso:"+ numeroEndoso, Level.INFO, e);
			throw new ApplicationException("No se pudo obtener registros para la p\u00F3liza "+ idToPoliza +" y endoso:"+ numeroEndoso);
		}	
		
		return total;
	}
	
	
	@Override
	public BigDecimal obtenerNumeroCotizacionSeycos(String numeroPoliza, int numeroRenovacion, int numeroEndoso){
		String queryString = "SELECT pp.id_cotizacion ";
		queryString = queryString.concat(" FROM seycos.pol_poliza pp ");
		queryString = queryString.concat(" left join seycos.conv_midas_seycos c on pp.id_docto_vers = c.id_seycos ");
		queryString = queryString.concat(" WHERE c.tipo = 'POLIZA' ");
		queryString = queryString.concat(" and pp.num_poliza = ? ");
		queryString = queryString.concat(" and pp.num_renov_pol = ? ");
		queryString = queryString.concat(" and pp.id_version_pol = 1 ");
		queryString = queryString.concat(" and c.id_midas like ? ");
		
		Query query = entityManager.createNativeQuery(queryString);
		
		query.setParameter(1, numeroPoliza);
		query.setParameter(2, numeroRenovacion);
		query.setParameter(3, "%|"+numeroEndoso);
		
		BigDecimal total = null;
		
		try {
			total = (BigDecimal) query.getSingleResult();
		} catch(Exception e){
			LogDeMidasEJB3.log("No se pudo obtener el idCotizacion de seycos de la p\u00F3liza "+ numeroPoliza +" y endoso:"+ numeroEndoso+"\n"+e.getMessage(), Level.INFO, e);
			throw new ApplicationException("No se pudo obtener el idCotizacion de la p\u00F3liza "+ numeroPoliza  +" y endoso:"+ numeroEndoso);
		}
		
		return total;
	}
	
	/**
	 * Bucar la poliza
	 * @param filtros
	 * @return
	 */
	@Override
	public List<Object[]> buscarPolizaSeycos(Map<String, Object> filtros) {
		
		String queryString = "SELECT T0.CODIGOPRODUCTO, T0.CODIGOTIPOPOLIZA, T0.NUMERORENOVACION, "
				+ "T5.ID_COTIZACION, T5.ID_VERSION_POL, T5.ID_LIN_NEGOCIO, T5.ID_INCISO, "
				+ "T0.NUMEROPOLIZA, MIN(T9.RECORDFROM) AS FECHAINICIOVIGENCIA, MAX(T9.RECORDTO) AS FECHAFINVIGENCIA, "
				+ "CASE WHEN T0.CLAVEESTATUS = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END AS CLAVEESTATUS, T11.NOMBRECONTRATANTE, T5.NOM_COND_HAB, "
				+ "T2.DESCRIPCIONTIPOPOLIZA, T10.NOMBRE AS PAQUETE, "
				+ "T5.DESC_VEHIC, "
				+ "T5.NUM_SERIE, T5.NUM_MOTOR, "
				+ "T5.NOM_COND_ADIC_1, T5.NOM_COND_ADIC_2, T5.NOM_COND_ADIC_3, T5.NOM_COND_ADIC_4, T5.NOM_COND_ADIC_5 " +
			"FROM MIDAS.TOPOLIZA T0 " 
				+ "INNER JOIN MIDAS.TOCOTIZACION T1 ON T1.IDTOCOTIZACION = T0.IDTOCOTIZACION "
				+ "INNER JOIN MIDAS.TOTIPOPOLIZA T2 ON T2.IDTOTIPOPOLIZA = T1.IDTOTIPOPOLIZA "
				+ "INNER JOIN SEYCOS.CONV_MIDAS_SEYCOS T3 ON T3.ID_MIDAS = T0.IDTOPOLIZA || '|0' AND T3.TIPO = 'POLIZA' "
				+ "INNER JOIN SEYCOS.POL_POLIZA T4 ON  T4.ID_COTIZACION   = T3.ID_SEYCOS "
				+ "INNER JOIN SEYCOS.POL_AUTO T5 ON T5.ID_COTIZACION = T4.ID_COTIZACION "
				+ "INNER JOIN MIDAS.MCOTIZACIONC T6 ON T0.IDTOCOTIZACION = T6.NUMERO "
				+ "INNER JOIN MIDAS.MINCISOC T7 ON T6.ID = T7.MCOTIZACIONC_ID "
				+ "INNER JOIN MIDAS.MAUTOINCISOC T8 ON T7.ID = T8.MINCISOC_ID "
				+ "INNER JOIN MIDAS.MAUTOINCISOB T9 ON T8.ID = T9.MAUTOINCISOC_ID "
				+ "INNER JOIN MIDAS.TOPAQUETEPOLIZA T10 ON T9.PAQUETE_ID = T10.IDTOPAQUETEPOLIZA "
				+ "INNER JOIN MIDAS.MCOTIZACIONB T11 ON T6.ID = T11.MCOTIZACIONC_ID "
				+ "INNER JOIN MIDAS.MSECCIONINCISOC  T12 ON T9.MAUTOINCISOC_ID = T12.MINCISOC_ID "		
				+ " WHERE T0.CODIGOPRODUCTO = " + filtros.get("codigoProducto") +
					" AND T0.CODIGOTIPOPOLIZA = " + filtros.get("codigoTipoPoliza") +
					" AND T0.NUMEROPOLIZA = " + filtros.get("numeroPoliza") +
					" AND T0.NUMERORENOVACION = " + filtros.get("numeroRenovacion") +					
					" AND T5.ID_INCISO = " + filtros.get("numeroInciso") + 
					" AND T0.CLAVEESTATUS = 1"
				+ " GROUP BY T0.CODIGOPRODUCTO, T0.CODIGOTIPOPOLIZA, T0.NUMERORENOVACION,"
				+ "T5.ID_COTIZACION, T5.ID_VERSION_POL, T5.ID_LIN_NEGOCIO, T5.ID_INCISO,"
				+ "T0.NUMEROPOLIZA, T0.CLAVEESTATUS, T11.NOMBRECONTRATANTE, T5.NOM_COND_HAB,"
				+ "T2.DESCRIPCIONTIPOPOLIZA, T10.NOMBRE, T5.DESC_VEHIC, T5.NUM_SERIE, T5.NUM_MOTOR,"
				+ "T5.NOM_COND_ADIC_1, T5.NOM_COND_ADIC_2, T5.NOM_COND_ADIC_3, T5.NOM_COND_ADIC_4, T5.NOM_COND_ADIC_5" ;
	
		Query query = entityManager.createNativeQuery(queryString, "PolizaSeycosMapping");
		List<Object[]> results = query.getResultList();
		
		return results;
	}
	
	/**
	 * Guardar conductores adicionales
	 * @param polizaAutoSeycos
	 * @return
	 */
	@Override
	public int guardarConductoresAdicionales(PolizaAutoSeycos polizaAutoSeycos){
		
		String queryString = "UPDATE SEYCOS.POL_AUTO P SET ";
			
		queryString += " P.NOM_COND_ADIC_1 ='"+polizaAutoSeycos.getNombreConductorAdicional1()+"',";
		queryString += " P.NOM_COND_ADIC_2 ='"+polizaAutoSeycos.getNombreConductorAdicional2()+"',";
		queryString += " P.NOM_COND_ADIC_3 ='"+polizaAutoSeycos.getNombreConductorAdicional3()+"',";	
		queryString += " P.NOM_COND_ADIC_4 ='"+polizaAutoSeycos.getNombreConductorAdicional4()+"',";
		queryString += " P.NOM_COND_ADIC_5 ='"+polizaAutoSeycos.getNombreConductorAdicional5()+"',";
		
		queryString = queryString.substring(0, queryString.length()-1);
		queryString += " WHERE P.ID_COTIZACION= " + polizaAutoSeycos.getIdPolizaAutoSeycos().getIdCotizacion()
				+ " AND P.ID_VERSION_POL = " + polizaAutoSeycos.getIdPolizaAutoSeycos().getIdVersionPoliza()
				+ " AND P.ID_LIN_NEGOCIO = " + polizaAutoSeycos.getIdPolizaAutoSeycos().getLineaNegocio();
		
		if(polizaAutoSeycos.getIdPolizaAutoSeycos().getNumeroInciso() != null){
			queryString += " AND P.ID_INCISO= " +polizaAutoSeycos.getIdPolizaAutoSeycos().getNumeroInciso();
		} else {
			queryString += " AND P.ID_INCISO= 1"; //Valor por default
		}
		Query query = entityManager.createNativeQuery(queryString);
		int update = query.executeUpdate();
		return update;
	}
}