package mx.com.afirme.midas.poliza.seccion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity SeccionPolizaDTO.
 * 
 * @see .SeccionPolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SeccionPolizaFacade implements SeccionPolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SeccionPolizaDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SeccionPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public SeccionPolizaDTO save(SeccionPolizaDTO entity) {
		LogDeMidasEJB3
				.log("saving SeccionPolizaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SeccionPolizaDTO entity.
	 * 
	 * @param entity
	 *            SeccionPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SeccionPolizaDTO entity) {
		LogDeMidasEJB3.log("deleting SeccionPolizaDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(SeccionPolizaDTO.class, entity
					.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SeccionPolizaDTO entity and return it or a
	 * copy of it to the sender. A copy of the SeccionPolizaDTO entity parameter
	 * is returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            SeccionPolizaDTO entity to update
	 * @return SeccionPolizaDTO the persisted SeccionPolizaDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SeccionPolizaDTO update(SeccionPolizaDTO entity) {
		LogDeMidasEJB3.log("updating SeccionPolizaDTO instance", Level.INFO,
				null);
		try {
			SeccionPolizaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SeccionPolizaDTO findById(SeccionPolizaId id) {
		LogDeMidasEJB3.log("finding SeccionPolizaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SeccionPolizaDTO instance = entityManager.find(
					SeccionPolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SeccionPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SeccionPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SeccionPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionPolizaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SeccionPolizaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SeccionPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SeccionPolizaDTO entities.
	 * 
	 * @return List<SeccionPolizaDTO> all SeccionPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionPolizaDTO> findAll() {
		LogDeMidasEJB3.log("finding all SeccionPolizaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from SeccionPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public void insertSeccionesPorCotizacion(BigDecimal idToCotizacion,BigDecimal idToPoliza){

		String queryString = "INSERT INTO MIDAS.toSeccionPol (idToPoliza, numeroInciso, idToSeccion, valorPrimaNeta) "+
					"SELECT "+idToPoliza+", seccioncot.numeroInciso, seccioncot.idToSeccion, seccioncot.valorPrimaNeta "+
					"FROM MIDAS.toSeccionCot seccioncot "+
					"WHERE seccioncot.idToCotizacion ="+idToCotizacion+" AND seccioncot.clavecontrato = 1";
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();		
	}
}