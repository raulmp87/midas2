package mx.com.afirme.midas.poliza.agrupacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity AgrupacionPolizaDTO.
 * 
 * @see .AgrupacionPolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class AgrupacionPolizaFacade implements AgrupacionPolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved AgrupacionPolizaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            AgrupacionPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(AgrupacionPolizaDTO entity) {
		LogUtil.log("saving AgrupacionPolizaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent AgrupacionPolizaDTO entity.
	 * 
	 * @param entity
	 *            AgrupacionPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(AgrupacionPolizaDTO entity) {
		LogUtil.log("deleting AgrupacionPolizaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(AgrupacionPolizaDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved AgrupacionPolizaDTO entity and return it or a
	 * copy of it to the sender. A copy of the AgrupacionPolizaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            AgrupacionPolizaDTO entity to update
	 * @return AgrupacionPolizaDTO the persisted AgrupacionPolizaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public AgrupacionPolizaDTO update(AgrupacionPolizaDTO entity) {
		LogUtil.log("updating AgrupacionPolizaDTO instance", Level.INFO, null);
		try {
			AgrupacionPolizaDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public AgrupacionPolizaDTO findById(AgrupacionPolizaId id) {
		LogUtil.log("finding AgrupacionPolizaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			AgrupacionPolizaDTO instance = entityManager.find(
					AgrupacionPolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all AgrupacionPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the AgrupacionPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<AgrupacionPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<AgrupacionPolizaDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding AgrupacionPolizaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from AgrupacionPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all AgrupacionPolizaDTO entities.
	 * 
	 * @return List<AgrupacionPolizaDTO> all AgrupacionPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<AgrupacionPolizaDTO> findAll() {
		LogUtil.log("finding all AgrupacionPolizaDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from AgrupacionPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void insertAgrupacionesPorCotizacion(BigDecimal idToCotizacion,BigDecimal idToPoliza){
		String queryString = "INSERT INTO MIDAS.toAgrupacionPol (idToPoliza, numeroAgrupacion, claveTipoAgrupacion, idToSeccion,valorSumaAsegurada, valorCuota, valorPrimaNeta) "+
			"SELECT "+idToPoliza+", agrupacioncot.numeroAgrupacion, agrupacioncot.clavetipoagrupacion,agrupacioncot.idToSeccion,agrupacioncot.valorSumaAsegurada, agrupacioncot.valorCuota, agrupacioncot.valorPrimaNeta "+
			"FROM MIDAS.toAgrupacionCot agrupacioncot "+
			"WHERE agrupacioncot.idToCotizacion = "+idToCotizacion;
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();		
	}
}