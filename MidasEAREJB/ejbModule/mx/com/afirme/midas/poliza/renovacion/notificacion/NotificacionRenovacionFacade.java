package mx.com.afirme.midas.poliza.renovacion.notificacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.usuario.LogUtil;

@Stateless
public class NotificacionRenovacionFacade implements
		NotificacionRenovacionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TtNotificacionRenovacion
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            TtNotificacionRenovacion entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(NotificacionRenovacionDTO entity) {
		LogUtil.log("saving NotificacionRenovacionDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent NotificacionRenovacionDTO entity.
	 * 
	 * @param entity
	 *            NotificacionRenovacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(NotificacionRenovacionDTO entity) {
		LogUtil.log("deleting NotificacionRenovacionDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(
					NotificacionRenovacionDTO.class, entity
							.getIdTtNotificacionRenovacion());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved NotificacionRenovacionDTO entity and return it
	 * or a copy of it to the sender. A copy of the RenovacionPolizaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            NotificacionRenovacionDTO entity to update
	 * @return RenovacionPolizaDTO the persisted NotificacionRenovacionDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public NotificacionRenovacionDTO update(NotificacionRenovacionDTO entity) {
		LogUtil.log("updating NotificacionRenovacionDTO instance", Level.INFO,
				null);
		try {
			NotificacionRenovacionDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public NotificacionRenovacionDTO findById(BigDecimal id) {
		LogUtil.log(
				"finding NotificacionRenovacionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			NotificacionRenovacionDTO instance = entityManager.find(
					NotificacionRenovacionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all NotificacionRenovacionDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the NotificacionRenovacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RenovacionPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<NotificacionRenovacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log(
				"finding NotificacionRenovacionDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from NotificacionRenovacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RenovacionPolizaDTO entities.
	 * 
	 * @return List<RenovacionPolizaDTO> all NotificacionRenovacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<NotificacionRenovacionDTO> findAll() {
		LogUtil.log("finding all NotificacionRenovacionDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from NotificacionRenovacionDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}
