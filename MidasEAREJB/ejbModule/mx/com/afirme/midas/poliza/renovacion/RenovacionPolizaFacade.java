package mx.com.afirme.midas.poliza.renovacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoSoporteEmision;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroFacadeRemote;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.usuario.LogUtil;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class RenovacionPolizaFacade extends EndosoSoporteEmision implements
		RenovacionPolizaFacadeRemote {

	@Resource
	private SessionContext context;

	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private IndemnizacionFacadeRemote indemnizacionFacadeRemote;
	
	@EJB
	private ReporteSiniestroFacadeRemote reporteSiniestroFacadeRemote;

	public void agregarDetalleSeguimiento(BigDecimal idToPoliza,
			SeguimientoRenovacionDTO seguimientoRenovacionDTO) {
		LogUtil
				.log("saving SeguimientoRenovacioDTO instance", Level.INFO,
						null);
		try {
			List<RenovacionPolizaDTO> instances = this.findByProperty(
					"polizaDTO.idToPoliza", idToPoliza);
			if (instances != null && instances.size() > 0) {
				RenovacionPolizaDTO instance = instances.get(0);
				List<SeguimientoRenovacionDTO> seguimientoRenovacion = instance
						.getSeguimientoRenovacion();
				if (seguimientoRenovacionDTO.getRenovacionPolizaDTO() == null)
					seguimientoRenovacionDTO.setRenovacionPolizaDTO(instance);
				if (seguimientoRenovacionDTO.getTipoMovimiento().intValue() == SeguimientoRenovacionDTO.TIPO_ENVIO_ALERTAMIENTO
						.intValue()) {
					Integer noNotificacion = this.getSiguienteNumeroNotificacion(idToPoliza);
					seguimientoRenovacionDTO.setDescripcionMovimiento(seguimientoRenovacionDTO.getDescripcionMovimiento() + "Notificaci�n No:" + noNotificacion);
				}
				seguimientoRenovacion.add(seguimientoRenovacionDTO);
				entityManager.persist(instance);
				LogUtil.log("save successful", Level.INFO, null);
			}

		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Perform an initial save of a previously unsaved Torenovacionpoliza
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Torenovacionpoliza entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RenovacionPolizaDTO entity) {
		LogUtil.log("saving RenovacionPolizaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent RenovacionPolizaDTO entity.
	 * 
	 * @param entity
	 *            RenovacionPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RenovacionPolizaDTO entity) {
		LogUtil.log("deleting RenovacionPolizaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(RenovacionPolizaDTO.class,
					entity.getIdToRenovacionPoliza());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved RenovacionPolizaDTO entity and return it or a
	 * copy of it to the sender. A copy of the RenovacionPolizaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            RenovacionPolizaDTO entity to update
	 * @return RenovacionPolizaDTO the persisted RenovacionPolizaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RenovacionPolizaDTO update(RenovacionPolizaDTO entity) {
		LogUtil.log("updating RenovacionPolizaDTO instance", Level.INFO, null);
		try {
			RenovacionPolizaDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RenovacionPolizaDTO findById(BigDecimal id) {
		LogUtil.log("finding RenovacionPolizaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			RenovacionPolizaDTO instance = entityManager.find(
					RenovacionPolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RenovacionPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RenovacionPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RenovacionPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RenovacionPolizaDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding RenovacionPolizaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RenovacionPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RenovacionPolizaDTO entities.
	 * 
	 * @return List<RenovacionPolizaDTO> all RenovacionPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RenovacionPolizaDTO> findAll() {
		LogUtil.log("finding all RenovacionPolizaDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from RenovacionPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Map<String, String> procesaRenovacionPoliza(BigDecimal idTopoliza,
			String usuarioCreacion, String usuarioAsigacion) {
		// TODO Auto-generated method stub
		Map<String, String> estatusOperacion = new HashMap<String, String>();
		
		EndosoDTO ultimoEndoso = endoso.getUltimoEndoso(idTopoliza);
		PolizaDTO polizaDTO = ultimoEndoso.getPolizaDTO();
		String mensaje = "";
		try {
			if (polizaDTO != null) {
				CotizacionDTO cotizacionBase = cotizacion.findById(ultimoEndoso
						.getIdToCotizacion());
				SolicitudDTO solicitudDTO = copiaSolicitud(polizaDTO
						.getIdToPoliza(), usuarioCreacion, polizaDTO
						.getCotizacionDTO().getFechaFinVigencia(),
						cotizacionBase, null, null, Short
								.valueOf(SistemaPersistencia.SOLICITUD_POLIZA));
				solicitudDTO.setIdToPolizaAnterior(polizaDTO.getIdToPoliza());
				solicitudDTO.setIdToPolizaEndosada(null);
				solicitudDTO.setEsRenovacion((short)1);
				solicitudDTO.setClaveEstatus(SistemaPersistencia.SOLICITUD_EN_PROCESO_AUTORIZACION);
				entityManager.persist(solicitudDTO);
				LogDeMidasEJB3.log("se copio la solicitud", Level.INFO, null);
				CotizacionDTO cotizacionDTO = creaCopiaCotizacion(solicitudDTO,
						usuarioCreacion, cotizacionBase, null);
				LogDeMidasEJB3.log("se copio la cotizacion", Level.INFO, null);
				cotizacionDTO.setSolicitudDTO(solicitudDTO);
				cotizacionDTO = cotizacion.save(cotizacionDTO);
				LogDeMidasEJB3.log(
						"cotizacion guardada exitosamente IdToCotizacion="
								+ cotizacionDTO.getIdToCotizacion(),
						Level.INFO, null);
				cotizacionDTO.setCodigoUsuarioCreacion(usuarioCreacion);
				cotizacionDTO.setCodigoUsuarioCotizacion(usuarioAsigacion);

				Double diasVigencia = obtenerDiasEntreFechas(polizaDTO
						.getCotizacionDTO().getFechaInicioVigencia(), polizaDTO
						.getCotizacionDTO().getFechaFinVigencia());

				cotizacionDTO.setFechaFinVigencia(sumaDias(cotizacionDTO
						.getFechaInicioVigencia(), diasVigencia.intValue()));
				cotizacionDTO
						.setClaveEstatus(SistemaPersistencia.COTIZACION_ASIGNADA);
				entityManager.merge(cotizacionDTO);
				copiaRelacionesCotizacion(cotizacionBase,cotizacionDTO);
				LogDeMidasEJB3.log(
						"se copiaron las relacionesd e la cotizacion",
						Level.INFO, null);

				entityManager.merge(cotizacionDTO);
				LogDeMidasEJB3
						.log(
								"termino la asignacion de la renovacion de cotizacion de poliza",
								Level.INFO, null);
				this.insertaAsignacionSeguimientoPoliza(polizaDTO,
						usuarioAsigacion, usuarioCreacion, cotizacionDTO);
				mensaje = "Se genero correctamente la renovacion de la poliza: "
						+ polizaDTO.getNumeroPolizaFormateada();
				estatusOperacion.put("icono", "30");// Exito
				estatusOperacion.put("mensaje", mensaje);
				estatusOperacion.put("numeroPoliza", polizaDTO
						.getNumeroPolizaFormateada());
			}

		} catch (RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("emitirEndoso rehabilitacion fallo",
					Level.SEVERE, re);
			mensaje = "Ocurrio un error al renovar la poliza: "
					+ polizaDTO.getNumeroPolizaFormateada();
			estatusOperacion.put("icono", "10");// Exito
			estatusOperacion.put("mensaje", mensaje);
			estatusOperacion.put("numeroPoliza", polizaDTO != null ? polizaDTO
					.getNumeroPolizaFormateada() : "N/D");
			return estatusOperacion;
		}
		return estatusOperacion;
	}

	private void insertaAsignacionSeguimientoPoliza(PolizaDTO polizaDTO,
			String usuarioAsigacion, String usuarioCreacion,
			CotizacionDTO cotizacionDTO) {
		RenovacionPolizaDTO renovacionPolizaDTO = new RenovacionPolizaDTO();
		renovacionPolizaDTO.setCodigoUsuarioAsignacion(usuarioAsigacion);
		renovacionPolizaDTO.setCodigoUsuarioCreacion(usuarioCreacion);
		renovacionPolizaDTO.setFechaCreacion(new Date());
		renovacionPolizaDTO.setPolizaDTO(polizaDTO);
		renovacionPolizaDTO.setCotizacionDTO(cotizacionDTO);

		this.save(renovacionPolizaDTO);
		String mensajeAsignacion = "Se asign� correctamente la renovacion de la p�liza: "
				+ polizaDTO.getNumeroPolizaFormateada()
				+ " al usuario: "
				+ usuarioAsigacion;

		SeguimientoRenovacionDTO seguimientoRenovacionDTO = new SeguimientoRenovacionDTO();

		seguimientoRenovacionDTO.setFechaMovimiento(renovacionPolizaDTO
				.getFechaCreacion());
		seguimientoRenovacionDTO
				.setTipoMovimiento(SistemaPersistencia.RENOVACION_MOVIMIENTO_ASIGNACION_SUSCRIPTOR);
		seguimientoRenovacionDTO.setDescripcionMovimiento(mensajeAsignacion);
		seguimientoRenovacionDTO.setRenovacionPolizaDTO(renovacionPolizaDTO);

		renovacionPolizaDTO.getSeguimientoRenovacion().add(
				seguimientoRenovacionDTO);
		entityManager.merge(renovacionPolizaDTO);
	}

	protected void copiaRelacionesCotizacion(CotizacionDTO cotizacionOrigen,
			CotizacionDTO cotizacionDestino) {

		String queryString = copiarDocumentosDigitales(cotizacionDestino
				.getIdToCotizacion(), cotizacionOrigen.getIdToCotizacion());
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los documentos digitales", Level.INFO,
				null);

		queryString = copiarDocumentosCotizacion(cotizacionDestino
				.getIdToCotizacion(), cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log(
				"se copiaron los documentos anexos de la cotizacion",
				Level.INFO, null);

		queryString = copiarDocumentosReaseguro(cotizacionDestino
				.getIdToCotizacion(), cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los documentos anexos de reaseguro",
				Level.INFO, null);

		queryString = copiarTextosAdicionales(cotizacionDestino
				.getIdToCotizacion(), cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los textos  adicionales", Level.INFO,
				null);

		queryString = copiarIncisos(cotizacionDestino.getIdToCotizacion(),
				cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los incisos", Level.INFO, null);

		queryString = copiarSecciones(cotizacionDestino.getIdToCotizacion(),
				cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron las secciones", Level.INFO, null);

		queryString = copiarCoberturas(cotizacionDestino.getIdToCotizacion(),
				cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron las coberturas", Level.INFO, null);

		queryString = copiarRiesgos(cotizacionDestino.getIdToCotizacion(),
				cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los riesgos", Level.INFO, null);

		queryString = copiarAgrupaciones(cotizacionDestino.getIdToCotizacion(),
				cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron las agrupaciones", Level.INFO, null);

		queryString = copiarsubIncisos(cotizacionDestino.getIdToCotizacion(),
				cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los subincisos", Level.INFO, null);

		queryString = copiarDetallePrimaCobertura(cotizacionDestino
				.getIdToCotizacion(), cotizacionOrigen.getIdToCotizacion(),
				null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los detalles de prima cobertura",
				Level.INFO, null);

		queryString = copiarDetallePrimaRiesgo(cotizacionDestino
				.getIdToCotizacion(), cotizacionOrigen.getIdToCotizacion(),
				null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los detalles de prima riesgo",
				Level.INFO, null);

		queryString = copiarComisiones(cotizacionDestino.getIdToCotizacion(),
				cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron las comisiones", Level.INFO, null);

		queryString = copiarDatosIncisos(cotizacionDestino.getIdToCotizacion(),
				cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los datos de inciso", Level.INFO, null);

		queryString = copiarAumentos(cotizacionDestino.getIdToCotizacion(),
				cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los aumentos", Level.INFO, null);

		queryString = copiarRecargos(cotizacionDestino.getIdToCotizacion(),
				cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los recargos", Level.INFO, null);

		queryString = copiarDescuentos(cotizacionDestino.getIdToCotizacion(),
				cotizacionOrigen.getIdToCotizacion(), null);
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los descuentos", Level.INFO, null);
	}

	@SuppressWarnings("unchecked")
	public List<PolizaDTO> buscarFiltrado(PolizaDTO polizaDTO,
			boolean esRenovacion, boolean esReporte) {
		try {
			String queryString = "select model from PolizaDTO model ";
			String sWhere = " model.cotizacionDTO.claveEstatus = 16 ";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (polizaDTO == null)
				return null;

			if (polizaDTO.getCodigoProducto() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
						sWhere, "codigoProducto", String.format("%1$-8s",
								polizaDTO.getCodigoProducto()));
			if (polizaDTO.getCodigoTipoPoliza() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
						sWhere, "codigoTipoPoliza", String.format("%1$-8s",
								polizaDTO.getCodigoTipoPoliza()));
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "numeroPoliza", polizaDTO.getNumeroPoliza());
			sWhere = Utilerias
					.agregaParametroQuery(listaParametrosValidos, sWhere,
							"numeroRenovacion", polizaDTO.getNumeroRenovacion());
			if (polizaDTO.getCodigoProductoAsoc() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
						sWhere, "codigoProductoAsoc", String.format("%1$-8s",
								polizaDTO.getCodigoProductoAsoc()));
			if (polizaDTO.getCodigoTipoPolizaAsoc() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
						sWhere, "codigoTipoPolizaAsoc", String.format("%1$-8s",
								polizaDTO.getCodigoTipoPolizaAsoc()));
			sWhere = Utilerias
					.agregaParametroQuery(listaParametrosValidos, sWhere,
							"numeroPolizaAsoc", polizaDTO.getNumeroPolizaAsoc());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "numeroRenovacionAsoc", polizaDTO
							.getNumeroRenovacionAsoc());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "idToPoliza", polizaDTO.getIdToPoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveEstatus", polizaDTO.getClaveEstatus());

			
			if (polizaDTO.getFechaCreacion() != null) {
				sWhere += " and model.fechaCreacion BETWEEN :start AND :end ";
			}
			sWhere += " and model.codigoTipoPoliza not in ( select tipoPoliza.codigo from TipoPolizaDTO tipoPoliza where tipoPoliza.claveRenovable = 0) ";
			sWhere += " and model.codigoProducto not in ( select producto.codigo from ProductoDTO producto where producto.claveRenovable = 0) ";
			
			if (polizaDTO.getCotizacionDTO() != null) {
				if (polizaDTO.getCotizacionDTO().getIdToCotizacion() != null)
					sWhere = Utilerias.agregaParametroQuery(
							listaParametrosValidos, sWhere,
							"cotizacionDTO.idToCotizacion", polizaDTO
									.getCotizacionDTO().getIdToCotizacion());

				if (polizaDTO.getCotizacionDTO().getNombreAsegurado() != null && !StringUtils.isBlank(polizaDTO.getCotizacionDTO().getNombreAsegurado()))
					sWhere += " and UPPER(model.cotizacionDTO.nombreAsegurado) like '%"
							+ polizaDTO.getCotizacionDTO().getNombreAsegurado().trim()
									.toUpperCase() +"%'";
				if (!esRenovacion) {
					if(!esReporte)
						sWhere += " and model.idToPoliza not in (Select seguimiento.renovacionPolizaDTO.polizaDTO.idToPoliza from SeguimientoRenovacionDTO seguimiento ) ";
					if (polizaDTO.getCotizacionDTO().getFechaInicioVigencia() != null
							&& polizaDTO.getCotizacionDTO()
									.getFechaFinVigencia() != null) {
						sWhere += " and model.cotizacionDTO.fechaFinVigencia BETWEEN :desde AND :hasta order by model.cotizacionDTO.fechaFinVigencia, model.numeroPoliza";
					}
				} else {
					if (polizaDTO.getCotizacionDTO().getFechaInicioVigencia() != null
							&& polizaDTO.getCotizacionDTO()
									.getFechaFinVigencia() != null) {
						sWhere += " and model.idToPoliza in (Select seguimiento.renovacionPolizaDTO.polizaDTO.idToPoliza from SeguimientoRenovacionDTO seguimiento where seguimiento.tipoMovimiento = 10 and seguimiento.fechaMovimiento BETWEEN :desde AND :hasta) ";
					} else {
						sWhere += " and model.idToPoliza in (Select seguimiento.renovacionPolizaDTO.polizaDTO.idToPoliza from SeguimientoRenovacionDTO seguimiento ) ";
					}

				}
			}

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			sWhere = sWhere.concat(" order by model.cotizacionDTO.fechaFinVigencia, model.numeroPoliza ");		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);

			if (polizaDTO.getFechaCreacion() != null) {

				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(polizaDTO.getFechaCreacion());
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);

				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(gcFechaInicio.getTime());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);

				System.out.println("Poliza: Rango de Fecha inicio ->"
						+ gcFechaInicio.getTime());
				System.out.println("Poliza: Rango de Fecha fin ->"
						+ gcFechaFin.getTime());

				query.setParameter("start", gcFechaInicio.getTime(),
						TemporalType.TIMESTAMP);
				query.setParameter("end", gcFechaFin.getTime(),
						TemporalType.TIMESTAMP);
			}
			if (polizaDTO.getCotizacionDTO() != null
					&& polizaDTO.getCotizacionDTO().getFechaInicioVigencia() != null
					&& polizaDTO.getCotizacionDTO().getFechaFinVigencia() != null) {

				System.out
						.println("Poliza: Fecha Fin Vigencia desde  ->"
								+ polizaDTO.getCotizacionDTO()
										.getFechaInicioVigencia());
				System.out.println("Poliza: Fecha Fin Vigencia hasta ->"
						+ polizaDTO.getCotizacionDTO().getFechaFinVigencia());

				query.setParameter("desde", polizaDTO.getCotizacionDTO()
						.getFechaInicioVigencia(), TemporalType.TIMESTAMP);
				query.setParameter("hasta", polizaDTO.getCotizacionDTO()
						.getFechaFinVigencia(), TemporalType.TIMESTAMP);
			}

			if (polizaDTO.getPrimerRegistroACargar() != null
					&& polizaDTO.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(polizaDTO.getPrimerRegistroACargar()
						.intValue());
				query.setMaxResults(polizaDTO.getNumeroMaximoRegistrosACargar()
						.intValue());
			}

			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<PolizaDTO> polizas = (List<PolizaDTO>) query.getResultList();
			if (polizas != null && polizas.size()>0 && esReporte){
				Double montoReclamado = 0D;
				SeguimientoRenovacionDTO seguimientoRenovacionDTO = null;
				for(PolizaDTO poliza: polizas){
					try{
						montoReclamado = indemnizacionFacadeRemote.getMontoReclamado(poliza.getIdToPoliza());
						if(montoReclamado > 0){
							List<ReporteSiniestroDTO> reportes = reporteSiniestroFacadeRemote.listarFiltradoPorNumPoliza(poliza.getIdToPoliza().toString());
							poliza.setNumeroSiniestro(obtenerReportesString(reportes));
						}
						seguimientoRenovacionDTO = this.getUltimoDetalleRenovacionPoliza(poliza.getIdToPoliza());
						if(seguimientoRenovacionDTO != null){
							poliza.setStatusRenovacion(seguimientoRenovacionDTO.getDescripcionTipoMovimiento());
						}else{
							poliza.setStatusRenovacion("Sin Renovaci�n");
						}
					}catch (Exception e){}
					poliza.setMontoReclamado(montoReclamado);
				}
			}
			
			return polizas;

		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find in PolizaDTO failed", Level.SEVERE, re);
			throw re;
		}
	}

	public String obtenerReportesString(List<ReporteSiniestroDTO> reportes) {
		StringBuilder reportesString = new StringBuilder("| ");
		for(ReporteSiniestroDTO reporte: reportes){
			reportesString.append(reporte.getNumeroReporte()).append(" | ");
		}	return reportesString.toString();
	}

	public RenovacionPolizaDTO buscarDetalleRenovacionPoliza(
			BigDecimal idToPoliza) {
		LogUtil.log("buscando Detalle de Seguimiento de Renovacion de Polizas",
				Level.INFO, null);
		RenovacionPolizaDTO instance = null;
		try {
			List<RenovacionPolizaDTO> instances = this.findByProperty(
					"polizaDTO.idToPoliza", idToPoliza);
			if (instances != null && instances.size() > 0) {
				instance = instances.get(0);
				LogUtil.log("Detalles asociados a la Renovacion buscada: "
						+ instance.getSeguimientoRenovacion().size(),
						Level.INFO, null);
			}
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RenovacionPolizaDTO> buscarFiltrado(
			RenovacionPolizaDTO renovacionPolizaDTO, boolean esNotificacion){
		try {
			String queryString = "select model from RenovacionPolizaDTO model  ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (renovacionPolizaDTO == null)
				return null;

			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigoUsuarioCreacion", renovacionPolizaDTO.getCodigoUsuarioCreacion());
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigoUsuarioAsignacion", renovacionPolizaDTO.getCodigoUsuarioAsignacion());

			if(renovacionPolizaDTO.getPolizaDTO() != null){
				if (renovacionPolizaDTO.getPolizaDTO().getCodigoProducto() != null)
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
							sWhere, "polizaDTO.codigoProducto", String.format("%1$-8s",
									renovacionPolizaDTO.getPolizaDTO().getCodigoProducto()));
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
						sWhere, "polizaDTO.numeroPoliza", renovacionPolizaDTO.getPolizaDTO().getNumeroPoliza());			
			}
			
			if (renovacionPolizaDTO.getCotizacionDTO() != null && renovacionPolizaDTO.getCotizacionDTO().getNombreAsegurado() != null && 
					!StringUtils.isBlank(renovacionPolizaDTO.getCotizacionDTO().getNombreAsegurado()))
				sWhere += " UPPER(model.polizaDTO.cotizacionDTO.nombreAsegurado) like '%"
						+ renovacionPolizaDTO.getCotizacionDTO().getNombreAsegurado().trim()
								.toUpperCase()
						+ "%' ";			
			
			if(renovacionPolizaDTO.getCotizacionDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
						sWhere, "cotizacionDTO.idToCotizacion", renovacionPolizaDTO.getCotizacionDTO().getIdToCotizacion());					
			}

			
			if (renovacionPolizaDTO.getCotizacionDTO() != null
					&& renovacionPolizaDTO.getCotizacionDTO().getFechaInicioVigencia() != null
					&& renovacionPolizaDTO.getCotizacionDTO().getFechaFinVigencia() != null) {
				if (Utilerias.esAtributoQueryValido(sWhere)) {
			        sWhere = sWhere.concat(" and ");
			     }
				sWhere += " model.polizaDTO.cotizacionDTO.fechaFinVigencia BETWEEN :desde AND :hasta ";

			}			
			
			if(renovacionPolizaDTO.getFechaCreacionDesde() != null &&
					renovacionPolizaDTO.getFechaCreacionHasta() != null){
				if (Utilerias.esAtributoQueryValido(sWhere)) {
			        sWhere = sWhere.concat(" and ");
			     }				
				sWhere += " model.fechaCreacion BETWEEN :inicio AND :fin ";
			}
			if(esNotificacion){
				if (Utilerias.esAtributoQueryValido(sWhere)) {
			        sWhere = sWhere.concat(" and ");
			     }
				sWhere += " model.idToRenovacionPoliza not in ( ";
				sWhere += " Select seguimiento.renovacionPolizaDTO.idToRenovacionPoliza ";
				sWhere += " from SeguimientoRenovacionDTO seguimiento  where seguimiento.tipoMovimiento = 50) ";				
			}

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			sWhere = sWhere.concat(" order by model.polizaDTO.cotizacionDTO.fechaFinVigencia, model.polizaDTO.numeroPoliza ");
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);		

			if (renovacionPolizaDTO.getCotizacionDTO() != null &&
					renovacionPolizaDTO.getCotizacionDTO().getFechaInicioVigencia() != null &&
					renovacionPolizaDTO.getCotizacionDTO().getFechaFinVigencia() != null) {
				
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(renovacionPolizaDTO.getCotizacionDTO().getFechaInicioVigencia());
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
				
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(renovacionPolizaDTO.getCotizacionDTO().getFechaFinVigencia());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				
				System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
				System.out.println("Poliza: Rango de Fecha fin ->" + gcFechaFin.getTime());
				
				query.setParameter("desde", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
				query.setParameter("hasta", gcFechaFin.getTime(), TemporalType.TIMESTAMP);
			}
			if (renovacionPolizaDTO.getFechaCreacionDesde() != null &&
					renovacionPolizaDTO.getFechaCreacionHasta()!= null) {
				
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(renovacionPolizaDTO.getFechaCreacionDesde());
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
				
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(renovacionPolizaDTO.getFechaCreacionHasta());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				
				System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
				System.out.println("Poliza: Rango de Fecha fin ->" + gcFechaFin.getTime());
				
				query.setParameter("inicio", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
				query.setParameter("fin", gcFechaFin.getTime(), TemporalType.TIMESTAMP);
			}						
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find in buscarFiltrado failed", Level.SEVERE, re);
			throw re;
		}				
	}

	public Integer getSiguienteNumeroNotificacion(BigDecimal idToPoliza) {
		LogDeMidasEJB3.log("siguiente notificacion", Level.INFO, null);
		try {
			final String queryString = "select count(model.tipoMovimiento) from SeguimientoRenovacionDTO model where model.renovacionPolizaDTO.polizaDTO.idToPoliza = :idToPoliza and model.tipoMovimiento = 30";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToPoliza", idToPoliza);			
			Long numNotificacion = (Long) query.getSingleResult();
			if (numNotificacion == null)
				numNotificacion = 1l;
			else{
				numNotificacion=numNotificacion+1l;
			}
			return numNotificacion.intValue();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("count failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public SeguimientoRenovacionDTO getUltimoDetalleRenovacionPoliza(
			BigDecimal idToPoliza) {
		LogUtil.log("getUltimoDetalleRenovacionPoliza", Level.INFO, null);
		try {
			final String queryString = "select model from SeguimientoRenovacionDTO model "
					+ "where model.renovacionPolizaDTO.polizaDTO.idToPoliza= :idToPoliza and model.idToTeguimientoRenovacion = ("
					+ "select max(detalle.idToTeguimientoRenovacion) from SeguimientoRenovacionDTO detalle "
					+ "where detalle.renovacionPolizaDTO.polizaDTO.idToPoliza= :idToPoliza)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToPoliza", idToPoliza);
			List result = query.getResultList();
			if (!result.isEmpty()) {
				return (SeguimientoRenovacionDTO) result.get(0);
			} else {
				return null;
			}
		} catch (RuntimeException re) {
			LogUtil.log("getUltimoEndoso failed", Level.SEVERE, re);
			throw re;
		}
	}
}
