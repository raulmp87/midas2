package mx.com.afirme.midas.poliza;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVAFacadeRemote;
import mx.com.afirme.midas.consultas.gastoexpedicion.GastoExpedicionDTO;
import mx.com.afirme.midas.consultas.gastoexpedicion.GastoExpedicionFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionId;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.endoso.agrupacion.AgrupacionEndosoFacadeRemote;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoFacadeRemote;
import mx.com.afirme.midas.endoso.cobertura.subinciso.SubIncisoCoberturaEndosoFacadeRemote;
import mx.com.afirme.midas.endoso.riesgo.RiesgoEndosoFacadeRemote;
import mx.com.afirme.midas.endoso.riesgo.subinciso.SubIncisoRiesgoEndosoFacadeRemote;
import mx.com.afirme.midas.endoso.subinciso.SubIncisoEndosoFacadeRemote;
import mx.com.afirme.midas.poliza.agrupacion.AgrupacionPolizaFacadeRemote;
import mx.com.afirme.midas.poliza.cobertura.CoberturaPolizaDTO;
import mx.com.afirme.midas.poliza.cobertura.CoberturaPolizaFacadeRemote;
import mx.com.afirme.midas.poliza.cobertura.subinciso.SubIncisoCoberturaPolizaFacadeRemote;
import mx.com.afirme.midas.poliza.folio.FolioPolizaDTO;
import mx.com.afirme.midas.poliza.folio.FolioPolizaFacadeRemote;
import mx.com.afirme.midas.poliza.folio.FolioPolizaId;
import mx.com.afirme.midas.poliza.inciso.IncisoPolizaFacadeRemote;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaFacadeRemote;
import mx.com.afirme.midas.poliza.riesgo.RiesgoPolizaDTO;
import mx.com.afirme.midas.poliza.riesgo.RiesgoPolizaFacadeRemote;
import mx.com.afirme.midas.poliza.riesgo.RiesgoPolizaId;
import mx.com.afirme.midas.poliza.riesgo.subinciso.SubIncisoRiesgoPolizaFacadeRemote;
import mx.com.afirme.midas.poliza.seccion.SeccionPolizaFacadeRemote;
import mx.com.afirme.midas.poliza.subinciso.SubIncisoPolizaFacadeRemote;
import mx.com.afirme.midas.sistema.Utilerias;

public class PolizaSoporteEmision {

	@EJB
	PolizaFacadeRemote poliza;
	@EJB
	CotizacionFacadeRemote cotizacion;
	@EJB
	FolioPolizaFacadeRemote folioPoliza;
	@EJB
	IncisoPolizaFacadeRemote incisoPoliza;
	@EJB
	SeccionPolizaFacadeRemote seccionPoliza;
	@EJB
	CoberturaCotizacionFacadeRemote coberturaCotizacion;
	@EJB
	ParametroGeneralFacadeRemote parametroGeneral;
	@EJB
	AgrupacionCotFacadeRemote agrupacionCot;
	@EJB
	ComisionCotizacionFacadeRemote comisionCotizacion;
	@EJB
	CoberturaPolizaFacadeRemote coberturaPoliza;
	@EJB
	RiesgoCotizacionFacadeRemote riesgoCotizacion;
	@EJB
	RiesgoPolizaFacadeRemote riesgoPoliza;
	@EJB
	SubIncisoPolizaFacadeRemote subIncisoPoliza;
	@EJB
	SubIncisoCoberturaPolizaFacadeRemote subIncisoCoberturaPoliza;
	@EJB
	SubIncisoRiesgoPolizaFacadeRemote subIncisoRiesgoPoliza; 
	@EJB
	AgrupacionPolizaFacadeRemote agrupacionPoliza;
	@EJB
	EndosoFacadeRemote endoso;
	@EJB
	CoberturaEndosoFacadeRemote coberturaEndoso;
	@EJB
	RiesgoEndosoFacadeRemote riesgoEndoso;
	@EJB
	SubIncisoEndosoFacadeRemote subIncisoEndoso;
	@EJB
	SubIncisoCoberturaEndosoFacadeRemote subIncisoCoberturaEndoso;
	@EJB
	SubIncisoRiesgoEndosoFacadeRemote subIncisoRiesgoEndoso;
	@EJB
	AgrupacionEndosoFacadeRemote agrupacionEndoso;
	@EJB
	GastoExpedicionFacadeRemote gastoExpedicion; 
	@EJB
	CodigoPostalIVAFacadeRemote codigoPostalIVAFacadeRemote; 
	@EJB
	RenovacionPolizaFacadeRemote renovacionPolizaFacadeRemote;
	
	// Constantes para Numero de Renovacion en una poliza
	private static final int POLIZA_NUEVA = 0;
	// Constantes de Estatus de poliza;
	private static final short ESTATUS_VIGENTE = 1;
	// Constantes para tipos de comision
	private static final short TIPO_RO = 1;
	private static final short TIPO_RCI = 2;
	private static final short TIPO_PRR = 3;
	// Constantes para tipos de agrupoacion
	private static final short TIPO_PRIMER_RIESGO = 1;

	private double valorPrimaNetaEndoso;
	private double valorRecargoPagoFraccionadoEndoso;
	private double valorDerechosEndoso;
	private double valorBonificacionComisionEndoso;
	private double valorIVAEndoso;
	private double valorPrimaTotalEndoso;
	private double valorBonificacionComisionRPFEndoso;
	private double valorComisionEndoso;
	private double valorComisionRPFEndoso;

	//Se encarga de generar y pasar toda la estructura de una cotizacion a una poliza
	protected void poblarPolizaDTO(BigDecimal idToCotizacion,
			PolizaDTO polizaDTO) {
		CotizacionDTO cotizacionDTO = cotizacion.findById(idToCotizacion);

		polizaDTO.setCotizacionDTO(cotizacionDTO);
		polizaDTO.setIdToSolicitud(cotizacionDTO.getSolicitudDTO()
				.getIdToSolicitud());
		polizaDTO.setCodigoProducto(cotizacionDTO.getSolicitudDTO()
				.getProductoDTO().getCodigo());
		polizaDTO.setCodigoTipoPoliza(cotizacionDTO.getTipoPolizaDTO()
				.getCodigo());

		polizaDTO
				.setNumeroPoliza(folioPoliza.getSiguienteNumeroPoliza(polizaDTO
						.getCodigoProducto(), polizaDTO.getCodigoTipoPoliza()));
		polizaDTO.setNumeroRenovacion(POLIZA_NUEVA);
		
		FolioPolizaId id = new FolioPolizaId();
		id.setCodigoProducto(polizaDTO.getCodigoProducto());
		id.setCodigoTipoPoliza(polizaDTO.getCodigoTipoPoliza());
		id.setNumeroPoliza(polizaDTO.getNumeroPoliza());

		FolioPolizaDTO folioPolizaDTO = new FolioPolizaDTO();
		folioPolizaDTO.setId(id);

		folioPoliza.save(folioPolizaDTO);

		if (polizaDTO.getNumeroPolizaAsoc() != null
				&& polizaDTO.getNumeroPolizaAsoc().intValue() > 0) {
			PolizaDTO polizaAsociadaDTO = new PolizaDTO();
			polizaAsociadaDTO.setCodigoProductoAsoc(polizaDTO.getCodigoProductoAsoc());
			polizaAsociadaDTO.setCodigoTipoPolizaAsoc(polizaDTO.getCodigoTipoPolizaAsoc());
			polizaAsociadaDTO.setNumeroPoliza(polizaDTO.getNumeroPolizaAsoc());
			polizaAsociadaDTO.setNumeroRenovacion(polizaDTO.getNumeroRenovacionAsoc());
			List<PolizaDTO> polizas = poliza.buscarFiltrado(polizaAsociadaDTO);
			if(polizas.size() > 0){
				polizaAsociadaDTO = polizas.get(0);
				
				polizaDTO.setCodigoProductoAsoc(polizaAsociadaDTO
						.getCodigoProducto());
				polizaDTO.setCodigoTipoPolizaAsoc(polizaAsociadaDTO
						.getCodigoProducto());
				polizaDTO.setNumeroPolizaAsoc(polizaAsociadaDTO.getNumeroPoliza());
				polizaDTO.setNumeroRenovacionAsoc(polizaAsociadaDTO
						.getNumeroRenovacion());				
			}
		} else {
			polizaDTO.setCodigoProductoAsoc("0");
			polizaDTO.setCodigoTipoPolizaAsoc("0");
			polizaDTO.setNumeroPolizaAsoc(POLIZA_NUEVA);
			polizaDTO.setNumeroRenovacionAsoc(POLIZA_NUEVA);
		}
		polizaDTO.setPorcentajeBonifComision(cotizacionDTO
				.getPorcentajebonifcomision());
		polizaDTO.setClaveEstatus(ESTATUS_VIGENTE);
		polizaDTO.setFechaCreacion(new Date());
		polizaDTO.setFechaModificacion(new Date());
		
	}


	protected void emitir(BigDecimal idToCotizacion,PolizaDTO polizaDTO,Double ivaCotizacion){
		incisoPoliza.insertIncisosPorCotizacion(idToCotizacion, polizaDTO.getIdToPoliza());
		agrupacionPoliza.insertAgrupacionesPorCotizacion(idToCotizacion, polizaDTO.getIdToPoliza());
		seccionPoliza.insertSeccionesPorCotizacion(idToCotizacion, polizaDTO.getIdToPoliza());
		subIncisoPoliza.insertSubIncisoPolizaPorCotizacion(idToCotizacion, polizaDTO.getIdToPoliza());
		coberturaPoliza.insertCoberturaPolizaPorCotizacion(idToCotizacion, polizaDTO.getIdToPoliza());
		subIncisoCoberturaPoliza.insertSubIncisoCoberturaPolizaPorCotizacion(idToCotizacion, polizaDTO.getIdToPoliza());
		riesgoPoliza.insertRiesgoPolizaPorCotizacion(idToCotizacion, polizaDTO.getIdToPoliza());
		subIncisoRiesgoPoliza.insertSubIncisoRiesgoPolizaPorCotizacion(idToCotizacion, polizaDTO.getIdToPoliza());
		//calcularCoberturas(idToCotizacion, polizaDTO);
		calcularRiesgos(idToCotizacion, polizaDTO,ivaCotizacion);
		acumulaCoberturas(idToCotizacion, polizaDTO);

		//Dias por devengar
		double diasPorDevengar = Utilerias.obtenerDiasEntreFechas(
				polizaDTO.getCotizacionDTO().getFechaInicioVigencia(),
				polizaDTO.getCotizacionDTO().getFechaFinVigencia());

		CotizacionDTO cotizacionDTO = cotizacion.findById(idToCotizacion);
		if(cotizacionDTO.getTipoPolizaDTO().getProductoDTO() != null && 
				cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getClaveAjusteVigencia().intValue() == 0){
			diasPorDevengar = 365d;
		}	
		
		endoso.insertEndosoDePoliza(polizaDTO.getIdToPoliza(),0,(short)2, diasPorDevengar/365);
		agrupacionEndoso.insertAgrupacionEndosoDePoliza(idToCotizacion, polizaDTO.getIdToPoliza(),0, diasPorDevengar,(short) 2);
		subIncisoEndoso.insertSubIncisoEndosoDePoliza(idToCotizacion, polizaDTO.getIdToPoliza(),0, diasPorDevengar,(short) 2);
		coberturaEndoso.insertCoberturaEndosoDePoliza(idToCotizacion, polizaDTO.getIdToPoliza(),0,(short) 2, new BigDecimal(0D));
		subIncisoCoberturaEndoso.insertSubIncisoCoberturaEndosoDePoliza(idToCotizacion, polizaDTO.getIdToPoliza(),0, diasPorDevengar,(short) 2);
		riesgoEndoso.insertRiesgoEndosoDePoliza(idToCotizacion, polizaDTO.getIdToPoliza(),0,(short) 2, new BigDecimal(0D));
		subIncisoRiesgoEndoso.insertSubIncisoRiesgoEndosoDeCotizacion(idToCotizacion, polizaDTO.getIdToPoliza(),0, diasPorDevengar,(short) 2);
		
		EndosoDTO ultimoEndoso = endoso.getUltimoEndoso(polizaDTO.getIdToPoliza());
		acumulaEndoso(ultimoEndoso);
		
	}
	protected void acumulaEndoso(EndosoDTO ultimoEndoso){
		ultimoEndoso.setValorPrimaNeta(valorPrimaNetaEndoso);
		ultimoEndoso.setValorRecargoPagoFrac(valorRecargoPagoFraccionadoEndoso);
		ultimoEndoso.setValorDerechos(valorDerechosEndoso);
		ultimoEndoso.setValorBonifComision(valorBonificacionComisionEndoso);
		ultimoEndoso.setValorIVA(valorIVAEndoso);
		ultimoEndoso.setValorPrimaTotal(valorPrimaTotalEndoso);
		ultimoEndoso.setValorBonifComisionRPF(valorBonificacionComisionRPFEndoso);
		ultimoEndoso.setValorComision(valorComisionEndoso);
		ultimoEndoso.setValorComisionRPF(valorComisionRPFEndoso);
		endoso.update(ultimoEndoso);
	}	
	public void acumulaCoberturas(BigDecimal idToCotizacion,PolizaDTO polizaDTO){
		List<CoberturaPolizaDTO> coberturas = coberturaPoliza.findByProperty("id.idToPoliza", polizaDTO.getIdToPoliza());
		
		double valorRPF;
		double valorDerechos;
		double valorBonificacionComis;
		double valorBonificacionComisRFP;
		double valorIVA;
		double valorPrimaNetaTotal;
		double valorPrimaNeta;
		double porcentajeComision;
		double valorComision;
		double valorComisionFinal;
		double valorComisionRPF;
		double valorComisionFinalRPF;	
		
		for(CoberturaPolizaDTO cobertura: coberturas){
			valorRPF = 0D;
			valorDerechos = 0D;
			valorBonificacionComis = 0D;	
			valorBonificacionComisRFP = 0D;
			valorIVA = 0D;
			valorPrimaNetaTotal = 0d;
			valorPrimaNeta = 0d;
			porcentajeComision = 0D;
			valorComision = 0D;
			valorComisionFinal = 0D;
			valorComisionRPF = 0D;
			valorComisionFinalRPF = 0D;
			
			CoberturaCotizacionId id = new CoberturaCotizacionId();
			id.setIdToCotizacion(idToCotizacion);
			id.setIdToCobertura(cobertura.getId().getIdToCobertura());
			id.setIdToSeccion(cobertura.getId().getIdToSeccion());
			id.setNumeroInciso(cobertura.getId().getNumeroInciso());
			
			CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
			coberturaCotizacionDTO.setId(id);
			
			coberturaCotizacionDTO = coberturaCotizacion.findById(id);
			
			RiesgoPolizaId id2  = new RiesgoPolizaId();
			id2.setIdToPoliza(cobertura.getId().getIdToPoliza());
			id2.setNumeroInciso(cobertura.getId().getNumeroInciso());
			id2.setIdToSeccion(cobertura.getId().getIdToSeccion());
			id2.setIdToCobertura(cobertura.getId().getIdToCobertura());
			
			List<RiesgoPolizaDTO> riesgos = riesgoPoliza.listarFiltrado(id2);
			
			for(RiesgoPolizaDTO riesgo : riesgos){
				valorRPF += riesgo.getValorRecargoPagoFrac();
				valorDerechos += riesgo.getValorDerechos();
				valorBonificacionComis += riesgo.getValorBonifComision();
				valorBonificacionComisRFP += riesgo.getValorBonifComRecPagoFrac();
				valorIVA += riesgo.getValorIVA();
				valorPrimaNetaTotal += riesgo.getValorPrimaTotal();
				porcentajeComision = riesgo.getPorcentajeComision();
				valorComision += riesgo.getValorComision();
				valorComisionFinal += riesgo.getValorComisionFinal();
				valorComisionRPF += riesgo.getValorComisionRecPagoFrac();
				valorComisionFinalRPF += riesgo.getValorComFinalRecPagoFrac();
				valorPrimaNeta += riesgo.getValorPrimaNeta();
			}
			// valorCoaseguro = valorPrimaNeta * porcentajeCoaseguro /100	
			cobertura.setValorCoaseguro(cobertura.getValorPrimaNeta() * coberturaCotizacionDTO.getPorcentajeCoaseguro() / 100D);
			// valorDeducible = valorPrimaNeta * porcentajeDeducible /100
			cobertura.setValorDeducible(cobertura.getValorPrimaNeta() * coberturaCotizacionDTO.getPorcentajeDeducible() / 100D);
			cobertura.setValorRecargoPagoFrac(valorRPF);
			cobertura.setValorBonifComision(valorBonificacionComis);
			cobertura.setValorBonifComRecPagoFrac(valorBonificacionComisRFP);
			cobertura.setValorDerechos(valorDerechos);
			cobertura.setValorIVA(valorIVA);
			cobertura.setValorPrimaTotal(valorPrimaNetaTotal);
			cobertura.setPorcentajeComision(porcentajeComision);
			cobertura.setValorComision(valorComision);
			cobertura.setValorComisionFinal(valorComisionFinal);
			cobertura.setValorComisionRecPagoFrac(valorComisionRPF);
			cobertura.setValorComFinalRecPagoFrac(valorComisionFinalRPF);
			cobertura.setValorPrimaNeta(valorPrimaNeta);
			
			coberturaPoliza.update(cobertura);
		}
	}
	public void calcularCoberturas(BigDecimal idToCotizacion, PolizaDTO polizaDTO,Double ivaCotizacion){
		int INCENDIO = 1;
		Double primaNetaCotizacion = cotizacion.getPrimaNetaCotizacion(idToCotizacion);
		Double derechos = 0D;
		CotizacionDTO cotizacionDTO = cotizacion.findById(idToCotizacion);
		//Se obtienen los gastos de expedicion
		GastoExpedicionDTO gasto = gastoExpedicion.getGastoExpedicion(cotizacionDTO
				.getIdMoneda(), BigDecimal.valueOf(primaNetaCotizacion));	
		if(gasto != null)
			derechos = gasto.getGastoExpedicionPoliza();
		
		List<ParametroGeneralDTO> parametrosLimiteSA = parametroGeneral
		.findByProperty("id.codigoParametroGeneral",
				BigDecimal.valueOf(30010D));
		
		AgrupacionCotDTO agrupacion = agrupacionCot
		.buscarPorCotizacion(idToCotizacion,
				TIPO_PRIMER_RIESGO);		
		
		Double limiteSA = Double.valueOf(parametrosLimiteSA.get(0).getValor());
		Double tipoCambioVal = polizaDTO.getTipoCambio();		
		
		List<CoberturaPolizaDTO> coberturas = coberturaPoliza.findByProperty("id.idToPoliza", polizaDTO.getIdToPoliza());
		if(ivaCotizacion==null){
		    throw new RuntimeException("No se pudo obtener el valor del IVA.");
		}
		
		for(CoberturaPolizaDTO cobertura: coberturas){
			CoberturaCotizacionId id = new CoberturaCotizacionId();
			id.setIdToCotizacion(idToCotizacion);
			id.setIdToCobertura(cobertura.getId().getIdToCobertura());
			id.setIdToSeccion(cobertura.getId().getIdToSeccion());
			id.setNumeroInciso(cobertura.getId().getNumeroInciso());
			
			CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
			coberturaCotizacionDTO.setId(id);
			
			coberturaCotizacionDTO = coberturaCotizacion.findById(id);
		
			ComisionCotizacionId id3 = new ComisionCotizacionId();
			id3.setIdTcSubramo(coberturaCotizacionDTO.getIdTcSubramo());
			id3.setIdToCotizacion(idToCotizacion);

			ComisionCotizacionDTO comision = null;
			Double sumaAseguradaIncendio = 0D;

			if (coberturaCotizacionDTO.getIdTcSubramo().intValue()== INCENDIO) {
				limiteSA = limiteSA / tipoCambioVal;
				// Se obtiene la SA de Incendio por Inciso
				sumaAseguradaIncendio = coberturaCotizacion
						.obtenerSACoberturasBasicasIncendioPorCotizacion(
								idToCotizacion);	
				if (agrupacion != null) {
					if (cobertura.getNumeroAgrupacion() == agrupacion
							.getId().getNumeroAgrupacion()) {
						id3.setTipoPorcentajeComision(TIPO_PRR);
					}else{
						id3.setTipoPorcentajeComision(TIPO_RO);
					}
				} else {
					if (sumaAseguradaIncendio < limiteSA) {
						id3.setTipoPorcentajeComision(TIPO_RO);
					} else if (sumaAseguradaIncendio >= limiteSA) {
						id3.setTipoPorcentajeComision(TIPO_RCI);
					}else{
						id3.setTipoPorcentajeComision(TIPO_RO);	
					}
				}				
			} else {
				id3.setTipoPorcentajeComision(TIPO_RO);
			}
			comision = comisionCotizacion.findById(id3);

			cobertura.setPorcentajeComision(comision.getPorcentajeComisionCotizacion().doubleValue());
			// valorComision = primaNeta de la cobertura * porcentajeComision/100
			cobertura.setValorComision(cobertura.getValorPrimaNeta()
					* cobertura.getPorcentajeComision() / 100);
			// valorRecargoPagoFrac = valorPrimaNeta * PorcentajeRPF/100
			cobertura.setValorRecargoPagoFrac(coberturaCotizacionDTO
					.getValorPrimaNeta() * polizaDTO.getPctPagoFraccionado() / 100);				
			// valorComisionRecPagoFrac = valorRecargoPagoFrac * porcentajeComision/100
			cobertura.setValorComisionRecPagoFrac(cobertura.getValorRecargoPagoFrac()
							* cobertura.getPorcentajeComision() / 100);		
			// valorCoaseguro = valorPrimaNeta * porcentajeCoaseguro /100			
			cobertura.setValorCoaseguro(coberturaCotizacionDTO.getValorPrimaNeta()
					* coberturaCotizacionDTO.getPorcentajeCoaseguro() / 100);	
			// valorDeducible = valorPrimaNeta * porcentajeDeducible /100
			cobertura.setValorDeducible(coberturaCotizacionDTO.getValorPrimaNeta()
					* coberturaCotizacionDTO.getPorcentajeDeducible() / 100);	
			//derechos = derechos poliza * valorPrimaNetaCobertura / primaNetaCotizacion 
			cobertura.setValorDerechos(derechos * (coberturaCotizacionDTO.getValorPrimaNeta()/ primaNetaCotizacion));
			// valorBonifComision = ValorComision * porcentajeBonifComision/100
			cobertura.setValorBonifComision(cobertura.getValorComision()
					* cotizacionDTO.getPorcentajebonifcomision() / 100);
			// valorBonifComRecPagoFrac = ValorComisionRecPagoFrac * porcentajeBonifComision/100
			cobertura.setValorBonifComRecPagoFrac(cobertura.getValorComisionRecPagoFrac()
							* cotizacionDTO.getPorcentajebonifcomision() / 100);
			// valorComFinalRecPagoFrac = valorComisionRecPagoFrac - valorBonifComRecPagoFac
			cobertura.setValorComFinalRecPagoFrac(cobertura.getValorComisionRecPagoFrac()
							- cobertura.getValorBonifComRecPagoFrac());				
			// valorComisionFinal = valorComision - valorBonifComision
			cobertura.setValorComisionFinal(cobertura.getValorComision()
					- cobertura.getValorBonifComision());			
			
			// valorIVA = (primaNeta de la cobertura +
			// valorRecargoPagoFrac + valorDerechos - valorBonfComision
			// - valorBonifComRecPagoFrac) * porcentaje de IVA
			Double iva = (cobertura.getValorPrimaNeta()
					+ cobertura.getValorRecargoPagoFrac()
					+ cobertura.getValorDerechos()
					- cobertura.getValorBonifComision() 
					- cobertura.getValorBonifComRecPagoFrac())
					* ivaCotizacion/100D;
			cobertura.setValorIVA(iva);
			// valorPrimaTotal = primaNeta de la cobertura +
			// valorRecargoPagoFrac + valorDerechos - valorBonfComision
			// - valorBonifComRecPagoFrac + IVA
			Double primaTotal = cobertura.getValorPrimaNeta()
					+ cobertura.getValorRecargoPagoFrac()
					+ cobertura.getValorDerechos()
					- cobertura.getValorBonifComision()
					- cobertura.getValorBonifComRecPagoFrac();

			cobertura.setValorPrimaTotal(primaTotal + cobertura.getValorIVA());		

			coberturaPoliza.update(cobertura);
		}
		
	}
	public void calcularRiesgos(BigDecimal idToCotizacion, PolizaDTO polizaDTO,Double ivaCotizacion){
		int INCENDIO = 1;
		valorPrimaNetaEndoso= 0D;
		valorRecargoPagoFraccionadoEndoso= 0D;
		valorDerechosEndoso = 0D;
		valorBonificacionComisionEndoso = 0D;
		valorIVAEndoso = 0D;
		valorPrimaTotalEndoso= 0D;
		valorBonificacionComisionRPFEndoso = 0D;
		valorComisionEndoso = 0D;
		valorComisionRPFEndoso = 0D;		
		CotizacionDTO cotizacionDTO = cotizacion.findById(idToCotizacion);
		
		//Dias por devengar
		double diasPorDevengar = Utilerias.obtenerDiasEntreFechas(
				polizaDTO.getCotizacionDTO().getFechaInicioVigencia(),
				polizaDTO.getCotizacionDTO().getFechaFinVigencia());		
		
		if(cotizacionDTO.getTipoPolizaDTO().getProductoDTO() != null && 
				cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getClaveAjusteVigencia().intValue() == 0){
			diasPorDevengar = 365d;
		}	
		
		Double primaNetaCotizacion = coberturaPoliza.obtenerPrimaNetaPoliza(polizaDTO.getIdToPoliza());
		primaNetaCotizacion = primaNetaCotizacion * (diasPorDevengar / 365D );

		Double derechos = 0D;
		AgrupacionCotDTO agrupacion = agrupacionCot.buscarPorCotizacion(idToCotizacion,TIPO_PRIMER_RIESGO);
		 if(ivaCotizacion==null){
		     throw new RuntimeException("No se pudo obtener el valor del IVA.");
		 }
		//List<ParametroGeneralDTO> parametroIva = parametroGeneral.findByProperty("id.codigoParametroGeneral", BigDecimal.valueOf(20020D));
		List<ParametroGeneralDTO> parametrosLimiteSA = parametroGeneral.findByProperty("id.codigoParametroGeneral", BigDecimal.valueOf(30010D));
		Double limiteSA = Double.valueOf(parametrosLimiteSA.get(0).getValor());
		Double tipoCambioVal = polizaDTO.getTipoCambio();		
		
		//Se obtienen los gastos de expedicion
		if(cotizacionDTO.getValorDerechosUsuario() != null){
			derechos = cotizacionDTO.getValorDerechosUsuario();
		}else{
			GastoExpedicionDTO gasto = gastoExpedicion.getGastoExpedicion(cotizacionDTO.getIdMoneda(), BigDecimal.valueOf(primaNetaCotizacion));	
			if(gasto != null)
				derechos = gasto.getGastoExpedicionPoliza();
		}
		
		
		List<RiesgoPolizaDTO> riesgos = riesgoPoliza.findByProperty("id.idToPoliza", polizaDTO.getIdToPoliza());
		for(RiesgoPolizaDTO riesgo: riesgos){
			RiesgoCotizacionId id = new RiesgoCotizacionId();
			id.setIdToCotizacion(idToCotizacion);
			id.setIdToCobertura(riesgo.getId().getIdToCobertura());
			id.setIdToRiesgo(riesgo.getId().getIdToRiesgo());
			id.setIdToSeccion(riesgo.getId().getIdToSeccion());
			id.setNumeroInciso(riesgo.getId().getNumeroInciso());
			
			RiesgoCotizacionDTO riesgoCotizacionDTO = riesgoCotizacion.findById(id);
			
			ComisionCotizacionId id3 = new ComisionCotizacionId();
			id3.setIdTcSubramo(riesgoCotizacionDTO.getIdTcSubramo());
			id3.setIdToCotizacion(idToCotizacion);
			
			ComisionCotizacionDTO comision = null;
			Double sumaAseguradaIncendio = 0D;
			if(riesgoCotizacionDTO.getIdTcSubramo().intValue() == INCENDIO){
				limiteSA = limiteSA/tipoCambioVal;
				// Se obtiene la SA de Incendio por Inciso
				sumaAseguradaIncendio = coberturaCotizacion
						.obtenerSACoberturasBasicasIncendioPorCotizacion(
								idToCotizacion);					
				if (agrupacion != null) {
					if (riesgoCotizacionDTO.getCoberturaCotizacionDTO().getNumeroAgrupacion() == agrupacion.getId().getNumeroAgrupacion()) {
						id3.setTipoPorcentajeComision(TIPO_PRR);
					}else{
						id3.setTipoPorcentajeComision(TIPO_RO);
					}
				} else {
					if (sumaAseguradaIncendio < limiteSA) {
						id3.setTipoPorcentajeComision(TIPO_RO);
					} else if (sumaAseguradaIncendio >= limiteSA) {
						id3.setTipoPorcentajeComision(TIPO_RCI);
					}else{
						id3.setTipoPorcentajeComision(TIPO_RO);	
					}
				}					
			}else{
				id3.setTipoPorcentajeComision(TIPO_RO);
			}
			comision = comisionCotizacion.findById(id3);
			
			riesgo.setValorPrimaNeta(riesgo.getValorPrimaNeta() * (diasPorDevengar / 365D ));
			
			riesgo.setPorcentajeComision(comision.getPorcentajeComisionCotizacion().doubleValue());
			//valorComision = primaNeta del riesgo * porcentajeComision/100
			riesgo.setValorComision(riesgo.getValorPrimaNeta()*riesgo.getPorcentajeComision()/100);
			//valorRecargoPagoFrac = valorPrimaNeta * PorcentajeRPF/100
			riesgo.setValorRecargoPagoFrac(riesgo.getValorPrimaNeta() * polizaDTO.getPctPagoFraccionado() / 100);		
			//valorComisionRecPagoFrac = valorRecargoPagoFrac * porcentajeComision/100
			riesgo.setValorComisionRecPagoFrac(riesgo.getValorRecargoPagoFrac() * riesgo.getPorcentajeComision()/100);
			//valorCoaseguro = valorPrimaNeta * porcentajeCoaseguro / 100
			riesgo.setValorCoaseguro(riesgo.getValorPrimaNeta() * riesgoCotizacionDTO.getPorcentajeCoaseguro() / 100);			
			//valorDeducible = valorPrimaNeta * porcentajeDeducible / 100
			riesgo.setValorDeducible(riesgo.getValorPrimaNeta() * riesgoCotizacionDTO.getPorcentajeDeducible() / 100);
			//valorCuota = valorPrimaNeta / valorSumaAsegurada
			riesgo.setValorCuota(riesgo.getValorPrimaNeta()/riesgo.getValorSumaAsegurada());
			//derechos = derechos poliza * valorPrimaNetaCobertura / primaNetaCotizacion 			
			riesgo.setValorDerechos(derechos * (riesgo.getValorPrimaNeta()/ primaNetaCotizacion));		
			//valorBonifComision =  getValorComision * porcentajeBonifComision/100
			riesgo.setValorBonifComision(riesgo.getValorComision() * cotizacionDTO.getPorcentajebonifcomision()/100);		
			//valorBonifComRecPagoFrac = ValorComisionRecPagoFrac * porcentajeBonifComision/100
			riesgo.setValorBonifComRecPagoFrac(riesgo.getValorComisionRecPagoFrac() * cotizacionDTO.getPorcentajebonifcomision()/100);
			//valorComisionFinal = valorComision - valorBonifComision
			riesgo.setValorComisionFinal(riesgo.getValorComision() - riesgo.getValorBonifComision());
			//valorComFinalRecPagoFrac = valorComisionRecPagoFrac - valorBonifComRecPagoFac
			riesgo.setValorComFinalRecPagoFrac(riesgo.getValorComisionRecPagoFrac() - riesgo.getValorBonifComRecPagoFrac());
			
			//valorIVA = (primaNeta del riesgo + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac) * porcentaje de IVA
			Double iva = (riesgo.getValorPrimaNeta() + riesgo.getValorRecargoPagoFrac() +riesgo.getValorDerechos() 
						- riesgo.getValorBonifComision() -riesgo.getValorBonifComRecPagoFrac())* ivaCotizacion/100D; 
			riesgo.setValorIVA(iva);
			//valorPrimaTotal = primaNeta del riesgo + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac + IVA
			Double primaTotal = riesgo.getValorPrimaNeta() + riesgo.getValorRecargoPagoFrac() +riesgo.getValorDerechos()
								- riesgo.getValorBonifComision() -riesgo.getValorBonifComRecPagoFrac();
			riesgo.setValorPrimaTotal(primaTotal + riesgo.getValorIVA());					

			
			valorPrimaNetaEndoso += riesgo.getValorPrimaNeta();
			valorRecargoPagoFraccionadoEndoso += riesgo.getValorRecargoPagoFrac();
			valorDerechosEndoso += riesgo.getValorDerechos();
			valorBonificacionComisionEndoso += riesgo.getValorBonifComision();
			valorIVAEndoso += riesgo.getValorIVA();
			valorPrimaTotalEndoso += riesgo.getValorPrimaTotal();
			valorBonificacionComisionRPFEndoso += riesgo.getValorBonifComRecPagoFrac();
			valorComisionEndoso += riesgo.getValorComision();
			valorComisionRPFEndoso += riesgo.getValorComisionRecPagoFrac();			
			riesgoPoliza.update(riesgo);
		}
	}
}
