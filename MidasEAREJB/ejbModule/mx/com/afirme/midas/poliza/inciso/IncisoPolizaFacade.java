package mx.com.afirme.midas.poliza.inciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity IncisoPolizaDTO.
 * 
 * @see .IncisoPolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class IncisoPolizaFacade implements IncisoPolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved IncisoPolizaDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            IncisoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public IncisoPolizaDTO save(IncisoPolizaDTO entity) {
		LogDeMidasEJB3.log("saving IncisoPolizaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent IncisoPolizaDTO entity.
	 * 
	 * @param entity
	 *            IncisoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(IncisoPolizaDTO entity) {
		LogDeMidasEJB3.log("deleting IncisoPolizaDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(IncisoPolizaDTO.class, entity
					.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved IncisoPolizaDTO entity and return it or a copy
	 * of it to the sender. A copy of the IncisoPolizaDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            IncisoPolizaDTO entity to update
	 * @return IncisoPolizaDTO the persisted IncisoPolizaDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public IncisoPolizaDTO update(IncisoPolizaDTO entity) {
		LogDeMidasEJB3.log("updating IncisoPolizaDTO instance", Level.INFO,
				null);
		try {
			IncisoPolizaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public IncisoPolizaDTO findById(IncisoPolizaId id) {
		LogDeMidasEJB3.log("finding IncisoPolizaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			IncisoPolizaDTO instance = entityManager.find(
					IncisoPolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all IncisoPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the IncisoPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<IncisoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<IncisoPolizaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding IncisoPolizaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from IncisoPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all IncisoPolizaDTO entities.
	 * 
	 * @return List<IncisoPolizaDTO> all IncisoPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<IncisoPolizaDTO> findAll() {
		LogDeMidasEJB3.log("finding all IncisoPolizaDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from IncisoPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public void insertIncisosPorCotizacion(BigDecimal idToCotizacion,
			BigDecimal idToPoliza) {
		String queryString = "INSERT INTO MIDAS.toincisopol (idtopoliza,numeroinciso,claveestatus)"
				+ " SELECT "
				+ idToPoliza
				+ " as idTopoliza, incisocot.numeroinciso,"
				+ 1
				+ " as claveestatus "
				+ " FROM MIDAS.toincisocot incisocot WHERE incisocot.idtocotizacion = "
				+ idToCotizacion;
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();
	}
}