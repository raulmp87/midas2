package mx.com.afirme.midas.interfaz.moneda;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class MonedaInterfazFacade implements MonedaFacadeRemote {

	@SuppressWarnings("unchecked")
	public List<MonedaDTO> findAll(String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		
		try {
			LogDeMidasInterfaz.log("Entrando a MonedaFacade.findAll..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.CMONEDA");

			storedHelper
					.estableceMapeoResultados(
							"mx.com.afirme.midas.catalogos.moneda.MonedaDTO",
							
							"idTcMoneda," +
							"descripcion",
													
							"IDMoneda," +
							"Descripcion");
						
			List<MonedaDTO> monedaList = storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de MonedaFacade.findAll..." + this, Level.INFO, null);
			return monedaList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.CMONEDA", MonedaDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de MonedaFacade.findAll..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en MonedaFacade.findAll..." + this, Level.WARNING, e);
			throw e;
		}
	}

}
