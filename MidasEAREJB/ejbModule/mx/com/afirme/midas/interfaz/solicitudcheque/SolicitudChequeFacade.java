package mx.com.afirme.midas.interfaz.solicitudcheque;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dto.calculos.DetalleSolicitudCheque;
import mx.com.afirme.midas2.util.MidasException;

@Stateless
public class SolicitudChequeFacade implements SolicitudChequeFacadeRemote {

	public SolicitudChequeDTO consultaEstatusSolicitudCheque(BigDecimal idPago,
			String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a SolicitudChequeFacade.consultaEstatusSolicitudCheque..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.STPESTATUS_SOL_CHEQUE");

			storedHelper
					.estableceMapeoResultados(
							SolicitudChequeDTO.class.getCanonicalName(),
							
							"situacionPago," +
							"cuentaBancariaEmisora," +
							"tipoPago," +
							"folio," +
							"fechaMovimiento",
													
							"Sit_pago," +
							"cuenta," +
							"tipo," +
							"folio," +
							"fmov");
						
			storedHelper.estableceParametro("pPago_Midas", idPago);       
						
			SolicitudChequeDTO solicitudChequeDTO = (SolicitudChequeDTO)storedHelper.obtieneResultadoSencillo();
			LogDeMidasInterfaz.log("Saliendo de SolicitudChequeFacade.consultaEstatusSolicitudCheque..." + this, Level.INFO, null);
			if (solicitudChequeDTO != null) {
				return solicitudChequeDTO;
			}
			
			return null;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.STPESTATUS_SOL_CHEQUE", SolicitudChequeDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de SolicitudChequeFacade.consultaEstatusSolicitudCheque..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en SolicitudChequeFacade.consultaEstatusSolicitudCheque..." + this, Level.WARNING, e);
			throw e;
		}
		
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BigDecimal solicitaCheque(SolicitudChequeDTO solicitudCheque,String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreStoredProcedure = "SEYCOS.PKG_INT_MIDAS.STPSOLICITAR_CHEQUE";
		String descripcionParametros = "";
		try {
			String[] nombreParametros = {
					"pId_Pago_Midas","pTrans_cont","pId_moneda",
					"pTipo_Cambio","pId_prest_serv","pBeneficiario",
					"pTipo_pago","pConcepto_pol","pUsuario"};
			Object[] valoresParametros = {
					solicitudCheque.getIdPago(),solicitudCheque.getClaveTransaccionContable(),solicitudCheque.getIdMoneda(),
					solicitudCheque.getTipoCambio(),solicitudCheque.getIdPrestadorServicio(),solicitudCheque.getNombreBeneficiario(),
					solicitudCheque.getTipoPago(),solicitudCheque.getConceptoPoliza(),nombreUsuario};
			
			storedHelper = new StoredProcedureHelper(nombreStoredProcedure);
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valoresParametros[i]);
			}
			
			descripcionParametros = obtenerDescripcionParametros(nombreParametros, valoresParametros);
			LogDeMidasInterfaz.log("Entrando a SolicitudChequeFacade.solicitaCheque. Invocando SP: " + nombreStoredProcedure+", Par�metros: ("+descripcionParametros+")", Level.INFO, null);
			
			BigDecimal idSolicitudCheque = new BigDecimal(storedHelper.ejecutaActualizar());
			LogDeMidasInterfaz.log("Saliendo de SolicitudChequeFacade.solicitaCheque. Se invoc� el SP: " + nombreStoredProcedure+", Par�metros: ("+descripcionParametros+"), Resultado: idSolicitudCheque: "+idSolicitudCheque, Level.INFO, null);
			return idSolicitudCheque;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"SEYCOS.PKG_INT_MIDAS.STPSOLICITAR_CHEQUE", SolicitudChequeDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de SolicitudChequeFacade.solicitaCheque. SP consultado: " + nombreStoredProcedure+", Par�metros: ("+descripcionParametros+")", Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en SolicitudChequeFacade.solicitaCheque. SP consultado: " + nombreStoredProcedure+", Par�metros: ("+descripcionParametros+")", Level.SEVERE, e);
			throw e;
		}
	}

	public String obtenerDescripcionParametros(String[] nombreParametros,
			Object[] valoresParametros) {
		StringBuilder descripcionParametros = new StringBuilder("");
		for(int i=0;i<nombreParametros.length;i++){
				descripcionParametros.append(nombreParametros[i]).append(": ").append((valoresParametros[i] != null ? valoresParametros[i].toString() : valoresParametros[i]));
			if(i != (nombreParametros.length -1)){
				descripcionParametros.append(", ");
			}
		}
		return descripcionParametros.toString();
	}

	public String cancelaSolicitudCheque(BigDecimal idPago, String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a SolicitudChequeFacade.cancelaSolicitudCheque..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.stpCan_Sol_Cheque");

			storedHelper
					.estableceMapeoResultados(
							SolicitudChequeDTO.class.getCanonicalName(),
							
							"situacionPago",
							
							"Sit_pago");
						
			storedHelper.estableceParametro("pPagoMidas", idPago);       
						
			SolicitudChequeDTO solicitudChequeDTO = (SolicitudChequeDTO)storedHelper.obtieneResultadoSencillo();
			LogDeMidasInterfaz.log("Saliendo de SolicitudChequeFacade.cancelaSolicitudCheque..." + this, Level.INFO, null);
			if (solicitudChequeDTO != null) {
				return solicitudChequeDTO.getSituacionPago();
			}
			
			return null;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.MODIFICAR,
					"SEYCOS.PKG_INT_MIDAS.stpCan_Sol_Cheque", SolicitudChequeDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de SolicitudChequeFacade.cancelaSolicitudCheque..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en SolicitudChequeFacade.cancelaSolicitudCheque..." + this, Level.WARNING, e);
			throw e;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public SolicitudChequeDTO obtenerSolicitudCheque(Long idSolicitudCheque) {
		
		List<SolicitudChequeDTO> solicitudChequeList=new ArrayList<SolicitudChequeDTO>();
		
		String sp="SEYCOS.STPCONSULTARCHEQUE";
		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper(sp);
			
			String [] atributosDTO = {
					"idSolCheque",
					"importe",
					"fechaMovimiento",
					"idMoneda", 
					"tipoCambio", 
					"nombreBeneficiario",
					"conceptoMov"
			};
			String [] columnasResulset = {
					"ID_SOL_CHEQUE",
					"IMP_CHEQUE",
					"F_SOLICITUD",
					"ID_MONEDA", 
					"TIPO_CAMBIO", 
					"BENEFICIARIO_CHEQUE", 
					"TX_CONCEPTO" 
			};
			storedHelper.estableceMapeoResultados(SolicitudChequeDTO.class.getCanonicalName(),atributosDTO, columnasResulset);
			storedHelper.estableceParametro("pIdSolicitudCheque", idSolicitudCheque);
			solicitudChequeList=storedHelper.obtieneListaResultados();
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null, StoredProcedureErrorLog.TipoAccion.BUSCAR, sp, SolicitudChequeFacade.class, codErr, descErr);
			throw new RuntimeException(e);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return (solicitudChequeList != null ? solicitudChequeList.get(0) : null);		
		
	}
	
	/**
	 * Metodo para obtener el detalle de cada solicitud de cheque
	 * @param idSolicitudCheque
	 * @return
	 * @throws MidasException
	 */
	@SuppressWarnings("unchecked")
	public List<DetalleSolicitudCheque> obtenerDetallePorSolicitudCheque(Long idSolicitudCheque) {
		List<DetalleSolicitudCheque> detalleSolicitudCheque=new ArrayList<DetalleSolicitudCheque>();
		String sp="SEYCOS.PKG_INT_MIDAS_E2.STPCONSULTARDETALLECHEQUE";
		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper(sp);
			String [] atributosDTO = {
					"idSolicitudCheque",
					"rubro",
					"idCuentaContable",
					"concepto", 
					"importe", 
					"cuentaContable" 
			};
			String [] columnasResulset = {
					"id_sol_cheque",
					"cvel_t_rubro",
					"id_cuenta_cont",
					"cve_c_a", 
					"imp_rubro", 
					"cuenta_contable" 
			};
			storedHelper.estableceMapeoResultados(DetalleSolicitudCheque.class.getCanonicalName(),atributosDTO, columnasResulset);
			storedHelper.estableceParametro("pIdSolicitudCheque", idSolicitudCheque);
			detalleSolicitudCheque=storedHelper.obtieneListaResultados();
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null, StoredProcedureErrorLog.TipoAccion.BUSCAR, sp, SolicitudChequeFacade.class, codErr, descErr);
			throw new RuntimeException(e);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return detalleSolicitudCheque;
	}

}
