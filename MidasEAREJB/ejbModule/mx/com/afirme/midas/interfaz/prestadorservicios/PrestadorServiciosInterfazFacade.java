package mx.com.afirme.midas.interfaz.prestadorservicios;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class PrestadorServiciosInterfazFacade implements PrestadorServiciosInterfazFacadeRemote {

	public PrestadorServiciosDTO detallePrestador(
			BigDecimal idPrestadorServicios, String nombreUsuario)
			throws Exception {
		
		StoredProcedureHelper storedHelper = null;
		try {
			
			LogDeMidasInterfaz.log("Entrando a PrestadorServiciosFacade.detallePrestador..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.cDetalle_Prestador");

			storedHelper
					.estableceMapeoResultados(
							PrestadorServiciosDTO.class.getCanonicalName(),
							
							"idPrestadorServicios," +
							"nombrePrestador," +
							"tipoPrestador",
													
							"Id_prestador," +
							"Nombre," +
							"Tipo_prestador");
			
						
			storedHelper.estableceParametro("pId_Prestador", idPrestadorServicios);       
						
			PrestadorServiciosDTO prestadorServicios = (PrestadorServiciosDTO)storedHelper.obtieneResultadoSencillo();
			LogDeMidasInterfaz.log("Saliendo de PrestadorServiciosFacade.detallePrestador..." + this, Level.INFO, null);
			return prestadorServicios;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.cDetalle_Prestador", PrestadorServiciosDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de PrestadorServiciosFacade.detallePrestador..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en PrestadorServiciosFacade.detallePrestador..." + this, Level.WARNING, e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<PrestadorServiciosDTO> listarPrestadores(
			PrestadorServiciosDTO prestadorServicios, String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a PrestadorServiciosFacade.listarPrestadores..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.cLista_Prestador");

			storedHelper
					.estableceMapeoResultados(
							PrestadorServiciosDTO.class.getCanonicalName(),
							
							"idPrestadorServicios," +
							"nombrePrestador," +
							"tipoPrestador",
													
							"Id_prestador," +
							"Nombre," +
							"Tipo_prestador");
			
						
			storedHelper.estableceParametro("pNombre", prestadorServicios.getNombrePrestador());       
						
			List<PrestadorServiciosDTO> prestadorServiciosList = storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de PrestadorServiciosFacade.listarPrestadores..." + this, Level.INFO, null);
			return prestadorServiciosList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.cLista_Prestador", PrestadorServiciosDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de PrestadorServiciosFacade.listarPrestadores..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en PrestadorServiciosFacade.listarPrestadores..." + this, Level.WARNING, e);
			throw e;
		}
	}

}
