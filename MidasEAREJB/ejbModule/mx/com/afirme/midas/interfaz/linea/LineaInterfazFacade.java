package mx.com.afirme.midas.interfaz.linea;

import javax.ejb.Stateless;

import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;

@Stateless
public class LineaInterfazFacade implements LineaFacadeRemote {

	public void save(LineaDTO entity, String nombreUsuario) throws Exception {
		try
        {
			/*
			StoredProcedureHelper storedHelper = new StoredProcedureHelper("stpMtto_Linea");
			storedHelper.estableceMapeoResultados("void.class", "", "");
			storedHelper.estableceParametro("pId_midas", entity.getIdTmLinea());
			//TODO: Encontrar el parametro a mandar
//			storedHelper.estableceParametro("pDescMidas", ""); 
			
			storedHelper.estableceParametro("pUsuario", nombreUsuario);
			
			int res = storedHelper.ejecutaActualizar();
			
	        if (res != 1) {
	        	throw new Exception("No se pudo ejecutar la transaccion");
	        }
	        */
			System.out.println("AAC entro a la interfaz de linea- Guardar en Seycos...");
        }
	    catch(Exception e) {
	        StoredProcedureErrorLog.doLog(nombreUsuario, StoredProcedureErrorLog.TipoAccion.GUARDAR, "stpMtto_Linea", entity,null,null);
	        throw e;
	    } 

	}

}
