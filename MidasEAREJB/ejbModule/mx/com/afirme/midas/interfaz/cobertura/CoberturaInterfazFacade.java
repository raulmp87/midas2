package mx.com.afirme.midas.interfaz.cobertura;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;

@Stateless
public class CoberturaInterfazFacade implements CoberturaFacadeRemote {

	public void save(CoberturaDTO entity, String nombreUsuario)
			throws Exception {
		try
        {
			/*
			StoredProcedureHelper storedHelper = new StoredProcedureHelper("stpMtto_Cobertura");
			storedHelper.estableceMapeoResultados("void.class", "", "");
			storedHelper.estableceParametro("pId_midas", entity.getIdToCobertura());
			storedHelper.estableceParametro("pDescMidas", entity.getDescripcion());
			storedHelper.estableceParametro("pUsuario", nombreUsuario);
			
			int res = storedHelper.ejecutaActualizar();
			
	        if (res != 1) {
	        	throw new Exception("No se pudo ejecutar la transaccion");
	        }
	        */
			System.out.println("AAC entro a la interfaz de cobertura- Guardar en Seycos...");
        }
	    catch(Exception e) {
	        StoredProcedureErrorLog.doLog(nombreUsuario, StoredProcedureErrorLog.TipoAccion.GUARDAR, "stpMtto_Cobertura", entity, null, null);
	        throw e;
	    } 

	}

}
