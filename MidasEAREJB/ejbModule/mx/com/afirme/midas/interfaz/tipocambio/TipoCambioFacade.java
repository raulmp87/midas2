package mx.com.afirme.midas.interfaz.tipocambio;

import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelperSQL;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class TipoCambioFacade implements TipoCambioFacadeRemote {

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Double obtieneTipoCambioPorDia(Date fecha, String nombreUsuario)
			throws Exception {
		StoredProcedureHelperSQL storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a TipoCambioFacade.obtieneTipoCambioPorDia..." + this, Level.INFO, null);
			Double cantidad = null;
	
			storedHelper = new StoredProcedureHelperSQL(
					"stpConsTC_dia");
			
			storedHelper.estableceParametro("@fecha", fecha); 
			storedHelper.estableceParametro("@tipocambio", new Double("0")); 
			
			cantidad = (Double) storedHelper.obtieneResultadoOutputSencillo(2, java.sql.Types.DOUBLE);
			LogDeMidasInterfaz.log("Saliendo de TipoCambioFacade.obtieneTipoCambioPorDia..." + this, Level.INFO, null);			
			return cantidad;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"stpConsTC_dia", Double.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de TipoCambioFacade.obtieneTipoCambioPorDia..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en TipoCambioFacade.obtieneTipoCambioPorDia..." + this, Level.WARNING, e);
			throw e;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Double obtieneTipoCambioPorMes(Integer mes, Integer anio,
			String nombreUsuario) throws Exception {
		StoredProcedureHelperSQL storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a TipoCambioFacade.obtieneTipoCambioPorMes..." + this, Level.INFO, null);
			Double cantidad = null;
	
			storedHelper = new StoredProcedureHelperSQL(
					"stpConsTC_mes");
			
			storedHelper.estableceParametro("@mes", mes); 
			storedHelper.estableceParametro("@anio", anio); 
			storedHelper.estableceParametro("@tipocambio", new Double("0")); 
			
			cantidad = (Double) storedHelper.obtieneResultadoOutputSencillo(3, java.sql.Types.DOUBLE);
			LogDeMidasInterfaz.log("Saliendo de TipoCambioFacade.obtieneTipoCambioPorMes..." + this, Level.INFO, null);			
			return cantidad;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"stpConsTC_mes", Double.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de TipoCambioFacade.obtieneTipoCambioPorMes..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en TipoCambioFacade.obtieneTipoCambioPorMes..." + this, Level.WARNING, e);
			throw e;
		}
	}

}
