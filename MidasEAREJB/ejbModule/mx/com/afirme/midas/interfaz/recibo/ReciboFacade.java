package mx.com.afirme.midas.interfaz.recibo;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class ReciboFacade implements ReciboFacadeRemote {

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReciboDTO> emiteRecibosPoliza(BigDecimal idPoliza, String nombreUsuario)
			throws Exception {
		
		StoredProcedureHelper storedHelper = null;
		try {
			
			LogDeMidasInterfaz.log("Entrando a ReciboFacade.emiteRecibosPoliza..." + this, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.stpEmiteRecibosPol");

			storedHelper
					.estableceMapeoResultados(
							ReciboDTO.class.getCanonicalName(),
							
							"numeroFolioRecibo," +
							"numeroExhibicion," +
							"llaveFiscal," +
							"importe," +
							"fechaLimitePago",
													
							"num_folio_rbo," +
							"num_exhibicion," +
							"llave_fiscal," +
							"importe," +
							"fecha_limite_pago");
			
		
			
			storedHelper.estableceParametro("id_poliza", idPoliza);       
			
			List<ReciboDTO> reciboList = storedHelper.obtieneListaResultados();
			
			LogDeMidasInterfaz.log("Saliendo de ReciboFacade.emiteRecibosPoliza..." + this, Level.INFO, null);
			
			return reciboList;
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"SEYCOS.PKG_INT_MIDAS.stpEmiteRecibosPol", ReciboDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de ReciboFacade.emiteRecibosPoliza..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ReciboFacade.emiteRecibosPoliza..." + this, Level.WARNING, e);
			throw e;
		}
	}
	
	/**
	 * Consulta la informaci�n de los Recibos de una P�liza, indicando tambi�n el numero de endoso
	 * @param numPolizaEndoso (Id de la p�liza | Numero del Endoso)
	 * @param nombreUsuario Nombre del Usuario que realiza la operacion
	 * @return Los recibos de la p�liza/endoso
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<ReciboDTO> consultaRecibos(String numPolizaEndoso, String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			
			LogDeMidasInterfaz.log("Entrando a ReciboFacade.consultaRecibos..." + this, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.stpConsultaRecibo");

			storedHelper
					.estableceMapeoResultados(
							ReciboDTO.class.getCanonicalName(),
							
							"numeroFolioRecibo," +
							"numeroExhibicion," +
							"llaveFiscal," +
							"importe," +
							"fechaLimitePago," +
							"origenRecibo," +
							"numeroEndoso," +
							"situacion," +
							"fechaSituacion," +
							"origenSituacion," +
							"fechaIncioVigencia," +
							"fechaFinVigencia" ,
																			
							"num_folio_rbo," +
							"num_exhibicion," +
							"llave_fiscal," +
							"importe," +
							"fecha_limite_pago," +
							"origen_recibo," +
							"endoso," +
							"situacion," +
							"f_situacion," +
							"origen_situacion,"+
							"f_inivig,"+
							"f_finvig");
			
			
			storedHelper.estableceParametro("pid_cot_midas", numPolizaEndoso);       
			
			
			List<ReciboDTO> reciboList = storedHelper.obtieneListaResultados();
			
			LogDeMidasInterfaz.log("Saliendo de ReciboFacade.consultaRecibos..." + this, Level.INFO, null);
			
			return reciboList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.stpConsultaRecibo", ReciboDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de ReciboFacade.consultaRecibos..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ReciboFacade.consultaRecibos..." + this, Level.WARNING, e);
			throw e;
		}
	}

	@Override
	public List<ReciboDTO> findByPolicy(String insurancePolicy) throws Exception{
		List<ReciboDTO> receipts = findByPolicy(insurancePolicy, null);
		return receipts;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<ReciboDTO> findByPolicy(String insurancePolicy,
			String subsection) {
		StoredProcedureHelper spRecibos = null;
		List<ReciboDTO> receipts = new ArrayList<ReciboDTO>(1);
		try {
			if(subsection != null){
				spRecibos = new StoredProcedureHelper("SEYCOS.PKG_CC_PAYMENT.RECEIPTS_BY_POLICY_SUBSECTION");
			}else{
				spRecibos = new StoredProcedureHelper("SEYCOS.PKG_CC_PAYMENT.RECEIPTS_BY_POLICY");
			}
			
			String[] parts = insurancePolicy.split("-");
			spRecibos.estableceMapeoResultados(
					ReciboDTO.class.getCanonicalName(), "idCotizacion,"
							+ "idRecibo," + "numeroFolioRecibo,"
							+ "fechaLimitePago," + "folioFiscal,"
							+ "serieFiscal," + "importe,"
							+ "fechaIncioVigencia," + "fechaFinVigencia,"
							+ "idMoneda," + "numeroExhibicion," + "idInciso,"
							+ "situacion," + "idLinea",

					"idCotizacion," + "idRecibo," + "folio," + "vencimiento,"
							+ "folioFiscal," + "serieFiscal," + "total,"
							+ "cubreDesde," + "cubreHasta," + "idMoneda,"
							+ "exhibicion," + "idInciso," + "situacion," + "idLinea");
			spRecibos.estableceParametro("idCentroEmis", parts[0]);
			spRecibos.estableceParametro("numPoliza", parts[1]);
			spRecibos.estableceParametro("numRenovPol", parts[2]);
			if(subsection != null){
				spRecibos.estableceParametro("idInciso", subsection);
			}
			receipts = spRecibos.obtieneListaResultados();
		} catch (SQLException e) {
			LogDeMidasInterfaz.log("Excepcion en BD de ReciboFacade.findByPolicy..." + this, Level.WARNING, e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ReciboFacade.findByPolicy..." + this, Level.WARNING, e);
		}
		return receipts;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<ReciboDTO> findByPolicyAndRFC(String insurancePolicy, String subsection, String siglasRFC, String fechaRFC, String homoClaveRFC) {
		StoredProcedureHelper spRecibos = null;
		List<ReciboDTO> receipts = new ArrayList<ReciboDTO>();
		try {
			if(subsection != null){
				spRecibos = new StoredProcedureHelper("SEYCOS.PKG_CC_PAYMENT.RECEIPTS_POLICY_SUBSECTION_RFC");
			}else{
				spRecibos = new StoredProcedureHelper("SEYCOS.PKG_CC_PAYMENT.RECEIPTS_BY_POLICY_AND_RFC");
			}
			
			String[] parts = insurancePolicy.split("-");
			spRecibos.estableceMapeoResultados(
					ReciboDTO.class.getCanonicalName(), "idCotizacion,"
							+ "idRecibo," + "numeroFolioRecibo,"
							+ "fechaLimitePago," + "folioFiscal,"
							+ "serieFiscal," + "importe,"
							+ "fechaIncioVigencia," + "fechaFinVigencia,"
							+ "idMoneda," + "numeroExhibicion," + "idInciso,"
							+ "situacion," + "idLinea," + "generacionRecibo",

							"idCotizacion," + "idRecibo," + "folio," + "vencimiento,"
							+ "folioFiscal," + "serieFiscal," + "total,"
							+ "cubreDesde," + "cubreHasta," + "idMoneda,"
							+ "exhibicion," + "idInciso," + "situacion," + "idLinea,"+ "generacionRecibos");
			spRecibos.estableceParametro("idCentroEmis", parts[0]);
			spRecibos.estableceParametro("numPoliza", parts[1]);
			spRecibos.estableceParametro("numRenovPol", parts[2]);			
			if(subsection != null){
				spRecibos.estableceParametro("idInciso", subsection);
			}
			spRecibos.estableceParametro("siglasRFC", siglasRFC);
			spRecibos.estableceParametro("fechaRFC", fechaRFC);
			spRecibos.estableceParametro("homoClaveRFC", homoClaveRFC);
			receipts = spRecibos.obtieneListaResultados();
		} catch (SQLException e) {
			LogDeMidasInterfaz.log("Excepcion en BD de ReciboFacade.findByPolicy..." + this, Level.WARNING, e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ReciboFacade.findByPolicy..." + this, Level.WARNING, e);
		}
		return receipts;
	}
	
	@Override
	public List<ReciboDTO> findById(String insurancePolicy, String subsection, String[] ids){
		List<ReciboDTO> receipts = findByPolicy(insurancePolicy, subsection);
		List<ReciboDTO> filteredReceipts = new ArrayList<ReciboDTO>();
		for(ReciboDTO receipt: receipts){
			for(String id: ids){
				if(receipt.getIdRecibo().toString().equals(id)){
					filteredReceipts.add(receipt);
				}
			}
		}
		return filteredReceipts;
	}

	@Override
	public List<ReciboDTO> findById(String insurancePolicy, String[] ids){
		return findById(insurancePolicy, null, ids);
	}

	
}
