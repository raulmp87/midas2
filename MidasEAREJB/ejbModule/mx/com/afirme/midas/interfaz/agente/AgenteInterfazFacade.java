package mx.com.afirme.midas.interfaz.agente;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.agente.tipocedula.TipoCedulaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

@Stateless
public class AgenteInterfazFacade implements AgenteFacadeRemote {

	@SuppressWarnings("unchecked")
	public List<AgenteDTO> listarAgentes(String nombreUsuario, String tipoCedula)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a AgenteFacade.listarAgentes..." + this, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.CLISTA_AGENTE");
			
			storedHelper.estableceMapeoResultados(AgenteDTO.class.getCanonicalName(),this.propiedadesDTO(),this.camposProcedure());
			
			storedHelper.estableceParametro("pTipoCedula",tipoCedula); 
			
			List<AgenteDTO> agenteList = storedHelper.obtieneListaResultados();
			
			LogDeMidasInterfaz.log("Saliendo de AgenteFacade.listarAgentes..." + this, Level.INFO, null);

			return agenteList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.CLISTA_AGENTE", AgenteDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de AgenteFacade.listarAgentes..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en AgenteFacade.listarAgentes..." + this, Level.WARNING, e);
			throw e;
		}
	}

	public AgenteDTO findById(Integer idAgente, String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a AgenteFacade.findById..." + this, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.CDETALLE_AGENTE");

			storedHelper.estableceMapeoResultados(AgenteDTO.class.getCanonicalName(),this.propiedadesDTO(),this.camposProcedure());
			
			storedHelper.estableceParametro("pId_Agente", idAgente); 
			
			AgenteDTO agente = (AgenteDTO)storedHelper.obtieneResultadoSencillo();
			LogDeMidasInterfaz.log("Saliendo de AgenteFacade.findById..." + this, Level.INFO, null);
			
			return agente;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.CDETALLE_AGENTE", AgenteDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de AgenteFacade.findById..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en AgenteFacade.findById..." + this, Level.WARNING, e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TipoCedulaDTO> listarTiposCedula(String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a AgenteFacade.listarTiposCedula..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.CTIPOS_CEDULA");

			storedHelper
					.estableceMapeoResultados(
							TipoCedulaDTO.class.getCanonicalName(),
							
							"cedula," +
							"descripcion",
													
							"Cedula," +
							"Descripcion");
						
			List<TipoCedulaDTO> tipoCedulaDTOList =  storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de AgenteFacade.listarTiposCedula..." + this, Level.INFO, null);
			return tipoCedulaDTOList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.CTIPOS_CEDULA", TipoCedulaDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de AgenteFacade.listarTiposCedula..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en AgenteFacade.listarTiposCedula..." + this, Level.WARNING, e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<AgenteDTO> listaEjecutivoAgentes(String idOficina,
			String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a AgenteFacade.listaEjecutivoAgentes..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.CLISTA_EJECUTIVO_AGT");

			storedHelper
					.estableceMapeoResultados(
							AgenteDTO.class.getCanonicalName(),
							
							"idTcAgente," +
							"nombre," + 
							"idOficina," +
							"nombreOficina",
							
							"ID_Agente," +
							"Nombre," + 
							"ID_Oficina," +
							"Nom_Oficina");
			
			storedHelper.estableceParametro("pId_Oficina", idOficina); 
						
			List<AgenteDTO> agenteDTOList =  storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de AgenteFacade.listaEjecutivoAgentes..." + this, Level.INFO, null);
			return agenteDTOList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.CLISTA_EJECUTIVO_AGT", AgenteDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de AgenteFacade.listaEjecutivoAgentes..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en AgenteFacade.listaEjecutivoAgentes..." + this, Level.WARNING, e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<AgenteDTO> listaEjecutivos(String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a AgenteFacade.listaEjecutivos..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.CLISTA_EJECUTIVO");

			storedHelper
					.estableceMapeoResultados(
							AgenteDTO.class.getCanonicalName(),
							
							"idOficina," +
							"nombreOficina",
							
							"ID_Oficina," +
							"Nom_Oficina");
			
						
			List<AgenteDTO> ejecutivoDTOList =  storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de AgenteFacade.listaEjecutivos..." + this, Level.INFO, null);
			return ejecutivoDTOList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.CLISTA_EJECUTIVO", AgenteDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de AgenteFacade.listaEjecutivos..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en AgenteFacade.listaEjecutivos..." + this, Level.WARNING, e);
			throw e;
		}
	}
	
	private String propiedadesDTO(){
	    String propiedadesDTO=
    				"idTcAgente,"+
    				"nombre,"+
    	                        "tipoCedula,"+
    	                        "email,"+
    	                        "tipoPersona,"+
    	                        "telefonoOficina,"+
    	                    	"promotoria,"+
    	                    	"nombrePromotoria,"+
    	                       	"idOficina,"+
    	                    	"nombreOficina,"+
    	                    	"idGerencia,"+
    	                    	"nombreGerencia,"+
    	                    	"direccion,"+
    	                    	"idColonia,"+
    	                    	"colonia,"+
    	                    	"codigoPostal,"+
    	                    	"idCiudad,"+
    	                    	"ciudad,"+
    	                    	"idEstado,"+
    	                    	"estado,"+
    	                    	"generaComision,"+
    	                    	"contabilizaComision,"+
    	                    	"generaCheque,"+
    	                    	"imprimeEstadoCuenta,"+
    	                    	"emailOficina,"+
    	                    	"emailGerencia,"+
    	                    	"esVentaDirecta," +
    	                    	"fechaVencimientoCedula,"+
    	                    	"situacionAgente";
	    return propiedadesDTO;
	}
	
	private String camposProcedure(){
	    String camposProcedure= 
                	    "ID_Agente,"+
                	    "Nombre,"+
                	    "Tipo_Cedula,"+
                	    "E_mail_agte,"+
                	    "Tipo_Persona,"+
                	    "Telefono,"+
                	    "ID_Promotoria,"+
                	    "Nom_Promotoria,"+
                	    "ID_Oficina,"+
                	    "Nom_Oficina,"+
                	    "ID_Gerencia,"+
                	    "Nom_Gerencia,"+
                	    "Calle_numero,"+
                	    "Id_Colonia,"+
                	    "Colonia,"+
                	    "Codigo_Postal,"+
                	    "Id_Ciudad,"+
                	    "Ciudad,"+
                	    "Id_Estado,"+
                	    "Estado,"+
                	    "b_comision,"+
                	   "b_contab_comis,"+
                	    "b_genera_cheque,"+
                	    "b_imp_edo_cta,"+
                	    "e_mail_oficina,"+
                	    "e_mail_gerencia,"+
	    				"b_vta_directa,"+
	    				"f_venc_cedula,"+
	    				"sit_agente";
	    return camposProcedure;
	}

	@Override
	public List<AgenteDTO> findAll() {
		try {
			return listarAgentes(null,null);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<AgenteDTO> listRelated(Object id) {
		throw new RuntimeException("Not implemented yet");
	}

	@Override
	public AgenteDTO findById(BigDecimal id) {
		try {
			return findById(id.intValue(),"");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public AgenteDTO findById(CatalogoValorFijoId id) {
		throw new RuntimeException("Not implemented");
	}

	@Override
	public AgenteDTO findById(double id) {
		try {
			return findById((int)id,"");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
