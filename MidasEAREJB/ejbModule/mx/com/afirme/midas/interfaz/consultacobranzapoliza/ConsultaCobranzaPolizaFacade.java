package mx.com.afirme.midas.interfaz.consultacobranzapoliza;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class ConsultaCobranzaPolizaFacade implements ConsultaCobranzaPolizaFacadeRemote {
	
	public DatosCobranzaPolizaDTO consultaCobranzaPoliza (BigDecimal idToPoliza,String nombreUsuario) throws Exception{
		StoredProcedureHelper storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a ConsultaCobranzaPolizaFacade.consultaCobranzaPoliza..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper("SEYCOS.PKG_INT_MIDAS.STPCONSULTAPOLIZA");

			storedHelper
					.estableceMapeoResultados(
							DatosCobranzaPolizaDTO.class.getCanonicalName(),
							
							"idToPoliza," +
							"numeroPoliza," +
							"situacion," +
							"ultimoReciboPagado," +
							"fechaVencimiento," +
							"fechaUltimoPago," +
							"montoPagado," +
							"montoPorVencer," +
							"montoVencido",
													
							"IDTOPOLIZA," +
							"NUMPOLIZA," +
							"SITUACION," +
							"ULTIMORECIBOPAGADO," +
							"FECHAVENCIMIENTO," +
							"FECHAULTIMOPAGO," +
							"MONTOPAGADO," +
							"MONTOPORVENCER," +
							"MONTOVENCIDO");
						
			storedHelper.estableceParametro("pIdentificador", idToPoliza.toString());       
						
			DatosCobranzaPolizaDTO datosCobranzaPolizaDTO = (DatosCobranzaPolizaDTO)storedHelper.obtieneResultadoSencillo();
			LogDeMidasInterfaz.log("Saliendo de ConsultaCobranzaPolizaFacade.consultaCobranzaPoliza..." + this, Level.INFO, null);
			if (datosCobranzaPolizaDTO != null) {
				return datosCobranzaPolizaDTO;
			}
			
			return null;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.stpConsultaPoliza", DatosCobranzaPolizaDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de ConsultaCobranzaPolizaFacade.consultaCobranzaPoliza..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ConsultaCobranzaPolizaFacade.consultaCobranzaPoliza..." + this, Level.WARNING, e);
			throw e;
		}
	}
}
