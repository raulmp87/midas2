package mx.com.afirme.midas.interfaz.formapago;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class FormaPagoInterfazFacade implements FormaPagoFacadeRemote {

	@SuppressWarnings("unchecked")
	public List<FormaPagoIDTO> findByProperty(FormaPagoIDTO formaPago,
			String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.CIDFORMA_PAGO");

			storedHelper
					.estableceMapeoResultados(
							FormaPagoIDTO.class.getCanonicalName(),
							
							"idFormaPago," +
							"descripcion," +
							"porcentajeRecargoPagoFraccionado," +
							"numeroRecibosGenerados",
													
							"IDForma_pago," +
							"Descripcion," +
							"Pct_rpf," +
							"Recibos_Generados");
			
			
			storedHelper.estableceParametro("pId_Forma_Pago", formaPago.getIdFormaPago());       
			storedHelper.estableceParametro("pDescripcion", formaPago.getDescripcion());
			storedHelper.estableceParametro("pId_Moneda", formaPago.getIdMoneda());
			
			List<FormaPagoIDTO> formaPagoList = storedHelper.obtieneListaResultados();
			return formaPagoList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.CIDFORMA_PAGO", FormaPagoIDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de FormaPagoFacade.findByProperty..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en FormaPagoFacade.findByProperty..." + this, Level.WARNING, e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<FormaPagoIDTO> findByProperty(FormaPagoIDTO formaPago,
			String nombreUsuario, String claveNegocio) throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.CIDFORMA_PAGO");

			storedHelper
					.estableceMapeoResultados(
							FormaPagoIDTO.class.getCanonicalName(),
							
							"idFormaPago," +
							"descripcion," +
							"porcentajeRecargoPagoFraccionado," +
							"numeroRecibosGenerados," +
							"derechosProrrateados",
													
							"IDForma_pago," +
							"Descripcion," +
							"Pct_rpf," +
							"Recibos_Generados," +
							"DERECHOSPRORRATEADOS");
			
			
			storedHelper.estableceParametro("pId_Forma_Pago", formaPago.getIdFormaPago());       
			storedHelper.estableceParametro("pDescripcion", formaPago.getDescripcion());
			storedHelper.estableceParametro("pId_Moneda", formaPago.getIdMoneda());
			storedHelper.estableceParametro("pcveOrigen", claveNegocio);
			
			List<FormaPagoIDTO> formaPagoList = storedHelper.obtieneListaResultados();
			return formaPagoList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.CIDFORMA_PAGO", FormaPagoIDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de FormaPagoFacade.findByProperty..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en FormaPagoFacade.findByProperty..." + this, Level.WARNING, e);
			throw e;
		}
	}

}
