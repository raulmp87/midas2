package mx.com.afirme.midas.interfaz.cliente;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.ClientSystemException;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionCampo;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionGrupo;
import mx.com.afirme.midas2.dto.ClientBeanValidator.SectionType;
import mx.com.afirme.midas2.dto.ClientBeanValidatorProcessor;
import mx.com.afirme.midas2.service.negocio.cliente.ConfiguracionSeccionPorNegocioService;
import org.apache.commons.lang.StringUtils;
@Stateless
public class ClienteFacade implements ClienteFacadeRemote {
	@EJB
	private ConfiguracionSeccionPorNegocioService configuracionSeccionPorNegocioService;
	
	public ClienteDTO findById(BigDecimal idCliente, String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		
		try {
			
			LogDeMidasInterfaz.log("Entrando a ClienteFacade.findById..." + this, Level.FINE, null);
			
			storedHelper = new StoredProcedureHelper("SEYCOS.PKG_INT_MIDAS.CDETALLE_CLIENTE");

			String [] atributosDTO = {
					"idCliente", "claveTipoPersona", "nombre", "apellidoPaterno",
					"apellidoMaterno", "fechaNacimiento", "codigoRFC", "telefono",
					"email", "nombreCalle", "idColonia", "nombreColonia", 
					"codigoPostal", "idEstadoString", "descripcionEstado", "idMunicipioString", "nombreDelegacion",
					"sexo","claveOcupacion","descripcionOcupacion",
					"codigoCURP","claveNacionalidad","claveEstadoNacimiento","claveCiudadNacimiento"
			};
			
			String [] columnasResulset = {
					"Id_Cliente", "Tipo_Persona", "Nombre", "Apellido_Paterno",
					"Apellido_Materno", "Fecha", "RFC", "Telefono",
					"E_mail", "Calle_Numero", "id_Colonia", "Colonia", 
					"Codigo_postal", "id_estado", "Estado", "id_ciudad", "Ciudad",
					"Sexo","Id_Giro_Ocup","Desc_Giro_Ocup",
					"curp","id_pais","id_estado_nac","id_ciudad_nac"
			};
			
			storedHelper.estableceMapeoResultados(
							ClienteDTO.class.getCanonicalName(),
							atributosDTO,
							columnasResulset);
			
			storedHelper.estableceParametro("pId_Cliente", idCliente);
			
			
			
			ClienteDTO cliente =  (ClienteDTO)storedHelper.obtieneResultadoSencillo();
			if(cliente != null){
				if (cliente.getIdEstadoString() != null) {
					cliente.setIdEstado(new BigDecimal(cliente.getIdEstadoString()));
				}
				
				if (cliente.getIdMunicipioString() != null) {
					cliente.setIdMunicipio(new BigDecimal(cliente.getIdMunicipioString()));
				}
			}
			LogDeMidasInterfaz.log("Saliendo de ClienteFacade.findById..." + this, Level.FINE, null);
			return cliente;
			
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.CDETALLE_CLIENTE", ClienteDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.findById..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.findById..." + this, Level.SEVERE, e);
			throw e;
		}
		
	}

	public List<ClienteDTO> findByProperty(String propertyName, Object value,
			String nombreUsuario) throws Exception {
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<ClienteDTO> listarFiltrado(ClienteDTO entity,
			String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		
		try {
			LogDeMidasInterfaz.log("Entrando a ClienteFacade.listarFiltrado..." + this, Level.FINE, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.CLISTA_CLIENTE");

			storedHelper
					.estableceMapeoResultados(
							ClienteDTO.class.getCanonicalName(),
							
							"nombre," +
							"codigoRFC," +
							"direccionCompleta," +
							"idCliente" ,//"claveTipoPersona" ,
							
							"Nombre_Cliente," +
							"RFC," +
							"Domicilio," +
							"ID_Cliente");//"Tipo_persona");
							
			
			storedHelper.estableceParametro("pNombre", ((entity.getNombre()==null)?"":entity.getNombre()));
			storedHelper.estableceParametro("pApellido_Paterno", ((entity.getApellidoPaterno()==null)?"":entity.getApellidoPaterno()));
			storedHelper.estableceParametro("pApellido_Materno", ((entity.getApellidoMaterno()==null)?"":entity.getApellidoMaterno()));
			storedHelper.estableceParametro("pRFC", ((entity.getCodigoRFC()==null)?"":entity.getCodigoRFC()));
			storedHelper.estableceParametro("pTipo_Persona", ((entity.getClaveTipoPersona()==null)?"":entity.getClaveTipoPersona()));
			storedHelper.estableceParametro("pId_Cliente", ((entity.getIdCliente()==null)?"":entity.getIdCliente()));
			storedHelper.estableceParametro("pcve_usuario", ((nombreUsuario==null)?"":nombreUsuario));
			
			
			
			List<ClienteDTO> clienteList = storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de ClienteFacade.listarFiltrado..." + this, Level.FINE, null);
			return clienteList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.CLISTA_CLIENTE", entity.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.listarFiltrado..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.listarFiltrado..." + this, Level.SEVERE, e);
			throw e;
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BigDecimal save(ClienteDTO entity, String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		
		try {
			LogDeMidasInterfaz.log("Entrando a ClienteFacade.save..." + this, Level.FINE, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.STPGRABAR_CLIENTE");
			storedHelper.estableceParametro("pNombre", entity.getNombre());
			storedHelper.estableceParametro("pApellido_Paterno", entity.getApellidoPaterno());
			storedHelper.estableceParametro("pApellido_Materno", entity.getApellidoMaterno());
			storedHelper.estableceParametro("pRFC", (entity.getCodigoRFC() != null?entity.getCodigoRFC().trim() : null));
			storedHelper.estableceParametro("pFecha", entity.getFechaNacimiento());
			storedHelper.estableceParametro("pCodigo_Postal", entity.getCodigoPostal());
			storedHelper.estableceParametro("pCalle_Numero", entity.getNombreCalle() );
			storedHelper.estableceParametro("pid_colonia", entity.getIdColonia());
			storedHelper.estableceParametro("pNom_otra_Colonia", entity.getNombreColonia());
			storedHelper.estableceParametro("pid_Municipio", entity.getIdMunicipio());
			storedHelper.estableceParametro("pid_Estado", entity.getIdEstado());
			storedHelper.estableceParametro("pTelefono", entity.getTelefono());
			storedHelper.estableceParametro("pE_Mail", entity.getEmail());
			storedHelper.estableceParametro("pId_Usuario",nombreUsuario);
			storedHelper.estableceParametro("pTipo_Persona",entity.getClaveTipoPersona());
			storedHelper.estableceParametro("pCURP",entity.getCodigoCURP());
			storedHelper.estableceParametro("pSexo",entity.getSexo());
			storedHelper.estableceParametro("pNacionalidad",entity.getClaveNacionalidad());
			storedHelper.estableceParametro("pIdOcupacion",entity.getClaveOcupacion());
			storedHelper.estableceParametro("pCveEstadoNac",entity.getClaveEstadoNacimiento());
			storedHelper.estableceParametro("pCveCiudadNac",entity.getClaveCiudadNacimiento());
			BigDecimal idCliente = new BigDecimal(storedHelper.ejecutaActualizar());
			LogDeMidasInterfaz.log("Saliendo de ClienteFacade.save..." + this, Level.FINE, null);
			return idCliente;
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"SEYCOS.PKG_INT_MIDAS.STPGRABAR_CLIENTE", entity.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.save..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.save..." + this, Level.SEVERE, e);
			throw e;
		}

	}

	public BigDecimal update(ClienteDTO entity, String nombreUsuario)
			throws Exception {
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<ClienteDTO> buscarPorCURP(ClienteDTO entity,
			String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		
		try {
			LogDeMidasInterfaz.log("Entrando a ClienteFacade.buscarPorCURP..." + this, Level.FINE, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.STP_CONSCLIENTE");

			storedHelper
					.estableceMapeoResultados(
							ClienteDTO.class.getCanonicalName(),
							
							"idCliente," +
							"idDomicilio," +
							"idColoniaString," +
							"idMunicipioString," +
							"idEstadoString," +
							"nombre," +
							"nombreContratante," +
							"apellidoPaterno," +
							"apellidoMaterno," +
							"claveTipoPersona," +							
							"codigoRFC," +
							"nombreCalle," +
							"nombreColonia," +
							"codigoPostal," +
							"nombreDelegacion," +
							"descripcionEstado," +
							"nombreRepresentante," +
							"fechaNacimiento," +							
							"estadoNacimiento,"+
							"sexo,"+
							"codigoCURP,"+ 
							"claveTipoPersona,"+
							"email,"+
							"estadoNacimiento,"+
							"telefono,"+
							"fechaNacimientoRepresentante",
							
							"id_cliente," +
							"id_domicilio," +
							"idcolonia," +
							"idmunicipio," +
							"idestado," +							
							"nombre," +
							"nombre_contratante," +
							"apellido_paterno," +
							"apellido_materno," +
							"TipoPersona," +
							"rfc," +
							"calle_numero," +
							"colonia," +
							"codigo_postal," +
							"ciudad," +
							"estado," +
							"representante_legal," +
							"f_nacimiento," +
							"id_Lugar_Nac," +
							"genero,"+ 
							"curp,"+
							"TipoPersona,"+
							"e_mail,"+
							"Lugar_Nac,"+
							"telef_casa,"+
							"f_constitucion");	
            
			storedHelper.estableceParametro("pCurp", ((entity.getCodigoCURP()==null)?"":entity.getCodigoCURP().toUpperCase()));
			storedHelper.estableceParametro("pTipoPersona", ((entity.getClaveTipoPersona()==null)?"":entity.getClaveTipoPersona()));
			storedHelper.estableceParametro("pid_Cliente", ((entity.getIdCliente()==null)?"":entity.getIdCliente()));
			storedHelper.estableceParametro("pid_Domicilio", ((entity.getIdDomicilio()==null)?"":entity.getIdDomicilio()));
			
			List<ClienteDTO> clienteList = storedHelper.obtieneListaResultados();
			reasignaCliente(clienteList);
			LogDeMidasInterfaz.log("Saliendo de ClienteFacade.buscarPorCURP..." + this, Level.FINE, null);
			return clienteList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.STP_CONSCLIENTE", entity.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.buscarPorCURP..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.buscarPorCURP..." + this, Level.SEVERE, e);
			throw e;
		}	}

		private void reasignaCliente(List<ClienteDTO> clienteList){
			for (ClienteDTO cliente: clienteList){
				Date fechaTemp = null;
				Date fechaTemp2 = null;
				if(cliente.getClaveTipoPersona().intValue() == 2){
					fechaTemp = cliente.getFechaNacimientoRepresentante();
					fechaTemp2 = cliente.getFechaNacimiento();
					cliente.setNombreRepresentante(cliente.getNombre());
					cliente.setNombre(cliente.getNombreContratante());
					cliente.setFechaNacimiento(fechaTemp);					
					cliente.setFechaNacimientoRepresentante(fechaTemp2);

				}
			}
		}

		@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
		public ClienteDTO saveV2(ClienteDTO entity, String nombreUsuario) throws Exception {
			StoredProcedureHelper storedHelper = null;	
			try {
				LogDeMidasInterfaz.log("Entrando a ClienteFacade.save..." + this, Level.FINE, null);
				String idMunicipio = entity.getIdEstadoNacimiento().toString();
				if(idMunicipio!=null){
					idMunicipio = obtenerId(idMunicipio);
				}
				storedHelper = new StoredProcedureHelper(
						"SEYCOS.PKG_INT_MIDAS.STPGRABAR_CLIENTEB");

				storedHelper
				.estableceMapeoResultados(ClienteDTO.class.getCanonicalName(),
						"idCliente," +
						"idDomicilio",
						"id_cliente,"+
						"id_domicilio");
					
				
				storedHelper.estableceParametro("pRazonSocial", entity.getClaveTipoPersona().intValue() == 2?entity.getNombre():"");
				storedHelper.estableceParametro("pNombre", entity.getClaveTipoPersona().intValue() == 2?entity.getNombreRepresentante():entity.getNombre());
				storedHelper.estableceParametro("pApellido_Paterno", entity.getApellidoPaterno());
				storedHelper.estableceParametro("pApellido_Materno", entity.getApellidoMaterno());
				storedHelper.estableceParametro("pGenero", entity.getSexo());
				storedHelper.estableceParametro("pTipo_Persona",entity.getClaveTipoPersona());
				storedHelper.estableceParametro("pRFC", (entity.getCodigoRFC() != null?entity.getCodigoRFC().trim() : null));
				storedHelper.estableceParametro("pFechaNac", entity.getClaveTipoPersona().intValue() == 2?entity.getFechaNacimientoRepresentante():entity.getFechaNacimiento());
				storedHelper.estableceParametro("pLugarNac",idMunicipio);
				storedHelper.estableceParametro("pCurp",entity.getCodigoCURP());
				storedHelper.estableceParametro("pCodigo_Postal", entity.getCodigoPostal());
				storedHelper.estableceParametro("pCalle_Numero", entity.getNombreCalle() );
				storedHelper.estableceParametro("pid_colonia", entity.getIdColonia());
				storedHelper.estableceParametro("pNom_otra_Colonia", entity.getNombreColonia());
				storedHelper.estableceParametro("pid_Municipio", entity.getIdMunicipio());
				storedHelper.estableceParametro("pid_Estado", entity.getIdEstado());				
				storedHelper.estableceParametro("pTelefono", entity.getTelefono());
				storedHelper.estableceParametro("pE_Mail", entity.getEmail());
				storedHelper.estableceParametro("pId_Usuario",nombreUsuario);
				storedHelper.estableceParametro("pid_Cliente", ((entity.getIdCliente()==null)?"":entity.getIdCliente()));
				storedHelper.estableceParametro("pid_Domicilio", ((entity.getIdDomicilio()==null)?"":entity.getIdDomicilio()));
				
				ClienteDTO cliente = (ClienteDTO)storedHelper.obtieneResultadoSencillo();
				
				if(cliente != null){
					if(entity.getClaveTipoPersona().intValue() ==2)
						cliente.setClaveTipoPersona((short)2);
					cliente = (this.buscarPorCURP(cliente, nombreUsuario)).get(0);
					LogDeMidasInterfaz.log("Saliendo de ClienteFacade.save..." + this, Level.FINE, null);
				}
				return cliente;
			} catch (SQLException e) {
				
				Integer codErr = null;
				String descErr = null;
				
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				
				StoredProcedureErrorLog.doLog(nombreUsuario,
						StoredProcedureErrorLog.TipoAccion.GUARDAR,
						"SEYCOS.PKG_INT_MIDAS.STPGRABAR_CLIENTE", entity.getClass(), codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.save..." + this, Level.WARNING, e);
				throw e;
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.save..." + this, Level.SEVERE, e);
				throw e;
			}

		}
		public String obtenerId(String id) {
			return StringUtils.leftPad(id, 5, '0');	
		}

		private void validaCodigoPostalCliente(ClienteGenericoDTO entity, String nombreUsuario){
			StoredProcedureHelper storedHelper = null;
			String sp="SEYCOS.PKG_INT_MIDAS_E2.stp_ValCodigoPostal";
			try {
				LogDeMidasInterfaz.log("Entrando a ClienteFacade.validaCodigoPostalCliente..." + this, Level.FINE, null);					
				storedHelper = new StoredProcedureHelper(sp);

				String nuevaColonia=entity.getNombreColoniaDiferente();
				String nombreColonia=(entity.getNombreColonia()!=null && !entity.getNombreColonia().isEmpty())?entity.getNombreColonia():entity.getNombreColoniaDiferente();
				nombreColonia=(nuevaColonia!=null && !nuevaColonia.isEmpty())?nuevaColonia:nombreColonia;

				storedHelper.estableceParametro("pcodigo_postal",str(entity.getCodigoPostal()));
				storedHelper.estableceParametro("pnom_colonia",str(nombreColonia));
				storedHelper.estableceParametro("pid_estado",entity.getIdEstadoString());
				storedHelper.estableceParametro("pid_ciudad",entity.getIdMunicipioString());

				storedHelper.ejecutaActualizar();
				LogDeMidasInterfaz.log("Saliendo de ClienteFacade.validaCodigoPostalCliente..." + this, Level.FINE, null);
			} catch (SQLException e) {				
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(nombreUsuario,
						StoredProcedureErrorLog.TipoAccion.GUARDAR,
						sp, entity.getClass(), codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.validaCodigoPostalCliente..." + this, Level.WARNING, e);
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.validaCodigoPostalCliente..." + this, Level.SEVERE, e);
			}
		}

		@Override
		public ClienteGenericoDTO saveFullData(ClienteGenericoDTO entity, String nombreUsuario,boolean validaNegocio)throws ClientSystemException,Exception {
			Map<String, Map<String,String>> camposValidados=validateClientFields(entity,validaNegocio);
			List<Map<String,String>> listaErrores=new ArrayList<Map<String,String>>();
			if(camposValidados!=null && !camposValidados.isEmpty() || !validaNegocio){
				SectionType[] tiposSeccion={SectionType.DATOS_GENERALES,SectionType.DATOS_CONTACTO,SectionType.DATOS_FISCALES,SectionType.DATOS_COBRANZA,SectionType.AVISOS_SINIESTROS,SectionType.CANAL_VENTAS};
				for(SectionType seccion:tiposSeccion){
					Map<String,String> errores=camposValidados.get(seccion.getValue());
					//Si no contiene errores en la seccion entonces se manda a guardar la informacion.
					if(errores==null || errores.isEmpty()){
						try{
							switch(seccion){
								case DATOS_GENERALES:
									//Valida codigo postal
									validaCodigoPostalCliente(entity, nombreUsuario);
									//Save
									Integer idCliente=saveGeneralData(entity, nombreUsuario);//Se guardan los datos del usuario
									BigDecimal id=(idCliente!=null)?new BigDecimal(idCliente):null;
									entity.setIdCliente(id);
								break;
								case DATOS_CONTACTO:
									if(entity!=null && entity.getIdCliente()!=null){
										saveContactData(entity, nombreUsuario);
									}
								break;
								case DATOS_FISCALES:
									if(entity!=null && entity.getIdCliente()!=null && entity.getCodigoPostalFiscal()!=null && !entity.getCodigoPostalFiscal().equals("")){
										saveFiscalData(entity, nombreUsuario);
									}
								break;
								case CANAL_VENTAS:
									//TODO guardar en canal de ventas
								break;
							}
						}catch(Exception e){
							if(errores==null){
								errores=new HashMap<String, String>();
							}
							errores.put("exception.unknownError",e.getMessage());
							listaErrores.add(errores);
						}
					}else{
						//Si no esta vacia la lista de errores entonces se muestran en pantalla
						listaErrores.add(errores);
					}
					//Si errores es null significa que la seccion no requiere ningun dato de la forma y entonces 
				}
			}
			if(!listaErrores.isEmpty()){
				Integer idCliente=(entity.getIdCliente()!=null)?entity.getIdCliente().intValue():null;
				throw new ClientSystemException(listaErrores,idCliente);
			}
			return entity;
		}
		/**
		 * 
		 * @param entity
		 * @param nombreUsuario
		 * @return
		 * @throws Exception
		 */
		private Integer saveGeneralData(ClienteGenericoDTO entity,String nombreUsuario)throws Exception{
			StoredProcedureHelper storedHelper = null;
			Integer idCliente=null;
			String sp="SEYCOS.PKG_INT_MIDAS_E2.STP_CATCLIENTES";
			try {
				LogDeMidasInterfaz.log("Entrando a ClienteFacade.saveGeneralData..." + this, Level.FINE, null);
				String idMunicipio = entity.getClaveCiudadNacimiento();
				if(idMunicipio!=null){
					idMunicipio = obtenerId(idMunicipio);
				}
				
				storedHelper = new StoredProcedureHelper(sp);
				//storedHelper.estableceMapeoResultados(ClienteGenericoDTO.class.getCanonicalName(),new String[]{},new String[]{});
				//si no es persona moral, entonces el nombre es el de persona fisica capturado en los datos generales.
				int idTipoPersonaMoral=2;
				String tipoSituacion=entity.getTipoSituacionString();
				tipoSituacion=(tipoSituacion!=null && tipoSituacion.equals("A"))?"01":"02";
				String rama_default="01";
				//Date fechaConstitucion=(entity.getClaveTipoPersona().intValue()==idTipoPersonaMoral)?entity.getFechaNacimiento():null;
				String razonSocial=(entity.getClaveTipoPersona().intValue()==idTipoPersonaMoral)?entity.getRazonSocial():null;
				String nombre=(entity.getClaveTipoPersona().intValue()==idTipoPersonaMoral)?entity.getRazonSocial():entity.getNombre();
				String nuevaColonia=entity.getNombreColoniaDiferente();
				String nombreColonia=(entity.getNombreColonia()!=null && !entity.getNombreColonia().isEmpty())?entity.getNombreColonia():entity.getNombreColoniaDiferente();
				nombreColonia=(nuevaColonia!=null && !nuevaColonia.isEmpty())?nuevaColonia:nombreColonia;
				storedHelper.estableceParametro("ptipo_persona", str((Short)entity.getClaveTipoPersona()));
				storedHelper.estableceParametro("pnombre", str(nombre));
				storedHelper.estableceParametro("papellido_pat", str(entity.getApellidoPaterno()));
				storedHelper.estableceParametro("papellido_mat", str(entity.getApellidoMaterno()));
				storedHelper.estableceParametro("pgenero", str(entity.getSexo()));
				storedHelper.estableceParametro("pRFC", str(entity.getCodigoRFC()));
				storedHelper.estableceParametro("pe_mail", str(entity.getEmail()));
				storedHelper.estableceParametro("pCURP", str(entity.getCodigoCURP()));
				storedHelper.estableceParametro("pTelefono", str(entity.getTelefono()));
				storedHelper.estableceParametro("pExtencion", str(entity.getExtensionContacto()));
				storedHelper.estableceParametro("pEstadoCivil", str(entity.getEstadoCivil()));
				storedHelper.estableceParametro("pFechaNac", str(entity.getFechaNacimiento()));
				storedHelper.estableceParametro("pidpaisNac", str(entity.getClaveNacionalidad()));//*******************
				storedHelper.estableceParametro("pidestadoNac", str(entity.getClaveEstadoNacimiento()));
				storedHelper.estableceParametro("pidCiudadNac", str(idMunicipio));
				storedHelper.estableceParametro("pCve_Actividad_PF", str(tipoSituacion));
				storedHelper.estableceParametro("pCve_Tipo_Sector",str(entity.getClaveSectorFinanciero()));
				storedHelper.estableceParametro("pCve_Rama_PF", rama_default);//TODO falta la rama
				storedHelper.estableceParametro("pcalle_numero", str(entity.getNombreCalle()));
				storedHelper.estableceParametro("pcodigo_postal",str(entity.getCodigoPostal()));
				storedHelper.estableceParametro("pnom_colonia",str(nombreColonia));
				storedHelper.estableceParametro("pNomRazonSocial",str(razonSocial));
				storedHelper.estableceParametro("PFConsitucion",str(entity.getFechaConstitucion()));
				storedHelper.estableceParametro("pCve_Rama_Activ", "");//TODO falta rama activ
				storedHelper.estableceParametro("pCve_sub_rama_ac", "");
				storedHelper.estableceParametro("pid_sector", str(entity.getClaveSectorFinanciero()));
				storedHelper.estableceParametro("pid_giro", str(entity.getIdGiro()));
				storedHelper.estableceParametro("pidrepresentante",str(entity.getIdRepresentante()));
				storedHelper.estableceParametro("pid_cliente", str(entity.getIdCliente()));
				storedHelper.estableceParametro("pSituacion", str(entity.getTipoSituacionString()));
				idCliente=storedHelper.ejecutaActualizar();
				entity.setIdCliente(new BigDecimal(idCliente));
				entity.setIdClienteString(String.valueOf(idCliente));
				LogDeMidasInterfaz.log("Saliendo de ClienteFacade.saveGeneralData..." + this, Level.FINE, null);
			} catch (SQLException e) {				
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(nombreUsuario,
						StoredProcedureErrorLog.TipoAccion.GUARDAR,
						sp, entity.getClass(), codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.saveGeneralData..." + this, Level.WARNING, e);
				throw e;
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.saveGeneralData..." + this, Level.SEVERE, e);
				throw e;
			}
			return idCliente;
		}
		
		private Object str(Object str){
			return (str!=null)?str:"";
		}
		
		/**
		 * Metodo que guarda los datos de contacto de un cliente
		 * @param entity
		 * @param nombreUsuario
		 * @throws Exception
		 */
		private void saveContactData(ClienteGenericoDTO entity,String nombreUsuario)throws Exception{
			StoredProcedureHelper storedHelper = null;	
			String sp="SEYCOS.PKG_INT_MIDAS_E2.STP_CATCONTACTOS";
			try {
				LogDeMidasInterfaz.log("Entrando a ClienteFacade.saveContactData..." + this, Level.FINE, null);
				storedHelper = new StoredProcedureHelper(sp);

				//storedHelper.estableceMapeoResultados(ClienteGenericoDTO.class.getCanonicalName(),new String[]{},new String[]{});
				//si no es persona moral, entonces el nombre es el de persona fisica capturado en los datos generales.
				int idTipoPersonaMoral=2;
				String nombreContacto=(entity.getClaveTipoPersona().intValue()==idTipoPersonaMoral)?entity.getNombreContacto():entity.getNombre();
				String puestoContacto=(entity.getClaveTipoPersona().intValue()==idTipoPersonaMoral)?entity.getPuestoContacto():entity.getDescripcionOcupacion();
				storedHelper.estableceParametro("pid_cliente", str(entity.getIdCliente()));
				storedHelper.estableceParametro("pnombre_contacto", nombreContacto);
				storedHelper.estableceParametro("ppuesto_contacto", puestoContacto);
				storedHelper.estableceParametro("ptel_casa", str(entity.getTelefono()));
				storedHelper.estableceParametro("ptel_oficina", str(entity.getTelefonoOficinaContacto()));
				storedHelper.estableceParametro("ptel_celular",str(entity.getCelularContacto()));
				storedHelper.estableceParametro("ptel_fax", str(entity.getFaxContacto()));
				storedHelper.estableceParametro("pcorreo_electronico", str(entity.getEmail()));
				storedHelper.estableceParametro("pfacebook",str(entity.getFacebookContacto()));
				storedHelper.estableceParametro("ptwitter",str(entity.getTwitterContacto()));
				storedHelper.estableceParametro("ppagina_web",str(entity.getPaginaWebContacto()));
				storedHelper.estableceParametro("ptels_adicionales",str(entity.getTelefonosAdicionalesContacto()));
				storedHelper.estableceParametro("pcorreos_adicionales", str(entity.getCorreosAdicionalesContacto()));
				storedHelper.ejecutaActualizar();
				LogDeMidasInterfaz.log("Saliendo de ClienteFacade.saveContactData..." + this, Level.FINE, null);
			} catch (SQLException e) {				
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(nombreUsuario,
						StoredProcedureErrorLog.TipoAccion.GUARDAR,
						sp, entity.getClass(), codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.saveContactData..." + this, Level.WARNING, e);
				throw e;
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.saveContactData..." + this, Level.SEVERE, e);
				throw e;
			}
		}
		/**
		 * Metodo para guardar los datos fiscales del cliente
		 * @param entity
		 * @param nombreUsuario
		 * @throws Exception
		 */
		private void saveFiscalData(ClienteGenericoDTO entity,String nombreUsuario)throws Exception{
			StoredProcedureHelper storedHelper = null;	
			String sp="SEYCOS.PKG_INT_MIDAS_E2.STP_CATFISCALES";
			try {
				LogDeMidasInterfaz.log("Entrando a ClienteFacade.saveFiscalData..." + this, Level.FINE, null);
				storedHelper = new StoredProcedureHelper(sp);
				String nuevaColonia=entity.getNombreColoniaDiferenteFiscal();
				String nombreColonia=(entity.getNombreColoniaFiscal()!=null && !entity.getNombreColoniaFiscal().isEmpty())?entity.getNombreColoniaFiscal():entity.getNombreColoniaDiferenteFiscal();
				nombreColonia=(nuevaColonia!=null && !nuevaColonia.isEmpty())?nuevaColonia:nombreColonia;
				//Si no selecciono ninguna persona fisica o moral, entonecs la persona de facturacion es el mismo cliente registrado.
				String idPersonaFiscal=(entity.getIdPersonaFiscal()!=null && !entity.getIdPersonaFiscal().isEmpty())?entity.getIdPersonaFiscal():entity.getIdCliente().toString();
				//storedHelper.estableceMapeoResultados(ClienteGenericoDTO.class.getCanonicalName(),new String[]{},new String[]{});
				storedHelper.estableceParametro("pid_cliente", str(entity.getIdCliente()));
				storedHelper.estableceParametro("pid_cliente_fact", str(idPersonaFiscal));
				storedHelper.estableceParametro("pcalle_numero", str(entity.getNombreCalleFiscal()));
				storedHelper.estableceParametro("pcodigo_postal", str(entity.getCodigoPostalFiscal()));
				storedHelper.estableceParametro("pnom_colonia", str(nombreColonia));
				storedHelper.ejecutaActualizar();
				LogDeMidasInterfaz.log("Saliendo de ClienteFacade.saveFiscalData..." + this, Level.FINE, null);
			} catch (SQLException e) {				
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(nombreUsuario,
						StoredProcedureErrorLog.TipoAccion.GUARDAR,
						sp, entity.getClass(), codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.saveFiscalData..." + this, Level.WARNING, e);
				throw e;
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.saveFiscalData..." + this, Level.SEVERE, e);
				throw e;
			}
		}
		/**
		 * Se guardan los datos de cobranza
		 * @param entity
		 * @param nombreUsuario
		 * @throws Exception
		 */
		public Long guardarDatosCobranza(ClienteGenericoDTO entity,String nombreUsuario, boolean validaNegocio) throws Exception{
			LogDeMidasInterfaz.log("ClienteFacade.guardarDatosCobranza ... entrando " + this, Level.FINE, null);
			Long idConducto = null;
			if(entity.getIdTipoTarjetaCobranza()!=null){
				if(entity.getMesFechaVencimiento() != null && entity.getAnioFechaVencimiento() != null){
					String fechaVencimiento=entity.getMesFechaVencimiento().toString().length()<2 ?
							"0"+entity.getMesFechaVencimiento().toString():entity.getMesFechaVencimiento().toString();
					fechaVencimiento+=entity.getAnioFechaVencimiento();
					entity.setFechaVencimientoTarjetaCobranza(fechaVencimiento);				
				}
			}
			Map<String, Map<String,String>> camposValidados = null;
			if (entity != null && entity.getIdNegocio()!=null && entity.getIdNegocio() != -1l) {
				camposValidados = validateClientFields(entity,validaNegocio);
			}else{
				if (entity != null) {
					LogDeMidasInterfaz.log("ClienteFacade.guardarDatosCobranza ... antes de saveDataCharge 1" + this, Level.FINE, null);
					return saveDataCharge(entity, nombreUsuario);					
				}
			}
			List<Map<String,String>> listaErrores = new ArrayList<Map<String,String>>();
			if(entity == null || entity.getIdCliente()== null){
				throw new Exception("Favor de capturar los datos generales antes de hacer cualquier otro movimiento");
			}
			if(camposValidados!=null && !camposValidados.isEmpty()){
				Map<String,String> errores=camposValidados.get(SectionType.DATOS_COBRANZA.getValue());
				//Si no contiene errores en la seccion entonces se manda a guardar la informacion.
				if(errores==null || errores.isEmpty()){
					try{
						LogDeMidasInterfaz.log("ClienteFacade.guardarDatosCobranza ... antes de saveDataCharge 2" + this, Level.FINE, null);
						idConducto = saveDataCharge(entity, nombreUsuario);
					}catch(Exception e){
						if(errores==null){
							errores=new HashMap<String, String>(1);
						}
						errores.put("anotherError",e.getMessage());
						listaErrores.add(errores);
					}
				}else{
					//Si no esta vacia la lista de errores entonces se muestran en pantalla
					listaErrores.add(errores);
					//Si errores es null significa que la seccion no requiere ningun dato de la forma y entonces
				} 
			}
			if(!listaErrores.isEmpty()){
				throw new ClientSystemException(listaErrores);
			}
			return idConducto;
		}
		
		
		/**
		 * Este metodo se manda a llamar por separado ya que tiene su propia alta,baja y cambio.
		 * @param entity
		 * @param nombreUsuario
		 * @throws Exception
		 */
		private Long saveDataCharge(ClienteGenericoDTO entity,String nombreUsuario)throws Exception{		
			Long idConducto = null;
			StoredProcedureHelper storedHelper = null;	
			String sp="SEYCOS.PKG_INT_MIDAS_E2.STP_CATCOBRANZA";
			try {
				LogDeMidasInterfaz.log("Entrando a ClienteFacade.guardarDatosCobranza..." + this, Level.FINE, null);
				storedHelper = new StoredProcedureHelper(sp);				
				storedHelper.estableceParametro("pid_cliente", str(entity.getIdCliente()));
				storedHelper.estableceParametro("pid_conducto", str(entity.getIdConductoCobranza()));
				storedHelper.estableceParametro("pnom_titular", str(entity.getNombreTarjetaHabienteCobranza()));
				storedHelper.estableceParametro("pcorreo_electronico", str(entity.getEmailCobranza()));
				storedHelper.estableceParametro("pTelefono", str(entity.getTelefonoCobranza()));
				storedHelper.estableceParametro("pcalle_numero", str(entity.getNombreCalleCobranza()));
				storedHelper.estableceParametro("pcodigo_postal", str(entity.getCodigoPostalCobranza()));
				storedHelper.estableceParametro("pnom_colonia", str(entity.getNombreColoniaCobranza()));
				storedHelper.estableceParametro("pid_cond_cobro", str(entity.getIdTipoConductoCobro()));//TODO que es aqui?
				storedHelper.estableceParametro("pidBanco", str(entity.getIdBancoCobranza()));
				storedHelper.estableceParametro("ptipo_cuenta", str(entity.getIdTipoTarjetaCobranza()));//TODO que es aqui?				
				storedHelper.estableceParametro("pnum_cuenta", str(entity.getNumeroTarjetaCobranza()));
				storedHelper.estableceParametro("pCodigo", str(entity.getCodigoSeguridadCobranza()));
				storedHelper.estableceParametro("pFechaVenc", str(entity.getFechaVencimientoTarjetaCobranza()));
				storedHelper.estableceParametro("pTipoPromo", str(entity.getTipoPromocion()));
				storedHelper.estableceParametro("pDiasPago", str(entity.getDiaPagoTarjetaCobranza()));				
				storedHelper.estableceParametro("pRFC", str(entity.getRfcCobranza()));
				idConducto = Long.valueOf(storedHelper.ejecutaActualizar());
				LogDeMidasInterfaz.log("Saliendo de ClienteFacade.guardarDatosCobranza... idConducto:"+idConducto+" " + this, Level.FINE, null);
			} catch (SQLException e) {				
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(nombreUsuario,
						StoredProcedureErrorLog.TipoAccion.GUARDAR,
						sp, entity.getClass(), codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.guardarDatosCobranza..." + this, Level.WARNING, e);
				if(codErr != null && codErr.intValue() > 0 && codErr.intValue() < 100){
					throw new RuntimeException(descErr);
				}else{
					throw e;
				}
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.guardarDatosCobranza..." + this, Level.SEVERE, e);
				throw e;
			}
			return idConducto;
		}
		/**
		 * Metodo para validar y guardar los datos de aviso en caso de siniestro
		 * @param entity
		 * @param nombreUsuario
		 * @throws Exception
		 */
		public void guardarDatosAvisoSiniestro(ClienteGenericoDTO entity,String nombreUsuario, boolean validaNegocio) throws Exception{
			Map<String, Map<String,String>> camposValidados=validateClientFields(entity,validaNegocio);
			List<Map<String,String>> listaErrores=new ArrayList<Map<String,String>>();
			if(entity==null || entity.getIdCliente()==null){
				throw new Exception("Favor de capturar los datos generales antes de hacer cualquier otro movimiento");
			}
			if(camposValidados!=null && !camposValidados.isEmpty()){
				Map<String,String> errores=camposValidados.get(SectionType.AVISOS_SINIESTROS.getValue());
				//Si no contiene errores en la seccion entonces se manda a guardar la informacion.
				if(errores==null || errores.isEmpty()){
					try{
						saveDataInSinisterCase(entity, nombreUsuario);
					}catch(Exception e){
						if(errores==null){
							errores=new HashMap<String, String>();
						}
						errores.put("anotherError",e.getMessage());
						listaErrores.add(errores);
					}
				}else{//Si no esta vacia la lista de errores entonces se muestran en pantalla
					listaErrores.add(errores);
				}//Si errores es null significa que la seccion no requiere ningun dato de la forma y entonces 
			}
			if(!listaErrores.isEmpty()){
				throw new ClientSystemException(listaErrores);
			}
		}
		
		private void saveDataInSinisterCase(ClienteGenericoDTO entity,String nombreUsuario)throws Exception{
			StoredProcedureHelper storedHelper = null;	
			String sp="SEYCOS.PKG_INT_MIDAS_E2.STP_CATAVISOSIN";
			try {
				LogDeMidasInterfaz.log("Entrando a ClienteFacade.guadarDatosAvisoSiniestro..." + this, Level.FINE, null);
				storedHelper = new StoredProcedureHelper(sp);
				
				storedHelper.estableceParametro("pid_cliente", str(entity.getIdCliente()));
				storedHelper.estableceParametro("pParentesco", str(entity.getParentescoAviso()));
				storedHelper.estableceParametro("pnombre", str(entity.getNombreAviso()));
				storedHelper.estableceParametro("pcorreo_electronico", str(entity.getCorreoElectronicoAviso()));
				storedHelper.estableceParametro("pTel_casa", str(entity.getTelefonoCasaAviso()));
				storedHelper.estableceParametro("pTel_Oficina", str(entity.getTelefonoOficinaAviso()));
				storedHelper.estableceParametro("pTel_Celular", str(entity.getCelularAviso()));
				storedHelper.ejecutaActualizar();
				LogDeMidasInterfaz.log("Saliendo de ClienteFacade.guadarDatosAvisoSiniestro..." + this, Level.FINE, null);
			} catch (SQLException e) {				
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(nombreUsuario,
						StoredProcedureErrorLog.TipoAccion.GUARDAR,
						sp, entity.getClass(), codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.guadarDatosAvisoSiniestro..." + this, Level.WARNING, e);
				throw e;
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.guadarDatosAvisoSiniestro..." + this, Level.SEVERE, e);
				throw e;
			}
		}
		/**
		 * Se validan los datos de un cliente de acuerdo a su negocio y tipo de persona.
		 */
		@Override
		public Map<String, Map<String,String>> validateClientFields(ClienteGenericoDTO entity, boolean validaNegocio) throws Exception {
			Map<String, Map<String,String>> errors=new HashMap<String, Map<String,String>>();
			if(entity==null){
				//TODO mensaje de error
				onError("Favor de proporcionar el cliente");
			}
			Long idNegocio=entity.getIdNegocio();
			idNegocio=(idNegocio!=null)?idNegocio:0L;
			Short tipoPersona=entity.getClaveTipoPersona();
			if(tipoPersona==null){
				onError("Favor de proporcionar el tipo de persona");
			}
			if(validaNegocio){
				if(idNegocio!=null){
					List<ConfiguracionGrupo> validaciones=configuracionSeccionPorNegocioService.getCamposRequeridos(idNegocio, tipoPersona);
					if(validaciones!=null && !validaciones.isEmpty()){
						List<ConfiguracionCampo> campos=new ArrayList<ConfiguracionCampo>();
						//De las validaciones de todas las secciones, se agregan los campos a validar en una sola lista.
						for(ConfiguracionGrupo grupo:validaciones){
							campos.addAll(grupo.getConfiguracionCampos());
						}
						errors=ClientBeanValidatorProcessor.validarClienteGenerico(entity, campos);
					}
				}else{
					LogDeMidasInterfaz.log("No se encontró una linea de negocio en el cliente, por lo tanto no se podrá cargar las validaciones del cliente..." + this, Level.FINE, null);
				}
			}
			return errors;
		}
		/**
		 * Obtiene los campos requeridos segun la configuracion de la base de datos y del objeto cliente de los atributos mapeados.
		 * @param entity
		 * @return
		 */
		public List<String> obtenerCamposRequeridos(ClienteGenericoDTO entity)throws Exception{
			if(entity==null || entity.getClaveTipoPersona()==null){
				throw new Exception("Favor de especificar el tipo de persona del cliente.");
			}
			List<String> camposObligatorios=new ArrayList<String>();
			Long idNegocio=(entity.getIdNegocio()!=null)?entity.getIdNegocio():0L;
			entity.setIdNegocio(idNegocio);
			if(idNegocio!=null){
				List<ConfiguracionGrupo> validaciones=configuracionSeccionPorNegocioService.getCamposRequeridos(idNegocio,entity.getClaveTipoPersona());
				if(validaciones!=null && !validaciones.isEmpty()){
					List<ConfiguracionCampo> campos=new ArrayList<ConfiguracionCampo>();
					//De las validaciones de todas las secciones, se agregan los campos a validar en una sola lista.
					for(ConfiguracionGrupo grupo:validaciones){
						campos.addAll(grupo.getConfiguracionCampos());
					}
					if(!campos.isEmpty()){
						camposObligatorios=ClientBeanValidatorProcessor.obtenerCamposObligatorios(entity, campos);
					}
				} 
			}
			return camposObligatorios;
		}
		
		public List<ClienteGenericoDTO> findByFiltersNoAddress(ClienteGenericoDTO filtro,String usuario) throws Exception{
			List<ClienteGenericoDTO> clientes=new ArrayList<ClienteGenericoDTO>();
			if(filtro!=null){
				StoredProcedureHelper storedHelper = null;
				try {
					LogDeMidasInterfaz.log("Entrando a ClienteFacade.findByFilters..." + this, Level.FINE, null);
					storedHelper = new StoredProcedureHelper("SEYCOS.PKG_INT_MIDAS_E2.CCLIGRALFILTRONODETALLADA");
					String [] atributosDTO = {
							"idCliente","idToPersona","nombreCompleto","claveTipoPersona", "nombre", "apellidoPaterno",
							"apellidoMaterno","sexo","codigoRFC","email","codigoCURP","telefono","extensionContacto","estadoCivil", 
							"fechaNacimiento","claveNacionalidad", "claveEstadoNacimiento","claveCiudadNacimiento",
							"tipoSituacionString", "claveSectorFinanciero", "razonSocial", "fechaConstitucion", 
							"claveSectorFinanciero","idGiro","idRepresentante","nombreContacto","puestoContacto",
							"telefonoOficinaContacto","celularContacto","faxContacto",
							"facebookContacto","twitterContacto","paginaWebContacto","telefonosAdicionalesContacto",
							"correosAdicionalesContacto","nombreFiscal","apellidoPaternoFiscal","apellidoMaternoFiscal",
							"codigoRFCFiscal","telefonoFiscal","razonSocialFiscal","fechaNacimientoFiscal"
					};
					
					String [] columnasResulset = {
							"idCliente","idToPersona","nombreCompleto","claveTipoPersona", "nombre", "apellidoPaterno",
							"apellidoMaterno","genero","codigoRFC","email","curp","telefono","extensionContacto","estadoCivil", 
							"fechaNacimiento","claveNacionalidad", "claveEstadoNacimiento","claveCiudadNacimiento",
							"tipoSituacion", "claveSectorFinanciero", "razonSocial", "fechaConstitucion", 
							"idSector","idGiro", "idRepresentante","nombreContacto","puestoContacto",
							"telefonoOficinaContacto","celularContacto","faxContacto",
							"facebookContacto","twitterContacto","paginaWebContacto","telefonosAdicionalesContacto",
							"correosAdicionalesContacto","nombreFiscal","apellidoPaternoFiscal","apellidoMaternoFiscal",
							"codigoRFCFiscal","telefonoFiscal","razonSocialFiscal","fechaNacimientoFiscal"
					};
					Object fechaConstitucion=(filtro.getFechaConstitucion()!=null)?filtro.getFechaConstitucion():filtro.getFechaConstitucionString();
					storedHelper.estableceMapeoResultados(ClienteGenericoDTO.class.getCanonicalName(),atributosDTO,columnasResulset);
					storedHelper.estableceParametro("pId_Cliente", str(filtro.getIdCliente()));
					storedHelper.estableceParametro("pnombre", str(filtro.getNombre()));
					storedHelper.estableceParametro("papellidoPaterno", str(filtro.getApellidoPaterno()));
					storedHelper.estableceParametro("papellidoMaterno", str(filtro.getApellidoMaterno()));
					storedHelper.estableceParametro("prazonSocial", str(filtro.getRazonSocial()));
					storedHelper.estableceParametro("pcve_per_juridica", str(filtro.getClaveTipoPersonaString()));
					storedHelper.estableceParametro("pRFC", str(filtro.getCodigoRFC()));
					storedHelper.estableceParametro("pcurp", str(filtro.getCodigoCURP()));
					storedHelper.estableceParametro("prepresentanteLegal", str(filtro.getIdRepresentante()));
					storedHelper.estableceParametro("pFechaConstitucion", str(fechaConstitucion));
					storedHelper.estableceParametro("pSituacion", str(filtro.getTipoSituacionString()));
					clientes = storedHelper.obtieneListaResultadosHerencia();
					LogDeMidasInterfaz.log("Saliendo de ClienteFacade.findByFilters..." + this, Level.FINE, null);
				} catch (SQLException e) {
					Integer codErr = null;
					String descErr = null;
					if (storedHelper != null) {
						codErr = storedHelper.getCodigoRespuesta();
						descErr = storedHelper.getDescripcionRespuesta();
					}
					StoredProcedureErrorLog.doLog(usuario,StoredProcedureErrorLog.TipoAccion.BUSCAR,"SEYCOS.PKG_INT_MIDAS.CDETALLE_CLIENTE", ClienteDTO.class, codErr, descErr);
					LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.findById..." + this, Level.WARNING, e);
					throw e;
				} catch (Exception e) {
					LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.findById..." + this, Level.SEVERE, e);
					throw e;
				}
			}
			return clientes;
		}
		@Override
		public List<ClienteGenericoDTO> findByFilters(ClienteGenericoDTO filtro,String usuario) throws Exception{
			List<ClienteGenericoDTO> clientes=new ArrayList<ClienteGenericoDTO>();
			if(filtro!=null){
				StoredProcedureHelper storedHelper = null;
				try {
					LogDeMidasInterfaz.log("Entrando a ClienteFacade.findByFilters..." + this, Level.FINE, null);
					storedHelper = new StoredProcedureHelper("SEYCOS.PKG_INT_MIDAS_E2.CCLIGRALFILTRO");
					String [] atributosDTO = {
							"idCliente","idToPersona","nombreCompleto","claveTipoPersona", "nombre", "apellidoPaterno",
							"apellidoMaterno","sexo","codigoRFC","email","codigoCURP","telefono","extensionContacto","estadoCivil", 
							"fechaNacimiento","claveNacionalidad", "claveEstadoNacimiento","claveCiudadNacimiento",
							"tipoSituacionString", "claveSectorFinanciero", "razonSocial", "fechaConstitucion", 
							"claveSectorFinanciero","idGiro","idRepresentante","nombreContacto","puestoContacto",
							"telefonoOficinaContacto","celularContacto","faxContacto",
							"facebookContacto","twitterContacto","paginaWebContacto","telefonosAdicionalesContacto",
							"correosAdicionalesContacto","nombreFiscal","apellidoPaternoFiscal","apellidoPaternoFiscal",
							"idDomicilioConsulta","nombreCalleFiscal","codigoPostalFiscal","nombreColoniaFiscal","idMunicipioFiscal","idEstadoFiscal",
							"codigoRFCFiscal","telefonoFiscal","razonSocialFiscal","fechaNacimientoFiscal","nombreCalle",
							"codigoPostal","nombreColonia","idMunicipioString","idEstadoString","idPaisString","idColoniaString","idColoniaStringDom" 
					};
					
					String [] columnasResulset = {
							"idCliente","idToPersona","nombreCompleto","claveTipoPersona", "nombre", "apellidoPaterno",
							"apellidoMaterno","genero","codigoRFC","email","curp","telefono","extensionContacto","estadoCivil", 
							"fechaNacimiento","claveNacionalidad", "claveEstadoNacimiento","claveCiudadNacimiento",
							"tipoSituacion", "claveSectorFinanciero", "razonSocial", "fechaConstitucion", 
							"idSector","idGiro", "idRepresentante","nombreContacto","puestoContacto",
							"telefonoOficinaContacto","celularContacto","faxContacto",
							"facebookContacto","twitterContacto","paginaWebContacto","telefonosAdicionalesContacto",
							"correosAdicionalesContacto","nombreFiscal","apellidoPaternoFiscal","apellidoPaternoFiscal",
							"idDomicilioConsulta","nombreCalleFiscal","codigoPostalFiscal","nombreColoniaFiscal","idMunicipioFiscal","idEstadoFiscal",
							"codigoRFCFiscal","telefonoFiscal","razonSocialFiscal","fechaNacimientoFiscal","nombreCalle",
							"codigoPostal","nombreColonia","idMunicipioString","idEstadoString","idPaisString","idColoniaString","idColoniaStringDom" 
					};
					Object fechaConstitucion=(filtro.getFechaConstitucion()!=null)?filtro.getFechaConstitucion():filtro.getFechaConstitucionString();
					storedHelper.estableceMapeoResultados(ClienteGenericoDTO.class.getCanonicalName(),atributosDTO,columnasResulset);
					storedHelper.estableceParametro("pId_Cliente", str(filtro.getIdCliente()));
					storedHelper.estableceParametro("pnombre", str(filtro.getNombre()));
					storedHelper.estableceParametro("papellidoPaterno", str(filtro.getApellidoPaterno()));
					storedHelper.estableceParametro("papellidoMaterno", str(filtro.getApellidoMaterno()));
					storedHelper.estableceParametro("prazonSocial", str(filtro.getRazonSocial()));
					storedHelper.estableceParametro("pcve_per_juridica", str(filtro.getClaveTipoPersonaString()));
					storedHelper.estableceParametro("pRFC", str(filtro.getCodigoRFC()));
					storedHelper.estableceParametro("pcurp", str(filtro.getCodigoCURP()));
					storedHelper.estableceParametro("prepresentanteLegal", str(filtro.getIdRepresentante()));
					storedHelper.estableceParametro("pFechaConstitucion", str(fechaConstitucion));
					storedHelper.estableceParametro("pSituacion", str(filtro.getTipoSituacionString()));
					clientes = storedHelper.obtieneListaResultadosHerencia();
					LogDeMidasInterfaz.log("Saliendo de ClienteFacade.findByFilters..." + this, Level.FINE, null);
				} catch (SQLException e) {
					Integer codErr = null;
					String descErr = null;
					if (storedHelper != null) {
						codErr = storedHelper.getCodigoRespuesta();
						descErr = storedHelper.getDescripcionRespuesta();
					}
					StoredProcedureErrorLog.doLog(usuario,StoredProcedureErrorLog.TipoAccion.BUSCAR,"SEYCOS.PKG_INT_MIDAS.CDETALLE_CLIENTE", ClienteDTO.class, codErr, descErr);
					LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.findById..." + this, Level.WARNING, e);
					throw e;
				} catch (Exception e) {
					LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.findById..." + this, Level.SEVERE, e);
					throw e;
				}
			}
			return clientes;
		}
		
		/** Obtiene los datos de un cliente ya sea por su Id de Cliente o por su RFC
		 * @param filtro
		 * @return
		 * @throws Exception 
		 */
		@SuppressWarnings("unchecked")
		@Override
		public List<ClienteGenericoDTO> findByIdRFC(ClienteGenericoDTO filtro, String usuario) throws Exception {
			List<ClienteGenericoDTO> clientes=new ArrayList<ClienteGenericoDTO>();
			if(filtro!=null){
				StoredProcedureHelper storedHelper = null;
				try {
					storedHelper = new StoredProcedureHelper("SEYCOS.PKG_INT_MIDAS_E2.cCliPorIdRFC");
					String [] atributosDTO = {
							"idCliente","idToPersona","nombreCompleto","claveTipoPersona", "nombre", "apellidoPaterno",
							"apellidoMaterno","sexo","codigoRFC","email","codigoCURP","telefono","extensionContacto","estadoCivil", 
							"fechaNacimiento","claveNacionalidad", "claveEstadoNacimiento","claveCiudadNacimiento",
							"tipoSituacionString", "claveSectorFinanciero", "razonSocial", "fechaConstitucion", 
							"claveSectorFinanciero","idGiro","idRepresentante","nombreContacto","puestoContacto",
							"telefonoOficinaContacto","celularContacto","faxContacto",
							"facebookContacto","twitterContacto","paginaWebContacto","telefonosAdicionalesContacto",
							"correosAdicionalesContacto","nombreFiscal","apellidoPaternoFiscal","apellidoPaternoFiscal",
							"idDomicilioConsulta","nombreCalleFiscal","codigoPostalFiscal","nombreColoniaFiscal","idMunicipioFiscal","idEstadoFiscal",
							"codigoRFCFiscal","telefonoFiscal","razonSocialFiscal","fechaNacimientoFiscal","nombreCalle",
							"codigoPostal","nombreColonia","idMunicipioString","idEstadoString","idPaisString" 
					};
					
					String [] columnasResulset = {
							"idCliente","idToPersona","nombreCompleto","claveTipoPersona", "nombre", "apellidoPaterno",
							"apellidoMaterno","genero","codigoRFC","email","curp","telefono","extensionContacto","estadoCivil", 
							"fechaNacimiento","claveNacionalidad", "claveEstadoNacimiento","claveCiudadNacimiento",
							"tipoSituacion", "claveSectorFinanciero", "razonSocial", "fechaConstitucion", 
							"idSector","idGiro", "idRepresentante","nombreContacto","puestoContacto",
							"telefonoOficinaContacto","celularContacto","faxContacto",
							"facebookContacto","twitterContacto","paginaWebContacto","telefonosAdicionalesContacto",
							"correosAdicionalesContacto","nombreFiscal","apellidoPaternoFiscal","apellidoPaternoFiscal",
							"idDomicilioConsulta","nombreCalleFiscal","codigoPostalFiscal","nombreColoniaFiscal","idMunicipioFiscal","idEstadoFiscal",
							"codigoRFCFiscal","telefonoFiscal","razonSocialFiscal","fechaNacimientoFiscal","nombreCalle",
							"codigoPostal","nombreColonia","idMunicipioString","idEstadoString","idPaisString" 
					};
					storedHelper.estableceMapeoResultados(ClienteGenericoDTO.class.getCanonicalName(),atributosDTO,columnasResulset);
					storedHelper.estableceParametro("pId_Cliente", str(filtro.getIdCliente()));
					storedHelper.estableceParametro("pRFC", str(filtro.getCodigoRFC()));
					clientes = storedHelper.obtieneListaResultadosHerencia();
					
				} catch (SQLException e) {
					Integer codErr = null;
					String descErr = null;
					if (storedHelper != null) {
						codErr = storedHelper.getCodigoRespuesta();
						descErr = storedHelper.getDescripcionRespuesta();
					}
					StoredProcedureErrorLog.doLog(usuario,StoredProcedureErrorLog.TipoAccion.BUSCAR,"SEYCOS.PKG_INT_MIDAS_E2.cCliPorIdRFC", ClienteDTO.class, codErr, descErr);
					throw e;
				} catch (Exception e) {
					LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.findByIdRFC..." + this, Level.SEVERE, e);
					throw e;
				}
			}
			return clientes;
		}
		
		
		/**
		 * Obtiene los datos de un cliente sin considerar los datos de domicilio
		 * @param filtro
		 * @return
		 * @throws Exception
		 */
		@Override
		public ClienteGenericoDTO loadByIdNoAddress(ClienteGenericoDTO filtro) throws Exception{
			ClienteGenericoDTO dto=null;
			if(filtro==null || filtro.getIdCliente()==null){
				onError("IdCliente is null!");
			}
			List<ClienteGenericoDTO> list=findByFiltersNoAddress(filtro, null);
			if(list!=null && !list.isEmpty()){
				dto=list.get(0);
			}
			return dto;
		}
		/**
		 * Obtiene los datos de un cliente.
		 * @param filtro
		 * @return
		 * @throws Exception
		 */
		public ClienteGenericoDTO loadById(ClienteGenericoDTO filtro) throws Exception{
			if(filtro==null || filtro.getIdCliente()==null){
				onError("IdCliente is null!");
			}
			List<ClienteGenericoDTO> list=findByFilters(filtro, null);
			if(list!=null && !list.isEmpty()){
				filtro=list.get(0);
				setTelefono(filtro);
			}
			return filtro;
		}
		
		private void setTelefono(ClienteGenericoDTO cliente){
			String separadorTel = "-";
			//Set Clave
			String claveTelefono = cliente.getClaveTelefono();
			String telefono = cliente.getTelefono();
			try{
				if(claveTelefono == null && telefono != null){
					String[] telStr = telefono.split(separadorTel);
					if(telStr.length == 3){
						claveTelefono = telStr[0].replaceAll("\\D+", "");
						cliente.setClaveTelefono(claveTelefono);
					}else{
						cliente.setClaveTelefono("0");
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			//Set Clave2
			String clave2Telefono = cliente.getClave2Telefono();
			try{
				if(clave2Telefono == null && telefono != null){
					String[] telStr = telefono.split(separadorTel);
					if(telStr.length == 3){
						clave2Telefono = telStr[1].replaceAll("\\D+", "");
						cliente.setClave2Telefono(clave2Telefono);
					}else{
						cliente.setClave2Telefono("0");
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			//Set numero telefono
			String numeroTelefono = cliente.getNumeroTelefono();
			try{
				if(numeroTelefono == null && telefono != null){
					String[] telStr = telefono.split(separadorTel);
					if(telStr.length == 3){
						numeroTelefono = telStr[2].replaceAll("\\D+", "");
						cliente.setNumeroTelefono(numeroTelefono);
					}else{
						numeroTelefono = telefono.replaceAll("\\D+", "");
						cliente.setNumeroTelefono(numeroTelefono);
					}
				}
			}catch(Exception e){
//				e.printStackTrace();
				LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.setTelefono..." + this, Level.SEVERE, e);
			}				
		}
		/**
		 * Lista de medios de pago de un cliente.
		 * @param cliente
		 * @param usuario
		 * @return
		 * @throws Exception 
		 */
		@Override
		public List<ClienteGenericoDTO> loadMediosPagoPorCliente(ClienteGenericoDTO cliente,String usuario) throws Exception{
			List<ClienteGenericoDTO> clientes=new ArrayList<ClienteGenericoDTO>();
			if(cliente!=null){
				StoredProcedureHelper storedHelper = null;
				try {
					LogDeMidasInterfaz.log("Entrando a ClienteFacade.loadMediosPagoPorCliente..." + this, Level.FINE, null);
					storedHelper = new StoredProcedureHelper("SEYCOS.PKG_INT_MIDAS_E2.CCLICOB");
					String [] atributosDTO = {
							"idCliente", "idConductoCobranza", 
							"idVersionConducto", "nombreTarjetaHabienteCobranza",
							"emailCobranza","telefonoCobranza", 
							"nombreCalleCobranza","codigoPostalCobranza",
							"idColoniaCobranza","nombreColoniaCobranza",
							"idEstadoCobranza","idMunicipioCobranza",
							"idTipoConductoCobro","idBancoCobranza",
							"idTipoTarjetaCobranza", "numeroTarjetaCobranza", 
							"codigoSeguridadCobranza", "fechaVencimientoTarjetaCobranza", 
							"tipoPromocion", "diaPagoTarjetaCobranza",
							"rfcCobranza", "cuentaNoEncriptada"
					};
					
					String [] columnasResulset = {
							"idCliente", "idConductoCobranza", 
							"idVersionConducto", "nombreTarjetaHabienteCobranza",
							"emailCobranza","telefonoCobranza", 
							"nombreCalleCobranza","codigoPostalCobranza",
							"idColoniaCobranza","nombreColoniaCobranza",
							"idEstadoCobranza","idMunicipioCobranza",
							"idTipoConductoCobro","idBancoCobranza",
							"idTipoTarjetaCobranza", "numeroTarjetaCobranza", 
							"codigoSeguridadCobranza", "fechaVencimientoTC", 
							"tipoPromocion", "diaPagoTarjetaCobranza", 
							"rfcCobranza", "cuentaNoEncriptada"
					};
					storedHelper.estableceMapeoResultados(ClienteGenericoDTO.class.getCanonicalName(),atributosDTO,columnasResulset);
					storedHelper.estableceParametro("pid_cliente", str(cliente.getIdCliente()));
					clientes = storedHelper.obtieneListaResultados();
					LogDeMidasInterfaz.log("Saliendo de ClienteFacade.findByFilters..." + this, Level.FINE, null);
				} catch (SQLException e) {
					Integer codErr = null;
					String descErr = null;
					if (storedHelper != null) {
						codErr = storedHelper.getCodigoRespuesta();
						descErr = storedHelper.getDescripcionRespuesta();
					}
					StoredProcedureErrorLog.doLog(usuario,StoredProcedureErrorLog.TipoAccion.BUSCAR,"SEYCOS.PKG_INT_MIDAS_E2.CCLICOB", ClienteDTO.class, codErr, descErr);
					LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.findById..." + this, Level.WARNING, e);
					throw e;
				} catch (Exception e) {
					LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.findById..." + this, Level.SEVERE, e);
					throw e;
				}
			}
			return clientes;
		}
		
		private void onError(String msg)throws Exception{
			throw new Exception(msg);
		}

		@Override
		public ClienteGenericoDTO getConductoCobro(Long idCliente,
				Long idConductoCobro) throws Exception {
			ClienteGenericoDTO conducto = new ClienteGenericoDTO();
			ClienteGenericoDTO filtro = new ClienteGenericoDTO();
			filtro.setIdCliente(BigDecimal.valueOf(idCliente));
			List<ClienteGenericoDTO> lista = loadMediosPagoPorCliente(filtro, "SYSTEM");
			LogDeMidasInterfaz.log("lista de mediosPagoPorCliente... lista.size:"+ lista.size()+" idCliente:"+ idCliente+" idConductoCobro:"+ idConductoCobro+" " + this, Level.FINE, null);
			for(ClienteGenericoDTO dto : lista){
				LogDeMidasInterfaz.log("dto.getIdConductoCobranza:"+dto.getIdConductoCobranza()+" idConductoCobro.longValue():"+idConductoCobro.longValue() +" " + this, Level.FINE, null);
				if(dto.getIdConductoCobranza().longValue() == idConductoCobro.longValue()){
					conducto = dto;
				}
			}
			return conducto;
		}

		@Override
		public BigDecimal getDomicilioPorCliente(BigDecimal idCliente) {
			BigDecimal idDomicilio = null;
			ClienteGenericoDTO filtro = new ClienteGenericoDTO();
			try{
				filtro.setIdCliente(idCliente);
				List<ClienteGenericoDTO> clientes = findByFilters(filtro, null);
				if(!clientes.isEmpty()){
					ClienteGenericoDTO cliente = clientes.get(0);
					idDomicilio = cliente.getIdDomicilioConsulta() != null ? 
							BigDecimal.valueOf(cliente.getIdDomicilioConsulta()) : null;
				}
			}catch(Exception ex){
				throw new RuntimeException("Imposible consultar el domicilio para el cliente " + idCliente);
			}
			return idDomicilio;
		}
		
		@Override
		public ClienteGenericoDTO getRFCPorCliente(BigDecimal idCliente){
			
			ClienteGenericoDTO clienteGen = new ClienteGenericoDTO();
			StoredProcedureHelper storedHelper = null;
			
			try {
				
				LogDeMidasInterfaz.log("Entrando a ClienteFacade.findById..." + this, Level.FINE, null);
				
				storedHelper = new StoredProcedureHelper("SEYCOS.PKG_INT_MIDAS_E2.cCliGetRFC");

				String [] atributosDTO = {
						"idCliente", "codigoRFC"
				};
				
				String [] columnasResulset = {
						"Id_Cliente", "codigoRFC"
				};
				
				storedHelper.estableceMapeoResultados(
								ClienteDTO.class.getCanonicalName(),
								atributosDTO,
								columnasResulset);
				
				storedHelper.estableceParametro("pId_Cliente", idCliente);
				
				ClienteDTO cliente =  (ClienteDTO)storedHelper.obtieneResultadoSencillo();

				LogDeMidasInterfaz.log("Saliendo de ClienteFacade.findById..." + this, Level.FINE, null);				
				clienteGen.setIdCliente(cliente.getIdCliente());
				clienteGen.setCodigoRFC(cliente.getCodigoRFC());
				
				
			} catch (SQLException e) {
				
				Integer codErr = null;
				String descErr = null;
				
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				
				StoredProcedureErrorLog.doLog("",
						StoredProcedureErrorLog.TipoAccion.BUSCAR,
						"SEYCOS.PKG_INT_MIDAS.CDETALLE_CLIENTE", ClienteDTO.class, codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.findById..." + this, Level.WARNING, e);
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.findById..." + this, Level.SEVERE, e);
			}
			
			return clienteGen;
		}
		
		@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
		public void obtenerDatosTarjeta(BigDecimal idToPersonaContratante, Integer idTipoConductoCobro, String usuario) {
			StoredProcedureHelper storedHelper = null;
			try {
				LogDeMidasInterfaz.log("Entrando a ClienteFacade.obtenerDatosTarjeta..." + this, Level.FINE, null);
				storedHelper = new StoredProcedureHelper("SEYCOS.PKG_COBRANZA_CLIENTE.stp_obtenerDatosTarjeta");
				
				storedHelper.estableceParametro("pId_cliente", str(idToPersonaContratante));
				storedHelper.estableceParametro("pId_cond_cobro", str(idTipoConductoCobro));
				
				storedHelper.ejecutaActualizar();
				LogDeMidasInterfaz.log("Saliendo de ClienteFacade.obtenerDatosTarjeta..." + this, Level.FINE, null);
			} catch (SQLException e) {
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(usuario,StoredProcedureErrorLog.TipoAccion.BUSCAR,"SEYCOS.PKG_COBRANZA_CLIENTE.stp_obtenerDatosTarjeta", ClienteDTO.class, codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de ClienteFacade.stp_obtenerDatosTarjeta..." + this, Level.WARNING, e);
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en ClienteFacade.obtenerDatosTarjeta..." + this, Level.SEVERE, e);
			}
		}
}