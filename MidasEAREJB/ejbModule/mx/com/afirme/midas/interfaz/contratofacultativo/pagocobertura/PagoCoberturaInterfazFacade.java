package mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class PagoCoberturaInterfazFacade implements PagoCoberturaFacadeRemote {

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<PagoCoberturaDTO> calcularPagos(Date fechaInicialBase, Date fechaFinalBase, int idFormaPagoBase,
												Date fechaInicioVigencia, Date fechaFinVigencia, int formaPago, 
												double montoAPagar, String nombreUsuario) throws Exception {		
		StoredProcedureHelper storedHelper = null;
		
		//Nombre del procedimiento
		final String NOMBRE_SP = "SEYCOS.PKG_INT_MIDAS.stpDistribucion_Rec";
		
		//Parámetros de entrada		
		final String FECHA_INICIAL_BASE = "pf_ini_base";
		//final String FECHA_FINAL_BASE = "pf_fin_base";
		//final String FORMA_PAGO_BASE = "pid_fpago_base";		
		final String FECHA_INICIO_VIGENCIA = "pf_inicial";
		final String FECHA_FIN_VIGENCIA = "pf_final";
		final String FORMA_PAGO = "pid_forma_pago";
		final String MONTO_A_PAGAR = "pimporte";
		
		//Datos del cursor de salida		
		final String NUMERO_EXHIBICION = "num_exhibicion";
		final String FECHA_INICIO_PAGO = "fecha_desde";
		final String FECHA_FIN_PAGO = "fecha_hasta";
		final String PERIODO_COMPLETO = "periodo_comp";
		final String FACTOR = "factor";
		final String MONTO_PAGO = "importe";		
						
		try {
			String parametros = "("+FECHA_INICIAL_BASE+" = "+fechaInicialBase+"," +
			FECHA_INICIO_VIGENCIA+" = "+fechaInicioVigencia+","+  				
			FECHA_FIN_VIGENCIA+" = "+fechaFinalBase+"," +
			FORMA_PAGO+" = "+formaPago+"," +
			MONTO_A_PAGAR+" = "+montoAPagar+")";
			LogDeMidasInterfaz.log("Entrando a PagoCoberturaFacade.calculaPagos..." + this, Level.INFO, null);			
			LogDeMidasInterfaz.log("Se llama SP "+NOMBRE_SP + parametros, Level.INFO, null);
						
			storedHelper = new StoredProcedureHelper(NOMBRE_SP);

			storedHelper
					.estableceMapeoResultados(
							PagoCoberturaDTO.class.getCanonicalName(),							
							"numeroExhibicion," +
							"fechaInicioPago," +
							"fechaFinPago," +
							"periodoCompleto," +
							"factor," +	
							"montoPago",		
							
							NUMERO_EXHIBICION+"," +
							FECHA_INICIO_PAGO+"," +
							FECHA_FIN_PAGO+"," +
							PERIODO_COMPLETO+"," +
							FACTOR+"," +
							MONTO_PAGO);
			
														
			storedHelper.estableceParametro(FECHA_INICIAL_BASE, fechaInicialBase);
			//storedHelper.estableceParametro(FECHA_FINAL_BASE, fechaFinalBase);
			//storedHelper.estableceParametro(FORMA_PAGO_BASE, idFormaPagoBase);
			storedHelper.estableceParametro(FECHA_INICIO_VIGENCIA, fechaInicioVigencia);
			storedHelper.estableceParametro(FECHA_FIN_VIGENCIA, fechaFinVigencia);
			storedHelper.estableceParametro(FORMA_PAGO, formaPago);
			storedHelper.estableceParametro(MONTO_A_PAGAR, montoAPagar);
						
			List<PagoCoberturaDTO> pagos = storedHelper.obtieneListaResultados();						
			
			LogDeMidasInterfaz.log("Saliendo de PagoCoberturaFacade.calculaPagos..." + this, Level.INFO, null);
			
			return pagos;
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					NOMBRE_SP, PagoCoberturaDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de PagoCoberturaFacade.calculaPagos..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en PagoCoberturaFacade.calculaPagos..." + this, Level.WARNING, e);
			throw e;
		}
	}	
}
