package mx.com.afirme.midas.interfaz.asientocontable;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class AsientoContableFacade implements AsientoContableFacadeRemote {
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<AsientoContableDTO> contabilizaMovimientos(String idObjetoContable, String claveTransaccionContable, String nombreUsuario) 
	throws Exception {
		
		StoredProcedureHelper storedHelper = null;
		
		try {
			
			LogDeMidasInterfaz.log("Entrando a AsientoContableFacade.contabilizaMovimientos... SEYCOS.PKG_INT_MIDAS.STPCONTABILIZA_MOVS; parametros: (pIdentificador: "+idObjetoContable+", pCve_trans_cont: "+claveTransaccionContable, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.STPCONTABILIZA_MOVS");
			
			storedHelper
			.estableceMapeoResultados(
					AsientoContableDTO.class.getCanonicalName(),
					
							
					"idRamo," +
					"idSubRamo," +
					"conceptoPoliza," +
					"idMoneda," +
					"tipoCambio," +
					"claveConceptoMovimiento," +
					"auxiliar," +
					"centroCosto," +
					"tipoPersona," +
					"importeNeto," +
					"importeBonificacion," +
					"importeDescuento," +
					"importeDerechos," +
					"importeRecargos," +
					"importeIva," +
					"importeComision," +
					"importeGastos," +
					"importeOtrosConceptos," +
					"importeOtrosImpuestos," +
					"conceptoMovimiento," +
					"nombreUsuario" ,
					
					"Id_Ramo," +
					"Id_subr," +
					"Concepto," +
					"Id_Moneda," +
					"Tipo_cambio," +
					"Cve_concepto_mov," +
					"Auxiliar," +
					"Ccosto," +
					"TipoPersona," +
					"Imp_Neto," +
					"Imp_Bonificacion," +
					"Imp_Descuento," +
					"Imp_Derechos," +
					"Imp_Recargos," +
					"Imp_Iva," +
					"Imp_Comision," +
					"Imp_Gastos," +
					"Imp_Otros_conc," +
					"Imp_Otros_imp," +
					"Concepto_mov," +
					"Usuario");

			
			
			
			storedHelper.estableceParametro("pIdentificador", idObjetoContable);
			storedHelper.estableceParametro("pCve_trans_cont", claveTransaccionContable);
			
			List<AsientoContableDTO> asientoContableList = storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de AsientoContableFacade.contabilizaMovimientos... SEYCOS.PKG_INT_MIDAS.STPCONTABILIZA_MOVS; parametros: (pIdentificador: "+idObjetoContable+", pCve_trans_cont: "+claveTransaccionContable+" registros: "+(asientoContableList != null ? asientoContableList.size() : null), Level.INFO, null);
			return asientoContableList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"SEYCOS.PKG_INT_MIDAS.STPCONTABILIZA_MOVS", AsientoContableDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de AsientoContableFacade.contabilizaMovimientos... SEYCOS.PKG_INT_MIDAS.STPCONTABILIZA_MOVS; parametros: (pIdentificador: "+idObjetoContable+", pCve_trans_cont: "+claveTransaccionContable, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en AsientoContableFacade.contabilizaMovimientos... SEYCOS.PKG_INT_MIDAS.STPCONTABILIZA_MOVS; parametros: (pIdentificador: "+idObjetoContable+", pCve_trans_cont: "+claveTransaccionContable, Level.WARNING, e);
			throw e;
		}
	}

}
