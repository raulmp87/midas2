package mx.com.afirme.midas.interfaz.remesa;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class RemesaFacade implements RemesaFacadeRemote {

	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public BigDecimal registraRemesa(BigDecimal idRemesa, String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a RemesaFacade.registraRemesa..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.STPREGISTRA_REMESAS");
		
			storedHelper.estableceParametro("pId_Remesa", idRemesa);
			storedHelper.estableceParametro("pUsuario", nombreUsuario);
			
						
			BigDecimal idRemesaRegistrada = new BigDecimal(storedHelper.ejecutaActualizar());
			LogDeMidasInterfaz.log("Saliendo de RemesaFacade.registraRemesa..." + this, Level.INFO, null);
			return idRemesaRegistrada;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"SEYCOS.PKG_INT_MIDAS.STPREGISTRA_REMESAS", BigDecimal.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de RemesaFacade.registraRemesa..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en RemesaFacade.registraRemesa..." + this, Level.WARNING, e);
			throw e;
		}
	}

}
