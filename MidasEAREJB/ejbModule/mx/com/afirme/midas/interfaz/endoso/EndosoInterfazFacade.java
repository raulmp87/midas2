package mx.com.afirme.midas.interfaz.endoso;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente.TipoEmision;

@Stateless
public class EndosoInterfazFacade implements EndosoFacadeRemote {

	/**
	 * Emite un endoso en Seycos
	 * 
	 * @param numPolizaEndoso
	 *            (Id de la p�liza | Numero del Endoso)
	 * @param nombreUsuario
	 *            Nombre del Usuario que realiza la operacion
	 * @return Listado con referencias a los recibos del Endoso emitido
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<EndosoIDTO> emiteEndoso(String numPolizaEndoso,
			String nombreUsuario) throws Exception {

		StoredProcedureHelper storedHelper = null;
		try {

			LogDeMidasInterfaz.log("Entrando a EndosoFacade.emiteEndoso..."
					+ this, Level.INFO, null);

			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.stpEmiteEndoso");

			storedHelper.estableceMapeoResultados(EndosoIDTO.class
					.getCanonicalName(),

			"numeroFolioRecibo," + "numeroExhibicion," + "llaveFiscal,"
					+ "importe," + "fechaLimitePago",

			"NUM_FOLIO_RBO," + "NUM_EXHIBICION," + "LLAVE_FISCAL," + "IMPORTE,"
					+ "FECHA_LIMITE_PAGO");

			storedHelper.estableceParametro("pIdentificador", numPolizaEndoso);

			List<EndosoIDTO> reciboEndosoList = storedHelper
					.obtieneListaResultados();

			LogDeMidasInterfaz.log("Saliendo de EndosoFacade.emiteEndoso..."
					+ this, Level.INFO, null);

			return reciboEndosoList;

		} catch (SQLException e) {

			Integer codErr = null;
			String descErr = null;

			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}

			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"SEYCOS.PKG_INT_MIDAS.stpEmiteEndoso", EndosoIDTO.class,
					codErr, descErr);
			LogDeMidasInterfaz.log(
					"Excepcion en BD de EndosoFacade.emiteEndoso..." + this,
					Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log(
					"Excepcion general en EndosoFacade.emiteEndoso..." + this,
					Level.WARNING, e);
			throw e;
		}

	}

	/**
	 * Valida si la poliza es Cancelable a inicio de vigencia
	 * 
	 * @param numPolizaEndoso
	 *            (Id de la p�liza | Numero del Endoso)
	 * @param nombreUsuario
	 *            Nombre del Usuario que realiza la operacion
	 * @param fechaConsulta
	 *            Fecha en la que se desea cancelar la poliza
	 * @return true si la poliza es Cancelable a inicio de vigencia, false en
	 *         caso contrario. En caso de no ser Cancelable, incluye el motivo
	 *         por lo cual no lo es
	 * @throws Exception
	 */
	public EndosoIDTO validaEsCancelable(String numPolizaEndoso,
			String nombreUsuario, Date fechaCancelacion, Integer tipoEndoso)
			throws Exception {

		StoredProcedureHelper storedHelper = null;
		try {

			LogDeMidasInterfaz.log(
					"Entrando a EndosoFacade.validaEsCancelable..." + this,
					Level.INFO, null);

			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.stpPermiteCancelar");

			storedHelper.estableceMapeoResultados(EndosoIDTO.class
					.getCanonicalName(),

			"esValido," + "motivoNoCancelable," + "fechaInicioVigencia,"
					+ "primaNeta," + "bonificacionComision,"
					+ "bonificacionComisionRPF," + "derechos," + "recargoPF,"
					+ "iva," + "primaTotal," + "comision," + "comisionRPF,"
					+ "calculo",

			"fValido," + "fMensaje," + "FECHAINICIOVIGENCIA," + "PRIMANETA,"
					+ "BONIFCOMIS," + "BONIFCOMISRPF," + "DERECHOS,"
					+ "RECARGOPF," + "IVA," + "PRIMATOTAL," + "COMISION,"
					+ "COMISIONRPF," + "Calculo");

			storedHelper.estableceParametro("pIdentificador", numPolizaEndoso);
			storedHelper.estableceParametro("pF_Proceso", fechaCancelacion);
			storedHelper.estableceParametro("pTipo_Endoso", tipoEndoso);

			EndosoIDTO endoso = (EndosoIDTO) storedHelper
					.obtieneResultadoSencillo();

			LogDeMidasInterfaz.log(
					"Saliendo de EndosoFacade.validaEsCancelable..." + this,
					Level.INFO, null);

			return endoso;

		} catch (SQLException e) {

			Integer codErr = null;
			String descErr = null;

			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}

			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.stpPermiteCancelar",
					EndosoIDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log(
					"Excepcion en BD de EndosoFacade.validaEsCancelable..."
							+ this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log(
					"Excepcion general en EndosoFacade.validaEsCancelable..."
							+ this, Level.WARNING, e);
			throw e;
		}
	}
		
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<EndosoIDTO> validaPagosRealizados (String identificador, String nombreUsuario, Date fechaInicioVigencia, TipoEmision tipoEmision) {
		StoredProcedureHelper storedHelper = null;
		try {
			
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.STP_VALIDAENDOSO");

			storedHelper.estableceMapeoResultados(EndosoIDTO.class.getCanonicalName(),

			"esValido," + "motivoNoCancelable," + "fechaInicioVigencia,"
				+ "numeroLinea," + "numeroInciso," + "primaNeta," 
				+ "bonificacionComision," + "bonificacionComisionRPF," + "recargoPF,"
				+ "comision," + "comisionRPF," + "derechos," + "iva,"
				+ "sobreComisionAgente," + "sobreComisionProm," + "bonoAgente," 
				+ "bonoProm," + "cesionDerechosAgente," + "cesionDerechosProm," 
				+ "sobreComisionUDIAgente," + "sobreComisionUDIProm," + "calculo," 
				+ "importeNotaCredito," + "factorDctoIgualacion",
	
			"FVALIDO," + "FMENSAJE," + "FECHAINICIOVIGENCIA," 
				+ "NUMLINEA," + "NUMINCISO," + "PRIMANETA,"
				+ "BONIFCOMIS," + "BONIFCOMISRPF," + "RECARGOPF,"
				+ "COMISION," + "COMISIONRPF," + "DERECHOS," + "IVA,"
				+ "sobrecom_agt," + "sobrecom_prom," + "cesion_derechos_agt," 
				+ "cesion_derechos_prom," + "sobrecom_udi_agt," + "sobrecom_udi_prom," 
				+ "bono_agt," + "bono_prom," + "calculo," 
				+ "impncr," + "fact_descto_igpm");

			storedHelper.estableceParametro("PIDENTIFICADOR", identificador);
			storedHelper.estableceParametro("PTIPOEMISION", tipoEmision.getTipoEmision());
			storedHelper.estableceParametro("PF_PROCESO", fechaInicioVigencia);
			
			//TODO ELIMINAR 
			System.out.println(
					"Parametros validaPagosRealizados IDENTIFICADOR "+ identificador + "PTIPOEMISION " +tipoEmision.getTipoEmision()+ "PF_PROCESO " +fechaInicioVigencia);
			

			List<EndosoIDTO> listaResultados = storedHelper.obtieneListaResultados();
	
			
			
			return listaResultados;
			
		} catch (SQLException e) {

			Integer codErr = null;
			String descErr = null;

			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}

			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.STP_VALIDAENDOSO",
					EndosoIDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log(
					"Excepcion en BD de validaPagosRealizados..."
							+ this, Level.WARNING, e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Obtiene una lista con los documentos cancelables (con recibos vencidos)
	 * de una fecha determinada
	 * 
	 * @param fechaConsulta
	 *            Fecha de consulta para documentos cancelables (con recibos
	 *            vencidos)
	 * @param nombreUsuario
	 *            Nombre del Usuario que realiza la operacion
	 * @return Lista con los documentos cancelables
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<EndosoIDTO> obtieneListaCancelables(Date fechaConsulta,String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = "SEYCOS.PKG_INT_MIDAS.cLista_Cancelables";
		try {

			LogDeMidasInterfaz.log("Entrando a EndosoFacade.obtieneListaCancelables..."+ this, Level.INFO, null);

			storedHelper = new StoredProcedureHelper(nombreSP);

			storedHelper.estableceMapeoResultados(EndosoIDTO.class.getCanonicalName(),
					"idPoliza," + "numeroEndosoString," + "fechaInicioVigencia,"
					+ "primaNeta," + "bonificacionComision,"
					+ "bonificacionComisionRPF," + "derechos," + "recargoPF,"
					+ "iva," + "primaTotal," + "comision," + "comisionRPF",

					"IDTOPOLIZA," + "NUMEROENDOSOSTRING," + "FECHAINICIOVIGENCIA,"
					+ "PRIMANETA," + "BONIFCOMIS," + "BONIFCOMISRPF,"
					+ "DERECHOS," + "RECARGOPF," + "IVA," + "PRIMATOTAL,"
					+ "COMISION," + "COMISIONRPF");

			storedHelper.estableceParametro("pF_Proceso", fechaConsulta);
			List<EndosoIDTO> listaEndoso = storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de EndosoFacade.obtieneListaCancelables..."+ this, Level.INFO, null);
			this.obtenerSoloRegistrosValidos(listaEndoso);
			return listaEndoso;
		} catch (SQLException e) {

			Integer codErr = null;
			String descErr = null;

			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}

			StoredProcedureErrorLog.doLog(nombreUsuario,StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP,EndosoIDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de EndosoFacade.obtieneListaCancelables..."+ this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en EndosoFacade.obtieneListaCancelables..."+ this, Level.WARNING, e);
			throw e;
		}
	}
	
	/**
	 * Obtiene una lista con los documentos cancelables (con recibos vencidos) de una fecha determinada
	 * La lista de registros incluye datos adicionales (numeroPoliza,solicitante, agente, supervisoria, 
	 * oficina,gerencia, clave del movimiento y dias vencidos.)
	 * @param fechaConsulta Fecha de consulta para documentos cancelables (con recibos vencidos)
	 * @param nombreUsuario Nombre del Usuario que realiza la operacion
	 * @return Lista con los documentos cancelables
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<EndosoIDTO> obtieneListaCancelablesReporte(Date fechaConsulta,String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = "SEYCOS.PKG_INT_MIDAS.stp_ReporteListaCancelables";
		
		String [] atributosMapeoDTO = {
				"idPoliza","numeroEndoso" , "fechaInicioVigencia",
				"primaNeta" , "bonificacionComision" , "bonificacionComisionRPF" ,
				"derechos" , "recargoPF" , "iva",
				"primaTotal" , "comision" , "comisionRPF" ,
				"numeroPoliza" , "nombreSolicitante", "idAgente",
				"nombreAgente" , "nombreSupervisoria" , "nombreOficina" ,
				"nombreGerencia" , "claveMovimientoEndoso" , "diasVencidos"
		};
		
		String [] atributosMapeoCursor = {
				"IDTOPOLIZA" , "NUMEROENDOSO" , "FECHAINICIOVIGENCIA",
				"PRIMANETA" , "BONIFCOMIS" , "BONIFCOMISRPF",
				"DERECHOS" , "RECARGOPF" , "IVA" ,
				"PRIMATOTAL" , "COMISION" , "COMISIONRPF",
				"NUMEROPOLIZA" , "NOM_SOLICITANTE" , "ID_AGENTE",
				"NOMBRE_AGTE" , "NOM_SUPERVISORIA" , "NOM_OFICINA",
				"NOM_GERENCIA" , "CVE_MOVTO_END" , "DIAS_VENCIDOS"
		};
		
		try {

			LogDeMidasInterfaz.log("Entrando a EndosoFacade.obtieneListaCancelablesReporte..."+ this, Level.INFO, null);

			storedHelper = new StoredProcedureHelper(nombreSP);

			storedHelper.estableceMapeoResultados(EndosoIDTO.class.getCanonicalName(),atributosMapeoDTO,atributosMapeoCursor);

			storedHelper.estableceParametro("pF_Proceso", fechaConsulta);
			List<EndosoIDTO> listaEndoso = storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de EndosoFacade.obtieneListaCancelablesReporte..."+ this, Level.INFO, null);
			this.obtenerSoloRegistrosValidos(listaEndoso);
			return listaEndoso;
		} catch (SQLException e) {

			Integer codErr = null;
			String descErr = null;

			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}

			StoredProcedureErrorLog.doLog(nombreUsuario,StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP,EndosoIDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de EndosoFacade.obtieneListaCancelables..."+ this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en EndosoFacade.obtieneListaCancelables..."+ this, Level.WARNING, e);
			throw e;
		}
	}

	/**
	 * Obtiene una lista con los documentos rehabilitables en una fecha
	 * determinada
	 * 
	 * @param fechaConsulta
	 *            Fecha de consulta para documentos rehabilitables
	 * @param nombreUsuario
	 *            Nombre del Usuario que realiza la operacion
	 * @return Lista con los documentos rehabilitables
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<EndosoIDTO> obtieneListaRehabilitables(Date fechaConsulta,
			String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {

			LogDeMidasInterfaz.log(
					"Entrando a EndosoFacade.obtieneListaRehabilitables..."
							+ this, Level.INFO, null);

			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.cLista_Rehab");

			storedHelper.estableceMapeoResultados(EndosoIDTO.class
					.getCanonicalName(),

			"idPoliza," + "numeroEndosoString," + "referencia,"
					+ "fechaInicioVigencia," + "primaNeta,"
					+ "bonificacionComision," + "bonificacionComisionRPF,"
					+ "derechos," + "recargoPF," + "iva," + "primaTotal,"
					+ "comision," + "comisionRPF",

			"IDTOPOLIZA," + "NUMEROENDOSOSTRING," + "REFERENCIA,"
					+ "FECHAINICIOVIGENCIA," + "PRIMANETA," + "BONIFCOMIS,"
					+ "BONIFCOMISRPF," + "DERECHOS," + "RECARGOPF," + "IVA,"
					+ "PRIMATOTAL," + "COMISION," + "COMISIONRPF");

			storedHelper.estableceParametro("pF_Proceso", fechaConsulta);

			List<EndosoIDTO> listaEndoso = storedHelper
					.obtieneListaResultados();

			LogDeMidasInterfaz.log(
					"Saliendo de EndosoFacade.obtieneListaRehabilitables..."
							+ this, Level.INFO, null);

			this.obtenerSoloRegistrosValidos(listaEndoso);

			return listaEndoso;

		} catch (SQLException e) {

			Integer codErr = null;
			String descErr = null;

			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}

			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.cLista_Rehab", EndosoIDTO.class,
					codErr, descErr);
			LogDeMidasInterfaz.log(
					"Excepcion en BD de EndosoFacade.obtieneListaRehabilitables..."
							+ this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log(
					"Excepcion general en EndosoFacade.obtieneListaRehabilitables..."
							+ this, Level.WARNING, e);
			throw e;
		}
	}

	/**
	 * Obtiene una lista con las coberturas agrupada que pertenecen a un endoso
	 * determinado
	 * 
	 * @param id
	 *            Es el id del endoso a consultar que corespoonde a el
	 *            idToPoliza y en numeroEndoso
	 * @return Lista con coberturas Agrupadas
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<EndosoCoberturaDTO> obtieneCoberturasAgrupadas(EndosoId id,
			String nombreUsuario) throws Exception {

		StoredProcedureHelper storedHelper = null;
		try {

			LogDeMidasInterfaz.log(
					"Entrando a EndosoFacade.obtieneCoberturasAgrupadas..."
							+ this, Level.INFO, null);

			storedHelper = new StoredProcedureHelper(
					"MIDAS.pkgDAN_Generales.spDAN_CoberturasEndoso");

			storedHelper.estableceMapeoResultados(EndosoCoberturaDTO.class
					.getCanonicalName(),

			"idToSeccion," + "nombreComercialSeccion," + "idTocobertura,"
					+ "nombreComercialCobertura," + "valorPrimaneta",
					"idToSeccion," + "nombreComercialSeccion,"
							+ "idTocobertura," + "nombreComercialCobertura,"
							+ "valorPrimaneta");

			storedHelper.estableceParametro("pIdTopoliza", id.getIdToPoliza());
			storedHelper.estableceParametro("pNumeroEndoso", id
					.getNumeroEndoso());

			List<EndosoCoberturaDTO> coberturasAgrupadas = storedHelper
					.obtieneListaResultados();

			LogDeMidasInterfaz.log(
					"Saliendo de EndosoFacade.obtieneCoberturasAgrupadas..."
							+ this, Level.INFO, null);

			return coberturasAgrupadas;

		} catch (SQLException e) {

			Integer codErr = null;
			String descErr = null;

			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}

			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"MIDAS.pkgDAN_Generales.spDAN_CoberturasEndoso",
					EndosoIDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log(
					"Excepcion en BD de EndosoFacade.obtieneCoberturasAgrupadas..."
							+ this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log(
					"Excepcion general en EndosoFacade.obtieneCoberturasAgrupadas..."
							+ this, Level.WARNING, e);
			throw e;
		}

	}
	
	
	public BigDecimal obtieneConversionSeycos(String tipoConversion, String identificadorMidas) {
		String queryString = "SELECT ID_SEYCOS FROM SEYCOS.CONV_MIDAS_SEYCOS WHERE TIPO = '" + tipoConversion + "' " +
				"AND ID_MIDAS = '" + identificadorMidas + "'";
		
		
		Query query = entityManager.createNativeQuery(queryString.toString());
		
		return (BigDecimal)query.getSingleResult();
		
	}
	
	public BigDecimal obtieneNumeroEndosoSeycos(BigDecimal idSolicitudSeycos) {
		String queryString = "SELECT NUM_ENDOSO FROM SEYCOS.END_ENDOSO_T WHERE ID_SOLICITUD = " + idSolicitudSeycos;
		
		Query query = entityManager.createNativeQuery(queryString.toString());
		
		return (BigDecimal)query.getSingleResult();
	}
	

	private void obtenerSoloRegistrosValidos(List<EndosoIDTO> endosos) {
		for (EndosoIDTO endoso : endosos) {
			try {
				endoso.setNumeroEndoso(Short.valueOf(endoso.getNumeroEndosoString()));
				endoso.setEsValido((short) 1);
			} catch (NumberFormatException e) {
				endoso.setEsValido((short) 0);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<EndosoIDTO> validaEndosoPT (BigDecimal idToPoliza, Integer numeroInciso, Date fechaInicioVigencia, Short motivoEndoso) {
		StoredProcedureHelper storedHelper = null;
		try {
			
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.STP_VALIDAENDOSO_PT");

			storedHelper.estableceMapeoResultados(EndosoIDTO.class.getCanonicalName(),

			"tipoMovimiento," + "esValido," + "motivoNoCancelable," + "fechaInicioVigencia,"
				+ "numeroLinea," + "numeroInciso," + "primaNeta," 
				+ "bonificacionComision," + "bonificacionComisionRPF," + "recargoPF,"
				+ "comision," + "comisionRPF," + "derechos," + "iva,"
				+ "sobreComisionAgente," + "sobreComisionProm," + "bonoAgente," 
				+ "bonoProm," + "cesionDerechosAgente," + "cesionDerechosProm," 
				+ "sobreComisionUDIAgente," + "sobreComisionUDIProm," + "calculo," 
				+ "importeNotaCredito," + "factorDctoIgualacion," + "idCobertura" ,
	
			"TIPO_MOVIMIENTO," + "FVALIDO," + "FMENSAJE," + "FECHAINICIOVIGENCIA," 
				+ "NUMLINEA," + "NUMINCISO," + "PRIMANETA,"
				+ "BONIFCOMIS," + "BONIFCOMISRPF," + "RECARGOPF,"
				+ "COMISION," + "COMISIONRPF," + "DERECHOS," + "IVA,"
				+ "sobrecom_agt," + "sobrecom_prom," + "cesion_derechos_agt," 
				+ "cesion_derechos_prom," + "sobrecom_udi_agt," + "sobrecom_udi_prom," 
				+ "bono_agt," + "bono_prom," + "calculo," 
				+ "impncr," + "fact_descto_igpm," + "ID_COBERTURA");

			storedHelper.estableceParametro("PIDTOPOLIZA", idToPoliza);
			storedHelper.estableceParametro("PNUMEROINCISO", numeroInciso);
			storedHelper.estableceParametro("PFECHAINIVIGEND", fechaInicioVigencia);
			storedHelper.estableceParametro("PMOTIVOENDOSO", motivoEndoso);
			storedHelper.estableceParametro("PCVEUSUARIO", "TEST");
			
			//TODO ELIMINAR 
			System.out.println(
					"Parametros validaEndosoPT PIDTOPOLIZA " + idToPoliza + "PNUMEROINCISO " + numeroInciso 
					+ "PFECHAINIVIGEND " + fechaInicioVigencia + "PMOTIVOENDOSO" + motivoEndoso);

			List<EndosoIDTO> listaResultados = storedHelper.obtieneListaResultados();
				
			return listaResultados;
			
		} catch (SQLException e) {

			Integer codErr = null;
			String descErr = null;

			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}

			StoredProcedureErrorLog.doLog(null,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.STP_VALIDAENDOSO_PT",
					EndosoIDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log(
					"Excepcion en BD de validaEndosoPT..."
							+ this, Level.WARNING, e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@PersistenceContext
	private EntityManager entityManager;

}
