package mx.com.afirme.midas.interfaz.cotizacion.diasgracia;

import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.diasgracia.DiasGraciaServicio;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class DiasGraciaServicioImpl implements DiasGraciaServicio {

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Integer obtenerDiasGraciaDefault() {
		// TODO Implementar consulta de SP
		LogDeMidasInterfaz.log("Consultando Dias de gracia de seycos.", Level.INFO, null);
		
		StoredProcedureHelper storedHelper = null;
		
		String nombreSP = "SEYCOS.PKG_INT_MIDAS.stp_dias_gracia";
		
		try{
			
			LogDeMidasInterfaz.log("Entrando a DiasGraciaServicioImpl.obtenerDiasGraciaDefault. Invocando: "+nombreSP, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(nombreSP);
			
			storedHelper.estableceMapeoResultados(CotizacionDTO.class.getCanonicalName(),
					new String[]{"diasGracia"}, new String[]{"vlNormaCanc"});
			
			CotizacionDTO cotizacionDTO = (CotizacionDTO)storedHelper.obtieneResultadoSencillo();
			
			return cotizacionDTO.getDiasGracia();
			
		}
		catch(Exception e){
			LogDeMidasInterfaz.log("Excepcion en MidasInterfaz. nNombreSP: "+nombreSP, Level.WARNING, e);
			throw new RuntimeException(e);
		}
		
	}

}
