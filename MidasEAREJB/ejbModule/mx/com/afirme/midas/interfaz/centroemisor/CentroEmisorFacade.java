package mx.com.afirme.midas.interfaz.centroemisor;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class CentroEmisorFacade implements CentroEmisorFacadeRemote {

	@SuppressWarnings("unchecked")
	public List<CentroEmisorDTO> listarCentrosEmisores(String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			
			LogDeMidasInterfaz.log("Entrando a CentroEmisorFacade.listarCentrosEmisores..." + this, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.CCENTRO_EMISOR");

			storedHelper
					.estableceMapeoResultados(
							CentroEmisorDTO.class.getCanonicalName(),
							
							"clave," +
							"nombre",
													
							"ID_Centro_emis," +
							"Nom_centro_emis");
			
			
			storedHelper.estableceParametro("pUsuario", nombreUsuario);       
						
			List<CentroEmisorDTO> centroEmisorList = storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de CentroEmisorFacade.listarCentrosEmisores..." + this, Level.INFO, null);
			return centroEmisorList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.CCENTRO_EMISOR", CentroEmisorDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de CentroEmisorFacade.listarCentrosEmisores..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en CentroEmisorFacade.listarCentrosEmisores..." + this, Level.WARNING, e);
			throw e;
		}
	}

}
