package mx.com.afirme.midas.interfaz.ingreso.referencia;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class ReferenciaIngresoFacade implements ReferenciaIngresoFacadeRemote{

	@SuppressWarnings("unchecked")
	public List<ReferenciaIngresoDTO> obtenerReferenciasIngreso(String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = "SEYCOS.PKG_INT_MIDAS.CREFERENCIA_INGRESOS";
		try {
			LogDeMidasInterfaz.log("Entrando a ReferenciaIngresoFacade.obtenerReferenciasIngreso. SP a invocar: " + nombreSP, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(nombreSP);

			storedHelper.estableceMapeoResultados(
							ReferenciaIngresoDTO.class.getCanonicalName(),
							"idReferenciaExterna," +
							"descripcionReferenciaExterna",
							"IDENTIFICADOR," +
							"DESCRIPCION");
			
			List<ReferenciaIngresoDTO> referenciaIngresoList = storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de ReferenciaIngresoFacade.obtenerReferenciasIngreso. SP invocado: " + nombreSP+
					((referenciaIngresoList != null)?referenciaIngresoList.size()+" registros fueron recibidos.":
						"El resultado de la invocación es un objeto nulo: "+referenciaIngresoList), Level.INFO, null);
			return referenciaIngresoList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,nombreSP, ReferenciaIngresoDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de ReferenciaIngresoFacade.obtenerReferenciasIngreso. SP invocado: " + nombreSP, Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ReferenciaIngresoFacade.obtenerReferenciasIngreso. SP invocado: " + nombreSP, Level.SEVERE, e);
			throw e;
		}
	}

}
