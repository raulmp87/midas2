package mx.com.afirme.midas.interfaz.poliza;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class PolizaInterfazFacade implements PolizaFacadeRemote {	
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, String> emitePoliza(CotizacionDTO cotizacionDTO) throws Exception {
		StoredProcedureHelper storedHelper = null;
		Map<String, String> mensaje = new HashMap<String, String>();
		try {
			LogDeMidasInterfaz.log("Entrando a PolizaFacade.emitePoliza..." + this, Level.INFO, null);
			LogDeMidasInterfaz.log("Datos de Emision: pIdCotizacion ="+cotizacionDTO.getIdToCotizacion()+
									" pDiasPorDevengar="+cotizacionDTO.getDiasPorDevengar()+
									" pGastosExpedicion="+cotizacionDTO.getDerechosPoliza()+
									" pPorcentajeRPF="+cotizacionDTO.getPorcentajePagoFraccionado()+
									" pCodigoUsuario="+cotizacionDTO.getCodigoUsuarioModificacion()+
									" pFolioPolizaAsociada="+cotizacionDTO.getFolioPolizaAsociada(),Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(
			"MIDAS.pkgDAN_Generales.spDAN_EmitePoliza");
	
			storedHelper.estableceParametro("pIdCotizacion", cotizacionDTO.getIdToCotizacion());
			storedHelper.estableceParametro("pDiasPorDevengar", cotizacionDTO.getDiasPorDevengar());	
			storedHelper.estableceParametro("pGastosExpedicion", cotizacionDTO.getDerechosPoliza());
			storedHelper.estableceParametro("pPorcentajeRPF", cotizacionDTO.getPorcentajePagoFraccionado());	
			storedHelper.estableceParametro("pCodigoUsuario", cotizacionDTO.getCodigoUsuarioModificacion());
			storedHelper.estableceParametro("pFolioPolizaAsociada", cotizacionDTO.getFolioPolizaAsociada());
			
			Integer idPoliza = Integer.valueOf(storedHelper.ejecutaActualizar());
			
			if(idPoliza != null && idPoliza.intValue() > 0){
				mensaje.put("icono", "30");//Exito
				mensaje.put("idpoliza", idPoliza.toString());
				mensaje.put("mensaje", "La p\u00f3liza se emiti\u00f3 correctamente.</br> El n\u00famero de  P\u00f3liza es: ");					
			}else{
				String descErr = null;
				mensaje.put("icono", "10");//Error
				if (storedHelper != null) {
					descErr = storedHelper.getDescripcionRespuesta();
					mensaje.put("mensaje", "Error al generar la P\u00f3liza: "+descErr);	
				}			
						
			}		
			return mensaje;
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			mensaje.put("icono", "10");//Error
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
				mensaje.put("mensaje", "Error al generar la P\u00f3liza: "+descErr);
			}
			
			StoredProcedureErrorLog.doLog(cotizacionDTO.getCodigoUsuarioModificacion(),
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"MIDAS.pkgDAN_Generales.spDAN_EmitePoliza", CotizacionDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de PolizaFacade.emitePoliza..." + this, Level.WARNING, e);
			return mensaje;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en PolizaFacade.emitePoliza..." + this, Level.WARNING, e);
			mensaje.put("icono", "10");//Error
			mensaje.put("mensaje", "Error al generar la P\u00f3liza: "+e.getMessage());
			return mensaje;
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String consultaDatosPoliza(BigDecimal idCotizacionSeycos,String claveConsulta){
		
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select SEYCOS.PKG_INT_MIDAS.fn_datos_CM(");
		queryString.append(idCotizacionSeycos);
		queryString.append(",").append("'").append(claveConsulta).append("') from dual");
		
		Query query = entityManager.createNativeQuery(queryString.toString());
		
		String resultado = (String)query.getSingleResult();
		
		return resultado;
	}

	/**
	 * Obtiene el valor de la tabla de conversion para el tipo y id
	 * @param type
	 * @param midasValue
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BigDecimal getConvertion(String type,String midasId) {
		final String queryString = " SELECT SEYCOS.PKG_INT_MIDAS." +
				"fn_midas_seycos(?1, ?2) FROM DUAL ";		
		final Query query = entityManager.createNativeQuery(queryString);
		query.setParameter(1, type);
		query.setParameter(2, midasId);
		return (BigDecimal) query.getSingleResult();
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BigDecimal obtenerIdToCotizacionDePolizaSeycos(BigDecimal idCotizacionSeycos){
		
		String resultado = consultaDatosPoliza(idCotizacionSeycos, "COT");
		
		BigDecimal idtoCotizacion = null;
		
		if(resultado != null){
			String [] idCotStr = resultado.split("\\|");
			if(idCotStr != null && idCotStr[0].equals("1")){
				idtoCotizacion = new BigDecimal(idCotStr[1]);
			}
		}
		
		return idtoCotizacion;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean esPolizaCasaNueva(BigDecimal idCotizacionSeycos){
		
		String resultado = consultaDatosPoliza(idCotizacionSeycos, "COT");
		
		boolean resultadoB = false;
		if(resultado != null){
			String [] idCotStr = resultado.split("\\|");
			if(idCotStr != null && idCotStr[0].equals("1")){
				resultadoB = true;
			}
		}
		
		return resultadoB;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String obtenerFolioPolizaSeycos(BigDecimal idCotizacionSeycos){
		
		String resultado = consultaDatosPoliza(idCotizacionSeycos, "POL");
		
		return resultado;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Date obtenerFechaEmisionPoliza(BigDecimal idCotizacionSeycos){
		
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select fh_emision from SEYCOS.pol_poliza where id_cotizacion = ");
		queryString.append(idCotizacionSeycos);
		
		Query query = entityManager.createNativeQuery(queryString.toString());
		
		Object resultado = query.getSingleResult();
		
		Date fechaEmision = null;
		if(resultado != null)
			fechaEmision = (Date)resultado;
		
		return fechaEmision;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean reexpidePoliza(BigDecimal idPoliza, BigDecimal idCotizacion) {
		
		String info = "PolizaFacade.reexpidePoliza: Se reexpide el idPoliza " 
			+ idPoliza + " que pertence a la cotizacion " + idCotizacion;
		
		StoredProcedureHelper storedHelper = null;

		try {
			LogDeMidasInterfaz.log(info, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(
			"MIDAS.SP_REEXPIDE_POLIZA", StoredProcedureHelper.DATASOURCE_MIDAS);
	
			storedHelper.estableceParametro("pIdToPoliza", idPoliza);
						
			Integer exito = Integer.valueOf(storedHelper.ejecutaActualizar());
			
			if(exito != null && exito.intValue() > 0){
				return true;			
			}else{
				return false;
			}		
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;

			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog("SISTEMA",
					StoredProcedureErrorLog.TipoAccion.BORRAR,
					"MIDAS.SP_REEXPIDE_POLIZA", "idPoliza", codErr, descErr + " " + info);
			LogDeMidasInterfaz.log("Excepcion en BD de PolizaFacade.reexpidePoliza..." + info + " "  + this, Level.WARNING, e);
			return false;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en PolizaFacade.reexpidePoliza..." + info + " " + this, Level.WARNING, e);
			return false;
		}
	}
}
