package mx.com.afirme.midas.interfaz.mediopago;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class MedioPagoInterfazFacade implements MedioPagoFacadeRemote {

	@SuppressWarnings("unchecked")
	public List<MedioPagoDTO> findByProperty(MedioPagoDTO medioPago,
			String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a MedioPagoFacade.findByProperty..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.CIDMEDIO_PAGO");

			storedHelper
					.estableceMapeoResultados(
							MedioPagoDTO.class.getCanonicalName(),
							
							"idMedioPago," +
							"descripcion",
													
							"IDMedio_Pago," +
							"Descripcion");
						
			storedHelper.estableceParametro("pId_Medio_Pago", medioPago.getIdMedioPago());       
			storedHelper.estableceParametro("pDescripcion", medioPago.getDescripcion());
						
			List<MedioPagoDTO> medioPagoList = storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de MedioPagoFacade.findByProperty..." + this, Level.INFO, null);
			return medioPagoList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.CIDMEDIO_PAGO", MedioPagoDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de MedioPagoFacade.findByProperty..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en MedioPagoFacade.findByProperty..." + this, Level.WARNING, e);
			throw e;
		}
	}

}
