package mx.com.afirme.midas.interfaz.codigopostalcolonia;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class CodigoPostalColoniaFacade implements
		CodigoPostalColoniaFacadeRemote {

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<CodigoPostalColoniaDTO> agregaCodigoPostal(CodigoPostalColoniaDTO codigoPostal, String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		Integer codErr = null;
		try {
			
			LogDeMidasInterfaz.log("Entrando a CodigoPostalColoniaFacade.agregaCodigoPostal..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.STPCAT_CODPOST");
		
			storedHelper
			.estableceMapeoResultados(
					CodigoPostalColoniaDTO.class.getCanonicalName(),
					
					"codigoPostal," +
					"idColonia," +
					"nombreColonia",
											
					"Codigo_Postal," +
					"id_usr_cod_post," +
					"desc_usr_codpost");
			
			storedHelper.estableceParametro("pCodigo_Postal", codigoPostal.getCodigoPostal());
			storedHelper.estableceParametro("pId_colonia", codigoPostal.getIdColonia());
			storedHelper.estableceParametro("pNombreColonia", codigoPostal.getNombreColonia());
			
						
			List<CodigoPostalColoniaDTO> codigoPostalColoniaList = storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de CodigoPostalColoniaFacade.agregaCodigoPostal..." + this, Level.INFO, null);
			return codigoPostalColoniaList;
			
		} catch (SQLException e) {
						
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			//Validacion especial: Si el nombre de la colonia recibido ya existe con otro id_colonia se regresa el error al 
			//usuario y no se manda correo ni se ingresa en el log de error con interfaz
			if (codErr != null && codErr.intValue() == 1) {
				throw new Exception("COLONIA_DUPLICADA");
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"SEYCOS.PKG_INT_MIDAS.STPCAT_CODPOST", CodigoPostalColoniaDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de CodigoPostalColoniaFacade.agregaCodigoPostal..." + this, Level.WARNING, e);
			throw e;
			
		} catch (Exception e) {
			
			if (!e.getMessage().equals("COLONIA_DUPLICADA")){
				LogDeMidasInterfaz.log("Excepcion general en CodigoPostalColoniaFacade.agregaCodigoPostal..." + this, Level.WARNING, e);
			}
			throw e;
		} 
	}

	@SuppressWarnings("unchecked")
	public List<CodigoPostalColoniaDTO> listaColonias(CodigoPostalColoniaDTO codigoPostal, String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Entrando a CodigoPostalColoniaFacade.listaColonias..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"SEYCOS.PKG_INT_MIDAS.CLISTA_COLONIAS");
		
			storedHelper
			.estableceMapeoResultados(
					CodigoPostalColoniaDTO.class.getCanonicalName(),
					
					"codigoPostal," +
					"idColonia," +
					"nombreColonia",
											
					"Codigo_Postal," +
					"id_usr_cod_post," +
					"desc_usr_codpost");
			
			storedHelper.estableceParametro("pCodigo_Postal", codigoPostal.getCodigoPostal());
						
			List<CodigoPostalColoniaDTO> codigoPostalColoniaList = storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de CodigoPostalColoniaFacade.listaColonias..." + this, Level.INFO, null);
			return codigoPostalColoniaList;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_INT_MIDAS.CLISTA_COLONIAS", CodigoPostalColoniaDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de CodigoPostalColoniaFacade.listaColonias..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en CodigoPostalColoniaFacade.listaColonias..." + this, Level.WARNING, e);
			throw e;
		}
	}

	public List<CodigoPostalColoniaDTO> findByZipCode(String zipCode)
			throws Exception {
		CodigoPostalColoniaDTO codigoPostalColoniaDTO = new CodigoPostalColoniaDTO();
		codigoPostalColoniaDTO.setCodigoPostal(new DecimalFormat("00000").format(Double.parseDouble(zipCode)));
		return this.listaColonias(codigoPostalColoniaDTO, "");
	}
}
