package mx.com.afirme.midas.interfaz.producto;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.producto.ProductoDTO;

@Stateless
public class ProductoInterfazFacade implements ProductoFacadeRemote {

	public void save(ProductoDTO entity, String nombreUsuario) throws Exception {
		try
        {
			/*
			StoredProcedureHelper storedHelper = new StoredProcedureHelper("stpMtto_Producto");
			storedHelper.estableceMapeoResultados("void.class", "", "");
			storedHelper.estableceParametro("pId_midas", entity.getIdToProducto());
			storedHelper.estableceParametro("pDescMidas", entity.getDescripcion());
			storedHelper.estableceParametro("pUsuario", nombreUsuario);
			
			int res = storedHelper.ejecutaActualizar();
			
	        if (res != 1) {
	        	throw new Exception("No se pudo ejecutar la transaccion");
	        }
			*/
			System.out.println("AAC entro a la interfaz de producto- Guardar en Seycos...");
			
        }
	    catch(Exception e) {
	        StoredProcedureErrorLog.doLog(nombreUsuario, StoredProcedureErrorLog.TipoAccion.GUARDAR, "stpMtto_Producto", entity,null,null);
	        throw e;
	    } 

	}

}
