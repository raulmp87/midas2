package mx.com.afirme.midas.interfaz.cobranza;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;

@Stateless
public class CobranzaServiceImpl implements CobranzaService {

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private List<CoberturaCobranzaDTO> consultaSaldo(BigDecimal polizaId,
			BigDecimal lineId, BigDecimal incisoId, String situacion) {
		String spName = "SEYCOS.PKG_INT_MIDAS_E2.cListaRecibos";
		
		List<CoberturaCobranzaDTO> listaResultados = new ArrayList<CoberturaCobranzaDTO>();

		String[] atributosDTO = { "polizaId", "lineaNegocioId", "numeroInciso","coberturaId",
				"valorPN", "valorBonificacionComisionPN",
				"valorBonificacionComisionRPF", "valorBonificacionComision",
				"valorDerechos", "valorRecargo", "valorIVA", "valorPrimaTotal",
				"valorComisionPNAgente", "valorComisionRPFAgente",
				"valorComisionAgente" };

		String[] columnasCursor = { "idmidas", "idlinmidas", "idincisomidas","idcobmidas",
				"impprimaneta", "impboncompn", "impboncomrpf", "impboncomis",
				"impderechos", "imprcgospagofr", "impiva", "impprimatotal",
				"impcompnagte", "impcomrpfagte", "impcomisagte" };
		
		StoredProcedureHelper storedHelper;

		try {
			storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pidtopoliza", polizaId);
			storedHelper.estableceParametro("pidlinnegocio", lineId);
			storedHelper.estableceParametro("pidinciso", incisoId);
			storedHelper.estableceParametro("psituacion", situacion);

			storedHelper
					.estableceMapeoResultados(
							"mx.com.afirme.midas.interfaz.cobranza.CoberturaCobranzaDTO",
							atributosDTO, columnasCursor);

			listaResultados = (List<CoberturaCobranzaDTO>) storedHelper
			.obtieneListaResultados();			 

		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName + " Mensaje Error: " + e.getMessage(), e);
		}		
		
		if(listaResultados.isEmpty())
		{
			Object[] values = new Object[]{polizaId};
			throw new NegocioEJBExeption(null,values,"No es posible realizar la operaci\u00F3n. Favor de revisar los estatus de los recibos.");			
		}
		
		return listaResultados;
	}

	@Override
	public List<CoberturaCobranzaDTO> consultaCoberturasConSaldoPendiente(
			BigDecimal polizaId) {
		return this.consultaSaldo(polizaId, null, null,
				CoberturaCobranzaDTO.SALDO);
	}

	@Override
	public List<CoberturaCobranzaDTO> consultaCoberturasConSaldoPendiente(
			BigDecimal polizaId, BigDecimal lineaId) {
		return this.consultaSaldo(polizaId, lineaId, null,
				CoberturaCobranzaDTO.SALDO);
	}

	@Override
	public List<CoberturaCobranzaDTO> consultaCoberturasConSaldoPendiente(
			BigDecimal polizaId, BigDecimal lineaId, BigDecimal numeroInciso) {
		return this.consultaSaldo(polizaId, lineaId, numeroInciso,
				CoberturaCobranzaDTO.SALDO);
	}

	@Override
	public List<CoberturaCobranzaDTO> consultaCoberturasSaldoTotal(
			BigDecimal polizaId, BigDecimal lineaId) {
		return this.consultaSaldo(polizaId, lineaId, null,
				CoberturaCobranzaDTO.TOTAL);
	}

	@Override
	public List<CoberturaCobranzaDTO> consultaCoberturasSaldoTotal(
			BigDecimal polizaId, BigDecimal lineaId, BigDecimal numeroInciso) {
		return this.consultaSaldo(polizaId, lineaId, numeroInciso,
				CoberturaCobranzaDTO.TOTAL);
	}

	@Override
	public List<CoberturaCobranzaDTO> consultaCoberturasSaldoTotal(
			BigDecimal polizaId) {
		return this.consultaSaldo(polizaId, null, null,
				CoberturaCobranzaDTO.TOTAL);
	}

	@Override
	public Double consultaSaldoTotal(BigDecimal polizaId) {
		List<CoberturaCobranzaDTO> coberturas = this
				.consultaCoberturasSaldoTotal(polizaId);
		Double saldo = 0D;
		for (CoberturaCobranzaDTO cobertura : coberturas) {
			saldo += cobertura.getValorPN();
		}
		return saldo;
	}

	@Override
	public Double consultaSaldoPendiente(BigDecimal polizaId) {
		List<CoberturaCobranzaDTO> coberturas = this
				.consultaCoberturasConSaldoPendiente(polizaId);
		Double saldo = 0D;
		for (CoberturaCobranzaDTO cobertura : coberturas) {
			saldo += cobertura.getValorPN();
		}
		return saldo;
	}
	
	
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public MensajeDTO stpValidaPrimerRecNoPagado(BigDecimal polizaId, Date fechaProceso) {
		MensajeDTO mensajeDTO = null;
		String spName = "SEYCOS.PKG_INT_MIDAS_E2.stpValidaPrimerRecNoPagado";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pidtopoliza", polizaId);
			storedHelper.estableceParametro("pfechaProceso", fechaProceso);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return mensajeDTO;
	}
}
