package mx.com.afirme.midas.contratofacultativo;
// default package


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo.ParticipacionCorredorFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity DetalleContratoFacultativoDTO.
 * 
 * @see .DetalleContratoFacultativoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class DetalleContratoFacultativoFacade implements
		DetalleContratoFacultativoFacadeRemote {
	// property constants
	public static final String PORCENTAJE_COMISION = "porcentajeComision";
	public static final String DEDUCIBLE_COBERTURA = "deducibleCobertura";
	public static final String PRIMA_TOTAL_COBERTURA = "primaTotalCobertura";
	public static final String PRIMA_FACULTADA_COBERTURA = "primaFacultadaCobertura";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * DetalleContratoFacultativoDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            DetalleContratoFacultativoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DetalleContratoFacultativoDTO entity) {
		LogDeMidasEJB3.log("saving DetalleContratoFacultativoDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent DetalleContratoFacultativoDTO entity.
	 * 
	 * @param entity
	 *            DetalleContratoFacultativoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DetalleContratoFacultativoDTO entity) {
		LogDeMidasEJB3.log("deleting DetalleContratoFacultativoDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					DetalleContratoFacultativoDTO.class, entity
							.getContratoFacultativoDTO());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved DetalleContratoFacultativoDTO entity and
	 * return it or a copy of it to the sender. A copy of the
	 * DetalleContratoFacultativoDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            DetalleContratoFacultativoDTO entity to update
	 * @return DetalleContratoFacultativoDTO the persisted
	 *         DetalleContratoFacultativoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DetalleContratoFacultativoDTO update(
			DetalleContratoFacultativoDTO entity) {
		LogDeMidasEJB3.log("updating DetalleContratoFacultativoDTO instance",
				Level.INFO, null);
		try {
			DetalleContratoFacultativoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DetalleContratoFacultativoDTO findById(BigDecimal id) {
		LogDeMidasEJB3
				.log("finding DetalleContratoFacultativoDTO instance with id: "
						+ id, Level.INFO, null);
		try {
			DetalleContratoFacultativoDTO instance = entityManager.find(
					DetalleContratoFacultativoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DetalleContratoFacultativoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the DetalleContratoFacultativoDTO property to
	 *            query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<DetalleContratoFacultativoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<DetalleContratoFacultativoDTO> findByProperty(
			String propertyName, final Object value,
			final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log(
				"finding DetalleContratoFacultativoDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from DetalleContratoFacultativoDTO model where model."
					+ propertyName
					+ "= :propertyValue order by model.numeroInciso";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<DetalleContratoFacultativoDTO> findByPorcentajeComision(
			Object porcentajeComision, int... rowStartIdxAndCount) {
		return findByProperty(PORCENTAJE_COMISION, porcentajeComision,
				rowStartIdxAndCount);
	}

	public List<DetalleContratoFacultativoDTO> findByDeducibleCobertura(
			Object deducibleCobertura, int... rowStartIdxAndCount) {
		return findByProperty(DEDUCIBLE_COBERTURA, deducibleCobertura,
				rowStartIdxAndCount);
	}

	public List<DetalleContratoFacultativoDTO> findByPrimaTotalCobertura(
			Object primaTotalCobertura, int... rowStartIdxAndCount) {
		return findByProperty(PRIMA_TOTAL_COBERTURA, primaTotalCobertura,
				rowStartIdxAndCount);
	}

	public List<DetalleContratoFacultativoDTO> findByPrimaFacultadaCobertura(
			Object primaFacultadaCobertura, int... rowStartIdxAndCount) {
		return findByProperty(PRIMA_FACULTADA_COBERTURA,
				primaFacultadaCobertura, rowStartIdxAndCount);
	}

	/**
	 * Find all DetalleContratoFacultativoDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<DetalleContratoFacultativoDTO> all
	 *         DetalleContratoFacultativoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DetalleContratoFacultativoDTO> findAll(
			final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log(
				"finding all DetalleContratoFacultativoDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from DetalleContratoFacultativoDTO model";
			Query query = entityManager.createQuery(queryString);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public DetalleContratoFacultativoDTO findByNaturalKey(
			BigDecimal idTmContratoFacultativo, BigDecimal idTcSubramo,
			BigDecimal idToCobertura, BigDecimal numeroInciso,
			BigDecimal numeroSubInciso, BigDecimal idToSeccion) {
			DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = null;
			String queryString = "select detalleContratoFacultativoDTO from DetalleContratoFacultativoDTO detalleContratoFacultativoDTO "
					+ "inner join fetch detalleContratoFacultativoDTO.participacionFacultativoDTOs "
					+ "where detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo and "
					+ "detalleContratoFacultativoDTO.idTcSubramo = :idTcSubramo and "
					+ "detalleContratoFacultativoDTO.idToCobertura = :idToCobertura and "
					+ "detalleContratoFacultativoDTO.numeroInciso = :numeroInciso and "
					+ "detalleContratoFacultativoDTO.idToSeccion = :idToSeccion";
			if (numeroSubInciso != null
					&& numeroSubInciso.toBigInteger().intValue() != 0) {
				queryString += " and detalleContratoFacultativoDTO.numeroSubInciso = :numeroSubInciso";
			}
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTmContratoFacultativo",
					idTmContratoFacultativo);
			query.setParameter("idTcSubramo", idTcSubramo);
			query.setParameter("idToCobertura", idToCobertura);
			query.setParameter("numeroInciso", numeroInciso);
			query.setParameter("idToSeccion", idToSeccion);
			if (numeroSubInciso != null
					&& numeroSubInciso.toBigInteger().intValue() != 0) {
				query.setParameter("numeroSubInciso", numeroSubInciso);
			}
			List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOs = (List<DetalleContratoFacultativoDTO>) query.getResultList();
			if (detalleContratoFacultativoDTOs.size() == 1) {
				detalleContratoFacultativoDTO = detalleContratoFacultativoDTOs.get(0);
				// Se tuvo que poner el refresh porque algo sucede que no trae a sus
				// hijos. Cuando en realidad si tiene hijos. Cache?
				entityManager.refresh(detalleContratoFacultativoDTO);
			} else if (detalleContratoFacultativoDTOs.size() > 1) {
				throw new RuntimeException("Existe mas de un DetalleContratoFacultativoDTO para: " +
						String.format(DetalleContratoFacultativoFacade.DET_CONTRATO_BUSQUEDA_MENSAJE,
								idTmContratoFacultativo, idTcSubramo,
								idToCobertura, numeroInciso,
								numeroSubInciso, idToSeccion));	
			}

			return detalleContratoFacultativoDTO;
	}
	
	public DetalleContratoFacultativoDTO findUniqueByNaturalKey(
			BigDecimal idTmContratoFacultativo, BigDecimal idTcSubramo,
			BigDecimal idToCobertura, BigDecimal numeroInciso,
			BigDecimal numeroSubInciso, BigDecimal idToSeccion) {
		try {
			DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = findByNaturalKey(idTmContratoFacultativo, idTcSubramo,
					idToCobertura, numeroInciso, numeroSubInciso, idToSeccion);
			if (detalleContratoFacultativoDTO == null) {
				throw new RuntimeException("No se encontro ningun registro para DetalleContratoFacultativoDTO para: "  +
						String.format(DetalleContratoFacultativoFacade.DET_CONTRATO_BUSQUEDA_MENSAJE,
								idTmContratoFacultativo, idTcSubramo,
								idToCobertura, numeroInciso,
								numeroSubInciso, idToSeccion));
			}
			
			return detalleContratoFacultativoDTO;
		} 
		catch(RuntimeException ex) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, ex);
			throw ex;
		}
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<DetalleContratoFacultativoDTO> aplicarConfiguracionATodasLasCoberturas(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO) {
		LogDeMidasEJB3.log("copiando participaciones del DetalleContratoFacultativoDTO "+detalleContratoFacultativoDTO.getContratoFacultativoDTO()+" a otros detalles del mismo ContratoFacultativoDTO, DetalleContratoFacultativoDTO",Level.INFO, null);
		List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOList = null;
		try {
			detalleContratoFacultativoDTOList = this.findByProperty("contratoFacultativoDTO_1.idTmContratoFacultativo",detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getIdTmContratoFacultativo());
			Query query = entityManager.createQuery("SELECT model from ParticipacionFacultativoDTO model where model.detalleContratoFacultativoDTO.contratoFacultativoDTO = :contratoFacultativoDTO");
			query.setParameter("contratoFacultativoDTO",detalleContratoFacultativoDTO.getContratoFacultativoDTO()); // Obtener las participaciones del detalle que sirve como referencia
			List<ParticipacionFacultativoDTO> participacionFacultativoDTOList = query.getResultList();

			for (ParticipacionFacultativoDTO participacionFacultativoDTO : participacionFacultativoDTOList) {
				Query query4 = entityManager.createQuery("SELECT model from ParticipacionCorredorFacultativoDTO model where model.participacionFacultativoDTO.participacionFacultativoDTO = :participacionFacultativoDTO");
				query4.setParameter("participacionFacultativoDTO",participacionFacultativoDTO.getParticipacionFacultativoDTO());
				List<ParticipacionCorredorFacultativoDTO> participacionCorredorFacultativoDTOList = query4.getResultList();
				participacionFacultativoDTO.setParticipacionCorredorFacultativoDTOs(participacionCorredorFacultativoDTOList);
			}
			
			for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTOInTurn : detalleContratoFacultativoDTOList) {
				if ( detalleContratoFacultativoDTOInTurn.getContratoFacultativoDTO().compareTo(detalleContratoFacultativoDTO.getContratoFacultativoDTO())  != 0) {
					// a continuación se borran las participaciones previas de cada detalle
					Query query2 = entityManager.createQuery("DELETE from ParticipacionCorredorFacultativoDTO model WHERE model.participacionFacultativoDTO.detalleContratoFacultativoDTO.contratoFacultativoDTO = :contratoFacultativoDTO");
					query2.setParameter("contratoFacultativoDTO",detalleContratoFacultativoDTOInTurn.getContratoFacultativoDTO());
					query2.executeUpdate();

					Query query3 = entityManager.createQuery("DELETE from ParticipacionFacultativoDTO model WHERE model.detalleContratoFacultativoDTO.contratoFacultativoDTO = :contratoFacultativoDTO");
					query3.setParameter("contratoFacultativoDTO",detalleContratoFacultativoDTOInTurn.getContratoFacultativoDTO());
					query3.executeUpdate();
					
					detalleContratoFacultativoDTOInTurn.setParticipacionFacultativoDTOs(new ArrayList<ParticipacionFacultativoDTO>());

					for (ParticipacionFacultativoDTO participacionFacultativoDTO : participacionFacultativoDTOList) {
						List<ParticipacionCorredorFacultativoDTO> participacionCorredorFacultativoDTOList = participacionFacultativoDTO.getParticipacionCorredorFacultativoDTOs();
						ParticipacionFacultativoDTO participacionFacultativoDTO2 = new ParticipacionFacultativoDTO();

						participacionFacultativoDTO2.setDetalleContratoFacultativoDTO(detalleContratoFacultativoDTOInTurn);
						participacionFacultativoDTO2.setComision(participacionFacultativoDTO.getComision());
						participacionFacultativoDTO2.setContactoDTO(participacionFacultativoDTO.getContactoDTO());
						participacionFacultativoDTO2.setCuentaBancoDTOByIdtccuentabancodolares(participacionFacultativoDTO.getCuentaBancoDTOByIdtccuentabancodolares());
						participacionFacultativoDTO2.setCuentaBancoDTOByIdtccuentabancopesos(participacionFacultativoDTO.getCuentaBancoDTOByIdtccuentabancopesos());
						participacionFacultativoDTO2.setPorcentajeParticipacion(participacionFacultativoDTO.getPorcentajeParticipacion());
						participacionFacultativoDTO2.setPorcentajeRetencion(participacionFacultativoDTO.getPorcentajeRetencion());
						participacionFacultativoDTO2.setPrirmaretencion(participacionFacultativoDTO.getPrirmaretencion());
						participacionFacultativoDTO2.setReaseguradorCorredorDTO(participacionFacultativoDTO.getReaseguradorCorredorDTO());
						participacionFacultativoDTO2.setTipo(participacionFacultativoDTO.getTipo());
						detalleContratoFacultativoDTOInTurn.getParticipacionFacultativoDTOs().add(participacionFacultativoDTO2);
//						entityManager.persist(participacionFacultativoDTO2);
						participacionFacultativoDTO2.setParticipacionCorredorFacultativoDTOs(new ArrayList<ParticipacionCorredorFacultativoDTO>());
						for (ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO : participacionCorredorFacultativoDTOList) {
							ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO2 = new ParticipacionCorredorFacultativoDTO();
							participacionCorredorFacultativoDTO2.setParticipacionFacultativoDTO(participacionFacultativoDTO2);
							participacionCorredorFacultativoDTO2.setPorcentajeParticipacion(participacionCorredorFacultativoDTO.getPorcentajeParticipacion());
							participacionCorredorFacultativoDTO2.setReaseguradorCorredorDTO(participacionCorredorFacultativoDTO.getReaseguradorCorredorDTO());
							participacionFacultativoDTO2.getParticipacionCorredorFacultativoDTOs().add(participacionCorredorFacultativoDTO2);
//							entityManager.persist(participacionCorredorFacultativoDTO2);
						}
					}
					entityManager.merge(detalleContratoFacultativoDTOInTurn);
				}
			}
			LogDeMidasEJB3.log("Finaliza la copia de participaciones del DetalleContratoFacultativoDTO "+detalleContratoFacultativoDTO.getContratoFacultativoDTO()+" a otros detalles del mismo ContratoFacultativoDTO, DetalleContratoFacultativoDTO",Level.INFO, null);
		} catch (Exception re) {
			LogDeMidasEJB3.log("aplicarConfiguracionATodasLasCoberturas",Level.SEVERE, re);
		}
		return detalleContratoFacultativoDTOList;
	}

	@SuppressWarnings("unchecked")
	public boolean validarDetalleParticipacionPorContrato(BigDecimal id) {
		boolean autorizado = true;
		double porcentajeParticipacion = 0;
		if (id == null)
			return false;
		String queryString = "SELECT detalle FROM DetalleContratoFacultativoDTO detalle "
				+ "WHERE detalle.contratoFacultativoDTO_1.idTmContratoFacultativo = :idContratoFacultativo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idContratoFacultativo", id);

		List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOList = query
				.getResultList();
		if (detalleContratoFacultativoDTOList != null
				&& detalleContratoFacultativoDTOList.size() > 0) {
			for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : detalleContratoFacultativoDTOList) {
				queryString = "SELECT participacion FROM ParticipacionFacultativoDTO participacion WHERE participacion.detalleContratoFacultativoDTO.contratoFacultativoDTO = :detalleContratoFacultativo";
				query = entityManager.createQuery(queryString);
				query.setParameter("detalleContratoFacultativo",
						detalleContratoFacultativoDTO
								.getContratoFacultativoDTO());
				List<ParticipacionFacultativoDTO> participacionFacultativoDTOList = query
						.getResultList();
				porcentajeParticipacion = 0;
				for (ParticipacionFacultativoDTO participacionFacultativoDTO : participacionFacultativoDTOList) {
					if (participacionFacultativoDTO.getTipo().equals("0")) { // 0:
																				// Corredor,
																				// 1:
																				// Reasegurador,
																				// sólo
																				// interesa
																				// el
																				// caso
																				// en
																				// que
																				// es
																				// Corredor.
						queryString = "SELECT SUM(participacionCorredor.porcentajeParticipacion) FROM ParticipacionCorredorFacultativoDTO participacionCorredor WHERE participacionCorredor.participacionFacultativoDTO.participacionFacultativoDTO = :participacionFacultativoDTO";
						query = entityManager.createQuery(queryString);
						query.setParameter("participacionFacultativoDTO",
								participacionFacultativoDTO
										.getParticipacionFacultativoDTO());
						Double sumaParticipacionesReaseguradores = (Double) query
								.getSingleResult();
						if (sumaParticipacionesReaseguradores == null
								|| !sumaParticipacionesReaseguradores
										.equals(participacionFacultativoDTO
												.getPorcentajeParticipacion()))
							return false;
					}
					porcentajeParticipacion += participacionFacultativoDTO
							.getPorcentajeParticipacion().doubleValue();
				}
				if (porcentajeParticipacion != 100)
					return false;
			}
		} else
			autorizado = false;

		return autorizado;
	}

	/*
	 * Valida que el detalle facultativo tenga el 100% de participaciones
	 */
	@SuppressWarnings("unchecked")
	public boolean validarDetalleParticipacion(BigDecimal id) {
		LogDeMidasEJB3.log("Validando detalle participacion facultativo",
				Level.INFO, null);
		boolean resultado = true;
		if (id != null) {
			String nativeQueryString = "select pf.tipo tipo,pf.porcentajeparticipacion , "
					+ " case "
					+ " when (pf.tipo = 1) then 0 "
					+ " else (select sum(pcf1.porcentajeparticipacion) "
					+ " from MIDAS.tdparticipacioncorredorfac pcf1 "
					+ " where pcf1.idtdparticipacionfacultativo = pf.idtdparticipacionfacultativo "
					+ " group by pcf1.idtdparticipacionfacultativo) "
					+ " end porcentajepartcorredor "
					+ " from MIDAS.tdparticipacionfacultativo pf "
					+ " where pf.idtdcontratofacultativo = " + id;
			Query nativeQuery = entityManager
					.createNativeQuery(nativeQueryString);
			List listaObjectResultado = nativeQuery.getResultList();
			if (listaObjectResultado == null || listaObjectResultado.isEmpty()) {
				resultado = false;
			} else {
				Iterator resIterator = listaObjectResultado.iterator();
				BigDecimal tipo, porcentajeParticipador, sumaPorcentajeSubParticipadores;
				double porcentajeParticipaciones = 0;
				while (resIterator.hasNext()) {
					Object[] row = (Object[]) resIterator.next();
					tipo = (BigDecimal) row[0];
					porcentajeParticipador = (BigDecimal) row[1];
					if (row[2] != null)
						sumaPorcentajeSubParticipadores = (BigDecimal) row[2];
					else
						sumaPorcentajeSubParticipadores = BigDecimal.ZERO;
					if (tipo.compareTo(BigDecimal.ZERO) == 0) {// si es
																// corredor, se
																// validan sus
																// reaseguradores
						if (!(sumaPorcentajeSubParticipadores
								.compareTo(porcentajeParticipador) == 0)) {
							resultado = false;
							break;
						}
					}
					porcentajeParticipaciones += porcentajeParticipador
							.doubleValue();
				}// fin while
				resultado = (porcentajeParticipaciones == 100d);
			}
		} else
			resultado = false;
		return resultado;

		// DetalleContratoFacultativoDTO detalleContratoFacultativoDTO =
		// this.findById(id);
		// if (detalleContratoFacultativoDTO ==
		// null||detalleContratoFacultativoDTO.getContratoFacultativoDTO()==null)return
		// false;
		// String queryString =
		// "SELECT participacion FROM ParticipacionFacultativoDTO participacion WHERE participacion.detalleContratoFacultativoDTO.contratoFacultativoDTO = :detalleContratoFacultativo";
		// Query query = entityManager.createQuery(queryString);
		// query.setParameter("detalleContratoFacultativo",
		// detalleContratoFacultativoDTO.getContratoFacultativoDTO());
		// List<ParticipacionFacultativoDTO> participacionFacultativoDTOList =
		// query.getResultList();
		// if (participacionFacultativoDTOList.size()<1)return false;
		// for (ParticipacionFacultativoDTO participacionFacultativoDTO :
		// participacionFacultativoDTOList) {
		// if (participacionFacultativoDTO.getTipo().equals("0")){ // 0:
		// Corredor, 1: Reasegurador, sólo interesa el caso en que es Corredor.
		// queryString =
		// "SELECT SUM(participacionCorredor.porcentajeParticipacion) FROM ParticipacionCorredorFacultativoDTO participacionCorredor "
		// +
		// "WHERE participacionCorredor.participacionFacultativoDTO.participacionFacultativoDTO = :participacionFacultativoDTO";
		// query = entityManager.createQuery(queryString);
		// query.setParameter("participacionFacultativoDTO",
		// participacionFacultativoDTO.getParticipacionFacultativoDTO());
		// Double sumaParticipacionesReaseguradores = (Double)
		// query.getSingleResult();
		// if (sumaParticipacionesReaseguradores == null ||
		// !sumaParticipacionesReaseguradores.equals(participacionFacultativoDTO.getPorcentajeParticipacion()))
		// return false;
		// }
		// porcentajeParticipaciones +=
		// participacionFacultativoDTO.getPorcentajeParticipacion().doubleValue();
		// }
		// if (porcentajeParticipaciones != 100) return false;
		// return true;
	}

	@SuppressWarnings("unchecked")
	public List<DetalleContratoFacultativoDTO> listarFiltrado(
			DetalleContratoFacultativoDTO entity) {
		try {
			LogDeMidasEJB3.log(
					"Inicia listarFiltrado DetalleContratoFacultativoDTO ",
					Level.INFO, null);
			String queryString = "select model from DetalleContratoFacultativoDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			if (entity.getContratoFacultativoDTO_1() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
						sWhere,
						"contratoFacultativoDTO_1.idTmContratoFacultativo",
						entity.getContratoFacultativoDTO_1()
								.getIdTmContratoFacultativo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "contratoFacultativoDTO", entity
							.getContratoFacultativoDTO());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "idToCotizacion", entity.getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "idToCobertura", entity.getIdToCobertura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "idTcSubramo", entity.getIdTcSubramo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "idToSeccion", entity.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "numeroInciso", entity.getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "numeroSubInciso", entity.getNumeroSubInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "tipoDistribucion", entity.getTipoDistribucion());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("FALLO: listarFiltrado LineaSoporteReaseguro",
					Level.SEVERE, re);
			throw re;
		}
	}

	
	/*@SuppressWarnings("unchecked")
	public List<DetalleContratoFacultativoDTO> listarFiltrado(DetalleContratoFacultativoDTO entity) {
		try {
			LogDeMidasEJB3.log("Inicia listarFiltrado DetalleContratoFacultativoDTO ", Level.INFO, null);
			String queryString = "select model from DetalleContratoFacultativoDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			if(entity.getContratoFacultativoDTO_1() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "contratoFacultativoDTO_1.idTmContratoFacultativo", entity.getContratoFacultativoDTO_1().getIdTmContratoFacultativo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "contratoFacultativoDTO", entity.getContratoFacultativoDTO());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToCotizacion", entity.getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToCobertura", entity.getIdToCobertura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idTcSubramo", entity.getIdTcSubramo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToSeccion", entity.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroInciso", entity.getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroSubInciso", entity.getNumeroSubInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "tipoDistribucion", entity.getTipoDistribucion());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("FALLO: listarFiltrado LineaSoporteReaseguro", Level.SEVERE, re);
			throw re;
		}
	}*/
	
	private static final String DET_CONTRATO_BUSQUEDA_MENSAJE = "idTmContratoFacultativo: %s"
		+ " idTcSubramo: %s"
		+ " idToCobertura: %s"
		+ " numeroInciso: %s"
		+ " numeroSubInciso: %s"
		+ " idToSeccion: %s";
}