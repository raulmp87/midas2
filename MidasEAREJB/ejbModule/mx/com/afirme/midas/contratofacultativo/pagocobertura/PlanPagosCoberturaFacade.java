package mx.com.afirme.midas.contratofacultativo.pagocobertura;
// default package

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoFacadeRemote;
import mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo.ParticipacionCorredorFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

/**
 * Facade for entity PlanPagosCoberturaDTO.
 * @see .PlanPagosCobertura
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class PlanPagosCoberturaFacade  implements PlanPagosCoberturaFacadeRemote {
	@PersistenceContext private EntityManager entityManager;
	@EJB PagoCoberturaFacadeRemote pagoCobertura;
	@EJB PagoCoberturaReaseguradorFacadeRemote pagoCoberturaReasegurador; 
	@EJB DetalleContratoFacultativoFacadeRemote detalleContratoFacultativo;	
	@EJB CoberturaFacadeRemote cobertura;	
	
	//Estatus de autorizaci�n
	private final Byte AUTORIZACION_NO_REQUERIDA = 0;
	private final Byte AUTORIZACION_REQUERIDA = 1;
	private final Byte AUTORIZADO = 7;
	//private final Byte RECHAZADO = 8;
	
		/**
	 Perform an initial save of a previously unsaved PlanPagosCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PlanPagosCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public PlanPagosCoberturaDTO save(PlanPagosCoberturaDTO entity) {
    				LogDeMidasEJB3.log("saving PlanPagosCoberturaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            entityManager.flush();
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
	        }
	        return entity;
    }
    
    /**
	 Delete a persistent PlanPagosCoberturaDTO entity.
	  @param entity PlanPagosCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PlanPagosCoberturaDTO entity) {
    				LogDeMidasEJB3.log("deleting PlanPagosCoberturaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(PlanPagosCoberturaDTO.class, entity.getIdToPlanPagosCobertura());
            entityManager.remove(entity);
            entityManager.flush();
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved PlanPagosCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the PlanPagosCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PlanPagosCoberturaDTO entity to update
	 @return PlanPagosCoberturaDTO the persisted PlanPagosCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public PlanPagosCoberturaDTO update(PlanPagosCoberturaDTO entity) {
    				LogDeMidasEJB3.log("updating PlanPagosCoberturaDTO instance", Level.INFO, null);
	        try {
	        	if(entity.getDetalleContratoFacultativoDTO()==null){
	        		entity.setDetalleContratoFacultativoDTO(this.findById(entity.getIdToPlanPagosCobertura()).getDetalleContratoFacultativoDTO());
	        	}
	        	
            PlanPagosCoberturaDTO result = entityManager.merge(entity);
            entityManager.flush();
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public PlanPagosCoberturaDTO findById( Long id) {
    				LogDeMidasEJB3.log("finding PlanPagosCoberturaDTO instance with id: " + id, Level.INFO, null);
	        try {	        
            PlanPagosCoberturaDTO instance = entityManager.find(PlanPagosCoberturaDTO.class, id);
            entityManager.refresh(instance);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all PlanPagosCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PlanPagosCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<PlanPagosCoberturaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<PlanPagosCoberturaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding PlanPagosCoberturaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from PlanPagosCoberturaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}				
	
	/**
	 * Find all PlanPagosCoberturaDTO entities.
	  	  @return List<PlanPagosCoberturaDTO> all PlanPagosCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<PlanPagosCoberturaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all PlanPagosCoberturaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from PlanPagosCoberturaDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	public void modificarPagosCobertura(Long idToPlanPagosCobertura, 
			List<mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDTO> pagos,
			boolean omitirAutorizacion, String usuarioModificacion){
		List<PagoCoberturaDTO> pagosCoberturaAnteriores = null;
		
		pagosCoberturaAnteriores = 
			pagoCobertura.findByProperty("id.idToPlanPagosCobertura", idToPlanPagosCobertura);
		
		for(PagoCoberturaDTO pago : pagosCoberturaAnteriores){
			pagoCobertura.delete(pago);
		}		
		entityManager.flush();
		
		for(mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDTO pago : pagos){
			PagoCoberturaDTO pagoCoberturaDTO = new PagoCoberturaDTO();
			PagoCoberturaId id = new PagoCoberturaId(idToPlanPagosCobertura,pago.getNumeroExhibicion());
			pagoCoberturaDTO.setId(id);			
			pagoCoberturaDTO.setFechaPago(pago.getFechaInicioPago());
			pagoCoberturaDTO.setMontoPagoPrimaNeta(pago.getMontoPago());
			pagoCobertura.save(pagoCoberturaDTO);
		}
		entityManager.flush();
		
		if(!omitirAutorizacion){
			List<PagoCoberturaDTO> pagosCoberturaActuales = 
				pagoCobertura.findByProperty("id.idToPlanPagosCobertura", idToPlanPagosCobertura);
			this.actualizarEstatusAutorizacionPlanPagos(idToPlanPagosCobertura, pagosCoberturaAnteriores, 
														pagosCoberturaActuales, usuarioModificacion);		
		}
		
	}	
	
	private void actualizarEstatusAutorizacionPlanPagos(Long idToPlanPagosCobertura,
														List<PagoCoberturaDTO> pagosCoberturaAnteriores, 
														List<PagoCoberturaDTO> pagosCoberturaActuales,
														String usuarioSolicitaAutorizacion){
		boolean requiereAutorizacion = false;
		
		if(pagosCoberturaAnteriores.size() != pagosCoberturaActuales.size())
			requiereAutorizacion = true;
		else{
			for(int i = 0; i<pagosCoberturaActuales.size(); i++){
				PagoCoberturaDTO pagoAnterior = pagosCoberturaAnteriores.get(i);
				PagoCoberturaDTO pagoActual = pagosCoberturaActuales.get(i);
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String fechaAnterior = dateFormat.format(pagoAnterior.getFechaPago());
				String fechaActual = dateFormat.format(pagoActual.getFechaPago());
				
				if(fechaAnterior.compareTo(fechaActual)!=0){
					requiereAutorizacion = true;
					break;
				}
									
				Double montoAnterior = pagoAnterior.getMontoPagoPrimaNeta();
				Double montoActual = pagoActual.getMontoPagoPrimaNeta();
				
				if(montoAnterior.compareTo(montoActual)!=0){
					requiereAutorizacion = true;
					break;
				}
			}
		}
		
		if(requiereAutorizacion){			
			PlanPagosCoberturaDTO planPagosCobertura = this.findById(idToPlanPagosCobertura);
			planPagosCobertura.setEstatusAutorizacionPlanPagos(this.AUTORIZACION_REQUERIDA);
			planPagosCobertura.setFechaSolicitudAutorizacionPlanPagos(new Date());
			planPagosCobertura.setUsuarioSolicitudAutorizacionPlanPagos(usuarioSolicitaAutorizacion);
			planPagosCobertura.setFechaAutorizacionPlanPagos(null);
			planPagosCobertura.setUsuarioAutorizacionPlanPagos(null);
			this.update(planPagosCobertura);
		}					
	}
	
	public void distribuirPagosPorReasegurador(BigDecimal idTdContratoFacultativo)throws ExcepcionDeLogicaNegocio{
		LogDeMidasEJB3.log("Inicia la distribuci�n de pagos por reasegurador (idTdContratoFacultativo = "+idTdContratoFacultativo+")", Level.INFO, null);
		List<PlanPagosCoberturaDTO> listaPlanPagos = this.findByProperty("detalleContratoFacultativoDTO.contratoFacultativoDTO", 
																		 idTdContratoFacultativo);
		if(listaPlanPagos.size()>0){
			PlanPagosCoberturaDTO planPagosCobertura = listaPlanPagos.get(0);
			Set<PagoCoberturaDTO> pagosCobertura = planPagosCobertura.getPagoCoberturaDTOs();
			DetalleContratoFacultativoDTO detalleContratoFacultativo = planPagosCobertura.getDetalleContratoFacultativoDTO();
			List<ParticipacionFacultativoDTO> participaciones = detalleContratoFacultativo.getParticipacionFacultativoDTOs();
			final String CORREDOR = "0";
			
			List<PagoCoberturaReaseguradorDTO> exhibiciones = new ArrayList<PagoCoberturaReaseguradorDTO>();
			
			for(PagoCoberturaDTO pagoCobertura : pagosCobertura){
				for(ParticipacionFacultativoDTO participacion : participaciones){
					PagoCoberturaReaseguradorDTO exhibicion = null;
					if(participacion.getTipo().equals(CORREDOR)){
						//Si se trata de un corredor, se registran exhibiciones por cada reasegurador asignado al corredor.
						List<ParticipacionCorredorFacultativoDTO> participacionesCorredor = 
							participacion.getParticipacionCorredorFacultativoDTOs();						
						for(ParticipacionCorredorFacultativoDTO participacionCorredor : participacionesCorredor){							
							exhibicion = this.generarExhibicion(pagoCobertura, participacion, participacionCorredor);							
							exhibiciones.add(exhibicion);
						}
					}else{
						//Si se trata de un reasegurador, se registran exhibiciones para ese reasegurador					
							exhibicion = this.generarExhibicion(pagoCobertura, participacion, null);						
							exhibiciones.add(exhibicion);
					}
				}
			}			
			this.guardarExhibiciones(idTdContratoFacultativo, exhibiciones);
			LogDeMidasEJB3.log("Termina la distribuci�n de pagos por reasegurador (idTdContratoFacultativo = "+idTdContratoFacultativo+")", Level.INFO, null);			
		}else{
			LogDeMidasEJB3.log("No es posible distribuir los pagos por reasegurador debido a que no existe un plan de pagos para el contrato facultativo (idTdContratoFacultativo = "+idTdContratoFacultativo+")", Level.SEVERE, null);			
				throw new ExcepcionDeLogicaNegocio("PlanPagosCoberturaFacade", "No es posible distribuir los pagos por reasegurador debido a que no existe un plan de pagos para el contrato facultativo");			
		}		
	}
	
	private PagoCoberturaReaseguradorDTO generarExhibicion(PagoCoberturaDTO pagoCobertura,
								  						   ParticipacionFacultativoDTO participacion, 
								  						   ParticipacionCorredorFacultativoDTO participacionCorredor)throws ExcepcionDeLogicaNegocio{
		LogDeMidasEJB3.log("Inicia la generaci�n de exhibici�n (idTdContratoFacultativo = "+pagoCobertura.getPlanPagosCobertura().getDetalleContratoFacultativoDTO().getContratoFacultativoDTO()+", "+
																"idToPlanPagosCobertura = "+pagoCobertura.getId().getIdToPlanPagosCobertura()+", "+
																"numeroExhibicion = "+pagoCobertura.getId().getNumeroExhibicion()+", "+
																"idTdParticipacionFacultativo = "+participacion.getParticipacionFacultativoDTO()+", "+
																"idTdParticipacionCorredorFac = "+(participacionCorredor != null ? participacionCorredor.getIdTdParticapacionCorredorFac() : "null"), 
																Level.INFO, null);
		final Byte PAGO_PENDIENTE = 0;
		
		PagoCoberturaReaseguradorDTO exhibicion = new PagoCoberturaReaseguradorDTO();
		BigDecimal idReasegurador = participacionCorredor != null ? 
									participacionCorredor.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor() :
									participacion.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor();
		PagoCoberturaReaseguradorId id = new PagoCoberturaReaseguradorId(pagoCobertura.getId().getIdToPlanPagosCobertura(), 
																		 pagoCobertura.getId().getNumeroExhibicion(), idReasegurador);
		exhibicion.setId(id);							
		exhibicion.setPagoCobertura(pagoCobertura);
		
		ReaseguradorCorredorDTO corredor = participacionCorredor != null ? participacion.getReaseguradorCorredorDTO() : null;
		ReaseguradorCorredorDTO reasegurador = participacionCorredor != null ? 
											   participacionCorredor.getReaseguradorCorredorDTO() :
											   participacion.getReaseguradorCorredorDTO();
		
		exhibicion.setCorredor(corredor);
		exhibicion.setReasegurador(reasegurador);
		
		exhibicion.setEstatusPago(PAGO_PENDIENTE);
				
		Double porcentajeParticipacion = participacion.getPorcentajeParticipacion().doubleValue();		
		Double factorParticipacion = porcentajeParticipacion/100D;
		
		if(participacionCorredor != null){
			porcentajeParticipacion = participacionCorredor.getPorcentajeParticipacion().doubleValue();
			factorParticipacion = porcentajeParticipacion/participacion.getPorcentajeParticipacion().doubleValue()*factorParticipacion;			
		}
													  
		exhibicion.setPorcentajeParticipacion(porcentajeParticipacion);
		
		Double montoPagoPrimaNeta = pagoCobertura.getMontoPagoPrimaNeta() * factorParticipacion.doubleValue();
		exhibicion.setMontoPagoPrimaNeta(montoPagoPrimaNeta);
		
		Double porcentajeComision = participacion.getComision().doubleValue();
		exhibicion.setPorcentajeComision(porcentajeComision);
		
		Double montoComision = montoPagoPrimaNeta * porcentajeComision / 100D;
		exhibicion.setMontoComision(montoComision);
		
		if(reasegurador.getImpuestoResidenciaFiscal() == null ||
		   reasegurador.getTieneConstanciaResidenciaFiscal() == null ||
		   reasegurador.getFechaFinVigenciaConstanciaResidenciaFiscal() == null){
				LogDeMidasEJB3.log("El reasegurador "+reasegurador.getNombre()+" (id="+reasegurador.getIdtcreaseguradorcorredor()+ ") " +
								   "no tiene configurados correctamente los datos de impuesto por falta de constancia de residencia fiscal " +
								   "(idTdContratoFacultativo = "+pagoCobertura.getPlanPagosCobertura().getDetalleContratoFacultativoDTO().getContratoFacultativoDTO()+", " +								   		
								   	"idToPlanPagosCobertura = "+pagoCobertura.getId().getIdToPlanPagosCobertura()+", "+
								   	"numeroExhibicion = "+pagoCobertura.getId().getNumeroExhibicion()+", "+
								   	"idTdParticipacionFacultativo = "+participacion.getParticipacionFacultativoDTO()+", "+
								   	"idTdParticipacionCorredorFac = "+(participacionCorredor != null ? participacionCorredor.getIdTdParticapacionCorredorFac() : "null"), 
					Level.INFO, null);
			
				throw new ExcepcionDeLogicaNegocio("PlanPagosCoberturaFacade", "El reasegurador "+reasegurador.getNombre()+" no tiene configurados correctamente los datos de impuesto " +
									"por falta de constancia de residencia fiscal. Es necesario realizar esta configuraci�n para continuar.");								
		}
		
		Double porcentajeImpuesto = reasegurador.getImpuestoResidenciaFiscal().getPorcentaje();
		
		exhibicion.setPorcentajeImpuesto(porcentajeImpuesto);
		
		Double montoImpuesto = montoPagoPrimaNeta * porcentajeImpuesto / 100D;
		exhibicion.setMontoImpuesto(montoImpuesto);
		
		Short tieneConstanciaResidenciaFiscal = reasegurador.getTieneConstanciaResidenciaFiscal();
		Date fechaFinVigenciaConstanciaResidenciaFiscal = reasegurador.getFechaFinVigenciaConstanciaResidenciaFiscal();
		Boolean retenerImpuesto = false;
		
		if(tieneConstanciaResidenciaFiscal==0 || fechaFinVigenciaConstanciaResidenciaFiscal.compareTo(new Date())<0)
			retenerImpuesto = true;
		
		
		exhibicion.setRetenerImpuesto(retenerImpuesto);
		
		Double montoImpuestoFinal = retenerImpuesto ? montoImpuesto : 0D;
		
		Double montoPagoTotal = montoPagoPrimaNeta-montoComision-montoImpuestoFinal;
		
		exhibicion.setMontoPagoTotal(montoPagoTotal);
		
		final Byte AUTORIZACION_NO_REQUERIDA = 0; 
		
		exhibicion.setEstatusAutorizacionExhibicionVencida(AUTORIZACION_NO_REQUERIDA);
		exhibicion.setFechaSolicitudAutorizacionExhibicionVencida(null);
		exhibicion.setUsuarioSolicitudAutorizacionExhibicionVencida(null);
		exhibicion.setFechaAutorizacionExhibicionVencida(null);				
		exhibicion.setUsuarioAutorizacionExhibicionVencida(null);
		
		exhibicion.setEstatusAutorizacionFaltaPagoAsegurado(AUTORIZACION_NO_REQUERIDA);				
		exhibicion.setFechaSolicitudAutorizacionFaltaPagoAsegurado(null);
		exhibicion.setUsuarioSolicitudAutorizacionFaltaPagoAsegurado(null);
		exhibicion.setFechaAutorizacionFaltaPagoAsegurado(null);				
		exhibicion.setUsuarioAutorizacionFaltaPagoAsegurado(null);
		
		exhibicion.setEgresoReaseguro(null);
		exhibicion.setEstadoCuenta(null);
		
		LogDeMidasEJB3.log("Termina la generaci�n de exhibici�n (idTdContratoFacultativo = "+pagoCobertura.getPlanPagosCobertura().getDetalleContratoFacultativoDTO().getContratoFacultativoDTO()+", "+
				"idToPlanPagosCobertura = "+pagoCobertura.getId().getIdToPlanPagosCobertura()+", "+
				"numeroExhibicion = "+pagoCobertura.getId().getNumeroExhibicion()+", "+
				"idTdParticipacionFacultativo = "+participacion.getParticipacionFacultativoDTO()+", "+
				"idTdParticipacionCorredorFac = "+(participacionCorredor != null ? participacionCorredor.getIdTdParticapacionCorredorFac() : "null"), 
				Level.INFO, null);
		
		return exhibicion;
	}
	
	private void guardarExhibiciones(BigDecimal idTdContratoFacultativo, List<PagoCoberturaReaseguradorDTO> exhibiciones)throws ExcepcionDeLogicaNegocio{		
		this.eliminarExhibicionesPorDetalleContrato(idTdContratoFacultativo);		
		for(PagoCoberturaReaseguradorDTO exhibicion : exhibiciones){
			pagoCoberturaReasegurador.save(exhibicion);			
		}
		entityManager.flush();
	}		
	
	public void eliminarPlanPagosPorIdContratoFacultativo(BigDecimal idTmContratoFacultativo) throws ExcepcionDeLogicaNegocio{
		this.eliminarExhibicionesPorContrato(idTmContratoFacultativo);
		pagoCobertura.eliminarPagosPorContrato(idTmContratoFacultativo);
		
		LogDeMidasEJB3.log("Eliminando plan de pagos para el contrato facultativo (idTmContratoFacultativo="+idTmContratoFacultativo+")", Level.INFO, null);
		try{
			String queryString = "DELETE " +
							  	 "FROM PlanPagosCoberturaDTO planPagos " +
							  	 "WHERE planPagos.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo="+idTmContratoFacultativo;
			Query query = entityManager.createQuery(queryString);
			query.executeUpdate();
			entityManager.flush();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� PlanPagosCoberturaReaseguradorFacade.eliminarPlanPagosPorIdContratoFacultativo (idTmContratoFacultativo="+idTmContratoFacultativo+")", Level.SEVERE, re);
			throw re;
		}		
	}
	
	@SuppressWarnings("unchecked")
	private void eliminarExhibicionesPorDetalleContrato(BigDecimal idTdContratoFacultativo)throws ExcepcionDeLogicaNegocio{
		List<PagoCoberturaReaseguradorDTO> pagosNoPendientes = null;
		try{
		final Byte PAGO_PENDIENTE = 0;
		String queryString = "SELECT exhibicion " +
							 "FROM PagoCoberturaReaseguradorDTO exhibicion " +
							 "WHERE exhibicion.pagoCobertura.planPagosCobertura.detalleContratoFacultativoDTO.contratoFacultativoDTO="+idTdContratoFacultativo+" "+
							 "AND exhibicion.estatusPago<>"+PAGO_PENDIENTE;
		Query query = entityManager.createQuery(queryString);
		pagosNoPendientes = query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� la validaci�n de pagos pendientes en PlanPagosCoberturaeliminarExhibicionesPorDetalleContrato (idTdContratoFacultativo="+idTdContratoFacultativo+")", Level.SEVERE, re);
			throw re;
		}
		
		if(pagosNoPendientes != null && pagosNoPendientes.size() > 0){
			
			DetalleContratoFacultativoDTO detalle = detalleContratoFacultativo.findById(idTdContratoFacultativo);
			BigDecimal idToCobertura = detalle.getIdToCobertura();
			CoberturaDTO coberturaDTO = cobertura.findById(idToCobertura);
			String nombreCobertura = coberturaDTO.getNombreComercial();
			String idContratoFacultativo = detalle.getContratoFacultativoDTO_1().getIdTmContratoFacultativo().toString();
			
			String mensajeError = "No es posible eliminar el plan de pagos, ya que existen exhibiciones pagadas o en proceso de pago. (idTdContratoFacultativo="+idTdContratoFacultativo+") - " +
			"Exhibiciones pagadas/en proceso de pago: ";
			
			mensajeError += obtenerMensajeError(pagosNoPendientes);
			LogDeMidasEJB3.log(mensajeError, Level.SEVERE, null);
			throw new ExcepcionDeLogicaNegocio("PlanPagosCoberturaFacade", "No es posible eliminar el plan de pagos de la cobertura "+nombreCobertura+" del contrato facultativo "+idContratoFacultativo+", ya que existen exhibiciones pagadas o en proceso de pago.");
		}
		
		pagoCoberturaReasegurador.eliminarExhibicionesPorDetalleContrato(idTdContratoFacultativo);
	}
	
	@SuppressWarnings("unchecked")
	private void eliminarExhibicionesPorContrato(BigDecimal idTmContratoFacultativo)throws ExcepcionDeLogicaNegocio{
		List<PagoCoberturaReaseguradorDTO> pagosNoPendientes = null;
		try{
		final Byte PAGO_PENDIENTE = 0;
		String queryString = "SELECT exhibicion " +
							 "FROM PagoCoberturaReaseguradorDTO exhibicion " +
							 "WHERE exhibicion.pagoCobertura.planPagosCobertura.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo="+idTmContratoFacultativo+" "+
							 "AND exhibicion.estatusPago<>"+PAGO_PENDIENTE;
		Query query = entityManager.createQuery(queryString);
		pagosNoPendientes = query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� la validaci�n de pagos pendientes en PlanPagosCobertura.eliminarExhibicionesPorContrato (idTmContratoFacultativo="+idTmContratoFacultativo+")", Level.SEVERE, re);
			throw re;
		}
		
		if(pagosNoPendientes != null && pagosNoPendientes.size() > 0){
			String mensajeError = "No es posible eliminar el plan de pagos, ya que existen exhibiciones pagadas o en proceso de pago. (idTmContratoFacultativo="+idTmContratoFacultativo+") - " +
			"Exhibiciones pagadas/en proceso de pago: ";
			
			mensajeError += obtenerMensajeError(pagosNoPendientes);
			LogDeMidasEJB3.log(mensajeError, Level.SEVERE, null);
			throw new ExcepcionDeLogicaNegocio("PlanPagosCoberturaFacade", "No es posible eliminar el plan de pagos de a cobertura del contrato facultativo "+idTmContratoFacultativo+", ya que existen exhibiciones pagadas o en proceso de pago.");
		}
		
		pagoCoberturaReasegurador.eliminarExhibicionesPorContrato(idTmContratoFacultativo);
	}
	
	public String obtenerMensajeError(
			List<PagoCoberturaReaseguradorDTO> pagosNoPendientes) {
		StringBuilder mensajeError = new StringBuilder("");
		if (pagosNoPendientes.isEmpty()) return mensajeError.toString();
		for(PagoCoberturaReaseguradorDTO pago : pagosNoPendientes){
			mensajeError.append("(idToPlanPagosCobertura = ").append(pago.getId().getIdToPlanPagosCobertura() ).append(",").append(
							" numeroExhibicion = " ).append(pago.getId().getNumeroExhibicion()).append("," ).append(
							" idReasegurador = " ).append(pago.getId().getIdReasegurador() ).append( "), ");
		}
	
		return mensajeError.substring(0, mensajeError.length()-2);
		
	}

	public void autorizarPlanPagosPorContratoFacultativo(BigDecimal idTdContratoFacultativo){
		List<PlanPagosCoberturaDTO> listaPlanPagosCobertura = this.findByProperty("detalleContratoFacultativoDTO.contratoFacultativoDTO", idTdContratoFacultativo);
		if(listaPlanPagosCobertura.size()>0){
			PlanPagosCoberturaDTO planPagosCobertura = listaPlanPagosCobertura.get(0);
			if(planPagosCobertura.getEstatusAutorizacionPlanPagos().compareTo(AUTORIZACION_NO_REQUERIDA)==0){
				planPagosCobertura.setEstatusAutorizacionPlanPagos(AUTORIZADO);
				Date fechaAutorizacion = new Date();
				planPagosCobertura.setFechaSolicitudAutorizacionPlanPagos(fechaAutorizacion);
				planPagosCobertura.setUsuarioSolicitudAutorizacionPlanPagos("SISTEMA");
				planPagosCobertura.setFechaAutorizacionPlanPagos(fechaAutorizacion);
				planPagosCobertura.setUsuarioAutorizacionPlanPagos("SISTEMA");
				this.update(planPagosCobertura);
				entityManager.flush();
			}
		}
	}
		
	@SuppressWarnings("unchecked")
	public List<PagoCoberturaDecoradoDTO> listarExhibicionesAgrupadasPorCobertura(String idsEstadoCuenta){		
		String queryString = "SELECT EXH.IDTOPLANPAGOSCOBERTURA, EXH.IDREASEGURADOR, " + 
									"LPAD(POL.CODIGOPRODUCTO,2,'0')||" +
										"LPAD(POL.CODIGOTIPOPOLIZA,2,'0')||'-'||" +
											"LPAD(POL.NUMEROPOLIZA,6,'0')||'-'||LPAD(POL.NUMERORENOVACION,2,'0') POLIZA, " +
									"EDOCTA.NUMEROENDOSO ENDOSO, TDCON.NUMEROINCISO INCISO, TDCON.IDTOSECCION, SECC.NOMBRECOMERCIALSECCION SECCION, " +
									"TDCON.IDTOCOBERTURA, COB.NOMBRECOMERCIALCOBERTURA COBERTURA, TDCON.NUMEROSUBINCISO SUBINCISO, " +
									"REA.NOMBRE REASEGURADOR, COR.NOMBRE CORREDOR, SUM(PAGOCOB.MONTOPAGOPRIMANETA) PRIMA_REASEGURO, " +
									"EXH.PORCENTAJEPARTICIPACION PCT_PARTICIPACION, SUM(EXH.MONTOPAGOPRIMANETA) PARTICIPACION, " +
									"SUM(EXH.MONTOCOMISION) COMISION, IMP.PORCENTAJE PCT_IMPUESTO_REAL, " +
									"CASE " +
										"WHEN (REA.TIENECONSTANCIARESIDFISCAL = 1 AND REA.FECHAFINVIGCONSTRESFISCAL >= SYSDATE) " +
										"THEN '0' " +
										"ELSE '1' " +
									"END RETENER_IMPUESTO_REAL, " +
									"SUM(EXH.MONTOPAGOPRIMANETA) * IMP.PORCENTAJE / 100 * (CASE " +
																						  		"WHEN (REA.TIENECONSTANCIARESIDFISCAL = 1 AND REA.FECHAFINVIGCONSTRESFISCAL >= SYSDATE) " +
																						  		"THEN 0 " +
																						  		"ELSE 1 " +
																						  "END) IMPUESTO_REAL, " +
									"SUM(EXH.MONTOPAGOPRIMANETA) - SUM(EXH.MONTOCOMISION) - SUM(EXH.MONTOPAGOPRIMANETA) * IMP.PORCENTAJE / 100 * (CASE " +
																																					"WHEN (REA.TIENECONSTANCIARESIDFISCAL = 1 " +
																																					  "AND REA.FECHAFINVIGCONSTRESFISCAL >= SYSDATE) " +
																																					"THEN 0 " +
																																					"ELSE 1 " +
																																				 "END) PN_REASEGURO, " +
									"EDOCTA.IDTOESTADOCUENTA " + 
							 "FROM MIDAS.TDPAGOCOBERTURAREASEGURADOR EXH " +
							 	  "INNER JOIN MIDAS.TMPAGOCOBERTURA PAGOCOB ON EXH.IDTOPLANPAGOSCOBERTURA = PAGOCOB.IDTOPLANPAGOSCOBERTURA " +
                                                  							  "AND EXH.NUMEROEXHIBICION = PAGOCOB.NUMEROEXHIBICION " +
                                  "INNER JOIN MIDAS.TOPLANPAGOSCOBERTURA PLANPAGO ON PAGOCOB.IDTOPLANPAGOSCOBERTURA = PLANPAGO.IDTOPLANPAGOSCOBERTURA " +
                                  "INNER JOIN MIDAS.TDCONTRATOFACULTATIVO TDCON ON PLANPAGO.IDTDCONTRATOFACULTATIVO = TDCON.IDTDCONTRATOFACULTATIVO " +
                                  "INNER JOIN MIDAS.TMCONTRATOFACULTATIVO TMCON ON TDCON.IDTMCONTRATOFACULTATIVO = TMCON.IDTMCONTRATOFACULTATIVO " +
                                  "INNER JOIN MIDAS.TOESTADOCUENTA EDOCTA ON TMCON.IDTMCONTRATOFACULTATIVO = EDOCTA.IDTMCONTRATOFACULTATIVO " +
							                                  	  "AND NVL(EDOCTA.IDTCCORREDOR,0) = NVL(EXH.IDCORREDOR,0) " +
							                                  	  "AND EDOCTA.IDTCREASEGURADOR = EXH.IDREASEGURADOR " +
                                  "INNER JOIN MIDAS.TOPOLIZA POL ON EDOCTA.IDTOPOLIZA = POL.IDTOPOLIZA " +
                                  "INNER JOIN MIDAS.TCREASEGURADORCORREDOR REA ON EXH.IDREASEGURADOR = REA.IDTCREASEGURADORCORREDOR " +
                                  "LEFT OUTER JOIN MIDAS.TCREASEGURADORCORREDOR COR ON EXH.IDCORREDOR = COR.IDTCREASEGURADORCORREDOR " +
                                  "INNER JOIN MIDAS.TOSECCION SECC ON TDCON.IDTOSECCION = SECC.IDTOSECCION " +
                                  "INNER JOIN MIDAS.TOCOBERTURA COB ON TDCON.IDTOCOBERTURA = COB.IDTOCOBERTURA " + 
                                  "LEFT OUTER JOIN MIDAS.TCIMPUESTORESIDENCIAFISCAL IMP ON REA.IDTCIMPUESTORESIDENCIAFISCAL = IMP.IDTCIMPUESTORESIDENCIAFISCAL " +           
                                  "WHERE EDOCTA.IDTOESTADOCUENTA IN (" +  idsEstadoCuenta + ") " +                                  	
                             "GROUP BY EXH.IDTOPLANPAGOSCOBERTURA, EXH.IDREASEGURADOR, POL.CODIGOPRODUCTO, POL.CODIGOTIPOPOLIZA, POL.NUMEROPOLIZA, POL.NUMERORENOVACION, " +
                             		  "EDOCTA.NUMEROENDOSO, TDCON.NUMEROINCISO, TDCON.IDTOSECCION, SECC.NOMBRECOMERCIALSECCION, TDCON.IDTOCOBERTURA, COB.NOMBRECOMERCIALCOBERTURA, " +
                             		  "TDCON.NUMEROSUBINCISO, REA.NOMBRE, COR.NOMBRE, EXH.PORCENTAJEPARTICIPACION, IMP.PORCENTAJE, " +
                             		  "REA.TIENECONSTANCIARESIDFISCAL, REA.FECHAFINVIGCONSTRESFISCAL, EDOCTA.IDTOESTADOCUENTA " +
                             "ORDER BY POL.CODIGOPRODUCTO, POL.CODIGOTIPOPOLIZA, POL.NUMEROPOLIZA, POL.NUMERORENOVACION, EDOCTA.NUMEROENDOSO, TDCON.NUMEROINCISO, " + 
                             		  "TDCON.IDTOSECCION, TDCON.IDTOCOBERTURA, TDCON.NUMEROSUBINCISO, EXH.IDREASEGURADOR";
		
		Query query = entityManager.createNativeQuery(queryString, "coberturaReaseguradorMapping");
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<PagoCoberturaReaseguradorDecoradoDTO> listarExhibicionesCoberturasReaseguradores(String idsCoberturasReaseguradores){
		actualizarImpuestosExhibicionesCoberturasReaseguradores(idsCoberturasReaseguradores);
		String queryString = "SELECT EXH.IDTOPLANPAGOSCOBERTURA, EXH.NUMEROEXHIBICION, EXH.IDREASEGURADOR, " + 
							 		"LPAD(POL.CODIGOPRODUCTO,2,'0')||LPAD(POL.CODIGOTIPOPOLIZA,2,'0')||'-'||LPAD(POL.NUMEROPOLIZA,6,'0')||'-'||LPAD(POL.NUMERORENOVACION,2,'0')  POLIZA, " +
							 		"EDOCTA.NUMEROENDOSO ENDOSO, TDCON.NUMEROINCISO INCISO, TDCON.IDTOSECCION, SECC.NOMBRECOMERCIALSECCION SECCION, " +
							 		"TDCON.IDTOCOBERTURA, COB.NOMBRECOMERCIALCOBERTURA COBERTURA, TDCON.NUMEROSUBINCISO SUBINCISO, " +
							 		"REA.NOMBRE REASEGURADOR, TO_CHAR(PAGOCOB.FECHAPAGO,'DD/MM/YYYY') FECHAPAGO, EXH.MONTOPAGOTOTAL, EXH.ESTATUSPAGO, " +
							 		"EXH.ESTATUSAUTEXHVENCIDA, EXH.FECHASOLAUTEXHVENCIDA, EXH.USUARIOSOLAUTEXHVENCIDA, EXH.FECHAAUTEXHVENCIDA, EXH.USUARIOAUTEXHVENCIDA, " +
							 		"EXH.ESTATUSAUTFALTAPAGOASEG, EXH.FECHASOLAUTFALTAPAGOASEG, EXH.USUARIOSOLAUTFALTAPAGOASEG, EXH.FECHAAUTFALTAPAGOASEG, EXH.USUARIOAUTFALTAPAGOASEG, " +
							 		"EDOCTA.IDTOESTADOCUENTA " +
							 "FROM MIDAS.TDPAGOCOBERTURAREASEGURADOR EXH " +
							 	  "INNER JOIN MIDAS.TMPAGOCOBERTURA PAGOCOB ON EXH.IDTOPLANPAGOSCOBERTURA = PAGOCOB.IDTOPLANPAGOSCOBERTURA " +
                                                  							  "AND EXH.NUMEROEXHIBICION = PAGOCOB.NUMEROEXHIBICION " +
                                  "INNER JOIN MIDAS.TOPLANPAGOSCOBERTURA PLANPAGO ON PAGOCOB.IDTOPLANPAGOSCOBERTURA = PLANPAGO.IDTOPLANPAGOSCOBERTURA " +
                                  "INNER JOIN MIDAS.TDCONTRATOFACULTATIVO TDCON ON PLANPAGO.IDTDCONTRATOFACULTATIVO = TDCON.IDTDCONTRATOFACULTATIVO " +
                                  "INNER JOIN MIDAS.TMCONTRATOFACULTATIVO TMCON ON TDCON.IDTMCONTRATOFACULTATIVO = TMCON.IDTMCONTRATOFACULTATIVO " +
                                  "INNER JOIN MIDAS.TOESTADOCUENTA EDOCTA ON TMCON.IDTMCONTRATOFACULTATIVO = EDOCTA.IDTMCONTRATOFACULTATIVO " +
                                  									  "AND NVL(EDOCTA.IDTCCORREDOR,0) = NVL(EXH.IDCORREDOR,0) " +
                                  									  "AND EDOCTA.IDTCREASEGURADOR = EXH.IDREASEGURADOR " +
                                  "INNER JOIN MIDAS.TOPOLIZA POL ON EDOCTA.IDTOPOLIZA = POL.IDTOPOLIZA " +
                                  "INNER JOIN MIDAS.TCREASEGURADORCORREDOR REA ON EXH.IDREASEGURADOR = REA.IDTCREASEGURADORCORREDOR " +     
                                  "INNER JOIN MIDAS.TOSECCION SECC ON TDCON.IDTOSECCION = SECC.IDTOSECCION " +
                                  "INNER JOIN MIDAS.TOCOBERTURA COB ON TDCON.IDTOCOBERTURA = COB.IDTOCOBERTURA " +      
                             "WHERE (EXH.IDTOPLANPAGOSCOBERTURA, EXH.IDREASEGURADOR) IN ("+idsCoberturasReaseguradores+") " + 
                             "ORDER BY POL.CODIGOPRODUCTO, POL.CODIGOTIPOPOLIZA, POL.NUMEROPOLIZA, POL.NUMERORENOVACION, EDOCTA.NUMEROENDOSO, TDCON.NUMEROINCISO, " + 
                             		  "TDCON.IDTOSECCION, TDCON.IDTOCOBERTURA, TDCON.NUMEROSUBINCISO, EXH.IDREASEGURADOR, EXH.NUMEROEXHIBICION";

		Query query = entityManager.createNativeQuery(queryString, "exhibicionMapping");
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<PagoCoberturaReaseguradorDecoradoDTO> listarExhibicionesDecoradas(String idsExhibiciones){		
		String queryString = "SELECT EXH.IDTOPLANPAGOSCOBERTURA, EXH.NUMEROEXHIBICION, EXH.IDREASEGURADOR, " + 
							 		"EDOCTA.IDTOPOLIZA  POLIZA, " +
							 		"EDOCTA.NUMEROENDOSO ENDOSO, TDCON.NUMEROINCISO INCISO, TDCON.IDTOSECCION, SECC.NOMBRECOMERCIALSECCION SECCION, " +
							 		"TDCON.IDTOCOBERTURA, COB.NOMBRECOMERCIALCOBERTURA COBERTURA, TDCON.NUMEROSUBINCISO SUBINCISO, " +
							 		"REA.NOMBRE REASEGURADOR, TO_CHAR(PAGOCOB.FECHAPAGO,'DD/MM/YYYY') FECHAPAGO, EXH.MONTOPAGOTOTAL, EXH.ESTATUSPAGO, " +
							 		"EXH.ESTATUSAUTEXHVENCIDA, EXH.FECHASOLAUTEXHVENCIDA, EXH.USUARIOSOLAUTEXHVENCIDA, EXH.FECHAAUTEXHVENCIDA, EXH.USUARIOAUTEXHVENCIDA, " +
							 		"EXH.ESTATUSAUTFALTAPAGOASEG, EXH.FECHASOLAUTFALTAPAGOASEG, EXH.USUARIOSOLAUTFALTAPAGOASEG, EXH.FECHAAUTFALTAPAGOASEG, EXH.USUARIOAUTFALTAPAGOASEG, " +
							 		"EDOCTA.IDTOESTADOCUENTA " +
							 "FROM MIDAS.TDPAGOCOBERTURAREASEGURADOR EXH " +
							 	  "INNER JOIN MIDAS.TMPAGOCOBERTURA PAGOCOB ON EXH.IDTOPLANPAGOSCOBERTURA = PAGOCOB.IDTOPLANPAGOSCOBERTURA " +
                                                  							  "AND EXH.NUMEROEXHIBICION = PAGOCOB.NUMEROEXHIBICION " +
                                  "INNER JOIN MIDAS.TOPLANPAGOSCOBERTURA PLANPAGO ON PAGOCOB.IDTOPLANPAGOSCOBERTURA = PLANPAGO.IDTOPLANPAGOSCOBERTURA " +
                                  "INNER JOIN MIDAS.TDCONTRATOFACULTATIVO TDCON ON PLANPAGO.IDTDCONTRATOFACULTATIVO = TDCON.IDTDCONTRATOFACULTATIVO " +
                                  "INNER JOIN MIDAS.TMCONTRATOFACULTATIVO TMCON ON TDCON.IDTMCONTRATOFACULTATIVO = TMCON.IDTMCONTRATOFACULTATIVO " +
                                  "INNER JOIN MIDAS.TOESTADOCUENTA EDOCTA ON TMCON.IDTMCONTRATOFACULTATIVO = EDOCTA.IDTMCONTRATOFACULTATIVO " +
                                  									  "AND NVL(EDOCTA.IDTCCORREDOR,0) = NVL(EXH.IDCORREDOR,0) " +
                                  									  "AND EDOCTA.IDTCREASEGURADOR = EXH.IDREASEGURADOR " +                                  
                                  "INNER JOIN MIDAS.TCREASEGURADORCORREDOR REA ON EXH.IDREASEGURADOR = REA.IDTCREASEGURADORCORREDOR " +     
                                  "INNER JOIN MIDAS.TOSECCION SECC ON TDCON.IDTOSECCION = SECC.IDTOSECCION " +
                                  "INNER JOIN MIDAS.TOCOBERTURA COB ON TDCON.IDTOCOBERTURA = COB.IDTOCOBERTURA " +      
                             "WHERE (EXH.IDTOPLANPAGOSCOBERTURA, EXH.NUMEROEXHIBICION, EXH.IDREASEGURADOR) IN ("+idsExhibiciones+") " + 
                             "ORDER BY EDOCTA.IDTOPOLIZA, EDOCTA.NUMEROENDOSO, TDCON.NUMEROINCISO, " + 
                             		  "TDCON.IDTOSECCION, TDCON.IDTOCOBERTURA, TDCON.NUMEROSUBINCISO, EXH.IDREASEGURADOR, EXH.NUMEROEXHIBICION";

		Query query = entityManager.createNativeQuery(queryString, "exhibicionMapping");
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private void actualizarImpuestosExhibicionesCoberturasReaseguradores(String idsCoberturasReaseguradores){
		//Se eliminan el primer y �ltimo parentesis
		String idsCoberturasReaseguradoresStr = idsCoberturasReaseguradores.substring(1, idsCoberturasReaseguradores.length()-1);
		
		//Se obtienen las parejas de idToPlanPagosCobertura y idReasegurador, separados por ",".
		String[] ids = idsCoberturasReaseguradoresStr.split("\\),\\(");
		
		for(String id : ids){
			String[] idArr = id.split(",");
			String idToPlanPagosCobertura = idArr[0];
			String idReasegurador = idArr[1];
			
			String queryString = "SELECT rea " +
			 "FROM ReaseguradorCorredorDTO rea " +
			 "WHERE rea.idtcreaseguradorcorredor = :idReasegurador";

			Query query = entityManager.createQuery(queryString);
			query.setParameter("idReasegurador", new BigDecimal(idReasegurador));

			ReaseguradorCorredorDTO reasegurador = (ReaseguradorCorredorDTO) query.getSingleResult();
															
			Short tieneConstanciaResidenciaFiscal = reasegurador.getTieneConstanciaResidenciaFiscal();
			Date fechaFinVigenciaConstanciaResidenciaFiscal = reasegurador.getFechaFinVigenciaConstanciaResidenciaFiscal();
			Double porcentajeImpuestoReal = reasegurador.getImpuestoResidenciaFiscal().getPorcentaje();
						
			//Si no tiene constancia o la constancia est� caducada, se retiene impuesto
			Boolean retenerImpuestoReal = tieneConstanciaResidenciaFiscal == 0 || 
										  fechaFinVigenciaConstanciaResidenciaFiscal.compareTo(new Date()) < 0;
			
			queryString = "SELECT exh " +
						  "FROM PagoCoberturaReaseguradorDTO exh " +
						  "WHERE exh.id.idToPlanPagosCobertura = :idToPlanPagosCobertura " +
						  "AND exh.id.idReasegurador = :idReasegurador " +
			  		      "AND exh.estatusPago = :estatusPago " +
						  "AND (exh.retenerImpuesto <> :retenerImpuestoReal " +
						  	   "OR exh.porcentajeImpuesto <> :porcentajeImpuestoReal)";	
			
			query = entityManager.createQuery(queryString);
			query.setParameter("idToPlanPagosCobertura", new Long(idToPlanPagosCobertura));
			query.setParameter("idReasegurador", new BigDecimal(idReasegurador));
			query.setParameter("estatusPago", PagoCoberturaReaseguradorDTO.PENDIENTE);
			query.setParameter("retenerImpuestoReal", retenerImpuestoReal);
			query.setParameter("porcentajeImpuestoReal", porcentajeImpuestoReal);			
			
			List<PagoCoberturaReaseguradorDTO> exhibicionesPorActualizar = query.getResultList();
									
			for(PagoCoberturaReaseguradorDTO exhibicion : exhibicionesPorActualizar){
					Double montoPagoPrimaNeta = exhibicion.getMontoPagoPrimaNeta();
					Double montoComision = exhibicion.getMontoComision();
					exhibicion.setRetenerImpuesto(retenerImpuestoReal);
					exhibicion.setPorcentajeImpuesto(porcentajeImpuestoReal);
					Double montoImpuesto = montoPagoPrimaNeta * porcentajeImpuestoReal / 100D;
					exhibicion.setMontoImpuesto(montoImpuesto);										
					Double montoImpuestoFinal = exhibicion.getRetenerImpuesto() ? montoImpuesto : 0D;						
					Double montoPagoTotal = montoPagoPrimaNeta-montoComision-montoImpuestoFinal;										
					exhibicion.setMontoPagoTotal(montoPagoTotal);
					
					String retenerImpuesto = exhibicion.getRetenerImpuesto() ? "1" : "0";
					
					String updateString = "UPDATE MIDAS.TDPAGOCOBERTURAREASEGURADOR " +
										  "SET RETENERIMPUESTO = "+retenerImpuesto+", " +
										      "PORCENTAJEIMPUESTO = "+exhibicion.getPorcentajeImpuesto().toString()+", " +
										      "MONTOIMPUESTO = "+exhibicion.getMontoImpuesto().toString()+", " +
										      "MONTOPAGOTOTAL = "+exhibicion.getMontoPagoTotal().toString()+ " " +
										  "WHERE IDTOPLANPAGOSCOBERTURA = "+exhibicion.getId().getIdToPlanPagosCobertura().toString()+" "+
										    "AND NUMEROEXHIBICION = "+exhibicion.getId().getNumeroExhibicion().toString()+" "+
										    "AND IDREASEGURADOR = "+exhibicion.getId().getIdReasegurador().toString();
					query = entityManager.createNativeQuery(updateString);					
					query.executeUpdate();										
			}			
		}
	}		
	
	public PagoCoberturaReaseguradorDTO obtenerExhibicionPorId(PagoCoberturaReaseguradorId id){
		return pagoCoberturaReasegurador.findById(id);
	}
	
	public PagoCoberturaReaseguradorDTO actualizarExhibicion(PagoCoberturaReaseguradorDTO exhibicion){
		return pagoCoberturaReasegurador.update(exhibicion);
	}
	
	public List<PagoCoberturaReaseguradorDTO> obtenerExhibicionesPorEgreso(BigDecimal idToEgresoReaseguro){
		return pagoCoberturaReasegurador.findByProperty("egresoReaseguro.idEgresoReaseguro", idToEgresoReaseguro);
	}		
}