package mx.com.afirme.midas.contratofacultativo.pagocobertura;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity PagoCoberturaReaseguradorDTO.
 * @see .PagoCoberturaReasegurador
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class PagoCoberturaReaseguradorFacade  implements PagoCoberturaReaseguradorFacadeRemote {	
    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved PagoCoberturaReaseguradorDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PagoCoberturaReaseguradorDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public PagoCoberturaReaseguradorDTO save(PagoCoberturaReaseguradorDTO entity) {
    				LogDeMidasEJB3.log("saving PagoCoberturaReaseguradorDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
	        }
	        return entity;
    }
    
    /**
	 Delete a persistent PagoCoberturaReaseguradorDTO entity.
	  @param entity PagoCoberturaReaseguradorDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PagoCoberturaReaseguradorDTO entity) {
    				LogDeMidasEJB3.log("deleting PagoCoberturaReaseguradorDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(PagoCoberturaReaseguradorDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved PagoCoberturaReaseguradorDTO entity and return it or a copy of it to the sender. 
	 A copy of the PagoCoberturaReaseguradorDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PagoCoberturaReaseguradorDTO entity to update
	 @return PagoCoberturaReaseguradorDTO the persisted PagoCoberturaReaseguradorDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public PagoCoberturaReaseguradorDTO update(PagoCoberturaReaseguradorDTO entity) {
    				LogDeMidasEJB3.log("updating PagoCoberturaReaseguradorDTO instance", Level.INFO, null);
	        try {
            PagoCoberturaReaseguradorDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public PagoCoberturaReaseguradorDTO findById( PagoCoberturaReaseguradorId id) {
    				LogDeMidasEJB3.log("finding PagoCoberturaReaseguradorDTO instance with id: " + id, Level.INFO, null);
	        try {
            PagoCoberturaReaseguradorDTO instance = entityManager.find(PagoCoberturaReaseguradorDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all PagoCoberturaReaseguradorDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PagoCoberturaReaseguradorDTO property to query
	  @param value the property value to match
	  	  @return List<PagoCoberturaReaseguradorDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<PagoCoberturaReaseguradorDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding PagoCoberturaReaseguradorDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from PagoCoberturaReaseguradorDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}	
	
	/**
	 * Find all PagoCoberturaReaseguradorDTO entities.
	  	  @return List<PagoCoberturaReaseguradorDTO> all PagoCoberturaReaseguradorDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<PagoCoberturaReaseguradorDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all PagoCoberturaReaseguradorDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from PagoCoberturaReaseguradorDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
		
	public void eliminarExhibicionesPorDetalleContrato(BigDecimal idTdContratoFacultativo){		
		LogDeMidasEJB3.log("Eliminando exhibiciones para el detalle de contrato facultativo (idTdContratoFacultativo="+idTdContratoFacultativo+")", Level.INFO, null);
		try{
			String queryString = "DELETE " +
							  	 "FROM PagoCoberturaReaseguradorDTO exhibicion " +
							  	 "WHERE exhibicion.pagoCobertura.planPagosCobertura.detalleContratoFacultativoDTO.contratoFacultativoDTO="+idTdContratoFacultativo;
			Query query = entityManager.createQuery(queryString);
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� PagoCoberturaReaseguradorFacade.eliminarExhibicionesPorDetalleContrato (idTdContratoFacultativo="+idTdContratoFacultativo+")", Level.SEVERE, re);
			throw re;
		}
	}	
	
	public void eliminarExhibicionesPorContrato(BigDecimal idTmContratoFacultativo){		
		LogDeMidasEJB3.log("Eliminando exhibiciones para el contrato facultativo (idTmContratoFacultativo="+idTmContratoFacultativo+")", Level.INFO, null);
		try{
			String queryString = "DELETE " +
							  	 "FROM PagoCoberturaReaseguradorDTO exhibicion " +
							  	 "WHERE exhibicion.pagoCobertura.planPagosCobertura.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo="+idTmContratoFacultativo;
			Query query = entityManager.createQuery(queryString);
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� PagoCoberturaReaseguradorFacade.eliminarExhibicionesPorContrato (idTdContratoFacultativo="+idTmContratoFacultativo+")", Level.SEVERE, re);
			throw re;
		}
	}
}