package mx.com.afirme.midas.contratofacultativo.pagocobertura;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity PagoCoberturaDTO.
 * @see .PagoCobertura
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class PagoCoberturaFacade  implements PagoCoberturaFacadeRemote {
    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved PagoCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PagoCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public PagoCoberturaDTO save(PagoCoberturaDTO entity) {
    				LogDeMidasEJB3.log("saving PagoCoberturaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            entityManager.flush();
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
	        }
	        return entity;
    }
    
    /**
	 Delete a persistent PagoCoberturaDTO entity.
	  @param entity PagoCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PagoCoberturaDTO entity) {
    				LogDeMidasEJB3.log("deleting PagoCoberturaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(PagoCoberturaDTO.class, entity.getId());
            entityManager.remove(entity);
            entityManager.flush();
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved PagoCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the PagoCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PagoCoberturaDTO entity to update
	 @return PagoCoberturaDTO the persisted PagoCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public PagoCoberturaDTO update(PagoCoberturaDTO entity) {
    				LogDeMidasEJB3.log("updating PagoCoberturaDTO instance", Level.INFO, null);
	        try {
            PagoCoberturaDTO result = entityManager.merge(entity);
            entityManager.flush();
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public PagoCoberturaDTO findById( PagoCoberturaId id) {
    				LogDeMidasEJB3.log("finding PagoCoberturaDTO instance with id: " + id, Level.INFO, null);
	        try {
            PagoCoberturaDTO instance = entityManager.find(PagoCoberturaDTO.class, id);
            entityManager.refresh(instance);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all PagoCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PagoCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<PagoCoberturaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<PagoCoberturaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding PagoCoberturaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model " +
									   "from PagoCoberturaDTO model " +
									   "where model." + propertyName + "= :propertyValue " +
			 						   "order by model.id.numeroExhibicion";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}				
	
	
	/**
	 * Find all PagoCoberturaDTO entities.
	  	  @return List<PagoCoberturaDTO> all PagoCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<PagoCoberturaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all PagoCoberturaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from PagoCoberturaDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<PagoCoberturaDTO> buscarPorDetalleContratoFacultativo(BigDecimal idTdContratoFacultativo){
		LogDeMidasEJB3.log("Buscando PagoCobertura por DetalleContratoFacultativo", Level.INFO, null);
		try {
			final String queryString = "select pago from PagoCoberturaDTO model " +
									   "where pago.planPagosCobertura.detalleContratoFacultativoDTO.contratoFacultativoDTO = :idTdContratoFacultativo";
							Query query = entityManager.createQuery(queryString);
							query.setParameter(":idTdContratoFacultativo", idTdContratoFacultativo);
				return query.getResultList();
		} catch (RuntimeException re) {
					LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void eliminarPagosPorContrato(BigDecimal idTmContratoFacultativo){		
		LogDeMidasEJB3.log("Eliminando pagos para el contrato facultativo (idTmContratoFacultativo="+idTmContratoFacultativo+")", Level.INFO, null);
		try{
			String queryString = "DELETE " +
							  	 "FROM PagoCoberturaDTO pago " +
							  	 "WHERE pago.planPagosCobertura.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo="+idTmContratoFacultativo;
			Query query = entityManager.createQuery(queryString);
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� PagoCoberturaFacade.eliminarPagosPorContrato (idTmContratoFacultativo="+idTmContratoFacultativo+")", Level.SEVERE, re);
			throw re;
		}
	}	
	
}