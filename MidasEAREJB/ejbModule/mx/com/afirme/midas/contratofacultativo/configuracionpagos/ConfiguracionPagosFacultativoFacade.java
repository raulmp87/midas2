package mx.com.afirme.midas.contratofacultativo.configuracionpagos;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity ConfiguracionPagosFacultativoDTO.
 * @see .ConfiguracionPagosFacultativoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class ConfiguracionPagosFacultativoFacade  implements ConfiguracionPagosFacultativoFacadeRemote {


    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ConfiguracionPagosFacultativoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ConfiguracionPagosFacultativoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public void save(ConfiguracionPagosFacultativoDTO entity) {
    				LogUtil.log("saving ConfiguracionPagosFacultativoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ConfiguracionPagosFacultativoDTO entity.
	  @param entity ConfiguracionPagosFacultativoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public void delete(ConfiguracionPagosFacultativoDTO entity) {
    				LogUtil.log("deleting ConfiguracionPagosFacultativoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ConfiguracionPagosFacultativoDTO.class, entity.getIdToConfiguracionPagosFacultativo());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ConfiguracionPagosFacultativoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ConfiguracionPagosFacultativoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ConfiguracionPagosFacultativoDTO entity to update
	 @return ConfiguracionPagosFacultativoDTO the persisted ConfiguracionPagosFacultativoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public ConfiguracionPagosFacultativoDTO update(ConfiguracionPagosFacultativoDTO entity) {
    				LogUtil.log("updating ConfiguracionPagosFacultativoDTO instance", Level.INFO, null);
	        try {
            ConfiguracionPagosFacultativoDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public ConfiguracionPagosFacultativoDTO findById( BigDecimal id) {
    				LogUtil.log("finding ConfiguracionPagosFacultativoDTO instance with id: " + id, Level.INFO, null);
	        try {
            ConfiguracionPagosFacultativoDTO instance = entityManager.find(ConfiguracionPagosFacultativoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ConfiguracionPagosFacultativoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ConfiguracionPagosFacultativoDTO property to query
	  @param value the property value to match
	  	  @return List<ConfiguracionPagosFacultativoDTO> found by query
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    @SuppressWarnings("unchecked")
    public List<ConfiguracionPagosFacultativoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding ConfiguracionPagosFacultativoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ConfiguracionPagosFacultativoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ConfiguracionPagosFacultativoDTO entities.
	  	  @return List<ConfiguracionPagosFacultativoDTO> all ConfiguracionPagosFacultativoDTO entities
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
	@SuppressWarnings("unchecked")
	public List<ConfiguracionPagosFacultativoDTO> findAll(
		) {
					LogUtil.log("finding all ConfiguracionPagosFacultativoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ConfiguracionPagosFacultativoDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
    
    @SuppressWarnings("unchecked")
	public List<ConfiguracionPagosFacultativoDTO> listarPorReasegurador(BigDecimal idtcreaseguradorcorredor, BigDecimal idTmContratoFacultativo){
    	String queryString = "SELECT model FROM ConfiguracionPagosFacultativoDTO model WHERE " +
    			"model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor AND " +
    			"model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo ORDER BY model.fechaPago";
    	Query query = entityManager.createQuery(queryString);
    	query.setParameter("idtcreaseguradorcorredor", idtcreaseguradorcorredor);
    	query.setParameter("idTmContratoFacultativo", idTmContratoFacultativo);
    	return query.getResultList();
    }
	
}