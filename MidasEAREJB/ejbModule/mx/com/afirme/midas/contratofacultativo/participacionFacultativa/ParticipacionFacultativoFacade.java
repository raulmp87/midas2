package mx.com.afirme.midas.contratofacultativo.participacionFacultativa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ParticipacionFacultativoDTO.
 * 
 * @see .ParticipacionFacultativoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ParticipacionFacultativoFacade implements
		ParticipacionFacultativoFacadeRemote {
	// property constants
	public static final String PORCENTAJE_RETENCION = "porcentajeRetencion";
	public static final String PRIRMARETENCION = "prirmaretencion";
	public static final String PORCENTAJE_PARTICIPACION = "porcentajeParticipacion";
	public static final String COMISION = "comision";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * ParticipacionFacultativoDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ParticipacionFacultativoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ParticipacionFacultativoDTO entity) {
		LogDeMidasEJB3.log("saving ParticipacionFacultativoDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			entityManager.flush();
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ParticipacionFacultativoDTO entity.
	 * 
	 * @param entity
	 *            ParticipacionFacultativoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ParticipacionFacultativoDTO entity) {
		LogDeMidasEJB3.log("deleting ParticipacionFacultativoDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					ParticipacionFacultativoDTO.class, entity
							.getParticipacionFacultativoDTO());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ParticipacionFacultativoDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * ParticipacionFacultativoDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            ParticipacionFacultativoDTO entity to update
	 * @return ParticipacionFacultativoDTO the persisted
	 *         ParticipacionFacultativoDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ParticipacionFacultativoDTO update(ParticipacionFacultativoDTO entity) {
		LogDeMidasEJB3.log("updating ParticipacionFacultativoDTO instance",
				Level.INFO, null);
		try {
			ParticipacionFacultativoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ParticipacionFacultativoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding ParticipacionFacultativoDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			ParticipacionFacultativoDTO instance = entityManager.find(
					ParticipacionFacultativoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ParticipacionFacultativoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the ParticipacionFacultativoDTO property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<ParticipacionFacultativoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipacionFacultativoDTO> findByProperty(
			String propertyName, final Object value,
			final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log(
				"finding ParticipacionFacultativoDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ParticipacionFacultativoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<ParticipacionFacultativoDTO> findByPorcentajeRetencion(
			Object porcentajeRetencion, int... rowStartIdxAndCount) {
		return findByProperty(PORCENTAJE_RETENCION, porcentajeRetencion,
				rowStartIdxAndCount);
	}

	public List<ParticipacionFacultativoDTO> findByPrirmaretencion(
			Object prirmaretencion, int... rowStartIdxAndCount) {
		return findByProperty(PRIRMARETENCION, prirmaretencion,
				rowStartIdxAndCount);
	}

	public List<ParticipacionFacultativoDTO> findByPorcentajeParticipacion(
			Object porcentajeParticipacion, int... rowStartIdxAndCount) {
		return findByProperty(PORCENTAJE_PARTICIPACION,
				porcentajeParticipacion, rowStartIdxAndCount);
	}

	public List<ParticipacionFacultativoDTO> findByComision(Object comision,
			int... rowStartIdxAndCount) {
		return findByProperty(COMISION, comision, rowStartIdxAndCount);
	}

	/**
	 * Find all ParticipacionFacultativoDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ParticipacionFacultativoDTO> all ParticipacionFacultativoDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipacionFacultativoDTO> findAll(
			final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log("finding all ParticipacionFacultativoDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from ParticipacionFacultativoDTO model";
			Query query = entityManager.createQuery(queryString);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ParticipacionFacultativoDTO> listarFiltrado(ParticipacionFacultativoDTO participacionFacultativoDTO){
		try {
			entityManager.flush();
			String queryString = "select model from ParticipacionFacultativoDTO model ";
			String sWhere = "";
			Query query;
	    	List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (participacionFacultativoDTO == null)
				return null;
			
			if (participacionFacultativoDTO.getDetalleContratoFacultativoDTO() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "detalleContratoFacultativoDTO.contratoFacultativoDTO", participacionFacultativoDTO.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO(), "contratoFacultativoDTO");
			 				
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}		
	}
	
	
	

}