package mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ParticipacionCorredorFacultativoDTO.
 * 
 * @see .ParticipacionCorredorFacultativoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ParticipacionCorredorFacultativoFacade implements
		ParticipacionCorredorFacultativoFacadeRemote {
	// property constants
	public static final String PORCENTAJE_PARTICIPACION = "porcentajeParticipacion";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * ParticipacionCorredorFacultativoDTO entity. All subsequent persist
	 * actions of this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ParticipacionCorredorFacultativoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ParticipacionCorredorFacultativoDTO entity) {
		LogDeMidasEJB3.log("saving ParticipacionCorredorFacultativoDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ParticipacionCorredorFacultativoDTO entity.
	 * 
	 * @param entity
	 *            ParticipacionCorredorFacultativoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ParticipacionCorredorFacultativoDTO entity) {
		LogDeMidasEJB3.log("deleting ParticipacionCorredorFacultativoDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					ParticipacionCorredorFacultativoDTO.class, entity
							.getIdTdParticapacionCorredorFac());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ParticipacionCorredorFacultativoDTO entity and
	 * return it or a copy of it to the sender. A copy of the
	 * ParticipacionCorredorFacultativoDTO entity parameter is returned when the
	 * JPA persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            ParticipacionCorredorFacultativoDTO entity to update
	 * @return ParticipacionCorredorFacultativoDTO the persisted
	 *         ParticipacionCorredorFacultativoDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ParticipacionCorredorFacultativoDTO update(
			ParticipacionCorredorFacultativoDTO entity) {
		LogDeMidasEJB3.log("updating ParticipacionCorredorFacultativoDTO instance",
				Level.INFO, null);
		try {
			ParticipacionCorredorFacultativoDTO result = entityManager
					.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ParticipacionCorredorFacultativoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding ParticipacionCorredorFacultativoDTO instance with id: "
						+ id, Level.INFO, null);
		try {
			ParticipacionCorredorFacultativoDTO instance = entityManager.find(
					ParticipacionCorredorFacultativoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ParticipacionCorredorFacultativoDTO entities with a specific
	 * property value.
	 * 
	 * @param propertyName
	 *            the name of the ParticipacionCorredorFacultativoDTO property
	 *            to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<ParticipacionCorredorFacultativoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipacionCorredorFacultativoDTO> findByProperty(
			String propertyName, final Object value,
			final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log(
				"finding ParticipacionCorredorFacultativoDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ParticipacionCorredorFacultativoDTO model where model."
					+ propertyName + "= :propertyValue";
			@SuppressWarnings("unused")
			ParticipacionCorredorFacultativoDTO instance = entityManager.getReference(ParticipacionCorredorFacultativoDTO.class, value);
	        //entityManager.refresh(instance);
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<ParticipacionCorredorFacultativoDTO> findByPorcentajeParticipacion(
			Object porcentajeParticipacion, int... rowStartIdxAndCount) {
		return findByProperty(PORCENTAJE_PARTICIPACION,
				porcentajeParticipacion, rowStartIdxAndCount);
	}

	/**
	 * Find all ParticipacionCorredorFacultativoDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ParticipacionCorredorFacultativoDTO> all
	 *         ParticipacionCorredorFacultativoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipacionCorredorFacultativoDTO> findAll(
			final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log(
				"finding all ParticipacionCorredorFacultativoDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from ParticipacionCorredorFacultativoDTO model";
			Query query = entityManager.createQuery(queryString);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}