package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity SlipAnexoEquipoContratistaDTO.
 * 
 * @see .SlipAnexoEquipoContratistaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SlipAnexoEquipoContratistaFacade implements
		SlipAnexoEquipoContratistaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * SlipAnexoEquipoContratistaDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            SlipAnexoEquipoContratistaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipAnexoEquipoContratistaDTO entity) {
		LogDeMidasEJB3.log("saving SlipAnexoEquipoContratistaDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SlipAnexoEquipoContratistaDTO entity.
	 * 
	 * @param entity
	 *            SlipAnexoEquipoContratistaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipAnexoEquipoContratistaDTO entity) {
		LogDeMidasEJB3.log("deleting SlipAnexoEquipoContratistaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					SlipAnexoEquipoContratistaDTO.class, entity
							.getIdToSlipDocumentoAnexo());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SlipAnexoEquipoContratistaDTO entity and
	 * return it or a copy of it to the sender. A copy of the
	 * SlipAnexoEquipoContratistaDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            SlipAnexoEquipoContratistaDTO entity to update
	 * @return SlipAnexoEquipoContratistaDTO the persisted
	 *         SlipAnexoEquipoContratistaDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipAnexoEquipoContratistaDTO update(
			SlipAnexoEquipoContratistaDTO entity) {
		LogDeMidasEJB3.log("updating SlipAnexoEquipoContratistaDTO instance",
				Level.INFO, null);
		try {
			SlipAnexoEquipoContratistaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SlipAnexoEquipoContratistaDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SlipAnexoEquipoContratistaDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			SlipAnexoEquipoContratistaDTO instance = entityManager.find(
					SlipAnexoEquipoContratistaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipAnexoEquipoContratistaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SlipAnexoEquipoContratistaDTO property to
	 *            query
	 * @param value
	 *            the property value to match
	 * @return List<SlipAnexoEquipoContratistaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SlipAnexoEquipoContratistaDTO> findByProperty(
			String propertyName, final Object value) {
		LogDeMidasEJB3.log(
				"finding SlipAnexoEquipoContratistaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SlipAnexoEquipoContratistaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipAnexoEquipoContratistaDTO entities.
	 * 
	 * @return List<SlipAnexoEquipoContratistaDTO> all
	 *         SlipAnexoEquipoContratistaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SlipAnexoEquipoContratistaDTO> findAll() {
		LogDeMidasEJB3.log("finding all SlipAnexoEquipoContratistaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from SlipAnexoEquipoContratistaDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}