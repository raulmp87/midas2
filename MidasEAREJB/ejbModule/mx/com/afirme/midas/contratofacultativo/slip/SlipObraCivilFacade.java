package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratofacultativo.slip.SlipObraCivilDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipObraCivilFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity SlipObraCivilDTO.
 * 
 * @see .SlipObraCivilDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SlipObraCivilFacade implements SlipObraCivilFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SlipObraCivilDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SlipObraCivilDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipObraCivilDTO entity) {
		LogDeMidasEJB3.log("saving SlipObraCivilDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SlipObraCivilDTO entity.
	 * 
	 * @param entity
	 *            SlipObraCivilDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipObraCivilDTO entity) {
		LogDeMidasEJB3.log("deleting SlipObraCivilDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SlipObraCivilDTO.class, entity
					.getIdToSlip());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SlipObraCivilDTO entity and return it or a
	 * copy of it to the sender. A copy of the SlipObraCivilDTO entity parameter
	 * is returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipObraCivilDTO entity to update
	 * @return SlipObraCivilDTO the persisted SlipObraCivilDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipObraCivilDTO update(SlipObraCivilDTO entity) {
		LogDeMidasEJB3.log("updating SlipObraCivilDTO instance", Level.INFO, null);
		try {
			SlipObraCivilDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SlipObraCivilDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SlipObraCivilDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SlipObraCivilDTO instance = entityManager.find(
					SlipObraCivilDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipObraCivilDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipObraCivilDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipObraCivilDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SlipObraCivilDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SlipObraCivilDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SlipObraCivilDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipObraCivilDTO entities.
	 * 
	 * @return List<SlipObraCivilDTO> all SlipObraCivilDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SlipObraCivilDTO> findAll() {
		LogDeMidasEJB3.log("finding all SlipObraCivilDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from SlipObraCivilDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}