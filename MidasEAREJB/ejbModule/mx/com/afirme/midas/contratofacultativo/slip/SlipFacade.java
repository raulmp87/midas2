package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoFacadeRemote;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoFacadeRemote;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaFacadeRemote;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroFacadeRemote;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipAviacionSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipBarcosSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipEquipoContratistaSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipGeneralSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipIncendioSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipObraCivilSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipPrimerRiesgoSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipRCConstructoresSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipRCFuncionarioSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipTransportesSoporteDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity SlipDTO.
 * @see .SlipDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class SlipFacade  implements SlipFacadeRemote {


	@EJB private ContratoFacultativoFacadeRemote contratoFacultativoFacade;
	@EJB private LineaSoporteCoberturaFacadeRemote lineaSoporteCoberturaFacade;
	
	@Resource
    private SessionContext context;

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved SlipDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SlipDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public SlipDTO save(SlipDTO entity) {
    				LogDeMidasEJB3.log("saving SlipDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
            			return entity;
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent SlipDTO entity.
	  @param entity SlipDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SlipDTO entity) {
    				LogDeMidasEJB3.log("deleting SlipDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(SlipDTO.class, entity.getIdToSlip());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved SlipDTO entity and return it or a copy of it to the sender. 
	 A copy of the SlipDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SlipDTO entity to update
	 @return SlipDTO the persisted SlipDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public SlipDTO update(SlipDTO entity) {
    				LogDeMidasEJB3.log("updating SlipDTO instance", Level.INFO, null);
	        try {
            SlipDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public SlipDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding SlipDTO instance with id: " + id, Level.INFO, null);
	        try {
            SlipDTO instance = entityManager.find(SlipDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all SlipDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SlipDTO property to query
	  @param value the property value to match
	  	  @return List<SlipDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<SlipDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding SlipDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from SlipDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all SlipDTO entities.
	  	  @return List<SlipDTO> all SlipDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SlipDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all SlipDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from SlipDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
   	public List<SlipDTO> listarCotizacionesEstatus(String estatus) {
		try {
			entityManager.flush();
			String queryString = "SELECT model FROM SlipDTO model ";
			String sWhere = "";
			Query query;
	  		sWhere = "model.estatus = 1 AND model.estatusCotizacion = "+BigDecimal.valueOf(Integer.valueOf(estatus)) +
	  		" ORDER BY model.idToCotizacion DESC"; 
	  		if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
	 		System.out.print("El query a ejecutar es " + queryString);
			query = entityManager.createQuery(queryString);
 			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<SlipDTO> listarSlipPorEstatus(BigDecimal idToCotizacion,String estatusCotizacion){
	 try{
		 String queryString = "select model from SlipDTO model where model.idToCotizacion = :idToCotizacion and model.estatusCotizacion  = :estatusCotizacion";
 		 Query query = entityManager.createQuery(queryString); 
		 query.setParameter("idToCotizacion", idToCotizacion);
		 query.setParameter("estatusCotizacion",BigDecimal.valueOf(Integer.valueOf(estatusCotizacion)));
         return query.getResultList();
	    }catch(RuntimeException re){
    	 LogDeMidasEJB3.log("fallo al buscar el slip por cotizacion", Level.SEVERE, re);
    	 throw re;
       }
     }
	
	 
	@SuppressWarnings("unchecked")
	public SlipDTO obtenerSlipLineaSoporte(LineaSoporteReaseguroDTO lineaSoporteReaseguro){
		SlipDTO slipDTO =  null;
		   String queryString = "select model from SlipDTO model where model.idTmLineaSoporteReaseguro = :idTmLineaSoporteReaseguro and";
		   queryString += " model.idToSoporteReaseguro = :idToSoporteReaseguro and";
		   queryString += " model.idToCotizacion = :idToCotizacion";
		   Query query = entityManager.createQuery(queryString);
		   query.setParameter("idTmLineaSoporteReaseguro", lineaSoporteReaseguro.getId().getIdTmLineaSoporteReaseguro());
		   query.setParameter("idToSoporteReaseguro", lineaSoporteReaseguro.getId().getIdToSoporteReaseguro());
		   query.setParameter("idToCotizacion", lineaSoporteReaseguro.getIdToCotizacion());
		   List listaResultado = query.getResultList();
// 		   if (query.getResultList().size() == 1){
//		     slipDTO  = (SlipDTO) query.getSingleResult();	   
//		   }
		   if (listaResultado.size() == 1){
			     slipDTO  = (SlipDTO) listaResultado.get(0);	   
			   }
   	    return slipDTO; 
	}
	
	@SuppressWarnings("unchecked")
	public SlipDTO obtenerSlipLineaSoporteDuplicado(LineaSoporteReaseguroDTO lineaSoporteReaseguro){
		SlipDTO slipDTO =  null;
		   String queryString = "select model from SlipDTO model where model.idTmLineaSoporteReaseguro = :idTmLineaSoporteReaseguro and";
		   queryString += " model.idToSoporteReaseguro = :idToSoporteReaseguro and";
		   queryString += " model.idToCotizacion = :idToCotizacion order by model.idToSlip desc";
		   Query query = entityManager.createQuery(queryString);
		   query.setParameter("idTmLineaSoporteReaseguro", lineaSoporteReaseguro.getId().getIdTmLineaSoporteReaseguro());
		   query.setParameter("idToSoporteReaseguro", lineaSoporteReaseguro.getId().getIdToSoporteReaseguro());
		   query.setParameter("idToCotizacion", lineaSoporteReaseguro.getIdToCotizacion());
		   List listaResultado = query.getResultList();
// 		   if (query.getResultList().size() == 1){
//		     slipDTO  = (SlipDTO) query.getSingleResult();	   
//		   }
		   if (listaResultado.size() >= 1){
			     slipDTO  = (SlipDTO) listaResultado.get(0);	   
			   }
   	    return slipDTO; 
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cancelarCotizacionFacultativo(BigDecimal idToCotizacion) throws RuntimeException{
		try{
			List<SlipDTO> slipDTOList = this.findByProperty("idToCotizacion", idToCotizacion);
			if (slipDTOList != null){
				for (SlipDTO slipDTO : slipDTOList) {
					contratoFacultativoFacade.cancelarContratoFacultativo(slipDTO.getIdToSlip());
				}
			}
		} catch (RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("cancelarCotizacionFacultativo failed", Level.SEVERE, re);
			throw re;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void modificarPorcentajesContratos(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO, SlipDTO slipDTO, ContratoFacultativoDTO contratoFacultativoDTO) {
		try{
			if (contratoFacultativoDTO != null){
				contratoFacultativoDTO = contratoFacultativoFacade.save(contratoFacultativoDTO);
				lineaSoporteReaseguroDTO.setContratoFacultativoDTO(contratoFacultativoDTO);
				if(lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() == null || lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty())
					lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(lineaSoporteCoberturaFacade.findByProperty("id.idTmLineaSoporteReaseguro", lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro()));
				for(LineaSoporteCoberturaDTO lineaCobertura : lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs()){
					lineaCobertura.setAplicaControlReclamo(false);
				}
//				List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOList = contratoFacultativoDTO.getDetalleContratoFacultativoDTOs();// detalleContratoFacultativoFacade.findByProperty("contratoFacultativoDTO_1.idTmContratoFacultativo", contratoFacultativoDTO.getIdTmContratoFacultativo());
//				if (detalleContratoFacultativoDTOList != null){
//					if(lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() == null || lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty())
//						lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(lineaSoporteCoberturaFacade.findByProperty("id.idTmLineaSoporteReaseguro", lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro()));
//					for(LineaSoporteCoberturaDTO lineaCobertura : lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs()){
//						lineaCobertura.setAplicaControlReclamo(false);
//					}
//					for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : detalleContratoFacultativoDTOList) {
//						LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO =  lineaSoporteCoberturaFacade.getLineaSoporteCoberturaDTOByDetalleContratoFacultativoDTO(detalleContratoFacultativoDTO);
//						lineaSoporteCoberturaDTO.setAplicaControlReclamo(false);
//						entityManager.merge(lineaSoporteCoberturaDTO);
//					}
//				}
			}
					
			//07/01/2011. Se mantiene el estatus del slip en proceso para dar opci�n de agregar prima adicional.
//			if (lineaSoporteReaseguroDTO.getPorcentajeFacultativo().doubleValue() == 0){
//				slipDTO.setEstatusCotizacion(new BigDecimal("2"));
//				entityManager.merge(slipDTO);
//				lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDTO.AUTORIZADA_PARA_EMISION);
//			}
			
			entityManager.merge(lineaSoporteReaseguroDTO);	
		} catch (RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("modificarPorcentajesContratos failed", Level.SEVERE, re);
			throw re;
		}
	}

	public int obtenerCantidadSlipsPorCotizacion(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("contando registros SlipDTO para la cotizacion: " + idToCotizacion, Level.INFO, null);
		try {
			final String queryString = "select count (model.idToSlip) from SlipDTO model where model.idToCotizacion = :idToCotizacion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			Object result = query.getSingleResult();
			int cantidadContratos=0;// = (BigDecimal) query.getSingleResult();
			if(result == null)
				cantidadContratos = 0;
			else if(result instanceof Long)
				cantidadContratos = ((Long)result).intValue();
			else if(result instanceof Integer)
				cantidadContratos = ((Integer)result).intValue();
			else if(result instanceof BigDecimal)
				cantidadContratos = ((BigDecimal)result).intValue();
			return cantidadContratos;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� el conteo de registros SlipDTO para la cotizacion: "+idToCotizacion, Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SlipDTO> listarFiltrado(SlipDTO entity) {
		try {
			LogDeMidasEJB3.log("listarFiltrado SlipDTO ", Level.INFO, null);
			String queryString = "select model from SlipDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToSlip", entity.getIdToSlip());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToCotizacion", entity.getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idTcSubRamo", entity.getIdTcSubRamo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idTmLineaSoporteReaseguro", entity.getIdTmLineaSoporteReaseguro());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToSoporteReaseguro", entity.getIdToSoporteReaseguro());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroInciso", entity.getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToSeccion", entity.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroSubInciso", entity.getNumeroSubInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "tipoDistribucion", entity.getTipoDistribucion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "estatus", entity.getEstatus());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "estatusCotizacion", entity.getEstatusCotizacion());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("FALLO: listarFiltrado LineaSoporteReaseguro", Level.SEVERE, re);
			throw re;
		}
	}
	
	public SlipDTO llenarSlip(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) {
		SlipDTO slipDTO = new SlipDTO();
		BigDecimal numeroInciso = null;
		BigDecimal numeroSubInciso = null;
		BigDecimal idToSeccion = null;
		LineaDTO lineaDTO = lineaSoporteReaseguroDTO.getLineaDTO();
		int tipoDistribucion = lineaDTO.getTipoDistribucion().intValue();
		slipDTO.setIdToCotizacion(lineaSoporteReaseguroDTO.getIdToCotizacion());	
	    slipDTO.setIdTcSubRamo(lineaDTO.getSubRamo().getIdTcSubRamo());  
	    slipDTO.setTipoDistribucion(lineaDTO.getTipoDistribucion().toString());
	    if(tipoDistribucion == 2){
	    	numeroInciso = BigDecimal.valueOf(lineaSoporteReaseguroFacadeRemote.obtenerNumeroInciso(lineaSoporteReaseguroDTO));
	    	slipDTO.setNumeroInciso(numeroInciso);
	    }else if (tipoDistribucion == 3){
	    	numeroInciso = BigDecimal.valueOf(lineaSoporteReaseguroFacadeRemote.obtenerNumeroInciso(lineaSoporteReaseguroDTO));
	    	numeroSubInciso = BigDecimal.valueOf(lineaSoporteReaseguroFacadeRemote.obtenerNumeroSubInciso(lineaSoporteReaseguroDTO));
	    	idToSeccion =  lineaSoporteReaseguroFacadeRemote.obtenerIdToSeccion(lineaSoporteReaseguroDTO);
	    	slipDTO.setNumeroInciso(numeroInciso);
	    	slipDTO.setNumeroSubInciso(numeroSubInciso);
	    	slipDTO.setIdToSeccion(idToSeccion);
	    }
	    slipDTO.setInformacionAdicional("SLIP" + (lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo().getDescripcionSubRamo()!= null? 
	    		" para Subramo: "+lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo().getDescripcionSubRamo() : ""));
	    if(lineaSoporteReaseguroDTO.getEsPrimerRiesgo()){
	    	slipDTO.setTipoSlip(""+SlipPrimerRiesgoSoporteDTO.TIPO);
	    }else{
	    	slipDTO.setTipoSlip(""+SlipGeneralSoporteDTO.TIPO);
	    }
	    slipDTO.setEstatus(BigDecimal.ONE);
	    slipDTO.setEstatusCotizacion(BigDecimal.ZERO);
	    slipDTO.setIdTmLineaSoporteReaseguro(lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro());
	    slipDTO.setIdToSoporteReaseguro(lineaSoporteReaseguroDTO.getId().getIdToSoporteReaseguro());
	    return slipDTO;
	}
	
	public SlipDTO establecerTipoSlip(SlipDTO slipDTO) {
		SubRamoDTO subRamoDTO = new SubRamoDTO();
		subRamoDTO.setIdTcSubRamo(slipDTO.getIdTcSubRamo());
		subRamoDTO = subRamoFacadeRemote.findById(subRamoDTO.getIdTcSubRamo());
		
		int tipo = Integer.valueOf(slipDTO.getTipoSlip());

		
		if(tipo != SlipPrimerRiesgoSoporteDTO.TIPO){
		 	if (subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.INCENDIO){
				slipDTO.setTipoSlip(Integer.valueOf(SlipIncendioSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.TRANSPORTES_CARGA){
				slipDTO.setTipoSlip(Integer.valueOf(SlipTransportesSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.CASCO_AVION){
				slipDTO.setTipoSlip(Integer.valueOf(SlipAviacionSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.CASCO_BARCO){
				slipDTO.setTipoSlip(Integer.valueOf(SlipBarcosSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.EQUIPO_CONTRATISTA_MAQUINARIA_PESADA){
				slipDTO.setTipoSlip(Integer.valueOf(SlipEquipoContratistaSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.OBRA_CIVIL_CONSTRUCCION){
				slipDTO.setTipoSlip(Integer.valueOf(SlipObraCivilSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.RESPONSABILIDAD_CIVIL_AVIONES){
				slipDTO.setTipoSlip(Integer.valueOf(SlipAviacionSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.RESPONSABILIDAD_CIVIL_BARCOS){
				slipDTO.setTipoSlip(Integer.valueOf(SlipBarcosSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.RESPONSABILIDAD_CIVIL_CONSEJEROS_FUNCIONARIOS){
				slipDTO.setTipoSlip(Integer.valueOf(SlipRCFuncionarioSoporteDTO.TIPO).toString());
			} else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.RESPONSABILIDAD_CIVIL_GENERAL){
				slipDTO.setTipoSlip(Integer.valueOf(SlipRCConstructoresSoporteDTO.TIPO).toString());
			}else{
				slipDTO.setTipoSlip(Integer.valueOf(SlipGeneralSoporteDTO.TIPO).toString());
			}
			
		}
		
		return slipDTO;
	}
	
	private LineaSoporteReaseguroFacadeRemote lineaSoporteReaseguroFacadeRemote;
	private SubRamoFacadeRemote subRamoFacadeRemote;
	
	@EJB
	public void setLineaSoporteReaseguroFacadeRemote(
			LineaSoporteReaseguroFacadeRemote lineaSoporteReaseguroFacadeRemote) {
		this.lineaSoporteReaseguroFacadeRemote = lineaSoporteReaseguroFacadeRemote;
	}
	
	@EJB
	public void setSubRamoFacadeRemote(SubRamoFacadeRemote subRamoFacadeRemote) {
		this.subRamoFacadeRemote = subRamoFacadeRemote;
	}
	
}