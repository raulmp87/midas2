package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratofacultativo.slip.SlipAviacionDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipAviacionFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity SlipAviacionDTO.
 * 
 * @see .SlipAviacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SlipAviacionFacade implements SlipAviacionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SlipAviacionDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SlipAviacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipAviacionDTO entity) {
		LogDeMidasEJB3.log("saving SlipAviacionDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SlipAviacionDTO entity.
	 * 
	 * @param entity
	 *            SlipAviacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipAviacionDTO entity) {
		LogDeMidasEJB3.log("deleting SlipAviacionDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SlipAviacionDTO.class, entity
					.getIdToSlip());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SlipAviacionDTO entity and return it or a copy
	 * of it to the sender. A copy of the SlipAviacionDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipAviacionDTO entity to update
	 * @return SlipAviacionDTO the persisted SlipAviacionDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipAviacionDTO update(SlipAviacionDTO entity) {
		LogDeMidasEJB3.log("updating SlipAviacionDTO instance", Level.INFO, null);
		try {
			SlipAviacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SlipAviacionDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SlipAviacionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SlipAviacionDTO instance = entityManager.find(
					SlipAviacionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipAviacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipAviacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipAviacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SlipAviacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SlipAviacionDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SlipAviacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipAviacionDTO entities.
	 * 
	 * @return List<SlipAviacionDTO> all SlipAviacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SlipAviacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all SlipAviacionDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from SlipAviacionDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}