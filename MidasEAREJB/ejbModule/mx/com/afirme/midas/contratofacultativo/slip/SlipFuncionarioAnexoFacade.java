package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioAnexoDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioAnexoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity SlipFuncionarioAnexoDTO.
 * 
 * @see .SlipFuncionarioAnexoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SlipFuncionarioAnexoFacade implements SlipFuncionarioAnexoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SlipFuncionarioAnexoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SlipFuncionarioAnexoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipFuncionarioAnexoDTO entity) {
		LogDeMidasEJB3
				.log("saving SlipFuncionarioAnexoDTO instance", Level.INFO,
						null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SlipFuncionarioAnexoDTO entity.
	 * 
	 * @param entity
	 *            SlipFuncionarioAnexoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipFuncionarioAnexoDTO entity) {
		LogDeMidasEJB3.log("deleting SlipFuncionarioAnexoDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(SlipFuncionarioAnexoDTO.class,
					entity.getIdSlipDocumentoAnexo());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SlipFuncionarioAnexoDTO entity and return it
	 * or a copy of it to the sender. A copy of the SlipFuncionarioAnexoDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipFuncionarioAnexoDTO entity to update
	 * @return SlipFuncionarioAnexoDTO the persisted SlipFuncionarioAnexoDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipFuncionarioAnexoDTO update(SlipFuncionarioAnexoDTO entity) {
		LogDeMidasEJB3.log("updating SlipFuncionarioAnexoDTO instance", Level.INFO,
				null);
		try {
			SlipFuncionarioAnexoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SlipFuncionarioAnexoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SlipFuncionarioAnexoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SlipFuncionarioAnexoDTO instance = entityManager.find(
					SlipFuncionarioAnexoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipFuncionarioAnexoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipFuncionarioAnexoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipFuncionarioAnexoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SlipFuncionarioAnexoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SlipFuncionarioAnexoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SlipFuncionarioAnexoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipFuncionarioAnexoDTO entities.
	 * 
	 * @return List<SlipFuncionarioAnexoDTO> all SlipFuncionarioAnexoDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<SlipFuncionarioAnexoDTO> findAll() {
		LogDeMidasEJB3.log("finding all SlipFuncionarioAnexoDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from SlipFuncionarioAnexoDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SlipFuncionarioAnexoDTO> obtenerAnexosPorSlipInciso(BigDecimal idToSlip,BigDecimal numeroInciso) {
		LogDeMidasEJB3.log("buscando instancias SlipFuncionarioAnexoDTO del idToSlip: " +idToSlip+ " y numeroInciso: " + numeroInciso, Level.INFO, null);
		try {
			final String queryString = "select model from SlipFuncionarioAnexoDTO model where model.slipFuncionarioDTO.id.idToSlip = :idToSlip and model.slipFuncionarioDTO.id.numeroInciso = :numeroInciso";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSlip", idToSlip);
			query.setParameter("numeroInciso", numeroInciso);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
}