package mx.com.afirme.midas.contratofacultativo.slip;
// default package



import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity SlipFuncionarioDTO.
 * 
 * @see .SlipFuncionarioDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SlipFuncionarioFacade implements SlipFuncionarioFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SlipFuncionarioDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SlipFuncionarioDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipFuncionarioDTO entity) {
		LogDeMidasEJB3.log("saving SlipFuncionarioDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SlipFuncionarioDTO entity.
	 * 
	 * @param entity
	 *            SlipFuncionarioDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipFuncionarioDTO entity) {
		LogDeMidasEJB3.log("deleting SlipFuncionarioDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SlipFuncionarioDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SlipFuncionarioDTO entity and return it or a
	 * copy of it to the sender. A copy of the SlipFuncionarioDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipFuncionarioDTO entity to update
	 * @return SlipFuncionarioDTO the persisted SlipFuncionarioDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipFuncionarioDTO update(SlipFuncionarioDTO entity) {
		LogDeMidasEJB3.log("updating SlipFuncionarioDTO instance", Level.INFO, null);
		try {
			SlipFuncionarioDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	/*public SlipFuncionarioDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SlipFuncionarioDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SlipFuncionarioDTO instance = entityManager.find(
					SlipFuncionarioDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}*/
	
	public SlipFuncionarioDTO findById(SlipFuncionarioDTOId id) {
		LogUtil.log("finding SlipFuncionarioDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SlipFuncionarioDTO instance = entityManager.find(
					SlipFuncionarioDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}	

	/**
	 * Find all SlipFuncionarioDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipFuncionarioDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipFuncionarioDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SlipFuncionarioDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SlipFuncionarioDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SlipFuncionarioDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipFuncionarioDTO entities.
	 * 
	 * @return List<SlipFuncionarioDTO> all SlipFuncionarioDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SlipFuncionarioDTO> findAll() {
		LogDeMidasEJB3.log("finding all SlipFuncionarioDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from SlipFuncionarioDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}