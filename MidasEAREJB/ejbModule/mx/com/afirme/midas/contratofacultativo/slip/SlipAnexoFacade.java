package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity SlipDocumentoAnexoDTO.
 * @see .SlipDocumentoAnexoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class SlipAnexoFacade  implements SlipAnexoFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved SlipDocumentoAnexoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SlipDocumentoAnexoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public SlipAnexoDTO save(SlipAnexoDTO entity) {
    				LogDeMidasEJB3.log("saving SlipDocumentoAnexoDTO instance", Level.INFO, null);
	        try {
	        	entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
            	return entity;
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent SlipDocumentoAnexoDTO entity.
	  @param entity SlipDocumentoAnexoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SlipAnexoDTO entity) {
    				LogDeMidasEJB3.log("deleting SlipDocumentoAnexoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(SlipAnexoDTO.class, entity.getIdToSlipDocumentoAnexo());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved SlipDocumentoAnexoDTO entity and return it or a copy of it to the sender. 
	 A copy of the SlipDocumentoAnexoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SlipDocumentoAnexoDTO entity to update
	 @return SlipDocumentoAnexoDTO the persisted SlipDocumentoAnexoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public SlipAnexoDTO update(SlipAnexoDTO entity) {
    				LogDeMidasEJB3.log("updating SlipDocumentoAnexoDTO instance", Level.INFO, null);
	        try {
            SlipAnexoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public SlipAnexoDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding SlipDocumentoAnexoDTO instance with id: " + id, Level.INFO, null);
	        try {
            SlipAnexoDTO instance = entityManager.find(SlipAnexoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all SlipDocumentoAnexoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SlipDocumentoAnexoDTO property to query
	  @param value the property value to match
	  	  @return List<SlipDocumentoAnexoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<SlipAnexoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding SlipAnexoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from SlipAnexoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}	
    
    @SuppressWarnings("unchecked")
	public List<SlipAnexoDTO> encontrarSlipAnaxoDTO(BigDecimal idToSlip) {
				LogDeMidasEJB3.log("buscando anexos del slip", Level.INFO, null);
		try {
		final String queryString = "select model from SlipAnexoDTO model where model.slipDTO.idToSlip  = :idToSlip";
							Query query = entityManager.createQuery(queryString);
				query.setParameter("idToSlip", idToSlip);
				return query.getResultList();
	} catch (RuntimeException re) {
					LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
	}
}	
    
    
    
    
	/**
	 * Find all SlipDocumentoAnexoDTO entities.
	  	  @return List<SlipDocumentoAnexoDTO> all SlipDocumentoAnexoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SlipAnexoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all SlipDocumentoAnexoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from SlipAnexoDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}


	
}