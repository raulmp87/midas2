package mx.com.afirme.midas.contratofacultativo.slip;
// default package



import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratofacultativo.slip.SlipConstructoresDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipConstructoresFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity SlipConstructoresDTO.
 * 
 * @see .SlipConstructoresDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SlipConstructoresFacade implements
		SlipConstructoresFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SlipConstructoresDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SlipConstructoresDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipConstructoresDTO entity) {
		LogDeMidasEJB3.log("saving SlipConstructoresDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SlipConstructoresDTO entity.
	 * 
	 * @param entity
	 *            SlipConstructoresDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipConstructoresDTO entity) {
		LogDeMidasEJB3.log("deleting SlipConstructoresDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SlipConstructoresDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SlipConstructoresDTO entity and return it or a
	 * copy of it to the sender. A copy of the SlipConstructoresDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipConstructoresDTO entity to update
	 * @return SlipConstructoresDTO the persisted SlipConstructoresDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipConstructoresDTO update(SlipConstructoresDTO entity) {
		LogDeMidasEJB3.log("updating SlipConstructoresDTO instance", Level.INFO, null);
		try {
			SlipConstructoresDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	/*public SlipConstructoresDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SlipConstructoresDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SlipConstructoresDTO instance = entityManager.find(
					SlipConstructoresDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}*/
	
	public SlipConstructoresDTO findById(SlipConstructoresDTOId id) {
		LogUtil.log("finding SlipConstructoresDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SlipConstructoresDTO instance = entityManager.find(
					SlipConstructoresDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}	

	/**
	 * Find all SlipConstructoresDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipConstructoresDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipConstructoresDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SlipConstructoresDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SlipConstructoresDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SlipConstructoresDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipConstructoresDTO entities.
	 * 
	 * @return List<SlipConstructoresDTO> all SlipConstructoresDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SlipConstructoresDTO> findAll() {
		LogDeMidasEJB3.log("finding all SlipConstructoresDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from SlipConstructoresDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	


}