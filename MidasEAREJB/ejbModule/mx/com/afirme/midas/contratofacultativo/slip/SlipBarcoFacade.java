package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratofacultativo.slip.SlipBarcoDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipBarcoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity SlipBarcoDTO.
 * 
 * @see .SlipBarcoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SlipBarcoFacade implements SlipBarcoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SlipBarcoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SlipBarcoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipBarcoDTO entity) {
		LogDeMidasEJB3.log("saving SlipBarcoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SlipBarcoDTO entity.
	 * 
	 * @param entity
	 *            SlipBarcoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipBarcoDTO entity) {
		LogDeMidasEJB3.log("deleting SlipBarcoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SlipBarcoDTO.class, entity
					.getIdToSlip());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SlipBarcoDTO entity and return it or a copy of
	 * it to the sender. A copy of the SlipBarcoDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SlipBarcoDTO entity to update
	 * @return SlipBarcoDTO the persisted SlipBarcoDTO entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipBarcoDTO update(SlipBarcoDTO entity) {
		LogDeMidasEJB3.log("updating SlipBarcoDTO instance", Level.INFO, null);
		try {
			SlipBarcoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SlipBarcoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SlipBarcoDTO instance with id: " + id, Level.INFO,
				null);
		try {
			SlipBarcoDTO instance = entityManager.find(SlipBarcoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipBarcoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipBarcoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipBarcoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SlipBarcoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SlipBarcoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SlipBarcoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipBarcoDTO entities.
	 * 
	 * @return List<SlipBarcoDTO> all SlipBarcoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SlipBarcoDTO> findAll() {
		LogDeMidasEJB3.log("finding all SlipBarcoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from SlipBarcoDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}