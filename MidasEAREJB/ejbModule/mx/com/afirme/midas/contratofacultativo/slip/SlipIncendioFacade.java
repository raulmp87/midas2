package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratofacultativo.slip.SlipIncendioDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipIncendioFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity SlipIncendioDTO.
 * 
 * @see .SlipIncendioDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SlipIncendioFacade implements SlipIncendioFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SlipIncendioDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SlipIncendioDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipIncendioDTO entity) {
		LogDeMidasEJB3.log("saving SlipIncendioDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SlipIncendioDTO entity.
	 * 
	 * @param entity
	 *            SlipIncendioDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipIncendioDTO entity) {
		LogDeMidasEJB3.log("deleting SlipIncendioDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SlipIncendioDTO.class, entity
					.getIdToSlip());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SlipIncendioDTO entity and return it or a copy
	 * of it to the sender. A copy of the SlipIncendioDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipIncendioDTO entity to update
	 * @return SlipIncendioDTO the persisted SlipIncendioDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipIncendioDTO update(SlipIncendioDTO entity) {
		LogDeMidasEJB3.log("updating SlipIncendioDTO instance", Level.INFO, null);
		try {
			SlipIncendioDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SlipIncendioDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SlipIncendioDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SlipIncendioDTO instance = entityManager.find(
					SlipIncendioDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipIncendioDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipIncendioDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipIncendioDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SlipIncendioDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SlipIncendioDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SlipIncendioDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipIncendioDTO entities.
	 * 
	 * @return List<SlipIncendioDTO> all SlipIncendioDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SlipIncendioDTO> findAll() {
		LogDeMidasEJB3.log("finding all SlipIncendioDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from SlipIncendioDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}