package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity SlipAnexoConstructoresDTO.
 * 
 * @see .SlipAnexoConstructoresDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SlipAnexoConstructoresFacade implements
		SlipAnexoConstructoresFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SlipAnexoConstructoresDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SlipAnexoConstructoresDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipAnexoConstructoresDTO entity) {
		LogDeMidasEJB3.log("saving SlipAnexoConstructoresDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SlipAnexoConstructoresDTO entity.
	 * 
	 * @param entity
	 *            SlipAnexoConstructoresDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipAnexoConstructoresDTO entity) {
		LogDeMidasEJB3.log("deleting SlipAnexoConstructoresDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(
					SlipAnexoConstructoresDTO.class, entity
							.getIdToSlipDocumentoAnexo());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SlipAnexoConstructoresDTO entity and return it
	 * or a copy of it to the sender. A copy of the SlipAnexoConstructoresDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipAnexoConstructoresDTO entity to update
	 * @return SlipAnexoConstructoresDTO the persisted SlipAnexoConstructoresDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipAnexoConstructoresDTO update(SlipAnexoConstructoresDTO entity) {
		LogDeMidasEJB3.log("updating SlipAnexoConstructoresDTO instance", Level.INFO,
				null);
		try {
			SlipAnexoConstructoresDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SlipAnexoConstructoresDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding SlipAnexoConstructoresDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SlipAnexoConstructoresDTO instance = entityManager.find(
					SlipAnexoConstructoresDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipAnexoConstructoresDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SlipAnexoConstructoresDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipAnexoConstructoresDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SlipAnexoConstructoresDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding SlipAnexoConstructoresDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SlipAnexoConstructoresDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SlipAnexoConstructoresDTO entities.
	 * 
	 * @return List<SlipAnexoConstructoresDTO> all SlipAnexoConstructoresDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<SlipAnexoConstructoresDTO> findAll() {
		LogDeMidasEJB3.log("finding all SlipAnexoConstructoresDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from SlipAnexoConstructoresDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	/**
	 * @param constructor
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<SlipAnexoConstructoresDTO> buscarAnexos(BigDecimal idToSlip, BigDecimal numeroInciso){
		LogDeMidasEJB3.log("buscarAnexos(SlipConstructoresDTO constructor", Level.INFO,null);
		
		try {
			final String queryString = "select model from SlipAnexoConstructoresDTO model "
				+ " where model.slipConstructoresDTO.id.idToSlip = :idToSlip"
				+ " and model.slipConstructoresDTO.id.numeroInciso = :numeroInciso";

			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSlip", idToSlip);
			query.setParameter("numeroInciso", numeroInciso);
			
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}		
	}	

}