package mx.com.afirme.midas.contratofacultativo;

// default package


import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.configuracionpagos.ConfiguracionPagosFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.pagocobertura.PlanPagosCoberturaFacadeRemote;
import mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo.ParticipacionCorredorFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoFacadeRemote;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.contratos.linea.CalculoSuscripcionesDTO;
import mx.com.afirme.midas.contratos.linea.SuscripcionDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroServiciosRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroFacadeRemote;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroId;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipAviacionSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipBarcosSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipEquipoContratistaSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipGeneralSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipIncendioSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipObraCivilSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipRCConstructoresSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipRCFuncionarioSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipTransportesSoporteDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

import org.apache.commons.beanutils.BeanUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;


/**
 * Facade for entity ContratoFacultativoDTO.
 * 
 * @see .ContratoFacultativoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ContratoFacultativoFacade implements
		ContratoFacultativoFacadeRemote {
	// property constants
	public static final String NOTA_COBERTURA = "notaCobertura";
	public static final String SUMA_ASEGURADA_TOTAL = "sumaAseguradaTotal";
	public static final String SUMA_ASEGURADA_FACULTADA = "sumaAseguradaFacultada";

	// Constantes de tipo de endoso, tomadas de /MidasWeb/../sistema/Sistema.java;
	public static final int TIPO_ENDOSO_CAMBIO_DATOS = 1;
	public static final int TIPO_ENDOSO_AUMENTO = 2;
	public static final int TIPO_ENDOSO_DISMINUCION = 3;
	public static final int TIPO_ENDOSO_CANCELACION = 4;
	public static final int TIPO_ENDOSO_REHABILITACION = 5;
	
    @PersistenceContext
    private EntityManager entityManager;
    
    @EJB
    private MovimientoReaseguroServiciosRemote movimientoReaseguroServicios;    
  
    @EJB
    private LineaSoporteReaseguroFacadeRemote lineaSoporteReaseguroFacade;
    
    @EJB PlanPagosCoberturaFacadeRemote planPagosCobertura;
    
    @Resource
    private SessionContext context;
	@SuppressWarnings("unused")
	private int band;

	/**
	 * Perform an initial save of a previously unsaved ContratoFacultativoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            ContratoFacultativoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public ContratoFacultativoDTO save(ContratoFacultativoDTO entity) {
		LogDeMidasEJB3.log("saving ContratoFacultativoDTO instance", Level.INFO, null);
		try {  
			List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOs = entity.getDetalleContratoFacultativoDTOs();
			SlipDTO slipDTO  = entityManager.find(SlipDTO.class, entity.getSlipDTO().getIdToSlip());
		    List<LineaSoporteCoberturaDTO> lineaSoporteCoberturaList = this.obtenerLineaSoporteCoberturas("id.idTmLineaSoporteReaseguro", slipDTO.getIdTmLineaSoporteReaseguro());
		    /**
		     * Calcular la porci�n de la prima adicional que se asignar� a cada cobertura
		     */
		    BigDecimal porcionPrimaAdicional = BigDecimal.ZERO;
		    BigDecimal ajustePrimaAdicional = BigDecimal.ZERO;
		    if(entity.getMontoPrimaAdicional() != null && entity.getMontoPrimaAdicional().compareTo(BigDecimal.ZERO) > 0){
		    	int cantidadCoberturasContratadas = obtenerTotalCoberturasContratadas(lineaSoporteCoberturaList);
		    	porcionPrimaAdicional = entity.getMontoPrimaAdicional().divide(new BigDecimal((double)cantidadCoberturasContratadas),RoundingMode.HALF_UP).setScale(10,RoundingMode.HALF_UP);
		    	ajustePrimaAdicional = entity.getMontoPrimaAdicional().subtract(porcionPrimaAdicional.multiply(new BigDecimal((double)lineaSoporteCoberturaList.size()))).setScale(10,RoundingMode.HALF_UP);
		    }
		  //obtiene los campos de la llave compuesta para buscar la linea para guardar idToCotizacion,idToCobertura,MontoSumaAsegurada
		    LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = null;
		    if(lineaSoporteCoberturaList != null && !lineaSoporteCoberturaList.isEmpty()){
		    	LineaSoporteReaseguroId lineaSoporteReaseguroId = new LineaSoporteReaseguroId();
		    	lineaSoporteReaseguroId.setIdTmLineaSoporteReaseguro(lineaSoporteCoberturaList.get(0).getId().getIdTmLineaSoporteReaseguro());
		    	lineaSoporteReaseguroId.setIdToSoporteReaseguro(lineaSoporteCoberturaList.get(0).getId().getIdToSoporteReaseguro());
		    	lineaSoporteReaseguroDTO = entityManager.find(LineaSoporteReaseguroDTO.class, lineaSoporteReaseguroId);
		    }
		    boolean aplicaAjustePrima = true;
            for(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : lineaSoporteCoberturaList){
         	    
  	    	   //crea una nueva instancia del DetalleCoberturas para ingresarlas a partir de las coberturas del soporte de la linea
  	    	   DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = new DetalleContratoFacultativoDTO();
  	    	   detalleContratoFacultativoDTO.setContratoFacultativoDTO_1(entity);
  	    	   detalleContratoFacultativoDTO.setIdToCotizacion(lineaSoporteReaseguroDTO.getIdToCotizacion());
  	    	   //detalleContratoFacultativoDTO.setNumeroInciso(slipDTO.getNumeroInciso());
  	    	   //detalleContratoFacultativoDTO.setIdToSeccion(slipDTO.getIdToSeccion());
  	    	   detalleContratoFacultativoDTO.setNumeroInciso(lineaSoporteCoberturaDTO.getNumeroInciso());
  	    	   detalleContratoFacultativoDTO.setIdToSeccion(lineaSoporteCoberturaDTO.getIdToSeccion());
  	    	   detalleContratoFacultativoDTO.setNumeroSubInciso(slipDTO.getNumeroSubInciso());
    		   detalleContratoFacultativoDTO.setIdTcSubramo(slipDTO.getIdTcSubRamo());
    		   detalleContratoFacultativoDTO.setIdToCobertura(lineaSoporteCoberturaDTO.getIdToCobertura());
    		   detalleContratoFacultativoDTO.setSumaAsegurada(lineaSoporteReaseguroDTO.getMontoSumaAsegurada());
    		   detalleContratoFacultativoDTO.setPrimaTotalCobertura(lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion());
   	    	   detalleContratoFacultativoDTO.setTipoDistribucion(new BigDecimal(slipDTO.getTipoDistribucion()));
   	    	   detalleContratoFacultativoDTO.setDeducibleCobertura(lineaSoporteCoberturaDTO.getPorcentajeDeducible()); // TODO revisar, no est� definido en dise�o
   	    	   detalleContratoFacultativoDTO.setPorcentajeComision(lineaSoporteCoberturaDTO.getPorcentajeCoaseguro());
	   	       detalleContratoFacultativoDTO.setSumaFacultada(lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion());
    		   detalleContratoFacultativoDTO.setPrimaFacultadaCobertura(lineaSoporteCoberturaDTO.getMontoPrimaFacultativo());
    	   	   detalleContratoFacultativoDTO.setPrimaFacultadaCobertura(lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion());
    	   	   detalleContratoFacultativoDTO.setEstatus(DetalleContratoFacultativoDTO.ESTATUS_INICIAL_COBERTURA);
    	   	   detalleContratoFacultativoDTO.setMontoPrimaNoDevengada(lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada() != null ?
	    	   			lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada() : BigDecimal.ZERO);
    	   	   if(coberturaContratada(lineaSoporteCoberturaDTO)){
    	   		   if(aplicaAjustePrima){
    	   			   detalleContratoFacultativoDTO.setMontoPrimaAdicional(porcionPrimaAdicional.add(ajustePrimaAdicional));
    	   			   aplicaAjustePrima = false;
    	   		   }else{
    	   			   detalleContratoFacultativoDTO.setMontoPrimaAdicional(porcionPrimaAdicional);
    	   		   }
    	   	   }
    	   	   detalleContratoFacultativoDTOs.add(detalleContratoFacultativoDTO);
            }
            entityManager.persist(entity);
	 		LogDeMidasEJB3.log("save successful", Level.INFO, null);
	 		return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}
      
    private int obtenerTotalCoberturasContratadas(List<LineaSoporteCoberturaDTO> lineaSoporteCoberturaList){
    	int totalContratadas = 0;
    	for(LineaSoporteCoberturaDTO cobertura : lineaSoporteCoberturaList){
    		if(coberturaContratada(cobertura)){
    			totalContratadas = totalContratadas+1;
    		}
    	}
    	return totalContratadas;
    }
    
    private boolean coberturaContratada(LineaSoporteCoberturaDTO cobertura){
    	BigDecimal primaSuscripcion = cobertura.getMontoPrimaSuscripcion() != null ? cobertura.getMontoPrimaSuscripcion() : BigDecimal.ZERO;
		BigDecimal primaNoDevengada = cobertura.getMontoPrimaNoDevengada() != null ? cobertura.getMontoPrimaNoDevengada() : BigDecimal.ZERO;
		BigDecimal diferencia = primaSuscripcion.subtract(primaNoDevengada);
		return (diferencia.compareTo(BigDecimal.ZERO) >= 0 && primaSuscripcion.compareTo(BigDecimal.ZERO) > 0);
    }
    
    @SuppressWarnings("unchecked")
	private List<LineaSoporteCoberturaDTO> obtenerLineaSoporteCoberturas(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding LineaSoporteCoberturaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from LineaSoporteCoberturaDTO model where model."
					+ propertyName + " = :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
  	/**
	 * Delete a persistent ContratoFacultativoDTO entity.
	 * 
	 * @param entity
	 *            ContratoFacultativoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public void delete(ContratoFacultativoDTO entity) {
		LogDeMidasEJB3.log("deleting ContratoFacultativoDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(ContratoFacultativoDTO.class,
					entity.getIdTmContratoFacultativo());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ContratoFacultativoDTO entity and return it or
	 * a copy of it to the sender. A copy of the ContratoFacultativoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            ContratoFacultativoDTO entity to update
	 * @return ContratoFacultativoDTO the persisted ContratoFacultativoDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
    @SuppressWarnings("unchecked")
	@Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public ContratoFacultativoDTO update(ContratoFacultativoDTO entity) {
    	LogDeMidasEJB3.log("updating ContratoFacultativoDTO instance", Level.INFO, null);
		try {  
			String queryStringListarDetalles = "select model from DetalleContratoFacultativoDTO model where model.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo";
			Query queryListadoDetalle = entityManager.createQuery(queryStringListarDetalles);
			queryListadoDetalle.setParameter("idTmContratoFacultativo", entity.getIdTmContratoFacultativo());
			queryListadoDetalle.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOs = queryListadoDetalle.getResultList();
			/*
			 * calcular la prima adicional anterior, si es igual, no se modificar�
			 */
			queryStringListarDetalles = "select sum(model.montoPrimaAdicional) from DetalleContratoFacultativoDTO model where model.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo";
			queryListadoDetalle = entityManager.createQuery(queryStringListarDetalles);
			queryListadoDetalle.setParameter("idTmContratoFacultativo", entity.getIdTmContratoFacultativo());
			queryListadoDetalle.setHint(QueryHints.REFRESH, HintValues.TRUE);
			Object resultado = queryListadoDetalle.getSingleResult();
			BigDecimal sumatoriaPrimaAdicionalAnterior = BigDecimal.ZERO;
			if(resultado != null){
				if(resultado instanceof BigDecimal){
					sumatoriaPrimaAdicionalAnterior = ((BigDecimal)resultado);
				}else if(resultado instanceof Long){
					sumatoriaPrimaAdicionalAnterior = new BigDecimal(((Long)resultado).toString());
				}else if(resultado instanceof Double){
					sumatoriaPrimaAdicionalAnterior = new BigDecimal(((Double)resultado).toString());
				}else if(resultado instanceof Integer){
					sumatoriaPrimaAdicionalAnterior = new BigDecimal(((Integer)resultado).toString());
				}else if(resultado instanceof Float){
					sumatoriaPrimaAdicionalAnterior = new BigDecimal(((Float)resultado).toString());
				}else{
					sumatoriaPrimaAdicionalAnterior = new BigDecimal((resultado.toString()).toString());
				}
			}
			
			if(entity.getMontoPrimaAdicional() == null)
				entity.setMontoPrimaAdicional(BigDecimal.ZERO);
			
			if(sumatoriaPrimaAdicionalAnterior.compareTo(entity.getMontoPrimaAdicional()) != 0){
			    /*
			     * Calcular la porci�n de la prima adicional que se asignar� a cada cobertura
			     */
			    BigDecimal porcionPrimaAdicional = BigDecimal.ZERO;
			    BigDecimal ajustePrimaAdicional = BigDecimal.ZERO;
			    if(entity.getMontoPrimaAdicional() != null && entity.getMontoPrimaAdicional().compareTo(BigDecimal.ZERO) > 0){
			    	porcionPrimaAdicional = entity.getMontoPrimaAdicional().divide(new BigDecimal((double)detalleContratoFacultativoDTOs.size()),RoundingMode.HALF_UP).setScale(10,RoundingMode.HALF_UP);
			    	ajustePrimaAdicional = entity.getMontoPrimaAdicional().subtract(porcionPrimaAdicional.multiply(new BigDecimal((double)detalleContratoFacultativoDTOs.size()))).setScale(10,RoundingMode.HALF_UP);
			    }
			    boolean aplicaAjustePrima = true;
	            for(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : detalleContratoFacultativoDTOs){
	    	   	   if(aplicaAjustePrima){
	    	   		   detalleContratoFacultativoDTO.setMontoPrimaAdicional(porcionPrimaAdicional.add(ajustePrimaAdicional));
	    	   		   aplicaAjustePrima = false;
	    	   	   }else{
	    	   		   detalleContratoFacultativoDTO.setMontoPrimaAdicional(porcionPrimaAdicional);
	    	   	   }
	            }
			}
            entity = entityManager.merge(entity);
	 		LogDeMidasEJB3.log("update successful", Level.INFO, null);
	 		return entity;
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Error al actualizar el contratoFacultativoDTO: "+entity, Level.INFO, e);
			throw e;
		}
	}
    
    /**
     * Metodo usado para ajustar la prima adicional del contrato a partir de la prima adicional guardada en los detalles del mismo.
     */
//    @SuppressWarnings("unchecked")
	public ContratoFacultativoDTO ajustarPrimaAdicionalSegunDetalles(BigDecimal idTmContratoFacultativo) {
		try {  
			ContratoFacultativoDTO entity = findById(idTmContratoFacultativo);
			/*
			 * calcular la prima adicional anterior, si es igual, no se modificar�
			 */
			String queryStringListarDetalles = "select sum(model.montoPrimaAdicional) from DetalleContratoFacultativoDTO model where model.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo";
			Query queryListadoDetalle = entityManager.createQuery(queryStringListarDetalles);
			queryListadoDetalle.setParameter("idTmContratoFacultativo", idTmContratoFacultativo);
			queryListadoDetalle.setHint(QueryHints.REFRESH, HintValues.TRUE);
			Object resultado = queryListadoDetalle.getSingleResult();
			BigDecimal sumatoriaPrimaAdicionalAnterior = BigDecimal.ZERO;
			if(resultado instanceof BigDecimal){
				sumatoriaPrimaAdicionalAnterior = ((BigDecimal)resultado);
			}else if(resultado instanceof Long){
				sumatoriaPrimaAdicionalAnterior = new BigDecimal(((Long)resultado).toString());
			}else if(resultado instanceof Double){
				sumatoriaPrimaAdicionalAnterior = new BigDecimal(((Double)resultado).toString());
			}else if(resultado instanceof Integer){
				sumatoriaPrimaAdicionalAnterior = new BigDecimal(((Integer)resultado).toString());
			}else if(resultado instanceof Float){
				sumatoriaPrimaAdicionalAnterior = new BigDecimal(((Float)resultado).toString());
			}else{
				sumatoriaPrimaAdicionalAnterior = new BigDecimal((resultado.toString()).toString());
			}
			if(sumatoriaPrimaAdicionalAnterior.compareTo(entity.getMontoPrimaAdicional()) != 0){
				entity.setMontoPrimaAdicional(sumatoriaPrimaAdicionalAnterior);
				entity = entityManager.merge(entity);
			}
	 		LogDeMidasEJB3.log("Se actualiz� la prima adicional del contrato idTmContratoFacultativo: "+idTmContratoFacultativo+" en base a los montos de sus detalles, sumando: "+sumatoriaPrimaAdicionalAnterior, Level.INFO, null);
	 		return entity;
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Ocurri� un error al intentar actualizar la prima adicional del contrato idTmContratoFacultativo: "+idTmContratoFacultativo+" en base a los montos de sus detalles", Level.INFO, null);
			throw e;
		}
	}

	public ContratoFacultativoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding ContratoFacultativoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			ContratoFacultativoDTO instance = entityManager.find(
					ContratoFacultativoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ContratoFacultativoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ContratoFacultativoDTO property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<ContratoFacultativoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ContratoFacultativoDTO> findByProperty(String propertyName,
			final Object value, final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log("finding ContratoFacultativoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ContratoFacultativoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<ContratoFacultativoDTO> findByNotaCobertura(
			Object notaCobertura, int... rowStartIdxAndCount) {
		return findByProperty(NOTA_COBERTURA, notaCobertura,
				rowStartIdxAndCount);
	}

	public List<ContratoFacultativoDTO> findBySumaAseguradaTotal(
			Object sumaAseguradaTotal, int... rowStartIdxAndCount) {
		return findByProperty(SUMA_ASEGURADA_TOTAL, sumaAseguradaTotal,
				rowStartIdxAndCount);
	}

	public List<ContratoFacultativoDTO> findBySumaAseguradaFacultada(
			Object sumaAseguradaFacultada, int... rowStartIdxAndCount) {
		return findByProperty(SUMA_ASEGURADA_FACULTADA, sumaAseguradaFacultada,
				rowStartIdxAndCount);
	}

	/**
	 * Find all ContratoFacultativoDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ContratoFacultativoDTO> all ContratoFacultativoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ContratoFacultativoDTO> findAll(
			final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log("finding all ContratoFacultativoDTO instances", Level.INFO,
				null);
		try { 
			final String queryString = "select model from ContratoFacultativoDTO model";
			Query query = entityManager.createQuery(queryString);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ContratoFacultativoDTO> buscarFiltrado(ContratoFacultativoDTO contratoFacultativoDTO) {
		try {		
			String queryString = "select model from ContratoFacultativoDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (contratoFacultativoDTO == null)
				return null;
			
			//Clausulas sWhere Aqui 

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return query.getResultList();
			
        } catch (RuntimeException re) {
				LogDeMidasEJB3.log("find in ContratoFacultativoDTO failed", Level.SEVERE, re);
		    throw re;
		}			
	}
 		
	@SuppressWarnings("unchecked")
	public String autorizarCotizacionFacultativa(BigDecimal idToSlip){
		StringBuilder mensajeVeredicto = new StringBuilder();
		LineaSoporteReaseguroId lineaSoporteReaseguroId = new LineaSoporteReaseguroId();
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO;
				
		SlipDTO slipDTO = entityManager.find(SlipDTO.class, idToSlip);
		
		lineaSoporteReaseguroId.setIdTmLineaSoporteReaseguro(slipDTO.getIdTmLineaSoporteReaseguro());
		lineaSoporteReaseguroId.setIdToSoporteReaseguro(slipDTO.getIdToSoporteReaseguro());
		lineaSoporteReaseguroDTO = entityManager.find(LineaSoporteReaseguroDTO.class, lineaSoporteReaseguroId);
		 
		if (slipDTO.getTipoEndoso() != null && slipDTO.getTipoEndoso().intValue() != TIPO_ENDOSO_CANCELACION){

			mensajeVeredicto.append(validarCobertura(idToSlip,lineaSoporteReaseguroDTO));
			mensajeVeredicto.append(validarParticipacion(idToSlip,lineaSoporteReaseguroDTO));
			mensajeVeredicto.append(validarDetalleParticipacion(idToSlip,lineaSoporteReaseguroDTO));

			// A continuaci�n se actualiza el Estatus de ContratoFacultativoDTO
			if (mensajeVeredicto.equals("")){
				final String queryString = "SELECT model FROM ContratoFacultativoDTO model, SlipDTO s WHERE model.slipDTO.idToSlip = s.idToSlip AND s.idToSlip = :idToSlip";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idToSlip", idToSlip);
				List<ContratoFacultativoDTO> contratoFacultativoDTOList = query.getResultList();
				for (ContratoFacultativoDTO contratoFacultativoDTO : contratoFacultativoDTOList) {
					contratoFacultativoDTO.setEstatus(new BigDecimal(1));
					this.update(contratoFacultativoDTO);
				}

				final String queryString2 = "UPDATE SlipDTO model SET model.estatusCotizacion = 1, model.estatus = 1  WHERE model.idToSlip = :idToSlip";
				query = entityManager.createQuery(queryString2);
				query.setParameter("idToSlip", idToSlip);
				query.executeUpdate();

				slipDTO = entityManager.find(SlipDTO.class, idToSlip);			
				
				lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDTO.ESTATUS_COTIZACION_FAC_AUTORIZADA);
				entityManager.merge(lineaSoporteReaseguroDTO);			
			}
		}else if (slipDTO.getTipoEndoso() != null && slipDTO.getTipoEndoso().intValue() == TIPO_ENDOSO_CANCELACION){
			String queryString = "UPDATE DetalleContratoFacultativoDTO model SET model.montoPrimaNoDevengada = 0 " +
					"WHERE model.montoPrimaNoDevengada IS NULL"; //Al momento de cancelar el contrato por endoso de cancelaci�n las Primas No Devengadas que est�n NULL se tomar�n como ceros
			Query query = entityManager.createQuery(queryString);
			query.executeUpdate();
			slipDTO.setEstatusCotizacion(new BigDecimal("4")); //Al tratarse de un Endoso de Cancelaci�n la autorizaci�n del contrato consiste en su cancelaci�n
			
			this.procesarMovimientosCancelacionRehabilitacionCobertura(lineaSoporteReaseguroDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo(), 2, 0);
			entityManager.merge(slipDTO);
		} else if(slipDTO.getTipoEndoso() == null){//Este es el flujo por donde siempre entra. Lo demas nunca se utiliz�.
			mensajeVeredicto.append(validarCobertura(idToSlip,lineaSoporteReaseguroDTO));
			mensajeVeredicto.append(validarParticipacion(idToSlip,lineaSoporteReaseguroDTO));
			mensajeVeredicto.append(validarDetalleParticipacion(idToSlip,lineaSoporteReaseguroDTO));
			
			//Si la l�nea no tiene porcentaje facultativo (100% retenci�n), se permite pasar sin plan de pago. 
			if(lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo() != null && 
					lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo().compareTo(BigDecimal.ZERO) >0){			
				mensajeVeredicto.append(validarPlanesPago(idToSlip));				
				if (mensajeVeredicto.toString().equals("")){
					//Distribuir los pagos por reasegurador
					//Solo se distribuyen los pagos de las coberturas con prima <> 0
					final String queryString = "SELECT detalle " +
												"FROM DetalleContratoFacultativoDTO detalle " +
												"WHERE detalle.contratoFacultativoDTO_1.slipDTO.idToSlip = :idToSlip " +
												"AND detalle.primaFacultadaCobertura - detalle.montoPrimaNoDevengada <> 0";
					Query query = entityManager.createQuery(queryString);
					query.setParameter("idToSlip", idToSlip);
					List<DetalleContratoFacultativoDTO> detallesContratosFacultativos = query.getResultList();
					for(DetalleContratoFacultativoDTO detalleContratoFacultativo : detallesContratosFacultativos){
						try {
							planPagosCobertura.distribuirPagosPorReasegurador(detalleContratoFacultativo.getContratoFacultativoDTO());
							planPagosCobertura.autorizarPlanPagosPorContratoFacultativo(detalleContratoFacultativo.getContratoFacultativoDTO());
						} catch (ExcepcionDeLogicaNegocio e) {
							mensajeVeredicto.append("<br/><br/><font style='color:red;'>Error al distribuir los pagos por reasegurador:</font><ul>");										
							mensajeVeredicto.append("<li>").append(e.getDescripcion()).append("</li>");				
							mensajeVeredicto.append("</ul>");		
						}
					}
				}
			}
			
			/* CODIGO PARA EVITAR QUE SE AUTORICE LA COTIZACI�N FACULTATIVA, PARA PRUEBAS DE LA DISTRIBUCI�N DE PAGOS. 
			    if (mensajeVeredicto.equals("")){
				mensajeVeredicto = "NO HAY ERROR";
			}
			*/ 
			
			// A continuaci�n se actualiza el Estatus de ContratoFacultativoDTO
			if (mensajeVeredicto.toString().equals("")){
				final String queryString = "SELECT model FROM ContratoFacultativoDTO model, SlipDTO s WHERE model.slipDTO.idToSlip = s.idToSlip AND s.idToSlip = :idToSlip";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idToSlip", idToSlip);
				List<ContratoFacultativoDTO> contratoFacultativoDTOList = query.getResultList();
				for (ContratoFacultativoDTO contratoFacultativoDTO : contratoFacultativoDTOList) {
						contratoFacultativoDTO.setEstatus(new BigDecimal(1));
						this.update(contratoFacultativoDTO);
				}
				
				final String queryString2 = "UPDATE SlipDTO model SET model.estatusCotizacion = 1, model.estatus = 1  WHERE model.idToSlip = :idToSlip";
				query = entityManager.createQuery(queryString2);
				query.setParameter("idToSlip", idToSlip);
				query.executeUpdate();
				
				slipDTO = entityManager.find(SlipDTO.class, idToSlip);
				
				boolean coberturasIguales = true;
				for(LineaSoporteCoberturaDTO lineaCobertura : lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs()){
					BigDecimal montoPrimaAdicional = lineaCobertura.getMontoPrimaAdicional() != null ? lineaCobertura.getMontoPrimaAdicional() : BigDecimal.ZERO;
					lineaCobertura.setMontoPrimaFacultativo(montoPrimaAdicional.add(lineaCobertura.getMontoPrimaFacultativo()));
					BigDecimal diferenciaSuscripcionFacultativo = 
						lineaCobertura.getMontoPrimaSuscripcion().subtract(
								lineaCobertura.getMontoPrimaFacultativo(),new MathContext(10, RoundingMode.HALF_UP)).abs();
					if(diferenciaSuscripcionFacultativo.compareTo(BigDecimal.ONE) >= 0 && 
							lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo().compareTo(BigDecimal.ZERO) != 0){
						coberturasIguales = false;
						//se remueve el break para que contin�e ajustando las primas de cada cobertura con la prima adicional
					}
				}
				lineaSoporteReaseguroDTO.setEstatusFacultativo(coberturasIguales ? LineaSoporteReaseguroDTO.ESTATUS_FACULTATIVO_INTEGRADO : LineaSoporteReaseguroDTO.ESTATUS_COTIZACION_FAC_AUTORIZADA);
				entityManager.merge(lineaSoporteReaseguroDTO);				
			}
		}
		
		return mensajeVeredicto.toString();
	}
	
	@SuppressWarnings("unchecked")
	public String validarCobertura(BigDecimal idToSlip,LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO){
		StringBuilder mensajeVeredicto = new StringBuilder("");
		//Si la l�nea no tiene porcentaje facultativo, se permite pasar sin participantes.
		if(lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo() != null && 
				lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo().compareTo(BigDecimal.ZERO) >0){
			final String queryString = "SELECT detalle FROM DetalleContratoFacultativoDTO detalle, ContratoFacultativoDTO contrato " +
			"WHERE detalle.contratoFacultativoDTO_1.idTmContratoFacultativo = contrato.idTmContratoFacultativo AND contrato.slipDTO.idToSlip = :idToSlip AND (SELECT COUNT(participacion.participacionFacultativoDTO)" +
			" from ParticipacionFacultativoDTO participacion WHERE participacion.detalleContratoFacultativoDTO.contratoFacultativoDTO = detalle.contratoFacultativoDTO) = 0 " +
			"ORDER BY detalle.contratoFacultativoDTO_1.idTmContratoFacultativo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSlip", idToSlip);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOList = query.getResultList();
			if (detalleContratoFacultativoDTOList.size() > 0){
				mensajeVeredicto.append("<br/><br/><font style='color:blue;'>Las siguientes coberturas no tienen participaciones:</font><ul>");
				for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : detalleContratoFacultativoDTOList) {				
					mensajeVeredicto.append("<li>Cobertura ").append(detalleContratoFacultativoDTO.getContratoFacultativoDTO() ).append(" del Contrato ").append(detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getIdTmContratoFacultativo()).append("</li>");
				}
				mensajeVeredicto.append("</ul>");
			}
		}
		
		return mensajeVeredicto.toString();			
	} 
	
	@SuppressWarnings("unchecked")
	public String validarParticipacion(BigDecimal idToSlip,LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO){
		StringBuilder mensajeVeredicto = new StringBuilder("");
		//Si la l�nea no tiene porcentaje facultativo, se permite pasar sin participantes.
		if(lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo() != null &&
				lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo().compareTo(BigDecimal.ZERO) > 0){
			String queryString = "SELECT DISTINCT p.detalleContratoFacultativoDTO.contratoFacultativoDTO FROM ParticipacionFacultativoDTO p,DetalleContratoFacultativoDTO detalle,ContratoFacultativoDTO contrato " +
	                             "where p.detalleContratoFacultativoDTO.contratoFacultativoDTO = detalle.contratoFacultativoDTO and " +
	                             "detalle.contratoFacultativoDTO_1.idTmContratoFacultativo = contrato.idTmContratoFacultativo AND contrato.slipDTO.idToSlip = :idToSlip AND " +
	                             "(SELECT SUM(participacion.porcentajeParticipacion) FROM ParticipacionFacultativoDTO participacion WHERE " +
	                             "participacion.detalleContratoFacultativoDTO = p.detalleContratoFacultativoDTO) <> 100 " +
	                             "ORDER BY p.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo, p.detalleContratoFacultativoDTO.contratoFacultativoDTO";
	 		Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSlip", idToSlip);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<BigDecimal> participacionFacultativoIdList = query.getResultList();
			if (participacionFacultativoIdList.size() > 0){
				mensajeVeredicto.append("<br/><font style='color:blue;'>Las siguientes participaciones no cubren el 100% del porcentaje de participaci&oacute;n de su respectiva cobertura:</font><ul>"); 
				for (BigDecimal participacionFacultativoId : participacionFacultativoIdList) {
					List<ParticipacionFacultativoDTO> participacionFacultativoDTOList = this.findParticipacionFacultativo(participacionFacultativoId);
					 for(ParticipacionFacultativoDTO participacionFacultativoDTO : participacionFacultativoDTOList){
					     mensajeVeredicto.append("<li> La participaci&oacute;n ").append(participacionFacultativoDTO.getParticipacionFacultativoDTO()).append(" (").append(participacionFacultativoDTO.getPorcentajeParticipacion()).append("%) de la cobertura ").append(participacionFacultativoDTO.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO()).append("</li>");
					 }
				 }
				mensajeVeredicto.append("</ul>");
			}
		}
		return mensajeVeredicto.toString(); 
	}
	
	@SuppressWarnings("unchecked")
	public String validarDetalleParticipacion(BigDecimal idToSlip,LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO){
		StringBuilder mensajeVeredicto = new StringBuilder("");
		//Si la l�nea no tiene porcentaje facultativo, se permite pasar sin participantes.
		if(lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo() != null &&
				lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo().compareTo(BigDecimal.ZERO) > 0){
			String queryString = "SELECT detalle FROM DetalleContratoFacultativoDTO detalle, ContratoFacultativoDTO contrato " +
			"WHERE detalle.contratoFacultativoDTO_1.idTmContratoFacultativo = contrato.idTmContratoFacultativo AND contrato.slipDTO.idToSlip = :idToSlip"; 
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSlip", idToSlip);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOList = query.getResultList();
			int bandera = 0;
			mensajeVeredicto.append("<br/><font style='color:blue;'>Reaseguradores por corredor:</font><ul>");
			if (detalleContratoFacultativoDTOList != null && detalleContratoFacultativoDTOList.size() > 0){
				for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : detalleContratoFacultativoDTOList) {
					queryString = "SELECT participacion FROM ParticipacionFacultativoDTO participacion WHERE " +
							"participacion.detalleContratoFacultativoDTO.contratoFacultativoDTO = :detalleContratoFacultativo"; 
					query = entityManager.createQuery(queryString);
					query.setParameter("detalleContratoFacultativo", detalleContratoFacultativoDTO.getContratoFacultativoDTO());
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					List<ParticipacionFacultativoDTO> participacionFacultativoDTOList = query.getResultList();
	
					for (ParticipacionFacultativoDTO participacionFacultativoDTO : participacionFacultativoDTOList) {
						if (participacionFacultativoDTO.getTipo().equals("0")){ // 0: Corredor, 1: Reasegurador, s�lo interesa el caso en que es Corredor.
							queryString = "SELECT SUM(participacionCorredor.porcentajeParticipacion), COUNT(participacionCorredor.porcentajeParticipacion) " +
									"FROM ParticipacionCorredorFacultativoDTO participacionCorredor WHERE " +
									"participacionCorredor.participacionFacultativoDTO.participacionFacultativoDTO = :participacionFacultativoDTO " +
									"ORDER BY participacionCorredor.participacionFacultativoDTO.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo, participacionCorredor.participacionFacultativoDTO.detalleContratoFacultativoDTO.contratoFacultativoDTO"; 
							query = entityManager.createQuery(queryString);
							query.setParameter("participacionFacultativoDTO", participacionFacultativoDTO.getParticipacionFacultativoDTO());
							query.setHint(QueryHints.REFRESH, HintValues.TRUE);
							Object[] results = (Object[]) query.getSingleResult();
							BigDecimal sumaParticipacionesReaseguradores = (BigDecimal) results[0];
							BigDecimal porcentajeParticipacionReasegurador = participacionFacultativoDTO.getPorcentajeParticipacion(); 
							Long numeroParticipacionesReaseguradores = (Long) results[1];
							if (sumaParticipacionesReaseguradores == null || !(sumaParticipacionesReaseguradores.compareTo(porcentajeParticipacionReasegurador) == 0)){
								bandera = 1;
								if (numeroParticipacionesReaseguradores.equals(new Long(0)))
									mensajeVeredicto.append("<li>El corredor ").append(participacionFacultativoDTO.getReaseguradorCorredorDTO().getNombre()).append(" de la cobertura ").append(participacionFacultativoDTO.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO() ).append(" en la participaci&oacute;n ").append(participacionFacultativoDTO.getParticipacionFacultativoDTO() ).append(" no tiene reaseguradores asignados</li><br/>");
								else
									mensajeVeredicto.append("<li>Los reaseguradores asignados al corredor ").append(participacionFacultativoDTO.getReaseguradorCorredorDTO().getNombre()).append(" de la cobertura ").append(participacionFacultativoDTO.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO()).append(" en la participaci&oacute;n ").append(participacionFacultativoDTO.getParticipacionFacultativoDTO() ).append(" no cubren el porcentaje asignado al corredor (").append(participacionFacultativoDTO.getPorcentajeParticipacion()).append("%)</li><br/>");
							}
						}
					}
				}
				mensajeVeredicto.append("</ul>");
			}
			if (bandera == 0)
				mensajeVeredicto.append("");
		}
		
		return mensajeVeredicto.toString(); 
	}
	
	@SuppressWarnings("unchecked")
	public String validarPlanesPago(BigDecimal idToSlip){
		StringBuilder mensajeVeredicto = new StringBuilder("");
		
		//Este query obtiene todos los detalles de contrato facultativo que tienen un plan de pagos que requiere autorizaci�n o que fue rechazado.
		String queryString = "SELECT detalle FROM DetalleContratoFacultativoDTO detalle, PlanPagosCoberturaDTO planPagos WHERE " +
		"detalle.contratoFacultativoDTO_1.slipDTO.idToSlip = :idToSlip AND " +
		"planPagos.detalleContratoFacultativoDTO.contratoFacultativoDTO = detalle.contratoFacultativoDTO AND " +
		"planPagos.estatusAutorizacionPlanPagos IN (1,8) " +//1: Requiere Autorizaci�n, 8: Rechazado
		"ORDER BY detalle.contratoFacultativoDTO_1.idTmContratoFacultativo, detalle.contratoFacultativoDTO"; 
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToSlip", idToSlip);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);				
		List<DetalleContratoFacultativoDTO> detallesContratoFacultativo = query.getResultList();
		if (detallesContratoFacultativo.size() > 0){
			mensajeVeredicto.append("<br/><br/><font style='color:blue;'>Las siguientes coberturas tienen un plan de pagos pendiente por autorizar:</font><ul>");
			for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : detallesContratoFacultativo) {				
				mensajeVeredicto.append("<li>Cobertura ").append(detalleContratoFacultativoDTO.getContratoFacultativoDTO() ).append(" del Contrato ").append(detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getIdTmContratoFacultativo() ).append("</li>");
			}
			mensajeVeredicto.append("</ul>");
		}
		
		//Este query obtiene las coberturas sin un plan de pagos configurado
		//S�lo se consideran las coberturas con prima <> 0
		queryString = "SELECT DET.IDTMCONTRATOFACULTATIVO, DET.IDTDCONTRATOFACULTATIVO, COUNT(PAGOS.IDTOPLANPAGOSCOBERTURA) " +
					  "FROM MIDAS.TDCONTRATOFACULTATIVO DET " +
					  "INNER JOIN MIDAS.TMCONTRATOFACULTATIVO CON " +
					  "ON DET.IDTMCONTRATOFACULTATIVO = CON.IDTMCONTRATOFACULTATIVO " +
					  "AND CON.IDTOSLIP = " + idToSlip + " " +
					  "LEFT OUTER JOIN MIDAS.TOPLANPAGOSCOBERTURA PLANPAGOS " +
					  "ON DET.IDTDCONTRATOFACULTATIVO = PLANPAGOS.IDTDCONTRATOFACULTATIVO " +
					  "LEFT OUTER JOIN MIDAS.TMPAGOCOBERTURA PAGOS " +
					  "ON PLANPAGOS.IDTOPLANPAGOSCOBERTURA = PAGOS.IDTOPLANPAGOSCOBERTURA " +
					  "WHERE DET.PRIMAFACULTADACOBERTURA - DET.MONTOPRIMANODEVENGADA <> 0 " +
					  "GROUP BY DET.IDTMCONTRATOFACULTATIVO, DET.IDTDCONTRATOFACULTATIVO " +
					  "HAVING COUNT(PAGOS.IDTOPLANPAGOSCOBERTURA)=0"; 

		
		query = entityManager.createNativeQuery(queryString);		
		
		List<Object[]> detalles = query.getResultList();
		
		if(detalles.size()>0){
			mensajeVeredicto.append("<br/><br/><font style='color:blue;'>Las siguientes coberturas no tienen un plan de pagos configurado:</font><ul>");
			for (Object[] detalle : detalles) {				
				mensajeVeredicto.append("<li>Cobertura ").append(detalle[1] ).append(" del Contrato ").append(detalle[0] ).append("</li>");
			}
			mensajeVeredicto.append("</ul>");
		}
		
		return mensajeVeredicto.toString();
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean autorizarContratoFacultativo(BigDecimal idTmContratoFacultativo){
		LogDeMidasEJB3.log("ContratoFacultativoFacade.autorizarContratoFacultativo: autorizando ContratoFacultativo: "+idTmContratoFacultativo, Level.INFO, null);
		boolean resultado = false;
		LineaSoporteReaseguroId lineaSoporteReaseguroId = new LineaSoporteReaseguroId();
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO;
		try {
			ContratoFacultativoDTO contratoFacultativoDTO = new ContratoFacultativoDTO();
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			SlipDTO slipDTO = new SlipDTO();
			BigDecimal idToCotizacion = new BigDecimal("0");
			if (idTmContratoFacultativo != null){
				contratoFacultativoDTO = this.findById(idTmContratoFacultativo);
				idToCotizacion = contratoFacultativoDTO.getSlipDTO().getIdToCotizacion();
				cotizacionDTO = entityManager.find(CotizacionDTO.class, idToCotizacion);
				short estatus = cotizacionDTO.getClaveEstatus();
				if (estatus == CotizacionDTO.ESTATUS_COT_ASIGNADA_EMISION){
					contratoFacultativoDTO.setEstatus(new BigDecimal("2"));
					slipDTO = contratoFacultativoDTO.getSlipDTO();
					slipDTO.setEstatusCotizacion(new BigDecimal("2"));
					entityManager.merge(contratoFacultativoDTO);
					entityManager.merge(slipDTO);
					resultado = true;
					
					lineaSoporteReaseguroId.setIdTmLineaSoporteReaseguro(slipDTO.getIdTmLineaSoporteReaseguro());
					lineaSoporteReaseguroId.setIdToSoporteReaseguro(slipDTO.getIdToSoporteReaseguro());
					lineaSoporteReaseguroDTO = entityManager.find(LineaSoporteReaseguroDTO.class, lineaSoporteReaseguroId);
					lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDTO.ESTATUS_AUTORIZADA_EMISION);
					entityManager.merge(lineaSoporteReaseguroDTO);
				} else if (estatus == CotizacionDTO.ESTATUS_COT_EMITIDA) {		
					//Verificar que tenga endoso de rehabilitacion o cancelacion.
					/*
					 * 23/06/2011 Se elimina la validaci�n debido a que los endosos son distribuidos al realizar la emisi�n.
					 */
//					EndosoDTO endosoDTO = endosoFacadeRemote.buscarPorCotizacion(idToCotizacion);
//					if (endosoDTO != null) {
//						short tipoEndoso = endosoDTO.getClaveTipoEndoso();
//						if (tipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION || tipoEndoso == EndosoDTO.TIPO_ENDOSO_REHABILITACION || tipoEndoso == EndosoDTO.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO) {
//							BigDecimal idToPoliza = endosoDTO.getId().getIdToPoliza();
//							int numeroEndoso = endosoDTO.getId().getNumeroEndoso();
//
//							String identificadorLog = String.format("idToCotizacion: %s idToPoliza: %s numeroEndoso: %s",idToCotizacion, idToPoliza, numeroEndoso);
//							LogDeMidasEJB3.log(identificadorLog + " es endoso de cancelacion/rehabilitacion se procede a validar el SoporteReaseguro.", Level.INFO, null);
//							
//							//Validar
//							List<String> validacionSoporteList = soporteReaseguroFacadeRemote.validarSoporteReaseguroDTOListoParaDistribucion(idToPoliza, numeroEndoso);
//							
//							SoporteReaseguroDTO soporteReaseguroDTO = soporteReaseguroFacadeRemote.obtenerSoporteReaseguroPorPolizaEndoso(idToPoliza, numeroEndoso);
//							
//							if (validacionSoporteList.size() == 0) { //Distribuir
//								LogDeMidasEJB3.log(identificadorLog + " pas� satisfactoriamente la validacion del SoporteReaseguro.", Level.INFO, null);								
//								LogDeMidasEJB3.log(identificadorLog + " se procede a la distribucion para el idToSoporteReaseguro: " +soporteReaseguroDTO.getIdToSoporteReaseguro(), Level.INFO, null);
//								temporizadorDistribuirPolizaFacadeRemote.iniciarTemporizador(TIEMPO_INICIAR_DISTRIBUCION, soporteReaseguroDTO.getIdToSoporteReaseguro());
//								
//								LogDeMidasEJB3.log(identificadorLog + " se cambia el estatus del Slip y el ContratoFacultativo." +soporteReaseguroDTO.getIdToSoporteReaseguro(), Level.INFO, null);
//								
//								slipDTO = contratoFacultativoDTO.getSlipDTO();
//								slipDTO.setEstatusCotizacion(new BigDecimal("2"));
//								contratoFacultativoDTO.setEstatus(new BigDecimal("2"));
//
//								entityManager.merge(contratoFacultativoDTO);
//								entityManager.merge(slipDTO);
//								resultado = true;
//							} else {
//								//Establecer el estatus de las lineas a estatus incosistente.
//								LogDeMidasEJB3.log(identificadorLog + " se encontraron errores en la validacion del SoporteReaseguro" +
//										" por lo tanto no se realiza distribucion de la prima. Errores de validacion: "+validacionSoporteList, Level.INFO, null);
//								try{
//									LogDeMidasEJB3.log(identificadorLog + " se procede a establecer las lineas con estatus de incosistencias.", Level.INFO, null);
//									lineaSoporteReaseguroFacade.actualizarEstatusLineaSoporte(soporteReaseguroDTO.getIdToSoporteReaseguro(),
//													LineaSoporteReaseguroDTO.EMITIDO_SIN_DISTRIBUIR,
//													LineaSoporteReaseguroDTO.ESTATUS_LINEA_EMITIDO_INCONSISTENCIAS);
//								}catch(Exception e){
//									LogDeMidasEJB3.log(identificadorLog + " no fue posible establecer el estatus de las lineas.", 
//											Level.SEVERE, null);
//								}
//							}
//						}
//					}
				}
			}
			return resultado;
		} catch (RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("updating failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean cancelarContratoFacultativo(BigDecimal idToSlip){
		LogDeMidasEJB3.log("updating ContratoFacultativo instances", Level.INFO,
				null);
		boolean result = false;
		try {
			ContratoFacultativoDTO contratoFacultativoDTO = new ContratoFacultativoDTO();
			SlipDTO slipDTO = new SlipDTO();
			slipDTO = entityManager.find(SlipDTO.class, idToSlip);
		 	if (slipDTO != null){
		 		LineaSoporteReaseguroId lineaSoporteReaseguroId = new LineaSoporteReaseguroId();
		 		lineaSoporteReaseguroId.setIdTmLineaSoporteReaseguro(slipDTO.getIdTmLineaSoporteReaseguro());
		 		lineaSoporteReaseguroId.setIdToSoporteReaseguro(slipDTO.getIdToSoporteReaseguro());
		 		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = entityManager.find(LineaSoporteReaseguroDTO.class, lineaSoporteReaseguroId);
		 		if (lineaSoporteReaseguroDTO.getEstatusLineaSoporte().intValue() != 1){
		 			//slipDTO = contratoFacultativoDTO.getSlipDTO();
		 			slipDTO.setEstatusCotizacion(new BigDecimal("4"));
		 			entityManager.merge(slipDTO);
		 			result = true;
		 			/*if (lineaSoporteReaseguroDTO.getPorcentajeFacultativo().equals(new Double("0"))){
		 				lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDTO.SOPORTADO);
		 			}else{
		 				lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDTO.REQUIERE_FACULTATIVO);
		 			}*/
		 		}else{
		 			contratoFacultativoDTO = lineaSoporteReaseguroDTO.getContratoFacultativoDTO();
		 			
		 			boolean esEndoso =false;
		 			if(contratoFacultativoDTO != null){
			 			//Bidirectional relationship
			 			lineaSoporteReaseguroDTO.setContratoFacultativoDTO(null);
			 			contratoFacultativoDTO.getLineaSoporteReaseguroDTOs().remove(lineaSoporteReaseguroDTO);
			 			
			 			esEndoso = contratoFacultativoDTO.getNumeroEndosoFacultativo() != null && contratoFacultativoDTO.getNumeroEndosoFacultativo() > 0;
			 			entityManager.merge(lineaSoporteReaseguroDTO);
			 			eliminarSlipsParticularesPorSlipDTO(slipDTO);
			 			if (contratoFacultativoDTO != null){
			 				this.deleteWithDetails(contratoFacultativoDTO);
			 			}
		 			}
		 			entityManager.remove(slipDTO);
		 			result = true;
		 			if(esEndoso){
		 				lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDTO.ENDOSO_FACULTATIVO);
		 			}else if (lineaSoporteReaseguroDTO.getPorcentajeFacultativo().equals(new Double("0"))){
		 				lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDTO.ESTATUS_SOPORTADO_POR_REASEGURO);
		 			}else{
		 				lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDTO.ESTATUS_REQUIERE_FACULTATIVO);
		 			}
		 		}
		 		entityManager.merge(lineaSoporteReaseguroDTO);
			}
			return result;
		} catch (RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("updating failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void deleteWithDetails(ContratoFacultativoDTO contratoFacultativoDTO){
		try {
			planPagosCobertura.eliminarPlanPagosPorIdContratoFacultativo(contratoFacultativoDTO.getIdTmContratoFacultativo());
		} catch (ExcepcionDeLogicaNegocio e) {			
			LogDeMidasEJB3.log("ContratoFacultativoFacade.deleteWithDetails failed", Level.SEVERE, e);
		}
		this.eliminarDetallesContratoFacultativoDTO(contratoFacultativoDTO);
		this.eliminarConfiguracionPagosFacultativo(contratoFacultativoDTO);
		this.eliminarEstadosCuentaPagosFacultativo(contratoFacultativoDTO);
		this.delete(contratoFacultativoDTO);
	}
	
	private void eliminarConfiguracionPagosFacultativo(ContratoFacultativoDTO contratoFacultativoDTO){
		String queryString = "DELETE FROM ConfiguracionPagosFacultativoDTO model WHERE model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTmContratoFacultativo", contratoFacultativoDTO.getIdTmContratoFacultativo());
		query.executeUpdate();
	}
	
	private void eliminarEstadosCuentaPagosFacultativo(ContratoFacultativoDTO contratoFacultativoDTO){
		String queryString = "DELETE FROM EstadoCuentaDTO model WHERE model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTmContratoFacultativo", contratoFacultativoDTO.getIdTmContratoFacultativo());
		query.executeUpdate();
	}
	
	private void eliminarDetallesContratoFacultativoDTO(ContratoFacultativoDTO contratoFacultativoDTO){
		eliminarParticipacionesFacultativas(contratoFacultativoDTO);
		String queryString = "DELETE FROM DetalleContratoFacultativoDTO model WHERE model.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTmContratoFacultativo", contratoFacultativoDTO.getIdTmContratoFacultativo());
		query.executeUpdate();
		
	}		
	
	private void eliminarParticipacionesFacultativas(ContratoFacultativoDTO contratoFacultativoDTO){
		eliminarParticipacionesCorredorFacultativas(contratoFacultativoDTO);
		String queryString = "DELETE FROM ParticipacionFacultativoDTO model WHERE model.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTmContratoFacultativo", contratoFacultativoDTO.getIdTmContratoFacultativo());
		query.executeUpdate();
	}
	
	private void eliminarParticipacionesCorredorFacultativas(ContratoFacultativoDTO contratoFacultativoDTO){
		String queryString = "DELETE FROM ParticipacionCorredorFacultativoDTO model WHERE model.participacionFacultativoDTO.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTmContratoFacultativo", contratoFacultativoDTO.getIdTmContratoFacultativo());
		query.executeUpdate();
	}
	
	private void eliminarSlipsParticularesPorSlipDTO(SlipDTO slipDTO){
		if (slipDTO != null && slipDTO.getTipoSlip() != null && slipDTO.getTipoSlip().length() > 0){
			int tipoSlip = new Integer(slipDTO.getTipoSlip()).intValue();
			String queryString = "DELETE FROM ";
			String tabla = "";
			if (tipoSlip == SlipAviacionSoporteDTO.TIPO){
				tabla = "SlipAviacionDTO";
			}
			
			if (tipoSlip == SlipBarcosSoporteDTO.TIPO){
				tabla = "SlipBarcoDTO";
			}
			
			if (tipoSlip == SlipEquipoContratistaSoporteDTO.TIPO){
				//tabla = "";
				//TODO Pendiente definir qu� tabla usar
			}
			
			if (tipoSlip == SlipIncendioSoporteDTO.TIPO){
				tabla = "SlipIncendioDTO";
			}
			
			if (tipoSlip == SlipObraCivilSoporteDTO.TIPO){
				tabla = "SlipObraCivilDTO";
			}
			
			if (tipoSlip == SlipRCConstructoresSoporteDTO.TIPO){
				tabla = "SlipConstructoresDTO";
			}
			
			if (tipoSlip == SlipRCFuncionarioSoporteDTO.TIPO){
				tabla = "SlipFuncionarioDTO";
			}
			
			if (tipoSlip == SlipTransportesSoporteDTO.TIPO){
				//tabla = "";
				//TODO Pendiente definir qu� tabla usar
			}
			
			if (tipoSlip == SlipGeneralSoporteDTO.TIPO){
				//tabla = "";
				//TODO Pendiente definir qu� tabla usar
			}
			
			if (tabla.length() > 0){
				queryString = queryString + tabla + " model WHERE model.slipDTO.idToSlip = :idToSlip";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idToSlip", slipDTO.getIdToSlip());
				query.executeUpdate();
			}
			
		}
	}
 
//	@SuppressWarnings("unchecked")
	public List<ParticipacionFacultativoDTO> findParticipacionFacultativo(BigDecimal idTdContratoFacultativo){
		return findParticipacionFacultativo(idTdContratoFacultativo, null);
	}
	
	@SuppressWarnings("unchecked")
	public List<ParticipacionFacultativoDTO> findParticipacionFacultativo(BigDecimal idTdContratoFacultativo,BigDecimal idTcReaseguradorCorredor){
		List<ParticipacionFacultativoDTO> participacionFacultativoDTO = new ArrayList<ParticipacionFacultativoDTO>();
		LogDeMidasEJB3.log("Buscando participaciones facultativas del tdContratoFacultativo: "+idTdContratoFacultativo+
				(idTcReaseguradorCorredor != null ? ", del reasegurador/Corredor: "+idTcReaseguradorCorredor : ", de cualquier reasegurador/Corredor"), Level.INFO,null);
		try {
			String queryString = "select model from ParticipacionFacultativoDTO model where model.detalleContratoFacultativoDTO.contratoFacultativoDTO = :idTdContratoFacultativo "+
				(idTcReaseguradorCorredor != null ? "and model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor": "");
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTdContratoFacultativo", idTdContratoFacultativo);
			if(idTcReaseguradorCorredor != null)
				query.setParameter("idtcreaseguradorcorredor", idTcReaseguradorCorredor);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			participacionFacultativoDTO = query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
		return participacionFacultativoDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ConfiguracionPagosFacultativoDTO> findConfiguracionPagosFacultativoDTOByContratoFacultativo(BigDecimal idTmContratoFacultativo){
		List<ReaseguradorCorredorDTO> reaseguradorCorredorFromParticipacionList = new ArrayList<ReaseguradorCorredorDTO>();
		List<ReaseguradorCorredorDTO> reaseguradorCorredorFromParticipacionCorredorList = new ArrayList<ReaseguradorCorredorDTO>();
		List<ConfiguracionPagosFacultativoDTO> configuracionPagosFacultativoDTOList = new ArrayList<ConfiguracionPagosFacultativoDTO>();
		List<SuscripcionDTO> suscripcionDTOList;
		ContratoFacultativoDTO contratoFacultativoDTO;
		LogDeMidasEJB3.log("Retrieving all participants in the 'contrato facultativo'", Level.INFO, null);
		try {
			String queryString = "SELECT model FROM ConfiguracionPagosFacultativoDTO model WHERE model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTmContratoFacultativo", idTmContratoFacultativo);
			configuracionPagosFacultativoDTOList = query.getResultList();
			
			if (validarCambioFormaPagoContratoFacultativo(configuracionPagosFacultativoDTOList)){
				/*
				 * 21/09/2010. Se agrega consulta de la linea soporte reaseguro, para obtener el porcentaje facultativo y aplicarlo a cada pago, ya que se aplicaban al 100 % de la prima
				 */
				queryString = "select model.disPrimaPorcentajeFacultativo from LineaSoporteReaseguroDTO model where model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo";
				query = entityManager.createQuery(queryString);
				query.setParameter("idTmContratoFacultativo", idTmContratoFacultativo);
				BigDecimal porcentajeFacultativo = (BigDecimal)query.getSingleResult();
				
				contratoFacultativoDTO = this.findById(idTmContratoFacultativo);
				queryString = "SELECT DISTINCT model.reaseguradorCorredorDTO FROM ParticipacionFacultativoDTO model " +
					"WHERE model.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo";
				query = null;
				query = entityManager.createQuery(queryString);
				query.setParameter("idTmContratoFacultativo", idTmContratoFacultativo);
				reaseguradorCorredorFromParticipacionList = query.getResultList();

				queryString = "SELECT DISTINCT model.reaseguradorCorredorDTO FROM ParticipacionCorredorFacultativoDTO model " +
					"WHERE model.participacionFacultativoDTO.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo";
				query = null;
				query = entityManager.createQuery(queryString);
				query.setParameter("idTmContratoFacultativo", idTmContratoFacultativo);
				reaseguradorCorredorFromParticipacionCorredorList = query.getResultList();
				
				suscripcionDTOList = CalculoSuscripcionesDTO.obtenerSuscripciones(contratoFacultativoDTO.getFechaInicial(), 
						contratoFacultativoDTO.getFechaFinal(), contratoFacultativoDTO.getIdFormaDePago());
				
				for (ReaseguradorCorredorDTO reaseguradorCorredorDTO : reaseguradorCorredorFromParticipacionList) {
					if (reaseguradorCorredorDTO.getTipo().equals("1"))
						this.generarConfiguracionPagosFacultativoPorReasegurador(suscripcionDTOList, porcentajeFacultativo, contratoFacultativoDTO, reaseguradorCorredorDTO, configuracionPagosFacultativoDTOList);
				}
				
				for (ReaseguradorCorredorDTO reaseguradorCorredorDTO : reaseguradorCorredorFromParticipacionCorredorList) {
					boolean band = true;
					for (ReaseguradorCorredorDTO reaseguradorCorredorDTO2 : reaseguradorCorredorFromParticipacionList) {
						if (reaseguradorCorredorDTO.getIdtcreaseguradorcorredor().equals(reaseguradorCorredorDTO2.getIdtcreaseguradorcorredor())){
							band = false;
						}
					}
					if (band && reaseguradorCorredorDTO.getTipo().equals("1"))
						this.generarConfiguracionPagosFacultativoPorReasegurador(suscripcionDTOList, porcentajeFacultativo, contratoFacultativoDTO, reaseguradorCorredorDTO, configuracionPagosFacultativoDTOList);
				}
			}
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("findConfiguracionPagosFacultativoDTOByContratoFacultativo failed", Level.SEVERE, re);
			throw re;
		}
		return configuracionPagosFacultativoDTOList;
	}
	
	private boolean validarCambioFormaPagoContratoFacultativo(List<ConfiguracionPagosFacultativoDTO> configuracionPagosFacultativoDTOList){
		boolean resultado = false;
		
		if (configuracionPagosFacultativoDTOList != null && configuracionPagosFacultativoDTOList.size() > 0){
			if (configuracionPagosFacultativoDTOList.get(0).getFormaPago().intValue() != configuracionPagosFacultativoDTOList.get(0).getContratoFacultativoDTO().getIdFormaDePago().intValue()){
				String queryString = "DELETE FROM ConfiguracionPagosFacultativoDTO model WHERE " +
						"model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idTmContratoFacultativo", configuracionPagosFacultativoDTOList.get(0).getContratoFacultativoDTO().getIdTmContratoFacultativo());
				query.executeUpdate();
				resultado = true;
			}else{
				resultado = false;
			}
		}else{
			resultado = true;
		}
		
		return resultado;
	}
	
	private void generarConfiguracionPagosFacultativoPorReasegurador(List<SuscripcionDTO> suscripcionDTOList, BigDecimal porcentajeFacultativo,
			ContratoFacultativoDTO contratoFacultativoDTO, ReaseguradorCorredorDTO reaseguradorCorredorDTO, List<ConfiguracionPagosFacultativoDTO> configuracionPagosFacultativoDTOList){
		ConfiguracionPagosFacultativoDTO configuracionPagosFacultativoDTO;
		DecimalFormat decimalFormat = new DecimalFormat("0.0000");
		double diff = 0;
		double montoTotal = 0;
		double monto = 0;
		montoTotal = this.obtenerPrimaPorReaseguradorYContratoFacultativo(reaseguradorCorredorDTO, contratoFacultativoDTO, porcentajeFacultativo);
		monto = montoTotal / suscripcionDTOList.size();
		diff = new Double(decimalFormat.format(montoTotal)).doubleValue() - (new Double(decimalFormat.format(monto)).doubleValue() * new Double(suscripcionDTOList.size()).doubleValue());
		int cont = 0;
		for (SuscripcionDTO suscripcionDTO : suscripcionDTOList) {
			cont++;
			configuracionPagosFacultativoDTO = new ConfiguracionPagosFacultativoDTO();
			configuracionPagosFacultativoDTO.setReaseguradorCorredorDTO(reaseguradorCorredorDTO);
			configuracionPagosFacultativoDTO.setFormaPago(contratoFacultativoDTO.getIdFormaDePago().intValue());
			configuracionPagosFacultativoDTO.setContratoFacultativoDTO(contratoFacultativoDTO);
			configuracionPagosFacultativoDTO.setFechaPago(suscripcionDTO.getFechaFinal());
			if (cont == suscripcionDTOList.size())
				monto = monto + diff;
			configuracionPagosFacultativoDTO.setMonto(new Double(decimalFormat.format(monto)));
			configuracionPagosFacultativoDTOList.add(configuracionPagosFacultativoDTO);
			entityManager.persist(configuracionPagosFacultativoDTO);
		}
	}
	
	@SuppressWarnings("unchecked")
	public double obtenerPrimaPorReaseguradorYContratoFacultativo(ReaseguradorCorredorDTO reaseguradorCorredorDTO, ContratoFacultativoDTO contratoFacultativoDTO,BigDecimal porcentajeFacultativo){
		double monto = 0;
		List<ParticipacionFacultativoDTO> participacionFacultativoDTOList;
		List<ParticipacionCorredorFacultativoDTO> participacionCorredorFacultativoDTOList;
		String queryString = "SELECT part FROM ParticipacionFacultativoDTO part " +
		"WHERE part.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo " +
		"AND part.tipo = 1 AND part.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";

		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTmContratoFacultativo", contratoFacultativoDTO.getIdTmContratoFacultativo());
		query.setParameter("idtcreaseguradorcorredor", reaseguradorCorredorDTO.getIdtcreaseguradorcorredor());
		participacionFacultativoDTOList = query.getResultList();

		for (ParticipacionFacultativoDTO participacionFacultativoDTO : participacionFacultativoDTOList) {
			monto = monto + participacionFacultativoDTO.getPorcentajeParticipacion().doubleValue() / 100 * participacionFacultativoDTO.getDetalleContratoFacultativoDTO().getPrimaFacultadaCobertura().doubleValue(); 
		}

		queryString = "SELECT partCorr FROM ParticipacionCorredorFacultativoDTO partCorr WHERE " +
		"partCorr.participacionFacultativoDTO.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo " +
		"AND partCorr.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";

		query = entityManager.createQuery(queryString);
		query.setParameter("idTmContratoFacultativo", contratoFacultativoDTO.getIdTmContratoFacultativo());
		query.setParameter("idtcreaseguradorcorredor", reaseguradorCorredorDTO.getIdtcreaseguradorcorredor());
		participacionCorredorFacultativoDTOList = query.getResultList();
		
		for (ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO : participacionCorredorFacultativoDTOList) {
			monto = monto + participacionCorredorFacultativoDTO.getPorcentajeParticipacion().doubleValue() / 100 * participacionCorredorFacultativoDTO.getParticipacionFacultativoDTO().getDetalleContratoFacultativoDTO().getPrimaFacultadaCobertura().doubleValue();
		}
		
		return monto*porcentajeFacultativo.doubleValue()/100d;
	}
	
	public void deleteSlipAndDetails(BigDecimal idToSlip){
		SlipDTO slipDTO = entityManager.find(SlipDTO.class, idToSlip);
		try{
			List<ContratoFacultativoDTO> contratoFacultativoDTOList;
			if (slipDTO != null){
				LineaSoporteReaseguroId lineaSoporteReaseguroId = new LineaSoporteReaseguroId();
				lineaSoporteReaseguroId.setIdTmLineaSoporteReaseguro(slipDTO.getIdTmLineaSoporteReaseguro());
				lineaSoporteReaseguroId.setIdToSoporteReaseguro(slipDTO.getIdToSoporteReaseguro());
				LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = entityManager.find(LineaSoporteReaseguroDTO.class, lineaSoporteReaseguroId);
				lineaSoporteReaseguroDTO.setContratoFacultativoDTO(null);
				entityManager.merge(lineaSoporteReaseguroDTO);
				eliminarSlipsParticularesPorSlipDTO(slipDTO);
				if (slipDTO.getEstatusCotizacion().intValue() == 0){
					contratoFacultativoDTOList = this.findByProperty("slipDTO.idToSlip", slipDTO.getIdToSlip());
					for (ContratoFacultativoDTO contratoFacultativoDTO : contratoFacultativoDTOList) {
						if (contratoFacultativoDTO != null){
							this.deleteWithDetails(contratoFacultativoDTO);
						}
					}
				}
			}
		}catch (RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("updating failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)	
	public boolean modificarNotaCobertura(ContratoFacultativoDTO contratoFacultativoDTO){
		boolean resultado = false;
		
		if (contratoFacultativoDTO != null && contratoFacultativoDTO.getIdTmContratoFacultativo() != null ){
			ContratoFacultativoDTO contratoFacultativoTMP = findById(contratoFacultativoDTO.getIdTmContratoFacultativo());
			contratoFacultativoTMP.setAjustadorNombrado(contratoFacultativoDTO.getAjustadorNombrado());
			contratoFacultativoTMP.setNotaCobertura(contratoFacultativoDTO.getNotaCobertura());
			update(contratoFacultativoTMP);
			resultado = true;			
		}
		return resultado;	
	}


	public int obtenerCantidadContratosPorCotizacion(BigDecimal idToCotizacion,BigDecimal estatus) {
		LogDeMidasEJB3.log("contando registros ContratoFacultativoDTO para la cotizacion: " + idToCotizacion, Level.INFO, null);
		try {
			final String queryString = "select count model.idTmContratoFacultativo from ContratoFacultativoDTO model where model.slipDTO.idToSlip in " +
					"(select slip.idToSlip from SlipDTO slip where slip.idToCotizacion = :idToCotizacion)" +
					(estatus!=null?" and model.estatus = :estatus":"");
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			if (estatus != null)	query.setParameter("estatus", estatus);
			Object result = query.getSingleResult();
			int cantidadContratos=0;// = (BigDecimal) query.getSingleResult();
			if(result == null)
				cantidadContratos = 0;
			else if(result instanceof Long)
				cantidadContratos = ((Long)result).intValue();
			else if(result instanceof Integer)
				cantidadContratos = ((Integer)result).intValue();
			else if(result instanceof BigDecimal)
				cantidadContratos = ((BigDecimal)result).intValue();
			return cantidadContratos;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� el conteo de registros ContratoFacultativoDTO para la cotizacion: " + idToCotizacion, Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ContratoFacultativoDTO> obtenerContratosPorCotizacion(BigDecimal idToCotizacion,BigDecimal estatus) {
		LogDeMidasEJB3.log("listando registros ContratoFacultativoDTO para la cotizacion: " + idToCotizacion, Level.INFO, null);
		try {
			String queryString = "select model from ContratoFacultativoDTO model where model.slipDTO.idToSlip in " +
					"(select slip.idToSlip from SlipDTO slip where slip.idToCotizacion = :idToCotizacion)"+
					(estatus!=null?" and model.estatus = :estatus":"");
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			if (estatus != null)	query.setParameter("estatus", estatus);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� el conteo de registros ContratoFacultativoDTO para la cotizacion: " + idToCotizacion, Level.SEVERE, re);
			throw re;
		}
	}
	
	public ContratoFacultativoDTO duplicarContratoFacultativo(BigDecimal idTmContratoFacultativo, BigDecimal idToSlip, int numeroEndosoFacultativo){
		LogDeMidasEJB3.log("Duplicating ContratoFacultativoDTO with id: " + idTmContratoFacultativo.toBigInteger().toString() 
				+ " and SlipDTO with id: " + idToSlip.toBigInteger().toString(), Level.INFO, null);
		int numeroDetalles = 0;
//		@SuppressWarnings("unused")
//		int numeroParticipaciones = 0;
//		@SuppressWarnings("unused")
//		int numeroParticipacionesFac = 0;
		ContratoFacultativoDTO contratoFacultativoDTO = entityManager.find(ContratoFacultativoDTO.class, idTmContratoFacultativo);
		ContratoFacultativoDTO contratoFacultativoDTODuplicate = contratoFacultativoDTO.duplicate();
		//Crear una nueva lista para el detalle, no debe ser la misma del otro contrato que se duplic�
		List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOs = 
			new ArrayList<DetalleContratoFacultativoDTO>(); 
		contratoFacultativoDTODuplicate.setDetalleContratoFacultativoDTOs(detalleContratoFacultativoDTOs);
		
		SlipDTO slipNuevo = entityManager.find(SlipDTO.class, idToSlip);
		LineaSoporteReaseguroDTO lineaSoporteNueva = lineaSoporteReaseguroFacade.findByProperty("id.idTmLineaSoporteReaseguro", slipNuevo.getIdTmLineaSoporteReaseguro()).get(0);
		contratoFacultativoDTODuplicate.setSumaAseguradaTotal(lineaSoporteNueva.getMontoSumaAsegurada());		
		contratoFacultativoDTODuplicate.setSlipDTO(slipNuevo);
		contratoFacultativoDTODuplicate.setNumeroEndosoFacultativo(numeroEndosoFacultativo);
		contratoFacultativoDTODuplicate.setIdTmContratoFacultativoAnterior(contratoFacultativoDTO.getIdTmContratoFacultativo());
		entityManager.persist(contratoFacultativoDTODuplicate);
		numeroDetalles = contratoFacultativoDTO.getDetalleContratoFacultativoDTOs().size();
		if (numeroDetalles > 0){
			LogDeMidasEJB3.log("\nPreparing to duplicate " + numeroDetalles + " DetalleContratoFacultativoDTO's", Level.INFO, null);
		}
		// cambiar por lineasoportereaseguro
		//LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = entityManager.find(LineaSoporteReaseguroDTO.class, contratoFacultativoDTO.getLineaSoporteReaseguroDTOs().get(0).getId());
//		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTOList =  entityManager.find(LineaSoporteReaseguroDTO.class, contratoFacultativoDTO.getLineaSoporteReaseguroDTOs().get(0).getId());  //soporteNuevoEndoso.getIdToSoporteReaseguro()
		BigDecimal sumatoriaPrimaAdicional = new BigDecimal(0d);
		for(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : lineaSoporteNueva.getLineaSoporteCoberturaDTOs()){
			BigDecimal montoPrimaSubscripcion = lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion();
			//BigDecimal montoPrimaNoDevengada = lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada();
			BigDecimal primaAdicional = (lineaSoporteCoberturaDTO.getMontoPrimaAdicional() != null ? lineaSoporteCoberturaDTO.getMontoPrimaAdicional() : BigDecimal.ZERO).abs();
			//Se omite la resta de la prima no devengada para generar correctamente el plan de pagos. En el action se manejan ambos casos (con y sin prima no devengada)
			BigDecimal primaEndoso = montoPrimaSubscripcion/*.subtract(montoPrimaNoDevengada.subtract(primaAdicional))*/;
			
			LogDeMidasEJB3.log("Duplicating lineaSoporteReaseguroDTO with id: " + lineaSoporteNueva.getId().getIdTmLineaSoporteReaseguro()+", idTdLineaSoporteCobertura: "+lineaSoporteCoberturaDTO.getId().getIdTdLineaSoporteCobertura(), Level.INFO, null);
			DetalleContratoFacultativoDTO detalleContratoFacultativoDTONuevo = new DetalleContratoFacultativoDTO();
			detalleContratoFacultativoDTONuevo.setContratoFacultativoDTO_1(contratoFacultativoDTODuplicate);
			// ? lineaSoporteCoberturaDTO.setDeducibleCobertura
			detalleContratoFacultativoDTONuevo.setDeducibleCobertura(lineaSoporteCoberturaDTO.getPorcentajeDeducible());
			detalleContratoFacultativoDTONuevo.setIdTcSubramo(lineaSoporteNueva.getLineaDTO().getSubRamo().getIdTcSubRamo());
			detalleContratoFacultativoDTONuevo.setIdToCobertura(lineaSoporteCoberturaDTO.getIdToCobertura());
			detalleContratoFacultativoDTONuevo.setIdToCotizacion(lineaSoporteNueva.getIdToCotizacion());
			detalleContratoFacultativoDTONuevo.setIdToSeccion(lineaSoporteCoberturaDTO.getIdToSeccion());
			detalleContratoFacultativoDTONuevo.setNumeroInciso(lineaSoporteCoberturaDTO.getNumeroInciso());
			detalleContratoFacultativoDTONuevo.setNumeroSubInciso(lineaSoporteCoberturaDTO.getNumeroSubInciso());
			detalleContratoFacultativoDTONuevo.setPorcentajeComision(BigDecimal.ZERO);
			detalleContratoFacultativoDTONuevo.setPrimaFacultadaCobertura(primaEndoso);
			detalleContratoFacultativoDTONuevo.setPrimaTotalCobertura(primaEndoso);
			detalleContratoFacultativoDTONuevo.setSumaAsegurada(lineaSoporteNueva.getMontoSumaAsegurada());
			detalleContratoFacultativoDTONuevo.setSumaFacultada(lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion());
			detalleContratoFacultativoDTONuevo.setTipoDistribucion(lineaSoporteNueva.getLineaDTO().getTipoDistribucion());
			detalleContratoFacultativoDTONuevo.setMontoPrimaAdicional(primaAdicional);
			detalleContratoFacultativoDTONuevo.setMontoPrimaNoDevengada(lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada() != null ?
					lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada() : BigDecimal.ZERO);
			
			if (lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada() == null)
				lineaSoporteCoberturaDTO.setMontoPrimaNoDevengada(BigDecimal.ZERO);
			
			if (lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion().doubleValue() == 0 && !(lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada().doubleValue() == 0)){
				// Caso en que Prima Suscripcion (PS) es cero y la Prima No Devengada (PND) es diferente de cero, en este caso esta cobertura ha sido eliminada.
				//TODO Generar la cobertura y dado que est� cancelada se debe marcar con el estatus DetalleContratoFacultativoDTO.ESTATUS_COBERTURA_CANCELADA
				detalleContratoFacultativoDTONuevo.setEstatus(DetalleContratoFacultativoDTO.ESTATUS_COBERTURA_CANCELADA);
				primaAdicional = primaAdicional.multiply(new BigDecimal(-1));
				detalleContratoFacultativoDTONuevo.setMontoPrimaAdicional(primaAdicional);
			}else{
				if (! (lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion().doubleValue() == 0 )&& lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada().doubleValue() == 0){
					// Caso en que PS es diferente de cero y PND es cero, en este caso esta cobertura es nueva.
					//TODO Generar la cobertura y dado que es nueva se debe marcar con el estatus = DetalleContratoFacultativoDTO.ESTATUS_INICIAL_COBERTURA.
					detalleContratoFacultativoDTONuevo.setEstatus(DetalleContratoFacultativoDTO.ESTATUS_INICIAL_COBERTURA);
					
				}else{
					if (!(lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion().doubleValue() == 0) && !(lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada().doubleValue() == 0)){
						// Caso en que PS y PND son diferentes de cero, en este caso esta cobertura ha sido modificada.
						//TODO Generar la cobertura, se debe marcar con el estatus = DetalleContratoFacultativoDTO.ESTATUS_INICIAL_COBERTURA.
						detalleContratoFacultativoDTONuevo.setEstatus(DetalleContratoFacultativoDTO.ESTATUS_INICIAL_COBERTURA);
					}else{
						throw new RuntimeException("La cobertura presenta un estatus inconsistente, o no se ha considerado dicho estatus");
					}
				}
			}
			sumatoriaPrimaAdicional = sumatoriaPrimaAdicional.add(primaAdicional);
			contratoFacultativoDTODuplicate.getDetalleContratoFacultativoDTOs().add(detalleContratoFacultativoDTONuevo);
			// Para que el ID se asigne de inmediato.
			entityManager.persist(detalleContratoFacultativoDTONuevo); 
		}
		contratoFacultativoDTODuplicate.setMontoPrimaAdicional(sumatoriaPrimaAdicional);
		entityManager.merge(contratoFacultativoDTODuplicate);
	
		lineaSoporteNueva.setContratoFacultativoDTO(contratoFacultativoDTODuplicate);
		lineaSoporteNueva.setEstatusFacultativo(LineaSoporteReaseguroDTO.ESTATUS_FACULTATIVO_SOLICITADO);
		entityManager.merge(lineaSoporteNueva);
		
		//Ademas de duplicar el contrato se crean las participaciones.
		copiarParticipacionesFacultativoEndosoAnterior(contratoFacultativoDTODuplicate);
		
		return contratoFacultativoDTODuplicate;		
	}
		
	@SuppressWarnings("unchecked")
	public List<ContratoFacultativoDTO> notificacionEndoso(SoporteReaseguroDTO soporteNuevoEndoso, List<Object[]> slipDTO_contratoFacultativoSoportaLineaList, int tipoEndoso){
		LogDeMidasEJB3.log("Iniciando el proceso de notificacionEndoso a Facultativo", Level.INFO, null);
		for (Object[] slipDTO_contratoFacultativoSoportaLinea : slipDTO_contratoFacultativoSoportaLineaList) {
			SlipDTO slipDTO = (SlipDTO) slipDTO_contratoFacultativoSoportaLinea[0];
			boolean contratoFacultativoSoportaLinea = ((Boolean) slipDTO_contratoFacultativoSoportaLinea[1]).booleanValue();
			entityManager.persist(slipDTO);
			
			LineaSoporteReaseguroId lineaSoporteReaseguroId = new LineaSoporteReaseguroId();
			lineaSoporteReaseguroId.setIdTmLineaSoporteReaseguro(slipDTO.getIdTmLineaSoporteReaseguro());
			lineaSoporteReaseguroId.setIdToSoporteReaseguro(slipDTO.getIdToSoporteReaseguro());
			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = entityManager.find(LineaSoporteReaseguroDTO.class, lineaSoporteReaseguroId);
			ContratoFacultativoDTO contratoFacultativoDTO = lineaSoporteReaseguroDTO.getContratoFacultativoDTO();
			if (contratoFacultativoDTO != null && (contratoFacultativoSoportaLinea || tipoEndoso == TIPO_ENDOSO_CANCELACION || tipoEndoso == TIPO_ENDOSO_REHABILITACION)){
				if (tipoEndoso != TIPO_ENDOSO_CANCELACION){
					slipDTO.setEstatusCotizacion(new BigDecimal("2")); //Este estatus indica que el contrato se mantiene y por lo tanto debe seguir autorizado
					entityManager.merge(slipDTO);
				}
				contratoFacultativoDTO.setSlipDTO(slipDTO);
				entityManager.merge(contratoFacultativoDTO); //El contratoFacultativoDTO siempre apuntar� al Slip del �ltimo endoso de p�liza
			}else{
				if (!contratoFacultativoSoportaLinea){
					lineaSoporteReaseguroDTO.setContratoFacultativoDTO(null);
					entityManager.merge(lineaSoporteReaseguroDTO);
				}
			}
		}

		List<ContratoFacultativoDTO> contratoFacultativoDTOList;
		String queryString = "SELECT lsr.contratoFacultativoDTO FROM LineaSoporteReaseguroDTO lsr, SlipDTO slip WHERE " +
		"lsr.soporteReaseguroDTO.idToSoporteReaseguro = :idToSoporteReaseguro AND " +
		"slip.idTmLineaSoporteReaseguro = lsr.id.idTmLineaSoporteReaseguro AND " +
		"slip.idToSoporteReaseguro = lsr.id.idToSoporteReaseguro AND " +
		"slip.numeroEndoso = :numeroEndoso AND slip.tipoEndoso = :tipoEndoso";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToSoporteReaseguro", soporteNuevoEndoso.getIdToSoporteReaseguro());
		query.setParameter("numeroEndoso", soporteNuevoEndoso.getNumeroEndoso());
		query.setParameter("tipoEndoso", new Integer(tipoEndoso));
		contratoFacultativoDTOList = query.getResultList();
		
		try{
			if (tipoEndoso == TIPO_ENDOSO_CANCELACION){
				this.notificacionEndosoCancelacion(contratoFacultativoDTOList);
			}else{
				if (tipoEndoso == TIPO_ENDOSO_REHABILITACION){
					this.notificacionEndosoRehabilitacion(contratoFacultativoDTOList);
				}else{
					if (tipoEndoso == TIPO_ENDOSO_AUMENTO){
						this.notificacionEndosoAumentoDisminucionCoberturas(contratoFacultativoDTOList, soporteNuevoEndoso);
					}else{
						if (tipoEndoso == TIPO_ENDOSO_DISMINUCION){
							
						}
					}
				}
			}
		}catch(RuntimeException re){
			LogDeMidasEJB3.log("Fall� el proceso de notificacionEndoso a Facultativo", Level.SEVERE, re);
			throw re;
		}
		LogDeMidasEJB3.log("Finaliz� correctamente el proceso de notificacionEndoso a Facultativo", Level.INFO, null);
		return null;
	}
	
	private void notificacionEndosoCancelacion(List<ContratoFacultativoDTO> contratoFacultativoDTOList){
		LogDeMidasEJB3.log("Iniciando Notificaci�n endoso de cancelaci�n", Level.INFO, null);
		List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOList;
		try{
			for (ContratoFacultativoDTO contratoFacultativoDTO : contratoFacultativoDTOList) {
				contratoFacultativoDTO.getDetalleContratoFacultativoDTOs().size(); //Vuelve eager la relaci�n lazy
				detalleContratoFacultativoDTOList = contratoFacultativoDTO.getDetalleContratoFacultativoDTOs();
				for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : detalleContratoFacultativoDTOList) {
					detalleContratoFacultativoDTO.setEstatus(DetalleContratoFacultativoDTO.ESTATUS_COBERTURA_CANCELADA);
					entityManager.merge(detalleContratoFacultativoDTO);
				}
				contratoFacultativoDTO.setEstatus(new BigDecimal("0"));
				entityManager.merge(contratoFacultativoDTO);
			}
		}catch(RuntimeException re){
			LogDeMidasEJB3.log("Fall� la Notificaci�n de endoso de cancelaci�n", Level.SEVERE, re);
			throw re;
		}		
	}
	
	private void notificacionEndosoRehabilitacion(List<ContratoFacultativoDTO> contratoFacultativoDTOList) throws RuntimeException{
		LogDeMidasEJB3.log("Iniciando Notificaci�n endoso de rehabilitaci�n", Level.INFO, null);
		List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOList;
		try{
			for (ContratoFacultativoDTO contratoFacultativoDTO : contratoFacultativoDTOList) {
				contratoFacultativoDTO.getDetalleContratoFacultativoDTOs().size(); //Vuelve eager la relaci�n lazy
				detalleContratoFacultativoDTOList = contratoFacultativoDTO.getDetalleContratoFacultativoDTOs();
				for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : detalleContratoFacultativoDTOList) {
					detalleContratoFacultativoDTO.setEstatus(DetalleContratoFacultativoDTO.ESTATUS_INICIAL_COBERTURA);
					entityManager.merge(detalleContratoFacultativoDTO);
				}
				contratoFacultativoDTO.setEstatus(new BigDecimal("0"));
				entityManager.merge(contratoFacultativoDTO);

				this.procesarMovimientosCancelacionRehabilitacionCobertura(contratoFacultativoDTO.getIdTmContratoFacultativo(), 1, 1);
			}
			
			
		}catch(RuntimeException re){
			LogDeMidasEJB3.log("Fall� la Notificaci�n de endoso de rehabilitaci�n", Level.SEVERE, re);
			throw re;
		}		
	}
	
	

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ContratoFacultativoDTO procesarMovimientosCancelacionRehabilitacionCobertura(BigDecimal idTmContratoFacultativo, int tipoMovimiento, int movimiento) throws RuntimeException{
		 LogDeMidasEJB3.log("Prima Devengada ContratoFacultativoDTO with id: " + idTmContratoFacultativo.toBigInteger().toString(), Level.INFO, null);
		 	// tipoMovimiento Cancelacion o Rehabilitacion
		 	// movimiento
		    @SuppressWarnings("unused")
			int numeroDetalles = 0;
			
			ContratoFacultativoDTO contratoFacultativoDTO = entityManager.find(ContratoFacultativoDTO.class, idTmContratoFacultativo);
			band = 0;
			numeroDetalles=contratoFacultativoDTO.getDetalleContratoFacultativoDTOs().size();
			// si el valor tipoMovimiento=0 quiere decir que esta cancelado
			// movimiento es lo mismo?
			if (movimiento == 0) {
				for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : contratoFacultativoDTO.getDetalleContratoFacultativoDTOs()) {
					if(detalleContratoFacultativoDTO.getEstatus() != null && detalleContratoFacultativoDTO.getEstatus().intValue() == 1){
						band=1;
						throw new RuntimeException("Se encontro una cobertura no cancelada.");
					}
				}
			}
			int tipoMov = 0;
			if (tipoMovimiento == 1){
				tipoMov = 0;
			}else {
				tipoMov = 1;
			}
			numeroDetalles=contratoFacultativoDTO.getDetalleContratoFacultativoDTOs().size();
			for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : contratoFacultativoDTO.getDetalleContratoFacultativoDTOs()) {
				 //DetalleContratoFacultativoDTO detalleContratoFacultativoDTONuevo = new DetalleContratoFacultativoDTO();
				 // crear objeto movimiento reaseguro dto
				 //detalleContratoFacultativoDTONuevo.getMontoPrimaNoDevengada();				
				 detalleContratoFacultativoDTO.getParticipacionFacultativoDTOs().size();
				 for (ParticipacionFacultativoDTO participacionFacultativoDTO : detalleContratoFacultativoDTO.getParticipacionFacultativoDTOs()) {
					 ParticipacionFacultativoDTO participacionFacultativoDTONuevo = new ParticipacionFacultativoDTO();				 
					 if(participacionFacultativoDTO.getTipo().equals("0")){					 
						 participacionFacultativoDTONuevo.getParticipacionCorredorFacultativoDTOs().size();
						 for (ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO : participacionFacultativoDTO.getParticipacionCorredorFacultativoDTOs()) {
							 MovimientoReaseguroDTO movimientoReaseguroDTO= new MovimientoReaseguroDTO();
						     LineaSoporteReaseguroId lineaSoporteReaseguroId = new LineaSoporteReaseguroId();
						     lineaSoporteReaseguroId.setIdTmLineaSoporteReaseguro(contratoFacultativoDTO.getSlipDTO().getIdTmLineaSoporteReaseguro());
						     lineaSoporteReaseguroId.setIdToSoporteReaseguro(contratoFacultativoDTO.getSlipDTO().getIdToSoporteReaseguro());
							 LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = entityManager.find(LineaSoporteReaseguroDTO.class, lineaSoporteReaseguroId);
							 movimientoReaseguroDTO.setIdPoliza(lineaSoporteReaseguroDTO.getIdToPoliza());
							 SlipDTO slipDTO = contratoFacultativoDTO.getSlipDTO();
							 movimientoReaseguroDTO.setNumeroEndoso(slipDTO.getNumeroEndoso());
							 movimientoReaseguroDTO.setIdCobertura(detalleContratoFacultativoDTO.getIdToCobertura());
							 SubRamoDTO subRamoDTO = entityManager.find(SubRamoDTO.class, slipDTO.getIdTcSubRamo());
							 subRamoDTO.setIdTcSubRamo(slipDTO.getIdTcSubRamo());
							 movimientoReaseguroDTO.setSubRamo(subRamoDTO);
							 movimientoReaseguroDTO.setIdSubInciso(detalleContratoFacultativoDTO.getNumeroSubInciso());
							 movimientoReaseguroDTO.setTipoReaseguro(new Integer(slipDTO.getIdToSoporteReaseguro().intValue())); 
							 
							 if (tipoMov == 0) {
								 movimientoReaseguroDTO.setTipoMovimiento(2);
								 movimientoReaseguroDTO.setConceptoMovimiento(2);
							 }else{
								 movimientoReaseguroDTO.setTipoMovimiento(1);
								 movimientoReaseguroDTO.setConceptoMovimiento(1);
							 }
							 							 
							 BigDecimal Cantidad = new BigDecimal("0");							 
							 Cantidad = detalleContratoFacultativoDTO.getMontoPrimaNoDevengada().multiply(participacionCorredorFacultativoDTO.getPorcentajeParticipacion()).divide(BigDecimal.valueOf(100.0));
							 Cantidad.setScale(10,RoundingMode.HALF_UP);
							 movimientoReaseguroDTO.setCantidad(Cantidad);
							 
							 movimientoReaseguroDTO.setIdMoneda(new Integer(lineaSoporteReaseguroDTO.getIdMoneda().intValue()));
							 //movimientoReaseguroDTO.setDescripcion("esto es una prueba 1");
							 ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
						 	 reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(participacionFacultativoDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
							 movimientoReaseguroDTO.setReaseguradorCorredor(participacionFacultativoDTO.getReaseguradorCorredorDTO());
							 movimientoReaseguroDTO.setFechaRegistro(new Date());
							 movimientoReaseguroDTO.setAcumulado(1);
							 movimientoReaseguroDTO.setContratoFacultativoDTO(contratoFacultativoDTO);
							 
							 movimientoReaseguroServicios.registrarMovimiento(movimientoReaseguroDTO,Boolean.FALSE);
							 
						 }						 
					 }else if (participacionFacultativoDTO.getTipo().equals("1")){
						  //ContratoFacultativoDTO contratoFacultativoDTO2 = entityManager.find(ContratoFacultativoDTO.class, idTmContratoFacultativo);
						  //ParticipacionFacultativoDTO participacionFacultativoDTO2 = entityManager.find(ParticipacionFacultativoDTO.class, idTmContratoFacultativo);
						
						// for (ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO2 : participacionFacultativoDTO.getParticipacionCorredorFacultativoDTOs()) {
						     MovimientoReaseguroDTO movimientoReaseguroDTO= new MovimientoReaseguroDTO();
						     LineaSoporteReaseguroId lineaSoporteReaseguroId = new LineaSoporteReaseguroId();
						     lineaSoporteReaseguroId.setIdTmLineaSoporteReaseguro(contratoFacultativoDTO.getSlipDTO().getIdTmLineaSoporteReaseguro());
						     lineaSoporteReaseguroId.setIdToSoporteReaseguro(contratoFacultativoDTO.getSlipDTO().getIdToSoporteReaseguro());
							 LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = entityManager.find(LineaSoporteReaseguroDTO.class, lineaSoporteReaseguroId);
							 movimientoReaseguroDTO.setIdPoliza(lineaSoporteReaseguroDTO.getIdToPoliza());
							 SlipDTO slipDTO = contratoFacultativoDTO.getSlipDTO();
							 movimientoReaseguroDTO.setNumeroEndoso(slipDTO.getNumeroEndoso());
							 movimientoReaseguroDTO.setIdCobertura(detalleContratoFacultativoDTO.getIdToCobertura());
							 SubRamoDTO subRamoDTO = entityManager.find(SubRamoDTO.class, slipDTO.getIdTcSubRamo());
							 subRamoDTO.setIdTcSubRamo(slipDTO.getIdTcSubRamo());
							 movimientoReaseguroDTO.setSubRamo(subRamoDTO);
							 movimientoReaseguroDTO.setIdSubInciso(detalleContratoFacultativoDTO.getNumeroSubInciso());
							 movimientoReaseguroDTO.setTipoReaseguro(new Integer(slipDTO.getIdToSoporteReaseguro().intValue())); 
							 
							 if (tipoMov == 0) {
								 movimientoReaseguroDTO.setTipoMovimiento(2);
								 movimientoReaseguroDTO.setConceptoMovimiento(2);
							 }else{
								 movimientoReaseguroDTO.setTipoMovimiento(1);
								 movimientoReaseguroDTO.setConceptoMovimiento(1);
							 }
							 	 							 
							 BigDecimal Cantidad = new BigDecimal("0");
							 try{
								 Cantidad = detalleContratoFacultativoDTO.getMontoPrimaNoDevengada().multiply(participacionFacultativoDTO.getPorcentajeParticipacion()).divide(BigDecimal.valueOf(100.0));
							 }catch(Exception e){
								 Cantidad = BigDecimal.ZERO;
							 }
							 Cantidad.setScale(10,RoundingMode.HALF_UP);
							 movimientoReaseguroDTO.setCantidad(Cantidad);
									 
							 movimientoReaseguroDTO.setIdMoneda(new Integer(lineaSoporteReaseguroDTO.getIdMoneda().intValue()));
							 //movimientoReaseguroDTO.setDescripcion("esto es una prueba 1");
							 ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
							 reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(participacionFacultativoDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
							 movimientoReaseguroDTO.setReaseguradorCorredor(participacionFacultativoDTO.getReaseguradorCorredorDTO());
							 movimientoReaseguroDTO.setFechaRegistro(new Date());
							 movimientoReaseguroDTO.setAcumulado(1);
							 movimientoReaseguroDTO.setLineaDTO(lineaSoporteReaseguroDTO.getLineaDTO());			
							 movimientoReaseguroDTO.setContratoFacultativoDTO(contratoFacultativoDTO);
								 
							 movimientoReaseguroServicios.registrarMovimiento(movimientoReaseguroDTO,Boolean.FALSE);
						}												 
					// }				 		 
				 }				 
			}			
			/*
			for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : contratoFacultativoDTO.getDetalleContratoFacultativoDTOs()) {
				 DetalleContratoFacultativoDTO detalleContratoFacultativoDTONuevo = new DetalleContratoFacultativoDTO();
				 // crear objeto movimiento reaseguro dto
				 
				 MovimientoReaseguroDTO movimientoReaseguroDTO = new MovimientoReaseguroDTO();
				 movimientoReaseguroDTO.setTipoMovimiento(TipoMovimientoDTO.COBRO); //TODO Validar con Dulce
				 LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = entityManager.find(LineaSoporteReaseguroDTO.class, contratoFacultativoDTO.getSlipDTO().getIdTmLineaSoporteReaseguro());
				 if (lineaSoporteReaseguroDTO != null){			 
				 movimientoReaseguroDTO.setLineaDTO(lineaSoporteReaseguroDTO.getLineaDTO());
				 detalleContratoFacultativoDTONuevo.getMontoPrimaNoDevengada();
				 registrarMovimiento(algo);
				 }else{
					 throw new RuntimeException("No se encontro una LineaSoporteReaseguroDTO para el ContratoFacultativoDTO con id: " + contratoFacultativoDTO.getIdTmContratoFacultativo());
				 }
			}			*/
			
			return null;	
	}
	
	private void notificacionEndosoAumentoDisminucionCoberturas(List<ContratoFacultativoDTO> contratoFacultativoDTOList, SoporteReaseguroDTO soporteActual){
		LogDeMidasEJB3.log("Iniciando Notificaci�n endoso de rehabilitaci�n", Level.INFO, null);
		List<LineaSoporteCoberturaDTO> lineaSoporteCoberturaList;
		SlipDTO slipDTO;
		for (ContratoFacultativoDTO contratoFacultativoDTO : contratoFacultativoDTOList) {
			slipDTO = contratoFacultativoDTO.getSlipDTO();
			if (slipDTO != null){
				//TODO Se debe duplicar el contratoFacultativoDTO usando el m�todo contratoFacultativoDTO.duplicate, debe asign�rsele el SlipDTO reci�n recuperado y una vez persisitido el nuevo contrato
				//   se debe actualizar la LineaSoporteReaseguroDTO a la que pertenece el SlipDTO ya mencionado
				// IMPORTANTE: El SlipDTO debe de persistirse con estatusCotizacion = 0 para que el contrato entre al flujo de facultativo desde la Negociaci�n
				lineaSoporteCoberturaList = this.obtenerLineaSoporteCoberturas("id.idTmLineaSoporteReaseguro", slipDTO.getIdTmLineaSoporteReaseguro());
				for (LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : lineaSoporteCoberturaList) {
					if (lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion().equals(new Double("0")) && !lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada().equals(new BigDecimal("0"))){
						// Caso en que Prima Suscripcion (PS) es cero y la Prima No Devengada (PND) es diferente de cero, en este caso esta cobertura ha sido eliminada.
						//TODO Generar la cobertura y dado que est� cancelada se debe marcar con el estatus DetalleContratoFacultativoDTO.ESTATUS_COBERTURA_CANCELADA
					}else{
						if (!lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion().equals(new Double("0")) && lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada().equals(new BigDecimal("0"))){
							// Caso en que PS es diferente de cero y PND es cero, en este caso esta cobertura es nueva.
							//TODO Generar la cobertura y dado que es nueva se debe marcar con el estatus = DetalleContratoFacultativoDTO.ESTATUS_INICIAL_COBERTURA.
						}else{
							if (!lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion().equals(new Double("0")) && !lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada().equals(new BigDecimal("0"))){
								// Caso en que PS y PND son diferentes de cero, en este caso esta cobertura ha sido modificada.
								//TODO Generar la cobertura, se debe marcar con el estatus = DetalleContratoFacultativoDTO.ESTATUS_INICIAL_COBERTURA.
							}else{
								throw new RuntimeException("La cobertura presenta un estatus inconsistente, o no se ha considerado dicho estatus");
							}
						}
					}

				}
			}
		}
	}
	
	public ContratoFacultativoDTO copiarParticipacionesFacultativoEndosoAnterior(
			BigDecimal idTmContratoFacultativoActual) {
		ContratoFacultativoDTO contratoFacultativoActual = 
			findById(idTmContratoFacultativoActual);
		return copiarParticipacionesFacultativoEndosoAnterior(contratoFacultativoActual);
	}
	
	public DetalleContratoFacultativoDTO copiarParticipacionesFacultativoCoberturaEndosoAnterior(
			BigDecimal idTdContratoFacultativoActual) {
		DetalleContratoFacultativoDTO detalleContratoFacultativoActual = entityManager.find(
				DetalleContratoFacultativoDTO.class, idTdContratoFacultativoActual);
		return copiarParticipacionesFacultativoCoberturaEndosoAnterior(detalleContratoFacultativoActual);
	}


	private ContratoFacultativoDTO copiarParticipacionesFacultativoEndosoAnterior(
			ContratoFacultativoDTO contratoFacultativoActual) {
		ContratoFacultativoDTO contratoFacultativoAnterior =
			findById(contratoFacultativoActual.getIdTmContratoFacultativoAnterior());
		if (contratoFacultativoAnterior != null) {
			//Validar si la suma asegurada disminuyo.
//			BigDecimal sumaAseguradaActual = contratoFacultativoActual.getSumaAseguradaTotal();
//			BigDecimal sumaAseguradaAnterior = contratoFacultativoAnterior.getSumaAseguradaTotal();
			//Se coment� para poder copiar los reaseguradores del endoso anterior (cancelacion de endosos 
//			if (sumaAseguradaActual.compareTo(sumaAseguradaAnterior) <= 0) {
				for (DetalleContratoFacultativoDTO detalleContratoFacultativoActual : 
					contratoFacultativoActual.getDetalleContratoFacultativoDTOs()) {
					copiarParticipacionesFacultativoCoberturaEndosoAnterior(detalleContratoFacultativoActual);
				}
//			}
		}
		return contratoFacultativoActual;
	}
	
	private DetalleContratoFacultativoDTO copiarParticipacionesFacultativoCoberturaEndosoAnterior(
			DetalleContratoFacultativoDTO detalleContratoFacultativoActual) {
		ContratoFacultativoDTO contratoFacultativoActual = 
			detalleContratoFacultativoActual.getContratoFacultativoDTO_1();
		ContratoFacultativoDTO contratoFacultativoAnterior = 
			findById(contratoFacultativoActual.getIdTmContratoFacultativoAnterior());
		
		
		BigDecimal idTmContratoFacultativo = contratoFacultativoAnterior.getIdTmContratoFacultativo();
		BigDecimal idTcSubramo = detalleContratoFacultativoActual
				.getIdTcSubramo();
		BigDecimal idToCobertura = detalleContratoFacultativoActual
				.getIdToCobertura();
		BigDecimal numeroInciso = detalleContratoFacultativoActual
				.getNumeroInciso();
		BigDecimal numeroSubInciso = detalleContratoFacultativoActual
				.getNumeroSubInciso();
		BigDecimal idToSeccion = detalleContratoFacultativoActual
				.getIdToSeccion();
		
		//Existe la cobertura en el contrato anterior?.
		DetalleContratoFacultativoDTO detalleContratoFacultativoAnterior = 
			detalleContratoFacultativoFacadeRemote.findByNaturalKey(
				idTmContratoFacultativo, idTcSubramo,
				idToCobertura, numeroInciso, numeroSubInciso,
				idToSeccion);
		if (detalleContratoFacultativoAnterior != null) {
			//Copiar las participaciones al contrato actual (detalleContratoFacultativo)
			copiarParticipacionesFacultativoCobertura(detalleContratoFacultativoActual, 
					detalleContratoFacultativoAnterior);
		}
		
		return detalleContratoFacultativoActual;
	}
	
	/**
	 * Este metodo copia todas las participacion de un DetalleContratoFacultativo de origen 
	 * hacia el DetalleContratoFacultativo destino. En el caso de que ya tenga participaciones
	 * primero las borra y despues agrega las mismas participaciones como las tiene el origen.
	 * @param destino
	 * @param origen
	 */
	private void copiarParticipacionesFacultativoCobertura(DetalleContratoFacultativoDTO detalleContratoFacultativoDestino,
			DetalleContratoFacultativoDTO detalleContratoFacultativoOrigen) {
		try {
			//Borrar las participaciones que pudieran existir en el contrato destino.
			borrarParticipacionesFacultativoCobertura(detalleContratoFacultativoDestino);
			//Agregar las participaciones como las tiene el origen.
			for (ParticipacionFacultativoDTO participacionFacultativoOrigen 
					: detalleContratoFacultativoOrigen.getParticipacionFacultativoDTOs()) {
				ParticipacionFacultativoDTO participacionFacultativoDestino = 
					new ParticipacionFacultativoDTO();
				BeanUtils.copyProperties(participacionFacultativoDestino, 
						participacionFacultativoOrigen);
				participacionFacultativoDestino.setParticipacionFacultativoDTO(null); //Poner el id en null.
				participacionFacultativoDestino.setDetalleContratoFacultativoDTO(
						detalleContratoFacultativoDestino);
				
				//Debemos agregar las participaciones de los corredores.
				participacionFacultativoDestino.setParticipacionCorredorFacultativoDTOs(
						new ArrayList<ParticipacionCorredorFacultativoDTO>());
				for (ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoOrigen 
						: participacionFacultativoOrigen.getParticipacionCorredorFacultativoDTOs()) {
					ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDestino =
						new ParticipacionCorredorFacultativoDTO();
					BeanUtils.copyProperties(participacionCorredorFacultativoDestino, 
							participacionCorredorFacultativoOrigen);
					participacionCorredorFacultativoDestino
							.setIdTdParticapacionCorredorFac(null); // Poner el
																	// id en
																	// null.
					participacionCorredorFacultativoDestino
							.setParticipacionFacultativoDTO(participacionFacultativoDestino);
					participacionFacultativoDestino
							.getParticipacionCorredorFacultativoDTOs().add(
									participacionCorredorFacultativoDestino);
				}
				
				//Por ultimo agregarlo a la collecion
				detalleContratoFacultativoDestino
					.getParticipacionFacultativoDTOs().add(
						participacionFacultativoDestino);
			} 
		}
		catch(IllegalAccessException ex) {
			throw new RuntimeException("No se pudieron copiar las propiedades del DetalleContratoFacultativo:", 
					ex);
		}
		catch(InvocationTargetException ex) {
			throw new RuntimeException("No se pudieron copiar las propiedades del DetalleContratoFacultativo:", 
					ex);
		}
	}
	

	private void borrarParticipacionesFacultativoCobertura(
			DetalleContratoFacultativoDTO detalleContratoFacultativoDTO) {
		for (ParticipacionFacultativoDTO participacionFacultativoDTO 
				: detalleContratoFacultativoDTO.getParticipacionFacultativoDTOs()) {
			participacionFacultativoFacadeRemote.delete(participacionFacultativoDTO);
		}
		detalleContratoFacultativoDTO.getParticipacionFacultativoDTOs().clear();
	}
	
	public BigDecimal getPrimaEndosoTotal(BigDecimal idTmContratoFacultativo) {
		String queryString = "select nvl(sum(nvl(primafacultadacobertura,0))- sum(nvl(montoprimanodevengada,0))+ sum(nvl(montoprimaadicional,0)),0) primaEndosoTotal " +
				"from MIDAS.tdcontratofacultativo where idtmcontratofacultativo = "+idTmContratoFacultativo;
		Query query = entityManager.createNativeQuery(queryString);
		Object result = query.getSingleResult();
		return Utilerias.obtenerBigDecimal(result);
	}

	//22/03/2011 Se deja de usar el m�todo debido a que toma mucho tiempo de respuesta al barrer los objetos DetalleContratoFacultatidoDTO.
//	private BigDecimal getPrimaEndosoTotal(ContratoFacultativoDTO contratoFacultativoDTO) {
//		BigDecimal total = BigDecimal.ZERO;
//		//TODO Cambiar este ciclo por un query nativo.
//		for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : contratoFacultativoDTO.getDetalleContratoFacultativoDTOs()) {
//			//Se dejo de contemplar la prima total cobertura, ya que en algunos casos guardaba datos inconsistentes
//			//En su lugar se utiliza la prima facultada y la no devengada.
//			//BigDecimal prima = detalleContratoFacultativoDTO.getPrimaTotalCobertura();
//			BigDecimal prima = detalleContratoFacultativoDTO.getPrimaFacultadaCobertura().subtract(detalleContratoFacultativoDTO.getMontoPrimaNoDevengada());
//			if(detalleContratoFacultativoDTO.getMontoPrimaAdicional() != null && detalleContratoFacultativoDTO.getMontoPrimaAdicional().compareTo(BigDecimal.ZERO) != 0){
//				prima = prima.add(detalleContratoFacultativoDTO.getMontoPrimaAdicional());
//			}
//			total = total.add(prima);			
//		}
//		return total;
//	}

	public BigDecimal getPrimaNegociadaTotal(BigDecimal idTmContratoFacultativo) {
		String queryString = "select nvl(sum(nvl(primafacultadacobertura,0))- sum(nvl(montoprimanodevengada,0)),0) primaNegociadaTotal " +
				"from MIDAS.tdcontratofacultativo where idtmcontratofacultativo = "+idTmContratoFacultativo;
		Query query = entityManager.createNativeQuery(queryString);
		Object result = query.getSingleResult();
		return  Utilerias.obtenerBigDecimal(result);
	}

	@SuppressWarnings("unchecked")
	public BigDecimal getComisionTotal(BigDecimal idTmContratoFacultativo) {
		BigDecimal porcentajeFacultativo = BigDecimal.ZERO;
		String queryString = "select model.disPrimaPorcentajeFacultativo from LineaSoporteReaseguroDTO model where model." +
			"contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo";
		Query query = entityManager.createQuery(queryString);
		query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		query.setParameter("idTmContratoFacultativo", idTmContratoFacultativo);
		List resultadoList = query.getResultList();
		if(resultadoList != null && !resultadoList.isEmpty())
			porcentajeFacultativo = Utilerias.obtenerBigDecimal(resultadoList.get(0));//.getDisPrimaPorcentajeFacultativo();
		
		queryString = "select nvl(sum((prima-primaND)*porccom/100*porcpart/100 *"+porcentajeFacultativo+"/100),0) montoComision from (" +
				" select nvl(detcon.primafacultadacobertura,0) prima,nvl(detcon.montoprimanodevengada,0) primaND,(part.porcentajeparticipacion) porcPart,part.comision porcCom " +
				" from MIDAS.tdparticipacionfacultativo part, MIDAS.tdcontratofacultativo detcon " +
				" where detcon.idtdcontratofacultativo = part.idtdcontratofacultativo and " +
				" detcon.idtmcontratofacultativo = "+idTmContratoFacultativo+")";
		Query query2=entityManager.createNativeQuery(queryString);
		Object resultado = query2.getSingleResult();
		return Utilerias.obtenerBigDecimal(resultado);
//		return  getComisionTotal(contratoFacultativoDTO,idTmContratoFacultativo);
	}
	
//	@SuppressWarnings("unchecked")
//	private BigDecimal getComisionTotal(ContratoFacultativoDTO contratoFacultativoDTO,BigDecimal idTmContratoFacultativo) {
//		BigDecimal total = BigDecimal.ZERO;
//		BigDecimal porcentajeFacultativo = BigDecimal.ZERO;
//		BigDecimal cien = new BigDecimal(100d);
//		String queryString = "select model.disPrimaPorcentajeFacultativo from LineaSoporteReaseguroDTO model where model." +
//			"contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo";
//		Query query = entityManager.createQuery(queryString);
//		query = entityManager.createQuery(queryString);
//		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
//		query.setParameter("idTmContratoFacultativo", idTmContratoFacultativo);
//		List resultadoList = query.getResultList();
//		List<LineaSoporteReaseguroDTO> listaLineasSoporte = query.getResultList();
//		if(resultadoList != null && !resultadoList.isEmpty())
//			porcentajeFacultativo = Utilerias.obtenerBigDecimal(resultadoList.get(0));//.getDisPrimaPorcentajeFacultativo();
//		
//		queryString = "select sum((prima-primaND)*porccom/100*porcpart/100 *"+porcentajeFacultativo+"/100) montoComision from (" +
//				" select nvl(detcon.primafacultadacobertura,0) prima,nvl(detcon.montoprimanodevengada,0) primaND,(part.porcentajeparticipacion) porcPart,part.comision porcCom " +
//				" from tdparticipacionfacultativo part,tdcontratofacultativo detcon " +
//				" where detcon.idtdcontratofacultativo = part.idtdcontratofacultativo and " +
//				" detcon.idtmcontratofacultativo = "+idTmContratoFacultativo+")";
//		Query query2=entityManager.createNativeQuery(queryString);
//		Object resultado = query2.getSingleResult();
//		return Utilerias.obtenerBigDecimal(resultado);
		
//		for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : contratoFacultativoDTO.getDetalleContratoFacultativoDTOs()) {
//			LineaSoporteCoberturaDTO lineaCobertura = lineaSoporteCoberturaFacade.getLineaSoporteCoberturaDTOByDetalleContratoFacultativoDTO(detalleContratoFacultativoDTO);
//			BigDecimal primaNoDevengada = lineaCobertura.getMontoPrimaNoDevengada() != null ? lineaCobertura.getMontoPrimaNoDevengada() : BigDecimal.ZERO;
//			BigDecimal prima = detalleContratoFacultativoDTO.getPrimaFacultadaCobertura().subtract(primaNoDevengada);
//			queryString = "select model from ParticipacionFacultativoDTO model where model." +
//					"detalleContratoFacultativoDTO.contratoFacultativoDTO = :idTdContratoFacultativo";
//			query = entityManager.createQuery(queryString);
//			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
//			query.setParameter("idTdContratoFacultativo", detalleContratoFacultativoDTO.getContratoFacultativoDTO());
//			List<ParticipacionFacultativoDTO> listaParticipaciones = query.getResultList();
//			for(ParticipacionFacultativoDTO participacion : listaParticipaciones){
////				participacion.getComision().divide(cien).multiply(prima).multiply(participacion.getPorcentajeParticipacion().divide(cien)).multiply(porcentajeFacultativo.divide(cien))
//				total = total.add(participacion.getComision().divide(cien).multiply(prima).multiply(participacion.getPorcentajeParticipacion().divide(cien)).multiply(porcentajeFacultativo.divide(cien)));
//			}
//		}
//		return total;
//	}
	
//	@EJB
//	private LineaSoporteCoberturaFacadeRemote lineaSoporteCoberturaFacade;
	
	@SuppressWarnings("unchecked")
	public List<String> obtenerAjustadorNombrado(BigDecimal idToPoliza,short numeroEndoso){
		List<String> listaAjustadorNombrado = new ArrayList<String>();
		try{
			if(idToPoliza != null){
				final String queryString = "select distinct model.contratoFacultativoDTO.ajustadorNombrado from LineaSoporteReaseguroDTO model " +
						" where model.soporteReaseguroDTO.idToPoliza = :idToPoliza " +
						" and model.soporteReaseguroDTO.numeroEndoso = :numeroEndoso " +
						" and model.contratoFacultativoDTO.ajustadorNombrado is not null";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idToPoliza", idToPoliza);
				query.setParameter("numeroEndoso", new Integer(numeroEndoso));
				List<Object> listaResultado = query.getResultList();
				for(Object nombre : listaResultado){
					if(nombre != null){
						listaAjustadorNombrado.add((String)nombre);
					}
				}
			}
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Ocurri� un error al consultar los ajustadores nombrados de la poliza: "+idToPoliza+", endoso: "+numeroEndoso, Level.SEVERE, e);
		}
		return listaAjustadorNombrado;
	}

//	private BigDecimal getPrimaNegociadaTotal(ContratoFacultativoDTO contratoFacultativoDTO) {
//		BigDecimal total = BigDecimal.ZERO;
//		for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : contratoFacultativoDTO.getDetalleContratoFacultativoDTOs()) {
//			//Ahora se le resta la prima no devengada a la prima facultada para obtener la prima c�dida.
//			//Antes la prima facultada ya exclu�a la prima no devengada.
//			//BigDecimal prima = detalleContratoFacultativoDTO.getPrimaFacultadaCobertura()
//			BigDecimal prima = detalleContratoFacultativoDTO.getPrimaFacultadaCobertura().subtract(detalleContratoFacultativoDTO.getMontoPrimaNoDevengada());
//			total = total.add(prima);
//		}
//		return total;
//	}

//	private static final long TIEMPO_INICIAR_DISTRIBUCION = 1000;
	
	private DetalleContratoFacultativoFacadeRemote detalleContratoFacultativoFacadeRemote;
	private ParticipacionFacultativoFacadeRemote participacionFacultativoFacadeRemote;
//	private EndosoFacadeRemote endosoFacadeRemote;
//	private SoporteReaseguroFacadeRemote soporteReaseguroFacadeRemote;
//	private TemporizadorDistribuirPolizaFacadeRemote temporizadorDistribuirPolizaFacadeRemote;

	
	@EJB
	public void setDetalleContratoFacultativoFacadeRemote(
			DetalleContratoFacultativoFacadeRemote detalleContratoFacultativoFacadeRemote) {
		this.detalleContratoFacultativoFacadeRemote = detalleContratoFacultativoFacadeRemote;
	}
	
	@EJB
	public void setParticipacionFacultativoFacadeRemote(
			ParticipacionFacultativoFacadeRemote participacionFacultativoFacadeRemote) {
		this.participacionFacultativoFacadeRemote = participacionFacultativoFacadeRemote;
	}
	
//	@EJB
//	public void setEndosoFacadeRemote(EndosoFacadeRemote endosoFacadeRemote) {
//		this.endosoFacadeRemote = endosoFacadeRemote;
//	}
//	
//	@EJB
//	public void setSoporteReaseguroFacadeRemote(
//			SoporteReaseguroFacadeRemote soporteReaseguroFacadeRemote) {
//		this.soporteReaseguroFacadeRemote = soporteReaseguroFacadeRemote;
//	}
	
//	@EJB
//	public void setTemporizadorDistribuirPolizaFacadeRemote(
//			TemporizadorDistribuirPolizaFacadeRemote temporizadorDistribuirPolizaFacadeRemote) {
//		this.temporizadorDistribuirPolizaFacadeRemote = temporizadorDistribuirPolizaFacadeRemote;
//	}
}