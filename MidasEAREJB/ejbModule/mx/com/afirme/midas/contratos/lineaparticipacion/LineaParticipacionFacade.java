package mx.com.afirme.midas.contratos.lineaparticipacion;
// default package

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity LineaParticipacionDTO.
 * 
 * @see .LineaParticipacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class LineaParticipacionFacade implements LineaParticipacionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved LineaParticipacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            LineaParticipacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(LineaParticipacionDTO entity) {
		LogDeMidasEJB3.log("saving LineaParticipacionDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent LineaParticipacionDTO entity.
	 * 
	 * @param entity
	 *            LineaParticipacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(LineaParticipacionDTO entity) {
		LogDeMidasEJB3.log("deleting LineaParticipacionDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(LineaParticipacionDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved LineaParticipacionDTO entity and return it or a
	 * copy of it to the sender. A copy of the LineaParticipacionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            LineaParticipacionDTO entity to update
	 * @return LineaParticipacionDTO the persisted LineaParticipacionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public LineaParticipacionDTO update(LineaParticipacionDTO entity) {
		LogDeMidasEJB3.log("updating LineaParticipacionDTO instance", Level.INFO, null);
		try {
			LineaParticipacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find  LineaParticipacionDTO by id property.
	 * 
	 * @return LineaParticipacionDTO found by id
	 */
	public LineaParticipacionDTO findById(LineaParticipacionId id) {
		LogDeMidasEJB3.log("finding LineaParticipacionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			LineaParticipacionDTO instance = entityManager.find(
					LineaParticipacionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all LineaParticipacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the LineaParticipacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<LineaParticipacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<LineaParticipacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding LineaParticipacionDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from LineaParticipacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all LineaParticipacionDTO entities.
	 * 
	 * @return List<LineaParticipacionDTO> all LineaParticipacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<LineaParticipacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all LineaParticipacionDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from LineaParticipacionDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}