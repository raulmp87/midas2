package mx.com.afirme.midas.contratos.contratocuotaparte;
// default package

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ContratoCuotaParteDTO.
 * @see .ContratoCuotaParteDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ContratoCuotaParteFacade  implements ContratoCuotaParteFacadeRemote {


    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ContratoCuotaParteDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ContratoCuotaParteDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public ContratoCuotaParteDTO save(ContratoCuotaParteDTO entity) {
    				LogDeMidasEJB3.log("saving ContratoCuotaParteDTO instance", Level.INFO, null);
	        try {
	        	entityManager.persist(entity);
            	LogDeMidasEJB3.log("save successful", Level.INFO, null);
            	return entity;
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ContratoCuotaParteDTO entity.
	  @param entity ContratoCuotaParteDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public void delete(ContratoCuotaParteDTO entity) {
    				LogDeMidasEJB3.log("deleting ContratoCuotaParteDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ContratoCuotaParteDTO.class, entity.getIdTmContratoCuotaParte());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ContratoCuotaParteDTO entity and return it or a copy of it to the sender. 
	 A copy of the ContratoCuotaParteDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ContratoCuotaParteDTO entity to update
	 @return ContratoCuotaParteDTO the persisted ContratoCuotaParteDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public ContratoCuotaParteDTO update(ContratoCuotaParteDTO entity) {
    				LogDeMidasEJB3.log("updating ContratoCuotaParteDTO instance", Level.INFO, null);
	        try {
            ContratoCuotaParteDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
 
    
    /**
	 * Find the ContratoCuotaParteDTO entity with a specific Id value.  
	 
	  @param id the id of the ContratoCuotaParteDTO to query
	  	  @return ContratoCuotaParteDTO found by query
	*/
    public ContratoCuotaParteDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding ContratoCuotaParteDTO instance with id: " + id, Level.INFO, null);
	        try {
            ContratoCuotaParteDTO instance = entityManager.find(ContratoCuotaParteDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ContratoCuotaParteDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ContratoCuotaParteDTO property to query
	  @param value the property value to match
	  	  @return List<ContratoCuotaParteDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ContratoCuotaParteDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ContratoCuotaParteDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ContratoCuotaParteDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ContratoCuotaParteDTO entities.
	  	  @return List<ContratoCuotaParteDTO> all ContratoCuotaParteDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ContratoCuotaParteDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ContratoCuotaParteDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ContratoCuotaParteDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ContratoCuotaParteDTO> listarFiltrado(ContratoCuotaParteDTO contratoCuotaParteDTO) {
		try {
			String queryString = "select model from ContratoCuotaParteDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (contratoCuotaParteDTO == null)
				return null;
						
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicial", contratoCuotaParteDTO.getFechaInicial());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaFinal", contratoCuotaParteDTO.getFechaFinal());					
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ContratoCuotaParteDTO> buscarPorFechaInicialFinal(Date fInicial, Date fFinal
		) {
					LogDeMidasEJB3.log("finding all ContratoCuotaParteDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ContratoCuotaParteDTO model";
			String sWhere = " where model.fechaInicial= :fechaInicial";
			sWhere += " and model.fechaFinal= :fechaFinal";
			
				Query query = entityManager.createQuery(queryString + sWhere);
				query.setParameter("fechaInicial", fInicial);
				query.setParameter("fechaFinal", fFinal);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	public boolean actualizaFechasLineasPorIdLineas(String ids,Date fInicial,Date fFinal){
					LogDeMidasEJB3.log("update all ContratoCuotaParteDTO instances", Level.INFO, null);
			try {
				if (ids==null || ids.trim().length()==0)return false;
			String queryString = "update ContratoCuotaParteDTO model set ";
			queryString += " model.fechaInicial= :fechaInicial";
			queryString += " ,model.fechaFinal= :fechaFinal";
			String sWhere = " where model.idTmContratoCuotaParte in (" + ids + ")";
			String consulta = queryString + sWhere;
			
			Query query = entityManager.createQuery(consulta);
			query.setParameter("fechaInicial", fInicial);
			query.setParameter("fechaFinal", fFinal);
			query.executeUpdate();
			return true;
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("update all failed", Level.SEVERE, re);
				throw re;
		}

	}

	public ContratoCuotaParteDTO cancelarContratoCuotaParte(ContratoCuotaParteDTO contratoCuotaParteDTO){
		contratoCuotaParteDTO = this.findById(contratoCuotaParteDTO.getIdTmContratoCuotaParte());
		contratoCuotaParteDTO.setEstatus(new BigDecimal("2")); //TODO DEFINIR EL VALOR QUE SE DEBE ASIGNAR PARA EL CANCELADO
		
		return contratoCuotaParteDTO;
	}
	
	@SuppressWarnings("unchecked")
	public ContratoCuotaParteDTO obtenerContratoCuotaParteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO){
		ContratoCuotaParteDTO contratoCPAnterior = null;
		String logInfo;
		if(estadoCuentaDTO != null && estadoCuentaDTO.getIdEstadoCuenta() != null && estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			String queryString = "";
			Calendar fechaInicio = Calendar.getInstance();
			Calendar fechaFin = Calendar.getInstance();
			fechaInicio.set(estadoCuentaDTO.getEjercicio()-1,//anio anterior
					Calendar.APRIL,//abril
					1,0,0,0);//d�a primero, hora 0, minuto 0 y segundo 0
			fechaFin.set(estadoCuentaDTO.getEjercicio(),//mismo anio
					Calendar.MARCH,//marzo
					31,23,59,59);//d�a 31, hora 23, minuto 59 y segundo 59
			//Ajuste a las fechas para validar con rango cerrado
			fechaInicio.add(Calendar.DATE, -1);
			fechaFin.add(Calendar.DATE, 1);
			Query query;
			//Si tiene corredor, se busca el corredor como participante del contrato, posteriormente, el reasegurador como participante del corredor
			if(estadoCuentaDTO.getCorredorDTO() != null && estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor() != null){
				queryString = "select model.participacion.contratoCuotaParte from ParticipacionCorredorDTO model, LineaDTO linea where " +
						"model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtccorredor and " +
						"model.participacion.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreasegurador and " +
						"model.participacion.contratoCuotaParte is not null and " +
						"model.participacion.contratoCuotaParte.formaPago = :formaPago and " +
						"model.participacion.contratoCuotaParte.idTcMoneda= :idTcMoneda and " +
						"linea.contratoCuotaParte.idTmContratoCuotaParte = model.participacion.contratoCuotaParte.idTmContratoCuotaParte and " +
						"linea.subRamo.idTcSubRamo = :idTcSubRamo and " +
						"linea.subRamo.ramoDTO.idTcRamo = :idTcRamo and " +
						"model.participacion.contratoCuotaParte.fechaInicial > :fechaInicial and " +
						"model.participacion.contratoCuotaParte.fechaFinal < :fechaFinal and " +
						"model.participacion.contratoCuotaParte.estatus = 1";
				query = entityManager.createQuery(queryString);
				query.setParameter("idtccorredor",estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
				query.setParameter("idtcreasegurador",estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor());
			}
			else{//Si no tiene corredor, s�lo se busca el reasegurador en la lista de participaciones
				queryString = "select model.contratoCuotaParte from ParticipacionDTO model, LineaDTO linea where " +
						"model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreasegurador and " +
						"model.contratoCuotaParte is not null and " +
						"model.contratoCuotaParte.formaPago = :formaPago and " +
						"model.contratoCuotaParte.idTcMoneda= :idTcMoneda and " +
						"linea.contratoCuotaParte.idTmContratoCuotaParte = model.contratoCuotaParte.idTmContratoCuotaParte and " +
						"linea.subRamo.idTcSubRamo = :idTcSubRamo and " +
						"linea.subRamo.ramoDTO.idTcRamo = :idTcRamo and " +
						"model.contratoCuotaParte.fechaInicial > :fechaInicial and " +
						"model.contratoCuotaParte.fechaFinal < :fechaFinal and " +
						"model.contratoCuotaParte.estatus = 1";
				query = entityManager.createQuery(queryString);
				query.setParameter("idtcreasegurador",estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
			}
			query.setParameter("formaPago",estadoCuentaDTO.getContratoCuotaParteDTO().getFormaPago());
			query.setParameter("idTcMoneda",estadoCuentaDTO.getIdMoneda());
			query.setParameter("idTcSubRamo",estadoCuentaDTO.getLineaDTO().getSubRamo().getIdTcSubRamo());
			query.setParameter("idTcRamo",estadoCuentaDTO.getLineaDTO().getSubRamo().getRamoDTO().getIdTcRamo());
			query.setParameter("fechaInicial",new java.sql.Date(fechaInicio.getTime().getTime()));
			query.setParameter("fechaFinal",new java.sql.Date(fechaFin.getTime().getTime()));
			
			logInfo = String.format("Buscando Contrato Cuota Parte Anterior (reasegurador: %s, corredor: %s, formaPago: %s, idMoneda: %s, idSubRamo: %s, idRamo: %s, fechaInicial: %s, fechaFinal: %s)",
					estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().toString(),
					(estadoCuentaDTO.getCorredorDTO() != null && estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor() != null ? estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor().toString() : "null"),
					estadoCuentaDTO.getContratoCuotaParteDTO().getFormaPago().toString(),
					(""+estadoCuentaDTO.getIdMoneda()),
					estadoCuentaDTO.getLineaDTO().getSubRamo().getIdTcSubRamo().toString(),
					estadoCuentaDTO.getLineaDTO().getSubRamo().getRamoDTO().getIdTcRamo().toString(),
					DateFormat.getDateInstance().format(fechaInicio.getTime()),
					DateFormat.getDateInstance().format(fechaFin.getTime()));
			LogDeMidasEJB3.log(logInfo, Level.INFO, null);
			List<ContratoCuotaParteDTO> listaContratosEncontrados = query.getResultList();
			if(listaContratosEncontrados != null && !listaContratosEncontrados.isEmpty()){
				if(listaContratosEncontrados.size() == 1){
					LogDeMidasEJB3.log(listaContratosEncontrados.size()+" Contrato(s) encontrado(s).", Level.INFO, null);
					contratoCPAnterior = listaContratosEncontrados.get(0);
				}else{
					LogDeMidasEJB3.log(listaContratosEncontrados.size()+" Contrato(s) encontrado(s). Se ignorar�n por ser consistente, s�lo debe existir uno o ninguno.", Level.INFO, null);
				}
			}
			else
				LogDeMidasEJB3.log("Ning�n Contrato encontrado.", Level.INFO, null);
		}
		return contratoCPAnterior;
	}
	
	@SuppressWarnings("unchecked")
	public List<ContratoCuotaParteDTO> obtenerListaContratoCuotaParteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO){
		List<ContratoCuotaParteDTO> listaContratosEncontrados = null;
		String logInfo;
		if(estadoCuentaDTO != null && estadoCuentaDTO.getIdEstadoCuenta() != null && estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			String queryString = "";
			Calendar fechaFin = Calendar.getInstance();
			fechaFin.set(estadoCuentaDTO.getEjercicio(),//mismo anio
					Calendar.MARCH,//marzo
					31,23,59,59);//d�a 31, hora 23, minuto 59 y segundo 59
			//Ajuste a las fechas para validar con rango cerrado
			fechaFin.add(Calendar.DATE, 1);
			Query query;
			//Si tiene corredor, se busca el corredor como participante del contrato, posteriormente, el reasegurador como participante del corredor
			if(estadoCuentaDTO.getCorredorDTO() != null && estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor() != null){
				queryString = "select model.participacion.contratoCuotaParte from ParticipacionCorredorDTO model, LineaDTO linea where " +
						"model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtccorredor and " +
						"model.participacion.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreasegurador and " +
						"model.participacion.contratoCuotaParte is not null and " +
						"model.participacion.contratoCuotaParte.formaPago = :formaPago and " +
						"model.participacion.contratoCuotaParte.idTcMoneda= :idTcMoneda and " +
						"linea.contratoCuotaParte.idTmContratoCuotaParte = model.participacion.contratoCuotaParte.idTmContratoCuotaParte and " +
						"linea.subRamo.idTcSubRamo = :idTcSubRamo and " +
						"linea.subRamo.ramoDTO.idTcRamo = :idTcRamo and " +
						"model.participacion.contratoCuotaParte.fechaFinal < :fechaFinal and " +
						"model.participacion.contratoCuotaParte.estatus = 1 order by model.participacion.contratoCuotaParte.fechaFinal";
				query = entityManager.createQuery(queryString);
				query.setParameter("idtccorredor",estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
				query.setParameter("idtcreasegurador",estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor());
			}
			else{//Si no tiene corredor, s�lo se busca el reasegurador en la lista de participaciones
				queryString = "select model.contratoCuotaParte from ParticipacionDTO model, LineaDTO linea where " +
						"model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreasegurador and " +
						"model.contratoCuotaParte is not null and " +
						"model.contratoCuotaParte.formaPago = :formaPago and " +
						"model.contratoCuotaParte.idTcMoneda= :idTcMoneda and " +
						"linea.contratoCuotaParte.idTmContratoCuotaParte = model.contratoCuotaParte.idTmContratoCuotaParte and " +
						"linea.subRamo.idTcSubRamo = :idTcSubRamo and " +
						"linea.subRamo.ramoDTO.idTcRamo = :idTcRamo and " +
						"model.contratoCuotaParte.fechaFinal < :fechaFinal and " +
						"model.contratoCuotaParte.estatus = 1 order by model.contratoCuotaParte.fechaFinal";
				query = entityManager.createQuery(queryString);
				query.setParameter("idtcreasegurador",estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
			}
			query.setParameter("formaPago",estadoCuentaDTO.getContratoCuotaParteDTO().getFormaPago());
			query.setParameter("idTcMoneda",estadoCuentaDTO.getIdMoneda());
			query.setParameter("idTcSubRamo",estadoCuentaDTO.getLineaDTO().getSubRamo().getIdTcSubRamo());
			query.setParameter("idTcRamo",estadoCuentaDTO.getLineaDTO().getSubRamo().getRamoDTO().getIdTcRamo());
			query.setParameter("fechaFinal",new java.sql.Date(fechaFin.getTime().getTime()));
			
			logInfo = String.format("Buscando Contratos Cuota Parte Anteriores (reasegurador: %s, corredor: %s, formaPago: %s, idMoneda: %s, idSubRamo: %s, idRamo: %s,  fechaFinal: %s)",
					estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().toString(),
					(estadoCuentaDTO.getCorredorDTO() != null && estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor() != null ? estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor().toString() : "null"),
					estadoCuentaDTO.getContratoCuotaParteDTO().getFormaPago().toString(),
					(""+estadoCuentaDTO.getIdMoneda()),
					estadoCuentaDTO.getLineaDTO().getSubRamo().getIdTcSubRamo().toString(),
					estadoCuentaDTO.getLineaDTO().getSubRamo().getRamoDTO().getIdTcRamo().toString(),
					DateFormat.getDateInstance().format(fechaFin.getTime()));
			LogDeMidasEJB3.log(logInfo, Level.INFO, null);
			listaContratosEncontrados = query.getResultList();
			if(listaContratosEncontrados != null){
				LogDeMidasEJB3.log(listaContratosEncontrados.size()+" Contrato(s) encontrado(s).", Level.INFO, null);
			}else{
				LogDeMidasEJB3.log("Ning�n Contrato encontrado.", Level.INFO, null);
			}
		}
		return listaContratosEncontrados;
	}
}