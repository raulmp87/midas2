package mx.com.afirme.midas.contratos.ingreso;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroServiciosRemote;
import mx.com.afirme.midas.contratos.movimiento.TipoMovimientoDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity IngresoReaseguroDTO.
 * @see .IngresoReaseguroDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class IngresoReaseguroFacade  implements IngresoReaseguroFacadeRemote {

	@EJB
	MovimientoReaseguroServiciosRemote movimientoReaseguroServicios;


    @PersistenceContext 
    private EntityManager entityManager;
    @Resource
    private SessionContext context;
	
		/**
	 Perform an initial save of a previously unsaved IngresoReaseguroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity IngresoReaseguroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(IngresoReaseguroDTO entity) {
    				LogDeMidasEJB3.log("saving IngresoReaseguroDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent IngresoReaseguroDTO entity.
	  @param entity IngresoReaseguroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(IngresoReaseguroDTO entity) {
    				LogDeMidasEJB3.log("deleting IngresoReaseguroDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(IngresoReaseguroDTO.class, entity.getIdIngresoReaseguro());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved IngresoReaseguroDTO entity and return it or a copy of it to the sender. 
	 A copy of the IngresoReaseguroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity IngresoReaseguroDTO entity to update
	 @return IngresoReaseguroDTO the persisted IngresoReaseguroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public IngresoReaseguroDTO update(IngresoReaseguroDTO entity) {
    				LogDeMidasEJB3.log("updating IngresoReaseguroDTO instance", Level.INFO, null);
	        try {
            IngresoReaseguroDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public IngresoReaseguroDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding IngresoReaseguroDTO instance with id: " + id, Level.INFO, null);
	        try {
            IngresoReaseguroDTO instance = entityManager.find(IngresoReaseguroDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

    /**
     * Find all IngresoReaseguroDTO entities with a specific property value.  

	  @param propertyName the name of the IngresoReaseguroDTO property to query
	  @param value the property value to match
	  	  @return List<IngresoReaseguroDTO> found by query
     */
    @SuppressWarnings("unchecked")
    public List<IngresoReaseguroDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding IngresoReaseguroDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from IngresoReaseguroDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all IngresoReaseguroDTO entities.
	  	  @return List<IngresoReaseguroDTO> all IngresoReaseguroDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<IngresoReaseguroDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all IngresoReaseguroDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from IngresoReaseguroDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<IngresoReaseguroDTO> obtenerIngresosPendientes() {
		List<IngresoReaseguroDTO> ingresoReaseguroDTOList;
		String queryString = "SELECT model FROM IngresoReaseguroDTO model WHERE model.estatus = 0 " +
				"ORDER BY model.fechaIngreso";
		
		Query query = entityManager.createQuery(queryString);
		
		ingresoReaseguroDTOList = query.getResultList();
		
		return ingresoReaseguroDTOList;
	}
	
	public boolean mismoReaseguradorCorredor(List<EstadoCuentaDecoradoDTO> listaEstadosCuenta){
		final int TIPO_REASEGURO_RETENCION = 4;
		BigDecimal idTcReasegurador = null;
		BigDecimal idTcCorredor = null;
		int idMoneda = 0;
		for(EstadoCuentaDecoradoDTO estadoCuenta : listaEstadosCuenta){
			if (estadoCuenta.getTipoReaseguro() == TIPO_REASEGURO_RETENCION)
				return false; //El estado de cuenta no puede ser de retenci�n.
			
			if(idTcCorredor == null && idTcReasegurador == null && idMoneda==0){
				//Se trata del primer estado de cuenta, se inicializan los par�metros a validar.
				idTcCorredor = estadoCuenta.getCorredorDTO() != null ? 
							   estadoCuenta.getCorredorDTO().getIdtcreaseguradorcorredor() : new BigDecimal("0");						
				idTcReasegurador = estadoCuenta.getReaseguradorCorredorDTO() != null ? 
								   estadoCuenta.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor() : new BigDecimal("0");;
				idMoneda = estadoCuenta.getIdMoneda();
			}else{						
				BigDecimal idTcCorredorActual = estadoCuenta.getCorredorDTO() != null ? 
						   						estadoCuenta.getCorredorDTO().getIdtcreaseguradorcorredor() : new BigDecimal("0");;							
				if(idTcCorredorActual.compareTo(idTcCorredor)!=0)
					return false; //Existen estados de cuenta con diferente corredor
				
				BigDecimal idTcReaseguradorActual = estadoCuenta.getReaseguradorCorredorDTO() != null ? 
						estadoCuenta.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor() : new BigDecimal("0");
				if(idTcCorredor.compareTo(new BigDecimal("0"))==0 && 
				   idTcReaseguradorActual.compareTo(idTcReasegurador)!=0)
					return false; //Existen estados de cuenta con diferente reasegurador (sin pertenecer a un corredor)
				
				int idMonedaActual = estadoCuenta.getIdMoneda();
				if(idMonedaActual!=idMoneda)
					return false; //Existen estados de cuenta con diferente moneda
			}			
		}		
		return true; //Los estados de cuenta seleccionados son v�lidos
	}
	
	/*
	public boolean mismoReaseguradorCorredor(List<EstadoCuentaDecoradoDTO> listaEstadosCuenta){
		//Validaci�n de la siguiente REGLA DE NEGOCIO: Los estados de cuenta a relacionar en un ingreso/egreso deben o bien ser el mismo reasegurador no asignado a corredor o
		// o todos los reaseguradores (iguales o no) en los estados de cuenta relacionados al mismo corredor 
		boolean resultado = true;
		for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO : listaEstadosCuenta) {
			for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO2 : listaEstadosCuenta) {
				if (!resultado)
					break;
				if (estadoCuentaDecoradoDTO.getNombreReasegurador().equalsIgnoreCase("Retenci�n")){
					resultado = false; //Si el flujo llega aqu� entonces el estado de cuenta es de Retenci�n, lo que incumple la regla de negocio
					break;
				}
				if (!estadoCuentaDecoradoDTO.getIdEstadoCuenta().equals(estadoCuentaDecoradoDTO2.getIdEstadoCuenta())){
					
					if(estadoCuentaDecoradoDTO.getIdMoneda() != estadoCuentaDecoradoDTO2.getIdMoneda())
						return false; //Existen estados de cuenta de diferente moneda, lo cual rompe con la regla de negocio.
					
					if (!estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().equals(
							estadoCuentaDecoradoDTO2.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor())){
						if (estadoCuentaDecoradoDTO.getEstadoCuentaDTO().getCorredorDTO() == null || estadoCuentaDecoradoDTO2.getEstadoCuentaDTO().getCorredorDTO() == null ||
								!estadoCuentaDecoradoDTO.getEstadoCuentaDTO().getCorredorDTO().getIdtcreaseguradorcorredor().equals(estadoCuentaDecoradoDTO2.getEstadoCuentaDTO().getCorredorDTO().getIdtcreaseguradorcorredor())){
							resultado = false; //Si el flujo llega aqu� entonces los Edos de Cta tienen reaseguradores diferentes y no pertenecen al mismo corredor
							break;
						}
					}else{ //Si el flujo llega aqu� entonces ambos Edos de Cta tienen el mismo Reasegurador asignado
						if (estadoCuentaDecoradoDTO.getEstadoCuentaDTO().getCorredorDTO() != null){ 
							if (estadoCuentaDecoradoDTO2.getEstadoCuentaDTO().getCorredorDTO() != null){
								if(!estadoCuentaDecoradoDTO.getEstadoCuentaDTO().getCorredorDTO().getIdtcreaseguradorcorredor().equals(
										estadoCuentaDecoradoDTO2.getEstadoCuentaDTO().getCorredorDTO().getIdtcreaseguradorcorredor())){
									resultado = false; //Si el flujo llega aqu� entonces ambos estados de cuenta tiene reaseguradores iguales con corredores distintos, lo que incumple la regla de negocio
									break;
								}
							}else{
								resultado = false; //Si el flujo llega aqu� entonces el 1er estado de cuenta tiene al reasegurador con corredor y el 2o lo tiene sin corredor, lo que incumple la regla de negocio
								break;
							}
						}else{
							if (estadoCuentaDecoradoDTO2.getEstadoCuentaDTO().getCorredorDTO() != null){ //Si se cumple la condici�n entonces el 1er Edo de Cta no tiene corredor y el 2o si tiene, lo que no cumple con la regla de negocio. Si no se cumple esta condici�n entonces los reaseguradores (que son el mismo en ambos Edos. de Cta.) no tienen corredor, cumpliendo la regla de negocio
								resultado = false;
								break;
							}
						}
					}
				}
			}
		}
		
		return resultado;
	}
	*/

//	public boolean mismoReaseguradorCorredorTemp(List<EstadoCuentaDecoradoDTO> listaEstadosCuenta){
//		boolean esCorrecto = true;
//		boolean hayRetencion = false;
//		boolean hayReasegurador = false;
//		boolean existe = false;
//		String nombreReasegurador = listaEstadosCuenta.get(0).getNombreReasegurador();
//		for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO : listaEstadosCuenta) {
//			if (estadoCuentaDecoradoDTO.getNombreReasegurador().equalsIgnoreCase("Retenci�n")){
//				hayRetencion = true;
//			}else{
//				hayReasegurador = true;
//				if (!estadoCuentaDecoradoDTO.getNombreReasegurador().equalsIgnoreCase(nombreReasegurador)){
//					existe = existePaticipacionConCorredor(estadoCuentaDecoradoDTO, listaEstadosCuenta );
//					
//					if (!existe){
//						esCorrecto = false;
//						break;
//					}
//				}
//			}
//			if (hayRetencion && hayReasegurador){
//				esCorrecto = false;
//				break;
//			}
//		}
//		
//		return esCorrecto;
//	}
//	
//	private boolean existePaticipacionConCorredor(EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO, List<EstadoCuentaDecoradoDTO> listaEstadosCuenta) {
//		boolean todosParticipan = true;
//		boolean ambosParticipan = false;
//		int tipoReaseguro;
//		ContratoCuotaParteDTO contratoCP;
//		List<ParticipacionDTO> participaciones = new ArrayList<ParticipacionDTO>();
//		ContratoPrimerExcedenteDTO contratoPE;
//		ContratoFacultativoDTO contratoFacultativo;
//		List<DetalleContratoFacultativoDTO> listaDetalle;
//		List<ParticipacionFacultativoDTO> participacionesFacultativo = new ArrayList<ParticipacionFacultativoDTO>();
//		
//		
//		for (EstadoCuentaDecoradoDTO estadoCuentaEnTurno : listaEstadosCuenta) {
//			if (estadoCuentaEnTurno.getNombreReasegurador().equalsIgnoreCase("Retenci�n")){
//				continue;
//			}
//			
//			tipoReaseguro = estadoCuentaDecoradoDTO.getTipoReaseguro();
//			
//			if (tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
//				contratoCP = estadoCuentaDecoradoDTO.getContratoCuotaParteDTO();
//				participaciones = this.obtenerParticipacionesDTOByContratoCuotaParteDTO(contratoCP);
//			}
//			if (tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
//				contratoPE = estadoCuentaDecoradoDTO.getContratoPrimerExcedenteDTO();
//				participaciones = this.obtenerParticipacionesDTOByContratoPrimerExcedenteDTO(contratoPE);
//			}
//			if (tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
//				contratoFacultativo = estadoCuentaDecoradoDTO.getContratoFacultativoDTO();
//				listaDetalle = obtenerListaDetalleContratoFacultativoDTOByContratoFacultativoDTO(contratoFacultativo);
//				
//				participaciones = new ArrayList<ParticipacionDTO>();
//				
//				for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : listaDetalle){
//					if (detalleContratoFacultativoDTO. getIdTcSubramo().equals(estadoCuentaDecoradoDTO.getSubRamoDTO().getIdTcSubRamo())){
//						 List<ParticipacionFacultativoDTO> participacionesDetalle = this.obtenerListaParticipacionFacultativoDTOByDetalleContratoFacultativoDTO(detalleContratoFacultativoDTO);
//						 participacionesFacultativo.addAll(participacionesDetalle);
//					}
//				}
//			}
//		
//			ambosParticipan = false;
//			/**
//			 * In the case when participations came from a Contrato Cuota Parte or a Contrato Primer Excedente (ParticipacionDTO)
//			 */
//			for (ParticipacionDTO participacionDTO : participaciones) {
//				if (participacionDTO.getTipo().equals(new BigDecimal("0"))){
//					List<ParticipacionCorredorDTO> participacionesCorredor = this.obtenerListaParticipacionCorredorDTOByParticipacionDTO(participacionDTO);
//					boolean unoEnCorredor = false;
//					boolean dosEnCorredor = false;
//					for (ParticipacionCorredorDTO participacionCorredorDTO : participacionesCorredor) {
//						if (estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().equals(participacionCorredorDTO.getReaseguradorCorredor().getIdtcreaseguradorcorredor())){
//							unoEnCorredor = true;
//						}
//						if (estadoCuentaEnTurno.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().equals(participacionCorredorDTO.getReaseguradorCorredor().getIdtcreaseguradorcorredor())){
//							dosEnCorredor = true;
//						}
//					}
//					if (unoEnCorredor && dosEnCorredor){
//						ambosParticipan = true;
//						break;
//					}
//				}
//				
//			}
//			
//			if (!ambosParticipan){
//				todosParticipan = false;
//				break;
//			}
//			
//			/**
//			 * In the case when participations came from a Contrato Facultativo (ParticipacionFacultativoDTO)
//			 */
//			for (ParticipacionFacultativoDTO participacionFacultativoDTO : participacionesFacultativo) {
//				if (participacionFacultativoDTO.getTipo().equals(new BigDecimal("0"))){
//					List<ParticipacionCorredorFacultativoDTO> participacionesCorredorFacultativo = this.obtenerListaParticipacionCorredorFacultativoDTOByParticipacionCorredorDTO(participacionFacultativoDTO);
//					boolean unoEnCorredor = false;
//					boolean dosEnCorredor = false;
//					for (ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO : participacionesCorredorFacultativo) {
//						if (estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().equals(participacionCorredorFacultativoDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor())){
//							unoEnCorredor = true;
//						}
//						if (estadoCuentaEnTurno.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().equals(participacionCorredorFacultativoDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor())){
//							dosEnCorredor = true;
//						}
//					}
//					if (unoEnCorredor && dosEnCorredor){
//						ambosParticipan = true;
//						break;
//					}
//				}
//				
//			}
//			
//			if (!ambosParticipan){
//				todosParticipan = false;
//				break;
//			}
//			
//		}
//		return todosParticipan;
//	}
//	
//	@SuppressWarnings("unchecked")
//	private List<ParticipacionDTO> obtenerParticipacionesDTOByContratoCuotaParteDTO(ContratoCuotaParteDTO contratoCuotaParteDTO){
//		List<ParticipacionDTO> participacionDTOList = new ArrayList<ParticipacionDTO>();
//		String queryString = "SELECT model FROM ParticipacionDTO model WHERE model.contratoCuotaParte.idTmContratoCuotaParte = :idTmContratoCuotaParte";
//		Query query = entityManager.createQuery(queryString);
//		query.setParameter("idTmContratoCuotaParte", contratoCuotaParteDTO.getIdTmContratoCuotaParte());
//		participacionDTOList = query.getResultList();
//		
//		return participacionDTOList;
//	}
//	
//	@SuppressWarnings("unchecked")
//	private List<ParticipacionDTO> obtenerParticipacionesDTOByContratoPrimerExcedenteDTO(ContratoPrimerExcedenteDTO contratoPrimerExcedente){
//		List<ParticipacionDTO> participacionDTOList = new ArrayList<ParticipacionDTO>();
//		String queryString = "SELECT model FROM ParticipacionDTO model WHERE model.contratoPrimerExcedente.idTmContratoPrimerExcedente = :idTmContratoPrimerExcedente";
//		Query query = entityManager.createQuery(queryString);
//		query.setParameter("idTmContratoPrimerExcedente", contratoPrimerExcedente.getIdTmContratoPrimerExcedente());
//		participacionDTOList = query.getResultList();
//		
//		return participacionDTOList;
//	}
//	
//	@SuppressWarnings("unchecked")
//	private List<ParticipacionCorredorDTO> obtenerListaParticipacionCorredorDTOByParticipacionDTO(ParticipacionDTO participacionDTO){
//		List<ParticipacionCorredorDTO> participacionCorredorDTOList = new ArrayList<ParticipacionCorredorDTO>();
//		String queryString = "SELECT model FROM ParticipacionCorredorDTO model WHERE model.participacion.idTdParticipacion = :idTdParticipacion";
//		Query query = entityManager.createQuery(queryString);
//		query.setParameter("idTdParticipacion", participacionDTO.getIdTdParticipacion());
//		participacionCorredorDTOList = query.getResultList();
//		
//		return participacionCorredorDTOList;
//	}
//	
//	@SuppressWarnings("unchecked")
//	private List<ParticipacionFacultativoDTO> obtenerListaParticipacionFacultativoDTOByDetalleContratoFacultativoDTO(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO){
//		List<ParticipacionFacultativoDTO> participacionFacultativoDTOList = new ArrayList<ParticipacionFacultativoDTO>();
//		String queryString = "SELECT model FROM ParticipacionFacultativoDTO model WHERE model.detalleContratoFacultativoDTO.contratoFacultativoDTO = :contratoFacultativoDTO";
//		Query query = entityManager.createQuery(queryString);
//		query.setParameter("contratoFacultativoDTO", detalleContratoFacultativoDTO.getContratoFacultativoDTO());
//		participacionFacultativoDTOList = query.getResultList();
//		
//		return participacionFacultativoDTOList;
//	}
//	
//	private List<ParticipacionCorredorFacultativoDTO> obtenerListaParticipacionCorredorFacultativoDTOByParticipacionCorredorDTO(ParticipacionFacultativoDTO participacionFacultativoDTO){
//		List<ParticipacionCorredorFacultativoDTO> participacionCorredorFacultativoDTOList = new ArrayList<ParticipacionCorredorFacultativoDTO>();
//		String queryString = "SELECT model FROM ParticipacionCorredorFacultativoDTO model WHERE model.participacionFacultativoDTO.participacionFacultativoDTO = :participacionFacultativoDTO";
//		Query query = entityManager.createQuery(queryString);
//		query.setParameter("participacionFacultativoDTO", participacionFacultativoDTO.getParticipacionFacultativoDTO());
//		return participacionCorredorFacultativoDTOList;
//	}
//	
//	
//	
//	@SuppressWarnings("unchecked")
//	private List<DetalleContratoFacultativoDTO> obtenerListaDetalleContratoFacultativoDTOByContratoFacultativoDTO(ContratoFacultativoDTO contratoFacultativoDTO){
//		List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOList = new ArrayList<DetalleContratoFacultativoDTO>();
//		String queryString = "SELECT model FROM DetalleContratoFacultativoDTO model WHERE model.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo";
//		Query query = entityManager.createQuery(queryString);
//		query.setParameter("idTmContratoFacultativo", contratoFacultativoDTO.getIdTmContratoFacultativo());
//		detalleContratoFacultativoDTOList = query.getResultList();
//		
//		return detalleContratoFacultativoDTOList;
//	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void relacionarIngreso(IngresoReaseguroDTO ingresoReaseguro, List<EstadoCuentaDecoradoDTO> lista){
		try {
			IngresoEstadoCuentaDTO ingresoEstadoCuentaDTO;
			for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO : lista) {
				ingresoEstadoCuentaDTO = new IngresoEstadoCuentaDTO();
				ingresoEstadoCuentaDTO.setIngresoReaseguro(ingresoReaseguro);
				ingresoEstadoCuentaDTO.setMonto(estadoCuentaDecoradoDTO.getMontoIngreso());
				
				IngresoEstadoCuentaId ingresoEstadoCuentaId = new IngresoEstadoCuentaId();
				ingresoEstadoCuentaId. setIdEstadoCuenta(estadoCuentaDecoradoDTO.getIdEstadoCuenta());
			    ingresoEstadoCuentaId. setIdIngresoReaseguro(ingresoReaseguro.getIdIngresoReaseguro());
			
			    ingresoEstadoCuentaDTO.setId(ingresoEstadoCuentaId);
				entityManager.persist(ingresoEstadoCuentaDTO);
				
				registrarMovimiento(ingresoReaseguro, estadoCuentaDecoradoDTO);
			}
			
			ingresoReaseguro.setEstatus(1); // Estatus: Relacionado
			entityManager.merge(ingresoReaseguro);
		}catch(RuntimeException e){
			context.setRollbackOnly();
			LogDeMidasEJB3.log("relacionarIngreso failed", Level.SEVERE, e);
			throw e;
		}
	}
	
	private void registrarMovimiento(IngresoReaseguroDTO ingresoReaseguro, EstadoCuentaDecoradoDTO estadoCuenta){
		MovimientoReaseguroDTO movimiento = new MovimientoReaseguroDTO();
		if (estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			movimiento.setContratoCuotaParteDTO(estadoCuenta.getContratoCuotaParteDTO());
		}
		
		if (estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
			movimiento.setContratoPrimerExcedenteDTO(estadoCuenta.getContratoPrimerExcedenteDTO());
		}
		
		if (estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
			movimiento.setContratoFacultativoDTO(estadoCuenta.getContratoFacultativoDTO());
			movimiento.setIdPoliza(estadoCuenta.getIdPoliza());	
			movimiento.setNumeroEndoso(estadoCuenta.getNumeroEndoso());
		}
			
			movimiento.setLineaDTO(estadoCuenta.getLineaDTO());
			movimiento.setEjercicio(estadoCuenta.getEjercicio());
			movimiento.setFechaRegistro(ingresoReaseguro.getFechaIngreso());
			movimiento.setIdMoneda(estadoCuenta.getIdMoneda());
			movimiento.setReaseguradorCorredor(estadoCuenta.getReaseguradorCorredorDTO());
			movimiento.setSubRamo(estadoCuenta.getSubRamoDTO());
			movimiento.setTipoMovimiento(TipoMovimientoDTO.COBRO);
			movimiento.setTipoReaseguro(estadoCuenta.getTipoReaseguro());
			movimiento.setComodin("INGRESO-"+ingresoReaseguro.getIdIngresoReaseguro().toPlainString());
			movimiento.setAcumulado(1);
			movimiento.setCantidad(estadoCuenta.getMontoIngreso());
			movimiento.setConceptoMovimiento(ingresoReaseguro.getConceptoIngresoReaseguro().getIdConcepto());
			movimiento.setNumeroEndoso(0); //TODO Eliminar esta linea una vez que la tabla TOMOVIMIENTOREASEGURO cambie el NumeroEndoso a Nullable
			
			movimientoReaseguroServicios.registrarMovimiento(movimiento, estadoCuenta.getEstadoCuentaDTO());
		
	}
	
	
}