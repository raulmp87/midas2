package mx.com.afirme.midas.contratos.ingreso;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless
public class ConceptoIngresoReaseguroFacade implements ConceptoIngresoReaseguroFacadeRemote{
	
	@PersistenceContext
	 private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<ConceptoIngresoReaseguroDTO> findAll() {
		LogDeMidasEJB3.log("finding all ConceptoIngresoReaseguroDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from ConceptoIngresoReaseguroDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ConceptoIngresoReaseguroDTO findById(Byte id) {
		LogDeMidasEJB3.log("finding ConceptoIngresoReaseguroDTO instance with id: " + id, Level.INFO, null);
        try {
        	ConceptoIngresoReaseguroDTO instance = entityManager.find(ConceptoIngresoReaseguroDTO.class, id);
        	return instance;
	    } catch (RuntimeException re) {
	    	LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	        throw re;
	    }
	}
	

}
