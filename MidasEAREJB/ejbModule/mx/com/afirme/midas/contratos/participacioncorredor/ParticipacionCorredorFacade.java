package mx.com.afirme.midas.contratos.participacioncorredor;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ParticipacionCorredorDTO.
 * @see .ParticipacionCorredorDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ParticipacionCorredorFacade  implements ParticipacionCorredorFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ParticipacionCorredorDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ParticipacionCorredorDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ParticipacionCorredorDTO entity) {
    				LogDeMidasEJB3.log("saving ParticipacionCorredorDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            entityManager.flush();
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ParticipacionCorredorDTO entity.
	  @param entity ParticipacionCorredorDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ParticipacionCorredorDTO entity) {
    				LogDeMidasEJB3.log("deleting ParticipacionCorredorDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ParticipacionCorredorDTO.class, entity.getIdTdParticipacionCorredor());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ParticipacionCorredorDTO entity and return it or a copy of it to the sender. 
	 A copy of the ParticipacionCorredorDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ParticipacionCorredorDTO entity to update
	 @return ParticipacionCorredorDTO the persisted ParticipacionCorredorDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ParticipacionCorredorDTO update(ParticipacionCorredorDTO entity) {
    				LogDeMidasEJB3.log("updating ParticipacionCorredorDTO instance", Level.INFO, null);
	        try {
	        	ParticipacionCorredorDTO result = entityManager.merge(entity);
	        	entityManager.flush();
            	LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
	/**
	 * Find  ParticipacionDTO by idParticipacionDTO property.
	 * 
	 * @return ParticipacionDTO found by id
	 */
    public ParticipacionCorredorDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding ParticipacionCorredorDTO instance with id: " + id, Level.INFO, null);
	        try {
            ParticipacionCorredorDTO instance = entityManager.find(ParticipacionCorredorDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ParticipacionCorredorDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ParticipacionCorredorDTO property to query
	  @param value the property value to match
	  	  @return List<ParticipacionCorredorDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ParticipacionCorredorDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ParticipacionCorredorDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ParticipacionCorredorDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ParticipacionCorredorDTO entities.
	  	  @return List<ParticipacionCorredorDTO> all ParticipacionCorredorDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipacionCorredorDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ParticipacionCorredorDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ParticipacionCorredorDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find filtered ParticipacionCorredorDTO entities.
	  	  @return List<ParticipacionCorredorDTO> filtered ParticipacionCorredorDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipacionCorredorDTO> listarFiltrado(ParticipacionCorredorDTO participacionCorredorDTO){
		try {
			String queryString = "select model from ParticipacionCorredorDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (participacionCorredorDTO == null)
				return null;
						
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "participacion.idTdParticipacion", participacionCorredorDTO.getParticipacion().getIdTdParticipacion(), "idTdParticipacion");						
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "reaseguradorCorredor.idtcreaseguradorcorredor", participacionCorredorDTO.getReaseguradorCorredor().getId(), "idtcreaseguradorcorredor");
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}		
	}
	
}