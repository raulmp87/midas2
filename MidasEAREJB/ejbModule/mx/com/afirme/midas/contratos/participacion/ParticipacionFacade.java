package mx.com.afirme.midas.contratos.participacion;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ParticipacionDTO.
 * @see .ParticipacionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ParticipacionFacade  implements ParticipacionFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ParticipacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ParticipacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public ParticipacionDTO save(ParticipacionDTO entity) {
    				LogDeMidasEJB3.log("saving ParticipacionDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);            
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
            return entity;
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ParticipacionDTO entity.
	  @param entity ParticipacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ParticipacionDTO entity) {
    				LogDeMidasEJB3.log("deleting ParticipacionDTO instance", Level.INFO, null);
	        try {
	        	
	        	String queryString = "DELETE FROM LineaParticipacionDTO model WHERE " +
	        	                     "model.id.idTdParticipacion = :idTdParticipacion";
	    		Query query = entityManager.createQuery(queryString);
	    		query.setParameter("idTdParticipacion", entity.getIdTdParticipacion());
	    		query.executeUpdate();
	    		
	        	entity = entityManager.getReference(ParticipacionDTO.class, entity.getIdTdParticipacion());
	            entityManager.remove(entity);
	            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);    		
        	
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ParticipacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the ParticipacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ParticipacionDTO entity to update
	 @return ParticipacionDTO the persisted ParticipacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ParticipacionDTO update(ParticipacionDTO entity) {
    				LogDeMidasEJB3.log("updating ParticipacionDTO instance", Level.INFO, null);
	        try {
            ParticipacionDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
	/**
	 * Find  ParticipacionDTO by id property.
	 * 
	 * @return ParticipacionDTO found by id
	 */
    public ParticipacionDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding ParticipacionDTO instance with id: " + id, Level.INFO, null);
	        try {
            ParticipacionDTO instance = entityManager.find(ParticipacionDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ParticipacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ParticipacionDTO property to query
	  @param value the property value to match
	  	  @return List<ParticipacionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ParticipacionDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ParticipacionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ParticipacionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ParticipacionDTO entities.
	  	  @return List<ParticipacionDTO> all ParticipacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipacionDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ParticipacionDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ParticipacionDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find filtered ParticipacionDTO entities.
	  	  @return List<ParticipacionDTO> filtered ParticipacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipacionDTO> listarFiltrado(ParticipacionDTO participacionDTO){
		try {
			entityManager.flush();
			String queryString = "select model from ParticipacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (participacionDTO == null)
				return null;
			
			if (participacionDTO.getContratoCuotaParte() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "contratoCuotaParte.idTmContratoCuotaParte", participacionDTO.getContratoCuotaParte().getIdTmContratoCuotaParte(), "idTmContratoCuotaParte");
			if (participacionDTO.getContratoPrimerExcedente() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "contratoPrimerExcedente.idTmContratoPrimerExcedente", participacionDTO.getContratoPrimerExcedente().getIdTmContratoPrimerExcedente(), "idTmContratoPrimerExcedente");
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}		
	}
	
	/**
	 * Find filtered ParticipacionDTO entities.
	  	  @return List<ParticipacionDTO> filtered ParticipacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipacionDTO> buscarPorLineaCCP(ContratoCuotaParteDTO contratoCuotaParteDTO,LineaDTO lineaDTO){
		LogDeMidasEJB3.log("finding all ParticipacionDTO instances", Level.INFO, null);
		try {
			String queryString = "select model from ParticipacionDTO model, LineaParticipacionDTO lp";
			queryString += " where model.contratoCuotaParte = :contratoCuotaParte";
			queryString += " and lp.linea=:linea and model.idTdParticipacion = lp.participacion.idTdParticipacion";
					Query query = entityManager.createQuery(queryString);
					query.setParameter("contratoCuotaParte", contratoCuotaParteDTO);
					query.setParameter("linea", lineaDTO);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find filtered ParticipacionDTO entities.
	  	  @return List<ParticipacionDTO> filtered ParticipacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipacionDTO> buscarPorLineaCPE(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO,LineaDTO lineaDTO){
		LogDeMidasEJB3.log("finding all ParticipacionDTO instances", Level.INFO, null);
		try {
		String queryString = "select model from ParticipacionDTO model, LineaParticipacionDTO lp";
			queryString += " where model.contratoPrimerExcedente = :contratoPrimerExcedente";
			queryString += " and lp.linea=:linea and model.idTdParticipacion = lp.participacion.idTdParticipacion";
					Query query = entityManager.createQuery(queryString);
					query.setParameter("contratoPrimerExcedente", contratoPrimerExcedenteDTO);
					query.setParameter("linea", lineaDTO);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}