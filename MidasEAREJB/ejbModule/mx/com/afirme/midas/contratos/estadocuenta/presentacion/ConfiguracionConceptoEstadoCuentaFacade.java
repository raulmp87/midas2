package mx.com.afirme.midas.contratos.estadocuenta.presentacion;
// default package

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity ConfiguracionConceptoEstadoCuentaDTO.
 * @see .ConfiguracionConceptoEstadoCuentaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class ConfiguracionConceptoEstadoCuentaFacade  implements ConfiguracionConceptoEstadoCuentaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ConfiguracionConceptoEstadoCuentaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ConfiguracionConceptoEstadoCuentaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ConfiguracionConceptoEstadoCuentaDTO entity) {
    				LogUtil.log("saving ConfiguracionConceptoEstadoCuentaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ConfiguracionConceptoEstadoCuentaDTO entity.
	  @param entity ConfiguracionConceptoEstadoCuentaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ConfiguracionConceptoEstadoCuentaDTO entity) {
    				LogUtil.log("deleting ConfiguracionConceptoEstadoCuentaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ConfiguracionConceptoEstadoCuentaDTO.class, entity.getIdTcConfiguracionConcepto());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ConfiguracionConceptoEstadoCuentaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ConfiguracionConceptoEstadoCuentaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ConfiguracionConceptoEstadoCuentaDTO entity to update
	 @return ConfiguracionConceptoEstadoCuentaDTO the persisted ConfiguracionConceptoEstadoCuentaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ConfiguracionConceptoEstadoCuentaDTO update(ConfiguracionConceptoEstadoCuentaDTO entity) {
    				LogUtil.log("updating ConfiguracionConceptoEstadoCuentaDTO instance", Level.INFO, null);
	        try {
            ConfiguracionConceptoEstadoCuentaDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ConfiguracionConceptoEstadoCuentaDTO findById( Integer id) {
    				LogUtil.log("finding ConfiguracionConceptoEstadoCuentaDTO instance with id: " + id, Level.INFO, null);
	        try {
            ConfiguracionConceptoEstadoCuentaDTO instance = entityManager.find(ConfiguracionConceptoEstadoCuentaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ConfiguracionConceptoEstadoCuentaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ConfiguracionConceptoEstadoCuentaDTO property to query
	  @param value the property value to match
	  	  @return List<ConfiguracionConceptoEstadoCuentaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ConfiguracionConceptoEstadoCuentaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding ConfiguracionConceptoEstadoCuentaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ConfiguracionConceptoEstadoCuentaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ConfiguracionConceptoEstadoCuentaDTO entities.
	  	  @return List<ConfiguracionConceptoEstadoCuentaDTO> all ConfiguracionConceptoEstadoCuentaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConfiguracionConceptoEstadoCuentaDTO> findAll(
		) {
					LogUtil.log("finding all ConfiguracionConceptoEstadoCuentaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ConfiguracionConceptoEstadoCuentaDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}