package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import java.math.BigDecimal;
import java.util.Date;

class EstructuraFiltradoMovimientos {

	private BigDecimal idToPoliza;
	private Integer numeroEndoso;
	private BigDecimal idToReporteSiniestro;
	private BigDecimal idTcReasegurador;
	private BigDecimal idTcCorredor;
	private BigDecimal idMoneda;
	private BigDecimal idToSeccion;
	private Integer numeroInciso;
	private BigDecimal[] idTcSubRamos;
	private BigDecimal idConceptoMovDetalle;
	private BigDecimal[] idConceptosMovtosDetalle;
	private boolean incluirTodasLasPolizas = false;
	private boolean incluirTodosLosSiniestros = false;
	private boolean incluirSoloFacultativo = false;
	private boolean incluirSoloAutomatico = false;
	private boolean incluirFiltradoHasta = false;
	private Integer mesHasta;
	private Integer anioHasta;
	private boolean incluirFiltradoFecha = false;
	private Date fechaMovimientos;
	private BigDecimal tipoCambio;
	private boolean validarTipoCambioNulo = false;
	
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public BigDecimal getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	public BigDecimal getIdTcReasegurador() {
		return idTcReasegurador;
	}
	public void setIdTcReasegurador(BigDecimal idTcReasegurador) {
		this.idTcReasegurador = idTcReasegurador;
	}
	public BigDecimal getIdTcCorredor() {
		return idTcCorredor;
	}
	public void setIdTcCorredor(BigDecimal idTcCorredor) {
		this.idTcCorredor = idTcCorredor;
	}
	public BigDecimal getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}
	public BigDecimal[] getIdTcSubRamos() {
		return idTcSubRamos;
	}
	public void setIdTcSubRamos(BigDecimal[] idTcSubRamos) {
		this.idTcSubRamos = idTcSubRamos;
	}
	public BigDecimal getIdConceptoMovDetalle() {
		return idConceptoMovDetalle;
	}
	public void setIdConceptoMovDetalle(BigDecimal idConceptoMovDetalle) {
		this.idConceptoMovDetalle = idConceptoMovDetalle;
	}
	public boolean isIncluirTodasLasPolizas() {
		return incluirTodasLasPolizas;
	}
	public void setIncluirTodasLasPolizas(boolean incluirTodasLasPolizas) {
		this.incluirTodasLasPolizas = incluirTodasLasPolizas;
	}
	public boolean isIncluirTodosLosSiniestros() {
		return incluirTodosLosSiniestros;
	}
	public void setIncluirTodosLosSiniestros(boolean incluirTodosLosSiniestros) {
		this.incluirTodosLosSiniestros = incluirTodosLosSiniestros;
	}
	public boolean isIncluirSoloFacultativo() {
		return incluirSoloFacultativo;
	}
	public void setIncluirSoloFacultativo(boolean incluirSoloFacultativo) {
		this.incluirSoloFacultativo = incluirSoloFacultativo;
	}
	public boolean isIncluirFiltradoHasta() {
		if(!(getMesHasta() != null && getAnioHasta() != null && 
				getMesHasta()>0 && getMesHasta()<=12 && getAnioHasta()>1900))
			incluirFiltradoHasta = false;
		else incluirFiltradoHasta = true;
		return incluirFiltradoHasta;
	}
	public void setIncluirFiltradoHasta(boolean incluirFiltradoHasta) {
		this.incluirFiltradoHasta = incluirFiltradoHasta;
	}
	public Integer getMesHasta() {
		return mesHasta;
	}
	public void setMesHasta(Integer mesHasta) {
		this.mesHasta = mesHasta;
	}
	public Integer getAnioHasta() {
		return anioHasta;
	}
	public void setAnioHasta(Integer anioHasta) {
		this.anioHasta = anioHasta;
	}
	public boolean isIncluirFiltradoFecha() {
		return incluirFiltradoFecha;
	}
	public void setIncluirFiltradoFecha(boolean incluirFiltradoFecha) {
		this.incluirFiltradoFecha = incluirFiltradoFecha;
	}
	public Date getFechaMovimientos() {
		return fechaMovimientos;
	}
	public void setFechaMovimientos(Date fechaMovimientos) {
		this.fechaMovimientos = fechaMovimientos;
	}
	public BigDecimal[] getIdConceptosMovtosDetalle() {
		return idConceptosMovtosDetalle;
	}
	public void setIdConceptosMovtosDetalle(BigDecimal[] idConceptosMovtosDetalle) {
		this.idConceptosMovtosDetalle = idConceptosMovtosDetalle;
	}
	public boolean isIncluirSoloAutomatico() {
		return incluirSoloAutomatico;
	}
	public void setIncluirSoloAutomatico(boolean incluirSoloAutomatico) {
		this.incluirSoloAutomatico = incluirSoloAutomatico;
	}
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public boolean isValidarTipoCambioNulo() {
		return validarTipoCambioNulo;
	}
	public void setValidarTipoCambioNulo(boolean validarTipoCambioNulo) {
		this.validarTipoCambioNulo = validarTipoCambioNulo;
	}
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}
	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
}
