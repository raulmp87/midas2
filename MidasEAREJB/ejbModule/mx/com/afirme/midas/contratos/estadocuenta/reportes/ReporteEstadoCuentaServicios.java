package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo.ParticipacionCorredorFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.SaldoConceptoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.UtileriasCalculoEstadoCuenta;
import mx.com.afirme.midas.contratos.estadocuenta.presentacion.AgrupacionEstadoCuentaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.presentacion.ConfiguracionConceptoEstadoCuentaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.presentacion.PresentacionEstadoCuentaServiciosRemote;
import mx.com.afirme.midas.contratos.linea.CalculoSuscripcionesDTO;
import mx.com.afirme.midas.contratos.linea.SuscripcionDTO;
import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDetalleDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ReporteEstadoCuentaServicios implements ReporteEstadoCuentaServiciosRemote{
	/*
	 * Id's de conceptos a nivel de detalle para c�lculo de saldos.
	 */
	private static final BigDecimal CONCEPTO_PRIMA_CEDIDA = new BigDecimal(ConceptoMovimientoDetalleDTO.PRIMA_CEDIDA);
	private static final BigDecimal CONCEPTO_PRIMA_NO_DEVENGADA = new BigDecimal(ConceptoMovimientoDetalleDTO.PRIMA_NO_DEVENGADA); 
	private static final BigDecimal CONCEPTO_COMISION_PRIMA_CEDIDA = new BigDecimal(ConceptoMovimientoDetalleDTO.COMISION_PRIMA_CEDIDA);
	private static final BigDecimal CONCEPTO_COMISION_PRIMA_NO_DEVENGADA = new BigDecimal(ConceptoMovimientoDetalleDTO.COMISION_PRIMA_NO_DEVENGADA);
	private static final BigDecimal CONCEPTO_INGRESO_PRIMA_NO_DEVENGADA = new BigDecimal(ConceptoMovimientoDetalleDTO.INGRESO_PRIMA_NO_DEVENGADA);
	private static final BigDecimal CONCEPTO_PRIMA_NO_DEVENGADA_INGRESO = new BigDecimal(ConceptoMovimientoDetalleDTO.PRIMA_NO_DEVENGADA_INGRESO);
	private static final BigDecimal CONCEPTO_CANCELAR_INGRESO_PRIMA_NO_DEVENGADA = new BigDecimal(ConceptoMovimientoDetalleDTO.CANCELACION_INGRESO_PRIMA_NO_DEVENGADA);
	private static final BigDecimal CONCEPTO_INGRESO_BONO_POR_NO_SINIESTRO = new BigDecimal(ConceptoMovimientoDetalleDTO.INGRESO_BONO_POR_NO_SINIESTRO);
	private static final BigDecimal CONCEPTO_CANCELAR_INGRESO_BONO_POR_NO_SINIESTRO = new BigDecimal(ConceptoMovimientoDetalleDTO.CANCELACION_INGRESO_BONO_POR_NO_SINIESTRO);
	private static final BigDecimal CONCEPTO_CARGOS_REMESAS_SALDOS = new BigDecimal(ConceptoMovimientoDetalleDTO.CARGOS_REMESAS_SALDOS);
	private static final BigDecimal CONCEPTO_PAGO_PRIMA = new BigDecimal(ConceptoMovimientoDetalleDTO.PAGOS_PRIMA);
	private static final BigDecimal CONCEPTO_CANCELAR_PAGO_PRIMA = new BigDecimal(ConceptoMovimientoDetalleDTO.CANCELAR_PAGO_PRIMA);
	private static final BigDecimal CONCEPTO_RETENCION_IMPUESTOS = new BigDecimal(ConceptoMovimientoDetalleDTO.RETENCION_IMPUESTOS);
	private static final BigDecimal CONCEPTO_CANCELAR_RETENCION_IMPUESTOS = new BigDecimal(ConceptoMovimientoDetalleDTO.CANCELACION_RETENCION_IMPUESTOS);
	private static final BigDecimal CONCEPTO_INDEMNIZACION = new BigDecimal(ConceptoMovimientoDetalleDTO.INDEMNIZACION);
	private static final BigDecimal CONCEPTO_CANCELACION_INDEMNIZACION = new BigDecimal(ConceptoMovimientoDetalleDTO.CANCELACION_INDEMNIZACION);
	private static final BigDecimal CONCEPTO_GASTOS_DE_AJUSTE = new BigDecimal(ConceptoMovimientoDetalleDTO.GASTOS_DE_AJUSTE);
	private static final BigDecimal CONCEPTO_CANCELACION_GASTOS_DE_AJUSTE = new BigDecimal(ConceptoMovimientoDetalleDTO.CANCELACION_GASTOS_DE_AJUSTE);
	private static final BigDecimal CONCEPTO_DEDUCIBLE = new BigDecimal(ConceptoMovimientoDetalleDTO.DEDUCIBLE);
	private static final BigDecimal CONCEPTO_CANCELACION_DEDUCIBLE = new BigDecimal(ConceptoMovimientoDetalleDTO.CANCELACION_DEDUCIBLE);
	private static final BigDecimal CONCEPTO_COASEGURO = new BigDecimal(ConceptoMovimientoDetalleDTO.COASEGURO);
	private static final BigDecimal CONCEPTO_CANCELACION_COASEGURO = new BigDecimal(ConceptoMovimientoDetalleDTO.CANCELACION_COASEGURO);
	private static final BigDecimal CONCEPTO_SALVAMENTO = new BigDecimal(ConceptoMovimientoDetalleDTO.SALVAMENTO);
	private static final BigDecimal CONCEPTO_CANCELACION_SALVAMENTO = new BigDecimal(ConceptoMovimientoDetalleDTO.CANCELACION_SALVAMENTO);
	private static final BigDecimal CONCEPTO_RECUPERACION = new BigDecimal(ConceptoMovimientoDetalleDTO.RECUPERACION);
	private static final BigDecimal CONCEPTO_CANCELACION_RECUPERACION = new BigDecimal(ConceptoMovimientoDetalleDTO.CANCELACION_RECUPERACION);
	private static final BigDecimal CONCEPTO_INGRESO_POR_SINIESTRO = new BigDecimal(ConceptoMovimientoDetalleDTO.INGRESO_POR_SINIESTRO);
	private static final BigDecimal CONCEPTO_ABONOS_DE_REMESAS_SALDOS = new BigDecimal(ConceptoMovimientoDetalleDTO.ABONOS_DE_REMESAS_SALDOS);
	private static final BigDecimal CONCEPTO_CANCELACION_INGRESO_POR_SINIESTRO = new BigDecimal(ConceptoMovimientoDetalleDTO.CANCELACION_INGRESO_POR_SINIESTRO);
	
	@EJB
	private PresentacionEstadoCuentaServiciosRemote presentacionEstadoCuentaServicios;
	
	@EJB
	private CotizacionFacadeRemote cotizacionFacade;
	
	@EJB
	private EndosoFacadeRemote endosoFacade;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ReporteReaseguradorNegociosFacultativosDTO obtenerReporteMovimientosFacultativosTrimestre(BigDecimal idTcReasegurador,Integer ejercicio,Integer suscripcion,boolean excluirSaldoCorredor){
		SuscripcionDTO suscripcionDTO = UtileriasCalculoEstadoCuenta.obtenerSuscripcion(ejercicio, suscripcion);
		ReaseguradorCorredorDTO reaseguradorDTO = entityManager.find(ReaseguradorCorredorDTO.class, idTcReasegurador);
		
		ReporteReaseguradorNegociosFacultativosDTO resumenNegociosFacultativos = new ReporteReaseguradorNegociosFacultativosDTO();
		resumenNegociosFacultativos.setIdTcReasegurador(idTcReasegurador);
		resumenNegociosFacultativos.setNombreReasegurador(reaseguradorDTO.getNombre());
		resumenNegociosFacultativos.setSuscripcionDTO(suscripcionDTO);
		
		//1. Consulta de las cotizaciones facultadas en el periodo
		List<CotizacionDTO> listaCotizacionesFacultadas = cotizacionFacade.obtenerCotizacionesFacultadas(suscripcionDTO.getFechaInicial(),suscripcionDTO.getFechaFinal());
		
		if(listaCotizacionesFacultadas != null && !listaCotizacionesFacultadas.isEmpty()){
			List<ParticipacionFacultativoDTO> listaDetallesParticipacion = null;
			List<ParticipacionCorredorFacultativoDTO> listaDetallesParticipacionCorredor = null;
			
			Map<BigDecimal,List<ParticipacionFacultativoDTO>> mapaParticipacionesPorContrato = 
				new HashMap<BigDecimal, List<ParticipacionFacultativoDTO>>();
			Map<BigDecimal,List<ParticipacionCorredorFacultativoDTO>> mapaParticipacionesCorredorPorContrato = 
					new HashMap<BigDecimal, List<ParticipacionCorredorFacultativoDTO>>();
			List<BigDecimal> listaIdTmContratos = new ArrayList<BigDecimal>();
			Map<BigDecimal,BigDecimal> mapaPorcentajesFacultativosPorLineaReas= new HashMap<BigDecimal, BigDecimal>();
			Map<BigDecimal,SubRamoDTO> mapaSubRamos = new HashMap<BigDecimal,SubRamoDTO>();
			//Mapa para respaldar los datos del endoso, necesario para fechas de vigencia y tipo de endoso
			Map<BigDecimal,EndosoDTO> mapaEndosos= new HashMap<BigDecimal, EndosoDTO>();
			
			for(CotizacionDTO cotizacionDTO : listaCotizacionesFacultadas){
				//2. Consulta de los contratos facultativos en los que particip� el reasegurador
				listaDetallesParticipacion = obtenerParticipacionesFacultativas(idTcReasegurador, cotizacionDTO.getIdToCotizacion());
				
				//3. Consulta de los contratos facultativos en los que particip� el reasegurador bajo un corredor
				if(!excluirSaldoCorredor){
					listaDetallesParticipacionCorredor = obtenerParticipacionesFacultativasCorredor(idTcReasegurador, cotizacionDTO.getIdToCotizacion());
				}
				
				//4. Agrupaci�n de las participaciones por contrato facultativo
				agruparParticipacionesPorContratoFacultativo(listaIdTmContratos, listaDetallesParticipacion, mapaParticipacionesPorContrato, 
						excluirSaldoCorredor, listaDetallesParticipacionCorredor, mapaParticipacionesCorredorPorContrato);
					
				//5. Ya con las participaciones agrupadas, se procede a calcular el saldo
				for(BigDecimal idTmContratoFacultativo : listaIdTmContratos){
					//Por cada contrato facultativo en el que particip�, se agrega un registro al reporte
					ReporteSaldoPorNegocioFacultativoDTO reporteNegocioFacultativo = new ReporteSaldoPorNegocioFacultativoDTO();
					listaDetallesParticipacion = mapaParticipacionesPorContrato.get(idTmContratoFacultativo);
					
					ContratoFacultativoDTO contratoFacultativoEnCurso = calcularPrimasReasegurador(reporteNegocioFacultativo, idTmContratoFacultativo, listaDetallesParticipacion, 
							excluirSaldoCorredor, mapaParticipacionesCorredorPorContrato, mapaPorcentajesFacultativosPorLineaReas,mapaSubRamos);
					
					//Se iteraron las participaciones del reasegurador, se procede a generar el DTO
					if(contratoFacultativoEnCurso != null){
						reporteNegocioFacultativo.setIdMoneda(cotizacionDTO.getIdMoneda().intValue());
						reporteNegocioFacultativo.setDescripcionMoneda(Utilerias.obtenerDescripcionMoneda(cotizacionDTO.getIdMoneda().intValue()));
						
						if(contratoFacultativoEnCurso.getSlipDTO() == null || contratoFacultativoEnCurso.getSlipDTO().getIdTcSubRamo() == null){
							LogDeMidasEJB3.log("Se encontr� un contrato facultativo inconsistente. IdTmContratoFacultativo: "+contratoFacultativoEnCurso.getIdTmContratoFacultativo(), Level.WARNING, null);
							
							LineaSoporteReaseguroDTO lineaSoporteReaseguro = contratoFacultativoEnCurso.getLineaSoporteReaseguroDTOs().get(0);
							
							if(lineaSoporteReaseguro != null){
								SubRamoDTO subRamoTMP = lineaSoporteReaseguro.getLineaDTO().getSubRamo();
								reporteNegocioFacultativo.setIdTcSubRamo(subRamoTMP.getIdTcSubRamo());
								reporteNegocioFacultativo.setDescripcionSubRamo(subRamoTMP.getDescripcionSubRamo());
							}
						}
						else{
							SubRamoDTO subRamoTMP = obtenerSubRamoDTO(contratoFacultativoEnCurso.getSlipDTO().getIdTcSubRamo(), mapaSubRamos);
							reporteNegocioFacultativo.setIdTcSubRamo(subRamoTMP.getIdTcSubRamo());
							reporteNegocioFacultativo.setDescripcionSubRamo(subRamoTMP.getDescripcionSubRamo());
						}
						reporteNegocioFacultativo.setNombreAsegurado(cotizacionDTO.getNombreAsegurado());
						//Consultar el endoso
						EndosoDTO endoso = obtenerEndosoDTO(cotizacionDTO.getIdToCotizacion(), mapaEndosos);
						reporteNegocioFacultativo.setNumeroEndoso(endoso.getId().getNumeroEndoso().intValue());
						reporteNegocioFacultativo.setDescripcionTipoEndoso(Utilerias.calcularDescripcionTipoEndoso(endoso));
						
						PolizaDTO poliza = endoso.getPolizaDTO();
						reporteNegocioFacultativo.setNumeroPoliza(Utilerias.obtenerNumeroPoliza(poliza));
						reporteNegocioFacultativo.setIdToPoliza(poliza.getIdToPoliza());
						resumenNegociosFacultativos.getListaSaldosNegociosFacultativos().add(reporteNegocioFacultativo);
					}
				}//fin iteracion idTmContratos
			}//Fin iteraci�n cotizaciones
		}// fin validacion listaCotizacionesFacultadas != null
		
		return resumenNegociosFacultativos;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ReporteSaldoTrimestralDTO obtenerSaldosPorReasegurador(BigDecimal idTcReasegurador,Integer[] idTcTipoReaseguro,Integer ejercicio,Integer suscripcion,
			boolean incluirEjerciciosAnteriores,BigDecimal idTcMoneda,boolean excluirSaldoCorredor){
		
		ReporteSaldoTrimestralDTO reporteTrimestralReasegurador = new ReporteSaldoTrimestralDTO();
		
		reporteTrimestralReasegurador.setMontoTotalPrimaTrimestre(BigDecimal.ZERO);
		reporteTrimestralReasegurador.setMontoTotalComisionTrimestre(BigDecimal.ZERO);
		reporteTrimestralReasegurador.setMontoTotalSiniestroTrimestre(BigDecimal.ZERO);
		
		boolean parametrosValidos = true;
		String descripcionError = "Se recibieron par�metros inv�lidos: ";
		ReaseguradorCorredorDTO reaseguradorDTO = null;
		//validar datos recibidos.
		if(ejercicio == null || ejercicio < 2000){
			parametrosValidos = false;
			descripcionError += "ejercicio no v�lido: "+ejercicio;
		}
		else if(suscripcion == null || suscripcion < 1 || suscripcion >4){
			parametrosValidos = false;
			descripcionError += "suscripci�n no v�lida: "+suscripcion;
		}
		else if(idTcMoneda == null ){
			parametrosValidos = false;
			descripcionError += "idTcMoneda no v�lida: "+idTcMoneda;
		}
		else if(suscripcion == null || suscripcion < 1 || suscripcion >4){
			parametrosValidos = false;
			descripcionError += "suscripci�n no v�lida: "+suscripcion;
		}
		else if(idTcReasegurador == null ){
			parametrosValidos = false;
			descripcionError += "idTcReasegurador no v�lido: "+idTcReasegurador;
		}
		else if(idTcTipoReaseguro == null || idTcTipoReaseguro.length == 0){
			parametrosValidos = false;
			descripcionError += "idTcTipoReaseguro no v�lido: "+idTcTipoReaseguro;
		}
		
		reaseguradorDTO = entityManager.find(ReaseguradorCorredorDTO.class, idTcReasegurador);
		reporteTrimestralReasegurador.setNombreReasegurador(reaseguradorDTO.getNombre());
		
		if(parametrosValidos && reaseguradorDTO != null){
//			EstadoCuentaDecoradoDTO estadoCuentaDecoradoTMP = null;
			//1. Por cada moneda:
			List<ReporteSaldoPorSubRamoDTO> listaReporteSaldoSubRamoPorMoneda = null;
			List<ReporteSaldoPorSubRamoDTO> listaReporteSaldoSubRamoPendientes= new ArrayList<ReporteSaldoPorSubRamoDTO>();
			SuscripcionDTO suscripcionDTO = null;
			SuscripcionDTO suscripcionAnteriorDTO = null;

			//2. Consulta de todos los subramos en los que ha participado el reasegurador en los tipos de reaseguro recibidos en idTcRipoReaseguro
			listaReporteSaldoSubRamoPorMoneda = obtenerSubRamosAfectados(reaseguradorDTO, idTcTipoReaseguro, idTcMoneda, excluirSaldoCorredor);
			
			List<EstadoCuentaDecoradoDTO> listaEjerciciosEstadoCuentaSubRamo = null;
			//3. Por cada subramo y tipo de reaseguro, se consulta el estado de cuenta
			for(ReporteSaldoPorSubRamoDTO saldoSubRamo : listaReporteSaldoSubRamoPorMoneda){
				if(saldoSubRamo.getIdTcTipoReaseguro() != TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
					//Se debe obtener una lista de estados de cuenta, que contenga el saldo del subramo por cada contrato (anual) encontrado
					//Esta lista debe ser provista por el servicio de presentacionEstadoCuentaServicios
					
					listaEjerciciosEstadoCuentaSubRamo = presentacionEstadoCuentaServicios.obtenerEstadosCuentaSeparados(
							saldoSubRamo.getIdTcSubRamo(), saldoSubRamo.getIdTcReasegurador(), saldoSubRamo.getIdMoneda(),
							saldoSubRamo.getIdTcTipoReaseguro(), ejercicio, suscripcion, 
							AgrupacionEstadoCuentaDTO.CLAVE_FORMATO_AUTOMATICOS_COMPLETO, true,AgrupacionEstadoCuentaDTO.CLAVE_FORMATO_AUTOMATICOS_CONSULTA_PRIMA_COMISION_SINIESTRO);
					
					if(suscripcionDTO == null && !listaEjerciciosEstadoCuentaSubRamo.isEmpty()){
						suscripcionDTO = listaEjerciciosEstadoCuentaSubRamo.get(0).getSuscripcionDTO();
						if(suscripcionDTO != null && suscripcionDTO.getIdSuscripcion() > 1)
							suscripcionAnteriorDTO = (new CalculoSuscripcionesDTO()).getSuscripcion(
									listaEjerciciosEstadoCuentaSubRamo.get(0), suscripcionDTO.getIdSuscripcion()-1);
						
						reporteTrimestralReasegurador.setSuscripcionDTO(suscripcionDTO);
						reporteTrimestralReasegurador.setSuscripcionAnteriorDTO(suscripcionAnteriorDTO);
					}
				}
				else{
					if(suscripcionDTO != null){
						EstadoCuentaDecoradoDTO estadoCuentaFacultativo = presentacionEstadoCuentaServicios.obtenerEstadoCuentaCombinadoConSaldosCalculados(null, idTcReasegurador,
								idTcMoneda, saldoSubRamo.getIdTcSubRamo(), true,suscripcionDTO.obtenerMesesIncluidos() , false, null, null, false, 
								AgrupacionEstadoCuentaDTO.CLAVE_FORMATO_FACULTATIVO_CONSULTA_DESDE_AUTOMATICOS_PRIMA_COMISION_SINIESTRO);
						
						if(estadoCuentaFacultativo != null){
							listaEjerciciosEstadoCuentaSubRamo = new ArrayList<EstadoCuentaDecoradoDTO>();
							listaEjerciciosEstadoCuentaSubRamo.add(estadoCuentaFacultativo);
						}
					}
					else{
						listaReporteSaldoSubRamoPendientes.add(saldoSubRamo);
					}
				}
				//Cada registro en la lista corresponde a un registro en el reporte, ya que es un subramo - contrato - ejercicio del reasegurador, moneda y suscripcion
				if(listaEjerciciosEstadoCuentaSubRamo != null){
					for(EstadoCuentaDecoradoDTO estadoCuentaDecoradoSubRamoEjercicio : listaEjerciciosEstadoCuentaSubRamo){
						//Por cada estado de cuenta se genera un objeto SaldoTrimestralSubramo
						//4. A partir del estado de cuenta, se genera un objeto ReporteSaldoPorSubRamoDTO y se settean los saldos por primas, comisiones y siniestros
						try{
							ReporteSaldoPorSubRamoDTO saldoSubRamoTMP = saldoSubRamo.duplicate();
							if(saldoSubRamoTMP.getIdTcTipoReaseguro() != TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
								
								if(estadoCuentaDecoradoSubRamoEjercicio.getSaldoTecnicoAnterior() != null)
									saldoSubRamoTMP.setSaldoAnterior(estadoCuentaDecoradoSubRamoEjercicio.getSaldoTecnicoAnterior().getSaldoAcumulado());
								else
									saldoSubRamoTMP.setSaldoAnterior(BigDecimal.ZERO);
								
								saldoSubRamoTMP.setMontoPrimaMes1(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(0).get(0).getSaldo());
								saldoSubRamoTMP.setMontoComisionMes1(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(0).get(1).getSaldo());
								saldoSubRamoTMP.setMontoSiniestroMes1(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(0).get(2).getSaldo());
								
								saldoSubRamoTMP.setMontoPrimaMes2(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(1).get(0).getSaldo());
								saldoSubRamoTMP.setMontoComisionMes2(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(1).get(1).getSaldo());
								saldoSubRamoTMP.setMontoSiniestroMes2(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(1).get(2).getSaldo());
								
								saldoSubRamoTMP.setMontoPrimaMes3(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(2).get(0).getSaldo());
								saldoSubRamoTMP.setMontoComisionMes3(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(2).get(1).getSaldo());
								saldoSubRamoTMP.setMontoSiniestroMes3(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(2).get(2).getSaldo());
								
								saldoSubRamoTMP.setSaldoMes1(estadoCuentaDecoradoSubRamoEjercicio.getSaldosAcumuladosPorMes().get(0).getSaldoAcumulado());
								saldoSubRamoTMP.setSaldoMes2(estadoCuentaDecoradoSubRamoEjercicio.getSaldosAcumuladosPorMes().get(1).getSaldoAcumulado());
								saldoSubRamoTMP.setSaldoMes3(estadoCuentaDecoradoSubRamoEjercicio.getSaldosAcumuladosPorMes().get(2).getSaldoAcumulado());
								
								SaldoConceptoDTO saldoConceptoTMP = null;
								if(estadoCuentaDecoradoSubRamoEjercicio.getIdMoneda() == MonedaDTO.MONEDA_PESOS){
									saldoConceptoTMP = UtileriasCalculoEstadoCuenta.obtenerSaldoPorConcepto(
											ConfiguracionConceptoEstadoCuentaDTO.ID_CONFIG_CONCEPTO_REMESA_SALDO_AUTOMATICOS_PESOS, estadoCuentaDecoradoSubRamoEjercicio);
								}
								else if(estadoCuentaDecoradoSubRamoEjercicio.getIdMoneda() == MonedaDTO.MONEDA_DOLARES){
									saldoConceptoTMP = UtileriasCalculoEstadoCuenta.obtenerSaldoPorConcepto(
											ConfiguracionConceptoEstadoCuentaDTO.ID_CONFIG_CONCEPTO_REMESA_SALDO_AUTOMATICOS_DOLARES, estadoCuentaDecoradoSubRamoEjercicio);
								}
								
								if(saldoConceptoTMP != null){
									saldoSubRamoTMP.setMontoRemesasSaldos(saldoConceptoTMP.getSaldo());
								}
								else{
									saldoSubRamoTMP.setMontoRemesasSaldos(BigDecimal.ZERO);
								}
								
								reporteTrimestralReasegurador.setMontoTotalPrimaMes1(
										reporteTrimestralReasegurador.getMontoTotalPrimaMes1().add(saldoSubRamoTMP.getMontoPrimaMes1()));
								reporteTrimestralReasegurador.setMontoTotalPrimaMes2(
										reporteTrimestralReasegurador.getMontoTotalPrimaMes2().add(saldoSubRamoTMP.getMontoPrimaMes2()));
								reporteTrimestralReasegurador.setMontoTotalPrimaMes3(
										reporteTrimestralReasegurador.getMontoTotalPrimaMes3().add(saldoSubRamoTMP.getMontoPrimaMes3()));
								reporteTrimestralReasegurador.setMontoTotalComisionMes1(
										reporteTrimestralReasegurador.getMontoTotalComisionMes1().add(saldoSubRamoTMP.getMontoComisionMes1()));
								reporteTrimestralReasegurador.setMontoTotalComisionMes2(
										reporteTrimestralReasegurador.getMontoTotalComisionMes2().add(saldoSubRamoTMP.getMontoComisionMes2()));
								reporteTrimestralReasegurador.setMontoTotalComisionMes3(
										reporteTrimestralReasegurador.getMontoTotalComisionMes3().add(saldoSubRamoTMP.getMontoComisionMes3()));
								reporteTrimestralReasegurador.setMontoTotalSiniestroMes1(
										reporteTrimestralReasegurador.getMontoTotalSiniestroMes1().add(saldoSubRamoTMP.getMontoSiniestroMes1()));
								reporteTrimestralReasegurador.setMontoTotalSiniestroMes2(
										reporteTrimestralReasegurador.getMontoTotalSiniestroMes2().add(saldoSubRamoTMP.getMontoSiniestroMes2()));
								reporteTrimestralReasegurador.setMontoTotalSiniestroMes3(
										reporteTrimestralReasegurador.getMontoTotalSiniestroMes3().add(saldoSubRamoTMP.getMontoSiniestroMes3()));
								reporteTrimestralReasegurador.setMontoTotalPrimaTrimestre(
										reporteTrimestralReasegurador.getMontoTotalPrimaTrimestre().add(saldoSubRamoTMP.getMontoTotalPrima()));
								reporteTrimestralReasegurador.setMontoTotalComisionTrimestre(
										reporteTrimestralReasegurador.getMontoTotalComisionTrimestre().add(saldoSubRamoTMP.getMontoTotalComision()));
								reporteTrimestralReasegurador.setMontoTotalSiniestroTrimestre(
										reporteTrimestralReasegurador.getMontoTotalSiniestroTrimestre().add(saldoSubRamoTMP.getMontoTotalSiniestro()));
								reporteTrimestralReasegurador.setMontoTotalSaldoAnterior(
										reporteTrimestralReasegurador.getMontoTotalSaldoAnterior().add(saldoSubRamoTMP.getSaldoAnterior()));
								reporteTrimestralReasegurador.setMontoTotalSaldoTecnicoTrimestre(
										reporteTrimestralReasegurador.getMontoTotalSaldoTecnicoTrimestre().add(saldoSubRamoTMP.getSaldoTotalTrimestre()));
								reporteTrimestralReasegurador.setMontoTotalRemesaSaldo(
										reporteTrimestralReasegurador.getMontoTotalRemesaSaldo().add(saldoSubRamoTMP.getMontoRemesasSaldos()));
								reporteTrimestralReasegurador.setMontoTotalArrastreSaldo(
										reporteTrimestralReasegurador.getMontoTotalArrastreSaldo().add(saldoSubRamoTMP.getMontoArrastreSaldos()));
								
							}
							else{
								saldoSubRamoTMP.setMontoPrimaFacultativoMes1(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(0).get(0).getSaldo());
								saldoSubRamoTMP.setMontoComisionFacultativoMes1(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(0).get(1).getSaldo());
								saldoSubRamoTMP.setMontoSiniestroFacultativoMes1(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(0).get(2).getSaldo());
								
								saldoSubRamoTMP.setMontoPrimaFacultativoMes2(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(1).get(0).getSaldo());
								saldoSubRamoTMP.setMontoComisionFacultativoMes2(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(1).get(1).getSaldo());
								saldoSubRamoTMP.setMontoSiniestroFacultativoMes2(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(1).get(2).getSaldo());
								
								saldoSubRamoTMP.setMontoPrimaFacultativoMes3(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(2).get(0).getSaldo());
								saldoSubRamoTMP.setMontoComisionFacultativoMes3(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(2).get(1).getSaldo());
								saldoSubRamoTMP.setMontoSiniestroFacultativoMes3(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(2).get(2).getSaldo());
								
								saldoSubRamoTMP.setSaldoFacultativoMes1(estadoCuentaDecoradoSubRamoEjercicio.getSaldosAcumuladosPorMes().get(0).getSaldoAcumulado());
								saldoSubRamoTMP.setSaldoFacultativoMes2(estadoCuentaDecoradoSubRamoEjercicio.getSaldosAcumuladosPorMes().get(1).getSaldoAcumulado());
								saldoSubRamoTMP.setSaldoFacultativoMes3(estadoCuentaDecoradoSubRamoEjercicio.getSaldosAcumuladosPorMes().get(2).getSaldoAcumulado());
								
								reporteTrimestralReasegurador.setMontoTotalPrimaFacultativoMes1(
										reporteTrimestralReasegurador.getMontoTotalPrimaFacultativoMes1().add(saldoSubRamo.getMontoPrimaFacultativoMes1()));
								reporteTrimestralReasegurador.setMontoTotalPrimaFacultativoMes2(
										reporteTrimestralReasegurador.getMontoTotalPrimaFacultativoMes2().add(saldoSubRamo.getMontoPrimaFacultativoMes2()));
								reporteTrimestralReasegurador.setMontoTotalPrimaFacultativoMes3(
										reporteTrimestralReasegurador.getMontoTotalPrimaFacultativoMes3().add(saldoSubRamo.getMontoPrimaFacultativoMes3()));
								reporteTrimestralReasegurador.setMontoTotalComisionFacultativoMes1(
										reporteTrimestralReasegurador.getMontoTotalComisionFacultativoMes1().add(saldoSubRamo.getMontoComisionFacultativoMes1()));
								reporteTrimestralReasegurador.setMontoTotalComisionFacultativoMes2(
										reporteTrimestralReasegurador.getMontoTotalComisionFacultativoMes2().add(saldoSubRamo.getMontoComisionFacultativoMes2()));
								reporteTrimestralReasegurador.setMontoTotalComisionFacultativoMes3(
										reporteTrimestralReasegurador.getMontoTotalComisionFacultativoMes3().add(saldoSubRamo.getMontoComisionFacultativoMes3()));
								reporteTrimestralReasegurador.setMontoTotalSiniestroFacultativoMes1(
										reporteTrimestralReasegurador.getMontoTotalSiniestroFacultativoMes1().add(saldoSubRamoTMP.getMontoSiniestroFacultativoMes1()));
								reporteTrimestralReasegurador.setMontoTotalSiniestroFacultativoMes2(
										reporteTrimestralReasegurador.getMontoTotalSiniestroFacultativoMes2().add(saldoSubRamoTMP.getMontoSiniestroFacultativoMes2()));
								reporteTrimestralReasegurador.setMontoTotalSiniestroFacultativoMes3(
										reporteTrimestralReasegurador.getMontoTotalSiniestroFacultativoMes3().add(saldoSubRamoTMP.getMontoSiniestroFacultativoMes3()));
								reporteTrimestralReasegurador.setMontoTotalPrimaFacultativoTrimestre(
										reporteTrimestralReasegurador.getMontoTotalPrimaFacultativoTrimestre().add(saldoSubRamoTMP.getMontoTotalPrimaFacultativo()));
								reporteTrimestralReasegurador.setMontoTotalComisionFacultativoTrimestre(
										reporteTrimestralReasegurador.getMontoTotalComisionFacultativoTrimestre().add(saldoSubRamoTMP.getMontoTotalComisionFacultativo()));
								reporteTrimestralReasegurador.setMontoTotalSiniestroFacultativoTrimestre(
										reporteTrimestralReasegurador.getMontoTotalSiniestroFacultativoTrimestre().add(saldoSubRamoTMP.getMontoTotalSiniestroFacultativo()));
								
								reporteTrimestralReasegurador.setMontoTotalSaldoTecnicoFacultativoTrimestre(
										reporteTrimestralReasegurador.getMontoTotalSaldoTecnicoFacultativoTrimestre().add(saldoSubRamoTMP.getSaldoTotalTrimestreFacultativo()));
							}
							
							try{
								saldoSubRamoTMP.setEjercicio(estadoCuentaDecoradoSubRamoEjercicio.getEjercicio());
							}
							catch(Exception e){
								LogDeMidasEJB3.log("Error al consultar el ejercicio del estado de cuenta: "+estadoCuentaDecoradoSubRamoEjercicio, Level.WARNING, e);
								if(estadoCuentaDecoradoSubRamoEjercicio.getSaldosAcumuladosPorMes().get(0).getMesPertenceSaldo() != null){
									saldoSubRamoTMP.setEjercicio(
											estadoCuentaDecoradoSubRamoEjercicio.getSaldosAcumuladosPorMes().get(0).getMesPertenceSaldo().get(Calendar.YEAR)
											);
								}
							}
							
							reporteTrimestralReasegurador.getListaSaldosSubRamo().add(saldoSubRamoTMP);
						}catch(Exception e){
							LogDeMidasEJB3.log("Se recibi� un estado de cuenta sin los saldos desglozados por mes.", Level.WARNING, e);
						}
					}
				}
			}//Fin iteracion subramos por reporte.
			
			//Consultar los subramos facultados que quedaron pendientes.
			if(!listaReporteSaldoSubRamoPendientes.isEmpty() && suscripcionDTO != null){
				for(ReporteSaldoPorSubRamoDTO saldoSubRamo : listaReporteSaldoSubRamoPendientes){
					EstadoCuentaDecoradoDTO estadoCuentaFacultativo = presentacionEstadoCuentaServicios.obtenerEstadoCuentaCombinadoConSaldosCalculados(null, idTcReasegurador,
							idTcMoneda, saldoSubRamo.getIdTcSubRamo(), true,suscripcionDTO.obtenerMesesIncluidos() , false, null, null, false, (short)4);
					//Cada registro en la lista corresponde a un registro en el reporte, ya que es un subramo - contrato - ejercicio del reasegurador, moneda y suscripcion
					if(estadoCuentaFacultativo != null){
						for(EstadoCuentaDecoradoDTO estadoCuentaDecoradoSubRamoEjercicio : listaEjerciciosEstadoCuentaSubRamo){
							//Por cada estado de cuenta se genera un objeto SaldoTrimestralSubramo
							try{
								//4. A partir del estado de cuenta, se genera un objeto ReporteSaldoPorSubRamoDTO y se settean los saldos por primas, comisiones y siniestros
								saldoSubRamo.setMontoPrimaFacultativoMes1(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(0).get(0).getSaldo());
								saldoSubRamo.setMontoComisionFacultativoMes1(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(0).get(1).getSaldo());
								saldoSubRamo.setMontoSiniestroFacultativoMes1(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(0).get(2).getSaldo());
								
								saldoSubRamo.setMontoPrimaFacultativoMes2(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(1).get(0).getSaldo());
								saldoSubRamo.setMontoComisionFacultativoMes2(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(1).get(1).getSaldo());
								saldoSubRamo.setMontoSiniestroFacultativoMes2(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(1).get(2).getSaldo());
								
								saldoSubRamo.setMontoPrimaFacultativoMes3(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(2).get(0).getSaldo());
								saldoSubRamo.setMontoComisionFacultativoMes3(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(2).get(1).getSaldo());
								saldoSubRamo.setMontoSiniestroFacultativoMes3(estadoCuentaDecoradoSubRamoEjercicio.getListaSaldosAcumuladosPorMes().get(2).get(2).getSaldo());
								
								saldoSubRamo.setSaldoFacultativoMes1(estadoCuentaDecoradoSubRamoEjercicio.getSaldosAcumuladosPorMes().get(0).getSaldoAcumulado());
								saldoSubRamo.setSaldoFacultativoMes2(estadoCuentaDecoradoSubRamoEjercicio.getSaldosAcumuladosPorMes().get(1).getSaldoAcumulado());
								saldoSubRamo.setSaldoFacultativoMes3(estadoCuentaDecoradoSubRamoEjercicio.getSaldosAcumuladosPorMes().get(2).getSaldoAcumulado());
								
								if(estadoCuentaDecoradoSubRamoEjercicio.getSaldosAcumuladosPorMes().get(0).getMesPertenceSaldo() != null){
									saldoSubRamo.setEjercicio(
											estadoCuentaDecoradoSubRamoEjercicio.getSaldosAcumuladosPorMes().get(0).getMesPertenceSaldo().get(Calendar.YEAR)
											);
								}
								
								reporteTrimestralReasegurador.setMontoTotalPrimaFacultativoMes1(
										reporteTrimestralReasegurador.getMontoTotalPrimaFacultativoMes1().add(saldoSubRamo.getMontoPrimaFacultativoMes1()));
								reporteTrimestralReasegurador.setMontoTotalPrimaFacultativoMes2(
										reporteTrimestralReasegurador.getMontoTotalPrimaFacultativoMes2().add(saldoSubRamo.getMontoPrimaFacultativoMes2()));
								reporteTrimestralReasegurador.setMontoTotalPrimaFacultativoMes3(
										reporteTrimestralReasegurador.getMontoTotalPrimaFacultativoMes3().add(saldoSubRamo.getMontoPrimaFacultativoMes3()));
								reporteTrimestralReasegurador.setMontoTotalComisionFacultativoMes1(
										reporteTrimestralReasegurador.getMontoTotalComisionFacultativoMes1().add(saldoSubRamo.getMontoComisionFacultativoMes1()));
								reporteTrimestralReasegurador.setMontoTotalComisionFacultativoMes2(
										reporteTrimestralReasegurador.getMontoTotalComisionFacultativoMes2().add(saldoSubRamo.getMontoComisionFacultativoMes2()));
								reporteTrimestralReasegurador.setMontoTotalComisionFacultativoMes3(
										reporteTrimestralReasegurador.getMontoTotalComisionFacultativoMes3().add(saldoSubRamo.getMontoComisionFacultativoMes3()));
								reporteTrimestralReasegurador.setMontoTotalSiniestroFacultativoMes1(
										reporteTrimestralReasegurador.getMontoTotalSiniestroFacultativoMes1().add(saldoSubRamo.getMontoSiniestroFacultativoMes1()));
								reporteTrimestralReasegurador.setMontoTotalSiniestroFacultativoMes2(
										reporteTrimestralReasegurador.getMontoTotalSiniestroFacultativoMes2().add(saldoSubRamo.getMontoSiniestroFacultativoMes2()));
								reporteTrimestralReasegurador.setMontoTotalSiniestroFacultativoMes3(
										reporteTrimestralReasegurador.getMontoTotalSiniestroFacultativoMes3().add(saldoSubRamo.getMontoSiniestroFacultativoMes3()));
								reporteTrimestralReasegurador.setMontoTotalPrimaFacultativoTrimestre(
										reporteTrimestralReasegurador.getMontoTotalPrimaFacultativoTrimestre().add(saldoSubRamo.getMontoTotalPrimaFacultativo()));
								reporteTrimestralReasegurador.setMontoTotalComisionFacultativoTrimestre(
										reporteTrimestralReasegurador.getMontoTotalComisionFacultativoTrimestre().add(saldoSubRamo.getMontoTotalComisionFacultativo()));
								reporteTrimestralReasegurador.setMontoTotalSiniestroFacultativoTrimestre(
										reporteTrimestralReasegurador.getMontoTotalSiniestroFacultativoTrimestre().add(saldoSubRamo.getMontoTotalSiniestroFacultativo()));
								
								reporteTrimestralReasegurador.setMontoTotalSaldoTecnicoFacultativoTrimestre(
										reporteTrimestralReasegurador.getMontoTotalSaldoTecnicoFacultativoTrimestre().add(saldoSubRamo.getSaldoTotalTrimestreFacultativo()));
								
								reporteTrimestralReasegurador.getListaSaldosSubRamo().add(saldoSubRamo);
							}catch(Exception e){
								LogDeMidasEJB3.log("Se recibi� un estado de cuenta sin los saldos desglozados por mes.", Level.WARNING, e);
							}
						}
					}
				}
			}
			
			
			return reporteTrimestralReasegurador;	
		}
		else
			throw new RuntimeException(descripcionError);
		
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@SuppressWarnings("unchecked")
	public List<ReporteSaldoPorSubRamoDTO> obtenerSubRamosAfectados(ReaseguradorCorredorDTO reaseguradorDTO,Integer[] idTcTipoReaseguro,BigDecimal idTcMoneda,boolean excluirSaldoCorredor){
		List<ReporteSaldoPorSubRamoDTO> listaSubRamos = new ArrayList<ReporteSaldoPorSubRamoDTO>();
		
		String queryString = "select distinct edo.idtcsubramo,sr.codigosubramo,sr.descripcionsubramo from MIDAS.toestadocuenta edo, MIDAS.tcsubramo sr where " +
				"edo.idtcsubramo = sr.idtcsubramo and " +
				"edo.idtcreasegurador = "+reaseguradorDTO.getIdtcreaseguradorcorredor()+" and ";
		if(excluirSaldoCorredor){
			queryString += "edo.idtccorredor is null and ";
		}
		queryString += "edo.idtctiporeaseguro = %s and " +
				"edo.idMoneda = %s order by sr.codigosubramo";

		String queryTMP = null;
		Query query = null;
		ReporteSaldoPorSubRamoDTO reporteSaldoPorSubRamo = null;
		
		for(int i=0;i<idTcTipoReaseguro.length;i++){
			queryTMP = String.format(queryString, idTcTipoReaseguro[i].toString(),idTcMoneda.toString());
			query = entityManager.createNativeQuery(queryTMP);

			Object result = query.getResultList();
			if(result != null && result instanceof List)  {
				List<Object> listaResultados = (List<Object>) result;
				for(Object object : listaResultados){
					Object[] singleResult = (Object[]) object;
					reporteSaldoPorSubRamo = new ReporteSaldoPorSubRamoDTO();
					reporteSaldoPorSubRamo.setIdTcSubRamo(Utilerias.obtenerBigDecimal(singleResult[0]));
					if(singleResult[1] instanceof String)
						reporteSaldoPorSubRamo.setCodigoSubRamo(Utilerias.llenarIzquierda((String)singleResult[1], "0", 4));
					else
						reporteSaldoPorSubRamo.setCodigoSubRamo(Utilerias.llenarIzquierda(Utilerias.obtenerBigDecimal(singleResult[1]).toBigInteger().toString(), "0", 4));
					
					reporteSaldoPorSubRamo.setDescripcionSubRamo((String)singleResult[2]);
					reporteSaldoPorSubRamo.setIdTcTipoReaseguro(idTcTipoReaseguro[i].intValue());
					reporteSaldoPorSubRamo.setIdTcReasegurador(reaseguradorDTO.getIdtcreaseguradorcorredor());
					reporteSaldoPorSubRamo.setNombreReasegurador(reaseguradorDTO.getNombre());
					reporteSaldoPorSubRamo.setIdMoneda(idTcMoneda);
					
					listaSubRamos.add(reporteSaldoPorSubRamo);
				}
			}
		}
		
		Collections.sort(listaSubRamos);
		
		return listaSubRamos;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReporteReaseguradorEstadoCuentaDTO> obtenerMovimientosReaseguroCuentasPorPagar(BigDecimal idToPoliza,
			BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal[] idTcSubRamo,Integer mes,Integer anio,Date fechaInicio,Date fechaFin,boolean incluirSoloFacultativo,boolean incluirSoloAutomaticos){
		
		EstructuraFiltradoAgrupacionMovimientos filtroAgrupacion = new EstructuraFiltradoAgrupacionMovimientos();
		if(idToPoliza != null){
			filtroAgrupacion.setIdToPoliza(idToPoliza);
		}
		else{
			filtroAgrupacion.setIncluirTodasLasPolizas(true);
			if(idMoneda != null && (idMoneda.intValue() == MonedaDTO.MONEDA_PESOS || idMoneda.intValue() == MonedaDTO.MONEDA_DOLARES)){
				filtroAgrupacion.setIdMoneda(idMoneda);
			}
		}
		if(mes != null && anio != null){
			filtroAgrupacion.setAnioHasta(anio);
			filtroAgrupacion.setMesHasta(mes);
		}else if(fechaInicio != null && fechaFin != null){
			filtroAgrupacion.setFechaInicio(fechaInicio);
			filtroAgrupacion.setFechaFin(fechaFin);
		}
		filtroAgrupacion.setIdTcReasegurador(idTcReasegurador);
		filtroAgrupacion.setIdTcSubRamos(idTcSubRamo);
		filtroAgrupacion.setIncluirSoloFacultativo(incluirSoloFacultativo);
		filtroAgrupacion.setIncluirSoloAutomaticos(incluirSoloAutomaticos);
		filtroAgrupacion.setIdTcConceptosPorConsultar(CONCEPTO_PRIMA_CEDIDA,CONCEPTO_PRIMA_NO_DEVENGADA,CONCEPTO_COMISION_PRIMA_CEDIDA,CONCEPTO_COMISION_PRIMA_NO_DEVENGADA,
				CONCEPTO_INGRESO_PRIMA_NO_DEVENGADA,CONCEPTO_PRIMA_NO_DEVENGADA_INGRESO,CONCEPTO_CANCELAR_INGRESO_PRIMA_NO_DEVENGADA,CONCEPTO_INGRESO_BONO_POR_NO_SINIESTRO,
				CONCEPTO_CANCELAR_INGRESO_BONO_POR_NO_SINIESTRO,CONCEPTO_CARGOS_REMESAS_SALDOS,CONCEPTO_PAGO_PRIMA,CONCEPTO_CANCELAR_PAGO_PRIMA,CONCEPTO_RETENCION_IMPUESTOS,
				CONCEPTO_CANCELAR_RETENCION_IMPUESTOS);
		
		List<EstructuraAgrupacionFiltradoMovimientos> listaAgrupaciones = obtenerAgrupacionesMovimientos(filtroAgrupacion);
		
		Map<BigDecimal,ReporteReaseguradorEstadoCuentaDTO> mapaReaseguradores = new HashMap<BigDecimal, ReporteReaseguradorEstadoCuentaDTO>();
		
		if(listaAgrupaciones != null){
			//mapa para respaldar los reaseguradores y corredores
			Map<BigDecimal,ReaseguradorCorredorDTO> mapaReaseguradoresDTO = new HashMap<BigDecimal, ReaseguradorCorredorDTO>();
			//Mapa para respaldar los datos de la p�liza, necesario para el n�mero de p�liza.
			Map<BigDecimal,PolizaDTO> mapaPolizas = new HashMap<BigDecimal, PolizaDTO>();
			//Mapa para respaldar los datos del endoso, necesario para fechas de vigencia y tipo de endoso
			Map<BigDecimal[],EndosoDTO> mapaEndosos= new HashMap<BigDecimal[], EndosoDTO>();
			//Mapa para respaldar los datos del subramo, necesario para el c�digo de subramo.
			Map<BigDecimal,SubRamoDTO> mapaSubRamos = new HashMap<BigDecimal, SubRamoDTO>();
			//Mapa para respaldar los datos de la secci�n, necesario para el nombre de la secci�n.
			Map<BigDecimal,SeccionDTO> mapaSecciones= new HashMap<BigDecimal, SeccionDTO>();
			
			
			for(EstructuraAgrupacionFiltradoMovimientos agrupacionMovimientos : listaAgrupaciones){
				agrupacionMovimientos.setIncluirSoloFacultativo(incluirSoloFacultativo);
				ReporteReaseguradorEstadoCuentaDTO reaseguradorEdoCta = mapaReaseguradores.get(agrupacionMovimientos.getIdTcReasegurador());
				if (reaseguradorEdoCta == null){
					reaseguradorEdoCta = new ReporteReaseguradorEstadoCuentaDTO();
					reaseguradorEdoCta.setListaMovimientosReaseguro(new ArrayList<ReporteMovimientoReaseguroDTO>());
					reaseguradorEdoCta.setNombreReasegurador(consultarNombreReasegurador(agrupacionMovimientos.getIdTcReasegurador(), mapaReaseguradoresDTO));
					reaseguradorEdoCta.setTotalImpuestos(BigDecimal.ZERO);
					reaseguradorEdoCta.setTotalIngresosBonoPorNoSiniestro(BigDecimal.ZERO);
					reaseguradorEdoCta.setTotalIngresosPorDevolucionPrima(BigDecimal.ZERO);
					reaseguradorEdoCta.setTotalPagos(BigDecimal.ZERO);
					reaseguradorEdoCta.setTotalPrimaMenosComision(BigDecimal.ZERO);
					reaseguradorEdoCta.setTotalSaldoAPagar(BigDecimal.ZERO);
					
					mapaReaseguradores.put(agrupacionMovimientos.getIdTcReasegurador(), reaseguradorEdoCta);
				}
				
				ReporteMovimientoReaseguroDTO movimientoReaseguro = generarReporteMovimientoReaseguro(agrupacionMovimientos, mapaPolizas, mapaEndosos, mapaSubRamos, mapaReaseguradoresDTO,null,mapaSecciones);
				
				movimientoReaseguro.setIdentificador(calcularIdentificador(movimientoReaseguro, agrupacionMovimientos, mapaPolizas));
				
				reaseguradorEdoCta.getListaMovimientosReaseguro().add(movimientoReaseguro);
				reaseguradorEdoCta.setTotalImpuestos(reaseguradorEdoCta.getTotalImpuestos().add(movimientoReaseguro.getImpuestos()) );
				reaseguradorEdoCta.setTotalIngresosBonoPorNoSiniestro(reaseguradorEdoCta.getTotalIngresosBonoPorNoSiniestro().add(movimientoReaseguro.getIngresosBonoPorNoSiniestro()));
				reaseguradorEdoCta.setTotalIngresosPorDevolucionPrima(reaseguradorEdoCta.getTotalIngresosPorDevolucionPrima().add(movimientoReaseguro.getIngresosDevolucionDePrima()));
				reaseguradorEdoCta.setTotalPagos(reaseguradorEdoCta.getTotalPagos().add(movimientoReaseguro.getPagos()));
				reaseguradorEdoCta.setTotalPrimaMenosComision(reaseguradorEdoCta.getTotalPrimaMenosComision().add(movimientoReaseguro.getPrimaMenosComision()));
				reaseguradorEdoCta.setTotalSaldoAPagar(reaseguradorEdoCta.getTotalSaldoAPagar().add(movimientoReaseguro.getSaldoAPagar()));
			}
		}
		
		List<ReporteReaseguradorEstadoCuentaDTO> listaResultado = new ArrayList<ReporteReaseguradorEstadoCuentaDTO>();
		for(BigDecimal key : mapaReaseguradores.keySet()){
			listaResultado.add(mapaReaseguradores.get(key));
		}
		
		return listaResultado;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReporteReaseguradorEstadoCuentaDTO> obtenerMovimientosReaseguroCuentasPorCobrar(BigDecimal idToReporteSiniestro,
			BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal[] idTcSubRamo,Integer mes,Integer anio,Date fechaInicio,Date fechaFin,boolean incluirSoloFacultativo,boolean incluirSoloAutomaticos){
		
		EstructuraFiltradoAgrupacionMovimientos filtroAgrupacion = new EstructuraFiltradoAgrupacionMovimientos();
		if(idToReporteSiniestro != null){
			filtroAgrupacion.setIdToReporteSiniestro(idToReporteSiniestro);
		}
		else{
			filtroAgrupacion.setIncluirTodosLosSiniestros(true);
			if(idMoneda != null && (idMoneda.intValue() == MonedaDTO.MONEDA_PESOS || idMoneda.intValue() == MonedaDTO.MONEDA_DOLARES)){
				filtroAgrupacion.setIdMoneda(idMoneda);
			}
		}
		if(mes != null && anio != null){
			filtroAgrupacion.setAnioHasta(anio);
			filtroAgrupacion.setMesHasta(mes);
		}else if(fechaInicio != null && fechaFin != null){
			filtroAgrupacion.setFechaInicio(fechaInicio);
			filtroAgrupacion.setFechaFin(fechaFin);
		}
		filtroAgrupacion.setIdTcReasegurador(idTcReasegurador);
		filtroAgrupacion.setIdTcSubRamos(idTcSubRamo);
		filtroAgrupacion.setIncluirSoloFacultativo(incluirSoloFacultativo);
		filtroAgrupacion.setIdTcConceptosPorConsultar(CONCEPTO_INDEMNIZACION,CONCEPTO_CANCELACION_INDEMNIZACION,
				CONCEPTO_GASTOS_DE_AJUSTE,CONCEPTO_CANCELACION_GASTOS_DE_AJUSTE,
				CONCEPTO_DEDUCIBLE,CONCEPTO_CANCELACION_DEDUCIBLE,CONCEPTO_COASEGURO,CONCEPTO_CANCELACION_COASEGURO,
				CONCEPTO_SALVAMENTO,CONCEPTO_CANCELACION_SALVAMENTO,CONCEPTO_RECUPERACION,CONCEPTO_CANCELACION_RECUPERACION,
				CONCEPTO_INGRESO_POR_SINIESTRO,CONCEPTO_ABONOS_DE_REMESAS_SALDOS,CONCEPTO_CANCELACION_INGRESO_POR_SINIESTRO);//buscar s�lo conceptos de siniestros
		
		List<EstructuraAgrupacionFiltradoMovimientos> listaAgrupaciones = obtenerAgrupacionesMovimientos(filtroAgrupacion);
		
		Map<BigDecimal,ReporteReaseguradorEstadoCuentaDTO> mapaReaseguradores = new HashMap<BigDecimal, ReporteReaseguradorEstadoCuentaDTO>();
		
		if(listaAgrupaciones != null){
			//mapa para respaldar los reaseguradores y corredores
			Map<BigDecimal,ReaseguradorCorredorDTO> mapaReaseguradoresDTO = new HashMap<BigDecimal, ReaseguradorCorredorDTO>();
			//Mapa para respaldar los datos de la p�liza, necesario para el n�mero de p�liza.
			Map<BigDecimal,PolizaDTO> mapaPolizas = new HashMap<BigDecimal, PolizaDTO>();
			//Mapa para respaldar los datos del endoso, necesario para fechas de vigencia y tipo de endoso
			Map<BigDecimal[],EndosoDTO> mapaEndosos= new HashMap<BigDecimal[], EndosoDTO>();
			//Mapa para respaldar los datos del subramo, necesario para el c�digo de subramo.
			Map<BigDecimal,SubRamoDTO> mapaSubRamos = new HashMap<BigDecimal, SubRamoDTO>();
			//Mapa para respaldar los datos del siniestro, necesario para n�mero de siniestro, fecha de ocurrencia y estatus del siniestro
			Map<BigDecimal,ReporteSiniestroDTO> mapaSiniestros= new HashMap<BigDecimal, ReporteSiniestroDTO>();
			//Mapa para guardar la descripci�n de cada concepto
			Map<BigDecimal,String> mapaDescripcionConceptos= new HashMap<BigDecimal, String>();
			//Mapa para respaldar los datos de la secci�n, necesario para el nombre de la secci�n.
			Map<BigDecimal,SeccionDTO> mapaSecciones= new HashMap<BigDecimal, SeccionDTO>();
			mapaDescripcionConceptos.put(CONCEPTO_INDEMNIZACION,"INDEMNIZACIONES");
			mapaDescripcionConceptos.put(CONCEPTO_CANCELACION_INDEMNIZACION,"CANCELACION DE INDEMNIZACIONES");
			mapaDescripcionConceptos.put(CONCEPTO_GASTOS_DE_AJUSTE,"GASTOS DE AJUSTE");
			mapaDescripcionConceptos.put(CONCEPTO_CANCELACION_GASTOS_DE_AJUSTE,"CANCELACION DE GASTOS DE AJUSTE");
			mapaDescripcionConceptos.put(CONCEPTO_DEDUCIBLE,"DEDUCIBLE");
			mapaDescripcionConceptos.put(CONCEPTO_CANCELACION_DEDUCIBLE,"CANCELACION DE DEDUCIBLE");
			mapaDescripcionConceptos.put(CONCEPTO_COASEGURO,"COASEGURO");
			mapaDescripcionConceptos.put(CONCEPTO_CANCELACION_COASEGURO,"CANCELACION DE COASEGURO");
			mapaDescripcionConceptos.put(CONCEPTO_SALVAMENTO,"SALVAMENTO");
			mapaDescripcionConceptos.put(CONCEPTO_CANCELACION_SALVAMENTO,"CANCELACION DE SALVAMENTO");
			mapaDescripcionConceptos.put(CONCEPTO_RECUPERACION,"RECUPERACION");
			mapaDescripcionConceptos.put(CONCEPTO_CANCELACION_RECUPERACION,"CANCELACION DE RECUPERACION");
			mapaDescripcionConceptos.put(CONCEPTO_INGRESO_POR_SINIESTRO,"INCGRESOS POR SINIESTRO");
			mapaDescripcionConceptos.put(CONCEPTO_ABONOS_DE_REMESAS_SALDOS,"ABONOS DE REMESAS SALDOS");
			mapaDescripcionConceptos.put(CONCEPTO_CANCELACION_INGRESO_POR_SINIESTRO,"CANCELACION DE INGRESO POR SINIESTRO");
			
			for(EstructuraAgrupacionFiltradoMovimientos agrupacionMovimientos : listaAgrupaciones){
				agrupacionMovimientos.setIncluirSoloFacultativo(incluirSoloFacultativo);
				ReporteReaseguradorEstadoCuentaDTO reaseguradorEdoCta = mapaReaseguradores.get(agrupacionMovimientos.getIdTcReasegurador());
				if (reaseguradorEdoCta == null){
					reaseguradorEdoCta = new ReporteReaseguradorEstadoCuentaDTO();
					reaseguradorEdoCta.setListaMovimientosReaseguro(new ArrayList<ReporteMovimientoReaseguroDTO>());
					reaseguradorEdoCta.setNombreReasegurador(consultarNombreReasegurador(agrupacionMovimientos.getIdTcReasegurador(), mapaReaseguradoresDTO));
					reaseguradorEdoCta.setTotalCesionSiniestro(BigDecimal.ZERO);
					reaseguradorEdoCta.setTotalCobros(BigDecimal.ZERO);
					reaseguradorEdoCta.setTotalSaldoPorCobrar(BigDecimal.ZERO);
					
					mapaReaseguradores.put(agrupacionMovimientos.getIdTcReasegurador(), reaseguradorEdoCta);
				}
				ReporteMovimientoReaseguroDTO movimientoReaseguro = generarReporteMovimientoReaseguro(agrupacionMovimientos, mapaPolizas, mapaEndosos, mapaSubRamos, mapaReaseguradoresDTO,mapaSiniestros,mapaSecciones);
				movimientoReaseguro.setDescripcionConcepto(mapaDescripcionConceptos.get(agrupacionMovimientos.getIdTcConceptoMovimiento()));
				
				reaseguradorEdoCta.getListaMovimientosReaseguro().add(movimientoReaseguro);
				reaseguradorEdoCta.setTotalCesionSiniestro(reaseguradorEdoCta.getTotalCesionSiniestro().add(movimientoReaseguro.getCesionSiniestro()));
				reaseguradorEdoCta.setTotalCobros(reaseguradorEdoCta.getTotalCobros().add(movimientoReaseguro.getCobros()));
				reaseguradorEdoCta.setTotalSaldoPorCobrar(reaseguradorEdoCta.getTotalSaldoPorCobrar().add(movimientoReaseguro.getSaldoPorCobrar()));
			}
		}
		
		List<ReporteReaseguradorEstadoCuentaDTO> listaResultado = new ArrayList<ReporteReaseguradorEstadoCuentaDTO>();
		for(BigDecimal key : mapaReaseguradores.keySet()){
			listaResultado.add(mapaReaseguradores.get(key));
		}
		
		return listaResultado;
	}
	
	private EstructuraFiltradoMovimientos poblarEstructuraFiltradoMovimientos(EstructuraAgrupacionFiltradoMovimientos agrupacionMovimientos){
		EstructuraFiltradoMovimientos filtroMovimientos = new EstructuraFiltradoMovimientos();
		filtroMovimientos.setIdTcReasegurador(agrupacionMovimientos.getIdTcReasegurador());
		filtroMovimientos.setIdTcCorredor(agrupacionMovimientos.getIdTcCorredor());
		filtroMovimientos.setIdToPoliza(agrupacionMovimientos.getIdToPoliza());
		filtroMovimientos.setNumeroEndoso(agrupacionMovimientos.getNumeroEndoso());
		filtroMovimientos.setIdToSeccion(agrupacionMovimientos.getIdToSeccion());
		filtroMovimientos.setNumeroInciso(agrupacionMovimientos.getNumeroInciso());
		if(agrupacionMovimientos.getIdToReporteSiniestro() != null){
			filtroMovimientos.setIdToReporteSiniestro(agrupacionMovimientos.getIdToReporteSiniestro());
		}
		if(agrupacionMovimientos.getFechaMovimiento() != null){
			filtroMovimientos.setFechaMovimientos(agrupacionMovimientos.getFechaMovimiento());
			filtroMovimientos.setIncluirFiltradoFecha(true);
		}
		if(agrupacionMovimientos.isIncluirSoloFacultativo()){
			filtroMovimientos.setIncluirSoloFacultativo(agrupacionMovimientos.isIncluirSoloFacultativo());
		}
		else if(agrupacionMovimientos.isIncluirSoloAutomaticos()){
			filtroMovimientos.setIncluirSoloAutomatico(agrupacionMovimientos.isIncluirSoloAutomaticos());
		}
		if(agrupacionMovimientos.getIdTcSubRamo() != null){
			BigDecimal[] idTcSubRamos = {agrupacionMovimientos.getIdTcSubRamo()};
			filtroMovimientos.setIdTcSubRamos(idTcSubRamos);
		}
		filtroMovimientos.setIdMoneda(agrupacionMovimientos.getIdMoneda());
		if(agrupacionMovimientos.getTipoCambio() != null){
			filtroMovimientos.setTipoCambio(agrupacionMovimientos.getTipoCambio());
		}
		else{
			filtroMovimientos.setValidarTipoCambioNulo(true);
		}
		
		return filtroMovimientos;
	}
	
	private ReporteMovimientoReaseguroDTO generarReporteMovimientoReaseguro(
			EstructuraAgrupacionFiltradoMovimientos agrupacionMovimientos,Map<BigDecimal,PolizaDTO> mapaPolizas,
			Map<BigDecimal[],EndosoDTO> mapaEndosos,Map<BigDecimal,SubRamoDTO> mapaSubRamos,
			Map<BigDecimal,ReaseguradorCorredorDTO> mapaReaseguradoresDTO,
			Map<BigDecimal,ReporteSiniestroDTO> mapaSiniestros,
			Map<BigDecimal,SeccionDTO> mapaSecciones){
		
		EstructuraFiltradoMovimientos filtroMovimientos = poblarEstructuraFiltradoMovimientos(agrupacionMovimientos);
		
		ReporteMovimientoReaseguroDTO movimientoReaseguro = new ReporteMovimientoReaseguroDTO();
		
		//Poblar datos de la p�liza
		poblarDatosPoliza(agrupacionMovimientos.getIdToPoliza(), mapaPolizas, movimientoReaseguro);
		
		//Poblar datos del endoso
		poblarDatosEndoso(agrupacionMovimientos.getIdToPoliza(), new BigDecimal(agrupacionMovimientos.getNumeroEndoso().toString()), mapaEndosos, movimientoReaseguro);
		
		//poblar datos del movimiento
		movimientoReaseguro.setNombreCorredor(consultarNombreReasegurador(agrupacionMovimientos.getIdTcCorredor(), mapaReaseguradoresDTO));
		movimientoReaseguro.setFechaMovimiento(agrupacionMovimientos.getFechaMovimiento());
		poblarCodigoSubRamo(agrupacionMovimientos.getIdTcSubRamo(), mapaSubRamos, movimientoReaseguro);
		movimientoReaseguro.setTipoCambio(agrupacionMovimientos.getTipoCambio());
		movimientoReaseguro.setIdMoneda(agrupacionMovimientos.getIdMoneda().intValue());
		movimientoReaseguro.setDescripcionSeccion(consultarSeccion(agrupacionMovimientos.getIdToSeccion(), mapaSecciones));
		movimientoReaseguro.setNumeroInciso(agrupacionMovimientos.getNumeroInciso());
		
		//Poblar datos del siniestro
		if(agrupacionMovimientos.getIdToReporteSiniestro() != null){
			poblarDatosSiniestro(agrupacionMovimientos.getIdToReporteSiniestro(), mapaSiniestros, movimientoReaseguro);
		}
		
		BigDecimal[] idTcConceptosSuma= null;
		BigDecimal[] idTcConceptosResta = null;
		
		if(agrupacionMovimientos.getIdToReporteSiniestro() == null){
			//Prima cedida
			idTcConceptosSuma = new BigDecimal[1];
			idTcConceptosSuma[0] = CONCEPTO_PRIMA_CEDIDA;
			idTcConceptosResta = new BigDecimal[1];
			idTcConceptosResta[0] = CONCEPTO_PRIMA_NO_DEVENGADA;
			movimientoReaseguro.setPrimaCedida( obtenerSaldoNeto(idTcConceptosSuma, idTcConceptosResta, filtroMovimientos));
			
			//Comisiones
			idTcConceptosSuma[0] = CONCEPTO_COMISION_PRIMA_CEDIDA;
			idTcConceptosResta[0] = CONCEPTO_COMISION_PRIMA_NO_DEVENGADA;
			movimientoReaseguro.setComisiones(obtenerSaldoNeto(idTcConceptosSuma, idTcConceptosResta, filtroMovimientos));
			
			//Prima menos comision se calcula en el bean
			
			//ingresos por bono por no siniestro
			idTcConceptosSuma[0] = CONCEPTO_INGRESO_BONO_POR_NO_SINIESTRO;
			idTcConceptosResta[0] = CONCEPTO_CANCELAR_INGRESO_BONO_POR_NO_SINIESTRO;
			movimientoReaseguro.setIngresosBonoPorNoSiniestro(obtenerSaldoNeto(idTcConceptosSuma, idTcConceptosResta, filtroMovimientos));
			
			//Impuestos
			idTcConceptosSuma[0] = CONCEPTO_RETENCION_IMPUESTOS;
			idTcConceptosResta[0] = CONCEPTO_CANCELAR_RETENCION_IMPUESTOS;
			movimientoReaseguro.setImpuestos(obtenerSaldoNeto(idTcConceptosSuma, idTcConceptosResta, filtroMovimientos));
			
			//pagos
			idTcConceptosSuma = new BigDecimal[2];
			idTcConceptosSuma[0] = CONCEPTO_CARGOS_REMESAS_SALDOS;
			idTcConceptosSuma[1] = CONCEPTO_PAGO_PRIMA;
			idTcConceptosResta[0] = CONCEPTO_CANCELAR_PAGO_PRIMA;
			movimientoReaseguro.setPagos(obtenerSaldoNeto(idTcConceptosSuma, idTcConceptosResta, filtroMovimientos));
			
			//ingresos por devoluci�n de prima
			idTcConceptosSuma[0] = CONCEPTO_INGRESO_PRIMA_NO_DEVENGADA;
			idTcConceptosSuma[1] = CONCEPTO_PRIMA_NO_DEVENGADA_INGRESO;
			idTcConceptosResta[0] = CONCEPTO_CANCELAR_INGRESO_PRIMA_NO_DEVENGADA;
			movimientoReaseguro.setIngresosDevolucionDePrima(obtenerSaldoNeto(idTcConceptosSuma, idTcConceptosResta, filtroMovimientos));
			
			//Calcular saldo a pagar
			BigDecimal saldoPorConcepto = movimientoReaseguro.getPrimaMenosComision().add(movimientoReaseguro.getIngresosDevolucionDePrima()).add(
					movimientoReaseguro.getIngresosBonoPorNoSiniestro()).add(movimientoReaseguro.getImpuestos()).subtract(movimientoReaseguro.getPagos());
			movimientoReaseguro.setSaldoAPagar(saldoPorConcepto);
		}
		else{//Consulta de CxC
			
			movimientoReaseguro.setCesionSiniestro(BigDecimal.ZERO);
			movimientoReaseguro.setCobros(BigDecimal.ZERO);
			
			//Cesi�n siniestro
				//Indemnizaciones y gastos son positivos
			if(agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_INDEMNIZACION) == 0 ||
					agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_GASTOS_DE_AJUSTE) == 0){
				filtroMovimientos.setIdConceptoMovDetalle(agrupacionMovimientos.getIdTcConceptoMovimiento());
				filtroMovimientos.setIdConceptosMovtosDetalle(null);
				movimientoReaseguro.setCesionSiniestro(obtenerSaldoPorConcepto(filtroMovimientos));
			}
			else if (agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_CANCELACION_INDEMNIZACION) == 0 ||
					agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_CANCELACION_GASTOS_DE_AJUSTE) == 0){
				filtroMovimientos.setIdConceptoMovDetalle(agrupacionMovimientos.getIdTcConceptoMovimiento());
				filtroMovimientos.setIdConceptosMovtosDetalle(null);
				movimientoReaseguro.setCesionSiniestro(obtenerSaldoPorConcepto(filtroMovimientos).negate());
			}
				//deducible, coaseguro, salvamento y recuperaciones son negativos
			else if (agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_DEDUCIBLE) == 0 ||
					agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_COASEGURO) == 0 ||
					agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_SALVAMENTO) == 0 ||
					agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_RECUPERACION) == 0 ){
				filtroMovimientos.setIdConceptoMovDetalle(agrupacionMovimientos.getIdTcConceptoMovimiento());
				filtroMovimientos.setIdConceptosMovtosDetalle(null);
				movimientoReaseguro.setCesionSiniestro(obtenerSaldoPorConcepto(filtroMovimientos).negate());
			}
			else if(agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_CANCELACION_DEDUCIBLE) == 0 ||
					agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_CANCELACION_COASEGURO) == 0 ||
					agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_CANCELACION_SALVAMENTO) == 0 ||
					agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_CANCELACION_RECUPERACION) == 0 ){
				filtroMovimientos.setIdConceptoMovDetalle(agrupacionMovimientos.getIdTcConceptoMovimiento());
				filtroMovimientos.setIdConceptosMovtosDetalle(null);
				movimientoReaseguro.setCesionSiniestro(obtenerSaldoPorConcepto(filtroMovimientos));
			}
			//Cobros
				//Ingresos por siniestros va positivo
			else if(agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_INGRESO_POR_SINIESTRO) == 0 ||
					agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_ABONOS_DE_REMESAS_SALDOS) == 0){
				filtroMovimientos.setIdConceptoMovDetalle(agrupacionMovimientos.getIdTcConceptoMovimiento());
				filtroMovimientos.setIdConceptosMovtosDetalle(null);
				movimientoReaseguro.setCobros(obtenerSaldoPorConcepto(filtroMovimientos));
			}
			else if(agrupacionMovimientos.getIdTcConceptoMovimiento().compareTo(CONCEPTO_CANCELACION_INGRESO_POR_SINIESTRO) == 0){
				filtroMovimientos.setIdConceptoMovDetalle(agrupacionMovimientos.getIdTcConceptoMovimiento());
				filtroMovimientos.setIdConceptosMovtosDetalle(null);
				movimientoReaseguro.setCobros(obtenerSaldoPorConcepto(filtroMovimientos).negate());
			}
		}
		return movimientoReaseguro;
	}
	
	private BigDecimal obtenerSaldoNeto(BigDecimal[] idTcConceptosSuma,BigDecimal[] idTcConceptosResta,EstructuraFiltradoMovimientos filtroMovimientos){

		BigDecimal saldoMas = BigDecimal.ZERO;
		BigDecimal saldoMenos = BigDecimal.ZERO;
		
		if(idTcConceptosSuma != null && idTcConceptosSuma.length >0){
			if(idTcConceptosSuma.length == 1){
				filtroMovimientos.setIdConceptoMovDetalle(idTcConceptosSuma[0]);
				filtroMovimientos.setIdConceptosMovtosDetalle(null);
				saldoMas = saldoMas.add(obtenerSaldoPorConcepto(filtroMovimientos));
			}
			else{
				filtroMovimientos.setIdConceptoMovDetalle(null);
				filtroMovimientos.setIdConceptosMovtosDetalle(idTcConceptosSuma);
				saldoMas = saldoMas.add(obtenerSaldoPorConcepto(filtroMovimientos));
			}
		}
		
		if(idTcConceptosResta != null && idTcConceptosResta.length >0){
			if(idTcConceptosResta.length == 1){
				filtroMovimientos.setIdConceptoMovDetalle(idTcConceptosResta[0]);
				filtroMovimientos.setIdConceptosMovtosDetalle(null);
				saldoMenos = saldoMenos.add(obtenerSaldoPorConcepto(filtroMovimientos));
			}
			else{
				filtroMovimientos.setIdConceptoMovDetalle(null);
				filtroMovimientos.setIdConceptosMovtosDetalle(idTcConceptosResta);
				saldoMenos = saldoMenos.add(obtenerSaldoPorConcepto(filtroMovimientos));
			}
		}
		
		return saldoMas.subtract(saldoMenos);
	}
	
	private void poblarDatosEndoso(BigDecimal idToPoliza,BigDecimal numeroEndoso,Map<BigDecimal[],EndosoDTO> mapaEndosos,ReporteMovimientoReaseguroDTO movimientoReaseguro){
		BigDecimal[] key = {idToPoliza,numeroEndoso};
		EndosoDTO endosoDTO = mapaEndosos.get(key);
		if(endosoDTO == null){
			EndosoId id = new EndosoId(idToPoliza,numeroEndoso.shortValue());
			endosoDTO = entityManager.find(EndosoDTO.class, id);
			endosoDTO.setDescripcionTipoEndoso(Utilerias.calcularDescripcionTipoEndoso(endosoDTO));
			mapaEndosos.put(key, endosoDTO);
		}
		movimientoReaseguro.setNumeroEndoso(numeroEndoso.intValue());
		movimientoReaseguro.setFechaEmision(endosoDTO.getFechaCreacion());
		movimientoReaseguro.setFechaInicioVigencia(endosoDTO.getFechaInicioVigencia());
		movimientoReaseguro.setFechaFinVigencia(endosoDTO.getFechaFinVigencia());
		movimientoReaseguro.setTipoEndoso(endosoDTO.getDescripcionTipoEndoso());
	}
	
	private String calcularIdentificador(ReporteMovimientoReaseguroDTO movimientoReaseguro,
			EstructuraAgrupacionFiltradoMovimientos agrupacionMovimientos,Map<BigDecimal,PolizaDTO> mapaPolizas){
		String identificador = "";
		
		if(agrupacionMovimientos.getIdToSeccion() != null && agrupacionMovimientos.getNumeroInciso() != null){
			if(movimientoReaseguro.getPrimaCedida().compareTo(BigDecimal.ZERO) <0){
				identificador += "20";
			}
			else{
				identificador += "00";
			}
			
			identificador += mapaPolizas.get(agrupacionMovimientos.getIdToPoliza()).getFolioPoliza();
			identificador += Utilerias.llenarIzquierda(agrupacionMovimientos.getIdToPoliza().toBigInteger().toString(), "0", 6);
			identificador += Utilerias.llenarIzquierda(agrupacionMovimientos.getNumeroEndoso().toString(), "0", 4);
			identificador += Utilerias.llenarIzquierda(agrupacionMovimientos.getNumeroInciso().toString(), "0", 4);
			identificador += Utilerias.llenarIzquierda(agrupacionMovimientos.getIdToSeccion().toBigInteger().toString(), "0", 4);
		}
		
		return identificador;
	}
	
	private String consultarSeccion(BigDecimal idToSeccion,Map<BigDecimal,SeccionDTO> mapaSecciones){
		String descripcion = "";
		if(idToSeccion != null){
			SeccionDTO seccionDTO = mapaSecciones.get(idToSeccion);
			if(seccionDTO == null){
				seccionDTO = entityManager.find(SeccionDTO.class, idToSeccion);
				mapaSecciones.put(idToSeccion, seccionDTO);
			}
			descripcion = mapaSecciones.get(idToSeccion).getNombreComercial();
		}
		return descripcion;
	}
	
	private void poblarCodigoSubRamo(BigDecimal idTcSubRamo,Map<BigDecimal,SubRamoDTO> mapaSubRamos,ReporteMovimientoReaseguroDTO movimientoReaseguro){
		SubRamoDTO subRamoDTO = mapaSubRamos.get(idTcSubRamo);
		if(subRamoDTO == null){
			subRamoDTO = entityManager.find(SubRamoDTO.class, idTcSubRamo);
			mapaSubRamos.put(idTcSubRamo, subRamoDTO);
		}
		movimientoReaseguro.setCodigoSubRamo(Utilerias.llenarIzquierda(subRamoDTO.getCodigoSubRamo().toBigInteger().toString(), "0", 4));
	}
	
	private SubRamoDTO obtenerSubRamoDTO(BigDecimal idTcSubRamo,Map<BigDecimal,SubRamoDTO> mapaSubRamos){
		SubRamoDTO subRamoDTO = mapaSubRamos.get(idTcSubRamo);
		if(subRamoDTO == null){
			subRamoDTO = entityManager.find(SubRamoDTO.class, idTcSubRamo);
			mapaSubRamos.put(idTcSubRamo, subRamoDTO);
		}
		return mapaSubRamos.get(idTcSubRamo);
	}
	
	private void poblarDatosSiniestro(BigDecimal idToReporteSiniestro,Map<BigDecimal,ReporteSiniestroDTO> mapaSiniestros,ReporteMovimientoReaseguroDTO movimientoReaseguro){
		ReporteSiniestroDTO siniestroDTO = mapaSiniestros.get(idToReporteSiniestro);
		if(siniestroDTO == null){
			siniestroDTO = entityManager.find(ReporteSiniestroDTO.class, idToReporteSiniestro);
			mapaSiniestros.put(idToReporteSiniestro, siniestroDTO);
		}
		movimientoReaseguro.setNumeroSiniestro(siniestroDTO.getNumeroReporte());
		movimientoReaseguro.setFechaOcurrenciaSiniestro(siniestroDTO.getFechaSiniestro());
		movimientoReaseguro.setEstatusSiniestro(siniestroDTO.getEstatusSiniestro().getDescripcion());
	}
	
	private void poblarDatosPoliza(BigDecimal idToPoliza,Map<BigDecimal,PolizaDTO> mapaPolizas,ReporteMovimientoReaseguroDTO movimientoReaseguro){
		PolizaDTO polizaDTO = mapaPolizas.get(idToPoliza);
		if(polizaDTO == null){
			polizaDTO = entityManager.find(PolizaDTO.class, idToPoliza);
			polizaDTO.setFolioPoliza(Utilerias.obtenerNumeroPoliza(polizaDTO));
			mapaPolizas.put(idToPoliza, polizaDTO);
		}
		movimientoReaseguro.setNombreAsegurado(polizaDTO.getCotizacionDTO().getNombreAsegurado());
		movimientoReaseguro.setNumeroPoliza(polizaDTO.getFolioPoliza());
	}
	
	private String consultarNombreReasegurador(BigDecimal idTcReasegurador,
			Map<BigDecimal,ReaseguradorCorredorDTO> mapaReaseguradoresDTO){
		String nombreReasegurador = "No disponible";
		if(idTcReasegurador != null){
			if(!mapaReaseguradoresDTO.containsKey(idTcReasegurador)){
				ReaseguradorCorredorDTO reasegurador = entityManager.find(ReaseguradorCorredorDTO.class, idTcReasegurador);
				if(reasegurador != null){
					mapaReaseguradoresDTO.put(idTcReasegurador, reasegurador);
					nombreReasegurador = reasegurador.getNombre();
				}
			}
			else{
				nombreReasegurador = mapaReaseguradoresDTO.get(idTcReasegurador).getNombre();
			}
		}
		else{
			nombreReasegurador = "";
		}
		return nombreReasegurador;
		
	}
	
	/**
	 * Consulta el saldo por concepto de la configuraci�n de movimientos recibida en el objeto EstructuraFiltradoMovimientos.
	 * @param filtroMovimientos
	 * @return saldo del concepto bajo la configuraci�n encapsulada en el filtro recibido.
	 */
	private BigDecimal obtenerSaldoPorConcepto(EstructuraFiltradoMovimientos filtroMovimientos){
		BigDecimal saldoCalculado = BigDecimal.ZERO;
		if(filtroMovimientos.isIncluirFiltradoHasta()){
			if(!(filtroMovimientos.getMesHasta() != null && filtroMovimientos.getAnioHasta() != null && 
					filtroMovimientos.getMesHasta()>0 && filtroMovimientos.getMesHasta()<=12 && filtroMovimientos.getAnioHasta()>1900))
				filtroMovimientos.setIncluirFiltradoHasta(false);
		}
		String descripcionSubRamos = "";
		boolean incluirFiltradoPorSubramos = filtroMovimientos.getIdTcSubRamos() != null && filtroMovimientos.getIdTcSubRamos().length > 0;
		if(incluirFiltradoPorSubramos){
			descripcionSubRamos = obtenerListaSubRamos(filtroMovimientos.getIdTcSubRamos());
		}
		
		String descripcionIdConceptos = "";
		boolean incluirFiltradoPorConceptos = filtroMovimientos.getIdConceptoMovDetalle() == null && 
												filtroMovimientos.getIdConceptosMovtosDetalle() != null && 
												filtroMovimientos.getIdConceptosMovtosDetalle().length >0;
		if(incluirFiltradoPorConceptos){
			descripcionIdConceptos = obtenerListaSubRamos(filtroMovimientos.getIdConceptosMovtosDetalle());
		}
		if((filtroMovimientos.getIdToReporteSiniestro() != null || filtroMovimientos.getIdTcReasegurador() != null || filtroMovimientos.getIdToPoliza() != null) &&
				(filtroMovimientos.getIdConceptoMovDetalle() != null || filtroMovimientos.getIdConceptosMovtosDetalle() != null )){
			String queryString = "select nvl(sum(mov.cantidad),0) saldoConcepto from midas.tomovimientoreaseguro mov where ";
			if(filtroMovimientos.isIncluirFiltradoFecha() && filtroMovimientos.getFechaMovimientos() != null){
				DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
				queryString += "TRUNC(mov.fecharegistro) = TO_DATE('"+(formato.format(filtroMovimientos.getFechaMovimientos()))+"','dd/MM/yyyy') and ";
			}//fin incluir filtrado fecha
			else if(filtroMovimientos.isIncluirFiltradoHasta()){
					queryString += "to_number(to_char(mov.fecharegistro,'mm')) <= 'X' and ";
					queryString += "to_number(to_char(mov.fecharegistro,'yyyy')) <= 'X' and ";
			}//fin incluirFiltradoHasta
			if(filtroMovimientos.getIdToPoliza() != null){
					queryString += "mov.idtopoliza = "+filtroMovimientos.getIdToPoliza().toBigInteger().toString()+" and "; 
					if(filtroMovimientos.getNumeroEndoso() != null){
						queryString += "mov.numeroendoso = "+filtroMovimientos.getNumeroEndoso()+" and ";
					}//fin numeroendoso
			} //fin idtopoliza
			else if (filtroMovimientos.isIncluirTodasLasPolizas()){
					queryString += "mov.idtopoliza is not null and ";
			}//fin incluirTodasLasPolizas
			if(filtroMovimientos.getIdToReporteSiniestro() != null){
					queryString += "mov.idtoreportesiniestro = "+filtroMovimientos.getIdToReporteSiniestro().toBigInteger().toString()+" and ";
			}//fin idToReporteSiniestro
			else if (filtroMovimientos.isIncluirTodosLosSiniestros()){
					queryString += "mov.idtoreportesiniestro is not null and ";
			} //fin incluirTodosLosSiniestros
			if(filtroMovimientos.getIdTcReasegurador() != null){
					queryString += "mov.idtcreaseguradorcorredor = "+filtroMovimientos.getIdTcReasegurador().toBigInteger().toString()+" and ";
					if(filtroMovimientos.getIdTcCorredor() != null){
					  queryString += "mov.idtccorredor = "+filtroMovimientos.getIdTcCorredor().toBigInteger().toString()+" and ";
					}//Fin idTcCorredor
			}//Fin idTcReasegurador
			if(filtroMovimientos.getIdMoneda() != null){
					queryString += "mov.idmoneda = "+filtroMovimientos.getIdMoneda().toBigInteger().toString()+" and ";
			}//fin idMoneda
			if(incluirFiltradoPorSubramos){
					queryString += "mov.idtcsubramo in ("+descripcionSubRamos+") and ";
			}//Fin incluirFiltroSubRamos
			if(filtroMovimientos.getIdToSeccion() != null){
				queryString += "mov.IDTOSECCION = "+filtroMovimientos.getIdToSeccion()+" and ";
			}
			else{
				queryString += "mov.IDTOSECCION is null and ";
			}
			if(filtroMovimientos.getNumeroInciso() != null){
				queryString += "mov.IDTOINCISO = "+filtroMovimientos.getNumeroInciso()+" and ";
			}
			else{
				queryString += "mov.IDTOINCISO is null and ";
			}
			if(filtroMovimientos.getIdConceptoMovDetalle() != null){
					queryString += "mov.idtcconceptomovimiento = "+filtroMovimientos.getIdConceptoMovDetalle().toBigInteger().toString()+" and ";
			}//fin idTcConceptoMovimiento
			else if(incluirFiltradoPorConceptos){
					queryString += "mov.idtcconceptomovimiento in ("+descripcionIdConceptos+") and ";
			}//fin incluirfiltroIdTcConceptoMovDet
			if(filtroMovimientos.isIncluirSoloFacultativo()){
				queryString += "mov.idtctiporeaseguro = 3 and ";
			}//fin incluirSoloFacultativo
			else if (filtroMovimientos.isIncluirSoloAutomatico()){
					queryString += "mov.idtctiporeaseguro <> 3 and ";
			}//fin incluirSoloAutomatico
			if(filtroMovimientos.getTipoCambio() != null){
				queryString += "mov.tipocambio = "+filtroMovimientos.getTipoCambio().toString() + " and ";
			}
			else if(filtroMovimientos.isValidarTipoCambioNulo()){
				queryString += "mov.tipocambio is null and ";
			}
			queryString += "mov.acumulado = 1 ";
			Query query = entityManager.createNativeQuery(queryString);
			Object resultado = query.getSingleResult();
			saldoCalculado = Utilerias.obtenerBigDecimal(resultado);
		}
		return saldoCalculado;
	}
	
	/**
	 * Consulta las diferentes combinaciones idTcReasegurador-idTcCorredor-idToPoliza-numeroEndoso-fechaMovimiento-idTcSubRamo-tipoCambio 
	 * de los movimientos reaseguro para consulta de movimientos de la cuenta por pagar.
	 * @param filtroAgrupacion criterios de b�squeda de las agrupaciones.
	 * @return List<EstructuraAgrupacionFiltradoMovimientos> Agrupaciones encontradas.
	 */
	@SuppressWarnings("unchecked")
	private List<EstructuraAgrupacionFiltradoMovimientos> obtenerAgrupacionesMovimientos(EstructuraFiltradoAgrupacionMovimientos filtroAgrupacion/*,boolean reporteDetallado*/){
		String descripcionSubRamos = "";
		boolean incluirFiltradoPorSubramos = filtroAgrupacion.getIdTcSubRamos() != null && filtroAgrupacion.getIdTcSubRamos().length > 0;
		if(incluirFiltradoPorSubramos){
			descripcionSubRamos = obtenerListaSubRamos(filtroAgrupacion.getIdTcSubRamos());
		}
		boolean incluirFiltradoPorConceptos = filtroAgrupacion.getIdTcConceptos() != null && filtroAgrupacion.getIdTcConceptos().length > 0;
		String descripcionIdTcConceptos= "";
		if(incluirFiltradoPorConceptos){
			descripcionIdTcConceptos = obtenerListaSubRamos(filtroAgrupacion.getIdTcConceptos());
		}
		String queryString = null;
		if(filtroAgrupacion.getIdToPoliza() != null || filtroAgrupacion.isIncluirTodasLasPolizas()){//Si es consulta por p�liza
			/*if(reporteDetallado){
				queryString = "select distinct mov.idtcreaseguradorcorredor,mov.IDTCCORREDOR,mov.idtopoliza,mov.numeroEndoso," +
						"trunc(mov.fecharegistro),mov.idtcsubramo,mov.tipocambio,mov.idmoneda from tomovimientoreaseguro mov where ";
			}
			else{*/
				queryString = "select distinct mov.idtcreaseguradorcorredor,mov.IDTCCORREDOR,mov.idtopoliza,mov.numeroEndoso," +
						"trunc(mov.fecharegistro),mov.idtcsubramo,mov.tipocambio,mov.idmoneda,mov.IDTOSECCION,mov.IDTOINCISO from MIDAS.tomovimientoreaseguro mov where ";
			//}
		}
		else{//Es consulta por siniestros
			queryString = "select distinct mov.idtcreaseguradorcorredor,mov.IDTCCORREDOR,mov.idtopoliza,mov.numeroEndoso," +
					"trunc(mov.fecharegistro),mov.idtcsubramo,mov.tipocambio,mov.idmoneda,mov.IDTOSECCION,mov.IDTOINCISO,mov.IDTOREPORTESINIESTRO,mov.IDTCCONCEPTOMOVIMIENTO from MIDAS.tomovimientoreaseguro mov where ";
		}
		//Incluir filtro por Poliza
		if(filtroAgrupacion.getIdToPoliza() != null){
			queryString += "mov.idtopoliza = "+filtroAgrupacion.getIdToPoliza().toBigInteger().toString()+" and ";
		}
		//Incluir filtro por todas las polizas
		else if(filtroAgrupacion.isIncluirTodasLasPolizas()){
		    queryString += "mov.idtopoliza is not null and ";
		    //Incluir filtro por moneda
		    if(filtroAgrupacion.getIdMoneda() != null){
		    	queryString += "mov.idMoneda = "+filtroAgrupacion.getIdMoneda().toBigInteger().toString()+" and "; 
		    }
		}
		//Incluir filtro por siniestro
		else if(filtroAgrupacion.getIdToReporteSiniestro() != null){
			queryString += "mov.IDTOREPORTESINIESTRO = "+filtroAgrupacion.getIdToReporteSiniestro().toBigInteger().toString()+" and ";
		}
		//Incluir todos los siniestros
		else if(filtroAgrupacion.isIncluirTodosLosSiniestros()){
			queryString += "mov.IDTOREPORTESINIESTRO is not null and ";
		    //Incluir filtro por moneda
		    if(filtroAgrupacion.getIdMoneda() != null){
		    	queryString += "mov.idMoneda = "+filtroAgrupacion.getIdMoneda().toBigInteger().toString()+" and "; 
		    }
		}
		//Incluir filtrado por reasegurador
		if(filtroAgrupacion.getIdTcReasegurador() != null){
		    queryString += "mov.idtcreaseguradorcorredor = "+filtroAgrupacion.getIdTcReasegurador().toBigInteger().toString()+" and ";
		}
		else{
			queryString += "mov.idtcreaseguradorcorredor is not null and ";
		}
		//Incluir filtro por corredor
		if(filtroAgrupacion.getIdTcCorredor() != null){
		    queryString += "mov.IDTCCORREDOR = "+filtroAgrupacion.getIdTcCorredor().toBigInteger().toString()+" and "; 
		}
		//Incluir filtro por subramos
		if(incluirFiltradoPorSubramos){
			queryString += "mov.idtcsubramo in ("+descripcionSubRamos+") and "; 
		}
		//Filtro por fecha de corte
		if(filtroAgrupacion.isIncluirFiltradoHasta()){
			queryString += "to_number(to_char(mov.fecharegistro,'mm')) <= "+filtroAgrupacion.getMesHasta()+" and "; 
			queryString += "to_number(to_char(mov.fecharegistro,'yyyy')) <= "+filtroAgrupacion.getAnioHasta()+" and "; 
		}
		//Filtrado por fechas
		else if(filtroAgrupacion.isIncluirFiltradoRangoFechas()){
			queryString += "trunc(mov.fecharegistro) >= TO_DATE('"+(new java.sql.Date(filtroAgrupacion.getFechaInicio().getTime()).toString())+"','yyyy-MM-dd') and ";
			queryString += "trunc(mov.fecharegistro) <= TO_DATE('"+(new java.sql.Date(filtroAgrupacion.getFechaFin().getTime()).toString())+"','yyyy-MM-dd') and ";
		}
		//Incluir filtro por conceptos de los movimientos
		if(incluirFiltradoPorConceptos){
			queryString += " mov.IDTCCONCEPTOMOVIMIENTO in ("+descripcionIdTcConceptos+") and ";
		}
		//Incluir s�lo movtos facultativos
		if(filtroAgrupacion.isIncluirSoloFacultativo()){
			queryString += "mov.IDTCTIPOREASEGURO = 3 ";
		}
		//Incluir s�lo movtos automaticos
		else if(filtroAgrupacion.isIncluirSoloAutomaticos()){
			queryString += "mov.IDTCTIPOREASEGURO <> 3 ";
		}
		queryString += "and mov.acumulado = 1 order by mov.idtcreaseguradorcorredor,mov.idtccorredor,mov.idtopoliza,mov.numeroendoso,trunc(mov.fecharegistro)";
		
		Query query = entityManager.createNativeQuery(queryString);
		Object result = query.getResultList();
		List<EstructuraAgrupacionFiltradoMovimientos> listaEstructuraAgrupacion = new ArrayList<EstructuraAgrupacionFiltradoMovimientos>();
		if(result != null && result instanceof List)  {
			List<Object> listaResultados = (List<Object>) result;
			for(Object object : listaResultados){
				Object[] singleResult = (Object[]) object;
				EstructuraAgrupacionFiltradoMovimientos estructuraAgrupacionTMP = new EstructuraAgrupacionFiltradoMovimientos();
				estructuraAgrupacionTMP.setIdTcReasegurador(Utilerias.obtenerBigDecimal(singleResult[0]));
				if(singleResult[1] != null)
					estructuraAgrupacionTMP.setIdTcCorredor(Utilerias.obtenerBigDecimal(singleResult[1]));
				estructuraAgrupacionTMP.setIdToPoliza(Utilerias.obtenerBigDecimal(singleResult[2]));
				estructuraAgrupacionTMP.setNumeroEndoso(Utilerias.obtenerInteger(singleResult[3]));
				estructuraAgrupacionTMP.setFechaMovimiento(Utilerias.obtenerDate(singleResult[4]));
				estructuraAgrupacionTMP.setIdTcSubRamo(Utilerias.obtenerBigDecimal(singleResult[5]));
				estructuraAgrupacionTMP.setTipoCambio(Utilerias.obtenerBigDecimal(singleResult[6]));
				estructuraAgrupacionTMP.setIdMoneda(Utilerias.obtenerBigDecimal(singleResult[7]));
				estructuraAgrupacionTMP.setIdToSeccion(Utilerias.obtenerBigDecimal(singleResult[8]));
				estructuraAgrupacionTMP.setNumeroInciso(Utilerias.obtenerInteger(singleResult[9]));
				if(filtroAgrupacion.getIdToPoliza() != null || !filtroAgrupacion.isIncluirTodasLasPolizas()){//Si no es consulta por p�liza
					estructuraAgrupacionTMP.setIdToReporteSiniestro(Utilerias.obtenerBigDecimal(singleResult[10]));
					estructuraAgrupacionTMP.setIdTcConceptoMovimiento(Utilerias.obtenerBigDecimal(singleResult[11]));
				}
				listaEstructuraAgrupacion.add(estructuraAgrupacionTMP);
			}
		}
		
		return listaEstructuraAgrupacion;
	}
	
	public String obtenerListaSubRamos(BigDecimal[] idTcSubRamos){
		String descripcionSubRamos = "";
		StringBuilder descripcion = new StringBuilder("");
		boolean incluirFiltradoPorSubramos = idTcSubRamos != null && idTcSubRamos.length > 0;
		if(incluirFiltradoPorSubramos){
			for(int i=0;i<idTcSubRamos.length;i++){
				descripcion.append(idTcSubRamos[i].toBigInteger().toString()).append(",");
			}
			descripcionSubRamos = descripcion.toString();
			descripcionSubRamos = descripcionSubRamos.substring(0, descripcionSubRamos.length()-1);
		}
		return descripcionSubRamos;
	}
	
	private ContratoFacultativoDTO calcularPrimasReasegurador(ReporteSaldoPorNegocioFacultativoDTO reporteNegocioFacultativo,BigDecimal idTmContratoFacultativo,
			List<ParticipacionFacultativoDTO> listaDetallesParticipacion,boolean excluirSaldoCorredor,
			Map<BigDecimal,List<ParticipacionCorredorFacultativoDTO>> mapaParticipacionesCorredorPorContrato,
			Map<BigDecimal,BigDecimal> mapaPorcentajesFacultativosPorLineaReas,
			Map<BigDecimal,SubRamoDTO> mapaSubRamos){
		BigDecimal montoPrima = BigDecimal.ZERO;
		BigDecimal porcentajeParticipacion = BigDecimal.ZERO;
		BigDecimal porcentajeComision = BigDecimal.ZERO;
		BigDecimal montoPrimaParticipacion = BigDecimal.ZERO;
		BigDecimal montoComision = BigDecimal.ZERO;
		BigDecimal montoPrimaAPagar = BigDecimal.ZERO;
		
		ContratoFacultativoDTO contratoFacultativoEnCurso = null;
		
		//La consulta del porcentaje facultativo ser� a trav�s de un query, para evitar el consumo masivo de memoria
		BigDecimal porcentajeFacultativo = consultarPorcentajeFacultativoPorContratoFacultativo(mapaPorcentajesFacultativosPorLineaReas, idTmContratoFacultativo);
		int cantidadParticipaciones = 0;
		
		if(listaDetallesParticipacion != null && !listaDetallesParticipacion.isEmpty()){
			cantidadParticipaciones = listaDetallesParticipacion.size();
			
			for(ParticipacionFacultativoDTO participacionTMP : listaDetallesParticipacion){
				if(contratoFacultativoEnCurso == null){
					contratoFacultativoEnCurso = participacionTMP.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO_1();
				}
				//Calculo de la prima de la cobertura aplicando el % facultativo
				BigDecimal prima = participacionTMP.getDetalleContratoFacultativoDTO().getPrimaFacultadaCobertura().subtract(
						participacionTMP.getDetalleContratoFacultativoDTO().getMontoPrimaNoDevengada()).multiply(porcentajeFacultativo).divide(new BigDecimal(100));
				
				montoPrima = montoPrima.add(prima);
				porcentajeParticipacion = porcentajeParticipacion.add(participacionTMP.getPorcentajeParticipacion());
				porcentajeComision = porcentajeComision.add(participacionTMP.getComision());
				
				//Calculo de la prima al reasegurador aplicando el % de participacion
				BigDecimal primaParticipacionTMP = prima.multiply(participacionTMP.getPorcentajeParticipacion().divide(new BigDecimal(100)));
				montoPrimaParticipacion = montoPrimaParticipacion.add( primaParticipacionTMP );
				
				//Calculo de la comision aplicando el % de comsion a la prima de la participacion
				BigDecimal comisionParticipacionTMP = primaParticipacionTMP.multiply(participacionTMP.getComision().divide(new BigDecimal(100)));
				montoComision = montoComision.add(comisionParticipacionTMP);
				
				//Calculo de la prima a pagar restando la comision de la prima participaci�n
				BigDecimal primaAPagarTMP = primaParticipacionTMP.subtract(comisionParticipacionTMP);
				
				montoPrimaAPagar = montoPrimaAPagar.add(primaAPagarTMP);
				
			}
		}
		
		if(!excluirSaldoCorredor && mapaParticipacionesCorredorPorContrato != null){
			List<ParticipacionCorredorFacultativoDTO> listaParticipacionesCorredorContrato = mapaParticipacionesCorredorPorContrato.get(idTmContratoFacultativo);
			if(listaParticipacionesCorredorContrato != null && !listaParticipacionesCorredorContrato.isEmpty()){
				cantidadParticipaciones += listaParticipacionesCorredorContrato.size();
				for(ParticipacionCorredorFacultativoDTO participacionCorredorContrato : listaParticipacionesCorredorContrato){
					if(contratoFacultativoEnCurso == null){
						contratoFacultativoEnCurso = participacionCorredorContrato.getParticipacionFacultativoDTO().
									getDetalleContratoFacultativoDTO().getContratoFacultativoDTO_1();
					}
					
					//Calculo de la prima de la cobertura aplicando el % facultativo
					BigDecimal prima = participacionCorredorContrato.getParticipacionFacultativoDTO().getDetalleContratoFacultativoDTO().getPrimaFacultadaCobertura().subtract(
							participacionCorredorContrato.getParticipacionFacultativoDTO().getDetalleContratoFacultativoDTO().getMontoPrimaNoDevengada())
							.multiply(porcentajeFacultativo).divide(new BigDecimal(100));
					
					montoPrima = montoPrima.add(prima);
					BigDecimal porcentajeParticipacionTMP = participacionCorredorContrato.getParticipacionFacultativoDTO().getPorcentajeParticipacion().divide(new BigDecimal(100))
											.divide(participacionCorredorContrato.getPorcentajeParticipacion().divide(new BigDecimal(100)));
					porcentajeParticipacion = porcentajeParticipacion.add(porcentajeParticipacionTMP);
					//En participacion por corredor no se maneja comision
					porcentajeComision = porcentajeComision.add(BigDecimal.ZERO);
					
					//Calculo de la prima al reasegurador aplicando el % de participacion
					BigDecimal primaParticipacionTMP = prima.multiply(porcentajeParticipacionTMP);
					montoPrimaParticipacion = montoPrimaParticipacion.add( primaParticipacionTMP );
					
					//En participaciones por corredor no se maneja comision
					BigDecimal comisionParticipacionTMP = BigDecimal.ZERO;
					montoComision = montoComision.add(comisionParticipacionTMP);
					
					//Calculo de la prima a pagar restando la comision de la prima participaci�n
					BigDecimal primaAPagarTMP = primaParticipacionTMP.subtract(comisionParticipacionTMP);
					
					montoPrimaAPagar = montoPrimaAPagar.add(primaAPagarTMP);
				}
			}
		}//Fin inclusi�n de participaciones con corredor
		
		if(contratoFacultativoEnCurso != null){
			reporteNegocioFacultativo.setMontoPrimaFacultada(montoPrima);
			reporteNegocioFacultativo.setPorcentajeParticipacion(porcentajeParticipacion.divide(new BigDecimal(cantidadParticipaciones)));
			reporteNegocioFacultativo.setPorcentajeComision(porcentajeComision.divide(new BigDecimal(cantidadParticipaciones)));
			reporteNegocioFacultativo.setMontoPrimaParticipacion(montoPrimaParticipacion);
			reporteNegocioFacultativo.setMontoComision(montoComision);
			reporteNegocioFacultativo.setMontoPrimaAPagar(montoPrimaAPagar);
		}
		
		return contratoFacultativoEnCurso;
	}
	
	@SuppressWarnings("unchecked")
	private List<ParticipacionCorredorFacultativoDTO> obtenerParticipacionesFacultativasCorredor(BigDecimal idTcReasegurador,BigDecimal idToCotizacion){
		String queryStringBusquedaCorredor = "select model " +
			"from ParticipacionCorredorFacultativoDTO model where " +
			"model.participacionFacultativoDTO.detalleContratoFacultativoDTO.idToCotizacion = :idToCotizacion and " +
			"model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";
		Query query = null;
		query = entityManager.createQuery(queryStringBusquedaCorredor);
		query.setParameter("idToCotizacion", idToCotizacion);
		query.setParameter("idtcreaseguradorcorredor", idTcReasegurador);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private List<ParticipacionFacultativoDTO> obtenerParticipacionesFacultativas(BigDecimal idTcReasegurador,BigDecimal idToCotizacion){
		String queryStringBusquedaReasegurador = "select model from ParticipacionFacultativoDTO model where " +
			"model.detalleContratoFacultativoDTO.idToCotizacion = :idToCotizacion and model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";
		Query query = null;
		query = entityManager.createQuery(queryStringBusquedaReasegurador);
		query.setParameter("idToCotizacion", idToCotizacion);
		query.setParameter("idtcreaseguradorcorredor", idTcReasegurador);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);

		return query.getResultList();
	}
	
	private EndosoDTO obtenerEndosoDTO(BigDecimal idToCotizacion,Map<BigDecimal,EndosoDTO> mapaEndosos){
		EndosoDTO endoso = mapaEndosos.get(idToCotizacion);
		if(endoso == null){
			endoso = endosoFacade.buscarPorCotizacion(idToCotizacion);
			mapaEndosos.put(idToCotizacion, endoso);
		}
		return endoso;
	}
	
	private BigDecimal consultarPorcentajeFacultativoPorContratoFacultativo(Map<BigDecimal,BigDecimal> mapaPorcentajesFacultativosPorLineaReas,BigDecimal idTmContratoFacultativo){
		if(!mapaPorcentajesFacultativosPorLineaReas.containsKey(idTmContratoFacultativo)){
			String queryString = "select model.disPrimaPorcentajeFacultativo from LineaSoporteReaseguroDTO model where " +
					"model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTmContratoFacultativo", idTmContratoFacultativo);
			Object resultado = query.getSingleResult();
			BigDecimal porcentaje = Utilerias.obtenerBigDecimal(resultado);
			mapaPorcentajesFacultativosPorLineaReas.put(idTmContratoFacultativo, porcentaje);
		}
		return mapaPorcentajesFacultativosPorLineaReas.get(idTmContratoFacultativo);
	}
	
	private void agruparParticipacionesPorContratoFacultativo(List<BigDecimal> listaIdTmContratos,
			List<ParticipacionFacultativoDTO> listaDetallesParticipacion,
			Map<BigDecimal,List<ParticipacionFacultativoDTO>> mapaParticipacionesPorContrato,boolean excluirSaldoCorredor,
			List<ParticipacionCorredorFacultativoDTO> listaDetallesParticipacionCorredor,
			Map<BigDecimal,List<ParticipacionCorredorFacultativoDTO>> mapaParticipacionesCorredorPorContrato){
		if (( listaDetallesParticipacion != null && !listaDetallesParticipacion.isEmpty()) ){
			//Agrupaci�n de las participaciones por contrato facultativo
			for(ParticipacionFacultativoDTO participacionTMP : listaDetallesParticipacion){
				BigDecimal idTmContratoFacultativoEnCurso = participacionTMP.
										getDetalleContratoFacultativoDTO().getContratoFacultativoDTO_1().getIdTmContratoFacultativo();
				if(!mapaParticipacionesPorContrato.containsKey(idTmContratoFacultativoEnCurso)){
					mapaParticipacionesPorContrato.put(idTmContratoFacultativoEnCurso, new ArrayList<ParticipacionFacultativoDTO>());
				}
				mapaParticipacionesPorContrato.get(idTmContratoFacultativoEnCurso).add(participacionTMP);
				if(!listaIdTmContratos.contains(idTmContratoFacultativoEnCurso))
					listaIdTmContratos.add(idTmContratoFacultativoEnCurso);
			}
		}//Fin listaDetallesParticipacion != null
			
		if(!excluirSaldoCorredor && listaDetallesParticipacionCorredor != null && !listaDetallesParticipacionCorredor.isEmpty()){
			//Agrupaci�n de las participaciones bajo corredor por contrato facultativo
			for(ParticipacionCorredorFacultativoDTO participacionCorredorTMP : listaDetallesParticipacionCorredor){
				BigDecimal idTmContratoFacultativoEnCurso = participacionCorredorTMP.getParticipacionFacultativoDTO().
										getDetalleContratoFacultativoDTO().getContratoFacultativoDTO_1().getIdTmContratoFacultativo();
				if(!mapaParticipacionesCorredorPorContrato.containsKey(idTmContratoFacultativoEnCurso)){
					mapaParticipacionesCorredorPorContrato.put(idTmContratoFacultativoEnCurso, new ArrayList<ParticipacionCorredorFacultativoDTO>());
				}
				mapaParticipacionesCorredorPorContrato.get(idTmContratoFacultativoEnCurso).add(participacionCorredorTMP);
				if(!listaIdTmContratos.contains(idTmContratoFacultativoEnCurso))
					listaIdTmContratos.add(idTmContratoFacultativoEnCurso);
			}
		}
	}
}
