package mx.com.afirme.midas.contratos.estadocuenta;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.estadocuenta.presentacion.SaldoAgrupacionDTO;
import mx.com.afirme.midas.contratos.linea.CalculoSuscripcionesDTO;
import mx.com.afirme.midas.contratos.linea.SuscripcionDTO;
import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;

public class UtileriasCalculoEstadoCuenta {
	
	public static SaldoConceptoDTO obtenerSaldosAcumulados(List<SaldoConceptoDTO> listaSaldosConceptos, SaldoConceptoDTO saldoAnterior){
		BigDecimal saldoAcumulado = new BigDecimal(0);
		SaldoConceptoDTO ultimoSaldoConcepto;
		SaldoConceptoDTO saldoConceptoDTO = new SaldoConceptoDTO();
		boolean sumaSaldoAnterior = false;
		if(saldoAnterior != null){
			listaSaldosConceptos.add(0,saldoAnterior);
			sumaSaldoAnterior = true;
		}
		if (listaSaldosConceptos != null && listaSaldosConceptos.size() > 0){
			for (SaldoConceptoDTO saldoConceptoDTO2 : listaSaldosConceptos) {
				if((saldoConceptoDTO2.getDescripcionConcepto() != null && saldoConceptoDTO2.getDescripcionConcepto().startsWith("Saldo Anterior") ) || sumaSaldoAnterior){
					saldoAcumulado = saldoAcumulado.add(saldoConceptoDTO2.getSaldoAcumulado());
					sumaSaldoAnterior = false;
				}
				else{
					saldoAcumulado = saldoAcumulado.add(saldoConceptoDTO2.getSaldo());
				}
				saldoConceptoDTO2.setSaldoAcumulado(saldoAcumulado);
			}
			ultimoSaldoConcepto = listaSaldosConceptos.get(listaSaldosConceptos.size() - 1);
			saldoConceptoDTO.setDescripcionConcepto("Saldo T�cnico");
			saldoConceptoDTO.setSaldo(ultimoSaldoConcepto.getSaldo());
			saldoConceptoDTO.setSaldoAcumulado(ultimoSaldoConcepto.getSaldoAcumulado());
		}
		
		return saldoConceptoDTO;
	}
	
	public static SaldoAgrupacionDTO obtenerSaldoPorAgrupacion(List<SaldoConceptoDTO> listaSaldosConceptos){
		BigDecimal saldoAcumulado = BigDecimal.ZERO;
		BigDecimal totalDebe = BigDecimal.ZERO;
		BigDecimal totalHaber= BigDecimal.ZERO;
		if (listaSaldosConceptos != null && listaSaldosConceptos.size() > 0){
			for (SaldoConceptoDTO saldoConceptoDTO2 : listaSaldosConceptos) {
				saldoAcumulado = saldoAcumulado.add(saldoConceptoDTO2.getSaldo());
				totalDebe = totalDebe.add(saldoConceptoDTO2.getDebe() != null ? saldoConceptoDTO2.getDebe() : BigDecimal.ZERO);
				totalHaber = totalHaber.add(saldoConceptoDTO2.getHaber() != null ? saldoConceptoDTO2.getHaber() : BigDecimal.ZERO);
			}
		}
		SaldoAgrupacionDTO saldoConceptoDTO = new SaldoAgrupacionDTO("SubTotal","",saldoAcumulado,BigDecimal.ZERO,totalDebe,totalHaber,saldoAcumulado);
		saldoConceptoDTO.setResaltarConcepto(true);
		return saldoConceptoDTO;
	}
	
	public static EstadoCuentaDTO obtenerUltimoEstadoCuenta(EstadoCuentaDTO estadoCuentaDTO){
		if(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior() != null)
			return obtenerUltimoEstadoCuenta(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior());
		else
			return estadoCuentaDTO;
	}
	
	public static EstadoCuentaDTO duplicarEstadoCuenta(EstadoCuentaDTO estadoCuentaOriginal,ContratoCuotaParteDTO contratoCP,ContratoPrimerExcedenteDTO contrato1E,int anioContrato,int suscripcion){
		EstadoCuentaDTO estadoCuentaDTO = null;
		if(estadoCuentaOriginal != null){
			estadoCuentaDTO = estadoCuentaOriginal.duplicate();
			estadoCuentaDTO.setContratoCuotaParteDTO(contratoCP);
			estadoCuentaDTO.setContratoPrimerExcedenteDTO(contrato1E);
			estadoCuentaDTO.setAcumuladorDTOs(new ArrayList<AcumuladorDTO>());
			estadoCuentaDTO.setEjercicio(anioContrato);
			estadoCuentaDTO.setSuscripcion(suscripcion);
			estadoCuentaDTO.setIdEstadoCuenta(null);
			estadoCuentaDTO.setEstadoCuentaSuscripcionAnterior(null);
		}
		else{
			estadoCuentaDTO = new EstadoCuentaDTO();
			estadoCuentaDTO.setAcumuladorDTOs(new ArrayList<AcumuladorDTO>());
			if(contratoCP != null){
				estadoCuentaDTO.setContratoCuotaParteDTO(contratoCP);
				estadoCuentaDTO.setIdMoneda(contratoCP.getIdTcMoneda().intValue());
				estadoCuentaDTO.setTipoReaseguro(TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE);
			}
			else if(contrato1E != null){
				estadoCuentaDTO.setContratoPrimerExcedenteDTO(contrato1E);
				estadoCuentaDTO.setIdMoneda(contrato1E.getIdMoneda().intValue());
				estadoCuentaDTO.setTipoReaseguro(TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE);
			}
			estadoCuentaDTO.setEjercicio(anioContrato);
			estadoCuentaDTO.setSuscripcion(suscripcion);
		}
		return estadoCuentaDTO;
	}
	
	public static EstadoCuentaDTO obtenerPrimerEstadoCuentaConContratoYReasegurador(EstadoCuentaDTO estadoCuentaDTO){
		//Si el estado de cuenta tiene id tipo reaseguro, contrato y reasegurador o corredor
		if (estadoCuentaDTO.getIdEstadoCuenta() != null && 
				estadoCuentaDTO.getIdEstadoCuenta().compareTo(BigDecimal.ZERO) > 0){
			return estadoCuentaDTO;
		}
		else if(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior() != null)
			return obtenerPrimerEstadoCuentaConContratoYReasegurador(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior());
		else
			return null;
	}
	
	public static void copiarDatosEstadoCuenta(EstadoCuentaDTO estadoCuentaOrigen,EstadoCuentaDTO estadoCuentaDestino){
		estadoCuentaDestino.setContratoCuotaParteDTO(estadoCuentaOrigen.getContratoCuotaParteDTO());
		estadoCuentaDestino.setContratoFacultativoDTO(estadoCuentaOrigen.getContratoFacultativoDTO());
		estadoCuentaDestino.setContratoPrimerExcedenteDTO(estadoCuentaOrigen.getContratoPrimerExcedenteDTO());
		estadoCuentaDestino.setCorredorDTO(estadoCuentaOrigen.getCorredorDTO());
		estadoCuentaDestino.setEjercicio(estadoCuentaOrigen.getEjercicio());
		estadoCuentaDestino.setIdEstadoCuenta(BigDecimal.ZERO);
		estadoCuentaDestino.setIdMoneda(estadoCuentaOrigen.getIdMoneda());
		estadoCuentaDestino.setLineaDTO(estadoCuentaOrigen.getLineaDTO());
		estadoCuentaDestino.setReaseguradorCorredorDTO(estadoCuentaOrigen.getReaseguradorCorredorDTO());
		estadoCuentaDestino.setSubRamoDTO(estadoCuentaOrigen.getSubRamoDTO());
		estadoCuentaDestino.setTipoReaseguro(estadoCuentaOrigen.getTipoReaseguro());
	}
	
	public static List<SaldoConceptoDTO> obtenerSaldosAgrupadosPorEjercicio(List<EstadoCuentaDecoradoDTO> listaEstadoCuentaEjerciciosAnteriores,List<ConceptoAcumuladorDTO> listaConceptosAcumulador){
		List<SaldoConceptoDTO> listaSaldosCombinados = new ArrayList<SaldoConceptoDTO>();
		String descripcionPeriodo = null;
		
		ConceptoAcumuladorDTO conceptoPrimaAnterior = new ConceptoAcumuladorDTO();
		conceptoPrimaAnterior.setConceptoMovimientoDTO(new ConceptoMovimientoDTO());
		conceptoPrimaAnterior.getConceptoMovimientoDTO().setDescripcion("Saldo Anterior");
		listaConceptosAcumulador.add(0, conceptoPrimaAnterior);
		//iterar los diferentes conceptos
		for(ConceptoAcumuladorDTO conceptoTMP : listaConceptosAcumulador){
			SaldoConceptoDTO saldoConceptoEnCurso = null;
			//cada concepto se buscar� en los diferentes estados de cuenta 
			for(EstadoCuentaDecoradoDTO estadoCuentaEnCurso : listaEstadoCuentaEjerciciosAnteriores){
				descripcionPeriodo = " ("+estadoCuentaEnCurso.getEjercicio()+")";
				for(SaldoConceptoDTO saldoConceptoTMP : estadoCuentaEnCurso.getSaldosAcumulados()){
					if(saldoConceptoTMP.getDescripcionConcepto().compareTo(conceptoTMP.getConceptoMovimientoDTO().getDescripcion()) == 0){
						saldoConceptoEnCurso = saldoConceptoTMP;
						break;
					}
				}
				if(saldoConceptoEnCurso != null){
					//TODO si el concepto no trae saldo (en ceros), no agregarlo a la lista
					saldoConceptoEnCurso.setDescripcionConcepto(saldoConceptoEnCurso.getDescripcionConcepto()+descripcionPeriodo);
					listaSaldosCombinados.add(saldoConceptoEnCurso);
					saldoConceptoEnCurso = null;
				}
			}
		}
		return listaSaldosCombinados;
	}
	
	public static SuscripcionDTO obtenerSuscripcion(int ejercicio,int suscripcion){
		Calendar calendarInicio = Calendar.getInstance();
		calendarInicio.set(Calendar.YEAR, ejercicio);
		calendarInicio.set(Calendar.MONTH, Calendar.APRIL);
		calendarInicio.set(Calendar.DATE, 1);
		Calendar calendarFin = Calendar.getInstance();
		calendarFin.set(Calendar.YEAR, ejercicio+1);
		calendarFin.set(Calendar.MONTH,Calendar.APRIL);
		calendarFin.set(Calendar.DATE, 1);
		calendarFin.add(Calendar.DATE, -1);
		CalculoSuscripcionesDTO calculo = new CalculoSuscripcionesDTO(ejercicio, calendarInicio.getTime(), 
				calendarFin.getTime(), new BigDecimal(CalculoSuscripcionesDTO.FORMA_PAGO_TRIMESTRAL));
		SuscripcionDTO suscripcionDTO = calculo.getListaSuscripciones().get(suscripcion-1);
		
		return suscripcionDTO;
	}
	
	public static SaldoConceptoDTO obtenerSaldoPorConcepto(Integer idTcConfigConcepto,EstadoCuentaDecoradoDTO estadoCuentaDecorado){
		SaldoConceptoDTO saldoConceptoEncontrado = null;
		for(SaldoConceptoDTO saldoConceptoTMP : estadoCuentaDecorado.getSaldosAcumulados()){
			if(saldoConceptoTMP.getIdTcConfiguracionSaldoConcepto() != null && idTcConfigConcepto != null &&
					saldoConceptoTMP.getIdTcConfiguracionSaldoConcepto().intValue() == idTcConfigConcepto.intValue()){
				saldoConceptoEncontrado = saldoConceptoTMP;
				break;
			}
		}
		return saldoConceptoEncontrado;
	}
}
