package mx.com.afirme.midas.contratos.estadocuenta.presentacion;
// default package

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity DetalleConfiguracionEstadoCuentaDTO.
 * @see .DetalleConfiguracionEstadoCuentaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DetalleConfiguracionEstadoCuentaFacade  implements DetalleConfiguracionEstadoCuentaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DetalleConfiguracionEstadoCuentaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DetalleConfiguracionEstadoCuentaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DetalleConfiguracionEstadoCuentaDTO entity) {
    				LogUtil.log("saving DetalleConfiguracionEstadoCuentaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DetalleConfiguracionEstadoCuentaDTO entity.
	  @param entity DetalleConfiguracionEstadoCuentaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DetalleConfiguracionEstadoCuentaDTO entity) {
    				LogUtil.log("deleting DetalleConfiguracionEstadoCuentaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DetalleConfiguracionEstadoCuentaDTO.class, entity.getIdTcDetalleConfigEstadoCuenta());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DetalleConfiguracionEstadoCuentaDTO entity and return it or a copy of it to the sender. 
	 A copy of the DetalleConfiguracionEstadoCuentaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DetalleConfiguracionEstadoCuentaDTO entity to update
	 @return DetalleConfiguracionEstadoCuentaDTO the persisted DetalleConfiguracionEstadoCuentaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DetalleConfiguracionEstadoCuentaDTO update(DetalleConfiguracionEstadoCuentaDTO entity) {
    				LogUtil.log("updating DetalleConfiguracionEstadoCuentaDTO instance", Level.INFO, null);
	        try {
            DetalleConfiguracionEstadoCuentaDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DetalleConfiguracionEstadoCuentaDTO findById( Integer id) {
    				LogUtil.log("finding DetalleConfiguracionEstadoCuentaDTO instance with id: " + id, Level.INFO, null);
	        try {
            DetalleConfiguracionEstadoCuentaDTO instance = entityManager.find(DetalleConfiguracionEstadoCuentaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DetalleConfiguracionEstadoCuentaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DetalleConfiguracionEstadoCuentaDTO property to query
	  @param value the property value to match
	  	  @return List<DetalleConfiguracionEstadoCuentaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DetalleConfiguracionEstadoCuentaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding DetalleConfiguracionEstadoCuentaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DetalleConfiguracionEstadoCuentaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all DetalleConfiguracionEstadoCuentaDTO entities.
	  	  @return List<DetalleConfiguracionEstadoCuentaDTO> all DetalleConfiguracionEstadoCuentaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DetalleConfiguracionEstadoCuentaDTO> findAll(
		) {
					LogUtil.log("finding all DetalleConfiguracionEstadoCuentaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from DetalleConfiguracionEstadoCuentaDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}