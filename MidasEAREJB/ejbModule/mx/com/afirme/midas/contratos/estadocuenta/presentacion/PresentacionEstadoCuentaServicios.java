package mx.com.afirme.midas.contratos.estadocuenta.presentacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorFacadeRemote;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteFacadeRemote;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteFacadeRemote;
import mx.com.afirme.midas.contratos.estadocuenta.AcumuladorDTO;
import mx.com.afirme.midas.contratos.estadocuenta.ConceptoAcumuladorDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaFacadeRemote;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaFiltroDTO;
import mx.com.afirme.midas.contratos.estadocuenta.SaldoConceptoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.UtileriasCalculoEstadoCuenta;
import mx.com.afirme.midas.contratos.estadocuenta.reportes.ReporteEstadoCuentaServiciosRemote;
import mx.com.afirme.midas.contratos.estadocuenta.reportes.ReporteSaldoPorSubRamoDTO;
import mx.com.afirme.midas.contratos.linea.CalculoSuscripcionesDTO;
import mx.com.afirme.midas.contratos.linea.EjercicioDTO;
import mx.com.afirme.midas.contratos.linea.LineaFacadeRemote;
import mx.com.afirme.midas.contratos.linea.SuscripcionDTO;
import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoMovimientoDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class PresentacionEstadoCuentaServicios implements PresentacionEstadoCuentaServiciosRemote{
//	private static final boolean CONSULTAR_ACUMULADORES_EDO_CTA = false; 
	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private EstadoCuentaFacadeRemote estadoCuentaFacade;
	
	@EJB
	private AgrupacionEstadoCuentaFacadeRemote agrupacionEstadoCuentaFacade;
	
	@EJB
	private ReaseguradorCorredorFacadeRemote reaseguradorCorredorFacade;
	
	@EJB
	private ContratoCuotaParteFacadeRemote contratoCuotaParteFacade;
	
	@EJB
	private ContratoPrimerExcedenteFacadeRemote contratoPrimerExcedenteFacade;

	/**
	 * Consulta un estado de cuenta, correspondiente a una suscripci�n, y los estados de cuenta de suscripciones anteriores, adem�s consulta el estado 
	 * de cuenta de los contratos cuota parte � primer excedente de a�os contractuales anteriores y combina los saldos en un estado de cuenta �nico.
	 * @param idToEstadoCuenta
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaHistoricoCombinado(BigDecimal idToEstadoCuenta,short claveFormatoEstadoCuenta){
		//Buscar el estado de cuenta con detalle hist�rico
		EstadoCuentaDTO estadoCuentaDTO = estadoCuentaFacade.obtenerEstadoCuentaHistorico(idToEstadoCuenta);
		List<EstadoCuentaDTO> listaEstadoCuentaContratosAnteriores = new ArrayList<EstadoCuentaDTO>();
		
		if(estadoCuentaDTO != null){
			listaEstadoCuentaContratosAnteriores = consultarEstadosCuentaContratosAnteriores(estadoCuentaDTO);
		}
		
		listaEstadoCuentaContratosAnteriores.add(0, estadoCuentaDTO);
		List<AgrupacionEstadoCuentaDTO> listaAgrupaciones = agrupacionEstadoCuentaFacade.obtenerAgrupacionesPorEstadoCuenta(estadoCuentaDTO, claveFormatoEstadoCuenta);
		
		EstadoCuentaDecoradoDTO estadoCuentaDecorado = calcularSaldoCombinadoEstadosCuenta(
				listaEstadoCuentaContratosAnteriores, listaAgrupaciones, false, null, false, null, null, false, null, null, true, false,false);
		
//		estadoCuentaDecorado.setReaseguradorCorredorDTO(estadoCuentaDTO.getReaseguradorCorredorDTO());
		estadoCuentaDecorado.setEstadoCuentaDTO(estadoCuentaDTO);
		
		return estadoCuentaDecorado;
	}
	
	@EJB
	ReporteEstadoCuentaServiciosRemote reporteEstadoCuentaServicios;
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<EstadoCuentaDecoradoDTO> obtenerEstadoCuentaFacultativoPorSuscripcion(BigDecimal idTcReasegurador,BigDecimal idMoneda,int ejercicio, int suscripcion){
		ReaseguradorCorredorDTO reasegurador = null;
		List<EstadoCuentaDecoradoDTO> listaEstadoCuentaEncontrados = null;
		if(idTcReasegurador != null && idMoneda != null){
			//Busqueda del reasegurador
			reasegurador = entityManager.find(ReaseguradorCorredorDTO.class, idTcReasegurador);
			listaEstadoCuentaEncontrados = new ArrayList<EstadoCuentaDecoradoDTO>();
			if(reasegurador != null){
				Integer[] tipoReaseguro = {TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO};
				//Consulta de los subramos facultados en los que ha participado el reasegurador
				List<ReporteSaldoPorSubRamoDTO> listaSubRamos = reporteEstadoCuentaServicios.obtenerSubRamosAfectados(reasegurador, tipoReaseguro, idMoneda, true);
				SuscripcionDTO suscripcionDTO = UtileriasCalculoEstadoCuenta.obtenerSuscripcion(ejercicio, suscripcion);
				if(suscripcionDTO != null){
//					List<Date> listaMesesIncluidos = suscripcionDTO.obtenerMesesIncluidos();
					for(ReporteSaldoPorSubRamoDTO subRamoAfectado : listaSubRamos){
						EstadoCuentaDecoradoDTO estadoCuentaTMP = null;//obtenerEstadoCuentaFacultativoPorSuscripcionInterno(reasegurador, idMoneda, subRamoAfectado.getIdTcSubRamo(), suscripcionDTO, listaMesesIncluidos,null);
						estadoCuentaTMP = obtenerEstadoCuentaFacultativoHistorico(reasegurador, idMoneda, subRamoAfectado.getIdTcSubRamo(), suscripcionDTO, null);
						if(estadoCuentaTMP != null)
							listaEstadoCuentaEncontrados.add(estadoCuentaTMP);
					}
				}
			}
		}
		return listaEstadoCuentaEncontrados;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaFacultativoPorSuscripcion(BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal idTcSubRamo,int ejercicio, int suscripcion){
		ReaseguradorCorredorDTO reasegurador = null;
		EstadoCuentaDecoradoDTO estadoCuentaEncontrado = null;
		if(idTcReasegurador != null && idMoneda != null){
			//Busqueda del reasegurador
			reasegurador = entityManager.find(ReaseguradorCorredorDTO.class, idTcReasegurador);
			if(reasegurador != null){
				SuscripcionDTO suscripcionDTO = UtileriasCalculoEstadoCuenta.obtenerSuscripcion(ejercicio, suscripcion);
				if(suscripcionDTO != null){
//					List<Date> listaMesesIncluidos = suscripcionDTO.obtenerMesesIncluidos();
//					estadoCuentaEncontrado =  obtenerEstadoCuentaFacultativoPorSuscripcionInterno(reasegurador, idMoneda, idTcSubRamo, suscripcionDTO, listaMesesIncluidos,null);
					estadoCuentaEncontrado = obtenerEstadoCuentaFacultativoHistorico(reasegurador, idMoneda, idTcSubRamo, suscripcionDTO, null);
				}
			}
		}
		return estadoCuentaEncontrado;
	}
	
	@EJB
	LineaFacadeRemote lineaFacade;
	
	/**
	 * TODO este metodo deber� consultar el estado de cuenta facultativo por trimestre hacia atras en suscripciones
	 * @param reasegurador
	 * @param idMoneda
	 * @param idTcSubRamo
	 * @param suscripcionDTO
	 * @return
	 */
	private EstadoCuentaDecoradoDTO obtenerEstadoCuentaFacultativoHistorico(ReaseguradorCorredorDTO reasegurador,BigDecimal idMoneda,BigDecimal idTcSubRamo,SuscripcionDTO suscripcionDTO,Integer primerEjercicio){
		if(primerEjercicio == null){
			List<EjercicioDTO> listaEjercicios = lineaFacade.obtenerEjercicios();
			if (listaEjercicios != null && !listaEjercicios.isEmpty())
				primerEjercicio = listaEjercicios.get(0).getAnio();
			else
				primerEjercicio = 2009;
		}
		if(suscripcionDTO.getIdSuscripcion() == 1 && suscripcionDTO.getEjercicio() == primerEjercicio){
			EstadoCuentaDecoradoDTO estadoCuentaEncontrados = null;
			if(reasegurador != null && reasegurador.getIdtcreaseguradorcorredor() != null && idMoneda != null){
				estadoCuentaEncontrados = obtenerEstadoCuentaFacultativoPorSuscripcionInterno(reasegurador, idMoneda, idTcSubRamo, suscripcionDTO, suscripcionDTO.obtenerMesesIncluidos(), null);
			}
			return estadoCuentaEncontrados;
		}
		else{
			SuscripcionDTO suscripcionAnterior = null;
			if(suscripcionDTO.getIdSuscripcion() == 1){
				suscripcionAnterior = UtileriasCalculoEstadoCuenta.obtenerSuscripcion(suscripcionDTO.getEjercicio() -1, 4);
			}
			else{
				suscripcionAnterior = UtileriasCalculoEstadoCuenta.obtenerSuscripcion(suscripcionDTO.getEjercicio(), suscripcionDTO.getIdSuscripcion()-1);
			}
			EstadoCuentaDecoradoDTO estadoCuentaSuscripcionAnterior = obtenerEstadoCuentaFacultativoHistorico(reasegurador, idMoneda, idTcSubRamo, suscripcionAnterior, primerEjercicio);
			
			SaldoConceptoDTO saldoTecnicoAnterior = null;
			if(estadoCuentaSuscripcionAnterior != null && estadoCuentaSuscripcionAnterior.getSaldoTecnico() != null){
				saldoTecnicoAnterior = estadoCuentaSuscripcionAnterior.getSaldoTecnico().duplicate();
				saldoTecnicoAnterior.setDescripcionConcepto("Saldo Anterior");
				saldoTecnicoAnterior.setFechaTrimestre(suscripcionAnterior.getDescripcion());
				if(saldoTecnicoAnterior.getSaldoAcumulado() != null && saldoTecnicoAnterior.getSaldoAcumulado().compareTo(BigDecimal.ZERO) > 0){
					saldoTecnicoAnterior.setHaber(saldoTecnicoAnterior.getSaldoAcumulado());
				}else if (saldoTecnicoAnterior.getSaldoAcumulado() != null && saldoTecnicoAnterior.getSaldoAcumulado().compareTo(BigDecimal.ZERO) < 0){
					saldoTecnicoAnterior.setDebe(saldoTecnicoAnterior.getSaldoAcumulado().abs());
				}
			}
			
			EstadoCuentaDecoradoDTO estadoCuentaSuscripcionEnCurso = obtenerEstadoCuentaFacultativoPorSuscripcionInterno(reasegurador, idMoneda, idTcSubRamo, 
					suscripcionDTO, suscripcionDTO.obtenerMesesIncluidos(), saldoTecnicoAnterior);
			
			if(estadoCuentaSuscripcionEnCurso != null){
				estadoCuentaSuscripcionEnCurso.setSaldoTecnicoAnterior(saldoTecnicoAnterior);
			}
			
			return estadoCuentaSuscripcionEnCurso;
		}
	}
	
	private EstadoCuentaDecoradoDTO obtenerEstadoCuentaFacultativoPorSuscripcionInterno(ReaseguradorCorredorDTO reasegurador,BigDecimal idMoneda,BigDecimal idTcSubRamo,SuscripcionDTO suscripcionDTO,List<Date> listaMesesIncluidos,SaldoConceptoDTO saldoAnterior){
		EstadoCuentaDecoradoDTO estadoCuentaEncontrados = null;
		if(reasegurador != null && reasegurador.getIdtcreaseguradorcorredor() != null && idMoneda != null){
			if(listaMesesIncluidos != null){
				//Por cada subramo se consulta el estado de cuenta incluyendo un filtro por los meses que componen el trimestre.
				EstadoCuentaDecoradoDTO estadoCuentaTMP = obtenerEstadoCuentaCombinadoConSaldosCalculados(null, reasegurador.getIdtcreaseguradorcorredor(),
						idMoneda, idTcSubRamo, true, listaMesesIncluidos, false, null, null, false,
						AgrupacionEstadoCuentaDTO.CLAVE_FORMATO_FACULTATIVO_CONSULTA_DESDE_AUTOMATICOS_COMPLETO);
				if(estadoCuentaTMP != null && estadoCuentaTMP.getSaldosAcumuladosPorMes() != null &&
						!estadoCuentaTMP.getSaldosAcumuladosPorMes().isEmpty() &&
						estadoCuentaTMP.getListaSaldosAcumuladosPorMes() != null && !estadoCuentaTMP.getListaSaldosAcumuladosPorMes().isEmpty()){
					List<SaldoConceptoDTO> listaSaldosPeriodo = new ArrayList<SaldoConceptoDTO>();
					SaldoConceptoDTO saldoTecnico = null;
					
					for(List<SaldoConceptoDTO> listaMensualTMP : estadoCuentaTMP.getListaSaldosAcumuladosPorMes()){
						listaSaldosPeriodo.addAll(listaMensualTMP);
					}
					
					List<SaldoConceptoDTO> listaSaldosCombinados = new ArrayList<SaldoConceptoDTO>();
					
					for(SaldoConceptoDTO saldoConceptoTMP : estadoCuentaTMP.getListaSaldosAcumuladosPorMes().get(0)){
						SaldoConceptoDTO saldoConceptoCombinado = obtenerSaldoPorConcepto(listaSaldosPeriodo, saldoConceptoTMP);
						saldoConceptoCombinado.setMostrarSaldoAcumulado(saldoConceptoTMP.isMostrarSaldoAcumulado());
						listaSaldosCombinados.add(saldoConceptoCombinado);
					}
							//Si es un estado de cuenta v�lido, se genera el saldo t�cnico por todos los meses y se guarda para ser enviado al cliente
					saldoTecnico = UtileriasCalculoEstadoCuenta.obtenerSaldosAcumulados(listaSaldosCombinados, saldoAnterior);
					if(saldoTecnico != null && saldoTecnico.getSaldoAcumulado() != null && saldoTecnico.getSaldoAcumulado().compareTo(BigDecimal.ZERO) != 0){
						estadoCuentaTMP.setSaldosAcumulados(listaSaldosCombinados);
						estadoCuentaTMP.setSaldoTecnico(saldoTecnico);
						estadoCuentaTMP.setSubRamoDTO(entityManager.find(SubRamoDTO.class, idTcSubRamo));
						estadoCuentaTMP.setTipoReaseguro(TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);
						estadoCuentaTMP.setSuscripcionDTOCalculada(suscripcionDTO);
						estadoCuentaTMP.setReaseguradorCorredorDTO(reasegurador);
						estadoCuentaTMP.setIdMoneda(idMoneda.intValue());
						
						if(estadoCuentaTMP.getEstadoCuentaDTO() == null)
							estadoCuentaTMP.setEstadoCuentaDTO(new EstadoCuentaDTO());
						estadoCuentaTMP.getEstadoCuentaDTO().setIdEstadoCuenta(BigDecimal.ZERO);
						
						estadoCuentaEncontrados = estadoCuentaTMP;
					}
					
				}
			}
		}
		return estadoCuentaEncontrados;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<EstadoCuentaDecoradoDTO> obtenerEstadosCuentaSeparados(BigDecimal idTcSubRamo, BigDecimal idTcReasegurador,
			BigDecimal idMoneda,int idTcTipoReaseguro,int ejercicio, int suscripcion,short claveFormatoEstadoCuenta,
			boolean desglosarSaldoPorMes,short claveFormatoEstadoCuentaDesglosadoPorMes){
		
		List<EstadoCuentaDecoradoDTO> listaResultanteEstadosCuentaDecorados = new ArrayList<EstadoCuentaDecoradoDTO>();
		List<EstadoCuentaDTO> listaResultanteEstadosCuenta = new ArrayList<EstadoCuentaDTO>();
		
		//1. Buscar el estado de cuenta del ejercicio y suscripci�n recibidos, con detalle hist�rico
		EstadoCuentaDTO estadoCuentaDTO = obtenerEstadoCuentaHistorico(idTcReasegurador, idTcSubRamo, null, null, idTcTipoReaseguro, idMoneda.intValue(), ejercicio, suscripcion);
		
		if(estadoCuentaDTO != null){
		
			listaResultanteEstadosCuenta.add(estadoCuentaDTO);
			
			EstadoCuentaDTO estadoCuentaTMP = estadoCuentaDTO;
			
			List<EstadoCuentaDTO> listaEstadoCuentaContratosAnteriores = new ArrayList<EstadoCuentaDTO>();
			
			//2. Consultar los estados de cuenta de contratos de ejercicios anteriores correspondientes a la suscripci�n consultada
			do{
				if(estadoCuentaTMP.getIdEstadoCuenta() != null &&
						estadoCuentaTMP.getIdEstadoCuenta().compareTo(BigDecimal.ZERO) >0){
					listaEstadoCuentaContratosAnteriores = consultarEstadosCuentaContratosAnteriores(estadoCuentaDTO);
					break;
				}
				else
					estadoCuentaTMP = estadoCuentaTMP.getEstadoCuentaSuscripcionAnterior();
			}while(true);
			
			//3. Combinar los estados de cuenta separados en una �nica lista
			if(listaEstadoCuentaContratosAnteriores != null && !listaEstadoCuentaContratosAnteriores.isEmpty()){
				listaResultanteEstadosCuenta.addAll(listaEstadoCuentaContratosAnteriores);
			}
			
			//4. Por cada estadoCuentaDTO, se genera un EstadoCuentaDecoradoDTO usando las agrupaciones configuradas
			List<AgrupacionEstadoCuentaDTO> listaAgrupaciones = agrupacionEstadoCuentaFacade.obtenerAgrupacionesPorEstadoCuenta(estadoCuentaDTO, claveFormatoEstadoCuenta);
			List<AgrupacionEstadoCuentaDTO> listaAgrupacionesDesgloce = null;
			if(desglosarSaldoPorMes)
				listaAgrupacionesDesgloce = agrupacionEstadoCuentaFacade.obtenerAgrupacionesPorEstadoCuenta(estadoCuentaDTO, claveFormatoEstadoCuentaDesglosadoPorMes);
			EstadoCuentaDecoradoDTO estadoCuentaDecoradoTMP = null;
			for(EstadoCuentaDTO estadoCuentaDTOResultante : listaResultanteEstadosCuenta){
				//Si se solicita el saldo desglozado, se parametriza el m�todode calcular saldos
				estadoCuentaDecoradoTMP = obtenerEstadoCuentaConSaldosCalculados(estadoCuentaDTOResultante, listaAgrupaciones, 
						false,null,null,desglosarSaldoPorMes, null,listaAgrupacionesDesgloce, false, false);
//				estadoCuentaDecoradoTMP.setEstadoCuentaDTO(estadoCuentaDTOResultante);
				
				if(estadoCuentaDecoradoTMP.getListaSaldosAcumuladosPorMes() == null || estadoCuentaDecoradoTMP.getListaSaldosAcumuladosPorMes().isEmpty() ||
						estadoCuentaDecoradoTMP.getSaldosAcumuladosPorMes() == null || estadoCuentaDecoradoTMP.getSaldosAcumuladosPorMes().isEmpty() ||
						estadoCuentaDecoradoTMP.getSaldosAcumuladosPorMes().size() != estadoCuentaDecoradoTMP.getListaSaldosAcumuladosPorMes().size()){
					LogDeMidasEJB3.log("Estado de cuenta inconsistente: "+estadoCuentaDecoradoTMP.getIdEstadoCuenta(), Level.WARNING, null);
				}
				
				listaResultanteEstadosCuentaDecorados.add(estadoCuentaDecoradoTMP);
			}
			
		}
		
		
		
		return listaResultanteEstadosCuentaDecorados;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaConSaldosCalculados(BigDecimal idtoEstadoCuenta,short claveFormatoEstadoCuenta){
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = null;
		EstadoCuentaDTO estadoCuentaDTO = estadoCuentaFacade.obtenerEstadoCuentaHistorico(idtoEstadoCuenta);//findById(idtoEstadoCuenta);
		if(estadoCuentaDTO != null){
//			estadoCuentaDTO.setAcumuladorDTOs(estadoCuentaFacade.obtenerAcumuladoresPorIdEstadoCta(idtoEstadoCuenta));
			List<AgrupacionEstadoCuentaDTO> listaAgrupaciones = agrupacionEstadoCuentaFacade.obtenerAgrupacionesPorEstadoCuenta(estadoCuentaDTO, claveFormatoEstadoCuenta);
			estadoCuentaDecoradoDTO = obtenerEstadoCuentaConSaldosCalculados(estadoCuentaDTO, listaAgrupaciones,false,null,null,false,null,null,null,false);
		}
		return estadoCuentaDecoradoDTO;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<EstadoCuentaDecoradoDTO> obtenerEstadoCuentaCombinadoConSaldosCalculados(BigDecimal idToPoliza,short claveFormatoEstadoCuenta){
		List<EstadoCuentaDTO> listaEstadoCuentaDTO = consultarEstadosCuentaFacultativosEndoso0(idToPoliza,null,null,null,false,null);

	 	List<EstadoCuentaDecoradoDTO> listaEstadosCuenta;
	 	if(!listaEstadoCuentaDTO.isEmpty()){
	 		Map<BigDecimal,List<EstadoCuentaDTO>> mapaEstadosCuentaPorSubRamo = obtenerEstadosCuentaFacultativosAgrupadosPorSubRamo(idToPoliza, listaEstadoCuentaDTO,null,null,null,true);
		 	//Una vez que se tienen todos los estados de cuenta por contrato facultativo endoso 0, se agrupan en un s�lo estado de ceunta por contrato facultativo
		 	List<AgrupacionEstadoCuentaDTO> listaAgrupaciones = agrupacionEstadoCuentaFacade.obtenerAgrupacionesPorEstadoCuenta(listaEstadoCuentaDTO.get(0), claveFormatoEstadoCuenta);
		 	listaEstadosCuenta = obtenerEstadosCuentaDecoradosPorAgrupacionIdContratoFac(mapaEstadosCuentaPorSubRamo, listaAgrupaciones,false,null,null,false,null,null);
	 	}
	 	else	listaEstadosCuenta = new ArrayList<EstadoCuentaDecoradoDTO>();
	 	
		return listaEstadosCuenta;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaCombinadoConSaldosCalculados(BigDecimal idToPoliza,BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal idTcSubRamo,
			boolean separarSaldoPorMes,List<Date> mesesPorIncluir, boolean incluirFiltradoHasta,Integer mes,Integer anio,boolean separarConceptosPorReasegurador,short claveFormatoEstadoCuenta){
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = null;
		//Consulta de los estados de cuenta facultativos de endoso 0
		if(incluirFiltradoHasta)
			incluirFiltradoHasta = mes != null && anio != null;
		List<EstadoCuentaDTO> listaEstadoCuentaDTO = consultarEstadosCuentaFacultativosEndoso0(idToPoliza,idTcReasegurador,idMoneda,idTcSubRamo,separarSaldoPorMes,mesesPorIncluir);
//		boolean incluirFiltroHasta = mes != null && anio != null;
		if(listaEstadoCuentaDTO != null && !listaEstadoCuentaDTO.isEmpty()){
			List<AgrupacionEstadoCuentaDTO> listaAgrupaciones = agrupacionEstadoCuentaFacade.obtenerAgrupacionesPorEstadoCuenta(listaEstadoCuentaDTO.get(0), claveFormatoEstadoCuenta);
			if(separarConceptosPorReasegurador){
				//Se agrupan los estados de cuenta por reasegurador
				Map<BigDecimal,List<EstadoCuentaDTO>> mapaEstadosCuentaPorReasegurador = obtenerEstadosCuentaFacultativosAgrupadosPorReasegurador(idToPoliza, listaEstadoCuentaDTO);
				
				//Se cuenta con el mapa de estados de cuenta por reasegurador, falta cada lista concentrarla en un s�lo listado de conceptos y al final sumar todo el listado de conceptos
				List<EstadoCuentaDecoradoDTO> listaEstadosCuentaFinal = new ArrayList<EstadoCuentaDecoradoDTO>();
				for(BigDecimal idReasegurador : mapaEstadosCuentaPorReasegurador.keySet()){
					List<EstadoCuentaDTO> listaEstadosCuentaDelReasegurador = mapaEstadosCuentaPorReasegurador.get(idReasegurador);
					if (listaEstadosCuentaDelReasegurador != null && !listaEstadosCuentaDelReasegurador.isEmpty()){
						EstadoCuentaDecoradoDTO estadoCuentaDecTMP = calcularSaldoCombinadoEstadosCuenta(listaEstadosCuentaDelReasegurador, 
								listaAgrupaciones,true,listaEstadosCuentaDelReasegurador.get(0).getReaseguradorCorredorDTO().getNombre(),incluirFiltradoHasta,mes,anio,separarSaldoPorMes,mesesPorIncluir,listaAgrupaciones,false,true,true);
						listaEstadosCuentaFinal.add(estadoCuentaDecTMP);
					}
				}
				estadoCuentaDecoradoDTO = combinarEstadosDeCuentaDecorados(listaEstadosCuentaFinal,listaAgrupaciones, false, null,false,separarSaldoPorMes,mesesPorIncluir,true);
			}else{
			 	List<EstadoCuentaDecoradoDTO> listaEstadosCuenta = new ArrayList<EstadoCuentaDecoradoDTO>();
			 	if(!listaEstadoCuentaDTO.isEmpty()){
			 		Map<BigDecimal,List<EstadoCuentaDTO>> mapaEstadosCuentaPorSubRamo = obtenerEstadosCuentaFacultativosAgrupadosPorSubRamo(idToPoliza, listaEstadoCuentaDTO,idTcReasegurador,idMoneda,idTcSubRamo,false);
				 	//Una vez que se tienen todos los estados de cuenta por contrato facultativo endoso 0, se agrupan en un s�lo estado de ceunta por contrato facultativo
				 	listaEstadosCuenta = obtenerEstadosCuentaDecoradosPorAgrupacionIdContratoFac(mapaEstadosCuentaPorSubRamo, listaAgrupaciones,incluirFiltradoHasta,mes,anio,separarSaldoPorMes,mesesPorIncluir,listaAgrupaciones);
			 	}
			 	
			 	estadoCuentaDecoradoDTO = combinarEstadosDeCuentaDecorados(listaEstadosCuenta,listaAgrupaciones, false, null,true,separarSaldoPorMes,mesesPorIncluir,true);
			}
		}
		
		return estadoCuentaDecoradoDTO;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaCombinadoConSaldosCalculados(BigDecimal idToPoliza,BigDecimal[] idTcSubRamo,
			BigDecimal[] idTmContratoFacultativo,Integer mes,Integer anio,
			boolean separarConceptosPorReasegurador,short claveFormatoEstadoCuenta){
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = null;
		
		if(idTcSubRamo != null && idTmContratoFacultativo != null && 
				idTcSubRamo.length == idTmContratoFacultativo.length && idTmContratoFacultativo.length > 0){
			List<EstadoCuentaDTO> listaEstadoCuentaDTO = new ArrayList<EstadoCuentaDTO>();
			//Se incluye en la lista de estados de cuenta todos los correspondientes a las combinaciones subramo-contrato facultativo recibidos.
			for(int i=0;i<idTcSubRamo.length;i++){
				listaEstadoCuentaDTO.addAll(consultarEstadosCuentaFacultativos(idToPoliza, idTcSubRamo[i], idTmContratoFacultativo[i]));
			}
			
			boolean incluirFiltroHasta = mes != null && anio != null;
	
			if(separarConceptosPorReasegurador){
				//Se agrupan los estados de cuenta por reasegurador. Busca los estados de cuenta de los endosos superiores del contrato facultativo
				Map<BigDecimal,List<EstadoCuentaDTO>> mapaEstadosCuentaPorReasegurador = obtenerEstadosCuentaFacultativosAgrupadosPorReasegurador(idToPoliza, listaEstadoCuentaDTO);
				
				//Se cuenta con el mapa de estados de cuenta por reasegurador, falta cada lista concentrarla en un s�lo listado de conceptos y al final sumar todo el listado de conceptos
				List<AgrupacionEstadoCuentaDTO> listaAgrupaciones = agrupacionEstadoCuentaFacade.obtenerAgrupacionesPorEstadoCuenta(listaEstadoCuentaDTO.get(0), claveFormatoEstadoCuenta);
				List<EstadoCuentaDecoradoDTO> listaEstadosCuentaFinal = new ArrayList<EstadoCuentaDecoradoDTO>();
				for(BigDecimal idReasegurador : mapaEstadosCuentaPorReasegurador.keySet()){
					List<EstadoCuentaDTO> listaEstadosCuentaDelReasegurador = mapaEstadosCuentaPorReasegurador.get(idReasegurador);
					if (listaEstadosCuentaDelReasegurador != null && !listaEstadosCuentaDelReasegurador.isEmpty()){
						EstadoCuentaDecoradoDTO estadoCuentaDecTMP = calcularSaldoCombinadoEstadosCuenta(listaEstadosCuentaDelReasegurador,  
								listaAgrupaciones,true,listaEstadosCuentaDelReasegurador.get(0).getReaseguradorCorredorDTO().getNombre(),incluirFiltroHasta,mes,anio,false,null,null,false,true,false);
						listaEstadosCuentaFinal.add(estadoCuentaDecTMP);
					}
				}
				estadoCuentaDecoradoDTO = combinarEstadosDeCuentaDecorados(listaEstadosCuentaFinal,listaAgrupaciones, false, null,false,false,null,true);
			}
			else{
				List<EstadoCuentaDTO> listaCompletaEstadoCuenta = new ArrayList<EstadoCuentaDTO>();
				if(listaEstadoCuentaDTO != null && !listaEstadoCuentaDTO.isEmpty()){
					for (EstadoCuentaDTO estadoCuentaDTO : listaEstadoCuentaDTO) {
						if(!listaCompletaEstadoCuenta.contains(estadoCuentaDTO)){
							listaCompletaEstadoCuenta.add(estadoCuentaDTO);
						}
				 	}
					List<AgrupacionEstadoCuentaDTO> listaAgrupaciones = agrupacionEstadoCuentaFacade.obtenerAgrupacionesPorEstadoCuenta(listaEstadoCuentaDTO.get(0), claveFormatoEstadoCuenta);
					
					estadoCuentaDecoradoDTO = calcularSaldoCombinadoEstadosCuenta(listaEstadoCuentaDTO, listaAgrupaciones,false,null,incluirFiltroHasta,mes,anio,false,null,null,null,false,false);
				}
			}
		}
		return estadoCuentaDecoradoDTO;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaPorSiniestro(BigDecimal idToReporteSiniestro,BigDecimal idMoneda,
			BigDecimal[] idTcSubRamos,Integer mes,Integer anio,boolean separarConceptosPorReasegurador,Short claveFormatoEstadoCuenta){
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = null;
		if(claveFormatoEstadoCuenta == null)
			claveFormatoEstadoCuenta = AgrupacionEstadoCuentaDTO.CLAVE_FORMATO_FACULTATIVO_POR_SINIESTRO;
		boolean incluirFiltroHasta = mes != null && anio != null;
		if(idToReporteSiniestro != null && idMoneda != null){
			
			List<AgrupacionEstadoCuentaDTO> listaAgrupaciones = agrupacionEstadoCuentaFacade.
							obtenerAgrupacionesFacultativo(claveFormatoEstadoCuenta, idMoneda.shortValue());
			
			estadoCuentaDecoradoDTO = obtenerEstadoCuentaSiniestroConSaldosCalculados(idToReporteSiniestro,idTcSubRamos,incluirFiltroHasta,mes,anio,separarConceptosPorReasegurador,listaAgrupaciones);
		}
		return estadoCuentaDecoradoDTO;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaPorReasegurador(BigDecimal idTcReasegurador,BigDecimal idMoneda,
			BigDecimal idToSiniestro,Integer mes,Integer anio,BigDecimal idTcSubRamo,Short claveFormatoEstadoCuenta){
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = null;
		boolean incluirFiltroHasta = mes != null && anio != null;
		List<AgrupacionEstadoCuentaDTO> listaAgrupaciones = null;
		if(claveFormatoEstadoCuenta == null){
			claveFormatoEstadoCuenta = AgrupacionEstadoCuentaDTO.CLAVE_FORMATO_FACULTATIVO_POR_SINIESTRO;
		}
		listaAgrupaciones = agrupacionEstadoCuentaFacade.obtenerAgrupacionesFacultativo(claveFormatoEstadoCuenta, idMoneda.shortValue());
		
		estadoCuentaDecoradoDTO = obtenerEstadoCuentaReaseguradorConSaldosCalculados(idTcReasegurador, idToSiniestro,idMoneda, idTcSubRamo, incluirFiltroHasta,false, mes, anio, listaAgrupaciones);
		
		return estadoCuentaDecoradoDTO;
	}
	
	@SuppressWarnings({ "unused" })
	private EstadoCuentaDecoradoDTO obtenerEstadoCuentaConSaldosCalculados(EstadoCuentaDTO estadoCuentaDTO,List<ConceptoAcumuladorDTO> listaConceptosAcumulador){
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoSaldoAnterior = null;
		if(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior() != null){
			estadoCuentaDecoradoSaldoAnterior = obtenerEstadoCuentaConSaldosCalculados(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior(), listaConceptosAcumulador);
		}
		else{
			List<SaldoConceptoDTO> listaSaldosConceptos = estadoCuentaFacade.obtenerSaldosIndividuales(estadoCuentaDTO,listaConceptosAcumulador,estadoCuentaDTO.getAcumuladorDTOs(),false);
			
			//Una vez que se tienen los saldos por concepto, se acumulan para calcular el saldo t�cnico
			SaldoConceptoDTO saldoTecnico = UtileriasCalculoEstadoCuenta.obtenerSaldosAcumulados(listaSaldosConceptos, null);
			
			//Se regresa el estado de cuenta para acumularlo hacia arriba
			EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO =  new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
			estadoCuentaDecoradoDTO.setSaldosAcumulados(listaSaldosConceptos);
			estadoCuentaDecoradoDTO.setSaldoTecnico(saldoTecnico);
			saldoTecnico.setFechaTrimestre(estadoCuentaDecoradoDTO.getDescripcionSuscripcion());
			return estadoCuentaDecoradoDTO;
		}
		SaldoConceptoDTO saldoTecnicoAnterior = estadoCuentaDecoradoSaldoAnterior.getSaldoTecnico();
		List<SaldoConceptoDTO> listaSaldosConceptos = estadoCuentaFacade.obtenerSaldosIndividuales(estadoCuentaDTO,listaConceptosAcumulador,estadoCuentaDTO.getAcumuladorDTOs(),false);
		saldoTecnicoAnterior.setDescripcionConcepto("Saldo Anterior");
		if(saldoTecnicoAnterior.getSaldoAcumulado() != null && saldoTecnicoAnterior.getSaldoAcumulado().compareTo(BigDecimal.ZERO) > 0){
			saldoTecnicoAnterior.setHaber(saldoTecnicoAnterior.getSaldoAcumulado());
		}else if (saldoTecnicoAnterior.getSaldoAcumulado() != null && saldoTecnicoAnterior.getSaldoAcumulado().compareTo(BigDecimal.ZERO) < 0){
			saldoTecnicoAnterior.setDebe(saldoTecnicoAnterior.getSaldoAcumulado().abs());
		}
		listaSaldosConceptos.add(0,saldoTecnicoAnterior);
		//Una vez que se tienen los saldos por concepto, se acumulan para calcular el saldo t�cnico
		SaldoConceptoDTO saldoTecnico = UtileriasCalculoEstadoCuenta.obtenerSaldosAcumulados(listaSaldosConceptos, null);
		
		//Se regresa el estado de cuenta para acumularlo hacia arriba
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO =  new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
		estadoCuentaDecoradoDTO.setSaldosAcumulados(listaSaldosConceptos);
		estadoCuentaDecoradoDTO.setSaldoTecnico(saldoTecnico);
		saldoTecnico.setFechaTrimestre(estadoCuentaDecoradoDTO.getDescripcionSuscripcion());
		return estadoCuentaDecoradoDTO;
	}	
	
	/**
	 * Consulta los estados de cuenta hist�ricos de contratos cuota parte � primer excedente de anios contractuales anteriores.
	 * Los datos de b�squeda se reciben en el objeto estadoCuentaDTO.
	 * @param estadoCuentaDTO
	 * @param nombreUsuario
	 * @return
	 */
	private List<EstadoCuentaDTO> consultarEstadosCuentaContratosAnteriores(EstadoCuentaDTO estadoCuentaDTO) {
		List<EstadoCuentaDTO> listaEstadoCuentaContratosAnteriores = new ArrayList<EstadoCuentaDTO>();
		CalculoSuscripcionesDTO calculoSuscripciones = null;
		
		//Calcular una fecha entre el rango, para utilizarla como refrencia al buscar suscripciones de otros periodos.
		calculoSuscripciones = new CalculoSuscripcionesDTO();
		SuscripcionDTO suscripcionPeriodoActual = calculoSuscripciones.getSuscripcion(new EstadoCuentaDecoradoDTO(estadoCuentaDTO), estadoCuentaDTO.getSuscripcion());
		Calendar fechaEntrePeriodoEstadoCuenta = Calendar.getInstance();
		fechaEntrePeriodoEstadoCuenta.setTime(suscripcionPeriodoActual.getFechaInicial());
		fechaEntrePeriodoEstadoCuenta.add(Calendar.DATE, 5);
		
		//Obtener los contratos CP o 1E anteriores del mismo reasegurador
		if(estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			List<ContratoCuotaParteDTO> listaContratosCPAnteriores = contratoCuotaParteFacade.obtenerListaContratoCuotaParteEjercicioAnterior(estadoCuentaDTO);
			if(listaContratosCPAnteriores != null && !listaContratosCPAnteriores.isEmpty()){
				listaEstadoCuentaContratosAnteriores = new ArrayList<EstadoCuentaDTO>();
				for(ContratoCuotaParteDTO contratoCPEnCurso : listaContratosCPAnteriores){
					//Identificar el anio del contrato, en base  a su anio, determinar su suscripcion correspondiente a la del estado de cuenta del ejercicio actual
					Calendar fechaInicioContrato = Calendar.getInstance();
					fechaInicioContrato.setTime(contratoCPEnCurso.getFechaInicial());
					int anioContrato = fechaInicioContrato.get(Calendar.YEAR);
					calculoSuscripciones = new CalculoSuscripcionesDTO(anioContrato,contratoCPEnCurso.getFechaInicial(),
							estadoCuentaDTO.getContratoCuotaParteDTO().getFechaFinal(),contratoCPEnCurso.getFormaPago());
					
					SuscripcionDTO suscripcionPeriodoAnterior = calculoSuscripciones.obtenerPeriodo(fechaEntrePeriodoEstadoCuenta.getTime());
					int suscripcion = suscripcionPeriodoAnterior.getIdSuscripcion();
					
					EstadoCuentaDTO estadoCuentaPeriodoEnCursoDTO = null;
					estadoCuentaPeriodoEnCursoDTO = obtenerEstadoCuentaHistorico(estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor(),
							estadoCuentaDTO.getLineaDTO().getSubRamo().getIdTcSubRamo(), contratoCPEnCurso, null,null, estadoCuentaDTO.getIdMoneda(), anioContrato, suscripcion);
					
					if(estadoCuentaPeriodoEnCursoDTO != null){
						listaEstadoCuentaContratosAnteriores.add(estadoCuentaPeriodoEnCursoDTO);
					}
				}
			}
		}else if(estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
			List<ContratoPrimerExcedenteDTO> listaContratos1EAnteriores = contratoPrimerExcedenteFacade.obtenerListaContratoPrimerExcedenteEjercicioAnterior(estadoCuentaDTO);
			if(listaContratos1EAnteriores != null && !listaContratos1EAnteriores.isEmpty()){
				listaEstadoCuentaContratosAnteriores = new ArrayList<EstadoCuentaDTO>();
				for(ContratoPrimerExcedenteDTO contrato1EEnCurso : listaContratos1EAnteriores){
					//Identificar el anio del contrato, en base  a su anio, determinar su suscripcion correspondiente a la del estado de cuenta del ejercicio actual
					Calendar fechaInicioContrato = Calendar.getInstance();
					fechaInicioContrato.setTime(contrato1EEnCurso.getFechaInicial());
					int anioContrato = fechaInicioContrato.get(Calendar.YEAR);
					calculoSuscripciones = new CalculoSuscripcionesDTO(anioContrato,contrato1EEnCurso.getFechaInicial(),
							estadoCuentaDTO.getContratoPrimerExcedenteDTO().getFechaFinal(),contrato1EEnCurso.getFormaPago());
					
					SuscripcionDTO suscripcionPeriodoAnterior = calculoSuscripciones.obtenerPeriodo(fechaEntrePeriodoEstadoCuenta.getTime());
					int suscripcion = suscripcionPeriodoAnterior.getIdSuscripcion();
					
					EstadoCuentaDTO estadoCuentaPeriodoEnCursoDTO = null;
					estadoCuentaPeriodoEnCursoDTO = obtenerEstadoCuentaHistorico(estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor(),
							estadoCuentaDTO.getLineaDTO().getSubRamo().getIdTcSubRamo(), null, contrato1EEnCurso,null, estadoCuentaDTO.getIdMoneda(), anioContrato, suscripcion);
					
					if(estadoCuentaPeriodoEnCursoDTO != null){
						listaEstadoCuentaContratosAnteriores.add(estadoCuentaPeriodoEnCursoDTO);
					}
				}
			}
		}
		
		return listaEstadoCuentaContratosAnteriores;
	}
	
	/**
	 * Consulta el estado de cuenta con sus suscripciones anteriores para un reasegurador, subramo, tipo reaseguro, moneda, ejercicio y suscripcion.
	 * Si no existe estado de cuenta para la suscripcion recibida, se genera un estado de cuenta vac�o y se contin�a la b&uacute;squeda hacia atr&aacute;s.
	 * @param idTcReasegurador
	 * @param idTcSubRamo
	 * @param contratoCuotaParteDTO
	 * @param contratoPrimerExcedenteDTO
	 * @param tipoReaseguro
	 * @param idMoneda
	 * @param ejercicio
	 * @param suscripcion
	 * @return
	 */
	private EstadoCuentaDTO obtenerEstadoCuentaHistorico(BigDecimal idTcReasegurador,BigDecimal idTcSubRamo,ContratoCuotaParteDTO contratoCuotaParteDTO, 
			ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO,Integer tipoReaseguro,int idMoneda,int ejercicio,int suscripcion) {
//		boolean consultaValida = true;
		EstadoCuentaFiltroDTO filtroEstadoCtaEnCurso = new EstadoCuentaFiltroDTO();
		filtroEstadoCtaEnCurso.setEjercicio(ejercicio);
		filtroEstadoCtaEnCurso.setIdMoneda(idMoneda);
		filtroEstadoCtaEnCurso.setIdtcreaseguradorcorredor(idTcReasegurador);
		filtroEstadoCtaEnCurso.setIdTcSubRamo(idTcSubRamo);
		//TODO falta settear el corredor al filtro
		
		int tipoReaseguroLocal = 0;
		if(contratoCuotaParteDTO != null && contratoCuotaParteDTO.getIdTmContratoCuotaParte() != null){
			tipoReaseguroLocal = TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE;
			filtroEstadoCtaEnCurso.setIdTmContratoCuotaParte(contratoCuotaParteDTO.getIdTmContratoCuotaParte());
		}
		else if(contratoPrimerExcedenteDTO != null && contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente() != null){
			tipoReaseguroLocal = TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE;
			filtroEstadoCtaEnCurso.setIdTmContratoPrimerExcedente(contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente());
		}
		else if(tipoReaseguro != null && tipoReaseguro != TipoReaseguroDTO.TIPO_RETENCION && tipoReaseguro != TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
			tipoReaseguroLocal = tipoReaseguro;
		}
		else{
			throw new RuntimeException("No se recibi� un contrato cuota parte ni primer excedente ni especificaci�n v�lida del tipo de reaseguro.");
		}
		
		filtroEstadoCtaEnCurso.setTipoReaseguro(tipoReaseguroLocal);
		
		List<EstadoCuentaDTO> listaEstadosCuentaPeriodoEnCurso = null;
		EstadoCuentaDTO estadoCuentaPeriodoEnCursoDTO = null;
		EstadoCuentaDTO estadoCuentaTMP = null;
		
		boolean estadoCuentaEncontrado = false;
		
		while(suscripcion >= 1){//Buscar el mismo estado de cuenta para la suscripcion anterior
			filtroEstadoCtaEnCurso.setSuscripcion(suscripcion);
			listaEstadosCuentaPeriodoEnCurso = estadoCuentaFacade.obtenerEstadosCuenta(filtroEstadoCtaEnCurso);
			//Si encuentra el estado de cuenta, se busca su historial y termina la b�squeda.
			if(listaEstadosCuentaPeriodoEnCurso != null && !listaEstadosCuentaPeriodoEnCurso.isEmpty()){
				estadoCuentaTMP = listaEstadosCuentaPeriodoEnCurso.get(0);
				estadoCuentaTMP = estadoCuentaFacade.obtenerEstadoCuentaHistorico(estadoCuentaTMP.getIdEstadoCuenta());
				if(estadoCuentaPeriodoEnCursoDTO != null){
					UtileriasCalculoEstadoCuenta.obtenerUltimoEstadoCuenta(estadoCuentaPeriodoEnCursoDTO).setEstadoCuentaSuscripcionAnterior(estadoCuentaTMP);
				}else{
					estadoCuentaPeriodoEnCursoDTO = estadoCuentaTMP;
				}
				estadoCuentaEncontrado = true;
				break;
			}
			else{//Si no lo encuentra, se simula uno y se continua la b�squeda hacia atr�s
				estadoCuentaTMP = UtileriasCalculoEstadoCuenta.duplicarEstadoCuenta(null, contratoCuotaParteDTO, contratoPrimerExcedenteDTO, ejercicio, suscripcion);
				if(estadoCuentaPeriodoEnCursoDTO == null){
					estadoCuentaPeriodoEnCursoDTO = estadoCuentaTMP;
				}
				else{
					UtileriasCalculoEstadoCuenta.obtenerUltimoEstadoCuenta(estadoCuentaPeriodoEnCursoDTO).setEstadoCuentaSuscripcionAnterior(estadoCuentaTMP);
				}
				suscripcion = suscripcion -1;
			}
		}
		
		if(estadoCuentaEncontrado){//Si se encontr� un estado de cuenta, los datos del reasegurador se pasan al primer estado de cuenta de la suscripci�n consultada originalmente
			EstadoCuentaDTO estadoCuentaReal = UtileriasCalculoEstadoCuenta.obtenerPrimerEstadoCuentaConContratoYReasegurador(estadoCuentaPeriodoEnCursoDTO);
			
			if(estadoCuentaReal != null && estadoCuentaReal != estadoCuentaPeriodoEnCursoDTO){
				UtileriasCalculoEstadoCuenta.copiarDatosEstadoCuenta(estadoCuentaReal, estadoCuentaPeriodoEnCursoDTO);
			}
		}
		else{//Si no se encontr� estado de cuenta, no hay nada que mostrar
			estadoCuentaPeriodoEnCursoDTO = null;
		}
		
		return estadoCuentaPeriodoEnCursoDTO;
	}
	
	private EstadoCuentaDecoradoDTO obtenerEstadoCuentaReaseguradorConSaldosCalculados(BigDecimal idTcReasegurador,BigDecimal idToReporteSiniestro,BigDecimal idMoneda, BigDecimal idTcSubRamo,
			boolean incluirFiltradoHasta,boolean incluirFiltradoPorMes,Integer mes,Integer anio,List<AgrupacionEstadoCuentaDTO> listaAgrupaciones){
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(new EstadoCuentaDTO());
		
		BigDecimal[] idTcSubRamoArray = null;
		if(idTcSubRamo != null){
			idTcSubRamoArray = new BigDecimal[1];
			idTcSubRamoArray[0] = idTcSubRamo;
		}
		
		//Se incluye el arreglo de subramos para consultar los saldos por subramo
		estadoCuentaDecoradoDTO.setSaldosAcumulados(calcularSaldosAcumulados(
					null,idToReporteSiniestro,idTcReasegurador,idMoneda,idTcSubRamoArray, listaAgrupaciones,incluirFiltradoHasta,incluirFiltradoPorMes,mes,anio,null,false));
		
		estadoCuentaDecoradoDTO.setSaldoTecnico(UtileriasCalculoEstadoCuenta.obtenerSaldosAcumulados(estadoCuentaDecoradoDTO.getSaldosAcumulados(), null));

		return estadoCuentaDecoradoDTO;
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaSiniestroConSaldosCalculados(BigDecimal idToReporteSiniestro, BigDecimal[] idTcSubRamo,
			boolean incluirFiltradoHasta,Integer mes,Integer anio,boolean separarConceptosPorReasegurador,List<AgrupacionEstadoCuentaDTO> listaAgrupaciones){
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = null;
		
			if(separarConceptosPorReasegurador){
				//1. Si se solicita el estado de cuenta separado por reaseguradores, se debe consultar la lista de reaseguradores que participaron en la distribuci�n del siniestro
				//leyendo los registros ToMovimientoReaseguro
				List<BigDecimal> listaIdTcReasegurador = obtenerListaIdTcReaseguradoresSiniestro(idToReporteSiniestro, true);
				//2.Despu�s de obtener la lista de los diferentes reaseguradores, se debe realizar la consulta de saldos por cada uno y generar un objeto EstadoCuentaDecorado.
				List<EstadoCuentaDecoradoDTO> listaEstadoCuentaDecPorReasegurador = new ArrayList<EstadoCuentaDecoradoDTO>();
				for(BigDecimal idTcReasegurador : listaIdTcReasegurador){
					ReaseguradorCorredorDTO reaseguradorDTO = reaseguradorCorredorFacade.findById(idTcReasegurador);
					String descripcionReasegurador = reaseguradorDTO != null ? reaseguradorDTO.getNombre() : "Nombre no disponible";
					EstadoCuentaDecoradoDTO estadoCuentaPorReaseguradorEnCurso = new EstadoCuentaDecoradoDTO(new EstadoCuentaDTO());
					//Se incluye el arreglo de subramos para consultar los saldos por subramo
					estadoCuentaPorReaseguradorEnCurso.setSaldosAcumulados(calcularSaldosAcumulados(
							null,idToReporteSiniestro,idTcReasegurador,null,idTcSubRamo, listaAgrupaciones,incluirFiltradoHasta,false,mes,anio,false,true));
					List<EstadoCuentaDecoradoDTO> listaTMPEstadoCuentaPorReasegurador = new ArrayList<EstadoCuentaDecoradoDTO>();
					listaTMPEstadoCuentaPorReasegurador.add(estadoCuentaPorReaseguradorEnCurso);
					estadoCuentaPorReaseguradorEnCurso = combinarEstadosDeCuentaDecorados(listaTMPEstadoCuentaPorReasegurador,listaAgrupaciones, true, descripcionReasegurador,false,false,null,true);
					listaEstadoCuentaDecPorReasegurador.add(estadoCuentaPorReaseguradorEnCurso);
				}
				//3. Combinar todos los estados de cuenta en uno s�lo
				estadoCuentaDecoradoDTO = combinarEstadosDeCuentaDecorados(listaEstadoCuentaDecPorReasegurador,listaAgrupaciones, false, null,false,false,null,true);
			}
			else{
				//Esta invocaci�n funcionar� s�lo para el caso en que no se solicite separar el estado de cuenta por reaseguradores.
				estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(new EstadoCuentaDTO());
				estadoCuentaDecoradoDTO.setSaldosAcumulados(new ArrayList<SaldoConceptoDTO>());
				
				estadoCuentaDecoradoDTO.setSaldosAcumulados(calcularSaldosAcumulados(null,idToReporteSiniestro,null,null,idTcSubRamo, listaAgrupaciones,incluirFiltradoHasta,false,mes,anio,null,false));
				estadoCuentaDecoradoDTO.setSaldoTecnico(UtileriasCalculoEstadoCuenta.obtenerSaldosAcumulados(estadoCuentaDecoradoDTO.getSaldosAcumulados(), null));
			}
		
		return estadoCuentaDecoradoDTO;
	}
	
	private EstadoCuentaDecoradoDTO obtenerEstadoCuentaConSaldosCalculados(EstadoCuentaDTO estadoCuentaDTO,
			List<AgrupacionEstadoCuentaDTO> listaAgrupaciones,
			boolean incluirFiltradoHasta,Integer mes,Integer anio,boolean incluirFiltradoPorMes,List<Date> mesesPorIncluir,List<AgrupacionEstadoCuentaDTO> listaAgrupacionesDesglosePorMes,
			Boolean mostrarSaldoAcumuladoPorAgrupacion,boolean forzarMostrarSubTotalPorAgrupacion){
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = null;
		if(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior() == null){
			estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
			estadoCuentaDecoradoDTO.setSaldosAcumulados(new ArrayList<SaldoConceptoDTO>());
			
			//2. por cada grupo a mostrar del estado de cuenta, consultar sus conceptos
			estadoCuentaDecoradoDTO.setSaldosAcumulados(calcularSaldosAcumulados(estadoCuentaDTO,null,null,null,null, listaAgrupaciones,incluirFiltradoHasta,incluirFiltradoPorMes,mes,anio,mostrarSaldoAcumuladoPorAgrupacion,forzarMostrarSubTotalPorAgrupacion));
			
			//4. Calcular el saldo por cada concepto del grupo y guardarlo en un SaldoConceptoDTO
			estadoCuentaDecoradoDTO.setSaldoTecnico(UtileriasCalculoEstadoCuenta.obtenerSaldosAcumulados(estadoCuentaDecoradoDTO.getSaldosAcumulados(), null));
		}
		else{
			EstadoCuentaDecoradoDTO estadoCuentaDecoradoSuscripcionAnterior = obtenerEstadoCuentaConSaldosCalculados(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior(),
					listaAgrupaciones,incluirFiltradoHasta,mes,anio,incluirFiltradoPorMes,mesesPorIncluir,listaAgrupacionesDesglosePorMes,mostrarSaldoAcumuladoPorAgrupacion,forzarMostrarSubTotalPorAgrupacion);
			
			estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
			estadoCuentaDecoradoDTO.setSaldosAcumulados(new ArrayList<SaldoConceptoDTO>());
			
			//2. por cada grupo a mostrar del estado de cuenta, consultar sus conceptos
			estadoCuentaDecoradoDTO.setSaldosAcumulados(calcularSaldosAcumulados(estadoCuentaDTO,null,null,null,null, listaAgrupaciones,incluirFiltradoHasta,incluirFiltradoPorMes,mes,anio,mostrarSaldoAcumuladoPorAgrupacion,forzarMostrarSubTotalPorAgrupacion));
			
			//02/12/2010 Se agrega referencia al saldo tecnico de la suscripcion anterior, para usarlo en los reportes
			SaldoConceptoDTO saldoAnterior = estadoCuentaDecoradoSuscripcionAnterior.getSaldoTecnico().duplicate();
			saldoAnterior.setDescripcionConcepto("Saldo Anterior");
			if(saldoAnterior.getSaldoAcumulado() != null && saldoAnterior.getSaldoAcumulado().compareTo(BigDecimal.ZERO) > 0){
				saldoAnterior.setHaber(saldoAnterior.getSaldoAcumulado());
			}else if (saldoAnterior.getSaldoAcumulado() != null && saldoAnterior.getSaldoAcumulado().compareTo(BigDecimal.ZERO) < 0){
				saldoAnterior.setDebe(saldoAnterior.getSaldoAcumulado().abs());
			}
			
			//4. Calcular el saldo por cada concepto del grupo y guardarlo en un SaldoConceptoDTO
			estadoCuentaDecoradoDTO.setSaldoTecnico(UtileriasCalculoEstadoCuenta.obtenerSaldosAcumulados(estadoCuentaDecoradoDTO.getSaldosAcumulados(), saldoAnterior));
			
			
			estadoCuentaDecoradoDTO.setSaldoTecnicoAnterior(saldoAnterior);
		}
		if(incluirFiltradoPorMes){
			List<Date> listaMesesIncluidos = null;
			if(mesesPorIncluir == null){
				//Consultar los meses incluidos en la suscripcion
				EstadoCuentaDecoradoDTO estadoCuentaDecoradoTMP = new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
				listaMesesIncluidos = estadoCuentaDecoradoTMP.getSuscripcionDTO().obtenerMesesIncluidos();
			}
			else{
				listaMesesIncluidos = mesesPorIncluir;
			}
			if(listaMesesIncluidos != null){
				List<List<SaldoConceptoDTO>> listaSaldoConceptoPorMeses = new ArrayList<List<SaldoConceptoDTO>>();
				List<SaldoConceptoDTO> listaSaldoConceptoPorMes = new ArrayList<SaldoConceptoDTO>();
				List<SaldoConceptoDTO> listaSaldosTecnicosPorMes = new ArrayList<SaldoConceptoDTO>();
				SaldoConceptoDTO saldoTecnicoPorMes = null;
				Calendar calendarTMP = Calendar.getInstance();
				
				for(Date mesEnCurso : listaMesesIncluidos){
					//por cada mes que compone la suscripcion, se invoca el metodo de calculo del saldo
					calendarTMP.setTime(mesEnCurso);
					//2. por cada grupo a mostrar del estado de cuenta, consultar sus conceptos, por cada mes que compone la suscripcion
					listaSaldoConceptoPorMes = calcularSaldosAcumulados(estadoCuentaDTO,null,null,null,null, listaAgrupacionesDesglosePorMes,incluirFiltradoHasta,
							incluirFiltradoPorMes,calendarTMP.get(Calendar.MONTH)+1,calendarTMP.get(Calendar.YEAR),mostrarSaldoAcumuladoPorAgrupacion,forzarMostrarSubTotalPorAgrupacion);
					
					listaSaldoConceptoPorMeses.add(listaSaldoConceptoPorMes);
					
					//4. Calcular el saldo por cada concepto del grupo y guardarlo en un SaldoConceptoDTO
					saldoTecnicoPorMes = UtileriasCalculoEstadoCuenta.obtenerSaldosAcumulados(listaSaldoConceptoPorMes,null);
					saldoTecnicoPorMes.setMesPertenceSaldo(calendarTMP);
					
					listaSaldosTecnicosPorMes.add(saldoTecnicoPorMes);
					
				}
				estadoCuentaDecoradoDTO.setListaSaldosAcumuladosPorMes(listaSaldoConceptoPorMeses);
				estadoCuentaDecoradoDTO.setSaldosAcumuladosPorMes(listaSaldosTecnicosPorMes);
			}
			
		}
		return estadoCuentaDecoradoDTO;
	}
	
	/**
	 * Agrupa la lista de estados de cuenta en un mapa que usa como llave el Id de cada reasegurador, y como valor tiene la lista de estados de cuetna que le corresponden al reasegurador.
	 * Posteriormente consulta los estados de cuenta de los endosos del contrato facultativo de cada estado de cuenta y los agrega al mapa una s�la vez para evitar duplicidad.
	 * La lista que recibe ser�n lo estados de cuetna y contratos facultativos que procesar�. 
	 * @param idToPoliza
	 * @param listaEstadoCuentaDTO
	 * @return Map<BigDecimal,List<EstadoCuentaDTO>>.
	 */
	private Map<BigDecimal,List<EstadoCuentaDTO>> obtenerEstadosCuentaFacultativosAgrupadosPorReasegurador(BigDecimal idToPoliza,List<EstadoCuentaDTO> listaEstadoCuentaDTO){
		Map<BigDecimal,List<EstadoCuentaDTO>> mapaEstadosCuentaPorReasegurador = new HashMap<BigDecimal, List<EstadoCuentaDTO>>();
		for (EstadoCuentaDTO estadoCuentaDTO : listaEstadoCuentaDTO) {
			if(!mapaEstadosCuentaPorReasegurador.containsKey(estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor())){
				mapaEstadosCuentaPorReasegurador.put(estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor(),new ArrayList<EstadoCuentaDTO>());
			}
			List<EstadoCuentaDTO> listaEstadosCuentaPorReaseguradorEnCurso = 
				mapaEstadosCuentaPorReasegurador.get(estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
			if(!listaEstadosCuentaPorReaseguradorEnCurso.contains(estadoCuentaDTO)){
				listaEstadosCuentaPorReaseguradorEnCurso.add(estadoCuentaDTO);
			}
		}
		return mapaEstadosCuentaPorReasegurador;
	}
	
	/**
	 * Recibe la lista de estados de cuenta facultativos de endoso 0 registrados para la p�liza cuyo id se recibe.
	 * Cada estado de cuenta lo agrupar por subramo y consulta sus endosos posteriores (los estados de cuenta del contrato
	 * facultativo de endoso siguiente).
	 * @param idToPoliza
	 * @param listaEstadoCuentaDTO
	 * @return
	 */
	private Map<BigDecimal,List<EstadoCuentaDTO>> obtenerEstadosCuentaFacultativosAgrupadosPorSubRamo(BigDecimal idToPoliza,
			List<EstadoCuentaDTO> listaEstadoCuentaDTO,BigDecimal idTcReasegurador,BigDecimal idMoneda,
			BigDecimal idTcSubRamo,boolean agruparPorContrato){
		Map<BigDecimal,List<EstadoCuentaDTO>> mapaEstadosCuentaPorSubRamo = new HashMap<BigDecimal, List<EstadoCuentaDTO>>();
		List<BigDecimal> listaIdContratoFacultativoConsultado = new ArrayList<BigDecimal>();
	 	for (EstadoCuentaDTO estadoCuentaDTO : listaEstadoCuentaDTO) {
	 		if(agruparPorContrato){
		 		if(!mapaEstadosCuentaPorSubRamo.containsKey(estadoCuentaDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo())){
		 			mapaEstadosCuentaPorSubRamo.put(estadoCuentaDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo(), new ArrayList<EstadoCuentaDTO>());
		 		}
		 		mapaEstadosCuentaPorSubRamo.get(estadoCuentaDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo()).add(estadoCuentaDTO);
	 		}
	 		else{
	 			if(!mapaEstadosCuentaPorSubRamo.containsKey(estadoCuentaDTO.getSubRamoDTO().getIdTcSubRamo())){
		 			mapaEstadosCuentaPorSubRamo.put(estadoCuentaDTO.getSubRamoDTO().getIdTcSubRamo(), new ArrayList<EstadoCuentaDTO>());
		 		}
		 		mapaEstadosCuentaPorSubRamo.get(estadoCuentaDTO.getSubRamoDTO().getIdTcSubRamo()).add(estadoCuentaDTO);
	 		}
	 		//Buscar los endosos posteriores realizados al contrato facultativo del estado de cuenta, para ser incluidos en los c�lculos
	 		if(!listaIdContratoFacultativoConsultado.contains(estadoCuentaDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo())){
	 			listaIdContratoFacultativoConsultado.add(estadoCuentaDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo());
	 		}
	 	}
	 	return mapaEstadosCuentaPorSubRamo;
	}
	
	@SuppressWarnings("unchecked")
	private List<EstadoCuentaDTO> consultarEstadosCuentaFacultativos(BigDecimal idToPoliza,BigDecimal idTcSubRamo,BigDecimal idTmContratoFacultativo){
		String queryString = "select model from EstadoCuentaDTO model where model.idPoliza = :idPoliza and " +
			" model.tipoReaseguro = :tipoReaseguro and model.subRamoDTO.idTcSubRamo = :idTcSubRamo and " +
			" model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idPoliza", idToPoliza);
		query.setParameter("idTcSubRamo", idTcSubRamo);
		query.setParameter("idTmContratoFacultativo", idTmContratoFacultativo);
		query.setParameter("tipoReaseguro", TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);
		
		return query.getResultList();
	}
	
	private SaldoConceptoDTO obtenerSaldoPorConcepto(SaldoConceptoDTO saldoConceptoFiltro,
			List<EstadoCuentaDecoradoDTO> listaEstadoCuentaDecorado,List<AgrupacionEstadoCuentaDTO> listaAgrupaciones){
		SaldoConceptoDTO conceptoDTO = new SaldoConceptoDTO(saldoConceptoFiltro.getDescripcionConcepto(),"",saldoConceptoFiltro.getIdTcConfiguracionSaldoConcepto(),BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO);
		boolean conceptoSaldoAnterior = (saldoConceptoFiltro.getDescripcionConcepto().equalsIgnoreCase("Saldo Anterior"));
		BigDecimal sumatoriaDebe = BigDecimal.ZERO;
		BigDecimal sumatoriaHaber = BigDecimal.ZERO;
		BigDecimal sumatoriaSaldoAcumulado = BigDecimal.ZERO;
		BigDecimal sumatoriaSaldo = BigDecimal.ZERO;
		if(!(saldoConceptoFiltro instanceof SaldoAgrupacionDTO)){
			for(EstadoCuentaDecoradoDTO estadoCuentaDecorado : listaEstadoCuentaDecorado){
				for(SaldoConceptoDTO saldoConceptoTMP : estadoCuentaDecorado.getSaldosAcumulados()){
					if(saldoConceptoTMP.getDescripcionConcepto().compareTo(saldoConceptoFiltro.getDescripcionConcepto()) == 0){
						if(!conceptoSaldoAnterior){
							sumatoriaDebe = sumatoriaDebe.add(saldoConceptoTMP.getDebe());
							sumatoriaHaber= sumatoriaHaber.add(saldoConceptoTMP.getHaber());
						}
						else{
							sumatoriaSaldoAcumulado = sumatoriaSaldoAcumulado.add(saldoConceptoTMP.getSaldoAcumulado());
							if(saldoConceptoTMP.getSaldo() != null)
								sumatoriaSaldo = sumatoriaSaldo.add(saldoConceptoTMP.getSaldo());
						}
					}
				}
			}
		}
		//B�squeda de la naturaleza del concepto recibido
		ConfiguracionConceptoEstadoCuentaDTO configuracionResultado = obtenerConfiguracionConcepto(saldoConceptoFiltro, listaAgrupaciones);
		/*
		 * Se debe calcular el saldo del concepto y colocarlo en el debe o haber, de acuerdo a la naturaleza del concepto.
		 */
		if(!conceptoSaldoAnterior){
			if(configuracionResultado != null && configuracionResultado.getNaturaleza() != null){
				calcularSaldosEnBaseADebeYHaber(conceptoDTO, configuracionResultado.getNaturaleza(), sumatoriaHaber, sumatoriaDebe);
			}
			else{
				conceptoDTO.setDebe(sumatoriaDebe);
				conceptoDTO.setHaber(sumatoriaHaber);
				conceptoDTO.setSaldo(sumatoriaHaber.subtract(sumatoriaDebe));
			}
		}
		else{
			conceptoDTO.setSaldo(sumatoriaSaldo);
			conceptoDTO.setSaldoAcumulado(sumatoriaSaldoAcumulado);
		}
		
		return conceptoDTO;
	}
	
	private SaldoConceptoDTO obtenerSaldoPorConcepto(List<SaldoConceptoDTO> listaSaldosConcepto,SaldoConceptoDTO saldoConceptoFiltro){
		SaldoConceptoDTO conceptoDTO = new SaldoConceptoDTO(saldoConceptoFiltro.getDescripcionConcepto(),"",saldoConceptoFiltro.getIdTcConfiguracionSaldoConcepto(),BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO);
		boolean conceptoSaldoAnterior = (saldoConceptoFiltro.getDescripcionConcepto().equalsIgnoreCase("Saldo Anterior"));
		BigDecimal sumatoriaDebe = BigDecimal.ZERO;
		BigDecimal sumatoriaHaber = BigDecimal.ZERO;
		BigDecimal sumatoriaSaldoAcumulado = BigDecimal.ZERO;
		BigDecimal sumatoriaSaldo = BigDecimal.ZERO;
		if(!(saldoConceptoFiltro instanceof SaldoAgrupacionDTO)){
			for(SaldoConceptoDTO saldoConceptoTMP : listaSaldosConcepto){
				if(saldoConceptoTMP.getDescripcionConcepto().compareTo(saldoConceptoFiltro.getDescripcionConcepto()) == 0){
					if(!conceptoSaldoAnterior){
						sumatoriaDebe = sumatoriaDebe.add(saldoConceptoTMP.getDebe());
						sumatoriaHaber= sumatoriaHaber.add(saldoConceptoTMP.getHaber());
					}
					else{
						sumatoriaSaldoAcumulado = sumatoriaSaldoAcumulado.add(saldoConceptoTMP.getSaldoAcumulado());
						if(saldoConceptoTMP.getSaldo() != null)
							sumatoriaSaldo = sumatoriaSaldo.add(saldoConceptoTMP.getSaldo());
					}
				}
			}
		}
		//B�squeda de la naturaleza del concepto recibido
//		ConfiguracionConceptoEstadoCuentaDTO configuracionResultado = obtenerConfiguracionConcepto(saldoConceptoFiltro, listaAgrupaciones);
		/*
		 * Se debe calcular el saldo del concepto y colocarlo en el debe o haber, de acuerdo a la naturaleza del concepto.
		 */
		if(!conceptoSaldoAnterior){
//			if(configuracionResultado != null && configuracionResultado.getNaturaleza() != null){
//				calcularSaldosEnBaseADebeYHaber(conceptoDTO, configuracionResultado.getNaturaleza(), sumatoriaHaber, sumatoriaDebe);
//			}
//			else{
				conceptoDTO.setDebe(sumatoriaDebe);
				conceptoDTO.setHaber(sumatoriaHaber);
				conceptoDTO.setSaldo(sumatoriaHaber.subtract(sumatoriaDebe));
//			}
		}
		else{
			conceptoDTO.setDebe(sumatoriaDebe);
			conceptoDTO.setHaber(sumatoriaHaber);
			conceptoDTO.setSaldo(sumatoriaSaldo);
			conceptoDTO.setSaldoAcumulado(sumatoriaSaldoAcumulado);
		}
		
		return conceptoDTO;
	}
	
	private ConfiguracionConceptoEstadoCuentaDTO obtenerConfiguracionConcepto(SaldoConceptoDTO saldoConceptoFiltro,List<AgrupacionEstadoCuentaDTO> listaAgrupaciones){
		ConfiguracionConceptoEstadoCuentaDTO configuracionResultado = null;
		for(AgrupacionEstadoCuentaDTO agrupacion : listaAgrupaciones){
			for(ConfiguracionConceptoEstadoCuentaDTO configConcepto : agrupacion.getConfiguracionConceptoEstadoCuentaList()){
				if(saldoConceptoFiltro.getDescripcionConcepto().equalsIgnoreCase(configConcepto.getNombreConcepto())){
					configuracionResultado = configConcepto;
					break;
				}
			}
			if(configuracionResultado != null)
				break;
		}
		return configuracionResultado;
	}
	
	/**
	 * Consulta los estados de cuenta facultativos de endoso 0 en base a los par�metros de filtrado recibidos.
	 * @param idToPoliza
	 * @param idTcReasegurador
	 * @param idMoneda
	 * @param idTcSubRamo
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<EstadoCuentaDTO> consultarEstadosCuentaFacultativosEndoso0(BigDecimal idToPoliza,BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal idTcSubRamo,boolean separarSaldoPorMes,List<Date> mesesPorIncluir){
		String queryString = null;
		Query query = null;
		if(separarSaldoPorMes && mesesPorIncluir != null && !mesesPorIncluir.isEmpty()){
			queryString = "select distinct model.estadoCuentaDTO from AcumuladorDTO model where model.estadoCuentaDTO.tipoReaseguro = :tipoReaseguro " +
				(idToPoliza != null?" and model.estadoCuentaDTO.idPoliza = :idPoliza ":"")+
				(idTcReasegurador != null? " and model.estadoCuentaDTO.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor " : "")+
				(idTcSubRamo!=null?" and model.estadoCuentaDTO.subRamoDTO.idTcSubRamo = :idTcSubRamo":"")+
				(idMoneda!=null?" and model.estadoCuentaDTO.idMoneda = :idMoneda":"") +" and ("+ obtenerMesesPorIncluir(mesesPorIncluir) + ")";
		}
		else{
			queryString = "select model from EstadoCuentaDTO model where " +
				" model.tipoReaseguro = :tipoReaseguro "+//and model.contratoFacultativoDTO.idTmContratoFacultativoAnterior is null"+
				(idToPoliza != null?" and model.idPoliza = :idPoliza ":"")+
				(idTcReasegurador != null? " and model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor " : "")+
				(idTcSubRamo!=null?" and model.subRamoDTO.idTcSubRamo = :idTcSubRamo":"")+
				(idMoneda!=null?" and model.idMoneda = :idMoneda":"");
		}
		query = entityManager.createQuery(queryString);
		if(idToPoliza!= null)
			query.setParameter("idPoliza", idToPoliza);
		if(idTcReasegurador != null)
			query.setParameter("idtcreaseguradorcorredor", idTcReasegurador);
		if(idTcSubRamo != null)
			query.setParameter("idTcSubRamo", idTcSubRamo);
		if(idMoneda != null)
			query.setParameter("idMoneda", idMoneda.intValue());
		
		query.setParameter("tipoReaseguro", TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);
		return query.getResultList();
	}
	
	public String obtenerMesesPorIncluir(List<Date> mesesPorIncluir) {
		StringBuilder queryString = new StringBuilder("");
		for(int i=0;i<mesesPorIncluir.size();i++){
			Date fecha = mesesPorIncluir.get(i);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(fecha);
			queryString.append(" (model.mes = "+(calendar.get(Calendar.MONTH)+1)+" and model.anio = "+calendar.get(Calendar.YEAR)).append(") ");
			if(i != mesesPorIncluir.size()-1)
				queryString.append(" or ");
		}		
		return queryString.toString();
	}

	private List<EstadoCuentaDecoradoDTO> obtenerEstadosCuentaDecoradosPorAgrupacionIdContratoFac(Map<BigDecimal,List<EstadoCuentaDTO>> mapaEstadosCuentaPorSubRamo,
			List<AgrupacionEstadoCuentaDTO> listaAgrupaciones,boolean incluirFiltradoHasta,Integer mes,Integer anio,boolean separarSaldoPorMes,List<Date> mesesPorIncluir,
			List<AgrupacionEstadoCuentaDTO> listaAgrupacionesDesglosePorMes){
		List<EstadoCuentaDecoradoDTO> listaEstadoCuentaDecoradoResultado = new ArrayList<EstadoCuentaDecoradoDTO>();
		List<EstadoCuentaDTO> listaEstadoCuentaDTO = null;
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = null;
		for(BigDecimal idTcSubRamo : mapaEstadosCuentaPorSubRamo.keySet()){
	 		listaEstadoCuentaDTO = mapaEstadosCuentaPorSubRamo.get(idTcSubRamo);
	 		if(listaEstadoCuentaDTO != null && !listaEstadoCuentaDTO.isEmpty()){
	 			estadoCuentaDecoradoDTO = calcularSaldoCombinadoEstadosCuenta(listaEstadoCuentaDTO, listaAgrupaciones,false,null,incluirFiltradoHasta,mes,anio,separarSaldoPorMes,mesesPorIncluir,listaAgrupacionesDesglosePorMes,null,false,false);
	 			estadoCuentaDecoradoDTO.setSubRamoDTO(listaEstadoCuentaDTO.get(0).getSubRamoDTO());
	 			estadoCuentaDecoradoDTO.setLineaDTO(listaEstadoCuentaDTO.get(0).getLineaDTO());
	 			estadoCuentaDecoradoDTO.getEstadoCuentaDTO().setContratoFacultativoDTO(listaEstadoCuentaDTO.get(0).getContratoFacultativoDTO());
				listaEstadoCuentaDecoradoResultado.add(estadoCuentaDecoradoDTO);
			}
	 	}
		return listaEstadoCuentaDecoradoResultado;
	}
	
	private EstadoCuentaDecoradoDTO calcularSaldoCombinadoEstadosCuenta(List<EstadoCuentaDTO> listaEstadoCuentaDTO,
			List<AgrupacionEstadoCuentaDTO> listaAgrupaciones,boolean agregarDescripcionReasegurador,String descripcionAgrupacion,
			boolean incluirFiltradoHasta,Integer mes,Integer anio,boolean incluirFiltradoPorMes,List<Date> mesesPorIncluir,List<AgrupacionEstadoCuentaDTO> listaAgrupacionesDesglosePorMes,
			Boolean mostrarSaldoAcumuladoPorAgrupacion,boolean forzarMostrarSubTotalPorAgrupacion,boolean omitirBanderaAplicaDesglose){
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = null;
		if(listaEstadoCuentaDTO != null && !listaEstadoCuentaDTO.isEmpty()){
			List<EstadoCuentaDecoradoDTO> listaEstadoCuentaDecorado = new ArrayList<EstadoCuentaDecoradoDTO>();
			for(EstadoCuentaDTO estadoCuentaTMP : listaEstadoCuentaDTO){
//				estadoCuentaTMP.setAcumuladorDTOs(estadoCuentaTMP.getAcumuladorDTOs());
				estadoCuentaDecoradoDTO = obtenerEstadoCuentaConSaldosCalculados(estadoCuentaTMP, listaAgrupaciones,incluirFiltradoHasta,mes,anio,incluirFiltradoPorMes,mesesPorIncluir,listaAgrupacionesDesglosePorMes,mostrarSaldoAcumuladoPorAgrupacion,forzarMostrarSubTotalPorAgrupacion);
				listaEstadoCuentaDecorado.add(estadoCuentaDecoradoDTO);
			}
			estadoCuentaDecoradoDTO = combinarEstadosDeCuentaDecorados(listaEstadoCuentaDecorado,listaAgrupaciones,agregarDescripcionReasegurador,descripcionAgrupacion,
					true,incluirFiltradoPorMes,mesesPorIncluir,omitirBanderaAplicaDesglose);
		}
		return estadoCuentaDecoradoDTO;
	}
	
	private EstadoCuentaDecoradoDTO combinarEstadosDeCuentaDecorados(List<EstadoCuentaDecoradoDTO> listaEstadoCuentaDecorado,List<AgrupacionEstadoCuentaDTO> listaAgrupaciones,
			boolean agregarDescripcionAgrupacion,String descripcionAgrupacion,boolean combinarSaldos,boolean incluirFiltradoPorMes,List<Date> mesesPorIncluir,boolean omitirBanderaAplicaDesglose){
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(new EstadoCuentaDTO());
		if(!listaEstadoCuentaDecorado.isEmpty()){
			estadoCuentaDecoradoDTO.setSaldosAcumulados(new ArrayList<SaldoConceptoDTO>());
			List<SaldoConceptoDTO> listaConceptosPorAgrupacion = new ArrayList<SaldoConceptoDTO>();
			if(agregarDescripcionAgrupacion && descripcionAgrupacion != null){
				SaldoConceptoDTO saldoConceptoTMP = new SaldoConceptoDTO();
				saldoConceptoTMP.setDescripcionConcepto(descripcionAgrupacion);
				saldoConceptoTMP.setDebe(BigDecimal.ZERO);
				saldoConceptoTMP.setHaber(BigDecimal.ZERO);
				saldoConceptoTMP.setSaldo(BigDecimal.ZERO);
				saldoConceptoTMP.setOcultarSaldos(true);
				saldoConceptoTMP.setMostrarSaldoAcumulado(false);
				saldoConceptoTMP.setResaltarConcepto(true);
				estadoCuentaDecoradoDTO.getSaldosAcumulados().add(saldoConceptoTMP);
			}
			if(combinarSaldos){
				combinarListaConceptos(estadoCuentaDecoradoDTO, listaEstadoCuentaDecorado, listaEstadoCuentaDecorado.get(0).getSaldosAcumulados(),
						listaAgrupaciones, omitirBanderaAplicaDesglose, incluirFiltradoPorMes, mesesPorIncluir);
//				for(SaldoConceptoDTO saldoConceptoDTO : listaEstadoCuentaDecorado.get(0).getSaldosAcumulados()){
//					SaldoConceptoDTO saldoConceptoTMP = null;
//					if(saldoConceptoDTO instanceof SaldoAgrupacionDTO){
//						saldoConceptoTMP = UtileriasCalculoEstadoCuenta.obtenerSaldoPorAgrupacion(listaConceptosPorAgrupacion);
//						listaConceptosPorAgrupacion.clear();
//					}
//					else{
//						if(saldoConceptoDTO.isAplicaDesglosePorEjercicio() && !omitirBanderaAplicaDesglose){
//							//Consultar todos los saldos del concepto y aplicar
//							for(EstadoCuentaDecoradoDTO estadoCuentaDecoradoTMP : listaEstadoCuentaDecorado){
//								List<EstadoCuentaDecoradoDTO> listaEstadoCuentaDecoradoTMP = new ArrayList<EstadoCuentaDecoradoDTO>();
//								listaEstadoCuentaDecoradoTMP.add(estadoCuentaDecoradoTMP);
//								saldoConceptoTMP = obtenerSaldoPorConcepto(saldoConceptoDTO, listaEstadoCuentaDecoradoTMP,listaAgrupaciones);
//								if(saldoConceptoTMP != null){
//									saldoConceptoTMP.setDescripcionConcepto(saldoConceptoTMP.getDescripcionConcepto()+" "+
//											estadoCuentaDecoradoTMP.getEjercicio()+" - "+(estadoCuentaDecoradoTMP.getEjercicio()+1));
//									listaConceptosPorAgrupacion.add(saldoConceptoTMP);
//									
//									saldoConceptoTMP.setOcultarSaldos(saldoConceptoDTO.isOcultarSaldos());
//									saldoConceptoTMP.setMostrarSaldoAcumulado(saldoConceptoDTO.isMostrarSaldoAcumulado());
//									saldoConceptoTMP.setResaltarConcepto(saldoConceptoDTO.isResaltarConcepto());
//									estadoCuentaDecoradoDTO.getSaldosAcumulados().add(saldoConceptoTMP);
//								}
//							}
//						}
//						else{
//							saldoConceptoTMP = obtenerSaldoPorConcepto(saldoConceptoDTO, listaEstadoCuentaDecorado,listaAgrupaciones);
//							listaConceptosPorAgrupacion.add(saldoConceptoTMP);
//							
//							saldoConceptoTMP.setOcultarSaldos(saldoConceptoDTO.isOcultarSaldos());
//							saldoConceptoTMP.setMostrarSaldoAcumulado(saldoConceptoDTO.isMostrarSaldoAcumulado());
//							saldoConceptoTMP.setResaltarConcepto(saldoConceptoDTO.isResaltarConcepto());
//							estadoCuentaDecoradoDTO.getSaldosAcumulados().add(saldoConceptoTMP);
//						}
//					}
//				}
				//02/12/2010 Si se el estado de cuenta contiene la lista de saldos por mes, incluir la combinaci�n de saldos mensuales.
//				if(incluirFiltradoPorMes && mesesPorIncluir != null && !mesesPorIncluir.isEmpty()){
//					boolean aplicarFiltradoPorMes = false;
//					for(EstadoCuentaDecoradoDTO edoCtaTMP : listaEstadoCuentaDecorado){
//						if(edoCtaTMP.getListaSaldosAcumuladosPorMes() != null && !edoCtaTMP.getListaSaldosAcumuladosPorMes().isEmpty() &&
//								edoCtaTMP.getSaldosAcumuladosPorMes() != null && !edoCtaTMP.getSaldosAcumuladosPorMes().isEmpty() && 
//								edoCtaTMP.getSaldosAcumuladosPorMes().size() == edoCtaTMP.getListaSaldosAcumuladosPorMes().size()){
//							aplicarFiltradoPorMes = true;
//						}
//						else{
//							aplicarFiltradoPorMes = false;
//							break;
//						}
//					}
//					
//					if(aplicarFiltradoPorMes){
//						List<List<SaldoConceptoDTO>> listaSaldoConceptoCombinadosPorMes = new ArrayList<List<SaldoConceptoDTO>>();
//						List<SaldoConceptoDTO> listaSaldoTecnicoPorMes = new ArrayList<SaldoConceptoDTO>();
//						for(int i=0;i<listaEstadoCuentaDecorado.get(0).getSaldosAcumuladosPorMes().size();i++){
//							listaSaldoConceptoCombinadosPorMes.add(i,new ArrayList<SaldoConceptoDTO>());
//							
//							SaldoConceptoDTO saldoTecnicoListaI = listaEstadoCuentaDecorado.get(0).getSaldosAcumuladosPorMes().get(i);
//							SaldoConceptoDTO saldoTecnicoTotalListaI = new SaldoConceptoDTO();
//							saldoTecnicoTotalListaI.setDebe(BigDecimal.ZERO);
//							saldoTecnicoTotalListaI.setHaber(BigDecimal.ZERO);
//							saldoTecnicoTotalListaI.setSaldo(BigDecimal.ZERO);
//							saldoTecnicoTotalListaI.setSaldoAcumulado(BigDecimal.ZERO);
//							saldoTecnicoTotalListaI.setDescripcionConcepto(saldoTecnicoListaI.getDescripcionConcepto());
//							saldoTecnicoTotalListaI.setFechaTrimestre(saldoTecnicoListaI.getFechaTrimestre());
//							saldoTecnicoTotalListaI.setMesPertenceSaldo(saldoTecnicoListaI.getMesPertenceSaldo());
//							
//							for(int j=0;j<listaEstadoCuentaDecorado.get(0).getListaSaldosAcumuladosPorMes().get(0).size();j++){
//								
//								//Duplicar el saldo para incluirlo en la nueva lista
//								SaldoConceptoDTO saldoEdoCta1 = listaEstadoCuentaDecorado.get(0).getListaSaldosAcumuladosPorMes().get(i).get(j);
//								
//								SaldoConceptoDTO saldoNuevo = new SaldoConceptoDTO();
//								saldoNuevo.setDebe(BigDecimal.ZERO);
//								saldoNuevo.setHaber(BigDecimal.ZERO);
//								saldoNuevo.setSaldo(BigDecimal.ZERO);
//								saldoNuevo.setSaldoAcumulado(BigDecimal.ZERO);
//								saldoNuevo.setDescripcionConcepto(saldoEdoCta1.getDescripcionConcepto());
//								saldoNuevo.setFechaTrimestre(saldoEdoCta1.getFechaTrimestre());
//								saldoNuevo.setMesPertenceSaldo(saldoEdoCta1.getMesPertenceSaldo());
//								saldoNuevo.setMostrarSaldoAcumulado(saldoEdoCta1.isMostrarSaldoAcumulado());
//								
//								for(int k=0;k<listaEstadoCuentaDecorado.size();k++){
//									SaldoConceptoDTO saldoEdoCtaIterado = listaEstadoCuentaDecorado.get(k).getListaSaldosAcumuladosPorMes().get(i).get(j);
//									saldoNuevo.setDebe(saldoNuevo.getDebe().add(saldoEdoCtaIterado.getDebe()));
//									saldoNuevo.setHaber(saldoNuevo.getHaber().add(saldoEdoCtaIterado.getHaber()));
//									saldoNuevo.setSaldo(saldoNuevo.getSaldo().add(saldoEdoCtaIterado.getSaldo()));
//									
//									SaldoConceptoDTO saldoTecnicoEdoCtaIterado = listaEstadoCuentaDecorado.get(k).getSaldosAcumuladosPorMes().get(i);
////									saldoTecnicoListaI.setDebe(saldoTecnicoListaI.getDebe().add(saldoTecnicoEdoCtaIterado.getDebe()));
////									saldoTecnicoListaI.setHaber(saldoTecnicoListaI.getHaber().add(saldoTecnicoEdoCtaIterado.getHaber()));
//									saldoTecnicoListaI.setSaldo(saldoTecnicoListaI.getSaldo().add(saldoTecnicoEdoCtaIterado.getSaldo()));
//									saldoTecnicoListaI.setSaldoAcumulado(saldoTecnicoListaI.getSaldoAcumulado().add(saldoTecnicoEdoCtaIterado.getSaldoAcumulado()));
//								}
//								
//								listaSaldoConceptoCombinadosPorMes.get(i).add(j,saldoNuevo);
//							}
//							
//							listaSaldoTecnicoPorMes.add(i,saldoTecnicoListaI);
//						}
//					estadoCuentaDecoradoDTO.setListaSaldosAcumuladosPorMes(listaSaldoConceptoCombinadosPorMes);
//					estadoCuentaDecoradoDTO.setSaldosAcumuladosPorMes(listaSaldoTecnicoPorMes);//9 elementos, deberian ser solo 3
//					}
//				}
			}
			else{
				for(EstadoCuentaDecoradoDTO estadoCuentaTMP : listaEstadoCuentaDecorado){
					if(estadoCuentaTMP.getSaldosAcumulados() != null)
						for(SaldoConceptoDTO saldoConceptoDTO : estadoCuentaTMP.getSaldosAcumulados()){
							SaldoConceptoDTO saldoConceptoTMP = null;
							if(saldoConceptoDTO instanceof SaldoAgrupacionDTO){
								saldoConceptoTMP = UtileriasCalculoEstadoCuenta.obtenerSaldoPorAgrupacion(listaConceptosPorAgrupacion);
								listaConceptosPorAgrupacion.clear();
							}
							else{
								saldoConceptoTMP = saldoConceptoDTO;
								listaConceptosPorAgrupacion.add(saldoConceptoDTO);
							}
							estadoCuentaDecoradoDTO.getSaldosAcumulados().add(saldoConceptoTMP);
						}
				}
			}
			estadoCuentaDecoradoDTO.setSaldoTecnico(UtileriasCalculoEstadoCuenta.obtenerSaldosAcumulados(estadoCuentaDecoradoDTO.getSaldosAcumulados(), null));
		}
		return estadoCuentaDecoradoDTO;
	}
	
	private void combinarListaConceptos(EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO,List<EstadoCuentaDecoradoDTO> listaEstadoCuentaDecorado,
			List<SaldoConceptoDTO> listaSaldosConcepto,List<AgrupacionEstadoCuentaDTO> listaAgrupaciones,boolean omitirBanderaAplicaDesglose,
			boolean incluirFiltradoPorMes,List<Date> mesesPorIncluir){
		List<SaldoConceptoDTO> listaConceptosPorAgrupacion = new ArrayList<SaldoConceptoDTO>();
		for(SaldoConceptoDTO saldoConceptoDTO : listaSaldosConcepto){
			SaldoConceptoDTO saldoConceptoTMP = null;
			if(saldoConceptoDTO instanceof SaldoAgrupacionDTO){
				saldoConceptoTMP = UtileriasCalculoEstadoCuenta.obtenerSaldoPorAgrupacion(listaConceptosPorAgrupacion);
				listaConceptosPorAgrupacion.clear();
				estadoCuentaDecoradoDTO.getSaldosAcumulados().add(saldoConceptoTMP);
			}
			else{
				if(saldoConceptoDTO.isAplicaDesglosePorEjercicio() && !omitirBanderaAplicaDesglose){
					//Consultar todos los saldos del concepto y aplicar
					for(EstadoCuentaDecoradoDTO estadoCuentaDecoradoTMP : listaEstadoCuentaDecorado){
						List<EstadoCuentaDecoradoDTO> listaEstadoCuentaDecoradoTMP = new ArrayList<EstadoCuentaDecoradoDTO>();
						listaEstadoCuentaDecoradoTMP.add(estadoCuentaDecoradoTMP);
						saldoConceptoTMP = obtenerSaldoPorConcepto(saldoConceptoDTO, listaEstadoCuentaDecoradoTMP,listaAgrupaciones);
						if(saldoConceptoTMP != null){
							saldoConceptoTMP.setDescripcionConcepto(saldoConceptoTMP.getDescripcionConcepto()+" "+
									estadoCuentaDecoradoTMP.getEjercicio()+" - "+(estadoCuentaDecoradoTMP.getEjercicio()+1));
							listaConceptosPorAgrupacion.add(saldoConceptoTMP);
							
							saldoConceptoTMP.setOcultarSaldos(saldoConceptoDTO.isOcultarSaldos());
							saldoConceptoTMP.setMostrarSaldoAcumulado(saldoConceptoDTO.isMostrarSaldoAcumulado());
							saldoConceptoTMP.setResaltarConcepto(saldoConceptoDTO.isResaltarConcepto());
							estadoCuentaDecoradoDTO.getSaldosAcumulados().add(saldoConceptoTMP);
						}
					}
				}
				else{
					saldoConceptoTMP = obtenerSaldoPorConcepto(saldoConceptoDTO, listaEstadoCuentaDecorado,listaAgrupaciones);
					listaConceptosPorAgrupacion.add(saldoConceptoTMP);
					
					saldoConceptoTMP.setOcultarSaldos(saldoConceptoDTO.isOcultarSaldos());
					saldoConceptoTMP.setMostrarSaldoAcumulado(saldoConceptoDTO.isMostrarSaldoAcumulado());
					saldoConceptoTMP.setResaltarConcepto(saldoConceptoDTO.isResaltarConcepto());
					estadoCuentaDecoradoDTO.getSaldosAcumulados().add(saldoConceptoTMP);
				}
			}
		}
		//02/12/2010 Si se el estado de cuenta contiene la lista de saldos por mes, incluir la combinaci�n de saldos mensuales.
		if(incluirFiltradoPorMes && mesesPorIncluir != null && !mesesPorIncluir.isEmpty()){
			boolean aplicarFiltradoPorMes = false;
			for(EstadoCuentaDecoradoDTO edoCtaTMP : listaEstadoCuentaDecorado){
				if(edoCtaTMP.getListaSaldosAcumuladosPorMes() != null && !edoCtaTMP.getListaSaldosAcumuladosPorMes().isEmpty() &&
						edoCtaTMP.getSaldosAcumuladosPorMes() != null && !edoCtaTMP.getSaldosAcumuladosPorMes().isEmpty() && 
						edoCtaTMP.getSaldosAcumuladosPorMes().size() == edoCtaTMP.getListaSaldosAcumuladosPorMes().size()){
					aplicarFiltradoPorMes = true;
				}
				else{
					aplicarFiltradoPorMes = false;
					break;
				}
			}
			
			if(aplicarFiltradoPorMes){
				List<List<SaldoConceptoDTO>> listaSaldoConceptoCombinadosPorMes = new ArrayList<List<SaldoConceptoDTO>>();
				List<SaldoConceptoDTO> listaSaldoTecnicoPorMes = new ArrayList<SaldoConceptoDTO>();
				for(int i=0;i<listaEstadoCuentaDecorado.get(0).getSaldosAcumuladosPorMes().size();i++){
					listaSaldoConceptoCombinadosPorMes.add(i,new ArrayList<SaldoConceptoDTO>());
					
					SaldoConceptoDTO saldoTecnicoListaI = listaEstadoCuentaDecorado.get(0).getSaldosAcumuladosPorMes().get(i);
					SaldoConceptoDTO saldoTecnicoTotalListaI = new SaldoConceptoDTO();
					saldoTecnicoTotalListaI.setDebe(BigDecimal.ZERO);
					saldoTecnicoTotalListaI.setHaber(BigDecimal.ZERO);
					saldoTecnicoTotalListaI.setSaldo(BigDecimal.ZERO);
					saldoTecnicoTotalListaI.setSaldoAcumulado(BigDecimal.ZERO);
					saldoTecnicoTotalListaI.setDescripcionConcepto(saldoTecnicoListaI.getDescripcionConcepto());
					saldoTecnicoTotalListaI.setFechaTrimestre(saldoTecnicoListaI.getFechaTrimestre());
					saldoTecnicoTotalListaI.setMesPertenceSaldo(saldoTecnicoListaI.getMesPertenceSaldo());
					
					for(int j=0;j<listaEstadoCuentaDecorado.get(0).getListaSaldosAcumuladosPorMes().get(0).size();j++){
						
						//Duplicar el saldo para incluirlo en la nueva lista
						SaldoConceptoDTO saldoEdoCta1 = listaEstadoCuentaDecorado.get(0).getListaSaldosAcumuladosPorMes().get(i).get(j);
						
						SaldoConceptoDTO saldoNuevo = new SaldoConceptoDTO();
						saldoNuevo.setDebe(BigDecimal.ZERO);
						saldoNuevo.setHaber(BigDecimal.ZERO);
						saldoNuevo.setSaldo(BigDecimal.ZERO);
						saldoNuevo.setSaldoAcumulado(BigDecimal.ZERO);
						saldoNuevo.setDescripcionConcepto(saldoEdoCta1.getDescripcionConcepto());
						saldoNuevo.setFechaTrimestre(saldoEdoCta1.getFechaTrimestre());
						saldoNuevo.setMesPertenceSaldo(saldoEdoCta1.getMesPertenceSaldo());
						saldoNuevo.setMostrarSaldoAcumulado(saldoEdoCta1.isMostrarSaldoAcumulado());
						
						for(int k=0;k<listaEstadoCuentaDecorado.size();k++){
							SaldoConceptoDTO saldoEdoCtaIterado = listaEstadoCuentaDecorado.get(k).getListaSaldosAcumuladosPorMes().get(i).get(j);
							saldoNuevo.setDebe(saldoNuevo.getDebe().add(saldoEdoCtaIterado.getDebe()));
							saldoNuevo.setHaber(saldoNuevo.getHaber().add(saldoEdoCtaIterado.getHaber()));
							saldoNuevo.setSaldo(saldoNuevo.getSaldo().add(saldoEdoCtaIterado.getSaldo()));
							
							SaldoConceptoDTO saldoTecnicoEdoCtaIterado = listaEstadoCuentaDecorado.get(k).getSaldosAcumuladosPorMes().get(i);
//							saldoTecnicoListaI.setDebe(saldoTecnicoListaI.getDebe().add(saldoTecnicoEdoCtaIterado.getDebe()));
//							saldoTecnicoListaI.setHaber(saldoTecnicoListaI.getHaber().add(saldoTecnicoEdoCtaIterado.getHaber()));
							saldoTecnicoListaI.setSaldo(saldoTecnicoListaI.getSaldo().add(saldoTecnicoEdoCtaIterado.getSaldo()));
							saldoTecnicoListaI.setSaldoAcumulado(saldoTecnicoListaI.getSaldoAcumulado().add(saldoTecnicoEdoCtaIterado.getSaldoAcumulado()));
						}
						
						listaSaldoConceptoCombinadosPorMes.get(i).add(j,saldoNuevo);
					}
					
					listaSaldoTecnicoPorMes.add(i,saldoTecnicoListaI);
				}
			estadoCuentaDecoradoDTO.setListaSaldosAcumuladosPorMes(listaSaldoConceptoCombinadosPorMes);
			estadoCuentaDecoradoDTO.setSaldosAcumuladosPorMes(listaSaldoTecnicoPorMes);//9 elementos, deberian ser solo 3
			}
		}
	}
	
	private List<SaldoConceptoDTO> calcularSaldosAcumulados(EstadoCuentaDTO estadoCuentaDTO,BigDecimal idToReporteSiniestro,BigDecimal idTcReasegurador,BigDecimal idMoneda, BigDecimal[] idTcSubRamos,
			List<AgrupacionEstadoCuentaDTO> listaAgrupaciones,boolean incluirFiltradoHasta,boolean incluirFiltradoPorMes,Integer mes,Integer anio,Boolean mostrarSaldoAcumulado,boolean forzarMostrarSubTotalPorAgrupacion){
		List<SaldoConceptoDTO> listaSaldosCalculados = new ArrayList<SaldoConceptoDTO>();
		SaldoConceptoDTO saldoConceptoTMP;
		boolean mostrarSaldoAcumuladoLocal = (mostrarSaldoAcumulado != null ? mostrarSaldoAcumulado.booleanValue() : listaAgrupaciones.size() == 1);
		for(AgrupacionEstadoCuentaDTO agrupacionEstadoCuentaTMP : listaAgrupaciones){
			List<ConfiguracionConceptoEstadoCuentaDTO> listaConfiguracionConceptos = this.obtenerConfiguracionConceptosPorAgrupacion(agrupacionEstadoCuentaTMP);
			//Se guarda la lista de configuraciones de conceptos en cada agrupacion.
			agrupacionEstadoCuentaTMP.setConfiguracionConceptoEstadoCuentaList(listaConfiguracionConceptos);
			//Mostar/Ocultar el encabezado del grupo de conceptos
			if(agrupacionEstadoCuentaTMP.getClaveMostrarEncabezado().shortValue() == AgrupacionEstadoCuentaDTO.MOSTRAR_ENCABEZADO.shortValue()){
				saldoConceptoTMP = new SaldoConceptoDTO(agrupacionEstadoCuentaTMP.getNombreAgrupacion(), "",0, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO);
				saldoConceptoTMP.setResaltarConcepto(true);
				saldoConceptoTMP.setOcultarSaldos(true);
				listaSaldosCalculados.add(saldoConceptoTMP);
			}
//			List<ConfiguracionConceptoEstadoCuentaDTO> listaConfiguracionConceptos = agrupacionEstadoCuentaTMP.getConfiguracionConceptoEstadoCuentaList();
			//ordenar la lista de saldos tomando como criterio el campo "orden"
			Collections.sort(listaConfiguracionConceptos);
			List<SaldoConceptoDTO> listaSaldosPorConfiguracionAgrupacion = new ArrayList<SaldoConceptoDTO>();
			for(ConfiguracionConceptoEstadoCuentaDTO configuracionConceptoTMP : listaConfiguracionConceptos){
					//Por cada configuraci�n de concepto se genera un saldo
				saldoConceptoTMP = calcularSaldoPorConfiguracionConcepto(estadoCuentaDTO,
						idToReporteSiniestro,idTcReasegurador,idMoneda,idTcSubRamos, configuracionConceptoTMP,incluirFiltradoHasta,incluirFiltradoPorMes,mes,anio);
				saldoConceptoTMP.setMostrarSaldoAcumulado(mostrarSaldoAcumuladoLocal);
				listaSaldosCalculados.add(saldoConceptoTMP);
				listaSaldosPorConfiguracionAgrupacion.add(saldoConceptoTMP);
			}
			//Mostar/Ocultar el encabezado del subtotal del grupo de conceptos
			if(forzarMostrarSubTotalPorAgrupacion || 
					(agrupacionEstadoCuentaTMP.getClaveMostrarSubTotal().shortValue() == AgrupacionEstadoCuentaDTO.MOSTRAR_SUBTOTAL.shortValue())){
				saldoConceptoTMP = UtileriasCalculoEstadoCuenta.obtenerSaldoPorAgrupacion(listaSaldosPorConfiguracionAgrupacion);
				listaSaldosCalculados.add(saldoConceptoTMP);
			}
		}
		return listaSaldosCalculados;
	}
	
	private SaldoConceptoDTO calcularSaldoPorConfiguracionConcepto(EstadoCuentaDTO estadoCuentaDTO,
			BigDecimal idToReporteSiniestro,BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal[] idTcSubRamos,
			ConfiguracionConceptoEstadoCuentaDTO configuracionConceptoDTO,boolean incluirFiltradoHasta,boolean incluirFiltradoPorMes,Integer mes,Integer anio){
		SaldoConceptoDTO saldoConceptoDTO = new SaldoConceptoDTO(configuracionConceptoDTO.getNombreConcepto(), "",configuracionConceptoDTO.getIdTcConfiguracionConcepto(), BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO);
		saldoConceptoDTO.setAplicaDesglosePorEjercicio(
				configuracionConceptoDTO.getAplicaDesglocePorEjercicio() != null && configuracionConceptoDTO.getAplicaDesglocePorEjercicio().intValue() == 1);
		ConceptoMovimientoDTO conceptoMovimientoAcumulador = null;
		BigDecimal sumaAbono = BigDecimal.ZERO;
		BigDecimal sumaCargo = BigDecimal.ZERO;
		
		List<AcumuladorDTO> listaAcumuladores = null;
		if(estadoCuentaDTO != null){
			listaAcumuladores = estadoCuentaDTO.getAcumuladorDTOs();
			if (listaAcumuladores == null || listaAcumuladores.isEmpty()){
				listaAcumuladores = estadoCuentaFacade.obtenerAcumuladoresPorIdEstadoCta(estadoCuentaDTO.getIdEstadoCuenta());
			}
		}
		boolean estadoCuentaFacultativo = 
			configuracionConceptoDTO.getAgrupacionEstadoCuentaDTO().getClaveTipoEstadoCuenta().intValue() == 
				AgrupacionEstadoCuentaDTO.TIPO_FACULTATIVO.intValue();
		List<DetalleConfiguracionEstadoCuentaDTO> listaDetalleConcepto = configuracionConceptoDTO.getDetalleConfiguracionEstadoCuentaDTOs();
		Integer naturaleza = configuracionConceptoDTO.getNaturaleza();
		//La combinaci�n de los detalles conforma el saldo total del concepto, tomando en cuenta la operaci�n de cada detalle (signo)
		for(DetalleConfiguracionEstadoCuentaDTO detalleConceptoTMP : listaDetalleConcepto){
			Integer aplicaConceptoDetalle = detalleConceptoTMP.getClaveAplicaConceptoDetalle();
			BigDecimal idTcConceptoDetalle = detalleConceptoTMP.getIdTcConceptoDetalle();
			if(aplicaConceptoDetalle != null && aplicaConceptoDetalle.intValue() == 1 && 
					idTcConceptoDetalle != null && idTcConceptoDetalle.intValue() != 0){
				//Consultar el saldo a nivel de movimientos, aplica s�lo para el caso de siniestros
				if ((idTcReasegurador == null && idToReporteSiniestro != null) || //Consulta del saldo por siniestro 
						(idTcReasegurador != null && idToReporteSiniestro == null ) ||//Consulta del saldo por reasegurador
						(idTcReasegurador != null && idToReporteSiniestro != null ) ){//Consulta del saldo por reasegurador y siniestro
					if(detalleConceptoTMP.getClaveTipoOperacion().intValue() == DetalleConfiguracionEstadoCuentaDTO.CLAVE_OPERACION_SUMA){
						if(naturaleza == ConceptoMovimientoDTO.TIPO_NATURALEZA_HABER){
							sumaAbono = sumaAbono.add(obtenerSaldoSiniestro(idToReporteSiniestro,idTcReasegurador,idMoneda,idTcSubRamos,idTcConceptoDetalle, 
									estadoCuentaFacultativo, estadoCuentaFacultativo,incluirFiltradoHasta,mes,anio));
						}
						else{
							sumaCargo = sumaCargo.add(obtenerSaldoSiniestro(idToReporteSiniestro,idTcReasegurador,idMoneda,idTcSubRamos,idTcConceptoDetalle, 
									estadoCuentaFacultativo, estadoCuentaFacultativo,incluirFiltradoHasta,mes,anio));
						}
					}
					else if(detalleConceptoTMP.getClaveTipoOperacion().intValue() == DetalleConfiguracionEstadoCuentaDTO.CLAVE_OPERACION_RESTA){
						if(naturaleza == ConceptoMovimientoDTO.TIPO_NATURALEZA_HABER){
							sumaCargo = sumaCargo.add(obtenerSaldoSiniestro(idToReporteSiniestro,idTcReasegurador,idMoneda,idTcSubRamos,idTcConceptoDetalle, 
									estadoCuentaFacultativo, estadoCuentaFacultativo,incluirFiltradoHasta,mes,anio));
						}
						else{
							sumaAbono = sumaAbono.add(obtenerSaldoSiniestro(idToReporteSiniestro,idTcReasegurador,idMoneda,idTcSubRamos,idTcConceptoDetalle,
									estadoCuentaFacultativo, estadoCuentaFacultativo,incluirFiltradoHasta,mes,anio));
						}
					}
				}
			}else{
				//Consultar el saldo a nivel de acumuladores
				if(estadoCuentaDTO != null){
					for (AcumuladorDTO acumuladorDTO : listaAcumuladores) {
						boolean aplicaAfectacionAcumuladorEnCurso = false;
						if(incluirFiltradoHasta && mes!= null && anio != null && 
								mes.intValue()>0 && mes.intValue()<=12 && anio.intValue()>1900 ){
							if(acumuladorDTO.getMes() <= mes.intValue() && acumuladorDTO.getAnio() <= anio.intValue()){
								aplicaAfectacionAcumuladorEnCurso=true;
							}
						}
						else{
							if(incluirFiltradoPorMes && mes!= null && anio != null && 
								mes.intValue()>0 && mes.intValue()<=12 && anio.intValue()>1900){
								if(acumuladorDTO.getMes() == mes.intValue() && acumuladorDTO.getAnio() == anio.intValue()){
									aplicaAfectacionAcumuladorEnCurso=true;
								}
							}
							else
								aplicaAfectacionAcumuladorEnCurso=true;
						}
						if(aplicaAfectacionAcumuladorEnCurso){
							conceptoMovimientoAcumulador = acumuladorDTO.getConceptoMovimientoDTO();
							//Si el acumulador en curso es del concepto en curso, sus saldos se acumulan.
							if(conceptoMovimientoAcumulador.getIdConceptoMovimiento() == detalleConceptoTMP.getConceptoMovimientoDTO().getIdConceptoMovimiento()) {
								sumaAbono = sumaAbono.add(acumuladorDTO.getAbono());
								sumaCargo = sumaCargo.add(acumuladorDTO.getCargo());
				            }
						}
					}
				}
				else if (idToReporteSiniestro != null){
					sumaAbono = sumaAbono.add(obtenerSaldoSiniestro(idToReporteSiniestro,idTcReasegurador,detalleConceptoTMP.getConceptoMovimientoDTO().getIdConceptoMovimiento(),
							TipoMovimientoDTO.NATURALEZA_ABONO,estadoCuentaFacultativo));
					sumaCargo = sumaCargo.add(obtenerSaldoSiniestro(idToReporteSiniestro,idTcReasegurador,detalleConceptoTMP.getConceptoMovimientoDTO().getIdConceptoMovimiento(),
							TipoMovimientoDTO.NATURALEZA_CARGO,estadoCuentaFacultativo));
				}
			}
		}
		//Ya que se tiene el debe y haber, se calcula el saldo individual considerando la naturaleza
		calcularSaldosEnBaseADebeYHaber(saldoConceptoDTO, naturaleza, sumaAbono, sumaCargo);
		return saldoConceptoDTO;
	}
	
	private void calcularSaldosEnBaseADebeYHaber(SaldoConceptoDTO saldoConceptoDTO,int naturaleza,BigDecimal sumaAbono,BigDecimal sumaCargo){
		if(saldoConceptoDTO != null){
			BigDecimal saldoCalculado = BigDecimal.ZERO;
			BigDecimal saldoCalculadoPorAcumular = null;
			
			saldoCalculado = naturaleza == ConceptoMovimientoDTO.TIPO_NATURALEZA_HABER ? (sumaAbono.subtract(sumaCargo)) : (sumaCargo.subtract(sumaAbono));
			saldoCalculadoPorAcumular = saldoCalculado;
			
			if(naturaleza != ConceptoMovimientoDTO.TIPO_NATURALEZA_HABER)
		    	saldoCalculadoPorAcumular = saldoCalculadoPorAcumular.multiply(new BigDecimal(-1d));
		    saldoConceptoDTO.setSaldo(saldoCalculadoPorAcumular);
		    
		    if (naturaleza == ConceptoMovimientoDTO.TIPO_NATURALEZA_HABER){
		    	if (saldoCalculado.compareTo(BigDecimal.ZERO) < 0)
		    		saldoConceptoDTO.setDebe(saldoCalculado.abs());
		    	else
		    		saldoConceptoDTO.setHaber(saldoCalculado);
		    }else if (naturaleza == ConceptoMovimientoDTO.TIPO_NATURALEZA_DEBE){
		    	if (saldoCalculado.compareTo(BigDecimal.ZERO) < 0)
		    		saldoConceptoDTO.setHaber(saldoCalculado.abs());
		    	else
		    		saldoConceptoDTO.setDebe(saldoCalculado);
		    }else if (naturaleza == ConceptoMovimientoDTO.TIPO_NATURALEZA_DEBE_HABER){
		    	saldoConceptoDTO.setHaber(sumaAbono);
		    	saldoConceptoDTO.setDebe(sumaCargo);
		    }
		}
	}
	
	private BigDecimal obtenerSaldoSiniestro(BigDecimal idToReporteSiniestro,BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal[] idTcSubRamos,BigDecimal idConceptoMovDetalle,
			boolean ignorarEstatusReporteSiniestro,boolean incluirSoloFacultativo,boolean incluirFiltradoHasta,Integer mes,Integer anio){
		BigDecimal saldoCalculado = BigDecimal.ZERO;
		incluirFiltradoHasta = mes != null && anio != null && mes>0 && mes<=12 && anio>1900;
		String descripcionSubRamos = "";
		boolean incluirFiltradoPorSubramos = idTcSubRamos != null && idTcSubRamos.length > 0;
		if(incluirFiltradoPorSubramos){
			descripcionSubRamos = obtenerDescripcionSubramos(idTcSubRamos);
			descripcionSubRamos = descripcionSubRamos.substring(0, descripcionSubRamos.length()-1);
		}
		if((idToReporteSiniestro != null || idTcReasegurador != null)  && idConceptoMovDetalle != null){
			String queryString = "select nvl(sum(mov.cantidad),0) saldoConcepto from " +
					"midas.tomovimientoreaseguro mov, midas.toreportesiniestro sin where " +
					(idToReporteSiniestro != null ? 
							"sin.idtoreportesiniestro = "+idToReporteSiniestro.toBigInteger().toString()+" and " : 
								"sin.idtoreportesiniestro > 0 and "+(idMoneda!=null?" mov.idMoneda = "+idMoneda.toBigInteger().toString()+" and " : "") )+
					"sin.idtoreportesiniestro = mov.idtoreportesiniestro and " +
					(incluirSoloFacultativo ? "mov.idtmcontratofacultativo is not null and " : "" )+
					(incluirFiltradoHasta ? "to_number(to_char(mov.fecharegistro,'mm')) <= "+mes.intValue()
							+" and to_number(to_char(mov.fecharegistro,'yyyy')) <= "+anio.intValue()+" and ":"") +
					(idTcReasegurador != null ? "mov.idtcreaseguradorcorredor = "+idTcReasegurador.toBigInteger().toString()+" and ":"")+
					(incluirFiltradoPorSubramos ? "mov.idtcsubramo in ("+descripcionSubRamos+") and " : "")+
					"mov.acumulado = 1 and "+
					"mov.idtcconceptomovimiento = %d /*in ( " +
						"select conceptoDet.idtcconceptodetalle from midas.tcconceptomovimientodetalle conceptoDet " +
						"where conceptoDet.IDTCCONCEPTODETALLE = x and " +
						"1 = (select (case " +
							"when conceptoDet.aplicaterminacionsiniestros = 0 then 1 " +
							"when 1 = %d then 1 " +
							"when conceptoDet.aplicaterminacionsiniestros = 1 and ( sin.idtcreporteestatus >= 24 ) then 1 " +
							"else 0 " +
							"end ) valor from dual ) )*/"; 
			int ignoraEstatusReporte = ignorarEstatusReporteSiniestro ? 1 : 0;
			String queryAbono = String.format(queryString, idConceptoMovDetalle.intValue(), ignoraEstatusReporte);
			Query query = entityManager.createNativeQuery(queryAbono);
			Object resultado = query.getSingleResult();
			saldoCalculado = Utilerias.obtenerBigDecimal(resultado);
		}
		return saldoCalculado;
	}
	
	public String obtenerDescripcionSubramos(BigDecimal[] idTcSubRamos) {
		StringBuilder descripcionSubRamos = new StringBuilder("");
		for(int i=0;i<idTcSubRamos.length;i++){
			descripcionSubRamos.append(idTcSubRamos[i].toBigInteger().toString()).append(",");
		}
		return descripcionSubRamos.toString();
	}

	/**
	 * Consulta el saldo por concepto para un siniestro y (opcionalmente) un reasegurador.
	 * @param idToReporteSiniestro
	 * @param idTcReasegurador
	 * @param idConceptoMovimiento
	 * @param naturaleza
	 * @param incluirSoloFacultativo
	 * @return
	 */
	private BigDecimal obtenerSaldoSiniestro(BigDecimal idToReporteSiniestro,BigDecimal idTcReasegurador,int idConceptoMovimiento,int naturaleza,boolean incluirSoloFacultativo){
		BigDecimal saldoCalculado = BigDecimal.ZERO;
		if(idToReporteSiniestro != null){
			String queryString = "select nvl(sum(mov.cantidad),0) saldoConcepto from " +
					"midas.tomovimientoreaseguro mov, midas.toreportesiniestro sin,midas.tcTipoMovimiento tipoMov where " +
					"sin.idtoreportesiniestro = %d and " +
					"sin.idtoreportesiniestro = mov.idtoreportesiniestro and " +
					(incluirSoloFacultativo ? "mov.idtmcontratofacultativo is not null and ":"") +
					(idTcReasegurador != null ? "mov.idtcreaseguradorcorredor = "+idTcReasegurador.toBigInteger().toString()+" and " : "")+
					"mov.acumulado = 1 and "+
					"mov.idtcconceptomovimiento = %d /*in ( " +//Se omite validaci�n del estatus del siniestro, ya que basta con la bandera "acumulado"
						"select conceptoDet.idtcconceptodetalle from midas.tcconceptomovimientodetalle conceptoDet " +
						"where conceptoDet.idtcconceptomovimiento = x and conceptoDet.acumulaedocta = 1 and " +
							"1 = (select (case " +
								"when conceptoDet.aplicaterminacionsiniestros = 0 then 1 " +
								"when conceptoDet.aplicaterminacionsiniestros = 1 and ( sin.idtcreporteestatus >= 24 ) then 1 " +
								"else 0 " +
								"end " +
							") valor from dual ) )*/ and " +
					"mov.idtctipomovimiento = tipomov.idtctipomovimiento and " +
					"tipomov.naturaleza = %d";
			
			String queryAbono = String.format(queryString, idToReporteSiniestro.intValue(), idConceptoMovimiento, naturaleza);
			Query query = entityManager.createNativeQuery(queryAbono);
			Object resultado = query.getSingleResult();
			saldoCalculado = Utilerias.obtenerBigDecimal(resultado);
		}
		return saldoCalculado;
	}
	
	@SuppressWarnings("unchecked")
	private List<BigDecimal> obtenerListaIdTcReaseguradoresSiniestro(BigDecimal idToReporteSiniestro,boolean incluirSoloFacultativo){
		String queryString = "select distinct mov.idtcreaseguradorcorredor from " +
				"midas.tomovimientoreaseguro mov, midas.toreportesiniestro sin where " +
				"sin.idtoreportesiniestro = %d and " +
				"sin.idtoreportesiniestro = mov.idtoreportesiniestro " +
				(incluirSoloFacultativo? "and mov.idtmcontratofacultativo is not null " : "");
		String queryReaseguradores = String.format(queryString, idToReporteSiniestro.intValue());
		Query query = entityManager.createNativeQuery(queryReaseguradores);
		List<Object> listaResultado= query.getResultList();
		List<BigDecimal> listaIdTcReasegurador = new ArrayList<BigDecimal>();
		for(Object singleResult : listaResultado)
			listaIdTcReasegurador.add(Utilerias.obtenerBigDecimal(singleResult));
		return listaIdTcReasegurador;
	}
	
	@SuppressWarnings("unchecked")
	private List<ConfiguracionConceptoEstadoCuentaDTO> obtenerConfiguracionConceptosPorAgrupacion(AgrupacionEstadoCuentaDTO agrupacionEstadoCuentaDTO){
		String queryString = "select model from ConfiguracionConceptoEstadoCuentaDTO model where model.agrupacionEstadoCuentaDTO.idTcAgrupacionEstadoCuenta = :idTcAgrupacionEstadoCuenta";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTcAgrupacionEstadoCuenta", agrupacionEstadoCuentaDTO.getIdTcAgrupacionEstadoCuenta());
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
//	private boolean validarEstadoCuenta(EstadoCuentaDTO estadoCuentaDTO,boolean consultaAcumuladores){
//		boolean estadoCuentaValido = false;
//		if(estadoCuentaDTO != null && estadoCuentaDTO.getIdEstadoCuenta() != null){
//			estadoCuentaValido = true;
//			if(consultaAcumuladores && (estadoCuentaDTO.getAcumuladorDTOs() == null || estadoCuentaDTO.getAcumuladorDTOs().isEmpty())){
//				estadoCuentaDTO.setAcumuladorDTOs(estadoCuentaFacade.obtenerAcumuladoresPorIdEstadoCta(estadoCuentaDTO.getIdEstadoCuenta()));
//			}
//		}
//		return estadoCuentaValido;
//	}

}
