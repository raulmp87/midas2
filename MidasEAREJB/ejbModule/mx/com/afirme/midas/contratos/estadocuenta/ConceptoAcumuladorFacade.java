package mx.com.afirme.midas.contratos.estadocuenta;
// default package

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.estadocuenta.ConceptoAcumuladorDTO;
import mx.com.afirme.midas.contratos.estadocuenta.ConceptoAcumuladorDTOId;
import mx.com.afirme.midas.contratos.estadocuenta.ConceptoAcumuladorFacadeRemote;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity ConceptoAcumuladorDTO.
 * @see .ConceptoAcumuladorDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ConceptoAcumuladorFacade  implements ConceptoAcumuladorFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ConceptoAcumuladorDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ConceptoAcumuladorDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ConceptoAcumuladorDTO entity) {
    				LogUtil.log("saving ConceptoAcumuladorDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ConceptoAcumuladorDTO entity.
	  @param entity ConceptoAcumuladorDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ConceptoAcumuladorDTO entity) {
    				LogUtil.log("deleting ConceptoAcumuladorDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ConceptoAcumuladorDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ConceptoAcumuladorDTO entity and return it or a copy of it to the sender. 
	 A copy of the ConceptoAcumuladorDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ConceptoAcumuladorDTO entity to update
	 @return ConceptoAcumuladorDTO the persisted ConceptoAcumuladorDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ConceptoAcumuladorDTO update(ConceptoAcumuladorDTO entity) {
    				LogUtil.log("updating ConceptoAcumuladorDTO instance", Level.INFO, null);
	        try {
            ConceptoAcumuladorDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ConceptoAcumuladorDTO findById( ConceptoAcumuladorDTOId id) {
    				LogUtil.log("finding ConceptoAcumuladorDTO instance with id: " + id, Level.INFO, null);
	        try {
            ConceptoAcumuladorDTO instance = entityManager.find(ConceptoAcumuladorDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ConceptoAcumuladorDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ConceptoAcumuladorDTO property to query
	  @param value the property value to match
	  	  @return List<ConceptoAcumuladorDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ConceptoAcumuladorDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding ConceptoAcumuladorDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ConceptoAcumuladorDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ConceptoAcumuladorDTO entities.
	  	  @return List<ConceptoAcumuladorDTO> all ConceptoAcumuladorDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConceptoAcumuladorDTO> findAll(
		) {
					LogUtil.log("finding all ConceptoAcumuladorDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ConceptoAcumuladorDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}