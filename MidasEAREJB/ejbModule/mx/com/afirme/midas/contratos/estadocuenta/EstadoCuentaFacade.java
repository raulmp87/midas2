package mx.com.afirme.midas.contratos.estadocuenta;
// default package

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo.ParticipacionCorredorFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.linea.CalculoSuscripcionesDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.contratos.linea.SuscripcionDTO;
import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoMovimientoDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;


/**
 * Facade for entity EstadoCuentaDTO.
 * @see .EstadoCuentaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class EstadoCuentaFacade  implements EstadoCuentaFacadeRemote {

    @PersistenceContext private EntityManager entityManager;

//    @EJB
//    private ContratoCuotaParteFacadeRemote contratoCuotaParteFacade;
//    
//    @EJB
//    private ContratoPrimerExcedenteFacadeRemote contratoPrimerExcedenteFacade;
		/**
	 Perform an initial save of a previously unsaved EstadoCuentaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EstadoCuentaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EstadoCuentaDTO entity) {
    				LogDeMidasEJB3.log("saving EstadoCuentaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent EstadoCuentaDTO entity.
	  @param entity EstadoCuentaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EstadoCuentaDTO entity) {
    				LogDeMidasEJB3.log("deleting EstadoCuentaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(EstadoCuentaDTO.class, entity.getIdEstadoCuenta());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved EstadoCuentaDTO entity and return it or a copy of it to the sender. 
	 A copy of the EstadoCuentaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EstadoCuentaDTO entity to update
	 @return EstadoCuentaDTO the persisted EstadoCuentaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public EstadoCuentaDTO update(EstadoCuentaDTO entity) {
    				LogDeMidasEJB3.log("updating EstadoCuentaDTO instance", Level.INFO, null);
	        try {
            EstadoCuentaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public EstadoCuentaDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding EstadoCuentaDTO instance with id: " + id, Level.INFO, null);
	        try {
            EstadoCuentaDTO instance = entityManager.find(EstadoCuentaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all EstadoCuentaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EstadoCuentaDTO property to query
	  @param value the property value to match
	  	  @return List<EstadoCuentaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<EstadoCuentaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding EstadoCuentaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from EstadoCuentaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all EstadoCuentaDTO entities.
	  	  @return List<EstadoCuentaDTO> all EstadoCuentaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<EstadoCuentaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all EstadoCuentaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from EstadoCuentaDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	/**
	 * Determina el estado de cuenta asociado para el movimiento de reaseguro indicado.
	 * En caso de no exitir un estado de cuenta asociado regresar null
	 * @param movimiento
	 * @return EstadoCuentaDTO
	 */	
	@SuppressWarnings("unchecked")
	public EstadoCuentaDTO obtenerEstadoCuentaPorMovimiento(
			MovimientoReaseguroDTO movimiento) {
		EstadoCuentaDTO estadoCuenta = null;
		List<EstadoCuentaDTO> estadoCuentaDTOList;
		/*		
		 * Si tipoReaseguro puede ser
		 *      TipoReaseguroDTO. TIPO_CONTRATO_CUOTA_PARTE
		 *		TipoReaseguroDTO. TIPO_CONTRATO_PRIMER_EXCEDENTE
		 *		TipoReaseguroDTO. TIPO_CONTRATO_FACULTATIVO
		 *		TipoReaseguroDTO. TIPO_RETENCION
	 	 *		
		*/		
 		
		if (movimiento.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			if (movimiento.getReaseguradorCorredor() != null){ 
			 String queryString = "select model from EstadoCuentaDTO model where ";
			 queryString += "model.subRamoDTO.idTcSubRamo = :idTcSubRamo and ";
			 queryString += "model.tipoReaseguro = :tipoReaseguro and ";
			 queryString += "model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idTcCreaseguradorCorredor and ";
			 queryString += "model.contratoCuotaParteDTO.idTmContratoCuotaParte = :idTmContratoCoutaParte and ";
			 queryString += "model.idMoneda = :idMoneda and ";
			 queryString += "model.ejercicio = :ejercicio and ";
			 queryString += "model.suscripcion = :suscripcion";
			  Query query = entityManager.createQuery(queryString);
			 query.setParameter("idTcSubRamo",movimiento.getSubRamo().getId());
			 query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE);	
			 query.setParameter("idTcCreaseguradorCorredor",movimiento.getReaseguradorCorredor().getIdtcreaseguradorcorredor());
			 query.setParameter("idTmContratoCoutaParte",movimiento.getContratoCuotaParteDTO().getIdTmContratoCuotaParte());
			 query.setParameter("idMoneda",movimiento.getIdMoneda());
			 query.setParameter("ejercicio",movimiento.getEjercicio());
			 query.setParameter("suscripcion",movimiento.getSuscripcion());
			 estadoCuentaDTOList = query.getResultList();
			  if(estadoCuentaDTOList.size() == 1){
			    estadoCuenta= estadoCuentaDTOList.get(0);
	 		 }else if(estadoCuentaDTOList.size() != 0){
	 			throw new RuntimeException("Mas de un estado de cuenta fue obtenido"); 
	  		 }
		  }else{
			String queryString = "select model from EstadoCuentaDTO model where ";
			queryString += "model.subRamoDTO.idTcSubRamo = :idTcSubRamo and ";
			queryString += "model.lineaDTO.idTmLinea = :idTmLinea and ";
			queryString += "model.tipoReaseguro = :tipoReaseguro and ";
			queryString += "model.contratoCuotaParteDTO.idTmContratoCuotaParte = :idTmContratoCuotaParte and ";
			queryString += "model.idMoneda = :idMoneda and ";
			queryString += "model.ejercicio = :ejercicio and ";
			queryString += "model.suscripcion = :suscripcion and ";
			queryString += "model.reaseguradorCorredorDTO.idtcreaseguradorcorredor is null";
		 	Query query = entityManager.createQuery(queryString);
			query.setParameter("idTcSubRamo",movimiento.getSubRamo().getIdTcSubRamo());
			query.setParameter("idTmLinea",movimiento.getLineaDTO().getIdTmLinea());
			query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE);	
			query.setParameter("idTmContratoCuotaParte",movimiento.getContratoCuotaParteDTO().getIdTmContratoCuotaParte());
			query.setParameter("idMoneda",movimiento.getIdMoneda());
			query.setParameter("ejercicio",movimiento.getEjercicio());
			query.setParameter("suscripcion",movimiento.getSuscripcion());
			estadoCuentaDTOList = query.getResultList();
	 		if(estadoCuentaDTOList.size() == 1){
			    estadoCuenta= estadoCuentaDTOList.get(0);
	 		 }else if(estadoCuentaDTOList.size() != 0){
	 			throw new RuntimeException("Mas de un estado de cuenta fue obtenido"); 
	  		 }
		  }
		}else{
			if (movimiento.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
				String queryString = "select model from EstadoCuentaDTO model where ";
				queryString += "model.subRamoDTO.idTcSubRamo = :idTcSubRamo and ";
				queryString += "model.tipoReaseguro = :tipoReaseguro and ";
				queryString += "model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idTcCreaseguradorCorredor and ";
				queryString += "model.contratoPrimerExcedenteDTO.idTmContratoPrimerExcedente = :idTmContratoPrimerExcedente and ";
				queryString += "model.idMoneda = :idMoneda and ";
				queryString += "model.ejercicio = :ejercicio and ";
				queryString += "model.suscripcion = :suscripcion";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idTcSubRamo",movimiento.getSubRamo().getId());
				query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE);	
				query.setParameter("idTcCreaseguradorCorredor",movimiento.getReaseguradorCorredor().getIdtcreaseguradorcorredor());
				query.setParameter("idTmContratoPrimerExcedente",movimiento.getContratoPrimerExcedenteDTO().getIdTmContratoPrimerExcedente());
				query.setParameter("idMoneda",movimiento.getIdMoneda());
				query.setParameter("ejercicio",movimiento.getEjercicio());
				query.setParameter("suscripcion",movimiento.getSuscripcion());
				estadoCuentaDTOList = query.getResultList();
				if(estadoCuentaDTOList.size() == 1){
					estadoCuenta= estadoCuentaDTOList.get(0);
				}else if(estadoCuentaDTOList.size() != 0){
					throw new RuntimeException("Mas de un estado de cuenta fue obtenido"); 
				}
			}else{
				if (movimiento.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
					if (movimiento.getReaseguradorCorredor() != null){ 
						String queryString = "select model from EstadoCuentaDTO model where ";
						queryString += "model.subRamoDTO.idTcSubRamo = :idTcSubRamo and ";
						queryString += "model.tipoReaseguro = :tipoReaseguro and ";
						queryString += "model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idTcCreaseguradorCorredor and ";
						queryString += "model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo and ";
						queryString += "model.idMoneda = :idMoneda and ";
						queryString += "model.idPoliza = :idPoliza and ";
						queryString += "model.numeroEndoso = :numeroEndoso and ";
						queryString += "model.ejercicio = :ejercicio and ";
						queryString += "model.suscripcion = :suscripcion";
						if(movimiento.getCorredor() != null && movimiento.getCorredor().getIdtcreaseguradorcorredor() != null)
							queryString += " and model.corredorDTO.idtcreaseguradorcorredor = :idTcCorredor ";
						Query query = entityManager.createQuery(queryString);
						query.setParameter("idTcSubRamo",movimiento.getSubRamo().getIdTcSubRamo());
						query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);	
						query.setParameter("idTcCreaseguradorCorredor",movimiento.getReaseguradorCorredor().getIdtcreaseguradorcorredor());
						query.setParameter("idTmContratoFacultativo",movimiento.getContratoFacultativoDTO().getIdTmContratoFacultativo());
						query.setParameter("idMoneda",movimiento.getIdMoneda());
						query.setParameter("idPoliza",movimiento.getIdPoliza());
						query.setParameter("numeroEndoso",movimiento.getNumeroEndoso());
						query.setParameter("ejercicio",movimiento.getEjercicio());
						query.setParameter("suscripcion",movimiento.getSuscripcion());
						if(movimiento.getCorredor() != null && movimiento.getCorredor().getIdtcreaseguradorcorredor() != null)
							query.setParameter("idTcCorredor",movimiento.getCorredor().getIdtcreaseguradorcorredor());	
						estadoCuentaDTOList = query.getResultList();
						if(estadoCuentaDTOList.size() == 1){
							estadoCuenta= estadoCuentaDTOList.get(0);
						}else if(estadoCuentaDTOList.size() != 0){
							throw new RuntimeException("Mas de un estado de cuenta fue obtenido"); 
						}
					}else{
						String queryString = "select model from EstadoCuentaDTO model where ";
						queryString += "model.subRamoDTO.idTcSubRamo = :idTcSubRamo and ";
						queryString += "model.lineaDTO.idTmLinea = :idTmLinea and ";
						queryString += "model.tipoReaseguro = :tipoReaseguro and ";
						queryString += "model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo and ";
						queryString += "model.idMoneda = :idMoneda and ";
						queryString += "model.idPoliza = :idPoliza and ";
						queryString += "model.numeroEndoso = :numeroEndoso and ";
						queryString += "model.ejercicio = :ejercicio and ";
						queryString += "model.suscripcion = :suscripcion and ";
						queryString += "model.reaseguradorCorredorDTO.idtcreaseguradorcorredor is null";
						Query query = entityManager.createQuery(queryString);
						query.setParameter("idTcSubRamo",movimiento.getSubRamo().getIdTcSubRamo());
						query.setParameter("idTmLinea",movimiento.getLineaDTO().getIdTmLinea());
						query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);	
						query.setParameter("idTmContratoFacultativo",movimiento.getContratoFacultativoDTO().getIdTmContratoFacultativo());
						query.setParameter("idMoneda",movimiento.getIdMoneda());
						query.setParameter("idPoliza",movimiento.getIdPoliza());
						query.setParameter("numeroEndoso",movimiento.getNumeroEndoso());
						query.setParameter("ejercicio",movimiento.getEjercicio());
						query.setParameter("suscripcion",movimiento.getSuscripcion());
						estadoCuentaDTOList = query.getResultList();
						if(estadoCuentaDTOList.size() == 1){
							estadoCuenta= estadoCuentaDTOList.get(0);
						}else if(estadoCuentaDTOList.size() != 0){
							throw new RuntimeException("Mas de un estado de cuenta fue obtenido"); 
						}
					}
				}else{
					if (movimiento.getTipoReaseguro() == TipoReaseguroDTO.TIPO_RETENCION){
						String queryString = "select model from EstadoCuentaDTO model where ";
						queryString += "model.subRamoDTO.idTcSubRamo = :idTcSubRamo and ";
						queryString += "model.tipoReaseguro = :tipoReaseguro and ";
						queryString += "model.lineaDTO.idTmLinea = :idTmLinea and ";
						queryString += "model.idMoneda = :idMoneda and ";
						queryString += "model.ejercicio = :ejercicio and ";
						queryString += "model.suscripcion = :suscripcion";
						Query query = entityManager.createQuery(queryString);
						query.setParameter("idTcSubRamo",movimiento.getSubRamo().getId());
						query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_RETENCION);	
						query.setParameter("idTmLinea",movimiento.getLineaDTO().getIdTmLinea());
						query.setParameter("idMoneda",movimiento.getIdMoneda());
						query.setParameter("ejercicio",movimiento.getEjercicio());
						query.setParameter("suscripcion",movimiento.getSuscripcion());
						estadoCuentaDTOList = query.getResultList();
						if(estadoCuentaDTOList.size() == 1){
							estadoCuenta= estadoCuentaDTOList.get(0);
						}else if(estadoCuentaDTOList.size() != 0){
							throw new RuntimeException("Mas de un estado de cuenta fue obtenido");
						}
					}
				}
			}
		}
        return estadoCuenta;
	}
	
	/**
	 * 
	 * @param reasegurador
	 * @return
	 */
	public List<EstadoCuentaDTO> obtenerEstadosCuenta(ReaseguradorCorredorDTO reasegurador){
		return null;
	}
	
	/**
	 * 
	 * @param reasegurador
	 * @return
	 */
	public List<EstadoCuentaDTO> obtenerEstadosCuenta(ReaseguradorCorredorDTO reasegurador, Date fechaInicio, Date fechaFin){
		return null;
	}	
	
	@SuppressWarnings("unchecked")
	public List<EstadoCuentaDTO> obtenerEstadosCuenta(EstadoCuentaFiltroDTO filtro){
		String queryString = "SELECT edoCta.IDTOESTADOCUENTA, edoCta.IDTCSUBRAMO, edoCta.IDMONEDA, edoCta.IDTCREASEGURADOR, edoCta.IDTOPOLIZA, edoCta.IDTMCONTRATOCUOTAPARTE, " +
				                     "edoCta.IDTMCONTRATOPRIMEREXCEDENTE, edoCta.IDTMCONTRATOFACULTATIVO, edoCta.OBSERVACIONES, edoCta.IDTCTIPOREASEGURO, edoCta.IDLINEA, " +
				                     "edoCta.EJERCICIO, edoCta.SUSCRIPCION, edoCta.NUMEROENDOSO, edoCta.IDTCCORREDOR, edoCta.ESTATUSAUTPAGOFACEDOCUENTA, edoCta.FECHASOLAUTPAGOFACEDOCUENTA, " +
				                     "edoCta.USUARIOSOLAUTPAGOFACEDOCUENTA, edoCta.FECHAAUTPAGOFACEDOCUENTA, edoCta.USUARIOAUTPAGOFACEDOCUENTA " +
                             "FROM MIDAS.ToEstadoCuenta edoCta, MIDAS.TcSubRamo sRamo ";
		String sWhere = "WHERE edoCta.idTcSubRamo = sRamo.idTcSubRamo ";		
		Query query;
		
		List<EstadoCuentaDTO> listaEstadoCuentaEncontrados = null;
		if (filtro != null){
			//Consulta por id del estado de cuenta.
			if(filtro.getIdToEstadoCuenta() != null && filtro.getIdToEstadoCuenta().length > 0){
				sWhere = sWhere + " AND edoCta.idToEstadoCuenta in (";
				StringBuilder where = new StringBuilder(sWhere);
				boolean aplico = false;
				for(int i=0;i<filtro.getIdToEstadoCuenta().length; i++){
					if(filtro.getIdToEstadoCuenta()[i] != null){
						where.append(filtro.getIdToEstadoCuenta()[i].toString()).append(",");
						aplico = true;
					}
				}
				sWhere = where.toString();
				if (aplico){
					sWhere = sWhere.substring(0, sWhere.length()-1);
				}
				sWhere += ") ";
			}
			if (filtro.getTipoReaseguro() != null){
				sWhere = sWhere + " AND edoCta.idTcTipoReaseguro = " + filtro.getTipoReaseguro().toString();
				if (filtro.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
					sWhere = sWhere + " AND edoCta.idTmContratoCuotaParte IS NOT NULL ";			
					sWhere = sWhere + " AND edoCta.ejercicio = " + filtro.getEjercicio();
					if(filtro.getSuscripcion() != null)
						sWhere = sWhere + " AND edoCta.suscripcion = " + filtro.getSuscripcion().toString();					
				}else if (filtro.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
					sWhere = sWhere + " AND edoCta.idTmContratoPrimerExcedente IS NOT NULL";
					sWhere = sWhere + " AND edoCta.ejercicio = " + filtro.getEjercicio();
					if(filtro.getSuscripcion() != null)
						sWhere = sWhere + " AND edoCta.suscripcion = " + filtro.getSuscripcion().toString();				
				}else if (filtro.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
					sWhere = sWhere + " AND (edoCta.idTmContratoFacultativo IS NOT NULL " +
			 		 		 				"AND edoCta.suscripcion = " +
			 		 		 								"(SELECT MAX(edoCtaAux.suscripcion) " +
			 		 		 								 "FROM MIDAS.ToEstadoCuenta edoCtaAux " +
			 		 		 								 "WHERE edoCtaAux.idTmContratoFacultativo = edoCta.idTmContratoFacultativo " +
			 		 		 								   "AND edoCtaAux.idTcReasegurador = edoCta.idTcReasegurador " +
			 		 		 								   "AND NVL(edoCtaAux.idTcCorredor,0) = NVL(edoCta.idTcCorredor,0) " +
			 		 		 								") " +
			 		 		 				") ";										
				}
			}else{
				sWhere += " AND ((edoCta.idTmContratoFacultativo IS NOT NULL " +
				 		 		 "AND edoCta.suscripcion = " +
				 		 		 		"(SELECT MAX(edoCtaAux.suscripcion) " +
				 		 		 		 "FROM MIDAS.ToEstadoCuenta edoCtaAux " +
				 		 		 		 "WHERE edoCtaAux.idTmContratoFacultativo = edoCta.idTmContratoFacultativo " +
				 		 		 		   "AND edoCtaAux.idTcReasegurador = edoCta.idTcReasegurador " +
				 		 		 		   "AND NVL(edoCtaAux.idTcCorredor,0) = NVL(edoCta.idTcCorredor,0) " +
				 		 		 		") " +
				 		 		 ") " +
				 		 		 "OR (edoCta.idTmContratoFacultativo IS NULL ";
				sWhere +=            "AND edoCta.ejercicio = " + filtro.getEjercicio();
				if(filtro.getSuscripcion() != null)
					sWhere +=       " AND edoCta.suscripcion = " + filtro.getSuscripcion().toString();
				 sWhere += 		 	 ") " +
				 		 	   ") ";
			}
			if (filtro.getIdtcreaseguradorcorredor() != null){
				if (filtro.getIdtcreaseguradorcorredor().intValue()  == TipoReaseguroDTO.ID_RETENCION){
					sWhere = sWhere + " AND edoCta.idTcReasegurador IS NULL";
					sWhere = sWhere + " AND edoCta.ejercicio = " + filtro.getEjercicio();
					if(filtro.getSuscripcion() != null)
						sWhere = sWhere + " AND edoCta.suscripcion = " + filtro.getSuscripcion().toString();
				}else
					sWhere = sWhere + " AND edoCta.idTcReasegurador = " + filtro.getIdtcreaseguradorcorredor().toString();
			}						 						
			
			if(filtro.getIdTcRamo() != null)				
				sWhere += " AND sRamo.idTcRamo = " + filtro.getIdTcRamo().toString();			
			
			if(filtro.getIdTcSubRamo() != null)
				sWhere += " AND edoCta.idTcSubramo = " + filtro.getIdTcSubRamo();
			
			if(filtro.getIdMoneda() != null)
				sWhere += " AND edoCta.idMoneda = " + filtro.getIdMoneda();
			
			if(filtro.getTipoReaseguro() != null)
				sWhere += " AND edoCta.idTcTipoReaseguro = " + filtro.getTipoReaseguro();
			
			if(filtro.getIdToPoliza() != null)
				sWhere += " AND edoCta.idToPoliza = " + filtro.getIdToPoliza();
			
			if(filtro.getIdTmContratoCuotaParte() != null)
				sWhere += " AND edoCta.idTmContratoCuotaParte = " + filtro.getIdTmContratoCuotaParte();
			
			if(filtro.getIdTmContratoFacultativo() != null)
				sWhere += " AND edoCta.idTmContratoFacultativo = " + filtro.getIdTmContratoFacultativo();
			
			if(filtro.getIdTmContratoPrimerExcedente() != null)
				sWhere += " AND edoCta.IdTmContratoPrimerExcedente = " + filtro.getIdTmContratoPrimerExcedente();
	
			
			//03/02/2011 Se agrega validacion para los endosos de disminucion, que deben mostrarse solo si activa tienen la bandera "bono por no siniestro"
			if(filtro.getIncluirValidacionBonoPorNoSiniestro() != null && filtro.getIncluirValidacionBonoPorNoSiniestro()){
				
				sWhere += " AND 1=(" +
						"  select (" +
						"    case " +
						"      /*Si no es facultativo, se ignora la validacion*/" +
						"      when edoCta.idtctiporeaseguro <> 3 then 1" +
						"      when 1=(" +
						"          select (" +
						"            case" +
						"              /*Valida que el endoso no sea de disminucion*/" +
						"              when (select endoso.clavetipoendoso claveTipoEndoso from MIDAS.toendoso endoso where endoso.idtopoliza = edoCta.idtopoliza and " +
						"                        endoso.NUMEROENDOSO = edoCta.numeroendoso) <> 3 /*Disminucion*/ then 1" +
						"              /*si el endoso es de disminucion, valida la bandera 'bonoPorNoSiniestro'*/" +
						"              when (select nvl(contFac.ESBONOPORNOSINIESTRO,0) from MIDAS.tmcontratofacultativo contFac where " +
						"                        contFac.idtmcontratofacultativo = nvl(edoCta.idtmcontratofacultativo,0)) = 1 then 1" +
						"              else 0" +
						"            end" +
						"          ) vCase2 from dual" +
						"        ) then 1" +
						"      else 0" +
						"    end" +
						"  )v from dual" +
						") ";
			}
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(sWhere);
			
			queryString = queryString + " ORDER BY edoCta.idTcTipoReaseguro, edoCta.ejercicio DESC, edoCta.suscripcion";
			
			query = entityManager.createNativeQuery(queryString, "estadoCuentaMapping");			
									
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			listaEstadoCuentaEncontrados = query.getResultList();
		}
		return listaEstadoCuentaEncontrados;
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaDetalle(BigDecimal idEstadoCuenta,List<ConceptoAcumuladorDTO> listaConceptosPorMoneda,List<AcumuladorDTO> acumuladorDTOList){
		EstadoCuentaDTO estadoCuentaDTO = this.findById(idEstadoCuenta);
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = this.obtenerEstadoCuentaConSaldosCalculados(estadoCuentaDTO,listaConceptosPorMoneda,acumuladorDTOList);
	
		return estadoCuentaDecoradoDTO;
	}

	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaConSaldosCalculados(EstadoCuentaDTO estadoCuentaDTO, List<ConceptoAcumuladorDTO> listaConceptosPorMoneda,List<AcumuladorDTO> listaAcumuladoresEdoCta) {
		SaldoConceptoDTO saldoConceptoAnterior =  obtenerSaldoAnterior(estadoCuentaDTO, estadoCuentaDTO.getSuscripcion());
		List<SaldoConceptoDTO> listaSaldoConceptos =  this.obtenerSaldosIndividuales(estadoCuentaDTO,listaConceptosPorMoneda,listaAcumuladoresEdoCta,true);
		
		SaldoConceptoDTO saldoTecnico = obtenerSaldosAcumulados(listaSaldoConceptos, saldoConceptoAnterior);
		
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO =  new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
		estadoCuentaDecoradoDTO.setSaldosAcumulados(listaSaldoConceptos);
		estadoCuentaDecoradoDTO.setSaldoTecnico(saldoTecnico);
		
		return estadoCuentaDecoradoDTO;
	}
	
	private SaldoConceptoDTO obtenerSaldoAnterior(EstadoCuentaDTO estadoCuentaDTO, int suscripcion)throws RuntimeException{
		SuscripcionDTO suscripcionDTO;
		CalculoSuscripcionesDTO calculoSuscripcionesDTO = new CalculoSuscripcionesDTO();
		SaldoConceptoDTO saldoAnterior = new SaldoConceptoDTO();
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO;
		List<Object[]> objectsResult = null;
		if (suscripcion < 1){
			throw new RuntimeException("N�mero de suscripci�n no v�lido");
		}
		else if (suscripcion == 1){
				SaldoConceptoDTO saldoConceptoDTO = new SaldoConceptoDTO();
				saldoConceptoDTO.setFechaTrimestre("");
				saldoConceptoDTO.setDebe(new BigDecimal(0));
				saldoConceptoDTO.setHaber(new BigDecimal(0));
				saldoConceptoDTO.setSaldo(new BigDecimal(0));
				saldoConceptoDTO.setSaldoAcumulado(new BigDecimal(0));
				saldoConceptoDTO.setDescripcionConcepto("Saldo Anterior("+estadoCuentaDTO.getEjercicio()+")");
				return saldoConceptoDTO;
		}
		else {
			objectsResult = obtenerEstadoCuentaAcumuladores(estadoCuentaDTO,estadoCuentaDTO.getSuscripcion()-1);
		}
		if (objectsResult != null && objectsResult.size() > 0){ // Si devuelve un EstadoCuentaDTO
			List<AcumuladorDTO> acumuladorDTOList = new ArrayList<AcumuladorDTO>();
			EstadoCuentaDTO estadoCuentaDTO2;
			estadoCuentaDTO2 = (EstadoCuentaDTO) objectsResult.get(0)[0];
			int bandera = 0;
			for (Object[] object : objectsResult) {
				if(estadoCuentaDTO2.getIdEstadoCuenta().compareTo(((EstadoCuentaDTO)object[0]).getIdEstadoCuenta()) != 0){//Indica que se encontr� m�s de un Estado de Cuenta, lo cual es inconsistente
					/*
					 * 29/07/2010. Jose Luis Arellano. es inconsistente siempre y cuando pertenezcan al mismo reasegurador.
					 */
					if(estadoCuentaDTO2.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().compareTo(((EstadoCuentaDTO)object[0]).getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor()) == 0){
						bandera = 1;
						break;
					}
				}
				if (object[1] != null)
					acumuladorDTOList.add((AcumuladorDTO) object[1]);
			}
			if (bandera > 0){
				throw new RuntimeException("M�s de un Estado de Cuenta encontrado, lo cual es inconsistente");
			}else{
				estadoCuentaDecoradoDTO = obtenerEstadoCuentaConSaldosCalculados(estadoCuentaDTO2,null,null);
				saldoAnterior = estadoCuentaDecoradoDTO.getSaldoTecnico();
				saldoAnterior.setDescripcionConcepto("Saldo Anterior");
			}
		}else{ // Si no existi� un EstadoCuentaDTO
			saldoAnterior = obtenerSaldoAnterior(estadoCuentaDTO, suscripcion-1);  // Llamada Recursiva
		}
		
		if(saldoAnterior.getSaldoAcumulado() != null && saldoAnterior.getSaldoAcumulado().compareTo(BigDecimal.ZERO) > 0){
			saldoAnterior.setHaber(saldoAnterior.getSaldoAcumulado());
		}else if (saldoAnterior.getSaldoAcumulado() != null && saldoAnterior.getSaldoAcumulado().compareTo(BigDecimal.ZERO) < 0){
			saldoAnterior.setDebe(saldoAnterior.getSaldoAcumulado());
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		suscripcionDTO = calculoSuscripcionesDTO.getSuscripcion(new EstadoCuentaDecoradoDTO(estadoCuentaDTO), (suscripcion != 1 ? suscripcion - 1 : suscripcion)); //Suscripci�n - 1 es debido a que se hace referencia a la suscripci�n anterior
		String fechaTrimestre = simpleDateFormat.format(suscripcionDTO.getFechaInicial()) 
		+ " - " + simpleDateFormat.format(suscripcionDTO.getFechaFinal());
		saldoAnterior.setFechaTrimestre(fechaTrimestre);
		
		return saldoAnterior;
	}

	
	public List<SaldoConceptoDTO> obtenerSaldosIndividuales(EstadoCuentaDTO estadoCuentaDTO,List<ConceptoAcumuladorDTO> listaConceptosPorMoneda,List<AcumuladorDTO> listaAcumuladoresEdoCta,boolean consultaAcumuladores){
 		List<ConceptoAcumuladorDTO> listaConceptosAcumulador = obtenerConceptosMovimientos(new BigDecimal(estadoCuentaDTO.getIdMoneda()),listaConceptosPorMoneda);
		List<SaldoConceptoDTO> listaSaldosConceptos = new ArrayList<SaldoConceptoDTO>();
		for (ConceptoAcumuladorDTO conceptoAcumuladorDTO : listaConceptosAcumulador) {
			SaldoConceptoDTO saldoConceptoDTO = obtenerSaldoCalculado(estadoCuentaDTO, conceptoAcumuladorDTO.getConceptoMovimientoDTO(),listaAcumuladoresEdoCta,consultaAcumuladores);
			if(saldoConceptoDTO != null){
				listaSaldosConceptos.add(saldoConceptoDTO);
			}else{
				saldoConceptoDTO = new SaldoConceptoDTO();
				if(conceptoAcumuladorDTO.getConceptoMovimientoDTO().getNaturaleza() ==  TipoMovimientoDTO.NATURALEZA_ABONO){
					saldoConceptoDTO.setHaber(new BigDecimal(0));
				}
				if(conceptoAcumuladorDTO.getConceptoMovimientoDTO().getNaturaleza() ==  TipoMovimientoDTO.NATURALEZA_CARGO){
					saldoConceptoDTO.setDebe(new BigDecimal(0));
				}
				saldoConceptoDTO.setSaldo(new BigDecimal(0));
				saldoConceptoDTO.setSaldoAcumulado(new BigDecimal(0));
				saldoConceptoDTO.setDescripcionConcepto(conceptoAcumuladorDTO.getConceptoMovimientoDTO().getDescripcion());
				listaSaldosConceptos.add(saldoConceptoDTO);
			}
		}
		return listaSaldosConceptos;
	}
	
	@SuppressWarnings("unchecked")
	public List<ConceptoAcumuladorDTO> obtenerConceptosMovimientos(BigDecimal idMoneda, List<ConceptoAcumuladorDTO> listaConceptosPorMoneda){
		List<ConceptoAcumuladorDTO> conceptoAcumuladorDTOList = null;
		
		if(listaConceptosPorMoneda == null || listaConceptosPorMoneda.isEmpty()){
			String queryString = "SELECT model FROM ConceptoAcumuladorDTO model WHERE model.id.idMoneda = :idMoneda " + 
			"AND model.conceptoMovimientoDTO.naturaleza <> 0 ORDER BY model.orden";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idMoneda", idMoneda);
			conceptoAcumuladorDTOList = query.getResultList();
			for(ConceptoAcumuladorDTO conceptoAcumuladorDTO : conceptoAcumuladorDTOList){
				ConceptoMovimientoDTO conceptoMovTMP = conceptoAcumuladorDTO.getConceptoMovimientoDTO();
				conceptoAcumuladorDTO.setConceptoMovimientoDTO(conceptoMovTMP);
			}
		}else{
			conceptoAcumuladorDTOList = listaConceptosPorMoneda;
		}
		
		return conceptoAcumuladorDTOList;
	}
	
	private SaldoConceptoDTO obtenerSaldoCalculado(EstadoCuentaDTO estadoCuentaDTO, ConceptoMovimientoDTO conceptoMovimientoDTO,List<AcumuladorDTO> acumuladorDTOList,boolean consultaAcumuladores){
		SaldoConceptoDTO saldoConceptoDTO = null;
		ConceptoMovimientoDTO conceptoMovimientoAcumulador = null;
		double sumaAbono = 0;
		double sumaCargo = 0;
		
		if (estadoCuentaDTO != null && (acumuladorDTOList == null || acumuladorDTOList.isEmpty() && consultaAcumuladores)){
			acumuladorDTOList = consultarAcumuladoresEstadoCuenta(estadoCuentaDTO.getIdEstadoCuenta());
			estadoCuentaDTO.setAcumuladorDTOs(acumuladorDTOList);
		}
		if(acumuladorDTOList != null && acumuladorDTOList.size() > 0){
			for (AcumuladorDTO acumuladorDTO : acumuladorDTOList) {
				conceptoMovimientoAcumulador = acumuladorDTO.getConceptoMovimientoDTO();
				
				if(conceptoMovimientoAcumulador.getIdConceptoMovimiento() == conceptoMovimientoDTO.getIdConceptoMovimiento()) {
					sumaAbono = sumaAbono + acumuladorDTO.getAbono().doubleValue();
					sumaCargo = sumaCargo + acumuladorDTO.getCargo().doubleValue();
	            }
			}
			saldoConceptoDTO = new SaldoConceptoDTO();
		    saldoConceptoDTO.setDescripcionConcepto(conceptoMovimientoDTO.getDescripcion());
		    double saldoCalculado = conceptoMovimientoDTO.getNaturaleza() == ConceptoMovimientoDTO.TIPO_NATURALEZA_HABER ? (sumaAbono - sumaCargo) : (sumaCargo-sumaAbono);
		    BigDecimal saldoCalculadoPorAcumular = new BigDecimal(saldoCalculado);
		    if(conceptoMovimientoDTO.getNaturaleza() != ConceptoMovimientoDTO.TIPO_NATURALEZA_HABER)
		    	saldoCalculadoPorAcumular = saldoCalculadoPorAcumular.multiply(new BigDecimal(-1d));
		    saldoConceptoDTO.setSaldo(saldoCalculadoPorAcumular);
		    
		    if (conceptoMovimientoDTO.getNaturaleza() == ConceptoMovimientoDTO.TIPO_NATURALEZA_HABER){
		    	if (saldoCalculado < 0)
		    		saldoConceptoDTO.setDebe(new BigDecimal(Math.abs(saldoCalculado*-1)));
		    	else
		    		saldoConceptoDTO.setHaber(new BigDecimal(saldoCalculado));
		    }else if (conceptoMovimientoDTO.getNaturaleza() == ConceptoMovimientoDTO.TIPO_NATURALEZA_DEBE){
		    	if (saldoCalculado < 0)
		    		saldoConceptoDTO.setHaber(new BigDecimal(Math.abs(saldoCalculado*-1)));
		    	else
		    		saldoConceptoDTO.setDebe(new BigDecimal(saldoCalculado));
		    }else if (conceptoMovimientoDTO.getNaturaleza() == ConceptoMovimientoDTO.TIPO_NATURALEZA_DEBE_HABER){
		    	saldoConceptoDTO.setHaber(new BigDecimal(sumaAbono));
		    	saldoConceptoDTO.setDebe(new BigDecimal(sumaCargo));
		    }
		}
		return saldoConceptoDTO;
	}
	
	@SuppressWarnings("unchecked")
	private List<AcumuladorDTO> consultarAcumuladoresEstadoCuenta(BigDecimal idToEstadoCuenta){
		List<AcumuladorDTO> acumuladorDTOList = null;
		String queryString = "SELECT model.acumuladorDTOs FROM EstadoCuentaDTO model WHERE model.idEstadoCuenta = :idEstadoCuenta";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idEstadoCuenta", idToEstadoCuenta);
		acumuladorDTOList = query.getResultList();
		return acumuladorDTOList;
	}
	
	public SaldoConceptoDTO obtenerSaldosAcumulados(List<SaldoConceptoDTO> listaSaldosConceptos, SaldoConceptoDTO saldoAnterior){
		return UtileriasCalculoEstadoCuenta.obtenerSaldosAcumulados(listaSaldosConceptos, saldoAnterior);
	}

	@SuppressWarnings("unchecked")
	public List<PolizaSoporteDanosDTO> filtrarPolizasFacultativas(List<PolizaSoporteDanosDTO> polizas) {
		List<PolizaSoporteDanosDTO> listaPolizasFiltrada = new ArrayList<PolizaSoporteDanosDTO>();
		String queryString = "SELECT model FROM EstadoCuentaDTO model WHERE model.idPoliza= :idToPoliza and model.tipoReaseguro = :tipoReaseguro";
		Query query;
		for (PolizaSoporteDanosDTO polizaSoporteDanosDTO : polizas){
			query = entityManager.createQuery(queryString);
			query.setParameter("idToPoliza", polizaSoporteDanosDTO.getIdToPoliza());
			query.setParameter("tipoReaseguro", TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);
			List<SoporteReaseguroDTO> result = query.getResultList();
			if (result.size() > 0)
				listaPolizasFiltrada.add(polizaSoporteDanosDTO);
		}
		
		return listaPolizasFiltrada;
	}
	
	@SuppressWarnings("unchecked")
	public List<BigDecimal> filtrarPolizasFacultativasPorReaseguradorMoneda(BigDecimal idTcReasegurador,BigDecimal idMoneda) {
		List<BigDecimal> listaPolizasEncontradas = new ArrayList<BigDecimal>();
		String queryString = "SELECT DISTINCT model.idPoliza FROM EstadoCuentaDTO model WHERE model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idTcReasegurador " +
				" and model.idMoneda = :idMoneda and model.tipoReaseguro = :tipoReaseguro and model.idPoliza is not null";
		Query query;
		query = entityManager.createQuery(queryString);
		query.setParameter("idTcReasegurador", idTcReasegurador);
		query.setParameter("idMoneda", idMoneda);
		query.setParameter("tipoReaseguro", TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);
		
		List<Object> listaResultado = query.getResultList();
		for(Object result : listaResultado){
			listaPolizasEncontradas.add(Utilerias.obtenerBigDecimal(result));
		}
		
		return listaPolizasEncontradas;
	}
	
	@SuppressWarnings("unchecked")
	public List<BigDecimal> filtrarSiniestrosFacultativosPorReaseguradorMoneda(BigDecimal idTcReasegurador,BigDecimal idMoneda) {
		List<BigDecimal> listaSiniestrosEncontrados = new ArrayList<BigDecimal>();
		String queryString = "SELECT DISTINCT model.idToReporteSiniestro FROM MovimientoReaseguroDTO model WHERE model.reaseguradorCorredor.idtcreaseguradorcorredor = :idTcReasegurador " +
				" and model.idMoneda = :idMoneda and model.tipoReaseguroDTO.tipoReaseguro = :tipoReaseguro and model.idToReporteSiniestro is not null";
		Query query;
		query = entityManager.createQuery(queryString);
		query.setParameter("idTcReasegurador", idTcReasegurador);
		query.setParameter("idMoneda", idMoneda.intValue());
		query.setParameter("tipoReaseguro", TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);
		
		List<Object> listaResultado = query.getResultList();
		for(Object result : listaResultado){
			listaSiniestrosEncontrados.add(Utilerias.obtenerBigDecimal(result));
		}
		
		return listaSiniestrosEncontrados;
	}
	
	@SuppressWarnings("unchecked")
	public List<EstadoCuentaDTO> buscarEstadosCuentaFacultativos(PolizaSoporteDanosDTO poliza) {
		List<EstadoCuentaDTO> estadoCuentaDTOList = new ArrayList<EstadoCuentaDTO>();
		if (poliza != null) {
			String queryString = "SELECT model FROM EstadoCuentaDTO model WHERE model.idPoliza = :idPoliza "
					+ "AND model.contratoFacultativoDTO IS NOT NULL AND model.tipoReaseguro = :tipoReaseguro "
					+ "ORDER BY model.ejercicio DESC, model.suscripcion";

			Query query = entityManager.createQuery(queryString);
			query.setParameter("idPoliza", poliza.getIdToPoliza());
			query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);
			estadoCuentaDTOList = query.getResultList();
 
		}
		return estadoCuentaDTOList;
	}
	
	@SuppressWarnings("unchecked")
	private List<ParticipacionCorredorDTO> obtenerListaParticipacionCorredorDTOByParticipacionDTO(ParticipacionDTO participacionDTO){
		List<ParticipacionCorredorDTO> participacionCorredorDTOList = new ArrayList<ParticipacionCorredorDTO>();
		String queryString = "SELECT model FROM ParticipacionCorredorDTO model WHERE model.participacion.idTdParticipacion = :idTdParticipacion";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTdParticipacion", participacionDTO.getIdTdParticipacion());
		participacionCorredorDTOList = query.getResultList();
		
		return participacionCorredorDTOList;
	}
	
	@SuppressWarnings("unchecked")
	private List<ParticipacionCorredorFacultativoDTO> obtenerListaParticipacionCorredorFacultativoDTOByParticipacionCorredorDTO(ParticipacionFacultativoDTO participacionFacultativoDTO){
		List<ParticipacionCorredorFacultativoDTO> participacionCorredorFacultativoDTOList = new ArrayList<ParticipacionCorredorFacultativoDTO>();
		String queryString = "SELECT model FROM ParticipacionCorredorFacultativoDTO model WHERE model.participacionFacultativoDTO.participacionFacultativoDTO = :participacionFacultativoDTO";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("participacionFacultativoDTO", participacionFacultativoDTO.getParticipacionFacultativoDTO());
		participacionCorredorFacultativoDTOList = query.getResultList();
		return participacionCorredorFacultativoDTOList;
	}
	
	@SuppressWarnings("unchecked")
	private List<DetalleContratoFacultativoDTO> obtenerListaDetalleContratoFacultativoDTOByContratoFacultativoDTO(ContratoFacultativoDTO contratoFacultativoDTO){
		List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOList = new ArrayList<DetalleContratoFacultativoDTO>();
		String queryString = "SELECT model FROM DetalleContratoFacultativoDTO model WHERE model.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTmContratoFacultativo", contratoFacultativoDTO.getIdTmContratoFacultativo());
		detalleContratoFacultativoDTOList = query.getResultList();
		
		return detalleContratoFacultativoDTOList;
	}
	
	@SuppressWarnings("unchecked")
	private List<ParticipacionFacultativoDTO> obtenerListaParticipacionFacultativoDTOByDetalleContratoFacultativoDTO(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO){
		List<ParticipacionFacultativoDTO> participacionFacultativoDTOList = new ArrayList<ParticipacionFacultativoDTO>();
		String queryString = "SELECT model FROM ParticipacionFacultativoDTO model WHERE model.detalleContratoFacultativoDTO.contratoFacultativoDTO = :contratoFacultativoDTO";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("contratoFacultativoDTO", detalleContratoFacultativoDTO.getContratoFacultativoDTO());
		participacionFacultativoDTOList = query.getResultList();
		
		return participacionFacultativoDTOList;
	}
	
	/*
	private void registrarSuscripcion(BigDecimal id, Date fechaInicial, Date fechaFinal, BigDecimal formaPago, int suscripciones, String tipo){
		
		ParametroGeneralId parametroGeneralId = new ParametroGeneralId();
		
		parametroGeneralId.setIdToGrupoParametroGeneral(new BigDecimal(9));
		parametroGeneralId.setCodigoParametroGeneral(id);
		
		ParametroGeneralDTO parametro = entityManager.find(ParametroGeneralDTO.class, parametroGeneralId);
		
		if(parametro == null){
			parametro = new ParametroGeneralDTO();
			parametro.setId(parametroGeneralId);
		}
		
		StringBuffer buffer = new StringBuffer();
		buffer.append(tipo);		
		buffer.append(" ID=");
		buffer.append(id);
		buffer.append(" fechaInicial=");
		buffer.append(fechaInicial);
		buffer.append(" fechaFinal=");	
		buffer.append(fechaFinal);
		buffer.append(" formaPago=");
		buffer.append(formaPago);
		buffer.append(" suscripciones=");	
		buffer.append(suscripciones);		

		parametro.setClaveTipoValor(claveTipoValor);
		parametro.setDescripcionParametroGeneral(descripcionParametroGeneral);
		parametro.setValor(buffer.toString());
		
	}
	*/
	
	/**
	 * Crea los estados de cuenta correspondiente a una l�nea autorizada.
	 * @param linea
	 */
	@SuppressWarnings("unchecked")
	public void crearEstadosCuentaLinea(LineaDTO linea){
		/*
		 * Contrato Cuota Parte
		 */
		if(linea.getContratoCuotaParte() != null){
			
			String queryString = "SELECT model.contratoCuotaParte.participaciones from LineaDTO model " +
					"WHERE model.idTmLinea = :idTmLinea";
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTmLinea", linea.getIdTmLinea());
			List<ParticipacionDTO> participaciones = query.getResultList();
			if(linea.getContratoCuotaParte() == null){
				System.out.println("\n\nCP es nulo");
			}
			List<SuscripcionDTO>  suscripciones = CalculoSuscripcionesDTO.obtenerSuscripciones(
									linea.getContratoCuotaParte().getFechaInicial(),
									linea.getContratoCuotaParte().getFechaFinal(), 
									linea.getContratoCuotaParte().getFormaPago());
			
			for(SuscripcionDTO suscripcion : suscripciones){

				/*
				 * Retenci�n en CuotaParte
				 */
				agregarEstadoCuenta(linea, 
						   linea.getContratoCuotaParte().getIdTcMoneda(),
						   suscripcion.getEjercicio(),
						   suscripcion.getIdSuscripcion(),
						   null, null,
						   TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE
				);					
				
				for(ParticipacionDTO participacion : participaciones){
					// Corredor
					if(participacion.getTipo().compareTo(BigDecimal.ZERO) == 0){  
						for(ParticipacionCorredorDTO reaseg : this.obtenerListaParticipacionCorredorDTOByParticipacionDTO(participacion)){
							agregarEstadoCuenta(linea, 
									   linea.getContratoCuotaParte().getIdTcMoneda(),
									   suscripcion.getEjercicio(),
									   suscripcion.getIdSuscripcion(),
									   reaseg.getReaseguradorCorredor(),
									   participacion.getReaseguradorCorredor(),
									   TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE
							);							
						}
					} else {  
						// Reasegurador
						agregarEstadoCuenta(linea, 
										   linea.getContratoCuotaParte().getIdTcMoneda(),
										   suscripcion.getEjercicio(),
										   suscripcion.getIdSuscripcion(),
										   participacion.getReaseguradorCorredor(),
										   null,
										   TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE
					     );
						
					}
				}
				
			}
			
		} //

		/*
		 * Contrato Primer Excedente
		 */
		if(linea.getContratoPrimerExcedente() != null){
			
			String queryString = "SELECT model.contratoPrimerExcedente.participacionDTOList from LineaDTO model " +
			"WHERE model.idTmLinea = :idTmLinea";
	
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTmLinea", linea.getIdTmLinea());
			List<ParticipacionDTO> participaciones = query.getResultList();
			List<SuscripcionDTO>  suscripciones = CalculoSuscripcionesDTO.obtenerSuscripciones(
					linea.getContratoPrimerExcedente().getFechaInicial(),
					linea.getContratoPrimerExcedente().getFechaFinal(), 
					linea.getContratoPrimerExcedente().getFormaPago());				
			
			for(SuscripcionDTO suscripcion : suscripciones){
				
				for(ParticipacionDTO participacion : participaciones){
					// Corredor
					if(participacion.getTipo().compareTo(BigDecimal.ZERO) == 0){  
						for(ParticipacionCorredorDTO reaseg : this.obtenerListaParticipacionCorredorDTOByParticipacionDTO(participacion)){
							agregarEstadoCuenta(linea, 
									linea.getContratoPrimerExcedente().getIdMoneda(),
									suscripcion.getEjercicio(),
									suscripcion.getIdSuscripcion(),
									reaseg.getReaseguradorCorredor(),
									participacion.getReaseguradorCorredor(),
									TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE
							);							
						}
					} else {  
						// Reasegurador
						agregarEstadoCuenta(linea, 
								linea.getContratoPrimerExcedente().getIdMoneda(),
								suscripcion.getEjercicio(),
								suscripcion.getIdSuscripcion(),
								participacion.getReaseguradorCorredor(),
								null,
								TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE
						);
						
					}
				}					
				
			}

		} //
		
		List<SuscripcionDTO>  suscripciones = CalculoSuscripcionesDTO.obtenerSuscripciones(
				linea.getFechaInicial(),
				linea.getFechaFinal(), 
				new BigDecimal(LineaDTO.FORMA_PAGO_RETENCION_PURA));
		
		for(SuscripcionDTO suscripcion : suscripciones){
			/*
			 * Retenci�n Pura
			 */
			agregarEstadoCuenta(linea, 
					   null,
					   suscripcion.getEjercicio(),
					   suscripcion.getIdSuscripcion(),
					   null,
					   null,
					   TipoReaseguroDTO.TIPO_RETENCION
					   );					
		}		
	}
	
	/**
	 * Crea los estados de cuenta correspondientes a las l�neas autorizadas
	 */
	@SuppressWarnings("unchecked")
	public void crearEstadosCuentaContratosAutomaticos(){
		
		String queryString = "SELECT model, model.contratoCuotaParteDTO, model.contratoPrimerExcedenteDTO FROM LineaDTO model WHERE model.estatus = 1";
		Query query = entityManager.createQuery(queryString);
		List<Object[]> results = query.getResultList();
		List<LineaDTO> listaLineas = new ArrayList<LineaDTO>();
		List<ContratoCuotaParteDTO> contratoCuotaParteDTOList = new ArrayList<ContratoCuotaParteDTO>();
		List<ContratoPrimerExcedenteDTO> contratoPrimerExcedenteList = new ArrayList<ContratoPrimerExcedenteDTO>();
		if (results != null){
			for (Object[] resultsObject : results) {
				if(resultsObject[0] != null)
					listaLineas.add((LineaDTO) resultsObject[0]);
				if(resultsObject[1] != null)
					contratoCuotaParteDTOList.add((ContratoCuotaParteDTO) resultsObject[1]);
				if(resultsObject[2] != null)
					contratoPrimerExcedenteList.add((ContratoPrimerExcedenteDTO) resultsObject[2]);
			}
		}
		for(LineaDTO linea : listaLineas){
			crearEstadosCuentaLinea(linea);
		}  // for lineas
		
		
	}

	/**
	 * Crea los estados de cuenta correspondiente a un contrato facultativo autorizado.
	 * @param contrato
	 */
	public void crearEstadosCuentaFacultativo(ContratoFacultativoDTO contrato){
		LineaSoporteReaseguroDTO soporte = obtenerLineaSoporteReaseguro(contrato);
		if (soporte != null){
			List<SuscripcionDTO>  suscripciones = CalculoSuscripcionesDTO.obtenerSuscripciones(
					contrato.getFechaInicial(),
					contrato.getFechaFinal(), 
					contrato.getIdFormaDePago());

			for(SuscripcionDTO suscripcion : suscripciones){				

				List<DetalleContratoFacultativoDTO> listaDetalle = this.obtenerListaDetalleContratoFacultativoDTOByContratoFacultativoDTO(contrato);
				for(DetalleContratoFacultativoDTO detalle : listaDetalle){
					List<ParticipacionFacultativoDTO>  listaParticipacion = this.obtenerListaParticipacionFacultativoDTOByDetalleContratoFacultativoDTO(detalle);

					for(ParticipacionFacultativoDTO  participacion : listaParticipacion){
						// Corredor
						if(participacion.getTipo().equalsIgnoreCase("0")){  
							for(ParticipacionCorredorFacultativoDTO reaseg : this.obtenerListaParticipacionCorredorFacultativoDTOByParticipacionCorredorDTO(participacion)){
								agregarEstadoCuenta(soporte,
										null,  // pendiente definir idMoneda en Contrato Facultativo
										suscripcion.getEjercicio(),
										suscripcion.getIdSuscripcion(),
										reaseg.getReaseguradorCorredorDTO(),
										participacion.getReaseguradorCorredorDTO(),
										TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO,
										contrato
								);							
							}
						} else {  
							// Reasegurador
							agregarEstadoCuenta(soporte, 
									null,  // pendiente definir idMoneda en Contrato Facultativo
									suscripcion.getEjercicio(),
									suscripcion.getIdSuscripcion(),
									participacion.getReaseguradorCorredorDTO(),
									null,
									TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO,
									contrato
							);

						}


					}   // for(ParticipacionFacultativoDTO  participacion : listaParticipacion){
				}  // for(DetalleContratoFacultativoDTO detalle : listaDetalle){


			}  // for(SuscripcionDTO suscripcion : suscripciones){		
		}
	}
	
	
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public void crearEstadosCuentaContratosFacultativos(){	
		List<ContratoFacultativoDTO> listaFacultativos = new ArrayList<ContratoFacultativoDTO>();
		ContratoFacultativoDTO contratoFacultativoDTO; 
		List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOList;
		String queryString = "SELECT model, model.detalleContratoFacultativoDTOs FROM ContratoFacultativoDTO model WHERE model.estatus = 3";
		Query query = entityManager.createQuery(queryString);
		List<Object[]> results = query.getResultList();
		if (results != null){
			for (Object[] resultsObject : results) {
				if(resultsObject[0] != null){
					detalleContratoFacultativoDTOList = new ArrayList<DetalleContratoFacultativoDTO>();
					contratoFacultativoDTO = new ContratoFacultativoDTO();
					detalleContratoFacultativoDTOList = (List<DetalleContratoFacultativoDTO>) resultsObject[1];
					contratoFacultativoDTO = (ContratoFacultativoDTO) resultsObject[0];
					contratoFacultativoDTO.setDetalleContratoFacultativoDTOs(detalleContratoFacultativoDTOList);
					listaFacultativos.add(contratoFacultativoDTO);
				}
			}
		}
		
		for(ContratoFacultativoDTO contrato : listaFacultativos){
			crearEstadosCuentaFacultativo(contrato);
		}
		
	}
	
	
	/**
	 * 
	 * @param lineaDTO
	 * @param idMoneda
	 * @param ejercicio
	 * @param suscripcion
	 * @param reasegurador
	 * @param tipoReaseguro
	 */
	@SuppressWarnings("unchecked")
	private void agregarEstadoCuenta(LineaDTO lineaDTO, BigDecimal idMoneda, int ejercicio,	int suscripcion, ReaseguradorCorredorDTO reasegurador, ReaseguradorCorredorDTO corredor,int tipoReaseguro){
		SubRamoDTO subRamoDTO = lineaDTO.getSubRamo();
		String queryString;
		String queryStringAux = "";
		EstadoCuentaDTO estadoCuentaDTO;
		
		if (idMoneda == null){
			idMoneda = new BigDecimal(484);
		}		
		
		if (tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			queryStringAux = "AND model.contratoCuotaParteDTO.idTmContratoCuotaParte = :idTmContratoCuotaParte ";
			 if (reasegurador != null){
				 queryStringAux+= "AND model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor ";
			 }else{
				 queryStringAux+= "AND model.lineaDTO.idTmLinea = :idTmLinea ";
			 }
		}
		if (tipoReaseguro  == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
			 queryStringAux = "AND model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor " +
			 				  "AND model.contratoPrimerExcedenteDTO.idTmContratoPrimerExcedente = :idTmContratoPrimerExcedente ";
		}
		if (tipoReaseguro == TipoReaseguroDTO.TIPO_RETENCION){
			queryStringAux = "AND model.lineaDTO.idTmLinea = :idTmLinea ";
		}
		queryString = "SELECT model FROM EstadoCuentaDTO model WHERE " +
					  "model.subRamoDTO.idTcSubRamo = :idTcSubRamo " +
					  queryStringAux +
					  " AND model.tipoReaseguro = :tipoReaseguro " +
					  " AND model.idMoneda = :idMoneda " +
					  " AND model.ejercicio = :ejercicio " +
					  " AND model.suscripcion = :suscripcion ";
				 
		Query query = entityManager.createQuery(queryString);
			query.setParameter("idTcSubRamo", subRamoDTO.getIdTcSubRamo());
		
			if (tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
				query.setParameter("idTmContratoCuotaParte", lineaDTO.getContratoCuotaParte().getIdTmContratoCuotaParte());
				if (reasegurador != null){
					query.setParameter("idtcreaseguradorcorredor", reasegurador.getIdtcreaseguradorcorredor());
				}else{
					query.setParameter("idTmLinea", lineaDTO.getIdTmLinea());
				}
			}else{
				if (tipoReaseguro  == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
					query.setParameter("idtcreaseguradorcorredor", reasegurador.getIdtcreaseguradorcorredor());
					query.setParameter("idTmContratoPrimerExcedente", lineaDTO.getContratoPrimerExcedente().getIdTmContratoPrimerExcedente());
				  }else{
					  	if (tipoReaseguro == TipoReaseguroDTO.TIPO_RETENCION){
					  		query.setParameter("idTmLinea", lineaDTO.getIdTmLinea());
					  	}
				  }
			}
			query.setParameter("tipoReaseguro", tipoReaseguro);
			query.setParameter("idMoneda", idMoneda);
			query.setParameter("ejercicio", ejercicio);
			query.setParameter("suscripcion", suscripcion);
			
			List<EstadoCuentaDTO> estadoCuentaDTOList = query.getResultList();
		
		if (estadoCuentaDTOList.size() == 0){
			estadoCuentaDTO = new EstadoCuentaDTO();
			estadoCuentaDTO.setSubRamoDTO(subRamoDTO);
			
			if (tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
				estadoCuentaDTO.setContratoCuotaParteDTO(lineaDTO.getContratoCuotaParte());
				if (reasegurador != null){
					estadoCuentaDTO.setReaseguradorCorredorDTO(reasegurador);
					estadoCuentaDTO.setCorredorDTO(corredor);
					estadoCuentaDTO.setLineaDTO(lineaDTO); 
				}else{
					estadoCuentaDTO.setLineaDTO(lineaDTO);
				}
			}else{
				if (tipoReaseguro  == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
					estadoCuentaDTO.setReaseguradorCorredorDTO(reasegurador);
					estadoCuentaDTO.setLineaDTO(lineaDTO);
					estadoCuentaDTO.setContratoPrimerExcedenteDTO(lineaDTO.getContratoPrimerExcedente());
				  }else{
					  	if (tipoReaseguro == TipoReaseguroDTO.TIPO_RETENCION){
					  		estadoCuentaDTO.setLineaDTO(lineaDTO);
					  	}
				  }
			}
			
			estadoCuentaDTO.setTipoReaseguro(tipoReaseguro);
			if (idMoneda == null)
				estadoCuentaDTO.setIdMoneda(484);
			else
				estadoCuentaDTO.setIdMoneda(idMoneda.intValue());
			estadoCuentaDTO.setEjercicio(ejercicio);
			estadoCuentaDTO.setSuscripcion(suscripcion);
			estadoCuentaDTO.setNumeroEndoso(0);
			this.save(estadoCuentaDTO);
		}		
		
	}	


	@SuppressWarnings("unchecked")
	private void agregarEstadoCuenta(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,
										BigDecimal idMoneda,
										int ejercicio, 
										int suscripcion,
										ReaseguradorCorredorDTO reasegurador,
										ReaseguradorCorredorDTO corredor,
										int tipoReaseguro,
										ContratoFacultativoDTO contrato										
										){
		EstadoCuentaDTO estadoCuentaDTO;
		String queryStringAux = "";
		if (reasegurador != null){
			 queryStringAux+= "AND model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor ";
		 }else{
			 queryStringAux+= "AND model.lineaDTO.idTmLinea = :idTmLinea";
		 }
		
		if (idMoneda == null){
			idMoneda = new BigDecimal(484);
		}
		
		String queryString = "SELECT model FROM EstadoCuentaDTO model WHERE " +
		  "model.subRamoDTO.idTcSubRamo = :idTcSubRamo " +
		  queryStringAux +
		  " AND model.tipoReaseguro = :tipoReaseguro " +
		  " AND model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo" +
		  " AND model.idMoneda = :idMoneda " +
		  " AND model.idPoliza = :idPoliza " +
		  " AND model.numeroEndoso = :numeroEndoso" +
		  " AND model.ejercicio = :ejercicio " +
		  " AND model.suscripcion = :suscripcion";
	 
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTcSubRamo", lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo().getIdTcSubRamo());
		
		if (reasegurador != null){
			query.setParameter("idtcreaseguradorcorredor", reasegurador.getIdtcreaseguradorcorredor());
		}else{
			query.setParameter("idTmLinea", lineaSoporteReaseguroDTO.getLineaDTO().getIdTmLinea());
		}
		query.setParameter("tipoReaseguro", TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);
		query.setParameter("idTmContratoFacultativo", contrato.getIdTmContratoFacultativo());
		query.setParameter("idMoneda", idMoneda);
		query.setParameter("idPoliza", lineaSoporteReaseguroDTO.getSoporteReaseguroDTO().getIdToPoliza());
		query.setParameter("numeroEndoso", lineaSoporteReaseguroDTO.getSoporteReaseguroDTO().getNumeroEndoso());
		query.setParameter("ejercicio", ejercicio);
		query.setParameter("suscripcion", suscripcion);
		
		List<EstadoCuentaDTO> estadoCuentaDTOList = query.getResultList();
		
		if (estadoCuentaDTOList.size() == 0){
			estadoCuentaDTO = new EstadoCuentaDTO();
			
			estadoCuentaDTO.setSubRamoDTO(lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo());
			estadoCuentaDTO.setReaseguradorCorredorDTO(reasegurador);
			estadoCuentaDTO.setCorredorDTO(corredor);
			estadoCuentaDTO.setLineaDTO(lineaSoporteReaseguroDTO.getLineaDTO());
			estadoCuentaDTO.setTipoReaseguro(tipoReaseguro);
			estadoCuentaDTO.setContratoFacultativoDTO(contrato);
			if (idMoneda == null)
				estadoCuentaDTO.setIdMoneda(484);
			else
				estadoCuentaDTO.setIdMoneda(idMoneda.intValue());
			estadoCuentaDTO.setEjercicio(ejercicio);
			estadoCuentaDTO.setSuscripcion(suscripcion);
			estadoCuentaDTO.setNumeroEndoso(lineaSoporteReaseguroDTO.getSoporteReaseguroDTO().getNumeroEndoso());
			estadoCuentaDTO.setIdPoliza(lineaSoporteReaseguroDTO.getSoporteReaseguroDTO().getIdToPoliza());
			
			this.save(estadoCuentaDTO);
		}	

	}		
	
	/**
	 * 
	 * @param contrato
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private LineaSoporteReaseguroDTO obtenerLineaSoporteReaseguro(ContratoFacultativoDTO contrato){
		
		String queryString = "SELECT model FROM LineaSoporteReaseguroDTO model WHERE " +
				"model.contratoFacultativoDTO = :contratoFacultativoDTO";
		
		Query query = entityManager.createQuery(queryString);
		query.setParameter("contratoFacultativoDTO", contrato);
		
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = null;
		List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTOList = query.getResultList();
		for(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO2 : lineaSoporteReaseguroDTOList){		
			if (lineaSoporteReaseguroDTO2.getSoporteReaseguroDTO().getNumeroEndoso().intValue() == 0){		
				lineaSoporteReaseguroDTO = lineaSoporteReaseguroDTO2;
			}
		}
		 
		return lineaSoporteReaseguroDTO;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<EstadoCuentaDTO> obtenerSerieEstadosCuenta(EstadoCuentaDTO estadoCuentaDTO){
		String queryString = "SELECT model FROM EstadoCuentaDTO model WHERE " +
				"model.subRamoDTO.idTcSubRamo = :idTcSubRamo " +
				"AND model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor " +
				"AND model.idMoneda = :idMoneda " +
				"AND model.tipoReaseguro = :tipoReaseguro " +
				"AND model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo " +
				"AND model.idPoliza = :idPoliza " +
				"AND model.numeroEndoso = :numeroEndoso " +
				"AND model.ejercicio = :ejercicio ORDER BY model.suscripcion";
		
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTcSubRamo", estadoCuentaDTO.getSubRamoDTO().getIdTcSubRamo());
		query.setParameter("idtcreaseguradorcorredor", estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
		query.setParameter("idMoneda", estadoCuentaDTO.getIdMoneda());
		query.setParameter("tipoReaseguro", TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);
		query.setParameter("idTmContratoFacultativo", estadoCuentaDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo());
		query.setParameter("idPoliza", estadoCuentaDTO.getIdPoliza());
		query.setParameter("numeroEndoso", estadoCuentaDTO.getNumeroEndoso());
		query.setParameter("ejercicio", estadoCuentaDTO.getEjercicio());
		
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public CuentaBancoDTO obtenerCuentaBanco(EstadoCuentaDecoradoDTO estadoCuenta,Integer idMoneda) {
		
		CuentaBancoDTO cuentaBancoDTO = new CuentaBancoDTO();
		BigDecimal idtcreaseguradorcorredor = estadoCuenta.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor();
		idMoneda = idMoneda != null ? idMoneda : estadoCuenta.getIdMoneda();
		BigDecimal idTmContratoCuotaParte = null;
		BigDecimal idTmContratoPrimerExcedente = null;
		BigDecimal idTmContratoFacultativo = null;
		String queryString = "";
		if (idMoneda == MonedaDTO.MONEDA_PESOS){
			queryString = "cuentaBancoPesos";
		}else{
			if (idMoneda == MonedaDTO.MONEDA_DOLARES){
				queryString = "cuentaBancoDolares";
			}
		}
		if (estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			idTmContratoCuotaParte = estadoCuenta.getContratoCuotaParteDTO().getIdTmContratoCuotaParte();
				queryString = "SELECT model."+queryString+" FROM ParticipacionDTO model WHERE model.contratoCuotaParte.idTmContratoCuotaParte = :idTmContratoCuotaParte " +
				" AND model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";
		}else{
			if (estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
				idTmContratoPrimerExcedente = estadoCuenta.getContratoPrimerExcedenteDTO().getIdTmContratoPrimerExcedente();
				queryString = "SELECT model."+queryString+" FROM ParticipacionDTO model WHERE model.contratoPrimerExcedente.idTmContratoPrimerExcedente = :idTmContratoPrimerExcedente " +
				" AND model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";
			}else{
				if (estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
					idTmContratoFacultativo = estadoCuenta.getContratoFacultativoDTO().getIdTmContratoFacultativo();
					if (idMoneda == MonedaDTO.MONEDA_PESOS){
						queryString = "cuentaBancoDTOByIdtccuentabancopesos";
					}else{
						if (idMoneda == MonedaDTO.MONEDA_DOLARES){
							queryString = "cuentaBancoDTOByIdtccuentabancodolares";
						}
					}
					queryString = "SELECT model."+queryString+" FROM ParticipacionFacultativoDTO model WHERE " +
							"model.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo " +
							"AND model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";
				}
			}
		}
		Query query = entityManager.createQuery(queryString);
		if (queryString.contains(":idTmContratoCuotaParte"))
			query.setParameter("idTmContratoCuotaParte", idTmContratoCuotaParte);
		if (queryString.contains(":idTmContratoPrimerExcedente"))
			query.setParameter("idTmContratoPrimerExcedente", idTmContratoPrimerExcedente);
		if (queryString.contains(":idTmContratoFacultativo"))
			query.setParameter("idTmContratoFacultativo", idTmContratoFacultativo);
		
		query.setParameter("idtcreaseguradorcorredor", idtcreaseguradorcorredor);
		Vector<CuentaBancoDTO> cuentaBancoDTOList = (Vector<CuentaBancoDTO>) query.getResultList();
		if (cuentaBancoDTOList.size() > 0)
			cuentaBancoDTO = (CuentaBancoDTO)cuentaBancoDTOList.get(0);
		else
			cuentaBancoDTO = null;
		
		if (cuentaBancoDTO == null || cuentaBancoDTO.getIdtccuentabanco() == null){
			if (idMoneda == MonedaDTO.MONEDA_PESOS){
				queryString = "cuentaBancoPesos";
			}else{
				if (idMoneda == MonedaDTO.MONEDA_DOLARES){
					queryString = "cuentaBancoDolares";
				}
			}
			
			if (estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
				queryString = "SELECT model.participacion."+queryString+" FROM ParticipacionCorredorDTO model " +
						"WHERE model.participacion.contratoCuotaParte.idTmContratoCuotaParte = :idTmContratoCuotaParte " +
						"AND model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";
			}else{
				if (estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
					queryString = "SELECT model.participacion."+queryString+" FROM ParticipacionCorredorDTO model " +
					"WHERE model.participacion.contratoPrimerExcedente.idTmContratoPrimerExcedente = :idTmContratoPrimerExcedente " +
					"AND model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";
				}else{
					if (estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
						if (idMoneda == MonedaDTO.MONEDA_PESOS){
							queryString = "cuentaBancoDTOByIdtccuentabancopesos";
						}else{
							if (idMoneda == MonedaDTO.MONEDA_DOLARES){
								queryString = "cuentaBancoDTOByIdtccuentabancodolares";
							}
						}
						queryString = "SELECT model.participacionFacultativoDTO."+queryString+" FROM ParticipacionCorredorFacultativoDTO model WHERE " +
								"model.participacionFacultativoDTO.detalleContratoFacultativoDTO.contratoFacultativoDTO_1.idTmContratoFacultativo = :idTmContratoFacultativo " +
								"AND model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";
					}
				}
			}
			
			query = entityManager.createQuery(queryString);
			if (queryString.contains(":idTmContratoCuotaParte"))
				query.setParameter("idTmContratoCuotaParte", idTmContratoCuotaParte);
			if (queryString.contains(":idTmContratoPrimerExcedente"))
				query.setParameter("idTmContratoPrimerExcedente", idTmContratoPrimerExcedente);
			if (queryString.contains(":idTmContratoFacultativo"))
				query.setParameter("idTmContratoFacultativo", idTmContratoFacultativo);
			
			query.setParameter("idtcreaseguradorcorredor", idtcreaseguradorcorredor);
			
			cuentaBancoDTOList = new Vector<CuentaBancoDTO>();
			cuentaBancoDTOList = (Vector<CuentaBancoDTO>) query.getResultList();
			if (cuentaBancoDTOList.size() > 0)
				cuentaBancoDTO = (CuentaBancoDTO)cuentaBancoDTOList.get(0);
			else
				cuentaBancoDTO = null;
			
		}
		
		return cuentaBancoDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<LineaDTO> obtenerLineasAutorizadas(Date fecha){
			try {
		 String queryString = "SELECT model FROM LineaDTO model WHERE model.estatus = 1 " +
					"and model.estadosCuentaCreados = :estadoscuentacreados";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("estadoscuentacreados", 0);
			return query.getResultList();
			} catch (RuntimeException re) {
				LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
		            throw re;
			} 
	     }

	@SuppressWarnings("unchecked")
	public List<ContratoFacultativoDTO> obtenerContratoAutorizadasFacultativo(Date fecha){
		try {
			String queryString = "SELECT model FROM ContratoFacultativoDTO model WHERE model.estatus = 1 " +
				"and model.estadosCuentaCreados = :estadoscuentacreados";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("estadoscuentacreados", 0);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		} 
	 }

	@SuppressWarnings("unchecked")
	public List<AcumuladorDTO> obtenerAcumuladoresPorIdEstadoCta(BigDecimal idEstadoCuenta){
		List<AcumuladorDTO> acumuladorDTOList = null;
		try{
			String queryString = "SELECT model.acumuladorDTOs FROM EstadoCuentaDTO model WHERE model.idEstadoCuenta = :idEstadoCuenta";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idEstadoCuenta", idEstadoCuenta);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			acumuladorDTOList = query.getResultList();
			for(AcumuladorDTO acumuladorDTO : acumuladorDTOList){
				ConceptoMovimientoDTO conceptoTMP = acumuladorDTO.getConceptoMovimientoDTO();
				acumuladorDTO.setConceptoMovimientoDTO(conceptoTMP);
			}
			return acumuladorDTOList;
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Ocurri� un error al consultar los acumuladores del estado de cuenta: "+idEstadoCuenta, Level.SEVERE, e);
			throw e;
		}
	}
	
	@SuppressWarnings({ "unchecked" })
	public SaldoEgresoDTO obtenerEgresoPorIdEstadoCta(BigDecimal idEstadoCuenta){
		try{
			String queryString = "select sum(edo.monto), edo.IDTOESTADOCUENTA from MIDAS.TOESTADOCUENTA cta " +
								 "inner join MIDAS.TREGRESOESTADOCUENTA edo " +
								 "        on cta.IDTOESTADOCUENTA = edo.IDTOESTADOCUENTA " +
								 "where edo.ESTATUS = 0 and cta.IDTOESTADOCUENTA=" + idEstadoCuenta +
								 " group by edo.IDTOESTADOCUENTA";
			Query query = entityManager.createNativeQuery(queryString);
			
			 List<SaldoEgresoDTO> listaEgresos = query.getResultList();
			 SaldoEgresoDTO saldoEgreso = new SaldoEgresoDTO();
			 for (Object resultadoEgreso : listaEgresos) { 				 
				 BigDecimal monto = new BigDecimal("0.0");
				 BigDecimal idtoestadocuenta = new BigDecimal("0.0");	
				 monto = (BigDecimal) ((Object[])resultadoEgreso)[0];
				 idtoestadocuenta = (BigDecimal) ((Object[])resultadoEgreso)[1];
				 saldoEgreso.setMonto(monto);
				 saldoEgreso.setIdestadocuenta(idtoestadocuenta);
			 }
	         
          	 return saldoEgreso;			
			
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Ocurri� un error al consultar los saldos del egreso, en el estado de cuenta: "+idEstadoCuenta, Level.SEVERE, e);
			throw e;
		}
	}

	public EstadoCuentaDTO obtenerEstadoCuentaHistorico(BigDecimal idEstadoCuenta){
		EstadoCuentaDTO estadoCuentaDTO = this.findById(idEstadoCuenta);
		estadoCuentaDTO.setAcumuladorDTOs(consultarAcumuladoresEstadoCuenta(idEstadoCuenta));
		obtenerEstadoCuentaAnterior(estadoCuentaDTO);
		return estadoCuentaDTO;
	}
	
	private void obtenerEstadoCuentaAnterior(EstadoCuentaDTO estadoCuentaDTO){
		EstadoCuentaDTO estadoCuentaSuscripcionAnterior = null;
		if (estadoCuentaDTO.getSuscripcion() < 1){
			throw new RuntimeException("N�mero de suscripci�n no v�lido");
		}
		else if (estadoCuentaDTO.getSuscripcion() == 1){
			//Primera suscripcion, no existe estado de cuenta anterior.
			return;
		}
		else {
			//Se buscan los acumuladores de la suscripci�n anterior
			estadoCuentaSuscripcionAnterior = obtenerEstadoCuentaPorSuscripcion(estadoCuentaDTO, estadoCuentaDTO.getSuscripcion()-1);
			if(estadoCuentaSuscripcionAnterior == null){
				//Si no existe el estado de cuenta anterior, se instancia un estado de cuenta representativo para ese periodo
				estadoCuentaSuscripcionAnterior = estadoCuentaDTO.duplicate();
				estadoCuentaSuscripcionAnterior.setAcumuladorDTOs(new ArrayList<AcumuladorDTO>());
				estadoCuentaSuscripcionAnterior.setSuscripcion(estadoCuentaDTO.getSuscripcion()-1);
				estadoCuentaSuscripcionAnterior.setIdEstadoCuenta(BigDecimal.ZERO);
//				estadoCuentaDTO.setEstadoCuentaSuscripcionAnterior(estadoCuentaSuscripcionAnterior);
			}else{
				estadoCuentaSuscripcionAnterior.setAcumuladorDTOs(consultarAcumuladoresEstadoCuenta(estadoCuentaSuscripcionAnterior.getIdEstadoCuenta()));
			}
			estadoCuentaDTO.setEstadoCuentaSuscripcionAnterior(estadoCuentaSuscripcionAnterior);
			obtenerEstadoCuentaAnterior(estadoCuentaSuscripcionAnterior);
		}
	}
	
	@SuppressWarnings("unchecked")
	private EstadoCuentaDTO obtenerEstadoCuentaPorSuscripcion(EstadoCuentaDTO estadoCuentaDTO,int suscripcion){
		EstadoCuentaDTO estadoCuentaSuscripcionDTO = null;
		String queryString = "";
		queryString ="SELECT model from EstadoCuentaDTO model " +
					"WHERE model.idMoneda = :idMoneda AND model.subRamoDTO.idTcSubRamo = :idTcSubRamo " +
					"AND model.tipoReaseguro = :tipoReaseguro AND model.ejercicio = :ejercicio AND model.suscripcion  = :suscripcion";
		if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_RETENCION){
			queryString = queryString + " AND model.lineaDTO.idTmLinea = :idTmLinea";
		}
		else{
			if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
				queryString = queryString + " AND model.contratoCuotaParteDTO.idTmContratoCuotaParte = :idTmContratoCuotaParte";
			}else if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
				queryString = queryString + " AND model.contratoPrimerExcedenteDTO.idTmContratoPrimerExcedente = :idTmContratoPrimerExcedente";
			}else if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
				queryString = queryString + " AND model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo";
			}
			if (estadoCuentaDTO.getReaseguradorCorredorDTO() != null){
				queryString = queryString + " AND model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";
			}
			if (estadoCuentaDTO.getCorredorDTO() != null){
				queryString = queryString + " AND model.corredorDTO.idtcreaseguradorcorredor = :idtccorredor";
			}
		}
		
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idMoneda", estadoCuentaDTO.getIdMoneda());
		query.setParameter("idTcSubRamo", estadoCuentaDTO.getSubRamoDTO().getIdTcSubRamo());
		query.setParameter("tipoReaseguro", estadoCuentaDTO.getTipoReaseguro());
		query.setParameter("ejercicio", estadoCuentaDTO.getEjercicio());
		query.setParameter("suscripcion", suscripcion);
		
		if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE)
			query.setParameter("idTmContratoCuotaParte", estadoCuentaDTO.getContratoCuotaParteDTO().getIdTmContratoCuotaParte());
		if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE)
			query.setParameter("idTmContratoPrimerExcedente", estadoCuentaDTO.getContratoPrimerExcedenteDTO().getIdTmContratoPrimerExcedente());
		if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO)
			query.setParameter("idTmContratoFacultativo", estadoCuentaDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo());
		if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_RETENCION)
			query.setParameter("idTmLinea", estadoCuentaDTO.getLineaDTO().getIdTmLinea());
		if (queryString.contains(":idtcreaseguradorcorredor"))
			query.setParameter("idtcreaseguradorcorredor", estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
		if (queryString.contains(":idtccorredor"))
			query.setParameter("idtccorredor", estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor());
		
		List<EstadoCuentaDTO> listaEstadoCuenta = query.getResultList();
		if(listaEstadoCuenta != null && !listaEstadoCuenta.isEmpty()){
			if(listaEstadoCuenta.size() == 1){
				estadoCuentaSuscripcionDTO = listaEstadoCuenta.get(0);
				estadoCuentaSuscripcionDTO.setAcumuladorDTOs(estadoCuentaSuscripcionDTO.getAcumuladorDTOs());
			}
			else{
				String errorInfo = "Se encontr� m�s de un estado de cuenta para la suscripcion "+suscripcion+" del estado de cuenta: "+estadoCuentaDTO.getIdEstadoCuenta();
				LogDeMidasEJB3.log(errorInfo, Level.SEVERE, new RuntimeException(errorInfo));
				estadoCuentaSuscripcionDTO = null;
			}
		}
		return estadoCuentaSuscripcionDTO;
	}
	
	@SuppressWarnings("unchecked")
	private List<Object[]> obtenerEstadoCuentaAcumuladores(EstadoCuentaDTO estadoCuentaDTO,int suscripcion){
		String queryString = "";
		queryString ="SELECT model, model.acumuladorDTOs from EstadoCuentaDTO model " +
					"WHERE model.idMoneda = :idMoneda AND model.subRamoDTO.idTcSubRamo = :idTcSubRamo " +
					"AND model.tipoReaseguro = :tipoReaseguro AND model.ejercicio = :ejercicio AND model.suscripcion  = :suscripcion";
		
		if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_RETENCION){
			queryString = queryString + " AND model.lineaDTO.idTmLinea = :idTmLinea";
		}
		else{
			if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
				queryString = queryString + " AND model.contratoCuotaParteDTO.idTmContratoCuotaParte = :idTmContratoCuotaParte";
			}else if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
				queryString = queryString + " AND model.contratoPrimerExcedenteDTO.idTmContratoPrimerExcedente = :idTmContratoPrimerExcedente";
			}else if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
				queryString = queryString + " AND model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo";
			}
			if (estadoCuentaDTO.getReaseguradorCorredorDTO() != null){
				queryString = queryString + " AND model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";
			}
			if (estadoCuentaDTO.getCorredorDTO() != null){
				queryString = queryString + " AND model.corredorDTO.idtcreaseguradorcorredor = :idtccorredor";
			}
		}
		
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idMoneda", estadoCuentaDTO.getIdMoneda());
		query.setParameter("idTcSubRamo", estadoCuentaDTO.getSubRamoDTO().getIdTcSubRamo());
		query.setParameter("tipoReaseguro", estadoCuentaDTO.getTipoReaseguro());
		query.setParameter("ejercicio", estadoCuentaDTO.getEjercicio());
		
		query.setParameter("suscripcion", suscripcion);
		
		if (queryString.contains(":idTmContratoCuotaParte"))
			query.setParameter("idTmContratoCuotaParte", estadoCuentaDTO.getContratoCuotaParteDTO().getIdTmContratoCuotaParte());
		if (queryString.contains(":idTmContratoPrimerExcedente"))
			query.setParameter("idTmContratoPrimerExcedente", estadoCuentaDTO.getContratoPrimerExcedenteDTO().getIdTmContratoPrimerExcedente());
		if (queryString.contains(":idTmContratoFacultativo"))
			query.setParameter("idTmContratoFacultativo", estadoCuentaDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo());
		if (queryString.contains(":idTmLinea"))
			query.setParameter("idTmLinea", estadoCuentaDTO.getLineaDTO().getIdTmLinea());
		if (queryString.contains(":idtcreaseguradorcorredor"))
			query.setParameter("idtcreaseguradorcorredor", estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
		if (queryString.contains(":idtccorredor"))
			query.setParameter("idtccorredor", estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor());
		
		List<Object[]> objectsResult = query.getResultList();
		return objectsResult;
	}
	
	@SuppressWarnings("unchecked")
	public List<EstadoCuentaDTO> obtenerEstadosCuentaPorIdsTipoReaseguro(String[] idsEdosCta, int[] tiposReaseguro){		
		try{
			String idsEdoCtaStr = obtenerIdsEdoCta(idsEdosCta);
			idsEdoCtaStr = idsEdoCtaStr.substring(0,idsEdoCtaStr.length()-1);
			
			String tiposReaseguroStr = obtenerReaseguroStr(tiposReaseguro);
			tiposReaseguroStr = tiposReaseguroStr.substring(0,tiposReaseguroStr.length()-1);
			
			String queryString = "";
			queryString ="SELECT edoCta from EstadoCuentaDTO edoCta " +
						"WHERE edoCta.idEstadoCuenta in (" + idsEdoCtaStr + ") " +
						  "AND edoCta.tipoReaseguro in (" + tiposReaseguroStr + ")";
			
			
			Query query = entityManager.createQuery(queryString);			
			
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� EstadoCuentaFacade.obtenerEstadosCuentaPorIdsTipoReaseguro", Level.SEVERE, re);
	            throw re;
		} 
	}

	public String obtenerReaseguroStr(int[] tiposReaseguro) {
		StringBuilder tiposReaseguroStr = new StringBuilder("");
		for(int tipoReaseguro : tiposReaseguro)
			tiposReaseguroStr.append(tipoReaseguro).append(",");
		return tiposReaseguroStr.toString();
	}

	public String obtenerIdsEdoCta(String[] idsEdosCta) {
		StringBuilder idsEdoCtaStr = new StringBuilder("");
		for(String idEdoCta : idsEdosCta)
			idsEdoCtaStr.append(idEdoCta).append(",");
		return idsEdoCtaStr.toString();
	}
}