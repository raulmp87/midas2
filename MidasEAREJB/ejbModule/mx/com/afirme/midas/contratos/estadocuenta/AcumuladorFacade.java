package mx.com.afirme.midas.contratos.estadocuenta;
// default package

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDTO;
import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDetalleDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroServiciosRemote;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity AcumuladorDTO.
 * @see .AcumuladorDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class AcumuladorFacade  implements AcumuladorFacadeRemote {

	@EJB
	private MovimientoReaseguroServiciosRemote movimientoReaseguroServicios;
	
//	@EJB
//	private EstadoCuentaFacadeRemote estadoCuentaFacade;
	
    @PersistenceContext 
    private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved AcumuladorDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AcumuladorDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AcumuladorDTO entity) {
    				LogDeMidasEJB3.log("saving AcumuladorDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent AcumuladorDTO entity.
	  @param entity AcumuladorDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AcumuladorDTO entity) {
    				LogDeMidasEJB3.log("deleting AcumuladorDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(AcumuladorDTO.class, entity.getIdAcumulador());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved AcumuladorDTO entity and return it or a copy of it to the sender. 
	 A copy of the AcumuladorDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AcumuladorDTO entity to update
	 @return AcumuladorDTO the persisted AcumuladorDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public AcumuladorDTO update(AcumuladorDTO entity) {
    				LogDeMidasEJB3.log("updating AcumuladorDTO instance", Level.INFO, null);
	        try {
            AcumuladorDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public AcumuladorDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding AcumuladorDTO instance with id: " + id, Level.INFO, null);
	        try {
            AcumuladorDTO instance = entityManager.find(AcumuladorDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all AcumuladorDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AcumuladorDTO property to query
	  @param value the property value to match
	  	  @return List<AcumuladorDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<AcumuladorDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding AcumuladorDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from AcumuladorDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all AcumuladorDTO entities.
	  	  @return List<AcumuladorDTO> all AcumuladorDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<AcumuladorDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all AcumuladorDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from AcumuladorDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	 /**
	 * Obtener el acumulador basado en el movimiento a acumular y el estado de cuenta al cu�l se asocia.
	 * Si el acumulador no es encontrado regresar null 
	 * @param movimiento
	 * @param estadoCuenta
	 * @return AcumuladorDTO
	 */
	@SuppressWarnings("unchecked")
	public AcumuladorDTO obtenerAcumuladorPorMovimientoYEstadoCuenta(MovimientoReaseguroDTO movimiento, EstadoCuentaDTO estadoCuenta){
		AcumuladorDTO acumuladorDTO = null;
		int idConceptoMovimiento = 0;

		if (movimiento.getConceptoMovimientoDetalleDTO() == null){
			movimiento.setConceptoMovimientoDetalleDTO(entityManager.find(ConceptoMovimientoDetalleDTO.class, movimiento.getConceptoMovimiento()));
		}

		if(movimiento.getConceptoMovimientoDetalleDTO().getConceptoMovimientoDTO() == null){
			String queryStr = "select model.conceptoMovimientoDTO from ConceptoMovimientoDetalleDTO model where model.idConceptoDetalle = :idConceptoDetalle";
			Query query = entityManager.createQuery(queryStr);
			query.setParameter("idConceptoDetalle", movimiento.getConceptoMovimiento());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);

			ConceptoMovimientoDTO conceptoDTO = (ConceptoMovimientoDTO)query.getSingleResult();
			if(conceptoDTO != null){
				idConceptoMovimiento = conceptoDTO.getIdConceptoMovimiento();
			}
		}else{
			idConceptoMovimiento = movimiento.getConceptoMovimientoDetalleDTO().getConceptoMovimientoDTO().getIdConceptoMovimiento();
		}


		/** obtener Acumulador en base al movimiento y al estado de cuenta
		 * select model from AcumuladorDTO model where
		 * model.estadoCuentaDTO.idEstadoCuenta = ? and
		 * model.conceptoMovimientoDTO.idConceptoMovimiento = ? and
		 * model.mes = ? and
		 * model.anio = ?
		 */
		String queryString = "SELECT model FROM AcumuladorDTO model WHERE ";
		queryString += "model.estadoCuentaDTO.idEstadoCuenta = :idEstadoCuenta AND ";
		queryString += " model.conceptoMovimientoDTO.idConceptoMovimiento = :idConceptoMovimiento AND ";
		queryString += " model.mes = :mes AND ";
		queryString += "model.anio = :anio";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idEstadoCuenta",estadoCuenta.getIdEstadoCuenta());
		//query.setParameter("idConceptoMovimiento",conceptoDetalle.getConceptoMovimientoDTO().getIdConceptoMovimiento());	
		query.setParameter("idConceptoMovimiento",idConceptoMovimiento);
		query.setParameter("mes",obtenerMes(movimiento.getFechaRegistro()));
		query.setParameter("anio",obtenerAno(movimiento.getFechaRegistro()));
		List<AcumuladorDTO> acumuladorDTOList = query.getResultList();
		if (acumuladorDTOList.size() == 1){
			acumuladorDTO = acumuladorDTOList.get(0);
		}else if (acumuladorDTOList.size() != 0){
			new SQLException("Mas de un acumulador fue obtenido");
		}
		return acumuladorDTO;
	}

	private int obtenerAno(Date FechaRegistro){
		try{
		 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	     String fecha= sdf.format(FechaRegistro);
	     return Integer.valueOf(fecha.substring(6, 10)).intValue();
	 	} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fallo al obtener el anio del movimiento", Level.SEVERE, re);
         throw re;
		}  
	}
	
	
	private int obtenerMes(Date FechaRegistro){
		try{
		 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	     String fecha= sdf.format(FechaRegistro);
	     return Integer.valueOf(fecha.substring(3, 5)).intValue();
	 	} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fallo al obtener el mes del movimiento", Level.SEVERE, re);
         throw re;
		}  
	}
	
	public Date obtenerFechaLinea(LineaDTO lineaDTO){
     Date fechaInicial;
		try{
		LineaDTO  instance = entityManager.find(LineaDTO.class, lineaDTO.getIdTmLinea()); 
        LineaDTO linea = instance;
        fechaInicial = linea.getFechaInicial();
		} catch (RuntimeException re) {
    				LogDeMidasEJB3.log("Fallo al obtener la fechaLinea", Level.SEVERE, re);
            throw re;
      }
      return fechaInicial;
	}

	public void reestablecerAcumuladores(Integer mes,Integer anio) {
		String queryString = "UPDATE AcumuladorDTO model SET model.cargo = 0, model.abono = 0";
		boolean reestablecerPorRango = false;
		if(mes != null && anio != null){
			if(mes.intValue() > 0 && mes.intValue()< 12 && anio.intValue() > 2008){
				queryString += " where model.mes = :mes and model.anio = :anio";
				reestablecerPorRango = true;
			}
		}
		Query query = entityManager.createQuery(queryString);
		if(reestablecerPorRango){
			query.setParameter("mes", mes);
			query.setParameter("anio", anio);
		}
		query.executeUpdate();
	}
	
	/**
	 * Determina el estado de cuenta asociado para el movimiento de reaseguro indicado.
	 * En caso de no exitir un estado de cuenta asociado regresar null
	 * @param movimiento
	 * @return EstadoCuentaDTO
	 */	
	@SuppressWarnings("unchecked")
	public EstadoCuentaDTO obtenerEstadoCuentaPorMovimiento(MovimientoReaseguroDTO movimiento) {
		EstadoCuentaDTO estadoCuenta = null;
		List<EstadoCuentaDTO> estadoCuentaDTOList;
		/*		
		 * Si tipoReaseguro puede ser
		 *      TipoReaseguroDTO. TIPO_CONTRATO_CUOTA_PARTE
		 *		TipoReaseguroDTO. TIPO_CONTRATO_PRIMER_EXCEDENTE
		 *		TipoReaseguroDTO. TIPO_CONTRATO_FACULTATIVO
		 *		TipoReaseguroDTO. TIPO_RETENCION
	 	 *		
		*/		
 		
		if (movimiento.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			if (movimiento.getReaseguradorCorredor() != null){ 
			 String queryString = "select model from EstadoCuentaDTO model where ";
			 queryString += "model.subRamoDTO.idTcSubRamo = :idTcSubRamo and ";
			 queryString += "model.tipoReaseguro = :tipoReaseguro and ";
			 queryString += "model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idTcCreaseguradorCorredor and ";
			 queryString += "model.contratoCuotaParteDTO.idTmContratoCuotaParte = :idTmContratoCoutaParte and ";
			 queryString += "model.idMoneda = :idMoneda and ";
			 queryString += "model.ejercicio = :ejercicio and ";
			 queryString += "model.suscripcion = :suscripcion";
			  Query query = entityManager.createQuery(queryString);
			 query.setParameter("idTcSubRamo",movimiento.getSubRamo().getId());
			 query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE);	
			 query.setParameter("idTcCreaseguradorCorredor",movimiento.getReaseguradorCorredor().getIdtcreaseguradorcorredor());
			 query.setParameter("idTmContratoCoutaParte",movimiento.getContratoCuotaParteDTO().getIdTmContratoCuotaParte());
			 query.setParameter("idMoneda",movimiento.getIdMoneda());
			 query.setParameter("ejercicio",movimiento.getEjercicio());
			 query.setParameter("suscripcion",movimiento.getSuscripcion());
			 estadoCuentaDTOList = query.getResultList();
			  if(estadoCuentaDTOList.size() == 1){
			    estadoCuenta= estadoCuentaDTOList.get(0);
	 		 }else if(estadoCuentaDTOList.size() != 0){
	 			throw new RuntimeException("Mas de un estado de cuenta fue obtenido"); 
	  		 }
		  }else{
			String queryString = "select model from EstadoCuentaDTO model where ";
			queryString += "model.subRamoDTO.idTcSubRamo = :idTcSubRamo and ";
			queryString += "model.lineaDTO.idTmLinea = :idTmLinea and ";
			queryString += "model.tipoReaseguro = :tipoReaseguro and ";
			queryString += "model.contratoCuotaParteDTO.idTmContratoCuotaParte = :idTmContratoCuotaParte and ";
			queryString += "model.idMoneda = :idMoneda and ";
			queryString += "model.ejercicio = :ejercicio and ";
			queryString += "model.suscripcion = :suscripcion and ";
			queryString += "model.reaseguradorCorredorDTO.idtcreaseguradorcorredor is null";
		 	Query query = entityManager.createQuery(queryString);
			query.setParameter("idTcSubRamo",movimiento.getSubRamo().getIdTcSubRamo());
			query.setParameter("idTmLinea",movimiento.getLineaDTO().getIdTmLinea());
			query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE);	
			query.setParameter("idTmContratoCuotaParte",movimiento.getContratoCuotaParteDTO().getIdTmContratoCuotaParte());
			query.setParameter("idMoneda",movimiento.getIdMoneda());
			query.setParameter("ejercicio",movimiento.getEjercicio());
			query.setParameter("suscripcion",movimiento.getSuscripcion());
			estadoCuentaDTOList = query.getResultList();
	 		if(estadoCuentaDTOList.size() == 1){
			    estadoCuenta= estadoCuentaDTOList.get(0);
	 		 }else if(estadoCuentaDTOList.size() != 0){
	 			throw new RuntimeException("Mas de un estado de cuenta fue obtenido"); 
	  		 }
		  }
		}else{
			if (movimiento.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
				String queryString = "select model from EstadoCuentaDTO model where ";
				queryString += "model.subRamoDTO.idTcSubRamo = :idTcSubRamo and ";
				queryString += "model.tipoReaseguro = :tipoReaseguro and ";
				queryString += "model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idTcCreaseguradorCorredor and ";
				queryString += "model.contratoPrimerExcedenteDTO.idTmContratoPrimerExcedente = :idTmContratoPrimerExcedente and ";
				queryString += "model.idMoneda = :idMoneda and ";
				queryString += "model.ejercicio = :ejercicio and ";
				queryString += "model.suscripcion = :suscripcion";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idTcSubRamo",movimiento.getSubRamo().getId());
				query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE);	
				query.setParameter("idTcCreaseguradorCorredor",movimiento.getReaseguradorCorredor().getIdtcreaseguradorcorredor());
				query.setParameter("idTmContratoPrimerExcedente",movimiento.getContratoPrimerExcedenteDTO().getIdTmContratoPrimerExcedente());
				query.setParameter("idMoneda",movimiento.getIdMoneda());
				query.setParameter("ejercicio",movimiento.getEjercicio());
				query.setParameter("suscripcion",movimiento.getSuscripcion());
				estadoCuentaDTOList = query.getResultList();
				if(estadoCuentaDTOList.size() == 1){
					estadoCuenta= estadoCuentaDTOList.get(0);
				}else if(estadoCuentaDTOList.size() != 0){
					throw new RuntimeException("Mas de un estado de cuenta fue obtenido"); 
				}
			}else{
				if (movimiento.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
					if (movimiento.getReaseguradorCorredor() != null){ 
						String queryString = "select model from EstadoCuentaDTO model where ";
						queryString += "model.subRamoDTO.idTcSubRamo = :idTcSubRamo and ";
						queryString += "model.tipoReaseguro = :tipoReaseguro and ";
						queryString += "model.reaseguradorCorredorDTO.idtcreaseguradorcorredor = :idTcCreaseguradorCorredor and ";
						queryString += "model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo and ";
						queryString += "model.idMoneda = :idMoneda and ";
						queryString += "model.idPoliza = :idPoliza and ";
						queryString += "model.numeroEndoso = :numeroEndoso and ";
						queryString += "model.ejercicio = :ejercicio and ";
						queryString += "model.suscripcion = :suscripcion";
						Query query = entityManager.createQuery(queryString);
						query.setParameter("idTcSubRamo",movimiento.getSubRamo().getIdTcSubRamo());
						query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);	
						query.setParameter("idTcCreaseguradorCorredor",movimiento.getReaseguradorCorredor().getIdtcreaseguradorcorredor());
						query.setParameter("idTmContratoFacultativo",movimiento.getContratoFacultativoDTO().getIdTmContratoFacultativo());
						query.setParameter("idMoneda",movimiento.getIdMoneda());
						query.setParameter("idPoliza",movimiento.getIdPoliza());
						query.setParameter("numeroEndoso",movimiento.getNumeroEndoso());
						query.setParameter("ejercicio",movimiento.getEjercicio());
						query.setParameter("suscripcion",movimiento.getSuscripcion());
						estadoCuentaDTOList = query.getResultList();
						if(estadoCuentaDTOList.size() == 1){
							estadoCuenta= estadoCuentaDTOList.get(0);
						}else if(estadoCuentaDTOList.size() != 0){
							throw new RuntimeException("Mas de un estado de cuenta fue obtenido"); 
						}
					}else{
						String queryString = "select model from EstadoCuentaDTO model where ";
						queryString += "model.subRamoDTO.idTcSubRamo = :idTcSubRamo and ";
						queryString += "model.lineaDTO.idTmLinea = :idTmLinea and ";
						queryString += "model.tipoReaseguro = :tipoReaseguro and ";
						queryString += "model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo and ";
						queryString += "model.idMoneda = :idMoneda and ";
						queryString += "model.idPoliza = :idPoliza and ";
						queryString += "model.numeroEndoso = :numeroEndoso and ";
						queryString += "model.ejercicio = :ejercicio and ";
						queryString += "model.suscripcion = :suscripcion and ";
						queryString += "model.reaseguradorCorredorDTO.idtcreaseguradorcorredor is null";
						Query query = entityManager.createQuery(queryString);
						query.setParameter("idTcSubRamo",movimiento.getSubRamo().getIdTcSubRamo());
						query.setParameter("idTmLinea",movimiento.getLineaDTO().getIdTmLinea());
						query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);	
						query.setParameter("idTmContratoFacultativo",movimiento.getContratoFacultativoDTO().getIdTmContratoFacultativo());
						query.setParameter("idMoneda",movimiento.getIdMoneda());
						query.setParameter("idPoliza",movimiento.getIdPoliza());
						query.setParameter("numeroEndoso",movimiento.getNumeroEndoso());
						query.setParameter("ejercicio",movimiento.getEjercicio());
						query.setParameter("suscripcion",movimiento.getSuscripcion());
						estadoCuentaDTOList = query.getResultList();
						if(estadoCuentaDTOList.size() == 1){
							estadoCuenta= estadoCuentaDTOList.get(0);
						}else if(estadoCuentaDTOList.size() != 0){
							throw new RuntimeException("Mas de un estado de cuenta fue obtenido"); 
						}
					}
				}else{
					if (movimiento.getTipoReaseguro() == TipoReaseguroDTO.TIPO_RETENCION){
						String queryString = "select model from EstadoCuentaDTO model where ";
						queryString += "model.subRamoDTO.idTcSubRamo = :idTcSubRamo and ";
						queryString += "model.tipoReaseguro = :tipoReaseguro and ";
						queryString += "model.lineaDTO.idTmLinea = :idTmLinea and ";
						queryString += "model.idMoneda = :idMoneda and ";
						queryString += "model.ejercicio = :ejercicio and ";
						queryString += "model.suscripcion = :suscripcion";
						Query query = entityManager.createQuery(queryString);
						query.setParameter("idTcSubRamo",movimiento.getSubRamo().getId());
						query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_RETENCION);	
						query.setParameter("idTmLinea",movimiento.getLineaDTO().getIdTmLinea());
						query.setParameter("idMoneda",movimiento.getIdMoneda());
						query.setParameter("ejercicio",movimiento.getEjercicio());
						query.setParameter("suscripcion",movimiento.getSuscripcion());
						estadoCuentaDTOList = query.getResultList();
						if(estadoCuentaDTOList.size() == 1){
							estadoCuenta= estadoCuentaDTOList.get(0);
						}else if(estadoCuentaDTOList.size() != 0){
							throw new RuntimeException("Mas de un estado de cuenta fue obtenido");
						}
					}
				}
			}
		}
        return estadoCuenta;
	}
	
	public String procesarLoteMovimientosARegenerar(List<MovimientoReaseguroDTO> movimientoReaseguroDTOList,Boolean restarCantidad){
//		BigDecimal abonoAcumulador;
//		BigDecimal cargoAcumulador;
//		AcumuladorDTO acumuladorDTO = null;
//		EstadoCuentaDTO estadoCuentaDTO;
//		int naturaleza = 0;
		StringBuilder movimientosNoProcesados = new StringBuilder("");
//		String descripcionCargos;
		for (MovimientoReaseguroDTO movimientoReaseguroDTO : movimientoReaseguroDTOList) {
			try{
				movimientoReaseguroServicios.registrarMovimiento(movimientoReaseguroDTO,restarCantidad);
//				LogDeMidasEJB3.log("Procesando entidad movimientoReaseguroDTO con id: " + movimientoReaseguroDTO.getIdMovimiento(), Level.INFO, null);
//				acumuladorDTO = null;
//				estadoCuentaDTO = null;
//				movimientoReaseguroDTO.setSuscripcion(movimientoReaseguroServicios.calcularSuscripcion(movimientoReaseguroDTO));
//				naturaleza = movimientoReaseguroDTO.getTipoMovimientoDTO().getNaturaleza();
//				
//				estadoCuentaDTO = this.obtenerEstadoCuentaPorMovimiento(movimientoReaseguroDTO);
//				if(estadoCuentaDTO == null){ //si no existe estado de cuenta crear uno
//					estadoCuentaDTO = movimientoReaseguroServicios.generaEstadoCuenta(movimientoReaseguroDTO);
//					entityManager.persist(estadoCuentaDTO);
//					LogDeMidasEJB3.log("El movimientoReaseguroDTO con id: " + movimientoReaseguroDTO.getIdMovimiento() + " no ten�a un estadoCuentaDTO asociado, el estadoCuentaDTO con id: " + estadoCuentaDTO.getIdEstadoCuenta() + " fue creado.", Level.INFO, null);
//				}
//				acumuladorDTO = this.obtenerAcumuladorPorMovimientoYEstadoCuenta(movimientoReaseguroDTO, estadoCuentaDTO);
//				if(acumuladorDTO == null){
//					acumuladorDTO = movimientoReaseguroServicios.generaAcumulador(movimientoReaseguroDTO, estadoCuentaDTO);
//					entityManager.persist(acumuladorDTO);
//					LogDeMidasEJB3.log("El movimientoReaseguroDTO con id: " + movimientoReaseguroDTO.getIdMovimiento() + " y estadoCuentaDTO con id: " +estadoCuentaDTO.getIdEstadoCuenta() + 
//							" no ten�a un acumuladorDTO asociado, el acumuladorDTO con id: " + acumuladorDTO.getIdAcumulador() + " fue creado.", Level.INFO, null);
//				}
//				descripcionCargos = "\tabono Original= \t" + acumuladorDTO.getAbono() + "\tcargoOriginal= \t" + acumuladorDTO.getCargo();
//				if (naturaleza == TipoMovimientoDTO.NATURALEZA_ABONO){
//					abonoAcumulador = acumuladorDTO.getAbono();
//					if(restarCantidad != null && restarCantidad)
//						abonoAcumulador = abonoAcumulador.subtract(movimientoReaseguroDTO.getCantidad());
//					else
//						abonoAcumulador = abonoAcumulador.add(movimientoReaseguroDTO.getCantidad());
//					acumuladorDTO.setAbono(abonoAcumulador.setScale(10, BigDecimal.ROUND_UNNECESSARY));
//				}else if (naturaleza == TipoMovimientoDTO.NATURALEZA_CARGO){
//					cargoAcumulador = acumuladorDTO.getCargo();
//					if(restarCantidad != null && restarCantidad)
//						cargoAcumulador = cargoAcumulador.subtract(movimientoReaseguroDTO.getCantidad());
//					else
//						cargoAcumulador = cargoAcumulador.add(movimientoReaseguroDTO.getCantidad());
//					acumuladorDTO.setCargo(cargoAcumulador.setScale(10, BigDecimal.ROUND_UNNECESSARY));
//				}
//				descripcionCargos += "\tCantidad: "+movimientoReaseguroDTO.getCantidad()+"\tabono Nuevo: "+ acumuladorDTO.getAbono()+"\tcargo Nuevo:\t"+acumuladorDTO.getCargo();
//				LogDeMidasEJB3.log("Actualizando acumuladorDTO with id: \t" + acumuladorDTO.getIdAcumulador() + "\t, movimientoReaseguroDTO id: \t" + movimientoReaseguroDTO.getIdMovimiento()+descripcionCargos, Level.INFO, null);
//				entityManager.merge(acumuladorDTO);
			} catch (RuntimeException re) {
				LogDeMidasEJB3.log("Method procesarLoteMovimientosARegenerar FAILED!, the entity movimientoReaseguroDTO id: " + movimientoReaseguroDTO.getIdMovimiento() + " was not added to its AcumuladorDTO.", Level.SEVERE, re);
				movimientosNoProcesados.append("," ).append(movimientoReaseguroDTO.getIdMovimiento());
			}
		}
		return movimientosNoProcesados.toString();
	}
	
	@SuppressWarnings("unchecked")
	public List<BigDecimal> obtenerIdsAcumuladores(Integer mes,Integer anio) {
		String queryString = null;
		boolean validarPorRango = false;
		queryString = "SELECT model.idAcumulador FROM AcumuladorDTO model ";
		if(mes != null && anio != null){
			if(mes.intValue() > 0 && mes.intValue()< 12 && anio.intValue() > 2008){
				queryString += " where model.mes = :mes and model.anio = :anio";
				validarPorRango = true;
			}
		}
		Query query = entityManager.createQuery(queryString);
		if(validarPorRango){
			query.setParameter("mes", mes);
			query.setParameter("anio", anio);
		}
		List<BigDecimal> resultado = query.getResultList();
		return resultado;
	}
}