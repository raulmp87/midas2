package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import java.math.BigDecimal;
import java.util.Date;

class EstructuraAgrupacionFiltradoMovimientos {
	private BigDecimal idTcReasegurador;
	private BigDecimal idTcCorredor;
	private BigDecimal idToPoliza;
	private Integer numeroEndoso;
	private Date fechaMovimiento;
	private BigDecimal idTcSubRamo;
	private BigDecimal tipoCambio;
	private BigDecimal idMoneda;
	private boolean incluirSoloFacultativo = false;
	private boolean incluirSoloAutomaticos = false;
	private BigDecimal idToReporteSiniestro;
	private BigDecimal idTcConceptoMovimiento;
	private Integer numeroInciso;
	private BigDecimal idToSeccion;
	
	public BigDecimal getIdTcReasegurador() {
		return idTcReasegurador;
	}
	public void setIdTcReasegurador(BigDecimal idTcReasegurador) {
		this.idTcReasegurador = idTcReasegurador;
	}
	public BigDecimal getIdTcCorredor() {
		return idTcCorredor;
	}
	public void setIdTcCorredor(BigDecimal idTcCorredor) {
		this.idTcCorredor = idTcCorredor;
	}
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	public BigDecimal getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public BigDecimal getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}
	public boolean isIncluirSoloFacultativo() {
		return incluirSoloFacultativo;
	}
	public void setIncluirSoloFacultativo(boolean incluirSoloFacultativo) {
		this.incluirSoloFacultativo = incluirSoloFacultativo;
	}
	public boolean isIncluirSoloAutomaticos() {
		return incluirSoloAutomaticos;
	}
	public void setIncluirSoloAutomaticos(boolean incluirSoloAutomaticos) {
		this.incluirSoloAutomaticos = incluirSoloAutomaticos;
	}
	public BigDecimal getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	public BigDecimal getIdTcConceptoMovimiento() {
		return idTcConceptoMovimiento;
	}
	public void setIdTcConceptoMovimiento(BigDecimal idTcConceptoMovimiento) {
		this.idTcConceptoMovimiento = idTcConceptoMovimiento;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}
	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
}
