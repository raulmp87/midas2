package mx.com.afirme.midas.contratos.estadocuenta.presentacion;
// default package

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity AgrupacionEstadoCuentaDTO.
 * @see .AgrupacionEstadoCuentaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class AgrupacionEstadoCuentaFacade  implements AgrupacionEstadoCuentaFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved AgrupacionEstadoCuentaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AgrupacionEstadoCuentaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AgrupacionEstadoCuentaDTO entity) {
    	LogUtil.log("saving AgrupacionEstadoCuentaDTO instance", Level.INFO, null);
    	try {
    		entityManager.persist(entity);
    		LogUtil.log("save successful", Level.INFO, null);
    	} catch (RuntimeException re) {
    		LogUtil.log("save failed", Level.SEVERE, re);
    		throw re;
        }
    }
    
    /**
	 Delete a persistent AgrupacionEstadoCuentaDTO entity.
	  @param entity AgrupacionEstadoCuentaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AgrupacionEstadoCuentaDTO entity) {
    	LogUtil.log("deleting AgrupacionEstadoCuentaDTO instance", Level.INFO, null);
    	try {
        	entity = entityManager.getReference(AgrupacionEstadoCuentaDTO.class, entity.getIdTcAgrupacionEstadoCuenta());
            entityManager.remove(entity);
            LogUtil.log("delete successful", Level.INFO, null);
    	} catch (RuntimeException re) {
    		LogUtil.log("delete failed", Level.SEVERE, re);
    		throw re;
        }
    }
    
    /**
	 Persist a previously saved AgrupacionEstadoCuentaDTO entity and return it or a copy of it to the sender. 
	 A copy of the AgrupacionEstadoCuentaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AgrupacionEstadoCuentaDTO entity to update
	 @return AgrupacionEstadoCuentaDTO the persisted AgrupacionEstadoCuentaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public AgrupacionEstadoCuentaDTO update(AgrupacionEstadoCuentaDTO entity) {
    	LogUtil.log("updating AgrupacionEstadoCuentaDTO instance", Level.INFO, null);
    	try {
    		AgrupacionEstadoCuentaDTO result = entityManager.merge(entity);
    		LogUtil.log("update successful", Level.INFO, null);
    		return result;
    	} catch (RuntimeException re) {
    		LogUtil.log("update failed", Level.SEVERE, re);
    		throw re;
        }
    }
    
    public AgrupacionEstadoCuentaDTO findById( Integer id) {
    	LogUtil.log("finding AgrupacionEstadoCuentaDTO instance with id: " + id, Level.INFO, null);
    	try {
    		AgrupacionEstadoCuentaDTO instance = entityManager.find(AgrupacionEstadoCuentaDTO.class, id);
            return instance;
    	} catch (RuntimeException re) {
    		LogUtil.log("find failed", Level.SEVERE, re);
    		throw re;
        }
    }
    

/**
	 * Find all AgrupacionEstadoCuentaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AgrupacionEstadoCuentaDTO property to query
	  @param value the property value to match
	  	  @return List<AgrupacionEstadoCuentaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<AgrupacionEstadoCuentaDTO> findByProperty(String propertyName, final Object value) {
    	LogUtil.log("finding AgrupacionEstadoCuentaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
    	try {
    		final String queryString = "select model from AgrupacionEstadoCuentaDTO model where model." 
    			+ propertyName + "= :propertyValue";
    		Query query = entityManager.createQuery(queryString);
    		query.setParameter("propertyValue", value);
    		return query.getResultList();
    	} catch (RuntimeException re) {
    		LogUtil.log("find by property name failed", Level.SEVERE, re);
    		throw re;
    	}
	}			
	
	/**
	 * Find all AgrupacionEstadoCuentaDTO entities.
	  	  @return List<AgrupacionEstadoCuentaDTO> all AgrupacionEstadoCuentaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<AgrupacionEstadoCuentaDTO> findAll() {
		LogUtil.log("finding all AgrupacionEstadoCuentaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from AgrupacionEstadoCuentaDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public List<AgrupacionEstadoCuentaDTO> obtenerAgrupacionesFacultativo(short claveFormatoEstadoCuenta,short idMoneda){
		List<AgrupacionEstadoCuentaDTO> listaAgrupaciones = null;
		
		AgrupacionEstadoCuentaDTO filtroAgrupacion = new AgrupacionEstadoCuentaDTO(null, 
				AgrupacionEstadoCuentaDTO.TIPO_FACULTATIVO,
				claveFormatoEstadoCuenta, idMoneda, null, null, null);
		listaAgrupaciones = listarFiltrado(filtroAgrupacion);
		return listaAgrupaciones;
	}
	
	public List<AgrupacionEstadoCuentaDTO> obtenerAgrupacionesPorEstadoCuenta(EstadoCuentaDTO estadoCuentaDTO,Short claveFormatoEstadoCuenta){
		List<AgrupacionEstadoCuentaDTO> listaAgrupaciones = null;
		
		AgrupacionEstadoCuentaDTO filtroAgrupacion = new AgrupacionEstadoCuentaDTO(null, 
				estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO ? AgrupacionEstadoCuentaDTO.TIPO_FACULTATIVO : AgrupacionEstadoCuentaDTO.TIPO_AUTOMATICO,
				claveFormatoEstadoCuenta, (short)estadoCuentaDTO.getIdMoneda(), null, null, null);
		listaAgrupaciones = listarFiltrado(filtroAgrupacion);
		return listaAgrupaciones;
	}
	
	public List<AgrupacionEstadoCuentaDTO> obtenerAgrupaciones(int tipoReaseguro,int idMoneda,Short claveFormatoEstadoCuenta){
		List<AgrupacionEstadoCuentaDTO> listaAgrupaciones = null;
		
		AgrupacionEstadoCuentaDTO filtroAgrupacion = new AgrupacionEstadoCuentaDTO(null, 
				tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO ? AgrupacionEstadoCuentaDTO.TIPO_FACULTATIVO : AgrupacionEstadoCuentaDTO.TIPO_AUTOMATICO,
				claveFormatoEstadoCuenta, (short)idMoneda, null, null, null);
		listaAgrupaciones = listarFiltrado(filtroAgrupacion);
		return listaAgrupaciones;
	}
	
	@SuppressWarnings("unchecked")
	public List<AgrupacionEstadoCuentaDTO> listarFiltrado(AgrupacionEstadoCuentaDTO agrupacionEstadoCuentaDTO){
		List<AgrupacionEstadoCuentaDTO> listaAgrupaciones = null;
		try {
			String queryString = "select model from AgrupacionEstadoCuentaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (agrupacionEstadoCuentaDTO != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveFormatoEstadoCuenta", agrupacionEstadoCuentaDTO.getClaveFormatoEstadoCuenta());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveMostrarEncabezado", agrupacionEstadoCuentaDTO.getClaveMostrarEncabezado());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveMostrarSubTotal", agrupacionEstadoCuentaDTO.getClaveMostrarSubTotal());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveTipoEstadoCuenta", agrupacionEstadoCuentaDTO.getClaveTipoEstadoCuenta());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idMoneda", agrupacionEstadoCuentaDTO.getIdMoneda());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idTcAgrupacionEstadoCuenta", agrupacionEstadoCuentaDTO.getIdTcAgrupacionEstadoCuenta());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombreAgrupacion", agrupacionEstadoCuentaDTO.getNombreAgrupacion());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "orden", agrupacionEstadoCuentaDTO.getOrden());

				if (Utilerias.esAtributoQueryValido(sWhere))
					queryString = queryString.concat(" where ").concat(sWhere).concat(" order by model.orden");

				query = entityManager.createQuery(queryString);
				Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				listaAgrupaciones = query.getResultList();
			}
			return listaAgrupaciones;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarFiltrado failed", Level.SEVERE, re);
			throw re;
		}
	}
}