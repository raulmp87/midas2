package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import java.math.BigDecimal;
import java.util.Date;

class EstructuraFiltradoAgrupacionMovimientos {
	private BigDecimal idToPoliza;
	private boolean incluirTodasLasPolizas;
	private BigDecimal idToReporteSiniestro;
	private boolean incluirTodosLosSiniestros;
	private BigDecimal idMoneda;
	private BigDecimal idTcReasegurador;
	private BigDecimal idTcCorredor;
	private BigDecimal[] idTcSubRamos;
	private boolean incluirFiltradoHasta;
	private boolean incluirSoloFacultativo;
	private Integer mesHasta;
	private Integer anioHasta;
	private BigDecimal[] idTcConceptos;
	private boolean incluirSoloAutomaticos;
	private Date fechaInicio;
	private Date fechaFin;
	private boolean incluirFiltradoRangoFechas;
	
	
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public boolean isIncluirTodasLasPolizas() {
		return incluirTodasLasPolizas;
	}
	public void setIncluirTodasLasPolizas(boolean incluirTodasLasPolizas) {
		this.incluirTodasLasPolizas = incluirTodasLasPolizas;
	}
	public BigDecimal getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}
	public BigDecimal getIdTcReasegurador() {
		return idTcReasegurador;
	}
	public void setIdTcReasegurador(BigDecimal idTcReasegurador) {
		this.idTcReasegurador = idTcReasegurador;
	}
	public BigDecimal getIdTcCorredor() {
		return idTcCorredor;
	}
	public void setIdTcCorredor(BigDecimal idTcCorredor) {
		this.idTcCorredor = idTcCorredor;
	}
	public BigDecimal[] getIdTcSubRamos() {
		return idTcSubRamos;
	}
	public void setIdTcSubRamos(BigDecimal[] idTcSubRamos) {
		this.idTcSubRamos = idTcSubRamos;
	}
	public boolean isIncluirFiltradoHasta() {
		if(!(getMesHasta() != null && getAnioHasta() != null && 
				getMesHasta()>0 && getMesHasta()<=12 && getAnioHasta()>1900))
			incluirFiltradoHasta = false;
		else incluirFiltradoHasta = true;
		return incluirFiltradoHasta;
	}
	public void setIncluirFiltradoHasta(boolean incluirFiltradoHasta) {
		this.incluirFiltradoHasta = incluirFiltradoHasta;
	}
	public Integer getMesHasta() {
		return mesHasta;
	}
	public void setMesHasta(Integer mesHasta) {
		this.mesHasta = mesHasta;
	}
	public Integer getAnioHasta() {
		return anioHasta;
	}
	public void setAnioHasta(Integer anioHasta) {
		this.anioHasta = anioHasta;
	}
	public boolean isIncluirSoloFacultativo() {
		return incluirSoloFacultativo;
	}
	public void setIncluirSoloFacultativo(boolean incluirSoloFacultativo) {
		this.incluirSoloFacultativo = incluirSoloFacultativo;
	}
	public BigDecimal[] getIdTcConceptos() {
		return idTcConceptos;
	}
	public void setIdTcConceptos(BigDecimal[] idTcConceptos) {
		this.idTcConceptos = idTcConceptos;
	}
	public void setIdTcConceptosPorConsultar(BigDecimal... idTcConceptos) {
		this.idTcConceptos = idTcConceptos;
	}
	public BigDecimal getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	public boolean isIncluirTodosLosSiniestros() {
		return incluirTodosLosSiniestros;
	}
	public void setIncluirTodosLosSiniestros(boolean incluirTodosLosSiniestros) {
		this.incluirTodosLosSiniestros = incluirTodosLosSiniestros;
	}
	public boolean isIncluirSoloAutomaticos() {
		return incluirSoloAutomaticos;
	}
	public void setIncluirSoloAutomaticos(boolean incluirSoloAutomaticos) {
		this.incluirSoloAutomaticos = incluirSoloAutomaticos;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public boolean isIncluirFiltradoRangoFechas() {
		if(getFechaInicio() != null && getFechaFin() != null)
			incluirFiltradoRangoFechas = true;
		else incluirFiltradoRangoFechas = false;
		return incluirFiltradoRangoFechas;
	}
	public void setIncluirFiltradoRangoFechas(boolean incluirFiltradoRangoFechas) {
		this.incluirFiltradoRangoFechas = incluirFiltradoRangoFechas;
	}
	
}
