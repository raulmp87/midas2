package mx.com.afirme.midas.contratos.egreso;
// default package

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoFacadeRemote;
import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.pagocobertura.PagoCoberturaDTO;
import mx.com.afirme.midas.contratofacultativo.pagocobertura.PagoCoberturaReaseguradorDTO;
import mx.com.afirme.midas.contratofacultativo.pagocobertura.PagoCoberturaReaseguradorId;
import mx.com.afirme.midas.contratofacultativo.pagocobertura.PlanPagosCoberturaDTO;
import mx.com.afirme.midas.contratofacultativo.pagocobertura.PlanPagosCoberturaFacadeRemote;
import mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo.ParticipacionCorredorFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionReaseguradorContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDetalleDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroServiciosRemote;
import mx.com.afirme.midas.contratos.movimiento.TipoMovimientoDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDecoradoDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDecoradoDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;


/**
 * Facade for entity EgresoReaseguroDTO.
 * @see .EgresoReaseguroDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class EgresoReaseguroFacade  implements EgresoReaseguroFacadeRemote {

	@EJB
	private MovimientoReaseguroServiciosRemote movimientoReaseguroServicios;
	
	@EJB
	private PlanPagosCoberturaFacadeRemote planPagosCobertura;
	
	/*@EJB
	private ContabilidadFacadeRemote contabilidadFacadeRemote;	*/
	
	@Resource
    private SessionContext context;

    @PersistenceContext private EntityManager entityManager;

	/**
	 Perform an initial save of a previously unsaved EgresoReaseguroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EgresoReaseguroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EgresoReaseguroDTO entity) {
    				LogDeMidasEJB3.log("saving EgresoReaseguroDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent EgresoReaseguroDTO entity.
	  @param entity EgresoReaseguroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EgresoReaseguroDTO entity) {
    				LogDeMidasEJB3.log("deleting EgresoReaseguroDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(EgresoReaseguroDTO.class, entity.getIdEgresoReaseguro());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved EgresoReaseguroDTO entity and return it or a copy of it to the sender. 
	 A copy of the EgresoReaseguroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EgresoReaseguroDTO entity to update
	 @return EgresoReaseguroDTO the persisted EgresoReaseguroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public EgresoReaseguroDTO update(EgresoReaseguroDTO entity) {
    				LogDeMidasEJB3.log("updating EgresoReaseguroDTO instance", Level.INFO, null);
	        try {
            EgresoReaseguroDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public EgresoReaseguroDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding EgresoReaseguroDTO instance with id: " + id, Level.INFO, null);
	        try {
            EgresoReaseguroDTO instance = entityManager.find(EgresoReaseguroDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all EgresoReaseguroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EgresoReaseguroDTO property to query
	  @param value the property value to match
	  	  @return List<EgresoReaseguroDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<EgresoEstadoCuentaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding EgresoReaseguroDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from EgresoReaseguroDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
    
	
	/**
	 * Find all EgresoReaseguroDTO entities.
	  	  @return List<EgresoReaseguroDTO> all EgresoReaseguroDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<EgresoReaseguroDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all EgresoReaseguroDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from EgresoReaseguroDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<EgresoReaseguroDTO> obtenerEgresosPendientes(){
		List<EgresoReaseguroDTO> egresoReaseguroDTOList = new ArrayList<EgresoReaseguroDTO>();
		String estatusPosibles = EgresoReaseguroDTO.ESTATUS_PENDIENTE+","+EgresoReaseguroDTO.ESTATUS_EN_SOLICITUD_MIZAR;
		String queryString = "SELECT model from EgresoReaseguroDTO model " +
							 "WHERE model.estatus IN ("+estatusPosibles+") " +
							 "ORDER BY model.fechaEgreso";
		Query query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		egresoReaseguroDTOList = query.getResultList();
		
		return egresoReaseguroDTOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EgresoReaseguroDTO> obtenerEgresosPendientesCancel(){ 
		List<EgresoReaseguroDTO> egresoReaseguroDTOList = new ArrayList<EgresoReaseguroDTO>();
		String estatusPosiblesCancelacion = EgresoReaseguroDTO.ESTATUS_PENDIENTE+"";
		String queryString = "SELECT model from EgresoReaseguroDTO model " +
				             "WHERE model.estatus IN ("+estatusPosiblesCancelacion+")" +
							 " ORDER BY model.fechaEgreso";

		Query query = entityManager.createQuery(queryString);		
		egresoReaseguroDTOList = query.getResultList();
		
		return egresoReaseguroDTOList;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public EgresoReaseguroDTO agregarEgreso(EgresoReaseguroDTO egresoReaseguro, 
											List<EgresoEstadoCuentaDTO> egresoEstadosCuenta,
											List<PagoCoberturaReaseguradorDTO> exhibiciones) {
		try{																					
			egresoReaseguro.setEstatus(0);
			entityManager.persist(egresoReaseguro);
									
			for (EgresoEstadoCuentaDTO egresoEstadoCuentaDTO : egresoEstadosCuenta) {								
			    egresoEstadoCuentaDTO.setEgresoReaseguro(egresoReaseguro);			    			
			    egresoEstadoCuentaDTO.getId().setIdEgresoReaseguro(egresoReaseguro.getIdEgresoReaseguro());			
			    entityManager.persist(egresoEstadoCuentaDTO);		    
			}
			
			for(PagoCoberturaReaseguradorDTO exhibicion : exhibiciones){
				exhibicion.setEgresoReaseguro(egresoReaseguro);
				exhibicion.setEstatusPago(PagoCoberturaReaseguradorDTO.PROGRAMADO);
				entityManager.merge(exhibicion);
			}
			
			entityManager.flush();
			
			return egresoReaseguro;
		}catch(RuntimeException e){
			context.setRollbackOnly();
			LogDeMidasEJB3.log("Agregar Egreso failed", Level.SEVERE, e);
			throw e;
		}
	}
	/*
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void agregarEgreso2(EgresoReaseguroDTO egresoReaseguro, List<EstadoCuentaDecoradoDTO> lista) {
		try{
			egresoReaseguro.setEstatus(0);
			entityManager.persist(egresoReaseguro);
			
			EgresoEstadoCuentaId egresoEstadoCuentaId;
			EgresoEstadoCuentaDTO egresoEstadoCuentaDTO;
			for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO : lista) {
				egresoEstadoCuentaDTO = new EgresoEstadoCuentaDTO();
				egresoEstadoCuentaDTO.setEstadoCuentaDTO(estadoCuentaDecoradoDTO.getEstadoCuentaDTO());
			    egresoEstadoCuentaDTO.setEgresoReaseguro(egresoReaseguro);
			    egresoEstadoCuentaDTO.setMonto(estadoCuentaDecoradoDTO.getMontoEgreso());
			    egresoEstadoCuentaDTO.setCuentaAfirme(estadoCuentaDecoradoDTO.getCuentaAfirme());
			
			    egresoEstadoCuentaId = new EgresoEstadoCuentaId();
			    egresoEstadoCuentaId.setIdEstadoCuenta(estadoCuentaDecoradoDTO.getIdEstadoCuenta());
			    egresoEstadoCuentaId.setIdEgresoReaseguro(egresoReaseguro.getIdEgresoReaseguro());
			
			    egresoEstadoCuentaDTO.setId(egresoEstadoCuentaId);			
			    entityManager.persist(egresoEstadoCuentaDTO);
		    
			}
		}catch(RuntimeException e){
			context.setRollbackOnly();
			LogDeMidasEJB3.log("Relacionar Egreso failed", Level.SEVERE, e);
			throw e;
		}
	}
	*/
	private void registrarMovimiento(EgresoReaseguroDTO egresoReaseguro, 
									 EgresoEstadoCuentaDTO egresoEstadoCuenta, 
									 EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO)  {
		MovimientoReaseguroDTO movimiento = new MovimientoReaseguroDTO();
		
		if (estadoCuentaDecoradoDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			movimiento.setContratoCuotaParteDTO(estadoCuentaDecoradoDTO.getContratoCuotaParteDTO());
			movimiento.setNumeroEndoso(0); //TODO, Borrar esta linea una vez que el campo en la tabla sea NULLABLE
		}
		
		if (estadoCuentaDecoradoDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
			movimiento.setContratoPrimerExcedenteDTO(estadoCuentaDecoradoDTO.getContratoPrimerExcedenteDTO());
			movimiento.setNumeroEndoso(0); //TODO, Borrar esta linea una vez que el campo en la tabla sea NULLABLE
		}
		
		if(estadoCuentaDecoradoDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
			movimiento.setContratoFacultativoDTO(estadoCuentaDecoradoDTO.getContratoFacultativoDTO());
			movimiento.setIdPoliza(estadoCuentaDecoradoDTO.getIdPoliza());	
			movimiento.setNumeroEndoso(estadoCuentaDecoradoDTO.getNumeroEndoso());
		}
		
		movimiento.setLineaDTO(estadoCuentaDecoradoDTO.getLineaDTO());
		movimiento.setEjercicio(estadoCuentaDecoradoDTO.getEjercicio());
		if (egresoReaseguro.getFechaEgreso() != null) {
			movimiento.setFechaRegistro(egresoReaseguro.getFechaEgreso());
		} else {
			movimiento.setFechaRegistro(new Date());
		}
		movimiento.setIdMoneda(estadoCuentaDecoradoDTO.getIdMoneda());
		movimiento.setReaseguradorCorredor(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO());
		movimiento.setSubRamo(estadoCuentaDecoradoDTO.getSubRamoDTO());
		movimiento.setTipoMovimiento(TipoMovimientoDTO.PAGO);
		movimiento.setTipoReaseguro(estadoCuentaDecoradoDTO.getTipoReaseguro());
		movimiento.setAcumulado(1);
		movimiento.setCantidad(estadoCuentaDecoradoDTO.getMontoEgreso());
		movimiento.setConceptoMovimiento(ConceptoMovimientoDetalleDTO.CARGO_DE_REMESAS_SALDOS);  // Cargos de Remesas saldos
		movimiento.setComodin(egresoEstadoCuenta.getId().getIdEgresoReaseguro() + "-" + egresoEstadoCuenta.getId().getIdEstadoCuenta());
		//movimiento.setUsuario(userName);
		
		movimientoReaseguroServicios.registrarMovimiento(movimiento, estadoCuentaDecoradoDTO.getEstadoCuentaDTO());
		
		if(egresoEstadoCuenta.getImpuesto()!=null && egresoEstadoCuenta.getImpuesto().doubleValue() != 0){
			movimiento.setIdMovimiento(null);
			movimiento.setTipoMovimiento(TipoMovimientoDTO.COBRO);
			movimiento.setConceptoMovimiento(ConceptoMovimientoDetalleDTO.RETENCION_IMPUESTOS);
			movimiento.setCantidad(egresoEstadoCuenta.getImpuesto());
			movimientoReaseguroServicios.registrarMovimiento(movimiento, estadoCuentaDecoradoDTO.getEstadoCuentaDTO());
		}
		
		/*
		 * Aqui va registrar movimiento
		try{
			BigDecimal idCheque = contabilidadFacadeRemote.solicitarChequeReaseguro(
												egresoEstadoCuenta, movimiento, movimiento.getUsuario());
			egresoEstadoCuenta.setIdCheque(idCheque);	
			
			
		}catch (Exception exc){
			throw new RuntimeException("Error al intentar registrar pago en Contabilidad", exc);
		}	
		*/	
		
	}
	
	private void registrarMovimientoCancel(EgresoReaseguroDTO egresoReaseguro, 
			 							   EgresoEstadoCuentaDTO egresoEstadoCuenta, 
			 							   EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO)  {
		
			MovimientoReaseguroDTO movimiento = new MovimientoReaseguroDTO();
			
			if (estadoCuentaDecoradoDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
				movimiento.setContratoCuotaParteDTO(estadoCuentaDecoradoDTO.getContratoCuotaParteDTO());
				movimiento.setNumeroEndoso(0); //TODO, Borrar esta linea una vez que el campo en la tabla sea NULLABLE
			}
			
			if (estadoCuentaDecoradoDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
				movimiento.setContratoPrimerExcedenteDTO(estadoCuentaDecoradoDTO.getContratoPrimerExcedenteDTO());
				movimiento.setNumeroEndoso(0); //TODO, Borrar esta linea una vez que el campo en la tabla sea NULLABLE
			}
			
			if(estadoCuentaDecoradoDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
				movimiento.setContratoFacultativoDTO(estadoCuentaDecoradoDTO.getContratoFacultativoDTO());
				movimiento.setIdPoliza(estadoCuentaDecoradoDTO.getIdPoliza());	
				movimiento.setNumeroEndoso(estadoCuentaDecoradoDTO.getNumeroEndoso());
			}
			
			movimiento.setLineaDTO(estadoCuentaDecoradoDTO.getLineaDTO());
			movimiento.setEjercicio(estadoCuentaDecoradoDTO.getEjercicio());
			if (egresoReaseguro.getFechaEgreso() != null) {
				movimiento.setFechaRegistro(egresoReaseguro.getFechaEgreso());
			} else {
				movimiento.setFechaRegistro(new Date());
			}
				movimiento.setIdMoneda(estadoCuentaDecoradoDTO.getIdMoneda());
				movimiento.setReaseguradorCorredor(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO());
				movimiento.setSubRamo(estadoCuentaDecoradoDTO.getSubRamoDTO());
				movimiento.setTipoMovimiento(TipoMovimientoDTO.COBRO);
				movimiento.setTipoReaseguro(estadoCuentaDecoradoDTO.getTipoReaseguro());
				movimiento.setAcumulado(1);
				movimiento.setCantidad(estadoCuentaDecoradoDTO.getMontoEgreso());
				movimiento.setConceptoMovimiento(ConceptoMovimientoDetalleDTO.CARGO_DE_REMESAS_SALDOS);  // Cargos de Remesas saldos
				movimiento.setComodin(egresoEstadoCuenta.getId().getIdEgresoReaseguro() + "-" + egresoEstadoCuenta.getId().getIdEstadoCuenta());
				//movimiento.setUsuario(userName);
			
				movimientoReaseguroServicios.registrarMovimiento(movimiento, estadoCuentaDecoradoDTO.getEstadoCuentaDTO());
				
				if(egresoEstadoCuenta.getImpuesto()!=null && egresoEstadoCuenta.getImpuesto().doubleValue() != 0){
					movimiento.setIdMovimiento(null);
					movimiento.setTipoMovimiento(TipoMovimientoDTO.PAGO);
					movimiento.setConceptoMovimiento(ConceptoMovimientoDetalleDTO.CANCELACION_RETENCION_IMPUESTOS);
					movimiento.setCantidad(egresoEstadoCuenta.getImpuesto());
					movimientoReaseguroServicios.registrarMovimiento(movimiento, estadoCuentaDecoradoDTO.getEstadoCuentaDTO());
				}
			
	}
	

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void confirmarPago(EgresoReaseguroDTO egreso) {		
		/*
		 * TODO 1.- Registrar a Contabilidad para confirmar el Pago del Egreso
		 * Pendiente por Definir Interface con Contabilidad.
		 */
		try{
			//egreso.setEstatus(1);
			//entityManager.merge(egreso);
			String queryString = "SELECT model FROM EgresoEstadoCuentaDTO model " +
								 "WHERE model.id.idEgresoReaseguro = :idEgresoReaseguro"; 						
			
			Query query = entityManager.createQuery(queryString);			
			query.setParameter("idEgresoReaseguro", egreso.getIdEgresoReaseguro());
			
			List<EgresoEstadoCuentaDTO> egresoEstadoCuentaList = query.getResultList();
			EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO;
			if (egresoEstadoCuentaList != null){
				for (EgresoEstadoCuentaDTO egresoEstadoCuentaDTO : egresoEstadoCuentaList) {
					estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(egresoEstadoCuentaDTO.getEstadoCuentaDTO());
					estadoCuentaDecoradoDTO.setMontoEgreso(egresoEstadoCuentaDTO.getMonto());
					
					registrarMovimiento(egreso, egresoEstadoCuentaDTO, estadoCuentaDecoradoDTO);
				}
			}
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Confirmar Pago failed", Level.SEVERE, e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<EgresoEstadoCuentaDTO> obtenerEstadosCuentaEgreso(BigDecimal idEgresoReaseguro) {
		
		String queryString = "SELECT model FROM EgresoEstadoCuentaDTO model WHERE model.id.idEgresoReaseguro = :idEgresoReaseguro";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idEgresoReaseguro", idEgresoReaseguro);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query.getResultList();
	}

	@SuppressWarnings({ "unchecked" })
	public List<ParticipacionDecoradoDTO> obtenerParticipaciones(EstadoCuentaDecoradoDTO estadoCuenta){
		List<ParticipacionDTO> listaParticipaciones = null;
		List<ParticipacionCorredorDTO> listaParticipacionesCorredor = null;
		List<ParticipacionFacultativoDTO> listaParticipacionesFacultativo = null;
		List<ParticipacionDecoradoDTO> lista = new ArrayList<ParticipacionDecoradoDTO>();
		Query query;
		String queryString = "";
		int tipoReaseguro = estadoCuenta.getTipoReaseguro();
		
		if(tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE || tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
			boolean estadoCuentaCorredor = estadoCuenta.getCorredorDTO() != null && estadoCuenta.getCorredorDTO().getIdtcreaseguradorcorredor() != null;
			
			if(estadoCuentaCorredor){
				if(tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
					queryString = "select model from ParticipacionCorredorDTO model where " +
						"model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtccorredor and " +
						"model.participacion.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreasegurador and " +
						"model.participacion.contratoCuotaParte.idTmContratoCuotaParte = :idTmContratoCuotaParte ";
				}
				else if(tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
					queryString = "select model from ParticipacionCorredorDTO model where " +
						"model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtccorredor and " +
						"model.participacion.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreasegurador and " +
						"model.participacion.contratoPrimerExcedente.idTmContratoPrimerExcedente = :idTmContratoPrimerExcedente ";
				}
			}
			else if(estadoCuenta.getReaseguradorCorredorDTO() != null && estadoCuenta.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor() != null){
				if(tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
					queryString = "select model from ParticipacionDTO model where " +
						"model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreasegurador and " +
						"model.contratoCuotaParte.idTmContratoCuotaParte = :idTmContratoCuotaParte ";
				}
				else if(tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
					queryString = "select model from ParticipacionDTO model where " +
						"model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreasegurador and " +
						"model.contratoPrimerExcedente.idTmContratoPrimerExcedente = :idTmContratoPrimerExcedente ";
				}
			}

			query = entityManager.createQuery(queryString);
			
			if (queryString.contains(":idTmContratoCuotaParte")) {
				query.setParameter("idTmContratoCuotaParte", estadoCuenta.getContratoCuotaParteDTO().getIdTmContratoCuotaParte());
			}
			if (queryString.contains(":idTmContratoPrimerExcedente")) {
				query.setParameter("idTmContratoPrimerExcedente", estadoCuenta.getContratoPrimerExcedenteDTO().getIdTmContratoPrimerExcedente());
			}
			if (queryString.contains(":idtccorredor")) {
				query.setParameter("idtccorredor", estadoCuenta.getCorredorDTO().getIdtcreaseguradorcorredor());
			}
			if (queryString.contains(":idtcreasegurador")) {
				query.setParameter("idtcreasegurador", estadoCuenta.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
			}

			if(estadoCuentaCorredor){
				listaParticipacionesCorredor = query.getResultList();
				for (ParticipacionCorredorDTO participacionCorredorDTO : listaParticipacionesCorredor){
					ParticipacionDecoradoDTO participacionDecoradoDTO = new ParticipacionDecoradoDTO(participacionCorredorDTO);
					participacionDecoradoDTO.setTipoReaseguro(tipoReaseguro);
					lista.add(participacionDecoradoDTO);
				}
			}
			else{
				listaParticipaciones = query.getResultList();
				for (ParticipacionDTO participacionDTO : listaParticipaciones){
					ParticipacionDecoradoDTO participacionDecoradoDTO = new ParticipacionDecoradoDTO(participacionDTO);
					participacionDecoradoDTO.setTipoReaseguro(tipoReaseguro);
					lista.add(participacionDecoradoDTO);
				}
			}
		}
		else if (estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
			queryString = "select dcf.sumaasegurada, sum(primatotalcobertura), sum(primafacultadacobertura), " +
					  "comision, pf.PORCENTAJEPARTICIPACION, " +
					  "pf.tipo, pf.idtdreaseguradorcorredor, sopor.porcentajefacultativo " +
					  "from MIDAS.tdcontratoFacultativo dcf " +
					  "inner join MIDAS.TMLINEASOPORTEREASEGURO sopor on sopor.IDTMCONTRATOFACULTATIVO = dcf.IDTMCONTRATOFACULTATIVO " +
					  "inner join MIDAS.tdParticipacionFacultativo pf " +
					  "    on dcf.IDTDCONTRATOFACULTATIVO = pf.IDTDCONTRATOFACULTATIVO " +
					  " left join  MIDAS.tdParticipacionCorredorFac pcf on pcf.IDTDPARTICIPACIONFACULTATIVO = pf.IDTDPARTICIPACIONFACULTATIVO " +
					  "INNER JOIN MIDAS.TCREASEGURADORCORREDOR rac " +
					  "    ON PF.IDTDREASEGURADORCORREDOR = rac.idtcreaseguradorcorredor " +
					  "where dcf.IDTMCONTRATOFACULTATIVO=" + estadoCuenta.getContratoFacultativoDTO().getIdTmContratoFacultativo() +
					  " and dcf.idtcsubramo=" + estadoCuenta.getSubRamoDTO().getIdTcSubRamo() ;
			if (estadoCuenta.getReaseguradorCorredorDTO()!=null){
				queryString = queryString + " AND (pf.IDTDREASEGURADORCORREDOR = " + estadoCuenta.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor();
				if (estadoCuenta.getReaseguradorCorredorDTO()!=null){
					queryString = queryString + " OR pcf.IDTCREASEGURADORCORREDOR = " + estadoCuenta.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor() + ") ";
				} else{
					queryString = queryString + " ) ";
				}
			}
			queryString = queryString + " group by sumaasegurada, comision, pf.PORCENTAJEPARTICIPACION, " +
								" pf.tipo, pf.idtdreaseguradorcorredor, sopor.porcentajefacultativo ";
			
			query = entityManager.createNativeQuery(queryString);
			
			if (estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
				listaParticipacionesFacultativo =  query.getResultList();
			}
			
			for(Object resultadoFacultativo : listaParticipacionesFacultativo){
				BigDecimal primaNeta = new BigDecimal("0.0");
				BigDecimal primaReaseguro = new BigDecimal("0.0");
				BigDecimal primaReaseguroTotal = new BigDecimal("0.0");
				BigDecimal porcentajeParticipacion = new BigDecimal("0.0");
				BigDecimal porcentajeComision = new BigDecimal("0.0");
				BigDecimal comision = new BigDecimal("0.0");
				BigDecimal primaNetaReaseguro = new BigDecimal("0.0");
				BigDecimal participacion = new BigDecimal("0.0");
				BigDecimal sumaAsegurada = new BigDecimal("0.0");
				BigDecimal tipo = new BigDecimal("0.0");
				BigDecimal facultativoDTO = null;
				BigDecimal idreaseguradorcorredor = new BigDecimal("0.0");
				BigDecimal porcentajefacultativo = null;
				
				primaNeta = (BigDecimal) ((Object[])resultadoFacultativo)[1];
				primaReaseguro = (BigDecimal) ((Object[])resultadoFacultativo)[2];
				porcentajefacultativo = (BigDecimal)((Object[])resultadoFacultativo)[7];
				primaReaseguroTotal=primaReaseguro.multiply(porcentajefacultativo.divide(BigDecimal.valueOf(100.0)));
					
				porcentajeParticipacion = (BigDecimal) ((Object[])resultadoFacultativo)[4];
				participacion = (primaReaseguroTotal.multiply(porcentajeParticipacion.divide(BigDecimal.valueOf(100.0)))); //(porcentajeParticipacion * primaReaseguro); //100;
				porcentajeComision = (BigDecimal) ((Object[])resultadoFacultativo)[3];
				
				comision = (participacion.multiply(porcentajeComision.divide(BigDecimal.valueOf(100.0))));
					
				primaNetaReaseguro = participacion.subtract(comision);
				sumaAsegurada = (BigDecimal) ((Object[])resultadoFacultativo)[0];		
				tipo =(BigDecimal)((Object[])resultadoFacultativo)[5];	
				facultativoDTO = (BigDecimal)((Object[])resultadoFacultativo)[6];
				idreaseguradorcorredor = (BigDecimal)((Object[])resultadoFacultativo)[6];
					
				ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
				reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(idreaseguradorcorredor);
					
				ParticipacionFacultativoDTO participacionFacultativoDTO2 = new ParticipacionFacultativoDTO();
				participacionFacultativoDTO2.setParticipacionFacultativoDTO(facultativoDTO);
				participacionFacultativoDTO2.setTipo(tipo.toString());
				participacionFacultativoDTO2.setReaseguradorCorredorDTO(reaseguradorCorredorDTO);
				
				participacionFacultativoDTO2.setPorcentajeParticipacion(porcentajeParticipacion);
									
				ParticipacionDecoradoDTO participacionDecoradoDTO = new ParticipacionDecoradoDTO(participacionFacultativoDTO2);
											
				participacionDecoradoDTO.setPrimaNeta(primaNeta);
				participacionDecoradoDTO.setPrimaReaseguro(primaReaseguroTotal);
				participacionDecoradoDTO.setPorcentajeParticipacion(porcentajeParticipacion);
				participacionDecoradoDTO.setParticipacion(participacion);
				participacionDecoradoDTO.setPorcentajeComision(porcentajeComision);
				participacionDecoradoDTO.setComision(comision);
				participacionDecoradoDTO.setPrimaNetaReaseguro(primaNetaReaseguro);	
				participacionDecoradoDTO.setTipoReaseguro(tipoReaseguro);
				participacionDecoradoDTO.setSumaAsegurada(sumaAsegurada);										
				
				lista.add(participacionDecoradoDTO);				
				
			}//Fin iteracion listaParticipacionesFacultativo
		}//fin tiporeaseguro == facultativo
		return lista;
	}
	
	@EJB
	ContratoFacultativoFacadeRemote contratoFacultativoFacade;
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ParticipacionReaseguradorContratoFacultativoDTO obtenerParticipacionesEgresoContratoFacultativo(EstadoCuentaDecoradoDTO estadoCuenta){
		if(estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO) {
			ContratoFacultativoDTO contratoFacultativo = estadoCuenta.getContratoFacultativoDTO();
			contratoFacultativo = entityManager.find(ContratoFacultativoDTO.class, contratoFacultativo.getIdTmContratoFacultativo());
			
			//Consultar el % facultativo y el numeroendoso del soporte, se usa query para no saturar la memoria
			String queryString = "select linsr.disPrimaPorcentajeFacultativo,linsr.soporteReaseguroDTO.numeroEndoso from LineaSoporteReaseguroDTO linsr " +
					"where linsr.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTmContratoFacultativo", contratoFacultativo.getIdTmContratoFacultativo());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List listaResultado = query.getResultList();
			
			BigDecimal porcentajeFacultativoParticipacion = BigDecimal.ZERO;
			Integer numeroEndosoParticipacion = 0;
			
			if(listaResultado != null && !listaResultado.isEmpty()){
				Object[] arrayResultado = (Object[]) listaResultado.get(0);
				porcentajeFacultativoParticipacion = Utilerias.obtenerBigDecimal(arrayResultado[0]);
				numeroEndosoParticipacion = Utilerias.obtenerInteger(arrayResultado[1]);
			} else{
				throw new RuntimeException("No se encontr� LineaSoporteReaseguro para el contrato facultativo: "+contratoFacultativo.getIdTmContratoFacultativo());
			}
			BigDecimal montoPrimaCobertura = null;
			BigDecimal montoPrimaTotalPoliza = BigDecimal.ZERO;
			BigDecimal montoPrimaParticipacionContrato = BigDecimal.ZERO;
			BigDecimal montoPrimaComisionContrato = BigDecimal.ZERO;
			BigDecimal montoPrimaImpuestoContrato = BigDecimal.ZERO;
			final BigDecimal cien = new BigDecimal("100");
			String nombreReasegurador = StringUtils.EMPTY;
			//por cada detalle del contrato facultativo
			for(DetalleContratoFacultativoDTO detalleContratoFac : contratoFacultativo.getDetalleContratoFacultativoDTOs()){
				montoPrimaCobertura = detalleContratoFac.getPrimaFacultadaCobertura().subtract(
						detalleContratoFac.getMontoPrimaNoDevengada() != null ? detalleContratoFac.getMontoPrimaNoDevengada() : BigDecimal.ZERO);
				
				montoPrimaTotalPoliza = montoPrimaTotalPoliza.add( montoPrimaCobertura );
				
				BigDecimal participacionFacCobertura = null;
				BigDecimal comisionFacCobertura = null;
				
				if(estadoCuenta.getCorredorDTO() == null){//Solo es reasegurador, buscar participaciones del reasegurador
					List<ParticipacionFacultativoDTO> listaParticipacionesFacultativo = contratoFacultativoFacade.findParticipacionFacultativo(
							detalleContratoFac.getContratoFacultativoDTO(), estadoCuenta.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
					if(listaParticipacionesFacultativo != null && !listaParticipacionesFacultativo.isEmpty()){
						for(ParticipacionFacultativoDTO participacionReasegurador : listaParticipacionesFacultativo){
							//por cada participacion del reasegurador en la cobertura, se calcula la prima facultada a su participacion, comision y prima menos comision 
							participacionFacCobertura = montoPrimaCobertura.multiply(porcentajeFacultativoParticipacion).divide(cien).
										multiply(participacionReasegurador.getPorcentajeParticipacion()).divide(cien);
							montoPrimaParticipacionContrato = montoPrimaParticipacionContrato.add(participacionFacCobertura);
							
							comisionFacCobertura = participacionFacCobertura.multiply(participacionReasegurador.getComision()).divide(cien);
							montoPrimaComisionContrato = montoPrimaComisionContrato.add(comisionFacCobertura);
						}
					}
					nombreReasegurador = estadoCuenta.getReaseguradorCorredorDTO().getNombre();
				}
				else{//Es reasegurador con corredor
					//Consulta de las participaciones del corredor, se env�a al servicio el ID del corredor en vez del reasegurador
					List<ParticipacionFacultativoDTO> listaParticipacionesCorredorFacultativo = contratoFacultativoFacade.findParticipacionFacultativo(
							detalleContratoFac.getContratoFacultativoDTO(), estadoCuenta.getCorredorDTO().getIdtcreaseguradorcorredor());
					if(listaParticipacionesCorredorFacultativo != null && !listaParticipacionesCorredorFacultativo.isEmpty()){
						
						for(ParticipacionFacultativoDTO participacionCorredor : listaParticipacionesCorredorFacultativo){
							BigDecimal porcentajeParticipacionReasCor = BigDecimal.ZERO;
							for(ParticipacionCorredorFacultativoDTO participacionReasegurador : participacionCorredor.getParticipacionCorredorFacultativoDTOs()){
								if(participacionReasegurador.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().compareTo(
										estadoCuenta.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor()) == 0){
									porcentajeParticipacionReasCor = porcentajeParticipacionReasCor.add(participacionReasegurador.getPorcentajeParticipacion());
									nombreReasegurador = participacionReasegurador.getReaseguradorCorredorDTO().getNombre();
								}
							}
							//El porcentaje ya est� desglozado, no es necesario hacer la multiplicaci�n:
//							porcentajeParticipacionReasCor = porcentajeParticipacionReasCor.multiply(participacionCorredor.getPorcentajeParticipacion()).divide(cien);
							
							//por cada participacion del reasegurador en la cobertura, se calcula la prima facultada a su participacion, comision y prima menos comision 
							participacionFacCobertura = montoPrimaCobertura.multiply(porcentajeFacultativoParticipacion).divide(cien).
										multiply(porcentajeParticipacionReasCor).divide(cien);
							montoPrimaParticipacionContrato = montoPrimaParticipacionContrato.add(participacionFacCobertura);
							
							comisionFacCobertura = participacionFacCobertura.multiply(participacionCorredor.getComision()).divide(cien);
							montoPrimaComisionContrato = montoPrimaComisionContrato.add(comisionFacCobertura);
						}
					}
				}
				//se procede a consultar el monto total y % de impuestos
				queryString = "select model from PagoCoberturaReaseguradorDTO model where " +
						"model.pagoCobertura.planPagosCobertura.detalleContratoFacultativoDTO.contratoFacultativoDTO = :idDetalleContrato";
				query = entityManager.createQuery(queryString);
				query.setParameter("idDetalleContrato", detalleContratoFac.getContratoFacultativoDTO());
				List<PagoCoberturaReaseguradorDTO> listaPagosPorCobertura = query.getResultList();
				
				if(listaPagosPorCobertura != null && !listaPagosPorCobertura.isEmpty()){
					for(PagoCoberturaReaseguradorDTO pagoCoberturaTMP : listaPagosPorCobertura){
						if(pagoCoberturaTMP.getRetenerImpuesto()){
							montoPrimaImpuestoContrato = montoPrimaImpuestoContrato.add(new BigDecimal(pagoCoberturaTMP.getMontoImpuesto()));
						}
					}
				}
			}//Fin iteraci�n de coberturas
			//se cuenta con el total de prima del reasegurador y el total de comisiones, se calculan los porcentajes globales
			BigDecimal porcentajeComision = BigDecimal.ZERO;
			BigDecimal porcentajeImpuestoParticipacion = BigDecimal.ZERO;
			if(montoPrimaParticipacionContrato.compareTo(BigDecimal.ZERO) != 0){
				porcentajeComision = montoPrimaComisionContrato.multiply(cien).divide(montoPrimaParticipacionContrato,new java.math.MathContext(6,RoundingMode.HALF_UP));
				porcentajeImpuestoParticipacion = montoPrimaImpuestoContrato.multiply(cien).divide(montoPrimaParticipacionContrato,new java.math.MathContext(6,RoundingMode.HALF_UP));
			}
			final BigDecimal montoPrimaFacultada = montoPrimaTotalPoliza.multiply(porcentajeFacultativoParticipacion).divide(cien);
			
			final ParticipacionReaseguradorContratoFacultativoDTO participacionPrimaReaseguradorFac = new ParticipacionReaseguradorContratoFacultativoDTO();
			final BigDecimal sumaAseguradaParticipacion = contratoFacultativo.getSumaAseguradaTotal();
			participacionPrimaReaseguradorFac.setSumaAseguradaTotal(sumaAseguradaParticipacion);
			participacionPrimaReaseguradorFac.setPorcentajeFacultativo(porcentajeFacultativoParticipacion.setScale(2,RoundingMode.HALF_UP));
			participacionPrimaReaseguradorFac.setSumaAseguradaFacultada(sumaAseguradaParticipacion.multiply(porcentajeFacultativoParticipacion).divide(cien).setScale(2,RoundingMode.HALF_UP));
			participacionPrimaReaseguradorFac.setSubRamoDTO(estadoCuenta.getSubRamoDTO());
			participacionPrimaReaseguradorFac.setNumeroEndoso(numeroEndosoParticipacion);
			participacionPrimaReaseguradorFac.setPrimaTotalPoliza(montoPrimaTotalPoliza);
			participacionPrimaReaseguradorFac.setPrimaFacultada(montoPrimaFacultada);

			final BigDecimal porcentajeParticipacionReasegurador = montoPrimaParticipacionContrato.multiply(cien).divide(
					participacionPrimaReaseguradorFac.getPrimaFacultada(),
					new java.math.MathContext(6, RoundingMode.HALF_UP)).setScale(2, RoundingMode.HALF_UP);

			participacionPrimaReaseguradorFac.setPrimaParticipacionFacultativo(montoPrimaParticipacionContrato);
			participacionPrimaReaseguradorFac.setPorcentajeParticipacionFacultativo(porcentajeParticipacionReasegurador);
			participacionPrimaReaseguradorFac.setPrimaRetenidaImpuestos(montoPrimaImpuestoContrato);
			participacionPrimaReaseguradorFac.setPorcentajeRetencionImpuestos(porcentajeImpuestoParticipacion.setScale(2,RoundingMode.HALF_UP));
			participacionPrimaReaseguradorFac.setPrimaComision(montoPrimaComisionContrato);
			participacionPrimaReaseguradorFac.setPorcentajeComision(porcentajeComision);
			participacionPrimaReaseguradorFac.setPrimaNetaReasegurador(montoPrimaParticipacionContrato.subtract(montoPrimaComisionContrato).subtract(montoPrimaImpuestoContrato));
			participacionPrimaReaseguradorFac.setNombreReasegurador(nombreReasegurador);
			
			return participacionPrimaReaseguradorFac;
		} else{
			throw new RuntimeException("Se recibi� un estado de cuenta que no corresponde a un contrato facultativo");
		}
		
	}
	/**
	 * {@link Deprecated} Se deja solo como referencia a lo que se estaba haciendo antes
	 */
	@Deprecated
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerConfiguracionPagosEgresoContratoFacultativoOld(BigDecimal idToEgresoReaseguro){
		
		List<ParticipacionReaseguradorContratoFacultativoDTO> listaParticipaciones = new ArrayList<ParticipacionReaseguradorContratoFacultativoDTO>();
		
		String queryString = null;
		Query query = null;
		EstadoCuentaDTO estadoCuenta = null;
		Map<BigDecimal, PolizaDTO> mapaPolizas = new HashMap<BigDecimal, PolizaDTO>();
		Map<BigDecimal,String> mapaDescripcionSecciones = new HashMap<BigDecimal, String>();
		Map<BigDecimal,String> mapaDescripcionCoberturas = new HashMap<BigDecimal, String>();
		
//		PolizaDTO polizaDTO = null;
		
		EgresoReaseguroDTO egresoReaseguro = entityManager.find(EgresoReaseguroDTO.class, idToEgresoReaseguro);
		if(egresoReaseguro == null) {
			throw new RuntimeException("No se encontr� el egresoReaseguro: "+idToEgresoReaseguro);
		}
		
		for (EgresoEstadoCuentaDTO egresoEstadoCuentaTMP : egresoReaseguro.getEgresoEstadoCuentas()) {
			estadoCuenta = egresoEstadoCuentaTMP.getEstadoCuentaDTO();
			if(estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
//				polizaDTO = obtenerPoliza(estadoCuenta.getIdPoliza(), mapaPolizas);
				ContratoFacultativoDTO contratoFacultativo = estadoCuenta.getContratoFacultativoDTO();
				for(DetalleContratoFacultativoDTO detalleContratoFacultativo : contratoFacultativo.getDetalleContratoFacultativoDTOs()){
					//Consulta del % de participacion del reasegurador
					BigDecimal porcentajeParticipacionCobertura = BigDecimal.ZERO; 
					for(ParticipacionFacultativoDTO participacionFac : detalleContratoFacultativo.getParticipacionFacultativoDTOs()){
						if(estadoCuenta.getCorredorDTO() == null){
							if(participacionFac.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().compareTo(
									estadoCuenta.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor()) == 0){
								porcentajeParticipacionCobertura = participacionFac.getPorcentajeParticipacion();
								break;
							}
						}
						else if(participacionFac.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().compareTo(
								estadoCuenta.getCorredorDTO().getIdtcreaseguradorcorredor()) == 0){
							List<ParticipacionCorredorFacultativoDTO> listaPartCorredor = participacionFac.getParticipacionCorredorFacultativoDTOs();
							if(listaPartCorredor != null){
								for(ParticipacionCorredorFacultativoDTO participacionCorredor : listaPartCorredor){
									if(participacionCorredor.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().compareTo(
											estadoCuenta.getCorredorDTO().getIdtcreaseguradorcorredor()) == 0){
										porcentajeParticipacionCobertura = participacionFac.getPorcentajeParticipacion();
										break;
									}
								}
							}
						}
					}
					//Consulta del plan de pagos
					queryString = "select model from PlanPagosCoberturaDTO model where " +
							"model.detalleContratoFacultativoDTO.contratoFacultativoDTO = :idTdDetalleContrato";
					query = entityManager.createQuery(queryString);
					query.setParameter("idTdDetalleContrato", detalleContratoFacultativo.getContratoFacultativoDTO());
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					List<PlanPagosCoberturaDTO> listaPlanPagos = query.getResultList();
					if(listaPlanPagos == null) {
						listaPlanPagos = new ArrayList<PlanPagosCoberturaDTO>();
					}
					for(PlanPagosCoberturaDTO planPagos : listaPlanPagos){
						for(PagoCoberturaDTO pagoCobertura : planPagos.getPagoCoberturaDTOs()){
							//no. exhibicion, fecha pago
							for(PagoCoberturaReaseguradorDTO pagoCoberturaReas : pagoCobertura.getPagoCoberturaReaseguradorDTOs()){
								ParticipacionReaseguradorContratoFacultativoDTO participacion = new ParticipacionReaseguradorContratoFacultativoDTO();
								participacion = generarParticipacionContratoFacultativoDTO(pagoCoberturaReas, pagoCobertura, estadoCuenta, detalleContratoFacultativo, 
										porcentajeParticipacionCobertura, mapaPolizas, mapaDescripcionSecciones, mapaDescripcionCoberturas,null);
								if(participacion != null) {
									listaParticipaciones.add(participacion);
								}
							}
						}
					}
				}
			}
		}
		return listaParticipaciones;
	}
	
	@SuppressWarnings("unchecked")
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerConfiguracionPagosEgresoContratoFacultativo(BigDecimal idToEgresoReaseguro){
		Validate.notNull(idToEgresoReaseguro);
		final StringBuilder sb = new StringBuilder(2500);
		sb.append(" SELECT planpagos.idtoplanpagoscobertura, trim(ex.IDTOPLANPAGOSCOBERTURA || ex.NUMEROEXHIBICION || ex.IDREASEGURADOR) as id, dtcontrato.numeroinciso, nvl(dtcontrato.numerosubinciso,0) AS numeroSubInciso,seccion.nombrecomercialseccion AS descripcionSeccion, ")
		.append(" cobertura.nombrecomercialcobertura AS descripcionCobertura, ex.montopagoprimaneta, NVL(ex.montocomision, 0) AS primaComision, ex.porcentajecomision, ")
		.append(" CASE WHEN ex.retenerImpuesto = 0 THEN 0 ELSE NVL(ex.porcentajeImpuesto,0) END  AS porcentajeRetencionImpuestos, ")
		.append(" CASE WHEN ex.retenerImpuesto = 0 THEN 0 ELSE NVL(ex.montoImpuesto,0) END  AS primaRetenidaImpuestos, ")
		.append(" pagocobertura.numeroExhibicion, pagocobertura.fechaPago, NVL(ex.montopagototal, 0) AS primaNetaReasegurador, ")
		.append(" ex.porcentajeparticipacion as porcentajeParticipacion, NVL(ec.numeroendoso, 0) AS numeroEndoso, ")
		.append(" trim(pol.codigoproducto) || trim(pol.CODIGOTIPOPOLIZA) || '-' || lpad(pol.numeropoliza,6,0) || '-' || lpad(pol.numerorenovacion,2,0) AS numeroPoliza, ")
		.append(" reasegurador.nombre AS nombreReasegurador, corredor.nombre AS nombreCorredor, ")
		.append(" decode(ex.estatuspago, 0, 'Pendiente', 1, 'Programado', 2, 'Pagado', 'Desconocido') as estatus FROM midas.tdpagocoberturareasegurador ex ")
		.append(" INNER JOIN MIDAS.TMPAGOCOBERTURA pagoCobertura ON (pagocobertura.idtoplanpagoscobertura = ex.idtoplanpagoscobertura AND ex.numeroexhibicion = pagoCobertura.numeroexhibicion) ")
		.append(" INNER JOIN MIDAS.TOPLANPAGOSCOBERTURA planpagos ON (ex.idtoplanpagoscobertura = planpagos.idtoplanpagoscobertura) ")
		.append(" INNER JOIN MIDAS.tdcontratofacultativo dtContrato ON (planpagos.idtdcontratofacultativo = dtcontrato.idtdcontratofacultativo) ")
		.append(" INNER JOIN MIDAS.TOCOBERTURA cobertura ON (dtcontrato.idtocobertura = cobertura.idtocobertura) ")
		.append(" INNER JOIN MIDAS.TMCONTRATOFACULTATIVO contrato ON (dtcontrato.idtmcontratofacultativo = contrato.idtmcontratofacultativo)  ")
		.append(" INNER JOIN MIDAS.TOESTADOCUENTA ec ON (ec.idtmcontratofacultativo = contrato.idtmcontratofacultativo AND NVL(ec.IDTCCORREDOR,0) = NVL(ex.IDCORREDOR,0) AND ec.idtcreasegurador = ex.IDREASEGURADOR) ")
		.append(" INNER JOIN MIDAS.TOPOLIZA POL ON (pol.idtopoliza = ec.idtopoliza)")
		.append(" INNER JOIN MIDAS.TCREASEGURADORCORREDOR reasegurador ON (reasegurador.idtcreaseguradorcorredor = ex.idreasegurador) ")
		.append(" LEFT JOIN MIDAS.TCREASEGURADORCORREDOR corredor ON (corredor.idtcreaseguradorcorredor = ex.idcorredor) ")
		.append(" LEFT JOIN MIDAS.toseccion seccion ON (dtcontrato.idtoseccion = seccion.idtoseccion) ")
		.append(" WHERE ex.idtoegresoreaseguro = ").append( idToEgresoReaseguro )
		.append(" ORDER BY planpagos.idtoplanpagoscobertura  ");
		
		final Query query = entityManager.createNativeQuery(sb.toString(), "participacionMap"); 
		List<ParticipacionReaseguradorContratoFacultativoDTO> result = null;
		try {
			result = query.getResultList();	
		} catch (Exception e) {
			LogDeMidasEJB3.log(e.getMessage(), Level.SEVERE, e);
			result = new ArrayList<ParticipacionReaseguradorContratoFacultativoDTO>();
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private ParticipacionReaseguradorContratoFacultativoDTO generarParticipacionContratoFacultativoDTO(
			PagoCoberturaReaseguradorDTO pagoCoberturaReasegurador,PagoCoberturaDTO pagoCobertura,
			EstadoCuentaDTO estadoCuentaDTO,DetalleContratoFacultativoDTO detalleContratoFacultativo,
			BigDecimal porcentajeParticipacionCobertura,
			Map<BigDecimal, PolizaDTO> mapaPolizas,
			Map<BigDecimal,String> mapaDescripcionSecciones,
			Map<BigDecimal,String> mapaDescripcionCoberturas,
			Map<BigDecimal,SubRamoDTO> mapaSubRamo){
		ParticipacionReaseguradorContratoFacultativoDTO participacion = null;
		if(pagoCobertura != null && pagoCoberturaReasegurador != null){
			
			participacion = new ParticipacionReaseguradorContratoFacultativoDTO();
			
			if(detalleContratoFacultativo != null){
				participacion.setNumeroInciso(
						detalleContratoFacultativo.getNumeroInciso() != null ? detalleContratoFacultativo.getNumeroInciso().intValue() : 0);
				participacion.setNumeroSubInciso(
						detalleContratoFacultativo.getNumeroSubInciso() != null ? detalleContratoFacultativo.getNumeroSubInciso().intValue() : 0);
				if(mapaDescripcionSecciones != null){
					participacion.setDescripcionSeccion(
							obtenerDescripcionSeccion(detalleContratoFacultativo.getIdToSeccion(), mapaDescripcionSecciones));
				}
				if(mapaDescripcionCoberturas != null){
					participacion.setDescripcionCobertura(
							obtenerDescripcionCobertura(detalleContratoFacultativo.getIdToCobertura(), mapaDescripcionCoberturas));
				}
				if(mapaSubRamo != null){
					SubRamoDTO subRamo = obtenerSubRamo(detalleContratoFacultativo.getIdTcSubramo(), mapaSubRamo);
					if(subRamo != null) {
						participacion.setSubRamoDTO(subRamo);
					}
				}
			}
			
			if(pagoCoberturaReasegurador.getMontoComision() != null) {
				participacion.setPrimaComision(new BigDecimal(pagoCoberturaReasegurador.getMontoComision()));
			} else {
				participacion.setPrimaComision(BigDecimal.ZERO);
			}
			
			if(pagoCoberturaReasegurador.getRetenerImpuesto() != null && pagoCoberturaReasegurador.getRetenerImpuesto()){
				if(pagoCoberturaReasegurador.getPorcentajeImpuesto() != null) {
					participacion.setPorcentajeRetencionImpuestos(new BigDecimal(pagoCoberturaReasegurador.getPorcentajeImpuesto()));
				} else {
					participacion.setPorcentajeRetencionImpuestos(BigDecimal.ZERO);
				}
				
				if(pagoCoberturaReasegurador.getMontoImpuesto() != null) {
					participacion.setPrimaRetenidaImpuestos(new BigDecimal(pagoCoberturaReasegurador.getMontoImpuesto()));
				} else {
					participacion.setPrimaRetenidaImpuestos(BigDecimal.ZERO);
				}
			}
			else{//Sin impuesto
				participacion.setPorcentajeRetencionImpuestos(BigDecimal.ZERO);
				participacion.setPrimaRetenidaImpuestos(BigDecimal.ZERO);
			}
			
			participacion.setNumeroExhibicion(pagoCobertura.getId().getNumeroExhibicion());
			participacion.setFechaPago(pagoCobertura.getFechaPago());
			
			if(pagoCoberturaReasegurador.getMontoPagoTotal() != null) {
				participacion.setPrimaNetaReasegurador(new BigDecimal(pagoCoberturaReasegurador.getMontoPagoTotal()));
			} else {
				participacion.setPrimaNetaReasegurador(BigDecimal.ZERO);
			}
			
			participacion.setEstatus(""+pagoCoberturaReasegurador.getEstatusPago());
			
			participacion.setPorcentajeParticipacionFacultativo(porcentajeParticipacionCobertura);
			
			PolizaDTO polizaDTO = null;
			if(estadoCuentaDTO != null && estadoCuentaDTO.getTipoReaseguro() != TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
				polizaDTO = obtenerPoliza(estadoCuentaDTO.getIdPoliza(), mapaPolizas);
				participacion.setNumeroEndoso(estadoCuentaDTO.getNumeroEndoso());
			}
			else if(detalleContratoFacultativo != null && detalleContratoFacultativo.getContratoFacultativoDTO_1() != null && 
					detalleContratoFacultativo.getContratoFacultativoDTO_1().getIdTmContratoFacultativo() != null){
				//buscar la poliza a traves del contrato facultativo
				String queryString = "select model.idToPoliza,model.soporteReaseguroDTO.numeroEndoso from LineaSoporteReaseguroDTO model where model.contratoFacultativoDTO.idTmContratoFacultativo = :idTmContratoFacultativo" +
						" order by model.idToPoliza";
				
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idTmContratoFacultativo",detalleContratoFacultativo.getContratoFacultativoDTO_1().getIdTmContratoFacultativo());
				
				List<Object> listaResultado = query.getResultList();
				if(listaResultado != null && !listaResultado.isEmpty()){
					Object[] resultados = (Object[])listaResultado.get(0);
					
					BigDecimal idToPoliza = Utilerias.obtenerBigDecimal(resultados[0]);
					polizaDTO = obtenerPoliza(idToPoliza, mapaPolizas);
					
					Integer numeroEndoso = Utilerias.obtenerInteger(resultados[1]);
					participacion.setNumeroEndoso(numeroEndoso);
				}
			}
			if(polizaDTO != null){
				participacion.setNumeroPoliza(polizaDTO.getFolioPoliza());
			}
			
			if(pagoCoberturaReasegurador.getReasegurador() != null){
				participacion.setNombreReasegurador(pagoCoberturaReasegurador.getReasegurador().getNombre());
			}
			if(pagoCoberturaReasegurador.getCorredor() != null){
				participacion.setNombreCorredor(pagoCoberturaReasegurador.getCorredor().getNombre());
			}
			
			if(pagoCoberturaReasegurador.getId() != null){
				participacion.setIdPagoCoberturaReasegurador(""+
						pagoCoberturaReasegurador.getId().getIdToPlanPagosCobertura()+"_"+
						pagoCoberturaReasegurador.getId().getNumeroExhibicion()+"_"+
						pagoCoberturaReasegurador.getId().getIdReasegurador());
			}
			
			participacion.setEstatusAutorizacionExhibicionVencida(pagoCoberturaReasegurador.getEstatusAutorizacionExhibicionVencida());
			participacion.setEstatusAutorizacionFaltaPagoAsegurado(pagoCoberturaReasegurador.getEstatusAutorizacionFaltaPagoAsegurado());
		}
		return participacion;
	}
	
	@SuppressWarnings("unchecked")
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerPagosPendientes(Date fechaCorte){
		List<ParticipacionReaseguradorContratoFacultativoDTO> listaParticipaciones = null;
		String queryString = "select model from PagoCoberturaReaseguradorDTO model where model.estatusPago = :estatusPago";
		if(fechaCorte != null){
			queryString += " and model.pagoCobertura.fechaPago <= :fechaCorte";
		}
		Query query = entityManager.createQuery(queryString);
		query.setParameter("estatusPago",PagoCoberturaReaseguradorDTO.PENDIENTE);
		if(fechaCorte != null) {
			query.setParameter("fechaCorte",fechaCorte);
		}
		query.setHint(QueryHints.REFRESH,HintValues.TRUE);
		
		List<PagoCoberturaReaseguradorDTO> listaPagosPendientes = query.getResultList();
		
		if(listaPagosPendientes != null){
			listaParticipaciones = obtenerListaParticipacionReasegurador(listaPagosPendientes);
		}
		return listaParticipaciones;
	}
	
	@SuppressWarnings("unchecked")
	public List<EstadoCuentaDecoradoDTO> obtenerAutorizacionesPendientesPagoFacultativo(){
		List<EstadoCuentaDecoradoDTO> listaEstadoCuentaDecorado = new ArrayList<EstadoCuentaDecoradoDTO>();
		try{
			String queryString = "select model from EstadoCuentaDTO model where (model.estatusAutorizacionPagoFacultativoPorEstadoCuenta = :estatusPendiente " +
					" or model.estatusAutorizacionPagoFacultativoPorEstadoCuenta = :estatusRechazado) and model.tipoReaseguro = :tipoReaseguro";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("estatusPendiente",SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA);
			query.setParameter("estatusRechazado",SistemaPersistencia.ESTATUS_RECHAZADA);
			query.setParameter("tipoReaseguro",TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);
			query.setHint(QueryHints.REFRESH,HintValues.TRUE);

			List<EstadoCuentaDTO> listaEstadoCuenta = query.getResultList();
			
			if(listaEstadoCuenta != null && !listaEstadoCuenta.isEmpty()){
				Map<EndosoId, EndosoDTO> mapaEndosos = new HashMap<EndosoId, EndosoDTO>();
				
				for(EstadoCuentaDTO estadoCuenta : listaEstadoCuenta){
					if(estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
						
						EndosoDTO endosoDTO = obtenerEndoso(estadoCuenta.getIdPoliza(), estadoCuenta.getNumeroEndoso().shortValue(), mapaEndosos);
						
						if(endosoDTO != null){
							PolizaDTO polizaDTO = endosoDTO.getPolizaDTO();
							if(polizaDTO != null){
								polizaDTO.setFolioPoliza(Utilerias.obtenerNumeroPoliza(polizaDTO));
								
								EstadoCuentaDecoradoDTO estadoCuentaDecorado = new EstadoCuentaDecoradoDTO(estadoCuenta);
								
								estadoCuentaDecorado.setPolizaDTO(polizaDTO);
								estadoCuentaDecorado.setEndosoDTO(endosoDTO);
								
								listaEstadoCuentaDecorado.add(estadoCuentaDecorado);
							}
						}//Fin validacion endoso != null
					}//Fin validacion tipo reaseguro
				}//Fin iteracion estados de cuena
			}//Fin validacion listaestadocuenta
			
		}catch(Exception e){
			LogDeMidasEJB3.log("Error al consultar los estados de cuenta con pago facultativo pendientes de autorizar", Level.SEVERE, e);
		}
		return listaEstadoCuentaDecorado ;
	}
	
	@SuppressWarnings("unchecked")
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerAutorizacionesPagosPendientes(short tipoAutorizacionPendiente){
		List<ParticipacionReaseguradorContratoFacultativoDTO> listaParticipaciones = null;
		String tipoAutorizacion = "No valida";
		String campoBusqueda = null;
		try{
			if(tipoAutorizacionPendiente == PagoCoberturaReaseguradorDTO.AUTORIZACION_PENDIENTE_EXHIBICION_VENCIDA){
				tipoAutorizacion = "Pago de exhibiciones vencidas";
				campoBusqueda = "estatusAutorizacionExhibicionVencida";
			}else if(tipoAutorizacionPendiente == PagoCoberturaReaseguradorDTO.AUTORIZACION_PENDIENTE_EXHIBICION_SIN_PAGO_ASEGURADO){
				tipoAutorizacion = "Pago de exhibiciones sin pago de asegurado";
				campoBusqueda = "estatusAutorizacionFaltaPagoAsegurado";
			}else{
				throw new RuntimeException("Se recibi� un par�metro no v�lido: tipoAutorizacionPendiente="+tipoAutorizacionPendiente+
						", utilice las constantes de PagoCoberturaReaseguradorDTO: AUTORIZACION_PENDIENTE_EXHIBICION_VENCIDA o AUTORIZACION_PENDIENTE_EXHIBICION_SIN_PAGO_ASEGURADO");
			}
			LogDeMidasEJB3.log("Consultando los pagos pendientes por autorizar. Tipo de autorizacion consultada: "+tipoAutorizacion+"("+tipoAutorizacionPendiente+")", Level.INFO, null);
			
			String queryString = "select model from PagoCoberturaReaseguradorDTO model where model."+campoBusqueda+" = :estatusPendiente or model."+campoBusqueda+" = :estatusRechazado";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("estatusPendiente",SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA);
			query.setParameter("estatusRechazado",SistemaPersistencia.ESTATUS_RECHAZADA);
			
			List<PagoCoberturaReaseguradorDTO> listaResultado = query.getResultList();
			if(listaResultado != null && !listaResultado.isEmpty()){
				listaParticipaciones = obtenerListaParticipacionReasegurador(listaResultado);
			}
			
		}catch(Exception e){
			LogDeMidasEJB3.log("Error al consultar los pagos pendientes por autorizar. Tipo de autorizacion consultada: "+tipoAutorizacion+"("+tipoAutorizacionPendiente+")", Level.SEVERE, e);
		}
		return listaParticipaciones;
	}
	
	public boolean autorizarPagoFacultativoPorEstadoCuenta(BigDecimal idToEstadoCuenta,boolean autorizacion,String claveUsuario){
		boolean resultado = false;
		try{
			if(claveUsuario != null && !claveUsuario.trim().equals("")){
				String operacion = (autorizacion ? "Autorizar" : "Rechazar");
				Short valorAutorizacion = (autorizacion ? SistemaPersistencia.ESTATUS_AUTORIZADA : SistemaPersistencia.ESTATUS_RECHAZADA);
				LogDeMidasEJB3.log("Intentando "+operacion+" pago facultativo del estado de cuenta: "+idToEstadoCuenta, Level.INFO, null);
				EstadoCuentaDTO estadoCuenta = entityManager.find(EstadoCuentaDTO.class,idToEstadoCuenta);
				if(estadoCuenta != null && estadoCuenta.getEstatusAutorizacionPagoFacultativoPorEstadoCuenta() != null && (
						estadoCuenta.getEstatusAutorizacionPagoFacultativoPorEstadoCuenta().shortValue() == SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA.shortValue() ||
						estadoCuenta.getEstatusAutorizacionPagoFacultativoPorEstadoCuenta().shortValue() == SistemaPersistencia.ESTATUS_RECHAZADA.shortValue())){
					estadoCuenta.setEstatusAutorizacionPagoFacultativoPorEstadoCuenta(valorAutorizacion);
					estadoCuenta.setUsuarioAutorizacionPagoFacultativoPorEstadoCuenta(claveUsuario);
					estadoCuenta.setFechaAutorizacionPagoFacultativoPorEstadoCuenta(new Date());
					
					entityManager.merge(estadoCuenta);
					resultado = true;
				}
			}
		}catch(Exception e){
			LogDeMidasEJB3.log("Error al intentar autorizar/rechazar un pago facultativo por estado de cuenta", Level.WARNING, e);
		}
		return resultado;
	}
	
	public boolean autorizarPagoCoberturaReasegurador(Long idToPlanPagosCobertura,Short numeroExhibicion,BigDecimal idReasegurador,
			boolean autorizacion,String claveUsuario,short tipoAutorizacionPendiente){
		boolean resultado = false;
		try{
			if(claveUsuario != null && !claveUsuario.trim().equals("") && 
					idToPlanPagosCobertura != null && numeroExhibicion != null && idReasegurador != null){
				String operacion = (autorizacion ? "Autorizar" : "Rechazar");
				String tipoAutorizacion = null;
				Short valorAutorizacion = (autorizacion ? SistemaPersistencia.ESTATUS_AUTORIZADA : SistemaPersistencia.ESTATUS_RECHAZADA);
				if(tipoAutorizacionPendiente == PagoCoberturaReaseguradorDTO.AUTORIZACION_PENDIENTE_EXHIBICION_VENCIDA){
					tipoAutorizacion = "Pago de exhibiciones vencidas";
//					campoBusqueda = "estatusAutorizacionExhibicionVencida";
				}else if(tipoAutorizacionPendiente == PagoCoberturaReaseguradorDTO.AUTORIZACION_PENDIENTE_EXHIBICION_SIN_PAGO_ASEGURADO){
					tipoAutorizacion = "Pago de exhibiciones sin pago de asegurado";
//					campoBusqueda = "estatusAutorizacionFaltaPagoAsegurado";
				}else{
					throw new RuntimeException("Se recibi� un par�metro no v�lido: tipoAutorizacionPendiente="+tipoAutorizacionPendiente+
							", utilice las constantes de PagoCoberturaReaseguradorDTO: AUTORIZACION_PENDIENTE_EXHIBICION_VENCIDA o AUTORIZACION_PENDIENTE_EXHIBICION_SIN_PAGO_ASEGURADO");
				}
				LogDeMidasEJB3.log("Intentando "+operacion+" "+tipoAutorizacion+". idToPlanPagosCobertura: "+idToPlanPagosCobertura+", numeroExhibicion: "+numeroExhibicion+
						"idReasegurador: "+idReasegurador, Level.INFO, null);
				PagoCoberturaReaseguradorId id = new PagoCoberturaReaseguradorId();
				id.setIdToPlanPagosCobertura(idToPlanPagosCobertura);
				id.setIdReasegurador(idReasegurador);
				id.setNumeroExhibicion(numeroExhibicion);
				
				PagoCoberturaReaseguradorDTO pagoPorAutorizar = entityManager.find(PagoCoberturaReaseguradorDTO.class,id);
				entityManager.refresh(pagoPorAutorizar);
				
				if(pagoPorAutorizar != null){
					if(tipoAutorizacionPendiente == PagoCoberturaReaseguradorDTO.AUTORIZACION_PENDIENTE_EXHIBICION_VENCIDA){
						if(pagoPorAutorizar.getEstatusAutorizacionExhibicionVencida() != null && (
								pagoPorAutorizar.getEstatusAutorizacionExhibicionVencida().shortValue() == SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA ||
								pagoPorAutorizar.getEstatusAutorizacionExhibicionVencida().shortValue() == SistemaPersistencia.ESTATUS_RECHAZADA)){
							pagoPorAutorizar.setEstatusAutorizacionExhibicionVencida(valorAutorizacion.byteValue());
							pagoPorAutorizar.setUsuarioAutorizacionExhibicionVencida(claveUsuario);
							pagoPorAutorizar.setFechaAutorizacionExhibicionVencida(new Date());
							
							entityManager.merge(pagoPorAutorizar);
							resultado = true;
						}
					}
					else if(pagoPorAutorizar.getEstatusAutorizacionFaltaPagoAsegurado() != null && (
								pagoPorAutorizar.getEstatusAutorizacionFaltaPagoAsegurado().shortValue() == SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA ||
								pagoPorAutorizar.getEstatusAutorizacionFaltaPagoAsegurado().shortValue() == SistemaPersistencia.ESTATUS_RECHAZADA
								)){
						pagoPorAutorizar.setEstatusAutorizacionFaltaPagoAsegurado(valorAutorizacion.byteValue());
						pagoPorAutorizar.setUsuarioAutorizacionFaltaPagoAsegurado(claveUsuario);
						pagoPorAutorizar.setFechaAutorizacionFaltaPagoAsegurado(new Date());
						
						entityManager.merge(pagoPorAutorizar);
						resultado = true;
					}
				}
			}
		}catch(Exception e){
			LogDeMidasEJB3.log("Error al intentar autorizar/rechazar un pago cobertura Reasegurador", Level.WARNING, e);
		}
		return resultado;
	}
	
	private List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerListaParticipacionReasegurador(List<PagoCoberturaReaseguradorDTO> listaPagos){
		List<ParticipacionReaseguradorContratoFacultativoDTO> listaResultado = null;
		
		if(listaPagos != null){
			listaResultado = new ArrayList<ParticipacionReaseguradorContratoFacultativoDTO>();
			
			Map<BigDecimal, PolizaDTO> mapaPolizas = new HashMap<BigDecimal, PolizaDTO>();
			Map<BigDecimal,String> mapaDescripcionSeccion = new HashMap<BigDecimal, String>();
			Map<BigDecimal,String> mapaDescripcionCobertura = new HashMap<BigDecimal, String>();
			Map<BigDecimal,SubRamoDTO> mapaDescripcionSubRamo = new HashMap<BigDecimal, SubRamoDTO>();
			
			for(PagoCoberturaReaseguradorDTO pagoCoberturaReasegurador : listaPagos){
				
				ContratoFacultativoDTO contratoFacultativo = pagoCoberturaReasegurador.getPagoCobertura().getPlanPagosCobertura().getDetalleContratoFacultativoDTO().getContratoFacultativoDTO_1();
				
				if(contratoFacultativo != null){
					ParticipacionReaseguradorContratoFacultativoDTO participacion = null;
					
					DetalleContratoFacultativoDTO detalleContrato = pagoCoberturaReasegurador.getPagoCobertura().getPlanPagosCobertura().getDetalleContratoFacultativoDTO();
					
					participacion = generarParticipacionContratoFacultativoDTO(pagoCoberturaReasegurador, pagoCoberturaReasegurador.getPagoCobertura(), 
							null, detalleContrato, null, mapaPolizas, mapaDescripcionSeccion, mapaDescripcionCobertura,mapaDescripcionSubRamo);
					
					if(participacion != null){
						listaResultado.add(participacion);
					}
				}
			}//fin iteracion listaPagosPendientes
		}
		
		return listaResultado;
	}
	
//	private String obtenerNombreReasegurador(BigDecimal idTcReaseguradorCorredor,Map<BigDecimal,String> mapaNombreReasegurador){
//		String descripcion = mapaNombreReasegurador.get(idTcReaseguradorCorredor);
//		if(descripcion == null){
//			String queryString = "select model.nombre from ReaseguradorCorredorDTO model where model.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("idtcreaseguradorcorredor", idTcReaseguradorCorredor);
//			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
//			Object result = query.getSingleResult();
//			if(result != null){
//				descripcion = (String) result;
//				mapaNombreReasegurador.put(idTcReaseguradorCorredor, descripcion);
//			}
//		}
//		return descripcion;
//	}
	
	private SubRamoDTO obtenerSubRamo(BigDecimal idTcSubRamo,Map<BigDecimal,SubRamoDTO> mapaSubRamo){
		SubRamoDTO subRamo = null;
		if(mapaSubRamo != null){
			subRamo = mapaSubRamo.get(idTcSubRamo);
			if(subRamo == null){
				String queryString = "select model from SubRamoDTO model where model.idTcSubRamo = :idTcSubRamo";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idTcSubRamo",idTcSubRamo);
				query.setHint(QueryHints.REFRESH,HintValues.TRUE);
				subRamo = (SubRamoDTO)query.getSingleResult();
				
				mapaSubRamo.put(idTcSubRamo, subRamo);
			}
		}
		else{
			String queryString = "select model from SubRamoDTO model where model.idTcSubRamo = :idTcSubRamo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTcSubRamo",idTcSubRamo);
			query.setHint(QueryHints.REFRESH,HintValues.TRUE);
			subRamo = (SubRamoDTO)query.getSingleResult();
		}
		
		return subRamo;
	}
	
	private String obtenerDescripcionCobertura(BigDecimal idToCobertura,Map<BigDecimal,String> mapaDescripcionCobertura){
		String descripcion = mapaDescripcionCobertura.get(idToCobertura);
		if(descripcion == null){
			String queryString = "select model.nombreComercial from CoberturaDTO model where model.idToCobertura = :idToCobertura";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCobertura", idToCobertura);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			Object result = query.getSingleResult();
			if(result != null){
				descripcion = (String) result;
				mapaDescripcionCobertura.put(idToCobertura, descripcion);
			}
		}
		return descripcion;
	}
	
	private String obtenerDescripcionSeccion(BigDecimal idToSeccion,Map<BigDecimal,String> mapaDescripcionSeccion){
		String descripcion = mapaDescripcionSeccion.get(idToSeccion);
		if(descripcion == null){
			String queryString = "select model.nombreComercial from SeccionDTO model where model.idToSeccion = :idToSeccion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			Object result = query.getSingleResult();
			if(result != null){
				descripcion = (String) result;
				mapaDescripcionSeccion.put(idToSeccion, descripcion);
			}
		}
		return descripcion;
	}
	
	private PolizaDTO obtenerPoliza(BigDecimal idToPoliza,Map<BigDecimal,PolizaDTO> mapaPolizas){
		PolizaDTO polizaDTO = mapaPolizas.get(idToPoliza);
		if(polizaDTO == null){
			polizaDTO = entityManager.find(PolizaDTO.class, idToPoliza);
			polizaDTO.setFolioPoliza(Utilerias.obtenerNumeroPoliza(polizaDTO));
			mapaPolizas.put(idToPoliza, polizaDTO);
		}
		return polizaDTO;
	}
	
	private EndosoDTO obtenerEndoso(BigDecimal idToPoliza,Short numeroEndoso,Map<EndosoId,EndosoDTO> mapaEndosos){
		EndosoId endosoId = new EndosoId(idToPoliza,numeroEndoso);
		EndosoDTO endosoDTO = mapaEndosos.get(endosoId);
		if(endosoDTO == null){
			endosoDTO = entityManager.find(EndosoDTO.class, endosoId);
			mapaEndosos.put(endosoId, endosoDTO);
		}
		return endosoDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ParticipacionCorredorDecoradoDTO> obtenerParticipaciones(ParticipacionDecoradoDTO participacion, BigDecimal idContratoFacultativo, BigDecimal idEgresoReaseguro){
		try{	
			List<ParticipacionCorredorDecoradoDTO> listaDecorado = new ArrayList<ParticipacionCorredorDecoradoDTO>();
				String queryString = "";
				Query query;
				ParticipacionCorredorDecoradoDTO participacionCorredorDecoradoDTO;
				
				//if (participacion.getTipo().equals(ReaseguradorCorredorDTO.CORREDOR)){
					if (participacion.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE || participacion.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
						List<ParticipacionCorredorDTO> listaQuery;
						queryString = "SELECT model	FROM ParticipacionCorredorDTO model	WHERE model.participacion.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreaseguradorcorredor";
						query = entityManager.createQuery(queryString);
						query.setParameter("idtcreaseguradorcorredor", participacion.getParticipacionDTO().getReaseguradorCorredor().getIdtcreaseguradorcorredor());
						listaQuery = query.getResultList();
						//x=0;
						for (ParticipacionCorredorDTO participacionCorredorDTO : listaQuery) {
							participacionCorredorDecoradoDTO = new ParticipacionCorredorDecoradoDTO(participacionCorredorDTO);
							listaDecorado.add(participacionCorredorDecoradoDTO);
						}
					}else{
						if (participacion.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
							List listaQuery;
							if (participacion.getTipo().equals(ReaseguradorCorredorDTO.CORREDOR)){
									queryString ="select rac.NOMBRE, rac.CNFS, pcf.PORCENTAJEPARTICIPACION, rac.procedencia, egre.MONTO," +
												 " rac.idtcreaseguradorcorredor, sopor.porcentajefacultativo, pf.COMISION " +
												 "from MIDAS.tdParticipacionFacultativo pf inner join MIDAS.tdparticipacionCorredorFac pcf " +
												 "on pf.IDTDPARTICIPACIONFACULTATIVO = pcf.IDTDPARTICIPACIONFACULTATIVO " +
												 "inner join MIDAS.tdcontratoFacultativo dcf on dcf.IDTDCONTRATOFACULTATIVO = pf.IDTDCONTRATOFACULTATIVO " +
												 "inner join MIDAS.TMLINEASOPORTEREASEGURO sopor on sopor.IDTMCONTRATOFACULTATIVO = dcf.IDTMCONTRATOFACULTATIVO " +
												 "INNER JOIN MIDAS.TCREASEGURADORCORREDOR rac ON pcf.IDTCREASEGURADORCORREDOR = rac.idtcreaseguradorcorredor " +
												 "inner join MIDAS.TOESTADOCUENTA edo on edo.IDTMCONTRATOFACULTATIVO=sopor.IDTMCONTRATOFACULTATIVO and edo.IDTCREASEGURADOR=pcf.IDTCREASEGURADORCORREDOR " +
												 "inner join MIDAS.TREGRESOESTADOCUENTA egre on egre.IDTOESTADOCUENTA=edo.IDTOESTADOCUENTA " +
											 	 "where pf.IDTDREASEGURADORCORREDOR=" + participacion.getParticipacionFacultativoDTO().getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor() +
												 " and dcf.idTmContratoFacultativo=" + idContratoFacultativo + 
												 " and egre.IDTOEGRESOREASEGURO=" + idEgresoReaseguro +
												 " group by rac.NOMBRE, rac.CNFS, pcf.PORCENTAJEPARTICIPACION, rac.procedencia, egre.MONTO, rac.idtcreaseguradorcorredor, " +
  												 "sopor.porcentajefacultativo, pf.COMISION";
												
							}else{
									queryString="select rac.NOMBRE, rac.CNFS, pf.PORCENTAJEPARTICIPACION, rac.procedencia, egre.MONTO, rac.idtcreaseguradorcorredor, " +
												"sopor.porcentajefacultativo, pf.COMISION from MIDAS.tdParticipacionFacultativo pf " +
												"inner join MIDAS.tdcontratoFacultativo dcf on dcf.IDTDCONTRATOFACULTATIVO = pf.IDTDCONTRATOFACULTATIVO " +
												"inner join MIDAS.TMLINEASOPORTEREASEGURO sopor on sopor.IDTMCONTRATOFACULTATIVO = dcf.IDTMCONTRATOFACULTATIVO " +
												"inner join MIDAS.TOESTADOCUENTA edo on edo.IDTMCONTRATOFACULTATIVO=sopor.IDTMCONTRATOFACULTATIVO " +
												"INNER JOIN MIDAS.TCREASEGURADORCORREDOR rac ON edo.idtcreasegurador = rac.idtcreaseguradorcorredor " +
												"inner join MIDAS.TREGRESOESTADOCUENTA egre on egre.IDTOESTADOCUENTA=edo.IDTOESTADOCUENTA " +
												"where pf.IDTDREASEGURADORCORREDOR=" + participacion.getParticipacionFacultativoDTO().getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor() +
												" and dcf.idTmContratoFacultativo=" + idContratoFacultativo + 
												" and egre.IDTOEGRESOREASEGURO=" + idEgresoReaseguro + 
												" group by rac.NOMBRE, rac.CNFS, pf.PORCENTAJEPARTICIPACION, rac.procedencia, egre.MONTO, rac.idtcreaseguradorcorredor, " +
												"sopor.porcentajefacultativo, pf.COMISION";
							}
							query = entityManager.createNativeQuery(queryString);
							//query.setParameter("idtcreaseguradorcorredor", participacion.getParticipacionFacultativoDTO().getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
							listaQuery = query.getResultList();
												
													
							for (Object resultadoCorredorFacultativo : listaQuery) { 
								BigDecimal montoAsignado = new BigDecimal("0.0");
								//x=0;
								String nombre = "";
								String cnfs = "";
								BigDecimal porcentajeParticipacion = new BigDecimal("0.0");
								String procedencia  = "";
								BigDecimal corredor = new BigDecimal("0.0");
								BigDecimal primaTotal = new BigDecimal("0.0");	
								BigDecimal porcentajeFacultativo = new BigDecimal("0.0");
								@SuppressWarnings("unused")
								BigDecimal primaFacultada = new BigDecimal("0.0"); 
								BigDecimal comision = new BigDecimal("0.0");
								BigDecimal subTotal = new BigDecimal("0.0");
								@SuppressWarnings("unused")
								BigDecimal total = new BigDecimal("0.0");
								
								nombre = (String) ((Object[])resultadoCorredorFacultativo)[0];
								cnfs = (String) ((Object[])resultadoCorredorFacultativo)[1];
								porcentajeParticipacion = (BigDecimal) ((Object[])resultadoCorredorFacultativo)[2];
								procedencia = (String) ((Object[])resultadoCorredorFacultativo)[3];
								primaTotal = (BigDecimal) ((Object[])resultadoCorredorFacultativo)[4];								
								corredor = (BigDecimal) ((Object[])resultadoCorredorFacultativo)[5];
								porcentajeFacultativo = (BigDecimal) ((Object[])resultadoCorredorFacultativo)[6];
								comision = (BigDecimal) ((Object[])resultadoCorredorFacultativo)[7];
								primaFacultada = primaTotal.multiply(porcentajeFacultativo.divide(BigDecimal.valueOf(100.0)));
								
								ReaseguradorCorredorDTO corredorDTO = new ReaseguradorCorredorDTO();
								corredorDTO.setCnfs(cnfs);
								corredorDTO.setProcedencia(procedencia);
								corredorDTO.setIdtcreaseguradorcorredor(corredor);
								corredorDTO.setNombre(nombre);
								
								ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO = new ParticipacionCorredorFacultativoDTO();
								participacionCorredorFacultativoDTO.setPorcentajeParticipacion(porcentajeParticipacion);
								participacionCorredorFacultativoDTO.setReaseguradorCorredorDTO(corredorDTO);
											
								//montoAsignado=primaFacultada.multiply(new BigDecimal(porcentajeParticipacion.doubleValue()).divide(BigDecimal.valueOf(100.0)));
								montoAsignado=primaTotal;
								subTotal = montoAsignado.multiply(comision.divide(BigDecimal.valueOf(100.0)));
								total = montoAsignado.subtract(subTotal);
								participacionCorredorDecoradoDTO = new ParticipacionCorredorDecoradoDTO(participacionCorredorFacultativoDTO);
							
								participacionCorredorDecoradoDTO.setMontoAsignado(montoAsignado);
								listaDecorado.add(participacionCorredorDecoradoDTO);									
							}
						}						
					}
				//}
				
			return listaDecorado;
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Obtener Participaciones failed", Level.SEVERE, e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<EgresoEstadoCuentaDTO> obtenerEgresosPorEstadoCuenta(EstadoCuentaDTO estadoCuenta, BigDecimal idEgresoReaseguro) {
		try{
			if(idEgresoReaseguro != null){
				String queryString = "SELECT model FROM EgresoEstadoCuentaDTO model " +
				"WHERE model.id.idEgresoReaseguro = :idEgresoReaseguro";
	
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idEgresoReaseguro", idEgresoReaseguro);
	
				return query.getResultList();
			}else{
				String queryString = "SELECT model FROM EgresoEstadoCuentaDTO model " +
				"WHERE model.id.idEstadoCuenta = :idEstadoCuenta";
	
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idEstadoCuenta", estadoCuenta.getIdEstadoCuenta());
	
				return query.getResultList();
			}
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Obtener Egresos Por Estado de Cuenta failed", Level.SEVERE, e);
			throw e;
		}
	}

	/*
	public List<EgresoEstadoCuentaDTO> borrarEgresoDTO(EgresoReaseguroDTO egresoReaseguroId, BigDecimal idEstadoCuenta) {
		//EgresoReaseguroDTO
		this.regresarPago(egresoReaseguroId, idEstadoCuenta);
		eliminarEgresoDTOEstadoCuenta(egresoReaseguroId);
		String queryString = "DELETE FROM EgresoReaseguroDTO model WHERE model.idEgresoReaseguro = :idTmEgresoReaseguro";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTmEgresoReaseguro", egresoReaseguroId.getIdEgresoReaseguro());
		//IDTOEGRESOREASEGURO 
		query.executeUpdate();
		return null;		
	}
	
	public List<EgresoEstadoCuentaDTO> borrarEgresoCheque(EgresoReaseguroDTO egresoReaseguroId, BigDecimal idEstadoCuenta) {
		//EgresoReaseguroDTO
		this.regresarPago(egresoReaseguroId, idEstadoCuenta);
		eliminarEgresoDTOEstadoCuenta(egresoReaseguroId);
		String queryString = "DELETE FROM EgresoReaseguroDTO model WHERE model.idEgresoReaseguro = :idTmEgresoReaseguro and model.estatus=5";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTmEgresoReaseguro", egresoReaseguroId.getIdEgresoReaseguro());
		//IDTOEGRESOREASEGURO 
		query.executeUpdate();
		return null;		
	}
	*/

	public List<EgresoEstadoCuentaDTO> eliminarEgresoDTOEstadoCuenta(EgresoReaseguroDTO egresoReaseguroId) {
		// TODO Auto-generated method stub
		//String queryString = "DELETE FROM EgresoEstadoCuentaDTO model WHERE model.idEgresoReaseguro = :idTmEgresoReaseguro and model.estatus=0 ";
		String queryString =  "delete from EgresoEstadoCuentaDTO model where model.id.idEgresoReaseguro=:idTmEgresoReaseguro " +
							  "and model.id.idEgresoReaseguro in (select d.idEgresoReaseguro from EgresoReaseguroDTO d " +
							  "where d.idEgresoReaseguro=:idTmEgresoReaseguro)"; 
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTmEgresoReaseguro", egresoReaseguroId.getIdEgresoReaseguro());
		//IDTOEGRESOREASEGURO 
		query.executeUpdate();
		
		return null;		
	}
	
	
	public Object ultimoEgresoDTO() {
		Object resultado = null;
		String queryString =  "select MAX(IDTOEGRESOREASEGURO) from MIDAS.TOEGRESOREASEGURO"; 
		Query nativeQuery = entityManager.createNativeQuery(queryString);
		resultado = nativeQuery.getSingleResult();		
		return resultado;	
	}
	
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void actualizaEgresoReaseguro(BigDecimal idEgresoReaseguro, Integer estatus, BigDecimal noCheque) {
		Validate.notNull(noCheque);
		try {				
			EgresoReaseguroDTO egresoReaseguro = this.findById(idEgresoReaseguro);
			Boolean cambioEstatus = egresoReaseguro.getEstatus() != estatus;
						
			String queryString = "UPDATE MIDAS.TREGRESOESTADOCUENTA " +
								 "SET ESTATUS=" + estatus + ", IDCHEQUE=" + noCheque + " " +
								 "WHERE IDTOEGRESOREASEGURO = " + idEgresoReaseguro;			
			Query query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();	
						
			queryString = "UPDATE MIDAS.TOEGRESOREASEGURO " +
						 "SET ESTATUS=" + estatus + " " +
						 "WHERE IDTOEGRESOREASEGURO = " + idEgresoReaseguro;			
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();	
			
			//Se actualizan las exhibiciones y estados de cuenta con el nuevo estatus
			if(cambioEstatus && estatus == EgresoReaseguroDTO.ESTATUS_PAGADO){				
				List<PagoCoberturaReaseguradorDTO> exhibiciones = 
					planPagosCobertura.obtenerExhibicionesPorEgreso(idEgresoReaseguro);
				for(PagoCoberturaReaseguradorDTO exhibicion : exhibiciones){
					exhibicion.setEstatusPago(PagoCoberturaReaseguradorDTO.PAGADO);				
				    planPagosCobertura.actualizarExhibicion(exhibicion);
				}								
				this.confirmarPago(egresoReaseguro);				
			}else if(cambioEstatus && estatus == EgresoReaseguroDTO.ESTATUS_CANCELADO){
				List<PagoCoberturaReaseguradorDTO> exhibiciones = 
					planPagosCobertura.obtenerExhibicionesPorEgreso(idEgresoReaseguro);
				for(PagoCoberturaReaseguradorDTO exhibicion : exhibiciones){
					exhibicion.setEstatusPago(PagoCoberturaReaseguradorDTO.PENDIENTE);				
				    planPagosCobertura.actualizarExhibicion(exhibicion);
				}				
				this.regresarPago(egresoReaseguro);				
			}
			
			entityManager.flush();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Error al actualizar estatus y idCheque en TOEGRESOREASEGURO y TREGRESOESTADOCUENTA : " +
					           "idToEgresoReaseguro = "+idEgresoReaseguro+" ", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void actualizaEgresoReaseguroBorrar(BigDecimal egresoReaseguro, Integer estatus, BigDecimal noCheque) {
		// TODO Auto-generated method stub
		//EgresoReaseguroDTO
		try {	
			String queryString = "UPDATE MIDAS.TREGRESOESTADOCUENTA SET ESTATUS=" + estatus + ", IDCHEQUE=" + noCheque + " WHERE IDTOEGRESOREASEGURO = " + egresoReaseguro + " ";
			Query query;
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();	
			//aqui voy TOEGRESOREASEGURO
			String str = "UPDATE MIDAS.TOEGRESOREASEGURO SET ESTATUS=" + estatus + " WHERE IDTOEGRESOREASEGURO = " + egresoReaseguro + "";
			Query query1;
			query1 = entityManager.createNativeQuery(str);
			query1.executeUpdate();	
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Error al Actualizar estatus TOEGRESOREASEGURO y TREGRESOESTADOCUENTA = "+egresoReaseguro+" ", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<EgresoEstadoCuentaDTO> obtenerEstadosCuentaEgresoEstatus() {
		
		String queryString = "SELECT model FROM EgresoEstadoCuentaDTO model WHERE model.idEstatus=0 ";
		Query nativeQuery = entityManager.createQuery(queryString);
		List<EgresoEstadoCuentaDTO> lista = nativeQuery.getResultList();
		return lista;
	}
	
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void regresarPago(EgresoReaseguroDTO egreso) {		
		/*
		 * TODO 1.- Registrar a Contabilidad para confirmar el Pago del Egreso
		 * Pendiente por Definir Interface con Contabilidad.
		 */
		try{
			//egreso.setEstatus(5);
			//entityManager.merge(egreso);
			String queryString = "SELECT model FROM EgresoEstadoCuentaDTO model " +
			 					 "WHERE model.id.idEgresoReaseguro = :idEgresoReaseguro"; 	 
				
			Query query = entityManager.createQuery(queryString);
	        query.setParameter("idEgresoReaseguro", egreso.getIdEgresoReaseguro());
			List<EgresoEstadoCuentaDTO> egresoEstadoCuentaList = query.getResultList();
			EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO;
			if (egresoEstadoCuentaList != null){
				for (EgresoEstadoCuentaDTO egresoEstadoCuentaDTO : egresoEstadoCuentaList) {
					estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(egresoEstadoCuentaDTO.getEstadoCuentaDTO());
					estadoCuentaDecoradoDTO.setMontoEgreso(egresoEstadoCuentaDTO.getMonto());
					registrarMovimientoCancel(egreso, egresoEstadoCuentaDTO, estadoCuentaDecoradoDTO);
				}
			}
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Regresar Pago failed", Level.SEVERE, e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<EgresoReaseguroDTO> obtenerEgresosCancelacionPendiente() {				
		String queryString = "SELECT model " +
							 "FROM EgresoReaseguroDTO model " +
							 "WHERE model.estatus="+EgresoReaseguroDTO.ESTATUS_EN_PROCESO_DE_CANCELACION;
		Query query = entityManager.createQuery(queryString);
		List<EgresoReaseguroDTO> egresos = query.getResultList();
		return egresos;
	}
	
	@SuppressWarnings("unchecked")
	public List<EgresoEstadoCuentaDTO> obtenerEstadosCuentaEgresoCancel() {
		
		String queryString = "SELECT model FROM EgresoEstadoCuentaDTO model WHERE model.idEstatus=5 ";
		Query nativeQuery = entityManager.createQuery(queryString);
		List<EgresoEstadoCuentaDTO> lista = nativeQuery.getResultList();
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	public EgresoReaseguroDTO obtenerEgresosEstadoCuentaCheque(BigDecimal idEstadoCuenta, BigDecimal idEgresoReaseguro) {
		try{
            String queryString = "SELECT model FROM EgresoReaseguroDTO model, EgresoEstadoCuentaDTO e " +
            			         "WHERE model.idEgresoReaseguro = e.id.idEgresoReaseguro " +
            			         "AND e.id.idEstadoCuenta = :idEstadoCuenta " +
		                         "AND e.id.idEgresoReaseguro = :idEgresoReaseguro";
            
            /*"SELECT model from EgresoReaseguroDTO model, EgresoEstadoCuentaDTO e WHERE " +
			 " model.idEgresoReaseguro = e.id.idEgresoReaseguro " +
			 " AND (e.idEstatus = 1)" +
			 " ORDER BY model.fechaEgreso";	*/
	
		     Query query = entityManager.createQuery(queryString);
		     query.setParameter("idEstadoCuenta",idEstadoCuenta);
	         query.setParameter("idEgresoReaseguro", idEgresoReaseguro);
	         List<EgresoReaseguroDTO> listaEgresos = query.getResultList();
	         EgresoReaseguroDTO egreso = null;
	         if(listaEgresos != null && !listaEgresos.isEmpty()){
	        	 if(listaEgresos.size() == 1) {
					egreso = listaEgresos.get(0);
				} else{
	        		 //trae mas de un registro
	        		 //egreso = listaEgresos;
	        	 }
	         }
          	 return egreso;

          }catch(RuntimeException e){
		    	LogDeMidasEJB3.log("Obtener Egresos Por Estado de Cuenta failed", Level.SEVERE, e);
			    throw e;
  	      }
   }
	
	public void eliminarEgreso(BigDecimal idEgresoReaseguro) {
		try {				
			String queryString = "update PagoCoberturaReaseguradorDTO exh " +
			 "set exh.egresoReaseguro = null, exh.estatusPago = " + PagoCoberturaReaseguradorDTO.PENDIENTE +
			 "where exh.egresoReaseguro.idEgresoReaseguro = :idEgresoReaseguro"; 
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idEgresoReaseguro", idEgresoReaseguro);
			query.executeUpdate();	
			
			queryString = "delete " +
			 			  "from EgresoEstadoCuentaDTO egrEdoCta " +
			 			  "where egrEdoCta.id.idEgresoReaseguro = :idEgresoReaseguro";
			
			query = entityManager.createQuery(queryString);
			query.setParameter("idEgresoReaseguro", idEgresoReaseguro);
			query.executeUpdate();	
			
			queryString = "delete " +
						  "from EgresoReaseguroDTO egr " +
						  "where egr.idEgresoReaseguro = :idEgresoReaseguro";
			
			query = entityManager.createQuery(queryString);
			query.setParameter("idEgresoReaseguro", idEgresoReaseguro);
			query.executeUpdate();	
								 
			entityManager.flush();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Error al eliminar egreso (idToEgresoReaseguro= "+idEgresoReaseguro+") ", Level.SEVERE, re);
			throw re;
		}
	}
}