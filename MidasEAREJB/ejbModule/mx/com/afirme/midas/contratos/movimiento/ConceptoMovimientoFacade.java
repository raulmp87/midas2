package mx.com.afirme.midas.contratos.movimiento;
// default package

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity ConceptoMovimientoDTO.
 * @see .ConceptoMovimientoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ConceptoMovimientoFacade  implements ConceptoMovimientoFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ConceptoMovimientoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ConceptoMovimientoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ConceptoMovimientoDTO entity) {
    				LogDeMidasEJB3.log("saving ConceptoMovimientoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ConceptoMovimientoDTO entity.
	  @param entity ConceptoMovimientoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ConceptoMovimientoDTO entity) {
    				LogDeMidasEJB3.log("deleting ConceptoMovimientoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ConceptoMovimientoDTO.class, entity.getIdConceptoMovimiento());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ConceptoMovimientoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ConceptoMovimientoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ConceptoMovimientoDTO entity to update
	 @return ConceptoMovimientoDTO the persisted ConceptoMovimientoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ConceptoMovimientoDTO update(ConceptoMovimientoDTO entity) {
    				LogDeMidasEJB3.log("updating ConceptoMovimientoDTO instance", Level.INFO, null);
	        try {
            ConceptoMovimientoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ConceptoMovimientoDTO findById( int id) {
    				LogDeMidasEJB3.log("finding ConceptoMovimientoDTO instance with id: " + id, Level.INFO, null);
	        try {
            ConceptoMovimientoDTO instance = entityManager.find(ConceptoMovimientoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ConceptoMovimientoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ConceptoMovimientoDTO property to query
	  @param value the property value to match
	  	  @return List<ConceptoMovimientoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ConceptoMovimientoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ConceptoMovimientoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ConceptoMovimientoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ConceptoMovimientoDTO entities.
	  	  @return List<ConceptoMovimientoDTO> all ConceptoMovimientoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConceptoMovimientoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ConceptoMovimientoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ConceptoMovimientoDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}