package mx.com.afirme.midas.contratos.movimiento;
// default package

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDetalleDTO;
import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDetalleFacadeRemote;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity ConceptoMovimientoDetalleDTO.
 * @see .ConceptoMovimientoDetalleDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ConceptoMovimientoDetalleFacade  implements ConceptoMovimientoDetalleFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ConceptoMovimientoDetalleDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ConceptoMovimientoDetalleDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ConceptoMovimientoDetalleDTO entity) {
    				LogUtil.log("saving ConceptoMovimientoDetalleDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ConceptoMovimientoDetalleDTO entity.
	  @param entity ConceptoMovimientoDetalleDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ConceptoMovimientoDetalleDTO entity) {
    				LogUtil.log("deleting ConceptoMovimientoDetalleDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ConceptoMovimientoDetalleDTO.class, entity.getIdConceptoDetalle());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ConceptoMovimientoDetalleDTO entity and return it or a copy of it to the sender. 
	 A copy of the ConceptoMovimientoDetalleDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ConceptoMovimientoDetalleDTO entity to update
	 @return ConceptoMovimientoDetalleDTO the persisted ConceptoMovimientoDetalleDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ConceptoMovimientoDetalleDTO update(ConceptoMovimientoDetalleDTO entity) {
    				LogUtil.log("updating ConceptoMovimientoDetalleDTO instance", Level.INFO, null);
	        try {
            ConceptoMovimientoDetalleDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ConceptoMovimientoDetalleDTO findById( int id) {
    				LogUtil.log("finding ConceptoMovimientoDetalleDTO instance with id: " + id, Level.INFO, null);
	        try {
            ConceptoMovimientoDetalleDTO instance = entityManager.find(ConceptoMovimientoDetalleDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ConceptoMovimientoDetalleDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ConceptoMovimientoDetalleDTO property to query
	  @param value the property value to match
	  	  @return List<ConceptoMovimientoDetalleDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ConceptoMovimientoDetalleDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding ConceptoMovimientoDetalleDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ConceptoMovimientoDetalleDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ConceptoMovimientoDetalleDTO entities.
	  	  @return List<ConceptoMovimientoDetalleDTO> all ConceptoMovimientoDetalleDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConceptoMovimientoDetalleDTO> findAll(
		) {
					LogUtil.log("finding all ConceptoMovimientoDetalleDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ConceptoMovimientoDetalleDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}