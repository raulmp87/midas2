package mx.com.afirme.midas.contratos.movimiento;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;


/**
 * Facade for entity TipoReaseguroDTO.
 * @see .TipoReaseguroDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class TipoReaseguroFacade  implements TipoReaseguroFacadeRemote {
	
	public static final int TIPO_CONTRATO_CUOTA_PARTE = 1;
	public static final int TIPO_CONTRATO_PRIMER_EXCEDENTE = 2;
	public static final int TIPO_CONTRATO_FACULTATIVO = 3;
	public static final int TIPO_RETENCION = 4;
	

    @PersistenceContext private EntityManager entityManager;
		/**
	 Perform an initial save of a previously unsaved TipoReaseguroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoReaseguroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoReaseguroDTO entity) {
    				LogDeMidasEJB3.log("saving TipoReaseguroDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent TipoReaseguroDTO entity.
	  @param entity TipoReaseguroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoReaseguroDTO entity) {
    				LogDeMidasEJB3.log("deleting TipoReaseguroDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(TipoReaseguroDTO.class, entity.getTipoReaseguro());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved TipoReaseguroDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoReaseguroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoReaseguroDTO entity to update
	 @return TipoReaseguroDTO the persisted TipoReaseguroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public TipoReaseguroDTO update(TipoReaseguroDTO entity) {
    				LogDeMidasEJB3.log("updating TipoReaseguroDTO instance", Level.INFO, null);
	        try {
            TipoReaseguroDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public TipoReaseguroDTO findById( int id) {
    				LogDeMidasEJB3.log("finding TipoReaseguroDTO instance with id: " + id, Level.INFO, null);
	        try {
            TipoReaseguroDTO instance = entityManager.find(TipoReaseguroDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all TipoReaseguroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoReaseguroDTO property to query
	  @param value the property value to match
	  	  @return List<TipoReaseguroDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<TipoReaseguroDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding TipoReaseguroDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from TipoReaseguroDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all TipoReaseguroDTO entities.
	  	  @return List<TipoReaseguroDTO> all TipoReaseguroDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoReaseguroDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all TipoReaseguroDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from TipoReaseguroDTO model order by model.tipoReaseguro";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	public TipoReaseguroDTO findById(BigDecimal id) {
		return this.findById(id.intValue());
	}

	public TipoReaseguroDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoReaseguroDTO findById(double id) {
		return this.findById((int)id);
	}

	public List<TipoReaseguroDTO> listRelated(Object id) {
		return null;
	}	
}