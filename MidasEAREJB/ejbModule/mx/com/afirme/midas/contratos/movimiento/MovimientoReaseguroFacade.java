package mx.com.afirme.midas.contratos.movimiento;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity MovimientoReaseguroDTO.
 * @see .MovimientoReaseguroDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class MovimientoReaseguroFacade  implements MovimientoReaseguroFacadeRemote {
    @PersistenceContext private EntityManager entityManager;
    
		/**
	 Perform an initial save of a previously unsaved MovimientoReaseguroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MovimientoReaseguroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public MovimientoReaseguroDTO save(MovimientoReaseguroDTO entity) {LogDeMidasEJB3.log("saving MovimientoReaseguroDTO instance", Level.INFO, null);
        try {
        	entityManager.persist(entity);
        	LogDeMidasEJB3.log("save successful", Level.INFO, null);
        	return entity;
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
        	throw re;
	    }
    }
    
    /**
	 Delete a persistent MovimientoReaseguroDTO entity.
	  @param entity MovimientoReaseguroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MovimientoReaseguroDTO entity) {
    	LogDeMidasEJB3.log("deleting MovimientoReaseguroDTO instance", Level.INFO, null);
    	try {
    		entity = entityManager.getReference(MovimientoReaseguroDTO.class, entity.getIdMovimiento());
    		entityManager.remove(entity);
    		LogDeMidasEJB3.log("delete successful", Level.INFO, null);
    	} catch (RuntimeException re) {
    		LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
    		throw re;
    	}
    }
    
    /**
	 Persist a previously saved MovimientoReaseguroDTO entity and return it or a copy of it to the sender. 
	 A copy of the MovimientoReaseguroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MovimientoReaseguroDTO entity to update
	 @return MovimientoReaseguroDTO the persisted MovimientoReaseguroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public MovimientoReaseguroDTO update(MovimientoReaseguroDTO entity) {
    	LogDeMidasEJB3.log("updating MovimientoReaseguroDTO instance", Level.INFO, null);
    	try {
    		MovimientoReaseguroDTO result = entityManager.merge(entity);
    		LogDeMidasEJB3.log("update successful", Level.INFO, null);
    		return result;
    	} catch (RuntimeException re) {
    		LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
    		throw re;
        }
    }
    
    public MovimientoReaseguroDTO findById( BigDecimal id) {
    	LogDeMidasEJB3.log("finding MovimientoReaseguroDTO instance with id: " + id, Level.INFO, null);
    	try {
    		MovimientoReaseguroDTO instance = entityManager.find(MovimientoReaseguroDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
        	throw re;
        }
    }
    

/**
	 * Find all MovimientoReaseguroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the MovimientoReaseguroDTO property to query
	  @param value the property value to match
	  	  @return List<MovimientoReaseguroDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<MovimientoReaseguroDTO> findByProperty(String propertyName, final Object value) {
    	LogDeMidasEJB3.log("finding MovimientoReaseguroDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
    	try {
    		final String queryString = "select model from MovimientoReaseguroDTO model where model." 
    			+ propertyName + "= :propertyValue";
    		Query query = entityManager.createQuery(queryString);
    		query.setParameter("propertyValue", value);
    		return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}			
	
	/**
	 * Find all MovimientoReaseguroDTO entities.
	  	  @return List<MovimientoReaseguroDTO> all MovimientoReaseguroDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<MovimientoReaseguroDTO> findAll() {
		LogDeMidasEJB3.log("finding all MovimientoReaseguroDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from MovimientoReaseguroDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void notificarMovimientosContabilidadReaseguro(BigDecimal idToPoliza,int numeroEndoso,Date fechaContabilizado,String comodin){
		try {
			LogDeMidasEJB3.log("Inicia notificación de contabilidad para los movimientos de la póliza: "+idToPoliza+", numeroEndoso: "+numeroEndoso+", fechaContabilizado: "+fechaContabilizado+" (idtoreportesiniestro is null and idtomovimientosiniestro is null)", Level.INFO, null);
			//Actualizacion de los movimientos soporte reaseguro
			String queryString = "UPDATE MIDAS.toMovimientoReaseguro SET contabilizado = 1, fechaContabilizado = TO_DATE('"+(new java.sql.Date(fechaContabilizado.getTime()).toString())+"','yyyy-MM-dd') " +
					", comodin = '"+comodin+"'"
					+" where idToPoliza = "+idToPoliza+" and numeroEndoso = "+numeroEndoso+" and idtoreportesiniestro is null and idtomovimientosiniestro is null";
			Query query;
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
			LogDeMidasEJB3.log("Termina exitosamente la notificación de contabilidad para los movimientos de la póliza: "+idToPoliza+", numeroEndoso: "+numeroEndoso+", fechaContabilizado: "+fechaContabilizado+" (idtoreportesiniestro is null and idtomovimientosiniestro is null)", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Ocurrió un error en la notificación de contabilidad para los movimientos de la póliza: "+idToPoliza+", numeroEndoso: "+numeroEndoso+", fechaContabilizado: "+fechaContabilizado+" (idtoreportesiniestro is null and idtomovimientosiniestro is null)", Level.SEVERE, re);
			throw re;
		}
	}
	
	public BigDecimal obtenerCantidadMovimientosReaseguro(BigDecimal idToPoliza,int numeroEndoso,String propiedadTabla,String valor){
		try {
			LogDeMidasEJB3.log("Contando cantidad de movimientos de la póliza: "+idToPoliza+", numeroEndoso: "+numeroEndoso+", con propiedad: \""+propiedadTabla+"\", valor: "+valor+" (idtoreportesiniestro is null and idtomovimientosiniestro is null)", Level.INFO, null);
			//Actualizacion de los movimientos soporte reaseguro
			String queryString = "SELECT COUNT (idToMovimiento) from MIDAS.toMovimientoReaseguro where idToPoliza = "+idToPoliza+" and numeroEndoso = "+numeroEndoso
					+((propiedadTabla != null && valor != null)? " and "+propiedadTabla+" = "+valor : "" )+" and idtoreportesiniestro is null and idtomovimientosiniestro is null";
			Query query;
			query = entityManager.createNativeQuery(queryString);
			BigDecimal resultado = (BigDecimal) query.getSingleResult();
			LogDeMidasEJB3.log(resultado+" movimientos encontrados de la póliza: "+idToPoliza+", numeroEndoso: "+numeroEndoso+", con propiedad: \""+propiedadTabla+"\", valor: "+valor+" (idtoreportesiniestro is null and idtomovimientosiniestro is null)", Level.INFO, null);
			return resultado;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Ocurrió un al contar los movimientos de la póliza: "+idToPoliza+", numeroEndoso: "+numeroEndoso, Level.SEVERE, re);
			throw re;
		}
	}
}