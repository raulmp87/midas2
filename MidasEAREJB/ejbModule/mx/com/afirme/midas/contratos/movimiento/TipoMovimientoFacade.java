package mx.com.afirme.midas.contratos.movimiento;
// default package

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.movimiento.TipoMovimientoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity TipoMovimientoDTO.
 * @see .TipoMovimientoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class TipoMovimientoFacade  implements TipoMovimientoFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved TipoMovimientoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoMovimientoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoMovimientoDTO entity) {
    				LogDeMidasEJB3.log("saving TipoMovimientoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent TipoMovimientoDTO entity.
	  @param entity TipoMovimientoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoMovimientoDTO entity) {
    				LogDeMidasEJB3.log("deleting TipoMovimientoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(TipoMovimientoDTO.class, entity.getIdTipoMovimiento());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved TipoMovimientoDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoMovimientoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoMovimientoDTO entity to update
	 @return TipoMovimientoDTO the persisted TipoMovimientoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public TipoMovimientoDTO update(TipoMovimientoDTO entity) {
    				LogDeMidasEJB3.log("updating TipoMovimientoDTO instance", Level.INFO, null);
	        try {
            TipoMovimientoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public TipoMovimientoDTO findById( int id) {
    				LogDeMidasEJB3.log("finding TipoMovimientoDTO instance with id: " + id, Level.INFO, null);
	        try {
            TipoMovimientoDTO instance = entityManager.find(TipoMovimientoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all TipoMovimientoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoMovimientoDTO property to query
	  @param value the property value to match
	  	  @return List<TipoMovimientoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<TipoMovimientoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding TipoMovimientoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from TipoMovimientoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all TipoMovimientoDTO entities.
	  	  @return List<TipoMovimientoDTO> all TipoMovimientoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoMovimientoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all TipoMovimientoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from TipoMovimientoDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}