package mx.com.afirme.midas.contratos.movimiento;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.estadocuenta.AcumuladorDTO;
import mx.com.afirme.midas.contratos.estadocuenta.AcumuladorFacadeRemote;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaFacadeRemote;
import mx.com.afirme.midas.contratos.linea.CalculoSuscripcionesDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.contratos.linea.SuscripcionDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableDTO;
import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableFacadeRemote;
import mx.com.afirme.midas.interfaz.remesa.RemesaFacadeRemote;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;


@Stateless
public class MovimientoReaseguroServicios implements MovimientoReaseguroServiciosRemote {

	public static final String CLAVE_MODULO_EMISION = "REASEGURO-EMIS";
    @Resource
    private SessionContext context;	

	@EJB
	private EstadoCuentaFacadeRemote estadoCuentaEjb;
	
	@EJB
	private AcumuladorFacadeRemote acumuladorEjb;
	
	@EJB
	private AsientoContableFacadeRemote asientoContableFacade;	
	
	@EJB
	private RemesaFacadeRemote remesaFacadeRemote;
	
	 @PersistenceContext
	 private EntityManager entityManager;
	
	 @TransactionAttribute(TransactionAttributeType.REQUIRED)
	 public MovimientoReaseguroDTO registrarMovimiento(MovimientoReaseguroDTO movimiento,Boolean restarCantidad) {
		 EstadoCuentaDTO estadoCuenta = new EstadoCuentaDTO();
		 AcumuladorDTO acumulador = new AcumuladorDTO();
		 ReporteSiniestroDTO reporteSiniestroDTO;
		 boolean aplicaTerminacionSiniestroSatisfactoria = true;
		 try{
			 /*
			  * se establece la descripcion del movimiento y se perciste el movimiento
			  */
			 validarCamposRequeridos(movimiento);//valida los campos que se requieren para el movimiento
			 calcularEjercicio(movimiento);//se calcula el ejercicio
			 calcularSuscripcion(movimiento);//calcula suscripcion
			 //si el movimiento tiene un idtoMovimiento, no se persiste.
			 if(movimiento.getIdMovimiento() == null || movimiento.getIdMovimiento().compareTo(BigDecimal.ZERO) == 0){
				 establecerDescripcion(movimiento);//se establece la descripcion del movimiento dinamicamente
				 entityManager.persist(movimiento);//persiste el movimiento
			 }
			 /*
			  * condicion que se maneja para no acumular si es 1 si es cero nada mas guarda el movimiento
			  */
			 if(movimiento.getAcumulado() == 1){
				 estadoCuenta = estadoCuentaEjb.obtenerEstadoCuentaPorMovimiento(movimiento);
				 if(estadoCuenta == null){ // si no existe estado de cuenta crear uno.
					 /*
					  *  Crear un estado de cuenta y "setear" las propiedades necesarias, tomandolas de MovimientoReaseguroDTO
					  */
					 estadoCuenta = generaEstadoCuenta(movimiento);
					 entityManager.persist(estadoCuenta);
					 LogDeMidasEJB3.log("El movimientoReaseguroDTO con id: " + movimiento.getIdMovimiento() + " no ten�a un estadoCuentaDTO asociado, el estadoCuentaDTO con id: " + estadoCuenta.getIdEstadoCuenta() + " fue creado.", Level.INFO, null);
				 }
				 
				 if (movimiento.getConceptoMovimientoDetalleDTO() == null){
					 movimiento.setConceptoMovimientoDetalleDTO(entityManager.find(ConceptoMovimientoDetalleDTO.class, new Integer(movimiento.getConceptoMovimiento())));
					 movimiento.setConceptoMovimientoDetalleDTO(entityManager.getReference(ConceptoMovimientoDetalleDTO.class,movimiento.getConceptoMovimientoDetalleDTO().getIdConceptoDetalle()));
					 entityManager.refresh(movimiento.getConceptoMovimientoDetalleDTO());
				 }
				 
				 if (movimiento.getConceptoMovimientoDetalleDTO() != null && movimiento.getConceptoMovimientoDetalleDTO().getBanderaAplicaTerminacionSiniestros() 
						 && movimiento.getTipoReaseguro() != TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){//Si es facultativo, el movimiento se acumula sin importar el estatus del siniestro
					 reporteSiniestroDTO = entityManager.find(ReporteSiniestroDTO.class, movimiento.getIdToReporteSiniestro());
					 if (reporteSiniestroDTO != null){
						 if ((reporteSiniestroDTO.getReporteEstatus().getIdTcReporteEstatus().intValue() != ReporteSiniestroDTO.REPORTE_TERMINADO.intValue() )){
							 if ((reporteSiniestroDTO.getReporteEstatus().getIdTcReporteEstatus().intValue() != ReporteSiniestroDTO.PENDIENTE_CERRADO.intValue() )){
								 aplicaTerminacionSiniestroSatisfactoria = false;
							 }
						 }
					 }else{
						 throw new RuntimeException("Reporte Siniestro no encontrado, no se puede continuar");
					 }
				 }
				 if(movimiento.getConceptoMovimientoDetalleDTO().getBanderaAcumulaEdoCta() && aplicaTerminacionSiniestroSatisfactoria){
					 /*
					  * Determinar el acumulador del estado de cuenta donde se cargar� o abonar� el movimiento
					  */
					 acumulador = acumuladorEjb.obtenerAcumuladorPorMovimientoYEstadoCuenta(movimiento, estadoCuenta);
					 if(acumulador == null){
						 /*
						  * Crear el acumulador y setear las propiedades necesarias, tomandolas del
						  * MovimientoReaseguroDTO y EstadoCuentaDTO.
						  */
						 acumulador = generaAcumulador(movimiento, estadoCuenta);
						 entityManager.persist(acumulador);
						 LogDeMidasEJB3.log("El movimientoReaseguroDTO con id: " + movimiento.getIdMovimiento() + " y estadoCuentaDTO con id: " +estadoCuenta.getIdEstadoCuenta() + 
									" no ten�a un acumuladorDTO asociado, el acumuladorDTO con id: " + acumulador.getIdAcumulador() + " fue creado.", Level.INFO, null);
					 }
					 /*
					  * Cada vez que se genere un Movimiento de Reaseguro se debe "sumar" el Monto del Movimiento 
					  * a un Acumularor que le corresponda: de Cargo para las Cuentas por Cobrar y de Abono para 
					  * las Cuentas por Pagar.
					  */

					 TipoMovimientoDTO tipoMovimientoDTO = obtenerTipoMovimientoNaturaleza(movimiento.getTipoMovimiento());
					 if(tipoMovimientoDTO.getNaturaleza() == TipoMovimientoDTO.NATURALEZA_ABONO){
						 BigDecimal abonoAcumulador = acumulador.getAbono() != null ? acumulador.getAbono() : BigDecimal.ZERO;
						 if (restarCantidad != null && restarCantidad){
							 abonoAcumulador = abonoAcumulador.subtract(movimiento.getCantidad());
						 }else{
							 abonoAcumulador = abonoAcumulador.add(movimiento.getCantidad());
						 }
						 acumulador.setAbono(abonoAcumulador);
					 }else if(tipoMovimientoDTO.getNaturaleza() == TipoMovimientoDTO.NATURALEZA_CARGO){
						 BigDecimal cargoAcumulador = acumulador.getCargo() != null ? acumulador.getCargo() : BigDecimal.ZERO;
						 if (restarCantidad != null && restarCantidad){
							 cargoAcumulador = cargoAcumulador.subtract(movimiento.getCantidad());
						 }else{
							 cargoAcumulador = cargoAcumulador.add(movimiento.getCantidad());
						 }
						 acumulador.setCargo(cargoAcumulador);
					 }
					 entityManager.merge(acumulador);
				 }else{
					 movimiento.setAcumulado(0); //Se marca el movimiento como No Acumulado
					 entityManager.merge(movimiento);
				 }
			 }
		 }catch(RuntimeException exc){
			 context.setRollbackOnly();
			 throw exc;
		 }
		 return movimiento;
	 }
	
	
	/**
	 * Registra un conjunto de movimientos reaseguro en una sola transacci�n.
	 * @param movimientos
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<MovimientoReaseguroDTO> registrarMovimientos(List<MovimientoReaseguroDTO> movimientos){
		if(movimientos != null){
			for(MovimientoReaseguroDTO movimiento : movimientos){
				registrarMovimiento(movimiento,Boolean.FALSE);
			}
		}
		
		return movimientos;
	}
	
	/**
	 * This method is pretty similar to the method MovimientoReaseguroDTO registrarMovimiento(MovimientoReaseguroDTO movimiento), the difference consist
	 * on the origin of the estadoCuenta entity, in this implementation that entity is provided, in the prior method the entity must be retrieved/created.
	 * @param movimiento
	 * @param estadoCuenta, Receives the EstadoCuentaDTO that will be used to persist the MovimientoReaseguroDTO
	 * @return MovimientoReaseguroDTO
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public MovimientoReaseguroDTO registrarMovimiento(MovimientoReaseguroDTO movimiento, EstadoCuentaDTO estadoCuenta) {
		AcumuladorDTO acumulador = new AcumuladorDTO();
		ReporteSiniestroDTO reporteSiniestroDTO;
		boolean aplicaTerminacionSiniestroSatisfactoria = true;
		try{
			/*
			 * se establece la descripcion del movimiento 
			 * y se persiste el movimiento
		    */
			validarCamposRequeridos(movimiento);//valida los campos que se requieren para el movimiento
            calcularEjercicio(movimiento);//se calcula el ejercicio
            calcularSuscripcion(movimiento);//calcula suscripcion
            establecerDescripcion(movimiento);//se establece la descripcion del movimiento dinamicamente
			entityManager.persist(movimiento);//persiste el movimiento
			/*
			 * condicion que se maneja para no acumular si es 1 si es cero nada mas guarda el
			 * movimiento  
		    */
			if(movimiento.getAcumulado() == 1){
				if (movimiento.getConceptoMovimientoDetalleDTO() == null){
					movimiento.setConceptoMovimientoDetalleDTO(entityManager.find(ConceptoMovimientoDetalleDTO.class, movimiento.getConceptoMovimiento()));
					movimiento.setConceptoMovimientoDetalleDTO(entityManager.getReference(ConceptoMovimientoDetalleDTO.class,movimiento.getConceptoMovimientoDetalleDTO().getIdConceptoDetalle()));
					entityManager.refresh(movimiento.getConceptoMovimientoDetalleDTO());
				}
				if (movimiento.getConceptoMovimientoDetalleDTO() != null && movimiento.getConceptoMovimientoDetalleDTO().getBanderaAplicaTerminacionSiniestros()
						&& movimiento.getTipoReaseguro() != TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){//Si es facultativo, el movimiento se acumula sin importar el estatus del siniestro
					 reporteSiniestroDTO = entityManager.find(ReporteSiniestroDTO.class, movimiento.getIdToReporteSiniestro());
					 if (reporteSiniestroDTO != null){
						 if (reporteSiniestroDTO.getReporteEstatus().getIdTcReporteEstatus().intValue() != ReporteSiniestroDTO.REPORTE_TERMINADO.intValue()){
							 if ((reporteSiniestroDTO.getReporteEstatus().getIdTcReporteEstatus().intValue() != ReporteSiniestroDTO.PENDIENTE_CERRADO.intValue() )){
								 aplicaTerminacionSiniestroSatisfactoria = false;
							 }
						 }
					 }else{
						 throw new RuntimeException("Reporte Siniestro no encontrado, no se puede continuar");
					 }
				 }
				if(movimiento.getConceptoMovimientoDetalleDTO().getBanderaAcumulaEdoCta() && aplicaTerminacionSiniestroSatisfactoria){
					acumulador = acumuladorEjb.obtenerAcumuladorPorMovimientoYEstadoCuenta(movimiento, estadoCuenta);
					if(acumulador == null){
						/*
						 * Crear el acumulador y setear las propiedades necesarias, tomandolas del
						 * MovimientoReaseguroDTO y EstadoCuentaDTO.
						 */
						acumulador = generaAcumulador(movimiento, estadoCuenta);
						entityManager.persist(acumulador);
					}
					/*
					 * Cada vez que se genere un Movimiento de Reaseguro se debe "sumar" el Monto del Movimiento 
					 * a un Acumularor que le corresponda: de Cargo para las Cuentas por Cobrar y de Abono para 
					 * las Cuentas por Pagar.
					 */
					TipoMovimientoDTO tipoMovimientoDTO = obtenerTipoMovimientoNaturaleza(movimiento.getTipoMovimiento());
					if(tipoMovimientoDTO.getNaturaleza() == TipoMovimientoDTO.NATURALEZA_ABONO){
						BigDecimal abonoAcumulador = BigDecimal.valueOf(0.0);
						if (acumulador.getAbono().compareTo(BigDecimal.valueOf(0.0)) == 0){
							abonoAcumulador = movimiento.getCantidad();
						}else{
							abonoAcumulador  = acumulador.getAbono().add(movimiento.getCantidad());
						}
						acumulador.setAbono(abonoAcumulador);
					}

					if(tipoMovimientoDTO.getNaturaleza() == TipoMovimientoDTO.NATURALEZA_CARGO){
						BigDecimal cargoAcumulador = BigDecimal.valueOf(0.0);
						if (acumulador.getCargo().compareTo(BigDecimal.valueOf(0.0)) == 0) {
							cargoAcumulador = movimiento.getCantidad();
						}else{
							cargoAcumulador = acumulador.getCargo().add(movimiento.getCantidad());
						}
						acumulador.setCargo(cargoAcumulador);
					}
					entityManager.merge(acumulador);
				}else{
					movimiento.setAcumulado(0); //Se marca el movimiento como No Acumulado
					entityManager.merge(movimiento);
				}
			}
	 	}catch(RuntimeException exc){
			context.setRollbackOnly();
			throw exc;
		}
 		return movimiento;
	}
 	
 	public EstadoCuentaDTO generaEstadoCuenta(MovimientoReaseguroDTO movimientoReaseguroDTO){
 		EstadoCuentaDTO estadoCuentaDTO = new EstadoCuentaDTO(); 
 		if (movimientoReaseguroDTO.getTipoReaseguro()== TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
 			if (movimientoReaseguroDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo() != null){
 				ContratoFacultativoDTO contratoFacultativoDTO = new ContratoFacultativoDTO(); 
 				contratoFacultativoDTO.setIdTmContratoFacultativo(movimientoReaseguroDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo());
 				estadoCuentaDTO.setContratoFacultativoDTO(contratoFacultativoDTO);
 			}
 		}
 		if (movimientoReaseguroDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
 			if (movimientoReaseguroDTO.getContratoCuotaParteDTO().getIdTmContratoCuotaParte() != null){
 				ContratoCuotaParteDTO contratoCuotaParteDTO = new ContratoCuotaParteDTO(); 
 				contratoCuotaParteDTO.setIdTmContratoCuotaParte(movimientoReaseguroDTO.getContratoCuotaParteDTO().getIdTmContratoCuotaParte());
 				estadoCuentaDTO.setContratoCuotaParteDTO(contratoCuotaParteDTO);
 			}
 		}
 		if (movimientoReaseguroDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
 			if (movimientoReaseguroDTO.getContratoPrimerExcedenteDTO().getIdTmContratoPrimerExcedente() != null){
 				ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO = new ContratoPrimerExcedenteDTO(); 
 				contratoPrimerExcedenteDTO.setIdTmContratoPrimerExcedente(movimientoReaseguroDTO.getContratoPrimerExcedenteDTO().getIdTmContratoPrimerExcedente());
 				estadoCuentaDTO.setContratoPrimerExcedenteDTO(contratoPrimerExcedenteDTO);
 			}
 		}
 		if (movimientoReaseguroDTO.getLineaDTO() != null){
 			LineaDTO lineaDTO = new LineaDTO(); 
 			lineaDTO.setIdTmLinea(movimientoReaseguroDTO.getLineaDTO().getIdTmLinea());
 			estadoCuentaDTO.setLineaDTO(lineaDTO);
 		}
 		if (movimientoReaseguroDTO.getSubRamo().getIdTcSubRamo() != null){
 			estadoCuentaDTO.setSubRamoDTO(movimientoReaseguroDTO.getSubRamo());
 		}
 		if (movimientoReaseguroDTO.getIdMoneda() != 0){
 			estadoCuentaDTO.setIdMoneda(movimientoReaseguroDTO.getIdMoneda());
 		}
 		if (movimientoReaseguroDTO.getReaseguradorCorredor() != null){
 			ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
 			reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(movimientoReaseguroDTO.getReaseguradorCorredor().getIdtcreaseguradorcorredor());
 			estadoCuentaDTO.setReaseguradorCorredorDTO(reaseguradorCorredorDTO);
 			estadoCuentaDTO.setCorredorDTO(movimientoReaseguroDTO.getCorredor());
 		}
 		if (movimientoReaseguroDTO.getIdPoliza() != null){
 			estadoCuentaDTO.setIdPoliza(movimientoReaseguroDTO.getIdPoliza());
 		}
 		if (movimientoReaseguroDTO.getNumeroEndoso() != null){
 			estadoCuentaDTO.setNumeroEndoso(movimientoReaseguroDTO.getNumeroEndoso());
 		}
 		if (movimientoReaseguroDTO.getEjercicio() != 0){
 			estadoCuentaDTO.setEjercicio(movimientoReaseguroDTO.getEjercicio());
 		}
 		if (movimientoReaseguroDTO.getSuscripcion() != null){
 			estadoCuentaDTO.setSuscripcion(Integer.valueOf(movimientoReaseguroDTO.getSuscripcion().intValue()));
 		}
 		estadoCuentaDTO.setTipoReaseguro(movimientoReaseguroDTO.getTipoReaseguro());
 		estadoCuentaDTO.setObservaciones("");
 		return estadoCuentaDTO;
 	}

 	public AcumuladorDTO generaAcumulador(MovimientoReaseguroDTO movimientoReaseguroDTO, EstadoCuentaDTO estadoCuentaDTO){
 		AcumuladorDTO acumuladorDTO = new AcumuladorDTO();
 		
 	       if(estadoCuentaDTO.getIdEstadoCuenta() != null){
 	    	   acumuladorDTO.setEstadoCuentaDTO(estadoCuentaDTO);
 	       }
 	       if(movimientoReaseguroDTO.getConceptoMovimiento() != 0){
 	    	  //ConceptoMovimientoDTO conceptoMovimientoDTO= new  ConceptoMovimientoDTO();
 	    	  //conceptoMovimientoDTO.setIdConceptoMovimiento(movimientoReaseguroDTO.getConceptoMovimiento());
 	    	  
 	    	 ConceptoMovimientoDTO conceptoMovimiento = null;
 	    	 ConceptoMovimientoDetalleDTO conceptoDetalle = entityManager.find(
 	    			 				ConceptoMovimientoDetalleDTO.class, 
 	    			 				movimientoReaseguroDTO.getConceptoMovimiento());
 	    	 
 	    	 /*
 	    	  * HERE
 	    	  */
 	    	int idConceptoMovimiento = 0;
 	    	String descripcion = "";
 	    	 if(conceptoDetalle.getConceptoMovimientoDTO() == null){
 	    		 String queryStr = "select model.conceptoMovimientoDTO from ConceptoMovimientoDetalleDTO model where model.idConceptoDetalle = :idConceptoDetalle";
 	    		 Query query = entityManager.createQuery(queryStr);
 	    		 query.setParameter("idConceptoDetalle", movimientoReaseguroDTO.getConceptoMovimiento());
 	    		 
 	    		 conceptoMovimiento = (ConceptoMovimientoDTO)query.getSingleResult();
 	    		 if(conceptoMovimiento != null){
 	    			 idConceptoMovimiento = conceptoMovimiento.getIdConceptoMovimiento();
 	    			 descripcion = conceptoMovimiento.getDescripcion();
 	    		}
 	    	 }else{
 	    		 conceptoMovimiento = conceptoDetalle.getConceptoMovimientoDTO();
 	    		 idConceptoMovimiento = conceptoMovimiento.getIdConceptoMovimiento();
 	    		 descripcion = conceptoMovimiento.getDescripcion();
 	    	 } 	    	 
 	    	 
 	    	//LogDeMidasEJB3.log("*Reaseguro: ConceptoMovimientoDetalleDTO="+conceptoDetalle, Level.INFO, null);
 	    	 
 	    	 if(conceptoMovimiento != null){
  	    		/*LogDeMidasEJB3.log(
 	    				"*Reaseguro: ConceptoMovimientoDetalleDTO.getDescripcion()=" + conceptoDetalle.getDescripcion(), 
 	    				Level.INFO, null); */	    		 
 	    		LogDeMidasEJB3.log(
 	    				"*Reaseguro: ConceptoMovimientoDetalleDTO.getConceptoMovimientoDTO().getIdConceptoMovimiento()=" + idConceptoMovimiento, 
 	    				Level.INFO, null); 	    		
  	    		LogDeMidasEJB3.log(
 	    				"*Reaseguro: ConceptoMovimientoDetalleDTO.getConceptoMovimientoDTO().getDescripcion()=" + descripcion, 
 	    				Level.INFO, null);
 	    		acumuladorDTO.setConceptoMovimientoDTO(conceptoMovimiento);  
 	    	 }
 	    	   
 	       }
 	       
 	      if(movimientoReaseguroDTO.getFechaRegistro() != null){
 	    	 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
 			 String fecha= sdf.format(movimientoReaseguroDTO.getFechaRegistro());
 			 int mes =  Integer.valueOf(fecha.substring(3, 5)).intValue();
 		     int ano = Integer.valueOf(fecha.substring(6, 10)).intValue();
 		 	 acumuladorDTO.setMes(mes);   
	      	 acumuladorDTO.setAnio(ano);   
	       }
 	      
 	    acumuladorDTO.setCargo(BigDecimal.valueOf(0.0));
 	    acumuladorDTO.setAbono(BigDecimal.valueOf(0.0));
 		return acumuladorDTO;
 	}
 	
    	
 	private TipoMovimientoDTO obtenerTipoMovimientoNaturaleza(int tipoMovimiento){
 		TipoMovimientoDTO  tipoMovimientoDTO = new TipoMovimientoDTO();	
 		try{
 			tipoMovimientoDTO.setIdTipoMovimiento(tipoMovimiento);
 			TipoMovimientoDTO  instance = entityManager.find(TipoMovimientoDTO.class, tipoMovimientoDTO.getIdTipoMovimiento()); 
 			tipoMovimientoDTO = instance;
 		} catch (RuntimeException re) {
 			LogDeMidasEJB3.log("fallo al obtener tipo movimiento", Level.SEVERE, re);
 			throw re;
 		}
 		return tipoMovimientoDTO;
 		}
 	
 	private void validarCamposRequeridos(MovimientoReaseguroDTO movimientoReaseguroDTO){
 	  	//se valida que si el movimiento es cuentas por pagar o cuentas por cobrar
 		//poliza, endozo y cobertura no sean null
 		if (movimientoReaseguroDTO.getTipoMovimiento() == TipoMovimientoDTO.CUENTAS_POR_PAGAR 
 				|| movimientoReaseguroDTO.getTipoMovimiento() == TipoMovimientoDTO.CUENTAS_POR_COBRAR){
 			   
 			    if (movimientoReaseguroDTO.getIdPoliza() == null){
 			        throw new RuntimeException("La poliza no pude ser nula");
   		         }
 			    if (movimientoReaseguroDTO.getNumeroEndoso() == null){
 			    	//throw new RuntimeException("El endoso no pude ser nulo");
 			    	movimientoReaseguroDTO.setNumeroEndoso(0);
 	   		     }
 			    if (movimientoReaseguroDTO.getIdCobertura() == null && movimientoReaseguroDTO.getConceptoMovimiento() != ConceptoMovimientoDetalleDTO.PRIMA_ADICIONAL && movimientoReaseguroDTO.getConceptoMovimiento() != ConceptoMovimientoDetalleDTO.CANCELACION_PRIMA_ADICIONAL){
 			    	throw new RuntimeException("La cobertura puede ser nula");
			     }
 	 	}
 	  // se valida que el movimiento tenga un linea asignada	
 	   if(movimientoReaseguroDTO.getLineaDTO() == null){
 		  throw new RuntimeException("El movimiento debe de tener una linea asignada");
	   }
 	   TipoReaseguroDTO tipoReaseguroDTO = obtenerTipoReaseguro(movimientoReaseguroDTO);
 	   LineaDTO  lineaDTO = obtenerLinea(movimientoReaseguroDTO);
 	   if(tipoReaseguroDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE &&
 			  lineaDTO.getContratoCuotaParte() == null){
 		  throw new RuntimeException("La linea no tiene asignado un contrato couta parte");
 	   }
 	   if(tipoReaseguroDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE &&
 			  lineaDTO.getContratoPrimerExcedente() == null){
 		  throw	new RuntimeException("La linea no tiene asignado un contrato primer excedente");
 	   }
 	  if(tipoReaseguroDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE &&
 			 movimientoReaseguroDTO.getContratoCuotaParteDTO() == null){
 		 throw	new RuntimeException("El movimiento no tiene asignado un contrato couta parte");
 	   }
 	  if(tipoReaseguroDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE &&
  			 movimientoReaseguroDTO.getContratoPrimerExcedenteDTO() == null){
 		 throw new RuntimeException("El movimiento no tiene asignado un contrato primer excedente");
  	   }
 	  if(tipoReaseguroDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO &&
   			 movimientoReaseguroDTO.getContratoFacultativoDTO() == null ){
 		 throw	new RuntimeException("El movimiento no tiene asignado un contrato facultativo");
   	   }
 	   if(tipoReaseguroDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE &&
   			 movimientoReaseguroDTO.getReaseguradorCorredor() == null){
 		  throw	new RuntimeException("El movimiento debe tener asignado un reasegurado");
   	   }
 	   if(movimientoReaseguroDTO.getReaseguradorCorredor() != null){
 	   ReaseguradorCorredorDTO reaseguradorCorredorDTO= obtenerReaseguradorCorredor(movimientoReaseguroDTO);
  	     if(reaseguradorCorredorDTO.getTipo() == "0"){
  	    	throw new RuntimeException("Para corredores no se permite registrar movimientos");
  	     }
 	   }
 	 	if(((movimientoReaseguroDTO.getConceptoMovimiento() == ConceptoMovimientoDTO.SINIESTROS) ||
 	 			(movimientoReaseguroDTO.getConceptoMovimiento() == ConceptoMovimientoDTO.GASTOS_AJUSTE) ||
 	 			(movimientoReaseguroDTO.getConceptoMovimiento() == ConceptoMovimientoDTO.SALVAMENTO)) && (movimientoReaseguroDTO.getIdToReporteSiniestro() == null)){
 	 		throw new RuntimeException("Debe existir un Reporte de siniestro");
 		}
 	   
    }
 	
 	private void establecerDescripcion(MovimientoReaseguroDTO movimientoReaseguroDTO){
 	     TipoReaseguroDTO tipoReaseguroDTO = obtenerTipoReaseguro(movimientoReaseguroDTO);
  	     LineaDTO  lineaDTO = obtenerLinea(movimientoReaseguroDTO);
  	      ReaseguradorCorredorDTO reaseguradorCorredorDTO = null;
  	     String reasegurador = "";
  	     if(movimientoReaseguroDTO.getReaseguradorCorredor() != null){
  	          reaseguradorCorredorDTO = obtenerReaseguradorCorredor(movimientoReaseguroDTO);
  	          reasegurador += " del Reasegurador "+ reaseguradorCorredorDTO.getNombrecorto(); 
		 }else{
			  reasegurador += " en " + lineaDTO.getSubRamo().getDescription();
  	     }
  	     StringBuilder descripcion = new StringBuilder();
  	     if (movimientoReaseguroDTO.getTipoMovimiento() == TipoMovimientoDTO.CUENTAS_POR_PAGAR
 	          || movimientoReaseguroDTO.getTipoMovimiento() == TipoMovimientoDTO.CUENTAS_POR_COBRAR){
 	        
 	       if (tipoReaseguroDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
 	    	  ContratoCuotaParteDTO contratoCuotaParteDTO  = obtenerContratoCoutaParte(movimientoReaseguroDTO);
 	    	   descripcion.append(obtenerDescripcionConcepto(movimientoReaseguroDTO)) 
 	    	      .append(reasegurador)
 	    	      .append(" del Contrato Cuota Parte " + contratoCuotaParteDTO.getFolioContrato())
 	    	      .append(" de la Poliza " + movimientoReaseguroDTO.getIdPoliza())
 	    	      .append(" del endoso " + movimientoReaseguroDTO.getNumeroEndoso())
 	    	      .append(" de la cobertura " + movimientoReaseguroDTO.getIdCobertura());
 	          }
 	       if (tipoReaseguroDTO.getTipoReaseguro()  == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
 	    	  ContratoPrimerExcedenteDTO  contratoPrimerExcedenteDTO = obtenerContratoPrimerExcedente(movimientoReaseguroDTO);
 	    	   descripcion.append(obtenerDescripcionConcepto(movimientoReaseguroDTO)) 
    	         .append(" del Reasegurador " + reaseguradorCorredorDTO.getNombrecorto()) 
    	         .append(" del Contrato Primer Excedente " + contratoPrimerExcedenteDTO.getFolioContrato())
    	         .append(" de la Poliza " + movimientoReaseguroDTO.getIdPoliza())
    	         .append(" del endoso " + movimientoReaseguroDTO.getNumeroEndoso())
    	         .append(" de la cobertura " + movimientoReaseguroDTO.getIdCobertura());
 	        }
 	       if (tipoReaseguroDTO.getTipoReaseguro()  == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
 	    	    descripcion.append(obtenerDescripcionConcepto(movimientoReaseguroDTO))
	    	    .append(" del Contrato Facultativo "+ movimientoReaseguroDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo())
	    	    .append(reasegurador)
	    	    .append(" de la Poliza " +  movimientoReaseguroDTO.getIdPoliza())
	    	    .append(" del endoso " + movimientoReaseguroDTO.getNumeroEndoso())
	    	    .append(movimientoReaseguroDTO.getIdCobertura() != null ? " de la cobertura " + movimientoReaseguroDTO.getIdCobertura() : "");
	        } 
 	      if (tipoReaseguroDTO.getTipoReaseguro()  == TipoReaseguroDTO.TIPO_RETENCION){
	    	   descripcion.append(obtenerDescripcionConcepto(movimientoReaseguroDTO)) 
 	            .append(" del Retencion "+ lineaDTO.getSubRamo().getDescription()) 
 	            .append(" de la Poliza " +  movimientoReaseguroDTO.getIdPoliza())
 	            .append(" del endoso " + movimientoReaseguroDTO.getNumeroEndoso())
 	            .append(" de la cobertura " + movimientoReaseguroDTO.getIdCobertura());
	       } 
 	    }
 	if (movimientoReaseguroDTO.getTipoMovimiento() == TipoMovimientoDTO.PAGO
	          || movimientoReaseguroDTO.getTipoMovimiento() == TipoMovimientoDTO.COBRO){
 		
 	     if (tipoReaseguroDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
 	    	ContratoCuotaParteDTO contratoCuotaParteDTO  = obtenerContratoCoutaParte(movimientoReaseguroDTO);
 	    	   descripcion.append(obtenerDescripcionConcepto(movimientoReaseguroDTO)) 
	    	      .append(reasegurador) 
	    	      .append(" del Contrato Cuota Parte "+ contratoCuotaParteDTO.getFolioContrato());
 	     }
         if (tipoReaseguroDTO.getTipoReaseguro()  == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
        	 ContratoPrimerExcedenteDTO  contratoPrimerExcedenteDTO = obtenerContratoPrimerExcedente(movimientoReaseguroDTO);
        	   descripcion.append(obtenerDescripcionConcepto(movimientoReaseguroDTO)) 
  	             .append(" del Reasegurador "+ reaseguradorCorredorDTO.getNombrecorto()) 
  	             .append(" del Contrato Primer Excedente "+ contratoPrimerExcedenteDTO.getFolioContrato());
  	     }
         if (tipoReaseguroDTO.getTipoReaseguro()  == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
        	    descripcion.append(obtenerDescripcionConcepto(movimientoReaseguroDTO)) 
  	            .append(reasegurador) 
  	            .append(" del Contrato Facultativo "+ movimientoReaseguroDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo());
  	     }
         if (tipoReaseguroDTO.getTipoReaseguro()  == TipoReaseguroDTO.TIPO_RETENCION){
	    	   descripcion.append(obtenerDescripcionConcepto(movimientoReaseguroDTO)) 
	           .append(" de Retencion "+ lineaDTO.getSubRamo().getDescription()); 
	       } 	
         }
 	    movimientoReaseguroDTO.setDescripcion(descripcion.toString());  
  	}
 	
 	private String obtenerDescripcionConcepto(MovimientoReaseguroDTO movimientoReaseguroDTO){

 		ReporteSiniestroDTO   reporteSiniestroDTO = null;
 		String descripcionConcepto = "";
 		
 		if(movimientoReaseguroDTO.getIdToReporteSiniestro() != null){
 		  reporteSiniestroDTO = obtenerReporteSiniestro(movimientoReaseguroDTO);
 		}

 		ConceptoMovimientoDetalleDTO conceptoMovimientoDetalleDTO = obtenerConceptoMovimiento(movimientoReaseguroDTO);
 		if(conceptoMovimientoDetalleDTO != null){
 	 	 	descripcionConcepto += conceptoMovimientoDetalleDTO.getDescripcion();
 	 	 	
 	 	 	if(reporteSiniestroDTO != null){
 	 	 		//movimientoReaseguroDTO.getConceptoMovimiento()
 	 	 		
 	 	 		if(conceptoMovimientoDetalleDTO.getConceptoMovimientoDTO() != null){
 	 	 	 	  	if(conceptoMovimientoDetalleDTO.getConceptoMovimientoDTO().getIdConceptoMovimiento() == ConceptoMovimientoDTO.SINIESTROS ||
 	 	 	 	 	  		conceptoMovimientoDetalleDTO.getConceptoMovimientoDTO().getIdConceptoMovimiento() == ConceptoMovimientoDTO.GASTOS_AJUSTE ||
 	 	 	 	 	  		conceptoMovimientoDetalleDTO.getConceptoMovimientoDTO().getIdConceptoMovimiento() == ConceptoMovimientoDTO.SALVAMENTO){
 	 	 	 	 	 		    descripcionConcepto += " del siniestro " + reporteSiniestroDTO.getNumeroReporte(); 
 	 	 	 	 	  	}		 	 			
 	 	 		}
 	 	 		
 		
 	 	 	}			
 		}

 	   return descripcionConcepto;	
 	}
 	
    private void calcularEjercicio(MovimientoReaseguroDTO movimientoReaseguroDTO){
        LineaDTO  lineaDTO = obtenerLinea(movimientoReaseguroDTO);
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String fechaInicial= sdf.format(lineaDTO.getFechaInicial());  
		movimientoReaseguroDTO.setEjercicio(Integer.valueOf(fechaInicial.substring(6, 10)).intValue());	
    }
    
    public BigDecimal calcularSuscripcion(MovimientoReaseguroDTO movimientoReaseguroDTO){
	 	int formaPago = 0;
		Date fechaInicio = new Date();
		Date fechaFin = new Date();
		int idSuscripcion = 0;
		Date fechaMovimiento = movimientoReaseguroDTO.getFechaRegistro();

		List<SuscripcionDTO> suscripcionDTOList = new ArrayList<SuscripcionDTO>();
	  	TipoReaseguroDTO tipoReaseguroDTO = obtenerTipoReaseguro(movimientoReaseguroDTO);
	  	
		if (tipoReaseguroDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			ContratoCuotaParteDTO contratoCuotaParteDTO  = movimientoReaseguroDTO.getContratoCuotaParteDTO();//obtenerContratoCoutaParte(movimientoReaseguroDTO);
			formaPago =  Integer.valueOf(contratoCuotaParteDTO.getFormaPago().intValue());
			fechaInicio = contratoCuotaParteDTO.getFechaInicial();
			fechaFin = contratoCuotaParteDTO.getFechaFinal();
	 	}
		if (tipoReaseguroDTO.getTipoReaseguro()  == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
			ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO = movimientoReaseguroDTO.getContratoPrimerExcedenteDTO();//obtenerContratoPrimerExcedente(movimientoReaseguroDTO);
			formaPago =  Integer.valueOf(contratoPrimerExcedenteDTO.getFormaPago().intValue());
			fechaInicio = contratoPrimerExcedenteDTO.getFechaInicial();
			fechaFin = contratoPrimerExcedenteDTO.getFechaFinal();
 		}//contratoFacultativoDTO.setIdFormaDePago(new BigDecimal(LineaDTO.FORMA_PAGO_ANUAL));
		if (tipoReaseguroDTO.getTipoReaseguro()  == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
			ContratoFacultativoDTO contratoFacultativoDTO = movimientoReaseguroDTO.getContratoFacultativoDTO();//obtenerContratoFacultativo(movimientoReaseguroDTO);
			//Se coloca por defecto la forma de pago ANUAL, ya que ahora no se captura forma de pago a nivel contrato, sino que 
			//se definen cantidad de exhibiciones a nivel cobertura.
			//formaPago =  Integer.valueOf(contratoFacultativoDTO.getIdFormaDePago().intValue());
			formaPago =  LineaDTO.FORMA_PAGO_ANUAL;
			fechaInicio = contratoFacultativoDTO.getFechaInicial();
			fechaFin = contratoFacultativoDTO.getFechaFinal();
 		}
		if (tipoReaseguroDTO.getTipoReaseguro()  == TipoReaseguroDTO.TIPO_RETENCION){
			LineaDTO  lineaDTO = movimientoReaseguroDTO.getLineaDTO();//obtenerLinea(movimientoReaseguroDTO);
			fechaInicio = lineaDTO.getFechaInicial();
			fechaFin = lineaDTO.getFechaFinal();
	 		formaPago =  LineaDTO.FORMA_PAGO_RETENCION_PURA;
	 	}
	 	CalculoSuscripcionesDTO calculoSuscripcionesDTO = new CalculoSuscripcionesDTO();
		calculoSuscripcionesDTO.setFechaInicial(fechaInicio);
		calculoSuscripcionesDTO.setFechaFinal(fechaFin);
		calculoSuscripcionesDTO.setFormaPago(BigDecimal.valueOf(Integer.valueOf(formaPago)));
		calculoSuscripcionesDTO.setEjercicio(movimientoReaseguroDTO.getEjercicio());
		if (Utilerias.removerHorasMinutosSegundos(fechaMovimiento).compareTo(Utilerias.removerHorasMinutosSegundos(fechaFin)) > 0){
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(fechaMovimiento);
			calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
			calculoSuscripcionesDTO.setFechaFinal(calendar.getTime());
		}
		suscripcionDTOList = calculoSuscripcionesDTO.getListaSuscripciones();
 		if (suscripcionDTOList.size() > 0){
	        for(SuscripcionDTO suscripcionDTO : suscripcionDTOList){
	        	if(Utilerias.fechaEntreRango(fechaMovimiento, suscripcionDTO.getFechaInicial(), suscripcionDTO.getFechaFinal())){
	        		idSuscripcion =  suscripcionDTO.getIdSuscripcion(); 
	        	}
	         }
 	 	}
 		if (idSuscripcion == 0 && tipoReaseguroDTO.getTipoReaseguro()  == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
 			idSuscripcion = 1; 			
 		}
 		if(idSuscripcion == 0){
 			
 			throw new RuntimeException("No existe suscripcion");
 		}else{
 			movimientoReaseguroDTO.setSuscripcion(BigDecimal.valueOf(Integer.valueOf(idSuscripcion)));
 			return BigDecimal.valueOf(Integer.valueOf(idSuscripcion));
 		}
   	}
    
    private ConceptoMovimientoDetalleDTO obtenerConceptoMovimiento(MovimientoReaseguroDTO movimientoReaseguroDTO ){
    	ConceptoMovimientoDetalleDTO  conceptoMovimientoDetalleDTO = new ConceptoMovimientoDetalleDTO();	
    	try{
    		String queryStr = "SELECT model FROM ConceptoMovimientoDetalleDTO model WHERE model.idConceptoDetalle = :idConceptoDetalle";
    		Query query = entityManager.createQuery(queryStr);
    		query.setParameter("idConceptoDetalle", movimientoReaseguroDTO.getConceptoMovimiento());
    		query.setHint(QueryHints.REFRESH, HintValues.TRUE);

    		conceptoMovimientoDetalleDTO = (ConceptoMovimientoDetalleDTO)query.getSingleResult();
    		entityManager.refresh(conceptoMovimientoDetalleDTO);
    	}catch(RuntimeException re){
    		LogDeMidasEJB3.log("Fall� al obtener concepto movimiento", Level.SEVERE, re);
    		throw re; 	     		
    	}

    	return conceptoMovimientoDetalleDTO;
    }
 	
	private TipoReaseguroDTO obtenerTipoReaseguro(MovimientoReaseguroDTO movimientoReaseguroDTO ){
		TipoReaseguroDTO  tipoReaseguroDTO = new TipoReaseguroDTO();	
 		   try{
 			  tipoReaseguroDTO.setTipoReaseguro(movimientoReaseguroDTO.getTipoReaseguro());
 			  TipoReaseguroDTO  instance = entityManager.find(TipoReaseguroDTO.class, tipoReaseguroDTO.getTipoReaseguro()); 
 			tipoReaseguroDTO = instance;
 	     	} catch (RuntimeException re) {
 	    				LogDeMidasEJB3.log("fallo al obtener concepto movimiento", Level.SEVERE, re);
 	            throw re;
 	      }
 	      return tipoReaseguroDTO;
 		}
	public LineaDTO obtenerLinea(MovimientoReaseguroDTO movimientoReaseguroDTO){
		    LineaDTO  lineaDTO = new LineaDTO();
			try{
			lineaDTO.setIdTmLinea(movimientoReaseguroDTO.getLineaDTO().getIdTmLinea());	
			LineaDTO  instance = entityManager.find(LineaDTO.class, lineaDTO.getIdTmLinea()); 
			lineaDTO  = instance;
	       } catch (RuntimeException re) {
	    				LogDeMidasEJB3.log("fallo al obtener la  linea", Level.SEVERE, re);
	            throw re;
	      }
	      return lineaDTO;
		}
	public ReaseguradorCorredorDTO obtenerReaseguradorCorredor(MovimientoReaseguroDTO movimientoReaseguroDTO){
		ReaseguradorCorredorDTO  reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
		try{
		reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(movimientoReaseguroDTO.getReaseguradorCorredor().getIdtcreaseguradorcorredor());	
		ReaseguradorCorredorDTO  instance = entityManager.find(ReaseguradorCorredorDTO.class, reaseguradorCorredorDTO.getIdtcreaseguradorcorredor()); 
		reaseguradorCorredorDTO  = instance;
       } catch (RuntimeException re) {
    				LogDeMidasEJB3.log("fallo al obtener Reasegurador Corredor", Level.SEVERE, re);
            throw re;
      }
      return reaseguradorCorredorDTO;
	}
	
	public ContratoPrimerExcedenteDTO obtenerContratoPrimerExcedente(MovimientoReaseguroDTO movimientoReaseguroDTO){
		ContratoPrimerExcedenteDTO  contratoPrimerExcedenteDTO = new ContratoPrimerExcedenteDTO();
		try{
		contratoPrimerExcedenteDTO.setIdTmContratoPrimerExcedente(movimientoReaseguroDTO.getContratoPrimerExcedenteDTO().getIdTmContratoPrimerExcedente());	
		ContratoPrimerExcedenteDTO  instance = entityManager.find(ContratoPrimerExcedenteDTO.class, contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente()); 
		contratoPrimerExcedenteDTO  = instance;
       } catch (RuntimeException re) {
    				LogDeMidasEJB3.log("fallo al obtener la  linea", Level.SEVERE, re);
            throw re;
      }
      return contratoPrimerExcedenteDTO;
	}
	
	public ContratoCuotaParteDTO obtenerContratoCoutaParte(MovimientoReaseguroDTO movimientoReaseguroDTO){
		ContratoCuotaParteDTO  contratoCuotaParteDTO = new ContratoCuotaParteDTO();
		try{
	    contratoCuotaParteDTO.setIdTmContratoCuotaParte(movimientoReaseguroDTO.getContratoCuotaParteDTO().getIdTmContratoCuotaParte());	
		ContratoCuotaParteDTO  instance = entityManager.find(ContratoCuotaParteDTO.class, contratoCuotaParteDTO.getIdTmContratoCuotaParte()); 
		contratoCuotaParteDTO  = instance;
       } catch (RuntimeException re) {
    				LogDeMidasEJB3.log("fallo al obtener la  contrato couta parte ", Level.SEVERE, re);
            throw re;
      }
      return contratoCuotaParteDTO;
	}

	public ContratoFacultativoDTO obtenerContratoFacultativo(MovimientoReaseguroDTO movimientoReaseguroDTO){
		ContratoFacultativoDTO  contratoFacultativoDTO = new ContratoFacultativoDTO();
		try{
		contratoFacultativoDTO.setIdTmContratoFacultativo(movimientoReaseguroDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo());	
	    ContratoFacultativoDTO  instance = entityManager.find(ContratoFacultativoDTO.class, contratoFacultativoDTO.getIdTmContratoFacultativo()); 
		contratoFacultativoDTO  = instance;
       } catch (RuntimeException re) {
    				LogDeMidasEJB3.log("fallo al obtener la  contrato couta parte ", Level.SEVERE, re);
            throw re;
      }
      return contratoFacultativoDTO;
	}
	
	public ReporteSiniestroDTO obtenerReporteSiniestro(MovimientoReaseguroDTO movimientoReaseguroDTO){
		ReporteSiniestroDTO  reporteSiniestroDTO = new ReporteSiniestroDTO();
	 	try{
	 	//reporteSiniestroDTO.setIdToReporteSiniestro(movimientoReaseguroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
	 	reporteSiniestroDTO.setIdToReporteSiniestro(movimientoReaseguroDTO.getIdToReporteSiniestro());	
	 	ReporteSiniestroDTO  instance = entityManager.find(ReporteSiniestroDTO.class, reporteSiniestroDTO.getIdToReporteSiniestro());
	 	if(instance != null)
	 		entityManager.refresh(instance);
	 	reporteSiniestroDTO  = instance;
       } catch (RuntimeException re) {
    				LogDeMidasEJB3.log("fallo al obtener la  contrato couta parte ", Level.SEVERE, re);
            throw re;
      }
      return reporteSiniestroDTO;
	}
	
	/**
	 * 
	 * @param movimientos
	 */
	/*public void marcarComoContabilizados(List<MovimientoReaseguroDTO> movimientos){
		if(movimientos != null){
			for(MovimientoReaseguroDTO movimiento : movimientos){
				movimiento.setContabilizado(1);
				movimiento.setFechaContabilizado(Calendar.getInstance().getTime());
				entityManager.merge(movimiento);
			}
		}
	}*/
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void registrarContabilidad(String identificador, String claveModulo, List<MovimientoReaseguroDTO> listaMovimientos) {
		try{
			List<AsientoContableDTO> asientos = asientoContableFacade.contabilizaMovimientos(
					identificador, // aqui va identificador
					claveModulo,
					"MIDAS");
			LogDeMidasEJB3.log("La Notificaci�n a Contabilidad fue Exitosa bajo la referencia : " + identificador ,Level.INFO, null);
			if(asientos != null && !asientos.isEmpty()){
				if(listaMovimientos != null){
					for(MovimientoReaseguroDTO movimiento : listaMovimientos){
						movimiento.setContabilizado(1);
						movimiento.setFechaContabilizado(Calendar.getInstance().getTime());
						movimiento.setComodin(claveModulo + "-" + identificador);
						entityManager.merge(movimiento);
						LogDeMidasEJB3.log("Se Actualizo el Movimiento ID: "+ movimiento.getIdMovimiento() + " de Reaseguro en resultado de la Contabilidad, bajo la referencia: " + identificador ,Level.INFO, null);
					}
					
				} else {
					LogDeMidasEJB3.log("No se definio la Lista de Movimientos Reaseguro para poder acutalzar el resultado de la Contabilidad, bajo la referencia: " + identificador ,Level.SEVERE, null);
				}
			}else {
				LogDeMidasEJB3.log("La Contabilidad No retorno ningun asiento contable para acuse de recibo de Contabilidad bajo la referencia: " + identificador ,Level.SEVERE, null);				
			}
			
		} catch (Exception exc){
			LogDeMidasEJB3.log("Contabilidad: " + exc.getMessage(), Level.SEVERE, exc);
			throw new RuntimeException("No se pudo contabilizar. Identificador: " + identificador
					+ " Excepcion: " + exc.getMessage());
		}

	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<AsientoContableDTO> registrarContabilidad(String identificador, String claveModulo) {
		try{
			List<AsientoContableDTO> asientos = asientoContableFacade.contabilizaMovimientos(identificador, claveModulo,"MIDAS");
			LogDeMidasEJB3.log("La Notificaci�n a Contabilidad fue Exitosa bajo la referencia : " + identificador ,Level.INFO, null);
			if(asientos == null || asientos.isEmpty()){
				LogDeMidasEJB3.log("La Contabilidad No retorno ningun asiento contable para acuse de recibo de Contabilidad bajo la referencia: " + identificador ,Level.SEVERE, null);
			}
			return asientos;
		} catch (Exception exc){
			LogDeMidasEJB3.log("Contabilidad: " + exc.getMessage(), Level.SEVERE, exc);
			throw new RuntimeException("No se pudo contabilizar. Identificador: " + identificador+ " Excepcion: " + exc.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<String> obtenerMovimientosIngresosPendientesPorContabilizar(){
		List<String> lista = null;

		   try{
			   StringBuffer queryBuffer = new StringBuffer();
			   queryBuffer.append("select distinct model.comodin from MovimientoReaseguroDTO model ");
			   queryBuffer.append("where model.contabilizado = 0 and model.comodin like 'INGRESO%' ");
			   
			   Query query = entityManager.createQuery(queryBuffer.toString());
			   
			   lista = (List<String>)query.getResultList();

	 	   } catch (RuntimeException re) {
	 	    	LogDeMidasEJB3.log("fall� al obtener concepto movimiento", Level.SEVERE, re);
	 	        throw re;
	 	  }		
		
		return lista;		
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void registrarContabilidad(List<String> listaClaveRemesas, String usuario){
		if(usuario == null){
			usuario = "MIDAS";
		}
		
		if(listaClaveRemesas != null){
			for(String claveRemesa : listaClaveRemesas){
				try{
					/*
					 * 1.- TODO:REMESA Invocar Interface RegistrarRemesa(claveRemesa,usuario)
					 */
					
					String[] tokens = claveRemesa.split("-");
					
					if(tokens != null && tokens.length == 2){
						String idRemesa = tokens[1];
						remesaFacadeRemote.registraRemesa(new BigDecimal(idRemesa), usuario);
					}
			
					
					/*
					 * 2.- obtener movimientos con la claveRemesa para cambiar estatus de contabilizado a 1
					 */
					List<MovimientoReaseguroDTO> movimientosRemesa = obtenerMovimientosRemesa(claveRemesa);
					
					if(movimientosRemesa != null){
						for(MovimientoReaseguroDTO movimiento : movimientosRemesa){
							movimiento.setContabilizado(1);
							movimiento.setFechaContabilizado(Calendar.getInstance().getTime());
							entityManager.merge(movimiento);
						}
					}
										
				}catch(Exception exc){
					LogDeMidasEJB3.log("fall� al contabilizar remesa " + claveRemesa, Level.SEVERE, exc);					
				}

			} // for(String claveRemesa : listaClaveRemesas)
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private List<MovimientoReaseguroDTO> obtenerMovimientosRemesa(String claveRemesa){
			List<MovimientoReaseguroDTO> movimientos = null;
		
		   try{
			   StringBuffer queryBuffer = new StringBuffer();
			   queryBuffer.append("select model from MovimientoReaseguroDTO model ");
			   queryBuffer.append("where model.comodin = :claveRemesa");
			   
			   Query query = entityManager.createQuery(queryBuffer.toString());
			   query.setParameter("claveRemesa", claveRemesa);
			   
			   movimientos = (List<MovimientoReaseguroDTO>)query.getResultList();

	 	   } catch (RuntimeException re) {
	 	    	LogDeMidasEJB3.log("fall� al obtener movimientos remesas", Level.SEVERE, re);
	 	        throw re;
	 	   }	
	 	   
	 	  return movimientos;
		
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@SuppressWarnings("unchecked")
	public void deshacerMovimientosPorPoliza(PolizaSoporteDanosDTO polizaSoporteDanosDTO) {
		try{
			LogDeMidasEJB3.log("Entering to deshacerMovimientosPorPoliza in " + this.getClass().getName() + " with idToPoliza = " + polizaSoporteDanosDTO.getIdToPoliza(), Level.INFO, null);
			List<MovimientoReaseguroDTO> movimientoReaseguroDTOList;
			AcumuladorDTO acumuladorDTO;
			int naturaleza = 0;
			ReporteSiniestroDTO reporteSiniestroDTO;
			boolean aplicaTerminacionSiniestroSatisfactoria;
			BigDecimal abonoAcumulador;
			BigDecimal cargoAcumulador;
			String queryString = "SELECT model FROM MovimientoReaseguroDTO model WHERE model.idPoliza = :idPoliza";

			Query query = entityManager.createQuery(queryString);
			query.setParameter("idPoliza", polizaSoporteDanosDTO.getIdToPoliza());
			movimientoReaseguroDTOList = query.getResultList();
			
			queryString = "SELECT model.idConcepto FROM ConceptoIngresoReaseguroDTO model";
			query = null;
			query =	entityManager.createQuery(queryString);
			List<Byte> idConceptoList = query.getResultList();
			
			List<EstadoCuentaDTO> estadoCuentaDTOList = estadoCuentaEjb.findByProperty("idPoliza", polizaSoporteDanosDTO.getIdToPoliza());
			for (MovimientoReaseguroDTO movimientoReaseguroDTO : movimientoReaseguroDTOList) {
				aplicaTerminacionSiniestroSatisfactoria = true;
				if (idConceptoList.contains(new Byte(String.valueOf(movimientoReaseguroDTO.getConceptoMovimientoDetalleDTO().getIdConceptoDetalle())))){
					throw new RuntimeException("The movimientoReaseguroDTO with id: " + movimientoReaseguroDTO.getIdMovimiento() + " is from an income (Ingreso-Reaseguro) and cannot be deleted.");
				}
				
				if (movimientoReaseguroDTO.getConceptoMovimientoDetalleDTO().getIdConceptoDetalle() == ConceptoMovimientoDetalleDTO.CARGO_DE_REMESAS_SALDOS){
					throw new RuntimeException("The movimientoReaseguroDTO with id: " + movimientoReaseguroDTO.getIdMovimiento() + " is from an outcome (Egreso-Reaseguro) and cannot be deleted.");
				}
				
				if (movimientoReaseguroDTO.getConceptoMovimientoDetalleDTO() == null){
					movimientoReaseguroDTO.setConceptoMovimientoDetalleDTO(entityManager.find(ConceptoMovimientoDetalleDTO.class, movimientoReaseguroDTO.getConceptoMovimiento()));
				}
				
				if (movimientoReaseguroDTO.getConceptoMovimientoDetalleDTO() != null && movimientoReaseguroDTO.getConceptoMovimientoDetalleDTO().getBanderaAplicaTerminacionSiniestros()){
					reporteSiniestroDTO = entityManager.find(ReporteSiniestroDTO.class, movimientoReaseguroDTO.getIdToReporteSiniestro());
					if (reporteSiniestroDTO != null){
						if (reporteSiniestroDTO.getEstatusSiniestro().getIdTcEstatusSiniestro().intValue() != ReporteSiniestroDTO.REPORTE_TERMINADO.intValue()){
							aplicaTerminacionSiniestroSatisfactoria = false;
						}
					}else{
						throw new RuntimeException("Error, ReporteSiniestroDTO was not found! for movimientoReaseguroDTO with id: " + movimientoReaseguroDTO.getIdMovimiento());
					}
				}				
				
				if (movimientoReaseguroDTO.getConceptoMovimientoDetalleDTO().getBanderaAcumulaEdoCta() && aplicaTerminacionSiniestroSatisfactoria){
					for (EstadoCuentaDTO estadoCuentaDTO : estadoCuentaDTOList) {
						acumuladorDTO = acumuladorEjb.obtenerAcumuladorPorMovimientoYEstadoCuenta(movimientoReaseguroDTO, estadoCuentaDTO);
						if (acumuladorDTO != null){
//							if (acumuladorDTO.getEstadoCuentaDTO().getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO && polizaSoporteDanosDTO.getNumeroUltimoEndoso() == 0){
//								LogDeMidasEJB3.log("Removing acumuladorDTO with id:" + acumuladorDTO.getIdAcumulador(), Level.INFO, null);
//								entityManager.remove(acumuladorDTO);
//							}else{
								naturaleza = movimientoReaseguroDTO.getTipoMovimientoDTO().getNaturaleza();
								if (naturaleza == TipoMovimientoDTO.NATURALEZA_ABONO){
									abonoAcumulador = acumuladorDTO.getAbono();
									abonoAcumulador = abonoAcumulador.subtract(movimientoReaseguroDTO.getCantidad());
									acumuladorDTO.setAbono(abonoAcumulador);
								}else if (naturaleza == TipoMovimientoDTO.NATURALEZA_CARGO){
									cargoAcumulador = acumuladorDTO.getCargo();
									cargoAcumulador = cargoAcumulador.subtract(movimientoReaseguroDTO.getCantidad());
									acumuladorDTO.setCargo(cargoAcumulador);
								}
								LogDeMidasEJB3.log("Updating acumuladorDTO with id:" + acumuladorDTO.getIdAcumulador(), Level.INFO, null);
								entityManager.merge(acumuladorDTO);
//							}
						}
					}
				}
				LogDeMidasEJB3.log("Removing movimientoReaseguroDTO with id:" + movimientoReaseguroDTO.getIdMovimiento(), Level.INFO, null);
				entityManager.remove(movimientoReaseguroDTO);
			}
			
//			LogDeMidasEJB3.log("Removing all estadoCuentaDTO's with idPoliza: " + polizaSoporteDanosDTO.getIdToPoliza() + " and numeroEndoso Zero", Level.INFO, null);
//			queryString = "DELETE FROM EstadoCuentaDTO model WHERE model.idPoliza = :idPoliza " +
//			"AND model.tipoReaseguro = :tipoReaseguro AND model.numeroEndoso = 0";
//			query = null;
//			query = entityManager.createQuery(queryString);
//			query.setParameter("idPoliza", polizaSoporteDanosDTO.getIdToPoliza());
//			query.setParameter("tipoReaseguro", TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);
//			query.executeUpdate();
			
			LogDeMidasEJB3.log("Method deshacerMovimientosPorPoliza finalized correctly.", Level.INFO, null);
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Method deshacerMovimientosPorPoliza failed, aplying Rollback...", Level.SEVERE, null);
			e.printStackTrace();
			context.setRollbackOnly();
			throw e;
		}
	}

	public int getMaxId(Integer mes,Integer anio) {
		String queryString = null;
		Calendar calendarInicial = null, calendarFinal = null;
		boolean validarPorRango = false;
		queryString = "SELECT MAX(model.idMovimiento) FROM MovimientoReaseguroDTO model";
		if(mes != null && anio != null){
			if(mes.intValue() > 0 && mes.intValue()< 12 && anio.intValue() > 2008){
				calendarInicial = Calendar.getInstance();
				calendarInicial.set(anio,mes-1,0,0,0,0);
				calendarFinal = Calendar.getInstance();
				calendarFinal.set(anio, mes-1, 0, 23, 59, 59);
				calendarFinal.add(Calendar.MONTH, 1);
				calendarFinal.add(Calendar.DATE, -1);
				queryString = "SELECT COUNT(model.idMovimiento) FROM MovimientoReaseguroDTO model where model.fechaRegistro BETWEEN :fechaInicial and :fechaFinal";
				validarPorRango = true;
			}
		}
		Query query = entityManager.createQuery(queryString);
		if(validarPorRango){
			query.setParameter("fechaInicial", calendarInicial.getTime());
			query.setParameter("fechaFinal", calendarFinal.getTime());
		}
		int idMaximoMovimientos = 0;
		Object resultado = query.getSingleResult();
		if(resultado instanceof BigDecimal){
			idMaximoMovimientos = ((BigDecimal)resultado).intValue();
		}else if(resultado instanceof Long){
			idMaximoMovimientos = ((Long)resultado).intValue();
		}else if(resultado instanceof Double){
			idMaximoMovimientos = ((Double)resultado).intValue();
		}else if(resultado instanceof Integer){
			idMaximoMovimientos = ((Integer)resultado).intValue();
		}else if(resultado instanceof Float){
			idMaximoMovimientos = ((Float)resultado).intValue();
		}else{
			idMaximoMovimientos = Integer.valueOf(resultado.toString()).intValue();
		}
		return idMaximoMovimientos;
	}

	public int obtenerCantidadMovimientosSiniestrosNoAcumulables(){
		String queryString = null;
		queryString = "SELECT COUNT(model.idMovimiento) FROM MovimientoReaseguroDTO model where model.idToReporteSiniestro is not null and " +
				"model.conceptoMovimientoDetalleDTO.banderaAcumulaEdoCta = 0 and model.acumulado = 0 ";
		Query query = entityManager.createQuery(queryString);
		int idMaximoMovimientos = ((Long)query.getSingleResult()).intValue();
		return idMaximoMovimientos;
	}
	
	public int obtenerCantidadMovimientosSiniestrosConFechaIncorrecta(){
		LogDeMidasEJB3.log("Consultando cantidad de movimientos de reaseguro provenientes de siniestros con fecha de registro incorrecta.", Level.INFO, null);
		try{
			String queryString = "select count(mov.idtomovimiento) from MIDAS.todistribucionmovsiniestro sin, MIDAS.tomovimientoreaseguro mov  where (" +
					" mov.idtomovimientosiniestro = sin.idmovimientosiniestro and mov.idtoreportesiniestro = sin.idtoreportesiniestro" +
					" and sin.idtoseccion = mov.idtoseccion and sin.idtocobertura = mov.idtocobertura" +
					" and nvl(sin.numeroinciso,0) = nvl(mov.idtoinciso,0) and nvl(sin.numerosubinciso,0) = nvl(mov.idtosubinciso,0)" +
					" and sin.idtcmoneda = mov.idmoneda" +
					" and sin.numeroendoso = mov.numeroendoso" +
					" and mov.idtomovimientosiniestro is not null" +
					" and sin.idmovimientosiniestro is not null" +
					" and sin.idtopoliza = mov.idtopoliza" +
					" and sin.idtcconceptodetalle = mov.idtcconceptomovimiento" +
					" and to_date(sin.fechamovimiento,'dd/MM/yyyy') <> to_date(mov.fecharegistro,'dd/MM/yyyy')" +
					")  and mov.cantidad <>0 ";//and mov.contabilizado = 1";
			Query query = entityManager.createNativeQuery(queryString);
			Object resultado = query.getSingleResult();
			int cantidadMovimientos = 0;
			if(resultado instanceof BigDecimal){
				cantidadMovimientos = ((BigDecimal)resultado).intValue();
			}else if(resultado instanceof Long){
				cantidadMovimientos = ((Long)resultado).intValue();
			}else if(resultado instanceof Double){
				cantidadMovimientos = ((Double)resultado).intValue();
			}else if(resultado instanceof Integer){
				cantidadMovimientos = ((Integer)resultado).intValue();
			}else if(resultado instanceof Float){
				cantidadMovimientos = ((Float)resultado).intValue();
			}else{
				cantidadMovimientos = Integer.valueOf(resultado.toString()).intValue();
			}
			LogDeMidasEJB3.log("Cantidad de movimientos de reaseguro provenientes de siniestros con fecha de registro incorrecta: "+cantidadMovimientos, Level.INFO, null);
			return cantidadMovimientos;
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Ocurri� un error al Consultar la cantidad de movimientos de reaseguro provenientes de siniestros con fecha de registro incorrecta.", Level.INFO, e);
			throw e;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<MovimientoReaseguroDTO> listarPorRangoIds(Integer mes,Integer anio,int limiteInferior, int limiteSuperior) {
		List<MovimientoReaseguroDTO> movimientoReaseguroDTOList;
		String queryString = null;
		Calendar calendarInicial = null, calendarFinal = null;
		boolean validarPorRango = false;
		queryString = "SELECT model FROM MovimientoReaseguroDTO model WHERE " +
					"model.idMovimiento >= :limiteInferior AND model.idMovimiento <= :limiteSuperior AND model.acumulado = 1 ORDER BY model.idMovimiento";
		if(mes != null && anio != null){
			if(mes.intValue() > 0 && mes.intValue()< 12 && anio.intValue() > 2008){
				calendarInicial = Calendar.getInstance();
				calendarInicial.set(anio,mes-1,0,0,0,0);
				calendarFinal = Calendar.getInstance();
				calendarFinal.set(anio, mes-1, 0, 23, 59, 59);
				calendarFinal.add(Calendar.MONTH, 1);
				calendarFinal.add(Calendar.DATE, -1);
				queryString = "SELECT model FROM MovimientoReaseguroDTO model where model.fechaRegistro BETWEEN :fechaInicial and :fechaFinal ORDER BY model.idMovimiento";
				validarPorRango = true;
			}
		}
		Query query = entityManager.createQuery(queryString);
		if(validarPorRango){
			query.setParameter("fechaInicial", calendarInicial.getTime());
			query.setParameter("fechaFinal", calendarFinal.getTime());
			query.setFirstResult(limiteInferior);
			query.setMaxResults(limiteSuperior-limiteInferior);
		}else{
			query.setParameter("limiteInferior", limiteInferior);
			query.setParameter("limiteSuperior", limiteSuperior);
		}
		
		movimientoReaseguroDTOList = query.getResultList();
		return movimientoReaseguroDTOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MovimientoReaseguroDTO> listarPorRangoIds(List<BigDecimal> listaIdToMovimientos,int limiteInferior, int limiteSuperior) {
		List<MovimientoReaseguroDTO> movimientoReaseguroDTOList;
		String queryString = "SELECT model FROM MovimientoReaseguroDTO model WHERE model.idMovimiento in ( ";
		String idToMovimientosString =obtenerIdToMovimientosString(listaIdToMovimientos);
		queryString += idToMovimientosString + ") AND model.acumulado = 1";
		Query query = entityManager.createQuery(queryString);
		if(limiteSuperior > limiteInferior){
			query.setFirstResult(limiteInferior);
			query.setMaxResults(limiteSuperior-limiteInferior);
		}
		movimientoReaseguroDTOList = query.getResultList();
		return movimientoReaseguroDTOList;
	}
	
	public String obtenerIdToMovimientosString(
			List<BigDecimal> listaIdToMovimientos) {
		String idToMovimientosString =  StringUtils.join(listaIdToMovimientos.iterator(), ',');
		return idToMovimientosString.length() >= 1 ? idToMovimientosString : "0";
	}
	@SuppressWarnings("unchecked")
	public List<MovimientoReaseguroDTO> listarMovimientosSiniestrosNoAcumulables(Integer limiteInferior, Integer limiteSuperior) {
		List<MovimientoReaseguroDTO> movimientoReaseguroDTOList;
		String queryString = null;
		boolean incluirRangoRegistros = (limiteInferior != null && limiteSuperior != null && limiteInferior.intValue() < limiteSuperior.intValue());
		queryString = "SELECT model FROM MovimientoReaseguroDTO model where model.idToReporteSiniestro is not null and " +
					"model.conceptoMovimientoDetalleDTO.banderaAcumulaEdoCta = 0 and model.acumulado = 0 order by model.idMovimiento";
		Query query = entityManager.createQuery(queryString);
		if(incluirRangoRegistros){
			
			query.setFirstResult(limiteInferior);
			query.setMaxResults(limiteSuperior-limiteInferior);
		}
		movimientoReaseguroDTOList = query.getResultList();
		return movimientoReaseguroDTOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MovimientoReaseguroDTO> listarMovimientosSiniestrosConFechaIncorrecta(Integer limiteInferior, Integer limiteSuperior) {
		LogDeMidasEJB3.log("Consultando movimientos de reaseguro provenientes de siniestros con fecha de registro incorrecta del registro "+limiteInferior+" al "+limiteSuperior, Level.INFO, null);
		List<MovimientoReaseguroDTO> listaResultado = null;
		try{
			String queryString = "select mov.idtomovimiento from MIDAS.todistribucionmovsiniestro sin, MIDAS.tomovimientoreaseguro mov  where (" +
					" mov.idtomovimientosiniestro = sin.idmovimientosiniestro and mov.idtoreportesiniestro = sin.idtoreportesiniestro" +
					" and sin.idtoseccion = mov.idtoseccion and sin.idtocobertura = mov.idtocobertura" +
					" and nvl(sin.numeroinciso,0) = nvl(mov.idtoinciso,0) and nvl(sin.numerosubinciso,0) = nvl(mov.idtosubinciso,0)" +
					" and sin.idtcmoneda = mov.idmoneda" +
					" and sin.numeroendoso = mov.numeroendoso" +
					" and mov.idtomovimientosiniestro is not null" +
					" and sin.idmovimientosiniestro is not null" +
					" and sin.idtopoliza = mov.idtopoliza" +
					" and sin.idtcconceptodetalle = mov.idtcconceptomovimiento" +
					" and to_date(sin.fechamovimiento,'dd/MM/yyyy') <> to_date(mov.fecharegistro,'dd/MM/yyyy')" +
					")  and mov.cantidad <>0 order by mov.idToMovimiento";//and mov.contabilizado = 1 order by mov.idToMovimiento";
			Query query = entityManager.createNativeQuery(queryString);
			Iterator iteradorIdMovimientos = query.getResultList().iterator();
			String idMovimientos = obtenerIdMovimientos(query.getResultList().iterator());
			queryString = "select model from MovimientoReaseguroDTO model where model.idMovimiento in ("+idMovimientos+")";
			query = entityManager.createQuery(queryString);
			if(limiteInferior != null && limiteSuperior != null && limiteSuperior > limiteInferior){
				query.setFirstResult(limiteInferior);
				query.setMaxResults(limiteSuperior - limiteInferior);
			}
			listaResultado = query.getResultList();
			if(listaResultado != null)
				LogDeMidasEJB3.log("Cantidad de movimientos de reaseguro provenientes de siniestros con fecha de registro incorrecta: "+listaResultado.size()+" del "+limiteInferior+" al "+limiteSuperior, Level.INFO, null);
			return listaResultado;
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Ocurri� un error al Consultar los movimientos de reaseguro provenientes de siniestros con fecha de registro incorrecta.", Level.INFO, e);
			throw e;
		}
	}
	
	public String obtenerIdMovimientos(Iterator iteradorIdMovimientos) {
		StringBuilder idMovimientos = new StringBuilder("");
		while(iteradorIdMovimientos.hasNext()){
			Object objectIdMovimiento = (Object)iteradorIdMovimientos.next();
			if(objectIdMovimiento instanceof BigDecimal){
				idMovimientos.append(((BigDecimal)objectIdMovimiento).intValue()).append(",");
			}else if(objectIdMovimiento instanceof Long){
				idMovimientos.append(((Long)objectIdMovimiento).intValue()).append(",");
			}else if(objectIdMovimiento instanceof Double){
				idMovimientos.append(((Double)objectIdMovimiento).intValue()).append(",");
			}else if(objectIdMovimiento instanceof Integer){
				idMovimientos.append(((Integer)objectIdMovimiento).intValue()).append(",");
			}else if(objectIdMovimiento instanceof Float){
				idMovimientos.append(((Float)objectIdMovimiento).intValue()).append(",");
			}else{
				idMovimientos.append(Integer.valueOf(objectIdMovimiento.toString()).intValue()).append(",");
			}
		}
		return  (idMovimientos.length()>1) ? idMovimientos.substring(0, idMovimientos.length()-1):idMovimientos+"0";
	}

	
	@SuppressWarnings("unchecked")
	public void acumularMovimientosTerminacionSiniestros(BigDecimal idToReporteSiniestro) throws RuntimeException{
		LogDeMidasEJB3.log("Entering method acumularMovimientosTerminacionSiniestros with idToReporteSiniestro: " + idToReporteSiniestro.toBigInteger().toString(), Level.INFO, null);
		ReporteSiniestroDTO reporteSiniestroDTO = entityManager.find(ReporteSiniestroDTO.class, idToReporteSiniestro);
		if (reporteSiniestroDTO != null){
			List<MovimientoReaseguroDTO> movimientoReaseguroDTOList;
			String queryString = "SELECT model FROM MovimientoReaseguroDTO model WHERE model.idToReporteSiniestro = :idToReporteSiniestro AND " +
			"model.conceptoMovimientoDetalleDTO.banderaAplicaTerminacionSiniestros = true AND model.acumulado = 0";

			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToReporteSiniestro", reporteSiniestroDTO.getIdToReporteSiniestro());
			movimientoReaseguroDTOList = query.getResultList();
			if (movimientoReaseguroDTOList != null && movimientoReaseguroDTOList.size() > 0){
				for (MovimientoReaseguroDTO movimientoReaseguroDTO : movimientoReaseguroDTOList) {
					movimientoReaseguroDTO.setAcumulado(1); //Se marca con 1 para ser acumulado en acumuladorEjb.procesarLoteMovimientosARegenerar
					entityManager.merge(movimientoReaseguroDTO);
					registrarMovimiento(movimientoReaseguroDTO,Boolean.FALSE);
				}
			}else{
				LogDeMidasEJB3.log("No movements were found to accumulate for ReporteSiniestroDTO with id: " + idToReporteSiniestro, Level.INFO, null);
			}
			LogDeMidasEJB3.log("Finalizing method acumularMovimientosTerminacionSiniestros", Level.INFO, null);
		}else{
			throw new RuntimeException("Error, reporteSiniestroDTO not found with id: " + idToReporteSiniestro);
		}
	}

	public List<AsientoContableDTO> contabilizaMovimientosReaIn(String idObjetoContable, String claveTransaccionContable,  String usuario) throws RuntimeException {
		List<AsientoContableDTO> list = null;
		 try{   
		     list = asientoContableFacade.contabilizaMovimientos(idObjetoContable, claveTransaccionContable, usuario);			
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Ocurri� un error al registrar ingresos en reaseguro .", Level.INFO, e);
			throw e;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return list;
	}
	
	public boolean recalcularAcumulador(BigDecimal idToAcumulador){
		boolean calculoExitoso = true;
		
		String queryString = "select nvl(sum(mov.cantidad),0) cantidad from MIDAS.toestadocuenta edo, MIDAS.toacumulador acu, MIDAS.tomovimientoreaseguro mov, MIDAS.tcTipoMovimiento tipoMov where " +
				" edo.idtoestadocuenta = acu.idtoestadocuenta and " +
				" nvl(mov.idtmcontratocuotaparte,0) = nvl(edo.idtmcontratocuotaparte,0) and " +
				" nvl(mov.idtmcontratoprimerexcedente,0) = nvl(edo.idtmcontratoprimerexcedente,0) and " +
				" nvl(mov.idtcreaseguradorcorredor,0) = nvl(edo.idtcreasegurador,0) and " +
				" nvl(mov.idtccorredor,0) = nvl(edo.idtccorredor,0) and " +
				" nvl(mov.idtmcontratofacultativo,0) = nvl(edo.idtmcontratofacultativo,0) and " +
				" 1 = (select (case " +
				"    when mov.idtmcontratofacultativo is null then 1 " +
				"    when nvl(mov.numeroendoso,0) = nvl(edo.numeroendoso,0) then 1 " +
				"    else 0 " +
				"    end " +
				"    ) endoso from dual ) and " +
				" nvl(mov.idtmcontratofacultativo,0) = nvl(edo.idtmcontratofacultativo,0) and " +
				" edo.idmoneda = mov.idmoneda and " +
				" edo.ejercicio = mov.ejercicio and " +
				" edo.idtcsubramo = mov.idtcsubramo and " +
				" mov.idtcconceptomovimiento in ( " +
				" select conceptoDet.idtcconceptodetalle from MIDAS.tcconceptomovimientodetalle conceptoDet " +
				" where conceptoDet.idtcconceptomovimiento = acu.idtcconceptomovimiento and conceptoDet.acumulaedocta = 1 /*and " +
				" 1 = (select (case " +
				" when conceptoDet.aplicaterminacionsiniestros = 0 then 1 " +
				" when conceptoDet.aplicaterminacionsiniestros = 1 and ( " +
				" select nvl(sin.idtcreporteestatus,0) from MIDAS.toreportesiniestro sin " +
				" where sin.idtoreportesiniestro = nvl(mov.idtoreportesiniestro,0) ) = 24 then 1 " +
				" else 0 " +
				" end " +
				" ) valor from dual )*/ ) and " +
				" to_number(to_char(mov.fecharegistro,'mm')) = acu.mes and " +
				" to_number(to_char(mov.fecharegistro,'yyyy')) = acu.anio and " +
				" mov.acumulado = 1 and " +
				" mov.idtctipomovimiento = tipomov.idtctipomovimiento and " +
				" tipomov.naturaleza = %d and acu.idtoacumulador = %s";
		AcumuladorDTO acumuladorDTO = entityManager.find(AcumuladorDTO.class, idToAcumulador);
		if(acumuladorDTO != null){
			//calcular el abono, de naturaleza 1
			String queryAbono = String.format(queryString, TipoMovimientoDTO.NATURALEZA_ABONO, acumuladorDTO.getIdAcumulador().toString());
			Query query = entityManager.createNativeQuery(queryAbono);
			Object resultado = query.getSingleResult();
			BigDecimal abono = Utilerias.obtenerBigDecimal(resultado);
			//calcular el cargo, de naturaleza 2
			String queryCargo = String.format(queryString, TipoMovimientoDTO.NATURALEZA_CARGO, acumuladorDTO.getIdAcumulador().toString());
			Query query2 = entityManager.createNativeQuery(queryCargo);
			resultado = query2.getSingleResult();
			BigDecimal cargo = Utilerias.obtenerBigDecimal(resultado);
			//actualizar el acumulador
			acumuladorDTO.setAbono(abono);
			acumuladorDTO.setCargo(cargo);
			entityManager.merge(acumuladorDTO);
			LogDeMidasEJB3.log("Se calcul� correctamente el acumulador con id: "+idToAcumulador+", cargo: "+cargo+", abono: "+abono, Level.INFO, null);
		}else{
			LogDeMidasEJB3.log("Error al calcular el acumulador con id: "+idToAcumulador+" no se encontr� el acumulador", Level.SEVERE, null);
			calculoExitoso = false;
		}
		return calculoExitoso;
	}
	
	public BigDecimal obtenerSaldoSiniestro(BigDecimal idToReporteSiniestro,int idConceptoMovimiento,int naturaleza){
		BigDecimal saldoCalculado = BigDecimal.ZERO;
		if(idToReporteSiniestro != null){
			String queryString = "select nvl(sum(mov.cantidad),0) saldoConcepto from " +
					"midas.tomovimientoreaseguro mov, midas.toreportesiniestro sin,midas.tcTipoMovimiento tipoMov where " +
					"sin.idtoreportesiniestro = %d and " +
					"sin.idtoreportesiniestro = mov.idtoreportesiniestro and " +
					"mov.idtcconceptomovimiento in ( " +
						"select conceptoDet.idtcconceptodetalle from midas.tcconceptomovimientodetalle conceptoDet " +
						"where conceptoDet.idtcconceptomovimiento = %d and conceptoDet.acumulaedocta = 1 and " +
							"1 = (select (case " +
								"when conceptoDet.aplicaterminacionsiniestros = 0 then 1 " +
								"when conceptoDet.aplicaterminacionsiniestros = 1 and ( sin.idtcreporteestatus >= 24 ) then 1 " +
								"else 0 " +
								"end " +
							") valor from dual ) ) and " +
					"mov.idtctipomovimiento = tipomov.idtctipomovimiento and " +
					"tipomov.naturaleza = %d";
			
			String queryAbono = String.format(queryString, idToReporteSiniestro.intValue(), idConceptoMovimiento, naturaleza);
			Query query = entityManager.createNativeQuery(queryAbono);
			Object resultado = query.getSingleResult();
			saldoCalculado = Utilerias.obtenerBigDecimal(resultado);
		}
		return saldoCalculado;
	}
	
	@SuppressWarnings("unchecked")
	public List<BigDecimal> obtenerIdReporteSiniestrosTerminados(){
		String queryString = "select distinct sin.idtoreportesiniestro from MIDAS.toreportesiniestro sin where sin.idtcreporteestatus >= 24";
		Query query = entityManager.createNativeQuery(queryString);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<SubRamoDTO> obtenerSubRamosAfectadosPorSiniestro(BigDecimal idToReporteSiniestro,boolean incluirSoloSubRamosFacultados){
		List<SubRamoDTO> subRamosAfectados = null;
		if(idToReporteSiniestro != null){
			String queryString = "select distinct model.subRamo from MovimientoReaseguroDTO model where model.idToReporteSiniestro = :idToReporteSiniestro";
			if(incluirSoloSubRamosFacultados)
				queryString += " and model.tipoReaseguroDTO.tipoReaseguro = :tipoReaseguro";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			if(incluirSoloSubRamosFacultados)
				query.setParameter("tipoReaseguro", TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);
			subRamosAfectados = query.getResultList();
		}
		return subRamosAfectados;
	}
}