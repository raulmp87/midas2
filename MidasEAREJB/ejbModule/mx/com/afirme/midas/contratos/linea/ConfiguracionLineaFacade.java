package mx.com.afirme.midas.contratos.linea;
// default package

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ConfiguracionLineaDTO.
 * @see .ConfiguracionLinea
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ConfiguracionLineaFacade  implements ConfiguracionLineaFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ConfiguracionLineaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ConfiguracionLineaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ConfiguracionLineaDTO entity) {
    				LogDeMidasEJB3.log("saving ConfiguracionLineaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ConfiguracionLineaDTO entity.
	  @param entity ConfiguracionLineaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ConfiguracionLineaDTO entity) {
    				LogDeMidasEJB3.log("deleting ConfiguracionLineaDTO instance", Level.INFO, null);
	        try {
		        	entity = entityManager.getReference(ConfiguracionLineaDTO.class, entity.getIdTmLinea());
		            entityManager.remove(entity);
            		LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ConfiguracionLineaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ConfiguracionLineaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ConfiguracionLineaDTO entity to update
	 @return ConfiguracionLineaDTO the persisted ConfiguracionLineaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ConfiguracionLineaDTO update(ConfiguracionLineaDTO entity) {
    				LogDeMidasEJB3.log("updating ConfiguracionLineaDTO instance", Level.INFO, null);
	        try {
            ConfiguracionLineaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 * Find the ConfiguracionLineaDTO entity with a specific Id value.  
	 
	  @param id the id of the ConfiguracionLineaDTO to query
	  	  @return ConfiguracionLineaDTO found by query
	*/
    public ConfiguracionLineaDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding ConfiguracionLineaDTO instance with id: " + id, Level.INFO, null);
	        try {
            ConfiguracionLineaDTO instance = entityManager.getReference(ConfiguracionLineaDTO.class, id);
            entityManager.refresh(instance);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ConfiguracionLineaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ConfiguracionLineaDTO property to query
	  @param value the property value to match
	  	  @return List<ConfiguracionLineaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ConfiguracionLineaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ConfiguracionLineaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ConfiguracionLineaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return refrescarListaConfiguracionLinea(query.getResultList());
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ConfiguracionLineaDTO entities.
	  	  @return List<ConfiguracionLineaDTO> all ConfiguracionLineaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConfiguracionLineaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ConfiguracionLineaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ConfiguracionLineaDTO model";
								Query query = entityManager.createQuery(queryString);
					return refrescarListaConfiguracionLinea(query.getResultList());
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find filtered ConfiguracionLineaDTO entities.
	  	  @return List<ConfiguracionLineaDTO> filtered ConfiguracionLineaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConfiguracionLineaDTO> listarFiltrado(ConfiguracionLineaDTO configuracionLineaDTO){
		try {
			String queryString = "select model from ConfiguracionLineaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (configuracionLineaDTO == null)
				return null;
						
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicial", configuracionLineaDTO.getFechaInicial());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaFinal", configuracionLineaDTO.getFechaFinal());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcRamo", configuracionLineaDTO.getIdTcRamo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "estatus", configuracionLineaDTO.getEstatus());
					
				
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return refrescarListaConfiguracionLinea(query.getResultList());
		} catch (RuntimeException re) {
			throw re;
		}		
	}
	
	/**
	 * Find filtered ConfiguracionLineaDTO entities.
	  	  @return List<ConfiguracionLineaDTO> filtered ConfiguracionLineaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConfiguracionLineaDTO> listarFiltradoContrato(ConfiguracionLineaDTO configuracionLineaDTO){
		try {
			String queryString = "select model from ConfiguracionLineaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			Date fechaInicial = new Date();
			
			if (configuracionLineaDTO == null)
				return null;
			
			if (configuracionLineaDTO.getFechaInicial()!=null){
				sWhere += " model.fechaInicial = :fechaInicial";
			}else{
				sWhere += " (model.fechaInicial > :fechaInicial)";
			}
			
			if (configuracionLineaDTO.getFechaFinal()!=null){
				sWhere += " and model.fechaFinal = :fechaFinal";
			}
			
			if (configuracionLineaDTO.getIdTcRamo()!=null){
				sWhere += " and model.idTcRamo = :idTcRamo";
			}
			
			sWhere += " and model.estatus = :estatus";
					
				
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setParameter("fechaInicial", fechaInicial);
			if (configuracionLineaDTO.getFechaFinal()!=null)query.setParameter("fechaFinal",configuracionLineaDTO.getFechaFinal());
			if (configuracionLineaDTO.getIdTcRamo()!=null)query.setParameter("idTcRamo", configuracionLineaDTO.getIdTcRamo());
			query.setParameter("estatus", configuracionLineaDTO.getEstatus());
			return refrescarListaConfiguracionLinea(query.getResultList());
		} catch (RuntimeException re) {
			throw re;
		}		
	}
	
	/**
	 * Find filtered ConfiguracionLineaDTO entities.
	  	  @return List<ConfiguracionLineaDTO> filtered ConfiguracionLineaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConfiguracionLineaDTO> listarFiltradoVigencia(ConfiguracionLineaDTO configuracionLineaDTO){
		try {
			String queryString = "select model from ConfiguracionLineaDTO model ";
			String sWhere = "";
			Query query;
//			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			Date diaActual = new Date();
			List<ConfiguracionLineaDTO> lista = null;
			
			if (configuracionLineaDTO == null)
				return null;
			
			if (configuracionLineaDTO.getFechaInicial()!=null){
				sWhere += " model.fechaInicial = :fechaInicial";
			}else{
				sWhere += " (model.fechaInicial <= :fechaInicial)";
			}
			
			if (configuracionLineaDTO.getFechaFinal()!=null){
				sWhere += " and model.fechaFinal = :fechaFinal";
			}else{
				sWhere += " and (model.fechaFinal >= :fechaFinal)";
			}
			
			if (configuracionLineaDTO.getIdTcRamo()!=null){
				sWhere += " and model.idTcRamo = :idTcRamo";
			}
			
			sWhere += " and model.estatus = :estatus";
					
				
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
//			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			if(configuracionLineaDTO.getFechaInicial() != null)
				query.setParameter("fechaInicial", configuracionLineaDTO.getFechaInicial());
			else
				query.setParameter("fechaInicial", diaActual);
			if(configuracionLineaDTO.getFechaFinal() != null)
				query.setParameter("fechaFinal", configuracionLineaDTO.getFechaFinal());
			else
				query.setParameter("fechaFinal", diaActual);
			if (configuracionLineaDTO.getIdTcRamo()!=null)
				query.setParameter("idTcRamo", configuracionLineaDTO.getIdTcRamo());
			
			query.setParameter("estatus", configuracionLineaDTO.getEstatus());
			lista = query.getResultList();
			return refrescarListaConfiguracionLinea(lista);
		} catch (RuntimeException re) {
			throw re;
		}		
	}
	
	/**
	 * Find filtered ConfiguracionLineaDTO entities.
	  	  @return List<ConfiguracionLineaDTO> filtered ConfiguracionLineaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConfiguracionLineaDTO> listarLineaFiltrado(ConfiguracionLineaDTO configuracionLineaDTO){
		
		try {
			String queryString = "select model from ConfiguracionLineaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (configuracionLineaDTO == null)
				return null;
						
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicial", configuracionLineaDTO.getFechaInicial());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaFinal", configuracionLineaDTO.getFechaFinal());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcRamo", configuracionLineaDTO.getIdTcRamo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "estatus", configuracionLineaDTO.getEstatus());
					
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return refrescarListaConfiguracionLinea(query.getResultList());
		} catch (RuntimeException re) {
			throw re;
		}		
	}
	
	/**
	 * Find filtered ConfiguracionLineaDTO entities.
	  	  @return List<ConfiguracionLineaDTO> filtered ConfiguracionLineaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConfiguracionLineaDTO> getCLineasPorContratosDeCLinea(ConfiguracionLineaDTO configuracionLineaDTO){
		List<ConfiguracionLineaDTO> configuracionLineasDTO = new ArrayList<ConfiguracionLineaDTO>();
		try {
			String queryString = "select model from ConfiguracionLineaDTO model ";
			String sWhere = "";
			Query query;
			
			if (configuracionLineaDTO == null || configuracionLineaDTO.getIdTmLinea()==null)
				return null;
			
			if (configuracionLineaDTO.getIdTmContratoPrimerExcedente()!=null ||
					configuracionLineaDTO.getIdTmContratoCuotaParte()!=null){
				sWhere = " where ";
				sWhere += " model.idTmLinea<>"+configuracionLineaDTO.getIdTmLinea().toString();
				sWhere += " and (";
				if (configuracionLineaDTO.getIdTmContratoCuotaParte()!=null){
					sWhere += " model.idTmContratoCuotaParte="+configuracionLineaDTO.getIdTmContratoCuotaParte().toString();
					if (configuracionLineaDTO.getIdTmContratoPrimerExcedente()!=null)
						sWhere += " and ";
				}
				if (configuracionLineaDTO.getIdTmContratoPrimerExcedente()!=null)
					sWhere += " model.idTmContratoPrimerExcedente="+configuracionLineaDTO.getIdTmContratoPrimerExcedente().toString();
				
				sWhere += ")";
				
				if (Utilerias.esAtributoQueryValido(sWhere))
					queryString = queryString.concat(sWhere);
			
				query = entityManager.createQuery(queryString);
				configuracionLineasDTO = query.getResultList();
			}		
			return refrescarListaConfiguracionLinea(configuracionLineasDTO);
		} catch (RuntimeException re) {
			throw re;
		}		
	}
	
	/**
	 * Find filtered ConfiguracionLineaDTO entities.
	  	  @return List<ConfiguracionLineaDTO> filtered ConfiguracionLineaDTO by idLineaDTO
	 */
	@SuppressWarnings("unchecked")
	public List<ConfiguracionLineaDTO> getConfiguracionLineaPorIDLineas(List<String> idLineas){
		List<ConfiguracionLineaDTO> list = new ArrayList<ConfiguracionLineaDTO>();
		try {
			String queryString = "select model from ConfiguracionLineaDTO model ";
			String sWhere = " model.idTmLinea in ("+ obtenerInSQL(idLineas) +")";
			Query query;
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			list = query.getResultList();	
		} catch (RuntimeException re) {
			throw re;
		}
		return refrescarListaConfiguracionLinea(list);
	}
	
	public String obtenerInSQL(List<String> idLineas) {
		String lineas ="";
		if (idLineas != null && idLineas.size()>0){
			lineas = StringUtils.join(idLineas.iterator(), ',');
		}
		return lineas;
	}

	public List<ConfiguracionLineaDTO> refrescarListaConfiguracionLinea(List<ConfiguracionLineaDTO> configuracionLineaDTOList){
		if (configuracionLineaDTOList != null)
			for (ConfiguracionLineaDTO configuracionLineaDTO : configuracionLineaDTOList) {
				entityManager.refresh(configuracionLineaDTO);
			}
		return configuracionLineaDTOList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SubRamoDTO> getSubRamosRestantes(String ids, BigDecimal idTcRamo){
		LogDeMidasEJB3.log("finding SubRamosDTO  restantes instance ", Level.INFO, null);
		List<SubRamoDTO> list = new ArrayList<SubRamoDTO>();
		try {
			String queryString = "select model from SubRamoDTO model ";
			String sWhere = " model.idTcSubRamo not in ("+ ids +") ";
			if (idTcRamo!=null) sWhere += (" and model.ramoDTO.idTcRamo=:idTcRamo ");
			sWhere += " order by model.ramoDTO.descripcion, model.descripcionSubRamo ";
			
			queryString = queryString.concat(" where ").concat(sWhere);			
			Query query = entityManager.createQuery(queryString);
			if (idTcRamo!=null) query.setParameter("idTcRamo", idTcRamo);
			list = query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
		return list;
	}
}