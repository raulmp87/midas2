package mx.com.afirme.midas.contratos.linea;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.lineaparticipacion.LineaParticipacionDTO;
import mx.com.afirme.midas.contratos.lineaparticipacion.LineaParticipacionId;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroFacade;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity LineaDTO.
 * @see .LineaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class LineaFacade  implements LineaFacadeRemote {

    @PersistenceContext
    private EntityManager entityManager;
    
    @Resource
    private SessionContext context;
    
    @EJB
    ParticipacionCorredorFacadeRemote participacionCorredorFacade;
		/**
	 Perform an initial save of a previously unsaved LineaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity LineaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public LineaDTO save(LineaDTO entity) {
    				LogDeMidasEJB3.log("saving LineaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
            return entity;
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent LineaDTO entity.
	  @param entity LineaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(LineaDTO entity) {
    				LogDeMidasEJB3.log("deleting LineaDTO instance", Level.INFO, null);
	        try {
	        entity = entityManager.getReference(LineaDTO.class, entity.getIdTmLinea());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved LineaDTO entity and return it or a copy of it to the sender. 
	 A copy of the LineaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity LineaDTO entity to update
	 @return LineaDTO the persisted LineaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public LineaDTO update(LineaDTO entity) {
    				LogDeMidasEJB3.log("updating LineaDTO instance", Level.INFO, null);
	        try {
            LineaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 * Find the LineaDTO entity with a specific Id value.  
	 
	  @param id the id of the LineaDTO to query
	  	  @return LineaDTO found by query
	*/
    public LineaDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding LineaDTO instance with id: " + id, Level.INFO, null);
	        try {
            LineaDTO instance = entityManager.find(LineaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

    /**
	 * Find all LineaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the LineaDTO property to query
	  @param value the property value to match
	  	  @return List<LineaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<LineaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding LineaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from LineaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all LineaDTO entities.
	  	  @return List<LineaDTO> all LineaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<LineaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all LineaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from LineaDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	 /**
	 * Encuentra todas las entidades LineaDTO que cumplen con la fecha inicial  
	   y la fecha final proporcionadas en los par�metros
	  @param fInicial fecha inicial por la que se har� la b�squeda
	  @param fFinal fecha final por la que se har� la b�squeda
	  	  @return List<LineaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<LineaDTO> buscarPorFechaInicialFinal(Date fInicial, Date fFinal
		) {
					LogDeMidasEJB3.log("finding all LineaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from LineaDTO model";
			String sWhere = " where model.fechaInicial = :fechaInicial" ;
			sWhere += " and model.fechaFinal = :fechaFinal" ;
			
			Query query = entityManager.createQuery(queryString + sWhere);
			query.setParameter("fechaFinal",fFinal);
			query.setParameter("fechaInicial",fInicial);
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Update LineaDTO entities by dates using as a list of ids as filter(using this format 'id,id,id') .
	 *   	  @return LineaDTO the persisted LineaDTO entity instance, may not be the same
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean actualizaFechasLineasPorIdLineas(String ids,String idsCCP,String idsCPE, Date fInicial, Date fFinal
		) {
					LogDeMidasEJB3.log("update all LineaDTO instances", Level.INFO, null);

				try {

				if (ids==null || ids.trim().length()==0)return false;
				
				String queryString = "update LineaDTO model set ";
				queryString += " model.fechaInicial= :fechaInicial";
				queryString += " , model.fechaFinal= :fechaFinal";
				String sWhere = " where model.idTmLinea in (" + ids + ")";				
				Query query = entityManager.createQuery(queryString + sWhere);
				query.setParameter("fechaFinal",fFinal);
				query.setParameter("fechaInicial",fInicial);				
				query.executeUpdate();
				
				if(idsCCP!=null){
					queryString = "update ContratoCuotaParteDTO model set ";
					queryString += " model.fechaInicial= :fechaInicial";
					queryString += " ,model.fechaFinal= :fechaFinal";
					sWhere = " where model.idTmContratoCuotaParte in (" + idsCCP + ")";
					String consulta = queryString + sWhere;				
					query = entityManager.createQuery(consulta);
					query.setParameter("fechaInicial", fInicial);
					query.setParameter("fechaFinal", fFinal);
					query.executeUpdate();
				}
				
				if (idsCPE!=null){
					queryString = "update ContratoPrimerExcedenteDTO model set ";
					queryString += " model.fechaInicial= :fechaInicial";
					queryString += " ,model.fechaFinal= :fechaFinal";
					sWhere = " where model.idTmContratoPrimerExcedente in (" + idsCPE + ")";				
					query = entityManager.createQuery(queryString + sWhere);
					query.setParameter("fechaInicial", fInicial);
					query.setParameter("fechaFinal", fFinal);
					query.executeUpdate();
				}
				return true;
		} catch (RuntimeException re) {	
			context.setRollbackOnly();
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Validate exercise by LineaDTO.
	  	  @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean validaEjercicio(LineaDTO lineaDTO
		) {
					LogDeMidasEJB3.log("Validate exercise for LineaDTO instances", Level.INFO, null);
			List<Object> rowCountObj = new ArrayList<Object>();

			Double rowCount = new Double("0.0");
			if (lineaDTO==null)return false;
			try {
				String queryString = " select count(model.idTmLinea) from LineaDTO model ";
				queryString += " where model.subRamo = :subRamo " + ((lineaDTO.getIdTmLinea()!=null)?" and not (model.idTmLinea = :idTmLinea) ":"");
				queryString += " and not ((model.fechaInicial > :fechaInicial and model.fechaInicial > :fechaFinal) ";
			    queryString += " or (model.fechaFinal < :fechaInicial and model.fechaFinal < :fechaFinal))";
				
				Query query = entityManager.createQuery(queryString);
				if (lineaDTO.getIdTmLinea()!=null)query.setParameter("idTmLinea", lineaDTO.getIdTmLinea());
				query.setParameter("subRamo", lineaDTO.getSubRamo());
				query.setParameter("fechaInicial", lineaDTO.getFechaInicial());
				query.setParameter("fechaFinal", lineaDTO.getFechaFinal());	
				rowCountObj = query.getResultList();
		
				rowCount = new Double(rowCountObj.get(0).toString());
				if (rowCount.doubleValue() > 0)
					return false;
				else
					return true;
		} catch (RuntimeException re) {
			re.printStackTrace();
						LogDeMidasEJB3.log("Validate exercise failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LineaDTO duplicarLinea(LineaDTO lineaDTO, List<ParticipacionDTO> pCCP, List<ParticipacionDTO> pCPE){
		try{
			LineaDTO linea = new LineaDTO();
			ContratoCuotaParteDTO contratoCuotaParteDTO = new ContratoCuotaParteDTO();
			ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO=new ContratoPrimerExcedenteDTO();			
			List<ParticipacionDTO> listParticipaciones = new ArrayList<ParticipacionDTO>();	
			LineaParticipacionDTO lineaParticipacionDTO = null;
			ParticipacionDTO participacion = new ParticipacionDTO();
			ParticipacionCorredorDTO participacionCorredorDTO = new ParticipacionCorredorDTO();
			
			Double comision = new Double(0);
			HashMap<String, Double> participacionComision = new HashMap<String, Double>();			
			
			contratoCuotaParteDTO = duplicaCCP(lineaDTO.getContratoCuotaParte(), lineaDTO);
			contratoPrimerExcedenteDTO = duplicaCPE(lineaDTO.getContratoPrimerExcedente(), lineaDTO);
			linea = duplicaLinea(lineaDTO);
			
			entityManager.persist(linea);
			if (contratoPrimerExcedenteDTO!=null) entityManager.persist(contratoPrimerExcedenteDTO);
			if (contratoCuotaParteDTO!=null) entityManager.persist(contratoCuotaParteDTO);			
			
			if (lineaDTO.getLineaParticipaciones()!=null){
				for (LineaParticipacionDTO lineaParticipacion : lineaDTO.getLineaParticipaciones()) {
					participacionComision.put(lineaParticipacion.getParticipacion().getIdTdParticipacion().toString(), lineaParticipacion.getComision());
				}				
			}
			
			
			if (contratoCuotaParteDTO!=null && contratoCuotaParteDTO.getIdTmContratoCuotaParte()!=null){
				listParticipaciones = pCCP;
				for (ParticipacionDTO participacionDTO : listParticipaciones) {
					participacion = new ParticipacionDTO(); 
					participacion = duplicaParticipacion(participacionDTO);
					participacion.setContratoCuotaParte(contratoCuotaParteDTO);
					participacion.setContratoPrimerExcedente(null);
					entityManager.persist(participacion);
				
					if (participacionDTO!=null && participacionDTO.getIdTdParticipacion()!=null && participacionDTO.getParticipacionCorredores()!=null){
						List<ParticipacionCorredorDTO> listaPC = participacionCorredorFacade.findByProperty("participacion", participacionDTO);
						for (ParticipacionCorredorDTO corredor : listaPC/*participacionDTO.getParticipacionCorredores()*/) {						
							participacionCorredorDTO = new ParticipacionCorredorDTO();
							participacionCorredorDTO.setParticipacion(participacion);
							participacionCorredorDTO.setPorcentajeParticipacion(corredor.getPorcentajeParticipacion());
							participacionCorredorDTO.setReaseguradorCorredor(corredor.getReaseguradorCorredor());
							entityManager.persist(participacionCorredorDTO);
						}
					}
					
					comision = participacionComision.get(participacionDTO.getIdTdParticipacion().toString());
					lineaParticipacionDTO = new LineaParticipacionDTO();
					lineaParticipacionDTO.setId(new LineaParticipacionId());
					lineaParticipacionDTO.getId().setIdTdParticipacion(participacion.getIdTdParticipacion());
					lineaParticipacionDTO.getId().setIdTmLinea(linea.getIdTmLinea());
					lineaParticipacionDTO.setComision(comision);
					lineaParticipacionDTO.setLinea(null);
					lineaParticipacionDTO.setParticipacion(null);	
					entityManager.persist(lineaParticipacionDTO);
				}
			}
			
			if (contratoPrimerExcedenteDTO!=null && contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente()!=null){
				listParticipaciones = pCPE;

				for (ParticipacionDTO participacionDTO : listParticipaciones) {
					participacion = new ParticipacionDTO();
					participacion = duplicaParticipacion(participacionDTO);
					participacion.setContratoPrimerExcedente(contratoPrimerExcedenteDTO);
					participacion.setContratoCuotaParte(null);
					entityManager.persist(participacion);					
					if (participacionDTO!=null && participacionDTO.getIdTdParticipacion()!=null && participacionDTO.getParticipacionCorredores()!=null){
						List<ParticipacionCorredorDTO> listaPC = participacionCorredorFacade.findByProperty("participacion.idTdParticipacion", participacionDTO.getIdTdParticipacion());
						for (ParticipacionCorredorDTO corredor : listaPC/*participacionDTO.getParticipacionCorredores()*/) {						
							participacionCorredorDTO = new ParticipacionCorredorDTO();
							participacionCorredorDTO.setParticipacion(participacion);
							participacionCorredorDTO.setPorcentajeParticipacion(corredor.getPorcentajeParticipacion());
							participacionCorredorDTO.setReaseguradorCorredor(corredor.getReaseguradorCorredor());
							entityManager.persist(participacionCorredorDTO);
						}
					}
					
					comision = participacionComision.get(participacionDTO.getIdTdParticipacion().toString());
					lineaParticipacionDTO = new LineaParticipacionDTO();
					lineaParticipacionDTO.setId(new LineaParticipacionId());
					lineaParticipacionDTO.getId().setIdTdParticipacion(participacion.getIdTdParticipacion());
					lineaParticipacionDTO.getId().setIdTmLinea(linea.getIdTmLinea());
					lineaParticipacionDTO.setComision(comision);
					lineaParticipacionDTO.setLinea(null);
					lineaParticipacionDTO.setParticipacion(null);			
					entityManager.persist(lineaParticipacionDTO);
				}
			}
			
			//linea.setContratoCuotaParte(contratoCuotaParteDTO);
			//linea.setContratoPrimerExcedente(contratoPrimerExcedenteDTO);
			//update(linea);
		
		return linea;
		} catch (RuntimeException  re) {	
			context.setRollbackOnly();
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	private ParticipacionDTO duplicaParticipacion(ParticipacionDTO participacionDTO){
		CuentaBancoDTO cuentaBancoDolares = new CuentaBancoDTO();
		CuentaBancoDTO cuentaBancoPesos = new CuentaBancoDTO();
		ParticipacionDTO participacion = new ParticipacionDTO();
		participacion.setContacto(participacionDTO.getContacto());
		participacion.setCuentaBancoDolares(participacionDTO.getCuentaBancoDolares());
		participacion.setCuentaBancoPesos(participacionDTO.getCuentaBancoPesos());
		participacion.setPorcentajeParticipacion(participacionDTO.getPorcentajeParticipacion());
		participacion.setTipo(participacionDTO.getTipo());
		participacion.setReaseguradorCorredor(participacionDTO.getReaseguradorCorredor());
		cuentaBancoDolares = entityManager.find(CuentaBancoDTO.class, participacionDTO.getCuentaBancoDolares().getId());
		participacion.setCuentaBancoDolares(cuentaBancoDolares);
		cuentaBancoPesos = entityManager.find(CuentaBancoDTO.class, participacionDTO.getCuentaBancoPesos().getId());
		participacion.setCuentaBancoPesos(cuentaBancoPesos);
		return participacion;
	}
	
	private LineaDTO duplicaLinea(LineaDTO lineaDTO){
		LineaDTO linea = new LineaDTO();
			linea.setSubRamo(lineaDTO.getSubRamo());
			linea.setEstatus(lineaDTO.getEstatus());
			linea.setFechaAutorizacion(lineaDTO.getFechaAutorizacion());
			linea.setFechaFinal(lineaDTO.getFechaFinal());
			linea.setFechaInicial(lineaDTO.getFechaInicial());
			linea.setMaximo(lineaDTO.getMaximo());
			linea.setTipoDistribucion(lineaDTO.getTipoDistribucion());
			linea.setUsuarioAutorizo(lineaDTO.getUsuarioAutorizo());
			linea.setContratoCuotaParte(null);
			linea.setContratoPrimerExcedente(null);
			linea.setLineaParticipaciones(null);
		return linea;
	}
	private ContratoCuotaParteDTO duplicaCCP(ContratoCuotaParteDTO contratoCuotaParteDTO, LineaDTO lineaDTO){
		ContratoCuotaParteDTO ccp = new ContratoCuotaParteDTO();
		if (contratoCuotaParteDTO==null || contratoCuotaParteDTO.getIdTmContratoCuotaParte()==null)return null;
			ccp.setEstatus(contratoCuotaParteDTO.getEstatus());
			ccp.setFechaAutorizacion(contratoCuotaParteDTO.getFechaAutorizacion());
			ccp.setFechaFinal(lineaDTO.getFechaFinal());
			ccp.setFechaInicial(lineaDTO.getFechaInicial());
			ccp.setFolioContrato(contratoCuotaParteDTO.getFolioContrato());
			ccp.setFormaPago(contratoCuotaParteDTO.getFormaPago());
			ccp.setIdTcMoneda(contratoCuotaParteDTO.getIdTcMoneda());
			ccp.setPorcentajeCesion(contratoCuotaParteDTO.getPorcentajeCesion());
			ccp.setPorcentajeRetencion(contratoCuotaParteDTO.getPorcentajeRetencion());
			ccp.setUsuarioAutorizo(contratoCuotaParteDTO.getUsuarioAutorizo());
		return ccp;
	}
	
	private ContratoPrimerExcedenteDTO duplicaCPE(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO, LineaDTO lineaDTO){
		ContratoPrimerExcedenteDTO cpe = new ContratoPrimerExcedenteDTO();
		if (contratoPrimerExcedenteDTO==null || contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente()==null) return null;
			cpe.setEstatus(contratoPrimerExcedenteDTO.getEstatus());
			cpe.setFechaAutorizacion(contratoPrimerExcedenteDTO.getFechaAutorizacion());
			cpe.setFechaFinal(lineaDTO.getFechaFinal());
			cpe.setFechaInicial(lineaDTO.getFechaInicial());
			cpe.setFolioContrato(contratoPrimerExcedenteDTO.getFolioContrato());
			cpe.setFormaPago(contratoPrimerExcedenteDTO.getFormaPago());
			cpe.setIdMoneda(contratoPrimerExcedenteDTO.getIdMoneda());
			cpe.setLimiteMaximo(contratoPrimerExcedenteDTO.getLimiteMaximo());
			cpe.setMontoPleno(contratoPrimerExcedenteDTO.getMontoPleno());
			cpe.setNumeroPlenos(contratoPrimerExcedenteDTO.getNumeroPlenos());
			cpe.setUsuarioAutorizo(contratoPrimerExcedenteDTO.getUsuarioAutorizo());
		return cpe;
	}
	
	public List<EjercicioDTO> obtenerEjercicios(){
		Date fechaInicialMax;
		Date fechaActual = new Date();
		Calendar calendar = Calendar.getInstance();
		Date fechaInicialMin;
		String queryString = "SELECT MIN(model.fechaInicial), MAX(model.fechaInicial) FROM LineaDTO model WHERE model.estatus = 1";
		List<EjercicioDTO> ejercicioDTOList = new ArrayList<EjercicioDTO>();
		CalculoEjerciciosDTO calculoEjerciciosDTO = new CalculoEjerciciosDTO();

		Query query = entityManager.createQuery(queryString);
		Object[] results = (Object[]) query.getSingleResult();
		if (results != null && results.length > 0){
			fechaInicialMin = (Date) results[0];
			fechaInicialMax = (Date) results[1];
			if (fechaInicialMin != null && fechaInicialMax != null){
				calendar.setTime(fechaActual);
				int anioActual = calendar.get(Calendar.YEAR);
				calendar.setTime(fechaInicialMax);
				int anioInicialMax = calendar.get(Calendar.YEAR);
				if (anioActual > anioInicialMax)
					fechaInicialMax = fechaActual;
				
				calculoEjerciciosDTO.setFechaInicial(fechaInicialMin);
				calculoEjerciciosDTO.setFechaFin(fechaInicialMax);
				ejercicioDTOList = calculoEjerciciosDTO.obtenerListaPeriodos();
			}
		}
		
		return ejercicioDTOList;
	}
	
	@SuppressWarnings("unchecked")
	public Object obtenerLineaPorSubRamo(BigDecimal idTcSubRamo, int ejercicio, int tipoReaseguro) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(ejercicio, 0, 1, 0, 0, 0);
		Date fechaInicial = calendar.getTime();
		calendar.set(ejercicio+1, 11, 31, 23, 59, 59);
		Date fechaFinal = calendar.getTime();
		String queryStr;
		Object objectToReturn = null;
		
		if (tipoReaseguro != TipoReaseguroFacade.TIPO_CONTRATO_FACULTATIVO){
			queryStr = "SELECT model FROM LineaDTO model WHERE model.subRamo.idTcSubRamo = :idTcSubRamo AND model.estatus = 1 " +
					"AND model.fechaInicial >= :fechaInicial AND model.fechaFinal <= :fechaFinal";
			
			if (tipoReaseguro == TipoReaseguroFacade.TIPO_CONTRATO_CUOTA_PARTE)
				queryStr = queryStr + " AND model.contratoCuotaParte.estatus = 1";
			else
				if (tipoReaseguro == TipoReaseguroFacade.TIPO_CONTRATO_PRIMER_EXCEDENTE)
					queryStr = queryStr + " AND model.contratoPrimerExcedente.estatus = 1";
			
			Query query = entityManager.createQuery(queryStr);
			query.setParameter("idTcSubRamo", idTcSubRamo);
			query.setParameter("fechaInicial", fechaInicial);
			query.setParameter("fechaFinal", fechaFinal);
			
			List<LineaDTO> lineaDTOList = query.getResultList();
			if (lineaDTOList != null && lineaDTOList.size() > 0)
				objectToReturn = lineaDTOList.get(0); // El query anterior en teor�a deber�a traer una sola l�nea pues es una l�nea por subramo por periodo contractual. Siendo as�, obtener primer elemento de la lista de l�neas.
		}else{
			if (tipoReaseguro == TipoReaseguroFacade.TIPO_CONTRATO_FACULTATIVO){
				queryStr = "SELECT DISTINCT model.contratoFacultativoDTO_1 FROM DetalleContratoFacultativoDTO model " +
						"WHERE model.idTcSubramo = :idTcSubRamo AND model.contratoFacultativoDTO_1.fechaInicial >= :fechaInicial " +
						"AND model.contratoFacultativoDTO_1.fechaFinal <= :fechaFinal AND model.contratoFacultativoDTO_1.estatus = 1";
				
				Query query = entityManager.createQuery(queryStr);
				query.setParameter("idTcSubRamo", idTcSubRamo);
				query.setParameter("fechaInicial", fechaInicial);
				query.setParameter("fechaFinal", fechaFinal);
				List<ContratoFacultativoDTO> contratoFacultativoDTOList = query.getResultList();
				if (contratoFacultativoDTOList != null && contratoFacultativoDTOList.size() > 0)
					objectToReturn = contratoFacultativoDTOList.get(0); // El query anterior en teor�a deber�a traer un solo Contrato pues es un contrato por subramo por periodo contractual. Siendo as�, obtener primer elemento de la lista de Contratos.
			}
		}
		
		return objectToReturn;
	}
	
	public List<SuscripcionDTO> obtenerSuscripcionesPorLinea(BigDecimal idTcSubRamo,int ejercicio ,int tipoReaseguro){
		LineaDTO lineaDTO;
		ContratoFacultativoDTO contratoFacultativoDTO;
		Object object = (LineaDTO) obtenerLineaPorSubRamo(idTcSubRamo, ejercicio, tipoReaseguro);
		BigDecimal formaPago = new BigDecimal("-1");
		List<SuscripcionDTO> suscripcionDTOList = new ArrayList<SuscripcionDTO>();
		Date fechaInicial;
		Date fechaFinal;
		if(object != null){
			lineaDTO = (LineaDTO) object;
			if (tipoReaseguro != TipoReaseguroFacade.TIPO_CONTRATO_FACULTATIVO){
				// Caso en que TipoReaseguro NO es Contrato Facultativo
				if (tipoReaseguro == TipoReaseguroFacade.TIPO_CONTRATO_CUOTA_PARTE){
					if (lineaDTO.getContratoCuotaParte() != null){
						formaPago = lineaDTO.getContratoCuotaParte().getFormaPago();
					}
				}else{
					if (tipoReaseguro == TipoReaseguroFacade.TIPO_CONTRATO_PRIMER_EXCEDENTE){
						if (lineaDTO.getContratoPrimerExcedente() != null){
							formaPago = lineaDTO.getContratoPrimerExcedente().getFormaPago();
						}
					}else{
						if (tipoReaseguro == TipoReaseguroFacade.TIPO_RETENCION){
							formaPago = new BigDecimal(LineaDTO.FORMA_PAGO_RETENCION_PURA);
						}
					}
				}
				fechaInicial = lineaDTO.getFechaInicial();
				fechaFinal = lineaDTO.getFechaFinal();
			}else{
				// Caso en que TipoReaseguro es Contrato Facultativo
				contratoFacultativoDTO = (ContratoFacultativoDTO) object;
				fechaInicial = contratoFacultativoDTO.getFechaInicial();
				fechaFinal = contratoFacultativoDTO.getFechaFinal();
				formaPago = contratoFacultativoDTO.getIdFormaDePago();
			}
			
			if (!formaPago.equals(new BigDecimal(-1))){
				CalculoSuscripcionesDTO calculoSuscripcionesDTO = new CalculoSuscripcionesDTO();
				calculoSuscripcionesDTO.setFechaInicial(fechaInicial);
				calculoSuscripcionesDTO.setFechaFinal(fechaFinal);
				calculoSuscripcionesDTO.setFormaPago(formaPago);
				calculoSuscripcionesDTO.setEjercicio(ejercicio);
				suscripcionDTOList = calculoSuscripcionesDTO.getListaSuscripciones();
			}
		}
		
		return suscripcionDTOList;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void desasociarContratoCPLinea(LineaDTO lineaDTO) {
		try{
			if (lineaDTO.getContratoCuotaParte() != null && lineaDTO.getContratoCuotaParte().getIdTmContratoCuotaParte() != null){
				String queryString = "DELETE FROM LineaParticipacionDTO model WHERE model.linea.idTmLinea = :idTmLinea AND " +
						"model.participacion.contratoCuotaParte.idTmContratoCuotaParte = :idTmContratoCuotaParte";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idTmLinea",lineaDTO.getIdTmLinea());
				query.setParameter("idTmContratoCuotaParte", lineaDTO.getContratoCuotaParte().getIdTmContratoCuotaParte());
				query.executeUpdate();
				lineaDTO.setContratoCuotaParte(null);
				entityManager.merge(lineaDTO);
			}
		} catch (RuntimeException re) {	
			context.setRollbackOnly();
			LogDeMidasEJB3.log("desasociarContratoCPLinea failed", Level.SEVERE, re);
			throw re;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void desasociarContratoPELinea(LineaDTO lineaDTO) {
		try{
			if (lineaDTO.getContratoPrimerExcedente() != null && lineaDTO.getContratoPrimerExcedente().getIdTmContratoPrimerExcedente() != null){
				String queryString = "DELETE FROM LineaParticipacionDTO model WHERE model.linea.idTmLinea = :idTmLinea AND " +
				"model.participacion.contratoPrimerExcedente.idTmContratoPrimerExcedente = :idTmContratoPrimerExcedente";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idTmLinea",lineaDTO.getIdTmLinea());
				query.setParameter("idTmContratoPrimerExcedente", lineaDTO.getContratoPrimerExcedente().getIdTmContratoPrimerExcedente());
				query.executeUpdate();
	
				lineaDTO.setContratoPrimerExcedente(null);
				entityManager.merge(lineaDTO);
			}
		} catch (RuntimeException re) {	
			context.setRollbackOnly();
			LogDeMidasEJB3.log("desasociarContratoPELinea failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public int obtenerEjercicioActual(){
		int ejercicioActual = 0;
		Calendar calendar = Calendar.getInstance();
		String queryString = "SELECT model FROM LineaDTO model WHERE model.estatus = 1 AND model.fechaInicial <= :fechaActual AND model.fechaFinal >= :fechaActual";
		Query query = entityManager.createQuery(queryString);
		query.setMaxResults(1);
		query.setParameter("fechaActual", new Date());
		LineaDTO lineaDTO = (LineaDTO )query.getSingleResult();
		calendar.setTime(lineaDTO.getFechaInicial());
		ejercicioActual = calendar.get(Calendar.YEAR);
		
		return ejercicioActual;
	}
	
	public BigDecimal obtenerTipoDistribucion(BigDecimal idToPoliza,BigDecimal idToCobertura){
		BigDecimal distribucion = null;
		if(idToPoliza != null && idToCobertura != null){
			try {
				LogDeMidasEJB3.log("Buscando tipo de distribucion de la p�liza: "+idToPoliza+", idToCobertura: "+idToCobertura+".", Level.INFO, null);
				String queryString = "select tipodistribucion from MIDAS.tmlinea where idtmlinea in ( select idtclinea from MIDAS.tmlineasoportereaseguro where idtopoliza = " + idToPoliza + ")"+
					"and idtcsubramo in (select idtcsubramo from MIDAS.tocobertura where idtocobertura = "+idToCobertura+")";
				Query query;
				query = entityManager.createNativeQuery(queryString);
				Object result = query.getSingleResult();
				distribucion = result != null ? (BigDecimal) query.getSingleResult() : null;
				LogDeMidasEJB3.log("Tipo de distribucion de la p�liza : "+idToPoliza+", idToCobertura: "+idToCobertura+": "+distribucion, Level.INFO, null);
			} catch (RuntimeException re) {
				LogDeMidasEJB3.log("Ocurri� un error al buscar el tipo de distribucion de la p�liza: "+idToPoliza+", idToCobertura: "+idToCobertura+".", Level.INFO, null);
				throw re;
			}
		}else{
			LogDeMidasEJB3.log("LineaFacade.obtenerTipoDistribucion. Se recibieron parametros nulos: idToPoliza = "+idToPoliza+", idToCobertura = "+idToCobertura, Level.SEVERE, null);
		}
		return distribucion;
	}
}