package mx.com.afirme.midas.contratos.contratoprimerexcedente;
// default package

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ContratoPrimerExcedenteDTO.
 * @see .ContratoPrimerExcedenteDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class ContratoPrimerExcedenteFacade  implements ContratoPrimerExcedenteFacadeRemote {

	 
    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ContratoPrimerExcedenteDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ContratoPrimerExcedenteDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public ContratoPrimerExcedenteDTO save(ContratoPrimerExcedenteDTO entity) {
    	LogDeMidasEJB3.log("saving ContratoPrimerExcedenteDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            LogDeMidasEJB3.log("save successful", Level.INFO, null);
            return entity;            			
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ContratoPrimerExcedenteDTO entity.
	  @param entity ContratoPrimerExcedenteDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public void delete(ContratoPrimerExcedenteDTO entity) {
    				LogDeMidasEJB3.log("deleting ContratoPrimerExcedenteDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ContratoPrimerExcedenteDTO.class, entity.getIdTmContratoPrimerExcedente());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ContratoPrimerExcedenteDTO entity and return it or a copy of it to the sender. 
	 A copy of the ContratoPrimerExcedenteDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ContratoPrimerExcedenteDTO entity to update
	 @return ContratoPrimerExcedenteDTO the persisted ContratoPrimerExcedenteDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public ContratoPrimerExcedenteDTO update(ContratoPrimerExcedenteDTO entity) {
    				LogDeMidasEJB3.log("updating ContratoPrimerExcedenteDTO instance", Level.INFO, null);
	        try {
	        	
	        	ContratoPrimerExcedenteDTO result = entityManager.merge(entity);
	        	//Se reasigna la propiedad porque al no ser una propiedad de persistencia se pierde en el merge
	        	//result.setNombreUsuarioLog("hola");
	        	
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 * Find the ContratoPrimerExcedenteDTO entity with a specific Id value.  
	 
	  @param id the id of the ContratoPrimerExcedenteDTO to query
	  	  @return ContratoPrimerExcedenteDTO found by query
	*/
    public ContratoPrimerExcedenteDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding ContratoPrimerExcedenteDTO instance with id: " + id, Level.INFO, null);
	        try {
            ContratoPrimerExcedenteDTO instance = entityManager.find(ContratoPrimerExcedenteDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ContratoPrimerExcedenteDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ContratoPrimerExcedenteDTO property to query
	  @param value the property value to match
	  	  @return List<ContratoPrimerExcedenteDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ContratoPrimerExcedenteDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ContratoPrimerExcedenteDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ContratoPrimerExcedenteDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ContratoPrimerExcedenteDTO entities.
	  	  @return List<ContratoPrimerExcedenteDTO> all ContratoPrimerExcedenteDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ContratoPrimerExcedenteDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ContratoPrimerExcedenteDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ContratoPrimerExcedenteDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find filtered ContratoPrimerExcedenteDTO entities.
	  	  @return List<ContratoPrimerExcedenteDTO> filtered ContratoPrimerExcedenteDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ContratoPrimerExcedenteDTO> listarFiltrado(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO) {
		try {
			String queryString = "select model from ContratoPrimerExcedenteDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (contratoPrimerExcedenteDTO == null)
				return null;
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicial", contratoPrimerExcedenteDTO.getFechaInicial());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaFinal", contratoPrimerExcedenteDTO.getFechaFinal());					
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ContratoPrimerExcedenteDTO> buscarPorFechaInicialFinal(Date fInicial, Date fFinal
		) {
					LogDeMidasEJB3.log("finding all ContratoPrimerExcedenteDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ContratoPrimerExcedenteDTO model";
			String sWhere = " where model.fechaInicial= :fechaInicial";
			sWhere += " and model.fechaFinal= :fechaFinal";
			
			Query query = entityManager.createQuery(queryString + sWhere);
			query.setParameter("fechaInicial", fInicial);
			query.setParameter("fechaFinal", fFinal);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ContratoPrimerExcedenteDTO> actualizaLineasPorIdLineas(String ids,Date fInicial, Date fFinal
		) {
					LogDeMidasEJB3.log("update all ContratoPrimerExcedenteDTO instances", Level.INFO, null);
			try {
				if (ids.trim().length()==0)return null;
			String queryString = "update ContratoPrimerExcedenteDTO model set ";
			queryString += " model.fechaInicial= :fechaInicial";
			queryString += " ,model.fechaFinal= :fechaFinal";
			String sWhere = " where model.idTmContratoPrimerExcedente in (" + ids + ")";
			
			Query query = entityManager.createQuery(queryString + sWhere);
			query.setParameter("fechaInicial", fInicial);
			query.setParameter("fechaFinal", fFinal);
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("update all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	public boolean actualizaFechasLineasPorIdLineas(String ids,Date fInicial, Date fFinal
		) {
					LogDeMidasEJB3.log("update all ContratoPrimerExcedenteDTO instances", Level.INFO, null);
			try {
				if (ids==null || ids.trim().length()==0)return false;
			String queryString = "update ContratoPrimerExcedenteDTO model set ";
			queryString += " model.fechaInicial= :fechaInicial";
			queryString += " ,model.fechaFinal= :fechaFinal";
			String sWhere = " where model.idTmContratoPrimerExcedente in (" + ids + ")";
			
			Query query = entityManager.createQuery(queryString + sWhere);
			query.setParameter("fechaInicial", fInicial);
			query.setParameter("fechaFinal", fFinal);
			query.executeUpdate();
			return true;
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("update all failed", Level.SEVERE, re);
				throw re;
		}		
	}
	
	/**
	 *Consulta el contrato primer excedente del ejercicio anterior que tenga los mismos atributos que el estado de cuenta recibido 
	 *(reasegurador, corredor, moneda, ramo, subramo y forma de pago).
	 * @param estadoCuentaDTO
	 * @return ContratoPrimerExcedenteDTO del ejercicio anterior, null si no existe.
	 */
	@SuppressWarnings("unchecked")
	public ContratoPrimerExcedenteDTO obtenerContratoPrimerExcedenteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO){
		ContratoPrimerExcedenteDTO contrato1EAnterior = null;
		String logInfo;
		if(estadoCuentaDTO != null && estadoCuentaDTO.getIdEstadoCuenta() != null && estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
			String queryString = "";
			Calendar fechaInicio = Calendar.getInstance();
			Calendar fechaFin = Calendar.getInstance();
			fechaInicio.set(estadoCuentaDTO.getEjercicio()-1,//anio anterior
					Calendar.APRIL,//abril
					1,0,0,0);//d�a primero, hora 0, minuto 0 y segundo 0
			fechaFin.set(estadoCuentaDTO.getEjercicio(),//mismo anio
					Calendar.MARCH,//marzo
					31,23,59,59);//d�a 31, hora 23, minuto 59 y segundo 59
			//Ajuste a las fechas para validar con rango cerrado
			fechaInicio.add(Calendar.DATE, -1);
			fechaFin.add(Calendar.DATE, 1);
			Query query;
			//Si tiene corredor, se busca el corredor como participante del contrato, posteriormente, el reasegurador como participante del corredor
			if(estadoCuentaDTO.getCorredorDTO() != null && estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor() != null){
				queryString = "select model.participacion.contratoPrimerExcedente from ParticipacionCorredorDTO model, LineaDTO linea where " +
						"model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtccorredor and " +
						"model.participacion.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreasegurador and " +
						"model.participacion.contratoPrimerExcedente is not null and " +
						"model.participacion.contratoPrimerExcedente.formaPago = :formaPago and " +
						"model.participacion.contratoPrimerExcedente.idMoneda = :idTcMoneda and " +
						"linea.contratoPrimerExcedente.idTmContratoPrimerExcedente = model.participacion.contratoPrimerExcedente.idTmContratoPrimerExcedente and " +
						"linea.subRamo.idTcSubRamo = :idTcSubRamo and " +
						"linea.subRamo.ramoDTO.idTcRamo = :idTcRamo and " +
						"model.participacion.contratoPrimerExcedente.fechaInicial > :fechaInicial and " +
						"model.participacion.contratoPrimerExcedente.fechaFinal < :fechaFinal ";
				query = entityManager.createQuery(queryString);
				query.setParameter("idtccorredor",estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
				query.setParameter("idtcreasegurador",estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor());
			}
			else{//Si no tiene corredor, s�lo se busca el reasegurador en la lista de participaciones
				queryString = "select model.contratoPrimerExcedente from ParticipacionDTO model, LineaDTO linea where " +
						"model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreasegurador and " +
						"model.contratoPrimerExcedente is not null and " +
						"model.contratoPrimerExcedente.formaPago = :formaPago and " +
						"model.contratoPrimerExcedente.idMoneda= :idTcMoneda and " +
						"linea.contratoPrimerExcedente.idTmContratoPrimerExcedente = model.contratoPrimerExcedente.idTmContratoPrimerExcedente and " +
						"linea.subRamo.idTcSubRamo = :idTcSubRamo and " +
						"linea.subRamo.ramoDTO.idTcRamo = :idTcRamo and " +
						"model.contratoPrimerExcedente.fechaInicial > :fechaInicial and " +
						"model.contratoPrimerExcedente.fechaFinal < :fechaFinal ";
				query = entityManager.createQuery(queryString);
				query.setParameter("idtcreasegurador",estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
			}
			query.setParameter("formaPago",estadoCuentaDTO.getContratoPrimerExcedenteDTO().getFormaPago());
			query.setParameter("idTcMoneda",estadoCuentaDTO.getIdMoneda());
			query.setParameter("idTcSubRamo",estadoCuentaDTO.getLineaDTO().getSubRamo().getIdTcSubRamo());
			query.setParameter("idTcRamo",estadoCuentaDTO.getLineaDTO().getSubRamo().getRamoDTO().getIdTcRamo());
			query.setParameter("fechaInicial",new java.sql.Date(fechaInicio.getTime().getTime()));
			query.setParameter("fechaFinal",new java.sql.Date(fechaFin.getTime().getTime()));
			
			logInfo = "Buscando Contrato Primer Excedente Anterior (reasegurador: %s, corredor: %s, formaPago: %s, idMoneda: %s, idSubRamo: %s, idRamo: %s, fechaInicial: %s, fechaFinal: %s)";
			logInfo = String.format(logInfo, estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().toString(),
					(estadoCuentaDTO.getCorredorDTO() != null && estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor() != null ? estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor().toString() : "null"),
					estadoCuentaDTO.getContratoPrimerExcedenteDTO().getFormaPago().toString(),
					(""+estadoCuentaDTO.getIdMoneda()),
					estadoCuentaDTO.getLineaDTO().getSubRamo().getIdTcSubRamo().toString(),
					estadoCuentaDTO.getLineaDTO().getSubRamo().getRamoDTO().getIdTcRamo().toString(),
					DateFormat.getDateInstance().format(fechaInicio.getTime()),
					DateFormat.getDateInstance().format(fechaFin.getTime()));
			LogDeMidasEJB3.log(logInfo, Level.INFO, null);
			List<ContratoPrimerExcedenteDTO> listaContratosEncontrados = query.getResultList();
			if(listaContratosEncontrados != null && !listaContratosEncontrados.isEmpty()){
				if(listaContratosEncontrados.size() == 1){
					LogDeMidasEJB3.log(listaContratosEncontrados.size()+" Contrato(s) encontrado(s).", Level.INFO, null);
					contrato1EAnterior = listaContratosEncontrados.get(0);
				}else{
					LogDeMidasEJB3.log(listaContratosEncontrados.size()+" Contrato(s) encontrado(s). Se ignorar�n por ser consistente, s�lo debe existir uno o ninguno.", Level.INFO, null);
				}
			}
			else
				LogDeMidasEJB3.log("Ning�n Contrato encontrado.", Level.INFO, null);
		}
		return contrato1EAnterior;
	}
	
	/**
	 *Consulta la lista de contratos primer excedente de ejercicios anteriores que tengan los mismos atributos que el estado de cuenta recibido 
	 *(reasegurador, corredor, moneda, ramo, subramo y forma de pago).
	 * @param estadoCuentaDTO
	 * @return ContratoPrimerExcedenteDTO del ejercicio anterior, null si no existe.
	 */
	@SuppressWarnings("unchecked")
	public List<ContratoPrimerExcedenteDTO> obtenerListaContratoPrimerExcedenteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO){
		List<ContratoPrimerExcedenteDTO> listaContratosEncontrados = null;
		String logInfo;
		if(estadoCuentaDTO != null && estadoCuentaDTO.getIdEstadoCuenta() != null && estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
			String queryString = "";
			Calendar fechaFin = Calendar.getInstance();
			fechaFin.set(estadoCuentaDTO.getEjercicio(),//mismo anio
					Calendar.MARCH,//marzo
					31,23,59,59);//d�a 31, hora 23, minuto 59 y segundo 59
			//Ajuste a las fechas para validar con rango cerrado
			fechaFin.add(Calendar.DATE, 1);
			Query query;
			//Si tiene corredor, se busca el corredor como participante del contrato, posteriormente, el reasegurador como participante del corredor
			if(estadoCuentaDTO.getCorredorDTO() != null && estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor() != null){
				queryString = "select model.participacion.contratoPrimerExcedente from ParticipacionCorredorDTO model, LineaDTO linea where " +
						"model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtccorredor and " +
						"model.participacion.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreasegurador and " +
						"model.participacion.contratoPrimerExcedente is not null and " +
						"model.participacion.contratoPrimerExcedente.formaPago = :formaPago and " +
						"model.participacion.contratoPrimerExcedente.idMoneda = :idTcMoneda and " +
						"linea.contratoPrimerExcedente.idTmContratoPrimerExcedente = model.participacion.contratoPrimerExcedente.idTmContratoPrimerExcedente and " +
						"linea.subRamo.idTcSubRamo = :idTcSubRamo and " +
						"linea.subRamo.ramoDTO.idTcRamo = :idTcRamo and " +
						"model.participacion.contratoPrimerExcedente.fechaFinal < :fechaFinal and " +
						"model.participacion.contratoPrimerExcedente.estatus = 1 order by model.participacion.contratoPrimerExcedente.fechaFinal";
				query = entityManager.createQuery(queryString);
				query.setParameter("idtccorredor",estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
				query.setParameter("idtcreasegurador",estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor());
			}
			else{//Si no tiene corredor, s�lo se busca el reasegurador en la lista de participaciones
				queryString = "select model.contratoPrimerExcedente from ParticipacionDTO model, LineaDTO linea where " +
						"model.reaseguradorCorredor.idtcreaseguradorcorredor = :idtcreasegurador and " +
						"model.contratoPrimerExcedente is not null and " +
						"model.contratoPrimerExcedente.formaPago = :formaPago and " +
						"model.contratoPrimerExcedente.idMoneda= :idTcMoneda and " +
						"linea.contratoPrimerExcedente.idTmContratoPrimerExcedente = model.contratoPrimerExcedente.idTmContratoPrimerExcedente and " +
						"linea.subRamo.idTcSubRamo = :idTcSubRamo and " +
						"linea.subRamo.ramoDTO.idTcRamo = :idTcRamo and " +
						"model.contratoPrimerExcedente.fechaFinal < :fechaFinal and " +
						"model.contratoPrimerExcedente.estatus = 1 order by model.contratoPrimerExcedente.fechaFinal";
				query = entityManager.createQuery(queryString);
				query.setParameter("idtcreasegurador",estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
			}
			query.setParameter("formaPago",estadoCuentaDTO.getContratoPrimerExcedenteDTO().getFormaPago());
			query.setParameter("idTcMoneda",estadoCuentaDTO.getIdMoneda());
			query.setParameter("idTcSubRamo",estadoCuentaDTO.getLineaDTO().getSubRamo().getIdTcSubRamo());
			query.setParameter("idTcRamo",estadoCuentaDTO.getLineaDTO().getSubRamo().getRamoDTO().getIdTcRamo());
			query.setParameter("fechaFinal",new java.sql.Date(fechaFin.getTime().getTime()));
			
			logInfo = "Buscando Contratos Primer Excedente Anteriores (reasegurador: %s, corredor: %s, formaPago: %s, idMoneda: %s, idSubRamo: %s, idRamo: %s, fechaFinal: %s)";
			logInfo = String.format(logInfo, estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().toString(),
					(estadoCuentaDTO.getCorredorDTO() != null && estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor() != null ? estadoCuentaDTO.getCorredorDTO().getIdtcreaseguradorcorredor().toString() : "null"),
					estadoCuentaDTO.getContratoPrimerExcedenteDTO().getFormaPago().toString(),
					(""+estadoCuentaDTO.getIdMoneda()),
					estadoCuentaDTO.getLineaDTO().getSubRamo().getIdTcSubRamo().toString(),
					estadoCuentaDTO.getLineaDTO().getSubRamo().getRamoDTO().getIdTcRamo().toString(),
					DateFormat.getDateInstance().format(fechaFin.getTime()));
			LogDeMidasEJB3.log(logInfo, Level.INFO, null);
			listaContratosEncontrados = query.getResultList();
			if(listaContratosEncontrados != null && !listaContratosEncontrados.isEmpty()){
				LogDeMidasEJB3.log(listaContratosEncontrados.size()+" Contrato(s) Primer Excedente encontrado(s).", Level.INFO, null);
			}
			else
				LogDeMidasEJB3.log("Ning�n Contrato encontrado.", Level.INFO, null);
		}
		return listaContratosEncontrados;
	}
}