package mx.com.afirme.midas.emision;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.diasgracia.DiasGraciaServicio;
import mx.com.afirme.midas.cotizacion.documento.DocPrevencionOperIlicitasDTO;
import mx.com.afirme.midas.cotizacion.documento.DocPrevencionOperIlicitasFacadeRemote;
import mx.com.afirme.midas.cotizacion.resumen.ResumenCotizacionFacadeRemote;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.solicitud.SolicitudFacadeRemote;

@Stateless
public class EmisionFacade implements EmisionFacadeRemote {

	public PolizaDTO emitirPolizaCasa (CotizacionDTO cotizacion) {
		
		return emitir(cotizacion);
		
	}
	
	
	private PolizaDTO emitir(CotizacionDTO cotizacion) {
		
		PolizaDTO polizaEmitida = null;
		BigDecimal idPolizaEmitida = null;
		BigDecimal numeroEndosoEmitido = null;
		
		//Se establecen los dias de gracia para el pago de los recibos de la poliza a emitir
		cotizacion.setDiasGracia(diasGraciaServicio.obtenerDiasGraciaDefault());
		
//		Validacion de Cotizacion
		String erroresValidacion = validarCotizacion(cotizacion);
		
		if (erroresValidacion != null && !erroresValidacion.trim().equals("")) {
			throw new RuntimeException("Errores en validaci\u00f3n para emitir: " + erroresValidacion);
		}
		
//		Identifica por medio de la solicitud si se trata de la emision de una poliza o de un endoso		
//		Si se trata de una poliza
		if (cotizacion.getSolicitudDTO().getClaveTipoSolicitud().equals (SistemaPersistencia.CLAVE_SOLICITUD_POLIZA)) {
//			Se emite la poliza		
//			Se obtienen las coberturas contratadas	
			List<CoberturaCotizacionDTO> coberturasContratadas = coberturaCotizacionFacade.listarCoberturasContratadas(
					cotizacion.getIdToCotizacion());
						
//			Se calculan los totales del resumen de la cotizacion	
			try {
				cotizacion = resumenCotizacionFacade.calcularTotalesResumenCotizacion(cotizacion, coberturasContratadas);
			} catch (SystemException e) {
				throw new RuntimeException(e);
			}
			cotizacion.setPorcentajeIva(cotizacion.getFactorIVA());
			
			cotizacionFacade.update(cotizacion);
			
			if (cotizacion.getDiasPorDevengar() == null 
					|| cotizacion.getDerechosPoliza() == null 
					||	cotizacion.getPorcentajePagoFraccionado() == null){
				throw new RuntimeException("Error al generar la P\u00f3liza: Los datos necesarios para la emisi\u00f3n no fueron obtenidos exitosamente");
			}
			
//			Se crea la estructura de la poliza (esta en SP de BD)
			Map<String, String> mensaje;
			try {
				mensaje = polizaFacadeInterfaz.emitePoliza(cotizacion);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			
			if(mensaje.get("icono").equals("30")) { //Exito de Emision
//				Se cargan los valores de la poliza emitida
				idPolizaEmitida = new BigDecimal(mensaje.get("idpoliza"));
				numeroEndosoEmitido = new BigDecimal("0");
								
//				Se obtiene la poliza
				polizaEmitida = polizaFacade.findById(new BigDecimal(mensaje.get("idpoliza")));
//				Se cambia el estatus de la cotizacion a EMITIDO
				cotizacion.setClaveEstatus(SistemaPersistencia.ESTATUS_COT_EMITIDA);
				
				cotizacionFacade.update(cotizacion);
				
//				Se generan los recibos de la poliza	
				polizaFacade.generarRecibosEndoso(polizaEmitida, new Short(numeroEndosoEmitido.toString()));
			
			} else { //Algo fallo durante la emision
				throw new RuntimeException("Error al generar la P\u00f3liza: Error al generar la estructura de la p\u00f3liza");
			}
		}
			
//		Se registran estadisticas
		solicitudFacade.registraEstadisticas(cotizacion.getSolicitudDTO().getIdToSolicitud(), 
				cotizacion.getIdToCotizacion(), idPolizaEmitida, numeroEndosoEmitido, 
				SistemaPersistencia.USUARIO_SISTEMA, SistemaPersistencia.SE_EMITE_COTIZACION);
		
//		La notificacion a reaseguro se maneja en un servicio independiente de este proceso		
		return polizaEmitida;
				
	}
	
	public boolean reexpidePoliza(BigDecimal idPoliza, BigDecimal idCotizacion) {
		return polizaFacadeInterfaz.reexpidePoliza(idPoliza, idCotizacion);
	}
	
	private String validarCotizacion(CotizacionDTO cotizacion) {
		
		StringBuilder infoErrores = new StringBuilder("");
		
		int tipoRetroActividadDiferimiento=validaRetroActividadDiferimiento(cotizacion);
		int tipoVigenciaMinimaMaxima=validaVigenciaMinimaMaxima(cotizacion);
		
//		TODO Validar en caso de Renovacion
//		if (cotizacion.getSolicitudDTO().getEsRenovacion().intValue() == SistemaPersistencia.ES_RENOVACION) {
//			int tipoRetroDiferimientoRenovacion = validaRetroActividadDiferimientoRenovacion(cotizacion);
//			if (cotizacion.getClaveAutRetroacDifer() != null
//					&& cotizacion.getClaveAutRetroacDifer().equals(SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA)) {
//				if (tipoRetroDiferimientoRenovacion == 1)
//					infoErrores.append(getMsg("cotizacion.renovacion.retroactividad"));
//			}
//		}
		
//		TODO Validar CURP de Asegurado cuando sea necesario
//		//Asegurado
//		ClienteDTO clienteDTO = null;
//		
//		clienteDTO = clienteFacade.findById(cotizacion.getIdToPersonaAsegurado(), SistemaPersistencia.USUARIO_SISTEMA);					
//
//		if(clienteDTO != null){
//			if(!ValidaCURPDN.getInstancia().esValido(clienteDTO))
//				errores.add("cotizacion.asegurado.curp.invalido");
//		}
			
		
//		TODO Validar CURP de Contratante cuando sea necesario
//		//Contratante
//		clienteDTO = new ClienteDTO();
//		clienteDTO = clienteFacade.findById(cotizacion.getIdToPersonaContratante(), SistemaPersistencia.USUARIO_SISTEMA);
//
//		if(clienteDTO != null){
//			if(!ValidaCURPDN.getInstancia().esValido(clienteDTO))
//				errores.add("cotizacion.contratante.curp.invalido");
//		}
		
		
		
		if (cotizacion.getDiasGracia() == null) {
			infoErrores.append(getMsg("cotizacion.diasGracia.vacia"));
		}
		else{
			if(cotizacion.getClaveAutorizacionDiasGracia() != null && (
					cotizacion.getClaveAutorizacionDiasGracia().shortValue() == SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA ||
					cotizacion.getClaveAutorizacionDiasGracia().shortValue() == SistemaPersistencia.ESTATUS_RECHAZADA )){
				infoErrores.append(getMsg("cotizacion.diasGracia.autorizacion"));
			}
		}

		if (cotizacion.getFolioPolizaAsociada() != null && !cotizacion.getFolioPolizaAsociada().equals("")) {
			// Verificar que la poliza asociada exista
			if (!existePoliza(cotizacion.getFolioPolizaAsociada())) {
				infoErrores.append(getMsg("cotizacion.folioPolizaAsociada.noExiste"));
			}
		}

		if (cotizacion.getTipoNegocioDTO() == null) {
			infoErrores.append(getMsg("cotizacion.tipoNegocio.vacia"));
		}
		
		if(cotizacion.getClaveAutRetroacDifer() != null && 
				cotizacion.getClaveAutRetroacDifer().equals(SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA)){
		    if (tipoRetroActividadDiferimiento==1){
		    	infoErrores.append(getMsg("cotizacion.retroactividad"));
		    }else if(tipoRetroActividadDiferimiento==2){
		    	infoErrores.append(getMsg("cotizacion.diferimiento"));
		    }
		}

		if(cotizacion.getClaveAutVigenciaMaxMin() != null && 
				cotizacion.getClaveAutVigenciaMaxMin().equals(SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA)){
		    if (tipoVigenciaMinimaMaxima==1){
		    	infoErrores.append(getMsg("cotizacion.vigencia.minima"));
		    }else if(tipoVigenciaMinimaMaxima==2){
		    	infoErrores.append(getMsg("cotizacion.vigencia.maxima"));
		    }
		    
		}
		
		if(cotizacion.getClaveAutRetroacDifer() != null && 
				cotizacion.getClaveAutRetroacDifer().shortValue() == SistemaPersistencia.ESTATUS_RECHAZADA){
			infoErrores.append(getMsg("cotizacion.retroactividad.rechazada"));
		}

		if(cotizacion.getClaveAutVigenciaMaxMin() != null && 
				cotizacion.getClaveAutVigenciaMaxMin().shortValue() == SistemaPersistencia.ESTATUS_RECHAZADA){
			infoErrores.append(getMsg("cotizacion.vigencia.rechazada"));
		}	
		
		if (cotizacion.getIdMedioPago() == null) {
			infoErrores.append(getMsg("cotizacion.medioPago.vacia"));
		}

		if (!validarDocumentosComplementar(cotizacion)) {
			infoErrores.append(getMsg("cotizacion.documentosOperacionesIlicitas.vacia"));
		}

		
		// Se valida con Reaseguro si autoriza la emision
		//(No aplica la validacion para CASA porque no existen contratos facultativos)
//		List<String> motivoError = new ArrayList<String>();
//		if( !soporteReaseguroFacade.autorizadoEmision(cotizacion.getSolicitudDTO().getClaveTipoEndoso(),motivoError)){
//			for(String error: motivoError)
//				if(error != null)
//					infoErrores.append(error);
//		}   	

		//Validacion para no emitir en un periodo determinado. 
		Date fechaInicial = Utilerias.obtenerFechaDeCadena(getMsg("midas.emision.validacion.fechanoemitir.inicio"));
		Date fechaFinal = Utilerias.obtenerFechaDeCadena(getMsg("midas.emision.validacion.fechanoemitir.fin"));
		
		if(Utilerias.fechaEntreRango(new Date(), fechaInicial, fechaFinal)){
			infoErrores.append(getMsg("cotizacion.emitir.sinAutorizacionDelSistema.PorFecha"));
		}
				
		if(cotizacion.getClaveAutorizacionRecargoPagoFraccionado() != null && 
				cotizacion.getClaveAutorizacionRecargoPagoFraccionado().compareTo(SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA) == 0){
			infoErrores.append(getMsg("cotizacion.rpf.sinAutorizar"));
		}
		
		if(cotizacion.getClaveAutorizacionRecargoPagoFraccionado() != null && 
				cotizacion.getClaveAutorizacionRecargoPagoFraccionado().compareTo(SistemaPersistencia.ESTATUS_RECHAZADA) == 0){
			infoErrores.append(getMsg("cotizacion.rpf.rechazado"));
		}
		
		if(cotizacion.getClaveAutorizacionDerechos() != null && 
				cotizacion.getClaveAutorizacionDerechos().compareTo(SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA) == 0){
			infoErrores.append(getMsg("cotizacion.derechos.sinAutorizar"));
		}
		
		if(cotizacion.getClaveAutorizacionDerechos() != null && 
				cotizacion.getClaveAutorizacionDerechos().compareTo(SistemaPersistencia.ESTATUS_RECHAZADA) == 0){
			infoErrores.append(getMsg("cotizacion.derechos.rechazados"));
		}
		
		return infoErrores.toString();
		
	}
	
	private  int validaRetroActividadDiferimiento(CotizacionDTO cotizacionDTO) {
    	Date fechaSolicitud = cotizacionDTO.getSolicitudDTO().getFechaCreacion();
		Date fechaInicioVigencia = cotizacionDTO.getFechaInicioVigencia();
		int tipo=0;
		if (fechaInicioVigencia != null && fechaSolicitud != null) {
		    //Retroactividad
		    if(!validarRetroactividad(fechaInicioVigencia, fechaSolicitud)){
			 if(cotizacionDTO.getClaveAutRetroacDifer()==null || 
				    !SistemaPersistencia.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutRetroacDifer().toString())){
	    			 tipo=1;
	    			 cotizacionDTO.setClaveAutRetroacDifer(SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA); 
	    			 cotizacionDTO.setFechaSolAutRetroacDifer(Calendar.getInstance().getTime());
	    			 
	    		    }
		    }
		    
		    //Diferimiento
		    if(!validarDiferimiento(fechaInicioVigencia, fechaSolicitud)){
			 if(cotizacionDTO.getClaveAutRetroacDifer()==null || 
				 !SistemaPersistencia.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutRetroacDifer().toString())){
	   			 	tipo=2;
	   			 	cotizacionDTO.setClaveAutRetroacDifer(SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA);
	   			 	cotizacionDTO.setFechaSolAutRetroacDifer(Calendar.getInstance().getTime());
	   			 }
		    }
		    
		    if(tipo==0 ){
			if(cotizacionDTO.getClaveAutRetroacDifer()==null || 
				 !SistemaPersistencia.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutRetroacDifer().toString())){
			    cotizacionDTO.setClaveAutRetroacDifer(SistemaPersistencia.ESTATUS_AUTORIZACION_NO_REQUERIDA); 
			    cotizacionDTO.setFechaSolAutRetroacDifer(null);
			    
			    }
		   }
		   cotizacionFacade.update(cotizacionDTO);
		}
		return tipo;
		
	}
	
	private  int validaVigenciaMinimaMaxima(CotizacionDTO cotizacionDTO) {
    	Date fechaInicioVigencia = cotizacionDTO.getFechaInicioVigencia();
		Date fechaFinVigencia = cotizacionDTO.getFechaFinVigencia();
		int tipo=0;
		
		if (fechaInicioVigencia != null && fechaFinVigencia != null) {
		     //Vigencia Minima
		    if(!validarVigenciaMinima(fechaInicioVigencia, fechaFinVigencia)){
			 if(cotizacionDTO.getClaveAutVigenciaMaxMin()==null || 
				    !SistemaPersistencia.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutVigenciaMaxMin().toString())){
	    			 tipo=1;
	    			 cotizacionDTO.setClaveAutVigenciaMaxMin(SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA); 
	    			 cotizacionDTO.setFechaSolAutVigenciaMaxMin(Calendar.getInstance().getTime());
			    }
		    }
		    //Vigencia Maxima
		    if(!validarVigenciaMaxima(fechaInicioVigencia, fechaFinVigencia)){
			 if(cotizacionDTO.getClaveAutVigenciaMaxMin()==null || 
				 !SistemaPersistencia.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutVigenciaMaxMin().toString())){
	   			 	tipo=2;
	   			 	cotizacionDTO.setClaveAutVigenciaMaxMin(SistemaPersistencia.ESTATUS_AUTORIZACION_REQUERIDA);
	   			 	cotizacionDTO.setFechaSolAutVigenciaMaxMin(Calendar.getInstance().getTime());
			   }
		    }
		    if(tipo==0){
			if(cotizacionDTO.getClaveAutVigenciaMaxMin()==null || 
				 !SistemaPersistencia.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutVigenciaMaxMin().toString())){
			    cotizacionDTO.setClaveAutVigenciaMaxMin(SistemaPersistencia.ESTATUS_AUTORIZACION_NO_REQUERIDA); 
			    cotizacionDTO.setFechaSolAutVigenciaMaxMin(null);
			}
		    }
		    cotizacionFacade.update(cotizacionDTO);
		}
		return tipo;
	}
		
	
	private boolean validarDocumentosComplementar(CotizacionDTO cotizacionDTO) {
		boolean esValido = true;
		List<DocPrevencionOperIlicitasDTO> documentos = docPrevencionOperIlicitasFacade.findByProperty("idToCotizacion", cotizacionDTO.getIdToCotizacion());
		if (cotizacionDTO.getClaveAutoEmisionDocOperIlicitas() != null) {
			if (cotizacionDTO.getClaveAutoEmisionDocOperIlicitas() != SistemaPersistencia.ESTATUS_AUTORIZADA) {
				if (documentos == null || documentos.size() == 0) {
					esValido = false;
				}
			}

		}
		return esValido;
	}

	private boolean existePoliza(String numeroDePolizaConFormato) {
		boolean existePoliza = false;
		
		String[] numeroDePolizaArray = numeroDePolizaConFormato.split("-");
		String codigoProducto = numeroDePolizaArray[0].substring(0, 2);
		String codigoTipoPoliza = numeroDePolizaArray[0].substring(2, 4);
		Integer numeroPoliza = new Integer(numeroDePolizaArray[1]);
		Integer numeroRenovacion = new Integer(numeroDePolizaArray[2]);
		
		PolizaDTO polizaDTO = new PolizaDTO();
		polizaDTO.setCodigoProducto(codigoProducto);
		polizaDTO.setCodigoTipoPoliza(codigoTipoPoliza);
		polizaDTO.setNumeroPoliza(numeroPoliza);
		polizaDTO.setNumeroRenovacion(numeroRenovacion);
		
		List<PolizaDTO> listaPoliza = polizaFacade.buscarFiltrado(polizaDTO);
		
		if (listaPoliza != null && listaPoliza.size() == 1) {
			existePoliza = true;
		}
		
		return existePoliza;
	}
	
	private String getMsg(String llave) {
		return Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, llave);
	}
	
	private boolean validarVigenciaMaxima(Date fechaInicioVigencia,
			Date fechaFinVigencia) {
		boolean esValido = true;
		final long diasMaximo = Long.parseLong(getMsg("midas.emision.vigencia.dias.maximo"));
		long diasDiferencia = diasDiferencia(fechaInicioVigencia, fechaFinVigencia);
		if (diasDiferencia > diasMaximo) {
			esValido = false;
		}
		return esValido;
	}
	
	private boolean validarVigenciaMinima(Date fechaInicioVigencia,
			Date fechaFinVigencia) {
		boolean esValido = true;
		final long diasMinimo = Long.parseLong(getMsg("midas.emision.vigencia.dias.minimo"));
		long diasDiferencia = diasDiferencia(fechaInicioVigencia, fechaFinVigencia);
		if (diasDiferencia < diasMinimo) {
			esValido = false;
		}
		return esValido;
	}
	
	private boolean validarDiferimiento(Date fechaInicioVigencia,
			Date fechaSolicitud) {
		boolean esValido = true;
		if (fechaInicioVigencia.compareTo(fechaSolicitud) > 0) {
			long diasDiferencia = diasDiferencia(fechaInicioVigencia, fechaSolicitud);
			if (diasDiferencia > 15) {
				esValido = false;
			}
		}
		return esValido;
	}

	
	private boolean validarRetroactividad(Date fechaInicioVigencia, Date fechaSolicitud) {
		boolean esValido = true;
		if (fechaInicioVigencia.compareTo(fechaSolicitud) < 0) {
			long diasDiferencia = diasDiferencia(fechaInicioVigencia, fechaSolicitud);
			if (diasDiferencia > 15) {
				esValido = false;
			}
		}
		return esValido;
	}
	
	private long diasDiferencia(Date fechaUno, Date fechaDos) {
	    	    
		long ms;
		final long DIAS_MILISEGUNDOS = 24 * 60 * 60 * 1000;
		if (fechaUno.compareTo(fechaDos) > 0) {
			ms = fechaUno.getTime() - fechaDos.getTime();
		}
		else {
			ms = fechaDos.getTime() - fechaUno.getTime();
		}
		return ms / DIAS_MILISEGUNDOS;	
	}
	
	
	private DocPrevencionOperIlicitasFacadeRemote docPrevencionOperIlicitasFacade;
	
	private CotizacionFacadeRemote cotizacionFacade;
	
	private PolizaFacadeRemote polizaFacade;
		
	private CoberturaCotizacionFacadeRemote coberturaCotizacionFacade;
	
	private ResumenCotizacionFacadeRemote resumenCotizacionFacade;
	
	private mx.com.afirme.midas.interfaz.poliza.PolizaFacadeRemote polizaFacadeInterfaz;
	
	private SolicitudFacadeRemote solicitudFacade;
	
	private DiasGraciaServicio diasGraciaServicio;
	
	@EJB
	public void setDocPrevencionOperIlicitasFacade(
			DocPrevencionOperIlicitasFacadeRemote docPrevencionOperIlicitasFacade) {
		this.docPrevencionOperIlicitasFacade = docPrevencionOperIlicitasFacade;
	}
	
	@EJB
	public void setCotizacionFacade(CotizacionFacadeRemote cotizacionFacade) {
		this.cotizacionFacade = cotizacionFacade;
	}
	
	@EJB
	public void setPolizaFacade(PolizaFacadeRemote polizaFacade) {
		this.polizaFacade = polizaFacade;
	}
	
	@EJB
	public void setCoberturaCotizacionFacade(
			CoberturaCotizacionFacadeRemote coberturaCotizacionFacade) {
		this.coberturaCotizacionFacade = coberturaCotizacionFacade;
	}
	
	@EJB
	public void setResumenCotizacionFacade(
			ResumenCotizacionFacadeRemote resumenCotizacionFacade) {
		this.resumenCotizacionFacade = resumenCotizacionFacade;
	}
	
	@EJB
	public void setPolizaFacadeInterfaz(
			mx.com.afirme.midas.interfaz.poliza.PolizaFacadeRemote polizaFacadeInterfaz) {
		this.polizaFacadeInterfaz = polizaFacadeInterfaz;
	}
	
	@EJB
	public void setSolicitudFacade(SolicitudFacadeRemote solicitudFacade) {
		this.solicitudFacade = solicitudFacade;
	}

	@EJB
	public void setDiasGraciaServicio(DiasGraciaServicio diasGraciaServicio) {
		this.diasGraciaServicio = diasGraciaServicio;
	}
	
}
