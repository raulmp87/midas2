package mx.com.afirme.midas.catalogos.materialcombustible;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity MaterialCombustibleDTO.
 * @see .MaterialCombustibleDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class MaterialCombustibleFacade  implements MaterialCombustibleFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved MaterialCombustibleDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MaterialCombustibleDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(MaterialCombustibleDTO entity) {
    				LogDeMidasEJB3.log("saving MaterialCombustibleDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent MaterialCombustibleDTO entity.
	  @param entity MaterialCombustibleDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MaterialCombustibleDTO entity) {
    				LogDeMidasEJB3.log("deleting MaterialCombustibleDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(MaterialCombustibleDTO.class, entity.getIdMaterialCombustible());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved MaterialCombustibleDTO entity and return it or a copy of it to the sender. 
	 A copy of the MaterialCombustibleDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MaterialCombustibleDTO entity to update
	 @return MaterialCombustibleDTO the persisted MaterialCombustibleDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public MaterialCombustibleDTO update(MaterialCombustibleDTO entity) {
    				LogDeMidasEJB3.log("updating MaterialCombustibleDTO instance", Level.INFO, null);
	        try {
            MaterialCombustibleDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public MaterialCombustibleDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding MaterialCombustibleDTO instance with id: " + id, Level.INFO, null);
	        try {
            MaterialCombustibleDTO instance = entityManager.find(MaterialCombustibleDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all MaterialCombustibleDTO entities with a specific property value.  
	 
	  @param propertyName the name of the MaterialCombustibleDTO property to query
	  @param value the property value to match
	  	  @return List<MaterialCombustibleDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<MaterialCombustibleDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding MaterialCombustibleDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from MaterialCombustibleDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all MaterialCombustibleDTO entities.
	  	  @return List<MaterialCombustibleDTO> all MaterialCombustibleDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<MaterialCombustibleDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all MaterialCombustibleDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from MaterialCombustibleDTO model " +
					"order by model.descripcionMaterialCombustible";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<MaterialCombustibleDTO> listarFiltrado(MaterialCombustibleDTO materialCombustibleDTO){
		try {
			String queryString = "select model from MaterialCombustibleDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (materialCombustibleDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoMaterialCombustible", materialCombustibleDTO.getCodigoMaterialCombustible());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "descripcionMaterialCombustible", materialCombustibleDTO.getDescripcionMaterialCombustible());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public MaterialCombustibleDTO findById(CatalogoValorFijoId arg0) {
		return null;
	}

	public MaterialCombustibleDTO findById(double arg0) {
		return null;
	}

	public List<MaterialCombustibleDTO> listRelated(Object id) {
		return findAll();
	}
	
}