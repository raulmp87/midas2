package mx.com.afirme.midas.catalogos.girorc;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity GiroRC.
 * 
 * @see .GiroRC
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class GiroRCFacade implements GiroRCFacadeRemote {
	// property constants
	public static final String DESCRIPCIONGIRORC = "descripciongirorc";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved GiroRC entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            GiroRC entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(GiroRCDTO entity) {
		LogDeMidasEJB3.log("saving GiroRC instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent GiroRC entity.
	 * 
	 * @param entity
	 *            GiroRC entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(GiroRCDTO entity) {
		LogDeMidasEJB3.log("deleting GiroRC instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(GiroRCDTO.class, entity.getIdTcGiroRC());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved GiroRC entity and return it or a copy of it to
	 * the sender. A copy of the GiroRC entity parameter is returned when the
	 * JPA persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            GiroRC entity to update
	 * @return GiroRC the persisted GiroRC entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public GiroRCDTO update(GiroRCDTO entity) {
		LogDeMidasEJB3.log("updating GiroRC instance", Level.INFO, null);
		try {
			GiroRCDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public GiroRCDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding GiroRC instance with id: " + id, Level.INFO, null);
		try {
			GiroRCDTO instance = entityManager.find(GiroRCDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all GiroRC entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the GiroRC property to query
	 * @param value
	 *            the property value to match
	 * @return List<GiroRC> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<GiroRCDTO> findByProperty(String propertyName, final Object value) {
		LogDeMidasEJB3.log("finding GiroRC instance with property: " + propertyName
				+ ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from GiroRCDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<GiroRCDTO> findByDescripciongirorc(Object descripciongirorc) {
		return findByProperty(DESCRIPCIONGIRORC, descripciongirorc);
	}

	/**
	 * Find all GiroRC entities.
	 * 
	 * @return List<GiroRC> all GiroRC entities
	 */
	@SuppressWarnings("unchecked")
	public List<GiroRCDTO> findAll() {
		LogDeMidasEJB3.log("finding all GiroRC instances", Level.INFO, null);
		try {
			final String queryString = "select model from GiroRCDTO model " +
					"order by model.descripcionGiroRC";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<GiroRCDTO> listarFiltrado(GiroRCDTO entity) {
		String queryString = "select model from GiroRCDTO model ";
		String sWhere = "";
		Query query;
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		if (entity == null) {
			return null;
		}
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoGiroRC", entity.getCodigoGiroRC());
		sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionGiroRC", entity.getDescripcionGiroRC());
		if (Utilerias.esAtributoQueryValido(sWhere)) {
			queryString = queryString.concat(" where ").concat(sWhere);
		}
		query = entityManager.createQuery(queryString);
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	public EntityTransaction getTransaction() {
		EntityTransaction transaccion = entityManager.getTransaction();
		return transaccion;
	}

	public GiroRCDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public GiroRCDTO findById(double id) {
		return null;
	}

	public List<GiroRCDTO> listRelated(Object id) {
		return findAll();
	}

}