package mx.com.afirme.midas.catalogos.girorc;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

@Stateless
public class SubGiroRCFacade implements SubGiroRCFacadeRemote {
	// property constants
	public static final String DESCRIPCIONSUBGIRORC = "descripcionsubgirorc";
	public static final String CLAVETIPORIESGO = "clavetiporiesgo";
	public static final String CLAVEINSPECCION = "claveinspeccion";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SubGiroRC entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SubGiroRC entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubGiroRCDTO entity) {
		LogDeMidasEJB3.log("saving SubGiroRC instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SubGiroRC entity.
	 * 
	 * @param entity
	 *            SubGiroRC entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubGiroRCDTO entity) {
		LogDeMidasEJB3.log("deleting SubGiroRC instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SubGiroRCDTO.class, entity.getIdTcSubGiroRC());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SubGiroRC entity and return it or a copy of it
	 * to the sender. A copy of the SubGiroRC entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SubGiroRC entity to update
	 * @return SubGiroRC the persisted SubGiroRC entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubGiroRCDTO update(SubGiroRCDTO entity) {
		LogDeMidasEJB3.log("updating SubGiroRC instance", Level.INFO, null);
		try {
			SubGiroRCDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SubGiroRCDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SubGiroRC instance with id: " + id, Level.INFO,
				null);
		try {
			SubGiroRCDTO instance = entityManager.find(SubGiroRCDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubGiroRC entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SubGiroRC property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubGiroRC> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SubGiroRCDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SubGiroRC instance with property: " + propertyName
				+ ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SubGiroRCDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<SubGiroRCDTO> findByDescripcionsubgirorc(
			Object descripcionsubgirorc) {
		return findByProperty(DESCRIPCIONSUBGIRORC, descripcionsubgirorc);
	}

	public List<SubGiroRCDTO> findByClavetiporiesgo(Object clavetiporiesgo) {
		return findByProperty(CLAVETIPORIESGO, clavetiporiesgo);
	}

	public List<SubGiroRCDTO> findByClaveinspeccion(Object claveinspeccion) {
		return findByProperty(CLAVEINSPECCION, claveinspeccion);
	}

	/**
	 * Find all SubGiroRC entities.
	 * 
	 * @return List<SubGiroRC> all SubGiroRC entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubGiroRCDTO> findAll() {
		LogDeMidasEJB3.log("finding all SubGiroRC instances", Level.INFO, null);
		try {
			final String queryString = "select model from SubGiroRCDTO model " +
					"order by model.descripcionSubGiroRC";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SubGiroRCDTO> listarFiltrado(SubGiroRCDTO subGiroRCDTO) {
		String queryString = "select model from SubGiroRCDTO AS model ";
		String sWhere = "";
		Query query = null;
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

		if (subGiroRCDTO == null) {
			return null;
		}
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoSubGiroRC", subGiroRCDTO.getCodigoSubGiroRC());
		sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionSubGiroRC", subGiroRCDTO.getDescripcionSubGiroRC());
//		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveInspeccion", subGiroRCDTO.getClaveInspeccion());
		if (Utilerias.esAtributoQueryValido(sWhere)) {
			queryString = queryString.concat(" where ").concat(sWhere);
		}
		query = entityManager.createQuery(queryString);
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	public SubGiroRCDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public SubGiroRCDTO findById(double id) {
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<SubGiroRCDTO> listRelated(Object id) {
			LogDeMidasEJB3.log("finding all SubGiroRCDTO related with id: "+id, Level.INFO, null);
			try {
				final String queryString = "select model from SubGiroRCDTO model where " +
						"model.giroRC.idTcGiroRC = (select sg.giroRC.idTcGiroRC " +
						"from SubGiroRCDTO sg where sg.idTcSubGiroRC = :id) order by model.descripcionSubGiroRC";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("id", (BigDecimal)id);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
			} catch (RuntimeException re) {
				LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
			}
	}
}