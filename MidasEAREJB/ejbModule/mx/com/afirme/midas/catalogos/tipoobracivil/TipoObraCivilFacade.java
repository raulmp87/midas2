package mx.com.afirme.midas.catalogos.tipoobracivil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity TipoObraCivilDTO.
 * 
 * @see .TipoObraCivilDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoObraCivilFacade implements TipoObraCivilFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoObraCivilDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoObraCivilDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoObraCivilDTO entity) {
		LogDeMidasEJB3
				.log("saving TipoObraCivilDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoObraCivilDTO entity.
	 * 
	 * @param entity
	 *            TipoObraCivilDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoObraCivilDTO entity) {
		LogDeMidasEJB3.log("deleting TipoObraCivilDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(TipoObraCivilDTO.class, entity
					.getIdTipoObraCivil());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoObraCivilDTO entity and return it or a
	 * copy of it to the sender. A copy of the TipoObraCivilDTO entity parameter
	 * is returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoObraCivilDTO entity to update
	 * @return TipoObraCivilDTO the persisted TipoObraCivilDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoObraCivilDTO update(TipoObraCivilDTO entity) {
		LogDeMidasEJB3.log("updating TipoObraCivilDTO instance", Level.INFO,
				null);
		try {
			TipoObraCivilDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoObraCivilDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding TipoObraCivilDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoObraCivilDTO instance = entityManager.find(
					TipoObraCivilDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoObraCivilDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoObraCivilDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoObraCivilDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoObraCivilDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TipoObraCivilDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoObraCivilDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoObraCivilDTO entities.
	 * 
	 * @return List<TipoObraCivilDTO> all TipoObraCivilDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoObraCivilDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoObraCivilDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from TipoObraCivilDTO model " +
					"order by model.descripcionTipoObraCivil";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TipoObraCivilDTO> listarFiltrado(
			TipoObraCivilDTO tipoObraCivilDTO) {
		try {
			String queryString = "select model from TipoObraCivilDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (tipoObraCivilDTO == null)
				return null;

			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigoTipoObraCivil", tipoObraCivilDTO
							.getCodigoTipoObraCivil());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "descripcionTipoObraCivil", tipoObraCivilDTO
							.getDescripcionTipoObraCivil());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveAutorizacion", tipoObraCivilDTO
							.getClaveAutorizacion());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoObraCivilDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoObraCivilDTO findById(double id) {
		return null;
	}

	public List<TipoObraCivilDTO> listRelated(Object id) {
		return findAll();
	}

}