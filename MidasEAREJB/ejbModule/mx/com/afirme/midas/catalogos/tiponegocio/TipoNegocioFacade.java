package mx.com.afirme.midas.catalogos.tiponegocio;

// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity TipoNegocioDTO.
 * 
 * @see .TipoNegocioDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoNegocioFacade implements TipoNegocioFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoNegocioDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoNegocioDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoNegocioDTO entity) {
		LogDeMidasEJB3.log("saving TipoNegocioDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoNegocioDTO entity.
	 * 
	 * @param entity
	 *            TipoNegocioDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoNegocioDTO entity) {
		LogDeMidasEJB3
				.log("deleting TipoNegocioDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(TipoNegocioDTO.class, entity
					.getIdTcTipoNegocio());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoNegocioDTO entity and return it or a copy
	 * of it to the sender. A copy of the TipoNegocioDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoNegocioDTO entity to update
	 * @return TipoNegocioDTO the persisted TipoNegocioDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoNegocioDTO update(TipoNegocioDTO entity) {
		LogDeMidasEJB3
				.log("updating TipoNegocioDTO instance", Level.INFO, null);
		try {
			TipoNegocioDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoNegocioDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding TipoNegocioDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoNegocioDTO instance = entityManager.find(TipoNegocioDTO.class,
					id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoNegocioDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoNegocioDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoNegocioDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoNegocioDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TipoNegocioDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoNegocioDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoNegocioDTO entities.
	 * 
	 * @return List<TipoNegocioDTO> all TipoNegocioDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoNegocioDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoNegocioDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from TipoNegocioDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
}