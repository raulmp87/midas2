package mx.com.afirme.midas.catalogos.marcavehiculo;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity SubMarcaVehiculoDTO.
 * @see .SubMarcaVehiculoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class SubMarcaVehiculoFacade  implements SubMarcaVehiculoFacadeRemote {
    
	@PersistenceContext private EntityManager entityManager;
    private TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote;
    private MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote;
    
	
    @EJB
	public void setMarcaVehiculoFacadeRemote(
			MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote) {
		this.marcaVehiculoFacadeRemote = marcaVehiculoFacadeRemote;
	}

	@EJB
	public void setTipoVehiculoFacadeRemote(
			TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote) {
		this.tipoVehiculoFacadeRemote = tipoVehiculoFacadeRemote;
	}
	
		/**
	 Perform an initial save of a previously unsaved DTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SubMarcaVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(SubMarcaVehiculoDTO entity) {
    				LogUtil.log("saving SubMarcaVehiculoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent SubMarcaVehiculoDTO entity.
	  @param entity SubMarcaVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SubMarcaVehiculoDTO entity) {
    				LogUtil.log("deleting SubMarcaVehiculoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(SubMarcaVehiculoDTO.class, entity.getIdSubTcMarcaVehiculo());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved SubMarcaVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the SubMarcaVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SubMarcaVehiculoDTO entity to update
	 @return SubMarcaVehiculoDTO the persisted SubMarcaVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public SubMarcaVehiculoDTO update(SubMarcaVehiculoDTO entity) {
    				LogUtil.log("updating SubMarcaVehiculoDTO instance", Level.INFO, null);
	        try {
	        	SubMarcaVehiculoDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public SubMarcaVehiculoDTO findById( BigDecimal id) {
    				LogUtil.log("finding SubMarcaVehiculoDTO instance with id: " + id, Level.INFO, null);
	        try {
	        	SubMarcaVehiculoDTO instance = entityManager.find(SubMarcaVehiculoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all SubMarcaVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SubMarcaVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<SubMarcaVehiculoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<SubMarcaVehiculoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding SubMarcaVehiculoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from SubMarcaVehiculoDTO model where model." 
			 						+ propertyName + "= :propertyValue " +
			 								"  order by model.descripcionSubMarcaVehiculo asc";
			
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all SubMarcaVehiculoDTO entities.
	  	  @return List<SubMarcaVehiculoDTO> all SubMarcaVehiculoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubMarcaVehiculoDTO> findAll(
		) {
					LogUtil.log("finding all SubMarcaVehiculoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from SubMarcaVehiculoDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SubMarcaVehiculoDTO> listarFiltrado(SubMarcaVehiculoDTO subMarcaVehiculoDTO){
		try {
			String queryString = "select distinct model from SubMarcaVehiculoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (subMarcaVehiculoDTO== null)
				return null;
			if(subMarcaVehiculoDTO.getMarcaVehiculoDTO().getTipoBienAutosDTO() != null && subMarcaVehiculoDTO.getMarcaVehiculoDTO().getTipoBienAutosDTO().getClaveTipoBien() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "marcaVehiculoDTO.tipoBienAutosDTO.claveTipoBien", subMarcaVehiculoDTO.getMarcaVehiculoDTO().getTipoBienAutosDTO().getClaveTipoBien());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "marcaVehiculoDTO.idTcMarcaVehiculo", subMarcaVehiculoDTO.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "marcaVehiculoDTO.codigoMarcaVehiculo", subMarcaVehiculoDTO.getMarcaVehiculoDTO().getCodigoMarcaVehiculo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "marcaVehiculoDTO.descripcionMarcaVehiculo",  subMarcaVehiculoDTO.getMarcaVehiculoDTO().getDescripcionMarcaVehiculo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idSubTcMarcaVehiculo",  subMarcaVehiculoDTO.getIdSubTcMarcaVehiculo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "descripcionSubMarcaVehiculo",  subMarcaVehiculoDTO.getDescripcionSubMarcaVehiculo());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);			

			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public SubMarcaVehiculoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public SubMarcaVehiculoDTO findById(double id) {
		return findById(new BigDecimal(""+id));
	}

	public List<SubMarcaVehiculoDTO> listRelated(Object id) {
		return null;
	}
}