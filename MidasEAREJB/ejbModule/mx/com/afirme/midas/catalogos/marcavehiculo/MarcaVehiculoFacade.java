package mx.com.afirme.midas.catalogos.marcavehiculo;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity MarcaVehiculoDTO.
 * @see .MarcaVehiculoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class MarcaVehiculoFacade  implements MarcaVehiculoFacadeRemote {
    
	@PersistenceContext private EntityManager entityManager;
    private TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote;
    
	@EJB
	public void setTipoVehiculoFacadeRemote(
			TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote) {
		this.tipoVehiculoFacadeRemote = tipoVehiculoFacadeRemote;
	}
	
		/**
	 Perform an initial save of a previously unsaved MarcaVehiculoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MarcaVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(MarcaVehiculoDTO entity) {
    				LogUtil.log("saving MarcaVehiculoDTO instance", Level.FINE, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.FINE, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent MarcaVehiculoDTO entity.
	  @param entity MarcaVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MarcaVehiculoDTO entity) {
    				LogUtil.log("deleting MarcaVehiculoDTO instance", Level.FINE, null);
	        try {
        	entity = entityManager.getReference(MarcaVehiculoDTO.class, entity.getIdTcMarcaVehiculo());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.FINE, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved MarcaVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the MarcaVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MarcaVehiculoDTO entity to update
	 @return MarcaVehiculoDTO the persisted MarcaVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public MarcaVehiculoDTO update(MarcaVehiculoDTO entity) {
    				LogUtil.log("updating MarcaVehiculoDTO instance", Level.FINE, null);
	        try {
            MarcaVehiculoDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.FINE, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public MarcaVehiculoDTO findById( BigDecimal id) {
    				LogUtil.log("finding MarcaVehiculoDTO instance with id: " + id, Level.FINE, null);
	        try {
            MarcaVehiculoDTO instance = entityManager.find(MarcaVehiculoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all MarcaVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the MarcaVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<MarcaVehiculoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<MarcaVehiculoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding MarcaVehiculoDTO instance with property: " + propertyName + ", value: " + value, Level.FINE, null);
			try {
			final String queryString = "select model from MarcaVehiculoDTO model where model." 
			 						+ propertyName + "= :propertyValue " +
			 								"  order by model.descripcionMarcaVehiculo asc";
			
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all MarcaVehiculoDTO entities.
	  	  @return List<MarcaVehiculoDTO> all MarcaVehiculoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<MarcaVehiculoDTO> findAll(
		) {
					LogUtil.log("finding all MarcaVehiculoDTO instances", Level.FINE, null);
			try {
			final String queryString = "select model from MarcaVehiculoDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<MarcaVehiculoDTO> listarFiltrado(MarcaVehiculoDTO marcaVehiculoDTO){
		try {
			String queryString = "select distinct model from MarcaVehiculoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (marcaVehiculoDTO== null)
				return null;
			if(marcaVehiculoDTO.getTipoBienAutosDTO() != null && marcaVehiculoDTO.getTipoBienAutosDTO().getClaveTipoBien() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoBienAutosDTO.claveTipoBien", marcaVehiculoDTO.getTipoBienAutosDTO().getClaveTipoBien());

			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcMarcaVehiculo", marcaVehiculoDTO.getIdTcMarcaVehiculo());
			
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoMarcaVehiculo", marcaVehiculoDTO.getCodigoMarcaVehiculo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionMarcaVehiculo", marcaVehiculoDTO.getDescripcionMarcaVehiculo());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
//			List<MarcaVehiculoDTO> respuesta = (List<MarcaVehiculoDTO>)query.getResultList();
//			int index=0;
//			for(MarcaVehiculoDTO item : respuesta){
//				TipoVehiculoDTO tipoVehiculoDTO = tipoVehiculoFacadeRemote.findById(item.getIdTcTipoVehiculo());
//				respuesta.get(index).setTipoVehiculoDTO(tipoVehiculoDTO);
//				index++;
//			}
//			return respuesta;
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public MarcaVehiculoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public MarcaVehiculoDTO findById(double id) {
		return findById(new BigDecimal(""+id));
	}

	public List<MarcaVehiculoDTO> listRelated(Object id) {
		return null;
	}
}