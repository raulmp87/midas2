package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity TipoMontajeMaquinaDTO.
 * @see .TipoMontajeMaquinaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class TipoMontajeMaquinaFacade  implements TipoMontajeMaquinaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved TipoMontajeMaquinaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoMontajeMaquinaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoMontajeMaquinaDTO entity) {
    				LogDeMidasEJB3.log("saving TipoMontajeMaquinaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent TipoMontajeMaquinaDTO entity.
	  @param entity TipoMontajeMaquinaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoMontajeMaquinaDTO entity) {
    				LogDeMidasEJB3.log("deleting TipoMontajeMaquinaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(TipoMontajeMaquinaDTO.class, entity.getIdtctipomontajemaq());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved TipoMontajeMaquinaDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoMontajeMaquinaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoMontajeMaquinaDTO entity to update
	 @return TipoMontajeMaquinaDTO the persisted TipoMontajeMaquinaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public TipoMontajeMaquinaDTO update(TipoMontajeMaquinaDTO entity) {
    				LogDeMidasEJB3.log("updating TipoMontajeMaquinaDTO instance", Level.INFO, null);
	        try {
            TipoMontajeMaquinaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public TipoMontajeMaquinaDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding TipoMontajeMaquinaDTO instance with id: " + id, Level.INFO, null);
	        try {
            TipoMontajeMaquinaDTO instance = entityManager.find(TipoMontajeMaquinaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all TipoMontajeMaquinaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoMontajeMaquinaDTO property to query
	  @param value the property value to match
	  	  @return List<TipoMontajeMaquinaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<TipoMontajeMaquinaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding TipoMontajeMaquinaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from TipoMontajeMaquinaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all TipoMontajeMaquinaDTO entities.
	  	  @return List<TipoMontajeMaquinaDTO> all TipoMontajeMaquinaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoMontajeMaquinaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all TipoMontajeMaquinaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from TipoMontajeMaquinaDTO model " +
					"order by model.descripciontipomontajemaq";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
    
    @SuppressWarnings("unchecked")
	public List<TipoMontajeMaquinaDTO> listarFiltrado(TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO){
		try {
			String queryString = "select model from TipoMontajeMaquinaDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (tipoMontajeMaquinaDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigotipomontajemaq", tipoMontajeMaquinaDTO.getCodigotipomontajemaq());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "descripciontipomontajemaq", tipoMontajeMaquinaDTO.getDescripciontipomontajemaq());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoMontajeMaquinaDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoMontajeMaquinaDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<TipoMontajeMaquinaDTO> listRelated(Object id) {
		return this.findAll();
	}
}