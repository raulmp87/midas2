package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.tipomontajemaquina.SubtipoMontajeMaquinaDTO;
import mx.com.afirme.midas.catalogos.tipomontajemaquina.SubtipoMontajeMaquinaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity SubtipoMontajeMaquinaDTO.
 * @see .SubtipoMontajeMaquinaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class SubtipoMontajeMaquinaFacade  implements SubtipoMontajeMaquinaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved SubtipoMontajeMaquinaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SubtipoMontajeMaquinaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(SubtipoMontajeMaquinaDTO entity) {
    				LogDeMidasEJB3.log("saving SubtipoMontajeMaquinaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent SubtipoMontajeMaquinaDTO entity.
	  @param entity SubtipoMontajeMaquinaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SubtipoMontajeMaquinaDTO entity) {
    				LogDeMidasEJB3.log("deleting SubtipoMontajeMaquinaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(SubtipoMontajeMaquinaDTO.class, entity.getIdtcsubtipomontajemaq());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved SubtipoMontajeMaquinaDTO entity and return it or a copy of it to the sender. 
	 A copy of the SubtipoMontajeMaquinaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SubtipoMontajeMaquinaDTO entity to update
	 @return SubtipoMontajeMaquinaDTO the persisted SubtipoMontajeMaquinaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public SubtipoMontajeMaquinaDTO update(SubtipoMontajeMaquinaDTO entity) {
    				LogDeMidasEJB3.log("updating SubtipoMontajeMaquinaDTO instance", Level.INFO, null);
	        try {
            SubtipoMontajeMaquinaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public SubtipoMontajeMaquinaDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding SubtipoMontajeMaquinaDTO instance with id: " + id, Level.INFO, null);
	        try {
            SubtipoMontajeMaquinaDTO instance = entityManager.find(SubtipoMontajeMaquinaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all SubtipoMontajeMaquinaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SubtipoMontajeMaquinaDTO property to query
	  @param value the property value to match
	  	  @return List<SubtipoMontajeMaquinaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<SubtipoMontajeMaquinaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding SubtipoMontajeMaquinaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from SubtipoMontajeMaquinaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all SubtipoMontajeMaquinaDTO entities.
	  	  @return List<SubtipoMontajeMaquinaDTO> all SubtipoMontajeMaquinaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubtipoMontajeMaquinaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all SubtipoMontajeMaquinaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from SubtipoMontajeMaquinaDTO model " +
					"order by model.descripcionsubtipomontajemaq";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	
	/**
     private BigDecimal codigosubtipomontajemaq;
     private BigDecimal descripcionsubtipomontajemaq;
     private Short claveAutorizacion;
	 * @param subtipoMontajeMaquinaDTO
	 * @return
	 */
	
	 @SuppressWarnings("unchecked")
		public List<SubtipoMontajeMaquinaDTO> listarFiltrado(SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO){
			try {
				String queryString = "select model from SubtipoMontajeMaquinaDTO AS model ";
				String sWhere = "";
				Query query;
				List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
				
				if (subtipoMontajeMaquinaDTO == null)
					return null;
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigosubtipomontajemaq", subtipoMontajeMaquinaDTO.getCodigosubtipomontajemaq());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "descripcionsubtipomontajemaq", subtipoMontajeMaquinaDTO.getDescripcionsubtipomontajemaq());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveAutorizacion", subtipoMontajeMaquinaDTO.getClaveAutorizacion());
				if (Utilerias.esAtributoQueryValido(sWhere))
					queryString = queryString.concat(" where ").concat(sWhere);
				query = entityManager.createQuery(queryString);
				Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
			} catch (RuntimeException re) {
				throw re;
			}
		}

	public SubtipoMontajeMaquinaDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public SubtipoMontajeMaquinaDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<SubtipoMontajeMaquinaDTO> listRelated(Object id) {
		return this.findAll();
	}	
}