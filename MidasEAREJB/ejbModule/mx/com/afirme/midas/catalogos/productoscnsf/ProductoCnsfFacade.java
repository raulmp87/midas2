package mx.com.afirme.midas.catalogos.productoscnsf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.productoscnsf.ProductoCnsfDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ProductoCnsfFacade implements ProductoCnsfFacadeRemote{

	@PersistenceContext
	private EntityManager entityManager;
	
	public void delete(ProductoCnsfDTO entity) {
		LogDeMidasEJB3.log("deleting CAT_CLAVEPRODSERV instance", Level.INFO, null);
		entity = entityManager
				.getReference(ProductoCnsfDTO.class, entity.getId());
		entityManager.remove(entity);
		LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	}

	public List<ProductoCnsfDTO> findAll() {
		LogDeMidasEJB3.log("finding all CAT_CLAVEPRODSERV instances", Level.INFO, null);
		final String queryString = "select model from ProductoCnsfDTO AS model";
		Query query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	public ProductoCnsfDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding CAT_CLAVEPRODSERV instance with id: " + id,
				Level.INFO, null);
		ProductoCnsfDTO instance = entityManager.find(ProductoCnsfDTO.class, id);
		return instance;
	}

	public List<ProductoCnsfDTO> findByProperty(String propertyName, Object value) {
		LogDeMidasEJB3.log("finding CAT_CLAVEPRODSERV instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		final String queryString = "select model from ProductoCnsfDTO AS model where model."
				+ propertyName + "= :propertyValue";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("propertyValue", value);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	public EntityTransaction getTransaction() {
		EntityTransaction transaccion = entityManager.getTransaction();

		return transaccion;
	}

	public List<ProductoCnsfDTO> listarFiltrado(ProductoCnsfDTO ProductoCnsfDTO) {
		try {
			String queryString = "select model from ProductoCnsfDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (ProductoCnsfDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveProdServ", ProductoCnsfDTO.getClaveProdServ());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "descripcionProd", ProductoCnsfDTO.getDescripcionProd());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public void save(ProductoCnsfDTO entity) {
		LogDeMidasEJB3.log("saving CAT_CLAVEPRODSERV instance", Level.INFO, null);
		entityManager.persist(entity);
		LogDeMidasEJB3.log("save successful", Level.INFO, null);
	}

	public ProductoCnsfDTO update(ProductoCnsfDTO entity) {
		LogDeMidasEJB3.log("updating CAT_CLAVEPRODSERV instance", Level.INFO, null);
		ProductoCnsfDTO result = entityManager.merge(entity);
		LogDeMidasEJB3.log("update successful", Level.INFO, null);
		return result;
	}

	public ProductoCnsfDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public ProductoCnsfDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<ProductoCnsfDTO> listRelated(BigDecimal id) {
		return this.findAll();
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public Long getCount(ProductoCnsfDTO filtro) {
		try {			
			String queryString = "select count(model) from ProductoCnsfDTO model ";
			
			String sWhere = "";
			
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "claveProdServ", filtro.getClaveProdServ());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionProd", filtro.getDescripcionProd());
			
			
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
			}
			
			Query query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return (Long)query.getSingleResult();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
		
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<ProductoCnsfDTO> findByFilters(ProductoCnsfDTO filtro,
			int primerRegistroACargar, int numeroMaximoRegistrosACargar) {
		try {
			String queryString = "select model from ProductoCnsfDTO model ";
			String sWhere = "";
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "claveProdServ", filtro.getClaveProdServ());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionProd", filtro.getDescripcionProd());
			
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
			}
			
			queryString = queryString.concat(" order by model.descripcionProd desc");
		
			Query query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			query.setFirstResult(primerRegistroACargar);
			query.setMaxResults(numeroMaximoRegistrosACargar);
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}
