package mx.com.afirme.midas.catalogos.distanciaciudad;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity DistanciaCiudadDTO.
 * @see .DistanciaCiudadDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DistanciaCiudadFacade  implements DistanciaCiudadFacadeRemote {
	//property constants
	public static final String IDCIUDADORIGEN = "idCiudadOrigen";
	public static final String IDCIUDADDESTINO = "idCiudadDestino";
	public static final String DISTANCIA = "distancia";
	public static final String IDESTADOORIGEN = "idEstadoOrigen";
	public static final String IDESTADODESTINO = "idEstadoDestino";





    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DistanciaCiudadDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DistanciaCiudadDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DistanciaCiudadDTO entity) {
    				LogDeMidasEJB3.log("saving DistanciaCiudadDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DistanciaCiudadDTO entity.
	  @param entity DistanciaCiudadDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DistanciaCiudadDTO entity) {
    				LogDeMidasEJB3.log("deleting DistanciaCiudadDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DistanciaCiudadDTO.class, entity.getIdDistanciaCiudad());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DistanciaCiudadDTO entity and return it or a copy of it to the sender. 
	 A copy of the DistanciaCiudadDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DistanciaCiudadDTO entity to update
	 @return DistanciaCiudadDTO the persisted DistanciaCiudadDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DistanciaCiudadDTO update(DistanciaCiudadDTO entity) {
    				LogDeMidasEJB3.log("updating DistanciaCiudadDTO instance", Level.INFO, null);
	        try {
            DistanciaCiudadDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DistanciaCiudadDTO findById( Long id) {
    				LogDeMidasEJB3.log("finding DistanciaCiudadDTO instance with id: " + id, Level.INFO, null);
	        try {
            DistanciaCiudadDTO instance = entityManager.find(DistanciaCiudadDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DistanciaCiudadDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DistanciaCiudadDTO property to query
	  @param value the property value to match
	  	  @return List<DistanciaCiudadDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DistanciaCiudadDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding DistanciaCiudadDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DistanciaCiudadDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	public List<DistanciaCiudadDTO> findByIdciudadorigen(Object idciudadorigen
	) {
		return findByProperty(IDCIUDADORIGEN, idciudadorigen
		);
	}
	
	public List<DistanciaCiudadDTO> findByIdciudaddestino(Object idciudaddestino
	) {
		return findByProperty(IDCIUDADDESTINO, idciudaddestino
		);
	}
	
	public List<DistanciaCiudadDTO> findByDistancia(Object distancia
	) {
		return findByProperty(DISTANCIA, distancia
		);
	}
	
	public List<DistanciaCiudadDTO> findByIdestadoorigen(Object idestadoorigen
	) {
		return findByProperty(IDESTADOORIGEN, idestadoorigen
		);
	}
	
	public List<DistanciaCiudadDTO> findByIdestadodestino(Object idestadodestino
	) {
		return findByProperty(IDESTADODESTINO, idestadodestino
		);
	}
	
	
	/**
	 * Find all DistanciaCiudadDTO entities.
	  	  @return List<DistanciaCiudadDTO> all DistanciaCiudadDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DistanciaCiudadDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all DistanciaCiudadDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from DistanciaCiudadDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find all DistanciaCiudadDTO entities.
	  	  @return List<DistanciaCiudadDTO> all DistanciaCiudadDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DistanciaCiudadDTO> listarTodo(
		) {
					LogDeMidasEJB3.log("finding all DistanciaCiudadDTO instances", Level.INFO, null);
			try {
			final String queryString = "select distancia, origen.cityName, destino.cityName " +
									   "from DistanciaCiudadDTO distancia, CiudadDTO origen, CiudadDTO destino" +
									   "where distancia.idCiudadOrigen = origen.cityId and distancia.idCiudadDestino = destino.cityId";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<DistanciaCiudadDTO> listarFiltrado(DistanciaCiudadDTO distanciaCiudadDTO) {		
		try {
			String queryString = "select model from DistanciaCiudadDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (distanciaCiudadDTO == null)
				return null;
						
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idCiudadOrigen", distanciaCiudadDTO.getIdCiudadOrigen());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idCiudadDestino", distanciaCiudadDTO.getIdCiudadDestino());		
			
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public DistanciaCiudadDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding DistanciaCiudadDTO instance with id: " + id,
				Level.INFO, null);
		try {
			DistanciaCiudadDTO instance = entityManager.find(DistanciaCiudadDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DistanciaCiudadDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public DistanciaCiudadDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<DistanciaCiudadDTO> listRelated(Object id) {
		return this.findAll();
	}
}