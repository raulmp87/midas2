package mx.com.afirme.midas.catalogos.tipovivienda;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class TipoViviendaFacade implements TipoViviendaFacadeRemote {

	public static final String DESCRIPCION = "descripcionTipoVivienda";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved Tctipotecho entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            Tctipotecho entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoViviendaDTO entity) {
		LogDeMidasEJB3.log("saving TipoViviendaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent Tctipotecho entity.
	 * 
	 * @param entity
	 *            Tctipotecho entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoViviendaDTO entity) {
		LogDeMidasEJB3.log("deleting TipoViviendaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(TipoViviendaDTO.class, entity
					.getIdTipoVivienda());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved Tctipotecho entity and return it or a copy of
	 * it to the sender. A copy of the Tctipotecho entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            Tctipotecho entity to update
	 * @return Tctipotecho the persisted Tctipotecho entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoViviendaDTO update(TipoViviendaDTO entity) {
		LogDeMidasEJB3.log("updating TipoViviendaDTO instance", Level.INFO, null);
		try {
			TipoViviendaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoViviendaDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding TipoViviendaDTO instance with id: " + id, Level.INFO,
				null);
		try {
			TipoViviendaDTO instance = entityManager.find(TipoViviendaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Tctipotecho entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Tctipotecho property to query
	 * @param value
	 *            the property value to match
	 * @return List<Tctipotecho> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoViviendaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TipoViviendaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoViviendaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<TipoViviendaDTO> findByDescripciontipotecho(
			Object descripciontipotecho) {
		return findByProperty(DESCRIPCION, descripciontipotecho);
	}

	/**
	 * Find all Tctipotecho entities.
	 * 
	 * @return List<Tctipotecho> all Tctipotecho entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoViviendaDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoViviendaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from TipoViviendaDTO model " +
					"order by model.codigoTipoVivienda";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoViviendaDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoViviendaDTO findById(double id) {
		return null;
	}

	public List<TipoViviendaDTO> listRelated(Object id) {
		return null;
	}

}
