package mx.com.afirme.midas.catalogos.institucionbancaria;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

@Stateless
public class BancoFacade implements BancoFacadeRemote{
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<BancoDTO> findAll() {
		LogDeMidasEJB3.log("finding all BancoDTO instances", Level.INFO, null);
		try{
			String statement="select model from BancoDTO model";
			Query query= entityManager.createQuery(statement);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public BancoDTO findById(BigDecimal idBanco) {
		return findById((idBanco!=null)?idBanco.intValue():null);
	}

	@Override
	public BancoDTO findById(CatalogoValorFijoId arg0) {
		return null;
	}

	@Override
	public BancoDTO findById(double idBanco) {
		return findById(Integer.parseInt(String.valueOf(idBanco)));
	}

	@Override
	public List<BancoDTO> listRelated(Object arg0) {
		return null;
	}

	@Override
	public BancoDTO findById(Integer idBanco) {
		LogDeMidasEJB3.log("finding BancoDTO by idBanco with id:"+idBanco, Level.INFO, null);
		try{
			BancoDTO instance=entityManager.find(BancoDTO.class,idBanco);
			return instance;
		}catch(RuntimeException  re){
			LogDeMidasEJB3.log("find by id", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public BancoDTO findByName(String nombre) {
		LogDeMidasEJB3.log("finding BancoDTO by bank name with name:"+nombre, Level.INFO, null);
		try{
			if(nombre==null || nombre.isEmpty()){
				return null;
			}
			String queryString="select model from BancoDTO model ";
			String sWhere="";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreBanco",nombre);
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
			}
			queryString += " order by model.idBanco desc, model.nombreBanco asc ";//TODO: revisar el comportamiento del paginado con la lista ordenada
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return (BancoDTO)query.getSingleResult();
		}catch(RuntimeException re){
			LogDeMidasEJB3.log("find by name", Level.SEVERE, re);
			throw re;
		}
	}

}
