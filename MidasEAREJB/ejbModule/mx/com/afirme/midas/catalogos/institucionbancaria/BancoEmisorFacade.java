package mx.com.afirme.midas.catalogos.institucionbancaria;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

@Stateless
public class BancoEmisorFacade implements BancoEmisorFacadeRemote{
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<BancoEmisorDTO> findAll() {
		LogDeMidasEJB3.log("finding all BancoEmisorDTO instances", Level.INFO, null);
		try{
			String statement="select model from BancoEmisorDTO model";
			Query query= entityManager.createQuery(statement);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public BancoEmisorDTO findById(BigDecimal idBanco) {
		return findById((idBanco!=null)?idBanco.intValue():null);
	}

	@Override
	public BancoEmisorDTO findById(CatalogoValorFijoId arg0) {
		return null;
	}

	@Override
	public BancoEmisorDTO findById(double idBanco) {
		return findById(Integer.parseInt(String.valueOf(idBanco)));
	}

	@Override
	public List<BancoEmisorDTO> listRelated(Object arg0) {
		return null;
	}

	@Override
	public BancoEmisorDTO findById(Integer idBanco) {
		LogDeMidasEJB3.log("finding BancoEmisorDTO by idBanco with id:"+idBanco, Level.INFO, null);
		try{
			BancoEmisorDTO instance=entityManager.find(BancoEmisorDTO.class,idBanco);
			return instance;
		}catch(RuntimeException  re){
			LogDeMidasEJB3.log("find by id", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public BancoEmisorDTO findByName(String nombre) {
		LogDeMidasEJB3.log("finding BancoEmisorDTO by bank name with name:"+nombre, Level.INFO, null);
		try{
			if(nombre==null || nombre.isEmpty()){
				return null;
			}
			String queryString="select model from BancoEmisorDTO model ";
			String sWhere="";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreBanco",nombre);
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
			}
			queryString += " order by model.idBanco desc, model.nombreBanco asc ";//TODO: revisar el comportamiento del paginado con la lista ordenada
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return (BancoEmisorDTO)query.getSingleResult();
		}catch(RuntimeException re){
			LogDeMidasEJB3.log("find by name", Level.SEVERE, re);
			throw re;
		}
	}

	public String encriptaDatos(String encriptaDato) {

		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT SEYCOS.PKG_CRYPTO.FN_ENCRIPTA('");
		queryString.append(encriptaDato);
		queryString.append("') FROM DUAL");

		Query query = entityManager.createNativeQuery(queryString.toString());

		String resultado = (String) query.getSingleResult();
		return resultado;
	}
	
	public String desEncriptaDatos(String encriptaDatos) {

		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT SEYCOS.PKG_CRYPTO.FN_DESENCRIPTA('");
		queryString.append(encriptaDatos);
		queryString.append("') FROM DUAL");

		Query query = entityManager.createNativeQuery(queryString.toString());

		String resultado = (String) query.getSingleResult();
		return resultado;
	}
}
