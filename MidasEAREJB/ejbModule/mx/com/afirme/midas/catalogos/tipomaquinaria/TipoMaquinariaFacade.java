package mx.com.afirme.midas.catalogos.tipomaquinaria;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.tipomaquinaria.TipoMaquinariaDTO;
import mx.com.afirme.midas.catalogos.tipomaquinaria.TipoMaquinariaFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity TipoMaquinaria.
 * @see .TipoMaquinaria
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class TipoMaquinariaFacade  implements TipoMaquinariaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved TipoMaquinaria entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoMaquinaria entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoMaquinariaDTO entity) {
    				LogUtil.log("saving TipoMaquinaria instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent TipoMaquinaria entity.
	  @param entity TipoMaquinaria entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoMaquinariaDTO entity) {
    				LogUtil.log("deleting TipoMaquinaria instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(TipoMaquinariaDTO.class, entity.getIdTcTipoMaquinaria());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved TipoMaquinaria entity and return it or a copy of it to the sender. 
	 A copy of the TipoMaquinaria entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoMaquinaria entity to update
	 @return TipoMaquinaria the persisted TipoMaquinaria entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public TipoMaquinariaDTO update(TipoMaquinariaDTO entity) {
    				LogUtil.log("updating TipoMaquinaria instance", Level.INFO, null);
	        try {
            TipoMaquinariaDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public TipoMaquinariaDTO findById( BigDecimal id) {
    				LogUtil.log("finding TipoMaquinaria instance with id: " + id, Level.INFO, null);
	        try {
            TipoMaquinariaDTO instance = entityManager.find(TipoMaquinariaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all TipoMaquinaria entities with a specific property value.  
	 
	  @param propertyName the name of the TipoMaquinaria property to query
	  @param value the property value to match
	  	  @return List<TipoMaquinaria> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<TipoMaquinariaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding TipoMaquinaria instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from TipoMaquinariaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all TipoMaquinaria entities.
	  	  @return List<TipoMaquinaria> all TipoMaquinaria entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoMaquinariaDTO> findAll(
		) {
					LogUtil.log("finding all TipoMaquinaria instances", Level.INFO, null);
			try {
			final String queryString = "select model from TipoMaquinariaDTO model " +
					"order by model.descripcionTipoMaquinaria";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	public TipoMaquinariaDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoMaquinariaDTO findById(double id) {
		return null;
	}

	public List<TipoMaquinariaDTO> listRelated(Object id) {
		return findAll();
	}
	
}