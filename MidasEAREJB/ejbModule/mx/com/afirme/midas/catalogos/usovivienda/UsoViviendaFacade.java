package mx.com.afirme.midas.catalogos.usovivienda;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class UsoViviendaFacade implements UsoViviendaFacadeRemote {

	public static final String DESCRIPCION = "descripcionUsoVivienda";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved Tctipotecho entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            Tctipotecho entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(UsoViviendaDTO entity) {
		LogDeMidasEJB3.log("saving UsoViviendaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent Tctipotecho entity.
	 * 
	 * @param entity
	 *            Tctipotecho entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(UsoViviendaDTO entity) {
		LogDeMidasEJB3.log("deleting UsoViviendaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(UsoViviendaDTO.class, entity
					.getIdUsoVivienda());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved Tctipotecho entity and return it or a copy of
	 * it to the sender. A copy of the Tctipotecho entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            Tctipotecho entity to update
	 * @return Tctipotecho the persisted Tctipotecho entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public UsoViviendaDTO update(UsoViviendaDTO entity) {
		LogDeMidasEJB3.log("updating UsoViviendaDTO instance", Level.INFO, null);
		try {
			UsoViviendaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public UsoViviendaDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding UsoViviendaDTO instance with id: " + id, Level.INFO,
				null);
		try {
			UsoViviendaDTO instance = entityManager.find(UsoViviendaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Tctipotecho entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Tctipotecho property to query
	 * @param value
	 *            the property value to match
	 * @return List<Tctipotecho> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<UsoViviendaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding UsoViviendaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from UsoViviendaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<UsoViviendaDTO> findByDescripciontipotecho(
			Object descripciontipotecho) {
		return findByProperty(DESCRIPCION, descripciontipotecho);
	}

	/**
	 * Find all Tctipotecho entities.
	 * 
	 * @return List<Tctipotecho> all Tctipotecho entities
	 */
	@SuppressWarnings("unchecked")
	public List<UsoViviendaDTO> findAll() {
		LogDeMidasEJB3.log("finding all UsoViviendaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from UsoViviendaDTO model " +
					"order by model.codigoUsoVivienda";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public UsoViviendaDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public UsoViviendaDTO findById(double id) {
		return null;
	}

	public List<UsoViviendaDTO> listRelated(Object id) {
		return null;
	}

}
