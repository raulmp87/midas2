package mx.com.afirme.midas.catalogos.tipobienautos;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity TipoBienAutosDTO.
 * @see .TipoBienAutosDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class TipoBienAutosFacade  implements TipoBienAutosFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved TipoBienAutosDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoBienAutosDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoBienAutosDTO entity) {
    				LogUtil.log("saving TipoBienAutosDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent TipoBienAutosDTO entity.
	  @param entity TipoBienAutosDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoBienAutosDTO entity) {
    				LogUtil.log("deleting TipoBienAutosDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(TipoBienAutosDTO.class, entity.getClaveTipoBien());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved TipoBienAutosDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoBienAutosDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoBienAutosDTO entity to update
	 @return TipoBienAutosDTO the persisted TipoBienAutosDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public TipoBienAutosDTO update(TipoBienAutosDTO entity) {
    				LogUtil.log("updating TipoBienAutosDTO instance", Level.INFO, null);
	        try {
            TipoBienAutosDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public TipoBienAutosDTO findById( String id) {
    				LogUtil.log("finding TipoBienAutosDTO instance with id: " + id, Level.INFO, null);
	        try {
            TipoBienAutosDTO instance = entityManager.find(TipoBienAutosDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all TipoBienAutosDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoBienAutosDTO property to query
	  @param value the property value to match
	  	  @return List<TipoBienAutosDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<TipoBienAutosDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding TipoBienAutosDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from TipoBienAutosDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all TipoBienAutosDTO entities.
	  	  @return List<TipoBienAutosDTO> all TipoBienAutosDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoBienAutosDTO> findAll(
		) {
					LogUtil.log("finding all TipoBienAutosDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from TipoBienAutosDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TipoBienAutosDTO> listarFiltrado(TipoBienAutosDTO tipoBienAutosDTO){
		try {
			String queryString = "select model from TipoBienAutosDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (tipoBienAutosDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "claveTipoBien", tipoBienAutosDTO.getClaveTipoBien());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionTipoBien", tipoBienAutosDTO.getDescripcionTipoBien());
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoBienAutosDTO findById(BigDecimal id) {
		return findById(id.toString());
	}

	public TipoBienAutosDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoBienAutosDTO findById(double id) {
		return findById(""+id);
	}

	public List<TipoBienAutosDTO> listRelated(Object id) {
		return null;
	}
}