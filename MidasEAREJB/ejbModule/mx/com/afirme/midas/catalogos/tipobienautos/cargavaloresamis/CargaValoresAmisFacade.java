package mx.com.afirme.midas.catalogos.tipobienautos.cargavaloresamis;


// default package

import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas.usuario.LogUtil;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Facade for entity CargaValoresAmisDTO.
 * @see .CargaValoresAmisDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class CargaValoresAmisFacade  implements CargaValoresAmisFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved CargaValoresAmisDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CargaValoresAmisDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CargaValoresAmisDTO entity) {
    				LogUtil.log("saving CargaValoresAmisDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CargaValoresAmisDTO entity.
	  @param entity CargaValoresAmisDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CargaValoresAmisDTO entity) {
    				LogUtil.log("deleting CargaValoresAmisDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(CargaValoresAmisDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CargaValoresAmisDTO entity and return it or a copy of it to the sender. 
	 A copy of the CargaValoresAmisDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CargaValoresAmisDTO entity to update
	 @return CargaValoresAmisDTO the persisted CargaValoresAmisDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CargaValoresAmisDTO update(CargaValoresAmisDTO entity) {
    				LogUtil.log("updating CargaValoresAmisDTO instance", Level.INFO, null);
	        try {
            CargaValoresAmisDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public CargaValoresAmisDTO findById( CargaValoresAmisId id) {
    				LogUtil.log("finding CargaValoresAmisDTO instance with id: " + id, Level.INFO, null);
	        try {
            CargaValoresAmisDTO instance = entityManager.find(CargaValoresAmisDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all CargaValoresAmisDTO entities with a specific property value.  
	 
	  @param propertyName the name of the CargaValoresAmisDTO property to query
	  @param value the property value to match
	  	  @return List<CargaValoresAmisDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<CargaValoresAmisDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding CargaValoresAmisDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from CargaValoresAmisDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all CargaValoresAmisDTO entities.
	  	  @return List<CargaValoresAmisDTO> all CargaValoresAmisDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CargaValoresAmisDTO> findAll(
		) {
					LogUtil.log("finding all CargaValoresAmisDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from CargaValoresAmisDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	public CargaValoresAmisDTO findById(BigDecimal id) {
		// TODO Auto-generated method stub
		return null;
	}

	public CargaValoresAmisDTO findById(CatalogoValorFijoId id) {
		// TODO Auto-generated method stub
		return null;
	}

	public CargaValoresAmisDTO findById(double id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<CargaValoresAmisDTO> listRelated(Object id) {
		// TODO Auto-generated method stub
		return null;
	}
	
}