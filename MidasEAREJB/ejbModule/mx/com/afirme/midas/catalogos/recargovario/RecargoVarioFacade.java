package mx.com.afirme.midas.catalogos.recargovario;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity RecargoVarioDTO.
 * @see .RecargoVarioDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class RecargoVarioFacade  implements RecargoVarioFacadeRemote {
    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved RecargoVarioDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity RecargoVarioDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(RecargoVarioDTO entity) {
    				LogDeMidasEJB3.log("saving RecargoVarioDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent RecargoVarioDTO entity.
	  @param entity RecargoVarioDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(RecargoVarioDTO entity) {
    				LogDeMidasEJB3.log("deleting RecargoVarioDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(RecargoVarioDTO.class, entity.getIdtorecargovario());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved RecargoVarioDTO entity and return it or a copy of it to the sender. 
	 A copy of the RecargoVarioDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity RecargoVarioDTO entity to update
	 @return RecargoVarioDTO the persisted RecargoVarioDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public RecargoVarioDTO update(RecargoVarioDTO entity) {
    				LogDeMidasEJB3.log("updating RecargoVarioDTO instance", Level.INFO, null);
	        try {
            RecargoVarioDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public RecargoVarioDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding RecargoVarioDTO instance with id: " + id, Level.INFO, null);
	        try {
            RecargoVarioDTO instance = entityManager.find(RecargoVarioDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all RecargoVarioDTO entities with a specific property value.  
	 
	  @param propertyName the name of the RecargoVarioDTO property to query
	  @param value the property value to match
	  	  @return List<RecargoVarioDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<RecargoVarioDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding RecargoVarioDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from RecargoVarioDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all RecargoVarioDTO entities.
	  	  @return List<RecargoVarioDTO> all RecargoVarioDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RecargoVarioDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all RecargoVarioDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from RecargoVarioDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<RecargoVarioDTO> listarFiltrado(RecargoVarioDTO recargoVarioDTO) {		
		try {
			String queryString = "select model from RecargoVarioDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (recargoVarioDTO == null)
				return null;
								
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionrecargo", recargoVarioDTO.getDescripcionrecargo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "clavetiporecargo", recargoVarioDTO.getClavetiporecargo());		
			
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RecargoVarioDTO> listarRecargosPorAsociar(BigDecimal idToProducto) {
		String queryString = "select model from RecargoVarioDTO as model";
		queryString += " where model.idtorecargovario not in (select r.id.idToRecargoVario from RecargoVarioProductoDTO r where r.id.idToProducto = :idToProducto)";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToProducto", idToProducto);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<RecargoVarioDTO> listarRecargosPorAsociarTipoPoliza(
			BigDecimal idToTipoPoliza) {
		String queryString = "select model from RecargoVarioDTO as model";
		queryString += " where model.idtorecargovario not in (" +
				" select r.id.idtorecargovario from RecargoVarioTipoPolizaDTO r where r.id.idtotipopoliza = :idToTipoPoliza) " +
				" and model.idtorecargovario not in ( " +
				" select er.id.idtorecargovario from ExclusionRecargoVarioProductoDTO er where er.id.idtotipopoliza = :idToTipoPolizaEx " +
				")";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToTipoPoliza", idToTipoPoliza);
		query.setParameter("idToTipoPolizaEx", idToTipoPoliza);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<RecargoVarioDTO> listarRecargosPorAsociarCobertura(
			BigDecimal idToCobertura) {
		String queryString = "select model from RecargoVarioDTO as model";
		queryString += " where model.idtorecargovario not in (select r.id.idtorecargovario from RecargoVarioCoberturaDTO r where r.id.idtocobertura = :idToCobertura)";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCobertura", idToCobertura);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<RecargoVarioDTO> listarRecargosEspeciales() {
		String queryString = "select model from RecargoVarioDTO as model";
		queryString += " where model.idtorecargovario < 0";
		Query query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
}