package mx.com.afirme.midas.catalogos.descuento;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity DescuentoDTO.
 * @see .DescuentoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DescuentoFacade  implements DescuentoFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DescuentoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DescuentoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DescuentoDTO entity) {
    				LogDeMidasEJB3.log("saving DescuentoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DescuentoDTO entity.
	  @param entity DescuentoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DescuentoDTO entity) {
    				LogDeMidasEJB3.log("deleting DescuentoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DescuentoDTO.class, entity.getIdToDescuentoVario());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DescuentoDTO entity and return it or a copy of it to the sender. 
	 A copy of the DescuentoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DescuentoDTO entity to update
	 @return DescuentoDTO the persisted DescuentoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DescuentoDTO update(DescuentoDTO entity) {
    				LogDeMidasEJB3.log("updating DescuentoDTO instance", Level.INFO, null);
	        try {
            DescuentoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DescuentoDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding DescuentoDTO instance with id: " + id, Level.INFO, null);
	        try {
            DescuentoDTO instance = entityManager.find(DescuentoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DescuentoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DescuentoDTO property to query
	  @param value the property value to match
	  	  @return List<DescuentoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DescuentoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding DescuentoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DescuentoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all DescuentoDTO entities.
	  	  @return List<DescuentoDTO> all DescuentoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DescuentoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all DescuentoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from DescuentoDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	private boolean esAtributoValido(Object atributo) {
		if (atributo == null) {
			return false;
		}	
		if (atributo instanceof String) {
			return atributo.toString().trim().length() > 0;
		}
		/**
		@SuppressWarnings("unused")
		HashMap hash =  new HashMap();
		Set  keys = hash.keySet();
		Object [] ids = keys.toArray();
		**/		
		return true;
	}

	@SuppressWarnings("unused")
	private String agregaParametro(String parametros, String atributo) {

		if (esAtributoValido(parametros)) {
			parametros = parametros.concat(" and").concat(atributo);
		} else {
			parametros = parametros.concat(atributo);
		}
		return parametros;
	}
	
	@SuppressWarnings("unchecked")
	public List<DescuentoDTO> listarFiltrado(DescuentoDTO descuentoDTO) {
		try {
			String queryString = "select model from DescuentoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			
			
			if (descuentoDTO == null)
				return null;
			
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveTipo", descuentoDTO.getClaveTipo());		
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcion", descuentoDTO.getDescripcion());
			
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		
		} catch (RuntimeException re) {
			throw re;
		}
		
	}

	@SuppressWarnings("unchecked")
	public List<DescuentoDTO> listarDescuentosPorAsociar(BigDecimal idToProducto) {
		String queryString = "select model from DescuentoDTO as model";
		queryString += " where model.idToDescuentoVario not in (select d.id.idToDescuentoVario from DescuentoVarioProductoDTO d where d.id.idToProducto = :idToProducto)";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToProducto", idToProducto);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<DescuentoDTO> listarDescuentosPorAsociarTipoPoliza(BigDecimal idToTipoPoliza) {
		String queryString = "select model from DescuentoDTO as model";
		queryString += " where model.idToDescuentoVario not in (select d.id.idtodescuentovario from DescuentoVarioTipoPolizaDTO d where d.id.idtotipopoliza = :idToTipoPoliza)";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToTipoPoliza", idToTipoPoliza);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<DescuentoDTO> listarDescuentosPorAsociarCobertura(
			BigDecimal idToCobertura) {
		String queryString = "select model from DescuentoDTO as model";
		queryString += " where model.idToDescuentoVario not in (select d.id.idtodescuentovario from DescuentoVarioCoberturaDTO d where d.id.idtocobertura = :idToCobertura)";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCobertura", idToCobertura);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<DescuentoDTO> listarDescuentosEspeciales() {
		String queryString = "select model from DescuentoDTO as model";
		queryString += " where model.idToDescuentoVario < 0";
		Query query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
}