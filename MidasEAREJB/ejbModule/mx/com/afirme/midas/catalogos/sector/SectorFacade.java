package mx.com.afirme.midas.catalogos.sector;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

@Stateless
public class SectorFacade implements SectorFacadeRemote{
	@PersistenceContext
	private EntityManager entityManager;
	@Override
	public SectorDTO findById(BigDecimal arg0) {
		return null;
	}

	@Override
	public SectorDTO findById(CatalogoValorFijoId arg0) {
		return null;
	}

	@Override
	public SectorDTO findById(double arg0) {
		return null;
	}

	@Override
	public List<SectorDTO> listRelated(Object arg0) {
		return null;
	}
	
	/**
	 * Obtiene el catalogo de sector
	 */
	@Override
	public List<SectorDTO> findAll() {
		LogDeMidasEJB3.log("finding all SectorDTO instances", Level.INFO, null);
		try{
			String statement="select model from SectorDTO model";
			Query query= entityManager.createQuery(statement);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Se obtiene el sector por su id
	 */
	@Override
	public SectorDTO findById(String idSector) {
		LogDeMidasEJB3.log("finding SectorDTO by idSector with id:"+idSector, Level.INFO, null);
		try{
			SectorDTO instance=entityManager.find(SectorDTO.class,idSector);
			return instance;
		}catch(RuntimeException  re){
			LogDeMidasEJB3.log("find by id", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Se obtiene el sector por su nombre
	 */
	@Override
	public SectorDTO findByName(String nombreSector) {
		LogDeMidasEJB3.log("finding SectorDTO by sector name with name:"+nombreSector, Level.INFO, null);
		try{
			if(nombreSector==null || nombreSector.isEmpty()){
				return null;
			}
			String queryString="select model from SectorDTO model ";
			String sWhere="";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreSector",nombreSector);
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				
			}
			
			queryString += " order by model.idSector desc, model.nombreSector asc ";//TODO: revisar el comportamiento del paginado con la lista ordenada
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return (SectorDTO)query.getSingleResult();
		}catch(RuntimeException re){
			LogDeMidasEJB3.log("find by name", Level.SEVERE, re);
			throw re;
		}
	}

}
