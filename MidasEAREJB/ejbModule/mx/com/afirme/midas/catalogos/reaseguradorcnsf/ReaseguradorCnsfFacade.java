package mx.com.afirme.midas.catalogos.reaseguradorcnsf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ReaseguradorCnsfFacade implements ReaseguradorCnsfFacadeRemote {
	@PersistenceContext private EntityManager entityManager;
	
	@Override
	public ReaseguradorCnsfDTO findById(BigDecimal arg0) {
		return null;
	}

	@Override
	public List<ReaseguradorCnsfDTO> listRelated(Object arg0) {
		return null;
	}

	@Override
	public void delete(ReaseguradorCnsfDTO entity) {
		LogDeMidasEJB3.log("deleting ReaseguradorCnsfDTO instance", Level.INFO, null);
        try {
    	entity = entityManager.getReference(ReaseguradorCnsfDTO.class, entity.getId());
        entityManager.remove(entity);
        			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
    				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
            throw re;
        }
		
	}

	@Override
	public List<ReaseguradorCnsfDTO> findAll() {
		LogDeMidasEJB3.log("finding all ReaseguradorCnsfDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from ReaseguradorCnsfDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@Override
	public ReaseguradorCnsfDTO findById(String id) {
		LogDeMidasEJB3.log("finding ReaseguradorCnsfDTO instance with id: " + id, Level.INFO, null);
        try {
        	ReaseguradorCnsfDTO instance = entityManager.find(ReaseguradorCnsfDTO.class, id);
        return instance;
	    } catch (RuntimeException re) {
	    				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
	    }
	}

	@Override
	public List<ReaseguradorCnsfDTO> findByProperty(String propertyName, Object value) {
		LogDeMidasEJB3.log("finding ReaseguradorCnsfDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
		try {
		final String queryString = "select model from ReaseguradorCnsfDTO model where model." 
		 						+ propertyName + "= :propertyValue";
							Query query = entityManager.createQuery(queryString);
				query.setParameter("propertyValue", value);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		} 
	}

	@Override
	public List<ReaseguradorCnsfDTO> findByPropertyID(BigDecimal value) {
		LogDeMidasEJB3.log("finding ReaseguradorCnsfDTO instance with property: " +  ", value: " + value, Level.INFO, null);
		try {
		final String queryString = "select model from ReaseguradorCnsfDTO model where model.idReasegurador" 
		 						 + "= :propertyValue";
							Query query = entityManager.createQuery(queryString);
				query.setParameter("propertyValue", value);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}

	@Override
	public List<ReaseguradorCnsfDTO> listarFiltrado(ReaseguradorCnsfDTO ReaseguradorCnsfDTO) {
		try {
			String queryString = "select model from ReaseguradorCnsfDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (ReaseguradorCnsfDTO == null)
				return null;
			if (ReaseguradorCnsfDTO.getIdagencia()!= null)		
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "idagencia", ReaseguradorCnsfDTO.getIdagencia());
			if (ReaseguradorCnsfDTO.getCalificacion()!= null)		
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "calificacion", ReaseguradorCnsfDTO.getCalificacion());
			if (ReaseguradorCnsfDTO.getIdReasegurador()!= null)		
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "idReasegurador", ReaseguradorCnsfDTO.getIdReasegurador());						
			
			if (ReaseguradorCnsfDTO.getClaveCnsf()!= null)
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "claveCnsf", ReaseguradorCnsfDTO.getClaveCnsf());
			
			if (ReaseguradorCnsfDTO.getClaveCnsfAnt()!= null)	
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "claveCnsfAnt", ReaseguradorCnsfDTO.getClaveCnsfAnt());
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@Override
	public void save(ReaseguradorCnsfDTO entity) {
		LogDeMidasEJB3.log("saving ReaseguradorCnsfDTO instance", Level.INFO, null);
        try {
        entityManager.persist(entity);
        			LogDeMidasEJB3.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
    				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
            throw re;
        }
		
	}
	
	@Override 
	public ReaseguradorCnsfDTO update(ReaseguradorCnsfDTO entity) {
		LogDeMidasEJB3.log("updating ReaseguradorCnsfDTO instance", Level.INFO, null);
        try {
        	ReaseguradorCnsfDTO result = entityManager.merge(entity);
        	
        			
            return result;
	    } catch (RuntimeException re) {
	    				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
	    }
	}    
	
	@Override
	public List<ReaseguradorCNSFBitacoraDTO> findAllBitacora() {
		LogDeMidasEJB3.log("finding all ReaseguradorCNSFBitacoraDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from ReaseguradorCNSFBitacoraDTO model" +
					" where model.usuario is not null" +
					" order by model.idReasBit asc";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@Override
	public ReaseguradorCnsfDTO findById(CatalogoValorFijoId arg0) {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public ReaseguradorCnsfDTO findById(double arg0) {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

}
