package mx.com.afirme.midas.catalogos.cuentabanco;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity CuentaBancoDTO.
 * @see .CuentaBancoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class CuentaBancoFacade  implements CuentaBancoFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved CuentaBancoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CuentaBancoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CuentaBancoDTO entity) {
    				LogDeMidasEJB3.log("saving CuentaBancoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CuentaBancoDTO entity.
	  @param entity CuentaBancoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CuentaBancoDTO entity) {
    				LogDeMidasEJB3.log("deleting CuentaBancoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(CuentaBancoDTO.class, entity.getIdtccuentabanco());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CuentaBancoDTO entity and return it or a copy of it to the sender. 
	 A copy of the CuentaBancoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CuentaBancoDTO entity to update
	 @return CuentaBancoDTO the persisted CuentaBancoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CuentaBancoDTO update(CuentaBancoDTO entity) {
    				LogDeMidasEJB3.log("updating CuentaBancoDTO instance", Level.INFO, null);
	        try {
            CuentaBancoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public CuentaBancoDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding CuentaBancoDTO instance with id: " + id, Level.INFO, null);
	        try {
            CuentaBancoDTO instance = entityManager.find(CuentaBancoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all CuentaBancoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the CuentaBancoDTO property to query
	  @param value the property value to match
	  	  @return List<CuentaBancoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<CuentaBancoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding CuentaBancoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from CuentaBancoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all CuentaBancoDTO entities.
	  	  @return List<CuentaBancoDTO> all CuentaBancoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CuentaBancoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all CuentaBancoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from CuentaBancoDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find filtered CuentaBancoDTO entities.
	  	  @return List<CuentaBancoDTO> filtered CuentaBancoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CuentaBancoDTO> listarFiltrado(CuentaBancoDTO cuentaBancoDTO) {		
		try {
			String queryString = "select model from CuentaBancoDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (cuentaBancoDTO == null)
				return null;
			
			if (cuentaBancoDTO.getBeneficiariocuenta()!=null) 
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "beneficiariocuenta",cuentaBancoDTO.getBeneficiariocuenta());
			if (cuentaBancoDTO.getCuentaffc() != null)  
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "cuentaffc", cuentaBancoDTO.getCuentaffc());
			if (cuentaBancoDTO.getDireccionbanco()!=null)  
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "direccionbanco", cuentaBancoDTO.getDireccionbanco());
			if (cuentaBancoDTO.getEstatus()!=null)  
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "estatus", cuentaBancoDTO.getEstatus().toBigInteger().toString());
			if (cuentaBancoDTO.getFfc()!=null)  
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "ffc", cuentaBancoDTO.getFfc());
			if (cuentaBancoDTO.getNombrebanco()!=null)  
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombrebanco", cuentaBancoDTO.getNombrebanco());
			if (cuentaBancoDTO.getNumeroaba()!=null)  
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "numeroaba", cuentaBancoDTO.getNumeroaba());
			if (cuentaBancoDTO.getNumeroclabe()!=null)  
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "numeroclabe", cuentaBancoDTO.getNumeroclabe());
			if (cuentaBancoDTO.getNumerocuenta()!=null)  
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "numerocuenta", cuentaBancoDTO.getNumerocuenta());
			if (cuentaBancoDTO.getNumeroswift()!=null)  
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "numeroswift", cuentaBancoDTO.getNumeroswift());
			if (cuentaBancoDTO.getPais()!=null)  
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "pais", cuentaBancoDTO.getPais());			
			if (cuentaBancoDTO.getReaseguradorCorredor()!=null && cuentaBancoDTO.getReaseguradorCorredor().getIdtcreaseguradorcorredor()!=null) sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "reaseguradorCorredor.idtcreaseguradorcorredor", cuentaBancoDTO.getReaseguradorCorredor().getIdtcreaseguradorcorredor(), "idtcreaseguradorcorredor");
			if (cuentaBancoDTO.getIdTcMoneda()!=null) 
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcMoneda", cuentaBancoDTO.getIdTcMoneda());			
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public CuentaBancoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public CuentaBancoDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<CuentaBancoDTO> listRelated(Object id) {
		return this.findAll();
	}
}