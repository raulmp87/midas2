package mx.com.afirme.midas.catalogos.girotransporte;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity GiroTransporteDTO.
 * @see .GiroTransporteDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class GiroTransporteFacade implements GiroTransporteFacadeRemote {

	@PersistenceContext private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved GiroTransporteDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            GiroTransporteDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(GiroTransporteDTO entity) {
		LogDeMidasEJB3.log("saving GiroTransporteDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent GiroTransporteDTO entity.
	 * 
	 * @param entity
	 *            GiroTransporteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(GiroTransporteDTO entity) {
		LogDeMidasEJB3.log("deleting GiroTransporteDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(GiroTransporteDTO.class, entity.getIdGiroTransporte());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved GiroTransporteDTO entity and return it or a
	 * copy of it to the sender. A copy of the GiroTransporteDTO entity parameter
	 * is returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            GiroTransporteDTO entity to update
	 * @return GiroTransporteDTO the persisted GiroTransporteDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public GiroTransporteDTO update(GiroTransporteDTO entity) {
		LogDeMidasEJB3.log("updating GiroTransporteDTO instance", Level.INFO, null);
		try {
			GiroTransporteDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public GiroTransporteDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding GiroTransporteDTO instance with id: " + id,
				Level.INFO, null);
		try {
			GiroTransporteDTO instance = entityManager.find(
					GiroTransporteDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all GiroTransporteDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the GiroTransporteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<GiroTransporteDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<GiroTransporteDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding GiroTransporteDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from GiroTransporteDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all GiroTransporteDTO entities.
	 * 
	 * @return List<GiroTransporteDTO> all GiroTransporteDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<GiroTransporteDTO> findAll() {
		LogDeMidasEJB3.log("finding all GiroTransporteDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from GiroTransporteDTO model " +
					"order by model.descripcionGiroTransporte";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<GiroTransporteDTO> listarFiltrado(GiroTransporteDTO giroTransporteDTO){
		try {
			String queryString = "select model from GiroTransporteDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (giroTransporteDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoGiroTransporte", giroTransporteDTO.getCodigoGiroTransporte());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "descripcionGiroTransporte", giroTransporteDTO.getDescripcionGiroTransporte());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idGrupoROT", giroTransporteDTO.getIdGrupoROT());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public GiroTransporteDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public GiroTransporteDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<GiroTransporteDTO> listRelated(Object id) {
		return this.findAll();
	}
}