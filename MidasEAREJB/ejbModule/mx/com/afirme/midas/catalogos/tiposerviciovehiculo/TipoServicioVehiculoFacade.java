package mx.com.afirme.midas.catalogos.tiposerviciovehiculo;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity TipoServicioVehiculoDTO.
 * @see .TipoServicioVehiculoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class TipoServicioVehiculoFacade  implements TipoServicioVehiculoFacadeRemote {
    @PersistenceContext private EntityManager entityManager;
    private TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote;
    
	@EJB
	public void setTipoVehiculoFacadeRemote(
			TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote) {
		this.tipoVehiculoFacadeRemote = tipoVehiculoFacadeRemote;
	}

	
		/**
	 Perform an initial save of a previously unsaved TipoServicioVehiculoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoServicioVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoServicioVehiculoDTO entity) {
    				LogUtil.log("saving TipoServicioVehiculoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent TipoServicioVehiculoDTO entity.
	  @param entity TipoServicioVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoServicioVehiculoDTO entity) {
    				LogUtil.log("deleting TipoServicioVehiculoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(TipoServicioVehiculoDTO.class, entity.getIdTcTipoServicioVehiculo());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved TipoServicioVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoServicioVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoServicioVehiculoDTO entity to update
	 @return TipoServicioVehiculoDTO the persisted TipoServicioVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public TipoServicioVehiculoDTO update(TipoServicioVehiculoDTO entity) {
    				LogUtil.log("updating TipoServicioVehiculoDTO instance", Level.INFO, null);
	        try {
            TipoServicioVehiculoDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public TipoServicioVehiculoDTO findById( BigDecimal id) {
    				LogUtil.log("finding TipoServicioVehiculoDTO instance with id: " + id, Level.INFO, null);
	        try {
            TipoServicioVehiculoDTO instance = entityManager.find(TipoServicioVehiculoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all TipoServicioVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoServicioVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<TipoServicioVehiculoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<TipoServicioVehiculoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding TipoServicioVehiculoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from TipoServicioVehiculoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all TipoServicioVehiculoDTO entities.
	  	  @return List<TipoServicioVehiculoDTO> all TipoServicioVehiculoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoServicioVehiculoDTO> findAll(
		) {
					LogUtil.log("finding all TipoServicioVehiculoDTO instances", Level.INFO, null);
			try {
				final String queryString = "select model from TipoServicioVehiculoDTO model";
				Query query = entityManager.createQuery(queryString);
					
				List<TipoServicioVehiculoDTO> respuesta = (List<TipoServicioVehiculoDTO>)query.getResultList();
				int index=0;
				for(TipoServicioVehiculoDTO item : respuesta){
					TipoVehiculoDTO tipoVehiculoDTO = tipoVehiculoFacadeRemote.findById(item.getIdTcTipoVehiculo());
					respuesta.get(index).setTipoVehiculoDTO(tipoVehiculoDTO);
					index++;
				}
				return respuesta;
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TipoServicioVehiculoDTO> listarFiltrado(TipoServicioVehiculoDTO tipoServicioVehiculoDTO){
		try {
			String queryString = "select model from TipoServicioVehiculoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (tipoServicioVehiculoDTO == null)
				return null;
			/*if(tipoServicioVehiculoDTO.getTipoVehiculoDTO().getId() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoVehiculoDTO..id.idTcTipoVehiculo", tipoServicioVehiculoDTO.getTipoVehiculoDTO().getId().getIdTcTipoVehiculo());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoVehiculoDTO.id.claveTipoBien", tipoServicioVehiculoDTO.getTipoVehiculoDTO().getId().getClaveTipoBien());
			}*/
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcTipoVehiculo", tipoServicioVehiculoDTO.getIdTcTipoVehiculo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcTipoServicioVehiculo", tipoServicioVehiculoDTO.getIdTcTipoServicioVehiculo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoTipoServicioVehiculo", tipoServicioVehiculoDTO.getCodigoTipoServicioVehiculo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionTipoServVehiculo", tipoServicioVehiculoDTO.getDescripcionTipoServVehiculo());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			

			List<TipoServicioVehiculoDTO> respuesta = (List<TipoServicioVehiculoDTO>)query.getResultList();
			int index=0;
			for(TipoServicioVehiculoDTO item : respuesta){
				TipoVehiculoDTO tipoVehiculoDTO = tipoVehiculoFacadeRemote.findById(item.getIdTcTipoVehiculo());
				respuesta.get(index).setTipoVehiculoDTO(tipoVehiculoDTO);
				index++;
			}
			return respuesta;
			
//			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@Override
	public TipoServicioVehiculoDTO findById(CatalogoValorFijoId arg0) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public TipoServicioVehiculoDTO findById(double arg0) {
		return findById(new BigDecimal(""+arg0));
	}

	@Override
	public List<TipoServicioVehiculoDTO> listRelated(Object arg0) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}
}