package mx.com.afirme.midas.catalogos.ajustador;
// default package


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity AjustadorDTO.
 * @see .AjustadorDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class AjustadorFacade  implements AjustadorFacadeRemote {

	public static final String RAZONSOCIAL = "razonsocial";
	public static final String RFC = "rfc";
	public static final String NOMBRE = "nombre";
	public static final String CALLEYNUMERO = "calleynumero";
	public static final String CODIGOPOSTAL = "codigopostal";
	public static final String ESTADO = "estado";
	public static final String CIUDAD = "ciudad";
	public static final String COLONIA = "colonia";
	public static final String TELEFONO = "telefono";
	public static final String TELEFONOCELULAR = "telefonocelular";
	public static final String RADIOLOCALIZADOR = "radiolocalizador";
	public static final String RAMO = "ramo";
	public static final String TIPODECONTRIBUYENTE = "tipodecontribuyente";
	public static final String TIPODEPROVEEDOR = "tipodeproveedor";
	public static final String EMAIL = "email";	
	public static final String ASIGNACIONES = "asignaciones";	



    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved AjustadorDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AjustadorDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AjustadorDTO entity) {
    				LogDeMidasEJB3.log("saving AjustadorDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent AjustadorDTO entity.
	  @param entity AjustadorDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AjustadorDTO entity) {
    				LogDeMidasEJB3.log("deleting AjustadorDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(AjustadorDTO.class, entity.getIdTcAjustador());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved AjustadorDTO entity and return it or a copy of it to the sender. 
	 A copy of the AjustadorDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AjustadorDTO entity to update
	 @return AjustadorDTO the persisted AjustadorDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public AjustadorDTO update(AjustadorDTO entity) {
    				LogDeMidasEJB3.log("updating AjustadorDTO instance", Level.INFO, null);
	        try {
            AjustadorDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public AjustadorDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding AjustadorDTO instance with id: " + id, Level.INFO, null);
	        try {
            AjustadorDTO instance = entityManager.find(AjustadorDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all AjustadorDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AjustadorDTO property to query
	  @param value the property value to match
	  	  @return List<AjustadorDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<AjustadorDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding AjustadorDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from AjustadorDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all AjustadorDTO entities.
	  	  @return List<AjustadorDTO> all AjustadorDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<AjustadorDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all AjustadorDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from AjustadorDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	private boolean esAtributoValido(Object atributo) {
		if (atributo instanceof String && atributo != null) {
			return atributo.toString().trim().length() > 0;
		}

		if (atributo == null) {
			return false;
		}
		return false;
	}

	@SuppressWarnings("unused")
	private String agregaParametro(String parametros, String atributo) {

		if (esAtributoValido(parametros)) {
			parametros = parametros.concat(" and").concat(atributo);
		} else {
			parametros = parametros.concat(atributo);
		}
		return parametros;
	}	
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<AjustadorDTO> listarFiltrado(AjustadorDTO ajustadorDTO) {
		LogDeMidasEJB3.log("finding all AjustadorDTO instances", Level.INFO, null);
		try {
			String queryString = "select model from AjustadorDTO AS model";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (ajustadorDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, RAZONSOCIAL, ajustadorDTO.getRazonSocial());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, RFC, ajustadorDTO.getRfc());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, NOMBRE, ajustadorDTO.getNombre());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, CALLEYNUMERO, ajustadorDTO.getCalleYNumero());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, CODIGOPOSTAL, ajustadorDTO.getCodigoPostal());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, ESTADO, ajustadorDTO.getEstado());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, CIUDAD, ajustadorDTO.getCiudad());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, COLONIA, ajustadorDTO.getColonia());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, TELEFONO, ajustadorDTO.getTelefono());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, EMAIL, ajustadorDTO.getEmail());
			
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
			}
//			queryString = queryString.concat(" order by model.fechaHoraReporte desc");
			System.out.println(queryString);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}	
	
	@SuppressWarnings("unchecked")
	public List<AjustadorDTO> listarPorOrdenAsignacion() {
					LogDeMidasEJB3.log("listarPorOrdenAsignacion AjustadorDTO instances", Level.INFO, null);
			try {
			final String queryString = "select ajustador from AjustadorDTO ajustador order by ajustador.asignaciones, ajustador.idTcAjustador desc";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("listarPorOrdenAsignacion failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<AjustadorDTO> ajustadoresPorEstatus(Short estatus){
		try {
			StringBuilder sb = new StringBuilder();		
			sb.append("select  ");
			sb.append("		model ");
			sb.append("from ");
			sb.append("		AjustadorDTO model ");
			sb.append("where ");
			sb.append("		model.estatus = :estatus ");
			
			final String queryString = sb.toString();
			Query query = entityManager.createQuery(queryString);
			query.setParameter("estatus", estatus);
			
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("ajustadoresPorEstatus failed", Level.SEVERE, re);
			throw re;
		}		
	}

	public AjustadorDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public AjustadorDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<AjustadorDTO> listRelated(Object id) {
		return this.findAll();
	}
}