package mx.com.afirme.midas.catalogos.lloyds;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.lloyds.LloydsDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity LloydsDTO.
 * @see .LloydsDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class LloydsFacade  implements LloydsFacadeRemote {
	//property constants

    @PersistenceContext private EntityManager entityManager;
    
    public LloydsDTO findById(BigDecimal id) {
    		LogDeMidasEJB3.log("finding LloydsDTO instance with id: " + id, Level.INFO, null);
	        try {
            LloydsDTO instance = entityManager.find(LloydsDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all LloydsDTO entities with a specific property value.  
	 
	  @param propertyName the name of the LloydsDTO property to query
	  @param value the property value to match
	  	  @return List<LloydsDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<LloydsDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding LloydsDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from LloydsDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all LloydsDTO entities.
	  	  @return List<LloydsDTO> all LloydsDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<LloydsDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all LloydsDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from LloydsDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find all LloydsDTO entities.
	  	  @return List<LloydsDTO> all LloydsDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<LloydsDTO> findAllOrderedBy(String...fieldNames) {
					LogDeMidasEJB3.log("finding all LloydsDTO instances", Level.INFO, null);
			try {
			StringBuffer orderedString = new StringBuffer();
			for (int x=0; x < fieldNames.length; x++) {
				if (x != 0) {
					orderedString.append(",");
				}
				orderedString.append("model.");
				orderedString.append(fieldNames[x]);
			}
			final String queryString = "select model from LloydsDTO model order by " 
				+ orderedString;
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	
	
}