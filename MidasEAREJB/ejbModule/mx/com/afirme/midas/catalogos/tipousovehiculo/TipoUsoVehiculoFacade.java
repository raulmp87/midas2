package mx.com.afirme.midas.catalogos.tipousovehiculo;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity TipoUsoVehiculoDTO.
 * @see .TipoUsoVehiculoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class TipoUsoVehiculoFacade  implements TipoUsoVehiculoFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
    
    private TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote;
    
	@EJB
	public void setTipoVehiculoFacadeRemote(
			TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote) {
		this.tipoVehiculoFacadeRemote = tipoVehiculoFacadeRemote;
	}

		/**
	 Perform an initial save of a previously unsaved TipoUsoVehiculoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoUsoVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoUsoVehiculoDTO entity) {
    				LogUtil.log("saving TipoUsoVehiculoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent TipoUsoVehiculoDTO entity.
	  @param entity TipoUsoVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoUsoVehiculoDTO entity) {
    				LogUtil.log("deleting TipoUsoVehiculoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(TipoUsoVehiculoDTO.class, entity.getIdTcTipoUsoVehiculo());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved TipoUsoVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoUsoVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoUsoVehiculoDTO entity to update
	 @return TipoUsoVehiculoDTO the persisted TipoUsoVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public TipoUsoVehiculoDTO update(TipoUsoVehiculoDTO entity) {
    				LogUtil.log("updating TipoUsoVehiculoDTO instance", Level.INFO, null);
	        try {
            TipoUsoVehiculoDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public TipoUsoVehiculoDTO findById( BigDecimal id) {
    				LogUtil.log("finding TipoUsoVehiculoDTO instance with id: " + id, Level.INFO, null);
	        try {
            TipoUsoVehiculoDTO instance = entityManager.find(TipoUsoVehiculoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all TipoUsoVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoUsoVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<TipoUsoVehiculoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<TipoUsoVehiculoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding TipoUsoVehiculoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from TipoUsoVehiculoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all TipoUsoVehiculoDTO entities.
	  	  @return List<TipoUsoVehiculoDTO> all TipoUsoVehiculoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoUsoVehiculoDTO> findAll(
		) {
					LogUtil.log("finding all TipoUsoVehiculoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from TipoUsoVehiculoDTO model";
								Query query = entityManager.createQuery(queryString);
					List<TipoUsoVehiculoDTO> respuesta = (List<TipoUsoVehiculoDTO>)query.getResultList();
					int index=0;
					for(TipoUsoVehiculoDTO item : respuesta){
						TipoVehiculoDTO tipoVehiculoDTO = tipoVehiculoFacadeRemote.findById(item.getIdTcTipoVehiculo());
						respuesta.get(index).setTipoVehiculoDTO(tipoVehiculoDTO);
						index++;
					}
					return respuesta;
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TipoUsoVehiculoDTO> listarFiltrado(TipoUsoVehiculoDTO tipoUsoVehiculoDTO){
		try {
			String queryString = "select model from TipoUsoVehiculoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (tipoUsoVehiculoDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcTipoVehiculo", tipoUsoVehiculoDTO.getIdTcTipoVehiculo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcTipoUsoVehiculo", tipoUsoVehiculoDTO.getIdTcTipoUsoVehiculo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoTipoUsoVehiculo", tipoUsoVehiculoDTO.getCodigoTipoUsoVehiculo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionTipoUsoVehiculo", tipoUsoVehiculoDTO.getDescripcionTipoUsoVehiculo());
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			
			List<TipoUsoVehiculoDTO> respuesta = (List<TipoUsoVehiculoDTO>)query.getResultList();
			int index=0;
			for(TipoUsoVehiculoDTO item : respuesta){
				TipoVehiculoDTO tipoVehiculoDTO = tipoVehiculoFacadeRemote.findById(item.getIdTcTipoVehiculo());
				respuesta.get(index).setTipoVehiculoDTO(tipoVehiculoDTO);
				index++;
			}
			return respuesta;
//			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoUsoVehiculoDTO> findByClaveTipoBien(String claveTipoBien){
		try {
			final String queryString = "select distinct model from TipoUsoVehiculoDTO model, TipoVehiculoDTO vehiculo " +
															"where model.idTcTipoVehiculo = vehiculo.idTcTipoVehiculo " +
																"and vehiculo.tipoBienAutosDTO.claveTipoBien = :claveTipoBien " +
																"order by model.codigoTipoUsoVehiculo asc";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("claveTipoBien", claveTipoBien);	
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all TipoUsoVehiculo by claveTipoBien= "+claveTipoBien + " failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	

	
	
	@SuppressWarnings("unchecked")
	public List<TipoUsoVehiculoDTO> findByClaveTipoBienBySeccion(BigDecimal idToSeccion){
		try {
			final String queryString = "select distinct model from TipoUsoVehiculoDTO model,TipoVehiculoDTO vehiculo,SeccionDTO seccion " +
			" WHERE model.idTcTipoVehiculo = vehiculo.idTcTipoVehiculo " +
				" and seccion.tipoVehiculo.idTcTipoVehiculo = vehiculo.idTcTipoVehiculo " +
				" and seccion.idToSeccion = :idToSeccion " +
				" order by model.codigoTipoUsoVehiculo asc";
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", idToSeccion);	
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all TipoUsoVehiculo by idToSeccion= "+idToSeccion + " failed", Level.SEVERE, re);
				throw re;
		}
	}

	@Override
	public TipoUsoVehiculoDTO findById(CatalogoValorFijoId arg0) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public TipoUsoVehiculoDTO findById(double arg0) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public List<TipoUsoVehiculoDTO> listRelated(Object arg0) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public TipoUsoVehiculoDTO findByTipoVehiculoAndDescripcion(
			TipoVehiculoDTO tipoVehiculoDTO, String usoDescripcion) {
		final String jpql = "select model from TipoUsoVehiculoDTO model where model.idTcTipoVehiculo = :idTcTipoVehiculo " +
				"and upper(model.descripcionTipoUsoVehiculo) = :usoDescripcion";
		TypedQuery<TipoUsoVehiculoDTO> query = entityManager.createQuery(jpql, TipoUsoVehiculoDTO.class);
		query.setParameter("idTcTipoVehiculo", tipoVehiculoDTO.getIdTcTipoVehiculo());
		query.setParameter("usoDescripcion", usoDescripcion.toUpperCase());
		List<TipoUsoVehiculoDTO> resultList = query.getResultList();
		//Los tipoUso no deberian repetirse con la descripcion sin embargo si se pueden repetir por eso la condicion checa
		//que sea mayor a 0 en lugar de que sea igual a uno y para estos casos regresaria el primero de la lista. Esto puede generar un
		//comportamiento no deterministico.
		if (resultList.size() > 0) {
			return resultList.get(0);
		}
		return null;
	}
}