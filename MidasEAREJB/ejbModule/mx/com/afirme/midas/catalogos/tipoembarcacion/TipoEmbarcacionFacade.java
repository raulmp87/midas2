package mx.com.afirme.midas.catalogos.tipoembarcacion;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity TipoEmbarcacionDTO.
 * 
 * @see .TipoEmbarcacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoEmbarcacionFacade implements TipoEmbarcacionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoEmbarcacionDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoEmbarcacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoEmbarcacionDTO entity) {
		LogDeMidasEJB3.log("saving TipoEmbarcacionDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoEmbarcacionDTO entity.
	 * 
	 * @param entity
	 *            TipoEmbarcacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoEmbarcacionDTO entity) {
		LogDeMidasEJB3.log("deleting TipoEmbarcacionDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(TipoEmbarcacionDTO.class, entity
					.getIdTcTipoEmbarcacion());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoEmbarcacionDTO entity and return it or a copy
	 * of it to the sender. A copy of the TipoEmbarcacionDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoEmbarcacionDTO entity to update
	 * @return TipoEmbarcacionDTO the persisted TipoEmbarcacionDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoEmbarcacionDTO update(TipoEmbarcacionDTO entity) {
		LogDeMidasEJB3.log("updating TipoEmbarcacionDTO instance", Level.INFO, null);
		try {
			TipoEmbarcacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoEmbarcacionDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding TipoEmbarcacionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoEmbarcacionDTO instance = entityManager.find(
					TipoEmbarcacionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoEmbarcacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoEmbarcacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoEmbarcacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoEmbarcacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TipoEmbarcacionDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoEmbarcacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoEmbarcacionDTO entities.
	 * 
	 * @return List<TipoEmbarcacionDTO> all TipoEmbarcacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoEmbarcacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoEmbarcacionDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from TipoEmbarcacionDTO model " +
					"order by model.descripcionTipoEmbarcacion";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find filtered TipoEmbarcacionDTO entities.
	 * 
	 * @return List<TipoEmbarcacionDTO> filtered TipoEmbarcacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoEmbarcacionDTO> listarFiltrado(TipoEmbarcacionDTO tipoEmbarcacionDTO) {		
		try {
			String queryString = "select model from TipoEmbarcacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (tipoEmbarcacionDTO == null)
				return null;
						
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionTipoEmbarcacion", tipoEmbarcacionDTO.getDescripcionTipoEmbarcacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoTipoEmbarcacion", tipoEmbarcacionDTO.getCodigoTipoEmbarcacion());		
			
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoEmbarcacionDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoEmbarcacionDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<TipoEmbarcacionDTO> listRelated(Object id) {
		return this.findAll();
	}
}