package mx.com.afirme.midas.catalogos.agenciacalificadora;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class AgenciaCalificadoraFacade implements
AgenciaCalificadoraFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;
	@SuppressWarnings("unchecked")
	public List<AgenciaCalificadoraDTO> findAll() {
		final String queryString = "select model from AgenciaCalificadoraDTO as model ORDER BY model.nombreAgencia";
		Query query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<AgenciaCalificadoraDTO> findByProperty(String property, String value) {
		StringBuilder queryBuidler = new StringBuilder();
		
		queryBuidler.append("SELECT model FROM AgenciaCalificadoraDTO model WHERE model." + property + " = :value");
		Query query = entityManager.createQuery(queryBuidler.toString());
		query.setParameter("value", value);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query.getResultList();
	}

	@Override
	public AgenciaCalificadoraDTO findById(BigDecimal id) {
		
		LogDeMidasEJB3.log("finding ContactoDTO instance with id: " + id, Level.INFO, null);
        try {
	        return entityManager.find(AgenciaCalificadoraDTO.class, id);
	    } catch (RuntimeException re) {
	    				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
	    }
    
	}

	@Override
	public AgenciaCalificadoraDTO findById(CatalogoValorFijoId arg0) {
		return null;
	}

	@Override
	public AgenciaCalificadoraDTO findById(double arg0) {
		return null;
	}

	@Override
	public List<AgenciaCalificadoraDTO> listRelated(Object arg0) {
		return null;
	}
 
	public AgenciaCalificadoraDTO findById(String id) {
		LogDeMidasEJB3.log("finding PaisDTO instance with id: " + id,
				Level.INFO, null);
		try {
			return entityManager.find(AgenciaCalificadoraDTO.class,new BigDecimal(id));
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
}