package mx.com.afirme.midas.catalogos.tipobandera;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity TipoBanderaDTO.
 * 
 * @see .TipoBanderaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoBanderaFacade implements TipoBanderaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoBanderaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoBanderaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoBanderaDTO entity) {
		LogDeMidasEJB3.log("saving TipoBanderaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoBanderaDTO entity.
	 * 
	 * @param entity
	 *            TipoBanderaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoBanderaDTO entity) {
		LogDeMidasEJB3.log("deleting TipoBanderaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(TipoBanderaDTO.class, entity
					.getIdTcTipoBandera());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoBanderaDTO entity and return it or a copy of
	 * it to the sender. A copy of the TipoBanderaDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            TipoBanderaDTO entity to update
	 * @return TipoBanderaDTO the persisted TipoBanderaDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoBanderaDTO update(TipoBanderaDTO entity) {
		LogDeMidasEJB3.log("updating TipoBanderaDTO instance", Level.INFO, null);
		try {
			TipoBanderaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoBanderaDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding TipoBanderaDTO instance with id: " + id, Level.INFO,
				null);
		try {
			TipoBanderaDTO instance = entityManager.find(TipoBanderaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoBanderaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoBanderaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoBanderaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoBanderaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TipoBanderaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoBanderaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoBanderaDTO entities.
	 * 
	 * @return List<TipoBanderaDTO> all TipoBanderaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoBanderaDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoBanderaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from TipoBanderaDTO model " +
					"order by model.descripcionTipoBandera";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	/**
	 * Find filtered TipoBanderaDTO entities.
	 * 
	 * @return List<TipoBanderaDTO> filtered TipoBanderaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoBanderaDTO> listarFiltrado(TipoBanderaDTO tipoBanderaDTO) {		
		try {
			String queryString = "select model from TipoBanderaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (tipoBanderaDTO == null)
				return null;
						
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionTipoBandera", tipoBanderaDTO.getDescripcionTipoBandera());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoTipoBandera", tipoBanderaDTO.getCodigoTipoBandera());		
			
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoBanderaDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoBanderaDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<TipoBanderaDTO> listRelated(Object id) {
		return this.findAll();
	}
}