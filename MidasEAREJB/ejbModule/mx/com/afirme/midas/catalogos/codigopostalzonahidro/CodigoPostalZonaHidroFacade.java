package mx.com.afirme.midas.catalogos.codigopostalzonahidro;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoDTO;
import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity CodigoPostalZonaHidroDTO.
 * 
 * @see .CodigoPostalZonaHidroDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CodigoPostalZonaHidroFacade implements
		CodigoPostalZonaHidroFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private CodigoPostalZonaSismoFacadeRemote codigoPostalZonaSismoFacadeRemote;

	@Resource
	private SessionContext context;
	/**
	 * Perform an initial save of a previously unsaved CodigoPostalZonaHidroDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CodigoPostalZonaHidroDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CodigoPostalZonaHidroDTO entity) {
		LogDeMidasEJB3.log("saving CodigoPostalZonaHidroDTO instance",
				Level.FINE, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent CodigoPostalZonaHidroDTO entity.
	 * 
	 * @param entity
	 *            CodigoPostalZonaHidroDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CodigoPostalZonaHidroDTO entity) {
		LogDeMidasEJB3.log("deleting CodigoPostalZonaHidroDTO instance",
				Level.FINE, null);
		try {
			entity = entityManager.getReference(CodigoPostalZonaHidroDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved CodigoPostalZonaHidroDTO entity and return it
	 * or a copy of it to the sender. A copy of the CodigoPostalZonaHidroDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CodigoPostalZonaHidroDTO entity to update
	 * @return CodigoPostalZonaHidroDTO the persisted CodigoPostalZonaHidroDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CodigoPostalZonaHidroDTO update(CodigoPostalZonaHidroDTO entity) {
		LogDeMidasEJB3.log("updating CodigoPostalZonaHidroDTO instance",
				Level.FINE, null);
		try {
			CodigoPostalZonaHidroDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.FINE, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CodigoPostalZonaHidroDTO findById(CodigoPostalZonaHidroId id) {
		LogDeMidasEJB3.log(
				"finding CodigoPostalZonaHidroDTO instance with id: " + id,
				Level.FINE, null);
		try {
			CodigoPostalZonaHidroDTO instance = entityManager.find(
					CodigoPostalZonaHidroDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CodigoPostalZonaHidroDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the CodigoPostalZonaHidroDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CodigoPostalZonaHidroDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CodigoPostalZonaHidroDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding CodigoPostalZonaHidroDTO instance with property: "
						+ propertyName + ", value: " + value, Level.FINE, null);
		try {
			final String queryString = "select model from CodigoPostalZonaHidroDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CodigoPostalZonaHidroDTO entities.
	 * 
	 * @return List<CodigoPostalZonaHidroDTO> all CodigoPostalZonaHidroDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<CodigoPostalZonaHidroDTO> findAll() {
		LogDeMidasEJB3.log("finding all CodigoPostalZonaHidroDTO instances",
				Level.FINE, null);
		try {
			final String queryString = "select model from CodigoPostalZonaHidroDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find filtered CodigoPostalZonaHidroDTO entities.
	 * 
	 * @return List<CodigoPostalZonaHidroDTO> filtered CodigoPostalZonaHidroDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<CodigoPostalZonaHidroDTO> listarFiltrado(
			CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO) {
		try {
			String queryString = "select model from CodigoPostalZonaHidroDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (codigoPostalZonaHidroDTO == null) {
				return null;
			}
			
			BigDecimal idZonaHidro = null;
			
			if(codigoPostalZonaHidroDTO.getNombreColonia() != null) {
				idZonaHidro = new BigDecimal(codigoPostalZonaHidroDTO.getNombreColonia());
			}
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "zonaHidro.idTcZonaHidro", idZonaHidro, "idTcZonaHidro");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.codigoPostal", codigoPostalZonaHidroDTO
							.getCodigoPostal(), "codigoPostal");

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if(codigoPostalZonaHidroDTO.getPrimerRegistroACargar() != null && codigoPostalZonaHidroDTO.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(codigoPostalZonaHidroDTO.getPrimerRegistroACargar().intValue());
				query.setMaxResults(codigoPostalZonaHidroDTO.getNumeroMaximoRegistrosACargar().intValue());
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
			
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public CodigoPostalZonaHidroDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding CodigoPostalZonaHidroDTO instance with id: " + id,
				Level.FINE, null);
		try {
			CodigoPostalZonaHidroDTO instance = entityManager.find(CodigoPostalZonaHidroDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void agregarCodigoPostalZonasHidroSismo(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO, 
		CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO){
	    
	    try{
		this.save(codigoPostalZonaHidroDTO);
		this.codigoPostalZonaSismoFacadeRemote.save(codigoPostalZonaSismoDTO);
		
	    }catch (RuntimeException e) {
		context.setRollbackOnly();
		LogDeMidasEJB3.log("agregarCodigosPostalesZonaHidroZonaSismo failed", Level.SEVERE, e);
		throw e;
	    }
	    
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void modificarCodigoPostalZonasHidroSismo(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO, 
		CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO){
	    
	    try{
		this.update(codigoPostalZonaHidroDTO);
		this.codigoPostalZonaSismoFacadeRemote.update(codigoPostalZonaSismoDTO);
		
	    }catch (RuntimeException e) {
		context.setRollbackOnly();
		LogDeMidasEJB3.log("modificarCodigosPostalesZonaHidroZonaSismo failed", Level.SEVERE, e);
		throw e;
	    }
	    
	}
	
	

	public CodigoPostalZonaHidroDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public CodigoPostalZonaHidroDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<CodigoPostalZonaHidroDTO> listRelated(BigDecimal id) {
		return this.findAll();
	}

	@SuppressWarnings("unchecked")
	public Long obtenerTotalFiltrado(
			CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO) {
		try {
			String queryString = "select count(model) from CodigoPostalZonaHidroDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (codigoPostalZonaHidroDTO == null) {
				return null;
			}
			
			BigDecimal idZonaHidro = null;
			
			if(codigoPostalZonaHidroDTO.getNombreColonia() != null) {
				idZonaHidro = new BigDecimal(codigoPostalZonaHidroDTO.getNombreColonia());
			}
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "zonaHidro.idTcZonaHidro", idZonaHidro, "idTcZonaHidro");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.codigoPostal", codigoPostalZonaHidroDTO
							.getCodigoPostal(), "codigoPostal");

			if (Utilerias.esAtributoQueryValido(sWhere)) {
				queryString = queryString.concat(" where ").concat(sWhere);
			}
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			return (Long)query.getSingleResult();
			
		} catch (RuntimeException re) {
			throw re;
		}
	}
}