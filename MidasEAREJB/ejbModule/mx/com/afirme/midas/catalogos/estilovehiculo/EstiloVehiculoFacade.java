package mx.com.afirme.midas.catalogos.estilovehiculo;
// default package

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.marcavehiculo.SubMarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.SubMarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoId;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.tipovehiculo.SeccionTipoVehiculoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.tipovehiculo.SeccionTipoVehiculoId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas.usuario.LogUtil;
import mx.com.afirme.midas2.dao.catalogos.VarModifDescripcionDao;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.dto.wrapper.VersionCargaComboDTO;
import mx.com.afirme.midas2.service.ExcelRowMapper;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.impl.ExcelConverter;
import mx.com.afirme.midas2.service.impl.ExcelConverter.ExcelConfig.ExcelConfigBuilder;
import mx.com.afirme.midas2.service.tarifa.TarifaAgrupadorTarifaService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.exceptions.QueryException;
/**
 * Facade for entity EstiloVehiculoDTO.
 * @see .EstiloVehiculoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class EstiloVehiculoFacade  implements EstiloVehiculoFacadeRemote {




	 private MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote;
	 private SubMarcaVehiculoFacadeRemote subMarcaVehiculoFacadeRemote;
    @PersistenceContext private EntityManager entityManager;    
    private TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService;
    
    private EntidadService entidadService;
    
    private VarModifDescripcionDao varModifDescripcionDao;
    
	@EJB
	public void setVarModifDescripcionDao(VarModifDescripcionDao varModifDescripcionDao) {
		this.varModifDescripcionDao = varModifDescripcionDao;
	}
    
    @EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
    
	@EJB
	public void setTarifaAgrupadorTarifaService(
			TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService) {
		this.tarifaAgrupadorTarifaService = tarifaAgrupadorTarifaService;
	}
	
	 private TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote;
	    
		@EJB
		public void setTipoVehiculoFacadeRemote(
				TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote) {
			this.tipoVehiculoFacadeRemote = tipoVehiculoFacadeRemote;
		}
		@EJB
		public void setMarcaVehiculoFacadeRemote(
				MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote) {
			this.marcaVehiculoFacadeRemote = marcaVehiculoFacadeRemote;
		}
		@EJB
		public void setSubMarcaVehiculoFacadeRemote(
				SubMarcaVehiculoFacadeRemote subMarcaVehiculoFacadeRemote) {
			this.subMarcaVehiculoFacadeRemote = subMarcaVehiculoFacadeRemote;
		}

		/**
	 Perform an initial save of a previously unsaved EstiloVehiculoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EstiloVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EstiloVehiculoDTO entity) {
    				LogUtil.log("saving EstiloVehiculoDTO instance", Level.FINE, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.FINE, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent EstiloVehiculoDTO entity.
	  @param entity EstiloVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EstiloVehiculoDTO entity) {
    				LogUtil.log("deleting EstiloVehiculoDTO instance", Level.FINE, null);
	        try {
        	entity = entityManager.getReference(EstiloVehiculoDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.FINE, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved EstiloVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the EstiloVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EstiloVehiculoDTO entity to update
	 @return EstiloVehiculoDTO the persisted EstiloVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public EstiloVehiculoDTO update(EstiloVehiculoDTO entity) {
    				LogUtil.log("updating EstiloVehiculoDTO instance", Level.FINE, null);
	        try {
            EstiloVehiculoDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.FINE, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public EstiloVehiculoDTO findById( EstiloVehiculoId id) {
    				LogUtil.log("finding EstiloVehiculoDTO instance with id: " + id, Level.FINE, null);
	        try {
            EstiloVehiculoDTO instance = entityManager.find(EstiloVehiculoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all EstiloVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EstiloVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<EstiloVehiculoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<EstiloVehiculoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding EstiloVehiculoDTO instance with property: " + propertyName + ", value: " + value, Level.FINE, null);
			try {
			final String queryString = "select model from EstiloVehiculoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all EstiloVehiculoDTO entities.
	  	  @return List<EstiloVehiculoDTO> all EstiloVehiculoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<EstiloVehiculoDTO> findAll(
		) {
					LogUtil.log("finding all EstiloVehiculoDTO instances", Level.FINE, null);
			try {
			final String queryString = "select model from EstiloVehiculoDTO model where model.id.claveTipoBien = 'CAMN' AND model.id.idVersionCarga = 34657";	
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (QueryException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<EstiloVehiculoDTO> listarFiltrado(EstiloVehiculoDTO estiloVehiculoDTO){
		try {
			String queryString = "select model from EstiloVehiculoDTO AS model ";
			String sWhere = "";
			Query query;
			@SuppressWarnings("rawtypes")
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (estiloVehiculoDTO == null)
				return null;
			if(estiloVehiculoDTO.getTipoSubMarcaVacio()!=null && estiloVehiculoDTO.getTipoSubMarcaVacio()){
				sWhere="model.subMarcaVehiculoDTO is null ";
			}
			if(estiloVehiculoDTO.getId() != null){
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "id.claveTipoBien", estiloVehiculoDTO.getId().getClaveTipoBien());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "id.claveEstilo", estiloVehiculoDTO.getId().getClaveEstilo());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idVersionCarga", estiloVehiculoDTO.getId().getIdVersionCarga());
			}
			
			/*if(estiloVehiculoDTO.getTipoVehiculoDTO().getId() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoVehiculoDTO.id.idTcTipoVehiculo", estiloVehiculoDTO.getTipoVehiculoDTO().getId().getIdTcTipoVehiculo());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoVehiculoDTO.id.claveTipoBien", estiloVehiculoDTO.getTipoVehiculoDTO().getId().getClaveTipoBien());
			}*/
			if(estiloVehiculoDTO.getMarcaVehiculoDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "marcaVehiculoDTO.idTcMarcaVehiculo", estiloVehiculoDTO.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
			}
			if(estiloVehiculoDTO.getTipoBienAutosDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoBienAutosDTO.claveTipoBien", estiloVehiculoDTO.getTipoBienAutosDTO().getClaveTipoBien());
			}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcTipoVehiculo", estiloVehiculoDTO.getIdTcTipoVehiculo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "descripcionEstilo", estiloVehiculoDTO.getDescripcionEstilo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "numeroAsientos", estiloVehiculoDTO.getNumeroAsientos());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "numeroPuertas", estiloVehiculoDTO.getNumeroPuertas());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "capacidadToneladas", estiloVehiculoDTO.getCapacidadToneladas());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveAlarmaFabricante", estiloVehiculoDTO.getClaveAlarmaFabricante());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveUnidadMedida", estiloVehiculoDTO.getClaveUnidadMedida());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveCilindros", estiloVehiculoDTO.getClaveCilindros());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveTipoInyeccion", estiloVehiculoDTO.getClaveTipoInyeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveFrenos", estiloVehiculoDTO.getClaveFrenos());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveAireAcondicionado", estiloVehiculoDTO.getClaveAireAcondicionado());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveSistemaElectrico", estiloVehiculoDTO.getClaveSistemaElectrico());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveVestidura", estiloVehiculoDTO.getClaveVestidura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveSonido", estiloVehiculoDTO.getClaveSonido());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveQuemaCocos", estiloVehiculoDTO.getClaveQuemaCocos());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveBolsasAire", estiloVehiculoDTO.getClaveBolsasAire());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveTransmision", estiloVehiculoDTO.getClaveTransmision());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveCatalogoFabricante", estiloVehiculoDTO.getClaveCatalogoFabricante());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveVersion", estiloVehiculoDTO.getClaveVersion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveTipoCombustible", estiloVehiculoDTO.getClaveTipoCombustible());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			queryString += " ORDER BY model.descripcionEstilo ";
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			List<EstiloVehiculoDTO> respuesta = (List<EstiloVehiculoDTO>)query.getResultList();
			int index=0;
			for(EstiloVehiculoDTO item : respuesta){
				TipoVehiculoDTO tipoVehiculoDTO = tipoVehiculoFacadeRemote.findById(item.getIdTcTipoVehiculo());
				MarcaVehiculoDTO marcaVehiculoDTO= this.marcaVehiculoFacadeRemote.findById(item.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
				respuesta.get(index).setTipoVehiculoDTO(tipoVehiculoDTO);
				respuesta.get(index).setMarcaVehiculoDTO(marcaVehiculoDTO);
				/*respuesta.get(index).setClaveEstiloDesc(item.getId().getClaveEstilo());
				if(marcaVehiculoDTO!=null && !StringUtil.isEmpty(marcaVehiculoDTO.getDescripcionMarcaVehiculo())){
					respuesta.get(index).setMarcaDesc(marcaVehiculoDTO.getDescripcionMarcaVehiculo());
				}
				if(item.getSubMarcaVehiculoDTO()!=null && !StringUtil.isEmpty(item.getSubMarcaVehiculoDTO().getDescripcionSubMarcaVehiculo())){
					respuesta.get(index).setSubmarcaDesc(item.getSubMarcaVehiculoDTO().getDescripcionSubMarcaVehiculo());
				}*/
				index++;
			}
			return respuesta;
//			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<EstiloVehiculoDTO> listarDistintosFiltrado(EstiloVehiculoDTO estiloVehiculoDTO){
		try {
			String queryString = " select model.descripcionEstilo,model.id.claveTipoBien, " +
								 " model.id.claveEstilo, model.marcaVehiculoDTO.idTcMarcaVehiculo,  " +
								 " model.id.idVersionCarga from EstiloVehiculoDTO AS model ";
								 
			String sWhere = "";
			Query query;
			@SuppressWarnings("rawtypes")
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (estiloVehiculoDTO == null)
				return null;
			if(estiloVehiculoDTO.getId() != null){
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "id.claveTipoBien", estiloVehiculoDTO.getId().getClaveTipoBien());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "id.claveEstilo", estiloVehiculoDTO.getId().getClaveEstilo());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idVersionCarga", estiloVehiculoDTO.getId().getIdVersionCarga());
			}

			if(estiloVehiculoDTO.getMarcaVehiculoDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "marcaVehiculoDTO.idTcMarcaVehiculo", estiloVehiculoDTO.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
			}
			if(estiloVehiculoDTO.getTipoBienAutosDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoBienAutosDTO.claveTipoBien", estiloVehiculoDTO.getTipoBienAutosDTO().getClaveTipoBien());
			}

			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				queryString = queryString.concat(" and model.id.idVersionCarga = (select max(estilo.id.idVersionCarga) "+
						" from EstiloVehiculoDTO estilo where estilo.id.claveTipoBien like :tipoBienAutosDTOclaveTipoBien " +
						" and  estilo.marcaVehiculoDTO.idTcMarcaVehiculo= :marcaVehiculoDTOidTcMarcaVehiculo )");				
			}

			queryString = queryString.concat(
					 " group by model.descripcionEstilo,model.id.claveTipoBien, "+
					 " model.id.claveEstilo,model.marcaVehiculoDTO.idTcMarcaVehiculo, "+
					 " model.id.idVersionCarga order by model.descripcionEstilo ");
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			Object result  = query.getResultList();
			List<EstiloVehiculoDTO> estilos = new ArrayList<EstiloVehiculoDTO>();
			if(result instanceof List)  {
				List<Object> listaResultados = (List<Object>) result;
				String claveTipoBien=null, claveEstilo= null, descripcion = null;
				BigDecimal marca = null, idVersionCarga= null;
				EstiloVehiculoDTO estilo = null;
				EstiloVehiculoId id = null;
				MarcaVehiculoDTO marcaV = null;
				for(Object object : listaResultados){
					id = new EstiloVehiculoId();
					estilo = new EstiloVehiculoDTO();
					marcaV = new MarcaVehiculoDTO();
					
					Object[] singleResult = (Object[]) object;
					descripcion = (String)singleResult[0];
					claveTipoBien = (String)singleResult[1];
					claveEstilo = (String)singleResult[2];

					if(singleResult[3] instanceof BigDecimal)
						marca = (BigDecimal)singleResult[3];
					else if (singleResult[3] instanceof Long)
						marca = BigDecimal.valueOf((Long)singleResult[3]);
					else if (singleResult[3] instanceof Double)
						marca = BigDecimal.valueOf((Double)singleResult[3]);

					if(singleResult[4] instanceof BigDecimal)
						idVersionCarga = (BigDecimal)singleResult[4];
					else if (singleResult[4] instanceof Long)
						idVersionCarga = BigDecimal.valueOf((Long)singleResult[4]);
					else if (singleResult[4] instanceof Double)
						idVersionCarga = BigDecimal.valueOf((Double)singleResult[4]);					
					
					id.setClaveEstilo(claveEstilo);
					id.setClaveTipoBien(claveTipoBien);
					id.setIdVersionCarga(idVersionCarga);
					
					estilo.setId(id);
					estilo.setDescripcionEstilo(descripcion);
					marcaV.setIdTcMarcaVehiculo(marca);
					estilo.setMarcaVehiculoDTO(marcaV);
					estilos.add(estilo);
				}
			}
			return estilos;
		} catch (RuntimeException re) {
			throw re;
		}
	}	

	@SuppressWarnings("unchecked")
	public List<EstiloVehiculoDTO> listarDistintosFiltrado(String cveTipoBien,Integer modelo, String descripcionVehiculo, BigDecimal idToNegSeccion){
		try {
			StringBuffer queryString = new StringBuffer(); 
			queryString.append("SELECT   ev.descripcionestilo, ev.clavetipobien, ev.claveestilo, ev.idtcmarcavehiculo, ev.idversioncarga ");
			if(idToNegSeccion != null){
				queryString.append(" FROM MIDAS.tonegestilovehiculo  nev, MIDAS.tcestilovehiculo ev INNER JOIN MIDAS.tcmodelovehiculo modelo ");
			}else{
				queryString.append(" FROM MIDAS.tcestilovehiculo ev INNER JOIN MIDAS.tcmodelovehiculo modelo ");
			}
			queryString.append(" ON ev.clavetipobien = modelo.clavetipobien ");
			queryString.append(" AND ev.claveestilo = modelo.claveestilo ");
			queryString.append(" AND ev.idversioncarga = modelo.idversioncarga ");
			queryString.append(" WHERE UPPER (ev.descripcionestilo) LIKE '%"+descripcionVehiculo.toUpperCase()+"%' ");
			if(idToNegSeccion != null){
				queryString.append(" AND nev.IdToNegSeccion = " + idToNegSeccion + " ");
				queryString.append(" AND nev.claveTipoBien = ev.claveTipobien ");
				queryString.append(" AND nev.claveestilo = ev.claveestilo ");
				queryString.append(" AND nev.idversioncarga = ev.idversioncarga ");
			}
			queryString.append(" AND ev.idversioncarga = (SELECT MAX (ev2.idversioncarga)" );
			queryString.append(" FROM MIDAS.tcestilovehiculo ev2, MIDAS.tocargavaloresamis va ");
			queryString.append(" WHERE UPPER (ev2.descripcionestilo) LIKE '%"+descripcionVehiculo.toUpperCase()+"%' ");
			queryString.append(" AND va.idversioncarga = ev2.idversioncarga ");
			if(cveTipoBien != null){
				queryString.append(" AND UPPER (ev2.clavetipobien) = '" + cveTipoBien.toUpperCase() + "' ");
			}
			queryString.append(" AND va.claveestatus = 1)");
			if(modelo!=null){
				queryString.append(" AND modelo.modelovehiculo = "+modelo+" ");
			}
			if(cveTipoBien != null){
				queryString.append(" AND UPPER (ev.clavetipobien) = '" + cveTipoBien.toUpperCase() + "' ");
			}
			//queryString.append(" AND rownum < 30");
			queryString.append(" GROUP BY ev.descripcionestilo,ev.clavetipobien, ev.claveestilo,ev.idtcmarcavehiculo,ev.idversioncarga ");
								 
			Query query;
			LogDeMidasEJB3.log(this.getClass().getName()+".listarDistintosFiltrado  query =>"   + queryString.toString(),
					Level.FINE, null);
			
			query = entityManager.createNativeQuery(queryString.toString());
//			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			Object result  = query.getResultList();
			List<EstiloVehiculoDTO> estilos = new ArrayList<EstiloVehiculoDTO>();
			if(result instanceof List)  {
				List<Object> listaResultados = (List<Object>) result;
				String claveTipoBien=null, claveEstilo= null, descripcion = null;
				BigDecimal marca = null, idVersionCarga= null;
				EstiloVehiculoDTO estilo = null;
				EstiloVehiculoId id = null;
				MarcaVehiculoDTO marcaV = null;
				for(Object object : listaResultados){
					id = new EstiloVehiculoId();
					estilo = new EstiloVehiculoDTO();
					marcaV = new MarcaVehiculoDTO();
					
					Object[] singleResult = (Object[]) object;
					descripcion = (String)singleResult[0];
					claveTipoBien = (String)singleResult[1];
					claveEstilo = (String)singleResult[2];

					if(singleResult[3] instanceof BigDecimal)
						marca = (BigDecimal)singleResult[3];
					else if (singleResult[3] instanceof Long)
						marca = BigDecimal.valueOf((Long)singleResult[3]);
					else if (singleResult[3] instanceof Double)
						marca = BigDecimal.valueOf((Double)singleResult[3]);

					if(singleResult[4] instanceof BigDecimal)
						idVersionCarga = (BigDecimal)singleResult[4];
					else if (singleResult[4] instanceof Long)
						idVersionCarga = BigDecimal.valueOf((Long)singleResult[4]);
					else if (singleResult[4] instanceof Double)
						idVersionCarga = BigDecimal.valueOf((Double)singleResult[4]);					
					
					id.setClaveEstilo(claveEstilo);
					id.setClaveTipoBien(claveTipoBien);
					id.setIdVersionCarga(idVersionCarga);
					
					estilo.setId(id);
					estilo.setDescripcionEstilo(descripcion);
					marcaV.setIdTcMarcaVehiculo(marca);
					estilo.setMarcaVehiculoDTO(marcaV);
					estilos.add(estilo);
				}
			}
			if(estilos.size() == 0 && idToNegSeccion != null){
				estilos = listarDistintosFiltrado(cveTipoBien, modelo, descripcionVehiculo, null);
			}
			return estilos;
		} catch (RuntimeException re) {
			throw re;
		}
	}
	/**
	 * Obtiene los estilos asociados a una seccion
	 * con una marca seleciconada, si la marca es null
	 * retorna todos los estilos asociados sin importar la marca
	 * @author martin
	 */
	public List<EstiloVehiculoDTO> listarDistintosFiltradoVersionAgrupador(final EstiloVehiculoDTO estiloVehiculoDTO, NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion, BigDecimal idToNegSeccion ){
		try {
			TarifaAgrupadorTarifa tarifaAgrupadorTarifa = tarifaAgrupadorTarifaService.getPorNegocioAgrupadorTarifaSeccion(negocioAgrupadorTarifaSeccion);
			Map<String,Object> params = new HashMap<String, Object>();
			if (idToNegSeccion != null) {
				params.put("negocioSeccion.idToNegSeccion", idToNegSeccion);
			}
			if (estiloVehiculoDTO.getTipoBienAutosDTO() != null){
				params.put("estiloVehiculoDTO.tipoBienAutosDTO.claveTipoBien", estiloVehiculoDTO.getTipoBienAutosDTO().getClaveTipoBien());
			}
			if (tarifaAgrupadorTarifa!= null && tarifaAgrupadorTarifa.getId() != null) {
				params.put("estiloVehiculoDTO.id.idVersionCarga", tarifaAgrupadorTarifa.getId().getIdVertarifa());
			}
			
			List<NegocioEstiloVehiculo> result = entidadService.findByProperties(NegocioEstiloVehiculo.class, params);
			
			List<EstiloVehiculoDTO> estilosAsociados = new ArrayList<EstiloVehiculoDTO>();
			
			String claveEstiloVehiculo = null;
			if(result != null){
				for (NegocioEstiloVehiculo item : result) {
					claveEstiloVehiculo = item.getEstiloVehiculoDTO().getId().getClaveEstilo() +" - ";
					if (item.getEstiloVehiculoDTO().getDescripcionEstilo().indexOf(claveEstiloVehiculo) >= 0){
						item.getEstiloVehiculoDTO().setDescripcionEstilo(item.getEstiloVehiculoDTO().getDescripcionEstilo());
					}else{
						item.getEstiloVehiculoDTO().setDescripcionEstilo(claveEstiloVehiculo + item.getEstiloVehiculoDTO().getDescripcionEstilo());
					}
					estilosAsociados.add(item.getEstiloVehiculoDTO());
				}
			}
			// Busca todos los estilos
			if (estilosAsociados.size() == 0) {
				List<SeccionTipoVehiculoDTO> seccionTipoVehiculoList = null;
					if (idToNegSeccion != null) {
						NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idToNegSeccion);
						try{
						seccionTipoVehiculoList = entidadService.findByProperty(SeccionTipoVehiculoDTO.class, "id.idToSeccion", negocioSeccion.getSeccionDTO().getIdToSeccion());
						}catch(Exception e){}
						if(seccionTipoVehiculoList == null || seccionTipoVehiculoList.isEmpty()){
							seccionTipoVehiculoList = new ArrayList<SeccionTipoVehiculoDTO>(1);
							SeccionTipoVehiculoDTO seccionTipoVehiculo = new SeccionTipoVehiculoDTO();
							SeccionTipoVehiculoId id = new SeccionTipoVehiculoId();
							id.setIdToSeccion(negocioSeccion.getSeccionDTO().getIdToSeccion());
							id.setIdTcTipoVehiculo(negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());
							seccionTipoVehiculo.setId(id);
							seccionTipoVehiculoList.add(seccionTipoVehiculo);
						}
					}
				return listarDistintosFiltradoVersionAgrupadorTodo(estiloVehiculoDTO, negocioAgrupadorTarifaSeccion, seccionTipoVehiculoList);
			}
			
			// Si no tiene marca retorna los estilos asociados
			if(estiloVehiculoDTO.getMarcaVehiculoDTO() != null && estiloVehiculoDTO.getMarcaVehiculoDTO().getIdTcMarcaVehiculo() == null){
				return estilosAsociados;
			}
			
			// Saca solo los estilos Asociados al negocio por la marca seleccionada
			List<EstiloVehiculoDTO> dtos = new ArrayList<EstiloVehiculoDTO>();
			int size = estilosAsociados.size();
			EstiloVehiculoDTO dto = null;
			for(int x = 0; x < size; x++){
				dto = new EstiloVehiculoDTO(); 
				dto = estilosNegocioPorMarca(estilosAsociados, estiloVehiculoDTO.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
				if (dto != null) {
					claveEstiloVehiculo = dto.getId().getClaveEstilo() + " - ";
					if (dto.getDescripcionEstilo().indexOf(claveEstiloVehiculo) >= 0){
						dto.setDescripcionEstilo(dto.getDescripcionEstilo());
					}else{
						dto.setDescripcionEstilo(claveEstiloVehiculo + dto.getDescripcionEstilo());
					}
					dtos.add(dto);
					estilosAsociados.remove(dto);
				}
			}
			return dtos;
		} catch (RuntimeException re) {
			throw re;
		}
	}

	
	/**
	 * Obtiene los estilos asociados al negocio
	 * por la marca seleccionada
	 * @param estilosAsociados
	 * @param marca
	 * @return
	 * @autor martin
	 */
	private EstiloVehiculoDTO estilosNegocioPorMarca(List<EstiloVehiculoDTO> estilosAsociados, final BigDecimal marca){
		Predicate predicate = new Predicate() {
			@Override
			public boolean evaluate(Object item) {
				EstiloVehiculoDTO estilo = (EstiloVehiculoDTO)item;
				if (estilo.getMarcaVehiculoDTO().getIdTcMarcaVehiculo() != null) {
					if (estilo.getMarcaVehiculoDTO().getIdTcMarcaVehiculo().equals(marca)){
						return true;
					}
				}
				
				return false;
			}
		};
		return (EstiloVehiculoDTO)CollectionUtils.find(estilosAsociados, predicate);
	}
	
	@Override
	public List<EstiloVehiculoDTO> listarDistintosFiltradoVersionAgrupadorTodo(EstiloVehiculoDTO estiloVehiculoDTO, NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion, 
			List<SeccionTipoVehiculoDTO> seccionTipoVehiculoList ){
		try {
			String queryString = " select model.descripcionEstilo,model.id.claveTipoBien, " +
								 " model.id.claveEstilo, model.marcaVehiculoDTO.idTcMarcaVehiculo,  " +
								 " model.id.idVersionCarga, model.numeroAsientos from EstiloVehiculoDTO AS model ";
								 
			String sWhere = "";
			Query query;
			@SuppressWarnings("rawtypes")
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if(estiloVehiculoDTO.getModeloVehiculo() != null){
				queryString = " select model.descripcionEstilo,model.id.claveTipoBien, " +
				 " model.id.claveEstilo, model.marcaVehiculoDTO.idTcMarcaVehiculo,  " +
				 " model.id.idVersionCarga, model.numeroAsientos from EstiloVehiculoDTO AS model, ModeloVehiculoDTO AS model2 ";
			}
			
			if (estiloVehiculoDTO == null)
				return null;
			if(estiloVehiculoDTO.getId() != null){
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "id.claveTipoBien", estiloVehiculoDTO.getId().getClaveTipoBien());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "id.claveEstilo", estiloVehiculoDTO.getId().getClaveEstilo());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idVersionCarga", estiloVehiculoDTO.getId().getIdVersionCarga());
			}

			if(estiloVehiculoDTO.getMarcaVehiculoDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "marcaVehiculoDTO.idTcMarcaVehiculo", estiloVehiculoDTO.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
			}
			if(estiloVehiculoDTO.getTipoBienAutosDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoBienAutosDTO.claveTipoBien", estiloVehiculoDTO.getTipoBienAutosDTO().getClaveTipoBien());
			}
			
			if(estiloVehiculoDTO.getDescripcionEstilo() != null && !estiloVehiculoDTO.getDescripcionEstilo().equals("")){
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionEstilo", estiloVehiculoDTO.getDescripcionEstilo());
			}
			/*
			if(estiloVehiculoDTO.getIdTcTipoVehiculo() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcTipoVehiculo", estiloVehiculoDTO.getIdTcTipoVehiculo());
			}
			*/
			

			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				
				if (negocioAgrupadorTarifaSeccion != null) {
					
					TarifaAgrupadorTarifa tarifaAgrupadorTarifa = tarifaAgrupadorTarifaService.getPorNegocioAgrupadorTarifaSeccion(negocioAgrupadorTarifaSeccion);			
					
					if(tarifaAgrupadorTarifa != null){
						queryString = queryString.concat(" and model.id.idVersionCarga = " + new BigDecimal(tarifaAgrupadorTarifa.getId().getIdVertarifa()));
					}else{
						queryString = queryString.concat(" and model.id.idVersionCarga is null ");
					}
				}
				
			}
			
			if(seccionTipoVehiculoList != null && !seccionTipoVehiculoList.isEmpty()){
				queryString = queryString.concat(obtenerQueryTipoVehiculo(seccionTipoVehiculoList));
			}
			
			if(estiloVehiculoDTO.getModeloVehiculo() != null){
				queryString = queryString.concat(" and model.id.claveTipoBien = model2.id.claveTipoBien " +
				 " and model.id.claveEstilo = model2.id.claveEstilo " +
				 " and model.id.idVersionCarga = model2.id.idVersionCarga " +
				 " and model2.id.modeloVehiculo = " + estiloVehiculoDTO.getModeloVehiculo());
				
				if (negocioAgrupadorTarifaSeccion != null) {
					queryString = queryString.concat(" and model2.id.idMoneda = " + negocioAgrupadorTarifaSeccion.getIdMoneda());
				}
			}

			queryString = queryString.concat(
					 " group by model.descripcionEstilo,model.id.claveTipoBien, "+
					 " model.id.claveEstilo,model.marcaVehiculoDTO.idTcMarcaVehiculo, "+
					 " model.id.idVersionCarga, model.numeroAsientos order by model.descripcionEstilo ");
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			Object result  = query.getResultList();
			List<EstiloVehiculoDTO> estilos = new ArrayList<EstiloVehiculoDTO>();
			if(result instanceof List)  {
				List<Object> listaResultados = (List<Object>) result;
				String claveTipoBien=null, claveEstilo= null, descripcion = null;
				BigDecimal marca = null, idVersionCarga= null;
				Short numeroAsientos = null;
				EstiloVehiculoDTO estilo = null;
				EstiloVehiculoId id = null;
				MarcaVehiculoDTO marcaV = null;
				for(Object object : listaResultados){
					id = new EstiloVehiculoId();
					estilo = new EstiloVehiculoDTO();
					marcaV = new MarcaVehiculoDTO();
					
					Object[] singleResult = (Object[]) object;
					descripcion = (String)singleResult[0];
					claveTipoBien = (String)singleResult[1];
					claveEstilo = (String)singleResult[2];

					if(singleResult[3] instanceof BigDecimal)
						marca = (BigDecimal)singleResult[3];
					else if (singleResult[3] instanceof Long)
						marca = BigDecimal.valueOf((Long)singleResult[3]);
					else if (singleResult[3] instanceof Double)
						marca = BigDecimal.valueOf((Double)singleResult[3]);

					if(singleResult[4] instanceof BigDecimal)
						idVersionCarga = (BigDecimal)singleResult[4];
					else if (singleResult[4] instanceof Long)
						idVersionCarga = BigDecimal.valueOf((Long)singleResult[4]);
					else if (singleResult[4] instanceof Double)
						idVersionCarga = BigDecimal.valueOf((Double)singleResult[4]);
					
					if(singleResult[5] instanceof Short)
						numeroAsientos = (Short)singleResult[5];
					else if (singleResult[5] instanceof BigDecimal)
						numeroAsientos = (Short)singleResult[5];
					else if (singleResult[5] instanceof Long)
						numeroAsientos =(Short)singleResult[5];
					else if (singleResult[5] instanceof Double)
						numeroAsientos = (Short)singleResult[5];	
					
					id.setClaveEstilo(claveEstilo);
					id.setClaveTipoBien(claveTipoBien);
					id.setIdVersionCarga(idVersionCarga);
					
					estilo.setId(id);
					if (descripcion.indexOf(claveEstilo + " - ") >= 0){
						estilo.setDescripcionEstilo(descripcion);
					}else{
						estilo.setDescripcionEstilo(claveEstilo + " - " + descripcion);
					}
					marcaV.setIdTcMarcaVehiculo(marca);
					estilo.setMarcaVehiculoDTO(marcaV);
					estilo.setNumeroAsientos(numeroAsientos);
					estilos.add(estilo);
				}
			}
			return estilos;
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public String obtenerQueryTipoVehiculo(
			List<SeccionTipoVehiculoDTO> seccionTipoVehiculoList) {
		StringBuilder queryTipoVehiculo = new StringBuilder(" and model.idTcTipoVehiculo IN (");
		boolean isFirst = true;
		for(SeccionTipoVehiculoDTO item : seccionTipoVehiculoList){
			if(!isFirst){
				queryTipoVehiculo.append(", ");
			}else{
				isFirst = false;
			}
			queryTipoVehiculo.append(item.getId().getIdTcTipoVehiculo());
		}
		queryTipoVehiculo.append(") ");		
		return queryTipoVehiculo.toString();
	}

	@SuppressWarnings("unchecked")
	public List<VersionCargaComboDTO> listarVersionCarga(EstiloVehiculoDTO estiloVehiculoDTO){
		try {
			String queryString = "select distinct model.id.idVersionCarga from EstiloVehiculoDTO AS model ";
			String sWhere = "";
			Query query;
			@SuppressWarnings("rawtypes")
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (estiloVehiculoDTO == null)
				return null;
			if(estiloVehiculoDTO.getMarcaVehiculoDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "marcaVehiculoDTO.idTcMarcaVehiculo", estiloVehiculoDTO.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
			}
			if(estiloVehiculoDTO.getTipoBienAutosDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoBienAutosDTO.claveTipoBien", estiloVehiculoDTO.getId().getClaveTipoBien());
			}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcTipoVehiculo", estiloVehiculoDTO.getIdTcTipoVehiculo());			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			queryString = queryString.concat(" order by model.id.idVersionCarga desc ");
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			List<BigDecimal> respuesta = new ArrayList<BigDecimal>();
			respuesta = (List<BigDecimal>)query.getResultList();
			List<VersionCargaComboDTO> versionCargaComboDTO = new ArrayList<VersionCargaComboDTO>();
			for(BigDecimal item : respuesta){
				versionCargaComboDTO.add(new VersionCargaComboDTO(item));
			}
			return versionCargaComboDTO;
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public VersionCargaComboDTO getVersionCarga(EstiloVehiculoDTO estiloVehiculoDTO){
		try {
			String queryString = "select distinct model.id.idVersionCarga from EstiloVehiculoDTO AS model ";
			String sWhere = "";
			Query query;
			@SuppressWarnings("rawtypes")
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (estiloVehiculoDTO == null)
				return null;
			if(estiloVehiculoDTO.getMarcaVehiculoDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "marcaVehiculoDTO.idTcMarcaVehiculo", estiloVehiculoDTO.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
			}
			if(estiloVehiculoDTO.getTipoBienAutosDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoBienAutosDTO.claveTipoBien", estiloVehiculoDTO.getId().getClaveTipoBien());
			}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcTipoVehiculo", estiloVehiculoDTO.getIdTcTipoVehiculo());			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idVersionCarga", estiloVehiculoDTO.getId().getIdVersionCarga());			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			List<BigDecimal> respuesta = new ArrayList<BigDecimal>();
			respuesta = (List<BigDecimal>)query.getResultList();
			VersionCargaComboDTO versionCargaComboDTO = new VersionCargaComboDTO();
			for(BigDecimal item : respuesta){
				versionCargaComboDTO = new VersionCargaComboDTO(item);
				break
				;
			}
			return versionCargaComboDTO;
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	
	public List<EstiloVehiculoDTO> getListEstiloVehiculoDTO(BigDecimal idToControlArchivo) throws IOException{
		ControlArchivoDTO controlArchivo = entityManager.find(ControlArchivoDTO.class, idToControlArchivo);
		String OSName = System.getProperty("os.name");
		String uploadFolder = null;
		if (OSName.toLowerCase().indexOf("windows")!=-1) {
			uploadFolder = SistemaPersistencia.UPLOAD_FOLDER;
		} else {
			uploadFolder = SistemaPersistencia.LINUX_UPLOAD_FOLDER;
		}
		String urlArchivo = uploadFolder+controlArchivo.obtenerNombreArchivoFisico();
		InputStream archivo = null;
		List<EstiloVehiculoDTO> list = null;
				
				try {
					archivo = new FileInputStream(urlArchivo);
					ExcelConverter converter = new ExcelConverter(archivo, new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());
					
				for (String sheetName : converter.getSheetNames()) {

					list = converter.parseWithRowMapper(sheetName,
							new ExcelRowMapper<EstiloVehiculoDTO>() {

								@Override
								public EstiloVehiculoDTO mapRow(Map<String, String> row,
										int rowNum) {
									EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
									estiloVehiculoDTO = poblarEstiloVehiculo( row );
									return estiloVehiculoDTO;
								}
							});
				}

			} catch (FileNotFoundException e) {
				LogUtil.log("getListEstiloVehiculoDTO failed ", Level.SEVERE, e);
			} finally {
				if (archivo != null) {
					try {
						archivo.close();
					} catch (IOException e) {
						throw e;
					}
				}
			}
			
			return list;
	}
	
	private EstiloVehiculoDTO poblarEstiloVehiculo(Map<String, String> row) {

		EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
		EstiloVehiculoId id = new EstiloVehiculoId();
		id.setClaveEstilo(row.get("clave_estilo"));
		id.setClaveTipoBien(row.get("clave_tipo_bien"));
		id.setIdVersionCarga(BigDecimal.valueOf(Integer.valueOf(row.get("version_carga"))));
		estiloVehiculoDTO.setId(id);
		estiloVehiculoDTO = entityManager.find(EstiloVehiculoDTO.class, id); 
		SubMarcaVehiculoDTO subMarcaVehiculoDTO= new SubMarcaVehiculoDTO();
		subMarcaVehiculoDTO.setMarcaVehiculoDTO(estiloVehiculoDTO.getMarcaVehiculoDTO());
		subMarcaVehiculoDTO.setDescripcionSubMarcaVehiculo(row.get("tipo"));
		List<SubMarcaVehiculoDTO> lista=this.subMarcaVehiculoFacadeRemote.listarFiltrado(subMarcaVehiculoDTO);
		if(lista!=null && !lista.isEmpty()){
			subMarcaVehiculoDTO=lista.get(0);
			estiloVehiculoDTO.setSubMarcaVehiculoDTO(subMarcaVehiculoDTO );
		}		
		return estiloVehiculoDTO;
	}
	public String actualizarMasivo(String idToControlArchivo){
		List<EstiloVehiculoDTO> list;
		try {
			list = getListEstiloVehiculoDTO(BigDecimal.valueOf(Integer.valueOf(idToControlArchivo)));
			if(CollectionUtils.isNotEmpty(list)){
				for(EstiloVehiculoDTO item :list){
					EstiloVehiculoDTO modelo = entityManager.find(EstiloVehiculoDTO.class, item.getId());					
		            modelo.setSubMarcaVehiculoDTO(item.getSubMarcaVehiculoDTO());
					entityManager.merge(modelo);	
					entityManager.flush();
				}
				return "Carga Exitosa";
			}
		} catch (NumberFormatException e) {
			LogUtil.log("actualizarMasivo failed", Level.SEVERE, e);
		} catch (IOException e) {
			LogUtil.log("actualizarMasivo failed", Level.SEVERE, e);
		}
		return "No se proceso el archivo, o este esta vacio";
	}
	
	public EstiloVehiculoDTO findById(BigDecimal id) {
		return null;
	}

	public EstiloVehiculoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public EstiloVehiculoDTO findById(double id) {
		return null;
	}

	public List<EstiloVehiculoDTO> listRelated(Object id) {
		return null;
	}

	@Override
	public List<VarModifDescripcion> findByFiltersModifDescripcion(
			VarModifDescripcion arg0) {
		return varModifDescripcionDao.findByFilters(arg0);
	}
}