package mx.com.afirme.midas.catalogos.estilovehiculo.grupo;
// default package

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity EstiloVehiculoGruposDTO.
 * @see .EstiloVehiculoGruposDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class EstiloVehiculoGrupoFacade  implements EstiloVehiculoGrupoFacadeRemote {

    @PersistenceContext private EntityManager entityManager;

    @EJB
    private EstiloVehiculoFacadeRemote estiloVehiculoFacade;
		/**
	 Perform an initial save of a previously unsaved EstiloVehiculoGruposDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EstiloVehiculoGruposDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EstiloVehiculoGrupoDTO entity) {
    				LogUtil.log("saving EstiloVehiculoGrupoDTO instance", Level.FINE, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.FINE, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent EstiloVehiculoGrupoDTO entity.
	  @param entity EstiloVehiculoGrupoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EstiloVehiculoGrupoDTO entity) {
    				LogUtil.log("deleting EstiloVehiculoGrupoDTO instance", Level.FINE, null);
	        try {
        	entity = entityManager.getReference(EstiloVehiculoGrupoDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.FINE, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved EstiloVehiculoGrupoDTO entity and return it or a copy of it to the sender. 
	 A copy of the EstiloVehiculoGrupoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EstiloVehiculoGrupoDTO entity to update
	 @return EstiloVehiculoGrupoDTO the persisted EstiloVehiculoGrupoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public EstiloVehiculoGrupoDTO update(EstiloVehiculoGrupoDTO entity) {
    				LogUtil.log("updating EstiloVehiculoGrupoDTO instance", Level.FINE, null);
	        try {
            EstiloVehiculoGrupoDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.FINE, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public EstiloVehiculoGrupoDTO findById( EstiloVehiculoGrupoId id) {
    				LogUtil.log("finding EstiloVehiculoGrupoDTO instance with id: " + id, Level.FINE, null);
	        try {
            EstiloVehiculoGrupoDTO instance = entityManager.find(EstiloVehiculoGrupoDTO.class, id);
            if(instance != null){
            	EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(
            			instance.getId().getClaveTipoBien(),
            			instance.getId().getClaveEstilo(),
            			instance.getId().getIdVersionCarga());
            	instance.setEstiloVehiculoDTO(estiloVehiculoFacade.findById(estiloVehiculoId));
            }
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all EstiloVehiculoGrupoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EstiloVehiculoGrupoDTO property to query
	  @param value the property value to match
	  	  @return List<EstiloVehiculoGrupoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<EstiloVehiculoGrupoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding EstiloVehiculoGrupoDTO instance with property: " + propertyName + ", value: " + value, Level.FINE, null);
			try {
			final String queryString = "select model from EstiloVehiculoGrupoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all EstiloVehiculoGrupoDTO entities.
	  	  @return List<EstiloVehiculoGrupoDTO> all EstiloVehiculoGrupoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<EstiloVehiculoGrupoDTO> findAll(
		) {
					LogUtil.log("finding all EstiloVehiculoGrupoDTO instances", Level.FINE, null);
			try {
			final String queryString = "select model from EstiloVehiculoGrupoDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<EstiloVehiculoGrupoDTO> listarFiltrado(EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO){
		try {
			String queryString = "select model from EstiloVehiculoGrupoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (estiloVehiculoGrupoDTO == null)
				return null;
			if(estiloVehiculoGrupoDTO.getId() != null){
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "id.claveTipoBien", estiloVehiculoGrupoDTO.getId().getClaveTipoBien());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "id.claveEstilo", estiloVehiculoGrupoDTO.getId().getClaveEstilo());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idVersionCarga", estiloVehiculoGrupoDTO.getId().getIdVersionCarga());
			}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicioVigencia", estiloVehiculoGrupoDTO.getFechaInicioVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaFinVigencia", estiloVehiculoGrupoDTO.getFechaFinVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idGrupoDM", estiloVehiculoGrupoDTO.getIdGrupoDM());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idGrupoRT", estiloVehiculoGrupoDTO.getIdGrupoRT());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idGrupoRC", estiloVehiculoGrupoDTO.getIdGrupoRC());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "claveAMIS", estiloVehiculoGrupoDTO.getClaveAMIS());
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
}