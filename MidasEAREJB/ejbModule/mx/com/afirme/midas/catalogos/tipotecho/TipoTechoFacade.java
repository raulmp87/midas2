package mx.com.afirme.midas.catalogos.tipotecho;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity Tctipotecho.
 * 
 * @see mx.com.afirme.midas.catalogos.tipotecho.TipoTechoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoTechoFacade implements TipoTechoFacadeRemote {
	// property constants
	public static final String DESCRIPCIONTIPOTECHO = "descripciontipotecho";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved Tctipotecho entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            Tctipotecho entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoTechoDTO entity) {
		LogDeMidasEJB3.log("saving TipoTechoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent Tctipotecho entity.
	 * 
	 * @param entity
	 *            Tctipotecho entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoTechoDTO entity) {
		LogDeMidasEJB3.log("deleting TipoTechoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(TipoTechoDTO.class, entity
					.getIdTipoTecho());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved Tctipotecho entity and return it or a copy of
	 * it to the sender. A copy of the Tctipotecho entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            Tctipotecho entity to update
	 * @return Tctipotecho the persisted Tctipotecho entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoTechoDTO update(TipoTechoDTO entity) {
		LogDeMidasEJB3.log("updating TipoTechoDTO instance", Level.INFO, null);
		try {
			TipoTechoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoTechoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding TipoTechoDTO instance with id: " + id, Level.INFO,
				null);
		try {
			TipoTechoDTO instance = entityManager.find(TipoTechoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Tctipotecho entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Tctipotecho property to query
	 * @param value
	 *            the property value to match
	 * @return List<Tctipotecho> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoTechoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TipoTechoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoTechoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<TipoTechoDTO> findByDescripciontipotecho(
			Object descripciontipotecho) {
		return findByProperty(DESCRIPCIONTIPOTECHO, descripciontipotecho);
	}

	/**
	 * Find all Tctipotecho entities.
	 * 
	 * @return List<Tctipotecho> all Tctipotecho entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoTechoDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoTechoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from TipoTechoDTO model " +
					"order by model.descripcionTipoTecho";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TipoTechoDTO> listarFiltrado(TipoTechoDTO tipoTechoDTO){
		try {
			String queryString = "select model from TipoTechoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (tipoTechoDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoTipoTecho", tipoTechoDTO.getCodigoTipoTecho());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "descripcionTipoTecho", tipoTechoDTO.getCodigoTipoTecho());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoTechoDTO findById(CatalogoValorFijoId arg0) {
		return null;
	}

	public TipoTechoDTO findById(double arg0) {
		return null;
	}

	public List<TipoTechoDTO> listRelated(Object id) {
		return findAll();
	}
}