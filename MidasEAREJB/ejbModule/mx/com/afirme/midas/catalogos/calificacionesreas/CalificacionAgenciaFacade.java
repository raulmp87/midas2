package mx.com.afirme.midas.catalogos.calificacionesreas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
 
@Stateless

public class CalificacionAgenciaFacade  implements CalificacionAgenciaFacadeRemote {
    @PersistenceContext private EntityManager entityManager;
    
    public void save(CalificacionAgenciaDTO entity) {
    				LogDeMidasEJB3.log("saving CalificacionAgenciaDTO instance", Level.INFO, null);
	        try {
            entityManager.merge(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public void delete(CalificacionAgenciaDTO entity) {
    	LogDeMidasEJB3.log("deleting CalificacionAgenciaDTO instance", Level.INFO, null);
	        try {
	        CalificacionAgenciaDTO entityD = entityManager.getReference(CalificacionAgenciaDTO.class, entity.getId());
            entityManager.remove(entityD);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public CalificacionAgenciaDTO update(CalificacionAgenciaDTO entity) {
    				LogDeMidasEJB3.log("updating CalificacionAgenciaDTO instance", Level.INFO, null);
	        try {
	        	CalificacionAgenciaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public CalificacionAgenciaDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding CalificacionAgenciaDTO instance with id: " + id, Level.INFO, null);
	        try {
            return entityManager.find(CalificacionAgenciaDTO.class, id);
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    
    @SuppressWarnings("unchecked")
    public List<CalificacionAgenciaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding CalificacionAgenciaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from CalificacionAgenciaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		} 
	}		
    
    @SuppressWarnings("unchecked")
    public List<CalificacionAgenciaDTO> findByPropertyID(BigDecimal value
        ) {
    				LogDeMidasEJB3.log("finding CalificacionAgenciaDTO instance with property: " +  ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from CalificacionAgenciaDTO model where model.id" 
			 						 + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
		
	@SuppressWarnings("unchecked")
	public List<CalificacionAgenciaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all CalificacionAgenciaDTO instances", Level.INFO, null);
			try {
				final String queryString = "select model from CalificacionAgenciaDTO model";
				Query query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<CalificacionAgenciaDTO> listarFiltrado(CalificacionAgenciaDTO calificacionAgenciaDTO) {		
		try {
			String queryString = "select model from CalificacionAgenciaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (calificacionAgenciaDTO == null){
				return new ArrayList<CalificacionAgenciaDTO>();
			}
			if (calificacionAgenciaDTO.getAgencia()!= null)		
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "agencia.idagencia", calificacionAgenciaDTO.getAgencia().getIdagencia());						
			
			if (calificacionAgenciaDTO.getCalificacion()!= null)
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "calificacion", calificacionAgenciaDTO.getCalificacion());
			
			if (calificacionAgenciaDTO.getValor()!= null)	
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "valor", calificacionAgenciaDTO.getValor());
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CalificacionAgenciaDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public CalificacionAgenciaDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<CalificacionAgenciaDTO> listRelated(Object id) {
		return this.findAll();
	}
	
	public CalificacionAgenciaDTO findById(String id) {
		LogDeMidasEJB3.log("finding CalificacionAgenciaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			return entityManager.find(CalificacionAgenciaDTO.class, id);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
}