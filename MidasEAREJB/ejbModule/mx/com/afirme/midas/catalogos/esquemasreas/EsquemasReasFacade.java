package mx.com.afirme.midas.catalogos.esquemasreas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
 
@Stateless 

public class EsquemasReasFacade  implements EsquemasReasFacadeRemote {
    @PersistenceContext private EntityManager entityManager;
    
    public void save(EsquemasDTO entity) {
    				LogDeMidasEJB3.log("saving EsquemasDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public void delete(EsquemasDTO entity) {
    				LogDeMidasEJB3.log("deleting EsquemasDTO instance", Level.INFO, null);
	        try {
	        EsquemasDTO entityD = entityManager.getReference(EsquemasDTO.class, entity.getId());
            entityManager.remove(entityD);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public int update(EsquemasDTO entity) {
    				LogDeMidasEJB3.log("updating EsquemasDTO instance", Level.INFO, null);
	        try {
	        	
	        	int updateCont;
	        	final String queryString = "update EsquemasDTO model set model.reinstalaciones = :propertyValue1 " +
	        			"where model.idContrato = :propertyValue2 " +
	        			"and model.nivel = :propertyValue3 " +
	        			"and model.ordenEntrada = :propertyValue4 " +
	        			"and model.cer = :propertyValue5";
	        	
				Query query = entityManager.createQuery(queryString);
				query.setParameter("propertyValue1", entity.getReinstalaciones());
				query.setParameter("propertyValue2", entity.getIdContrato());
				query.setParameter("propertyValue3", entity.getNivel());
				query.setParameter("propertyValue4", entity.getOrdenEntrada());
				query.setParameter("propertyValue5", entity.getCer());
				
				LogDeMidasEJB3.log(entity.toString(), Level.INFO, null);
				
				updateCont =  query.executeUpdate();
				
    			LogDeMidasEJB3.log("update successful " + updateCont + " " + query.toString(), Level.INFO, null);
				
				entityManager.flush();
				
				return updateCont;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public EsquemasDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding EsquemasDTO instance with id: " + id, Level.INFO, null);
	        try {
	        	return entityManager.find(EsquemasDTO.class, id);
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    
    @SuppressWarnings("unchecked")
    public List<EsquemasDTO> findByPropertyID(BigDecimal value
        ) {
    				LogDeMidasEJB3.log("finding EsquemasDTO instance with property: " +  ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from EsquemasDTO model where model.id" 
			 						 + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	
	@SuppressWarnings("unchecked")
	public List<EsquemasDTO> findAll(
		) {
					List<EsquemasDTO> listaDetalle = new ArrayList<EsquemasDTO>(); 
					LogDeMidasEJB3.log("finding all EsquemasDTO instances", Level.INFO, null);
					try {
						StringBuilder queryString = new StringBuilder(); 
							
						queryString.append("select distinct model.idContrato, model.anio, model.cer, model.reinstalaciones," +
								" model.nivel, model.ordenEntrada" +
								" from EsquemasDTO model " );
								
						String sWhere = "";
						Query query;
						List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
						sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "aut", 0);
						sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "cveNegocio", 2);
						
							
						if (Utilerias.esAtributoQueryValido(sWhere))
							queryString = queryString.append(" where ").append(sWhere);
						
						queryString.append(" order by model.anio, model.cer,model.nivel, model.ordenEntrada");
						
						query = entityManager.createQuery(queryString.toString());
						Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
						
						query.setHint(QueryHints.REFRESH, HintValues.TRUE);
						
						
						List<?> resultSet = query.getResultList();
						
						for(Object record : resultSet){			
							Object[] detallesReserva = (Object[])record;
							
							EsquemasDTO esquemaDTO = new EsquemasDTO();
							
							esquemaDTO.setIdContrato((String)detallesReserva[0]);
							esquemaDTO.setAnio((BigDecimal)detallesReserva[1]);					
							esquemaDTO.setCer((BigDecimal)detallesReserva[2]);
							esquemaDTO.setReinstalaciones((BigDecimal)detallesReserva[3]);
							esquemaDTO.setNivel((BigDecimal)detallesReserva[4]);
							esquemaDTO.setOrdenEntrada((BigDecimal)detallesReserva[5]);
							
							listaDetalle.add(esquemaDTO);
						}
						return listaDetalle;
					
					} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<EsquemasDTO> listarFiltrado(EsquemasDTO esquemasDTO) {
		List<EsquemasDTO> listaDetalle = new ArrayList<EsquemasDTO>(); 
		try {
			StringBuilder queryString = new StringBuilder("select distinct model.idContrato, model.anio, model.cer, model.reinstalaciones, " +
					"model.nivel, model.ordenEntrada " +
					"from EsquemasDTO model ") ;
					
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "aut", 0);
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "cveNegocio", 2);
			if (esquemasDTO != null)
			{
				if (esquemasDTO.getIdContrato() != null)	
					sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "idContrato", esquemasDTO.getIdContrato());						
				
				if (esquemasDTO.getCer() != null)
					sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "cer", esquemasDTO.getCer());
				
				if (esquemasDTO.getAnio() != null)	
					sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "anio", esquemasDTO.getAnio());
			}
				
			if (Utilerias.esAtributoQueryValido(sWhere))
			    queryString.append(" where ").append(sWhere);
			
			
			queryString.append(" order by model.anio, model.cer,model.nivel, model.ordenEntrada");
			
			query = entityManager.createQuery(queryString.toString());
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			return llenaLista(query.getResultList());
		
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			return listaDetalle;
		}
	}
	
	public List<EsquemasDTO> llenaLista(List<?> resultS)
	{
		List<EsquemasDTO> listaDetalle = new ArrayList<EsquemasDTO>(); 
		
		for(Object record : resultS){			
			Object[] detallesReserva = (Object[])record;
			
			EsquemasDTO esquemaDTO = new EsquemasDTO();
			
			esquemaDTO.setIdContrato((String)detallesReserva[0]);
			esquemaDTO.setAnio((BigDecimal)detallesReserva[1]);					
			esquemaDTO.setCer((BigDecimal)detallesReserva[2]);
			esquemaDTO.setReinstalaciones((BigDecimal)detallesReserva[3]);
			esquemaDTO.setNivel((BigDecimal)detallesReserva[4]);
			esquemaDTO.setOrdenEntrada((BigDecimal)detallesReserva[5]);
			
			listaDetalle.add(esquemaDTO);
		}
		
		return listaDetalle;
	}

	public EsquemasDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public EsquemasDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<EsquemasDTO> listRelated(Object id) {
		return this.findAll();
	}
}