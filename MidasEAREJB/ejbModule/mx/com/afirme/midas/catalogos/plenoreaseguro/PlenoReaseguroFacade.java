package mx.com.afirme.midas.catalogos.plenoreaseguro;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity PlenoReaseguroDTO.
 * @see .PlenoReaseguroDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class PlenoReaseguroFacade  implements PlenoReaseguroFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved PlenoReaseguroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PlenoReaseguroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(PlenoReaseguroDTO entity) {
    				LogDeMidasEJB3.log("saving PlenoReaseguroDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent PlenoReaseguroDTO entity.
	  @param entity PlenoReaseguroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PlenoReaseguroDTO entity) {
    				LogDeMidasEJB3.log("deleting PlenoReaseguroDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(PlenoReaseguroDTO.class, entity.getIdTcPlenoReaseguro());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved PlenoReaseguroDTO entity and return it or a copy of it to the sender. 
	 A copy of the PlenoReaseguroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PlenoReaseguroDTO entity to update
	 @return PlenoReaseguroDTO the persisted PlenoReaseguroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public PlenoReaseguroDTO update(PlenoReaseguroDTO entity) {
    				LogDeMidasEJB3.log("updating PlenoReaseguroDTO instance", Level.INFO, null);
	        try {
            PlenoReaseguroDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public PlenoReaseguroDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding PlenoReaseguroDTO instance with id: " + id, Level.INFO, null);
	        try {
            PlenoReaseguroDTO instance = entityManager.find(PlenoReaseguroDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all PlenoReaseguroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PlenoReaseguroDTO property to query
	  @param value the property value to match
	  	  @return List<PlenoReaseguroDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<PlenoReaseguroDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding PlenoReaseguroDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from PlenoReaseguroDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all PlenoReaseguroDTO entities.
	  	  @return List<PlenoReaseguroDTO> all PlenoReaseguroDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<PlenoReaseguroDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all PlenoReaseguroDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from PlenoReaseguroDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	public PlenoReaseguroDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public PlenoReaseguroDTO findById(double id) {
		return findById(new BigDecimal(id));
	}

	public List<PlenoReaseguroDTO> listRelated(Object id) {
		return findAll();
	}
	
}