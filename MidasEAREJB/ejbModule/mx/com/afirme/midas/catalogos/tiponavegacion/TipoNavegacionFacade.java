package mx.com.afirme.midas.catalogos.tiponavegacion;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity TipoNavegacionDTO.
 * 
 * @see .TipoNavegacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoNavegacionFacade implements TipoNavegacionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoNavegacionDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoNavegacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoNavegacionDTO entity) {
		LogDeMidasEJB3.log("saving TipoNavegacionDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoNavegacionDTO entity.
	 * 
	 * @param entity
	 *            TipoNavegacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoNavegacionDTO entity) {
		LogDeMidasEJB3.log("deleting TipoNavegacionDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(TipoNavegacionDTO.class, entity
					.getIdTcTipoNavegacion());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoNavegacionDTO entity and return it or a copy
	 * of it to the sender. A copy of the TipoNavegacionDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoNavegacionDTO entity to update
	 * @return TipoNavegacionDTO the persisted TipoNavegacionDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoNavegacionDTO update(TipoNavegacionDTO entity) {
		LogDeMidasEJB3.log("updating TipoNavegacionDTO instance", Level.INFO, null);
		try {
			TipoNavegacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoNavegacionDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding TipoNavegacionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoNavegacionDTO instance = entityManager.find(TipoNavegacionDTO.class,
					id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoNavegacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoNavegacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoNavegacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoNavegacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TipoNavegacionDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoNavegacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoNavegacionDTO entities.
	 * 
	 * @return List<TipoNavegacionDTO> all TipoNavegacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoNavegacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoNavegacionDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from TipoNavegacionDTO model " +
					"order by model.descripcionTipoNavegacion";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find filtered TipoBanderaDTO entities.
	 * 
	 * @return List<TipoBanderaDTO> filtered TipoBanderaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoNavegacionDTO> listarFiltrado(TipoNavegacionDTO tipoNavegacionDTO) {		
		try {
			String queryString = "select model from TipoNavegacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (tipoNavegacionDTO == null)
				return null;
						
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionTipoNavegacion", tipoNavegacionDTO.getDescripcionTipoNavegacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoTipoNavegacion", tipoNavegacionDTO.getCodigoTipoNavegacion());		
			
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoNavegacionDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoNavegacionDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<TipoNavegacionDTO> listRelated(Object id) {
		return this.findAll();
	}
}