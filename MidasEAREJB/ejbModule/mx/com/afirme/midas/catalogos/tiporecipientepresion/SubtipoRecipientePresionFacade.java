package mx.com.afirme.midas.catalogos.tiporecipientepresion;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity SubtipoRecipientePresionDTO.
 * 
 * @see .SubtipoRecipientePresionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SubtipoRecipientePresionFacade implements
		SubtipoRecipientePresionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * SubtipoRecipientePresionDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            SubtipoRecipientePresionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubtipoRecipientePresionDTO entity) {
		LogDeMidasEJB3.log("saving SubtipoRecipientePresionDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SubtipoRecipientePresionDTO entity.
	 * 
	 * @param entity
	 *            SubtipoRecipientePresionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubtipoRecipientePresionDTO entity) {
		LogDeMidasEJB3.log("deleting SubtipoRecipientePresionDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					SubtipoRecipientePresionDTO.class, entity
							.getIdSubtipoRecipientePresion());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SubtipoRecipientePresionDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * SubtipoRecipientePresionDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            SubtipoRecipientePresionDTO entity to update
	 * @return SubtipoRecipientePresionDTO the persisted
	 *         SubtipoRecipientePresionDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubtipoRecipientePresionDTO update(SubtipoRecipientePresionDTO entity) {
		LogDeMidasEJB3.log("updating SubtipoRecipientePresionDTO instance",
				Level.INFO, null);
		try {
			SubtipoRecipientePresionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SubtipoRecipientePresionDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding SubtipoRecipientePresionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SubtipoRecipientePresionDTO instance = entityManager.find(
					SubtipoRecipientePresionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubtipoRecipientePresionDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SubtipoRecipientePresionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubtipoRecipientePresionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SubtipoRecipientePresionDTO> findByProperty(
			String propertyName, final Object value) {
		LogDeMidasEJB3.log(
				"finding SubtipoRecipientePresionDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SubtipoRecipientePresionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubtipoRecipientePresionDTO entities.
	 * 
	 * @return List<SubtipoRecipientePresionDTO> all SubtipoRecipientePresionDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubtipoRecipientePresionDTO> findAll() {
		LogDeMidasEJB3.log("finding all SubtipoRecipientePresionDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from SubtipoRecipientePresionDTO model " +
					"order by model.descripcionSubtipoRecPresion";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SubtipoRecipientePresionDTO> listarFiltrado(
			SubtipoRecipientePresionDTO subTipoRecPresionDTO) {

		try {
			String queryString = "select model from SubtipoRecipientePresionDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (subTipoRecPresionDTO == null)
				return null;

			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigoSubtipoRecipientePresion",
					subTipoRecPresionDTO.getCodigoSubtipoRecipientePresion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "descripcionSubtipoRecPresion",
					subTipoRecPresionDTO.getDescripcionSubtipoRecPresion());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public SubtipoRecipientePresionDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public SubtipoRecipientePresionDTO findById(double id) {
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<SubtipoRecipientePresionDTO> listRelated(Object id) {
		LogDeMidasEJB3.log("finding all SubtipoRecipientePresionDTO related with id: "+id, Level.INFO, null);
		try {
			final String queryString = "select model from SubtipoRecipientePresionDTO model where " +
					"model.tipoRecipientePresionDTO.idTipoRecipientePresion = (select sg.tipoRecipientePresionDTO.idTipoRecipientePresion " +
					"from SubtipoRecipientePresionDTO sg where sg.idSubtipoRecipientePresion = :id) order by model.descripcionSubtipoRecPresion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("id", (BigDecimal)id);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}