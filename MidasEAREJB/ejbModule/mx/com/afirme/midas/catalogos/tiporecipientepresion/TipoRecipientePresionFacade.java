package mx.com.afirme.midas.catalogos.tiporecipientepresion;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity TipoRecipientePresionDTO.
 * 
 * @see .TipoRecipientePresionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoRecipientePresionFacade implements
		TipoRecipientePresionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoRecipientePresionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            TipoRecipientePresionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoRecipientePresionDTO entity) {
		LogDeMidasEJB3.log("saving TipoRecipientePresionDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoRecipientePresionDTO entity.
	 * 
	 * @param entity
	 *            TipoRecipientePresionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoRecipientePresionDTO entity) {
		LogDeMidasEJB3.log("deleting TipoRecipientePresionDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(TipoRecipientePresionDTO.class,
					entity.getIdTipoRecipientePresion());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoRecipientePresionDTO entity and return it
	 * or a copy of it to the sender. A copy of the TipoRecipientePresionDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoRecipientePresionDTO entity to update
	 * @return TipoRecipientePresionDTO the persisted TipoRecipientePresionDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoRecipientePresionDTO update(TipoRecipientePresionDTO entity) {
		LogDeMidasEJB3.log("updating TipoRecipientePresionDTO instance",
				Level.INFO, null);
		try {
			TipoRecipientePresionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoRecipientePresionDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding TipoRecipientePresionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoRecipientePresionDTO instance = entityManager.find(
					TipoRecipientePresionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoRecipientePresionDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the TipoRecipientePresionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoRecipientePresionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoRecipientePresionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding TipoRecipientePresionDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoRecipientePresionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoRecipientePresionDTO entities.
	 * 
	 * @return List<TipoRecipientePresionDTO> all TipoRecipientePresionDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoRecipientePresionDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoRecipientePresionDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from TipoRecipientePresionDTO model " +
					"order by model.descripcionTipoRecPresion";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TipoRecipientePresionDTO> listarFiltrado(
			TipoRecipientePresionDTO tipoRecipientePresionDTO) {
		try {
			String queryString = "select model from TipoRecipientePresionDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (tipoRecipientePresionDTO == null)
				return null;

			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigoTipoRecipientePresion",
					tipoRecipientePresionDTO.getCodigoTipoRecipientePresion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "descripcionTipoRecPresion",
					tipoRecipientePresionDTO.getDescripcionTipoRecPresion());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoRecipientePresionDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoRecipientePresionDTO findById(double id) {
		return null;
	}

	public List<TipoRecipientePresionDTO> listRelated(Object id) {
		return findAll();
	}

}