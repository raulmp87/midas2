package mx.com.afirme.midas.catalogos.contacto;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity ContactoDTO.
 * @see .ContactoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ContactoFacade  implements ContactoFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ContactoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ContactoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ContactoDTO entity) {
    				LogDeMidasEJB3.log("saving ContactoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ContactoDTO entity.
	  @param entity ContactoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ContactoDTO entity) {
    				LogDeMidasEJB3.log("deleting ContactoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ContactoDTO.class, entity.getIdtccontacto());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ContactoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ContactoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ContactoDTO entity to update
	 @return ContactoDTO the persisted ContactoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ContactoDTO update(ContactoDTO entity) {
    				LogDeMidasEJB3.log("updating ContactoDTO instance", Level.INFO, null);
	        try {
            ContactoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ContactoDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding ContactoDTO instance with id: " + id, Level.INFO, null);
	        try {
            ContactoDTO instance = entityManager.find(ContactoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ContactoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ContactoDTO property to query
	  @param value the property value to match
	  	  @return List<ContactoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ContactoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ContactoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ContactoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ContactoDTO entities.
	  	  @return List<ContactoDTO> all ContactoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ContactoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ContactoDTO instances", Level.INFO, null);
			try {
				final String queryString = "select model from ContactoDTO model";
				Query query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find filtered ContactoDTO entities.
	  	  @return List<ContactoDTO> filtered ContactoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ContactoDTO> listarFiltrado(ContactoDTO contactoDTO) {		
		try {
			String queryString = "select model from ContactoDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (contactoDTO == null)
				return null;
			if (contactoDTO.getReaseguradorCorredor()!= null)		
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "reaseguradorCorredor.idtcreaseguradorcorredor", contactoDTO.getReaseguradorCorredor().getIdtcreaseguradorcorredor(), "idtcreaseguradorcorredor");						
			
			if (contactoDTO.getNombre()!= null)
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombre", contactoDTO.getNombre());
			
			if (contactoDTO.getTelefono()!= null)	
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "telefono", contactoDTO.getTelefono());
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public ContactoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public ContactoDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<ContactoDTO> listRelated(Object id) {
		return this.findAll();
	}
}