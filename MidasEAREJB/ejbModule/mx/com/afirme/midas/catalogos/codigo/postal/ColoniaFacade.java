package mx.com.afirme.midas.catalogos.codigo.postal;

// default package

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity ColoniaDTO.
 * 
 * @see .ColoniaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ColoniaFacade implements ColoniaFacadeRemote {
	// property constants

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved ColoniaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ColoniaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ColoniaDTO entity) {
		LogDeMidasEJB3.log("saving ColoniaDTO instance", Level.FINE, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ColoniaDTO entity.
	 * 
	 * @param entity
	 *            ColoniaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ColoniaDTO entity) {
		LogDeMidasEJB3.log("deleting ColoniaDTO instance", Level.FINE, null);
		try {
			entity = entityManager.getReference(ColoniaDTO.class, entity
					.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ColoniaDTO entity and return it or a copy of
	 * it to the sender. A copy of the ColoniaDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ColoniaDTO entity to update
	 * @returns ColoniaDTO the persisted ColoniaDTO entity instance, may not be
	 *          the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ColoniaDTO update(ColoniaDTO entity) {
		LogDeMidasEJB3.log("updating ColoniaDTO instance", Level.FINE, null);
		try {
			ColoniaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.FINE, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ColoniaDTO findById(String id) {
		LogDeMidasEJB3.log("finding ColoniaDTO instance with id: " + id,
				Level.FINE, null);
		try {
			ColoniaDTO instance = entityManager.find(ColoniaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}

	}

	/**
	 * Find all ColoniaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ColoniaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ColoniaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ColoniaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding ColoniaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.FINE, null);
		try {
			final String queryString = "select model from ColoniaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ColoniaDTO entities.
	 * 
	 * @return List<ColoniaDTO> all ColoniaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ColoniaDTO> findAll() {
		LogDeMidasEJB3
				.log("finding all ColoniaDTO instances", Level.FINE, null);
		try {
			final String queryString = "select model from ColoniaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ColoniaDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding ColoniaDTO instance with id: " + id,
				Level.FINE, null);
		try {
			ColoniaDTO instance = entityManager.find(ColoniaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ColoniaDTO> listarFiltrado(ColoniaDTO coloniaDTO){
	    LogDeMidasEJB3.log("Entra a listar filtrado" ,
			Level.FINE, null);
	    try {
		if((coloniaDTO==null)){
		    return null;
		}
		 LogDeMidasEJB3.log("listar filtrado por ZipCode: "+ coloniaDTO.getZipCode() +" y colonyName: "+coloniaDTO.getColonyName(),
				Level.FINE, null);
		  
		String queryString="select model from ColoniaDTO As model ";
		String sWhere="";
		Query query;
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "zipCode", coloniaDTO.getZipCode());
		sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "colonyName", coloniaDTO.getColonyName());
	    
		if (Utilerias.esAtributoQueryValido(sWhere)){
			queryString = queryString.concat(" where ").concat(sWhere);
			
		}
		
		queryString += " order by model.colonyName asc, model.zipCode asc ";//TODO: revisar el comportamiento del paginado con la lista ordenada
		query = entityManager.createQuery(queryString);
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		if(coloniaDTO.getPrimerRegistroACargar() != null && coloniaDTO.getNumeroMaximoRegistrosACargar() != null) {
			query.setFirstResult(coloniaDTO.getPrimerRegistroACargar().intValue());
			query.setMaxResults(coloniaDTO.getNumeroMaximoRegistrosACargar().intValue());
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	    
	    } catch (RuntimeException re) {
		LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
		throw re;
	    }
	
	
	}
	
	@SuppressWarnings("unchecked")
	public Long obtenerTotalFiltrado(ColoniaDTO coloniaDTO){
		LogDeMidasEJB3.log("Entra a listar filtrado" ,
				Level.FINE, null);
		    try {
			if((coloniaDTO==null)){
			    return null;
			}
			 LogDeMidasEJB3.log("listar numero de elementos filtrados por ZipCode: "+coloniaDTO.getZipCode() +" y colonyName: "+coloniaDTO.getColonyName(),
					Level.FINE, null);
			  
			String queryString="select count(model) from ColoniaDTO As model ";
			String sWhere="";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "zipCode", coloniaDTO.getZipCode());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "colonyName", coloniaDTO.getColonyName());
		    
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			return (Long)query.getSingleResult();
		    
		    } catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		    }
	}

	public ColoniaDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public ColoniaDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<ColoniaDTO> listRelated(Object id) {
		return this.findAll();
	}

	@SuppressWarnings("unchecked")
	public List<ColoniaDTO> findByColonyName(BigDecimal cityId, BigDecimal zipCode, String colonyName) {
		LogDeMidasEJB3.log("finding ColoniaDTO instances with cityId: " + cityId + " colonyName: " + colonyName, Level.FINE, null);
		try {
			String queryString = "select model from ColoniaDTO model " +
					"where model.cityId = :cityId " +
					"and model.zipCode = :zipCode " +
					"and model.colonyName = :colonyName";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("cityId", new DecimalFormat("00000.####").format(cityId.doubleValue()));
			query.setParameter("zipCode", new DecimalFormat("00000.####").format(zipCode.doubleValue()));
			query.setParameter("colonyName", colonyName);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
	/**
	 * Obtiene la lista de colonias por el id de ciudad y nombre de la colonia
	 * @param cityId
	 * @param colonyName
	 * @return
	 */
	public List<ColoniaDTO> findByColonyName(BigDecimal cityId, String colonyName) {
		LogDeMidasEJB3.log("finding ColoniaDTO instances with cityId: " + cityId + " colonyName: " + colonyName, Level.FINE, null);
		try {
			String queryString = "select model from ColoniaDTO model " +
					"where model.cityId = :cityId " +
					"and model.colonyName = :colonyName";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("cityId", new DecimalFormat("00000.####").format(cityId.doubleValue()));
			query.setParameter("colonyName", colonyName);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	
	@SuppressWarnings("unchecked")
	public List<ColoniaDTO> getColonyByZipCode(String zipCode) {
		String query = "select model from ColoniaDTO model " +
				" where model.zipCode = :zipCode";
		List<ColoniaDTO> list = entityManager.createQuery(query)
				.setParameter("zipCode", zipCode)
				.getResultList();
		return list;
	}	
}