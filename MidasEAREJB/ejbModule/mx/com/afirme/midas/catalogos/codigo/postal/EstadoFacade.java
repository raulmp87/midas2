package mx.com.afirme.midas.catalogos.codigo.postal;

// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity EstadoDTO.
 * 
 * @see .EstadoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class EstadoFacade implements EstadoFacadeRemote {
	// property constants

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved EstadoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            EstadoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(EstadoDTO entity) {
		LogDeMidasEJB3.log("saving EstadoDTO instance", Level.FINE, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void save(RecargosDescuentosCPDTO entity) {
		LogDeMidasEJB3.log("saving RecargosDescuentosCPDTO instance", Level.FINE, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent EstadoDTO entity.
	 * 
	 * @param entity
	 *            EstadoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(EstadoDTO entity) {
		LogDeMidasEJB3.log("deleting EstadoDTO instance", Level.FINE, null);
		try {
			entity = entityManager
					.getReference(EstadoDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void delete(RecargosDescuentosCPDTO entity) {
		LogDeMidasEJB3.log("deleting RecargosDescuentosCPDTO instance", Level.FINE, null);
		try {
			String sql;
			Query query;
			if (entity.getIdRecargoDescuento()==null){
				sql="select model.idRecargoDescuento from RecargosDescuentosCPDTO model where model.codigoPostal=?1 and model.idToCobertura=?2 and model.idToSeccion=?3";
				query = entityManager.createQuery(sql);
				query.setParameter("1",entity.getCodigoPostal());
				query.setParameter("2",entity.getIdToCobertura());
				query.setParameter("3",entity.getIdToSeccion());
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				Long id=Long.valueOf(query.getResultList().get(0).toString());
				entity.setIdRecargoDescuento(id);;
			}
			sql = "delete from RecargosDescuentosCPDTO where idRecargoDescuento=?1";
			query=null;
			query = entityManager.createQuery(sql);
			query.setParameter("1",entity.getIdRecargoDescuento());
			query.executeUpdate();
			LogDeMidasEJB3.log("delete successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			//throw re;
		}
	}

	/**
	 * Persist a previously saved EstadoDTO entity and return it or a copy of it
	 * to the sender. A copy of the EstadoDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            EstadoDTO entity to update
	 * @returns EstadoDTO the persisted EstadoDTO entity instance, may not be
	 *          the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public EstadoDTO update(EstadoDTO entity) {
		LogDeMidasEJB3.log("updating EstadoDTO instance", Level.FINE, null);
		try {
			EstadoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.FINE, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RecargosDescuentosCPDTO update(RecargosDescuentosCPDTO entity) {
		LogDeMidasEJB3.log("updating RecargosDescuentosCPDTO instance", Level.FINE, null);
		try {
			String sql = "UPDATE RecargosDescuentosCPDTO SET codigoPostal=?1,tipoValor=?2,valor=?3,fechaVencimiento=?4 where idRecargoDescuento=?5";
			Query query = entityManager.createQuery(sql);
			query.setParameter("1",entity.getCodigoPostal());
			query.setParameter("2",entity.getTipoValor());
			query.setParameter("3",entity.getValor());
			query.setParameter("4",entity.getFechaVencimiento());
			query.setParameter("5",entity.getIdRecargoDescuento());
			query.executeUpdate();			
			LogDeMidasEJB3.log("update successful", Level.FINE, null);
			return null;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public EstadoDTO findById(String id) {
		LogDeMidasEJB3.log("finding EstadoDTO instance with id: " + id,
				Level.FINE, null);
		try {
			//El Estado proviene de la vista VW_STATE y el campo que funciona como ID de los estados es STATE_ID
			//pero este campo es de tipo VARCHAR y en la clase EstadoDTO se mape� como "String stateId", por lo cual los registros se deben 
			//buscar enviando un atributo tipo String, el cual debe contener 5 caracteres, ya que as� est�n registrados en la vista.
			//Por lo tanto, al obtener el IdEstado del objeto DireccionDTO, se debe validar si el String correspondiente tiene esta longitud.
			//De no ser as�, a la cadena se le agregar�n tantos ceros a la izquierda como sean necesarios para completar la longitud de 5 caracteres.
			final String idEstado = StringUtils.leftPad(id, 5, '0');
			return entityManager.find(EstadoDTO.class, idEstado);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all EstadoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the EstadoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<EstadoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<EstadoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding EstadoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.FINE, null);
		try {
			final String queryString = "select model from EstadoDTO model where model."
					+ propertyName + "= :propertyValue order by model.stateName ASC ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all EstadoDTO entities.
	 * 
	 * @return List<EstadoDTO> all EstadoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<EstadoDTO> findAll() {
		LogDeMidasEJB3.log("finding all EstadoDTO instances", Level.FINE, null);
		try {
			final String queryString = "select model from EstadoDTO model order by model.stateName asc";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public EstadoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding EstadoDTO instance with id: " + id,
				Level.FINE, null);
		try {
			EstadoDTO instance = entityManager.find(EstadoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	public EstadoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public EstadoDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<EstadoDTO> listRelated(Object id) {
		return this.findAll();
	}
	
	public String getStateIdByZipCode(String zipCode) {
		String stateId = null;

		String cityId = ciudadFacadeRemote.getCityIdByZipCode(zipCode);
		if (cityId != null) {
			CiudadDTO ciudadDTO =
				ciudadFacadeRemote.findById(cityId);
			if (ciudadDTO != null) {
				stateId = ciudadDTO.getStateId();
			}
		}
		return stateId;
	}
	
	public Boolean isEstadoFronterizo(String idEstado) {
		if (idEstado == null || idEstado == "") {
			return null;
		}
		idEstado = idEstado + "%";
		String sql = "select count(*) from  seycos.poblacion where CVE_RET_IMP = 'V' and cve_poblacion LIKE ?";
		Query query = entityManager.createNativeQuery(sql);
		query.setParameter(1, idEstado);
		Long total = ((BigDecimal) query.getResultList().get(0)).longValue();
		if (total > 0) {
			return true;
		}
		return false;
	}
	
	private CiudadFacadeRemote ciudadFacadeRemote;
	
	@EJB
	public void setCiudadFacadeRemote(CiudadFacadeRemote ciudadFacadeRemote) {
		this.ciudadFacadeRemote = ciudadFacadeRemote;
	}
	
	public String validarCodigoPostal(String codigoPostal,int idEstado){
		
		String sql = "select count(*) from midas.tclimitescodigopostal where CVE_ESTADO=?1 and LIMITEINFERIOR<=?2 and LIMITESUPERIOR>=?2";
		Query query = entityManager.createNativeQuery(sql);
		query.setParameter(1, idEstado);
		query.setParameter(2, codigoPostal);
		BigDecimal result=(BigDecimal) query.getResultList().get(0);
		Object []tem=null;
	
		return result.toString();
	}
	
	
	
	public List<RecargosDescuentosCPDTO> getRegargosCP(String idCobertura,String idSeccion){
		
		String sql = "select model from RecargosDescuentosCPDTO model where model.idToSeccion=?1 and model.idToCobertura=?2";
		Query query = entityManager.createQuery(sql);
		query.setParameter("1", Integer.parseInt(idSeccion));
		query.setParameter("2", Integer.parseInt(idCobertura));
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
}