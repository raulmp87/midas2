package mx.com.afirme.midas.catalogos.codigo.postal;

// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity PaisDTO.
 * 
 * @see .PaisDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class PaisFacade implements PaisFacadeRemote {
	// property constants

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved PaisDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            PaisDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(PaisDTO entity) {
		LogDeMidasEJB3.log("saving PaisDTO instance", Level.FINE, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent PaisDTO entity.
	 * 
	 * @param entity
	 *            PaisDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(PaisDTO entity) {
		LogDeMidasEJB3.log("deleting PaisDTO instance", Level.FINE, null);
		try {
			entity = entityManager.getReference(PaisDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved PaisDTO entity and return it or a copy of it
	 * to the sender. A copy of the PaisDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            PaisDTO entity to update
	 * @returns PaisDTO the persisted PaisDTO entity instance, may not be the
	 *          same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public PaisDTO update(PaisDTO entity) {
		LogDeMidasEJB3.log("updating PaisDTO instance", Level.FINE, null);
		try {
			PaisDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.FINE, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public PaisDTO findById(String id) {
		LogDeMidasEJB3.log("finding PaisDTO instance with id: " + id,
				Level.FINE, null);
		try {
			PaisDTO instance = entityManager.find(PaisDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all PaisDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the PaisDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<PaisDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<PaisDTO> findByProperty(String propertyName, final Object value) {
		LogDeMidasEJB3.log("finding PaisDTO instance with property: "
				+ propertyName + ", value: " + value, Level.FINE, null);
		try {
			final String queryString = "select model from PaisDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all PaisDTO entities.
	 * 
	 * @return List<PaisDTO> all PaisDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<PaisDTO> findAll() {
		LogDeMidasEJB3.log("finding all PaisDTO instances", Level.FINE, null);
		try {
			final String queryString = "select model from PaisDTO model ORDER BY model.countryName ASC";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<PaisDTO> getPaisDTONotAssociatedToTipoDestino(){
		LogDeMidasEJB3.log("finding all PaisDTO instances not referenced in PaisTipoDestinoTransporteDTO", Level.FINE, null);
		try{
			final String queryString = "SELECT model FROM PaisDTO model WHERE model.countryId <> ALL (SELECT model2.idPais FROM PaisTipoDestinoTransporteDTO model2) ORDER BY model.countryName ASC";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getPaisDTONotAssociatedToTipoDestino failed", Level.SEVERE, re);
			throw re;
		}
	}

	public PaisDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding PaisDTO instance with id: " + id,
				Level.FINE, null);
		try {
			PaisDTO instance = entityManager.find(PaisDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	public PaisDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public PaisDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<PaisDTO> listRelated(Object id) {
		return this.findAll();
	}
}