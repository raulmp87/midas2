package mx.com.afirme.midas.catalogos.codigo.postal;

// default package

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity CiudadDTO.
 * 
 * @see .CiudadDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CiudadFacade implements CiudadFacadeRemote {
	// property constants

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved CiudadDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            CiudadDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CiudadDTO entity) {
		LogDeMidasEJB3.log("saving CiudadDTO instance", Level.FINE, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent CiudadDTO entity.
	 * 
	 * @param entity
	 *            CiudadDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CiudadDTO entity) {
		LogDeMidasEJB3.log("deleting CiudadDTO instance", Level.FINE, null);
		try {
			entity = entityManager
					.getReference(CiudadDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved CiudadDTO entity and return it or a copy of it
	 * to the sender. A copy of the CiudadDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            CiudadDTO entity to update
	 * @returns CiudadDTO the persisted CiudadDTO entity instance, may not be
	 *          the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CiudadDTO update(CiudadDTO entity) {
		LogDeMidasEJB3.log("updating CiudadDTO instance", Level.FINE, null);
		try {
			CiudadDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.FINE, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CiudadDTO findById(String id) {
		LogDeMidasEJB3.log("finding CiudadDTO instance with id: " + id,
				Level.FINE, null);
		try {
			CiudadDTO instance = entityManager.find(CiudadDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CiudadDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CiudadDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CiudadDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CiudadDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding CiudadDTO instance with property: "
				+ propertyName + ", value: " + value, Level.FINE, null);
		try {
			final String queryString = "select model from CiudadDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CiudadDTO entities.
	 * 
	 * @return List<CiudadDTO> all CiudadDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CiudadDTO> findAll() {
		LogDeMidasEJB3.log("finding all CiudadDTO instances", Level.FINE, null);
		try {
			final String queryString = "select model from CiudadDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CiudadDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding CiudadDTO instance with id: " + id,
				Level.FINE, null);
		try {
			CiudadDTO instance = entityManager.find(CiudadDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CiudadDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public CiudadDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<CiudadDTO> listRelated(Object id) {
		return this.findAll();
	}

	@SuppressWarnings("unchecked")
	public List<CiudadDTO> findByCityName(BigDecimal stateId, String cityName) {
		LogDeMidasEJB3.log("finding CiudadDTO instance with stateId: "
				+ stateId.toString() + " and cityName: " + cityName, Level.FINE, null);
		try {
			String queryString = "select model from CiudadDTO model ";
			String sWhere = "";
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
//			String queryString = "select model from CiudadDTO model " +
//					"where model.stateId = :stateId " +
//					"and model.cityName like '%:cityName%' "  +
//					"and model.cityId not like '-%'";
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"stateId", new DecimalFormat("00000.####").format(stateId.doubleValue()));
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "cityName", cityName);
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);	
			queryString += " and model.cityId not like '-%'";
			Query query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
//			query.setParameter("stateId", new DecimalFormat("00000.####").format(stateId.doubleValue()));
//			query.setParameter("cityName", cityName);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getCityIdByZipCode(String zipCode) {
		String cityId = null;
		List<ColoniaDTO> list = 
			entityManager.createQuery("select model from ColoniaDTO model " +
				" where model.zipCode = :zipCode")
				.setParameter("zipCode", zipCode.trim())
				.setMaxResults(1)
				.getResultList();
		if (list.size() == 1) {
			ColoniaDTO coloniaDTO = list.get(0);			
			cityId = coloniaDTO.getCityId();
		}
		return cityId;
	}
	
	@SuppressWarnings("unchecked")
	public CiudadDTO getCityByZipCode(String zipCode) {
		String cityId = null;
		CiudadDTO ciudad = null;
		
		List<ColoniaDTO> list = 
			entityManager.createQuery("select model from ColoniaDTO model " +
				" where model.zipCode = :zipCode")
				.setParameter("zipCode", zipCode.trim())
				.setMaxResults(1)
				.getResultList();
		
		if (list.size() > 0) {
			ColoniaDTO coloniaDTO = list.get(0);			
			cityId = coloniaDTO.getCityId();
			
			ciudad = this.findById(cityId);
		}
		
		return ciudad;
	}
}