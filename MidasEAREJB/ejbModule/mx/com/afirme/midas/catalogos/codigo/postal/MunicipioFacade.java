package mx.com.afirme.midas.catalogos.codigo.postal;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.*;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
/**
 * Facade for entity MunicipioDTO.
 * @see .MunicipioDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class MunicipioFacade  implements MunicipioFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved MunicipioDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MunicipioDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(MunicipioDTO entity) {
    				LogDeMidasEJB3.log("saving MunicipioDTO instance", Level.FINE, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.FINE, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent MunicipioDTO entity.
	  @param entity MunicipioDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MunicipioDTO entity) {
    				LogDeMidasEJB3.log("deleting MunicipioDTO instance", Level.FINE, null);
	        try {
        	entity = entityManager.getReference(MunicipioDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.FINE, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved MunicipioDTO entity and return it or a copy of it to the sender. 
	 A copy of the MunicipioDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MunicipioDTO entity to update
	 @return MunicipioDTO the persisted MunicipioDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public MunicipioDTO update(MunicipioDTO entity) {
    				LogDeMidasEJB3.log("updating MunicipioDTO instance", Level.FINE, null);
	        try {
            MunicipioDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.FINE, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public MunicipioDTO findById( String id) {
    	try {
    		//El Municipio proviene de la vista VW_MUNICIPALITY y el campo que funciona como ID de los estados es MUNICIPALITY_ID
			//pero este campo es de tipo VARCHAR y en la clase MunicipioDTO se mape� como "String municipalityId", por lo cual los registros se deben 
			//buscar enviando un atributo tipo String, el cual debe contener 5 caracteres, ya que as� est�n registrados en la vista.
			//Por lo tanto, al obtener el IdMunicipio del objeto DireccionDTO, se debe validar si el String correspondiente tiene esta longitud.
			//De no ser as�, a la cadena se le agregar�n tantos ceros a la izquierda como sean necesarios para completar la longitud de 5 caracteres.
    		final String idMunicipio = StringUtils.leftPad(id, 5, '0');
            return entityManager.find(MunicipioDTO.class, idMunicipio);
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("find MunicipioDTO instance with id: " + id+"failed", Level.SEVERE, re);
        	throw re;
        }
    }    
    

/**
	 * Find all MunicipioDTO entities with a specific property value.  
	 
	  @param propertyName the name of the MunicipioDTO property to query
	  @param value the property value to match
	  	  @return List<MunicipioDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<MunicipioDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding MunicipioDTO instance with property: " + propertyName + ", value: " + value, Level.FINE, null);
			try {
			final String queryString = "select model from MunicipioDTO model where model." 
			 						+ propertyName + "= :propertyValue "
			 						+ " order by model.municipalityName DESC";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all MunicipioDTO entities.
	  	  @return List<MunicipioDTO> all MunicipioDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<MunicipioDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all MunicipioDTO instances", Level.FINE, null);
			try {
			final String queryString = "select model from MunicipioDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	public MunicipioDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding MunicipioDTO instance with id: " + id,
				Level.FINE, null);
		try {
			MunicipioDTO instance = entityManager.find(MunicipioDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	public MunicipioDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public MunicipioDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<MunicipioDTO> listRelated(Object id) {
		return this.findAll();
	}
}