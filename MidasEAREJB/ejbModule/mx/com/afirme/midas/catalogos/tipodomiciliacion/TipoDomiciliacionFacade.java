package mx.com.afirme.midas.catalogos.tipodomiciliacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

@Stateless
public class TipoDomiciliacionFacade implements TipoDomiciliacionFacadeRemote{
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<TipoDomiciliacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoDomiciliacionDTO instances", Level.INFO, null);
		try{
			String statement="select model from TipoDomiciliacionDTO model";
			Query query= entityManager.createQuery(statement);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public TipoDomiciliacionDTO findById(BigDecimal id) {
		return (id!=null)?findById(id.intValue()):null;
	}

	@Override
	public TipoDomiciliacionDTO findById(CatalogoValorFijoId arg0) {
		return null;
	}

	@Override
	public TipoDomiciliacionDTO findById(double id) {
		return findById(Integer.parseInt(String.valueOf(id)));
	}

	@Override
	public List<TipoDomiciliacionDTO> listRelated(Object arg0) {
		return null;
	}

	@Override
	public TipoDomiciliacionDTO findById(Integer id) {
		LogDeMidasEJB3.log("finding TipoDomiciliacionDTO by id with id:"+id, Level.INFO, null);
		try{
			TipoDomiciliacionDTO instance=entityManager.find(TipoDomiciliacionDTO.class,id);
			return instance;
		}catch(RuntimeException  re){
			LogDeMidasEJB3.log("find by id", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public TipoDomiciliacionDTO findByName(String valor) {
		LogDeMidasEJB3.log("finding TipoDomiciliacionDTO by tipo domiciliacion name with name:"+valor, Level.INFO, null);
		try{
			if(valor==null || valor.isEmpty()){
				return null;
			}
			String queryString="select model from TipoDomiciliacionDTO model ";
			String sWhere="";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "valor",valor);
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
			}
			queryString += " order by model.id desc, model.valor asc ";//TODO: revisar el comportamiento del paginado con la lista ordenada
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return (TipoDomiciliacionDTO)query.getSingleResult();
		}catch(RuntimeException re){
			LogDeMidasEJB3.log("find by name", Level.SEVERE, re);
			throw re;
		}
	}

}
