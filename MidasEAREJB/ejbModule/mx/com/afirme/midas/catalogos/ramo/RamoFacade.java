package mx.com.afirme.midas.catalogos.ramo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity Tcramo.
 * 
 * @see mx.com.afirme.midas.catalogos.ramo.RamoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class RamoFacade implements RamoFacadeRemote {

	private String claveNegocioDefault ="D";
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved Tcramo entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            Tcramo entity to persist
	 */
	public void save(RamoDTO entity) {
		LogDeMidasEJB3.log("saving Tcramo instance", Level.INFO, null);
		entityManager.persist(entity);
		LogDeMidasEJB3.log("save successful", Level.INFO, null);
	}

	/**
	 * Delete a persistent Tcramo entity.
	 * 
	 * @param entity
	 *            Tcramo entity to delete
	 */
	public void delete(RamoDTO entity) {
		LogDeMidasEJB3.log("deleting Tcramo instance", Level.INFO, null);
		entity = entityManager
				.getReference(RamoDTO.class, entity.getIdTcRamo());
		entityManager.remove(entity);
		LogDeMidasEJB3.log("delete successful", Level.INFO, null);

	}

	/**
	 * Persist a previously saved Tcramo entity and return it or a copy of it to
	 * the sender. A copy of the Tcramo entity parameter is returned when the
	 * JPA persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            Tcramo entity to update
	 * @return Tcramo the persisted Tcramo entity instance, may not be the same
	 */
	public RamoDTO update(RamoDTO entity) {
		LogDeMidasEJB3.log("updating Tcramo instance", Level.INFO, null);
		RamoDTO result = entityManager.merge(entity);
		LogDeMidasEJB3.log("update successful", Level.INFO, null);
		return result;

	}

	public RamoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding Tcramo instance with id: " + id,
				Level.INFO, null);
		RamoDTO instance = entityManager.find(RamoDTO.class, id);
		return instance;

	}

	/**
	 * Find all Tcramo entities with a specific property value.  By default only bring RamoDTO with claveNegocio = D
	 * 
	 * @param propertyName
	 *            the name of the Tcramo property to query
	 * @param value
	 *            the property value to match
	 * @return List<Tcramo> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RamoDTO> findByProperty(String propertyName, final Object value) {
		LogDeMidasEJB3.log("finding Tcramo instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		final String queryString = "select model from RamoDTO AS model where model."
				+ propertyName + "= :propertyValue" +
				(!isEmptyClaveNegocioDefault()?" and model.claveNegocio =:defaultClaveNegocio":"");
		Query query = entityManager.createQuery(queryString);
		query.setParameter("propertyValue", value);
		if(!isEmptyClaveNegocioDefault())
			query.setParameter("defaultClaveNegocio", claveNegocioDefault);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();

	}

	/**
	 * Find all Tcramo entities.  By default only bring RamoDTO with claveNegocio = D
	 * 
	 * @return List<Tcramo> all Tcramo entities
	 */
	@SuppressWarnings("unchecked")
	public List<RamoDTO> findAll() {
		LogDeMidasEJB3.log("finding all Tcramo instances", Level.INFO, null);
		final String queryString = "select model from RamoDTO AS model"+ 
		(isEmptyClaveNegocioDefault()?" where model.claveNegocio =:defaultClaveNegocio":"");
		Query query = entityManager.createQuery(queryString);
		if(isEmptyClaveNegocioDefault())
			query.setParameter("defaultClaveNegocio", claveNegocioDefault);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();

	}

	@SuppressWarnings("unchecked")
	public List<RamoDTO> listarFiltrado(RamoDTO ramoDTO) {
		try {
			String queryString = "select model from RamoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (ramoDTO == null)
				return null;
			if(ramoDTO.getIdTcRamo()!=null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
						sWhere, "idTcRamo", ramoDTO.getIdTcRamo());
			}			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigo", ramoDTO.getCodigo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "descripcion", ramoDTO.getDescripcion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveNegocio", ramoDTO.getClaveNegocio());
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<RamoDTO> listarFiltradoLike(RamoDTO ramoDTO) {
		try {
			String queryString = "select model from RamoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (ramoDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigo", ramoDTO.getCodigo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "descripcion", ramoDTO.getDescripcion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveNegocio", ramoDTO.getClaveNegocio());
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public EntityTransaction getTransaction() {
		EntityTransaction transaccion = entityManager.getTransaction();

		return transaccion;
	}

	/**
	 * Lista todos los Ramos que no están asociadas una sección determinada por
	 * el id que recibe el método.  By default only bring RamoDTO with claveNegocio = D
	 * LPV - Eliminado claveNegocioDefault
	 * @param BigDecimal
	 *            idToSeccion el id de la seccion.
	 * @return List<RamoDTO> registros Ramo que no están asociados a la seccion.
	 */
	@SuppressWarnings("unchecked")
	public List<RamoDTO> obtenerRamosSinAsociarSeccion(BigDecimal idToSeccion, BigDecimal idToTipoPoliza) {
		LogDeMidasEJB3.log("Encontrando los ramos no asociados a la seccion: "
				+ idToSeccion, Level.INFO, null);
		try {
			final String queryString = "select model from RamoDTO model, TipoPolizaDTO tp where "
					+ "model.idTcRamo not in (select s.id.idtcramo from RamoSeccionDTO s where s.id.idtoseccion = :propertyValue)"
					+ "and model.idTcRamo in (select p.id.idtcramo from RamoTipoPolizaDTO p where p.id.idtotipopoliza = :idToTipoPoliza) "+
					" and model.claveNegocio = tp.productoDTO.claveNegocio " +
					" and tp.idToTipoPoliza = :idToTipoPoliza ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", idToSeccion);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"Fallo en encontrar las Ramos no asiciadas a la seccion: "
							+ idToSeccion, Level.SEVERE, re);
			throw re;
		}
	}
	private boolean isEmptyClaveNegocioDefault(){
		return claveNegocioDefault != null && !claveNegocioDefault.isEmpty();
	}

	/**
	 * Lista todos los Ramos que no están asociadas un producto determinado por
	 * el id que recibe el método.  By default only bring RamoDTO with claveNegocio = D
	 * LPV - Eliminada condicion claveNegocioDefault
	 * 
	 * @param BigDecimal
	 *            idToProducto el id del producto.
	 * @return List<RamoDTO> registros Ramo que no están asociados al producto.
	 */
	@SuppressWarnings("unchecked")
	public List<RamoDTO> obtenerRamosSinAsociarProducto(BigDecimal idToProducto) {
		LogDeMidasEJB3.log("Encontrando los ramos no asociados al produto: "
				+ idToProducto, Level.INFO, null);
		try {
			final String queryString = "select model from RamoDTO model, ProductoDTO prod where "
					+ "model.idTcRamo not in (select s.id.idtcramo from RamoProductoDTO s where s.id.idtoproducto = :propertyValue)"+
					" and model.claveNegocio = prod.claveNegocio " +
					" and prod.idToProducto = :idToProducto ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", idToProducto);
			query.setParameter("idToProducto", idToProducto);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"Fallo en encontrar las Ramos no asiciadas al producto: "
							+ idToProducto, Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Lista todos los Ramos que no están asociadas un TipoPoliza determinado
	 * por el id que recibe el método.  By default only bring RamoDTO with claveNegocio = D
	 * LPV - Eliminado claveNegocioDefautl
	 * @param BigDecimal
	 *            idToTipoPoliza el id del tipoPoliza.
	 * @return List<RamoDTO> registros Ramo que no están asociados al
	 *         TipoPoliza.
	 */
	@SuppressWarnings("unchecked")
	public List<RamoDTO> obtenerRamosSinAsociarTipoPoliza(
			BigDecimal idToTipoPoliza, BigDecimal idToProducto ) {
		LogDeMidasEJB3.log("Encontrando los ramos no asociados al tipoPoliza: "
				+ idToTipoPoliza + " y producto:" + idToProducto, Level.INFO, null);
		try {
			final String queryString = "select model from RamoDTO model, ProductoDTO prod where "
					+ "model.idTcRamo not in (select s.id.idtcramo from RamoTipoPolizaDTO s where s.id.idtotipopoliza = :propertyValue) "
					+ "and model.idTcRamo in (select p.id.idtcramo from RamoProductoDTO p where p.id.idtoproducto = :idToProducto) "+
					" and model.claveNegocio = prod.claveNegocio " +
					" and prod.idToProducto = :idToProducto ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", idToTipoPoliza);
			query.setParameter("idToProducto", idToProducto);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"Fallo en encontrar las Ramos no asiciadas al tipoPoliza: "
							+ idToTipoPoliza, Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Lista todos los ramos que no se puedan desasociar a un producto.
	 * Los ramos que no se pueden desasociar del producto son aquellos que se encuentran asociados a alguna P�liza VIGENTE que le pertenece al producto.
	 *  By default only bring RamoDTO with claveNegocio = D 
	 * @param BigDecimal idToProducto el id del Producto.
	 * @return List<RamoDTO> registros RamoProducto que no se deban desasociar al producto.
	 */
	@SuppressWarnings("unchecked")
	public List<RamoDTO> obtenerRamosObligatoriosPorProducto(BigDecimal idToProducto){
		LogDeMidasEJB3.log("Encontrando los ramos obligatorios del producto: " + idToProducto, Level.INFO, null);
		try {
			final String queryString = "select model from RamoDTO model where model.idTcRamo in (" +
					"select ramoTP.id.idtcramo from RamoTipoPolizaDTO ramoTP where ramoTP.id.idtotipopoliza in (" +
					"select tipoPoliza.idToTipoPoliza from TipoPolizaDTO tipoPoliza where ((tipoPoliza.productoDTO.idToProducto = :idToProducto) " +
					"and (tipoPoliza.claveEstatus <> 3 )) ))" +
					(!isEmptyClaveNegocioDefault()?" and model.claveNegocio =:defaultClaveNegocio":"");
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToProducto", idToProducto);
			if(!isEmptyClaveNegocioDefault())
				query.setParameter("defaultClaveNegocio", claveNegocioDefault);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fallo en encontrar los ramos obligatorios del producto: "+idToProducto, Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Lista todos los ramos que no se puedan desasociar a un TipoPoliza.
	 * Los ramos que no se pueden desasociar del tipoPoliza son aquellos que se encuentran asociados a alguna seccion VIGENTE asociada al tipoPoliza.
	 *  By default only bring RamoDTO with claveNegocio = D 
	 * @param BigDecimal idToTipoPoliza el id del TipoPoliza.
	 * @return List<RamoDTO> registros Ramo que no se deban desasociar al producto.
	 */
	@SuppressWarnings("unchecked")
	public List<RamoDTO> obtenerRamosObligatoriosPorTipoPoliza(BigDecimal idToTipoPoliza){
		LogDeMidasEJB3.log("Encontrando los ramos obligatorios del tipoPoliza: " + idToTipoPoliza, Level.INFO, null);
		try {
			final String queryString = "select model from RamoDTO model where model.idTcRamo in (" +
					"select ramoSeccion.id.idtcramo from RamoSeccionDTO ramoSeccion where ramoSeccion.id.idtoseccion in (" +
					"select seccion.idToSeccion from SeccionDTO seccion where ((seccion.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza) " +
					"and (seccion.claveEstatus <>3)) ))" +
					(!isEmptyClaveNegocioDefault()?" and model.claveNegocio =:defaultClaveNegocio":"");
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			if(!isEmptyClaveNegocioDefault())
				query.setParameter("defaultClaveNegocio", claveNegocioDefault);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fallo en encontrar los ramos obligatorios del tipoPoliza: "+idToTipoPoliza, Level.SEVERE, re);
			throw re;
		}
	}
	
	public void setClaveNegocioDefault(String claveNegocioDefault) {
		this.claveNegocioDefault = claveNegocioDefault;
	}
	public String getClaveNegocioDefault() {
		return claveNegocioDefault;
	}

	public RamoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public RamoDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<RamoDTO> listRelated(Object id) {
		return this.findAll();
	}
}