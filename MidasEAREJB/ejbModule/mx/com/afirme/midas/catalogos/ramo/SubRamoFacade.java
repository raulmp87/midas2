package mx.com.afirme.midas.catalogos.ramo;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity SubRamoDTO.
 * @see .SubRamoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class SubRamoFacade  implements SubRamoFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved SubRamoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SubRamoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(SubRamoDTO entity) {
    				LogDeMidasEJB3.log("saving SubRamoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent SubRamoDTO entity.
	  @param entity SubRamoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SubRamoDTO entity) {
    				LogDeMidasEJB3.log("deleting SubRamoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(SubRamoDTO.class, entity.getIdTcSubRamo());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved SubRamoDTO entity and return it or a copy of it to the sender. 
	 A copy of the SubRamoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SubRamoDTO entity to update
	 @return SubRamoDTO the persisted SubRamoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public SubRamoDTO update(SubRamoDTO entity) {
    				LogDeMidasEJB3.log("updating SubRamoDTO instance", Level.INFO, null);
	        try {
            SubRamoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public SubRamoDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding SubRamoDTO instance with id: " + id, Level.INFO, null);
	        try {
            SubRamoDTO instance = entityManager.find(SubRamoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all SubRamoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SubRamoDTO property to query
	  @param value the property value to match
	  	  @return List<SubRamoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<SubRamoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding SubRamoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from SubRamoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all SubRamoDTO entities.
	  	  @return List<SubRamoDTO> all SubRamoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubRamoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all SubRamoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from SubRamoDTO as model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	@Override
	public List<SubRamoDTO> findAll(BigDecimal idTcRamo){

		List<SubRamoDTO> list = null;//beanRemoto.findByProperty("ramoDTO.idTcRamo", UtileriasWeb.regresaBigDecimal(id));
		try {
			final String queryString = "select model from SubRamoDTO as model " +
					" where model.ramoDTO.idTcRamo =:pIdTcRamo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("pIdTcRamo", idTcRamo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			list = query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all by idTcRamo failed", Level.SEVERE, re);
			throw re;
		}
		return list;
	}
	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<SubRamoDTO> listarFiltrado(SubRamoDTO subRamoDTO){
		try {
			String queryString = "select model from SubRamoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (subRamoDTO == null)
				return null;
			
			if(subRamoDTO.getIdTcSubRamo()!=null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcSubRamo", subRamoDTO.getIdTcSubRamo());
			}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoSubRamo", subRamoDTO.getCodigoSubRamo());
			
			/*sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idemision", subRamoDTO.getIdemision());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idtecnico", subRamoDTO.getIdtecnico());*/
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "descripcionSubRamo", subRamoDTO.getDescripcionSubRamo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "porcentajeMaximoComisionPRR", subRamoDTO.getPorcentajeMaximoComisionPRR());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "porcentajeMaximoComisionRCI", subRamoDTO.getPorcentajeMaximoComisionRCI());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "porcentajeMaximoComisionRO", subRamoDTO.getPorcentajeMaximoComisionRO());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if(subRamoDTO.getPrimerRegistroACargar() != null && subRamoDTO.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(subRamoDTO.getPrimerRegistroACargar().intValue());
				query.setMaxResults(subRamoDTO.getNumeroMaximoRegistrosACargar().intValue());
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<SubRamoDTO> listarFiltradoLike(SubRamoDTO subRamoDTO){
		try {
			String queryString = "select model from SubRamoDTO AS model join fetch model.ramoDTO";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (subRamoDTO == null)
				return null;
			
			if(subRamoDTO.getIdTcSubRamo()!=null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcSubRamo", subRamoDTO.getIdTcSubRamo());
			}
			
			if(subRamoDTO.getRamoDTO()!= null && subRamoDTO.getRamoDTO().getCodigo()!=null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "ramoDTO.codigo", subRamoDTO.getRamoDTO().getCodigo());
			}
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoSubRamo", subRamoDTO.getCodigoSubRamo());
						
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionSubRamo", subRamoDTO.getDescripcionSubRamo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "porcentajeMaximoComisionPRR", subRamoDTO.getPorcentajeMaximoComisionPRR());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "porcentajeMaximoComisionRCI", subRamoDTO.getPorcentajeMaximoComisionRCI());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "porcentajeMaximoComisionRO", subRamoDTO.getPorcentajeMaximoComisionRO());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if(subRamoDTO.getPrimerRegistroACargar() != null && subRamoDTO.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(subRamoDTO.getPrimerRegistroACargar().intValue());
				query.setMaxResults(subRamoDTO.getNumeroMaximoRegistrosACargar().intValue());
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public Long obtenerTotalFiltrado(SubRamoDTO subRamoDTO){
		try {
			String queryString = "select count(model) from SubRamoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (subRamoDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoSubRamo", subRamoDTO.getCodigoSubRamo());
			
			/*sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idemision", subRamoDTO.getIdemision());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idtecnico", subRamoDTO.getIdtecnico());*/
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "descripcionSubRamo", subRamoDTO.getDescripcionSubRamo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "porcentajeMaximoComisionPRR", subRamoDTO.getPorcentajeMaximoComisionPRR());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "porcentajeMaximoComisionRCI", subRamoDTO.getPorcentajeMaximoComisionRCI());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "porcentajeMaximoComisionRO", subRamoDTO.getPorcentajeMaximoComisionRO());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
						
			return (Long)query.getSingleResult();
			
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SubRamoDTO> getSubRamosInCotizacion(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("finding SubRamoDTO instances in Cotizacion: "
				+ idToCotizacion, Level.INFO, null);
		try {
			final String queryString = "select model from SubRamoDTO as model where model.idTcSubRamo in " +
					"(select distinct toc.idTcSubramo from CoberturaCotizacionDTO as toc where toc.id.idToCotizacion = :idToCotizacion)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SubRamoDTO> getSubRamosPorIncisoCotizacion(
			BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		LogDeMidasEJB3.log("finding SubRamoDTO instances in Cotizacion: "
				+ idToCotizacion, Level.INFO, null);
		try {
			final String queryString = "select model from SubRamoDTO as model where model.idTcSubRamo in "
					+ "(select distinct toc.idTcSubramo from CoberturaCotizacionDTO as toc where toc.id.idToCotizacion = :idToCotizacion "
					+ " and toc.id.numeroInciso= :numeroInciso )";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SubRamoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public SubRamoDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<SubRamoDTO> listRelated(Object id) {
		return this.findAll();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SubRamoDTO> getSubRamosPorTipoPoliza(BigDecimal idToTipoPoliza) {
		LogDeMidasEJB3.log("finding SubRamoDTO instances in TipoPolizaDTO: "
				+ idToTipoPoliza, Level.INFO, null);
		try {
			String queryString = "";
			queryString = " SELECT IDTCSUBRAMO, IDTCRAMO FROM MIDAS.TCSUBRAMO  ";
			queryString += " WHERE IDTCSUBRAMO IN ( ";
			queryString += " 	SELECT DISTINCT COB.IDTCSUBRAMO FROM MIDAS.TOCOBERTURASECCION COBSEC, MIDAS.TOCOBERTURA COB ";
			queryString += "	WHERE COBSEC.IDTOCOBERTURA = COB.IDTOCOBERTURA ";
			queryString += " 	AND COBSEC.IDTOSECCION IN ( ";
			queryString += "		SELECT SEC.IDTOSECCION FROM MIDAS.TOSECCION SEC ";
			queryString += "		WHERE SEC.CLAVEESTATUS <> 3 ";
			queryString += "		AND SEC.IDTOTIPOPOLIZA = " + idToTipoPoliza;
			queryString += "		) ";
			queryString += "	) ";
			
			Query query = entityManager.createNativeQuery(queryString);
			Object result = query.getResultList();
			List<SubRamoDTO> subRamosList = new ArrayList<SubRamoDTO>(1);
			BigDecimal idTcSubRamo = null;
			if(result instanceof List)  {
				List<Object> listaResultados = (List<Object>) result;
				for(Object object : listaResultados){
					Object[] singleResult = (Object[]) object;
					
					if(singleResult[0] instanceof BigDecimal)
						idTcSubRamo = (BigDecimal)singleResult[0];
					else if (singleResult[0] instanceof Long)
						idTcSubRamo = BigDecimal.valueOf((Long)singleResult[0]);
					else if (singleResult[0] instanceof Double)
						idTcSubRamo = BigDecimal.valueOf((Double)singleResult[0]);

					SubRamoDTO subRamoDTO = findById(idTcSubRamo);
					subRamosList.add(subRamoDTO);
				}
			}
			return subRamosList;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}		
	}
	
}