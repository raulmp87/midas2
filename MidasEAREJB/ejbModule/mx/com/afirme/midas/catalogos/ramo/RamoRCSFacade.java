package mx.com.afirme.midas.catalogos.ramo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity Tcramo.
 * 
 * @see mx.com.afirme.midas.catalogos.ramo.RamoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class RamoRCSFacade implements RamoRCSFacadeRemote{

	
	private String claveNegocioDefault;
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved Tcramo entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method. MidasDataSource  
	 * 
	 * @param entity
	 *            Tcramo entity to persist
	 */
	public void save(RamoRCSDTO entity) {
		LogDeMidasEJB3.log("saving Tcramo instance", Level.INFO, null);
		entityManager.persist(entity);
		LogDeMidasEJB3.log("save successful", Level.INFO, null);
	}

	/**
	 * Delete a persistent Tcramo entity.
	 * 
	 * @param entity
	 *            Tcramo entity to delete
	 */
	public void delete(RamoRCSDTO entity) {
		LogDeMidasEJB3.log("deleting Tcramo instance", Level.INFO, null);
		entity = entityManager
				.getReference(RamoRCSDTO.class, entity.getId_ramo());
		entityManager.remove(entity);
		LogDeMidasEJB3.log("delete successful", Level.INFO, null);

	}

	/**
	 * Persist a previously saved Tcramo entity and return it or a copy of it to
	 * the sender. A copy of the Tcramo entity parameter is returned when the
	 * JPA persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            Tcramo entity to update
	 * @return Tcramo the persisted Tcramo entity instance, may not be the same
	 */
	public RamoRCSDTO update(RamoRCSDTO entity) {
		LogDeMidasEJB3.log("updating Tcramo instance", Level.INFO, null);
		RamoRCSDTO result = entityManager.merge(entity);
		LogDeMidasEJB3.log("update successful", Level.INFO, null);
		return result;

	}

	public RamoRCSDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding Tcramo instance with id: " + id,
				Level.INFO, null);
		RamoRCSDTO instance = entityManager.find(RamoRCSDTO.class, id);
		return instance;

	}

	

	
	@SuppressWarnings("unchecked")
	public List<RamoRCSDTO> listarFiltrado(RamoDTO ramoDTO) {
		try {
			String queryString = "select model from RamoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (ramoDTO == null)
				return null;
			if(ramoDTO.getIdTcRamo()!=null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
						sWhere, "idTcRamo", ramoDTO.getIdTcRamo());
			}			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigo", ramoDTO.getCodigo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "descripcion", ramoDTO.getDescripcion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveNegocio", ramoDTO.getClaveNegocio());
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<RamoRCSDTO> listarFiltradoLike(RamoDTO ramoDTO) {
		try {
			String queryString = "select model from RamoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (ramoDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigo", ramoDTO.getCodigo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "descripcion", ramoDTO.getDescripcion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveNegocio", ramoDTO.getClaveNegocio());
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public EntityTransaction getTransaction() {
		EntityTransaction transaccion = entityManager.getTransaction();

		return transaccion;
	}

	

	
	
	public void setClaveNegocioDefault(String claveNegocioDefault) {
		this.claveNegocioDefault = claveNegocioDefault;
	}
	public String getClaveNegocioDefault() {
		return claveNegocioDefault;
	}

	public RamoRCSDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public RamoRCSDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<RamoRCSDTO> listRelated(Object id) {
		return this.findAll();
	}

	@Override
	public List<RamoRCSDTO> findByProperty(String propertyName, Object value) {
		// TODO Apéndice de método generado automáticamente
		return null;
	}
	
	private boolean isEmptyClaveNegocioDefault(){
		return claveNegocioDefault != null && !claveNegocioDefault.isEmpty();
	}

	/**
	 * Find all Tcramo entities.  By default only bring RamoDTO with claveNegocio = D
	 * 
	 * @return List<Tcramo> all Tcramo entities
	 */
	@SuppressWarnings("unchecked")
	public List<RamoRCSDTO> findAll2() {
		LogDeMidasEJB3.log("finding all Tcramo instances", Level.INFO, null);
		final String queryString = "select model from RamoRCSDTO AS model"
		+" where model.descripcion =:descripcion";
		Query query = entityManager.createQuery(queryString);
		if(isEmptyClaveNegocioDefault())
			query.setParameter("descripcion", "1");
		
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		try {
			List<RamoRCSDTO> lista  = query.getResultList();
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}	
		
		
		return null;

	}
	
	
	@SuppressWarnings("unchecked")
	public List<RamoRCSDTO> findAll() {
		LogDeMidasEJB3.log("finding all Tcramo instances", Level.INFO, null);
		final String queryString = "select model from RamoRCSDTO AS model"+ 
		(isEmptyClaveNegocioDefault()?" where model.modulo =:defaultClaveNegocio":" where model.modulo in (1,2)");
		Query query = entityManager.createQuery(queryString);
		if(isEmptyClaveNegocioDefault())
			query.setParameter("defaultClaveNegocio", new BigDecimal(claveNegocioDefault));
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();

	}
}