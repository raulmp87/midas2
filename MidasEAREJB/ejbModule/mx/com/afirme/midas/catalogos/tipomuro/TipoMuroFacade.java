package mx.com.afirme.midas.catalogos.tipomuro;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity TipoMuroDTO.
 * @see .TipoMuroDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class TipoMuroFacade  implements TipoMuroFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved TipoMuroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoMuroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoMuroDTO entity) {
    				LogDeMidasEJB3.log("saving TipoMuroDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent TipoMuroDTO entity.
	  @param entity TipoMuroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoMuroDTO entity) {
    				LogDeMidasEJB3.log("deleting TipoMuroDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(TipoMuroDTO.class, entity.getIdTipoMuro());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved TipoMuroDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoMuroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoMuroDTO entity to update
	 @return TipoMuroDTO the persisted TipoMuroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public TipoMuroDTO update(TipoMuroDTO entity) {
    				LogDeMidasEJB3.log("updating TipoMuroDTO instance", Level.INFO, null);
	        try {
            TipoMuroDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public TipoMuroDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding TipoMuroDTO instance with id: " + id, Level.INFO, null);
	        try {
            TipoMuroDTO instance = entityManager.find(TipoMuroDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all TipoMuroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoMuroDTO property to query
	  @param value the property value to match
	  	  @return List<TipoMuroDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<TipoMuroDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding TipoMuroDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from TipoMuroDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all TipoMuroDTO entities.
	  	  @return List<TipoMuroDTO> all TipoMuroDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoMuroDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all TipoMuroDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from TipoMuroDTO model " +
					"order by model.descripcionTipoMuro";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoMuroDTO> listarFiltrado(TipoMuroDTO tipoMuroDTO){
		try {
			String queryString = "select model from TipoMuroDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (tipoMuroDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoTipoMuro", tipoMuroDTO.getCodigoTipoMuro());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "descripcionTipoMuro", tipoMuroDTO.getDescripcionTipoMuro());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoMuroDTO findById(CatalogoValorFijoId arg0) {
		return null;
	}

	public TipoMuroDTO findById(double arg0) {
		return null;
	}

	public List<TipoMuroDTO> listRelated(Object id) {
		return findAll();
	}
}