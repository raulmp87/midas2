package mx.com.afirme.midas.catalogos.tipoempaque;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity TipoEmpaqueDTO.
 * 
 * @see .TipoEmpaqueDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoEmpaqueFacade implements TipoEmpaqueFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoEmpaqueDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoEmpaqueDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoEmpaqueDTO entity) {
		LogDeMidasEJB3.log("saving TipoEmpaqueDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		} catch (Exception e) {
			System.out.println("Excepcion CACHADAAAAA ================="
					+ e.getMessage());
			System.out.println("Excepcion CACHADAAAAA =================");
		}
	}

	/**
	 * Delete a persistent TipoEmpaqueDTO entity.
	 * 
	 * @param entity
	 *            TipoEmpaqueDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoEmpaqueDTO entity) {
		LogDeMidasEJB3
				.log("deleting TipoEmpaqueDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(TipoEmpaqueDTO.class, entity
					.getIdTipoEmpaque());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoEmpaqueDTO entity and return it or a copy
	 * of it to the sender. A copy of the TipoEmpaqueDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoEmpaqueDTO entity to update
	 * @return TipoEmpaqueDTO the persisted TipoEmpaqueDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoEmpaqueDTO update(TipoEmpaqueDTO entity) {
		LogDeMidasEJB3
				.log("updating TipoEmpaqueDTO instance", Level.INFO, null);
		try {
			TipoEmpaqueDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoEmpaqueDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding TipoEmpaqueDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoEmpaqueDTO instance = entityManager.find(TipoEmpaqueDTO.class,
					id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoEmpaqueDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoEmpaqueDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoEmpaqueDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoEmpaqueDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TipoEmpaqueDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoEmpaqueDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoEmpaqueDTO entities.
	 * 
	 * @return List<TipoEmpaqueDTO> all TipoEmpaqueDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoEmpaqueDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoEmpaqueDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from TipoEmpaqueDTO model " +
					"order by model.descripcionTipoEmpaque";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TipoEmpaqueDTO> listarFiltrado(TipoEmpaqueDTO tipoEmpaqueDTO) {
		try {
			String queryString = "select model from TipoEmpaqueDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (tipoEmpaqueDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigoTipoEmpaque", tipoEmpaqueDTO
							.getCodigoTipoEmpaque());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "descripcionTipoEmpaque", tipoEmpaqueDTO
							.getDescripcionTipoEmpaque());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoEmpaqueDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoEmpaqueDTO findById(double id) {
		return null;
	}

	public List<TipoEmpaqueDTO> listRelated(Object id) {
		return findAll();
	}

}