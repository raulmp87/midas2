package mx.com.afirme.midas.catalogos.equipoelectronico;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.equipoelectronico.EquipoElectronicoDTO;
import mx.com.afirme.midas.catalogos.equipoelectronico.EquipoElectronicoFacadeRemote;
import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity EquipoElectronicoDTO.
 * 
 * @see .EquipoElectronicoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class EquipoElectronicoFacade implements EquipoElectronicoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved EquipoElectronicoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            EquipoElectronicoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(EquipoElectronicoDTO entity) {
		LogDeMidasEJB3.log("saving EquipoElectronicoDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent EquipoElectronicoDTO entity.
	 * 
	 * @param entity
	 *            EquipoElectronicoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(EquipoElectronicoDTO entity) {
		LogDeMidasEJB3.log("deleting EquipoElectronicoDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(EquipoElectronicoDTO.class,
					entity.getIdtctipoequipoelectronico());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved EquipoElectronicoDTO entity and return it or a
	 * copy of it to the sender. A copy of the EquipoElectronicoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            EquipoElectronicoDTO entity to update
	 * @return EquipoElectronicoDTO the persisted EquipoElectronicoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public EquipoElectronicoDTO update(EquipoElectronicoDTO entity) {
		LogDeMidasEJB3.log("updating EquipoElectronicoDTO instance",
				Level.INFO, null);
		try {
			EquipoElectronicoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public EquipoElectronicoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding EquipoElectronicoDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			EquipoElectronicoDTO instance = entityManager.find(
					EquipoElectronicoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all EquipoElectronicoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the EquipoElectronicoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<EquipoElectronicoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<EquipoElectronicoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding EquipoElectronicoDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from EquipoElectronicoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all EquipoElectronicoDTO entities.
	 * 
	 * @return List<EquipoElectronicoDTO> all EquipoElectronicoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<EquipoElectronicoDTO> findAll() {
		LogDeMidasEJB3.log("finding all EquipoElectronicoDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from EquipoElectronicoDTO model " +
					"order by model.descripciontipoeqelectro";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RamoDTO> listarFiltrado(
			EquipoElectronicoDTO equipoElectronicoDTO) {
		try {
			String queryString = "select model from EquipoElectronicoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (equipoElectronicoDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigotipoequipoelectronico", equipoElectronicoDTO
							.getCodigotipoequipoelectronico());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "descripciontipoeqelectro", equipoElectronicoDTO
							.getDescripciontipoeqelectro());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public EquipoElectronicoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public EquipoElectronicoDTO findById(double id) {
		return null;
	}

	public List<EquipoElectronicoDTO> listRelated(Object id) {
		return findAll();
	}

}