package mx.com.afirme.midas.catalogos.equipoelectronico;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.equipoelectronico.SubtipoEquipoElectronicoDTO;
import mx.com.afirme.midas.catalogos.equipoelectronico.SubtipoEquipoElectronicoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity SubtipoEquipoElectronicoDTO.
 * @see .SubtipoEquipoElectronicoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class SubtipoEquipoElectronicoFacade  implements SubtipoEquipoElectronicoFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved SubtipoEquipoElectronicoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SubtipoEquipoElectronicoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(SubtipoEquipoElectronicoDTO entity) {
    				LogDeMidasEJB3.log("saving SubtipoEquipoElectronicoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent SubtipoEquipoElectronicoDTO entity.
	  @param entity SubtipoEquipoElectronicoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SubtipoEquipoElectronicoDTO entity) {
    				LogDeMidasEJB3.log("deleting SubtipoEquipoElectronicoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(SubtipoEquipoElectronicoDTO.class, entity.getIdtcsubtipoequipoelectronico());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved SubtipoEquipoElectronicoDTO entity and return it or a copy of it to the sender. 
	 A copy of the SubtipoEquipoElectronicoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SubtipoEquipoElectronicoDTO entity to update
	 @return SubtipoEquipoElectronicoDTO the persisted SubtipoEquipoElectronicoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public SubtipoEquipoElectronicoDTO update(SubtipoEquipoElectronicoDTO entity) {
    				LogDeMidasEJB3.log("updating SubtipoEquipoElectronicoDTO instance", Level.INFO, null);
	        try {
            SubtipoEquipoElectronicoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public SubtipoEquipoElectronicoDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding SubtipoEquipoElectronicoDTO instance with id: " + id, Level.INFO, null);
	        try {
            SubtipoEquipoElectronicoDTO instance = entityManager.find(SubtipoEquipoElectronicoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all SubtipoEquipoElectronicoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SubtipoEquipoElectronicoDTO property to query
	  @param value the property value to match
	  	  @return List<SubtipoEquipoElectronicoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<SubtipoEquipoElectronicoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding SubtipoEquipoElectronicoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from SubtipoEquipoElectronicoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all SubtipoEquipoElectronicoDTO entities.
	  	  @return List<SubtipoEquipoElectronicoDTO> all SubtipoEquipoElectronicoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubtipoEquipoElectronicoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all SubtipoEquipoElectronicoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from SubtipoEquipoElectronicoDTO model " +
					"order by model.descripcionsubtipoeqelectro";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	public SubtipoEquipoElectronicoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public SubtipoEquipoElectronicoDTO findById(double id) {
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<SubtipoEquipoElectronicoDTO> listRelated(Object id) {
		LogDeMidasEJB3.log("finding all SubtipoEquipoElectronicoDTO related with id: "+id, Level.INFO, null);
		try {
			final String queryString = "select model from SubtipoEquipoElectronicoDTO model where " +
					"model.equipoElectronicoDTO.idtctipoequipoelectronico = (select seq.equipoElectronicoDTO.idtctipoequipoelectronico " +
					"from SubtipoEquipoElectronicoDTO seq where seq.idtcsubtipoequipoelectronico = :id) order by model.descripcionsubtipoeqelectro";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("id", id);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
}