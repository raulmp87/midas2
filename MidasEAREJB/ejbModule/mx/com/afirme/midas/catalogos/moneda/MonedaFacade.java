package mx.com.afirme.midas.catalogos.moneda;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity MonedaDTO.
 * 
 * @see .MonedaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class MonedaFacade implements MonedaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved MonedaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            MonedaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(MonedaDTO entity) {
		LogDeMidasEJB3.log("saving MonedaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent MonedaDTO entity.
	 * 
	 * @param entity
	 *            MonedaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(MonedaDTO entity) {
		LogDeMidasEJB3.log("deleting MonedaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(MonedaDTO.class, entity.getIdTcMoneda());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved MonedaDTO entity and return it or a copy of it to
	 * the sender. A copy of the MonedaDTO entity parameter is returned when the
	 * JPA persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            MonedaDTO entity to update
	 * @return MonedaDTO the persisted MonedaDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public MonedaDTO update(MonedaDTO entity) {
		LogDeMidasEJB3.log("updating MonedaDTO instance", Level.INFO, null);
		try {
			MonedaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public MonedaDTO findById(Short id) {
		LogDeMidasEJB3.log("finding MonedaDTO instance with id: " + id, Level.INFO, null);
		try {
			MonedaDTO instance = entityManager.find(MonedaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all MonedaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the MonedaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<MonedaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<MonedaDTO> findByProperty(String propertyName, final Object value) {
		LogDeMidasEJB3.log("finding MonedaDTO instance with property: " + propertyName
				+ ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from MonedaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all MonedaDTO entities.
	 * 
	 * @return List<MonedaDTO> all MonedaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<MonedaDTO> findAll() {
		LogDeMidasEJB3.log("finding all MonedaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from MonedaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<MonedaDTO> listarMonedasAsociadas(BigDecimal idToProducto) {
		LogDeMidasEJB3.log("finding all MonedaDTO instances related with Producto id: "+idToProducto, Level.INFO, null);
		try {
			final String queryString = "select model from MonedaDTO model where model.idTcMoneda in " +
				"(select m.id.idMoneda from MonedaProductoDTO m where m.id.idToProducto = :idToProducto)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToProducto", idToProducto);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public List<MonedaDTO> listarMonedasPorAsociar(BigDecimal idToProducto) {
		String queryString = "select model from MonedaDTO as model";
		queryString += " where model.idTcMoneda not in (select d.id.idMoneda from MonedaProductoDTO d where d.id.idToProducto = :idToProducto)";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToProducto", idToProducto);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	/**
	 * Find all MonedaDTO entities related with a TipoPoliza Entity.
	 * @param BigDecimal idToTipoPoliza
	 * @return List<MonedaDTO> all MonedaDTO entities not related with the tipoPoliza entity
	 */
	@SuppressWarnings("unchecked")
	public List<MonedaDTO> listarMonedasAsociadasTipoPoliza(BigDecimal idToTipoPoliza) {
		LogDeMidasEJB3.log("finding all MonedaDTO instances related with TipoPoliza id: "+idToTipoPoliza, Level.INFO, null);
		try {
			final String queryString = "select model from MonedaDTO model where model.idTcMoneda in " +
				"(select m.id.idMoneda from MonedaTipoPolizaDTO m where m.id.idToTipoPoliza = :idToTipoPoliza)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	/**
	 * Find all MonedaDTO entities not related with a TipoPoliza.
	 * @param BigDecimal idToTipoPoliza
	 * 
	 * @return List<MonedaDTO> all MonedaDTO entities not related with the tipoPoliza entity
	 */
	@SuppressWarnings("unchecked")
	public List<MonedaDTO> findNotRelatedTipoPoliza(BigDecimal idToTipoPoliza,BigDecimal idToProducto) {
		LogDeMidasEJB3.log("finding all MonedaDTO instances not related with TipoPoliza id: "+idToTipoPoliza+ " owned to idToProducto: "+idToProducto, Level.INFO, null);
		try {
			final String queryString = "select model from MonedaDTO model where model.idTcMoneda not in " +
				"(select m.id.idMoneda from MonedaTipoPolizaDTO m where m.id.idToTipoPoliza = :idToTipoPoliza) " +
				"and model.idTcMoneda in (select mp.id.idMoneda from MonedaProductoDTO mp where mp.id.idToProducto = :idToProducto )";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setParameter("idToProducto", idToProducto);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find all MonedaDTO entities related with a TipoPoliza.
	 * @param BigDecimal idToTipoPoliza
	 * @return List<MonedaDTO> all MonedaDTO entities not related with the tipoPoliza entity
	 */
	@SuppressWarnings("unchecked")
	public List<MonedaDTO> findRelatedTipoPoliza(BigDecimal idToTipoPoliza) {
		LogDeMidasEJB3.log("finding all MonedaDTO instances related with TipoPoliza id: "+idToTipoPoliza, Level.INFO, null);
		try {
			final String queryString = "select model from MonedaDTO model where model.idTcMoneda in " +
				"(select m.id.idMoneda from MonedaTipoPolizaDTO m where m.id.idToTipoPoliza = :idToTipoPoliza) ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public MonedaDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding MonedaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			MonedaDTO instance = entityManager.find(MonedaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	public MonedaDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public MonedaDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<MonedaDTO> listRelated(Object id) {
		return this.findAll();
	}
}