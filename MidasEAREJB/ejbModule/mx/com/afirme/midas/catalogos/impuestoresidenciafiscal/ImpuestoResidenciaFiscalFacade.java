package mx.com.afirme.midas.catalogos.impuestoresidenciafiscal;
// default package

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ImpuestoResidenciaFiscalDTO.
 * @see .ImpuestoResidenciaFiscal
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ImpuestoResidenciaFiscalFacade  implements ImpuestoResidenciaFiscalFacadeRemote {
	//property constants
	public static final String DESCRIPCION = "descripcion";
	public static final String PORCENTAJE = "porcentaje";





    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ImpuestoResidenciaFiscalDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ImpuestoResidenciaFiscalDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ImpuestoResidenciaFiscalDTO entity) {
    				LogDeMidasEJB3.log("saving ImpuestoResidenciaFiscalDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ImpuestoResidenciaFiscalDTO entity.
	  @param entity ImpuestoResidenciaFiscalDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ImpuestoResidenciaFiscalDTO entity) {
    				LogDeMidasEJB3.log("deleting ImpuestoResidenciaFiscalDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ImpuestoResidenciaFiscalDTO.class, entity.getIdTcImpuestoResidenciaFiscal());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ImpuestoResidenciaFiscalDTO entity and return it or a copy of it to the sender. 
	 A copy of the ImpuestoResidenciaFiscalDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ImpuestoResidenciaFiscalDTO entity to update
	 @return ImpuestoResidenciaFiscalDTO the persisted ImpuestoResidenciaFiscalDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ImpuestoResidenciaFiscalDTO update(ImpuestoResidenciaFiscalDTO entity) {
    				LogDeMidasEJB3.log("updating ImpuestoResidenciaFiscalDTO instance", Level.INFO, null);
	        try {
            ImpuestoResidenciaFiscalDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ImpuestoResidenciaFiscalDTO findById( Long id) {
    				LogDeMidasEJB3.log("finding ImpuestoResidenciaFiscalDTO instance with id: " + id, Level.INFO, null);
	        try {
            ImpuestoResidenciaFiscalDTO instance = entityManager.find(ImpuestoResidenciaFiscalDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ImpuestoResidenciaFiscalDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ImpuestoResidenciaFiscalDTO property to query
	  @param value the property value to match
	  	  @return List<ImpuestoResidenciaFiscalDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ImpuestoResidenciaFiscalDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ImpuestoResidenciaFiscalDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ImpuestoResidenciaFiscalDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	public List<ImpuestoResidenciaFiscalDTO> findByDescripcion(Object descripcion
	) {
		return findByProperty(DESCRIPCION, descripcion
		);
	}
	
	public List<ImpuestoResidenciaFiscalDTO> findByPorcentaje(Object porcentaje
	) {
		return findByProperty(PORCENTAJE, porcentaje
		);
	}
	
	
	/**
	 * Find all ImpuestoResidenciaFiscalDTO entities.
	  	  @return List<ImpuestoResidenciaFiscalDTO> all ImpuestoResidenciaFiscalDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ImpuestoResidenciaFiscalDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ImpuestoResidenciaFiscalDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ImpuestoResidenciaFiscalDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}