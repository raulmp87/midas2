package mx.com.afirme.midas.catalogos.rangosumaasegurada;
// default package

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.rangosumaasegurada.RangoSumaAseguradaDTO;
import mx.com.afirme.midas.catalogos.rangosumaasegurada.RangoSumaAseguradaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity RangoSumaAseguradaDTO.
 * @see .RangoSumaAseguradaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class RangoSumaAseguradaFacade  implements RangoSumaAseguradaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved RangoSumaAseguradaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity RangoSumaAseguradaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(RangoSumaAseguradaDTO entity) {
    				LogDeMidasEJB3.log("saving RangoSumaAseguradaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent RangoSumaAseguradaDTO entity.
	  @param entity RangoSumaAseguradaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(RangoSumaAseguradaDTO entity) {
    				LogDeMidasEJB3.log("deleting RangoSumaAseguradaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(RangoSumaAseguradaDTO.class, entity.getIdTcRangoSumaAsegurada());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved RangoSumaAseguradaDTO entity and return it or a copy of it to the sender. 
	 A copy of the RangoSumaAseguradaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity RangoSumaAseguradaDTO entity to update
	 @return RangoSumaAseguradaDTO the persisted RangoSumaAseguradaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public RangoSumaAseguradaDTO update(RangoSumaAseguradaDTO entity) {
    				LogDeMidasEJB3.log("updating RangoSumaAseguradaDTO instance", Level.INFO, null);
	        try {
            RangoSumaAseguradaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public RangoSumaAseguradaDTO findById( int id) {
    				LogDeMidasEJB3.log("finding RangoSumaAseguradaDTO instance with id: " + id, Level.INFO, null);
	        try {
            RangoSumaAseguradaDTO instance = entityManager.find(RangoSumaAseguradaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all RangoSumaAseguradaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the RangoSumaAseguradaDTO property to query
	  @param value the property value to match
	  	  @return List<RangoSumaAseguradaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<RangoSumaAseguradaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding RangoSumaAseguradaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from RangoSumaAseguradaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all RangoSumaAseguradaDTO entities.
	  	  @return List<RangoSumaAseguradaDTO> all RangoSumaAseguradaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RangoSumaAseguradaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all RangoSumaAseguradaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from RangoSumaAseguradaDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}