package mx.com.afirme.midas.catalogos.eventocatastrofico;
// default package


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity EventoCatastroficoDTO.
 * @see .EventoCatastroficoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class EventoCatastroficoFacade  implements EventoCatastroficoFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved EventoCatastroficoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EventoCatastroficoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EventoCatastroficoDTO entity) {
    				LogDeMidasEJB3.log("saving EventoCatastroficoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent EventoCatastroficoDTO entity.
	  @param entity EventoCatastroficoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EventoCatastroficoDTO entity) {
    				LogDeMidasEJB3.log("deleting EventoCatastroficoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(EventoCatastroficoDTO.class, entity.getIdTcEventoCatastrofico());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved EventoCatastroficoDTO entity and return it or a copy of it to the sender. 
	 A copy of the EventoCatastroficoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EventoCatastroficoDTO entity to update
	 @return EventoCatastroficoDTO the persisted EventoCatastroficoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public EventoCatastroficoDTO update(EventoCatastroficoDTO entity) {
    				LogDeMidasEJB3.log("updating EventoCatastroficoDTO instance", Level.INFO, null);
	        try {
            EventoCatastroficoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public EventoCatastroficoDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding EventoCatastroficoDTO instance with id: " + id, Level.INFO, null);
	        try {
            EventoCatastroficoDTO instance = entityManager.find(EventoCatastroficoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all EventoCatastroficoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EventoCatastroficoDTO property to query
	  @param value the property value to match
	  	  @return List<EventoCatastroficoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<EventoCatastroficoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding EventoCatastroficoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from EventoCatastroficoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all EventoCatastroficoDTO entities.
	  	  @return List<EventoCatastroficoDTO> all EventoCatastroficoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<EventoCatastroficoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all EventoCatastroficoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from EventoCatastroficoDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	public EventoCatastroficoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public EventoCatastroficoDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<EventoCatastroficoDTO> listRelated(Object id) {
		return this.findAll();
	}
	
	@SuppressWarnings("unchecked")
	public List<EventoCatastroficoDTO> listarFiltrado(EventoCatastroficoDTO eventoCatastroficoDTO) {
		try {
			String queryString = "select model from EventoCatastroficoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (eventoCatastroficoDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigoEvento", eventoCatastroficoDTO.getCodigoEvento());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "descripcionEvento", eventoCatastroficoDTO.getDescripcionEvento());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
}