package mx.com.afirme.midas.catalogos.codigopostalzonasismo;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity CodigoPostalZonaSismoDTO.
 * 
 * @see .CodigoPostalZonaSismoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CodigoPostalZonaSismoFacade implements
		CodigoPostalZonaSismoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved CodigoPostalZonaSismoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CodigoPostalZonaSismoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CodigoPostalZonaSismoDTO entity) {
		LogDeMidasEJB3.log("saving CodigoPostalZonaSismoDTO instance",
				Level.FINE, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent CodigoPostalZonaSismoDTO entity.
	 * 
	 * @param entity
	 *            CodigoPostalZonaSismoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CodigoPostalZonaSismoDTO entity) {
		LogDeMidasEJB3.log("deleting CodigoPostalZonaSismoDTO instance",
				Level.FINE, null);
		try {
			entity = entityManager.getReference(CodigoPostalZonaSismoDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved CodigoPostalZonaSismoDTO entity and return it
	 * or a copy of it to the sender. A copy of the CodigoPostalZonaSismoDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CodigoPostalZonaSismoDTO entity to update
	 * @return CodigoPostalZonaSismoDTO the persisted CodigoPostalZonaSismoDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CodigoPostalZonaSismoDTO update(CodigoPostalZonaSismoDTO entity) {
		LogDeMidasEJB3.log("updating CodigoPostalZonaSismoDTO instance",
				Level.FINE, null);
		try {
			CodigoPostalZonaSismoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.FINE, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CodigoPostalZonaSismoDTO findById(CodigoPostalZonaSismoId id) {
		LogDeMidasEJB3.log(
				"finding CodigoPostalZonaSismoDTO instance with id: " + id,
				Level.FINE, null);
		try {
			CodigoPostalZonaSismoDTO instance = entityManager.find(
					CodigoPostalZonaSismoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CodigoPostalZonaSismoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the CodigoPostalZonaSismoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CodigoPostalZonaSismoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CodigoPostalZonaSismoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding CodigoPostalZonaSismoDTO instance with property: "
						+ propertyName + ", value: " + value, Level.FINE, null);
		try {
			final String queryString = "select model from CodigoPostalZonaSismoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CodigoPostalZonaSismoDTO entities.
	 * 
	 * @return List<CodigoPostalZonaSismoDTO> all CodigoPostalZonaSismoDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<CodigoPostalZonaSismoDTO> findAll() {
		LogDeMidasEJB3.log("finding all CodigoPostalZonaSismoDTO instances",
				Level.FINE, null);
		try {
			final String queryString = "select model from CodigoPostalZonaSismoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CodigoPostalZonaSismoDTO> listarFiltrado(
			CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO) {
		try {
			String queryString = "select model from CodigoPostalZonaSismoDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (codigoPostalZonaSismoDTO == null) {
				return null;
			}
			
			BigDecimal idZonaSismo = null;
			
			if (codigoPostalZonaSismoDTO.getNombreColonia() != null) {
				idZonaSismo = new BigDecimal(codigoPostalZonaSismoDTO.getNombreColonia());
			}
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "zonaSismoDTO.idZonaSismo", idZonaSismo, "idZonaSismo");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.codigoPostal", codigoPostalZonaSismoDTO
							.getCodigoPostal(), "codigoPostal");

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if(codigoPostalZonaSismoDTO.getPrimerRegistroACargar() != null && codigoPostalZonaSismoDTO.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(codigoPostalZonaSismoDTO.getPrimerRegistroACargar().intValue());
				query.setMaxResults(codigoPostalZonaSismoDTO.getNumeroMaximoRegistrosACargar().intValue());
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
			
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public CodigoPostalZonaSismoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding CodigoPostalZonaSismoDTO instance with id: " + id,
				Level.FINE, null);
		try {
			CodigoPostalZonaSismoDTO instance = entityManager.find(CodigoPostalZonaSismoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CodigoPostalZonaSismoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public CodigoPostalZonaSismoDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<CodigoPostalZonaSismoDTO> listRelated(BigDecimal id) {
		return this.findAll();
	}

	@SuppressWarnings("unchecked")
	public Long obtenerTotalFiltrado(
			CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO) {
		try {
			String queryString = "select count(model) from CodigoPostalZonaSismoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (codigoPostalZonaSismoDTO == null) {
				return null;
			}
			
			BigDecimal idZonaSismo = null;
			
			if (codigoPostalZonaSismoDTO.getNombreColonia() != null) {
				idZonaSismo = new BigDecimal(codigoPostalZonaSismoDTO.getNombreColonia());
			}
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "zonaSismoDTO.idZonaSismo", idZonaSismo, "idZonaSismo");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.codigoPostal", codigoPostalZonaSismoDTO
							.getCodigoPostal(), "codigoPostal");

			if (Utilerias.esAtributoQueryValido(sWhere)) {
				queryString = queryString.concat(" where ").concat(sWhere);
			}
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			return (Long)query.getSingleResult();
			
		} catch (RuntimeException re) {
			throw re;
		}
	}
}