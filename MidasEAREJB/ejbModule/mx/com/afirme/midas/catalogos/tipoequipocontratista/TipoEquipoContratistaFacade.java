package mx.com.afirme.midas.catalogos.tipoequipocontratista;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity TipoEquipoContratistaDTO.
 * 
 * @see .TipoEquipoContratistaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoEquipoContratistaFacade implements
		TipoEquipoContratistaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoEquipoContratistaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            TipoEquipoContratistaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoEquipoContratistaDTO entity) {
		LogDeMidasEJB3.log("saving TipoEquipoContratistaDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoEquipoContratistaDTO entity.
	 * 
	 * @param entity
	 *            TipoEquipoContratistaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoEquipoContratistaDTO entity) {
		LogDeMidasEJB3.log("deleting TipoEquipoContratistaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(TipoEquipoContratistaDTO.class,
					entity.getIdTipoEquipoContratista());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoEquipoContratistaDTO entity and return it
	 * or a copy of it to the sender. A copy of the TipoEquipoContratistaDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoEquipoContratistaDTO entity to update
	 * @return TipoEquipoContratistaDTO the persisted TipoEquipoContratistaDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoEquipoContratistaDTO update(TipoEquipoContratistaDTO entity) {
		LogDeMidasEJB3.log("updating TipoEquipoContratistaDTO instance",
				Level.INFO, null);
		try {
			TipoEquipoContratistaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoEquipoContratistaDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding TipoEquipoContratistaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoEquipoContratistaDTO instance = entityManager.find(
					TipoEquipoContratistaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoEquipoContratistaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the TipoEquipoContratistaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoEquipoContratistaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoEquipoContratistaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding TipoEquipoContratistaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoEquipoContratistaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoEquipoContratistaDTO entities.
	 * 
	 * @return List<TipoEquipoContratistaDTO> all TipoEquipoContratistaDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoEquipoContratistaDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoEquipoContratistaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from TipoEquipoContratistaDTO model " +
					"order by model.descripcionTipoEqContr";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TipoEquipoContratistaDTO> listarFiltrado(
			TipoEquipoContratistaDTO tipoEquipoContratistaDTO) {

		try {
			String queryString = "select model from TipoEquipoContratistaDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listarParametrosValidos = new ArrayList<HashMap>();

			if (tipoEquipoContratistaDTO == null)
				return null;

			sWhere = Utilerias.agregaParametroQuery(listarParametrosValidos,
					sWhere, "codigoTipoEquipoContratista",
					tipoEquipoContratistaDTO.getCodigoTipoEquipoContratista());
			sWhere = Utilerias.agregaParametroQuery(listarParametrosValidos,
					sWhere, "descripcionTipoEqContr", tipoEquipoContratistaDTO
							.getDescripcionTipoEqContr());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listarParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoEquipoContratistaDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoEquipoContratistaDTO findById(double id) {
		return null;
	}

	public List<TipoEquipoContratistaDTO> listRelated(Object id) {
		return findAll();
	}
}