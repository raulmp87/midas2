package mx.com.afirme.midas.catalogos.tipoequipocontratista;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity SubtipoEquipoContratistaDTO.
 * 
 * @see .SubtipoEquipoContratistaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SubtipoEquipoContratistaFacade implements
		SubtipoEquipoContratistaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * SubtipoEquipoContratistaDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            SubtipoEquipoContratistaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubtipoEquipoContratistaDTO entity) {
		LogDeMidasEJB3.log("saving SubtipoEquipoContratistaDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SubtipoEquipoContratistaDTO entity.
	 * 
	 * @param entity
	 *            SubtipoEquipoContratistaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubtipoEquipoContratistaDTO entity) {
		LogDeMidasEJB3.log("deleting SubtipoEquipoContratistaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					SubtipoEquipoContratistaDTO.class, entity
							.getIdSubtipoEquipoContratista());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SubtipoEquipoContratistaDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * SubtipoEquipoContratistaDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            SubtipoEquipoContratistaDTO entity to update
	 * @return SubtipoEquipoContratistaDTO the persisted
	 *         SubtipoEquipoContratistaDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubtipoEquipoContratistaDTO update(SubtipoEquipoContratistaDTO entity) {
		LogDeMidasEJB3.log("updating SubtipoEquipoContratistaDTO instance",
				Level.INFO, null);
		try {
			SubtipoEquipoContratistaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SubtipoEquipoContratistaDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding SubtipoEquipoContratistaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SubtipoEquipoContratistaDTO instance = entityManager.find(
					SubtipoEquipoContratistaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubtipoEquipoContratistaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SubtipoEquipoContratistaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubtipoEquipoContratistaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SubtipoEquipoContratistaDTO> findByProperty(
			String propertyName, final Object value) {
		LogDeMidasEJB3.log(
				"finding SubtipoEquipoContratistaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SubtipoEquipoContratistaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubtipoEquipoContratistaDTO entities.
	 * 
	 * @return List<SubtipoEquipoContratistaDTO> all SubtipoEquipoContratistaDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubtipoEquipoContratistaDTO> findAll() {
		LogDeMidasEJB3.log("finding all SubtipoEquipoContratistaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from SubtipoEquipoContratistaDTO model " +
					"order by model.descripcionSubtipoEqCont";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SubtipoEquipoContratistaDTO> listarFiltrado(
			SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO) {

		try {
			String queryString = "select model from SubtipoEquipoContratistaDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listarParametrosValidos = new ArrayList<HashMap>();

			if (subtipoEquipoContratistaDTO == null)
				return null;

			sWhere = Utilerias.agregaParametroQuery(listarParametrosValidos,
					sWhere, "codigoSubtipoEquipoContratista",
					subtipoEquipoContratistaDTO
							.getCodigoSubtipoEquipoContratista());
			sWhere = Utilerias.agregaParametroQuery(listarParametrosValidos,
					sWhere, "descripcionSubtipoEqCont",
					subtipoEquipoContratistaDTO.getDescripcionSubtipoEqCont());
			sWhere = Utilerias.agregaParametroQuery(listarParametrosValidos,
					sWhere, "claveAutorizacion", subtipoEquipoContratistaDTO
							.getClaveAutorizacion());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listarParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public SubtipoEquipoContratistaDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public SubtipoEquipoContratistaDTO findById(double id) {
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<SubtipoEquipoContratistaDTO> listRelated(Object id) {
		LogDeMidasEJB3.log("finding all SubtipoEquipoContratistaDTO related with id: "+id, Level.INFO, null);
		try {
			final String queryString = "select model from SubtipoEquipoContratistaDTO model where " +
					"model.tipoEquipoContratistaDTO.idTipoEquipoContratista = (select sg.tipoEquipoContratistaDTO.idTipoEquipoContratista " +
					"from SubtipoEquipoContratistaDTO sg where sg.idSubtipoEquipoContratista = :id) order by model.descripcionSubtipoEqCont";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("id", (BigDecimal)id);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}