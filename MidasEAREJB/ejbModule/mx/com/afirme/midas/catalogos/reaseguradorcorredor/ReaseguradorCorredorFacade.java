package mx.com.afirme.midas.catalogos.reaseguradorcorredor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity Tcreaseguradorcorredor.
 * 
 * @see mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ReaseguradorCorredorFacade implements
ReaseguradorCorredorFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved Tcreaseguradorcorredor
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Tcreaseguradorcorredor entity to persist
	 */
	public void save(ReaseguradorCorredorDTO entity) {
		LogDeMidasEJB3.log("saving Tcreaseguradorcorredor instance",
				Level.INFO, null);
		entityManager.persist(entity);
		LogDeMidasEJB3.log("save successful", Level.INFO, null);
	}

	/**
	 * Delete a persistent Tcreaseguradorcorredor entity.
	 * 
	 * @param entity
	 *            Tcreaseguradorcorredor entity to delete
	 */
	public void delete(ReaseguradorCorredorDTO entity) {
		LogDeMidasEJB3.log("deleting Tcreaseguradorcorredor instance",
				Level.INFO, null);
		entity = entityManager.getReference(ReaseguradorCorredorDTO.class,
				entity.getIdtcreaseguradorcorredor());
		entityManager.remove(entity);
		LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	}

	/**
	 * Persist a previously saved Tcreaseguradorcorredor entity and return it or
	 * a copy of it to the sender. A copy of the Tcreaseguradorcorredor entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            Tcreaseguradorcorredor entity to update
	 * @return Tcreaseguradorcorredor the persisted Tcreaseguradorcorredor
	 *         entity instance, may not be the same
	 */
	public ReaseguradorCorredorDTO update(ReaseguradorCorredorDTO entity) {
		ReaseguradorCorredorDTO result = entityManager.merge(entity);
		LogDeMidasEJB3.log("update successful", Level.INFO, null);
		return result;
	}

	public ReaseguradorCorredorDTO findById(BigDecimal id) {
		ReaseguradorCorredorDTO instance = entityManager.find(
				ReaseguradorCorredorDTO.class, id);
		return instance;
	}

	/**
	 * Find all Tcreaseguradorcorredor entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Tcreaseguradorcorredor property to query
	 * @param value
	 *            the property value to match
	 * @return List<Tcreaseguradorcorredor> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguradorCorredorDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding Tcreaseguradorcorredor instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		final String queryString = "select model from ReaseguradorCorredorDTO model where model."
				+ propertyName + "= :propertyValue";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("propertyValue", value);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	/**
	 * Find all Tcreaseguradorcorredor entities.
	 * 
	 * @return List<Tcreaseguradorcorredor> all Tcreaseguradorcorredor entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguradorCorredorDTO> findAll() {
		final String queryString = "select model from ReaseguradorCorredorDTO as model ORDER BY model.nombre";
		Query query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<ReaseguradorCorredorDTO> listarFiltrado(ReaseguradorCorredorDTO reaseguradorCorredorDTO){
		try {
			String queryString = "select model from ReaseguradorCorredorDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (reaseguradorCorredorDTO == null)
				return null;
			sWhere = "model.estatus>0 ";
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombre", reaseguradorCorredorDTO.getNombre());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "ubicacion", reaseguradorCorredorDTO.getUbicacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "correoelectronico", reaseguradorCorredorDTO.getCorreoelectronico());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "cnfs", reaseguradorCorredorDTO.getCnfs());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "tipo", reaseguradorCorredorDTO.getTipo());
			if(reaseguradorCorredorDTO.getEstatus()!=null)
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "estatus", reaseguradorCorredorDTO.getEstatus().toBigInteger().toString());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombrecorto", reaseguradorCorredorDTO.getNombrecorto() );
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "procedencia", reaseguradorCorredorDTO.getProcedencia());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "ciudad", reaseguradorCorredorDTO.getCiudad());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "estado", reaseguradorCorredorDTO.getEstado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "pais", reaseguradorCorredorDTO.getPais());
			if(reaseguradorCorredorDTO.getTelefonofijo()!=null)
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "telefonofijo", reaseguradorCorredorDTO.getTelefonofijo());
			if(reaseguradorCorredorDTO.getTelefonomovil()!=null)
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "telefonomovil", reaseguradorCorredorDTO.getTelefonomovil());
			
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			queryString = queryString + " ORDER BY model.nombre";
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public ReaseguradorCorredorDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public ReaseguradorCorredorDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<ReaseguradorCorredorDTO> listRelated(Object id) {
		return this.findAll();
	}
}