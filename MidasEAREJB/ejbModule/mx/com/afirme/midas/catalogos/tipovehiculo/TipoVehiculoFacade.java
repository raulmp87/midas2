package mx.com.afirme.midas.catalogos.tipovehiculo;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity TipoVehiculoDTO.
 * @see .TipoVehiculoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class TipoVehiculoFacade  implements TipoVehiculoFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved TipoVehiculoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoVehiculoDTO entity) {
    				LogUtil.log("saving TipoVehiculoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent TipoVehiculoDTO entity.
	  @param entity TipoVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoVehiculoDTO entity) {
    				LogUtil.log("deleting TipoVehiculoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(TipoVehiculoDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved TipoVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoVehiculoDTO entity to update
	 @return TipoVehiculoDTO the persisted TipoVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public TipoVehiculoDTO update(TipoVehiculoDTO entity) {
    				LogUtil.log("updating TipoVehiculoDTO instance", Level.INFO, null);
	        try {
            TipoVehiculoDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public TipoVehiculoDTO findById( BigDecimal id) {
    				LogUtil.log("finding TipoVehiculoDTO instance with id: " + id, Level.INFO, null);
	        try {
            TipoVehiculoDTO instance = entityManager.find(TipoVehiculoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all TipoVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<TipoVehiculoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<TipoVehiculoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding TipoVehiculoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from TipoVehiculoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all TipoVehiculoDTO entities.
	  	  @return List<TipoVehiculoDTO> all TipoVehiculoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoVehiculoDTO> findAll(
		) {
					LogUtil.log("finding all TipoVehiculoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from TipoVehiculoDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TipoVehiculoDTO> listarFiltrado(TipoVehiculoDTO tipoVehiculoDTO){
		try {
			String queryString = "select model from TipoVehiculoDTO AS model ";
			String sWhere = "";
			Query query;
			@SuppressWarnings("rawtypes")
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (tipoVehiculoDTO == null)
				return null;
			if(tipoVehiculoDTO.getId()!=null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idTcTipoVehiculo", tipoVehiculoDTO.getId());
//				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.claveTipoBien", tipoVehiculoDTO.getId());
			}
			if(tipoVehiculoDTO.getTipoBienAutosDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoBienAutosDTO.claveTipoBien", tipoVehiculoDTO.getTipoBienAutosDTO().getClaveTipoBien());
			}
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoTipoVehiculo", tipoVehiculoDTO.getCodigoTipoVehiculo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionTipoVehiculo", tipoVehiculoDTO.getDescripcionTipoVehiculo());
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoVehiculoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoVehiculoDTO findById(double id) {
		return findById(new BigDecimal(""+id));
		
	}

	public List<TipoVehiculoDTO> listRelated(Object id) {
		return null;
	}


}