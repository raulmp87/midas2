package mx.com.afirme.midas.catalogos.tipocambio;
// default package

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity Tctipocambiomensual.
 * 
 * @see .Tctipocambiomensual
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoCambioMidasFacade implements TipoCambioFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved Tctipocambiomensual
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Tctipocambiomensual entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoCambioMensualDTO entity) {
		LogUtil.log("saving Tctipocambiomensual instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent Tctipocambiomensual entity.
	 * 
	 * @param entity
	 *            Tctipocambiomensual entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoCambioMensualDTO entity) {
		LogUtil.log("deleting Tctipocambiomensual instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(TipoCambioMensualDTO.class,
					entity.getIdtctipocambiomensual());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved Tctipocambiomensual entity and return it or a
	 * copy of it to the sender. A copy of the Tctipocambiomensual entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            Tctipocambiomensual entity to update
	 * @return Tctipocambiomensual the persisted Tctipocambiomensual entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoCambioMensualDTO update(TipoCambioMensualDTO entity) {
		LogUtil.log("updating Tctipocambiomensual instance", Level.INFO, null);
		try {
			TipoCambioMensualDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoCambioMensualDTO findById(Short id) {
		LogUtil.log("finding Tctipocambiomensual instance with id: " + id,
				Level.INFO, null);
		try {
			TipoCambioMensualDTO instance = entityManager.find(
					TipoCambioMensualDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Tctipocambiomensual entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Tctipocambiomensual property to query
	 * @param value
	 *            the property value to match
	 * @return List<Tctipocambiomensual> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoCambioMensualDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding Tctipocambiomensual instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from MIDAS.Tctipocambiomensual model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Tctipocambiomensual entities.
	 * 
	 * @return List<Tctipocambiomensual> all Tctipocambiomensual entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoCambioMensualDTO> findAll() {
		LogUtil.log("finding all Tctipocambiomensual instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from MIDAS.Tctipocambiomensual model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actualizarTipoCambioMensual(Date fechaInicial, Date fechaFinal,String nombreUsuario) {
		//validar rango de fechas
		
		LogDeMidasEJB3.log("TipoCambioFacade. Actualizando Tipo de cambio mensual en TcTipoCambioMensual. fechaInicial: "+fechaInicial+", fechaFinal: "+fechaFinal, Level.INFO, null);
		
		if (fechaInicial != null && fechaFinal != null){
			fechaInicial = Utilerias.removerHorasMinutosSegundos(fechaInicial);
			fechaFinal = Utilerias.removerHorasMinutosSegundos(fechaFinal);
			
			Calendar inicio = Calendar.getInstance();
			Calendar fin = Calendar.getInstance();
			
			inicio.setTime(fechaInicial);
			fin.setTime(fechaFinal);
			
			String queryString = "Select model from TipoCambioMensualDTO model where model.mes = :mes and model.anio = :anio";
			Query query = null;
			
			if(inicio.getTimeInMillis() <= fin.getTimeInMillis()){
				
				//iterar fechas
				
				do{
					int anio = inicio.get(Calendar.YEAR);
					int mes = inicio.get(Calendar.MONTH) +1 ;
					
					//consultar tc mizar
					Double tcMizar = null;
					
					try {
						tcMizar = tipoCambioFacade.obtieneTipoCambioPorMes(mes, anio, nombreUsuario);
					} catch (Exception e) {
						LogDeMidasEJB3.log("No se pudo obtener el tipo de cambio para el mes/anio: "+mes+"/"+anio,Level.WARNING,null);
						throw new RuntimeException(e);
					}
					
					if(tcMizar != null){
						//consultar tc midas
						query=entityManager.createQuery(queryString);
						query.setParameter("mes", (short)mes);
						query.setParameter("anio", (short)anio);
						
						List<TipoCambioMensualDTO> listaTCMidas = query.getResultList();
						TipoCambioMensualDTO tipoCambioMidas = null;
						
						if(listaTCMidas == null || listaTCMidas.isEmpty()){
							//si no existe registrar
							tipoCambioMidas = new TipoCambioMensualDTO(null, (short)mes, (short)anio, tcMizar);
							
							save(tipoCambioMidas);
						}
						else if(listaTCMidas.size() == 1){
							tipoCambioMidas = listaTCMidas.get(0);
							if (tipoCambioMidas.getTipoCambio().compareTo(tcMizar) != 0){
								//si existe validar si son iguales
								//si son diferentes, actualizar
								update(tipoCambioMidas);
							}
						}
						else{
							//existe m�s de un TC para el mismo mes, eliminarlos y registrar el m�s reci�n consultado.
							for(TipoCambioMensualDTO tcTMP : listaTCMidas){
								delete(tcTMP);
							}
							
							tipoCambioMidas = new TipoCambioMensualDTO(null, (short)mes, (short)anio, tcMizar);
							
							save(tipoCambioMidas);
						}
							
						
					}
					
					inicio.add(Calendar.MONTH, 1);
					
					if((inicio.get(Calendar.YEAR) == fin.get(Calendar.YEAR) && inicio.get(Calendar.MONTH) > fin.get(Calendar.MONTH))
							|| (inicio.get(Calendar.YEAR) > fin.get(Calendar.YEAR)) ){
						break;
					}
					
				}while (true);
				
			}
			else{
				throw new RuntimeException("Rango de fechas inv�lido: inicio:"+fechaInicial+", fechaFinal:"+fechaFinal);
			}
		}
		else{
			throw new RuntimeException("Rango de fechas inv�lido: inicio:"+fechaInicial+", fechaFinal:"+fechaFinal);
		}
		
		LogDeMidasEJB3.log("TipoCambioFacade. Finaliza actualizaci�n de Tipo de cambio mensual en TcTipoCambioMensual. fechaInicial: "+fechaInicial+", fechaFinal: "+fechaFinal, Level.INFO, null);
	}

	@EJB
	private mx.com.afirme.midas.interfaz.tipocambio.TipoCambioFacadeRemote tipoCambioFacade;
}