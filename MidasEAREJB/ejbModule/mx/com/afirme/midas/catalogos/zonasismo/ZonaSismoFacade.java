package mx.com.afirme.midas.catalogos.zonasismo;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity ZonaSismoDTO.
 * 
 * @see .ZonaSismoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ZonaSismoFacade implements ZonaSismoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved ZonaSismoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ZonaSismoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ZonaSismoDTO entity) {
		LogDeMidasEJB3.log("saving ZonaSismoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ZonaSismoDTO entity.
	 * 
	 * @param entity
	 *            ZonaSismoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ZonaSismoDTO entity) {
		LogDeMidasEJB3.log("deleting ZonaSismoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(ZonaSismoDTO.class, entity
					.getIdZonaSismo());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ZonaSismoDTO entity and return it or a copy of it
	 * to the sender. A copy of the ZonaSismoDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ZonaSismoDTO entity to update
	 * @return ZonaSismoDTO the persisted ZonaSismoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ZonaSismoDTO update(ZonaSismoDTO entity) {
		LogDeMidasEJB3.log("updating ZonaSismoDTO instance", Level.INFO, null);
		try {
			ZonaSismoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ZonaSismoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding ZonaSismoDTO instance with id: " + id, Level.INFO,
				null);
		try {
			ZonaSismoDTO instance = entityManager.find(ZonaSismoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ZonaSismoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ZonaSismoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ZonaSismoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ZonaSismoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding ZonaSismoDTO instance with property: " + propertyName
				+ ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ZonaSismoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ZonaSismoDTO entities.
	 * 
	 * @return List<ZonaSismoDTO> all ZonaSismoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ZonaSismoDTO> findAll() {
		LogDeMidasEJB3.log("finding all ZonaSismoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from ZonaSismoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	
	@SuppressWarnings("unchecked")
	public List<ZonaSismoDTO> listarFiltrado(ZonaSismoDTO zonaSismoDTO){
		try {
			String queryString = "select model from ZonaSismoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (zonaSismoDTO == null)
				return null;
						
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoZonaSismo", zonaSismoDTO.getCodigoZonaSismo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionZonaSismo", zonaSismoDTO.getDescripcionZonaSismo());		
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public ZonaSismoDTO findById(CatalogoValorFijoId arg0) {
		return null;
	}

	public ZonaSismoDTO findById(double arg0) {
		return null;
	}

	public List<ZonaSismoDTO> listRelated(Object id) {
		return findAll();
	}
	
}