package mx.com.afirme.midas.catalogos.modelovehiculo;
// default package

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.moneda.MonedaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.tipovehiculo.SeccionTipoVehiculoDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.usuario.LogUtil;
import mx.com.afirme.midas2.service.ExcelRowMapper;
import mx.com.afirme.midas2.service.impl.ExcelConverter;
import mx.com.afirme.midas2.service.impl.ExcelConverter.ExcelConfig.ExcelConfigBuilder;

import org.apache.commons.collections.CollectionUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity ModeloVehiculoDTO.
 * @see .ModeloVehiculoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ModeloVehiculoFacade  implements ModeloVehiculoFacadeRemote {

    @PersistenceContext private EntityManager entityManager;

    @EJB
    private EstiloVehiculoFacadeRemote estiloVehiculoFacade;
    
    @EJB
    private MonedaFacadeRemote monedaFacade;
		/**
	 Perform an initial save of a previously unsaved ModeloVehiculoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ModeloVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ModeloVehiculoDTO entity) {
    				LogUtil.log("saving ModeloVehiculoDTO instance", Level.FINE, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.FINE, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ModeloVehiculoDTO entity.
	  @param entity ModeloVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ModeloVehiculoDTO entity) {
    				LogUtil.log("deleting ModeloVehiculoDTO instance", Level.FINE, null);
	        try {
        	entity = entityManager.getReference(ModeloVehiculoDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.FINE, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ModeloVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ModeloVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ModeloVehiculoDTO entity to update
	 @return ModeloVehiculoDTO the persisted ModeloVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ModeloVehiculoDTO update(ModeloVehiculoDTO entity) {
    				LogUtil.log("updating ModeloVehiculoDTO instance", Level.FINE, null);
	        try {
            ModeloVehiculoDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.FINE, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ModeloVehiculoDTO findById( ModeloVehiculoId id) {
    				LogUtil.log("finding ModeloVehiculoDTO instance with id: " + id, Level.FINE, null);
	        try {
            ModeloVehiculoDTO instance = entityManager.find(ModeloVehiculoDTO.class, id);
            if(instance != null){
            	EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(
            			instance.getId().getClaveTipoBien(),
            			instance.getId().getClaveEstilo(),
            			instance.getId().getIdVersionCarga());
            	instance.setEstiloVehiculoDTO(estiloVehiculoFacade.findById(estiloVehiculoId));
            	
            	instance.setMonedaDTO(monedaFacade.findById(new Short(instance.getId().getIdMoneda().shortValue())));
            	
            }
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ModeloVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ModeloVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<ModeloVehiculoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ModeloVehiculoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding ModeloVehiculoDTO instance with property: " + propertyName + ", value: " + value, Level.FINE, null);
			try {
			final String queryString = "select model from ModeloVehiculoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ModeloVehiculoDTO entities.
	  	  @return List<ModeloVehiculoDTO> all ModeloVehiculoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ModeloVehiculoDTO> findAll(
		) {
					LogUtil.log("finding all ModeloVehiculoDTO instances", Level.FINE, null);
			try {
			final String queryString = "select model from ModeloVehiculoDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<ModeloVehiculoDTO> listarFiltrado(ModeloVehiculoDTO modeloVehiculoDTO){
		try {
			String queryString = "select model from ModeloVehiculoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (modeloVehiculoDTO == null)
				return null;
			if(modeloVehiculoDTO.getId() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.claveEstilo", modeloVehiculoDTO.getId().getClaveEstilo());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.claveTipoBien", modeloVehiculoDTO.getId().getClaveTipoBien());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idMoneda", modeloVehiculoDTO.getId().getIdMoneda());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idVersionCarga", modeloVehiculoDTO.getId().getIdVersionCarga());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.modeloVehiculo", modeloVehiculoDTO.getId().getModeloVehiculo());
			}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveCondRiesgo", modeloVehiculoDTO.getClaveCondRiesgo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoUsuarioCreacion", modeloVehiculoDTO.getCodigoUsuarioCreacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaCreacion", modeloVehiculoDTO.getFechaCreacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "valorComercial", modeloVehiculoDTO.getValorComercial());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "valorNuevo", modeloVehiculoDTO.getValorNuevo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "valorCaratula", modeloVehiculoDTO.getValorCaratula());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere).concat(" ORDER BY model.id.modeloVehiculo DESC ");
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ModeloVehiculoDTO> buscarPorMarcaMonedaSeccionVersionCarga(BigDecimal idMoneda, 
			BigDecimal idMarca, String claveTipoBien, Long idVersionCarga, List<SeccionTipoVehiculoDTO> seccionTipoVehiculoList){
		try {
			
			String queryTipoVehiculo = "";
			if(seccionTipoVehiculoList != null && !seccionTipoVehiculoList.isEmpty()){
				queryTipoVehiculo = obtenerQueryTipoVehiculo(seccionTipoVehiculoList);
					
			}
			
			String queryString = "SELECT DISTINCT MV.MODELOVEHICULO " +
					" FROM MIDAS.TCMODELOVEHICULO MV, MIDAS.TCESTILOVEHICULO EV " +
					" WHERE MV.IDMONEDA = " + idMoneda + 
					" AND MV.CLAVETIPOBIEN = '" + claveTipoBien + "'" +
					" AND MV.IDVERSIONCARGA = " + idVersionCarga +
					" AND MV.IDVERSIONCARGA = EV.IDVERSIONCARGA" +
					" AND MV.CLAVETIPOBIEN = EV.CLAVETIPOBIEN " +
					" AND MV.CLAVEESTILO = EV.CLAVEESTILO " +
					" AND EV.IDTCMARCAVEHICULO = " + idMarca +
					queryTipoVehiculo +
					" ORDER BY MV.MODELOVEHICULO DESC ";

			Query query = entityManager.createNativeQuery(queryString.toString());
			Object result  = query.getResultList();

			List<ModeloVehiculoDTO> modelos = new ArrayList<ModeloVehiculoDTO>(1);
			if(result instanceof List)  {
				List<Object> listaResultados = (List<Object>) result;				
				for(Object object : listaResultados){
					Object singleResult = (Object) object;
					
					BigDecimal idModelo = null;
					if(singleResult instanceof BigDecimal)
						idModelo = (BigDecimal)singleResult;
					else if (singleResult instanceof Long)
						idModelo = BigDecimal.valueOf((Long)singleResult);
					else if (singleResult instanceof Double)
						idModelo = BigDecimal.valueOf((Double)singleResult);
					else if (singleResult instanceof Short)
						idModelo = BigDecimal.valueOf((Short)singleResult);
					
					ModeloVehiculoDTO modelo = new ModeloVehiculoDTO();
					ModeloVehiculoId id = new ModeloVehiculoId();
					id.setModeloVehiculo(idModelo.shortValue());
					modelo.setId(id);
					
					modelos.add(modelo);
				}
			}
			
			return modelos;
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public String obtenerQueryTipoVehiculo(
			List<SeccionTipoVehiculoDTO> seccionTipoVehiculoList) {
	StringBuilder	queryTipoVehiculo =	new StringBuilder(" AND EV.IDTCTIPOVEHICULO IN (");
		boolean isFirst = true;
		for(SeccionTipoVehiculoDTO item : seccionTipoVehiculoList){
			if(!isFirst){
				queryTipoVehiculo.append(", ");
			}else{
				isFirst = false;
			}
			queryTipoVehiculo.append(item.getId().getIdTcTipoVehiculo());
		}
		queryTipoVehiculo.append(") ");	
		return queryTipoVehiculo.toString();
	}

	public String actualizarMasivo(String idToControlArchivo){

		List<ModeloVehiculoDTO> list;
		try {
			list = getListModeloVehiculoDTO(BigDecimal.valueOf(Integer.valueOf(idToControlArchivo)));
			if(CollectionUtils.isNotEmpty(list)){
				for(ModeloVehiculoDTO item :list){
		            ModeloVehiculoDTO modelo = entityManager.find(ModeloVehiculoDTO.class, item.getId());
		            modelo.setValorCaratula(item.getValorCaratula());
					entityManager.merge(modelo);	
					entityManager.flush();
				}
				return "Carga Exitosa";
			}
		} catch (NumberFormatException e) {
			LogUtil.log("actualizarMasivo failed", Level.SEVERE, e);
		} catch (IOException e) {
			LogUtil.log("actualizarMasivo failed", Level.SEVERE, e);
		}
		return "No se proceso el archivo, o este esta vacio";
	}
	
	public List<ModeloVehiculoDTO> getListModeloVehiculoDTO(BigDecimal idToControlArchivo) throws IOException{
		ControlArchivoDTO controlArchivo = entityManager.find(ControlArchivoDTO.class, idToControlArchivo);
		String OSName = System.getProperty("os.name");
		String uploadFolder = null;
		if (OSName.toLowerCase().indexOf("windows")!=-1) {
			uploadFolder = SistemaPersistencia.UPLOAD_FOLDER;
		} else {
			uploadFolder = SistemaPersistencia.LINUX_UPLOAD_FOLDER;
		}
		String urlArchivo = uploadFolder+controlArchivo.obtenerNombreArchivoFisico();
		InputStream archivo = null;
		List<ModeloVehiculoDTO> list = null;
				
				try {
					archivo = new FileInputStream(urlArchivo);
					ExcelConverter converter = new ExcelConverter(archivo, new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());
					
				for (String sheetName : converter.getSheetNames()) {

					list = converter.parseWithRowMapper(sheetName,
							new ExcelRowMapper<ModeloVehiculoDTO>() {

								@Override
								public ModeloVehiculoDTO mapRow(Map<String, String> row,
										int rowNum) {
									ModeloVehiculoDTO modeloVehiculo = new ModeloVehiculoDTO();
									modeloVehiculo = poblarModeloVehiculo( row );
									return modeloVehiculo;
								}
							});
				}

			} catch (FileNotFoundException e) {
				LogUtil.log("getListModeloVehiculoDTO failed ", Level.SEVERE, e);
			} finally {
				if (archivo != null) {
					try {
						archivo.close();
					} catch (IOException e) {
						throw e;
					}
				}
			}
			
			return list;
	}

	private ModeloVehiculoDTO poblarModeloVehiculo(Map<String, String> row) {

		ModeloVehiculoDTO modeloVehiculo = new ModeloVehiculoDTO();
		ModeloVehiculoId id = new ModeloVehiculoId();
		id.setClaveEstilo(row.get("clave_estilo"));
		id.setClaveTipoBien(row.get("clave_tipo_bien"));
		id.setIdMoneda(BigDecimal.valueOf(Integer.valueOf(row.get("moneda"))));
		id.setIdVersionCarga(BigDecimal.valueOf(Integer.valueOf(row.get("version_carga"))));
		id.setModeloVehiculo(Short.valueOf(row.get("modelo")));
		modeloVehiculo.setId(id);
		modeloVehiculo.setValorCaratula(BigDecimal.valueOf(Integer.valueOf(row.get("valor_caratula"))));		
		return modeloVehiculo;
	}
}