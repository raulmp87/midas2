package mx.com.afirme.midas.catalogos.rechazocancelacion;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity RechazoCancelacion.
 * @see .RechazoCancelacion
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class RechazoCancelacionFacade  implements RechazoCancelacionFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved RechazoCancelacion entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity RechazoCancelacion entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public RechazoCancelacionDTO save(RechazoCancelacionDTO entity) {
    				LogDeMidasEJB3.log("saving RechazoCancelacionDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
            		return entity;
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent RechazoCancelacionDTO entity.
	  @param entity RechazoCancelacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(RechazoCancelacionDTO entity) {
    				LogDeMidasEJB3.log("deleting RechazoCancelacionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(RechazoCancelacionDTO.class, entity.getIdTcRechazoCancelacion());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved RechazoCancelacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the RechazoCancelacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity RechazoCancelacionDTO entity to update
	 @return RechazoCancelacionDTO the persisted RechazoCancelacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public RechazoCancelacionDTO update(RechazoCancelacionDTO entity) {
    				LogDeMidasEJB3.log("updating RechazoCancelacionDTO instance", Level.INFO, null);
	        try {
            RechazoCancelacionDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public RechazoCancelacionDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding RechazoCancelacionDTO instance with id: " + id, Level.INFO, null);
	        try {
            RechazoCancelacionDTO instance = entityManager.find(RechazoCancelacionDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all RechazoCancelacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the RechazoCancelacionDTO property to query
	  @param value the property value to match
	  	  @return List<RechazoCancelacionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<RechazoCancelacionDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding RechazoCancelacionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from RechazoCancelacionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all RechazoCancelacionDTO entities.
	  	  @return List<RechazoCancelacionDTO> all RechazoCancelacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RechazoCancelacionDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all RechazoCancelacionDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from RechazoCancelacionDTO model order by model.descripcionMotivo";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	public RechazoCancelacionDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public RechazoCancelacionDTO findById(double id) {
		return null;
	}

	public List<RechazoCancelacionDTO> listRelated(Object id) {
		return findAll();
	}
}