package mx.com.afirme.midas.catalogos.mediotransporte;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity MedioTransporteDTO.
 * @see .MedioTransporteDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class MedioTransporteFacade  implements MedioTransporteFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved MedioTransporteDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MedioTransporteDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(MedioTransporteDTO entity) {
    				LogDeMidasEJB3.log("saving MedioTransporteDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent MedioTransporteDTO entity.
	  @param entity MedioTransporteDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MedioTransporteDTO entity) {
    				LogDeMidasEJB3.log("deleting MedioTransporteDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(MedioTransporteDTO.class, entity.getIdMedioTransporte());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved MedioTransporteDTO entity and return it or a copy of it to the sender. 
	 A copy of the MedioTransporteDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MedioTransporteDTO entity to update
	 @return MedioTransporteDTO the persisted MedioTransporteDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public MedioTransporteDTO update(MedioTransporteDTO entity) {
    				LogDeMidasEJB3.log("updating MedioTransporteDTO instance", Level.INFO, null);
	        try {
            MedioTransporteDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public MedioTransporteDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding MedioTransporteDTO instance with id: " + id, Level.INFO, null);
	        try {
            MedioTransporteDTO instance = entityManager.find(MedioTransporteDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all MedioTransporteDTO entities with a specific property value.  
	 
	  @param propertyName the name of the MedioTransporteDTO property to query
	  @param value the property value to match
	  	  @return List<MedioTransporteDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<MedioTransporteDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding MedioTransporteDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from MedioTransporteDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all MedioTransporteDTO entities.
	  	  @return List<MedioTransporteDTO> all MedioTransporteDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<MedioTransporteDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all MedioTransporteDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from MedioTransporteDTO model " +
					"order by model.descripcionMedioTransporte";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<MedioTransporteDTO> listarFiltrado(MedioTransporteDTO medioTransporteDTO){
		try {
			String queryString = "select model from MedioTransporteDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (medioTransporteDTO == null)
				return null;
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoMedioTransporte", medioTransporteDTO.getCodigoMedioTransporte());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "descripcionMedioTransporte", medioTransporteDTO.getDescripcionMedioTransporte());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public MedioTransporteDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public MedioTransporteDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<MedioTransporteDTO> listRelated(Object id) {
		return this.findAll();
	}
	
}