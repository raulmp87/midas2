package mx.com.afirme.midas.catalogos.codigopostaliva;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.usuario.LogUtil;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;


/**
 * Facade for entity CodigoPostalIVADTO.
 * 
 * @see .CodigoPostalIVADTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CodigoPostalIVAFacade implements 	CodigoPostalIVAFacadeRemote {

    @PersistenceContext
    private EntityManager entityManager;

    @EJB
    private CotizacionFacadeRemote cotizacionFacade;
    
    @EJB
    private ClienteFacadeRemote clienteFacadeRemote;
    
    @EJB
    private DomicilioFacadeRemote domicilioFacadeRemote;
    
    public void setDomicilioFacadeRemote(DomicilioFacadeRemote domicilioFacadeRemote) {
		this.domicilioFacadeRemote = domicilioFacadeRemote;
	}

	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}

	/**
     * Perform an initial save of a previously unsaved CodigoPostalIVADTO
     * entity. All subsequent persist actions of this entity should use the
     * #update() method.
     * 
     * @param entity
     *            CodigoPostalIVADTO entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(CodigoPostalIVADTO entity) {
	LogUtil.log("saving CodigoPostalIVADTO instance", Level.FINE, null);
	try {
	    entityManager.persist(entity);
	    LogUtil.log("save successful", Level.FINE, null);
	} catch (RuntimeException re) {
	    LogUtil.log("save failed", Level.SEVERE, re);
	    throw re;
	}
    }

    /**
     * Delete a persistent CodigoPostalIVADTO entity.
     * 
     * @param entity
     *            CodigoPostalIVADTO entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(CodigoPostalIVADTO entity) {
	LogUtil.log("deleting CodigoPostalIVADTO instance", Level.FINE, null);
	try {
	    entity = entityManager.getReference(CodigoPostalIVADTO.class,
		    entity.getId());
	    entityManager.remove(entity);
	    LogUtil.log("delete successful", Level.FINE, null);
	} catch (RuntimeException re) {
	    LogUtil.log("delete failed", Level.SEVERE, re);
	    throw re;
	}
    }

    /**
     * Persist a previously saved CodigoPostalIVADTO entity and return it or a
     * copy of it to the sender. A copy of the CodigoPostalIVADTO entity
     * parameter is returned when the JPA persistence mechanism has not
     * previously been tracking the updated entity.
     * 
     * @param entity
     *            CodigoPostalIVADTO entity to update
     * @return CodigoPostalIVADTO the persisted CodigoPostalIVADTO entity
     *         instance, may not be the same
     * @throws RuntimeException
     *             if the operation fails
     */
    public CodigoPostalIVADTO update(CodigoPostalIVADTO entity) {
	LogUtil.log("updating CodigoPostalIVADTO instance", Level.FINE, null);
	try {
	    CodigoPostalIVADTO result = entityManager.merge(entity);
	    LogUtil.log("update successful", Level.FINE, null);
	    return result;
	} catch (RuntimeException re) {
	    LogUtil.log("update failed", Level.SEVERE, re);
	    throw re;
	}
    }

    public CodigoPostalIVADTO findById(CodigoPostalIVAId id) {
	LogUtil.log("finding CodigoPostalIVADTO instance with id: " + id,
		Level.FINE, null);
	try {
	    CodigoPostalIVADTO instance = entityManager.find(
		    CodigoPostalIVADTO.class, id);
	    return instance;
	} catch (RuntimeException re) {
	    LogUtil.log("find failed", Level.SEVERE, re);
	    throw re;
	}
    }

    /**
     * Find all CodigoPostalIVADTO entities with a specific property value.
     * 
     * @param propertyName
     *            the name of the CodigoPostalIVADTO property to query
     * @param value
     *            the property value to match
     * @return List<CodigoPostalIVADTO> found by query
     */
    @SuppressWarnings("unchecked")
    public List<CodigoPostalIVADTO> findByProperty(String propertyName,
	    final Object value) {
	LogUtil.log("finding CodigoPostalIVADTO instance with property: "
		+ propertyName + ", value: " + value, Level.FINE, null);
	try {
	    final String queryString = "select model from CodigoPostalIVADTO model where model."
		    + propertyName + "= :propertyValue";
	    Query query = entityManager.createQuery(queryString);
	    query.setParameter("propertyValue", value);
	    return query.getResultList();
	} catch (RuntimeException re) {
	    LogUtil.log("find by property name failed", Level.SEVERE, re);
	    throw re;
	}
    }

    /**
     * Find all CodigoPostalIVADTO entities.
     * 
     * @return List<CodigoPostalIVADTO> all CodigoPostalIVADTO entities
     */
    @SuppressWarnings("unchecked")
    public List<CodigoPostalIVADTO> findAll() {
	LogUtil.log("finding all CodigoPostalIVADTO instances", Level.FINE,
		null);
	try {
	    final String queryString = "select model from CodigoPostalIVADTO model";
	    Query query = entityManager.createQuery(queryString);
	    return query.getResultList();
	} catch (RuntimeException re) {
	    LogUtil.log("find all failed", Level.SEVERE, re);
	    throw re;
	}
    }
        

    @SuppressWarnings("unchecked")
	public List<CodigoPostalIVADTO> findByCodigoPostal(BigDecimal codigoPostal) {
		LogUtil.log("finding CodigoPostalIVADTO instances with codigoPostal: " + codigoPostal, Level.FINE, null);
		try {
			String queryString = "select model from CodigoPostalIVADTO model where model.id.codigoPostal = :codigoPostal";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("codigoPostal", codigoPostal);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("findByCodigoPostal failed", Level.SEVERE, re);
			throw re;
		}
	}
    
    public Double getIVAPorIdCotizacion(BigDecimal idToCotizacion, String nombreUsuario) throws SystemException{    	
		Double iva = null;
		String claveColonia = null;
		CotizacionDTO cotizacionDTO= cotizacionFacade.findById(idToCotizacion);		 
		ClienteGenericoDTO filtroCliente = new ClienteGenericoDTO();
		filtroCliente.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
		Long idToPersona = null;
		try {
			filtroCliente = clienteFacadeRemote.loadById(filtroCliente);
			if (filtroCliente != null && filtroCliente.getIdToPersona() != null) {
				idToPersona = filtroCliente.getIdToPersona().longValue();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (cotizacionDTO != null) {			
			try{		    	
				if(cotizacionDTO.getIdDomicilioContratante() != null){
					CodigoPostalIVAId codigoPostalIVAId= new CodigoPostalIVAId();
					Domicilio domicilio = domicilioFacadeRemote.findById(cotizacionDTO.getIdDomicilioContratante().longValue(),idToPersona);	
					claveColonia = domicilio.getIdColonia();
		    		codigoPostalIVAId.setNombreColonia(claveColonia);
		    		codigoPostalIVAId.setCodigoPostal(new BigDecimal(domicilio.getCodigoPostal().trim()));
		    		CodigoPostalIVADTO  codigoPostalIVADTO = findById(codigoPostalIVAId);				
	    		    if (codigoPostalIVADTO != null) {
	    		    	iva = codigoPostalIVADTO.getValorIVA();
	    		    }else{
	    			LogDeMidasEJB3.log("No se pudo obtener el valor del IVA, para la colonia: "+claveColonia+" y codigo postal: "+ domicilio.getCodigoPostal()+"...", Level.WARNING,null);
	    		    }
	    		}else{
	    		    LogDeMidasEJB3.log("No se pudo encontrar una instancia de Cliente para el contratante con id: "+idToCotizacion+" en la busqueda del IVA.", Level.WARNING, null);
	    		}			
			} catch (Exception e) {
				LogDeMidasEJB3.log("Error en la ejecucion de CodigoPostalIVADN.getIVAPorIdCotizacion...", Level.WARNING, e);
			}
			
		}else{
		    LogDeMidasEJB3.log("No se pudo encontrar una instancia de Cotizacion para el id: "+idToCotizacion+" en la busqueda del IVA.", Level.WARNING, null);
		}
		return iva;
	
	}
}