package mx.com.afirme.midas.catalogos.aumentovario;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity AumentoVarioDTO.
 * @see .AumentoVarioDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class AumentoVarioFacade  implements AumentoVarioFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved AumentoVarioDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AumentoVarioDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AumentoVarioDTO entity) {
    				LogDeMidasEJB3.log("saving AumentoVarioDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent AumentoVarioDTO entity.
	  @param entity AumentoVarioDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AumentoVarioDTO entity) {
    				LogDeMidasEJB3.log("deleting AumentoVarioDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(AumentoVarioDTO.class, entity.getIdAumentoVario());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved AumentoVarioDTO entity and return it or a copy of it to the sender. 
	 A copy of the AumentoVarioDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AumentoVarioDTO entity to update
	 @return AumentoVarioDTO the persisted AumentoVarioDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public AumentoVarioDTO update(AumentoVarioDTO entity) {
    				LogDeMidasEJB3.log("updating AumentoVarioDTO instance", Level.INFO, null);
	        try {
            AumentoVarioDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
            			entityManager.flush();
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public AumentoVarioDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding AumentoVarioDTO instance with id: " + id, Level.INFO, null);
	        try {
            AumentoVarioDTO instance = entityManager.find(AumentoVarioDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all AumentoVarioDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AumentoVarioDTO property to query
	  @param value the property value to match
	  	  @return List<AumentoVarioDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<AumentoVarioDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding AumentoVarioDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from AumentoVarioDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all AumentoVarioDTO entities.
	  	  @return List<AumentoVarioDTO> all AumentoVarioDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<AumentoVarioDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all AumentoVarioDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from AumentoVarioDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<AumentoVarioDTO> listarFiltrado(AumentoVarioDTO aumentoVarioDTO) {		
		try {
			String queryString = "select model from AumentoVarioDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (aumentoVarioDTO == null)
				return null;
						
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionAumento", aumentoVarioDTO.getDescripcionAumento());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveTipoAumento", aumentoVarioDTO.getClaveTipoAumento());		
			
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<AumentoVarioDTO> listarAumentosPorAsociar(BigDecimal idToProducto) {
		String queryString = "select model from AumentoVarioDTO as model";
		queryString += " where model.idAumentoVario not in (select d.id.idtoaumentovario from AumentoVarioProductoDTO d where d.id.idtoproducto = :idToProducto)";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToProducto", idToProducto);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<AumentoVarioDTO> listarAumentosPorAsociarTipoPoliza(
			BigDecimal idToTipoPoliza) {
		String queryString = "select model from AumentoVarioDTO as model";
		queryString += " where model.idAumentoVario not in (select d.id.idtoaumentovario from AumentoVarioTipoPolizaDTO d where d.id.idtotipopoliza = :idToTipoPoliza)";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToTipoPoliza", idToTipoPoliza);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<AumentoVarioDTO> listarAumentosPorAsociarCobetura(
			BigDecimal idToCobertura) {
		String queryString = "select model from AumentoVarioDTO as model";
		queryString += " where model.idAumentoVario not in (select d.id.idtoaumentovario from AumentoVarioCoberturaDTO d where d.id.idtocobertura = :idToCobertura)";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCobertura", idToCobertura);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
}