package mx.com.afirme.midas.catalogos.paistipodestinotransporte;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity Tctipodestinotransporte.
 * 
 * @see mx.com.afirme.midas.catalogos.paistipodestinotransporte.Tctipodestinotransporte
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoDestinoTransporteFacade implements
		TipoDestinoTransporteFacadeRemote {
	// property constants
	public static final String DESCRIPCIONTIPODESTINOTRA = "descripciontipodestinotra";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved Tctipodestinotransporte
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Tctipodestinotransporte entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoDestinoTransporteDTO entity) {
		LogDeMidasEJB3.log("saving TipoDestinoTransporteDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoDestinoTransporteDTO entity.
	 * 
	 * @param entity
	 *            TipoDestinoTransporteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoDestinoTransporteDTO entity) {
		LogDeMidasEJB3.log("deleting TipoDestinoTransporteDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(TipoDestinoTransporteDTO.class,
					entity.getIdTipoDestinoTransporte());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoDestinoTransporteDTO entity and return it
	 * or a copy of it to the sender. A copy of the TipoDestinoTransporteDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoDestinoTransporteDTO entity to update
	 * @return TipoDestinoTransporteDTO the persisted TipoDestinoTransporteDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoDestinoTransporteDTO update(TipoDestinoTransporteDTO entity) {
		LogDeMidasEJB3.log("updating TipoDestinoTransporteDTO instance",
				Level.INFO, null);
		try {
			TipoDestinoTransporteDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoDestinoTransporteDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding TipoDestinoTransporteDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoDestinoTransporteDTO instance = entityManager.find(
					TipoDestinoTransporteDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoDestinoTransporteDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the TipoDestinoTransporteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoDestinoTransporteDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoDestinoTransporteDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding TipoDestinoTransporteDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoDestinoTransporteDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<TipoDestinoTransporteDTO> findByDescripcionTipoDestinoTransporte(
			Object descripcionTipoDestinoTransporte) {
		return findByProperty(DESCRIPCIONTIPODESTINOTRA,
				descripcionTipoDestinoTransporte);
	}

	/**
	 * Find all TipoDestinoTransporteDTO entities.
	 * 
	 * @return List<TipoDestinoTransporteDTO> all TipoDestinoTransporteDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoDestinoTransporteDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoDestinoTransporteDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from TipoDestinoTransporteDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List result = query.getResultList();
			this.sortTipoDestinoTransporteCollection(result);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	private void sortTipoDestinoTransporteCollection(List list) {
		Collections.sort(list, new Comparator(){
			public int compare(Object o1, Object o2) {
				if(o1 instanceof TipoDestinoTransporteDTO && o2 instanceof TipoDestinoTransporteDTO) {
					return ((TipoDestinoTransporteDTO)o1).getDescription().compareTo(((TipoDestinoTransporteDTO)o2).getDescription());
				}
				return 0;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public List<TipoDestinoTransporteDTO> listarFiltrado(
			TipoDestinoTransporteDTO tipoDestinoTransporteDTO) {
		try {
			String queryString = "select model from TipoDestinoTransporteDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (tipoDestinoTransporteDTO == null)
				return null;

			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigoTipoDestinoTransporte",
					tipoDestinoTransporteDTO.getCodigoTipoDestinoTransporte());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "descripcionTipoDestinoTransporte",
					tipoDestinoTransporteDTO
							.getDescripcionTipoDestinoTransporte());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoDestinoTransporteDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoDestinoTransporteDTO findById(double id) {
		return null;
	}

	public List<TipoDestinoTransporteDTO> listRelated(Object id) {
		return findAll();
	}

}