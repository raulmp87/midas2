package mx.com.afirme.midas.catalogos.paistipodestinotransporte;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity PaisTipoDestinoTransporteDTO.
 * 
 * @see mx.com.afirme.midas.catalogos.paistipodestinotransporte.Tcpaistipodestinotransporte
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class PaisTipoDestinoTransporteFacade implements PaisTipoDestinoTransporteFacadeRemote{
	
	// property constants

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * PaisTipoDestinoTransporteDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            PaisTipoDestinoTransporteDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(PaisTipoDestinoTransporteDTO entity) {
		LogDeMidasEJB3.log("saving PaisTipoDestinoTransporteDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent PaisTipoDestinoTransporteDTO entity.
	 * 
	 * @param entity
	 *            PaisTipoDestinoTransporteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(PaisTipoDestinoTransporteDTO entity) {
		LogDeMidasEJB3.log("deleting PaisTipoDestinoTransporteDTO instance through the use of the ID PaisTipoDestinoTransporteID",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					PaisTipoDestinoTransporteDTO.class, entity.getIdPais());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved PaisTipoDestinoTransporteDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * PaisTipoDestinoTransporteDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            PaisTipoDestinoTransporteDTO entity to update
	 * @return PaisTipoDestinoTransporteDTO the persisted
	 *         PaisTipoDestinoTransporteDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public PaisTipoDestinoTransporteDTO update(PaisTipoDestinoTransporteDTO entity) {
		LogDeMidasEJB3.log("updating PaisTipoDestinoTransporteDTO instance",
				Level.INFO, null);
		try {
			PaisTipoDestinoTransporteDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public PaisTipoDestinoTransporteDTO findById(PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO) {
		LogDeMidasEJB3.log("finding PaisTipoDestinoTransporteDTO instance with idPais: "
				+ paisTipoDestinoTransporteDTO.getIdPais(), Level.INFO, null);
		try {
			PaisTipoDestinoTransporteDTO instance = entityManager.find(
					PaisTipoDestinoTransporteDTO.class, paisTipoDestinoTransporteDTO.getIdPais());
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
	

	/**
	 * Find all PaisTipoDestinoTransporteDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the PaisTipoDestinoTransporteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<PaisTipoDestinoTransporteDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<PaisTipoDestinoTransporteDTO> findByProperty(
			String propertyName, final Object value) {
		LogDeMidasEJB3.log(
				"finding PaisTipoDestinoTransporteDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from PaisTipoDestinoTransporteDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all PaisTipoDestinoTransporteDTO entities.
	 * 
	 * @return List<PaisTipoDestinoTransporteDTO> all PaisTipoDestinoTransporteDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<PaisTipoDestinoTransporteDTO> findAll() {
		LogDeMidasEJB3.log("finding all PaisTipoDestinoTransporteDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from PaisTipoDestinoTransporteDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<PaisTipoDestinoTransporteDTO> listarFiltrado(PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO){
		try {
			String queryString = "select model from PaisTipoDestinoTransporteDTO AS model";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (paisTipoDestinoTransporteDTO == null)
				return null;
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idPais", paisTipoDestinoTransporteDTO.getPais().getCountryId(), "idPais");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "pais.countryName", paisTipoDestinoTransporteDTO.getPais().getCountryName(), "countryName");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoDestinoTransporte.idTipoDestinoTransporte", paisTipoDestinoTransporteDTO.getTipoDestinoTransporte().getIdTipoDestinoTransporte(), "idTipoDestinoTransporte");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoDestinoTransporte.codigoTipoDestinoTransporte", paisTipoDestinoTransporteDTO.getTipoDestinoTransporte().getCodigoTipoDestinoTransporte(), "codigoTipoDestinoTransporte");
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoDestinoTransporte.descripcionTipoDestinoTransporte", paisTipoDestinoTransporteDTO.getTipoDestinoTransporte().getDescripcionTipoDestinoTransporte(), "descripcionTipoDestinoTransporte");
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public PaisTipoDestinoTransporteDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding PaisTipoDestinoTransporteDTO instance with id: " + id,
				Level.INFO, null);
		try {
			PaisTipoDestinoTransporteDTO instance = entityManager.find(PaisTipoDestinoTransporteDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	public PaisTipoDestinoTransporteDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public PaisTipoDestinoTransporteDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<PaisTipoDestinoTransporteDTO> listRelated(Object id) {
		return this.findAll();
	}
}