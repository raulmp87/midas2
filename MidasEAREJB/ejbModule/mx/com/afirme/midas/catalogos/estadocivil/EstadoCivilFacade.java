package mx.com.afirme.midas.catalogos.estadocivil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

@Stateless
public class EstadoCivilFacade implements EstadoCivilFacadeRemote{
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<EstadoCivilDTO> findAll() {
		LogDeMidasEJB3.log("finding all EstadoCivilDTO instances", Level.FINE, null);
		try{
			String statement="select model from EstadoCivilDTO model";
			Query query= entityManager.createQuery(statement);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public EstadoCivilDTO findById(BigDecimal arg0) {
		return null;
	}

	@Override
	public EstadoCivilDTO findById(CatalogoValorFijoId arg0) {
		return null;
	}

	@Override
	public EstadoCivilDTO findById(double arg0) {
		return null;
	}

	@Override
	public List<EstadoCivilDTO> listRelated(Object arg0) {
		return null;
	}

	@Override
	public EstadoCivilDTO findById(String idEstadoCivil) {
		LogDeMidasEJB3.log("finding EstadoCivilDTO by idEstadoCivil with id:"+idEstadoCivil, Level.FINE, null);
		try{
			EstadoCivilDTO instance=entityManager.find(EstadoCivilDTO.class,idEstadoCivil);
			return instance;
		}catch(RuntimeException  re){
			LogDeMidasEJB3.log("find by id", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public EstadoCivilDTO findByName(String nombre) {
		LogDeMidasEJB3.log("finding EstadoCivilDTO by estado civil name with name:"+nombre, Level.FINE, null);
		try{
			if(nombre==null || nombre.isEmpty()){
				return null;
			}
			String queryString="select model from EstadoCivilDTO model ";
			String sWhere="";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEstadoCivil",nombre);
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
			}
			queryString += " order by model.idEstadoCivil desc, model.nombreEstadoCivil asc ";//TODO: revisar el comportamiento del paginado con la lista ordenada
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return (EstadoCivilDTO)query.getSingleResult();
		}catch(RuntimeException re){
			LogDeMidasEJB3.log("find by name", Level.SEVERE, re);
			throw re;
		}
	}

}
