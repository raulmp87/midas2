package mx.com.afirme.midas.catalogos.giro;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity SubGiroDTO.
 * 
 * @see .SubGiroDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SubGiroFacade implements SubGiroFacadeRemote {
	// property constants
	public static final String DESCRIPCIONSUBGIRO = "descripcionsubgiro";
	public static final String CLAVEINSPECCION = "claveinspeccion";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SubGiroDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SubGiroDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubGiroDTO entity) {
		LogDeMidasEJB3.log("saving SubGiroDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SubGiroDTO entity.
	 * 
	 * @param entity
	 *            SubGiroDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubGiroDTO entity) {
		LogDeMidasEJB3.log("deleting SubGiroDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SubGiroDTO.class, entity.getIdTcSubGiro());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SubGiroDTO entity and return it or a copy of
	 * it to the sender. A copy of the SubGiroDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SubGiroDTO entity to update
	 * @return SubGiroDTO the persisted SubGiroDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubGiroDTO update(SubGiroDTO entity) {
		LogDeMidasEJB3.log("updating SubGiroDTO instance", Level.INFO, null);
		try {
			SubGiroDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SubGiroDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SubGiroDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SubGiroDTO instance = entityManager.find(SubGiroDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SubGiroDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SubGiroDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubGiroDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SubGiroDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SubGiroDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SubGiroDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<SubGiroDTO> findByDescripcionSubGiro(Object descripcionSubGiro) {
		return findByProperty(DESCRIPCIONSUBGIRO, descripcionSubGiro);
	}

	public List<SubGiroDTO> findByClaveInspeccion(Object claveInspeccion) {
		return findByProperty(CLAVEINSPECCION, claveInspeccion);
	}

	/**
	 * Find all SubGiroDTO entities.
	 * 
	 * @return List<SubGiroDTO> all SubGiroDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubGiroDTO> findAll() {
		LogDeMidasEJB3
				.log("finding all SubGiroDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from SubGiroDTO model " +
					"order by model.descripcionSubGiro";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SubGiroDTO> listarFiltrado(SubGiroDTO subGiroDTO) {
		String queryString = "select model from SubGiroDTO AS model ";
		String sWhere = "";
		Query query = null;
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

		if (subGiroDTO == null) {
			return null;
		}
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoSubGiro", subGiroDTO.getCodigoSubGiro());
		sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionSubGiro", subGiroDTO.getDescripcionSubGiro());
//		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "giroDTO", subGiroDTO.getGiro());
//		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idGrupoRobo", subGiroDTO.getIdGrupoRobo());
//		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idGrupoDineroValores", subGiroDTO.getIdGrupoDineroValores());
//		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idGrupoCristales", subGiroDTO.getIdGrupoCristales());
//		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idGrupoPlenos", subGiroDTO.getIdGrupoPlenos());
		if (Utilerias.esAtributoQueryValido(sWhere)) {
			queryString = queryString.concat(" where ").concat(sWhere);
		}
		query = entityManager.createQuery(queryString);
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	public SubGiroDTO findById(CatalogoValorFijoId arg0) {
		return null;
	}

	public SubGiroDTO findById(double arg0) {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<SubGiroDTO> listRelated(Object id) {
		LogDeMidasEJB3.log("finding all SubGiroDTO related with id: "+id, Level.INFO, null);
		try {
			final String queryString = "select model from SubGiroDTO model where " +
					"model.giroDTO.idTcGiro = (select sg.giroDTO.idTcGiro " +
					"from SubGiroDTO sg where sg.idTcSubGiro = :id) order by model.descripcionSubGiro";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("id", (BigDecimal)id);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
}