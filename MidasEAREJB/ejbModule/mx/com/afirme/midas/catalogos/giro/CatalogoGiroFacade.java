package mx.com.afirme.midas.catalogos.giro;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

@Stateless
public class CatalogoGiroFacade implements CatalogoGiroFacadeRemote{
	@PersistenceContext
	private EntityManager entityManager;
	@Override
	public List<CatalogoGiroDTO> findAll() {
		LogDeMidasEJB3.log("finding all CatalogoGiroDTO instances", Level.INFO, null);
		try{
			String statement="select model from CatalogoGiroDTO model";
			Query query= entityManager.createQuery(statement);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public CatalogoGiroDTO findById(BigDecimal idBanco) {
		if(idBanco!=null){
			return findById(idBanco.intValue());
		}
		return null;
	}

	@Override
	public CatalogoGiroDTO findById(CatalogoValorFijoId banco) {
		return null;
	}

	@Override
	public CatalogoGiroDTO findById(double idBanco) {
		return findById(Integer.parseInt(String.valueOf(idBanco)));
	}

	@Override
	public List<CatalogoGiroDTO> listRelated(Object arg0) {
		return null;
	}

	@Override
	public CatalogoGiroDTO findById(Integer idGiro) {
		LogDeMidasEJB3.log("finding CatalogoGiroDTO by idGiro with id:"+idGiro, Level.INFO, null);
		try{
			CatalogoGiroDTO instance=entityManager.find(CatalogoGiroDTO.class,idGiro);
			return instance;
		}catch(RuntimeException  re){
			LogDeMidasEJB3.log("find by id", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public CatalogoGiroDTO findByName(String nombre) {
		LogDeMidasEJB3.log("finding CatalogoGiroDTO by giro name with name:"+nombre, Level.INFO, null);
		try{
			if(nombre==null || nombre.isEmpty()){
				return null;
			}
			String queryString="select model from CatalogoGiroDTO model ";
			String sWhere="";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreGiro",nombre);
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
			}
			queryString += " order by model.idGiro desc, model.nombreGiro asc ";//TODO: revisar el comportamiento del paginado con la lista ordenada
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return (CatalogoGiroDTO)query.getSingleResult();
		}catch(RuntimeException re){
			LogDeMidasEJB3.log("find by name", Level.SEVERE, re);
			throw re;
		}
	}

}
