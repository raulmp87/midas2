package mx.com.afirme.midas.catalogos.giro;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity GiroDTO.
 * 
 * @see .GiroDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class GiroFacade implements GiroFacadeRemote {
	// property constants
	public static final String DESCRIPCIONGIRO = "descripciongiro";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved GiroDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            GiroDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(GiroDTO entity) {
		LogDeMidasEJB3.log("saving GiroDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent GiroDTO entity.
	 * 
	 * @param entity
	 *            GiroDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(GiroDTO entity) {
		LogDeMidasEJB3.log("deleting GiroDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(GiroDTO.class, entity
					.getIdTcGiro());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved GiroDTO entity and return it or a copy of it
	 * to the sender. A copy of the GiroDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            GiroDTO entity to update
	 * @return GiroDTO the persisted GiroDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public GiroDTO update(GiroDTO entity) {
		LogDeMidasEJB3.log("updating GiroDTO instance", Level.INFO, null);
		try {
			GiroDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public GiroDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding GiroDTO instance with id: " + id,
				Level.INFO, null);
		try {
			GiroDTO instance = entityManager.find(GiroDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all GiroDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the GiroDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<GiroDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<GiroDTO> findByProperty(String propertyName, final Object value) {
		LogDeMidasEJB3.log("finding GiroDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from GiroDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<GiroDTO> findByDescripcionGiro(Object descripcionGiro) {
		return findByProperty(DESCRIPCIONGIRO, descripcionGiro);
	}

	/**
	 * Find all GiroDTO entities.
	 * 
	 * @return List<GiroDTO> all GiroDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<GiroDTO> findAll() {
		LogDeMidasEJB3.log("finding all GiroDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from GiroDTO model " +
					"order by model.descripcionGiro";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all GiroDTO entities with depends of a filter.
	 * 
	 * @param entity
	 *            the name of the entity GiroDTO
	 * @return List<CoberturaDTO> GiroDTO found by query
	 */
	@SuppressWarnings("unchecked")
	public List<GiroDTO> listarFiltrado(GiroDTO entity) {
		String queryString = "select model from GiroDTO model ";
		String sWhere = "";
		Query query;
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		if (entity == null) {
			return null;
		}
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoGiro", entity.getCodigoGiro());
		sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionGiro", entity.getDescripcionGiro());
		if (Utilerias.esAtributoQueryValido(sWhere)) {
			queryString = queryString.concat(" where ").concat(sWhere);
		}
		query = entityManager.createQuery(queryString);
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	public EntityTransaction getTransaction() {
		EntityTransaction transaccion = entityManager.getTransaction();
		return transaccion;
	}

	public GiroDTO findById(CatalogoValorFijoId arg0) {
		return null;
	}

	public GiroDTO findById(double arg0) {
		return null;
	}

	public List<GiroDTO> listRelated(Object id) {
		return findAll();
	}

}