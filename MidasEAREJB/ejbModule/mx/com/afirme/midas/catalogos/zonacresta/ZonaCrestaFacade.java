package mx.com.afirme.midas.catalogos.zonacresta;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;


/**
 * Facade for entity ZonaCrestaDTO.
 * @see .ZonaCrestaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ZonaCrestaFacade  implements ZonaCrestaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ZonaCrestaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ZonaCrestaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ZonaCrestaDTO entity) {
    				LogDeMidasEJB3.log("saving ZonaCrestaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ZonaCrestaDTO entity.
	  @param entity ZonaCrestaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    @SuppressWarnings("unchecked")
	public void delete(ZonaCrestaDTO entity) {
    				LogDeMidasEJB3.log("deleting ZonaCrestaDTO instance", Level.INFO, null);
	        try {
	        	BigDecimal id = entity.getIdtczonacresta();
		        ZonaCrestaNuevaViejoId idTr;
		        final String queryString = "select model from ZonaCrestaNuevaViejoDTO as model" +
		        							" where model.id.idtczonacresta = :idCresta";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idCresta",id);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				List<ZonaCrestaNuevaViejoDTO> listaTr = query.getResultList();
				for(int indiceLista = 0; indiceLista < listaTr.size(); indiceLista++){
					idTr = listaTr.get(indiceLista).getId();
					ZonaCrestaNuevaViejoDTO entidad = entityManager.find(ZonaCrestaNuevaViejoDTO.class, idTr);
					entityManager.remove(entidad);
				}
		        
	        	entity = entityManager.getReference(ZonaCrestaDTO.class, id);
	            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ZonaCrestaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ZonaCrestaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ZonaCrestaDTO entity to update
	 @return ZonaCrestaDTO the persisted ZonaCrestaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ZonaCrestaDTO update(ZonaCrestaDTO entity) {
    				LogDeMidasEJB3.log("updating ZonaCrestaDTO instance", Level.INFO, null);
	        try {
            ZonaCrestaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ZonaCrestaDTO findById(BigDecimal id) {
    				LogDeMidasEJB3.log("finding ZonaCrestaDTO instance with id: " + id, Level.INFO, null);
	        try {
            ZonaCrestaDTO instance = entityManager.find(ZonaCrestaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ZonaCrestaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ZonaCrestaDTO property to query
	  @param value the property value to match
	  	  @return List<ZonaCrestaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ZonaCrestaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ZonaCrestaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ZonaCrestaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ZonaCrestaDTO entities.
	  	  @return List<ZonaCrestaDTO> all ZonaCrestaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ZonaCrestaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ZonaCrestaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ZonaCrestaDTO as model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ZonaCrestaDTO> listarFiltrado(ZonaCrestaDTO zonaCrestaDTO){
		try {
			String queryString = "select model from ZonaCrestaDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (zonaCrestaDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "nombrearea", zonaCrestaDTO.getNombrearea());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "numeroarea", zonaCrestaDTO.getNumeroarea());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "geocodigo", zonaCrestaDTO.getGeocodigo());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public ZonaCrestaDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public ZonaCrestaDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<ZonaCrestaDTO> listRelated(Object id) {
		return this.findAll();
	}
}