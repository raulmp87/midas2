package mx.com.afirme.midas.catalogos.zonacresta;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity ZonaCrestaNuevaViejoDTO.
 * @see .ZonaCrestaNuevaViejoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ZonaCrestaNuevaViejoFacade  implements ZonaCrestaNuevaViejoFacadeRemote {
    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ZonaCrestaNuevaViejoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ZonaCrestaNuevaViejoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ZonaCrestaNuevaViejoDTO entity) {
    				LogDeMidasEJB3.log("saving ZonaCrestaNuevaViejoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ZonaCrestaNuevaViejoDTO entity.
	  @param entity ZonaCrestaNuevaViejoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ZonaCrestaNuevaViejoDTO entity) {
    				LogDeMidasEJB3.log("deleting ZonaCrestaNuevaViejoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ZonaCrestaNuevaViejoDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ZonaCrestaNuevaViejoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ZonaCrestaNuevaViejoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ZonaCrestaNuevaViejoDTO entity to update
	 @return ZonaCrestaNuevaViejoDTO the persisted ZonaCrestaNuevaViejoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ZonaCrestaNuevaViejoDTO update(ZonaCrestaNuevaViejoDTO entity) {
    				LogDeMidasEJB3.log("updating ZonaCrestaNuevaViejoDTO instance", Level.INFO, null);
	        try {
            ZonaCrestaNuevaViejoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ZonaCrestaNuevaViejoDTO findById( ZonaCrestaNuevaViejoId id) {
    				LogDeMidasEJB3.log("finding ZonaCrestaNuevaViejoDTO instance with id: " + id, Level.INFO, null);
	        try {
            ZonaCrestaNuevaViejoDTO instance = entityManager.find(ZonaCrestaNuevaViejoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ZonaCrestaNuevaViejoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ZonaCrestaNuevaViejoDTO property to query
	  @param value the property value to match
	  	  @return List<ZonaCrestaNuevaViejoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ZonaCrestaNuevaViejoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ZonaCrestaNuevaViejoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ZonaCrestaNuevaViejoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ZonaCrestaNuevaViejoDTO entities.
	  	  @return List<ZonaCrestaNuevaViejoDTO> all ZonaCrestaNuevaViejoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ZonaCrestaNuevaViejoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ZonaCrestaNuevaViejoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ZonaCrestaNuevaViejoDTO as model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	public ZonaCrestaNuevaViejoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding ZonaCrestaNuevaViejoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			ZonaCrestaNuevaViejoDTO instance = entityManager.find(ZonaCrestaNuevaViejoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ZonaCrestaNuevaViejoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public ZonaCrestaNuevaViejoDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<ZonaCrestaNuevaViejoDTO> listRelated(Object id) {
		return this.findAll();
	}
}