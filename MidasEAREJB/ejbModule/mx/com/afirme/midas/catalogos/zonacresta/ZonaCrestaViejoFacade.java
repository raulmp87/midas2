package mx.com.afirme.midas.catalogos.zonacresta;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity ZonaCrestaViejoDTO.
 * @see .ZonaCrestaViejoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ZonaCrestaViejoFacade  implements ZonaCrestaViejoFacadeRemote {
    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ZonaCrestaViejoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ZonaCrestaViejoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ZonaCrestaViejoDTO entity) {
    				LogDeMidasEJB3.log("saving ZonaCrestaViejoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ZonaCrestaViejoDTO entity.
	  @param entity ZonaCrestaViejoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    @SuppressWarnings("unchecked")
	public void delete(ZonaCrestaViejoDTO entity) {
    				LogDeMidasEJB3.log("deleting ZonaCrestaViejoDTO instance", Level.INFO, null);
		 try {
		        BigDecimal id = entity.getIdtczonacrestaviejo();
		        ZonaCrestaNuevaViejoId idTr;
		        final String queryString = "select model from ZonaCrestaNuevaViejoDTO as model" +
		        							" where model.id.idtczonacrestaviejo = :idCresta";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idCresta",id);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				List<ZonaCrestaNuevaViejoDTO> listaTr = query.getResultList();
				for(int indiceLista = 0; indiceLista < listaTr.size(); indiceLista++){
					idTr = listaTr.get(indiceLista).getId();
					ZonaCrestaNuevaViejoDTO entidad = entityManager.find(ZonaCrestaNuevaViejoDTO.class, idTr);
					entityManager.remove(entidad);
				}
		        
	        	entity = entityManager.getReference(ZonaCrestaViejoDTO.class, id);
	            entityManager.remove(entity);
	        			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
	    				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
	    }			
    				
    	/**
	        try {
        	entity = entityManager.getReference(ZonaCrestaViejoDTO.class, entity.getIdtczonacrestaviejo());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
        **/
    }
    
    /**
	 Persist a previously saved ZonaCrestaViejoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ZonaCrestaViejoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ZonaCrestaViejoDTO entity to update
	 @return ZonaCrestaViejoDTO the persisted ZonaCrestaViejoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ZonaCrestaViejoDTO update(ZonaCrestaViejoDTO entity) {
    				LogDeMidasEJB3.log("updating ZonaCrestaViejoDTO instance", Level.INFO, null);
	        try {
            ZonaCrestaViejoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ZonaCrestaViejoDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding ZonaCrestaViejoDTO instance with id: " + id, Level.INFO, null);
	        try {
            ZonaCrestaViejoDTO instance = entityManager.find(ZonaCrestaViejoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ZonaCrestaViejoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ZonaCrestaViejoDTO property to query
	  @param value the property value to match
	  	  @return List<ZonaCrestaViejoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ZonaCrestaViejoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ZonaCrestaViejoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ZonaCrestaViejoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ZonaCrestaViejoDTO entities.
	  	  @return List<ZonaCrestaViejoDTO> all ZonaCrestaViejoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ZonaCrestaViejoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ZonaCrestaViejoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ZonaCrestaViejoDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ZonaCrestaViejoDTO> buscarFiltradoSegunIdNuevo(BigDecimal idNuevo){
		LogDeMidasEJB3.log("Buscando ZonasCresta Viejas Segun ZonasCresta nueva", Level.INFO, null);
		try {
			final String queryString = "select model " +
										"from ZonaCrestaViejoDTO model," +
										"		ZonaCrestaDTO nuevo," +
										"		ZonaCrestaNuevaViejoDTO tr " +
										"where model.idtczonacrestaviejo = tr.id.idtczonacrestaviejo " +
										"	and tr.id.idtczonacresta = nuevo.idtczonacresta" +
										"	and nuevo.idtczonacresta = :idZonaNuevo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idZonaNuevo", idNuevo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();

		}catch (RuntimeException re) {
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ZonaCrestaViejoDTO> listarFiltrado(ZonaCrestaViejoDTO zonaCrestaViejoDTO){
		try {
			String queryString = "select model from ZonaCrestaViejoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (zonaCrestaViejoDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "geocodigoviejo", zonaCrestaViejoDTO.getGeocodigoviejo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "nombreareaviejo", zonaCrestaViejoDTO.getNombreareaviejo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "numeroareaviejo", zonaCrestaViejoDTO.getNumeroareaviejo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipozona", zonaCrestaViejoDTO.getTipozona());
			
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public ZonaCrestaViejoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public ZonaCrestaViejoDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<ZonaCrestaViejoDTO> listRelated(Object id) {
		return this.findAll();
	}
}