package mx.com.afirme.midas.catalogos.clasificacionembarcacion;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity ClasificacionEmbarcacionDTO.
 * 
 * @see .ClasificacionEmbarcacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ClasificacionEmbarcacionFacade implements
		ClasificacionEmbarcacionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved ClasificacionEmbarcacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            ClasificacionEmbarcacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ClasificacionEmbarcacionDTO entity) {
		LogDeMidasEJB3.log("saving ClasificacionEmbarcacionDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ClasificacionEmbarcacionDTO entity.
	 * 
	 * @param entity
	 *            ClasificacionEmbarcacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ClasificacionEmbarcacionDTO entity) {
		LogDeMidasEJB3.log("deleting ClasificacionEmbarcacionDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(ClasificacionEmbarcacionDTO.class,
					entity.getIdTcClasificacionEmbarcacion());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ClasificacionEmbarcacionDTO entity and return it
	 * or a copy of it to the sender. A copy of the ClasificacionEmbarcacionDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            ClasificacionEmbarcacionDTO entity to update
	 * @return ClasificacionEmbarcacionDTO the persisted ClasificacionEmbarcacionDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ClasificacionEmbarcacionDTO update(ClasificacionEmbarcacionDTO entity) {
		LogDeMidasEJB3.log("updating ClasificacionEmbarcacionDTO instance", Level.INFO,
				null);
		try {
			ClasificacionEmbarcacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ClasificacionEmbarcacionDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding ClasificacionEmbarcacionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			ClasificacionEmbarcacionDTO instance = entityManager.find(
					ClasificacionEmbarcacionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ClasificacionEmbarcacionDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the ClasificacionEmbarcacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ClasificacionEmbarcacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ClasificacionEmbarcacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding ClasificacionEmbarcacionDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ClasificacionEmbarcacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ClasificacionEmbarcacionDTO entities.
	 * 
	 * @return List<ClasificacionEmbarcacionDTO> all ClasificacionEmbarcacionDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<ClasificacionEmbarcacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all ClasificacionEmbarcacionDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from ClasificacionEmbarcacionDTO model " +
					"order by model.descripcionClasifEmbarcacion";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find filtered ClasificacionEmbarcacionDTO entities.
	 * 
	 * @return List<ClasificacionEmbarcacionDTO> filtered ClasificacionEmbarcacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ClasificacionEmbarcacionDTO> listarFiltrado(ClasificacionEmbarcacionDTO clasificacionEmbarcacionDTO) {		
		try {
			String queryString = "select model from ClasificacionEmbarcacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (clasificacionEmbarcacionDTO == null)
				return null;
						
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionClasifEmbarcacion", clasificacionEmbarcacionDTO.getDescripcionClasifEmbarcacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoClasificacionEmbarcacion", clasificacionEmbarcacionDTO.getCodigoClasificacionEmbarcacion());		
			
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public ClasificacionEmbarcacionDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public ClasificacionEmbarcacionDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<ClasificacionEmbarcacionDTO> listRelated(Object id) {
		return this.findAll();
	}

}