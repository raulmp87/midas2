package mx.com.afirme.midas.catalogos.tipodocumentosiniestro;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity TipoDocumentoSiniestro.
 * @see .TipoDocumentoSiniestro
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class TipoDocumentoSiniestroFacade  implements TipoDocumentoSiniestroFacadeRemote {
	//property constants

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved TipoDocumentoSiniestro entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoDocumentoSiniestro entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoDocumentoSiniestroDTO entity) {
    				LogUtil.log("saving TipoDocumentoSiniestroDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent TipoDocumentoSiniestro entity.
	  @param entity TipoDocumentoSiniestro entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoDocumentoSiniestroDTO entity) {
    				LogUtil.log("deleting TipoDocumentoSiniestroDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(TipoDocumentoSiniestroDTO.class, entity.getIdTcTipoDocumentoSiniestro());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved TipoDocumentoSiniestro entity and return it or a copy of it to the sender. 
	 A copy of the TipoDocumentoSiniestro entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoDocumentoSiniestro entity to update
	 @return TipoDocumentoSiniestro the persisted TipoDocumentoSiniestro entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public TipoDocumentoSiniestroDTO update(TipoDocumentoSiniestroDTO entity) {
    				LogUtil.log("updating TipoDocumentoSiniestroDTO instance", Level.INFO, null);
	        try {
	        	TipoDocumentoSiniestroDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public TipoDocumentoSiniestroDTO findById( BigDecimal id) {
    				LogUtil.log("finding TipoDocumentoSiniestroDTO instance with id: " + id, Level.INFO, null);
	        try {
	        	TipoDocumentoSiniestroDTO instance = entityManager.find(TipoDocumentoSiniestroDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all TipoDocumentoSiniestro entities with a specific property value.  
	 
	  @param propertyName the name of the TipoDocumentoSiniestro property to query
	  @param value the property value to match
	  	  @return List<TipoDocumentoSiniestro> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<TipoDocumentoSiniestroDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding TipoDocumentoSiniestroDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from TipoDocumentoSiniestroDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all TipoDocumentoSiniestro entities.
	  	  @return List<TipoDocumentoSiniestro> all TipoDocumentoSiniestro entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoDocumentoSiniestroDTO> findAll(
		) {
					LogUtil.log("finding all TipoDocumentoSiniestroDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from TipoDocumentoSiniestroDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}