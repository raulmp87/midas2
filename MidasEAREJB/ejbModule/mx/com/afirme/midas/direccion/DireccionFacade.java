package mx.com.afirme.midas.direccion;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity DireccionDTO.
 * @see .DireccionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DireccionFacade  implements DireccionFacadeRemote {

    @PersistenceContext private EntityManager entityManager;

    @EJB
    private EstadoFacadeRemote estadoFacade;
    
    @EJB
    private MunicipioFacadeRemote municipioFacade;
    
    @EJB
    private ColoniaFacadeRemote coloniaFacade;
    
		/**
	 Perform an initial save of a previously unsaved DireccionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DireccionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public DireccionDTO save(DireccionDTO entity) {
    				LogDeMidasEJB3.log("saving DireccionDTO instance", Level.INFO, null);
	        try {
	        if(org.apache.commons.lang.StringUtils.isEmpty(entity.getNumeroExterior()))
	        	entity.setNumeroExterior("0");
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
            return entity;
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DireccionDTO entity.
	  @param entity DireccionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DireccionDTO entity) {
    				LogDeMidasEJB3.log("deleting DireccionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DireccionDTO.class, entity.getIdToDireccion());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DireccionDTO entity and return it or a copy of it to the sender. 
	 A copy of the DireccionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DireccionDTO entity to update
	 @return DireccionDTO the persisted DireccionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DireccionDTO update(DireccionDTO entity) {
    				LogDeMidasEJB3.log("updating DireccionDTO instance", Level.INFO, null);
	        try {
	        	if(org.apache.commons.lang.StringUtils.isEmpty(entity.getNumeroExterior()))
		        	entity.setNumeroExterior("0");
            DireccionDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DireccionDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding DireccionDTO instance with id: " + id, Level.INFO, null);
	        try {
            DireccionDTO instance = entityManager.find(DireccionDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DireccionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DireccionDTO property to query
	  @param value the property value to match
	  	  @return List<DireccionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DireccionDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding DireccionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DireccionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all DireccionDTO entities.
	  	  @return List<DireccionDTO> all DireccionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DireccionDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all DireccionDTO instances", Level.INFO, null);
			try {
				final String queryString = "select model from DireccionDTO model";
				Query query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	/**
	 * Find a DireccionDTO entity related with a specific CotizacionDTO.
	 * CotizacionDTO contains three direccionDTO records. The found record is indicated by
	 * the param int direccionOrden in the next order:
	 * 1 - Direccion contratante.
	 * 2 - Direccion cobro.
	 * 3 - Direccion asegurado.    
	 * @param int direccionOrden
	 * @param BigDecimal idToCotizacion
	  	  @return DireccionDTO found by query
	 */
	@SuppressWarnings("unchecked")
	public DireccionDTO findByIdCotizacion(BigDecimal idToCotizacion,int direccionOrden){
		LogDeMidasEJB3.log("finding DireccionDTO instance related with Cotizacion: " + idToCotizacion, Level.INFO, null);
		try {
			String queryString = "select model from DireccionDTO model where model.idToDireccion in ";
			boolean error=false;
			DireccionDTO direccion = null;
			if (direccionOrden == 1){
				queryString += "(select cot.direccionContratanteDTO.idToDireccion from CotizacionDTO cot where cot.idToCotizacion = :idToCotizacion)";
			}else if (direccionOrden == 2){
				queryString += "(select cot.direccionCobroDTO.idToDireccion from CotizacionDTO cot where cot.idToCotizacion = :idToCotizacion)";
			}else if (direccionOrden == 3){
				queryString +="(select cot.direccionAseguradoDTO.idToDireccion from CotizacionDTO cot where cot.idToCotizacion = :idToCotizacion)";
			}else
				error = true;
			if (!error){
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idToCotizacion", idToCotizacion);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				List<DireccionDTO> lista = query.getResultList();
				if (!lista.isEmpty())
					direccion = (DireccionDTO) lista.get(0); 
			}
			return direccion;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by idToCotizacion failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find a DireccionDTO entity related with a specific ProveedorInspeccionDTO.
	 * @param BigDecimal idToProveedorInspeccion
	  	  @return DireccionDTO found by query
	 */
	public DireccionDTO findByIdProveedorInspeccion(BigDecimal idToProveedorInspeccion){
		LogDeMidasEJB3.log("finding DireccionDTO instance related with ProveedorInspeccion: " + idToProveedorInspeccion, Level.INFO, null);
		try {
			String queryString = "select model from DireccionDTO model where model.idToDireccion in " +
					"(select prov.direccionDTO.idToDireccion from ProveedorInspeccionDTO prov where prov.idToProveedorInspeccion = :idToProveedorInspeccion)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToProveedorInspeccion", idToProveedorInspeccion);
			DireccionDTO direccion = (DireccionDTO) query.getSingleResult();
			return direccion;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find DireccionDTO by idToProveedorInspeccion failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BigDecimal> listarIdToDirecciones(DireccionDTO direccionDTO){
		if(direccionDTO==null){
			return null;
		}
		
		try{	
			String queryString = "select model.idToDireccion from DireccionDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreCalle", direccionDTO.getNombreCalle());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idEstado", direccionDTO.getIdEstado());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idMunicipio", direccionDTO.getIdMunicipio());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "nombreColonia", direccionDTO.getNombreColonia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoPostal", direccionDTO.getCodigoPostal());
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
		
	}
	
	/**
	 * Consulta los nombres de estado, ciudad y colonia y los almacena en los atributos estado, ciudad y colonia respectivamente.
	 * @param direccionDTO con los id's de estado, ciudad y colonia.
	 * @return direccionDTO con los nombres de estado, ciudad y colonia.
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public DireccionDTO poblarDatosDireccion(DireccionDTO direccionDTO){
		if (direccionDTO != null){
			if (direccionDTO.getIdEstado() != null){
				EstadoDTO estadoDTO = estadoFacade.findById(direccionDTO.getIdEstado().toBigInteger().toString());
				if(estadoDTO != null){
					direccionDTO.setEstado(estadoDTO.getStateName());
				}
				else{
					direccionDTO.setEstado("");
				}
			}
			if (direccionDTO.getIdMunicipio() != null){
				MunicipioDTO municipio = municipioFacade.findById(direccionDTO.getIdMunicipio().toBigInteger().toString());
				if(municipio != null){
					direccionDTO.setMunicipio(municipio.getMunicipalityName());
				}
				else{
					direccionDTO.setMunicipio("");
				}				
			}
			if (direccionDTO.getNombreColonia() != null){
				ColoniaDTO colonia = coloniaFacade.findById(direccionDTO.getNombreColonia());
				if(colonia != null){
					direccionDTO.setColonia(colonia.getColonyName());
				}
				else{
					direccionDTO.setColonia("");
				}
			}
		}
		return direccionDTO;
	}
	
	
}