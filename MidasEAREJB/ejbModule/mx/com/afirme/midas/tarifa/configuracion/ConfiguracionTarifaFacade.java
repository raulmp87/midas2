package mx.com.afirme.midas.tarifa.configuracion;

// default package

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ConfiguracionTarifaDTO.
 * 
 * @see .ConfiguracionTarifaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ConfiguracionTarifaFacade implements
		ConfiguracionTarifaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved ConfiguracionTarifaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            ConfiguracionTarifaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ConfiguracionTarifaDTO entity) {
		LogDeMidasEJB3.log("saving ConfiguracionTarifaDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ConfiguracionTarifaDTO entity.
	 * 
	 * @param entity
	 *            ConfiguracionTarifaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ConfiguracionTarifaDTO entity) {
		LogDeMidasEJB3.log("deleting ConfiguracionTarifaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(ConfiguracionTarifaDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ConfiguracionTarifaDTO entity and return it or
	 * a copy of it to the sender. A copy of the ConfiguracionTarifaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            ConfiguracionTarifaDTO entity to update
	 * @return ConfiguracionTarifaDTO the persisted ConfiguracionTarifaDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ConfiguracionTarifaDTO update(ConfiguracionTarifaDTO entity) {
		LogDeMidasEJB3.log("updating ConfiguracionTarifaDTO instance",
				Level.INFO, null);
		try {
			ConfiguracionTarifaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ConfiguracionTarifaDTO findById(ConfiguracionTarifaId id) {
		LogDeMidasEJB3.log("finding ConfiguracionTarifaDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			ConfiguracionTarifaDTO instance = entityManager.find(
					ConfiguracionTarifaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ConfiguracionTarifaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ConfiguracionTarifaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ConfiguracionTarifaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ConfiguracionTarifaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding ConfiguracionTarifaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ConfiguracionTarifaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ConfiguracionTarifaDTO entities.
	 * 
	 * @return List<ConfiguracionTarifaDTO> all ConfiguracionTarifaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConfiguracionTarifaDTO> findAll() {
		LogDeMidasEJB3.log("finding all ConfiguracionTarifaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from ConfiguracionTarifaDTO model";
			Query query = entityManager.createQuery(queryString);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find all ConfiguracionTarifaDTO entities with depends of a filter. 
	 * @param entity. the name of the entity ProductDTO
	 * @return ProductoDTO found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ConfiguracionTarifaDTO> listarFiltrado(ConfiguracionTarifaDTO entity) {
		try {
			String queryString = "select model from ConfiguracionTarifaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToRiesgo", entity.getId().getIdToRiesgo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idConcepto", entity.getId().getIdConcepto());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idDato", entity.getId().getIdDato());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "claseRemota", entity.getClaseRemota());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "etiqueta", entity.getEtiqueta());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveTipoValidacion", entity.getClaveTipoValidacion());
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		}catch (RuntimeException re){
			LogDeMidasEJB3.log("listarFiltrado failed", Level.SEVERE, re);
			throw re;			
		}
	}

}