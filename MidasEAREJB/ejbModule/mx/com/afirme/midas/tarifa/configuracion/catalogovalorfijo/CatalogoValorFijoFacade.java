package mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity catalogoValorFijo.
 * @see .catalogoValorFijo
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class CatalogoValorFijoFacade  implements CatalogoValorFijoFacadeRemote {
	//property constants
	public static final String DESCRIPCION = "descripcion";





    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved catalogoValorFijo entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity catalogoValorFijo entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CatalogoValorFijoDTO entity) {
    				LogDeMidasEJB3.log("saving CatalogoValorFijoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CatalogoValorFijoDTO entity.
	  @param entity CatalogoValorFijoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CatalogoValorFijoDTO entity) {
    				LogDeMidasEJB3.log("deleting CatalogoValorFijoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(CatalogoValorFijoDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CatalogoValorFijoDTO entity and return it or a copy of it to the sender. 
	 A copy of the CatalogoValorFijoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CatalogoValorFijoDTO entity to update
	 @return CatalogoValorFijoDTO the persisted CatalogoValorFijoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CatalogoValorFijoDTO update(CatalogoValorFijoDTO entity) {
    				LogDeMidasEJB3.log("updating CatalogoValorFijoDTO instance", Level.INFO, null);
	        try {
            CatalogoValorFijoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public CatalogoValorFijoDTO findById( CatalogoValorFijoId id) {
    				LogDeMidasEJB3.log("finding CatalogoValorFijoDTO instance with id: " + id, Level.INFO, null);
	        try {
            CatalogoValorFijoDTO instance = entityManager.find(CatalogoValorFijoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all CatalogoValorFijoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the CatalogoValorFijoDTO property to query
	  @param value the property value to match
	  	  @return List<CatalogoValorFijoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<CatalogoValorFijoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding CatalogoValorFijoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
					final String queryString = "select model from CatalogoValorFijoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
					Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					//Obliga al persistence context a refrescar su cache con los datos del query en la BD
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	public List<CatalogoValorFijoDTO> findByDescripcion(Object descripcion
	) {
		return findByProperty(DESCRIPCION, descripcion
		);
	}
	
	
	/**
	 * Find all CatalogoValorFijoDTO entities.
	  	  @return List<CatalogoValorFijoDTO> all CatalogoValorFijoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CatalogoValorFijoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all CatalogoValorFijoDTO instances", Level.INFO, null);
			try {
					final String queryString = "select model from CatalogoValorFijoDTO model";
					Query query = entityManager.createQuery(queryString);
					//Obliga al persistence context a refrescar su cache con los datos del query en la BD
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	public CatalogoValorFijoDTO findById(BigDecimal arg0) {
		return null;
	}

	public CatalogoValorFijoDTO findById(double arg0) {
		return null;
	}

	public List<CatalogoValorFijoDTO> listRelated(Object id) {
		return findAll();
	}
	
	/**
	 * Busca un registro de CatalogoValorFijo usando sus id�s y devuelve la
	 * descripci�n del cat�logo
	 * 
	 * @param int idGrupoValores, el grupo de valores del catalogo de valores
	 *        fijos.
	 * @param int idDato, el ID del registro con el grupo de valores indicado.
	 * @return String result La descripci�n del registro CatalogoValorFijo, si
	 *         no encuentra el registro devuelve una cadena vac�a.
	 */
	public String getDescripcionCatalogoValorFijo(int idGrupoValores,int idDato) {
		String result = "";
		CatalogoValorFijoId id = new CatalogoValorFijoId(idGrupoValores, idDato);
		try {
			CatalogoValorFijoDTO catalogoValorFijoDTO = findById(id);
			result = catalogoValorFijoDTO.getDescripcion();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Busca un registro de CatalogoValorFijo usando sus id�s y devuelve la
	 * descripci�n del cat�logo
	 * 
	 * @param int idGrupoValores, el grupo de valores del catalogo de valores
	 *        fijos.
	 * @param String
	 *            idDato, el ID del registro con el grupo de valores indicado.
	 * @return String result La descripci�n del registro CatalogoValorFijo, si
	 *         no encuentra el registro devuelve una cadena vac�a.
	 */
	public String getDescripcionCatalogoValorFijo(int idGrupoValores,String idDato) {
		String result = "";
		result = getDescripcionCatalogoValorFijo(idGrupoValores, Integer.valueOf(idDato));
		return result;
	}

	@Override
	public List<CatalogoValorFijoDTO> findAll(BigDecimal... arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CatalogoValorFijoDTO findByIdGrupoValoresAndDescripcion(
			int idGrupoValores, String descripcion) {
		String jpql = "select m from CatalogoValorFijoDTO m where m.id.idGrupoValores = :idGrupoValores and m.descripcion = :descripcion";
		TypedQuery<CatalogoValorFijoDTO> query = entityManager.createQuery(jpql, CatalogoValorFijoDTO.class);
		query.setParameter("idGrupoValores", idGrupoValores);
		query.setParameter("descripcion", descripcion);
		List<CatalogoValorFijoDTO> list = query.getResultList();
		if (list.size() > 1) {
			throw new RuntimeException("Mas de un resultado.");
		}
		if (list.size() == 1) {
			return list.get(0);
		}
		return null;
	}
	
}