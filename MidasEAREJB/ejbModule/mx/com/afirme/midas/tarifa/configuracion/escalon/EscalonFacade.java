package mx.com.afirme.midas.tarifa.configuracion.escalon;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas.tarifa.configuracion.escalon.EscalonDTO;

/**
 * Facade for entity EscalonDTO.
 * @see .EscalonDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class EscalonFacade  implements EscalonFacadeRemote {
	//property constants
	public static final String CLAVE_CLASIFICACION_ESCALON = "claveClasificacionEscalon";
	public static final String VALOR_RANGO_INFERIOR = "valorRangoInferior";
	public static final String VALOR_RANGO_SUPERIOR = "valorRangoSuperior";





    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved EscalonDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EscalonDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EscalonDTO entity) {
    				LogDeMidasEJB3.log("saving EscalonDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent EscalonDTO entity.
	  @param entity EscalonDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EscalonDTO entity) {
    				LogDeMidasEJB3.log("deleting EscalonDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(EscalonDTO.class, entity.getIdToEscalon());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved EscalonDTO entity and return it or a copy of it to the sender. 
	 A copy of the EscalonDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EscalonDTO entity to update
	 @return EscalonDTO the persisted EscalonDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public EscalonDTO update(EscalonDTO entity) {
    				LogDeMidasEJB3.log("updating EscalonDTO instance", Level.INFO, null);
	        try {
            EscalonDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public EscalonDTO findById( double id) {
    				LogDeMidasEJB3.log("finding EscalonDTO instance with id: " + id, Level.INFO, null);
	        try {
            EscalonDTO instance = entityManager.find(EscalonDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all EscalonDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EscalonDTO property to query
	  @param value the property value to match
	  	  @return List<EscalonDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<EscalonDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding EscalonDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
					final String queryString = "select model from EscalonDTO model where model." 
			 						+ propertyName + "= :propertyValue";
					Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					//Obliga al persistence context a refrescar su cache con los datos del query en la BD
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	
	/**
	 * Find all EscalonDTO entities.
	  	  @return List<EscalonDTO> all EscalonDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<EscalonDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all EscalonDTO instances", Level.INFO, null);
			try {
					final String queryString = "select model from EscalonDTO model";
					Query query = entityManager.createQuery(queryString);
					//Obliga al persistence context a refrescar su cache con los datos del query en la BD
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
/**
 * This method is implemented because EscalonFacadeRemote extends MidasInterfaceBase
 * 
 * */
	public EscalonDTO findById(BigDecimal id) {
		return findById(Double.valueOf(id.toString()));
	}

	/**
	 * This method is implemented because EscalonFacadeRemote extends MidasInterfaceBase
	 * 
	 * */
	public EscalonDTO findById(CatalogoValorFijoId arg0) {
		return null;
	}

	public List<EscalonDTO> listRelated(Object id) {
		return findAll();
	}
	
}