package mx.com.afirme.midas.tarifa;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.TarifaDTO;
import mx.com.afirme.midas.tarifa.TarifaFacadeRemote;
import mx.com.afirme.midas.tarifa.TarifaId;

/**
 * Facade for entity TarifaDTO.
 * 
 * @see .TarifaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TarifaFacade implements TarifaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TarifaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TarifaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TarifaDTO entity) {
		LogDeMidasEJB3.log("saving TarifaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			entityManager.flush();
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TarifaDTO entity.
	 * 
	 * @param entity
	 *            TarifaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TarifaDTO entity) {
		LogDeMidasEJB3.log("deleting TarifaDTO instance", Level.INFO, null);
		try {
			entity = entityManager
					.getReference(TarifaDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TarifaDTO entity and return it or a copy of it
	 * to the sender. A copy of the TarifaDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            TarifaDTO entity to update
	 * @return TarifaDTO the persisted TarifaDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TarifaDTO update(TarifaDTO entity) {
		LogDeMidasEJB3.log("updating TarifaDTO instance", Level.INFO, null);
		try {
			TarifaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TarifaDTO findById(TarifaId id) {
		LogDeMidasEJB3.log("finding TarifaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TarifaDTO instance = entityManager.find(TarifaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TarifaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TarifaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TarifaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TarifaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TarifaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TarifaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TarifaDTO entities.
	 * 
	 * @return List<TarifaDTO> all TarifaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TarifaDTO> findAll() {
		LogDeMidasEJB3.log("finding all TarifaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from TarifaDTO model";
			entityManager.flush();
			Query query = entityManager.createQuery(queryString);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find TarifaDTO entities with the received idToRiesgo and idConcepto and version. 
	 * @param idToRiesgo
	 * @param idConcepto
	 * @param version
	 * @return List<TarifaDTO> all TarifaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TarifaDTO> findByIdToRiesgoIdConceptoVersion(BigDecimal idToRiesgo,String idConcepto, Long version) {
		LogDeMidasEJB3.log("finding TarifaDTO instances with idToRiesgo: "+idToRiesgo+" and idConcepto: "+idConcepto+" and version: "+version, Level.INFO, null);
		try {
			final String queryString = "select model from TarifaDTO model where model.id.idToRiesgo = :idToRiesgo and model.id.idConcepto = :idConcepto " +
					" and model.id.version = :version";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToRiesgo", idToRiesgo);
			query.setParameter("idConcepto", idConcepto);
			query.setParameter("version", version);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by idToRiesgo and idConcepto and version failed", Level.SEVERE, re);
			throw re;
		}
	}
}