package mx.com.afirme.midas.danios;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ReaseguroIncisoDetalleCoberturaCotizacionDTO.
 * 
 * @see .ReaseguroIncisoDetalleCoberturaCotizacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ReaseguroIncisoDetalleCoberturaCotizacionFacade implements
		ReaseguroIncisoDetalleCoberturaCotizacionFacadeRemote {
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * ReaseguroIncisoDetalleCoberturaCotizacionDTO entity. All subsequent
	 * persist actions of this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ReaseguroIncisoDetalleCoberturaCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public ReaseguroIncisoDetalleCoberturaCotizacionDTO save(ReaseguroIncisoDetalleCoberturaCotizacionDTO entity) {
		LogDeMidasEJB3.log(
				"saving ReaseguroIncisoDetalleCoberturaCotizacionDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ReaseguroIncisoDetalleCoberturaCotizacionDTO entity.
	 * 
	 * @param entity
	 *            ReaseguroIncisoDetalleCoberturaCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ReaseguroIncisoDetalleCoberturaCotizacionDTO entity) {
		LogDeMidasEJB3.log(
						"deleting ReaseguroIncisoDetalleCoberturaCotizacionDTO instance",
						Level.INFO, null);
		try {
			entity = entityManager.getReference(
					ReaseguroIncisoDetalleCoberturaCotizacionDTO.class, entity
							.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ReaseguroIncisoDetalleCoberturaCotizacionDTO
	 * entity and return it or a copy of it to the sender. A copy of the
	 * ReaseguroIncisoDetalleCoberturaCotizacionDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ReaseguroIncisoDetalleCoberturaCotizacionDTO entity to update
	 * @return ReaseguroIncisoDetalleCoberturaCotizacionDTO the persisted
	 *         ReaseguroIncisoDetalleCoberturaCotizacionDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ReaseguroIncisoDetalleCoberturaCotizacionDTO update(
			ReaseguroIncisoDetalleCoberturaCotizacionDTO entity) {
		LogDeMidasEJB3.log(
						"updating ReaseguroIncisoDetalleCoberturaCotizacionDTO instance",
						Level.INFO, null);
		try {
			ReaseguroIncisoDetalleCoberturaCotizacionDTO result = entityManager
					.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ReaseguroIncisoDetalleCoberturaCotizacionDTO findById(
			ReaseguroIncisoDetalleCoberturaCotizacionId id) {
		LogDeMidasEJB3.log(
				"finding ReaseguroIncisoDetalleCoberturaCotizacionDTO instance with id: "
						+ id, Level.INFO, null);
		try {
			ReaseguroIncisoDetalleCoberturaCotizacionDTO instance = entityManager
					.find(ReaseguroIncisoDetalleCoberturaCotizacionDTO.class,
							id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ReaseguroIncisoDetalleCoberturaCotizacionDTO entities with a
	 * specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ReaseguroIncisoDetalleCoberturaCotizacionDTO
	 *            property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> findByProperty(
			String propertyName, final Object value,
			final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log(
				"finding ReaseguroIncisoDetalleCoberturaCotizacionDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ReaseguroIncisoDetalleCoberturaCotizacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ReaseguroIncisoDetalleCoberturaCotizacionDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> all
	 *         ReaseguroIncisoDetalleCoberturaCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> findAll(
			final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log(
						"finding all ReaseguroIncisoDetalleCoberturaCotizacionDTO instances",
						Level.INFO, null);
		try {
			final String queryString = "select model from ReaseguroIncisoDetalleCoberturaCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find filtered ReaseguroDetalleCoberturaCotizacionDTO entities.
	  	  @return List<ConfiguracionLineaDTO> filtered ReaseguroDetalleCoberturaCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> listarFiltrado(ReaseguroIncisoDetalleCoberturaCotizacionDTO entity){
		LogDeMidasEJB3.log(
				"filtering ReaseguroIncisoDetalleCoberturaCotizacionDTO instance",
						Level.INFO, null);
		try {
			String queryString = "select model from ReaseguroIncisoDetalleCoberturaCotizacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCotizacion", entity.getId().getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idTcSubRamo", entity.getId().getIdTcSubRamo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCobertura", entity.getId().getIdToCobertura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.numeroInciso", entity.getId().getNumeroInciso());
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}		
	}

	@SuppressWarnings("unchecked")
	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> obtenerDetalleIncisoCoberturasCotizacion(BigDecimal idCotizacion, BigDecimal idSubramo,
			BigDecimal numeroInciso) {
		
			LogDeMidasEJB3.log("finding ReaseguroIncisoDetalleCoberturaCotizacionDTO instances with idCotizacion: "
					+ idCotizacion + ", and idSubramo: " + idSubramo + ", and numeroInciso: " + numeroInciso, Level.INFO, null);
			try {
				final String queryString = "SELECT model FROM ReaseguroIncisoDetalleCoberturaCotizacionDTO model WHERE model.id.idToCotizacion = :idCotizacion"+
										   " AND model.id.idTcSubRamo = :idSubramo AND model.id.numeroInciso = :numeroInciso";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idCotizacion", idCotizacion);
				query.setParameter("idSubramo", idSubramo);
				query.setParameter("numeroInciso", numeroInciso);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
			}catch(RuntimeException re){
				LogDeMidasEJB3.log("obtenerDetalleIncisoCoberturasCotizacion in ReaseguroIncisoDetalleCoberturaCotizacionFacade failed", Level.SEVERE, re);
				throw re;
			}
	}

}