package mx.com.afirme.midas.danios;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ReaseguroDetalleCoberturaCotizacionDTO.
 * 
 * @see .ReaseguroDetalleCoberturaCotizacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ReaseguroDetalleCoberturaCotizacionFacade implements
		ReaseguroDetalleCoberturaCotizacionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * ReaseguroDetalleCoberturaCotizacionDTO entity. All subsequent persist
	 * actions of this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ReaseguroDetalleCoberturaCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public ReaseguroDetalleCoberturaCotizacionDTO save(ReaseguroDetalleCoberturaCotizacionDTO entity) {
		LogDeMidasEJB3.log("saving ReaseguroDetalleCoberturaCotizacionDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ReaseguroDetalleCoberturaCotizacionDTO entity.
	 * 
	 * @param entity
	 *            ReaseguroDetalleCoberturaCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ReaseguroDetalleCoberturaCotizacionDTO entity) {
		LogDeMidasEJB3.log("deleting ReaseguroDetalleCoberturaCotizacionDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					ReaseguroDetalleCoberturaCotizacionDTO.class, entity
							.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ReaseguroDetalleCoberturaCotizacionDTO entity
	 * and return it or a copy of it to the sender. A copy of the
	 * ReaseguroDetalleCoberturaCotizacionDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ReaseguroDetalleCoberturaCotizacionDTO entity to update
	 * @return ReaseguroDetalleCoberturaCotizacionDTO the persisted
	 *         ReaseguroDetalleCoberturaCotizacionDTO entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ReaseguroDetalleCoberturaCotizacionDTO update(
			ReaseguroDetalleCoberturaCotizacionDTO entity) {
		LogDeMidasEJB3.log("updating ReaseguroDetalleCoberturaCotizacionDTO instance",
				Level.INFO, null);
		try {
			ReaseguroDetalleCoberturaCotizacionDTO result = entityManager
					.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ReaseguroDetalleCoberturaCotizacionDTO findById(
			ReaseguroDetalleCoberturaCotizacionId id) {
		LogDeMidasEJB3.log(
				"finding ReaseguroDetalleCoberturaCotizacionDTO instance with id: "
						+ id, Level.INFO, null);
		try {
			ReaseguroDetalleCoberturaCotizacionDTO instance = entityManager
					.find(ReaseguroDetalleCoberturaCotizacionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ReaseguroDetalleCoberturaCotizacionDTO entities with a specific
	 * property value.
	 * 
	 * @param propertyName
	 *            the name of the ReaseguroDetalleCoberturaCotizacionDTO
	 *            property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<ReaseguroDetalleCoberturaCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguroDetalleCoberturaCotizacionDTO> findByProperty(
			String propertyName, final Object value,
			final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log(
				"finding ReaseguroDetalleCoberturaCotizacionDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ReaseguroDetalleCoberturaCotizacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ReaseguroDetalleCoberturaCotizacionDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ReaseguroDetalleCoberturaCotizacionDTO> all
	 *         ReaseguroDetalleCoberturaCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguroDetalleCoberturaCotizacionDTO> findAll(
			final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log(
				"finding all ReaseguroDetalleCoberturaCotizacionDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from ReaseguroDetalleCoberturaCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find filtered ReaseguroDetalleCoberturaCotizacionDTO entities.
	  	  @return List<ConfiguracionLineaDTO> filtered ReaseguroDetalleCoberturaCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguroDetalleCoberturaCotizacionDTO> listarFiltrado(ReaseguroDetalleCoberturaCotizacionDTO entity){
		LogDeMidasEJB3.log(
				"filtering ReaseguroDetalleCoberturaCotizacionDTO instance",
						Level.INFO, null);
		try {
			String queryString = "select model from ReaseguroDetalleCoberturaCotizacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCotizacion", entity.getId().getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idTcSubRamo", entity.getId().getIdTcSubRamo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCobertura", entity.getId().getIdToCobertura());
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<ReaseguroDetalleCoberturaCotizacionDTO> obtenerDetalleCoberturasCotizacion(BigDecimal idCotizacion, BigDecimal idSubramo) {
		LogDeMidasEJB3.log("finding ReaseguroDetalleCoberturaCotizacionDTO instances with idCotizacion: "
						+ idCotizacion + ", and idSubramo: " + idSubramo, Level.INFO, null);
		try {
			final String queryString = "SELECT model FROM ReaseguroDetalleCoberturaCotizacionDTO model WHERE model.id.idToCotizacion = :idCotizacion"+
									   " AND model.id.idTcSubRamo = :idSubramo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idCotizacion", idCotizacion);
			query.setParameter("idSubramo", idSubramo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		}catch(RuntimeException re){
			LogDeMidasEJB3.log("obtenerDetalleCoberturasCotizacion in ReaseguroDetalleCoberturaCotizacionFacade failed", Level.SEVERE, re);
			throw re;
		}
	}
}