package mx.com.afirme.midas.danios;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ReaseguroSubIncisoDetalleCoberturaCotizacionDTO.
 * 
 * @see .ReaseguroSubIncisoDetalleCoberturaCotizacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ReaseguroSubIncisoDetalleCoberturaCotizacionFacade implements
		ReaseguroSubIncisoDetalleCoberturaCotizacionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity. All subsequent
	 * persist actions of this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity to
	 *            persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public ReaseguroSubIncisoDetalleCoberturaCotizacionDTO save(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity) {
		LogDeMidasEJB3.log(
						"saving ReaseguroSubIncisoDetalleCoberturaCotizacionDTO instance",
						Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ReaseguroSubIncisoDetalleCoberturaCotizacionDTO
	 * entity.
	 * 
	 * @param entity
	 *            ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity to
	 *            delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity) {
		LogDeMidasEJB3.log(
						"deleting ReaseguroSubIncisoDetalleCoberturaCotizacionDTO instance",
						Level.INFO, null);
		try {
			entity = entityManager.getReference(
					ReaseguroSubIncisoDetalleCoberturaCotizacionDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved
	 * ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity and return it or a
	 * copy of it to the sender. A copy of the
	 * ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity to
	 *            update
	 * @return ReaseguroSubIncisoDetalleCoberturaCotizacionDTO the persisted
	 *         ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ReaseguroSubIncisoDetalleCoberturaCotizacionDTO update(
			ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity) {
		LogDeMidasEJB3.log(
						"updating ReaseguroSubIncisoDetalleCoberturaCotizacionDTO instance",
						Level.INFO, null);
		try {
			ReaseguroSubIncisoDetalleCoberturaCotizacionDTO result = entityManager
					.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ReaseguroSubIncisoDetalleCoberturaCotizacionDTO findById(
			ReaseguroSubIncisoDetalleCoberturaCotizacionId id) {
		LogDeMidasEJB3.log(
				"finding ReaseguroSubIncisoDetalleCoberturaCotizacionDTO instance with id: "
						+ id, Level.INFO, null);
		try {
			ReaseguroSubIncisoDetalleCoberturaCotizacionDTO instance = entityManager
					.find(
							ReaseguroSubIncisoDetalleCoberturaCotizacionDTO.class,
							id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entities with a
	 * specific property value.
	 * 
	 * @param propertyName
	 *            the name of the
	 *            ReaseguroSubIncisoDetalleCoberturaCotizacionDTO property to
	 *            query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> found by
	 *         query
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> findByProperty(
			String propertyName, final Object value,
			final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log(
						"finding ReaseguroSubIncisoDetalleCoberturaCotizacionDTO instance with property: "
								+ propertyName + ", value: " + value,
						Level.INFO, null);
		try {
			final String queryString = "select model from ReaseguroSubIncisoDetalleCoberturaCotizacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> all
	 *         ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> findAll(
			final int... rowStartIdxAndCount) {
		LogDeMidasEJB3.log(
						"finding all ReaseguroSubIncisoDetalleCoberturaCotizacionDTO instances",
						Level.INFO, null);
		try {
			final String queryString = "select model from ReaseguroSubIncisoDetalleCoberturaCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find filtered ReaseguroIncisoDetalleCoberturaCotizacionDTO entities.
	  	  @return List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> filtered reaseguroSubIncisoDetalleCoberturaCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> listarFiltrado(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity){
		LogDeMidasEJB3.log(
				"filtering ReaseguroDetalleCoberturaCotizacionDTO instance",
						Level.INFO, null);
		try {
			String queryString = "select model from ReaseguroSubIncisoDetalleCoberturaCotizacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCotizacion", entity.getId().getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idTcSubRamo", entity.getId().getIdTcSubRamo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCobertura", entity.getId().getIdToCobertura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToSeccion", entity.getId().getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.numeroInciso", entity.getId().getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.numeroSubInciso", entity.getId().getNumeroSubInciso());
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}		
	}

	@SuppressWarnings("unchecked")
	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> obtenerDetalleSubIncisosCoberturasCotizacion(BigDecimal idCotizacion, BigDecimal idSubramo,
			BigDecimal numeroInciso, BigDecimal idSeccion, BigDecimal numeroSubinciso){
			LogDeMidasEJB3.log("finding ReaseguroSubIncisoDetalleCoberturaCotizacionDTO instances with idCotizacion: "
					+ idCotizacion + ", idSubramo: " + idSubramo+ ", numeroInciso: " + numeroInciso+ ", idSeccion: " + idSeccion+ ", numeroSubinciso: " + numeroSubinciso, Level.INFO, null);
			
			try {
				final String queryString = "SELECT model FROM ReaseguroSubIncisoDetalleCoberturaCotizacionDTO model WHERE model.id.idToCotizacion = :idCotizacion"+
										   " AND model.id.idTcSubRamo = :idSubramo" +
										   " AND model.id.numeroInciso = :numeroInciso" +
										   " AND model.id.idToSeccion = :idSeccion" +
										   " AND model.id.numeroSubInciso = :numeroSubinciso";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idCotizacion", idCotizacion);
				query.setParameter("idSubramo", idSubramo);
				query.setParameter("numeroInciso", numeroInciso);
				query.setParameter("idSeccion", idSeccion);
				query.setParameter("numeroSubinciso", numeroSubinciso);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
			}catch(RuntimeException re){
				LogDeMidasEJB3.log("obtenerDetalleSubIncisosCoberturasCotizacion in ReaseguroSubIncisoDetalleCoberturaCotizacionFacade failed", Level.SEVERE, re);
				throw re;
			}
	}

}