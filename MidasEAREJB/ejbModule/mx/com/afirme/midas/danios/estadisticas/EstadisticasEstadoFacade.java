package mx.com.afirme.midas.danios.estadisticas;
// default package

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.usuario.LogUtil;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity EstadisticasEstado.
 * 
 * @see .EstadisticasEstado
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class EstadisticasEstadoFacade implements 
	EstadisticasEstadoFacadeRemote {

	protected UsuarioService usuarioService;
	
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Perform an initial save of a previously unsaved EstadisticasEstado
     * entity. All subsequent persist actions of this entity should use the
     * #update() method.
     * 
     * @param entity
     *            EstadisticasEstado entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(EstadisticasEstadoDTO entity) {
	LogUtil.log("saving EstadisticasEstado instance", Level.INFO, null);
	try {
	    entityManager.persist(entity);
	    LogUtil.log("save successful", Level.INFO, null);
	} catch (RuntimeException re) {
	    LogUtil.log("save failed", Level.SEVERE, re);
	    throw re;
	}
    }

    /**
     * Delete a persistent EstadisticasEstado entity.
     * 
     * @param entity
     *            EstadisticasEstado entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(EstadisticasEstadoDTO entity) {
	LogUtil.log("deleting EstadisticasEstado instance", Level.INFO, null);
	try {
	    entity = entityManager.getReference(EstadisticasEstadoDTO.class,
		    entity.getId());
	    entityManager.remove(entity);
	    LogUtil.log("delete successful", Level.INFO, null);
	} catch (RuntimeException re) {
	    LogUtil.log("delete failed", Level.SEVERE, re);
	    throw re;
	}
    }

    /**
     * Persist a previously saved EstadisticasEstado entity and return it or a
     * copy of it to the sender. A copy of the EstadisticasEstado entity
     * parameter is returned when the JPA persistence mechanism has not
     * previously been tracking the updated entity.
     * 
     * @param entity
     *            EstadisticasEstado entity to update
     * @return EstadisticasEstado the persisted EstadisticasEstado entity
     *         instance, may not be the same
     * @throws RuntimeException
     *             if the operation fails
     */
    public EstadisticasEstadoDTO update(EstadisticasEstadoDTO entity) {
	LogUtil.log("updating EstadisticasEstado instance", Level.INFO, null);
	try {
	    EstadisticasEstadoDTO result = entityManager.merge(entity);
	    LogUtil.log("update successful", Level.INFO, null);
	    return result;
	} catch (RuntimeException re) {
	    LogUtil.log("update failed", Level.SEVERE, re);
	    throw re;
	}
    }

    public EstadisticasEstadoDTO findById(EstadisticasEstadoId id) {
	LogUtil.log("finding EstadisticasEstado instance with id: " + id,
		Level.INFO, null);
	try {
	    EstadisticasEstadoDTO instance = entityManager.find(
		    EstadisticasEstadoDTO.class, id);
	    return instance;
	} catch (RuntimeException re) {
	    LogUtil.log("find failed", Level.SEVERE, re);
	    throw re;
	}
    }

    /**
     * Find all EstadisticasEstado entities with a specific property value.
     * 
     * @param propertyName
     *            the name of the EstadisticasEstado property to query
     * @param value
     *            the property value to match
     * @return List<EstadisticasEstado> found by query
     */
    @SuppressWarnings("unchecked")
    public List<EstadisticasEstadoDTO> findByProperty(String propertyName,
	    final Object value) {
	LogUtil.log("finding EstadisticasEstadoDTO instance with property: "
		+ propertyName + ", value: " + value, Level.INFO, null);
	try {
	    final String queryString = "select model from EstadisticasEstadoDTO model where model."
		    + propertyName + "= :propertyValue";
	    Query query = entityManager.createQuery(queryString);
	    query.setParameter("propertyValue", value);
	    return query.getResultList();
	} catch (RuntimeException re) {
	    LogUtil.log("find by property name failed", Level.SEVERE, re);
	    throw re;
	}
    }

    /**
     * Find all EstadisticasEstadoDTO entities.
     * 
     * @return List<EstadisticasEstadoDTO> all EstadisticasEstadoDTO entities
     */
    @SuppressWarnings("unchecked")
    public List<EstadisticasEstadoDTO> findAll() {
	LogUtil.log("finding all EstadisticasEstadoDTO instances", Level.INFO,
		null);
	try {
	    final String queryString = "select model from EstadisticasEstadoDTO model";
	    Query query = entityManager.createQuery(queryString);
	    return query.getResultList();
	} catch (RuntimeException re) {
	    LogUtil.log("find all failed", Level.SEVERE, re);
	    throw re;
	}
    }
    
    
    @SuppressWarnings("unchecked")
    public  EstadisticasEstadoDTO  buscarParaActualizarPorRetornoEstatus(BigDecimal idToSolicitud, BigDecimal idToCotizacion){
	LogUtil.log("finding all EstadisticasEstadoDTO instances", Level.INFO,
		null);
	try {
	    EstadisticasEstadoDTO estadisticasEstadoDTO=null;
	    final String queryString = "select model from EstadisticasEstadoDTO model where " +
	    		"model.id.idCodigoEstadistica= :idCodigoEstadistica and " +
	    		"model.id.idSubCodigoEstadistica=:idSubCodigoEstadistica " +
	    		"and model.idBase1=:idBase1 and model.idBase2=:idBase2 ";
	    
	    Query query = entityManager.createQuery(queryString);
	    query.setParameter("idCodigoEstadistica",new  BigDecimal("10"));
	    query.setParameter("idSubCodigoEstadistica", new  BigDecimal("40"));
	    query.setParameter("idBase1", idToSolicitud);
	    query.setParameter("idBase2", idToCotizacion);
	    query.setHint(QueryHints.REFRESH, HintValues.TRUE);
	    List<EstadisticasEstadoDTO> listaEstadisticas=query.getResultList();
	    if(listaEstadisticas!=null && !listaEstadisticas.isEmpty()){
		estadisticasEstadoDTO= listaEstadisticas.get(0);
	    }
	    return estadisticasEstadoDTO;
	   
	} catch (RuntimeException re) {
	    LogUtil.log("find all failed", Level.SEVERE, re);
	    throw re;
	}
    }

	public void registraEstadistica(SolicitudDTO solicitud, SolicitudDTO.ACCION accion){
	    EstadisticasEstadoDTO estadisticasEstadoDTO = new EstadisticasEstadoDTO();
	    EstadisticasEstadoId estadisticasEstadoId = new EstadisticasEstadoId();
		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("10"));
		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
		estadisticasEstadoDTO.setId(estadisticasEstadoId);
		estadisticasEstadoDTO.setIdBase1(solicitud.getIdToSolicitud());
		estadisticasEstadoDTO.setIdBase2(BigDecimal.ZERO);
		estadisticasEstadoDTO.setIdBase3(BigDecimal.ZERO);
		estadisticasEstadoDTO.setIdBase4(BigDecimal.ZERO);
		estadisticasEstadoDTO.setValorVariable1(usuarioService.getUsuarioActual().getNombreUsuario());
		
		switch (accion) {
		case CREACION:
			estadisticasEstadoDTO.setValorVariable1(solicitud.getCodigoUsuarioCreacion());
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("10"));
			break;
		case ASIGNACION_COORDINADOR:
			estadisticasEstadoDTO.setValorVariable1(solicitud.getCodigoUsuarioAsignacion());
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("11"));
			break;
		case ASIGNACION_SUSCRIPTOR:
			estadisticasEstadoDTO.setValorVariable1(solicitud.getCodigoUsuarioAsignacion());
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("12"));
			break;		
		case CANCELACION:
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("13"));
			break;	
		case RECHAZO:
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("14"));
			break;	
		case TERMINAR:
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("15"));
			break;				
		case MODIFICACION:
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("16"));
			break;
		case REASIGNACION_SUSCRIPTOR:
			estadisticasEstadoDTO.setValorVariable1(solicitud.getCodigoUsuarioAsignacion());
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("17"));
		default:
			break;
		}
		this.save(estadisticasEstadoDTO);
	}    

	public void registraEstadistica(CotizacionDTO cotizacion, CotizacionDTO.ACCION accion){
	    EstadisticasEstadoDTO estadisticasEstadoDTO = new EstadisticasEstadoDTO();
	    EstadisticasEstadoId estadisticasEstadoId = new EstadisticasEstadoId();
		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("20"));
		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
		estadisticasEstadoDTO.setId(estadisticasEstadoId);
		estadisticasEstadoDTO.setIdBase1(cotizacion.getSolicitudDTO().getIdToSolicitud());
		estadisticasEstadoDTO.setIdBase2(cotizacion.getIdToCotizacion());
		estadisticasEstadoDTO.setIdBase3(BigDecimal.ZERO);
		estadisticasEstadoDTO.setIdBase4(BigDecimal.ZERO);
		estadisticasEstadoDTO.setValorVariable1(usuarioService.getUsuarioActual() != null ? usuarioService.getUsuarioActual().getNombreUsuario() : 
												cotizacion.getCodigoUsuarioCreacion());
		
		switch (accion) {
		case CREACION:
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("21"));
			break;
		case MODIFICACION:
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("22"));
			break;
		case LIBERACION:
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("23"));
			break;		
		case ASIGNACION_EMISION:
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("24"));
			break;	
		case RECHAZO:
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("25"));
			break;	
		case EMISION:
			estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("26"));
			break;				
		default:
			break;
		}
		this.save(estadisticasEstadoDTO);
	}
	@EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}	
}