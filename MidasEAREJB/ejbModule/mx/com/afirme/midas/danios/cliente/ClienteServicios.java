package mx.com.afirme.midas.danios.cliente;

import java.math.BigDecimal;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless
public class ClienteServicios implements ClienteServiciosRemote{

	@EJB
	private ClienteFacadeRemote clienteFacade;
	
	public ClienteDTO findById(BigDecimal idCliente, String nombreUsuario) throws Exception {
		try{
			return clienteFacade.findById(idCliente, nombreUsuario);
		}catch(Exception e){
			LogDeMidasEJB3.log("Error al consultar cliente. idCliente: "+idCliente+", nombreUsuario: "+nombreUsuario, Level.WARNING, e);
			throw e;
		}
	}

	public BigDecimal save(ClienteDTO entity, String nombreUsuario)
			throws Exception {
		try{
			return clienteFacade.save(entity, nombreUsuario);
		}catch(Exception e){
			LogDeMidasEJB3.log("Error al guardar cliente. idCliente: "+entity.getNombre() + " " 
					+ entity.getApellidoPaterno() + " " + entity.getApellidoMaterno()
					+ ", nombreUsuario: "+nombreUsuario, Level.WARNING, e);
			throw e;
		}
	}

}
