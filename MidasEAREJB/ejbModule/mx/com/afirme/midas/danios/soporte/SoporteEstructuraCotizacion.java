package mx.com.afirme.midas.danios.soporte;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;

public class SoporteEstructuraCotizacion extends ReporteCotizacionBase{
	private List<AgrupacionCotDTO> listaAgrupacionCotizacion;
	private List<DetallePrimaCoberturaCotizacionDTO> listaDetallePrimaCoberturaCot;
	private Map<BigDecimal,List<CoberturaCotizacionDTO>> mapaCoberturasContratadasPorSubRamo;
	private Map<BigDecimal[],List<CoberturaCotizacionDTO>> mapaCoberturasContratadasPorIncisoSubRamo;
	private Map<BigDecimal,List<IncisoCotizacionDTO>> mapaIncisosPorSubRamo;
	private Map<BigDecimal,List<CoberturaCotizacionDTO>> mapaCoberturasContratadasPorInciso;
	private Map<BigDecimal,List<CoberturaCotizacionDTO>> mapaCoberturasContratadasPorIdToCobertura;
	private Map<BigDecimal[],List<CoberturaCotizacionDTO>> mapaCoberturasContratadasPorIncisoIdToCobertura;
	private Map<Short,List<CoberturaCotizacionDTO>> mapaCoberturasContratadasLUCPorNumeroAgrupacion;
	protected Map<Integer,List<SeccionCotizacionDTO>> mapaTotalSeccionesPorNumeroInciso;
	
	protected List<SeccionCotizacionDTO> listaSeccionesNoContratadasCotizacion;
	protected List<CoberturaCotizacionDTO> listaCoberturasNoContratadasCotizacion;
	protected Map<Integer,List<SeccionCotizacionDTO>> mapaSeccionesNoContratadasPorNumeroInciso;
	protected Map<Integer[],List<CoberturaCotizacionDTO>> mapaCoberturasNoContratadasPorSeccion;
	private Map<BigDecimal[],List<CoberturaCotizacionDTO>> mapaCoberturasNoContratadasPorIncisoIdToCobertura;
	private Map<BigDecimal[],CoberturaCotizacionDTO> mapaCoberturasContratadasPorIdCompleto;
	protected Map<BigDecimal[],SubIncisoCotizacionDTO> mapaSubIncisosPorIdCompleto;
	
	protected AdministradorDatoIncisoCotizacion administradorDatosInciso;
	
	protected DetallePrimaCoberturaCotizacionFacadeRemote detallePrimaCoberturaCotizacionFacade;

	{
		detallePrimaCoberturaCotizacionFacade = ServiceLocatorP.getInstance().getEJB(DetallePrimaCoberturaCotizacionFacadeRemote.class);
	}
	
	public SoporteEstructuraCotizacion(BigDecimal idToCotizacion) throws javax.transaction.SystemException{
		super();
		this.idToCotizacion = idToCotizacion;
		administradorDatosInciso = new AdministradorDatoIncisoCotizacion(idToCotizacion);
		cotizacionDTO = cotizacionFacade.findById(idToCotizacion);
	}
	
	public SoporteEstructuraCotizacion(CotizacionDTO cotizacionDTO) throws javax.transaction.SystemException{
		super();
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		administradorDatosInciso = new AdministradorDatoIncisoCotizacion(idToCotizacion);
		this.cotizacionDTO = cotizacionDTO;
	}
	
	public List<SeccionCotizacionDTO> consultarSeccionesNoContratadas() throws SystemException{
		if(listaSeccionesNoContratadasCotizacion == null){
			listaSeccionesNoContratadasCotizacion = seccionCotizacionFacade.listarSeccionesContratadasPorCotizacion(idToCotizacion, ConstantesCotizacion.NO_CONTRATADO);
		}
		return listaSeccionesNoContratadasCotizacion;
	}
	
	public List<CoberturaCotizacionDTO> consultarCoberturasNoContratadas() throws SystemException{
		if(listaCoberturasNoContratadasCotizacion == null){
			CoberturaCotizacionDTO coberturaFiltro = new CoberturaCotizacionDTO();
			coberturaFiltro.setId(new CoberturaCotizacionId());
			coberturaFiltro.getId().setIdToCotizacion(idToCotizacion);
			coberturaFiltro.setSeccionCotizacionDTO(new SeccionCotizacionDTO());
			//solo se coloca la seccion como no contratada, para que la consulta arroje las coberturas marcadas 
			//como contratadas y no contratadas pero que su secci�n no est� contratada
			coberturaFiltro.getSeccionCotizacionDTO().setClaveContrato(ConstantesCotizacion.NO_CONTRATADO);
			listaCoberturasNoContratadasCotizacion = coberturaCotizacionFacade.listarFiltrado(coberturaFiltro);
			
			coberturaFiltro.getSeccionCotizacionDTO().setClaveContrato(ConstantesCotizacion.CONTRATADO);
			coberturaFiltro.setClaveContrato(ConstantesCotizacion.NO_CONTRATADO);
			List<CoberturaCotizacionDTO> coberturasNoContratadasConSeccionContratada = coberturaCotizacionFacade.listarFiltrado(coberturaFiltro);
			if(coberturasNoContratadasConSeccionContratada != null){
				listaCoberturasNoContratadasCotizacion.addAll(coberturasNoContratadasConSeccionContratada);
			}
		}
		return listaCoberturasNoContratadasCotizacion;
	}

//	/**
//	 * Si la orden de trabajo es v�lida, cambia su estatus a "Orden de trabajo terminada". 
//	 * @param idToCotizacion
//	 * @param usuario
//	 * @return
//	 * @throws SystemException
//	 */
//	public Set<String> terminarOrdenTrabajo (BigDecimal idToCotizacion, Usuario usuario) throws SystemException {
//    	Set<String> errores = null;
//		errores = validarOrdenTrabajo(idToCotizacion);
//		CotizacionDTO cotizacionDTO =CotizacionDN.getInstancia(usuario.getNombreUsuario()).getPorId(idToCotizacion);
//		cotizacionDTO = CotizacionDN.getInstancia(usuario.getNombreUsuario()).obtenerDatosForaneos(cotizacionDTO);
//		if (OrdenTrabajoDN.ordenTrabajoValida(errores)) {
//			//Cambiar estatus a requerida para retroactividad, diferimiento,
//			//vigencia max o minima.
//			if (errores.size() > 0) {
//			    	if (errores.contains("ordentrabajo.retroactividad") || 
//						errores.contains("ordentrabajo.diferimiento")){
//			    	    if(cotizacionDTO.getClaveAutRetroacDifer()== null ||
//						!Sistema.CLAVES_AUTORIZADA_RECHAZADA.contains
//						(cotizacionDTO.getClaveAutVigenciaMaxMin().toString())){
//			    		cotizacionDTO.setClaveAutRetroacDifer(Sistema.AUTORIZACION_REQUERIDA);
//			    		 cotizacionDTO.setFechaSolAutRetroacDifer(Calendar.getInstance().getTime());
//			    	    }
//			    	}else{
//			    	    cotizacionDTO.setClaveAutRetroacDifer(Sistema.AUTORIZACION_NO_REQUERIDA);  
//			    	    cotizacionDTO.setFechaSolAutRetroacDifer(null);
//			    	}
//				
//				if (errores.contains("ordentrabajo.vigencia.maxima") || 
//						errores.contains("ordentrabajo.vigencia.minima")) {
//				    if(cotizacionDTO.getClaveAutVigenciaMaxMin()== null||
//						!Sistema.CLAVES_AUTORIZADA_RECHAZADA.contains
//							(cotizacionDTO.getClaveAutVigenciaMaxMin().toString())){
//					cotizacionDTO.setClaveAutVigenciaMaxMin(Sistema.AUTORIZACION_REQUERIDA);
//					 cotizacionDTO.setFechaSolAutVigenciaMaxMin(Calendar.getInstance().getTime());
//				    }
//				}else{
//				    cotizacionDTO.setClaveAutVigenciaMaxMin(Sistema.AUTORIZACION_NO_REQUERIDA);
//				    cotizacionDTO.setFechaSolAutVigenciaMaxMin(null);
//				}
//			}
//			cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_ODT_TERMINADA);
//			CotizacionDN.getInstancia(usuario.getNombreUsuario()).modificar(cotizacionDTO);
//						
//		}
//		
//		return errores;
//	}
//	
//	private Set<String> validarOrdenTrabajo(BigDecimal idToCotizacion) throws SystemException {
//		consultarIncisosCotizacion();
//		consultarSeccionesContratadasCotizacion();
//		List<IncisoCotizacionDTO> incisos = getListaIncisos();
//		
//		//Todas las validaciones segun las reglas de negocio.
//		Set<String> errores = new HashSet<String>();
//		
//		Date fechaSolicitud = cotizacionDTO.getSolicitudDTO().getFechaCreacion();
//		Date fechaInicioVigencia = cotizacionDTO.getFechaInicioVigencia();
//		Date fechaFinVigencia = cotizacionDTO.getFechaFinVigencia();
//		if (fechaSolicitud == null) {
//			errores.add("ordentrabajo.fechaSolicitud.vacia");
//		}
//		if (fechaInicioVigencia == null) {
//			errores.add("ordentrabajo.fechaInicioVigencia.vacia");
//		}
//		if (fechaFinVigencia == null) {
//			errores.add("ordentrabajo.fechaFinVigencia.vacia");
//		}
//		if (fechaInicioVigencia != null && fechaSolicitud != null) {
//			if (!OrdenTrabajoDN.validarRetroactividad(fechaInicioVigencia, fechaSolicitud) && (cotizacionDTO.getClaveAutRetroacDifer()==null ||
//			         (cotizacionDTO.getClaveAutRetroacDifer()!= null && cotizacionDTO.getClaveAutRetroacDifer() != Sistema.AUTORIZADA))) {
//				errores.add("ordentrabajo.retroactividad");
//			}
//			if (!OrdenTrabajoDN.validarDiferimiento(fechaInicioVigencia, fechaSolicitud) && (cotizacionDTO.getClaveAutRetroacDifer()==null ||
//						(cotizacionDTO.getClaveAutRetroacDifer() != null && cotizacionDTO.getClaveAutRetroacDifer() != Sistema.AUTORIZADA))) {
//				errores.add("ordentrabajo.diferimiento");			
//			}
//		}
//		if (fechaInicioVigencia != null && fechaFinVigencia != null) {
//			if (!OrdenTrabajoDN.validarVigenciaMaxima(fechaInicioVigencia, fechaFinVigencia) && (cotizacionDTO.getClaveAutVigenciaMaxMin() ==null||
//					 (cotizacionDTO.getClaveAutVigenciaMaxMin() !=null && cotizacionDTO.getClaveAutVigenciaMaxMin() != Sistema.AUTORIZADA))) {
//				errores.add("ordentrabajo.vigencia.maxima");
//			}
//			if (!OrdenTrabajoDN.validarVigenciaMinima(fechaInicioVigencia, fechaFinVigencia)&& (cotizacionDTO.getClaveAutVigenciaMaxMin()==null ||
//						(cotizacionDTO.getClaveAutVigenciaMaxMin() != null && cotizacionDTO.getClaveAutVigenciaMaxMin() != Sistema.AUTORIZADA))) {
//				errores.add("ordentrabajo.vigencia.minima");
//			}
//		}
//		if (incisos == null || incisos.size() == 0) { //Validar si tiene incisos
//			errores.add("ordentrabajo.incisos.vacio");
//		}
//		else {
//			if(listaSeccionesContratadasCotizacion == null || listaSeccionesContratadasCotizacion.isEmpty())
//				consultarSeccionesContratadasCotizacion();
//			if (!validarIncisosTieneSeccionContratada()) {
//				errores.add("ordentrabajo.seccion.sinContratar");
//			}
//			else {
//				if(listaCoberturasContratadasCotizacion == null || listaCoberturasContratadasCotizacion.isEmpty())
//					consultarCoberturasContratadasCotizacion();
//				if (!validarSeccionTieneCoberturaContratada()) {
//					errores.add("ordentrabajo.cobertura.sinContratar");
//				}
//			}
//		}
//		if (cotizacionDTO.getIdToPersonaContratante() == null) {
//			errores.add("ordentrabajo.personaContratante.noExiste");
//		}
//		if (cotizacionDTO.getIdToPersonaAsegurado() == null) {
//			errores.add("ordentrabajo.personaAsegurado.noExiste");
//		}
//
//		return errores;
//	}
//	
//	private boolean validarSeccionTieneCoberturaContratada() throws SystemException {
//		boolean esValido = true;
//		for (IncisoCotizacionDTO inciso : listaIncisos) {
//			List<SeccionCotizacionDTO> secciones = obtenerSeccionesContratadas(inciso.getId().getNumeroInciso());
//			for (SeccionCotizacionDTO seccion : secciones) {
//				List<CoberturaCotizacionDTO> coberturas = obtenerCoberturasContratadas(seccion.getId().getIdToSeccion(), seccion.getId().getNumeroInciso());
//				boolean coberturaContratadaSeccion = !coberturas.isEmpty();
//				if (!coberturaContratadaSeccion) {
//					esValido = false;
//					break;
//				}
//			}
//			if(!esValido)
//				break;
//		}
//		return esValido;
//	}
//	
//	private boolean validarIncisosTieneSeccionContratada() throws SystemException {
//		boolean esValido = false;
//		
//		for (IncisoCotizacionDTO inciso : listaIncisos) {
//			List<SeccionCotizacionDTO> secciones = obtenerSeccionesContratadas(inciso.getId().getNumeroInciso());
//			for (SeccionCotizacionDTO seccion: secciones) {
//				if (seccion.getClaveContrato().compareTo(Sistema.CONTRATADO) == 0) {
//					esValido = true;
//					break;
//				}
//			}
//		}
//		return esValido;
//	}
//	
//	/**
//	 * Valida que todas las coberturas contratadas b�sicas de la cotizaci�n tengan una suma asegurada v�lida.
//	 * @param cotizacionForm
//	 * @param esCotizacion
//	 * @return
//	 * @throws SystemException 
//	 */
//	public boolean validadaSumasAseguradas(CotizacionForm cotizacionForm, boolean esCotizacion) throws SystemException {
//		consultarCoberturasContratadasCotizacion();
//		boolean resultado = true;
//		String encabezadoError="La Orden de Trabajo NO fue terminada debido a que ";
//		if(esCotizacion){
//			encabezadoError="La cotizacion NO fue liberada debido a que ";
//		}
//		String icono = Sistema.ERROR;
//		StringBuilder mensaje = new StringBuilder();
//		StringBuilder mensajeError= new StringBuilder();
//		List<String>mensajesCoberturas=new ArrayList<String>();
//		//Se ejecuta el validador de Sumas Aseguradas contratadas
//		validadorSumasAseguradas(mensajeError,mensajesCoberturas);
//
//		if(mensajeError!=null && mensajeError.toString().length()>0){
//			//Excepciones relacionadas si en la Orden no se contrato ninguna 
//			//seccion, del inciso o incisos que se presentaron del producto, asi como tambien
//			//que no presente ninguna cobertura contratada de la seccion o secciones contratadas, del inciso o incisos
//			icono = Sistema.INFORMACION;
//			mensaje.append(encabezadoError);
//			mensaje.append(mensajeError);
//			cotizacionForm.setTipoMensaje(icono);
//			cotizacionForm.setMensaje(mensaje.toString());
//			resultado = false;
//		}else{
//			if(!mensajesCoberturas.isEmpty()){
//				//Existieron sumas aseguradas en 0 de las coberturas basicas contratadas; despliega mensaje
//				icono = Sistema.INFORMACION;
//				mensaje.append(encabezadoError);
//				mensaje.append("las siguientes coberturas tienen </br> sumas aseguradas en CEROS:<br> ");
//
//				for(String mensajeCobertura:mensajesCoberturas){
//					mensaje.append(mensajeCobertura);
//				}
//				cotizacionForm.setTipoMensaje(icono);
//				cotizacionForm.setMensaje(mensaje.toString());
//				resultado =false;
//			}
//		}
//		return resultado;
//	}
	
//	/**
//	 * validadorSumasAseguradas 
//	 * @param cotizacionDTO
//	 * @param mensajeError
//	 * @param coberturasEnCero
//	 */
//	private void validadorSumasAseguradas(StringBuilder mensajeError, List<String> coberturasEnCero){
//		String coberturaEnCero;
//		
//		List<CoberturaCotizacionDTO> listaCoberturasContratadas = getListaCoberturasContratadasCotizacion();
//		if(listaCoberturasContratadas != null && !listaCoberturasContratadas.isEmpty()){
//			for(CoberturaCotizacionDTO cobertura : listaCoberturasContratadas){
//				if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_BASICA) &&
//						cobertura.getValorSumaAsegurada().equals(new Double(0d))) {
//					StringBuilder mensaje = new StringBuilder();
//					mensaje.append("*No. Inciso: "	+ cobertura.getId().getNumeroInciso());
//					mensaje.append(" Seccion: "	+ cobertura.getSeccionCotizacionDTO().getSeccionDTO().getDescripcion());
//					mensaje.append(" Cobertura: "+ cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion());
//					mensaje.append("</br>");
//					
//					coberturaEnCero = mensaje.toString();
//					coberturasEnCero.add(coberturaEnCero);
//				}
//			}
//		}else{
//			mensajeError.append("NO tiene coberturas contratadas en ningun Inciso");
//		}
//	}
	
	
	
	/**
	 * Consulta toda la informacion de la cotizaci�n: incisos, subincisos, coberturas contratadas, 
	 * detalle de prima de coberturas y agrupaciones de primer riesgo o LUC
	 * @param nombreUsuario
	 * @throws SystemException
	 */
	public void consultarInformacionValidacionCotizacion(String nombreUsuario) throws SystemException{
		consultarAgrupacionCotizacion();
		consultarCoberturasContratadasCotizacion();
		consultarIncisosCotizacion();
		consultarSubIncisosCotizacion();
		consultarDetallePrimaCoberturaCotizacion();
	}
	
	/**
	 * Consulta la lista de sub incisos de la cotizaci�n.
	 * @throws SystemException
	 */
	public void consultarSubIncisosCotizacion() throws SystemException{
		listaSubIncisosCotizacion = subIncisoCotFacade.listarSubIncisosPorCotizacion(idToCotizacion);
	}
	
	/**
	 * Consulta la lista de secciones contratadas (claveContrato = 1) de la cotizaci�n.
	 * @throws SystemException
	 */
	public void consultarSeccionesContratadasCotizacion() throws SystemException{
		listaSeccionesContratadasCotizacion = seccionCotizacionFacade.listarSeccionesContratadasPorCotizacion(idToCotizacion,ConstantesCotizacion.CONTRATADO);
	}
	
	/**
	 * Consulta la lista de DetallePrimaCobertura de la cotizaci�n.
	 * @throws SystemException
	 */
	public void consultarDetallePrimaCoberturaCotizacion() throws SystemException{
		listaDetallePrimaCoberturaCot = detallePrimaCoberturaCotizacionFacade.listarPorCotizacion(idToCotizacion);
	}
	
	/**
	 * Consulta la lista de riesgos contratados (claveContrato = 1) de la cotizaci�n.
	 * @throws SystemException
	 */
	public void consultarRiesgosContratadosCotizacion() throws SystemException{
		listaRiesgosContratadosCotizacion = riesgoCotizacionFacade.listarRiesgosContratadosPorCotizacion(idToCotizacion);
	}
	
	/**
	 * Consulta la lista de registros AgrupacionCotDTO de la cotizaci�n.
	 * @throws SystemException
	 */
	public void consultarAgrupacionCotizacion() throws SystemException{
		listaAgrupacionCotizacion = agrupacionCotizacionFacade.listarPorCotizacion(idToCotizacion);
	}
	
	
	public List<SeccionCotizacionDTO> obtenerSeccionesNoContratadas(BigDecimal numeroInciso){
		if (this.listaSeccionesNoContratadasCotizacion != null){
			if (mapaSeccionesNoContratadasPorNumeroInciso == null){
				mapaSeccionesNoContratadasPorNumeroInciso = new HashMap<Integer,List<SeccionCotizacionDTO>>();
			}
			Integer keyNumeroInciso = Integer.valueOf(numeroInciso.toString());
			List<SeccionCotizacionDTO> listaSeccionesNoContratadasPorInciso = mapaSeccionesNoContratadasPorNumeroInciso.get(keyNumeroInciso);
			if (listaSeccionesNoContratadasPorInciso == null){
				listaSeccionesNoContratadasPorInciso = new ArrayList<SeccionCotizacionDTO>();
				for(SeccionCotizacionDTO seccionCot : listaSeccionesNoContratadasCotizacion){
					if (seccionCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0)
						listaSeccionesNoContratadasPorInciso.add(seccionCot);
				}
				mapaSeccionesNoContratadasPorNumeroInciso.put(keyNumeroInciso, listaSeccionesNoContratadasPorInciso);
			}
			return listaSeccionesNoContratadasPorInciso;
		}
		else
			return null;
	}
	
	/**
	 * Regresa la lista de secciones contratadas y no contratadas de la cotizacion.
	 * @throws SystemException
	 */
	public List<SeccionCotizacionDTO> obtenerListaCompletaSeccionesPorInciso(BigDecimal numeroInciso) throws SystemException{
		if(listaSeccionesContratadasCotizacion == null){
			consultarSeccionesContratadasCotizacion();
		}
		if(listaSeccionesNoContratadasCotizacion == null){
			consultarSeccionesNoContratadas();
		}
		List<SeccionCotizacionDTO> listaSeccionesContratadasPorInciso = obtenerSeccionesContratadas(numeroInciso);
		List<SeccionCotizacionDTO> listaSeccionesNoContratadasPorInciso = obtenerSeccionesNoContratadas(numeroInciso);
		List<SeccionCotizacionDTO> listaTotalSeccionesPorInciso = new ArrayList<SeccionCotizacionDTO>();
		if(listaSeccionesContratadasPorInciso != null)
			listaTotalSeccionesPorInciso.addAll(listaSeccionesContratadasPorInciso);
		if(listaSeccionesNoContratadasPorInciso != null)
			listaTotalSeccionesPorInciso.addAll(listaSeccionesNoContratadasPorInciso);
		
		return listaTotalSeccionesPorInciso;
	}
	
	/**
	 * Regresa la lista de coberturas no contratadas por seccion e inciso.
	 * @param numeroInciso
	 * @return List<CoberturaCotizacionDTO>
	 * @throws SystemException 
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasNoContratadas(BigDecimal idToSeccion,BigDecimal numeroInciso) throws SystemException{
		consultarCoberturasNoContratadas();
		if (mapaCoberturasNoContratadasPorSeccion == null){
			mapaCoberturasNoContratadasPorSeccion = new HashMap<Integer[],List<CoberturaCotizacionDTO>>();
		}
		Integer keySeccion = Integer.valueOf(idToSeccion.toString());
		Integer keyNumeroInciso = Integer.valueOf(numeroInciso.toString());
		Integer[] keyCoberturas = new Integer[2];
		keyCoberturas[0] = keyNumeroInciso;
		keyCoberturas[1] = keySeccion;
		List<CoberturaCotizacionDTO> listaCoberturasNoContratadasPorSeccion = mapaCoberturasNoContratadasPorSeccion.get(keyCoberturas);
		if (listaCoberturasNoContratadasPorSeccion == null){
			listaCoberturasNoContratadasPorSeccion = new ArrayList<CoberturaCotizacionDTO>();
			for(CoberturaCotizacionDTO coberturaCot : listaCoberturasNoContratadasCotizacion){
				if (coberturaCot.getId().getIdToSeccion().compareTo(idToSeccion) == 0 && coberturaCot.getId().getNumeroInciso().compareTo(numeroInciso) ==0)
					listaCoberturasNoContratadasPorSeccion.add(coberturaCot);
			}
			mapaCoberturasNoContratadasPorSeccion.put(keyCoberturas, listaCoberturasNoContratadasPorSeccion);
		}
		return listaCoberturasNoContratadasPorSeccion;
	}
	
	/**
	 * Regresa la lista de coberturas contratadas y no contratadas de la cotizacion.
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> obtenerListaCompletaCoberturas(BigDecimal numeroInciso,BigDecimal idToSeccion) throws SystemException{
		List<CoberturaCotizacionDTO> listaCoberturasNoContratadasPorSeccion = obtenerCoberturasNoContratadas(idToSeccion, numeroInciso);
		List<CoberturaCotizacionDTO> listaCoberturasContratadasPorSeccion = obtenerCoberturasContratadas(idToSeccion, numeroInciso);
		List<CoberturaCotizacionDTO> listaCoberturasTodas = new ArrayList<CoberturaCotizacionDTO>();
		if(listaCoberturasContratadasPorSeccion != null)
			listaCoberturasTodas.addAll(listaCoberturasContratadasPorSeccion);
		if(listaCoberturasNoContratadasPorSeccion != null)
			listaCoberturasTodas.addAll(listaCoberturasNoContratadasPorSeccion);
		
		return listaCoberturasTodas;
	}
	
	/**
	 * Regresa la seccion que coincida con los par�metros recibidos. La b�squeda se realiza en las listas de secciones contratadas
	 * y no contratadas.
	 * @throws SystemException
	 */
	public SeccionCotizacionDTO obtenerSeccionPorIncisoOmiteContrato(BigDecimal idToSeccion,BigDecimal numeroInciso) throws SystemException{
		if(listaSeccionesContratadasCotizacion == null){
			consultarSeccionesContratadasCotizacion();
		}
		if(listaSeccionesNoContratadasCotizacion == null){
			consultarSeccionesNoContratadas();
		}
		SeccionCotizacionDTO seccionEncontrada = null;
		for(SeccionCotizacionDTO seccionCot : listaSeccionesContratadasCotizacion){
			if (seccionCot.getId().getIdToSeccion().compareTo(idToSeccion) == 0 && 
					seccionCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0){
				seccionEncontrada = seccionCot;
				break;
			}
		}
		if(seccionEncontrada == null){
			for(SeccionCotizacionDTO seccionCot : listaSeccionesNoContratadasCotizacion){
				if (seccionCot.getId().getIdToSeccion().compareTo(idToSeccion) == 0 && 
						seccionCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0){
					seccionEncontrada = seccionCot;
					break;
				}
			}
		}
		return seccionEncontrada;
	}
	
	/**
	 * Busca el subinciso que tenga los datos recibidos en la lista de subincisos de la cotizacion.
	 * @param idToSeccion
	 * @param numeroInciso
	 * @param numeroSubInciso
	 * @return
	 * @throws SystemException 
	 */
	public SubIncisoCotizacionDTO obtenerSubInciso(BigDecimal idToSeccion,BigDecimal numeroInciso,BigDecimal numeroSubInciso) throws SystemException{
		if(listaSubIncisosCotizacion == null){
			consultarSubIncisosCotizacion();
		}
		if(mapaSubIncisosPorIdCompleto == null)
			mapaSubIncisosPorIdCompleto= new HashMap<BigDecimal[], SubIncisoCotizacionDTO>();
		
		BigDecimal[] idSubInciso = new BigDecimal[3];
		idSubInciso[0]=idToSeccion;
		idSubInciso[1]=numeroInciso;
		idSubInciso[2]=numeroSubInciso;
		
		SubIncisoCotizacionDTO subIncisoEncontrado = mapaSubIncisosPorIdCompleto.get(idSubInciso);
		
		if(subIncisoEncontrado == null){
			for(SubIncisoCotizacionDTO subIncisoCot : listaSubIncisosCotizacion){
				if (subIncisoCot.getId().getNumeroInciso().compareTo(numeroInciso)==0 
						&& subIncisoCot.getId().getIdToSeccion().compareTo(idToSeccion) == 0 &&
						subIncisoCot.getId().getNumeroSubInciso().compareTo(numeroSubInciso) == 0){
					subIncisoEncontrado = subIncisoCot;
					break;
				}
			}
			if(subIncisoEncontrado != null)
				mapaSubIncisosPorIdCompleto.put(idSubInciso, subIncisoEncontrado);
		}
		return subIncisoEncontrado;
	}
	
	public DetallePrimaCoberturaCotizacionDTO obtenerDetallePrimaCoberturaCotizacionPorId(DetallePrimaCoberturaCotizacionId id){
		DetallePrimaCoberturaCotizacionDTO detallePrima = null;
		if (listaDetallePrimaCoberturaCot != null){
			for(DetallePrimaCoberturaCotizacionDTO detalle : listaDetallePrimaCoberturaCot){
				if (detalle.getId().getNumeroInciso().compareTo(id.getNumeroInciso()) == 0)
					if (detalle.getId().getNumeroSubInciso().compareTo(id.getNumeroSubInciso()) == 0)
						if (detalle.getId().getIdToCotizacion().compareTo(id.getIdToCotizacion()) == 0)
							if (detalle.getId().getIdToSeccion().compareTo(id.getIdToSeccion()) == 0)
								if (detalle.getId().getIdToCobertura().compareTo(id.getIdToCobertura()) == 0){
									detallePrima = detalle;
									break;
								}
			}
		}
		return detallePrima;
	}

	/**
	 * Busca el registro de AgrupacionCotizacion que tenga la claveTipoAgrupacion recibida.
	 * @param Short claveTipoAgrupacion
	 * @return AgrupacionCotDTO que tenga la claveTipoAgrupacion recibida. Si no se encuentra, regresa null
	 */
	public AgrupacionCotDTO obtenerAgrupacionCotizacionPorTipoAgrupacion(Short claveTipoAgrupacion) {
		AgrupacionCotDTO agrupacion = null;
		for(AgrupacionCotDTO agrupacionTMP : listaAgrupacionCotizacion){
			if (agrupacionTMP.getClaveTipoAgrupacion().intValue() == claveTipoAgrupacion.intValue()){
				agrupacion = agrupacionTMP;
				break;
			}
		}
		return agrupacion;
	}
	
	/**
	 * Busca el registro de AgrupacionCotizacion que tenga la n�mero de Agrupacion recibido, almacenado en 
	 * el objeto aniudado AgrupacionCotId.
	 * @param Short numeroAgrupacion
	 * @return AgrupacionCotDTO que tenga la claveTipoAgrupacion recibida. Si no se encuentra, regresa null
	 */
	public AgrupacionCotDTO obtenerAgrupacionCotizacionPorNumeroAgrupacion(Short numeroAgrupacion) {
		
		AgrupacionCotDTO agrupacion = null;
		for(AgrupacionCotDTO agrupacionTMP : listaAgrupacionCotizacion){
			if (agrupacionTMP.getId().getNumeroAgrupacion().compareTo(numeroAgrupacion) == 0){
				agrupacion = agrupacionTMP;
				break;
			}
		}
		return agrupacion;
	}
	
	/**
	 * Indica si la CoberturaCotizacion recibida aplic� a primer riesgo. Para invocar este m�todo, se debe invocar antes a consultarAgrupacionCotizacion().
	 * @param coberturaCotizacionDTO
	 * @return
	 */
	public boolean aplicoPrimerRiesgo(CoberturaCotizacionDTO coberturaCotizacionDTO) {
		boolean aplicoPR = false;
		for(AgrupacionCotDTO agrupacion : listaAgrupacionCotizacion){
			if (agrupacion.getId().getNumeroAgrupacion().intValue() == coberturaCotizacionDTO.getNumeroAgrupacion().intValue() &&
				agrupacion.getId().getIdToCotizacion().compareTo(coberturaCotizacionDTO.getId().getIdToCotizacion()) == 0 && 
				agrupacion.getClaveTipoAgrupacion().shortValue() == ConstantesCotizacion.TIPO_PRIMER_RIESGO){
				aplicoPR = true;
				break;
			}
		}
		return aplicoPR;
	}
	
	/**
	 * Indica si la CoberturaCotizacion recibida aplic� a LUC. Para invocar este m�todo, se debe invocar antes a consultarAgrupacionCotizacion().
	 * @param coberturaCotizacionDTO
	 * @return
	 */
	public boolean aplicoLUC(CoberturaCotizacionDTO coberturaCotizacionDTO) {
		boolean aplicoLUC = false;
		for(AgrupacionCotDTO agrupacion : listaAgrupacionCotizacion){
			if (agrupacion.getId().getNumeroAgrupacion().intValue() == coberturaCotizacionDTO.getNumeroAgrupacion().intValue() &&
				agrupacion.getId().getIdToCotizacion().compareTo(coberturaCotizacionDTO.getId().getIdToCotizacion()) == 0 && 
				agrupacion.getClaveTipoAgrupacion().shortValue() != ConstantesCotizacion.TIPO_PRIMER_RIESGO){
				aplicoLUC = true;
				break;
			}
		}
		return aplicoLUC;
	}
	
	/**
	 * Regresa la lista de registros AgrupacionCotDTO relacionados con la cotizacion cuyo ID se recibi� al instanciar el objeto.
	 * @return
	 */
	public List<AgrupacionCotDTO> obtenerListaAgrupacionCotizacion(){
		return listaAgrupacionCotizacion;
	}
	
	/**
	 * Consulta la lista de incisos de la cotizaci�n.
	 * @return
	 * @throws SystemException
	 */
	public List<IncisoCotizacionDTO> listarIncisos() throws SystemException{
		if(listaIncisos == null)
			consultarIncisosCotizacion();
		return getListaIncisos();
	}
	
	/**
	 * Calcula la lista de coberturas contratadas pertenecientes a un subramo, sin importar el inciso, ni secci�n.
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasContratadasPorSubRamo(BigDecimal idTcSubRamo){
		if (this.listaCoberturasContratadasCotizacion != null){
			if (mapaCoberturasContratadasPorSubRamo == null){
				mapaCoberturasContratadasPorSubRamo = new HashMap<BigDecimal,List<CoberturaCotizacionDTO>>();
			}
			List<CoberturaCotizacionDTO> listaCoberturasContratadasPorSubRamo= mapaCoberturasContratadasPorSubRamo.get(idTcSubRamo);
			if (listaCoberturasContratadasPorSubRamo == null){
				listaCoberturasContratadasPorSubRamo = new ArrayList<CoberturaCotizacionDTO>();
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
					if (coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo().compareTo(idTcSubRamo) == 0)
						listaCoberturasContratadasPorSubRamo.add(coberturaCot);
				}
				mapaCoberturasContratadasPorSubRamo.put(idTcSubRamo, listaCoberturasContratadasPorSubRamo);
			}
			return listaCoberturasContratadasPorSubRamo;
		}
		else
			return null;
	}
	
	/**
	 * Calcula la lista de los incisos de la cotizaci�n que tengan al menos una cobertura perteneciente al subramo recibido.
	 * @return List<CoberturaCotizacionDTO>
	 * @throws SystemException 
	 */
	public List<IncisoCotizacionDTO> obtenerIncisosPorSubRamo(BigDecimal idTcSubRamo) throws SystemException{
		if(this.listaIncisos == null)
			consultarIncisosCotizacion();
		if (mapaIncisosPorSubRamo == null){
			mapaIncisosPorSubRamo = new HashMap<BigDecimal,List<IncisoCotizacionDTO>>();
		}
		List<IncisoCotizacionDTO> listaIncisosPorSubRamo= mapaIncisosPorSubRamo.get(idTcSubRamo);
		if (listaIncisosPorSubRamo == null){
			listaIncisosPorSubRamo = new ArrayList<IncisoCotizacionDTO>();
			for(IncisoCotizacionDTO incisoCot : listaIncisos){
				if (contieneCoberturaPorSubramo(obtenerCoberturasContratadasPorInciso(incisoCot.getId().getNumeroInciso()), idTcSubRamo))
					listaIncisosPorSubRamo.add(incisoCot);
			}
			mapaIncisosPorSubRamo.put(idTcSubRamo, listaIncisosPorSubRamo);
		}
		return listaIncisosPorSubRamo;
	}
	
	/**
	 * Calcula la lista de los incisos de la cotizaci�n que tengan al menos una cobertura perteneciente al subramo recibido.
	 * @return List<CoberturaCotizacionDTO>
	 */
	public IncisoCotizacionDTO obtenerInciso(BigDecimal numeroInciso){
		IncisoCotizacionDTO incisoEncontrado = null;
		if (this.listaIncisos != null){
			for(IncisoCotizacionDTO incisoCot : listaIncisos){
				if(incisoCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0){
					incisoEncontrado = incisoCot;
					break;
				}
			}
		}
		return incisoEncontrado;
	}
	
	/**
	 * Verifica que al menos una coberturaCotizacion de la lista recibida pertenezca al subRamo cuyo ID se recibe.
	 * @param listaCoberturas. lista de coberturas por verificar.
	 * @param idTcSubRamo. ID del SubRamo por verificar
	 * @return true, si se encuentra al menos una cobertura del subRamo contratado, false de lo contrario.
	 */
	private boolean contieneCoberturaPorSubramo(List<CoberturaCotizacionDTO> listaCoberturas,BigDecimal idTcSubRamo){
		boolean contieneSubRamo = false;
		for (CoberturaCotizacionDTO coberturaTMP : listaCoberturas){
			if (coberturaTMP.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo().compareTo(idTcSubRamo) == 0){
				contieneSubRamo = true;
				break;
			}
		}
		return contieneSubRamo;
	}
	
	/**
	 * Regresa la lista de coberturas contratadas por inciso.
	 * @param numeroInciso
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasContratadasPorInciso(BigDecimal numeroInciso){
		if (this.listaCoberturasContratadasCotizacion != null){
			if (mapaCoberturasContratadasPorInciso == null){
				mapaCoberturasContratadasPorInciso = new HashMap<BigDecimal,List<CoberturaCotizacionDTO>>();
			}
			List<CoberturaCotizacionDTO> listaCoberturasContratadasPorInciso= mapaCoberturasContratadasPorInciso.get(numeroInciso);
			if (listaCoberturasContratadasPorInciso == null){
				listaCoberturasContratadasPorInciso = new ArrayList<CoberturaCotizacionDTO>();
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
					if (coberturaCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0)
						listaCoberturasContratadasPorInciso.add(coberturaCot);
				}
				mapaCoberturasContratadasPorInciso.put(numeroInciso, listaCoberturasContratadasPorInciso);
			}
			return listaCoberturasContratadasPorInciso;
		}
		else
			return null;
	}
	
	/**
	 * Regresa la lista de coberturas contratadas que tengan el IdToCobertura recibido.
	 * @param idToCobertura
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasContratadasPorIdToCobertura(BigDecimal idToCobertura){
		if (this.listaCoberturasContratadasCotizacion != null){
			if (mapaCoberturasContratadasPorIdToCobertura == null){
				mapaCoberturasContratadasPorIdToCobertura = new HashMap<BigDecimal,List<CoberturaCotizacionDTO>>();
			}
			List<CoberturaCotizacionDTO> listaCoberturasContratadasPorIdToCobertura = mapaCoberturasContratadasPorIdToCobertura.get(idToCobertura);
			if (listaCoberturasContratadasPorIdToCobertura == null){
				listaCoberturasContratadasPorIdToCobertura = new ArrayList<CoberturaCotizacionDTO>();
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
					if (coberturaCot.getId().getIdToCobertura().compareTo(idToCobertura) == 0)
						listaCoberturasContratadasPorIdToCobertura.add(coberturaCot);
				}
				mapaCoberturasContratadasPorIdToCobertura.put(idToCobertura, listaCoberturasContratadasPorIdToCobertura);
			}
			return listaCoberturasContratadasPorIdToCobertura;
		}
		else
			return null;
	}
	
	/**
	 * Regresa la lista de coberturas contratadas que tengan el IdToCobertura y el n�mero de inciso recibidos.
	 * @param idToCobertura
	 * @param numeroInciso
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasContratadasPorIncisoIdToCobertura(BigDecimal numeroInciso,BigDecimal idToCobertura){
		if (this.listaCoberturasContratadasCotizacion != null){
			if (mapaCoberturasContratadasPorIncisoIdToCobertura == null){
				mapaCoberturasContratadasPorIncisoIdToCobertura = new HashMap<BigDecimal[],List<CoberturaCotizacionDTO>>();
			}
			BigDecimal []keyIncisoCobertura = new BigDecimal[2];
			keyIncisoCobertura[0] = numeroInciso;
			keyIncisoCobertura[1] = idToCobertura;
			List<CoberturaCotizacionDTO> listaCoberturasContratadasPorIncisoIdToCobertura = mapaCoberturasContratadasPorIncisoIdToCobertura.get(keyIncisoCobertura);
			if (listaCoberturasContratadasPorIncisoIdToCobertura == null){
				listaCoberturasContratadasPorIncisoIdToCobertura = new ArrayList<CoberturaCotizacionDTO>();
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
					if (coberturaCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0 && 
							coberturaCot.getId().getIdToCobertura().compareTo(idToCobertura) == 0)
						listaCoberturasContratadasPorIncisoIdToCobertura.add(coberturaCot);
				}
				mapaCoberturasContratadasPorIncisoIdToCobertura.put(keyIncisoCobertura, listaCoberturasContratadasPorIncisoIdToCobertura);
			}
			return listaCoberturasContratadasPorIncisoIdToCobertura;
		}
		else
			return null;
	}
	
	/**
	 * Regresa la lista de coberturas NO contratadas que tengan el IdToCobertura y el n�mero de inciso recibidos.
	 * @param idToCobertura
	 * @param numeroInciso
	 * @return List<CoberturaCotizacionDTO>
	 * @throws SystemException 
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasNoContratadasPorIncisoIdToCobertura(BigDecimal numeroInciso,BigDecimal idToCobertura) throws SystemException{
		consultarCoberturasNoContratadas();
		if (mapaCoberturasNoContratadasPorIncisoIdToCobertura == null){
			mapaCoberturasNoContratadasPorIncisoIdToCobertura = new HashMap<BigDecimal[],List<CoberturaCotizacionDTO>>();
		}
		BigDecimal []keyIncisoCobertura = new BigDecimal[2];
		keyIncisoCobertura[0] = numeroInciso;
		keyIncisoCobertura[1] = idToCobertura;
		List<CoberturaCotizacionDTO> listaCoberturasNoContratadasPorIncisoIdToCobertura = mapaCoberturasNoContratadasPorIncisoIdToCobertura.get(keyIncisoCobertura);
		if (listaCoberturasNoContratadasPorIncisoIdToCobertura == null){
			listaCoberturasNoContratadasPorIncisoIdToCobertura = new ArrayList<CoberturaCotizacionDTO>();
			for(CoberturaCotizacionDTO coberturaCot : listaCoberturasNoContratadasCotizacion){
				if (coberturaCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0 && 
						coberturaCot.getId().getIdToCobertura().compareTo(idToCobertura) == 0)
					listaCoberturasNoContratadasPorIncisoIdToCobertura.add(coberturaCot);
			}
			mapaCoberturasNoContratadasPorIncisoIdToCobertura.put(keyIncisoCobertura, listaCoberturasNoContratadasPorIncisoIdToCobertura);
		}
		return listaCoberturasNoContratadasPorIncisoIdToCobertura;
	}
	
	/**
	 * Regresa la cobertura contratada que tenga el IdToCobertura, el n�mero de inciso e idToSeccion recibidos.
	 * Si la cobertura no est� contratada o no existe, regresa nulo
	 * @param idToCobertura
	 * @param numeroInciso
	 * @param idToSeccion
	 * @return CoberturaCotizacionDTO
	 */
	public CoberturaCotizacionDTO obtenerCoberturaContratadaPorId(BigDecimal numeroInciso,BigDecimal idToSeccion,BigDecimal idToCobertura){
		CoberturaCotizacionDTO coberturaEncontrada = null;
		if (this.listaCoberturasContratadasCotizacion != null){
			if (mapaCoberturasContratadasPorIdCompleto == null){
				mapaCoberturasContratadasPorIdCompleto = new HashMap<BigDecimal[],CoberturaCotizacionDTO>();
			}
			BigDecimal []keyIncisoCoberturaSeccion = new BigDecimal[3];
			keyIncisoCoberturaSeccion[0] = numeroInciso;
			keyIncisoCoberturaSeccion[1] = idToCobertura;
			keyIncisoCoberturaSeccion[2] = idToSeccion;
			coberturaEncontrada= mapaCoberturasContratadasPorIdCompleto.get(keyIncisoCoberturaSeccion);
			if (coberturaEncontrada == null){
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
					if (coberturaCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0 && 
							coberturaCot.getId().getIdToCobertura().compareTo(idToCobertura) == 0 && 
							coberturaCot.getId().getIdToSeccion().compareTo(idToSeccion) == 0){
						coberturaEncontrada = coberturaCot;
						break;
					}
				}
				if(coberturaEncontrada != null){
					mapaCoberturasContratadasPorIdCompleto.put(keyIncisoCoberturaSeccion, coberturaEncontrada);
				}
			}
			return coberturaEncontrada;
		}
		if (this.listaCoberturasContratadasCotizacion != null){
			List<CoberturaCotizacionDTO> listaCoberturasContratadasPorIncisoIdToCobertura = obtenerCoberturasContratadasPorIncisoIdToCobertura(numeroInciso, idToCobertura);
			for(CoberturaCotizacionDTO coberturaEnCurso : listaCoberturasContratadasPorIncisoIdToCobertura){
				if(coberturaEnCurso.getId().getIdToSeccion().compareTo(idToSeccion) == 0){
					coberturaEncontrada = coberturaEnCurso;
					break;
				}
			}
		}
		return coberturaEncontrada;
	}
	
	/**
	 * Regresa la cobertura no contratada que tenga el IdToCobertura, el n�mero de inciso e idToSeccion recibidos.
	 * Si la cobertura est� contratada o no existe, regresa nulo.
	 * @param idToCobertura
	 * @param numeroInciso
	 * @param idToSeccion
	 * @return CoberturaCotizacionDTO
	 * @throws SystemException 
	 */
	public CoberturaCotizacionDTO obtenerCoberturaNoContratadaPorId(BigDecimal numeroInciso,BigDecimal idToSeccion,BigDecimal idToCobertura) throws SystemException{
		consultarCoberturasNoContratadas();
		CoberturaCotizacionDTO coberturaEncontrada = null;
		if (this.listaCoberturasNoContratadasCotizacion != null){
			List<CoberturaCotizacionDTO> listaCoberturasNoContratadasPorIncisoIdToCobertura = obtenerCoberturasNoContratadasPorIncisoIdToCobertura(numeroInciso, idToCobertura);
			for(CoberturaCotizacionDTO coberturaEnCurso : listaCoberturasNoContratadasPorIncisoIdToCobertura){
				if(coberturaEnCurso.getId().getIdToSeccion().compareTo(idToSeccion) == 0){
					coberturaEncontrada = coberturaEnCurso;
					break;
				}
			}
		}
		return coberturaEncontrada;
	}
	
	/**
	 * Regresa la cobertura que tenga el IdToCobertura, el n�mero de inciso e idToSeccion recibidos.
	 * @param idToCobertura
	 * @param numeroInciso
	 * @param idToSeccion
	 * @return CoberturaCotizacionDTO
	 * @throws SystemException 
	 */
	public CoberturaCotizacionDTO obtenerCoberturaPorIdOmiteContrato(BigDecimal numeroInciso,BigDecimal idToSeccion,BigDecimal idToCobertura) throws SystemException{
		CoberturaCotizacionDTO coberturaEncontrada = obtenerCoberturaContratadaPorId(numeroInciso, idToSeccion, idToCobertura);
		if(coberturaEncontrada == null)
			coberturaEncontrada = obtenerCoberturaNoContratadaPorId(numeroInciso, idToSeccion, idToCobertura);
		return coberturaEncontrada;
	}
	
	/**
	 * Regresa la lista de coberturas contratadas por inciso.
	 * @param numeroInciso
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasContratadasLUCPorNumeroAgrupacion(Short numeroAgrupacion){
		if (this.listaCoberturasContratadasCotizacion != null){
			if (mapaCoberturasContratadasLUCPorNumeroAgrupacion == null){
				mapaCoberturasContratadasLUCPorNumeroAgrupacion = new HashMap<Short,List<CoberturaCotizacionDTO>>();
			}
			List<CoberturaCotizacionDTO> listaCoberturasContratadasLUCPorAgrupacion = mapaCoberturasContratadasLUCPorNumeroAgrupacion.get(numeroAgrupacion);
			if (listaCoberturasContratadasLUCPorAgrupacion == null){
				listaCoberturasContratadasLUCPorAgrupacion = new ArrayList<CoberturaCotizacionDTO>();
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
					if (coberturaCot.getNumeroAgrupacion().intValue() == numeroAgrupacion.intValue())
						listaCoberturasContratadasLUCPorAgrupacion.add(coberturaCot);
				}
				mapaCoberturasContratadasLUCPorNumeroAgrupacion.put(numeroAgrupacion, listaCoberturasContratadasLUCPorAgrupacion);
			}
			return listaCoberturasContratadasLUCPorAgrupacion;
		}
		else
			return null;
	}
	
	/**
	 * Regresa la lista de coberturas contratadas por inciso.
	 * @param numeroInciso
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasContratadasLUCPorSeccion(BigDecimal idToSeccion){
		if (this.listaCoberturasContratadasCotizacion != null){
			List<CoberturaCotizacionDTO> listaCoberturasContratadasLUCPorSeccion = new ArrayList<CoberturaCotizacionDTO>() ;
			listaCoberturasContratadasLUCPorSeccion = new ArrayList<CoberturaCotizacionDTO>();
			for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
				if (coberturaCot.getId().getIdToSeccion().intValue() == idToSeccion.intValue())
					listaCoberturasContratadasLUCPorSeccion.add(coberturaCot);
			}
			return listaCoberturasContratadasLUCPorSeccion;
		}
		else
			return null;
	}
	
	/**
	 * Calcula la lista de coberturas contratadas pertenecientes a un subramo, sin importar el inciso, ni secci�n.
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasContratadasPorIncisoSubRamo(BigDecimal numeroInciso,BigDecimal idTcSubRamo){
		if (this.listaCoberturasContratadasCotizacion != null){
			if (mapaCoberturasContratadasPorIncisoSubRamo == null){
				mapaCoberturasContratadasPorIncisoSubRamo = new HashMap<BigDecimal[],List<CoberturaCotizacionDTO>>();
			}
			BigDecimal []keyIncisoSubRamo = new BigDecimal[2];
			keyIncisoSubRamo[0] = numeroInciso;
			keyIncisoSubRamo[1] = idTcSubRamo;
			List<CoberturaCotizacionDTO> listaCoberturasContratadasPorIncisoSubRamo= mapaCoberturasContratadasPorIncisoSubRamo.get(keyIncisoSubRamo);
			if (listaCoberturasContratadasPorIncisoSubRamo == null){
				listaCoberturasContratadasPorIncisoSubRamo = new ArrayList<CoberturaCotizacionDTO>();
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
					if (coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo().compareTo(idTcSubRamo) == 0 &&
						coberturaCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0)
						listaCoberturasContratadasPorIncisoSubRamo.add(coberturaCot);
				}
				mapaCoberturasContratadasPorIncisoSubRamo.put(keyIncisoSubRamo, listaCoberturasContratadasPorIncisoSubRamo);
			}
			return listaCoberturasContratadasPorIncisoSubRamo;
		}
		else
			return null;
	}

	public AdministradorDatoIncisoCotizacion getAdministradorDatosInciso() {
		return administradorDatosInciso;
	}

	public void setAdministradorDatosInciso(AdministradorDatoIncisoCotizacion administradorDatosInciso) {
		this.administradorDatosInciso = administradorDatosInciso;
	}
}
