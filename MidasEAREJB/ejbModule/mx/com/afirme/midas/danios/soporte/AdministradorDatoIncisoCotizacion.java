package mx.com.afirme.midas.danios.soporte;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.equipoelectronico.SubtipoEquipoElectronicoDTO;
import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.giro.SubGiroFacadeRemote;
import mx.com.afirme.midas.catalogos.girorc.SubGiroRCDTO;
import mx.com.afirme.midas.catalogos.girorc.SubGiroRCFacadeRemote;
import mx.com.afirme.midas.catalogos.plenoreaseguro.PlenoReaseguroDTO;
import mx.com.afirme.midas.catalogos.plenoreaseguro.PlenoReaseguroFacadeRemote;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaDTO;
import mx.com.afirme.midas.catalogos.tipomaquinaria.SubTipoMaquinariaDTO;
import mx.com.afirme.midas.catalogos.tipomontajemaquina.SubtipoMontajeMaquinaDTO;
import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * 
 * @author jose luis arellano
 */
public class AdministradorDatoIncisoCotizacion {
	private BigDecimal idToCotizacion;
	private List<DatoIncisoCotizacionDTO> listaDatosIncisoCotizacion;
	
	private Map<BigDecimal[],List<DatoIncisoCotizacionDTO>> mapaDatosIncisoCotizacion = new HashMap<BigDecimal[], List<DatoIncisoCotizacionDTO>>();
	private Map<BigDecimal[],DatoIncisoCotizacionDTO> mapaDatosIncisoCotizacionPorIncisoRamoIdDato = new HashMap<BigDecimal[], DatoIncisoCotizacionDTO>();
	
//	private static Map<BigDecimal[],ConfiguracionDatoIncisoCotizacionDTO> mapaConfiguracionDatoInciso = new HashMap<BigDecimal[], ConfiguracionDatoIncisoCotizacionDTO>();
	
	//Mapa usado para almacenar los subgiros
	private Map<BigDecimal,SubGiroDTO> mapaSubGiros = new HashMap<BigDecimal, SubGiroDTO>();
	
	//Mapa usado para almacenar los subgiroRC
	private Map<BigDecimal,SubGiroRCDTO> mapaSubGiroRC = new HashMap<BigDecimal, SubGiroRCDTO>();
	
	//Mapa usado para almacenar los registros de PlenoReaseguroDTO
	private Map<BigDecimal,PlenoReaseguroDTO> mapaPlenoReaseguro= new HashMap<BigDecimal, PlenoReaseguroDTO>();
	
	//Mapa usado para almacenar los SubTipoMontajeMaquina
	private Map<BigDecimal,SubtipoMontajeMaquinaDTO> mapaSubTipoMontajeMaquina = new HashMap<BigDecimal, SubtipoMontajeMaquinaDTO>();
	
	//Mapa usado para almacenar los SubTipoEquipoElectronico
	private Map<BigDecimal,SubtipoEquipoElectronicoDTO> mapaSubTipoEquipoElectronico = new HashMap<BigDecimal, SubtipoEquipoElectronicoDTO>();
	
	//Mapa usado para almacenar los SubtipoEquipoContratistaDTO
	private Map<BigDecimal,SubtipoEquipoContratistaDTO> mapaSubTipoEquipoContratista= new HashMap<BigDecimal, SubtipoEquipoContratistaDTO>();
	
	//Mapa usado para almacenar los SubTipoObraCivilDTO
	private Map<BigDecimal,TipoObraCivilDTO> mapaSubTipoObraCivil =new HashMap<BigDecimal, TipoObraCivilDTO>();

	private Map<BigDecimal,SubTipoMaquinariaDTO> mapaSubTipoMaquinaria = new HashMap<BigDecimal, SubTipoMaquinariaDTO>();
	
	/*
	 * EJB's necesarios para el acceso a datos.
	 */
	private static DatoIncisoCotizacionFacadeRemote datoIncisoCotizacionFacade;
	private static SubGiroFacadeRemote subGiroFacade;
	private static SubGiroRCFacadeRemote subGiroRCFacade;
	private static PlenoReaseguroFacadeRemote plenoReaseguroFacade;
	
	static {
		try {
			datoIncisoCotizacionFacade = ServiceLocatorP.getInstance().getEJB(DatoIncisoCotizacionFacadeRemote.class);
			subGiroFacade = ServiceLocatorP.getInstance().getEJB(SubGiroFacadeRemote.class);
			subGiroRCFacade = ServiceLocatorP.getInstance().getEJB(SubGiroRCFacadeRemote.class);
			plenoReaseguroFacade = ServiceLocatorP.getInstance().getEJB(PlenoReaseguroFacadeRemote.class);
		} catch (javax.transaction.SystemException e) {
			e.printStackTrace();
		}
	}

	public AdministradorDatoIncisoCotizacion(BigDecimal idToCotizacion){
		this.idToCotizacion = idToCotizacion;
	}
	
	public void consultarDatosIncisoCotizacion() throws SystemException{
		if(listaDatosIncisoCotizacion == null){
			listaDatosIncisoCotizacion = datoIncisoCotizacionFacade.listarPorIdToCotizacion(idToCotizacion);
		}
	}
	
	public List<DatoIncisoCotizacionDTO> obtenerDatoIncisoCotPorNumeroInciso(BigDecimal numeroInciso) throws SystemException{
		consultarDatosIncisoCotizacion();
		BigDecimal []key = new BigDecimal[1];
		key[0] = numeroInciso;
		List<DatoIncisoCotizacionDTO> listaEncontrada = mapaDatosIncisoCotizacion.get(key);
		if(listaEncontrada == null){
			listaEncontrada = new ArrayList<DatoIncisoCotizacionDTO>();
			for(DatoIncisoCotizacionDTO datoTMP : listaDatosIncisoCotizacion){
				if(datoTMP.getId().getNumeroInciso().compareTo(numeroInciso) == 0){
					listaEncontrada.add(datoTMP);
				}
			}
			mapaDatosIncisoCotizacion.put(key, listaEncontrada);
		}
		return listaEncontrada;
	}
	
	public List<DatoIncisoCotizacionDTO> obtenerDatoIncisoCotPorIncisoSubInciso(BigDecimal numeroInciso,BigDecimal numeroSubInciso) throws SystemException{
		consultarDatosIncisoCotizacion();
		BigDecimal []key = new BigDecimal[2];
		key[0] = numeroInciso;
		key[1] = numeroSubInciso;
		List<DatoIncisoCotizacionDTO> listaEncontrada = mapaDatosIncisoCotizacion.get(key);
		if(listaEncontrada == null){
			listaEncontrada = new ArrayList<DatoIncisoCotizacionDTO>();
			
			List<DatoIncisoCotizacionDTO> listaEncontradaPorInciso = busquedaRapidaPorInciso(numeroInciso);
			List<DatoIncisoCotizacionDTO> listaUniversoPorBuscar = null;
			
			if(listaEncontradaPorInciso != null){
				listaUniversoPorBuscar = listaEncontradaPorInciso;
			}
			else{
				listaUniversoPorBuscar = listaDatosIncisoCotizacion;
			}
			for(DatoIncisoCotizacionDTO datoTMP : listaUniversoPorBuscar){
				if(datoTMP.getId().getNumeroInciso().compareTo(numeroInciso) == 0 &&
						datoTMP.getId().getNumeroSubinciso().compareTo(numeroSubInciso) == 0){
					listaEncontrada.add(datoTMP);
				}
			}
			mapaDatosIncisoCotizacion.put(key, listaEncontrada);
		}
		return listaEncontrada;
	}
	
	public DatoIncisoCotizacionDTO obtenerDatoIncisoCotPorIncisoRamoIdDato(BigDecimal numeroInciso,BigDecimal idTcRamo,BigDecimal idDato) throws SystemException{
		consultarDatosIncisoCotizacion();
		BigDecimal []key = new BigDecimal[3];
		key[0] = numeroInciso;
		key[1] = idTcRamo;
		key[2] = idDato;
		DatoIncisoCotizacionDTO datoEncontrado = mapaDatosIncisoCotizacionPorIncisoRamoIdDato.get(key);
		if(datoEncontrado == null){
			
			List<DatoIncisoCotizacionDTO> listaPorInciso = busquedaRapidaPorInciso(numeroInciso);
			List<DatoIncisoCotizacionDTO> listaUniversoPorBuscar = listaPorInciso!= null ? listaPorInciso: listaDatosIncisoCotizacion;
			
			for(DatoIncisoCotizacionDTO datoTMP : listaUniversoPorBuscar){
				if(datoTMP.getId().getNumeroInciso().compareTo(numeroInciso) == 0 &&
						datoTMP.getId().getIdTcRamo().compareTo(idTcRamo) == 0 &&
						datoTMP.getId().getIdDato().compareTo(idDato) == 0){
					datoEncontrado = datoTMP;
					break;
				}
			}
			if(datoEncontrado != null)
				mapaDatosIncisoCotizacionPorIncisoRamoIdDato.put(key, datoEncontrado);
		}
		return datoEncontrado;
	}
	
	public SubGiroDTO obtenerSubGiroDTO(BigDecimal numeroInciso,BigDecimal idTcRamo) throws SystemException{
		//Busqueda del datoIncisoCot
		//Para obtener el subgiro, se utiliza el idDato = 20
		DatoIncisoCotizacionDTO datoSubGiro = obtenerDatoIncisoCotPorIncisoRamoIdDato(numeroInciso, idTcRamo, new BigDecimal("20"));
		if(datoSubGiro == null){
			throw new SystemException("No se encontr� datoIncisoCot para definir el subGiro. idToCotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo: "+idTcRamo);
		}
		
		BigDecimal idTcSubGiro = Utilerias.regresaBigDecimal(datoSubGiro.getValor());
		SubGiroDTO subGiro = mapaSubGiros.get(idTcSubGiro);
		
		if(subGiro == null){
			subGiro = subGiroFacade.findById(idTcSubGiro);
			if(subGiro != null){
				mapaSubGiros.put(idTcSubGiro, subGiro);
			}
		}
		return subGiro;
	}
	
	public SubGiroRCDTO obtenerSubGiroRCDTO(BigDecimal numeroInciso,BigDecimal idTcRamo) throws SystemException{
		//Busqueda del datoIncisoCot
		//Para obtener el subgiroRC, se utiliza el idDato = 20
		DatoIncisoCotizacionDTO datoSubGiro = obtenerDatoIncisoCotPorIncisoRamoIdDato(numeroInciso, idTcRamo, new BigDecimal("20"));
		if(datoSubGiro == null){
			throw new SystemException("No se encontr� datoIncisoCot para definir el subGiroRC. idToCotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo: "+idTcRamo);
		}
		
		BigDecimal idTcSubGiro = Utilerias.regresaBigDecimal(datoSubGiro.getValor());
		SubGiroRCDTO subGiro = mapaSubGiroRC.get(idTcSubGiro);
		
		if(subGiro == null){
			subGiro = subGiroRCFacade.findById(idTcSubGiro);
			if(subGiro != null){
				mapaSubGiroRC.put(idTcSubGiro, subGiro);
			}
		}
		return subGiro;
	}
	
	public PlenoReaseguroDTO obtenerPlenoReaseguro(BigDecimal numeroInciso,BigDecimal idTcRamo) throws SystemException{
		SubGiroDTO subGiro = obtenerSubGiroDTO(numeroInciso, idTcRamo);
		if(subGiro == null){
			throw new SystemException("No se encontr� subgiro para los datos: idToCotizacion: "+idToCotizacion+", idTcRamo: "+idTcRamo);
		}
		BigDecimal idPlenoReaseguro = subGiro.getIdGrupoPlenos();
		PlenoReaseguroDTO plenoEncontrado = mapaPlenoReaseguro.get(idPlenoReaseguro);
		if(plenoEncontrado == null){
			plenoEncontrado = plenoReaseguroFacade.findById(idPlenoReaseguro);
			if(plenoEncontrado != null){
				mapaPlenoReaseguro.put(idPlenoReaseguro, plenoEncontrado);
			}
		}
		return plenoEncontrado;
	}

	private List<DatoIncisoCotizacionDTO> busquedaRapidaPorInciso(BigDecimal numeroInciso){
		BigDecimal[] keyBusquedaPorInciso = new BigDecimal[1];
		keyBusquedaPorInciso[0] = numeroInciso;
		
		List<DatoIncisoCotizacionDTO> listaEncontradaPorInciso = mapaDatosIncisoCotizacion.get(keyBusquedaPorInciso);
		return listaEncontradaPorInciso;
	}
	
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public List<DatoIncisoCotizacionDTO> getListaDatosIncisoCotizacion() {
		return listaDatosIncisoCotizacion;
	}

	public void setListaDatosIncisoCotizacion(List<DatoIncisoCotizacionDTO> listaDatosIncisoCotizacion) {
		this.listaDatosIncisoCotizacion = listaDatosIncisoCotizacion;
	}
	public Map<BigDecimal, SubTipoMaquinariaDTO> getMapaSubTipoMaquinaria() {
		return mapaSubTipoMaquinaria;
	}

	public void setMapaSubTipoMaquinaria(Map<BigDecimal, SubTipoMaquinariaDTO> mapaSubTipoMaquinaria) {
		this.mapaSubTipoMaquinaria = mapaSubTipoMaquinaria;
	}
	
	public Map<BigDecimal, SubtipoMontajeMaquinaDTO> getMapaSubTipoMontajeMaquina() {
		return mapaSubTipoMontajeMaquina;
	}

	public void setMapaSubTipoMontajeMaquina(Map<BigDecimal, SubtipoMontajeMaquinaDTO> mapaSubTipoMontajeMaquina) {
		this.mapaSubTipoMontajeMaquina = mapaSubTipoMontajeMaquina;
	}
	
	public Map<BigDecimal, SubtipoEquipoElectronicoDTO> getMapaSubTipoEquipoElectronico() {
		return mapaSubTipoEquipoElectronico;
	}

	public void setMapaSubTipoEquipoElectronico(Map<BigDecimal, SubtipoEquipoElectronicoDTO> mapaSubTipoEquipoElectronico) {
		this.mapaSubTipoEquipoElectronico = mapaSubTipoEquipoElectronico;
	}
	
	public Map<BigDecimal, SubtipoEquipoContratistaDTO> getMapaSubTipoEquipoContratista() {
		return mapaSubTipoEquipoContratista;
	}

	public void setMapaSubTipoEquipoContratista(Map<BigDecimal, SubtipoEquipoContratistaDTO> mapaSubTipoEquipoContratista) {
		this.mapaSubTipoEquipoContratista = mapaSubTipoEquipoContratista;
	}
	
	public Map<BigDecimal, TipoObraCivilDTO> getMapaSubTipoObraCivil() {
		return mapaSubTipoObraCivil;
	}

	public void setMapaSubTipoObraCivil(Map<BigDecimal, TipoObraCivilDTO> mapaSubTipoObraCivil) {
		this.mapaSubTipoObraCivil = mapaSubTipoObraCivil;
	}
}
