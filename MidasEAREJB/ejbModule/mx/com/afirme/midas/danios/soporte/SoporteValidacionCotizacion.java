package mx.com.afirme.midas.danios.soporte;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.SubTipoGenerico;
import mx.com.afirme.midas.catalogos.equipoelectronico.SubtipoEquipoElectronicoDTO;
import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.girorc.SubGiroRCDTO;
import mx.com.afirme.midas.catalogos.plenoreaseguro.PlenoReaseguroDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoFacadeRemote;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaDTO;
import mx.com.afirme.midas.catalogos.tipomaquinaria.SubTipoMaquinariaDTO;
import mx.com.afirme.midas.catalogos.tipomontajemaquina.SubtipoMontajeMaquinaDTO;
import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.reaseguro.ConstantesReaseguro;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.validacion.CumuloPoliza;
import mx.com.afirme.midas.reaseguro.soporte.validacion.DetalleCobertura;
import mx.com.afirme.midas.reaseguro.soporte.validacion.ResultadoValidacionReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.validacion.SoporteReaseguroCotizacion;
import mx.com.afirme.midas.reaseguro.soporte.validacion.ValidacionReaseguroFacultativoPorCatalogo;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;

public class SoporteValidacionCotizacion {

	private Map<BigDecimal, Double> sumasAseguradasIncendio= new HashMap<BigDecimal, Double>();
	private Map<BigDecimal, List<CoberturaCotizacionDTO>> subRamosPrimerRiesgo= new HashMap<BigDecimal, List<CoberturaCotizacionDTO>>();
	private List<CumuloPoliza> listaCumulos = new ArrayList<CumuloPoliza>();
	
	private boolean mostrarMensajeErrorIncisos = false;
	private boolean mostrarMensajeErrorSubRamosNoValidos = false;
	
	private StringBuilder mensajeError= new StringBuilder();
	private StringBuilder subRamosNoValidos = new StringBuilder();
	private BigDecimal factor=BigDecimal.ONE;
	
	private SoporteEstructuraCotizacion estructuraCotizacion = null;
	private SoporteReaseguroCotizacion soporteReaseguroCotizacion = null;
	
	protected DatoIncisoCotizacionFacadeRemote datoIncisoCotizacionFacade;
	protected CotizacionFacadeRemote cotizacionFacade;
	protected SubRamoFacadeRemote subRamofacade;
	
	protected static final short AREA_REASEGURO = (short) 2;
	protected static final short CLAVE_INSPECCION_EXCLUIDO = (short) 9;
	
	{
		try {
			datoIncisoCotizacionFacade = ServiceLocatorP.getInstance().getEJB(DatoIncisoCotizacionFacadeRemote.class);
			cotizacionFacade = ServiceLocatorP.getInstance().getEJB(CotizacionFacadeRemote.class);
			subRamofacade = ServiceLocatorP.getInstance().getEJB(SubRamoFacadeRemote.class);
		} catch (javax.transaction.SystemException e) {
			e.printStackTrace();
			throw new RuntimeException("Error al instanciar SoporteValidacionCotizacion, no se pudo obtener EJB.",e);
		}
	}
	
	public SoporteValidacionCotizacion(SoporteEstructuraCotizacion estructuraCotizacion,
			SoporteReaseguroCotizacion soporteReaseguroCotizacion){
		this.estructuraCotizacion = estructuraCotizacion;
		this.soporteReaseguroCotizacion = soporteReaseguroCotizacion;
		factor = Utilerias.getFactorVigencia(estructuraCotizacion.getCotizacionDTO().getFechaInicioVigencia(),
				estructuraCotizacion.getCotizacionDTO().getFechaFinVigencia());
	}
	
	public void procesarSubRamosCotizacion() throws SystemException{
		List<SubRamoDTO> listaSubRamosContratados = estructuraCotizacion.listarSubRamosContratados();
		
		for (SubRamoDTO subRamo : listaSubRamosContratados) {
			LineaDTO lineaDTO=null;
			LogDeMidasEJB3.log("SoporteValidacionCotizacion.procesarSubRamosCotizacion: se invocar� SoporteReaseguro.getTipoDistribucionSubramo, para la fechaInicioVigencia: "+estructuraCotizacion.getCotizacionDTO().getFechaInicioVigencia()+" , idTcSubRamo: "+subRamo.getIdTcSubRamo(), Level.INFO, null);
			
			lineaDTO = soporteReaseguroCotizacion.obtenerLineaPorSubRamo(estructuraCotizacion.getCotizacionDTO().getFechaInicioVigencia(), subRamo.getIdTcSubRamo());
			
			LogDeMidasEJB3.log("SoporteValidacionCotizacion.procesarSubRamosCotizacion: termin� la invocaci�n de SoporteReaseguro.getTipoDistribucionSubramo", Level.INFO, null);
			// Si el tipo de c�mulo es por p�liza:
			if (lineaDTO != null && lineaDTO.getTipoDistribucion().intValue() == ConstantesReaseguro.TIPO_CUMULO_POLIZA) {
				procesarCumuloPoliza(lineaDTO);
			}
			else if (lineaDTO != null && lineaDTO.getTipoDistribucion().intValue() == ConstantesReaseguro.TIPO_CUMULO_INCISO) {
				procesarCumuloInciso(lineaDTO);
			}
			else if (lineaDTO != null && lineaDTO.getTipoDistribucion().intValue() == ConstantesReaseguro.TIPO_CUMULO_SUBINCISO) {
				procesarCumuloSubInciso(lineaDTO);
			}
			else{
				registrarSubRamoInvalido(subRamo);
			}
		}//fin ciclo subramo
		
		procesarCumulosAgrupacion();
	}
	
	public void registrarCumulosReaseguro(){
		try {
				soporteReaseguroCotizacion.insertarCumulosLinea(listaCumulos);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	public ResultadoValidacionReaseguroDTO obtenerResultadoValidacion(){
		ResultadoValidacionReaseguroDTO resultado = new ResultadoValidacionReaseguroDTO();
		
		List<LineaSoporteReaseguroDTO> listaCumuloPoliza = soporteReaseguroCotizacion.getListaLineasSoporte(new BigDecimal(ConstantesReaseguro.TIPO_CUMULO_POLIZA));
		List<LineaSoporteReaseguroDTO> listaCumuloInciso = soporteReaseguroCotizacion.getListaLineasSoporte(new BigDecimal(ConstantesReaseguro.TIPO_CUMULO_INCISO));
		List<LineaSoporteReaseguroDTO> listaCumuloSubInciso = soporteReaseguroCotizacion.getListaLineasSoporte(new BigDecimal(ConstantesReaseguro.TIPO_CUMULO_SUBINCISO));
		
		resultado.setListaCumuloInciso(listaCumuloInciso);
		resultado.setListaCumuloPoliza(listaCumuloPoliza);
		resultado.setListaCumuloSubInciso(listaCumuloSubInciso);
		
		StringBuilder mensajesErrorCumulo = new StringBuilder();
		if (mostrarMensajeErrorSubRamosNoValidos){
			mensajesErrorCumulo.append("Los siguientes SubRamos no presentaron tipo de cumulo v�lido : <br/>").append(subRamosNoValidos);
			mensajesErrorCumulo.append("<hr>");
		}
		
		mensajesErrorCumulo.append(mensajeError.toString().length()>0?mensajeError.toString():"");
		
		resultado.setMensajeErrorCumulos(mensajesErrorCumulo.toString());
		
		return resultado;
	}
	
	private void procesarCumuloPoliza(LineaDTO lineaDTO) throws SystemException{
		double sumaAseguradaIncendio=0;
		
		// i. Validar si se requiere facultativo por c�talogo
		ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroPorCatalogo = 
				validarReaseguroFacultativoPorCatalogoCumuloPoliza(estructuraCotizacion, lineaDTO.getSubRamo(), estructuraCotizacion.getIdToCotizacion());

		// ii. Obtener todas las coberturas contratadas de la cotizaci�n correspondientes al subramo en cuesti�n y que no hayan aplicado a primer riesgo.
		List<CoberturaCotizacionDTO> listaCoberturasTMP = null;
		listaCoberturasTMP = estructuraCotizacion.obtenerCoberturasContratadasPorSubRamo(lineaDTO.getSubRamo().getIdTcSubRamo());
		// Que no apliquen primerRiesgo:
		List<CoberturaCotizacionDTO> listaCoberturas = new ArrayList<CoberturaCotizacionDTO>();
		Double sumaAseguradaLinea = 0d;

		DetalleCobertura detalleCobertura;
		List<DetalleCobertura> listaDetalleCobertura = new ArrayList<DetalleCobertura>();

		Boolean permitirSolicitarFacultativo = validacionReaseguroPorCatalogo.getPermitirSolicitarFacultativo();
		Double porcentajePleno = validacionReaseguroPorCatalogo.getPorcentajePleno();
		
		for (CoberturaCotizacionDTO coberturaCot : listaCoberturasTMP) {
			if (!estructuraCotizacion.aplicoPrimerRiesgo(coberturaCot)) {
				/**
				 * 16/01/2010 Jos� Luis Arellano. Se agreg� validaci�n para no enviar coberturas que aplicaron a LUC.
				 */
				if (!estructuraCotizacion.aplicoLUC(coberturaCot)){
					listaCoberturas.add(coberturaCot);
					// Si es suma asegurada b�sica, se suma
					if (coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_BASICA)
							&& !lineaDTO.getSubRamo().getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_HIDRO)
								&&!lineaDTO.getSubRamo().getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_TERREMOTO)){
					    sumaAseguradaLinea += coberturaCot.getValorSumaAsegurada();
					}
					detalleCobertura = new DetalleCobertura();
					detalleCobertura.setNumeroInciso(coberturaCot.getId().getNumeroInciso().intValue());
					detalleCobertura.setIdSeccion(coberturaCot.getId().getIdToSeccion());
					detalleCobertura.setIdCobertura(coberturaCot.getId().getIdToCobertura());
					detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
					detalleCobertura.setMonedaPrima(estructuraCotizacion.getCotizacionDTO().getIdMoneda().intValue());
					detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
					detalleCobertura.setMontoPrimaSuscripcion(coberturaCot.getValorPrimaNeta()*factor.doubleValue());
					detalleCobertura.setEsPrimerRiesgo(false);
					listaDetalleCobertura.add(detalleCobertura);
				}
			} else{
			    if(!subRamosPrimerRiesgo.containsKey(lineaDTO.getSubRamo().getIdTcSubRamo())){
			    	subRamosPrimerRiesgo.put(lineaDTO.getSubRamo().getIdTcSubRamo(), new ArrayList<CoberturaCotizacionDTO>());
			    }
			    subRamosPrimerRiesgo.get(lineaDTO.getSubRamo().getIdTcSubRamo()).add(coberturaCot);
			}
		}
		// Se agreg� esta condici�n para validar que no se envien l�neas sin detalles de coberturas
		if (!listaDetalleCobertura.isEmpty()) {
			if (lineaDTO.getSubRamo().getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_INCENDIO)){
			    sumaAseguradaIncendio= sumaAseguradaLinea;
			}
			if (lineaDTO.getSubRamo().getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_HIDRO)|| lineaDTO.getSubRamo().getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_TERREMOTO)){
			    sumaAseguradaLinea = sumaAseguradaIncendio;
			}
			CumuloPoliza cumuloPoliza = new CumuloPoliza();
			cumuloPoliza.setIdSubRamo(lineaDTO.getSubRamo().getIdTcSubRamo());
			cumuloPoliza.setIdCotizacion(estructuraCotizacion.getCotizacionDTO().getIdToCotizacion());
			cumuloPoliza.setSumaAsegurada(sumaAseguradaLinea);
			cumuloPoliza.setNumeroInciso(null);
			cumuloPoliza.setIdSeccion(null);
			cumuloPoliza.setNumeroSubInciso(null);
			cumuloPoliza.setTipoDistribucion(ConstantesReaseguro.TIPO_CUMULO_POLIZA);
			cumuloPoliza.setLineaDTO(lineaDTO);
			cumuloPoliza.setPorcentajePleno(porcentajePleno);
			cumuloPoliza.setAplicaPrimerRiesgo(false);
			cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
			cumuloPoliza.setFechaInicioVigencia(estructuraCotizacion.getCotizacionDTO().getFechaInicioVigencia());
			cumuloPoliza.setIdMoneda(estructuraCotizacion.getCotizacionDTO().getIdMoneda());
			cumuloPoliza.setIdToPersonaAsegurado(estructuraCotizacion.getCotizacionDTO().getIdToPersonaAsegurado());
			listaCumulos.add(cumuloPoliza);
			if (!permitirSolicitarFacultativo && !validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().isEmpty()){
				if (!mostrarMensajeErrorIncisos){
					mensajeError.append("Los siguientes incisos requieren facultativo, por lo que el c�mulo completo requerir� Soporte Facultativo<br/>");
					mostrarMensajeErrorIncisos = !mostrarMensajeErrorIncisos;
				}
				mensajeError.append("<hr>C�mulo por p&oacute;liza, <br/>SubRamo: "+lineaDTO.getSubRamo().getDescripcionSubRamo()+"<br/>Incisos: ");
				for(int i=0;i < validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().size();i++)
					mensajeError.append(" "+validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().get(i).getId().getNumeroInciso()+", ");
				mensajeError = new StringBuilder(mensajeError.substring(0, mensajeError.length()-2));
			} 
		}
	
	}
	
	private void procesarCumuloInciso(LineaDTO lineaDTO) throws SystemException{
		List<IncisoCotizacionDTO> listaIncisos = estructuraCotizacion.getListaIncisos();
		
		for (IncisoCotizacionDTO inciso : listaIncisos) {
			LogDeMidasEJB3.log("CotizacionSoporteDN.validarReaseguroFacultativo: inicia validaci�n por cat�logo para c�mulo por inciso", Level.INFO, null);
			ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroPorCatalogo = validarReaseguroFacultativoPorCatalogo(estructuraCotizacion, 2, lineaDTO.getSubRamo(), 
					inciso.getId().getNumeroInciso(), null, null);
			LogDeMidasEJB3.log("CotizacionSoporteDN.validarReaseguroFacultativo: termina validaci�n por cat�logo para c�mulo por inciso", Level.INFO, null);
			List<CoberturaCotizacionDTO> listaCoberturasTMP = null;
			listaCoberturasTMP = estructuraCotizacion.obtenerCoberturasContratadasPorIncisoSubRamo(inciso.getId().getNumeroInciso(), lineaDTO.getSubRamo().getIdTcSubRamo());
			// Que no apliquen primerRiesgo:
			List<CoberturaCotizacionDTO> listaCoberturas = new ArrayList<CoberturaCotizacionDTO>();
			double sumaAseguradaLinea = 0;
			DetalleCobertura detalleCobertura;
			List<DetalleCobertura> listaDetalleCobertura = new ArrayList<DetalleCobertura>();
			for (CoberturaCotizacionDTO coberturaCot : listaCoberturasTMP) {
				try {
					if (!estructuraCotizacion.aplicoPrimerRiesgo(coberturaCot)) {
						/**
						 * 16/01/2010 Jos� Luis Arellano. Se agreg� validaci�n para no enviar coberturas que aplicaron a LUC.
						 */
						if (!estructuraCotizacion.aplicoLUC(coberturaCot)) {
							listaCoberturas.add(coberturaCot);
							if (coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals("1")
									&& !lineaDTO.getSubRamo().getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_HIDRO)&&
										!lineaDTO.getSubRamo().getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_TERREMOTO)){
											sumaAseguradaLinea += coberturaCot.getValorSumaAsegurada();
							}
							detalleCobertura = new DetalleCobertura();
							detalleCobertura.setIdSeccion(coberturaCot.getId().getIdToSeccion());
							detalleCobertura.setIdCobertura(coberturaCot.getId().getIdToCobertura());
							detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
							detalleCobertura.setMonedaPrima(estructuraCotizacion.getCotizacionDTO().getIdMoneda().intValue());
							detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
							detalleCobertura.setNumeroInciso(Integer.valueOf(inciso.getId().getNumeroInciso().toBigInteger().toString()));
							detalleCobertura.setMontoPrimaSuscripcion(coberturaCot.getValorPrimaNeta()*factor.doubleValue());
							detalleCobertura.setEsPrimerRiesgo(false);
							listaDetalleCobertura.add(detalleCobertura);
						}
					} else{
					    if(!subRamosPrimerRiesgo.containsKey(lineaDTO.getSubRamo().getIdTcSubRamo())){
					    	subRamosPrimerRiesgo.put(lineaDTO.getSubRamo().getIdTcSubRamo(), new ArrayList<CoberturaCotizacionDTO>());
					    }
					    subRamosPrimerRiesgo.get(lineaDTO.getSubRamo().getIdTcSubRamo()).add(coberturaCot);
					}
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
			// Se agreg� esta condici�n para validar que no se envien l�neas sin detalles de coberturas
			if (!listaDetalleCobertura.isEmpty()) {
			    Double porcentajePleno = validacionReaseguroPorCatalogo.getPorcentajePleno();
				//Si no se encontr� S.A. b�sica, se env�a la suma asegurada por
				//la cual est�n amparadas las coberturas del detalle.
				//Esto ocurre con los subramos de hidrometeorol�gicos y terremoto
			    if(lineaDTO.getSubRamo().getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_INCENDIO)){
		    		if(!sumasAseguradasIncendio.containsKey(inciso.getId().getNumeroInciso())){
		    			sumasAseguradasIncendio.put(inciso.getId().getNumeroInciso(),sumaAseguradaLinea);
		    		}
			    }
			    if (lineaDTO.getSubRamo().getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_HIDRO)||
			    		lineaDTO.getSubRamo().getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_TERREMOTO)){
			    	if(sumasAseguradasIncendio.containsKey(inciso.getId().getNumeroInciso())){
			    		sumaAseguradaLinea= sumasAseguradasIncendio.get(inciso.getId().getNumeroInciso());
			    	}
			    }
			    CumuloPoliza cumuloPoliza = new CumuloPoliza();
			    cumuloPoliza.setIdSubRamo(lineaDTO.getSubRamo().getIdTcSubRamo());
			    cumuloPoliza.setIdCotizacion(estructuraCotizacion.getCotizacionDTO().getIdToCotizacion());
			    cumuloPoliza.setSumaAsegurada(sumaAseguradaLinea);
			    cumuloPoliza.setNumeroInciso(Integer.valueOf(inciso.getId().getNumeroInciso().toBigInteger().toString()));
			    cumuloPoliza.setIdSeccion(null);
			    cumuloPoliza.setNumeroSubInciso(null);
		    	cumuloPoliza.setTipoDistribucion(ConstantesReaseguro.TIPO_CUMULO_INCISO);
		    	cumuloPoliza.setPorcentajePleno(porcentajePleno);
		    	cumuloPoliza.setLineaDTO(lineaDTO);
		    	cumuloPoliza.setAplicaPrimerRiesgo(false);
		    	cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
		    	cumuloPoliza.setFechaInicioVigencia(estructuraCotizacion.getCotizacionDTO().getFechaInicioVigencia());
		    	cumuloPoliza.setIdMoneda(estructuraCotizacion.getCotizacionDTO().getIdMoneda());
		    	cumuloPoliza.setIdToPersonaAsegurado(estructuraCotizacion.getCotizacionDTO().getIdToPersonaAsegurado());
			    listaCumulos.add(cumuloPoliza);
			}
		}
	}
	
	private void procesarCumuloSubInciso(LineaDTO lineaDTO) throws SystemException{
		List<IncisoCotizacionDTO> listaIncisos = estructuraCotizacion.getListaIncisos();
		
		for (IncisoCotizacionDTO inciso : listaIncisos) {
			List<CoberturaCotizacionDTO> listaCoberturasTMP = null;
			listaCoberturasTMP = estructuraCotizacion.obtenerCoberturasContratadasPorIncisoSubRamo(inciso.getId().getNumeroInciso(), lineaDTO.getSubRamo().getIdTcSubRamo());

			// Que no apliquen primerRiesgo:
			List<CoberturaCotizacionDTO> listaCoberturas = new ArrayList<CoberturaCotizacionDTO>();
			for (CoberturaCotizacionDTO coberturaCot : listaCoberturasTMP) {
				if (!estructuraCotizacion.aplicoPrimerRiesgo(coberturaCot)) {
					/**
					 * 16/01/2010 Jos� Luis Arellano. Se agreg� validaci�n para no enviar coberturas con LUC.
					 */
					if (!estructuraCotizacion.aplicoLUC(coberturaCot))
						listaCoberturas.add(coberturaCot);
				} else{
					if(!subRamosPrimerRiesgo.containsKey(lineaDTO.getSubRamo().getIdTcSubRamo())){
						subRamosPrimerRiesgo.put(lineaDTO.getSubRamo().getIdTcSubRamo(), new ArrayList<CoberturaCotizacionDTO>());
					}
					subRamosPrimerRiesgo.get(lineaDTO.getSubRamo().getIdTcSubRamo()).add(coberturaCot);
				}
			}
			if(listaCoberturas.size()>0){
				List<SubIncisoCotizacionDTO> listaSubIncisos = estructuraCotizacion.obtenerSubIncisos(listaCoberturas.get(0).getId().getIdToSeccion(), inciso.getId().getNumeroInciso());
				DetalleCobertura detalleCobertura;
				List<DetalleCobertura> listaDetalleCobertura = null;
				Double porcentajePleno = 100d;
				for (SubIncisoCotizacionDTO subInciso : listaSubIncisos) {
					ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroFacultativoPorCatalogo = validarReaseguroFacultativoPorCatalogo(
							estructuraCotizacion, 3, lineaDTO.getSubRamo(), inciso.getId().getNumeroInciso(), subInciso.getId().getIdToSeccion(), 
							subInciso.getId().getNumeroSubInciso());
					porcentajePleno = validacionReaseguroFacultativoPorCatalogo.getPorcentajePleno();
					Double sumatoriaSumaAsegurada = subInciso.getValorSumaAsegurada();
					listaDetalleCobertura= new ArrayList<DetalleCobertura>();
					for (CoberturaCotizacionDTO coberturaCot : listaCoberturas) {
						DetallePrimaCoberturaCotizacionId detallePrimaId = new DetallePrimaCoberturaCotizacionId();
						detallePrimaId.setNumeroSubInciso(subInciso.getId().getNumeroSubInciso());
						detallePrimaId.setIdToCotizacion(estructuraCotizacion.getCotizacionDTO().getIdToCotizacion());
						detallePrimaId.setNumeroInciso(inciso.getId().getNumeroInciso());
						detallePrimaId.setIdToSeccion(coberturaCot.getId().getIdToSeccion());
						detallePrimaId.setIdToCobertura(coberturaCot.getId().getIdToCobertura());
						DetallePrimaCoberturaCotizacionDTO detalle = null;
						detalle = estructuraCotizacion.obtenerDetallePrimaCoberturaCotizacionPorId(detallePrimaId);
						if (detalle != null) {
							detalleCobertura = new DetalleCobertura();
							detalleCobertura.setIdSeccion(coberturaCot.getId().getIdToSeccion());
							detalleCobertura.setIdCobertura(coberturaCot.getId().getIdToCobertura());
							detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
							detalleCobertura.setMonedaPrima(estructuraCotizacion.getCotizacionDTO().getIdMoneda().intValue());
							detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
							detalleCobertura.setNumeroInciso(Integer.valueOf(inciso.getId().getNumeroInciso().toBigInteger().toString()));
							detalleCobertura.setMontoPrimaSuscripcion(detalle.getValorPrimaNeta()*factor.doubleValue());
							detalleCobertura.setEsPrimerRiesgo(false);
							detalleCobertura.setNumeroSubinciso(Integer.valueOf(subInciso.getId().getNumeroSubInciso().toBigInteger().toString()));
							listaDetalleCobertura.add(detalleCobertura);
						}
					}// fin iterar CoberturaCot
					if (!listaDetalleCobertura.isEmpty()) {
						CumuloPoliza cumuloPoliza = new CumuloPoliza();
						cumuloPoliza.setIdSubRamo(lineaDTO.getSubRamo().getIdTcSubRamo());
						cumuloPoliza.setIdCotizacion(estructuraCotizacion.getCotizacionDTO().getIdToCotizacion());
						cumuloPoliza.setSumaAsegurada(sumatoriaSumaAsegurada);
						cumuloPoliza.setNumeroInciso(Integer.valueOf(subInciso.getId().getNumeroInciso().toBigInteger().toString()));
						cumuloPoliza.setIdSeccion(subInciso.getId().getIdToSeccion());
						cumuloPoliza.setNumeroSubInciso(Integer.valueOf(subInciso.getId().getNumeroSubInciso().toBigInteger().toString()));
						cumuloPoliza.setTipoDistribucion(ConstantesReaseguro.TIPO_CUMULO_SUBINCISO);
						cumuloPoliza.setLineaDTO(lineaDTO);
						cumuloPoliza.setPorcentajePleno(porcentajePleno);
						cumuloPoliza.setAplicaPrimerRiesgo(false);
						cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
						cumuloPoliza.setFechaInicioVigencia(estructuraCotizacion.getCotizacionDTO().getFechaInicioVigencia());
						cumuloPoliza.setIdMoneda(estructuraCotizacion.getCotizacionDTO().getIdMoneda());
						cumuloPoliza.setIdToPersonaAsegurado(estructuraCotizacion.getCotizacionDTO().getIdToPersonaAsegurado());
						listaCumulos.add(cumuloPoliza);
					}
				}// fin iterar subincisos
			}// fin if listaCoberturas > 0
		}// fin ciclo incisos
	}
	
	private void procesarCumulosAgrupacion() throws SystemException{
		List<AgrupacionCotDTO> listaAgrupacionCotizacion = estructuraCotizacion.obtenerListaAgrupacionCotizacion();
		if (listaAgrupacionCotizacion != null && !listaAgrupacionCotizacion.isEmpty()) {
			for(AgrupacionCotDTO agrupacionTMP : listaAgrupacionCotizacion){
				if (agrupacionTMP.getClaveTipoAgrupacion().intValue() == 1){
					procesarCumuloPrimerRiesgo(agrupacionTMP);
				}//Fin validaci�n agrupacionTMP.getClaveTipoAgrupacion().intValue() == 1
				/**
				 * 16/01/2010. Jos� Luis Arellano.
				 * Para cada agrupaci�n de LUC se env�a un c�mulo.
				 */
				else{
					procesarCumuloLUC(agrupacionTMP);
				}
			}
		}//fin validaci�n listaAgrupacion != null && not Empty
	}
	
	private void procesarCumuloLUC(AgrupacionCotDTO agrupacionDTO) throws SystemException{

		List<CoberturaCotizacionDTO> listaCoberturasLUC = null;
		listaCoberturasLUC = estructuraCotizacion.obtenerCoberturasContratadasLUCPorNumeroAgrupacion(agrupacionDTO.getId().getNumeroAgrupacion());
		List<DetalleCobertura> listaDetalleCoberturaLuc = new ArrayList<DetalleCobertura>();
		BigDecimal idTcSubRamoCumulo = BigDecimal.ZERO;
		if (listaCoberturasLUC != null && !listaCoberturasLUC.isEmpty()){
			idTcSubRamoCumulo = listaCoberturasLUC.get(0).getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo();
			ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroPorCatalogo = validarReaseguroFacultativoPorCatalogo(estructuraCotizacion,ConstantesReaseguro.TIPO_CUMULO_POLIZA, 
					listaCoberturasLUC.get(0).getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO(), null, null, null);
			int numeroIncisoCumuloLUC = 0;
			int j=0;
			for(CoberturaCotizacionDTO coberturaLuc : listaCoberturasLUC){
			  	DetalleCobertura detalleCobertura = new DetalleCobertura();
				detalleCobertura.setNumeroInciso(Integer.valueOf(coberturaLuc.getId().getNumeroInciso().toBigInteger().toString()));
				detalleCobertura.setIdSeccion(agrupacionDTO.getIdToSeccion());
				detalleCobertura.setIdCobertura(coberturaLuc.getId().getIdToCobertura());
				detalleCobertura.setPorcentajeCoaseguro(coberturaLuc.getPorcentajeCoaseguro());
				detalleCobertura.setMontoPrimaSuscripcion(coberturaLuc.getValorPrimaNeta()*factor.doubleValue());
				detalleCobertura.setMonedaPrima(estructuraCotizacion.getCotizacionDTO().getIdMoneda().intValue());
				detalleCobertura.setPorcentajeDeducible(coberturaLuc.getPorcentajeDeducible());
				/**
				 * 16/01/2010. Jos� Luis Arellano.
				 * Por especificaci�n de Aram, el campo de esPrimerRiesgo se env�a con valor de false en el caso de los c�mulos por LUC
				 */
				detalleCobertura.setEsPrimerRiesgo(false);
				detalleCobertura.setNumeroSubinciso(0);
				listaDetalleCoberturaLuc.add(detalleCobertura);
				if(j==0){
					numeroIncisoCumuloLUC = coberturaLuc.getId().getNumeroInciso().intValue();
					j=j+1;
				}
				else{
					if(numeroIncisoCumuloLUC != coberturaLuc.getId().getNumeroInciso().intValue()){
						numeroIncisoCumuloLUC = 0;
					}
					j=j+1;
				}
			}
			CumuloPoliza cumuloPoliza = new CumuloPoliza();
			cumuloPoliza.setIdCotizacion(estructuraCotizacion.getCotizacionDTO().getIdToCotizacion());
			cumuloPoliza.setIdSubRamo(idTcSubRamoCumulo);
			cumuloPoliza.setSumaAsegurada(agrupacionDTO.getValorSumaAsegurada());
			cumuloPoliza.setNumeroInciso(numeroIncisoCumuloLUC);
			LineaDTO lineaTMP = null;
			try {
				lineaTMP = soporteReaseguroCotizacion.obtenerLineaPorSubRamo(estructuraCotizacion.getCotizacionDTO().getFechaInicioVigencia(), idTcSubRamoCumulo);
			} catch (SystemException e1) {}
			if(lineaTMP != null){
				cumuloPoliza.setTipoDistribucion(lineaTMP.getTipoDistribucion().intValue());
				cumuloPoliza.setLineaDTO(lineaTMP);
				/**
				 * 16/01/2010. Jos� Luis Arellano.
				 * Por especificaci�n de Aram, el campo de aplicaPrimerRiesgo se env�a con valor de false en el caso de los c�mulos por LUC
				 */
				cumuloPoliza.setAplicaPrimerRiesgo(false);
				cumuloPoliza.setDetalleCoberturas(listaDetalleCoberturaLuc);
				cumuloPoliza.setFechaInicioVigencia(estructuraCotizacion.getCotizacionDTO().getFechaInicioVigencia());
				cumuloPoliza.setIdMoneda(estructuraCotizacion.getCotizacionDTO().getIdMoneda());
				cumuloPoliza.setIdToPersonaAsegurado(estructuraCotizacion.getCotizacionDTO().getIdToPersonaAsegurado());
				cumuloPoliza.setPorcentajePleno(validacionReaseguroPorCatalogo.getPorcentajePleno());
				listaCumulos.add(cumuloPoliza);
				if (!validacionReaseguroPorCatalogo.getPermitirSolicitarFacultativo() && !validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().isEmpty()){
					if (!mostrarMensajeErrorIncisos){
						mensajeError.append("Los siguientes incisos requieren facultativo, por lo que el c�mulo completo requerir� Soporte Facultativo<br/>");
						mostrarMensajeErrorIncisos = !mostrarMensajeErrorIncisos;
					}
					mensajeError.append("<hr>C�mulo por primer riesgo,<br/>SubRamo: "+
							listaCoberturasLUC.get(0).getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO().getDescripcionSubRamo()+"<br/>Incisos:");
					for(int i=0;i < validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().size();i++)
						mensajeError.append(" "+validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().get(i).getId().getNumeroInciso()+", ");
					mensajeError = new StringBuilder(mensajeError.substring(0, mensajeError.length()-1));
				}
			}
			else{
				registrarSubRamoInvalido(listaCoberturasLUC.get(0).getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO());
			}
			
		}
	
	}
	
	private void procesarCumuloPrimerRiesgo(AgrupacionCotDTO agrupacionCot) throws SystemException{
		Iterator<Entry<BigDecimal, List<CoberturaCotizacionDTO>>> it = subRamosPrimerRiesgo.entrySet().iterator();
		while (it.hasNext()){
			Entry<BigDecimal, List<CoberturaCotizacionDTO>> subRamoLista = (Entry<BigDecimal, List<CoberturaCotizacionDTO>>)it.next();
			SubRamoDTO subRamoDTO = subRamofacade.findById(subRamoLista.getKey());
			ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroPorCatalogo = 
				validarReaseguroFacultativoPorCatalogo(estructuraCotizacion,ConstantesReaseguro.TIPO_CUMULO_POLIZA, 
							subRamoDTO, null, null, null);
			DetalleCobertura  detalleCobertura;
			List<DetalleCobertura>listaDetalleCobertura= new ArrayList<DetalleCobertura>();
			int numeroIncisoCumulo = 0;
			int j = 0;
			for (CoberturaCotizacionDTO coberturaCot : subRamoLista.getValue()) {
				detalleCobertura = new DetalleCobertura();
				detalleCobertura.setNumeroInciso(Integer.valueOf(coberturaCot.getId().getNumeroInciso().toBigInteger().toString()));
				detalleCobertura.setIdSeccion(coberturaCot.getId().getIdToSeccion());
				detalleCobertura.setIdCobertura(coberturaCot.getId().getIdToCobertura());
				detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
				detalleCobertura.setMontoPrimaSuscripcion(coberturaCot.getValorPrimaNeta()*factor.doubleValue());
				detalleCobertura.setMonedaPrima(Integer.valueOf(estructuraCotizacion.getCotizacionDTO().getIdMoneda().toBigInteger().toString()));
				detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
				detalleCobertura.setEsPrimerRiesgo(true);
				detalleCobertura.setNumeroSubinciso(0);
				listaDetalleCobertura.add(detalleCobertura);
				//12/01/2010 Calculo del numeroInciso del c�mulo
				if(j==0){
					numeroIncisoCumulo = coberturaCot.getId().getNumeroInciso().intValue();
					j=j+1;
				}
				else{
					if(numeroIncisoCumulo != coberturaCot.getId().getNumeroInciso().intValue()){
						numeroIncisoCumulo = 0;
					}
					j=j+1;
				}
			}
			// Obtener el inciso al que pertenece la cobertura
			LineaDTO lineaTMP = soporteReaseguroCotizacion.obtenerLineaPorSubRamo(estructuraCotizacion.getCotizacionDTO().getFechaInicioVigencia(), subRamoLista.getKey());
			CumuloPoliza cumuloPoliza = new CumuloPoliza();
			cumuloPoliza.setIdCotizacion(estructuraCotizacion.getCotizacionDTO().getIdToCotizacion());
			cumuloPoliza.setIdSubRamo(subRamoLista.getKey());
			cumuloPoliza.setSumaAsegurada(agrupacionCot.getValorSumaAsegurada());
			cumuloPoliza.setTipoDistribucion(2);
			cumuloPoliza.setLineaDTO(lineaTMP);
			cumuloPoliza.setAplicaPrimerRiesgo(true);
			cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
			cumuloPoliza.setFechaInicioVigencia(estructuraCotizacion.getCotizacionDTO().getFechaInicioVigencia());
			cumuloPoliza.setIdMoneda(estructuraCotizacion.getCotizacionDTO().getIdMoneda());
			cumuloPoliza.setIdToPersonaAsegurado(estructuraCotizacion.getCotizacionDTO().getIdToPersonaAsegurado());
			cumuloPoliza.setPorcentajePleno(validacionReaseguroPorCatalogo.getPorcentajePleno());
			cumuloPoliza.setNumeroInciso(numeroIncisoCumulo);
			cumuloPoliza.setNumeroSubInciso(0);
			listaCumulos.add(cumuloPoliza);
			if (!validacionReaseguroPorCatalogo.getPermitirSolicitarFacultativo() && !validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().isEmpty()){
				if (!mostrarMensajeErrorIncisos){
					mensajeError.append("Los siguientes incisos requieren facultativo, por lo que el c�mulo completo requerir� Soporte Facultativo<br/>");
					mostrarMensajeErrorIncisos = !mostrarMensajeErrorIncisos;
				}
				mensajeError.append("<hr>C�mulo por primer riesgo, <br/>SubRamo: "+subRamoDTO.getDescripcionSubRamo()+"<br/>Incisos: ");
				for(int i=0;i < validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().size();i++)
					mensajeError.append(" "+validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().get(i).getId().getNumeroInciso()+", ");
				mensajeError = new StringBuilder(mensajeError.substring(0, mensajeError.length()-2));
			}
		}
	}
	
	/**
	 * Method validarReaseguroFacultativoPorCatalogoCumuloPoliza()
	 * @param estructuraCotizacion
	 * @param subRamoDTO
	 * @param idToCotizacion
	 * @return ValidacionReaseguroFacultativoPorCatalogo
	 * @throws SystemException
	 */
	private ValidacionReaseguroFacultativoPorCatalogo validarReaseguroFacultativoPorCatalogoCumuloPoliza(SoporteEstructuraCotizacion estructuraCotizacion, SubRamoDTO subRamoDTO, 
		BigDecimal idToCotizacion) throws SystemException {
		boolean requiereFacultativo = false;
		boolean permitirSolicitarFacultativo = true;
		double porcentajePleno = 100;
		
		short CLAVE_INSPECCION_EXCLUIDO = (short) 9;
		List<IncisoCotizacionDTO> incisosRequierenFacultativo = new ArrayList<IncisoCotizacionDTO>();
			// Obtener incisos en donde las coberturas esten contratadas
		List<IncisoCotizacionDTO> incisos =  estructuraCotizacion.obtenerIncisosPorSubRamo(subRamoDTO.getIdTcSubRamo());
		if (incisos == null)	incisos = new ArrayList<IncisoCotizacionDTO>();
		for (IncisoCotizacionDTO inciso : incisos) {
			try{
				Object subTipoDelSubRamo = datoIncisoCotizacionFacade.obtenerDTOSubTipoCorrespondienteAlSubRamo(subRamoDTO, idToCotizacion, inciso.getId().getNumeroInciso());
				if(subTipoDelSubRamo instanceof SubGiroDTO){
					SubGiroDTO subGiroDTO = (SubGiroDTO)subTipoDelSubRamo;
					PlenoReaseguroDTO plenoReaseguro = cotizacionFacade.obtenerPlenoReaseguro(subGiroDTO);
					Double porcentajePlenoTemp = plenoReaseguro.getPorcentajeMaximo();
					if (porcentajePlenoTemp < porcentajePleno)
						porcentajePleno = porcentajePlenoTemp;
					if (subGiroDTO.getClaveInspeccion() == CLAVE_INSPECCION_EXCLUIDO) {
						incisosRequierenFacultativo.add(inciso);
						porcentajePleno = 0d;
						requiereFacultativo = true;
					}
				} else if(subTipoDelSubRamo instanceof SubTipoGenerico){
//					java.lang.reflect.Method getClaveAutorizacion = subTipoDelSubRamo.getClass().getMethod("getClaveAutorizacion");
					Short claveAutorizacion = ((SubTipoGenerico)subTipoDelSubRamo).getClaveAutorizacion();
					if(claveAutorizacion.shortValue() == AREA_REASEGURO ){
						incisosRequierenFacultativo.add(inciso);
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		// Si s�lo algunos incisos requieren facultativo por cat�logo, no se permitir� solicitar facultativo
		if (requiereFacultativo == true && incisosRequierenFacultativo.size() != incisos.size()) {
			permitirSolicitarFacultativo = false;
		}
		ValidacionReaseguroFacultativoPorCatalogo validacionPorCatalogo = new ValidacionReaseguroFacultativoPorCatalogo();
		validacionPorCatalogo.setTipoDistribucion(ConstantesReaseguro.TIPO_CUMULO_POLIZA);
		validacionPorCatalogo.setIdTcSubRamo(subRamoDTO.getIdTcSubRamo());
		validacionPorCatalogo.setIdToCotizacion(idToCotizacion);
		validacionPorCatalogo.setRequiereFacultativo(requiereFacultativo);
		validacionPorCatalogo.setPorcentajePleno(porcentajePleno);
		validacionPorCatalogo.setIncisosRequierenFacultativo(incisosRequierenFacultativo);
		validacionPorCatalogo.setPermitirSolicitarFacultativo(permitirSolicitarFacultativo);
		return validacionPorCatalogo;
	}
	
	/**
	 * Method validarReaseguroFacultativoPorCatalogo().
	 * @param estructuraCotizacion
	 * @param tipoCumulo
	 * @param subRamoDTO
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param numeroSubInciso
	 * @return ValidacionReaseguroFacultativoPorCatalogo
	 * @throws SystemException
	 */
	private ValidacionReaseguroFacultativoPorCatalogo validarReaseguroFacultativoPorCatalogo(SoporteEstructuraCotizacion estructuraCotizacion, int tipoCumulo, SubRamoDTO subRamoDTO, 
		BigDecimal numeroInciso, BigDecimal idToSeccion,BigDecimal numeroSubInciso) throws SystemException {
		boolean requiereFacultativo = false;
		boolean permitirSolicitarFacultativo = true;
		double porcentajePleno = 100;
		short CLAVE_INSPECCION_EXCLUIDO = (short) 9;
		BigDecimal idToCotizacion = estructuraCotizacion.getIdToCotizacion();
		BigDecimal INCENDIO = SubRamoDTO.SUBRAMO_INCENDIO;
		BigDecimal OBRA_CIVIL_EN_CONSTRUCCION = SubRamoDTO.SUBRAMO_OBRA_CIVIL_EN_CONSTRUCCION;
		BigDecimal EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA = SubRamoDTO.SUBRAMO_EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA;
		BigDecimal EQUIPO_ELECTRONICO = SubRamoDTO.SUBRAMO_EQUIPO_ELECTRONICO;
		BigDecimal MONTAJE_MAQUINA = SubRamoDTO.SUBRAMO_MONTAJE_MAQUINA;
		BigDecimal ROTURA_MAQUINARIA = SubRamoDTO.SUBRAMO_ROTURA_MAQUINARIA;
		BigDecimal RESPONSABILIDAD_CIVIL_GENERAL = SubRamoDTO.SUBRAMO_RESPONSABILIDAD_CIVIL_GENERAL;
		List<IncisoCotizacionDTO> incisosRequierenFacultativo = new ArrayList<IncisoCotizacionDTO>();
		
		// Si el cumulo es por poliza
		if (tipoCumulo == ConstantesReaseguro.TIPO_CUMULO_POLIZA) {
		
			// Obtener incisos en donde las coberturas esten contratadas
			List<IncisoCotizacionDTO> incisos = estructuraCotizacion.obtenerIncisosPorSubRamo(subRamoDTO.getIdTcSubRamo());
			
			for (IncisoCotizacionDTO inciso : incisos) {
				// Si el subramo es incendio
				if (subRamoDTO.getIdTcSubRamo().compareTo(INCENDIO) == 0) {
					SubGiroDTO subGiroDTO;
					try {
						subGiroDTO = estructuraCotizacion.getAdministradorDatosInciso().obtenerSubGiroDTO(
								inciso.getId().getNumeroInciso(), subRamoDTO.getRamoDTO().getIdTcRamo());
						if (subGiroDTO != null) {
							PlenoReaseguroDTO plenoReaseguro = estructuraCotizacion.getAdministradorDatosInciso().obtenerPlenoReaseguro(
									inciso.getId().getNumeroInciso(), subRamoDTO.getRamoDTO().getIdTcRamo());
							
							Double porcentajePlenoTemp = plenoReaseguro.getPorcentajeMaximo();
							if (porcentajePlenoTemp < porcentajePleno)
								porcentajePleno = porcentajePlenoTemp;
	
							if (subGiroDTO.getClaveInspeccion() == CLAVE_INSPECCION_EXCLUIDO) {
								incisosRequierenFacultativo.add(inciso);
								porcentajePleno = 0d;
								requiereFacultativo = true;
							}
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}// Fin subramo incendio
				// si el subramo corresponde a RESPONSABILIDAD_CIVIL_GENERAL
				else if (subRamoDTO.getIdTcSubRamo().compareTo(RESPONSABILIDAD_CIVIL_GENERAL) == 0) {
					SubGiroRCDTO subGiroRC;
					try {
						subGiroRC = estructuraCotizacion.getAdministradorDatosInciso().obtenerSubGiroRCDTO(
								inciso.getId().getNumeroInciso(), subRamoDTO.getRamoDTO().getIdTcRamo());
//							datoIncisoCotDN.obtenerSubGiroRC(idToCotizacion, inciso.getId().getNumeroInciso(), subRamoDTO.getRamoDTO().getIdTcRamo());
						if (subGiroRC != null && subGiroRC.getClaveAutorizacion() == AREA_REASEGURO) {
							incisosRequierenFacultativo.add(inciso);
							requiereFacultativo = true;
							porcentajePleno = 0d;
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}// Fin subramo Responsabilidad Civil General.
				// si el subramo corresponde a ROTURA_MAQUINARIA
				else if (subRamoDTO.getIdTcSubRamo().compareTo(ROTURA_MAQUINARIA) == 0) {
					SubTipoMaquinariaDTO subTipoMaquinariaDTO;
					try {
						//En teoria, esta condici�n no se cumplir� debido a que este caso es a nivel subInciso, en el cual se reciben los datos de idToSeccion y numeroSubInciso
						if (idToSeccion == null && numeroSubInciso == null)
							subTipoMaquinariaDTO = datoIncisoCotizacionFacade.obtenerSubTipoMaquinaria(idToCotizacion,inciso.getId().getNumeroInciso(),subRamoDTO.getRamoDTO().getIdTcRamo());
						else
							subTipoMaquinariaDTO = datoIncisoCotizacionFacade.obtenerSubTipoMaquinaria(idToCotizacion,inciso.getId().getNumeroInciso(),subRamoDTO.getRamoDTO().getIdTcRamo(),
									numeroSubInciso,idToSeccion);
						if (subTipoMaquinariaDTO != null && subTipoMaquinariaDTO.getClaveAutorizacion() == AREA_REASEGURO) {
							incisosRequierenFacultativo.add(inciso);
							requiereFacultativo = true;
							porcentajePleno = 0d;
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}// Fin subramo Rotura Maquinaria.
				// si el subramo corresponde a MONTAJE_MAQUINA
				else if (subRamoDTO.getIdTcSubRamo().compareTo(MONTAJE_MAQUINA) == 0) {
					SubtipoMontajeMaquinaDTO subTipoMontajeMaquinaDTO;
					try {
						subTipoMontajeMaquinaDTO = datoIncisoCotizacionFacade.obtenerSubTipoMontajeMaquina(idToCotizacion,inciso.getId().getNumeroInciso(),
								subRamoDTO.getRamoDTO().getIdTcRamo());
						if (subTipoMontajeMaquinaDTO != null && subTipoMontajeMaquinaDTO.getClaveAutorizacion() == AREA_REASEGURO) {
							incisosRequierenFacultativo.add(inciso);
							requiereFacultativo = true;
							porcentajePleno = 0d;
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}// Fin subramo Montaje Maquina.
				// si el subramo corresponde a EQUIPO_ELECTRONICO
				else if (subRamoDTO.getIdTcSubRamo().compareTo(EQUIPO_ELECTRONICO) == 0) {
					SubtipoEquipoElectronicoDTO subTipoEquipoElectronicoDTO;
					try {
						//En teoria, esta condici�n no se cumplir� debido a que este caso es a nivel subInciso, en el cual se reciben los datos de idToSeccion y numeroSubInciso
						if (idToSeccion == null && numeroSubInciso == null)
							subTipoEquipoElectronicoDTO = datoIncisoCotizacionFacade.obtenerSubtipoEquipoElectronico(idToCotizacion, inciso.getId().getNumeroInciso(),
									subRamoDTO.getRamoDTO().getIdTcRamo());
						else
							subTipoEquipoElectronicoDTO = datoIncisoCotizacionFacade.obtenerSubtipoEquipoElectronico(idToCotizacion, inciso.getId().getNumeroInciso(),
									subRamoDTO.getRamoDTO().getIdTcRamo(),numeroSubInciso,idToSeccion);
						if (subTipoEquipoElectronicoDTO != null && subTipoEquipoElectronicoDTO.getClaveAutorizacion() == AREA_REASEGURO) {
							incisosRequierenFacultativo.add(inciso);
							requiereFacultativo = true;
							porcentajePleno = 0d;
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}// Fin subtipo equipo electronico.
				// si el subramo corresponde a
				// EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA
				else if (subRamoDTO.getIdTcSubRamo().compareTo(EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA) == 0) {
					SubtipoEquipoContratistaDTO subTipoEquipoContratistaDTO;
					try {
						//En teoria, esta condici�n no se cumplir� debido a que este caso es a nivel subInciso, en el cual se reciben los datos de idToSeccion y numeroSubInciso
						if (idToSeccion == null && numeroSubInciso == null)
							subTipoEquipoContratistaDTO = datoIncisoCotizacionFacade.obtenerSubtipoEquipoContratista(idToCotizacion, inciso.getId().getNumeroInciso(),
									subRamoDTO.getRamoDTO().getIdTcRamo());
						else
							subTipoEquipoContratistaDTO = datoIncisoCotizacionFacade.obtenerSubtipoEquipoContratista(idToCotizacion, inciso.getId().getNumeroInciso(),
									subRamoDTO.getRamoDTO().getIdTcRamo(),numeroSubInciso,idToSeccion);
						if (subTipoEquipoContratistaDTO != null && subTipoEquipoContratistaDTO.getClaveAutorizacion() == AREA_REASEGURO) {
							incisosRequierenFacultativo.add(inciso);
							requiereFacultativo = true;
							porcentajePleno = 0d;
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}// Fin subtipo equipo contratista.
				// si el subramo corresponde a OBRA_CIVIL_EN_CONSTRUCCION
				else if (subRamoDTO.getIdTcSubRamo().compareTo(OBRA_CIVIL_EN_CONSTRUCCION) == 0) {
					TipoObraCivilDTO tipoObraCivilDTO;
					try {
						tipoObraCivilDTO = datoIncisoCotizacionFacade.obtenerTipoObraCivil(idToCotizacion, inciso.getId().getNumeroInciso(),
								subRamoDTO.getRamoDTO().getIdTcRamo());
						if (tipoObraCivilDTO != null && tipoObraCivilDTO.getClaveAutorizacion() == AREA_REASEGURO) {
							incisosRequierenFacultativo.add(inciso);
							requiereFacultativo = true;
							porcentajePleno = 0d;
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}else if(subRamoDTO.getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_TERREMOTO) || subRamoDTO.getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_HIDRO)){
				    	Double porcentajePlenoTemp = getPorcentajePlenoIncendio(estructuraCotizacion, inciso.getId().getNumeroInciso());
					if (porcentajePlenoTemp < porcentajePleno){
					    porcentajePleno = porcentajePlenoTemp;
					}
				}
			}
			// Si s�lo algunos incisos requieren facultativo por cat�logo, no se permitir� solicitar facultativo
			if (requiereFacultativo == true && incisosRequierenFacultativo.size() != incisos.size()) {
				permitirSolicitarFacultativo = false;
			}
		}// fin tipo cumulo por poliza
		// si el tipoCumulo es por inciso o por subinciso
		else if (tipoCumulo ==ConstantesReaseguro.TIPO_CUMULO_INCISO || tipoCumulo ==ConstantesReaseguro.TIPO_CUMULO_SUBINCISO) {
			// Si el subramo es incendio
			if (subRamoDTO.getIdTcSubRamo().compareTo(INCENDIO) == 0) {
				SubGiroDTO subGiroDTO;
				try {
					subGiroDTO = estructuraCotizacion.getAdministradorDatosInciso().obtenerSubGiroDTO(
							numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo());
					if (subGiroDTO != null) {
						PlenoReaseguroDTO plenoReaseguro = estructuraCotizacion.getAdministradorDatosInciso().obtenerPlenoReaseguro(
								numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo());
						if(plenoReaseguro!=null)
							porcentajePleno = plenoReaseguro.getPorcentajeMaximo();
						if (subGiroDTO.getClaveInspeccion() == CLAVE_INSPECCION_EXCLUIDO) {
							porcentajePleno = 0d;
							requiereFacultativo = true;
						}
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
			// si el subramo corresponde a RESPONSABILIDAD_CIVIL_GENERAL
			else if (subRamoDTO.getIdTcSubRamo().compareTo(RESPONSABILIDAD_CIVIL_GENERAL) == 0) {
				SubGiroRCDTO subGiroRC;
				try {
					subGiroRC = datoIncisoCotizacionFacade.obtenerSubGiroRC(idToCotizacion, numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo());
					if (subGiroRC != null && subGiroRC.getClaveAutorizacion() == AREA_REASEGURO) {
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}// Fin subramo Responsabilidad Civil General.
			// si el subramo corresponde a ROTURA_MAQUINARIA
			else if (subRamoDTO.getIdTcSubRamo().compareTo(ROTURA_MAQUINARIA) == 0) {
				SubTipoMaquinariaDTO subTipoMaquinariaDTO;
				try {
					//En teoria, esta condici�n no se cumplir� debido a que este caso es a nivel subInciso, en el cual se reciben los datos de idToSeccion y numeroSubInciso
					if (idToSeccion == null && numeroSubInciso == null)
						subTipoMaquinariaDTO = datoIncisoCotizacionFacade.obtenerSubTipoMaquinaria(idToCotizacion,numeroInciso,subRamoDTO.getRamoDTO().getIdTcRamo());
					else
						subTipoMaquinariaDTO = datoIncisoCotizacionFacade.obtenerSubTipoMaquinaria(idToCotizacion,numeroInciso,subRamoDTO.getRamoDTO().getIdTcRamo(),
								numeroSubInciso,idToSeccion);
					if (subTipoMaquinariaDTO != null && subTipoMaquinariaDTO.getClaveAutorizacion() == AREA_REASEGURO) {
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}// Fin subramo Rotura Maquinaria.
			// si el subramo corresponde a MONTAJE_MAQUINA
			else if (subRamoDTO.getIdTcSubRamo().compareTo(MONTAJE_MAQUINA) == 0) {
				SubtipoMontajeMaquinaDTO subTipoMontajeMaquinaDTO;
				try {
					subTipoMontajeMaquinaDTO = datoIncisoCotizacionFacade.obtenerSubTipoMontajeMaquina(idToCotizacion,numeroInciso,
							subRamoDTO.getRamoDTO().getIdTcRamo());
					if (subTipoMontajeMaquinaDTO != null && subTipoMontajeMaquinaDTO.getClaveAutorizacion() == AREA_REASEGURO) {
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}// Fin subramo Montaje Maquina.
			// si el subramo corresponde a EQUIPO_ELECTRONICO
			else if (subRamoDTO.getIdTcSubRamo().compareTo(EQUIPO_ELECTRONICO) == 0) {
				SubtipoEquipoElectronicoDTO subTipoEquipoElectronicoDTO;
				try {
					//En teoria, esta condici�n no se cumplir� debido a que este caso es a nivel subInciso, en el cual se reciben los datos de idToSeccion y numeroSubInciso
					if (idToSeccion == null && numeroSubInciso == null)
						subTipoEquipoElectronicoDTO = datoIncisoCotizacionFacade.obtenerSubtipoEquipoElectronico(idToCotizacion,numeroInciso,
								subRamoDTO.getRamoDTO().getIdTcRamo());
					else
						subTipoEquipoElectronicoDTO = datoIncisoCotizacionFacade.obtenerSubtipoEquipoElectronico(idToCotizacion,numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo(),
								numeroSubInciso,idToSeccion);
					if (subTipoEquipoElectronicoDTO != null && subTipoEquipoElectronicoDTO.getClaveAutorizacion() == AREA_REASEGURO) {
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}// Fin subtipo equipo electronico.
			// si el subramo corresponde a
			// EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA
			else if (subRamoDTO.getIdTcSubRamo().compareTo(EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA) == 0) {
				SubtipoEquipoContratistaDTO subTipoEquipoContratistaDTO;
				try {
					//En teoria, esta condici�n no se cumplir� debido a que este caso es a nivel subInciso, en el cual se reciben los datos de idToSeccion y numeroSubInciso
					if (idToSeccion == null && numeroSubInciso == null)
						subTipoEquipoContratistaDTO = datoIncisoCotizacionFacade.obtenerSubtipoEquipoContratista(idToCotizacion, numeroInciso,
								subRamoDTO.getRamoDTO().getIdTcRamo());
					else
						subTipoEquipoContratistaDTO = datoIncisoCotizacionFacade.obtenerSubtipoEquipoContratista(idToCotizacion, numeroInciso,
								subRamoDTO.getRamoDTO().getIdTcRamo(),numeroSubInciso,idToSeccion);
					if (subTipoEquipoContratistaDTO != null && subTipoEquipoContratistaDTO.getClaveAutorizacion() == AREA_REASEGURO) {
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}// Fin subtipo equipo contratista.
			// si el subramo corresponde a OBRA_CIVIL_EN_CONSTRUCCION
			else if (subRamoDTO.getIdTcSubRamo().compareTo(OBRA_CIVIL_EN_CONSTRUCCION) == 0) {
				TipoObraCivilDTO tipoObraCivilDTO;
				try {
					tipoObraCivilDTO = datoIncisoCotizacionFacade.obtenerTipoObraCivil(idToCotizacion, numeroInciso,subRamoDTO.getRamoDTO().getIdTcRamo());
					if (tipoObraCivilDTO != null && tipoObraCivilDTO.getClaveAutorizacion() == AREA_REASEGURO) {
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}// Fin tipo obra civil.
			else if(subRamoDTO.getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_TERREMOTO) || subRamoDTO.getIdTcSubRamo().equals(SubRamoDTO.SUBRAMO_HIDRO)){
			    porcentajePleno= getPorcentajePlenoIncendio(estructuraCotizacion, numeroInciso);
			}
		}// fin tipocumulo por inciso
	
		ValidacionReaseguroFacultativoPorCatalogo validacionPorCatalogo = new ValidacionReaseguroFacultativoPorCatalogo();
		validacionPorCatalogo.setTipoDistribucion(new Integer(tipoCumulo));
		validacionPorCatalogo.setIdTcSubRamo(subRamoDTO.getIdTcSubRamo());
		validacionPorCatalogo.setIdToCotizacion(idToCotizacion);
		validacionPorCatalogo.setNumeroInciso(numeroInciso);
		validacionPorCatalogo.setIdToSeccion(idToSeccion);
		validacionPorCatalogo.setNumeroSubInciso(numeroSubInciso);
		validacionPorCatalogo.setRequiereFacultativo(requiereFacultativo);
		validacionPorCatalogo.setPorcentajePleno(porcentajePleno);
		validacionPorCatalogo.setIncisosRequierenFacultativo(incisosRequierenFacultativo);
		validacionPorCatalogo.setPermitirSolicitarFacultativo(permitirSolicitarFacultativo);
	
		return validacionPorCatalogo;
	}
	
	private double getPorcentajePlenoIncendio(SoporteEstructuraCotizacion estructuraCotizacion,BigDecimal numeroInciso) throws SystemException{
		double pleno = 100d;
		try{
	    	PlenoReaseguroDTO plenoReaseguro = estructuraCotizacion.getAdministradorDatosInciso().obtenerPlenoReaseguro(numeroInciso, SubRamoDTO.SUBRAMO_INCENDIO);
	    	if(plenoReaseguro!=null)
			    pleno = plenoReaseguro.getPorcentajeMaximo();
		}catch(Exception e){
			e.printStackTrace();
		}
		return pleno;
	}
	
	private void registrarSubRamoInvalido(SubRamoDTO subRamo){
		subRamosNoValidos.append(subRamo.getDescripcionSubRamo()).append("<br/>");
	    mostrarMensajeErrorSubRamosNoValidos = true;
	}
	
	public List<CumuloPoliza> getListaCumulos(){
		return listaCumulos;
	}
	
	public boolean getMostrarMensajeErrorIncisos(){
		return mostrarMensajeErrorIncisos;
	}
	
	public boolean getMostrarMensajeErrorSubRamosNoValidos(){
		return mostrarMensajeErrorSubRamosNoValidos;
	}
	
	public String getSubRamosNoValidos(){
		return subRamosNoValidos.toString();
	}
	
	public String getMensajeError(){
		return mensajeError.toString();
	}
}
