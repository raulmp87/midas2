package mx.com.afirme.midas.danios.reportes.cotizacion.empresarial;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;

import org.apache.commons.lang.NullArgumentException;

public class PL1SumasAseguradas1erRiesgoLUC extends PlantillaCotizacionBase{

	public PL1SumasAseguradas1erRiesgoLUC(BigDecimal idToCotizacion,Map <String,Object> mapaParametrosGenerales,ReporteCotizacionBase reporteBase) throws NullArgumentException{
		super(idToCotizacion,mapaParametrosGenerales,reporteBase);
		setListaRegistrosContenido( new ArrayList<Object>() );
		setNombrePlantilla( Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.primerRiesgo.objetos") );
		setPaquetePlantilla( Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.cotizacionEmpresarial.paquete") );
		setTipoReporte( ConstantesReporte.TIPO_PDF );
	}

	public byte[] obtenerReporte(String nombreUsuario) throws SystemException, javax.transaction.SystemException {
		procesarDatosPlantilla1erRiesgoLuc(nombreUsuario,true);
		return getByteArrayReport();
	}
	
}
