package mx.com.afirme.midas.danios.reportes.poliza.casa;

import java.math.BigDecimal;

import javax.transaction.SystemException;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.danios.reportes.poliza.ReportePolizaBase;
import mx.com.afirme.midas.poliza.PolizaDTO;

/**
 * @author jose luis arellano
 */
public class ReportePolizaCasa extends ReportePolizaBase{
	
//	public ReportePolizaCasa(BigDecimal idToPoliza) throws SystemException {
//		polizaDTO = polizaFacade.findById(idToPoliza);
//		cotizacionDTO = polizaDTO.getCotizacionDTO();
//		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
//		setListaPlantillas(new ArrayList<byte[]>());
//		setMapaParametrosGeneralesPlantillas(new java.util.HashMap<String, Object>());
//	}
	
	public ReportePolizaCasa(BigDecimal idToPoliza) throws SystemException {
		polizaDTO = polizaFacade.findById(idToPoliza);
		cotizacionDTO = polizaDTO.getCotizacionDTO();
//		cotizacionDTO = cotizacionFacade.findById(idToCotizacion);
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		
	}
	
	public ReportePolizaCasa(PolizaDTO polizaDTO) throws SystemException {
		this.polizaDTO = polizaDTO;
		cotizacionDTO = polizaDTO.getCotizacionDTO();
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
	}
	
	
	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException, mx.com.afirme.midas.sistema.SystemException {
		generarReporte(claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioEmision());
	}

	private void generarReporte(String nombreUsuario) throws SystemException,mx.com.afirme.midas.sistema.SystemException{
		super.poblarDatosContratante(nombreUsuario);
		super.poblarDatosBeneficiario(nombreUsuario);
		//Recuperar incisos, subincisos, coberturas, etc. de la cotizacion
		consultarInformacionCotizacion(nombreUsuario);
		
		poblarParametrosComunes(polizaDTO,nombreUsuario,listaIncisos,null);
		
		for(IncisoCotizacionDTO incisoCotEnCurso : super.getListaIncisos()){
			byte[] reporteTMP = null;
			
			PlantillaCotizacionBase plantillaCasa = new PlantillaPolizaCasa(cotizacionDTO, incisoCotEnCurso, getMapaParametrosGeneralesPlantillas(), this);
			
			try {
				plantillaCasa.setMostrarLeyendaSANivelCobertura(Boolean.FALSE);
				reporteTMP = plantillaCasa.obtenerReporte(nombreUsuario);
			} catch (mx.com.afirme.midas.sistema.SystemException e) {} 
			catch (SystemException e) {}
			if (reporteTMP !=null)
				getListaPlantillas().add(reporteTMP);
		}	
		
	}

}
