	package mx.com.afirme.midas.danios.reportes.reportercs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.danios.reportes.reportercs.log.SolvenciaLogDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsfMov;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless 
public class ReporteRCSFacade implements ReporteRCSFacadeRemote {
	
	@PersistenceContext 
	private EntityManager entityManager;
	public static final String CONSULTAREPORTESRCS = "MIDAS.PKG_SOLVENCIA_CNSF.spDAN_DetRepRCS";
	public static final String SP_RCSVIDALP = "SEYCOS.PKG_INFO_VIGOR_VIDA.spVida_RepRCSLP";
	
	private static final String FECHA_INICIO = "pFechaInicio";
	private static final String ID_RAMO = "pIdRamo";
	private static final String NUM_CORTES = "pNumCortes";
	private static final String MAPEO_SALIDA = "registro";
	private static final String MODULO = "MODULO";
	private static final String ANIO_CORTE = "pAnioCorte";
	private static final String PARAMETRO_MES = "pMes";
	private static final String PARAMETRO_DIA = "pdia";
	private static final String MENSAJE_LLAMADA_SP = "Consultando reporte bases emision. SP a invocar: ";
	private static final String MENSAJE_LLAMADA_SP_PARAMETROS = ", Parametros: ";
	private static final String MENSAJE_FIN_LLAMADA_SP = "Finaliza consulta reporte bases emision. SP invocado: ";
	private static final String MENSAJE_REGISTROS = "registros encontrados: ";
	private static final String MENSAJE_ERROR_SP = "Ocurrio un error al consultar reporte bases emision. SP invocado: ";
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReporteRCSDTO> obtieneReportesRCSGenerico(
			ReporteRCSDTO movimientoEmision, Double tipoCambio, String nombreSP) throws Exception {
		StoredProcedureHelper storedHelper = null;
		
		StringBuilder descripcionParametros = new StringBuilder();
		try {
			
			String[] nombreParametros = {FECHA_INICIO, 
					"pMes",
					"pdia", 
					"tipoCambio",
					ID_RAMO,
					NUM_CORTES};
			Object[] valorParametros={movimientoEmision.getFechaInicio().get(Calendar.YEAR),
				movimientoEmision.getFechaInicio().get(Calendar.MONTH)+1,
				movimientoEmision.getFechaInicio().get(Calendar.DAY_OF_MONTH),
				tipoCambio,
				movimientoEmision.getIdRamo(),
				movimientoEmision.getNumeroCortes()};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					MAPEO_SALIDA,
					MAPEO_SALIDA);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString()+
					MENSAJE_REGISTROS+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
						
			return listaResultado != null? listaResultado : new ArrayList<ReporteRCSDTO>();
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
						
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}
		
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReporteRCSDTO> obtieneDetalleRCSVidaCP(
			ReporteRCSDTO movimientoEmision) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder();
		try {
			
			String[] nombreParametros = {FECHA_INICIO, 
					ID_RAMO,
					NUM_CORTES};
			Object[] valorParametros={movimientoEmision.getFechaInicio().get(Calendar.YEAR),
				movimientoEmision.getIdRamo(),
				movimientoEmision.getNumeroCortes()};

			storedHelper = new StoredProcedureHelper(CONSULTAREPORTESRCS, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					MAPEO_SALIDA,								
					MAPEO_SALIDA);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+CONSULTAREPORTESRCS+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP+CONSULTAREPORTESRCS+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString()+
					MENSAJE_REGISTROS+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
						
			return listaResultado != null? listaResultado : new ArrayList<ReporteRCSDTO>();
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+CONSULTAREPORTESRCS, Level.SEVERE, e); 
			
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+CONSULTAREPORTESRCS, Level.SEVERE, e);
			throw e;
		}
	} 
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReporteRCSDTO> obtieneRCSVidaLP(ReporteRCSDTO movimientoEmision) throws Exception {
		StringBuilder descripcionParametros = new StringBuilder();
		
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(SP_RCSVIDALP, StoredProcedureHelper.DATASOURCE_MIDAS);
			String[] nombreParametros = {ANIO_CORTE, PARAMETRO_MES, PARAMETRO_DIA, ID_RAMO, NUM_CORTES};
			Object[] valorParametros={movimientoEmision.getFechaInicio().get(Calendar.YEAR), movimientoEmision.getFechaInicio().get(Calendar.MONTH) + 1, movimientoEmision.getFechaInicio().get(Calendar.DATE), movimientoEmision.getIdRamo(), movimientoEmision.getNumeroCortes()};
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", [" + nombreParametros[i] + "] : " + valorParametros[i]);
			}
			
			storedHelper.estableceMapeoResultados(ReporteRCSDTO.class.getCanonicalName(), MAPEO_SALIDA, MAPEO_SALIDA);
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP + CONSULTAREPORTESRCS + MENSAJE_LLAMADA_SP_PARAMETROS + descripcionParametros.toString(), Level.INFO, null);
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP + CONSULTAREPORTESRCS + MENSAJE_LLAMADA_SP_PARAMETROS + descripcionParametros.toString() + MENSAJE_REGISTROS + (listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
			return listaResultado != null? listaResultado : new ArrayList<ReporteRCSDTO>();
		} catch (SQLException e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+CONSULTAREPORTESRCS, Level.SEVERE, e); 
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+CONSULTAREPORTESRCS, Level.SEVERE, e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReporteRCSDTO> obtieneDetalleRCSVidaLP(
			ReporteRCSDTO movimientoEmision) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder();
		
		try {
			
			String[] nombreParametros = {FECHA_INICIO, 
					ID_RAMO,
					NUM_CORTES};
			Object[] valorParametros={movimientoEmision.getFechaInicio().get(Calendar.YEAR),
				movimientoEmision.getIdRamo(),
				movimientoEmision.getNumeroCortes()};

			storedHelper = new StoredProcedureHelper(CONSULTAREPORTESRCS, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					MAPEO_SALIDA,
					MAPEO_SALIDA);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+CONSULTAREPORTESRCS+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP+CONSULTAREPORTESRCS+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString()+
					MENSAJE_REGISTROS+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
						
			return listaResultado != null? listaResultado : new ArrayList<ReporteRCSDTO>();
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+CONSULTAREPORTESRCS, Level.SEVERE, e); 
			
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+CONSULTAREPORTESRCS, Level.SEVERE, e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String obtieneNomArchivo(String nombreArch) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder();
		String nomenclatura = "";
		String nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.sp_obtenerNomenclatura";
		try {
			
			String[] nombreParametros = {"ptipoArchivo", 
					"pvalor",
					MODULO};
			Object[] valorParametros={nombreArch,
					nombreArch,
					nombreArch};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					MAPEO_SALIDA,
					MAPEO_SALIDA);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			if(listaResultado != null && listaResultado.size() == 1)
				nomenclatura = listaResultado.get(0).getRegistro();
			
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString()+
					MENSAJE_REGISTROS+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
						
			return nomenclatura;
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
			
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}
	  
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String obtenerNumRangosDuracion(int tipoRango) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder();
		String vTipoRango = "0";
		String nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.spObtenerNumRangosDuracion";
		try {			
			String[] nombreParametros = {"tipoRango", 
					"pvalor",
					MODULO};
			Object[] valorParametros={tipoRango,
					"",
					""};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					MAPEO_SALIDA,
					MAPEO_SALIDA);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			if(listaResultado != null && listaResultado.size() == 1)
				vTipoRango = listaResultado.get(0).getRegistro();
			
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString()+
					MENSAJE_REGISTROS+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
						
			return vTipoRango;
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
			
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean actualizaRangos(
			ReporteRCSDTO movimientoEmision) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder();
		String nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.spActualizaRangos";
		try {
			
			String[] nombreParametros = {ANIO_CORTE,"pMesCorte", 
					"pCveNegocio",
					"pMesCorte2"};
			Object[] valorParametros={movimientoEmision.getFechaInicio().get(Calendar.YEAR),
					movimientoEmision.getFechaInicio().get(Calendar.MONTH)+1
					,movimientoEmision.getCveNegocio(),
				movimientoEmision.getNumeroCortes()};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS + descripcionParametros.toString(), Level.INFO, null);
			
			int resp = storedHelper.ejecutaActualizar();
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+", respuesta: " + resp, Level.INFO, null);
						
			return true;
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
			
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean eliminaRangos(
			ReporteRCSDTO movimientoEmision) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder();
		String nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.spEliminaRangos";
		try {
			
			String[] nombreParametros = {"pRamo", 
					"pMesCorte2"};
			Object[] valorParametros={
					movimientoEmision.getIdRamo(),
				movimientoEmision.getNumeroCortes()};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS + descripcionParametros.toString(), Level.INFO, null);
			
			int resp = storedHelper.ejecutaActualizar();
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+", respuesta: " + resp, Level.INFO, null);
						
			return true;
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
			
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean setRangosDuracionSP(
			Integer duracionPromedio, Integer duracionRemanente) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder();
		String nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.spRangosDuracion";
		try {
			
			String[] nombreParametros = {"pduracionPromedio", 
					"pduracionRemanente"};
			Object[] valorParametros={
					duracionPromedio,
					duracionRemanente};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS + descripcionParametros.toString(), Level.INFO, null);
			
			int resp = storedHelper.ejecutaActualizar();
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+", respuesta: " + resp, Level.INFO, null);
						
			return true;
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
			
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean actualizaCalificaciones(int dia,
			ReporteRCSDTO movimientoEmision, int anio) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder();
		String nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.spActualizaCalificaciones";
		try {
			
			String[] nombreParametros = {ANIO_CORTE,"pMesCorte","pMesDia"};
			Object[] valorParametros={anio,
					movimientoEmision.getNumeroCortes()
					,dia};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS + descripcionParametros.toString(), Level.INFO, null);
			
			int resp = storedHelper.ejecutaActualizar();
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+", respuesta: " + resp, Level.INFO, null);
						
			return true;
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
			
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}

	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReporteRCSDTO> obtieneREASRCS(
			ReporteRCSDTO movimientoEmision, int tipoReporte, Double tipoCambio) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder();
		String nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.spDAN_RepREASRCS";
		
		if(tipoReporte == 2)
		{
			nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.spDAN_RepDetREASRCS";
		}
		else if(tipoReporte == 3)
		{
			nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.spDAN_RepValidaRR6";
		}
		try {
			
			String[] nombreParametros = {FECHA_INICIO, 
					"pMes",
					"pdia", 
					ID_RAMO,
					NUM_CORTES};
			Object[] valorParametros={movimientoEmision.getFechaInicio().get(Calendar.YEAR),
				movimientoEmision.getFechaInicio().get(Calendar.MONTH)+1,
				movimientoEmision.getFechaInicio().get(Calendar.DAY_OF_MONTH),
				tipoCambio,
				movimientoEmision.getNumeroCortes()};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					MAPEO_SALIDA,
					MAPEO_SALIDA);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString()+
					MENSAJE_REGISTROS+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
						
			return listaResultado != null? listaResultado : new ArrayList<ReporteRCSDTO>();
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
			
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED) 
	public List<ReporteRCSDTO> obtieneValidacionesRCS(
			ReporteRCSDTO movimientoEmision) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder();
		String nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.spValidaciones_EIQ";
		
		try {
			
			String[] nombreParametros = {FECHA_INICIO, 
					"pCveNegocio",
					NUM_CORTES};
			Object[] valorParametros={movimientoEmision.getFechaInicio().getTime(),
				movimientoEmision.getCveNegocio(),
				movimientoEmision.getNumeroCortes()};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					MAPEO_SALIDA,
					MAPEO_SALIDA);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString()+
					MENSAJE_REGISTROS+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
						
			return listaResultado != null? listaResultado : new ArrayList<ReporteRCSDTO>();
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
				
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReporteRCSDTO> obtieneClaveRamo(
			String idRamo, String modulo) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder(); 
		String nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.sp_claveRamo";
		
		try {
			
			String[] nombreParametros = {ID_RAMO, 
					"MODULO",
					MODULO};
			Object[] valorParametros={idRamo,
					modulo,
					modulo};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					MAPEO_SALIDA,
					MAPEO_SALIDA);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString()+
					MENSAJE_REGISTROS+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
						
			return listaResultado != null? listaResultado : new ArrayList<ReporteRCSDTO>();
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
			
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int sp_actualizaNom(
			String tipoArchivo, String valor) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder(); 
		String nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.sp_actualizaNom";
		
		try {
			
			String[] nombreParametros = {"ptipoArchivo", 
					"pvalor",
					MODULO};
			Object[] valorParametros={tipoArchivo,
					valor,
					valor};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
					
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
			
			int resp = storedHelper.ejecutaActualizar();
			
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
						
			return resp;
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
				
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int generaInfoRCS(
			String anio, String mes, String day, String reproceso, String negocio) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder(); 
		/**String nombreSP = "PKG_SOLVENCIA_CNSF.SP_GENERARDATOSSOLVCNSF";*/
		String nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.SP_PROGRAMAR_TAREA";
		
		try {
			
			String[] nombreParametros = {"anio", 
					"mes",
					"dia",
					"reproceso",
					"negocio"};
			Object[] valorParametros={anio,
					mes,
					day,
					reproceso,
					negocio};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
					
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
			
			storedHelper.ejecutaActualizar();
			
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
						
			return storedHelper.getCodigoRespuesta() != null? storedHelper.getCodigoRespuesta() : 0;
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
				
			return storedHelper.getCodigoRespuesta() != null? storedHelper.getCodigoRespuesta() : 0;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			return storedHelper.getCodigoRespuesta() != null? storedHelper.getCodigoRespuesta() : 0;
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int generaInfoRCSVida(
			String anio, String mes, String day, String reproceso) throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder(); 
		String nombreSP = "SEYCOS.PKG_INFO_VIGOR_VIDA.SP_GENERARDATOSSOLVCNSF";
		
		try {
			
			String[] nombreParametros = {"anio", 
					"mes",
					"dia",
					"reproceso"};
			Object[] valorParametros={anio,
					mes,
					day,
					reproceso};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
					
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
			
			storedHelper.ejecutaActualizar();
			
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
						
			return storedHelper.getCodigoRespuesta() != null? storedHelper.getCodigoRespuesta() : 0;
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
				
			return storedHelper.getCodigoRespuesta() != null? storedHelper.getCodigoRespuesta() : 0;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			return storedHelper.getCodigoRespuesta() != null? storedHelper.getCodigoRespuesta() : 0;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SolvenciaLogDTO> findByFechaCorte(Date fechaCorte, List<String> ramos){
		LogDeMidasEJB3.log(
				"finding SolvenciaLog instance with property: corte "
						 + ", value: " , Level.INFO, null);
		
	      Calendar calendar = Calendar.getInstance();
	            calendar.setTime(fechaCorte); 
	            calendar.add(Calendar.DAY_OF_YEAR, 1);  
		try {
			final String queryString = "select model from SolvenciaLogDTO model" +
					" where model.ramo "
					 + "in :propertyValue " +
					 		"and model.corte >= :corte " +
					 		"and model.corte <= :corte2";
			Query query = entityManager.createQuery(queryString);
			
			query.setParameter("propertyValue", ramos);
			query.setParameter("corte",fechaCorte);
			query.setParameter("corte2",calendar.getTime());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
		
	}
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReporteRCSDTO> obtieneRCSVidaPas(ReporteRCSDTO movimientoEmision) 
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder();
		String nombreSP = "SEYCOS.PKG_INFO_VIGOR_VIDA.spVida_RepRCSLPPASIVO";
		
		try {
			
			String[] nombreParametros = {ANIO_CORTE, };
			Object[] valorParametros={movimientoEmision.getFechaInicio().getTime()
				};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					MAPEO_SALIDA,
					MAPEO_SALIDA
					);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString()+
					MENSAJE_REGISTROS+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
						
			return listaResultado != null? listaResultado : new ArrayList<ReporteRCSDTO>();
			
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
				
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReporteRCSDTO> obtieneRCSVidaCad(ReporteRCSDTO movimientoEmision)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		StringBuilder descripcionParametros = new StringBuilder();
		String nombreSP = "SEYCOS.PKG_INFO_VIGOR_VIDA.spVida_RepRCSLPCAD";
		
		try {
			
			String[] nombreParametros = {ANIO_CORTE, };
			Object[] valorParametros={movimientoEmision.getFechaInicio().getTime()
				};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					MAPEO_SALIDA,
					MAPEO_SALIDA);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append( ", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log(MENSAJE_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log(MENSAJE_FIN_LLAMADA_SP+nombreSP+MENSAJE_LLAMADA_SP_PARAMETROS+descripcionParametros.toString()+
					MENSAJE_REGISTROS+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
						
			return listaResultado != null? listaResultado : new ArrayList<ReporteRCSDTO>();
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e); 
				
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log(MENSAJE_ERROR_SP+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}

	public Date getFechaUltimoCorteProcesado() {
		StoredProcedureHelper helper;
		final String sp = "MIDAS.PKG_SOLVENCIA_CNSF.GET_FECHA_ULTIMOCORTEPROCESADO";
		ReaseguradorCnsfMov result;
		Date resultDate = new Date();
		
		try {
			final String atributo = "fechacorte";
			final String columna = "fechacorte";
			
			helper = new StoredProcedureHelper(sp, StoredProcedureHelper.DATASOURCE_MIDAS);
			helper.estableceMapeoResultados(ReaseguradorCnsfMov.class.getCanonicalName(), atributo, columna);
			
			result = (ReaseguradorCnsfMov) helper.obtieneResultadoSencillo();
			
			resultDate = result.getFechacorte();
			
		} catch(Exception e) {
			LogDeMidasEJB3.log("Ha ocurrido un error al tratar de obtener la ultima fecha de corte procesada", Level.WARNING, e);
		}
		
		return resultDate;
	}

}
