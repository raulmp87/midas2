package mx.com.afirme.midas.danios.reportes.reportepmltev;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.danios.reportes.reportepml.AtributoEntradaDTO_PML;
import mx.com.afirme.midas.danios.reportes.reportepml.ParametroSalidaSP_PML;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless
public class ReportePMLTEVFacade implements ReportePMLTEVFacadeRemote {
	public static String SP_PMLTEV = "MIDAS.pkgDAN_Reportes.spDAN_RepPMLTEV";
	public static String SP_PMLTEV_IND_CREDITOS_HIPOTECARIOS = "MIDAS.pkgDAN_Reportes.spDAN_RepPMLTEV_IndCH";
	public static String SP_PMLTEV_IND_GRANDES_RIESGOS = "MIDAS.pkgDAN_Reportes.spDAN_RepPMLTEV_IndGR";
	public static String SP_PMLTEV_IND_RIESGOS_NORMALES = "MIDAS.pkgDAN_Reportes.spDAN_RepPMLTEV_IndRN";
	public static String SP_PMLTEV_SEMI_AGR_CREDITOS_HIPOTECARIOS = "MIDAS.pkgDAN_Reportes.spDAN_RepPMLTEV_SemiAgrCH";
	public static String SP_PMLTEV_SEMI_AGR_GRANDES_RIESGOS = "MIDAS.pkgDAN_Reportes.spDAN_RepPMLTEV_SemiAgrGR";
	public static String SP_PMLTEV_SEMI_AGR_RIESGOS_NORMALES = "MIDAS.pkgDAN_Reportes.spDAN_RepPMLTEV_SemiAgrRN";
	public static String SP_PMLTEV_TECNICOS= "MIDAS.pkgDAN_Reportes.spDAN_RepPMLTEV_Tec";

	@TransactionAttribute(TransactionAttributeType.NEVER)
	public boolean calcularReportePMLTEV(ReportePMLTEVDTO filtroReporte, String nombreUsuario) {
		StoredProcedureHelper storedHelper = null;
		boolean resultado = false;
		String nombreSP = SP_PMLTEV;
		String []nombreParametros = {"pCodigoUsuario","pOpcion","pFechaCorte","pTipoCambio"};
		Object []valorParametros = new Object[4];
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);

			valorParametros[0] = nombreUsuario;
			valorParametros[1] = filtroReporte.getClaveTipoReporte();
			valorParametros[2] = filtroReporte.getFechaCorte();
			valorParametros[3] = filtroReporte.getTipoCambio();
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas.danios.reportes.reportepmltev.ReportePMLTEVDTO", "", "");
			
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			storedHelper.obtieneResultadoSencillo();
			resultado = true;
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLTEVDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". Parametros enviados: "+nombreParametros[0]+"="+valorParametros[0]+", "
					+nombreParametros[1]+"="+valorParametros[1], Level.SEVERE, e);
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". Parametros enviados: "+nombreParametros[0]+"="+valorParametros[0]+", "
					+nombreParametros[1]+"="+valorParametros[1], Level.SEVERE, e);
		}
		return resultado;
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVIndependientesCreditosHipotecarios(Integer claveTipoReporte,String nombreUsuario)throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = SP_PMLTEV_IND_CREDITOS_HIPOTECARIOS;
		
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			String []nombreParametros = {"pCodigoUsuario","pOpcion"};
			Object []valorParametros = {nombreUsuario,claveTipoReporte};
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			String[] atributosDTO = {
					AtributoEntradaDTO_PML.NUM_POLIZA
					,AtributoEntradaDTO_PML.NUM_REGISTRO
					,AtributoEntradaDTO_PML.FECHA_INICIO
					,AtributoEntradaDTO_PML.FECHA_FIN
					,AtributoEntradaDTO_PML.INM_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.INM_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.INM_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.INM_DEDUCIBLE
					,AtributoEntradaDTO_PML.INM_COASEGURO
					,AtributoEntradaDTO_PML.CONT_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONT_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.CONT_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONT_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONT_COASEGURO
					,AtributoEntradaDTO_PML.CONSEC_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONSEC_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.CONSEC_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONSEC_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONSEC_COASEGURO
					,AtributoEntradaDTO_PML.CLAVE_ESTADO
					,AtributoEntradaDTO_PML.ZONA_SISMICA
					,AtributoEntradaDTO_PML.NUM_PISOS
					,AtributoEntradaDTO_PML.ES_INDUSTRIAL
					,AtributoEntradaDTO_PML.CLAVE_MUNICIPIO
					,AtributoEntradaDTO_PML.CODIGO_POSTAL
					,AtributoEntradaDTO_PML.LONGITUD
					,AtributoEntradaDTO_PML.LATITUD
					,AtributoEntradaDTO_PML.EDI_SUELO
					,AtributoEntradaDTO_PML.EDI_FECHA_CONSTRUCCION
					,AtributoEntradaDTO_PML.EDI_USO
					,AtributoEntradaDTO_PML.EST_COLUMNAS
					,AtributoEntradaDTO_PML.EST_TRABES
					,AtributoEntradaDTO_PML.EST_MUROS
					,AtributoEntradaDTO_PML.EST_CUBIERTA
					,AtributoEntradaDTO_PML.EST_CLAROS
					,AtributoEntradaDTO_PML.EST_MUROS_PRE
					,AtributoEntradaDTO_PML.EST_CONTRAVENTEO
					,AtributoEntradaDTO_PML.OTR_COLUMNAS_CORTAS
					,AtributoEntradaDTO_PML.OTR_SOBREPESO
					,AtributoEntradaDTO_PML.OTR_GOLPETEO
					,AtributoEntradaDTO_PML.OTR_ESQUINA
					,AtributoEntradaDTO_PML.OTR_IRRE_ELEVACION
					,AtributoEntradaDTO_PML.OTR_IRRE_PLANTA
					,AtributoEntradaDTO_PML.OTR_HUNDIMIENTOS
					,AtributoEntradaDTO_PML.OTR_DA_PREVIOS
					,AtributoEntradaDTO_PML.OTR_DA_REPARADO
					,AtributoEntradaDTO_PML.OTR_REFORZADA
					,AtributoEntradaDTO_PML.OTR_FECHA
					,AtributoEntradaDTO_PML.PRIMA
					,AtributoEntradaDTO_PML.CEDIDA
					,AtributoEntradaDTO_PML.RETENIDA
					,AtributoEntradaDTO_PML.MONEDA
					,AtributoEntradaDTO_PML.RSR_T
					,AtributoEntradaDTO_PML.OFI_EMI
					,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.VALOR_RETENIDO
					,AtributoEntradaDTO_PML.INCISO
			};
			
			String[] camposResulset = {
					ParametroSalidaSP_PML.NUM_POLIZA
					,ParametroSalidaSP_PML.NUM_REGISTRO
					,ParametroSalidaSP_PML.FECHA_INICIO
					,ParametroSalidaSP_PML.FECHA_FIN
					,ParametroSalidaSP_PML.INM_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.INM_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.INM_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.INM_DEDUCIBLE
					,ParametroSalidaSP_PML.INM_COASEGURO
					,ParametroSalidaSP_PML.CONT_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONT_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.CONT_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONT_DEDUCIBLE
					,ParametroSalidaSP_PML.CONT_COASEGURO
					,ParametroSalidaSP_PML.CONSEC_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONSEC_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.CONSEC_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONSEC_DEDUCIBLE
					,ParametroSalidaSP_PML.CONSEC_COASEGURO
					,ParametroSalidaSP_PML.CLAVE_ESTADO
					,ParametroSalidaSP_PML.ZONA_SISMICA
					,ParametroSalidaSP_PML.NUM_PISOS
					,ParametroSalidaSP_PML.ES_INDUSTRIAL
					,ParametroSalidaSP_PML.CLAVE_MUNICIPIO
					,ParametroSalidaSP_PML.CODIGO_POSTAL
					,ParametroSalidaSP_PML.LONGITUD
					,ParametroSalidaSP_PML.LATITUD
					,ParametroSalidaSP_PML.EDI_SUELO
					,ParametroSalidaSP_PML.EDI_FECHA_CONSTRUCCION
					,ParametroSalidaSP_PML.EDI_USO
					,ParametroSalidaSP_PML.EST_COLUMNAS
					,ParametroSalidaSP_PML.EST_TRABES
					,ParametroSalidaSP_PML.EST_MUROS
					,ParametroSalidaSP_PML.EST_CUBIERTA
					,ParametroSalidaSP_PML.EST_CLAROS
					,ParametroSalidaSP_PML.EST_MUROS_PRE
					,ParametroSalidaSP_PML.EST_CONTRAVENTEO
					,ParametroSalidaSP_PML.OTR_COLUMNAS_CORTAS
					,ParametroSalidaSP_PML.OTR_SOBREPESO
					,ParametroSalidaSP_PML.OTR_GOLPETEO
					,ParametroSalidaSP_PML.OTR_ESQUINA
					,ParametroSalidaSP_PML.OTR_IRRE_ELEVACION
					,ParametroSalidaSP_PML.OTR_IRRE_PLANTA
					,ParametroSalidaSP_PML.OTR_HUNDIMIENTOS
					,ParametroSalidaSP_PML.OTR_DA_PREVIOS
					,ParametroSalidaSP_PML.OTR_DA_REPARADO
					,ParametroSalidaSP_PML.OTR_REFORZADA
					,ParametroSalidaSP_PML.OTR_FECHA
					,ParametroSalidaSP_PML.PRIMA
					,ParametroSalidaSP_PML.CEDIDA
					,ParametroSalidaSP_PML.RETENIDA
					,ParametroSalidaSP_PML.MONEDA
					,ParametroSalidaSP_PML.RSR_T
					,ParametroSalidaSP_PML.OFI_EMI
					,ParametroSalidaSP_PML.VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.VALOR_RETENIDO
					,ParametroSalidaSP_PML.INCISO
			};
			storedHelper.estableceMapeoResultados(ReportePMLTEVDTO.class.getCanonicalName(), atributosDTO,camposResulset);
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			return storedHelper.obtieneListaResultados();
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLTEVDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVIndependientesGrandesRiesgos(Integer claveTipoReporte,String nombreUsuario)throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = SP_PMLTEV_IND_GRANDES_RIESGOS;
		
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			String []nombreParametros = {"pCodigoUsuario","pOpcion"};
			Object []valorParametros = {nombreUsuario,claveTipoReporte};
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			String[] atributosDTO = {
					AtributoEntradaDTO_PML.NUM_POLIZA
					,AtributoEntradaDTO_PML.NUM_REGISTRO
					,AtributoEntradaDTO_PML.FECHA_INICIO
					,AtributoEntradaDTO_PML.FECHA_FIN
					,AtributoEntradaDTO_PML.INM_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.INM_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.INM_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.INM_DEDUCIBLE
					,AtributoEntradaDTO_PML.INM_COASEGURO
					,AtributoEntradaDTO_PML.CONT_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONT_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.CONT_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONT_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONT_COASEGURO
					,AtributoEntradaDTO_PML.CONSEC_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONSEC_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.CONSEC_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONSEC_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONSEC_COASEGURO
					,AtributoEntradaDTO_PML.CLAVE_ESTADO
					,AtributoEntradaDTO_PML.ZONA_SISMICA
					,AtributoEntradaDTO_PML.NUM_PISOS
					,AtributoEntradaDTO_PML.ES_INDUSTRIAL
					,AtributoEntradaDTO_PML.CLAVE_MUNICIPIO
					,AtributoEntradaDTO_PML.CODIGO_POSTAL
					,AtributoEntradaDTO_PML.LONGITUD
					,AtributoEntradaDTO_PML.LATITUD
					,AtributoEntradaDTO_PML.EDI_SUELO
					,AtributoEntradaDTO_PML.EDI_FECHA_CONSTRUCCION
					,AtributoEntradaDTO_PML.EDI_USO
					,AtributoEntradaDTO_PML.EST_COLUMNAS
					,AtributoEntradaDTO_PML.EST_TRABES
					,AtributoEntradaDTO_PML.EST_MUROS
					,AtributoEntradaDTO_PML.EST_CUBIERTA
					,AtributoEntradaDTO_PML.EST_CLAROS
					,AtributoEntradaDTO_PML.EST_MUROS_PRE
					,AtributoEntradaDTO_PML.EST_CONTRAVENTEO
					,AtributoEntradaDTO_PML.OTR_COLUMNAS_CORTAS
					,AtributoEntradaDTO_PML.OTR_SOBREPESO
					,AtributoEntradaDTO_PML.OTR_GOLPETEO
					,AtributoEntradaDTO_PML.OTR_ESQUINA
					,AtributoEntradaDTO_PML.OTR_IRRE_ELEVACION
					,AtributoEntradaDTO_PML.OTR_IRRE_PLANTA
					,AtributoEntradaDTO_PML.OTR_HUNDIMIENTOS
					,AtributoEntradaDTO_PML.OTR_DA_PREVIOS
					,AtributoEntradaDTO_PML.OTR_DA_REPARADO
					,AtributoEntradaDTO_PML.OTR_REFORZADA
					,AtributoEntradaDTO_PML.OTR_FECHA
					,AtributoEntradaDTO_PML.PRIMA
					,AtributoEntradaDTO_PML.CEDIDA
					,AtributoEntradaDTO_PML.RETENIDA
					,AtributoEntradaDTO_PML.MONEDA
					,AtributoEntradaDTO_PML.RSR_T
					,AtributoEntradaDTO_PML.OFI_EMI
					,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.VALOR_RETENIDO
					,AtributoEntradaDTO_PML.INCISO
			};
			
			String[] camposResulset = {
					ParametroSalidaSP_PML.NUM_POLIZA
					,ParametroSalidaSP_PML.NUM_REGISTRO
					,ParametroSalidaSP_PML.FECHA_INICIO
					,ParametroSalidaSP_PML.FECHA_FIN
					,ParametroSalidaSP_PML.INM_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.INM_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.INM_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.INM_DEDUCIBLE
					,ParametroSalidaSP_PML.INM_COASEGURO
					,ParametroSalidaSP_PML.CONT_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONT_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.CONT_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONT_DEDUCIBLE
					,ParametroSalidaSP_PML.CONT_COASEGURO
					,ParametroSalidaSP_PML.CONSEC_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONSEC_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.CONSEC_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONSEC_DEDUCIBLE
					,ParametroSalidaSP_PML.CONSEC_COASEGURO
					,ParametroSalidaSP_PML.CLAVE_ESTADO
					,ParametroSalidaSP_PML.ZONA_SISMICA
					,ParametroSalidaSP_PML.NUM_PISOS
					,ParametroSalidaSP_PML.ES_INDUSTRIAL
					,ParametroSalidaSP_PML.CLAVE_MUNICIPIO
					,ParametroSalidaSP_PML.CODIGO_POSTAL
					,ParametroSalidaSP_PML.LONGITUD
					,ParametroSalidaSP_PML.LATITUD
					,ParametroSalidaSP_PML.EDI_SUELO
					,ParametroSalidaSP_PML.EDI_FECHA_CONSTRUCCION
					,ParametroSalidaSP_PML.EDI_USO
					,ParametroSalidaSP_PML.EST_COLUMNAS
					,ParametroSalidaSP_PML.EST_TRABES
					,ParametroSalidaSP_PML.EST_MUROS
					,ParametroSalidaSP_PML.EST_CUBIERTA
					,ParametroSalidaSP_PML.EST_CLAROS
					,ParametroSalidaSP_PML.EST_MUROS_PRE
					,ParametroSalidaSP_PML.EST_CONTRAVENTEO
					,ParametroSalidaSP_PML.OTR_COLUMNAS_CORTAS
					,ParametroSalidaSP_PML.OTR_SOBREPESO
					,ParametroSalidaSP_PML.OTR_GOLPETEO
					,ParametroSalidaSP_PML.OTR_ESQUINA
					,ParametroSalidaSP_PML.OTR_IRRE_ELEVACION
					,ParametroSalidaSP_PML.OTR_IRRE_PLANTA
					,ParametroSalidaSP_PML.OTR_HUNDIMIENTOS
					,ParametroSalidaSP_PML.OTR_DA_PREVIOS
					,ParametroSalidaSP_PML.OTR_DA_REPARADO
					,ParametroSalidaSP_PML.OTR_REFORZADA
					,ParametroSalidaSP_PML.OTR_FECHA
					,ParametroSalidaSP_PML.PRIMA
					,ParametroSalidaSP_PML.CEDIDA
					,ParametroSalidaSP_PML.RETENIDA
					,ParametroSalidaSP_PML.MONEDA
					,ParametroSalidaSP_PML.RSR_T
					,ParametroSalidaSP_PML.OFI_EMI
					,ParametroSalidaSP_PML.VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.VALOR_RETENIDO
					,ParametroSalidaSP_PML.INCISO
			};
			storedHelper.estableceMapeoResultados(ReportePMLTEVDTO.class.getCanonicalName(), atributosDTO,camposResulset);
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			return storedHelper.obtieneListaResultados();
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLTEVDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVIndependientesRiesgosNormales(Integer claveTipoReporte,String nombreUsuario)throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = SP_PMLTEV_IND_RIESGOS_NORMALES;
		
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			String []nombreParametros = {"pCodigoUsuario","pOpcion"};
			Object []valorParametros = {nombreUsuario,claveTipoReporte};
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			String[] atributosDTO = {
					AtributoEntradaDTO_PML.NUM_POLIZA
					,AtributoEntradaDTO_PML.NUM_REGISTRO
					,AtributoEntradaDTO_PML.FECHA_INICIO
					,AtributoEntradaDTO_PML.FECHA_FIN
					,AtributoEntradaDTO_PML.INM_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.INM_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.INM_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.INM_DEDUCIBLE
					,AtributoEntradaDTO_PML.INM_COASEGURO
					,AtributoEntradaDTO_PML.CONT_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONT_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.CONT_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONT_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONT_COASEGURO
					,AtributoEntradaDTO_PML.CONSEC_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONSEC_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.CONSEC_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONSEC_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONSEC_COASEGURO
					,AtributoEntradaDTO_PML.CLAVE_ESTADO
					,AtributoEntradaDTO_PML.ZONA_SISMICA
					,AtributoEntradaDTO_PML.NUM_PISOS
					,AtributoEntradaDTO_PML.ES_INDUSTRIAL
					,AtributoEntradaDTO_PML.CLAVE_MUNICIPIO
					,AtributoEntradaDTO_PML.CODIGO_POSTAL
					,AtributoEntradaDTO_PML.LONGITUD
					,AtributoEntradaDTO_PML.LATITUD
					,AtributoEntradaDTO_PML.EDI_SUELO
					,AtributoEntradaDTO_PML.EDI_FECHA_CONSTRUCCION
					,AtributoEntradaDTO_PML.EDI_USO
					,AtributoEntradaDTO_PML.EST_COLUMNAS
					,AtributoEntradaDTO_PML.EST_TRABES
					,AtributoEntradaDTO_PML.EST_MUROS
					,AtributoEntradaDTO_PML.EST_CUBIERTA
					,AtributoEntradaDTO_PML.EST_CLAROS
					,AtributoEntradaDTO_PML.EST_MUROS_PRE
					,AtributoEntradaDTO_PML.EST_CONTRAVENTEO
					,AtributoEntradaDTO_PML.OTR_COLUMNAS_CORTAS
					,AtributoEntradaDTO_PML.OTR_SOBREPESO
					,AtributoEntradaDTO_PML.OTR_GOLPETEO
					,AtributoEntradaDTO_PML.OTR_ESQUINA
					,AtributoEntradaDTO_PML.OTR_IRRE_ELEVACION
					,AtributoEntradaDTO_PML.OTR_IRRE_PLANTA
					,AtributoEntradaDTO_PML.OTR_HUNDIMIENTOS
					,AtributoEntradaDTO_PML.OTR_DA_PREVIOS
					,AtributoEntradaDTO_PML.OTR_DA_REPARADO
					,AtributoEntradaDTO_PML.OTR_REFORZADA
					,AtributoEntradaDTO_PML.OTR_FECHA
					,AtributoEntradaDTO_PML.PRIMA
					,AtributoEntradaDTO_PML.CEDIDA
					,AtributoEntradaDTO_PML.RETENIDA
					,AtributoEntradaDTO_PML.MONEDA
					,AtributoEntradaDTO_PML.RSR_T
					,AtributoEntradaDTO_PML.OFI_EMI
					,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.VALOR_RETENIDO
					,AtributoEntradaDTO_PML.INCISO
			};
			
			String[] camposResulset = {
					ParametroSalidaSP_PML.NUM_POLIZA
					,ParametroSalidaSP_PML.NUM_REGISTRO
					,ParametroSalidaSP_PML.FECHA_INICIO
					,ParametroSalidaSP_PML.FECHA_FIN
					,ParametroSalidaSP_PML.INM_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.INM_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.INM_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.INM_DEDUCIBLE
					,ParametroSalidaSP_PML.INM_COASEGURO
					,ParametroSalidaSP_PML.CONT_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONT_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.CONT_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONT_DEDUCIBLE
					,ParametroSalidaSP_PML.CONT_COASEGURO
					,ParametroSalidaSP_PML.CONSEC_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONSEC_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.CONSEC_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONSEC_DEDUCIBLE
					,ParametroSalidaSP_PML.CONSEC_COASEGURO
					,ParametroSalidaSP_PML.CLAVE_ESTADO
					,ParametroSalidaSP_PML.ZONA_SISMICA
					,ParametroSalidaSP_PML.NUM_PISOS
					,ParametroSalidaSP_PML.ES_INDUSTRIAL
					,ParametroSalidaSP_PML.CLAVE_MUNICIPIO
					,ParametroSalidaSP_PML.CODIGO_POSTAL
					,ParametroSalidaSP_PML.LONGITUD
					,ParametroSalidaSP_PML.LATITUD
					,ParametroSalidaSP_PML.EDI_SUELO
					,ParametroSalidaSP_PML.EDI_FECHA_CONSTRUCCION
					,ParametroSalidaSP_PML.EDI_USO
					,ParametroSalidaSP_PML.EST_COLUMNAS
					,ParametroSalidaSP_PML.EST_TRABES
					,ParametroSalidaSP_PML.EST_MUROS
					,ParametroSalidaSP_PML.EST_CUBIERTA
					,ParametroSalidaSP_PML.EST_CLAROS
					,ParametroSalidaSP_PML.EST_MUROS_PRE
					,ParametroSalidaSP_PML.EST_CONTRAVENTEO
					,ParametroSalidaSP_PML.OTR_COLUMNAS_CORTAS
					,ParametroSalidaSP_PML.OTR_SOBREPESO
					,ParametroSalidaSP_PML.OTR_GOLPETEO
					,ParametroSalidaSP_PML.OTR_ESQUINA
					,ParametroSalidaSP_PML.OTR_IRRE_ELEVACION
					,ParametroSalidaSP_PML.OTR_IRRE_PLANTA
					,ParametroSalidaSP_PML.OTR_HUNDIMIENTOS
					,ParametroSalidaSP_PML.OTR_DA_PREVIOS
					,ParametroSalidaSP_PML.OTR_DA_REPARADO
					,ParametroSalidaSP_PML.OTR_REFORZADA
					,ParametroSalidaSP_PML.OTR_FECHA
					,ParametroSalidaSP_PML.PRIMA
					,ParametroSalidaSP_PML.CEDIDA
					,ParametroSalidaSP_PML.RETENIDA
					,ParametroSalidaSP_PML.MONEDA
					,ParametroSalidaSP_PML.RSR_T
					,ParametroSalidaSP_PML.OFI_EMI
					,ParametroSalidaSP_PML.VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.VALOR_RETENIDO
					,ParametroSalidaSP_PML.INCISO
			};
			storedHelper.estableceMapeoResultados(ReportePMLTEVDTO.class.getCanonicalName(), atributosDTO,camposResulset);
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			return storedHelper.obtieneListaResultados();
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLTEVDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVSemiAgrupadoCreditosHipotecarios(Integer claveTipoReporte,String nombreUsuario)throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = SP_PMLTEV_SEMI_AGR_CREDITOS_HIPOTECARIOS;
		
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			String []nombreParametros = {"pCodigoUsuario","pOpcion"};
			Object []valorParametros = {nombreUsuario,claveTipoReporte};
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			String[] atributosDTO = {
					AtributoEntradaDTO_PML.NUM_POLIZA
					,AtributoEntradaDTO_PML.NUM_REGISTRO
					,AtributoEntradaDTO_PML.FECHA_INICIO
					,AtributoEntradaDTO_PML.FECHA_FIN
					,AtributoEntradaDTO_PML.INM_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.INM_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.INM_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.INM_DEDUCIBLE
					,AtributoEntradaDTO_PML.INM_COASEGURO
					,AtributoEntradaDTO_PML.CONT_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONT_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.CONT_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONT_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONT_COASEGURO
					,AtributoEntradaDTO_PML.CONSEC_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONSEC_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.CONSEC_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONSEC_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONSEC_COASEGURO
					,AtributoEntradaDTO_PML.CLAVE_ESTADO
					,AtributoEntradaDTO_PML.ZONA_SISMICA
					,AtributoEntradaDTO_PML.NUM_PISOS
					,AtributoEntradaDTO_PML.ES_INDUSTRIAL
					,AtributoEntradaDTO_PML.CLAVE_MUNICIPIO
					,AtributoEntradaDTO_PML.CODIGO_POSTAL
					,AtributoEntradaDTO_PML.LONGITUD
					,AtributoEntradaDTO_PML.LATITUD
					,AtributoEntradaDTO_PML.EDI_SUELO
					,AtributoEntradaDTO_PML.EDI_FECHA_CONSTRUCCION
					,AtributoEntradaDTO_PML.EDI_USO
					,AtributoEntradaDTO_PML.EST_COLUMNAS
					,AtributoEntradaDTO_PML.EST_TRABES
					,AtributoEntradaDTO_PML.EST_MUROS
					,AtributoEntradaDTO_PML.EST_CUBIERTA
					,AtributoEntradaDTO_PML.EST_CLAROS
					,AtributoEntradaDTO_PML.EST_MUROS_PRE
					,AtributoEntradaDTO_PML.EST_CONTRAVENTEO
					,AtributoEntradaDTO_PML.OTR_COLUMNAS_CORTAS
					,AtributoEntradaDTO_PML.OTR_SOBREPESO
					,AtributoEntradaDTO_PML.OTR_GOLPETEO
					,AtributoEntradaDTO_PML.OTR_ESQUINA
					,AtributoEntradaDTO_PML.OTR_IRRE_ELEVACION
					,AtributoEntradaDTO_PML.OTR_IRRE_PLANTA
					,AtributoEntradaDTO_PML.OTR_HUNDIMIENTOS
					,AtributoEntradaDTO_PML.OTR_DA_PREVIOS
					,AtributoEntradaDTO_PML.OTR_DA_REPARADO
					,AtributoEntradaDTO_PML.OTR_REFORZADA
					,AtributoEntradaDTO_PML.OTR_FECHA
					,AtributoEntradaDTO_PML.PRIMA
					,AtributoEntradaDTO_PML.CEDIDA
					,AtributoEntradaDTO_PML.RETENIDA
					,AtributoEntradaDTO_PML.MONEDA
					,AtributoEntradaDTO_PML.RSR_T
					,AtributoEntradaDTO_PML.OFI_EMI
					,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.VALOR_RETENIDO
					,AtributoEntradaDTO_PML.INCISO
			};
			
			String[] camposResulset = {
					ParametroSalidaSP_PML.NUM_POLIZA
					,ParametroSalidaSP_PML.NUM_REGISTRO
					,ParametroSalidaSP_PML.FECHA_INICIO
					,ParametroSalidaSP_PML.FECHA_FIN
					,ParametroSalidaSP_PML.INM_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.INM_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.INM_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.INM_DEDUCIBLE
					,ParametroSalidaSP_PML.INM_COASEGURO
					,ParametroSalidaSP_PML.CONT_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONT_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.CONT_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONT_DEDUCIBLE
					,ParametroSalidaSP_PML.CONT_COASEGURO
					,ParametroSalidaSP_PML.CONSEC_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONSEC_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.CONSEC_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONSEC_DEDUCIBLE
					,ParametroSalidaSP_PML.CONSEC_COASEGURO
					,ParametroSalidaSP_PML.CLAVE_ESTADO
					,ParametroSalidaSP_PML.ZONA_SISMICA
					,ParametroSalidaSP_PML.NUM_PISOS
					,ParametroSalidaSP_PML.ES_INDUSTRIAL
					,ParametroSalidaSP_PML.CLAVE_MUNICIPIO
					,ParametroSalidaSP_PML.CODIGO_POSTAL
					,ParametroSalidaSP_PML.LONGITUD
					,ParametroSalidaSP_PML.LATITUD
					,ParametroSalidaSP_PML.EDI_SUELO
					,ParametroSalidaSP_PML.EDI_FECHA_CONSTRUCCION
					,ParametroSalidaSP_PML.EDI_USO
					,ParametroSalidaSP_PML.EST_COLUMNAS
					,ParametroSalidaSP_PML.EST_TRABES
					,ParametroSalidaSP_PML.EST_MUROS
					,ParametroSalidaSP_PML.EST_CUBIERTA
					,ParametroSalidaSP_PML.EST_CLAROS
					,ParametroSalidaSP_PML.EST_MUROS_PRE
					,ParametroSalidaSP_PML.EST_CONTRAVENTEO
					,ParametroSalidaSP_PML.OTR_COLUMNAS_CORTAS
					,ParametroSalidaSP_PML.OTR_SOBREPESO
					,ParametroSalidaSP_PML.OTR_GOLPETEO
					,ParametroSalidaSP_PML.OTR_ESQUINA
					,ParametroSalidaSP_PML.OTR_IRRE_ELEVACION
					,ParametroSalidaSP_PML.OTR_IRRE_PLANTA
					,ParametroSalidaSP_PML.OTR_HUNDIMIENTOS
					,ParametroSalidaSP_PML.OTR_DA_PREVIOS
					,ParametroSalidaSP_PML.OTR_DA_REPARADO
					,ParametroSalidaSP_PML.OTR_REFORZADA
					,ParametroSalidaSP_PML.OTR_FECHA
					,ParametroSalidaSP_PML.PRIMA
					,ParametroSalidaSP_PML.CEDIDA
					,ParametroSalidaSP_PML.RETENIDA
					,ParametroSalidaSP_PML.MONEDA
					,ParametroSalidaSP_PML.RSR_T
					,ParametroSalidaSP_PML.OFI_EMI
					,ParametroSalidaSP_PML.VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.VALOR_RETENIDO
					,ParametroSalidaSP_PML.INCISO
			};
			storedHelper.estableceMapeoResultados(ReportePMLTEVDTO.class.getCanonicalName(), atributosDTO,camposResulset);
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			return storedHelper.obtieneListaResultados();
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLTEVDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVSemiAgrupadoGrandesRiesgos(Integer claveTipoReporte,String nombreUsuario)throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = SP_PMLTEV_SEMI_AGR_GRANDES_RIESGOS;
		
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			String []nombreParametros = {"pCodigoUsuario","pOpcion"};
			Object []valorParametros = {nombreUsuario,claveTipoReporte};
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			String[] atributosDTO = {
					AtributoEntradaDTO_PML.NUM_POLIZA
					,AtributoEntradaDTO_PML.NUM_REGISTRO
					,AtributoEntradaDTO_PML.FECHA_INICIO
					,AtributoEntradaDTO_PML.FECHA_FIN
					,AtributoEntradaDTO_PML.INM_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.INM_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.INM_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.INM_DEDUCIBLE
					,AtributoEntradaDTO_PML.INM_COASEGURO
					,AtributoEntradaDTO_PML.CONT_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONT_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.CONT_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONT_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONT_COASEGURO
					,AtributoEntradaDTO_PML.CONSEC_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONSEC_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.CONSEC_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONSEC_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONSEC_COASEGURO
					,AtributoEntradaDTO_PML.CLAVE_ESTADO
					,AtributoEntradaDTO_PML.ZONA_SISMICA
					,AtributoEntradaDTO_PML.NUM_PISOS
					,AtributoEntradaDTO_PML.ES_INDUSTRIAL
					,AtributoEntradaDTO_PML.CLAVE_MUNICIPIO
					,AtributoEntradaDTO_PML.CODIGO_POSTAL
					,AtributoEntradaDTO_PML.LONGITUD
					,AtributoEntradaDTO_PML.LATITUD
					,AtributoEntradaDTO_PML.EDI_SUELO
					,AtributoEntradaDTO_PML.EDI_FECHA_CONSTRUCCION
					,AtributoEntradaDTO_PML.EDI_USO
					,AtributoEntradaDTO_PML.EST_COLUMNAS
					,AtributoEntradaDTO_PML.EST_TRABES
					,AtributoEntradaDTO_PML.EST_MUROS
					,AtributoEntradaDTO_PML.EST_CUBIERTA
					,AtributoEntradaDTO_PML.EST_CLAROS
					,AtributoEntradaDTO_PML.EST_MUROS_PRE
					,AtributoEntradaDTO_PML.EST_CONTRAVENTEO
					,AtributoEntradaDTO_PML.OTR_COLUMNAS_CORTAS
					,AtributoEntradaDTO_PML.OTR_SOBREPESO
					,AtributoEntradaDTO_PML.OTR_GOLPETEO
					,AtributoEntradaDTO_PML.OTR_ESQUINA
					,AtributoEntradaDTO_PML.OTR_IRRE_ELEVACION
					,AtributoEntradaDTO_PML.OTR_IRRE_PLANTA
					,AtributoEntradaDTO_PML.OTR_HUNDIMIENTOS
					,AtributoEntradaDTO_PML.OTR_DA_PREVIOS
					,AtributoEntradaDTO_PML.OTR_DA_REPARADO
					,AtributoEntradaDTO_PML.OTR_REFORZADA
					,AtributoEntradaDTO_PML.OTR_FECHA
					,AtributoEntradaDTO_PML.PRIMA
					,AtributoEntradaDTO_PML.CEDIDA
					,AtributoEntradaDTO_PML.RETENIDA
					,AtributoEntradaDTO_PML.MONEDA
					,AtributoEntradaDTO_PML.RSR_T
					,AtributoEntradaDTO_PML.OFI_EMI
					,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.VALOR_RETENIDO
					,AtributoEntradaDTO_PML.INCISO
			};
			
			String[] camposResulset = {
					ParametroSalidaSP_PML.NUM_POLIZA
					,ParametroSalidaSP_PML.NUM_REGISTRO
					,ParametroSalidaSP_PML.FECHA_INICIO
					,ParametroSalidaSP_PML.FECHA_FIN
					,ParametroSalidaSP_PML.INM_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.INM_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.INM_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.INM_DEDUCIBLE
					,ParametroSalidaSP_PML.INM_COASEGURO
					,ParametroSalidaSP_PML.CONT_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONT_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.CONT_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONT_DEDUCIBLE
					,ParametroSalidaSP_PML.CONT_COASEGURO
					,ParametroSalidaSP_PML.CONSEC_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONSEC_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.CONSEC_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONSEC_DEDUCIBLE
					,ParametroSalidaSP_PML.CONSEC_COASEGURO
					,ParametroSalidaSP_PML.CLAVE_ESTADO
					,ParametroSalidaSP_PML.ZONA_SISMICA
					,ParametroSalidaSP_PML.NUM_PISOS
					,ParametroSalidaSP_PML.ES_INDUSTRIAL
					,ParametroSalidaSP_PML.CLAVE_MUNICIPIO
					,ParametroSalidaSP_PML.CODIGO_POSTAL
					,ParametroSalidaSP_PML.LONGITUD
					,ParametroSalidaSP_PML.LATITUD
					,ParametroSalidaSP_PML.EDI_SUELO
					,ParametroSalidaSP_PML.EDI_FECHA_CONSTRUCCION
					,ParametroSalidaSP_PML.EDI_USO
					,ParametroSalidaSP_PML.EST_COLUMNAS
					,ParametroSalidaSP_PML.EST_TRABES
					,ParametroSalidaSP_PML.EST_MUROS
					,ParametroSalidaSP_PML.EST_CUBIERTA
					,ParametroSalidaSP_PML.EST_CLAROS
					,ParametroSalidaSP_PML.EST_MUROS_PRE
					,ParametroSalidaSP_PML.EST_CONTRAVENTEO
					,ParametroSalidaSP_PML.OTR_COLUMNAS_CORTAS
					,ParametroSalidaSP_PML.OTR_SOBREPESO
					,ParametroSalidaSP_PML.OTR_GOLPETEO
					,ParametroSalidaSP_PML.OTR_ESQUINA
					,ParametroSalidaSP_PML.OTR_IRRE_ELEVACION
					,ParametroSalidaSP_PML.OTR_IRRE_PLANTA
					,ParametroSalidaSP_PML.OTR_HUNDIMIENTOS
					,ParametroSalidaSP_PML.OTR_DA_PREVIOS
					,ParametroSalidaSP_PML.OTR_DA_REPARADO
					,ParametroSalidaSP_PML.OTR_REFORZADA
					,ParametroSalidaSP_PML.OTR_FECHA
					,ParametroSalidaSP_PML.PRIMA
					,ParametroSalidaSP_PML.CEDIDA
					,ParametroSalidaSP_PML.RETENIDA
					,ParametroSalidaSP_PML.MONEDA
					,ParametroSalidaSP_PML.RSR_T
					,ParametroSalidaSP_PML.OFI_EMI
					,ParametroSalidaSP_PML.VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.VALOR_RETENIDO
					,ParametroSalidaSP_PML.INCISO
			};
			storedHelper.estableceMapeoResultados(ReportePMLTEVDTO.class.getCanonicalName(), atributosDTO,camposResulset);
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			return storedHelper.obtieneListaResultados();
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLTEVDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVSemiAgrupadoRiesgosNormales(Integer claveTipoReporte,String nombreUsuario)throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = SP_PMLTEV_SEMI_AGR_RIESGOS_NORMALES;
		
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			String []nombreParametros = {"pCodigoUsuario","pOpcion"};
			Object []valorParametros = {nombreUsuario,claveTipoReporte};
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			String[] atributosDTO = {
					AtributoEntradaDTO_PML.NUM_POLIZA
					,AtributoEntradaDTO_PML.NUM_REGISTRO
					,AtributoEntradaDTO_PML.FECHA_INICIO
					,AtributoEntradaDTO_PML.FECHA_FIN
					,AtributoEntradaDTO_PML.INM_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.INM_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.INM_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.INM_DEDUCIBLE
					,AtributoEntradaDTO_PML.INM_COASEGURO
					,AtributoEntradaDTO_PML.CONT_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONT_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.CONT_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONT_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONT_COASEGURO
					,AtributoEntradaDTO_PML.CONSEC_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONSEC_PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.CONSEC_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONSEC_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONSEC_COASEGURO
					,AtributoEntradaDTO_PML.CLAVE_ESTADO
					,AtributoEntradaDTO_PML.ZONA_SISMICA
					,AtributoEntradaDTO_PML.NUM_PISOS
					,AtributoEntradaDTO_PML.ES_INDUSTRIAL
					,AtributoEntradaDTO_PML.CLAVE_MUNICIPIO
					,AtributoEntradaDTO_PML.CODIGO_POSTAL
					,AtributoEntradaDTO_PML.LONGITUD
					,AtributoEntradaDTO_PML.LATITUD
					,AtributoEntradaDTO_PML.EDI_SUELO
					,AtributoEntradaDTO_PML.EDI_FECHA_CONSTRUCCION
					,AtributoEntradaDTO_PML.EDI_USO
					,AtributoEntradaDTO_PML.EST_COLUMNAS
					,AtributoEntradaDTO_PML.EST_TRABES
					,AtributoEntradaDTO_PML.EST_MUROS
					,AtributoEntradaDTO_PML.EST_CUBIERTA
					,AtributoEntradaDTO_PML.EST_CLAROS
					,AtributoEntradaDTO_PML.EST_MUROS_PRE
					,AtributoEntradaDTO_PML.EST_CONTRAVENTEO
					,AtributoEntradaDTO_PML.OTR_COLUMNAS_CORTAS
					,AtributoEntradaDTO_PML.OTR_SOBREPESO
					,AtributoEntradaDTO_PML.OTR_GOLPETEO
					,AtributoEntradaDTO_PML.OTR_ESQUINA
					,AtributoEntradaDTO_PML.OTR_IRRE_ELEVACION
					,AtributoEntradaDTO_PML.OTR_IRRE_PLANTA
					,AtributoEntradaDTO_PML.OTR_HUNDIMIENTOS
					,AtributoEntradaDTO_PML.OTR_DA_PREVIOS
					,AtributoEntradaDTO_PML.OTR_DA_REPARADO
					,AtributoEntradaDTO_PML.OTR_REFORZADA
					,AtributoEntradaDTO_PML.OTR_FECHA
					,AtributoEntradaDTO_PML.PRIMA
					,AtributoEntradaDTO_PML.CEDIDA
					,AtributoEntradaDTO_PML.RETENIDA
					,AtributoEntradaDTO_PML.MONEDA
					,AtributoEntradaDTO_PML.RSR_T
					,AtributoEntradaDTO_PML.OFI_EMI
					,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.VALOR_RETENIDO
					,AtributoEntradaDTO_PML.INCISO
			};
			
			String[] camposResulset = {
					ParametroSalidaSP_PML.NUM_POLIZA
					,ParametroSalidaSP_PML.NUM_REGISTRO
					,ParametroSalidaSP_PML.FECHA_INICIO
					,ParametroSalidaSP_PML.FECHA_FIN
					,ParametroSalidaSP_PML.INM_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.INM_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.INM_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.INM_DEDUCIBLE
					,ParametroSalidaSP_PML.INM_COASEGURO
					,ParametroSalidaSP_PML.CONT_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONT_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.CONT_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONT_DEDUCIBLE
					,ParametroSalidaSP_PML.CONT_COASEGURO
					,ParametroSalidaSP_PML.CONSEC_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONSEC_PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.CONSEC_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONSEC_DEDUCIBLE
					,ParametroSalidaSP_PML.CONSEC_COASEGURO
					,ParametroSalidaSP_PML.CLAVE_ESTADO
					,ParametroSalidaSP_PML.ZONA_SISMICA
					,ParametroSalidaSP_PML.NUM_PISOS
					,ParametroSalidaSP_PML.ES_INDUSTRIAL
					,ParametroSalidaSP_PML.CLAVE_MUNICIPIO
					,ParametroSalidaSP_PML.CODIGO_POSTAL
					,ParametroSalidaSP_PML.LONGITUD
					,ParametroSalidaSP_PML.LATITUD
					,ParametroSalidaSP_PML.EDI_SUELO
					,ParametroSalidaSP_PML.EDI_FECHA_CONSTRUCCION
					,ParametroSalidaSP_PML.EDI_USO
					,ParametroSalidaSP_PML.EST_COLUMNAS
					,ParametroSalidaSP_PML.EST_TRABES
					,ParametroSalidaSP_PML.EST_MUROS
					,ParametroSalidaSP_PML.EST_CUBIERTA
					,ParametroSalidaSP_PML.EST_CLAROS
					,ParametroSalidaSP_PML.EST_MUROS_PRE
					,ParametroSalidaSP_PML.EST_CONTRAVENTEO
					,ParametroSalidaSP_PML.OTR_COLUMNAS_CORTAS
					,ParametroSalidaSP_PML.OTR_SOBREPESO
					,ParametroSalidaSP_PML.OTR_GOLPETEO
					,ParametroSalidaSP_PML.OTR_ESQUINA
					,ParametroSalidaSP_PML.OTR_IRRE_ELEVACION
					,ParametroSalidaSP_PML.OTR_IRRE_PLANTA
					,ParametroSalidaSP_PML.OTR_HUNDIMIENTOS
					,ParametroSalidaSP_PML.OTR_DA_PREVIOS
					,ParametroSalidaSP_PML.OTR_DA_REPARADO
					,ParametroSalidaSP_PML.OTR_REFORZADA
					,ParametroSalidaSP_PML.OTR_FECHA
					,ParametroSalidaSP_PML.PRIMA
					,ParametroSalidaSP_PML.CEDIDA
					,ParametroSalidaSP_PML.RETENIDA
					,ParametroSalidaSP_PML.MONEDA
					,ParametroSalidaSP_PML.RSR_T
					,ParametroSalidaSP_PML.OFI_EMI
					,ParametroSalidaSP_PML.VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.VALOR_RETENIDO
					,ParametroSalidaSP_PML.INCISO
			};
			storedHelper.estableceMapeoResultados(ReportePMLTEVDTO.class.getCanonicalName(), atributosDTO,camposResulset);
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			return storedHelper.obtieneListaResultados();
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLTEVDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVTecnicos(Integer claveTipoReporte,String nombreUsuario)throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = SP_PMLTEV_TECNICOS;
		
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			String []nombreParametros = {"pCodigoUsuario","pOpcion"};
			Object []valorParametros = {nombreUsuario,claveTipoReporte};
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			String[] atributosDTO = {
					AtributoEntradaDTO_PML.NUM_POLIZA
					,AtributoEntradaDTO_PML.NUM_REGISTRO
					,AtributoEntradaDTO_PML.FECHA_INICIO
					,AtributoEntradaDTO_PML.FECHA_FIN
					,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.VALOR_RETENIDO
					,AtributoEntradaDTO_PML.PRIMA
					,AtributoEntradaDTO_PML.CEDIDA
					,AtributoEntradaDTO_PML.RETENIDA
					,AtributoEntradaDTO_PML.MONEDA
					,AtributoEntradaDTO_PML.RSR_T
					,AtributoEntradaDTO_PML.OFI_EMI
					,AtributoEntradaDTO_PML.ZONA_AMIS
			};
			
			String[] camposResulset = {
					ParametroSalidaSP_PML.NUM_POLIZA
					,ParametroSalidaSP_PML.NUM_REGISTRO
					,ParametroSalidaSP_PML.FECHA_INICIO
					,ParametroSalidaSP_PML.FECHA_FIN
					,ParametroSalidaSP_PML.VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.VALOR_RETENIDO
					,ParametroSalidaSP_PML.PRIMA
					,ParametroSalidaSP_PML.CEDIDA
					,ParametroSalidaSP_PML.RETENIDA
					,ParametroSalidaSP_PML.MONEDA
					,ParametroSalidaSP_PML.RSR_T
					,ParametroSalidaSP_PML.OFI_EMI
					,ParametroSalidaSP_PML.ZONA_AMIS
			};
			storedHelper.estableceMapeoResultados(ReportePMLTEVDTO.class.getCanonicalName(), atributosDTO,camposResulset);
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			return storedHelper.obtieneListaResultados();
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLTEVDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		}
	}
	
	@Deprecated
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLTEVDTO> obtieneReportePMLTEV(
			ReportePMLTEVDTO filtroReporte, String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper(
					"MIDAS.pkgDAN_Reportes.spDAN_RepPMLTEV", StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper
			.estableceMapeoResultados(
					ReportePMLTEVDTO.class.getCanonicalName(),
					
					"idRegistro," +
					"numeroPoliza," +
					"numeroRegistro," +
					"fechaInicio," +
					"fechaFin," +
					"inmValorAsegurable," +
					"inmPorcentajeRetencion," +
					"inmLimiteMaximo," +
					"inmDeducible," +
					"inmCoaseguro," +
					"contValorAsegurable," +
					"contPorcentajeRetencion," +
					"contLimiteMaximo," +
					"contDeducible," +
					"contCoaseguro," +
					"consecValorAsegurable," +
					"consecPorcentajeRetencion," +
					"consecLimiteMaximo," +
					"consecDeducible," +
					"consecCoaseguro," +
					"claveEstado," +
					"zonaSismica," +
					"numeroPisos," +
					"esIndustrial," +
					"claveMunicipio," +
					"codigoPostal," +
					"longitud," +
					"latitud," +
					"ediSuelo," +
					"ediFechaConstruccion," +
					"ediUso," +
					"estColumnas," +
					"estTrabes," +
					"estMuros," +
					"estCubierta," +
					"estClaros," +
					"estMurosPre," +
					"estContraventeo," +
					"otrColumnasCortas," +
					"otrSobrepeso," +
					"otrGolpeteo," +
					"otrEsquina," +
					"otrIrreElevacion," +
					"otrIrrePlanta," +
					"otrHundimientos," +
					"otrDaPrevios," +
					"otrDaReparado," +
					"otrReforzada," +
					"otrFecha," +
					"valorAsegurable," +
					"valorRetenido," +
					"prima," +
					"cedida," +
					"retenida," +
					"moneda," +
					"rsrt," +
					"ofiEmi" ,
					
					"id_registro," +
					"num_poliza," +
					"num_registro," +
					"fecha_inicio," +
					"fecha_fin," +
					"inm_valor_asegurable," +
					"inm_porcentaje_retencion," +
					"inm_limite_maximo," +
					"inm_deducible," +
					"inm_coaseguro," +
					"cont_valor_asegurable," +
					"cont_porcentaje_retencion," +
					"cont_limite_maximo," +
					"cont_deducible," +
					"cont_coaseguro," +
					"consec_valor_asegurable," +
					"consec_porcentaje_retencion," +
					"consec_limite_maximo," +
					"consec_deducible," +
					"consec_coaseguro," +
					"clave_estado," +
					"zona_sismica," +
					"num_pisos," +
					"es_industrial," +
					"clave_municipio," +
					"codigo_postal," +
					"longitud," +
					"latitud," +
					"edi_suelo," +
					"edi_fecha_construccion," +
					"edi_uso," +
					"est_columnas," +
					"est_trabes," +
					"est_muros," +
					"est_cubierta," +
					"est_claros," +
					"est_muros_pre," +
					"est_contraventeo," +
					"otr_columnas_cortas," +
					"otr_sobrepeso," +
					"otr_golpeteo," +
					"otr_esquina," +
					"otr_irre_elevacion," +
					"otr_irre_planta," +
					"otr_hundimientos," +
					"otr_da_previos," +
					"otr_da_reparado," +
					"otr_reforzada," +
					"otr_fecha," +
					"valor_asegurable," +
					"valor_retenido," +
					"prima," +
					"cedida," +
					"retenida," +
					"moneda," +
					"rsr_t," +
					"ofi_emi");

			
			storedHelper.estableceParametro("pFechaCorte", filtroReporte.getFechaCorte());
			storedHelper.estableceParametro("pTipoCambio", filtroReporte.getTipoCambio());
						
			return storedHelper.obtieneListaResultados();
			
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"pkgDAN_Reportes.spDAN_RepPMLTEV", ReportePMLTEVDTO.class, codErr, descErr);
			throw e;
		} catch (Exception e) {
			throw e;
		}
	}

}
