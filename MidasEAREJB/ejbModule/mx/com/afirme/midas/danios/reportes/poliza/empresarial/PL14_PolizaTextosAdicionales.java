package mx.com.afirme.midas.danios.reportes.poliza.empresarial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.SystemException;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaPolizaBase;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import net.sf.jasperreports.engine.JRException;

public class PL14_PolizaTextosAdicionales extends PlantillaPolizaBase{

	public PL14_PolizaTextosAdicionales(CotizacionDTO cotizacionDTO,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,reporteBase);
		super.setNombrePlantilla(Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.poliza.reporte.textosAdicionales"));
		setPaquetePlantilla(Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.paquete"));
	}
	
	public PL14_PolizaTextosAdicionales(CotizacionDTO cotizacionDTO,Map<String,Object> mapaParametros,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,mapaParametros,reporteBase);
		super.setNombrePlantilla(Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.poliza.reporte.textosAdicionales"));
		setPaquetePlantilla(Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.paquete"));
	}

	public byte[] obtenerReporte(String nombreUsuario) throws SystemException {
		procesarDatosReporte(nombreUsuario);
		return getByteArrayReport();
	}
	
	private void procesarDatosReporte(String claveUsuario) throws SystemException {
		if (this.cotizacionDTO != null){
			if (getParametrosVariablesReporte() == null){
				super.generarParametrosComunes(cotizacionDTO, claveUsuario);
			}
			List<TexAdicionalCotDTO> listaTextos = consultarTextosAdicionales();
			
			setListaRegistrosContenido(new ArrayList<Object>());
			
			if (listaTextos.isEmpty()){
				setByteArrayReport(null);
				generarLogPlantillaSinDatosParaMostrar();
				return;
			}
			else
				getListaRegistrosContenido().addAll(listaTextos);
		    try {
				super.setByteArrayReport( generaReporte(ConstantesReporte.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()));
			} catch (JRException e) {
				setByteArrayReport( null );
			}
		}
		else setByteArrayReport( null );
	}
}
