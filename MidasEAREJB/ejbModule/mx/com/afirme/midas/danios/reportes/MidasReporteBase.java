package mx.com.afirme.midas.danios.reportes;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.SystemException;
import net.sf.jasperreports.engine.util.JRProperties;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.SimpleBookmark;

public abstract class MidasReporteBase{
	
	protected List<byte[]> listaPlantillas;
	protected Map<String,Object> mapaParametrosGeneralesPlantillas;

	{
		setListaPlantillas(new ArrayList<byte[]>());
		setMapaParametrosGeneralesPlantillas(new java.util.HashMap<String, Object>());
	}
	
	public MidasReporteBase (){
		setListaPlantillas(new ArrayList<byte[]>());
		
		JRProperties.setProperty("net.sf.jasperreports.query.executer.factory.plsql", "org.jasperforge.jaspersoft.demo.PlSqlQueryExecuterFactory");
		JRProperties.setProperty("net.sf.jasperreports.awt.ignore.missing.font", "true");
		JRProperties.setProperty("net.sf.jasperreports.default.pdf.font.name", "Helvetica");
		JRProperties.setProperty("net.sf.jasperreports.default.font.name", "SansSerif");
		JRProperties.setProperty("net.sf.jasperreports.default.font.size", "10");
		
	}
	
	public byte[] obtenerReporte(String claveUsuario,boolean agregaPiePagina) throws SystemException,javax.transaction.SystemException {
		if(agregaPiePagina)
			return obtenerReporte(claveUsuario);
		if (listaPlantillas == null )
			throw new SystemException("Error al generar reporte. No se han obtenido los datos para generar el reporte.",40);
		if (listaPlantillas.isEmpty())
			throw new SystemException("Error al generar reporte. No se han obtenido los datos para generar el reporte.",40);
		
		ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
		concatenarReportes(outputByteArray);
		return (outputByteArray.toByteArray());
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException,javax.transaction.SystemException {
		if (listaPlantillas == null )
			throw new SystemException("Error al generar reporte. No se han obtenido los datos para generar el reporte.",40);
		if (listaPlantillas.isEmpty())
			throw new SystemException("Error al generar reporte. No se han obtenido los datos para generar el reporte.",40);
		
		ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
		concatenarReportes(outputByteArray);
		byte byteArrayReporte[] = outputByteArray.toByteArray();
		ByteArrayOutputStream outputByteArrayReporte = new ByteArrayOutputStream();
		agregarPieDePagina(byteArrayReporte,outputByteArrayReporte,claveUsuario);
		return (outputByteArrayReporte.toByteArray());
	}
	
	private void agregarPieDePagina(byte[] reporte,ByteArrayOutputStream byteOutput,String claveUsuario){
		try{
            PdfReader reader = new PdfReader(reporte);
            PdfStamper stamper = new PdfStamper(reader, byteOutput);
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA,BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
//            Usuario usuarioCot = null;
//            DummyFiller dummy = new DummyFiller();
//            try{
//            	usuarioCot = dummy.obtieneUsuarioPorId(Integer.valueOf(claveUsuario).intValue());
//            }catch(NumberFormatException e){
//            	usuarioCot = dummy.obtieneUsuarioPorNombreUsuario(claveUsuario);
//            }
//            String iniciales = "";
//            if (usuarioCot != null){
//            	try{
//	            	String nombres[] = usuarioCot.getNombre().split(" ");
//	            	for (int j=0;j<nombres.length;j++)
//	            		if (nombres[j] != null & nombres[j].length()>0)
//	            			iniciales += nombres[j].toUpperCase().charAt(0);
//            	}catch(Exception e){}
//            	if (usuarioCot.getApellidoPaterno() != null && usuarioCot.getApellidoPaterno().length()>0)
//            		iniciales += usuarioCot.getApellidoPaterno().toUpperCase().charAt(0);
//            	if (usuarioCot.getApellidoMaterno() != null && usuarioCot.getApellidoMaterno().length()>0)
//            		iniciales += usuarioCot.getApellidoMaterno().toUpperCase().charAt(0);
//            }
            int i=1;
            while(i <= reader.getNumberOfPages()){
            	PdfContentByte over;
            	over = stamper.getOverContent(i);
                over.saveState();
                over.beginText();
                over.moveText(520, 20);
                over.setFontAndSize(bf, 9);
                over.showText("P�gina "+i+" de "+reader.getNumberOfPages());
                over.endText();
                
                over.beginText();
                over.moveText(20, 20);
                over.setFontAndSize(bf, 9);
                over.showText(claveUsuario);
                over.endText();
                i++;
            }
            stamper.close();
        } catch (IOException e) {
                e.printStackTrace();
        } catch (DocumentException e) {
                e.printStackTrace();
        }
	}
	
	@SuppressWarnings("unchecked")
	protected void concatenarReportes(OutputStream outputStream) {
		if (listaPlantillas.size()>1){
			try {
				int pageOffset = 0;
				int f = 0;
				List<byte[]> master = new ArrayList<byte[]>();
				Document document = null;
				PdfCopy writer = null;
				Iterator iterator = listaPlantillas.iterator();
				while(iterator.hasNext()){
					byte[] data = (byte[])iterator.next();
					if(data == null){
						f++;
						continue;
					}
					PdfReader reader = new PdfReader(data);
					reader.consolidateNamedDestinations();
					// we retrieve the total number of pages
					int n = reader.getNumberOfPages();
					List bookmarks = SimpleBookmark.getBookmark(reader);
					if (bookmarks != null) {
						if (pageOffset != 0)
							SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
						master.addAll(bookmarks);
					}
					pageOffset += n;
					if (f == 0) {
						// step 1: creation of a document-object
						document = new Document(reader.getPageSizeWithRotation(1));
						// step 2: we create a writer that listens to the document
						writer = new PdfCopy(document, outputStream);
						// step 3: we open the document
						document.open();
					}
					// step 4: we add content
					PdfImportedPage page;
					for (int i = 0; i < n; ) {
						++i;
						page = writer.getImportedPage(reader, i);
						writer.addPage(page);
					}
					PRAcroForm form = reader.getAcroForm();
					if (form != null){
						writer.copyAcroForm(reader);
					}
					f++;
				}
				if (!master.isEmpty()){
					writer.setOutlines(master);
				}
				// step 5: we close the document
				if(document != null){
					document.close();
				}
			}
			catch(Exception e) {}
		} else{
			try {
				outputStream.write(listaPlantillas.get(0));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public List<byte[]> getListaPlantillas() {
		return listaPlantillas;
	}

	public void setListaPlantillas(List<byte[]> listaPlantillas) {
		this.listaPlantillas = listaPlantillas;
	}

	public Map<String, Object> getMapaParametrosGeneralesPlantillas() {
		return mapaParametrosGeneralesPlantillas;
	}

	public void setMapaParametrosGeneralesPlantillas(
			Map<String, Object> mapaParametrosGeneralesPlantillas) {
		this.mapaParametrosGeneralesPlantillas = mapaParametrosGeneralesPlantillas;
	}
}