package mx.com.afirme.midas.danios.reportes.poliza.empresarial;

import java.math.BigDecimal;
import java.util.Map;

import javax.transaction.SystemException;

import mx.com.afirme.midas.danios.reportes.PlantillaPolizaBase;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;

public class PL1_PolizaSumasAseguradas1erRiesgoLUC extends PlantillaPolizaBase{

	public PL1_PolizaSumasAseguradas1erRiesgoLUC(BigDecimal idToCotizacion,Map<String,Object> hasHtableParametros,ReporteCotizacionBase reporteBase) {
		super(idToCotizacion,hasHtableParametros,reporteBase);
		setNombrePlantilla( Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.poliza.reporte.primerRiesgo.objetos") );
		setPaquetePlantilla( Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.paquete") );
	}

	public byte[] obtenerReporte(String nombreUsuario) throws SystemException {
		procesarDatosPlantilla1erRiesgoLuc(nombreUsuario,false);
		return getByteArrayReport();
	}
}
