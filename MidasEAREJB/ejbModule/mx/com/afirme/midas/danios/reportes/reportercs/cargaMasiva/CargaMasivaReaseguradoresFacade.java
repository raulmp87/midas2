package mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;


import mx.com.afirme.midas.catalogos.reaseguradorcnsf.ReaseguradorCargaCNSF;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless 
public class CargaMasivaReaseguradoresFacade implements
CargaMasivaReaseguradoresFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private CargaMasivaReaseguradoresFacadeRemote cargaMasivaReaseguradoresFacadeRemote;
	
	java.math.BigDecimal cerAux = new java.math.BigDecimal(0);
	
	public void save(ReaseguradorCargaCNSF entity) {
		LogDeMidasEJB3.log("saving ReaseguradorCargaCNSF instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void delete(ReaseguradorCargaCNSF entity) {
		LogDeMidasEJB3.log("deleting ReaseguradorCargaCNSF instance",
				Level.INFO, null);
		try { 
			ReaseguradorCargaCNSF entity2 = entityManager.getReference(ReaseguradorCargaCNSF.class,
					entity.getIdReaseguradorCargaCNSF());
			entityManager.remove(entity2);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}
			
	public void agregar(
			List<ReaseguradorCargaCNSF> direccionesValidas) {
		LogDeMidasEJB3.log("agregando instancia de CargaMasivaCotDTO",
				Level.INFO, null);
		try {
			
			this.insertarEsquemas(
					direccionesValidas); 
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
		 
	}
	
		
	public void insertarEsquemas(
			List<ReaseguradorCargaCNSF> direcciones) {
		cerAux = new java.math.BigDecimal(0);
		for (ReaseguradorCargaCNSF direccion : direcciones) {
			insertaEsquema(direccion);		 
		}

	}
	
	private ReaseguradorCargaCNSF insertaEsquema(ReaseguradorCargaCNSF esquema) {
	
		cargaMasivaReaseguradoresFacadeRemote.save(esquema);

		return null;
		
	}
	
	@Override
	public int delete(String arg0, String cveNegocio) {
		LogDeMidasEJB3.log( 
				"finding EsquemasDTO instance with property: anio"
						+  ", value: " + arg0, Level.INFO, null);
		try {
			final String queryString = "delete from EsquemasDTO model where model.aut = 0 and model.anio"
					 + "= :propertyValue" +
					 " and model.cveNegocio = :proper_cveNegocio";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", Integer.parseInt(arg0));
			query.setParameter("proper_cveNegocio", Integer.parseInt(cveNegocio));
			return query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
    public List<ReaseguradorCargaCNSF> findByPropertyID(BigDecimal value
        ) {
    				LogDeMidasEJB3.log("finding ReaseguradorCargaCNSF instance with property: " +  ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from CalificacionAgenciaDTO model where model.id" 
			 						 + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
}