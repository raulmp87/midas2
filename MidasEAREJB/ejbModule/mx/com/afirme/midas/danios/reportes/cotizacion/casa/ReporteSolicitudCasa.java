package mx.com.afirme.midas.danios.reportes.cotizacion.casa;

import java.util.ArrayList;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteSolicitudCasa extends ReporteCotizacionBase{

	public ReporteSolicitudCasa(CotizacionDTO cotizacionDTO) throws javax.transaction.SystemException {
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		this.cotizacionDTO = cotizacionDTO;
		setListaPlantillas(new ArrayList<byte[]>());
		setMapaParametrosGeneralesPlantillas(new java.util.HashMap<String, Object>());
	}
	
	
	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException, javax.transaction.SystemException {
		generarReporte(claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioCotizacion());
	}
	
	private void generarReporte(String nombreUsuario) throws SystemException{
		super.poblarDatosContratante(nombreUsuario);
		super.poblarDatosBeneficiario(nombreUsuario);
		//Recuperar incisos, subincisos, coberturas, etc. de la cotizacion
		consultarInformacionCotizacion(nombreUsuario);
		poblarParametrosComunes(nombreUsuario,true);
		
		for(IncisoCotizacionDTO incisoCotEnCurso : super.getListaIncisos()){
			byte[] reporteTMP = null;
			
			PlantillaSolicitudCasa plantillaSolicitudCasa = new PlantillaSolicitudCasa(cotizacionDTO, incisoCotEnCurso, getMapaParametrosGeneralesPlantillas(), this);
			
			try {
				plantillaSolicitudCasa.setMostrarLeyendaSANivelCobertura(Boolean.FALSE);
				reporteTMP = plantillaSolicitudCasa.obtenerReporte(nombreUsuario);
			} catch (SystemException e) {} 
			catch (javax.transaction.SystemException e) {}
			if (reporteTMP !=null)
				getListaPlantillas().add(reporteTMP);
		}
		
	}
}
