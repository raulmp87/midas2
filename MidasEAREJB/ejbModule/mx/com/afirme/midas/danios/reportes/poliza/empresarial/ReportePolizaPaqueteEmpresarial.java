package mx.com.afirme.midas.danios.reportes.poliza.empresarial;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.poliza.ReportePolizaBase;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class ReportePolizaPaqueteEmpresarial extends ReportePolizaBase{
	
	public ReportePolizaPaqueteEmpresarial(BigDecimal idToPoliza) throws SystemException, javax.transaction.SystemException {
		setListaPlantillas(new ArrayList<byte[]>());
		polizaDTO = polizaFacade.findById(idToPoliza);
		cotizacionDTO = polizaDTO.getCotizacionDTO();
		mapaSeccionesContratadasPorNumeroInciso = null;
	}
	
	public ReportePolizaPaqueteEmpresarial(PolizaDTO polizaDTO) throws SystemException, javax.transaction.SystemException{
		setListaPlantillas(new ArrayList<byte[]>());
		this.polizaDTO = polizaDTO;
		this.cotizacionDTO = polizaDTO.getCotizacionDTO();
		mapaSeccionesContratadasPorNumeroInciso = null;
	}
	
	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException, javax.transaction.SystemException {
		generarReportePaqueteEmpresarial(polizaDTO, claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioEmision());
	}
	
	private void generarReportePaqueteEmpresarial(PolizaDTO polizaDTO,String nombreUsuario) throws SystemException, javax.transaction.SystemException{
		setMapaParametrosGeneralesPlantillas(new HashMap<String,Object>());
		try {
			CotizacionDTO cotizacionDTO = polizaDTO.getCotizacionDTO();
			consultarInformacionCotizacion(nombreUsuario);
			
			//Poblar los par�metros comunes para todas las plantillas
			poblarParametrosComunes(polizaDTO,nombreUsuario,listaIncisos,null);
			String abreviaturas,descripcionAbreviaturas;
			AgrupacionCotDTO agrupacionCot = agrupacionCotizacionFacade.buscarPorCotizacion(cotizacionDTO.getIdToCotizacion(), (short)1);
			//plantilla Unica ubicacion
			if (listaIncisos.size() == 1 && agrupacionCot == null){
				abreviaturas = "S/VR Eq. Da�.\nS.A.\nS/S.A.\nMIN\nMAX\nDSMGVDFMS\nUMA\nS/P\nAMPARADO:\n\nLUC\n\nCOBERTURAS\n NORMALES:";
				descripcionAbreviaturas = "Sobre Valor de Reposici�n del Equipo Da�ado\nSuma Asegurada\nSobre Suma Asegurada\nCon m�nimo de:\nCon m�ximo de:\n" +
					"D�as de Salario M�nimo General Vigente en el Distrito Federal al momento del siniestro\nUnidad de Medida y Actualización\nSobre p�rdida\nLa suma asegurada " +
					"para esta cobertura es el valor establecido en la cobertura b�sica del Bien o Secci�n contratado, salvo los subl�mites establecidos\nL�mite �nico y Combinado  " +
					"(L�mite m�ximo de responsabilidad para la Instituci�n, por uno o todos los siniestros que puedan ocurrir por una o " +
					"todas las ubicaciones aseguradas, durante la vigencia del seguro)\n\nIncendio y/o rayo y extensi�n de cubierta cuando sean  contratadas.";
				getMapaParametrosGeneralesPlantillas().put("ABREVIATURAS", abreviaturas);
				getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_ABREVIATURAS", descripcionAbreviaturas);
				generarReporteUnicaUbicacion(cotizacionDTO, listaIncisos,nombreUsuario);
			}
			else{
				//Si aplic� primer riesgo
				if (agrupacionCot != null){
					abreviaturas = "S.A.\nS/S.A.\nR.C.\nS/VR Eq. Da�.\nS/P\nMIN\nMAX\nDSMGVDFMS\nUMA\nAMPARADO:\n\nCOBERTURAS\n NORMALES:";
					descripcionAbreviaturas = "Suma Asegurada\nSobre Suma Asegurada\nResponsabilidad Civil\nSobre Valor de Reposici�n del Equipo Da�ado\nSobre p�rdida\nCon m�nimo de:\n" +
							"Con m�ximo de:\nD�as de Salario M�nimo General Vigente en el Distrito Federal al momento del siniestro\nUnidad de Medida y Actualización\nComplementa la " +
							"protecci�n del bien especificado en la Secci�n.\nL�mite �nico y Combinado  (L�mite m�ximo de responsabilidad para la " +
							"Instituci�n, por uno o todos los siniestros que puedan ocurrir por una o todas las ubicaciones aseguradas, " +
							"durante la vigencia del seguro)\n\nComprende las coberturas de Incendio y/o rayo y extensi�n de cubierta cuando sean  contratadas.";
				}
				else{
					abreviaturas = "S/VR Eq. Da�.\nS.A.\nS/S.A.\nMIN\nMAX\nDSMGVDFMS\nUMA\nS/P\nAMPARADO:\n\nLUC\n\nCOBERTURAS\n NORMALES:";
					descripcionAbreviaturas = "Sobre Valor de Reposici�n del Equipo Da�ado\nSuma Asegurada\nSobre Suma Asegurada\nCon m�nimo de:\nCon m�ximo de:\n" +
							"D�as de Salario M�nimo General Vigente en el Distrito Federal al momento del siniestro\nUnidad de Medida y Actualización\nSobre p�rdida\nLa suma asegurada " +
							"para esta cobertura es el valor establecido en la cobertura b�sica del Bien o Secci�n contratado, salvo los subl�mites establecidos\nL�mite �nico y Combinado  " +
							"(L�mite m�ximo de responsabilidad para la Instituci�n, por uno o todos los siniestros que puedan ocurrir por una o " +
							"todas las ubicaciones aseguradas, durante la vigencia del seguro)\n\nIncendio y/o rayo y extensi�n de cubierta cuando sean  contratadas.";
				}
				getMapaParametrosGeneralesPlantillas().put("ABREVIATURAS", abreviaturas);
				getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_ABREVIATURAS", descripcionAbreviaturas);
				generarReporteMultiplesUbicaciones(cotizacionDTO, listaIncisos, nombreUsuario);
			}
		} catch (SystemException e) {}
	}
	
	private void generarReporteMultiplesUbicaciones(CotizacionDTO cotizacionDTO,List<IncisoCotizacionDTO> listaIncisos,String nombreUsuario) throws SystemException, javax.transaction.SystemException{
		byte[] reporteTMP = null;
		MidasPlantillaBase plantillaPrimerRiesgo = new PL1_PolizaSumasAseguradas1erRiesgoLUC(cotizacionDTO.getIdToCotizacion(),getMapaParametrosGeneralesPlantillas(),this);
		try {
			reporteTMP = plantillaPrimerRiesgo.obtenerReporte(nombreUsuario);
		} catch (javax.transaction.SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		
		MidasPlantillaBase plantillaDocAnexos = new PL2_PolizaDocumentosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas(),this);
		try {
			reporteTMP = plantillaDocAnexos.obtenerReporte(nombreUsuario);
		} catch (javax.transaction.SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		plantillaDocAnexos = null;
		MidasPlantillaBase plantillaTextosAdicionales = new PL14_PolizaTextosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas(),this);
		try {
			reporteTMP = plantillaTextosAdicionales.obtenerReporte(nombreUsuario);
		} catch (javax.transaction.SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		plantillaTextosAdicionales = null;
		for(IncisoCotizacionDTO incisoCot : listaIncisos){
			MidasPlantillaBase plantillaDetalleInciso = new PL3_PolizaDetalleInciso(cotizacionDTO,incisoCot,getMapaParametrosGeneralesPlantillas(),this);
			try {
				reporteTMP = plantillaDetalleInciso.obtenerReporte(nombreUsuario);
			} catch (javax.transaction.SystemException e1) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
			//Se debe obtener la lista de secciones de cada inciso para generar el reporte de subincisos
			List<SeccionCotizacionDTO> listaSeccionCotContratadas = this.obtenerSeccionesContratadas(incisoCot.getId().getNumeroInciso()); //SeccionCotizacionDN.getInstancia().listarSeccionesContratadas(idToCotizacion, incisoCot.getId().getNumeroInciso());
			for(SeccionCotizacionDTO seccion : listaSeccionCotContratadas){
				MidasPlantillaBase plantilla4 = new PL4_PolizaDetalleSubincisoPorInciso(cotizacionDTO,incisoCot,seccion.getId().getIdToSeccion(),getMapaParametrosGeneralesPlantillas(),this);
				try {
					reporteTMP = plantilla4.obtenerReporte(nombreUsuario);
				} catch (javax.transaction.SystemException e) {}
				if (reporteTMP !=null){
					getListaPlantillas().add(reporteTMP);
					reporteTMP = null;
				}
			}
		}
	}
	
	private void generarReporteUnicaUbicacion(CotizacionDTO cotizacionDTO,List<IncisoCotizacionDTO> listaIncisos,String nombreUsuario) {
		byte[] reporteTMP = null;
		IncisoCotizacionDTO incisoCot = listaIncisos.get(0);
		PL6_PolizaUnicaUbicacion plantillaUnicaUbicacion = new PL6_PolizaUnicaUbicacion(cotizacionDTO,incisoCot,getMapaParametrosGeneralesPlantillas(),this);
		try {
			plantillaUnicaUbicacion.setMostrarLeyendaSANivelCobertura(Boolean.TRUE);
			reporteTMP = plantillaUnicaUbicacion.obtenerReporte(nombreUsuario);
		} catch (javax.transaction.SystemException e) {}
		if (reporteTMP !=null)
			getListaPlantillas().add(reporteTMP);
		reporteTMP = null;
		//Se debe obtener la lista de secciones de cada inciso para generar el reporte de subincisos
		List<SeccionCotizacionDTO> listaSeccionCotContratadas = this.obtenerSeccionesContratadas(incisoCot.getId().getNumeroInciso()); //SeccionCotizacionDN.getInstancia().listarSeccionesContratadas(idToCotizacion, incisoCot.getId().getNumeroInciso());
		for(SeccionCotizacionDTO seccion : listaSeccionCotContratadas){
			MidasPlantillaBase plantilla4 = new PL4_PolizaDetalleSubincisoPorInciso(cotizacionDTO,incisoCot,seccion.getId().getIdToSeccion(),getMapaParametrosGeneralesPlantillas(),this);
			try {
				reporteTMP = plantilla4.obtenerReporte(nombreUsuario);
			} catch (javax.transaction.SystemException e) {
			} catch(SystemException e){}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
		}
		MidasPlantillaBase plantillaDocAnexos = new PL2_PolizaDocumentosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas(),this);
		try {
			reporteTMP = plantillaDocAnexos.obtenerReporte(nombreUsuario);
		} catch (javax.transaction.SystemException e) {
		} catch(SystemException e){}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		MidasPlantillaBase plantillaTextosAdicionales = new PL14_PolizaTextosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas(),this);
		try {
			reporteTMP = plantillaTextosAdicionales.obtenerReporte(nombreUsuario);
		} catch (javax.transaction.SystemException e) {
		} catch(SystemException e){}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
	}
	
}
