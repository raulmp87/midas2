package mx.com.afirme.midas.danios.reportes.reportercs.documento;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class DocumentoDigitalRangosFacade implements
		DocumentoDigitalRangosFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	public void save(DocumentoDigitalRangosDTO entity) {
		LogDeMidasEJB3.log("saving DocumentoDigitalRangosDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	public void delete(DocumentoDigitalRangosDTO entity) {
		LogDeMidasEJB3.log("deleting DocumentoDigitalRangosDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					DocumentoDigitalRangosDTO.class, entity
							.getIdToDocumentoDigitalSolicitud());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DocumentoDigitalRangosDTO update(
			DocumentoDigitalRangosDTO entity) {
		LogDeMidasEJB3.log("updating DocumentoDigitalRangosDTO instance",
				Level.INFO, null);
		try {
			DocumentoDigitalRangosDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DocumentoDigitalRangosDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding DocumentoDigitalRangosDTO instance with id: " + id,
				Level.INFO, null);
		try {
			DocumentoDigitalRangosDTO instance = entityManager.find(
					DocumentoDigitalRangosDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DocumentoDigitalRangosDTO> findByProperty(
			String propertyName, final Object value) {
		LogDeMidasEJB3.log(
				"finding DocumentoDigitalRangosDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from DocumentoDigitalRangosDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value+"");
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DocumentoDigitalRangosDTO> findAll() {
		LogDeMidasEJB3.log(
				"finding all DocumentoDigitalRangosDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from DocumentoDigitalRangosDTO model order by model.fechaCreacion desc";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}