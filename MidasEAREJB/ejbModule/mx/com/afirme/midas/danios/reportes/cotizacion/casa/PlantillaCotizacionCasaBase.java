package mx.com.afirme.midas.danios.reportes.cotizacion.casa;

import java.math.BigDecimal;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;

import org.apache.commons.lang.NullArgumentException;

public abstract class PlantillaCotizacionCasaBase extends PlantillaCotizacionBase {

	public PlantillaCotizacionCasaBase(BigDecimal idToCotizacion,
			Map<String, Object> mapaParametrosGenerales,
			ReporteCotizacionBase reporteBase) throws NullArgumentException {
		super(idToCotizacion, mapaParametrosGenerales, reporteBase);
	}
	
	public PlantillaCotizacionCasaBase(CotizacionDTO cotizacionDTO,
			IncisoCotizacionDTO incisoCotizacionDTO,
			Map<String, Object> mapaParametrosGenerales,
			ReporteCotizacionBase reporteBase) throws NullArgumentException {
		super(cotizacionDTO,incisoCotizacionDTO, mapaParametrosGenerales, reporteBase);
	}
	
	/**
	 * M�todo que recupera informaci�n espec�fica del IncisoCotizacionDTO para el producto de Casa recibido en el constructor.
	 * La informaci�n que recupera es guardada en el mapa de par�metros de la plantilla. Consiste en los siguientes datos:
	 * TIPO_VIVIENDA, TIPO_USO_VIVIENDA, NIVEL_BIEN, NUMERO_PISOS
	 * @author Jose Luis Arellano
	 */
	protected void poblarParametrosPlantillaDatosGeneralesInciso(){
		
		super.poblarParametrosPlantillaDatosGeneralesInciso();
		
		
		final String NO_DISPONIBLE = "No disponible";
		
		String datoInciso[] = null;
		
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = new DatoIncisoCotizacionDTO(
				new DatoIncisoCotizacionId(
						idToCotizacion, 
						BigDecimal.ONE, BigDecimal.ZERO, BigDecimal.ZERO, 
						new BigDecimal("6011"), BigDecimal.ZERO, 
						BigDecimal.ONE, BigDecimal.ONE, (short)0, new BigDecimal("100")), null);
		
		datoInciso = reporteBase.obtenerConfigDatoIncisoCotFacade().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO, null);
		
		if(datoInciso != null && datoInciso.length >= 2){
			getParametrosVariablesReporte().put("TIPO_VIVIENDA", datoInciso[1]);
		}
		else{
			getParametrosVariablesReporte().put("TIPO_VIVIENDA", NO_DISPONIBLE);
		}
		
		datoIncisoCotizacionDTO = new DatoIncisoCotizacionDTO(
				new DatoIncisoCotizacionId(
						idToCotizacion, 
						BigDecimal.ONE, BigDecimal.ZERO, BigDecimal.ZERO, 
						new BigDecimal("6011"), BigDecimal.ZERO, 
						BigDecimal.ONE, BigDecimal.ONE, (short)0, new BigDecimal("110")), null);
		
		datoInciso = reporteBase.obtenerConfigDatoIncisoCotFacade().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO, null);
		
		if(datoInciso != null && datoInciso.length >= 2){
			getParametrosVariablesReporte().put("TIPO_USO_VIVIENDA", datoInciso[1]);
		}
		else{
			getParametrosVariablesReporte().put("TIPO_USO_VIVIENDA", NO_DISPONIBLE);
		}
		
		datoIncisoCotizacionDTO = new DatoIncisoCotizacionDTO(
				new DatoIncisoCotizacionId(
						idToCotizacion, 
						BigDecimal.ONE, BigDecimal.ZERO, BigDecimal.ZERO, 
						new BigDecimal("6011"), BigDecimal.ZERO, 
						BigDecimal.ONE, BigDecimal.ONE, (short)0, new BigDecimal("120")), null);
		
		datoInciso = reporteBase.obtenerConfigDatoIncisoCotFacade().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO, null);
		
		if(datoInciso != null && datoInciso.length >= 2){
			getParametrosVariablesReporte().put("NIVEL_BIEN", datoInciso[1]);
		}
		else{
			getParametrosVariablesReporte().put("NIVEL_BIEN", NO_DISPONIBLE);
		}
		
	}

}
