package mx.com.afirme.midas.danios.reportes.poliza.casa;

import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.casa.PlantillaCotizacionCasaBase;

import org.apache.commons.lang.NullArgumentException;

/**
 * 
 * @author jose luis arellano
 */
public abstract class PlantillaPolizaCasaBase extends PlantillaCotizacionCasaBase {

	public PlantillaPolizaCasaBase(CotizacionDTO cotizacionDTO,
			IncisoCotizacionDTO incisoCotizacionDTO,
			Map<String, Object> mapaParametrosGenerales,
			ReporteCotizacionBase reporteBase) throws NullArgumentException {
		super(cotizacionDTO, incisoCotizacionDTO, mapaParametrosGenerales, reporteBase);
	}

}
