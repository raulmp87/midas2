package mx.com.afirme.midas.danios.reportes.cotizacion;

import java.math.BigDecimal;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.casa.ReporteCotizacionCasa;
import mx.com.afirme.midas.danios.reportes.cotizacion.casa.ReporteSolicitudCasa;
import mx.com.afirme.midas.danios.reportes.poliza.ReportePolizaBase;
import mx.com.afirme.midas.danios.reportes.poliza.casa.ReportePolizaCasa;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SystemException;

@Stateless
public class ReporteCotizacionServicios implements ReporteCotizacionServiciosRemote{

	@EJB
	private CotizacionFacadeRemote cotizacionFacade;
	
	@EJB
	private EndosoFacadeRemote endosoFacade;
	
	@EJB
	private PolizaFacadeRemote polizaFacade;
	
	@EJB
	private mx.com.afirme.midas.interfaz.poliza.PolizaFacadeRemote polizaFacadeInterfaz;
		
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public byte[] imprimirCotizacion(BigDecimal idToCotizacion,String nombreUsuario) throws SystemException {
		int tipoPlantilla = 0;
		try {
			CotizacionDTO cotizacionDTO = cotizacionFacade.findById(idToCotizacion);
			tipoPlantilla = calcularTipoPlantillaCotizacion(cotizacionDTO);
			byte[] byteArray = null;
//			boolean cotizacionPoliza = true;
			ReporteCotizacionBase reporteCotizacion = null;
			
			switch (tipoPlantilla){
				default:
					reporteCotizacion = new ReporteCotizacionCasa(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(nombreUsuario);
			}
			
			return byteArray;
//			this.writeBytes(response, byteArray, Sistema.TIPO_PDF, "Cot-"+(cotizacionPoliza?"Pol":"End")+"_"+reporteCotizacion.getNumeroCotizacion());
		} catch (SystemException e) {
			throw e;
		} catch (Exception e){
			LogDeMidasEJB3.log("Error al imprimir cotizacion: "+idToCotizacion, Level.SEVERE, e);
			throw new SystemException("Error al imprimir cotizacion: "+idToCotizacion);
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public byte[] imprimirSolicitud(BigDecimal idToCotizacion,String nombreUsuario) throws SystemException {
		int tipoPlantilla = 0;
		try {
			CotizacionDTO cotizacionDTO = cotizacionFacade.findById(idToCotizacion);
			tipoPlantilla = calcularTipoPlantillaCotizacion(cotizacionDTO);
			byte[] byteArray = null;
//			boolean cotizacionPoliza = true;
			ReporteCotizacionBase reporteCotizacion = null;
			
			switch (tipoPlantilla){
			//Solicitud Seguro casa

				default:
//					String error = "Error al definir el tipo de plantilla: "+tipoPlantilla+" cotizacion "+idToCotizacion;
//					LogDeMidasEJB3.log(error, Level.SEVERE, null);
//					throw new SystemException(error,20);
					reporteCotizacion = new ReporteSolicitudCasa(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(nombreUsuario);
			}
			
			return byteArray;
//			this.writeBytes(response, byteArray, Sistema.TIPO_PDF, "Cot-"+(cotizacionPoliza?"Pol":"End")+"_"+reporteCotizacion.getNumeroCotizacion());
		} catch (SystemException e) {
			throw e;
		} catch (Exception e){
			LogDeMidasEJB3.log("Error al imprimir cotizacion: "+idToCotizacion, Level.SEVERE, e);
			throw new SystemException("Error al imprimir cotizacion: "+idToCotizacion);
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public byte[] imprimirPoliza(BigDecimal idToCotizacionSeycos,String nombreUsuario) throws SystemException{
		/*
		 * Este servicio solo se usa para imprimir p�lizas de casa, por ahora
		 * la p�liza no se genera en Midas, pero s� la impresi�n en base a la informaci�n de la cotizaci�n.
		 * Por ahora el servicio recibe un id de cotizaci�n de Seycos, el cual regresar� el folio de la p�liza
		 * y el id de la cotizaci�n de Midas.
		 */
		int tipoPlantilla = 0;
		//Consultar el idCotizacion de Midas
		//TODO Remover esta linea cuando se est� generando el folio de la p�liza en Midas
		BigDecimal idToCotizacion = polizaFacadeInterfaz.obtenerIdToCotizacionDePolizaSeycos(idToCotizacionSeycos);
		
				
		try {
//			CotizacionDTO cotizacionDTO = cotizacionFacade.findById(idToCotizacion);
//			tipoPlantilla = calcularTipoPlantillaCotizacion(cotizacionDTO);
			byte[] byteArray = null;
//			boolean cotizacionPoliza = true;
			ReportePolizaCasa reporteCotizacion = null;
			
			switch (tipoPlantilla){
				default:
					reporteCotizacion = new ReportePolizaCasa(idToCotizacion);
					reporteCotizacion.setIdToCotizacionSeycos(idToCotizacionSeycos);
					byteArray = reporteCotizacion.obtenerReporte(nombreUsuario);
			}
			
			return byteArray;
		} catch (SystemException e) {
			throw e;
		} catch (Exception e){
			LogDeMidasEJB3.log("Error al imprimir cotizacion: "+idToCotizacion, Level.SEVERE, e);
			throw new SystemException("Error al imprimir cotizacion: "+idToCotizacion,e);
		}
	}
	
	public byte[] imprimirEndoso(BigDecimal idToPoliza,Short numeroEndoso,String nombreUsuario) throws SystemException{
		int tipoPlantilla = 0;
		try {
			PolizaDTO polizaDTO;
			CotizacionDTO cotizacionDTO;
			EndosoDTO endosoDTO ;
			if (numeroEndoso == null || numeroEndoso.intValue() == 0){
				polizaDTO = polizaFacade.findById(idToPoliza);
				cotizacionDTO = polizaDTO.getCotizacionDTO();
			}
			else{
				EndosoId endosoId = new EndosoId();
				endosoId.setIdToPoliza(idToPoliza);
				endosoId.setNumeroEndoso(numeroEndoso);
				endosoDTO = endosoFacade.findById(endosoId);
				polizaDTO = endosoDTO.getPolizaDTO();
				cotizacionDTO = cotizacionFacade.findById(endosoDTO.getIdToCotizacion());
			}
			tipoPlantilla = calcularTipoPlantillaCotizacion(cotizacionDTO);
			byte[] byteArray = null;
			ReportePolizaBase reportePoliza;
			switch (tipoPlantilla){
			//Seguro paquete empresarial
//				case ConstantesReporte.SEGURO_EMPRESARIAL:
//					reportePoliza = new ReportePolizaPaqueteEmpresarial(polizaDTO);
//					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
//					break;
//			//Transportes
//				case ConstantesReporte.TRANSPORTES:
//					//plantilla "Cotizaci�n Transportes"
//					reportePoliza = new ReportePolizaTransportes(polizaDTO);
//					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
//					break;
//			//Generica
//				case ConstantesReporte.GENERICA:
//					reportePoliza = new ReportePolizaGenerica(polizaDTO);
//					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
//					break;
//				case ConstantesReporte.ENDOSO:
//					reportePoliza =new ReportePolizaEndoso(polizaDTO,cotizacionDTO,numeroEndosoShort);
//					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
//					break;
//				case ConstantesReporte.SEGURO_FAMILIAR:
//					reportePoliza = new ReportePolizaPaqueteFamiliar(polizaDTO);
//					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
//					break;
//				case ConstantesReporte.ENDOSO_CANCELACION:
//					reportePoliza =new ReportePolizaEndosoCancelacion(polizaDTO,cotizacionDTO,numeroEndosoShort);
//					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
//					break;
//				case ConstantesReporte.ENDOSO_REHABILITACION:
//					reportePoliza =new ReportePolizaEndosoRehabilitacion(polizaDTO,cotizacionDTO,numeroEndosoShort);
//					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
//					break;
//				case ConstantesReporte.RC_FUNCIONARIOS:
//					reportePoliza =new ReportePolizaRCFuncionarios(polizaDTO,cotizacionDTO,numeroEndosoShort);
//					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
//					break;
//				case ConstantesReporte.RC_MEDICOS://06/01/2011 RC Medico se imprime con el mismo formato que RC Funcionarios
//					reportePoliza =new ReportePolizaRCFuncionarios(polizaDTO,cotizacionDTO,numeroEndosoShort);
//					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
//					break;
//				case ConstantesReporte.RC_PROFESIONAL_MEDICOS://06/01/2011 RC Medico se imprime con el mismo formato que RC Funcionarios
//					reportePoliza =new ReportePolizaRCFuncionarios(polizaDTO,cotizacionDTO,numeroEndosoShort);
//					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
//					break;
				default:
					reportePoliza = new ReportePolizaCasa(polizaDTO);
					byteArray = reportePoliza.obtenerReporte(nombreUsuario);
//					String error = "Error al definir el tipo de plantilla: "+tipoPlantilla+" cotizacion "+id;
//					LogDeMidasWeb.log(error, Level.SEVERE, null);
//					throw new SystemException(error,20);
					
			}
		return byteArray;
		} /*catch (SystemException e) {
			throw e;
		}*/ catch (Exception e){
			String error = "Error al imprimir endoso. idToPoliza: "+idToPoliza+", numeroEndoso: "+numeroEndoso;
			LogDeMidasEJB3.log(error, Level.SEVERE, e);
			throw new SystemException(error);
		}
	}
	
	private int calcularTipoPlantillaCotizacion(CotizacionDTO cotizacionDTO){
		int tipo = 0;

		return tipo;
	}
}
