package mx.com.afirme.midas.danios.reportes.reportercs.esquemas;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;


@Stateless
public class DocumentoDigitalEsquemasFacade implements
		DocumentoDigitalEsquemasFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	
	public void save(DocumentoDigitalEsquemasDTO entity) {
		LogDeMidasEJB3.log("saving DocumentoDigitalEsquemasDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void delete(DocumentoDigitalEsquemasDTO entity) {
		LogDeMidasEJB3.log("deleting DocumentoDigitalEsquemasDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					DocumentoDigitalEsquemasDTO.class, entity
							.getIdToDocumentoDigitalSolicitud());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	
	public DocumentoDigitalEsquemasDTO update(
			DocumentoDigitalEsquemasDTO entity) {
		LogDeMidasEJB3.log("updating DocumentoDigitalEsquemasDTO instance",
				Level.INFO, null);
		try {
			DocumentoDigitalEsquemasDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DocumentoDigitalEsquemasDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding DocumentoDigitalEsquemasDTO instance with id: " + id,
				Level.INFO, null);
		try {
			DocumentoDigitalEsquemasDTO instance = entityManager.find(
					DocumentoDigitalEsquemasDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	
	@SuppressWarnings("unchecked")
	public List<DocumentoDigitalEsquemasDTO> findByProperty(
			String propertyName, final Object value) {
		LogDeMidasEJB3.log(
				"finding DocumentoDigitalEsquemasDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from DocumentoDigitalEsquemasDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DocumentoDigitalEsquemasDTO> findAll() {
		LogDeMidasEJB3.log(
				"finding all DocumentoDigitalEsquemasDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from DocumentoDigitalEsquemasDTO model order by model.fechaCreacion desc";
			Query query = entityManager.createQuery(queryString);
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}