package mx.com.afirme.midas.danios.reportes.poliza.empresarial;

import java.math.BigDecimal;
import java.util.Map;

import javax.transaction.SystemException;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaSubIncisosBase;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;

public class PL4_PolizaDetalleSubincisoPorInciso extends PlantillaSubIncisosBase{

	public PL4_PolizaDetalleSubincisoPorInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,BigDecimal idToSeccion,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,incisoCotizacionDTO,idToSeccion,reporteBase);
		inicializarDatosPlantilla();
	}
	
	public PL4_PolizaDetalleSubincisoPorInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,BigDecimal idToSeccion,Map<String,Object> mapaParametrosPlantilla,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,incisoCotizacionDTO,idToSeccion,mapaParametrosPlantilla,reporteBase);
		inicializarDatosPlantilla();
	}

	public byte[] obtenerReporte(String nombreUsuario) throws SystemException {
		super.poblarSubIncisos(nombreUsuario);
		return getByteArrayReport();
	}
	
	private void inicializarDatosPlantilla(){
		setNombrePlantilla( Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.poliza.reporte.subincisosPorInciso") );
		setPaquetePlantilla( Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.paquete") );
	}
}
