package mx.com.afirme.midas.danios.reportes.cotizacion.empresarial;

import java.math.BigDecimal;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaSubIncisosBase;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;

public class PL4DetalleSubincisoPorInciso extends PlantillaSubIncisosBase{

	public PL4DetalleSubincisoPorInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,BigDecimal idToSeccion,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,incisoCotizacionDTO,idToSeccion,reporteBase);
		inicializarDatosPlantilla();
	}
	
	public PL4DetalleSubincisoPorInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,BigDecimal idToSeccion,Map<String,Object> mapaParametrosPlantilla,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,incisoCotizacionDTO,idToSeccion,mapaParametrosPlantilla,reporteBase);
		inicializarDatosPlantilla();
	}

	public byte[] obtenerReporte(String nombreUsuario) throws SystemException, javax.transaction.SystemException {
		super.poblarSubIncisos(nombreUsuario);
		return getByteArrayReport();
	}
	
	private void inicializarDatosPlantilla(){
		setNombrePlantilla( Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.subincisosPorInciso") );
		setPaquetePlantilla( Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.cotizacionEmpresarial.paquete") );
	}
}
