package mx.com.afirme.midas.danios.reportes.comun.bean;

import java.io.Serializable;

public class LicitacionForm implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -360213515760842958L;
	private String descripcionLicitacion;
	private String sumaAsegurada;
	private String deducible;
	private String coaseguro;
	
	public String getDescripcionLicitacion() {
		return descripcionLicitacion;
	}
	public void setDescripcionLicitacion(String descripcionLicitacion) {
		this.descripcionLicitacion = descripcionLicitacion;
	}
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public String getDeducible() {
		return deducible;
	}
	public void setDeducible(String deducible) {
		this.deducible = deducible;
	}
	public String getCoaseguro() {
		return coaseguro;
	}
	public void setCoaseguro(String coaseguro) {
		this.coaseguro = coaseguro;
	}
}
