package mx.com.afirme.midas.danios.reportes.cotizacion.casa;

import java.util.Map;

import javax.transaction.SystemException;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import net.sf.jasperreports.engine.JasperReport;

import org.apache.commons.lang.NullArgumentException;

/**
 * @author jose luis arellano
 */
public class PlantillaSolicitudCasa extends PlantillaCotizacionCasaBase {

	public PlantillaSolicitudCasa(CotizacionDTO cotizacionDTO,IncisoCotizacionDTO incisoCotizacionDTO,
			Map<String, Object> mapaParametrosGenerales,ReporteCotizacionBase reporteBase) throws NullArgumentException {
		super(cotizacionDTO, incisoCotizacionDTO, mapaParametrosGenerales, reporteBase);
		inicializarDatosPlantilla();
	}

	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException,mx.com.afirme.midas.sistema.SystemException {
		generarPlantilla(claveUsuario);
		return getByteArrayReport();
	}

	private void generarPlantilla(String claveUsuario) throws SystemException{
		if (this.cotizacionDTO != null ){
			if (getParametrosVariablesReporte() == null){
				super.generarParametrosComunes(cotizacionDTO, claveUsuario);
			}
			poblarParametrosPlantillaDatosGeneralesInciso();
			
			super.poblarSeccionesPorInciso();
			
			if (getListaRegistrosContenido() == null || getListaRegistrosContenido().isEmpty()){
				setByteArrayReport(null);
				generarLogPlantillaSinDatosParaMostrar();
			}
			
//			Map<String,Object> parametrosReporteCoberturas = new HashMap<String,Object>();
			String nombrePlantillaSubReporte = getPaquetePlantilla()+ 
				Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.cotizacion.casa.plantilla.solicitud.subReporteCoberturas");
			JasperReport subReporteCoberturas = getJasperReport(nombrePlantillaSubReporte);
			if(subReporteCoberturas == null)
				generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte,null);
			getParametrosVariablesReporte().put("SUBREPORTE_COBERTURAS", subReporteCoberturas);
			
//			nombrePlantillaSubReporte = getPaquetePlantilla() + 
//				Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.unicaUbicacion.subReporteRiesgos");
//		    JasperReport subReporteRiesgos = getJasperReport(nombrePlantillaSubReporte);
//		    if(subReporteRiesgos == null)
//				generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte,null);
//			parametrosReporteCoberturas.put("SUBREPORTE_RIESGOS", subReporteRiesgos);
//			getParametrosVariablesReporte().put("PARAMETROS_SUBREPORTE_COBERTURA",parametrosReporteCoberturas);
		    
			finalizarReporte();
		}
	}
	
	private void inicializarDatosPlantilla(){
		super.setNombrePlantilla(Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.cotizacion.casa.plantilla.solicitud"));
		setPaquetePlantilla(Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.cotizacion.casa.plantilla.paquete"));
	}
}
