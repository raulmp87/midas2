package mx.com.afirme.midas.danios.reportes;

import java.math.BigDecimal;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;

import org.apache.commons.lang.NullArgumentException;

public abstract class PlantillaPolizaBase extends PlantillaCotizacionBase{

	public PlantillaPolizaBase(BigDecimal idToCotizacion,Map<String, Object> mapaParametrosGenerales,ReporteCotizacionBase reporteBase)throws NullArgumentException {
		super(idToCotizacion, mapaParametrosGenerales,reporteBase);
	}

	public PlantillaPolizaBase(CotizacionDTO cotizacionDTO,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,reporteBase);
	}
	
	public PlantillaPolizaBase(CotizacionDTO cotizacionDTO,Map<String,Object> mapaParametros,ReporteCotizacionBase reporteBase){
		super(cotizacionDTO,mapaParametros,reporteBase);
	}
	
	public PlantillaPolizaBase(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,ReporteCotizacionBase reporteBase){
		super(cotizacionDTO,incisoCotizacionDTO,reporteBase);
	}
	
	public PlantillaPolizaBase(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,Map<String,Object> mapaParametrosPlantilla,ReporteCotizacionBase reporteBase){
		super(cotizacionDTO,incisoCotizacionDTO,mapaParametrosPlantilla,reporteBase);
	}
}
