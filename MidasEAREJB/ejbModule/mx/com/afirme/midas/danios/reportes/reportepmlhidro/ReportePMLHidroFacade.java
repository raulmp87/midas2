package mx.com.afirme.midas.danios.reportes.reportepmlhidro;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.danios.reportes.reportepml.AtributoEntradaDTO_PML;
import mx.com.afirme.midas.danios.reportes.reportepml.ParametroSalidaSP_PML;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless
public class ReportePMLHidroFacade implements ReportePMLHidroFacadeRemote {
	public static final String SP_PMLHIDRO="MIDAS.pkgDAN_Reportes.spDAN_RepPMLHidro";
	public static final String SP_PMLHIDRO_INDEPENDIENTES ="MIDAS.pkgDAN_Reportes.spDAN_RepPMLHidro_Ind";
	public static final String SP_PMLHIDRO_SEMIAGR_INCISOS ="MIDAS.pkgDAN_Reportes.spDAN_RepPMLHidro_SemiAgrI";
	public static final String SP_PMLHIDRO_SEMIAGR_DATOS_GEN = "MIDAS.pkgDAN_Reportes.spDAN_RepPMLHidro_SemiAgrDG";
	public static final String SP_PMLHIDRO_SEMIAGR_DATOS_FIN = "MIDAS.pkgDAN_Reportes.spDAN_RepPMLHidro_SemiAgrDF";
	public static final String SP_PMLHIDRO_TECNICOS = "MIDAS.pkgDAN_Reportes.spDAN_RepPMLHidro_Tec";
	
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public boolean calcularReportePMLHidro(ReportePMLHidroDTO filtroReporte, String nombreUsuario){
		StoredProcedureHelper storedHelper = null;
		boolean resultado = false;
		String nombreSP = SP_PMLHIDRO;
		String []nombreParametros = {"pCodigoUsuario","pOpcion","pFechaCorte","pTipoCambio"};
		Object []valorParametros = new Object[4];
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			valorParametros[0] = nombreUsuario;
			valorParametros[1] = filtroReporte.getClaveTipoReporte();
			valorParametros[2] = filtroReporte.getFechaCorte();
			valorParametros[3] = filtroReporte.getTipoCambio();
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
						
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas.danios.reportes.reportepmlhidro.ReportePMLHidroDTO", "", "");
			
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			storedHelper.obtieneResultadoSencillo();
			resultado = true;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLHidroDTO.class, codErr, descErr);
			
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". Parametros enviados: "+nombreParametros[0]+"="+valorParametros[0]+", "
					+nombreParametros[1]+"="+valorParametros[1], Level.SEVERE, e);
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". Parametros enviados: "+nombreParametros[0]+"="+valorParametros[0]+", "
					+nombreParametros[1]+"="+valorParametros[1], Level.SEVERE, e);
		}
		return resultado;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroIndependientes(Integer claveTipoReporte,String nombreUsuario)throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = SP_PMLHIDRO_INDEPENDIENTES;
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			String []nombreParametros = {"pCodigoUsuario","pOpcion"};
			Object []valorParametros = {nombreUsuario,claveTipoReporte};
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			String[] atributosDTO = {
					AtributoEntradaDTO_PML.NUM_POLIZA
					,AtributoEntradaDTO_PML.NUM_REGISTRO
					,AtributoEntradaDTO_PML.FECHA_INICIO
					,AtributoEntradaDTO_PML.FECHA_FIN
					,AtributoEntradaDTO_PML.INM_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONT_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONSEC_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONVENIO_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.TIPO_PRIMER_RIESGO
					,AtributoEntradaDTO_PML.MONTO_PRIMER_RIESGO	
					,AtributoEntradaDTO_PML.CONSEC_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONSEC_PERIODO_COBERTURA
					,AtributoEntradaDTO_PML.CONVENIO_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.INM_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONT_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONSEC_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONVENIO_DEDUCIBLE
					,AtributoEntradaDTO_PML.INM_COASEGURO
					,AtributoEntradaDTO_PML.CONT_COASEGURO
					,AtributoEntradaDTO_PML.CONSEC_COASEGURO
					,AtributoEntradaDTO_PML.CONVENIO_COASEGURO
					,AtributoEntradaDTO_PML.CLAVE_ESTADO
					,AtributoEntradaDTO_PML.CODIGO_POSTAL
					,AtributoEntradaDTO_PML.LONGITUD
					,AtributoEntradaDTO_PML.LATITUD
					,AtributoEntradaDTO_PML.PRIMERA_LINEA_MAR
					,AtributoEntradaDTO_PML.PRIMERA_LINEA_LAGO
					,AtributoEntradaDTO_PML.SOBREELEVACION_DESPLANTE
					,AtributoEntradaDTO_PML.RUGOSIDAD
					,AtributoEntradaDTO_PML.USO_INMUEBLE
					,AtributoEntradaDTO_PML.NUM_PISOS
					,AtributoEntradaDTO_PML.PISO
					,AtributoEntradaDTO_PML.TIPO_CUBIERTA
					,AtributoEntradaDTO_PML.FORMA_CUBIERTA
					,AtributoEntradaDTO_PML.IRRE_PLANTA
					,AtributoEntradaDTO_PML.OBJETOS_CERCA
					,AtributoEntradaDTO_PML.AZOTEA
					,AtributoEntradaDTO_PML.TAMANO_CRISTAL
					,AtributoEntradaDTO_PML.TIPO_VENTANAS
					,AtributoEntradaDTO_PML.TIPO_DOMOS
					,AtributoEntradaDTO_PML.SOPORTE_VENTANA
					,AtributoEntradaDTO_PML.PROCENTAJE_CRISTAL_FACHADAS
					,AtributoEntradaDTO_PML.PORCENTAJE_DOMOS
					,AtributoEntradaDTO_PML.OTROS_FACHADA
					,AtributoEntradaDTO_PML.MUROS_CONTENCION
					,AtributoEntradaDTO_PML.PRIMA
					,AtributoEntradaDTO_PML.CEDIDA
					,AtributoEntradaDTO_PML.RETENIDA
					,AtributoEntradaDTO_PML.MONEDA
					,AtributoEntradaDTO_PML.RSR_T
					,AtributoEntradaDTO_PML.OFI_EMI
					,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.VALOR_RETENIDO
					,AtributoEntradaDTO_PML.INCISO
					,AtributoEntradaDTO_PML.ZONA_AMIS
			};
			
			String[] camposResulset = {
					ParametroSalidaSP_PML.NUM_POLIZA
					,ParametroSalidaSP_PML.NUM_REGISTRO
					,ParametroSalidaSP_PML.FECHA_INICIO
					,ParametroSalidaSP_PML.FECHA_FIN
					,ParametroSalidaSP_PML.INM_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONT_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONSEC_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONVENIO_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.TIPO_PRIMER_RIESGO
					,ParametroSalidaSP_PML.MONTO_PRIMER_RIESGO	
					,ParametroSalidaSP_PML.CONSEC_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONSEC_PERIODO_COBERTURA
					,ParametroSalidaSP_PML.CONVENIO_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.INM_DEDUCIBLE
					,ParametroSalidaSP_PML.CONT_DEDUCIBLE
					,ParametroSalidaSP_PML.CONSEC_DEDUCIBLE
					,ParametroSalidaSP_PML.CONVENIO_DEDUCIBLE
					,ParametroSalidaSP_PML.INM_COASEGURO
					,ParametroSalidaSP_PML.CONT_COASEGURO
					,ParametroSalidaSP_PML.CONSEC_COASEGURO
					,ParametroSalidaSP_PML.CONVENIO_COASEGURO
					,ParametroSalidaSP_PML.CLAVE_ESTADO
					,ParametroSalidaSP_PML.CODIGO_POSTAL
					,ParametroSalidaSP_PML.LONGITUD
					,ParametroSalidaSP_PML.LATITUD
					,ParametroSalidaSP_PML.PRIMERA_LINEA_MAR
					,ParametroSalidaSP_PML.PRIMERA_LINEA_LAGO
					,ParametroSalidaSP_PML.SOBREELEVACION_DESPLANTE
					,ParametroSalidaSP_PML.RUGOSIDAD
					,ParametroSalidaSP_PML.USO_INMUEBLE
					,ParametroSalidaSP_PML.NUM_PISOS
					,ParametroSalidaSP_PML.PISO
					,ParametroSalidaSP_PML.TIPO_CUBIERTA
					,ParametroSalidaSP_PML.FORMA_CUBIERTA
					,ParametroSalidaSP_PML.IRRE_PLANTA
					,ParametroSalidaSP_PML.OBJETOS_CERCA
					,ParametroSalidaSP_PML.AZOTEA
					,ParametroSalidaSP_PML.TAMANO_CRISTAL
					,ParametroSalidaSP_PML.TIPO_VENTANAS
					,ParametroSalidaSP_PML.TIPO_DOMOS
					,ParametroSalidaSP_PML.SOPORTE_VENTANA
					,ParametroSalidaSP_PML.PROCENTAJE_CRISTAL_FACHADAS
					,ParametroSalidaSP_PML.PORCENTAJE_DOMOS
					,ParametroSalidaSP_PML.OTROS_FACHADA
					,ParametroSalidaSP_PML.MUROS_CONTENCION
					,ParametroSalidaSP_PML.PRIMA
					,ParametroSalidaSP_PML.CEDIDA
					,ParametroSalidaSP_PML.RETENIDA
					,ParametroSalidaSP_PML.MONEDA
					,ParametroSalidaSP_PML.RSR_T
					,ParametroSalidaSP_PML.OFI_EMI
					,ParametroSalidaSP_PML.VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.VALOR_RETENIDO
					,ParametroSalidaSP_PML.INCISO
					,ParametroSalidaSP_PML.ZONA_AMIS
			};
			storedHelper.estableceMapeoResultados(ReportePMLHidroDTO.class.getCanonicalName(), atributosDTO,camposResulset);
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			return storedHelper.obtieneListaResultados();
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLHidroDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroSemiAgrupadoIncisos(Integer claveTipoReporte,String nombreUsuario)throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = SP_PMLHIDRO_SEMIAGR_INCISOS;
		
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			String []nombreParametros = {"pCodigoUsuario","pOpcion"};
			Object []valorParametros = {nombreUsuario,claveTipoReporte};
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			String[] atributosDTO = {
					AtributoEntradaDTO_PML.NUM_POLIZA
					,AtributoEntradaDTO_PML.NUM_REGISTRO
					,AtributoEntradaDTO_PML.FECHA_INICIO
					,AtributoEntradaDTO_PML.FECHA_FIN
					,AtributoEntradaDTO_PML.INM_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONT_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONSEC_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.CONVENIO_VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.TIPO_PRIMER_RIESGO
					,AtributoEntradaDTO_PML.MONTO_PRIMER_RIESGO
					,AtributoEntradaDTO_PML.CONSEC_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.CONSEC_PERIODO_COBERTURA
					,AtributoEntradaDTO_PML.CONVENIO_LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.INM_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONT_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONSEC_DEDUCIBLE
					,AtributoEntradaDTO_PML.CONVENIO_DEDUCIBLE
					,AtributoEntradaDTO_PML.INM_COASEGURO
					,AtributoEntradaDTO_PML.CONT_COASEGURO
					,AtributoEntradaDTO_PML.CONSEC_COASEGURO
					,AtributoEntradaDTO_PML.CONVENIO_COASEGURO
					,AtributoEntradaDTO_PML.CLAVE_ESTADO
					,AtributoEntradaDTO_PML.CODIGO_POSTAL
					,AtributoEntradaDTO_PML.LONGITUD
					,AtributoEntradaDTO_PML.LATITUD
					,AtributoEntradaDTO_PML.PRIMERA_LINEA_MAR
					,AtributoEntradaDTO_PML.PRIMERA_LINEA_LAGO
					,AtributoEntradaDTO_PML.SOBREELEVACION_DESPLANTE
					,AtributoEntradaDTO_PML.RUGOSIDAD
					,AtributoEntradaDTO_PML.USO_INMUEBLE
					,AtributoEntradaDTO_PML.NUM_PISOS
					,AtributoEntradaDTO_PML.PISO
					,AtributoEntradaDTO_PML.TIPO_CUBIERTA
					,AtributoEntradaDTO_PML.FORMA_CUBIERTA
					,AtributoEntradaDTO_PML.IRRE_PLANTA
					,AtributoEntradaDTO_PML.OBJETOS_CERCA
					,AtributoEntradaDTO_PML.AZOTEA
					,AtributoEntradaDTO_PML.TAMANO_CRISTAL
					,AtributoEntradaDTO_PML.TIPO_VENTANAS
					,AtributoEntradaDTO_PML.TIPO_DOMOS
					,AtributoEntradaDTO_PML.SOPORTE_VENTANA
					,AtributoEntradaDTO_PML.PROCENTAJE_CRISTAL_FACHADAS
					,AtributoEntradaDTO_PML.PORCENTAJE_DOMOS
					,AtributoEntradaDTO_PML.OTROS_FACHADA
					,AtributoEntradaDTO_PML.MUROS_CONTENCION
					,AtributoEntradaDTO_PML.PRIMA
					,AtributoEntradaDTO_PML.CEDIDA
					,AtributoEntradaDTO_PML.RETENIDA
					,AtributoEntradaDTO_PML.MONEDA
					,AtributoEntradaDTO_PML.RSR_T
					,AtributoEntradaDTO_PML.OFI_EMI
					,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.VALOR_RETENIDO
					,AtributoEntradaDTO_PML.INCISO
					,AtributoEntradaDTO_PML.ZONA_AMIS
			};
			
			String[] camposResulset = {
					ParametroSalidaSP_PML.NUM_POLIZA
					,ParametroSalidaSP_PML.NUM_REGISTRO
					,ParametroSalidaSP_PML.FECHA_INICIO
					,ParametroSalidaSP_PML.FECHA_FIN
					,ParametroSalidaSP_PML.INM_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONT_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONSEC_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.CONVENIO_VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.TIPO_PRIMER_RIESGO
					,ParametroSalidaSP_PML.MONTO_PRIMER_RIESGO	
					,ParametroSalidaSP_PML.CONSEC_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.CONSEC_PERIODO_COBERTURA
					,ParametroSalidaSP_PML.CONVENIO_LIMITE_MAXIMO
					,ParametroSalidaSP_PML.INM_DEDUCIBLE
					,ParametroSalidaSP_PML.CONT_DEDUCIBLE
					,ParametroSalidaSP_PML.CONSEC_DEDUCIBLE
					,ParametroSalidaSP_PML.CONVENIO_DEDUCIBLE
					,ParametroSalidaSP_PML.INM_COASEGURO
					,ParametroSalidaSP_PML.CONT_COASEGURO
					,ParametroSalidaSP_PML.CONSEC_COASEGURO
					,ParametroSalidaSP_PML.CONVENIO_COASEGURO
					,ParametroSalidaSP_PML.CLAVE_ESTADO
					,ParametroSalidaSP_PML.CODIGO_POSTAL
					,ParametroSalidaSP_PML.LONGITUD
					,ParametroSalidaSP_PML.LATITUD
					,ParametroSalidaSP_PML.PRIMERA_LINEA_MAR
					,ParametroSalidaSP_PML.PRIMERA_LINEA_LAGO
					,ParametroSalidaSP_PML.SOBREELEVACION_DESPLANTE
					,ParametroSalidaSP_PML.RUGOSIDAD
					,ParametroSalidaSP_PML.USO_INMUEBLE
					,ParametroSalidaSP_PML.NUM_PISOS
					,ParametroSalidaSP_PML.PISO
					,ParametroSalidaSP_PML.TIPO_CUBIERTA
					,ParametroSalidaSP_PML.FORMA_CUBIERTA
					,ParametroSalidaSP_PML.IRRE_PLANTA
					,ParametroSalidaSP_PML.OBJETOS_CERCA
					,ParametroSalidaSP_PML.AZOTEA
					,ParametroSalidaSP_PML.TAMANO_CRISTAL
					,ParametroSalidaSP_PML.TIPO_VENTANAS
					,ParametroSalidaSP_PML.TIPO_DOMOS
					,ParametroSalidaSP_PML.SOPORTE_VENTANA
					,ParametroSalidaSP_PML.PROCENTAJE_CRISTAL_FACHADAS
					,ParametroSalidaSP_PML.PORCENTAJE_DOMOS
					,ParametroSalidaSP_PML.OTROS_FACHADA
					,ParametroSalidaSP_PML.MUROS_CONTENCION
					,ParametroSalidaSP_PML.PRIMA
					,ParametroSalidaSP_PML.CEDIDA
					,ParametroSalidaSP_PML.RETENIDA
					,ParametroSalidaSP_PML.MONEDA
					,ParametroSalidaSP_PML.RSR_T
					,ParametroSalidaSP_PML.OFI_EMI
					,ParametroSalidaSP_PML.VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.VALOR_RETENIDO
					,ParametroSalidaSP_PML.INCISO
					,ParametroSalidaSP_PML.ZONA_AMIS
			};
			storedHelper.estableceMapeoResultados(ReportePMLHidroDTO.class.getCanonicalName(), atributosDTO,camposResulset);
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			return storedHelper.obtieneListaResultados();
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLHidroDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroSemiAgrupadoDatosGenerales(Integer claveTipoReporte,String nombreUsuario)throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = SP_PMLHIDRO_SEMIAGR_DATOS_GEN;
		
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			String []nombreParametros = {"pCodigoUsuario","pOpcion"};
			Object []valorParametros = {nombreUsuario,claveTipoReporte};
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			String[] atributosDTO = {
					AtributoEntradaDTO_PML.NUM_POLIZA
					,AtributoEntradaDTO_PML.TIPO_POLIZA
					,AtributoEntradaDTO_PML.FECHA_INICIO
					,AtributoEntradaDTO_PML.FECHA_FIN
			};
			
			String[] camposResulset = {
					ParametroSalidaSP_PML.NUM_POLIZA
					,ParametroSalidaSP_PML.TIPO_POLIZA
					,ParametroSalidaSP_PML.FECHA_INICIO
					,ParametroSalidaSP_PML.FECHA_FIN
			};
			storedHelper.estableceMapeoResultados(ReportePMLHidroDTO.class.getCanonicalName(), atributosDTO,camposResulset);
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			return storedHelper.obtieneListaResultados();
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLHidroDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroSemiAgrupadoDatosFinancieros(Integer claveTipoReporte,String nombreUsuario)throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = SP_PMLHIDRO_SEMIAGR_DATOS_FIN;
		
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			String []nombreParametros = {"pCodigoUsuario","pOpcion"};
			Object []valorParametros = {nombreUsuario,claveTipoReporte};
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			String[] atributosDTO = {
					AtributoEntradaDTO_PML.NUM_POLIZA
					,AtributoEntradaDTO_PML.NUM_CAPA
					,AtributoEntradaDTO_PML.PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.LIMITE_MAXIMO
					,AtributoEntradaDTO_PML.COASEGURO
			};
			
			String[] camposResulset = {
					ParametroSalidaSP_PML.NUM_POLIZA
					,ParametroSalidaSP_PML.NUM_CAPA
					,ParametroSalidaSP_PML.PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.LIMITE_MAXIMO
					,ParametroSalidaSP_PML.COASEGURO
			};
			storedHelper.estableceMapeoResultados(ReportePMLHidroDTO.class.getCanonicalName(), atributosDTO,camposResulset);
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			return storedHelper.obtieneListaResultados();
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLHidroDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroTecnicos(Integer claveTipoReporte,String nombreUsuario)throws Exception {
		StoredProcedureHelper storedHelper = null;
		String nombreSP = SP_PMLHIDRO_TECNICOS;
		
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			String []nombreParametros = {"pCodigoUsuario","pOpcion"};
			Object []valorParametros = {nombreUsuario,claveTipoReporte};
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			String[] atributosDTO = {
					AtributoEntradaDTO_PML.NUM_POLIZA
					,AtributoEntradaDTO_PML.NUM_REGISTRO
					,AtributoEntradaDTO_PML.FECHA_INICIO
					,AtributoEntradaDTO_PML.FECHA_FIN
					,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
					,AtributoEntradaDTO_PML.PORCENTAJE_RETENCION
					,AtributoEntradaDTO_PML.VALOR_RETENIDO
					,AtributoEntradaDTO_PML.PRIMA
					,AtributoEntradaDTO_PML.CEDIDA
					,AtributoEntradaDTO_PML.RETENIDA
					,AtributoEntradaDTO_PML.MONEDA
					,AtributoEntradaDTO_PML.RSR_T
					,AtributoEntradaDTO_PML.OFI_EMI
					,AtributoEntradaDTO_PML.ZONA_AMIS
			};
			
			String[] camposResulset = {
					ParametroSalidaSP_PML.NUM_POLIZA
					,ParametroSalidaSP_PML.NUM_REGISTRO
					,ParametroSalidaSP_PML.FECHA_INICIO
					,ParametroSalidaSP_PML.FECHA_FIN
					,ParametroSalidaSP_PML.VALOR_ASEGURABLE
					,ParametroSalidaSP_PML.PORCENTAJE_RETENCION
					,ParametroSalidaSP_PML.VALOR_RETENIDO
					,ParametroSalidaSP_PML.PRIMA
					,ParametroSalidaSP_PML.CEDIDA
					,ParametroSalidaSP_PML.RETENIDA
					,ParametroSalidaSP_PML.MONEDA
					,ParametroSalidaSP_PML.RSR_T
					,ParametroSalidaSP_PML.OFI_EMI
					,ParametroSalidaSP_PML.ZONA_AMIS
			};
			storedHelper.estableceMapeoResultados(ReportePMLHidroDTO.class.getCanonicalName(), atributosDTO,camposResulset);
			LogDeMidasEJB3.log("Ejecutando SP: "+nombreSP+", parametros: "+nombreParametros+", valores: "+valorParametros, Level.INFO, null);
			return storedHelper.obtieneListaResultados();
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReportePMLHidroDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error en la ejecuci�n del SP: "+nombreSP+". No se env�an par�metros.", Level.SEVERE, e);
			throw e;
		}
	}
	
	@Deprecated
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReportePMLHidroDTO> obtieneReportePMLHidro(
			ReportePMLHidroDTO filtroReporte, String nombreUsuario)
			throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper(
					"MIDAS.pkgDAN_Reportes.spDAN_RepPMLHidro", StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper
			.estableceMapeoResultados(
					ReportePMLHidroDTO.class.getCanonicalName(),
					
										
					"idRegistro," +
					"numeroPoliza," +
					"numeroRegistro," +
					"fechaInicio," +
					"fechaFin," +
					"inmValorAsegurable," +
					"contValorAsegurable," +
					"consecValorAsegurable," +
					"convenioValorAsegurable," +
					"porcentajeRetencion," +
					"tipoPrimerRiesgo," + 
					"montoPrimerRiesgo," +
					"consecLimiteMaximo," +
					"consecPeriodoCobertura," +
					"convenioLimiteMaximo," +
					"inmDeducible," +
					"contDeducible," +
					"consecDeducible," +
					"convenioDeducible," +
					"inmCoaseguro," +
					"contCoaseguro," +
					"consecCoaseguro," +
					"convenioCoaseguro," +
					"claveEstado," +
					"codigoPostal," +
					"longitud," +
					"latitud," +
					"primeraLineaMar," +
					"primeraLineaLago," +
					"sobreelevacionDesplante," +
					"rugosidad," +
					"usoInmueble," +
					"numeroPisos," +
					"piso," +
					"tipoCubierta," +
					"formaCubierta," +
					"irrePlanta," +
					"objetosCerca," +
					"azotea," +
					"tamanoCristal," +
					"tipoVentanas," +
					"tipoDomos," +
					"soporteVentana," +
					"porcentajeCristalFachadas," +
					"porcentajeDomos," +
					"otrosFachada," +
					"murosContencion," +
					"valorAsegurable," +
					"valorRetenido," +
					"prima," +
					"cedida," +
					"retenida," +
					"moneda," +
					"rsrt," +
					"ofiEmi" ,
					
					"id_registro," +
					"num_poliza," +
					"num_registro," +
					"fecha_inicio," +
					"fecha_fin," +
					"inm_valor_asegurable," +
					"cont_valor_asegurable," +
					"consec_valor_asegurable," +
					"convenio_valor_asegurable," +
					"porcentaje_retencion," +
					"tipo_primer_riesgo," +
					"monto_primer_riesgo," +
					"consec_limite_maximo," +
					"consec_periodo_cobertura," +
					"convenio_limite_maximo," +
					"inm_deducible," +
					"cont_deducible," +
					"consec_deducible," +
					"convenio_deducible," +
					"inm_coaseguro," +
					"cont_coaseguro," +
					"consec_coaseguro," +
					"convenio_coaseguro," +
					"clave_estado," +
					"codigo_postal," +
					"longitud," +
					"latitud," +
					"primera_linea_mar," +
					"primera_linea_lago," +
					"sobreelevacion_desplante," +
					"rugosidad," +
					"uso_inmueble," +
					"num_pisos," +
					"piso," +
					"tipo_cubierta," +
					"forma_cubierta," +
					"irre_planta," +
					"objetos_cerca," +
					"azotea," +
					"tamano_cristal," +
					"tipo_ventanas," +
					"tipo_domos," +
					"soporte_ventana," +
					"procentaje_cristal_fachadas," +
					"porcentaje_domos," +
					"otros_fachada," +
					"muros_contencion," +
					"valor_asegurable," +
					"valor_retenido," +
					"prima," +
					"cedida," +
					"retenida," +
					"moneda," +
					"rsr_t," +
					"ofi_emi");

			storedHelper.estableceParametro("pFechaCorte", filtroReporte.getFechaCorte());
			storedHelper.estableceParametro("pTipoCambio", filtroReporte.getTipoCambio());
						
			return storedHelper.obtieneListaResultados();
			
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"pkgDAN_Reportes.spDAN_RepPMLHidro", ReportePMLHidroDTO.class, codErr, descErr);
			throw e;
		} catch (Exception e) {
			throw e;
		}
	}

}
