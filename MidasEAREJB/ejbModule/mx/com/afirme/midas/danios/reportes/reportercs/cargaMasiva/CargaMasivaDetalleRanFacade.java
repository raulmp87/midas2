package mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva;


import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import mx.com.afirme.midas.catalogos.esquemasreas.EsquemasDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.caducidad.CaducidadesDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless 
public class CargaMasivaDetalleRanFacade implements
		CargaMasivaDetalleRanFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private CargaMasivaDetalleRanFacadeRemote cargaMasivaDetalleRanFacadeRemote;
	
	java.math.BigDecimal cerAux = new java.math.BigDecimal(0);
	
	public void save(CargaMasivaRangosRamosDTO entity) {
		LogDeMidasEJB3.log("saving CargaMasivaRangosRamosDTO instance",
				Level.INFO, null);
		try {
			
			entityManager.merge(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void save(EsquemasDTO entity) {
		LogDeMidasEJB3.log("saving EsquemasDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void save(CaducidadesDTO entity) {
		LogDeMidasEJB3.log("saving CaducidadesDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void save(CargaMasivaDetalleRanDTO entity) {
		LogDeMidasEJB3.log("saving CargaMasivaDetalleCotDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	public void delete(CargaMasivaDetalleRanDTO entity) {
		LogDeMidasEJB3.log("deleting CargaMasivaDetalleCotDTO instance",
				Level.INFO, null);
		try {
			CargaMasivaDetalleRanDTO entity2 = entityManager.getReference(CargaMasivaDetalleRanDTO.class,
					entity.getIdLimite());
			entityManager.remove(entity2);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	public void delete(EsquemasDTO entity) {
		LogDeMidasEJB3.log("deleting EsquemasDTO instance",
				Level.INFO, null);
		try {
			EsquemasDTO entity2 = entityManager.getReference(EsquemasDTO.class,
					entity.getId());
			entityManager.remove(entity2);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	public void delete(CargaMasivaRangosRamosDTO entity) {
		LogDeMidasEJB3.log("deleting CargaMasivaRangosRamosDTO instance",
				Level.INFO, null);
		try {
			CargaMasivaRangosRamosDTO entity2 = entityManager.getReference(CargaMasivaRangosRamosDTO.class,
					entity.getIdRango());
			entityManager.remove(entity2);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public int deleteRangos(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding EsquemasDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "delete from CargaMasivaRangosRamosDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CargaMasivaDetalleRanDTO update(CargaMasivaDetalleRanDTO entity) {
		LogDeMidasEJB3.log("updating CargaMasivaDetalleCotDTO instance",
				Level.INFO, null);
		try {
			CargaMasivaDetalleRanDTO result =null;// 
			entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CargaMasivaDetalleRanDTO findById(CargaMasivaDetalleRanDTO id) {
		LogDeMidasEJB3.log(
				"finding CargaMasivaDetalleCotDTO instance with id: " + id,
				Level.INFO, null);
		try {
			CargaMasivaDetalleRanDTO instance = entityManager.find(
					CargaMasivaDetalleRanDTO.class, id.getIdLimite());
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CargaMasivaRangosRamosDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding CargaMasivaRangosRamosDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from CargaMasivaRangosRamosDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public int deleteEsquemas(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding EsquemasDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "delete EsquemasDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CargaMasivaRangosRamosDTO> findByProperties(String propertyName,
			final Object value,String propertyName2,
			final Object value2) {
		LogDeMidasEJB3.log(
				"finding CargaMasivaDetalleCotDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from CargaMasivaRangosRamosDTO model where model."
					+ propertyName + "= :propertyValue and model."
					+ propertyName2 + "= :propertyValue2";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setParameter("propertyValue2", value2);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<EsquemasDTO> findByPropertiesEsq(String propertyName,
			final Object value,String propertyName2,
			final Object value2) {
		LogDeMidasEJB3.log(
				"finding EsquemasDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from EsquemasDTO model where model."
					+ propertyName + "= :propertyValue and model."
					+ propertyName2 + "= :propertyValue2";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setParameter("propertyValue2", value2);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<java.math.BigDecimal> getMaxCER(String propertyName,
			final Object value,String propertyName2,
			final Object value2) {
		LogDeMidasEJB3.log(
				"finding EsquemasDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select max(model.cer) from EsquemasDTO model where model."
					+ propertyName + "= :propertyValue and model."
					+ propertyName2 + "= :propertyValue2";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setParameter("propertyValue2", value2);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<CargaMasivaDetalleRanDTO> findAll() {
		LogDeMidasEJB3.log("finding all CargaMasivaDetalleCotDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from CargaMasivaDetalleCotDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	} 
	
	
	public CargaMasivaDetalleRanDTO agregar2(
			List<CargaMasivaRangosRamosDTO> direccionesValidas,
			List<CargaMasivaRangosRamosDTO> direccionesInvalidas) {
		LogDeMidasEJB3.log("agregando instancia de CargaMasivaCotDTO",
				Level.INFO, null);
		try {
			
			this.insertarDetalleCargaMasiva(
					direccionesValidas); 
			return null;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
		
		 
	}
	
	public EsquemasDTO agregar(
			List<EsquemasDTO> direccionesValidas) {
		LogDeMidasEJB3.log("agregando instancia de CargaMasivaCotDTO",
				Level.INFO, null);
		try {
			this.insertarEsquemas(
					direccionesValidas); 
			return null;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
		 
	}
	
	public CaducidadesDTO agregarCad(
			List<CaducidadesDTO> direccionesValidas) {
		LogDeMidasEJB3.log("agregando instancia de CargaMasivaCotDTO",
				Level.INFO, null);
		try {
			this.insertarCad(
					direccionesValidas); 
			return null;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
		
	}
	
	public void insertarDetalleCargaMasiva(
			List<CargaMasivaRangosRamosDTO> direcciones) {
		
		CargaMasivaRangosRamosDTO cargaMasivaRangosRamosDTO;
		CargaMasivaRangosRamosDTO rangosRamosDTOC;
		
		for (CargaMasivaRangosRamosDTO direccion : direcciones) {
			
			String[] ramos = direccion.getRamo().split("-");
			
			for (String ramo : ramos) {
				if("0".equals(ramo))
				{
					continue;
				}
				cargaMasivaRangosRamosDTO = direccion.clone();
				BigDecimal bigDecimal = new BigDecimal(ramo);
				
				cargaMasivaRangosRamosDTO.setRamo(bigDecimal.intValue()+"");
				
				String [] coberturas = cargaMasivaRangosRamosDTO.getCobertura().split("-");
				for (String cobertura : coberturas) {
					
					if("0".equals(cobertura))
					{
						continue;
					}
					bigDecimal = new BigDecimal(cobertura); 
					
					rangosRamosDTOC = cargaMasivaRangosRamosDTO.clone();
					rangosRamosDTOC.setCobertura(bigDecimal.intValue()+"");
										
					insertaDetalleCargaMasiva(rangosRamosDTOC);
				}
				
			}
		}

		}
	
	public void insertarEsquemas(
			List<EsquemasDTO> direcciones) {
		cerAux = new java.math.BigDecimal(0);
		for (EsquemasDTO direccion : direcciones) {
			insertaEsquema(direccion);		 
		}

	}
	
	public void insertarCad(
			List<CaducidadesDTO> caducidadLis) {
		
		for (CaducidadesDTO caducidad : caducidadLis) {
			insertaCaducidad(caducidad);		 
		}

	}
	
	private EsquemasDTO insertaEsquema(EsquemasDTO esquema) {
		
		List<java.math.BigDecimal> max;
		if(esquema.getIdContrato().charAt(0) == 'V' ){
			esquema.setCveNegocio(new java.math.BigDecimal(4));
			max = getMaxCER("anio", esquema.getAnio(),"cveNegocio",esquema.getCveNegocio());
			if(max.get(0)!=null){
				if(cerAux.equals(esquema.getCer())){
					esquema.setCer(max.get(0));
				}
				else{
					cerAux = esquema.getCer();
					esquema.setCer(max.get(0).add(new java.math.BigDecimal(1)));
				}
					
			}
			else{
				cerAux = esquema.getCer();
				esquema.setCer(new java.math.BigDecimal(esquema.getAnio().intValue()*10000+2001)); 
			}
			
		}if(esquema.getIdContrato().charAt(0) == 'C' ){
			esquema.setCveNegocio(new java.math.BigDecimal(4));
			max = getMaxCER("anio", esquema.getAnio(),"cveNegocio",esquema.getCveNegocio());
			if(max.get(0)!=null){
				if(cerAux.equals(esquema.getCer())){
					esquema.setCer(max.get(0));
				}
				else{
					cerAux = esquema.getCer();
					esquema.setCer(max.get(0).add(new java.math.BigDecimal(1)));
				}
					
			}
			else{
				cerAux = esquema.getCer();
				esquema.setCer(new java.math.BigDecimal(esquema.getAnio().intValue()*10000+2001)); 
			}
			
		}
		
		else if(esquema.getIdContrato().charAt(0) == 'A'){
			esquema.setCveNegocio(new java.math.BigDecimal(1));
			max = getMaxCER("anio", esquema.getAnio(),"cveNegocio",esquema.getCveNegocio());
			if(max.get(0)!=null){
				if(cerAux.compareTo(esquema.getCer())==0){
					esquema.setCer(max.get(0));
				}
				else{
					cerAux = esquema.getCer();
					esquema.setCer(max.get(0).add(new java.math.BigDecimal(1)));
				}
			}
			else{
				cerAux = esquema.getCer();
				esquema.setCer(new java.math.BigDecimal(esquema.getAnio().intValue()*10000+1001)); 
			}
		}else{
			esquema.setCveNegocio(new java.math.BigDecimal(2));
			max = getMaxCER("anio", esquema.getAnio(),"cveNegocio",esquema.getCveNegocio());
			if(max.get(0)!=null){
				if(cerAux.compareTo(esquema.getCer())==0){
					esquema.setCer(max.get(0));
				}
				else{ 
					cerAux = esquema.getCer();
					esquema.setCer(max.get(0).add(new java.math.BigDecimal(1)));
				}
			}
			else{
				cerAux = esquema.getCer();
				esquema.setCer(new java.math.BigDecimal(esquema.getAnio().intValue()*10000+5001)); 
			}
		}
		cargaMasivaDetalleRanFacadeRemote.save(esquema);

		return null;
		
	}
	
	
	private CaducidadesDTO insertaCaducidad(CaducidadesDTO esquema) {
		
		cargaMasivaDetalleRanFacadeRemote.save(esquema);

		return null;
		
	}

	private CargaMasivaRangosRamosDTO insertaDetalleCargaMasiva(CargaMasivaRangosRamosDTO detalle) {

		cargaMasivaDetalleRanFacadeRemote.save(detalle);

		return null;
	}

	@Override
	public CargaMasivaRangosRamosDTO findById(CargaMasivaRangosRamosDTO id) {
		LogDeMidasEJB3.log(
				"finding CargaMasivaDetalleCotDTO instance with id: " + id,
				Level.INFO, null);
		try {
			return entityManager.find(
					CargaMasivaRangosRamosDTO.class, id.getIdRango());
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public CargaMasivaRangosRamosDTO findByRamo(CargaMasivaRangosRamosDTO id) {
		LogDeMidasEJB3.log(
				"finding CargaMasivaDetalleCotDTO instance with id: " + id,
				Level.INFO, null);
		try {
			return entityManager.find(
					CargaMasivaRangosRamosDTO.class,new BigDecimal(id.getRamo()));
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			return null;
		}
	}
 
	@Override
	public int deleteEsquemas(BigDecimal anio, BigDecimal mes,
			BigDecimal dia, String cveNegocio) {
		LogDeMidasEJB3.log( 
				"finding EsquemasDTO instance with property: anio"
						+  ", value: " + anio, Level.INFO, null);
		try {
			final String queryString = "delete from EsquemasDTO model " +
					"where model.aut = 0 " +
					"and model.anio = :propertyValue " +
					"and model.mes = :pMes " +
					"and model.dia = :pDia " +
					"and model.cveNegocio = :proper_cveNegocio";
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", anio);
			query.setParameter("proper_cveNegocio", new BigDecimal(cveNegocio));
			query.setParameter("pMes", mes);
			query.setParameter("pDia", dia);
			
			return query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

}