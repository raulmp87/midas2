package mx.com.afirme.midas.danios.reportes.poliza.empresarial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.SystemException;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaPolizaBase;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import net.sf.jasperreports.engine.JRException;

public class PL2_PolizaDocumentosAdicionales extends PlantillaPolizaBase{

	public PL2_PolizaDocumentosAdicionales(CotizacionDTO cotizacionDTO,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,reporteBase);
		super.setNombrePlantilla(Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.poliza.reporte.documentosAnexos"));
		setPaquetePlantilla(Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.paquete"));
	}
	
	public PL2_PolizaDocumentosAdicionales(CotizacionDTO cotizacionDTO,Map<String,Object> mapaParametros,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,mapaParametros,reporteBase);
		super.setNombrePlantilla(Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.poliza.reporte.documentosAnexos"));
		setPaquetePlantilla(Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.paquete"));
	}

	public byte[] obtenerReporte(String nombreUsuario) throws SystemException {
		procesarDatosReporte(nombreUsuario);
		return getByteArrayReport();
	}
	
	private void procesarDatosReporte(String claveUsuario) throws SystemException {
		if (this.cotizacionDTO!= null){
			if (getParametrosVariablesReporte() == null){
				super.generarParametrosComunes(cotizacionDTO, claveUsuario);
			}
		    setListaRegistrosContenido(new ArrayList<Object>());
		    
		    List<DocAnexoCotDTO> listaAnexos = consultarDocumentosAnexos(true);
			
			if(listaRegistrosContenido == null)
				listaRegistrosContenido = new ArrayList<Object>();
			listaRegistrosContenido.addAll(listaAnexos);
			
			if (getListaRegistrosContenido().isEmpty()){
				setByteArrayReport( null );
				generarLogPlantillaSinDatosParaMostrar();
				return;
			}
			if (getListaRegistrosContenido().isEmpty()){
				setByteArrayReport( null );
				generarLogPlantillaSinDatosParaMostrar();
				return;
			}
		    try {
				super.setByteArrayReport( generaReporte(ConstantesReporte.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()));
			} catch (JRException e) {
				setByteArrayReport( null );
			}
		}
		else setByteArrayReport( null );
	}

}
