package mx.com.afirme.midas.danios.reportes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;

import javax.transaction.SystemException;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import net.sf.jasperreports.engine.JRException;

public abstract class PlantillaSubIncisosBase extends PlantillaCotizacionBase{
	protected BigDecimal idToSeccion;
	
	public PlantillaSubIncisosBase(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,BigDecimal idToSeccion,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,incisoCotizacionDTO,reporteBase);
		validarParametro(idToSeccion, "idToSeccion");
		this.idToSeccion = idToSeccion;
	}
	
	public PlantillaSubIncisosBase(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,incisoCotizacionDTO,reporteBase);
	}
	
	public PlantillaSubIncisosBase(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,BigDecimal idToSeccion,Map<String,Object> mapaParametrosPlantilla,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,incisoCotizacionDTO,mapaParametrosPlantilla,reporteBase);
		validarParametro(idToSeccion, "idToSeccion");
		this.idToSeccion = idToSeccion;
	}
	
	public PlantillaSubIncisosBase(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,Map<String,Object> mapaParametrosPlantilla,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,incisoCotizacionDTO,mapaParametrosPlantilla,reporteBase);
	}

	/**
	 * M�todo que llena la lista de registros de la plantilla con la informaci�n de los subincisos registrados para el inciso y secci�n recibidos en el constructor.
	 * @param claveUsuario
	 * @throws SystemException
	 */
	protected void poblarSubIncisos(String claveUsuario) throws SystemException {
		if (this.cotizacionDTO != null && incisoCotizacionDTO != null){
			if (getParametrosVariablesReporte() == null){
				super.generarParametrosComunes(cotizacionDTO, claveUsuario);
			}
			List<SubIncisoCotizacionDTO> listaSubIncisos = null;
			if(idToSeccion != null)
				listaSubIncisos = getReporteBase().obtenerSubIncisos(idToSeccion, incisoCotizacionDTO.getId().getNumeroInciso());
			else
				listaSubIncisos = reporteBase.obtenerSubIncisosPorInciso(incisoCotizacionDTO.getId().getNumeroInciso());
			//Si no se encontraron subincisos, no se debe mostrar la plantilla
			setListaRegistrosContenido(new ArrayList<Object>());
			if (listaSubIncisos != null){
				if (!listaSubIncisos.isEmpty())
					getListaRegistrosContenido().addAll(listaSubIncisos);
				else{
					setByteArrayReport( null );
					generarLogPlantillaSinDatosParaMostrar();
					return;
				}
			} else{
				setByteArrayReport( null );
				generarLogPlantillaSinDatosParaMostrar();
				return;
			}
			super.poblarParametrosPlantillaDatosGeneralesInciso();
			
			Double sumatoriaSumasAseguradas = 0d;
			if(listaSubIncisos != null)
				for(SubIncisoCotizacionDTO subInciso : listaSubIncisos)
					sumatoriaSumasAseguradas += subInciso.getValorSumaAsegurada();
			getParametrosVariablesReporte().put("SUMATORIA_SUBINCISOS", sumatoriaSumasAseguradas);
			try{
				getParametrosVariablesReporte().put("DESCRIPCION_SECCION", ReporteCotizacionBase.TITULO_SUBINCISOS_SECCION+listaSubIncisos.get(0).getSeccionCotizacionDTO().getSeccionDTO().getNombreComercial().toUpperCase());
			}catch(Exception e){
				try{
					getParametrosVariablesReporte().put("DESCRIPCION_SECCION", ReporteCotizacionBase.TITULO_SUBINCISOS_SECCION+reporteBase.buscarSeccionDTOPorId(idToSeccion).getNombreComercial().toUpperCase());
				}catch(Exception e1){
					getParametrosVariablesReporte().put("DESCRIPCION_SECCION", ReporteCotizacionBase.TITULO_SUBINCISOS_SECCION);
				}
			}
			try {
				setByteArrayReport( generaReporte(ConstantesReporte.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(),
						getParametrosVariablesReporte(), getListaRegistrosContenido()) );
			} catch (JRException e) {
				setByteArrayReport( null );
				generarLogErrorCompilacionPlantilla(e);
			}
			
		}
	}
	
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}
	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

}
