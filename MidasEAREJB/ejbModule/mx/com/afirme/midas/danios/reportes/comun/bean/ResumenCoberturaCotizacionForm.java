package mx.com.afirme.midas.danios.reportes.comun.bean;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas.cotizacion.resumen.ResumenRiesgoCotizacionForm;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ResumenCoberturaCotizacionForm implements Serializable{

	private static final long serialVersionUID = 7065949522782877374L;
	private String descripcionCobertura;
	private String sumaAsegurada;
	private String cuota;
	private String primaNeta;
	private String numeroCobertura;
	private String descripcionDeducible;
	private String descripcionCoaseguro;
	private List<ResumenRiesgoCotizacionForm> listaRiesgos;
	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}
	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public String getCuota() {
		return cuota;
	}
	public void setCuota(String cuota) {
		this.cuota = cuota;
	}
	public String getPrimaNeta() {
		return primaNeta;
	}
	public void setPrimaNeta(String primaNeta) {
		this.primaNeta = primaNeta;
	}
	public String getNumeroCobertura() {
		return numeroCobertura;
	}
	public void setNumeroCobertura(String numeroCobertura) {
		this.numeroCobertura = numeroCobertura;
	}
	public String getDescripcionDeducible() {
		return descripcionDeducible;
	}
	public void setDescripcionDeducible(String descripcionDeducible) {
		this.descripcionDeducible = descripcionDeducible;
	}
	public String getDescripcionCoaseguro() {
		return descripcionCoaseguro;
	}
	public void setDescripcionCoaseguro(String descripcionCoaseguro) {
		this.descripcionCoaseguro = descripcionCoaseguro;
	}
	public List<ResumenRiesgoCotizacionForm> getListaRiesgos() {
		return listaRiesgos;
	}
	public void setListaRiesgos(List<ResumenRiesgoCotizacionForm> listaRiesgos) {
		this.listaRiesgos = listaRiesgos;
	}
	public JRDataSource getListaRiesgosCotizacionJasper(){
		return new JRBeanCollectionDataSource(listaRiesgos);
	}
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!(obj instanceof ResumenCoberturaCotizacionForm))	return false;
		ResumenCoberturaCotizacionForm castObj = (ResumenCoberturaCotizacionForm) obj;
		if(castObj.getDescripcionCobertura() == null) return false;
        return (castObj.getDescripcionCobertura().equals(this.descripcionCobertura));
	}
	
	@Override
	public int hashCode() {
        int result = 17;
        result = 37 * result + ( getDescripcionCobertura() == null ? 0 : this.getDescripcionCobertura().hashCode() );
        return result;
	}
}
