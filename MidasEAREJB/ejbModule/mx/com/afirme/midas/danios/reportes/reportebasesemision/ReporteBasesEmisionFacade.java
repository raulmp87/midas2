package mx.com.afirme.midas.danios.reportes.reportebasesemision;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless
public class ReporteBasesEmisionFacade implements ReporteBasesEmisionFacadeRemote {

//	@EJB
//	private TipoCambioFacadeRemote tipoCambioFacade;
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReporteBasesEmisionDTO> obtieneMovimientosEmision(
			ReporteBasesEmisionDTO movimientoEmision, String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		
		String nombreSP = "MIDAS.pkgDAN_Reportes.spDAN_RepBasesEmision";
		
		try {
			
			String[] nombreParametros = {"pFechaInicio", 
					"pFechaFin",
					"pIdRamo",
					"pIdSubRamo",
					"pNivelContratoReaseguro",
					"pNivelAgrupamiento"};
			Object[] valorParametros={movimientoEmision.getFechaInicio(),
				movimientoEmision.getFechaFin(),
				movimientoEmision.getIdRamo(),
				movimientoEmision.getIdSubRamo(),
				movimientoEmision.getIdNivelContratoReaseguro(),
				movimientoEmision.getNivelAgrupamiento()};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteBasesEmisionDTO.class.getCanonicalName(),
					
					"oficinaEmision," +
					"descOficinaEmision," +
					"descRamo," +
					"ctaContableRamo," +
					"descSubRamo," +
					"ctaContableSubRamo," +
					"numeroPoliza," +
					"monedaPoliza," +
					"codigoProducto," +
					"descProducto," +
					"codigoTipoNegocio," +
					"descTipoNegocio," +
					"codigoUsuarioAsegurado," +
					"nombreUsuarioAsegurado," +
					"claveAgente," +
					"nombreAgente," +
					"formaPago," +
					"estatusPoliza," +
					"giroAsociadoPoliza," +
					"numeroEndoso," +
					"descTipoEndoso," +
					"descMotivoEndoso," +
					"fechaEmision," +
					"fechaInicioVigencia," +
					"fechaFinVigencia," +
					"tipoCambio," +
					"numeroInciso,"+
					"descSeccion,"+
					"descCobertura,"+
					"importePrimaNeta," +
					"importeDerechos," +
					"importeComisiones," +
					"importeBonifCom," +
					"importeComisionesRPF," +
					"importeBonifComRPF," +
					"importeRecargos," +
					"importeIVA," +
					"nivelContratoReaseguro," +
					"importePrimaRetenida," +
					"importePrimaCedida," +
					"importePrimaFacultada,"+
					"parteRelacionada",
											
					"oficinaEmision," +
					"descOficinaEmision," +
					"descRamo," +
					"ctaContableRamo," +
					"descSubRamo," +
					"ctaContableSubRamo," +
					"numeroPoliza," +
					"monedaPoliza," +
					"codigoProducto," +
					"descProducto," +
					"codigoTipoNegocio," +
					"descTipoNegocio," +
					"codigoUsuarioAsegurado," +
					"nombreUsuarioAsegurado," +
					"claveAgente," +
					"nombreAgente," +
					"formaPago," +
					"estatusPoliza," +
					"giroAsociadoPoliza," +
					"numeroEndoso," +
					"descTipoEndoso," +
					"descMotivoEndoso," +
					"fechaEmision," +
					"fechaInicioVigencia," +
					"fechaFinVigencia," +
					"tipoCambio," +
					"numeroInciso,"+
					"descSeccion,"+
					"descCobertura,"+
					"importePrimaNeta," +
					"importeDerechos," +
					"importeComisiones," +
					"importeBonifCom," +
					"importeComisionesRPF," +
					"importeBonifComRPF," +
					"importeRecargos," +
					"importeIVA," +
					"nivelContratoReaseguro," +
					"importePrimaRetenida," +
					"importePrimaCedida," +
					"importePrimaFacultada,"+
					"parteRelacionada");
			
			String descripcionParametros = obtenerDescripcionParametros(nombreParametros,valorParametros);
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			LogDeMidasEJB3.log("Consultando reporte bases emision. SP a invocar: "+nombreSP+", Parametros: "+descripcionParametros, Level.INFO, null);
			
			List<ReporteBasesEmisionDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log("Finaliza consulta reporte bases emision. SP invocado: "+nombreSP+", Parametros: "+descripcionParametros+
					"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
						
			return listaResultado;
			
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log("Ocurri� un error al consultar reporte bases emision. SP invocado: "+nombreSP, Level.SEVERE, e); 
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReporteBasesEmisionDTO.class, codErr, descErr);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Ocurri� un error al consultar reporte bases emision. SP invocado: "+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}

	public String obtenerDescripcionParametros(String[] nombreParametros,
			Object[] valorParametros) {
		StringBuilder descripcionParametros = new StringBuilder("");
		for(int i=0;i<nombreParametros.length;i++){
			descripcionParametros.append(", [").append(nombreParametros[i]).append("] : ").append(valorParametros[i]);
		}
		return descripcionParametros.toString();
	}

}
