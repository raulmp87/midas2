package mx.com.afirme.midas.danios.reportes.reportemonitoreosol;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;

@Stateless
public class ReporteMonitoreoSolicitudesFacade implements
		ReporteMonitoreoSolicitudesFacadeRemote {

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<ReporteMonitoreoSolicitudesDTO> obtieneReporteMonitoreoSolicitudes(
			ReporteMonitoreoSolicitudesDTO filtroReporte, String nombreUsuario)
			throws Exception {
		
		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper(
					"MIDAS.pkgDAN_Reportes.spDAN_RepMonitoreoSolicitudes", StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper
			.estableceMapeoResultados(
					ReporteMonitoreoSolicitudesDTO.class.getCanonicalName(),
					
										
					"idSOL," +
					"fechaSOL," +
					"tipoSOL," +
					"tipoEndoso," +
					"estatusSOL," +
					"codigoProductoSOL," +
					"codigoEjecutivoSOL," +
					"nombreEjecutivoSOL," +
					"codigoAgenteSOL," +
					"nombreAgenteSOL," +
					"nombreSolicitante,"+
					"codigoUsuarioSOL," +
					"horasSOL," +
					"idODT," +
					"fechaODT," +
					"codigoUsuarioODT," +
					"horasODT," +
					"idCOT," +
					"fechaCOT," +
					"codigoUsuarioCOT," +
					"horasCOT," +
					"fechaEMI," +
					"codigoUsuarioEMI," +
					"horasEMI," +
					"fechaTOT," +
					"codigoUsuarioTOT," +
					"idPOL," +
					"numeroEndosoPOL," +
					"horasTOT" ,

					"idSOL," +
					"fechaSOL," +
					"tipoSOL," +
					"tipoEndoso," +
					"estatusSOL," +
					"codigoProductoSOL," +
					"codigoEjecutivoSOL," +
					"nombreEjecutivoSOL," +
					"codigoAgenteSOL," +
					"nombreAgenteSOL," +
					"nombreSolicitante,"+
					"codigoUsuarioSOL," +
					"horasSOL," +
					"idODT," +
					"fechaODT," +
					"codigoUsuarioODT," +
					"horasODT," +
					"idCOT," +
					"fechaCOT," +
					"codigoUsuarioCOT," +
					"horasCOT," +
					"fechaEMI," +
					"codigoUsuarioEMI," +
					"horasEMI," +
					"fechaTOT," +
					"codigoUsuarioTOT," +
					"idPOL," +
					"numeroEndosoPOL," +
					"horasTOT");

			storedHelper.estableceParametro("pFechaInicio", filtroReporte.getFechaInicio());
			storedHelper.estableceParametro("pFechaFin", filtroReporte.getFechaFin());
			storedHelper.estableceParametro("pIdProducto", filtroReporte.getIdProductoSol());
			storedHelper.estableceParametro("pCodigoEjecutivo", filtroReporte.getCodigoEjecutivoSOL());
			storedHelper.estableceParametro("pCodigoAgente", filtroReporte.getCodigoAgenteSOL());
			storedHelper.estableceParametro("pCodigoUsuarioSolicitud", filtroReporte.getCodigoUsuarioSOL());
			
			
			return storedHelper.obtieneListaResultados();
			
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"MIDAS.pkgDAN_Reportes.spDAN_RepMonitoreoSolicitudes", ReporteMonitoreoSolicitudesDTO.class, codErr, descErr);
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
	}

}
