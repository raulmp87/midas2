package mx.com.afirme.midas.danios.reportes.reporterr6;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;


/**
 * Facade for entity ReporteRR6DTO.
 * 
 * @see .ReporteRR6DTO
 */

@Stateless
public class ReporteRR6Facade implements ReporteRR6FacadeRemote {
	
	public static final int RTRE = 1;
	public static final int RTRC = 2;	
	public static final int RTRF = 3;
	public static final int RTRR = 4;
	public static final int RTRS = 5;
	public static final int RARN = 6;
	public static final int RTRI = 7;
	public static final int CUMF = 8;
	
	@PersistenceContext
	private EntityManager entityManager;

	
	/**
	 * Obtiene la informacion de los reportes RR6.
	 * @param fechaIni
	 * @param fechaFinal
	 * @param tipoCambio
	 * @param reporte
	 * @param nombreUsuario
	 * @return listaResultado.
	 */
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReporteRR6DTO> obtenerReporte(String fechaIni, String fechaFinal, BigDecimal tipoCambio, 
							   int reporte, String nombreUsuario) throws Exception {
		
		StoredProcedureHelper storedHelper = null;
		String nombreSP = "";
		
		try {
			
			if (reporte == RTRE){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RTRE";
			}else if(reporte == RTRC){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RTRC";
			}else if(reporte == RTRF){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RTRF";
			}else if(reporte == RTRR){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RTRR";
			}else if(reporte == RTRS){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RTRS";
			}else if(reporte == RARN){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RARN";
			}else if(reporte == RTRI){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RTRI";
			}else if(reporte == CUMF){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_CUMF";
			}
	
			String[] nombreParametros = {"pfechainicial","pfechafinal","ptipocambio", "pidUsuario"};
			Object[] valorParametros={fechaIni, fechaFinal, tipoCambio, nombreUsuario};
			
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRR6DTO.class.getCanonicalName(),"registro",	"registro");
			
			
			String descripcionParametros = obtenerDescripcionParametros(nombreParametros,valorParametros);
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			LogDeMidasEJB3.log("Generando reporte. SP a invocar: "+nombreSP+", Parametros: "+descripcionParametros, Level.INFO, null);
			
						
			List<ReporteRR6DTO> listaResultado = storedHelper.obtieneListaResultados();
			
			
			LogDeMidasEJB3.log("Finaliza consulta del reporte. SP invocado: "+nombreSP+", Parametros: "+descripcionParametros+
					"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
			
			return listaResultado;
						
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log("Ocurrio un error al consultar el reporte. SP invocado: "+nombreSP, Level.SEVERE, e); 
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReporteRR6DTO.class, codErr, descErr);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Ocurrio un error al consultar el reporte. SP invocado: "+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}
	
	/**
	 * Procesa la informacion de los reportes RR6.
	 * @param fechaIni
	 * @param fechaFinal
	 * @param tipoCambio
	 * @param reporte
	 * @param nombreUsuario
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesarReporte(String fechaIni, String fechaFinal, BigDecimal tipoCambio, 
							   int reporte, String nombreUsuario) throws Exception {
		
		StoredProcedureHelper storedHelper = null;
		String nombreSP = "";
		
		try {
			
			if (reporte == RTRE){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_PROCESAR_RTRE";
			}else if(reporte == RTRC){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_PROCESAR_RTRC";
			}else if(reporte == RTRF){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_PROCESAR_RTRF";
			}else if(reporte == RTRR){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_PROCESAR_RTRR";
			}else if(reporte == RTRS){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_PROCESAR_RTRS";
			}else if(reporte == RARN){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_PROCESAR_RARN";
			}else if(reporte == RTRI){
				nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_PROCESAR_RTRI";
			}
				
			String[] nombreParametros = {"pfechainicial","pfechafinal","ptipocambio", "pidUsuario"};
			Object[] valorParametros={fechaIni, fechaFinal, tipoCambio, nombreUsuario};
			
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			String descripcionParametros = obtenerDescripcionParametros(nombreParametros,valorParametros);
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			LogDeMidasEJB3.log("Procesando reporte. SP a invocar: "+nombreSP+", Parametros: "+descripcionParametros, Level.INFO, null);
			
			storedHelper.ejecutaActualizar();
			
			LogDeMidasEJB3.log("Finaliza ejecucion del reporte. SP invocado: "+nombreSP+", Parametros: "+descripcionParametros, Level.INFO, null);
			
												
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log("Ocurrio un error al consultar el reporte. SP invocado: "+nombreSP, Level.SEVERE, e); 
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReporteRR6DTO.class, codErr, descErr);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Ocurrio un error al consultar el reporte. SP invocado: "+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}
	
	public String obtenerDescripcionParametros(String[] nombreParametros,
			Object[] valorParametros) {
		StringBuilder descripcionParametros = new StringBuilder("");
		for(int i=0;i<nombreParametros.length;i++){
			descripcionParametros.append(" [").append(nombreParametros[i]).append("] : ").append(valorParametros[i]);
		}
		return descripcionParametros.toString();
	}


	/**
	 * Find all ReporteRR6DTO entities.
	 * @param fechaCorte
	 * @return List<ReporteRR6DTO> all ReporteRR6DTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReporteRR6DTO> findAll(String fechaCorte) {
		Date fCorte = Utilerias.obtenerFechaDeCadena(fechaCorte);
		LogDeMidasEJB3.log(
				"finding all ReporteRR6DTO instances",
				Level.INFO, null);
		try {
			String queryString = "select model from ReporteRR6DTO model where model.fechaFin = ?1 order by model.fechaFin desc";
			Query query = entityManager.createQuery(queryString);
			query.setParameter(1, fCorte, TemporalType.DATE);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find ReporteRR6DTO entities by ID.
	 * @param id 
	 * @return instance ReporteRR6DTO entities
	 */
	public ReporteRR6DTO findById(Integer id) {
		LogDeMidasEJB3.log(
				"finding ReporteRR6DTO instance with id: " + id,
				Level.INFO, null);
		try {
			ReporteRR6DTO instance = entityManager.find(
					ReporteRR6DTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Delete a persistent ReporteRR6DTO entity.
	 * 
	 * @param entity
	 *            ReporteRR6DTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ReporteRR6DTO entity) {
		LogDeMidasEJB3.log("deleting ReporteRR6DTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					ReporteRR6DTO.class, entity
							.getIdcontrato());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find all CargaRR6DTO entities.
	 * @param fechaCorte
	 * @return List<ReporteRR6DTO> all ReporteRR6DTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CargaRR6DTO> findAllCarga(String fechaCorte) {
		Date fCorte = Utilerias.obtenerFechaDeCadena(fechaCorte);
		LogDeMidasEJB3.log(
				"finding all CargaRR6DTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from CargaRR6DTO model where model.fechaFin = ?1 order by model.fechaFin desc";
			Query query = entityManager.createQuery(queryString);
			query.setParameter(1, fCorte, TemporalType.DATE);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * update CargaRR6DTO entities.
	 * @param idCarga
	 * @param estatusCarga
	 */
	public void actualizarEstatusCarga(int idCarga, int estatusCarga) {
		
		LogDeMidasEJB3.log(
				"update CargaRR6DTO instances",
				Level.INFO, null);
		
		
		try {
			StringBuilder queryString=new StringBuilder("");
			queryString.append("UPDATE CargaRR6DTO model");
			queryString.append(" SET model.candado = :paramenterCandado");
			queryString.append(" WHERE model.idcarga = :paramenterIdCarga");
			
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("paramenterCandado", estatusCarga);
			query.setParameter("paramenterIdCarga", idCarga);
			query.executeUpdate();
			entityManager.flush();
		
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update  CargaRR6DTO failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Obtiene la cantidad de registros.
	 * @param fechaInicial
	 * @param fechaFinal
	 * @param tipoCambio
	 * @param reporte
	 * @param nombreUsuario
	 * @return La cantidad de registros
	 */
	public int obtenerRegistros(Date fechaInicial, Date fechaFinal, BigDecimal tipoCambio, 
			   int reporte, String nombreUsuario) {
		Integer registros = 0;
		String tabla = "";
		String fechaini = "";
		String fechaCorte = "";
		
		if (reporte == RTRE){
			tabla = "MIDAS.CNSF_RR6_RTRE";
			fechaCorte = "fcorte";
		}else if(reporte == RTRC){
			tabla = "MIDAS.CNSF_RR6_RTRC";
			fechaCorte = "fcorte";
		}else if(reporte == RTRF){
			tabla = "MIDAS.CNSF_RR6_RTRF";
			fechaini = "fechainicorte";
			fechaCorte = "fechafincorte";
		}else if(reporte == RTRR){
			tabla = "MIDAS.CNSF_RR6_RTRR";
			fechaCorte = "fcorte";
		}else if(reporte == RTRS){
			tabla = "MIDAS.CNSF_RR6_RTRS";
			fechaCorte = "fcorte";
		}else if(reporte == RARN){
			tabla = "MIDAS.CNSF_RR6_RARN";
			fechaini = "fechainicorte";
			fechaCorte = "fechafincorte";
		}else if(reporte == RTRI){
			tabla = "MIDAS.CNSF_RR6_RTRI";
			fechaCorte = "fcorte";
		}
		
		try {		
			
						
			StringBuilder queryString=new StringBuilder("");
			queryString.append("SELECT  count(*) ");
			queryString.append("FROM ");
			queryString.append(tabla);
			queryString.append(" WHERE trunc(");
			queryString.append(fechaCorte);
			queryString.append(") = ?1");
			queryString.append(" AND negocio = 'D'");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(1, fechaFinal, TemporalType.DATE);
			
			registros = ((BigDecimal)query.getSingleResult()).intValue();
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de consultar la tabla "+tabla, Level.SEVERE, e);
		}
		return registros;

	}
	
	/**
	 * Obtiene la información de la Suma Asegurada del reporte RARN.
	 * @param fechaFinal
	 * @return Lista de los registros con Suma Asegurada. 
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> obtenerReporteExcel(Date fechaFinal){
		List<Object[]> listaResultado = null;
		try {	
			StringBuilder queryString=new StringBuilder("");
			
			queryString.append("SELECT rarn.CLAVE_RAMO, rarn.RAMO_SUBRAMO, rarn.LIMITE_MAX_RESP SA_EIQ, SA_RETENIDA SA_RETENIDA_EIQ, ");
			queryString.append("decode(rarn.CLAVE_RAMO,110,0,rarn.LIMITE_MAX_RESP)SA_MIDAS, ");
			queryString.append("decode(rarn.CLAVE_RAMO,110,0,rarn.SA_RETENIDA) SA_RETENIDA_MIDAS, rarn.LIMITE_MAX_RESP-rarn.LIMITE_MAX_RESP DIFERENCIAS FROM( ");
			queryString.append("SELECT decode(det.RAMO_SUBRAMO,6,60,7,50,8,40,5,110,4,110)CLAVE_RAMO, ");
			queryString.append("decode(det.RAMO_SUBRAMO,6,'INCENDIO',7,'MARITIMO Y TRANSPORTES',8,'RESPONSABILIDAD CIVIL',5,'RAMOS TECNICOS',4,'MISCELANEOS')RAMO_SUBRAMO, ");
			queryString.append("SUM(decode(cob.CLAVETIPOSUMAASEGURADA,1, det.LIMITE_MAX_RESP,0)) LIMITE_MAX_RESP, ");
			queryString.append("SUM(decode(cob.CLAVETIPOSUMAASEGURADA,1, det.LIMITE_MAX_RESP,0) * det.retencion/100)SA_RETENIDA ");
			queryString.append("FROM MIDAS.CNSF_SOLVENCIA_DET det, MIDAS.TOCOBERTURA cob ");
			queryString.append("WHERE  det.id_cobertura = cob.idtocobertura(+) ");
			queryString.append("AND ramo_subramo IN (4,5,6,7,8) ");
			queryString.append("AND PRIMA_EMI_DIR_ANUAL > 1 ");
			queryString.append("AND trunc(CORTE) = ?1 ");
			queryString.append("AND FINVIGENCIA != TO_DATE ('01/01/4000','DD/MM/YYYY') ");
			queryString.append("GROUP BY det.ramo_subramo ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT 70, 'CATASTROFICO', MAX(SUMAASEGURADA), MAX(SUMAASEGURADARETENIDA) ");
			queryString.append("FROM MIDAS.CNSF_RR6_RARN ");
			queryString.append("WHERE trunc(FECHAFINCORTE) = ?1 ");
			queryString.append("AND RAMO = 70 ");
			queryString.append(")rarn ORDER BY rarn.CLAVE_RAMO ASC ");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(1, fechaFinal, TemporalType.DATE);
						
			listaResultado = query.getResultList();
			
							
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de obtenerReporteExcel ", Level.SEVERE, e);
		}
		return listaResultado;						
		
	}
	
	/**
	 * Obtiene la información de los archivos cargados.
	 * @param fechaFinal
	 * @param tipoArchivo
	 * @return Lista con en estatus de los archivos cargados. 
	 */
	@SuppressWarnings("unchecked")
	public List<String> obtenerArchivos(Date fechaFinal, int tipoArchivo){
		List<String> listaResultado = null;
		String reporte = "";
		
		if(tipoArchivo == RTRE){
			reporte = "RTRE";
		}else if(tipoArchivo == RTRC){
			reporte = "RTRC";
		}else if(tipoArchivo == RTRF){
			reporte = "RTRF";
		}else if(tipoArchivo == RTRR){
			reporte = "RTRR";
		}else if(tipoArchivo == RTRS){
			reporte = "RTRS";
		}else if(tipoArchivo == RARN){
			reporte = "RARN";
		}else if(tipoArchivo == RTRI){
			reporte = "RTRI";
		}
		
		try {	
			StringBuilder queryString=new StringBuilder("");
			
			queryString.append("SELECT rr6.REPORTE FROM MIDAS.CNSF_RR6_ESTATUS_CARGA rr6 ");
			queryString.append("WHERE  trunc(rr6.FECHAFIN) = ?1 ");
			queryString.append("AND rr6.REPORTE LIKE ?2 ");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(1, fechaFinal, TemporalType.DATE);
			query.setParameter(2, "%" + reporte + "%");
						
			listaResultado = (List<String>) query.getResultList();
			
							
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de obtenerReporteExcel ", Level.SEVERE, e);
		}
		return listaResultado;						
		
	}
	
	/**
	 * Obtiene la cantidad de registros.
	 * @param fechaInicial
	 * @param fechaFinal
	 * @param tipoCambio
	 * @param reporte
	 * @param nombreUsuario
	 * @return La cantidad de registros
	 */
	public int obtenerRegistrosSA(Date fechaFinal) {
		Integer registros = 0;
						
		try {			
			StringBuilder queryString=new StringBuilder("");
			queryString.append("SELECT  count(*) ");
			queryString.append("FROM MIDAS.CNSF_SOLVENCIA_DET ");
			queryString.append("WHERE trunc(corte) = ?1");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(1, fechaFinal, TemporalType.DATE);
			
			registros = ((BigDecimal)query.getSingleResult()).intValue();
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de consultar la tabla MIDAS.CNSF_SOLVENCIA_DET", Level.SEVERE, e);
		}
		return registros;

	}
		
}
