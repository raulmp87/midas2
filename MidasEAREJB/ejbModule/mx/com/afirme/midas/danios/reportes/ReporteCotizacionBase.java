package mx.com.afirme.midas.danios.reportes;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.transaction.SystemException;

import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVAFacadeRemote;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaFacadeRemote;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoFacadeRemote;
import mx.com.afirme.midas.consultas.formapago.FormaPagoInterfazServiciosRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotId;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.resumen.ResumenCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.resumen.SoporteResumen;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.danios.cliente.ClienteServiciosRemote;
import mx.com.afirme.midas.danios.reportes.bean.pago.EsquemaPagosDTO;
import mx.com.afirme.midas.danios.reportes.bean.pago.ReciboDTO;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionServiciosRemote;
import mx.com.afirme.midas.direccion.DireccionFacadeRemote;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.endoso.EndosoInterfazServiciosRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;

public abstract class ReporteCotizacionBase extends MidasReporteBase{
	protected BigDecimal idToCotizacion;
	protected CotizacionDTO cotizacionDTO;
	protected MonedaDTO monedaDTO;
	protected FormaPagoIDTO formaPagoIDTO;
	protected List<SeccionCotizacionDTO> listaSeccionesContratadasCotizacion;
	protected Map<Integer,List<SeccionCotizacionDTO>> mapaSeccionesContratadasPorNumeroInciso;
	
	protected List<CoberturaCotizacionDTO> listaCoberturasContratadasCotizacion;
	protected Map<Integer[],List<CoberturaCotizacionDTO>> mapaCoberturasContratadasPorSeccion;
	protected Map<BigDecimal,List<CoberturaCotizacionDTO>> mapaCoberturasContratadasBasicasPorInciso;
	
	protected List<RiesgoCotizacionDTO> listaRiesgosContratadosCotizacion;
	protected Map<BigDecimal[],List<RiesgoCotizacionDTO>> mapaRiesgosContratadosPorSeccionIncisoCobertura;
	
	protected List<SubIncisoCotizacionDTO> listaSubIncisosCotizacion;
	protected Map<BigDecimal[],List<SubIncisoCotizacionDTO>> mapaSubIncisosPorSeccionInciso;
	protected Map<BigDecimal,List<SubIncisoCotizacionDTO>> mapaSubIncisosPorInciso;
	
	protected List<MovimientoCotizacionEndosoDTO> movimientosCotizacion;
	
	protected List<IncisoCotizacionDTO> listaIncisos;
	protected List<AgrupacionCotDTO> listaAgrupacionCotizacion;
	
	private String numeroCotizacion;
	protected BigDecimal idTcRamo;
	protected BigDecimal idTcSubRamo;
	protected boolean esEndoso;
	
	protected ClienteDTO asegurado;
	protected ClienteDTO contratante;
	
	protected List<ConfiguracionDatoIncisoCotizacionDTO> listaConfiguracionDatosIncisoCotizacion;
	protected Map<BigDecimal[],List<ConfiguracionDatoIncisoCotizacionDTO>> mapaConfiguracionDatosIncisoCotPorIdRamoIdSubRamo;
	protected List<SubRamoDTO> listaSubRamosContratados;
	
	protected CotizacionFacadeRemote cotizacionFacade ;//= ServiceLocatorP.getInstance().getEJB(CotizacionFacadeRemote.class);
	protected SeccionCotizacionFacadeRemote seccionCotizacionFacade;
	protected CoberturaCotizacionFacadeRemote coberturaCotizacionFacade;
	protected RiesgoCotizacionFacadeRemote riesgoCotizacionFacade;
	protected SubIncisoCotizacionFacadeRemote subIncisoCotFacade;
	protected IncisoCotizacionFacadeRemote incisoCotFacade;
	protected ConfiguracionDatoIncisoCotizacionFacadeRemote configuracionDatoIncisoCotFacade;
	protected DatoIncisoCotizacionFacadeRemote datoIncisoCotFacade;
	protected MonedaFacadeRemote monedaFacade;
	protected ClienteServiciosRemote clienteServicios;
	protected FormaPagoInterfazServiciosRemote formaPagoInterfazFacade;
	protected EndosoFacadeRemote endosoFacade;
	protected EndosoInterfazServiciosRemote endosoInterfazServicios;
	protected CodigoPostalIVAFacadeRemote codigoPostalIVAFacade;
	protected MovimientoCotizacionEndosoFacadeRemote movimientoCotEndFacade;
	protected SeccionFacadeRemote seccionFacade;
	protected DocAnexoCotFacadeRemote docAnexoCotFacade;
	protected TexAdicionalCotFacadeRemote texAdicionalCotFacade;
	protected ResumenCotizacionFacadeRemote resumenCotizacionFacade;	
	protected AgrupacionCotFacadeRemote agrupacionCotizacionFacade;
	protected FormaPagoFacadeRemote formaPagoFacade;
	protected CatalogoValorFijoFacadeRemote catalogoValorFijoFacade;
	protected DireccionFacadeRemote direccionFacade;
	protected ReporteCotizacionServiciosRemote reporteCotizacionServicios;
	
	{
		reporteCotizacionServicios = ServiceLocatorP.getInstance().getEJB(ReporteCotizacionServiciosRemote.class);
		cotizacionFacade = ServiceLocatorP.getInstance().getEJB(CotizacionFacadeRemote.class);
		seccionCotizacionFacade = ServiceLocatorP.getInstance().getEJB(SeccionCotizacionFacadeRemote.class);
		coberturaCotizacionFacade = ServiceLocatorP.getInstance().getEJB(CoberturaCotizacionFacadeRemote.class);
		riesgoCotizacionFacade = ServiceLocatorP.getInstance().getEJB(RiesgoCotizacionFacadeRemote.class);
		subIncisoCotFacade = ServiceLocatorP.getInstance().getEJB(SubIncisoCotizacionFacadeRemote.class);
		incisoCotFacade = ServiceLocatorP.getInstance().getEJB(IncisoCotizacionFacadeRemote.class);
		configuracionDatoIncisoCotFacade = ServiceLocatorP.getInstance().getEJB(ConfiguracionDatoIncisoCotizacionFacadeRemote.class);
		monedaFacade = ServiceLocatorP.getInstance().getEJB(MonedaFacadeRemote.class);
		clienteServicios = ServiceLocatorP.getInstance().getEJB(ClienteServiciosRemote.class);
		formaPagoInterfazFacade = ServiceLocatorP.getInstance().getEJB(FormaPagoInterfazServiciosRemote.class);
		endosoFacade = ServiceLocatorP.getInstance().getEJB(EndosoFacadeRemote.class);
		endosoInterfazServicios = ServiceLocatorP.getInstance().getEJB(EndosoInterfazServiciosRemote.class);
		codigoPostalIVAFacade = ServiceLocatorP.getInstance().getEJB(CodigoPostalIVAFacadeRemote.class);
		movimientoCotEndFacade = ServiceLocatorP.getInstance().getEJB(MovimientoCotizacionEndosoFacadeRemote.class);
		seccionFacade = ServiceLocatorP.getInstance().getEJB(SeccionFacadeRemote.class);
		docAnexoCotFacade = ServiceLocatorP.getInstance().getEJB(DocAnexoCotFacadeRemote.class);
		texAdicionalCotFacade = ServiceLocatorP.getInstance().getEJB(TexAdicionalCotFacadeRemote.class);
		resumenCotizacionFacade = ServiceLocatorP.getInstance().getEJB(ResumenCotizacionFacadeRemote.class);
		agrupacionCotizacionFacade = ServiceLocatorP.getInstance().getEJB(AgrupacionCotFacadeRemote.class);
		formaPagoFacade = ServiceLocatorP.getInstance().getEJB(FormaPagoFacadeRemote.class);
		catalogoValorFijoFacade = ServiceLocatorP.getInstance().getEJB(CatalogoValorFijoFacadeRemote.class);
		datoIncisoCotFacade = ServiceLocatorP.getInstance().getEJB(DatoIncisoCotizacionFacadeRemote.class);
		direccionFacade = ServiceLocatorP.getInstance().getEJB(DireccionFacadeRemote.class);
	}
	
//	public ReporteCotizacionBase(ReporteCotizacionServiciosRemote reporteCotizacionServicios,
//			CotizacionFacadeRemote cotizacionFacade ,
//			SeccionCotizacionFacadeRemote seccionCotizacionFacade,
//			CoberturaCotizacionFacadeRemote coberturaCotizacionFacade,
//			RiesgoCotizacionFacadeRemote riesgoCotizacionFacade,
//			SubIncisoCotizacionFacadeRemote subIncisoCotFacade,
//			IncisoCotizacionFacadeRemote incisoCotFacade,
//			ConfiguracionDatoIncisoCotizacionFacadeRemote configuracionDatoIncisoCotFacade,
//			MonedaFacadeRemote monedaFacade,
//			ClienteServiciosRemote clienteServicios,
//			FormaPagoInterfazServiciosRemote formaPagoInterfazFacade,
//			EndosoFacadeRemote endosoFacade,
//			EndosoInterfazServiciosRemote endosoInterfazServicios,
//			CodigoPostalIVAFacadeRemote codigoPostalIVAFacade,
//			MovimientoCotizacionEndosoFacadeRemote movimientoCotEndFacade,
//			SeccionFacadeRemote seccionFacade,
//			DocAnexoCotFacadeRemote docAnexoCotFacade,
//			TexAdicionalCotFacadeRemote texAdicionalCotFacade,
//			ResumenCotizacionFacadeRemote resumenCotizacionFacade,
//			AgrupacionCotFacadeRemote agrupacionCotizacionFacade,
//			FormaPagoFacadeRemote formaPagoFacade,
//			CatalogoValorFijoFacadeRemote catalogoValorFijoFacade,
//			DatoIncisoCotizacionFacadeRemote datoIncisoCotFacade,
//			DireccionFacadeRemote direccionFacade) throws SystemException{
//		this.cotizacionFacade = cotizacionFacade;
//		this.seccionCotizacionFacade = seccionCotizacionFacade;
//		this.coberturaCotizacionFacade = coberturaCotizacionFacade;
//		this.riesgoCotizacionFacade = riesgoCotizacionFacade;
//		this.subIncisoCotFacade = subIncisoCotFacade;
//		this.incisoCotFacade = incisoCotFacade;
//		this.configuracionDatoIncisoCotFacade = configuracionDatoIncisoCotFacade;
//		this.monedaFacade = monedaFacade;
//		this.clienteServicios = clienteServicios;
//		this.formaPagoInterfazFacade = formaPagoInterfazFacade;
//		this.endosoFacade = endosoFacade;
//		this.endosoInterfazServicios = endosoInterfazServicios;
//		this.codigoPostalIVAFacade = codigoPostalIVAFacade;
//		this.movimientoCotEndFacade = movimientoCotEndFacade;
//		this.seccionFacade = seccionFacade;
//		this.docAnexoCotFacade = docAnexoCotFacade;
//		this.texAdicionalCotFacade = texAdicionalCotFacade;
//		this.resumenCotizacionFacade = resumenCotizacionFacade;
//		this.agrupacionCotizacionFacade = agrupacionCotizacionFacade;
//		this.formaPagoFacade = formaPagoFacade;
//		this.catalogoValorFijoFacade =catalogoValorFijoFacade;
//		this.datoIncisoCotFacade = datoIncisoCotFacade;
//		this.direccionFacade = direccionFacade;
//		this.reporteCotizacionServicios = reporteCotizacionServicios;
//	}
	
	public ReporteCotizacionBase() throws SystemException{
		
	}
	
	/**
	 * Declaraci�n de constantes utilizadas en las plantillas.
	 */
	public static final String PRIMER_RIESGO_RELATIVO = "Primer Riesgo Relativo";
	public static final String ABREVIATURAS_LUC = "L.U.C.";
	public static final String SECCION_NO_DISPONIBLE = "Secci�n no disponible";
	public static final String AMPARADA = "Amparada";
	public static final String TITULO_SUBINCISOS_SECCION = "SUBINCISOS DE LA SECCI�N ";
	public static final String SA_SEGUN_RELACION_ADJUNTA = "S/A seg�n relaci�n adjunta";
	public static final String UBICACION = "Ubicaci�n";
	public static final String BIENES = "Bien(es)";
	public static final String NO_DISPONIBLE = "NO DISPONIBLE";
	
	/**
	 * Consulta la informaci�n completa de la cotizaci�n, incluyendo sus secciones, coberturas y riesgos contratados, adem�s de 
	 * sus incisos, subincisos y la lista de configuraciones de datos de riesgo que deben ser mostradas en la impresi�n.
	 * Las listas son guardadas en los atributos del ReporteBase.
	 * @param nombreUsuario
	 * @throws SystemException
	 */
	public void consultarInformacionCotizacion(String nombreUsuario) {
		if (cotizacionDTO == null && idToCotizacion != null)
			cotizacionDTO = cotizacionFacade.findById(idToCotizacion);
		listaSeccionesContratadasCotizacion = seccionCotizacionFacade.listarSeccionesContratadasPorCotizacion(idToCotizacion,(short)1,true);
		listaCoberturasContratadasCotizacion = coberturaCotizacionFacade.listarCoberturasContratadas(idToCotizacion,true);
		listaRiesgosContratadosCotizacion = riesgoCotizacionFacade.listarRiesgoContratadosPorCotizacion(idToCotizacion);
		listaSubIncisosCotizacion = subIncisoCotFacade.findByProperty("id.idToCotizacion", idToCotizacion);
		listaIncisos = incisoCotFacade.findByCotizacionId(idToCotizacion);
		listarSubRamosContratados();
		obtenerListaAgrupacionCotizacion();
		obtenerConfiguracionDatosIncisoImprimibles();
	}
	
	protected List<AgrupacionCotDTO> obtenerListaAgrupacionCotizacion(){
		if(listaAgrupacionCotizacion == null)
			listaAgrupacionCotizacion = agrupacionCotizacionFacade.findByProperty("cotizacionDTO.idToCotizacion", idToCotizacion);
		return listaAgrupacionCotizacion;
	}
	
	protected AgrupacionCotDTO obtenerAgrupacionCotizacion(BigDecimal idToCotizacion,Short claveTipoAgrupacion){
		return agrupacionCotizacionFacade.buscarPorCotizacion(idToCotizacion, claveTipoAgrupacion);
	}
	
	protected List<CoberturaCotizacionDTO> listarCoberturasFiltrado(CoberturaCotizacionDTO coberturaCotFiltro){
		return coberturaCotizacionFacade.listarFiltrado(coberturaCotFiltro);
	}
	
	protected SeccionDTO buscarSeccionDTOPorId(BigDecimal idToSeccion){
		return seccionFacade.findById(idToSeccion);
	}
	
	protected ClienteDTO buscarClientePorId(BigDecimal idCliente,String claveUsuario) throws Exception{
		return clienteServicios.findById(idCliente, claveUsuario);
	}
	
	protected int contarIncisosPorCotizacion(BigDecimal idToCotizacion){
		return incisoCotFacade.contarIncisosPorCotizacion(idToCotizacion);
	}
	
	protected String obtenerDescripcionCatalogoValorFijo(Integer grupoValores,Short idDato){
		return catalogoValorFijoFacade.getDescripcionCatalogoValorFijo(grupoValores, idDato);
	}
	
	protected FormaPagoDTO buscarFormaPagoPorId(Integer idFormaPago){
		FormaPagoDTO formaPagoDTO = new FormaPagoDTO();
		formaPagoDTO.setIdFormaPago(idFormaPago);
		return formaPagoFacade.findById(formaPagoDTO);
	}
	
	protected DatoIncisoCotizacionFacadeRemote obtenerDatoIncisoCotFacade(){
		return this.datoIncisoCotFacade;
	}
	
	public ConfiguracionDatoIncisoCotizacionFacadeRemote obtenerConfigDatoIncisoCotFacade(){
		return this.configuracionDatoIncisoCotFacade;
	}
	
	public List<ConfiguracionDatoIncisoCotizacionDTO> obtenerConfiguracionDatosIncisoImprimibles() {
		if(listaConfiguracionDatosIncisoCotizacion == null){
			listaConfiguracionDatosIncisoCotizacion = new ArrayList<ConfiguracionDatoIncisoCotizacionDTO>();
			for(SubRamoDTO subRamoDTO : listarSubRamosContratados()){
				ConfiguracionDatoIncisoCotizacionDTO configuracionDatoFiltro = new ConfiguracionDatoIncisoCotizacionDTO((short)0,null,
							subRamoDTO.getRamoDTO().getIdTcRamo(),subRamoDTO.getIdTcSubRamo(),null);
				configuracionDatoFiltro.setClaveimpresionpoliza((short)1);
				List<ConfiguracionDatoIncisoCotizacionDTO> listaConfigDatos = configuracionDatoIncisoCotFacade.listarFiltrado(configuracionDatoFiltro);
				if(listaConfigDatos != null){
					listaConfiguracionDatosIncisoCotizacion.addAll(listaConfigDatos);
				}
			}
		}
		return this.listaConfiguracionDatosIncisoCotizacion;
	}
	
	public List<ConfiguracionDatoIncisoCotizacionDTO> obtenerConfiguracionDatosIncisoImprimibles(BigDecimal idTcRamo,BigDecimal idTcSubRamo) throws SystemException{
		if(idTcRamo == null || idTcSubRamo == null)
			return null;
		obtenerConfiguracionDatosIncisoImprimibles();
		if(mapaConfiguracionDatosIncisoCotPorIdRamoIdSubRamo == null)
			mapaConfiguracionDatosIncisoCotPorIdRamoIdSubRamo = new HashMap<BigDecimal[], List<ConfiguracionDatoIncisoCotizacionDTO>>();
		BigDecimal []key = {idTcRamo,idTcSubRamo};
		List<ConfiguracionDatoIncisoCotizacionDTO> listaConfDatoPorRamo_SubRamo = mapaConfiguracionDatosIncisoCotPorIdRamoIdSubRamo.get(key);
		if(listaConfDatoPorRamo_SubRamo == null){
			listaConfDatoPorRamo_SubRamo = new ArrayList<ConfiguracionDatoIncisoCotizacionDTO>();
			for(ConfiguracionDatoIncisoCotizacionDTO configDatoTMP : listaConfiguracionDatosIncisoCotizacion){
				if(configDatoTMP.getId().getIdTcRamo().compareTo(idTcRamo) == 0 && configDatoTMP.getId().getIdTcSubramo().compareTo(idTcSubRamo) == 0)
					listaConfDatoPorRamo_SubRamo.add(configDatoTMP);
			}
			mapaConfiguracionDatosIncisoCotPorIdRamoIdSubRamo.put(key, listaConfDatoPorRamo_SubRamo);
		}
		return listaConfDatoPorRamo_SubRamo;
	}
	
	/**
	 * Consulta la lista de incisos de la cotizaci�n.
	 * @throws SystemException
	 */
	public void consultarIncisosCotizacion() throws mx.com.afirme.midas.sistema.SystemException{
		listaIncisos = incisoCotFacade.findByCotizacionId(idToCotizacion);
	}
	
	/**
	 * Consulta la lista de coberturas contratadas (claveContrato = 1) de la cotizaci�n.
	 * @throws SystemException
	 */
	public void consultarCoberturasContratadasCotizacion() {
		listaCoberturasContratadasCotizacion = coberturaCotizacionFacade.listarCoberturasContratadas(idToCotizacion);
	}
	
	public List<SeccionCotizacionDTO> obtenerSeccionesContratadas(BigDecimal numeroInciso){
		if (this.listaSeccionesContratadasCotizacion != null){
			if (mapaSeccionesContratadasPorNumeroInciso == null){
				mapaSeccionesContratadasPorNumeroInciso = new HashMap<Integer,List<SeccionCotizacionDTO>>();
			}
			Integer keyNumeroInciso = Integer.valueOf(numeroInciso.toString());
			List<SeccionCotizacionDTO> listaSeccionesContratadasPorInciso = mapaSeccionesContratadasPorNumeroInciso.get(keyNumeroInciso);
			if (listaSeccionesContratadasPorInciso == null){
				listaSeccionesContratadasPorInciso = new ArrayList<SeccionCotizacionDTO>();
				for(SeccionCotizacionDTO seccionCot : listaSeccionesContratadasCotizacion){
					if (seccionCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0)
						listaSeccionesContratadasPorInciso.add(seccionCot);
				}
				mapaSeccionesContratadasPorNumeroInciso.put(keyNumeroInciso, listaSeccionesContratadasPorInciso);
			}
			return listaSeccionesContratadasPorInciso;
		}
		else
			return null;
	}
	
	/**
	 * Regresa la lista de coberturas contratadas por seccion e inciso.
	 * @param numeroInciso
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasContratadas(BigDecimal idToSeccion,BigDecimal numeroInciso){
		if (this.listaCoberturasContratadasCotizacion != null){
			if (mapaCoberturasContratadasPorSeccion == null){
				mapaCoberturasContratadasPorSeccion = new HashMap<Integer[],List<CoberturaCotizacionDTO>>();
			}
			Integer keySeccion = Integer.valueOf(idToSeccion.toString());
			Integer keyNumeroInciso = Integer.valueOf(numeroInciso.toString());
			Integer[] keyCoberturasContratadas = new Integer[2];
			keyCoberturasContratadas[0] = keyNumeroInciso;
			keyCoberturasContratadas[1] = keySeccion;
			List<CoberturaCotizacionDTO> listaCoberturasContratadasPorSeccion = mapaCoberturasContratadasPorSeccion.get(keyCoberturasContratadas);
			if (listaCoberturasContratadasPorSeccion == null){
				listaCoberturasContratadasPorSeccion = new ArrayList<CoberturaCotizacionDTO>();
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
					if (coberturaCot.getId().getIdToSeccion().compareTo(idToSeccion) == 0 && coberturaCot.getId().getNumeroInciso().compareTo(numeroInciso) ==0)
						listaCoberturasContratadasPorSeccion.add(coberturaCot);
				}
				mapaCoberturasContratadasPorSeccion.put(keyCoberturasContratadas, listaCoberturasContratadasPorSeccion);
			}
			return listaCoberturasContratadasPorSeccion;
		}
		else
			return null;
	}
	
	/**
	 * Regresa la lista de coberturas contratadas B�sicas n�mero de inciso.
	 * @param numeroInciso
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasContratadasBasicasPorInciso(BigDecimal numeroInciso){
		if (this.listaCoberturasContratadasCotizacion != null){
			if (mapaCoberturasContratadasBasicasPorInciso == null){
				mapaCoberturasContratadasBasicasPorInciso = new HashMap<BigDecimal,List<CoberturaCotizacionDTO>>();
			}
			List<CoberturaCotizacionDTO> listaCoberturasContratadasBasicasPorInciso = mapaCoberturasContratadasBasicasPorInciso.get(numeroInciso);
			if (listaCoberturasContratadasBasicasPorInciso == null){
				listaCoberturasContratadasBasicasPorInciso = new ArrayList<CoberturaCotizacionDTO>();
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
					if (coberturaCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0 && 
							coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_BASICA))
						listaCoberturasContratadasBasicasPorInciso.add(coberturaCot);
				}
				mapaCoberturasContratadasBasicasPorInciso.put(numeroInciso, listaCoberturasContratadasBasicasPorInciso);
			}
			return listaCoberturasContratadasBasicasPorInciso;
		}
		else
			return null;
	}
	
	/**
	 * Regresa la sumatoria de las sumas aseguradas de las coberturas contratadas 
	 * B�sicas pertenecientes al n�mero de inciso recibido.
	 * @param numeroInciso
	 * @return List<CoberturaCotizacionDTO>
	 */
	public Double obtenerSumatoriaSACoberturasBasicasPorInciso(BigDecimal numeroInciso){
		double sumatoriaSABasicas = 0d;
		List<CoberturaCotizacionDTO> coberturasBasicasInciso = obtenerCoberturasContratadasBasicasPorInciso(numeroInciso);
		if (coberturasBasicasInciso != null)
			for(CoberturaCotizacionDTO coberturaCot : coberturasBasicasInciso)
				sumatoriaSABasicas += coberturaCot.getValorSumaAsegurada().doubleValue();
		return sumatoriaSABasicas;
	}
	
	public List<RiesgoCotizacionDTO> obtenerRiesgosContratados(BigDecimal idToSeccion,BigDecimal numeroInciso,BigDecimal idToCobertura){
		if (this.listaRiesgosContratadosCotizacion != null){
			if (mapaRiesgosContratadosPorSeccionIncisoCobertura == null){
				mapaRiesgosContratadosPorSeccionIncisoCobertura = new HashMap<BigDecimal[],List<RiesgoCotizacionDTO>>();
			}
			BigDecimal[] keySeccionIncisoCobertura = new BigDecimal[3];
			keySeccionIncisoCobertura[0] = idToSeccion;
			keySeccionIncisoCobertura[1] = numeroInciso;
			keySeccionIncisoCobertura[2] = idToCobertura;
			List<RiesgoCotizacionDTO> listaRiesgosContratadosPorSeccionIncisoCobertura = mapaRiesgosContratadosPorSeccionIncisoCobertura.get(keySeccionIncisoCobertura);
			if (listaRiesgosContratadosPorSeccionIncisoCobertura == null){
				listaRiesgosContratadosPorSeccionIncisoCobertura = new ArrayList<RiesgoCotizacionDTO>();
				for(RiesgoCotizacionDTO riesgoCot : listaRiesgosContratadosCotizacion){
					if (riesgoCot.getId().getIdToSeccion().compareTo(idToSeccion) == 0 
							&& riesgoCot.getId().getNumeroInciso().compareTo(numeroInciso)==0 
								&& riesgoCot.getId().getIdToCobertura().compareTo(idToCobertura)==0)
						listaRiesgosContratadosPorSeccionIncisoCobertura .add(riesgoCot);
				}
				mapaRiesgosContratadosPorSeccionIncisoCobertura.put(keySeccionIncisoCobertura, listaRiesgosContratadosPorSeccionIncisoCobertura );
			}
			return listaRiesgosContratadosPorSeccionIncisoCobertura ;
		}
		else
			return null;
	}
	
	public List<SubIncisoCotizacionDTO> obtenerSubIncisos(BigDecimal idToSeccion,BigDecimal numeroInciso){
		if (this.listaSubIncisosCotizacion != null){
			if (mapaSubIncisosPorSeccionInciso == null){
				mapaSubIncisosPorSeccionInciso = new HashMap<BigDecimal[],List<SubIncisoCotizacionDTO>>();
			}
			BigDecimal[] keySeccionInciso = new BigDecimal[2];
			keySeccionInciso[0] = idToSeccion;
			keySeccionInciso[1] = numeroInciso;
			List<SubIncisoCotizacionDTO> listaSubIncisosPorSeccionInciso = mapaSubIncisosPorSeccionInciso.get(keySeccionInciso);
			if (listaSubIncisosPorSeccionInciso == null){
				listaSubIncisosPorSeccionInciso = new ArrayList<SubIncisoCotizacionDTO>();
				for(SubIncisoCotizacionDTO subIncisoCot : listaSubIncisosCotizacion){
					if (subIncisoCot.getId().getIdToSeccion().compareTo(idToSeccion) == 0 
							&& subIncisoCot.getId().getNumeroInciso().compareTo(numeroInciso)==0 )
						listaSubIncisosPorSeccionInciso.add(subIncisoCot);
				}
				mapaSubIncisosPorSeccionInciso.put(keySeccionInciso, listaSubIncisosPorSeccionInciso);
			}
			return listaSubIncisosPorSeccionInciso;
		}
		else
			return null;
	}
	
	public List<SubIncisoCotizacionDTO> obtenerSubIncisosPorInciso(BigDecimal numeroInciso){
		if (this.listaSubIncisosCotizacion != null){
			if (mapaSubIncisosPorInciso == null){
				mapaSubIncisosPorInciso = new HashMap<BigDecimal,List<SubIncisoCotizacionDTO>>();
			}
			List<SubIncisoCotizacionDTO> listaSubIncisosPorInciso = mapaSubIncisosPorInciso.get(numeroInciso);
			if (listaSubIncisosPorInciso == null){
				listaSubIncisosPorInciso = new ArrayList<SubIncisoCotizacionDTO>();
				for(SubIncisoCotizacionDTO subIncisoCot : listaSubIncisosCotizacion){
					if (subIncisoCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0 )
						listaSubIncisosPorInciso.add(subIncisoCot);
				}
				mapaSubIncisosPorInciso.put(numeroInciso, listaSubIncisosPorInciso);
			}
			return listaSubIncisosPorInciso;
		}
		else
			return null;
	}
	
	/**
	 * Genera el Mapa con los par�metros que utilizan todas las plantillas. Los parametros espec�ficos son calculados dentro 
	 * de cada plantilla que los requiere.
	 * @param nombreUsuario
	 * @param Boolean aplicaDiasPorDevengar. Indica si se debe distribuir el monto de la prima entre los dias de vigencia
	 * @throws SystemException 
	 */
	protected void poblarParametrosComunes(String nombreUsuario,boolean poblarCuadriculaTotales) throws mx.com.afirme.midas.sistema.SystemException{
		//Los siguientes par�metros son comunes para todas las plantillas:
		
		String NO_DISPONIBLE = "--------";
		
		getMapaParametrosGeneralesPlantillas().put("URL_LOGO_AFIRME", ConstantesReporte.LOGOTIPO_SEGUROS_AFIRME);
		getMapaParametrosGeneralesPlantillas().put("URL_IMAGEN_FONDO", ConstantesReporte.FONDO_PLANTILLA_COTIZACION);
		numeroCotizacion = Utilerias.llenarIzquierda(cotizacionDTO.getIdToCotizacion().toBigInteger().toString(), "0", 8);
		getMapaParametrosGeneralesPlantillas().put("NO_COTIZACION", "COT-" + numeroCotizacion);
	    
		if (cotizacionDTO.getIdMoneda() != null){
			try {
				if(monedaDTO == null)
					monedaDTO = monedaFacade.findById(new Short(cotizacionDTO.getIdMoneda().toBigInteger().toString()));
				getMapaParametrosGeneralesPlantillas().put("MONEDA", ((monedaDTO != null)?monedaDTO.getDescripcion() : NO_DISPONIBLE));
			} catch (Exception e) {
				getMapaParametrosGeneralesPlantillas().put("MONEDA", NO_DISPONIBLE);
			}
			
		}
		Format formatter = new SimpleDateFormat("dd/MM/yyyy");
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0){
			getMapaParametrosGeneralesPlantillas().put("FECHA_INICIO", formatter.format(cotizacionDTO.getFechaInicioVigencia()));
			esEndoso = true;
		}else{
			esEndoso = false;
			if(cotizacionDTO.getFechaInicioVigencia() != null){
				getMapaParametrosGeneralesPlantillas().put("FECHA_INICIO", formatter.format(cotizacionDTO.getFechaInicioVigencia()));
			}else{
				getMapaParametrosGeneralesPlantillas().put("FECHA_INICIO", NO_DISPONIBLE);
			}
			
		}
		if ( cotizacionDTO.getFechaFinVigencia() != null){
			if (formatter == null) formatter = new SimpleDateFormat("dd/MM/yyyy"); 
			getMapaParametrosGeneralesPlantillas().put("FECHA_FIN", formatter.format(cotizacionDTO.getFechaFinVigencia()));
		}
		else
			getMapaParametrosGeneralesPlantillas().put("FECHA_FIN", NO_DISPONIBLE);
//		consultarAseguradoContratante(nombreUsuario);

		
		int totalIncisos = 0;
		if (listaIncisos == null)
			consultarIncisosCotizacion();
		totalIncisos = listaIncisos.size();
		getMapaParametrosGeneralesPlantillas().put("TOTAL_INCISOS", ""+totalIncisos);
		
		///////DATOS PARA LA CUADR�CULA DE TOTALES
		////Datos para el primer rengl�n (totales globales)
		NumberFormat fMonto = new DecimalFormat("$#,##0.00");
		//Sumatoria de las primas netas
		Double sumatoriaPrimaNeta = 0d;
//		for(IncisoCotizacionDTO incisoTMP : listaIncisos){
//			sumatoriaPrimaNeta += incisoTMP.getValorPrimaNeta();
//		}
		if(listaCoberturasContratadasCotizacion == null)
			consultarCoberturasContratadasCotizacion();
		//FIXME cambiar por metodo nuevo que sea especial para endosos
		
		cotizacionDTO = resumenCotizacionFacade.calcularTotalesResumenCotizacion(cotizacionDTO, listaCoberturasContratadasCotizacion);
//		sumatoriaPrimaNeta = cotizacionDTO.getPrimaNetaAnual();
//		if (aplicaDiasPorDevengar)
			sumatoriaPrimaNeta = cotizacionDTO.getPrimaNetaCotizacion();
		BigDecimal totalPrimaNeta;
		totalPrimaNeta = new BigDecimal(sumatoriaPrimaNeta);
		String subTotalPrimaNetaString = fMonto.format(totalPrimaNeta);
		getMapaParametrosGeneralesPlantillas().put("SEMI_TOTAL_PRIMA_NETA", subTotalPrimaNetaString);
		
		//Recargo
		
		formaPagoIDTO = obtenerFormaPagoCotizacion(nombreUsuario);// formaPagoInterfazFacade.getPorId(Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()),Short.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()),nombreUsuario);
		
		BigDecimal totalRecargo;
		if(formaPagoIDTO != null){
			getMapaParametrosGeneralesPlantillas().put("FORMA_PAGO", formaPagoIDTO.getDescripcion());
			if(poblarCuadriculaTotales){
				totalRecargo = BigDecimal.valueOf(cotizacionDTO.getMontoRecargoPagoFraccionado());
				String totalRecargoString = fMonto.format(totalRecargo);
				getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", totalRecargoString);
				
				BigDecimal totalDerecho = BigDecimal.ZERO;
				String totalDerechoString = NO_DISPONIBLE;
	
				totalDerecho = new BigDecimal(cotizacionDTO.getDerechosPoliza());
				totalDerechoString = fMonto.format(totalDerecho);
				getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", totalDerechoString);
	
				//03/01/2010. Se debe agregar el IVA como par�metro para las plantillas, se muestra en la cuadr�cula de totales
				getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA", "IVA ("+String.format("%.0f", cotizacionDTO.getFactorIVA())+"%)");
				BigDecimal totalIva = BigDecimal.valueOf(cotizacionDTO.getMontoIVA());
				String totalIvaString = fMonto.format(totalIva);
				getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", totalIvaString);
				
				BigDecimal totalPrima = BigDecimal.ZERO;
				totalPrima = totalPrimaNeta.add(totalRecargo).add(totalDerecho).add(totalIva);
				String totalPrimaNetaString = fMonto.format(totalPrima);
				getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", totalPrimaNetaString);
				
				/*
				 * 06/04/2011 JL. Se agrega c�lculo de los recibos para mostrarlos en la cotizacion Casa
				 */
				calcularMontoPrimerRecibo(totalPrimaNeta, totalRecargo, new BigDecimal(cotizacionDTO.getFactorIVA()).divide(new BigDecimal("100",new MathContext(10, RoundingMode.HALF_UP))), totalIva, totalPrima, totalDerecho);
			}
			else{
				getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", "");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", "");
				getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA","");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", "");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", "");
			}
			
		}//fin validar formaPagoIDTO != null
		else{
			String error = "Error de comunicaci�n con el sistema Seycos, no se pudo recuperar la forma de pago.<br>Cotizacion: "+
				cotizacionDTO.getIdToCotizacion()+" Forma de pago: "+cotizacionDTO.getIdFormaPago()+" , Moneda: "+cotizacionDTO.getIdMoneda();
			LogDeMidasEJB3.log(error, Level.SEVERE, null);
			throw new mx.com.afirme.midas.sistema.SystemException(error,40);
		}

		//Nombre y n�mero del agente.
		getMapaParametrosGeneralesPlantillas().put("NOMBRE_AGENTE", cotizacionDTO.getSolicitudDTO().getNombreAgente());
		getMapaParametrosGeneralesPlantillas().put("NUMERO_AGENTE", cotizacionDTO.getSolicitudDTO().getCodigoAgente().toBigInteger().toString());
		
		//Nombre del producto
		getMapaParametrosGeneralesPlantillas().put("NOMBRE_PRODUCTO", "SEGURO "+cotizacionDTO.getSolicitudDTO().getProductoDTO().getNombreComercial().toUpperCase());
	}
	
	private void calcularMontoPrimerRecibo(BigDecimal totalPrimaNeta,BigDecimal totalRecargo,BigDecimal factorIVA,
			BigDecimal totalIva,BigDecimal totalPrima,BigDecimal totalDerecho){
		
		int pagosAnuales = formaPagoIDTO.getNumeroRecibosGenerados();
		
		EsquemaPagosDTO esquemaPagosDTO = resumenCotizacionFacade.calcularRecibosCotizacion(
				pagosAnuales, cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia(), 
				totalPrimaNeta, totalRecargo, totalDerecho, totalIva, factorIVA);
		NumberFormat fMonto = new DecimalFormat("$#,##0.00");
		
		if(esquemaPagosDTO != null && esquemaPagosDTO.getPrimerRecibo() != null && esquemaPagosDTO.getRecibosSubSecuentes() != null){
			ReciboDTO primerRecibo = esquemaPagosDTO.getPrimerRecibo();
			ReciboDTO reciboSubSecuente = esquemaPagosDTO.getRecibosSubSecuentes();
			
			//Se agregan los montos de los recibos
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_PRIMA_NETA",fMonto.format(primerRecibo.getPrimaNeta()));
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_RECARGO",fMonto.format(primerRecibo.getRecargoPagoFraccionado()));
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_DERECHO",fMonto.format(primerRecibo.getDerecho()));
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_IVA",fMonto.format(primerRecibo.getIva()));
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_TOTAL",fMonto.format(primerRecibo.getTotalRecibo()));
			
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_PRIMA_NETA", fMonto.format(reciboSubSecuente.getPrimaNeta()));
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_RECARGO", fMonto.format(reciboSubSecuente.getRecargoPagoFraccionado()));
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_DERECHO", fMonto.format(reciboSubSecuente.getDerecho()));
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_IVA", fMonto.format(reciboSubSecuente.getIva()));
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_TOTAL", fMonto.format(reciboSubSecuente.getTotalRecibo()));
			
			getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_SUBSECUENTES", " Subsecuentes("+(esquemaPagosDTO.getCantidadRecibos().intValue()-1)+")");
		}
	}
	
	/**
	 * Agrega los datos espec�ficos del contratante al mapa de par�metros que utilizan todas las plantillas. Datos generados:
	 * NOMBRE_CONTRATANTE, APPATERNO_CONTRATANTE, APMATERNO_CONTRATANTE, TIPO_PERSONA_CONTRATANTE, SEXO_CONTRATANTE, FECHA_NACIMIENTO,
	 * OCUPACION_CONTRATANTE, RFC_CONTRATANTE, DIRECCION_CALLE_Y_NUMERO_CONTRATANTE, DIRECCION_CODIGO_POSTAL_CONTRATANTE, 
	 * DIRECCION_ESTADO_CONTRATANTE, DIRECCION_MUNICIPIO_CONTRATANTE, DIRECCION_COLONIA_CONTRATANTE, TELEFONO_CONTRATANTE, EMAIL_CONTRATANTE
	 * @param nombreUsuario
	 */
	protected void poblarDatosContratante(String nombreUsuario){
		consultarAseguradoContratante(nombreUsuario);
		if(contratante != null){
			getMapaParametrosGeneralesPlantillas().put("NOMBRE_CONTRATANTE", contratante.getNombre());
			DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
			if(contratante.getClaveTipoPersona().equals(new Short(ConstantesCotizacion.CLAVE_TIPO_PERSONA_FISICA)) ){
				
				getMapaParametrosGeneralesPlantillas().put("NOMBRE_COMPLETO_CONTRATANTE", contratante.getNombre()+" "+
						contratante.getApellidoPaterno()+" "+contratante.getApellidoMaterno());
				getMapaParametrosGeneralesPlantillas().put("APPATERNO_CONTRATANTE", contratante.getApellidoPaterno());
				getMapaParametrosGeneralesPlantillas().put("APMATERNO_CONTRATANTE", contratante.getApellidoMaterno());
				
				getMapaParametrosGeneralesPlantillas().put("ETIQUETA_APPATERNO_CONTRATANTE", "Apellido Paterno");
				getMapaParametrosGeneralesPlantillas().put("ETIQUETA_APMATERNO_CONTRATANTE", "Apellido Materno");
				getMapaParametrosGeneralesPlantillas().put("TIPO_PERSONA_CONTRATANTE", "PERSONA F�SICA");
				
				getMapaParametrosGeneralesPlantillas().put("SEXO_CONTRATANTE", contratante.getDescripcionSexo());
				getMapaParametrosGeneralesPlantillas().put("ETIQUETA_SEXO_CONTRATANTE", "Sexo");
				
				getMapaParametrosGeneralesPlantillas().put("FECHA_NACIMIENTO", formatoFecha.format(contratante.getFechaNacimiento()));
				getMapaParametrosGeneralesPlantillas().put("ETIQUETA_FECHA_NACIMIENTO", "Fecha de Nacimiento");
				
				getMapaParametrosGeneralesPlantillas().put("OCUPACION_CONTRATANTE", (contratante.getDescripcionOcupacion() != null ? contratante.getDescripcionOcupacion() : "") );
				getMapaParametrosGeneralesPlantillas().put("ETIQUETA_OCUPACION_CONTRATANTE", "Ocupaci�n/Profesi�n");
				
				getMapaParametrosGeneralesPlantillas().put("ETIQUETA_NACIONALIDAD_CONTRATANTE", "");
				getMapaParametrosGeneralesPlantillas().put("NACIONALIDAD_CONTRATANTE", "");
				
				getMapaParametrosGeneralesPlantillas().put("ETIQUETA_LUGAR_NACIMIENTO_CONTRATANTE", "");
				getMapaParametrosGeneralesPlantillas().put("LUGAR_NACIMIENTO_CONTRATANTE", "");
				
				getMapaParametrosGeneralesPlantillas().put("ETIQUETA_CURP_CONTRATANTE", "");
				getMapaParametrosGeneralesPlantillas().put("CURP_CONTRATANTE", "");
			}
			else{
				getMapaParametrosGeneralesPlantillas().put("NOMBRE_COMPLETO_CONTRATANTE", contratante.getNombre());
				getMapaParametrosGeneralesPlantillas().put("TIPO_PERSONA_CONTRATANTE", "PERSONA MORAL");
				
				getMapaParametrosGeneralesPlantillas().put("FECHA_NACIMIENTO", formatoFecha.format(contratante.getFechaNacimiento()));
				getMapaParametrosGeneralesPlantillas().put("ETIQUETA_FECHA_NACIMIENTO", "Fecha de Constituci�n");
			}
			
			getMapaParametrosGeneralesPlantillas().put("ID_CONTRATANTE", ""+asegurado.getIdCliente() );
			
			getMapaParametrosGeneralesPlantillas().put("RFC_CONTRATANTE", contratante.getCodigoRFC());
			getMapaParametrosGeneralesPlantillas().put("DIRECCION_CALLE_Y_NUMERO_CONTRATANTE", contratante.getNombreCalle());
			getMapaParametrosGeneralesPlantillas().put("DIRECCION_CODIGO_POSTAL_CONTRATANTE", contratante.getCodigoPostal());
			getMapaParametrosGeneralesPlantillas().put("DIRECCION_ESTADO_CONTRATANTE", contratante.getDescripcionEstado());
			getMapaParametrosGeneralesPlantillas().put("DIRECCION_MUNICIPIO_CONTRATANTE", contratante.getNombreDelegacion());
			getMapaParametrosGeneralesPlantillas().put("DIRECCION_COLONIA_CONTRATANTE", contratante.getNombreColonia());
			getMapaParametrosGeneralesPlantillas().put("TELEFONO_CONTRATANTE", contratante.getTelefono());
			getMapaParametrosGeneralesPlantillas().put("EMAIL_CONTRATANTE", contratante.getEmail());
			
			String direccionAsegurado = asegurado.obtenerDireccionCliente();
		    getMapaParametrosGeneralesPlantillas().put("DIRECCION_CONTRATANTE", direccionAsegurado);
		}
	}
	
	/**
	 * Agrega los datos espec�ficos del beneficiario al mapa de par�metros que utilizan todas las plantillas. Datos generados:
	 * NOMBRE_BENEFICIARIO
	 * @param nombreUsuario
	 */
	protected void poblarDatosBeneficiario(String nombreUsuario){
		String nombreBeneficiario = "";
		if(cotizacionDTO.getBeneficiario() != null && cotizacionDTO.getBeneficiario().getNombre() != null){
			nombreBeneficiario = cotizacionDTO.getBeneficiario().getNombre().toUpperCase();
			
			if(cotizacionDTO.getBeneficiario().getApellidoPaterno() != null){
				nombreBeneficiario += " " + cotizacionDTO.getBeneficiario().getApellidoPaterno().toUpperCase();
			}
			
			if(cotizacionDTO.getBeneficiario().getApellidoMaterno() != null){
				nombreBeneficiario += " " + cotizacionDTO.getBeneficiario().getApellidoMaterno().toUpperCase();
			}
		}
		getMapaParametrosGeneralesPlantillas().put("NOMBRE_BENEFICIARIO", nombreBeneficiario);
	}
	
	private void consultarAseguradoContratante(String nombreUsuario){
		if(this.asegurado == null && cotizacionDTO != null && cotizacionDTO.getIdToPersonaAsegurado() != null){
			try {
				asegurado = clienteServicios.findById(cotizacionDTO.getIdToPersonaAsegurado(), nombreUsuario);
			} catch (Exception e) {
			}
		}
		if(this.contratante == null && cotizacionDTO != null && cotizacionDTO.getIdToPersonaContratante() != null){
			try {
				contratante = clienteServicios.findById(cotizacionDTO.getIdToPersonaContratante(), nombreUsuario);
			} catch (Exception e) {
			}
		}
	}
	
	/**
	 * Realiza una b�squeda de los movimientos realizados a una cotizaci�n y calcula la prima neta por el endoso, adem�s de los totales de recargo y 
	 * derecho que se deben cobrar por el endoso. Debe ser invocado despu�s de llamar al m�todo "poblarParametrosComunes".
	 * @param cotizacionDTO
	 * @param nombreUsuario
	 * @throws SystemException
	 */
	protected void poblarParametrosCuadriculaTotalesEndoso(CotizacionDTO cotizacionDTO, String nombreUsuario) throws mx.com.afirme.midas.sistema.SystemException{
		///////DATOS PARA LA CUADR�CULA DE TOTALES
		////Datos para el primer rengl�n (totales globales)
		NumberFormat fMonto = new DecimalFormat("$#,##0.00");

		EndosoIDTO endosoIDTO = new EndosoIDTO();
		EndosoDTO ultimoEndoso = endosoFacade.getUltimoEndoso(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
		
		Integer tipoEndoso = endosoFacade.calculaTipoEndoso(cotizacionDTO);

		try {
			endosoIDTO = endosoInterfazServicios.validaEsCancelable(
					ultimoEndoso.getId().getIdToPoliza().toString() + "|"
							+ ultimoEndoso.getId().getNumeroEndoso(), cotizacionDTO.getCodigoUsuarioModificacion(),
					cotizacionDTO.getFechaInicioVigencia(), tipoEndoso);
		} catch (Exception e1) { }
		
		if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()==ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CANCELACION &&
				endosoIDTO.getCalculo().equals(ConstantesCotizacion.TIPO_CALCULO_SEYCOS)){
			
			String subTotalPrimaNetaString = fMonto.format(endosoIDTO.getPrimaNeta()*-1D);
			getMapaParametrosGeneralesPlantillas().remove("SEMI_TOTAL_PRIMA_NETA");
			getMapaParametrosGeneralesPlantillas().put("SEMI_TOTAL_PRIMA_NETA", subTotalPrimaNetaString);
			
			formaPagoIDTO = obtenerFormaPagoCotizacion(nombreUsuario);//formaPagoInterfazFacade.getPorId(Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()),Short.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()), nombreUsuario);
			
			getMapaParametrosGeneralesPlantillas().put("FORMA_PAGO", formaPagoIDTO.getDescripcion());
			String totalRecargoString = fMonto.format(endosoIDTO.getRecargoPF()*-1D);
			getMapaParametrosGeneralesPlantillas().remove("TOTAL_RECARGO");
			getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", totalRecargoString);
			
			String totalDerechoString = fMonto.format(endosoIDTO.getDerechos()*-1D);			
			getMapaParametrosGeneralesPlantillas().remove("TOTAL_DERECHO");
			getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", totalDerechoString);
			
			getMapaParametrosGeneralesPlantillas().remove("DESCRIPCION_PORCENTAJE_IVA");
			getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA", "IVA ("+String.format("%.0f", cotizacionDTO.getFactorIVA())+"%)");		

			String totalIvaString = fMonto.format(endosoIDTO.getIva()*-1D);
			getMapaParametrosGeneralesPlantillas().remove("TOTAL_IVA");
			getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", totalIvaString);	
			
			String totalPrimaNetaString = fMonto.format(endosoIDTO.getPrimaTotal()*-1D);
			getMapaParametrosGeneralesPlantillas().remove("TOTAL_PRIMA_NETA");
			getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", totalPrimaNetaString);			
			
			
		}else if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()==ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){

			//cotizacionDTO.setFechaInicioVigencia(endosoIDTO.getFechaInicioVigencia());
			//CotizacionDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).modificar(cotizacionDTO);
			//TODO 30/03/2011 Pendiente el calculo de totales de la cotizacion
			cotizacionDTO = resumenCotizacionFacade.setTotalesResumenCambioFormaPagoImpresion(cotizacionDTO);
			if(ultimoEndoso.getIdToCotizacion()!= null){
				String subTotalPrimaNetaString = fMonto.format(0D);
				getMapaParametrosGeneralesPlantillas().remove("SEMI_TOTAL_PRIMA_NETA");
				getMapaParametrosGeneralesPlantillas().put("SEMI_TOTAL_PRIMA_NETA", subTotalPrimaNetaString);	
				
				formaPagoIDTO = obtenerFormaPagoCotizacion(nombreUsuario);//formaPagoInterfazFacade.getPorId(Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()),Short.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()), nombreUsuario);
				
				if(formaPagoIDTO != null){
					getMapaParametrosGeneralesPlantillas().remove("FORMA_PAGO");
					getMapaParametrosGeneralesPlantillas().put("FORMA_PAGO", formaPagoIDTO.getDescripcion());
					
					String totalRecargoString = fMonto.format(cotizacionDTO.getMontoRecargoPagoFraccionado());
					getMapaParametrosGeneralesPlantillas().remove("TOTAL_RECARGO");
					getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", totalRecargoString);
							
					String totalDerechoString = fMonto.format(0D);
					getMapaParametrosGeneralesPlantillas().remove("TOTAL_DERECHO");
					getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", totalDerechoString);

					getMapaParametrosGeneralesPlantillas().remove("DESCRIPCION_PORCENTAJE_IVA");
					getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA", "IVA ("+String.format("%.0f", cotizacionDTO.getFactorIVA())+"%)");
					
					String totalIvaString = fMonto.format(cotizacionDTO.getMontoIVA());
					getMapaParametrosGeneralesPlantillas().remove("TOTAL_IVA");
					getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", totalIvaString);
					
					String totalPrimaNetaString = fMonto.format(cotizacionDTO.getPrimaNetaTotal());
					getMapaParametrosGeneralesPlantillas().remove("TOTAL_PRIMA_NETA");
					getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", totalPrimaNetaString);
				}
			}			
		}else{
			//TODO 30/03/2011 Pendiente el c�lculo de totales en la capa de negocio.
			cotizacionDTO = resumenCotizacionFacade.setTotalesResumenCotizacion(cotizacionDTO, new ArrayList<SoporteResumen>());
			
			String subTotalPrimaNetaString = fMonto.format(cotizacionDTO.getPrimaNetaCotizacion());
			getMapaParametrosGeneralesPlantillas().remove("SEMI_TOTAL_PRIMA_NETA");
			getMapaParametrosGeneralesPlantillas().put("SEMI_TOTAL_PRIMA_NETA", subTotalPrimaNetaString);			
			
			formaPagoIDTO = obtenerFormaPagoCotizacion(nombreUsuario);//formaPagoInterfazFacade.getPorId(Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()),Short.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()), nombreUsuario);
			
			if(formaPagoIDTO != null){
				getMapaParametrosGeneralesPlantillas().remove("FORMA_PAGO");
				getMapaParametrosGeneralesPlantillas().put("FORMA_PAGO", formaPagoIDTO.getDescripcion());
	
				String totalRecargoString = fMonto.format(cotizacionDTO.getMontoRecargoPagoFraccionado());
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_RECARGO");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", totalRecargoString);
						
				String totalDerechoString = fMonto.format(cotizacionDTO.getDerechosPoliza());
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_DERECHO");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", totalDerechoString);
				
				Double ivaObtenido= codigoPostalIVAFacade.getIVAPorIdCotizacion(cotizacionDTO.getIdToCotizacion(), "");
				
				if (ivaObtenido != null){
					//03/01/2010. Se debe agregar el IVA como par�metro para las plantillas, se muestra en la cuadr�cula de totales
					getMapaParametrosGeneralesPlantillas().remove("DESCRIPCION_PORCENTAJE_IVA");
					getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA", "IVA ("+String.format("%.0f", cotizacionDTO.getFactorIVA())+"%)");
	
					String totalIvaString = fMonto.format(cotizacionDTO.getMontoIVA());
					getMapaParametrosGeneralesPlantillas().remove("TOTAL_IVA");
					getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", totalIvaString);
					
					String totalPrimaNetaString = fMonto.format(cotizacionDTO.getPrimaNetaTotal());
					getMapaParametrosGeneralesPlantillas().remove("TOTAL_PRIMA_NETA");
					getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", totalPrimaNetaString);
				
				}//fin validar IVA != null
				else{
					String error = "Error de comunicaci�n con la base de datos, no se pudo obtener un valor para el IVA.";
					LogDeMidasEJB3.log(error, Level.SEVERE, null);
					throw new mx.com.afirme.midas.sistema.SystemException(error,40);
				}
			}//fin validar formaPagoIDTO != null
					
		}
			
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_PRIMA_NETA", " ");
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_RECARGO", " ");
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_DERECHO", " ");
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_IVA", " ");
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_TOTAL", " ");
		getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_SUBSECUENTES", " ");	

		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_PRIMA_NETA", " ");
		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_RECARGO", " ");
		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_DERECHO", " ");
		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_IVA", " ");
		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_TOTAL", " ");
		getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_SUBSECUENTES", " ");			
	}
	
	//Getters y setters
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	
	public void consultarMovimientosCotizacion(CotizacionDTO cotizacionDTO,String claveUsuario){

		MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
		dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
		List<MovimientoCotizacionEndosoDTO> listaMovimientosTMP = movimientoCotEndFacade.listarFiltrado(dto);
		//Consultar las secciones
		Map<BigDecimal,SeccionDTO> mapaSeccionesTMP = new HashMap<BigDecimal, SeccionDTO>();
		for(MovimientoCotizacionEndosoDTO movimiento : listaMovimientosTMP){
			if(movimiento.getIdToSeccion().intValue() != 0)
				if(!mapaSeccionesTMP.containsKey(movimiento.getIdToSeccion()))
					mapaSeccionesTMP.put(movimiento.getIdToSeccion(), seccionFacade.findById( movimiento.getIdToSeccion()) );
		}
		//separar los movimientos sin seccion de los que si tienen seccion
		this.movimientosCotizacion = new ArrayList<MovimientoCotizacionEndosoDTO>();
		List<MovimientoCotizacionEndosoDTO> listaMovimientosSinSeccion = new ArrayList<MovimientoCotizacionEndosoDTO>();
		List<MovimientoCotizacionEndosoDTO> listaMovimientosSeccion = new ArrayList<MovimientoCotizacionEndosoDTO>();
		for(int i=0;i<listaMovimientosTMP.size();i++){
			if(listaMovimientosTMP.get(i).getIdToSeccion().intValue() != 0)
				listaMovimientosSeccion.add(listaMovimientosTMP.get(i));
			else
				listaMovimientosSinSeccion.add(listaMovimientosTMP.get(i));
		}
		this.movimientosCotizacion.addAll(listaMovimientosSinSeccion);
		//Ordenar todos los movimientos que tienen seccion
		MovimientoCotizacionEndosoDTO []arrayMovimientosSeccion = new MovimientoCotizacionEndosoDTO[listaMovimientosSeccion.size()];
		for(int i=0;i<arrayMovimientosSeccion.length;i++){
			arrayMovimientosSeccion[i]=listaMovimientosSeccion.get(i);
		}
		for(int i=0;i<arrayMovimientosSeccion.length;i++){
			for(int j=i+1;j<arrayMovimientosSeccion.length;j++){
				SeccionDTO seccionI = mapaSeccionesTMP.get(arrayMovimientosSeccion[i].getIdToSeccion());
				SeccionDTO seccionJ = mapaSeccionesTMP.get(arrayMovimientosSeccion[j].getIdToSeccion());
				if(seccionI.getNumeroSecuencia().intValue() > seccionJ.getNumeroSecuencia().intValue()){
					MovimientoCotizacionEndosoDTO movTMP = arrayMovimientosSeccion[i];
					arrayMovimientosSeccion[i]=arrayMovimientosSeccion[j];
					arrayMovimientosSeccion[j]=movTMP;
				}
			}
		}
		for(int i=0;i<arrayMovimientosSeccion.length;i++){
			this.movimientosCotizacion.add(arrayMovimientosSeccion[i]);
		}
	}
	
	protected List<DocAnexoCotDTO> consultarDocumentosAnexos(boolean modificarOrden) {
		DocAnexoCotDTO docAnexoCotDTO = new DocAnexoCotDTO();
		docAnexoCotDTO.setId(new DocAnexoCotId());
		docAnexoCotDTO.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
		docAnexoCotDTO.setClaveSeleccion((short)1);
		List<DocAnexoCotDTO> listaAnexos = docAnexoCotFacade.listarFiltrado(docAnexoCotDTO);
		if (modificarOrden){
			int contador = 1;
			for(DocAnexoCotDTO documento : listaAnexos){
				/**
				 * 31/12/2009 la columna de orden debe aparecer de forma secuencial, ignorar el campo "orden" de cada registro
				 * y cambiarlo por un n�mero secuencial.
				 * Jos� Luis Arellano B�rcenas.
				 */
				documento.setOrden(new BigDecimal(contador));
				contador++;
			}
		}
		return listaAnexos;
	}
	
/*	protected DatoIncisoCotizacionFacadeRemote obtenerDatoIncisoFacade(){
		return this.docAnexoCotFacade
	}*/
	
	/**
	 * Calcula la lista de los diferentes registros SubRamoDTO contratados en la cotizaci�n, utilizando 
	 * la lista de registros CoberturaCotizacionDTO.
	 * @return
	 * @throws SystemException
	 */
	public List<SubRamoDTO> listarSubRamosContratados() {
		if (listaCoberturasContratadasCotizacion == null)
			consultarCoberturasContratadasCotizacion();
		if (listaSubRamosContratados == null){
			listaSubRamosContratados = new ArrayList<SubRamoDTO>();
			for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
				if (!listaSubRamosContratados.contains(coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO()))
					listaSubRamosContratados.add(coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO());
			}
		}
		return listaSubRamosContratados;
	}
	
	protected List<TexAdicionalCotDTO> consultarTextosAdicionales(){
		TexAdicionalCotDTO textoAdicional = new TexAdicionalCotDTO();
		textoAdicional.setCotizacion(cotizacionDTO);
		return texAdicionalCotFacade.listarFiltrado(textoAdicional);
	}
	
	protected IncisoCotizacionDTO[] ordenarMapaIncisos(Map<BigDecimal,IncisoCotizacionDTO> mapaIncisosModificados){
		IncisoCotizacionDTO[] arrayIncisosModificados = new IncisoCotizacionDTO[mapaIncisosModificados.size()];
		int i = 0;
		for(IncisoCotizacionDTO incisoCot : mapaIncisosModificados.values()){
			arrayIncisosModificados[i] = incisoCot;
			i++;
		}
		
		for(i=0; i< mapaIncisosModificados.size(); i++){
			for(int j=0; j<mapaIncisosModificados.size(); j++){
				int incisoI = arrayIncisosModificados[i].getId().getNumeroInciso().intValue();
				int incisoJ = arrayIncisosModificados[j].getId().getNumeroInciso().intValue();
				if ( incisoI < incisoJ ){
					IncisoCotizacionDTO incisoTMP = arrayIncisosModificados[i];
					arrayIncisosModificados[i] = arrayIncisosModificados[j];
					arrayIncisosModificados[j] = incisoTMP;
				}
			}
		}
		return arrayIncisosModificados;
	}

	public FormaPagoIDTO obtenerFormaPagoCotizacion(String nombreUsuario){
		if(formaPagoIDTO == null){
			try {
				formaPagoIDTO = formaPagoInterfazFacade.getPorId(Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()),Short.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()),nombreUsuario);
			} catch (Exception e) {
			}
		}
		return formaPagoIDTO;
	}
	
	public MonedaDTO obtenerMonedaCotizacion(){
		if(this.monedaDTO == null)
			this.monedaDTO = monedaFacade.findById(cotizacionDTO.getIdMoneda());
		return monedaDTO;
	}
	
	public List<MovimientoCotizacionEndosoDTO> getMovimientosCotizacion() {
		return movimientosCotizacion;
	}
	public void setMovimientosCotizacion(List<MovimientoCotizacionEndosoDTO> movimientosCotizacion) {
		this.movimientosCotizacion = movimientosCotizacion;
	}

	public List<SeccionCotizacionDTO> getListaSeccionesContratadasCotizacion() {
		return listaSeccionesContratadasCotizacion;
	}
	public void setListaSeccionesContratadasCotizacion(List<SeccionCotizacionDTO> listaSeccionesContratadasCotizacion) {
		this.listaSeccionesContratadasCotizacion = listaSeccionesContratadasCotizacion;
	}
	public List<CoberturaCotizacionDTO> getListaCoberturasContratadasCotizacion() {
		return listaCoberturasContratadasCotizacion;
	}
	public void setListaCoberturasContratadasCotizacion(List<CoberturaCotizacionDTO> listaCoberturasContratadasCotizacion) {
		this.listaCoberturasContratadasCotizacion = listaCoberturasContratadasCotizacion;
	}
	public List<RiesgoCotizacionDTO> getListaRiesgosContratadosCotizacion() {
		return listaRiesgosContratadosCotizacion;
	}
	public void setListaRiesgosContratadosCotizacion(List<RiesgoCotizacionDTO> listaRiesgosContratadosCotizacion) {
		this.listaRiesgosContratadosCotizacion = listaRiesgosContratadosCotizacion;
	}
	public List<SubIncisoCotizacionDTO> getListaSubIncisosCotizacion() {
		return listaSubIncisosCotizacion;
	}
	public void setListaSubIncisosCotizacion(List<SubIncisoCotizacionDTO> listaSubIncisosCotizacion) {
		this.listaSubIncisosCotizacion = listaSubIncisosCotizacion;
	}
	public List<IncisoCotizacionDTO> getListaIncisos() {
		return listaIncisos;
	}
	public void setListaIncisos(List<IncisoCotizacionDTO> listaIncisos) {
		this.listaIncisos = listaIncisos;
	}
	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}
	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}
	public String getNumeroCotizacion() {
		return numeroCotizacion;
	}
	public void setNumeroCotizacion(String numeroCotizacion) {
		this.numeroCotizacion = numeroCotizacion;
	}
	public BigDecimal getIdTcRamo() {
		return idTcRamo;
	}
	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	public BigDecimal getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
}
