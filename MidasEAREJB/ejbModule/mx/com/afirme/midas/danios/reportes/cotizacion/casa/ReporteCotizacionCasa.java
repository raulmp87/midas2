
package mx.com.afirme.midas.danios.reportes.cotizacion.casa;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.SystemException;

/**
 * @author jose luis arellano
 */
public class ReporteCotizacionCasa extends ReporteCotizacionBase {

	public ReporteCotizacionCasa(CotizacionDTO cotizacionDTO) throws javax.transaction.SystemException {
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		this.cotizacionDTO = cotizacionDTO;
		setListaPlantillas(new ArrayList<byte[]>());
		setMapaParametrosGeneralesPlantillas(new java.util.HashMap<String, Object>());
	}
	
	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException, javax.transaction.SystemException {
		generarReporte(claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioCotizacion());
	}
	
	private void generarReporte(String nombreUsuario) throws SystemException{
		super.poblarDatosContratante(nombreUsuario);
		//Recuperar incisos, subincisos, coberturas, etc. de la cotizacion
		consultarInformacionCotizacion(nombreUsuario);
		poblarParametrosComunes(nombreUsuario,true);
		
		DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		getMapaParametrosGeneralesPlantillas().put("FECHA_HOY", formatoFecha.format(new java.util.Date()));
		
		getMapaParametrosGeneralesPlantillas().put("NOMBRE_PAQUETE", cotizacionDTO.getSolicitudDTO().getProductoDTO().getNombreComercial().toUpperCase());
		
		for(IncisoCotizacionDTO incisoCotEnCurso : super.getListaIncisos()){
			byte[] reporteTMP = null;
			
			PlantillaCotizacionCasa plantillaCasa = new PlantillaCotizacionCasa(cotizacionDTO, incisoCotEnCurso, getMapaParametrosGeneralesPlantillas(), this);
			
			try {
				plantillaCasa.setMostrarLeyendaSANivelCobertura(Boolean.FALSE);
				reporteTMP = plantillaCasa.obtenerReporte(nombreUsuario);
			} catch (SystemException e) {} 
			catch (javax.transaction.SystemException e) {}
			if (reporteTMP !=null)
				getListaPlantillas().add(reporteTMP);
		}	
		
	}

}
