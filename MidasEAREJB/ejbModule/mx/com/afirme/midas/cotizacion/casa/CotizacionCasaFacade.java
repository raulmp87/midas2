package mx.com.afirme.midas.cotizacion.casa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVAFacadeRemote;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoFacadeRemote;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioFacadeRemote;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.gastoexpedicion.GastoExpedicionDTO;
import mx.com.afirme.midas.consultas.gastoexpedicion.GastoExpedicionFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotFacadeRemote;
import mx.com.afirme.midas.danios.cliente.ClienteServiciosRemote;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.direccion.DireccionFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.persona.PersonaDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.PaquetePolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaFacadeRemote;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudFacadeRemote;

import org.apache.commons.lang.StringUtils;

@Stateless
public class CotizacionCasaFacade implements CotizacionCasaFacadeRemote {

	@EJB
	private PaquetePolizaFacadeRemote paquetePolizaFacade;
	
	public CotizacionDTO crearActualizarCotizacionCasa(ClienteDTO contratante, ClienteDTO asegurado,
			DireccionDTO direccionBien, FormaPagoDTO formaPago, Double porcentajeIva, String nombreBeneficiario,
			AgenteDTO agente, List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso,
			String[] datos, BigDecimal idCotizacion) {
		
		Boolean nuevo = true;
		
		if(StringUtils.isEmpty(contratante.getCodigoPostal()) || StringUtils.isEmpty(contratante.getNombreCalle()) ||
				contratante.getIdMunicipio() == null){
			//Si no se recibe direccion, se establece la direcci�n del inciso como default.
			contratante.setCodigoPostal(direccionBien.getCodigoPostal().toString());
			contratante.setNombreCalle(direccionBien.getNombreCalle());
			contratante.setIdMunicipio(direccionBien.getIdMunicipio());
			contratante.setIdEstado(direccionBien.getIdEstado());
			try{
				contratante.setIdColonia(new BigDecimal(direccionBien.getNombreColonia()));
			}catch(Exception e){
			}
			if(contratante.getIdColonia() == null){
				try{
					contratante.setIdColonia(new BigDecimal(
							direccionBien.getNombreColonia().substring(
									direccionBien.getNombreColonia().indexOf("-")+1,
									direccionBien.getNombreColonia().length())));
				}catch(Exception e){
					
				}
			}
			
			if (direccionBien.getNumeroExterior() != null)
				contratante.setNumeroExterior(new BigDecimal(direccionBien.getNumeroExterior()));
		}
		
		if(asegurado == null){
			asegurado = contratante;
		}
		else{
		//Se Copia la direccion del contratante al asegurado para que no quede vacia, la direccion del asegurado no es relevante para CASA
			copiaDireccionContratanteAsegurado (contratante, asegurado);
		}
		
		contratante.setClaveTipoPersona((short)1);//Solo maneja personas f�sicas, este dato no se captura en el cotizador Casa.
		asegurado.setClaveTipoPersona((short)1);
		
		//Se agrega o actualiza la direcci�n del Bien
		direccionBien = actualizarDireccionBien(idCotizacion,BigDecimal.ONE,direccionBien);
		
		SolicitudDTO solicitud = null;
		CotizacionDTO cotizacionOriginal = null;
		
		if(idCotizacion != null){
			//Se obtiene la cotizacion completa en caso de que exista
			cotizacionOriginal = cotizacionFacade.findById(idCotizacion);//getCotizacionFull(idCotizacion);
			
			if (cotizacionOriginal != null) {
				nuevo = false;
			}
		}
		
		//TODO Se obtiene la solicitud de la cotizacion (Se actualiza o se crea un nueva dependiendo el caso)
		solicitud = obtenerSolicitud(cotizacionOriginal, contratante, agente);
		
		entityManager.flush();
		entityManager.clear();
		
		
		if(idCotizacion != null){
			//Se obtiene la cotizacion completa en caso de que exista
			cotizacionOriginal = cotizacionFacade.findById(idCotizacion);//getCotizacionFull(idCotizacion);
		}
		
		//Se obtiene la cotizacion (Se actualiza o se crea un nueva dependiendo el caso)
		CotizacionDTO cotizacion = obtenerCotizacion(cotizacionOriginal, solicitud, formaPago, porcentajeIva, contratante, asegurado, nombreBeneficiario);
		
		if (nuevo) {
			//Se agrega el unico inciso que llevan las cotizaciones para CASA
			agregaInciso(cotizacion, direccionBien.getIdToDireccion(), datosInciso, datos);
			
			entityManager.flush();
			entityManager.clear();
			
			establecerComisionesSubRamosCotizacion (cotizacion);
			
		} else {
			//Se actualiza el unico inciso que llevan las cotizaciones para CASA
			actualizaInciso(cotizacion, BigDecimal.ONE, direccionBien, datosInciso, datos);
		}
		
		entityManager.flush();
		entityManager.clear();
		
		//Regresa la cotizacion (con su estructura completa)
//		cotizacion = cotizacionFacade.findById(cotizacion.getIdToCotizacion());
		
		//Si es la primera vez que se registra, se aplica el paquete por default para el tipo de p�liza.
		if(nuevo && cotizacion.getTipoPolizaDTO().getIdToPaqueteDefault() != null){
			paquetePolizaFacade.aplicarPaqueteCotizacion(
					cotizacion.getTipoPolizaDTO().getIdToPaqueteDefault(),
					cotizacion.getIdToCotizacion(), BigDecimal.ONE);
			
			entityManager.flush();
			entityManager.clear();
			
			//Recalcular la cotizaci�n.
			cotizacion = cotizacionFacade.getCotizacionFull(cotizacion.getIdToCotizacion());
			cotizacionFacade.recalcularCotizacion(cotizacion, "SISTEMA");
		}
		
		return cotizacion;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void prepararEmisionCasa(CotizacionDTO cotizacion) {
		llenaDatosRegistro (cotizacion);
	}
	
	private void copiaDireccionContratanteAsegurado (ClienteDTO contratante, ClienteDTO asegurado) {
		
		asegurado.setIdToDireccion(contratante.getIdToDireccion());
		asegurado.setNombreCalle(contratante.getNombreCalle());
		asegurado.setNumeroExterior(contratante.getNumeroExterior());
		asegurado.setNumeroInterior(contratante.getNumeroInterior());
		asegurado.setEntreCalles(contratante.getEntreCalles());
		asegurado.setIdColonia(contratante.getIdColonia());
		asegurado.setNombreColonia(contratante.getNombreColonia());
		asegurado.setNombreDelegacion(contratante.getNombreDelegacion());
		asegurado.setIdMunicipio(contratante.getIdMunicipio());
		asegurado.setIdEstado(contratante.getIdEstado());
		asegurado.setCodigoPostal(contratante.getCodigoPostal());
		asegurado.setDescripcionEstado(contratante.getDescripcionEstado());
		asegurado.setDireccionCompleta(contratante.getDireccionCompleta());
		
	}
	
	
	private void agregaInciso (CotizacionDTO cotizacion, BigDecimal idToDireccion,
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso, String[] datos) {
		
		BigDecimal numeroInciso = incisoCotizacionFacade.maxIncisos(cotizacion.getIdToCotizacion()).add(BigDecimal.ONE); 
			
		List<SeccionDTO> listaSeccionesTipoPoliza = seccionFacade.listarVigentes(cotizacion.getTipoPolizaDTO().getIdToTipoPoliza());
		
		for(SeccionDTO seccionDTO : listaSeccionesTipoPoliza) {
			List<CoberturaSeccionDTO> listaCoberturaSeccion = coberturaSeccionFacade.getVigentesPorSeccion(seccionDTO.getIdToSeccion());
			if (listaCoberturaSeccion == null)
				listaCoberturaSeccion = new ArrayList<CoberturaSeccionDTO>();
			for(CoberturaSeccionDTO coberturaSeccionDTO : listaCoberturaSeccion){
				List<RiesgoCoberturaDTO> listaRiesgosCobertura = riesgoCoberturaFacade.listarVigentesPorCoberturaSeccion(coberturaSeccionDTO.getId().getIdtocobertura(), coberturaSeccionDTO.getId().getIdtoseccion());
				if(listaRiesgosCobertura == null)
					listaRiesgosCobertura = new ArrayList<RiesgoCoberturaDTO>();
				coberturaSeccionDTO.setRiesgos( listaRiesgosCobertura );
			}
			seccionDTO.setCoberturas(listaCoberturaSeccion);
		}
		cotizacion.getTipoPolizaDTO().setSecciones(listaSeccionesTipoPoliza);
		incisoCotizacionFacade.agregarInciso(cotizacion, idToDireccion, numeroInciso, datosInciso, datos,null,null);
		
		//Se elimina el calculo de riesgos hidro debido a que utiliza los SP de danios, se necesitaria crear nuevos sp's para los nuevos riesgos.
//		try {
//			calculoCotizacionFacade.calcularRiesgosHidro(cotizacion.getIdToCotizacion(), numeroInciso, SistemaPersistencia.USUARIO_SISTEMA);
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
		//Se comenta debido a que en el cotizador no se utilizan anexos.
		//se actualizan documentos anexos de coberturas
//		calculoCotizacionFacade.actualizaAnexosDeCoberturasContratadas(cotizacion.getIdToCotizacion());	
		
		
	}
	
	
	private void actualizaInciso (CotizacionDTO cotizacion, BigDecimal numeroInciso, DireccionDTO direccionDTO,
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso, String[] datos) {
						
		incisoCotizacionFacade.actualizarIncisoCotizacion(cotizacion.getIdToCotizacion(), numeroInciso, 
				direccionDTO.getIdToDireccion(), datosInciso, datos);
		

//		try {
//			calculoCotizacionFacade.calcularRiesgosHidro(cotizacion.getIdToCotizacion(), numeroInciso, SistemaPersistencia.USUARIO_SISTEMA);
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
		//se actualizan documentos anexos de coberturas
//		calculoCotizacionFacade.actualizaAnexosDeCoberturasContratadas(cotizacion.getIdToCotizacion());	
		
	}
	
	private void establecerComisionesSubRamosCotizacion (CotizacionDTO cotizacion) {
		List<SubRamoDTO> subRamos = subRamoFacade.getSubRamosInCotizacion(cotizacion.getIdToCotizacion());
		for(SubRamoDTO subRamoDTO : subRamos) {
			//Si es incendio, se generan tres registros para comisi�n
			if (subRamoDTO.getIdTcSubRamo().compareTo(new BigDecimal(1))==0){
				for(int i=1;i<=3;i++){
					ComisionCotizacionId id = new ComisionCotizacionId();
					id.setIdTcSubramo(subRamoDTO.getIdTcSubRamo());
					id.setIdToCotizacion(cotizacion.getIdToCotizacion());
					id.setTipoPorcentajeComision((short)i);
					
					ComisionCotizacionDTO comisionCotizacionDTO = new ComisionCotizacionDTO();
					comisionCotizacionDTO.setId(id);
					comisionCotizacionDTO.setCotizacionDTO(cotizacion);
					comisionCotizacionDTO.setSubRamoDTO(subRamoDTO);
					comisionCotizacionDTO.setValorComisionCotizacion(new BigDecimal(0));
					comisionCotizacionDTO.setClaveAutComision((short)0);
					if (i == 1){
						comisionCotizacionDTO.setPorcentajeComisionCotizacion(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionRO()));
						comisionCotizacionDTO.setPorcentajeComisionDefault(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionRO()));
					}
					else if (i == 2){
						comisionCotizacionDTO.setPorcentajeComisionCotizacion(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionRCI()));
						comisionCotizacionDTO.setPorcentajeComisionDefault(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionRCI()));
					}
					else{
						comisionCotizacionDTO.setPorcentajeComisionCotizacion(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionPRR()));
						comisionCotizacionDTO.setPorcentajeComisionDefault(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionPRR()));
					}
					
					comisionCotizacionFacade.save(comisionCotizacionDTO);
				}
			}
			//Si no es incendio, s�lo se genera un registro de comisiones usando el campo porcentajeMaximoComisionRO
			else{
				ComisionCotizacionId id = new ComisionCotizacionId();
				id.setIdTcSubramo(subRamoDTO.getIdTcSubRamo());
				id.setIdToCotizacion(cotizacion.getIdToCotizacion());
				id.setTipoPorcentajeComision((short)1);
				
				ComisionCotizacionDTO comisionCotizacionDTO = new ComisionCotizacionDTO();
				comisionCotizacionDTO.setId(id);
				comisionCotizacionDTO.setCotizacionDTO(cotizacion);
				comisionCotizacionDTO.setSubRamoDTO(subRamoDTO);
				comisionCotizacionDTO.setValorComisionCotizacion(new BigDecimal(0));
				comisionCotizacionDTO.setClaveAutComision((short)0);
				comisionCotizacionDTO.setPorcentajeComisionCotizacion(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionRO()));
				comisionCotizacionDTO.setPorcentajeComisionDefault(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionRO()));
				
				comisionCotizacionFacade.save(comisionCotizacionDTO);
			}
		}	
	}
	
	private CotizacionDTO obtenerCotizacion (CotizacionDTO cotizacionDTO, SolicitudDTO solicitud, FormaPagoDTO formaPago, 
			Double porcentajeIva, ClienteDTO contratante, ClienteDTO asegurado,String beneficiario) {
		
		if (cotizacionDTO == null)
			cotizacionDTO = new CotizacionDTO();
		
		cotizacionDTO = llenarCotizacion(cotizacionDTO, solicitud, formaPago, porcentajeIva, contratante, asegurado,beneficiario);
		
		if (cotizacionDTO.getIdToCotizacion() != null) {
			cotizacionDTO = cotizacionFacade.update(cotizacionDTO);
		} else {
			cotizacionDTO = cotizacionFacade.save(cotizacionDTO);
			
			//Se establecen las comisiones por cada uno de los subramos en la cotizacion
//			establecerComisionesSubRamosCotizacion (cotizacionDTO); aun no se deben guardar por que no existe inciso en este momento
			
		}
		
		return cotizacionDTO;
		
	}
	
	private CotizacionDTO llenarCotizacion (CotizacionDTO cotizacionDTO, SolicitudDTO solicitud, FormaPagoDTO formaPago, Double porcentajeIva,
			ClienteDTO contratante, ClienteDTO asegurado,String beneficiario) {
		
		BigDecimal idContratante = null;
		BigDecimal idAsegurado = null;
		
		TipoPolizaDTO tipoPolizaCasaDefault = obtenerTipoPolizaCasa();
		
		//NOTA: En Midas no se actualiza el contratante ni el asegurado, falta desarrollar una interfaz con Seycos
		//Crea al contratante
		try {
			idContratante = clienteServicios.save(contratante, SistemaPersistencia.USUARIO_SISTEMA);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		//Crea al asegurado
		try {
			idAsegurado = clienteServicios.save(asegurado, SistemaPersistencia.USUARIO_SISTEMA);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		Date fechaInicioVigencia = (cotizacionDTO.getFechaInicioVigencia()!=null?cotizacionDTO.getFechaInicioVigencia():new Date());
		Date fechaFinVigencia = (cotizacionDTO.getFechaFinVigencia()!=null?cotizacionDTO.getFechaFinVigencia()
				:Utilerias.agregaAniosAFecha(fechaInicioVigencia, 1));
		
		TipoNegocioDTO tipoNegocio = tipoNegocioFacade.findByProperty("codigoTipoNegocio", SistemaPersistencia.CODIGO_TIPO_NEGOCIO_NORMAL).get(0);
		
		
		cotizacionDTO.setSolicitudDTO(solicitud);
		cotizacionDTO.setFechaCreacion(cotizacionDTO.getFechaCreacion()!=null?cotizacionDTO.getFechaCreacion():new Date());
		cotizacionDTO.setCodigoUsuarioCreacion(SistemaPersistencia.USUARIO_SISTEMA);
		cotizacionDTO.setFechaModificacion(new Date());
		cotizacionDTO.setCodigoUsuarioModificacion(SistemaPersistencia.USUARIO_SISTEMA);
		cotizacionDTO.setIdMedioPago(cotizacionDTO.getIdMedioPago()!=null?cotizacionDTO.getIdMedioPago():new BigDecimal("15")); //Efectivo
		cotizacionDTO.setClaveEstatus(cotizacionDTO.getClaveEstatus()!=null?cotizacionDTO.getClaveEstatus()
				:SistemaPersistencia.COTIZACION_EN_PROCESO);
		cotizacionDTO.setPorcentajebonifcomision(cotizacionDTO.getPorcentajebonifcomision()!=null
				?cotizacionDTO.getPorcentajebonifcomision():0D);
		cotizacionDTO.setTipoCambio(cotizacionDTO.getTipoCambio()!=null?cotizacionDTO.getTipoCambio():(double)1); 
		cotizacionDTO.setTipoPolizaDTO(cotizacionDTO.getTipoPolizaDTO()!=null?cotizacionDTO.getTipoPolizaDTO():tipoPolizaCasaDefault);
		cotizacionDTO.setIdFormaPago(formaPago != null? new BigDecimal(formaPago.getIdFormaPago() + ""):null);
		cotizacionDTO.setPorcentajeIva(porcentajeIva);
		cotizacionDTO.setIdToPersonaContratante(idContratante);
		cotizacionDTO.setNombreContratante(contratante.obtenerNombreCliente());
		cotizacionDTO.setIdToPersonaAsegurado(idAsegurado);
		cotizacionDTO.setNombreAsegurado(asegurado.obtenerNombreCliente());
		cotizacionDTO.setFechaInicioVigencia(fechaInicioVigencia);
		cotizacionDTO.setFechaFinVigencia(fechaFinVigencia);
		cotizacionDTO.setIdMoneda(cotizacionDTO.getIdMoneda()!=null?cotizacionDTO.getIdMoneda()
				:new BigDecimal(SistemaPersistencia.MONEDA_PESOS));
		cotizacionDTO.setTipoNegocioDTO(tipoNegocio);
		cotizacionDTO.setClaveAutVigenciaMaxMin(SistemaPersistencia.ESTATUS_AUTORIZADA);
		
		if(beneficiario != null && !beneficiario.trim().equals("")){
			
			if(cotizacionDTO.getBeneficiario() == null){
				cotizacionDTO.setBeneficiario(new PersonaDTO());
			}
			
			cotizacionDTO.getBeneficiario().setNombre(beneficiario);
			cotizacionDTO.getBeneficiario().setClaveTipoPersona(PersonaDTO.CLAVE_PERSONA_FISICA);
		}
		
		return cotizacionDTO;
	}
	
	private SolicitudDTO obtenerSolicitud (CotizacionDTO cotizacion, ClienteDTO contratante, AgenteDTO agente) {
		
			
		SolicitudDTO solicitudDTO = null;
		
		if (cotizacion != null && cotizacion.getSolicitudDTO() != null) {
			solicitudDTO = cotizacion.getSolicitudDTO();
		} else {
			solicitudDTO = new SolicitudDTO();
		}
		
		solicitudDTO = llenarSolicitud(solicitudDTO, contratante, agente);
		
		if (solicitudDTO.getIdToSolicitud() != null) {
			solicitudDTO = solicitudFacade.update(solicitudDTO);
		} else {
			solicitudDTO = solicitudFacade.save(solicitudDTO);
		}
				
		return solicitudDTO;
		
	}
	
	private SolicitudDTO llenarSolicitud (SolicitudDTO solicitudDTO, ClienteDTO contratante, AgenteDTO agente) {
		
		ProductoDTO productoCasa = obtenerProductoPorCodigo(ConstantesCotizacion.CODIGO_PRODUCTO_CASA);
				
		solicitudDTO.setEsRenovacion(solicitudDTO.getEsRenovacion()!= null?solicitudDTO.getEsRenovacion():(short)0);
		solicitudDTO.setClaveEstatus(solicitudDTO.getClaveEstatus()!=null?solicitudDTO.getClaveEstatus():(short)1);
		solicitudDTO.setClaveTipoPersona(contratante.getClaveTipoPersona());
		solicitudDTO.setNombrePersona(contratante.getNombre());
		solicitudDTO.setApellidoPaterno(contratante.getApellidoPaterno());
		solicitudDTO.setApellidoMaterno(contratante.getApellidoMaterno());
		solicitudDTO.setTelefonoContacto(contratante.getTelefono());
		solicitudDTO.setProductoDTO(productoCasa);
		solicitudDTO.setNombreAgente(agente.getNombre());
		solicitudDTO.setCodigoAgente(new BigDecimal (agente.getIdTcAgente()));
		if(agente.getIdOficina() != null) {
            solicitudDTO.setIdOficina(new BigDecimal(agente.getIdGerencia()));
            solicitudDTO.setNombreOficina(agente.getNombreGerencia());                    
            solicitudDTO.setCodigoEjecutivo(new BigDecimal(agente.getIdOficina()));
            solicitudDTO.setNombreEjecutivo(agente.getNombreOficina());
            solicitudDTO.setNombreOficinaAgente(agente.getNombrePromotoria());
        }
		solicitudDTO.setClaveOpcionEmision(solicitudDTO.getClaveOpcionEmision()!=null?solicitudDTO.getClaveOpcionEmision():(short)1);
		solicitudDTO.setIdToPolizaAnterior(solicitudDTO.getIdToPolizaAnterior()!=null?solicitudDTO.getIdToPolizaAnterior():BigDecimal.ZERO);
		solicitudDTO.setCodigoUsuarioCreacion(SistemaPersistencia.USUARIO_SISTEMA);
		solicitudDTO.setCodigoUsuarioModificacion(SistemaPersistencia.USUARIO_SISTEMA);
		solicitudDTO.setClaveTipoSolicitud(solicitudDTO.getClaveTipoSolicitud()!=null?solicitudDTO.getClaveTipoSolicitud():(short)1);
		solicitudDTO.setFechaCreacion(solicitudDTO.getFechaCreacion()!=null?solicitudDTO.getFechaCreacion():new Date());
		solicitudDTO.setFechaModificacion(new Date());
		
		return solicitudDTO;
	}
	
	
	private ProductoDTO obtenerProductoPorCodigo (String codigo) {
		
		
		ProductoDTO productoFiltro = new ProductoDTO();
		
		productoFiltro.setCodigo(codigo);
		
		List<ProductoDTO> productos = productoFacade.listarFiltrado(productoFiltro, false);
		
		if (productos != null && productos.size() > 0) {
			return productos.get(0);
		}
		return null;
		
		
	}
	
	
	private TipoPolizaDTO obtenerTipoPolizaCasa() {
		
//		List<TipoPolizaDTO> tiposPoliza = tipoPolizaFacade.oblistarPorIdProducto(producto.getIdToProducto());
		
		BigDecimal idToTipoPoliza = cotizacionFacade.obtenerIdToTipoPoliza(
				ConstantesCotizacion.CODIGO_PRODUCTO_CASA, ConstantesCotizacion.CODIGO_TIPO_POLIZA_CASA);
		
		
		if (idToTipoPoliza != null) {
			return tipoPolizaFacade.findById(idToTipoPoliza);
		}
		
		return null;
		
	}
	
	
	private DireccionDTO actualizarDireccionBien(BigDecimal idToCotizacion,BigDecimal numeroInciso,DireccionDTO direccionBien) {
		BigDecimal idToDireccion = direccionBien.getIdToDireccion();
		if(idToDireccion == null){
			//Verificar si existe la direcci�n.
			IncisoCotizacionDTO incisoCotizacionDTO = incisoCotizacionFacade.findById(new IncisoCotizacionId(idToCotizacion, numeroInciso));
			if(incisoCotizacionDTO != null && incisoCotizacionDTO.getDireccionDTO() != null){
				idToDireccion = incisoCotizacionDTO.getDireccionDTO().getIdToDireccion();
			}
		}
		direccionBien.setIdToDireccion(idToDireccion);
		//Se agrega o actualiza la direcci�n del Bien
		if (idToDireccion != null) {
			direccionBien = direccionFacade.update(direccionBien);
		} else {
			direccionBien = direccionFacade.save(direccionBien);
		}
		return direccionBien;
	}
	
	
	/**
	 * METODOS DE EMISION DE CASA 
	 */
	
	private InformacionReciboCasaDTO llenaDatosRegistro (Object objetoEstructuraCotizacion) {
		
		BigDecimal numeroCoberturasContratadas = new BigDecimal(0D);
	    String descripcionCobertura = null;
	    Double valorPrimaNeta = 0D;
	    Double valorBonificacionComision = 0D;
	    Double valorBonificacionComisionRPF = 0D;
	    Double valorDerechos = 0D;
	    Double valorRecargos = 0D;
	    Double valorIva = 0D;
	    Double valorPrimaTotal = 0D;
	    Double comisionPrimaNetaAgente = 0D;
	    Double comisionRecargoAgente = 0D;
	    Double comisionPrimaNetaSupervisor = 0D;
	    Double comisionRecargoSupervisor = 0D;
	    
	    CotizacionDTO cotizacionDatosGenerales = null;
	    InformacionReciboCasaDTOId id = null;
	    InformacionReciboCasaDTO informacionReciboCasa = null;
	    InformacionReciboCasaDTO infoReciboCasa = null;
	    
	    id = obtieneInformacionReciboCasaId(objetoEstructuraCotizacion);
	    informacionReciboCasa = obtieneInformacionReciboCasa(id);
		
	    
	    if(objetoEstructuraCotizacion instanceof CoberturaCotizacionDTO) {
			CoberturaCotizacionDTO coberturaCotizacion = (CoberturaCotizacionDTO) objetoEstructuraCotizacion;
						
			CoberturaDTO cobertura = coberturaFacade.findById(coberturaCotizacion.getId().getIdToCobertura());
			descripcionCobertura = cobertura.getNombreComercial() + " " + coberturaCotizacion.getId().getIdToCobertura();
			
			ComisionCotizacionDTO comision = obtenerComision(id);
			cotizacionDatosGenerales = cotizacionFacade.findById(coberturaCotizacion.getId().getIdToCotizacion());
			Double valorPrimaNetaCotizacion = cotizacionFacade.getPrimaNetaCotizacion(coberturaCotizacion.getId().getIdToCotizacion());
			Double derechos = obtenerDerechosPoliza(cotizacionDatosGenerales.getIdMoneda(), valorPrimaNetaCotizacion); 
			Double pctPagoFraccionado = obtenerPorcentajePagoFraccionado(cotizacionDatosGenerales);
			Double pctIva = obtenerPorcentajeIva(cotizacionDatosGenerales);
			cotizacionDatosGenerales.setPorcentajePagoFraccionado(pctPagoFraccionado);
			cotizacionDatosGenerales.setPorcentajeIva(pctIva);
			
			valorPrimaNeta = coberturaCotizacion.getValorPrimaNeta();
			valorDerechos = derechos * (valorPrimaNeta/valorPrimaNetaCotizacion);
			valorRecargos = valorPrimaNeta * (cotizacionDatosGenerales.getPorcentajePagoFraccionado()/100D);  //valorRPF
			comisionPrimaNetaAgente = valorPrimaNeta * (comision.getPorcentajeComisionCotizacion().doubleValue()/100D);//valorComision
			comisionRecargoAgente = valorRecargos * (comision.getPorcentajeComisionCotizacion().doubleValue()/100D); //valorComisionRecPagoFrac
			valorBonificacionComision = comisionPrimaNetaAgente * (cotizacionDatosGenerales.getPorcentajebonifcomision()/100D);
			valorBonificacionComisionRPF = comisionRecargoAgente * (cotizacionDatosGenerales.getPorcentajebonifcomision()/100D);
			valorIva = (valorPrimaNeta + valorRecargos + valorDerechos - valorBonificacionComision - valorBonificacionComisionRPF) 
				* (cotizacionDatosGenerales.getPorcentajeIva()/100D);
			valorPrimaTotal = valorPrimaNeta + valorRecargos + valorDerechos - valorBonificacionComision 
				- valorBonificacionComisionRPF + valorIva;
					
		} else if (objetoEstructuraCotizacion instanceof IncisoCotizacionDTO) {
			IncisoCotizacionDTO incisoCotizacion = (IncisoCotizacionDTO) objetoEstructuraCotizacion;
			//TODO Incluir el filtro por inciso en la consulta
			List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionFacade
				.listarCoberturasContratadas(incisoCotizacion.getId().getIdToCotizacion());
			
			cotizacionDatosGenerales = cotizacionFacade.findById(incisoCotizacion.getId().getIdToCotizacion());
			
			for (CoberturaCotizacionDTO cobertura : coberturas) {
				
				infoReciboCasa = llenaDatosRegistro(cobertura);
				
				valorPrimaNeta += infoReciboCasa.getValorPrimaNeta();
				valorBonificacionComision += infoReciboCasa.getValorBonificacionComision(); 
				valorBonificacionComisionRPF += infoReciboCasa.getValorBonificacionComisionRPF();
				valorDerechos += infoReciboCasa.getValorDerechos();
				valorRecargos += infoReciboCasa.getValorRecargos();
				valorIva += infoReciboCasa.getValorIva();
				valorPrimaTotal += infoReciboCasa.getValorPrimaTotal();
				comisionPrimaNetaAgente += infoReciboCasa.getComisionPrimaNetaAgente(); //valorComision
				comisionRecargoAgente += infoReciboCasa.getComisionRecargoAgente();//valorComisionRecPagoFrac
				
			}
					
		} else if (objetoEstructuraCotizacion instanceof CotizacionDTO) {
			CotizacionDTO cotizacion = (CotizacionDTO) objetoEstructuraCotizacion;
			
			List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionFacade
				.listarCoberturasContratadas(cotizacion.getIdToCotizacion());
									
			List<IncisoCotizacionDTO> incisos = incisoCotizacionFacade.listarIncisosPorCoberturaContratada(
					cotizacion.getIdToCotizacion(), coberturas, SistemaPersistencia.CONTRATADO);
						
			numeroCoberturasContratadas = new BigDecimal(coberturas.size());
			
			for (IncisoCotizacionDTO inciso : incisos) {
							
				infoReciboCasa = llenaDatosRegistro(inciso);
				
				valorPrimaNeta += infoReciboCasa.getValorPrimaNeta();
				valorBonificacionComision += infoReciboCasa.getValorBonificacionComision(); 
				valorBonificacionComisionRPF += infoReciboCasa.getValorBonificacionComisionRPF();
				valorDerechos += infoReciboCasa.getValorDerechos();
				valorRecargos += infoReciboCasa.getValorRecargos();
				valorIva += infoReciboCasa.getValorIva();
				valorPrimaTotal += infoReciboCasa.getValorPrimaTotal();
				comisionPrimaNetaAgente += infoReciboCasa.getComisionPrimaNetaAgente(); //valorComision
				comisionRecargoAgente += infoReciboCasa.getComisionRecargoAgente();//valorComisionRecPagoFrac
				
			}
			
			cotizacionDatosGenerales = cotizacion;
			
		} else {
			throw new ClassCastException("El tipo de objeto no es permitido");
		}
		
	    llenaDatosDefault(informacionReciboCasa, cotizacionDatosGenerales);
	    
	    informacionReciboCasa.setNumeroCoberturasContratadas(numeroCoberturasContratadas);
	    informacionReciboCasa.setDescripcionCobertura(descripcionCobertura);
	    informacionReciboCasa.setValorPrimaNeta(valorPrimaNeta);
	    informacionReciboCasa.setValorBonificacionComision(valorBonificacionComisionRPF);
	    informacionReciboCasa.setValorBonificacionComisionRPF(valorBonificacionComisionRPF);
	    informacionReciboCasa.setValorDerechos(valorDerechos);
	    informacionReciboCasa.setValorRecargos(valorRecargos);
	    informacionReciboCasa.setValorIva(valorIva);
	    informacionReciboCasa.setValorPrimaTotal(valorPrimaTotal);
	    informacionReciboCasa.setComisionPrimaNetaAgente(comisionPrimaNetaAgente);
	    informacionReciboCasa.setComisionRecargoAgente(comisionRecargoAgente);
	    informacionReciboCasa.setComisionPrimaNetaSupervisor(comisionPrimaNetaSupervisor);
	    informacionReciboCasa.setComisionRecargoSupervisor(comisionRecargoSupervisor);
		
	    
	    //Si es un registro que ya existia
	    if (informacionReciboCasa.getId() != null) {
	    	informacionReciboCasaFacade.update(informacionReciboCasa);
	    } else {
	    	//Es un nuevo registro
	    	informacionReciboCasa.setId(id);
	    	informacionReciboCasaFacade.save(informacionReciboCasa);
	    }
	    
	    return informacionReciboCasa;
		
	}
	
	
	
	private void llenaDatosDefault (InformacionReciboCasaDTO informacionReciboCasa, CotizacionDTO cotizacion) {
		
		informacionReciboCasa.setDescripcionProducto(cotizacion.getTipoPolizaDTO().getProductoDTO().getDescripcion());
		informacionReciboCasa.setIdCentroEmisor(SistemaPersistencia.CENTRO_EMISOR_DEFAULT);
		informacionReciboCasa.setNumeroPoliza(new BigDecimal(0D)); //En CASA no hay poliza aun, solo cotizacion
		informacionReciboCasa.setClaveTipoEndoso(new BigDecimal(0D)); //Poliza nueva
		informacionReciboCasa.setMotivoCancelacion(null);
		informacionReciboCasa.setIdContratante(cotizacion.getIdToPersonaContratante());
		informacionReciboCasa.setIdAgente(cotizacion.getSolicitudDTO().getCodigoAgente());
		informacionReciboCasa.setIdConductoCobro(cotizacion.getIdMedioPago());
		informacionReciboCasa.setIdTcFormaPago(cotizacion.getIdFormaPago());
		informacionReciboCasa.setIdMoneda(cotizacion.getIdMoneda());
		informacionReciboCasa.setTipoCambio(1D);  //Tipo de cambio para MN
		informacionReciboCasa.setCodigoUsuario(SistemaPersistencia.USUARIO_SISTEMA);
		informacionReciboCasa.setFechaInicioVigencia(cotizacion.getFechaInicioVigencia());
		informacionReciboCasa.setFechaFinVigencia(cotizacion.getFechaFinVigencia());
		informacionReciboCasa.setAuxiliar(null);
		informacionReciboCasa.setCentroCosto(null);
			
	}
	
		
	private InformacionReciboCasaDTOId obtieneInformacionReciboCasaId(Object objetoEstructuraCotizacion) {
		
		String idProducto = "0";
		BigDecimal idToPoliza = new BigDecimal(0D);
	    BigDecimal numeroRenovacion = new BigDecimal(0D);
	    BigDecimal numeroEndoso = new BigDecimal(0D);
	    BigDecimal idToCotizacion = new BigDecimal(0D);
	    BigDecimal idTipo = new BigDecimal(0D);
	    String idTcRamo = "0";
	    String idTcSubRamo = "0";
	    BigDecimal numeroInciso = new BigDecimal(0D);
	    String idToCobertura = "0";
	    BigDecimal idToSeccion = new BigDecimal(0D); 
		
	    ProductoDTO productoCasa = obtenerProductoPorCodigo(ConstantesCotizacion.CODIGO_PRODUCTO_CASA);
	    idProducto = productoCasa.getIdToProducto().toString();
		
		if(objetoEstructuraCotizacion instanceof CoberturaCotizacionDTO) {
			CoberturaCotizacionDTO coberturaCotizacion = (CoberturaCotizacionDTO) objetoEstructuraCotizacion;

			SubRamoDTO subRamo = subRamoFacade.findById(coberturaCotizacion.getIdTcSubramo());
			idTcRamo = subRamo.getRamoDTO().getIdTcRamo().toString();
			idToCotizacion = coberturaCotizacion.getId().getIdToCotizacion();
			idTcSubRamo = coberturaCotizacion.getIdTcSubramo().toString();
			numeroInciso = coberturaCotizacion.getId().getNumeroInciso();
			idToCobertura = coberturaCotizacion.getId().getIdToCobertura().toString();
			idToSeccion = coberturaCotizacion.getId().getIdToSeccion();
			idTipo = SistemaPersistencia.REGISTRO_TIPO_COBERTURA;
					
		} else if (objetoEstructuraCotizacion instanceof IncisoCotizacionDTO) {
			IncisoCotizacionDTO incisoCotizacion = (IncisoCotizacionDTO) objetoEstructuraCotizacion;
			
			
			idToCotizacion = incisoCotizacion.getId().getIdToCotizacion();
			numeroInciso = incisoCotizacion.getId().getNumeroInciso();
			idTipo = SistemaPersistencia.REGISTRO_TIPO_INCISO;
					
		} else if (objetoEstructuraCotizacion instanceof CotizacionDTO) {
			CotizacionDTO cotizacion = (CotizacionDTO) objetoEstructuraCotizacion;
			
			idToCotizacion = cotizacion.getIdToCotizacion();
			idTipo = SistemaPersistencia.REGISTRO_TIPO_POLIZA;
			
		} else {
			throw new ClassCastException("El tipo de objeto no es permitido");
		}
				
		
		InformacionReciboCasaDTOId id  = new InformacionReciboCasaDTOId();
		
		id.setIdToProducto(idProducto);
		id.setIdToPoliza(idToPoliza);
		id.setNumeroRenovacion(numeroRenovacion);
		id.setNumeroEndoso(numeroEndoso);
		id.setIdToCotizacion(idToCotizacion);
		id.setIdTipo(idTipo);
		id.setIdTcRamo(idTcRamo);
		id.setIdTcSubRamo(idTcSubRamo);
		id.setNumeroInciso(numeroInciso);
		id.setIdToCobertura(idToCobertura);
		id.setIdToSeccion(idToSeccion);
				
		return id;
		
	}
	
	
	private InformacionReciboCasaDTO obtieneInformacionReciboCasa (InformacionReciboCasaDTOId id) {
		InformacionReciboCasaDTO infoReciboCasa = null;		
		
		infoReciboCasa = informacionReciboCasaFacade.findById(id);
		
		if (infoReciboCasa != null) {
			return infoReciboCasa;
		}
		
		return new InformacionReciboCasaDTO();
		
	}
	
	
	
	
	private Double obtenerDerechosPoliza (BigDecimal monedaId, Double primaNetaCotizacion) {
		
		GastoExpedicionDTO gasto = gastoExpedicionFacade.getGastoExpedicion(monedaId, BigDecimal.valueOf(primaNetaCotizacion));
		
		return gasto.getGastoExpedicionPoliza();
		
	}
	
	
	private ComisionCotizacionDTO obtenerComision (InformacionReciboCasaDTOId infoReciboCasaId) {
		
		AgrupacionCotDTO agrupacion = agrupacionCotFacade.buscarPorCotizacion(infoReciboCasaId.getIdToCotizacion(),
				SistemaPersistencia.TIPO_PRIMER_RIESGO);
		List<ParametroGeneralDTO> parametrosLimiteSA = parametroGeneralFacade.findByProperty("id.codigoParametroGeneral", 
				BigDecimal.valueOf(30010D));
		Double limiteSA = Double.valueOf(parametrosLimiteSA.get(0).getValor());
		Double tipoCambioVal = 1D; //Tipo cambio Moneda Nacional
		
		
		ComisionCotizacionId id = new ComisionCotizacionId();
		id.setIdTcSubramo(new BigDecimal(infoReciboCasaId.getIdTcSubRamo()));
		id.setIdToCotizacion(infoReciboCasaId.getIdToCotizacion());
		
		ComisionCotizacionDTO comision = null;
		Double sumaAseguradaIncendio = 0D;
		if(id.getIdTcSubramo().intValue() == SistemaPersistencia.INCENDIO){
			limiteSA = limiteSA/tipoCambioVal;
			// Se obtiene la SA de Incendio por Inciso
			sumaAseguradaIncendio = coberturaCotizacionFacade
				.obtenerSACoberturasBasicasIncendioPorCotizacion(infoReciboCasaId.getIdToCotizacion());
			
								
			if (agrupacion != null) {
				
				CoberturaCotizacionId coberturaCotId = new CoberturaCotizacionId();
				
				coberturaCotId.setIdToCotizacion(infoReciboCasaId.getIdToCotizacion());
				coberturaCotId.setNumeroInciso(infoReciboCasaId.getNumeroInciso());
				coberturaCotId.setIdToSeccion(infoReciboCasaId.getIdToSeccion());
				coberturaCotId.setIdToCobertura(new BigDecimal(infoReciboCasaId.getIdToCobertura()));
				
				CoberturaCotizacionDTO coberturaCotizacion = coberturaCotizacionFacade.findById(coberturaCotId);
								
				if (coberturaCotizacion.getNumeroAgrupacion() == agrupacion.getId().getNumeroAgrupacion()) {
					id.setTipoPorcentajeComision(SistemaPersistencia.TIPO_PRR);
				}else{
					id.setTipoPorcentajeComision(SistemaPersistencia.TIPO_RO);
				}
			} else {
				if (sumaAseguradaIncendio < limiteSA) {
					id.setTipoPorcentajeComision(SistemaPersistencia.TIPO_RO);
				} else if (sumaAseguradaIncendio >= limiteSA) {
					id.setTipoPorcentajeComision(SistemaPersistencia.TIPO_RCI);
				}else{
					id.setTipoPorcentajeComision(SistemaPersistencia.TIPO_RO);	
				}
			}				
		}else{
			id.setTipoPorcentajeComision(SistemaPersistencia.TIPO_RO);
		}
		
		comision = comisionCotizacionFacade.findById(id);
		
		
		return comision;
		
	}
	
	private Double obtenerPorcentajePagoFraccionado (CotizacionDTO cotizacion) {
		try {
			Double pctPagoFraccionado = cotizacionFacade.obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacion);
			return pctPagoFraccionado;
			
		} catch (SystemException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	private Double obtenerPorcentajeIva (CotizacionDTO cotizacion) {
		//Se obtiene % de IVA
		try {
			Double ivaObtenido = codigoPostalIVAFacade.getIVAPorIdCotizacion(cotizacion.getIdToCotizacion(), "");
			return ivaObtenido;
		} catch (SystemException e) {
			throw new RuntimeException(e);
		}
	}
		
	
	@EJB
	private GastoExpedicionFacadeRemote gastoExpedicionFacade;
	
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	
	@EJB
	private AgrupacionCotFacadeRemote agrupacionCotFacade;
		
	@EJB
	private CoberturaFacadeRemote coberturaFacade;
	
	@EJB
	private InformacionReciboCasaFacadeRemote informacionReciboCasaFacade;
	
	@EJB	
	private CoberturaCotizacionFacadeRemote coberturaCotizacionFacade;

	@EJB
	private ClienteServiciosRemote clienteServicios;

//	@EJB
//	private CalculoCotizacionFacadeRemote calculoCotizacionFacade;

	@EJB
	private RiesgoCoberturaFacadeRemote riesgoCoberturaFacade;

	@EJB
	private CoberturaSeccionFacadeRemote coberturaSeccionFacade;

	@EJB
	private SeccionFacadeRemote seccionFacade;

	@EJB
	private TipoPolizaFacadeRemote tipoPolizaFacade;

	@EJB
	private IncisoCotizacionFacadeRemote incisoCotizacionFacade;
	
	@EJB
	private ComisionCotizacionFacadeRemote comisionCotizacionFacade;
		
	@EJB
	private SubRamoFacadeRemote subRamoFacade;
	
	@EJB
	private CotizacionFacadeRemote cotizacionFacade;
	
	@EJB
	private SolicitudFacadeRemote solicitudFacade;
	
	@EJB
	private ProductoFacadeRemote productoFacade;
		
	@EJB
	private DireccionFacadeRemote direccionFacade;
	
	@EJB
	private CodigoPostalIVAFacadeRemote codigoPostalIVAFacade;
	
	@EJB
	DatoIncisoCotizacionFacadeRemote datoIncisoCotizacionFacade;
	
	@EJB
	private TipoNegocioFacadeRemote tipoNegocioFacade;
	
	@PersistenceContext
    private EntityManager entityManager;
}
