package mx.com.afirme.midas.cotizacion.casa;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity InformacionReciboCasaDTO.
 * @see .InformacionReciboCasaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class InformacionReciboCasaFacade  implements InformacionReciboCasaFacadeRemote {
	//property constants





    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved InformacionReciboCasaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity InformacionReciboCasaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(InformacionReciboCasaDTO entity) {
    				LogUtil.log("saving InformacionReciboCasaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent InformacionReciboCasaDTO entity.
	  @param entity InformacionReciboCasaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(InformacionReciboCasaDTO entity) {
    				LogUtil.log("deleting InformacionReciboCasaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(InformacionReciboCasaDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved InformacionReciboCasaDTO entity and return it or a copy of it to the sender. 
	 A copy of the InformacionReciboCasaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity InformacionReciboCasaDTO entity to update
	 @return InformacionReciboCasaDTO the persisted InformacionReciboCasaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public InformacionReciboCasaDTO update(InformacionReciboCasaDTO entity) {
    				LogUtil.log("updating InformacionReciboCasaDTO instance", Level.INFO, null);
	        try {
	        	
	        	
	       		String queryString = 		"UPDATE InformacionReciboCasaDTO entity " +
						"SET    entity.descripcionProducto      = :getDescripcionProducto, " +
						"       entity.idCentroEmisor           = " + entity.getIdCentroEmisor() + ", " +
						"       entity.numeroPoliza                = " + entity.getNumeroPoliza() + ", " +
						"       entity.claveTipoEndoso          = " + entity.getClaveTipoEndoso() + ", " +
						"       entity.motivoCancelacion        = " + entity.getMotivoCancelacion() + ", " +
						"       entity.idContratante            = " + entity.getIdContratante() + ", " +
						"       entity.idAgente                 = " + entity.getIdAgente() + ", " +
						"       entity.idConductoCobro          = " + entity.getIdConductoCobro() + ", " +
						"       entity.idTcFormaPago            = " + entity.getIdTcFormaPago() + ", " +
						"       entity.idMoneda                 = " + entity.getIdMoneda() + ", " +
						"       entity.tipoCambio               = " + entity.getTipoCambio() + ", " +
						"       entity.codigoUsuario            = :getCodigoUsuario, " +
						"       entity.fechaInicioVigencia      = :getFechaInicioVigencia, " +
						"       entity.fechaFinVigencia         = :getFechaFinVigencia, " +
						"       entity.numeroCoberturasContratadas        = " + entity.getNumeroCoberturasContratadas() + ", " +
						"       entity.descripcionCobertura     = :getDescripcionCobertura, " +
						"       entity.auxiliar                 = " + entity.getAuxiliar() + ", " +
						"       entity.centroCosto              = " + entity.getCentroCosto() + ", " +
						"       entity.valorPrimaNeta           = " + entity.getValorPrimaNeta() + ", " +
						"       entity.valorBonificacionComision       = " + entity.getValorBonificacionComision() + ", " +
						"       entity.valorBonificacionComisionRPF = " + entity.getValorBonificacionComisionRPF() + ", " +
						"       entity.valorDerechos            = " + entity.getValorDerechos() + ", " +
						"       entity.valorRecargos            = " + entity.getValorRecargos() + ", " +
						"       entity.valorIva                 = " + entity.getValorIva() + ", " +
						"       entity.valorPrimaTotal          = " + entity.getValorPrimaTotal() + ", " +
						"       entity.comisionPrimaNetaAgente            = " + entity.getComisionPrimaNetaAgente() + ", " +
						"       entity.comisionRecargoAgente           = " + entity.getComisionRecargoAgente() + ", " +
						"       entity.comisionPrimaNetaSupervisor            = " + entity.getComisionPrimaNetaSupervisor() + ", " +
						"       entity.comisionRecargoSupervisor           = " + entity.getComisionRecargoSupervisor() + "  " +
						"WHERE " +
						"    entity.id.idToCotizacion = " + entity.getId().getIdToCotizacion() + " " +
						"    AND (entity.id.idTcSubRamo = " + entity.getId().getIdTcSubRamo() + ") " +
						"    AND (entity.id.idToProducto = " + entity.getId().getIdToProducto() + ") " +
						"    AND (entity.id.idTipo = " + entity.getId().getIdTipo() + ") " +
						"    AND (entity.id.numeroInciso = " + entity.getId().getNumeroInciso() + ") " +
						"    AND (entity.id.idToPoliza = " + entity.getId().getIdToPoliza() + ") " +
						"    AND (entity.id.numeroRenovacion = " + entity.getId().getNumeroRenovacion() + ") " +
						"    AND (entity.id.numeroEndoso = " + entity.getId().getNumeroEndoso() + ") " +
						"    AND (entity.id.idTcRamo = " + entity.getId().getIdTcRamo() + ") " +
						"    AND (entity.id.idToCobertura = " + entity.getId().getIdToCobertura() + ")";
	        	 
	        	Query query = entityManager.createQuery(queryString);
	        	
	        	
	        	
	        	query.setParameter("getDescripcionProducto", entity.getDescripcionProducto());
	        	query.setParameter("getFechaInicioVigencia", entity.getFechaInicioVigencia());
	        	query.setParameter("getFechaFinVigencia", entity.getFechaFinVigencia());
	        	query.setParameter("getDescripcionCobertura", entity.getDescripcionCobertura());
	        	query.setParameter("getCodigoUsuario", entity.getCodigoUsuario());
	        	
	        	
	            query.executeUpdate();
	        	
	        	
//            InformacionReciboCasaDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return entity;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public InformacionReciboCasaDTO findById( InformacionReciboCasaDTOId id) {
    				LogUtil.log("finding InformacionReciboCasaDTO instance with id: " + id, Level.INFO, null);
	        try {
	        	
	        	
	        	String queryString = "SELECT " +
	        		"  IDCONTRATANTE, VALORBONIFCOMRECPAGOFRAC, IDCENTROEMISOR, VALORPRIMATOTAL, IDAGENTE, FECHAINICIOVIGENCIA, " +
	        		"AUXILIAR, FECHAFINVIGENCIA, VALORDERECHOS, COMISIONRCGAGT, TIPOCAMBIO, DESCRIPCIONCOBERTURA, VALORIVA, " +
	        		"COMISIONRCGSUP, NUMPOLIZA, NUMCOBCONTRATADAS, COMISIONPNAGT, IDCONDUCTOCOBRO, IDTCFORMAPAGO, DESCRIPCIONPRODUCTO, " +
	        		"CODIGOUSUARIO, CLAVETIPOENDOSO, VALORRECARGOS, COMISIONPNSUP, VALORBONIFCOMISION, VALORPRIMANETA, CENTROCOSTO, " +
	        		"IDMONEDA, MOTIVOCANCELACION, IDTOCOTIZACION, IDTCSUBRAMO, IDTOPRODUCTO, IDTIPO, NUMEROINCISO, IDTOPOLIZA, " +
	        		"NUMRENOVACION, NUMEROENDOSO, IDTCRAMO, IDTOCOBERTURA   " +
					"FROM  " +
					"    MIDAS.TOINFORMACIONRECIBOSCASA " + 
					"WHERE " +
					"    IDTOCOTIZACION = " + id.getIdToCotizacion() + " " +
					"    AND (IDTCSUBRAMO = " + id.getIdTcSubRamo() + ") " +
					"    AND (IDTOPRODUCTO = " + id.getIdToProducto() + ") " +
					"    AND (IDTIPO = " + id.getIdTipo() + ") " +
					"    AND (NUMEROINCISO = " + id.getNumeroInciso() + ") " +
					"    AND (IDTOPOLIZA = " + id.getIdToPoliza() + ") " +
					"    AND (NUMRENOVACION = " + id.getNumeroRenovacion() + ") " +
					"    AND (NUMEROENDOSO = " + id.getNumeroEndoso() + ") " +
					"    AND (IDTCRAMO = " + id.getIdTcRamo() + ") " +
					"    AND (IDTOCOBERTURA = " + id.getIdToCobertura() + ")";
	        	 
	        	
//	        	String queryString2 = "select model from InformacionReciboCasaDTO model where model.id"
//					+ "= :id";
	        	
	        	entityManager.clear();
	        	
				Query query = entityManager.createNativeQuery(queryString);
//				query.setParameter("id", id);
//				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
//				InformacionReciboCasaDTO instance =  (InformacionReciboCasaDTO) query.getSingleResult();
				
				Object[] resultado =  new Object[30]; 
				
				try {
					resultado = (Object[])query.getSingleResult();
				} catch (NoResultException nre) {
					return null;
				}
				InformacionReciboCasaDTO instance = new InformacionReciboCasaDTO();
				
				instance.setIdContratante(resultado[0]!=null?(BigDecimal)resultado[0]:null);
				instance.setValorBonificacionComisionRPF(resultado[1]!=null?Double.parseDouble(resultado[1].toString()):null);
				instance.setIdCentroEmisor(resultado[2]!=null?(BigDecimal)resultado[2]:null);
				instance.setValorPrimaTotal(resultado[3]!=null?Double.parseDouble(resultado[3].toString()):null);
				instance.setIdAgente(resultado[4]!=null?(BigDecimal)resultado[4]:null);
				instance.setFechaInicioVigencia(resultado[5]!=null?(Date)resultado[5]:null);
				instance.setAuxiliar(resultado[6]!=null?Integer.parseInt(resultado[6].toString()):null);
				instance.setFechaFinVigencia(resultado[7]!=null?(Date)resultado[7]:null);
				instance.setValorDerechos(resultado[8]!=null?Double.parseDouble(resultado[8].toString()):null);
				instance.setComisionRecargoAgente(resultado[9]!=null?Double.parseDouble(resultado[9].toString()):null);
				instance.setTipoCambio(resultado[10]!=null?Double.parseDouble(resultado[10].toString()):null);  
				instance.setDescripcionCobertura(resultado[11]!=null?(String)resultado[11]:null);
				instance.setValorIva(resultado[12]!=null?Double.parseDouble(resultado[12].toString()):null);
				instance.setComisionRecargoSupervisor(resultado[13]!=null?Double.parseDouble(resultado[13].toString()):null);
				instance.setNumeroPoliza(resultado[14]!=null?(BigDecimal)resultado[14]:null); 
				instance.setNumeroCoberturasContratadas(resultado[15]!=null?(BigDecimal)resultado[15]:null);
				instance.setComisionPrimaNetaAgente(resultado[16]!=null?Double.parseDouble(resultado[16].toString()):null);
				instance.setIdConductoCobro(resultado[17]!=null?(BigDecimal)resultado[17]:null);
				instance.setIdTcFormaPago(resultado[18]!=null?(BigDecimal)resultado[18]:null);
				instance.setDescripcionProducto(resultado[19]!=null?(String)resultado[19]:null);
				instance.setCodigoUsuario(resultado[20]!=null?(String)resultado[20]:null);
				instance.setClaveTipoEndoso(resultado[21]!=null?(BigDecimal)resultado[21]:null);
				instance.setValorRecargos(resultado[22]!=null?Double.parseDouble(resultado[22].toString()):null);
				instance.setComisionPrimaNetaSupervisor(resultado[23]!=null?Double.parseDouble(resultado[23].toString()):null);
				instance.setValorBonificacionComision(resultado[24]!=null?Double.parseDouble(resultado[24].toString()):null);
				instance.setValorPrimaNeta(resultado[25]!=null?Double.parseDouble(resultado[25].toString()):null);
				instance.setCentroCosto(resultado[26]!=null?(String)resultado[26]:null);
				instance.setIdMoneda(resultado[27]!=null?(BigDecimal)resultado[27]:null);
				instance.setMotivoCancelacion(resultado[28]!=null?(String)resultado[28]:null);
				instance.setId(id);
				
				
//            	InformacionReciboCasaDTO instance = entityManager.find(InformacionReciboCasaDTO.class, id);
            	return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all InformacionReciboCasaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the InformacionReciboCasaDTO property to query
	  @param value the property value to match
	  	  @return List<InformacionReciboCasaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<InformacionReciboCasaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding InformacionReciboCasaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from InformacionReciboCasaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all InformacionReciboCasaDTO entities.
	  	  @return List<InformacionReciboCasaDTO> all InformacionReciboCasaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<InformacionReciboCasaDTO> findAll(
		) {
					LogUtil.log("finding all InformacionReciboCasaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from InformacionReciboCasaDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}