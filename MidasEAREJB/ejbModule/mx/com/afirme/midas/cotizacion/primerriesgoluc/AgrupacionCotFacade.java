package mx.com.afirme.midas.cotizacion.primerriesgoluc;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity AgrupacionCotDTO.
 * 
 * @see .AgrupacionCotDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class AgrupacionCotFacade implements AgrupacionCotFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved AgrupacionCotDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            AgrupacionCotDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(AgrupacionCotDTO entity) {
		LogDeMidasEJB3
				.log("saving AgrupacionCotDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent AgrupacionCotDTO entity.
	 * 
	 * @param entity
	 *            AgrupacionCotDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(AgrupacionCotDTO entity) {
		LogDeMidasEJB3.log("deleting AgrupacionCotDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(AgrupacionCotDTO.class, entity
					.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved AgrupacionCotDTO entity and return it or a
	 * copy of it to the sender. A copy of the AgrupacionCotDTO entity parameter
	 * is returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            AgrupacionCotDTO entity to update
	 * @return AgrupacionCotDTO the persisted AgrupacionCotDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public AgrupacionCotDTO update(AgrupacionCotDTO entity) {
		LogDeMidasEJB3.log("updating AgrupacionCotDTO instance", Level.INFO,
				null);
		try {
			AgrupacionCotDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public AgrupacionCotDTO findById(AgrupacionCotId id) {
		LogDeMidasEJB3.log("finding AgrupacionCotDTO instance with id: " + id,
				Level.INFO, null);
		try {
			AgrupacionCotDTO instance = entityManager.find(
					AgrupacionCotDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all AgrupacionCotDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the AgrupacionCotDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<AgrupacionCotDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<AgrupacionCotDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding AgrupacionCotDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from AgrupacionCotDTO model where model."
					+ propertyName
					+ "= :propertyValue"
					+ " order by model.id.idToCotizacion,model.claveTipoAgrupacion,model.idToSeccion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all AgrupacionCotDTO entities.
	 * 
	 * @return List<AgrupacionCotDTO> all AgrupacionCotDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<AgrupacionCotDTO> findAll() {
		LogDeMidasEJB3.log("finding all AgrupacionCotDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from AgrupacionCotDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<AgrupacionCotDTO> listarFiltrado(AgrupacionCotDTO dto) {
		LogDeMidasEJB3.log("finding all AgrupacionCotDTO instances",
				Level.INFO, null);
		try {
			String queryString = "select model from AgrupacionCotDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (dto == null)
				return null;
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
						sWhere, "id.idToCotizacion", dto.getId()
								.getIdToCotizacion());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
						sWhere, "id.numeroAgrupacion", dto.getId()
								.getNumeroAgrupacion());
			} catch (NullPointerException e) {
			}
			sWhere = Utilerias
					.agregaParametroQuery(listaParametrosValidos, sWhere,
							"claveTipoAgrupacion", dto.getClaveTipoAgrupacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "idToSeccion", dto.getIdToSeccion());
			if (Utilerias.esAtributoQueryValido(sWhere)) {
				queryString = queryString.concat(" where ").concat(sWhere);
				queryString += " order by model.id.idToCotizacion,model.claveTipoAgrupacion,model.claveTipoAgrupacion";
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();

		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarFiltrado failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Short maxAgrupacion(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("count agrupacion", Level.INFO, null);
		try {
			final String queryString = "select max(model.id.numeroAgrupacion) from AgrupacionCotDTO model where model.id.idToCotizacion = :idToCotizacion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			Short numAgrupacion = (Short) query.getSingleResult();
			if (numAgrupacion == null) {
				numAgrupacion = Short.valueOf("1");
			} else {
				numAgrupacion = Short.valueOf(Integer.valueOf(
						numAgrupacion.intValue() + 1).toString());
			}
			return numAgrupacion;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("count agrupacion", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public AgrupacionCotDTO buscarPorCotizacion(BigDecimal idToCotizacion,
			Short claveTipoAgrupacion) {
		LogDeMidasEJB3.log(
				"finding AgrupacionCotDTO instance with idToCotizacion: "
						+ idToCotizacion, Level.INFO, null);
		try {
			String queryString = "select model from AgrupacionCotDTO model where model.id.idToCotizacion = :idToCotizacion and model.claveTipoAgrupacion = :claveTipoAgrupacion"
					+ " order by model.id.idToCotizacion, model.claveTipoAgrupacion, model.claveTipoAgrupacion ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("claveTipoAgrupacion", claveTipoAgrupacion);
			List<AgrupacionCotDTO> resultados = (List<AgrupacionCotDTO>) query
					.getResultList();
			AgrupacionCotDTO agrupacionCotDTO = null;
			if (resultados.size() > 0)
				agrupacionCotDTO = resultados.get(0);

			return agrupacionCotDTO;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<AgrupacionCotDTO> buscarPorCotizacionAgrupacion(
			BigDecimal idToCotizacion, Short claveTipoAgrupacion) {
		LogDeMidasEJB3.log(
				"finding AgrupacionCotDTO instance with idToCotizacion: "
						+ idToCotizacion, Level.INFO, null);
		try {
			final String queryString = "select model from AgrupacionCotDTO model where model.id.idToCotizacion = :idToCotizacion and model.claveTipoAgrupacion = :claveTipoAgrupacion ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("claveTipoAgrupacion", claveTipoAgrupacion);
			List<AgrupacionCotDTO> resultados = (List<AgrupacionCotDTO>) query
					.getResultList();
			return resultados;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public AgrupacionCotDTO buscarPorCotizacionSeccion(
			BigDecimal idToCotizacion, Short claveTipoAgrupacion,
			BigDecimal idToSeccion) {
		LogDeMidasEJB3.log(
				"finding AgrupacionCotDTO instance with idToCotizacion: "
						+ idToCotizacion, Level.INFO, null);
		try {
			final String queryString = "select model from AgrupacionCotDTO model where model.id.idToCotizacion = :idToCotizacion and model.claveTipoAgrupacion = :claveTipoAgrupacion and model.idToSeccion = :idToSeccion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("claveTipoAgrupacion", claveTipoAgrupacion);
			query.setParameter("idToSeccion", idToSeccion);
			List<AgrupacionCotDTO> resultados = (List<AgrupacionCotDTO>) query
					.getResultList();
			AgrupacionCotDTO agrupacionCotDTO = null;
			if (resultados.size() > 0)
				agrupacionCotDTO = resultados.get(0);

			return agrupacionCotDTO;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	public void borrarAgrupaciones(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("borrando agrupaciones de la cotizacion: "
				+ idToCotizacion, Level.INFO, null);
		try {
			final String queryString = "DELETE MIDAS.TOAGRUPACIONCOT WHERE IDTOCOTIZACION ="
					+ idToCotizacion;
			Query query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Error al tratar de borrar agrupaciones",
					Level.SEVERE, re);
			throw re;
		}
	}
	
	public void borrarAgrupacion(BigDecimal idToCotizacion, BigDecimal numeroAgrupacion,  BigDecimal idToSeccion) {
		LogDeMidasEJB3.log("borrando agrupaciones de la cotizacion: "
				+ idToCotizacion, Level.INFO, null);
		try {
			final String queryString = "DELETE MIDAS.TOAGRUPACIONCOT WHERE IDTOCOTIZACION ="
					+ idToCotizacion + " AND NUMEROAGRUPACION = "+ numeroAgrupacion +" AND IDTOSECCION =" +idToSeccion;
			Query query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Error al tratar de borrar agrupaciones",
					Level.SEVERE, re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public List<SeccionDTO> listarSeccionesConAgrupacionPrimerRiesgo(BigDecimal idToCotizacion){
		LogDeMidasEJB3.log("finding listarSeccionesConAgrupacionPrimerRiesgo SeccionCotizacionDTO instances", Level.INFO, null);
		String queryString = "";
		List<SeccionDTO> secciones = new ArrayList<SeccionDTO>();
		try {
			if (idToCotizacion==null)
				return null;	
			queryString = " SELECT DISTINCT SEC.IDTOSECCION " +
				" FROM MIDAS.TOAGRUPACIONCOT AGR, " +
				" MIDAS.TOSECCIONCOT SEC, " +
				" MIDAS.TOCOBERTURACOT COB " +
				" WHERE AGR.IDTOCOTIZACION = " +idToCotizacion+
				" AND AGR.IDTOSECCION = 0 " + 
		        " AND AGR.CLAVETIPOAGRUPACION = 1 " +				
				" AND SEC.IDTOCOTIZACION = AGR.IDTOCOTIZACION " +
				" AND SEC.CLAVECONTRATO = 1 " +
				" AND COB.IDTOCOTIZACION =SEC.IDTOCOTIZACION " +
				" AND COB.NUMEROINCISO = SEC.NUMEROINCISO " +
				" AND COB.IDTOSECCION = SEC.IDTOSECCION " +
				" AND COB.CLAVECONTRATO = 1 " +
				" AND COB.NUMEROAGRUPACION = AGR.NUMEROAGRUPACION";			

			Query query = entityManager.createNativeQuery(queryString);
			List resultados = query.getResultList();
			for (Object resultado : resultados) {
				BigDecimal idToSeccion = (BigDecimal) ((Object) resultado);
				queryString = "select model from SeccionDTO model where "
					+ "model.idToSeccion =:idToSeccion ";
				query = entityManager.createQuery(queryString);
				query.setParameter("idToSeccion", idToSeccion);		
				
				SeccionDTO seccion = (SeccionDTO)query.getSingleResult();
				secciones.add(seccion);
			}
			
			return secciones;
		} catch (RuntimeException re) {
			re.printStackTrace();
			LogDeMidasEJB3.log("listarSeccionesConAgrupacionPrimerRiesgo failed", Level.SEVERE, re);
			throw re;
		}
	}
		
	/**
	 * Lista las agrupaciones de tipo LUC de una cotización que estén mal registradas en al menos una cobertura. 
	 * (coberturas que tengan numeroAgrupacion distinto al de la agrupación que le corresponda) 
	 * @param idToCotizacion
	 * @return List<AgrupacionCotDTO> entidades AgrupacionCotDTO encontradas por el query.
	 */
	@SuppressWarnings("unchecked")
	public List<AgrupacionCotDTO> listarAgrupacionesLUCMalRegistradas(BigDecimal idToCotizacion){
		LogDeMidasEJB3.log("Ejecutando listarAgrupacionesLUCMalRegistradas para idToCotizacion = "+idToCotizacion, Level.INFO, null);		
		try {			
			String queryString = "select distinct agr " +
							 	 "from AgrupacionCotDTO agr, CoberturaCotizacionDTO cob " +
							 	 "where agr.id.idToCotizacion = :idToCotizacion " +
							 	 "  and agr.id.idToCotizacion = cob.id.idToCotizacion " +
							 	 "  and agr.idToSeccion = cob.id.idToSeccion " +
							 	 "  and cob.claveContrato = 1 " +
							 	 "  and cob.seccionCotizacionDTO.claveContrato = 1 " +
							 	 "  and agr.claveTipoAgrupacion = 2 " +
							 	 "  and agr.id.numeroAgrupacion <> cob.numeroAgrupacion";
		
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);		
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);		
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Falló listarAgrupacionesLUCMalRegistradas para idToCotizacion = "+idToCotizacion, Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Lista las agrupaciones existentes en una cotización (idToCotizacionAgrExistente), pero que no se encuentran en otra (idToCotizacionAgrInexistente).  
	 * @param idToCotizacionAgrExistente
	 * @param idToCotizacionAgrInexistente 
	 * @return List<AgrupacionCotDTO> entidades AgrupacionCotDTO encontradas por el query.
	 */
	@SuppressWarnings("unchecked")
	public List<AgrupacionCotDTO> listarAgrupacionesExcluyentes(BigDecimal idToCotizacionAgrExistente, BigDecimal idToCotizacionAgrInexistente){
		LogDeMidasEJB3.log("Ejecutando listarAgrupacionesExcluyentes para idToCotizacionAgrExistente = "+idToCotizacionAgrExistente+", " +
																		 "idToCotizacionAgrInexistente = "+idToCotizacionAgrInexistente, 
		Level.INFO, null);		
		try {			
			String queryString = "select agr1 " +
							 	 "from AgrupacionCotDTO agr1 " +
							 	 "where agr1.id.idToCotizacion = :idToCotizacionAgrExistente " +
							 	 "  and agr1.idToSeccion not in (select agr2.idToSeccion " +
							 	 								"from AgrupacionCotDTO agr2 " +
							 	 								"where agr2.id.idToCotizacion = :idToCotizacionAgrInexistente) " +
							 	 "order by agr1.claveTipoAgrupacion, agr1.idToSeccion";			
		
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacionAgrExistente", idToCotizacionAgrExistente);		
			query.setParameter("idToCotizacionAgrInexistente", idToCotizacionAgrInexistente);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);		
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Falló listarAgrupacionesExcluyentes para idToCotizacionAgrExistente = "+idToCotizacionAgrExistente+", " +
																		 "idToCotizacionAgrInexistente = "+idToCotizacionAgrInexistente, 
			Level.SEVERE, re);
			throw re;
		}
	}
	
	public List<AgrupacionCotDTO> listarPorCotizacion(BigDecimal idToCotizacion){
		return findByProperty("cotizacionDTO.idToCotizacion", idToCotizacion);
	}
}