package mx.com.afirme.midas.cotizacion.inciso.cargamasiva;

// default package

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.esquemasreas.EsquemasDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity CargaMasivaDetalleCotDTO.
 * 
 * @see .CargaMasivaDetalleCotDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CargaMasivaDetalleCotFacade implements
		CargaMasivaDetalleCotFacadeRemote {
	
	
	
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved CargaMasivaDetalleCotDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CargaMasivaDetalleCotDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CargaMasivaDetalleCotDTO entity) {
		LogDeMidasEJB3.log("saving CargaMasivaDetalleCotDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent CargaMasivaDetalleCotDTO entity.
	 * 
	 * @param entity
	 *            CargaMasivaDetalleCotDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CargaMasivaDetalleCotDTO entity) {
		LogDeMidasEJB3.log("deleting CargaMasivaDetalleCotDTO instance",
				Level.INFO, null);
		try {
			entityManager.remove(
					entityManager.getReference(CargaMasivaDetalleCotDTO.class,entity.getId()));
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved CargaMasivaDetalleCotDTO entity and return it
	 * or a copy of it to the sender. A copy of the CargaMasivaDetalleCotDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CargaMasivaDetalleCotDTO entity to update
	 * @return CargaMasivaDetalleCotDTO the persisted CargaMasivaDetalleCotDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CargaMasivaDetalleCotDTO update(CargaMasivaDetalleCotDTO entity) {
		LogDeMidasEJB3.log("updating CargaMasivaDetalleCotDTO instance",
				Level.INFO, null);
		try {
			CargaMasivaDetalleCotDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CargaMasivaDetalleCotDTO findById(CargaMasivaDetalleCotId id) {
		LogDeMidasEJB3.log(
				"finding CargaMasivaDetalleCotDTO instance with id: " + id,
				Level.INFO, null);
		try {
			return entityManager.find(
					CargaMasivaDetalleCotDTO.class, id);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CargaMasivaDetalleCotDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the CargaMasivaDetalleCotDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CargaMasivaDetalleCotDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CargaMasivaDetalleCotDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding CargaMasivaDetalleCotDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from CargaMasivaDetalleCotDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CargaMasivaDetalleCotDTO entities.
	 * 
	 * @return List<CargaMasivaDetalleCotDTO> all CargaMasivaDetalleCotDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<CargaMasivaDetalleCotDTO> findAll() {
		LogDeMidasEJB3.log("finding all CargaMasivaDetalleCotDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from CargaMasivaDetalleCotDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public void save(EsquemasDTO arg0) {
		
	}

}