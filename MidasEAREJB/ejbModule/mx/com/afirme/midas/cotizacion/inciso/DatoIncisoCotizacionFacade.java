package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.equipoelectronico.SubtipoEquipoElectronicoDTO;
import mx.com.afirme.midas.catalogos.equipoelectronico.SubtipoEquipoElectronicoFacadeRemote;
import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.giro.SubGiroFacadeRemote;
import mx.com.afirme.midas.catalogos.girorc.SubGiroRCDTO;
import mx.com.afirme.midas.catalogos.girorc.SubGiroRCFacadeRemote;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaDTO;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaFacadeRemote;
import mx.com.afirme.midas.catalogos.tipomaquinaria.SubTipoMaquinariaDTO;
import mx.com.afirme.midas.catalogos.tipomaquinaria.SubTipoMaquinariaFacadeRemote;
import mx.com.afirme.midas.catalogos.tipomontajemaquina.SubtipoMontajeMaquinaDTO;
import mx.com.afirme.midas.catalogos.tipomontajemaquina.SubtipoMontajeMaquinaFacadeRemote;
import mx.com.afirme.midas.catalogos.tipomuro.TipoMuroDTO;
import mx.com.afirme.midas.catalogos.tipomuro.TipoMuroFacadeRemote;
import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilDTO;
import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilFacadeRemote;
import mx.com.afirme.midas.catalogos.tipotecho.TipoTechoDTO;
import mx.com.afirme.midas.catalogos.tipotecho.TipoTechoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity DatoIncisoCotizacionDTO.
 * 
 * @see .DatoIncisoCotizacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class DatoIncisoCotizacionFacade implements DatoIncisoCotizacionFacadeRemote {
	// property constants
	public static final String VALOR = "valor";

	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private TipoTechoFacadeRemote tipoTechoFacade;
	
	@EJB
	private TipoMuroFacadeRemote tipoMuroFacade;
	
	@EJB
	private SubGiroFacadeRemote subGiroFacade;

	@EJB
	private SubGiroRCFacadeRemote subGiroRCFacade;
	
	@EJB
	private SubTipoMaquinariaFacadeRemote subTipoMaquinariaFacade;
	
	@EJB
	private SubtipoMontajeMaquinaFacadeRemote subtipoMontajeMaquinaFacade;
	
	@EJB
	private SubtipoEquipoElectronicoFacadeRemote subtipoEquipoElectronicoFacade;
	
	@EJB
	private SubtipoEquipoContratistaFacadeRemote subtipoEquipoContratistaFacade;
	
	@EJB
	private TipoObraCivilFacadeRemote tipoObraCivilFacade;
	
	
	/**
	 * Perform an initial save of a previously unsaved DatoIncisoCotizacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            DatoIncisoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DatoIncisoCotizacionDTO entity) {
		LogDeMidasEJB3.log("saving DatoIncisoCotizacionDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
            entityManager.merge(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent DatoIncisoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            DatoIncisoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DatoIncisoCotizacionDTO entity) {
		LogDeMidasEJB3.log("deleting DatoIncisoCotizacionDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(DatoIncisoCotizacionDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved DatoIncisoCotizacionDTO entity and return it
	 * or a copy of it to the sender. A copy of the DatoIncisoCotizacionDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            DatoIncisoCotizacionDTO entity to update
	 * @return DatoIncisoCotizacionDTO the persisted DatoIncisoCotizacionDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DatoIncisoCotizacionDTO update(DatoIncisoCotizacionDTO entity) {
		LogDeMidasEJB3.log("updating DatoIncisoCotizacionDTO instance",
				Level.INFO, null);
		try {
			DatoIncisoCotizacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DatoIncisoCotizacionDTO findById(DatoIncisoCotizacionId id) {
		LogDeMidasEJB3.log("finding DatoIncisoCotizacionDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			DatoIncisoCotizacionDTO instance = entityManager.find(
					DatoIncisoCotizacionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DatoIncisoCotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the DatoIncisoCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DatoIncisoCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<DatoIncisoCotizacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding DatoIncisoCotizacionDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from DatoIncisoCotizacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<DatoIncisoCotizacionDTO> findByValor(Object valor) {
		return findByProperty(VALOR, valor);
	}

	/**
	 * Find all DatoIncisoCotizacionDTO entities.
	 * 
	 * @return List<DatoIncisoCotizacionDTO> all DatoIncisoCotizacionDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<DatoIncisoCotizacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all DatoIncisoCotizacionDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from DatoIncisoCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public void deleteAll(BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		LogDeMidasEJB3.log(
						"deleting all DatoIncisoCotizacionDTO instances with idToCotizacion: "
								+ idToCotizacion + " and numeroInciso: "
								+ numeroInciso, Level.INFO, null);
		try {
			String queryString = "delete from DatoIncisoCotizacionDTO model " +
					"where model.id.idToCotizacion = :idToCotizacion and model.id.numeroInciso = :numeroInciso";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Filtra los registros de DatoIncisoCotizacionDTO en base a los atributos recibidos en el objeto DatoIncisoCotizacionId.
	 * Los atributos enviados en la consulta son: idToCotizacion, numeroInciso, idToSeccion, idToCobertura, idToRiesgo,
	 * numeroSubinciso, idTcRamo, idTcSubramo, claveDetalle, idDato. Los atributos que se reciben con valor de null son ignorados.
	 * @param DatoIncisoCotizacionId idDatoInciso
	 */
	@SuppressWarnings("unchecked")
	public List<DatoIncisoCotizacionDTO> listarPorIdFiltrado(DatoIncisoCotizacionId idDatoInciso) {
		LogDeMidasEJB3.log("buscando instancias DatoIncisoCotizacionDTO con idToCotizacion: "+idDatoInciso.getIdToCotizacion()+
				", numeroInciso: "+idDatoInciso.getNumeroInciso()+", idToSeccion: "+idDatoInciso.getIdToSeccion()+
				", idToCobertura: "+idDatoInciso.getIdToCobertura()+", idToRiesgo: "+idDatoInciso.getIdToRiesgo()+
				", numeroSubInciso: "+idDatoInciso.getNumeroSubinciso()+", idTcRamo: "+idDatoInciso.getIdTcRamo()+
				", idTcSubRamo: "+idDatoInciso.getIdTcSubramo()+", claveDetalle: "+idDatoInciso.getClaveDetalle()+
				", idDato: "+idDatoInciso.getIdDato(),Level.INFO, null);
		try {
			String queryString = "select model from DatoIncisoCotizacionDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (idDatoInciso == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToCotizacion", idDatoInciso.getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.numeroInciso", idDatoInciso.getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToSeccion", idDatoInciso.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToCobertura", idDatoInciso.getIdToCobertura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToRiesgo", idDatoInciso.getIdToRiesgo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.numeroSubinciso", idDatoInciso.getNumeroSubinciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idTcRamo", idDatoInciso.getIdTcRamo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idTcSubramo", idDatoInciso.getIdTcSubramo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.claveDetalle", idDatoInciso.getClaveDetalle());			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idDato", idDatoInciso.getIdDato());			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			queryString = queryString.concat(" order by model.id.numeroInciso, model.id.idToSeccion, model.id.idToCobertura, model.id.idToRiesgo, model.id.numeroSubinciso, " +
					"model.id.idTcRamo, model.id.idTcSubramo, model.id.claveDetalle, model.id.idDato");
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DatoIncisoCotizacionDTO> getDatosRamoInciso(
			BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		try {
			String sql = "select model from DatoIncisoCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion " +
					"and model.id.numeroInciso = :numeroInciso " +
					"and model.id.idTcRamo > 0 " +
					"and model.id.idTcSubramo = 0 " +
					"and model.id.idToRiesgo = 0 " +
					"and model.id.claveDetalle = 0 " +
					"order by model.id.idTcRamo, model.id.idDato";
			Query query = entityManager.createQuery(sql);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			return (List<DatoIncisoCotizacionDTO>) query.getResultList();
		} catch (RuntimeException ex) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, ex);
			throw ex;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DatoIncisoCotizacionDTO> getDatosRiesgoInciso(
			BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		try {
			String sql = "select model from DatoIncisoCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion " +
					"and model.id.numeroInciso = :numeroInciso " +
					"and model.id.idToRiesgo > 0 " +
					"and model.id.claveDetalle = 0 " +
					"order by model.id.idToRiesgo, model.id.idDato";
			Query query = entityManager.createQuery(sql);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			return (List<DatoIncisoCotizacionDTO>) query.getResultList();
		} catch (RuntimeException ex) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, ex);
			throw ex;
		}
	}
	
	/**
	 * Obtiene el objeto TipoTecho correspondiente a un inciso, utilizando la configuraci�n adecuada para la tabla DatoIncisoCotizacion
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return TipoTechoDTO. Entidad tipoTechoDTO correspondiente al inciso, en base al valor del registro DatoInciso encontrado. Si no se encuentra el 
	 * registro, devuelve null.
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public TipoTechoDTO obtenerTipoTecho(BigDecimal idToCotizacion,BigDecimal numeroInciso){
		DatoIncisoCotizacionDTO datoInciso = DatoIncisoCotizacionDTO.instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoInciso.getId().setIdTcRamo(new BigDecimal(1d));
		datoInciso.getId().setIdDato(new BigDecimal(30d));

		TipoTechoDTO tipoTechoDTO = null;
		try {
			datoInciso = findById(datoInciso.getId());
			if (datoInciso == null){
				LogDeMidasEJB3.log("No se encontr� DatoInciso para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+1+", idDato: "+"30",Level.WARNING,null);
			}
			else{
				tipoTechoDTO = tipoTechoFacade.findById(Utilerias.regresaBigDecimal(datoInciso.getValor()));
			}
		} catch (Exception e) {}
		if (tipoTechoDTO == null){
			LogDeMidasEJB3.log("No se encontr� TipoTechoDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso,Level.WARNING,null);
		}
		return tipoTechoDTO;
	}
	
	/**
	 * Obtiene el objeto TipoMuro correspondiente a un inciso, utilizando la configuraci�n adecuada para la tabla DatoIncisoCotizacion
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return TipoMuroDTO. Entidad tipoMuroDTO correspondiente al inciso, en base al valor del registro DatoInciso encontrado. Si no se encuentra el 
	 * registro, devuelve null.
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public TipoMuroDTO obtenerTipoMuro(BigDecimal idToCotizacion,BigDecimal numeroInciso){
		DatoIncisoCotizacionDTO datoInciso = DatoIncisoCotizacionDTO.instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoInciso.getId().setIdTcRamo(new BigDecimal(1d));
		datoInciso.getId().setIdDato(new BigDecimal(40d));
		
		TipoMuroDTO tipoMuroDTO = null;
		try {
			datoInciso = findById(datoInciso.getId());
			if (datoInciso == null){
				LogDeMidasEJB3.log("No se encontr� DatoInciso para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+1+", idDato: "+"40",Level.WARNING,null);
			}
			tipoMuroDTO = tipoMuroFacade.findById(Utilerias.regresaBigDecimal(datoInciso.getValor()));
		} catch (Exception e) {}
		if (tipoMuroDTO == null){
			LogDeMidasEJB3.log("No se encontr� TipoMuroDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso,Level.WARNING,null);
		}
		return tipoMuroDTO;
	}
	
	/**
	 * Obtiene el registro de la tabla DatoIncisoCotizacion que contiene el n�mero de pisos del inciso cuyos atributos se reciben.
	 * El n�mero de pisos se recupera del campo "valor" de la tabla DatoIncisoCotizacion
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return String numero de pisos del inciso, si no se encuentra el registro, regresa "No disponible";
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String obtenerNumeroPisos(BigDecimal idToCotizacion,BigDecimal numeroInciso){
		//numero de pisos. Se obtiene de la tabla ToDatoIncisoCot.
		DatoIncisoCotizacionDTO datoInciso = DatoIncisoCotizacionDTO.instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoInciso.getId().setIdTcRamo(new BigDecimal(1d));
		datoInciso.getId().setIdDato(new BigDecimal(50d));
		
		String numeroPisos = "No disponible";
		try {
			datoInciso = findById(datoInciso.getId());
			if (datoInciso == null){
				LogDeMidasEJB3.log("No se encontr� DatoInciso para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+1+", idDato: "+"50",Level.WARNING,null);
			}
			numeroPisos = datoInciso.getValor();
		} catch (Exception e) {}
		return numeroPisos;
	}
	
	public List<DatoIncisoCotizacionDTO> listarPorIdToCotizacion(BigDecimal idToCotizacion) {
		return findByProperty("id.idToCotizacion", idToCotizacion);
	}
	
	/**
	 * Regresa el subtipo correspondiente al SubRamoDTO recibido. los Subtipos considerados son los siguientes: subGiro, subGiroRC, 
	 * SubTipoMaquinaria, SubTipoMontajeMaquina, SubTipoEquipoElectronico, SubTipoEquipoContratista y tipoObraCivil 
	 * @return Object el objeto de los datos de riesgo correspondiente al subramo recibido de la cotizaci�n e inciso recibidos. null en caso de error
	 * @throws SystemException. En caso de no encontrar el datoIncisoCotizacion, o de no encontrar la entidad a la que hace referencia el DatoIncisoCotizacion 
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Object obtenerDTOSubTipoCorrespondienteAlSubRamo(SubRamoDTO subRamoDTO,BigDecimal idToCotizacion, BigDecimal numeroInciso) throws SystemException{
		Object result = null;
		if (subRamoDTO.getIdTcSubRamo().compareTo(SubRamoDTO.SUBRAMO_INCENDIO) == 0)
			result = obtenerSubgiro(idToCotizacion, numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo());
		else if (subRamoDTO.getIdTcSubRamo().compareTo(SubRamoDTO.SUBRAMO_RESPONSABILIDAD_CIVIL_GENERAL) == 0)
			result = obtenerSubGiroRC(idToCotizacion, numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo());
		else if (subRamoDTO.getIdTcSubRamo().compareTo(SubRamoDTO.SUBRAMO_ROTURA_MAQUINARIA) == 0) 
			result = obtenerSubTipoMaquinaria(idToCotizacion,numeroInciso,subRamoDTO.getRamoDTO().getIdTcRamo());
		else if (subRamoDTO.getIdTcSubRamo().compareTo(SubRamoDTO.SUBRAMO_MONTAJE_MAQUINA) == 0)
			result = obtenerSubTipoMontajeMaquina(idToCotizacion,numeroInciso,subRamoDTO.getRamoDTO().getIdTcRamo());
		else if (subRamoDTO.getIdTcSubRamo().compareTo(SubRamoDTO.SUBRAMO_EQUIPO_ELECTRONICO) == 0)
			result = obtenerSubtipoEquipoElectronico(idToCotizacion, numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo());
		else if (subRamoDTO.getIdTcSubRamo().compareTo(SubRamoDTO.SUBRAMO_EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA) == 0)
			result = obtenerSubtipoEquipoContratista(idToCotizacion, numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo());
		else if (subRamoDTO.getIdTcSubRamo().compareTo(SubRamoDTO.SUBRAMO_OBRA_CIVIL_EN_CONSTRUCCION) == 0)
			result = obtenerTipoObraCivil(idToCotizacion, numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo());
		return result;
	}
	
	/**
	 * Regresa el SubGiroDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubGiroDTO. El sub giro del inciso, null en caso de no encontrar el registro.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SubGiroDTO obtenerSubgiro(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo) throws SystemException{
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = DatoIncisoCotizacionDTO.instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		//Para obtener el subgiro, se utiliza el idDato = 20
		datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(20d));
		SubGiroDTO subGiro = null;
//		try {
			datoIncisoCotizacionDTO = findById(datoIncisoCotizacionDTO.getId());
			if (datoIncisoCotizacionDTO == null){
				LogDeMidasEJB3.log("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: "+"20", Level.WARNING, null);
//				throw new SystemException();
			}
			subGiro = subGiroFacade.findById(Utilerias.regresaBigDecimal(datoIncisoCotizacionDTO.getValor()));
			if (subGiro == null){
				LogDeMidasEJB3.log("No se encontr� subGiroDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso, Level.WARNING, null);
//				throw new SystemException("No se encontr� subGiroDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso);
			}
//		} catch (SystemException e) {	e.printStackTrace();}
//		catch(Exception e){e.printStackTrace();}
		return subGiro;
	}
	
	/**
	 * Regresa el SubGiroRCDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubGiroRCDTO. El SubGiroRCDTO del inciso, null en caso de no encontrar el registro.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SubGiroRCDTO obtenerSubGiroRC(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo) throws SystemException{
		//Para obtener el subgiroRC, se utiliza el idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = DatoIncisoCotizacionDTO.instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		SubGiroRCDTO subGiroRC = null;
//		try {
			datoIncisoCotizacionDTO = findById(datoIncisoCotizacionDTO.getId());
			if (datoIncisoCotizacionDTO == null){
				LogDeMidasEJB3.log("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: "+"20", Level.WARNING, null);
//				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
//						+"idTcRamo: "+idTcRamo+", idDato: "+"20");
			}
			subGiroRC = subGiroRCFacade.findById(Utilerias.regresaBigDecimal(datoIncisoCotizacionDTO.getValor()));
			if (subGiroRC == null){
				LogDeMidasEJB3.log("No se encontr� subGiroRC para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso, Level.WARNING, null);
//				throw new SystemException("No se encontr� subGiroRC para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso);
			}
//		} catch (SystemException e) {
//			e.printStackTrace();}
		return subGiroRC;
	}
	
	/**
	 * Regresa el SubTipoMaquinariaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubTipoMaquinariaDTO. El SubTipoMaquinariaDTO del inciso, null en caso de no encontrar el registro.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SubTipoMaquinariaDTO obtenerSubTipoMaquinaria(BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo) throws SystemException{
		//SUBTIPO_MAQUINARIA utiliza idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = DatoIncisoCotizacionDTO.instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(SubRamoDTO.SUBRAMO_ROTURA_MAQUINARIA);
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		datoIncisoCotizacionDTO.getId().setClaveDetalle((short)2);
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2090));
		datoIncisoCotizacionDTO.getId().setNumeroSubinciso(null);
		datoIncisoCotizacionDTO.getId().setIdToSeccion(null);
		SubTipoMaquinariaDTO result = null;
//		try {
			List<DatoIncisoCotizacionDTO> listaDatosInciso = listarPorIdFiltrado(datoIncisoCotizacionDTO.getId());
			if (listaDatosInciso != null && !listaDatosInciso.isEmpty()){
				int i=0;
				do{
					datoIncisoCotizacionDTO = listaDatosInciso.get(i);
					i++;
				}while(datoIncisoCotizacionDTO.getId().getIdToSeccion().compareTo(BigDecimal.ZERO) == 0 && datoIncisoCotizacionDTO.getId().getNumeroSubinciso().compareTo(BigDecimal.ZERO) == 0 && i<listaDatosInciso.size());
				if (datoIncisoCotizacionDTO != null){
					BigDecimal idSubTipoMaquinaria = Utilerias.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
					
					result = subTipoMaquinariaFacade.findById(idSubTipoMaquinaria);
					
					if (result == null){
						String error = "No se encontr� SubTipoMaquinariaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo
										+", idTcSubRamo: "+SubRamoDTO.SUBRAMO_ROTURA_MAQUINARIA+", valor (idTcSubTipoMaquinaria):"+datoIncisoCotizacionDTO.getValor();
						LogDeMidasEJB3.log(error, Level.SEVERE, null);
//						throw new SystemException(error);
					}
				}
				else{
					String error = "No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
					+"idTcRamo: "+idTcRamo+", idDato: 20, con idToSeccion != 0 y numeroSubInciso != 0";
					LogDeMidasEJB3.log(error, Level.SEVERE, null);
//					throw new SystemException(error);
				}
			}
			else{
				String error = "No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
				+"idTcRamo: "+idTcRamo+", idDato: 20";
				LogDeMidasEJB3.log(error, Level.SEVERE, null);
//				throw new SystemException(error);
			}
//		} catch (SystemException e) {
//			e.printStackTrace();
//			throw e;
//		}
//		catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	/**
	 * Regresa el SubTipoMaquinariaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubTipoMaquinariaDTO. El SubTipoMaquinariaDTO del inciso, null en caso de no encontrar el registro.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SubTipoMaquinariaDTO obtenerSubTipoMaquinaria(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo,
			BigDecimal numeroSubInciso,BigDecimal idToSeccion) throws SystemException{
		//SUBTIPO_MAQUINARIA utiliza idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = DatoIncisoCotizacionDTO.instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(12));
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		datoIncisoCotizacionDTO.getId().setClaveDetalle((short)2);
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2090));
		datoIncisoCotizacionDTO.getId().setNumeroSubinciso(numeroSubInciso);
		datoIncisoCotizacionDTO.getId().setIdToSeccion(idToSeccion);
		SubTipoMaquinariaDTO result = null;
//		try {
			datoIncisoCotizacionDTO = findById(datoIncisoCotizacionDTO.getId());
			if (datoIncisoCotizacionDTO != null){
				BigDecimal idSubTipoMaquinaria = Utilerias.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
				
				result = subTipoMaquinariaFacade.findById(idSubTipoMaquinaria);
				
				if (result == null){
					LogDeMidasEJB3.log("No se encontr� SubTipoMaquinariaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo, Level.WARNING, null);
//					throw new SystemException("No se encontr� SubTipoMaquinariaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
				}
			}
			else{
				LogDeMidasEJB3.log("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: 20", Level.WARNING, null);
//				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
//						+"idTcRamo: "+idTcRamo+", idDato: 20");
			}
//		} catch (SystemException e) {
//			e.printStackTrace();
//			throw e;
//		}
		return result;
	}
	
	/**
	 * Regresa el SubtipoMontajeMaquinaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoMontajeMaquinaDTO. El SubtipoMontajeMaquinaDTO del inciso, null en caso de no encontrar el registro.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SubtipoMontajeMaquinaDTO obtenerSubTipoMontajeMaquina (BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo) throws SystemException{
		//Para obtener el subTipoMontajeM�quina, se utiliza el idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = null;
		SubtipoMontajeMaquinaDTO result = null;
//		try {
			for(int i=0;i<4;i++){
				datoIncisoCotizacionDTO = DatoIncisoCotizacionDTO.instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
				datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
				datoIncisoCotizacionDTO.getId().setIdDato(idDato);
				if (i==0){
					datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2540));
					datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(17));
				} else if (i==1){
					datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2540));
				} else if (i==2){
					datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(17));
				}
				datoIncisoCotizacionDTO = findById(datoIncisoCotizacionDTO.getId());
				if (datoIncisoCotizacionDTO != null)
					break;
			}
			if (datoIncisoCotizacionDTO == null){
				LogDeMidasEJB3.log("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: 20, idToRiesgo: 2540, idTcSubRamo: 17", Level.WARNING, null);
//				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
//						+"idTcRamo: "+idTcRamo+", idDato: 20, idToRiesgo: 2540, idTcSubRamo: 17");
			}
			BigDecimal idtcsubtipomontajemaq = Utilerias.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
			
			result = subtipoMontajeMaquinaFacade.findById(idtcsubtipomontajemaq);
			
			if (result == null){
				LogDeMidasEJB3.log("No se encontr� SubtipoMontajeMaquinaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso, Level.WARNING, null);
//				throw new SystemException("No se encontr� SubtipoMontajeMaquinaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso);
			}
//		} catch (SystemException e) {
//			e.printStackTrace();}
//		catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	/**
	 * Regresa el SubtipoEquipoElectronicoDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoEquipoElectronicoDTO. El SubtipoEquipoElectronicoDTO del inciso, null en caso de no encontrar el registro.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SubtipoEquipoElectronicoDTO obtenerSubtipoEquipoElectronico (BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo) throws SystemException{
		//SUBTIPO_EQUIPO_ELECTRONICO utiliza el idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = DatoIncisoCotizacionDTO.instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(SubRamoDTO.SUBRAMO_EQUIPO_ELECTRONICO);
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		datoIncisoCotizacionDTO.getId().setClaveDetalle((short)2);
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2240));
		datoIncisoCotizacionDTO.getId().setNumeroSubinciso(null);
		datoIncisoCotizacionDTO.getId().setIdToSeccion(null);
		SubtipoEquipoElectronicoDTO result = null;
//		try {
			List<DatoIncisoCotizacionDTO> listaDatosInciso = listarPorIdFiltrado(datoIncisoCotizacionDTO.getId());
			if (listaDatosInciso != null && !listaDatosInciso.isEmpty()){
				int i=0;
				do{
					datoIncisoCotizacionDTO = listaDatosInciso.get(i);
					i++;
				}while(datoIncisoCotizacionDTO.getId().getIdToSeccion().compareTo(BigDecimal.ZERO) == 0 && datoIncisoCotizacionDTO.getId().getNumeroSubinciso().compareTo(BigDecimal.ZERO) == 0 && i<listaDatosInciso.size());
				if (datoIncisoCotizacionDTO != null){
					BigDecimal idSubTipo = Utilerias.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());

					result = subtipoEquipoElectronicoFacade.findById(idSubTipo);
					
					if (result == null){
						String error  = "No se encontr� SubTipoEquipoElectronicoDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo+
								"valorDatoInciso(idTcSubTipoEquipoElectronico): "+datoIncisoCotizacionDTO.getValor();
						LogDeMidasEJB3.log(error, Level.SEVERE, null);
//						throw new SystemException(error);
					}
				}
				else{
					LogDeMidasEJB3.log("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
							+"idTcRamo: "+idTcRamo+", idDato: 20", Level.WARNING, null);
//					throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
//							+"idTcRamo: "+idTcRamo+", idDato: 20");
				}
			}
			else{
				String error = "No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
					+"idTcRamo: "+idTcRamo+", idDato: 20, idToRiesgo: 2240, idTcSubRamo: "+SubRamoDTO.SUBRAMO_EQUIPO_ELECTRONICO;
				LogDeMidasEJB3.log(error, Level.SEVERE, null);
//				throw new SystemException(error);
			}
//		}
//		catch(SystemException e){
//			e.printStackTrace();
//			throw e;
//		}
		return result;
	}
	
	/**
	 * Regresa el SubtipoEquipoElectronicoDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoEquipoElectronicoDTO. El SubtipoEquipoElectronicoDTO del inciso, null en caso de no encontrar el registro.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SubtipoEquipoElectronicoDTO obtenerSubtipoEquipoElectronico (BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo,
			BigDecimal numeroSubInciso,BigDecimal idToSeccion) throws SystemException{
		//SUBTIPO_EQUIPO_ELECTRONICO utiliza el idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = DatoIncisoCotizacionDTO.instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setNumeroSubinciso(numeroSubInciso);
		datoIncisoCotizacionDTO.getId().setIdToSeccion(idToSeccion);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(13));
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		datoIncisoCotizacionDTO.getId().setClaveDetalle((short)2);
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2240));
		SubtipoEquipoElectronicoDTO result = null;
//		try {
			datoIncisoCotizacionDTO = findById(datoIncisoCotizacionDTO.getId());
			if (datoIncisoCotizacionDTO != null){
				BigDecimal idSubTipo = Utilerias.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
				
				result = subtipoEquipoElectronicoFacade.findById(idSubTipo);
				
				if (result == null){
					LogDeMidasEJB3.log("No se encontr� SubTipoEquipoElectronicoDTO para la cotizacion: "+idToCotizacion+
							", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo, Level.WARNING, null);
//					throw new SystemException("No se encontr� SubTipoEquipoElectronicoDTO para la cotizacion: "+idToCotizacion+
//							", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
				}
			}
			else{
				LogDeMidasEJB3.log("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: 20", Level.WARNING, null);
//				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
//						+"idTcRamo: "+idTcRamo+", idDato: 20");
			}
//		}
//		catch(SystemException e){
//			e.printStackTrace();
//			throw e;
//		}
		return result;
	}
	
	/**
	 * Regresa el SubtipoEquipoContratistaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoEquipoContratistaDTO. El SubtipoEquipoContratistaDTO del inciso, null en caso de no encontrar el registro.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SubtipoEquipoContratistaDTO obtenerSubtipoEquipoContratista (BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo) throws SystemException{
		// para SUBTIPO_EQUIPO_CONTRATISTA se utiliza idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = DatoIncisoCotizacionDTO.instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(14));
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2650));
		datoIncisoCotizacionDTO.getId().setClaveDetalle((short)2);
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		datoIncisoCotizacionDTO.getId().setNumeroSubinciso(null);
		datoIncisoCotizacionDTO.getId().setIdToSeccion(null);
		SubtipoEquipoContratistaDTO result = null;
//		try {
			List<DatoIncisoCotizacionDTO> listaDatosInciso = listarPorIdFiltrado(datoIncisoCotizacionDTO.getId());
			if (listaDatosInciso != null && !listaDatosInciso.isEmpty()){
				int i=0;
				do{
					datoIncisoCotizacionDTO = listaDatosInciso.get(i);
					i++;
				}while(datoIncisoCotizacionDTO.getId().getIdToSeccion() != null && datoIncisoCotizacionDTO.getId().getNumeroSubinciso() != null && i<listaDatosInciso.size());
				if (datoIncisoCotizacionDTO != null){
					BigDecimal idSubTipo = Utilerias.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
					
					result = subtipoEquipoContratistaFacade.findById(idSubTipo);
					if (result == null){
						LogDeMidasEJB3.log("No se encontr� SubTipoEquipoElectronicoDTO para la cotizacion: "+idToCotizacion+
								", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo, Level.WARNING, null);
//						throw new SystemException("No se encontr� SubTipoEquipoElectronicoDTO para la cotizacion: "+idToCotizacion+
//								", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
					}
				}
				else{
					LogDeMidasEJB3.log("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
							+"idTcRamo: "+idTcRamo+", idDato: 20", Level.WARNING, null);
//					throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
//							+"idTcRamo: "+idTcRamo+", idDato: 20");
				}
			}
			else{
				LogDeMidasEJB3.log("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: 20", Level.WARNING, null);
//				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
//						+"idTcRamo: "+idTcRamo+", idDato: 20");
			}
//		} catch (SystemException e) {
//			e.printStackTrace();}
//		catch(Exception e){e.printStackTrace();}
		if (result == null){
			LogDeMidasEJB3.log("No se encontr� SubtipoEquipoContratistaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo, Level.WARNING, null);
//			throw new SystemException("No se encontr� SubtipoEquipoContratistaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
		}
		return result;
	}
	
	/**
	 * Regresa el SubtipoEquipoContratistaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoEquipoContratistaDTO. El SubtipoEquipoContratistaDTO del inciso, null en caso de no encontrar el registro.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SubtipoEquipoContratistaDTO obtenerSubtipoEquipoContratista (BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo,
			BigDecimal numeroSubInciso,BigDecimal idToSeccion) throws SystemException{
		// para SUBTIPO_EQUIPO_CONTRATISTA se utiliza idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = DatoIncisoCotizacionDTO.instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(14));
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2650));
		datoIncisoCotizacionDTO.getId().setClaveDetalle((short)2);
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		datoIncisoCotizacionDTO.getId().setNumeroSubinciso(numeroSubInciso);
		datoIncisoCotizacionDTO.getId().setIdToSeccion(idToSeccion);
		SubtipoEquipoContratistaDTO result = null;
//		try {
			datoIncisoCotizacionDTO = findById(datoIncisoCotizacionDTO.getId());
			if (datoIncisoCotizacionDTO != null){
				BigDecimal idSubTipo = Utilerias.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
				
				result = subtipoEquipoContratistaFacade.findById(idSubTipo);
				
				if (result == null){
					LogDeMidasEJB3.log("No se encontr� SubTipoEquipoElectronicoDTO para la cotizacion: "+idToCotizacion+
							", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo, Level.WARNING, null);
//					throw new SystemException("No se encontr� SubTipoEquipoElectronicoDTO para la cotizacion: "+idToCotizacion+
//							", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
				}
			}
			else{
				LogDeMidasEJB3.log("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: 20", Level.WARNING, null);
//				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
//						+"idTcRamo: "+idTcRamo+", idDato: 20");
			}
//		} catch (SystemException e) {
//			e.printStackTrace();}
//		catch(Exception e){e.printStackTrace();}
		if (result == null){
			LogDeMidasEJB3.log("No se encontr� SubtipoEquipoContratistaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo, Level.WARNING, null);
//			throw new SystemException("No se encontr� SubtipoEquipoContratistaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
		}
		return result;
	}
	
	/**
	 * Regresa el TipoObraCivilDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return TipoObraCivilDTO. El TipoObraCivilDTO del inciso, null en caso de no encontrar el registro.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public TipoObraCivilDTO obtenerTipoObraCivil (BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo)throws SystemException{
		//TIPOOBRA CIVIL utiliza idDato = 10
		BigDecimal idDato = new BigDecimal(10d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = DatoIncisoCotizacionDTO.instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(16));
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2710));
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		TipoObraCivilDTO result = null;
		try {
			datoIncisoCotizacionDTO = findById(datoIncisoCotizacionDTO.getId());
			if (datoIncisoCotizacionDTO == null){
				LogDeMidasEJB3.log("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: 50", Level.WARNING, null);
//				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
//						+"idTcRamo: "+idTcRamo+", idDato: 50");
			}
			BigDecimal id = Utilerias.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
			
			result = tipoObraCivilFacade.findById(id);
			
			if (result == null){
				throw new SystemException("No se encontr� TipoObraCivilDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
			}
		} catch (SystemException e) {
			e.printStackTrace();}
		catch(Exception e){e.printStackTrace();}
		return result;
	}
}