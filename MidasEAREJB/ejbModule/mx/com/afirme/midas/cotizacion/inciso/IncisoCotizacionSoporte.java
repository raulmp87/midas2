package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.catalogos.descuento.DescuentoFacadeRemote;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.direccion.DireccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.aumento.AumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.descuento.DescuentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.RecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.AumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.DescuentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.RecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaId;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoFacadeRemote;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudFacadeRemote;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudFacadeRemote;

public class IncisoCotizacionSoporte {
	@EJB
	DatoIncisoCotizacionFacadeRemote datoIncisoCotizacionFacade;
	@EJB
	IncisoCotizacionFacadeRemote incisoCotizacionFacade;
	@EJB
	SeccionFacadeRemote seccionFacade;
	@EJB
	CoberturaFacadeRemote coberturaFacade;
	@EJB
	RiesgoFacadeRemote riesgoFacade;
	@EJB
	SeccionCotizacionFacadeRemote seccionCotizacionFacade;
	@EJB
	CoberturaSeccionFacadeRemote coberturaSeccionFacade;
	@EJB
	CoberturaCotizacionFacadeRemote coberturaCotizacionFacade;
	@EJB
	RiesgoCoberturaFacadeRemote riesgoCoberturaFacade;
	@EJB
	RiesgoCotizacionFacadeRemote riesgoCotizacionFacade;
	@EJB
	RecargoVarioFacadeRemote recargoVarioFacade;
	@EJB
	DescuentoFacadeRemote descuentoFacade;
	@EJB
	RecargoRiesgoCotizacionFacadeRemote recargoRiesgoCotizacionFacade;
	@EJB
	DescuentoRiesgoCotizacionFacadeRemote descuentoRiesgoCotizacionFacade;
	@EJB
	AumentoRiesgoCotizacionFacadeRemote aumentoRiesgoCotizacionFacade;
	@EJB
	DetallePrimaRiesgoCotizacionFacadeRemote detallePrimaRiesgoCotizacionFacade;
	@EJB
	DetallePrimaCoberturaCotizacionFacadeRemote detallePrimaCoberturaCotizacionFacade;
	@EJB
	SubIncisoCotizacionFacadeRemote subIncisoCotizacionFacade;
	@EJB
	AgrupacionCotFacadeRemote agrupacionCotizacionFacade;
	@EJB
	DireccionFacadeRemote direccionFacade;
	@EJB
	CotizacionFacadeRemote cotizacionFacade;
	@EJB
	SolicitudFacadeRemote solicitudFacade;
	@EJB
	DocumentoDigitalSolicitudFacadeRemote documentoDigitalSolicitudFacade;
	@EJB
	ControlArchivoFacadeRemote controlArchivoFacade;
	@EJB
	DocumentoDigitalCotizacionFacadeRemote documentoDigitalCotizacionFacade;
	@EJB
	ComisionCotizacionFacadeRemote comisionCotizacionFacade;
	@EJB
	TexAdicionalCotFacadeRemote texAdicionalCotizacionFacade;
	@EJB
	CoaseguroCoberturaFacadeRemote coaseguroCoberturaFacadeRemote;
	@EJB
	DeducibleCoberturaFacadeRemote deducibleCoberturaFacadeRemote;
	@EJB
	CoaseguroRiesgoCoberturaFacadeRemote coaseguroRiesgoCoberturaFacadeRemote;
	@EJB
	DeducibleRiesgoCoberturaFacadeRemote deducibleRiesgoCoberturaFacadeRemote;
	
	private static final short NIVEL_PRODUCTO = 1;
	private static final short NIVEL_TIPO_POLIZA = 2;		
	private static final short NIVEL_COBERTURA = 3;
	//Constantes para Tipo de ARD
	@SuppressWarnings("unused")
	private static final short TIPO_TECNICO = 2;
	
	
	private static final short SECCION_OPCIONAL=0;
	private static final short COBERTURA_OPCIONAL=0;
	private static final short RIESGO_OPCIONAL=2;
	private static final short CONTRATADO=1;
	private static final short SIN_CONTRATAR=0;
	
	protected BigDecimal separarIncisoCotizacion(CotizacionDTO cotizacionDTO, BigDecimal numeroInciso){
		cotizacionDTO = cotizacionFacade.findById(cotizacionDTO.getIdToCotizacion());
		SolicitudDTO solicitudNuevaDTO = solicitudFacade.findById(cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
		solicitudNuevaDTO.setIdToSolicitud(null);
		solicitudNuevaDTO = solicitudFacade.save(solicitudNuevaDTO);
		
		CotizacionDTO cotizacionNuevaDTO = cotizacionFacade.findById(cotizacionDTO.getIdToCotizacion());
		cotizacionNuevaDTO.setIdToCotizacion(null);
		
		cotizacionNuevaDTO.setSolicitudDTO(solicitudNuevaDTO);
		cotizacionNuevaDTO.setIncisoCotizacionDTOs(new ArrayList<IncisoCotizacionDTO>());
		cotizacionNuevaDTO.setDocumentoDigitalCotizacionDTOs(new ArrayList<DocumentoDigitalCotizacionDTO>());
		cotizacionNuevaDTO = cotizacionFacade.save(cotizacionNuevaDTO);
		//Copiar los documentos de la solicitud
		List<DocumentoDigitalSolicitudDTO> documentosSolicitud = documentoDigitalSolicitudFacade.findByProperty("solicitudDTO.idToSolicitud", cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
		for(DocumentoDigitalSolicitudDTO documento : documentosSolicitud) {
			documento.setIdToDocumentoDigitalSolicitud(null);
			documento.setSolicitudDTO(solicitudNuevaDTO);
			ControlArchivoDTO controlArchivoNuevo = controlArchivoFacade.findById(documento.getIdToControlArchivo());
			controlArchivoNuevo.setIdToControlArchivo(null);
			controlArchivoNuevo = controlArchivoFacade.save(controlArchivoNuevo);
			/*ControlArchivoDTO archivoTMP = controlArchivoFacade.findById(documento.getIdToControlArchivo());
			if (archivoTMP != null)
				copiarArchivo(archivoTMP, controlArchivoNuevo, uploadFolder);*/
			documento.setIdToControlArchivo(controlArchivoNuevo.getIdToControlArchivo());
		}
		solicitudNuevaDTO.setDocumentoDigitalSolicitudDTOs(documentosSolicitud);
		solicitudNuevaDTO = solicitudFacade.update(solicitudNuevaDTO);
		//Copiar los documentos digitales de la cotizacion
		List<DocumentoDigitalCotizacionDTO> documentosCotizacion = documentoDigitalCotizacionFacade.findByProperty("cotizacionDTO.idToCotizacion", cotizacionDTO.getIdToCotizacion());
		for (DocumentoDigitalCotizacionDTO documentoDigitalCotizacionDTO : documentosCotizacion) {
			if (documentoDigitalCotizacionDTO.getControlArchivo() == null)
				documentoDigitalCotizacionDTO.setControlArchivo(controlArchivoFacade.findById(documentoDigitalCotizacionDTO.getIdControlArchivo()));
		}
		for(DocumentoDigitalCotizacionDTO documento : documentosCotizacion) {
			documento.setIdDocumentoDigitalCotizacion(null);
			documento.setCotizacionDTO(cotizacionNuevaDTO);
			ControlArchivoDTO controlArchivoNuevo = controlArchivoFacade.findById(documento.getIdControlArchivo());
			controlArchivoNuevo.setIdToControlArchivo(null);
			controlArchivoNuevo = controlArchivoFacade.save(controlArchivoNuevo);
			/*ControlArchivoDTO archivoTMP = controlArchivoFacade.findById(documento.getIdControlArchivo());
			if(archivoTMP != null){
				copiarArchivo(archivoTMP, controlArchivoNuevo, uploadFolder);
			}*/
			documento.setIdControlArchivo(controlArchivoNuevo.getIdToControlArchivo());
		}
		cotizacionNuevaDTO.setDocumentoDigitalCotizacionDTOs(documentosCotizacion);
		cotizacionFacade.update(cotizacionNuevaDTO);
		//Copiar las comisiones de la cotizacion
		List<ComisionCotizacionDTO> comisiones = comisionCotizacionFacade.findByProperty("cotizacionDTO.idToCotizacion", cotizacionDTO.getIdToCotizacion());
		for(ComisionCotizacionDTO comision : comisiones){
			comision.getId().setIdToCotizacion(cotizacionNuevaDTO.getIdToCotizacion());
			comision.setCotizacionDTO(cotizacionNuevaDTO);
			if(comision.getClaveAutComision().intValue() != 0)
				comision.setClaveAutComision((short)1);
			comisionCotizacionFacade.save(comision);
		}
		//Copiar el inciso cuyo ID se recibi�
		IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
		incisoCotizacionId.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
		incisoCotizacionId.setNumeroInciso(numeroInciso);
		IncisoCotizacionDTO incisoACopiar = incisoCotizacionFacade.findById(incisoCotizacionId);
		if (incisoACopiar != null){
			incisoACopiar.getId().setIdToCotizacion(cotizacionNuevaDTO.getIdToCotizacion());
			incisoACopiar.setCotizacionDTO(cotizacionNuevaDTO);
			if (incisoACopiar.getClaveEstatusInspeccion().intValue() != 0)
				incisoACopiar.setClaveEstatusInspeccion((short)1);
			if (incisoACopiar.getClaveAutInspeccion().intValue() != 0)
				incisoACopiar.setClaveAutInspeccion((short)1);
			IncisoCotizacionDTO incisoOriginal = incisoCotizacionFacade.findById(incisoCotizacionId);
			//Se copian los datos del inciso original al inciso nuevo
			//FIXME acomodar la llamada al m�todo copiarIncisoCotizacion
//			this.copiarIncisoCotizacion(incisoACopiar, incisoACopiar, cotizacionNuevaDTO);
			//Se borra el inciso Original de la cotizacion
			borrarIncisoCotizacion(incisoOriginal);
		}
		//Copiar los textos adicionales de la cotizacion
		List<TexAdicionalCotDTO> texAdicionalesCot = texAdicionalCotizacionFacade.findByProperty("cotizacion.idToCotizacion", cotizacionDTO.getIdToCotizacion());
		for (TexAdicionalCotDTO texAdicional : texAdicionalesCot){
			texAdicional.setIdToTexAdicionalCot(null);
			texAdicional.setCotizacion(cotizacionNuevaDTO);
			if(texAdicional.getClaveAutorizacion().intValue() != 0)
				texAdicional.setClaveAutorizacion((short)1);
			texAdicionalCotizacionFacade.save(texAdicional);
		}
		
		//Copiar los registros de AgrupacionCotizacion
		try{
			List<AgrupacionCotDTO> listaAgrupacion = agrupacionCotizacionFacade.findByProperty("id.idToCotizacion", cotizacionDTO.getIdToCotizacion());
			if (listaAgrupacion != null){
				for(AgrupacionCotDTO agrupacion : listaAgrupacion){
					agrupacion.getId().setIdToCotizacion(cotizacionNuevaDTO.getIdToCotizacion());
					agrupacionCotizacionFacade.save(agrupacion);
				}
			}
		}catch(Exception e){e.printStackTrace();}
		
		
		return cotizacionNuevaDTO.getIdToCotizacion();
	}
	
	protected void borrarIncisoCotizacion(IncisoCotizacionDTO incisoCotizacionDTO){
		incisoCotizacionDTO = incisoCotizacionFacade.findById(incisoCotizacionDTO.getId());

		List<SeccionCotizacionDTO> secciones = seccionCotizacionFacade.listarPorCotizacionNumeroInciso(incisoCotizacionDTO.getId().getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
		incisoCotizacionDTO.setSeccionCotizacionList(secciones);
		for(SeccionCotizacionDTO seccion : secciones) {
			List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionFacade.listarPorSeccionCotizacionId(seccion.getId(),Boolean.FALSE);
			seccion.setCoberturaCotizacionLista(coberturas);
			for(CoberturaCotizacionDTO cobertura : coberturas) {
				RiesgoCotizacionId id = new RiesgoCotizacionId();
				id.setIdToCotizacion(cobertura.getId().getIdToCotizacion());
				id.setNumeroInciso(cobertura.getId().getNumeroInciso());
				id.setIdToSeccion(cobertura.getId().getIdToSeccion());
				id.setIdToCobertura(cobertura.getId().getIdToCobertura());
				List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionFacade.listarPorIdFiltrado(id);
				cobertura.setRiesgoCotizacionLista(riesgos);
				for(RiesgoCotizacionDTO riesgo : riesgos) {
					List<AumentoRiesgoCotizacionDTO> listaAumentosRiesgoCotizacion = aumentoRiesgoCotizacionFacade.findByRiesgoCotizacion(riesgo.getId());
					riesgo.setAumentoRiesgoCotizacionDTOs(listaAumentosRiesgoCotizacion);
					List<RecargoRiesgoCotizacionDTO> listaRecargosRiesgoCotizacion = recargoRiesgoCotizacionFacade.findByRiesgoCotizacion(riesgo.getId());
					riesgo.setRecargoRiesgoCotizacionDTOs(listaRecargosRiesgoCotizacion);
					List<DescuentoRiesgoCotizacionDTO> listaDescuentosRiesgoCotizacion = descuentoRiesgoCotizacionFacade.findByRiesgoCotizacion(riesgo.getId());
					riesgo.setDescuentoRiesgoCotizacionDTOs(listaDescuentosRiesgoCotizacion);
					List<DetallePrimaRiesgoCotizacionDTO> listaDetallePrimaRiesgo = detallePrimaRiesgoCotizacionFacade.findByRiesgoCotizacion(riesgo.getId());
					riesgo.setDetallePrimaRiesgoCotizacionList(listaDetallePrimaRiesgo);
				}
			}
			//Borrar los subincisos
			SubIncisoCotizacionDTOId subIncisoCotizacionId = new SubIncisoCotizacionDTOId();
			subIncisoCotizacionId.setIdToCotizacion(seccion.getId().getIdToCotizacion());
			subIncisoCotizacionId.setNumeroInciso(seccion.getId().getNumeroInciso());
			subIncisoCotizacionId.setIdToSeccion(seccion.getId().getIdToSeccion());
			SubIncisoCotizacionDTO subIncisoCotizacionDTO = new SubIncisoCotizacionDTO();
			subIncisoCotizacionDTO.setId(subIncisoCotizacionId);
			List<SubIncisoCotizacionDTO> subIncisoCotizacionDTOLista = subIncisoCotizacionFacade.listarFiltrado(subIncisoCotizacionDTO);
			seccion.setSubIncisoCotizacionLista(subIncisoCotizacionDTOLista);
		}
		//Borrar los DatoIncisoCotizacion
		datoIncisoCotizacionFacade.deleteAll(incisoCotizacionDTO.getId().getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
		//Borra las agrupaciones de la cotizacion
		//agrupacionCotizacionFacade.borrarAgrupaciones(incisoCotizacionDTO.getId().getIdToCotizacion());
		//Borrar el inciso
		incisoCotizacionFacade.delete(incisoCotizacionDTO);

//		DireccionDTO direccionDTO = new DireccionDTO();
//		direccionDTO.setIdToDireccion(incisoCotizacionDTO.getIdToDireccionInciso());
//		direccionFacade.delete(direccionDTO);
	}

	protected IncisoCotizacionDTO copiarIncisoCotizacion(IncisoCotizacionDTO incisoNuevo, IncisoCotizacionDTO incisoOriginal, CotizacionDTO cotizacionDTO,
			DireccionDTO direccionNueva,List<DatoIncisoCotizacionDTO> listaDatosInciso){
		incisoNuevo.setDireccionDTO(direccionNueva);
//		incisoNuevo.setDireccion(direccionNueva);
		//incisoNuevo = incisoCotizacionFacade.save(incisoNuevo);
		incisoNuevo.setCotizacionDTO(cotizacionDTO);
		//Copiar los registros de DatoIncisoCotizacion
		for(DatoIncisoCotizacionDTO datoInciso : listaDatosInciso){
			datoInciso.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
			datoInciso.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
			datoIncisoCotizacionFacade.save(datoInciso);
		}
		List<SeccionCotizacionDTO> listaSeccionCotizacion = incisoOriginal.getSeccionCotizacionList();
		List<SeccionCotizacionDTO> listaSeccionesNuevasCotizacion = new ArrayList<SeccionCotizacionDTO>();
		for(SeccionCotizacionDTO seccionCot : listaSeccionCotizacion){
			//Copiar los registros SeccionCotizacion
			SeccionCotizacionDTO seccionCotizacionNueva = new SeccionCotizacionDTO();
			seccionCotizacionNueva.setId(new SeccionCotizacionDTOId());
			seccionCotizacionNueva.getId().setIdToCotizacion(incisoOriginal.getId().getIdToCotizacion());
			seccionCotizacionNueva.getId().setIdToSeccion(seccionCot.getId().getIdToSeccion());
			seccionCotizacionNueva.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
				//Los datos de la SeccionCotizacion obtenida del incisoOriginal se pasan al nuevo seccionCotizacion
			seccionCotizacionNueva.setValorPrimaNeta(seccionCot.getValorPrimaNeta());
			seccionCotizacionNueva.setValorPrimaNetaOriginal(seccionCot.getValorPrimaNetaOriginal());
			seccionCotizacionNueva.setValorSumaAsegurada(seccionCot.getValorSumaAsegurada());
			seccionCotizacionNueva.setClaveObligatoriedad(seccionCot.getClaveObligatoriedad());
			seccionCotizacionNueva.setClaveContrato(seccionCot.getClaveContrato());
			seccionCotizacionNueva.setIncisoCotizacionDTO(incisoNuevo);
			seccionCotizacionNueva.setSeccionDTO(seccionCot.getSeccionDTO());
				//Se guarda el nuevo registro de SeccionCotizacion correspondiente al nuevo inciso
			listaSeccionesNuevasCotizacion.add(seccionCotizacionNueva);
			//seccionCotizacionFacade.save(seccionCotizacionNueva);
			List<SubIncisoCotizacionDTO> listaSubIncisoCot = seccionCot.getSubIncisoCotizacionLista();
				//Copiar los subincisos
			List<SubIncisoCotizacionDTO> listaSubIncisosNuevos = new ArrayList<SubIncisoCotizacionDTO>();
			for (SubIncisoCotizacionDTO subIncisoCot : listaSubIncisoCot) {
				SubIncisoCotizacionDTO subIncisoTMP = new SubIncisoCotizacionDTO();
				subIncisoTMP.setId(new SubIncisoCotizacionDTOId());
				subIncisoTMP.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
				subIncisoTMP.getId().setIdToSeccion(subIncisoCot.getId().getIdToSeccion());
				subIncisoTMP.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
				subIncisoTMP.getId().setNumeroSubInciso(subIncisoCot.getId().getNumeroSubInciso());
				subIncisoTMP.setClaveAutReaseguro(subIncisoCot.getClaveAutReaseguro());
				subIncisoTMP.setCodigoUsuarioAutReaseguro(subIncisoCot.getCodigoUsuarioAutReaseguro());
				subIncisoTMP.setDescripcionSubInciso(subIncisoCot.getDescripcionSubInciso());
				subIncisoTMP.setSeccionCotizacionDTO(seccionCotizacionNueva);
				subIncisoTMP.setValorSumaAsegurada(subIncisoCot.getValorSumaAsegurada());

				listaSubIncisosNuevos.add(subIncisoTMP);
				//subIncisoCotizacionFacade.save(subIncisoTMP);
			}
			seccionCotizacionNueva.setSubIncisoCotizacionLista(listaSubIncisosNuevos);

			List<CoberturaCotizacionDTO> listaCoberturaCot = seccionCot.getCoberturaCotizacionLista();
			List<CoberturaCotizacionDTO> coberturasNuevasCotizacion = new ArrayList<CoberturaCotizacionDTO>();
			for(CoberturaCotizacionDTO coberturaCot : listaCoberturaCot) {
					//Se recuperan los registros CoberturaCotizacion del inciso Original
				CoberturaCotizacionDTO coberturaCotizacionNueva= new CoberturaCotizacionDTO();
				coberturaCotizacionNueva.setId(new CoberturaCotizacionId());
				coberturaCotizacionNueva.getId().setIdToCobertura(coberturaCot.getId().getIdToCobertura());
				coberturaCotizacionNueva.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
				coberturaCotizacionNueva.getId().setIdToSeccion(coberturaCot.getId().getIdToSeccion());
				coberturaCotizacionNueva.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
				coberturaCotizacionNueva.setSeccionCotizacionDTO(seccionCotizacionNueva);
				coberturaCotizacionNueva.setCoberturaSeccionDTO(coberturaCot.getCoberturaSeccionDTO());
						//Al nuevo registro se le establecen los valores del registro perteneciente al inciso original
				coberturaCotizacionNueva.setIdTcSubramo(coberturaCot.getIdTcSubramo());
				coberturaCotizacionNueva.setValorPrimaNeta(coberturaCot.getValorPrimaNeta());
				coberturaCotizacionNueva.setValorPrimaNetaOriginal(coberturaCot.getValorPrimaNetaOriginal());
				coberturaCotizacionNueva.setValorSumaAsegurada(coberturaCot.getValorSumaAsegurada());
				coberturaCotizacionNueva.setValorCoaseguro(coberturaCot.getValorCoaseguro());
				coberturaCotizacionNueva.setValorDeducible(coberturaCot.getValorDeducible());
				coberturaCotizacionNueva.setClaveObligatoriedad(coberturaCot.getClaveObligatoriedad());
				coberturaCotizacionNueva.setValorCuota(coberturaCot.getValorCuota());
				coberturaCotizacionNueva.setValorCuotaOriginal(coberturaCot.getValorCuotaOriginal());
				coberturaCotizacionNueva.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
				coberturaCotizacionNueva.setClaveContrato(coberturaCot.getClaveContrato());
				if (coberturaCot.getClaveAutCoaseguro().intValue() != 0)
					coberturaCotizacionNueva.setClaveAutCoaseguro((short)1);
				else
					coberturaCotizacionNueva.setClaveAutCoaseguro((short)0);
				coberturaCotizacionNueva.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
				if (coberturaCot.getClaveAutCoaseguro().intValue() != 0)
					coberturaCotizacionNueva.setClaveAutDeducible((short)1);
				else
					coberturaCotizacionNueva.setClaveAutDeducible((short)0);
				coberturaCotizacionNueva.setNumeroAgrupacion(coberturaCot.getNumeroAgrupacion());
				//Campos agregados el 26/01/2010
				coberturaCotizacionNueva.setClaveTipoDeducible(coberturaCot.getClaveTipoDeducible());
				coberturaCotizacionNueva.setClaveTipoLimiteDeducible(coberturaCot.getClaveTipoLimiteDeducible());
				coberturaCotizacionNueva.setValorMinimoLimiteDeducible(coberturaCot.getValorMinimoLimiteDeducible());
				coberturaCotizacionNueva.setValorMaximoLimiteDeducible(coberturaCot.getValorMaximoLimiteDeducible());
				coberturasNuevasCotizacion.add(coberturaCotizacionNueva);
				//coberturaCotizacionFacade.save(coberturaCotizacionNueva);
				
				List<DetallePrimaCoberturaCotizacionDTO> listaDetallePrimaCoberturaCotizacion = coberturaCot.getListaDetallePrimacoberturaCotizacion();
				List<DetallePrimaCoberturaCotizacionDTO> detallesPrimaCoberturaNuevaCotizacion = new ArrayList<DetallePrimaCoberturaCotizacionDTO>();
				for(DetallePrimaCoberturaCotizacionDTO detallePrimaCoberturaCot : listaDetallePrimaCoberturaCotizacion){
					DetallePrimaCoberturaCotizacionDTO detallePrimaCoberturaCotTMP = new DetallePrimaCoberturaCotizacionDTO();
					detallePrimaCoberturaCotTMP.setId(new DetallePrimaCoberturaCotizacionId());
					detallePrimaCoberturaCotTMP.getId().setIdToCobertura(coberturaCotizacionNueva.getId().getIdToCobertura());
					detallePrimaCoberturaCotTMP.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
					detallePrimaCoberturaCotTMP.getId().setIdToSeccion(coberturaCotizacionNueva.getId().getIdToSeccion());
					detallePrimaCoberturaCotTMP.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
					detallePrimaCoberturaCotTMP.getId().setNumeroSubInciso(detallePrimaCoberturaCot.getId().getNumeroSubInciso());
					detallePrimaCoberturaCotTMP.setCoberturaCotizacionDTO(coberturaCotizacionNueva);
					detallePrimaCoberturaCotTMP.setValorCuota(detallePrimaCoberturaCot.getValorCuota());
					detallePrimaCoberturaCotTMP.setValorCuotaARDT(detallePrimaCoberturaCot.getValorCuotaARDT());
					detallePrimaCoberturaCotTMP.setValorCuotaARDV(detallePrimaCoberturaCot.getValorCuotaARDV());
					detallePrimaCoberturaCotTMP.setValorCuotaB(detallePrimaCoberturaCot.getValorCuotaB());
					detallePrimaCoberturaCotTMP.setValorPrimaNeta(detallePrimaCoberturaCot.getValorPrimaNeta());
					detallePrimaCoberturaCotTMP.setValorPrimaNetaARDT(detallePrimaCoberturaCot.getValorPrimaNetaARDT());
					detallePrimaCoberturaCotTMP.setValorPrimaNetaB(detallePrimaCoberturaCot.getValorPrimaNetaB());

					detallesPrimaCoberturaNuevaCotizacion.add(detallePrimaCoberturaCotTMP);
					//detallePrimaCoberturaCotizacionFacade.save(detallePrimaCoberturaCotTMP);
				}
				coberturaCotizacionNueva.setListaDetallePrimacoberturaCotizacion(detallesPrimaCoberturaNuevaCotizacion);
				
				//Copiar los registros de DetallePrimaCoberturaCotizacion Por cada cobertura se obtienen los riesgos
				List<RiesgoCotizacionDTO> listaRiesgoCotizacion = coberturaCot.getRiesgoCotizacionLista();
				List<RiesgoCotizacionDTO> riesgosNuevosCotizacion = new ArrayList<RiesgoCotizacionDTO>();
				for(RiesgoCotizacionDTO riesgoCot : listaRiesgoCotizacion){
					RiesgoCotizacionDTO riesgoCotizacionNuevo = new RiesgoCotizacionDTO();
					riesgoCotizacionNuevo.setId(new RiesgoCotizacionId());
					riesgoCotizacionNuevo.getId().setIdToCobertura(riesgoCot.getId().getIdToCobertura());
					riesgoCotizacionNuevo.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
					riesgoCotizacionNuevo.getId().setIdToRiesgo(riesgoCot.getId().getIdToRiesgo());
					riesgoCotizacionNuevo.getId().setIdToSeccion(riesgoCot.getId().getIdToSeccion());
					riesgoCotizacionNuevo.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
					
					//Al nuevo registro se le establecen los valores del registro original
					riesgoCotizacionNuevo.setRiesgoCoberturaDTO(riesgoCot.getRiesgoCoberturaDTO());
					riesgoCotizacionNuevo.setCoberturaCotizacionDTO(coberturaCotizacionNueva);
					riesgoCotizacionNuevo.setIdTcSubramo(riesgoCot.getIdTcSubramo());
					riesgoCotizacionNuevo.setValorSumaAsegurada(riesgoCot.getValorSumaAsegurada());
					riesgoCotizacionNuevo.setValorCoaseguro(riesgoCot.getValorCoaseguro());
					riesgoCotizacionNuevo.setValorDeducible(riesgoCot.getValorDeducible());
					if (riesgoCot.getClaveAutCoaseguro().intValue() != 0)
						riesgoCotizacionNuevo.setClaveAutCoaseguro((short)1);
					else
						riesgoCotizacionNuevo.setClaveAutCoaseguro((short)0);
					if (riesgoCot.getClaveAutDeducible().intValue() != 0)
						riesgoCotizacionNuevo.setClaveAutDeducible((short)1);
					else
						riesgoCotizacionNuevo.setClaveAutDeducible((short)0);
					riesgoCotizacionNuevo.setClaveObligatoriedad(riesgoCot.getClaveObligatoriedad());
					riesgoCotizacionNuevo.setClaveContrato(riesgoCot.getClaveContrato());
					// TODO riesgoCotizacionNuevo.setClaveContrato((short)0);
					riesgoCotizacionNuevo.setValorAumento(riesgoCot.getValorAumento());
					riesgoCotizacionNuevo.setValorDescuento(riesgoCot.getValorDescuento());
					riesgoCotizacionNuevo.setValorRecargo(riesgoCot.getValorRecargo());
					riesgoCotizacionNuevo.setPorcentajeCoaseguro(riesgoCot.getPorcentajeCoaseguro());
					riesgoCotizacionNuevo.setPorcentajeDeducible(riesgoCot.getPorcentajeDeducible());
					riesgoCotizacionNuevo.setValorCuotaB(riesgoCot.getValorCuotaB());
					riesgoCotizacionNuevo.setValorCuotaARDT(riesgoCot.getValorCuotaARDT());
					riesgoCotizacionNuevo.setValorPrimaNeta(riesgoCot.getValorPrimaNeta());
					riesgoCotizacionNuevo.setValorPrimaNetaARDT(riesgoCot.getValorPrimaNetaARDT());
					riesgoCotizacionNuevo.setValorPrimaNetaB(riesgoCot.getValorPrimaNetaB());
					//Campos agregados el 26/01/2010
					riesgoCotizacionNuevo.setClaveTipoDeducible(riesgoCot.getClaveTipoDeducible());
					riesgoCotizacionNuevo.setClaveTipoLimiteDeducible(riesgoCot.getClaveTipoLimiteDeducible());
					riesgoCotizacionNuevo.setValorMinimoLimiteDeducible(riesgoCot.getValorMinimoLimiteDeducible());
					riesgoCotizacionNuevo.setValorMaximoLimiteDeducible(riesgoCot.getValorMaximoLimiteDeducible());
					riesgosNuevosCotizacion.add(riesgoCotizacionNuevo);
					//riesgoCotizacionFacade.save(riesgoCotizacionNuevo);
								//Recuperar los aumentos del riesgoCotizacionOriginal
					List<AumentoRiesgoCotizacionDTO> aumentosRiesgoCotizacionOriginal = riesgoCot.getAumentoRiesgoCotizacionDTOs();
					List<AumentoRiesgoCotizacionDTO> aumentosRiesgoNuevoCotizacion = new ArrayList<AumentoRiesgoCotizacionDTO>();
								//A cada aumento del riesgoCotizacion Original, se le establecen los id�s del nuevo inciso
					for(AumentoRiesgoCotizacionDTO aumento : aumentosRiesgoCotizacionOriginal){
						AumentoRiesgoCotizacionDTO aumentoTMP = new AumentoRiesgoCotizacionDTO();
						aumentoTMP.setAumentoVarioDTO(aumento.getAumentoVarioDTO());
						aumentoTMP.setClaveAutorizacion(aumento.getClaveAutorizacion());
						aumentoTMP.setClaveComercialTecnico(aumento.getClaveComercialTecnico());
						aumentoTMP.setClaveContrato(aumento.getClaveContrato());
						aumentoTMP.setClaveNivel(aumento.getClaveNivel());
						aumentoTMP.setClaveObligatoriedad(aumento.getClaveObligatoriedad());
						aumentoTMP.setCodigoUsuarioAutorizacion(aumento.getCodigoUsuarioAutorizacion());
						aumentoTMP.setValorAumento(aumento.getValorAumento());
						aumentoTMP.setId(new AumentoRiesgoCotizacionId());
						aumentoTMP.getId().setIdToAumentoVario(aumento.getId().getIdToAumentoVario());
						aumentoTMP.getId().setIdToCobertura(aumento.getId().getIdToCobertura());
						aumentoTMP.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
						aumentoTMP.getId().setIdToRiesgo(aumento.getId().getIdToRiesgo());
						aumentoTMP.getId().setIdToSeccion(aumento.getId().getIdToSeccion());
						aumentoTMP.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
								//TODO aumento.setClaveContrato((short)0);
							if (aumento.getClaveAutorizacion() != 0)
								aumentoTMP.setClaveAutorizacion((short)1);
						aumentosRiesgoNuevoCotizacion.add(aumentoTMP);
							//aumentoRiesgoCotizacionFacade.save(aumentoTMP);
					}
					riesgoCotizacionNuevo.setAumentoRiesgoCotizacionDTOs(aumentosRiesgoNuevoCotizacion);
								//Recuperar los descuentos del riesgoCotizacionOriginal
					List<DescuentoRiesgoCotizacionDTO> descuentosRiesgoCotizacionOriginal = riesgoCot.getDescuentoRiesgoCotizacionDTOs();
					List<DescuentoRiesgoCotizacionDTO> descuentosRiesgoNuevoCotizacion = new ArrayList<DescuentoRiesgoCotizacionDTO>();
					for(DescuentoRiesgoCotizacionDTO descuento : descuentosRiesgoCotizacionOriginal){
						DescuentoRiesgoCotizacionDTO descuentoTMP = new DescuentoRiesgoCotizacionDTO();
						descuentoTMP.setDescuentoDTO(descuento.getDescuentoDTO());
						descuentoTMP.setClaveAutorizacion(descuento.getClaveAutorizacion());
						descuentoTMP.setClaveComercialTecnico(descuento.getClaveComercialTecnico());
						descuentoTMP.setClaveContrato(descuento.getClaveContrato());
						descuentoTMP.setClaveNivel(descuento.getClaveNivel());
						descuentoTMP.setClaveObligatoriedad(descuento.getClaveObligatoriedad());
						descuentoTMP.setCodigoUsuarioAutorizacion(descuento.getCodigoUsuarioAutorizacion());
						descuentoTMP.setClaveTipo(descuento.getClaveTipo());
						descuentoTMP.setRiesgoCotizacionDTO(riesgoCotizacionNuevo);
						descuentoTMP.setValorDescuento(descuento.getValorDescuento());
						descuentoTMP.setId(new DescuentoRiesgoCotizacionId());
						descuentoTMP.getId().setIdToDescuentoVario(descuento.getId().getIdToDescuentoVario());
						descuentoTMP.getId().setIdToCobertura(descuento.getId().getIdToCobertura());
						descuentoTMP.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
						descuentoTMP.getId().setIdToRiesgo(descuento.getId().getIdToRiesgo());
						descuentoTMP.getId().setIdToSeccion(descuento.getId().getIdToSeccion());
						descuentoTMP.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
								//TODO descuento.setClaveContrato((short)0);
							if (descuento.getClaveAutorizacion() != 0)
								descuentoTMP.setClaveAutorizacion((short)1);
						descuentosRiesgoNuevoCotizacion.add(descuentoTMP);
							//descuentoRiesgoCotizacionFacade.save(descuentoTMP);
					}
					riesgoCotizacionNuevo.setDescuentoRiesgoCotizacionDTOs(descuentosRiesgoNuevoCotizacion);
								//Recuperar los recargos del riesgoCotizacionOriginal
					List<RecargoRiesgoCotizacionDTO> recargosRiesgoCotizacionOriginal = riesgoCot.getRecargoRiesgoCotizacionDTOs();

					List<RecargoRiesgoCotizacionDTO> recargosRiesgoNuevoCotizacion = new ArrayList<RecargoRiesgoCotizacionDTO>();
					for(RecargoRiesgoCotizacionDTO recargo: recargosRiesgoCotizacionOriginal){
						RecargoRiesgoCotizacionDTO recargoTMP = new RecargoRiesgoCotizacionDTO();
						recargoTMP.setClaveAutorizacion(recargo.getClaveAutorizacion());
						recargoTMP.setClaveComercialTecnico(recargo.getClaveComercialTecnico());
						recargoTMP.setClaveContrato(recargo.getClaveContrato());
						recargoTMP.setClaveNivel(recargo.getClaveNivel());
						recargoTMP.setClaveObligatoriedad(recargo.getClaveObligatoriedad());
						recargoTMP.setClaveTipo(recargo.getClaveTipo());
						recargoTMP.setCodigoUsuarioAutorizacion(recargo.getCodigoUsuarioAutorizacion());
						recargoTMP.setRecargoVarioDTO(recargo.getRecargoVarioDTO());
						recargoTMP.setRiesgoCotizacionDTO(riesgoCotizacionNuevo);
						recargoTMP.setValorRecargo(recargo.getValorRecargo());
						recargoTMP.setId(new RecargoRiesgoCotizacionId());
						recargoTMP.getId().setIdToRecargoVario(recargo.getId().getIdToRecargoVario());
						recargoTMP.getId().setIdToCobertura(recargo.getId().getIdToCobertura());
						recargoTMP.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
						recargoTMP.getId().setIdToRiesgo(recargo.getId().getIdToRiesgo());
						recargoTMP.getId().setIdToSeccion(recargo.getId().getIdToSeccion());
						recargoTMP.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
						//recargo.setClaveContrato((short)0);
						if (recargo.getClaveAutorizacion() != 0)
							recargo.setClaveAutorizacion((short)1);
						recargosRiesgoNuevoCotizacion.add(recargoTMP);
						//recargoRiesgoCotizacionFacade.save(recargoTMP);
					}
					riesgoCotizacionNuevo.setRecargoRiesgoCotizacionDTOs(recargosRiesgoNuevoCotizacion);
								//Copiar los registros de DetallePrimaRiesgoCotizacion
					List<DetallePrimaRiesgoCotizacionDTO> listaDetallePrimaRiesgo = riesgoCot.getDetallePrimaRiesgoCotizacionList();
					List<DetallePrimaRiesgoCotizacionDTO> detallesPrimaRiesgoNuevoCotizacion = new ArrayList<DetallePrimaRiesgoCotizacionDTO>();
					for(DetallePrimaRiesgoCotizacionDTO detalle : listaDetallePrimaRiesgo){
						DetallePrimaRiesgoCotizacionDTO detallePrimaRiesgoTMP = new DetallePrimaRiesgoCotizacionDTO();
						detallePrimaRiesgoTMP.setId(new DetallePrimaRiesgoCotizacionId());
						detallePrimaRiesgoTMP.getId().setIdToCobertura(detalle.getId().getIdToCobertura());
						detallePrimaRiesgoTMP.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
						detallePrimaRiesgoTMP.getId().setIdToRiesgo(detalle.getId().getIdToRiesgo());
						detallePrimaRiesgoTMP.getId().setIdToSeccion(detalle.getId().getIdToSeccion());
						detallePrimaRiesgoTMP.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
						detallePrimaRiesgoTMP.getId().setNumeroSubInciso(detalle.getId().getNumeroSubInciso());
						detallePrimaRiesgoTMP.setRiesgoCotizacionDTO(riesgoCot);
						detallePrimaRiesgoTMP.setValorCuota(detalle.getValorCuota());
						detallePrimaRiesgoTMP.setValorCuotaARDT(detalle.getValorCuotaARDT());
						detallePrimaRiesgoTMP.setValorCuotaARDV(detalle.getValorCuotaARDV());
						detallePrimaRiesgoTMP.setValorCuotaB(detalle.getValorCuotaB());
						detallePrimaRiesgoTMP.setValorPrimaNeta(detalle.getValorPrimaNeta());
						detallePrimaRiesgoTMP.setValorPrimaNetaARDT(detalle.getValorPrimaNetaARDT());
						detallePrimaRiesgoTMP.setValorPrimaNetaB(detalle.getValorPrimaNetaB());
						detallePrimaRiesgoTMP.setValorSumaAsegurada(detalle.getValorSumaAsegurada());
						detallesPrimaRiesgoNuevoCotizacion.add(detallePrimaRiesgoTMP);
						//detallePrimaRiesgoCotizacionFacade.save(detallePrimaRiesgoTMP);
					}
					riesgoCotizacionNuevo.setDetallePrimaRiesgoCotizacionList(detallesPrimaRiesgoNuevoCotizacion);
				}//Fin iterar riesgosCotizacion
				coberturaCotizacionNueva.setRiesgoCotizacionLista(riesgosNuevosCotizacion);
			}//Fin iterar CoberturasCotuzacion
			seccionCotizacionNueva.setCoberturaCotizacionLista(coberturasNuevasCotizacion);
		}//Fin iterar SeccionCotizacion
		incisoNuevo.setSeccionCotizacionList(listaSeccionesNuevasCotizacion);
//		incisoNuevo.setSeccionCotizacionList(listaSeccionCotizacion);
//		incisoCotizacionFacade.update(incisoNuevo);
		incisoNuevo = incisoCotizacionFacade.save(incisoNuevo);
		return incisoNuevo;
	}

	protected void agregarIncisoCotizacion(CotizacionDTO cotizacionDTO, BigDecimal idToDireccion,BigDecimal numeroInciso, 
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso, String[] datos,List<RecargoVarioDTO> recargosEspeciales,
				List<DescuentoDTO> descuentosEspeciales) {
//		Se recibir� la cotizaci�n completa, sin tener que consultarla dentro de la transacci�n
//		BigDecimal idCotizacion = cotizacionDTO.getIdToCotizacion();
//		cotizacionDTO = cotizacionFacade.findById(idCotizacion);

		IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
		incisoCotizacionId.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
//		BigDecimal numeroInciso = incisoCotizacionFacade.maxIncisos(cotizacionDTO.getIdToCotizacion()).add(BigDecimal.ONE);
		incisoCotizacionId.setNumeroInciso(numeroInciso);
		IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
		DireccionDTO direccionDTO = direccionFacade.findById(idToDireccion);
		incisoCotizacionDTO.setDireccionDTO(direccionDTO);
		incisoCotizacionDTO.setId(incisoCotizacionId);
		incisoCotizacionDTO.setClaveEstatusInspeccion((short)0);
		incisoCotizacionDTO.setClaveTipoOrigenInspeccion((short)0);
		incisoCotizacionDTO.setClaveMensajeInspeccion((short)0);
		incisoCotizacionDTO.setClaveAutInspeccion((short)0);
		incisoCotizacionDTO.setValorPrimaNeta(0d);
		if(datos != null && datosInciso != null){
			incisoCotizacionDTO.setDescripcionGiroAsegurado(datos[datos.length - 1]);
			datos[datos.length - 1] = null;
			incisoCotizacionDTO = registrarInciso(incisoCotizacionDTO,cotizacionDTO,recargosEspeciales,descuentosEspeciales);
			for(int i = 0; i < datos.length; i++) {
				if(datos[i] != null) {
					ConfiguracionDatoIncisoCotizacionDTO configuracion = datosInciso.get(i);
					DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
					id.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
					id.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
					id.setIdDato(configuracion.getId().getIdDato());
					id.setClaveDetalle(configuracion.getId().getClaveDetalle());
					id.setIdTcRamo(configuracion.getId().getIdTcRamo());
					id.setIdTcSubramo(configuracion.getId().getIdTcSubramo());
					id.setIdToCobertura(BigDecimal.ZERO);
					id.setIdToRiesgo(configuracion.getId().getIdToRiesgo());
					id.setIdToSeccion(BigDecimal.ZERO);
					id.setNumeroSubinciso(BigDecimal.ZERO);
					DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = new DatoIncisoCotizacionDTO();
					datoIncisoCotizacionDTO.setId(id);
					datoIncisoCotizacionDTO.setValor(datos[i]);
					datoIncisoCotizacionFacade.save(datoIncisoCotizacionDTO);
				}
			}
		}
	}
	
	protected IncisoCotizacionDTO actualizarIncisoCotizacion(IncisoCotizacionDTO incisoCotizacionDTO, BigDecimal idToDireccion, 
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso, String[] datos) {

		
//		DireccionDTO direccionDTO = direccionFacade.findById(idToDireccion);
//		incisoCotizacionDTO.setDireccionDTO(direccionDTO);
		
		if(datos != null && datosInciso != null){
			incisoCotizacionDTO.setDescripcionGiroAsegurado(datos[datos.length - 1]);
			datos[datos.length - 1] = null;
			
			//Eliminar los datos del inciso para despu�s registrarlos nuevamente.
			datoIncisoCotizacionFacade.deleteAll(incisoCotizacionDTO.getId().getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
			
			for(int i = 0; i < datos.length; i++) {
				if(datos[i] != null) {
					ConfiguracionDatoIncisoCotizacionDTO configuracion = datosInciso.get(i);
					DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
					id.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
					id.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
					id.setIdDato(configuracion.getId().getIdDato());
					id.setClaveDetalle(configuracion.getId().getClaveDetalle());
					id.setIdTcRamo(configuracion.getId().getIdTcRamo());
					id.setIdTcSubramo(configuracion.getId().getIdTcSubramo());
					id.setIdToCobertura(BigDecimal.ZERO);
					id.setIdToRiesgo(configuracion.getId().getIdToRiesgo());
					id.setIdToSeccion(BigDecimal.ZERO);
					id.setNumeroSubinciso(BigDecimal.ZERO);
					DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = new DatoIncisoCotizacionDTO();
					datoIncisoCotizacionDTO.setId(id);
					datoIncisoCotizacionDTO.setValor(datos[i]);
					datoIncisoCotizacionFacade.save(datoIncisoCotizacionDTO);
				}
			}
		}
		
		incisoCotizacionDTO = incisoCotizacionFacade.update(incisoCotizacionDTO);
		
		return incisoCotizacionDTO;
	}

	private IncisoCotizacionDTO registrarInciso(IncisoCotizacionDTO incisoCotizacionDTO, CotizacionDTO cotizacionDTO,
			List<RecargoVarioDTO> recargosEspeciales,List<DescuentoDTO> descuentosEspeciales){
		incisoCotizacionDTO.setCotizacionDTO(cotizacionDTO);
		//incisoCotizacionDTO = incisoCotizacionFacade.save(incisoCotizacionDTO);
		//List<SeccionDTO> secciones = seccionFacade.listarVigentes(incisoCotizacionDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza());
		List<SeccionDTO> secciones = cotizacionDTO.getTipoPolizaDTO().getSecciones();
		List<SeccionCotizacionDTO> seccionesCotizacion = new ArrayList<SeccionCotizacionDTO>();
		for(SeccionDTO seccionDTO : secciones) {
			SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
			seccionCotizacionDTO.setId(new SeccionCotizacionDTOId());
			seccionCotizacionDTO.getId().setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
			seccionCotizacionDTO.getId().setIdToSeccion(seccionDTO.getIdToSeccion());
			seccionCotizacionDTO.getId().setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
			seccionCotizacionDTO.setIncisoCotizacionDTO(incisoCotizacionDTO);
			seccionCotizacionDTO.setSeccionDTO(seccionDTO);
			seccionCotizacionDTO.setValorPrimaNeta(0D);
			seccionCotizacionDTO.setValorSumaAsegurada(0D);
			seccionCotizacionDTO.setClaveObligatoriedad(new Short(seccionDTO.getClaveObligatoriedad()));
			if (seccionCotizacionDTO.getClaveObligatoriedad().equals(SECCION_OPCIONAL)){
				seccionCotizacionDTO.setClaveContrato(SIN_CONTRATAR);
			}
			else{
				seccionCotizacionDTO.setClaveContrato(CONTRATADO);
			}
			seccionesCotizacion.add(seccionCotizacionDTO);
			//seccionCotizacionFacade.save(seccionCotizacionDTO);
			
//			List<CoberturaSeccionDTO> listaCoberturaSeccion = coberturaSeccionFacade.getVigentesPorSeccion(seccionDTO.getIdToSeccion());
			List<CoberturaSeccionDTO> listaCoberturaSeccion = seccionDTO.getCoberturas();
//				coberturaSeccionFacade.getVigentesPorSeccion(seccionDTO.getIdToSeccion());
			List<CoberturaCotizacionDTO> coberturasCotizacion = new ArrayList<CoberturaCotizacionDTO>();
			for(CoberturaSeccionDTO coberturaSeccionDTO : listaCoberturaSeccion) {
				CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
				coberturaCotizacionDTO.setId(new CoberturaCotizacionId());
				coberturaCotizacionDTO.getId().setIdToCobertura(coberturaSeccionDTO.getId().getIdtocobertura());
				coberturaCotizacionDTO.getId().setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
				coberturaCotizacionDTO.getId().setIdToSeccion(coberturaSeccionDTO.getId().getIdtoseccion());
				coberturaCotizacionDTO.getId().setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
				coberturaCotizacionDTO.setSeccionCotizacionDTO(seccionCotizacionDTO);
				coberturaCotizacionDTO.setCoberturaSeccionDTO(coberturaSeccionDTO);
				coberturaCotizacionDTO.setIdTcSubramo(coberturaSeccionDTO.getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo());
				coberturaCotizacionDTO.setValorPrimaNeta(0D);
				coberturaCotizacionDTO.setValorSumaAsegurada(0D);
				coberturaCotizacionDTO.setValorCoaseguro(0D);
				coberturaCotizacionDTO.setValorDeducible(0D);
				coberturaCotizacionDTO.setClaveObligatoriedad(new Short(coberturaSeccionDTO.getClaveObligatoriedad().toBigInteger().toString()));
				//Campos agregados el 26/01/2010
				coberturaCotizacionDTO.setClaveTipoDeducible(Short.parseShort(coberturaSeccionDTO.getCoberturaDTO().getClaveTipoDeducible()));
				coberturaCotizacionDTO.setClaveTipoLimiteDeducible(Short.parseShort(coberturaSeccionDTO.getCoberturaDTO().getClaveTipoLimiteDeducible()));
				coberturaCotizacionDTO.setValorMinimoLimiteDeducible(coberturaSeccionDTO.getCoberturaDTO().getValorMinimoLimiteDeducible());
				coberturaCotizacionDTO.setValorMaximoLimiteDeducible(coberturaSeccionDTO.getCoberturaDTO().getValorMaximoLimiteDeducible());
				//La clave de contrataci�n se debe establecer en base a la clave obligatoriedad de la cobertura, previamente establecida a coberturaCotizacion
				if (coberturaCotizacionDTO.getClaveObligatoriedad().equals(COBERTURA_OPCIONAL)){
				    coberturaCotizacionDTO.setClaveContrato(SIN_CONTRATAR);
				}
				else{
				    coberturaCotizacionDTO.setClaveContrato(CONTRATADO);
				}
				coberturaCotizacionDTO.setValorCuota(0D);
				coberturaCotizacionDTO.setPorcentajeCoaseguro(0D);
				List<CoaseguroCoberturaDTO> coaseguros = coaseguroCoberturaFacadeRemote.listarCoaseguros(coberturaCotizacionDTO.getId().getIdToCobertura());
				for(CoaseguroCoberturaDTO coaseguro : coaseguros) {
					if(coaseguro.getClaveDefault().shortValue() == 1) {
						coberturaCotizacionDTO.setPorcentajeCoaseguro(coaseguro.getValor());
						break;
					}
				}

				coberturaCotizacionDTO.setClaveAutCoaseguro((short)0);
				coberturaCotizacionDTO.setPorcentajeDeducible(0D);
				List<DeducibleCoberturaDTO> deducibles = deducibleCoberturaFacadeRemote.listarDeducibles(coberturaCotizacionDTO.getId().getIdToCobertura());
				for(DeducibleCoberturaDTO deducible : deducibles) {
					if(deducible.getClaveDefault().shortValue() == 1) {
						coberturaCotizacionDTO.setPorcentajeDeducible(deducible.getValor());
						break;
					}
				}

				coberturaCotizacionDTO.setClaveAutDeducible((short)0);
				coberturaCotizacionDTO.setNumeroAgrupacion((short)0);
				coberturasCotizacion.add(coberturaCotizacionDTO);
				//coberturaCotizacionFacade.save(coberturaCotizacionDTO);

//				List<RiesgoCoberturaDTO> riesgosCoberturas = riesgoCoberturaFacade.listarVigentesPorCoberturaSeccion(coberturaSeccionDTO.getId().getIdtocobertura(), coberturaSeccionDTO.getId().getIdtoseccion());
				List<RiesgoCoberturaDTO> riesgosCoberturas = coberturaSeccionDTO.getRiesgos();
//					riesgoCoberturaFacade.listarVigentesPorCoberturaSeccion(coberturaSeccionDTO.getId().getIdtocobertura(), coberturaSeccionDTO.getId().getIdtoseccion());
				List<RiesgoCotizacionDTO> riesgosCotizacion = new ArrayList<RiesgoCotizacionDTO>();
				for(RiesgoCoberturaDTO riesgo : riesgosCoberturas) {
					RiesgoCotizacionDTO riesgoCotizacionDTO = new RiesgoCotizacionDTO();
					riesgoCotizacionDTO.setId(new RiesgoCotizacionId());
					riesgoCotizacionDTO.getId().setIdToCobertura(riesgo.getId().getIdtocobertura());
					riesgoCotizacionDTO.getId().setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
					riesgoCotizacionDTO.getId().setIdToRiesgo(riesgo.getId().getIdtoriesgo());
					riesgoCotizacionDTO.getId().setIdToSeccion(riesgo.getId().getIdtoseccion());
					riesgoCotizacionDTO.getId().setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
					riesgoCotizacionDTO.setRiesgoCoberturaDTO(riesgo);
					riesgoCotizacionDTO.setCoberturaCotizacionDTO(coberturaCotizacionDTO);
					//riesgoCotizacionDTO.setIdTcSubramo(UtileriasWeb.regresaBigDecimal("0"));
					riesgoCotizacionDTO.setIdTcSubramo(coberturaSeccionDTO.getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo());
					riesgoCotizacionDTO.setValorPrimaNeta(0D);
					riesgoCotizacionDTO.setValorSumaAsegurada(0D);
					riesgoCotizacionDTO.setValorCoaseguro(0D);
					riesgoCotizacionDTO.setValorDeducible(0D);
					riesgoCotizacionDTO.setClaveAutCoaseguro((short)0);
					riesgoCotizacionDTO.setClaveAutDeducible((short)0);
					riesgoCotizacionDTO.setClaveObligatoriedad(riesgo.getClaveObligatoriedad());
					if (riesgo.getClaveObligatoriedad().equals(RIESGO_OPCIONAL)){
					    riesgoCotizacionDTO.setClaveContrato(SIN_CONTRATAR);
					}
					else{
					    riesgoCotizacionDTO.setClaveContrato(CONTRATADO);
					}
					riesgoCotizacionDTO.setValorAumento(0D);
					riesgoCotizacionDTO.setValorDescuento(0D);
					riesgoCotizacionDTO.setValorRecargo(0D);

					riesgoCotizacionDTO.setPorcentajeCoaseguro(0D);
					CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO = new CoaseguroRiesgoCoberturaDTO();
					CoaseguroRiesgoCoberturaId idCoaseguro = new CoaseguroRiesgoCoberturaId();
					idCoaseguro.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
					idCoaseguro.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
					idCoaseguro.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
					coaseguroRiesgoCoberturaDTO.setId(idCoaseguro);
					List<CoaseguroRiesgoCoberturaDTO> coasegurosRiesgo = coaseguroRiesgoCoberturaFacadeRemote.listarFiltrado(coaseguroRiesgoCoberturaDTO);
					for(CoaseguroRiesgoCoberturaDTO coaseguro : coasegurosRiesgo) {
						if(coaseguro.getClaveDefault().shortValue() == 1) {
							riesgoCotizacionDTO.setPorcentajeCoaseguro(coaseguro.getValor());
							break;
						}
					}
					
					riesgoCotizacionDTO.setPorcentajeDeducible(0D);
					DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO = new DeducibleRiesgoCoberturaDTO();
					DeducibleRiesgoCoberturaId idDeducible = new DeducibleRiesgoCoberturaId();
					idDeducible.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
					idDeducible.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
					idDeducible.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
					deducibleRiesgoCoberturaDTO.setId(idDeducible);
					List<DeducibleRiesgoCoberturaDTO> deduciblesRiesgo = deducibleRiesgoCoberturaFacadeRemote.listarFiltrado(deducibleRiesgoCoberturaDTO);
					for(DeducibleRiesgoCoberturaDTO deducible : deduciblesRiesgo){
						if(deducible.getClaveDefault().shortValue() == 1) {
							riesgoCotizacionDTO.setPorcentajeDeducible(deducible.getValor());
							break;
						}
					}

					riesgoCotizacionDTO.setValorCuotaARDT(0D);
					riesgoCotizacionDTO.setValorCuotaB(0d);
					riesgoCotizacionDTO.setValorPrimaNetaARDT(0d);
					riesgoCotizacionDTO.setValorPrimaNetaB(0d);
					//Campos agregados el 26/01/2010
					riesgoCotizacionDTO.setClaveTipoDeducible(riesgo.getClaveTipoDeducible());
					riesgoCotizacionDTO.setClaveTipoLimiteDeducible(riesgo.getClaveTipoLimiteDeducible());
					riesgoCotizacionDTO.setValorMinimoLimiteDeducible(riesgo.getValorMinimoLimiteDeducible());
					riesgoCotizacionDTO.setValorMaximoLimiteDeducible(riesgo.getValorMaximoLimiteDeducible());
					riesgosCotizacion.add(riesgoCotizacionDTO);
					//riesgoCotizacionFacade.save(riesgoCotizacionDTO);
				}
				coberturaCotizacionDTO.setRiesgoCotizacionLista(riesgosCotizacion);
			}
			seccionCotizacionDTO.setCoberturaCotizacionLista(coberturasCotizacion);
		}
		incisoCotizacionDTO.setSeccionCotizacionList(seccionesCotizacion);
		incisoCotizacionDTO = incisoCotizacionFacade.save(incisoCotizacionDTO);
		riesgoCotizacionFacade.insertarARD(incisoCotizacionDTO.getId().getIdToCotizacion(), 
				incisoCotizacionDTO.getId().getNumeroInciso(), 
				cotizacionDTO.getSolicitudDTO().getProductoDTO().getIdToProducto(), 
				cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
		
		return incisoCotizacionDTO;
	}
	
	@SuppressWarnings("unused")
	private List<DescuentoRiesgoCotizacionDTO> getDescuentosRiesgoCotizacion(RiesgoCotizacionDTO riesgoCotizacionDTO, 
			List<DescuentoVarioCoberturaDTO> descuentosCobertura, List<DescuentoVarioTipoPolizaDTO> descuentosTipoPoliza,
			List<DescuentoVarioProductoDTO> descuentosProducto) {
		
		List<DescuentoRiesgoCotizacionDTO> descuentos = new ArrayList<DescuentoRiesgoCotizacionDTO>();
		for(DescuentoVarioProductoDTO descuento : descuentosProducto) {
			DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO = new DescuentoRiesgoCotizacionDTO();
			descuentoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			descuentoRiesgoCotizacionDTO.setDescuentoDTO(descuento.getDescuentoDTO());
			descuentoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			descuentoRiesgoCotizacionDTO.setValorDescuento(descuento.getValor());
			descuentoRiesgoCotizacionDTO.setClaveObligatoriedad(Short.parseShort(descuento.getClaveObligatoriedad()));
			if(descuento.getClaveObligatoriedad().equals("1") || descuento.getClaveObligatoriedad().equals("3")) {
				descuentoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				descuentoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			descuentoRiesgoCotizacionDTO.setClaveComercialTecnico(Short.parseShort(descuento.getClaveComercialTecnico()));
			descuentoRiesgoCotizacionDTO.setClaveNivel(NIVEL_PRODUCTO);
			descuentoRiesgoCotizacionDTO.setId(new DescuentoRiesgoCotizacionId());
			descuentoRiesgoCotizacionDTO.getId().setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			descuentoRiesgoCotizacionDTO.getId().setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			descuentoRiesgoCotizacionDTO.getId().setIdToDescuentoVario(descuento.getId().getIdToDescuentoVario());
			descuentoRiesgoCotizacionDTO.getId().setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			descuentoRiesgoCotizacionDTO.getId().setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			descuentoRiesgoCotizacionDTO.getId().setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
			descuentos.add(descuentoRiesgoCotizacionDTO);
		}
		for(DescuentoVarioTipoPolizaDTO descuento : descuentosTipoPoliza) {
			DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO = new DescuentoRiesgoCotizacionDTO();
			descuentoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			descuentoRiesgoCotizacionDTO.setDescuentoDTO(descuento.getDescuentoDTO());
			descuentoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			descuentoRiesgoCotizacionDTO.setValorDescuento(descuento.getValor());
			descuentoRiesgoCotizacionDTO.setClaveObligatoriedad(descuento.getClaveobligatoriedad());
			if(descuento.getClaveobligatoriedad().shortValue() == 1 || descuento.getClaveobligatoriedad().shortValue() == 3) {
				descuentoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				descuentoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			descuentoRiesgoCotizacionDTO.setClaveComercialTecnico(descuento.getClavecomercialtecnico());
			descuentoRiesgoCotizacionDTO.setClaveNivel(NIVEL_TIPO_POLIZA);
			descuentoRiesgoCotizacionDTO.setId(new DescuentoRiesgoCotizacionId());
			descuentoRiesgoCotizacionDTO.getId().setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			descuentoRiesgoCotizacionDTO.getId().setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			descuentoRiesgoCotizacionDTO.getId().setIdToDescuentoVario(descuento.getId().getIdtodescuentovario());
			descuentoRiesgoCotizacionDTO.getId().setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			descuentoRiesgoCotizacionDTO.getId().setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			descuentoRiesgoCotizacionDTO.getId().setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
			descuentos.add(descuentoRiesgoCotizacionDTO);
		}
		for(DescuentoVarioCoberturaDTO descuento : descuentosCobertura) {
			DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO = new DescuentoRiesgoCotizacionDTO();
			descuentoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			descuentoRiesgoCotizacionDTO.setDescuentoDTO(descuento.getDescuentoDTO());
			descuentoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			descuentoRiesgoCotizacionDTO.setValorDescuento(descuento.getValor());
			descuentoRiesgoCotizacionDTO.setClaveObligatoriedad(descuento.getClaveobligatoriedad());
			if(descuento.getClaveobligatoriedad().shortValue() == 1 || descuento.getClaveobligatoriedad().shortValue() == 3) {
				descuentoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				descuentoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			descuentoRiesgoCotizacionDTO.setClaveComercialTecnico(descuento.getClavecomercialtecnico());
			descuentoRiesgoCotizacionDTO.setClaveNivel(NIVEL_COBERTURA);
			descuentoRiesgoCotizacionDTO.setId(new DescuentoRiesgoCotizacionId());
			descuentoRiesgoCotizacionDTO.getId().setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			descuentoRiesgoCotizacionDTO.getId().setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			descuentoRiesgoCotizacionDTO.getId().setIdToDescuentoVario(descuento.getId().getIdtodescuentovario());
			descuentoRiesgoCotizacionDTO.getId().setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			descuentoRiesgoCotizacionDTO.getId().setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			descuentoRiesgoCotizacionDTO.getId().setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
			descuentos.add(descuentoRiesgoCotizacionDTO);
		}
		return descuentos;
	}
	
	@SuppressWarnings("unused")
	private List<AumentoRiesgoCotizacionDTO> getAumentosRiesgoCotizacion(RiesgoCotizacionDTO riesgoCotizacionDTO,
			List<AumentoVarioCoberturaDTO> aumentosCobertura, List<AumentoVarioTipoPolizaDTO> aumentosTipoPoliza,
			List<AumentoVarioProductoDTO> aumentosProducto) {
		List<AumentoRiesgoCotizacionDTO> aumentos = new ArrayList<AumentoRiesgoCotizacionDTO>();
		for(AumentoVarioProductoDTO aumento : aumentosProducto) {
			AumentoRiesgoCotizacionDTO aumentoRiesgoCotizacionDTO = new AumentoRiesgoCotizacionDTO();
			aumentoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			aumentoRiesgoCotizacionDTO.setAumentoVarioDTO(aumento.getAumentoVarioDTO());
			aumentoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			aumentoRiesgoCotizacionDTO.setValorAumento(aumento.getValor());
			aumentoRiesgoCotizacionDTO.setClaveObligatoriedad(aumento.getClaveobligatoriedad());
			if(aumento.getClaveobligatoriedad().shortValue() == 1 || aumento.getClaveobligatoriedad().shortValue() == 3) {
				aumentoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				aumentoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			aumentoRiesgoCotizacionDTO.setClaveComercialTecnico(aumento.getClavecomercialtecnico());
			aumentoRiesgoCotizacionDTO.setClaveNivel(NIVEL_PRODUCTO);
			//aumentoRiesgoCotizacionDTO.setClaveTipo((short)1);
			aumentoRiesgoCotizacionDTO.setId(new AumentoRiesgoCotizacionId());
			aumentoRiesgoCotizacionDTO.getId().setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			aumentoRiesgoCotizacionDTO.getId().setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			aumentoRiesgoCotizacionDTO.getId().setIdToAumentoVario(aumento.getId().getIdtoaumentovario());
			aumentoRiesgoCotizacionDTO.getId().setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			aumentoRiesgoCotizacionDTO.getId().setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			aumentoRiesgoCotizacionDTO.getId().setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
			aumentos.add(aumentoRiesgoCotizacionDTO);
		}
		for(AumentoVarioTipoPolizaDTO aumento : aumentosTipoPoliza) {
			AumentoRiesgoCotizacionDTO aumentoRiesgoCotizacionDTO = new AumentoRiesgoCotizacionDTO();
			aumentoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			aumentoRiesgoCotizacionDTO.setAumentoVarioDTO(aumento.getAumentoVarioDTO());
			aumentoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			aumentoRiesgoCotizacionDTO.setValorAumento(aumento.getValor());
			aumentoRiesgoCotizacionDTO.setClaveObligatoriedad(aumento.getClaveobligatoriedad());
			if(aumento.getClaveobligatoriedad().shortValue() == 1 || aumento.getClaveobligatoriedad().shortValue() == 3) {
				aumentoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				aumentoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			aumentoRiesgoCotizacionDTO.setClaveComercialTecnico(aumento.getClavecomercialtecnico());
			aumentoRiesgoCotizacionDTO.setClaveNivel(NIVEL_TIPO_POLIZA);
			aumentoRiesgoCotizacionDTO.setId(new AumentoRiesgoCotizacionId());
			aumentoRiesgoCotizacionDTO.getId().setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			aumentoRiesgoCotizacionDTO.getId().setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			aumentoRiesgoCotizacionDTO.getId().setIdToAumentoVario(aumento.getId().getIdtoaumentovario());
			aumentoRiesgoCotizacionDTO.getId().setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			aumentoRiesgoCotizacionDTO.getId().setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			aumentoRiesgoCotizacionDTO.getId().setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
			aumentos.add(aumentoRiesgoCotizacionDTO);
		}

		for(AumentoVarioCoberturaDTO aumento : aumentosCobertura) {
			AumentoRiesgoCotizacionDTO aumentoRiesgoCotizacionDTO = new AumentoRiesgoCotizacionDTO();
			aumentoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			aumentoRiesgoCotizacionDTO.setAumentoVarioDTO(aumento.getAumentoVarioDTO());
			aumentoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			aumentoRiesgoCotizacionDTO.setValorAumento(aumento.getValor());
			aumentoRiesgoCotizacionDTO.setClaveObligatoriedad(aumento.getClaveobligatoriedad());
			if(aumento.getClaveobligatoriedad().shortValue() == 1 || aumento.getClaveobligatoriedad().shortValue() == 3) {
				aumentoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				aumentoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			aumentoRiesgoCotizacionDTO.setClaveComercialTecnico(aumento.getClavecomercialtecnico());
			aumentoRiesgoCotizacionDTO.setClaveNivel(NIVEL_COBERTURA);
			aumentoRiesgoCotizacionDTO.setId(new AumentoRiesgoCotizacionId());
			aumentoRiesgoCotizacionDTO.getId().setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			aumentoRiesgoCotizacionDTO.getId().setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			aumentoRiesgoCotizacionDTO.getId().setIdToAumentoVario(aumento.getId().getIdtoaumentovario());
			aumentoRiesgoCotizacionDTO.getId().setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			aumentoRiesgoCotizacionDTO.getId().setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			aumentoRiesgoCotizacionDTO.getId().setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
			aumentos.add(aumentoRiesgoCotizacionDTO);
		}
		return aumentos;
	}

	@SuppressWarnings("unused")
	private List<RecargoRiesgoCotizacionDTO> getRecargosRiesgoCotizacion(RiesgoCotizacionDTO riesgoCotizacionDTO,
			List<RecargoVarioCoberturaDTO> recargosCobertura, List<RecargoVarioTipoPolizaDTO> recargosTipoPoliza,
			List<RecargoVarioProductoDTO> recargosProducto) {

		List<RecargoRiesgoCotizacionDTO> recargos = new ArrayList<RecargoRiesgoCotizacionDTO>();
		for(RecargoVarioProductoDTO recargo : recargosProducto) {
			RecargoRiesgoCotizacionId id = new RecargoRiesgoCotizacionId();
			id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToRecargoVario(recargo.getId().getIdToRecargoVario());
			id.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			id.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
			RecargoRiesgoCotizacionDTO recargoRiesgoCotizacionDTO = new RecargoRiesgoCotizacionDTO();
			recargoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			recargoRiesgoCotizacionDTO.setRecargoVarioDTO(recargo.getRecargoVarioDTO());
			recargoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			recargoRiesgoCotizacionDTO.setValorRecargo(recargo.getValor());
			recargoRiesgoCotizacionDTO.setClaveObligatoriedad(Short.parseShort(recargo.getClaveObligatoriedad()));
			if(recargo.getClaveObligatoriedad().equals("1") || recargo.getClaveObligatoriedad().equals("3")) {
				recargoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				recargoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			recargoRiesgoCotizacionDTO.setClaveComercialTecnico(Short.parseShort(recargo.getClaveComercialTecnico()));
			recargoRiesgoCotizacionDTO.setClaveNivel(NIVEL_PRODUCTO);
			recargoRiesgoCotizacionDTO.setId(id);
			recargos.add(recargoRiesgoCotizacionDTO);
		}

		for(RecargoVarioTipoPolizaDTO recargo : recargosTipoPoliza) {
			RecargoRiesgoCotizacionId id = new RecargoRiesgoCotizacionId();
			id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToRecargoVario(recargo.getId().getIdtorecargovario());
			id.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			id.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());

			RecargoRiesgoCotizacionDTO recargoRiesgoCotizacionDTO = new RecargoRiesgoCotizacionDTO();
			recargoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			recargoRiesgoCotizacionDTO.setRecargoVarioDTO(recargo.getRecargoVarioDTO());
			recargoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			recargoRiesgoCotizacionDTO.setValorRecargo(recargo.getValor());
			recargoRiesgoCotizacionDTO.setClaveObligatoriedad(recargo.getClaveobligatoriedad());
			if(recargo.getClaveobligatoriedad().shortValue() == 1 || recargo.getClaveobligatoriedad().shortValue() == 3) {
				recargoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				recargoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			recargoRiesgoCotizacionDTO.setClaveComercialTecnico(recargo.getClavecomercialtecnico());
			recargoRiesgoCotizacionDTO.setClaveNivel(NIVEL_TIPO_POLIZA);
			recargoRiesgoCotizacionDTO.setId(id);
			recargos.add(recargoRiesgoCotizacionDTO);
		}

		for(RecargoVarioCoberturaDTO recargo : recargosCobertura) {
			RecargoRiesgoCotizacionId id = new RecargoRiesgoCotizacionId();
			id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToRecargoVario(recargo.getId().getIdtorecargovario());
			id.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			id.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());

			RecargoRiesgoCotizacionDTO recargoRiesgoCotizacionDTO = new RecargoRiesgoCotizacionDTO();
			recargoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			recargoRiesgoCotizacionDTO.setRecargoVarioDTO(recargo.getRecargoVarioDTO());
			recargoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			recargoRiesgoCotizacionDTO.setValorRecargo(recargo.getValor());
			recargoRiesgoCotizacionDTO.setClaveObligatoriedad(recargo.getClaveobligatoriedad());
			if(recargo.getClaveobligatoriedad().shortValue() == 1 || recargo.getClaveobligatoriedad().shortValue() == 3) {
				recargoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				recargoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			recargoRiesgoCotizacionDTO.setClaveComercialTecnico(recargo.getClavecomercialtecnico());
			recargoRiesgoCotizacionDTO.setClaveNivel(NIVEL_COBERTURA);
			recargoRiesgoCotizacionDTO.setId(id);
			recargos.add(recargoRiesgoCotizacionDTO);
		}
		return recargos;
	}

	public String copiarIncisos(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal numeroIncisoBase, BigDecimal idToDireccion) {
		String queryString = "insert into MIDAS.toincisocot (idtocotizacion, numeroinciso, idtodireccioninciso, valorprimaneta, claveestatusinspeccion,"
				+ " clavetipoorigeninspeccion, clavemensajeinspeccion, codigousuarioestinspeccion, claveautinspeccion, fechaestatusinspeccion,"
				+ " fechasolautinspeccion, fechaautinspeccion) "
				+ " select "
				+ idToCotizacion
				+ " , " + numeroInciso + ", " + idToDireccion + ", incisocot.valorprimaneta, incisocot.claveestatusinspeccion, incisocot.clavetipoorigeninspeccion, incisocot.clavemensajeinspeccion,"
				+ " incisocot.codigousuarioestinspeccion, incisocot.claveautinspeccion, incisocot.fechaestatusinspeccion,incisocot.fechasolautinspeccion, incisocot.fechaautinspeccion "
				+ " from MIDAS.toincisocot incisocot  where incisocot.idtocotizacion = "
				+ idToCotizacion + " and incisocot.numeroInciso = " + numeroIncisoBase;
		return queryString;
	}
	
	public String copiarIncisosAll(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal numeroIncisoBase, BigDecimal idToDireccion) {
		String queryString = "insert into MIDAS.toincisocot (IDTOCOTIZACION, NUMEROINCISO, IDTODIRECCIONINCISO, VALORPRIMANETA, " +
				"CLAVEESTATUSINSPECCION, CLAVETIPOORIGENINSPECCION, CLAVEMENSAJEINSPECCION, CODIGOUSUARIOESTINSPECCION, " +
				"CLAVEAUTINSPECCION, FECHAESTATUSINSPECCION, FECHASOLAUTINSPECCION, FECHAAUTINSPECCION, DESCRIPCIONGIROASEGURADO, " +
				"NUMEROSECUENCIA, IGUALACIONNIVELINCISO, IGUALACIONPRIMAANTERIOR, IGUALACIONDESCUENTOGENERADO) "
				+ " select "+ idToCotizacion+ " , " + numeroInciso + ", " + idToDireccion + ", incisocot.valorprimaneta, " +
				"incisocot.CLAVEESTATUSINSPECCION, incisocot.CLAVETIPOORIGENINSPECCION, incisocot.CLAVEMENSAJEINSPECCION, incisocot.CODIGOUSUARIOESTINSPECCION, " +
				"incisocot.CLAVEAUTINSPECCION, incisocot.FECHAESTATUSINSPECCION, incisocot.FECHASOLAUTINSPECCION, incisocot.FECHAAUTINSPECCION, incisocot.DESCRIPCIONGIROASEGURADO, " +
				"" + numeroInciso + ", incisocot.IGUALACIONNIVELINCISO, incisocot.IGUALACIONPRIMAANTERIOR, incisocot.IGUALACIONDESCUENTOGENERADO "
				+ " from MIDAS.toincisocot incisocot  where incisocot.idtocotizacion = "
				+ idToCotizacion + " and incisocot.numeroInciso = " + numeroIncisoBase;
		return queryString;
	}

	public String copiarSecciones(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal numeroIncisoBase) {
		String queryString = "insert into MIDAS.toseccioncot (idtocotizacion, numeroinciso, idtoseccion, valorprimaneta, valorsumaasegurada,"
				+ " claveobligatoriedad, clavecontrato) "
				+ " select "
				+ idToCotizacion
				+ " , " + numeroInciso + ", seccioncot.idtoseccion, seccioncot.valorprimaneta, seccioncot.valorsumaasegurada, seccioncot.claveobligatoriedad, seccioncot.clavecontrato "
				+ " from MIDAS.toseccioncot seccioncot where seccioncot.idtocotizacion = "
				+ idToCotizacion + " and seccioncot.numeroInciso = " + numeroIncisoBase;
		return queryString;
	}
	
	public String copiarIncisosAuto(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal numeroIncisoBase) {
		String queryString = "insert into MIDAS.TOINCISOAUTOCOT (ID, COTIZACION_ID, NUMEROINCISO, CLAVETIPOBIEN, IDVERSIONCARGA, IDMONEDA, MODELOVEHICULO, ESTADO_ID, MUNICIPIO_ID, MODIFICADORESPRIMA, " +
				"MODIFICADORESDESCRIPCION, NUMEROSERIE, NUMEROMOTOR, PAQUETE_ID, TIPOUSO_ID, MARCA_ID, DESCRIPCION_FINAL, " +
				"PLACA, NEGOCIOSECCION_ID, ESTILO_ID, NEGOCIOPAQUETE_ID, REPUVE, NUMEROLICENCIA, NOMBRECONDUCTOR, " +
				"PATERNOCONDUCTOR, MATERNOCONDUCTOR, FECHANACCONDUCTOR, OCUPACIONCONDUCTOR, ASOCIADA_COTIZACION, NOMBREASEGURADO, " +
				"PERSONAASEGURADO_ID, OBSERVACIONESINCISO, TIPOSERVICIO_ID, CLAVESIMILAR) "
				+ " select MIDAS.TOINCISOAUTOCOT_SEQ.nextval, "+ idToCotizacion + " , " + numeroInciso 
				+ ", autocot.CLAVETIPOBIEN, autocot.IDVERSIONCARGA, autocot.IDMONEDA, autocot.MODELOVEHICULO, autocot.ESTADO_ID, autocot.MUNICIPIO_ID, autocot.MODIFICADORESPRIMA, " +
				" autocot.MODIFICADORESDESCRIPCION, autocot.NUMEROSERIE, autocot.NUMEROMOTOR, autocot.PAQUETE_ID, autocot.TIPOUSO_ID, autocot.MARCA_ID, autocot.DESCRIPCION_FINAL, " +
				" autocot.PLACA, autocot.NEGOCIOSECCION_ID, autocot.ESTILO_ID, autocot.NEGOCIOPAQUETE_ID, autocot.REPUVE, autocot.NUMEROLICENCIA, autocot.NOMBRECONDUCTOR, " +
				" autocot.PATERNOCONDUCTOR, autocot.MATERNOCONDUCTOR, autocot.FECHANACCONDUCTOR, autocot.OCUPACIONCONDUCTOR, autocot.ASOCIADA_COTIZACION, autocot.NOMBREASEGURADO, " +
				"autocot.PERSONAASEGURADO_ID, autocot.OBSERVACIONESINCISO, autocot.TIPOSERVICIO_ID, autocot.CLAVESIMILAR "
				+ " from MIDAS.TOINCISOAUTOCOT autocot where autocot.COTIZACION_ID = "
				+ idToCotizacion + " and autocot.NUMEROINCISO = " + numeroIncisoBase;
		return queryString;
	}

	public String copiarCoberturas(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal numeroIncisoBase) {

		String queryString = "insert into MIDAS.tocoberturacot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura, idtcsubramo,"
				+ " valorprimaneta, valorsumaasegurada, valorcoaseguro, valordeducible, codigousuarioautreaseguro, "
				+ " claveobligatoriedad, clavecontrato, valorcuota, porcentajecoaseguro, claveautcoaseguro, "
				+ " codigousuarioautcoaseguro, porcentajededucible, claveautdeducible, codigousuarioautdeducible, numeroagrupacion, "
				+ " fechasolautcoaseguro, fechaautcoaseguro, fechasolautdeducible, fechaautdeducible, clavetipodeducible, "
				+ " clavetipolimitededucible, valorminimolimitededucible, valormaximolimitededucible) "
				+ " select "
				+ idToCotizacion
				+ " , " + numeroInciso + ", coberturacot.idtoseccion, coberturacot.idtocobertura, coberturacot.idtcsubramo,"
				+ " coberturacot.valorprimaneta, coberturacot.valorsumaasegurada, coberturacot.valorcoaseguro, coberturacot.valordeducible, coberturacot.codigousuarioautreaseguro,"
				+ " coberturacot.claveobligatoriedad, coberturacot.clavecontrato, coberturacot.valorcuota, coberturacot.porcentajecoaseguro, coberturacot.claveautcoaseguro,"
				+ " coberturacot.codigousuarioautcoaseguro, coberturacot.porcentajededucible, coberturacot.claveautdeducible, coberturacot.codigousuarioautdeducible, numeroagrupacion, "
				+ " coberturacot.fechasolautcoaseguro, coberturacot.fechaautcoaseguro, coberturacot.fechasolautdeducible, coberturacot.fechaautdeducible, coberturacot.clavetipodeducible,"
				+ " coberturacot.clavetipolimitededucible, coberturacot.valorminimolimitededucible, coberturacot.valormaximolimitededucible"
				+ " from MIDAS.tocoberturacot coberturacot where coberturacot.idtocotizacion ="
				+ idToCotizacion + " and coberturacot.numeroInciso = " + numeroIncisoBase;
		return queryString;
	}
	
	public String copiarCoberturasAll(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal numeroIncisoBase) {

		String queryString = "insert into MIDAS.tocoberturacot (IDTOCOTIZACION, NUMEROINCISO, IDTOSECCION, IDTOCOBERTURA, IDTCSUBRAMO, " +
				"VALORPRIMANETA, VALORSUMAASEGURADA, VALORCOASEGURO, VALORDEDUCIBLE, CODIGOUSUARIOAUTREASEGURO, " +
				"CLAVEOBLIGATORIEDAD, CLAVECONTRATO, VALORCUOTA, PORCENTAJECOASEGURO, CLAVEAUTCOASEGURO, " +
				"CODIGOUSUARIOAUTCOASEGURO, PORCENTAJEDEDUCIBLE, CLAVEAUTDEDUCIBLE, CODIGOUSUARIOAUTDEDUCIBLE, NUMEROAGRUPACION, " +
				"FECHASOLAUTCOASEGURO, FECHAAUTCOASEGURO, FECHASOLAUTDEDUCIBLE, FECHAAUTDEDUCIBLE, CLAVETIPODEDUCIBLE, " +
				"CLAVETIPOLIMITEDEDUCIBLE, VALORMINIMOLIMITEDEDUCIBLE, VALORMAXIMOLIMITEDEDUCIBLE, CLAVEFACULTATIVO, IDTOAGRUPADORTARIFA, " +
				"IDVERAGRUPADORTARIFA, IDVERSIONCARGA, IDVERSIONTARIFA, IDTARIFAEXT, IDVERSIONTARIFAEXT, " +
				"VALORPRIMADIARIA, CLAVEGRUPOPROLIBER, DIASSALARIOMINIMO) "
				+ " select "+ idToCotizacion+ " , " + numeroInciso + ", coberturacot.idtoseccion, coberturacot.idtocobertura, coberturacot.idtcsubramo,"
				+ " coberturacot.VALORPRIMANETA, coberturacot.VALORSUMAASEGURADA, coberturacot.VALORCOASEGURO, coberturacot.VALORDEDUCIBLE, coberturacot.CODIGOUSUARIOAUTREASEGURO, " +
				"coberturacot.CLAVEOBLIGATORIEDAD, coberturacot.CLAVECONTRATO, coberturacot.VALORCUOTA, coberturacot.PORCENTAJECOASEGURO, coberturacot.CLAVEAUTCOASEGURO, " +
				"coberturacot.CODIGOUSUARIOAUTCOASEGURO, coberturacot.PORCENTAJEDEDUCIBLE, coberturacot.CLAVEAUTDEDUCIBLE, coberturacot.CODIGOUSUARIOAUTDEDUCIBLE, coberturacot.NUMEROAGRUPACION, " +
				"coberturacot.FECHASOLAUTCOASEGURO, coberturacot.FECHAAUTCOASEGURO, coberturacot.FECHASOLAUTDEDUCIBLE, coberturacot.FECHAAUTDEDUCIBLE, coberturacot.CLAVETIPODEDUCIBLE, " +
				"coberturacot.CLAVETIPOLIMITEDEDUCIBLE, coberturacot.VALORMINIMOLIMITEDEDUCIBLE, coberturacot.VALORMAXIMOLIMITEDEDUCIBLE, coberturacot.CLAVEFACULTATIVO, coberturacot.IDTOAGRUPADORTARIFA, " +
				"coberturacot.IDVERAGRUPADORTARIFA, coberturacot.IDVERSIONCARGA, coberturacot.IDVERSIONTARIFA, coberturacot.IDTARIFAEXT, coberturacot.IDVERSIONTARIFAEXT, " +
				"coberturacot.VALORPRIMADIARIA, coberturacot.CLAVEGRUPOPROLIBER, coberturacot.DIASSALARIOMINIMO "
				+ " from MIDAS.tocoberturacot coberturacot where coberturacot.idtocotizacion ="
				+ idToCotizacion + " and coberturacot.numeroInciso = " + numeroIncisoBase;
		return queryString;
	}

	public String copiarRiesgos(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal numeroIncisoBase) {
		String queryString = "insert into MIDAS.toriesgocot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura, idtoriesgo, "
				+ " idtcsubramo, valorprimaneta, valorsumaasegurada, valorcoaseguro, valordeducible, "
				+ " valordescuento, valorrecargo, valoraumento, porcentajecoaseguro, claveautcoaseguro, "
				+ " codigousuarioautcoaseguro, porcentajededucible, claveautdeducible, codigousuarioautdeducible, claveobligatoriedad, "
				+ " clavecontrato, valorcuotab, valorcuotaardt, valorprimanetaardt, valorprimanetab,  "
				+ " fechasolautcoaseguro, fechaautcoaseguro, fechasolautdeducible, fechaautdeducible, clavetipodeducible,  "
				+ " clavetipolimitededucible, valorminimolimitededucible, valormaximolimitededucible) "
				+ " select "
				+ idToCotizacion
				+ " , " + numeroInciso + ", riesgocot.idtoseccion, riesgocot.idtocobertura, riesgocot.idtoriesgo,"
				+ " riesgocot.idtcsubramo, riesgocot.valorprimaneta, riesgocot.valorsumaasegurada, riesgocot.valorcoaseguro, riesgocot.valordeducible,"
				+ " riesgocot.valordescuento, riesgocot.valorrecargo, riesgocot.valoraumento, riesgocot.porcentajecoaseguro, riesgocot.claveautcoaseguro,"
				+ " riesgocot.codigousuarioautcoaseguro, riesgocot.porcentajededucible, riesgocot.claveautdeducible, riesgocot.codigousuarioautdeducible, riesgocot.claveobligatoriedad, "
				+ " riesgocot.clavecontrato, riesgocot.valorcuotab, riesgocot.valorcuotaardt, riesgocot.valorprimanetaardt, riesgocot.valorprimanetab,"
				+ " riesgocot.fechasolautcoaseguro, riesgocot.fechaautcoaseguro, riesgocot.fechasolautdeducible, riesgocot.fechaautdeducible, riesgocot.clavetipodeducible,"
				+ " riesgocot.clavetipolimitededucible, riesgocot.valorminimolimitededucible, riesgocot.valormaximolimitededucible"
				+ " from MIDAS.toriesgocot riesgocot where riesgocot.idtocotizacion ="
				+ idToCotizacion + " and riesgocot.numeroInciso = " + numeroIncisoBase;
		return queryString;
	}

	public String copiarSubIncisos(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal numeroIncisoBase) {
		String queryString = "insert into MIDAS.tosubincisocot (idtocotizacion, numeroinciso, idtoseccion, numerosubinciso, claveautreaseguro, "
				+ " codigousuarioautreaseguro, descripcionsubinciso, valorsumaasegurada, claveestatusdeclaracion) "
				+ " SELECT "
				+ idToCotizacion
				+ ", " + numeroInciso + ", subincisocot.idtoseccion, subincisocot.numerosubinciso, subincisocot.claveautreaseguro, "
				+ " subincisocot.codigousuarioautreaseguro, subincisocot.descripcionsubinciso, subincisocot.valorsumaasegurada, subincisocot.claveestatusdeclaracion "
				+ " FROM MIDAS.tosubincisocot subincisocot WHERE subincisocot.idToCotizacion = "
				+ idToCotizacion + " and subincisocot.numeroInciso = " + numeroIncisoBase;
		return queryString;
	}

	public String copiarDetallePrimaCobertura(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal numeroIncisoBase) {
		String queryString = "insert into MIDAS.todetprimacoberturacot (numerosubinciso, idtocotizacion, numeroinciso, idtoseccion, idtocobertura,"
				+ " valorprimanetab, valorprimanetaardt, valorprimaneta, valorcuotab, valorcuotaardt, "
				+ " valorcuotaardv, valorcuota)"
				+ " SELECT detprima.numerosubinciso, "
				+ idToCotizacion
				+ ", " + numeroInciso + ", detprima.idtoseccion, detprima.idtocobertura, "
				+ " detprima.valorprimanetab, detprima.valorprimanetaardt, detprima.valorprimaneta, detprima.valorcuotab, detprima.valorcuotaardt, "
				+ " detprima.valorcuotaardv, detprima.valorcuota "
				+ " FROM MIDAS.todetprimacoberturacot detprima WHERE detprima.idToCotizacion = "
				+ idToCotizacion + " and detprima.numeroInciso = " + numeroIncisoBase;
		return queryString;
	}

	public String copiarDetallePrimaRiesgo(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal numeroIncisoBase) {
		String queryString = "insert into MIDAS.todetprimariesgocot (numerosubinciso, idtocotizacion, numeroinciso, idtoseccion, idtocobertura, idtoriesgo,"
				+ " valorsumaasegurada, valorprimanetab, valorprimanetaardt, valorprimaneta, valorcuotab, valorcuotaardt, "
				+ " valorcuotaardv, valorcuota)"
				+ " SELECT detprima.numerosubinciso, "
				+ idToCotizacion
				+ ", " + numeroInciso + ", detprima.idtoseccion, detprima.idtocobertura, detprima.idtoriesgo,"
				+ " detprima.valorsumaasegurada, detprima.valorprimanetab, detprima.valorprimanetaardt, detprima.valorprimaneta, detprima.valorcuotab, detprima.valorcuotaardt, "
				+ " detprima.valorcuotaardv, detprima.valorcuota "
				+ " FROM MIDAS.todetprimariesgocot detprima WHERE detprima.idToCotizacion = "
				+ idToCotizacion + " and detprima.numeroInciso = " + numeroIncisoBase;
		return queryString;
	}

	protected String copiarDatosIncisos(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal numeroIncisoBase) {
		String query = "INSERT INTO MIDAS.todatoincisocot d1 " +
				"(d1.idtocotizacion, d1.numeroinciso, d1.idtoseccion, " +
				"d1.idtocobertura, d1.idtoriesgo, d1.numerosubinciso, d1.idtcramo, " +
				"d1.idtcsubramo, d1.clavedetalle, d1.iddato, d1.valor) " +
				"(SELECT "+idToCotizacion+", " + numeroInciso + ", d2.idtoseccion, " +
				"d2.idtocobertura, d2.idtoriesgo, d2.numerosubinciso, d2.idtcramo, " +
				"d2.idtcsubramo, d2.clavedetalle, d2.iddato, d2.valor " +
				"FROM MIDAS.todatoincisocot d2 " +
				"WHERE d2.idtocotizacion = " + idToCotizacion + " and d2.numeroInciso = " + numeroIncisoBase + ")";
		return query;
	}
	
	protected String copiarDatosIncisosAuto(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal numeroIncisoBase) {
		String query = "INSERT INTO midas.todatoincisocotauto d1 " +
				"(d1.id, d1.idtocotizacion, d1.numeroinciso, d1.idtoseccion, " +
				"d1.idtocobertura, d1.idtcramo, " +
				"d1.idtcsubramo, d1.clavedetalle, d1.iddato, d1.valor) " +
				"(SELECT  MIDAS.IDTODATOINCISOCOTAUTO_SEQ.nextval, "+idToCotizacion+", " + numeroInciso + ", d2.idtoseccion, " +
				"d2.idtocobertura, d2.idtcramo, " +
				"d2.idtcsubramo, d2.clavedetalle, d2.iddato, d2.valor " +
				"FROM midas.todatoincisocotauto d2 " +
				"WHERE d2.idtocotizacion = " + idToCotizacion + " and d2.numeroInciso = " + numeroIncisoBase + ")";
		return query;
	}		

	protected String copiarAumentos(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal numeroIncisoBase) {
		String queryString = "INSERT INTO MIDAS.toaumentoriesgocot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura,"
				+ " idtoriesgo, idtoaumentovario, claveautorizacion, codigousuarioautorizacion, valoraumento, claveobligatoriedad, "
				+ " clavecontrato, clavecomercialtecnico, clavenivel, fechasolicitudautorizacion, fechaautorizacion) "
				+ " select "
				+ idToCotizacion
				+ " , " + numeroInciso + ", riesgocot.idtoseccion, riesgocot.idtocobertura,"
				+ " riesgocot.idtoriesgo, riesgocot.idtoaumentovario, riesgocot.claveautorizacion, riesgocot.codigousuarioautorizacion, riesgocot.valoraumento, riesgocot.claveobligatoriedad,"
				+ " riesgocot.clavecontrato, riesgocot.clavecomercialtecnico, riesgocot.clavenivel, riesgocot.fechasolicitudautorizacion, riesgocot.fechaautorizacion"
				+ " from MIDAS.toaumentoriesgocot riesgocot where riesgocot.idtocotizacion = "
				+ idToCotizacion + " and riesgocot.numeroInciso = " + numeroIncisoBase;
		return queryString;
	}
	
	protected String copiarRecargos(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal numeroIncisoBase) {
		String queryString = "INSERT INTO MIDAS.torecargoriesgocot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura,"
				+ " idtoriesgo, idtorecargovario, claveautorizacion, codigousuarioautorizacion, valorrecargo, claveobligatoriedad, "
				+ " clavecontrato, clavecomercialtecnico, clavenivel, fechasolicitudautorizacion, fechaautorizacion) "
				+ " select "
				+ idToCotizacion
				+ " , " + numeroInciso + ", riesgocot.idtoseccion, riesgocot.idtocobertura,"
				+ " riesgocot.idtoriesgo, riesgocot.idtorecargovario, riesgocot.claveautorizacion, riesgocot.codigousuarioautorizacion, riesgocot.valorrecargo, riesgocot.claveobligatoriedad,"
				+ " riesgocot.clavecontrato, riesgocot.clavecomercialtecnico, riesgocot.clavenivel, riesgocot.fechasolicitudautorizacion, riesgocot.fechaautorizacion"
				+ " from MIDAS.torecargoriesgocot riesgocot where riesgocot.idtocotizacion = "
				+ idToCotizacion + " and riesgocot.numeroInciso = " + numeroIncisoBase;
		return queryString;
	}	

	protected String copiarDescuentos(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal numeroIncisoBase) {
		String queryString = "INSERT INTO MIDAS.todescuentoriesgocot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura,"
				+ " idtoriesgo, idtodescuentovario, claveautorizacion, codigousuarioautorizacion, valordescuento, claveobligatoriedad, "
				+ " clavecontrato, clavecomercialtecnico, clavenivel, fechasolicitudautorizacion, fechaautorizacion) "
				+ " select "
				+ idToCotizacion
				+ " , " + numeroInciso + ", riesgocot.idtoseccion, riesgocot.idtocobertura,"
				+ " riesgocot.idtoriesgo, riesgocot.idtodescuentovario, riesgocot.claveautorizacion, riesgocot.codigousuarioautorizacion, riesgocot.valordescuento, riesgocot.claveobligatoriedad,"
				+ " riesgocot.clavecontrato, riesgocot.clavecomercialtecnico, riesgocot.clavenivel, riesgocot.fechasolicitudautorizacion, riesgocot.fechaautorizacion"
				+ " from MIDAS.todescuentoriesgocot riesgocot where riesgocot.idtocotizacion = "
				+ idToCotizacion + " and riesgocot.numeroInciso = " + numeroIncisoBase;
		return queryString;
	}
}
