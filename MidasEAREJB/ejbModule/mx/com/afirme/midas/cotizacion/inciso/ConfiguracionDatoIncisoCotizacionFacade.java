package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity ConfiguracionDatoIncisoCotizacionDTO.
 * 
 * @see .ConfiguracionDatoIncisoCotizacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ConfiguracionDatoIncisoCotizacionFacade implements
		ConfiguracionDatoIncisoCotizacionFacadeRemote {
	// property constants
	public static final String CLAVE_TIPO_CONTROL = "claveTipoControl";
	public static final String CLAVE_TIPO_VALIDACION = "claveTipoValidacion";
	public static final String DESCRIPCION_ETIQUETA = "descripcionEtiqueta";
	public static final String DESCRIPCION_CLASE_REMOTA = "descripcionClaseRemota";

	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private DatoIncisoCotizacionFacadeRemote datoIncisoCotFacade;

	/**
	 * Perform an initial save of a previously unsaved
	 * ConfiguracionDatoIncisoCotizacionDTO entity. All subsequent persist
	 * actions of this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ConfiguracionDatoIncisoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ConfiguracionDatoIncisoCotizacionDTO entity) {
		LogDeMidasEJB3.log(
				"saving ConfiguracionDatoIncisoCotizacionDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ConfiguracionDatoIncisoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            ConfiguracionDatoIncisoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ConfiguracionDatoIncisoCotizacionDTO entity) {
		LogDeMidasEJB3.log(
				"deleting ConfiguracionDatoIncisoCotizacionDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					ConfiguracionDatoIncisoCotizacionDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ConfiguracionDatoIncisoCotizacionDTO entity
	 * and return it or a copy of it to the sender. A copy of the
	 * ConfiguracionDatoIncisoCotizacionDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ConfiguracionDatoIncisoCotizacionDTO entity to update
	 * @return ConfiguracionDatoIncisoCotizacionDTO the persisted
	 *         ConfiguracionDatoIncisoCotizacionDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ConfiguracionDatoIncisoCotizacionDTO update(
			ConfiguracionDatoIncisoCotizacionDTO entity) {
		LogDeMidasEJB3.log(
				"updating ConfiguracionDatoIncisoCotizacionDTO instance",
				Level.INFO, null);
		try {
			ConfiguracionDatoIncisoCotizacionDTO result = entityManager
					.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ConfiguracionDatoIncisoCotizacionDTO findById(
			ConfiguracionDatoIncisoCotizacionId id) {
		LogDeMidasEJB3.log(
				"finding ConfiguracionDatoIncisoCotizacionDTO instance with id: "
						+ id, Level.INFO, null);
		try {
			ConfiguracionDatoIncisoCotizacionDTO instance = entityManager.find(ConfiguracionDatoIncisoCotizacionDTO.class, id);
			
			try{
				if(instance != null)
					entityManager.refresh(instance);
			}catch(Exception e){}
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ConfiguracionDatoIncisoCotizacionDTO entities with a specific
	 * property value.
	 * 
	 * @param propertyName
	 *            the name of the ConfiguracionDatoIncisoCotizacionDTO property
	 *            to query
	 * @param value
	 *            the property value to match
	 * @return List<ConfiguracionDatoIncisoCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ConfiguracionDatoIncisoCotizacionDTO> findByProperty(
			String propertyName, final Object value) {
		LogDeMidasEJB3.log(
				"finding ConfiguracionDatoIncisoCotizacionDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ConfiguracionDatoIncisoCotizacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<ConfiguracionDatoIncisoCotizacionDTO> findByClaveTipoControl(
			Object claveTipoControl) {
		return findByProperty(CLAVE_TIPO_CONTROL, claveTipoControl);
	}

	public List<ConfiguracionDatoIncisoCotizacionDTO> findByClaveTipoValidacion(
			Object claveTipoValidacion) {
		return findByProperty(CLAVE_TIPO_VALIDACION, claveTipoValidacion);
	}

	public List<ConfiguracionDatoIncisoCotizacionDTO> findByDescripcionEtiqueta(
			Object descripcionEtiqueta) {
		return findByProperty(DESCRIPCION_ETIQUETA, descripcionEtiqueta);
	}

	public List<ConfiguracionDatoIncisoCotizacionDTO> findByDescripcionClaseRemota(
			Object descripcionClaseRemota) {
		return findByProperty(DESCRIPCION_CLASE_REMOTA, descripcionClaseRemota);
	}

	/**
	 * Find all ConfiguracionDatoIncisoCotizacionDTO entities.
	 * 
	 * @return List<ConfiguracionDatoIncisoCotizacionDTO> all
	 *         ConfiguracionDatoIncisoCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ConfiguracionDatoIncisoCotizacionDTO> findAll() {
		LogDeMidasEJB3.log(
				"finding all ConfiguracionDatoIncisoCotizacionDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from ConfiguracionDatoIncisoCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRamoInciso(BigDecimal idTcRamo) {
		LogDeMidasEJB3.log(
				"finding all ConfiguracionDatoIncisoCotizacionDTO instances",
				Level.INFO, null);
		try {
			String queryString = "select model from ConfiguracionDatoIncisoCotizacionDTO model " +
					"where model.id.idTcRamo = :idTcRamo and model.id.idTcSubramo = 0 and " +
					"model.id.idToRiesgo = 0 and model.id.claveDetalle = 0 order by model.id.idDato";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTcRamo", idTcRamo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoInciso(BigDecimal idToRiesgo) {
		LogDeMidasEJB3.log(
				"finding all ConfiguracionDatoIncisoCotizacionDTO instances",
				Level.INFO, null);
		try {
			String queryString = "select model from ConfiguracionDatoIncisoCotizacionDTO model " +
					"where model.id.idToRiesgo = :idToRiesgo and model.id.claveDetalle = 0 order by model.id.idDato";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToRiesgo", idToRiesgo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoSubInciso(BigDecimal idToRiesgo) {
		LogDeMidasEJB3.log(
				"finding all ConfiguracionDatoIncisoCotizacionDTO instances",
				Level.INFO, null);
		try {
			String queryString = "select model from ConfiguracionDatoIncisoCotizacionDTO model " +
					"where model.id.idToRiesgo = :idToRiesgo and model.id.claveDetalle = 2 order by model.id.idDato";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToRiesgo", idToRiesgo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoImpresionPoliza(BigDecimal idTcRamo, BigDecimal idTcSubRamo) {
		LogDeMidasEJB3.log(
				"ejecutando ConfiguracionDatoIncisoCotizacionFacade.getDatosRiesgoImpresionPoliza",
				Level.INFO, null);
		try {
			String queryString = "select model from ConfiguracionDatoIncisoCotizacionDTO model " +
					"where model.id.idTcRamo = :idTcRamo and model.id.idTcSubramo = :idTcSubRamo and " +
					"model.id.idToRiesgo > 0 and model.id.claveDetalle in (0,2) order by model.id.idToRiesgo, model.id.idDato";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTcRamo", idTcRamo);
			query.setParameter("idTcSubRamo", idTcSubRamo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("ConfiguracionDatoIncisoCotizacionFacade.getDatosRiesgoImpresionPoliza fallo", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ConfiguracionDatoIncisoCotizacionDTO> listarFiltrado(ConfiguracionDatoIncisoCotizacionDTO entity){
		try {
			LogDeMidasEJB3.log("listarFiltrado ConfiguracionDatoIncisoCotizacionDTO", Level.INFO, null);
			String queryString = "select model from ConfiguracionDatoIncisoCotizacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			if (entity.getId() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.claveDetalle", entity.getId().getClaveDetalle());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idDato", entity.getId().getIdDato());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idTcRamo", entity.getId().getIdTcRamo());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idTcSubramo", entity.getId().getIdTcSubramo());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToRiesgo", entity.getId().getIdToRiesgo());
			}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveimpresionpoliza", entity.getClaveimpresionpoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveTipoControl", entity.getClaveTipoControl());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveTipoValidacion", entity.getClaveTipoValidacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "descripcionClaseRemota", entity.getDescripcionClaseRemota());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "descripcionEtiqueta", entity.getDescripcionEtiqueta());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idGrupo", entity.getIdGrupo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoFormato", entity.getCodigoFormato());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("FALLO: listarFiltrado ConfiguracionDatoIncisoCotizacionDTO", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoInciso(BigDecimal idToTipoPoliza,
			BigDecimal idTcRamo) {
		String sql = "SELECT * " +
				"FROM MIDAS.tcconfigdatoincisocot conf, MIDAS.tcsubramo subr " +
				"WHERE conf.idtcramo = ? " +
				"AND subr.idtcsubramo = conf.idtcsubramo " +
				"AND conf.idtoriesgo IN ( " +
				"SELECT distinct riec.idtoriesgo " +
				"FROM MIDAS.totipopoliza tpol, " +
                "MIDAS.TOSECCION sec, " +
                "MIDAS.tocoberturaseccion cobs, " +
                "MIDAS.toriesgocobertura riec " +
                "WHERE tpol.idtotipopoliza = ? " +
                "AND sec.IDTOTIPOPOLIZA = tpol.idtotipopoliza " +
                "AND cobs.idtoseccion = sec.idtoseccion " +
                "AND riec.idtocobertura = cobs.idtocobertura " +
                "AND riec.IDTOSECCION = sec.idtoseccion) " +
                "AND conf.clavedetalle = 0 " +
                "order by codigocampo";
		List<ConfiguracionDatoIncisoCotizacionDTO> list = entityManager.createNativeQuery(sql, ConfiguracionDatoIncisoCotizacionDTO.class)
						.setParameter(1, idTcRamo)
						.setParameter(2, idToTipoPoliza)
						.getResultList();		
		return list;
	}
	
	/**
	 * Obtiene la etiqueta y valor real de un datoIncisoCotizacionDTO.
	 * @param datoIncisoCotizacionDTO
	 * @return String[] arreglo de String que contiene en la posici�n 0 la etiqueta, en la posici�n [1] el valor y en la posici�n [2] el valor registrado en datoIncisoCot.
	 * @throws SystemException
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String[] obtenerDescripcionDatoInciso(DatoIncisoCotizacionDTO datoIncisoCotizacionDTO,ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacionDTO){
		ConfiguracionDatoIncisoCotizacionDTO configuracionDato = null;
		if(configuracionDatoIncisoCotizacionDTO == null || configuracionDatoIncisoCotizacionDTO.getDescripcionEtiqueta() == null || configuracionDatoIncisoCotizacionDTO.getDescripcionEtiqueta().trim().equals("")){
			ConfiguracionDatoIncisoCotizacionId configuracionDatoId = new ConfiguracionDatoIncisoCotizacionId();
			configuracionDatoId.setIdTcRamo(datoIncisoCotizacionDTO.getId().getIdTcRamo());
			configuracionDatoId.setIdTcSubramo(datoIncisoCotizacionDTO.getId().getIdTcSubramo());
			configuracionDatoId.setIdToRiesgo(datoIncisoCotizacionDTO.getId().getIdToRiesgo());
			configuracionDatoId.setClaveDetalle(datoIncisoCotizacionDTO.getId().getClaveDetalle());
			configuracionDatoId.setIdDato(datoIncisoCotizacionDTO.getId().getIdDato());
			
			configuracionDato = findById(configuracionDatoId);
		} else{
			configuracionDato = configuracionDatoIncisoCotizacionDTO;
		}
		
		if (configuracionDato == null){
			LogDeMidasEJB3.log("No se ecnontr� la configuraci&oacute;n para el dato: \nidTcRamo: "+datoIncisoCotizacionDTO.getId().getIdTcRamo()+"\n"+
					"idTcSubRamo: "+datoIncisoCotizacionDTO.getId().getIdTcSubramo()+"\nidToRiesgo: "+datoIncisoCotizacionDTO.getId().getIdToRiesgo()+"\n"+
					"claveDetalle: "+datoIncisoCotizacionDTO.getId().getClaveDetalle()+"\nidDato: "+datoIncisoCotizacionDTO.getId().getIdDato(),
					Level.WARNING,null);
		}
		String result[] = new String[3];
		result[0]=configuracionDato.getDescripcionEtiqueta();
		//Si el dato no trae valor, se hace la consulta a la tabla.
		if (datoIncisoCotizacionDTO.getValor()==null){
			DatoIncisoCotizacionDTO datoIncisoCotTMP = null;
			datoIncisoCotTMP = datoIncisoCotFacade.findById(datoIncisoCotizacionDTO.getId());
			//Si no se encuentra, se lanza la excepcion
			if (datoIncisoCotTMP == null)
				LogDeMidasEJB3.log("No se encontr� DATOINCISOCOTIZACION con los datos: \nidTcRamo: "+datoIncisoCotizacionDTO.getId().getIdTcRamo()+"\n"+
					"idTcSubRamo: "+datoIncisoCotizacionDTO.getId().getIdTcSubramo()+"\nidToRiesgo: "+datoIncisoCotizacionDTO.getId().getIdToRiesgo()+"\n"+
					"claveDetalle: "+datoIncisoCotizacionDTO.getId().getClaveDetalle()+"\nidDato: "+datoIncisoCotizacionDTO.getId().getIdDato(),
					Level.WARNING,null);
			else
				datoIncisoCotizacionDTO = datoIncisoCotTMP;
		}
		result[1]=datoIncisoCotizacionDTO.getValor();
		result[2]=datoIncisoCotizacionDTO.getValor();
//		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		MidasInterfaceBase beanBase;
		switch(configuracionDato.getClaveTipoControl()) {
			case 1:
				try{
					Class remoteClass = Class.forName(configuracionDato.getDescripcionClaseRemota());
//					Context ctx = new InitialContext();
//					beanBase = (MidasInterfaceBase)ctx.lookup(configuracionDato.getDescripcionClaseRemota());
					beanBase = (MidasInterfaceBase)ServiceLocatorP.getInstance().getEJB(remoteClass);
					result[1] = beanBase.findById(Utilerias.regresaBigDecimal(result[1])).getDescription();
				}catch(Exception e){result[1] = "No disponible";}
				break;
			case 2:
				CatalogoValorFijoId catalogoFijoId = new CatalogoValorFijoId();
				try{
					
					catalogoFijoId.setIdDato(Integer.valueOf(result[1]).intValue());
					catalogoFijoId.setIdGrupoValores(Integer.valueOf(configuracionDato.getIdGrupo().toString()).intValue());
					Class remoteClass = Class.forName(configuracionDato.getDescripcionClaseRemota());
//					Context ctx = new InitialContext();
//					beanBase = (MidasInterfaceBase)ctx.lookup(configuracionDato.getDescripcionClaseRemota());
//					mx.com.afirme.midas.sistema.ServiceLocatorP.getInstance().getEJB(configuracionDato.getDescripcionClaseRemota())
					beanBase = (MidasInterfaceBase)ServiceLocatorP.getInstance().getEJB(remoteClass);
					result[1] = beanBase.findById(catalogoFijoId).getDescription();
				}catch(Exception e){result[1] = "No disponible";}
				
				break;
			default:{
				switch(configuracionDato.getCodigoFormato().intValue()){
					case 1:
						try{
							result[1] = new DecimalFormat("#,##0").format(Double.valueOf(result[1]));
						}catch(Exception e){
						}
				}
				break;
			}
		}
		return result;
	}
}