package mx.com.afirme.midas.cotizacion.inciso;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity IncisoCotizacionDTO.
 * 
 * @see .IncisoCotizacionDTO
 * @author MyEclipse Persistence Tools
 */

@Stateless
public class IncisoCotizacionFacade extends IncisoCotizacionSoporte implements IncisoCotizacionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	@Resource
	private SessionContext context;

	/**
	 * Perform an initial save of a previously unsaved IncisoCotizacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            IncisoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public IncisoCotizacionDTO save(IncisoCotizacionDTO entity) {
		LogDeMidasEJB3.log("saving IncisoCotizacionDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent IncisoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            IncisoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(IncisoCotizacionDTO entity) {
		LogDeMidasEJB3.log("deleting IncisoCotizacionDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(IncisoCotizacionDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved IncisoCotizacionDTO entity and return it or a
	 * copy of it to the sender. A copy of the IncisoCotizacionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            IncisoCotizacionDTO entity to update
	 * @return IncisoCotizacionDTO the persisted IncisoCotizacionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public IncisoCotizacionDTO update(IncisoCotizacionDTO entity) {
		LogDeMidasEJB3.log("updating IncisoCotizacionDTO instance", Level.INFO,
				null);
		try {
			IncisoCotizacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public IncisoCotizacionDTO findById(IncisoCotizacionId id) {
		LogDeMidasEJB3.log("finding IncisoCotizacionDTO instance with id: " + id, Level.INFO, null);
		try {
			IncisoCotizacionDTO instance = entityManager.find(IncisoCotizacionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
	/**
	 * Lista todos los Incisos correspondientes a una cotizacion
	 * con filtros opcionales: numeroSecuencia, linea Negocio, paqueteId
	 * @param idToCotizacion opcional
	 * @param nuermoInciso opcional
	 * @param numeroSecuencia opcional
	 * @param idToSeccion ppcional
	 * @param paqueteId opcional
	 * @return List<IncisoCotizacionDTO>
	 * @author martin
	 */
	@SuppressWarnings("unchecked")
	public List<IncisoCotizacionDTO> listarIncisosConFiltro(IncisoCotizacionDTO filtro){
		StringBuffer queryString = new StringBuffer();
		try{
			queryString.append("select model from IncisoCotizacionDTO model where ");
			
				// Agrega condiciones
			
				if (filtro.getId() != null && filtro.getId().getIdToCotizacion() != null){
					queryString.append("model.id.idToCotizacion = :idToCotizacion");
				}else{
					queryString.append("model.id.idToCotizacion != 0");
				}
				if (filtro.getId() != null && filtro.getId().getNumeroInciso() != null){
					queryString.append(" AND model.id.numeroInciso = :numeroInciso");
				}				
				if(filtro.getNumeroSecuencia() != null){
					queryString.append(" AND model.numeroSecuencia = :numeroSecuencia");
					
				}
				if(filtro.getIdToSeccion() != null){
					queryString.append(" AND model.id.numeroInciso IN (SELECT DISTINCT seccion.id.numeroInciso FROM SeccionCotizacionDTO seccion" +
							" WHERE seccion.seccionDTO.idToSeccion = :idToSeccion" +
							" AND seccion.id.idToCotizacion = :idToCotizacion )");
				}
				
				if(filtro.getIncisoAutoCot().getDescripcionFinal() != null && !filtro.getIncisoAutoCot().getDescripcionFinal().equals("null") && !filtro.getIncisoAutoCot().getDescripcionFinal().equals("")){
					queryString.append(" AND model.incisoAutoCot.descripcionFinal LIKE '%"+filtro.getIncisoAutoCot().getDescripcionFinal()+"%'");
				}
				
				if(filtro.getIncisoAutoCot().getNegocioPaqueteId() != null){
					queryString.append(" AND model.incisoAutoCot.negocioPaqueteId =:paqueteId");
				}
				
				if (filtro.getIncisoAutoCot().getNombreAsegurado() != null && !filtro.getIncisoAutoCot().getNombreAsegurado().equals("") ) {
					queryString.append(" AND model.incisoAutoCot.nombreAsegurado LIKE '%"+filtro.getIncisoAutoCot().getNombreAsegurado()+"%'");
				}
				
				if (filtro.getIncisoAutoCot().getPlaca() != null && !filtro.getIncisoAutoCot().getPlaca().equals("")) {
					queryString.append(" AND model.incisoAutoCot.placa LIKE '%" + filtro.getIncisoAutoCot().getPlaca() +"%'");
				}
	
				if (filtro.getIncisoAutoCot().getNumeroSerie() != null && !filtro.getIncisoAutoCot().getNumeroSerie().equals("")) {
					queryString.append(" AND model.incisoAutoCot.numeroSerie LIKE '%" + filtro.getIncisoAutoCot().getNumeroSerie()+ "%'");
				}
				
				queryString.append(" ORDER BY model.numeroSecuencia");
				
				//  Construye el query
				Query query = entityManager.createQuery(queryString.toString());
				
				// Agrega parametros al query
			
				if (filtro.getId() != null && filtro.getId().getIdToCotizacion() != null){
					query.setParameter("idToCotizacion", filtro.getId().getIdToCotizacion());
				}
				if (filtro.getId() != null && filtro.getId().getNumeroInciso() != null){
					query.setParameter("numeroInciso", filtro.getId().getNumeroInciso());
				}	
				if(filtro.getNumeroSecuencia() != null){
					query.setParameter("numeroSecuencia", filtro.getNumeroSecuencia());
				}
				if(filtro.getIdToSeccion() != null){
					query.setParameter("idToSeccion", filtro.getIdToSeccion());
				}
				if(filtro.getIncisoAutoCot().getNegocioPaqueteId() != null){
					query.setParameter("paqueteId", filtro.getIncisoAutoCot().getNegocioPaqueteId());
				}
				
				// Restricciones del paginado
				if(filtro.getPrimerRegistroACargar() != null && filtro.getNumeroMaximoRegistrosACargar() != null) {
					query.setFirstResult(filtro.getPrimerRegistroACargar().intValue());
					query.setMaxResults(filtro.getNumeroMaximoRegistrosACargar().intValue());
				}
				
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				// Ejecuta la consulta
				return query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public Long listarIncisosConFiltroCount(IncisoCotizacionDTO filtro){
		StringBuffer queryString = new StringBuffer();
		try{
			queryString.append("select count(model) from IncisoCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion");
			
				// Agrega condiciones
			
				if(filtro.getNumeroSecuencia() != null){
					queryString.append(" AND model.numeroSecuencia = :numeroSecuencia");
					
				}
				if(filtro.getIdToSeccion() != null){
					queryString.append(" AND model.id.numeroInciso IN (SELECT DISTINCT seccion.id.numeroInciso FROM SeccionCotizacionDTO seccion" +
							" WHERE seccion.seccionDTO.idToSeccion = :idToSeccion" +
							" AND seccion.id.idToCotizacion = :idToCotizacion )");
				}
				
				if(filtro.getIncisoAutoCot().getDescripcionFinal() != null && !filtro.getIncisoAutoCot().getDescripcionFinal().equals("null") && !filtro.getIncisoAutoCot().getDescripcionFinal().equals("")){
					queryString.append(" AND model.incisoAutoCot.descripcionFinal LIKE '%"+filtro.getIncisoAutoCot().getDescripcionFinal()+"%'");
				}
				
				if(filtro.getIncisoAutoCot().getNegocioPaqueteId() != null){
					queryString.append(" AND model.incisoAutoCot.negocioPaqueteId =:paqueteId");
				}
				
				if (filtro.getIncisoAutoCot().getNombreAsegurado() != null && !filtro.getIncisoAutoCot().getNombreAsegurado().equals("") ) {
					queryString.append(" AND model.incisoAutoCot.nombreAsegurado LIKE '%"+filtro.getIncisoAutoCot().getNombreAsegurado()+"%'");
				}
				
				if (filtro.getIncisoAutoCot().getPlaca() != null && !filtro.getIncisoAutoCot().getPlaca().equals("")) {
					queryString.append(" AND model.incisoAutoCot.placa LIKE '%" + filtro.getIncisoAutoCot().getPlaca() +"%'");
				}
	
				if (filtro.getIncisoAutoCot().getNumeroSerie() != null && !filtro.getIncisoAutoCot().getNumeroSerie().equals("")) {
					queryString.append(" AND model.incisoAutoCot.numeroSerie LIKE '%" + filtro.getIncisoAutoCot().getNumeroSerie()+ "%'");
				}
				
				queryString.append(" ORDER BY model.numeroSecuencia");
				
				Query query = entityManager.createQuery(queryString.toString());
				
				query.setParameter("idToCotizacion", filtro.getId().getIdToCotizacion());
				
				// Agrega parametros al query

				if (filtro.getId() != null){
					query.setParameter("idToCotizacion", filtro.getId().getIdToCotizacion());
				}
				if(filtro.getNumeroSecuencia() != null){
					query.setParameter("numeroSecuencia", filtro.getNumeroSecuencia());
				}
				if(filtro.getIdToSeccion() != null){
					query.setParameter("idToSeccion", filtro.getIdToSeccion());
				}
				if(filtro.getIncisoAutoCot().getNegocioPaqueteId() != null){
					query.setParameter("paqueteId", filtro.getIncisoAutoCot().getNegocioPaqueteId());
				}
				
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				// Ejecuta la consulta
				return (Long)query.getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Find all IncisoCotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the IncisoCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<IncisoCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<IncisoCotizacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding IncisoCotizacionDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from IncisoCotizacionDTO model where model."
					+ propertyName + "= :propertyValue order by model.id.numeroInciso";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all IncisoCotizacionDTO entities.
	 * 
	 * @return List<IncisoCotizacionDTO> all IncisoCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<IncisoCotizacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all IncisoCotizacionDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from IncisoCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all IncisoCotizacionDTO entities related with the given Cotizacion
	 * ID.
	 * 
	 * @param BigDecimal
	 *            idToCotizacion
	 * @return List<IncisoCotizacionDTO> all IncisoCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<IncisoCotizacionDTO> findByCotizacionId(
			BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log(
				"finding all IncisoCotizacionDTO instances related with Cotizacion ID: "
						+ idToCotizacion, Level.INFO, null);
		try {
			entityManager.flush();
			final String queryString = "select i from IncisoCotizacionDTO i where i.id.idToCotizacion = :idToCotizacion  order by i.numeroSecuencia";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<IncisoCotizacionDTO> findNoAsignadosByCotizacionId(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log(
				"finding all IncisoCotizacionDTO instances related with Cotizacion ID: "
						+ idToCotizacion, Level.INFO, null);
		try {
			entityManager.flush();
			final String queryString = "select i from IncisoCotizacionDTO i where i.id.idToCotizacion = :idToCotizacion  " +
					" and (i.incisoAutoCot.asociadaCotizacion = 0 OR i.incisoAutoCot.asociadaCotizacion is null) " +
					" order by i.numeroSecuencia ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public List<IncisoCotizacionDTO> findByCotizacionId(
			BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		try {
			entityManager.flush();
			StringBuffer queryString = new StringBuffer("SELECT i FROM IncisoCotizacionDTO i");
			queryString.append(" WHERE i.id.idToCotizacion = :idToCotizacion");
			if(numeroInciso != null){
				queryString.append(" AND i.id.numeroInciso = :numeroInciso");
			}
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public BigDecimal maxIncisos(BigDecimal idToCotizacion) {
		try {
			final String queryString = "select max(incisos.id.numeroInciso) from IncisoCotizacionDTO incisos where incisos.id.idToCotizacion = :idToCotizacion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			BigDecimal numInciso = (BigDecimal) query.getSingleResult();
			if (numInciso == null)
				numInciso = BigDecimal.ZERO;
			return numInciso;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("count failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public int contarIncisosPorCotizacion(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("Contando incisos de la cotizacion: "+idToCotizacion, Level.INFO, null);
		try {
			if(idToCotizacion == null)
				return 0;
			final String queryString = "select count(incisos.id.numeroInciso) from IncisoCotizacionDTO incisos where incisos.id.idToCotizacion = :idToCotizacion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			Object result = query.getSingleResult();
			int cantidadIncisos=0;// = (BigDecimal) query.getSingleResult();
			if(result == null)
				cantidadIncisos = 0;
			else if(result instanceof Long)
				cantidadIncisos = ((Long)result).intValue();
			else if(result instanceof Integer)
				cantidadIncisos = ((Integer)result).intValue();
			else if(result instanceof BigDecimal)
				cantidadIncisos = ((BigDecimal)result).intValue();
			return cantidadIncisos;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� el conteo de incisos de la cotizacion: "+idToCotizacion, Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<IncisoCotizacionDTO> listarAutorizacionesPendientesYRechazadasPorCotizacion(
			BigDecimal idToCotizacion) {
		LogDeMidasEJB3
				.log(
						"finding all IncisoCotizacionDTO instances with claveAutInspeccion = 1:Pendiente Autorizacion or 8:Rechazada, listed by idToCotizacion with value: "
								+ idToCotizacion, Level.INFO, null);
		try {
			final String queryString = "SELECT model FROM IncisoCotizacionDTO model WHERE model.id.idToCotizacion = :idToCotizacion AND (model.claveAutInspeccion = 1 OR  model.claveAutInspeccion = 8)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public Double getSumaAseguradaMayorPorUbicacion(BigDecimal idToCotizacion,
			String seccionesConComas) {
		LogDeMidasEJB3.log("suma asegurada mayor por ubicaci�n", Level.INFO,
				null);
		try {
			String queryString = "";
			Double sumaAsegurada = 0D;

			queryString = "select SUM (t.VALORSUMAASEGURADA) SUMAASEGURADA, t.numeroinciso";
			queryString += " FROM midas.toseccioncot t";
			queryString += " WHERE t.idtocotizacion = "
					+ idToCotizacion.intValue();
			queryString += " and t.idtoseccion in(" + seccionesConComas;
			queryString += ") and t.claveContrato = 1 GROUP BY t.numeroinciso ";
			queryString += " ORDER BY SUMAASEGURADA DESC";
			Query query = entityManager.createNativeQuery(queryString);

			List resultado = (List) query.getResultList();
			if (resultado != null) {
				if(resultado.get(0) instanceof List){
					List ubicacionMayor = (List) resultado.get(0);
					sumaAsegurada = ((BigDecimal) ubicacionMayor.get(0))
							.doubleValue();					
				}else{
					Object[] ubicacionMayor = (Object[]) resultado.get(0);
					sumaAsegurada = ((BigDecimal) ubicacionMayor[0]).doubleValue();
				}
			}
			return sumaAsegurada;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("count failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public BigDecimal primaNetaPorCotizacionPrimerRiesgo(
			BigDecimal idToCotizacion, String seccionesConComas) {
		LogDeMidasEJB3.log(
				"suma asegurada y prima neta agrupada por ubicacion",
				Level.INFO, null);
		try {

			String queryString = "";
			queryString = "SELECT   SUM (t.valorprimaneta) primaneta, t.numeroinciso ";
			queryString += " FROM midas.tocoberturacot t, midas.toseccioncot s";
			queryString += " WHERE t.idtocotizacion = "
					+ idToCotizacion.intValue();
			queryString += "   AND t.clavecontrato = 1 ";
			queryString += "   AND t.idtoseccion =  s.idtoseccion ";
			queryString += "   AND t.numeroinciso =  s.numeroinciso ";
			queryString += "   AND t.idtocotizacion =  s.idtocotizacion ";
			queryString += "   AND s.clavecontrato = 1 ";
			queryString += "   AND t.idtoseccion IN (" + seccionesConComas;
			queryString += ")  AND t.idtocobertura IN (SELECT idtocobertura";
			queryString += " FROM midas.tocobertura";
			queryString += "  WHERE claveprimerriesgo = 1)";
			queryString += "	GROUP BY t.numeroinciso";
			queryString += "	ORDER BY primaneta DESC ";

			Query query = entityManager.createNativeQuery(queryString);

			List resultado = (List) query.getResultList();
			BigDecimal primaNeta = BigDecimal.ZERO;
			if (resultado != null) {
				for (Object resultadoCobertura : resultado) {
					if(resultadoCobertura instanceof List){
						List ubicaciones = (List) resultadoCobertura;
						primaNeta = primaNeta.add((BigDecimal) ubicaciones.get(0));						
					}else{
						Object[] ubicaciones = (Object[]) resultadoCobertura;
						primaNeta = primaNeta.add((BigDecimal) ubicaciones[0]);		
					}
				}
			}
			return primaNeta;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("count failed", Level.SEVERE, re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")

	public BigDecimal sumaAseguradaPrimerRiesgoPorCotizacion(
			BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log(
				"suma asegurada y prima neta agrupada por ubicacion",
				Level.INFO, null);
		try {

			String queryString = "";

			queryString = "SELECT   SUM (t.valorsumaasegurada) valorsumaasegurada, t.numeroinciso ";
			queryString += " FROM midas.tocoberturacot t, midas.toseccioncot s";
			queryString += " WHERE t.idtocotizacion = "+idToCotizacion.intValue();
			queryString += " AND t.clavecontrato = 1";
			queryString += " AND t.idtocobertura IN (SELECT idtocobertura FROM midas.tocobertura WHERE claveprimerriesgo = 1 and claveTipoSumaAsegurada = 1)";
			queryString += " AND t.idtocotizacion = s.idtocotizacion " +
					"AND t.numeroinciso = s.numeroinciso " +
					"AND t.idtoseccion = s.idtoseccion " +
					"AND s.clavecontrato = 1";
			queryString += "	GROUP BY t.numeroinciso";
			queryString += "	ORDER BY valorsumaasegurada DESC ";

			Query query = entityManager.createNativeQuery(queryString);

			List resultado = (List) query.getResultList();
			BigDecimal primaNeta = BigDecimal.ZERO;
			if (resultado != null) {
				for (Object resultadoCobertura : resultado) {
					if(resultadoCobertura instanceof List){
						List ubicaciones = (List) resultadoCobertura;
						primaNeta = primaNeta.add((BigDecimal) ubicaciones.get(0));						
					}else{
						Object[] ubicaciones = (Object[]) resultadoCobertura;
						primaNeta = primaNeta.add((BigDecimal) ubicaciones[0]);		
					}
				}
			}
			return primaNeta;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("count failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Lista los incisos de una cotizacion que tengan las coberturas recibidas con la claveContrato recibido.
	 * @param BigDecimal idToCotizacion @param List<CoberturaCotizacionDTO> Lista de coberturas con la claveContrato recibida
	 * @param claveContrato
	 * @return List<IncisoCotizacionDTO> entidades IncisoCotizacionDTO encontradas pro el query
	 */
	@SuppressWarnings("unchecked")
	public List<IncisoCotizacionDTO> listarIncisosPorCoberturaContratada(BigDecimal idToCotizacion, List<CoberturaCotizacionDTO> coberturas,Short claveContrato) {
		try {
			String queryString = "select model from IncisoCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion " +
					"and (model.id.numeroInciso in (select distinct coberturaCot.id.numeroInciso  from CoberturaCotizacionDTO coberturaCot " +
					"where coberturaCot.id.idToCotizacion = :idToCotizacion and coberturaCot.claveContrato = :claveContrato and " +
					"coberturaCot.id.idToCobertura in ("+ obtenerIdCobertura(coberturas)+ "))) ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("claveContrato", claveContrato);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public String obtenerIdCobertura(List<CoberturaCotizacionDTO> coberturas) {
		StringBuilder ids = new StringBuilder("");
		if (coberturas.isEmpty()) return ids.toString();
		for(CoberturaCotizacionDTO coberturaCot : coberturas){
			ids.append(coberturaCot.getId().getIdToCobertura()).append(",");
		}
		return ids.substring(0, ids.length()-1);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<String, String> agregarInciso(CotizacionDTO cotizacionDTO,BigDecimal idToDireccion,
			BigDecimal numeroInciso,List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso,
				String[] datos,List<RecargoVarioDTO> recargosEspeciales,List<DescuentoDTO> descuentosEspeciales) {
		Map<String, String> mensaje = new HashMap<String, String>();
		try {
			super.agregarIncisoCotizacion(cotizacionDTO, idToDireccion, numeroInciso, datosInciso, datos,recargosEspeciales, descuentosEspeciales);
			this.entityManager.flush();
			mensaje.put("tipoMensaje", "30");//Exito
			mensaje.put("mensaje", "La informaci&oacute;n se registr&oacute; exitosamente.");	
			return mensaje;
		} catch(RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("agregarInciso failed", Level.SEVERE, re);
			mensaje.put("tipoMensaje", "10");//Error
			mensaje.put("mensaje", "Error al crear Inciso");
			return mensaje;
		}
	}

	public IncisoCotizacionDTO actualizarIncisoCotizacion(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idToDireccion,
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso, String[] datos){
		
		try {
			IncisoCotizacionDTO incisoCotizacionDTO = findById(new IncisoCotizacionId(idToCotizacion,numeroInciso));
			
			return super.actualizarIncisoCotizacion(incisoCotizacionDTO, idToDireccion, datosInciso, datos);
			
		} catch(RuntimeException re) {
			LogDeMidasEJB3.log("actualizarInciso failed", Level.SEVERE, re);
			throw re;
		}
	}
	/**
	 * Actualiza la secuencia en los incisos
	 * @param incisosDTO lista de incisos a actualizar su secuencia
	 * @author martin
	 */
	@Override
	public void actualizaNumeroSecuencia(BigDecimal idToCotizacion, Long numSecuencia) {
		List<IncisoCotizacionDTO> incisosDTO = findByCotizacionId(idToCotizacion);
		Iterator<IncisoCotizacionDTO> i = incisosDTO.iterator();
		ArrayList<Long> arrayNumeroSecuencia = new ArrayList<Long>();
		// Mete los numeros a un array list		
		while(i.hasNext()){
			IncisoCotizacionDTO obj = (IncisoCotizacionDTO) i.next();
			arrayNumeroSecuencia.add(obj.getNumeroSecuencia());
		}
		// Actualiza el numero de secuencia
		for(int x = (numSecuencia.intValue() - 1); x< incisosDTO.size(); x++){
			if(arrayNumeroSecuencia.get(x) - 1 == 0){
				break;
			}else{
				arrayNumeroSecuencia.set(x, arrayNumeroSecuencia.get(x) - 1 );
			}
		}
		// Actualiza los objetos
		int x = 0;
		try{
			Iterator<IncisoCotizacionDTO> b = incisosDTO.iterator();
			while(b.hasNext()){
				IncisoCotizacionDTO obj = (IncisoCotizacionDTO) b.next();
				obj.setNumeroSecuencia(arrayNumeroSecuencia.get(x));
				update(obj);
				x++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<String, String> borrarInciso(IncisoCotizacionDTO incisoCotizacionDTO) {
		Map<String, String> mensaje = new HashMap<String, String>();
		try {
			super.borrarIncisoCotizacion(incisoCotizacionDTO);
			mensaje.put("tipoMensaje", "30");//Exito
			mensaje.put("mensaje", "El inciso se elimin&oacute; exitosamente.");	
			return mensaje;
		} catch(RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("borrarInciso failed", Level.SEVERE, re);
			mensaje.put("tipoMensaje", "10");//Error
			mensaje.put("mensaje", "Error al borrar Inciso");
			return mensaje;
		}
	}
	
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Boolean copiarInciso(IncisoCotizacionDTO incisoNuevo, IncisoCotizacionDTO incisoOriginal, CotizacionDTO cotizacionDTO,
			DireccionDTO direccionNueva,List<DatoIncisoCotizacionDTO> listaDatosInciso) {
		try {
			super.copiarIncisoCotizacion(incisoNuevo, incisoOriginal, cotizacionDTO, direccionNueva, listaDatosInciso);
			return true;
		} catch(RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("copiarInciso failed", Level.SEVERE, re);
			return false;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public BigDecimal copiarInciso(BigDecimal idToCotizacion, BigDecimal numeroIncisoBase, BigDecimal idToDireccion) {
		try {
			BigDecimal numeroInciso = this.maxIncisos(idToCotizacion).add(BigDecimal.ONE);
			String queryString = copiarIncisos(idToCotizacion, numeroInciso, numeroIncisoBase, idToDireccion);
			Query query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();				
			LogDeMidasEJB3.log("se copiaron los incisos", Level.INFO, null);
			
			queryString = copiarSecciones(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();	
			LogDeMidasEJB3.log("se copiaron las secciones", Level.INFO, null);
			
			queryString = copiarCoberturas(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();	
			LogDeMidasEJB3.log("se copiaron las coberturas", Level.INFO, null);
			
			queryString = copiarRiesgos(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();			
			LogDeMidasEJB3.log("se copiaron los riesgos", Level.INFO, null);
			
			queryString = copiarSubIncisos(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();	
			LogDeMidasEJB3.log("se copiaron los subincisos", Level.INFO, null);
			
			queryString = copiarDetallePrimaCobertura(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
			LogDeMidasEJB3.log("se copiaron los detalles de prima cobertura", Level.INFO, null);
			
			queryString = copiarDetallePrimaRiesgo(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();		
			LogDeMidasEJB3.log("se copiaron los detalles de prima riesgo", Level.INFO, null);
	
			queryString = copiarDatosIncisos(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();		
			LogDeMidasEJB3.log("se copiaron los datos de inciso", Level.INFO, null);
			
			queryString = copiarAumentos(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();			
			LogDeMidasEJB3.log("se copiaron los aumentos", Level.INFO, null);
	
			queryString = copiarRecargos(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
			LogDeMidasEJB3.log("se copiaron los recargos", Level.INFO, null);
	
			queryString = copiarDescuentos(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
			LogDeMidasEJB3.log("se copiaron los descuentos", Level.INFO, null);
			return numeroInciso;
		} catch(RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("copiarInciso failed", Level.SEVERE, re);
			return null;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<String, String> separarInciso(CotizacionDTO cotizacionDTO, BigDecimal numeroInciso) {
		Map<String, String> mensaje = new HashMap<String, String>();
		try {
			super.separarIncisoCotizacion(cotizacionDTO, numeroInciso);
			mensaje.put("tipoMensaje", "30");//Exito
			mensaje.put("mensaje", "El inciso " + numeroInciso.intValue() + " ha sido separado de la cotizaci&oacute;n COT-" + String.format("%08d", cotizacionDTO.getIdToCotizacion().intValue()));	
			return mensaje;
		} catch(RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("separarInciso failed", Level.SEVERE, re);
			mensaje.put("tipoMensaje", "10");//Error
			mensaje.put("mensaje", "Error al separar Inciso");
			return mensaje;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<IncisoCotizacionDTO> obtenerIncisosPorIdCotizacionDireccion(BigDecimal idToCotizacion, BigDecimal idToDireccionInciso){
		try{
			String queryString = "select model from IncisoCotizacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToCotizacion",idToCotizacion);
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "direccionDTO.idToDireccion", idToDireccionInciso);
			
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("count failed", Level.SEVERE, re);
			throw re;
		}
		
	}
	
	/**
	 * Lista los incisos de una cotizacion que tengan al menos una cobertura contratada con el subramo especificado. 
	 * @param idToCotizacion, idTcSubRamo
	 * @return List<IncisoCotizacionDTO> entidades IncisoCotizacionDTO encontradas por el query.
	 */
	@SuppressWarnings("unchecked")
	public List<IncisoCotizacionDTO> listarIncisosPorSubRamo(BigDecimal idToCotizacion, BigDecimal idTcSubRamo) {
		try {
			String queryString = "select distinct c.seccionCotizacionDTO.incisoCotizacionDTO " +
					             "from CoberturaCotizacionDTO c " +
					             "where c.id.idToCotizacion = :idToCotizacion " +
					             "  and c.idTcSubramo = :idTcSubRamo" +
					             "  and c.claveContrato = 1" +
					             "  and c.seccionCotizacionDTO.claveContrato = 1";
							
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("idTcSubRamo", idTcSubRamo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarIncisosPorSubRamo failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Lista los incisos dados de baja en una cotizacion de endoso. 
	 * @param idToCotizacionEndoso
	 * @return List<IncisoCotizacionDTO> entidades IncisoCotizacionDTO encontradas por el query.
	 */
	@SuppressWarnings("unchecked")
	public List<IncisoCotizacionDTO> listarBajaIncisos(BigDecimal idToCotizacionEndosoActual, BigDecimal idToCotizacionEndosoAnterior) {		
		try {
			String queryString = "select inc " +
					             "from MovimientoCotizacionEndosoDTO mov, IncisoCotizacionDTO inc " +
					             "where mov.idToCotizacion = :idToCotizacionEndosoActual " +
					             "  and mov.claveTipoMovimiento = 8 " +
					             "  and inc.id.idToCotizacion = :idToCotizacionEndosoAnterior " +
					             "  and inc.id.numeroInciso = mov.numeroInciso " +
					             "order by inc.id.numeroInciso";			 
							
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacionEndosoActual", idToCotizacionEndosoActual);
			query.setParameter("idToCotizacionEndosoAnterior", idToCotizacionEndosoAnterior);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarBajaIncisos failed", Level.SEVERE, re);
			throw re;
		}
	}

	
	/**
	 * Perform an initial save of a previously unsaved IncisoCotizacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            IncisoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public IncisoCotizacionId saveAndGetId(IncisoCotizacionDTO entity){
		LogDeMidasEJB3.log("saving IncisoCotizacionDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return (IncisoCotizacionId)entityManager.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}
	/**
	 * Obtiene el numero de secuencia mas grande de los incisos de una 
	 * cotizacion
	 * @param idtoCotizacion id de la cotizacion
	 * @return BigDecimal numeroSecuenciaMax
	 * @author martin
	 */
	@Override
	public Long maxSecuencia(BigDecimal idToCotizacion) {
		try {
			StringBuffer queryString = new StringBuffer("SELECT MAX(incisos.numeroSecuencia) FROM IncisoCotizacionDTO incisos WHERE incisos.id.idToCotizacion = :idToCotizacion");
//			queryString.append(" AND incisos.id.numeroInciso IN (SELECT DISTINCT seccion.id.numeroInciso FROM SeccionCotizacionDTO seccion" + 
//								" WHERE seccion.seccionDTO.idToSeccion = :idToSeccion" +
//								" AND seccion.id.idToCotizacion = :idToCotizacion )");

			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("idToCotizacion", idToCotizacion);
//			query.setParameter("idToSeccion", idToSeccion);
			Long numeroSecuenciaMax = (Long) query.getSingleResult();
			if (numeroSecuenciaMax == null)
				numeroSecuenciaMax = new Long("0");
			return numeroSecuenciaMax;
		} catch (RuntimeException re) {
			re.printStackTrace();
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<IncisoCotizacionDTO> getIncisosPorLineaPaquete(
			BigDecimal cotizacionId, BigDecimal lineaId, Long paqueteId) {
			StringBuilder stm = new StringBuilder("");
			stm.append("select distinct  model.incisoCotizacionDTO from SeccionCotizacionDTO model");
			stm.append(" WHERE model.id.idToCotizacion=:cotizacionId");
			stm.append(" AND  model.incisoCotizacionDTO.incisoAutoCot.paquete.id=:paqueteId");
		    stm.append(" AND  model.id.idToSeccion=:lineaId");

			Query query = entityManager.createQuery(stm.toString());
			query.setParameter("cotizacionId", cotizacionId);
			query.setParameter("paqueteId", paqueteId);
			query.setParameter("lineaId", lineaId);
			query.setHint(QueryHints.REFRESH, HintValues.FALSE);
			query.setHint(QueryHints.CACHE_RETRIEVE_MODE, HintValues.TRUE);
			return query.getResultList();	
			
	}

	@Override
	public List<IncisoCotizacionDTO> getIncisosPorLinea(
			BigDecimal cotizacionId, BigDecimal lineaId) {
		try{
			
			String queryString = "select model.incisoCotizacionDTO from SeccionCotizacionDTO model ";
			String sWhere = "";
			Query query;
			@SuppressWarnings("rawtypes")
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToCotizacion",cotizacionId);
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToSeccion",lineaId);
			
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("count failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public SeccionCotizacionDTO getSeccionCotizacionDTO(
			BigDecimal idToCotizacion, BigDecimal idInciso) {
		String queryString = "select model.seccionCotizacionList from IncisoCotizacionDTO model where " + 
		" model.id.idToCotizacion = :idToCotizacion and model.id.numeroInciso = :numInciso ";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCotizacion", idToCotizacion);
		query.setParameter("numInciso", idInciso);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		List<SeccionCotizacionDTO> list = query.getResultList();		
		return list.isEmpty()?null:list.get(0);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public BigDecimal copiarInciso(BigDecimal idToCotizacion, BigDecimal numeroIncisoBase, BigDecimal numeroInciso, BigDecimal idToDireccion) {
		try {
			String queryString = copiarIncisosAll(idToCotizacion, numeroInciso, numeroIncisoBase, idToDireccion);
			Query query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();				
			LogDeMidasEJB3.log("se copiaron los incisos", Level.INFO, null);
			
			queryString = copiarSecciones(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();	
			LogDeMidasEJB3.log("se copiaron las secciones", Level.INFO, null);
			
			queryString = copiarIncisosAuto(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();	
			LogDeMidasEJB3.log("se copiaron los auto inciso", Level.INFO, null);
			
			queryString = copiarCoberturasAll(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();	
			LogDeMidasEJB3.log("se copiaron las coberturas", Level.INFO, null);
			
			queryString = copiarRiesgos(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();			
			LogDeMidasEJB3.log("se copiaron los riesgos", Level.INFO, null);
			
			queryString = copiarSubIncisos(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();	
			LogDeMidasEJB3.log("se copiaron los subincisos", Level.INFO, null);
			
			queryString = copiarDetallePrimaCobertura(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
			LogDeMidasEJB3.log("se copiaron los detalles de prima cobertura", Level.INFO, null);
			
			queryString = copiarDetallePrimaRiesgo(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();		
			LogDeMidasEJB3.log("se copiaron los detalles de prima riesgo", Level.INFO, null);
	
			queryString = copiarDatosIncisos(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();		
			LogDeMidasEJB3.log("se copiaron los datos de inciso", Level.INFO, null);
			
			queryString = copiarDatosIncisosAuto(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();		
			LogDeMidasEJB3.log("se copiaron los datos de inciso auto", Level.INFO, null);			
			
			queryString = copiarAumentos(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();			
			LogDeMidasEJB3.log("se copiaron los aumentos", Level.INFO, null);
	
			queryString = copiarRecargos(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
			LogDeMidasEJB3.log("se copiaron los recargos", Level.INFO, null);
	
			queryString = copiarDescuentos(idToCotizacion, numeroInciso, numeroIncisoBase);
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
			LogDeMidasEJB3.log("se copiaron los descuentos", Level.INFO, null);
			return numeroInciso;
		} catch(RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("copiarInciso failed", Level.SEVERE, re);
			return null;
		}
	}
}