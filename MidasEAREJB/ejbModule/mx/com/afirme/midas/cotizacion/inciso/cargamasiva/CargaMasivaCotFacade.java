package mx.com.afirme.midas.cotizacion.inciso.cargamasiva;

// default package

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

import org.apache.commons.lang.StringUtils;

/**
 * Facade for entity CargaMasivaCotDTO.
 * 
 * @see .CargaMasivaCotDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CargaMasivaCotFacade implements CargaMasivaCotFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	@EJB
	private CargaMasivaDetalleCotFacadeRemote cargaMasivaDetalleCotFacadeRemote;

	@EJB
	private ColoniaFacadeRemote coloniaFacadeRemote;

	@EJB
	private EstadoFacadeRemote estadoFacadeRemote;

	@EJB
	private CiudadFacadeRemote ciudadFacadeRemote;

	public static final Short ESTATUS_CORRECTO = 1;

	public static final Short ESTATUS_INCORRECTO = 0;

	/**
	 * Perform an initial save of a previously unsaved CargaMasivaCotDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            CargaMasivaCotDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CargaMasivaCotDTO entity) {
		LogDeMidasEJB3.log("saving CargaMasivaCotDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent CargaMasivaCotDTO entity.
	 * 
	 * @param entity
	 *            CargaMasivaCotDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CargaMasivaCotDTO entity) {
		LogDeMidasEJB3.log("deleting CargaMasivaCotDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(CargaMasivaCotDTO.class, entity
					.getIdToCotizacion());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved CargaMasivaCotDTO entity and return it or a
	 * copy of it to the sender. A copy of the CargaMasivaCotDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CargaMasivaCotDTO entity to update
	 * @return CargaMasivaCotDTO the persisted CargaMasivaCotDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CargaMasivaCotDTO update(CargaMasivaCotDTO entity) {
		LogDeMidasEJB3.log("updating CargaMasivaCotDTO instance", Level.INFO,
				null);
		try {
			CargaMasivaCotDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CargaMasivaCotDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding CargaMasivaCotDTO instance with id: " + id,
				Level.INFO, null);
		try {
			CargaMasivaCotDTO instance = entityManager.find(
					CargaMasivaCotDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CargaMasivaCotDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CargaMasivaCotDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CargaMasivaCotDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CargaMasivaCotDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding CargaMasivaCotDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from CargaMasivaCotDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CargaMasivaCotDTO entities.
	 * 
	 * @return List<CargaMasivaCotDTO> all CargaMasivaCotDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CargaMasivaCotDTO> findAll() {
		LogDeMidasEJB3.log("finding all CargaMasivaCotDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from CargaMasivaCotDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CargaMasivaCotDTO agregar(CargaMasivaCotDTO cargaMasivaCotDTO,
			List<DireccionDTO> direccionesValidas,
			List<DireccionDTO> direccionesInvalidas) {
		LogDeMidasEJB3.log("agregando instancia de CargaMasivaCotDTO",
				Level.INFO, null);
		try {
			entityManager.persist(cargaMasivaCotDTO);
			cargaMasivaCotDTO = entityManager.getReference(
					CargaMasivaCotDTO.class, cargaMasivaCotDTO
							.getIdToCotizacion());
			this.insertarDetalleCargaMasiva(cargaMasivaCotDTO,
					direccionesValidas, true);
			this.insertarDetalleCargaMasiva(cargaMasivaCotDTO,
					direccionesInvalidas, false);

			return cargaMasivaCotDTO;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	public void insertarDetalleCargaMasiva(CargaMasivaCotDTO cargaMasivaCotDTO,
			List<DireccionDTO> direcciones, boolean direccionesValidas) {
		List<ColoniaDTO> result = null;
		ColoniaDTO coloniaEncontrada = null;

		for (DireccionDTO direccion : direcciones) {
			// Insertar lo que venga
			CargaMasivaDetalleCotDTO cargaMasivaDetalleCotDTO;
			if (direccionesValidas) {
				cargaMasivaDetalleCotDTO = this.insertaDetalleCargaMasiva(
						cargaMasivaCotDTO, direccion, ESTATUS_CORRECTO);
				if (direccion.getCodigoPostal() != null
						&& direccion.getIdMunicipio() != null
						&& direccion.getNombreColonia() != null) {
					result = coloniaFacadeRemote.findByProperty("colonyId",
							direccion.getNombreColonia().trim());
				}
				if (result != null && result.size() > 0)
					coloniaEncontrada = result.get(0);
			} else {

				cargaMasivaDetalleCotDTO = this.insertaDetalleCargaMasiva(
						cargaMasivaCotDTO, direccion, ESTATUS_INCORRECTO);
				if (direccion.getCodigoPostal() != null) {
					result = coloniaFacadeRemote
							.getColonyByZipCode(new DecimalFormat("00000.####")
									.format(direccion.getCodigoPostal()));
				}else if (direccion.getNombreColonia() != null) {
					ColoniaDTO coloniaDTO = new ColoniaDTO();
					coloniaDTO.setColonyName(direccion.getNombreColonia());
					result = coloniaFacadeRemote.listarFiltrado(coloniaDTO);
				}
					
				if (result != null && result.size() > 0) {
					coloniaEncontrada = this.buscaColoniaCoincidente(direccion
							.getNombreColonia(), result);
				}
			}

			// Se actualiza el registro en base a lo que se encontro
			if (coloniaEncontrada != null) {
				if (direccionesValidas)
					cargaMasivaDetalleCotDTO
							.setNombreColoniaOriginal(coloniaEncontrada
									.getColonyName());

				cargaMasivaDetalleCotDTO.setIdColonia(coloniaEncontrada
						.getColonyId());
				cargaMasivaDetalleCotDTO
						.setNombreColoniaValida(coloniaEncontrada
								.getColonyName());
				CiudadDTO ciudad = ciudadFacadeRemote.findById(obtenerId(coloniaEncontrada.getCityId().trim()));
				EstadoDTO estado = estadoFacadeRemote.findById(obtenerId(ciudad.getStateId().trim()));
						cargaMasivaDetalleCotDTO.setNombreMunicipioValido(ciudad
						.getCityName());
				cargaMasivaDetalleCotDTO.setNombreEstadoValido(estado
						.getStateName());
				if(cargaMasivaDetalleCotDTO.getCodigoPostal() == null)
					cargaMasivaDetalleCotDTO.setCodigoPostal(coloniaEncontrada.getZipCode().trim());
				if(cargaMasivaDetalleCotDTO.getNombreEstadoOriginal() == null)
					cargaMasivaDetalleCotDTO.setNombreEstadoOriginal(estado.getStateName());
				if(cargaMasivaDetalleCotDTO.getNombreMunicipioOriginal() == null)
					cargaMasivaDetalleCotDTO.setNombreMunicipioOriginal(ciudad.getCityName());
				cargaMasivaDetalleCotFacadeRemote
						.update(cargaMasivaDetalleCotDTO);
			}

		}

	}
	public String obtenerId(String id) {
		return StringUtils.leftPad(id, 5, '0');	
	}
	private CargaMasivaDetalleCotDTO insertaDetalleCargaMasiva(
			CargaMasivaCotDTO cargaMasivaCotDTO, DireccionDTO detalle,
			Short claveEstatus) {

		CargaMasivaDetalleCotDTO entity = new CargaMasivaDetalleCotDTO();
		CargaMasivaDetalleCotId id = new CargaMasivaDetalleCotId(
				cargaMasivaCotDTO.getIdToCotizacion(), this
						.getNumeroInciso(cargaMasivaCotDTO.getIdToCotizacion()));

		entity.setId(id);
		entity.setClaveEstatus(claveEstatus);
		if (detalle.getCodigoPostal() != null)
			entity.setCodigoPostal(new DecimalFormat("00000.####")
					.format(detalle.getCodigoPostal()));
		entity.setMensajeError(detalle.getMensajeError());
		entity.setNombreCalle(detalle.getNombreCalle());
		entity.setNombreColoniaOriginal(detalle.getNombreColonia());
		if (detalle.getIdMunicipio() != null) {
			CiudadDTO ciudad = ciudadFacadeRemote.findById(new DecimalFormat(
					"00000.####").format(detalle.getIdMunicipio()));

			if (ciudad != null) {
				EstadoDTO estado = estadoFacadeRemote.findById(ciudad
						.getStateId());
				entity.setNombreEstadoOriginal(estado.getStateName());
				entity.setNombreMunicipioOriginal(ciudad.getCityName());
			}
		}
		entity.setNumeroExterior(detalle.getNumeroExterior());
		entity.setNumeroInterior(detalle.getNumeroInterior());
		entity.setSumaAseguradaSecRgo(detalle.getSumasAseguradas());

		cargaMasivaDetalleCotFacadeRemote.save(entity);

		return cargaMasivaDetalleCotFacadeRemote.findById(entity.getId());
	}

	private ColoniaDTO buscaColoniaCoincidente(String coloniaABuscar,
			List<ColoniaDTO> coincidencias) {
		ColoniaDTO coincidencia = null;
		for (ColoniaDTO colonia : coincidencias) {
			if (coloniaABuscar == null) {
				coincidencia = colonia;
				break;
			} else if (coloniaABuscar.toUpperCase().equals(
					colonia.getColonyName())) {
				coincidencia = colonia;
				break;
			} else {
				colonia.setNoDiferencias(StringUtils.getLevenshteinDistance(
						coloniaABuscar, colonia.getColonyName()));
			}
		}
		if (coincidencia == null) {
			Collections.sort(coincidencias);
			coincidencia = coincidencias.get(0);
		}
		return coincidencia;
	}

	private BigDecimal getNumeroInciso(BigDecimal idToCotizacion) {
		BigDecimal numeroInciso = BigDecimal.ONE;
		String queryString = "";
		LogDeMidasEJB3.log("getNumeroInciso cotizacion: " + idToCotizacion,
				Level.INFO, null);
		try {
			queryString = "select max(model.id.numeroInciso) from CargaMasivaDetalleCotDTO model where model.id.idToCotizacion"
					+ "= :idToCotizacion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			BigDecimal numInciso = (BigDecimal) query.getSingleResult();

			if (numInciso == null) {
				queryString = "select max(incisos.id.numeroInciso) from IncisoCotizacionDTO incisos where incisos.id.idToCotizacion = :idToCotizacion";
				query = entityManager.createQuery(queryString);
				query.setParameter("idToCotizacion", idToCotizacion);
				numInciso = (BigDecimal) query.getSingleResult();
			}

			if (numInciso != null) {
				numeroInciso = numInciso.add(BigDecimal.ONE);
			}
			return numeroInciso;
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CargaMasivaCotDTO getCargaMasiva(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("getCargaMasiva cotizacion: " + idToCotizacion,
				Level.INFO, null);
		try {
			CargaMasivaCotDTO cargaMasivaCotDTO = this.findById(idToCotizacion);
			if (cargaMasivaCotDTO == null)
				return null;
			try {
				if (cargaMasivaCotDTO != null) {
					entityManager.refresh(cargaMasivaCotDTO);
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			System.out.println(cargaMasivaCotDTO.getCargaMasivaDetalleCotDTO()
					.size());
			return cargaMasivaCotDTO;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("getCargaMasiva failed", Level.SEVERE, re);
			throw re;
		}

	}

	public void actualizaDetalle(CargaMasivaDetalleCotDTO detalle) {
		try {
			if (detalle != null && detalle.getId() != null) {
				LogDeMidasEJB3.log("actualizaDetalle cotizacion: "
						+ detalle.getId().getIdToCotizacion()
						+ " numero Inciso: "
						+ detalle.getId().getNumeroInciso(), Level.INFO, null);
				CargaMasivaDetalleCotDTO temp = cargaMasivaDetalleCotFacadeRemote
						.findById(detalle.getId());
				if (temp == null)
					return;
				if (detalle.getIdColonia() != null) {
					List<ColoniaDTO> result = coloniaFacadeRemote
							.findByProperty("colonyId", detalle.getIdColonia());
					if (result == null || result.size() == 0) {
						if (detalle.getIdColonia().length() > 8) {
							if (detalle.getCodigoPostal() != null) {
								result = coloniaFacadeRemote.findByProperty(
										"zipCode", detalle.getCodigoPostal());
								if (result != null && result.size() > 0) {
									ColoniaDTO coloniaEncontrada = this
											.buscaColoniaCoincidente(detalle
													.getIdColonia(), result);
									CiudadDTO ciudad = ciudadFacadeRemote
											.findById(obtenerId(coloniaEncontrada.getCityId().trim()));
									EstadoDTO estado = estadoFacadeRemote
											.findById(obtenerId(ciudad.getStateId().trim()));
							    	temp.setIdColonia(null);
									temp.setNombreEstadoValido(estado
											.getStateName());
									temp.setNombreMunicipioValido(ciudad
											.getCityName());
									temp.setClaveEstatus(ESTATUS_INCORRECTO);
									temp
											.setMensajeError("La colonia no corresponde al Codigo Postal");
								} else {
									temp.setClaveEstatus(ESTATUS_INCORRECTO);
									temp.setMensajeError("Colonia incorrecta");
								}
							}
						} else {
							temp.setMensajeError("Colonia incorrecta");
							temp.setIdColonia(null);
							temp.setClaveEstatus(ESTATUS_INCORRECTO);
						}
					} else {
						ColoniaDTO colonia = result.get(0);
						if (colonia != null) {
							CiudadDTO ciudad = ciudadFacadeRemote
									.findById(obtenerId(colonia.getCityId().trim()));
							EstadoDTO estado = estadoFacadeRemote
									.findById(obtenerId(ciudad.getStateId().trim()));
							if (!detalle.getCodigoPostal().equals(
									colonia.getZipCode())) {
								temp.setNombreEstadoValido(estado
										.getStateName());
								temp.setNombreMunicipioValido(ciudad
										.getCityName());
								temp.setClaveEstatus(ESTATUS_INCORRECTO);
								temp
										.setMensajeError("La colonia no corresponde al Codigo Postal");
							} else {
								temp.setNombreEstadoValido(estado
										.getStateName());
								temp.setNombreMunicipioValido(ciudad
										.getCityName());
								temp.setNombreColoniaValida(colonia
										.getColonyName());
								temp.setClaveEstatus(ESTATUS_CORRECTO);
								temp.setMensajeError(null);
							}
						}
					}
				} else {
					temp.setMensajeError("Colonia incorrecta");
					temp.setIdColonia(null);
					temp.setClaveEstatus(ESTATUS_INCORRECTO);
				}
				temp.setNombreCalle(detalle.getNombreCalle());
				temp.setNumeroExterior(detalle.getNumeroExterior());
				temp.setNumeroInterior(detalle.getNumeroInterior());
				temp.setCodigoPostal(detalle.getCodigoPostal());
				cargaMasivaDetalleCotFacadeRemote.update(temp);
			} else {
				return;
			}

		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("actualizaDetalle failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DireccionDTO poblarDireccion(CargaMasivaDetalleCotDTO detalle) {
		DireccionDTO direccion = new DireccionDTO();
		LogDeMidasEJB3.log("poblarDireccion cotizacion: "
				+ detalle.getId().getNumeroInciso(), Level.INFO, null);
		try {
			List<ColoniaDTO> result = coloniaFacadeRemote.findByProperty(
					"colonyId", detalle.getIdColonia());
			if (result != null && result.size() > 0) {
				ColoniaDTO colonia = result.get(0);
				direccion.setCodigoPostal(new BigDecimal(detalle
						.getCodigoPostal()));
				direccion.setNombreCalle(detalle.getNombreCalle());
				direccion.setNombreColonia(colonia.getColonyId());
				direccion.setNumeroExterior(detalle.getNumeroExterior());
				direccion.setNumeroInterior(detalle.getNumeroInterior());
				CiudadDTO ciudad = ciudadFacadeRemote.findById(colonia
						.getCityId());
				EstadoDTO estado = estadoFacadeRemote.findById(ciudad
						.getStateId());
				direccion.setIdMunicipio(new BigDecimal(ciudad.getCityId()));
				direccion.setIdEstado(new BigDecimal(estado.getStateId()));
			}

		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("actualizaDetalle failed", Level.SEVERE, re);
			throw re;
		}
		return direccion;
	}
}