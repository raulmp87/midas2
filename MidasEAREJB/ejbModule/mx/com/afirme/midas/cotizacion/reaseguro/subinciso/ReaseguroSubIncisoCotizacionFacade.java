package mx.com.afirme.midas.cotizacion.reaseguro.subinciso;
// default package

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ReaseguroSubIncisoCotizacionDTO.
 * @see .ReaseguroSubIncisoCotizacionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ReaseguroSubIncisoCotizacionFacade implements ReaseguroSubIncisoCotizacionFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ReaseguroSubIncisoCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ReaseguroSubIncisoCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ReaseguroSubIncisoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("saving ReaseguroSubIncisoCotizacionDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ReaseguroSubIncisoCotizacionDTO entity.
	  @param entity ReaseguroSubIncisoCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ReaseguroSubIncisoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("deleting ReaseguroSubIncisoCotizacionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ReaseguroSubIncisoCotizacionDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ReaseguroSubIncisoCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the ReaseguroSubIncisoCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ReaseguroSubIncisoCotizacionDTO entity to update
	 @return ReaseguroSubIncisoCotizacionDTO the persisted ReaseguroSubIncisoCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ReaseguroSubIncisoCotizacionDTO update(ReaseguroSubIncisoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("updating ReaseguroSubIncisoCotizacionDTO instance", Level.INFO, null);
	        try {
            ReaseguroSubIncisoCotizacionDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ReaseguroSubIncisoCotizacionDTO findById( ReaseguroSubIncisoCotizacionId id) {
    				LogDeMidasEJB3.log("finding ReaseguroSubIncisoCotizacionDTO instance with id: " + id, Level.INFO, null);
	        try {
            ReaseguroSubIncisoCotizacionDTO instance = entityManager.find(ReaseguroSubIncisoCotizacionDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ReaseguroSubIncisoCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReaseguroSubIncisoCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<ReaseguroSubIncisoCotizacionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ReaseguroSubIncisoCotizacionDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ReaseguroSubIncisoCotizacionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ReaseguroSubIncisoCotizacionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ReaseguroSubIncisoCotizacionDTO entities.
	  	  @return List<ReaseguroSubIncisoCotizacionDTO> all ReaseguroSubIncisoCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguroSubIncisoCotizacionDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ReaseguroSubIncisoCotizacionDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ReaseguroSubIncisoCotizacionDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find all ReaseguroSubIncisoCotizacionDTO entities with depends of a filter. 
	 * @param entity the name of the entity ReaseguroSubIncisoCotizacionDTO
	 * @return ReaseguroSubIncisoCotizacionDTO found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguroSubIncisoCotizacionDTO> listarFiltrado(ReaseguroSubIncisoCotizacionDTO entity) {
		try {
			String queryString = "select model from ReaseguroSubIncisoCotizacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCotizacion", entity.getId().getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.numeroInciso", entity.getId().getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToSeccion", entity.getId().getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.numeroSubinciso", entity.getId().getNumeroSubinciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idTcSubramo", entity.getId().getIdTcSubramo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "valorSumaAsegurada", entity.getValorSumaAsegurada());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "valorPrimaNeta", entity.getValorPrimaNeta());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveTipoOrigen", entity.getClaveTipoOrigen());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveEstatus", entity.getClaveEstatus());
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarFiltrado failed", Level.SEVERE, re);
			throw re;
		}
	}
}