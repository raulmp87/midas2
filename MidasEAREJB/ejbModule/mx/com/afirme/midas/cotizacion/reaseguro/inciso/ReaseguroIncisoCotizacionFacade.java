package mx.com.afirme.midas.cotizacion.reaseguro.inciso;
// default package

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ReaseguroIncisoCotizacionDTO.
 * @see .ReaseguroIncisoCotizacionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ReaseguroIncisoCotizacionFacade  implements ReaseguroIncisoCotizacionFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ReaseguroIncisoCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ReaseguroIncisoCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ReaseguroIncisoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("saving ReaseguroIncisoCotizacionDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ReaseguroIncisoCotizacionDTO entity.
	  @param entity ReaseguroIncisoCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ReaseguroIncisoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("deleting ReaseguroIncisoCotizacionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ReaseguroIncisoCotizacionDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ReaseguroIncisoCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the ReaseguroIncisoCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ReaseguroIncisoCotizacionDTO entity to update
	 @return ReaseguroIncisoCotizacionDTO the persisted ReaseguroIncisoCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ReaseguroIncisoCotizacionDTO update(ReaseguroIncisoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("updating ReaseguroIncisoCotizacionDTO instance", Level.INFO, null);
	        try {
            ReaseguroIncisoCotizacionDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ReaseguroIncisoCotizacionDTO findById( ReaseguroIncisoCotizacionId id) {
    				LogDeMidasEJB3.log("finding ReaseguroIncisoCotizacionDTO instance with id: " + id, Level.INFO, null);
	        try {
            ReaseguroIncisoCotizacionDTO instance = entityManager.find(ReaseguroIncisoCotizacionDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ReaseguroIncisoCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReaseguroIncisoCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<ReaseguroIncisoCotizacionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ReaseguroIncisoCotizacionDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ReaseguroIncisoCotizacionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ReaseguroIncisoCotizacionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ReaseguroIncisoCotizacionDTO entities.
	  	  @return List<ReaseguroIncisoCotizacionDTO> all ReaseguroIncisoCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguroIncisoCotizacionDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ReaseguroIncisoCotizacionDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ReaseguroIncisoCotizacionDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find all ReaseguroIncisoCotizacionDTO entities with depends of a filter. 
	 * @param entity the name of the entity ReaseguroIncisoCotizacionDTO
	 * @return ReaseguroIncisoCotizacionDTO found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ReaseguroIncisoCotizacionDTO> listarFiltrado(ReaseguroIncisoCotizacionDTO entity) {
		try {
			String queryString = "select model from ReaseguroIncisoCotizacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idTcSubRamo", entity.getId().getIdTcSubRamo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCotizacion", entity.getId().getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.numeroInciso", entity.getId().getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "valorSumaAsegurada", entity.getValorSumaAsegurada());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "valorPrimaNeta", entity.getValorPrimaNeta());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveTipoOrigen", entity.getClaveTipoOrigen());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveEstatus", entity.getClaveEstatus());
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarFiltrado failed", Level.SEVERE, re);
			throw re;
		}
	}
}