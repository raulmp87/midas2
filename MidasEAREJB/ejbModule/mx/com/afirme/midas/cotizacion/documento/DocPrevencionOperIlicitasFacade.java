package mx.com.afirme.midas.cotizacion.documento;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.cotizacion.documento.DocPrevencionOperIlicitasDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity DocPrevencionOperIlicitasDTO.
 * @see .DocPrevencionOperIlicitasDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DocPrevencionOperIlicitasFacade  implements DocPrevencionOperIlicitasFacadeRemote {
	//property constants
	public static final String CLAVE_TIPO_DOCUMENTO = "claveTipoDocumento";





    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DocPrevencionOperIlicitasDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocPrevencionOperIlicitasDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocPrevencionOperIlicitasDTO entity) {
    				LogDeMidasEJB3.log("saving DocPrevencionOperIlicitasDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DocPrevencionOperIlicitasDTO entity.
	  @param entity DocPrevencionOperIlicitasDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocPrevencionOperIlicitasDTO entity) {
    				LogDeMidasEJB3.log("deleting DocPrevencionOperIlicitasDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DocPrevencionOperIlicitasDTO.class, entity.getIdToControlArchivo());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DocPrevencionOperIlicitasDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocPrevencionOperIlicitasDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocPrevencionOperIlicitasDTO entity to update
	 @return DocPrevencionOperIlicitasDTO the persisted DocPrevencionOperIlicitasDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DocPrevencionOperIlicitasDTO update(DocPrevencionOperIlicitasDTO entity) {
    				LogDeMidasEJB3.log("updating DocPrevencionOperIlicitasDTO instance", Level.INFO, null);
	        try {
            DocPrevencionOperIlicitasDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DocPrevencionOperIlicitasDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding DocPrevencionOperIlicitasDTO instance with id: " + id, Level.INFO, null);
	        try {
            DocPrevencionOperIlicitasDTO instance = entityManager.find(DocPrevencionOperIlicitasDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DocPrevencionOperIlicitasDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocPrevencionOperIlicitasDTO property to query
	  @param value the property value to match
	  	  @return List<DocPrevencionOperIlicitasDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DocPrevencionOperIlicitasDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding DocPrevencionOperIlicitasDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DocPrevencionOperIlicitasDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	public List<DocPrevencionOperIlicitasDTO> findByClaveTipoDocumento(Object claveTipoDocumento
	) {
		return findByProperty(CLAVE_TIPO_DOCUMENTO, claveTipoDocumento
		);
	}
	
	
	/**
	 * Find all DocPrevencionOperIlicitasDTO entities.
	  	  @return List<DocPrevencionOperIlicitasDTO> all DocPrevencionOperIlicitasDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DocPrevencionOperIlicitasDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all DocPrevencionOperIlicitasDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from DocPrevencionOperIlicitasDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}