package mx.com.afirme.midas.cotizacion.documento;


import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoFacadeRemote;
import mx.com.afirme.midas.usuario.LogUtil;
import mx.com.afirme.midas2.util.VaultService;

/**
 * Facade for entity DocumentoAnexoReaseguroCotizacionDTO.
 * @see .DocumentoAnexoReaseguroCotizacionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DocumentoAnexoReaseguroCotizacionFacade  implements DocumentoAnexoReaseguroCotizacionFacadeRemote {
	//property constants
	public static final String DESCRIPCION_DOCUMENTO_ANEXO = "descripcionDocumentoAnexo";
	public static final String CODIGO_USUARIO_CREACION = "codigoUsuarioCreacion";
	public static final String NOMBRE_USUARIO_CREACION = "nombreUsuarioCreacion";
	public static final String CODIGO_USUARIO_MODIFICACION = "codigoUsuarioModificacion";
	public static final String NOMBRE_USUARIO_MODIFICACION = "nombreUsuarioModificacion";

	ControlArchivoFacadeRemote controlArchivoFacadeRemote;
	VaultService vaultService;



    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DocumentoAnexoReaseguroCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocumentoAnexoReaseguroCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocumentoAnexoReaseguroCotizacionDTO entity) {
    				LogUtil.log("saving DocumentoAnexoReaseguroCotizacionDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DocumentoAnexoReaseguroCotizacionDTO entity.
	  @param entity DocumentoAnexoReaseguroCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocumentoAnexoReaseguroCotizacionDTO entity) {
    				LogUtil.log("deleting DocumentoAnexoReaseguroCotizacionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DocumentoAnexoReaseguroCotizacionDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DocumentoAnexoReaseguroCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocumentoAnexoReaseguroCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocumentoAnexoReaseguroCotizacionDTO entity to update
	 @return DocumentoAnexoReaseguroCotizacionDTO the persisted DocumentoAnexoReaseguroCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DocumentoAnexoReaseguroCotizacionDTO update(DocumentoAnexoReaseguroCotizacionDTO entity) {
    				LogUtil.log("updating DocumentoAnexoReaseguroCotizacionDTO instance", Level.INFO, null);
	        try {
            DocumentoAnexoReaseguroCotizacionDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DocumentoAnexoReaseguroCotizacionDTO findById( DocumentoAnexoReaseguroCotizacionId id) {
    				LogUtil.log("finding DocumentoAnexoReaseguroCotizacionDTO instance with id: " + id, Level.INFO, null);
	        try {
            DocumentoAnexoReaseguroCotizacionDTO instance = entityManager.find(DocumentoAnexoReaseguroCotizacionDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DocumentoAnexoReaseguroCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocumentoAnexoReaseguroCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<DocumentoAnexoReaseguroCotizacionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DocumentoAnexoReaseguroCotizacionDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding DocumentoAnexoReaseguroCotizacionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DocumentoAnexoReaseguroCotizacionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	public List<DocumentoAnexoReaseguroCotizacionDTO> findByDescripcionDocumentoAnexo(Object descripcionDocumentoAnexo
	) {
		return findByProperty(DESCRIPCION_DOCUMENTO_ANEXO, descripcionDocumentoAnexo
		);
	}
	
	public List<DocumentoAnexoReaseguroCotizacionDTO> findByCodigoUsuarioCreacion(Object codigoUsuarioCreacion
	) {
		return findByProperty(CODIGO_USUARIO_CREACION, codigoUsuarioCreacion
		);
	}
	
	public List<DocumentoAnexoReaseguroCotizacionDTO> findByNombreUsuarioCreacion(Object nombreUsuarioCreacion
	) {
		return findByProperty(NOMBRE_USUARIO_CREACION, nombreUsuarioCreacion
		);
	}
	
	public List<DocumentoAnexoReaseguroCotizacionDTO> findByCodigoUsuarioModificacion(Object codigoUsuarioModificacion
	) {
		return findByProperty(CODIGO_USUARIO_MODIFICACION, codigoUsuarioModificacion
		);
	}
	
	public List<DocumentoAnexoReaseguroCotizacionDTO> findByNombreUsuarioModificacion(Object nombreUsuarioModificacion
	) {
		return findByProperty(NOMBRE_USUARIO_MODIFICACION, nombreUsuarioModificacion
		);
	}
	
	
	/**
	 * Find all DocumentoAnexoReaseguroCotizacionDTO entities.
	  	  @return List<DocumentoAnexoReaseguroCotizacionDTO> all DocumentoAnexoReaseguroCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DocumentoAnexoReaseguroCotizacionDTO> findAll(
		) {
					LogUtil.log("finding all DocumentoAnexoReaseguroCotizacionDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from DocumentoAnexoReaseguroCotizacionDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@Override
	public void copiarDocumentos(BigDecimal idCotizacionBase, BigDecimal idCotizacionCopia) {
		List<DocumentoAnexoReaseguroCotizacionDTO> documentosBase = this
				.findByProperty("cotizacionDTO.idToCotizacion",
						idCotizacionBase);
		DocumentoAnexoReaseguroCotizacionDTO documento = null;
		CotizacionDTO cotizacionCopia = entityManager.find(CotizacionDTO.class,
				idCotizacionCopia);
		for (DocumentoAnexoReaseguroCotizacionDTO item : documentosBase) {
			documento = new DocumentoAnexoReaseguroCotizacionDTO();
			documento.setCodigoUsuarioCreacion(item.getCodigoUsuarioCreacion());
			documento.setCodigoUsuarioModificacion(item
					.getCodigoUsuarioModificacion());
			documento.setCotizacionDTO(cotizacionCopia);
			documento.setFechaCreacion(item.getFechaCreacion());
			documento.setFechaModificacion(item.getFechaModificacion());
			documento.setNombreUsuarioCreacion(item.getNombreUsuarioCreacion());
			documento.setNombreUsuarioModificacion(item
					.getNombreUsuarioModificacion());

			ControlArchivoDTO controlArchivoBase = controlArchivoFacadeRemote
					.findById(item.getId().getIdToControlArchivo());
			ControlArchivoDTO controlArchivoNuevo = new ControlArchivoDTO();
			controlArchivoNuevo.setClaveTipo(controlArchivoBase.getClaveTipo());
			controlArchivoNuevo.setNombreArchivoOriginal(controlArchivoBase
					.getNombreArchivoOriginal());

			controlArchivoNuevo = controlArchivoFacadeRemote
					.save(controlArchivoNuevo);
			vaultService.copyFile(controlArchivoBase, controlArchivoNuevo);

			documento.setId(new DocumentoAnexoReaseguroCotizacionId(idCotizacionCopia, controlArchivoNuevo.getIdToControlArchivo()));
			documento.setDescripcionDocumentoAnexo(item.getDescripcionDocumentoAnexo());
			this.save(documento);
		}
		
	}

	@EJB
	public void setControlArchivoFacadeRemote(
			ControlArchivoFacadeRemote controlArchivoFacadeRemote) {
		this.controlArchivoFacadeRemote = controlArchivoFacadeRemote;
	}
	@EJB
	public void setVaultService(VaultService vaultService) {
		this.vaultService = vaultService;
	}	
}