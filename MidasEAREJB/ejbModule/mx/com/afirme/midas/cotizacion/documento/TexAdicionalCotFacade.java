package mx.com.afirme.midas.cotizacion.documento;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotFacadeRemote;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity TexAdicionalCotDTO.
 * @see .TexAdicionalCotDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class TexAdicionalCotFacade  implements TexAdicionalCotFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved TexAdicionalCotDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TexAdicionalCotDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public TexAdicionalCotDTO save(TexAdicionalCotDTO entity) {
    				LogDeMidasEJB3.log("saving TexAdicionalCotDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
            return entity;
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent TexAdicionalCotDTO entity.
	  @param entity TexAdicionalCotDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TexAdicionalCotDTO entity) {
    				LogDeMidasEJB3.log("deleting TexAdicionalCotDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(TexAdicionalCotDTO.class, entity.getIdToTexAdicionalCot());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved TexAdicionalCotDTO entity and return it or a copy of it to the sender. 
	 A copy of the TexAdicionalCotDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TexAdicionalCotDTO entity to update
	 @return TexAdicionalCotDTO the persisted TexAdicionalCotDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public TexAdicionalCotDTO update(TexAdicionalCotDTO entity) {
    				LogDeMidasEJB3.log("updating TexAdicionalCotDTO instance", Level.INFO, null);
	        try {
            TexAdicionalCotDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public TexAdicionalCotDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding TexAdicionalCotDTO instance with id: " + id, Level.INFO, null);
	        try {
            TexAdicionalCotDTO instance = entityManager.find(TexAdicionalCotDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all TexAdicionalCotDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TexAdicionalCotDTO property to query
	  @param value the property value to match
	  	  @return List<TexAdicionalCotDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<TexAdicionalCotDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding TexAdicionalCotDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from TexAdicionalCotDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			

	/**
	 * Find all TexAdicionalCotDTO entities.
	  	  @return List<TexAdicionalCotDTO> all TexAdicionalCotDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TexAdicionalCotDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all TexAdicionalCotDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from TexAdicionalCotDTO model order by model.numeroSecuencia";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find all TexAdicionalCotDTO distint entities filtered by cotization.
	  	  @return List<TexAdicionalCotDTO> all TexAdicionalCotDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TexAdicionalCotDTO> listarFiltrado(TexAdicionalCotDTO texAdicionalCotDTO){
			LogDeMidasEJB3.log("finding all TexAdicionalCotDTO instances", Level.INFO, null);
		try {
			if (texAdicionalCotDTO==null || texAdicionalCotDTO.getCotizacion()==null || texAdicionalCotDTO.getCotizacion().getIdToCotizacion()==null)
				return null;
			String queryString = "select model from TexAdicionalCotDTO model";
			queryString += " where model.cotizacion=:cotizacion order by model.numeroSecuencia"; 
			Query query = entityManager.createQuery(queryString);
			query.setParameter("cotizacion", texAdicionalCotDTO.getCotizacion());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
					LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find all TexAdicionalCotDTO distint entities filtered by cotization and will be pendig to be acepted
	  	  @return List<TexAdicionalCotDTO> all TexAdicionalCotDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TexAdicionalCotDTO> listarFiltradoPorAutorizar(TexAdicionalCotDTO texAdicionalCotDTO){
			LogDeMidasEJB3.log("finding all TexAdicionalCotDTO instances", Level.INFO, null);
		try {
			if (texAdicionalCotDTO==null || texAdicionalCotDTO.getCotizacion()==null || texAdicionalCotDTO.getCotizacion().getIdToCotizacion()==null)
				return null;
			String queryString = "select model from TexAdicionalCotDTO model";
			queryString += " where model.cotizacion=:cotizacion and (model.claveAutorizacion=1 or model.claveAutorizacion=8) order by model.numeroSecuencia"; 
			Query query = entityManager.createQuery(queryString);
			query.setParameter("cotizacion", texAdicionalCotDTO.getCotizacion());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
					LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find all TexAdicionalCotDTO distint entities filtered by cotization and will be pendig to be acepted
	  	  @return List<TexAdicionalCotDTO> all TexAdicionalCotDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TexAdicionalCotDTO> buscarPorClaveAutorizacionPorCotizacion(BigDecimal idToCotizacion, Short claveAutorizacion){
			LogDeMidasEJB3.log("finding all TexAdicionalCotDTO instances with claveAutorizacion: "+claveAutorizacion+" and idToCotizacion: "+idToCotizacion, Level.INFO, null);
		try {
			if (idToCotizacion==null || claveAutorizacion==null )
				return null;
			String queryString = "select model from TexAdicionalCotDTO model " +
					"where model.cotizacion.idToCotizacion = :idToCotizacion and model.claveAutorizacion=:claveAutorizacion  "; 
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("claveAutorizacion", claveAutorizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
					LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * update claveAutorizacion from TexAdicionalCotDTO for ids
	  	  @return true if the update operation was realized
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean cambiarEstatusTextosAdicionales(String ids, TexAdicionalCotDTO texAdicionalCotDTO){		
			LogDeMidasEJB3.log("update claveAutorizacion from TexAdicionalCotDTO instances", Level.INFO, null);
			try {
				if (texAdicionalCotDTO==null || texAdicionalCotDTO.getClaveAutorizacion()==null)return false;
				if (ids==null || ids.trim().length()<1) return false;
				String queryString = "update TexAdicionalCotDTO model set model.claveAutorizacion=:claveAutorizacion,";
				queryString +=	
					(texAdicionalCotDTO.getCodigoUsuarioAutorizacion()!=null)?" model.codigoUsuarioAutorizacion=:codigoUsuarioAutorizacion,":"";
				queryString +=	" model.codigoUsuarioModificacion=:codigoUsuarioModificacion,";
				queryString +=	" model.nombreUsuarioModificacion=:nombreUsuarioModificacion ";
				queryString += " where model.idToTexAdicionalCot in (" + ids + ")"; 
				Query query = entityManager.createQuery(queryString);
				
				query.setParameter("claveAutorizacion", texAdicionalCotDTO.getClaveAutorizacion());				
				query.setParameter("codigoUsuarioModificacion", texAdicionalCotDTO.getCodigoUsuarioModificacion());
				query.setParameter("nombreUsuarioModificacion", texAdicionalCotDTO.getNombreUsuarioModificacion());
				if (texAdicionalCotDTO.getCodigoUsuarioAutorizacion()!=null)
					query.setParameter("codigoUsuarioAutorizacion", texAdicionalCotDTO.getCodigoUsuarioAutorizacion());
				query.executeUpdate();
				return true;
			} catch (RuntimeException re) {
				LogDeMidasEJB3.log("update TexAdicionalCotDTO instances failed", Level.SEVERE, re);
				throw re;
			}
	}

	@Override
	public void copiarTextosAdicionales(BigDecimal idCotizacionBase,
			BigDecimal idCotizacionCopia) {
		List<TexAdicionalCotDTO> textosBase = this.findByProperty("cotizacion.idToCotizacion", idCotizacionBase);
		TexAdicionalCotDTO texto = null;
		CotizacionDTO cotizacionCopia = entityManager.find(CotizacionDTO.class, idCotizacionCopia);
		for(TexAdicionalCotDTO item: textosBase){
			texto = new TexAdicionalCotDTO();
			texto.setClaveAutorizacion(item.getClaveAutorizacion());
			texto.setCodigoUsuarioAutorizacion(item.getCodigoUsuarioAutorizacion());
			texto.setCodigoUsuarioCreacion(item.getCodigoUsuarioCreacion());
			texto.setCotizacion(cotizacionCopia);
			texto.setDescripcionTexto(item.getDescripcionTexto());
			texto.setFechaCreacion(item.getFechaCreacion());
			texto.setFechaModificacion(item.getFechaModificacion());
			texto.setNombreUsuarioCreacion(item.getNombreUsuarioCreacion());
			texto.setNombreUsuarioModificacion(item.getNombreUsuarioModificacion());
			texto.setNumeroSecuencia(item.getNumeroSecuencia());
			this.save(texto);
		}
	}
}