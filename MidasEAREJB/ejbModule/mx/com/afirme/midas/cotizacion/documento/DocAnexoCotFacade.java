package mx.com.afirme.midas.cotizacion.documento;
// default package

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo.DocumentoAnexoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo.DocumentoAnexoTipoPolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaFacadeRemote;
import mx.com.afirme.midas.producto.documentoanexo.DocumentoAnexoProductoDTO;
import mx.com.afirme.midas.producto.documentoanexo.DocumentoAnexoProductoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoFacadeRemote;
import mx.com.afirme.midas.usuario.LogUtil;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.aumento.CoberturaAumento;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.util.VaultService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity DocAnexoCotDTO.
 * @see .DocAnexoCotDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DocAnexoCotFacade  implements DocAnexoCotFacadeRemote {
	@EJB
	private DocumentoAnexoCoberturaFacadeRemote anexoCoberturaFacadeRemote;
	@EJB
	private DocumentoAnexoProductoFacadeRemote anexoProductoFacadeRemote;
	@EJB
	private DocumentoAnexoTipoPolizaFacadeRemote anexoTipoPolizaFacadeRemote;
	@EJB
	private EntidadService entidadService;
	@EJB
	private CoberturaService coberturaService;
	ControlArchivoFacadeRemote controlArchivoFacadeRemote;
	VaultService vaultService;


    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DocAnexoCotDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocAnexoCotDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocAnexoCotDTO entity) {
    				LogUtil.log("saving DocAnexoCotDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DocAnexoCotDTO entity.
	  @param entity DocAnexoCotDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocAnexoCotDTO entity) {
    				LogUtil.log("deleting DocAnexoCotDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DocAnexoCotDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DocAnexoCotDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocAnexoCotDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocAnexoCotDTO entity to update
	 @return DocAnexoCotDTO the persisted DocAnexoCotDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DocAnexoCotDTO update(DocAnexoCotDTO entity) {
    				LogUtil.log("updating DocAnexoCotDTO instance", Level.INFO, null);
	        try {
            DocAnexoCotDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DocAnexoCotDTO findById( DocAnexoCotId id) {
    				LogUtil.log("finding DocAnexoCotDTO instance with id: " + id, Level.INFO, null);
	        try {
            DocAnexoCotDTO instance = entityManager.find(DocAnexoCotDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DocAnexoCotDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocAnexoCotDTO property to query
	  @param value the property value to match
	  	  @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the  the row index in the query result-set to begin collecting the results. rowStartIdxAndCount[1] specifies the the maximum number of results to return.  
	  	  @return List<DocAnexoCotDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DocAnexoCotDTO> findByProperty(String propertyName, final Object value) {
    				LogUtil.log("finding DocAnexoCotDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DocAnexoCotDTO model where model." 
			 						+ propertyName + "= :propertyValue order by model.orden";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all DocAnexoCotDTO entities.
	  	  @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the  the row index in the query result-set to begin collecting the results. rowStartIdxAndCount[1] specifies the the maximum count of results to return.  
	  	  @return List<DocAnexoCotDTO> all DocAnexoCotDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DocAnexoCotDTO> findAll() {
					LogUtil.log("finding all DocAnexoCotDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from DocAnexoCotDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find all DocAnexoCotDTO entities with a specific property value.  
	 
	  @param CotizacionDTO the cotizacion property to query
	  	  @return List<DocAnexoCotDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DocAnexoCotDTO> listarFiltrado(DocAnexoCotDTO docAnexoCotDTO) {
    	LogDeMidasEJB3.log("finding DocAnexoCotDTO instance by cotizacion", Level.INFO, null);
		try {
			if (docAnexoCotDTO==null || docAnexoCotDTO.getId().getIdToCotizacion()==null)
				return null;
			String queryString = "select model from DocAnexoCotDTO model" +
					 			 " where model.id.idToCotizacion = :idToCotizacion ";
			Boolean comparaClaveSeleccion = false;
			if (docAnexoCotDTO.getClaveSeleccion() != null){
				queryString += "and model.claveSeleccion = :claveSeleccion ";
				comparaClaveSeleccion = true;
			}
			queryString += "order by model.orden";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", docAnexoCotDTO.getId().getIdToCotizacion());
			if (comparaClaveSeleccion)
				query.setParameter("claveSeleccion", docAnexoCotDTO.getClaveSeleccion());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}
    
    
	public int deleteDocAnexoCotNotInCotizacion(BigDecimal idToCotizacion, BigDecimal claveTipo) {
					LogUtil.log("finding all DocAnexoCotDTO instances", Level.INFO, null);
		try {
			final String queryString = " DELETE FROM DocAnexoCotDTO acot " +
					" WHERE acot.claveTipo = " + claveTipo + " " +
					" AND acot.id.idToCotizacion = " + idToCotizacion + " " +
					" AND (acot.idToCobertura NOT IN ( " +
					"  SELECT DISTINCT cot.id.idToCobertura " +
					"  FROM CoberturaCotizacionDTO cot " +
					"  WHERE cot.id.idToCotizacion = " + idToCotizacion + " " +
					" )  OR acot.id.idToControlArchivo NOT IN ( " +
					"	SELECT DISTINCT cob.idToControlArchivo " +
					"	FROM DocumentoAnexoCoberturaDTO cob " +
					"	WHERE cob.coberturaDTO.idToCobertura = acot.idToCobertura " +
					"	)" +
					" )";
			Query query = entityManager.createQuery(queryString);
			//query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.executeUpdate();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public void copiarDocumentos(BigDecimal idCotizacionBase, BigDecimal idCotizacionCopia) {
		List<DocAnexoCotDTO> documentosBase = this
				.findByProperty("cotizacionDTO.idToCotizacion",
						idCotizacionBase);
		DocAnexoCotDTO documento = null;
		CotizacionDTO cotizacionCopia = entityManager.find(CotizacionDTO.class,
				idCotizacionCopia);
		for (DocAnexoCotDTO item : documentosBase) {
			documento = new DocAnexoCotDTO();
			documento.setCodigoUsuarioCreacion(item.getCodigoUsuarioCreacion());
			documento.setCodigoUsuarioModificacion(item
					.getCodigoUsuarioModificacion());
			documento.setCotizacionDTO(cotizacionCopia);
			documento.setFechaCreacion(item.getFechaCreacion());
			documento.setFechaModificacion(item.getFechaModificacion());
			documento.setNombreUsuarioCreacion(item.getNombreUsuarioCreacion());
			documento.setNombreUsuarioModificacion(item
					.getNombreUsuarioModificacion());

			ControlArchivoDTO controlArchivoBase = controlArchivoFacadeRemote
					.findById(item.getId().getIdToControlArchivo());
			ControlArchivoDTO controlArchivoNuevo = new ControlArchivoDTO();
			controlArchivoNuevo.setClaveTipo(controlArchivoBase.getClaveTipo());
			controlArchivoNuevo.setNombreArchivoOriginal(controlArchivoBase
					.getNombreArchivoOriginal());

			controlArchivoNuevo = controlArchivoFacadeRemote
					.save(controlArchivoNuevo);
			vaultService.copyFile(controlArchivoBase, controlArchivoNuevo);

			documento.setId(new DocAnexoCotId(idCotizacionCopia, controlArchivoNuevo.getIdToControlArchivo()));
			documento.setClaveObligatoriedad(item.getClaveObligatoriedad());
			documento.setClaveSeleccion(item.getClaveSeleccion());
			documento.setDescripcionDocumentoAnexo(item.getDescripcionDocumentoAnexo());
			documento.setIdToCobertura(item.getIdToCobertura());
			documento.setOrden(item.getOrden());

			this.save(documento);
		}

	}
	/**
	 * Delete all DocAnexoCotDTO with calvetipo = 2
	 * @param idToCotizacion
	 */
	public void validateGenerateCobDocuments(BigDecimal idToCotizacion){
		List<DocAnexoCotDTO> list = this.findByProperty(
				"cotizacionDTO.idToCotizacion", idToCotizacion);
		if (list != null && !list.isEmpty()) {
			for (DocAnexoCotDTO dto : list) {
				if(dto.getClaveTipo().equals(new BigDecimal(2l))) {
					this.delete(dto);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<DocumentoAnexoCoberturaDTO> generateFindAllByCobertura(List<DocumentoAnexoCoberturaDTO> documentoAnexoCoberturaDTOs,final BigDecimal idToCobertura) {
		Predicate predicate = new Predicate(){
			@Override
			public boolean evaluate(Object arg0) {
				DocumentoAnexoCoberturaDTO dto = (DocumentoAnexoCoberturaDTO) arg0;
				return dto.getCoberturaDTO().getIdToCobertura().equals(idToCobertura);
			}
		};
		return (List<DocumentoAnexoCoberturaDTO>) CollectionUtils.select(documentoAnexoCoberturaDTOs, predicate);
	}
	
	@Override
	public void generateCobDocumentsByCotizacion(BigDecimal idToCotizacion) {
		
		//validateGenerateCobDocuments(idToCotizacion);
		BigDecimal claveTipoCobertura = new BigDecimal(2);
		deleteDocAnexoCotNotInCotizacion(idToCotizacion, claveTipoCobertura);
		
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		
		List<DocumentoAnexoCoberturaDTO> documentoAnexoCoberturaDTOs = new LinkedList<DocumentoAnexoCoberturaDTO>();
		
		List<CoberturaCotizacionDTO> coberturaCotizacionDTOs = coberturaService.getCoberturasDistinct(idToCotizacion, true);
		
		if (coberturaCotizacionDTOs != null) {
			
			documentoAnexoCoberturaDTOs = anexoCoberturaFacadeRemote.listarAnexosPorCotizacion(idToCotizacion);
			
			// Genera los Anexos de las coberturas
			if (documentoAnexoCoberturaDTOs != null) {
				for (DocumentoAnexoCoberturaDTO dto : documentoAnexoCoberturaDTOs) {
					DocAnexoCotDTO anexoCotDTO = null;
					DocAnexoCotId anexoCotId = new DocAnexoCotId();
					anexoCotId.setIdToControlArchivo(dto
							.getIdToControlArchivo());
					anexoCotId.setIdToCotizacion(idToCotizacion);
					
					anexoCotDTO = entidadService.findById(DocAnexoCotDTO.class, anexoCotId);
					if(anexoCotDTO == null){
					anexoCotDTO = new DocAnexoCotDTO();
					anexoCotDTO.setId(anexoCotId);
					anexoCotDTO.setClaveTipo(claveTipoCobertura);
					anexoCotDTO.setClaveSeleccion((short) 1); 
					anexoCotDTO.setClaveObligatoriedad(dto.getClaveObligatoriedad());
					anexoCotDTO.setCodigoUsuarioCreacion(cotizacionDTO.getCodigoUsuarioCreacion());
					anexoCotDTO.setNombreUsuarioCreacion(cotizacionDTO.getCodigoUsuarioCreacion());
					anexoCotDTO.setFechaCreacion(Calendar.getInstance().getTime());
					anexoCotDTO.setCotizacionDTO(cotizacionDTO);
					anexoCotDTO.setDescripcionDocumentoAnexo(dto.getDescripcion());
					anexoCotDTO.setIdToCobertura(dto.getCoberturaDTO().getIdToCobertura());
					this.save(anexoCotDTO);
					}
				}
			}
		}
	}
	@Override
	public void generateDocumentsByCotizacion(BigDecimal idToCotizacion){
		
		CotizacionDTO cotizacionDTO = entidadService.findById(
				CotizacionDTO.class, idToCotizacion);
		
		List<DocumentoAnexoTipoPolizaDTO> documentoAnexoTipoPolizaDTOs = new LinkedList<DocumentoAnexoTipoPolizaDTO>();
		documentoAnexoTipoPolizaDTOs = anexoTipoPolizaFacadeRemote
				.findByProperty("tipoPolizaDTO.idToTipoPoliza", cotizacionDTO
						.getTipoPolizaDTO().getIdToTipoPoliza());
		// Genera los Anexos de los tipo de poiza
		if (documentoAnexoTipoPolizaDTOs != null){
			for (DocumentoAnexoTipoPolizaDTO dto : documentoAnexoTipoPolizaDTOs) {
				DocAnexoCotDTO anexoCotDTO = new DocAnexoCotDTO();
				DocAnexoCotId anexoCotId = new DocAnexoCotId();
				anexoCotId.setIdToControlArchivo(dto.getIdToControlArchivo());
				anexoCotId.setIdToCotizacion(idToCotizacion);
				anexoCotDTO.setId(anexoCotId);
				anexoCotDTO.setClaveTipo(BigDecimal.valueOf(1l));
				anexoCotDTO.setClaveSeleccion((short)1);
				anexoCotDTO.setIdToCobertura(BigDecimal.valueOf(0l));
				anexoCotDTO.setClaveObligatoriedad(dto.getClaveObligatoriedad());
				anexoCotDTO.setCodigoUsuarioCreacion(cotizacionDTO.getCodigoUsuarioCreacion());
				anexoCotDTO.setNombreUsuarioCreacion(cotizacionDTO.getCodigoUsuarioCreacion());
				anexoCotDTO.setFechaCreacion(Calendar.getInstance().getTime());
				anexoCotDTO.setCotizacionDTO(cotizacionDTO);
				anexoCotDTO.setDescripcionDocumentoAnexo(dto.getDescripcion());
				this.save(anexoCotDTO);
			}
		}
		List<DocumentoAnexoProductoDTO> documentoAnexoProductoDTOs = new LinkedList<DocumentoAnexoProductoDTO>();
		documentoAnexoProductoDTOs = anexoProductoFacadeRemote
				.findByProperty("productoDTO.idToProducto",cotizacionDTO.getTipoPolizaDTO().getProductoDTO()
						.getIdToProducto());
		// Genera los Anexos de Producto
		if (documentoAnexoProductoDTOs != null) {
			for (DocumentoAnexoProductoDTO dto : documentoAnexoProductoDTOs) {
				DocAnexoCotDTO anexoCotDTO = new DocAnexoCotDTO();
				DocAnexoCotId anexoCotId = new DocAnexoCotId();
				anexoCotId.setIdToControlArchivo(dto.getIdToControlArchivo());
				anexoCotId.setIdToCotizacion(idToCotizacion);
				anexoCotDTO.setId(anexoCotId);
				anexoCotDTO.setClaveTipo(BigDecimal.valueOf(0l));
				anexoCotDTO.setClaveSeleccion((short)1);
				anexoCotDTO.setIdToCobertura(BigDecimal.valueOf(0l));
				anexoCotDTO.setClaveObligatoriedad(dto.getClaveObligatoriedad());
				anexoCotDTO.setCodigoUsuarioCreacion(cotizacionDTO.getCodigoUsuarioCreacion());
				anexoCotDTO.setNombreUsuarioCreacion(cotizacionDTO.getCodigoUsuarioCreacion());
				anexoCotDTO.setFechaCreacion(Calendar.getInstance().getTime());
				anexoCotDTO.setCotizacionDTO(cotizacionDTO);
				anexoCotDTO.setDescripcionDocumentoAnexo(dto.getDescripcion());
				this.save(anexoCotDTO);
			}
		}
		
		try {
			this.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	@EJB
	public void setControlArchivoFacadeRemote(
			ControlArchivoFacadeRemote controlArchivoFacadeRemote) {
		this.controlArchivoFacadeRemote = controlArchivoFacadeRemote;
	}
	@EJB
	public void setVaultService(VaultService vaultService) {
		this.vaultService = vaultService;
	}
}