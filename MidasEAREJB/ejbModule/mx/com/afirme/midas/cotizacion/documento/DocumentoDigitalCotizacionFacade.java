package mx.com.afirme.midas.cotizacion.documento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoFacadeRemote;
import mx.com.afirme.midas2.util.VaultService;

/**
 * Facade for entity DocumentoDigitalCotizacionDTO.
 * 
 * @see mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class DocumentoDigitalCotizacionFacade implements DocumentoDigitalCotizacionFacadeRemote {

	ControlArchivoFacadeRemote controlArchivoFacadeRemote;
	VaultService vaultService;
	
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved DocumentoDigitalCotizacionDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            DocumentoDigitalCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DocumentoDigitalCotizacionDTO entity) {
		LogDeMidasEJB3.log("saving DocumentoDigitalCotizacionDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent DocumentoDigitalCotizacionDTO entity.
	 * 
	 * @param entity
	 *            DocumentoDigitalCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DocumentoDigitalCotizacionDTO entity) {
		LogDeMidasEJB3.log("deleting DocumentoDigitalCotizacionDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(DocumentoDigitalCotizacionDTO.class, entity
					.getIdDocumentoDigitalCotizacion());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved DocumentoDigitalCotizacionDTO entity and return it or a copy
	 * of it to the sender. A copy of the DocumentoDigitalCotizacionDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            DocumentoDigitalCotizacionDTO entity to update
	 * @return DocumentoDigitalCotizacionDTO the persisted DocumentoDigitalCotizacionDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DocumentoDigitalCotizacionDTO update(DocumentoDigitalCotizacionDTO entity) {
		LogDeMidasEJB3.log("updating DocumentoDigitalCotizacionDTO instance", Level.INFO, null);
		try {
			DocumentoDigitalCotizacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DocumentoDigitalCotizacionDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding DocumentoDigitalCotizacionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			DocumentoDigitalCotizacionDTO instance = entityManager.find(
					DocumentoDigitalCotizacionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DocumentoDigitalCotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the DocumentoDigitalCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DocumentoDigitalCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<DocumentoDigitalCotizacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding DocumentoDigitalCotizacionDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from DocumentoDigitalCotizacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DocumentoDigitalCotizacionDTO entities.
	 * 
	 * @return List<DocumentoDigitalCotizacionDTO> all DocumentoDigitalCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DocumentoDigitalCotizacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all DocumentoDigitalCotizacionDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from DocumentoDigitalCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public void copiarDocumentos(BigDecimal idCotizacionBase,
			BigDecimal idCotizacionCopia) {
		List<DocumentoDigitalCotizacionDTO> documentosBase = this
				.findByProperty("cotizacionDTO.idToCotizacion",
						idCotizacionBase);
		DocumentoDigitalCotizacionDTO documento= null;
		CotizacionDTO cotizacionCopia = entityManager.find(CotizacionDTO.class, idCotizacionCopia);
		for(DocumentoDigitalCotizacionDTO item: documentosBase){
			documento = new DocumentoDigitalCotizacionDTO();
			documento.setCodigoUsuarioCreacion(item.getCodigoUsuarioCreacion());
			documento.setCodigoUsuarioModificacion(item.getCodigoUsuarioModificacion());
			documento.setCotizacionDTO(cotizacionCopia);
			documento.setFechaCreacion(item.getFechaCreacion());
			documento.setFechaModificacion(item.getFechaModificacion());
			documento.setNombreUsuarioCreacion(item.getNombreUsuarioCreacion());
			documento.setNombreUsuarioModificacion(item.getNombreUsuarioModificacion());

			ControlArchivoDTO controlArchivoBase = controlArchivoFacadeRemote.findById(item.getIdControlArchivo());
			ControlArchivoDTO controlArchivoNuevo = new ControlArchivoDTO();
			controlArchivoNuevo.setClaveTipo(controlArchivoBase.getClaveTipo());
			controlArchivoNuevo.setNombreArchivoOriginal(controlArchivoBase.getNombreArchivoOriginal());
			
			controlArchivoNuevo = controlArchivoFacadeRemote.save(controlArchivoNuevo);
			vaultService.copyFile(controlArchivoBase, controlArchivoNuevo);
			
			documento.setControlArchivo(controlArchivoNuevo);
			documento.setIdControlArchivo(controlArchivoNuevo.getIdToControlArchivo());
			
			this.save(documento);
		}
	}

	@EJB
	public void setControlArchivoFacadeRemote(
			ControlArchivoFacadeRemote controlArchivoFacadeRemote) {
		this.controlArchivoFacadeRemote = controlArchivoFacadeRemote;
	}
	@EJB
	public void setVaultService(VaultService vaultService) {
		this.vaultService = vaultService;
	}

}