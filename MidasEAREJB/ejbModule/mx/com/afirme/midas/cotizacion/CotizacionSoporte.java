package mx.com.afirme.midas.cotizacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.direccion.DireccionFacadeRemote;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoFacadeRemote;
import mx.com.afirme.midas.solicitud.SolicitudDTO;

public class CotizacionSoporte {
	@EJB
	ControlArchivoFacadeRemote controlArchivoFacadeRemote;
	@EJB
	DireccionFacadeRemote direccionFacadeRemote;
	@EJB
	IncisoCotizacionFacadeRemote incisoCotizacionFacadeRemote;
	// Constante para Estatus de Cotizacion Asignada
	
	
	protected CotizacionDTO creaCopiaCotizacion(CotizacionDTO cotizacionOrigen, SolicitudDTO solicitud, String nombreUsuarioCotizacion,
			String nombreUsuario) {
		
		CotizacionDTO cotizacionDestino = new CotizacionDTO();
		
		cotizacionDestino.setTipoPolizaDTO(cotizacionOrigen.getTipoPolizaDTO());
		cotizacionDestino.setSolicitudDTO(solicitud); //Se le asigna la nueva solicitud de endoso
		cotizacionDestino.setDireccionCobroDTO(cotizacionOrigen.getDireccionCobroDTO());
		cotizacionDestino.setIdMoneda(cotizacionOrigen.getIdMoneda());
		cotizacionDestino.setIdFormaPago(cotizacionOrigen.getIdFormaPago());
		if(solicitud.getClaveTipoSolicitud().equals(SistemaPersistencia.CLAVE_SOLICITUD_ENDOSO)) {
		    	cotizacionDestino.setFechaInicioVigencia(solicitud.getFechaInicioVigenciaEndoso()); //Se obtiene de la solicitud
			if (solicitud.getClaveTipoEndoso() != null
					&& (solicitud.getClaveTipoEndoso().equals(
							SistemaPersistencia.ENDOSO_DE_MODIFICACION) || solicitud
							.getClaveTipoEndoso().equals(SistemaPersistencia.ENDOSO_DE_CAMBIO_FORMA_PAGO))) {
        		    cotizacionDestino.setClaveEstatus(SistemaPersistencia.ODT_ASIGNADA);
        		    cotizacionDestino.setCodigoUsuarioOrdenTrabajo(nombreUsuarioCotizacion);
			} else {
				if (solicitud.getClaveTipoEndoso().equals(
						SistemaPersistencia.ENDOSO_DE_CANCELACION)) {
					if (solicitud.getClaveMotivoEndoso() != null) {
						cotizacionDestino.setClaveMotivoEndoso(solicitud
								.getClaveMotivoEndoso());
					}
				}
				cotizacionDestino
						.setClaveEstatus(SistemaPersistencia.COTIZACION_ASIGNADA);
				cotizacionDestino
						.setCodigoUsuarioCotizacion(nombreUsuarioCotizacion);
			}
		} else {
		    //Cuando se crea una soeicitud a partir de una poliza. en el caso de Solicitud de Poliza
		    cotizacionDestino.setClaveEstatus(SistemaPersistencia.COTIZACION_ASIGNADA);
		    cotizacionDestino.setCodigoUsuarioCotizacion(nombreUsuarioCotizacion);
			cotizacionDestino.setFechaInicioVigencia(cotizacionOrigen.getFechaInicioVigencia());
		}
	    if(solicitud.getEsRenovacion() != null && solicitud.getEsRenovacion().shortValue() == 1){
	    	cotizacionDestino.setFechaInicioVigencia(cotizacionOrigen.getFechaFinVigencia()); 
	    	cotizacionDestino.setFechaFinVigencia(this.sumaDias(cotizacionOrigen.getFechaFinVigencia(), 365));
	    }else{
	    	cotizacionDestino.setFechaFinVigencia(cotizacionOrigen.getFechaFinVigencia());
	    }
		
		cotizacionDestino.setNombreEmpresaContratante(cotizacionOrigen.getNombreEmpresaContratante());
		cotizacionDestino.setNombreEmpresaAsegurado(cotizacionOrigen.getNombreEmpresaAsegurado());
		
		 //Usuario al que se le asigna la cotización de endoso
		cotizacionDestino.setFechaCreacion(new Date());
		cotizacionDestino.setCodigoUsuarioCreacion(nombreUsuario);  //Va el usuario que esta creando la nueva cotizacion
		cotizacionDestino.setFechaModificacion(new Date());
		cotizacionDestino.setCodigoUsuarioModificacion(nombreUsuario);

		cotizacionDestino.setPorcentajebonifcomision(cotizacionOrigen.getPorcentajebonifcomision());
		cotizacionDestino.setIdToPersonaContratante(cotizacionOrigen.getIdToPersonaContratante());
		cotizacionDestino.setIdToPersonaAsegurado(cotizacionOrigen.getIdToPersonaAsegurado());
		cotizacionDestino.setIdMedioPago(cotizacionOrigen.getIdMedioPago());
		cotizacionDestino.setClaveAutRetroacDifer(null);
		cotizacionDestino.setClaveAutVigenciaMaxMin(null);
		cotizacionDestino.setDiasGracia(cotizacionOrigen.getDiasGracia());
		cotizacionDestino.setFolioPolizaAsociada(cotizacionOrigen.getFolioPolizaAsociada());
		cotizacionDestino.setClaveAutoEmisionDocOperIlicitas(null);
		cotizacionDestino.setNombreAsegurado(cotizacionOrigen.getNombreAsegurado());
		cotizacionDestino.setNombreContratante(cotizacionOrigen.getNombreContratante());
		cotizacionDestino.setTipoCambio(cotizacionOrigen.getTipoCambio());
		cotizacionDestino.setPorcentajeIva(cotizacionOrigen.getPorcentajeIva());
		
		//Agrega relacion con Tipo de Negocio
		cotizacionDestino.setTipoNegocioDTO(cotizacionOrigen.getTipoNegocioDTO());		
		if (cotizacionDestino.getTipoNegocioDTO().getCotizacionDTOs() == null) {
			cotizacionDestino.getTipoNegocioDTO().setCotizacionDTOs(new ArrayList<CotizacionDTO>());
		}
		cotizacionDestino.getTipoNegocioDTO().getCotizacionDTOs().add(cotizacionDestino);
		
		return cotizacionDestino;		
	}
		
	
	protected CotizacionDTO copiaRelacionesAnexosTextosCotizacion(CotizacionDTO cotizacionOrigen, CotizacionDTO cotizacionDestino) {						
//		//Se agregan las relaciondes de documentos digitales
//		List<DocumentoDigitalCotizacionDTO> documentosDigitales = new ArrayList<DocumentoDigitalCotizacionDTO>();
//		for (DocumentoDigitalCotizacionDTO documentoOriginal : cotizacionOrigen.getDocumentoDigitalCotizacionDTOs()) {
//			DocumentoDigitalCotizacionDTO documentoNuevo = new DocumentoDigitalCotizacionDTO();
//			documentoNuevo.setCotizacionDTO(cotizacionDestino);
//			documentoNuevo.setCodigoUsuarioCreacion(documentoOriginal.getCodigoUsuarioCreacion());
//			documentoNuevo.setCodigoUsuarioModificacion(documentoOriginal.getCodigoUsuarioModificacion());
//			documentoNuevo.setFechaCreacion(documentoOriginal.getFechaCreacion());
//			documentoNuevo.setFechaModificacion(documentoOriginal.getFechaModificacion());
//			documentoNuevo.setNombreUsuarioCreacion(documentoOriginal.getNombreUsuarioCreacion());
//			documentoNuevo.setNombreUsuarioModificacion(documentoOriginal.getNombreUsuarioModificacion());
//			
//			ControlArchivoDTO controlArchivoOriginal = controlArchivoFacadeRemote.findById(documentoOriginal.getIdControlArchivo());
//			ControlArchivoDTO controlArchivoNuevo = new ControlArchivoDTO();
//			controlArchivoNuevo.setClaveTipo(controlArchivoOriginal.getClaveTipo());
//			controlArchivoNuevo.setNombreArchivoOriginal(controlArchivoOriginal.getNombreArchivoOriginal());
//			controlArchivoNuevo = controlArchivoFacadeRemote.save(controlArchivoNuevo);
//			documentoNuevo.setIdControlArchivo(controlArchivoNuevo.getIdToControlArchivo());
//			documentoNuevo.setControlArchivo(controlArchivoNuevo);
//
//			cotizacionDestino.addArchivoPendiente(documentoOriginal.getControlArchivo(), controlArchivoNuevo);
//			documentosDigitales.add(documentoNuevo);
//		}
//		cotizacionDestino.setDocumentoDigitalCotizacionDTOs(documentosDigitales);
//		
//		//Se agregan los Documentos Anexos de la cotizacion
//		List<DocAnexoCotDTO> documentosAnexos = new ArrayList<DocAnexoCotDTO>();
//		for(DocAnexoCotDTO documentoOriginal : cotizacionOrigen.getDocumentoAnexoCotizacionDTOs()){
//			DocAnexoCotDTO documentoNuevo = new DocAnexoCotDTO();
//			documentoNuevo.setCotizacionDTO(documentoOriginal.getCotizacionDTO());
//			documentoNuevo.setClaveObligatoriedad(documentoOriginal.getClaveObligatoriedad());
//			documentoNuevo.setClaveSeleccion(documentoOriginal.getClaveSeleccion());
//			documentoNuevo.setClaveTipo(documentoOriginal.getClaveTipo());
//			documentoNuevo.setCodigoUsuarioCreacion(documentoOriginal.getCodigoUsuarioCreacion());
//			documentoNuevo.setCodigoUsuarioModificacion(documentoOriginal.getCodigoUsuarioModificacion());
//			documentoNuevo.setDescripcionDocumentoAnexo(documentoOriginal.getDescripcionDocumentoAnexo());
//			documentoNuevo.setFechaCreacion(documentoOriginal.getFechaCreacion());
//			documentoNuevo.setFechaModificacion(documentoOriginal.getFechaModificacion());
//			documentoNuevo.setNombreUsuarioCreacion(documentoOriginal.getNombreUsuarioCreacion());
//			documentoNuevo.setNombreUsuarioModificacion(documentoOriginal.getNombreUsuarioModificacion());
//			documentoNuevo.setOrden(documentoOriginal.getOrden());
//			documentoNuevo.setIdToCobertura(documentoOriginal.getIdToCobertura());
//
//			documentoNuevo.setId(new DocAnexoCotId());
//			documentoNuevo.getId().setIdToControlArchivo(documentoOriginal.getId().getIdToControlArchivo());
//			documentoNuevo.getId().setIdToCotizacion(cotizacionDestino.getIdToCotizacion());
//		
//			documentosAnexos.add(documentoNuevo);
//		}
//		cotizacionDestino.setDocumentoAnexoCotizacionDTOs(documentosAnexos);
//
//		//Se agregarn los documentos anexos de reaseguro de la cotizacion
//		List<DocumentoAnexoReaseguroCotizacionDTO> documentosAnexosReaseguro = new ArrayList<DocumentoAnexoReaseguroCotizacionDTO>();
//		for(DocumentoAnexoReaseguroCotizacionDTO documentoOriginal : cotizacionOrigen.getDocumentoAnexoReaseguroCotizacionDTOs()){
//			DocumentoAnexoReaseguroCotizacionDTO documentoNuevo = new DocumentoAnexoReaseguroCotizacionDTO();
//			documentoNuevo.setCotizacionDTO(documentoOriginal.getCotizacionDTO());
//			documentoNuevo.setCodigoUsuarioCreacion(documentoOriginal.getCodigoUsuarioCreacion());
//			documentoNuevo.setCodigoUsuarioModificacion(documentoOriginal.getCodigoUsuarioModificacion());
//			documentoNuevo.setDescripcionDocumentoAnexo(documentoOriginal.getDescripcionDocumentoAnexo());
//			documentoNuevo.setFechaCreacion(documentoOriginal.getFechaCreacion());
//			documentoNuevo.setFechaModificacion(documentoOriginal.getFechaModificacion());
//			documentoNuevo.setNombreUsuarioCreacion(documentoOriginal.getNombreUsuarioCreacion());
//			documentoNuevo.setNombreUsuarioModificacion(documentoOriginal.getNombreUsuarioModificacion());
//			documentoNuevo.setNumeroSecuencia(documentoOriginal.getNumeroSecuencia());
//					
//			documentoNuevo.setId(new DocumentoAnexoReaseguroCotizacionId());
//			documentoNuevo.getId().setIdToControlArchivo(documentoOriginal.getId().getIdToControlArchivo());
//			documentoNuevo.getId().setIdToCotizacion(cotizacionDestino.getIdToCotizacion());
//			
//			documentosAnexosReaseguro.add(documentoNuevo);
//		}
//		cotizacionDestino.setDocumentoAnexoReaseguroCotizacionDTOs(documentosAnexosReaseguro);
//		
		//Se agregan los Textos Adicionales de la cotizacion
		List<TexAdicionalCotDTO> textosAdicionales = new ArrayList<TexAdicionalCotDTO>();
		for(TexAdicionalCotDTO textoOriginal : cotizacionOrigen.getTexAdicionalCotDTOs()){
			TexAdicionalCotDTO textoNuevo = new TexAdicionalCotDTO();
			textoNuevo.setCotizacion(cotizacionDestino);
			textoNuevo.setClaveAutorizacion(textoOriginal.getClaveAutorizacion());
			textoNuevo.setCodigoUsuarioAutorizacion(textoOriginal.getCodigoUsuarioAutorizacion());
			textoNuevo.setCodigoUsuarioCreacion(textoOriginal.getCodigoUsuarioCreacion());
			textoNuevo.setCodigoUsuarioModificacion(textoOriginal.getCodigoUsuarioModificacion());
			textoNuevo.setDescripcionTexto(textoOriginal.getDescripcionTexto());
			textoNuevo.setFechaCreacion(textoOriginal.getFechaCreacion());
			textoNuevo.setFechaModificacion(textoOriginal.getFechaModificacion());
			textoNuevo.setNombreUsuarioCreacion(textoOriginal.getNombreUsuarioCreacion());
			textoNuevo.setNombreUsuarioModificacion(textoOriginal.getNombreUsuarioModificacion());
			textoNuevo.setNumeroSecuencia(textoOriginal.getNumeroSecuencia());

			textosAdicionales.add(textoNuevo);
		}
		cotizacionDestino.setTexAdicionalCotDTOs(textosAdicionales);		
			
		return cotizacionDestino;
		
	}

	public String copiarDocumentosDigitales(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = " INSERT INTO MIDAS.todocdigitalcot (idtodocdigitalcot, idtocotizacion, idtocontrolarchivo,"
				+ "fechacreacion, codigousuariocreacion, nombreusuariocreacion,fechamodificacion, codigousuariomodificacion,nombreusuariomodificacion)"
				+ " SELECT MIDAS.IDTODOCDIGITALCOT_SEQ.NEXTVAL,"
				+ idToCotizacion
				+ ", idtocontrolarchivo,"
				+ " fechacreacion, codigousuariocreacion, nombreusuariocreacion, fechamodificacion, codigousuariomodificacion, nombreusuariomodificacion"
				+ " FROM MIDAS.todocdigitalcot where idtocotizacion = "
				+ idToCotizacionBase;

		return queryString;
	}
	
	public String copiarDocumentosCotizacion(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "INSERT INTO MIDAS.todocanexocot (idtocotizacion, idtocontrolarchivo, fechacreacion,codigousuariocreacion, nombreusuariocreacion, fechamodificacion,"
				+ "codigousuariomodificacion, nombreusuariomodificacion,descripciondocumentoanexo, claveobligatoriedad, claveseleccion,orden, clavetipo, idtocobertura) "
				+ " SELECT "
				+ idToCotizacion
				+ ", idtocontrolarchivo, fechacreacion,codigousuariocreacion, nombreusuariocreacion, fechamodificacion,"
				+ " codigousuariomodificacion, nombreusuariomodificacion,descripciondocumentoanexo, claveobligatoriedad, claveseleccion, orden, clavetipo, idtocobertura "
				+ " FROM MIDAS.todocanexocot where idtocotizacion = "
				+ idToCotizacionBase;
		return queryString;
	}

	public String copiarDocumentosReaseguro(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "INSERT INTO MIDAS.todocanexoreasegurocot (idtocotizacion, idtocontrolarchivo, numerosecuencia, descripciondocumentoanexo, fechacreacion,"
				+ "  codigousuariocreacion, nombreusuariocreacion, fechamodificacion, codigousuariomodificacion, nombreusuariomodificacion)"
				+ " SELECT "
				+ idToCotizacion
				+ ", idtocontrolarchivo, numerosecuencia, descripciondocumentoanexo, fechacreacion, codigousuariocreacion,"
				+ " nombreusuariocreacion, fechamodificacion, codigousuariomodificacion, nombreusuariomodificacion FROM MIDAS.todocanexoreasegurocot "
				+ " WHERE idtocotizacion = " + idToCotizacionBase;
		return queryString;
	}

	public String copiarTextosAdicionales(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "INSERT INTO MIDAS.totexadicionalcot (idtotexadicionalcot, idtocotizacion, descripciontexto, claveautorizacion, codigousuarioautorizacion, fechacreacion,"
				+ " codigousuariocreacion, nombreusuariocreacion, fechamodificacion, codigousuariomodificacion, nombreusuariomodificacion, numerosecuencia, fechasolicitudautorizacion, fechaautorizacion)"
				+ " SELECT MIDAS.IDTOTEXADICIONALCOT_SEQ.nextval, "
				+ idToCotizacion
				+ ", descripciontexto,claveautorizacion, codigousuarioautorizacion, fechacreacion,"
				+ " codigousuariocreacion, nombreusuariocreacion, fechamodificacion, codigousuariomodificacion, nombreusuariomodificacion, numerosecuencia, fechasolicitudautorizacion, fechaautorizacion"
				+ " FROM MIDAS.totexadicionalcot WHERE idtocotizacion = "
				+ idToCotizacionBase;
		return queryString;
	}
	
	
	public String copiarIncisos(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "insert into MIDAS.toincisocot (idtocotizacion, numeroinciso, idtodireccioninciso, valorprimaneta, claveestatusinspeccion,"
				+ " clavetipoorigeninspeccion, clavemensajeinspeccion, codigousuarioestinspeccion, claveautinspeccion, fechaestatusinspeccion,"
				+ " fechasolautinspeccion, fechaautinspeccion,descripciongiroasegurado) "
				+ " select "
				+ idToCotizacion
				+ " , incisocot.numeroinciso, incisocot.idtodireccioninciso, incisocot.valorprimaneta, incisocot.claveestatusinspeccion, incisocot.clavetipoorigeninspeccion, incisocot.clavemensajeinspeccion,"
				+ " incisocot.codigousuarioestinspeccion, incisocot.claveautinspeccion, incisocot.fechaestatusinspeccion,incisocot.fechasolautinspeccion, incisocot.fechaautinspeccion, incisocot.descripciongiroasegurado "
				+ " from MIDAS.toincisocot incisocot  where incisocot.idtocotizacion = "
				+ idToCotizacionBase;
		return queryString;
	}

	public String copiarSecciones(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "insert into MIDAS.toseccioncot (idtocotizacion, numeroinciso, idtoseccion, valorprimaneta, valorsumaasegurada,"
				+ " claveobligatoriedad, clavecontrato) "
				+ " select "
				+ idToCotizacion
				+ " , seccioncot.numeroinciso, seccioncot.idtoseccion, seccioncot.valorprimaneta, seccioncot.valorsumaasegurada, seccioncot.claveobligatoriedad, seccioncot.clavecontrato "
				+ " from MIDAS.toseccioncot seccioncot where seccioncot.idtocotizacion = "
				+ idToCotizacionBase;
		return queryString;
	}

	public String copiarCoberturas(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {

		String queryString = "insert into MIDAS.tocoberturacot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura, idtcsubramo,"
				+ " valorprimaneta, valorsumaasegurada, valorcoaseguro, valordeducible, codigousuarioautreaseguro, "
				+ " claveobligatoriedad, clavecontrato, valorcuota, porcentajecoaseguro, claveautcoaseguro, "
				+ " codigousuarioautcoaseguro, porcentajededucible, claveautdeducible, codigousuarioautdeducible, numeroagrupacion, "
				+ " fechasolautcoaseguro, fechaautcoaseguro, fechasolautdeducible, fechaautdeducible, clavetipodeducible, "
				+ " clavetipolimitededucible, valorminimolimitededucible, valormaximolimitededucible,clavefacultativo) "
				+ " select "
				+ idToCotizacion
				+ " , coberturacot.numeroinciso, coberturacot.idtoseccion, coberturacot.idtocobertura, coberturacot.idtcsubramo,"
				+ " coberturacot.valorprimaneta, coberturacot.valorsumaasegurada, coberturacot.valorcoaseguro, coberturacot.valordeducible, coberturacot.codigousuarioautreaseguro,"
				+ " coberturacot.claveobligatoriedad, coberturacot.clavecontrato, coberturacot.valorcuota, coberturacot.porcentajecoaseguro, coberturacot.claveautcoaseguro,"
				+ " coberturacot.codigousuarioautcoaseguro, coberturacot.porcentajededucible, coberturacot.claveautdeducible, coberturacot.codigousuarioautdeducible, numeroagrupacion, "
				+ " coberturacot.fechasolautcoaseguro, coberturacot.fechaautcoaseguro, coberturacot.fechasolautdeducible, coberturacot.fechaautdeducible, coberturacot.clavetipodeducible,"
				+ " coberturacot.clavetipolimitededucible, coberturacot.valorminimolimitededucible, coberturacot.valormaximolimitededucible, coberturacot.clavefacultativo"
				+ " from MIDAS.tocoberturacot coberturacot where coberturacot.idtocotizacion ="
				+ idToCotizacionBase;
		return queryString;
	}

	public String copiarRiesgos(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "insert into MIDAS.toriesgocot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura, idtoriesgo, "
				+ " idtcsubramo, valorprimaneta, valorsumaasegurada, valorcoaseguro, valordeducible, "
				+ " valordescuento, valorrecargo, valoraumento, porcentajecoaseguro, claveautcoaseguro, "
				+ " codigousuarioautcoaseguro, porcentajededucible, claveautdeducible, codigousuarioautdeducible, claveobligatoriedad, "
				+ " clavecontrato, valorcuotab, valorcuotaardt, valorprimanetaardt, valorprimanetab,  "
				+ " fechasolautcoaseguro, fechaautcoaseguro, fechasolautdeducible, fechaautdeducible, clavetipodeducible,  "
				+ " clavetipolimitededucible, valorminimolimitededucible, valormaximolimitededucible) "
				+ " select "
				+ idToCotizacion
				+ " ,riesgocot.numeroinciso, riesgocot.idtoseccion, riesgocot.idtocobertura, riesgocot.idtoriesgo,"
				+ " riesgocot.idtcsubramo, riesgocot.valorprimaneta, riesgocot.valorsumaasegurada, riesgocot.valorcoaseguro, riesgocot.valordeducible,"
				+ " riesgocot.valordescuento, riesgocot.valorrecargo, riesgocot.valoraumento, riesgocot.porcentajecoaseguro, riesgocot.claveautcoaseguro,"
				+ " riesgocot.codigousuarioautcoaseguro, riesgocot.porcentajededucible, riesgocot.claveautdeducible, riesgocot.codigousuarioautdeducible, riesgocot.claveobligatoriedad, "
				+ " riesgocot.clavecontrato, riesgocot.valorcuotab, riesgocot.valorcuotaardt, riesgocot.valorprimanetaardt, riesgocot.valorprimanetab,"
				+ " riesgocot.fechasolautcoaseguro, riesgocot.fechaautcoaseguro, riesgocot.fechasolautdeducible, riesgocot.fechaautdeducible, riesgocot.clavetipodeducible,"
				+ " riesgocot.clavetipolimitededucible, riesgocot.valorminimolimitededucible, riesgocot.valormaximolimitededucible"
				+ " from MIDAS.toriesgocot riesgocot where riesgocot.idtocotizacion ="
				+ idToCotizacionBase;
		return queryString;
	}
	
	public String copiarAgrupaciones(BigDecimal idToCotizacion ,BigDecimal idToCotizacionBase) {
		String queryString = "INSERT INTO MIDAS.toagrupacioncot (idToCotizacion, numeroAgrupacion, claveTipoAgrupacion, idToSeccion, valorSumaAsegurada, valorCuota, valorPrimaNeta) "
				+ " SELECT "
				+ idToCotizacion
				+ ", agrupacioncot.numeroAgrupacion, agrupacioncot.claveTipoAgrupacion, agrupacioncot.idToSeccion, "
				+ " agrupacioncot.valorSumaAsegurada, agrupacioncot.valorCuota, agrupacioncot.valorPrimaNeta "
				+ " FROM MIDAS.toagrupacioncot agrupacioncot WHERE agrupacioncot.idToCotizacion = "
				+ idToCotizacionBase;
		return queryString;
	}	
	
	public String copiarsubIncisos(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "insert into MIDAS.tosubincisocot (idtocotizacion, numeroinciso, idtoseccion, numerosubinciso, claveautreaseguro, "
				+ " codigousuarioautreaseguro, descripcionsubinciso, valorsumaasegurada, claveestatusdeclaracion) "
				+ " SELECT "
				+ idToCotizacion
				+ ", subincisocot.numeroinciso, subincisocot.idtoseccion, subincisocot.numerosubinciso, subincisocot.claveautreaseguro, "
				+ " subincisocot.codigousuarioautreaseguro, subincisocot.descripcionsubinciso, subincisocot.valorsumaasegurada, subincisocot.claveestatusdeclaracion "
				+ " FROM MIDAS.tosubincisocot subincisocot WHERE subincisocot.idToCotizacion = "
				+ idToCotizacionBase;
		return queryString;
	}

	public String copiarDetallePrimaCobertura(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "insert into MIDAS.todetprimacoberturacot (numerosubinciso, idtocotizacion, numeroinciso, idtoseccion, idtocobertura,"
				+ " valorprimanetab, valorprimanetaardt, valorprimaneta, valorcuotab, valorcuotaardt, "
				+ " valorcuotaardv, valorcuota)"
				+ " SELECT detprima.numerosubinciso, "
				+ idToCotizacion
				+ ", detprima.numeroinciso, detprima.idtoseccion, detprima.idtocobertura, "
				+ " detprima.valorprimanetab, detprima.valorprimanetaardt, detprima.valorprimaneta, detprima.valorcuotab, detprima.valorcuotaardt, "
				+ " detprima.valorcuotaardv, detprima.valorcuota "
				+ " FROM MIDAS.todetprimacoberturacot detprima WHERE detprima.idToCotizacion = "
				+ idToCotizacionBase;
		return queryString;
	}

	public String copiarDetallePrimaRiesgo(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "insert into MIDAS.todetprimariesgocot (numerosubinciso, idtocotizacion, numeroinciso, idtoseccion, idtocobertura, idtoriesgo,"
				+ " valorsumaasegurada, valorprimanetab, valorprimanetaardt, valorprimaneta, valorcuotab, valorcuotaardt, "
				+ " valorcuotaardv, valorcuota)"
				+ " SELECT detprima.numerosubinciso, "
				+ idToCotizacion
				+ ", detprima.numeroinciso, detprima.idtoseccion, detprima.idtocobertura, detprima.idtoriesgo,"
				+ " detprima.valorsumaasegurada, detprima.valorprimanetab, detprima.valorprimanetaardt, detprima.valorprimaneta, detprima.valorcuotab, detprima.valorcuotaardt, "
				+ " detprima.valorcuotaardv, detprima.valorcuota "
				+ " FROM MIDAS.todetprimariesgocot detprima WHERE detprima.idToCotizacion = "
				+ idToCotizacionBase;
		return queryString;
	}

	protected String copiarComisiones(BigDecimal idToCotizacion, BigDecimal idToCotizacionBase) {
		String query = "INSERT INTO MIDAS.tocomisioncot c1 " +
				"(c1.idtocotizacion, c1.idtcsubramo, " +
				"c1.valorcomisioncotizacion, c1.claveautcomision, " +
				"c1.codigousuarioautcomision, " +
				"c1.clavetipoporcentajecomision, " +
				"c1.porcentajecomisiondefault, " +
				"c1.porcentajecomisioncotizacion, " +
				"c1.fechasolautcomision, c1.fechaautcomision) " +
				"(SELECT "+idToCotizacion+", c2.idtcsubramo, " +
				"c2.valorcomisioncotizacion, c2.claveautcomision, " +
				"c2.codigousuarioautcomision, " +
				"c2.clavetipoporcentajecomision, " +
				"c2.porcentajecomisiondefault, " +
				"c2.porcentajecomisioncotizacion, " +
				"c2.fechasolautcomision, c2.fechaautcomision " +
				"FROM MIDAS.tocomisioncot c2 " +
				"WHERE c2.idtocotizacion = "+idToCotizacionBase+")";
		return query;
	}
	     
	protected String copiarDatosIncisos(BigDecimal idToCotizacion, BigDecimal idToCotizacionBase) {
		String query = "INSERT INTO MIDAS.todatoincisocot d1 " +
				"(d1.idtocotizacion, d1.numeroinciso, d1.idtoseccion, " +
				"d1.idtocobertura, d1.idtoriesgo, d1.numerosubinciso, d1.idtcramo, " +
				"d1.idtcsubramo, d1.clavedetalle, d1.iddato, d1.valor) " +
				"(SELECT "+idToCotizacion+", d2.numeroinciso, d2.idtoseccion, " +
				"d2.idtocobertura, d2.idtoriesgo, d2.numerosubinciso, d2.idtcramo, " +
				"d2.idtcsubramo, d2.clavedetalle, d2.iddato, d2.valor " +
				"FROM MIDAS.todatoincisocot d2 " +
				"WHERE d2.idtocotizacion = "+idToCotizacionBase+")";
		return query;
	}	

	protected String copiarAumentos(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "INSERT INTO MIDAS.toaumentoriesgocot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura,"
				+ " idtoriesgo, idtoaumentovario, claveautorizacion, codigousuarioautorizacion, valoraumento, claveobligatoriedad, "
				+ " clavecontrato, clavecomercialtecnico, clavenivel, fechasolicitudautorizacion, fechaautorizacion) "
				+ " select "
				+ idToCotizacion
				+ " ,riesgocot.numeroinciso, riesgocot.idtoseccion, riesgocot.idtocobertura,"
				+ " riesgocot.idtoriesgo, riesgocot.idtoaumentovario, riesgocot.claveautorizacion, riesgocot.codigousuarioautorizacion, riesgocot.valoraumento, riesgocot.claveobligatoriedad,"
				+ " riesgocot.clavecontrato, riesgocot.clavecomercialtecnico, riesgocot.clavenivel, riesgocot.fechasolicitudautorizacion, riesgocot.fechaautorizacion"
				+ " from MIDAS.toaumentoriesgocot riesgocot where riesgocot.idtocotizacion = "
				+ idToCotizacionBase;
		return queryString;
	}
	
	protected String copiarRecargos(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "INSERT INTO MIDAS.torecargoriesgocot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura,"
				+ " idtoriesgo, idtorecargovario, claveautorizacion, codigousuarioautorizacion, valorrecargo, claveobligatoriedad, "
				+ " clavecontrato, clavecomercialtecnico, clavenivel, fechasolicitudautorizacion, fechaautorizacion) "
				+ " select "
				+ idToCotizacion
				+ " ,riesgocot.numeroinciso, riesgocot.idtoseccion, riesgocot.idtocobertura,"
				+ " riesgocot.idtoriesgo, riesgocot.idtorecargovario, riesgocot.claveautorizacion, riesgocot.codigousuarioautorizacion, riesgocot.valorrecargo, riesgocot.claveobligatoriedad,"
				+ " riesgocot.clavecontrato, riesgocot.clavecomercialtecnico, riesgocot.clavenivel, riesgocot.fechasolicitudautorizacion, riesgocot.fechaautorizacion"
				+ " from MIDAS.torecargoriesgocot riesgocot where riesgocot.idtocotizacion = "
				+ idToCotizacionBase;
		return queryString;
	}	

	protected String copiarDescuentos(BigDecimal idToCotizacion,
			BigDecimal idToCotizacionBase) {
		String queryString = "INSERT INTO MIDAS.todescuentoriesgocot (idtocotizacion, numeroinciso, idtoseccion, idtocobertura,"
				+ " idtoriesgo, idtodescuentovario, claveautorizacion, codigousuarioautorizacion, valordescuento, claveobligatoriedad, "
				+ " clavecontrato, clavecomercialtecnico, clavenivel, fechasolicitudautorizacion, fechaautorizacion) "
				+ " select "
				+ idToCotizacion
				+ " ,riesgocot.numeroinciso, riesgocot.idtoseccion, riesgocot.idtocobertura,"
				+ " riesgocot.idtoriesgo, riesgocot.idtodescuentovario, riesgocot.claveautorizacion, riesgocot.codigousuarioautorizacion, riesgocot.valordescuento, riesgocot.claveobligatoriedad,"
				+ " riesgocot.clavecontrato, riesgocot.clavecomercialtecnico, riesgocot.clavenivel, riesgocot.fechasolicitudautorizacion, riesgocot.fechaautorizacion"
				+ " from MIDAS.todescuentoriesgocot riesgocot where riesgocot.idtocotizacion = "
				+ idToCotizacionBase;
		return queryString;
	}
	
	protected void copiaDireccionesIncisos(CotizacionDTO cotizacionOrigen, CotizacionDTO cotizacionDestino){
		for(IncisoCotizacionDTO incisoOrigen : cotizacionOrigen.getIncisoCotizacionDTOs()){
			DireccionDTO direccionOrigen = incisoOrigen.getDireccionDTO();
			DireccionDTO direccionDestino = new DireccionDTO();
			direccionDestino.setCodigoPostal(direccionOrigen.getCodigoPostal());
			direccionDestino.setEntreCalles(direccionOrigen.getEntreCalles());
			direccionDestino.setIdEstado(direccionOrigen.getIdEstado());
			direccionDestino.setIdMunicipio(direccionOrigen.getIdMunicipio());
			direccionDestino.setNombreCalle(direccionOrigen.getNombreCalle());
			direccionDestino.setNombreColonia(direccionOrigen.getNombreColonia());
			direccionDestino.setNombreDelegacion(direccionOrigen.getNombreDelegacion());
			direccionDestino.setNumeroExterior(direccionOrigen.getNumeroExterior());
			direccionDestino.setNumeroInterior(direccionOrigen.getNumeroInterior());						
			direccionDestino = direccionFacadeRemote.save(direccionDestino);			 
			IncisoCotizacionId id = new IncisoCotizacionId(cotizacionDestino.getIdToCotizacion(), 
					                                       incisoOrigen.getId().getNumeroInciso()); 			
			IncisoCotizacionDTO incisoDestino = incisoCotizacionFacadeRemote.findById(id);			
			incisoDestino.setDireccionDTO(direccionDestino);
			incisoCotizacionFacadeRemote.update(incisoDestino);
		}
	}	
	/**
	 * Agrega dias a una fecha
	 * @param Date fecha
	 * @param int dias
	 * @return Date fechaIncrementada
	 */	
	public Date sumaDias(Date fecha, int dias) {
		
		Calendar fechaC = Calendar.getInstance();
		fechaC.setTime(fecha);
		
		fechaC.add(Calendar.DATE, dias);

		return fechaC.getTime();
	}	
}
