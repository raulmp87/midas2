package mx.com.afirme.midas.cotizacion.seccion;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity SeccionCotizacionDTO.
 * @see .SeccionCotizacionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class SeccionCotizacionFacade  implements SeccionCotizacionFacadeRemote {
	//property constants
	public static final String VALORPRIMANETA = "valorprimaneta";
	public static final String VALORSUMAASEGURADA = "valorsumaasegurada";
	public static final String CLAVEOBLIGATORIEDAD = "claveobligatoriedad";
	public static final String CLAVECONTRATO = "clavecontrato";


    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved SeccionCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SeccionCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(SeccionCotizacionDTO entity) {
    				LogDeMidasEJB3.log("saving SeccionCotizacionDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent SeccionCotizacionDTO entity.
	  @param entity SeccionCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SeccionCotizacionDTO entity) {
    				LogDeMidasEJB3.log("deleting SeccionCotizacionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(SeccionCotizacionDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved SeccionCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the SeccionCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SeccionCotizacionDTO entity to update
	 @return SeccionCotizacionDTO the persisted SeccionCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public SeccionCotizacionDTO update(SeccionCotizacionDTO entity) {
    				LogDeMidasEJB3.log("updating SeccionCotizacionDTO instance", Level.INFO, null);
	        try {
            SeccionCotizacionDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
            			entityManager.flush();
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public SeccionCotizacionDTO findById( SeccionCotizacionDTOId id) {
    				LogDeMidasEJB3.log("finding SeccionCotizacionDTO instance with id: " + id, Level.INFO, null);
	        try {
            SeccionCotizacionDTO instance = entityManager.find(SeccionCotizacionDTO.class, id);
			try{
				entityManager.refresh(instance);
			}catch (IllegalArgumentException e){}
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all SeccionCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SeccionCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<SeccionCotizacionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<SeccionCotizacionDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding SeccionCotizacionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from SeccionCotizacionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	public List<SeccionCotizacionDTO> findByValorPrimaNeta(Object valorPrimaNeta
	) {
		return findByProperty(VALORPRIMANETA, valorPrimaNeta
		);
	}
	
	public List<SeccionCotizacionDTO> findByValorSumaAsegurada(Object valorSumaAsegurada
	) {
		return findByProperty(VALORSUMAASEGURADA, valorSumaAsegurada
		);
	}
	
	public List<SeccionCotizacionDTO> findByClaveObligatoriedad(Object claveObligatoriedad
	) {
		return findByProperty(CLAVEOBLIGATORIEDAD, claveObligatoriedad
		);
	}
	
	public List<SeccionCotizacionDTO> findByClaveContrato(Object claveContrato
	) {
		return findByProperty(CLAVECONTRATO, claveContrato
		);
	}
	
	
	/**
	 * Find all SeccionCotizacionDTO entities.
	  	  @return List<SeccionCotizacionDTO> all SeccionCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionCotizacionDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all SeccionCotizacionDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from SeccionCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SeccionCotizacionDTO> listarPorIncisoCotizacionId(IncisoCotizacionId idToInciso){
			LogDeMidasEJB3.log("finding by IncisoCotizacionId all SeccionCotizacionDTO instances", Level.INFO, null);
			String queryString = "";
			
			try {
				if (idToInciso==null || idToInciso.getIdToCotizacion()==null || idToInciso.getNumeroInciso()==null)
					return null;
				entityManager.flush();
				queryString = "select model from SeccionCotizacionDTO model where";
				queryString += " model.id.idToCotizacion =:idToCotizacion ";
				queryString += " and model.id.numeroInciso =:numeroInciso and model.claveContrato = 1 " +
						"order by model.seccionDTO.numeroSecuencia";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idToCotizacion", idToInciso.getIdToCotizacion());
				query.setParameter("numeroInciso", idToInciso.getNumeroInciso());
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
			} catch (RuntimeException re) {
				re.printStackTrace();
				LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
			}
	}
	/**
	 * Lista Todas las lineas de negocio de una cotizacion si el idToCotizacion es null
	 * regresa todas las lineas de negocio en la bd
	 * @author Martin
	 * @param idToCotizacion - id de la cotizaci�n
	 * @return List<SeccionCotizacionDTO>
	 */
	public List<SeccionCotizacionDTO> listarPorCotizacion(BigDecimal idToContizacion){
		try{
			StringBuffer queryString = new StringBuffer();
			queryString.append("SELECT DISTINCT model from SeccionCotizacionDTO model");
			if(idToContizacion != null){
				queryString.append(" WHERE model.id.idToCotizacion = :idToCotizacion");
			}
			queryString.append(" ORDER BY model.seccionDTO.numeroSecuencia");
			Query query = entityManager.createQuery(queryString.toString());
			if(idToContizacion != null){
				query.setParameter("idToCotizacion", idToContizacion);
			}
			return query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SeccionCotizacionDTO> listarSeccionesPrimerRiesgo(BigDecimal idToCotizacion) {
		try {
			List<SeccionCotizacionDTO> seccionesAgrupadas = new ArrayList<SeccionCotizacionDTO>();
			String queryString = "select distinct model.seccionDTO.idToSeccion, SUM(model.valorPrimaNeta), SUM(model.valorSumaAsegurada) "
					+ "from SeccionCotizacionDTO model where "
					+ "model.id.idToCotizacion = :idToCotizacion and model.claveContrato = 1 "
					+ "and model.seccionDTO.clavePrimerRiesgo = 1 "
					+ "and model.seccionDTO.idToSeccion not in " +
							"(select distinct c.id.idToSeccion from CoberturaCotizacionDTO c " +
							"where c.claveFacultativo = 1 and c.id.idToCotizacion = :idToCotizacion and c.claveContrato = 1 " +
							"and c.seccionCotizacionDTO.claveContrato = 1) "
					+ "group by model.seccionDTO.idToSeccion, model.seccionDTO.numeroSecuencia "
					+ "order by model.seccionDTO.numeroSecuencia";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List resultados = query.getResultList();
			for (Object resultado : resultados) {
				BigDecimal idToSeccion = (BigDecimal) ((Object[]) resultado)[0];
				Double valorPrimaNetaAcumulada = (Double) ((Object[]) resultado)[1];
				Double valorSumaAseguradaAcumulada = (Double) ((Object[]) resultado)[2];

				queryString = "select model from SeccionCotizacionDTO model where "
						+ "model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion "
						+ "and model.claveContrato = 1 and model.seccionDTO.clavePrimerRiesgo = 1";
				query = entityManager.createQuery(queryString);
				query.setParameter("idToCotizacion", idToCotizacion);
				query.setParameter("idToSeccion", idToSeccion);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				List<SeccionCotizacionDTO> secciones = query.getResultList();
				entityManager.clear();

				SeccionCotizacionDTO seccionCotizacion = secciones.get(0);
				seccionCotizacion.setValorPrimaNeta(valorPrimaNetaAcumulada);
				seccionCotizacion.setValorSumaAsegurada(valorSumaAseguradaAcumulada);
				seccionesAgrupadas.add(seccionCotizacion);
			}
			return seccionesAgrupadas;

		} catch (RuntimeException re) {
			re.printStackTrace();
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public List<SeccionCotizacionDTO> listarSeccionesContratadas(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("finding SeccionCotizacionDTO instances with idToCotizacion: " + idToCotizacion,
				Level.INFO, null);
		try {
			List<SeccionCotizacionDTO> seccionesAgrupadas = new ArrayList<SeccionCotizacionDTO>();
			String queryString = "select distinct model.seccionDTO.idToSeccion, SUM(model.valorPrimaNeta), SUM(model.valorSumaAsegurada), model.seccionDTO.numeroSecuencia " +
					"from SeccionCotizacionDTO model where " +
					"model.id.idToCotizacion =:idToCotizacion and model.claveContrato = 1 " +
					"group by model.seccionDTO.idToSeccion, model.seccionDTO.numeroSecuencia order by model.seccionDTO.numeroSecuencia";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List resultados = query.getResultList();
			for(Object resultado : resultados) {
				BigDecimal idToSeccion = (BigDecimal) ((Object[])resultado)[0];
				Double valorPrimaNetaAcumulada = (Double) ((Object[])resultado)[1];
				Double valorSumaAseguradaAcumulada = (Double) ((Object[])resultado)[2];
		
				queryString = "select model from SeccionCotizacionDTO model where " +
						"model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion " +
						"and model.claveContrato = 1 order by model.seccionDTO.numeroSecuencia";
				query = entityManager.createQuery(queryString);
				query.setParameter("idToCotizacion", idToCotizacion);
				query.setParameter("idToSeccion", idToSeccion);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				List<SeccionCotizacionDTO> secciones = query.getResultList();
				entityManager.clear();

				SeccionCotizacionDTO seccionCotizacion = secciones.get(0);
				seccionCotizacion.setValorPrimaNeta(valorPrimaNetaAcumulada);
				seccionCotizacion.setValorSumaAsegurada(valorSumaAseguradaAcumulada);
				seccionesAgrupadas.add(seccionCotizacion);

				List<CoberturaCotizacionDTO> coberturasAgrupadas = new ArrayList<CoberturaCotizacionDTO>();
				/*queryString = "select distinct model.id.idToCobertura, SUM(model.valorPrimaNeta), SUM(model.valorSumaAsegurada), model.coberturaSeccionDTO.coberturaDTO.codigo " +
						"from CoberturaCotizacionDTO model where " +
						"model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion " +
						"and model.claveContrato = 1 group by model.id.idToCobertura, model.coberturaSeccionDTO.coberturaDTO.codigo " +
						"order by model.coberturaSeccionDTO.coberturaDTO.codigo";*/
				queryString = "SELECT t0.idtocobertura, SUM (t0.valorprimaneta), SUM (t0.valorsumaasegurada), t1.codigocobertura " +
						"FROM midas.tocoberturacot t0, midas.toseccioncot t2, midas.tocobertura t1 " +
						"WHERE t0.idtocotizacion = " + idToCotizacion + " " +
								"AND t0.idtoseccion = " + idToSeccion + " " +
								"AND t0.clavecontrato = 1 " +
								"AND t2.idToCotizacion = t0.idToCotizacion " +
								"AND t2.numeroInciso = t0.numeroInciso " +
								"AND t2.idtoseccion = t0.idtoseccion " +
								"AND t2.claveContrato = 1 " +
								"AND t1.idtocobertura = t0.idtocobertura " +
						"GROUP BY t0.idtocobertura, t1.codigocobertura ORDER BY t1.codigocobertura ASC";
				query = entityManager.createNativeQuery(queryString);
				/*query.setParameter("idToCotizacion", idToCotizacion);
				query.setParameter("idToSeccion", seccionCotizacion.getId().getIdToSeccion());*/
				List resultadosCobertura = query.getResultList();
				for(Object resultadoCobertura : resultadosCobertura) {
					BigDecimal idToCobertura = (BigDecimal) ((Object[])resultadoCobertura)[0];
					valorPrimaNetaAcumulada = ((BigDecimal) ((Object[])resultadoCobertura)[1]).doubleValue();
					valorSumaAseguradaAcumulada = ((BigDecimal) ((Object[])resultadoCobertura)[2]).doubleValue();
					queryString = "select model from CoberturaCotizacionDTO model " +
							"where model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion " +
							"and model.id.idToCobertura = :idToCobertura and model.claveContrato = 1 " +
							"order by model.coberturaSeccionDTO.coberturaDTO.codigo";
					query = entityManager.createQuery(queryString);
					query.setParameter("idToCotizacion", idToCotizacion);
					query.setParameter("idToSeccion", idToSeccion);
					query.setParameter("idToCobertura", idToCobertura);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					List<CoberturaCotizacionDTO> coberturas = query.getResultList();
					entityManager.clear();

					CoberturaCotizacionDTO cobertura = coberturas.get(0);
					cobertura.setValorPrimaNeta(valorPrimaNetaAcumulada);
					cobertura.setValorSumaAsegurada(valorSumaAseguradaAcumulada);
					coberturasAgrupadas.add(cobertura);
				}
				seccionCotizacion.setCoberturaCotizacionLista(coberturasAgrupadas);
			}
			return seccionesAgrupadas;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find SeccionCotizacionDTO failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SeccionCotizacionDTO> listarSeccionesContratadasPorCotizacion(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("finding SeccionCotizacionDTO instances with idToCotizacion: " + idToCotizacion,
				Level.INFO, null);
		try {
			List<SeccionCotizacionDTO> seccionesAgrupadas = new ArrayList<SeccionCotizacionDTO>();
			String queryString = "select distinct model.seccionDTO.idToSeccion, SUM(model.valorPrimaNeta), SUM(model.valorSumaAsegurada), model.seccionDTO.numeroSecuencia " +
					"from SeccionCotizacionDTO model where " +
					"model.id.idToCotizacion =:idToCotizacion and model.claveContrato = 1 " +
					"group by model.seccionDTO.idToSeccion, model.seccionDTO.numeroSecuencia order by model.seccionDTO.numeroSecuencia";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List resultados = query.getResultList();
			for(Object resultado : resultados) {
				BigDecimal idToSeccion = (BigDecimal) ((Object[])resultado)[0];
				Double valorPrimaNetaAcumulada = (Double) ((Object[])resultado)[1];
				Double valorSumaAseguradaAcumulada = (Double) ((Object[])resultado)[2];
		
				queryString = "select model from SeccionCotizacionDTO model where " +
						"model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion " +
						"and model.claveContrato = 1 order by model.seccionDTO.numeroSecuencia";
				query = entityManager.createQuery(queryString);
				query.setParameter("idToCotizacion", idToCotizacion);
				query.setParameter("idToSeccion", idToSeccion);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				List<SeccionCotizacionDTO> secciones = query.getResultList();
				entityManager.clear();

				SeccionCotizacionDTO seccionCotizacion = secciones.get(0);
				seccionCotizacion.setValorPrimaNeta(valorPrimaNetaAcumulada);
				seccionCotizacion.setValorSumaAsegurada(valorSumaAseguradaAcumulada);
				seccionesAgrupadas.add(seccionCotizacion);

				List<CoberturaCotizacionDTO> coberturasAgrupadas = new ArrayList<CoberturaCotizacionDTO>();
				queryString = "select distinct model.id.idToCobertura, SUM(model.valorPrimaNeta), SUM(model.valorSumaAsegurada), model.coberturaSeccionDTO.coberturaDTO.codigo " +
						"from CoberturaCotizacionDTO model where " +
						"model.id.idToCotizacion =:idToCotizacion and model.id.idToSeccion = :idToSeccion " +
						"and model.claveContrato = 1 group by model.id.idToCobertura, model.coberturaSeccionDTO.coberturaDTO.codigo " +
						"order by model.coberturaSeccionDTO.coberturaDTO.codigo";
				query = entityManager.createQuery(queryString);
				query.setParameter("idToCotizacion", idToCotizacion);
				query.setParameter("idToSeccion", seccionCotizacion.getId().getIdToSeccion());
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				List resultadosCobertura = query.getResultList();
				for(Object resultadoCobertura : resultadosCobertura) {
					BigDecimal idToCobertura = (BigDecimal) ((Object[])resultadoCobertura)[0];
					valorPrimaNetaAcumulada = (Double) ((Object[])resultadoCobertura)[1];
					valorSumaAseguradaAcumulada = (Double) ((Object[])resultadoCobertura)[2];
					queryString = "select model from CoberturaCotizacionDTO model " +
							"where model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion " +
							"and model.id.idToCobertura = :idToCobertura and model.claveContrato = 1 " +
							"order by model.coberturaSeccionDTO.coberturaDTO.codigo";
					query = entityManager.createQuery(queryString);
					query.setParameter("idToCotizacion", idToCotizacion);
					query.setParameter("idToSeccion", idToSeccion);
					query.setParameter("idToCobertura", idToCobertura);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					List<CoberturaCotizacionDTO> coberturas = query.getResultList();
					entityManager.clear();

					CoberturaCotizacionDTO cobertura = coberturas.get(0);
					cobertura.setValorPrimaNeta(valorPrimaNetaAcumulada);
					cobertura.setValorSumaAsegurada(valorSumaAseguradaAcumulada);
					coberturasAgrupadas.add(cobertura);
				}
				seccionCotizacion.setCoberturaCotizacionLista(coberturasAgrupadas);
			}
			return seccionesAgrupadas;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find SeccionCotizacionDTO failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SeccionCotizacionDTO> listarSeccionesContratadasPorCotizacion(BigDecimal idToCotizacion,Short claveContrato) {
		LogDeMidasEJB3.log("finding SeccionCotizacionDTO instances with idToCotizacion: " + idToCotizacion,Level.INFO, null);
		try {
			String queryString = "select model from SeccionCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion " +
					"and model.claveContrato = :claveContrato order by model.seccionDTO.numeroSecuencia";
			Query query;
			query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("claveContrato", claveContrato);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<SeccionCotizacionDTO> secciones = query.getResultList();
			return secciones;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find SeccionCotizacionDTO failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SeccionCotizacionDTO> listarSeccionesContratadasPorCotizacion(BigDecimal idToCotizacion,Short claveContrato,boolean aplicarMerge) {
		LogDeMidasEJB3.log("finding SeccionCotizacionDTO instances with idToCotizacion: " + idToCotizacion,Level.INFO, null);
		try {
			String queryString = "select model from SeccionCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion " +
					"and model.claveContrato = :claveContrato order by model.seccionDTO.numeroSecuencia";
			Query query;
			query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("claveContrato", claveContrato);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<SeccionCotizacionDTO> secciones = query.getResultList();
			if(aplicarMerge && !secciones.isEmpty()){
				for(SeccionCotizacionDTO seccionTMP : secciones){
					entityManager.refresh(seccionTMP);
				}
			}
			return secciones;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find SeccionCotizacionDTO failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SeccionCotizacionDTO> listarSeccionesContratadas(BigDecimal idToCotizacion,BigDecimal numeroInciso) {
		LogDeMidasEJB3.log("finding SeccionCotizacionDTO instances with idToCotizacion: " + idToCotizacion+" and numeroInciso: "+numeroInciso,Level.INFO, null);
		try {
			String queryString = "select model from SeccionCotizacionDTO model where " +
				"model.id.idToCotizacion = :idToCotizacion and model.id.numeroInciso = :numeroInciso " +
				"and model.claveContrato = 1 order by model.seccionDTO.numeroSecuencia";
			Query query;
			query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<SeccionCotizacionDTO> secciones = query.getResultList();
			return secciones;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find SeccionCotizacionDTO failed", Level.SEVERE, re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public List<SeccionCotizacionDTO> listarSeccionesLUC(BigDecimal idToCotizacion) {
		try {
			List<SeccionCotizacionDTO> seccionesAgrupadas = new ArrayList<SeccionCotizacionDTO>();
			String queryString = "select distinct model.seccionDTO.idToSeccion, SUM(model.valorPrimaNeta), SUM(model.valorSumaAsegurada) "
					+ "from SeccionCotizacionDTO model where "
					+ "model.id.idToCotizacion = :idToCotizacion and model.claveContrato = 1 "
					+ "and model.seccionDTO.claveLuc = 1 "
					+ "and model.seccionDTO.idToSeccion not in " +
							"(select distinct c.id.idToSeccion from CoberturaCotizacionDTO c " +
							"where c.claveFacultativo = 1 and c.id.idToCotizacion = :idToCotizacion and c.claveContrato = 1 " +
							"and c.seccionCotizacionDTO.claveContrato = 1) "
					+ "group by model.seccionDTO.idToSeccion, model.seccionDTO.numeroSecuencia "
					+ "order by model.seccionDTO.numeroSecuencia";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List resultados = query.getResultList();
			for (Object resultado : resultados) {
				BigDecimal idToSeccion = (BigDecimal) ((Object[]) resultado)[0];
				Double valorPrimaNetaAcumulada = (Double) ((Object[]) resultado)[1];
				//Double valorSumaAseguradaAcumulada = (Double) ((Object[]) resultado)[2];

				queryString = "select model from SeccionCotizacionDTO model where "
						+ "model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion "
						+ "and model.claveContrato = 1 and model.seccionDTO.claveLuc = 1";
				query = entityManager.createQuery(queryString);
				query.setParameter("idToCotizacion", idToCotizacion);
				query.setParameter("idToSeccion", idToSeccion);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				List<SeccionCotizacionDTO> secciones = query.getResultList();
				entityManager.clear();

				SeccionCotizacionDTO seccionCotizacion = secciones.get(0);
				seccionCotizacion.setValorPrimaNeta(valorPrimaNetaAcumulada);
				seccionCotizacion
						.setValorSumaAsegurada(0d);
				seccionesAgrupadas.add(seccionCotizacion);
			}
			return seccionesAgrupadas;

		} catch (RuntimeException re) {
			re.printStackTrace();
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * M�todo que regresa la lista de entidades SeccionCotizacion relacionadas con una cotizacion en el numero del inciso recibido.
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return List<SeccionCotizacionDTO>
	 * @author Martin
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionCotizacionDTO> listarPorCotizacionNumeroInciso(BigDecimal idToCotizacion,BigDecimal numeroInciso){
			LogDeMidasEJB3.log("finding by IncisoCotizacionId all SeccionCotizacionDTO instances", Level.INFO, null);
			String queryString = "";
			try {
				if (numeroInciso==null || idToCotizacion==null )
					return null;
				
				queryString = "select model from SeccionCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion " +
						"and model.id.numeroInciso = :numeroInciso order by model.seccionDTO.numeroSecuencia";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idToCotizacion", idToCotizacion);
				query.setParameter("numeroInciso", numeroInciso);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				List<SeccionCotizacionDTO> lista = query.getResultList();
				for(SeccionCotizacionDTO seccion : lista)
					entityManager.refresh(seccion);
				return lista;
			} catch (RuntimeException re) {
				re.printStackTrace();
				LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
			}
	}
	
	@SuppressWarnings("unchecked")
	public Double getSumatoriaCoberturasBasicas(SeccionCotizacionDTOId id){
		LogDeMidasEJB3.log("Sumatoria de Coberturas Basicas", Level.INFO,
				null);
		try {
			String queryString = "";
			Double sumaAsegurada = 0D;
			queryString = "select sum(t.valorsumaasegurada)SUMAASEGURADA";
			queryString += " from midas.tocoberturacot t, midas.tocobertura c";
			queryString += " WHERE t.idtocotizacion = "+ id.getIdToCotizacion();
			queryString += " and c.idtocobertura = t.idtocobertura and c.clavetiposumaasegurada = 1";
			queryString += " and t.numeroinciso = "+id.getNumeroInciso();
			queryString += " and t.idtoseccion = "+id.getIdToSeccion();
			queryString += "and t.claveContrato = 1";
			Query query = entityManager.createNativeQuery(queryString);
			Object result = query.getSingleResult();
			if(result instanceof List)  {
				if(result != null && ((List)result).get(0) != null)
					sumaAsegurada = (Double)((BigDecimal)((List)result).get(0)).doubleValue();
			} else if (result instanceof BigDecimal) {
				sumaAsegurada = (Double)((BigDecimal)result).doubleValue();
			}
			return sumaAsegurada;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("count failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Long obtenerTotalSeccionesContratadas(
			BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		LogDeMidasEJB3.log("Entro a SeccionCotizacionFacade.obtenerTotalSeccionesContratadas", Level.INFO, null);
		String queryString = "";
		try {
			if (numeroInciso==null || idToCotizacion==null )
				return null;
			
			queryString = "select count(model) from SeccionCotizacionDTO model where " +
			"model.id.idToCotizacion = :idToCotizacion and model.id.numeroInciso = :numeroInciso " +
			"and model.claveContrato = 1";
			Query query;
			query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			
			return (Long) query.getSingleResult();
		} catch (RuntimeException re) {
			re.printStackTrace();
			LogDeMidasEJB3.log("SeccionCotizacionFacade.obtenerTotalSeccionesContratadas fallo", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SeccionCotizacionDTO> listarSeccionesContratadasIgualacion(BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		LogDeMidasEJB3.log("finding SeccionCotizacionDTO instances with idToCotizacion: " + idToCotizacion,
				Level.INFO, null);
		try {
			List<SeccionCotizacionDTO> seccionesAgrupadas = new ArrayList<SeccionCotizacionDTO>();
			String queryString = "select distinct model.seccionDTO.idToSeccion, SUM(model.valorPrimaNeta), SUM(model.valorSumaAsegurada), model.seccionDTO.numeroSecuencia " +
					"from SeccionCotizacionDTO model where " +
					"model.id.idToCotizacion =:idToCotizacion and model.claveContrato = 1 and " +
					"model.id.numeroInciso =:numeroInciso " +
					"group by model.seccionDTO.idToSeccion, model.seccionDTO.numeroSecuencia order by model.seccionDTO.numeroSecuencia";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List resultados = query.getResultList();
			for(Object resultado : resultados) {
				BigDecimal idToSeccion = (BigDecimal) ((Object[])resultado)[0];
				Double valorPrimaNetaAcumulada = (Double) ((Object[])resultado)[1];
				Double valorSumaAseguradaAcumulada = (Double) ((Object[])resultado)[2];
		
				queryString = "select model from SeccionCotizacionDTO model where " +
						"model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion " +
						"and model.id.numeroInciso = :numeroInciso " +
						"and model.claveContrato = 1 order by model.seccionDTO.numeroSecuencia";
				query = entityManager.createQuery(queryString);
				query.setParameter("idToCotizacion", idToCotizacion);
				query.setParameter("idToSeccion", idToSeccion);
				query.setParameter("numeroInciso", numeroInciso);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				List<SeccionCotizacionDTO> secciones = query.getResultList();
				entityManager.clear();

				SeccionCotizacionDTO seccionCotizacion = secciones.get(0);
				seccionCotizacion.setValorPrimaNeta(valorPrimaNetaAcumulada);
				seccionCotizacion.setValorSumaAsegurada(valorSumaAseguradaAcumulada);
				seccionesAgrupadas.add(seccionCotizacion);

				List<CoberturaCotizacionDTO> coberturasAgrupadas = new ArrayList<CoberturaCotizacionDTO>();

				queryString = "SELECT t0.idtocobertura, SUM (t0.valorprimaneta), SUM (t0.valorsumaasegurada), t1.codigocobertura " +
						"FROM midas.tocoberturacot t0, midas.toseccioncot t2, midas.tocobertura t1 " +
						"WHERE t0.idtocotizacion = " + idToCotizacion + " " +
								"AND t0.idtoseccion = " + idToSeccion + " " +
								"AND t0.numeroInciso = " + numeroInciso + " " +
								"AND t0.clavecontrato = 1 " +
								"AND t2.idToCotizacion = t0.idToCotizacion " +
								"AND t2.numeroInciso = t0.numeroInciso " +
								"AND t2.idtoseccion = t0.idtoseccion " +
								"AND t2.claveContrato = 1 " +
								"AND t1.idtocobertura = t0.idtocobertura " +
						"GROUP BY t0.idtocobertura, t1.codigocobertura ORDER BY t1.codigocobertura ASC";
				query = entityManager.createNativeQuery(queryString);
				List resultadosCobertura = query.getResultList();
				for(Object resultadoCobertura : resultadosCobertura) {
					BigDecimal idToCobertura = (BigDecimal) ((Object[])resultadoCobertura)[0];
					valorPrimaNetaAcumulada = ((BigDecimal) ((Object[])resultadoCobertura)[1]).doubleValue();
					valorSumaAseguradaAcumulada = ((BigDecimal) ((Object[])resultadoCobertura)[2]).doubleValue();
					queryString = "select model from CoberturaCotizacionDTO model " +
							"where model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion " +
							"and model.id.numeroInciso = :numeroInciso " +
							"and model.id.idToCobertura = :idToCobertura and model.claveContrato = 1 " +
							"order by model.coberturaSeccionDTO.coberturaDTO.codigo";
					query = entityManager.createQuery(queryString);
					query.setParameter("idToCotizacion", idToCotizacion);
					query.setParameter("idToSeccion", idToSeccion);
					query.setParameter("idToCobertura", idToCobertura);
					query.setParameter("numeroInciso", numeroInciso);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					List<CoberturaCotizacionDTO> coberturas = query.getResultList();
					entityManager.clear();

					CoberturaCotizacionDTO cobertura = coberturas.get(0);
					cobertura.setValorPrimaNeta(valorPrimaNetaAcumulada);
					cobertura.setValorSumaAsegurada(valorSumaAseguradaAcumulada);
					coberturasAgrupadas.add(cobertura);
				}
				seccionCotizacion.setCoberturaCotizacionLista(coberturasAgrupadas);
			}
			return seccionesAgrupadas;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find SeccionCotizacionDTO failed", Level.SEVERE, re);
			throw re;
		}
	}
}