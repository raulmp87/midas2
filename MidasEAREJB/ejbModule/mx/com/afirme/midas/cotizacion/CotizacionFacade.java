package mx.com.afirme.midas.cotizacion;
// default package

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.plenoreaseguro.PlenoReaseguroDTO;
import mx.com.afirme.midas.catalogos.plenoreaseguro.PlenoReaseguroFacadeRemote;
import mx.com.afirme.midas.consultas.formapago.FormaPagoInterfazServiciosRemote;
import mx.com.afirme.midas.consultas.gastoexpedicion.GastoExpedicionDTO;
import mx.com.afirme.midas.consultas.gastoexpedicion.GastoExpedicionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.endoso.cancelacion.CancelacionEndosoFacadeRemote;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;
import mx.com.afirme.midas.solicitud.SolicitudDTO;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity CotizacionDTO.
 * @see .CotizacionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class CotizacionFacade extends CotizacionSoporte implements CotizacionFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
  
    @EJB
    private FormaPagoInterfazServiciosRemote formaPagoInterfazServicios;
    
    @EJB
    private EndosoFacadeRemote endosoFacade;
    
    @EJB
    private CancelacionEndosoFacadeRemote cancelacionEndosoFacade;
    
    @EJB
    private GastoExpedicionFacadeRemote gastoExpedicionFacade;
    
    @EJB
    private PolizaFacadeRemote polizaFacade;
    
    @EJB
	private CoberturaCotizacionFacadeRemote coberturaCotizacionFacade;
    
    @EJB
    private PlenoReaseguroFacadeRemote plenoReaseguroFacade;
    
//    @EJB
//    private ResumenCotizacionFacadeRemote resumenCotizacionFacade;
		/**
	 Perform an initial save of a previously unsaved CotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public CotizacionDTO save(CotizacionDTO entity) {
    				LogDeMidasEJB3.log("saving CotizacionDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
            			return entity;
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CotizacionDTO entity.
	  @param entity CotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public void delete(CotizacionDTO entity) {
    				LogDeMidasEJB3.log("deleting CotizacionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(CotizacionDTO.class, entity.getIdToCotizacion());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the CotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CotizacionDTO entity to update
	 @return CotizacionDTO the persisted CotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
    public CotizacionDTO update(CotizacionDTO entity) {
    	LogDeMidasEJB3.log("updating CotizacionDTO instance", Level.INFO, null);
    	try {
    		CotizacionDTO result = entityManager.merge(entity);
    		LogDeMidasEJB3.log("update successful", Level.INFO, null);
    		entityManager.flush();
    		return result;
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
        	throw re;
        }
    }
    
    public CotizacionDTO findById( BigDecimal id) {
		LogDeMidasEJB3.log("finding CotizacionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			return entityManager.find(CotizacionDTO.class, id);
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
    }    
    

/**
	 * Find all CotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the CotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<CotizacionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<CotizacionDTO> findByProperty(String propertyName, final Object value
        ) {
		LogDeMidasEJB3.log("finding CotizacionDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from CotizacionDTO model where model."
					+ propertyName + "= :propertyValue order by model.idToCotizacion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}			
	/**
	 * Find all CotizacionDTO entities.
	  	  @return List<CotizacionDTO> all CotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CotizacionDTO> findAll(
		) {
		LogDeMidasEJB3.log("finding all CotizacionEndoso instances",
				Level.INFO, null);
		try {
			entityManager.flush();
			final String queryString = "select model from CotizacionDTO model where model.solicitudDTO.claveTipoSolicitud <> 2";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CotizacionDTO> listarFiltrado(CotizacionDTO cotizacionDTO) {
		try {
			entityManager.flush();
			String queryString = "select model from CotizacionDTO AS model ";	
			String sWhere = "";
			Query query = null;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
//			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "model.solicitudDTO.claveTipoSolicitud", cotizacionDTO.getIdToCotizacion());
			
			if (cotizacionDTO == null)
				return null;
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idToCotizacion", cotizacionDTO.getIdToCotizacion());
			
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoPolizaDTO.idToTipoPoliza", cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
			} catch (NullPointerException e) {}
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.idToSolicitud", cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
			} catch (NullPointerException e) {}
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "direccionCobroDTO.idToDireccion", cotizacionDTO.getDireccionCobroDTO().getIdToDireccion());
			} catch (NullPointerException e ){}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idMoneda", cotizacionDTO.getIdMoneda());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idFormaPago", cotizacionDTO.getIdFormaPago());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicioVigencia", cotizacionDTO.getFechaInicioVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaFinVigencia", cotizacionDTO.getFechaFinVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveEstatus", cotizacionDTO.getClaveEstatus());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaContratante", cotizacionDTO.getNombreEmpresaContratante());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaAsegurado", cotizacionDTO.getNombreEmpresaAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioOrdenTrabajo", cotizacionDTO.getCodigoUsuarioOrdenTrabajo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCotizacion", cotizacionDTO.getCodigoUsuarioCotizacion());
			
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreAsegurado", cotizacionDTO.getNombreAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreContratante", cotizacionDTO.getNombreContratante());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCreacion", cotizacionDTO.getCodigoUsuarioCreacion());
			
			if (cotizacionDTO.getSolicitudDTO() != null) {
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.nombrePersona", cotizacionDTO.getSolicitudDTO().getNombrePersona());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.apellidoPaterno", cotizacionDTO.getSolicitudDTO().getApellidoPaterno());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.apellidoMaterno", cotizacionDTO.getSolicitudDTO().getApellidoMaterno());
				if(cotizacionDTO.getSolicitudDTO().getCodigoAgente() != null) {
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.codigoAgente", cotizacionDTO.getSolicitudDTO().getCodigoAgente());
				}
				if(cotizacionDTO.getSolicitudDTO().getNegocio()!= null){
					sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.negocio.claveNegocio", cotizacionDTO.getSolicitudDTO().getNegocio().getClaveNegocio());
				}
			}
			if (Utilerias.esAtributoQueryValido(sWhere)) {
				queryString = queryString.concat(" where ").concat(sWhere);
				//queryString += " and model.solicitudDTO.claveTipoSolicitud <> " + SistemaPersistencia.SOLICITUD_ENDOSO;
			} else {
				//queryString += " where model.solicitudDTO.claveTipoSolicitud <> " + SistemaPersistencia.SOLICITUD_ENDOSO;
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if(cotizacionDTO.getPrimerRegistroACargar() != null && cotizacionDTO.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(cotizacionDTO.getPrimerRegistroACargar().intValue());
				query.setMaxResults(cotizacionDTO.getNumeroMaximoRegistrosACargar().intValue());
			}
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CotizacionDTO> listarFiltradoCotizacion(CotizacionDTO cotizacionDTO, Date fechaInicial, Date fechaFinal, String agentes, String gerencias,BigDecimal valorPrimaTotalFin,Double descuento,Double descuentoFin) {
		try {
			entityManager.flush();
			String queryString = "select model from CotizacionDTO AS model ";	
			String sWhere = "";
			Query query = null;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
//			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "model.solicitudDTO.claveTipoSolicitud", cotizacionDTO.getIdToCotizacion());
			
			if (cotizacionDTO == null)
				return null;
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idToCotizacion", cotizacionDTO.getIdToCotizacion());
			
			if(cotizacionDTO.getTipoPolizaDTO() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoPolizaDTO.idToTipoPoliza", cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
			
			if(cotizacionDTO.getSolicitudDTO() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.idToSolicitud", cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
			
			if(cotizacionDTO.getDireccionCobroDTO() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "direccionCobroDTO.idToDireccion", cotizacionDTO.getDireccionCobroDTO().getIdToDireccion());

			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idMoneda", cotizacionDTO.getIdMoneda());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idFormaPago", cotizacionDTO.getIdFormaPago());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicioVigencia", cotizacionDTO.getFechaInicioVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaFinVigencia", cotizacionDTO.getFechaFinVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveEstatus", cotizacionDTO.getClaveEstatus());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaContratante", cotizacionDTO.getNombreEmpresaContratante());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaAsegurado", cotizacionDTO.getNombreEmpresaAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioOrdenTrabajo", cotizacionDTO.getCodigoUsuarioOrdenTrabajo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCotizacion", cotizacionDTO.getCodigoUsuarioCotizacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreAsegurado", cotizacionDTO.getNombreAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreContratante", cotizacionDTO.getNombreContratante());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaSeguimiento", cotizacionDTO.getFechaSeguimiento());
			//sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "conflictoNumeroSerie", cotizacionDTO.getConflictoNumeroSerie());
	
			if (cotizacionDTO.getSolicitudDTO() != null) {
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.nombrePersona", cotizacionDTO.getSolicitudDTO().getNombrePersona());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.apellidoPaterno", cotizacionDTO.getSolicitudDTO().getApellidoPaterno());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.apellidoMaterno", cotizacionDTO.getSolicitudDTO().getApellidoMaterno());
				if(cotizacionDTO.getSolicitudDTO().getCodigoAgente() != null) {
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.codigoAgente", cotizacionDTO.getSolicitudDTO().getCodigoAgente());
				}
				if(cotizacionDTO.getSolicitudDTO().getNegocio()!= null){
					sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.negocio.claveNegocio", cotizacionDTO.getSolicitudDTO().getNegocio().getClaveNegocio());
					if(cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio()!= null){
						sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.negocio.idToNegocio", cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio());
					}
				}
				if(cotizacionDTO.getSolicitudDTO().getNegocio().getClaveNegocio()!= null){
					sWhere.concat(" and model.solicitudDTO.negocio.idToNegocio IS NOT NULL") ;
				}
				if(cotizacionDTO.getSolicitudDTO().getProductoDTO() != null){
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "solicitudDTO.productoDTO.idToProducto", cotizacionDTO.getSolicitudDTO().getProductoDTO().getIdToProducto());
				}		
				if(cotizacionDTO.getSolicitudDTO().getIdOficina() != null) {
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.idOficina", cotizacionDTO.getSolicitudDTO().getIdOficina());
				}
				if(cotizacionDTO.getSolicitudDTO().getCodigoEjecutivo() != null) {
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.codigoEjecutivo", cotizacionDTO.getSolicitudDTO().getCodigoEjecutivo());
				}			
				// Filtra las solicitudes en proceso
				sWhere = sWhere + (" and model.solicitudDTO.claveEstatus <> 0");
				
			}

			if(agentes != null && !agentes.equals("")){
				if(cotizacionDTO.getSolicitudDTO() != null )
					cotizacionDTO.setSolicitudDTO(new SolicitudDTO());
				if(sWhere.equals("")){
					sWhere = sWhere + ("");
				}else{
					
				}
				sWhere = sWhere + (" and model.solicitudDTO.codigoAgente in ("+agentes+")") ;
			}
			
			if(gerencias != null && !gerencias.equals("")){
				if(cotizacionDTO.getSolicitudDTO() != null )
					cotizacionDTO.setSolicitudDTO(new SolicitudDTO());
				if(sWhere.equals("")){
					sWhere.concat("");
				}else{
					
				}
				sWhere.concat(" and model.solicitudDTO.idOficina in (:gerencias)");
			}
			if(cotizacionDTO.getCodigoUsuarioCreacion() != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " OR ";
				}
				sWhere += ("TRIM(UPPER(model.codigoUsuarioCreacion)) = :codigoUsuarioCreacion ");
			}
			
			
			if(fechaInicial != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.fechaCreacion > :fechaInicial";
			}
			if(fechaFinal != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.fechaCreacion < :fechaFinal";
			}
			
			if (cotizacionDTO.getTipoPolizaDTO() != null
					&& cotizacionDTO.getTipoPolizaDTO()
							.getClaveAplicaFlotillas() != null) {
				switch (cotizacionDTO.getTipoPolizaDTO()
						.getClaveAplicaFlotillas()) {
				case 0:
					if (sWhere.equals("" +
							"" +
							"" +
							"")) {
						sWhere += "";
					} else {
						sWhere += " and ";
					}
					sWhere += " model.tipoPolizaDTO.claveAplicaFlotillas = 0";
					break;
				case 1:
					if (sWhere.equals("")) {
						sWhere += "";
					} else {
						sWhere += " and ";
					}
					sWhere += " model.tipoPolizaDTO.claveAplicaFlotillas = 1";
				}
			}
			
			if(cotizacionDTO.getValorPrimaTotal() != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.valorPrimaTotal >= :valorPrimaTotal";
			}
			
			if(valorPrimaTotalFin != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.valorPrimaTotal <= :valorPrimaTotalFin";				
			}
			
			
			if(descuento != null && descuentoFin!=null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.porcentajeDescuentoGlobal BETWEEN :descuento AND :descuentoFin";				
			}			
	
			if (Utilerias.esAtributoQueryValido(sWhere)) {
				queryString = queryString.concat(" where ").concat(sWhere);
				//queryString += " and model.solicitudDTO.claveTipoSolicitud <> " + SistemaPersistencia.SOLICITUD_ENDOSO;
			} else {
				//queryString += " where model.solicitudDTO.claveTipoSolicitud <> " + SistemaPersistencia.SOLICITUD_ENDOSO;
			}
			queryString = queryString + " ORDER BY model.fechaCreacion desc, model.idToCotizacion";
			query = entityManager.createQuery(queryString,CotizacionDTO.class);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if(fechaInicial != null && fechaFinal != null){
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(fechaInicial);
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
				
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(fechaFinal);
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				
				System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
				System.out.println("Poliza: Rango de Fecha fin ->" + gcFechaFin.getTime());
				
				query.setParameter("fechaInicial", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
				query.setParameter("fechaFinal", gcFechaFin.getTime(), TemporalType.TIMESTAMP);					
			}else{
				if(fechaInicial != null){			
					query.setParameter("fechaInicial", fechaInicial);
				}
				if(fechaFinal != null){
					query.setParameter("fechaFinal", fechaFinal);
				}
			}
			
			if(gerencias != null && !gerencias.equals("")){
				query.setParameter("gerencias", gerencias);
			}
			
			if(cotizacionDTO.getValorPrimaTotal() != null){
				query.setParameter("valorPrimaTotal", cotizacionDTO.getValorPrimaTotal());
			}
			
			if(valorPrimaTotalFin != null){
				query.setParameter("valorPrimaTotalFin", valorPrimaTotalFin);
			}
			
			if(descuento != null){
				query.setParameter("descuento", descuento);
			}	
			
			if(descuentoFin != null){
				query.setParameter("descuentoFin", descuentoFin);
			}	
			
			if(cotizacionDTO.getCodigoUsuarioCreacion()!= null){
				query.setParameter("codigoUsuarioCreacion", cotizacionDTO.getCodigoUsuarioCreacion().toUpperCase().trim());
			}
			if(cotizacionDTO.getPrimerRegistroACargar() != null && cotizacionDTO.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(cotizacionDTO.getPrimerRegistroACargar().intValue());
				query.setMaxResults(cotizacionDTO.getNumeroMaximoRegistrosACargar().intValue());
			}

			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			return (List<CotizacionDTO>) query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public Long obtenerTotalFiltradoCotizacion(CotizacionDTO cotizacionDTO, Date fechaInicial, Date fechaFinal, String agentes,String gerencias, BigDecimal valorPrimaTotalFin,Double descuento,Double descuentoFin) {
		try {
			entityManager.flush();
			String queryString = "select count(model) from CotizacionDTO AS model ";	
			String sWhere = "";
			Query query = null;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (cotizacionDTO == null)
				return null;
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idToCotizacion", cotizacionDTO.getIdToCotizacion());
			
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoPolizaDTO.idToTipoPoliza", cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
			} catch (NullPointerException e) {}
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.idToSolicitud", cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
			} catch (NullPointerException e) {}
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "direccionCobroDTO.idToDireccion", cotizacionDTO.getDireccionCobroDTO().getIdToDireccion());
			} catch (NullPointerException e ){}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idMoneda", cotizacionDTO.getIdMoneda());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idFormaPago", cotizacionDTO.getIdFormaPago());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicioVigencia", cotizacionDTO.getFechaInicioVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaFinVigencia", cotizacionDTO.getFechaFinVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveEstatus", cotizacionDTO.getClaveEstatus());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaContratante", cotizacionDTO.getNombreEmpresaContratante());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaAsegurado", cotizacionDTO.getNombreEmpresaAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioOrdenTrabajo", cotizacionDTO.getCodigoUsuarioOrdenTrabajo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCotizacion", cotizacionDTO.getCodigoUsuarioCotizacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreAsegurado", cotizacionDTO.getNombreAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreContratante", cotizacionDTO.getNombreContratante());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaSeguimiento", cotizacionDTO.getFechaSeguimiento());
			//sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "conflictoNumeroSerie", cotizacionDTO.getConflictoNumeroSerie());
	
			if (cotizacionDTO.getSolicitudDTO() != null) {
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.nombrePersona", cotizacionDTO.getSolicitudDTO().getNombrePersona());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.apellidoPaterno", cotizacionDTO.getSolicitudDTO().getApellidoPaterno());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.apellidoMaterno", cotizacionDTO.getSolicitudDTO().getApellidoMaterno());
				if(cotizacionDTO.getSolicitudDTO().getCodigoAgente() != null) {
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.codigoAgente", cotizacionDTO.getSolicitudDTO().getCodigoAgente());
				}
				if(cotizacionDTO.getSolicitudDTO().getNegocio()!= null){
					sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.negocio.claveNegocio", cotizacionDTO.getSolicitudDTO().getNegocio().getClaveNegocio());
					if(cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio()!= null){
						sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.negocio.idToNegocio", cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio());	
					}
				}
				if(cotizacionDTO.getSolicitudDTO().getProductoDTO() != null){
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "solicitudDTO.productoDTO.idToProducto", cotizacionDTO.getSolicitudDTO().getProductoDTO().getIdToProducto());
				}		
				if(cotizacionDTO.getSolicitudDTO().getIdOficina() != null) {
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.idOficina", cotizacionDTO.getSolicitudDTO().getIdOficina());
				}
				if(cotizacionDTO.getSolicitudDTO().getCodigoEjecutivo() != null) {
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.codigoEjecutivo", cotizacionDTO.getSolicitudDTO().getCodigoEjecutivo());
				}				
				sWhere = sWhere + (" and model.solicitudDTO.claveEstatus <> 0");
			}
			if(agentes != null && !agentes.equals("")){
				if(cotizacionDTO.getSolicitudDTO() != null )
					cotizacionDTO.setSolicitudDTO(new SolicitudDTO());
				if(sWhere.equals("")){
					sWhere = sWhere + ("");
				}else{
					
				}
				sWhere = sWhere + (" and model.solicitudDTO.codigoAgente in ("+agentes+")") ;
			}
			if(gerencias != null && !gerencias.equals("")){
				if(cotizacionDTO.getSolicitudDTO() != null )
					cotizacionDTO.setSolicitudDTO(new SolicitudDTO());
				if(sWhere.equals("")){
					sWhere.concat("");
				}else{
					
				}
				sWhere.concat(" and model.solicitudDTO.idOficina in (:gerencias)") ;
			}
			if(cotizacionDTO.getCodigoUsuarioCreacion() != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " OR ";
				}
				sWhere += ("TRIM(UPPER(model.codigoUsuarioCreacion)) = :codigoUsuarioCreacion ") ;
			}
			
			
			if(fechaInicial != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.fechaCreacion > :fechaInicial";
			}
			if(fechaFinal != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.fechaCreacion < :fechaFinal";
			}
			
			if (cotizacionDTO.getTipoPolizaDTO() != null
					&& cotizacionDTO.getTipoPolizaDTO()
							.getClaveAplicaFlotillas() != null) {
				switch (cotizacionDTO.getTipoPolizaDTO()
						.getClaveAplicaFlotillas()) {
				case 0:
					if (sWhere.equals("")) {
						sWhere += "";
					} else {
						sWhere += " and ";
					}
					sWhere += " model.tipoPolizaDTO.claveAplicaFlotillas = 0";
					break;
				case 1:
					if (sWhere.equals("")) {
						sWhere += "";
					} else {
						sWhere += " and ";
					}
					sWhere += " model.tipoPolizaDTO.claveAplicaFlotillas = 1";
				}
			}
			
			if(cotizacionDTO.getValorPrimaTotal() != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.valorPrimaTotal >= :valorPrimaTotal";
			}
			
			if(valorPrimaTotalFin != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.valorPrimaTotal <= :valorPrimaTotalFin";				
			}
			
			
			if(descuento != null && descuentoFin!=null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.porcentajeDescuentoGlobal BETWEEN :descuento AND :descuentoFin";				
			}				
	
			if (Utilerias.esAtributoQueryValido(sWhere)) {
				queryString = queryString.concat(" where ").concat(sWhere);
				//queryString += " and model.solicitudDTO.claveTipoSolicitud <> " + SistemaPersistencia.SOLICITUD_ENDOSO;
			} else {
				//queryString += " where model.solicitudDTO.claveTipoSolicitud <> " + SistemaPersistencia.SOLICITUD_ENDOSO;
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if(fechaInicial != null && fechaFinal != null){
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(fechaInicial);
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
				
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(fechaFinal);
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				
				System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
				System.out.println("Poliza: Rango de Fecha fin ->" + gcFechaFin.getTime());
				
				query.setParameter("fechaInicial", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
				query.setParameter("fechaFinal", gcFechaFin.getTime(), TemporalType.TIMESTAMP);					
			}else{
				if(fechaInicial != null){			
					query.setParameter("fechaInicial", fechaInicial);
				}
				if(fechaFinal != null){
					query.setParameter("fechaFinal", fechaFinal);
				}
			}
//			if (agentes != null && !agentes.equals("")) {
//				query.setParameter("agentes", agentes);
//			}
			
			if(gerencias != null && !gerencias.equals("")){
				query.setParameter("gerencias", gerencias);
			}
			
			if(cotizacionDTO.getValorPrimaTotal() != null){
				query.setParameter("valorPrimaTotal", cotizacionDTO.getValorPrimaTotal());
			}
			
			if(valorPrimaTotalFin != null){
				query.setParameter("valorPrimaTotalFin", valorPrimaTotalFin);
			}
			
			if(descuento != null){
				query.setParameter("descuento", descuento);
			}

			if(descuentoFin != null){
				query.setParameter("descuentoFin", descuentoFin);
			}			
			
			if(cotizacionDTO.getCodigoUsuarioCreacion()!= null){
				query.setParameter("codigoUsuarioCreacion", cotizacionDTO.getCodigoUsuarioCreacion().toUpperCase().trim());
			}
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return (Long)query.getSingleResult();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public Long obtenerTotalFiltrado(CotizacionDTO cotizacionDTO) {
		LogDeMidasEJB3.log("Entra a CotizacionFacade.obtenerTotalFiltrado" ,
				Level.INFO, null);
		try {
			if ((cotizacionDTO == null)) {
				return null;
			}
			  
			String queryString = "select count(model) from CotizacionDTO As model ";
			String sWhere = "";
			Query query = null;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idToCotizacion", cotizacionDTO.getIdToCotizacion());
			
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoPolizaDTO.idToTipoPoliza", cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
			} catch (NullPointerException e) {
				
			}
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.idToSolicitud", cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
			} catch (NullPointerException e) {
				
			}
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "direccionCobroDTO.idToDireccion", cotizacionDTO.getDireccionCobroDTO().getIdToDireccion());
			} catch (NullPointerException e) {
				
			}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idMoneda", cotizacionDTO.getIdMoneda());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idFormaPago", cotizacionDTO.getIdFormaPago());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicioVigencia", cotizacionDTO.getFechaInicioVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaFinVigencia", cotizacionDTO.getFechaFinVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveEstatus", cotizacionDTO.getClaveEstatus());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaContratante", cotizacionDTO.getNombreEmpresaContratante());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaAsegurado", cotizacionDTO.getNombreEmpresaAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioOrdenTrabajo", cotizacionDTO.getCodigoUsuarioOrdenTrabajo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCotizacion", cotizacionDTO.getCodigoUsuarioCotizacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreAsegurado", cotizacionDTO.getNombreAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreContratante", cotizacionDTO.getNombreContratante());

			if(cotizacionDTO.getSolicitudDTO() != null){
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.nombrePersona", cotizacionDTO.getSolicitudDTO().getNombrePersona());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.apellidoPaterno", cotizacionDTO.getSolicitudDTO().getApellidoPaterno());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.apellidoMaterno", cotizacionDTO.getSolicitudDTO().getApellidoMaterno());
				if(cotizacionDTO.getSolicitudDTO().getCodigoAgente() != null) {
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.codigoAgente", cotizacionDTO.getSolicitudDTO().getCodigoAgente());
				}
			}
		    
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCreacion", cotizacionDTO.getCodigoUsuarioCreacion());			
			
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				//queryString += " and model.solicitudDTO.claveTipoSolicitud <> " + SistemaPersistencia.SOLICITUD_ENDOSO;
			} else {
				//queryString += " where model.solicitudDTO.claveTipoSolicitud <> " + SistemaPersistencia.SOLICITUD_ENDOSO;
			}
			
			query = entityManager.createQuery(queryString);
			
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			return (Long)query.getSingleResult();
		    
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("CotizacionFacade.obtenerTotalFiltrado fallo", Level.SEVERE, re);
			throw re;
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public CotizacionDTO generarCotizacionASolicitud(BigDecimal idCotizacion, SolicitudDTO solicitudEndoso, String nombreUsuario,
			String nombreUsuarioCotizacion) {
		
		CotizacionDTO cotizacionOriginal = findById(idCotizacion);
		//Se corrigio llamado antes estaba mal el llamado por que estaban el mal posicion los usuarios
		CotizacionDTO cotizacion = creaCopiaCotizacion(cotizacionOriginal, solicitudEndoso,  nombreUsuarioCotizacion,nombreUsuario);
		
		save(cotizacion);

		String queryString = copiarDocumentosDigitales(cotizacion.getIdToCotizacion(),cotizacionOriginal.getIdToCotizacion());
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();				
		LogDeMidasEJB3.log("se copiaron los documentos Digitales", Level.INFO, null);
		
		queryString = copiarDocumentosCotizacion(cotizacion.getIdToCotizacion(),cotizacionOriginal.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();				
		LogDeMidasEJB3.log("se copiaron los documentos anexos de la cotizacion", Level.INFO, null);
		
		queryString = copiarDocumentosReaseguro(cotizacion.getIdToCotizacion(),cotizacionOriginal.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();				
		LogDeMidasEJB3.log("se copiaron los documentos anexos de reaseguro la cotizacion", Level.INFO, null);
		
//			queryString = copiarTextosAdicionales(cotizacion.getIdToCotizacion(),cotizacionOriginal.getIdToCotizacion());
//			query = entityManager.createNativeQuery(queryString);
//			query.executeUpdate();				
//			LogDeMidasEJB3.log("se copiaron los textos adicionales de la cotizacion", Level.INFO, null);			
		copiaRelacionesAnexosTextosCotizacion(cotizacionOriginal, cotizacion);

		
		
		entityManager.merge(cotizacion);				
		entityManager.flush();
		
		copiaRelacionesCotizacion(cotizacionOriginal, cotizacion);

		return cotizacion;
	}
	
	
	@SuppressWarnings("unchecked")
	public CotizacionDTO getCotizacionFull(BigDecimal idToCotizacion){
		
		CotizacionDTO cotizacionDTO= null;
        String queryInciso = 	"select model from IncisoCotizacionDTO AS model where model.id.idToCotizacion=:idCot";
        String querySecciones=	"select model from SeccionCotizacionDTO AS model where model.id.idToCotizacion=:idCot and model.id.numeroInciso=:numInciso " +
        		" order by model.seccionDTO.numeroSecuencia";
        String queryCoberturas=	"select model from CoberturaCotizacionDTO AS model where model.id.idToCotizacion=:idCot and model.id.numeroInciso=:numInciso and model.id.idToSeccion=:idSeccion " +
        		" order by model.coberturaSeccionDTO.coberturaDTO.numeroSecuencia";
        String queryRiesgos=	"select model from RiesgoCotizacionDTO AS model where model.id.idToCotizacion=:idCot and model.id.numeroInciso=:numInciso and model.id.idToSeccion=:idSeccion  and model.id.idToCobertura=:idCober";
        String querySubInciso=  "select model from SubIncisoCotizacionDTO AS model where model.id.idToCotizacion=:idCot and model.id.numeroInciso=:numInciso and model.id.idToSeccion=:idSeccion";	
        Query query;
		cotizacionDTO=this.findById(idToCotizacion);
		
		if(cotizacionDTO!= null){
			
			query = entityManager.createQuery(queryInciso);
			query.setParameter("idCot", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			cotizacionDTO.setIncisoCotizacionDTOs((List<IncisoCotizacionDTO>) query.getResultList());
			if(cotizacionDTO.getIncisoCotizacionDTOs() !=null && !cotizacionDTO.getIncisoCotizacionDTOs().isEmpty()){
				//Se obtienen las secciones;
				for(IncisoCotizacionDTO inciso:cotizacionDTO.getIncisoCotizacionDTOs() ){
					query = entityManager.createQuery(querySecciones);
					query.setParameter("idCot",  inciso.getId().getIdToCotizacion());
					query.setParameter("numInciso", inciso.getId().getNumeroInciso());
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					inciso.setSeccionCotizacionList((List<SeccionCotizacionDTO>)query.getResultList());
					
					if(inciso.getSeccionCotizacionList()!=null && !inciso.getSeccionCotizacionList().isEmpty()){
						//Se obtienen las coberturas
						for(SeccionCotizacionDTO seccion:inciso.getSeccionCotizacionList()){
							query = entityManager.createQuery(queryCoberturas);
							query.setParameter("idCot", seccion.getId().getIdToCotizacion());
							query.setParameter("numInciso", seccion.getId().getNumeroInciso());
							query.setParameter("idSeccion", seccion.getId().getIdToSeccion());
							query.setHint(QueryHints.REFRESH, HintValues.TRUE);
							seccion.setCoberturaCotizacionLista((List<CoberturaCotizacionDTO>) query.getResultList());
							if(seccion.getCoberturaCotizacionLista()!=null&& !seccion.getCoberturaCotizacionLista().isEmpty()){
								//Se obtienen los riesgos
								for(CoberturaCotizacionDTO cobertura:seccion.getCoberturaCotizacionLista()){
									query = entityManager.createQuery(queryRiesgos);
									query.setParameter("idCot", cobertura.getId().getIdToCotizacion());
									query.setParameter("numInciso", cobertura.getId().getNumeroInciso());
									query.setParameter("idSeccion", cobertura.getId().getIdToSeccion());
									query.setParameter("idCober", cobertura.getId().getIdToCobertura());
									query.setHint(QueryHints.REFRESH, HintValues.TRUE);
									cobertura.setRiesgoCotizacionLista((List<RiesgoCotizacionDTO>) query.getResultList());
									
								}
								
							}
							
							
							query = entityManager.createQuery(querySubInciso);
							query.setParameter("idCot", seccion.getId().getIdToCotizacion());
							query.setParameter("numInciso", seccion.getId().getNumeroInciso());
							query.setParameter("idSeccion", seccion.getId().getIdToSeccion());
							query.setHint(QueryHints.REFRESH, HintValues.TRUE);
							seccion.setSubIncisoCotizacionLista((List<SubIncisoCotizacionDTO>)query.getResultList());
							
						}
					}
				
				}
				
				
			}
			
		}
		
		return cotizacionDTO;
	}

	@SuppressWarnings("unchecked")
	public Double getPrimaNetaCotizacion(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log(
				"getPrimaNetaCotizacion from CotizacionDTO instance",
				Level.INFO, null);
		String queryString = "";
		try {
			Double primaNeta = 0D;
			if (idToCotizacion == null)
				return primaNeta;

			queryString = "SELECT SUM (cob.valorprimaneta) AS valorprimaneta "+
						 " FROM MIDAS.tocotizacion cot, " +
						 "     MIDAS.toincisocot inc, "+
						 "     MIDAS.toseccioncot sec, "+
						 "     MIDAS.tocoberturacot cob "+
						 " WHERE cot.idtocotizacion ="+idToCotizacion+
						 " AND inc.idtocotizacion = cot.idtocotizacion "+
						 " AND sec.idtocotizacion = inc.idtocotizacion "+
						 " AND sec.numeroinciso = inc.numeroinciso "+
						 " AND sec.clavecontrato = 1 " +
						 " AND cob.idtocotizacion = sec.idtocotizacion "+
						 " AND cob.numeroinciso = sec.numeroinciso "+
						 " AND cob.idtoseccion = sec.idtoseccion "+
						 " AND cob.clavecontrato = 1 ";
			Query query = entityManager.createNativeQuery(queryString);
			Object result = query.getSingleResult();
			if (result instanceof List) {
				primaNeta = (Double) ((BigDecimal) ((List) result).get(0))
						.doubleValue();
			} else if (result instanceof BigDecimal) {
				primaNeta = (Double) ((BigDecimal) result).doubleValue();
			}
			return primaNeta;
		} catch (RuntimeException re) {
			re.printStackTrace();
			LogDeMidasEJB3.log("getPrimaNetaCotizacion failed", Level.SEVERE,
					re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CotizacionDTO> listarCotizaciones() {
		LogDeMidasEJB3.log("finding all Cotizacion instances",Level.INFO, null);
		try {
			entityManager.flush();
			final String queryString = "select model from CotizacionDTO model " +
					"where model.claveEstatus in" +SistemaPersistencia.ESTATUS_DE_COTIZACIONES+
					"and model.solicitudDTO.claveTipoSolicitud <> "+SistemaPersistencia.SOLICITUD_ENDOSO+
					" order by model.idToCotizacion";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public List<CotizacionDTO> listarCotizacionesFiltrado(CotizacionDTO cotizacionDTO) {
		try {
			entityManager.flush();
			String queryString = "select model from CotizacionDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
//			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "model.solicitudDTO.claveTipoSolicitud", cotizacionDTO.getIdToCotizacion());
			
			if (cotizacionDTO == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idToCotizacion", cotizacionDTO.getIdToCotizacion());
			try{
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoPolizaDTO.idToTipoPoliza", cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
			}catch (NullPointerException e){}
			try{
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.idToSolicitud", cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
			}catch (NullPointerException e){}
			try{
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "direccionCobroDTO.idToDireccion", cotizacionDTO.getDireccionCobroDTO().getIdToDireccion());
			}catch (NullPointerException e){}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idMoneda", cotizacionDTO.getIdMoneda());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idFormaPago", cotizacionDTO.getIdFormaPago());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicioVigencia", cotizacionDTO.getFechaInicioVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaFinVigencia", cotizacionDTO.getFechaFinVigencia());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCreacion", cotizacionDTO.getCodigoUsuarioCreacion());			
			if(cotizacionDTO.getClaveEstatus()!=null){
			    sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveEstatus", cotizacionDTO.getClaveEstatus());
			 }else{
			     if (Utilerias.esAtributoQueryValido(sWhere)){
			        sWhere = sWhere.concat(" and ");
			     }
			     sWhere +=" model.claveEstatus in";
			     sWhere +=SistemaPersistencia.ESTATUS_DE_COTIZACIONES;
			 }
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaContratante", cotizacionDTO.getNombreEmpresaContratante());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaAsegurado", cotizacionDTO.getNombreEmpresaAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioOrdenTrabajo", cotizacionDTO.getCodigoUsuarioOrdenTrabajo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCotizacion", cotizacionDTO.getCodigoUsuarioCotizacion());
			
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreAsegurado", cotizacionDTO.getNombreAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreContratante", cotizacionDTO.getNombreContratante());

			if(cotizacionDTO.getSolicitudDTO() != null){
				if(cotizacionDTO.getSolicitudDTO().getIdToSolicitud() != null) {
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.idToSolicitud", cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
				}
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.nombrePersona", cotizacionDTO.getSolicitudDTO().getNombrePersona());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.apellidoPaterno", cotizacionDTO.getSolicitudDTO().getApellidoPaterno());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.apellidoMaterno", cotizacionDTO.getSolicitudDTO().getApellidoMaterno());
			}
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				queryString += " and model.solicitudDTO.claveTipoSolicitud <>"+SistemaPersistencia.SOLICITUD_ENDOSO +
				" order by model.idToCotizacion";
			}else{
				queryString += " where model.solicitudDTO.claveTipoSolicitud <>"+SistemaPersistencia.SOLICITUD_ENDOSO +
				" order by model.idToCotizacion";
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if(cotizacionDTO.getPrimerRegistroACargar() != null && cotizacionDTO.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(cotizacionDTO.getPrimerRegistroACargar().intValue());
				query.setMaxResults(cotizacionDTO.getNumeroMaximoRegistrosACargar().intValue());
			}
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public Long obtenerTotalCotizacionesFiltrado(CotizacionDTO cotizacionDTO) {
		try {
			LogDeMidasEJB3.log("Entra a CotizacionFacade.obtenerTotalCotizacionesFiltrado" ,
					Level.INFO, null);
			
			entityManager.flush();
			String queryString = "select count(model) from CotizacionDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
//			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "model.solicitudDTO.claveTipoSolicitud", cotizacionDTO.getIdToCotizacion());
			
			if (cotizacionDTO == null) {
				return null;
			}
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idToCotizacion", cotizacionDTO.getIdToCotizacion());
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoPolizaDTO.idToTipoPoliza", cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
			} catch (NullPointerException e) {
				
			}
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.idToSolicitud", cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
			} catch (NullPointerException e) {
				
			}
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "direccionCobroDTO.idToDireccion", cotizacionDTO.getDireccionCobroDTO().getIdToDireccion());
			} catch (NullPointerException e) {
				
			}
	
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idMoneda", cotizacionDTO.getIdMoneda());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idFormaPago", cotizacionDTO.getIdFormaPago());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicioVigencia", cotizacionDTO.getFechaInicioVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaFinVigencia", cotizacionDTO.getFechaFinVigencia());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCreacion", cotizacionDTO.getCodigoUsuarioCreacion());
			
			if (cotizacionDTO.getClaveEstatus()!=null) {
			    sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveEstatus", cotizacionDTO.getClaveEstatus());
			} else {
			     if (Utilerias.esAtributoQueryValido(sWhere)) {
			        sWhere = sWhere.concat(" and ");
			     }
			     sWhere +=" model.claveEstatus in";
			     sWhere +=SistemaPersistencia.ESTATUS_DE_COTIZACIONES;
			}
			
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaContratante", cotizacionDTO.getNombreEmpresaContratante());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaAsegurado", cotizacionDTO.getNombreEmpresaAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioOrdenTrabajo", cotizacionDTO.getCodigoUsuarioOrdenTrabajo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCotizacion", cotizacionDTO.getCodigoUsuarioCotizacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreAsegurado", cotizacionDTO.getNombreAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreContratante", cotizacionDTO.getNombreContratante());

			if (cotizacionDTO.getSolicitudDTO() != null) {
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.nombrePersona", cotizacionDTO.getSolicitudDTO().getNombrePersona());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.apellidoPaterno", cotizacionDTO.getSolicitudDTO().getApellidoPaterno());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "solicitudDTO.apellidoMaterno", cotizacionDTO.getSolicitudDTO().getApellidoMaterno());
			}
			if (Utilerias.esAtributoQueryValido(sWhere)) {
				queryString = queryString.concat(" where ").concat(sWhere);
				queryString += " and model.solicitudDTO.claveTipoSolicitud <> " + SistemaPersistencia.SOLICITUD_ENDOSO;
			} else {
				queryString += " where model.solicitudDTO.claveTipoSolicitud <> " + SistemaPersistencia.SOLICITUD_ENDOSO;
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			return (Long)query.getSingleResult();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("CotizacionFacade.obtenerTotalCotizacionesFiltrado fallo", Level.SEVERE, re);
			throw re;
		}
	}
	
	private void copiaRelacionesCotizacion(CotizacionDTO cotizacionOrigen, CotizacionDTO cotizacionDestino){
		String queryString = copiarIncisos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();				
		LogDeMidasEJB3.log("se copiaron los incisos", Level.INFO, null);
		
		copiaDireccionesIncisos(cotizacionOrigen, cotizacionDestino);
		LogDeMidasEJB3.log("se copiaron direcciones de incisos", Level.INFO, null);
		
		queryString = copiarSecciones(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();	
		LogDeMidasEJB3.log("se copiaron las secciones", Level.INFO, null);
		
		queryString = copiarCoberturas(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();	
		LogDeMidasEJB3.log("se copiaron las coberturas", Level.INFO, null);
		
		queryString = copiarRiesgos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();			
		LogDeMidasEJB3.log("se copiaron los riesgos", Level.INFO, null);
		
		queryString = copiarAgrupaciones(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();		
		LogDeMidasEJB3.log("se copiaron las agrupaciones", Level.INFO, null);
		
		queryString = copiarsubIncisos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();	
		LogDeMidasEJB3.log("se copiaron los subincisos", Level.INFO, null);
		
		queryString = copiarDetallePrimaCobertura(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los detalles de prima cobertura", Level.INFO, null);
		
		queryString = copiarDetallePrimaRiesgo(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();		
		LogDeMidasEJB3.log("se copiaron los detalles de prima riesgo", Level.INFO, null);
		
		queryString = copiarComisiones(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();		
		LogDeMidasEJB3.log("se copiaron las comisiones", Level.INFO, null);

		queryString = copiarDatosIncisos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();		
		LogDeMidasEJB3.log("se copiaron los datos de inciso", Level.INFO, null);
		
		queryString = copiarAumentos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();			
		LogDeMidasEJB3.log("se copiaron los aumentos", Level.INFO, null);

		queryString = copiarRecargos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los recargos", Level.INFO, null);

		queryString = copiarDescuentos(cotizacionDestino.getIdToCotizacion(),cotizacionOrigen.getIdToCotizacion());
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		LogDeMidasEJB3.log("se copiaron los descuentos", Level.INFO, null);		
	}
	
	@SuppressWarnings("unchecked")
	public List<CotizacionDTO> obtenerCotizacionesFacultadas(Date fechaInicial,Date fechaFinal){
		LogDeMidasEJB3.log("Consultando polizas fecultativas emitidas del "+fechaInicial+" al "+fechaFinal, Level.INFO, null);
		List<CotizacionDTO> listaCotizacionesEncontradas = null;
		try{
			String queryString = "select model from CotizacionDTO model, EndosoDTO endoso where " +
					"model.idToCotizacion = endoso.idToCotizacion and " +
					"model.tipoNegocioDTO.codigoTipoNegocio in (:tipoNegocioPolizaFacultativa,:tipoNegocioPolizaFacCtrlReclamos) and " +
					"endoso.fechaCreacion > :fechaInicial and endoso.fechaCreacion < :fechaFinal order by model.idMoneda";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("tipoNegocioPolizaFacultativa", SistemaPersistencia.CODIGO_TIPO_NEGOCIO_FACULTATIVO);
			query.setParameter("tipoNegocioPolizaFacCtrlReclamos", SistemaPersistencia.CODIGO_TIPO_NEGOCIO_FACULTATIVO_CONTROL_RECLAMOS);

			fechaInicial = Utilerias.removerHorasMinutosSegundos(fechaInicial);
			fechaFinal = Utilerias.removerHorasMinutosSegundos(fechaFinal);
			//Se resta un segundo a la fecha inicial para utilizar el operador > y eliminar errores por horas, minutos y segundos
			Calendar calendarTMP = Calendar.getInstance();
			calendarTMP.setTime(fechaInicial);
			calendarTMP.add(Calendar.SECOND, -1);
			fechaInicial = calendarTMP.getTime();
			//Se suma un d�a a la fecha final para utilizar el operador < y eliminar errores por horas, minutos y segundos
			calendarTMP.setTime(fechaFinal);
			calendarTMP.add(Calendar.DATE, 1);
			fechaFinal= calendarTMP.getTime();
			query.setParameter("fechaInicial", fechaInicial);
			query.setParameter("fechaFinal", fechaFinal);
			listaCotizacionesEncontradas = query.getResultList();
			LogDeMidasEJB3.log("Se encontraron "+listaCotizacionesEncontradas.size()+" poliza(s) facultadas entre del "+fechaInicial+" al "+fechaFinal, Level.INFO, null);
		}
		catch(Exception e){
			LogDeMidasEJB3.log("Ocurri� un error al consultar polizas facultativas emitidas del "+fechaInicial+" al "+fechaFinal, Level.INFO, e);
		}
		return listaCotizacionesEncontradas;
	}
	
	public Double obtenerPorcentajeRecargoPagoFraccionadoCotizacion(CotizacionDTO cotizacionDTO) throws SystemException {
		FormaPagoIDTO formaPago = formaPagoInterfazServicios.getPorId(cotizacionDTO.getIdFormaPago().intValue(),cotizacionDTO.getIdMoneda().shortValue(),
				cotizacionDTO.getCodigoUsuarioModificacion());
		if(formaPago != null)
			return formaPago.getPorcentajeRecargoPagoFraccionado().doubleValue();
		else
			return null;
	}
	
	public CotizacionDTO modificarRecargoPagoFraccionado(BigDecimal idToCotizacion, Short claveRecargoPagoFraccionadoUsuario, Double valorRecargoPagoFraccionado, boolean calculoPoliza, boolean sinAutorizacion) throws SystemException{
		CotizacionDTO cotizacionDTO = findById(idToCotizacion);
		cotizacionDTO.setClaveRecargoPagoFraccionadoUsuario(claveRecargoPagoFraccionadoUsuario);
		
		double valorRPFAnterior = cotizacionDTO.getValorRecargoPagoFraccionadoUsuario() == null ? 0 : cotizacionDTO.getValorRecargoPagoFraccionadoUsuario().doubleValue();
		double porcentajeRPFAnterior = cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario() == null ? 0 : cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario().doubleValue();
		
		if(claveRecargoPagoFraccionadoUsuario.compareTo(ConstantesCotizacion.CLAVE_RPF_SISTEMA) == 0){
			cotizacionDTO.setPorcentajeRecargoPagoFraccionadoUsuario(this.obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacionDTO));
			cotizacionDTO.setValorRecargoPagoFraccionadoUsuario(this.calcularValorRPF(cotizacionDTO, calculoPoliza));
		}else if(claveRecargoPagoFraccionadoUsuario.compareTo(ConstantesCotizacion.CLAVE_RPF_USUARIO_MONTO) == 0){
			cotizacionDTO.setValorRecargoPagoFraccionadoUsuario(valorRecargoPagoFraccionado);
			cotizacionDTO.setPorcentajeRecargoPagoFraccionadoUsuario(this.calcularPorcentajeRPF(cotizacionDTO, calculoPoliza));				
		}else if(claveRecargoPagoFraccionadoUsuario.compareTo(ConstantesCotizacion.CLAVE_RPF_USUARIO_PORCENTAJE) == 0){
			cotizacionDTO.setPorcentajeRecargoPagoFraccionadoUsuario(valorRecargoPagoFraccionado);
			cotizacionDTO.setValorRecargoPagoFraccionadoUsuario(this.calcularValorRPF(cotizacionDTO, calculoPoliza));
		}
									
		if(claveRecargoPagoFraccionadoUsuario.compareTo(ConstantesCotizacion.CLAVE_RPF_SISTEMA) == 0 || sinAutorizacion){
			cotizacionDTO.setClaveAutorizacionRecargoPagoFraccionado(ConstantesCotizacion.AUTORIZACION_NO_REQUERIDA);
			cotizacionDTO.setCodigoUsuarioAutorizacionRecargoPagoFraccionado(null);
			cotizacionDTO.setFechaAutorizacionRecargoPagoFraccionado(null);
		}else{		
			DecimalFormat df10dec = new DecimalFormat("#.##########");
			DecimalFormat df4dec = new DecimalFormat("#.####");
			
			valorRPFAnterior = Double.valueOf(df10dec.format(valorRPFAnterior));
			porcentajeRPFAnterior = Double.valueOf(df4dec.format(porcentajeRPFAnterior));			
			double valorRPFActual = Double.valueOf(df10dec.format(cotizacionDTO.getValorRecargoPagoFraccionadoUsuario().doubleValue()));
			double porcentajeRPFActual = Double.valueOf(df4dec.format(cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario().doubleValue()));
			
			if(valorRPFActual != valorRPFAnterior || porcentajeRPFActual != porcentajeRPFAnterior){					
				cotizacionDTO.setClaveAutorizacionRecargoPagoFraccionado(ConstantesCotizacion.AUTORIZACION_REQUERIDA);
				cotizacionDTO.setCodigoUsuarioAutorizacionRecargoPagoFraccionado(null);
				cotizacionDTO.setFechaAutorizacionRecargoPagoFraccionado(null);
			}				
		}			
		cotizacionDTO = update(cotizacionDTO);					
		return cotizacionDTO;
	}
	
	//Calcula los derechos de una cotizaci�n 
	public Double calcularDerechosCotizacion(CotizacionDTO cotizacionDTO, boolean calculoPoliza){
		BigDecimal primaNetaCotizacion = BigDecimal.ZERO;			
		Double derechos = 0D;
		
		if (!calculoPoliza && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0)
			primaNetaCotizacion = BigDecimal.valueOf(calcularPrimaNetaCotizacionEndoso(cotizacionDTO));
		else
			primaNetaCotizacion = BigDecimal.valueOf(calcularPrimaNetaCotizacion(cotizacionDTO));
		
		GastoExpedicionDTO gasto = gastoExpedicionFacade.getGastoExpedicion(cotizacionDTO.getIdMoneda(), primaNetaCotizacion.abs());	
		if(gasto != null){
			if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0){
				
				EndosoDTO endosoValidacion = endosoFacade.buscarPorCotizacion(cotizacionDTO.getIdToCotizacion());
			
				if (endosoValidacion != null && endosoValidacion.getClaveTipoEndoso().shortValue() == ConstantesCotizacion.TIPO_ENDOSO_CE) {					
					derechos = gasto.getGastoExpedicionEndosoAumento() * -1D;					
				}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == ConstantesCotizacion.SOLICITUD_ENDOSO_DE_REHABILITACION){						
					derechos = gasto.getGastoExpedicionPoliza();		
				}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()==ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CANCELACION){
					//Se regresan los derechos de cada endoso, s�lo si el endoso en cuesti�n no tiene recibos pagados
					//Se toman encuenta todos los endosos de la p�liza, a partir del �ltimo endoso de CFP emitido.
					BigDecimal idToPoliza = cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada();
					List<EndosoDTO> endosos = endosoFacade.findByProperty("id.idToPoliza", idToPoliza,false);
					for(EndosoDTO endoso : endosos){
						try {
							if(!endosoFacade.primerReciboPagado(endoso,cotizacionDTO.getCodigoUsuarioCreacion()))
								derechos += endoso.getValorDerechos() * -1D;
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						if(endoso.getClaveTipoEndoso().compareTo(ConstantesCotizacion.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO) == 0)
							break;
					}						
				}else if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()==ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO) {
					try {
						if(!polizaFacade.existeReciboPagado(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada()))
							derechos = gasto.getGastoExpedicionPoliza();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else if(primaNetaCotizacion.doubleValue() > 0){//Si es endoso de modificaci�n con prima positiva = Endoso de Aumento
					derechos = gasto.getGastoExpedicionEndosoAumento();
				}	
			}else{
				derechos = gasto.getGastoExpedicionPoliza();
			}
		}
		return derechos;
	}
	
	public CotizacionDTO modificarDerechos(BigDecimal idToCotizacion, Short claveDerechosUsuario, Double valorDerechos, boolean calculoPoliza, boolean sinAutorizacion){
		CotizacionDTO cotizacionDTO = findById(idToCotizacion);
					
		cotizacionDTO.setClaveDerechosUsuario(claveDerechosUsuario);						
		
		if(claveDerechosUsuario.compareTo(ConstantesCotizacion.CLAVE_DERECHOS_SISTEMA) == 0){
			cotizacionDTO.setValorDerechosUsuario(calcularDerechosCotizacion(cotizacionDTO,calculoPoliza));
			cotizacionDTO.setClaveAutorizacionDerechos(ConstantesCotizacion.AUTORIZACION_NO_REQUERIDA);
			cotizacionDTO.setCodigoUsuarioAutorizacionDerechos(null);
			cotizacionDTO.setFechaAutorizacionDerechos(null);
		}else{
			
			if(cotizacionDTO.getValorDerechosUsuario() == null)
				cotizacionDTO.setValorDerechosUsuario(0D);
			
			DecimalFormat df10dec = new DecimalFormat("#.##########");				
			
			double valorDerechosAnterior = Double.valueOf(df10dec.format(cotizacionDTO.getValorDerechosUsuario()));							
			double valorDerechosActual = Double.valueOf(df10dec.format(valorDerechos));				
			
			if(valorDerechosAnterior != valorDerechosActual){
				cotizacionDTO.setValorDerechosUsuario(valorDerechos);
				cotizacionDTO.setClaveAutorizacionDerechos(ConstantesCotizacion.AUTORIZACION_REQUERIDA);
				cotizacionDTO.setCodigoUsuarioAutorizacionDerechos(null);
				cotizacionDTO.setFechaAutorizacionDerechos(null);
			}	
		}	
		
		if(sinAutorizacion){
			cotizacionDTO.setClaveAutorizacionDerechos(ConstantesCotizacion.AUTORIZACION_NO_REQUERIDA);
			cotizacionDTO.setCodigoUsuarioAutorizacionDerechos(null);
			cotizacionDTO.setFechaAutorizacionDerechos(null);
		}
		
		return update(cotizacionDTO);
//		cotizacionSN.modificar(cotizacionDTO);			
//		return cotizacionSN.getPorId(idToCotizacion);
	}
	
	//Calcula el valor del recargo por pago fraccionado de una cotizaci�n a partir de un porcentaje espec�fico de RPF y de la prima neta de la cotizaci�n.
	private Double calcularValorRPF(CotizacionDTO cotizacionDTO, boolean calculoPoliza){
		Double primaNetaCotizacion = 0D;
		if (!calculoPoliza && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0)
			primaNetaCotizacion = calcularPrimaNetaCotizacionEndoso(cotizacionDTO);
		else
			primaNetaCotizacion = calcularPrimaNetaCotizacion(cotizacionDTO);
		
		Double porcentajeRPF = cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario();
		return primaNetaCotizacion * porcentajeRPF / 100;
	}
	
	private Double calcularPrimaNetaCotizacionEndoso(CotizacionDTO cotizacionEndosoActual){
		BigDecimal idToPoliza = cotizacionEndosoActual.getSolicitudDTO().getIdToPolizaEndosada();
		
		EndosoDTO endosoAnterior;
		EndosoDTO endosoActual = endosoFacade.buscarPorCotizacion(cotizacionEndosoActual.getIdToCotizacion());
		
		if (endosoActual != null) {
			endosoAnterior = cancelacionEndosoFacade.obtieneEndosoAnterior(endosoActual.getId());
		} else {
			endosoAnterior = endosoFacade.getUltimoEndoso(idToPoliza);
		}
		
		BigDecimal factor = Utilerias.getFactorVigencia(cotizacionEndosoActual.getFechaInicioVigencia(), cotizacionEndosoActual.getFechaFinVigencia());
		
		Double primaNetaCotizacion = this.getPrimaNetaCotizacion(cotizacionEndosoActual.getIdToCotizacion());	
		Double primaNetaCotizacionBase = this.getPrimaNetaCotizacion(endosoAnterior.getIdToCotizacion());
								
		primaNetaCotizacion = (primaNetaCotizacion - primaNetaCotizacionBase) * factor.doubleValue();
					
		double diasPorDevengar = Utilerias.obtenerDiasEntreFechas(cotizacionEndosoActual.getFechaInicioVigencia(), cotizacionEndosoActual.getFechaFinVigencia());			
						
		EndosoDTO endosoEmitido = endosoFacade.buscarPorCotizacion(cotizacionEndosoActual.getIdToCotizacion());
		
		if(cotizacionEndosoActual.getSolicitudDTO().getClaveTipoEndoso().shortValue()==ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CANCELACION){
			primaNetaCotizacion = primaNetaCotizacionBase * (diasPorDevengar / 365D ) * -1D;
		}else if(cotizacionEndosoActual.getSolicitudDTO().getClaveTipoEndoso().shortValue()==ConstantesCotizacion.SOLICITUD_ENDOSO_DE_REHABILITACION){
			primaNetaCotizacion = primaNetaCotizacionBase * (diasPorDevengar / 365D );
		}else if(cotizacionEndosoActual.getSolicitudDTO().getClaveTipoEndoso().shortValue()== ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
			primaNetaCotizacion = this.getPrimaNetaCotizacion(cotizacionEndosoActual.getIdToCotizacion()) * (diasPorDevengar / 365D );	
		} else if (endosoEmitido!=null && (endosoEmitido.getClaveTipoEndoso().shortValue() == ConstantesCotizacion.TIPO_ENDOSO_CE
				|| endosoEmitido.getClaveTipoEndoso().shortValue() == ConstantesCotizacion.TIPO_ENDOSO_RE)
				&& primaNetaCotizacion < 0) {				
			primaNetaCotizacion = primaNetaCotizacion * -1D;
		}
		
		return primaNetaCotizacion;
	}
	
	private Double calcularPrimaNetaCotizacion(CotizacionDTO cotizacionDTO){
		Double primaNetaCotizacion = 0D;
		BigDecimal factor = Utilerias.getFactorVigencia(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
		if(cotizacionDTO.getTipoPolizaDTO().getProductoDTO() != null && 
				cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getClaveAjusteVigencia().intValue() == 0){
			primaNetaCotizacion = this.getPrimaNetaCotizacion(cotizacionDTO.getIdToCotizacion());							
			cotizacionDTO.setDiasPorDevengar(365D);
		}else{
			cotizacionDTO.setDiasPorDevengar(Utilerias.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
			primaNetaCotizacion = this.getPrimaNetaCotizacion(cotizacionDTO.getIdToCotizacion()) * factor.doubleValue();
		}
		
		return primaNetaCotizacion;
	}
	
	private Double calcularPorcentajeRPF(CotizacionDTO cotizacionDTO, boolean calculoPoliza)throws SystemException{
		Double primaNetaCotizacion = 0D;
		if (!calculoPoliza && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0)
			primaNetaCotizacion = calcularPrimaNetaCotizacionEndoso(cotizacionDTO);
		else
			primaNetaCotizacion = calcularPrimaNetaCotizacion(cotizacionDTO);
		
		if(primaNetaCotizacion==0)
			return -1D;
		
		Double valorRPF = cotizacionDTO.getValorRecargoPagoFraccionadoUsuario();
		return valorRPF/primaNetaCotizacion * 100;
	}
	
	public CotizacionDTO recalcularCotizacion(CotizacionDTO cotizacion,String nombreUsuario) {
		
		List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>();
		
		//Por cada una de las coberturas, se revisa si se altero su estatus de contratada/descontratada o su SA, en caso
		//de ser asi, se recalcula
		
		for (IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()) {
			for (SeccionCotizacionDTO seccion : inciso.getSeccionCotizacionList()) {
				for (CoberturaCotizacionDTO cobertura : seccion.getCoberturaCotizacionLista()) {
					//Si es cobertura amparada no se manda calcular.
					if(!cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_AMPARADA)){
						cobertura = coberturaCotizacionFacade.actualizarCoberturaEnCotizacion(cobertura, nombreUsuario);
					}
					//Si es una cobertura contratada se agrega para posteriormente recalcular los totales
					if (cobertura.getClaveContrato().intValue() == 1) {
						coberturas.add(cobertura);
					}
				}
			}
		}
		//Se recalculan los totales de la cotizacion
//		try {
//			cotizacion = getCotizacionFull(cotizacion.getIdToCotizacion()); //  Cotizaci�n del producto Casa
//			cotizacion = resumenCotizacionFacade.calcularTotalesResumenCotizacion(cotizacion, coberturas);
//		} catch (SystemException e) {
//			throw new RuntimeException(e);
//		}
		
		//Se guardan los cambios a la cotizacion
//		cotizacion = update(cotizacion);
		
		//Se regresa la cotizacion recalculada
		return cotizacion;
	}
	
	@SuppressWarnings("unchecked")
	public BigDecimal obtenerIdToTipoPoliza(String codigoProducto,String codigoTipoPoliza){
		try{
			String queryString = "select idtotipopoliza from MIDAS.totipopoliza pol " +
					"inner join MIDAS.toproducto prod on prod.idtoproducto = pol.idtoproducto " +
					"where pol.codigotipopoliza = '"+codigoTipoPoliza+"' and prod.codigoproducto = '"+codigoProducto+"'";
			Query query = entityManager.createNativeQuery(queryString);
			
			List listaResultado = query.getResultList();
			
			BigDecimal resultado = null;
			
			if(listaResultado != null && listaResultado.size() == 1){
				resultado = Utilerias.obtenerBigDecimal(listaResultado.get(0));
			}			
			return resultado;
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("error al consultar idToTipoPoliza. codigoProducto: "+codigoProducto+", codigoTipoPoliza:"+codigoTipoPoliza, Level.SEVERE, e);
			throw e;
		}
	}
	
	public PlenoReaseguroDTO obtenerPlenoReaseguro(SubGiroDTO subGiroDTO) {
		BigDecimal idGrupoPlenos = subGiroDTO.getIdGrupoPlenos();
		PlenoReaseguroDTO plenoReaseguro = null;
		plenoReaseguro = plenoReaseguroFacade.findById(idGrupoPlenos);
		return plenoReaseguro;
	}
	
	/**
	 Perform an initial save of a previously unsaved CotizacionDTO entity and return its id value. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
   @Interceptors({mx.com.afirme.midas.sistema.log.LogInterceptor.class})
   public BigDecimal saveAndGetId(CotizacionDTO entity) {
   				LogDeMidasEJB3.log("saving CotizacionDTO instance", Level.INFO, null);
	        try {
           entityManager.persist(entity);
           			LogDeMidasEJB3.log("save successful", Level.INFO, null);
           			return (BigDecimal)entityManager.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity);
	        } catch (RuntimeException re) {
       				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
       }
   }
   /**
    * Busca incisos por descripcion 
    * para luego aplicar un filtro a sus cotizaciones
    * @param IncisoCotizacionDTO filtroInciso contiene la descripcion del vehiculo
    * @param CotizacionDTO filtroCot, contiene los filtros de las cotizaciones
    * @author martin
    */
   public List<CotizacionDTO> cotizacionPorDescripcionVehiculo(IncisoCotizacionDTO filtroInicso,List<CotizacionDTO> cotizacionesFiltradas){
		// Obtiene los incisos filtrados por la descripcion
		List<IncisoCotizacionDTO> incisoCotizacionDTOs = new LinkedList<IncisoCotizacionDTO>();
		incisoCotizacionDTOs = incisoCotizacionFacadeRemote.listarIncisosConFiltro(filtroInicso);
		// 
		List<CotizacionDTO> result = new ArrayList<CotizacionDTO>();
		// Si hay un solo resultado no hace las iteraciones
		if(cotizacionesFiltradas.size() == 1){
			result.add(cotizacionesFiltradas.get(0));
			return result;
		}
		if(incisoCotizacionDTOs != null && cotizacionesFiltradas != null && !cotizacionesFiltradas.isEmpty()){
			for(IncisoCotizacionDTO inciso : incisoCotizacionDTOs){
				for(CotizacionDTO cotizacion : cotizacionesFiltradas){
					if(cotizacion.getIdToCotizacion().toString().equals(inciso.getId().getIdToCotizacion().toString())){
						result.add(cotizacion);
					}
				}
			}
		}
		return result;
	}

   public CotizacionDTO filtraCotizacionesComparator(List<CotizacionDTO> cotizacionesFiltradas,Predicate predicate){
   		return (CotizacionDTO) CollectionUtils.find(cotizacionesFiltradas, predicate);
   	}

}