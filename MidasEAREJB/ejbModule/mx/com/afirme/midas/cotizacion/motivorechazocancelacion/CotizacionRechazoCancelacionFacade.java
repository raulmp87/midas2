package mx.com.afirme.midas.cotizacion.motivorechazocancelacion;
// default package

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity CotizacionRechazoCancelacion.
 * @see .CotizacionRechazoCancelacion
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class CotizacionRechazoCancelacionFacade  implements CotizacionRechazoCancelacionFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved CotizacionRechazoCancelacion entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CotizacionRechazoCancelacion entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CotizacionRechazoCancelacionDTO entity) {
    				LogDeMidasEJB3.log("saving CotizacionRechazoCancelacionDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CotizacionRechazoCancelacionDTO entity.
	  @param entity CotizacionRechazoCancelacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CotizacionRechazoCancelacionDTO entity) {
    				LogDeMidasEJB3.log("deleting CotizacionRechazoCancelacionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(CotizacionRechazoCancelacionDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CotizacionRechazoCancelacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the CotizacionRechazoCancelacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CotizacionRechazoCancelacionDTO entity to update
	 @return CotizacionRechazoCancelacionDTO the persisted CotizacionRechazoCancelacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CotizacionRechazoCancelacionDTO update(CotizacionRechazoCancelacionDTO entity) {
    				LogDeMidasEJB3.log("updating CotizacionRechazoCancelacionDTO instance", Level.INFO, null);
	        try {
            CotizacionRechazoCancelacionDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public CotizacionRechazoCancelacionDTO findById( CotizacionRechazoCancelacionId id) {
    				LogDeMidasEJB3.log("finding CotizacionRechazoCancelacion instance with id: " + id, Level.INFO, null);
	        try {
            CotizacionRechazoCancelacionDTO instance = entityManager.find(CotizacionRechazoCancelacionDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all CotizacionRechazoCancelacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the CotizacionRechazoCancelacionDTO property to query
	  @param value the property value to match
	  	  @return List<CotizacionRechazoCancelacionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<CotizacionRechazoCancelacionDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding CotizacionRechazoCancelacionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from CotizacionRechazoCancelacionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all CotizacionRechazoCancelacionDTO entities.
	  	  @return List<CotizacionRechazoCancelacionDTO> all CotizacionRechazoCancelacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CotizacionRechazoCancelacionDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all CotizacionRechazoCancelacionDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from CotizacionRechazoCancelacionDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}