package mx.com.afirme.midas.cotizacion.subinciso;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity SubIncisoCotizacionDTO.
 * @see .SubIncisoCotizacionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class SubIncisoCotizacionFacade  implements SubIncisoCotizacionFacadeRemote {
	//property constants
	public static final String CLAVEAUTREASEGURO = "claveautreaseguro";
	public static final String CODIGOUSUARIOAUTREASEGURO = "codigousuarioautreaseguro";

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved SubIncisoCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SubIncisoCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public SubIncisoCotizacionDTO save(SubIncisoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("saving SubIncisoCotizacionDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
            return entity;
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent SubIncisoCotizacionDTO entity.
	  @param entity SubIncisoCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
	public void delete(SubIncisoCotizacionDTO entity) {
		LogDeMidasEJB3.log("deleting SubIncisoCotizacionDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SubIncisoCotizacionDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
			
			String queryString = "delete from MIDAS.todatoincisocot " +
					"where idtocotizacion = " + entity.getId().getIdToCotizacion() +
					"and numeroinciso = " + entity.getId().getNumeroInciso() +
					"and numerosubinciso = " + entity.getId().getNumeroSubInciso() +
					"and idtoseccion = " + entity.getId().getIdToSeccion();
			Query query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
			LogDeMidasEJB3.log("delete DatoInciso successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}
    
    /**
	 Persist a previously saved SubIncisoCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the SubIncisoCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SubIncisoCotizacionDTO entity to update
	 @return SubIncisoCotizacionDTO the persisted SubIncisoCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public SubIncisoCotizacionDTO update(SubIncisoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("updating SubIncisoCotizacionDTO instance", Level.INFO, null);
	        try {
            SubIncisoCotizacionDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public SubIncisoCotizacionDTO findById( SubIncisoCotizacionDTOId id) {
    				LogDeMidasEJB3.log("finding SubIncisoCotizacionDTO instance with id: " + id, Level.INFO, null);
	        try {
            SubIncisoCotizacionDTO instance = entityManager.find(SubIncisoCotizacionDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all SubIncisoCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SubIncisoCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<SubIncisoCotizacionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<SubIncisoCotizacionDTO> findByProperty(String propertyName, final Object value
        ) {
    		LogDeMidasEJB3.log("finding SubIncisoCotizacionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from SubIncisoCotizacionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	public List<SubIncisoCotizacionDTO> findByClaveAutReaseguro(Object claveAutReaseguro
	) {
		return findByProperty(CLAVEAUTREASEGURO, claveAutReaseguro
		);
	}
	
	public List<SubIncisoCotizacionDTO> findByCodigoUsuarioAutReaseguro(Object codigoUsuarioAutReaseguro
	) {
		return findByProperty(CODIGOUSUARIOAUTREASEGURO, codigoUsuarioAutReaseguro
		);
	}
	
	
	/**
	 * Find all SubIncisoCotizacionDTO entities.
	  	  @return List<SubIncisoCotizacionDTO> all SubIncisoCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoCotizacionDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all SubIncisoCotizacionDTO instances", Level.INFO, null);
			try {
				final String queryString = "select model from SubIncisoCotizacionDTO model";
				Query query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);	
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
    public List<SubIncisoCotizacionDTO> listarFiltrado(SubIncisoCotizacionDTO subIncisoCotizacionDTO) {
    		LogDeMidasEJB3.log("Listar Filtrado SubIncisoCotizacionDTO", Level.INFO, null);
    		try {
    			String queryString = "select model from SubIncisoCotizacionDTO model ";
    			String sWhere = "";
    			Query query;
    			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
    			
    			if (subIncisoCotizacionDTO == null)
    				return null;
    			else
    				if (subIncisoCotizacionDTO.getId() == null)
    						subIncisoCotizacionDTO.setId(new SubIncisoCotizacionDTOId()); // Avoid Null Pointer Exception
    						
    			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToCotizacion", subIncisoCotizacionDTO.getId().getIdToCotizacion(), "idToCotizacion");
    			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.numeroInciso", subIncisoCotizacionDTO.getId().getNumeroInciso(), "numeroInciso");
    			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToSeccion", subIncisoCotizacionDTO.getId().getIdToSeccion(), "idToSeccion");
    			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveEstatusDeclaracion", subIncisoCotizacionDTO.getClaveEstatusDeclaracion(), "claveEstatusDeclaracion");
    			
    			if (Utilerias.esAtributoQueryValido(sWhere)){
    				queryString = queryString.concat(" where ").concat(sWhere);
    				queryString += " order by model.id.idToCotizacion, model.id.numeroInciso, model.id.idToSeccion, model.id.numeroSubInciso  ";
    			}else{
    				queryString += " order by model.id.idToCotizacion, model.id.numeroInciso, model.id.idToSeccion, model.id.numeroSubInciso  ";
    			}    			
    			query = entityManager.createQuery(queryString);
    			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
    			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
    			List<SubIncisoCotizacionDTO> lista = query.getResultList();
    			for(SubIncisoCotizacionDTO inciso : lista)
    				entityManager.refresh(inciso);
    			return lista;
    		} catch (RuntimeException re) {
    			throw re;
    		}
	}
	
	@SuppressWarnings("unchecked")
    public List<SubIncisoCotizacionDTO> listarFiltrado(SubIncisoCotizacionDTO subIncisoCotizacionDTO,boolean aplicarMerge) {
    		LogDeMidasEJB3.log("Listar Filtrado SubIncisoCotizacionDTO", Level.INFO, null);
    		try {
    			String queryString = "select model from SubIncisoCotizacionDTO model ";
    			String sWhere = "";
    			Query query;
    			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
    			
    			if (subIncisoCotizacionDTO == null)
    				return null;
    			else
    				if (subIncisoCotizacionDTO.getId() == null)
    						subIncisoCotizacionDTO.setId(new SubIncisoCotizacionDTOId()); // Avoid Null Pointer Exception
    						
    			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToCotizacion", subIncisoCotizacionDTO.getId().getIdToCotizacion(), "idToCotizacion");
    			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.numeroInciso", subIncisoCotizacionDTO.getId().getNumeroInciso(), "numeroInciso");
    			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToSeccion", subIncisoCotizacionDTO.getId().getIdToSeccion(), "idToSeccion");
    			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveEstatusDeclaracion", subIncisoCotizacionDTO.getClaveEstatusDeclaracion(), "claveEstatusDeclaracion");
    			
    			if (Utilerias.esAtributoQueryValido(sWhere)){
    				queryString = queryString.concat(" where ").concat(sWhere);
    				queryString += " order by model.id.idToCotizacion, model.id.numeroInciso, model.id.idToSeccion, model.id.numeroSubInciso  ";
    			}else{
    				queryString += " order by model.id.idToCotizacion, model.id.numeroInciso, model.id.idToSeccion, model.id.numeroSubInciso  ";
    			}    			
    			query = entityManager.createQuery(queryString);
    			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
    			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
    			List<SubIncisoCotizacionDTO> subIncisos = query.getResultList();
    			if(aplicarMerge && subIncisos != null && !subIncisos.isEmpty()){
    				for(SubIncisoCotizacionDTO subInciso : subIncisos){
    					entityManager.refresh(subInciso);
    				}
    			}
    			return subIncisos;
    		} catch (RuntimeException re) {
    			throw re;
    		}
	}

	public BigDecimal maxSubIncisos(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idToSeccion) {
		LogDeMidasEJB3.log("max SubIncisoCotizacionDTO", Level.INFO, null);
		try {
			String queryString = "select max(model.id.numeroSubInciso) from SubIncisoCotizacionDTO model " +
					"where model.id.idToCotizacion = :idToCotizacion and model.id.numeroInciso = :numeroInciso " +
					"and model.id.idToSeccion = :idToSeccion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			query.setParameter("idToSeccion", idToSeccion);

			BigDecimal numSubInciso = (BigDecimal) query.getSingleResult();
			if (numSubInciso == null)
				numSubInciso = BigDecimal.ZERO;
			return numSubInciso;
		} catch (RuntimeException re) {
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public BigDecimal primaNetaSubInciso(SubIncisoCotizacionDTOId id) {
		LogDeMidasEJB3.log("sumatoria de primaneta ToDetPrimaCoberturaCot",
				Level.INFO, null);
		try {
			String queryString = "select sum(VALORPRIMANETA) primaneta from midas.ToDetPrimaCoberturaCot where";
			queryString += " IDTOCOTIZACION =" + id.getIdToCotizacion();
			queryString += " AND NUMEROINCISO =" + id.getNumeroInciso();
			queryString += " AND IDTOSECCION =" + id.getIdToSeccion();
			queryString += " AND NUMEROSUBINCISO =" + id.getNumeroSubInciso();
			Query query = entityManager.createNativeQuery(queryString);

			BigDecimal resultado = null;
			if(query.getSingleResult() instanceof List)  {
				resultado = BigDecimal.valueOf(Double.valueOf(((List)query.getSingleResult()).get(0).toString()));
			} else if (query.getSingleResult() instanceof BigDecimal) {
				resultado = (BigDecimal) query.getSingleResult();
			}
			if (resultado == null)
				resultado = BigDecimal.ZERO;
			return resultado;
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public void borrarSubincisosSeccionCotizacion(SeccionCotizacionDTOId id) {
		LogDeMidasEJB3.log("borrarSubincisosSeccionCotizacion", Level.INFO, null);
		try {
			String queryString = "delete from SubIncisoCotizacionDTO model " +
					"where model.id.idToCotizacion = :idToCotizacion " +
					"and model.id.numeroInciso = :numeroInciso " +
					"and model.id.idToSeccion = :idToSeccion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", id.getIdToCotizacion());
			query.setParameter("numeroInciso", id.getNumeroInciso());
			query.setParameter("idToSeccion", id.getIdToSeccion());
			query.executeUpdate();

			queryString = "delete from DatoIncisoCotizacionDTO model " +
					"where model.id.idToCotizacion = :idToCotizacion " +
					"and model.id.numeroInciso = :numeroInciso " +
					"and model.id.idToSeccion = :idToSeccion";
			query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", id.getIdToCotizacion());
			query.setParameter("numeroInciso", id.getNumeroInciso());
			query.setParameter("idToSeccion", id.getIdToSeccion());
			query.executeUpdate();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public List<SubIncisoCotizacionDTO> listarSubIncisosPorCotizacion(BigDecimal idToCotizacion) {
		SubIncisoCotizacionDTO subIncisoCotizacionDTO = new SubIncisoCotizacionDTO();
		subIncisoCotizacionDTO.setId(new SubIncisoCotizacionDTOId());
		subIncisoCotizacionDTO.getId().setIdToCotizacion(idToCotizacion);
		return listarFiltrado(subIncisoCotizacionDTO);
	}
}