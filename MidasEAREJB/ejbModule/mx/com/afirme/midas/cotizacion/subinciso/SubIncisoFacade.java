package mx.com.afirme.midas.cotizacion.subinciso;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity SubIncisoDTO.
 * @see .SubIncisoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class SubIncisoFacade  implements SubIncisoFacadeRemote {
	//property constants


    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved SubIncisoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SubIncisoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public SubIncisoDTO save(SubIncisoDTO entity) {
    				LogDeMidasEJB3.log("saving SubIncisoDTO instance", Level.INFO, null);
	        try {
	        	entityManager.persist(entity);
            	LogDeMidasEJB3.log("save successful", Level.INFO, null);
            	return entity;
	        } catch (RuntimeException re) {
        		LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent SubIncisoDTO entity.
	  @param entity SubIncisoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SubIncisoDTO entity) {
    				LogDeMidasEJB3.log("deleting SubIncisoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(SubIncisoDTO.class, entity.getIdToSubInciso());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved SubIncisoDTO entity and return it or a copy of it to the sender. 
	 A copy of the SubIncisoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SubIncisoDTO entity to update
	 @return SubIncisoDTO the persisted SubIncisoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public SubIncisoDTO update(SubIncisoDTO entity) {
    				LogDeMidasEJB3.log("updating SubIncisoDTO instance", Level.INFO, null);
	        try {
            SubIncisoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public SubIncisoDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding SubIncisoDTO instance with id: " + id, Level.INFO, null);
	        try {
            SubIncisoDTO instance = entityManager.find(SubIncisoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all SubIncisoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SubIncisoDTO property to query
	  @param value the property value to match
	  	  @return List<SubIncisoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<SubIncisoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding SubIncisoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from SubIncisoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find all SubIncisoDTO entities.
	  	  @return List<SubIncisoDTO> all SubIncisoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SubIncisoDTO> findAll() {
					LogDeMidasEJB3.log("finding all SubIncisoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from SubIncisoDTO model";
				Query query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
		} catch (RuntimeException re) {
				LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}