package mx.com.afirme.midas.cotizacion.comision;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ComisionCotizacionDTO.
 * 
 * @see .ComisionCotizacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ComisionCotizacionFacade implements ComisionCotizacionFacadeRemote {
	// property constants
	public static final String PORCENTAJE_COMISION_DEFAULT = "porcentajeComisionDefault";
	public static final String PORCENTAJE_COMISION_COTIZACION = "porcentajeComisionCotizacion";
	public static final String VALOR_COMISION_COTIZACION = "valorComisionCotizacion";
	public static final String CLAVE_AUT_COMISION = "claveAutComision";
	public static final String CODIGO_USUARIO_AUT_COMISION = "codigoUsuarioAutComision";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved ComisionCotizacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            ComisionCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ComisionCotizacionDTO entity) {
		LogDeMidasEJB3.log("saving ComisionCotizacionDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ComisionCotizacionDTO entity.
	 * 
	 * @param entity
	 *            ComisionCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ComisionCotizacionDTO entity) {
		LogDeMidasEJB3.log("deleting ComisionCotizacionDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(ComisionCotizacionDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ComisionCotizacionDTO entity and return it or
	 * a copy of it to the sender. A copy of the ComisionCotizacionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            ComisionCotizacionDTO entity to update
	 * @return ComisionCotizacionDTO the persisted ComisionCotizacionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ComisionCotizacionDTO update(ComisionCotizacionDTO entity) {
		LogDeMidasEJB3.log("updating ComisionCotizacionDTO instance",
				Level.INFO, null);
		try {
			ComisionCotizacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ComisionCotizacionDTO findById(ComisionCotizacionId id) {
		LogDeMidasEJB3.log("finding ComisionCotizacionDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			ComisionCotizacionDTO instance = entityManager.find(
					ComisionCotizacionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ComisionCotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ComisionCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ComisionCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ComisionCotizacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding ComisionCotizacionDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ComisionCotizacionDTO model where model."
					+ propertyName + "= :propertyValue order by model.subRamoDTO.idTcSubRamo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<ComisionCotizacionDTO> findByPorcentajeComisionDefault(
			Object porcentajeComisionDefault) {
		return findByProperty(PORCENTAJE_COMISION_DEFAULT,
				porcentajeComisionDefault);
	}

	public List<ComisionCotizacionDTO> findByPorcentajeComisionCotizacion(
			Object porcentajeComisionCotizacion) {
		return findByProperty(PORCENTAJE_COMISION_COTIZACION,
				porcentajeComisionCotizacion);
	}

	public List<ComisionCotizacionDTO> findByValorComisionCotizacion(
			Object valorComisionCotizacion) {
		return findByProperty(VALOR_COMISION_COTIZACION,
				valorComisionCotizacion);
	}

	public List<ComisionCotizacionDTO> findByClaveAutComision(
			Object claveAutComision) {
		return findByProperty(CLAVE_AUT_COMISION, claveAutComision);
	}

	public List<ComisionCotizacionDTO> findByCodigoUsuarioAutComision(
			Object codigoUsuarioAutComision) {
		return findByProperty(CODIGO_USUARIO_AUT_COMISION,
				codigoUsuarioAutComision);
	}

	/**
	 * Find all ComisionCotizacionDTO entities.
	 * 
	 * @return List<ComisionCotizacionDTO> all ComisionCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ComisionCotizacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all ComisionCotizacionDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from ComisionCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Encuentra los registros de ComisionCotizacionDTO correspondientes al idToCotizacion recibido
	 * y que ademas tienen clave de Autorizacion recibida.
	 * @param idToCotizacion
	 * @param claveAutorizacion
	 * @return List<ComisionCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ComisionCotizacionDTO> encontrarPorClaveAutirizacionPorCotizacion(BigDecimal idToCotizacion,Short claveAutorizacion) {
		LogDeMidasEJB3.log("encontrando instancias ComisionCotizacionDTO pendientes de la cotizacion "+ idToCotizacion, Level.INFO, null);
		try {
			//Se utiliza un 1 para definir que la comision est� pendiente
			final String queryString = "select model from ComisionCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion and model.claveAutComision = :claveAutorizacion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("claveAutorizacion", claveAutorizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Encuentra los registros de ComisionCotizacionDTO correspondientes al idToCotizacion recibido
	 * y que ademas pueden ser autorizados.
	 * @param idToCotizacion
	 * @param claveAutorizacion
	 * @return List<ComisionCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ComisionCotizacionDTO> buscarComisionesPorAutorizar(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("encontrando instancias ComisionCotizacionDTO pendientes de la cotizacion "+ idToCotizacion, Level.INFO, null);
		try {
			//Se utiliza un 1 para definir que la comision requiere autorizaci�n y un 8 para definir que fu� rechazada
			final String queryString = "select model from ComisionCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion " +
					"and model.claveAutComision = :estatuspendiente or model.claveAutComision = :estatusrechazada order by model.subRamoDTO.idTcSubRamo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("estatuspendiente", new Short("1"));
			query.setParameter("estatusrechazada", new Short("8"));
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
}