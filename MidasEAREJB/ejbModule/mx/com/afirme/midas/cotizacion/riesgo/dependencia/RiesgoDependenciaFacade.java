package mx.com.afirme.midas.cotizacion.riesgo.dependencia;

// default package

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity RiesgoDependenciaDTO.
 * 
 * @see .RiesgoDependenciaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class RiesgoDependenciaFacade implements RiesgoDependenciaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved RiesgoDependenciaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            RiesgoDependenciaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RiesgoDependenciaDTO entity) {
		LogUtil.log("saving RiesgoDependenciaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent RiesgoDependenciaDTO entity.
	 * 
	 * @param entity
	 *            RiesgoDependenciaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoDependenciaDTO entity) {
		LogUtil.log("deleting RiesgoDependenciaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(RiesgoDependenciaDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved RiesgoDependenciaDTO entity and return it or a
	 * copy of it to the sender. A copy of the RiesgoDependenciaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            RiesgoDependenciaDTO entity to update
	 * @return RiesgoDependenciaDTO the persisted RiesgoDependenciaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoDependenciaDTO update(RiesgoDependenciaDTO entity) {
		LogUtil.log("updating RiesgoDependenciaDTO instance", Level.INFO, null);
		try {
			RiesgoDependenciaDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RiesgoDependenciaDTO findById(RiesgoDependenciaId id) {
		LogUtil.log("finding RiesgoDependenciaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			RiesgoDependenciaDTO instance = entityManager.find(
					RiesgoDependenciaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoDependenciaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoDependenciaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoDependenciaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoDependenciaDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding RiesgoDependenciaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoDependenciaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoDependenciaDTO entities.
	 * 
	 * @return List<RiesgoDependenciaDTO> all RiesgoDependenciaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoDependenciaDTO> findAll() {
		LogUtil.log("finding all RiesgoDependenciaDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from RiesgoDependenciaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}