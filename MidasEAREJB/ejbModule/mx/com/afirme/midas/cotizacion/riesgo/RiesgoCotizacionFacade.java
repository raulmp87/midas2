package mx.com.afirme.midas.cotizacion.riesgo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.producto.configuracion.aumento.AumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.descuento.DescuentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.RecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.AumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.DescuentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.RecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity RiesgoCotizacionDTO.
 * 
 * @see .RiesgoCotizacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class RiesgoCotizacionFacade implements RiesgoCotizacionFacadeRemote {
	// property constants
	public static final String VALOR_PRIMA_NETA = "valorPrimaNeta";
	public static final String VALOR_SUMA_ASEGURADA = "valorSumaAsegurada";
	public static final String VALOR_COASEGURO = "valorCoaseguro";
	public static final String VALOR_DEDUCIBLE = "valorDeducible";
	public static final String VALOR_DESCUENTO = "valorDescuento";
	public static final String VALOR_RECARGO = "valorRecargo";
	public static final String VALOR_AUMENTO = "valorAumento";
	public static final String PORCENTAJE_COASEGURO = "porcentajeCoaseguro";
	public static final String CLAVE_AUT_COASEGURO = "claveAutCoaseguro";
	public static final String CODIGO_USUARIO_AUT_COASEGURO = "codigoUsuarioAutCoaseguro";
	public static final String PORCENTAJE_DEDUCIBLE = "porcentajeDeducible";
	public static final String CLAVE_AUT_DEDUCIBLE = "claveAutDeducible";
	public static final String CODIGO_USUARIO_AUT_DEDUCIBLE = "codigoUsuarioAutDeducible";
	public static final String CLAVE_OBLIGATORIEDAD = "claveObligatoriedad";
	public static final String CLAVE_CONTRATO = "claveContrato";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved RiesgoCotizacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            RiesgoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RiesgoCotizacionDTO entity) {
		LogDeMidasEJB3.log("saving RiesgoCotizacionDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent RiesgoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            RiesgoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoCotizacionDTO entity) {
		LogDeMidasEJB3.log("deleting RiesgoCotizacionDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(RiesgoCotizacionDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved RiesgoCotizacionDTO entity and return it or a
	 * copy of it to the sender. A copy of the RiesgoCotizacionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            RiesgoCotizacionDTO entity to update
	 * @return RiesgoCotizacionDTO the persisted RiesgoCotizacionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoCotizacionDTO update(RiesgoCotizacionDTO entity) {
		LogDeMidasEJB3.log("updating RiesgoCotizacionDTO instance", Level.INFO,
				null);
		try {
			RiesgoCotizacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			entityManager.refresh(result);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RiesgoCotizacionDTO findById(RiesgoCotizacionId id) {
		LogDeMidasEJB3.log("finding RiesgoCotizacionDTO instance with id: "+ id, Level.INFO, null);
		try {
			entityManager.flush();
			RiesgoCotizacionDTO instance = entityManager.find(
					RiesgoCotizacionDTO.class, id);
			try{
				if(instance != null) {
					entityManager.refresh(instance);
				}
			}catch (IllegalArgumentException e){
				e.printStackTrace();
				throw new RuntimeException();
			}
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoCotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoCotizacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding RiesgoCotizacionDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			entityManager.flush();
			final String queryString = "select model from RiesgoCotizacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			List<RiesgoCotizacionDTO> entities = query.getResultList();
			return entities;
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<RiesgoCotizacionDTO> findByValorPrimaNeta(Object valorPrimaNeta) {
		return findByProperty(VALOR_PRIMA_NETA, valorPrimaNeta);
	}

	public List<RiesgoCotizacionDTO> findByValorSumaAsegurada(
			Object valorSumaAsegurada) {
		return findByProperty(VALOR_SUMA_ASEGURADA, valorSumaAsegurada);
	}

	public List<RiesgoCotizacionDTO> findByValorCoaseguro(Object valorCoaseguro) {
		return findByProperty(VALOR_COASEGURO, valorCoaseguro);
	}

	public List<RiesgoCotizacionDTO> findByValorDeducible(Object valorDeducible) {
		return findByProperty(VALOR_DEDUCIBLE, valorDeducible);
	}

	public List<RiesgoCotizacionDTO> findByValorDescuento(Object valorDescuento) {
		return findByProperty(VALOR_DESCUENTO, valorDescuento);
	}

	public List<RiesgoCotizacionDTO> findByValorRecargo(Object valorRecargo) {
		return findByProperty(VALOR_RECARGO, valorRecargo);
	}

	public List<RiesgoCotizacionDTO> findByValorAumento(Object valorAumento) {
		return findByProperty(VALOR_AUMENTO, valorAumento);
	}

	public List<RiesgoCotizacionDTO> findByPorcentajeCoaseguro(
			Object porcentajeCoaseguro) {
		return findByProperty(PORCENTAJE_COASEGURO, porcentajeCoaseguro);
	}

	public List<RiesgoCotizacionDTO> findByClaveAutCoaseguro(
			Object claveAutCoaseguro) {
		return findByProperty(CLAVE_AUT_COASEGURO, claveAutCoaseguro);
	}

	public List<RiesgoCotizacionDTO> findByCodigoUsuarioAutCoaseguro(
			Object codigoUsuarioAutCoaseguro) {
		return findByProperty(CODIGO_USUARIO_AUT_COASEGURO,
				codigoUsuarioAutCoaseguro);
	}

	public List<RiesgoCotizacionDTO> findByPorcentajeDeducible(
			Object porcentajeDeducible) {
		return findByProperty(PORCENTAJE_DEDUCIBLE, porcentajeDeducible);
	}

	public List<RiesgoCotizacionDTO> findByClaveAutDeducible(
			Object claveAutDeducible) {
		return findByProperty(CLAVE_AUT_DEDUCIBLE, claveAutDeducible);
	}

	public List<RiesgoCotizacionDTO> findByCodigoUsuarioAutDeducible(
			Object codigoUsuarioAutDeducible) {
		return findByProperty(CODIGO_USUARIO_AUT_DEDUCIBLE,
				codigoUsuarioAutDeducible);
	}

	public List<RiesgoCotizacionDTO> findByClaveObligatoriedad(
			Object claveObligatoriedad) {
		return findByProperty(CLAVE_OBLIGATORIEDAD, claveObligatoriedad);
	}

	public List<RiesgoCotizacionDTO> findByClaveContrato(Object claveContrato) {
		return findByProperty(CLAVE_CONTRATO, claveContrato);
	}

	/**
	 * Find all RiesgoCotizacionDTO entities.
	 * 
	 * @return List<RiesgoCotizacionDTO> all RiesgoCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoCotizacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all RiesgoCotizacionDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DescuentoVarioCoberturaDTO> getDescuentosCobertura(BigDecimal idToCobertura, BigDecimal idToRiesgo) {
		LogDeMidasEJB3.log("finding DescuentoVarioCoberturaDTO instances with idToRiesgo: "+ idToRiesgo, Level.INFO, null);
		try {
			String queryString = "select model from DescuentoVarioCoberturaDTO model "
					+ "where model.id.idtocobertura = :idToCobertura and model.id.idtodescuentovario not in ("
					+ "select exc.id.idtodescuentovario from ExclusionDescuentoVarioCoberturaDTO exc "
					+ "where exc.id.idtocobertura = :idToCobertura and exc.id.idtoriesgo = :idToRiesgo)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCobertura", idToCobertura);
			query.setParameter("idToRiesgo", idToRiesgo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<AumentoVarioCoberturaDTO> getAumentosCobertura(
			BigDecimal idToCobertura, BigDecimal idToRiesgo) {
		LogDeMidasEJB3.log(
				"finding AumentoVarioCoberturaDTO instances with idToRiesgo: "
						+ idToRiesgo, Level.INFO, null);
		try {
			String queryString = "select model from AumentoVarioCoberturaDTO model "
					+ "where model.id.idtocobertura = :idToCobertura and model.id.idtoaumentovario not in ("
					+ "select exc.id.idtoaumentovario from ExclusionAumentoVarioCoberturaDTO exc "
					+ "where exc.id.idtocobertura = :idToCobertura and exc.id.idtoriesgo = :idToRiesgo)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCobertura", idToCobertura);
			query.setParameter("idToRiesgo", idToRiesgo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RecargoVarioCoberturaDTO> getRecargosCobertura(
			BigDecimal idToCobertura, BigDecimal idToRiesgo) {
		LogDeMidasEJB3.log(
				"finding RecargoVarioCoberturaDTO instances with idToRiesgo: "
						+ idToRiesgo, Level.INFO, null);
		try {
			String queryString = "select model from RecargoVarioCoberturaDTO model "
					+ "where model.id.idtocobertura = :idToCobertura and model.id.idtorecargovario not in ("
					+ "select exc.id.idtorecargovario from ExclusionRecargoVarioCoberturaDTO exc "
					+ "where exc.id.idtocobertura = :idToCobertura and exc.id.idtoriesgo = :idToRiesgo)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCobertura", idToCobertura);
			query.setParameter("idToRiesgo", idToRiesgo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DescuentoVarioTipoPolizaDTO> getDescuentosTipoPoliza(
			BigDecimal idToTipoPoliza, BigDecimal idToCobertura) {
		LogDeMidasEJB3.log(
				"finding DescuentoVarioTipoPolizaDTO instances with idToCobertura: "
						+ idToCobertura, Level.INFO, null);
		try {
			String queryString = "select model from DescuentoVarioTipoPolizaDTO model "
					+ "where model.id.idtotipopoliza = :idToTipoPoliza and model.id.idtodescuentovario not in ("
					+ "select exc.id.idtodescuentovario from ExclusionDescuentoVarioTipoPolizaDTO exc "
					+ "where exc.id.idtotipopoliza = :idToTipoPoliza and exc.id.idtocobertura = :idToCobertura)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setParameter("idToCobertura", idToCobertura);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<AumentoVarioTipoPolizaDTO> getAumentosTipoPoliza(
			BigDecimal idToTipoPoliza, BigDecimal idToCobertura) {
		LogDeMidasEJB3.log(
				"finding AumentoVarioTipoPolizaDTO instances with idToCobertura: "
						+ idToCobertura, Level.INFO, null);
		try {
			String queryString = "select model from AumentoVarioTipoPolizaDTO model "
					+ "where model.id.idtotipopoliza = :idToTipoPoliza and model.id.idtoaumentovario not in ("
					+ "select exc.id.idtoaumentovario from ExclusionAumentoVarioTipoPolizaDTO exc "
					+ "where exc.id.idtotipopoliza = :idToTipoPoliza and exc.id.idtocobertura = :idToCobertura)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setParameter("idToCobertura", idToCobertura);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RecargoVarioTipoPolizaDTO> getRecargosTipoPoliza(
			BigDecimal idToTipoPoliza, BigDecimal idToCobertura) {
		LogDeMidasEJB3.log(
				"finding RecargoVarioTipoPolizaDTO instances with idToCobertura: "
						+ idToCobertura, Level.INFO, null);
		try {
			String queryString = "select model from RecargoVarioTipoPolizaDTO model "
					+ "where model.id.idtotipopoliza = :idToTipoPoliza and model.id.idtorecargovario not in ("
					+ "select exc.id.idtorecargovario from ExclusionRecargoVarioTipoPolizaDTO exc "
					+ "where exc.id.idtotipopoliza = :idToTipoPoliza and exc.id.idtocobertura = :idToCobertura)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setParameter("idToCobertura", idToCobertura);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DescuentoVarioProductoDTO> getDescuentosProducto(
			BigDecimal idToProducto, BigDecimal idToTipoPoliza) {
		LogDeMidasEJB3.log(
				"finding DescuentoVarioProductoDTO instances with idToTipoPoliza: "
						+ idToTipoPoliza, Level.INFO, null);
		try {
			String queryString = "select model from DescuentoVarioProductoDTO model "
					+ "where model.id.idToProducto = :idToProducto and model.id.idToDescuentoVario not in ("
					+ "select exc.id.idtodescuentovario from ExclusionDescuentoVarioProductoDTO exc "
					+ "where exc.id.idtoproducto = :idToProducto and exc.id.idtotipopoliza = :idToTipoPoliza)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToProducto", idToProducto);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<AumentoVarioProductoDTO> getAumentosProducto(
			BigDecimal idToProducto, BigDecimal idToTipoPoliza) {
		LogDeMidasEJB3.log(
				"finding AumentoVarioProductoDTO instances with idToTipoPoliza: "
						+ idToTipoPoliza, Level.INFO, null);
		try {
			String queryString = "select model from AumentoVarioProductoDTO model "
					+ "where model.id.idtoproducto = :idToProducto and model.id.idtoaumentovario not in ("
					+ "select exc.id.idtoaumentovario from ExclusionAumentoVarioProductoDTO exc "
					+ "where exc.id.idtoproducto = :idToProducto and exc.id.idtotipopoliza = :idToTipoPoliza)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToProducto", idToProducto);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RecargoVarioProductoDTO> getRecargosProducto(
			BigDecimal idToProducto, BigDecimal idToTipoPoliza) {
		LogDeMidasEJB3.log(
				"finding RecargoVarioProductoDTO instances with idToTipoPoliza: "
						+ idToTipoPoliza, Level.INFO, null);
		try {
			String queryString = "select model from RecargoVarioProductoDTO model "
					+ "where model.id.idToProducto = :idToProducto and model.id.idToRecargoVario not in ("
					+ "select exc.id.idtorecargovario from ExclusionRecargoVarioProductoDTO exc "
					+ "where exc.id.idtoproducto = :idToProducto and exc.id.idtotipopoliza = :idToTipoPoliza)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToProducto", idToProducto);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RiesgoCotizacionDTO> listarRiesgoContratados(
			CoberturaCotizacionId id) {
		LogDeMidasEJB3.log("finding RiesgoCotizacionDTO instances", Level.INFO,
				null);
		try {
			String queryString = "select model from RiesgoCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion and model.id.numeroInciso = :numeroInciso " +
							"and model.id.idToSeccion = :idToSeccion "
					+ "and model.id.idToCobertura = :idToCobertura and model.claveContrato = 1 "
					+ "and model.coberturaCotizacionDTO.claveContrato = 1"
					+ "and model.coberturaCotizacionDTO.seccionCotizacionDTO.claveContrato = 1 " +
							"order by model.riesgoCoberturaDTO.riesgoDTO.codigo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", id.getIdToCotizacion());
			query.setParameter("numeroInciso", id.getNumeroInciso());
			query.setParameter("idToSeccion", id.getIdToSeccion());
			query.setParameter("idToCobertura", id.getIdToCobertura());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find RiesgoCotizacionDTO failed", Level.SEVERE,
					re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<RiesgoCotizacionDTO> listarRiesgoContratadosPorCotizacion(
			BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("finding RiesgoCotizacionDTO instances", Level.INFO,
				null);
		try {
			String queryString = "select model from RiesgoCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion "
					+ "and model.claveContrato = 1 "
					+ "and model.coberturaCotizacionDTO.claveContrato = 1"
					+ "and model.coberturaCotizacionDTO.seccionCotizacionDTO.claveContrato = 1 ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find RiesgoCotizacionDTO failed", Level.SEVERE,
					re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public List<RiesgoCotizacionDTO> listarRiesgoContratadosPorCobertura(
			CoberturaCotizacionId id) {
		LogDeMidasEJB3.log("finding RiesgoCotizacionDTO instances", Level.INFO,
				null);
		try {
			String queryString = "select model from RiesgoCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion "
					+ "and model.id.numeroInciso=:numeroInciso and model.id.idToCobertura = :idToCobertura and model.claveContrato = 1 order by model.id.idToRiesgo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", id.getIdToCotizacion());
			query.setParameter("idToSeccion", id.getIdToSeccion());
			query.setParameter("idToCobertura", id.getIdToCobertura());
			query.setParameter("numeroInciso", id.getNumeroInciso());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find RiesgoCotizacionDTO failed", Level.SEVERE,
					re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RiesgoCotizacionDTO> listarFiltrado(RiesgoCotizacionDTO entity) {
		LogDeMidasEJB3.log("filtering RiesgoCotizacionDTO instance",Level.INFO, null);
		try {
			String queryString = "select model from RiesgoCotizacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCotizacion", entity.getId().getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"id.numeroInciso", entity.getId().getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToSeccion", entity.getId().getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCobertura", entity.getId().getIdToCobertura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToRiesgo", entity.getId().getIdToRiesgo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveContrato", entity.getClaveContrato());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			queryString += " order by model.riesgoCoberturaDTO.riesgoDTO.codigo";
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			List<RiesgoCotizacionDTO> entities = query.getResultList();
			return entities;			
		} catch (RuntimeException re) {
			throw re;
		}
	}

	/**
	 * lista las entidades de RiesgoCotizacionDTO que tengan lo atributos
	 * recibidos en el objeto RiesgoCotizacionId. Los atributos utilizados como
	 * filtro son: idToCotizacion, idToCobertura, idToSeccion, idToRiesgo y
	 * numeroInciso.
	 * 
	 * @param id
	 * @return List<RiesgoCotizacionDTO>
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoCotizacionDTO> listarPorIdFiltrado(RiesgoCotizacionId id) {
		LogDeMidasEJB3.log(
				"finding RiesgoCotizacionDTO instances with idToCotizacion: "
						+ id.getIdToCotizacion() + ", IdToCobertura: "
						+ id.getIdToCobertura() + ", IdToRiesgo: "
						+ id.getIdToRiesgo() + ", IdToSeccion:"
						+ id.getIdToSeccion() + ", numeroInciso:"
						+ id.getNumeroInciso(), Level.INFO, null);
		try {
			String queryString = "select model from RiesgoCotizacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (id == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCobertura", id.getIdToCobertura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCotizacion", id.getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToRiesgo", id.getIdToRiesgo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToSeccion", id.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.numeroInciso", id.getNumeroInciso());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			List<RiesgoCotizacionDTO> lista = query.getResultList();
			return lista;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"listarPorIdFiltrado RiesgoCotizacionDTO failed",
					Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RiesgoCotizacionDTO> listarRiesgoContratadosPorSeccion(SeccionCotizacionDTOId id) {
		LogDeMidasEJB3.log("entrando a listar riesgos contratados por Seccion ", Level.INFO,
				null);
		try {
		    	if (id == null){
			    return new ArrayList<RiesgoCotizacionDTO>();
			}
			String queryString = "select model from RiesgoCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion and model.id.numeroInciso = :numeroInciso "
					+ "and model.id.idToSeccion = :idToSeccion and model.id.idToCobertura in ("
					+ "select cobertura.id.idToCobertura from CoberturaCotizacionDTO cobertura "
					+ "where cobertura.id.idToCotizacion = :idToCotizacion and cobertura.id.numeroInciso = :numeroInciso "
					+ "and cobertura.id.idToSeccion = :idToSeccion and cobertura.claveContrato = 1)"
					+ "and model.claveContrato = 1 order by model.id.idToRiesgo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", id.getIdToCotizacion());
			query.setParameter("numeroInciso", id.getNumeroInciso());
			query.setParameter("idToSeccion", id.getIdToSeccion());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find RiesgoCotizacionDTO failed", Level.SEVERE,
					re);
			throw re;
		}
	}

	public void insertarARD(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idToProducto, BigDecimal idToTipoPoliza) {
		String queryString = "INSERT INTO MIDAS.torecargoriesgocot recargoriesgo " +
				"(recargoriesgo.idtocotizacion, recargoriesgo.numeroinciso, " +
				"recargoriesgo.idtoseccion, recargoriesgo.idtocobertura, " +
				"recargoriesgo.idtoriesgo, recargoriesgo.idtorecargovario, " +
				"recargoriesgo.claveautorizacion,recargoriesgo.codigousuarioautorizacion, " +
				"recargoriesgo.valorrecargo, recargoriesgo.claveobligatoriedad, " +
				"recargoriesgo.clavecontrato, recargoriesgo.clavecomercialtecnico, " +
				"recargoriesgo.clavenivel, recargoriesgo.fechasolicitudautorizacion, recargoriesgo.fechaautorizacion) " +
				"(SELECT riesgo.idtocotizacion, riesgo.numeroinciso, riesgo.idtoseccion, riesgo.idtocobertura, " +
				"riesgo.idtoriesgo, recargo.idtorecargovario, 0, NULL, 0, 0, 1, 2, 3, SYSDATE, SYSDATE " +
				"FROM MIDAS.toriesgocot riesgo, MIDAS.torecargovario recargo, MIDAS.tocobertura cobertura " +
				"WHERE (riesgo.idtocotizacion = " + idToCotizacion + " " +
				"AND riesgo.numeroinciso = " + numeroInciso + " AND recargo.idtorecargovario < 0 " +
				"AND riesgo.idtocobertura = cobertura.idtocobertura) " +
				"UNION " +
				"SELECT riesgo.idtocotizacion, riesgo.numeroinciso, riesgo.idtoseccion, riesgo.idtocobertura, " +
				"riesgo.idtoriesgo, recargocobertura.idtorecargovario, 0, NULL, " +
				"recargocobertura.valor, recargocobertura.claveobligatoriedad, " +
				"DECODE (recargocobertura.claveobligatoriedad, 1, 1, 3, 1, 0), " +
				"recargocobertura.clavecomercialtecnico, 3, SYSDATE, SYSDATE " +
				"FROM MIDAS.toriesgocot riesgo, MIDAS.torecargovariocobertura recargocobertura " +
				"WHERE (riesgo.idtocotizacion = " + idToCotizacion + " AND riesgo.numeroinciso = " + numeroInciso + " " +
				"AND recargocobertura.idtorecargovario > 0 " +
				"AND recargocobertura.idtocobertura = riesgo.idtocobertura " +
				"AND recargocobertura.idtorecargovario " +
				"NOT IN (SELECT exc.idtorecargovario FROM MIDAS.trexcrecargovarcobertura exc " +
				"WHERE exc.idtocobertura = riesgo.idtocobertura " +
				"AND exc.idtoriesgo = riesgo.idtoriesgo)) " +
				"UNION " +
				"SELECT riesgo.idtocotizacion, riesgo.numeroinciso, riesgo.idtoseccion, riesgo.idtocobertura, " +
				"riesgo.idtoriesgo, recargotipopoliza.idtorecargovario, 0, NULL, " +
				"recargotipopoliza.valor, recargotipopoliza.claveobligatoriedad, " +
				"DECODE (recargotipopoliza.claveobligatoriedad, 1, 1, 3, 1, 0), recargotipopoliza.clavecomercialtecnico, 2, SYSDATE, SYSDATE " +
				"FROM MIDAS.toriesgocot riesgo, MIDAS.torecargovariotipopoliza recargotipopoliza " +
				"WHERE (riesgo.idtocotizacion = " + idToCotizacion + " AND riesgo.numeroinciso = " + numeroInciso + " " +
				"AND recargotipopoliza.idtorecargovario > 0 " +
				"AND recargotipopoliza.idtotipopoliza = " + idToTipoPoliza + " AND recargotipopoliza.idtorecargovario " +
				"NOT IN (SELECT exc.idtorecargovario " +
				"FROM MIDAS.trexcrecargovartipopoliza exc " +
				"WHERE exc.idtotipopoliza = " + idToTipoPoliza + " AND exc.idtocobertura = riesgo.idtocobertura)) " +
				"UNION " +
				"SELECT riesgo.idtocotizacion, riesgo.numeroinciso, riesgo.idtoseccion, riesgo.idtocobertura, " +
				"riesgo.idtoriesgo, recargoproducto.idtorecargovario, 0, NULL, " +
				"recargoproducto.valor, recargoproducto.claveobligatoriedad, " +
				"DECODE (recargoproducto.claveobligatoriedad, 1, 1, 3, 1, 0), recargoproducto.clavecomercialtecnico, 1, SYSDATE, SYSDATE " +
				"FROM MIDAS.toriesgocot riesgo, MIDAS.torecargovarioproducto recargoproducto " +
				"WHERE (riesgo.idtocotizacion = " + idToCotizacion + " AND riesgo.numeroinciso = " + numeroInciso + " " +
				"AND recargoproducto.idtorecargovario > 0 " +
				"AND recargoproducto.idtoproducto = " + idToProducto + " AND recargoproducto.idtorecargovario " +
				"NOT IN (SELECT exc.idtorecargovario FROM MIDAS.trexcrecargovarproducto exc " +
				"WHERE exc.idtoproducto = " + idToProducto + " AND exc.idtotipopoliza = " + idToTipoPoliza + ")))";
		Query query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();

		queryString = "INSERT INTO MIDAS.todescuentoriesgocot descuentoriesgo " +
				"(descuentoriesgo.idtocotizacion, descuentoriesgo.numeroinciso, " +
				"descuentoriesgo.idtoseccion, descuentoriesgo.idtocobertura, " +
				"descuentoriesgo.idtoriesgo, descuentoriesgo.idtodescuentovario, " +
				"descuentoriesgo.claveautorizacion,descuentoriesgo.codigousuarioautorizacion, " +
				"descuentoriesgo.valordescuento, descuentoriesgo.claveobligatoriedad, " +
				"descuentoriesgo.clavecontrato, descuentoriesgo.clavecomercialtecnico, " +
				"descuentoriesgo.clavenivel, descuentoriesgo.fechasolicitudautorizacion, descuentoriesgo.fechaautorizacion) " +
				"(SELECT riesgo.idtocotizacion, riesgo.numeroinciso, riesgo.idtoseccion, riesgo.idtocobertura, " +
				"riesgo.idtoriesgo, descuento.idtodescuentovario, 0, NULL, 0, 0, 1, 2, 3, SYSDATE, SYSDATE " +
				"FROM MIDAS.toriesgocot riesgo, MIDAS.todescuentovario descuento, MIDAS.tocobertura cobertura " +
				"WHERE (riesgo.idtocotizacion = " + idToCotizacion + " " +
				"AND riesgo.numeroinciso = " + numeroInciso + " AND descuento.idtodescuentovario < 0 " +
				"AND riesgo.idtocobertura = cobertura.idtocobertura) " +
				"UNION " +
				"SELECT riesgo.idtocotizacion, riesgo.numeroinciso, riesgo.idtoseccion, riesgo.idtocobertura, " +
				"riesgo.idtoriesgo, descuentocobertura.idtodescuentovario, 0, NULL, " +
				"descuentocobertura.valor, descuentocobertura.claveobligatoriedad, " +
				"DECODE (descuentocobertura.claveobligatoriedad, 1, 1, 3, 1, 0), " +
				"descuentocobertura.clavecomercialtecnico, 3, SYSDATE, SYSDATE " +
				"FROM MIDAS.toriesgocot riesgo, MIDAS.todescuentovariocobertura descuentocobertura " +
				"WHERE (riesgo.idtocotizacion = " + idToCotizacion + " AND riesgo.numeroinciso = " + numeroInciso + " " +
				"AND descuentocobertura.idtodescuentovario > 0 " +
				"AND descuentocobertura.idtocobertura = riesgo.idtocobertura " +
				"AND descuentocobertura.idtodescuentovario " +
				"NOT IN (SELECT exc.idtodescuentovario FROM MIDAS.trexcdescuentovarcobertura exc " +
				"WHERE exc.idtocobertura = riesgo.idtocobertura " +
				"AND exc.idtoriesgo = riesgo.idtoriesgo)) " +
				"UNION " +
				"SELECT riesgo.idtocotizacion, riesgo.numeroinciso, riesgo.idtoseccion, riesgo.idtocobertura, " +
				"riesgo.idtoriesgo, descuentotipopoliza.idtodescuentovario, 0, NULL, " +
				"descuentotipopoliza.valor, descuentotipopoliza.claveobligatoriedad, " +
				"DECODE (descuentotipopoliza.claveobligatoriedad, 1, 1, 3, 1, 0), descuentotipopoliza.clavecomercialtecnico, 2, SYSDATE, SYSDATE " +
				"FROM MIDAS.toriesgocot riesgo, MIDAS.todescuentovariotipopoliza descuentotipopoliza " +
				"WHERE (riesgo.idtocotizacion = " + idToCotizacion + " AND riesgo.numeroinciso = " + numeroInciso + " " +
				"AND descuentotipopoliza.idtodescuentovario > 0 " +
				"AND descuentotipopoliza.idtotipopoliza = " + idToTipoPoliza + " AND descuentotipopoliza.idtodescuentovario " +
				"NOT IN (SELECT exc.idtodescuentovario " +
				"FROM MIDAS.trexcdescuentovartipopoliza exc " +
				"WHERE exc.idtotipopoliza = " + idToTipoPoliza + " AND exc.idtocobertura = riesgo.idtocobertura)) " +
				"UNION " +
				"SELECT riesgo.idtocotizacion, riesgo.numeroinciso, riesgo.idtoseccion, riesgo.idtocobertura, " +
				"riesgo.idtoriesgo, descuentoproducto.idtodescuentovario, 0, NULL, " +
				"descuentoproducto.valor, descuentoproducto.claveobligatoriedad, " +
				"DECODE (descuentoproducto.claveobligatoriedad, 1, 1, 3, 1, 0), descuentoproducto.clavecomercialtecnico, 1, SYSDATE, SYSDATE " +
				"FROM MIDAS.toriesgocot riesgo, MIDAS.todescuentovarioproducto descuentoproducto " +
				"WHERE (riesgo.idtocotizacion = " + idToCotizacion + " AND riesgo.numeroinciso = " + numeroInciso + " " +
				"AND descuentoproducto.idtodescuentovario > 0 " +
				"AND descuentoproducto.idtoproducto = " + idToProducto + " AND descuentoproducto.idtodescuentovario " +
				"NOT IN (SELECT exc.idtodescuentovario FROM MIDAS.trexcdescuentovarproducto exc " +
				"WHERE exc.idtoproducto = " + idToProducto + " AND exc.idtotipopoliza = " + idToTipoPoliza + ")))";
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();

		queryString = "INSERT INTO MIDAS.toaumentoriesgocot aumentoriesgo " +
				"(aumentoriesgo.idtocotizacion, aumentoriesgo.numeroinciso, " +
				"aumentoriesgo.idtoseccion, aumentoriesgo.idtocobertura, " +
				"aumentoriesgo.idtoriesgo, aumentoriesgo.idtoaumentovario, " +
				"aumentoriesgo.claveautorizacion,aumentoriesgo.codigousuarioautorizacion, " +
				"aumentoriesgo.valoraumento, aumentoriesgo.claveobligatoriedad, " +
				"aumentoriesgo.clavecontrato, aumentoriesgo.clavecomercialtecnico, " +
				"aumentoriesgo.clavenivel, aumentoriesgo.fechasolicitudautorizacion, aumentoriesgo.fechaautorizacion) " +
				"(SELECT riesgo.idtocotizacion, riesgo.numeroinciso, riesgo.idtoseccion, riesgo.idtocobertura, " +
				"riesgo.idtoriesgo, aumentocobertura.idtoaumentovario, 0, NULL, " +
				"aumentocobertura.valor, aumentocobertura.claveobligatoriedad, " +
				"DECODE (aumentocobertura.claveobligatoriedad, 1, 1, 3, 1, 0), " +
				"aumentocobertura.clavecomercialtecnico, 3, SYSDATE, SYSDATE " +
				"FROM MIDAS.toriesgocot riesgo, MIDAS.toaumentovariocobertura aumentocobertura " +
				"WHERE (riesgo.idtocotizacion = " + idToCotizacion + " AND riesgo.numeroinciso = " + numeroInciso + " " +
				"AND aumentocobertura.idtoaumentovario > 0 " +
				"AND aumentocobertura.idtocobertura = riesgo.idtocobertura " +
				"AND aumentocobertura.idtoaumentovario " +
				"NOT IN (SELECT exc.idtoaumentovario FROM MIDAS.trexcaumentovarcobertura exc " +
				"WHERE exc.idtocobertura = riesgo.idtocobertura " +
				"AND exc.idtoriesgo = riesgo.idtoriesgo)) " +
				"UNION " +
				"SELECT riesgo.idtocotizacion, riesgo.numeroinciso, riesgo.idtoseccion, riesgo.idtocobertura, " +
				"riesgo.idtoriesgo, aumentotipopoliza.idtoaumentovario, 0, NULL, " +
				"aumentotipopoliza.valor, aumentotipopoliza.claveobligatoriedad, " +
				"DECODE (aumentotipopoliza.claveobligatoriedad, 1, 1, 3, 1, 0), aumentotipopoliza.clavecomercialtecnico, 2, SYSDATE, SYSDATE " +
				"FROM MIDAS.toriesgocot riesgo, MIDAS.toaumentovariotipopoliza aumentotipopoliza " +
				"WHERE (riesgo.idtocotizacion = " + idToCotizacion + " AND riesgo.numeroinciso = " + numeroInciso + " " +
				"AND aumentotipopoliza.idtoaumentovario > 0 " +
				"AND aumentotipopoliza.idtotipopoliza = " + idToTipoPoliza + " AND aumentotipopoliza.idtoaumentovario " +
				"NOT IN (SELECT exc.idtoaumentovario " +
				"FROM MIDAS.trexcaumentovartipopoliza exc " +
				"WHERE exc.idtotipopoliza = " + idToTipoPoliza + " AND exc.idtocobertura = riesgo.idtocobertura)) " +
				"UNION " +
				"SELECT riesgo.idtocotizacion, riesgo.numeroinciso, riesgo.idtoseccion, riesgo.idtocobertura, " +
				"riesgo.idtoriesgo, aumentoproducto.idtoaumentovario, 0, NULL, " +
				"aumentoproducto.valor, aumentoproducto.claveobligatoriedad, " +
				"DECODE (aumentoproducto.claveobligatoriedad, 1, 1, 3, 1, 0), aumentoproducto.clavecomercialtecnico, 1, SYSDATE, SYSDATE " +
				"FROM MIDAS.toriesgocot riesgo, MIDAS.toaumentovarioproducto aumentoproducto " +
				"WHERE (riesgo.idtocotizacion = " + idToCotizacion + " AND riesgo.numeroinciso = " + numeroInciso + " " +
				"AND aumentoproducto.idtoaumentovario > 0 " +
				"AND aumentoproducto.idtoproducto = " + idToProducto + " AND aumentoproducto.idtoaumentovario " +
				"NOT IN (SELECT exc.idtoaumentovario FROM MIDAS.trexcaumentovarproducto exc " +
				"WHERE exc.idtoproducto = " + idToProducto + " AND exc.idtotipopoliza = " + idToTipoPoliza + ")))";
		query = entityManager.createNativeQuery(queryString);
		query.executeUpdate();
		entityManager.flush();
	}

	@SuppressWarnings("unchecked")
	public List<BigDecimal> obtenerRiesgosContratadosCotizacion(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("finding idToRiesgo with idToCotizacion: " + idToCotizacion, Level.INFO, null);
		try {
			String queryString = "select distinct model.id.idToRiesgo from RiesgoCotizacionDTO model " +
					"where model.id.idToCotizacion = :idToCotizacion and model.id.idToCobertura in (" +
					"select cobertura.id.idToCobertura from CoberturaCotizacionDTO cobertura " +
					"where cobertura.id.idToCotizacion = :idToCotizacion and cobertura.claveContrato = 1 and cobertura.seccionCotizacionDTO.claveContrato = 1) " +
					"and model.claveContrato = 1 or (model.claveObligatoriedad = 1 or model.claveObligatoriedad = 3)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Long obtenerTotalRiesgoContratadosPorCobertura(
			CoberturaCotizacionId id) {
		LogDeMidasEJB3.log("Entro a RiesgoCotizacionFacade.obtenerTotalRiesgoContratadosPorCobertura", Level.INFO,
				null);
		try {
			String queryString = "select count(model) from RiesgoCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion "
					+ "and model.id.numeroInciso=:numeroInciso and model.id.idToCobertura = :idToCobertura and model.claveContrato = 1";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", id.getIdToCotizacion());
			query.setParameter("idToSeccion", id.getIdToSeccion());
			query.setParameter("idToCobertura", id.getIdToCobertura());
			query.setParameter("numeroInciso", id.getNumeroInciso());
			return (Long) query.getSingleResult();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("RiesgoCotizacionFacade.obtenerTotalRiesgoContratadosPorCobertura fallo", Level.SEVERE,
					re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RiesgoCotizacionDTO> listarRiesgosConCoasegurosDeduciblesPendientes(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("filtering RiesgoCotizacionDTO instance", Level.INFO, null);
		try {
			String queryString = "select model from RiesgoCotizacionDTO model " +
					"where (model.claveAutCoaseguro = 1 or model.claveAutDeducible = 1) " +
					"and model.id.idToCotizacion = :idToCotizacion " +
					"and model.coberturaCotizacionDTO.seccionCotizacionDTO.claveContrato = 1 " +
					"and model.coberturaCotizacionDTO.claveContrato = 1 " +
					"and model.claveContrato = 1";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	/**
	 * Method listarRiesgosContratadosPorCotizacion()
	 * @param idToCotizacion
	 * @return List<RiesgoCotizacionDTO>
	 * @throws SystemException
	 */
	public List<RiesgoCotizacionDTO> listarRiesgosContratadosPorCotizacion(BigDecimal idToCotizacion){
		RiesgoCotizacionDTO riesgoCot = new RiesgoCotizacionDTO();
		riesgoCot.setId(new RiesgoCotizacionId());
		riesgoCot.getId().setIdToCotizacion(idToCotizacion);
		riesgoCot.setClaveContrato((short)1);
		return this.listarFiltrado(riesgoCot);
	}
}