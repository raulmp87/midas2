package mx.com.afirme.midas.cotizacion.riesgo.aumento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity AumentoRiesgoCotizacionDTO.
 * 
 * @see .AumentoRiesgoCotizacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class AumentoRiesgoCotizacionFacade implements
		AumentoRiesgoCotizacionFacadeRemote {
	// property constants
	public static final String CLAVE_AUTORIZACION = "claveAutorizacion";
	public static final String CODIGO_USUARIO_AUTORIZACION = "codigoUsuarioAutorizacion";
	public static final String VALOR_AUMENTO = "valorAumento";
	public static final String CLAVE_OBLIGATORIEDAD = "claveObligatoriedad";
	public static final String CLAVE_CONTRATO = "claveContrato";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * AumentoRiesgoCotizacionDTO entity. All subsequent persist actions of this
	 * entity should use the #update() method.
	 * 
	 * @param entity
	 *            AumentoRiesgoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(AumentoRiesgoCotizacionDTO entity) {
		LogDeMidasEJB3.log("saving AumentoRiesgoCotizacionDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent AumentoRiesgoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            AumentoRiesgoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(AumentoRiesgoCotizacionDTO entity) {
		LogDeMidasEJB3.log("deleting AumentoRiesgoCotizacionDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					AumentoRiesgoCotizacionDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved AumentoRiesgoCotizacionDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * AumentoRiesgoCotizacionDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            AumentoRiesgoCotizacionDTO entity to update
	 * @return AumentoRiesgoCotizacionDTO the persisted
	 *         AumentoRiesgoCotizacionDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public AumentoRiesgoCotizacionDTO update(AumentoRiesgoCotizacionDTO entity) {
		LogDeMidasEJB3.log("updating AumentoRiesgoCotizacionDTO instance",
				Level.INFO, null);
		try {
			AumentoRiesgoCotizacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public AumentoRiesgoCotizacionDTO findById(AumentoRiesgoCotizacionId id) {
		LogDeMidasEJB3.log(
				"finding AumentoRiesgoCotizacionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			AumentoRiesgoCotizacionDTO instance = entityManager.find(
					AumentoRiesgoCotizacionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all AumentoRiesgoCotizacionDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the AumentoRiesgoCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<AumentoRiesgoCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<AumentoRiesgoCotizacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding AumentoRiesgoCotizacionDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from AumentoRiesgoCotizacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<AumentoRiesgoCotizacionDTO> findByClaveAutorizacion(
			Object claveAutorizacion) {
		return findByProperty(CLAVE_AUTORIZACION, claveAutorizacion);
	}

	public List<AumentoRiesgoCotizacionDTO> findByCodigoUsuarioAutorizacion(
			Object codigoUsuarioAutorizacion) {
		return findByProperty(CODIGO_USUARIO_AUTORIZACION,
				codigoUsuarioAutorizacion);
	}

	public List<AumentoRiesgoCotizacionDTO> findByValorAumento(
			Object valorAumento) {
		return findByProperty(VALOR_AUMENTO, valorAumento);
	}

	public List<AumentoRiesgoCotizacionDTO> findByClaveObligatoriedad(
			Object claveObligatoriedad) {
		return findByProperty(CLAVE_OBLIGATORIEDAD, claveObligatoriedad);
	}

	public List<AumentoRiesgoCotizacionDTO> findByClaveContrato(
			Object claveContrato) {
		return findByProperty(CLAVE_CONTRATO, claveContrato);
	}

	/**
	 * Find all AumentoRiesgoCotizacionDTO entities.
	 * 
	 * @return List<AumentoRiesgoCotizacionDTO> all AumentoRiesgoCotizacionDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<AumentoRiesgoCotizacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all AumentoRiesgoCotizacionDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from AumentoRiesgoCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * 	findByRiesgoCotizacion. Encuentra la lista de entidades AumentoRiesgoCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto riesgoCotId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, idToCobertura, idToRiesgo.
	 * @param RiesgoCotizacionId  
	 * @return List<AumentoRiesgoCotizacionDTO>
	 */
	@SuppressWarnings("unchecked")
	public List<AumentoRiesgoCotizacionDTO> findByRiesgoCotizacion(RiesgoCotizacionId riesgoCotId) {
		LogDeMidasEJB3.log("Buscando AumentoRiesgoCotizacion de la cotizacion: "+riesgoCotId.getIdToCotizacion()+" Inciso: "+
				riesgoCotId.getNumeroInciso()+" Seccion: "+riesgoCotId.getIdToSeccion()+" Cobertura: "+riesgoCotId.getIdToCobertura()+
				" Riesgo: "+riesgoCotId.getIdToRiesgo(),Level.INFO, null);
		try {
			String queryString = "select model from AumentoRiesgoCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion " +
					" and model.id.numeroInciso = :numeroInciso and model.id.idToSeccion = :idToSeccion and model.id.idToCobertura = :idToCobertura ";
			if(riesgoCotId.getIdToRiesgo() != null)
				queryString = queryString + " and model.id.idToRiesgo = :idToRiesgo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", riesgoCotId.getIdToCotizacion());
			query.setParameter("numeroInciso", riesgoCotId.getNumeroInciso());
			query.setParameter("idToSeccion", riesgoCotId.getIdToSeccion());
			query.setParameter("idToCobertura", riesgoCotId.getIdToCobertura());
			if(riesgoCotId.getIdToRiesgo() != null)
				query.setParameter("idToRiesgo", riesgoCotId.getIdToRiesgo());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	} 

	@SuppressWarnings("unchecked")
	public List<AumentoRiesgoCotizacionDTO> findByCoberturaCotizacion(RiesgoCotizacionId riesgoCotId) {
		LogDeMidasEJB3.log("Buscando AumentoRiesgoCotizacion de la cotizacion: "+riesgoCotId.getIdToRiesgo()+" Inciso: "+
				riesgoCotId.getNumeroInciso()+" Seccion: "+riesgoCotId.getIdToSeccion()+" Cobertura: "+riesgoCotId.getIdToCobertura(),Level.INFO, null);
		try {
			final String queryString = "select model from AumentoRiesgoCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion " +
					" and model.id.numeroInciso = :numeroInciso and model.id.idToSeccion = :idToSeccion and model.id.idToCobertura = :idToCobertura ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", riesgoCotId.getIdToCotizacion());
			query.setParameter("numeroInciso", riesgoCotId.getNumeroInciso());
			query.setParameter("idToSeccion", riesgoCotId.getIdToSeccion());
			query.setParameter("idToCobertura", riesgoCotId.getIdToCobertura());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<AumentoRiesgoCotizacionDTO> listarAumentosPendientesAutorizacion(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("filtering AumentoRiesgoCotizacionDTO instance", Level.INFO, null);
		try {
			String queryString = "select model from AumentoRiesgoCotizacionDTO model " +
					"where model.claveAutorizacion = 1 " +
					"and model.id.idToCotizacion = :idToCotizacion " +
					"and model.riesgoCotizacionDTO.coberturaCotizacionDTO.seccionCotizacionDTO.claveContrato = 1 " +
					"and model.riesgoCotizacionDTO.coberturaCotizacionDTO.claveContrato = 1 " +
					"and model.riesgoCotizacionDTO.claveContrato = 1 " +
					"and model.claveContrato = 1";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
}