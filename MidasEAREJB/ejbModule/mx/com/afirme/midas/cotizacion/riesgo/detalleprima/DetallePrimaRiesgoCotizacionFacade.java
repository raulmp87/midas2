package mx.com.afirme.midas.cotizacion.riesgo.detalleprima;
// default package

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTOId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity DetPrimaRiesgoCotizacionDTO.
 * @see .DetPrimaRiesgoCotizacionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DetallePrimaRiesgoCotizacionFacade  implements DetallePrimaRiesgoCotizacionFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DetPrimaRiesgoCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DetPrimaRiesgoCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DetallePrimaRiesgoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("saving DetallePrimaRiesgoCotizacionDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DetallePrimaRiesgoCotizacionDTO entity.
	  @param entity DetallePrimaRiesgoCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DetallePrimaRiesgoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("deleting DetallePrimaRiesgoCotizacionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DetallePrimaRiesgoCotizacionDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DetallePrimaRiesgoCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the DetallePrimaRiesgoCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DetallePrimaRiesgoCotizacionDTO entity to update
	 @return DetallePrimaRiesgoCotizacionDTO the persisted DetallePrimaRiesgoCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DetallePrimaRiesgoCotizacionDTO update(DetallePrimaRiesgoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("updating DetallePrimaRiesgoCotizacionDTO instance", Level.INFO, null);
	        try {
            DetallePrimaRiesgoCotizacionDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DetallePrimaRiesgoCotizacionDTO findById( DetallePrimaRiesgoCotizacionId id) {
    				LogDeMidasEJB3.log("finding DetallePrimaRiesgoCotizacionDTO instance with id: " + id, Level.INFO, null);
	        try {
            DetallePrimaRiesgoCotizacionDTO instance = entityManager.find(DetallePrimaRiesgoCotizacionDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DetallePrimaRiesgoCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DetallePrimaRiesgoCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<DetallePrimaRiesgoCotizacionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DetallePrimaRiesgoCotizacionDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding DetallePrimaRiesgoCotizacionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DetallePrimaRiesgoCotizacionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all DetallePrimaRiesgoCotizacionDTO entities.
	  	  @return List<DetallePrimaRiesgoCotizacionDTO> all DetallePrimaRiesgoCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DetallePrimaRiesgoCotizacionDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all DetallePrimaRiesgoCotizacionDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from DetallePrimaRiesgoCotizacionDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * 	findByRiesgoCotizacion. Encuentra la lista de entidades DetallePrimaRiesgoCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto riesgoCotizacionId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, idToCobertura, idToRiesgo.
	 * @param RiesgoCotizacionId  
	 * @return List<DetallePrimaRiesgoCotizacionDTO>
	 */
	@SuppressWarnings("unchecked")
	public List<DetallePrimaRiesgoCotizacionDTO> findByRiesgoCotizacion(RiesgoCotizacionId riesgoCotId) {
		LogDeMidasEJB3.log("Buscando DetallePrimaRiesgoCotizacion de la cotizacion: "+riesgoCotId.getIdToRiesgo()+" Inciso: "+
				riesgoCotId.getNumeroInciso()+" Seccion: "+riesgoCotId.getIdToSeccion()+" Cobertura: "+riesgoCotId.getIdToCobertura()+
				" Riesgo: "+riesgoCotId.getIdToRiesgo(),Level.INFO, null);
		try {
			final String queryString = "select model from DetallePrimaRiesgoCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion " +
					" and model.id.numeroInciso = :numeroInciso and model.id.idToSeccion = :idToSeccion and model.id.idToCobertura = :idToCobertura " +
					" and model.id.idToRiesgo = :idToRiesgo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", riesgoCotId.getIdToCotizacion());
			query.setParameter("numeroInciso", riesgoCotId.getNumeroInciso());
			query.setParameter("idToSeccion", riesgoCotId.getIdToSeccion());
			query.setParameter("idToCobertura", riesgoCotId.getIdToCobertura());
			query.setParameter("idToRiesgo", riesgoCotId.getIdToRiesgo());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			List<DetallePrimaRiesgoCotizacionDTO>  lista = query.getResultList();
			return lista;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DetallePrimaRiesgoCotizacionDTO> findBySubInciso(SubIncisoCotizacionDTOId id) {
		String queryString = "select model from DetallePrimaRiesgoCotizacionDTO model where " +
				"model.id.idToCotizacion = :idToCotizacion ";
		if(id.getNumeroInciso() != null) {
			queryString += "and model.id.numeroInciso = :numeroInciso ";
		}
		if(id.getIdToSeccion() != null) {
			queryString += "and model.id.idToSeccion = :idToSeccion ";
		}
		if(id.getNumeroSubInciso() != null) {
			queryString += "and model.id.numeroSubInciso = :numeroSubInciso ";
		}
		queryString += "and model.riesgoCotizacionDTO.coberturaCotizacionDTO.seccionCotizacionDTO.claveContrato = 1 " +
				"and model.riesgoCotizacionDTO.coberturaCotizacionDTO.claveContrato = 1 " +
				"and model.riesgoCotizacionDTO.claveContrato = 1";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCotizacion", id.getIdToCotizacion());
		if(id.getNumeroInciso() != null) {
			query.setParameter("numeroInciso", id.getNumeroInciso());
		}
		if(id.getIdToSeccion() != null) {
			query.setParameter("idToSeccion", id.getIdToSeccion());
		}
		if(id.getNumeroSubInciso() != null) {
			query.setParameter("numeroSubInciso", id.getNumeroSubInciso());
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<DetallePrimaRiesgoCotizacionDTO> listarFiltado(DetallePrimaRiesgoCotizacionId detallePrimaRiesgoCotizacionId) {
		LogDeMidasEJB3.log("Buscando DetallePrimaRiesgoCotizacion de la cotizacion: "+" Inciso: "+
				detallePrimaRiesgoCotizacionId.getNumeroInciso()+" Seccion: "+detallePrimaRiesgoCotizacionId.getIdToSeccion()
				+" Cobertura: "+detallePrimaRiesgoCotizacionId.getIdToCobertura()
				,Level.INFO, null);
		try {
		        if (detallePrimaRiesgoCotizacionId == null)
			return null;
		        
			String queryString = "select model from DetallePrimaRiesgoCotizacionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToCotizacion", detallePrimaRiesgoCotizacionId.getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.numeroInciso",detallePrimaRiesgoCotizacionId.getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToSeccion",detallePrimaRiesgoCotizacionId.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.numeroSubInciso",detallePrimaRiesgoCotizacionId.getNumeroSubInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToCobertura",detallePrimaRiesgoCotizacionId.getIdToCobertura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToRiesgo",detallePrimaRiesgoCotizacionId.getIdToRiesgo());
					
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	public void deleteDetallePrimaRiesgoCotizacion(RiesgoCotizacionId riesgoCotId) {
		LogDeMidasEJB3.log("deleteDetallePrimaRiesgoCotizacion: "+riesgoCotId.getIdToRiesgo()+" Inciso: "+
				riesgoCotId.getNumeroInciso()+" Seccion: "+riesgoCotId.getIdToSeccion()+" Cobertura: "+riesgoCotId.getIdToCobertura(),Level.INFO, null);
		try {
			String queryString = "delete from DetallePrimaRiesgoCotizacionDTO model " +
					"where model.id.idToCotizacion = :idToCotizacion " +
					"and model.id.numeroInciso = :numeroInciso " +
					"and model.id.idToSeccion = :idToSeccion";
			if(riesgoCotId.getIdToCobertura() != null) {
				queryString += " and model.id.idToCobertura = :idToCobertura";
				if(riesgoCotId.getIdToRiesgo() != null) {
					queryString += " and model.id.idToRiesgo = :idToRiesgo";
				}
			}
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", riesgoCotId.getIdToCotizacion());
			query.setParameter("numeroInciso", riesgoCotId.getNumeroInciso());
			query.setParameter("idToSeccion", riesgoCotId.getIdToSeccion());
			if(riesgoCotId.getIdToCobertura() != null) {
				query.setParameter("idToCobertura", riesgoCotId.getIdToCobertura());
				if(riesgoCotId.getIdToRiesgo() != null) {
					query.setParameter("idToRiesgo", riesgoCotId.getIdToRiesgo());
				}
			}
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("deleteDetallePrimaRiesgoCotizacion", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	  public void borrarFiltrado(DetallePrimaRiesgoCotizacionId detallePrimaRiesgoCotizacionId) {
		LogDeMidasEJB3.log("borrarFiltrado: "+detallePrimaRiesgoCotizacionId.getIdToCotizacion()+" Inciso: "+
			detallePrimaRiesgoCotizacionId.getNumeroInciso()+" Seccion: "+detallePrimaRiesgoCotizacionId.getIdToSeccion()+
			" NumSubInciso: "+detallePrimaRiesgoCotizacionId.getNumeroSubInciso()+
			" Cobertura: "+detallePrimaRiesgoCotizacionId.getIdToCobertura(),Level.INFO, null);
		try {
			String queryString = "delete from DetallePrimaRiesgoCotizacionDTO model " +
					"where model.id.idToCotizacion = :idToCotizacion " +
					"and model.id.numeroInciso = :numeroInciso ";
					
			if(detallePrimaRiesgoCotizacionId.getIdToSeccion()!=null){
			    queryString+=" and model.id.idToSeccion = :idToSeccion ";
			}
			if(detallePrimaRiesgoCotizacionId.getNumeroSubInciso()!=null){
			    queryString+=" and model.id.numeroSubInciso = :numeroSubInciso ";
			 }
        	       if(detallePrimaRiesgoCotizacionId.getIdToCobertura() != null) {
        		  queryString += " and model.id.idToCobertura = :idToCobertura ";
        		}
        		if(detallePrimaRiesgoCotizacionId.getIdToRiesgo() != null) {
        		   queryString += " and model.id.idToRiesgo = :idToRiesgo ";
        		}
        			
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", detallePrimaRiesgoCotizacionId.getIdToCotizacion());
			query.setParameter("numeroInciso", detallePrimaRiesgoCotizacionId.getNumeroInciso());
			
			if(detallePrimaRiesgoCotizacionId.getIdToSeccion()!=null){
			    query.setParameter("idToSeccion", detallePrimaRiesgoCotizacionId.getIdToSeccion());
			}
			if(detallePrimaRiesgoCotizacionId.getNumeroSubInciso()!=null){
			    query.setParameter("numeroSubInciso", detallePrimaRiesgoCotizacionId.getNumeroSubInciso());
			}
        	        if(detallePrimaRiesgoCotizacionId.getIdToCobertura() != null) {
			    query.setParameter("idToCobertura", detallePrimaRiesgoCotizacionId.getIdToCobertura());
			}
			if(detallePrimaRiesgoCotizacionId.getIdToRiesgo() != null) {
			    query.setParameter("idToRiesgo", detallePrimaRiesgoCotizacionId.getIdToRiesgo());
			 }
			
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("borrarFiltrado failed", Level.SEVERE, re);
			throw re;
		}
	}
}