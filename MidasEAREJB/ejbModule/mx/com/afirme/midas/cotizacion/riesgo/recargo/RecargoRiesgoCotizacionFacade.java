package mx.com.afirme.midas.cotizacion.riesgo.recargo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity RecargoRiesgoCotizacionDTO.
 * 
 * @see .RecargoRiesgoCotizacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class RecargoRiesgoCotizacionFacade implements
		RecargoRiesgoCotizacionFacadeRemote {
	// property constants
	public static final String CLAVE_AUTORIZACION = "claveAutorizacion";
	public static final String CODIGO_USUARIO_AUTORIZACION = "codigoUsuarioAutorizacion";
	public static final String VALOR_RECARGO = "valorRecargo";
	public static final String CLAVE_OBLIGATORIEDAD = "claveObligatoriedad";
	public static final String CLAVE_CONTRATO = "claveContrato";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * RecargoRiesgoCotizacionDTO entity. All subsequent persist actions of this
	 * entity should use the #update() method.
	 * 
	 * @param entity
	 *            RecargoRiesgoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RecargoRiesgoCotizacionDTO entity) {
		LogDeMidasEJB3.log("saving RecargoRiesgoCotizacionDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent RecargoRiesgoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            RecargoRiesgoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RecargoRiesgoCotizacionDTO entity) {
		LogDeMidasEJB3.log("deleting RecargoRiesgoCotizacionDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					RecargoRiesgoCotizacionDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved RecargoRiesgoCotizacionDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * RecargoRiesgoCotizacionDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            RecargoRiesgoCotizacionDTO entity to update
	 * @return RecargoRiesgoCotizacionDTO the persisted
	 *         RecargoRiesgoCotizacionDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RecargoRiesgoCotizacionDTO update(RecargoRiesgoCotizacionDTO entity) {
		LogDeMidasEJB3.log("updating RecargoRiesgoCotizacionDTO instance",
				Level.INFO, null);
		try {
			RecargoRiesgoCotizacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RecargoRiesgoCotizacionDTO findById(RecargoRiesgoCotizacionId id) {
		LogDeMidasEJB3.log(
				"finding RecargoRiesgoCotizacionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			RecargoRiesgoCotizacionDTO instance = entityManager.find(
					RecargoRiesgoCotizacionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RecargoRiesgoCotizacionDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the RecargoRiesgoCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RecargoRiesgoCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RecargoRiesgoCotizacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding RecargoRiesgoCotizacionDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RecargoRiesgoCotizacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<RecargoRiesgoCotizacionDTO> findByClaveAutorizacion(
			Object claveAutorizacion) {
		return findByProperty(CLAVE_AUTORIZACION, claveAutorizacion);
	}

	public List<RecargoRiesgoCotizacionDTO> findByCodigoUsuarioAutorizacion(
			Object codigoUsuarioAutorizacion) {
		return findByProperty(CODIGO_USUARIO_AUTORIZACION,
				codigoUsuarioAutorizacion);
	}

	public List<RecargoRiesgoCotizacionDTO> findByValorRecargo(
			Object valorRecargo) {
		return findByProperty(VALOR_RECARGO, valorRecargo);
	}

	public List<RecargoRiesgoCotizacionDTO> findByClaveObligatoriedad(
			Object claveObligatoriedad) {
		return findByProperty(CLAVE_OBLIGATORIEDAD, claveObligatoriedad);
	}

	public List<RecargoRiesgoCotizacionDTO> findByClaveContrato(
			Object claveContrato) {
		return findByProperty(CLAVE_CONTRATO, claveContrato);
	}

	/**
	 * Find all RecargoRiesgoCotizacionDTO entities.
	 * 
	 * @return List<RecargoRiesgoCotizacionDTO> all RecargoRiesgoCotizacionDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<RecargoRiesgoCotizacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all RecargoRiesgoCotizacionDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from RecargoRiesgoCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * findByRiesgoCotizacion. Encuentra la lista de entidades
	 * RecargoRiesgoCotizacionDTO que tengan los atributos recibidos en el
	 * objeto riesgoCotId. Los atributos usados para la b�squeda son los
	 * siguientes: idToCotizacion, numeroInciso, idToSeccion, idToCobertura,
	 * idToRiesgo.
	 * 
	 * @param RiesgoCotizacionId
	 * @return List<RecargoRiesgoCotizacionDTO>
	 */
	@SuppressWarnings("unchecked")
	public List<RecargoRiesgoCotizacionDTO> findByRiesgoCotizacion(RiesgoCotizacionId riesgoCotId) {
		LogDeMidasEJB3.log(
				"Buscando RecargoRiesgoCotizacion de la cotizacion: "
						+ riesgoCotId.getIdToCotizacion() + " Inciso: "+ riesgoCotId.getNumeroInciso() + " Seccion: "
						+ riesgoCotId.getIdToSeccion() + " Cobertura: "+ riesgoCotId.getIdToCobertura() + " Riesgo: "
						+ riesgoCotId.getIdToRiesgo(), Level.INFO, null);
		try {
			String queryString = "select model from RecargoRiesgoCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion "
					+ " and model.id.numeroInciso = :numeroInciso and model.id.idToSeccion = :idToSeccion and model.id.idToCobertura = :idToCobertura ";
			if(riesgoCotId.getIdToRiesgo()!= null)
				queryString = queryString+ " and model.id.idToRiesgo = :idToRiesgo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", riesgoCotId.getIdToCotizacion());
			query.setParameter("numeroInciso", riesgoCotId.getNumeroInciso());
			query.setParameter("idToSeccion", riesgoCotId.getIdToSeccion());
			query.setParameter("idToCobertura", riesgoCotId.getIdToCobertura());
			if(riesgoCotId.getIdToRiesgo()!= null)
				query.setParameter("idToRiesgo", riesgoCotId.getIdToRiesgo());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			List<RecargoRiesgoCotizacionDTO> lista = query.getResultList();
			return lista;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RecargoRiesgoCotizacionDTO> findByCoberturaCotizacion(
			RiesgoCotizacionId riesgoCotId) {
		LogDeMidasEJB3.log(
				"Buscando RecargoRiesgoCotizacion de la cotizacion: "
						+ riesgoCotId.getIdToCotizacion()+ " Inciso: "
						+ riesgoCotId.getNumeroInciso() + " Seccion: "
						+ riesgoCotId.getIdToSeccion() + " Cobertura: "
						+ riesgoCotId.getIdToCobertura(), Level.INFO, null);
		try {
			final String queryString = "select model from RecargoRiesgoCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion "
					+ " and model.id.numeroInciso = :numeroInciso and model.id.idToSeccion = :idToSeccion and model.id.idToCobertura = :idToCobertura "
					+ " and model.id.idToRiesgo = :idToRiesgo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", riesgoCotId
					.getIdToCotizacion());
			query.setParameter("numeroInciso", riesgoCotId.getNumeroInciso());
			query.setParameter("idToSeccion", riesgoCotId.getIdToSeccion());
			query.setParameter("idToCobertura", riesgoCotId.getIdToCobertura());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RecargoRiesgoCotizacionDTO> listarRecargosPendientesAutorizacion(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("filtering RecargoRiesgoCotizacionDTO instance", Level.INFO, null);
		try {
			String queryString = "select model from RecargoRiesgoCotizacionDTO model " +
					"where model.claveAutorizacion = 1 " +
					"and model.id.idToCotizacion = :idToCotizacion " +
					"and model.riesgoCotizacionDTO.coberturaCotizacionDTO.seccionCotizacionDTO.claveContrato = 1 " +
					"and model.riesgoCotizacionDTO.coberturaCotizacionDTO.claveContrato = 1 " +
					"and model.riesgoCotizacionDTO.claveContrato = 1 " +
					"and model.claveContrato = 1";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
}