package mx.com.afirme.midas.cotizacion.calculo;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.GrupoParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;

@Stateless
public class CalculoCotizacionFacade implements CalculoCotizacionFacadeRemote {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private static ParametroGeneralFacadeRemote parametroGeneralFacade;
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CalculoLUCDTO calcularLUC(CalculoLUCDTO calculoLUCDTO, String nombreUsuario)throws Exception{
		StoredProcedureHelper storedHelper = null;
		try {	
			
			LogDeMidasEJB3.log("Entrando a calcularLUC..." + this, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKGDAN_CALCULOSLUC.SPDAN_CALCULALUC", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper
					.estableceMapeoResultados(
							"mx.com.afirme.midas.cotizacion.calculo.CalculoLUCDTO",
							"primaNetaLUC," +
							"cuotaLUC",
							"PRIMANETALUC," +
							"CUOTALUC");
			storedHelper.estableceParametro("PidToCotizacion", calculoLUCDTO.getIdToCotizacion());
			storedHelper.estableceParametro("PidToSeccion", calculoLUCDTO.getIdToSeccion());
			storedHelper.estableceParametro("PsumaAsegurada", calculoLUCDTO.getSumaAsegurada());
			
			CalculoLUCDTO calculoLUCDTOResultado = (CalculoLUCDTO)storedHelper.obtieneResultadoSencillo();
			
			LogDeMidasEJB3.log("Saliendo de calcularLUC..." + this, Level.INFO, null);
			return calculoLUCDTOResultado;
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"MIDAS.PKGDAN_CALCULOSLUC.SPDAN_CALCULALUC", calculoLUCDTO, codErr, descErr);
			LogDeMidasEJB3.log("Excepcion en BD de calcularLUC..." + this, Level.WARNING, e);
			return null;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion General en calcularLUC..." + this, Level.WARNING, e);
			throw e;
		}		
	}
	
	
	public CalculoRiesgoDTO calcularRiesgo(CalculoRiesgoDTO calculoRiesgoDTO, String nombreUsuario) throws Exception{
		StoredProcedureHelper storedHelper = null;
		try {
			
			String nombreSP = "MIDAS.PKGDAN_CALCULOSRIESGOS.SPDAN_CALCULARIESGO";
			
			//Consulta de par�metro general para determinar el SP de c�lculo a utilizar.
			
			ParametroGeneralDTO parametroGeneral = consultarParametroGeneral(
					calculoRiesgoDTO.getIdToCotizacion(), 
					GrupoParametroGeneralDTO.GRUPO_SP_CALCULO_RIESGO_POR_PRODUCTO,new BigDecimal("80000"));
			
			if(parametroGeneral != null){
				nombreSP = parametroGeneral.getValor();
			}
			
			LogDeMidasEJB3.log("Entrando a calcularRiesgo. SP a ejecutar: " + nombreSP, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(
							"mx.com.afirme.midas.cotizacion.calculo.CalculoRiesgoDTO",
							"primaNetaB," +
							"primaNetaARDT," +
							"primaNetaARDV" ,
							
							"PRIMANETAB," +
							"PRIMANETAARDT," +
							"PRIMANETAARDV");

			storedHelper.estableceParametro("PidToCotizacion", calculoRiesgoDTO.getIdToCotizacion());
			storedHelper.estableceParametro("PnumeroInciso", calculoRiesgoDTO.getNumeroInciso());
			storedHelper.estableceParametro("PidToSeccion", calculoRiesgoDTO.getIdToSeccion());
			storedHelper.estableceParametro("PidToCobertura", calculoRiesgoDTO.getIdToCobertura());
			storedHelper.estableceParametro("PidToRiesgo", calculoRiesgoDTO.getIdToRiesgo());
			storedHelper.estableceParametro("PnumeroSubInciso", calculoRiesgoDTO.getNumeroSubInciso());
			storedHelper.estableceParametro("PsumaAsegurada", calculoRiesgoDTO.getSumaAsegurada());
			
			CalculoRiesgoDTO calculoRiesgoDTOResultado = (CalculoRiesgoDTO)storedHelper.obtieneResultadoSencillo();
			LogDeMidasEJB3.log("Saliendo de calcularRiesgo. SP ejecutado: " + nombreSP, Level.INFO, null);
			return calculoRiesgoDTOResultado;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"MIDAS.PKGDAN_CALCULOSRIESGOS.SPDAN_CALCULARIESGO", calculoRiesgoDTO, codErr, descErr);
			LogDeMidasEJB3.log("Excepcion en BD de calcularRiesgo." , Level.WARNING, e);
			return null;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion General en calcularRiesgo.", Level.WARNING, e);
			throw e;
		}
	}
	

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CalculoRiesgoDTO calcularRiesgoEmbarque(CalculoRiesgoDTO calculoRiesgoDTO, String nombreUsuario) throws Exception{
		StoredProcedureHelper storedHelper = null;
		try {
			
			LogDeMidasEJB3.log("Entrando a calcularRiesgoEmbarque..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKGDAN_CALCULOSRIESGOS.SPDAN_CALCULARIESGO_EMBARQUE", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper
					.estableceMapeoResultados(
							"mx.com.afirme.midas.cotizacion.calculo.CalculoRiesgoDTO",
							"primaNetaB," +
							"primaNetaARDT," +
							"primaNetaARDV" ,
							
							"PRIMANETAB," +
							"PRIMANETAARDT," +
							"PRIMANETAARDV");

			storedHelper.estableceParametro("PidToCotizacion", calculoRiesgoDTO.getIdToCotizacion());
			storedHelper.estableceParametro("PnumeroInciso", calculoRiesgoDTO.getNumeroInciso());
			storedHelper.estableceParametro("PidToSeccion", calculoRiesgoDTO.getIdToSeccion());
			storedHelper.estableceParametro("PnumeroSubInciso", calculoRiesgoDTO.getNumeroSubInciso());
			storedHelper.estableceParametro("PsumaAsegurada", calculoRiesgoDTO.getSumaAsegurada());
			
			CalculoRiesgoDTO calculoRiesgoDTOResultado = (CalculoRiesgoDTO)storedHelper.obtieneResultadoSencillo();
			LogDeMidasEJB3.log("Saliendo de calcularRiesgoEmbarque..." + this, Level.INFO, null);
			return calculoRiesgoDTOResultado;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"MIDAS.PKGDAN_CALCULOSRIESGOS.SPDAN_CALCULARIESGO_EMBARQUE", calculoRiesgoDTO, codErr, descErr);
			LogDeMidasEJB3.log("Excepcion en BD de calcularRiesgoEmbarque..." + this, Level.WARNING, e);
			return null;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion General en calcularRiesgoEmbarque..." + this, Level.WARNING, e);
			throw e;
		}
	}
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CalculoRiesgoDTO eliminarRiesgoEmbarque(CalculoRiesgoDTO calculoRiesgoDTO, String nombreUsuario) throws Exception{
		StoredProcedureHelper storedHelper = null;
		try {
			
			LogDeMidasEJB3.log("Entrando a eliminarRiesgoEmbarque..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKGDAN_CALCULOSRIESGOS.SPDAN_ELIMINARIESGO_EMBARQUE", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper
					.estableceMapeoResultados(
							"mx.com.afirme.midas.cotizacion.calculo.CalculoRiesgoDTO",
							"primaNetaB," +
							"primaNetaARDT," +
							"primaNetaARDV" ,
							
							"PRIMANETAB," +
							"PRIMANETAARDT," +
							"PRIMANETAARDV");

			storedHelper.estableceParametro("PidToCotizacion", calculoRiesgoDTO.getIdToCotizacion());
			storedHelper.estableceParametro("PnumeroInciso", calculoRiesgoDTO.getNumeroInciso());
			storedHelper.estableceParametro("PidToSeccion", calculoRiesgoDTO.getIdToSeccion());
			storedHelper.estableceParametro("PnumeroSubInciso", calculoRiesgoDTO.getNumeroSubInciso());
			
			CalculoRiesgoDTO calculoRiesgoDTOResultado = (CalculoRiesgoDTO)storedHelper.obtieneResultadoSencillo();
			LogDeMidasEJB3.log("Saliendo de eliminarRiesgoEmbarque..." + this, Level.INFO, null);
			return calculoRiesgoDTOResultado;
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"MIDAS.PKGDAN_CALCULOSRIESGOS.SPDAN_ELIMINARIESGO_EMBARQUE", calculoRiesgoDTO, codErr, descErr);
			LogDeMidasEJB3.log("Excepcion en BD de eliminarRiesgoEmbarque..." + this, Level.WARNING, e);
			return null;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion General en eliminarRiesgoEmbarque..." + this, Level.WARNING, e);
			throw e;
		}
	}
	
	
	public void inicializarCalculoRiesgo(BigDecimal idToCotizacion, String nombreUsuario) throws Exception{
		StoredProcedureHelper storedHelper = null;
		try {

			String nombreSP = "MIDAS.PKGDAN_CALCULOSRIESGOS.SPDAN_INICIALIZACALCULORIESGOS";
			
			//Consulta de par�metro general para determinar el SP de c�lculo a utilizar.
			ParametroGeneralDTO parametroGeneral = consultarParametroGeneral(idToCotizacion,
					GrupoParametroGeneralDTO.GRUPO_SP_INICIALIZA_CALCULO_RIESGO_POR_PRODUCTO,
					new BigDecimal("70000"));
			
			if(parametroGeneral != null){
				nombreSP = parametroGeneral.getValor();
			}
			
			LogDeMidasEJB3.log("Entrando a inicializarCalculoRiesgo. SP a ejecutar: " + nombreSP , Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.ejecutaActualizar();
			
			LogDeMidasEJB3.log("Saliendo de inicializarCalculoRiesgo. SP ejecutado: " + nombreSP, Level.INFO, null);
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.MODIFICAR,
					"MIDAS.PKGDAN_CALCULOSRIESGOS.SPDAN_INICIALIZACALCULORIESGOS", this, codErr, descErr);
			LogDeMidasEJB3.log("Excepcion en BD de inicializarCalculoRiesgo.", Level.WARNING, e);
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion General en inicializarCalculoRiesgo.", Level.WARNING, e);
			throw e;
		}

	}

	public void calcularRiesgosHidro(BigDecimal idToCotizacion, BigDecimal numeroInciso, String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		try {
			
			LogDeMidasEJB3.log("Entrando a calcularRiesgosHidro..." + this, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKGDAN_GENERALES.SPDAN_COADEDRIESGOSHIDROTEV", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("PidToCotizacion", idToCotizacion);
			storedHelper.estableceParametro("PnumeroInciso", numeroInciso);
			storedHelper.ejecutaActualizar();
			LogDeMidasEJB3.log("Saliendo de calcularRiesgosHidro..." + this, Level.INFO, null);
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.MODIFICAR,
					"MIDAS.PKGDAN_GENERALES.SPDAN_COADEDRIESGOSHIDROTEV", this, codErr, descErr);
			LogDeMidasEJB3.log("Excepcion en BD de calcularRiesgosHidro..." + this, Level.WARNING, e);
			
			//Se lanza un RuntimeException para que el contenedor haga rollback a la transaccion
			throw new RuntimeException(codErr + ": " + descErr);
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion General en calcularRiesgosHidro..." + this, Level.WARNING, e);
			
			//Se lanza un RuntimeException para que el contenedor haga rollback a la transaccion
			throw new RuntimeException(e);
		}

	}
	
	public CoberturaCotizacionDTO calcularCobertura(
			CoberturaCotizacionDTO coberturaCotizacionDTO,
			String nombreUsuario, boolean seContratoCobertura) throws Exception {
		return calcularCobertura(coberturaCotizacionDTO, nombreUsuario, seContratoCobertura, false);
	}
	
	
	public CoberturaCotizacionDTO calcularCoberturaCasa(
			CoberturaCotizacionDTO coberturaCotizacionDTO,
			String nombreUsuario, boolean seContratoCobertura) throws Exception {
		
		contrataSeccionCasa(coberturaCotizacionDTO);
		contrataDescontrataRiesgosCasa(coberturaCotizacionDTO);
		
		return calcularCobertura(coberturaCotizacionDTO, nombreUsuario, seContratoCobertura, true);
	}
	

	private CoberturaCotizacionDTO calcularCobertura(
			CoberturaCotizacionDTO coberturaCotizacionDTO,
			String nombreUsuario, boolean seContratoCobertura, boolean esCoberturaCasa) throws Exception {

		
		//1)	Se modifica la suma asegurada de la cobertura / Se contrata o descontrata una cobertura
		coberturaCotizacionDTO = coberturaCotizacionFacade.update(coberturaCotizacionDTO);
		
		
		if(coberturaCotizacionDTO.getCoberturaSeccionDTO().getSeccionDTO().getClaveSubIncisos().shortValue() 
				!= SistemaPersistencia.PERMITE_SUBINCISOS || esCoberturaCasa) {
			
			//2)	Se actualiza la suma asegurada de todos los riesgos de la cobertura
			//a.	Todos los riesgos tendr�n la misma suma asegurada de la cobertura independientemente del tipo de suma asegurada del riesgo y de su clave de contrato.
			RiesgoCotizacionId id = new RiesgoCotizacionId();
			
			id.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
			id.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
			id.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
			id.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
			
			List<RiesgoCotizacionDTO> riesgosPorCobertura = riesgoCotizacionFacade.listarPorIdFiltrado(id);
			if (riesgosPorCobertura.size() > 0){
				for(RiesgoCotizacionDTO riesgo: riesgosPorCobertura){
					riesgo.setValorSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
					riesgoCotizacionFacade.update(riesgo);
				}
			}
			//3)	Si se modific� / contrat� una cobertura con suma asegurada b�sica, 
			//	se modifican las sumas aseguradas amparadas de todos las coberturas amparadas por la b�sica, 
			//	as� como la suma asegurada de todos los riesgos de dichas coberturas amparadas.
			CoberturaDTO coberturaDTO = coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO();// coberturaFacade.findById(coberturaCotizacionDTO.getId().getIdToCobertura());
			if(coberturaDTO.getClaveTipoSumaAsegurada().equals(SistemaPersistencia.CLAVE_SUMA_ASEGURADA_BASICA)){
				
				SeccionCotizacionDTOId seccionId = new SeccionCotizacionDTOId();
				seccionId.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
				seccionId.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
				seccionId.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
				
				List<CoberturaCotizacionDTO> coberturasPorSeccion = coberturaCotizacionFacade.listarPorSeccionCotizacionId(seccionId,Boolean.FALSE);
				
				for(CoberturaCotizacionDTO coberturaPorSeccion: coberturasPorSeccion){
				    
					CoberturaDTO coberturaAsociada = coberturaFacade.findById(coberturaPorSeccion.getId().getIdToCobertura());
					if(!coberturaAsociada.getClaveTipoSumaAsegurada().equals(SistemaPersistencia.CLAVE_SUMA_ASEGURADA_SUBLIMITE)){
	        				if(coberturaAsociada.getIdCoberturaSumaAsegurada().intValue() == coberturaDTO.getIdToCobertura().intValue()){
	        					coberturaPorSeccion.setValorSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
	        					coberturaCotizacionFacade.update(coberturaPorSeccion);
	        					
	        					List<RiesgoCotizacionDTO> riesgosAsociados = riesgoCotizacionFacade.listarRiesgoContratadosPorCobertura(coberturaPorSeccion.getId());
	        					for(RiesgoCotizacionDTO riesgoAsociado: riesgosAsociados){
	        						riesgoAsociado.setValorSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
	        						riesgoCotizacionFacade.update(riesgoAsociado);
	        					}
	        				}
					}
				    
				}		
				
				//4)	Se actualiza la suma asegurada de la secci�n (sumatoria de sumas aseguradas B�SICAS de sus coberturas)
				SeccionCotizacionDTOId seccionCotizacionId = new SeccionCotizacionDTOId();
				
				seccionCotizacionId.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
				seccionCotizacionId.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
				seccionCotizacionId.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
				
				this.actualizarSumaAseguradaSeccion(seccionCotizacionId);
				
			}
			
			//5)	Se recalcula la prima neta de todos los riesgos de la cobertura (y la de la cobertura, secci�n e inciso):
			//	a.	Se obtiene la lista de riesgos contratados de la cobertura ordenados por idToRiesgo
			//	b.	Para cada uno de los riesgos obtenidos:
			//	i.	CalculoCotizacionDN.calcularRiesgo(idCotizacion, numInciso, idSeccion, idCobertura, idRiesgo, 0, sumaAseguradaRiesgo)
			List<RiesgoCotizacionDTO> riesgosContratados = riesgoCotizacionFacade.listarRiesgoContratadosPorCobertura(coberturaCotizacionDTO.getId());
			if (riesgosContratados.size() > 0){
				for (RiesgoCotizacionDTO riesgosContratado: riesgosContratados){
				    
					CalculoRiesgoDTO  calculoRiesgoDTO = new CalculoRiesgoDTO();
					calculoRiesgoDTO.setIdToCotizacion(riesgosContratado.getId().getIdToCotizacion());
					calculoRiesgoDTO.setNumeroInciso(riesgosContratado.getId().getNumeroInciso());
					calculoRiesgoDTO.setIdToSeccion(riesgosContratado.getId().getIdToSeccion());
					calculoRiesgoDTO.setIdToRiesgo(riesgosContratado.getId().getIdToRiesgo());
					calculoRiesgoDTO.setIdToCobertura(riesgosContratado.getId().getIdToCobertura());
					calculoRiesgoDTO.setNumeroSubInciso(BigDecimal.valueOf(0));
					calculoRiesgoDTO.setSumaAsegurada(riesgosContratado.getValorSumaAsegurada());

					inicializarCalculoRiesgo(riesgosContratado.getId().getIdToCotizacion(),nombreUsuario);
					calculoRiesgoDTO = calcularRiesgo(calculoRiesgoDTO, nombreUsuario);
				    
				}
			}
		} else {
			SubIncisoCotizacionDTO subIncisoCotDTO = new SubIncisoCotizacionDTO();
			subIncisoCotDTO.setId(new SubIncisoCotizacionDTOId());
			subIncisoCotDTO.getId().setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
			subIncisoCotDTO.getId().setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
			subIncisoCotDTO.getId().setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
			
			CoberturaDTO coberturaDTO = coberturaFacade.findById(coberturaCotizacionDTO.getId().getIdToCobertura());

			List<SubIncisoCotizacionDTO> listaSubIncisos = subIncisoCotizacionFacade.listarFiltrado(subIncisoCotDTO);			
			if(seContratoCobertura){

				//1) se acumula la SA de la Cobertura con la sumatoria de la SA de Subincisos	       
				Double total = 0D;
				for(SubIncisoCotizacionDTO subInciso : listaSubIncisos) {
					total += subInciso.getValorSumaAsegurada();
				}			
				//3)Se actualiza la suma asegurada de la cobertura con la sumatoria de los Subincisos
				coberturaCotizacionDTO.setValorSumaAsegurada(total);
				coberturaCotizacionDTO = coberturaCotizacionFacade.update(coberturaCotizacionDTO);
				//4) se borra el detalle de primas de cobertura y riesgo
				if(coberturaDTO.getClaveTipoSumaAsegurada().equals(SistemaPersistencia.CLAVE_SUMA_ASEGURADA_BASICA)){
					eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(coberturaCotizacionDTO);
					List<CoberturaCotizacionDTO> coberturasAmparadas = coberturaCotizacionFacade.listarCoberturasDeSumaAsegurada(coberturaCotizacionDTO);
					for(CoberturaCotizacionDTO coberturaAmparada: coberturasAmparadas){
						eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(coberturaAmparada);
					}			
				}else{
					eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(coberturaCotizacionDTO);
				}
				
				
				//5)Se actualiza la suma asegurada de los riesgos de la cobertura
				RiesgoCotizacionId idRiesgo = new RiesgoCotizacionId();
				idRiesgo.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
				idRiesgo.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
				idRiesgo.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
				idRiesgo.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
				List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionFacade.listarPorIdFiltrado(idRiesgo);
				for(RiesgoCotizacionDTO riesgo : riesgos) {
					//a.Todos los riesgos tendr�n la misma suma asegurada de la cobertura 
					//independientemente del tipo de suma asegurada del riesgo y de su clave de contrato.
					riesgo.setValorSumaAsegurada(total);
					riesgoCotizacionFacade.update(riesgo);
				}				
				for(SubIncisoCotizacionDTO subInciso : listaSubIncisos){
					//a.Se obtiene la lista de riesgos contratados de todas las coberturas contratadas de la secci�n ordenados por idToRiesgo
					List<RiesgoCotizacionDTO> riesgosCal = riesgoCotizacionFacade.listarRiesgoContratadosPorSeccion(coberturaCotizacionDTO.getSeccionCotizacionDTO().getId());
					for(RiesgoCotizacionDTO riesgo : riesgosCal) {
						CalculoRiesgoDTO calculoRiesgoDTO = new CalculoRiesgoDTO();
						calculoRiesgoDTO.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
						calculoRiesgoDTO.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
						calculoRiesgoDTO.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
						calculoRiesgoDTO.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
						calculoRiesgoDTO.setIdToRiesgo(riesgo.getId().getIdToRiesgo());
						calculoRiesgoDTO.setNumeroSubInciso(subInciso.getId().getNumeroSubInciso());
						calculoRiesgoDTO.setSumaAsegurada(subInciso.getValorSumaAsegurada());
						//b.Para cada uno de los riesgos obtenidos:
						inicializarCalculoRiesgo(coberturaCotizacionDTO.getId().getIdToCotizacion(),nombreUsuario);
						calculoRiesgoDTO = calcularRiesgo(calculoRiesgoDTO, nombreUsuario);
					}
				}				
			
			}else{
				//1) se borra el detalle de primas de cobertura y riesgo
				if(coberturaDTO.getClaveTipoSumaAsegurada().equals(SistemaPersistencia.CLAVE_SUMA_ASEGURADA_BASICA)){
					eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(coberturaCotizacionDTO);
					List<CoberturaCotizacionDTO> coberturasAmparadas = coberturaCotizacionFacade.listarCoberturasDeSumaAsegurada(coberturaCotizacionDTO);
					for(CoberturaCotizacionDTO coberturaAmparada: coberturasAmparadas){
						eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(coberturaAmparada);
					}			
				}else{
					eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(coberturaCotizacionDTO);
				}	
				//2)	Se actualiza la suma asegurada de todos los riesgos de la cobertura
				//a.	Todos los riesgos tendr�n la misma suma asegurada de la cobertura independientemente del tipo de suma asegurada del riesgo y de su clave de contrato.
				
				if(coberturaDTO.getClaveTipoSumaAsegurada().equals(SistemaPersistencia.CLAVE_SUMA_ASEGURADA_BASICA)){
					
					SeccionCotizacionDTOId seccionId = new SeccionCotizacionDTOId();
					seccionId.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
					seccionId.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
					seccionId.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
					
					List<CoberturaCotizacionDTO> coberturasPorSeccion = coberturaCotizacionFacade.listarPorSeccionCotizacionId(seccionId,Boolean.FALSE);
					
					for(CoberturaCotizacionDTO coberturaPorSeccion: coberturasPorSeccion){
					    
						CoberturaDTO coberturaAsociada = coberturaFacade.findById(coberturaPorSeccion.getId().getIdToCobertura());
						if(!coberturaAsociada.getClaveTipoSumaAsegurada().equals(SistemaPersistencia.CLAVE_SUMA_ASEGURADA_SUBLIMITE)){
		        				if(coberturaAsociada.getIdCoberturaSumaAsegurada().intValue() == coberturaDTO.getIdToCobertura().intValue()){
		        					coberturaPorSeccion.setValorSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
		        					coberturaCotizacionFacade.update(coberturaPorSeccion);
		        					
		        					List<RiesgoCotizacionDTO> riesgosAsociados = riesgoCotizacionFacade.listarRiesgoContratadosPorCobertura(coberturaPorSeccion.getId());
		        					for(RiesgoCotizacionDTO riesgoAsociado: riesgosAsociados){
		        						riesgoAsociado.setValorSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
		        						riesgoCotizacionFacade.update(riesgoAsociado);
		        						
		        						CalculoRiesgoDTO  calculoRiesgoDTO = new CalculoRiesgoDTO();
		        						calculoRiesgoDTO.setIdToCotizacion(riesgoAsociado.getId().getIdToCotizacion());
		        						calculoRiesgoDTO.setNumeroInciso(riesgoAsociado.getId().getNumeroInciso());
		        						calculoRiesgoDTO.setIdToSeccion(riesgoAsociado.getId().getIdToSeccion());
		        						calculoRiesgoDTO.setIdToRiesgo(riesgoAsociado.getId().getIdToRiesgo());
		        						calculoRiesgoDTO.setIdToCobertura(riesgoAsociado.getId().getIdToCobertura());
		        						calculoRiesgoDTO.setNumeroSubInciso(BigDecimal.valueOf(0));
		        						calculoRiesgoDTO.setSumaAsegurada(riesgoAsociado.getValorSumaAsegurada());

		        						inicializarCalculoRiesgo(riesgoAsociado.getId().getIdToCotizacion(),nombreUsuario);
		        						calculoRiesgoDTO = calcularRiesgo(calculoRiesgoDTO, nombreUsuario);				        						
		        					}
		        				}
						}
					    
					}		
					
					//4)	Se actualiza la suma asegurada de la secci�n (sumatoria de sumas aseguradas B�SICAS de sus coberturas)
					SeccionCotizacionDTOId seccionCotizacionId = new SeccionCotizacionDTOId();
					
					seccionCotizacionId.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
					seccionCotizacionId.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
					seccionCotizacionId.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
					
					this.actualizarSumaAseguradaSeccion(seccionCotizacionId);				
				
					RiesgoCotizacionId id = new RiesgoCotizacionId();
					
					id.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
					id.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
					id.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
					id.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
					
					List<RiesgoCotizacionDTO> riesgosPorCobertura = riesgoCotizacionFacade.listarPorIdFiltrado(id);
					if (riesgosPorCobertura.size() > 0){
						for(RiesgoCotizacionDTO riesgo: riesgosPorCobertura){
							riesgo.setValorSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
							riesgoCotizacionFacade.update(riesgo);		

							CalculoRiesgoDTO  calculoRiesgoDTO = new CalculoRiesgoDTO();
    						calculoRiesgoDTO.setIdToCotizacion(riesgo.getId().getIdToCotizacion());
    						calculoRiesgoDTO.setNumeroInciso(riesgo.getId().getNumeroInciso());
    						calculoRiesgoDTO.setIdToSeccion(riesgo.getId().getIdToSeccion());
    						calculoRiesgoDTO.setIdToRiesgo(riesgo.getId().getIdToRiesgo());
    						calculoRiesgoDTO.setIdToCobertura(riesgo.getId().getIdToCobertura());
    						calculoRiesgoDTO.setNumeroSubInciso(BigDecimal.valueOf(0));
    						calculoRiesgoDTO.setSumaAsegurada(riesgo.getValorSumaAsegurada());

    						inicializarCalculoRiesgo(riesgo.getId().getIdToCotizacion(),nombreUsuario);
    						calculoRiesgoDTO = calcularRiesgo(calculoRiesgoDTO, nombreUsuario);								
						}
					}
				} else {
					RiesgoCotizacionId id = new RiesgoCotizacionId();
					id.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
					id.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
					id.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
					id.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
					
					List<RiesgoCotizacionDTO> riesgosPorCobertura = riesgoCotizacionFacade.listarPorIdFiltrado(id);
					for(RiesgoCotizacionDTO riesgo : riesgosPorCobertura) {
						CalculoRiesgoDTO calculoRiesgoDTO = new CalculoRiesgoDTO();
						calculoRiesgoDTO.setIdToCotizacion(riesgo.getId().getIdToCotizacion());
						calculoRiesgoDTO.setNumeroInciso(riesgo.getId().getNumeroInciso());
						calculoRiesgoDTO.setIdToSeccion(riesgo.getId().getIdToSeccion());
						calculoRiesgoDTO.setIdToCobertura(riesgo.getId().getIdToCobertura());
						calculoRiesgoDTO.setIdToRiesgo(riesgo.getId().getIdToRiesgo());
						calculoRiesgoDTO.setNumeroSubInciso(BigDecimal.ZERO);
						calculoRiesgoDTO.setSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
						//b.Para cada uno de los riesgos obtenidos:
						inicializarCalculoRiesgo(riesgo.getId().getIdToCotizacion(),nombreUsuario);
						calculoRiesgoDTO = calcularRiesgo(calculoRiesgoDTO, nombreUsuario);
					}
				}
			}			
		}
		
		actualizaAnexosDeCoberturasContratadas(coberturaCotizacionDTO.getId().getIdToCotizacion());
		
		return coberturaCotizacionFacade.findById(coberturaCotizacionDTO.getId());
			
	}
	
	private void contrataDescontrataRiesgosCasa(CoberturaCotizacionDTO coberturaCotizacion) {
		
		boolean contratar = (coberturaCotizacion.getClaveContrato().shortValue() == SistemaPersistencia.CONTRATADO? true:false);
		
		boolean aplicarMerge = false;
		for (RiesgoCotizacionDTO riesgoCotizacion : coberturaCotizacion.getRiesgoCotizacionLista()) {
			aplicarMerge = false;
			
			if (riesgoCotizacion.getClaveContrato().shortValue() == SistemaPersistencia.NO_CONTRATADO && contratar) {
				riesgoCotizacion.setClaveContrato(SistemaPersistencia.CONTRATADO);
				aplicarMerge = true;
			} else if (riesgoCotizacion.getClaveContrato().shortValue() == SistemaPersistencia.CONTRATADO && (!contratar)) {
				riesgoCotizacion.setClaveContrato(SistemaPersistencia.NO_CONTRATADO);
				aplicarMerge = true;
			}

			if(aplicarMerge){
				riesgoCotizacionFacade.update(riesgoCotizacion);
			}
		}
		
	}
	
	/**
	 * Contrata la seccion de casa en caso de que se contrate una de sus coberturas
	 * @param coberturaCotizacion
	 */
	private void contrataSeccionCasa(CoberturaCotizacionDTO coberturaCotizacion) {
		
		boolean contratar = (coberturaCotizacion.getClaveContrato().shortValue() == SistemaPersistencia.CONTRATADO? true:false);
		
		SeccionCotizacionDTO seccionCotizacion = coberturaCotizacion.getSeccionCotizacionDTO();
		
		if (seccionCotizacion.getClaveContrato().shortValue() == SistemaPersistencia.NO_CONTRATADO && contratar) {
			
			seccionCotizacion.setClaveContrato(SistemaPersistencia.CONTRATADO);
			
			seccionCotizacionFacade.update(seccionCotizacion);
		}
		
		
		
	}
	
	public void actualizaAnexosDeCoberturasContratadas(BigDecimal idToCotizacion) {
		//Obtener todas las distintas coberturas contratadas
		List<BigDecimal> coberturasContratadas = coberturaCotizacionFacade.listarCoberturasDistintas(idToCotizacion, true);
		//Limpiar todos los anexos a nivel cobertura
		List<DocAnexoCotDTO>  documentosAnexos= docAnexoCotFacade.findByProperty("id.idToCotizacion", idToCotizacion);
		for(DocAnexoCotDTO documentoAnexo: documentosAnexos){
			if(documentoAnexo.getIdToCobertura().intValue() != 0){
				if (!documentoAnexo.getClaveSeleccion().equals(SistemaPersistencia.NO_CONTRATADO)
						&& documentoAnexo.getClaveObligatoriedad().equals(SistemaPersistencia.DOCUMENTO_ANEXO_OBLIGATORIO)) {
					
					documentoAnexo.setClaveSeleccion(SistemaPersistencia.NO_CONTRATADO);
					documentoAnexo = docAnexoCotFacade.update(documentoAnexo);
					
				}
					
			}
		}
		//Seleccionar los anexos de las distintas coberturas contratadas 		
		for(BigDecimal idToCobertura: coberturasContratadas){
			for(DocAnexoCotDTO documentoAnexo: documentosAnexos){
				if(documentoAnexo.getIdToCobertura().intValue() ==
					idToCobertura.intValue()){
					if (!documentoAnexo.getClaveSeleccion().equals(SistemaPersistencia.CONTRATADO)
							&& documentoAnexo.getClaveObligatoriedad().equals(SistemaPersistencia.DOCUMENTO_ANEXO_OBLIGATORIO)) {
						
						documentoAnexo.setClaveSeleccion(SistemaPersistencia.CONTRATADO);
						documentoAnexo = docAnexoCotFacade.update(documentoAnexo);
					}
						
				}
			}
		}
	}
	
	
	private void eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(CoberturaCotizacionDTO coberturaCotizacionDTO) {
	       DetallePrimaCoberturaCotizacionId detallePrimaCoberturaCotizacionId= new DetallePrimaCoberturaCotizacionId();
	       detallePrimaCoberturaCotizacionId.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
	       detallePrimaCoberturaCotizacionId.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
	       detallePrimaCoberturaCotizacionId.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
	       detallePrimaCoberturaCotizacionId.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
	       detallePrimaCoberturaCotizacionFacade.borrarFiltrado(detallePrimaCoberturaCotizacionId);
	       
	       DetallePrimaRiesgoCotizacionId detallePrimaRiesgoCotizacionId= new DetallePrimaRiesgoCotizacionId();
	       detallePrimaRiesgoCotizacionId.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
	       detallePrimaRiesgoCotizacionId.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
	       detallePrimaRiesgoCotizacionId.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
	       detallePrimaRiesgoCotizacionId.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
	       detallePrimaRiesgoCotizacionFacade.borrarFiltrado(detallePrimaRiesgoCotizacionId);
	 }
	
	
	
	/**
	 * Este metodo actualiza la Suma Asuegurada de una seccion
	 * calculando en base a la sumatoria de la suma asegurada
	 * de sus coberturas basicas
	 * @param id
	 *            SeccionCotizacionDTOId id de la seccion
	 *            que se desea actualizar
	 */			
	private void actualizarSumaAseguradaSeccion(
			SeccionCotizacionDTOId id) {
		SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
		
		Double sumatoriaCoberturasBasicas = seccionCotizacionFacade.getSumatoriaCoberturasBasicas(id);
		seccionCotizacionDTO = seccionCotizacionFacade.findById(id);
		seccionCotizacionDTO.setValorSumaAsegurada(sumatoriaCoberturasBasicas);
		
		IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
		incisoCotizacionId.setIdToCotizacion(id.getIdToCotizacion());
		incisoCotizacionId.setNumeroInciso(id.getNumeroInciso());
		
		IncisoCotizacionDTO incisoCotizacionDTO = incisoCotizacionFacade.findById(incisoCotizacionId);
		seccionCotizacionDTO.setIncisoCotizacionDTO(incisoCotizacionDTO);
		
		seccionCotizacionFacade.update(seccionCotizacionDTO);
	}
	
	private ParametroGeneralDTO consultarParametroGeneral(BigDecimal idToCotizacion,
			BigDecimal idGrupoParametro,BigDecimal prefijoIdGrupoParametro){
		
		//Consulta de par�metro general para determinar el SP de c�lculo a utilizar.
		String queryString = "select model.solicitudDTO.productoDTO.codigo from " +
				"CotizacionDTO model where model.idToCotizacion = :idToCotizacion";
		
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCotizacion", idToCotizacion);
		
//		BigDecimal idToProducto = Utilerias.obtenerBigDecimal(query.getSingleResult());
		String codigoProducto = (String) query.getSingleResult();
		
		BigDecimal codigoParametroGeneral = prefijoIdGrupoParametro.add(Utilerias.regresaBigDecimal(codigoProducto));
		
		ParametroGeneralDTO parametroGeneral = null;
		ParametroGeneralId parametroGralId = new ParametroGeneralId();
		parametroGralId.setIdToGrupoParametroGeneral(idGrupoParametro);
		parametroGralId.setCodigoParametroGeneral(codigoParametroGeneral);
		
		parametroGeneral = parametroGeneralFacade.findById(parametroGralId);
		
		return parametroGeneral;
	}
	
	@EJB
	private DocAnexoCotFacadeRemote docAnexoCotFacade;
	
	@EJB
	private DetallePrimaRiesgoCotizacionFacadeRemote detallePrimaRiesgoCotizacionFacade;
	
	@EJB
	private DetallePrimaCoberturaCotizacionFacadeRemote detallePrimaCoberturaCotizacionFacade;
		
	@EJB
	private SubIncisoCotizacionFacadeRemote subIncisoCotizacionFacade;
	
	@EJB
	private IncisoCotizacionFacadeRemote incisoCotizacionFacade;
		
	@EJB
	private SeccionCotizacionFacadeRemote seccionCotizacionFacade;
		
	@EJB
	private CoberturaFacadeRemote coberturaFacade;
	
	@EJB
	private RiesgoCotizacionFacadeRemote riesgoCotizacionFacade;
	
	@EJB
	private CoberturaCotizacionFacadeRemote coberturaCotizacionFacade;
		
	
	
}
