package mx.com.afirme.midas.cotizacion.cobertura.detalleprima;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity DetPrimaCoberturaCotizacionDTO.
 * @see .DetPrimaCoberturaCotizacionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DetallePrimaCoberturaCotizacionFacade  implements DetallePrimaCoberturaCotizacionFacadeRemote {


    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DetPrimaCoberturaCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DetPrimaCoberturaCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DetallePrimaCoberturaCotizacionDTO entity) {
    				LogDeMidasEJB3.log("saving DetallePrimaCoberturaCotizacionDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DetallePrimaCoberturaCotizacionDTO entity.
	  @param entity DetallePrimaCoberturaCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DetallePrimaCoberturaCotizacionDTO entity) {
    				LogDeMidasEJB3.log("deleting DetallePrimaCoberturaCotizacionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DetallePrimaCoberturaCotizacionDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DetallePrimaCoberturaCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the DetallePrimaCoberturaCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DetallePrimaCoberturaCotizacionDTO entity to update
	 @return DetallePrimaCoberturaCotizacionDTO the persisted DetallePrimaCoberturaCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DetallePrimaCoberturaCotizacionDTO update(DetallePrimaCoberturaCotizacionDTO entity) {
    				LogDeMidasEJB3.log("updating DetallePrimaCoberturaCotizacionDTO instance", Level.INFO, null);
	        try {
            DetallePrimaCoberturaCotizacionDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DetallePrimaCoberturaCotizacionDTO findById( DetallePrimaCoberturaCotizacionId id) {
    				LogDeMidasEJB3.log("finding DetallePrimaCoberturaCotizacionDTO instance with id: " + id, Level.INFO, null);
	        try {
            DetallePrimaCoberturaCotizacionDTO instance = entityManager.find(DetallePrimaCoberturaCotizacionDTO.class, id);
            if(instance != null){
            	entityManager.refresh(instance);
            }
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DetallePrimaCoberturaCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DetallePrimaCoberturaCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<DetallePrimaCoberturaCotizacionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DetallePrimaCoberturaCotizacionDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding DetallePrimaCoberturaCotizacionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DetallePrimaCoberturaCotizacionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all DetallePrimaCoberturaCotizacionDTO entities.
	  	  @return List<DetallePrimaCoberturaCotizacionDTO> all DetallePrimaCoberturaCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DetallePrimaCoberturaCotizacionDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all DetallePrimaCoberturaCotizacionDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from DetallePrimaCoberturaCotizacionDTO model";
					Query query = entityManager.createQuery(queryString);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);			
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * 	findByCoberturaCotizacion. Encuentra la lista de entidades DetallePrimaCoberturaCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto CoberturaCotizacionId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, idToCobertura.
	 * @param CoberturaCotizacionId
	 * @return List<DetallePrimaCoberturaCotizacionDTO>
	 */
	@SuppressWarnings("unchecked")
	public List<DetallePrimaCoberturaCotizacionDTO> findByCoberturaCotizacion(CoberturaCotizacionId coberturaCotId) {
		LogDeMidasEJB3.log("Buscando DetallePrimaCoberturaCotizacionDTO de la cotizacion: "+coberturaCotId.getIdToCotizacion()+" Inciso: "+
				coberturaCotId.getNumeroInciso()+" Seccion: "+coberturaCotId.getIdToSeccion()+" Cobertura: "+coberturaCotId.getIdToCobertura(),Level.INFO, null);
		try {
			final String queryString = "select model from DetallePrimaCoberturaCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion " +
					" and model.id.numeroInciso = :numeroInciso and model.id.idToSeccion = :idToSeccion and model.id.idToCobertura = :idToCobertura ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", coberturaCotId.getIdToCotizacion());
			query.setParameter("numeroInciso", coberturaCotId.getNumeroInciso());
			query.setParameter("idToSeccion", coberturaCotId.getIdToSeccion());
			query.setParameter("idToCobertura", coberturaCotId.getIdToCobertura());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<DetallePrimaCoberturaCotizacionDTO> lista = query.getResultList();
			return lista;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	/**
	 * 	findBySubIncisoCotizacion. Encuentra la lista de entidades DetallePrimaCoberturaCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto DetallePrimaCoberturaCotizacionId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, numeroSubInciso.
	 * @param DetallePrimaCoberturaCotizacionId
	 * @return List<DetallePrimaCoberturaCotizacionDTO>
	 */
	@SuppressWarnings("unchecked")
	public List<DetallePrimaCoberturaCotizacionDTO> findBySubIncisoCotizacion(DetallePrimaCoberturaCotizacionId detallePrimaCoberturaCotizacionId) {
		LogDeMidasEJB3.log("Buscando DetallePrimaCoberturaCotizacionDTO de la cotizacion: "+detallePrimaCoberturaCotizacionId.getIdToCotizacion()+" Inciso: "+
			detallePrimaCoberturaCotizacionId.getNumeroInciso()+" Seccion: "+detallePrimaCoberturaCotizacionId.getIdToSeccion()+" SubInciso: "+detallePrimaCoberturaCotizacionId.getNumeroSubInciso(),Level.INFO, null);
		try {
			final String queryString = "select model from DetallePrimaCoberturaCotizacionDTO model where model.id.idToCotizacion = :idToCotizacion " +
					" and model.id.numeroInciso = :numeroInciso and model.id.idToSeccion = :idToSeccion and model.id.numeroSubInciso = :numeroSubInciso ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", detallePrimaCoberturaCotizacionId.getIdToCotizacion());
			query.setParameter("numeroInciso", detallePrimaCoberturaCotizacionId.getNumeroInciso());
			query.setParameter("idToSeccion", detallePrimaCoberturaCotizacionId.getIdToSeccion());
			query.setParameter("numeroSubInciso", detallePrimaCoberturaCotizacionId.getNumeroSubInciso());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	


	public Double sumPrimaNetaARDTCoberturaCotizacion(CoberturaCotizacionId coberturaCotId) {
		LogDeMidasEJB3.log("sumPrimaNetaARDTCoberturaCotizacion Cotizacion: "+coberturaCotId.getIdToCotizacion()+" Inciso: "+
				coberturaCotId.getNumeroInciso()+" Seccion: "+coberturaCotId.getIdToSeccion()+" Cobertura: "+coberturaCotId.getIdToCobertura(),Level.INFO, null);
		try {
			String queryString = "select sum(model.valorPrimaNetaARDT) from DetallePrimaCoberturaCotizacionDTO model " +
					"where model.id.idToCotizacion = :idToCotizacion ";
			if(coberturaCotId.getNumeroInciso() != null) {
				queryString += "and model.id.numeroInciso = :numeroInciso ";
			}
			queryString +=  "and model.id.idToSeccion = :idToSeccion and " +
					"model.id.idToCobertura = :idToCobertura ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", coberturaCotId.getIdToCotizacion());
			if(coberturaCotId.getNumeroInciso() != null) {
				query.setParameter("numeroInciso", coberturaCotId.getNumeroInciso());
			}
			query.setParameter("idToSeccion", coberturaCotId.getIdToSeccion());
			query.setParameter("idToCobertura", coberturaCotId.getIdToCobertura());
			Object result = query.getSingleResult();
			if(result == null) {
				return 0D;
			}
			return (Double) result;
		} catch (NoResultException nre) {
			LogDeMidasEJB3.log("sumPrimaNetaARDTCoberturaCotizacion NoResultException", Level.SEVERE, nre);
			return 0D;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("sumPrimaNetaARDTCoberturaCotizacion failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Double sumPrimaNetaCoberturaCotizacion(CoberturaCotizacionId coberturaCotId) {
		LogDeMidasEJB3.log("sumPrimaNetaCoberturaCotizacion Cotizacion: "+coberturaCotId.getIdToCotizacion()+" Inciso: "+
				coberturaCotId.getNumeroInciso()+" Seccion: "+coberturaCotId.getIdToSeccion()+" Cobertura: "+coberturaCotId.getIdToCobertura(),Level.INFO, null);
		try {
			String queryString = "select sum(model.valorPrimaNeta) from DetallePrimaCoberturaCotizacionDTO model " +
					"where model.id.idToCotizacion = :idToCotizacion " +
					" and model.id.numeroInciso = :numeroInciso " +
					"and model.id.idToSeccion = :idToSeccion and " +
					"model.id.idToCobertura = :idToCobertura ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", coberturaCotId.getIdToCotizacion());
			query.setParameter("numeroInciso", coberturaCotId.getNumeroInciso());
			query.setParameter("idToSeccion", coberturaCotId.getIdToSeccion());
			query.setParameter("idToCobertura", coberturaCotId.getIdToCobertura());
			Object result = query.getSingleResult();
			if(result == null) {
				return 0D;
			}
			return (Double) result;
		} catch (NoResultException nre) {
			LogDeMidasEJB3.log("sumPrimaNetaCoberturaCotizacion NoResultException", Level.SEVERE, nre);
			return 0D;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("sumPrimaNetaCoberturaCotizacion failed", Level.SEVERE, re);
			throw re;
		}
	}

	public void deleteDetallePrimaCoberturaCotizacion(CoberturaCotizacionId coberturaCotId) {
		LogDeMidasEJB3.log("deleteDetallePrimaCobertura: "+coberturaCotId.getIdToCotizacion()+" Inciso: "+
				coberturaCotId.getNumeroInciso()+" Seccion: "+coberturaCotId.getIdToSeccion()+" Cobertura: "+coberturaCotId.getIdToCobertura(),Level.INFO, null);
		try {
			String queryString = "delete from DetallePrimaCoberturaCotizacionDTO model " +
					"where model.id.idToCotizacion = :idToCotizacion " +
					"and model.id.numeroInciso = :numeroInciso " +
					"and model.id.idToSeccion = :idToSeccion ";
			if(coberturaCotId.getIdToCobertura() != null) {
				queryString += "and model.id.idToCobertura = :idToCobertura ";
			}
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", coberturaCotId.getIdToCotizacion());
			query.setParameter("numeroInciso", coberturaCotId.getNumeroInciso());
			query.setParameter("idToSeccion", coberturaCotId.getIdToSeccion());
			if(coberturaCotId.getIdToCobertura() != null) {
				query.setParameter("idToCobertura", coberturaCotId.getIdToCobertura());
			}
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("deleteDetallePrimaCobertura failed", Level.SEVERE, re);
			throw re;
		}
	}
	public void borrarFiltrado(DetallePrimaCoberturaCotizacionId detallePrimaCoberturaCotizacionId) {
		LogDeMidasEJB3.log("borrarFiltrado: "+detallePrimaCoberturaCotizacionId.getIdToCotizacion()+" Inciso: "+
			detallePrimaCoberturaCotizacionId.getNumeroInciso()+" Seccion: "+detallePrimaCoberturaCotizacionId.getIdToSeccion()+
			" NumSubInciso: "+detallePrimaCoberturaCotizacionId.getNumeroSubInciso()
			+" Cobertura: "+detallePrimaCoberturaCotizacionId.getIdToCobertura(),Level.INFO, null);
		try {
			String queryString = "delete from DetallePrimaCoberturaCotizacionDTO model " +
					"where model.id.idToCotizacion = :idToCotizacion " +
					"and model.id.numeroInciso = :numeroInciso ";
			if(detallePrimaCoberturaCotizacionId.getIdToSeccion()!=null){
			    queryString += "and model.id.idToSeccion = :idToSeccion ";
			}
        		if(detallePrimaCoberturaCotizacionId.getNumeroSubInciso()!=null){
        			    queryString += "and model.id.numeroSubInciso = :numSubInciso ";
        		}
        			
        		if(detallePrimaCoberturaCotizacionId.getIdToCobertura() != null) {
        				queryString += "and model.id.idToCobertura = :idToCobertura ";
        		}
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", detallePrimaCoberturaCotizacionId.getIdToCotizacion());
			query.setParameter("numeroInciso", detallePrimaCoberturaCotizacionId.getNumeroInciso());
			if(detallePrimaCoberturaCotizacionId.getIdToSeccion()!=null){
			    query.setParameter("idToSeccion", detallePrimaCoberturaCotizacionId.getIdToSeccion());
			 }
			if(detallePrimaCoberturaCotizacionId.getNumeroSubInciso()!=null){
        		    query.setParameter("numSubInciso", detallePrimaCoberturaCotizacionId.getNumeroSubInciso());
        		  }
        		if(detallePrimaCoberturaCotizacionId.getIdToCobertura() != null) {
        		    query.setParameter("idToCobertura", detallePrimaCoberturaCotizacionId.getIdToCobertura());
        		  }
			
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("borrarFiltrado failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public List<DetallePrimaCoberturaCotizacionDTO> listarPorCotizacion(BigDecimal idToCotizacion){
		return findByProperty("id.idToCotizacion", idToCotizacion);
	}
}