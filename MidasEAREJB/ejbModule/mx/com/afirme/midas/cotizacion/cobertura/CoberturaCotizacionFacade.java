package mx.com.afirme.midas.cotizacion.cobertura;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotId;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity CoberturaCotizacionDTO.
 * 
 * @see .CoberturaCotizacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CoberturaCotizacionFacade implements
		CoberturaCotizacionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved CoberturaCotizacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CoberturaCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CoberturaCotizacionDTO entity) {
		LogDeMidasEJB3.log("saving CoberturaCotizacionDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent CoberturaCotizacionDTO entity.
	 * 
	 * @param entity
	 *            CoberturaCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoberturaCotizacionDTO entity) {
		LogDeMidasEJB3.log("deleting CoberturaCotizacionDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(CoberturaCotizacionDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved CoberturaCotizacionDTO entity and return it or
	 * a copy of it to the sender. A copy of the CoberturaCotizacionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CoberturaCotizacionDTO entity to update
	 * @return CoberturaCotizacionDTO the persisted CoberturaCotizacionDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoberturaCotizacionDTO update(CoberturaCotizacionDTO entity) {
		LogDeMidasEJB3.log("updating CoberturaCotizacionDTO instance",
				Level.INFO, null);
		try {
			CoberturaCotizacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CoberturaCotizacionDTO findById(CoberturaCotizacionId id) {
		LogDeMidasEJB3.log("finding CoberturaCotizacionDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			CoberturaCotizacionDTO instance = entityManager.find(
					CoberturaCotizacionDTO.class, id);
			try{
				if (instance != null) {
					entityManager.refresh(instance);
				}
			}catch (IllegalArgumentException e){
				e.printStackTrace();
			}
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoberturaCotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoberturaCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoberturaCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding CoberturaCotizacionDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			entityManager.flush();
			final String queryString = "select model from CoberturaCotizacionDTO model where model."
					+ propertyName
					+ "= :propertyValue"
					+ " Order By model.coberturaSeccionDTO.coberturaDTO.codigo ASC";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoberturaCotizacionDTO entities.
	 * 
	 * @return List<CoberturaCotizacionDTO> all CoberturaCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all CoberturaCotizacionDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from CoberturaCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarPorSeccionCotizacionId(SeccionCotizacionDTOId seccionCotizacionDTOId,Boolean listarSoloContratadas) {
		LogDeMidasEJB3.log("finding by IncisoCotizacionId all SeccionCotizacionDTO instances",Level.INFO, null);
		try {
			if (seccionCotizacionDTOId == null || seccionCotizacionDTOId.getIdToCotizacion() == null
					|| seccionCotizacionDTOId.getNumeroInciso() == null || seccionCotizacionDTOId.getIdToSeccion() == null)
				return null;
			StringBuffer queryString = new StringBuffer();
			queryString.append("select model from CoberturaCotizacionDTO model where");
			queryString.append(" model.id.idToCotizacion =:idToCotizacion ");
			queryString.append(" and model.id.idToSeccion =:idToSeccion ");
			queryString.append(" and model.id.numeroInciso =:numeroInciso ") ;
			queryString.append((listarSoloContratadas)?" and model.seccionCotizacionDTO.claveContrato = 1 ": "");
			queryString.append((listarSoloContratadas)?" and model.claveContrato = 1 " : "" +
					"order by model.coberturaSeccionDTO.coberturaDTO.codigo");
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("idToCotizacion", seccionCotizacionDTOId
					.getIdToCotizacion());
			query.setParameter("numeroInciso", seccionCotizacionDTOId
					.getNumeroInciso());
			query.setParameter("idToSeccion", seccionCotizacionDTOId
					.getIdToSeccion());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarPorSeccionCotizacionIdSinContratar(SeccionCotizacionDTOId seccionCotizacionDTOId) {
		LogDeMidasEJB3.log("finding by IncisoCotizacionId all SeccionCotizacionDTO instances",Level.INFO, null);
		try {
			if (seccionCotizacionDTOId == null
					|| seccionCotizacionDTOId.getIdToCotizacion() == null
					|| seccionCotizacionDTOId.getNumeroInciso() == null
					|| seccionCotizacionDTOId.getIdToSeccion() == null)
				return null;

			String queryString = "select model from CoberturaCotizacionDTO model where";
			queryString += " model.id.idToCotizacion =:idToCotizacion ";
			queryString += " and model.id.idToSeccion =:idToSeccion ";
			queryString += " and model.id.numeroInciso =:numeroInciso " +
					"order by model.coberturaSeccionDTO.coberturaDTO.codigo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", seccionCotizacionDTOId
					.getIdToCotizacion());
			query.setParameter("numeroInciso", seccionCotizacionDTOId
					.getNumeroInciso());
			query.setParameter("idToSeccion", seccionCotizacionDTOId
					.getIdToSeccion());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasDeSumaAsegurada(
			CoberturaCotizacionDTO coberturaCotizacionDTO) {
		LogDeMidasEJB3
				.log(
						"finding by IncisoCotizacionId all SeccionCotizacionDTO instances",
						Level.INFO, null);
		try {
			if (coberturaCotizacionDTO == null
					|| coberturaCotizacionDTO.getId() == null)
				return null;

			String queryString = "select model from CoberturaCotizacionDTO model where";
			queryString += " model.id.idToCotizacion =:idToCotizacion ";
			queryString += " and model.id.idToSeccion =:idToSeccion ";
			queryString += " and model.id.numeroInciso =:numeroInciso ";
			queryString += " and model.id.idToCobertura IN ";
			queryString += " (select cob.idToCobertura from CoberturaDTO cob where cob.idCoberturaSumaAsegurada =:idToCobertura)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", coberturaCotizacionDTO.getId()
					.getIdToCotizacion());
			query.setParameter("numeroInciso", coberturaCotizacionDTO.getId()
					.getNumeroInciso());
			query.setParameter("idToSeccion", coberturaCotizacionDTO.getId()
					.getIdToSeccion());
			query.setParameter("idToCobertura", coberturaCotizacionDTO.getId()
					.getIdToCobertura());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasContratadasPorSeccion(
			BigDecimal idToCotizacion, BigDecimal idToSeccion) {
		LogDeMidasEJB3.log(
				"buscando CoberturaCotizacionDTO with idToCotizacion: "
						+ idToCotizacion + " and idToSeccion: " + idToSeccion,
				Level.INFO, null);
		try {
			String queryString = "select model from CoberturaCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion " +
							"and model.claveContrato = 1 and model.seccionCotizacionDTO.claveContrato = 1 " +
							"order by model.coberturaSeccionDTO.coberturaDTO.codigo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarCoberturasContratadas failed",
					Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RecargoRiesgoCotizacionDTO> listarRecargosRiesgoPorCoberturaCotizacion(
			CoberturaCotizacionId id, BigDecimal idRecargoEspecial, Boolean contratado) {
		LogDeMidasEJB3.log("finding RecargoRiesgoCotizacionDTO intances",
				Level.INFO, null);
		try {
			String queryString = "select model from RecargoRiesgoCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion "
					+ "and model.id.numeroInciso = :numeroInciso "
					+ "and model.id.idToSeccion = :idToSeccion "
					+ "and model.id.idToCobertura = :idToCobertura "
					+ (idRecargoEspecial != null ? "and model.id.idToRecargoVario = :idToRecargoVario " : "")
					+ "and model.riesgoCotizacionDTO.claveContrato = 1 ";
			if(contratado) {
				queryString += "and model.claveContrato = 1";
			}
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", id.getIdToCotizacion());
			query.setParameter("numeroInciso", id.getNumeroInciso());
			query.setParameter("idToSeccion", id.getIdToSeccion());
			query.setParameter("idToCobertura", id.getIdToCobertura());
			
			if(idRecargoEspecial != null)
				query.setParameter("idToRecargoVario", idRecargoEspecial);
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("finding failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<AumentoRiesgoCotizacionDTO> listarAumentosRiesgoPorCoberturaCotizacion(CoberturaCotizacionId id, BigDecimal idAumentoEspecial, Boolean contratado) {
		LogDeMidasEJB3.log("finding AumentoRiesgoCotizacionDTO intances",Level.INFO, null);
		try {
			String queryString = "select model from AumentoRiesgoCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion "
					+ "and model.id.numeroInciso = :numeroInciso "
					+ "and model.id.idToSeccion = :idToSeccion "
					+ "and model.id.idToCobertura = :idToCobertura "
					+ (idAumentoEspecial != null ? "and model.id.idToAumentoVario = :idToAumentoVario " : "")
					+ "and model.riesgoCotizacionDTO.claveContrato = 1 ";
			if(contratado) {
				queryString += "and model.claveContrato = 1";
			}
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", id.getIdToCotizacion());
			query.setParameter("numeroInciso", id.getNumeroInciso());
			query.setParameter("idToSeccion", id.getIdToSeccion());
			query.setParameter("idToCobertura", id.getIdToCobertura());
			
			if(idAumentoEspecial != null)
				query.setParameter("idToAumentoVario", idAumentoEspecial);
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("finding failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DescuentoRiesgoCotizacionDTO> listarDescuentosRiesgoPorCoberturaCotizacion(
			CoberturaCotizacionId id, BigDecimal idDescuentoEspecial, Boolean contratado) {
		LogDeMidasEJB3.log("finding DescuentoRiesgoCotizacionDTO intances",
				Level.INFO, null);
		try {
			String queryString = "select model from DescuentoRiesgoCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion "
					+ "and model.id.numeroInciso = :numeroInciso "
					+ "and model.id.idToSeccion = :idToSeccion "
					+ "and model.id.idToCobertura = :idToCobertura "
					+ (idDescuentoEspecial != null ? "and model.id.idToDescuentoVario = :idToDescuentoVario " :"")
					+ "and model.riesgoCotizacionDTO.claveContrato = 1 ";
			if(contratado) {
				queryString += "and model.claveContrato = 1";
			}
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", id.getIdToCotizacion());
			query.setParameter("numeroInciso", id.getNumeroInciso());
			query.setParameter("idToSeccion", id.getIdToSeccion());
			query.setParameter("idToCobertura", id.getIdToCobertura());
			
			if(idDescuentoEspecial != null)
				query.setParameter("idToDescuentoVario", idDescuentoEspecial);
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("finding failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasContratadas(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log(
				"buscando CoberturaCotizacionDTO with idToCotizacion: "
						+ idToCotizacion, Level.INFO, null);
		try {
			String queryString = "select model from CoberturaCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion and model.claveContrato = 1 and model.seccionCotizacionDTO.claveContrato = 1 " +
							"order by model.coberturaSeccionDTO.coberturaDTO.codigo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarCoberturasContratadas failed",
					Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasContratadas(BigDecimal idToCotizacion,BigDecimal numeroInciso) {
		LogDeMidasEJB3.log("buscando CoberturaCotizacionDTO con idToCotizacion: "+ idToCotizacion+", numeroInciso: "+numeroInciso, Level.INFO, null);
		try {
			String queryString = "select model from CoberturaCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion and " +
							"model.id.numeroInciso = :numeroInciso and " +
							"model.claveContrato = 1 and model.seccionCotizacionDTO.claveContrato = 1 " +
							"order by model.coberturaSeccionDTO.coberturaDTO.codigo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarCoberturasContratadas failed",
					Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasContratadas(BigDecimal idToCotizacion,boolean aplicarMerge) {

		LogDeMidasEJB3.log(
				"buscando CoberturaCotizacionDTO with idToCotizacion: "
						+ idToCotizacion, Level.INFO, null);
		try {
			String queryString = "select model from CoberturaCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion and model.claveContrato = 1 and model.seccionCotizacionDTO.claveContrato = 1 " +
							"order by model.coberturaSeccionDTO.coberturaDTO.codigo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarCoberturasContratadas failed",
					Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasContratadasPorSeccionCobertura(
			CoberturaCotizacionId id) {
		LogDeMidasEJB3.log(
				"buscando CoberturaCotizacionDTO with idToCobertura: "
						+ id.getIdToCobertura(), Level.INFO, null);
		try {
			String queryString = "select model from CoberturaCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion "
					+ "and model.id.idToCobertura = :idToCobertura and model.claveContrato = 1";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", id.getIdToCotizacion());
			query.setParameter("idToSeccion", id.getIdToSeccion());
			query.setParameter("idToCobertura", id.getIdToCobertura());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarCoberturasContratadasPorSeccion failed",
					Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasCalculadasPrimerRiesgoLUC(BigDecimal idToCotizacion, Short numeroAgrupacion) {
		LogDeMidasEJB3.log("finding by idToCotizacion,secciones  all CoberturaCotizacionDTO instances",Level.INFO, null);
		try {
			String queryString = "select model from CoberturaCotizacionDTO model where";
			queryString += " model.id.idToCotizacion = :idToCotizacion ";
			queryString += " and model.claveContrato = 1 and model.seccionCotizacionDTO.claveContrato = 1 and model.numeroAgrupacion = :numeroAgrupacion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroAgrupacion", numeroAgrupacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
		
	public AgrupacionCotDTO getAgrupacionCotizacion(AgrupacionCotId id) {
		LogDeMidasEJB3.log("finding AgrupacionCotDTO instance", Level.INFO,
				null);
		try {
			AgrupacionCotDTO instance = entityManager.find(
					AgrupacionCotDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Lista los registros de CoberturaCotizacionDTO usando como filtro los
	 * atributos del objeto CoberturaCotizacionId recibido. Los atributos usados
	 * son: idToCotizacion, idToCobertura, idToSeccion y numeroInciso.
	 * 
	 * @param CoberturaCotizacionId
	 *            id
	 * @return List<CoberturaCotizacionDTO> lista de entidades encontrada
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarPorIdFiltrado(
			CoberturaCotizacionId id) {
		LogDeMidasEJB3.log(
				"finding CoberturaCotizacionDTO instance with IdToCobertura: "
						+ id.getIdToCobertura() + "" + ", IdToCotizacion: "
						+ id.getIdToCotizacion() + ", IdToSeccion: "
						+ id.getIdToSeccion() + " and NumeroInciso:"
						+ id.getNumeroInciso(), Level.INFO, null);
		try {
			String queryString = "select model from CoberturaCotizacionDTO as model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (id == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToCotizacion", id.getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.numeroInciso", id.getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToSeccion", id.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "id.idToCobertura", id.getIdToCobertura());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"listarPorIdFiltrado CoberturaCotizacionDTO failed",
					Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * ListarFiltrado
	 * 
	 * @param CoberturaCotizacionDTO
	 * @return List<CoberturaCotizacionDTO> lista de entidades encontrada
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarFiltrado(CoberturaCotizacionDTO coberturaCotizacion) {
		LogDeMidasEJB3.log("listando Filtrado CoberturaCotizacionDTO",Level.INFO, null);
		try {
			String queryString = "select model from CoberturaCotizacionDTO as model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (coberturaCotizacion.getId() != null) {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCotizacion", coberturaCotizacion.getId().getIdToCotizacion());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.numeroInciso", coberturaCotizacion.getId().getNumeroInciso());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToSeccion", coberturaCotizacion.getId().getIdToSeccion());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCobertura", coberturaCotizacion.getId().getIdToCobertura());
			}

			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"idTcSubramo", coberturaCotizacion.getIdTcSubramo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "valorPrimaNeta", coberturaCotizacion.getValorPrimaNeta());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "valorSumaAsegurada", coberturaCotizacion.getValorSumaAsegurada());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "valorCoaseguro", coberturaCotizacion.getValorCoaseguro());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "valorDeducible", coberturaCotizacion.getValorDeducible());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "codigoUsuarioAutReaseguro", coberturaCotizacion.getCodigoUsuarioAutReaseguro());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveObligatoriedad", coberturaCotizacion.getClaveObligatoriedad());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveContrato", coberturaCotizacion.getClaveContrato());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "valorCuota", coberturaCotizacion.getValorCuota());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "porcentajeCoaseguro", coberturaCotizacion.getPorcentajeCoaseguro());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveAutCoaseguro", coberturaCotizacion.getClaveAutCoaseguro());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "codigoUsuarioAutCoaseguro", coberturaCotizacion.getCodigoUsuarioAutCoaseguro());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "porcentajeDeducible", coberturaCotizacion.getPorcentajeDeducible());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveAutDeducible", coberturaCotizacion.getClaveAutDeducible());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "codigoUsuarioAutDeducible", coberturaCotizacion.getCodigoUsuarioAutDeducible());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "numeroAgrupacion", coberturaCotizacion.getNumeroAgrupacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "valorCuotaOriginal", coberturaCotizacion.getValorCuotaOriginal());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "valorPrimaNetaOriginal", coberturaCotizacion.getValorPrimaNetaOriginal());

			if(coberturaCotizacion.getCoberturaSeccionDTO() != null && coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO() != null){
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "coberturaSeccionDTO.coberturaDTO.claveTipoSumaAsegurada", coberturaCotizacion
							.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada());
			}
			if(coberturaCotizacion.getSeccionCotizacionDTO() != null && coberturaCotizacion.getSeccionCotizacionDTO().getClaveContrato() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "seccionCotizacionDTO.claveContrato", coberturaCotizacion.getSeccionCotizacionDTO().getClaveContrato());
				}
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			queryString += " order by model.coberturaSeccionDTO.coberturaDTO.codigo";
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"listarPorIdFiltrado CoberturaCotizacionDTO failed",
					Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public Double obtenerSACoberturasBasicasIncendioPorCotizacion(BigDecimal idToCotizacion){
		LogDeMidasEJB3.log("Sumatoria de Cobnerturas BAsicas de", Level.INFO,
				null);
		try {
			String queryString = "";
			Double sumaAsegurada = 0D;
			queryString = "SELECT SUM(COB.VALORSUMAASEGURADA) SUMAASEGURADA "+
						  " FROM MIDAS.TOSECCIONCOT SEC, "+
							" MIDAS.TOCOBERTURACOT COB, "+
							" MIDAS.TOCOBERTURA C "+
						  " WHERE SEC.IDTOCOTIZACION = "+idToCotizacion+
							" AND SEC.CLAVECONTRATO = 1 "+
							" AND COB.IDTOCOTIZACION = SEC.IDTOCOTIZACION "+
							" AND COB.NUMEROINCISO = SEC.NUMEROINCISO "+
							" AND COB.IDTOSECCION = SEC.IDTOSECCION "+
							" AND COB.CLAVECONTRATO = 1 "+
							" AND C.IDTOCOBERTURA = COB.IDTOCOBERTURA "+
							" AND C.IDTCSUBRAMO = 1 "+
							" AND C.CLAVETIPOSUMAASEGURADA = 1";		
			Query query = entityManager.createNativeQuery(queryString);
			Object result = query.getSingleResult();
			if(result instanceof List)  {
				if(((List) result).get(0) != null) {
					sumaAsegurada = (Double)((BigDecimal)((List)result).get(0)).doubleValue();
				}
			} else if (result instanceof BigDecimal) {
				sumaAsegurada = (Double)((BigDecimal)result).doubleValue();
			}
			return sumaAsegurada;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Sumatoria fallo", Level.SEVERE, re);
			throw re;
		}		
	}	
	@SuppressWarnings("unchecked")
	public Double getSumatoriaRiesgosBasicos(CoberturaCotizacionId id){
		LogDeMidasEJB3.log("Sumatoria de Riesgos Basicos", Level.INFO,
				null);
		try {
			String queryString = "";
			Double sumaAsegurada = 0D;
			queryString = "select sum(t.valorsumaasegurada)SUMAASEGURADA";
			queryString += " from midas.toriesgocot t, midas.toriesgocobertura c";
			queryString += " WHERE t.idtocotizacion = "+ id.getIdToCotizacion();
			queryString += " and c.idtoriesgo = t.idtoriesgo";
			queryString += " and c.idtocobertura = t.idtocobertura";
			queryString += " and c.idtoseccion = t.idtoseccion";
			queryString += " and c.clavetiposumaasegurada = 1";
			queryString += " and t.numeroinciso = "+id.getNumeroInciso();
			queryString += " and t.idtoseccion = "+id.getIdToSeccion();
			queryString += " and t.idtocobertura = "+id.getIdToCobertura();
			queryString += " and t.claveContrato = 1";
			Query query = entityManager.createNativeQuery(queryString);
			Object result = query.getSingleResult();
			if(result instanceof List)  {
				if(((List) result).get(0) != null) {
					sumaAsegurada = (Double)((BigDecimal)((List)result).get(0)).doubleValue();
				}
			} else if (result instanceof BigDecimal) {
				sumaAsegurada = (Double)((BigDecimal)result).doubleValue();
			}
			return sumaAsegurada;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Sumatoria fallo", Level.SEVERE, re);
			throw re;
		}		
	}

	@SuppressWarnings("unchecked")
	public List<BigDecimal> listarCoberturasDistintas(BigDecimal idToCotizacion, boolean soloContratadas) {
		LogDeMidasEJB3.log("Distinct CoberturaCotizacionDTO with idToCotizacion: "+ idToCotizacion, Level.INFO, null);
		try {
			String queryString = "select distinct model.id.idToCobertura from CoberturaCotizacionDTO model " +
					"where model.id.idToCotizacion = :idToCotizacion ";
			if (soloContratadas)
				queryString += " and model.claveContrato = 1 and model.seccionCotizacionDTO.claveContrato = 1 ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Distinct CoberturaCotizacionDTO failed",
					Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * METODO PARA OBTENER LA COBERTURA , SOLAMENTE SI ESTA CONTRATADA Y SI SU SECCION IGUAL LOS ESTA
	 * @param id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public CoberturaCotizacionDTO obtenerCoberturaContratadaPorId(CoberturaCotizacionId id) {
		LogDeMidasEJB3.log(
				"buscando CoberturaCotizacionDTO with idToCobertura: "
						+ id.getIdToCobertura(), Level.INFO, null);
		try {
			String queryString = "select model from CoberturaCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion and model.id.numeroInciso=:numInciso " +
					   "and model.id.idToSeccion = :idToSeccion  and  model.seccionCotizacionDTO.claveContrato = 1 "
					+ "and model.id.idToCobertura = :idToCobertura and model.claveContrato = 1";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", id.getIdToCotizacion());
			query.setParameter("numInciso", id.getNumeroInciso());
			query.setParameter("idToSeccion", id.getIdToSeccion());
			query.setParameter("idToCobertura", id.getIdToCobertura());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<CoberturaCotizacionDTO> listaCoberturas=(List<CoberturaCotizacionDTO>)query.getResultList();
			return listaCoberturas!=null && !listaCoberturas.isEmpty()?listaCoberturas.get(0):null;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenerCoberturaContratadaPorId failed",
					Level.SEVERE, re);
			throw re;
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasContratadasApliquenPrimerRiesgoLUC(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToCobertura) {
		LogDeMidasEJB3
				.log(
						"finding by idToCotizacion,secciones  all CoberturaCotizacionDTO instances",
						Level.INFO, null);
		try {
			String queryString = "select model from CoberturaCotizacionDTO model where";
			queryString += " model.id.idToCotizacion = :idToCotizacion ";
			queryString += " and model.id.numeroInciso = :numeroInciso ";
			queryString += " and model.id.idToCobertura = :idToCobertura ";
			queryString += " and model.claveContrato = 1 and model.seccionCotizacionDTO.claveContrato = 1 ";
			queryString += " and model.numeroAgrupacion > 0";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			query.setParameter("idToCobertura", idToCobertura);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasAsociadasPorSeccionCotizacionId(SeccionCotizacionDTOId seccionCotizacionDTOId, BigDecimal idToCoberturaBasica) {
		LogDeMidasEJB3
				.log(
						"finding by IncisoCotizacionId all SeccionCotizacionDTO instances",
						Level.INFO, null);
		try {
			if (seccionCotizacionDTOId == null
					|| seccionCotizacionDTOId.getIdToCotizacion() == null
					|| seccionCotizacionDTOId.getNumeroInciso() == null
					|| seccionCotizacionDTOId.getIdToSeccion() == null || idToCoberturaBasica==null)
				return null;
			
			String queryString = "select model from CoberturaCotizacionDTO model where";
			queryString += " model.id.idToCotizacion =:idToCotizacion ";
			queryString += " and model.id.idToSeccion =:idToSeccion ";
			queryString += " and model.id.numeroInciso =:numeroInciso " ;
			queryString += " and model.seccionCotizacionDTO.claveContrato = 1 " ;
			queryString += " and model.claveContrato = 1 and model.coberturaSeccionDTO.coberturaDTO.idCoberturaSumaAsegurada=:idToCoberturaBasica " +
					"order by model.coberturaSeccionDTO.coberturaDTO.codigo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", seccionCotizacionDTOId
					.getIdToCotizacion());
			query.setParameter("numeroInciso", seccionCotizacionDTOId
					.getNumeroInciso());
			query.setParameter("idToSeccion", seccionCotizacionDTOId
					.getIdToSeccion());
			query.setParameter("idToCoberturaBasica", idToCoberturaBasica);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Map<BigDecimal,BigDecimal> obtenerCoberturasPrimerRiesgo(BigDecimal idToCotizacion){
		LogDeMidasEJB3.log("Buscando coberturas a primer riesgo de la cotizacion: " + idToCotizacion, Level.INFO,null);
		try {
			String queryString = "";
			queryString = "SELECT cobCot.idToCobertura, SUM(cobCot.valorSumaAsegurada) valorSumaAsegurada ";
			queryString += "FROM MIDAS.ToCoberturaCot cobCot, MIDAS.ToSeccionCot secc, MIDAS.ToCobertura cob, MIDAS.ToAgrupacionCot agrup ";
			queryString += "WHERE cobCot.idToCotizacion = "+idToCotizacion+" AND ";
			queryString += "cobCot.claveContrato = 1                         AND ";
			queryString += "secc.idToCotizacion =  cobCot.idToCotizacion     AND ";
			queryString += "secc.numeroInciso = cobCot.numeroInciso          AND ";
			queryString += "secc.idToSeccion = cobCot.idToSeccion            AND ";
			queryString += "secc.claveContrato = 1                           AND ";
			queryString += "cob.idToCobertura = cobCot.idToCobertura         AND ";
			queryString += "cob.claveTipoSumaAsegurada = 1                   AND ";
			queryString += "agrup.idToCotizacion = cobCot.idToCotizacion     AND " +
						   "agrup.numeroAgrupacion = cobCot.numeroAgrupacion AND " +
						   "agrup.claveTipoAgrupacion = 1 " +
						   "GROUP BY cobCot.idToCobertura" ;
			
			Query query = entityManager.createNativeQuery(queryString);
			Object result = query.getResultList();
			Map<BigDecimal,BigDecimal> resultados = new HashMap<BigDecimal,BigDecimal>();
			BigDecimal idToCobertura = null, sumaAsegurada = null;
			if(result instanceof List)  {
				List<Object> listaResultados = (List<Object>) result;
				for(Object object : listaResultados){
					Object[] singleResult = (Object[]) object;
					if(singleResult[0] instanceof BigDecimal)
						idToCobertura = (BigDecimal)singleResult[0];
					else if (singleResult[0] instanceof Long)
						idToCobertura = BigDecimal.valueOf((Long)singleResult[0]);
					else if (singleResult[0] instanceof Double)
						idToCobertura = BigDecimal.valueOf((Double)singleResult[0]);
					if(singleResult[1] instanceof BigDecimal)
						sumaAsegurada = (BigDecimal)singleResult[1];
					else if (singleResult[1] instanceof Long)
						sumaAsegurada = BigDecimal.valueOf((Long)singleResult[1]);
					else if (singleResult[1] instanceof Double)
						sumaAsegurada = BigDecimal.valueOf((Double)singleResult[1]);
					resultados.put(idToCobertura, sumaAsegurada);
				}
			}
			return resultados;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Sumatoria fallo", Level.SEVERE, re);
			throw re;
		}		
	}


	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasPrimerRiesgoSeccion(
			BigDecimal idToCotizacion, BigDecimal idToSeccion) {
		LogDeMidasEJB3.log(
				"buscando CoberturaCotizacionDTO with idToCotizacion: "
						+ idToCotizacion + " and idToSeccion: " + idToSeccion,
				Level.INFO, null);
		try {
			String queryString = "select model from CoberturaCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion and model.id.idToSeccion = :idToSeccion " +
							"and model.claveContrato = 1 and model.seccionCotizacionDTO.claveContrato = 1 " +
							"and model.coberturaSeccionDTO.coberturaDTO.clavePrimerRiesgo = 1" +
							"order by model.coberturaSeccionDTO.coberturaDTO.codigo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarCoberturasContratadas failed",
					Level.SEVERE, re);
			throw re;
		}
	}

	public Long obtenerTotalCoberturasContratadasPorSeccionCotizacion(
			SeccionCotizacionDTOId seccionCotizacionDTOId) {
		LogDeMidasEJB3.log(
				"Entro a CoberturaCotizacionFacade.obtenerTotalCoberturasContratadasPorSeccionCotizacion",
				Level.INFO, null);
		try {
			if (seccionCotizacionDTOId == null
					|| seccionCotizacionDTOId.getIdToCotizacion() == null
					|| seccionCotizacionDTOId.getNumeroInciso() == null
					|| seccionCotizacionDTOId.getIdToSeccion() == null)
				return null;
			
			String queryString = "select count(model) from CoberturaCotizacionDTO model where";
			queryString += " model.id.idToCotizacion =:idToCotizacion ";
			queryString += " and model.id.idToSeccion =:idToSeccion ";
			queryString += " and model.id.numeroInciso =:numeroInciso " ;
			queryString += " and model.seccionCotizacionDTO.claveContrato = 1 " ;
			queryString += " and model.claveContrato = 1";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", seccionCotizacionDTOId
					.getIdToCotizacion());
			query.setParameter("numeroInciso", seccionCotizacionDTOId
					.getNumeroInciso());
			query.setParameter("idToSeccion", seccionCotizacionDTOId
					.getIdToSeccion());
			return (Long) query.getSingleResult();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("CoberturaCotizacionFacade.obtenerTotalCoberturasContratadasPorSeccionCotizacion fallo",
					Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasContratadasPorSeccionCotizacion(
			SeccionCotizacionDTOId seccionCotizacionDTOId) {
		LogDeMidasEJB3
		.log(
				"Entro a CoberturaCotizacionFacade.listarCoberturasContratadasPorSeccionCotizacion",
				Level.INFO, null);
		try {
			if (seccionCotizacionDTOId == null
					|| seccionCotizacionDTOId.getIdToCotizacion() == null
					|| seccionCotizacionDTOId.getNumeroInciso() == null
					|| seccionCotizacionDTOId.getIdToSeccion() == null)
				return null;
			
			String queryString = "select model from CoberturaCotizacionDTO model where";
			queryString += " model.id.idToCotizacion =:idToCotizacion ";
			queryString += " and model.id.idToSeccion =:idToSeccion ";
			queryString += " and model.id.numeroInciso =:numeroInciso " ;
			queryString += " and model.seccionCotizacionDTO.claveContrato = 1 " ;
			queryString += " and model.claveContrato = 1 " +
					"order by model.coberturaSeccionDTO.coberturaDTO.codigo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", seccionCotizacionDTOId
					.getIdToCotizacion());
			query.setParameter("numeroInciso", seccionCotizacionDTOId
					.getNumeroInciso());
			query.setParameter("idToSeccion", seccionCotizacionDTOId
					.getIdToSeccion());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("CoberturaCotizacionFacade.listarCoberturasContratadasPorSeccionCotizacion fallo", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasBasicasContratadas(BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		LogDeMidasEJB3.log("buscando CoberturaCotizacionDTO with idToCotizacion: "
								+ idToCotizacion + " and numeroInciso: "
								+ numeroInciso, Level.INFO, null);
		try {
			String queryString = "select model from CoberturaCotizacionDTO model "
					+ "where model.id.idToCotizacion = :idToCotizacion "
					+ "and model.id.numeroInciso = :numeroInciso "
					+ "and model.claveContrato = 1 "
					+ "and model.seccionCotizacionDTO.claveContrato = 1 "
					+ "and model.coberturaSeccionDTO.coberturaDTO.claveTipoSumaAsegurada = 1"
					+ "order by model.coberturaSeccionDTO.seccionDTO.numeroSecuencia, model.coberturaSeccionDTO.coberturaDTO.codigo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarCoberturasContratadas failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * METODO OBTENER LA LISTA DE SUBRAMOS INVOLUCRADOS EN EL ENDOSO. SE INCLUYEN SUBRAMOS DE COBERTURAS CONTRATADAS Y DADAS DE BAJA EN EL ENDOSO.
	 * @param idToCotizacion
	 * @return List<SubRamoDTO>
	 */
	@SuppressWarnings("unchecked")
	public List<SubRamoDTO> listarSubRamosEndoso(BigDecimal idToCotizacion){
		LogDeMidasEJB3.log("Buscando subramos contratados y dados de baja en cotizaci�n de endososo - idToCotizacion: "
				+ idToCotizacion, Level.INFO, null);

		try {			
			String queryString = "select model "+
								 "from SubRamoDTO model "+
								 "where model.idTcSubRamo in "+
								 "(  select distinct cob.idTcSubramo "+
								    "from CoberturaCotizacionDTO cob "+
								    "where cob.id.idToCotizacion = :idToCotizacion  "+
								      "and ((      cob.claveContrato = 1 "+ 
								              "and cob.seccionCotizacionDTO.claveContrato = 1 "+
								            ") "+
								            "or "+
								            "(      cob.id.idToCobertura in (select distinct mov.idToCobertura "+
								                							"from MovimientoCotizacionEndosoDTO mov "+
								                							"where mov.idToCotizacion = :idToCotizacion "+
								                							"and mov.claveTipoMovimiento = 2) "+
								             ") "+
							               ") "+
							     ") "+
							     "order by model.idTcSubRamo";
														 
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarSubRamosEndoso failed", Level.SEVERE, re);
			throw re;
		}				
	}
	
	/**
	 * Lista las coberturas contratadas en una secci�n y cuyo id se encuentre dentro de la lista de id's enviada como par�metro.
	 * @param seccion
	 * @param idsCoberturas
	 * @return List<CoberturaCotizacionDTO>
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasContratadasEnConjunto(SeccionCotizacionDTOId seccion, List<BigDecimal> idsCoberturas){
		LogDeMidasEJB3.log("listarCoberturasContratadasEnConjunto - idToCotizacion: "
				+ seccion.getIdToCotizacion(), Level.INFO, null);
		try {			
			String queryString = "select cob "+
								 "from CoberturaCotizacionDTO cob "+
								 "where cob.id.idToCotizacion = :idToCotizacion " +
								 "  and cob.id.numeroInciso = :numeroInciso " +
								 "  and cob.id.idToSeccion = :idToSeccion " +
								 "  and cob.claveContrato = 1 " +
								 "  and cob.id.idToCobertura in (" + obtenerIds(idsCoberturas) +
								 ") "+
							     "order by cob.id.idToCobertura";
														 
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", seccion.getIdToCotizacion());
			query.setParameter("numeroInciso", seccion.getNumeroInciso());
			query.setParameter("idToSeccion", seccion.getIdToSeccion());			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� listarCoberturasContratadasEnConjunto - idToCotizacion: "
				+ seccion.getIdToCotizacion(), Level.SEVERE, re);
			throw re;
		}				
	}
	
	public String obtenerIds(List<BigDecimal> idsCoberturas) {
		return StringUtils.join(idsCoberturas.iterator(), ",");
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaCotizacionDTO> listarCoberturasObligatorioParcial(
			BigDecimal idCotizacion, BigDecimal idSeccion,
			BigDecimal numeroInciso) {
		try {
			String sql = "select model from CoberturaCotizacionDTO model " +
				"where model.id.idToCotizacion = :idCotizacion " +
				 " and model.id.numeroInciso = :numeroInciso " +
				 " and model.id.idToSeccion = :idSeccion " +
				 " and model.coberturaSeccionDTO.claveObligatoriedad = :claveObligatoriedad";
			Query query = entityManager.createQuery(sql)
					.setParameter("idCotizacion", idCotizacion)
					.setParameter("numeroInciso", numeroInciso)
					.setParameter("idSeccion", idSeccion)
					.setParameter("claveObligatoriedad", new BigDecimal("2"));
			return (List<CoberturaCotizacionDTO>) query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	
	/**
	 * Recalcula la cobertura recibida, sin importar si fu� contratada, descontratada o si hubo cambio en SA. 
	 */
	public CoberturaCotizacionDTO actualizarCoberturaEnCotizacion(CoberturaCotizacionDTO coberturaCotizacion, String nombreUsuario) {
		CoberturaCotizacionDTO coberturaCotizacionRespuesta = null;
		try {
			Boolean coberturaContratada = coberturaCotizacion.getClaveContrato().shortValue() == 1? true : false;
			
			coberturaCotizacionRespuesta = calculoCotizacionFacade.calcularCoberturaCasa(coberturaCotizacion, 
					nombreUsuario, coberturaContratada);
			
			return coberturaCotizacionRespuesta;
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@EJB
	private CalculoCotizacionFacadeRemote calculoCotizacionFacade;
	
	
	
}