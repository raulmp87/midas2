package mx.com.afirme.midas.cotizacion.inspeccion.parametros;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity GrupoParametroGeneralDTO.
 * @see .GrupoParametroGeneralDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class GrupoParametroGeneralFacade  implements GrupoParametroGeneralFacadeRemote {


    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved GrupoParametroGeneralDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity GrupoParametroGeneralDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(GrupoParametroGeneralDTO entity) {
    				LogDeMidasEJB3.log("saving GrupoParametroGeneralDTO instance", Level.INFO, null);
	        try {
	        	entityManager.persist(entity);
            	LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent GrupoParametroGeneralDTO entity.
	  @param entity GrupoParametroGeneralDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(GrupoParametroGeneralDTO entity) {
    		LogDeMidasEJB3.log("deleting GrupoParametroGeneralDTO instance", Level.INFO, null);
	        try {
	        	entity = entityManager.getReference(GrupoParametroGeneralDTO.class, entity.getIdToGrupoParametroGeneral());
	        	entityManager.remove(entity);
            	LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        		LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
	        }
    }
    
    /**
	 Persist a previously saved GrupoParametroGeneralDTO entity and return it or a copy of it to the sender. 
	 A copy of the GrupoParametroGeneralDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity GrupoParametroGeneralDTO entity to update
	 @return GrupoParametroGeneralDTO the persisted GrupoParametroGeneralDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public GrupoParametroGeneralDTO update(GrupoParametroGeneralDTO entity) {
    		LogDeMidasEJB3.log("updating GrupoParametroGeneralDTO instance", Level.INFO, null);
	        try {
	            GrupoParametroGeneralDTO result = entityManager.merge(entity);
	            LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        		LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public GrupoParametroGeneralDTO findById(BigDecimal id) {
    		LogDeMidasEJB3.log("finding GrupoParametroGeneralDTO instance with id: " + id, Level.INFO, null);
	        try {
	            GrupoParametroGeneralDTO instance = entityManager.find(GrupoParametroGeneralDTO.class, id);
	            return instance;
        } catch (RuntimeException re) {
        		LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all GrupoParametroGeneralDTO entities with a specific property value.  
	 
	  @param propertyName the name of the GrupoParametroGeneralDTO property to query
	  @param value the property value to match
	  	  @return List<GrupoParametroGeneralDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<GrupoParametroGeneralDTO> findByProperty(String propertyName, final Object value) {
    				LogDeMidasEJB3.log("finding GrupoParametroGeneralDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from GrupoParametroGeneralDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all GrupoParametroGeneralDTO entities.
	  	  @return List<GrupoParametroGeneralDTO> all GrupoParametroGeneralDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<GrupoParametroGeneralDTO> findAll() {
				LogDeMidasEJB3.log("finding all GrupoParametroGeneralDTO instances", Level.INFO, null);
			try {
				final String queryString = "select model from GrupoParametroGeneralDTO model";
				Query query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				return query.getResultList();
		} catch (RuntimeException re) {
				LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}