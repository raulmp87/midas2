package mx.com.afirme.midas.cotizacion.inspeccion.documento;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity DocumentoAnexoInspeccionIncisoCotizacionDTO.
 * @see .DocumentoAnexoInspeccionIncisoCotizacionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DocumentoAnexoInspeccionIncisoCotizacionFacade  implements DocumentoAnexoInspeccionIncisoCotizacionFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DocumentoAnexoInspeccionIncisoCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocumentoAnexoInspeccionIncisoCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public DocumentoAnexoInspeccionIncisoCotizacionDTO save(DocumentoAnexoInspeccionIncisoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("saving DocumentoAnexoInspeccionIncisoCotizacionDTO instance", Level.INFO, null);
	        try {
	        		entityManager.persist(entity);
            		LogDeMidasEJB3.log("save successful", Level.INFO, null);
            		return entity;
	        } catch (RuntimeException re) {
        			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DocumentoAnexoInspeccionIncisoCotizacionDTO entity.
	  @param entity DocumentoAnexoInspeccionIncisoCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocumentoAnexoInspeccionIncisoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("deleting DocumentoAnexoInspeccionIncisoCotizacionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DocumentoAnexoInspeccionIncisoCotizacionDTO.class, entity.getIdToDocumentoAnexoInspeccionIncisoCotizacion());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DocumentoAnexoInspeccionIncisoCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocumentoAnexoInspeccionIncisoCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocumentoAnexoInspeccionIncisoCotizacionDTO entity to update
	 @return DocumentoAnexoInspeccionIncisoCotizacionDTO the persisted DocumentoAnexoInspeccionIncisoCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DocumentoAnexoInspeccionIncisoCotizacionDTO update(DocumentoAnexoInspeccionIncisoCotizacionDTO entity) {
    				LogDeMidasEJB3.log("updating DocumentoAnexoInspeccionIncisoCotizacionDTO instance", Level.INFO, null);
	        try {
            DocumentoAnexoInspeccionIncisoCotizacionDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DocumentoAnexoInspeccionIncisoCotizacionDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding DocumentoAnexoInspeccionIncisoCotizacionDTO instance with id: " + id, Level.INFO, null);
	        try {
            DocumentoAnexoInspeccionIncisoCotizacionDTO instance = entityManager.find(DocumentoAnexoInspeccionIncisoCotizacionDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DocumentoAnexoInspeccionIncisoCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocumentoAnexoInspeccionIncisoCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<DocumentoAnexoInspeccionIncisoCotizacionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DocumentoAnexoInspeccionIncisoCotizacionDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding DocumentoAnexoInspeccionIncisoCotizacionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DocumentoAnexoInspeccionIncisoCotizacionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all DocumentoAnexoInspeccionIncisoCotizacionDTO entities.
	  	  @return List<DocumentoAnexoInspeccionIncisoCotizacionDTO> all DocumentoAnexoInspeccionIncisoCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DocumentoAnexoInspeccionIncisoCotizacionDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all DocumentoAnexoInspeccionIncisoCotizacionDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from DocumentoAnexoInspeccionIncisoCotizacionDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}