package mx.com.afirme.midas.cotizacion.inspeccion.proveedor;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity ProveedorInspeccionDTO.
 * @see .ProveedorInspeccionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ProveedorInspeccionFacade  implements ProveedorInspeccionFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ProveedorInspeccionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ProveedorInspeccionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public ProveedorInspeccionDTO save(ProveedorInspeccionDTO entity) {
    				LogDeMidasEJB3.log("saving ProveedorInspeccionDTO instance", Level.INFO, null);
	        try {
	        	entityManager.persist(entity);
	        	LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        	return entity;
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ProveedorInspeccionDTO entity.
	  @param entity ProveedorInspeccionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ProveedorInspeccionDTO entity) {
    				LogDeMidasEJB3.log("deleting ProveedorInspeccionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ProveedorInspeccionDTO.class, entity.getIdToProveedorInspeccion());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ProveedorInspeccionDTO entity and return it or a copy of it to the sender. 
	 A copy of the ProveedorInspeccionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ProveedorInspeccionDTO entity to update
	 @return ProveedorInspeccionDTO the persisted ProveedorInspeccionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ProveedorInspeccionDTO update(ProveedorInspeccionDTO entity) {
    				LogDeMidasEJB3.log("updating ProveedorInspeccionDTO instance", Level.INFO, null);
	        try {
            ProveedorInspeccionDTO result = entityManager.merge(entity);
            entityManager.flush();
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ProveedorInspeccionDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding ProveedorInspeccionDTO instance with id: " + id, Level.INFO, null);
	        try {
            ProveedorInspeccionDTO instance = entityManager.find(ProveedorInspeccionDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ProveedorInspeccionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ProveedorInspeccionDTO property to query
	  @param value the property value to match
	  	  @return List<ProveedorInspeccionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ProveedorInspeccionDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ProveedorInspeccionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ProveedorInspeccionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ProveedorInspeccionDTO entities.
	  	  @return List<ProveedorInspeccionDTO> all ProveedorInspeccionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ProveedorInspeccionDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ProveedorInspeccionDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ProveedorInspeccionDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	public ProveedorInspeccionDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public ProveedorInspeccionDTO findById(double id) {
		return null;
	}

	public List<ProveedorInspeccionDTO> listRelated(Object id) {
		return findAll();
	}
	
}