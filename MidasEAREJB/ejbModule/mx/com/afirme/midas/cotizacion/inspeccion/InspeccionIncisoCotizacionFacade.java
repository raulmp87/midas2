package mx.com.afirme.midas.cotizacion.inspeccion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity InspeccionIncisoCotizacionDTO.
 * 
 * @see mx.com.afirme.midas.cotizacion.inspeccion.InspeccionIncisoCotizacionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class InspeccionIncisoCotizacionFacade implements InspeccionIncisoCotizacionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved InspeccionIncisoCotizacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            InspeccionIncisoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public InspeccionIncisoCotizacionDTO save(InspeccionIncisoCotizacionDTO entity) {
		LogDeMidasEJB3.log("saving Toinspeccionincisocot instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent InspeccionIncisoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            InspeccionIncisoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(InspeccionIncisoCotizacionDTO entity) {
		LogDeMidasEJB3
				.log("deleting Toinspeccionincisocot instance", Level.INFO,
						null);
		try {
			entity = entityManager.getReference(InspeccionIncisoCotizacionDTO.class,
					entity.getIdToInspeccionIncisoCotizacion());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved InspeccionIncisoCotizacionDTO entity and return it or
	 * a copy of it to the sender. A copy of the InspeccionIncisoCotizacionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            InspeccionIncisoCotizacionDTO entity to update
	 * @return InspeccionIncisoCotizacionDTO the persisted InspeccionIncisoCotizacionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public InspeccionIncisoCotizacionDTO update(InspeccionIncisoCotizacionDTO entity) {
		LogDeMidasEJB3.log("updating Toinspeccionincisocot instance", Level.INFO, null);
		try {
			InspeccionIncisoCotizacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public InspeccionIncisoCotizacionDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding Toinspeccionincisocot instance with id: " + id,	Level.INFO, null);
		try {
			InspeccionIncisoCotizacionDTO instance = entityManager.find(InspeccionIncisoCotizacionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all InspeccionIncisoCotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the InspeccionIncisoCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<InspeccionIncisoCotizacionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<InspeccionIncisoCotizacionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding InspeccionIncisoCotizacionDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from InspeccionIncisoCotizacionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all InspeccionIncisoCotizacionDTO entities.
	 * 
	 * @return List<InspeccionIncisoCotizacionDTO> all InspeccionIncisoCotizacionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<InspeccionIncisoCotizacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all InspeccionIncisoCotizacionDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from InspeccionIncisoCotizacionDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}