package mx.com.afirme.midas.cotizacion.inspeccion.parametros;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity ParametroGeneralDTO.
 * @see .ParametroGeneralDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ParametroGeneralFacade  implements ParametroGeneralFacadeRemote {


    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ParametroGeneralDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ParametroGeneralDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ParametroGeneralDTO entity) {
    		LogDeMidasEJB3.log("saving ParametroGeneralDTO instance", Level.INFO, null);
	        try {
	        	entityManager.persist(entity);
            	LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        		LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ParametroGeneralDTO entity.
	  @param entity ParametroGeneralDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ParametroGeneralDTO entity) {
    		LogDeMidasEJB3.log("deleting ParametroGeneralDTO instance", Level.INFO, null);
	        try {
	        	entity = entityManager.getReference(ParametroGeneralDTO.class, entity.getId());
	            entityManager.remove(entity);
            	LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        		LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
	        }
    }
    
    /**
	 Persist a previously saved ParametroGeneralDTO entity and return it or a copy of it to the sender. 
	 A copy of the ParametroGeneralDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ParametroGeneralDTO entity to update
	 @return ParametroGeneralDTO the persisted ParametroGeneralDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ParametroGeneralDTO update(ParametroGeneralDTO entity) {
    		LogDeMidasEJB3.log("updating ParametroGeneralDTO instance", Level.INFO, null);
	        try {
	        	ParametroGeneralDTO result = entityManager.merge(entity);
            	LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
	        } catch (RuntimeException re) {
        		LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
	        }
    }
    
    public ParametroGeneralDTO findById( ParametroGeneralId id) {
	        try {
	        	ParametroGeneralDTO instance = entityManager.find(ParametroGeneralDTO.class, id);
	        	return instance;
        } catch (RuntimeException re) {
        		LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    
    public ParametroGeneralDTO findById(ParametroGeneralId id, boolean refresh) {
    	ParametroGeneralDTO instance = entityManager.find(ParametroGeneralDTO.class, id);
    	if (refresh) {
    		entityManager.refresh(instance);
    	}
    	return instance;
    }
/**
	 * Find all ParametroGeneralDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ParametroGeneralDTO property to query
	  @param value the property value to match
	  	  @return List<ParametroGeneralDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ParametroGeneralDTO> findByProperty(String propertyName, final Object value
        ) {
    		LogDeMidasEJB3.log("finding ParametroGeneralDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
				final String queryString = "select model from ParametroGeneralDTO model where model." 
			 	+ propertyName + "= :propertyValue";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("propertyValue", value);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				return query.getResultList();
			} catch (RuntimeException re) {
				LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ParametroGeneralDTO entities.
	  	  @return List<ParametroGeneralDTO> all ParametroGeneralDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ParametroGeneralDTO> findAll(
		) {
			LogDeMidasEJB3.log("finding all ParametroGeneralDTO instances", Level.INFO, null);
			try {
				final String queryString = "select model from ParametroGeneralDTO model";
				Query query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ParametroGeneralDTO> listarFiltrado(ParametroGeneralDTO entity){
		entityManager.flush();
		String queryString = "select model from ParametroGeneralDTO AS model ";
		String sWhere = "";
		Query query;
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		if (entity == null)
			return null;
		if (entity.getId() == null)
			entity.setId(new ParametroGeneralId());
		if (entity.getGrupoParametroGeneral() == null)
			entity.setGrupoParametroGeneral(new GrupoParametroGeneralDTO());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.idToGrupoParametroGeneral", entity.getId().getIdToGrupoParametroGeneral());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "id.codigoParametroGeneral", entity.getId().getCodigoParametroGeneral());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "grupoParametroGeneral.idToGrupoParametroGeneral", entity.getGrupoParametroGeneral().getIdToGrupoParametroGeneral());
		sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "grupoParametroGeneral.descripcionGrupoParametroGral", entity.getGrupoParametroGeneral().getDescripcionGrupoParametroGral());
		sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionParametroGeneral", entity.getDescripcionParametroGeneral());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveTipoValor", entity.getClaveTipoValor());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "valor", entity.getValor());
		
		if (Utilerias.esAtributoQueryValido(sWhere))
			queryString = queryString.concat(" where ").concat(sWhere);
		query = entityManager.createQuery(queryString);
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
		
	public String getValueByCode (BigDecimal code) {
		
		List<ParametroGeneralDTO> parameterList =  this.findByProperty("id.codigoParametroGeneral", code);
		
		if (parameterList != null && parameterList.size() > 0) {
			
			return parameterList.get(0).getValor();
			
		}
		
		return null;
				
	}
	
	
	
}