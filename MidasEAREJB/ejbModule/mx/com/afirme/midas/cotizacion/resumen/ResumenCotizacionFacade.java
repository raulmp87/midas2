package mx.com.afirme.midas.cotizacion.resumen;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVAFacadeRemote;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoFacadeRemote;
import mx.com.afirme.midas.consultas.formapago.FormaPagoInterfazServiciosRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionId;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotFacadeRemote;
import mx.com.afirme.midas.danios.reportes.bean.pago.EsquemaPagosDTO;
import mx.com.afirme.midas.danios.reportes.bean.pago.ReciboDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.endoso.EndosoInterfazServiciosRemote;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;

@Stateless
public class ResumenCotizacionFacade implements ResumenCotizacionFacadeRemote{

	@EJB
	private MovimientoCotizacionEndosoFacadeRemote movimientoCotEndFacade;
	
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	
	@EJB
	private AgrupacionCotFacadeRemote agrupacionCotFacade;
	
	@EJB
	private CotizacionFacadeRemote cotizacionFacade;
	
	@EJB
	private CodigoPostalIVAFacadeRemote codigoPostalIVAFacade;
	
	@EJB
	private CoberturaCotizacionFacadeRemote coberturaCotizacionFacade;
	
	@EJB
	private ComisionCotizacionFacadeRemote comisionCotizacionFacade;
	
	@EJB
	private SubRamoFacadeRemote subRamoFacade;
	
	@EJB
	private EndosoFacadeRemote endosoFacade;
	
	@EJB
	private EndosoInterfazServiciosRemote endosoInterfazServicios;
	
	@EJB
	private PolizaFacadeRemote polizaFacade;
	
	@EJB
	private FormaPagoInterfazServiciosRemote formaPagoInterfazFacade;
	
	public CotizacionDTO calcularTotalesResumenCotizacion(CotizacionDTO cotizacionDTO, List<CoberturaCotizacionDTO> coberturas) throws SystemException {
		BigDecimal factor = Utilerias.getFactorVigencia(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
		double primaNetaCotizacion = 0D;
		double pctPagoFraccionado = 0D;
		double derechosPoliza = 0D;
		double factorIVA = 0D;
		double montoIVA = 0D;
		double valorBonifComision = 0D;
		double valorComision = 0D;
		double valorComisionRPF = 0D;

		List<MovimientoCotizacionEndosoDTO> movimientos = new ArrayList<MovimientoCotizacionEndosoDTO>();
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null
				&& cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()
						.intValue() > 0
				&& cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()
						.intValue() != ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO) {
			factor = Utilerias.getFactorVigencia(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
			
			MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
			dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());

			movimientos = movimientoCotEndFacade.listarFiltrado(dto);

			for (MovimientoCotizacionEndosoDTO movimiento : movimientos) {
				if (movimiento.getIdToCobertura() != null && movimiento.getIdToCobertura().intValue() >0)
					primaNetaCotizacion += movimiento.getValorDiferenciaPrimaNeta().doubleValue();
			}			
			cotizacionDTO.setPrimaNetaAnual(primaNetaCotizacion);
			primaNetaCotizacion = cotizacionDTO.getPrimaNetaAnual() * factor.doubleValue();
			cotizacionDTO.setDiasPorDevengar(Utilerias.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
		}else{
			if (cotizacionDTO.getPrimaNetaCotizacion()!= null){
				primaNetaCotizacion = cotizacionDTO.getPrimaNetaAnual();
			}else{
				
				if(coberturas == null || coberturas.isEmpty()){
					coberturas = coberturaCotizacionFacade.listarCoberturasContratadas(cotizacionDTO.getIdToCotizacion());
				}
				
				for(CoberturaCotizacionDTO cobertura:coberturas){
					primaNetaCotizacion +=cobertura.getValorPrimaNeta();
				}
				cotizacionDTO.setPrimaNetaAnual(primaNetaCotizacion);
				primaNetaCotizacion = cotizacionDTO.getPrimaNetaAnual();
			}
			if(cotizacionDTO.getTipoPolizaDTO().getProductoDTO() != null && 
					cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getClaveAjusteVigencia().intValue() == 0){
				primaNetaCotizacion = cotizacionDTO.getPrimaNetaAnual();
			}else{
				primaNetaCotizacion = cotizacionDTO.getPrimaNetaAnual() * factor.doubleValue();
			}	
			cotizacionDTO.setDiasPorDevengar(Utilerias.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
		}
	
		
		List<SoporteResumen> resumenComisionesTmp = new ArrayList<SoporteResumen>();
		//Prima neta de la cotizacion
		cotizacionDTO.setPrimaNetaCotizacion(primaNetaCotizacion);

		//Obtiene el limite de SA
		List<ParametroGeneralDTO> parametrosLimiteSA = parametroGeneralFacade.findByProperty("id.codigoParametroGeneral",BigDecimal.valueOf(30010D));
				
		double limiteSA = Double.valueOf(parametrosLimiteSA.get(0).getValor());
		
		//Obtiene si existe una agrupacion a Primer riesgo
		AgrupacionCotDTO agrupacion = agrupacionCotFacade.buscarPorCotizacion(cotizacionDTO.getIdToCotizacion(),ConstantesCotizacion.TIPO_PRIMER_RIESGO);	
		
		//Obtiene % por forma de Pago		
		if(cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario()==null || cotizacionDTO.getValorRecargoPagoFraccionadoUsuario() == null){		
			pctPagoFraccionado = cotizacionFacade.obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacionDTO);						
			cotizacionFacade.modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), ConstantesCotizacion.CLAVE_RPF_SISTEMA, pctPagoFraccionado, false, false);			
		}else{
			pctPagoFraccionado = cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario();
		}
		
		cotizacionDTO.setPorcentajePagoFraccionado(pctPagoFraccionado);
		
		//Se obtienen los gastos de expedicion		
		if(cotizacionDTO.getValorDerechosUsuario()==null){
			derechosPoliza = cotizacionFacade.calcularDerechosCotizacion(cotizacionDTO, false);
			cotizacionFacade.modificarDerechos(cotizacionDTO.getIdToCotizacion(), ConstantesCotizacion.CLAVE_DERECHOS_SISTEMA, derechosPoliza, false, false);
		}else{
			derechosPoliza = cotizacionDTO.getValorDerechosUsuario();
		}
										
		cotizacionDTO.setDerechosPoliza(derechosPoliza);
		
		//Se obtiene % de IVA
		Double ivaObtenido= codigoPostalIVAFacade.getIVAPorIdCotizacion(cotizacionDTO.getIdToCotizacion(), "");
		
		if(ivaObtenido==null){
		    throw new SystemException("No se pudo obtener el valor para el IVA.");
		}
		factorIVA = ivaObtenido/100D;
		cotizacionDTO.setFactorIVA(factorIVA * 100D);

		//se obtiene correctamente el monto de bonificacion de la comision
		
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0){
			coberturas= new ArrayList<CoberturaCotizacionDTO>();
			for(MovimientoCotizacionEndosoDTO movimiento: movimientos){
				if(movimiento.getIdToCobertura() != null && movimiento.getIdToCobertura().intValue() > 0){
					CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
					CoberturaCotizacionId id = new CoberturaCotizacionId();
					id.setIdToCotizacion(movimiento.getIdToCotizacion());
					id.setNumeroInciso(movimiento.getNumeroInciso());
					id.setIdToSeccion(movimiento.getIdToSeccion());
					id.setIdToCobertura(movimiento.getIdToCobertura());
					coberturaCotizacionDTO.setId(id);
					coberturaCotizacionDTO = coberturaCotizacionFacade.findById(coberturaCotizacionDTO.getId());
					if(coberturaCotizacionDTO != null){
						coberturaCotizacionDTO.setValorPrimaNeta(movimiento.getValorDiferenciaPrimaNeta().doubleValue());
						coberturas.add(coberturaCotizacionDTO);						
					}
				}
			}
		}else{
			coberturas = coberturaCotizacionFacade.listarCoberturasContratadas(cotizacionDTO.getIdToCotizacion());
		}
		for(CoberturaCotizacionDTO cobertura: coberturas){
			valorBonifComision += cobertura.getValorPrimaNeta()* cotizacionDTO.getPorcentajebonifcomision() / 100;
			ComisionCotizacionId id = new ComisionCotizacionId();
			id.setIdTcSubramo(cobertura.getIdTcSubramo());
			id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());

			ComisionCotizacionDTO comision = null;
			SoporteResumen resumenComision = new SoporteResumen();
			Double sumaAseguradaIncendio = 0D;
			
			if (cobertura.getIdTcSubramo().intValue()== 1) {
				limiteSA = limiteSA / cotizacionDTO.getTipoCambio();
				// Se obtiene la SA de Incendio por Inciso
				sumaAseguradaIncendio = coberturaCotizacionFacade.obtenerSACoberturasBasicasIncendioPorCotizacion(cobertura.getId().getIdToCotizacion());					
				if (agrupacion != null) {
					if (cobertura.getNumeroAgrupacion().intValue() == agrupacion.getId().getNumeroAgrupacion().intValue()) {
						id.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_PRR);
					}else{
						id.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RO);
					}
				} else {
					if (sumaAseguradaIncendio < limiteSA) {
						id.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RO);
					} else if (sumaAseguradaIncendio >= limiteSA) {
						id.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RCI);
					}else{
						id.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RO);
					}
				}
			} else {
				id.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RO);
			}
			comision = comisionCotizacionFacade.findById(id);		
			valorComision += cobertura.getValorPrimaNeta()* factor.doubleValue() * comision.getPorcentajeComisionCotizacion().doubleValue() / 100D;
			
			SubRamoDTO subRamoDTO = subRamoFacade.findById(cobertura.getIdTcSubramo());

			resumenComision.setMontoComision(cobertura.getValorPrimaNeta() * comision.getPorcentajeComisionCotizacion().doubleValue() / 100D);
			resumenComision.setDescripcionRamo(subRamoDTO.getDescripcionSubRamo());
			resumenComision.setPorcentajeComision(comision.getPorcentajeComisionCotizacion().doubleValue());
			resumenComision.setMontoComisionCedida(resumenComision.getMontoComision() * cotizacionDTO.getPorcentajebonifcomision() / 100D);
//			resumenComision.setTipoComision(comision.getId().getTipoPorcentajeComision());
//			resumenComision.setSubRamo(cobertura.getIdTcSubramo().intValue());
			resumenComision.setId(comision.getId());
			resumenComisionesTmp.add(resumenComision);
		}
		valorComisionRPF = valorComision * pctPagoFraccionado / 100D;
		// valorBonifComision = PrimaNeta de la cotizacion * porcentajeBonifComision/100		
		cotizacionDTO.setMontoBonificacionComision(valorComision * cotizacionDTO.getPorcentajebonifcomision() / 100D);		
		
		// valorRecargoPagoFrac = valorPrimaNeta * PorcentajeRPF/100
		cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getPrimaNetaCotizacion() * pctPagoFraccionado / 100D);
		
		// valorBonifComRecPagoFrac = valorRecargoPagoFrac * porcentajeBonifComision/100
		cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(valorComisionRPF * cotizacionDTO.getPorcentajebonifcomision() / 100D);		

		// valorIVA = (primaNeta + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac) * porcentaje de IVA
		montoIVA = (cotizacionDTO.getPrimaNetaCotizacion() + cotizacionDTO.getMontoRecargoPagoFraccionado() + cotizacionDTO.getDerechosPoliza() - cotizacionDTO.getMontoBonificacionComision() - cotizacionDTO.getMontoBonificacionComisionPagoFraccionado()) * factorIVA;
		cotizacionDTO.setMontoIVA(montoIVA);

		//Prime neta de la cotizacion
		cotizacionDTO.setPrimaNetaCotizacion(primaNetaCotizacion - cotizacionDTO.getMontoBonificacionComision());
		cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getMontoRecargoPagoFraccionado() - cotizacionDTO.getMontoBonificacionComisionPagoFraccionado());
		// valorPrimaTotal = primaNeta + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac + IVA		
		cotizacionDTO.setPrimaNetaTotal(cotizacionDTO.getPrimaNetaCotizacion()
				+ cotizacionDTO.getMontoRecargoPagoFraccionado()
				+ cotizacionDTO.getDerechosPoliza()
				+ montoIVA);
		//set comisiones
		
		return cotizacionDTO;
	}
	
	public CotizacionDTO setTotalesResumenCambioFormaPagoImpresion(CotizacionDTO cotizacionEndosoActual) throws SystemException{
		List<EndosoDTO> endosos = null;
		
		//Se obtienen los endosos de la p�liza en orden descendente
		BigDecimal idToPoliza = cotizacionEndosoActual.getSolicitudDTO().getIdToPolizaEndosada();		
		endosos = endosoFacade.findByProperty("id.idToPoliza", idToPoliza,false);
		
		//Se busca un endoso emitido con la cotizaci�n dada como par�metro
		EndosoDTO endosoEmitido = endosoFacade.buscarPorCotizacion(cotizacionEndosoActual.getIdToCotizacion());
		
		//Si no se ha emitido el endoso
		if(endosoEmitido == null){
			//Se actualiza la fecha de inicio de vigencia a la fecha fin de vigencia del �ltimo recibo pagado o el primero emitido
			EndosoDTO ultimoEndoso = endosoFacade.getUltimoEndoso(cotizacionEndosoActual.getSolicitudDTO().getIdToPolizaEndosada());
			Integer tipoEndoso = endosoFacade.calculaTipoEndoso(cotizacionEndosoActual);
			EndosoIDTO endosoIDTO;
			try {
				endosoIDTO = endosoInterfazServicios.validaEsCancelable(ultimoEndoso.getId().getIdToPoliza().toString() + "|" + 
						                                                            ultimoEndoso.getId().getNumeroEndoso(), 
						                                                            cotizacionEndosoActual.getCodigoUsuarioModificacion(),
						                                                            cotizacionEndosoActual.getFechaInicioVigencia(),
						                                                            tipoEndoso);
				cotizacionEndosoActual.setFechaInicioVigencia(endosoIDTO.getFechaInicioVigencia());
				cotizacionEndosoActual = cotizacionFacade.update(cotizacionEndosoActual);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{//Si el endoso ya fue emitido
			//Se eliminan de la lista el endoso de CFP emitido y todos los endosos posteriores a este.
			//(se deja la lista, tal como si no se hubiera emitido el endoso de CFP)
			for(EndosoDTO endoso : endosos){
				endosos.remove(endoso);
				if(endoso.getIdToCotizacion().compareTo(cotizacionEndosoActual.getIdToCotizacion()) == 0)
					break;
			}
		}

		double diasEndosoActual = Utilerias.obtenerDiasEntreFechas(cotizacionEndosoActual.getFechaInicioVigencia(), 
																	  cotizacionEndosoActual.getFechaFinVigencia());
		
		double primaNetaEndoso = cotizacionFacade.getPrimaNetaCotizacion(cotizacionEndosoActual.getIdToCotizacion());
		primaNetaEndoso *= diasEndosoActual/365;
		double recargoPagoFraccionadoEndososAnteriores = 0;
		for(EndosoDTO endoso : endosos){			
			//Se obtiene el factor de vigencia
			double diasEndoso = Utilerias.obtenerDiasEntreFechas(endoso.getFechaInicioVigencia(),endoso.getFechaFinVigencia());			
			double factor = diasEndosoActual/diasEndoso;
			
			factor = factor > 1 ? 1 : factor; //es necesario igualar el factor a 1 para no regresar m�s RPF de los emitidos				
			
			//Se acumula el recargo por pago fraccionado de los endosos anteriores considerando la bonificaci�n de comisi�n			
			recargoPagoFraccionadoEndososAnteriores += (endoso.getValorRecargoPagoFrac()-endoso.getValorBonifComisionRPF()) * factor;

			//Se termina el proceso cuando se encuentra el �ltimo endoso de CFP emitido.
			if(endoso.getClaveTipoEndoso().compareTo(ConstantesCotizacion.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO) == 0)
				break;
		}
		
		//Obtiene % por forma de Pago					
		BigDecimal pctPagoFraccionado = BigDecimal.valueOf(cotizacionFacade.obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacionEndosoActual));
		
		//Se calcula el monto del recargo por pago fraccionado a emitir/emitido 
		double montoRecargoPagoFraccionado = 0;
		if(endosoEmitido == null){//Si no se ha emitido el endoso
			montoRecargoPagoFraccionado = primaNetaEndoso * pctPagoFraccionado.doubleValue() / 100;
		}else{//Si el endoso ya fue emitido
			montoRecargoPagoFraccionado = endosoEmitido.getValorRecargoPagoFrac();
		}
		
		montoRecargoPagoFraccionado -= recargoPagoFraccionadoEndososAnteriores;						
		
		//Se obtiene % de IVA
		Double ivaObtenido = codigoPostalIVAFacade.getIVAPorIdCotizacion(cotizacionEndosoActual.getIdToCotizacion(), cotizacionEndosoActual.getCodigoUsuarioCreacion());
		if(ivaObtenido==null){
		    throw new SystemException("No se pudo obtener el valor para el IVA.");
		}
		
		BigDecimal factorIVA = BigDecimal.valueOf(ivaObtenido).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP);
		cotizacionEndosoActual.setFactorIVA(factorIVA.multiply(BigDecimal.valueOf(100D)).doubleValue());
		cotizacionEndosoActual.setPrimaNetaCotizacion(0D);
		cotizacionEndosoActual.setMontoRecargoPagoFraccionado(montoRecargoPagoFraccionado);				
		cotizacionEndosoActual.setDerechosPoliza(0D);				
		cotizacionEndosoActual.setMontoIVA(cotizacionEndosoActual.getMontoRecargoPagoFraccionado() * factorIVA.doubleValue());
		cotizacionEndosoActual.setPrimaNetaTotal(cotizacionEndosoActual.getMontoRecargoPagoFraccionado() + cotizacionEndosoActual.getMontoIVA());
		
		return cotizacionEndosoActual;
	}
	
	public CotizacionDTO setTotalesResumenCotizacion(CotizacionDTO cotizacionDTO, List<SoporteResumen> resumenComisiones) throws SystemException{
		BigDecimal factor = Utilerias.getFactorVigencia(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
		BigDecimal primaNetaCotizacion = BigDecimal.ZERO;
		BigDecimal pctPagoFraccionado = BigDecimal.ZERO;
		BigDecimal derechosPoliza = BigDecimal.ZERO;
		BigDecimal factorIVA = BigDecimal.ZERO;
		BigDecimal montoIVA = BigDecimal.ZERO;
		BigDecimal valorBonifComision = BigDecimal.ZERO;
		BigDecimal valorComision = BigDecimal.ZERO;
		BigDecimal valorComisionRPF = BigDecimal.ZERO;
		Double ivaObtenido=null;
		EndosoDTO ultimoEndoso = null;
		List<MovimientoCotizacionEndosoDTO> movimientos = new ArrayList<MovimientoCotizacionEndosoDTO>();
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0){
			if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
				ultimoEndoso = endosoFacade.getUltimoEndoso(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
				Integer tipoEndoso = endosoFacade.calculaTipoEndoso(cotizacionDTO);
				EndosoIDTO endosoIDTO;
				try {
					endosoIDTO = endosoInterfazServicios.validaEsCancelable(ultimoEndoso.getId().getIdToPoliza().toString() + "|"+ 
							ultimoEndoso.getId().getNumeroEndoso(), cotizacionDTO.getCodigoUsuarioModificacion(),cotizacionDTO.getFechaInicioVigencia(),tipoEndoso);
					
					factor = Utilerias.getFactorVigencia(endosoIDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());

					primaNetaCotizacion = BigDecimal.valueOf(cotizacionFacade.getPrimaNetaCotizacion(cotizacionDTO.getIdToCotizacion()));
					cotizacionDTO.setPrimaNetaAnual(primaNetaCotizacion.doubleValue());
					primaNetaCotizacion = BigDecimal.valueOf(cotizacionDTO.getPrimaNetaAnual()).multiply(factor);
					cotizacionDTO.setDiasPorDevengar(Utilerias.obtenerDiasEntreFechas(endosoIDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
					cotizacionDTO.setFechaInicioVigencia(endosoIDTO.getFechaInicioVigencia());
					cotizacionDTO = cotizacionFacade.update(cotizacionDTO);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else{
				factor = Utilerias.getFactorVigencia(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
				MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
				dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());

				movimientos = movimientoCotEndFacade.listarFiltrado(dto);

				for (MovimientoCotizacionEndosoDTO movimiento : movimientos) {
					if (movimiento.getIdToCobertura() != null && movimiento.getIdToCobertura().intValue() >0)
						primaNetaCotizacion = primaNetaCotizacion.add(movimiento.getValorDiferenciaPrimaNeta());
				}
				cotizacionDTO.setPrimaNetaAnual(primaNetaCotizacion.doubleValue());
				primaNetaCotizacion = BigDecimal.valueOf(cotizacionDTO.getPrimaNetaAnual()).multiply(factor);
				cotizacionDTO.setDiasPorDevengar(Utilerias.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
				
			}
		}else{
			if(cotizacionDTO.getTipoPolizaDTO().getProductoDTO() != null && 
					cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getClaveAjusteVigencia().intValue() == 0){
				primaNetaCotizacion = BigDecimal.valueOf(cotizacionDTO.getPrimaNetaAnual());
				cotizacionDTO.setDiasPorDevengar(365D);
			}else{
				primaNetaCotizacion = BigDecimal.valueOf(cotizacionDTO.getPrimaNetaAnual()).multiply(factor);
				cotizacionDTO.setDiasPorDevengar(Utilerias.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
			}
			
		}
		List<SoporteResumen> resumenComisionesTmp = new ArrayList<SoporteResumen>();
		//Prima neta de la cotizacion
		cotizacionDTO.setPrimaNetaCotizacion(primaNetaCotizacion.doubleValue());

		//Obtiene el limite de SA
		List<ParametroGeneralDTO> parametrosLimiteSA = parametroGeneralFacade.findByProperty("id.codigoParametroGeneral",BigDecimal.valueOf(30010D));
				
		double limiteSA = Double.valueOf(parametrosLimiteSA.get(0).getValor());
		
		//Obtiene si existe una agrupacion a Primer riesgo
		AgrupacionCotDTO agrupacion = agrupacionCotFacade.buscarPorCotizacion(cotizacionDTO.getIdToCotizacion(),ConstantesCotizacion.TIPO_PRIMER_RIESGO);	

		
		boolean esEndosoCFP = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null && 
				cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO;
		boolean esEndosoCancelacion = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null && 
				cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CANCELACION;
				 
		//Obtiene % por forma de Pago	
		CotizacionDTO cotizacionTmp = null;
		if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario() == null || cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(ConstantesCotizacion.CLAVE_RPF_SISTEMA) == 0 || esEndosoCFP){
			pctPagoFraccionado = BigDecimal.valueOf(cotizacionFacade.obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacionDTO));
			cotizacionTmp = cotizacionFacade.modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), ConstantesCotizacion.CLAVE_RPF_SISTEMA, pctPagoFraccionado.doubleValue(), false, false);
		}else if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(ConstantesCotizacion.CLAVE_RPF_USUARIO_MONTO) == 0){
			cotizacionTmp = cotizacionFacade.modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), ConstantesCotizacion.CLAVE_RPF_USUARIO_MONTO, cotizacionDTO.getValorRecargoPagoFraccionadoUsuario(), false, false);
		}else if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(ConstantesCotizacion.CLAVE_RPF_USUARIO_PORCENTAJE) == 0){
			cotizacionTmp = cotizacionFacade.modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), ConstantesCotizacion.CLAVE_RPF_USUARIO_PORCENTAJE, cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario(), false, false);
		}
		
		pctPagoFraccionado = BigDecimal.valueOf(cotizacionTmp.getPorcentajeRecargoPagoFraccionadoUsuario());				
		cotizacionDTO.setPorcentajePagoFraccionado(pctPagoFraccionado.doubleValue());
		
		//Se obtienen los gastos de expedicion
		if(cotizacionDTO.getValorDerechosUsuario()==null || cotizacionDTO.getClaveDerechosUsuario().compareTo(ConstantesCotizacion.CLAVE_DERECHOS_SISTEMA) == 0 || esEndosoCFP || esEndosoCancelacion){
			derechosPoliza = BigDecimal.valueOf(cotizacionFacade.calcularDerechosCotizacion(cotizacionDTO,false));
			boolean sinAutorizacion = false;
			if(esEndosoCFP || esEndosoCancelacion)
				sinAutorizacion = true;						
			cotizacionFacade.modificarDerechos(cotizacionDTO.getIdToCotizacion(), ConstantesCotizacion.CLAVE_DERECHOS_SISTEMA, derechosPoliza.doubleValue(), false, sinAutorizacion);
		}else{
			derechosPoliza = BigDecimal.valueOf(cotizacionDTO.getValorDerechosUsuario());
		}
		
		cotizacionDTO.setDerechosPoliza(derechosPoliza.doubleValue());
		
		//Se obtiene % de IVA
		ivaObtenido = codigoPostalIVAFacade.getIVAPorIdCotizacion(cotizacionDTO.getIdToCotizacion(), cotizacionDTO.getCodigoUsuarioCreacion());
		if(ivaObtenido==null){
		    throw new SystemException("No se pudo obtener el valor para el IVA.");
		}
		
		factorIVA = BigDecimal.valueOf(ivaObtenido).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP);
		cotizacionDTO.setFactorIVA(factorIVA.multiply(BigDecimal.valueOf(100D)).doubleValue());

		// se guardan las comisiones q se eliminaron en este arreglo
		int numeroEndosoAnterior = 0;
		EndosoDTO endosoDTO = null;
		List<SoporteResumen> resumenComisionesEliminadasTmp = new ArrayList<SoporteResumen>();
		EndosoDTO endosoActualDTO = endosoFacade.buscarPorCotizacion(cotizacionDTO.getIdToCotizacion());
		if (endosoActualDTO != null && endosoActualDTO.getId().getNumeroEndoso().intValue() != 0){
			numeroEndosoAnterior = endosoActualDTO.getId().getNumeroEndoso().intValue() - 1;
			endosoDTO = endosoFacade.findById(new EndosoId(endosoActualDTO.getId().getIdToPoliza(), (short)numeroEndosoAnterior));
		}else{
			endosoDTO = endosoFacade.getUltimoEndoso(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
		}

		//se obtiene correctamente el monto de bonificacion de la comision
		List<CoberturaCotizacionDTO> coberturas= new ArrayList<CoberturaCotizacionDTO>();
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0){
			for(MovimientoCotizacionEndosoDTO movimiento: movimientos){
				if(movimiento.getIdToCobertura() != null && movimiento.getIdToCobertura().intValue() > 0){
					CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
					CoberturaCotizacionId id = new CoberturaCotizacionId();
					id.setIdToCotizacion(movimiento.getIdToCotizacion());
					id.setNumeroInciso(movimiento.getNumeroInciso());
					id.setIdToSeccion(movimiento.getIdToSeccion());
					id.setIdToCobertura(movimiento.getIdToCobertura());
					coberturaCotizacionDTO.setId(id);
					coberturaCotizacionDTO = coberturaCotizacionFacade.findById(id);
					if(coberturaCotizacionDTO != null){
						coberturaCotizacionDTO.setValorPrimaNeta(movimiento.getValorDiferenciaPrimaNeta().doubleValue());
						coberturas.add(coberturaCotizacionDTO);
					}else{
						CoberturaCotizacionDTO coberturaCotizacionTmp = new CoberturaCotizacionDTO();
						CoberturaCotizacionId idTmp = new CoberturaCotizacionId();
						idTmp.setIdToCotizacion(endosoDTO.getIdToCotizacion());
						idTmp.setNumeroInciso(movimiento.getNumeroInciso());
						idTmp.setIdToSeccion(movimiento.getIdToSeccion());
						idTmp.setIdToCobertura(movimiento.getIdToCobertura());
						coberturaCotizacionTmp.setId(idTmp);
						coberturaCotizacionTmp = coberturaCotizacionFacade.findById(idTmp);						
						
						ComisionCotizacionId idComis = new ComisionCotizacionId();
						idComis.setIdTcSubramo(coberturaCotizacionTmp.getIdTcSubramo());
						idComis.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
						
						ComisionCotizacionDTO comision = null;
						SoporteResumen resumenComision = new SoporteResumen();
						Double sumaAseguradaIncendio = 0D;					
						
						if (coberturaCotizacionTmp.getIdTcSubramo().intValue()== 1) {
							limiteSA = limiteSA / cotizacionDTO.getTipoCambio();
							// Se obtiene la SA de Incendio por Inciso
							sumaAseguradaIncendio = coberturaCotizacionFacade.obtenerSACoberturasBasicasIncendioPorCotizacion(coberturaCotizacionTmp.getId().getIdToCotizacion());
							if (agrupacion != null) {
								if (coberturaCotizacionTmp.getNumeroAgrupacion().intValue() == agrupacion
										.getId().getNumeroAgrupacion().intValue()) {
									idComis.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_PRR);
								}else{
									idComis.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RO);
								}
							} else {
								if (sumaAseguradaIncendio < limiteSA) {
									idComis.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RO);
								} else if (sumaAseguradaIncendio >= limiteSA) {
									idComis.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RCI);
								}else{
									idComis.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RO);	
								}
							}
						} else {
							idComis.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RO);
						}
						comision = comisionCotizacionFacade.findById(idComis);		
						//valorComision = valorComision.add(BigDecimal.valueOf(coberturaCotizacionTmp.getValorPrimaNeta()).multiply(comision.getPorcentajeComisionCotizacion()).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP));
						SubRamoDTO subRamoDTO = subRamoFacade.findById(coberturaCotizacionTmp.getIdTcSubramo());

						resumenComision.setMontoComision((coberturaCotizacionTmp.getValorPrimaNeta() * comision.getPorcentajeComisionCotizacion().doubleValue() / 100D)*-1);
						resumenComision.setDescripcionRamo(subRamoDTO.getDescripcionSubRamo());
						resumenComision.setPorcentajeComision(comision.getPorcentajeComisionCotizacion().doubleValue());
						resumenComision.setMontoComisionCedida((resumenComision.getMontoComision() * cotizacionDTO.getPorcentajebonifcomision() / 100D)*-1);
						resumenComision.setId(comision.getId());
						resumenComisionesEliminadasTmp.add(resumenComision);						
						
					}
				}
			}
		}else{
			coberturas = coberturaCotizacionFacade.listarCoberturasContratadas(cotizacionDTO.getIdToCotizacion());
		}
			
		

		for(CoberturaCotizacionDTO cobertura: coberturas){
			valorBonifComision = valorBonifComision.add(BigDecimal.valueOf(cobertura.getValorPrimaNeta()).multiply(BigDecimal.valueOf(cotizacionDTO.getPorcentajebonifcomision())).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP));
			ComisionCotizacionId id = new ComisionCotizacionId();
			id.setIdTcSubramo(cobertura.getIdTcSubramo());
			id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());

			ComisionCotizacionDTO comision = null;
			SoporteResumen resumenComision = new SoporteResumen();
			Double sumaAseguradaIncendio = 0D;

			if (cobertura.getIdTcSubramo().intValue()== 1) {
				limiteSA = limiteSA / cotizacionDTO.getTipoCambio();
				// Se obtiene la SA de Incendio por Inciso
				sumaAseguradaIncendio = coberturaCotizacionFacade.obtenerSACoberturasBasicasIncendioPorCotizacion(cobertura.getId().getIdToCotizacion());
				if (agrupacion != null) {
					if (cobertura.getNumeroAgrupacion().intValue() == agrupacion
							.getId().getNumeroAgrupacion().intValue()) {
						id.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_PRR);
					}else{
						id.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RO);
					}
				} else {
					if (sumaAseguradaIncendio < limiteSA) {
						id.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RO);
					} else if (sumaAseguradaIncendio >= limiteSA) {
						id.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RCI);
					}else{
						id.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RO);
					}
				}
			} else {
				id.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RO);
			}
			comision = comisionCotizacionFacade.findById(id);		
			valorComision = valorComision.add(BigDecimal.valueOf(cobertura.getValorPrimaNeta() * factor.doubleValue()).multiply(comision.getPorcentajeComisionCotizacion()).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP));
			
			SubRamoDTO subRamoDTO = subRamoFacade.findById(cobertura.getIdTcSubramo());

			resumenComision.setMontoComision(cobertura.getValorPrimaNeta() * comision.getPorcentajeComisionCotizacion().doubleValue() / 100D);
			resumenComision.setDescripcionRamo(subRamoDTO.getDescripcionSubRamo());
			resumenComision.setPorcentajeComision(comision.getPorcentajeComisionCotizacion().doubleValue());
			resumenComision.setMontoComisionCedida(resumenComision.getMontoComision() * cotizacionDTO.getPorcentajebonifcomision() / 100D);
			resumenComision.setId(comision.getId());
			resumenComisionesTmp.add(resumenComision);
		}
		
		// valorBonifComision = PrimaNeta de la cotizacion * porcentajeBonifComision/100		
		cotizacionDTO.setMontoBonificacionComision(valorComision.multiply(BigDecimal.valueOf(cotizacionDTO.getPorcentajebonifcomision())).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP).doubleValue());		
		
		EndosoDTO endosoEmitido = endosoFacade.buscarPorCotizacion(cotizacionDTO.getIdToCotizacion());
		boolean esEndosoCE = false;
		boolean esEndosoRE = false;
		if(endosoEmitido != null){
			esEndosoCE = endosoEmitido.getClaveTipoEndoso().shortValue() == ConstantesCotizacion.TIPO_ENDOSO_CE;
			esEndosoRE = endosoEmitido.getClaveTipoEndoso().shortValue() == ConstantesCotizacion.TIPO_ENDOSO_RE;
		}
		if(esEndosoCE || esEndosoRE){
			primaNetaCotizacion = new BigDecimal(endosoEmitido.getValorPrimaNeta());
			cotizacionDTO.setMontoBonificacionComision(endosoEmitido.getValorBonifComision());
			cotizacionDTO.setMontoRecargoPagoFraccionado(endosoEmitido.getValorRecargoPagoFrac());
			cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(endosoEmitido.getValorBonifComisionRPF());
			cotizacionDTO.setDerechosPoliza(endosoEmitido.getValorDerechos());			
		}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!=null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue()==ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CANCELACION){
			BigDecimal idToPoliza = cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada();
			double diasPorDevengar = Utilerias.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
			cotizacionDTO = polizaFacade.setMontosRFPAcumulados(idToPoliza, diasPorDevengar, cotizacionDTO);
			cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getMontoRecargoPagoFraccionado() * -1D);
			cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(cotizacionDTO.getMontoBonificacionComisionPagoFraccionado() * -1D);
		}else{
			// valorRecargoPagoFrac = valorPrimaNeta * PorcentajeRPF/100
			cotizacionDTO.setMontoRecargoPagoFraccionado(primaNetaCotizacion.multiply(pctPagoFraccionado).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP).doubleValue());
					
			valorComisionRPF = valorComision.multiply(pctPagoFraccionado).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP);
			// valorBonifComRecPagoFrac = valorRecargoPagoFrac * porcentajeBonifComision/100
			cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(valorComisionRPF.multiply(BigDecimal.valueOf(cotizacionDTO.getPorcentajebonifcomision())).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP).doubleValue());		
		}
								
		// valorIVA = (primaNeta + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac) * porcentaje de IVA
		montoIVA = primaNetaCotizacion.add(BigDecimal.valueOf(cotizacionDTO.getMontoRecargoPagoFraccionado())).add(BigDecimal.valueOf(cotizacionDTO.getDerechosPoliza())).subtract(BigDecimal.valueOf(cotizacionDTO.getMontoBonificacionComision())).subtract(BigDecimal.valueOf(cotizacionDTO.getMontoBonificacionComisionPagoFraccionado())).multiply(factorIVA);
		cotizacionDTO.setMontoIVA(montoIVA.doubleValue());

		//Prime neta de la cotizacion
		cotizacionDTO.setPrimaNetaCotizacion(primaNetaCotizacion.doubleValue() - cotizacionDTO.getMontoBonificacionComision());
		cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getMontoRecargoPagoFraccionado() - cotizacionDTO.getMontoBonificacionComisionPagoFraccionado());
		
		// valorPrimaTotal = primaNeta + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac + IVA		
		cotizacionDTO.setPrimaNetaTotal(cotizacionDTO.getPrimaNetaCotizacion()
				+ cotizacionDTO.getMontoRecargoPagoFraccionado().doubleValue()
				+ cotizacionDTO.getDerechosPoliza().doubleValue()
				+ montoIVA.doubleValue());
		//set comisiones 
		resumenComisionesTmp.addAll(resumenComisionesEliminadasTmp);
		this.ordenarAgruparComisiones(resumenComisionesTmp, resumenComisiones);
		if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
			if(ultimoEndoso.getIdToCotizacion()!= null){
				CotizacionDTO cotizacionUltimoEndoso = cotizacionFacade.findById(ultimoEndoso.getIdToCotizacion());
				cotizacionUltimoEndoso.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
				List<CoberturaCotizacionDTO> coberturasUltimoEndoso = coberturaCotizacionFacade.listarCoberturasContratadas(cotizacionDTO.getIdToCotizacion());
				calcularTotalesResumenCotizacion(cotizacionUltimoEndoso, coberturasUltimoEndoso);
				cotizacionDTO.setPrimaNetaCotizacion(0D);
				cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getMontoRecargoPagoFraccionado() - cotizacionUltimoEndoso.getMontoRecargoPagoFraccionado());				
				cotizacionDTO.setDerechosPoliza(0D);				
				cotizacionDTO.setMontoIVA(cotizacionDTO.getMontoRecargoPagoFraccionado() * factorIVA.doubleValue());
				cotizacionDTO.setPrimaNetaTotal(cotizacionDTO.getMontoRecargoPagoFraccionado() + cotizacionDTO.getMontoIVA());
				
				cotizacionDTO.setValorRecargoPagoFraccionadoUsuario(cotizacionDTO.getMontoRecargoPagoFraccionado());
				cotizacionFacade.modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), ConstantesCotizacion.CLAVE_RPF_USUARIO_MONTO, cotizacionDTO.getMontoRecargoPagoFraccionado(), false, true);
				
				cotizacionDTO.setValorDerechosUsuario(cotizacionDTO.getDerechosPoliza());
				cotizacionFacade.modificarDerechos(cotizacionDTO.getIdToCotizacion(), ConstantesCotizacion.CLAVE_DERECHOS_USUARIO, cotizacionDTO.getDerechosPoliza(), false, true);
			}
		}
		
		return cotizacionDTO;
	}
	
	/**
	 * Calcula los totales de prima, derechos, recargo, IVA y en base a la forma de pago, define 
	 * los montos de recibos.
	 * @param idToCotizacion
	 * @return
	 */
	public EsquemaPagosDTO obtenerTotalesResumenCotizacion(BigDecimal idToCotizacion){
		EsquemaPagosDTO esquemaPagosDTO = new EsquemaPagosDTO();
		if(idToCotizacion != null){
			CotizacionDTO cotizacionDTO = cotizacionFacade.findById(idToCotizacion);
			
			try {
				cotizacionDTO = calcularTotalesResumenCotizacion(cotizacionDTO, null);
				
				FormaPagoIDTO formaPagoIDTO = obtenerFormaPagoCotizacion(cotizacionDTO, "SISTEMA");
				
				esquemaPagosDTO = this.calcularRecibosCotizacion(formaPagoIDTO.getNumeroRecibosGenerados(),
						cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia(),
						new BigDecimal(cotizacionDTO.getPrimaNetaAnual()),
						new BigDecimal(cotizacionDTO.getMontoRecargoPagoFraccionado()),
						new BigDecimal(cotizacionDTO.getDerechosPoliza()),
						null/*new BigDecimal(cotizacionDTO.getMontoIVA())*/,
						new BigDecimal(cotizacionDTO.getFactorIVA()));
				
				esquemaPagosDTO.setDescripcionFormaPago(formaPagoIDTO.getDescripcion());
				
			} catch (Exception e) {
				LogDeMidasEJB3.log("Error al calcular los totales. idToCotizacion="+idToCotizacion, Level.SEVERE, e);
				throw new RuntimeException(e);
			}
		}
		return esquemaPagosDTO;
	}
	
	public EsquemaPagosDTO calcularRecibosCotizacion(
			Integer cantidadRecibosAnual,BigDecimal totalPrimaNeta,
			BigDecimal totalRecargo,BigDecimal totalDerecho,
			BigDecimal totalIva,BigDecimal factorIVA){
		
		if (cantidadRecibosAnual == null){
			throw new RuntimeException("cantidadRecibosAnual no puede ser null.");
		}
		
		if(factorIVA == null){
			throw new RuntimeException("factorIVA no puede ser null.");
		}
		
		EsquemaPagosDTO esquemaPagos = null;
		esquemaPagos = calcularMontoRecibos(cantidadRecibosAnual, totalPrimaNeta, totalRecargo, factorIVA, totalIva, totalDerecho);
		
		return esquemaPagos;
	}
	
	public EsquemaPagosDTO calcularRecibosCotizacion(Integer cantidadRecibosAnual,Date fechaInicioVigencia,Date fechaFinVigencia,
			BigDecimal totalPrimaNeta,
			BigDecimal totalRecargo,BigDecimal totalDerecho,
			BigDecimal totalIVA,BigDecimal factorIVA){
		
		if (cantidadRecibosAnual == null){
			throw new RuntimeException("cantidadRecibosAnual no puede ser null.");
		}
		
		if(factorIVA == null){
			throw new RuntimeException("factorIVA no puede ser null.");
		}
		
		EsquemaPagosDTO esquemaPagos = null;
		
		int totalPagosEntero = cantidadRecibosAnual.intValue();
		
		if(fechaInicioVigencia != null && fechaFinVigencia != null && cantidadRecibosAnual != null){
			if(fechaInicioVigencia.before(fechaFinVigencia)){
				//Se obtiene el total de meses de la vigencia.
				Calendar calendarInicial = Calendar.getInstance();
				calendarInicial.setTime(fechaInicioVigencia);
				
				Calendar calendarFinal = Calendar.getInstance();
				calendarFinal.setTime(fechaFinVigencia);
				
				long millisVigencia = calendarFinal.getTimeInMillis() - calendarInicial.getTimeInMillis();
				Calendar calendarDiferencia = Calendar.getInstance();
				calendarDiferencia.setTimeInMillis(millisVigencia);
				
				int mesesVigencia = calendarDiferencia.get(Calendar.MONTH);
				//Se agregan los meses correspondientes al a�o.
				mesesVigencia += (calendarDiferencia.get(Calendar.YEAR) - 1970)*12;
				//Se agrega un mes extra por los d�as que se pase del mes
				mesesVigencia += 1;
				
				int pagosAnuales = cantidadRecibosAnual.intValue();
				float totalPagos = (float)(((float)mesesVigencia ) * ((float)pagosAnuales) / 12f);
				totalPagosEntero = Float.valueOf(totalPagos/**(float)pagosAnuales*/).intValue();
				if (totalPagosEntero == 0d){
					totalPagosEntero = 1;
				}
			}
		}
		
		esquemaPagos = calcularMontoRecibos(totalPagosEntero, totalPrimaNeta, totalRecargo, factorIVA, totalIVA, totalDerecho);
		
		return esquemaPagos;
	}
	
	private FormaPagoIDTO obtenerFormaPagoCotizacion(CotizacionDTO cotizacionDTO,String nombreUsuario){
		FormaPagoIDTO formaPagoIDTO = null;
		try {
			formaPagoIDTO = formaPagoInterfazFacade.getPorId(Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()),Short.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()),nombreUsuario);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return formaPagoIDTO;
	}
	
	private EsquemaPagosDTO calcularMontoRecibos(Integer totalPagosEntero,
			BigDecimal totalPrimaNeta,BigDecimal totalRecargo,BigDecimal factorIVA,
			BigDecimal totalIVA,BigDecimal totalDerecho){
		
		EsquemaPagosDTO esquemaPagosDTO = new EsquemaPagosDTO();
		esquemaPagosDTO.setRecibosSubSecuentes(new ReciboDTO());
		esquemaPagosDTO.setPrimerRecibo(new ReciboDTO());

		totalPrimaNeta = totalPrimaNeta != null ? totalPrimaNeta : BigDecimal.ZERO;
		totalRecargo = totalRecargo != null ? totalRecargo : BigDecimal.ZERO;
		totalDerecho = totalDerecho != null ? totalDerecho : BigDecimal.ZERO;
		
		if(factorIVA.compareTo(BigDecimal.ONE) > 0){
			factorIVA = factorIVA.divide(new BigDecimal("100"), new MathContext(12,RoundingMode.HALF_UP));
		}
		
		if(totalIVA == null){
			totalIVA = (totalPrimaNeta.add(totalRecargo).add(totalDerecho)).multiply(factorIVA, new MathContext(12,RoundingMode.HALF_UP));
		}
		BigDecimal totalPrima = totalPrimaNeta.add(totalRecargo).add(totalIVA).add(totalDerecho);
		
		esquemaPagosDTO.setPrimaNeta(totalPrimaNeta);
		esquemaPagosDTO.setDerecho(totalDerecho);
		esquemaPagosDTO.setIva(totalIVA);
		esquemaPagosDTO.setRecargoPagoFraccionado(totalRecargo);
		esquemaPagosDTO.setTotalRecibo(totalPrima);
		esquemaPagosDTO.setFactorIVA(factorIVA);
		
	////Datos para el tercer rengl�n (pagos subsecuentes)
		if (totalPagosEntero > 1){
			//Primero se calculan los montos para los recibos subsecuentes
			BigDecimal numeroRecibos = new BigDecimal (totalPagosEntero);
			BigDecimal reciboSubsecuentePrimeNeta = totalPrimaNeta.divide(numeroRecibos,2,RoundingMode.HALF_UP);
			BigDecimal reciboSubsecuenteTotalRecargo = totalRecargo.divide(numeroRecibos,2,RoundingMode.HALF_UP);
			//En los recibos subsecuentes no se cobra el derecho
			BigDecimal reciboSubsecuenteTotalIva = reciboSubsecuentePrimeNeta.add(reciboSubsecuenteTotalRecargo).multiply(factorIVA).divide(BigDecimal.ONE,2,RoundingMode.HALF_UP);
			BigDecimal totalReciboSubsecuente = reciboSubsecuentePrimeNeta.add(reciboSubsecuenteTotalRecargo).add(reciboSubsecuenteTotalIva).divide(BigDecimal.ONE,2,RoundingMode.HALF_UP);
			//Se agregan los montos de recibos subsecuentes
			esquemaPagosDTO.getRecibosSubSecuentes().setPrimaNeta(reciboSubsecuentePrimeNeta);
			esquemaPagosDTO.getRecibosSubSecuentes().setRecargoPagoFraccionado(reciboSubsecuenteTotalRecargo);
			esquemaPagosDTO.getRecibosSubSecuentes().setDerecho(BigDecimal.ZERO);
			esquemaPagosDTO.getRecibosSubSecuentes().setIva(reciboSubsecuenteTotalIva);
			esquemaPagosDTO.getRecibosSubSecuentes().setTotalRecibo(totalReciboSubsecuente);
			
			//Posteriormente se calculan los montos para el primer recibo, usando restas para obtener el redondeo por centavos
			BigDecimal numeroRecibosMenosUno = numeroRecibos.subtract(BigDecimal.ONE);
			BigDecimal primerReciboPrimeNeta = totalPrimaNeta.subtract(reciboSubsecuentePrimeNeta.multiply(numeroRecibosMenosUno)).divide(BigDecimal.ONE,2,RoundingMode.HALF_UP);
			BigDecimal primerReciboTotalRecargo = totalRecargo.subtract(reciboSubsecuenteTotalRecargo.multiply(numeroRecibosMenosUno)).divide(BigDecimal.ONE,2,RoundingMode.HALF_UP);
			BigDecimal primerReciboTotalIva = totalIVA.subtract(reciboSubsecuenteTotalIva.multiply(numeroRecibosMenosUno)).divide(BigDecimal.ONE,2,RoundingMode.HALF_UP);
			BigDecimal totalPrimerRecibo = totalPrima.subtract(totalReciboSubsecuente.multiply(numeroRecibosMenosUno)).divide(BigDecimal.ONE,2,RoundingMode.HALF_UP);
			
			esquemaPagosDTO.getPrimerRecibo().setPrimaNeta(primerReciboPrimeNeta);
			esquemaPagosDTO.getPrimerRecibo().setRecargoPagoFraccionado(primerReciboTotalRecargo);
			esquemaPagosDTO.getPrimerRecibo().setDerecho(totalDerecho);
			esquemaPagosDTO.getPrimerRecibo().setIva(primerReciboTotalIva);
			esquemaPagosDTO.getPrimerRecibo().setTotalRecibo(totalPrimerRecibo);
		}
		else{
			esquemaPagosDTO.setPrimerRecibo(new ReciboDTO());
			esquemaPagosDTO.getPrimerRecibo().setPrimaNeta(totalPrimaNeta);
			esquemaPagosDTO.getPrimerRecibo().setRecargoPagoFraccionado(totalRecargo);
			esquemaPagosDTO.getPrimerRecibo().setDerecho(totalDerecho);
			esquemaPagosDTO.getPrimerRecibo().setIva(totalIVA);
			esquemaPagosDTO.getPrimerRecibo().setTotalRecibo(totalPrima);
			
			esquemaPagosDTO.getRecibosSubSecuentes().setPrimaNeta(BigDecimal.ZERO);
			esquemaPagosDTO.getRecibosSubSecuentes().setRecargoPagoFraccionado(BigDecimal.ZERO);
			esquemaPagosDTO.getRecibosSubSecuentes().setDerecho(BigDecimal.ZERO);
			esquemaPagosDTO.getRecibosSubSecuentes().setIva(BigDecimal.ZERO);
			esquemaPagosDTO.getRecibosSubSecuentes().setTotalRecibo(BigDecimal.ZERO);
			
		}
		esquemaPagosDTO.setCantidadRecibos(totalPagosEntero);
		
		return esquemaPagosDTO;
	}
	
	private void ordenarAgruparComisiones(List<SoporteResumen> resumenComisiones, List<SoporteResumen> resumenComisionesAgrupada){
		Double totalComisionPRR = 0D;
		Double totalComisionRO = 0D;
		Double totalComisionRCI = 0D;
		Double totalComisionCedidaPRR = 0D;
		Double totalComisionCedidaRO = 0D;
		Double totalComisionCedidaRCI = 0D;	
		Double porcentajeComisionPRR = 0D;
		Double porcentajeComisionRO = 0D;
		Double porcentajeComisionRCI = 0D;
		ComisionCotizacionId idPRR = null;
		ComisionCotizacionId idRO = null;
		ComisionCotizacionId idRCI = null;
		String descripcionRamoIncendio= null;
		boolean tieneIncendio = false;
		List<Integer> subRamos = new ArrayList<Integer>();
		for(int i=0; i < resumenComisiones.size(); i++){
			SoporteResumen soporte = resumenComisiones.get(i);
			if(soporte.getId().getIdTcSubramo().intValue() == 1){
				tieneIncendio = true;
				if (idRO == null)
					idRO = soporte.getId();	
				descripcionRamoIncendio = soporte.getDescripcionRamo();
				if(soporte.getId().getTipoPorcentajeComision().intValue() == ConstantesCotizacion.TIPO_PRR){
					totalComisionPRR = totalComisionPRR + soporte.getMontoComision();
					totalComisionCedidaPRR = totalComisionCedidaPRR + soporte.getMontoComisionCedida();
					porcentajeComisionPRR = soporte.getPorcentajeComision();
				}else if(soporte.getId().getTipoPorcentajeComision().intValue() == ConstantesCotizacion.TIPO_RO){
					totalComisionRO = totalComisionRO + soporte.getMontoComision();
					totalComisionCedidaRO = totalComisionCedidaRO + soporte.getMontoComisionCedida();
					porcentajeComisionRO = soporte.getPorcentajeComision();
				}else if(soporte.getId().getTipoPorcentajeComision().intValue() == ConstantesCotizacion.TIPO_RCI){
					totalComisionRCI = totalComisionRCI + soporte.getMontoComision();
					totalComisionCedidaRCI = totalComisionCedidaRCI + soporte.getMontoComisionCedida();
					porcentajeComisionRCI = soporte.getPorcentajeComision();
				
				}
			}else{
				if(subRamos.size()== 0){
					subRamos.add(soporte.getId().getIdTcSubramo().intValue());
				}else{
					if (!subRamos.contains(soporte.getId().getIdTcSubramo().intValue())){
						subRamos.add(soporte.getId().getIdTcSubramo().intValue());
					}
				}
				
			}
		}
	
		if (tieneIncendio){
			SoporteResumen incendioRO = new SoporteResumen();
			incendioRO.setMontoComision(totalComisionRO);
			incendioRO.setMontoComisionCedida(totalComisionCedidaRO);
			incendioRO.setDescripcionRamo(descripcionRamoIncendio +" RO");
			incendioRO.setPorcentajeComision(porcentajeComisionRO);
			idRO.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RO);
			incendioRO.setId(idRO);
			resumenComisionesAgrupada.add(incendioRO);
			
			SoporteResumen incendioPRR = new SoporteResumen();
			incendioPRR.setMontoComision(totalComisionPRR);
			incendioPRR.setMontoComisionCedida(totalComisionCedidaPRR);
			incendioPRR.setDescripcionRamo(descripcionRamoIncendio +" PRR");
			incendioPRR.setPorcentajeComision(porcentajeComisionPRR);
			idPRR = new ComisionCotizacionId();
			idPRR.setIdTcSubramo(idRO.getIdTcSubramo());
			idPRR.setIdToCotizacion(idRO.getIdToCotizacion());
			idPRR.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_PRR);
			incendioPRR.setId(idPRR);
			resumenComisionesAgrupada.add(incendioPRR);

			SoporteResumen incendioRCI = new SoporteResumen();
			incendioRCI.setMontoComision(totalComisionRCI);
			incendioRCI.setMontoComisionCedida(totalComisionCedidaRCI);
			incendioRCI.setDescripcionRamo(descripcionRamoIncendio +" RCI");
			incendioRCI.setPorcentajeComision(porcentajeComisionRCI);
			idRCI = new ComisionCotizacionId();
			idRCI.setIdTcSubramo(idRO.getIdTcSubramo());
			idRCI.setIdToCotizacion(idRO.getIdToCotizacion());
			idRCI.setTipoPorcentajeComision(ConstantesCotizacion.TIPO_RCI);	
			incendioRCI.setId(idRCI);
			resumenComisionesAgrupada.add(incendioRCI);
			
		}
	
		for(Integer sub: subRamos){
			if(sub.intValue() != 1){
				SoporteResumen soporte = new SoporteResumen();
				Double totalComision = 0D;
				Double totalComisionCedida = 0D;
				String descripcionRamo = null;
				Double porcentajeComision = 0D;
				ComisionCotizacionId idComis = null;
				for(SoporteResumen resumen: resumenComisiones){
					if(sub.intValue() == resumen.getId().getIdTcSubramo().intValue()){
						totalComision = totalComision + resumen.getMontoComision();
						totalComisionCedida = totalComisionCedida + resumen.getMontoComisionCedida();
						descripcionRamo = resumen.getDescripcionRamo();
						porcentajeComision = resumen.getPorcentajeComision();
						idComis = resumen.getId();
					}
				}
				soporte.setDescripcionRamo(descripcionRamo);
				soporte.setMontoComision(totalComision);
				soporte.setMontoComisionCedida(totalComisionCedida);
				soporte.setPorcentajeComision(porcentajeComision);
				soporte.setId(idComis);
				resumenComisionesAgrupada.add(soporte);
				
			}
		}		
	}
}
