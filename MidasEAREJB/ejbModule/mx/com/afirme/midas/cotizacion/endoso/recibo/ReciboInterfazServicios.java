package mx.com.afirme.midas.cotizacion.endoso.recibo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.endoso.recibo.ReciboInterfazServiciosRemote;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboFacadeRemote;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.ws.cliente.PrintReportClient;

@Stateless
public class ReciboInterfazServicios implements ReciboInterfazServiciosRemote {

	@EJB
	private ReciboFacadeRemote reciboFacade;

	/**
	 * Consulta la informaci�n de los Recibos de una P�liza, indicando tambi�n el numero de endoso
	 * @param idPoliza Id de la p�liza
	 * @param numeroEndoso N�mero de endoso espec�fico
	 * @return Los recibos de la p�liza/endoso
	 * @throws SystemException
	 */
	public List<ReciboDTO> consultaRecibos(BigDecimal idPoliza, Short numeroEndoso,String nombreUsuario)throws Exception {
		String numPolizaEndoso = idPoliza + "|" + numeroEndoso;
		return consultaRecibos(numPolizaEndoso, nombreUsuario);
	}
	
	public List<ReciboDTO> consultaRecibos(String numPolizaEndoso,String nombreUsuario) throws Exception {
		return reciboFacade.consultaRecibos(numPolizaEndoso, nombreUsuario);
	}

	public List<ReciboDTO> emiteRecibosPoliza(BigDecimal idPoliza,String nombreUsuario) throws Exception {
		return reciboFacade.emiteRecibosPoliza(idPoliza, nombreUsuario);
	}

	/**
	 * Consulta la informaci�n de los Recibos de una P�liza sin endosar
	 * @param idPoliza Id de la p�liza 
	 * @return Los recibos de la p�liza sin endosar
	 * @throws SystemException
	 */
	public List<ReciboDTO> consultaRecibos(BigDecimal idPoliza,String nombreUsuario) throws Exception {
		return consultaRecibos (idPoliza, (short)0,nombreUsuario);
	}

	public void generarRecibos() {
		printReportClient.generateDigitalBill();
		
	}

	public byte[] obtieneFactura(String llaveFiscal) {
		return printReportClient.getDigitalBill(llaveFiscal);
	}
	
	
	private PrintReportClient printReportClient;

	@EJB
	public void setPrintReportClient(PrintReportClient printReportClient) {
		this.printReportClient = printReportClient;
	}
	
	
	
	
}
