package mx.com.afirme.midas.cotizacion.endoso.movimiento;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity MovimientoCotizacionEndosoDTO.
 * 
 * @see .MovimientoCotizacionEndosoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class MovimientoCotizacionEndosoFacade implements
		MovimientoCotizacionEndosoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	@EJB
	SeccionFacadeRemote seccionFacade;
	/**
	 * Perform an initial save of a previously unsaved
	 * MovimientoCotizacionEndosoDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            MovimientoCotizacionEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(MovimientoCotizacionEndosoDTO entity) {
		LogUtil.log("saving MovimientoCotizacionEndosoDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent MovimientoCotizacionEndosoDTO entity.
	 * 
	 * @param entity
	 *            MovimientoCotizacionEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(MovimientoCotizacionEndosoDTO entity) {
		LogUtil.log("deleting MovimientoCotizacionEndosoDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					MovimientoCotizacionEndosoDTO.class, entity
							.getIdToMovimientoCotEnd());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved MovimientoCotizacionEndosoDTO entity and
	 * return it or a copy of it to the sender. A copy of the
	 * MovimientoCotizacionEndosoDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            MovimientoCotizacionEndosoDTO entity to update
	 * @return MovimientoCotizacionEndosoDTO the persisted
	 *         MovimientoCotizacionEndosoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public MovimientoCotizacionEndosoDTO update(
			MovimientoCotizacionEndosoDTO entity) {
		LogUtil.log("updating MovimientoCotizacionEndosoDTO instance",
				Level.INFO, null);
		try {
			MovimientoCotizacionEndosoDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public MovimientoCotizacionEndosoDTO findById(BigDecimal id) {
		LogUtil.log("finding MovimientoCotizacionEndosoDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			MovimientoCotizacionEndosoDTO instance = entityManager.find(
					MovimientoCotizacionEndosoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all MovimientoCotizacionEndosoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the MovimientoCotizacionEndosoDTO property to
	 *            query
	 * @param value
	 *            the property value to match
	 * @return List<MovimientoCotizacionEndosoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<MovimientoCotizacionEndosoDTO> findByProperty(
			String propertyName, final Object value) {
		LogUtil.log(
				"finding MovimientoCotizacionEndosoDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from MovimientoCotizacionEndosoDTO model where model."
					+ propertyName + "= :propertyValue order by model.idToMovimientoCotEnd desc";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public void borrarPorIdToCotizacion(BigDecimal id) {
		LogUtil.log("deleting MovimientoCotizacionEndosoDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			final String queryString = "delete MIDAS.TOMOVIMIENTOCOTEND where IDTOCOTIZACION = "
					+ id;
			Query query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogUtil.log("deleting failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all MovimientoCotizacionEndosoDTO entities.
	 * 
	 * @return List<MovimientoCotizacionEndosoDTO> all
	 *         MovimientoCotizacionEndosoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<MovimientoCotizacionEndosoDTO> findAll() {
		LogUtil.log("finding all MovimientoCotizacionEndosoDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from MovimientoCotizacionEndosoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public List<MovimientoCotizacionEndosoDTO> listarFiltrado(MovimientoCotizacionEndosoDTO endosoDTO) {
		LogUtil.log("finding listarFiltrado MovimientoCotizacionEndosoDTO instances",
				Level.INFO, null);		
		try {		
			entityManager.flush();
			String queryString = "select model from MovimientoCotizacionEndosoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idToMovimientoCotEnd", endosoDTO.getIdToMovimientoCotEnd());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idToCotizacion", endosoDTO.getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "numeroInciso", endosoDTO.getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idToSeccion", endosoDTO.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idToCobertura", endosoDTO.getIdToCobertura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveTipoMovimiento", endosoDTO.getClaveTipoMovimiento());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveRegeneraRecibos", endosoDTO.getClaveRegeneraRecibos());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionMovimiento", endosoDTO.getDescripcionMovimiento());
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			queryString = queryString.concat(" order by model.idToMovimientoCotEnd, model.numeroInciso, model.idToSeccion");
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();				
		} catch (RuntimeException re) {
			LogUtil.log("find filtrado failed", Level.SEVERE, re);
			throw re;
		}			
	}
	public List<MovimientoCotizacionEndosoDTO> obtenerMovimientosPorCotizacion(
			BigDecimal idToCotizacion) {
		LogUtil.log(
				"finding MovimientoCotizacionEndosoDTO instance with idToCotizacion:"
						+ idToCotizacion, Level.INFO, null);
		try {
			final String queryString = 
				" SELECT idtomovimientocotend, idtocotizacion, numeroinciso, idtoseccion, " +
					" idtocobertura, clavetipomovimiento, valordiferenciasumaasegurada, " +
					" valordiferenciaprimaneta, valordiferenciacuota, descripcionmovimiento, claveregenerarecibos, claveagrupacion "+
				" FROM MIDAS.tomovimientocotend "+
				" WHERE idtocotizacion = " + idToCotizacion +
				    " AND clavetipomovimiento NOT IN (10) " +
				"ORDER BY numeroinciso, idtoseccion, idtocobertura, clavetipomovimiento ";				
			Query query = entityManager.createNativeQuery(queryString);
			return poblarMovimientos(query.getResultList());
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<MovimientoCotizacionEndosoDTO> poblarMovimientos(List resultados){
		List<MovimientoCotizacionEndosoDTO> movimientos = new ArrayList<MovimientoCotizacionEndosoDTO>();
		Iterator resIterator = resultados.iterator();
		Map<BigDecimal,SeccionDTO> mapaSeccionesTMP = new HashMap<BigDecimal, SeccionDTO>();
		while(resIterator.hasNext()){
			Object[] rows = (Object[])resIterator.next();
			MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
			dto.setIdToMovimientoCotEnd((BigDecimal)rows[0]);
			dto.setIdToCotizacion((BigDecimal)rows[1]);
			dto.setNumeroInciso((BigDecimal)rows[2]);
			dto.setIdToSeccion((BigDecimal)rows[3]);
			if(rows[10] != null) {
				dto.setClaveRegeneraRecibos(((BigDecimal)rows[10]).shortValue());
			} else {
				dto.setClaveRegeneraRecibos((short)0);
			}
			if(rows[11] != null) {
				dto.setClaveAgrupacion(((BigDecimal)rows[11]).shortValue());
			} else {
				dto.setClaveAgrupacion((short)1);
			}
			if(dto.getIdToSeccion() != null && dto.getIdToSeccion().intValue()>0){
				if (!mapaSeccionesTMP.containsKey(dto.getIdToSeccion())){
					SeccionDTO seccionTMP = null;
					try{
						seccionTMP = seccionFacade.findById(dto.getIdToSeccion());
					}catch(Exception e){}
					if (seccionTMP != null)
						mapaSeccionesTMP.put(dto.getIdToSeccion(), seccionTMP);
				}
				try{
					dto.setDescripcionSeccion(mapaSeccionesTMP.get(dto.getIdToSeccion()).getNombreComercial());
				}catch(Exception e){
					dto.setDescripcionSeccion("NO DISPONIBLE");
				}
			}
			dto.setIdToCobertura((BigDecimal)rows[4]);
			dto.setClaveTipoMovimiento((short)((BigDecimal)rows[5]).intValue());
			dto.setValorDiferenciaSumaAsegurada((BigDecimal)rows[6]);
			dto.setValorDiferenciaPrimaNeta((BigDecimal)rows[7]);
			dto.setValorDiferenciaCuota((BigDecimal)rows[8]);
			dto.setDescripcionMovimiento((String)rows[9]);
			movimientos.add(dto);
		}
		return movimientos;
	}
	public BigDecimal obtenerDiferenciaPrimaNeta(BigDecimal idToCotizacion) {
		try {
			StringBuffer sb = new StringBuffer();
			
			sb.append("Select sum(model.valorDiferenciaPrimaNeta) from MovimientoCotizacionEndosoDTO model");
			sb.append(" where model.idToCotizacion = :idToCotizacion and ");
			sb.append(" (model.claveTipoMovimiento = 1 or model.claveTipoMovimiento = 2 or model.claveTipoMovimiento = 3 or model.claveTipoMovimiento = 17)");
			Query query = entityManager.createQuery(sb.toString());
			
			query.setParameter("idToCotizacion", idToCotizacion);
			
			return (BigDecimal)query.getSingleResult();
			
			
		} catch (Exception re) {
			re.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<MovimientoCotizacionEndosoDTO> obtieneMovimientosAEvaluar(
			BigDecimal idToCotizacion) {
		
		List rawResults;
		Iterator resIterator;
		List<MovimientoCotizacionEndosoDTO> movimientos = new ArrayList<MovimientoCotizacionEndosoDTO>();
		Object[] row;
		MovimientoCotizacionEndosoDTO movimiento;
		
		LogDeMidasEJB3.log("MovimientoCotizacionEndosoFacade.obtieneMovimientosAEvaluar: "
				+ " idToCotizacion: " + idToCotizacion, Level.INFO, null);
		try {

			StringBuffer sb =  new StringBuffer();
			sb.append("select distinct(numeroinciso),idtocobertura,clavetipomovimiento ");
			sb.append("from MIDAS.tomovimientocotend where idtocotizacion = ");
			sb.append(idToCotizacion);
			sb.append(" and numeroinciso > 0 and idtocobertura > 0 ");
			sb.append(" order by numeroinciso,idtocobertura,clavetipomovimiento"); 
												
			Query query = entityManager.createNativeQuery(sb.toString());
			rawResults =  query.getResultList();
			
			resIterator = rawResults.iterator();
			
			while(resIterator.hasNext()){
				row = (Object[])resIterator.next();
				movimiento = new MovimientoCotizacionEndosoDTO();
				movimiento.setNumeroInciso((BigDecimal)row[0]);
				movimiento.setIdToCobertura((BigDecimal)row[1]);
				movimiento.setClaveTipoMovimiento(Short.parseShort(row[2].toString()));
				movimientos.add(movimiento);
			}
			
			return movimientos;
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("MovimientoCotizacionEndosoFacade.obtieneMovimientosAEvaluar fallo", Level.SEVERE, re);
			throw re;
		}
	}

	public void registrarMovimientos(List<MovimientoCotizacionEndosoDTO> listaMovimientos) {
		LogDeMidasEJB3.log("guardando lista de registros MovimientoCotizacionEndosoDTO",Level.INFO, null);
		try {
			if(listaMovimientos != null){
				for(MovimientoCotizacionEndosoDTO movtoTMP : listaMovimientos){
					entityManager.persist(movtoTMP);
				}
			}
		} catch (RuntimeException re) {
			throw re;
		}
	}
	

}