package mx.com.afirme.midas.cotizacion.endoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;

@Stateless
public class CotizacionEndosoFacade implements CotizacionEndosoFacadeRemote {
	@PersistenceContext
	private EntityManager entityManager;

	@Interceptors( { mx.com.afirme.midas.sistema.log.LogInterceptor.class })
	public void delete(CotizacionDTO entity) {
		LogDeMidasEJB3.log("deleting CotizacionEndoso instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(CotizacionDTO.class, entity
					.getIdToCotizacion());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}

	}

	@SuppressWarnings("unchecked")
	public List<CotizacionDTO> findAll() {
		LogDeMidasEJB3.log("finding all CotizacionEndoso instances",
				Level.INFO, null);
		try {
			entityManager.flush();
			final String queryString = "select model from CotizacionDTO model where model.solicitudDTO.claveTipoSolicitud ="+SistemaPersistencia.SOLICITUD_ENDOSO +
				" order by model.idToCotizacion";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CotizacionDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding CotizacionEndoso instance with id: " + id,
				Level.INFO, null);
		try {
	
			final String queryString = "select model from CotizacionDTO model where model.solicitudDTO.claveTipoSolicitud = "+SistemaPersistencia.SOLICITUD_ENDOSO+" and model.idToCotizacion"
					+ "= :id";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("id", id);
			return (CotizacionDTO) query.getSingleResult();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CotizacionDTO> findByProperty(String propertyName, Object value) {
		LogDeMidasEJB3.log("finding CotizacionEndoso instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from CotizacionDTO model where model.solicitudDTO.claveTipoSolicitud = "+SistemaPersistencia.SOLICITUD_ENDOSO+"  and model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public List<CotizacionDTO> listarCotizaciones() {
		LogDeMidasEJB3.log("finding all Cotizacion instances",Level.INFO, null);
		try {
			entityManager.flush();
			final String queryString = "select model from CotizacionDTO model " +
					"where model.claveEstatus in"+SistemaPersistencia.ESTATUS_DE_COTIZACIONES +
					"and model.solicitudDTO.claveTipoSolicitud = "+SistemaPersistencia.SOLICITUD_ENDOSO +
					" order by model.idToCotizacion";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public List<CotizacionDTO> listarFiltrado(CotizacionDTO cotizacionDTO) {
		try {
			entityManager.flush();
			String queryString = "select model from CotizacionDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (cotizacionDTO == null)
					return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idToCotizacion", cotizacionDTO.getIdToCotizacion());
			try{
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoPolizaDTO.idToTipoPoliza", cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
			}catch (NullPointerException e){}
			try{
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.idToSolicitud", cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
			}catch (NullPointerException e){}
			try{
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "direccionCobroDTO.idToDireccion", cotizacionDTO.getDireccionCobroDTO().getIdToDireccion());
			}catch (NullPointerException e){}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idMoneda", cotizacionDTO.getIdMoneda());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idFormaPago", cotizacionDTO.getIdFormaPago());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicioVigencia", cotizacionDTO.getFechaInicioVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaFinVigencia", cotizacionDTO.getFechaFinVigencia());
			if(cotizacionDTO.getClaveEstatus()!=null){
			    sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveEstatus", cotizacionDTO.getClaveEstatus());
			}else{
			    if (Utilerias.esAtributoQueryValido(sWhere)){
			        sWhere = sWhere.concat(" and ");
			     }
			     sWhere +=" model.claveEstatus in";
			     sWhere +=SistemaPersistencia.ESTATUS_DE_COTIZACIONES;
			}
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaContratante", cotizacionDTO.getNombreEmpresaContratante());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaAsegurado", cotizacionDTO.getNombreEmpresaAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioOrdenTrabajo", cotizacionDTO.getCodigoUsuarioOrdenTrabajo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCotizacion", cotizacionDTO.getCodigoUsuarioCotizacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCreacion", cotizacionDTO.getCodigoUsuarioCreacion());
			
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreAsegurado", cotizacionDTO.getNombreAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreContratante", cotizacionDTO.getNombreContratante());
	
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				queryString += " and model.solicitudDTO.claveTipoSolicitud = " +SistemaPersistencia.SOLICITUD_ENDOSO +
				" order by model.idToCotizacion";
						
			}else{
				queryString += " where model.solicitudDTO.claveTipoSolicitud = "+SistemaPersistencia.SOLICITUD_ENDOSO +
					" order by model.idToCotizacion";
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if(cotizacionDTO.getPrimerRegistroACargar() != null && cotizacionDTO.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(cotizacionDTO.getPrimerRegistroACargar().intValue());
				query.setMaxResults(cotizacionDTO.getNumeroMaximoRegistrosACargar().intValue());
			}
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@Interceptors( { mx.com.afirme.midas.sistema.log.LogInterceptor.class })
	public CotizacionDTO save(CotizacionDTO entity) {
		LogDeMidasEJB3
				.log("saving CotizacionEndoso instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Interceptors( { mx.com.afirme.midas.sistema.log.LogInterceptor.class })
	public CotizacionDTO update(CotizacionDTO entity) {
		LogDeMidasEJB3.log("updating CotizacionEndoso instance", Level.INFO,
				null);
		try {
			CotizacionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public Long obtenerTotalFiltrado(CotizacionDTO cotizacionDTO) {
		try {
			LogDeMidasEJB3.log("Entra a CotizacionEndosoFacade.obtenerTotalFiltrado", Level.INFO, null);
			entityManager.flush();
			String queryString = "select count(model) from CotizacionDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if (cotizacionDTO == null) {
					return null;
			}
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idToCotizacion", cotizacionDTO.getIdToCotizacion());
			
			try{
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "tipoPolizaDTO.idToTipoPoliza", cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
			} catch (NullPointerException e) {
				
			}
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "solicitudDTO.idToSolicitud", cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
			} catch (NullPointerException e) {
				
			}
			try {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "direccionCobroDTO.idToDireccion", cotizacionDTO.getDireccionCobroDTO().getIdToDireccion());
			} catch (NullPointerException e) {
				
			}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idMoneda", cotizacionDTO.getIdMoneda());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "idFormaPago", cotizacionDTO.getIdFormaPago());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaInicioVigencia", cotizacionDTO.getFechaInicioVigencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "fechaFinVigencia", cotizacionDTO.getFechaFinVigencia());
			if (cotizacionDTO.getClaveEstatus() != null) {
			    sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveEstatus", cotizacionDTO.getClaveEstatus());
			} else {
			    if (Utilerias.esAtributoQueryValido(sWhere)) {
			        sWhere = sWhere.concat(" and ");
			     }
			     sWhere += " model.claveEstatus in ";
			     sWhere += SistemaPersistencia.ESTATUS_DE_COTIZACIONES;
			}
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaContratante", cotizacionDTO.getNombreEmpresaContratante());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreEmpresaAsegurado", cotizacionDTO.getNombreEmpresaAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioOrdenTrabajo", cotizacionDTO.getCodigoUsuarioOrdenTrabajo());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCotizacion", cotizacionDTO.getCodigoUsuarioCotizacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreAsegurado", cotizacionDTO.getNombreAsegurado());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreContratante", cotizacionDTO.getNombreContratante());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCreacion", cotizacionDTO.getCodigoUsuarioCreacion());
	
			if (Utilerias.esAtributoQueryValido(sWhere)) {
				queryString = queryString.concat(" where ").concat(sWhere);
				queryString += " and model.solicitudDTO.claveTipoSolicitud = " + SistemaPersistencia.SOLICITUD_ENDOSO;
			} else {
				queryString += " where model.solicitudDTO.claveTipoSolicitud = " + SistemaPersistencia.SOLICITUD_ENDOSO;
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			return (Long)query.getSingleResult();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("CotizacionEndosoFacade.obtenerTotalFiltrado fallo", Level.SEVERE, re);
			throw re;
		}
	}

}
