package mx.com.afirme.midas.solicitud;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoDTO;
import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoFacadeRemote;
import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoId;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.impl.suscripcion.solicitud.SolicitudPolizaServiceImpl;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity SolicitudDTO.
 * 
 * @see .SolicitudDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SolicitudFacade implements SolicitudFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;
	@EJB
	protected EntidadService entidadService;
	public final static String COLUMNS = new String(
			"idToSolicitud,nombrePersona,apellidoPaterno,apellidoMaterno,nombreAgente,productoDTO"
			+ ",fechaCreacion,claveEstatus,codigoUsuarioCreacion,codigoUsuarioAsignacion,codigoAgente,nombreEjecutivo,claveTipoSolicitud,negocio");
	
	private UsuarioService usuarioService;
	private AgenteMidasService agenteMidasService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	/**
	 * Perform an initial save of a previously unsaved SolicitudDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SolicitudDTO entity to persist
	 * @return 
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public SolicitudDTO save(SolicitudDTO entity) {
		LogDeMidasEJB3.log("saving SolicitudDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SolicitudDTO entity.
	 * 
	 * @param entity
	 *            SolicitudDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SolicitudDTO entity) {
		LogDeMidasEJB3.log("deleting SolicitudDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SolicitudDTO.class, entity
					.getIdToSolicitud());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SolicitudDTO entity and return it or a copy of
	 * it to the sender. A copy of the SolicitudDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SolicitudDTO entity to update
	 * @return SolicitudDTO the persisted SolicitudDTO entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SolicitudDTO update(SolicitudDTO entity) {
		LogDeMidasEJB3.log("updating SolicitudDTO instance", Level.INFO, null);
		try {
			SolicitudDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SolicitudDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SolicitudDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SolicitudDTO instance = entityManager.find(SolicitudDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SolicitudDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SolicitudDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SolicitudDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SolicitudDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SolicitudDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SolicitudDTO entities.
	 * 
	 * @return List<SolicitudDTO> all SolicitudDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> findAll() {
		LogDeMidasEJB3.log("finding all SolicitudDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from SolicitudDTO model";
			Query query = entityManager.createQuery(queryString);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> findByUserId(String id) {
		LogDeMidasEJB3.log("finding SolicitudDTO instances with user id: " + id, Level.INFO,
				null);
		try {
			String queryString = "select model from SolicitudDTO model where model.codigoUsuarioCreacion = :id";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("id", String.format("%1$-8s", id));
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> findByEstatus(Short claveEstatus, String userId) {
		LogDeMidasEJB3.log("finding SolicitudDTO instances with user id: " + userId + " and claveEstatus: " + claveEstatus, Level.INFO,
				null);
		try {
			String queryString = "select model from SolicitudDTO model where model.claveEstatus = :claveEstatus and model.codigoUsuarioCreacion = :id";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("claveEstatus", claveEstatus);
			query.setParameter("id", String.format("%1$-8s", userId));
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * ListarFiltrado
	 * 
	 * @param SolicitudDTO
	 * @return List<SolicitudDTO> lista de entidades encontrada
	 */
	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> listarFiltrado(SolicitudDTO solicitudDTO) {
		try {
			String queryString = "select model from SolicitudDTO as model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"idToSolicitud", solicitudDTO.getIdToSolicitud());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombrePersona", solicitudDTO.getNombrePersona());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "apellidoPaterno", solicitudDTO.getApellidoPaterno());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "apellidoMaterno", solicitudDTO.getApellidoMaterno());			
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioModificacion", solicitudDTO.getCodigoUsuarioModificacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombreAgente", solicitudDTO.getNombreAgente());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioAsignacion", solicitudDTO.getCodigoUsuarioAsignacion());
			
			if(solicitudDTO.getClaveEstatus() != null){
				if(solicitudDTO.getClaveEstatus() == SolicitudDTO.Estatus.PROCESO.getEstatus()){
					if (Utilerias.esAtributoQueryValido(sWhere)) {
				        sWhere = sWhere.concat(" and ");
				     }
				     sWhere += " model.claveEstatus IN (" + SolicitudDTO.Estatus.PROCESO.getEstatus() + "," + SolicitudDTO.Estatus.PROCESO_INCOMPLETO.getEstatus() +")";	
				}else{
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveEstatus", solicitudDTO.getClaveEstatus());
				}
			}			
			if (solicitudDTO.getCodigoAgente() != null) {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoAgente", solicitudDTO.getCodigoAgente());
			}
			if(solicitudDTO.getNegocio() != null && solicitudDTO.getNegocio().getClaveNegocio() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"negocio.claveNegocio", solicitudDTO.getNegocio().getClaveNegocio());
				sWhere.concat(" and model.negocio.idToNegocio IS NOT NULL") ;
			}
			
			
			if (solicitudDTO.getFechaCreacion() != null) {
				if (Utilerias.esAtributoQueryValido(sWhere)) {
			        sWhere = sWhere.concat(" and ");
			     }
			     sWhere += " model.fechaCreacion BETWEEN :start AND :end ";
			}			
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				queryString+=" order by model.fechaCreacion desc,model.idToSolicitud ";
			}else{
			    queryString+=" order by model.fechaCreacion desc,model.idToSolicitud ";
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);

			if (solicitudDTO.getFechaCreacion() != null) {
				
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(solicitudDTO.getFechaCreacion());
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
				
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(gcFechaInicio.getTime());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				
				query.setParameter("start", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
				query.setParameter("end", gcFechaFin.getTime(), TemporalType.TIMESTAMP);
				
				System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
				System.out.println("Poliza: Rango de Fecha fin ->" + gcFechaFin.getTime());
				
			}			
			
			if(solicitudDTO.getPrimerRegistroACargar() != null && solicitudDTO.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(solicitudDTO.getPrimerRegistroACargar().intValue());
				query.setMaxResults(solicitudDTO.getNumeroMaximoRegistrosACargar().intValue());
			}
			
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarPorIdFiltrado SolicitudDTO failed",Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Long obtenerTotalFiltradoRapido(SolicitudDTO solicitudDTO) {
		try {
			String queryString="select count(model) from SolicitudDTO As model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"idToSolicitud", solicitudDTO.getIdToSolicitud());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombrePersona", solicitudDTO.getNombrePersona());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "apellidoPaterno", solicitudDTO.getApellidoPaterno());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "apellidoMaterno", solicitudDTO.getApellidoMaterno());			
			//sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCreacion", solicitudDTO.getCodigoUsuarioCreacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioModificacion", solicitudDTO.getCodigoUsuarioModificacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombreAgente", solicitudDTO.getNombreAgente());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoEjecutivo", solicitudDTO.getCodigoEjecutivo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveTipoSolicitud", solicitudDTO.getClaveTipoSolicitud());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioAsignacion", solicitudDTO.getCodigoUsuarioAsignacion());
			if(solicitudDTO.getClaveEstatus() != null){
				if(solicitudDTO.getClaveEstatus() == SolicitudDTO.Estatus.PROCESO.getEstatus()){
					if (Utilerias.esAtributoQueryValido(sWhere)) {
				        sWhere = sWhere.concat(" and ");
				     }
				     sWhere += " model.claveEstatus IN (" + SolicitudDTO.Estatus.PROCESO.getEstatus() + "," + SolicitudDTO.Estatus.PROCESO_INCOMPLETO.getEstatus() +")";	
				}else{
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveEstatus", solicitudDTO.getClaveEstatus());
				}
			}			
			if (solicitudDTO.getCodigoAgente() != null) {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoAgente", solicitudDTO.getCodigoAgente());
			}
			if(solicitudDTO.getNegocio() != null && solicitudDTO.getNegocio().getClaveNegocio() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"negocio.claveNegocio", solicitudDTO.getNegocio().getClaveNegocio());
				sWhere.concat(" and model.negocio.idToNegocio IS NOT NULL") ;
			}
			
			
			if (solicitudDTO.getFechaCreacion() != null) {
				if (Utilerias.esAtributoQueryValido(sWhere)) {
			        sWhere = sWhere.concat(" and ");
			     }
			     sWhere += " model.fechaCreacion BETWEEN :start AND :end ";
			}			
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				queryString+=" order by model.fechaCreacion,model.idToSolicitud ";
			}else{
			    queryString+=" order by model.fechaCreacion,model.idToSolicitud ";
			}
			query = entityManager.createQuery(queryString);
			
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);

			if (solicitudDTO.getFechaCreacion() != null) {
				
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(solicitudDTO.getFechaCreacion());
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
				
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(gcFechaInicio.getTime());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				
				System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
				System.out.println("Poliza: Rango de Fecha fin ->" + gcFechaFin.getTime());
				
				query.setParameter("start", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
				query.setParameter("end", gcFechaFin.getTime(), TemporalType.TIMESTAMP);
			}	
			
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			return (Long)query.getSingleResult();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarPorIdFiltrado SolicitudDTO failed",Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public Long obtenerTotalFiltrado(SolicitudDTO solicitudDTO) {
		LogDeMidasEJB3.log("Entra a SolicitudFacade.obtenerTotalFiltrado", Level.INFO, null);
		try {
			if((solicitudDTO==null)){
			    return null;
			}
			  
			String queryString="select count(model) from SolicitudDTO As model ";
			String sWhere="";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"idToSolicitud", solicitudDTO.getIdToSolicitud());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombrePersona", solicitudDTO.getNombrePersona());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "apellidoPaterno", solicitudDTO.getApellidoPaterno());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "apellidoMaterno", solicitudDTO.getApellidoMaterno());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveEstatus", solicitudDTO.getClaveEstatus());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCreacion", solicitudDTO.getCodigoUsuarioCreacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioModificacion", solicitudDTO.getCodigoUsuarioModificacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombreAgente", solicitudDTO.getNombreAgente());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioAsignacion", solicitudDTO.getCodigoUsuarioAsignacion());
			if (solicitudDTO.getCodigoAgente() != null) {
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoAgente", solicitudDTO.getCodigoAgente());
			}
			if(solicitudDTO.getNegocio() != null && solicitudDTO.getNegocio().getClaveNegocio() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"negocio.claveNegocio", solicitudDTO.getNegocio().getClaveNegocio());
				sWhere.concat(" and model.negocio.idToNegocio IS NOT NULL") ;
			}
			
			
			if (solicitudDTO.getFechaCreacion() != null) {
				if (Utilerias.esAtributoQueryValido(sWhere)) {
			        sWhere = sWhere.concat(" and ");
			     }
			     sWhere += " model.fechaCreacion BETWEEN :start AND :end ";
			}			
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				queryString+=" order by model.fechaCreacion,model.idToSolicitud ";
			}else{
			    queryString+=" order by model.fechaCreacion,model.idToSolicitud ";
			}
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if (solicitudDTO.getFechaCreacion() != null) {
				
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(solicitudDTO.getFechaCreacion());
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
				
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(gcFechaInicio.getTime());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				
				System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
				System.out.println("Poliza: Rango de Fecha fin ->" + gcFechaFin.getTime());
				
				query.setParameter("start", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
				query.setParameter("end", gcFechaFin.getTime(), TemporalType.TIMESTAMP);
			}
			
			return (Long)query.getSingleResult();
		    
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("SolicitudFacade.obtenerTotalFiltrado fallo", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Long obtenerTotalFiltrado(SolicitudDTO solicitudDTO, Date fechaFinal) {
		LogDeMidasEJB3.log("Entra a SolicitudFacade.obtenerTotalFiltrado", Level.INFO, null);
		try {
			if((solicitudDTO==null)){
			    return null;
			}
			  
			String queryString="select count(model) from SolicitudDTO As model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
						

			if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
				Agente agente = usuarioService.getAgenteUsuarioActual();
				Promotoria promotoria = agente.getPromotoria();
				String agenteSubquery = " model.codigoAgente in (select a.id from Agente a where a.promotoria.id = " + promotoria.getId() + ")";
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " " + agenteSubquery;

			} else if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
				Agente agente = usuarioService.getAgenteUsuarioActual();
				solicitudDTO.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
			}
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"idToSolicitud", solicitudDTO.getIdToSolicitud());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombrePersona", solicitudDTO.getNombrePersona());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "apellidoPaterno", solicitudDTO.getApellidoPaterno());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "apellidoMaterno", solicitudDTO.getApellidoMaterno());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveEstatus", solicitudDTO.getClaveEstatus());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCreacion", solicitudDTO.getCodigoUsuarioCreacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioModificacion", solicitudDTO.getCodigoUsuarioModificacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombreAgente", solicitudDTO.getNombreAgente());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoAgente", solicitudDTO.getCodigoAgente());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idOficina", solicitudDTO.getIdOficina());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoEjecutivo", solicitudDTO.getCodigoEjecutivo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveTipoPersona", solicitudDTO.getClaveTipoPersona());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveTipoSolicitud", solicitudDTO.getClaveTipoSolicitud());
			
			
			//sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioAsignacion", solicitudDTO.getCodigoUsuarioAsignacion());
			if(solicitudDTO.getCodigoUsuarioAsignacion() != null && !solicitudDTO.getCodigoUsuarioAsignacion().isEmpty()){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " (UPPER(model.codigoUsuarioAsignacion)  LIKE '%" + solicitudDTO.getCodigoUsuarioAsignacion().toUpperCase() + 
						"%' or  UPPER(model.codigoUsuarioCreacion) LIKE '%" + solicitudDTO.getCodigoUsuarioAsignacion().toUpperCase() + "%')";
			}
			
						
			if(solicitudDTO.getProductoDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "productoDTO.idToProducto", solicitudDTO.getProductoDTO().getIdToProducto());
			}
			if(solicitudDTO.getNegocio() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "negocio.idToNegocio", solicitudDTO.getNegocio().getIdToNegocio());
			}
			if(solicitudDTO.getNegocio() != null && solicitudDTO.getNegocio().getClaveNegocio() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"negocio.claveNegocio", solicitudDTO.getNegocio().getClaveNegocio());
				sWhere.concat(" and model.negocio.idToNegocio IS NOT NULL") ;
			}			
			if(solicitudDTO.getFechaCreacion() != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.fechaCreacion > :fechaInicial";
			}
			if(fechaFinal != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.fechaCreacion < :fechaFinal";
				
			}
			 
	
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				queryString+=" order by model.fechaCreacion desc,model.idToSolicitud ";
			}else{
			    queryString+=" order by model.fechaCreacion desc,model.idToSolicitud ";
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			if(solicitudDTO.getFechaCreacion() != null){
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(solicitudDTO.getFechaCreacion());
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);	
				System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
				query.setParameter("fechaInicial", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
			}
			if(fechaFinal != null){
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(fechaFinal);
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				System.out.println("Poliza: Rango de Fecha fin ->" + gcFechaFin.getTime());
				query.setParameter("fechaFinal", gcFechaFin.getTime(), TemporalType.TIMESTAMP);					
			}
			
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			return (Long)query.getSingleResult();
		    
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("SolicitudFacade.obtenerTotalFiltrado fallo", Level.SEVERE, re);
			throw re;
		}
	}

	public void registraEstadisticas(BigDecimal idToSolicitud,
			BigDecimal idToCotizacion, BigDecimal idToPoliza,
			BigDecimal numeroEndoso, String codigoUsuario, int tipoAccion) {
		
		EstadisticasEstadoDTO estadisticasEstadoDTO;
	    EstadisticasEstadoId estadisticasEstadoId;
	    switch (tipoAccion) {
	    case SistemaPersistencia.SE_CREA_SOLICITUD:
		if(idToSolicitud!=null){
        		estadisticasEstadoDTO= new EstadisticasEstadoDTO();
        		estadisticasEstadoId= new EstadisticasEstadoId();
        		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
        		estadisticasEstadoDTO.setId(estadisticasEstadoId);
        		estadisticasEstadoDTO.setIdBase1(idToSolicitud);
        		estadisticasEstadoDTO.setIdBase2(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setIdBase3(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setIdBase4(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
        		estadisticasEstadoFacade.save(estadisticasEstadoDTO);
		}
		break;
	    case SistemaPersistencia.SE_ASIGNA_SOL_A_ODT:
		if(idToSolicitud!=null && idToCotizacion!=null ){
        		estadisticasEstadoDTO= new EstadisticasEstadoDTO();
        		estadisticasEstadoId= new EstadisticasEstadoId();
        		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("20"));
        		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
        		estadisticasEstadoDTO.setId(estadisticasEstadoId);
        		estadisticasEstadoDTO.setIdBase1(idToSolicitud);
        		estadisticasEstadoDTO.setIdBase2(idToCotizacion);
        		estadisticasEstadoDTO.setIdBase3(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setIdBase4(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
        		estadisticasEstadoFacade.save(estadisticasEstadoDTO);
		}
		break;
	    case SistemaPersistencia.SE_ASIGNA_ODT_A_COT:
		if(idToSolicitud!=null && idToCotizacion!=null ){
        		estadisticasEstadoDTO= new EstadisticasEstadoDTO();
        		estadisticasEstadoId= new EstadisticasEstadoId();
        		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("30"));
        		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
        		estadisticasEstadoDTO.setId(estadisticasEstadoId);
        		estadisticasEstadoDTO.setIdBase1(idToSolicitud);
        		estadisticasEstadoDTO.setIdBase2(idToCotizacion);
        		estadisticasEstadoDTO.setIdBase3(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setIdBase4(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
        		estadisticasEstadoFacade.save(estadisticasEstadoDTO);
		}
		break;
	    case SistemaPersistencia.SE_ASIGNA_COT_A_EMISION:
		if(idToSolicitud!=null && idToCotizacion!=null ){
		    estadisticasEstadoDTO= estadisticasEstadoFacade.
    			buscarParaActualizarPorRetornoEstatus(idToSolicitud,idToCotizacion);
		    if(estadisticasEstadoDTO==null){
			estadisticasEstadoDTO= new EstadisticasEstadoDTO();
        		estadisticasEstadoId= new EstadisticasEstadoId();
        		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("40"));
        		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
        		estadisticasEstadoDTO.setId(estadisticasEstadoId);
        		estadisticasEstadoDTO.setIdBase1(idToSolicitud);
        		estadisticasEstadoDTO.setIdBase2(idToCotizacion);
        		estadisticasEstadoDTO.setIdBase3(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setIdBase4(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
        		estadisticasEstadoFacade.save(estadisticasEstadoDTO);
		    }else{
			estadisticasEstadoDTO.getId().setFechaHora(Calendar.getInstance().getTime());
			estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
			estadisticasEstadoFacade.update(estadisticasEstadoDTO);
		    }
		}
		break;
	    case SistemaPersistencia.SE_EMITE_COTIZACION:
		if(idToSolicitud!=null && idToCotizacion!=null && idToPoliza!=null 
			&& numeroEndoso!=null ){
        		estadisticasEstadoDTO= new EstadisticasEstadoDTO();
        		estadisticasEstadoId= new EstadisticasEstadoId();
        		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("50"));
        		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
        		estadisticasEstadoDTO.setId(estadisticasEstadoId);
        		estadisticasEstadoDTO.setIdBase1(idToSolicitud);
        		estadisticasEstadoDTO.setIdBase2(idToCotizacion);
        		estadisticasEstadoDTO.setIdBase3(idToPoliza);
        		estadisticasEstadoDTO.setIdBase4(numeroEndoso);
        		estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
        		estadisticasEstadoFacade.save(estadisticasEstadoDTO);
		}
		break;
		//para el caso de algunos endosos, y para cuando se Crea una solicitud a partir de Los Datos de una Poliza
	    case SistemaPersistencia.SE_ASIGNA_SOL_A_COT:
		if(idToSolicitud!=null && idToCotizacion!=null ){
		    	estadisticasEstadoDTO= new EstadisticasEstadoDTO();
        		estadisticasEstadoId= new EstadisticasEstadoId();
        		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("20"));
        		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
        		estadisticasEstadoDTO.setId(estadisticasEstadoId);
        		estadisticasEstadoDTO.setIdBase1(idToSolicitud);
        		estadisticasEstadoDTO.setIdBase2(idToCotizacion);
        		estadisticasEstadoDTO.setIdBase3(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setIdBase4(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
        		estadisticasEstadoFacade.save(estadisticasEstadoDTO);
        		estadisticasEstadoDTO.getId().setIdSubCodigoEstadistica(new BigDecimal("30"));        		
        		estadisticasEstadoFacade.save(estadisticasEstadoDTO);
		}
		break;
	     default:
		break;
	    }
		
	}

	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> listarFiltradoSolicitud(SolicitudDTO solicitudDTO, Date fechaInicial, Date fechaFinal) {
		try {
			String queryString = "select model from SolicitudDTO as model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"idToSolicitud", solicitudDTO.getIdToSolicitud());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombrePersona", solicitudDTO.getNombrePersona());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "apellidoPaterno", solicitudDTO.getApellidoPaterno());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "apellidoMaterno", solicitudDTO.getApellidoMaterno());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveEstatus", solicitudDTO.getClaveEstatus());
			//sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioCreacion", solicitudDTO.getCodigoUsuarioCreacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioModificacion", solicitudDTO.getCodigoUsuarioModificacion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombreAgente", solicitudDTO.getNombreAgente());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoAgente", solicitudDTO.getCodigoAgente());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idOficina", solicitudDTO.getIdOficina());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "codigoEjecutivo", solicitudDTO.getCodigoEjecutivo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveTipoPersona", solicitudDTO.getClaveTipoPersona());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveTipoSolicitud", solicitudDTO.getClaveTipoSolicitud());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigoUsuarioAsignacion", solicitudDTO.getCodigoUsuarioAsignacion());
			
			if(solicitudDTO.getProductoDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "productoDTO.idToProducto", solicitudDTO.getProductoDTO().getIdToProducto());
			}
			if(solicitudDTO.getNegocio() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "negocio.idToNegocio", solicitudDTO.getNegocio().getIdToNegocio());
			}
			if(solicitudDTO.getNegocio() != null && solicitudDTO.getNegocio().getClaveNegocio() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere,"negocio.claveNegocio", solicitudDTO.getNegocio().getClaveNegocio());
				sWhere.concat(" and model.negocio.idToNegocio IS NOT NULL") ;
			}		
			
			if(fechaInicial != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.fechaCreacion > :fechaInicial";
			}
			if(fechaFinal != null){
				if(sWhere.equals("")){
					sWhere += "";
				}else{
					sWhere += " and ";
				}
				sWhere += " model.fechaCreacion < :fechaFinal";
				
			}
			 
	
			if (Utilerias.esAtributoQueryValido(sWhere)){
				queryString = queryString.concat(" where ").concat(sWhere);
				queryString+=" order by model.fechaCreacion desc,model.idToSolicitud ";
			}else{
			    queryString+=" order by model.fechaCreacion desc,model.idToSolicitud ";
			}
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			if(fechaInicial != null){
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(fechaInicial);
				gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
				gcFechaInicio.set(GregorianCalendar.SECOND, 0);
				gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);	
				System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
				query.setParameter("fechaInicial", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
			}
			if(fechaFinal != null){
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(fechaFinal);
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				System.out.println("Poliza: Rango de Fecha fin ->" + gcFechaFin.getTime());
				query.setParameter("fechaFinal", gcFechaFin.getTime(), TemporalType.TIMESTAMP);					
			}
			
			if(solicitudDTO.getPrimerRegistroACargar() != null){
				query.setFirstResult(solicitudDTO.getPrimerRegistroACargar().intValue());
				
			}
			if(solicitudDTO.getNumeroMaximoRegistrosACargar() != null) {
				query.setMaxResults(solicitudDTO.getNumeroMaximoRegistrosACargar().intValue());
			}
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList(); 
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarPorIdFiltrado CoberturaCotizacionDTO failed",Level.SEVERE, re);
			throw re;
		}
	}
	
	
	/**
	 * Perform an initial save of a previously unsaved SolicitudDTO entity and return its id. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SolicitudDTO entity to persist
	 * @return 
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public BigDecimal saveAndGetId(SolicitudDTO entity) {
		LogDeMidasEJB3.log("saving SolicitudDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return (BigDecimal)entityManager.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity);	
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	private EstadisticasEstadoFacadeRemote estadisticasEstadoFacade;
	
	@EJB
	public void setEstadisticasEstadoFacade(
			EstadisticasEstadoFacadeRemote estadisticasEstadoFacade) {
		this.estadisticasEstadoFacade = estadisticasEstadoFacade;
	}

	@Override
	public List<SolicitudDTO> listarFilradoSolicitudGrid(
			SolicitudDTO solicitudDTO, Date fechaInicial, Date fechaFinal,Long idToNegocioProducto) {
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		Map<String,Object> queryParams = new HashMap<String, Object>();
		StringBuilder queryOrder = new StringBuilder();
		StringBuilder queryWhere = new StringBuilder();

		if (idToNegocioProducto != null) {
			NegocioProducto negocioProducto = entidadService.findByColumnsAndProperties(NegocioProducto.class,
					"productoDTO", null, null, new StringBuilder("model.idToNegProducto = " + idToNegocioProducto), null).get(0);
			solicitudDTO.setProductoDTO(new ProductoDTO());
			solicitudDTO.getProductoDTO().setIdToProducto(negocioProducto.getProductoDTO().getIdToProducto());
		}
		listarFilradoSolicitudGridSetParameters(params, queryParams, queryOrder, queryWhere, solicitudDTO, fechaInicial, fechaFinal);
		return entidadService.findByColumnsAndProperties(SolicitudDTO.class, COLUMNS, params, queryParams, queryWhere, queryOrder);
	}

	private void listarFilradoSolicitudGridSetParameters(
			Map<String, Object> params, Map<String, Object> queryParams,
			StringBuilder queryOrder, StringBuilder queryWhere,
			SolicitudDTO solicitudDTO, Date fechaInicial, Date fechaFinal) {
		// Adds parameters
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
			Agente agente = usuarioService.getAgenteUsuarioActual();
			Promotoria promotoria = agente.getPromotoria();
			String agenteSubquery = " and model.codigoAgente in (select a.id from Agente a where a.promotoria.id = " + promotoria.getId() + ")";
			queryWhere.append(agenteSubquery);
		} else if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			Agente agente = usuarioService.getAgenteUsuarioActual();
			solicitudDTO.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
		}
		
		params.put("idToSolicitud", solicitudDTO.getIdToSolicitud());
		params.put("nombrePersona", solicitudDTO.getNombrePersona());
		params.put("apellidoPaterno", solicitudDTO.getApellidoPaterno());
		params.put("apellidoMaterno", solicitudDTO.getApellidoMaterno());
		params.put("claveEstatus", solicitudDTO.getClaveEstatus());
		params.put("codigoUsuarioCreacion",solicitudDTO.getCodigoUsuarioCreacion());
		params.put("codigoUsuarioModificacion",solicitudDTO.getCodigoUsuarioModificacion());
		params.put("nombreAgente", solicitudDTO.getNombreAgente());
		params.put("codigoAgente", solicitudDTO.getCodigoAgente());
		params.put("idOficina", solicitudDTO.getIdOficina());
		params.put("codigoEjecutivo", solicitudDTO.getCodigoEjecutivo());
		params.put("claveTipoPersona", solicitudDTO.getClaveTipoPersona());
		params.put("claveTipoSolicitud", solicitudDTO.getClaveTipoSolicitud());
		//params.put("codigoUsuarioAsignacion",solicitudDTO.getCodigoUsuarioAsignacion());
		
		if(solicitudDTO.getCodigoUsuarioAsignacion() != null && !solicitudDTO.getCodigoUsuarioAsignacion().isEmpty()){
			queryWhere.append(" and (UPPER(model.codigoUsuarioAsignacion)  LIKE '%" + solicitudDTO.getCodigoUsuarioAsignacion().toUpperCase() + 
					"%' or  UPPER(model.codigoUsuarioCreacion) LIKE '%" + solicitudDTO.getCodigoUsuarioAsignacion().toUpperCase() + "%')") ;
		}

		if (solicitudDTO.getProductoDTO() != null) {
			params.put("productoDTO.idToProducto", solicitudDTO
					.getProductoDTO().getIdToProducto());
		}
		if (solicitudDTO.getNegocio() != null) {
			params.put("negocio.idToNegocio", solicitudDTO.getNegocio()
					.getIdToNegocio());
		}
		if (solicitudDTO.getNegocio() != null
				&& solicitudDTO.getNegocio().getClaveNegocio() != null) {
			params.put("negocio.claveNegocio", solicitudDTO.getNegocio()
					.getClaveNegocio());
			queryWhere.append(" and model.negocio.idToNegocio IS NOT NULL");
		}
		Map<String,Object> a = new LinkedHashMap<String, Object>();
		if (fechaInicial != null) {
			GregorianCalendar gcFechaInicio = new GregorianCalendar();
			gcFechaInicio.setTime(fechaInicial);
			gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
			gcFechaInicio.set(GregorianCalendar.SECOND, 0);
			gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
			queryWhere.append(" and model.fechaCreacion > '"
					+ Utilerias.cadenaDeFecha(gcFechaInicio.getTime(),
							"yyyy-MM-dd")+" 00:00:00.0'");
		}
		if (fechaFinal != null) {
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(fechaFinal);
			gcFechaFin.add(GregorianCalendar.DATE, 1);
			gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
			queryWhere.append(" and model.fechaCreacion < '"
					+ Utilerias.cadenaDeFecha(gcFechaFin.getTime(),SistemaPersistencia.FORMATO_FECHA_ORACLE) + "'");
		}
		// Add Order
		queryOrder
				.append(" order by model.fechaCreacion desc,model.idToSolicitud ");
		// Adds Query params
		if (solicitudDTO.getPrimerRegistroACargar() != null) {
			queryParams.put("firstResult",
					solicitudDTO.getPrimerRegistroACargar());
		}
		if (solicitudDTO.getNumeroMaximoRegistrosACargar() != null) {
			queryParams.put("maxResults",
					solicitudDTO.getNumeroMaximoRegistrosACargar());
		}
	}

	@Override
	public List<SolicitudDTO> listarFiltradoGrid(SolicitudDTO solicitudDTO) {
		Map<String,Object> params = new HashMap<String, Object>();
		StringBuilder queryWhere = new StringBuilder();
		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
			//Limitado a los agentes de la promotoria.
			Agente agente = usuarioService.getAgenteUsuarioActual();
			Promotoria promotoria = agente.getPromotoria();
			String agenteSubquery = " and model.codigoAgente in (select a.id from Agente a where a.promotoria.id = " + promotoria.getId() + ")";
			queryWhere.append(agenteSubquery);
		} else if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			Agente agente = usuarioService.getAgenteUsuarioActual();
			solicitudDTO.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
		}
				
		if (solicitudDTO.getCodigoAgente() != null) {
			queryWhere.append(" and model.codigoAgente = " + solicitudDTO.getCodigoAgente());
		}
		
		if (solicitudDTO.getCodigoUsuarioAsignacion() != null) {
			queryWhere.append(" and model.codigoUsuarioAsignacion LIKE '%"+solicitudDTO.getCodigoUsuarioAsignacion() +"%'");
		}
		if (solicitudDTO.getCodigoUsuarioCreacion() != null) {
			queryWhere.append(" and model.codigoUsuarioCreacion LIKE '%"+solicitudDTO.getCodigoUsuarioCreacion() +"%'");
		}
		
		
		GregorianCalendar gcFechaInicio = new GregorianCalendar();
		gcFechaInicio.setTime(solicitudDTO.getFechaCreacion());
		gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
		gcFechaInicio.set(GregorianCalendar.SECOND, 0);
		gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
		
		GregorianCalendar gcFechaFin = new GregorianCalendar();
		gcFechaFin.setTime(gcFechaInicio.getTime());
		gcFechaFin.add(GregorianCalendar.DATE, 1);
		gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
		
		queryWhere.append(" and model.fechaCreacion BETWEEN '" +  Utilerias.cadenaDeFecha(gcFechaInicio.getTime(), SistemaPersistencia.FORMATO_FECHA_ORACLE) + "' AND '" +
				Utilerias.cadenaDeFecha(gcFechaFin.getTime(), SistemaPersistencia.FORMATO_FECHA_ORACLE)+"'");
		params.put("negocio.claveNegocio", SolicitudPolizaServiceImpl.NEGOCIO_AUTOS);
		queryWhere.append(" and model.claveEstatus IN ("
				+ SolicitudDTO.Estatus.PROCESO.getEstatus() + ","
				+ SolicitudDTO.Estatus.PROCESO_INCOMPLETO.getEstatus() + ")");
		return entidadService.findByColumnsAndProperties(SolicitudDTO.class, SolicitudFacade.COLUMNS, params, new HashMap<String, Object>(), queryWhere, new StringBuilder(
		"order by model.fechaCreacion desc,model.idToSolicitud"));
	}

	@Override
	public Long listarFilradoSolicitudGridCount(
			SolicitudDTO solicitudDTO, Date fechaInicial, Date fechaFinal,
			Long idToNegocioProducto) {
		Map<String,Object> params = new HashMap<String, Object>();
		StringBuilder queryWhere = new StringBuilder();
		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual") && 
				!usuarioService.tienePermisoUsuarioActual("FN_M2_Cabina_Visibilidad_Consulta")) {
			
			if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
				//Limitado a los agentes de la promotoria.
				Agente agente = usuarioService.getAgenteUsuarioActual();
				Promotoria promotoria = agente.getPromotoria();
				String agenteSubquery = " and model.codigoAgente in (select a.id from Agente a where a.promotoria.id = " + promotoria.getId() + ")";
				queryWhere.append(agenteSubquery);				
			} else if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
				Agente agente = usuarioService.getAgenteUsuarioActual();
				solicitudDTO.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
			}
			
			if (solicitudDTO.getCodigoUsuarioCreacion() != null) {
				queryWhere.append(" and model.codigoUsuarioCreacion LIKE '%"+solicitudDTO.getCodigoUsuarioCreacion() +"%'");
			}
		}
		
		
		if (solicitudDTO.getCodigoAgente() != null) {
			queryWhere.append(" and model.codigoAgente = " + solicitudDTO.getCodigoAgente());
		}
		
		
		if (solicitudDTO.getCodigoUsuarioAsignacion() != null) {
			queryWhere.append(" and model.codigoUsuarioAsignacion LIKE '%"+solicitudDTO.getCodigoUsuarioAsignacion() +"%'");
		}		
		
		GregorianCalendar gcFechaInicio = new GregorianCalendar();
		gcFechaInicio.setTime(solicitudDTO.getFechaCreacion());
		gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
		gcFechaInicio.set(GregorianCalendar.SECOND, 0);
		gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
		
		GregorianCalendar gcFechaFin = new GregorianCalendar();
		gcFechaFin.setTime(gcFechaInicio.getTime());
		gcFechaFin.add(GregorianCalendar.DATE, 1);
		gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
		
		queryWhere.append(" and model.fechaCreacion BETWEEN '" +  Utilerias.cadenaDeFecha(gcFechaInicio.getTime(), SistemaPersistencia.FORMATO_FECHA_ORACLE) + "' AND '" +
				Utilerias.cadenaDeFecha(gcFechaFin.getTime(), SistemaPersistencia.FORMATO_FECHA_ORACLE)+"'");
		params.put("negocio.claveNegocio", SolicitudPolizaServiceImpl.NEGOCIO_AUTOS);
		queryWhere.append(" and model.claveEstatus IN ("
				+ SolicitudDTO.Estatus.PROCESO.getEstatus() + ","
				+ SolicitudDTO.Estatus.PROCESO_INCOMPLETO.getEstatus() + ")");
		return entidadService.findByColumnsAndPropertiesCount(SolicitudDTO.class, params, new HashMap<String, Object>(), queryWhere, null);
	}
}