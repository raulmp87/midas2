package mx.com.afirme.midas.solicitud.documento;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity DocumentoDigitalSolicitudDTO.
 * 
 * @see .DocumentoDigitalSolicitudDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class DocumentoDigitalSolicitudFacade implements
		DocumentoDigitalSolicitudFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * DocumentoDigitalSolicitudDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            DocumentoDigitalSolicitudDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DocumentoDigitalSolicitudDTO entity) {
		LogDeMidasEJB3.log("saving DocumentoDigitalSolicitudDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent DocumentoDigitalSolicitudDTO entity.
	 * 
	 * @param entity
	 *            DocumentoDigitalSolicitudDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DocumentoDigitalSolicitudDTO entity) {
		LogDeMidasEJB3.log("deleting DocumentoDigitalSolicitudDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					DocumentoDigitalSolicitudDTO.class, entity
							.getIdToDocumentoDigitalSolicitud());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved DocumentoDigitalSolicitudDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * DocumentoDigitalSolicitudDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            DocumentoDigitalSolicitudDTO entity to update
	 * @return DocumentoDigitalSolicitudDTO the persisted
	 *         DocumentoDigitalSolicitudDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DocumentoDigitalSolicitudDTO update(
			DocumentoDigitalSolicitudDTO entity) {
		LogDeMidasEJB3.log("updating DocumentoDigitalSolicitudDTO instance",
				Level.INFO, null);
		try {
			DocumentoDigitalSolicitudDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DocumentoDigitalSolicitudDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding DocumentoDigitalSolicitudDTO instance with id: " + id,
				Level.INFO, null);
		try {
			DocumentoDigitalSolicitudDTO instance = entityManager.find(
					DocumentoDigitalSolicitudDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DocumentoDigitalSolicitudDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the DocumentoDigitalSolicitudDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DocumentoDigitalSolicitudDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<DocumentoDigitalSolicitudDTO> findByProperty(
			String propertyName, final Object value) {
		LogDeMidasEJB3.log(
				"finding DocumentoDigitalSolicitudDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from DocumentoDigitalSolicitudDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DocumentoDigitalSolicitudDTO entities.
	 * 
	 * @return List<DocumentoDigitalSolicitudDTO> all
	 *         DocumentoDigitalSolicitudDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DocumentoDigitalSolicitudDTO> findAll() {
		LogDeMidasEJB3.log(
				"finding all DocumentoDigitalSolicitudDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from DocumentoDigitalSolicitudDTO model";
			Query query = entityManager.createQuery(queryString);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}