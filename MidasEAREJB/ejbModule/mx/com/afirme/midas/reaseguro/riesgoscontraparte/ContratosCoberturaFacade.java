package mx.com.afirme.midas.reaseguro.riesgoscontraparte;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity ContratosCoberturaDTO.
 * 
 * @see .ContratosCoberturaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ContratosCoberturaFacade implements
	ContratosCoberturaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * ContratosCoberturaDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ContratosCoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ContratosCoberturaDTO entity) {
		LogDeMidasEJB3.log("saving ContratosCoberturaDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ContratosCoberturaDTO entity.
	 * 
	 * @param entity
	 *            ContratosCoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ContratosCoberturaDTO entity) {
		LogDeMidasEJB3.log("deleting ContratosCoberturaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					ContratosCoberturaDTO.class, entity
							.getIdcontrato());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ContratosCoberturaDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * ContratosCoberturaDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            ContratosCoberturaDTO entity to update
	 * @return ContratosCoberturaDTO the persisted
	 *         ContratosCoberturaDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ContratosCoberturaDTO update(
			ContratosCoberturaDTO entity) {
		LogDeMidasEJB3.log("updating ContratosCoberturaDTO instance",
				Level.INFO, null);
		try {
			ContratosCoberturaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ContratosCoberturaDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding ContratosCoberturaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			ContratosCoberturaDTO instance = entityManager.find(
					ContratosCoberturaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ContratosCoberturaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the ContratosCoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ContratosCoberturaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ContratosCoberturaDTO> findByProperty(
			String propertyName, final Object value) {
		LogDeMidasEJB3.log(
				"finding ContratosCoberturaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ContratosCoberturaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value+"");
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ContratosCoberturaDTO entities.
	 * 
	 * @return List<ContratosCoberturaDTO> all
	 *         ContratosCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ContratosCoberturaDTO> findAll() {
		LogDeMidasEJB3.log(
				"finding all ContratosCoberturaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from ContratosCoberturaDTO model order by model.fechaCorte desc";
			Query query = entityManager.createQuery(queryString);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}