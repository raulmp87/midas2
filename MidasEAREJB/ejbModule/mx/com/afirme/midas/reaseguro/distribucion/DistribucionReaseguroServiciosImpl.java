package mx.com.afirme.midas.reaseguro.distribucion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoFacadeRemote;
import mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo.ParticipacionCorredorFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;
import mx.com.afirme.midas.contratos.linea.CalculoEjerciciosDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.contratos.linea.LineaFacadeRemote;
import mx.com.afirme.midas.contratos.lineaparticipacion.LineaParticipacionDTO;
import mx.com.afirme.midas.contratos.lineaparticipacion.LineaParticipacionFacadeRemote;
import mx.com.afirme.midas.contratos.lineaparticipacion.LineaParticipacionId;
import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDetalleDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroServiciosRemote;
import mx.com.afirme.midas.contratos.movimiento.TipoMovimientoDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionFacadeRemote;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorFacadeRemote;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaFacadeRemote;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroFacadeRemote;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.temporizador.TemporizadorDistribuirPolizaFacadeRemote;

@Stateless
public class DistribucionReaseguroServiciosImpl implements DistribucionReaseguroServicios {

//	@TransactionAttribute(TransactionAttributeType.REQUIRED)
//	public List<MovimientoReaseguroDTO> distribuirPoliza(SoporteReaseguroDTO soporteReaseguroDTO) {
//		/**
//		 * 21/04/2010 Jose Luis Arellano. Modificaci�n al m�todo para que se distribuya linea por linea de forma transaccional
//		 */
//		List<MovimientoReaseguroDTO> listaMovimientosReaseguroDTO = new ArrayList<MovimientoReaseguroDTO>();
//		for(LineaSoporteReaseguroDTO lineaSoporteReaseguro : soporteReaseguroDTO.getLineaSoporteReaseguroDTOs()){
//				List<MovimientoReaseguroDTO> listaMovimientosReaseguro = distribuirLineaSoporteReaseguro(lineaSoporteReaseguro);
//				listaMovimientosReaseguroDTO.addAll(listaMovimientosReaseguro);
//		}
//		//Una vez distribuida la poliza, se procede a contabilizarla
//		try {
//			String identificador = soporteReaseguroDTO.getIdToPoliza() + "|" + soporteReaseguroDTO.getNumeroEndoso();
//
//			if (listaMovimientosReaseguroDTO != null && listaMovimientosReaseguroDTO.size() > 0) {
//				LogDeMidasEJB3.log("Se procede a contabilizar los movimientos para el idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza()+", numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso(), Level.INFO, null);
//				movimientoReaseguroServicios.registrarContabilidad(identificador, "REASEGURO-EMIS", listaMovimientosReaseguroDTO);
//				LogDeMidasEJB3.log("Se contabilizaron satisfactoriamente los movimientos para el idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza(), Level.INFO, null);
//			} else {
//				LogDeMidasEJB3.log("No se generaron Movimientos, Tampoco se requirio contabilizar para la P�liza de idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza(), Level.INFO, null);
//			}
//		} catch (Exception ex) {
//			LogDeMidasEJB3.log("NO SE PUDIERON CONTABILIZAR LOS MOVIMIENTOS PARA idToPoliza: "+ soporteReaseguroDTO.getIdToPoliza()+", numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso()
//					+"LA DISTRIBUCION FUE EXITOSA.", Level.INFO, null);
//		}
		
//		List<MovimientoReaseguroDTO> listaMovimientosReaseguroDTO = generarMovimientosReaseguro(soporteReaseguroDTO.getLineaSoporteReaseguroDTOs());
//		listaMovimientosReaseguroDTO = movimientoReaseguroServicios.registrarMovimientos(listaMovimientosReaseguroDTO);
//		LogDeMidasEJB3.log("Se registraron los movimientos de Distribucion de la Prima con exito para elIdToSoporteReaseguro: "+ soporteReaseguroDTO.getIdToSoporteReaseguro(),Level.INFO, null);
//		lineaSoporteReaseguroFacade.update(soporteReaseguroDTO.getLineaSoporteReaseguroDTOs()); // Actualizar los estatus de todas las LineaSoporteReaseguroDTO.
//		LogDeMidasEJB3.log("Se actualizaron los estatus de LineaSoporteReaseguroDTO para el "+ "IdToSoporteReaseguro: " + soporteReaseguroDTO.getIdToSoporteReaseguro(),Level.INFO, null);
//		return listaMovimientosReaseguroDTO;
//	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void distribuirLineaSoporteReaseguro(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) {
		LogDeMidasEJB3.log("Generando movimientos de reaseguro para el IdToSoporteReaseguro: "+ lineaSoporteReaseguroDTO.getId().getIdToSoporteReaseguro()
				+", idTmLineaSoporteReaseguro: "+lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro(),Level.INFO, null);
		List<LineaSoporteReaseguroDTO> listaLinea = new ArrayList<LineaSoporteReaseguroDTO>();
		listaLinea.add(lineaSoporteReaseguroDTO);
		List<MovimientoReaseguroDTO> listaMovimientosReaseguroDTO = generarMovimientosReaseguro(listaLinea);
		listaMovimientosReaseguroDTO = movimientoReaseguroServicios.registrarMovimientos(listaMovimientosReaseguroDTO);
		LogDeMidasEJB3.log("Se registraron los movimientos de Distribucion de la Prima con exito para elIdToSoporteReaseguro: "+ 
				lineaSoporteReaseguroDTO.getId().getIdToSoporteReaseguro()+", idTmLineaSoporteReaseguro: "+lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro()+", Cantidad de movimientos: "+(listaMovimientosReaseguroDTO != null?listaMovimientosReaseguroDTO.size():0),Level.INFO, null);
		lineaSoporteReaseguroFacade.update(lineaSoporteReaseguroDTO); // Actualizar los estatus de todas las LineaSoporteReaseguroDTO.
		LogDeMidasEJB3.log("Se actualiz� el estatus de LineaSoporteReaseguroDTO para el IdToSoporteReaseguro: " + 
				lineaSoporteReaseguroDTO.getId().getIdToSoporteReaseguro()+", idTmLineaSoporteReaseguro: "+lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro(),Level.INFO, null);
//		return listaMovimientosReaseguroDTO;
	}

//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
//	public List<MovimientoReaseguroDTO> distribuirPoliza(BigDecimal idToPoliza,Integer numeroEndoso) {
//		SoporteReaseguroDTO soporteReaseguroDTO = soporteReaseguroFacade.obtenerSoporteReaseguroPorPolizaEndoso(idToPoliza,numeroEndoso);
//		List<MovimientoReaseguroDTO> listaMovimientosReaseguroDTO = this.distribuirPoliza(soporteReaseguroDTO);
//		return listaMovimientosReaseguroDTO;
//	}

	/**
	 * Este metodo distribuye las primas para todas las LineasSoporteCobertura
	 * que tengan el estatus 2.
	 * 
	 */
	public void distribuirPrimas() {
		List<SoporteReaseguroDTO> soporteReaseguroDTOs = soporteReaseguroFacade.obtenerSoporteReaseguroPorEstatusLineas(22);
		//TODO Aram hay que regresar al estatus 2 del 22 cuando el sistema se termine de ajustar
		LogDeMidasEJB3.log("SE INICIAN "+soporteReaseguroDTOs.size()+" TEMPORIZADORE(S) PARA  DISTRIBUCION MASIVA DE POLIZAS",Level.INFO, null);
		for (SoporteReaseguroDTO soporteReaseguroDTO : soporteReaseguroDTOs) {
			try {
				temporizadorDistribuirPolizaFacadeRemote.iniciarTemporizador(1,soporteReaseguroDTO);
			} catch (Exception ex) {
				LogDeMidasEJB3.log("ERROR AL INICAR TEMPORIZADOR PARA idToSoporteReaseguro: "
						+ soporteReaseguroDTO.getIdToSoporteReaseguro()+ " idToPoliza: " +soporteReaseguroDTO.getIdToPoliza(),
						Level.SEVERE, ex);
			}
		}
		LogDeMidasEJB3.log("SE TERMINO DE INICIAR "+soporteReaseguroDTOs.size()+" TEMPORIZADORE(S) PARA DISTRIBUCION MASIVA DE POLIZAS",
				Level.INFO, null);
		
	}

//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
//	public void distribuirYContabilizar(BigDecimal idToPoliza,Integer numeroEndoso) {
//		List<MovimientoReaseguroDTO> listaMovimientos = distribucionReaseguroServicios.distribuirPoliza(idToPoliza, numeroEndoso);
//		LogDeMidasEJB3.log("Se termino el proceso de distribucion para el idToPoliza: "+ idToPoliza + " numeroEndoso: " + numeroEndoso + ", se procede a la contabilizacion de estos movimientos.",Level.INFO, null);
//
//		/*
//		 * Contabilizar
//		 */
//		try {
//			String identificador = idToPoliza + "|" + numeroEndoso;
//
//			if (listaMovimientos != null && listaMovimientos.size() > 0) {
//				movimientoReaseguroServicios.registrarContabilidad(identificador, "REASEGURO-EMIS", listaMovimientos);
//				LogDeMidasEJB3.log("Se contabilizaron satisfactoriamente los movimientos para el idToPoliza: "+ idToPoliza, Level.INFO, null);
//			} else {
//				LogDeMidasEJB3.log("No se generaron Movimientos, Tampoco se requirio contabilizar para la P�liza de idToPoliza: "+ idToPoliza, Level.INFO, null);
//			}
//		} catch (Exception ex) {
//			LogDeMidasEJB3.log("NO SE PUDIERON CONTABILIZAR LOS MOVIMIENTOS PARA idToPoliza: "+ idToPoliza, Level.INFO, null);
//		}
//	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<MovimientoReaseguroDTO> distribuirMontoSiniestro(
			BigDecimal idToPoliza, Integer numeroEndoso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			Integer numeroInciso, Integer numeroSubInciso,
			Integer conceptoMovimiento, BigDecimal idToMoneda,
			Date fechaMovimiento, BigDecimal idMovimientoSiniestro,
			BigDecimal montoMovimiento, Integer tipoMovimiento,
			BigDecimal idToReporteSiniestro,
			BigDecimal idToDistribucionMovSiniestro) {
		
		List<MovimientoReaseguroDTO> listaMovimientosReaseguroDTO = null;
		//Validar si el movimiento de siniestros no ha sido distribuido.
		DistribucionMovSiniestroDTO distribucionMovSiniestroDTO = distribucionMovSiniestroFacadeRemote.findById(idToDistribucionMovSiniestro);
		if(distribucionMovSiniestroDTO != null){
			if(distribucionMovSiniestroDTO.getEstatus() != null && 
					distribucionMovSiniestroDTO.getEstatus().intValue() == DistribucionMovSiniestroDTO.ESTATUS_SIN_DISTRIBUIR){
				
				//Se procede con la distribuci�n del siniestro.
				// Obtener la LineaSoporteCobertura
				LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO = obtenerLineaSoporteCobertura(idToPoliza, numeroEndoso,idToSeccion, idToCobertura, numeroInciso,numeroSubInciso);
				// Generar movimientos para esta cobertura
				Integer tipoMovimientoReaseguro = (tipoMovimiento == TipoMovimientoDTO.CUENTAS_POR_COBRAR) ? 
													TipoMovimientoDTO.CUENTAS_POR_PAGAR : TipoMovimientoDTO.CUENTAS_POR_COBRAR; // Invertir el tipo de movimiento
				listaMovimientosReaseguroDTO = generarMovimientosSiniestros(lineaSoporteCoberturaDTO, idToReporteSiniestro,
						idMovimientoSiniestro, montoMovimiento, conceptoMovimiento,tipoMovimientoReaseguro,fechaMovimiento);

				// Registrar estos movimientos
				listaMovimientosReaseguroDTO = movimientoReaseguroServicios.registrarMovimientos(listaMovimientosReaseguroDTO);

				LogDeMidasEJB3.log("Se registraron los movimientos de Distribucion del Siniestro IdToReporteSiniestro: "+ idToReporteSiniestro+
						" para la Poliza: "+ idToPoliza+" endoso:"+numeroEndoso+" y la idToCobertura: "+ idToCobertura+" idToDistribucionMovSiniestro: "+idToDistribucionMovSiniestro, Level.INFO, null);

				actualizarEstatusMovimiento(distribucionMovSiniestroDTO);
			}
			else{
				LogDeMidasEJB3.log("Se recibi� un movimiento de sinietro para distribuir, pero su estatus indica que ya ha sido distribuido. idToDistribucionMovSiniestro: "+idToDistribucionMovSiniestro, Level.WARNING, null);
			}
		}
		else{
			LogDeMidasEJB3.log("Se recibi� un movimiento de sinietro para distribuir, no se encontr� el registro DistribucionMovSiniestroDTO. idToDistribucionMovSiniestro: "+idToDistribucionMovSiniestro, Level.WARNING, null);
		}
		
		return listaMovimientosReaseguroDTO;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void distribuirCobertura(BigDecimal idToPoliza,
			Integer numeroEndoso, BigDecimal idToSeccion,
			BigDecimal idToCobertura, Integer numeroInciso,
			Integer numeroSubInciso) {

		// Crear la lista de movimientos
		List<MovimientoReaseguroDTO> listaMovimientosReaseguroDTO = new ArrayList<MovimientoReaseguroDTO>();

		// Obtener la LineaSoporteCobertura actual y sus participaciones
		LineaSoporteCoberturaDTO lineaSoporteCoberturaActual = this
				.obtenerLineaSoporteCobertura(idToPoliza, numeroEndoso,
						idToSeccion, idToCobertura, numeroInciso,
						numeroSubInciso);
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = lineaSoporteCoberturaActual
				.getLineaSoporteReaseguroDTO();
		List<Participacion> listaParticipacionCP = getListaParticipaciones(
				lineaSoporteReaseguroDTO.getLineaDTO(),
				TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE);
		List<Participacion> listaParticipacion1E = getListaParticipaciones(
				lineaSoporteReaseguroDTO.getLineaDTO(),
				TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE);

		generarMovimientosReaseguro(lineaSoporteCoberturaActual, listaMovimientosReaseguroDTO, 
				listaParticipacionCP, listaParticipacion1E);

		// Guardar los movimientos
		movimientoReaseguroServicios
				.registrarMovimientos(listaMovimientosReaseguroDTO);
		LogDeMidasEJB3.log(
				"Se registraron los movimientos correctamente para la idToPoliza: "
						+ idToPoliza + " numeroEndoso: " + numeroEndoso
						+ " idToSeccion: " + idToSeccion + " idToCobertura: "
						+ idToCobertura + " numeroInciso: " + numeroInciso
						+ " numeroSubInciso: " + numeroSubInciso, Level.INFO,
				null);
	}

	public LineaSoporteReaseguroDTO getPorcentajeDistribucion(BigDecimal idToPoliza, Integer numeroEndoso,BigDecimal idToSeccion, BigDecimal idToCobertura,Integer numeroInciso, Integer numeroSubInciso) {
		BigDecimal tipoDistribucion = lineaFacade.obtenerTipoDistribucion(idToPoliza, idToCobertura);
		if(tipoDistribucion != null){
			if(tipoDistribucion.compareTo(BigDecimal.ONE) == 0){
//				numeroInciso = null;
				numeroSubInciso = null;
			} else if(tipoDistribucion.compareTo(BigDecimal.valueOf(2d)) == 0){
				numeroSubInciso = null;
			}
		}
		LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO = new LineaSoporteCoberturaDTO();
		int coincidencias = 0;
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = null;

		lineaSoporteCoberturaDTO.setIdToPoliza(idToPoliza);
		if (numeroInciso != null && !numeroInciso.equals(new Integer(0))) {
			lineaSoporteCoberturaDTO.setNumeroInciso(new BigDecimal(numeroInciso));
		}

		if (numeroSubInciso != null && !numeroSubInciso.equals(new Integer(0))) {
			lineaSoporteCoberturaDTO.setNumeroSubInciso(new BigDecimal(
					numeroSubInciso));
		}
		lineaSoporteCoberturaDTO.setIdToCobertura(idToCobertura);
		lineaSoporteCoberturaDTO.setIdToSeccion(idToSeccion);

		List<LineaSoporteCoberturaDTO> lineaSoporteCoberturaDTOs;

		lineaSoporteCoberturaDTOs = lineaSoporteCoberturaFacade.obtenPorLlaveCobertura(lineaSoporteCoberturaDTO);
		if (lineaSoporteCoberturaDTOs != null && lineaSoporteCoberturaDTOs.size() > 0) {
			for (LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO2 : lineaSoporteCoberturaDTOs) {
				if (lineaSoporteCoberturaDTO2.getLineaSoporteReaseguroDTO() != null) {
					if (lineaSoporteCoberturaDTO2.getLineaSoporteReaseguroDTO().getSoporteReaseguroDTO() != null) {
						if (numeroEndoso != null) {
							if (lineaSoporteCoberturaDTO2.getLineaSoporteReaseguroDTO().getSoporteReaseguroDTO().getNumeroEndoso().intValue() == numeroEndoso.intValue()) {
								coincidencias = coincidencias + 1;
								lineaSoporteReaseguroDTO = lineaSoporteCoberturaDTO2.getLineaSoporteReaseguroDTO();
							}
						}
					}
				}
			}
		}

		if (coincidencias != 1) {
			lineaSoporteReaseguroDTO = null;
		}
		
		return lineaSoporteReaseguroDTO;
	}

	private void actualizarEstatusMovimiento(DistribucionMovSiniestroDTO distribucionMovSiniestroDTO) {
		
		if (distribucionMovSiniestroDTO != null) {
			distribucionMovSiniestroDTO.setEstatus(DistribucionMovSiniestroDTO.ESTATUS_DISTRIBUIDO);
			distribucionMovSiniestroDTO.setNotaDelSistema(DistribucionMovSiniestroDTO.NOTA_SISTEM_DISTRIBUIDO);
			distribucionMovSiniestroFacadeRemote.update(distribucionMovSiniestroDTO);
		}
	}

	/**
	 * Este metodo lo que hace es generar un MovimientoReaseguroDTO con todos
	 * los datos que se le estan enviando.
	 * 
	 * @return a MovimientoReaseguroDTO que es el que se genero.
	 */
	private MovimientoReaseguroDTO generarMovimientoReaseguro(
			LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO,
			int conceptoMovimiento, int tipoMovimiento, int tipoReaseguro,
			BigDecimal cantidad,
			ReaseguradorCorredorDTO reaseguradorCorredorDTO,
			ReaseguradorCorredorDTO corredorDTO,
			ContratoFacultativoDTO contratoFacultativoDTO,
			BigDecimal idToReporteSiniestro,
			BigDecimal idToMovimientoSiniestro, Double tipoCambio,Date fechaMovimientoSiniestro) {
		MovimientoReaseguroDTO movimientoReaseguroDTO = new MovimientoReaseguroDTO();
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = lineaSoporteCoberturaDTO.getLineaSoporteReaseguroDTO();
		int ejercicioLineaReaseguro = CalculoEjerciciosDTO.obtenerEjercicio(lineaSoporteReaseguroDTO.getLineaDTO().getFechaInicial());

		movimientoReaseguroDTO.setConceptoMovimiento(conceptoMovimiento); // Comisi�n sobre Prima Cedida
		movimientoReaseguroDTO.setEjercicio(ejercicioLineaReaseguro);
		movimientoReaseguroDTO.setIdCobertura(lineaSoporteCoberturaDTO.getIdToCobertura());
		movimientoReaseguroDTO.setTipoReaseguro(tipoReaseguro);
		movimientoReaseguroDTO.setTipoMovimiento(tipoMovimiento);
		movimientoReaseguroDTO.setCantidad(cantidad);
		movimientoReaseguroDTO.setIdMoneda(lineaSoporteCoberturaDTO.getIdMoneda().intValue());
		movimientoReaseguroDTO.setFechaRegistro(fechaMovimientoSiniestro != null ? fechaMovimientoSiniestro : lineaSoporteReaseguroDTO.getFechaEmision());
		movimientoReaseguroDTO.setIdPoliza(lineaSoporteCoberturaDTO.getIdToPoliza());
		movimientoReaseguroDTO.setIdInciso(lineaSoporteCoberturaDTO.getNumeroInciso());
		movimientoReaseguroDTO.setIdSubInciso(lineaSoporteCoberturaDTO.getNumeroSubInciso());
		movimientoReaseguroDTO.setIdSeccion(lineaSoporteCoberturaDTO.getIdToSeccion());
		movimientoReaseguroDTO.setLineaDTO(lineaSoporteReaseguroDTO.getLineaDTO());
		movimientoReaseguroDTO.setNumeroEndoso(lineaSoporteReaseguroDTO.getSoporteReaseguroDTO().getNumeroEndoso());
		movimientoReaseguroDTO.setSubRamo(lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo());
		// TODO ADVERTENCIA en el uso del id de Moneda en PESOS MEXICANOS
		if (lineaSoporteCoberturaDTO.getIdMoneda().intValue() == 484) {
			movimientoReaseguroDTO.setTipoCambio(new Double(1));
		} else {
			movimientoReaseguroDTO.setTipoCambio(tipoCambio);
		}
		movimientoReaseguroDTO.setAcumulado(1);

		if (idToReporteSiniestro != null) {
			// ReporteSiniestroDTO reporteSiniestroDTO = new
			// ReporteSiniestroDTO();
			// reporteSiniestroDTO.setIdToReporteSiniestro(idToReporteSiniestro);
			// movimientoReaseguroDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
			movimientoReaseguroDTO.setIdToReporteSiniestro(idToReporteSiniestro);
			movimientoReaseguroDTO.setIdToMovimientoSiniestro(idToMovimientoSiniestro);
		}

		movimientoReaseguroDTO.setReaseguradorCorredor(reaseguradorCorredorDTO);
		movimientoReaseguroDTO.setCorredor(corredorDTO);
		if (tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE) {
			movimientoReaseguroDTO.setContratoCuotaParteDTO(lineaSoporteReaseguroDTO.getLineaDTO().getContratoCuotaParte());
		} else if (tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE) {
			movimientoReaseguroDTO.setContratoPrimerExcedenteDTO(lineaSoporteReaseguroDTO.getLineaDTO().getContratoPrimerExcedente());
		} else if (tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO) {
			movimientoReaseguroDTO.setContratoFacultativoDTO(contratoFacultativoDTO);
		}

		return movimientoReaseguroDTO;
	}

	private List<MovimientoReaseguroDTO> generarMovimientosSiniestros(
			LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO,
			BigDecimal idToReporteSiniestro,
			BigDecimal idToMovimientoSiniestro, BigDecimal montoMovimiento,
			Integer conceptoMovimiento, Integer tipoMovimiento,Date fechaMovimientoSiniestro) {
		List<MovimientoReaseguroDTO> listaMovimientosReaseguroDTO = new ArrayList<MovimientoReaseguroDTO>();

		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = lineaSoporteCoberturaDTO.getLineaSoporteReaseguroDTO();
		List<Participacion> listaParticipacionCP = getListaParticipaciones(lineaSoporteReaseguroDTO.getLineaDTO(),TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE);
		List<Participacion> listaParticipacion1E = getListaParticipaciones(lineaSoporteReaseguroDTO.getLineaDTO(),TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE);
		lineaSoporteReaseguroDTO.setEstatusLineaSoporte(LineaSoporteReaseguroDTO.EMITIDO_Y_DISTRIBUIDO);
		lineaSoporteReaseguroDTO.setNotaDelSistema(EMITIDO_Y_DISTRIBUIDO_NOTA);

		listaMovimientosReaseguroDTO = agregarMovimientosPorTipoContrato(lineaSoporteCoberturaDTO, TipoReaseguroDTO.TIPO_RETENCION,
				listaMovimientosReaseguroDTO, null, idToReporteSiniestro,
				idToMovimientoSiniestro, montoMovimiento, conceptoMovimiento,
				tipoMovimiento,fechaMovimientoSiniestro);
		listaMovimientosReaseguroDTO = agregarMovimientosPorTipoContrato(lineaSoporteCoberturaDTO,TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE,
				listaMovimientosReaseguroDTO, listaParticipacionCP,
				idToReporteSiniestro, idToMovimientoSiniestro, montoMovimiento,
				conceptoMovimiento, tipoMovimiento,fechaMovimientoSiniestro);
		listaMovimientosReaseguroDTO = agregarMovimientosPorTipoContrato(lineaSoporteCoberturaDTO,TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE,
				listaMovimientosReaseguroDTO, listaParticipacion1E,
				idToReporteSiniestro, idToMovimientoSiniestro, montoMovimiento,
				conceptoMovimiento, tipoMovimiento,fechaMovimientoSiniestro);
		listaMovimientosReaseguroDTO = agregarMovimientosPorTipoContrato(lineaSoporteCoberturaDTO,TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO,
				listaMovimientosReaseguroDTO, null, idToReporteSiniestro,
				idToMovimientoSiniestro, montoMovimiento, conceptoMovimiento,
				tipoMovimiento,fechaMovimientoSiniestro);
		return listaMovimientosReaseguroDTO;
	}

	/**
	 * Metodo que esconde la llamada al metodo que realiza realmente la tarea. De esta manera no tenemos que enviar
	 * parametros extras, solamente los requeridos. Se utiliza para agregar los movimientos en la distribucion de la prima.
	 * @param lineaSoporteCoberturaDTO
	 * @param tipoReaseguro
	 * @param movimientos
	 * @param participaciones
	 * @return
	 */
	private List<MovimientoReaseguroDTO> agregarMovimientosPorTipoContrato(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO,int tipoReaseguro, 
			List<MovimientoReaseguroDTO> movimientos,List<Participacion> participaciones) {
		return this.agregarMovimientosPorTipoContrato(lineaSoporteCoberturaDTO,tipoReaseguro, movimientos, participaciones, false, null, null,null, null, null,null);
	}
	
	/**
	 * Metodo que esconde la llamada al metodo que realiza realmente la tarea. De esta manera no tenemos que enviar
	 * paramaetros extras, solamente los requeridos. Se utiliza para agregar los movimientos en la distribucion de siniestro.
	 * @param lineaSoporteCoberturaDTO
	 * @param tipoReaseguro
	 * @param movimientos
	 * @param participaciones
	 * @param idToReporteSiniestro
	 * @param idToMovimientoSiniestro
	 * @param montoSiniestro
	 * @param conceptoMovimiento
	 * @param tipoMovimiento
	 * @return
	 */
	private List<MovimientoReaseguroDTO> agregarMovimientosPorTipoContrato(
			LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO,
			int tipoReaseguro, List<MovimientoReaseguroDTO> movimientos,
			List<Participacion> participaciones,
			BigDecimal idToReporteSiniestro,
			BigDecimal idToMovimientoSiniestro, BigDecimal montoSiniestro,
			Integer conceptoMovimiento, Integer tipoMovimiento,Date fechaMovimientoSiniestro) {
		return this.agregarMovimientosPorTipoContrato(lineaSoporteCoberturaDTO,
				tipoReaseguro, movimientos, participaciones, true,
				idToReporteSiniestro, idToMovimientoSiniestro, montoSiniestro,
				conceptoMovimiento, tipoMovimiento,fechaMovimientoSiniestro);
	}

	/**
	 * 
	 * @param lineaSoporteCoberturaDTO
	 *            requerido
	 * @param tipoReaseguro
	 *            requerido
	 * @param movimientos
	 *            La lista en donde se van a guardar los movimientos generados.
	 *            requerido
	 * @param participaciones
	 *            Para movimientos cuota parte y primer excedente es necesario
	 *            que se envien las participaciones.
	 * @param siniestro
	 *            Indicamos por medio de una bandera si son movimientos de
	 *            siniestro. requerido
	 * @param idToReporteSiniestro
	 *            En caso de ser siniestro es requerido sino entonces enviar
	 *            null.
	 * @param montoSiniestro
	 *            En caso de ser siniestro es requerido sino entonces enviar
	 *            null.
	 * @param conceptoMovimiento
	 *            En caso de ser siniestro es requerido sino entonces enviar
	 *            null.
	 * @param tipoMovimiento
	 *            En caso de ser siniestro es requerido sino entonces enviar
	 *            null.
	 * @param esPrimaNoDevengada
	 *            Indicamos por medio de una bandera si estamos hablando de
	 *            prima no devengada.
	 * @param montoPrimaNoDevengada
	 *            El monto de la prima no devengada. Es requerido si
	 *            esPrimaNoDevengada es true.
	 * @return a
	 *         <tt>List<MovimientoReaseguroDTO>/tt> esta es la misma lista que se envio pero para efectos de que
	 *         quede mas explicito de que este metodo genera movimientos se agrego como parametro de retorno.
	 */
	private List<MovimientoReaseguroDTO> agregarMovimientosPorTipoContrato(
			LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO,
			int tipoReaseguro, List<MovimientoReaseguroDTO> movimientos,
			List<Participacion> participaciones, boolean siniestro,
			BigDecimal idToReporteSiniestro,
			BigDecimal idToMovimientoSiniestro, BigDecimal montoSiniestro,
			Integer conceptoMovimiento, Integer tipoMovimiento,Date fechaMovimientoSiniestro) {
		BigDecimal porcentaje;
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = lineaSoporteCoberturaDTO.getLineaSoporteReaseguroDTO();

		if ((tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE || tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE)&& participaciones == null) {
			throw new RuntimeException(
					"Para los tipos de contrato cuota parte y primer excedente se debe de mandar"
							+ "participaciones. (Segun el contrato)");
		}

		if (siniestro) {
			if (montoSiniestro == null || conceptoMovimiento == null
					|| tipoMovimiento == null || idToReporteSiniestro == null
					|| idToMovimientoSiniestro == null) {
				throw new RuntimeException(
						"Debe de proporcionar idToReporteSiniestro, montoSiniestro, conceptoMovimiento, "
								+ "  tipoMovimiento, IdToMovimientoSiniestro si es distribucion de un siniestro. Alguno de estos parametros son nulos.");
			}
		}
		
		//Asignacion de los porcentajes segun contrato y tambien se tomara el porcentaje
		//de diferente campo dependiendo si es siniestro o distribucion de la prima.
		if (tipoReaseguro == TipoReaseguroDTO.TIPO_RETENCION) {
			porcentaje = siniestro ? 
					lineaSoporteReaseguroDTO.getPorcentajeRetencion() :
						lineaSoporteReaseguroDTO.getDisPrimaPorcentajeRetencion();
		} else if (tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE) {
			porcentaje = siniestro ? 
					lineaSoporteReaseguroDTO.getPorcentajeCuotaParte() :
						lineaSoporteReaseguroDTO.getDisPrimaPorcentajeCuotaParte();
		} else if (tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE) {
			porcentaje = siniestro ? 
					lineaSoporteReaseguroDTO.getPorcentajePrimerExcedente() :
						lineaSoporteReaseguroDTO.getDisPrimaPorcentajePrimerExcedente();
		} else if (tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO) {
			porcentaje = siniestro ? 
					lineaSoporteReaseguroDTO.getPorcentajeFacultativo() :
						lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo();
		} else {
			throw new RuntimeException(
					"Tipo Contrato INVALIDO. Utilice las constantes de TipoContratoDTO");
		}

		porcentaje = porcentaje.divide(BigDecimal.valueOf(100.00));
		BigDecimal montoTotal = BigDecimal.valueOf(0.0);

		int conceptoMovimientoPrima = 0;
		int conceptoMovimientoComisionPrima = 0;
		int tipoMovimientoPrima = 0;
		int tipoMovimientoComisionPrima = 0;

		if (siniestro) { // Distribucion de siniestros
			montoTotal = montoSiniestro;
		} else { // Distribucion de la prima
			montoTotal = lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion()
					.subtract(lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada() == null ? BigDecimal.ZERO : lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada()); //Asume que montoPrimaNoDevengada puede ser 0 pero nunca null.
			/*
			 * 14/07/2010. Se resta el monto correspondiente a la prima adicional, para posteriormente generar un s�lo movimiento de esta prima.
			 */
			montoTotal = montoTotal.subtract((lineaSoporteCoberturaDTO.getMontoPrimaAdicional() != null ? lineaSoporteCoberturaDTO.getMontoPrimaAdicional() : new BigDecimal(0d)));
			//Obtener el tipo y concepto movimiento dependiendo del monto calculado.
			if (montoTotal.compareTo(BigDecimal.valueOf(0.0)) > 0) { //Estamos hablando de prima cedida.
				conceptoMovimientoPrima = ConceptoMovimientoDetalleDTO.PRIMA_CEDIDA;
				conceptoMovimientoComisionPrima = ConceptoMovimientoDetalleDTO.COMISION_PRIMA_CEDIDA;
				tipoMovimientoPrima = TipoMovimientoDTO.CUENTAS_POR_PAGAR;
				tipoMovimientoComisionPrima = TipoMovimientoDTO.CUENTAS_POR_COBRAR;
				
			}
			else if (montoTotal.compareTo(BigDecimal.valueOf(0.0)) < 0) { //Estamos hablando de prima no devengada
				montoTotal = montoTotal.abs().subtract((lineaSoporteCoberturaDTO.getMontoPrimaAdicional() != null ? lineaSoporteCoberturaDTO.getMontoPrimaAdicional() : new BigDecimal(0d)).multiply(new BigDecimal(2d))); //Obtener el valor absoluto ya que el monto que fue calculado es negativo.
				conceptoMovimientoPrima = ConceptoMovimientoDetalleDTO.PRIMA_NO_DEVENGADA;
				conceptoMovimientoComisionPrima = ConceptoMovimientoDetalleDTO.COMISION_PRIMA_NO_DEVENGADA;
				tipoMovimientoPrima = TipoMovimientoDTO.CUENTAS_POR_COBRAR;
				tipoMovimientoComisionPrima = TipoMovimientoDTO.CUENTAS_POR_PAGAR;
				
			}
		}
		
		//En caso de que el porcentaje fuera 0 o el montoTotal calculado fuera 0 
		//no entra a este bloque, ni se generan nuevos movimientos
		if (porcentaje.compareTo(BigDecimal.valueOf(0.0)) > 0 && montoTotal.compareTo(BigDecimal.valueOf(0.0)) > 0) {
			BigDecimal montoContrato = montoTotal.multiply(porcentaje);
			if (tipoReaseguro != TipoReaseguroDTO.TIPO_RETENCION) {
				ContratoFacultativoDTO contratoFacultativoDTO = null;
				if (tipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO) {
					// Buscar las participaciones del contrato facultativo.
					contratoFacultativoDTO = lineaSoporteReaseguroDTO.getContratoFacultativoDTO();
					participaciones = getListaParticipacionesFacultativo(
							contratoFacultativoDTO.getIdTmContratoFacultativo(),
							lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo().getIdTcSubRamo(), lineaSoporteCoberturaDTO.getIdToCobertura(),
							lineaSoporteCoberturaDTO.getNumeroInciso(),lineaSoporteCoberturaDTO.getNumeroSubInciso(),lineaSoporteCoberturaDTO.getIdToSeccion());
				}
				for (Participacion participacion : participaciones) {
					BigDecimal montoParticipacionReasegurador = BigDecimal.valueOf(0.0);
					BigDecimal montoComisionReasegurador = BigDecimal.valueOf(0.0);

					montoParticipacionReasegurador = montoContrato.multiply(BigDecimal.valueOf(participacion.getPorcentajeParticipacion())).divide(BigDecimal.valueOf(100.0));

					if (siniestro) {
						movimientos.add(generarMovimientoReaseguro(
								lineaSoporteCoberturaDTO, conceptoMovimiento,
								tipoMovimiento, tipoReaseguro,
								montoParticipacionReasegurador, participacion.getReaseguradorDTO(), participacion.getCorredorDTO(),
								contratoFacultativoDTO, idToReporteSiniestro,idToMovimientoSiniestro,
								lineaSoporteReaseguroDTO.getTipoCambio(),fechaMovimientoSiniestro));
					} else {
						if (lineaSoporteReaseguroDTO.getEsPrimerRiesgo()) {
							montoComisionReasegurador = montoParticipacionReasegurador.multiply(BigDecimal.valueOf(participacion.getComisionEnCombinacion())).divide(BigDecimal.valueOf(100.0));
						} else {
							montoComisionReasegurador = montoParticipacionReasegurador.multiply(BigDecimal.valueOf(participacion.getPorcentajeComision())).divide(BigDecimal.valueOf(100.0));
						}

						if (montoParticipacionReasegurador.compareTo(BigDecimal.valueOf(0.0)) > 0) {
							movimientos.add(generarMovimientoReaseguro(
									lineaSoporteCoberturaDTO,conceptoMovimientoPrima,
									tipoMovimientoPrima, tipoReaseguro,montoParticipacionReasegurador,
									participacion.getReaseguradorDTO(),participacion.getCorredorDTO(),
									contratoFacultativoDTO, null, null,
									lineaSoporteReaseguroDTO.getTipoCambio(),fechaMovimientoSiniestro));
						}
						if (montoComisionReasegurador.compareTo(BigDecimal.valueOf(0.0)) > 0) {
							movimientos.add(generarMovimientoReaseguro(
									lineaSoporteCoberturaDTO,conceptoMovimientoComisionPrima,
									tipoMovimientoComisionPrima, tipoReaseguro,montoComisionReasegurador,
									participacion.getReaseguradorDTO(),participacion.getCorredorDTO(),
									contratoFacultativoDTO, null, null,
									lineaSoporteReaseguroDTO.getTipoCambio(),fechaMovimientoSiniestro));
						}
					}
				}

			} else { // Retencion
				if (montoContrato.compareTo(BigDecimal.valueOf(0)) > 0) {
					if (siniestro) {
						movimientos.add(generarMovimientoReaseguro(
								lineaSoporteCoberturaDTO, conceptoMovimiento,tipoMovimiento,
								TipoReaseguroDTO.TIPO_RETENCION, montoContrato,
								null,null, null, idToReporteSiniestro,
								idToMovimientoSiniestro,
								lineaSoporteReaseguroDTO.getTipoCambio(),fechaMovimientoSiniestro));
					} else {
						movimientos.add(generarMovimientoReaseguro(
								lineaSoporteCoberturaDTO,
								conceptoMovimientoPrima, tipoMovimientoPrima,
								TipoReaseguroDTO.TIPO_RETENCION, montoContrato,
								null, null, null, null, null,
								lineaSoporteReaseguroDTO.getTipoCambio(),fechaMovimientoSiniestro));
					}
				}
			}
		}

		return movimientos;
	}

	/**
	 * Regresa la lista de Participacion con los campos necesarios para poder
	 * distribuir la prima tipoContrato: 1: Contrato Couta Parte 2: Contrato
	 * Primer Excedente
	 */
	private List<Participacion> getListaParticipaciones(LineaDTO linea,int tipoContrato) {
		List<Participacion> listaParticipaciones = new ArrayList<Participacion>();
		Participacion participacion;
		List<ParticipacionDTO> participacionDTOs = new ArrayList<ParticipacionDTO>();

		if (tipoContrato == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE && linea.getContratoCuotaParte() != null) {
			participacionDTOs = participacionFacade.findByProperty("contratoCuotaParte.idTmContratoCuotaParte", linea.getContratoCuotaParte().getIdTmContratoCuotaParte());
		} else if (tipoContrato == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE && linea.getContratoPrimerExcedente() != null) {
			participacionDTOs = participacionFacade.findByProperty("contratoPrimerExcedente.idTmContratoPrimerExcedente",linea.getContratoPrimerExcedente().getIdTmContratoPrimerExcedente());
		}

		for (ParticipacionDTO participacionDTO : participacionDTOs) {
			// Si el participante es un Corredor
			if (participacionDTO.getReaseguradorCorredor().getTipo().equals("0")) {
				List<ParticipacionCorredorDTO> participacionCorredorDTOs = participacionCorredorFacade.
																findByProperty("participacion.idTdParticipacion",participacionDTO.getIdTdParticipacion());
				for (ParticipacionCorredorDTO participacionCorredorDTO : participacionCorredorDTOs) {
					participacion = new Participacion();
					participacion.setReaseguradorDTO(participacionCorredorDTO.getReaseguradorCorredor());
					participacion.setCorredorDTO(participacionDTO.getReaseguradorCorredor());
					participacion.setPorcentajeParticipacion(participacionCorredorDTO.getPorcentajeParticipacion());

					// Buscar la comision para esta participacion y linea
					LineaParticipacionDTO lineaParticipacionDTO = new LineaParticipacionDTO();
					lineaParticipacionDTO.setId(new LineaParticipacionId(participacionDTO.getIdTdParticipacion(), linea.getIdTmLinea()));
					lineaParticipacionDTO = lineaParticipacionFacade.findById(lineaParticipacionDTO.getId());
					if (lineaParticipacionDTO != null) {
						participacion.setPorcentajeComision(lineaParticipacionDTO.getComision());
						participacion.setComisionEnCombinacion(lineaParticipacionDTO.getComisionEnCombinacion());
					} else {
						participacion.setPorcentajeComision(0d);
						participacion.setComisionEnCombinacion(0d);
					}

					listaParticipaciones.add(participacion);
				}
			} else if (participacionDTO.getReaseguradorCorredor().getTipo().equals("1")) { // Reasegurador
				participacion = new Participacion();
				participacion.setPorcentajeParticipacion(participacionDTO.getPorcentajeParticipacion());
				participacion.setReaseguradorDTO(participacionDTO.getReaseguradorCorredor());

				// Buscar la comision para esta participacion y linea
				LineaParticipacionDTO lineaParticipacionDTO = new LineaParticipacionDTO();
				lineaParticipacionDTO.setId(new LineaParticipacionId(participacionDTO.getIdTdParticipacion(), linea.getIdTmLinea()));
				lineaParticipacionDTO = lineaParticipacionFacade.findById(lineaParticipacionDTO.getId());
				if (lineaParticipacionDTO != null) {
					if (lineaParticipacionDTO.getComision() != null) {
						participacion.setPorcentajeComision(lineaParticipacionDTO.getComision());
					} else {
						participacion.setPorcentajeComision(0d);
					}
					if (lineaParticipacionDTO.getComisionEnCombinacion() != null) {
						participacion.setComisionEnCombinacion(lineaParticipacionDTO.getComisionEnCombinacion());
					} else {
						participacion.setComisionEnCombinacion(0d);
					}
				} else {
					participacion.setPorcentajeComision(0d);
					participacion.setComisionEnCombinacion(0d);
				}
				listaParticipaciones.add(participacion);
			}
		}

		return listaParticipaciones;
	}

	private List<Participacion> getListaParticipacionesFacultativo(
			BigDecimal idTmContratoFacultativo, BigDecimal idTcSubramo,
			BigDecimal idToCobertura, BigDecimal numeroInciso,
			BigDecimal numeroSubInciso, BigDecimal idToSeccion) {
		DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = detalleContratoFacultativoFacade
				.findUniqueByNaturalKey(idTmContratoFacultativo, idTcSubramo,idToCobertura, numeroInciso, numeroSubInciso,idToSeccion);
		return generarParticipaciones(detalleContratoFacultativoDTO.getParticipacionFacultativoDTOs());
	}

	private List<Participacion> generarParticipaciones(List<ParticipacionFacultativoDTO> participacionFacultativoDTOs) {
		List<Participacion> participaciones = new ArrayList<Participacion>();
		for (ParticipacionFacultativoDTO participacionFacultativoDTO : participacionFacultativoDTOs) {
			Participacion participacion;
			if (participacionFacultativoDTO.getTipo().equals("0")) {
				List<ParticipacionCorredorFacultativoDTO> participacionCorredorFacultativoDTOs = participacionFacultativoDTO.getParticipacionCorredorFacultativoDTOs();
				for (ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO : participacionCorredorFacultativoDTOs) {
					participacion = new Participacion();
					participacion.setReaseguradorDTO(participacionCorredorFacultativoDTO.getReaseguradorCorredorDTO());
					participacion.setPorcentajeParticipacion(participacionCorredorFacultativoDTO.getPorcentajeParticipacion().doubleValue());
					participacion.setPorcentajeComision(participacionFacultativoDTO.getComision().doubleValue());
					participacion.setComisionEnCombinacion(participacionFacultativoDTO.getComision().doubleValue());
					participacion.setCorredorDTO(participacionFacultativoDTO.getReaseguradorCorredorDTO());
					participaciones.add(participacion);
				}
			} else {
				participacion = new Participacion();
				participacion.setReaseguradorDTO(participacionFacultativoDTO.getReaseguradorCorredorDTO());
				participacion.setPorcentajeParticipacion(participacionFacultativoDTO.getPorcentajeParticipacion().doubleValue());
				participacion.setPorcentajeComision(participacionFacultativoDTO.getComision().doubleValue());
				participacion.setComisionEnCombinacion(participacionFacultativoDTO.getComision().doubleValue());
				participaciones.add(participacion);
			}
		}
		return participaciones;
	}

	private LineaSoporteCoberturaDTO obtenerLineaSoporteCobertura(BigDecimal idToPoliza, Integer numeroEndoso,BigDecimal idToSeccion, 
							BigDecimal idToCobertura,Integer numeroInciso, Integer numeroSubInciso) {
		BigDecimal tipoDistribucion = lineaFacade.obtenerTipoDistribucion(idToPoliza, idToCobertura);
		if(tipoDistribucion != null){
			if(tipoDistribucion.compareTo(BigDecimal.ONE) == 0 || tipoDistribucion.compareTo(BigDecimal.valueOf(2d)) == 0){
				numeroSubInciso = null;
			}
		}
		LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO = new LineaSoporteCoberturaDTO();
		int coincidencias = 0;

		lineaSoporteCoberturaDTO.setIdToPoliza(idToPoliza);
		if (numeroInciso != null && !(numeroInciso == 0)) {
			lineaSoporteCoberturaDTO.setNumeroInciso(new BigDecimal(numeroInciso));
		}
		if (numeroSubInciso != null && !(numeroSubInciso==0)) {lineaSoporteCoberturaDTO.setNumeroSubInciso(new BigDecimal(numeroSubInciso));
		}
		lineaSoporteCoberturaDTO.setIdToCobertura(idToCobertura);
		lineaSoporteCoberturaDTO.setIdToSeccion(idToSeccion);

		List<LineaSoporteCoberturaDTO> lineaSoporteCoberturaDTOs;

		lineaSoporteCoberturaDTOs = lineaSoporteCoberturaFacade.obtenPorLlaveCobertura(lineaSoporteCoberturaDTO);
		if (lineaSoporteCoberturaDTOs != null && lineaSoporteCoberturaDTOs.size() > 0) {
			for (LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO2 : lineaSoporteCoberturaDTOs) {
				if (lineaSoporteCoberturaDTO2.getLineaSoporteReaseguroDTO() != null) {
					if (lineaSoporteCoberturaDTO2.getLineaSoporteReaseguroDTO().getSoporteReaseguroDTO() != null) {
						if (numeroEndoso != null) {
							if (lineaSoporteCoberturaDTO2.getLineaSoporteReaseguroDTO().getSoporteReaseguroDTO().getNumeroEndoso().equals(numeroEndoso)) {
								coincidencias = coincidencias + 1;
								lineaSoporteCoberturaDTO = lineaSoporteCoberturaDTO2;
								break;
							}
						}
					}
				}
			}
		}

		if (coincidencias == 1) {
			return lineaSoporteCoberturaDTO;
		} else {
			return null;
		}
	}

	private List<MovimientoReaseguroDTO> generarMovimientosReaseguro(List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTOs) {
		List<MovimientoReaseguroDTO> listaMovimientosReaseguroDTO = new ArrayList<MovimientoReaseguroDTO>();
		for (LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO : lineaSoporteReaseguroDTOs) {
			//TODO Aram hay que regresar al estatus 2 del 22 cuando el sistema se termine de ajustar
			if (lineaSoporteReaseguroDTO.getEstatusLineaSoporte() == LineaSoporteReaseguroDTO.EMITIDO_SIN_DISTRIBUIR || 
					lineaSoporteReaseguroDTO.getEstatusLineaSoporte() == LineaSoporteReaseguroDTO.EMITIDO_DISTRIBUCION_MASIVA) {
				generarMovimientosReaseguro(lineaSoporteReaseguroDTO,listaMovimientosReaseguroDTO);
			} else {
				LogDeMidasEJB3.log("Este c�mulo ya se encuentra distribuido idToLineaSoporte: "+ lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro(),Level.INFO, null);
			}
		}
		return listaMovimientosReaseguroDTO;
	}

	private List<MovimientoReaseguroDTO> generarMovimientosReaseguro(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,List<MovimientoReaseguroDTO> listaMovimientosReaseguroDTO) {
		//10/01/2010 Se agrega validaci�n para determinar si se deben generar movimientos en base a la bandera "aplicaDistribuci�n"
		if(lineaSoporteReaseguroDTO.getAplicaDistribucionPrima() == null || 
				lineaSoporteReaseguroDTO.getAplicaDistribucionPrima().intValue() == LineaSoporteReaseguroDTO.APLICA_DISTRIBUCION_PRIMA){
			List<Participacion> listaParticipacionCP = getListaParticipaciones(lineaSoporteReaseguroDTO.getLineaDTO(),TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE);
			List<Participacion> listaParticipacion1E = getListaParticipaciones(lineaSoporteReaseguroDTO.getLineaDTO(),TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE);
			
			for (LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs()) {
				generarMovimientosReaseguro(lineaSoporteCoberturaDTO, listaMovimientosReaseguroDTO, listaParticipacionCP, listaParticipacion1E);
			}
			/*
			 * 13/07/2010 Jos� Luis Arellano. Se agrega el movimiento por prima adicional
			 */
			agregarMovimientoPorPrimaAdicional(lineaSoporteReaseguroDTO, listaMovimientosReaseguroDTO);
		}
		
		lineaSoporteReaseguroDTO.setEstatusLineaSoporte(LineaSoporteReaseguroDTO.EMITIDO_Y_DISTRIBUIDO); // Emitida y distribuida
		lineaSoporteReaseguroDTO.setNotaDelSistema(EMITIDO_Y_DISTRIBUIDO_NOTA);
		
		return listaMovimientosReaseguroDTO;
	}
	
	private void agregarMovimientoPorPrimaAdicional(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO, List<MovimientoReaseguroDTO> movimientos){
		ContratoFacultativoDTO contratoFacultativoDTO = lineaSoporteReaseguroDTO.getContratoFacultativoDTO();
		if(contratoFacultativoDTO != null && contratoFacultativoDTO.getMontoPrimaAdicional() != null && contratoFacultativoDTO.getMontoPrimaAdicional().compareTo(BigDecimal.ZERO) != 0 ){
			int conceptoMovimiento = 0;
			if(contratoFacultativoDTO.getMontoPrimaAdicional().compareTo(BigDecimal.ZERO) > 0)
				conceptoMovimiento = ConceptoMovimientoDetalleDTO.PRIMA_ADICIONAL;
			else
				conceptoMovimiento = ConceptoMovimientoDetalleDTO.CANCELACION_PRIMA_ADICIONAL;
			MovimientoReaseguroDTO movimiento = new MovimientoReaseguroDTO();
			movimiento.setAcumulado(0);
			movimiento.setCantidad(contratoFacultativoDTO.getMontoPrimaAdicional().abs());
			movimiento.setConceptoMovimiento(conceptoMovimiento);
			movimiento.setContabilizado(0);
			movimiento.setContratoFacultativoDTO(lineaSoporteReaseguroDTO.getContratoFacultativoDTO());
			movimiento.setFechaRegistro(new Date());
			movimiento.setIdMoneda(lineaSoporteReaseguroDTO.getIdMoneda().intValue());
			movimiento.setIdPoliza(lineaSoporteReaseguroDTO.getIdToPoliza());
			movimiento.setLineaDTO(lineaSoporteReaseguroDTO.getLineaDTO());
			movimiento.setNumeroEndoso(lineaSoporteReaseguroDTO.getSoporteReaseguroDTO().getNumeroEndoso());
			movimiento.setSubRamo(lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo());
			movimiento.setTipoReaseguro(TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);
			movimiento.setTipoMovimiento(TipoMovimientoDTO.CUENTAS_POR_PAGAR);
			movimientos.add(movimiento);
		}
	}

	private List<MovimientoReaseguroDTO> generarMovimientosReaseguro(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO,
					List<MovimientoReaseguroDTO> listaMovimientosReaseguroDTO,List<Participacion> listaParticipacionCP,List<Participacion> listaParticipacion1E) {
		listaMovimientosReaseguroDTO = agregarMovimientosPorTipoContrato(lineaSoporteCoberturaDTO, TipoReaseguroDTO.TIPO_RETENCION,listaMovimientosReaseguroDTO, null);
		listaMovimientosReaseguroDTO = agregarMovimientosPorTipoContrato(lineaSoporteCoberturaDTO,TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE,listaMovimientosReaseguroDTO, listaParticipacionCP);
		listaMovimientosReaseguroDTO = agregarMovimientosPorTipoContrato(lineaSoporteCoberturaDTO,TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE,listaMovimientosReaseguroDTO, listaParticipacion1E);
		listaMovimientosReaseguroDTO = agregarMovimientosPorTipoContrato(lineaSoporteCoberturaDTO,TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO,listaMovimientosReaseguroDTO, null);

		return listaMovimientosReaseguroDTO;
	}
	
	private MovimientoReaseguroServiciosRemote movimientoReaseguroServicios;
	private SoporteReaseguroFacadeRemote soporteReaseguroFacade;
	private LineaSoporteReaseguroFacadeRemote lineaSoporteReaseguroFacade;
	private LineaSoporteCoberturaFacadeRemote lineaSoporteCoberturaFacade;
	private ParticipacionFacadeRemote participacionFacade;
	private ParticipacionCorredorFacadeRemote participacionCorredorFacade;
	private LineaParticipacionFacadeRemote lineaParticipacionFacade;
	private DetalleContratoFacultativoFacadeRemote detalleContratoFacultativoFacade;
//	private DistribucionReaseguroServicios distribucionReaseguroServicios;
	private DistribucionMovSiniestroFacadeRemote distribucionMovSiniestroFacadeRemote;
	private TemporizadorDistribuirPolizaFacadeRemote temporizadorDistribuirPolizaFacadeRemote;
	private LineaFacadeRemote lineaFacade;

	@EJB
	public void setMovimientoReaseguroServicios(
			MovimientoReaseguroServiciosRemote movimientoReaseguroServicios) {
		this.movimientoReaseguroServicios = movimientoReaseguroServicios;
	}

	@EJB
	public void setSoporteReaseguroFacade(
			SoporteReaseguroFacadeRemote soporteReaseguroFacade) {
		this.soporteReaseguroFacade = soporteReaseguroFacade;
	}

	@EJB
	public void setLineaSoporteReaseguroFacade(
			LineaSoporteReaseguroFacadeRemote lineaSoporteReaseguroFacade) {
		this.lineaSoporteReaseguroFacade = lineaSoporteReaseguroFacade;
	}

	@EJB
	public void setLineaSoporteCoberturaFacade(
			LineaSoporteCoberturaFacadeRemote lineaSoporteCoberturaFacade) {
		this.lineaSoporteCoberturaFacade = lineaSoporteCoberturaFacade;
	}

	@EJB
	public void setParticipacionFacade(
			ParticipacionFacadeRemote participacionFacade) {
		this.participacionFacade = participacionFacade;
	}

	@EJB
	public void setParticipacionCorredorFacade(
			ParticipacionCorredorFacadeRemote participacionCorredorFacade) {
		this.participacionCorredorFacade = participacionCorredorFacade;
	}

	@EJB
	public void setLineaParticipacionFacade(
			LineaParticipacionFacadeRemote lineaParticipacionFacade) {
		this.lineaParticipacionFacade = lineaParticipacionFacade;
	}

	@EJB
	public void setDetalleContratoFacultativoFacade(
			DetalleContratoFacultativoFacadeRemote detalleContratoFacultativoFacade) {
		this.detalleContratoFacultativoFacade = detalleContratoFacultativoFacade;
	}

//	@EJB
//	public void setDistribucionReaseguroServicios(
//			DistribucionReaseguroServicios distribucionReaseguroServicios) {
//		this.distribucionReaseguroServicios = distribucionReaseguroServicios;
//	}

	@EJB
	public void setDistribucionMovSiniestroFacadeRemote(
			DistribucionMovSiniestroFacadeRemote distribucionMovSiniestroFacadeRemote) {
		this.distribucionMovSiniestroFacadeRemote = distribucionMovSiniestroFacadeRemote;
	}

	@EJB
	public void setTemporizadorDistribuirPolizaFacadeRemote(
			TemporizadorDistribuirPolizaFacadeRemote temporizadorDistribuirPolizaFacadeRemote) {
		this.temporizadorDistribuirPolizaFacadeRemote = temporizadorDistribuirPolizaFacadeRemote;
	}
	
	@EJB
	public void setLineafacade(LineaFacadeRemote lineaFacade) {
		this.lineaFacade = lineaFacade;
	}

	public final String EMITIDO_Y_DISTRIBUIDO_NOTA = "Midas: Emitido y Distribuido";

	private class Participacion {

		public double getPorcentajeParticipacion() {
			return porcentajeParticipacion;
		}

		public void setPorcentajeParticipacion(double porcentajeParticipacion) {
			this.porcentajeParticipacion = porcentajeParticipacion;
		}

		public double getPorcentajeComision() {
			return porcentajeComision;
		}

		public void setPorcentajeComision(double porcentajeComision) {
			this.porcentajeComision = porcentajeComision;
		}

		public ReaseguradorCorredorDTO getReaseguradorDTO() {
			return reaseguradorDTO;
		}

		public void setReaseguradorDTO(ReaseguradorCorredorDTO reasegurtadorDTO) {
			reaseguradorDTO = reasegurtadorDTO;
		}

		public ReaseguradorCorredorDTO getCorredorDTO() {
			return corredorDTO;
		}

		public void setCorredorDTO(ReaseguradorCorredorDTO corredorDTO) {
			this.corredorDTO = corredorDTO;
		}

		public double getComisionEnCombinacion() {
			return comisionEnCombinacion;
		}

		public void setComisionEnCombinacion(double comisionEnCombinacion) {
			this.comisionEnCombinacion = comisionEnCombinacion;
		}

		double porcentajeParticipacion;
		double porcentajeComision;
		double comisionEnCombinacion;
		ReaseguradorCorredorDTO reaseguradorDTO;
		ReaseguradorCorredorDTO corredorDTO;
	}
}