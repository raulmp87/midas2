package mx.com.afirme.midas.reaseguro.distribucion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDetalleDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.usuario.LogUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity DistribucionMovSiniestroDTO.
 * 
 * @see .DistribucionMovSiniestroDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class DistribucionMovSiniestroFacade implements
		DistribucionMovSiniestroFacadeRemote {
	// property constants
	public static final String TIPO_MOVIMIENTO = "tipoMovimiento";
	public static final String NUMERO_ENDOSO = "numeroEndoso";
	public static final String NUMERO_INCISO = "numeroInciso";
	public static final String NUMERO_SUB_INCISO = "numeroSubInciso";
	public static final String ESTATUS = "estatus";
	public static final String _NOTA_DEL_SISTEMA = "NotaDelSistema";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * DistribucionMovSiniestroDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            DistribucionMovSiniestroDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DistribucionMovSiniestroDTO entity) {
		LogUtil.log("saving DistribucionMovSiniestroDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent DistribucionMovSiniestroDTO entity.
	 * 
	 * @param entity
	 *            DistribucionMovSiniestroDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DistribucionMovSiniestroDTO entity) {
		LogUtil.log("deleting DistribucionMovSiniestroDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					DistribucionMovSiniestroDTO.class, entity
							.getIdToDistribucionMovSiniestro());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved DistribucionMovSiniestroDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * DistribucionMovSiniestroDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            DistribucionMovSiniestroDTO entity to update
	 * @return DistribucionMovSiniestroDTO the persisted
	 *         DistribucionMovSiniestroDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DistribucionMovSiniestroDTO update(DistribucionMovSiniestroDTO entity) {
		LogUtil.log("updating DistribucionMovSiniestroDTO instance",
				Level.INFO, null);
		try {
			DistribucionMovSiniestroDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DistribucionMovSiniestroDTO findById(BigDecimal id) {
		LogUtil.log("finding DistribucionMovSiniestroDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			DistribucionMovSiniestroDTO instance = entityManager.find(
					DistribucionMovSiniestroDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DistribucionMovSiniestroDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the DistribucionMovSiniestroDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DistribucionMovSiniestroDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<DistribucionMovSiniestroDTO> findByProperty(
			String propertyName, final Object value) {
		LogUtil.log(
				"finding DistribucionMovSiniestroDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from DistribucionMovSiniestroDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<DistribucionMovSiniestroDTO> findByTipoMovimiento(
			Object tipoMovimiento) {
		return findByProperty(TIPO_MOVIMIENTO, tipoMovimiento);
	}

	public List<DistribucionMovSiniestroDTO> findByNumeroEndoso(
			Object numeroEndoso) {
		return findByProperty(NUMERO_ENDOSO, numeroEndoso);
	}

	public List<DistribucionMovSiniestroDTO> findByNumeroInciso(
			Object numeroInciso) {
		return findByProperty(NUMERO_INCISO, numeroInciso);
	}

	public List<DistribucionMovSiniestroDTO> findByNumeroSubInciso(
			Object numeroSubInciso) {
		return findByProperty(NUMERO_SUB_INCISO, numeroSubInciso);
	}

	public List<DistribucionMovSiniestroDTO> findByEstatus(Object estatus) {
		return findByProperty(ESTATUS, estatus);
	}

	public List<DistribucionMovSiniestroDTO> findByNotaDelSistema(
			Object NotaDelSistema) {
		return findByProperty(_NOTA_DEL_SISTEMA, NotaDelSistema);
	}

	/**
	 * Find all DistribucionMovSiniestroDTO entities.
	 * 
	 * @return List<DistribucionMovSiniestroDTO> all DistribucionMovSiniestroDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<DistribucionMovSiniestroDTO> findAll() {
		LogUtil.log("finding all DistribucionMovSiniestroDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from DistribucionMovSiniestroDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Map<String, String> registrarMovimientoDistribucionSiniestro(BigDecimal idToPoliza,
			Integer numeroEndoso, BigDecimal idToSeccion, BigDecimal idToCobertura, Integer numeroInciso,
			Integer numeroSubInciso, Integer conceptoMovimiento, BigDecimal idToMoneda, Date fechaMovimiento,
			BigDecimal idMovimientoSiniestro, BigDecimal montoMovimiento, Integer tipoMovimiento, BigDecimal idToReporteSiniestro) {
		
		Map<String,String> mapaResultado = new HashMap<String, String>();
		DistribucionMovSiniestroDTO distribucionMovSiniestroDTO = new DistribucionMovSiniestroDTO();
		try{
			distribucionMovSiniestroDTO.setIdToPoliza(idToPoliza);
			distribucionMovSiniestroDTO.setNumeroEndoso(numeroEndoso);
			distribucionMovSiniestroDTO.setIdToSeccion(idToSeccion);
			distribucionMovSiniestroDTO.setIdToCobertura(idToCobertura);
			distribucionMovSiniestroDTO.setNumeroInciso(numeroInciso);
			distribucionMovSiniestroDTO.setNumeroSubInciso(numeroSubInciso);
			ConceptoMovimientoDetalleDTO conceptoMovimientoDetalleDTO = new ConceptoMovimientoDetalleDTO();
			conceptoMovimientoDetalleDTO.setIdConceptoDetalle(conceptoMovimiento);
			distribucionMovSiniestroDTO.setConceptoMovimientoDetalleDTO(conceptoMovimientoDetalleDTO);
			distribucionMovSiniestroDTO.setIdTcMoneda(idToMoneda);
			distribucionMovSiniestroDTO.setFechaMovimiento(fechaMovimiento);
			distribucionMovSiniestroDTO.setIdMovimientoSiniestro(idMovimientoSiniestro);
			distribucionMovSiniestroDTO.setMonto(montoMovimiento);
			distribucionMovSiniestroDTO.setTipoMovimiento(tipoMovimiento);
			distribucionMovSiniestroDTO.setIdToReporteSiniestro(idToReporteSiniestro);
			distribucionMovSiniestroDTO.setEstatus(DistribucionMovSiniestroDTO.ESTATUS_SIN_DISTRIBUIR);
			distribucionMovSiniestroDTO.setFechaCreacion(new Date());
			distribucionMovSiniestroDTO.setFechaModificacion(new Date());
			distribucionMovSiniestroDTO.setNotaDelSistema(DistribucionMovSiniestroDTO.NOTA_SISTEMA_SIN_DISTRIBUIR);
			
			//12/01/2011. Consulta de la lineasoporte, en caso de no encontrarse, se genera mensaje de error al cliente.
			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = distribucionReaseguroServicios.getPorcentajeDistribucion(
					distribucionMovSiniestroDTO.getIdToPoliza(), distribucionMovSiniestroDTO.getNumeroEndoso(), distribucionMovSiniestroDTO.getIdToSeccion(), 
					distribucionMovSiniestroDTO.getIdToCobertura(), distribucionMovSiniestroDTO.getNumeroInciso(), distribucionMovSiniestroDTO.getNumeroSubInciso());
			
			if (lineaSoporteReaseguroDTO == null) {
				String parametrosBusqueda = " idToPoliza: " + distribucionMovSiniestroDTO.getIdToPoliza()
					+ ", numeroEndoso: " + distribucionMovSiniestroDTO.getNumeroEndoso()
					+ ", idToSeccion: " + distribucionMovSiniestroDTO.getIdToSeccion()
					+ ", idToCobertura: " + distribucionMovSiniestroDTO.getIdToCobertura()
					+ ", numeroInciso: " + distribucionMovSiniestroDTO.getNumeroInciso()
					+ ", numeroSubInciso: " + distribucionMovSiniestroDTO.getNumeroSubInciso();
				String parametrosSiniestro = "conceptoMovimiento: "+conceptoMovimiento+"" +
						", idToMoneda: "+idToMoneda+
						", fechaMovimiento: "+fechaMovimiento+
						", idMovimientoSiniestro: "+idMovimientoSiniestro+
						", montoMovimiento: "+montoMovimiento+
						", tipoMovimiento: "+tipoMovimiento+
						", idToReporteSiniestro: "+idToReporteSiniestro;
				
				String error1 = "DistribucionMovSiniestroFacade.registrarMovimientoDistribucionSiniestro(...). Se intent� registrar un movimiento de distribuci�n de siniestro, " +
						"pero no se encontr� LineaSoporteReaseguro para los datos: "+parametrosBusqueda;
				String error2 = "NO SE PUEDE CONTINUAR CON LA DISTRIBUCI�N DEL SINIESTRO: "+parametrosSiniestro;
				LogDeMidasEJB3.log(error1, Level.SEVERE, null);
				LogDeMidasEJB3.log(error2, Level.SEVERE, null);
				
				mapaResultado.put("claveRespuesta", SistemaPersistencia.CLAVE_MENSAJE_ERROR);
				mapaResultado.put("mensaje", error1);
			}
			else{
				distribucionMovSiniestroDTO.setLineaSoporteReaseguroDTO(lineaSoporteReaseguroDTO);
				//Persistir movimiento
				this.save(distribucionMovSiniestroDTO);
			}
			
		}catch(Exception e){
			LogDeMidasEJB3.log("Ocurri� un error al intentar registrar un objeto DistribucionMovSiniestroDTO.", Level.SEVERE, e);
			String error = null;
			if((error = e.getLocalizedMessage()) == null){
				if((error = e.getMessage()) == null)
					error = e.getStackTrace().toString();
			}
			mapaResultado.put("claveRespuesta", SistemaPersistencia.CLAVE_MENSAJE_ERROR);
			mapaResultado.put("mensaje", error);
		}
		
		if(!mapaResultado.containsKey("claveRespuesta")){//Si el proceso fue exitoso
			mapaResultado.put("claveRespuesta", SistemaPersistencia.CLAVE_MENSAJE_EXITO);
			mapaResultado.put("idToDistribucionMovSiniestro",distribucionMovSiniestroDTO.getIdToDistribucionMovSiniestro().toBigInteger().toString());
		}
		
		return mapaResultado;
	}
	
	public DistribucionMovSiniestroDTO registrarMovimiento(BigDecimal idToPoliza,
			Integer numeroEndoso, BigDecimal idToSeccion, BigDecimal idToCobertura, Integer numeroInciso,
			Integer numeroSubInciso, Integer conceptoMovimiento, BigDecimal idToMoneda, Date fechaMovimiento,
			BigDecimal idMovimientoSiniestro, BigDecimal montoMovimiento, Integer tipoMovimiento, BigDecimal idToReporteSiniestro) {
		DistribucionMovSiniestroDTO distribucionMovSiniestroDTO = new DistribucionMovSiniestroDTO();
		distribucionMovSiniestroDTO.setIdToPoliza(idToPoliza);
		distribucionMovSiniestroDTO.setNumeroEndoso(numeroEndoso);
		distribucionMovSiniestroDTO.setIdToSeccion(idToSeccion);
		distribucionMovSiniestroDTO.setIdToCobertura(idToCobertura);
		distribucionMovSiniestroDTO.setNumeroInciso(numeroInciso);
		distribucionMovSiniestroDTO.setNumeroSubInciso(numeroSubInciso);
		ConceptoMovimientoDetalleDTO conceptoMovimientoDetalleDTO = new ConceptoMovimientoDetalleDTO();
		conceptoMovimientoDetalleDTO.setIdConceptoDetalle(conceptoMovimiento);
		distribucionMovSiniestroDTO.setConceptoMovimientoDetalleDTO(conceptoMovimientoDetalleDTO);
		distribucionMovSiniestroDTO.setIdTcMoneda(idToMoneda);
		distribucionMovSiniestroDTO.setFechaMovimiento(fechaMovimiento);
		distribucionMovSiniestroDTO.setIdMovimientoSiniestro(idMovimientoSiniestro);
		distribucionMovSiniestroDTO.setMonto(montoMovimiento);
		distribucionMovSiniestroDTO.setTipoMovimiento(tipoMovimiento);
		distribucionMovSiniestroDTO.setIdToReporteSiniestro(idToReporteSiniestro);
		distribucionMovSiniestroDTO.setEstatus(DistribucionMovSiniestroDTO.ESTATUS_SIN_DISTRIBUIR);
		distribucionMovSiniestroDTO.setFechaCreacion(new Date());
		distribucionMovSiniestroDTO.setFechaModificacion(new Date());
		distribucionMovSiniestroDTO.setNotaDelSistema(DistribucionMovSiniestroDTO.NOTA_SISTEMA_SIN_DISTRIBUIR);
		validarMovimiento(distribucionMovSiniestroDTO);
		
		//Persistir movimiento
		this.save(distribucionMovSiniestroDTO);
		return distribucionMovSiniestroDTO;
	}

	private void validarMovimiento(DistribucionMovSiniestroDTO distribucionMovSiniestroDTO) {
		//Validar que exista una LineaSoporteReaseguro
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = distribucionReaseguroServicios.getPorcentajeDistribucion(
					distribucionMovSiniestroDTO.getIdToPoliza(), distribucionMovSiniestroDTO.getNumeroEndoso(), distribucionMovSiniestroDTO.getIdToSeccion(), 
					distribucionMovSiniestroDTO.getIdToCobertura(), distribucionMovSiniestroDTO.getNumeroInciso(), distribucionMovSiniestroDTO.getNumeroSubInciso());
		if (lineaSoporteReaseguroDTO == null) {
			throw new RuntimeException("No se encontro una LineaSoporteCobertura para"
					+ " idToPoliza: " + distribucionMovSiniestroDTO.getIdToPoliza()
					+ " numeroEndoso: " + distribucionMovSiniestroDTO.getNumeroEndoso()
					+ " idToSeccion: " + distribucionMovSiniestroDTO.getIdToSeccion()
					+ " idToCobertura: " + distribucionMovSiniestroDTO.getIdToCobertura()
					+ " numeroInciso: " + distribucionMovSiniestroDTO.getNumeroInciso()
					+ " numeroSubInciso: " + distribucionMovSiniestroDTO.getNumeroSubInciso());
		}
		distribucionMovSiniestroDTO.setLineaSoporteReaseguroDTO(lineaSoporteReaseguroDTO);
	}
	
	@EJB
	private DistribucionReaseguroServicios distribucionReaseguroServicios;  

	@SuppressWarnings("unchecked")
	public List<DistribucionMovSiniestroDTO> listarFiltrado(DistribucionMovSiniestroDTO entity ){
		try {
			String queryString = "select model from DistribucionMovSiniestroDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			if(entity.getLineaSoporteReaseguroDTO() != null && entity.getLineaSoporteReaseguroDTO().getId() != null && 
					entity.getLineaSoporteReaseguroDTO().getId().getIdToSoporteReaseguro() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "lineaSoporteReaseguroDTO.id.idToSoporteReaseguro", entity.getLineaSoporteReaseguroDTO().getId().getIdToSoporteReaseguro());
			if(entity.getLineaSoporteReaseguroDTO() != null && entity.getLineaSoporteReaseguroDTO().getId() != null && 
					entity.getLineaSoporteReaseguroDTO().getId().getIdTmLineaSoporteReaseguro() != null)
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "lineaSoporteReaseguroDTO.id.idTmLineaSoporteReaseguro", entity.getLineaSoporteReaseguroDTO().getId().getIdTmLineaSoporteReaseguro());
			
			//sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idTipoCalculo", entity.getIdTipoCalculo());
			//sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveEstatus", entity.getClaveEstatus());
			//sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveActivoProduccion", entity.getClaveActivoProduccion());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarFiltrado failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<DistribucionMovSiniestroDTO> obtenerMovimientosNoDistribuidos(BigDecimal idMovimientoSiniestro, BigDecimal idToReporteSiniestro,int idConceptoDetalle) {
		LogUtil.log("sumaPagosParcialesAgregar consulta", Level.INFO, null);
		try {
			
			StringBuilder sb = new StringBuilder();		
			sb.append(" select  ");
			sb.append("		model ");
			sb.append(" from ");
			sb.append("		DistribucionMovSiniestroDTO model ");
			sb.append(" where ");
			sb.append("		model.idMovimientoSiniestro = :idMovimientoSiniestro ");
			sb.append("		and model.idToReporteSiniestro = :idToReporteSiniestro ");
			sb.append(" 	and model.conceptoMovimientoDetalleDTO.idConceptoDetalle = :idConceptoDetalle ");
			sb.append(" 	and model.estatus = 1 ");
			
			Query query = entityManager.createQuery(sb.toString());
			query.setParameter("idMovimientoSiniestro", idMovimientoSiniestro);
			query.setParameter("idToReporteSiniestro", idToReporteSiniestro);
			query.setParameter("idConceptoDetalle", idConceptoDetalle);
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogUtil.log("sumaPagosParcialesAgregar failed", Level.SEVERE, re);
			throw re;
		}
	}
}