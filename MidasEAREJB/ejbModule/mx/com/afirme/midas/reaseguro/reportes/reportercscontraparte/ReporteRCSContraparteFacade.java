package mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ReporteRCSContraparteFacade implements ReporteRCSContraparteFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;
	List<ReporteRCSContraparteDTO> listaResultado = null;
	
	/**
	 * Obtiene la informacion de los reportes Contraparte.
	 * @param fechaCorte
	 * @param tipoCambio
	 * @param nombreUsuario
	 * @return listaResultado.
	 */
		
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReporteRCSContraparteDTO> obtenerReporte(String fechaCorte, BigDecimal tipoCambio, String nombreUsuario) {
		StoredProcedureHelper storedHelper = null;
				
		String nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.SP_CNSF_RepREASyPerdida";
		
		try {
			
			String[] nombreParametros = {"fechaCorte", "tipoCambio", "usuario"};
			Object[] valorParametros={fechaCorte, tipoCambio, nombreUsuario};
			
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSContraparteDTO.class.getCanonicalName(),"resultado",	"resultado");
			
			
			String descripcionParametros = obtenerDescripcionParametros(nombreParametros,valorParametros);
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			LogDeMidasEJB3.log("Generando reporte. SP a invocar: "+nombreSP+", Parametros: "+descripcionParametros, Level.INFO, null);
			
						
			listaResultado = storedHelper.obtieneListaResultados();
			
			
			LogDeMidasEJB3.log("Finaliza consulta del reporte. SP invocado: "+nombreSP+", Parametros: "+descripcionParametros+
					"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
											
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log("Ocurri� un error al consultar el reporte. SP invocado: "+nombreSP, Level.SEVERE, e); 
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReporteRCSContraparteDTO.class, codErr, descErr);
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Ocurri� un error al consultar el reporte. SP invocado: "+nombreSP, Level.SEVERE, e);
			
		}
		
		return listaResultado;
	}
	
	/**
	 * Procesa la informacion de los reportes Contraparte.
	 * @param fechaCorte
	 * @param tipoCambio
	 * @param nombreUsuario
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void procesarReporte(String fechaCorte, BigDecimal tipoCambio, String nombreUsuario) throws Exception {
		
		StoredProcedureHelper storedHelper = null;
		String nombreSP = "MIDAS.PKG_SOLVENCIA_CNSF.SP_CNSF_DISTRIBUCION_MIDAS";
		
		try {
							
			String[] nombreParametros = {"pfCorte","ptipocambio", "pidUsuario"};
			Object[] valorParametros={fechaCorte, tipoCambio, nombreUsuario};
			
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			String descripcionParametros = obtenerDescripcionParametros(nombreParametros,valorParametros);
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			
			LogDeMidasEJB3.log("Procesando reporte. SP a invocar: "+nombreSP+", Parametros: "+descripcionParametros, Level.INFO, null);
			
			storedHelper.ejecutaActualizar();
			
			LogDeMidasEJB3.log("Finaliza ejecucion del reporte. SP invocado: "+nombreSP+", Parametros: "+descripcionParametros, Level.INFO, null);
			
												
		} catch (SQLException e) {
			
			LogDeMidasEJB3.log("Ocurrio un error al consultar el reporte. SP invocado: "+nombreSP, Level.SEVERE, e); 
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(nombreUsuario,StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, ReporteRCSContraparteDTO.class, codErr, descErr);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Ocurrio un error al consultar el reporte. SP invocado: "+nombreSP, Level.SEVERE, e);
			throw e;
		}
	}


	public String obtenerDescripcionParametros(String[] nombreParametros,
			Object[] valorParametros) {
		StringBuilder descripcionParametros = new  StringBuilder("");
		for(int i=0;i<nombreParametros.length;i++){
			descripcionParametros.append(" [").append(nombreParametros[i]).append("] : ").append(valorParametros[i]);
		}
		return descripcionParametros.toString();
	}
	
	/**
	 * Find all ReporteRCSContraparteDTO entities.
	 * @param fechaCorte
	 * @return List<ReporteRCSContraparteDTO> all ReporteRCSContraparteDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReporteRCSContraparteDTO> findAll(String fechaCorte) {
		Date fCorte = Utilerias.obtenerFechaDeCadena(fechaCorte);
		LogDeMidasEJB3.log(
				"finding all ReporteRCSContraparteDTO instances",
				Level.INFO, null);
		try {
			String queryString = "select model from ReporteRCSContraparteDTO model where model.fechaCorte = ?1 order by model.fechaCorte desc";
			Query query = entityManager.createQuery(queryString);
			query.setParameter(1, fCorte, TemporalType.DATE);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find ReporteRCSContraparteDTO entities by ID.
	 * @param id 
	 * @return instance ReporteRCSContraparteDTO entities
	 */
	public ReporteRCSContraparteDTO findById(Integer id) {
		LogDeMidasEJB3.log(
				"finding ReporteRCSContraparteDTO instance with id: " + id,
				Level.INFO, null);
		try {
			ReporteRCSContraparteDTO instance = entityManager.find(
					ReporteRCSContraparteDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Delete a persistent ReporteRCSContraparteDTO entity.
	 * 
	 * @param entity
	 *            ReporteRCSContraparteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ReporteRCSContraparteDTO entity) {
		LogDeMidasEJB3.log("deleting ReporteRCSContraparteDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					ReporteRCSContraparteDTO.class, entity
							.getIdcontrato());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find all CargaContraParteDTO entities.
	 * @param fechaCorte
	 * @return List<CargaContraParteDTO> all CargaContraParteDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CargaContraParteDTO> findAllCarga(String fechaCorte) {
		Date fCorte = Utilerias.obtenerFechaDeCadena(fechaCorte);
		LogDeMidasEJB3.log(
				"finding all CargaContraParteDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from CargaContraParteDTO model where model.fechaFin = ?1 order by model.fechaFin desc";
			Query query = entityManager.createQuery(queryString);
			query.setParameter(1, fCorte, TemporalType.DATE);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * update CargaContraParteDTO entities.
	 * @param idCarga
	 * @param estatusCarga
	 */
	public void actualizarEstatusCarga(int idCarga, int estatusCarga) {
		
		LogDeMidasEJB3.log(
				"update CargaContraParteDTO instances",
				Level.INFO, null);
		
		
		try {
			StringBuilder queryString=new StringBuilder("");
			queryString.append("UPDATE CargaContraParteDTO model");
			queryString.append(" SET model.candado = :paramenterCandado");
			queryString.append(" WHERE model.idcarga = :paramenterIdCarga");
			
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("paramenterCandado", estatusCarga);
			query.setParameter("paramenterIdCarga", idCarga);
			query.executeUpdate();
			entityManager.flush();
		
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update  CargaContraParteDTO failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Obtiene la cantidad de registros.
	 * @param fechaCorte
	 * @return La cantidad de registros
	 */
	public int obtenerRegistros(Date fechaCorte) {
		Integer registros = 0;
		
		try {		
			
						
			StringBuilder queryString=new StringBuilder("");
			queryString.append("SELECT  count(model) FROM DistribucionMidasDTO model ");
			queryString.append(" WHERE model.fCorte = :fecha");
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("fecha", fechaCorte, TemporalType.DATE);
			
			registros = ((Long)query.getSingleResult()).intValue();
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de consultar la tabla MIDAS.CNSF_SIN_MIDAS_DETALLE", Level.SEVERE, e);
		}
		return registros;

	}
	
	/**
	 * Obtiene la informacion de los archivos cargados.
	 * @param fechaFinal
	 * @param tipoArchivo
	 * @return Lista con en estatus de los archivos cargados. 
	 */
	@SuppressWarnings("unchecked")
	public List<String> obtenerArchivos(Date fechaFinal){
		List<String> listaResultado = null;
		
				
		try {	
			StringBuilder queryString=new StringBuilder("");
			queryString.append("SELECT model.reporte FROM CargaContraParteDTO model ");
			queryString.append("WHERE  model.fechaFin = ?1 ");
			queryString.append("order by model.fechaFin desc ");
			
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter(1, fechaFinal, TemporalType.DATE);
						
			listaResultado = (List<String>) query.getResultList();
			
							
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de obtenerReporteExcel ", Level.SEVERE, e);
		}
		return listaResultado;						
		
	}
	
	/**
	 * Obtiene el desgloce del reporte.
	 * @param fechaFinal
	 * @return Lista de los registros por concepto. 
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> obtenerDesgloce(Date fechaFinal, BigDecimal tipoCambio){
		List<Object[]> listaResultado = null;
		try {	
			StringBuilder queryString=new StringBuilder("");
			
			queryString.append("SELECT reporte.sistema, SUM(reporte.monto) AS reserva_origen, ");
			queryString.append("DECODE(INSTR(UPPER(reporte.sistema), UPPER('USD'),1),0, SUM(reporte.monto), SUM(reporte.monto*"+tipoCambio+")) as reserva_pesos FROM( ");
			queryString.append("SELECT reasegurador, monto_distribucion AS monto, rgre, 'VIDA MN' AS sistema ");
			queryString.append("FROM MIDAS.CNSF_SIN_DISTRIB_VIDA ");
			queryString.append("WHERE monto_distribucion != 0 ");
			queryString.append("AND moneda LIKE 'NACIONAL' ");
			queryString.append("AND TRUNC(fecha_corte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, monto_distribucion AS monto, rgre, 'VIDA USD' ");
			queryString.append("FROM MIDAS.CNSF_SIN_DISTRIB_VIDA ");
			queryString.append("WHERE monto_distribucion != 0 ");
			queryString.append("AND moneda LIKE 'DOLARES US' ");
			queryString.append("AND TRUNC(fecha_corte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, reserva, rgre, 'SISE MN' ");
			queryString.append("FROM MIDAS.CNSF_SIN_DISTRIB_SISE ");
			queryString.append("WHERE reserva != 0 ");
			queryString.append("AND moneda LIKE 'NACIONAL' ");
			queryString.append("AND TRUNC(fecha_corte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, reserva, rgre, 'SISE USD' ");
			queryString.append("FROM MIDAS.CNSF_SIN_DISTRIB_SISE ");
			queryString.append("WHERE reserva != 0 ");
			queryString.append("AND moneda LIKE 'DOLARES US' ");
			queryString.append("AND TRUNC(fecha_corte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, reserva_pendiente_reas AS monto, cnsf AS rgre, 'MIDAS MN' ");
			queryString.append("FROM MIDAS.CNSF_SIN_MIDAS_DETALLE ");
			queryString.append("WHERE reserva_pendiente_reas != 0 ");
			queryString.append("AND moneda LIKE 'NACIONAL' ");
			queryString.append("AND TRUNC(fCorte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, reserva_pendiente_reas AS monto, cnsf AS rgre, 'MIDAS USD' ");
			queryString.append("FROM MIDAS.CNSF_SIN_MIDAS_DETALLE ");
			queryString.append("WHERE reserva_pendiente_reas != 0 ");
			queryString.append("AND moneda LIKE 'DOLARES US' ");
			queryString.append("AND TRUNC(fCorte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, reserva AS monto, cnsf AS rgre, 'AJUSTES MN' ");
			queryString.append("FROM MIDAS.CNSF_SIN_AJUSTES_MANUALES ");
			queryString.append("WHERE reserva != 0 ");
			queryString.append("AND moneda LIKE 'NACIONAL' ");
			queryString.append("AND TRUNC(fecha_corte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, reserva AS monto, cnsf AS rgre, 'AJUSTES USD' ");
			queryString.append("FROM MIDAS.CNSF_SIN_AJUSTES_MANUALES ");
			queryString.append("WHERE reserva != 0 ");
			queryString.append("AND moneda LIKE 'DOLARES US' ");
			queryString.append("AND TRUNC(fecha_corte) = ?1 ");
			queryString.append(")reporte ");
			queryString.append("GROUP BY reporte.sistema ");
			queryString.append("ORDER BY reporte.sistema ASC ");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(1, fechaFinal, TemporalType.DATE);
						
			listaResultado = query.getResultList();
			
							
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de obtenerReporteExcel ", Level.SEVERE, e);
		}
		return listaResultado;						
		
	}
	
	/**
	 * Obtiene el integracion por concepto.
	 * @param fechaFinal
	 * @return Lista de los registros por concepto. 
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> obtenerIntegracion(Date fechaFinal, BigDecimal tipoCambio, String concepto){
		List<Object[]> listaResultado = null;
		try {	
			StringBuilder queryString=new StringBuilder("");
			if(concepto.equalsIgnoreCase("SISE")){
				queryString.append("SELECT reporte.siniestro, reporte.reasegurador, reporte.rgre, reporte.moneda, reporte.reserva ");
				queryString.append("FROM MIDAS.CNSF_SIN_DISTRIB_SISE reporte ");
				queryString.append("WHERE reporte.reserva != 0 ");
				queryString.append("AND TRUNC(reporte.fecha_corte) = ?1 ");
				queryString.append("ORDER BY reporte.moneda , reporte.siniestro ASC ");
			}else if(concepto.equalsIgnoreCase("VIDA")){
				queryString.append("SELECT reporte.siniestro, reporte.reasegurador, reporte.rgre, reporte.moneda, reporte.monto_distribucion ");
				queryString.append("FROM MIDAS.CNSF_SIN_DISTRIB_VIDA reporte ");
				queryString.append("WHERE reporte.monto_distribucion != 0 ");
				queryString.append("AND TRUNC(reporte.fecha_corte) = ?1 ");
				queryString.append("ORDER BY reporte.moneda , reporte.siniestro ASC ");
			}else if(concepto.equalsIgnoreCase("MIDAS")){
				queryString.append("SELECT reporte.siniestro, reporte.reasegurador, reporte.cnsf, reporte.moneda, reporte.reserva_pendiente_reas ");
				queryString.append("FROM MIDAS.CNSF_SIN_MIDAS_DETALLE reporte ");
				queryString.append("WHERE reporte.reserva_pendiente_reas != 0 ");
				queryString.append("AND TRUNC(reporte.fcorte) = ?1 ");
				queryString.append("ORDER BY reporte.moneda , reporte.siniestro ASC ");
			}
						
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(1, fechaFinal, TemporalType.DATE);
			
			if(query.getResultList() != null){
				listaResultado = query.getResultList();
			}else{
				listaResultado = new ArrayList<Object[]>();;
			}
							
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de obtenerReporteExcel ", Level.SEVERE, e);
		}
		return listaResultado;						
		
	}
	
	/**
	 * Obtiene el integracion NRS.
	 * @param fechaFinal
	 * @return Lista de los registros por concepto. 
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> obtenerIntegracionNRS(Date fechaFinal, BigDecimal tipoCambio){
		List<Object[]> listaResultado = null;
		try {	
			StringBuilder queryString=new StringBuilder("");
			
			queryString.append("SELECT reporte.rgre, reporte.reasegurador, reporte.sistema,  ");
			queryString.append("DECODE(INSTR(UPPER(reporte.sistema), UPPER('USD'),1),0, SUM(reporte.reserva), SUM(reporte.reserva*"+tipoCambio+")) as reserva FROM( ");
			queryString.append("SELECT calificacion.rgre, calificacion.reasegurador, calificacion.reserva, ");
			queryString.append("CASE WHEN length(calificacion.rgre) > 5 THEN ");
			queryString.append("CASE WHEN (SELECT MIDAS.PKG_SOLVENCIA_CNSF.get_CalificacionCNSF(calificacion.rgre) FROM dual) = 99 THEN 8 ");
			queryString.append("ELSE (SELECT MIDAS.PKG_SOLVENCIA_CNSF.get_CalificacionCNSF(calificacion.rgre) FROM dual) END ");
			queryString.append("ELSE 0 END calificacion, calificacion.sistema FROM( ");
			queryString.append("SELECT agrupador.rgre, agrupador.reasegurador, SUM(agrupador.monto) reserva, agrupador.sistema FROM( ");
			queryString.append("SELECT CASE WHEN length(reporte.rgre) > 5 THEN ");
			queryString.append("DECODE(INSTR(UPPER(reporte.rgre), UPPER('R'),1),0,'RGRE-'||reporte.rgre, reporte.rgre) ");
			queryString.append("WHEN length(reporte.rgre) < 3 THEN 'S00'||reporte.rgre ");
			queryString.append("WHEN length(reporte.rgre) = 4 THEN 'S'||reporte.rgre ELSE ");
			queryString.append("reporte.rgre END rgre, reporte.reasegurador, SUM(reporte.monto) Monto, reporte.sistema FROM ( ");
			queryString.append("SELECT reasegurador, monto_distribucion AS monto, rgre, 'VIDA MN' AS sistema ");
			queryString.append("FROM MIDAS.CNSF_SIN_DISTRIB_VIDA ");
			queryString.append("WHERE monto_distribucion != 0 ");
			queryString.append("AND moneda LIKE 'NACIONAL' ");
			queryString.append("AND TRUNC(fecha_corte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, monto_distribucion AS monto, rgre, 'VIDA USD' ");
			queryString.append("FROM MIDAS.CNSF_SIN_DISTRIB_VIDA ");
			queryString.append("WHERE monto_distribucion != 0 ");
			queryString.append("AND moneda LIKE 'DOLARES US' ");
			queryString.append("AND TRUNC(fecha_corte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, reserva, rgre, 'SISE MN' ");
			queryString.append("FROM MIDAS.CNSF_SIN_DISTRIB_SISE ");
			queryString.append("WHERE reserva != 0 ");
			queryString.append("AND moneda LIKE 'NACIONAL' ");
			queryString.append("AND TRUNC(fecha_corte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, reserva, rgre, 'SISE USD' ");
			queryString.append("FROM MIDAS.CNSF_SIN_DISTRIB_SISE ");
			queryString.append("WHERE reserva != 0 ");
			queryString.append("AND moneda LIKE 'DOLARES US' ");
			queryString.append("AND TRUNC(fecha_corte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, reserva_pendiente_reas AS monto, cnsf AS rgre, 'MIDAS MN' ");
			queryString.append("FROM MIDAS.CNSF_SIN_MIDAS_DETALLE ");
			queryString.append("WHERE reserva_pendiente_reas != 0 ");
			queryString.append("AND moneda LIKE 'NACIONAL' ");
			queryString.append("AND TRUNC(fCorte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, reserva_pendiente_reas AS monto, cnsf AS rgre, 'MIDAS USD' ");
			queryString.append("FROM MIDAS.CNSF_SIN_MIDAS_DETALLE ");
			queryString.append("WHERE reserva_pendiente_reas != 0 ");
			queryString.append("AND moneda LIKE 'DOLARES US' ");
			queryString.append("AND TRUNC(fCorte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, reserva AS monto, cnsf AS rgre, 'AJUSTES MN' ");
			queryString.append("FROM MIDAS.CNSF_SIN_AJUSTES_MANUALES ");
			queryString.append("WHERE reserva != 0 ");
			queryString.append("AND moneda LIKE 'NACIONAL' ");
			queryString.append("AND TRUNC(fecha_corte) = ?1 ");
			queryString.append("UNION ALL ");
			queryString.append("SELECT reasegurador, reserva AS monto, cnsf AS rgre, 'AJUSTES USD' ");
			queryString.append("FROM MIDAS.CNSF_SIN_AJUSTES_MANUALES ");
			queryString.append("WHERE reserva != 0 ");
			queryString.append("AND moneda LIKE 'DOLARES US' ");
			queryString.append("AND TRUNC(fecha_corte) = ?1 ");
			queryString.append(")reporte ");
			queryString.append("GROUP BY reporte.rgre, reporte.reasegurador, reporte.sistema)agrupador ");
			queryString.append("GROUP BY agrupador.rgre, agrupador.reasegurador, agrupador.sistema)calificacion ");
			queryString.append(")reporte WHERE reporte.calificacion = 8 ");
			queryString.append("GROUP BY reporte.rgre, reporte.reasegurador, reporte.sistema ");
			queryString.append("ORDER BY reporte.rgre ASC ");
				
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(1, fechaFinal, TemporalType.DATE);
			
			if(query.getResultList() != null){
				listaResultado = query.getResultList();
			}else{
				listaResultado = new ArrayList<Object[]>();;
			}
							
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de obtenerReporteExcel ", Level.SEVERE, e);
		}
		return listaResultado;						
		
	}
		
}
