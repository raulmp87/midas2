package mx.com.afirme.midas.reaseguro.reportes.siniestrosconreaseguro.prioridad;

// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity PrioridadSiniestroReaseguroDTO.
 * @see .PrioridadSiniestroReaseguroDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class PrioridadSiniestroReaseguroFacade  implements PrioridadSiniestroReaseguroFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved PrioridadSiniestroReaseguroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PrioridadSiniestroReaseguroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(PrioridadSiniestroReaseguroDTO entity) {
    				LogUtil.log("saving PrioridadSiniestroReaseguroDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent PrioridadSiniestroReaseguroDTO entity.
	  @param entity PrioridadSiniestroReaseguroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PrioridadSiniestroReaseguroDTO entity) {
    				LogUtil.log("deleting PrioridadSiniestroReaseguroDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(PrioridadSiniestroReaseguroDTO.class, entity.getIdtmprioridad());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved PrioridadSiniestroReaseguroDTO entity and return it or a copy of it to the sender. 
	 A copy of the PrioridadSiniestroReaseguroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PrioridadSiniestroReaseguroDTO entity to update
	 @return PrioridadSiniestroReaseguroDTO the persisted PrioridadSiniestroReaseguroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public PrioridadSiniestroReaseguroDTO update(PrioridadSiniestroReaseguroDTO entity) {
    				LogUtil.log("updating PrioridadSiniestroReaseguroDTO instance", Level.INFO, null);
	        try {
            PrioridadSiniestroReaseguroDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public PrioridadSiniestroReaseguroDTO findById( BigDecimal id) {
    				LogUtil.log("finding PrioridadSiniestroReaseguroDTO instance with id: " + id, Level.INFO, null);
	        try {
            PrioridadSiniestroReaseguroDTO instance = entityManager.find(PrioridadSiniestroReaseguroDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all PrioridadSiniestroReaseguroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PrioridadSiniestroReaseguroDTO property to query
	  @param value the property value to match
	  	  @return List<PrioridadSiniestroReaseguroDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<PrioridadSiniestroReaseguroDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding PrioridadSiniestroReaseguroDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from PrioridadSiniestroReaseguroDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all PrioridadSiniestroReaseguroDTO entities.
	  	  @return List<PrioridadSiniestroReaseguroDTO> all PrioridadSiniestroReaseguroDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<PrioridadSiniestroReaseguroDTO> findAll(
		) {
					LogUtil.log("finding all PrioridadSiniestroReaseguroDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from PrioridadSiniestroReaseguroDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}