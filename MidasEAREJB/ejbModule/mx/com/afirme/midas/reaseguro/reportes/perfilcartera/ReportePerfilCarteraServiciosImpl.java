package mx.com.afirme.midas.reaseguro.reportes.perfilcartera;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless
public class ReportePerfilCarteraServiciosImpl implements ReportePerfilCarteraServicios {

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<RegistroPerfilCarteraDTO> obtenerRegistrosPerfilCartera(Date fechaInicial, 
			Date fechaFinal, Double tipoCambio,Integer tipoReporte,String nombreUsuario) throws Exception{
		StoredProcedureHelper storedHelper = null;
		final String nombreSP = "MIDAS.PKGREA_REPORTES.spREA_RptPerfilCartera";
		String descripcionParametros = "";
		
		if(fechaInicial == null || fechaFinal == null || tipoCambio == null)
			throw new RuntimeException("Datos no v�lidos: null");
		
		try {
			final String [] nombreParametrosSP = {"pFechaInicial","pFechaFinal","pTipoCambio","pTipoReporte"};
			final Object [] parametrosSP = {fechaInicial,fechaFinal,tipoCambio,tipoReporte};
			
			descripcionParametros=obtenerDescripcionParametros(nombreParametrosSP,parametrosSP);
			
			LogDeMidasEJB3.log("Entrando a ReportePerfilCarteraServicios.obtenerRegistrosPerfilCartera. Ejecutando SP: " + nombreSP+", parametros "+descripcionParametros, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper(nombreSP);
			
			String [] mapeoResultadoCursor ={
					"orden","idtcsubramo","descsubramo","rangomin",
					"rangomax","sumatoriasumaasegurada","totalsumaasegurada","promediosumaasegurada",
					"sumatoriaprimaneta","totalprimaneta","promedioprimaneta","porcentajeprimaneta",
					"porcentajeacumprimaneta","cantidadcumulos","totalcumulos","porcentajecumulos",
					"porcentajeacumcumulos","totalmontosiniestros",
					"cantidadpolizas","totalpolizas","cantidadsiniestros","TOTALSINIESTROS",
					"porcentajesiniestros","sumatoriamontosiniestros","porcentajemontosin","porcentajeacummontosin",
					"frecuencia","siniestralidad","costopromedio","severidad"
					};
			
			String [] mapeoResultadoDTO ={
					"orden","idTcSubRamo","descripcionSubRamo","rangoMinimo",
					"rangoMaximo","sumatoriaSumaAsegurada","totalSumaAsegurada","promedioSumaAsegurada",
					"sumatoriaPrimaNeta","totalPrimaNeta","promedioPrimaNeta","porcentajePrimaNeta",
					"porcentajeAcumuladoPrimaNeta","cantidadCumulos","totalCumulos","porcentajeCantidadCumulos",
					"porcentajeAcumuladoCantidadCumulos","totalMontoSiniestros",
					"cantidadPolizas","totalPolizas","cantidadSiniestros","totalSiniestros",
					"porcentajeCantidadSiniestros","sumatoriaMontoSiniestro","porcentajeMontoSiniestros","porcentajeAcumuladoMontoSiniestros",
					"frecuencia","siniestralidad","costoPromedio","severidad"
					};
			
			storedHelper.estableceMapeoResultados(RegistroPerfilCarteraDTO.class.getCanonicalName(),
						mapeoResultadoDTO,mapeoResultadoCursor);
			
			for(int i=0;i<nombreParametrosSP.length; i++){
				storedHelper.estableceParametro(nombreParametrosSP[i], parametrosSP[i]);
			}
			
			List<RegistroPerfilCarteraDTO> registrosPerfilCarteraList = storedHelper.obtieneListaResultados();
			LogDeMidasEJB3.log("Saliendo de ReportePerfilCarteraServicios.obtenerRegistrosPerfilCartera. SP Ejecutado: " + nombreSP+", parametros "+descripcionParametros+ ", Resultado: "+(registrosPerfilCarteraList != null ? registrosPerfilCarteraList.size()+" registro(s)" : registrosPerfilCarteraList), Level.INFO, null);
			return registrosPerfilCarteraList;
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, RegistroPerfilCarteraDTO.class, codErr, descErr);
			LogDeMidasEJB3.log("Excepcion en BD de ReportePerfilCarteraServicios.obtenerRegistrosPerfilCartera al invocar: "+nombreSP+", parametros "+descripcionParametros, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en ReportePerfilCarteraServicios.obtenerRegistrosPerfilCartera al invocar: "+nombreSP+", parametros "+descripcionParametros, Level.WARNING, e);
			throw e;
		}
	}


	public String obtenerDescripcionParametros(String[] nombreParametrosSP,
			Object[] parametrosSP) {
	StringBuilder 	descripcionParametros= new StringBuilder("(");
		for(int i=0;i<nombreParametrosSP.length; i++){
			descripcionParametros.append(nombreParametrosSP[i]).append(": ").append(parametrosSP[i]).append(", ");
		}
		descripcionParametros.append(")");
		return descripcionParametros.toString();
	}


	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<AgrupacionSubRamoPerfilCarteraDTO> obtenerRegistrosAgrupadosPerfilCartera(Date fechaInicial, 
			Date fechaFinal, Double tipoCambio,Integer tipoReporte,String nombreUsuario) throws Exception{
		
		try{
		List<RegistroPerfilCarteraDTO> listaRegistrosDesglosados = obtenerRegistrosPerfilCartera(fechaInicial, fechaFinal, tipoCambio, tipoReporte, nombreUsuario);
		
		Map<Integer, AgrupacionSubRamoPerfilCarteraDTO> mapaAgrupaciones = new HashMap<Integer, AgrupacionSubRamoPerfilCarteraDTO>();
		
		if(listaRegistrosDesglosados != null && !listaRegistrosDesglosados.isEmpty()){
			
			for(RegistroPerfilCarteraDTO registro : listaRegistrosDesglosados){
				
				AgrupacionSubRamoPerfilCarteraDTO agrupacionSubRamo = mapaAgrupaciones.get(registro.getIdTcSubRamo());
				
				if(agrupacionSubRamo == null){
					agrupacionSubRamo = new AgrupacionSubRamoPerfilCarteraDTO(
							registro.getIdTcSubRamo(),registro.getDescripcionSubRamo());
					agrupacionSubRamo.setSumatoriaSumaAsegurada(registro.getTotalSumaAsegurada());
					agrupacionSubRamo.setSumatoriaPrimaNeta(registro.getTotalPrimaNeta());
					
					agrupacionSubRamo.setTotalCumulos(registro.getTotalCumulos());
					
					agrupacionSubRamo.setTotalPolizas(registro.getTotalPolizas());
					agrupacionSubRamo.setTotalSiniestros(registro.getTotalSiniestros());
					
					agrupacionSubRamo.setPorcentajeCantidadSiniestros(BigDecimal.ZERO);
					agrupacionSubRamo.setSumatoriaMontoSiniestros(registro.getTotalMontoSiniestros());
					
					mapaAgrupaciones.put(registro.getIdTcSubRamo(), agrupacionSubRamo);
				}
				
				agrupacionSubRamo.setPorcentajePrimaNeta(registro.getPorcentajeAcumuladoPrimaNeta());
				agrupacionSubRamo.setPorcentajeCumulos(registro.getPorcentajeAcumuladoCantidadCumulos());
				agrupacionSubRamo.setPorcentajeMontoSiniestros(registro.getPorcentajeAcumuladoMontoSiniestros());
				
				if(registro.getPorcentajeCantidadSiniestros() != null){
					agrupacionSubRamo.setPorcentajeCantidadSiniestros(
							agrupacionSubRamo.getPorcentajeCantidadSiniestros().add(
									registro.getPorcentajeCantidadSiniestros()));
				}
				
				
				if(agrupacionSubRamo.getSumatoriaSumaAsegurada() != null && agrupacionSubRamo.getTotalCumulos() != null &&
						agrupacionSubRamo.getTotalCumulos().intValue() != 0){
					agrupacionSubRamo.setPromedioSumaAsegurada(
							agrupacionSubRamo.getSumatoriaSumaAsegurada().divide(
									new BigDecimal(agrupacionSubRamo.getTotalCumulos()),10,RoundingMode.HALF_UP));
				}
				
				//frecuencia
				if(agrupacionSubRamo.getTotalSiniestros() != null && agrupacionSubRamo.getTotalCumulos() != null &&
						agrupacionSubRamo.getTotalCumulos().intValue() != 0){
					agrupacionSubRamo.setFrecuencia(new BigDecimal (agrupacionSubRamo.getTotalSiniestros()).divide(
							new BigDecimal(agrupacionSubRamo.getTotalCumulos()),10,RoundingMode.HALF_UP));
				}
				
				//siniestralidad
				if(agrupacionSubRamo.getSumatoriaMontoSiniestros() != null && agrupacionSubRamo.getSumatoriaPrimaNeta() != null &&
						agrupacionSubRamo.getSumatoriaPrimaNeta().compareTo(BigDecimal. ZERO) != 0){
					agrupacionSubRamo.setSiniestralidad(agrupacionSubRamo.getSumatoriaMontoSiniestros().divide(
							agrupacionSubRamo.getSumatoriaPrimaNeta(),10,RoundingMode.HALF_UP));
				}
				
				//costo promedio
				if(agrupacionSubRamo.getSumatoriaMontoSiniestros() != null && agrupacionSubRamo.getTotalSiniestros() != null &&
						agrupacionSubRamo.getTotalSiniestros().intValue() != 0){
					agrupacionSubRamo.setCostoPromedio(agrupacionSubRamo.getSumatoriaMontoSiniestros().divide(
							new BigDecimal(agrupacionSubRamo.getTotalSiniestros()),10,RoundingMode.HALF_UP));
				}
				
				//severidad
				if(agrupacionSubRamo.getSumatoriaMontoSiniestros() != null && agrupacionSubRamo.getPromedioSumaAsegurada()!= null &&
						agrupacionSubRamo.getPromedioSumaAsegurada().compareTo(BigDecimal.ZERO) != 0){
					agrupacionSubRamo.setSeveridad(agrupacionSubRamo.getSumatoriaMontoSiniestros().divide(
							agrupacionSubRamo.getPromedioSumaAsegurada(),10,RoundingMode.HALF_UP));
				}
				
				agrupacionSubRamo.getListaRangosSubRamo().add(registro);		
			}
			
		}
		
		List<AgrupacionSubRamoPerfilCarteraDTO> listaResultado = new ArrayList<AgrupacionSubRamoPerfilCarteraDTO>();
		for(Integer clave : mapaAgrupaciones.keySet()){
			
			listaResultado.add(mapaAgrupaciones.get(clave));
			
		}
		
		return listaResultado;
		
		}
		catch(Exception e){
			LogDeMidasEJB3.log("Error al generar informaci�n para reporte PerfilCartera. ", Level.SEVERE, e);
			throw e;
		}
	}
}
