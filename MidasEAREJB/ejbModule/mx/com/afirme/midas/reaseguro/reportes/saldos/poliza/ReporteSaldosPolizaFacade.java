package mx.com.afirme.midas.reaseguro.reportes.saldos.poliza;
// default package

import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ReporteSaldosPolizaDTO.
 * @see .ReporteSaldosPolizaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ReporteSaldosPolizaFacade  implements ReporteSaldosPolizaFacadeRemote {
    @PersistenceContext private EntityManager entityManager;
    
    public ReporteSaldosPolizaDTO findById( Double id) {
    				LogDeMidasEJB3.log("finding ReporteSaldosPolizaDTO instance with id: " + id, Level.INFO, null);
	        try {
            ReporteSaldosPolizaDTO instance = entityManager.find(ReporteSaldosPolizaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ReporteSaldosPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReporteSaldosPolizaDTO property to query
	  @param value the property value to match
	  	  @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the  the row index in the query result-set to begin collecting the results. rowStartIdxAndCount[1] specifies the the maximum number of results to return.  
	  	  @return List<ReporteSaldosPolizaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ReporteSaldosPolizaDTO> findByProperty(String propertyName, final Object value
        , final int...rowStartIdxAndCount
        ) {
    				LogDeMidasEJB3.log("finding ReporteSaldosPolizaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ReporteSaldosPolizaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {	
						int rowStartIdx = Math.max(0,rowStartIdxAndCount[0]);
						if (rowStartIdx > 0) {
							query.setFirstResult(rowStartIdx);
						}
		
						if (rowStartIdxAndCount.length > 1) {
					    	int rowCount = Math.max(0,rowStartIdxAndCount[1]);
					    	if (rowCount > 0) {
					    		query.setMaxResults(rowCount);    
					    	}
						}
					}										
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ReporteSaldosPolizaDTO entities.
	  	  @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the  the row index in the query result-set to begin collecting the results. rowStartIdxAndCount[1] specifies the the maximum count of results to return.  
	  	  @return List<ReporteSaldosPolizaDTO> all ReporteSaldosPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ReporteSaldosPolizaDTO> findAll(
		final int...rowStartIdxAndCount
		) {
					LogDeMidasEJB3.log("finding all ReporteSaldosPolizaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from ReporteSaldosPolizaDTO model";
								Query query = entityManager.createQuery(queryString);
					if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {	
						int rowStartIdx = Math.max(0,rowStartIdxAndCount[0]);
						if (rowStartIdx > 0) {
							query.setFirstResult(rowStartIdx);
						}
		
						if (rowStartIdxAndCount.length > 1) {
					    	int rowCount = Math.max(0,rowStartIdxAndCount[1]);
					    	if (rowCount > 0) {
					    		query.setMaxResults(rowCount);    
					    	}
						}
					}										
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	
	@SuppressWarnings("unchecked")
    public List<ReporteSaldosPolizaDTO> consultarSaldosPoliza(Date fechaInicial,Date fechaFinal,boolean soloFacultativos,boolean soloAutomaticos,boolean incluirRetencion,
    		Double idMoneda, final int...rowStartIdxAndCount) {
		LogDeMidasEJB3.log("buscando registros ReporteSaldosPolizaDTO del: " + fechaInicial + " al: " + fechaFinal + 
				(soloFacultativos ? ", incluyendo solo registros de movimientos facultativos" : 
					(soloAutomaticos ? ", incluyendo solo registros de contratos automaticos" : "incluyendo todos los registros")), Level.INFO, null);
		try {
			String queryString = "select model from ReporteSaldosPolizaDTO model where model.fechaMovimiento >= :fechaInicial and " +
					"model.fechaMovimiento <= :fechaFinal ";
			if(soloFacultativos){
				queryString += " and model.idTcTipoReaseguro = 3 ";
			}
			else if(soloAutomaticos){
				queryString += " and model.idTcTipoReaseguro in (1,2"+(incluirRetencion ? ",4":"")+") ";
			}else if(!incluirRetencion){
				queryString += " and model.idTcTipoReaseguro <> 4 ";
			}
			if(idMoneda != null){
				queryString += " and model.idMoneda = :idMoneda ";
			}
			Query query = entityManager.createQuery(queryString);
			query.setParameter("fechaInicial", fechaInicial);
			query.setParameter("fechaFinal", fechaFinal);
			if(idMoneda != null)
				query.setParameter("idMoneda", idMoneda);
			
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {	
				int rowStartIdx = Math.max(0,rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}
				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0,rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);    
					}
				}
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Falla consulta de registros ReporteSaldosPolizaDTO. ", Level.SEVERE, re);
			throw re;
		}
	}
}