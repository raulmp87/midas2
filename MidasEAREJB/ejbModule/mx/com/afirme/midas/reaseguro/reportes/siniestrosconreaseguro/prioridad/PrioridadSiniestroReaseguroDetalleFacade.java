package mx.com.afirme.midas.reaseguro.reportes.siniestrosconreaseguro.prioridad;


import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity PrioridadSiniestroReaseguroDetalleDTO.
 * @see .PrioridadSiniestroReaseguroDetalleDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class PrioridadSiniestroReaseguroDetalleFacade  implements PrioridadSiniestroReaseguroDetalleFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved PrioridadSiniestroReaseguroDetalleDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PrioridadSiniestroReaseguroDetalleDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(PrioridadSiniestroReaseguroDetalleDTO entity) {
    				LogUtil.log("saving PrioridadSiniestroReaseguroDetalleDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogUtil.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent PrioridadSiniestroReaseguroDetalleDTO entity.
	  @param entity PrioridadSiniestroReaseguroDetalleDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PrioridadSiniestroReaseguroDetalleDTO entity) {
    				LogUtil.log("deleting PrioridadSiniestroReaseguroDetalleDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(PrioridadSiniestroReaseguroDetalleDTO.class, entity.getIdtdprioridad());
            entityManager.remove(entity);
            			LogUtil.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogUtil.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved PrioridadSiniestroReaseguroDetalleDTO entity and return it or a copy of it to the sender. 
	 A copy of the PrioridadSiniestroReaseguroDetalleDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PrioridadSiniestroReaseguroDetalleDTO entity to update
	 @return PrioridadSiniestroReaseguroDetalleDTO the persisted PrioridadSiniestroReaseguroDetalleDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public PrioridadSiniestroReaseguroDetalleDTO update(PrioridadSiniestroReaseguroDetalleDTO entity) {
    				LogUtil.log("updating PrioridadSiniestroReaseguroDetalleDTO instance", Level.INFO, null);
	        try {
            PrioridadSiniestroReaseguroDetalleDTO result = entityManager.merge(entity);
            			LogUtil.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogUtil.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public PrioridadSiniestroReaseguroDetalleDTO findById( BigDecimal id) {
    				LogUtil.log("finding PrioridadSiniestroReaseguroDetalleDTO instance with id: " + id, Level.INFO, null);
	        try {
            PrioridadSiniestroReaseguroDetalleDTO instance = entityManager.find(PrioridadSiniestroReaseguroDetalleDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogUtil.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all PrioridadSiniestroReaseguroDetalleDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PrioridadSiniestroReaseguroDetalleDTO property to query
	  @param value the property value to match
	  	  @return List<PrioridadSiniestroReaseguroDetalleDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<PrioridadSiniestroReaseguroDetalleDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogUtil.log("finding PrioridadSiniestroReaseguroDetalleDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from PrioridadSiniestroReaseguroDetalleDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all PrioridadSiniestroReaseguroDetalleDTO entities.
	  	  @return List<PrioridadSiniestroReaseguroDetalleDTO> all PrioridadSiniestroReaseguroDetalleDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<PrioridadSiniestroReaseguroDetalleDTO> findAll(
		) {
					LogUtil.log("finding all PrioridadSiniestroReaseguroDetalleDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from PrioridadSiniestroReaseguroDetalleDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogUtil.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}