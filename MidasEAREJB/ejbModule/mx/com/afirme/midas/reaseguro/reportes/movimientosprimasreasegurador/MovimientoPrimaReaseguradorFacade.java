package mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreasegurador;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless
public class MovimientoPrimaReaseguradorFacade implements MovimientoPrimaReaseguradorFacadeRemote{

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<MovimientoPrimaReaseguradorDTO> obtenerMovimientosPrimaPorReasegurador(MovimientoPrimaReaseguradorDTO movimientoPrimaReaseguradorDTO,String nombreUsuario) throws Exception{
		StoredProcedureHelper storedHelper = null;
		final String nombreSP = "MIDAS.PKGREA_REPORTES.spREA_RptMovtosPorReasegurador";
		String descripcionParametros = "";
		try {
			final String [] nombreParametrosSP = {"pFechaInicial","pFechaFinal","pIdToPoliza","pIdMoneda","pIdRamo","pIdSubRamo"};
			final Object [] parametrosSP = {
					((movimientoPrimaReaseguradorDTO.getFechaInicioVigencia()==null)?"":movimientoPrimaReaseguradorDTO.getFechaInicioVigencia()),
					((movimientoPrimaReaseguradorDTO.getFechaFinVigencia()==null)?"":movimientoPrimaReaseguradorDTO.getFechaFinVigencia()),
					((movimientoPrimaReaseguradorDTO.getIdToPoliza()==null)?"":movimientoPrimaReaseguradorDTO.getIdToPoliza()),
					((movimientoPrimaReaseguradorDTO.getIdMoneda()==null)?"":movimientoPrimaReaseguradorDTO.getIdMoneda()),
					((movimientoPrimaReaseguradorDTO.getIdRamo()==null)?"":movimientoPrimaReaseguradorDTO.getIdRamo()),
					((movimientoPrimaReaseguradorDTO.getIdSubRamo()==null)?"":movimientoPrimaReaseguradorDTO.getIdSubRamo())
			};
			descripcionParametros=obtenerDescripcionParametros(nombreParametrosSP, parametrosSP);
			LogDeMidasEJB3.log("Entrando a MovimientoPrimaReaseguradorFacade.obtenerMovimientosPrimaPorReasegurador. Ejecutando SP: " + nombreSP+", parametros "+descripcionParametros, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(nombreSP);

			storedHelper.estableceMapeoResultados(
							MovimientoPrimaReaseguradorDTO.class.getCanonicalName(),
							"codigoSubRamo," + "folioPoliza," +"numeroEndoso," + "fechaInicioVigencia," +
							"fechaFinVigencia," + "numeroInciso," +"numeroSubInciso," + "porcentajeRetencion," +
							"porcentajeCuotaParte," + "porcentajePrimerExcedente," +"porcentajeFacultativo," + "montoPrimaNeta," +
							"folioContrato," + "tipoReaseguro," +"nombreACuentaDe," + "porcentajeParticipacion," +
							"porcentajeComision," + "nombreCorredor," +"porcentajeParticipacionCorredor," + "porcentajeComisionCorredor," +
							"descripcionAmbito," + "montoPrimaCedida," +"montoComisiones," + "descripcionMoneda," +
							"fechaMovimiento," + "descripcionSucursal," +"idAsegurado," + "nombreAsegurado," +
							"descripcionMonedaReporte," + "folioPolizaReporte," +"montoSumaAsegurada," + "montoSumaAseguradaReaseguro," +
							"montoSumaAseguradaDistribuida," + "porcentajePleno," +"descripcionOficina," + "idTipoReaseguro"+
							",identificador",
							
							"fSubRamo,"+ "fPoliza,"+ "fEndoso,"+ "fInicioVigencia,"+
							"fFinVigencia,"+"fInciso,"+"fSubInciso,"+ "fPorRetencion,"+
							"fPorCuotaParte,"+"fPorPrimerExc,"+"fPorFacultativo,"+"fPrimaNeta,"+
							"fFolioContrato,"+"fTipoReaseguro,"+"fNombreCuentaDe,"+"fPorParticipacion,"+
							"fPorComision,"+"fCorredor,"+"fPorParticipacionCorredor,"+"fPorComisionCorredor,"+
							"fAmbito,"+"fPrimas,"+"fComisiones,"+"fMoneda,"+
							"fFechaMovto,"+"fSucursal,"+"fIdAsegurado,"+"fNombreAsegurado,"+
							"fMonedaRpt,"+"fPolizaRpt,"+"fSumaAsegurada,"+"fSumaAseguradaReas,"+
							"fSumaAseguradaDist,"+"fPorPleno,"+"fOficina,"+"fIdTipoReaseguro"+
							",fIdentificador");
			
			for(int i=0;i<nombreParametrosSP.length; i++){
				storedHelper.estableceParametro(nombreParametrosSP[i], parametrosSP[i]);
			}
			
			List<MovimientoPrimaReaseguradorDTO> movimientosPorReaseguradorList = storedHelper.obtieneListaResultados();
			LogDeMidasEJB3.log("Saliendo de MovimientoPrimaReaseguradorFacade.obtenerMovimientosPrimaPorReasegurador. SP Ejecutado: " + nombreSP+", parametros "+descripcionParametros+ ", Resultado: "+(movimientosPorReaseguradorList != null ? movimientosPorReaseguradorList.size()+" registro(s)" : movimientosPorReaseguradorList), Level.INFO, null);
			return movimientosPorReaseguradorList;
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, movimientoPrimaReaseguradorDTO.getClass(), codErr, descErr);
			LogDeMidasEJB3.log("Excepcion en BD de MovimientoPrimaReaseguradorFacade.obtenerMovimientosPrimaPorReasegurador al invocar: "+nombreSP+", parametros "+descripcionParametros, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en MovimientoPrimaReaseguradorFacade.obtenerMovimientosPrimaPorReasegurador al invocar: "+nombreSP+", parametros "+descripcionParametros, Level.WARNING, e);
			throw e;
		}
	}

	public String obtenerDescripcionParametros(String[] nombreParametrosSP,
			Object[] parametrosSP) {
		StringBuilder descripcionParametros = new StringBuilder("(");
		for(int i=0;i<nombreParametrosSP.length; i++){
			descripcionParametros.append(nombreParametrosSP[i]).append(": ").append(parametrosSP[i]).append(", ");
		}
		descripcionParametros.append(")");
		return descripcionParametros.toString();
	}

}