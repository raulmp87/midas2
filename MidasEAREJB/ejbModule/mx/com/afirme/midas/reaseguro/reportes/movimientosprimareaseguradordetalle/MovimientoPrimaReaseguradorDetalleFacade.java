package mx.com.afirme.midas.reaseguro.reportes.movimientosprimareaseguradordetalle;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreaseguradordetalle.MovimientoPrimaReaseguradorDetalleDTO;
import mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreaseguradordetalle.MovimientoPrimaReaseguradorDetalleFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

@Stateless
public class MovimientoPrimaReaseguradorDetalleFacade implements MovimientoPrimaReaseguradorDetalleFacadeRemote{

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public List<MovimientoPrimaReaseguradorDetalleDTO> obtenerMovimientosPrimaDetalladosPorReasegurador(MovimientoPrimaReaseguradorDetalleDTO movimientoPrimaReaseguradorDetalleDTO,String nombreUsuario) throws Exception {
		StoredProcedureHelper storedHelper = null;
		final String nombreSP = "MIDAS.PKGREA_REPORTES.spREA_RptMovtosPorReasDetalle";
		String descripcionParametros = "";
		try {
			final String nombreParametrosSP[] = {"pFechaInicial","pFechaFinal","pIdToPoliza","pIdMoneda","pIdRamo","pIdSubRamo","pRetencion"};
			final Object[] parametrosSP ={
					(movimientoPrimaReaseguradorDetalleDTO.getFechaInicioVigencia()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getFechaInicioVigencia(),
					(movimientoPrimaReaseguradorDetalleDTO.getFechaFinVigencia()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getFechaFinVigencia(),
					(movimientoPrimaReaseguradorDetalleDTO.getIdToPoliza()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getIdToPoliza(),
					(movimientoPrimaReaseguradorDetalleDTO.getIdMoneda()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getIdMoneda(),
					(movimientoPrimaReaseguradorDetalleDTO.getIdRamo()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getIdRamo(),
					(movimientoPrimaReaseguradorDetalleDTO.getIdSubRamo()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getIdSubRamo(),
					(movimientoPrimaReaseguradorDetalleDTO.getRetencion()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getRetencion()
			};
			
			
			
			descripcionParametros = obtenerDescripcionParametro(nombreParametrosSP, parametrosSP);
			LogDeMidasEJB3.log("Entrando a MovimientoPrimaReaseguradorDetalleFacade. obtenerMovimientosPrimaPorReasegurador.Ejecutando SP: " + nombreSP+", parametros "+descripcionParametros, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(nombreSP);
			storedHelper.estableceMapeoResultados( MovimientoPrimaReaseguradorDetalleDTO.class.getCanonicalName(),
					"codigoSubRamo," + "folioPoliza," +"numeroEndoso," +"tipoEndoso,"+ "fechaInicioVigencia," +
					"fechaFinVigencia," + "numeroInciso," +"numeroSubInciso," + "porcentajeRetencion," +
					"porcentajeCuotaParte," + "porcentajePrimerExcedente," +"porcentajeFacultativo," + "montoPrimaNeta," +
					"folioContrato," + "tipoReaseguro," +"nombreCuentaDe," + "porcentajeParticipacion," +
					"porcentajeComision," + "corredor," +"porcentajeParticipacionCorredor," + "porcentajeComisionCorredor," +
					"ambito," + "montoPrimas," +"montoComisiones," + "descripcionMoneda," +
					"fechaMovto," + "nombreSucursal," +"idAsegurado," + "nombreAsegurado," +
					"monedaReporte," + "folioPolizaReporte," +"montoSumaAsegurada," + "montoSumaAseguradaReaseguro," +
					"montoSumaAseguradaDistribuida," + "fechaEmisionPoliza," +"seccion," + "cobertura," + 
					"registroCNSF,"+"porcentajePleno,"+"nombreOficina,"+"claveGerencia," +
					"gerencia,"+"claveOficina,"+"claveSupervisoria,"+"supervisoria,"+
					"idTipoReaseguro,"+"folioPolizaAnterior,"+"montoPrimaRetenida,"+"montoPrimaCuotaParte,"+
					"montoPrimaPrimerExcedente,"+"montoPrimaFacultativo,"+"montoPrimasRetenidas"+
					",identificador"
					,
					
					"fSubRamo,"+ "fPoliza,"+ "fEndoso," +"fTipoEndoso,"+ "fInicioVigencia,"+
					"fFinVigencia,"+"fInciso,"+"fSubInciso,"+ "fPorRetencion,"+
					"fPorCuotaParte,"+"fPorPrimerExc,"+"fPorFacultativo,"+"fPrimaNeta,"+
					"fFolioContrato,"+"fTipoReaseguro,"+"fNombreCuentaDe,"+"fPorParticipacion,"+
					"fPorComision,"+"fCorredor,"+"fPorParticipacionCorredor,"+"fPorComisionCorredor,"+
					"fAmbito,"+"fPrimas,"+"fComisiones,"+"fMoneda,"+
					"fFechaMovto,"+"fSucursal,"+"fIdAsegurado,"+"fNombreAsegurado,"+
					"fMonedaRpt,"+"fPolizaRpt,"+"fSumaAsegurada,"+"fSumaAseguradaReas,"+
					"fSumaAseguradaDist,"+"fEmisionPol,"+"fSeccion,"+"fCobertura,"+
					"fCNSF,"+"fPorPleno,"+"fOficina,"+"fCveGerencia,"+
					"fGerencia,"+"fCveOficina,"+"fCveSupervisoria,"+"fSupervisoria,"+
					"fIdTipoReaseguro,"+"fPolizaOld,"+"fPrimaRetenida,"+"fPrimaCuotaP,"+
					"fPrimaPrimerE,"+"fPrimaFacul,"+"fPrimasRet"+
					",fIdentificador");
			
			for(int i=0;i<nombreParametrosSP.length; i++){
				storedHelper.estableceParametro(nombreParametrosSP[i], parametrosSP[i]);
			}
			
			List<MovimientoPrimaReaseguradorDetalleDTO> movimientosPorReaseguradorList = storedHelper.obtieneListaResultados();
			LogDeMidasEJB3.log("Saliendo de MovimientoPrimaReaseguradorDetalleFacade.obtenerMovimientosPrimaDetalladosPorReasegurador. SP Ejecutado: " + nombreSP+", parametros "+descripcionParametros+ ", Resultado: "+(movimientosPorReaseguradorList != null ? movimientosPorReaseguradorList.size()+" registro(s)" : movimientosPorReaseguradorList), Level.INFO, null);
			return movimientosPorReaseguradorList;
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					nombreSP, movimientoPrimaReaseguradorDetalleDTO.getClass(), codErr, descErr);
			LogDeMidasEJB3.log("Excepcion en BD de MovimientoPrimaReaseguradorDetalleFacade.obtenerMovimientosPrimaDetalladosPorReasegurador al invocar: "+nombreSP+", parametros "+descripcionParametros, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en MovimientoPrimaReaseguradorDetalleFacade.obtenerMovimientosPrimaDetalladosPorReasegurador al invocar: "+nombreSP+", parametros "+descripcionParametros, Level.WARNING, e);
			throw e;
		}
	}

	public String obtenerDescripcionParametro(String[] nombreParametrosSP, Object[] parametrosSP) {
		StringBuilder descripcionParametros = new StringBuilder("(");
		for(int i=0;i<nombreParametrosSP.length; i++){
			descripcionParametros.append(nombreParametrosSP[i]).append(": ").append(parametrosSP[i]).append(", ");
		}
		descripcionParametros.append(")");
		return descripcionParametros.toString();
	}
}