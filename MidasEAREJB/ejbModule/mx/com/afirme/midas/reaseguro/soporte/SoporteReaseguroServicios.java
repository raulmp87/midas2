package mx.com.afirme.midas.reaseguro.soporte;

import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.transaction.SystemException;

import mx.com.afirme.midas.danios.soporte.SoporteEstructuraCotizacion;
import mx.com.afirme.midas.danios.soporte.SoporteValidacionCotizacion;
import mx.com.afirme.midas.reaseguro.soporte.validacion.ResultadoValidacionReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.validacion.SoporteReaseguroCotizacion;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * 
 * @author jose luis arellano
 */
@Stateless
public class SoporteReaseguroServicios implements SoporteReaseguroServiciosRemote{
	

	/**
	 * Valida una cotizaci�n con reaseguro, para determinar los c�mulos correspondientes.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ResultadoValidacionReaseguroDTO validarReaseguroFacultativo(BigDecimal idToCotizacion){
		
		SoporteEstructuraCotizacion estructuraCotizacion = null;
		try {
			estructuraCotizacion = new SoporteEstructuraCotizacion(idToCotizacion);
		} catch (SystemException e) {
			LogDeMidasEJB3.log("Ocurri� un error al instanciar SoporteEstructuraCotizacion. idToCotizacion="+idToCotizacion, Level.SEVERE, e);
			throw new RuntimeException(e);
		}

		try {
			estructuraCotizacion.consultarAgrupacionCotizacion();
			estructuraCotizacion.consultarCoberturasContratadasCotizacion();
			estructuraCotizacion.consultarIncisosCotizacion();
			estructuraCotizacion.consultarSubIncisosCotizacion();
			estructuraCotizacion.consultarDetallePrimaCoberturaCotizacion();
		} catch (mx.com.afirme.midas.sistema.SystemException e) {
			LogDeMidasEJB3.log("Error al consultar la informaci�n de la cotizaci�n: "+idToCotizacion, Level.SEVERE, e);
			throw new RuntimeException(e);
		}
		
		//Creacion del soporte de reaseguro
		SoporteReaseguroCotizacion soporteReaseguroCotizacion = null;
		try {
			soporteReaseguroCotizacion = new SoporteReaseguroCotizacion(idToCotizacion);
			soporteReaseguroCotizacion.preparaCargaCumulos();
			
		} catch (mx.com.afirme.midas.sistema.SystemException e) {
			LogDeMidasEJB3.log("Error al instanciar el soporteReaseguro de la cotizaci�n: "+idToCotizacion, Level.SEVERE, e);
			throw new RuntimeException(e);
		}
		
		SoporteValidacionCotizacion soporteValidacionCotizacion = new SoporteValidacionCotizacion(estructuraCotizacion, soporteReaseguroCotizacion);
		
		try {
			soporteValidacionCotizacion.procesarSubRamosCotizacion();
			
			soporteValidacionCotizacion.registrarCumulosReaseguro();
			
		} catch (mx.com.afirme.midas.sistema.SystemException e) {
			e.printStackTrace();
		}
		
		LogDeMidasEJB3.log("Saliendo de SoporteReaseguroServicios.validarReaseguroFacultativo", Level.INFO, null);
		return soporteValidacionCotizacion.obtenerResultadoValidacion();
	}
	
	/**
	 * Notificar la emisi�n de una cotizaci�n a reaseguro, para proceder con la distribuci�n de la misma.
	 * @param idToCotizacion
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @param fechaEmision
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void notificarEmision(BigDecimal idToCotizacion,BigDecimal idToPoliza,Integer numeroEndoso, Date fechaEmision){
		//Creacion del soporte de reaseguro
		SoporteReaseguroCotizacion soporteReaseguroCotizacion = null;
		try {
			soporteReaseguroCotizacion = new SoporteReaseguroCotizacion(idToCotizacion);
			
			soporteReaseguroCotizacion.notificarEmision(idToPoliza, numeroEndoso, fechaEmision);
			
		} catch (mx.com.afirme.midas.sistema.SystemException e) {
			LogDeMidasEJB3.log("Error al instanciar el soporteReaseguro de la cotizaci�n: "+idToCotizacion, Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}
}
