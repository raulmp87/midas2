package mx.com.afirme.midas.reaseguro.soporte.validacion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.tipocambio.TipoCambioFacadeRemote;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.reaseguro.distribucion.DistribucionMovSiniestroDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaFacadeRemote;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroFacadeRemote;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;
import mx.com.afirme.midas.sistema.temporizador.TemporizadorDistribuirPolizaFacadeRemote;

public abstract class SoporteReaseguroBase {
	protected BigDecimal idToCotizacion;
	protected SoporteReaseguroDTO soporteReaseguroDTO;
	protected List<LineaSoporteReaseguroDTO> listaLineasPendientes;
	protected Map<BigDecimal,List<LineaSoporteReaseguroDTO>> mapaLineasPorDistribucion = new HashMap<BigDecimal, List<LineaSoporteReaseguroDTO>>();
	protected Double tipoCambio = null;
	protected boolean soporteReaseguroReciente = false;
	
	protected static final int TIPO_ENDOSO_CANCELACION = 1;
	protected static final int TIPO_ENDOSO_REHABILITACION = 2;
	protected static final int TIPO_ENDOSO_CAMBIO_FORMA_PAGO = 3;
	
	protected LineaSoporteReaseguroFacadeRemote lineaSoporteReaseguroFacade;
	protected SoporteReaseguroFacadeRemote soporteReaseguroFacade;
	protected LineaSoporteCoberturaFacadeRemote lineaSoportecoberturaFacade;
	protected TemporizadorDistribuirPolizaFacadeRemote temporizadorDistribuirPolizaFacade;
	protected TipoCambioFacadeRemote tipoCambioMidasFacade;
	protected mx.com.afirme.midas.interfaz.tipocambio.TipoCambioFacadeRemote tipoCambioInterfaz;
	
	{
		try {
			soporteReaseguroFacade = ServiceLocatorP.getInstance().getEJB(SoporteReaseguroFacadeRemote.class);
			lineaSoporteReaseguroFacade = ServiceLocatorP.getInstance().getEJB(LineaSoporteReaseguroFacadeRemote.class);
			temporizadorDistribuirPolizaFacade = ServiceLocatorP.getInstance().getEJB(TemporizadorDistribuirPolizaFacadeRemote.class);
			tipoCambioMidasFacade = ServiceLocatorP.getInstance().getEJB(TipoCambioFacadeRemote.class);
			lineaSoportecoberturaFacade = ServiceLocatorP.getInstance().getEJB(LineaSoporteCoberturaFacadeRemote.class);
			tipoCambioInterfaz = ServiceLocatorP.getInstance().getEJB(mx.com.afirme.midas.interfaz.tipocambio.TipoCambioFacadeRemote.class);
		} catch (javax.transaction.SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void preparaCargaCumulos() {
		//Se eliminan las lineas cuyo estatus sea "soportado por reaseguro" y "requiere facultativo"
		lineaSoporteReaseguroFacade.eliminarLineaSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro(), LineaSoporteReaseguroDTO.ESTATUS_SOPORTADO_POR_REASEGURO);
		lineaSoporteReaseguroFacade.eliminarLineaSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro(), LineaSoporteReaseguroDTO.ESTATUS_REQUIERE_FACULTATIVO);
		listaLineasPendientes = lineaSoporteReaseguroFacade.listarLineaSoporteReaseguroPorSoporte(soporteReaseguroDTO.getIdToSoporteReaseguro());
	}
	
	public void notificarEmision(BigDecimal idToPoliza,Integer numeroEndoso, Date fechaEmision) {
		if(soporteReaseguroDTO != null){
			LogDeMidasEJB3.log("SoporteReaseguro.notificarEmision: Se inicia la actualizaci�n del Soporte con Respecto a los datos de Emisi�n Poliza: " + idToPoliza + " Endoso: "+ numeroEndoso+" FechaEmision: "+fechaEmision,Level.INFO, null);
			String error;
			try{
				soporteReaseguroDTO = soporteReaseguroFacade.notificarEmision(soporteReaseguroDTO.getIdToSoporteReaseguro(), idToPoliza, numeroEndoso, fechaEmision);
				if(soporteReaseguroDTO != null){
					LogDeMidasEJB3.log("SoporteReaseguro.notificarEmision: Se actualiz� el Soporte con Respecto a los datos de Emisi�n Poliza: " + idToPoliza + " Endoso: "+ numeroEndoso+" FechaEmision: "+fechaEmision,Level.INFO, null);
		        //Se debe validar que los porcentajes de todas las lineas sumen 100, de no ser as�, no se lanza la distribuci�n
					iniciarTimerDistribucionSoporte(soporteReaseguroDTO.getIdToSoporteReaseguro(),idToPoliza, numeroEndoso);
		        //Invocar el m�todo ContratoFacultativoDN.notificacionEndoso para los casos en los que numeroEndoso > 0
		        //ContratoFacultativoDN.getInstancia("TMP").notificacionEndoso(soporteReaseguroDTO, tipoEndoso);
				}//fin validacion soporteReaseguro != null
			}catch(Exception e){
				error = "ERROR. SoporteReaseguro.notificarEmision: ocurri� un error al notificar el Soporte con Respecto a los datos de Emisi�n Poliza: " + idToPoliza +" Endoso: "+ numeroEndoso+" FechaEmision: "+fechaEmision+" es posible que no se haya realizado correctamente la notificaci�n."; 
				LogDeMidasEJB3.log(error,Level.SEVERE, null);
			}
		}else{
			LogDeMidasEJB3.log("No existe un soporteReaseguro para la cotizacion: "+idToCotizacion+".",Level.SEVERE,null);
		}
	}
	
	public LineaDTO obtenerLineaPorSubRamo(Date fechaInicioVigencia, BigDecimal idTcSubramo) throws SystemException{
		LineaDTO lineaDTO = soporteReaseguroFacade.obtenerLineaDTOVigente(fechaInicioVigencia,idTcSubramo);
		if (lineaDTO == null){
			throw new SystemException("No se encontr� Linea para los datos recibidos: fechaInicioVigencia = "+fechaInicioVigencia+", idTcSubRamo: "+idTcSubramo);
		}
		return lineaDTO;
	}
	
	protected void iniciarTimerDistribucionSoporte(BigDecimal idToSoporteReaseguro,BigDecimal idToPoliza,int numeroEndoso) throws SystemException{
		List<String> listaErroresValidacion = soporteReaseguroFacade.validarSoporteReaseguroDTOListoParaDistribucion(idToPoliza, numeroEndoso);
		boolean soporteReaseguroValido = listaErroresValidacion.isEmpty();
		if(soporteReaseguroValido){
			//De ser consistente se determina que se puede distribuir esta emisi�n sin problema y lanza un Temporizador para ello.
			LogDeMidasEJB3.log("SoporteReaseguroCotizacionDN.notificarEmision(): Se valid� el soporte reaseguro para la Poliza: " + idToPoliza.intValueExact() + " Endoso: "+ numeroEndoso+
					".El soporte es consistente con los datos de emisi�n, porcentajes de distribuci�n y participaciones en contratos; ser� distribuico posteriormente.",Level.INFO, null);
			LogDeMidasEJB3.log("SoporteReaseguroCotizacionDN.notificarEmision(): Se prepara a lanzar el Temporizador del proceso asincrono de Distribucion de Prima para la Poliza: " + idToPoliza.intValueExact() + " Endoso: "+ numeroEndoso,Level.INFO, null);
//			SoporteReaseguroDN.getInstancia().consultarLineasSoporte(soporteReaseguroDTO);
			temporizadorDistribuirPolizaFacade.iniciarTemporizador(SistemaPersistencia.TIEMPO_INICIAR_DISTRIBUIR_POLIZA, idToSoporteReaseguro);
			LogDeMidasEJB3.log("SoporteReaseguroCotizacionDN.notificarEmision(): Se lanz� el Temporizador del proceso asincrono de Distribucion de Prima para la Poliza: " + idToPoliza.intValueExact() + " Endoso: "+ numeroEndoso,Level.INFO, null);
		}else{
			lineaSoporteReaseguroFacade.registrarEstatusSoporteReaseguroInvalido(idToSoporteReaseguro, listaErroresValidacion);
		}
	}
	
	public List<LineaSoporteReaseguroDTO> getListaLineasSoporte(BigDecimal tipoDistribucion){
		return mapaLineasPorDistribucion.get(tipoDistribucion);
	}
	
//	public boolean autorizadoLiberacion(CotizacionDTO cotizacionDTO,List<String> motivo){
//		boolean autorizacion = true;
//		BigDecimal claveTipoEndoso = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso();
//		if(claveTipoEndoso == null ||
//				( 	claveTipoEndoso.intValue() != ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CANCELACION &&
//					claveTipoEndoso.intValue() != ConstantesCotizacion.SOLICITUD_ENDOSO_DE_REHABILITACION &&
//					claveTipoEndoso.intValue() != ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO)){
//			if(!soporteReaseguroReciente){
//				try {
//					autorizacion = validarLineasReaseguroRegistradas(motivo);
//					if(autorizacion){
//						autorizacion = validarLineasRequierenFacultativo(motivo);
//						if(autorizacion){
//							autorizacion = validarLineasFacultativoSolicitado(motivo);
//							if(autorizacion){
//								autorizacion = validarLineasCancelacionSolicitada(motivo);
//								if(autorizacion){
//									autorizacion = validarLineasFacultativoAutorizado(motivo);
//									if(autorizacion){
//										autorizacion = validarLineasEndosoFacultativo(motivo);
//										//Se agrega validaci�n del soporte de todos los subramos de la cotizaci�n.
//										if(autorizacion){
//											List<SubRamoDTO> subRamosContratados = 
////												CotizacionDN.getInstancia("").
//												listarSubRamosContratadosPorCotizacion(cotizacionDTO);
//											for(SubRamoDTO subRamo : subRamosContratados){
//												LineaDTO linea = null;
//												try{
//												linea = obtenerLineaPorSubRamo(cotizacionDTO.getFechaInicioVigencia(), subRamo.getIdTcSubRamo());
//												} catch (SystemException e) {
//												}
//												if(linea == null){
//													autorizacion = false;
//													String error = "La linea para el subramo '"+subRamo.getDescripcionSubRamo()+"' no ha sido configurada o no se ha autorizado en reaseguro.";
//													LogDeMidasEJB3.log("Se intent� liberar la cotizaci�n "+idToCotizacion+" pero "+error, Level.WARNING, null);
//													if(motivo != null){
//														motivo.add(error);
//													}
//													break;
//												}
//											}
//										}
//									}
//								}
//							}
//						}
//					}
//				} catch (SystemException e) {
//					LogDeMidasWeb.log("Ocurri� un error al consultar las lineas para el SoporteReaseguro: "+soporteReaseguroDTO.getIdToSoporteReaseguro(), Level.SEVERE, e);
//					if(motivo != null)
//						motivo.add("Ocurri� un error al consultar las lineas para el SoporteReaseguro de la cotizacion: "+idToCotizacion);
//					autorizacion = false;
//				}
//			}
//			else{
//				if(motivo != null)
//					motivo.add("No existen lineas de soporte reaseguro para la cotizacion: "+idToCotizacion);
//				autorizacion = false;
//			}
//		}
//		return autorizacion;
//	}
	
	public void notificarLiberacion(BigDecimal claveTipoEndoso) throws SystemException{
		if(claveTipoEndoso == null ||
				( 	claveTipoEndoso.intValue() != ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CANCELACION &&
					claveTipoEndoso.intValue() != ConstantesCotizacion.SOLICITUD_ENDOSO_DE_REHABILITACION &&
					claveTipoEndoso.intValue() != ConstantesCotizacion.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO)){
			try{
				/*
				 * 24/05/2010 JLAB Se actualiz� el flujo, ya que se agreg� el estatus de "facultativo integrado (7). Se deben actualizar las lineas que tengan este estatus a "liberado"
				 * para despues validarlo en "autorizadoEmision"
				 */
				if(soporteReaseguroDTO != null)
					lineaSoporteReaseguroFacade.actualizarEstatusFacultativoLineaSoporte(soporteReaseguroDTO.getIdToSoporteReaseguro(), LineaSoporteReaseguroDTO.ESTATUS_FACULTATIVO_INTEGRADO, LineaSoporteReaseguroDTO.ESTATUS_LIBERADA);
				
				/*
				 * 10/01/2011 JLAB Se agrega registro de bandera aplicaDistribucion
				 */
				soporteReaseguroFacade.actualizarBanderaLineasAplicanDistribucion(soporteReaseguroDTO.getIdToSoporteReaseguro());
			}catch(Exception e){
				LogDeMidasEJB3.log("Ocurri� un error al consultar las lineas del soporte reaseguro: "+soporteReaseguroDTO.getIdToSoporteReaseguro()+", de la cotizacion: "+idToCotizacion, Level.SEVERE, e);
			}
		}
	}
	
//	public void cancelarProcesoFacultativo(BigDecimal claveTipoEndoso) throws ExcepcionDeAccesoADatos, SystemException, ExcepcionDeLogicaNegocio{
//		if(claveTipoEndoso == null ||
//				( 	claveTipoEndoso.intValue() != Sistema.SOLICITUD_ENDOSO_DE_CANCELACION &&
//					claveTipoEndoso.intValue() != Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION &&
//					claveTipoEndoso.intValue() != Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO)){
//			if(soporteReaseguroDTO != null){
//				SlipDTO slipTMP = new SlipDTO();
//				slipTMP.setIdToSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro());
//				slipTMP.setEstatusCotizacion(new BigDecimal(2d));
//				List<SlipDTO> listaSlips = SlipDN.getInstancia().listarFiltrado(slipTMP);
//				//si no exite ning�n SLIP con estatusCotizacion == 2
//				boolean existeSlipIntegrado = false;
//				for (SlipDTO slip : listaSlips){
//					if(slip.getEstatusCotizacion().intValue() == 2){
//						existeSlipIntegrado = true;
//						break;
//					}
//				}
//				if( ! existeSlipIntegrado ){
//					if(!listaSlips.isEmpty()){
//						SoporteReaseguroDN.getInstancia().cancelarCotizacionFacultativo(idToCotizacion);
//					}
//					notificarCotizacionEnProceso();
//				}else{
//					//Se verifica si existe al menos un slipDTO con estatuscotizacion = 1
//					//y que la linea a la que pertenece el slip tenga los montos de sus coberturas iguales
//					//esto es equivalente a tener al menos una linea con estatosFacultativo = 7
//					int cantidadLineasIntegradas = LineaSoporteReaseguroDN.getInstancia().contarLineaSoporteReaseguroPorEstatus(soporteReaseguroDTO.getIdToSoporteReaseguro(), LineaSoporteReaseguroDN.ESTATUS_FACULTATIVO_INTEGRADO, true);
//					if(cantidadLineasIntegradas > 0){
//						throw new ExcepcionDeLogicaNegocio("SoporteReaseguroCotizacionDN","CancelarProcesoFacultativo: No se puede Cancelar Debido a que ya existe un Contrato Cerrado de Facultativo");
//					}
//				}
//			}else{
//				throw new ExcepcionDeLogicaNegocio("SoporteReaseguroCotizacionDN","CancelarProcesoFacultativo: No existe un soporteReaseguro para la cotizacion: "+idToCotizacion+". Es probable que se trate de un endoso de cancelacion/rehabilitacion.");
//			}
//		}
//	}
	
//	protected void notificarCotizacionEnProceso(){
//		List<LineaSoporteReaseguroDTO> lineasAutorizadasEmision = null;
//		try{
//			//Actualizar las lineas con estatus de liberadas a integradas
//			LineaSoporteReaseguroDN.getInstancia().actualizarEstatusFacultativoLineaSoporte(soporteReaseguroDTO.getIdToSoporteReaseguro(), LineaSoporteReaseguroDN.ESTATUS_LIBERADA, LineaSoporteReaseguroDN.ESTATUS_FACULTATIVO_INTEGRADO);
//			//Las lineas con estatus de autorizada para emision pueden provenir de dos estatus, se debe hacer procesamiento extra
//			lineasAutorizadasEmision = LineaSoporteReaseguroDN.getInstancia().listarLineaSoporteReaseguroPorEstatus(soporteReaseguroDTO.getIdToSoporteReaseguro(), LineaSoporteReaseguroDN.ESTATUS_AUTORIZADA_EMISION);
//		}catch(Exception e){
//			LogDeMidasWeb.log("Ocurri� un error al consultar las lineas del soporte reaseguro: "+soporteReaseguroDTO.getIdToSoporteReaseguro()+", de la cotizacion: "+idToCotizacion, Level.SEVERE, e);
//		}
//		if(lineasAutorizadasEmision != null && !lineasAutorizadasEmision.isEmpty()){
//			for(LineaSoporteReaseguroDTO lineaTMP : lineasAutorizadasEmision){
//				try{
//					if(lineaTMP.getPorcentajeFacultativo().doubleValue() > 0d){
//						lineaTMP.setEstatusFacultativo(LineaSoporteReaseguroDN.ESTATUS_COTIZACION_FAC_AUTORIZADA);
//						lineaTMP = LineaSoporteReaseguroDN.getInstancia().actualizar(lineaTMP);
//					}
//					/*
//					 * 11/06/2010. Jos� Luis Arellano. En caso de no tener % facultativo, lal inea fue autorizada por retencion, se deja con el mismo estatus para evitar error en el flujo de reaseguro
//					 */
////					else{
////						lineaTMP.setEstatusFacultativo(LineaSoporteReaseguroDN.ESTATUS_COTIZACION_FAC_AUTORIZADA_POR_RETENCION);
////					}
//				}catch(Exception e){
//					LogDeMidasWeb.log("Ocurri� un error al actualizar el estatus de la linea soporte reaseguro: "+lineaTMP.getId().getIdTmLineaSoporteReaseguro()+", del soporte: "+soporteReaseguroDTO.getIdToSoporteReaseguro()+", de la cotizacion: "+idToCotizacion, Level.SEVERE, e);
//				}
//			}
//		}
//	}
	
//	public boolean autorizadoEmision(BigDecimal claveTipoEndoso,List<String> motivo){
//		boolean autorizacion = true;
//		if(claveTipoEndoso == null ||
//				( 	claveTipoEndoso.intValue() != Sistema.SOLICITUD_ENDOSO_DE_CANCELACION &&
//					claveTipoEndoso.intValue() != Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION &&
//					claveTipoEndoso.intValue() != Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO)){
//			if(!soporteReaseguroReciente){
//				try {
//					autorizacion = validarLineasReaseguroRegistradas(motivo);
//					if(autorizacion){
//						autorizacion = validarLineasLiberadas(motivo);
//					}
//				} catch (SystemException e) {
//					LogDeMidasWeb.log("Ocurri� un error al consultar las lineas para el SoporteReaseguro: "+soporteReaseguroDTO.getIdToSoporteReaseguro(), Level.SEVERE, e);
//					if(motivo != null)
//						motivo.add("Ocurri� un error al consultar las lineas para el SoporteReaseguro de la cotizacion: "+idToCotizacion);
//					autorizacion = false;
//				}
//			}else{
//				if(motivo != null)
//					motivo.add("No existen lineas de soporte reaseguro para la cotizacion: "+idToCotizacion);
//				autorizacion = false;
//			}
//		}
//		return autorizacion;
//	}
	
//	public void validarCancelacionEndoso() throws SystemException{
//		if(soporteReaseguroDTO != null){
//			SoporteReaseguroDN.getInstancia().consultarLineasSoporte(soporteReaseguroDTO);
//			
//			for(LineaSoporteReaseguroDTO lineaSoporte : soporteReaseguroDTO.getLineaSoporteReaseguroDTOs()){
//				if(lineaSoporte.getEstatusFacultativo().intValue() != LineaSoporteReaseguroDTO.SOPORTADO){
//					throw new SystemException("No se puede realizar cancelaci�n de endoso, debido a que la p�liza est� relacionada con un contrato facultativo.");
//				}
//			}
//		}
//		else{
//			throw new SystemException("No existe un soporte de reaseguro para la cancelacion del endoso.");
//		}
//	}
	
	protected DistribucionMovSiniestroDTO obtenerDistribucionMovimiento(BigDecimal idToSeccion,BigDecimal idToCobertura,Integer numeroInciso,Integer numeroSubInciso,List<DistribucionMovSiniestroDTO> listaMovimientosSiniestro){
		DistribucionMovSiniestroDTO movtoEncontrado = null;
		numeroInciso = (numeroInciso != null? numeroInciso : 0);
		numeroSubInciso = (numeroSubInciso != null? numeroSubInciso : 0);
		for(DistribucionMovSiniestroDTO movimiento : listaMovimientosSiniestro){
			if(idToSeccion != null && movimiento.getIdToSeccion().compareTo(idToSeccion) == 0 && 
					idToCobertura != null && movimiento.getIdToCobertura().compareTo(idToCobertura) == 0){
				if(numeroInciso.intValue() == movimiento.getNumeroInciso().intValue() && numeroSubInciso.intValue() == movimiento.getNumeroSubInciso().intValue()){
					movtoEncontrado = movimiento;
					break;
				}
			}
		}
		return movtoEncontrado;
	}
	
//	private boolean validarLineasEndosoFacultativo(List<String> motivo) throws SystemException{
//		ResultadoConteo resultado = validarCantidadLineas(LineaSoporteReaseguroDN.ESTATUS_LINEA_ENDOSO_FACULTATIVO,true);
//		if(! resultado.validacion){
//			if(motivo != null)
//				motivo.add("Existe(n) "+resultado.cantidad +" linea(s) de reaseguro con endoso de contrato facultativo pendiente de la cotizaci�n"+idToCotizacion);
//		}
//		return resultado.validacion;
//	}
	
//	private boolean validarLineasCancelacionSolicitada(List<String> motivo) throws SystemException{
//		ResultadoConteo resultado = validarCantidadLineas(LineaSoporteReaseguroDN.ESTATUS_CANCELACION_FAC_SOLICITADA,true);
//		if(! resultado.validacion){
//			if(motivo != null)
//				motivo.add("Existe(n) "+resultado.cantidad +" linea(s) de reaseguro que fueron solicitadas para cancelar facultativo de la cotizacion "+idToCotizacion);
//		}
//		return resultado.validacion;
//	}
	
//	private boolean validarLineasFacultativoAutorizado(List<String> motivo) throws SystemException{
//		ResultadoConteo resultado = validarCantidadLineas(LineaSoporteReaseguroDN.ESTATUS_COTIZACION_FAC_AUTORIZADA,true);
//		if(! resultado.validacion){
//			if(motivo != null)
//				motivo.add("Existe(n) "+resultado.cantidad +" linea(s) autorizadas por reaseguro que no han sido integradas a la cotizaci�n "+idToCotizacion);
//		}
//		return resultado.validacion;
//	}
	
//	private boolean validarLineasFacultativoSolicitado(List<String> motivo) throws SystemException{
//		ResultadoConteo resultado = validarCantidadLineas(LineaSoporteReaseguroDN.ESTATUS_FACULTTIVO_SOLICITADO,true);
//		if(! resultado.validacion){
//			if(motivo != null)
//				motivo.add("Existe(n) "+resultado.cantidad +" linea(s) de reaseguro que fueron solicitadas para facultativo de la cotizacion "+idToCotizacion);
//		}
//		return resultado.validacion;
//	}
	
//	private boolean validarLineasRequierenFacultativo(List<String> motivo) throws SystemException{
//		ResultadoConteo resultado = validarCantidadLineas(LineaSoporteReaseguroDN.ESTATUS_REQUIERE_FACULTATIVO,true);
//		if(! resultado.validacion){
//			if(motivo != null)
//				motivo.add("Existe(n) "+resultado.cantidad+" linea(s) de reaseguro que requieren facultativo para la cotizacion "+idToCotizacion);
//		}
//		return resultado.validacion;
//	}
	
//	private boolean validarLineasLiberadas(List<String> motivo) throws SystemException{
//		ResultadoConteo resultado = validarCantidadLineas(LineaSoporteReaseguroDN.ESTATUS_LIBERADA,true);
//		if(! resultado.validacion){
//			if(motivo != null)
//				motivo.add("Existe(n) "+resultado.cantidad+" linea(s) de reaseguro que no han sido autorizadas para emisi�n, para la cotizaci�n "+idToCotizacion);
//		}
//		return resultado.validacion;
//	}
	
//	private boolean validarLineasReaseguroRegistradas(List<String> motivo) throws SystemException{
//		ResultadoConteo resultado = validarCantidadLineas(null,true);
//		if(resultado.validacion){//en este caso es correcto que encuentre al menos una l�nea
//			if(motivo != null)
//				motivo.add("No existen lineas de soporte reaseguro para la cotizacion: "+idToCotizacion);
//		}
//		return !resultado.validacion;
//	}
	
//	private ResultadoConteo validarCantidadLineas(Integer estatus,boolean igual) throws SystemException{
//		int cantidadLineasSoporte = LineaSoporteReaseguroDN.getInstancia().contarLineaSoporteReaseguroPorEstatus(soporteReaseguroDTO.getIdToSoporteReaseguro(), estatus,igual);
//		return new ResultadoConteo(cantidadLineasSoporte == 0, cantidadLineasSoporte);
//	}
	
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}
	
//	private final class ResultadoConteo{
//		public boolean validacion;
//		public int cantidad;
//		public ResultadoConteo(boolean validacion,int cantidad){
//			this.validacion=validacion;
//			this.cantidad=cantidad;
//		}
//	}
}
