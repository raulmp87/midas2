package mx.com.afirme.midas.reaseguro.soporte.validacion.utilerias;

import java.util.List;

import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.reaseguro.ConstantesReaseguro;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;

public class UtileriasValidacionFacultativo {
	
	public static LineaSoporteReaseguroDTO obtenerLineaSoporteReaseguroPorCumulo(LineaDTO lineaDTO,Integer numeroInciso,Integer numeroSubInciso,List<LineaSoporteReaseguroDTO> listaLineasPendientes) {
		LineaSoporteReaseguroDTO lineaSoporteTMP = null;
		if(listaLineasPendientes != null && !listaLineasPendientes.isEmpty()){
			int tipoDistribucion = lineaDTO.getTipoDistribucion().intValue();
			for (LineaSoporteReaseguroDTO linea : listaLineasPendientes) {
				if ( linea.getLineaDTO().getSubRamo().getIdTcSubRamo().compareTo(lineaDTO.getSubRamo().getIdTcSubRamo()) == 0) {
					if (tipoDistribucion == ConstantesReaseguro.TIPO_CUMULO_POLIZA){
						lineaSoporteTMP = linea;
						break;
					}else{
						int numeroIncisoLinea = obtenerNumeroInciso(linea);
						if( numeroIncisoLinea == numeroInciso.intValue() ){
							if(tipoDistribucion == ConstantesReaseguro.TIPO_CUMULO_INCISO){
								lineaSoporteTMP = linea;
								break;
							} else{
								if (tipoDistribucion == 3 && obtenerNumeroSubInciso(linea) == numeroSubInciso.intValue()){
									lineaSoporteTMP = linea;
									break;
								}
							}
						}
					}
				}
			}
		}
		return lineaSoporteTMP;
	}

	public static int obtenerNumeroInciso(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) {
		int numeroInciso = 0;
		if(!lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty() ){
			numeroInciso = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getNumeroInciso()!=null?lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getNumeroInciso().intValue():0;
			for(int i=1; i<lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().size(); i++){
				LineaSoporteCoberturaDTO lineaCobertura = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(i);
				if(lineaCobertura.getNumeroInciso() != null && lineaCobertura.getNumeroInciso().intValue() != numeroInciso){
					numeroInciso = 0;
					break;
				}
			}
		}
		return numeroInciso;
	}
	
	public static int obtenerNumeroSubInciso(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO){
//		if(lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() == null || lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty()){
//			lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(LineaSoporteCoberturaDN.getInstancia().listarLineaSoporteCoberturaPorLineaSoporteId(lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro()));
//		}
		int numeroSubInciso = 0;
		if(!lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty() ){
			numeroSubInciso = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getNumeroSubInciso()!=null?lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getNumeroSubInciso().intValue():0;
			for(int i=1; i<lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().size(); i++){
				LineaSoporteCoberturaDTO lineaCobertura = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(i);
				if(lineaCobertura.getNumeroSubInciso() != null && lineaCobertura.getNumeroSubInciso().intValue() != numeroSubInciso){
					numeroSubInciso = 0;
					break;
				}
			}
		}
		return numeroSubInciso;
	}
}
