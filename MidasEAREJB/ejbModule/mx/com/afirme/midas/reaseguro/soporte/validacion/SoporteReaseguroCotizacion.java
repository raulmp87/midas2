package mx.com.afirme.midas.reaseguro.soporte.validacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDTOId;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.validacion.utilerias.UtileriasValidacionFacultativo;
import mx.com.afirme.midas.sistema.SystemException;

public class SoporteReaseguroCotizacion extends SoporteReaseguroBase{
	
	public SoporteReaseguroCotizacion(BigDecimal idToCotizacion) throws SystemException{
		this.idToCotizacion = idToCotizacion;
		List<SoporteReaseguroDTO> listaSoporteReaseguroPorCotizacion = soporteReaseguroFacade.listarPorIdToCotizacion(idToCotizacion);
		if ( listaSoporteReaseguroPorCotizacion != null && !listaSoporteReaseguroPorCotizacion.isEmpty() ){
			soporteReaseguroDTO = listaSoporteReaseguroPorCotizacion.get(0);
		}
		else{
			soporteReaseguroDTO = new SoporteReaseguroDTO();
		    soporteReaseguroDTO.setIdToCotizacion(idToCotizacion);
		    soporteReaseguroDTO.setFechaCreacion(new Date());
 		    soporteReaseguroDTO.setFechaModificacion(new Date());
 		    soporteReaseguroDTO.setNumeroEndoso(new Integer(0));
		    soporteReaseguroDTO = soporteReaseguroFacade.save(soporteReaseguroDTO);
		    soporteReaseguroReciente = true;
		}
	}
	
	public SoporteReaseguroCotizacion(BigDecimal idToCotizacion,boolean generarNuevoSoporte) throws SystemException{
		this.idToCotizacion = idToCotizacion;
		List<SoporteReaseguroDTO> listaSoporteReaseguroPorCotizacion = soporteReaseguroFacade.listarPorIdToCotizacion(idToCotizacion);
		if ( listaSoporteReaseguroPorCotizacion != null && !listaSoporteReaseguroPorCotizacion.isEmpty() ){
			soporteReaseguroDTO = listaSoporteReaseguroPorCotizacion.get(0);
		} else if (generarNuevoSoporte){
			soporteReaseguroDTO = new SoporteReaseguroDTO();
		    soporteReaseguroDTO.setIdToCotizacion(idToCotizacion);
		    soporteReaseguroDTO.setFechaCreacion(new Date());
 		    soporteReaseguroDTO.setFechaModificacion(new Date());
 		    soporteReaseguroDTO.setNumeroEndoso(new Integer(0));
		    soporteReaseguroDTO = soporteReaseguroFacade.save(soporteReaseguroDTO);
		    soporteReaseguroReciente = true;
		}
//		else{
//			throw new SystemException("No existe soporte para la cotizacion "+idToCotizacion, 20);
//		}
	}
	
//	public boolean notificarEndosoCancelacion(BigDecimal idToPoliza,int numeroEndoso,BigDecimal idToCotizacionEndosoCancelacion,Date fechaEmisionCancelacion,
//			Date fechaInicioVigenciaEndoso,double factorPrima,List<String> listaErrores, Usuario usuario,Boolean ignorarSoporteExistente,Boolean ignorarSoporteFacultativo) throws SystemException{
//		boolean ignorarSoporteExistenteLocal = (ignorarSoporteExistente != null ? ignorarSoporteExistente : false);
//		boolean ignorarSoporteFacultativoLocal = (ignorarSoporteFacultativo != null ? ignorarSoporteFacultativo: false);
//		BigDecimal factorAplicacion = null;
//		try{
//			factorAplicacion = SoporteReaseguroDN.getInstancia().calcularFactorAplicacionEndosoCancelacionRehabilitacion(idToPoliza, numeroEndoso, EndosoDTO.TIPO_ENDOSO_CANCELACION);
//		}
//		catch(Exception e){
//			LogDeMidasWeb.log("Ocurri� un error al calcular el factor de aplicaci�n de la p�liza: "+idToPoliza+", endoso:"+numeroEndoso+". Se utilizar� el factor recibido: "+factorPrima, Level.SEVERE, e);
//			factorAplicacion = new BigDecimal(factorPrima);
//		}
//		boolean resultado = new SoporteReaseguroSN(false,false,false).notificarEmisionEndoso(this.soporteReaseguroDTO, idToPoliza, 
//				numeroEndoso, idToCotizacionEndosoCancelacion, fechaEmisionCancelacion,fechaInicioVigenciaEndoso,factorAplicacion,EndosoDTO.TIPO_ENDOSO_CANCELACION, listaErrores,ignorarSoporteExistenteLocal);
//		procesarReasultadoNotificacionEndoso(idToPoliza, numeroEndoso, idToCotizacionEndosoCancelacion, fechaEmisionCancelacion, EndosoDTO.TIPO_ENDOSO_CANCELACION,resultado, listaErrores,usuario,ignorarSoporteFacultativoLocal);
//		return resultado;
//	}
	
//	public boolean notificarEndosoRehabilitacion(BigDecimal idToPoliza,int numeroEndoso,BigDecimal idToCotizacionEndosoRehabilitacion,Date fechaEmisionRehabilitacion,
//			Date fechaInicioVigenciaEndoso,double factorPrima,List<String> listaErrores,Usuario usuario,Boolean ignorarSoporteExistente,Boolean ignorarSoporteFacultativo) throws SystemException{
//		boolean ignorarSoporteExistenteLocal = (ignorarSoporteExistente != null ? ignorarSoporteExistente : false);
//		boolean ignorarSoporteFacultativoLocal = (ignorarSoporteFacultativo != null ? ignorarSoporteFacultativo: false);
//		BigDecimal factorAplicacion = null;
//		try{
//			factorAplicacion = SoporteReaseguroDN.getInstancia().calcularFactorAplicacionEndosoCancelacionRehabilitacion(idToPoliza, numeroEndoso, EndosoDTO.TIPO_ENDOSO_REHABILITACION);
//		}
//		catch(Exception e){
//			LogDeMidasWeb.log("Ocurri� un error al calcular el factor de aplicaci�n de la p�liza: "+idToPoliza+", endoso:"+numeroEndoso+". Se utilizar� el factor recibido: "+factorPrima, Level.SEVERE, e);
//			factorAplicacion = new BigDecimal(factorPrima);
//		}
//		boolean resultado = new SoporteReaseguroSN(false,false,false).notificarEmisionEndoso(soporteReaseguroDTO, idToPoliza, 
//				numeroEndoso, idToCotizacionEndosoRehabilitacion, fechaEmisionRehabilitacion,fechaInicioVigenciaEndoso,factorAplicacion,EndosoDTO.TIPO_ENDOSO_REHABILITACION, listaErrores,ignorarSoporteExistenteLocal);
//		procesarReasultadoNotificacionEndoso(idToPoliza, numeroEndoso, idToCotizacionEndosoRehabilitacion, fechaEmisionRehabilitacion, EndosoDTO.TIPO_ENDOSO_REHABILITACION,resultado, listaErrores,usuario,ignorarSoporteFacultativoLocal);
//		return resultado;
//	}
	
//	public boolean notificarEndosoCambioFormaPago(BigDecimal idToPoliza,int numeroEndoso,BigDecimal idToCotizacionEndosoRehabilitacion,Date fechaEmisionRehabilitacion,
//			Date fechaInicioVigenciaEndoso,double factorPrima,List<String> listaErrores,Usuario usuario,Boolean ignorarSoporteExistente,Boolean ignorarSoporteFacultativo) throws SystemException{
//		boolean ignorarSoporteExistenteLocal = (ignorarSoporteExistente != null ? ignorarSoporteExistente : false);
//		boolean ignorarSoporteFacultativoLocal = (ignorarSoporteFacultativo != null ? ignorarSoporteFacultativo: false);
//		BigDecimal factorAplicacion = new BigDecimal(factorPrima);
//		boolean resultado = new SoporteReaseguroSN(false,false,false).notificarEmisionEndoso(soporteReaseguroDTO, idToPoliza, 
//				numeroEndoso, idToCotizacionEndosoRehabilitacion, fechaEmisionRehabilitacion,fechaInicioVigenciaEndoso,factorAplicacion,EndosoDTO.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO, listaErrores,ignorarSoporteExistenteLocal);
//		procesarReasultadoNotificacionEndoso(idToPoliza, numeroEndoso, idToCotizacionEndosoRehabilitacion, fechaEmisionRehabilitacion, EndosoDTO.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO,resultado, listaErrores, usuario,ignorarSoporteFacultativoLocal);
//		return resultado;
//	}
	
//	private void procesarReasultadoNotificacionEndoso(BigDecimal idToPoliza,int numeroEndoso,
//			BigDecimal idToCotizacionEndoso,Date fechaEmisionEndoso,short tipoEndoso,boolean resultado,List<String> listaErrores, Usuario usuario,boolean ignorarSoporteFacultativo) throws SystemException{
//		String operacion = (tipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION ? "Cancelacion" : tipoEndoso == EndosoDTO.TIPO_ENDOSO_REHABILITACION ? "Rehabilitacion" : tipoEndoso == EndosoDTO.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO ? "Cambio de forma de pago" : null);
//		//no se debe validar por la cantidad de slips, sino por el porcentaje facultativo en las lineas
//		if(resultado){
//			int lineasConFacultativo = LineaSoporteReaseguroDN.getInstancia().contarLineaSoporteReaseguroPorPorcentajeFacultativo(soporteReaseguroDTO.getIdToSoporteReaseguro(), 0d, false);
//			/*
//			 * 23/06/2011 Se elimina la validaci�n debido a que una vez emitido el endoso, se debe proceder con la distribuci�n para evitar descuadres da�os-reaseguro
//			 */
//			SoporteReaseguroDTO soporteReaseguroNuevoEndoso = SoporteReaseguroDN.getInstancia().obtenerSoporteReaseguroPorPolizaEndoso(idToPoliza, numeroEndoso);
//			
//			if(soporteReaseguroNuevoEndoso != null){
//				iniciarTimerDistribucionSoporte(soporteReaseguroNuevoEndoso.getIdToSoporteReaseguro(),idToPoliza, numeroEndoso);
//			}
//			
//			if(lineasConFacultativo > 0 ){
//				
//				LogDeMidasWeb.log("Se realiz� la "+operacion+" de una cotizaci�n con facultativo: idToCotizacion = "+soporteReaseguroDTO.getIdToCotizacion()+
//						"se encontraron "+lineasConFacultativo+" Lineas con facultativo.", Level.INFO, null);
//				
//				try{
//					SoporteDanosDN.getInstancia().enviaCorreoNotificacionEndosoFacultativo(soporteReaseguroDTO, usuario, tipoEndoso,operacion);
//				}catch(Exception e){
//					String error = "Ocurri� un error al intentar enviar un correo de notificacion de "+operacion+" de una p�liza con contratos facultativos.";
//					LogDeMidasWeb.log(error, Level.SEVERE, e);
//					UtileriasWeb.registraLogInteraccionReaseguro("procesarReasultadoNotificacionEndoso", (short)2, 
//							this.toString(), "procesarReasultadoNotificacionEndoso", usuario.getNombreUsuario(), "idToPoliza: "+idToPoliza+",numeroEndoso: "+numeroEndoso+"idToCotizacionEndoso: "+idToCotizacionEndoso+
//							",fechaEmisionEndoso: "+fechaEmisionEndoso+", tipoEndoso:"+tipoEndoso+"resultado: "+resultado, "void", e, error, error, this.toString(), (short)2);
//				}
//				
//			}
//		} else{
//			LogDeMidasWeb.log("Fall� la creaci�n de un soporteReaseguro de endoso de "+operacion+" para la cotizaci�n: idToCotizacion = "+soporteReaseguroDTO.getIdToCotizacion()+
//					"idToPoliza: "+soporteReaseguroDTO.getIdToPoliza(), Level.INFO, null);
//			String errores = "";
//			if(listaErrores != null)
//				for(String error : listaErrores)
//					errores += " "+error + ".";
//			UtileriasWeb.registraLogInteraccionReaseguro("notificarEndoso"+operacion, (short)2, this.toString(), "notificarEndoso"+operacion, "SISTEMA",
//					"idToPoliza:"+idToPoliza+",numeroEndoso:"+numeroEndoso+",idToCotizacionEndoso"+operacion+":"+idToCotizacionEndoso+",fechaEmision"+operacion+":"+fechaEmisionEndoso+",listaErrores:"+listaErrores,
//					""+resultado, null, "Ocurri� un error al generar un SoporteReaseguro de endoso de "+operacion+"de una poliza de facultativo. IdtoPoliza: "+idToPoliza, "ListaErrores:"+errores,
//					this.toString(), (short)2);
//		}
//	}
	
	public void insertarCumulosLinea(List<CumuloPoliza> listaCumuloPoliza) throws Exception{
		if(listaCumuloPoliza != null && !listaCumuloPoliza.isEmpty()){
			if(tipoCambio == null)
				tipoCambio  =  tipoCambioInterfaz.obtieneTipoCambioPorDia(new Date(), "TMP");
			if(tipoCambio == null)
				throw new SystemException("Imposible obtener el tipo de cambio actual");
			for(CumuloPoliza cumuloPoliza: listaCumuloPoliza){
				//verificar si existe la lineaSoporteReaseguro correspondiente al c�mulo, si existe, se deja intacta.
				LineaSoporteReaseguroDTO nuevaLineaSoporte = UtileriasValidacionFacultativo.obtenerLineaSoporteReaseguroPorCumulo(cumuloPoliza.getLineaDTO(), cumuloPoliza.getNumeroInciso(), cumuloPoliza.getNumeroSubInciso(),this.listaLineasPendientes);
				if(nuevaLineaSoporte == null){
					Short numeroEndoso = soporteReaseguroDTO.getNumeroEndoso() != null? soporteReaseguroDTO.getNumeroEndoso().shortValue() : (short)0;
					nuevaLineaSoporte = lineaSoporteReaseguroFacade.obtenerLineaSoporteReaseguro(cumuloPoliza,null, tipoCambio,soporteReaseguroDTO.getIdToSoporteReaseguro(),numeroEndoso,false,true);
					List<LineaSoporteCoberturaDTO> listaCoberturas = nuevaLineaSoporte.getLineaSoporteCoberturaDTOs();
					nuevaLineaSoporte.setLineaSoporteCoberturaDTOs(null);
					nuevaLineaSoporte = lineaSoporteReaseguroFacade.save(nuevaLineaSoporte);
					List<LineaSoporteCoberturaDTO> listaCoberturasRegistradas = new ArrayList<LineaSoporteCoberturaDTO>();
					if(listaCoberturas != null){
						for(LineaSoporteCoberturaDTO cobertura: listaCoberturas){
							cobertura.setId(new LineaSoporteCoberturaDTOId());
							cobertura.getId().setIdTmLineaSoporteReaseguro(nuevaLineaSoporte.getId().getIdTmLineaSoporteReaseguro());
							cobertura.getId().setIdToSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro());
							listaCoberturasRegistradas.add(lineaSoportecoberturaFacade.save(cobertura));
						}
						nuevaLineaSoporte.setLineaSoporteCoberturaDTOs(listaCoberturasRegistradas);
					}
				}
				if(mapaLineasPorDistribucion.get(cumuloPoliza.getLineaDTO().getTipoDistribucion()) == null){
					mapaLineasPorDistribucion.put(cumuloPoliza.getLineaDTO().getTipoDistribucion(), new ArrayList<LineaSoporteReaseguroDTO>());
				}
				mapaLineasPorDistribucion.get(cumuloPoliza.getLineaDTO().getTipoDistribucion()).add(nuevaLineaSoporte);
			}
		}
	}
	
}