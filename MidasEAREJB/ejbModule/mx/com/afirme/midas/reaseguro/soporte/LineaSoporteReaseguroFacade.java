package mx.com.afirme.midas.reaseguro.soporte;
// default package

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioFacadeRemote;
import mx.com.afirme.midas.reaseguro.soporte.validacion.CumuloPoliza;
import mx.com.afirme.midas.reaseguro.soporte.validacion.DetalleCobertura;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity LineaSoporteReaseguroDTO.
 * 
 * @see .LineaSoporteReaseguroDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class LineaSoporteReaseguroFacade implements LineaSoporteReaseguroFacadeRemote {
	// property constants
	public static final String MONTO_SUMA_ASEGURADA = "montoSumaAsegurada";
	public static final String TIPO_CAMBIO = "tipoCambio";
	public static final String PORCENTAJE_PLENO = "porcentajePleno";
	public static final String PORCENTAJE_RETENCION = "porcentajeRetencion";
	public static final String PORCENTAJE_CUOTA_PARTE = "porcentajeCuotaParte";
	public static final String PORCENTAJE_PRIMER_EXCEDENTE = "porcentajePrimerExcedente";
	public static final String PORCENTAJE_FACULTATIVO = "porcentajeFacultativo";
	public static final String ES_PRIMER_RIESGO = "esPrimerRiesgo";
	public static final String NOTA_DEL_SISTEMA = "notaDelSistema";

	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private TipoCambioFacadeRemote tipoCambioFacade;

	/**
	 * Perform an initial save of a previously unsaved LineaSoporteReaseguroDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            LineaSoporteReaseguroDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public LineaSoporteReaseguroDTO save(LineaSoporteReaseguroDTO entity) {
		LogDeMidasEJB3.log("saving LineaSoporteReaseguroDTO instance", Level.INFO,
				null);
		try {
			entityManager.flush();
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save LineaSoporteReaseguroDTO successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save LineaSoporteReaseguroDTO failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent LineaSoporteReaseguroDTO entity.
	 * 
	 * @param entity
	 *            LineaSoporteReaseguroDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(LineaSoporteReaseguroDTO entity) {
		LogDeMidasEJB3.log("deleting LineaSoporteReaseguroDTO instance", Level.INFO,null);
		try {
			entity = entityManager.getReference(LineaSoporteReaseguroDTO.class,entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved LineaSoporteReaseguroDTO entity and return it
	 * or a copy of it to the sender. A copy of the LineaSoporteReaseguroDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            LineaSoporteReaseguroDTO entity to update
	 * @return LineaSoporteReaseguroDTO the persisted LineaSoporteReaseguroDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public LineaSoporteReaseguroDTO update(LineaSoporteReaseguroDTO entity) {
		LogDeMidasEJB3.log("updating LineaSoporteReaseguroDTO instance", Level.INFO,
				null);
		try {
			LineaSoporteReaseguroDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public LineaSoporteReaseguroDTO findById(LineaSoporteReaseguroId id) {
		LogDeMidasEJB3.log("finding LineaSoporteReaseguroDTO instance with id: " + id,
				Level.INFO, null);
		try {
			LineaSoporteReaseguroDTO instance = entityManager.find(
					LineaSoporteReaseguroDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all LineaSoporteReaseguroDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the LineaSoporteReaseguroDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<LineaSoporteReaseguroDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<LineaSoporteReaseguroDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding LineaSoporteReaseguroDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from LineaSoporteReaseguroDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			List<LineaSoporteReaseguroDTO> listaResultado = query.getResultList();
			
			for (LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO : listaResultado){
				entityManager.refresh(lineaSoporteReaseguroDTO);
			}
			return listaResultado;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<LineaSoporteReaseguroDTO> findByMontoSumaAsegurada(
			Object montoSumaAsegurada) {
		return findByProperty(MONTO_SUMA_ASEGURADA, montoSumaAsegurada);
	}

	public List<LineaSoporteReaseguroDTO> findByTipoCambio(Object tipoCambio) {
		return findByProperty(TIPO_CAMBIO, tipoCambio);
	}

	public List<LineaSoporteReaseguroDTO> findByPorcentajePleno(
			Object porcentajePleno) {
		return findByProperty(PORCENTAJE_PLENO, porcentajePleno);
	}

	public List<LineaSoporteReaseguroDTO> findByPorcentajeRetencion(
			Object porcentajeRetencion) {
		return findByProperty(PORCENTAJE_RETENCION, porcentajeRetencion);
	}

	public List<LineaSoporteReaseguroDTO> findByPorcentajeCuotaParte(
			Object porcentajeCuotaParte) {
		return findByProperty(PORCENTAJE_CUOTA_PARTE, porcentajeCuotaParte);
	}

	public List<LineaSoporteReaseguroDTO> findByPorcentajePrimerExcedente(
			Object porcentajePrimerExcedente) {
		return findByProperty(PORCENTAJE_PRIMER_EXCEDENTE,
				porcentajePrimerExcedente);
	}

	public List<LineaSoporteReaseguroDTO> findByPorcentajeFacultativo(
			Object porcentajeFacultativo) {
		return findByProperty(PORCENTAJE_FACULTATIVO, porcentajeFacultativo);
	}

	public List<LineaSoporteReaseguroDTO> findByEsPrimerRiesgo(
			Object esPrimerRiesgo) {
		return findByProperty(ES_PRIMER_RIESGO, esPrimerRiesgo);
	}

	public List<LineaSoporteReaseguroDTO> findByNotaDelSistema(
			Object notaDelSistema) {
		return findByProperty(NOTA_DEL_SISTEMA, notaDelSistema);
	}

	/**
	 * Find all LineaSoporteReaseguroDTO entities.
	 * 
	 * @return List<LineaSoporteReaseguroDTO> all LineaSoporteReaseguroDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<LineaSoporteReaseguroDTO> findAll() {
		LogDeMidasEJB3.log("finding all LineaSoporteReaseguroDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from LineaSoporteReaseguroDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Update a List<LineaSoporteReaseguroDTO> using a transaction. This means
	 * all or nothing operation.
	 * @param List<LineaSoporteReaseguroDTO> all LineaSoporteReaseguroDTO
	 *         entities
	 */
	public void update(List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTOs) {
		for (LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO : lineaSoporteReaseguroDTOs) {
			this.update(lineaSoporteReaseguroDTO);
		}
	}

	/**
	 * Elimina el registro LineaSoporteReaseguroDTO recibido. El borrado consulta primero los registros LineaSoporteCoberturaDTO
	 * para evitar la violaci�n a la llave for�nea.
	 */
	public void eliminarLineaSoporteReaseguro(LineaSoporteReaseguroDTO entity) {
		if(entity == null){
			LogDeMidasEJB3.log("Se intent� borrar LineaSoporteReaseguroDTO : "+entity, Level.WARNING,null);
			return;
		}
		LogDeMidasEJB3.log("borrando LineaSoporteReaseguroDTO con id: "+(entity.getId()!=null ? entity.getId().getIdTmLineaSoporteReaseguro() : entity.getId()), Level.INFO,null);
		try {
			if(entity.getId() != null && entity.getId().getIdTmLineaSoporteReaseguro() != null){
				String queryString = "DELETE from LineaSoporteCoberturaDTO model where model.id.idTmLineaSoporteReaseguro = :idTmLineaSoporteReaseguro";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idTmLineaSoporteReaseguro", entity.getId().getIdTmLineaSoporteReaseguro());
				query.executeUpdate();
				
				entity = entityManager.getReference(LineaSoporteReaseguroDTO.class,entity.getId());
				entityManager.remove(entity);
				
				LogDeMidasEJB3.log("borrado de LineaSoporteReaseguroDTO exitoso", Level.INFO, null);
			}
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� el borrando LineaSoporteReaseguroDTO con id: "+(entity.getId()!=null ? entity.getId().getIdTmLineaSoporteReaseguro() : entity.getId()), Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Elimina los registros LineaSoporteReaseguroDTO del SoporteReaseguro cuyo id se recibe, y que tengan el estatus recibido. 
	 * El borrado consulta primero los registros LineaSoporteCoberturaDTO para evitar la violaci�n a la llave for�nea.
	 */
	public void eliminarLineaSoporteReaseguro(BigDecimal idToSoporteReaseguro, Integer estatus) {
		LogDeMidasEJB3.log("Borrando LineaSoporteReaseguroDTO del soporteReaseguro: "+idToSoporteReaseguro+", con estatus: "+estatus, Level.WARNING,null);
		try {
			if(idToSoporteReaseguro != null ){
				String queryString = "DELETE from MIDAS.tdlineasopreacobertura where idtmlineasoportereaseguro in (select t0.idtmlineasoportereaseguro " +
						"from MIDAS.TMLINEASOPORTEREASEGURO t0 where t0.IDTOSOPORTEREASEGURO = "+idToSoporteReaseguro+" and t0.ESTATUSFACULTATIVO = "+estatus+")";
					
//					"DELETE from LineaSoporteCoberturaDTO model where model.id.idTmLineaSoporteReaseguro in " +
//						"(select linea.id.idTmLineaSoporteReaseguro from LineaSoporteReaseguroDTO linea where linea.id.idToSoporteReaseguro = :idToSoporteReaseguro " +
//						"and linea.estatusFacultativo = :estatus) ";
				Query query = entityManager.createNativeQuery(queryString);
//				query.setParameter("idToSoporteReaseguro", idToSoporteReaseguro);
//				query.setParameter("estatus", estatus);
				query.executeUpdate();
				
				queryString = "delete from MIDAS.TMLINEASOPORTEREASEGURO where IDTOSOPORTEREASEGURO = "+idToSoporteReaseguro+" and ESTATUSFACULTATIVO = "+estatus;
					
//					"DELETE from LineaSoporteReaseguroDTO linea where linea.id.idToSoporteReaseguro = :idToSoporteReaseguro " +
//						"and linea.estatusFacultativo = :estatus ";
				Query query2 = entityManager.createNativeQuery(queryString);
				query2.executeUpdate();
			}
				LogDeMidasEJB3.log("Borrado exitoso de LineaSoporteReaseguroDTO del soporteReaseguro: "+idToSoporteReaseguro+", con estatus: "+estatus, Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Ocurri� un error en borrado de LineaSoporteReaseguroDTO del soporteReaseguro: "+idToSoporteReaseguro+", con estatus: "+estatus, Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<LineaSoporteReaseguroDTO> listarLineaSoporteReaseguroPorEstatus(BigDecimal idToSoporteReaseguro, Integer estatus1,Integer estatus2,boolean comparaEstatusIgual) {
		LogDeMidasEJB3.log("Listando LineaSoporteReaseguroDTO del soporteReaseguro: "+idToSoporteReaseguro+", con estatus"+(comparaEstatusIgual?" = ":" != ")+" "+estatus1+
				(estatus2!=null ? " y estatusFacultativo"+(comparaEstatusIgual?" = ":" <> ")+estatus2:""), Level.INFO,null);
		try {
			if(idToSoporteReaseguro != null ){
				String queryString ;//= "select linea from LineaSoporteReaseguroDTO linea where linea.id.idToSoporteReaseguro = :idToSoporteReaseguro " +
//						"and linea.estatusFacultativo "+(comparaEstatusIgual?" = ":" <> ")+" :estatus1 " +
//						(estatus2!=null ? "and linea.estatusFacultativo "+(comparaEstatusIgual?" = ":" <> ")+" :estatus2 ":"");
				
				queryString = "select linea from LineaSoporteReaseguroDTO linea where linea.id.idToSoporteReaseguro = :idToSoporteReaseguro " +
				"and linea.estatusFacultativo "+(comparaEstatusIgual?"":" not ")+"in (:estatus1"+(estatus2!=null ?",:estatus2":"")+")";
				
				Query query = entityManager.createQuery(queryString);
				query.setParameter("estatus1", estatus1);
				query.setParameter("idToSoporteReaseguro", idToSoporteReaseguro);
				if(estatus2!=null)	query.setParameter("estatus2", estatus2);
				return query.getResultList();
			}
			else
				return null;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Listando LineaSoporteReaseguroDTO del soporteReaseguro: "+idToSoporteReaseguro+", con estatus"+(comparaEstatusIgual?" = ":" != ")+" "+estatus1+
					(estatus2!=null ? " y estatusFacultativo"+(comparaEstatusIgual?" = ":" <> ")+estatus2:""), Level.WARNING,null);
			throw re;
		}
	}
	
	public int contarLineaSoporteReaseguroPorEstatus(BigDecimal idToSoporteReaseguro, Integer estatus1,boolean comparaEstatusIgual) {
		LogDeMidasEJB3.log("Contando LineaSoporteReaseguroDTO del soporteReaseguro: "+idToSoporteReaseguro+", con estatus"+(comparaEstatusIgual?" = ":" != ")+" "+estatus1, Level.INFO,null);
		int cantidadLineas=0;
		try {
			if(idToSoporteReaseguro != null ){
				String queryString = "select count (linea.id.idTmLineaSoporteReaseguro) from LineaSoporteReaseguroDTO linea where " +
						"linea.id.idToSoporteReaseguro = :idToSoporteReaseguro ";
				Object result = null;
				if(estatus1 != null){
					queryString += " and linea.estatusFacultativo "+(comparaEstatusIgual?" = ":" <> ")+" :estatus1 ";
					Query query = entityManager.createQuery(queryString);
					query.setParameter("estatus1", estatus1);
					query.setParameter("idToSoporteReaseguro", idToSoporteReaseguro);
					result = query.getSingleResult();
				}
				else{
					Query query = entityManager.createQuery(queryString);
					query.setParameter("idToSoporteReaseguro", idToSoporteReaseguro);
					result = query.getSingleResult();
				}
				if(result == null)
					cantidadLineas = 0;
				else if(result instanceof Long)
					cantidadLineas = ((Long)result).intValue();
				else if(result instanceof Integer)
					cantidadLineas = ((Integer)result).intValue();
				else if(result instanceof BigDecimal)
					cantidadLineas = ((BigDecimal)result).intValue();
			}
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Ocurri� un error al contar LineaSoporteReaseguroDTO del soporteReaseguro: "+idToSoporteReaseguro+", con estatus"+(comparaEstatusIgual?" = ":" != ")+" "+estatus1, Level.SEVERE,re);
			throw re;
		}
		return cantidadLineas;
	}
	
	public int contarLineaSoporteReaseguroPorPropiedad(BigDecimal idToSoporteReaseguro, String propiedad,Object valor,boolean comparaEstatusIgual) {
		LogDeMidasEJB3.log("Contando LineaSoporteReaseguroDTO del soporteReaseguro: "+idToSoporteReaseguro+", con "+propiedad+(comparaEstatusIgual?" = ":" != ")+" "+valor, Level.INFO,null);
		int cantidadLineas=0;
		try {
			if(idToSoporteReaseguro != null ){
				String queryString = "select count (linea.id.idTmLineaSoporteReaseguro) from LineaSoporteReaseguroDTO linea where " +
						"linea.id.idToSoporteReaseguro = :idToSoporteReaseguro and linea."+propiedad+(comparaEstatusIgual?" = ":" <> ")+" :valor";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("valor", valor);
				query.setParameter("idToSoporteReaseguro", idToSoporteReaseguro);
				Object result = query.getSingleResult();
				if(result == null)
					cantidadLineas = 0;
				else if(result instanceof Long)
					cantidadLineas = ((Long)result).intValue();
				else if(result instanceof Integer)
					cantidadLineas = ((Integer)result).intValue();
				else if(result instanceof BigDecimal)
					cantidadLineas = ((BigDecimal)result).intValue();
			}
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Ocurri� un error al contar LineaSoporteReaseguroDTO del soporteReaseguro: "+idToSoporteReaseguro+", con "+propiedad+(comparaEstatusIgual?" = ":" != ")+" "+valor, Level.SEVERE,re);
			throw re;
		}
		return cantidadLineas;
	}
	
	@SuppressWarnings("unchecked")
	public List<LineaSoporteReaseguroDTO> listarFiltrado(LineaSoporteReaseguroDTO entity) {
		try {
			LogDeMidasEJB3.log("listarFiltrado LineaSoporteReaseguroDTO ", Level.INFO, null);
			String queryString = "select model from LineaSoporteReaseguroDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			if (entity.getId() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idTmLineaSoporteReaseguro", entity.getId().getIdTmLineaSoporteReaseguro());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToSoporteReaseguro", entity.getId().getIdToSoporteReaseguro());
			}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToCotizacion", entity.getIdToCotizacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToPoliza", entity.getIdToPoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "porcentajePleno", entity.getPorcentajePleno());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "porcentajeRetencion", entity.getPorcentajeRetencion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "porcentajeCuotaParte", entity.getPorcentajeCuotaParte());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "porcentajePrimerExcedente", entity.getPorcentajePrimerExcedente());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "porcentajeFacultativo", entity.getPorcentajeFacultativo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "estatusLineaSoporte", entity.getEstatusLineaSoporte());
//			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "esPrimerRiesgo", entity.getEsPrimerRiesgo());se omite por ser tipo de dato primario
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "estatusFacultativo", entity.getEstatusFacultativo());
			if(entity.getLineaDTO() != null){
				if( entity.getLineaDTO().getSubRamo() != null){
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "lineaDTO.subRamo.idTcSubRamo", entity.getLineaDTO().getSubRamo().getIdTcSubRamo());
				}
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "lineaDTO.tipoDistribucion", entity.getLineaDTO().getTipoDistribucion());
			}

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("FALLO: listarFiltrado LineaSoporteReaseguro", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void actualizarEstatusLineaSoporte(BigDecimal idToSoporteReaseguro,Integer estatusOriginal,Integer estatusNuevo,String notaDelSistema){
		try {
			LogDeMidasEJB3.log("Actualizando estatus de LineaSoporteReaseguroDTO del idToSoporteReaseguro = "+idToSoporteReaseguro+" (Estatusoriginal= "+estatusOriginal+",EstatusNuevo="+estatusNuevo+")", Level.INFO, null);
			boolean aplicaNotaDelSistema = false;
			if(notaDelSistema != null && !notaDelSistema.trim().equals("")){
				aplicaNotaDelSistema = true;
				notaDelSistema = notaDelSistema.trim();
				if(notaDelSistema.length() > 499){
					notaDelSistema = notaDelSistema.substring(0, 500);
				}
			}
			String queryString = "UPDATE MIDAS.tmLineaSoporteReaseguro SET estatusLineaSoporte = "+estatusNuevo+
							(aplicaNotaDelSistema ? ", NOTADELSISTEMA = '"+notaDelSistema+"'" : "") +
							" where estatusLineaSoporte= "+estatusOriginal+" and idToSoporteReaseguro = "+idToSoporteReaseguro;
			Query query;
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
			LogDeMidasEJB3.log("Actualizacion exitosa de estatus de LineaSoporteReaseguroDTO del idToSoporteReaseguro = "+idToSoporteReaseguro+" (Estatusoriginal= "+estatusOriginal+",EstatusNuevo="+estatusNuevo+")", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Error al actualizar estatus de LineaSoporteReaseguroDTO del idToSoporteReaseguro = "+idToSoporteReaseguro+" (Estatusoriginal= "+estatusOriginal+",EstatusNuevo="+estatusNuevo+")", Level.INFO, null);
			throw re;
		}
	}
	
	public void eliminarLineaSoportePoridSoporteReaseguro(BigDecimal idToSoporteReaseguro,List<BigDecimal> listaIdTmLineaSoporteReaseguro,boolean verificarIdTmLineaIgual){
		try {
			if(idToSoporteReaseguro != null ){
				String descripcionIds = "";
				boolean existeIdValido = false;
				if (listaIdTmLineaSoporteReaseguro != null && !listaIdTmLineaSoporteReaseguro.isEmpty()){
					for(int i=0;i<listaIdTmLineaSoporteReaseguro.size();i++){
						BigDecimal idTmLineaSoporteReaseguro = listaIdTmLineaSoporteReaseguro.get(i); 
						if(idTmLineaSoporteReaseguro != null){
							existeIdValido=true;
						}
					}
					descripcionIds = obtenerDescripcionIds(listaIdTmLineaSoporteReaseguro);
				}
				else{
					descripcionIds = null;
					existeIdValido = true;
				}
				if(existeIdValido){
					LogDeMidasEJB3.log("Eliminando LineaSoporteReaseguroDTO del idToSoporteReaseguro = "+idToSoporteReaseguro+" " + (descripcionIds != null?
							"con idTmLineaSoporteReaseguro "+(verificarIdTmLineaIgual?" = ":"!=")+descripcionIds
							: ""), Level.INFO, null);
					String queryString = "DELETE from MIDAS.tmLineaSoporteReaseguro where idToSoporteReaseguro = "+idToSoporteReaseguro + (descripcionIds != null?
							" and IDTMLINEASOPORTEREASEGURO "+(verificarIdTmLineaIgual?" in ":"not in ")+"("+descripcionIds+")"
							: "");
					Query query;
					query = entityManager.createNativeQuery(queryString);
					query.executeUpdate();
				}
			}
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public String obtenerDescripcionIds(
			List<BigDecimal> listaIdTmLineaSoporteReaseguro) {
		StringBuilder descripcionIds = new StringBuilder("");
		for(int i=0;i<listaIdTmLineaSoporteReaseguro.size();i++){
			BigDecimal idTmLineaSoporteReaseguro = listaIdTmLineaSoporteReaseguro.get(i); 
			if(idTmLineaSoporteReaseguro != null){
				if(i==0)
					descripcionIds.append(idTmLineaSoporteReaseguro);
				else
					descripcionIds.append(",").append(idTmLineaSoporteReaseguro);
				//existeIdValido=true;
			}
		}
		return descripcionIds.toString();
	}

	public void notificarEmisionLineaSoporteReaseguro(BigDecimal idToSoporteReaseguro,BigDecimal idToPoliza,Short numeroEndoso,Date fechaEmision,int estatusFacultativoLineaEmitida,int estatusLineaSoporte,int estatusLineaDistribuida){
		try {
			LogDeMidasEJB3.log("Inicia notificaci�n de emision para las lineas del soporte reaseguro: "+idToSoporteReaseguro+", id de la poliza: "+idToPoliza+", fechaEmision: "+fechaEmision, Level.INFO, null);
			//Actualizacion del soporte reaseguro
			String queryString = "UPDATE MIDAS.tmLineaSoporteReaseguro SET estatusFacultativo = "+estatusFacultativoLineaEmitida+", IDTOPOLIZA = "+idToPoliza+", NUMEROENDOSO = "+numeroEndoso
					+", ESTATUSLINEASOPORTE = "+estatusLineaSoporte+", NOTADELSISTEMA = 'Emitido y no distribuido', FECHAEMISION= TO_DATE('"+(new java.sql.Date(fechaEmision.getTime()).toString())+"','yyyy-MM-dd')" 
					+", FECHAMODIFICACION = TO_DATE('"+(new java.sql.Date(new Date().getTime()).toString())+"','yyyy-MM-dd')"
					+" where idToSoporteReaseguro = "+idToSoporteReaseguro+" and estatusLineaSoporte <> "+estatusLineaDistribuida;
			Query query;
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Ocurri� un error en la notificaci�n de emision para las lineas del soporte reaseguro: "+idToSoporteReaseguro+", id de la poliza: "+idToPoliza+", fechaEmision: "+fechaEmision, Level.SEVERE, re);
			throw re;
		}
	}
	
	public void actualizarEstatusFacultativoLineaSoporte(BigDecimal idToSoporteReaseguro,Integer estatusOriginal,Integer estatusNuevo){
		try {
			LogDeMidasEJB3.log("Actualizando estatus facultativo de LineaSoporteReaseguroDTO del idToSoporteReaseguro = "+idToSoporteReaseguro+" (Estatusoriginal= "+estatusOriginal+",EstatusNuevo="+estatusNuevo+")", Level.INFO, null);
			String queryString = "UPDATE MIDAS.tmLineaSoporteReaseguro SET estatusFacultativo = "+estatusNuevo+
							" where estatusFacultativo = "+estatusOriginal+" and idToSoporteReaseguro = "+idToSoporteReaseguro;
			Query query;
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
			LogDeMidasEJB3.log("Actualizacion exitosa de estatus facultativo de LineaSoporteReaseguroDTO del idToSoporteReaseguro = "+idToSoporteReaseguro+" (Estatusoriginal= "+estatusOriginal+",EstatusNuevo="+estatusNuevo+")", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Error al Actualizar estatus facultativo de LineaSoporteReaseguroDTO del idToSoporteReaseguro = "+idToSoporteReaseguro+" (Estatusoriginal= "+estatusOriginal+",EstatusNuevo="+estatusNuevo+")", Level.SEVERE, re);
			throw re;
		}
	}
	
	public Object obtenerCampoLineaSoporte(BigDecimal idTmLineaSoporteReaseguro,String campo) {
		Object resultado = null;
		if(idTmLineaSoporteReaseguro != null && campo != null && !campo.trim().equals("")){
			LogDeMidasEJB3.log("Consulta de "+campo+" de la lineaSoporteReaseguro con idTmLineaSoporteReaseguro: "+idTmLineaSoporteReaseguro, Level.INFO,null);
			try {
				final String queryString = "select "+campo+" from MIDAS.TmLineaSoporteReaseguro where idTmLineaSoporteReaseguro = "+idTmLineaSoporteReaseguro;
				Query query = entityManager.createNativeQuery(queryString);
				resultado = query.getSingleResult();
				LogDeMidasEJB3.log("LineaSoporteReaseguro con idTmLineaSoporteReaseguro: "+idTmLineaSoporteReaseguro+", campo: "+campo+", valor: "+resultado, Level.INFO,null);
				return resultado;
			} catch (RuntimeException re) {
				LogDeMidasEJB3.log("count failed", Level.SEVERE, re);
				throw re;
			}
		}else{
			LogDeMidasEJB3.log("FALLA consulta de "+campo+" de la lineaSoporteReaseguro con idTmLineaSoporteReaseguro: "+idTmLineaSoporteReaseguro+" PARAMETROS NO VALIDOS", Level.INFO,null);
		}
		return resultado;
	}
	
	public int obtenerNumeroInciso(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) {		
		if(lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() == null || lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty()){
			lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(lineaSoporteCoberturaFacadeRemote.findByProperty("id.idTmLineaSoporteReaseguro", lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro()));
		}
		int numeroInciso = 0;
		if(!lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty() ){
			numeroInciso = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getNumeroInciso()!=null?lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getNumeroInciso().intValue():0;
			for(int i=1; i<lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().size(); i++){
				LineaSoporteCoberturaDTO lineaCobertura = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(i);
				if(lineaCobertura.getNumeroInciso() != null && lineaCobertura.getNumeroInciso().intValue() != numeroInciso){
					numeroInciso = 0;
					break;
				}
			}
		}
		return numeroInciso;
	}
	
	public int obtenerNumeroSubInciso(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) {
		if(lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() == null || lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty()){
			lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(lineaSoporteCoberturaFacadeRemote.findByProperty("id.idTmLineaSoporteReaseguro", lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro()));
		}
		int numeroSubInciso = 0;
		if(!lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty() ){
			numeroSubInciso = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getNumeroSubInciso()!=null?lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getNumeroSubInciso().intValue():0;
			for(int i=1; i<lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().size(); i++){
				LineaSoporteCoberturaDTO lineaCobertura = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(i);
				if(lineaCobertura.getNumeroSubInciso() != null && lineaCobertura.getNumeroSubInciso().intValue() != numeroSubInciso){
					numeroSubInciso = 0;
					break;
				}
			}
		}
		return numeroSubInciso;
	}
	
	public BigDecimal obtenerIdToSeccion(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) {
		if(lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() == null || lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty()){
			lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(lineaSoporteCoberturaFacadeRemote.findByProperty("id.idTmLineaSoporteReaseguro", lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro()));
		}
		BigDecimal idToSeccion = BigDecimal.ZERO;
		if(!lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty() ){
			idToSeccion = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getIdToSeccion()!=null?lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getIdToSeccion():BigDecimal.ZERO;
			for(int i=1; i<lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().size(); i++){
				LineaSoporteCoberturaDTO lineaCobertura = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(i);
				if(lineaCobertura.getIdToSeccion() != null && lineaCobertura.getIdToSeccion().compareTo(idToSeccion) != 0){
					idToSeccion = BigDecimal.ZERO;
					break;
				}
			}
		}
		return idToSeccion;
	}
	
	private LineaSoporteCoberturaFacadeRemote lineaSoporteCoberturaFacadeRemote;
	
	@EJB
	public void setLineaSoporteCoberturaFacadeRemote(LineaSoporteCoberturaFacadeRemote lineaSoporteCoberturaFacadeRemote) {
		this.lineaSoporteCoberturaFacadeRemote = lineaSoporteCoberturaFacadeRemote;
	}
	
	public void registrarLineasSoporteReaseguro(List<LineaSoporteReaseguroDTO> listaLineasSoporte) {
		LogDeMidasEJB3.log("guardando lista de registros LineaSoporteReaseguroDTO", Level.INFO,null);
		try {
			if(listaLineasSoporte != null){
				for(LineaSoporteReaseguroDTO lineaEnCurso : listaLineasSoporte){
					List<LineaSoporteCoberturaDTO> listaCoberturas = lineaEnCurso.getLineaSoporteCoberturaDTOs();
					lineaEnCurso.setLineaSoporteCoberturaDTOs(null);
					entityManager.persist(lineaEnCurso);
					BigDecimal idTmLinea = lineaEnCurso.getId().getIdTmLineaSoporteReaseguro();
					BigDecimal idToSoporte = lineaEnCurso.getId().getIdToSoporteReaseguro();
					if(listaCoberturas != null){
						for(LineaSoporteCoberturaDTO coberturaEnCurso : listaCoberturas){
							if(coberturaEnCurso.getId() == null){
								coberturaEnCurso.setId(new LineaSoporteCoberturaDTOId(null, idTmLinea, idToSoporte));
								entityManager.persist(coberturaEnCurso);
							}
						}
						lineaEnCurso.setLineaSoporteCoberturaDTOs(listaCoberturas);
					}
				}
			}
			LogDeMidasEJB3.log("finaliza correctamente registro de LineaSoporteReaseguroDTO", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save LineaSoporteReaseguroDTO failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public List<LineaSoporteReaseguroDTO> listarLineaSoporteReaseguroPorSoporte(BigDecimal idToSoporteReaseguro){
		LineaSoporteReaseguroDTO lineaTMP = new LineaSoporteReaseguroDTO();
		lineaTMP.setId(new LineaSoporteReaseguroId());
		lineaTMP.getId().setIdToSoporteReaseguro(idToSoporteReaseguro);
		return listarFiltrado(lineaTMP);
	}
	
	public void registrarEstatusSoporteReaseguroInvalido(BigDecimal idToSoporteReaseguro,List<String> listaErrores){
    	//generar el mensaje de error para registrar en el log
		String error = "Se notific� la emisi�n del soporteReaseguro con idToSoporteReaseguro: "+idToSoporteReaseguro+". Pero se detect� una inconsistencia en la configuraci�n del soporte. DETALLE: " +
			(listaErrores != null && !listaErrores.isEmpty() ? listaErrores.get(0) : "no disponible");
		LogDeMidasEJB3.log(error, Level.SEVERE, null);
		try{
	//setear estatus de las lineas en 4
			actualizarEstatusLineaSoporte(idToSoporteReaseguro, LineaSoporteReaseguroDTO.EMITIDO_SIN_DISTRIBUIR, 
					LineaSoporteReaseguroDTO.ESTATUS_LINEA_EMITIDO_INCONSISTENCIAS,error);
//			String retornoMetodoOrigenError = "soporte inconsistente,"+idToSoporteReaseguro;
//			UtileriasWeb.registraLogInteraccionReaseguro("SoporteReaseguroCotizacionDN.notificarEmision()", (short)2, "SoporteReaseguroBase", "SoporteReaseguroCotizacionDN.notificarEmision()","SISTEMA",
//					"ninguno", retornoMetodoOrigenError, null, error, "Pendiente Revizar SoporteReaseguro. Se cambi� el estatus de las lineas (estatusLineaSoporte) al valor"+LineaSoporteReaseguroDN.ESTATUS_LINEA_EMITIDO_INCONSISTENCIAS, "SoporteReaseguroBase", (short)2);
		}catch(Exception e){
//			LogDeMidasWeb.log("Ocurri� un error al intentar actualizar el estatus de las lineas del soporteReaseguro con idToSoporteReaseguro: "+idToSoporteReaseguro+". nuevo valor: "+
//					LineaSoporteReaseguroDN.ESTATUS_LINEA_EMITIDO_INCONSISTENCIAS, Level.SEVERE, e);
		} 
    }
	
	public LineaSoporteReaseguroDTO obtenerLineaSoporteReaseguro(
			CumuloPoliza cumuloLinea,LineaSoporteReaseguroDTO lineaSoporteReaseguroAnteriorDTO,
			Double tipoCambio,BigDecimal idToSoporteReaseguro,Short numeroEndoso,
			boolean esSoporteEndoso,boolean calcularLineaSoporte) throws Exception{
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = new LineaSoporteReaseguroDTO();
		if(idToSoporteReaseguro != null){
			lineaSoporteReaseguroDTO.setId(new LineaSoporteReaseguroId());
			lineaSoporteReaseguroDTO.getId().setIdToSoporteReaseguro(idToSoporteReaseguro);
		}
		lineaSoporteReaseguroDTO.setLineaDTO(cumuloLinea.getLineaDTO());
		lineaSoporteReaseguroDTO.setEsPrimerRiesgo(cumuloLinea.getAplicaPrimerRiesgo());
		lineaSoporteReaseguroDTO.setIdMoneda(cumuloLinea.getIdMoneda());
		lineaSoporteReaseguroDTO.setTipoCambio(tipoCambio);
		lineaSoporteReaseguroDTO.setFechaInicioVigencia(cumuloLinea.getFechaInicioVigencia());
		try{
			lineaSoporteReaseguroDTO.setNotaDelSistema("Cotizaci�n"+(esSoporteEndoso?" de endoso ":"")+" sobre Subramo: "+ cumuloLinea.getLineaDTO().getSubRamo().getDescription());
		}catch(Exception e){
			lineaSoporteReaseguroDTO.setNotaDelSistema("Cotizaci�n"+(esSoporteEndoso?" de endoso ":"")+" sobre Subramo: no disponible");
		}
		lineaSoporteReaseguroDTO.setMontoSumaAsegurada(new BigDecimal(cumuloLinea.getSumaAsegurada()));
		lineaSoporteReaseguroDTO.setPorcentajePleno(new BigDecimal(cumuloLinea.getPorcentajePleno()));
		lineaSoporteReaseguroDTO.setIdToCotizacion(cumuloLinea.getIdCotizacion());
		lineaSoporteReaseguroDTO.setEstatusLineaSoporte(1);
		lineaSoporteReaseguroDTO.setFechaModificacion(new Date());
		lineaSoporteReaseguroDTO.setFechaCreacion(new Date());
		lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(BigDecimal.ZERO);
		lineaSoporteReaseguroDTO.setPorcentajeRetencion(BigDecimal.ZERO);
		lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(BigDecimal.ZERO);
		lineaSoporteReaseguroDTO.setPorcentajeFacultativo(BigDecimal.ZERO);
		lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDTO.ESTATUS_SOPORTADO_POR_REASEGURO);
		lineaSoporteReaseguroDTO.setNumeroEndoso(numeroEndoso);
		if(lineaSoporteReaseguroAnteriorDTO != null && lineaSoporteReaseguroAnteriorDTO.getPorcentajePleno() != null ){
			lineaSoporteReaseguroDTO.setPorcentajePleno(lineaSoporteReaseguroAnteriorDTO.getPorcentajePleno());
		}
		//10/01/2010 Se considera el nuevo campo "aplicaDistribucion" para identificar si se tiene prima por distribuir.
		lineaSoporteReaseguroDTO.setAplicaDistribucionPrima(LineaSoporteReaseguroDTO.NO_APLICA_DISTRIBUCION_PRIMA);
		if (cumuloLinea.getDetalleCoberturas() != null){
			for (DetalleCobertura detalleCobertura : cumuloLinea.getDetalleCoberturas()) {
				LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO = new LineaSoporteCoberturaDTO();
				if (detalleCobertura.getNumeroInciso() != null)
					lineaSoporteCoberturaDTO.setNumeroInciso(BigDecimal.valueOf(detalleCobertura.getNumeroInciso()));
				if(detalleCobertura.getNumeroSubinciso() != null)
					lineaSoporteCoberturaDTO.setNumeroSubInciso(BigDecimal.valueOf(detalleCobertura.getNumeroSubinciso())); 
				lineaSoporteCoberturaDTO.setIdToSeccion(detalleCobertura.getIdSeccion());
				lineaSoporteCoberturaDTO.setIdToCobertura(detalleCobertura.getIdCobertura());
				lineaSoporteCoberturaDTO.setPorcentajeCoaseguro(new BigDecimal(detalleCobertura.getPorcentajeCoaseguro()));
				lineaSoporteCoberturaDTO.setMontoPrimaSuscripcion(new BigDecimal(detalleCobertura.getMontoPrimaSuscripcion()));
				lineaSoporteCoberturaDTO.setMontoPrimaFacultativo(new BigDecimal(detalleCobertura.getMontoPrimaSuscripcion()));
				lineaSoporteCoberturaDTO.setMontoPrimaAdicional(BigDecimal.ZERO);
				lineaSoporteCoberturaDTO.setFechaCreacion(new Date());
				lineaSoporteCoberturaDTO.setFechaModificacion(new Date());
				if(esSoporteEndoso){
					lineaSoporteCoberturaDTO.setMontoPrimaNoDevengada(new BigDecimal(detalleCobertura.getMontoPrimaNoDevengada()));
				}
				if(detalleCobertura.getMonedaPrima() != null)
					lineaSoporteCoberturaDTO.setIdMoneda(BigDecimal.valueOf(detalleCobertura.getMonedaPrima().doubleValue()));
				else
					lineaSoporteCoberturaDTO.setIdMoneda(cumuloLinea.getIdMoneda());
				lineaSoporteCoberturaDTO.setPorcentajeDeducible(new BigDecimal(detalleCobertura.getPorcentajeDeducible()));
				lineaSoporteCoberturaDTO.setEsPrimerRiesgo(detalleCobertura.getEsPrimerRiesgo());
				try{
					lineaSoporteCoberturaDTO.setNotaDelSistema("Cobertura del Subramo: "+ lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo().getDescripcionSubRamo());
				}catch(Exception e){
					lineaSoporteCoberturaDTO.setNotaDelSistema("Cobertura del Subramo: no disponible");
				}
				lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().add(lineaSoporteCoberturaDTO);
				if ((detalleCobertura.getMontoPrimaSuscripcion().doubleValue() - 
						(detalleCobertura.getMontoPrimaNoDevengada()!= null ? detalleCobertura.getMontoPrimaNoDevengada().doubleValue() : 0)) != 0){
					lineaSoporteReaseguroDTO.setAplicaDistribucionPrima(LineaSoporteReaseguroDTO.APLICA_DISTRIBUCION_PRIMA);
				}
			}
		}
		if(calcularLineaSoporte)
			calculaSoporteReaseguro(lineaSoporteReaseguroDTO,lineaSoporteReaseguroAnteriorDTO);
		else if (lineaSoporteReaseguroAnteriorDTO != null){
			lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(lineaSoporteReaseguroAnteriorDTO.getPorcentajeCuotaParte());
			lineaSoporteReaseguroDTO.setPorcentajeFacultativo(lineaSoporteReaseguroAnteriorDTO.getPorcentajeFacultativo());
			lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(lineaSoporteReaseguroAnteriorDTO.getPorcentajePrimerExcedente());
			lineaSoporteReaseguroDTO.setPorcentajeRetencion(lineaSoporteReaseguroAnteriorDTO.getPorcentajeRetencion());
			
			/*
			 * 14/01/2011 Se implementa correcci�n al c�lculo de porcentajes. En los casos donde se mantiene se disminuye la SA, se deben
			 * aplicar los % globales de distribuci�n, y no los del c�lculo de distribuci�n de prima.
			 */
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeRetencion(lineaSoporteReaseguroAnteriorDTO.getPorcentajeRetencion());
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeCuotaParte(lineaSoporteReaseguroAnteriorDTO.getPorcentajeCuotaParte());
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajePrimerExcedente(lineaSoporteReaseguroAnteriorDTO.getPorcentajePrimerExcedente());
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeFacultativo(lineaSoporteReaseguroAnteriorDTO.getPorcentajeFacultativo());

			BigDecimal diferenciaSumaAsegurada = lineaSoporteReaseguroDTO.getMontoSumaAsegurada().subtract(lineaSoporteReaseguroAnteriorDTO.getMontoSumaAsegurada());
			
			//18/03/2011 Se agrega registro de la SA del endoso
			lineaSoporteReaseguroDTO.setMontoSumaAseguradaEndoso(diferenciaSumaAsegurada);
			diferenciaSumaAsegurada = obtenerSumaAseguradaEnDolares(diferenciaSumaAsegurada, new BigDecimal(tipoCambio), lineaSoporteReaseguroDTO.getIdMoneda().intValue());
			
			
			if (lineaSoporteReaseguroAnteriorDTO.getMontoSumaAseguradaAcumulada() == null) {
				lineaSoporteReaseguroAnteriorDTO.setMontoSumaAseguradaAcumulada(new BigDecimal("0"));
			}
			
			BigDecimal sumaAseguradaAcumulada = lineaSoporteReaseguroAnteriorDTO.getMontoSumaAseguradaAcumulada().add(diferenciaSumaAsegurada);
			lineaSoporteReaseguroDTO.setMontoSumaAseguradaAcumulada(sumaAseguradaAcumulada);
		}
		if(lineaSoporteReaseguroDTO.getPorcentajeFacultativo().doubleValue() > 0d)
			lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDTO.ESTATUS_REQUIERE_FACULTATIVO);
		if (lineaSoporteReaseguroAnteriorDTO != null){
			if(lineaSoporteReaseguroAnteriorDTO.getContratoFacultativoDTO() != null &&
					lineaSoporteReaseguroAnteriorDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo() != null)
				lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDTO.ENDOSO_FACULTATIVO);
		}
		return lineaSoporteReaseguroDTO;
	}
	
	/**
	 * Para calcular el Soporte de Reaseguro primero tendra que validar si la linea tiene todos los datos necesarios para 
	 * ello.
	 * Condiciones:
	 * 1.- Debe tener una LineaDTO autorizada
	 * 2.- Debe tener Suma Asegurada mayor de 0
	 * 3.- Debe tener Porcentaje de Pleno mayor de %0 hasta 100%.
	 * @throws Exception 
	 *  
	 */
	private void calculaSoporteReaseguro(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,LineaSoporteReaseguroDTO lineaSoporteReaseguroAnteriorDTO) throws Exception {
		if (lineaSoporteReaseguroDTO.getLineaDTO() != null && lineaSoporteReaseguroDTO.getEstatusLineaSoporte().intValue() == 1){
			if (lineaSoporteReaseguroDTO.getLineaDTO().getEstatus().intValue() == 1 &&
					lineaSoporteReaseguroDTO.getPorcentajePleno().doubleValue() >= 0.0 && 
					lineaSoporteReaseguroDTO.getMontoSumaAsegurada().compareTo(BigDecimal.ZERO) >= 0 &&
						(lineaSoporteReaseguroDTO.getEstatusFacultativo() == LineaSoporteReaseguroDTO.ESTATUS_SOPORTADO_POR_REASEGURO 
							|| lineaSoporteReaseguroDTO.getEstatusFacultativo() == LineaSoporteReaseguroDTO.ESTATUS_REQUIERE_FACULTATIVO) ){
//				defineSoporteReaseguro(lineaSoporteReaseguroDTO,null,null);
				calcularPorcentajesLineaSoporteReaseguro(lineaSoporteReaseguroDTO, lineaSoporteReaseguroAnteriorDTO, null, null);
			}
		}
	}
	
	private void calcularPorcentajesLineaSoporteReaseguro(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,LineaSoporteReaseguroDTO lineaSoporteReaseguroAnteriorDTO,BigDecimal montoNuevoParaFacultarUSD, BigDecimal porcentajeNuevoParaFacultar) throws Exception{
		int escala = 12;
		BigDecimal maximo  = new BigDecimal(lineaSoporteReaseguroDTO.getLineaDTO().getMaximo());
		BigDecimal porcentajeCesionCP = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajeRetencion = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal plenosPrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal montoPlenoPrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal capacidadPrimerExcedente  = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal cien = new BigDecimal(100d).setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajePleno = lineaSoporteReaseguroDTO.getPorcentajePleno().setScale(escala,RoundingMode.HALF_UP);

		BigDecimal porcentajeACesionCP = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajeARetencion = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajePrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajeFacultativo = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal diferienciaPorcentual = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal tipoCambio = new BigDecimal(lineaSoporteReaseguroDTO.getTipoCambio()).setScale(escala,RoundingMode.HALF_UP);
		final BigDecimal CANTIDAD_INSIGNIFICANTE = new BigDecimal(0.009d);
		BigDecimal sumaAseguradaAcumulada = new BigDecimal(0d).setScale(escala,RoundingMode.HALF_UP);
		BigDecimal sumaAseguradaEndoso = BigDecimal.ZERO;
		
		/*
		 * Variables usadas para los casos de endosos
		 */
		BigDecimal sumaAseguradaEndosoAnterior = null;
		BigDecimal sumaAseguradaRetEndosoAnterior = null;
		BigDecimal sumaAseguradaCPEndosoAnterior = null;
		BigDecimal sumaAsegurada1EEndosoAnterior = null;
		BigDecimal sumaAseguradaFacEndosoAnterior = null;
		BigDecimal diferenciaSumaAseguradaUSD = null;
		BigDecimal diferenciaSumaAsegurada =null;
		
		// Paso 1 .- Obtiene datos de la Linea de Reaseguro
		if (lineaSoporteReaseguroDTO.getLineaDTO().getContratoCuotaParte() != null){
			porcentajeCesionCP = new BigDecimal(lineaSoporteReaseguroDTO.getLineaDTO().getContratoCuotaParte().getPorcentajeCesion()).setScale(escala,RoundingMode.HALF_UP);;
			porcentajeRetencion = new BigDecimal(lineaSoporteReaseguroDTO.getLineaDTO().getContratoCuotaParte().getPorcentajeRetencion()).setScale(escala,RoundingMode.HALF_UP);;
		}else{
			porcentajeRetencion = cien;
		}

		if (lineaSoporteReaseguroDTO.getLineaDTO().getContratoPrimerExcedente() != null ) {
			plenosPrimerExcedente = lineaSoporteReaseguroDTO.getLineaDTO().getContratoPrimerExcedente().getNumeroPlenos().setScale(escala,RoundingMode.HALF_UP);
			montoPlenoPrimerExcedente = new BigDecimal(lineaSoporteReaseguroDTO.getLineaDTO().getContratoPrimerExcedente().getMontoPleno()).setScale(escala,RoundingMode.HALF_UP);
			capacidadPrimerExcedente = montoPlenoPrimerExcedente.multiply(plenosPrimerExcedente).setScale(escala, RoundingMode.HALF_UP);
		} 
		//Paso 1.- Redefine la capacidad de la linea basado en la estimaci�n del pleno de suscripci�n.
		maximo = (maximo.multiply(porcentajePleno)).divide(cien,escala,RoundingMode.HALF_UP);
		capacidadPrimerExcedente = (capacidadPrimerExcedente.multiply(porcentajePleno)).divide(cien,escala,RoundingMode.HALF_UP);

		BigDecimal capacidadMaximaLinea = maximo.add(capacidadPrimerExcedente).setScale(escala, RoundingMode.HALF_UP);
		
		/*
		 * Obtener las capacidades disponibles basadas en las capacidades usadas en el endoso anterior
		 */
		if(lineaSoporteReaseguroAnteriorDTO != null){
			sumaAseguradaEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getMontoSumaAseguradaAcumulada();//obtenerSumaAseguradaEnDolares(lineaSoporteReaseguroAnteriorDTO.getMontoSumaAsegurada(), new BigDecimal (lineaSoporteReaseguroAnteriorDTO.getTipoCambio()).setScale(escala,RoundingMode.HALF_UP), lineaSoporteReaseguroAnteriorDTO.getIdMoneda().intValue());
			sumaAseguradaRetEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeRetencion().multiply(sumaAseguradaEndosoAnterior).divide(cien);
			sumaAseguradaCPEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeCuotaParte().multiply(sumaAseguradaEndosoAnterior).divide(cien);
			sumaAsegurada1EEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajePrimerExcedente().multiply(sumaAseguradaEndosoAnterior).divide(cien);
			sumaAseguradaFacEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeFacultativo().multiply(sumaAseguradaEndosoAnterior).divide(cien);
			
			/**
			 * 12/07/2010. Jose Luis Arellano. Se agreg� validaci�n para establecer cantidades disponibles de SA insignificantes en cero
			 */
			maximo = maximo.subtract(sumaAseguradaRetEndosoAnterior).subtract(sumaAseguradaCPEndosoAnterior);
			if(maximo.compareTo(CANTIDAD_INSIGNIFICANTE) < 0)
				maximo = BigDecimal.ZERO;
			capacidadPrimerExcedente = capacidadPrimerExcedente.subtract(sumaAsegurada1EEndosoAnterior);
			if(capacidadPrimerExcedente.compareTo(CANTIDAD_INSIGNIFICANTE) < 0)
				capacidadPrimerExcedente = BigDecimal.ZERO;
			capacidadMaximaLinea = maximo.add(capacidadPrimerExcedente);
			
			diferenciaSumaAsegurada = lineaSoporteReaseguroDTO.getMontoSumaAsegurada().subtract(lineaSoporteReaseguroAnteriorDTO.getMontoSumaAsegurada());

			diferenciaSumaAseguradaUSD = obtenerSumaAseguradaEnDolares(diferenciaSumaAsegurada, tipoCambio, lineaSoporteReaseguroDTO.getIdMoneda().intValue());
			sumaAseguradaAcumulada = sumaAseguradaAcumulada.add(sumaAseguradaEndosoAnterior).setScale(escala,RoundingMode.HALF_UP);
		}

		//Paso 3.- Obtener participacion Facultativa
		BigDecimal montoParaFacultar = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
	    BigDecimal montoSumaAseguradaUSD = diferenciaSumaAseguradaUSD != null ? diferenciaSumaAseguradaUSD : obtenerSumaAseguradaEnDolares(lineaSoporteReaseguroDTO.getMontoSumaAsegurada(), tipoCambio, lineaSoporteReaseguroDTO.getIdMoneda().intValue());

	    sumaAseguradaEndoso = diferenciaSumaAsegurada != null ? diferenciaSumaAsegurada : lineaSoporteReaseguroDTO.getMontoSumaAsegurada();

	    sumaAseguradaAcumulada = sumaAseguradaAcumulada.add(montoSumaAseguradaUSD).setScale(escala,RoundingMode.HALF_UP);
	    
		if( montoSumaAseguradaUSD.compareTo(capacidadMaximaLinea) > 0 ) {
			montoParaFacultar = montoSumaAseguradaUSD.subtract(capacidadMaximaLinea).setScale(escala, RoundingMode.HALF_UP); 
			//this.setPorcentajeFacultativo((montoParaFacultar.divide(montoSumaAseguradaUSD,RoundingMode.HALF_DOWN)).multiply(cien).doubleValue());
			porcentajeFacultativo = (montoParaFacultar.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
		}

	    //Paso 3a.- Si se requiere facultar una suma � porcentaje en especifico
        if (montoNuevoParaFacultarUSD != null && montoNuevoParaFacultarUSD.compareTo(BigDecimal.ZERO) > 0){
        	//debe ser mayor 0 igual a lo originalmente calculado a facultar, de lo contrario no procede.
        	if (montoNuevoParaFacultarUSD.compareTo(montoParaFacultar) >= 0){
        		//y menor o igual a la suma asegurada total
        		if (montoNuevoParaFacultarUSD.compareTo(montoSumaAseguradaUSD) <= 0){
        			montoParaFacultar = montoNuevoParaFacultarUSD;
        			porcentajeFacultativo = (montoNuevoParaFacultarUSD.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala, RoundingMode.HALF_UP);
        		}else{
        			throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede Facultar una Suma Mayor <br>a la Cotizada: <br>USD$" + montoSumaAseguradaUSD.setScale(2, RoundingMode.UP) + "<br> MX$" + montoSumaAseguradaUSD.multiply(tipoCambio));
        		}
        	}else{
        		throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede Facultar una Suma Menor <br>a la Originalmente Requerida <br>por superaci�n de contratos : <br>USD$" + montoParaFacultar.setScale(2, RoundingMode.UP) + "<br> MX$" + montoParaFacultar.multiply(tipoCambio));
        	}
        } else if (porcentajeNuevoParaFacultar != null && porcentajeNuevoParaFacultar.compareTo(BigDecimal.ZERO) > 0){
        	//debe ser mayor 0 igual a lo originalmente calculado a facultar, de lo contrario no procede.
        	if (porcentajeNuevoParaFacultar.compareTo(porcentajeFacultativo) >= 0){
        		//y menor o igual al 100%  de la suma asegurada
        		if (porcentajeNuevoParaFacultar.compareTo(cien) <= 0){
        			montoParaFacultar = montoSumaAseguradaUSD.multiply(porcentajeNuevoParaFacultar).divide(cien,escala,RoundingMode.HALF_UP);
        			porcentajeFacultativo = porcentajeNuevoParaFacultar;		
        		}else{
        			throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede Facultar una porcentaje mayor "+ porcentajeNuevoParaFacultar.setScale(4, RoundingMode.HALF_UP) +"%  al " + cien +"%");
        		}
        	}else{
        		throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","Este porcentaje "+porcentajeNuevoParaFacultar.setScale(4, RoundingMode.HALF_UP)+"% es menor <br> al necesario requerido por superaci�n de la Linea Reaseguro <br> De : " + porcentajeFacultativo.setScale(4, RoundingMode.UP) + "%");
        	}
        }
		
		// Paso 3b.- Ajuste de capacidades basado en lo que se faculta.
        if (montoSumaAseguradaUSD.compareTo(capacidadMaximaLinea) > 0 || porcentajeFacultativo.compareTo(BigDecimal.ZERO) > 0){
        	BigDecimal excedente = montoSumaAseguradaUSD.subtract(capacidadMaximaLinea);
        	if ((montoParaFacultar.subtract(excedente)).compareTo(capacidadPrimerExcedente) >= 0){
        		maximo = maximo.subtract((montoParaFacultar.subtract(excedente)).subtract(capacidadPrimerExcedente)).setScale(escala, RoundingMode.HALF_UP);
        	}
        }
		
		// Paso 4.- Calcula la Suma Asegurada a Cuota Parte, Retenci�n y la de Primer Excedente
		BigDecimal montoParaMaximo = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal montoParaPrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
    	
		if (( montoSumaAseguradaUSD.subtract(maximo)).compareTo(BigDecimal.ZERO) > 0){
			montoParaMaximo = maximo;
			montoParaPrimerExcedente = montoSumaAseguradaUSD.subtract(maximo).subtract(montoParaFacultar).setScale(escala, RoundingMode.HALF_UP);
		} else {
			montoParaMaximo = montoSumaAseguradaUSD;
		}

		BigDecimal montoParaCuotaParte = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		// Paso 5.- Obtener participacion de Cuota Parte
		if ( porcentajeCesionCP.compareTo(BigDecimal.ZERO) > 0 && ( montoParaMaximo.compareTo(BigDecimal.ZERO) > 0 ) ) {
			montoParaCuotaParte = ((montoParaMaximo.multiply(porcentajeCesionCP)).divide(cien));
		//this.setPorcentajeCuotaParte((montoParaCuotaParte.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien).doubleValue());
			porcentajeACesionCP = (montoParaCuotaParte.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
		} 

		BigDecimal montoParaRetencion = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		// Paso 6.- Obtener participacion de Retencion
		if ( porcentajeRetencion.compareTo(BigDecimal.ZERO) > 0 && ( montoParaMaximo.compareTo(BigDecimal.ZERO) > 0 ) ) {
			montoParaRetencion = (montoParaMaximo.multiply(porcentajeRetencion)).divide(cien,escala,RoundingMode.HALF_UP);
		//this.setPorcentajeRetencion((montoParaRetencion.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien).doubleValue());
			porcentajeARetencion = (montoParaRetencion.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala, RoundingMode.HALF_UP);
		}
			// Paso 7.- Obtener participacion de Primer Excedente
		if (montoParaPrimerExcedente.compareTo(BigDecimal.ZERO) > 0){
			//this.setPorcentajePrimerExcedente((montoParaPrimerExcedente.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien).doubleValue());
			porcentajePrimerExcedente = (montoParaPrimerExcedente.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala, RoundingMode.HALF_UP);
		}
		//Proceso para Corregir desviaciones sobre la distribucion al 100%
		porcentajeARetencion = porcentajeARetencion.setScale(escala,RoundingMode.HALF_UP);
		porcentajeACesionCP = porcentajeACesionCP.setScale(escala,RoundingMode.HALF_UP);
		porcentajePrimerExcedente = porcentajePrimerExcedente.setScale(escala,RoundingMode.HALF_UP);
		porcentajeFacultativo = porcentajeFacultativo.setScale(escala,RoundingMode.HALF_UP);
		
		if (!(porcentajeARetencion.add(porcentajeACesionCP).add(porcentajePrimerExcedente).add(porcentajeFacultativo).setScale(escala).compareTo(cien) == 0)){
			diferienciaPorcentual = cien.subtract(porcentajeARetencion.add(porcentajeACesionCP).add(porcentajePrimerExcedente).add(porcentajeFacultativo)).setScale(escala);
			if (porcentajeFacultativo.compareTo(BigDecimal.ZERO) > 0){
				porcentajeFacultativo = porcentajeFacultativo.add(diferienciaPorcentual);
			} else if (porcentajePrimerExcedente.compareTo(BigDecimal.ZERO) > 0) {
				porcentajePrimerExcedente = porcentajePrimerExcedente.add(diferienciaPorcentual);
			} else if(porcentajeACesionCP.compareTo(BigDecimal.ZERO) > 0){
				porcentajeACesionCP = porcentajeACesionCP.add(diferienciaPorcentual.multiply(porcentajeACesionCP).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
				if(porcentajeARetencion.compareTo(BigDecimal.ZERO) > 0){
					porcentajeARetencion = porcentajeARetencion.add(diferienciaPorcentual.multiply(porcentajeARetencion).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
				}
			} else 
				porcentajeARetencion = porcentajeARetencion.add(diferienciaPorcentual.multiply(porcentajeARetencion).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
		}
		
		/*
		 * los porcentajes y montos calculados pueden ser los del endoso de aumento
		 */
		if(lineaSoporteReaseguroAnteriorDTO != null){
			/*
			 * 01/11/2010 Se agreg� esta validaci�n debido a que hay casos en que da�os env�a una suma asegurada mayor por mil�simas, que son insignificantes,
			 * y esto causa que la suma asegurada en dolares sea 0 (por redondeo) y los porcentajes calculados se generan con valor de cero.
			 */
			if(diferenciaSumaAseguradaUSD.compareTo(CANTIDAD_INSIGNIFICANTE) > 0){
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajeRetencion(porcentajeARetencion);
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajeCuotaParte(porcentajeACesionCP);
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajePrimerExcedente(porcentajePrimerExcedente);
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajeFacultativo(porcentajeFacultativo);
				
				sumaAseguradaEndosoAnterior = obtenerSumaAseguradaEnDolares(lineaSoporteReaseguroAnteriorDTO.getMontoSumaAsegurada(), new BigDecimal (lineaSoporteReaseguroDTO.getTipoCambio()).setScale(escala,RoundingMode.HALF_UP), lineaSoporteReaseguroAnteriorDTO.getIdMoneda().intValue());
				sumaAseguradaRetEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeRetencion().multiply(sumaAseguradaEndosoAnterior).divide(cien);
				sumaAseguradaCPEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeCuotaParte().multiply(sumaAseguradaEndosoAnterior).divide(cien);
				sumaAsegurada1EEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajePrimerExcedente().multiply(sumaAseguradaEndosoAnterior).divide(cien);
				sumaAseguradaFacEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeFacultativo().multiply(sumaAseguradaEndosoAnterior).divide(cien);
				
				//posteriormente, calcular los porcentajes totales asignados a Ret, CP, 1E y Fac
				BigDecimal sumaAseguradaRet = sumaAseguradaRetEndosoAnterior.add(montoParaRetencion);
				BigDecimal sumaAseguradaCP = sumaAseguradaCPEndosoAnterior.add(montoParaCuotaParte);
				BigDecimal sumaAsegurada1E = sumaAsegurada1EEndosoAnterior.add(montoParaPrimerExcedente);
				BigDecimal sumaAseguradaFac = sumaAseguradaFacEndosoAnterior.add(montoParaFacultar);
				
				
				BigDecimal montoTotalSumaAsegurada = obtenerSumaAseguradaEnDolares(lineaSoporteReaseguroDTO.getMontoSumaAsegurada(), tipoCambio, lineaSoporteReaseguroDTO.getIdMoneda().intValue()).setScale(escala);
				BigDecimal porcentajeRetSiniestros = sumaAseguradaRet.divide(montoTotalSumaAsegurada,escala,RoundingMode.HALF_UP).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
				BigDecimal porcentajeCPSiniestros = sumaAseguradaCP.divide(montoTotalSumaAsegurada,escala,RoundingMode.HALF_UP).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
				BigDecimal porcentaje1ESiniestros = sumaAsegurada1E.divide(montoTotalSumaAsegurada,escala,RoundingMode.HALF_UP).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
				BigDecimal porcentajeFacSiniestros = sumaAseguradaFac.divide(montoTotalSumaAsegurada,escala,RoundingMode.HALF_UP).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
				
				if (!(porcentajeRetSiniestros.add(porcentajeCPSiniestros).add(porcentaje1ESiniestros).add(porcentajeFacSiniestros).setScale(escala).compareTo(cien) == 0)){
					diferienciaPorcentual = cien.subtract(porcentajeRetSiniestros.add(porcentajeCPSiniestros).add(porcentaje1ESiniestros).add(porcentajeFacSiniestros)).setScale(escala);
					if (porcentajeFacSiniestros.compareTo(BigDecimal.ZERO) > 0){
						porcentajeFacSiniestros = porcentajeFacSiniestros.add(diferienciaPorcentual);
					} else if (porcentaje1ESiniestros.compareTo(BigDecimal.ZERO) > 0) {
						porcentaje1ESiniestros = porcentaje1ESiniestros.add(diferienciaPorcentual);
					} else if(porcentajeCPSiniestros.compareTo(BigDecimal.ZERO) > 0){
						porcentajeCPSiniestros = porcentajeCPSiniestros.add(diferienciaPorcentual.multiply(porcentajeCPSiniestros).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
						if(porcentajeRetSiniestros.compareTo(BigDecimal.ZERO) > 0){
							porcentajeRetSiniestros = porcentajeRetSiniestros.add(diferienciaPorcentual.multiply(porcentajeRetSiniestros).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
						}
					} else 
						porcentajeRetSiniestros = porcentajeRetSiniestros.add(diferienciaPorcentual.multiply(porcentajeRetSiniestros).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
				}
				
				lineaSoporteReaseguroDTO.setPorcentajeRetencion(porcentajeRetSiniestros);
				lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(porcentajeCPSiniestros);
				lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(porcentaje1ESiniestros);
				lineaSoporteReaseguroDTO.setPorcentajeFacultativo(porcentajeFacSiniestros);
			}
			else{
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajeRetencion(lineaSoporteReaseguroAnteriorDTO.getDisPrimaPorcentajeRetencion());
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajeCuotaParte(lineaSoporteReaseguroAnteriorDTO.getDisPrimaPorcentajeCuotaParte());
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajePrimerExcedente(lineaSoporteReaseguroAnteriorDTO.getDisPrimaPorcentajePrimerExcedente());
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajeFacultativo(lineaSoporteReaseguroAnteriorDTO.getDisPrimaPorcentajeFacultativo());
				
				lineaSoporteReaseguroDTO.setPorcentajeRetencion(lineaSoporteReaseguroAnteriorDTO.getPorcentajeRetencion());
				lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(lineaSoporteReaseguroAnteriorDTO.getPorcentajeCuotaParte());
				lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(lineaSoporteReaseguroAnteriorDTO.getPorcentajePrimerExcedente());
				lineaSoporteReaseguroDTO.setPorcentajeFacultativo(lineaSoporteReaseguroAnteriorDTO.getPorcentajeFacultativo());
			}
		}else{
		
			lineaSoporteReaseguroDTO.setPorcentajeRetencion(porcentajeARetencion);
			lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(porcentajeACesionCP);
			lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(porcentajePrimerExcedente);
			lineaSoporteReaseguroDTO.setPorcentajeFacultativo(porcentajeFacultativo);
			
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeRetencion(porcentajeARetencion);
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeCuotaParte(porcentajeACesionCP);
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajePrimerExcedente(porcentajePrimerExcedente);
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeFacultativo(porcentajeFacultativo);
		}
		lineaSoporteReaseguroDTO.setMontoSumaAseguradaAcumulada(sumaAseguradaAcumulada);
		lineaSoporteReaseguroDTO.setMontoSumaAseguradaEndoso(sumaAseguradaEndoso);
	}
	
	private BigDecimal obtenerSumaAseguradaEnDolares(BigDecimal montoSumaAsegurada,BigDecimal tipoCambio,int idMoneda) throws Exception{
		BigDecimal montoSumaAseguradaUSD = montoSumaAsegurada.setScale(4,RoundingMode.HALF_UP);
	    if(idMoneda != MonedaDTO.MONEDA_DOLARES){
	    	if(tipoCambio == null){
	    		tipoCambio = new BigDecimal(tipoCambioFacade.obtieneTipoCambioPorDia(new Date(), "TMP"));
	    	}
	    	montoSumaAseguradaUSD = montoSumaAseguradaUSD.divide(tipoCambio,RoundingMode.HALF_UP);
	    }
	    return montoSumaAseguradaUSD;
	}
}