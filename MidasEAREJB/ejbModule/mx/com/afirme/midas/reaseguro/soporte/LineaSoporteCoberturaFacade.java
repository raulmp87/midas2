package mx.com.afirme.midas.reaseguro.soporte;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity LineaSoporteCoberturaDTO.
 * 
 * @see .LineaSoporteCoberturaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class LineaSoporteCoberturaFacade implements
		LineaSoporteCoberturaFacadeRemote {
	// property constants
	public static final String PORCENTAJE_COASEGURO = "porcentajeCoaseguro";
	public static final String PORCENTAJE_DEDUCIBLE = "porcentajeDeducible";
	public static final String MONTO_PRIMA_SUSCRIPCION = "montoPrimaSuscripcion";
	public static final String MONTO_PRIMA_FACULTATIVO = "montoPrimaFacultativo";
	public static final String APLICA_CONTROL_RECLAMO = "aplicaControlReclamo";
	public static final String FECHA_CREACION = "fechaCreacion";
	public static final String FECHA_MODIFICACION = "fechaModificacion";
	public static final String ID_TO_COBERTURA = "idToCobertura";
	public static final String ID_TO_POLIZA = "idToPoliza";
	public static final String ID_TO_SECCION = "idToSeccion";
	public static final String ID_TO_PERSONA_ASEGURADO = "idToPersonaAsegurado";
	public static final String NUMERO_INCISO = "numeroInciso";
	public static final String NUMERO_SUBINCISO = "numeroSubInciso";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved LineaSoporteCoberturaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            LineaSoporteCoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public LineaSoporteCoberturaDTO save(LineaSoporteCoberturaDTO entity) {
		LogDeMidasEJB3.log("saving LineaSoporteCoberturaDTO instance", Level.INFO,null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save LineaSoporteCoberturaDTO  successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save LineaSoporteCoberturaDTO failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent LineaSoporteCoberturaDTO entity.
	 * 
	 * @param entity
	 *            LineaSoporteCoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(LineaSoporteCoberturaDTO entity) {
		LogDeMidasEJB3.log("deleting LineaSoporteCoberturaDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(LineaSoporteCoberturaDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved LineaSoporteCoberturaDTO entity and return it
	 * or a copy of it to the sender. A copy of the LineaSoporteCoberturaDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            LineaSoporteCoberturaDTO entity to update
	 * @return LineaSoporteCoberturaDTO the persisted LineaSoporteCoberturaDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public LineaSoporteCoberturaDTO update(LineaSoporteCoberturaDTO entity) {
		LogDeMidasEJB3.log("updating LineaSoporteCoberturaDTO instance", Level.INFO,
				null);
		try {
			LineaSoporteCoberturaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public LineaSoporteCoberturaDTO findById(LineaSoporteCoberturaDTOId id) {
		LogDeMidasEJB3.log("finding LineaSoporteCoberturaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			LineaSoporteCoberturaDTO instance = entityManager.find(
					LineaSoporteCoberturaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all LineaSoporteCoberturaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the LineaSoporteCoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<LineaSoporteCoberturaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<LineaSoporteCoberturaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding LineaSoporteCoberturaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from LineaSoporteCoberturaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<LineaSoporteCoberturaDTO> obtenPorLlaveCobertura(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO) {
		LogDeMidasEJB3.log("finding LineaSoporteCoberturaDTO obteniendo por Llave Coberura: ", Level.INFO, null);
		try {
			 String queryString = "select model from LineaSoporteCoberturaDTO model ";
				boolean eresPrimerCondicion = true;

				if(lineaSoporteCoberturaDTO.getIdToPoliza() != null){
					if (!eresPrimerCondicion){ queryString = queryString + " and ";
						} else {queryString = queryString + " where "; eresPrimerCondicion = false;}
					
					queryString = queryString + "model."+ ID_TO_POLIZA + " = :"+ ID_TO_POLIZA;
				}
				if(lineaSoporteCoberturaDTO.getIdToSeccion() != null){
					if (!eresPrimerCondicion){ queryString = queryString + " and ";
					} else {queryString = queryString + " where "; eresPrimerCondicion = false;}
					queryString = queryString + "model."+ ID_TO_SECCION + " = :"+ ID_TO_SECCION;
				}
				if(lineaSoporteCoberturaDTO.getIdToCobertura() != null){
					if (!eresPrimerCondicion){ queryString = queryString + " and ";
					} else {queryString = queryString + " where "; eresPrimerCondicion = false;}
					queryString = queryString + "model."+ ID_TO_COBERTURA + " = :"+ ID_TO_COBERTURA;
				}
				if(lineaSoporteCoberturaDTO.getIdToPersonaAsegurado() != null){
					if (!eresPrimerCondicion){ queryString = queryString + " and ";
					} else {queryString = queryString + " where "; eresPrimerCondicion = false;}
					queryString = queryString + "model."+ ID_TO_PERSONA_ASEGURADO + " = :"+ ID_TO_PERSONA_ASEGURADO;
				}
				if(lineaSoporteCoberturaDTO.getNumeroInciso() != null){
					if (!eresPrimerCondicion){ queryString = queryString + " and ";
					} else {queryString = queryString + " where "; eresPrimerCondicion = false;}
					queryString = queryString + "model."+ NUMERO_INCISO + " = :"+ NUMERO_INCISO;
				}
				if(lineaSoporteCoberturaDTO.getNumeroSubInciso() != null){
					if (!eresPrimerCondicion){ queryString = queryString + " and ";
					} else {queryString = queryString + " where "; eresPrimerCondicion = false;}
					queryString = queryString + "model."+ NUMERO_SUBINCISO + " = :"+ NUMERO_SUBINCISO;
				}
			Query query = entityManager.createQuery(queryString);

			if(lineaSoporteCoberturaDTO.getIdToPoliza() != null){
				query.setParameter(ID_TO_POLIZA, lineaSoporteCoberturaDTO.getIdToPoliza());
			}
			if(lineaSoporteCoberturaDTO.getIdToSeccion() != null){
				query.setParameter(ID_TO_SECCION, lineaSoporteCoberturaDTO.getIdToSeccion());
			}
			if(lineaSoporteCoberturaDTO.getIdToCobertura() != null){
				query.setParameter(ID_TO_COBERTURA, lineaSoporteCoberturaDTO.getIdToCobertura());
			}
			if(lineaSoporteCoberturaDTO.getIdToPersonaAsegurado() != null){
				query.setParameter(ID_TO_PERSONA_ASEGURADO, lineaSoporteCoberturaDTO.getIdToPersonaAsegurado());
			}
			if(lineaSoporteCoberturaDTO.getNumeroInciso() != null){
				query.setParameter(NUMERO_INCISO, lineaSoporteCoberturaDTO.getNumeroInciso());
			}
			if(lineaSoporteCoberturaDTO.getNumeroSubInciso() != null){
				query.setParameter(NUMERO_SUBINCISO, lineaSoporteCoberturaDTO.getNumeroSubInciso());
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenindo Por Llave Cobertura fall� ", Level.SEVERE, re);
			throw re;
		}
	}	

	public List<LineaSoporteCoberturaDTO> findByPorcentajeCoaseguro(
			Object porcentajeCoaseguro) {
		return findByProperty(PORCENTAJE_COASEGURO, porcentajeCoaseguro);
	}

	public List<LineaSoporteCoberturaDTO> findByPorcentajeDeducible(
			Object porcentajeDeducible) {
		return findByProperty(PORCENTAJE_DEDUCIBLE, porcentajeDeducible);
	}

	public List<LineaSoporteCoberturaDTO> findByMontoPrimaSuscripcion(
			Object montoPrimaSuscripcion) {
		return findByProperty(MONTO_PRIMA_SUSCRIPCION, montoPrimaSuscripcion);
	}

	public List<LineaSoporteCoberturaDTO> findByMontoPrimaFacultativo(
			Object montoPrimaFacultativo) {
		return findByProperty(MONTO_PRIMA_FACULTATIVO, montoPrimaFacultativo);
	}

	public List<LineaSoporteCoberturaDTO> findByAplicaControlReclamo(
			Object aplicaControlReclamo) {
		return findByProperty(APLICA_CONTROL_RECLAMO, aplicaControlReclamo);
	}

	/**
	 * Find all LineaSoporteCoberturaDTO entities.
	 * 
	 * @return List<LineaSoporteCoberturaDTO> all LineaSoporteCoberturaDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<LineaSoporteCoberturaDTO> findAll() {
		LogDeMidasEJB3.log("finding all LineaSoporteCoberturaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from LineaSoporteCoberturaDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public LineaSoporteCoberturaDTO getLineaSoporteCoberturaDTOByDetalleContratoFacultativoDTO(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO){
		String queryString = "SELECT model FROM LineaSoporteCoberturaDTO model";
		String whereString = "";
		
		if (detalleContratoFacultativoDTO.getIdToCobertura() != null)
			whereString = " model.idToCobertura = :idToCobertura";
		
		if (detalleContratoFacultativoDTO.getIdToSeccion()!= null){
			if (whereString.length() > 0)
				whereString += " AND";
			
			whereString += " model.idToSeccion = :idToSeccion";
		}
		
		if (detalleContratoFacultativoDTO.getNumeroInciso()!= null){
			if (whereString.length() > 0)
				whereString += " AND";
			
			whereString += " model.numeroInciso = :numeroInciso";
		}
		
		if (detalleContratoFacultativoDTO.getNumeroSubInciso()!= null){
			if (whereString.length() > 0)
				whereString += " AND";
			
			whereString += " model.numeroSubInciso = :numeroSubInciso";
		}
		
		if (detalleContratoFacultativoDTO.getIdToCotizacion()!= null){
			if (whereString.length() > 0)
				whereString += " AND";
			
			whereString += " model.lineaSoporteReaseguroDTO.idToCotizacion = :idToCotizacion";
		}
		
		queryString = queryString + " WHERE " + whereString;
		
		Query query = entityManager.createQuery(queryString);
		if (queryString.contains("idToCobertura"))
			query.setParameter("idToCobertura", detalleContratoFacultativoDTO.getIdToCobertura());
		if (queryString.contains("idToSeccion"))
			query.setParameter("idToSeccion", detalleContratoFacultativoDTO.getIdToSeccion());
		if (queryString.contains("numeroInciso"))
			query.setParameter("numeroInciso", detalleContratoFacultativoDTO.getNumeroInciso());
		if (queryString.contains("numeroSubInciso"))
			query.setParameter("numeroSubInciso", detalleContratoFacultativoDTO.getNumeroSubInciso());
		if (queryString.contains("idToCotizacion"))
			query.setParameter("idToCotizacion", detalleContratoFacultativoDTO.getIdToCotizacion());
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO = (LineaSoporteCoberturaDTO) query.getSingleResult();
		
		return lineaSoporteCoberturaDTO;	
	}

	public void eliminarLineasSoporteCoberturaPorLineaSoporteReaseguro(BigDecimal idTmLineaSoporteReaseguro) {
		LogDeMidasEJB3.log("eliminando registros LineaSoporteCoberturaDTO relacionados con la LineaSoporteReaseguro id: "+idTmLineaSoporteReaseguro, Level.INFO,null);
		try {
			final String queryString = "DELETE from LineaSoporteCoberturaDTO model where model.id.idTmLineaSoporteReaseguro = :idTmLineaSoporteReaseguro";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTmLineaSoporteReaseguro", idTmLineaSoporteReaseguro);
			query.executeUpdate();
			LogDeMidasEJB3.log("borrado exitoso de LineaSoporteCobertura de la LineaSoporteReaseguro con id: "+idTmLineaSoporteReaseguro, Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("fall� el borrado de LineaSoporteCobertura de la LineaSoporteReaseguro con id: "+idTmLineaSoporteReaseguro, Level.SEVERE, re);
			throw re;
		}
	}
	
	public void notificarEmisionCoberturasSoporteReaseguro(BigDecimal idToSoporteReaseguro,BigDecimal idToPoliza){
		try {
			LogDeMidasEJB3.log("Inicia notificaci�n de emision para las coberturas del soporte reaseguro: "+idToSoporteReaseguro+", id de la poliza: "+idToPoliza, Level.INFO, null);
			//Actualizacion del soporte reaseguro
			String queryString = "UPDATE MIDAS.TDLINEASOPREACOBERTURA SET IDTOPOLIZA = "+idToPoliza+", FECHAMODIFICACION= TO_DATE('"+(new java.sql.Date(new java.util.Date().getTime()).toString())+"','yyyy-MM-dd')"
				+" where idToSoporteReaseguro = "+idToSoporteReaseguro;
			Query query;
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Ocurri� un error en la notificaci�n de emision para las coberturas del soporte reaseguro: "+idToSoporteReaseguro+", id de la poliza: "+idToPoliza, Level.SEVERE, re);
			throw re;
		}
	}
	
	public void integrarCoberturasSoporteReaseguro(BigDecimal idTmLineaSoporteReaseguro){
		try {
			LogDeMidasEJB3.log("Inicia integracion de coberturas de la linea soporte reaseguro: "+idTmLineaSoporteReaseguro, Level.INFO, null);
			//Actualizacion del soporte reaseguro
			String queryString = "UPDATE MIDAS.TDLINEASOPREACOBERTURA SET montoPrimaSuscripcion = montoPrimaFacultativo, FECHAMODIFICACION= TO_DATE('"+(new java.sql.Date(new java.util.Date().getTime()).toString())+"','yyyy-MM-dd') "
				+" where idtmlineasoportereaseguro = "+idTmLineaSoporteReaseguro+" and montoPrimaSuscripcion <> montoPrimaFacultativo";
			Query query;
			query = entityManager.createNativeQuery(queryString);
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Ocurri� un error en la integraci�n de coberturas de la linea soporte reaseguro: "+idTmLineaSoporteReaseguro, Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<LineaSoporteCoberturaDTO> listarFiltrado(LineaSoporteCoberturaDTO entity) {
		try {
			LogDeMidasEJB3.log("listarFiltrado LineaSoporteCoberturaDTO ", Level.INFO, null);
			String queryString = "select model from LineaSoporteCoberturaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			if (entity.getId() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idTmLineaSoporteReaseguro", entity.getId().getIdTmLineaSoporteReaseguro());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToSoporteReaseguro", entity.getId().getIdToSoporteReaseguro());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idTdLineaSoporteCobertura", entity.getId().getIdTdLineaSoporteCobertura());
			}
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToPoliza", entity.getIdToPoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToSeccion", entity.getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroInciso", entity.getNumeroInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroSubInciso", entity.getNumeroSubInciso());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToCobertura", entity.getIdToCobertura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "porcentajeCoaseguro", entity.getPorcentajeCoaseguro());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "porcentajeDeducible", entity.getPorcentajeDeducible());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "montoPrimaSuscripcion", entity.getMontoPrimaSuscripcion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "montoPrimaFacultativo", entity.getMontoPrimaFacultativo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idMoneda", entity.getIdMoneda());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "fechaModificacion", entity.getFechaModificacion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "fechaCreacion", entity.getMontoPrimaNoDevengada());
			if(entity.getLineaSoporteReaseguroDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "lineaSoporteReaseguroDTO.idToCotizacion", entity.getLineaSoporteReaseguroDTO().getIdToCotizacion());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "lineaSoporteReaseguroDTO.estatusLineaSoporte", entity.getLineaSoporteReaseguroDTO().getEstatusLineaSoporte());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "lineaSoporteReaseguroDTO.estatusFacultativo", entity.getLineaSoporteReaseguroDTO().getEstatusFacultativo());
				
				if( entity.getLineaSoporteReaseguroDTO().getSoporteReaseguroDTO() != null){
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "lineaSoporteReaseguroDTO.soporteReaseguroDTO.fechaEmision", entity.getLineaSoporteReaseguroDTO().getSoporteReaseguroDTO().getFechaEmision());
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "lineaSoporteReaseguroDTO.soporteReaseguroDTO.numeroEndoso", entity.getLineaSoporteReaseguroDTO().getSoporteReaseguroDTO().getNumeroEndoso());
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "lineaSoporteReaseguroDTO.soporteReaseguroDTO.idToCotizacion", entity.getLineaSoporteReaseguroDTO().getSoporteReaseguroDTO().getIdToCotizacion());
				}
			}

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("FALLO: listarFiltrado LineaSoporteReaseguro", Level.SEVERE, re);
			throw re;
		}
	}
}