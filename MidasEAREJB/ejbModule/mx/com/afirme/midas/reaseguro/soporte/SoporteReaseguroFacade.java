package mx.com.afirme.midas.reaseguro.soporte;
// default package


import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoFacadeRemote;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipFacadeRemote;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity SoporteReaseguroDTO.
 * 
 * @see .SoporteReaseguroDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SoporteReaseguroFacade implements SoporteReaseguroFacadeRemote {
	// property constants

	@EJB
	LineaSoporteReaseguroFacadeRemote lineaSoporteReaseguroFacade;
	@EJB
	LineaSoporteCoberturaFacadeRemote lineaSoporteCoberturaFacade;

	@Resource
	private SessionContext context;
	
	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved SoporteReaseguroDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SoporteReaseguroDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */ 
	public SoporteReaseguroDTO save(SoporteReaseguroDTO entity) {
		LogDeMidasEJB3.log("saving SoporteReaseguroDTO instance", Level.INFO, null);
		try { 
			entityManager.persist(entity);
			entityManager.flush();
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	public void save(BigDecimal idToSoporteReaseguroDTO,List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTOs) {
		LogDeMidasEJB3.log("saving SoporteReaseguroDTO instance", Level.INFO, null);
		try { 
//			SoporteReaseguroDTO  instance = entityManager.find(SoporteReaseguroDTO.class, entity.getIdToSoporteReaseguro());
		 	List<LineaSoporteCoberturaDTO>  LineaSoporteCoberturaDTOlist = new ArrayList<LineaSoporteCoberturaDTO>();
		 	
			//guardando coberturas en memoria para despues ser guardadas
	 		for(LineaSoporteReaseguroDTO lineaSoporteReaseguro : lineaSoporteReaseguroDTOs){
				if(lineaSoporteReaseguro.getLineaSoporteCoberturaDTOs() != null){
					LineaSoporteCoberturaDTOlist.addAll(lineaSoporteReaseguro.getLineaSoporteCoberturaDTOs());
//					for(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : lineaSoporteReaseguro.getLineaSoporteCoberturaDTOs()){
//					   LineaSoporteCoberturaDTOlist.add(lineaSoporteCoberturaDTO); 
//					}
				}
			}
				 
			if (lineaSoporteReaseguroDTOs.size() > 0){
			      for(LineaSoporteReaseguroDTO lineaSoporteReaseguro : lineaSoporteReaseguroDTOs){
			    	 LineaSoporteReaseguroId lineaSoperteReaseguroId = new LineaSoporteReaseguroId();
			    	 LineaSoporteReaseguroDTO lineaSoporteReaseguroAux = null;
			    	 lineaSoperteReaseguroId.setIdToSoporteReaseguro(idToSoporteReaseguroDTO);
			     	 if(lineaSoporteReaseguro.getId() != null){
			    	   lineaSoperteReaseguroId.setIdTmLineaSoporteReaseguro(lineaSoporteReaseguro.getId().getIdTmLineaSoporteReaseguro());
			    	   lineaSoporteReaseguroAux = encontrarLineaSoporteReaseguro(lineaSoporteReaseguro.getId().getIdTmLineaSoporteReaseguro());
			    	 }
			    	 lineaSoporteReaseguro.setId(lineaSoperteReaseguroId);
				 	 lineaSoporteReaseguro.getLineaSoporteCoberturaDTOs().clear();
			    	 if(lineaSoporteReaseguroAux != null){
			    	    lineaSoporteReaseguro.setId(lineaSoporteReaseguroAux.getId());
			    	    entityManager.merge(lineaSoporteReaseguro); 
			    	 }else{
			    		 entityManager.persist(lineaSoporteReaseguro);
			    	 }
			       }
			} 
			//guardando  cobertura
			for(LineaSoporteReaseguroDTO lineParaCobertura : lineaSoporteReaseguroDTOs){
				  for (LineaSoporteCoberturaDTO   lineaSoporteCoberturaDTO  : LineaSoporteCoberturaDTOlist){
					   LineaSoporteCoberturaDTO lineaSoporteCoberturaDTOAux = new LineaSoporteCoberturaDTO();
					   lineaSoporteCoberturaDTOAux = lineaSoporteCoberturaDTO;
					   LineaSoporteCoberturaDTOId lineaSoporteCoberturaDTOId = new LineaSoporteCoberturaDTOId();
				       lineaSoporteCoberturaDTOId.setIdTmLineaSoporteReaseguro(lineParaCobertura.getId().getIdTmLineaSoporteReaseguro());
				       lineaSoporteCoberturaDTOId.setIdToSoporteReaseguro(lineParaCobertura.getId().getIdToSoporteReaseguro());
				       lineaSoporteCoberturaDTOAux.setId(lineaSoporteCoberturaDTOId);
				       lineaSoporteCoberturaDTOAux.setFechaCreacion(new Date());
				       lineaSoporteCoberturaDTOAux.setFechaModificacion(new Date());
				       entityManager.persist(lineaSoporteCoberturaDTOAux);
			  	}  
			}
			
			entityManager.flush();
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	private LineaSoporteReaseguroDTO encontrarLineaSoporteReaseguro(BigDecimal idTmLineaSoporteReaseguro){
		LineaSoporteReaseguroDTO lineaSoporteReaseguro = null;
		String queryString = "select model from LineaSoporteReaseguroDTO model where model.id.idTmLineaSoporteReaseguro = :idTmLineaSoporteReaseguro";		
	    Query query = entityManager.createQuery(queryString);
	    query.setParameter("idTmLineaSoporteReaseguro", idTmLineaSoporteReaseguro);
	    List listaResultado = query.getResultList();
	    if(!listaResultado.isEmpty()){//if(query.getResultList().size()> 0){
	       lineaSoporteReaseguro = (LineaSoporteReaseguroDTO) listaResultado.get(0);
	    }
	    return lineaSoporteReaseguro;
	}

  	/**
	 * Delete a persistent SoporteReaseguroDTO entity.
	 * 
	 * @param entity
	 *            SoporteReaseguroDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SoporteReaseguroDTO entity) {
		LogDeMidasEJB3.log("deleting SoporteReaseguroDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SoporteReaseguroDTO.class,
					entity.getIdToSoporteReaseguro());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SoporteReaseguroDTO entity and return it or a
	 * copy of it to the sender. A copy of the SoporteReaseguroDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SoporteReaseguroDTO entity to update
	 * @return SoporteReaseguroDTO the persisted SoporteReaseguroDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SoporteReaseguroDTO update(SoporteReaseguroDTO entity) {
		entity.setFechaModificacion(new Date());
		LogDeMidasEJB3.log("updating SoporteReaseguroDTO instance", Level.INFO, null);
		try {
			SoporteReaseguroDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SoporteReaseguroDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SoporteReaseguroDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SoporteReaseguroDTO instance = entityManager.find(
					SoporteReaseguroDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SoporteReaseguroDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SoporteReaseguroDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SoporteReaseguroDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SoporteReaseguroDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SoporteReaseguroDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			
			final String queryString = "select model from SoporteReaseguroDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SoporteReaseguroDTO entities.
	 * 
	 * @return List<SoporteReaseguroDTO> all SoporteReaseguroDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SoporteReaseguroDTO> findAll() {
		LogDeMidasEJB3.log("finding all SoporteReaseguroDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from SoporteReaseguroDTO model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Retorna el tipo de Dsitribucion utilizada en la linea que se encuentra entre la vigencia de la linea y
	 * que pertenezca al subramo indicado.
	 * 
	 * @return int TipoDistribucion
	 */	
	@SuppressWarnings("unchecked")
	public LineaDTO getLineaDTO(Date fechaInicioVigencia, BigDecimal idSubramo,BigDecimal estatus){
		LogDeMidasEJB3.log("Encuentra la Instancia de la LineaDTO. fechaInicioVigencia: "+fechaInicioVigencia+", idSubRamo: "+idSubramo, Level.INFO,
				null);
		try {
			String queryString = "select linea from LineaDTO linea where linea.subRamo.idTcSubRamo =:idSubramo " +
					(estatus != null ? "and linea.estatus = :estatus " : "") +
					" and :fechaInicioVigencia between linea.fechaInicial and linea.fechaFinal";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idSubramo", idSubramo);
			query.setParameter("fechaInicioVigencia", fechaInicioVigencia);
			if(estatus != null)
				query.setParameter("estatus", estatus);
			List listaResultado = query.getResultList();
			if (!listaResultado.isEmpty()){//if (query.getResultList().size()>0){
			     return (LineaDTO) listaResultado.get(0);
			}else{
				return null;
			}
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("get LineaDTO by Fecha Inicio Vigencia y IdSumbRamo failed", Level.SEVERE, re);
			throw re;
		}		
	

}
	
	public LineaDTO getLineaDTO(BigDecimal idTmLinea){
		LogDeMidasEJB3.log("Encuentra la Instancia de la LineaDTO ", Level.INFO,
				null);
		try {
			final String queryString = "select linea from LineaDTO linea where linea.idTmLinea.idTmLinea =:idTmLinea ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idTmLinea", idTmLinea);
			return (LineaDTO) query.getSingleResult();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("get LineaDTO by idTmLinea failed", Level.SEVERE, re);
			throw re;
		}		

}	
//BigDecimal idToCotizacion
	public SoporteReaseguroDTO getPorIdCotizacion(BigDecimal idToCotizacion){
		List<SoporteReaseguroDTO> listaSoportesEncontrados = findByProperty("idToCotizacion",idToCotizacion);
 	 if (listaSoportesEncontrados.size()> 0){
		return listaSoportesEncontrados.get(0);
   	 }else{
		return null;
	 }
   }
	
	@SuppressWarnings("unchecked")
	public List<SoporteReaseguroDTO> obtenerSoporteReaseguroPorEstatusLineas(
			Integer estatusLineaSoporte) {
		try {
			Query query = entityManager.createNamedQuery(
			"listarSoporteReaseguroPorEstatusLineas");
			query.setParameter("estatusLineaSoporte", estatusLineaSoporte);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("obtenerSoporteReaseguroPorEstatusLineas by Estatus failed", 
					Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public SoporteReaseguroDTO obtenerSoporteReaseguroPorPolizaEndoso(BigDecimal idToPoliza, Integer numeroEndoso){

		try {
			final String queryString = "select distinct soporteReaseguroDTO from SoporteReaseguroDTO soporteReaseguroDTO " +
			"join soporteReaseguroDTO.lineaSoporteReaseguroDTOs lineaSoporteReaseguroDTO " +
			"join lineaSoporteReaseguroDTO.lineaSoporteCoberturaDTOs lineaSoporteCoberturaDTO " +
			"where soporteReaseguroDTO.idToPoliza = :idToPoliza" +
			" and " +
			"soporteReaseguroDTO.numeroEndoso = :numeroEndoso" +
			" and " +
			"soporteReaseguroDTO.fechaEmision is not null" ;
			entityManager.flush(); //TODO: Prueba
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToPoliza", idToPoliza);
			query.setParameter("numeroEndoso", numeroEndoso);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<SoporteReaseguroDTO> listaSoporteReaseguro = query.getResultList();
			if (listaSoporteReaseguro.size()>0){
				 SoporteReaseguroDTO soporteReaseguroDTO = listaSoporteReaseguro.get(0);
			     return soporteReaseguroDTO;
			}else{
				return null;
			}
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Obtener Soporte Reaseguro Por Poliza Endoso falló", Level.SEVERE, re);
			throw re;
		}		
	
	}
	
	public SoporteReaseguroDTO obtenerSoporteReaseguroPorPolizaEndoso(BigDecimal idToCotizacion, BigDecimal idToPoliza, Integer numeroEndoso){

		try {
			final String queryString = "select distinct soporteReaseguroDTO from SoporteReaseguroDTO soporteReaseguroDTO " +
			"join soporteReaseguroDTO.lineaSoporteReaseguroDTOs lineaSoporteReaseguroDTO " +
			"join lineaSoporteReaseguroDTO.lineaSoporteCoberturaDTOs lineaSoporteCoberturaDTO " +
			"where soporteReaseguroDTO.idToPoliza = :idToPoliza" +
			" and " +
			"soporteReaseguroDTO.numeroEndoso = :numeroEndoso" +
			" and " +
			"soporteReaseguroDTO.idToCotizacion = :idToCotizacion";
			entityManager.flush(); //TODO: Prueba
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToPoliza", idToPoliza);
			query.setParameter("numeroEndoso", numeroEndoso);
			query.setParameter("idToCotizacion", idToCotizacion);
			if (query.getResultList().size()>0){
				 SoporteReaseguroDTO soporteReaseguroDTO = (SoporteReaseguroDTO) query.getSingleResult();
				 entityManager.refresh(soporteReaseguroDTO);
			     return soporteReaseguroDTO;
			}else{
				return null;
			}
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Obtener Soporte Reaseguro Por Poliza Endoso falló", Level.SEVERE, re);
			throw re;
		}		
	
	}
	
	
	public boolean notificarEmisionEndoso(SoporteReaseguroDTO soporteReaseguroOriginal, BigDecimal idToPoliza,int numeroEndoso,
			BigDecimal idToCotizacionEndoso,Date fechaEmision,Date fechaInicioVigencia,BigDecimal factorPrima,short tipoEndoso,List<String> listaErrores,boolean omitirErrorSoporteExistente) {
		boolean resultado = true;
		String operacion = (tipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION ? "Cancelacion" : tipoEndoso == EndosoDTO.TIPO_ENDOSO_REHABILITACION ? "Rehabilitacion" : tipoEndoso == EndosoDTO.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO ? "Cambio de forma de pago" : null);
		if(operacion == null)
			throw new RuntimeException("Se intentó notificar un tipo de endoso inválido: "+tipoEndoso+", utilice las constantes de EndosoDTO. ("+EndosoDTO.TIPO_ENDOSO_CANCELACION+" Cancelacion, "+EndosoDTO.TIPO_ENDOSO_REHABILITACION+" Rehabilitacion, "+EndosoDTO.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO+" Cambio forma de pago)");
		try{
			if(soporteReaseguroOriginal != null){
				if(soporteReaseguroOriginal.getFechaEmision() != null){
					SoporteReaseguroDTO soporteExistente = obtenerSoporteReaseguroPorPolizaEndoso(idToPoliza, numeroEndoso);
					if(soporteExistente == null){
						//Se genera el soporte
						SoporteReaseguroDTO soporteReaseguroEndoso = new SoporteReaseguroDTO(null, idToCotizacionEndoso, Calendar.getInstance().getTime(), Calendar.getInstance().getTime());
						soporteReaseguroEndoso.setFechaEmision(fechaEmision);
						soporteReaseguroEndoso.setIdToPoliza(idToPoliza);
						soporteReaseguroEndoso.setNumeroEndoso(numeroEndoso);
//						soporteReaseguroEndoso.setSoporteReaseguroDTOEndosoAnterior(soporteReaseguroOriginal);
						soporteReaseguroEndoso.setIdToSoporteReaseguroEndosoAnterior(soporteReaseguroOriginal.getIdToSoporteReaseguro());
						soporteReaseguroEndoso = this.save(soporteReaseguroEndoso);
						entityManager.flush();
						if(soporteReaseguroEndoso.getIdToSoporteReaseguro() == null){
							throw new RuntimeException("Ocurrio un error al guardar el soporteReaseguro");
						}
						//Se generan las lineas del soporte
						if(soporteReaseguroOriginal.getLineaSoporteReaseguroDTOs() == null || soporteReaseguroOriginal.getLineaSoporteReaseguroDTOs().isEmpty()){
							LineaSoporteReaseguroDTO lineaTMP = new LineaSoporteReaseguroDTO();
							lineaTMP.setId(new LineaSoporteReaseguroId());
							lineaTMP.getId().setIdToSoporteReaseguro(soporteReaseguroEndoso.getIdToSoporteReaseguro());
							soporteReaseguroOriginal.setLineaSoporteReaseguroDTOs(lineaSoporteReaseguroFacade.listarFiltrado(lineaTMP));
						}
						for(LineaSoporteReaseguroDTO lineaSoporteOriginal : soporteReaseguroOriginal.getLineaSoporteReaseguroDTOs()){
							LineaSoporteReaseguroDTO lineaSoporteEndoso = new LineaSoporteReaseguroDTO();
							lineaSoporteEndoso.setId(new LineaSoporteReaseguroId());
							lineaSoporteEndoso.getId().setIdToSoporteReaseguro(soporteReaseguroEndoso.getIdToSoporteReaseguro());
							lineaSoporteEndoso.setEsPrimerRiesgo(lineaSoporteOriginal.getEsPrimerRiesgo());
							lineaSoporteEndoso.setEstatusFacultativo(lineaSoporteOriginal.getEstatusFacultativo());
							lineaSoporteEndoso.setEstatusLineaSoporte(LineaSoporteReaseguroDTO.EMITIDO_SIN_DISTRIBUIR);
							lineaSoporteEndoso.setFechaCreacion(Calendar.getInstance().getTime());
							lineaSoporteEndoso.setFechaEmision(fechaEmision);
							lineaSoporteEndoso.setFechaInicioVigencia(fechaInicioVigencia);
							lineaSoporteEndoso.setFechaModificacion(Calendar.getInstance().getTime());
							lineaSoporteEndoso.setIdMoneda(lineaSoporteOriginal.getIdMoneda());
							lineaSoporteEndoso.setIdToCotizacion(idToCotizacionEndoso);
							lineaSoporteEndoso.setIdToPoliza(idToPoliza);
							lineaSoporteEndoso.setLineaDTO(lineaSoporteOriginal.getLineaDTO());
							lineaSoporteEndoso.setMontoSumaAsegurada(lineaSoporteOriginal.getMontoSumaAsegurada());
							lineaSoporteEndoso.setNotaDelSistema("Emitido y no distribuido");				//Posible cambio
							lineaSoporteEndoso.setPorcentajeCuotaParte(lineaSoporteOriginal.getPorcentajeCuotaParte());//Posible cambio
							lineaSoporteEndoso.setPorcentajeFacultativo(lineaSoporteOriginal.getPorcentajeFacultativo());//Posible cambio
							lineaSoporteEndoso.setPorcentajePleno(lineaSoporteOriginal.getPorcentajePleno());
							lineaSoporteEndoso.setPorcentajePrimerExcedente(lineaSoporteOriginal.getPorcentajePrimerExcedente());
							lineaSoporteEndoso.setPorcentajeRetencion(lineaSoporteOriginal.getPorcentajeRetencion());
							lineaSoporteEndoso.setSoporteReaseguroDTO(soporteReaseguroEndoso);
							lineaSoporteEndoso.setNumeroEndoso((short)numeroEndoso);
							/*
							 * 14/01/2011 Se implementa corrección al cálculo de porcentajes. En los casos donde se mantiene se disminuye la SA, se deben
							 * aplicar los % globales de distribución, y no los del cálculo de distribución de prima.
							 */
							lineaSoporteEndoso.setDisPrimaPorcentajeCuotaParte(lineaSoporteOriginal.getPorcentajeCuotaParte());
							lineaSoporteEndoso.setDisPrimaPorcentajeFacultativo(lineaSoporteOriginal.getPorcentajeFacultativo());
							lineaSoporteEndoso.setDisPrimaPorcentajePrimerExcedente(lineaSoporteOriginal.getPorcentajePrimerExcedente());
							lineaSoporteEndoso.setDisPrimaPorcentajeRetencion(lineaSoporteOriginal.getPorcentajeRetencion());
							
							if(tipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION || tipoEndoso == EndosoDTO.TIPO_ENDOSO_REHABILITACION){
								lineaSoporteEndoso.setAplicaDistribucionPrima(LineaSoporteReaseguroDTO.APLICA_DISTRIBUCION_PRIMA);
							} else{//endoso de cambio de forma de pago
								lineaSoporteEndoso.setAplicaDistribucionPrima(LineaSoporteReaseguroDTO.NO_APLICA_DISTRIBUCION_PRIMA);
							}
							/*
							 * 10708/2010. Jose Luis Arellano
							 * Se actualizó la lógica para generar la copia del soporte: las lineas del nuevo soporte deben usar el tipo de cambio del día, en caso de cancelacion,
							 * la SA acumulada es 0, en caso de rehabilitación, la SA acumulada es la misma del endoso, pero usando el tipo de cambio actual
							 */
							lineaSoporteEndoso.setTipoCambio(lineaSoporteOriginal.getTipoCambio());
							if(tipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION){
								lineaSoporteEndoso.setMontoSumaAseguradaAcumulada(BigDecimal.ZERO);
							}else if (tipoEndoso == EndosoDTO.TIPO_ENDOSO_REHABILITACION){//es rehabilitacion
								if(lineaSoporteOriginal.getIdMoneda().intValue() == MonedaDTO.MONEDA_PESOS){
									lineaSoporteEndoso.setMontoSumaAseguradaAcumulada(lineaSoporteOriginal.getMontoSumaAsegurada().divide(new BigDecimal(lineaSoporteOriginal.getTipoCambio()),RoundingMode.HALF_UP));
								} else{
									lineaSoporteEndoso.setMontoSumaAseguradaAcumulada(lineaSoporteOriginal.getMontoSumaAsegurada());
								}
							}else{
								lineaSoporteEndoso.setMontoSumaAseguradaAcumulada(lineaSoporteOriginal.getMontoSumaAseguradaAcumulada());
							}
							/*
							 * 26/05/2010. Jose Luis Arellano
							 * Se actualizó la lógica para generar la copia del soporte: las lineas del nuevo soporte deben apuntar a los contratos
							 * del endoso anterior (para ambos casos).
							 */
							/*
							 * Modificacion realmente ahora lo que se necesita es que para cada linea se le debe crear un nuevo slip-contrato.
							 */
							lineaSoporteEndoso = lineaSoporteReaseguroFacade.save(lineaSoporteEndoso);
							lineaSoporteEndoso = entityManager.merge(lineaSoporteEndoso);
									
							//Se generan las coberturas de la linea
							if(lineaSoporteOriginal.getLineaSoporteCoberturaDTOs() == null || lineaSoporteOriginal.getLineaSoporteCoberturaDTOs().isEmpty()){
								lineaSoporteOriginal.setLineaSoporteCoberturaDTOs(lineaSoporteCoberturaFacade.findByProperty("id.idTmLineaSoporteReaseguro", lineaSoporteOriginal.getId().getIdTmLineaSoporteReaseguro()));
							}
							for(LineaSoporteCoberturaDTO coberturaOriginal : lineaSoporteOriginal.getLineaSoporteCoberturaDTOs()){
								LineaSoporteCoberturaDTO coberturaEndoso = new LineaSoporteCoberturaDTO(new LineaSoporteCoberturaDTOId(), 
										lineaSoporteEndoso, coberturaOriginal.getMontoPrimaSuscripcion(), coberturaOriginal.getIdMoneda(), Calendar.getInstance().getTime(), Calendar.getInstance().getTime());
								coberturaEndoso.getId().setIdTmLineaSoporteReaseguro(lineaSoporteEndoso.getId().getIdTmLineaSoporteReaseguro());
								coberturaEndoso.getId().setIdToSoporteReaseguro(soporteReaseguroEndoso.getIdToSoporteReaseguro());
								coberturaEndoso.setAplicaControlReclamo(coberturaOriginal.getAplicaControlReclamo());
								coberturaEndoso.setEsPrimerRiesgo(coberturaOriginal.getEsPrimerRiesgo());
								coberturaEndoso.setIdToCobertura(coberturaOriginal.getIdToCobertura());
								coberturaEndoso.setIdToPersonaAsegurado(coberturaOriginal.getIdToPersonaAsegurado());
								coberturaEndoso.setIdToPoliza(idToPoliza);
								coberturaEndoso.setIdToSeccion(coberturaOriginal.getIdToSeccion());
								/*
								 * 13/07/2010. Jose Luis Arellano. Se modificó el cálculo de las primas para considerar el nuevo campo "montoPrimaAdicional"
								 */
								if(tipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION){
									coberturaEndoso.setMontoPrimaSuscripcion(BigDecimal.ZERO);
									coberturaEndoso.setMontoPrimaFacultativo(BigDecimal.ZERO);
									coberturaEndoso.setMontoPrimaNoDevengada(coberturaOriginal.getMontoPrimaSuscripcion().multiply(factorPrima));
									coberturaEndoso.setMontoPrimaAdicional(coberturaOriginal.getMontoPrimaAdicional().multiply(factorPrima));
								}else if (tipoEndoso == EndosoDTO.TIPO_ENDOSO_REHABILITACION){//es rehabilitacion
									coberturaEndoso.setMontoPrimaSuscripcion(coberturaOriginal.getMontoPrimaNoDevengada().multiply(factorPrima));
									coberturaEndoso.setMontoPrimaFacultativo(coberturaEndoso.getMontoPrimaSuscripcion());
									coberturaEndoso.setMontoPrimaAdicional(coberturaOriginal.getMontoPrimaAdicional().multiply(factorPrima));
									coberturaEndoso.setMontoPrimaNoDevengada(BigDecimal.ZERO);
								}else if (tipoEndoso == EndosoDTO.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO){//endoso de cambio de forma de pago
									coberturaEndoso.setMontoPrimaSuscripcion(coberturaEndoso.getMontoPrimaSuscripcion().multiply(factorPrima));
									coberturaEndoso.setMontoPrimaFacultativo(coberturaEndoso.getMontoPrimaSuscripcion());
									coberturaEndoso.setMontoPrimaAdicional(coberturaOriginal.getMontoPrimaAdicional().multiply(factorPrima));
									coberturaEndoso.setMontoPrimaNoDevengada(coberturaEndoso.getMontoPrimaSuscripcion());
								}
								coberturaEndoso.setNotaDelSistema("Midas: emitido y no distribuido");
								coberturaEndoso.setNumeroInciso(coberturaOriginal.getNumeroInciso());
								coberturaEndoso.setNumeroSubInciso(coberturaOriginal.getNumeroSubInciso());
								coberturaEndoso.setPorcentajeCoaseguro(coberturaOriginal.getPorcentajeCoaseguro());
								coberturaEndoso.setPorcentajeDeducible(coberturaOriginal.getPorcentajeDeducible());
								entityManager.persist(coberturaEndoso);
								lineaSoporteEndoso.getLineaSoporteCoberturaDTOs().add(coberturaEndoso);
							}
							
							//Por ultimo se genera el contrato facultativo en caso de tenerlo.
							ContratoFacultativoDTO contratoFacultativoOriginal = lineaSoporteOriginal.getContratoFacultativoDTO();
							if (contratoFacultativoOriginal != null) {
								//Crear el slip-contrato
								lineaSoporteEndoso.setEstatusFacultativo(LineaSoporteReaseguroDTO.ENDOSO_FACULTATIVO);
								SlipDTO slipEndoso = slipFacadeRemote.llenarSlip(lineaSoporteEndoso);
								slipEndoso.setNumeroEndoso(numeroEndoso);
								slipEndoso = slipFacadeRemote.establecerTipoSlip(slipEndoso); //Establece el tipo de slip.
								entityManager.persist(slipEndoso);
								contratoFacultativoFacadeRemote
										.duplicarContratoFacultativo(contratoFacultativoOriginal.getIdTmContratoFacultativo(), 
												slipEndoso.getIdToSlip(), numeroEndoso);
							}
						}
					} else{
						if(soporteExistente.getLineaSoporteReaseguroDTOs() != null){
							for(LineaSoporteReaseguroDTO lineaSoporte: soporteExistente.getLineaSoporteReaseguroDTOs()){
								lineaSoporte.setEstatusFacultativo(LineaSoporteReaseguroDTO.EMITIDO_SIN_DISTRIBUIR);
								lineaSoporteReaseguroFacade.update(lineaSoporte);
							}
						}
						String error = "Se intentó hacer una notificacion de endoso de "+operacion+" para la poliza: "+idToPoliza+", endoso: "+numeroEndoso+
								", pero ya existe el soporteReaseguro: "+soporteExistente.getIdToSoporteReaseguro()+
								". Se actualizó el estatus de la(s) linea(s) ("+
								(soporteExistente.getLineaSoporteReaseguroDTOs() == null ? 0 : soporteExistente.getLineaSoporteReaseguroDTOs().size())
								+") registradas: Emitido y no distribuido: "+LineaSoporteReaseguroDTO.EMITIDO_SIN_DISTRIBUIR;
//						String error = "Se intentó hacer una notificacion de endoso de "+operacion+" para la poliza: "+idToPoliza+", endoso: "+numeroEndoso+
//						", pero ya existe el soporteReaseguro: "+soporteExistente.getIdToSoporteReaseguro();
						LogDeMidasEJB3.log(error, Level.WARNING, null);
						if(!omitirErrorSoporteExistente){
							resultado = false;
							if(listaErrores != null)	listaErrores.add(error);
						}
					}
				} else{
					resultado = false;
					String error = "Se intentó hacer una notificacion de endoso de "+operacion+" para la poliza: "+idToPoliza+", endoso: "+numeroEndoso+
							", pero el soporteReaseguroOriginal: "+soporteReaseguroOriginal.getIdToSoporteReaseguro()+" no ha sido notificado de emisión: fechaEmision = "+soporteReaseguroOriginal.getFechaEmision();
					LogDeMidasEJB3.log(error, Level.WARNING, null);
					if(listaErrores != null)	listaErrores.add(error);
				}
			} else{
				resultado = false;
				String error = "Se intentó hacer una notificacion de endoso de "+operacion+" para la poliza: "+idToPoliza+", endoso: "+numeroEndoso+
				", pero se recibió un soporteReaseguroOriginal nulo: "+soporteReaseguroOriginal;
				LogDeMidasEJB3.log(error, Level.WARNING, null);
				if(listaErrores != null)	listaErrores.add(error);
			}
		}catch(RuntimeException re){
			context.setRollbackOnly();
			String error = "Falló la creación del SoporteReaseguro para el endoso de "+operacion+" de la poliza: "+idToPoliza+", endoso: "+numeroEndoso+".";
			LogDeMidasEJB3.log(error, Level.SEVERE, re);
			if(listaErrores != null)	listaErrores.add(error);
			resultado = false;
		}
		return resultado;
	}
	
	public void aplicarFactorAjusteSoporteReaseguro(EndosoDTO endosoEmitido,BigDecimal factorAjuste) {
		if(endosoEmitido != null && endosoEmitido.getId() != null && factorAjuste != null){
			if(endosoEmitido.getClaveTipoEndoso() != null && 
					(endosoEmitido.getClaveTipoEndoso().shortValue() == EndosoDTO.TIPO_ENDOSO_CANCELACION_ENDOSO ||
							endosoEmitido.getClaveTipoEndoso().shortValue() == EndosoDTO.TIPO_ENDOSO_REHABILITACION_ENDOSO)){
				//Consulta del soporte del endoso
				SoporteReaseguroDTO soporteReaseguro = getPorIdCotizacion(endosoEmitido.getIdToCotizacion());
				factorAjuste = factorAjuste.abs();
				if(soporteReaseguro != null){
					MathContext mathContext = new MathContext(12);
					for(LineaSoporteReaseguroDTO lineaSoporte : soporteReaseguro.getLineaSoporteReaseguroDTOs()){
						for(LineaSoporteCoberturaDTO coberturaSoporte : lineaSoporte.getLineaSoporteCoberturaDTOs()){
							if(coberturaSoporte.getMontoPrimaSuscripcion() != null && 
									coberturaSoporte.getMontoPrimaSuscripcion().compareTo(BigDecimal.ZERO) != 0){
								coberturaSoporte.setMontoPrimaSuscripcion(
										coberturaSoporte.getMontoPrimaSuscripcion().multiply(factorAjuste, mathContext));
							}
							if(coberturaSoporte.getMontoPrimaFacultativo() != null && 
									coberturaSoporte.getMontoPrimaFacultativo().compareTo(BigDecimal.ZERO) != 0){
								coberturaSoporte.setMontoPrimaFacultativo(
										coberturaSoporte.getMontoPrimaFacultativo().multiply(factorAjuste, mathContext));
							}
							if(coberturaSoporte.getMontoPrimaNoDevengada() != null && 
									coberturaSoporte.getMontoPrimaNoDevengada().compareTo(BigDecimal.ZERO) != 0){
								coberturaSoporte.setMontoPrimaNoDevengada(
										coberturaSoporte.getMontoPrimaNoDevengada().multiply(factorAjuste, mathContext));
							}
							entityManager.merge(coberturaSoporte);
						}
					}
				}
				else{
					LogDeMidasEJB3.log("Se intentó aplicar factor de ajuste al soporte reaseguro de un endoso de tipo: claveTipoEndoso: " +
							endosoEmitido.getClaveTipoEndoso()+", pero no se encontró el soporte para la cotización del endoso: idToCotizacion: "+endosoEmitido.getIdToCotizacion(), Level.WARNING, null);
				}
			}
			else{
				LogDeMidasEJB3.log("Se intentó aplicar factor de ajuste al soporte reaseguro de un endoso de tipo: claveTipoEndoso: " +
						endosoEmitido.getClaveTipoEndoso()+", pero sólo se permite la aplicación del factor en cancelaciones y rehabilitaciones de endoso.", Level.INFO, null);
			}
		}
		else{
			LogDeMidasEJB3.log("Se intentó aplicar factor de ajuste al soporte reaseguro de un endoso, pero se recibieron parámetros incorrectos. EndosoDTO:"+endosoEmitido, Level.WARNING, null);
		}
	}
	
	public double obtenerDiferenciaPrimaSoportePoliza(/*BigDecimal idToSoporteReaseguro,*/ BigDecimal idToPoliza,Integer numeroEndoso){
		try {
			LogDeMidasEJB3.log("Calculando diferencia entre primas del SoporteReaseguro y la poliza con id: "+idToPoliza+", numeroEndoso: estatus "+numeroEndoso, Level.INFO, null);
			//Obtener la prima neta del soporteReaseguro
			String queryString = "select TRUNC(SUM(LSopCob.MONTOPRIMASUSCRIPCION) - SUM(NVL(LSopCob.MONTOPRIMANODEVENGADA, 0)),4) PrimaEmitida " +
					"from MIDAS.TdLineaSopReaCobertura LSopCob, MIDAS.ToSoporteReaseguro SopRea where SopRea.IDTOPOLIZA = "+idToPoliza+" and SopRea.NUMEROENDOSO = "+numeroEndoso+" " +
					"and SopRea.FECHAEMISION is not null and SopRea.IDTOSOPORTEREASEGURO = LSopCob.IDTOSOPORTEREASEGURO";
			Query query;
			query = entityManager.createNativeQuery(queryString);

			Object result = query.getSingleResult();
			double primaSoporte = 0d;
			if(result instanceof BigDecimal)
				primaSoporte = ((BigDecimal)result).doubleValue();
			else if (result instanceof Long)
				primaSoporte = ((Long)result).doubleValue();
			else if (result instanceof Double)
				primaSoporte = ((Double)result).doubleValue();
			
			//Obtener la prima neta de la póliza:
			queryString = "select TRUNC(SUM(cobEnd.valorprimaNeta), 4) PrimaEmitida from MIDAS.tocoberturaend cobEnd where cobEnd.idtopoliza = " + idToPoliza + " " +
					"and cobEnd.numeroendoso = "+numeroEndoso;
			Query query2;
			query2 = entityManager.createNativeQuery(queryString);

			Object result2 = query2.getSingleResult();
			double primaPoliza = 0d;
			if(result2 instanceof BigDecimal)
				primaPoliza = ((BigDecimal)result2).doubleValue();
			else if (result2 instanceof Long)
				primaPoliza = ((Long)result2).doubleValue();
			else if (result2 instanceof Double)
				primaPoliza = ((Double)result2).doubleValue();
			double diferencia = primaPoliza - primaSoporte;
			LogDeMidasEJB3.log("Se calculó exitosamente la diferencia entre primas del SoporteReaseguro y la poliza con id: "+idToPoliza+", numeroEndoso: "+numeroEndoso+" diferencia: "+diferencia, Level.INFO, null);
			
			return diferencia<0 ? diferencia * -1 : diferencia;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Ocurrió un error al calcular la diferencia entre primas del SoporteReaseguro y la poliza con id: "+idToPoliza+", numeroEndoso: estatus "+numeroEndoso, Level.SEVERE, re);
			throw re;
		}
	}
	
	public SoporteReaseguroDTO notificarEmision(BigDecimal idToSoporteReaseguro, BigDecimal idToPoliza,int numeroEndoso,Date fechaEmision) {
		int estatusFacultativoLineaEmitida = LineaSoporteReaseguroDTO.ESTATUS_EMITIDA;
		int estatusLineaDistribuida = LineaSoporteReaseguroDTO.EMITIDO_Y_DISTRIBUIDO;
		int estatusLineaSoporte = LineaSoporteReaseguroDTO.EMITIDO_SIN_DISTRIBUIR;
		SoporteReaseguroDTO soporteReaseguroDTO = null;
		try{
			if(idToSoporteReaseguro != null && idToPoliza != null && fechaEmision != null){
				LogDeMidasEJB3.log("Inicia notificación de emision para el soporteReaseguro: "+idToSoporteReaseguro+", id de la poliza: "+idToPoliza+",numeroEndoso: "+numeroEndoso+", fechaEmision: "+fechaEmision, Level.INFO, null);
				//actualizar el soporte reaseguro
				String queryString = "UPDATE MIDAS.toSoporteReaseguro SET IDTOPOLIZA = "+idToPoliza +", numeroEndoso = "+numeroEndoso
						+", FECHAEMISION= TO_DATE('"+(new java.sql.Date(fechaEmision.getTime()).toString())+"','yyyy-MM-dd')" 
						+", FECHAMODIFICACION = TO_DATE('"+(new java.sql.Date(new Date().getTime()).toString())+"','yyyy-MM-dd')"
						+" where idToSoporteReaseguro = "+idToSoporteReaseguro;
				Query query;
				query = entityManager.createNativeQuery(queryString);
				query.executeUpdate();
				
				//actualizar las lineas del soporte
				lineaSoporteReaseguroFacade.notificarEmisionLineaSoporteReaseguro(idToSoporteReaseguro, idToPoliza,new Short(""+numeroEndoso), fechaEmision, estatusFacultativoLineaEmitida, estatusLineaSoporte,estatusLineaDistribuida);
				
				//actualizar las coberturas del soporte
				lineaSoporteCoberturaFacade.notificarEmisionCoberturasSoporteReaseguro(idToSoporteReaseguro, idToPoliza);
				
				soporteReaseguroDTO = entityManager.getReference(SoporteReaseguroDTO.class, idToSoporteReaseguro);
				entityManager.refresh(soporteReaseguroDTO);
				return soporteReaseguroDTO;
			}
		}catch(RuntimeException re){
			context.setRollbackOnly();
			String error = "Falló la notificación de emisión del SoporteReaseguro para la poliza: "+idToPoliza+", endoso: "+numeroEndoso+".";
			LogDeMidasEJB3.log(error, Level.SEVERE, re);
		}
		return soporteReaseguroDTO;
	}
	
	public List<String> validarSoporteReaseguroDTOListoParaDistribucion(BigDecimal idToSoporteReaseguro) {
		SoporteReaseguroDTO soporteReaseguroDTO = soporteReaseguroFacadeRemote.findById(idToSoporteReaseguro);
		
		if (soporteReaseguroDTO == null) {
        	throw new IllegalArgumentException("No se pudo encontrar el SoporteReaseguroDTO para el idToSoporteReaseguro: "+idToSoporteReaseguro);
        }
		
        List<String> listaErrores = validarSoporteReaseguroDTOListoParaDistribucion(soporteReaseguroDTO);
        
        return listaErrores;
	}
	
	public List<String> validarSoporteReaseguroDTOListoParaDistribucion(BigDecimal idToPoliza, int numeroEndoso) {
        //Buscar SoporteReaseguroDTO
        SoporteReaseguroDTO soporteReaseguroDTO = soporteReaseguroFacadeRemote.obtenerSoporteReaseguroPorPolizaEndoso(idToPoliza, numeroEndoso);
        
        if (soporteReaseguroDTO == null) {
        	throw new IllegalArgumentException("No se pudo encontrar el SoporteReaseguroDTO para el idToPoliza: "
        			+ idToPoliza + " y el numeroEndoso: " + numeroEndoso);
        }
		
        List<String> listaErrores = validarSoporteReaseguroDTOListoParaDistribucion(soporteReaseguroDTO);
        
        return listaErrores;
	}
	
	private List<String> validarSoporteReaseguroDTOListoParaDistribucion(SoporteReaseguroDTO soporteReaseguroDTO){
		boolean validarPorcentajesDistribucion = false;
        boolean porcentajesGeneralesLineaNulos = false;
        boolean porcentajesDistribucionLineaNulos = false;
        boolean porcentajesLineasValido = true;
        boolean porcentajesDistribucionLineaValido = true;
		boolean configuracionContratoCP = true;
		boolean configuracionContrato1E = true;
		boolean configuracionContratoF = true;
		boolean primaSoporteConsistente = true;
		boolean totalCoberturasPrimasIgualadas = true;
		boolean totalCoberturasSoportadas = true;
		boolean primaCorrectaPorCobertura = true;
		boolean cantidadIgualesCoberturasDaniosReaseguro = true;
		boolean soporteValido = true;
		
        List<String> listaErrores = new ArrayList<String>();
		double diferenciaPrimas=0;
        String detalleError = "";
        
        for(LineaSoporteReaseguroDTO lsrDTO : soporteReaseguroDTO.getLineaSoporteReaseguroDTOs()){
        	porcentajesGeneralesLineaNulos = !(lsrDTO.getPorcentajeCuotaParte() != null && lsrDTO.getPorcentajeFacultativo() != null && 
        			lsrDTO.getPorcentajePrimerExcedente()!=null && lsrDTO.getPorcentajeRetencion() != null);
        	if(porcentajesGeneralesLineaNulos){
        		break;
        	}
        	porcentajesDistribucionLineaNulos = !(lsrDTO.getDisPrimaPorcentajeCuotaParte() != null && lsrDTO.getDisPrimaPorcentajeFacultativo() != null && 
        			lsrDTO.getDisPrimaPorcentajePrimerExcedente()!=null && lsrDTO.getDisPrimaPorcentajeRetencion() != null);
        	if(porcentajesDistribucionLineaNulos){
        		break;
        	}
        	double sumaPorcentajes = lsrDTO.getPorcentajeCuotaParte().add(lsrDTO.getPorcentajeFacultativo()).add(lsrDTO.getPorcentajePrimerExcedente()).add(lsrDTO.getPorcentajeRetencion()).doubleValue();
        	if(sumaPorcentajes != 100d){
        		porcentajesLineasValido = false;
        		break;
        	}
        	validarPorcentajesDistribucion = (lsrDTO.getAplicaDistribucionPrima() == null || lsrDTO.getAplicaDistribucionPrima().intValue() == LineaSoporteReaseguroDTO.APLICA_DISTRIBUCION_PRIMA);
        	double sumaPorcentajesDisPrim = lsrDTO.getDisPrimaPorcentajeCuotaParte().add(lsrDTO.getDisPrimaPorcentajeFacultativo()).add(
        			lsrDTO.getDisPrimaPorcentajePrimerExcedente()).add(lsrDTO.getDisPrimaPorcentajeRetencion()).doubleValue();
        	//Si la linea no tiene prima por distribuir, se permiten porcentajes de distribucion DE PRIMA en ceros.
        	if(sumaPorcentajesDisPrim != 100d && validarPorcentajesDistribucion){
        		porcentajesDistribucionLineaValido = false;
        		break;
        	}
        	if(lsrDTO.getPorcentajeCuotaParte().compareTo(BigDecimal.ZERO) > 0 && lsrDTO.getLineaDTO().getContratoCuotaParte() == null){
        		configuracionContratoCP = false;
        		break;
        	}
        	if(lsrDTO.getPorcentajePrimerExcedente().compareTo(BigDecimal.ZERO) > 0 && lsrDTO.getLineaDTO().getContratoPrimerExcedente() == null){
        		configuracionContrato1E = false;
        		break;
        	}
        	if(lsrDTO.getPorcentajeFacultativo().compareTo(BigDecimal.ZERO) > 0 && lsrDTO.getContratoFacultativoDTO() == null){
        		configuracionContratoF = false;
        		break;
        	}
        }//Fin iteración lista lineaSoporteReaseguro
        
        if( !porcentajesGeneralesLineaNulos && !porcentajesDistribucionLineaNulos && porcentajesDistribucionLineaValido &&
        		porcentajesLineasValido && configuracionContrato1E && configuracionContratoCP && configuracionContratoF){
        	String queryString = null;
        	Query query = null;
        	// Comparar que las primas sean iguales, de haber diferencia no se distribuira
            diferenciaPrimas = soporteReaseguroFacadeRemote.obtenerDiferenciaPrimaSoportePoliza(soporteReaseguroDTO.getIdToPoliza(), soporteReaseguroDTO.getNumeroEndoso());
            queryString = "select model.claveTipoEndoso from EndosoDTO model where model.id.idToPoliza = :idToPoliza"+
            	" and model.id.numeroEndoso = :numeroEndoso ";
			query = entityManager.createQuery(queryString);
			query.setParameter("numeroEndoso", soporteReaseguroDTO.getNumeroEndoso());
			query.setParameter("idToPoliza", soporteReaseguroDTO.getIdToPoliza());
			Integer claveTipoEndoso = Utilerias.obtenerInteger(query.getSingleResult());
            if (diferenciaPrimas > 1d)  {
            	if(claveTipoEndoso != EndosoDTO.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO){
            		primaSoporteConsistente = false;
            	}
            	else{
            		LogDeMidasEJB3.log("Se encontró diferencia de prima entre el soporte de reaseguro y la emisión, pero esto es permitido en los endosos de cambio de forma de pago." +
            				"idToPoliza: "+soporteReaseguroDTO.getIdToPoliza()+", numeroEndoso: "+soporteReaseguroDTO.getNumeroEndoso(), Level.INFO, null);
            	}
            }
            //Sin importar la consistencia de prima total, se valida cada cobertura
            if(claveTipoEndoso != EndosoDTO.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO){
            	int cantidadCoberturasConPrimaEnSoporte = 0;
                Map<mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoId, LineaSoporteCoberturaDTO> mapaCoberturasSubIncisos = new HashMap<mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoId, LineaSoporteCoberturaDTO>();
                for(LineaSoporteReaseguroDTO lineaSoporteTMP : soporteReaseguroDTO.getLineaSoporteReaseguroDTOs()){
                	for(LineaSoporteCoberturaDTO lineaCoberturaTMP : lineaSoporteTMP.getLineaSoporteCoberturaDTOs()){
                		BigDecimal numeroInciso = lineaCoberturaTMP.getNumeroInciso();
                		BigDecimal numeroSubInciso = (lineaCoberturaTMP.getNumeroSubInciso() != null ? lineaCoberturaTMP.getNumeroSubInciso() : BigDecimal.ZERO);
                		BigDecimal idToSeccion = lineaCoberturaTMP.getIdToSeccion();
                		BigDecimal idToCobertura = lineaCoberturaTMP.getIdToCobertura();
                		BigDecimal montoPrimaSuscripcion = lineaCoberturaTMP.getMontoPrimaSuscripcion();
                		BigDecimal montoPrimaFacultativo = lineaCoberturaTMP.getMontoPrimaFacultativo();
                		BigDecimal montoPrimaNoDevengada = (lineaCoberturaTMP.getMontoPrimaNoDevengada() != null ? lineaCoberturaTMP.getMontoPrimaNoDevengada() : BigDecimal.ZERO);
                		
                		//1. Se valida que cada cobertura tenga los campos "montoPrimaSuscripcion" y "montoPrimaFacultativo" iguales.
                		BigDecimal diferenciaSuscripcionFacultativo = montoPrimaSuscripcion.subtract(montoPrimaFacultativo,new MathContext(10, RoundingMode.HALF_UP)).abs();
                		if(diferenciaSuscripcionFacultativo.compareTo(BigDecimal.ONE) >= 0 && lineaSoporteTMP.getDisPrimaPorcentajeFacultativo().compareTo(BigDecimal.ZERO) != 0){
                			//error detectado
                			totalCoberturasPrimasIgualadas = false;
                			detalleError = "La cobertura "+idToCobertura+", seccion "+idToSeccion+", inciso "+numeroInciso+", subinciso "+numeroSubInciso+" tiene " +
                					""+montoPrimaFacultativo+" de prima facultativa y "+montoPrimaSuscripcion+" de prima suscripción, hay una diferencia de "+diferenciaSuscripcionFacultativo;
                			break;
                		}
                		
                		//2. Se valida que la cobertura exista en la emisión, en caso de que tenga prima por distribuir.
                		if(montoPrimaSuscripcion.compareTo(montoPrimaNoDevengada) != 0){// si la cobertura tiene prima
                			if(numeroSubInciso.compareTo(BigDecimal.ZERO) == 0){
                				boolean[] resultadoValidacionesCobertura = new boolean[2];
                				detalleError = validarCobertruaDanosReaseguro(soporteReaseguroDTO.getIdToPoliza(), soporteReaseguroDTO.getNumeroEndoso(), numeroInciso, idToSeccion, idToCobertura, montoPrimaSuscripcion, montoPrimaNoDevengada, resultadoValidacionesCobertura);
                				primaCorrectaPorCobertura = resultadoValidacionesCobertura[0];
                				totalCoberturasSoportadas = resultadoValidacionesCobertura[1];
                				if(!primaCorrectaPorCobertura || !totalCoberturasSoportadas)
                					break;
                				cantidadCoberturasConPrimaEnSoporte += 1;
                			}
                			else{//la cobertura es a nivel subinciso
                				//Se genera una cobertura donde se agruparán todos los subincisos de la misma cobertura,
                				mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoId llaveCobertura = new mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoId(soporteReaseguroDTO.getIdToPoliza(), soporteReaseguroDTO.getNumeroEndoso().shortValue(), numeroInciso, idToSeccion, idToCobertura);
                				LineaSoporteCoberturaDTO lineaCoberturaSubIncisos = null;
                				if(mapaCoberturasSubIncisos.containsKey(llaveCobertura)){
                					lineaCoberturaSubIncisos = mapaCoberturasSubIncisos.get(llaveCobertura);
                					lineaCoberturaSubIncisos.setMontoPrimaFacultativo(
                							lineaCoberturaSubIncisos.getMontoPrimaFacultativo().add(montoPrimaFacultativo));
                					lineaCoberturaSubIncisos.setMontoPrimaSuscripcion(
                							lineaCoberturaSubIncisos.getMontoPrimaSuscripcion().add(montoPrimaSuscripcion));
                					lineaCoberturaSubIncisos.setMontoPrimaNoDevengada(
                							lineaCoberturaSubIncisos.getMontoPrimaNoDevengada().add(montoPrimaNoDevengada));
                				}
                				else{
                					lineaCoberturaSubIncisos = new LineaSoporteCoberturaDTO();
                					lineaCoberturaSubIncisos.setNumeroInciso(numeroInciso);
                					lineaCoberturaSubIncisos.setIdToCobertura(idToCobertura);
                					lineaCoberturaSubIncisos.setIdToSeccion(idToSeccion);
                					lineaCoberturaSubIncisos.setMontoPrimaFacultativo(montoPrimaFacultativo);
                					lineaCoberturaSubIncisos.setMontoPrimaSuscripcion(montoPrimaSuscripcion);
                					lineaCoberturaSubIncisos.setMontoPrimaNoDevengada(montoPrimaNoDevengada);
                					
                					mapaCoberturasSubIncisos.put(llaveCobertura, lineaCoberturaSubIncisos);
                				}
                			}
                		}
                	}//fin iteracion coberturas
                	if(!totalCoberturasPrimasIgualadas || !totalCoberturasSoportadas || !primaCorrectaPorCobertura){
                		break;
                	}
                }//fin iteracion lineas
                
                if(totalCoberturasPrimasIgualadas && totalCoberturasSoportadas && primaCorrectaPorCobertura){
                	//si todo es correcto se validan las coberturas a nivel subinciso
                	if(!mapaCoberturasSubIncisos.isEmpty()){
                		for(LineaSoporteCoberturaDTO lineaCobertura : mapaCoberturasSubIncisos.values()){
                			boolean[] resultadoValidacionesCobertura = new boolean[2];
                			BigDecimal primaEmitidaReaseguro = BigDecimal.ZERO;
                			if(lineaCobertura.getMontoPrimaSuscripcion() != null){
                				primaEmitidaReaseguro = lineaCobertura.getMontoPrimaSuscripcion();
                				if(lineaCobertura.getMontoPrimaNoDevengada() != null)
                					primaEmitidaReaseguro = primaEmitidaReaseguro.subtract(
                							lineaCobertura.getMontoPrimaNoDevengada(),new MathContext(10, RoundingMode.HALF_UP));
                			}
                			if(primaEmitidaReaseguro.compareTo(BigDecimal.ZERO) != 0){
	            				
                				detalleError = validarCobertruaDanosReaseguro(soporteReaseguroDTO.getIdToPoliza(), soporteReaseguroDTO.getNumeroEndoso(), 
	            						lineaCobertura.getNumeroInciso(), lineaCobertura.getIdToSeccion(), lineaCobertura.getIdToCobertura(), lineaCobertura.getMontoPrimaSuscripcion(), 
	            						lineaCobertura.getMontoPrimaNoDevengada(), resultadoValidacionesCobertura);
	            				primaCorrectaPorCobertura = resultadoValidacionesCobertura[0];
	            				totalCoberturasSoportadas = resultadoValidacionesCobertura[1];
	            				if(!primaCorrectaPorCobertura || !totalCoberturasSoportadas)
	            					break;
	            				cantidadCoberturasConPrimaEnSoporte += 1;
                			}
                		}
                	}
                	
                	if(totalCoberturasPrimasIgualadas && totalCoberturasSoportadas && primaCorrectaPorCobertura){
                		//Por último se valida la cantidad de coberturas con prima en reaseguro contra la cantidad de coberturas
                		queryString = "select count(0) from MIDAS.tocoberturaend where idtopoliza= %s and numeroendoso = %s and valorprimaneta <> 0";
                		queryString = String.format(queryString, soporteReaseguroDTO.getIdToPoliza(),soporteReaseguroDTO.getNumeroEndoso());
                		query = entityManager.createNativeQuery(queryString);
                		Integer cantidadCoberturasConPrimaEnEndoso = Utilerias.obtenerInteger(query.getSingleResult());
                		if(cantidadCoberturasConPrimaEnEndoso.intValue() != cantidadCoberturasConPrimaEnSoporte){
                			cantidadIgualesCoberturasDaniosReaseguro = false;
                			detalleError = "La cantidad de coberturas registradas en daños es diferente a las registradas en reaseguro. " +
                					"Coberturas con prima en daños: "+cantidadCoberturasConPrimaEnEndoso+", Coberturas con prima en Reaseguro: " +
                					cantidadCoberturasConPrimaEnSoporte;
                		}
                	}
            	}
            }
            
        }
        if (!(!porcentajesGeneralesLineaNulos && !porcentajesDistribucionLineaNulos && porcentajesDistribucionLineaValido && porcentajesLineasValido && configuracionContrato1E && 
        		configuracionContratoCP && configuracionContratoF && primaSoporteConsistente && totalCoberturasPrimasIgualadas && totalCoberturasSoportadas && primaCorrectaPorCobertura && cantidadIgualesCoberturasDaniosReaseguro)){
        	//De ser inconsistente se genera una descripción del error.
        	if(porcentajesGeneralesLineaNulos){
        		detalleError = "Los porcentajes de distribución totales de las lineas del soporte son incorrectos, se registraron valores nulos.";
        	}else if(porcentajesDistribucionLineaNulos){
        		detalleError = "Los porcentajes de distribución de prima del endoso de las lineas del soporte son incorrectos, se registraron valores nulos.";
        	}else if(!porcentajesDistribucionLineaValido){
        		detalleError = "Los porcentajes de distribución de prima del endoso de las lineas del soporte son incorrectos, deben sumar 100.";
        	}else if(!porcentajesLineasValido){
        		detalleError = "Los porcentajes de las lineas del soporte es incorrecto, debe sumar 100.";
        	}else if(!configuracionContrato1E){
        		detalleError = "Se encontró un tmLineaSoporteReaseguro con porcentaje primer excedente > 0, pero el registro tmLinea no tiene un contrato 1E relacionado.";
        	}else if (!configuracionContratoCP){
        		detalleError = "Se encontró un tmLineaSoporteReaseguro con porcentaje cuota parte > 0, pero el registro tmLinea no tiene un contrato CP relacionado.";
        	}else if(!configuracionContratoF){
        		detalleError = "Se encontró un tmLineaSoporteReaseguro con porcentaje facultativo > 0, pero no tiene un contrato configurado.";
        	}else if(!cantidadIgualesCoberturasDaniosReaseguro){
        		//la variable detalleError ya trae el mensaje en este caso
        	}else if(primaSoporteConsistente){
        		detalleError = "La prima del soporte no coincide con la prima de la póliza. La diferencia de primas es "+diferenciaPrimas;
        	}
        	soporteValido = false;
        }
        if(!soporteValido && detalleError != null){
        	listaErrores.add(detalleError);
        }
        
        return listaErrores;
	}

	@SuppressWarnings("unchecked")
	private String validarCobertruaDanosReaseguro(BigDecimal idToPoliza,Integer numeroEndoso,BigDecimal numeroInciso,BigDecimal idToSeccion,BigDecimal idToCobertura,
			BigDecimal montoPrimaSuscripcion,BigDecimal montoPrimaNoDevengada,boolean[] arrayResultadoValidaciones){
		String detalleError = null;
		boolean primaCorrectaPorCobertura = true;
		boolean totalCoberturasSoportadas = true;
		//Buscar la cobertura
		String queryString = "select valorprimaneta from MIDAS.tocoberturaend where idtopoliza = %s and numeroendoso = %s and numeroinciso = %s and idtoseccion = %s and idtocobertura = %s";
		Query query = entityManager.createNativeQuery(String.format(queryString, idToPoliza,numeroEndoso,numeroInciso,idToSeccion,idToCobertura));
		List listaResultado = query.getResultList();
		if(listaResultado != null && !listaResultado.isEmpty()){
			if(listaResultado.size() == 1){
				//Dado que la cobertura existe, se valida la prima emitida contra la prima soportada por reaseguro
				BigDecimal primaEmitida = Utilerias.obtenerBigDecimal(listaResultado.get(0));
				BigDecimal primaSoportada = montoPrimaSuscripcion.subtract(montoPrimaNoDevengada,new MathContext(10, RoundingMode.HALF_UP));
				BigDecimal diferenciaDaniosReaseguro = primaEmitida.subtract(primaSoportada,new MathContext(10, RoundingMode.HALF_UP)).abs();
				if(diferenciaDaniosReaseguro.compareTo(new BigDecimal("0.09")) > 0){
					//error
					primaCorrectaPorCobertura = false;
					detalleError = "La cobertura "+idToCobertura+", seccion "+idToSeccion+", inciso "+numeroInciso+" tiene prima diferente en daños y reaseguro. " +
							"Prima Daños: "+primaEmitida+", Prima reaseguro: "+primaSoportada+", Diferencia: "+diferenciaDaniosReaseguro;
				}
			}
			else{
				//error
				totalCoberturasSoportadas = false;
				detalleError = "La cobertura "+idToCobertura+", seccion "+idToSeccion+", inciso "+numeroInciso+" está registrada más de una vez en la estructura del endoso (ToCoberturaEnd).";
			}
		}
		else{
			//error
			totalCoberturasSoportadas = false;
			detalleError = "La cobertura "+idToCobertura+", seccion "+idToSeccion+", inciso "+numeroInciso+" no fué encontrada en la estructura del endoso (ToCoberturaEnd).";
		}
		
		arrayResultadoValidaciones[0] = primaCorrectaPorCobertura;
		arrayResultadoValidaciones[1] = totalCoberturasSoportadas;
		
		return detalleError;
	}
	@SuppressWarnings("unchecked")
	public List<BigDecimal> consultarSoportesPendientesDistribucion() {
		LogDeMidasEJB3.log("Buscando idToSoporteReaseguro que aún no han sido distribuidos (excluyendo endosos de rehabilitación, cancelación y cambio de forma de pago).", Level.INFO, null);
		try {
			final String queryString = "select distinct sop.idtosoportereaseguro from MIDAS.tosoportereaseguro sop, MIDAS.tmlineasoportereaseguro lin, MIDAS.toendoso endoso where " +
					"sop.idtosoportereaseguro = lin.idtosoportereaseguro and " +
					"sop.idtopoliza = endoso.idtopoliza and " +
					"sop.numeroendoso = endoso.numeroendoso and " +
					"endoso.clavetipoendoso not in (4/*cancelacion*/, 5/*Rehabilitacion*/,6/*Cambio forma dde pago*/) and " +
					"lin.estatuslineasoporte = "+LineaSoporteReaseguroDTO.EMITIDO_SIN_DISTRIBUIR;
			Query query = entityManager.createNativeQuery(queryString);
			List<BigDecimal> listaResultado = new ArrayList<BigDecimal>();
			List<Object> listaResultadoObj = query.getResultList();
			if(listaResultadoObj != null){
				for(Object obj : listaResultadoObj){
					listaResultado.add(Utilerias.obtenerBigDecimal(obj));
				}
				LogDeMidasEJB3.log("Cantidad de idToSoporteReaseguro que aún no han sido distribuidos (excluyendo endosos de rehabilitación, cancelación y cambio de forma de pago) encontrados: "+listaResultado.size(), Level.INFO, null);
			}
			return listaResultado;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Ocurrió un error al buscar idToSoporteReaseguro que aún no han sido distribuidos (excluyendo endosos de rehabilitación, cancelación y cambio de forma de pago).", Level.SEVERE, re);
			throw re;
		}
	}
	
	public boolean actualizarBanderaLineasAplicanDistribucion(BigDecimal idToSoporteReaseguro){
		boolean result = true;
		try{
			//Consulta del soporteReaseguro
			SoporteReaseguroDTO soporteReaseguroDTO = findById(idToSoporteReaseguro);
			for(LineaSoporteReaseguroDTO lineaEnCurso : soporteReaseguroDTO.getLineaSoporteReaseguroDTOs()){
				String formatoQueryString =  "select nvl(count(idtdlineasopreacobertura),0) from MIDAS.tdlineasopreacobertura where idtmlineasoportereaseguro = "+lineaEnCurso.getId().getIdTmLineaSoporteReaseguro()+
						"and (nvl(montoprimasuscripcion,0) - nvl(montoprimanodevengada,0)) <> 0";
				Query query = entityManager.createNativeQuery(formatoQueryString);
				Integer queryResult = Utilerias.obtenerInteger(query.getSingleResult());
				if(queryResult == null || queryResult.intValue() > 0){
					lineaEnCurso.setAplicaDistribucionPrima(LineaSoporteReaseguroDTO.APLICA_DISTRIBUCION_PRIMA);
				}
				else{
					lineaEnCurso.setAplicaDistribucionPrima(LineaSoporteReaseguroDTO.NO_APLICA_DISTRIBUCION_PRIMA);
				}
				entityManager.merge(lineaEnCurso);
			}
		}catch(Exception e){
			LogDeMidasEJB3.log("Ocurri� un error al actualizar la bandera AplicaDistribucion del soporteReaseguro: "+idToSoporteReaseguro, Level.SEVERE, e);
			result = false;
		}
		return result;
	}
	
	public BigDecimal calcularFactorAplicacionEndosoCancelacionRehabilitacion(BigDecimal idToPoliza,int numeroEndosoCanceladoRehabilitado,short claveTipoEndoso){
		BigDecimal factorActual = null, factorCorrecto = null;
		String tipoEndoso = (claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION ? "Cancelacion" : (claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_REHABILITACION ? "Rehabilitacion" : null));
		if(tipoEndoso != null){
			LogDeMidasEJB3.log("Calculando factor de aplicación en reaseguro para la póliza: idToPoliza: "+idToPoliza+", endoso: "+numeroEndosoCanceladoRehabilitado+", tipoEndoso: "+tipoEndoso, Level.INFO, null);
			String queryString = null;
			if(claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION){
				queryString = "SELECT ENDO.FACTORAPLICACION FACTOR_ACTUAL," +
				"       ROUND(ABS(ENDO.VALORPRIMANETA/(SELECT SUM(LSRC.MONTOPRIMASUSCRIPCION)" +
				"          FROM MIDAS.TDLINEASOPREACOBERTURA LSRC, MIDAS.TOSOPORTEREASEGURO SOP" +
				"          WHERE SOP.IDTOPOLIZA = ENDO.IDTOPOLIZA" +
				"          AND SOP.NUMEROENDOSO = ENDO.NUMEROENDOSO-1" +
				"         AND SOP.IDTOSOPORTEREASEGURO = LSRC.IDTOSOPORTEREASEGURO)),10) FACTOR_CORRECTO" +
				"     FROM MIDAS.TOENDOSO ENDO WHERE ENDO.IDTOPOLIZA = "+idToPoliza+" and ENDO.NUMEROENDOSO = "+numeroEndosoCanceladoRehabilitado;
			}
			else{
				queryString = "SELECT ENDO.FACTORAPLICACION FACTOR_ACTUAL," +
						"       ROUND(ABS(ENDO.VALORPRIMANETA/(SELECT SUM(LSRC.MONTOPRIMANODEVENGADA)" +
						"                            FROM MIDAS.TDLINEASOPREACOBERTURA LSRC, MIDAS.TOSOPORTEREASEGURO SOP" +
						"                            WHERE SOP.IDTOPOLIZA = ENDO.IDTOPOLIZA" +
						"                            AND SOP.NUMEROENDOSO = ENDO.NUMEROENDOSO-1" +
						"                            AND SOP.IDTOSOPORTEREASEGURO = LSRC.IDTOSOPORTEREASEGURO)),10) FACTOR_CORRECTO" +
						" FROM MIDAS.TOENDOSO ENDO WHERE ENDO.IDTOPOLIZA = "+idToPoliza+" and ENDO.NUMEROENDOSO = "+numeroEndosoCanceladoRehabilitado;
			}
			Query query = entityManager.createNativeQuery(queryString);
			Object result = query.getSingleResult();
			if(result != null){
				Object[] listaResultados = (Object[]) result;
				factorActual = Utilerias.obtenerBigDecimal(listaResultados[0]);
				factorCorrecto = Utilerias.obtenerBigDecimal(listaResultados[1]);
				if(factorActual.compareTo(factorCorrecto) == 0){
					LogDeMidasEJB3.log("Se cacluló el factor de aplicación de la póliza "+idToPoliza+", endoso "+numeroEndosoCanceladoRehabilitado+", tipoEndoso: "+tipoEndoso +"."+
							"El factor calculado ("+factorCorrecto+") es igual al factor registrado en el endoso ("+factorActual+").", Level.INFO, null);
				}
				else{
					LogDeMidasEJB3.log("Se cacluló el factor de aplicación de la póliza "+idToPoliza+", endoso "+numeroEndosoCanceladoRehabilitado+", tipoEndoso: "+tipoEndoso +"."+
							"El factor calculado ("+factorCorrecto+") es DIFERENTE al factor registrado en el endoso ("+factorActual+").", Level.INFO, null);
				}
			}
		}
		return factorCorrecto;
	}
	
	private SlipFacadeRemote slipFacadeRemote;
	private ContratoFacultativoFacadeRemote contratoFacultativoFacadeRemote;
	private SoporteReaseguroFacadeRemote soporteReaseguroFacadeRemote;
	
	@EJB
	public void setSlipFacadeRemote(SlipFacadeRemote slipFacadeRemote) {
		this.slipFacadeRemote = slipFacadeRemote;
	}
	
	@EJB
	public void setContratoFacultativoFacadeRemote(
			ContratoFacultativoFacadeRemote contratoFacultativoFacadeRemote) {
		this.contratoFacultativoFacadeRemote = contratoFacultativoFacadeRemote;
	}
	
	@EJB
	public void setSoporteReaseguroFacadeRemote(
			SoporteReaseguroFacadeRemote soporteReaseguroFacadeRemote) {
		this.soporteReaseguroFacadeRemote = soporteReaseguroFacadeRemote;
	}
	
	public LineaDTO obtenerLineaDTOVigente(Date fechaInicioVigencia, BigDecimal idSubramo){
		return getLineaDTO(fechaInicioVigencia, idSubramo,BigDecimal.ONE);
	}
	
	public LineaDTO obtenerLineaDTO(Date fechaInicioVigencia, BigDecimal idSubramo){
    	return getLineaDTO(fechaInicioVigencia, idSubramo,null);
	}
	
	public List<SoporteReaseguroDTO> listarPorIdToCotizacion(BigDecimal idToCotizacion){
		return findByProperty("idToCotizacion", idToCotizacion);
	}
}