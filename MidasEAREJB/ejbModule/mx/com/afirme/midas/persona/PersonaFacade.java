package mx.com.afirme.midas.persona;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity PersonaDTO.
 * @see .PersonaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class PersonaFacade  implements PersonaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved PersonaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PersonaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public PersonaDTO save(PersonaDTO entity) {
    				LogDeMidasEJB3.log("saving PersonaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
			return entity;
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent PersonaDTO entity.
	  @param entity PersonaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PersonaDTO entity) {
    				LogDeMidasEJB3.log("deleting PersonaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(PersonaDTO.class, entity.getIdToPersona());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved PersonaDTO entity and return it or a copy of it to the sender. 
	 A copy of the PersonaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PersonaDTO entity to update
	 @return PersonaDTO the persisted PersonaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public PersonaDTO update(PersonaDTO entity) {
    				LogDeMidasEJB3.log("updating PersonaDTO instance", Level.INFO, null);
	        try {
            PersonaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public PersonaDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding PersonaDTO instance with id: " + id, Level.INFO, null);
	        try {
            PersonaDTO instance = entityManager.find(PersonaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all PersonaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PersonaDTO property to query
	  @param value the property value to match
	  	  @return List<PersonaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<PersonaDTO> findByProperty(String propertyName, final Object value) {
    				LogDeMidasEJB3.log("finding PersonaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
				final String queryString = "select model from PersonaDTO model where model." 
			 				+ propertyName + "= :propertyValue";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("propertyValue", value);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all PersonaDTO entities.
	  	  @return List<PersonaDTO> all PersonaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<PersonaDTO> findAll() {
					LogDeMidasEJB3.log("finding all PersonaDTO instances", Level.INFO, null);
			try {
				final String queryString = "select model from PersonaDTO model";
				Query query = entityManager.createQuery(queryString);
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find a PersonaDTO entity related with a specific CotizacionDTO.
	 * CotizacionDTO contains two PersonaDTO records. The found record is indicated by
	 * the param int personaOrden in the next order:
	 * 1 - Persona contratante.
	 * 2 - Persona asegurado.    
	 * @param int personaOrden
	 * @param BigDecimal idToCotizacion
	  	  @return PersonaDTO found by query
	 */
	@SuppressWarnings("unchecked")
	public PersonaDTO findByIdCotizacion(BigDecimal idToCotizacion,int personaOrden){
		LogDeMidasEJB3.log("finding PersonaDTO instance related with Cotizacion: " + idToCotizacion, Level.INFO, null);
		try {
			String queryString = "select model from PersonaDTO model where model.idToPersona in ";
			boolean error=false;
			PersonaDTO persona = null;
			if (personaOrden == 1){
				queryString += "(select cot.personaContratanteDTO.idToPersona from CotizacionDTO cot where cot.idToCotizacion = :idToCotizacion)";
			}else if (personaOrden == 2){
				queryString += "(select cot.personaAseguradoDTO.idToPersona from CotizacionDTO cot where cot.idToCotizacion = :idToCotizacion)";
			}else
				error = true;
			if (!error){
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idToCotizacion", idToCotizacion);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				List<PersonaDTO> lista = query.getResultList();
				if (!lista.isEmpty())
					persona = (PersonaDTO) lista.get(0); 
			}
			return persona;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by idToCotizacion failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find a PersonaDTO entity related with a specific ProveedorInspeccionDTO.
	 * @param BigDecimal idToProveedorInspeccion
	  	  @return DireccionDTO found by query
	 */
	public PersonaDTO findByIdProveedorInspeccion(BigDecimal idToProveedorInspeccion){
		LogDeMidasEJB3.log("finding PersonaDTO instance related with ProveedorInspeccion: " + idToProveedorInspeccion, Level.INFO, null);
		try {
			String queryString = "select model from PersonaDTO model where model.idToPersona in " +
					"(select prov.personaDTO.idToPersona from ProveedorInspeccionDTO prov where prov.idToProveedorInspeccion = :idToProveedorInspeccion)";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idToProveedorInspeccion", idToProveedorInspeccion);
				PersonaDTO persona = (PersonaDTO)query.getSingleResult();
			return persona;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by idToCotizacion failed", Level.SEVERE, re);
			throw re;
		}
	}
}