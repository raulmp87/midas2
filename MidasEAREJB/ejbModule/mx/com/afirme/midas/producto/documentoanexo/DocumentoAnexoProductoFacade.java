package mx.com.afirme.midas.producto.documentoanexo;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity DocumentoAnexoProductoDTO.
 * @see .DocumentoAnexoProductoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DocumentoAnexoProductoFacade  implements DocumentoAnexoProductoFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DocumentoAnexoProductoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocumentoAnexoProductoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocumentoAnexoProductoDTO entity) {
    				LogDeMidasEJB3.log("saving DocumentoAnexoProductoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DocumentoAnexoProductoDTO entity.
	  @param entity DocumentoAnexoProductoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocumentoAnexoProductoDTO entity) {
    				LogDeMidasEJB3.log("deleting DocumentoAnexoProductoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DocumentoAnexoProductoDTO.class, entity.getIdToDocumentoAnexoProducto());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DocumentoAnexoProductoDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocumentoAnexoProductoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocumentoAnexoProductoDTO entity to update
	 @return DocumentoAnexoProductoDTO the persisted DocumentoAnexoProductoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DocumentoAnexoProductoDTO update(DocumentoAnexoProductoDTO entity) {
    				LogDeMidasEJB3.log("updating DocumentoAnexoProductoDTO instance", Level.INFO, null);
	        try {
            DocumentoAnexoProductoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DocumentoAnexoProductoDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding DocumentoAnexoProductoDTO instance with id: " + id, Level.INFO, null);
	        try {
            DocumentoAnexoProductoDTO instance = entityManager.find(DocumentoAnexoProductoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DocumentoAnexoProductoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocumentoAnexoProductoDTO property to query
	  @param value the property value to match
	  	  @return List<DocumentoAnexoProductoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DocumentoAnexoProductoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding DocumentoAnexoProductoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DocumentoAnexoProductoDTO model where model." 
			 						+ propertyName + "= :propertyValue order by model.numeroSecuencia";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all DocumentoAnexoProductoDTO entities.
	  	  @return List<DocumentoAnexoProductoDTO> all DocumentoAnexoProductoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DocumentoAnexoProductoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all DocumentoAnexoProductoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from DocumentoAnexoProductoDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}