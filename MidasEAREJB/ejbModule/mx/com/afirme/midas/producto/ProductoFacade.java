package mx.com.afirme.midas.producto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity ProductoDTO.
 * 
 * @see .ProductoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ProductoFacade implements ProductoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved ProductoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ProductoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ProductoDTO entity) {
		LogDeMidasEJB3.log("saving ProductoDTO instance", Level.INFO, null);
		try {
			if (entity.getIdToProducto() == null) {
				entity.setVersion(Integer.valueOf(1));
			}
			entityManager.persist(entity);
			entityManager.flush();
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ProductoDTO entity.
	 * 
	 * @param entity
	 *            ProductoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ProductoDTO entity) {
		LogDeMidasEJB3.log("deleting ProductoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(ProductoDTO.class, entity
					.getIdToProducto());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ProductoDTO entity and return it or a copy of
	 * it to the sender. A copy of the ProductoDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ProductoDTO entity to update
	 * @return ProductoDTO the persisted ProductoDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ProductoDTO update(ProductoDTO entity) {
		LogDeMidasEJB3.log("updating ProductoDTO instance", Level.INFO, null);
		try {
			ProductoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ProductoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding ProductoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			ProductoDTO instance = entityManager.find(ProductoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ProductoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ProductoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ProductoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ProductoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding ProductoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ProductoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ProductoDTO entities.
	 * 
	 * @return List<ProductoDTO> all ProductoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ProductoDTO> findAll() {
		LogDeMidasEJB3.log("finding all ProductoDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from ProductoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ProductoDTO entities with a specific property value.
	 * 
	 * @param id
	 *            the id value to match
	 * @return ProductoDTO found by query
	 */
	public ProductoDTO findByChildId(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding ProductoDTO instance with TipoPolizaDTO child idToTipoPoliza: "
						+ id, Level.INFO, null);
		try {
			final String queryString = "select parent from ProductoDTO parent, TipoPolizaDTO child where parent.idToProducto = child.productoDTO.idToProducto and child.idToTipoPoliza = :idToTipoPoliza";

			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", id);
			return (ProductoDTO) query.getSingleResult();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by idToTipoPoliza failed", Level.SEVERE,
					re);
			throw re;
		}
	}
	/**
	 * Obtiene los productos agrupados
	 * por descripcion y codigo
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ProductoDTO> listarProductosGroupBy(Long idToNegocio) {

		List<ProductoDTO> tem = new ArrayList<ProductoDTO>();
		StringBuffer queryString = new StringBuffer("SELECT CODIGOPRODUCTO, NOMBRECOMERCIALPRODUCTO FROM MIDAS.TOPRODUCTO Tbl WHERE UPPER(CLAVENEGOCIO) LIKE ");
		queryString.append("'%A%' AND IDTOPRODUCTO NOT IN (SELECT DISTINCT IDTOPRODUCTO FROM MIDAS.TONEGPRODUCTO Tb2 WHERE IDTONEGOCIO =" + idToNegocio + ") ");
		queryString.append("AND CLAVEESTATUS IN (1,2) " );
		queryString.append("GROUP BY CODIGOPRODUCTO, NOMBRECOMERCIALPRODUCTO " );
		queryString.append("ORDER BY NOMBRECOMERCIALPRODUCTO");
		Query nativeQuery = entityManager.createNativeQuery(queryString.toString());
		List<Object> result = nativeQuery.getResultList();
		for(Object obj : result){
			ProductoDTO producto = new ProductoDTO();
			Object[] objects = (Object[])obj;
			producto.setCodigo((String)objects[0]);
			producto.setNombreComercial((String)objects[1]);
			tem.add(producto);
		}
		return tem;
	}	  

	@Override
	public Map<BigDecimal,Integer> getListaVersionesPorCodigoProducto(String codigo) {
		Map<BigDecimal,Integer> resultMap = new TreeMap<BigDecimal, Integer>();
		StringBuffer queryString = new StringBuffer("SELECT * FROM MIDAS.TOPRODUCTO " +
				" WHERE CODIGOPRODUCTO = '"+ codigo+"' AND CLAVEESTATUS IN (1,2) ORDER BY versionproducto");
		Query query = entityManager.createNativeQuery(queryString.toString(),ProductoDTO.class);
		List<ProductoDTO> result = query.getResultList();
		for(ProductoDTO dto : result){
			resultMap.put(dto.getIdToProducto(),dto.getVersion());
		}

		return resultMap;
	}
	/**
	 * Find all ProductoDTO entities with depends of a filter.
	 * 
	 * @param entity
	 *            the name of the entity ProductDTO
	 * 
	 * @return ProductoDTO found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ProductoDTO> listarFiltrado(ProductoDTO entity, Boolean mostrarInactivos) {
		List<ProductoDTO> listado = new ArrayList<ProductoDTO>(); 
		try {
			String queryString = "select model from ProductoDTO model ";
			String sWhere = "";
			Query query;
			String producto = "";
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			
			
			if (entity.getTiposPoliza() != null && entity.getTiposPoliza().size() > 0) {
				if(entity.getTiposPoliza().get(0).getClaveAplicaFlotillas().intValue() == 1 || 
						entity.getTiposPoliza().get(0).getClaveAplicaAutoexpedible().intValue() == 1){
					producto = "productoDTO.";
				
					queryString = "select DISTINCT model.productoDTO from TipoPolizaDTO model ";
								
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveAplicaFlotillas", 
							entity.getTiposPoliza().get(0).getClaveAplicaFlotillas());
				
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveAplicaAutoexpedible", 
							entity.getTiposPoliza().get(0).getClaveAplicaAutoexpedible());
				}
			}
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, producto + "idToProducto", entity.getIdToProducto());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere,  producto + "codigo", entity.getCodigo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere,  producto + "version", entity.getVersion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere,  producto + "descripcion", entity.getDescripcion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere,  producto + "nombreComercial", entity.getNombreComercial());
			
			if(entity.getClaveRenovable() != null && entity.getClaveRenovable().equalsIgnoreCase("1")){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere,  producto + "claveRenovable", entity.getClaveRenovable());
			}
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere,  producto + "descripcionRegistroCNFS", entity.getDescripcionRegistroCNFS());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere,  producto + "claveNegocio", entity.getClaveNegocio());
			//sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idTipoCalculo", entity.getIdTipoCalculo());
			//sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveEstatus", entity.getClaveEstatus());
			//sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveActivoProduccion", entity.getClaveActivoProduccion());

			if(!mostrarInactivos) {
				sWhere += " and model." +  producto + "claveEstatus IN (0,1) ";
			}else{
				sWhere += " and model." +  producto + "claveEstatus IN (1,2,3) ";				
			}
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			if (entity.getTiposPoliza() != null && entity.getTiposPoliza().size() > 0) {
				queryString = queryString.concat(" order by model." +  producto + "codigo, model." + producto + "version ");
				//queryString = queryString.concat(" GROUP BY model." +  producto + "codigo, model." + producto + "version ");
			}else{
				queryString = queryString.concat(" order by model." +  producto + "codigo, model." + producto + "version ");
				//queryString = queryString + " GROUP BY model.codigo, model.version ";				
			}
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			
			listado = query.getResultList();
			
			//Filtrado en el caso de autos 
			
			return listado;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarFiltrado failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Borra l�gicamente un registro de Producto, estableciendo en 0 los campos
	 * claveEstatus  y CLAVEACTIVO
	 * 
	 * @param entity
	 *            ProductoDTO entity el registro Producto a borrar
	 * @throws RuntimeException
	 *             si la operacion falla
	 */
	public ProductoDTO borradoLogico(ProductoDTO entity) {
		LogDeMidasEJB3.log("logical delete ProductoDTO instance", Level.INFO,
				null);
		try {
			entity.setClaveEstatus(new Short("3"));
			entity.setClaveActivo(new Short("0"));
			ProductoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("logical delete successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("logical delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Busca los registros de Producto que no hayan sido borrados logicamente
	 * 
	 * @return List<ProductoDTO> registros de Producto que no han sido borrados
	 *         logicamente
	 */
	@SuppressWarnings("unchecked")
	public List<ProductoDTO> listarVigentes() {
		LogDeMidasEJB3.log("finding all active ProductoDTO instances",Level.INFO, null);
		try {
			final String queryString = "select model from ProductoDTO model where model.claveEstatus IN (0,1,2)";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all active failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ProductoDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public ProductoDTO findById(double id) {
		return null;
	}

	/**
	 * Find a productoDTO entity wich is related with the given solicitudDTO entity.
	 * @param idToSolicitud SolicitudDTO entity id
	 * @return ProductoDTO entity
	 */
	@SuppressWarnings("unchecked")
	public ProductoDTO findProductoBySolicitud(BigDecimal idToSolicitud) {
		LogDeMidasEJB3.log("finding productoDTO entity wich is related with solicitudDTO: "+idToSolicitud, Level.INFO,null);
		try {
			final String queryString = "select model from ProductoDTO model where model.idToProducto in " +
					"(select s.productoDTO.idToProducto from SolicitudDTO s where s.idToSolicitud = :idToSolicitud)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSolicitud", idToSolicitud);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			List<ProductoDTO> listaProducto = query.getResultList();
			if (!listaProducto.isEmpty())
				return (ProductoDTO)listaProducto.get(0);
			return null;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<ProductoDTO> listRelated(Object id) {
		return findAll();
	}


	@TransactionAttribute (TransactionAttributeType.NOT_SUPPORTED)
	public Map<String,String>  generarNuevaVersion(java.math.BigDecimal pIdProducto) throws Exception{
		Map<String, String> mensaje = new HashMap<String, String>();
		String spName = "MIDAS.PKGPYT_SERVICIOS.spPYT_NuevaVersionProducto";
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);			
			
			storedHelper.estableceParametro("pIdProducto",pIdProducto);
			
			BigDecimal resultado= BigDecimal.valueOf(storedHelper.ejecutaActualizar());
			ProductoDTO nuevoProducto = this.findById(resultado);
			mensaje.put("exito", "1");//Exito
			mensaje.put("mensaje", "Se Gener\u00f3 correctamente la nueva versi\u00f3n del producto:" + nuevoProducto.getNombreComercial() +"</br> El n\u00famero de Versi\u00f3n  es: " + nuevoProducto.getVersion());	
			LogDeMidasInterfaz.log("Saliendo de ProductoFacade..." + this, Level.INFO, null);
		} catch (Exception e){
			LogDeMidasEJB3.log("nuevaversion failed", Level.SEVERE, e);
			mensaje.put("icono", "10");//Error
			mensaje.put("mensaje", "Error al generar la Versi\u00f3n.");
		    return mensaje;
		}
		return mensaje;
	}

}