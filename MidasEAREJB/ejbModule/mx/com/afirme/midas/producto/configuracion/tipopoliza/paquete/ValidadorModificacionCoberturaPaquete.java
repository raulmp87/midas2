
package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.cobertura.CoberturaSeccionPaqueteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;

public abstract class ValidadorModificacionCoberturaPaquete {

	protected PaquetePolizaFacade paquetePolizaFacade;
	
	public ValidadorModificacionCoberturaPaquete(PaquetePolizaFacade paquetePolizaFacade) {
		super();
		this.paquetePolizaFacade = paquetePolizaFacade;
	}

	public abstract ActualizacionCoberturaPaqueteDTO validarModificacionCoberturaPaquete(CoberturaSeccionDTO coberturaSeccionDTO,
				CoberturaSeccionPaqueteDTO coberturaPaqueteDTO, PaquetePolizaDTO paquetePolizaDTO);
		
	/**
	 * Usado para incluir una coberturaSeccion en el paquete
	 * @param coberturaSeccionDTO
	 * @param paquetePolizaDTO
	 * @return
	 */
	protected ActualizacionCoberturaPaqueteDTO contratarCoberturaPaquete(CoberturaSeccionDTO coberturaSeccionDTO,
			PaquetePolizaDTO paquetePolizaDTO){
		
		CoberturaSeccionPaqueteDTO coberturaPaqueteDTO = null;
		
		ActualizacionCoberturaPaqueteDTO resultadoActualizacion = new ActualizacionCoberturaPaqueteDTO();
		resultadoActualizacion.setCoberturaSeccion(coberturaSeccionDTO);
		resultadoActualizacion.setPaquetePoliza(paquetePolizaDTO);
		
		if(coberturaSeccionDTO.getCoberturaDTO().getClaveTipoSumaAsegurada().equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_BASICA)){
			
			coberturaPaqueteDTO = registrarCoberturaPaquete(paquetePolizaDTO, coberturaSeccionDTO, null);
			
			resultadoActualizacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_EXITOSO);
			resultadoActualizacion.setOperacionExitosa(Boolean.TRUE);
		}
		else if(coberturaSeccionDTO.getCoberturaDTO().getClaveTipoSumaAsegurada().equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_AMPARADA) ||
				coberturaSeccionDTO.getCoberturaDTO().getClaveTipoSumaAsegurada().equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_SUBLIMITE)){
			
			CoberturaSeccionPaqueteDTO coberturaBasicaContratada = contratarCoberturaBasica(
					paquetePolizaDTO.getTipoPolizaDTO().getIdToTipoPoliza(), 
					paquetePolizaDTO, coberturaSeccionDTO, null);
			
			if(coberturaBasicaContratada != null){
				//La cobertura b�sica no estaba en el paquete
				resultadoActualizacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_COBERTURA_BASICA_CONTRATADA);
				resultadoActualizacion.setCoberturaSeccionBasica(coberturaBasicaContratada);
				resultadoActualizacion.setOperacionExitosa(Boolean.TRUE);
			}
			
			if (coberturaSeccionDTO.getCoberturaDTO().getClaveTipoSumaAsegurada().equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_SUBLIMITE)){
				//Si la cobertura es subl�mite, se calcula su suma asegurada en base a su cobertura b�sica
				
				coberturaBasicaContratada = paquetePolizaFacade.consultarCoberturaPaqueteBasica(
						paquetePolizaDTO.getIdToPaquetePoliza(), coberturaSeccionDTO,null);
				
				BigDecimal valorSumaAseguradaSublimite = coberturaBasicaContratada.getValorSumaAsegurada();
				if(coberturaBasicaContratada.getValorSumaAsegurada().compareTo(BigDecimal.ZERO) >0 &&
						coberturaSeccionDTO.getCoberturaDTO().getFactorMinSumaAsegurada() != null){
					
					valorSumaAseguradaSublimite = aplicarFactorSumaAsegurada(
							valorSumaAseguradaSublimite,coberturaSeccionDTO.getCoberturaDTO().getFactorMinSumaAsegurada());
				}
				coberturaPaqueteDTO = registrarCoberturaPaquete(paquetePolizaDTO, coberturaSeccionDTO, valorSumaAseguradaSublimite);
				
			}
			else{
				coberturaPaqueteDTO = registrarCoberturaPaquete(paquetePolizaDTO, coberturaSeccionDTO, null);
			}
			
			if(resultadoActualizacion.getClaveResultado() == null){
				resultadoActualizacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_EXITOSO);
				resultadoActualizacion.setOperacionExitosa(Boolean.TRUE);
			}
			
		}
		
		resultadoActualizacion.setCoberturaPaqueteDTO(coberturaPaqueteDTO);
		
		return resultadoActualizacion;
	}
	
	/**
	 * Usado para excluir una cobertura del paquete. En la exclusi�n se incluyen las coberturas amparadas y subl�mites dependientes. 
	 * @param coberturaSeccionDTO
	 * @param coberturaPaqueteDTO
	 * @param paquetePolizaDTO
	 * @return
	 */
	protected ActualizacionCoberturaPaqueteDTO descontratarCoberturaConDependencias(CoberturaSeccionDTO coberturaSeccionDTO,
			CoberturaSeccionPaqueteDTO coberturaPaqueteDTO, PaquetePolizaDTO paquetePolizaDTO){
		
		ActualizacionCoberturaPaqueteDTO resultadoOperacion = new ActualizacionCoberturaPaqueteDTO();
		resultadoOperacion.setCoberturaSeccion(coberturaSeccionDTO);
//		resultadoOperacion.setCoberturaSeccionBasica(coberturaSeccionDTO);
		resultadoOperacion.setPaquetePoliza(paquetePolizaDTO);
		
		if(coberturaSeccionDTO.getCoberturaDTO().getClaveTipoSumaAsegurada().equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_BASICA)){
			
			ActualizacionCoberturaPaqueteDTO borradoCoberturasAmparadas = excluirCoberturasDependientes(
					coberturaSeccionDTO.getCoberturaDTO().getIdToCobertura(), 
					paquetePolizaDTO.getTipoPolizaDTO().getIdToTipoPoliza(),
					paquetePolizaDTO.getIdToPaquetePoliza(),
					ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_AMPARADA);
			
			if (!borradoCoberturasAmparadas.getOperacionExitosa()){
				resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_DESCONTRATO_COBERTURA_AMPARADA_OBLIGATORIA);
				resultadoOperacion.setOperacionExitosa(Boolean.FALSE);
				resultadoOperacion.setOperacionLimitante(borradoCoberturasAmparadas);
			}
			else{
				ActualizacionCoberturaPaqueteDTO borradoCoberturasSublimite = excluirCoberturasDependientes(
						coberturaSeccionDTO.getCoberturaDTO().getIdToCobertura(),
						paquetePolizaDTO.getTipoPolizaDTO().getIdToTipoPoliza(),
						paquetePolizaDTO.getIdToPaquetePoliza(),
						ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_SUBLIMITE);
				
				if (!borradoCoberturasSublimite.getOperacionExitosa()){
					resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_DESCONTRATO_COBERTURA_SUBLIMITE_OBLIGATORIA);
					resultadoOperacion.setOperacionExitosa(Boolean.FALSE);
					resultadoOperacion.setOperacionLimitante(borradoCoberturasSublimite);
				}
				else{
					//Se eliminaron correctamente las coberturas dependientes, se procede con el borrado de la cobertura b�sica
					paquetePolizaFacade.eliminarCoberturaSeccionPaquete(coberturaPaqueteDTO);
					
					if(!borradoCoberturasAmparadas.getCoberturasAmparadasDependientes().isEmpty() && 
							!borradoCoberturasSublimite.getCoberturasSubLimiteDependientes().isEmpty()){
						resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_COBERTURAS_AMPARADAS_Y_SUBLIMITE_DESCONTRATADAS);
					}
					else if(!borradoCoberturasAmparadas.getCoberturasAmparadasDependientes().isEmpty()){
						resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_COBERTURAS_AMPARADAS_DESCONTRATADAS);
					}
					else if (!borradoCoberturasSublimite.getCoberturasSubLimiteDependientes().isEmpty()){
						resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_COBERTURAS_SUBLIMITE_DESCONTRATADAS);
					}
					else{
						resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_EXITOSO);
					}
					resultadoOperacion.setOperacionExitosa(Boolean.TRUE);
					resultadoOperacion.setCoberturasAmparadasDependientes(borradoCoberturasAmparadas.getCoberturasAmparadasDependientes());
					resultadoOperacion.setCoberturasSubLimiteDependientes(borradoCoberturasSublimite.getCoberturasSubLimiteDependientes());
				}
			}
		}
		else{
			
			//3. Excluir la cobertura del paquete
			paquetePolizaFacade.eliminarCoberturaSeccionPaquete(coberturaPaqueteDTO);
			
			resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_EXITOSO);
			resultadoOperacion.setOperacionExitosa(Boolean.TRUE);
		}
		
		resultadoOperacion.setCoberturaPaqueteDTO(coberturaPaqueteDTO);
		
		return resultadoOperacion;
	}
	
	protected ActualizacionCoberturaPaqueteDTO excluirCoberturaObligatoria(
			CoberturaSeccionDTO coberturaSeccionDTO,
			CoberturaSeccionPaqueteDTO coberturaPaqueteDTO,
			PaquetePolizaDTO paquetePolizaDTO){
		ActualizacionCoberturaPaqueteDTO resultadoOperacion = new ActualizacionCoberturaPaqueteDTO();
		
		if(coberturaPaqueteDTO.getCoberturaSeccionDTO().getClaveObligatoriedad().shortValue() == ConstantesCotizacion.OBLIGATORIO){
			resultadoOperacion.setOperacionExitosa(Boolean.FALSE);
			resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_DESCONTRATO_COBERTURA_OBLIGATORIA);
			
			resultadoOperacion.setCoberturaPaqueteDTO(coberturaPaqueteDTO);
			resultadoOperacion.setCoberturaSeccion(coberturaPaqueteDTO.getCoberturaSeccionDTO());
			resultadoOperacion.setPaquetePoliza(paquetePolizaDTO);
		}
		else if(coberturaPaqueteDTO.getCoberturaSeccionDTO().getClaveObligatoriedad().shortValue() == ConstantesCotizacion.OBLIGATORIO_PARCIAL){
			List<CoberturaSeccionPaqueteDTO> listaCoberturasObligatorioParcial = 
				paquetePolizaFacade.obtenerCoberturasObligatoriaParcialEnPaquete(paquetePolizaDTO.getIdToPaquetePoliza(), 
						coberturaPaqueteDTO.getCoberturaSeccionDTO().getId().getIdtoseccion());
			
			if(listaCoberturasObligatorioParcial.size() > 1){
				
				resultadoOperacion = this.descontratarCoberturaConDependencias(coberturaSeccionDTO, coberturaPaqueteDTO, paquetePolizaDTO);
				
			}
			else{
				resultadoOperacion.setOperacionExitosa(Boolean.FALSE);
				resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_DESCONTRATO_COBERTURA_OBLIGATORIA_PARCIAL);
				
				resultadoOperacion.setCoberturaPaqueteDTO(coberturaPaqueteDTO);
				resultadoOperacion.setCoberturaSeccion(coberturaPaqueteDTO.getCoberturaSeccionDTO());
				resultadoOperacion.setPaquetePoliza(paquetePolizaDTO);
			}
		}
		else{
			resultadoOperacion = this.descontratarCoberturaConDependencias(coberturaSeccionDTO, coberturaPaqueteDTO, paquetePolizaDTO);
		}
		
		return resultadoOperacion;
	}
	
	/**
	 * Usado para actualizar la suma asegurada de una cobertura en el paquete.
	 * Si la cobertura es b�sica, se actualizan las sumas aseguradas de las coberturas subl�mite del paquete.
	 * Si la cobertura es subl�mite, se valida que la suma asegurada est� en los l�mites permitidos.
	 * Si la cobertrua es amparada, se cancela la operaci�n.
	 * @param coberturaSeccionDTO
	 * @param coberturaPaqueteDTO
	 * @param paquetePolizaDTO
	 * @return
	 */
	protected ActualizacionCoberturaPaqueteDTO actualizarSumaAseguradaCoberturaBasica(
			CoberturaSeccionDTO coberturaSeccionDTO, CoberturaSeccionPaqueteDTO coberturaPaqueteDTO,
			PaquetePolizaDTO paquetePolizaDTO){
		
		ActualizacionCoberturaPaqueteDTO resultadoOperacion = new ActualizacionCoberturaPaqueteDTO();
		
		if(coberturaPaqueteDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_BASICA)){
			
			//Validar la suma asegurada a registrar en base a los porcentajes de la posible cobertura b�sica de quien depende la SA.
			resultadoOperacion = validarDependenciaSubLimiteCambioSA(coberturaSeccionDTO, coberturaPaqueteDTO, paquetePolizaDTO);
			
			if(resultadoOperacion.getOperacionExitosa()){
				
				List<CoberturaSeccionDTO> listaCoberturasModificadas = new ArrayList<CoberturaSeccionDTO>();
				
				//Actualizar la SA de las coberturas sublimite dependientes
				actualizarCambioSACoberturasDependientes(coberturaPaqueteDTO, 
						ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_SUBLIMITE, 
						listaCoberturasModificadas);
				
				//Actualizar la SA de las coberturas basicas dependientes
				actualizarCambioSACoberturasDependientes(coberturaPaqueteDTO, 
						ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_BASICA, 
						listaCoberturasModificadas);
				
				if(!listaCoberturasModificadas.isEmpty()){
					resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_SA_COBERTURAS_SUBLIMITE_MODIFICADA);
					resultadoOperacion.setCoberturasSubLimiteDependientes(listaCoberturasModificadas);
				}
			}
			
    	}
		
		resultadoOperacion.setCoberturaSeccion(coberturaPaqueteDTO.getCoberturaSeccionDTO());
    	resultadoOperacion.setCoberturaPaqueteDTO(coberturaPaqueteDTO);
    	resultadoOperacion.setPaquetePoliza(paquetePolizaDTO);
    	
		return resultadoOperacion;
	}
	
	private void actualizarCambioSACoberturasDependientes(
			CoberturaSeccionPaqueteDTO coberturaPaqueteBasicaDTO,
			String claveTipoSumaAsegurada,
			List<CoberturaSeccionDTO> listaCoberturasModificadas){
		
		BigDecimal nuevaSumaAsegurada = coberturaPaqueteBasicaDTO.getValorSumaAsegurada();
		
		List<CoberturaSeccionDTO> listaCoberturasDependientes = paquetePolizaFacade.obtenerCoberturasDependientes(
				coberturaPaqueteBasicaDTO.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura(),
				coberturaPaqueteBasicaDTO.getPaquetePolizaDTO().getTipoPolizaDTO().getIdToTipoPoliza(),
				claveTipoSumaAsegurada);
		
		for(CoberturaSeccionDTO coberturaDependiente: listaCoberturasDependientes){
			CoberturaSeccionPaqueteDTO coberturaDependientePaquete = paquetePolizaFacade.obtenerCoberturaSeccionPaquete(
					coberturaPaqueteBasicaDTO.getPaquetePolizaDTO().getIdToPaquetePoliza(),coberturaDependiente);
			
			if(coberturaDependientePaquete != null){
				//Actualizacion de la suma asegurada de las coberturas sublimite.
				if(coberturaDependiente.getCoberturaDTO().getFactorMinSumaAsegurada() != null){
					coberturaDependientePaquete.setValorSumaAsegurada(
							aplicarFactorSumaAsegurada(nuevaSumaAsegurada, coberturaDependiente.getCoberturaDTO().getFactorMinSumaAsegurada()));
					
					paquetePolizaFacade.persistirCoberturaSeccionPaqueteDTO(coberturaDependientePaquete);
					
					if(listaCoberturasModificadas != null){
						listaCoberturasModificadas.add(coberturaDependiente);
					}
				}
			}
		}
	}
	
	/**
	 * Valida la dependencia de suma asegurada para las coberturas subl�mite, as� como las b�sicas cuya configuraci�n 
	 * indica la dependencia de SA en base a otra cobertura b�sica.
	 * La validaci�n considera el claveTipoSumaAsegurada o el campo claveFuenteSumaAsegurada (sublimite) para las coberturas
	 * b�sicas.
	 * @param coberturaSeccionDTO
	 * @param coberturaPaqueteDTO
	 * @param paquetePolizaDTO
	 */
	protected ActualizacionCoberturaPaqueteDTO validarDependenciaSubLimiteCambioSA(CoberturaSeccionDTO coberturaSeccionDTO,
			CoberturaSeccionPaqueteDTO coberturaPaqueteDTO, PaquetePolizaDTO paquetePolizaDTO){
		
		ActualizacionCoberturaPaqueteDTO resultadoOperacion = new ActualizacionCoberturaPaqueteDTO();
		BigDecimal nuevaSumaAsegurada = coberturaPaqueteDTO.getValorSumaAsegurada();
		boolean procederModificacion = false;
		
		if(coberturaPaqueteDTO.getCoberturaSeccionDTO().getCoberturaDTO().aplicaValidacionSumaAseguradaSubLimite() ){
			
			CoberturaSeccionPaqueteDTO coberturaBasicaPaquete = paquetePolizaFacade.consultarCoberturaPaqueteBasica(
					coberturaPaqueteDTO.getPaquetePolizaDTO().getIdToPaquetePoliza(), 
					coberturaPaqueteDTO.getCoberturaSeccionDTO(), null);
			
//			if(coberturaBasicaPaquete != null && coberturaPaqueteDTO.getCoberturaSeccionDTO().getCoberturaDTO().getFactorMinSumaAsegurada() != null &&
//					coberturaPaqueteDTO.getCoberturaSeccionDTO().getCoberturaDTO().getFactorMaxSumaAsegurada() != null){
			
			if(coberturaBasicaPaquete != null){
				BigDecimal limiteMinimoSA = null;
				BigDecimal limiteMaximoSA = null;
				
				limiteMinimoSA = aplicarFactorSumaAsegurada(coberturaBasicaPaquete.getValorSumaAsegurada(),
						coberturaPaqueteDTO.getCoberturaSeccionDTO().getCoberturaDTO().getFactorMinSumaAsegurada());
				
				limiteMaximoSA = aplicarFactorSumaAsegurada(coberturaBasicaPaquete.getValorSumaAsegurada(),
						coberturaPaqueteDTO.getCoberturaSeccionDTO().getCoberturaDTO().getFactorMaxSumaAsegurada());
				
				if(nuevaSumaAsegurada.compareTo(limiteMinimoSA) >= 0 && nuevaSumaAsegurada.compareTo(limiteMaximoSA) <= 0){
					
//					else{
						//Cumple con los l�mites, se contin�a con la actualizaci�n
						procederModificacion = true;
//					}
				}
				else{
					//La suma asegurada no est� en los l�mites, se cancela la operaci�n
					resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_REGISTRO_SA_COBERTURA_SUBLIMITE);
					resultadoOperacion.setOperacionExitosa(Boolean.FALSE);
					resultadoOperacion.setValorMinimoSumaAseguradaSublimite(limiteMinimoSA);
					resultadoOperacion.setValorMaximoSumaAseguradaSublimite(limiteMaximoSA);
					resultadoOperacion.setCoberturaSeccionBasica(coberturaBasicaPaquete);
				}
			}
			else{
				//La cobertura b�sica no se encuentra en el paquete
				CoberturaSeccionDTO coberturaSeccionBasica = paquetePolizaFacade.consultarCoberturaBasica(
						coberturaPaqueteDTO.getCoberturaSeccionDTO());
				coberturaBasicaPaquete = new CoberturaSeccionPaqueteDTO();
				coberturaBasicaPaquete.setCoberturaSeccionDTO(coberturaSeccionBasica);
				
				resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_COBERTURA_BASICA_NO_CONTRATADA);
				resultadoOperacion.setOperacionExitosa(Boolean.FALSE);
				resultadoOperacion.setCoberturaSeccionBasica(coberturaBasicaPaquete);
			}
		}
		else if (coberturaPaqueteDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_BASICA)){
			//basica que no depende de otra cobertura
			procederModificacion = true;
		}
		
		if(procederModificacion){	
			CoberturaDTO coberturaDTO = coberturaPaqueteDTO.getCoberturaSeccionDTO().getCoberturaDTO();
			if (coberturaDTO.getClaveTipoSumaAsegurada().equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_BASICA)){
				boolean cumpleLimiteMinimo = coberturaDTO.getMinSumaAsegurada() == null;
				boolean cumpleLimiteMaximo= coberturaDTO.getMaxSumaAsegurada() == null;
				//nuevaSumaAsegurada;
				if(!cumpleLimiteMinimo){
					cumpleLimiteMinimo = nuevaSumaAsegurada.compareTo(coberturaDTO.getMinSumaAsegurada()) >= 0;
				}
				if(!cumpleLimiteMaximo){
					cumpleLimiteMaximo = nuevaSumaAsegurada.compareTo(coberturaDTO.getMaxSumaAsegurada()) <= 0;
				}
				if(cumpleLimiteMaximo && cumpleLimiteMinimo){
					procederModificacion = true;
				}
				else{
					//La cobertura es b�sica pero la nueva SA no est� en sus rangos permitidos.
					resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_REGISTRO_SA_COBERTURA_BASICA_RANGOS);
					resultadoOperacion.setOperacionExitosa(Boolean.FALSE);
					resultadoOperacion.setValorMinimoSumaAseguradaSublimite(coberturaDTO.getMinSumaAsegurada());
					resultadoOperacion.setValorMaximoSumaAseguradaSublimite(coberturaDTO.getMaxSumaAsegurada());
					
					procederModificacion = false;
				}
			}
			
			if(procederModificacion){
				coberturaPaqueteDTO = paquetePolizaFacade.persistirCoberturaSeccionPaqueteDTO(coberturaPaqueteDTO);
				resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_EXITOSO);
				resultadoOperacion.setOperacionExitosa(Boolean.TRUE);
			}
			
		}
		
		resultadoOperacion.setCoberturaSeccion(coberturaPaqueteDTO.getCoberturaSeccionDTO());
    	resultadoOperacion.setCoberturaPaqueteDTO(coberturaPaqueteDTO);
    	resultadoOperacion.setPaquetePoliza(paquetePolizaDTO);
    	
    	return resultadoOperacion;
	}
	
	/**
	 * Genera el objeto ActualizacionCoberturaPaqueteDTO con el mensaje de error po intento de cambio de SA en cobertura amparada 
	 * @param coberturaSeccionDTO
	 * @param coberturaPaqueteDTO
	 * @param paquetePolizaDTO
	 * @return
	 */
	protected ActualizacionCoberturaPaqueteDTO generarResultadoErrorCambioSAAmparada(CoberturaSeccionDTO coberturaSeccionDTO,
			CoberturaSeccionPaqueteDTO coberturaPaqueteDTO, PaquetePolizaDTO paquetePolizaDTO){
		ActualizacionCoberturaPaqueteDTO resultadoOperacion = new ActualizacionCoberturaPaqueteDTO();
		
		resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_REGISTRO_SA_COBERTURA_AMPARADA);
		resultadoOperacion.setOperacionExitosa(Boolean.FALSE);
		
		resultadoOperacion.setCoberturaSeccion(coberturaPaqueteDTO.getCoberturaSeccionDTO());
    	resultadoOperacion.setCoberturaPaqueteDTO(coberturaPaqueteDTO);
    	resultadoOperacion.setPaquetePoliza(paquetePolizaDTO);
    	
		return resultadoOperacion;
	}
	
	/**
	 * Se encarga de registrar una cobertura en el paquete, este m�todo s�lo es invocado cuando se pasan las validaciones necesarias
	 * para continuar con el registro de la cobertura en el paquete.
	 * @param paquetePolizaDTO
	 * @param coberturaSeccionDTO
	 * @param valorSumaAsegurada
	 * @return
	 */
	private CoberturaSeccionPaqueteDTO registrarCoberturaPaquete(PaquetePolizaDTO paquetePolizaDTO,CoberturaSeccionDTO coberturaSeccionDTO,BigDecimal valorSumaAsegurada){
    	valorSumaAsegurada = valorSumaAsegurada != null ? valorSumaAsegurada : BigDecimal.ZERO;
    	CoberturaSeccionPaqueteDTO coberturaPaqueteDTO = new CoberturaSeccionPaqueteDTO(null,paquetePolizaDTO,ConstantesCotizacion.CONTRATADO,valorSumaAsegurada);
    	coberturaPaqueteDTO.setCoberturaSeccionDTO(coberturaSeccionDTO);
    	return paquetePolizaFacade.persistirCoberturaSeccionPaqueteDTO(coberturaPaqueteDTO);
    }
	
	  /**
	  * Recibe Como par�metro una cobertura amparada o subl�mite, y consulta si su cobertura b�sica est� registrada en el 
	  * paquete, de no ser as�, incluye la cobertura b�sica en el paquete.
	  * @param idToTipoPoliza
	  * @param paquetePolizaDTO
	  * @param coberturaDependiente
	  * @param listaCoberturasRegistradasPaquete
	  * @return
	  */
	private CoberturaSeccionPaqueteDTO contratarCoberturaBasica(BigDecimal idToTipoPoliza,
	 		PaquetePolizaDTO paquetePolizaDTO,CoberturaSeccionDTO coberturaDependiente,
	 		List<CoberturaSeccionPaqueteDTO> listaCoberturasRegistradasPaquete){
	 	
		CoberturaSeccionPaqueteDTO coberturaPaqueteDTO = paquetePolizaFacade.consultarCoberturaPaqueteBasica(
					paquetePolizaDTO.getIdToPaquetePoliza(), coberturaDependiente,listaCoberturasRegistradasPaquete);
			
			//Si la cobertura b�sica no se encuentra registrada, se incluye en el paquete.
		if(coberturaPaqueteDTO == null){
			CoberturaSeccionDTO coberturaBasica = paquetePolizaFacade.consultarCoberturaBasica(coberturaDependiente);
			coberturaPaqueteDTO = registrarCoberturaPaquete(paquetePolizaDTO, coberturaBasica, BigDecimal.ZERO);
				
			if(listaCoberturasRegistradasPaquete != null)
				listaCoberturasRegistradasPaquete.add(coberturaPaqueteDTO);
		}
		else{
			coberturaPaqueteDTO = null;
		}
		return coberturaPaqueteDTO;
	}
	
	private BigDecimal aplicarFactorSumaAsegurada(BigDecimal sumaAsegurada,BigDecimal factor){
		BigDecimal nuevoValor =  sumaAsegurada.multiply( factor, MathContext.DECIMAL32);
		if(factor.compareTo(BigDecimal.ONE) > 0){
			//El factor es de 0 a 100, se aplica division entre 100
			nuevoValor = nuevoValor.divide(new BigDecimal("100"),MathContext.DECIMAL32);
		}
		return nuevoValor;
	}
	
	private ActualizacionCoberturaPaqueteDTO excluirCoberturasDependientes(
			BigDecimal idToCoberturaBasica,BigDecimal idToTipoPoliza, BigDecimal idToPaquetePoliza,
			String claveDependencia){
		
		ActualizacionCoberturaPaqueteDTO resultadoOperacion = new ActualizacionCoberturaPaqueteDTO();
		
		List<CoberturaSeccionDTO> listaCoberturasExcluidas = new ArrayList<CoberturaSeccionDTO>();
		
		//1. Consultar coberturas b�sicas y subl�mite que dependen de la cobertura en cuestion
		List<CoberturaSeccionDTO> listaCoberturasDependientes = paquetePolizaFacade.obtenerCoberturasDependientes(idToCoberturaBasica,
				idToTipoPoliza, claveDependencia);
		
		//2. Excluir cada cobertura del paquete
		for(CoberturaSeccionDTO coberturaseccionTMP : listaCoberturasDependientes){
			
			CoberturaSeccionPaqueteDTO coberturaDependientePaquete = paquetePolizaFacade.obtenerCoberturaSeccionPaquete(idToPaquetePoliza,coberturaseccionTMP);
			
			if(coberturaDependientePaquete != null){
				
				ValidadorModificacionCoberturaPaquete validadorEliminarCobDependiente = new AdministradorValidacionCoberturaPaquete().obtenerValidadorModificacionCoberturaPaquete(
						coberturaseccionTMP.getCoberturaDTO().getClaveTipoSumaAsegurada(), 
						coberturaseccionTMP.getClaveObligatoriedad().shortValue(),
						AdministradorValidacionCoberturaPaquete.OPERACION_DESCONTRATAR,
						paquetePolizaFacade);
				
				ActualizacionCoberturaPaqueteDTO resultadoEliminarCoberturaDependiente = 
					validadorEliminarCobDependiente.validarModificacionCoberturaPaquete(coberturaseccionTMP,
							coberturaDependientePaquete, coberturaDependientePaquete.getPaquetePolizaDTO());
				
				if(!resultadoEliminarCoberturaDependiente.getOperacionExitosa()){
					return resultadoEliminarCoberturaDependiente;
				}
				else{
					listaCoberturasExcluidas.add(coberturaseccionTMP);
				}
			}
		}
		
		resultadoOperacion.setClaveResultado(ActualizacionCoberturaPaqueteDTO.CLAVE_EXITOSO);
		resultadoOperacion.setOperacionExitosa(Boolean.TRUE);
		
		if(claveDependencia.equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_AMPARADA)){
			resultadoOperacion.setCoberturasAmparadasDependientes(listaCoberturasExcluidas);
		}
		else if(claveDependencia.equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_SUBLIMITE)){
			resultadoOperacion.setCoberturasSubLimiteDependientes(listaCoberturasExcluidas);
		}
		
		return resultadoOperacion;
		
	}
	
	public static void actualizarMensajeErrorResultado(ActualizacionCoberturaPaqueteDTO resultadoOperacion){
		
		if(resultadoOperacion != null && resultadoOperacion.getClaveResultado() != null){
			String mensajeError = "";
			if(resultadoOperacion.getClaveResultado().shortValue() == 
				ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_DESCONTRATO_COBERTURA_AMPARADA_OBLIGATORIA.shortValue()){
				mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
						"configuracion.tipopoliza.paquete.error.descontratoCoberturaAmparadaObligatoria", 
						resultadoOperacion.getCoberturaSeccion().getCoberturaDTO().getNombreComercial(),
						resultadoOperacion.getOperacionLimitante().getCoberturaSeccion().getCoberturaDTO().getNombreComercial());
			}
			else if(resultadoOperacion.getClaveResultado().shortValue() == 
				ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_DESCONTRATO_COBERTURA_OBLIGATORIA.shortValue()){
				mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
						"configuracion.tipopoliza.paquete.error.descontratoCoberturaObligatoria", 
						resultadoOperacion.getCoberturaSeccion().getCoberturaDTO().getNombreComercial());
			}
			else if(resultadoOperacion.getClaveResultado().shortValue() == 
				ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_DESCONTRATO_COBERTURA_OBLIGATORIA_PARCIAL.shortValue()){
				mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
						"configuracion.tipopoliza.paquete.error.descontratoCoberturaObligatoriaParcial", 
						resultadoOperacion.getCoberturaSeccion().getCoberturaDTO().getNombreComercial());
			}
			else if(resultadoOperacion.getClaveResultado().shortValue() == 
				ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_DESCONTRATO_COBERTURA_SUBLIMITE_OBLIGATORIA.shortValue()){
				mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
						"configuracion.tipopoliza.paquete.error.descontratoCoberturaSublimiteObligatoria", 
						resultadoOperacion.getCoberturaSeccion().getCoberturaDTO().getNombreComercial(),
						resultadoOperacion.getOperacionLimitante().getCoberturaSeccion().getCoberturaDTO().getNombreComercial());
			}
			else if(resultadoOperacion.getClaveResultado().shortValue() == 
				ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_REGISTRO_SA_COBERTURA_AMPARADA.shortValue()){
				mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
					"configuracion.tipopoliza.paquete.error.registroSumaAseguradaCoberturaAmparada");
			}
			else if(resultadoOperacion.getClaveResultado().shortValue() == 
				ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_REGISTRO_SA_COBERTURA_SUBLIMITE.shortValue()){
				mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
						"configuracion.tipopoliza.paquete.error.registroSumaAseguradaCoberturaSublimite", 
						resultadoOperacion.getCoberturaSeccionBasica().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial(),
						resultadoOperacion.getValorMinimoSumaAseguradaSublimite(),
						resultadoOperacion.getValorMaximoSumaAseguradaSublimite());
			}
			else if (resultadoOperacion.getClaveResultado().shortValue() == 
				ActualizacionCoberturaPaqueteDTO.LIBERAR_PAQUETE_NO_EXITOSO_COBERTURA_BASICA_SIN_SA){
				mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
						"configuracion.tipopoliza.paquete.error.liberarPaqueteCoberturaBasicaSinSA", 
						resultadoOperacion.getCoberturaSeccionBasica().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
			}
			else if(resultadoOperacion.getClaveResultado().shortValue() ==
				ActualizacionCoberturaPaqueteDTO.CLAVE_SA_COBERTURAS_SUBLIMITE_MODIFICADA){
				String coberturasModificadas = obtenerDescripcionListaCoberturas(resultadoOperacion.getCoberturasSubLimiteDependientes());
				if(!StringUtil.isEmpty(coberturasModificadas)){
					mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
							"configuracion.tipopoliza.paquete.exito.actualizarSABasica.SubLimiteModificadas", 
							coberturasModificadas);
				}
			}
			else if(resultadoOperacion.getClaveResultado().shortValue() ==
				ActualizacionCoberturaPaqueteDTO.CLAVE_COBERTURA_BASICA_CONTRATADA){
				
				if(resultadoOperacion.getCoberturaSeccionBasica() != null){
					mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
							"configuracion.tipopoliza.paquete.exito.contratarDependiente.basicaContratada", 
							resultadoOperacion.getCoberturaSeccionBasica().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
				}
			}
			else if(resultadoOperacion.getClaveResultado().shortValue() ==
				ActualizacionCoberturaPaqueteDTO.CLAVE_COBERTURAS_AMPARADAS_Y_SUBLIMITE_DESCONTRATADAS){
				
				String coberturasAmparadas = obtenerDescripcionListaCoberturas(resultadoOperacion.getCoberturasAmparadasDependientes());
				String coberturasSubLimite = obtenerDescripcionListaCoberturas(resultadoOperacion.getCoberturasSubLimiteDependientes());
				
				if(!StringUtil.isEmpty(coberturasAmparadas) && !StringUtil.isEmpty(coberturasSubLimite)){
					mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
							"configuracion.tipopoliza.paquete.exito.descontratarBasica.dependientesDescontratadas", 
							coberturasAmparadas,coberturasSubLimite);
				}
			}
			else if(resultadoOperacion.getClaveResultado().shortValue() ==
				ActualizacionCoberturaPaqueteDTO.CLAVE_COBERTURAS_AMPARADAS_DESCONTRATADAS){
				
				String coberturasAmparadas = obtenerDescripcionListaCoberturas(resultadoOperacion.getCoberturasAmparadasDependientes());
				
				if(!StringUtil.isEmpty(coberturasAmparadas) ){
					mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
							"configuracion.tipopoliza.paquete.exito.descontratarBasica.amparadasDescontratadas", 
							coberturasAmparadas);
				}
			}
			else if(resultadoOperacion.getClaveResultado().shortValue() ==
				ActualizacionCoberturaPaqueteDTO.CLAVE_COBERTURAS_SUBLIMITE_DESCONTRATADAS){
				
				String coberturasSubLimite = obtenerDescripcionListaCoberturas(resultadoOperacion.getCoberturasSubLimiteDependientes());
				
				if(!StringUtil.isEmpty(coberturasSubLimite)){
					mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
							"configuracion.tipopoliza.paquete.exito.descontratarBasica.sublimiteDescontratadas", 
							coberturasSubLimite);
				}
			}
			else if (resultadoOperacion.getClaveResultado().shortValue() ==
				ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_COBERTURA_BASICA_NO_CONTRATADA){
				
				String descCoberturaBasica = "No disponible";
				
				if(resultadoOperacion.getCoberturaSeccionBasica() != null && 
						resultadoOperacion.getCoberturaSeccionBasica().getCoberturaSeccionDTO() != null)
					descCoberturaBasica = resultadoOperacion.getCoberturaSeccionBasica().getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion();
				
				mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
						"configuracion.tipopoliza.paquete.error.coberturaBasicaNoContratada", 
						descCoberturaBasica);
			}
			else if (resultadoOperacion.getClaveResultado().shortValue() ==
				ActualizacionCoberturaPaqueteDTO.CLAVE_NO_EXITOSO_REGISTRO_SA_COBERTURA_BASICA_RANGOS){

				mensajeError = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, 
						"configuracion.tipopoliza.paquete.error.cambioSACobBasicaFueraDelRango", 
						(resultadoOperacion.getValorMinimoSumaAseguradaSublimite() != null ? resultadoOperacion.getValorMinimoSumaAseguradaSublimite() : "cualquiera"),
						(resultadoOperacion.getValorMaximoSumaAseguradaSublimite() != null ? resultadoOperacion.getValorMaximoSumaAseguradaSublimite() : "cualquiera"));
			}

			resultadoOperacion.setMensajeError(mensajeError);
		}
	}
	
	public static String obtenerDescripcionListaCoberturas(List<CoberturaSeccionDTO> listaCoberturas){
		String descripcionCoberturas = "";
		StringBuilder descripcion= new StringBuilder();
		if(listaCoberturas != null && !listaCoberturas.isEmpty()){
			for(CoberturaSeccionDTO coberturaSubLimite : listaCoberturas){
				descripcion.append(coberturaSubLimite.getCoberturaDTO().getNombreComercial()).append(", ");
			}
			descripcionCoberturas = descripcion.toString();
			descripcionCoberturas = descripcionCoberturas.substring(0, descripcionCoberturas.length()-2);
		}
		return descripcionCoberturas;
	}
}
