package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ExclusionAumentoVarioCoberturaDTO.
 * @see .ExclusionAumentoVarioCoberturaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ExclusionAumentoVarioCoberturaFacade  implements ExclusionAumentoVarioCoberturaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ExclusionAumentoVarioCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ExclusionAumentoVarioCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ExclusionAumentoVarioCoberturaDTO entity) {
    				LogDeMidasEJB3.log("saving ExclusionAumentoVarioCoberturaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ExclusionAumentoVarioCoberturaDTO entity.
	  @param entity ExclusionAumentoVarioCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ExclusionAumentoVarioCoberturaDTO entity) {
    				LogDeMidasEJB3.log("deleting ExclusionAumentoVarioCoberturaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ExclusionAumentoVarioCoberturaDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ExclusionAumentoVarioCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExclusionAumentoVarioCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExclusionAumentoVarioCoberturaDTO entity to update
	 @return ExclusionAumentoVarioCoberturaDTO the persisted ExclusionAumentoVarioCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ExclusionAumentoVarioCoberturaDTO update(ExclusionAumentoVarioCoberturaDTO entity) {
    				LogDeMidasEJB3.log("updating ExclusionAumentoVarioCoberturaDTO instance", Level.INFO, null);
	        try {
            ExclusionAumentoVarioCoberturaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ExclusionAumentoVarioCoberturaDTO findById( ExclusionAumentoVarioCoberturaId id) {
    				LogDeMidasEJB3.log("finding ExclusionAumentoVarioCoberturaDTO instance with id: " + id, Level.INFO, null);
	        try {
            ExclusionAumentoVarioCoberturaDTO instance = entityManager.find(ExclusionAumentoVarioCoberturaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ExclusionAumentoVarioCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ExclusionAumentoVarioCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<ExclusionAumentoVarioCoberturaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ExclusionAumentoVarioCoberturaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ExclusionAumentoVarioCoberturaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ExclusionAumentoVarioCoberturaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ExclusionAumentoVarioCoberturaDTO entities.
	  	  @return List<ExclusionAumentoVarioCoberturaDTO> all ExclusionAumentoVarioCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionAumentoVarioCoberturaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ExclusionAumentoVarioCoberturaDTO instances", Level.INFO, null);
			try {
			    		final String queryString = "select model from ExclusionAumentoVarioCoberturaDTO model";
					Query query = entityManager.createQuery(queryString);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find a ExclusionAumentoVarioCoberturaDTO entity with the specific
	 * received id�s.
	 * 
	 * @param BigDecimal idToCobertura.
	 * @param BigDecimal idToAumentoVario.
	 * @param BigDecimal idToRiesgo.
	 * 
	 * @return List<ExclusionAumentoVarioCoberturaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionAumentoVarioCoberturaDTO> findByIDs(BigDecimal idToCobertura, BigDecimal idToAumentoVario, BigDecimal idToRiesgo) {
		LogDeMidasEJB3.log("finding ExclusionAumentoVarioCoberturaDTO instance with idtocobertura: "+ idToCobertura + ", idToAumentoVario: "+idToAumentoVario+"idToRiesgo: "+idToRiesgo, Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionAumentoVarioCoberturaDTO model where model.id.idtocobertura = :cobertura and model.id.idtoaumentovario = :idtoaumentovario and model.id.idtoriesgo = :idtoriesgo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("cobertura", idToCobertura);
			query.setParameter("idtoaumentovario", idToAumentoVario);
			query.setParameter("idtoriesgo", idToRiesgo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Encuentra los registros de ExclusionAumentoVarioCoberturaDTO relacionados con la CoberturaSeccionDTO cuyos ID�s se reciben y que adem�s est�n 
	 * relacionados s�lo con Riesgos que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToCobertura. El ID de la cobertura.
	  @param BigDecimal idToSeccion. El ID de la seccion.
	  @return List<ExclusionAumentoVarioCoberturaDTO> encontrados por el query formado.
	 */
    @SuppressWarnings("unchecked")
    public List<ExclusionAumentoVarioCoberturaDTO> getVigentesPorIdCoberturaIdSeccion(BigDecimal idToCobertura,BigDecimal idToSeccion) {
    	LogDeMidasEJB3.log("encontrando ExclusionAumentoVarioCoberturaDTO relacionadas con la cobertura: "+idToCobertura+" perteneciente a la secci�n: "+idToSeccion+"y con Riesgos Vigentes", Level.INFO, null);
			try {
			final String queryString = "select model from ExclusionAumentoVarioCoberturaDTO model where model.id.idtocobertura = :idToCobertura and" +
					"(model.riesgoDTO.claveEstatus <> 3 ) and "+
					"(model.id.idtocobertura in (select cobSeccion.id.idtocobertura from CoberturaSeccionDTO cobSeccion " +
					"where cobSeccion.id.idtoseccion = :idToSeccion))";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCobertura", idToCobertura);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
}