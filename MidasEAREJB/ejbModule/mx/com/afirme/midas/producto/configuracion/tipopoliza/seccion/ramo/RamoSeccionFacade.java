package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity CoberturaSeccionDTO.
 * @see .CoberturaSeccionDTO
  * @author Jos� Luis Arellano 
 */
@Stateless
public class RamoSeccionFacade implements RamoSeccionFacadeRemote{
	@PersistenceContext private EntityManager entityManager;
	/**
 	Perform an initial save of a previously unsaved RamoSeccionDTO entity. 
 	All subsequent persist actions of this entity should use the #update() method.
  	@param entity RamoSeccionDTO entity to persist
  	@throws RuntimeException when the operation fails
	 */
	public void save(RamoSeccionDTO entity) {
		LogDeMidasEJB3.log("saving RamoSeccionDTO instance", Level.INFO, null);
        try {
        	entityManager.persist(entity);
        	LogDeMidasEJB3.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
    		LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
        throw re;
        }
	}

	/**
 	Delete a persistent RamoSeccionDTO entity.
  	@param entity RamoSeccionDTO entity to delete
 	@throws RuntimeException when the operation fails
	 */
	public void delete(RamoSeccionDTO entity) {
		LogDeMidasEJB3.log("deleting RamoSeccionDTO instance", Level.INFO, null);
        try {
        	entity = entityManager.getReference(RamoSeccionDTO.class, entity.getId());
        	entityManager.remove(entity);
        	LogDeMidasEJB3.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
    		LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
            throw re;
        }
	}

	/**
 	Persist a previously saved RamoSeccionDTO entity and return it or a copy of it to the sender. 
 	A copy of the RamoSeccionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
  	@param entity RamoSeccionDTO entity to update
 	@return RamoSeccionDTO the persisted RamoSeccionDTO entity instance, may not be the same
 	@throws RuntimeException if the operation fails
	 */
	public RamoSeccionDTO update(RamoSeccionDTO entity) {
		LogDeMidasEJB3.log("updating RamoSeccionDTO instance", Level.INFO, null);
        try {
        	RamoSeccionDTO result = entityManager.merge(entity);
        	LogDeMidasEJB3.log("update successful", Level.INFO, null);
            return result;
        } catch (RuntimeException re) {
    		LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
            throw re;
        }
	}

	public RamoSeccionDTO findById( RamoSeccionId id) {
		LogDeMidasEJB3.log("finding RamoSeccionDTO instance with id: " + id, Level.INFO, null);
        try {
        	RamoSeccionDTO instance = entityManager.find(RamoSeccionDTO.class, id);
        	return instance;
        } catch (RuntimeException re) {
    		LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
            throw re;
        }
	}

	/**
	 * Find all RamoSeccionDTO entities with a specific property value.  
  	@param propertyName the name of the RamoSeccionDTO property to query
  	@param value the property value to match
  	@return List<RamoSeccionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RamoSeccionDTO> findByProperty(String propertyName, final Object value) {
		LogDeMidasEJB3.log("finding RamoSeccionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RamoSeccionDTO model where model." 
		 				+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RamoSeccionDTO entities.
  	 @return List<RamoSeccionDTO> all RamoSeccionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RamoSeccionDTO> findAll() {
		LogDeMidasEJB3.log("finding all RamoSeccionDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from RamoSeccionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Lista todos los ramos que no est�n asociadas a un producto determinada 
	 * por el id que recibe el m�todo
	 * @param BigDecimal idToSeccion el id de la seccion.
	 * @return List<RamoProductoDTO> registros RamoProducto que no est�n asociados a la seccion.
	 */
	@SuppressWarnings("unchecked")
	public List<RamoSeccionDTO> obtenerRamosSinAsociar(BigDecimal idToSeccion){
		LogDeMidasEJB3.log("Encontrando los ramos no asociados a la seccion: " + idToSeccion, Level.INFO, null);
		try {
			final String queryString = "select model from RamoSeccionDTO model where " +
				"model.id.idtoseccion <> :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fallo en encontrar las coberturas no asiciadas a la seccion: "+idToSeccion, Level.SEVERE, re);
			throw re;
		}
	}
}
