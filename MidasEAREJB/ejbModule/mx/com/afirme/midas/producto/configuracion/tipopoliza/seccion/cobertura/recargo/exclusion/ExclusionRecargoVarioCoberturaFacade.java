package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ExclusionRecargoVarioCoberturaDTO.
 * 
 * @see .ExclusionRecargoVarioCoberturaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ExclusionRecargoVarioCoberturaFacade implements
		ExclusionRecargoVarioCoberturaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * ExclusionRecargoVarioCoberturaDTO entity. All subsequent persist actions
	 * of this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ExclusionRecargoVarioCoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ExclusionRecargoVarioCoberturaDTO entity) {
		LogDeMidasEJB3.log("saving ExclusionRecargoVarioCoberturaDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ExclusionRecargoVarioCoberturaDTO entity.
	 * 
	 * @param entity
	 *            ExclusionRecargoVarioCoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ExclusionRecargoVarioCoberturaDTO entity) {
		LogDeMidasEJB3.log(
				"deleting ExclusionRecargoVarioCoberturaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					ExclusionRecargoVarioCoberturaDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ExclusionRecargoVarioCoberturaDTO entity and
	 * return it or a copy of it to the sender. A copy of the
	 * ExclusionRecargoVarioCoberturaDTO entity parameter is returned when the
	 * JPA persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            ExclusionRecargoVarioCoberturaDTO entity to update
	 * @return ExclusionRecargoVarioCoberturaDTO the persisted
	 *         ExclusionRecargoVarioCoberturaDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ExclusionRecargoVarioCoberturaDTO update(
			ExclusionRecargoVarioCoberturaDTO entity) {
		LogDeMidasEJB3.log(
				"updating ExclusionRecargoVarioCoberturaDTO instance",
				Level.INFO, null);
		try {
			ExclusionRecargoVarioCoberturaDTO result = entityManager
					.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ExclusionRecargoVarioCoberturaDTO findById(
			ExclusionRecargoVarioCoberturaId id) {
		LogDeMidasEJB3.log(
				"finding ExclusionRecargoVarioCoberturaDTO instance with id: "
						+ id, Level.INFO, null);
		try {
			ExclusionRecargoVarioCoberturaDTO instance = entityManager.find(
					ExclusionRecargoVarioCoberturaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ExclusionRecargoVarioCoberturaDTO entities with a specific
	 * property value.
	 * 
	 * @param propertyName
	 *            the name of the ExclusionRecargoVarioCoberturaDTO property to
	 *            query
	 * @param value
	 *            the property value to match
	 * @return List<ExclusionRecargoVarioCoberturaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionRecargoVarioCoberturaDTO> findByProperty(
			String propertyName, final Object value) {
		LogDeMidasEJB3.log(
				"finding ExclusionRecargoVarioCoberturaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionRecargoVarioCoberturaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ExclusionRecargoVarioCoberturaDTO entities.
	 * 
	 * @return List<ExclusionRecargoVarioCoberturaDTO> all
	 *         ExclusionRecargoVarioCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionRecargoVarioCoberturaDTO> findAll() {
		LogDeMidasEJB3.log(
				"finding all ExclusionRecargoVarioCoberturaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionRecargoVarioCoberturaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Encuentra los registros de ExclusionRecargoVarioCoberturaDTO relacionados con la CoberturaSeccionDTO cuyos ID�s se reciben y que adem�s est�n 
	 * relacionados s�lo con Riesgos que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToCobertura. El ID de la cobertura.
	  @param BigDecimal idToSeccion. El ID de la seccion.
	  @return List<ExclusionRecargoVarioCoberturaDTO> encontrados por el query formado.
	 */
    @SuppressWarnings("unchecked")
    public List<ExclusionRecargoVarioCoberturaDTO> getVigentesPorIdCoberturaIdSeccion(BigDecimal idToCobertura,BigDecimal idToSeccion) {
    	LogDeMidasEJB3.log("encontrando ExclusionRecargoVarioCoberturaDTO relacionadas con la cobertura: "+idToCobertura+" perteneciente a la secci�n: "+idToSeccion+"y con Riesgos Vigentes", Level.INFO, null);
			try {
			final String queryString = "select model from ExclusionRecargoVarioCoberturaDTO model where model.id.idtocobertura = :idToCobertura and" +
					"(model.riesgoDTO.claveEstatus <> 3 ) and "+
					"(model.id.idtocobertura in (select cobSeccion.id.idtocobertura from CoberturaSeccionDTO cobSeccion " +
					"where cobSeccion.id.idtoseccion = :idToSeccion))";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCobertura", idToCobertura);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
}