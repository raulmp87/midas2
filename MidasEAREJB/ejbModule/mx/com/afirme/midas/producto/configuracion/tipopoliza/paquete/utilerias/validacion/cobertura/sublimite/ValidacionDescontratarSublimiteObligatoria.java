package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.sublimite;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.ActualizacionCoberturaPaqueteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.PaquetePolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.PaquetePolizaFacade;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.ValidadorModificacionCoberturaPaquete;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.cobertura.CoberturaSeccionPaqueteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;

public class ValidacionDescontratarSublimiteObligatoria extends ValidadorModificacionCoberturaPaquete{

	public ValidacionDescontratarSublimiteObligatoria(
			PaquetePolizaFacade paquetePolizaFacade) {
		super(paquetePolizaFacade);
	}

	@Override
	public ActualizacionCoberturaPaqueteDTO validarModificacionCoberturaPaquete(
			CoberturaSeccionDTO coberturaSeccionDTO,
			CoberturaSeccionPaqueteDTO coberturaPaqueteDTO,
			PaquetePolizaDTO paquetePolizaDTO) {
		
		return excluirCoberturaObligatoria(coberturaSeccionDTO, coberturaPaqueteDTO, paquetePolizaDTO);
	}

}
