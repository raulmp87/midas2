package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaId;

/**
 * Facade for entity CoaseguroCoberturaDTO.
 * 
 * @see .CoaseguroCoberturaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CoaseguroCoberturaFacade implements CoaseguroCoberturaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved CoaseguroCoberturaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CoaseguroCoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CoaseguroCoberturaDTO entity) {
		LogDeMidasEJB3.log("saving CoaseguroCoberturaDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent CoaseguroCoberturaDTO entity.
	 * 
	 * @param entity
	 *            CoaseguroCoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoaseguroCoberturaDTO entity) {
		LogDeMidasEJB3.log("deleting CoaseguroCoberturaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(CoaseguroCoberturaDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved CoaseguroCoberturaDTO entity and return it or
	 * a copy of it to the sender. A copy of the CoaseguroCoberturaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CoaseguroCoberturaDTO entity to update
	 * @return CoaseguroCoberturaDTO the persisted CoaseguroCoberturaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoaseguroCoberturaDTO update(CoaseguroCoberturaDTO entity) {
		LogDeMidasEJB3.log("updating CoaseguroCoberturaDTO instance",
				Level.INFO, null);
		try {
			CoaseguroCoberturaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CoaseguroCoberturaDTO findById(CoaseguroCoberturaId id) {
		LogDeMidasEJB3.log("finding CoaseguroCoberturaDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			CoaseguroCoberturaDTO instance = entityManager.find(
					CoaseguroCoberturaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoaseguroCoberturaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoaseguroCoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoaseguroCoberturaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CoaseguroCoberturaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding CoaseguroCoberturaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from CoaseguroCoberturaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoaseguroCoberturaDTO entities.
	 * 
	 * @return List<CoaseguroCoberturaDTO> all CoaseguroCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CoaseguroCoberturaDTO> findAll() {
		LogDeMidasEJB3.log("finding all CoaseguroCoberturaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from CoaseguroCoberturaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoaseguroCoberturaDTO> listarCoaseguros(BigDecimal idToCobertura) {
		String queryString = "select model from CoaseguroCoberturaDTO model where model.id.idToCobertura = :idToCobertura order by model.valor";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCobertura", idToCobertura);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<CoaseguroCoberturaDTO> listarCoasegurosPorCobertura(BigDecimal idToCobertura) {
		String queryString = "select model from CoaseguroCoberturaDTO model " +
				"where model.id.idToCobertura = :idToCobertura " +
				"and model.claveDefault = 1";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCobertura", idToCobertura);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
	
	public BigDecimal nextNumeroSecuencia(BigDecimal idToCobertura) {
		String queryString = "select max(model.id.numeroSecuencia) from CoaseguroCoberturaDTO model " +
				"where model.id.idToCobertura = :idToCobertura " +
				"";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCobertura", idToCobertura);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		double valor = 0;
		BigDecimal next = (BigDecimal)query.getSingleResult();
		if (next != null){
		valor = next.doubleValue();
		valor++;
		}else{
			valor = 1;
		}
		return new BigDecimal(valor);
	}
}