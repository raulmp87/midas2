package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity CoberturaDTO.
 * 
 * @see .CoberturaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CoberturaFacade implements CoberturaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved CoberturaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            CoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CoberturaDTO entity) {
		LogDeMidasEJB3.log("saving CoberturaDTO instance", Level.INFO, null);
		try {
			if (entity.getIdToCobertura() == null) {
				entity.setVersion(Integer.valueOf(1));
			}
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent CoberturaDTO entity.
	 * 
	 * @param entity
	 *            CoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoberturaDTO entity) {
		LogDeMidasEJB3.log("deleting CoberturaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(CoberturaDTO.class, entity
					.getIdToCobertura());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved CoberturaDTO entity and return it or a copy of
	 * it to the sender. A copy of the CoberturaDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            CoberturaDTO entity to update
	 * @return CoberturaDTO the persisted CoberturaDTO entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoberturaDTO update(CoberturaDTO entity) {
		LogDeMidasEJB3.log("updating CoberturaDTO instance", Level.INFO, null);
		try {
			CoberturaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CoberturaDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding CoberturaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			CoberturaDTO instance = entityManager.find(CoberturaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoberturaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoberturaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding CoberturaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from CoberturaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoberturaDTO entities.
	 * 
	 * @return List<CoberturaDTO> all CoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaDTO> findAll() {
		LogDeMidasEJB3.log("finding all CoberturaDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from CoberturaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoberturaDTO entities with depends of a filter.
	 * 
	 * @param entity
	 *            the name of the entity CoberturaDTO

	 * @return CoberturaDTO found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaDTO> listarFiltrado(CoberturaDTO entity, Boolean mostrarInactivos) {
		try {
			String queryString = "select model from CoberturaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "codigo", entity.getCodigo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "version", entity.getVersion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcion", entity.getDescripcion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "nombreComercial", entity.getNombreComercial());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "claveTipoSumaAsegurada", entity.getClaveTipoSumaAsegurada());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "subRamoDTO.idTcSubRamo", entity.getSubRamoDTO().getIdTcSubRamo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveDesglosaRiesgos", entity.getClaveDesglosaRiesgos());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveImporteCero", entity.getClaveImporteCero());
//			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveActivoConfiguracion", entity.getClaveActivoConfiguracion());
//			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "claveActivoProduccion", entity.getClaveActivoProduccion());
			//sWhere = sWhere + " AND (model.claveActivoConfiguracion <> 0 OR model.claveActivoProduccion <> 0)";
			if(!mostrarInactivos) {
				sWhere += " and model.claveEstatus <> :claveEstatus and model.claveActivo <> :claveActivo";
				Utilerias.agregaHashLista(listaParametrosValidos, "claveEstatus", 0);
				Utilerias.agregaHashLista(listaParametrosValidos, "claveActivo", 0);
			}
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
		
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		}catch (RuntimeException re){
			LogDeMidasEJB3.log("listarFiltrado failed", Level.SEVERE, re);
			throw re;			
		}
	}
	
	/**
	 * Borra l�gicamente un registro de Cobertura, estableciendo en 0 los campos 
	 * CLAVEACTIVO y CLAVEESTATUS
	 * @param entity. CoberturaDTO entity el registro Cobertura a borrar
	 * @throws RuntimeException si la operacion falla
	 */
	public CoberturaDTO borradoLogico(CoberturaDTO entity){
		LogDeMidasEJB3.log("logical delete CoberturaDTO instance", Level.INFO, null);
		try {
			/*entity.setClaveActivoConfiguracion("0");
			entity.setClaveActivoProduccion("0");*/
			entity.setClaveActivo(new Short("0"));
			entity.setClaveEstatus(new Short("0"));
			CoberturaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("logical delete successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("logical delete failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Busca los registros de Cobertura que no hayan sido borrados logicamente
	 * 
	 * @return List<CoberturaDTO> registros de Cobertura que no han sido borrados logicamente
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaDTO> listarVigentes(){
		LogDeMidasEJB3.log("finding all active CoberturaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from CoberturaDTO model where model.claveActivo <> 0 and model.claveEstatus <> 0 and " +
					"model.claveDesglosaRiesgos = 0 and model.claveImporteCero = 0";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all active failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaDTO> listarCoberturasPorExcluir(BigDecimal idToCobertura, BigDecimal idToSeccion) {
		String queryString = "select distinct model.coberturaDTO from CoberturaSeccionDTO as model ";
		//queryString += "where model.id.idtoseccion = :idToSeccion and model.id.idtocobertura <> :idToCobertura ";
		queryString += "where model.id.idtocobertura <> :idToCobertura ";
		queryString += "and model.id.idtocobertura not in ";
		queryString += "(select excluidas.id.idToCoberturaExcluida from CoberturaExcluidaDTO excluidas  where excluidas.id.idToCobertura = :idToCobertura) ";
		queryString += "and model.id.idtocobertura not in ";
		queryString += "(select requeridas.id.idToCoberturaRequerida from CoberturaRequeridaDTO requeridas where requeridas.id.idToCobertura = :idToCobertura)";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCobertura", idToCobertura);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		//query.setParameter("idToSeccion", idToSeccion);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<CoberturaDTO> listarCoberturasPorAsociarSeccion(BigDecimal idToSeccion) {
		String queryString = "select model from CoberturaDTO as model where model.subRamoDTO.idTcSubRamo in " +
		 "( select subramo.idTcSubRamo from SubRamoDTO subramo where subramo.ramoDTO.idTcRamo in " +
		 "( select ramo.idTcRamo from RamoDTO ramo where ramo.idTcRamo in " +
		 "( select ramoseccion.id.idtcramo from RamoSeccionDTO ramoseccion where ramoseccion.id.idtoseccion = :idToSeccion)) )" +
		 "and model.claveActivo <> 0 and (model.idToCobertura not in (select cobSec.id.idtocobertura from " +
		 "CoberturaSeccionDTO cobSec where cobSec.id.idtoseccion = :idToSeccion ))";
		/*"select model from CoberturaSeccionDTO model where model.id.idtoseccion = :idToSeccion and (" +
		"model.coberturaDTO.claveActivo <> 0 )";*/
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToSeccion", idToSeccion);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaDTO> listarCoberturasNoRequeridas(BigDecimal idToCobertura,
			BigDecimal idToSeccion) {
		String queryString = "select distinct model.coberturaDTO from CoberturaSeccionDTO as model ";
		//queryString += "where model.id.idtoseccion = :idToSeccion and model.id.idtocobertura <> :idToCobertura ";
		queryString += "where model.id.idtocobertura <> :idToCobertura ";
		queryString += "and model.id.idtocobertura not in ";
		queryString += "(select excluidas.id.idToCoberturaExcluida from CoberturaExcluidaDTO excluidas where excluidas.id.idToCobertura = :idToCobertura) ";
		queryString += "and model.id.idtocobertura not in ";
		queryString += "(select requeridas.id.idToCoberturaRequerida from CoberturaRequeridaDTO requeridas where requeridas.id.idToCobertura = :idToCobertura)";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCobertura", idToCobertura);
		//query.setParameter("idToSeccion", idToSeccion);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
	
	/**
	 * Busca los registros de Cobertura que no hayan sido borrados logicamente y 
	 * que est�n relacionados con las secciones activas del TipoP�liza cuyo ID se recibe 
	 * @param idToTipoPoliza
	 * @return List<CoberturaDTO> registros de Cobertura que no han sido borrados logicamente
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaDTO> listarVigentesPorTipoPoliza(BigDecimal idToTipoPoliza){
		LogDeMidasEJB3.log("buscando los registros CoberturaDTO activos relacionados con las secciones vigentes del TipoPoliza: "+idToTipoPoliza, Level.INFO, null);
		try {
			/*final String queryString = "select model from CoberturaDTO model where (model.claveActivoConfiguracion<> 0 or model.claveActivoProduccion <> 0)" +"and (model.idToCobertura in (select cobSec.id.idtocobertura from CoberturaSeccionDTO cobSec where cobSec.id.idtoseccion in" +"(select sec.idToSeccion from SeccionDTO sec where (sec.claveActivoConfiguracion <> 0 or sec.claveActivoProduccion <> 0) and " +"sec.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza)))";*/
			final String queryString = "select distinct model from CoberturaDTO model where (model.claveActivo <> 0)" +
				"and (model.idToCobertura in (select cobSec.id.idtocobertura from CoberturaSeccionDTO cobSec where cobSec.id.idtoseccion in" +
				"(select sec.idToSeccion from SeccionDTO sec where (sec.claveActivo <> 0) and " +
				"sec.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza)))";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all active failed", Level.SEVERE, re);
			throw re;
		}
	}
}