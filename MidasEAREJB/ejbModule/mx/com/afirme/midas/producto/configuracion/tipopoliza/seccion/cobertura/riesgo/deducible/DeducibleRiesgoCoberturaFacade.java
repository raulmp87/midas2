package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity DeducibleRiesgoCoberturaDTO.
 * 
 * @see .DeducibleRiesgoCoberturaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class DeducibleRiesgoCoberturaFacade implements
		DeducibleRiesgoCoberturaFacadeRemote {
	// property constants
	public static final String VALOR = "valor";
	public static final String CLAVE_DEFAULT = "claveDefault";

	@PersistenceContext
	private EntityManager entityManager;
	@Resource
	private SessionContext context;

	/**
	 * Perform an initial save of a previously unsaved
	 * DeducibleRiesgoCoberturaDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            DeducibleRiesgoCoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DeducibleRiesgoCoberturaDTO entity) {
		LogDeMidasEJB3.log("saving DeducibleRiesgoCoberturaDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent DeducibleRiesgoCoberturaDTO entity.
	 * 
	 * @param entity
	 *            DeducibleRiesgoCoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DeducibleRiesgoCoberturaDTO entity) {
		LogDeMidasEJB3.log("deleting DeducibleRiesgoCoberturaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					DeducibleRiesgoCoberturaDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved DeducibleRiesgoCoberturaDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * DeducibleRiesgoCoberturaDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            DeducibleRiesgoCoberturaDTO entity to update
	 * @return DeducibleRiesgoCoberturaDTO the persisted
	 *         DeducibleRiesgoCoberturaDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DeducibleRiesgoCoberturaDTO update(DeducibleRiesgoCoberturaDTO entity) {
		LogDeMidasEJB3.log("updating DeducibleRiesgoCoberturaDTO instance",
				Level.INFO, null);
		try {
			DeducibleRiesgoCoberturaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DeducibleRiesgoCoberturaDTO findById(DeducibleRiesgoCoberturaId id) {
		LogDeMidasEJB3.log(
				"finding DeducibleRiesgoCoberturaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			DeducibleRiesgoCoberturaDTO instance = entityManager.find(
					DeducibleRiesgoCoberturaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DeducibleRiesgoCoberturaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the DeducibleRiesgoCoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DeducibleRiesgoCoberturaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<DeducibleRiesgoCoberturaDTO> findByProperty(
			String propertyName, final Object value) {
		LogDeMidasEJB3.log(
				"finding DeducibleRiesgoCoberturaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from DeducibleRiesgoCoberturaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<DeducibleRiesgoCoberturaDTO> findByValor(Object valor) {
		return findByProperty(VALOR, valor);
	}

	public List<DeducibleRiesgoCoberturaDTO> findByClaveDefault(
			Object claveDefault) {
		return findByProperty(CLAVE_DEFAULT, claveDefault);
	}

	/**
	 * Find all DeducibleRiesgoCoberturaDTO entities.
	 * 
	 * @return List<DeducibleRiesgoCoberturaDTO> all DeducibleRiesgoCoberturaDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<DeducibleRiesgoCoberturaDTO> findAll() {
		LogDeMidasEJB3.log("finding all DeducibleRiesgoCoberturaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from DeducibleRiesgoCoberturaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DeducibleRiesgoCoberturaDTO> findByRiesgoCoberturaSeccion(
			BigDecimal idToRiesgo, BigDecimal idToCobertura,
			BigDecimal idToSeccion) {
		LogDeMidasEJB3.log(
				"finding all DeducibleRiesgoCoberturaDTO instances with idToRiesgo: "
						+ idToRiesgo + " and idToCobertura: " + idToCobertura
						+ " and idToSeccion: " + idToSeccion, Level.INFO, null);
		try {
			final String queryString = "select model from DeducibleRiesgoCoberturaDTO model where model.id.idToRiesgo = :idToRiesgo and model.id.idToCobertura = :idToCobertura and model.id.idToSeccion = :idToSeccion order by model.valor";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToRiesgo", idToRiesgo);
			query.setParameter("idToCobertura", idToCobertura);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void synchronizeDeduciblesRiesgoCobertura(BigDecimal idToRiesgo,
			BigDecimal idToCobertura, BigDecimal idToSeccion) {
		LogDeMidasEJB3.log(
				"synchronizing DeducibleRiesgoCoberturaDTO instances with idToRiesgo: "
						+ idToRiesgo + " and idToCobertura: " + idToCobertura
						+ " and idToSeccion: " + idToSeccion, Level.INFO, null);
		try {
			String queryString = "select model from DeducibleCoberturaDTO model where model.id.idToCobertura = :idToCobertura " +
					"and model.id.numeroSecuencia not in (select riesgo.id.numeroSecuencia from DeducibleRiesgoCoberturaDTO riesgo " +
					"where riesgo.id.idToRiesgo = :idToRiesgo and riesgo.id.idToCobertura = :idToCobertura and riesgo.id.idToSeccion = :idToSeccion)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToRiesgo", idToRiesgo);
			query.setParameter("idToCobertura", idToCobertura);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			List<DeducibleCoberturaDTO> deduciblesCobertura = (List<DeducibleCoberturaDTO>)query.getResultList();
			
			if(!deduciblesCobertura.isEmpty()) {
				for(DeducibleCoberturaDTO deducibleCobertura : deduciblesCobertura) {
					DeducibleRiesgoCoberturaId id = new DeducibleRiesgoCoberturaId();
					id.setIdToRiesgo(idToRiesgo);
					id.setIdToCobertura(idToCobertura);
					id.setIdToSeccion(idToSeccion);
					id.setNumeroSecuencia(deducibleCobertura.getId().getNumeroSecuencia());

					DeducibleRiesgoCoberturaDTO deducibleRiesgoCobertura = new DeducibleRiesgoCoberturaDTO();
					deducibleRiesgoCobertura.setId(id);
					deducibleRiesgoCobertura.setClaveDefault(deducibleCobertura.getClaveDefault());
					deducibleRiesgoCobertura.setValor(deducibleCobertura.getValor());
					this.save(deducibleRiesgoCobertura);
				}
			}
			queryString = "select model from DeducibleRiesgoCoberturaDTO model where model.id.idToRiesgo = :idToRiesgo " +
					"and model.id.idToCobertura = :idToCobertura and model.id.idToSeccion = :idToSeccion " +
					"and model.id.numeroSecuencia not in (select cobertura.id.numeroSecuencia from DeducibleCoberturaDTO cobertura " +
					"where cobertura.id.idToCobertura = :idToCobertura)";
			query = entityManager.createQuery(queryString);
			query.setParameter("idToRiesgo", idToRiesgo);
			query.setParameter("idToCobertura", idToCobertura);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			List<DeducibleRiesgoCoberturaDTO> deduciblesRiesgoCobertura = (List<DeducibleRiesgoCoberturaDTO>)query.getResultList();
			
			if(!deduciblesRiesgoCobertura.isEmpty()) {
				for(DeducibleRiesgoCoberturaDTO deducibleRiesgoCobertura : deduciblesRiesgoCobertura) {
					this.delete(deducibleRiesgoCobertura);
				}
			}
		} catch (RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("synchronizing failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DeducibleRiesgoCoberturaDTO> listarFiltrado(DeducibleRiesgoCoberturaDTO entity) {
		LogDeMidasEJB3.log(
				"filtering DeducibleRiesgoCoberturaDTO instance",
						Level.INFO, null);
		try {
			String queryString = "select model from DeducibleRiesgoCoberturaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToSeccion", entity.getId().getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCobertura", entity.getId().getIdToCobertura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToRiesgo", entity.getId().getIdToRiesgo());
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
}