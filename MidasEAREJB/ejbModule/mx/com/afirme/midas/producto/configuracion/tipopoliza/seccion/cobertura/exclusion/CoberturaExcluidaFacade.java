package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity CoberturaExcluidaDTO.
 * 
 * @see .CoberturaExcluidaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CoberturaExcluidaFacade implements CoberturaExcluidaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved CoberturaExcluidaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CoberturaExcluidaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CoberturaExcluidaDTO entity) {
		LogDeMidasEJB3.log("saving CoberturaExcluidaDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent CoberturaExcluidaDTO entity.
	 * 
	 * @param entity
	 *            CoberturaExcluidaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoberturaExcluidaDTO entity) {
		LogDeMidasEJB3.log("deleting CoberturaExcluidaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(CoberturaExcluidaDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved CoberturaExcluidaDTO entity and return it or a
	 * copy of it to the sender. A copy of the CoberturaExcluidaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CoberturaExcluidaDTO entity to update
	 * @return CoberturaExcluidaDTO the persisted CoberturaExcluidaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoberturaExcluidaDTO update(CoberturaExcluidaDTO entity) {
		LogDeMidasEJB3.log("updating CoberturaExcluidaDTO instance",
				Level.INFO, null);
		try {
			CoberturaExcluidaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CoberturaExcluidaDTO findById(CoberturaExcluidaId id) {
		LogDeMidasEJB3.log("finding CoberturaExcluidaDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			CoberturaExcluidaDTO instance = entityManager.find(
					CoberturaExcluidaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoberturaExcluidaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoberturaExcluidaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoberturaExcluidaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaExcluidaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding CoberturaExcluidaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from CoberturaExcluidaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoberturaExcluidaDTO entities.
	 * 
	 * @return List<CoberturaExcluidaDTO> all CoberturaExcluidaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaExcluidaDTO> findAll() {
		LogDeMidasEJB3.log("finding all CoberturaExcluidaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from CoberturaExcluidaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}