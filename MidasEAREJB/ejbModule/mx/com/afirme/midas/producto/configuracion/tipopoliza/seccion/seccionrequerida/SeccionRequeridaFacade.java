package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.seccionrequerida;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity SeccionRequeridaDTO.
 * @see .SeccionRequeridaDTO
  * @author Jos� Luis Arellano
 */
@Stateless
public class SeccionRequeridaFacade  implements SeccionRequeridaFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
	/**
	 Perform an initial save of a previously unsaved SeccionRequeridaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SeccionRequeridaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(SeccionRequeridaDTO entity) {
		LogDeMidasEJB3.log("saving SeccionRequeridaDTO instance", Level.INFO, null);
    	try {
    		entityManager.persist(entity);
    		LogDeMidasEJB3.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }
    
    /**
	 Delete a persistent SeccionRequeridaDTO entity.
	  @param entity SeccionRequeridaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SeccionRequeridaDTO entity) {
    	LogDeMidasEJB3.log("deleting SeccionRequeridaDTO instance", Level.INFO, null);
        try {
        	entity = entityManager.getReference(SeccionRequeridaDTO.class, entity.getId());
            entityManager.remove(entity);
            LogDeMidasEJB3.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }
    
    /**
	 Persist a previously saved SeccionRequeridaDTO entity and return it or a copy of it to the sender. 
	 A copy of the SeccionRequeridaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SeccionRequeridaDTO entity to update
	 @return SeccionRequeridaDTO the persisted SeccionRequeridaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public SeccionRequeridaDTO update(SeccionRequeridaDTO entity) {
    	LogDeMidasEJB3.log("updating SeccionRequeridaDTO instance", Level.INFO, null);
        try {
        	SeccionRequeridaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
            return result;
        } catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }
    
    public SeccionRequeridaDTO findById( SeccionRequeridaId id) {
		LogDeMidasEJB3.log("finding SeccionRequeridaDTO instance with id: " + id, Level.INFO, null);
        try {
            SeccionRequeridaDTO instance = entityManager.find(SeccionRequeridaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }    
    

/**
	 * Find all SeccionRequeridaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SeccionRequeridaDTO property to query
	  @param value the property value to match
	  	  @return List<SeccionRequeridaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<SeccionRequeridaDTO> findByProperty(String propertyName, final Object value) {
		LogDeMidasEJB3.log("finding SeccionRequeridaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SeccionRequeridaDTO model where model." 
				+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}	
	
	/**
	 * Find all SeccionRequeridaDTO entities.
	  	  @return List<SeccionRequeridaDTO> all SeccionRequeridaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionRequeridaDTO> findAll() {
		LogDeMidasEJB3.log("finding all SeccionRequeridaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from SeccionRequeridaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find all SeccionRequeridaDTO entities related with SeccionDTO�d id recieved.
  	 * @return List<SeccionRequeridaDTO> all SeccionRequeridaDTO entities related with SeccionDTO�d id recieved.
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionRequeridaDTO> findRequiredSecctions(BigDecimal idToSeccion) {
		LogDeMidasEJB3.log("finding all SeccionRequeridaDTO instancesrelated with SeccionDTO id: "+idToSeccion, Level.INFO, null);
		try {
			final String queryString = "select model from SeccionRequeridaDTO model where model.id.idToSeccion = :idToSeccion or model.id.idSeccionRequerida = :idToSeccionRequerida";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", idToSeccion);
			query.setParameter("idToSeccionRequerida", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
}