package mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity CoberturaSeccionDTO.
 * @see .CoberturaSeccionDTO
  * @author Jos� Luis Arellano 
 */
@Stateless
public class RamoTipoPolizaFacade implements RamoTipoPolizaFacadeRemote{
	@PersistenceContext private EntityManager entityManager;
	/**
 	Perform an initial save of a previously unsaved RamoTipoPolizaDTO entity. 
 	All subsequent persist actions of this entity should use the #update() method.
  	@param entity RamoTipoPolizaDTO entity to persist
  	@throws RuntimeException when the operation fails
	 */
	public void save(RamoTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("saving RamoTipoPolizaDTO instance", Level.INFO, null);
        try {
        	entityManager.persist(entity);
        	LogDeMidasEJB3.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
    		LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
        throw re;
        }
	}

	/**
 	Delete a persistent RamoTipoPolizaDTO entity.
  	@param entity RamoTipoPolizaDTO entity to delete
 	@throws RuntimeException when the operation fails
	 */
	public void delete(RamoTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("deleting RamoTipoPolizaDTO instance", Level.INFO, null);
        try {
        	entity = entityManager.getReference(RamoTipoPolizaDTO.class, entity.getId());
        	entityManager.remove(entity);
        	LogDeMidasEJB3.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
    		LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
            throw re;
        }
	}

	/**
 	Persist a previously saved RamoTipoPolizaDTO entity and return it or a copy of it to the sender. 
 	A copy of the RamoTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
  	@param entity RamoTipoPolizaDTO entity to update
 	@return RamoTipoPolizaDTO the persisted RamoTipoPolizaDTO entity instance, may not be the same
 	@throws RuntimeException if the operation fails
	 */
	public RamoTipoPolizaDTO update(RamoTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("updating RamoTipoPolizaDTO instance", Level.INFO, null);
        try {
        	RamoTipoPolizaDTO result = entityManager.merge(entity);
        	LogDeMidasEJB3.log("update successful", Level.INFO, null);
            return result;
        } catch (RuntimeException re) {
    		LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
            throw re;
        }
	}

	public RamoTipoPolizaDTO findById( RamoTipoPolizaId id) {
		LogDeMidasEJB3.log("finding RamoTipoPolizaDTO instance with id: " + id, Level.INFO, null);
        try {
        	RamoTipoPolizaDTO instance = entityManager.find(RamoTipoPolizaDTO.class, id);
        	return instance;
        } catch (RuntimeException re) {
    		LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
            throw re;
        }
	}

	/**
	 * Find all RamoTipoPolizaDTO entities with a specific property value.  
  	@param propertyName the name of the RamoTipoPolizaDTO property to query
  	@param value the property value to match
  	@return List<RamoTipoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RamoTipoPolizaDTO> findByProperty(String propertyName, final Object value) {
		LogDeMidasEJB3.log("finding RamoTipoPolizaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RamoTipoPolizaDTO model where model." 
		 				+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RamoTipoPolizaDTO entities.
  	 @return List<RamoTipoPolizaDTO> all RamoTipoPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RamoTipoPolizaDTO> findAll() {
		LogDeMidasEJB3.log("finding all RamoTipoPolizaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from RamoTipoPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Lista todos los ramos que no est�n asociadas a un tipode poliza determinado
	 * por el id que recibe el m�todo
	 * @param BigDecimal idToTipoPoliza el id de la seccion.
	 * @return List<RamoTipoPolizaDTO> registros RamoTipoPoliza que no est�n asociados a la seccion.
	 */
	@SuppressWarnings("unchecked")
	public List<RamoTipoPolizaDTO> obtenerRamosSinAsociar(BigDecimal idToTipoPoliza){
		LogDeMidasEJB3.log("Encontrando los ramos no asociados al tipoPoliza: " + idToTipoPoliza, Level.INFO, null);
		try {
			final String queryString = "select model from RamoTipoPolizaDTO model where " +
				"model.id.idtotipopoliza <> :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fallo en encontrar las coberturas no asiciadas a la seccion: "+idToTipoPoliza, Level.SEVERE, re);
			throw re;
		}
	}
}
