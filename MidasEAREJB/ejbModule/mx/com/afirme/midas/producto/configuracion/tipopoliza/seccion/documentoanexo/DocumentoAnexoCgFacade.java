package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.documentoanexo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgCondiciones;
import mx.com.afirme.midas2.domain.condicionesGenerales.DocumentoAnexoCgFacadeRemote;

/**
 * Facade for entity CgCondiciones.
 * @see .CgCondiciones
 * @author MyEclipse Persistence Tools 
 */
@Stateless
public class DocumentoAnexoCgFacade  implements DocumentoAnexoCgFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
	/**
	 Perform an initial save of a previously unsaved CgCondiciones entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	 @param entity CgCondiciones entity to persist
	 @throws RuntimeException when the operation fails
	 */
    public void save(CgCondiciones entity) {
    				LogDeMidasEJB3.log("saving CgCondiciones instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CgCondiciones entity.
	  @param entity CgCondiciones entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CgCondiciones entity) {
    				LogDeMidasEJB3.log("deleting CgCondiciones instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(CgCondiciones.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CgCondiciones entity and return it or a copy of it to the sender. 
	 A copy of the CgCondiciones entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CgCondiciones entity to update
	 @return CgCondiciones the persisted CgCondiciones entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CgCondiciones update(CgCondiciones entity) {
    				LogDeMidasEJB3.log("updating CgCondiciones instance", Level.INFO, null);
	        try {
            CgCondiciones result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public CgCondiciones findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding CgCondiciones instance with id: " + id, Level.INFO, null);
	        try {
            CgCondiciones instance = entityManager.find(CgCondiciones.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all CgCondiciones entities with a specific property value.  
	 
	  @param propertyName the name of the CgCondiciones property to query
	  @param value the property value to match
	  	  @return List<CgCondiciones> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<CgCondiciones> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding CgCondiciones instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from CgCondiciones model where model." 
			 						+ propertyName + "= :propertyValue order by model.id";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all CgCondiciones entities.
	  	  @return List<CgCondiciones> all CgCondiciones entities
	 */
	@SuppressWarnings("unchecked")
	public List<CgCondiciones> findAll(
		) {
					LogDeMidasEJB3.log("finding all CgCondiciones instances", Level.INFO, null);
			try {
			final String queryString = "select model from CgCondiciones model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}