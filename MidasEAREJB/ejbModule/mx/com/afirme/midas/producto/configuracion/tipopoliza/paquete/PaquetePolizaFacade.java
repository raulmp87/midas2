package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.cobertura.CoberturaSeccionPaqueteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTOId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity PaquetePolizaDTO.
 * @see .PaquetePolizaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class PaquetePolizaFacade  implements PaquetePolizaFacadeRemote {


    @PersistenceContext 
    private EntityManager entityManager;
    
    @Resource
    private SessionContext context;
    
    @EJB
    private static CoberturaSeccionFacadeRemote coberturaSeccionFacade;
	
    @EJB
	private CoberturaCotizacionFacadeRemote coberturaCotFacade;
    
		/**
	 Perform an initial save of a previously unsaved PaquetePolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PaquetePolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public PaquetePolizaDTO save(PaquetePolizaDTO entity) {
    	LogDeMidasEJB3.log("saving PaquetePolizaDTO instance", Level.INFO, null);
	    try {
	    	entity.setEstatus(PaquetePolizaDTO.ESTATUS_CONFIGURANDO);
	    	entityManager.persist(entity);
	    	//Consultar las coberturas obligatorias registradas para la seccion.
	    	
	    	generarEstructuraInicialPaquete(entity);
	    	
            return entity;
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("Falla registro de PaquetePolizaDTO", Level.SEVERE, re);
        	throw re;
        }
    }
    
    /**
     * Valida las sumas aseguradas de las coberturas b&aacute;sicas registradas en el paquete.
     * Si las coberturas son v�lidas, se actualiza el estatus del paquete a "liberado" (1),
     * de lo contrario env�a mansaje de error en el objeto ActualizacionCoberturaPaqueteDTO
     * @param idToPaquete. Identificador del paquete a procesar.
     * @return ActualizacionCoberturaPaqueteDTO objeto que contiene la informaci�n del resultado de la operaci&oacute;n
     */
    public ActualizacionCoberturaPaqueteDTO liberarPaquete(BigDecimal idToPaquete){
    	PaquetePolizaDTO paquetePolizaDTO = findById(idToPaquete);
    	List<CoberturaSeccionPaqueteDTO> listaCoberturasPaquete = obtenerCoberturasRegistradas(idToPaquete);
    	
    	ActualizacionCoberturaPaqueteDTO actualizacionPaquete = new ActualizacionCoberturaPaqueteDTO();
    	actualizacionPaquete.setPaquetePoliza(paquetePolizaDTO);
    	
    	//1. Validar que todas las coberturas b�sicas tengan SA registrada
    	boolean liberacionExitosa = true;
    	for(int i=0; i<listaCoberturasPaquete.size();i++){
    		CoberturaSeccionPaqueteDTO coberturaPaquete = listaCoberturasPaquete.get(i);
    		if(coberturaPaquete.getCoberturaSeccionDTO().getCoberturaDTO().
    				getClaveTipoSumaAsegurada().equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_BASICA)){
    			if(coberturaPaquete.getValorSumaAsegurada() == null ||
    					coberturaPaquete.getValorSumaAsegurada().compareTo(BigDecimal.ZERO) <= 0){
    				liberacionExitosa = false;
    				actualizacionPaquete.setCoberturaSeccionBasica(coberturaPaquete);
    				break;
    			}
    		}
    		else if (coberturaPaquete.getCoberturaSeccionDTO().getCoberturaDTO().
    				getClaveTipoSumaAsegurada().equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_AMPARADA)){
    			//Si la cobertura es amparada, registrar la SA de la cobertura b�sica
				CoberturaSeccionPaqueteDTO coberturaBasica = this.consultarCoberturaPaqueteBasica(
						coberturaPaquete.getPaquetePolizaDTO().getIdToPaquetePoliza(),
						coberturaPaquete.getCoberturaSeccionDTO(), listaCoberturasPaquete);
				
				if(coberturaBasica != null){
					coberturaPaquete.setValorSumaAsegurada(coberturaBasica.getValorSumaAsegurada());
				}
				else{
					throw new RuntimeException("La cobertura "+
							coberturaPaquete.getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion()+
							" es amparada pero su cobertura b�sica no se encuentra en el paquete.");
				}
				
				this.persistirCoberturaSeccionPaqueteDTO(coberturaPaquete);
    		}
    	}
    	
    	if(liberacionExitosa){
    		paquetePolizaDTO.setEstatus(PaquetePolizaDTO.ESTATUS_LIBERADO);
    		update(paquetePolizaDTO);
    		actualizacionPaquete.setOperacionExitosa(Boolean.TRUE);
    	}
    	else{
    		actualizacionPaquete.setOperacionExitosa(Boolean.FALSE);
			actualizacionPaquete.setClaveResultado(ActualizacionCoberturaPaqueteDTO.LIBERAR_PAQUETE_NO_EXITOSO_COBERTURA_BASICA_SIN_SA);
    	}
    	
    	ValidadorModificacionCoberturaPaquete.actualizarMensajeErrorResultado(actualizacionPaquete);
    	
    	//No se realizan otras validaciones debido a que �stas ya se efectuaron durante la configuraci�n del paquete
    	return actualizacionPaquete;
    }
    
    /**
     * Elimina el paquete cuyo identificador se recibe.
     * Son eliminadas en cascada las coberturas del paquete (CoberturaSeccionPaqueteDTO).
     * @param idToPaquete. Identificador del paquete a procesar.
     * @return ActualizacionCoberturaPaqueteDTO objeto que contiene la informaci�n del resultado de la operaci&oacute;n
     */
    public ActualizacionCoberturaPaqueteDTO eliminarPaquete(BigDecimal idToPaquete){
    	ActualizacionCoberturaPaqueteDTO actualizacionPaquete = new ActualizacionCoberturaPaqueteDTO();
    	boolean operacionExitosa = true;
    	String mensajeError = "";
    	try{
    		PaquetePolizaDTO paquetePolizaDTO = findById(idToPaquete);
        	List<CoberturaSeccionPaqueteDTO> listaCoberturasPaquete = obtenerCoberturasRegistradas(idToPaquete);
        	actualizacionPaquete.setPaquetePoliza(paquetePolizaDTO);
        	
        	//Verificar si el paquete no est� definido como el paquete default.
        	if(!paquetePolizaDTO.getTipoPolizaDTO().getIdToPaqueteDefault().equals(paquetePolizaDTO.getIdToPaquetePoliza())){
        		//1. eliminar todas las coberturas del paquete        	
            	for(CoberturaSeccionPaqueteDTO coberturaPaquete : listaCoberturasPaquete){
            		eliminarCoberturaSeccionPaquete(coberturaPaquete);
            	}
            	
            	delete(paquetePolizaDTO);
        	}
        	else{
        		throw new RuntimeException("El paquete que intenta eliminar est� registrado como paquete Default.");
        	}
    	}
    	catch(RuntimeException re){
    		operacionExitosa = false;
    		mensajeError = re.getMessage();
    	}
    	
    	actualizacionPaquete.setOperacionExitosa(operacionExitosa);
    	if(!operacionExitosa){
    		actualizacionPaquete.setClaveResultado(ActualizacionCoberturaPaqueteDTO.ELIMINAR_PAQUETE_NO_EXITOSO);
    		actualizacionPaquete.setClaveMensaje(ActualizacionCoberturaPaqueteDTO.CLAVE_MENSAJE_ERROR);
    		actualizacionPaquete.setMensajeError(Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "configuracion.tipopoliza.paquete.error.eliminarPaquete", mensajeError));
    	}
    	
    	return actualizacionPaquete;
    }
    
    /**
     * Consulta la cobertura b&aacute;sica de la cual depende la cobertura amparada o subl&iacute;mite recibida.
     * La cobertura b&aacute;sica se busca primero en la misma secci&oacte;n de la cobertura recibida y despu&eacute;s
     * en la cobertura de "CONTENIDOS".
     * @param coberturaDependiente
     * @return
     */
    @SuppressWarnings("unchecked")
	public CoberturaSeccionDTO consultarCoberturaBasica(CoberturaSeccionDTO coberturaDependiente){
    	CoberturaSeccionDTO coberturaEncontrada = null;
    	
    	if(coberturaDependiente.getCoberturaDTO().getIdCoberturaSumaAsegurada() != null){
    		
    		
    		coberturaDependiente = entityManager.getReference(CoberturaSeccionDTO.class, coberturaDependiente.getId());
    		
    		String queryString = "select model from CoberturaSeccionDTO model where model.id.idtocobertura = :idtocobertura " +
    				"and model.seccionDTO.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza";
    		Query query = entityManager.createQuery(queryString);
    		query.setParameter("idtocobertura", coberturaDependiente.getCoberturaDTO().getIdCoberturaSumaAsegurada());
    		query.setParameter("idToTipoPoliza", coberturaDependiente.getSeccionDTO().getTipoPolizaDTO().getIdToTipoPoliza());
    		
    		List<CoberturaSeccionDTO> listaCoberturasEncontradas = query.getResultList();
    		if(!listaCoberturasEncontradas.isEmpty()){
    			if(listaCoberturasEncontradas.size()>1){
    				boolean coberturaDeLaMismaSeccion = false;
    				boolean coberturaDeLaSeccionContenidos = false;
    				for(CoberturaSeccionDTO coberturaSeccionTMP : listaCoberturasEncontradas){
    					if(coberturaSeccionTMP.getId().getIdtoseccion().compareTo(coberturaDependiente.getId().getIdtoseccion()) == 0){
    						coberturaDeLaMismaSeccion = true;
    						coberturaEncontrada = coberturaSeccionTMP;
    						break;
    					}
    				}
    				if(!coberturaDeLaMismaSeccion){
    					//si la cobertura no se encuentra en la misma seccion, se usa la cobertura de contenidos.
    					for(CoberturaSeccionDTO coberturaSeccionTMP : listaCoberturasEncontradas){
        					if(coberturaSeccionTMP.getSeccionDTO().getNombreComercial().toUpperCase().indexOf("CONTENIDOS") == 0){
        						coberturaDeLaSeccionContenidos = true;
        						coberturaEncontrada = coberturaSeccionTMP;
        						break;
        					}
        				}
    				}
    				if(!coberturaDeLaSeccionContenidos && !coberturaDeLaMismaSeccion){
    					throw new RuntimeException("No se encontro la cobertura b�sica: "+coberturaDependiente.getCoberturaDTO().getIdCoberturaSumaAsegurada()+
    							", de la cobertura amparada/subl�mite: "+coberturaDependiente.getId().getIdtocobertura()+" en la misma seccion("+coberturaDependiente.getId().getIdtoseccion()+
    							"ni en la secci�n de contenidos. Verifique la consistencia del producto (dependencia de coberturas)");
    				}
    			}
    			else{
    				coberturaEncontrada = listaCoberturasEncontradas.get(0);
    			}
    		}
    	}
    	
    	return coberturaEncontrada;
    }
    
    public List<CoberturaSeccionPaqueteDTO> obtenerCoberturasRegistradas(BigDecimal idToPaquetePoliza){
    	return buscarCoberturaPaquetePorPropiedad("paquetePolizaDTO.idToPaquetePoliza", idToPaquetePoliza);
    }
    
    @SuppressWarnings("unchecked")
	public List<CoberturaSeccionDTO> obtenerCoberturasDisponibles(BigDecimal idToPaquetePoliza){
    	PaquetePolizaDTO paquetePoliza = findById(idToPaquetePoliza);
    	
    	List<CoberturaSeccionPaqueteDTO> listaCoberturasPaquete = this.obtenerCoberturasRegistradas(idToPaquetePoliza);
    	
    	String queryString = "select model from CoberturaSeccionDTO model where model.seccionDTO.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza ";
    	Query query = entityManager.createQuery(queryString);
    	query.setParameter("idToTipoPoliza", paquetePoliza.getTipoPolizaDTO().getIdToTipoPoliza());
    	query.setHint(QueryHints.REFRESH, HintValues.TRUE);
    	List<CoberturaSeccionDTO> listaCompletaCoberturasPoliza = query.getResultList();
    	List<CoberturaSeccionDTO> listaFiltradaCoberturasPoliza = new ArrayList<CoberturaSeccionDTO>();
    	
    	for(CoberturaSeccionDTO coberturaTMP : listaCompletaCoberturasPoliza){
    		boolean coberturaRegistradaPaquete = false;
    		for(CoberturaSeccionPaqueteDTO coberturaPaq : listaCoberturasPaquete){
    			if(coberturaPaq.getCoberturaSeccionDTO().getId().equals(coberturaTMP.getId())){
    				coberturaRegistradaPaquete = true;
    				break;
        		}
    		}
    		if(!coberturaRegistradaPaquete)
				listaFiltradaCoberturasPoliza.add(coberturaTMP);
    		
    	}
    	return listaFiltradaCoberturasPoliza;
    }
    
    /**
     * Ejecuta la modificacion de una cobertura en el paquete. Las modificaciones posibles son: 
     * contratar o descontratar una cobertura y cambiar la suma asegurada de una cobertrua. La 
     * operacion se deduce en base a los parametros recibidos.
     * @param idToPaquetePoliza. Paquete sobre el cual se realizar�n las modificaciones.
     * @param idToCoberturaSeccionPaquete. cobertura a modificar, debe pertenecer al paquete cuyo ID se recibe.
     * 			Puede ser nulo, solo cuando la operacion es "agregar una cobertura".
     * @param idToSeccion. Seccion que se agrega al paquete. Puede ser nula, cuando ya existe la cobertura-seccion,
     * 			especificando un idToCoberturaSeccionPaquete.
     * @param idToCobertura. Cobertura que se agrega al paquete. Puede ser nula, cuando ya existe la cobertura-seccion,
     * 			especificando un idToCoberturaSeccionPaquete.
     * @param montoSumaAsegurada. Suma asegurada a registrar en caso de que la operacion sea "cambio de suma asegurada".
     * @param claveContrato. Usado para espeficar si la cobertura se agrega o se elimina del paquete, 
     * 			valores permitidos: 1 (CONTRATADO) y 0 (DESCONTRATADO).
     */
    public ActualizacionCoberturaPaqueteDTO registrarCoberturaPaquete(BigDecimal idToPaquetePoliza,BigDecimal idToCoberturaSeccionPaquete,
			BigDecimal idToSeccion,BigDecimal idToCobertura,BigDecimal montoSumaAsegurada,Short claveContrato){
    	
    	CoberturaSeccionDTO coberturaSeccionDTO = null;
    	CoberturaSeccionPaqueteDTO coberturaPaqueteDTO = null;
    	PaquetePolizaDTO paquetePolizaDTO = null;
    	
    	if(idToCoberturaSeccionPaquete != null){
    		//Se recibe id, operaciones disponibles: modificar SA, descontratar
    		coberturaPaqueteDTO = consultarCoberturaSeccionPaquetePorId(idToCoberturaSeccionPaquete);
    		if(coberturaPaqueteDTO == null){
    			//Se recibi� un ID de cobertura no v�lido, regresar mensaje en blanco
    			throw new RuntimeException("Se recibi� un ID de coberturaPaquete no v�lido.");
    		}
    	}
    	else{
    		//no se recibe id, la unica operacion disponible es agregar
    		paquetePolizaDTO = findById(idToPaquetePoliza);
    		coberturaSeccionDTO = coberturaSeccionFacade.findById(new CoberturaSeccionDTOId(idToSeccion, idToCobertura));
    	}
    	
    	return registrarCoberturaPaquete(paquetePolizaDTO, coberturaPaqueteDTO, 
    			coberturaSeccionDTO, montoSumaAsegurada, claveContrato);
    }
    
    private ActualizacionCoberturaPaqueteDTO registrarCoberturaPaquete(PaquetePolizaDTO paquetePolizaDTO,
    		CoberturaSeccionPaqueteDTO coberturaPaqueteDTO,
			CoberturaSeccionDTO coberturaSeccionDTO, BigDecimal montoSumaAsegurada,Short claveContrato){
    	
    	Integer claveOperacion = null;
    	if(coberturaPaqueteDTO != null){
    		//Se recibe id, operaciones disponibles: modificar SA, descontratar
    		coberturaSeccionDTO = coberturaPaqueteDTO.getCoberturaSeccionDTO();
    		paquetePolizaDTO = coberturaPaqueteDTO.getPaquetePolizaDTO();
    		
    		if(claveContrato.shortValue() == ConstantesCotizacion.CONTRATADO){
    			//Operacion disponible: modificarSA
    			claveOperacion = AdministradorValidacionCoberturaPaquete.OPERACION_CAMBIO_SUMA_ASEGURADA;
    			coberturaPaqueteDTO.setValorSumaAsegurada(montoSumaAsegurada);
    		}
    		else{//descontratar
    			claveOperacion = AdministradorValidacionCoberturaPaquete.OPERACION_DESCONTRATAR;
    		}
    	}
    	else{
    		//no se recibe id, la unica operacion disponible es agregar
    		coberturaSeccionDTO = entityManager.getReference(CoberturaSeccionDTO.class, coberturaSeccionDTO.getId());
    		claveOperacion = AdministradorValidacionCoberturaPaquete.OPERACION_CONTRATAR;
    	}
    	
    	ValidadorModificacionCoberturaPaquete validador = new AdministradorValidacionCoberturaPaquete().obtenerValidadorModificacionCoberturaPaquete(
    			coberturaSeccionDTO.getCoberturaDTO().getClaveTipoSumaAsegurada(), coberturaSeccionDTO.getClaveObligatoriedad().shortValue(), 
    			claveOperacion, this);
		
    	ActualizacionCoberturaPaqueteDTO resultadoOperacion = 
    		validador.validarModificacionCoberturaPaquete(coberturaSeccionDTO, coberturaPaqueteDTO, paquetePolizaDTO);
    	
    	if(resultadoOperacion.getOperacionExitosa() != null && 
    			!resultadoOperacion.getOperacionExitosa()){
    		context.setRollbackOnly();
    	}
    	
    	ValidadorModificacionCoberturaPaquete.actualizarMensajeErrorResultado(resultadoOperacion);
    	
    	return resultadoOperacion;
    }
    
    /**
     * Consulta la cobertura b�sica y si est� registrada en el paquete, regresa la entidad CoberturaSeccionPaqueteDTO, 
     * de lo contrario regresa nulo
     * @param idToTipoPoliza
     * @param idToPaquetePoliza
     * @param coberturaDependiente
     * @param listaCoberturasRegistradasPaquete
     * @return
     */
    public CoberturaSeccionPaqueteDTO consultarCoberturaPaqueteBasica(BigDecimal idToPaquetePoliza,
    		CoberturaSeccionDTO coberturaDependiente,
    		List<CoberturaSeccionPaqueteDTO> listaCoberturasRegistradasPaquete){

    	CoberturaSeccionPaqueteDTO coberturaEncontrada = null;
    	
    	//Buscar la cobertura b�sica 
		CoberturaSeccionDTO coberturaBasica = consultarCoberturaBasica(coberturaDependiente);
    	
    	if(listaCoberturasRegistradasPaquete != null){
    		for(CoberturaSeccionPaqueteDTO coberturaPaqueteDTO : listaCoberturasRegistradasPaquete){
    			if(coberturaPaqueteDTO.getCoberturaSeccionDTO().getId().equals(coberturaBasica.getId())){
    				coberturaEncontrada = coberturaPaqueteDTO;
    				break;
    			}
    		}
    	}
    	else{
    		coberturaEncontrada = obtenerCoberturaSeccionPaquete(idToPaquetePoliza,coberturaBasica);
    	}
		//Verificar que la cobertura se encuentre en el paquete
		
		return coberturaEncontrada;
    }
    
    public List<CoberturaSeccionDTO> obtenerCoberturasDependientes(BigDecimal idToCoberturaBasica,BigDecimal idToTipoPoliza, String claveTipoSumaAsegurada){
    	CoberturaSeccionDTO coberturaFiltro = new CoberturaSeccionDTO();
    	coberturaFiltro.setSeccionDTO(new SeccionDTO());
    	coberturaFiltro.getSeccionDTO().setTipoPolizaDTO(new TipoPolizaDTO());
    	coberturaFiltro.getSeccionDTO().getTipoPolizaDTO().setIdToTipoPoliza(idToTipoPoliza);
    	coberturaFiltro.setCoberturaDTO(new CoberturaDTO());
    	coberturaFiltro.getCoberturaDTO().setClaveTipoSumaAsegurada(claveTipoSumaAsegurada);
    	coberturaFiltro.getCoberturaDTO().setIdCoberturaSumaAsegurada(idToCoberturaBasica);
    	
    	return coberturaSeccionFacade.listarFiltrado(coberturaFiltro);
    }
    
    /**
	 Delete a persistent PaquetePolizaDTO entity.
	  @param entity PaquetePolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    private void delete(PaquetePolizaDTO entity) {
    	LogDeMidasEJB3.log("deleting PaquetePolizaDTO instance", Level.INFO, null);
    	try {
    		entity = entityManager.getReference(PaquetePolizaDTO.class, entity.getIdToPaquetePoliza());
            entityManager.remove(entity);
            LogDeMidasEJB3.log("delete successful", Level.INFO, null);
    	} catch (RuntimeException re) {
    		LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
    		throw re;
        }
    }
    
    /**
	 Persist a previously saved PaquetePolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the PaquetePolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PaquetePolizaDTO entity to update
	 @return PaquetePolizaDTO the persisted PaquetePolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public PaquetePolizaDTO update(PaquetePolizaDTO entity) {
    				LogDeMidasEJB3.log("updating PaquetePolizaDTO instance", Level.INFO, null);
	        try {
            PaquetePolizaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public PaquetePolizaDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding PaquetePolizaDTO instance with id: " + id, Level.INFO, null);
	        try {
            PaquetePolizaDTO instance = entityManager.find(PaquetePolizaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all PaquetePolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PaquetePolizaDTO property to query
	  @param value the property value to match
	  	  @return List<PaquetePolizaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<PaquetePolizaDTO> findByProperty(String propertyName, final Object value,boolean poblarCoberturas) {
    	LogDeMidasEJB3.log("finding PaquetePolizaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
    	try {
    		final String queryString = "select model from PaquetePolizaDTO model where model."
    			+ propertyName + "= :propertyValue";
    		Query query = entityManager.createQuery(queryString);
    		query.setParameter("propertyValue", value);
    		List<PaquetePolizaDTO> listaResultado = query.getResultList();
    		if(poblarCoberturas && !listaResultado.isEmpty()){
    			for(PaquetePolizaDTO paqueteTMP : listaResultado){
    				paqueteTMP.setCoberturaSeccionPaqueteDTOs(this.obtenerCoberturasRegistradas(paqueteTMP.getIdToPaquetePoliza()));
    			}
    		}
    		return listaResultado;
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}
    
    public List<PaquetePolizaDTO> listarPorTipoPoliza(BigDecimal idToTipoPoliza,boolean poblarCoberturas,Short claveEstatus) {
    	try {
    		PaquetePolizaDTO paquetePolFiltro = new PaquetePolizaDTO();
    		paquetePolFiltro.setTipoPolizaDTO(new TipoPolizaDTO());
    		paquetePolFiltro.getTipoPolizaDTO().setIdToTipoPoliza(idToTipoPoliza);
    		paquetePolFiltro.setEstatus(claveEstatus);
    		List<PaquetePolizaDTO> listaResultado = listarFiltradoPaquete(paquetePolFiltro, poblarCoberturas);
    		return listaResultado;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("falla consulta de Paquetes por TipoPoliza: idToTipoPoliza: "+idToTipoPoliza, Level.SEVERE, re);
			throw re;
		}
	}
    
	public CoberturaSeccionPaqueteDTO consultarCoberturaSeccionPaquetePorId(BigDecimal id) {
		return entityManager.find(CoberturaSeccionPaqueteDTO.class, id);
	}

	/**
	 * Ejecuta un "update" o "save" sobre el objeto recibido.
	 * @param coberturaSeccionPaqueteDTO
	 * @return el coberturaSeccionPaqueteDTO objeto actualizado.
	 */
	public CoberturaSeccionPaqueteDTO persistirCoberturaSeccionPaqueteDTO(CoberturaSeccionPaqueteDTO coberturaSeccionPaqueteDTO){
		if(coberturaSeccionPaqueteDTO.getIdToCoberturaSeccionPaquete() != null){
			entityManager.merge(coberturaSeccionPaqueteDTO);
		}
		else{
			entityManager.persist(coberturaSeccionPaqueteDTO);
		}
		return coberturaSeccionPaqueteDTO;
	}
	
	public void eliminarCoberturaSeccionPaquete(CoberturaSeccionPaqueteDTO coberturaSeccionPaqueteDTO){
		entityManager.remove(coberturaSeccionPaqueteDTO);
	}
	
	public boolean eliminarCoberturaSeccionPaquete(BigDecimal idToPaquetePoliza,CoberturaSeccionDTO coberturaSeccionDTO){
		boolean resultado = false;
		CoberturaSeccionPaqueteDTO coberturaEncontrada = obtenerCoberturaSeccionPaquete(idToPaquetePoliza,coberturaSeccionDTO);
		if(coberturaEncontrada != null){
			eliminarCoberturaSeccionPaquete(coberturaEncontrada);
			resultado = true;
		}
		return resultado;
	}
	
	/**
	 * Aplica la configuraci�n del paquete recibido en la cotizaci�n-inciso recibidos.
	 */
	public void aplicarPaqueteCotizacion(BigDecimal idToPaquete,BigDecimal idToCotizacion,BigDecimal numeroInciso){
		
		PaquetePolizaDTO paquetePoliza = findById(idToPaquete);
		
		if(!paquetePoliza.getEstatus().equals(PaquetePolizaDTO.ESTATUS_LIBERADO)){
			throw new RuntimeException("El paquete no est� liberado: idToPaquete= "+idToPaquete+", estatus = "+paquetePoliza.getEstatus());
		}
		
		List<CoberturaSeccionPaqueteDTO> listaCoberturasPaq = this.obtenerCoberturasRegistradas(idToPaquete);
		
		List<CoberturaCotizacionDTO> listaCoberturasContratadas = 
			coberturaCotFacade.listarCoberturasContratadas(idToCotizacion, numeroInciso);
		
		//Descontratar todas las coberturas del inciso, no se considera la obligatoriedad debido a que posteriormente se
		//establecer�n los contratos definidos en el paquete, los cuales deben ser consistentes.
		for(CoberturaCotizacionDTO coberturaCotDTO : listaCoberturasContratadas){
			coberturaCotDTO.setClaveContrato(ConstantesCotizacion.NO_CONTRATADO);
			coberturaCotDTO.setValorSumaAsegurada(0d);
			
			coberturaCotFacade.update(coberturaCotDTO);
		}
		
		CoberturaCotizacionDTO coberturaCotDTO = null;
		//Contratar las coberturas incluidas en el paquete y contratarlas en el inciso.
		for(CoberturaSeccionPaqueteDTO coberturaSeccionPaqDTO : listaCoberturasPaq){
			//Obtener la cobertura y contratarla
			coberturaCotDTO = coberturaCotFacade.findById(new CoberturaCotizacionId(idToCotizacion, numeroInciso, 
					coberturaSeccionPaqDTO.getCoberturaSeccionDTO().getId().getIdtoseccion(), 
					coberturaSeccionPaqDTO.getCoberturaSeccionDTO().getId().getIdtocobertura()));
			
			if(coberturaCotDTO == null){
				throw new RuntimeException("La cobertura no existe en el inciso. (idToCotizacion, numeroInciso,Idtoseccion, Idtocobertura):" +
						idToCotizacion + numeroInciso + 
						coberturaSeccionPaqDTO.getCoberturaSeccionDTO().getId().getIdtoseccion() + 
						coberturaSeccionPaqDTO.getCoberturaSeccionDTO().getId().getIdtocobertura());
			}
			
			coberturaCotDTO.setClaveContrato(ConstantesCotizacion.CONTRATADO);
			coberturaCotDTO.setValorSumaAsegurada(coberturaSeccionPaqDTO.getValorSumaAsegurada().doubleValue());
			
			coberturaCotFacade.update(coberturaCotDTO);
		}
		
	}
	
	public CoberturaSeccionPaqueteDTO obtenerCoberturaSeccionPaquete(BigDecimal idToPaquetePoliza,CoberturaSeccionDTO coberturaSeccionDTO){
		CoberturaSeccionPaqueteDTO coberturaEncontrada = null;
		CoberturaSeccionPaqueteDTO coberturaSeccionFiltro = new CoberturaSeccionPaqueteDTO();
		coberturaSeccionFiltro.setPaquetePolizaDTO(new PaquetePolizaDTO());
		coberturaSeccionFiltro.getPaquetePolizaDTO().setIdToPaquetePoliza(idToPaquetePoliza);
		coberturaSeccionFiltro.setCoberturaSeccionDTO(coberturaSeccionDTO);
		List<CoberturaSeccionPaqueteDTO> listaCoberturasPaquete = //buscarCoberturaPaquetePorPropiedad("coberturaSeccionDTO", coberturaSeccionDTO);
			 this.listarFiltradoCoberturaPaquete(coberturaSeccionFiltro);
		if(!listaCoberturasPaquete.isEmpty()){
			coberturaEncontrada = listaCoberturasPaquete.get(0);
		}
		return coberturaEncontrada;
	}
	
	public PaquetePolizaDTO establecerPaqueteDefault(BigDecimal idToPaquetePoliza){
		PaquetePolizaDTO paquetePoliza = null;
		try{
			LogDeMidasEJB3.log("Actualizando paquetePoliza por default. idToPaquetePoliza="+idToPaquetePoliza, Level.INFO, null);
			paquetePoliza = findById(idToPaquetePoliza);
			
			if(paquetePoliza.getEstatus().equals(PaquetePolizaDTO.ESTATUS_LIBERADO)){
			
				TipoPolizaDTO tipoPoliza = paquetePoliza.getTipoPolizaDTO();
				
				tipoPoliza.setIdToPaqueteDefault(idToPaquetePoliza);
				entityManager.merge(tipoPoliza);
			}
			else{
				throw new RuntimeException("El paquete no ha sido liberado.");
			}
			
		}catch(RuntimeException e){
			LogDeMidasEJB3.log("Error al establecer paquete por default de tipo de p�liza. idToPaquetePoliza="+idToPaquetePoliza, Level.SEVERE, e);
		}
		
		return paquetePoliza;
	}
	
	@SuppressWarnings("unchecked")
    private List<CoberturaSeccionPaqueteDTO> buscarCoberturaPaquetePorPropiedad(String propertyName, final Object value) {
		try {
			
			String queryString = "select model from CoberturaSeccionPaqueteDTO model where model." 
				+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	} 
	
	List<CoberturaSeccionPaqueteDTO> obtenerCoberturasObligatoriaParcialEnPaquete(
			BigDecimal idToPaquetePoliza,BigDecimal idToSeccion){
		CoberturaSeccionPaqueteDTO filtro = new CoberturaSeccionPaqueteDTO();
		
		filtro.setPaquetePolizaDTO(new PaquetePolizaDTO());
		filtro.getPaquetePolizaDTO().setIdToPaquetePoliza(idToPaquetePoliza);
		
		filtro.setCoberturaSeccionDTO(new CoberturaSeccionDTO());
		filtro.getCoberturaSeccionDTO().setClaveObligatoriedad(new BigDecimal(ConstantesCotizacion.OBLIGATORIO_PARCIAL));
		
		filtro.getCoberturaSeccionDTO().setId(new CoberturaSeccionDTOId());
		filtro.getCoberturaSeccionDTO().getId().setIdtoseccion(idToSeccion);
		
		return listarFiltradoCoberturaPaquete(filtro);
	}
	
	@SuppressWarnings("unchecked")
	private List<CoberturaSeccionPaqueteDTO> listarFiltradoCoberturaPaquete(CoberturaSeccionPaqueteDTO entity){
		try {
			String queryString = "select model from CoberturaSeccionPaqueteDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToCoberturaSeccionPaquete", entity.getIdToCoberturaSeccionPaquete());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveContrato", entity.getClaveContrato());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "valorSumaAsegurada", entity.getValorSumaAsegurada());
			
			if(entity.getCoberturaSeccionDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "coberturaSeccionDTO.claveObligatoriedad", entity.getCoberturaSeccionDTO().getClaveObligatoriedad());
				
				if(entity.getCoberturaSeccionDTO().getId() != null){
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "coberturaSeccionDTO.id.idtocobertura", entity.getCoberturaSeccionDTO().getId().getIdtocobertura());
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "coberturaSeccionDTO.id.idtoseccion", entity.getCoberturaSeccionDTO().getId().getIdtoseccion());
				}
				if(entity.getCoberturaSeccionDTO().getCoberturaDTO() != null){
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "coberturaSeccionDTO.coberturaDTO.claveTipoSumaAsegurada", 
							entity.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada());
					
				}
			}
			
			if(entity.getPaquetePolizaDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "paquetePolizaDTO.idToPaquetePoliza", entity.getPaquetePolizaDTO().getIdToPaquetePoliza());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "paquetePolizaDTO.descripcion", entity.getPaquetePolizaDTO().getDescripcion());
				sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "paquetePolizaDTO.nombre", entity.getPaquetePolizaDTO().getNombre());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "paquetePolizaDTO.orden", entity.getPaquetePolizaDTO().getOrden());
				
				if(entity.getPaquetePolizaDTO().getTipoPolizaDTO() != null){
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "paquetePolizaDTO.tipoPolizaDTO.idToTipoPoliza", 
							entity.getPaquetePolizaDTO().getTipoPolizaDTO().getIdToTipoPoliza());
				}
			}

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarFiltrado failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<PaquetePolizaDTO> listarFiltradoPaquete(PaquetePolizaDTO entity,boolean poblarCoberturas){
		try {
			String queryString = "select model from PaquetePolizaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToPaquetePoliza", entity.getIdToPaquetePoliza());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "descripcion", entity.getDescripcion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "estatus", entity.getEstatus());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "nombre", entity.getNombre());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "orden", entity.getOrden());
			
			if(entity.getTipoPolizaDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "tipoPolizaDTO.idToTipoPoliza", entity.getTipoPolizaDTO().getIdToTipoPoliza());
			}
			
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<PaquetePolizaDTO> listaResultado = query.getResultList();
			if(poblarCoberturas){
				for(PaquetePolizaDTO paqueteTMP : listaResultado){
					paqueteTMP.setCoberturaSeccionPaqueteDTOs(this.obtenerCoberturasRegistradas(paqueteTMP.getIdToPaquetePoliza()));
				}
			}
			return listaResultado;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarFiltrado failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	private List<CoberturaSeccionDTO> obtenerCoberturas(BigDecimal idToTipoPoliza, String claveTipoSumaAsegurada, BigDecimal ... claveObligatoriedad){
    	CoberturaSeccionDTO coberturaFiltro = new CoberturaSeccionDTO();
    	coberturaFiltro.setSeccionDTO(new SeccionDTO());
    	coberturaFiltro.getSeccionDTO().setTipoPolizaDTO(new TipoPolizaDTO());
    	coberturaFiltro.getSeccionDTO().getTipoPolizaDTO().setIdToTipoPoliza(idToTipoPoliza);
    	coberturaFiltro.setCoberturaDTO(new CoberturaDTO());
    	coberturaFiltro.getCoberturaDTO().setClaveTipoSumaAsegurada(claveTipoSumaAsegurada);
    	
    	List<CoberturaSeccionDTO> listaCoberturasEncontradas = null;
    	
    	if(claveObligatoriedad != null && claveObligatoriedad.length>0){
    		listaCoberturasEncontradas = new ArrayList<CoberturaSeccionDTO>();
    		for(int i=0;i<claveObligatoriedad.length;i++){
    			coberturaFiltro.setClaveObligatoriedad(claveObligatoriedad[i]);
    			
    			listaCoberturasEncontradas.addAll(coberturaSeccionFacade.listarFiltrado(coberturaFiltro));
    		}
    	}
    	else{
    		listaCoberturasEncontradas = coberturaSeccionFacade.listarFiltrado(coberturaFiltro);
    	}
    	return listaCoberturasEncontradas;
    }
	
	private void generarEstructuraInicialPaquete(PaquetePolizaDTO paquetePolizaDTO){
    	List<CoberturaSeccionPaqueteDTO> listaCoberturasRegistradasPaq = new ArrayList<CoberturaSeccionPaqueteDTO>();
    	BigDecimal idToTipoPoliza = paquetePolizaDTO.getTipoPolizaDTO().getIdToTipoPoliza();
    	//1. Consultar coberturas obligatorias y obligatorias parciales b�sicas
    	List<CoberturaSeccionDTO> coberturasObligatoriasBasicas = obtenerCoberturas(idToTipoPoliza, ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_BASICA,
    			new BigDecimal(ConstantesCotizacion.OBLIGATORIO), new BigDecimal(ConstantesCotizacion.OBLIGATORIO_PARCIAL));
    	
    	//2. Registrar cada cobertura usando suma asegurada 0
    	ActualizacionCoberturaPaqueteDTO actualizacionCobertura = null;
    	for(CoberturaSeccionDTO coberturaObligatoriaBasica : coberturasObligatoriasBasicas){
    		
    		actualizacionCobertura = registrarCoberturaPaquete(
    				paquetePolizaDTO, null,
    				coberturaObligatoriaBasica,
    				BigDecimal.ZERO,ConstantesCotizacion.CONTRATADO);
    		
    		if(!actualizacionCobertura.getOperacionExitosa()){
    			//el escenario no debe generar error, debido a que se incluyen coberturas b�scias oblogatorias y obligatorias parcial
    			//se deja la validaci�n para futuras modificaciones en la definici�n de tipo de coberturas.
    			
    			throw new RuntimeException("Ocurrio un error al crear el paquete, " +
    					"verifique la consistencia de la configuraci�n del producto. Detalle: "+
    					actualizacionCobertura.getMensajeError());
    		}
    		else{
    			listaCoberturasRegistradasPaq.add(actualizacionCobertura.getCoberturaPaqueteDTO());
    		}
    		
//    		listaCoberturasRegistradasPaq.add( registrarCoberturaPaquete(paquetePolizaDTO, coberturaObligatoriaBasica, null) );
    	}
    	
    	//3. Procesar coberturas b�sicas y subl�mite
    	procesarCoberturasDependientes(paquetePolizaDTO, listaCoberturasRegistradasPaq, ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_AMPARADA);
    	
    	procesarCoberturasDependientes(paquetePolizaDTO, listaCoberturasRegistradasPaq, ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_SUBLIMITE);
    	
    }
    
    private void procesarCoberturasDependientes(PaquetePolizaDTO paquetePolizaDTO,List<CoberturaSeccionPaqueteDTO> listaCoberturasRegistradasPaq,String claveTipoSumaASegurada){
    	BigDecimal idToTipoPoliza = paquetePolizaDTO.getTipoPolizaDTO().getIdToTipoPoliza();
    	//3. Consultar coberturas obligatorias y obligatorias parciales amparadas � subl�mite.
    	List<CoberturaSeccionDTO> coberturasObligatoriasAmparadas = obtenerCoberturas(idToTipoPoliza, claveTipoSumaASegurada,
    			new BigDecimal(ConstantesCotizacion.OBLIGATORIO), new BigDecimal(ConstantesCotizacion.OBLIGATORIO_PARCIAL));
    	
    	//4. Por cada cobertura obligatoria y obligatoria parcial amparada, consultar su cobertura b�sica, si no est� en el paquete, aplicar el paso 2 para la cobertura b�sica.
    	ActualizacionCoberturaPaqueteDTO actualizacionCobertura = null;
    	for(CoberturaSeccionDTO coberturaObligatoriaAmparada : coberturasObligatoriasAmparadas){
    		
    		actualizacionCobertura = registrarCoberturaPaquete(
    				paquetePolizaDTO, null,
    				coberturaObligatoriaAmparada,
    				BigDecimal.ZERO, ConstantesCotizacion.CONTRATADO);
    		
    		if(!actualizacionCobertura.getOperacionExitosa()){
    			//el escenario no debe generar error, debido a que se incluyen coberturas b�scias oblogatorias y obligatorias parcial
    			//se deja la validaci�n para futuras modificaciones en la definici�n de tipo de coberturas.
    			
    			throw new RuntimeException("Ocurrio un error al crear el paquete, " +
    					"verifique la consistencia de la configuraci�n del producto. Detalle: "+
    					actualizacionCobertura.getMensajeError());
    		}
    		else{
    			if (actualizacionCobertura.getClaveResultado().shortValue() == ActualizacionCoberturaPaqueteDTO.CLAVE_COBERTURA_BASICA_CONTRATADA
    					&& actualizacionCobertura.getCoberturaSeccionBasica() != null){
    				listaCoberturasRegistradasPaq.add(actualizacionCobertura.getCoberturaSeccionBasica());
    			}
    			listaCoberturasRegistradasPaq.add(actualizacionCobertura.getCoberturaPaqueteDTO());
    		}
    		
//    		//5. Una vez procesada la cobertura b�sica, se procede con la amparada.
//    		listaCoberturasRegistradasPaq.add(registrarCoberturaPaquete(paquetePolizaDTO, coberturaObligatoriaAmparada, BigDecimal.ZERO));
    	}
    }
    
//    /**
//     * Recibe Como par�metro una cobertura amparada o subl�mite, y consulta si su cobertura b�sica est� registrada en el 
//     * paquete, de no ser as�, incluye la cobertura b�sica en el paquete.
//     * @param idToTipoPoliza
//     * @param paquetePolizaDTO
//     * @param coberturaDependiente
//     * @param listaCoberturasRegistradasPaquete
//     * @return
//     */
//    @Deprecated
//    public CoberturaSeccionPaqueteDTO contratarCoberturaBasica(BigDecimal idToTipoPoliza,
//    		PaquetePolizaDTO paquetePolizaDTO,CoberturaSeccionDTO coberturaDependiente,
//    		List<CoberturaSeccionPaqueteDTO> listaCoberturasRegistradasPaquete){
//    	
//    	CoberturaSeccionPaqueteDTO coberturaPaqueteDTO = consultarCoberturaPaqueteBasica(
//				paquetePolizaDTO.getIdToPaquetePoliza(), coberturaDependiente,listaCoberturasRegistradasPaquete);
//		
//		//Si la cobertura b�sica no se encuentra registrada, se incluye en el paquete.
//		if(coberturaPaqueteDTO == null){
//			CoberturaSeccionDTO coberturaBasica = consultarCoberturaBasica(coberturaDependiente);
//			coberturaPaqueteDTO = registrarCoberturaPaquete(paquetePolizaDTO, coberturaBasica, BigDecimal.ZERO);
//			
//			if(listaCoberturasRegistradasPaquete != null)
//				listaCoberturasRegistradasPaquete.add(coberturaPaqueteDTO);
//		}
//		else{
//			coberturaPaqueteDTO = null;
//		}
//		return coberturaPaqueteDTO;
//    }
}