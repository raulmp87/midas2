package mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ExclusionDescuentoVarioTipoPolizaDTO.
 * @see .ExclusionDescuentoVarioTipoPolizaDTO
  * @author Jos� Luis Arellano
 */
@Stateless
public class ExclusionDescuentoVarioTipoPolizaFacade implements ExclusionDescuentoVarioTipoPolizaFacadeRemote{
	@PersistenceContext private EntityManager entityManager;
	
	/**
	 Perform an initial save of a previously unsaved ExclusionDescuentoVarioTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ExclusionDescuentoVarioTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
	public void save(ExclusionDescuentoVarioTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("saving ExclusionDescuentoVarioTipoPolizaDTO instance", Level.INFO, null);
        try {
        	entityManager.persist(entity);
        	LogDeMidasEJB3.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
            throw re;
        }
	}

	/**
	 Delete a persistent ExclusionDescuentoVarioTipoPolizaDTO entity.
	  @param entity ExclusionDescuentoVarioTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
	public void delete(ExclusionDescuentoVarioTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("deleting ExclusionDescuentoVarioTipoPolizaDTO instance", Level.INFO, null);
        try {
        	entity = entityManager.getReference(ExclusionDescuentoVarioTipoPolizaDTO.class, entity.getId());
        	entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
            throw re;
        }
	}

	/**
	 Persist a previously saved ExclusionDescuentoVarioTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExclusionDescuentoVarioTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExclusionDescuentoVarioTipoPolizaDTO entity to update
	 @return ExclusionDescuentoVarioTipoPolizaDTO the persisted ExclusionDescuentoVarioTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ExclusionDescuentoVarioTipoPolizaDTO update(ExclusionDescuentoVarioTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("updating ExclusionDescuentoVarioTipoPolizaDTO instance", Level.INFO, null);
        try {
        	ExclusionDescuentoVarioTipoPolizaDTO result = entityManager.merge(entity);
        	LogDeMidasEJB3.log("update successful", Level.INFO, null);
            return result;
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
            throw re;
        }
	}

	public ExclusionDescuentoVarioTipoPolizaDTO findById( ExclusionDescuentoVarioTipoPolizaId id) {
		LogDeMidasEJB3.log("finding ExclusionDescuentoVarioTipoPolizaDTO instance with id: " + id, Level.INFO, null);
        try {
        	ExclusionDescuentoVarioTipoPolizaDTO instance = entityManager.find(ExclusionDescuentoVarioTipoPolizaDTO.class, id);
        	return instance;
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
            throw re;
        }
	}

	/**
	 * Find all ExclusionDescuentoVarioTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ExclusionDescuentoVarioTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<ExclusionDescuentoVarioTipoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionDescuentoVarioTipoPolizaDTO> findByProperty(String propertyName, final Object value) {
		LogDeMidasEJB3.log("finding ExclusionDescuentoVarioTipoPolizaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionDescuentoVarioTipoPolizaDTO model where model." 
				+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ExclusionDescuentoVarioTipoPolizaDTO entities.
	  	  @return List<ExclusionDescuentoVarioTipoPolizaDTO> all ExclusionDescuentoVarioTipoPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionDescuentoVarioTipoPolizaDTO> findAll() {
		LogDeMidasEJB3.log("finding all ExclusionDescuentoVarioTipoPolizaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionDescuentoVarioTipoPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find a ExclusionDescuentoVarioTipoPolizaDTO entity with the specific
	 * received id�s.
	 * 
	 * @param BigDecimal idToTipoPoliza.
	 * @param BigDecimal idToDescuentoVario.
	 * @param BigDecimal idToCobertura.
	 * 
	 * @return List<ExclusionRecargoVarioTipoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionDescuentoVarioTipoPolizaDTO> findByIDs(BigDecimal idToTipoPoliza, BigDecimal idToDescuentoVario, BigDecimal idToCobertura) {
		LogDeMidasEJB3.log("finding ExclusionDescuentoVarioTipoPolizaDTO instance with idtotipopoliza: "+ idToTipoPoliza + ", idToDescuentoVario: "+idToDescuentoVario+"idToCobertura: "+idToCobertura, Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionDescuentoVarioTipoPolizaDTO model where model.id.idtotipopoliza = :poliza and model.id.idtocobertura = :cobertura and model.id.idtodescuentovario = :descuento";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("poliza", idToTipoPoliza);
			query.setParameter("cobertura", idToCobertura);
			query.setParameter("descuento", idToDescuentoVario);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Encuentra los registros de ExclusionDescuentoVarioTipoPolizaDTO relacionados con el TipoPolizaDTO cuyo ID se recibe y que adem�s est�n 
	 * relacionados s�lo con las coberturas que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToTipoPoliza. El ID del tipoPoliza
	  @return List<ExclusionDescuentoVarioTipoPolizaDTO> encontrados por el query formado.
	 */
    @SuppressWarnings("unchecked")
    public List<ExclusionDescuentoVarioTipoPolizaDTO> getVigentesPorIdTipoPoliza(BigDecimal idToTipoPoliza) {
    	LogDeMidasEJB3.log("encontrando ExclusionDescuentoVarioTipoPolizaDTO relacionadas con el tipoPoliza: "+idToTipoPoliza+" y con coberturas vigentes", Level.INFO, null);
			try {
			final String queryString = "select model from ExclusionDescuentoVarioTipoPolizaDTO model where model.id.idtotipopoliza = :idToTipoPoliza and" +
					"(model.coberturaDTO.claveActivo <> 0 ) and " +
					"(model.coberturaDTO.idToCobertura in (select cobSec.id.idtocobertura from CoberturaSeccionDTO cobSec where cobSec.id.idtoseccion in" +
					"(select sec.idToSeccion from SeccionDTO sec where (sec.claveEstatus <> 3 ) and " +
					"sec.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza)))";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
}
