package mx.com.afirme.midas.producto.configuracion.tipopoliza;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity TipoPolizaDTO.
 * 
 * @see .TipoPolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoPolizaFacade implements TipoPolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoPolizaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoPolizaDTO entity) {
		LogDeMidasEJB3.log("saving TipoPolizaDTO instance", Level.INFO, null);
		try {
			if (entity.getIdToTipoPoliza() ==  null){
				entity.setVersion(Integer.valueOf(1));
			}
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoPolizaDTO entity.
	 * 
	 * @param entity
	 *            TipoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoPolizaDTO entity) {
		LogDeMidasEJB3.log("deleting TipoPolizaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(TipoPolizaDTO.class, entity
					.getIdToTipoPoliza());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoPolizaDTO entity and return it or a copy
	 * of it to the sender. A copy of the TipoPolizaDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoPolizaDTO entity to update
	 * @return TipoPolizaDTO the persisted TipoPolizaDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoPolizaDTO update(TipoPolizaDTO entity) {
		LogDeMidasEJB3.log("updating TipoPolizaDTO instance", Level.INFO, null);
		try {
			TipoPolizaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoPolizaDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding TipoPolizaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoPolizaDTO instance = entityManager
					.find(TipoPolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoPolizaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TipoPolizaDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoPolizaDTO entities.
	 * 
	 * @return List<TipoPolizaDTO> all TipoPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoPolizaDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoPolizaDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from TipoPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoPolizaDTO entities with a specific property value.
	 * @param id
	 *            the id value to match
	 * @return TipoPolizaDTO found by query
	 */
	public TipoPolizaDTO findByChildId(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding TipoPolizaDTO instance with SeccionDTO child idToSeccion: "
						+ id, Level.INFO, null);
		try {
			final String queryString = "select parent from TipoPolizaDTO parent, SeccionDTO child where parent.idToTipoPoliza = child.tipoPolizaDTO.idToTipoPoliza and child.idToSeccion = :idToSeccion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", id);
			return (TipoPolizaDTO) query.getSingleResult();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by idToSeccion failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Borra l�gicamente un registro de TipoPoliza, estableciendo en 0 los campos 
	 * CLAVEACTIVO y CLAVEESTATUS
	 * 
	 * @param entity
	 *            TipoPolizaDTO entity el registro TipoPoliza a borrar
	 * @throws RuntimeException
	 *             si la operacion falla
	 */
	public TipoPolizaDTO borradoLogico(TipoPolizaDTO entity){
		LogDeMidasEJB3.log("logical delete TipoPolizaDTO instance", Level.INFO, null);
		try {
			/*entity.setClaveActivoConfiguracion(0);
			entity.setClaveActivoProduccion(0);*/
			entity.setClaveActivo(new Short("0"));
			entity.setClaveEstatus(new Short("3"));
			TipoPolizaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("logical delete successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("logical delete failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Busca los registros de TipoPoliza que no hayan sido borrados logicamente
	 * 
	 * @return List<TipoPolizaDTO> registros de TipoPoliza que no han sido borrados logicamente
	 */
	@SuppressWarnings("unchecked")
	public List<TipoPolizaDTO> listarVigentes(){
		LogDeMidasEJB3.log("finding all active RiesgoDTO instances", Level.INFO, null);
		try {
			/*final String queryString = "select model from TipoPolizaDTO model where model.claveEstatus" +
					"<> 0 and model.claveActivo <> 0";*/
			final String queryString = "select model from TipoPolizaDTO model where model.claveEstatus" +
					"<> 3";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all active failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Busca los registros de TipoPoliza que no hayan sido borrados logicamente
	 * y que pertenezcan a un producto.
	 * 
	 * @return List<TipoPolizaDTO> registros de TipoPoliza que no han sido borrados logicamente
	 * 		pertenecientes al producto cuyo ID se recibe.
	 * @param BigDecimal idToProducto el id del producto al que pertenecen 
	 *		los registros TipoPoliza
	 */
	@SuppressWarnings("unchecked")
	public List<TipoPolizaDTO> listarVigentesPorIdProducto(BigDecimal idToProducto){
		LogDeMidasEJB3.log("finding all active RiesgoDTO instances with idToProducto = "+idToProducto.toString(), Level.INFO, null);
		try {
/*			final String queryString = "select model from TipoPolizaDTO model where ( model.claveEstatus" +
					"<> 0 and model.claveActivo <> 0 ) and model.productoDTO.idToProducto = :idToProducto";*/
			final String queryString = "select model from TipoPolizaDTO model where ( model.claveEstatus" +
					"<> 3 ) and model.productoDTO.idToProducto = :idToProducto";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToProducto", idToProducto);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all active failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoPolizaDTO> listarPorIdProducto(BigDecimal idToProducto, Boolean verInactivos, Boolean soloActivos){
		LogDeMidasEJB3.log("finding all TipoPolizaDTO instances with idToProducto = "+idToProducto.toString(), Level.INFO, null);
		try {
			String sVerInactivos = "";
			if(soloActivos){
				sVerInactivos = " ( model.claveEstatus IN (1,2)) and  ";
			}else{
				sVerInactivos = (verInactivos?"( model.claveEstatus IN (1,2,3)) and":" ( model.claveEstatus IN (0,1)) and  ");
			}
			
			String queryString = "select model from TipoPolizaDTO model where " +
					sVerInactivos + " model.productoDTO.idToProducto = :idToProducto order by model.codigo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToProducto", idToProducto);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	
	
	
	
	/**
	 * Find a tipoPolizaDTO entity wich is related with the given cotizacionDTO id.
	 * @param idToCotizacion CotizacionDTO entity id
	 * @return TipoPolizaDTO entity
	 */
	@SuppressWarnings("unchecked")
	public TipoPolizaDTO findTipoPolizaByCotizacion(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("finding tipoPolizaDTO entity wich is related with cotizacionDTO: "+idToCotizacion, Level.INFO,null);
		try {
			if (idToCotizacion != null){
				final String queryString = "select model from TipoPolizaDTO model where model.idToTipoPoliza in " +
						"(select c.tipoPolizaDTO.idToTipoPoliza from CotizacionDTO c where c.idToCotizacion = :idToCotizacion)";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("idToCotizacion", idToCotizacion);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				List<TipoPolizaDTO> lista = query.getResultList();
				if (!lista.isEmpty())
					return (TipoPolizaDTO)lista.get(0);
			}
			return null;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find TipoPolizaByCotizacion failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Busca los registros de TipoPoliza (activas e inactivas) que pertenezcan a un Producto.
	 * 
	 * @param idToProducto identificador del Producto
	 * @return lista de TipoPoliza (activas e inactivas)
	 */
	@SuppressWarnings("unchecked")
	public List<TipoPolizaDTO> listarPorIdProducto(BigDecimal idToProducto) {
		LogDeMidasEJB3.log(
				"finding all TipoPolizaDTO instances with idToProducto: "
						+ idToProducto, Level.INFO, null);
		try {
			final String queryString = "select model from TipoPolizaDTO model where model.productoDTO.idToProducto = :idToProducto";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToProducto", idToProducto);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all TipoPolizaDTO failed", Level.SEVERE,
					re);
			throw re;
		}
	}
}