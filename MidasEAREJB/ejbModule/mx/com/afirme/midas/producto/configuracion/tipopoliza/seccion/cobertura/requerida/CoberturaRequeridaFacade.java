package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaId;

/**
 * Facade for entity CoberturaRequeridaDTO.
 * 
 * @see .CoberturaRequeridaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CoberturaRequeridaFacade implements CoberturaRequeridaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved CoberturaRequeridaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CoberturaRequeridaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CoberturaRequeridaDTO entity) {
		LogDeMidasEJB3.log("saving CoberturaRequeridaDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent CoberturaRequeridaDTO entity.
	 * 
	 * @param entity
	 *            CoberturaRequeridaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoberturaRequeridaDTO entity) {
		LogDeMidasEJB3.log("deleting CoberturaRequeridaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(CoberturaRequeridaDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved CoberturaRequeridaDTO entity and return it or
	 * a copy of it to the sender. A copy of the CoberturaRequeridaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CoberturaRequeridaDTO entity to update
	 * @return CoberturaRequeridaDTO the persisted CoberturaRequeridaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoberturaRequeridaDTO update(CoberturaRequeridaDTO entity) {
		LogDeMidasEJB3.log("updating CoberturaRequeridaDTO instance",
				Level.INFO, null);
		try {
			CoberturaRequeridaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CoberturaRequeridaDTO findById(CoberturaRequeridaId id) {
		LogDeMidasEJB3.log("finding CoberturaRequeridaDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			CoberturaRequeridaDTO instance = entityManager.find(
					CoberturaRequeridaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoberturaRequeridaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoberturaRequeridaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoberturaRequeridaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaRequeridaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding CoberturaRequeridaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from CoberturaRequeridaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all CoberturaRequeridaDTO entities.
	 * 
	 * @return List<CoberturaRequeridaDTO> all CoberturaRequeridaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaRequeridaDTO> findAll() {
		LogDeMidasEJB3.log("finding all CoberturaRequeridaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from CoberturaRequeridaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}