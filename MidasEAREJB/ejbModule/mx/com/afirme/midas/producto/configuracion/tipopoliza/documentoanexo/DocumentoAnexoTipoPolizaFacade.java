package mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity DocumentoAnexoTipoPolizaDTO.
 * @see .DocumentoAnexoTipoPolizaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DocumentoAnexoTipoPolizaFacade  implements DocumentoAnexoTipoPolizaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DocumentoAnexoTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocumentoAnexoTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocumentoAnexoTipoPolizaDTO entity) {
    				LogDeMidasEJB3.log("saving DocumentoAnexoTipoPolizaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DocumentoAnexoTipoPolizaDTO entity.
	  @param entity DocumentoAnexoTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocumentoAnexoTipoPolizaDTO entity) {
    				LogDeMidasEJB3.log("deleting DocumentoAnexoTipoPolizaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DocumentoAnexoTipoPolizaDTO.class, entity.getIdToDocumentoAnexoTipoPoliza());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DocumentoAnexoTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocumentoAnexoTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocumentoAnexoTipoPolizaDTO entity to update
	 @return DocumentoAnexoTipoPolizaDTO the persisted DocumentoAnexoTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DocumentoAnexoTipoPolizaDTO update(DocumentoAnexoTipoPolizaDTO entity) {
    				LogDeMidasEJB3.log("updating DocumentoAnexoTipoPolizaDTO instance", Level.INFO, null);
	        try {
            DocumentoAnexoTipoPolizaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DocumentoAnexoTipoPolizaDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding DocumentoAnexoTipoPolizaDTO instance with id: " + id, Level.INFO, null);
	        try {
            DocumentoAnexoTipoPolizaDTO instance = entityManager.find(DocumentoAnexoTipoPolizaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DocumentoAnexoTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocumentoAnexoTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<DocumentoAnexoTipoPolizaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DocumentoAnexoTipoPolizaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding DocumentoAnexoTipoPolizaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			    	final String queryString = "select model from DocumentoAnexoTipoPolizaDTO model where model." 
			 						+ propertyName + "= :propertyValue order by model.numeroSecuencia";
								Query query = entityManager.createQuery(queryString);
				query.setParameter("propertyValue", value);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all DocumentoAnexoTipoPolizaDTO entities.
	  	  @return List<DocumentoAnexoTipoPolizaDTO> all DocumentoAnexoTipoPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DocumentoAnexoTipoPolizaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all DocumentoAnexoTipoPolizaDTO instances", Level.INFO, null);
			try {
			    	final String queryString = "select model from DocumentoAnexoTipoPolizaDTO model";
								Query query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}