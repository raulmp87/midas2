package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete;

import java.util.logging.Level;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.amparada.ValidacionCambioSAAmparadaObligatoria;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.amparada.ValidacionCambioSAAmparadaObligatoriaParcial;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.amparada.ValidacionCambioSAAmparadaOpcional;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.amparada.ValidacionContratarAmparadaObligatoria;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.amparada.ValidacionContratarAmparadaObligatoriaParcial;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.amparada.ValidacionContratarAmparadaOpcional;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.amparada.ValidacionDescontratarAmparadaObligatoria;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.amparada.ValidacionDescontratarAmparadaObligatoriaParcial;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.amparada.ValidacionDescontratarAmparadaOpcional;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.basica.ValidacionCambioSABasicaObligatoria;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.basica.ValidacionCambioSABasicaObligatoriaParcial;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.basica.ValidacionCambioSABasicaOpcional;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.basica.ValidacionContratarBasicaObligatoria;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.basica.ValidacionContratarBasicaObligatoriaParcial;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.basica.ValidacionContratarBasicaOpcional;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.basica.ValidacionDescontratarBasicaObligatoria;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.basica.ValidacionDescontratarBasicaObligatoriaParcial;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.basica.ValidacionDescontratarBasicaOpcional;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.sublimite.ValidacionCambioSASublimiteObligatoria;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.sublimite.ValidacionCambioSASublimiteObligatoriaParcial;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.sublimite.ValidacionCambioSASublimiteOpcional;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.sublimite.ValidacionContratarSublimiteObligatoria;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.sublimite.ValidacionContratarSublimiteObligatoriaParcial;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.sublimite.ValidacionContratarSublimiteOpcional;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.sublimite.ValidacionDescontratarSublimiteObligatoria;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.sublimite.ValidacionDescontratarSublimiteObligatoriaParcial;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.sublimite.ValidacionDescontratarSublimiteOpcional;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;

public class AdministradorValidacionCoberturaPaquete {
	
	public static final Integer OPERACION_CONTRATAR = 0;
	public static final Integer OPERACION_DESCONTRATAR = 1;
	public static final Integer OPERACION_CAMBIO_SUMA_ASEGURADA = 2;
	
	private static final Integer SUMA_ASEGURADA_BASICA = 0;
	private static final Integer SUMA_ASEGURADA_AMPARADA = 1;
	private static final Integer SUMA_ASEGURADA_SUBLIMITE = 2;
	
	private static final Integer CLAVE_OBLIGATORIA = 0;
	private static final Integer CLAVE_OBLIGATORIA_PARCIAL = 1;
	private static final Integer CLAVE_OPCIONAL = 2;
	
	@SuppressWarnings({ "unchecked"})
	private static final Class[][][] arrayValidadores =new Class[3][3][3];
	
	static{
		arrayValidadores[0][0][0] = ValidacionContratarBasicaObligatoria.class;
		arrayValidadores[0][0][1] = ValidacionDescontratarBasicaObligatoria.class;
		arrayValidadores[0][0][2] = ValidacionCambioSABasicaObligatoria.class;
		arrayValidadores[1][0][0] = ValidacionContratarAmparadaObligatoria.class;
		arrayValidadores[1][0][1] = ValidacionDescontratarAmparadaObligatoria.class;
		arrayValidadores[1][0][2] = ValidacionCambioSAAmparadaObligatoria.class;
		arrayValidadores[2][0][0] = ValidacionContratarSublimiteObligatoria.class;
		arrayValidadores[2][0][1] = ValidacionDescontratarSublimiteObligatoria.class;
		arrayValidadores[2][0][2] = ValidacionCambioSASublimiteObligatoria.class;
		arrayValidadores[0][1][0] = ValidacionContratarBasicaObligatoriaParcial.class;
		arrayValidadores[0][1][1] = ValidacionDescontratarBasicaObligatoriaParcial.class;
		arrayValidadores[0][1][2] = ValidacionCambioSABasicaObligatoriaParcial.class;
		arrayValidadores[1][1][0] = ValidacionContratarAmparadaObligatoriaParcial.class;
		arrayValidadores[1][1][1] = ValidacionDescontratarAmparadaObligatoriaParcial.class;
		arrayValidadores[1][1][2] = ValidacionCambioSAAmparadaObligatoriaParcial.class;
		arrayValidadores[2][1][0] = ValidacionContratarSublimiteObligatoriaParcial.class;
		arrayValidadores[2][1][1] = ValidacionDescontratarSublimiteObligatoriaParcial.class;
		arrayValidadores[2][1][2] = ValidacionCambioSASublimiteObligatoriaParcial.class;
		arrayValidadores[0][2][0] = ValidacionContratarBasicaOpcional.class;
		arrayValidadores[0][2][1] = ValidacionDescontratarBasicaOpcional.class;
		arrayValidadores[0][2][2] = ValidacionCambioSABasicaOpcional.class;
		arrayValidadores[1][2][0] = ValidacionContratarAmparadaOpcional.class;
		arrayValidadores[1][2][1] = ValidacionDescontratarAmparadaOpcional.class;
		arrayValidadores[1][2][2] = ValidacionCambioSAAmparadaOpcional.class;
		arrayValidadores[2][2][0] = ValidacionContratarSublimiteOpcional.class;
		arrayValidadores[2][2][1] = ValidacionDescontratarSublimiteOpcional.class;
		arrayValidadores[2][2][2] = ValidacionCambioSASublimiteOpcional.class;
	}
	
	AdministradorValidacionCoberturaPaquete(){
		
	}
	
	@SuppressWarnings("unchecked")
	public ValidadorModificacionCoberturaPaquete obtenerValidadorModificacionCoberturaPaquete(
			String claveTipoSumaAsegurada,Short claveObligatoriedad,Integer claveOperacion,
			PaquetePolizaFacade paquetePolizaFacade){
		
		Integer claveObligatoriedadInterno = null;
		Integer claveTipoSAInterno = null;
		
		if(claveTipoSumaAsegurada != null){
			if (claveTipoSumaAsegurada.equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_BASICA)){
				claveTipoSAInterno = SUMA_ASEGURADA_BASICA;
			}
			else if(claveTipoSumaAsegurada.equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_AMPARADA)){
				claveTipoSAInterno = SUMA_ASEGURADA_AMPARADA;
			}
			else if(claveTipoSumaAsegurada.equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_SUBLIMITE)){
				claveTipoSAInterno = SUMA_ASEGURADA_SUBLIMITE;
			}
		}
		
		if(claveObligatoriedad != null){
			if(claveObligatoriedad.shortValue() == ConstantesCotizacion.OBLIGATORIO){
				claveObligatoriedadInterno = CLAVE_OBLIGATORIA;
			}
			else if(claveObligatoriedad.shortValue() == ConstantesCotizacion.OBLIGATORIO_PARCIAL){
				claveObligatoriedadInterno = CLAVE_OBLIGATORIA_PARCIAL;
			}
			else if(claveObligatoriedad.shortValue() == ConstantesCotizacion.OPCIONAL || 
					claveObligatoriedad.shortValue() == ConstantesCotizacion.DEFAULT){
				claveObligatoriedadInterno = CLAVE_OPCIONAL;
			}
		}
		
		if(claveTipoSAInterno == null || claveObligatoriedadInterno == null || 
				(claveOperacion == null || 
						(claveOperacion.intValue() != OPERACION_CONTRATAR.intValue() && 
						claveOperacion.intValue() != OPERACION_DESCONTRATAR.intValue() &&
						claveOperacion.intValue() != OPERACION_CAMBIO_SUMA_ASEGURADA.intValue())) ){
			throw new RuntimeException("Parametros no v�lidos: claveTipoSumaAsegurada="+claveTipoSumaAsegurada+"," +
					"claveObligatoriedad="+claveObligatoriedad+", claveOperacion="+claveOperacion+
					". Utilice las constantes de las clases ConstantesCotizacion y AdministradorValidacionCoberturaPaquete");
		}
		
		try {
			ValidadorModificacionCoberturaPaquete validador = (ValidadorModificacionCoberturaPaquete)
				(arrayValidadores[claveTipoSAInterno][claveObligatoriedadInterno][claveOperacion].getConstructor(PaquetePolizaFacade.class)).newInstance(paquetePolizaFacade);
			
			return validador;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error al instanciar validador.", Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}
	
}
