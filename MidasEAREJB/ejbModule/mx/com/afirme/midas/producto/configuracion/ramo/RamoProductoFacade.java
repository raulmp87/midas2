package mx.com.afirme.midas.producto.configuracion.ramo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity CoberturaSeccionDTO.
 * @see .CoberturaSeccionDTO
  * @author Jos� Luis Arellano 
 */
@Stateless
public class RamoProductoFacade implements RamoProductoFacadeRemote{
	@PersistenceContext private EntityManager entityManager;
	/**
 	Perform an initial save of a previously unsaved CoberturaSeccionDTO entity. 
 	All subsequent persist actions of this entity should use the #update() method.
  	@param entity CoberturaSeccionDTO entity to persist
  	@throws RuntimeException when the operation fails
	 */
	public void save(RamoProductoDTO entity) {
		LogDeMidasEJB3.log("saving RamoProductoDTO instance", Level.INFO, null);
        try {
        	entityManager.persist(entity);
        	LogDeMidasEJB3.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
    		LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
        throw re;
        }
	}

	/**
 	Delete a persistent RamoProductoDTO entity.
  	@param entity RamoProductoDTO entity to delete
 	@throws RuntimeException when the operation fails
	 */
	public void delete(RamoProductoDTO entity) {
		LogDeMidasEJB3.log("deleting RamoProductoDTO instance", Level.INFO, null);
        try {
        	entity = entityManager.getReference(RamoProductoDTO.class, entity.getId());
        	entityManager.remove(entity);
        	LogDeMidasEJB3.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
    		LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
            throw re;
        }
	}

	/**
 	Persist a previously saved RamoProductoDTO entity and return it or a copy of it to the sender. 
 	A copy of the RamoProductoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
  	@param entity RamoProductoDTO entity to update
 	@return RamoProductoDTO the persisted RamoProductoDTO entity instance, may not be the same
 	@throws RuntimeException if the operation fails
	 */
	public RamoProductoDTO update(RamoProductoDTO entity) {
		LogDeMidasEJB3.log("updating RamoProductoDTO instance", Level.INFO, null);
        try {
        	RamoProductoDTO result = entityManager.merge(entity);
        	LogDeMidasEJB3.log("update successful", Level.INFO, null);
            return result;
        } catch (RuntimeException re) {
    		LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
            throw re;
        }
	}

	public RamoProductoDTO findById( RamoProductoId id) {
		LogDeMidasEJB3.log("finding RamoProductoDTO instance with id: " + id, Level.INFO, null);
        try {
        	RamoProductoDTO instance = entityManager.find(RamoProductoDTO.class, id);
        	return instance;
        } catch (RuntimeException re) {
    		LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
            throw re;
        }
	}

	/**
	 * Find all RamoProductoDTO entities with a specific property value.  
  	@param propertyName the name of the RamoProductoDTO property to query
  	@param value the property value to match
  	@return List<RamoProductoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RamoProductoDTO> findByProperty(String propertyName, final Object value) {
		LogDeMidasEJB3.log("finding RamoProductoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RamoProductoDTO model where model." 
		 				+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RamoProductoDTO entities.
  	 @return List<RamoProductoDTO> all RamoProductoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RamoProductoDTO> findAll() {
		LogDeMidasEJB3.log("finding all RamoProductoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from RamoProductoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Lista todos los ramos que no est�n asociadas a un producto determinada 
	 * por el id que recibe el m�todo
	 * @param BigDecimal idToSeccion el id de la seccion.
	 * @return List<RamoProductoDTO> registros RamoProducto que no est�n asociados a la seccion.
	 */
	@SuppressWarnings("unchecked")
	public List<RamoProductoDTO> obtenerRamosSinAsociar(BigDecimal idToProducto){
		LogDeMidasEJB3.log("Encontrando los ramos no asociados al producto: " + idToProducto, Level.INFO, null);
		try {
			final String queryString = "select model from RamoProductoDTO model where " +
				"model.id.idtoproducto <> :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", idToProducto);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fallo en encontrar las coberturas no asiciadas a la seccion: "+idToProducto, Level.SEVERE, re);
			throw re;
		}
	}
	
}
