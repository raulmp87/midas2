package mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda;

// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO_;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity MonedaTipoPolizaDTO.
 * 
 * @see .MonedaTipoPolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class MonedaTipoPolizaFacade implements MonedaTipoPolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved MonedaTipoPolizaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            MonedaTipoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(MonedaTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("saving MonedaTipoPolizaDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent MonedaTipoPolizaDTO entity.
	 * 
	 * @param entity
	 *            MonedaTipoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(MonedaTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("deleting MonedaTipoPolizaDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(MonedaTipoPolizaDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved MonedaTipoPolizaDTO entity and return it or a
	 * copy of it to the sender. A copy of the MonedaTipoPolizaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            MonedaTipoPolizaDTO entity to update
	 * @return MonedaTipoPolizaDTO the persisted MonedaTipoPolizaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public MonedaTipoPolizaDTO update(MonedaTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("updating MonedaTipoPolizaDTO instance", Level.INFO,
				null);
		try {
			MonedaTipoPolizaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public MonedaTipoPolizaDTO findById(MonedaTipoPolizaId id) {
		LogDeMidasEJB3.log("finding MonedaTipoPolizaDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			MonedaTipoPolizaDTO instance = entityManager.find(
					MonedaTipoPolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all MonedaTipoPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the MonedaTipoPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<MonedaTipoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<MonedaTipoPolizaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding MonedaTipoPolizaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from MonedaTipoPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all MonedaTipoPolizaDTO entities.
	 * 
	 * @return List<MonedaTipoPolizaDTO> all MonedaTipoPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<MonedaTipoPolizaDTO> findAll() {
		LogDeMidasEJB3.log("finding all MonedaTipoPolizaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from MonedaTipoPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public List<MonedaTipoPolizaDTO> findByTipoPolizaId(BigDecimal idToTipoPoliza) {
		return findByProperty(
				MonedaTipoPolizaDTO_.tipoPolizaDTO.getName()+"."+TipoPolizaDTO_.idToTipoPoliza.getName(), 
				idToTipoPoliza);
	}

	
}