package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Facade for entity SeccionDTO.
 * 
 * @see .SeccionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class SeccionFacade implements SeccionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOG = LoggerFactory.getLogger(SeccionFacade.class);
	/**
	 * Perform an initial save of a previously unsaved SeccionDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SeccionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SeccionDTO entity) {
		LogDeMidasEJB3.log("saving SeccionDTO instance", Level.INFO, null);
		try {
			if (entity.getIdToSeccion() == null){
				entity.setVersion(Integer.valueOf(1));
			}
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent SeccionDTO entity.
	 * 
	 * @param entity
	 *            SeccionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SeccionDTO entity) {
		LogDeMidasEJB3.log("deleting SeccionDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(SeccionDTO.class, entity
					.getIdToSeccion());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved SeccionDTO entity and return it or a copy of
	 * it to the sender. A copy of the SeccionDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SeccionDTO entity to update
	 * @return SeccionDTO the persisted SeccionDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SeccionDTO update(SeccionDTO entity) {
		LogDeMidasEJB3.log("updating SeccionDTO instance", Level.INFO, null);
		try {
			SeccionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public SeccionDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding SeccionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			SeccionDTO instance = entityManager.find(SeccionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SeccionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SeccionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SeccionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding SeccionDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from SeccionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all SeccionDTO entities.
	 * 
	 * @return List<SeccionDTO> all SeccionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionDTO> findAll() {
		LogDeMidasEJB3
				.log("finding all SeccionDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from SeccionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Borra l�gicamente un registro de Seccion, estableciendo en 0 los campos 
	 * CLAVEACTIVO y CLAVEESTATUS
	 * 
	 * @param entity
	 *            SeccionDTO entity el registro Seccion a borrar
	 * @throws RuntimeException
	 *             si la operacion falla
	 */
	public SeccionDTO borradoLogico(SeccionDTO entity){
		LogDeMidasEJB3.log("logical delete SeccionDTO instance", Level.INFO, null);
		try {
			/*entity.setClaveActivoConfiguracion("0");
			entity.setClaveActivoProduccion("0");*/
			entity.setClaveActivo(new Short("0"));
			entity.setClaveEstatus(new Short("3"));
			SeccionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("logical delete successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("logical delete failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Busca los registros de Seccion que no hayan sido borrados logicamente
	 * 
	 * @return List<SeccionDTO> registros de Seccion que no han sido borrados logicamente
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionDTO> listarVigentes(){
		LogDeMidasEJB3.log("finding all active SeccionDTO instances", Level.INFO, null);
		try {
			/*final String queryString = "select model from SeccionDTO model where model.claveActivoConfiguracion" +
					"<> 0 or model.claveActivoProduccion <> 0";*/
			final String queryString = "select model from SeccionDTO model where model.claveEstatus <> 3 ";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all active failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Busca los registros de Seccion que no hayan sido borrados logicamente
	 * pertenecientes al TipoPoliza cuyo ID se recibe.
	 * @param BigDecimal idToProducto el id del producto al que pertenecen 
	 *		los registros TipoPoliza
	 * @return List<SeccionDTO> registros de Seccion que no han sido borrados logicamente
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionDTO> listarVigentes(BigDecimal idToTipoPoliza){
		LogDeMidasEJB3.log("finding all active SeccionDTO instances", Level.INFO, null);
		try {
			/*final String queryString = "select model from SeccionDTO model where ( model.claveActivoConfiguracion" +
			"<> 0 or model.claveActivoProduccion <> 0 ) and model.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza";*/
			final String queryString = "select model from SeccionDTO model where ( model.claveEstatus <> 3 )" +
				" and model.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all active failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SeccionDTO> listarPorIdTipoPoliza(BigDecimal idToTipoPoliza, Boolean verInactivos, Boolean soloActivos){
		LogDeMidasEJB3.log("finding all SeccionDTO instances by idTipoPoliza " + idToTipoPoliza, Level.INFO, null);
		try {
			String sVerInactivos = "";
			if(soloActivos){
				sVerInactivos =" ( model.claveEstatus IN (1,2)) and  ";
			}else{
				sVerInactivos = (verInactivos?"( model.claveEstatus IN (1,2,3)) and":" ( model.claveEstatus IN (0,1)) and  ");
			}
			
			String queryString = "select model from SeccionDTO model where " + sVerInactivos + 
				" model.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza order by model.codigo, model.version";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	/**
	 * Busca los registros de Seccion que no sean requeridos para la secci�n cuyo id se recibe.
	 * @param BigDecimal idToSeccion el id de la seccion
	 * @param BigDecimal idTiTipoPoliza el id del TipoPoliza al que pertenece la seccion
	 * @return List<SeccionDTO> registros de Seccion que no est�n relacionados con la seccion.
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionDTO> listarSeccionesNoRequeridas(BigDecimal idToSeccion,BigDecimal idToTipoPoliza){
		LogDeMidasEJB3.log("finding all SeccionDTO instances not related with Seccion id: "+idToSeccion, Level.INFO, null);
		try {
			/*String queryString = "select model from SeccionDTO model where " +
					"( (model.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza and model.idToSeccion <> :idToSeccion)" +
					"and ( (model.idToSeccion not in (select sb.id.idSeccionRequerida from SeccionRequeridaDTO sb where sb.id.idToSeccion = :idToSeccion) )" +
					"and (model.idToSeccion not in (select sr.id.idToSeccion from SeccionRequeridaDTO sr where sr.id.idSeccionRequerida = :idToSeccion) ) ) )";*/
			/*String queryString = "select model from SeccionDTO model where " +
			"( (model.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza and model.idToSeccion <> :idToSeccion)" +
			"( (model.idToSeccion <> :idToSeccion)" +
			"and ( (model.idToSeccion not in (select sb.id.idSeccionRequerida from SeccionRequeridaDTO sb ) )" +
			"and (model.idToSeccion not in (select sr.id.idToSeccion from SeccionRequeridaDTO sr where sr.id.idSeccionRequerida = :idToSeccion) ) ) ) )";*/
			String queryString = "select model from SeccionDTO model where " +
				"(model.claveEstatus <> 3)  and " +
				"( (model.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza and model.idToSeccion <> :idToSeccion)" +
				"and ( (model.idToSeccion not in (select sb.id.idSeccionRequerida from SeccionRequeridaDTO sb ) )" +
				"and (model.idToSeccion not in (select sr.id.idToSeccion from SeccionRequeridaDTO sr) ) ) )";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all active failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Busca los registros de Seccion que sean requeridos para la secci�n cuyo id se recibe.
	 * @param BigDecimal idToSeccion el id de la seccion
	 * @return List<SeccionDTO> registros de Seccion que est�n relacionados con la seccion.
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionDTO> listarSeccionesRequeridas(BigDecimal idToSeccion){
		LogDeMidasEJB3.log("finding all SeccionDTO instances related with Seccion id: "+idToSeccion, Level.INFO, null);
		try {
			/*String queryString = "select model from SeccionDTO model where ( ( model.idToSeccion <> :idToSeccion) and " +
					"(model.idToSeccion in (select sb.id.idSeccionRequerida from SeccionRequeridaDTO sb where sb.id.idToSeccion = :idToSeccion) " +
					"or model.idToSeccion in (select sr.id.idToSeccion from SeccionRequeridaDTO sr where sr.id.idSeccionRequerida = :idToSeccion) ) )";*/
			String queryString = "select model from SeccionDTO model where ( " +
					"( model.idToSeccion <> :idToSeccion) and " +
					"(model.claveEstatus <> 3)  and " +
			"(model.idToSeccion in (select sb.id.idSeccionRequerida from SeccionRequeridaDTO sb where sb.id.idToSeccion = :idToSeccion) ) )";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all active failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Busca los registros de Seccion vigentes que dependan de un tipo poliza, que a su vez dependa de un producto cuya claveNegocio sea A.
	 * @return List<SeccionDTO> registros de Seccion que est�n relacionados con la seccion.
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionDTO> listarVigentesAutos(){
		LogDeMidasEJB3.log("finding all active SeccionDTO instances to vehicles", Level.INFO, null);
		try {
			final String queryString = "select distinct model from SeccionDTO model " +
								"join model.tipoPolizaDTO pol " +
								"join pol.productoDTO prod " +
								"where  model.claveEstatus <> 3 and prod.claveNegocio='A' " +
								"order by model.idToSeccion asc";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find list active sections to vehicles failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SeccionDTO> listarVigentesAutosUsables(){
		LogDeMidasEJB3.log("finding all active SeccionDTO instances to vehicles", Level.INFO, null);
		try {
			final String queryString = "select distinct model from SeccionDTO model " +
								"join model.tipoPolizaDTO pol " +
								"join pol.productoDTO prod " +
								"where  model.claveEstatus IN (1,2) and prod.claveNegocio='A' " +
								" and model.claveActivo = 1" +
								" and pol.claveActivo = 1" +
								" and prod.claveActivo = 1" +
								" order by model.descripcion asc";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find list active sections to vehicles failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Busca los registros de Seccion vigentes que dependan de un tipo poliza, que a su vez dependa de un producto cuya claveNegocio sea A.
	 * @return List<SeccionDTO> registros de Seccion que est�n relacionados con la seccion.
	 */
	@SuppressWarnings("unchecked")
	public List<SeccionDTO> listarVigentesPorNegocio(String claveNegocio){
		LogDeMidasEJB3.log("finding all active SeccionDTO instances to vehicles", Level.INFO, null);
		try {
			final String queryString = "select distinct model from SeccionDTO model " +
								"join model.tipoPolizaDTO pol " +
								"join pol.productoDTO prod " +
								"where  model.claveEstatus <> 3 and prod.claveNegocio=:claveNegocio " +
								"order by model.idToSeccion asc";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("claveNegocio", claveNegocio);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find list active sections to vehicles failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ValorCatalogoAgentes> listarCedulasAsociadasPorSeccion(BigDecimal idToSeccion) {
		LOG.debug("finding all ValorCatalogoAgentes instances related with Seccion id {}",idToSeccion);
		int grupoCataloCedula=70;
		try {
			StringBuilder stringQuery = new StringBuilder();
			stringQuery.append("select model from ValorCatalogoAgentes model where ");
			stringQuery.append("(model.grupoCatalogoAgente.id = :grupoCataloCedula) ");
			stringQuery.append("and (model.id in (select sc.idTipoCedula from SeccionCedula sc where (sc.idToSeccion = :idToSeccion)))");
			Query query = entityManager.createQuery(stringQuery.toString());
			query.setParameter("grupoCataloCedula", grupoCataloCedula);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch(RuntimeException re) {
			LOG.error("find all ValorCatalogoAgentes instances related with Seccion id failed {}",re.getMessage());
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ValorCatalogoAgentes> listarCedulasNoAsociadasPorSeccion(BigDecimal idToSeccion) {
		LOG.debug("finding all ValorCatalogoAgentes instances not related with Seccion id {}",idToSeccion);
		int grupoCataloCedula=70;
		try {
			StringBuilder stringQuery = new StringBuilder();
			stringQuery.append("select model from ValorCatalogoAgentes model where ");
			stringQuery.append("(model.grupoCatalogoAgente.id = :grupoCataloCedula) ");
			stringQuery.append("and (model.id not in (select sc.idTipoCedula from SeccionCedula sc where (sc.idToSeccion = :idToSeccion)))");
			Query query = entityManager.createQuery(stringQuery.toString());
			query.setParameter("grupoCataloCedula", grupoCataloCedula);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch(RuntimeException re) {
			LOG.error("find all grupoCatalogoAgente instances not related with Seccion id failed {}",re.getMessage());
			throw re;
		}
	}
	
	public Integer guardarCedulasAsociadasPorSeccion(BigDecimal idToSeccion, String idsCedulas) {
		LOG.debug("inserting all ValorCatalogoAgentes instances related with Seccion id {}",idToSeccion);
		try {
            String stringQuery = "delete from MIDAS.TOSECCION_TIPOCEDULA where TOSECCION_ID = ?1";
            Query query = entityManager.createNativeQuery(stringQuery);
            query.setParameter(1, idToSeccion);
            query.executeUpdate();
            if(!idsCedulas.equals("")) {
            	String[] cedulas = idsCedulas.split(",");
            	for(String cedula : cedulas) {
                	stringQuery = "insert into MIDAS.TOSECCION_TIPOCEDULA values(MIDAS.TOSECCION_TIPOCEDULA_SEQ.NEXTVAL,?1,?2)";
                    query = entityManager.createNativeQuery(stringQuery);
                    query.setParameter(1, idToSeccion);
                    query.setParameter(2, cedula);
                    query.executeUpdate();
                }
            }
            return 1;
        } catch (RuntimeException re) {
        	LOG.error("save all ValorCatalogoAgentes failed {}",re.getMessage());
        	return 0;
        } 
	}
	
	public BigDecimal countCedulasAsociadasPorSeccion(Long idTipoCedula, BigDecimal idSeccion) {
		LOG.debug("counting all cedulaDTO instances related with Seccion id: {}", idSeccion);
		try {
			StringBuilder stringQuery = new StringBuilder();
			stringQuery.append("select count(*) from MIDAS.TOSECCION_TIPOCEDULA tt where tt.TIPOCEDULA_ID = ?1 ");
			stringQuery.append("and tt.TOSECCION_ID = ?2");
            Query query = entityManager.createNativeQuery(stringQuery.toString());
            query.setParameter(1, idTipoCedula);
            query.setParameter(2, idSeccion);
            return (BigDecimal) query.getSingleResult();
		} catch(RuntimeException re) {
			LogDeMidasEJB3.log("counting all CedulaDTO failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public BigDecimal countCedulasAsociadasPorSeccionesEnTipoPoliza(Long idTipoCedula, BigDecimal idTipoPoliza) {
		LOG.debug("counting all cedulaDTO instances related with TipoPoliza id: {}", idTipoPoliza);
		try{
			StringBuilder stringQuery = new StringBuilder();
			stringQuery.append("select count(*) from MIDAS.TOSECCION_TIPOCEDULA tstc "); 
			stringQuery.append("inner join MIDAS.TOSECCION ts on (tstc.TOSECCION_ID = ts.IDTOSECCION) ");
			stringQuery.append("and (ts.IDTOTIPOPOLIZA = ?1) and (tstc.TIPOCEDULA_ID = ?2)");
			Query query = entityManager.createNativeQuery(stringQuery.toString());
			query.setParameter(1, idTipoPoliza);
			query.setParameter(2, idTipoCedula);
			return (BigDecimal) query.getSingleResult();
		} catch(RuntimeException re) {
			LogDeMidasEJB3.log("counting all CedulaDTO failed", Level.SEVERE, re);
			throw re;
		}
	}

	@TransactionAttribute (TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, String> generarNuevaVersion(BigDecimal pIdSeccion) throws Exception{
		Map<String, String> mensaje = new HashMap<String, String>();
		String spName = "MIDAS.PKGPYT_SERVICIOS.spPYT_NuevaVersionSeccion";
		try {
			LogDeMidasInterfaz.log("Entrando a SeccionFacade.generaNuevaVersion..." + this, Level.INFO, null);
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);	
			
			storedHelper.estableceParametro("pIdSeccion",pIdSeccion);
			BigDecimal resultado= BigDecimal.valueOf(storedHelper.ejecutaActualizar());
			SeccionDTO nuevoProducto = new SeccionDTO();
			nuevoProducto = this.findById(resultado);
			
			mensaje.put("exito", "1");//Exito
			mensaje.put("mensaje", "Se Gener\u00f3 correctamente la nueva versi\u00f3n del producto:" + nuevoProducto.getDescripcion() +"</br> El n\u00famero de  Versi\u00f3n es: " + nuevoProducto.getVersion());	



			LogDeMidasInterfaz.log("Saliendo de SeccionFacade.generaNuevaVersion..." + this, Level.INFO, null);

			return mensaje;
		} catch (RuntimeException re) {	
			LogDeMidasEJB3.log("nuevaversion failed", Level.SEVERE, re);
			mensaje.put("icono", "10");//Error
			mensaje.put("mensaje", "Error al generar la Versi\u00f3n.");
			return mensaje;
		} catch (Exception e){
			LogDeMidasEJB3.log("nuevaversion failed", Level.SEVERE, e);
			mensaje.put("icono", "10");//Error
			mensaje.put("mensaje", "Error al generar la Versi\u00f3n.");
			return mensaje;
		}
	}		
	
}