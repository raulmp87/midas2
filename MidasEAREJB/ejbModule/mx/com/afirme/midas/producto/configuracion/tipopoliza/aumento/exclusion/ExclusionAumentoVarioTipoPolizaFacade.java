package mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ExclusionAumentoVarioTipoPolizaDTO.
 * @see .ExclusionAumentoVarioTipoPolizaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ExclusionAumentoVarioTipoPolizaFacade  implements ExclusionAumentoVarioTipoPolizaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ExclusionAumentoVarioTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ExclusionAumentoVarioTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ExclusionAumentoVarioTipoPolizaDTO entity) {
    				LogDeMidasEJB3.log("saving ExclusionAumentoVarioTipoPolizaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ExclusionAumentoVarioTipoPolizaDTO entity.
	  @param entity ExclusionAumentoVarioTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ExclusionAumentoVarioTipoPolizaDTO entity) {
    				LogDeMidasEJB3.log("deleting ExclusionAumentoVarioTipoPolizaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ExclusionAumentoVarioTipoPolizaDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ExclusionAumentoVarioTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExclusionAumentoVarioTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExclusionAumentoVarioTipoPolizaDTO entity to update
	 @return ExclusionAumentoVarioTipoPolizaDTO the persisted ExclusionAumentoVarioTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ExclusionAumentoVarioTipoPolizaDTO update(ExclusionAumentoVarioTipoPolizaDTO entity) {
    				LogDeMidasEJB3.log("updating ExclusionAumentoVarioTipoPolizaDTO instance", Level.INFO, null);
	        try {
            ExclusionAumentoVarioTipoPolizaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ExclusionAumentoVarioTipoPolizaDTO findById( ExclusionAumentoVarioTipoPolizaId id) {
    				LogDeMidasEJB3.log("finding ExclusionAumentoVarioTipoPolizaDTO instance with id: " + id, Level.INFO, null);
	        try {
            ExclusionAumentoVarioTipoPolizaDTO instance = entityManager.find(ExclusionAumentoVarioTipoPolizaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ExclusionAumentoVarioTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ExclusionAumentoVarioTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<ExclusionAumentoVarioTipoPolizaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ExclusionAumentoVarioTipoPolizaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ExclusionAumentoVarioTipoPolizaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from ExclusionAumentoVarioTipoPolizaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ExclusionAumentoVarioTipoPolizaDTO entities.
	  	  @return List<ExclusionAumentoVarioTipoPolizaDTO> all ExclusionAumentoVarioTipoPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionAumentoVarioTipoPolizaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ExclusionAumentoVarioTipoPolizaDTO instances", Level.INFO, null);
			try {
			    	final String queryString = "select model from ExclusionAumentoVarioTipoPolizaDTO model";
				Query query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find a ExclusionAumentoVarioTipoPolizaDTO entity with the specific
	 * received id�s.
	 * 
	 * @param BigDecimal idToTipoPoliza.
	 * @param BigDecimal idToAumentoVario.
	 * @param BigDecimal idToCobertura.
	 * 
	 * @return List<ExclusionAumentoVarioTipoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionAumentoVarioTipoPolizaDTO> findByIDs(BigDecimal idToTipoPoliza, BigDecimal idToAumentoVario, BigDecimal idToCobertura) {
		LogDeMidasEJB3.log("finding ExclusionAumentoVarioTipoPolizaDTO instance with idtotipopoliza: "+ idToTipoPoliza + ", idToAumentoVario: "+idToAumentoVario+"idToCobertura: "+idToCobertura, Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionAumentoVarioTipoPolizaDTO model where model.id.idtotipopoliza = :poliza and model.id.idtocobertura = :cobertura and model.id.idtoaumentovario = :aumento";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("poliza", idToTipoPoliza);
			query.setParameter("cobertura", idToCobertura);
			query.setParameter("aumento", idToAumentoVario);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Encuentra los registros de ExclusionAumentoVarioTipoPolizaDTO relacionados con el TipoPolizaDTO cuyo ID se recibe y que adem�s est�n 
	 * relacionados s�lo con las coberturas que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToTipoPoliza. El ID del tipoPoliza
	  @return List<ExclusionAumentoVarioTipoPolizaDTO> encontrados por el query formado.
	 */
    @SuppressWarnings("unchecked")
    public List<ExclusionAumentoVarioTipoPolizaDTO> getVigentesPorIdTipoPoliza(BigDecimal idToTipoPoliza) {
    	LogDeMidasEJB3.log("encontrando ExclusionAumentoVarioTipoPolizaDTO relacionadas con el tipoPoliza: "+idToTipoPoliza+" y con coberturas vigentes", Level.INFO, null);
			try {
			final String queryString = "select model from ExclusionAumentoVarioTipoPolizaDTO model where model.id.idtotipopoliza = :idToTipoPoliza and" +
				"(model.coberturaDTO.claveActivo <> 0 ) and " +
				"(model.coberturaDTO.idToCobertura in (select cobSec.id.idtocobertura from CoberturaSeccionDTO cobSec where cobSec.id.idtoseccion in" +
				"(select sec.idToSeccion from SeccionDTO sec where (sec.claveEstatus <> 3) and " +
				"sec.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza)))";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
}