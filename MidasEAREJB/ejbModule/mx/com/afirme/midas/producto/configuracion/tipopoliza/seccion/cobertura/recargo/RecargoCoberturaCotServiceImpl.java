package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo;

import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class RecargoCoberturaCotServiceImpl implements RecargoCoberturaCotService {

	@PersistenceContext
	private EntityManager entityManager;
	
	
	@Override
	public void bulkDelete(Long idToCotizacion, Long numeroInciso,
			Long idToSeccion, Long idToRecargoVario) {
		bulkDelete(idToCotizacion, numeroInciso, idToSeccion, Arrays.asList(idToRecargoVario));
	}
	
	@Override
	public void bulkDelete(Long idToCotizacion, Long numeroInciso,
			Long idToSeccion, List<Long> idToRecargoVarios) {
		String jpql = "delete from RecargoCoberturaCot dc where " 
				+ "dc.id.idToCotizacion = :idToCotizacion "
				+ "and dc.id.numeroInciso = :numeroInciso " 
				+ "and dc.id.idToSeccion = :idToSeccion " 
				+ "and dc.id.idToRecargoVario in :idToRecargoVarios";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("idToCotizacion", idToCotizacion)
			.setParameter("numeroInciso", numeroInciso)
			.setParameter("idToSeccion", idToSeccion)
			.setParameter("idToRecargoVarios", idToRecargoVarios);
		query.executeUpdate();
		entityManager.getEntityManagerFactory().getCache().evict(RecargoCoberturaCot.class);
	}

}
