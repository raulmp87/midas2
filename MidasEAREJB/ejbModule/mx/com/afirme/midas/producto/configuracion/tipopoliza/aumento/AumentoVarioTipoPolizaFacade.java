package mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity AumentoVarioTipoPolizaDTO.
 * @see .AumentoVarioTipoPolizaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class AumentoVarioTipoPolizaFacade  implements AumentoVarioTipoPolizaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved AumentoVarioTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AumentoVarioTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AumentoVarioTipoPolizaDTO entity) {
    				LogDeMidasEJB3.log("saving AumentoVarioTipoPolizaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent AumentoVarioTipoPolizaDTO entity.
	  @param entity AumentoVarioTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AumentoVarioTipoPolizaDTO entity) {
    				LogDeMidasEJB3.log("deleting AumentoVarioTipoPolizaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(AumentoVarioTipoPolizaDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved AumentoVarioTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the AumentoVarioTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AumentoVarioTipoPolizaDTO entity to update
	 @return AumentoVarioTipoPolizaDTO the persisted AumentoVarioTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public AumentoVarioTipoPolizaDTO update(AumentoVarioTipoPolizaDTO entity) {
    				LogDeMidasEJB3.log("updating AumentoVarioTipoPolizaDTO instance", Level.INFO, null);
	        try {
            AumentoVarioTipoPolizaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
            			entityManager.flush();
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public AumentoVarioTipoPolizaDTO findById( AumentoVarioTipoPolizaId id) {
    				LogDeMidasEJB3.log("finding AumentoVarioTipoPolizaDTO instance with id: " + id, Level.INFO, null);
	        try {
            AumentoVarioTipoPolizaDTO instance = entityManager.find(AumentoVarioTipoPolizaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all AumentoVarioTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AumentoVarioTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<AumentoVarioTipoPolizaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<AumentoVarioTipoPolizaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding AumentoVarioTipoPolizaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from AumentoVarioTipoPolizaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all AumentoVarioTipoPolizaDTO entities.
	  	  @return List<AumentoVarioTipoPolizaDTO> all AumentoVarioTipoPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<AumentoVarioTipoPolizaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all AumentoVarioTipoPolizaDTO instances", Level.INFO, null);
			try {
			    	final String queryString = "select model from AumentoVarioTipoPolizaDTO model";
				Query query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}