package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo;

// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity RecargoVarioCoberturaDTO.
 * 
 * @see .RecargoVarioCoberturaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class RecargoVarioCoberturaFacade implements
		RecargoVarioCoberturaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved RecargoVarioCoberturaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            RecargoVarioCoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RecargoVarioCoberturaDTO entity) {
		LogDeMidasEJB3.log("saving RecargoVarioCoberturaDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent RecargoVarioCoberturaDTO entity.
	 * 
	 * @param entity
	 *            RecargoVarioCoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RecargoVarioCoberturaDTO entity) {
		LogDeMidasEJB3.log("deleting RecargoVarioCoberturaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(RecargoVarioCoberturaDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved RecargoVarioCoberturaDTO entity and return it
	 * or a copy of it to the sender. A copy of the RecargoVarioCoberturaDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            RecargoVarioCoberturaDTO entity to update
	 * @return RecargoVarioCoberturaDTO the persisted RecargoVarioCoberturaDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RecargoVarioCoberturaDTO update(RecargoVarioCoberturaDTO entity) {
		LogDeMidasEJB3.log("updating RecargoVarioCoberturaDTO instance",
				Level.INFO, null);
		try {
			RecargoVarioCoberturaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RecargoVarioCoberturaDTO findById(RecargoVarioCoberturaId id) {
		LogDeMidasEJB3.log(
				"finding RecargoVarioCoberturaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			RecargoVarioCoberturaDTO instance = entityManager.find(
					RecargoVarioCoberturaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RecargoVarioCoberturaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the RecargoVarioCoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RecargoVarioCoberturaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RecargoVarioCoberturaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding RecargoVarioCoberturaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RecargoVarioCoberturaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RecargoVarioCoberturaDTO entities.
	 * 
	 * @return List<RecargoVarioCoberturaDTO> all RecargoVarioCoberturaDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<RecargoVarioCoberturaDTO> findAll() {
		LogDeMidasEJB3.log("finding all RecargoVarioCoberturaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from RecargoVarioCoberturaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RecargoVarioCoberturaDTO> buscarRecargoCobertura(
			BigDecimal idToCobertura, BigDecimal idToRecargoVario) {
		String queryString = "select model from RecargoVarioCoberturaDTO as model";
		queryString += " where model.recargoVarioDTO.idtorecargovario = :idToRecargoVario and model.coberturaDTO.idToCobertura = :idToCobertura";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToRecargoVario", idToRecargoVario);
		query.setParameter("idToCobertura", idToCobertura);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
}