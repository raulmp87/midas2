package mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo;

// default package

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity RecargoVarioTipoPolizaDTO.
 * 
 * @see .RecargoVarioTipoPolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class RecargoVarioTipoPolizaFacade implements
		RecargoVarioTipoPolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved RecargoVarioTipoPolizaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            RecargoVarioTipoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RecargoVarioTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("saving RecargoVarioTipoPolizaDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent RecargoVarioTipoPolizaDTO entity.
	 * 
	 * @param entity
	 *            RecargoVarioTipoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RecargoVarioTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("deleting RecargoVarioTipoPolizaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					RecargoVarioTipoPolizaDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved RecargoVarioTipoPolizaDTO entity and return it
	 * or a copy of it to the sender. A copy of the RecargoVarioTipoPolizaDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            RecargoVarioTipoPolizaDTO entity to update
	 * @return RecargoVarioTipoPolizaDTO the persisted RecargoVarioTipoPolizaDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RecargoVarioTipoPolizaDTO update(RecargoVarioTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("updating RecargoVarioTipoPolizaDTO instance",
				Level.INFO, null);
		try {
			RecargoVarioTipoPolizaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RecargoVarioTipoPolizaDTO findById(RecargoVarioTipoPolizaId id) {
		LogDeMidasEJB3.log(
				"finding RecargoVarioTipoPolizaDTO instance with id: " + id,
				Level.INFO, null);
		try {
			RecargoVarioTipoPolizaDTO instance = entityManager.find(
					RecargoVarioTipoPolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RecargoVarioTipoPolizaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the RecargoVarioTipoPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RecargoVarioTipoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RecargoVarioTipoPolizaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding RecargoVarioTipoPolizaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RecargoVarioTipoPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RecargoVarioTipoPolizaDTO entities.
	 * 
	 * @return List<RecargoVarioTipoPolizaDTO> all RecargoVarioTipoPolizaDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<RecargoVarioTipoPolizaDTO> findAll() {
		LogDeMidasEJB3.log("finding all RecargoVarioTipoPolizaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from RecargoVarioTipoPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}