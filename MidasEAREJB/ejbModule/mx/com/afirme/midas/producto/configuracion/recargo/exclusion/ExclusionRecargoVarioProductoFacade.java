package mx.com.afirme.midas.producto.configuracion.recargo.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ExclusionRecargoVarioProducto.
 * 
 * @see .ExclusionRecargoVarioProducto
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ExclusionRecargoVarioProductoFacade implements
		ExclusionRecargoVarioProductoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * ExclusionRecargoVarioProducto entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ExclusionRecargoVarioProducto entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ExclusionRecargoVarioProductoDTO entity) {
		LogDeMidasEJB3.log("saving ExclusionRecargoVarioProducto instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ExclusionRecargoVarioProducto entity.
	 * 
	 * @param entity
	 *            ExclusionRecargoVarioProducto entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ExclusionRecargoVarioProductoDTO entity) {
		LogDeMidasEJB3.log(
				"deleting ExclusionRecargoVarioProductoDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					ExclusionRecargoVarioProductoDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ExclusionRecargoVarioProductoDTO entity and
	 * return it or a copy of it to the sender. A copy of the
	 * ExclusionRecargoVarioProductoDTO entity parameter is returned when the
	 * JPA persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            ExclusionRecargoVarioProductoDTO entity to update
	 * @return ExclusionRecargoVarioProductoDTO the persisted
	 *         ExclusionRecargoVarioProductoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ExclusionRecargoVarioProductoDTO update(
			ExclusionRecargoVarioProductoDTO entity) {
		LogDeMidasEJB3.log(
				"updating ExclusionRecargoVarioProductoDTO instance",
				Level.INFO, null);
		try {
			ExclusionRecargoVarioProductoDTO result = entityManager
					.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ExclusionRecargoVarioProductoDTO findById(
			ExclusionRecargoVarioProductoId id) {
		LogDeMidasEJB3.log(
				"finding ExclusionRecargoVarioProductoDTO instance with id: "
						+ id, Level.INFO, null);
		try {
			ExclusionRecargoVarioProductoDTO instance = entityManager.find(
					ExclusionRecargoVarioProductoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ExclusionRecargoVarioProductoDTO entities with a specific
	 * property value.
	 * 
	 * @param propertyName
	 *            the name of the ExclusionRecargoVarioProductoDTO property to
	 *            query
	 * @param value
	 *            the property value to match
	 * @return List<ExclusionRecargoVarioProductoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionRecargoVarioProductoDTO> findByProperty(
			String propertyName, final Object value) {
		LogDeMidasEJB3.log(
				"finding ExclusionRecargoVarioProductoDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionRecargoVarioProductoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ExclusionRecargoVarioProductoDTO entities.
	 * 
	 * @return List<ExclusionRecargoVarioProductoDTO> all
	 *         ExclusionRecargoVarioProductoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionRecargoVarioProductoDTO> findAll() {
		LogDeMidasEJB3.log(
				"finding all ExclusionRecargoVarioProductoDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionRecargoVarioProductoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Encuentra los registros de ExclusionRecargoVarioProductoDTO relacionados con el ProductoDTO cuyo ID se recibe y que adem�s est�n 
	 * relacionados s�lo con Tipos de p�liza que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToProducto. El ID del producto
	  @return List<ExclusionRecargoVarioProductoDTO> encontrados por el query formado.
	 */
    @SuppressWarnings("unchecked")
    public List<ExclusionRecargoVarioProductoDTO> getVigentesPorIdProducto(BigDecimal idToProducto) {
    	LogDeMidasEJB3.log("encontrando ExclusionRecargoVarioProductoDTO relacionadas con el producto: "+idToProducto+" y con tipos de p�liza vigentes", Level.INFO, null);
			try {
			final String queryString = "select model from ExclusionRecargoVarioProductoDTO model where model.id.idtoproducto = :idToProducto and" +
					"(model.tipoPolizaDTO.claveEstatus <> 3 )";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToProducto", idToProducto);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
}