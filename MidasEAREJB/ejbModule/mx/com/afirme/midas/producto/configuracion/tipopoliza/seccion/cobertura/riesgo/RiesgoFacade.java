package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity RiesgoDTO.
 * 
 * @see .RiesgoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class RiesgoFacade implements RiesgoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved RiesgoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            RiesgoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RiesgoDTO entity) {
		LogDeMidasEJB3.log("saving RiesgoDTO instance", Level.INFO, null);
		try {
			if (entity.getIdToRiesgo() == null) {
				entity.setVersion(Integer.valueOf(1));
			}
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent RiesgoDTO entity.
	 * 
	 * @param entity
	 *            RiesgoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoDTO entity) {
		LogDeMidasEJB3.log("deleting RiesgoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(RiesgoDTO.class, entity
					.getIdToRiesgo());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved RiesgoDTO entity and return it or a copy of it
	 * to the sender. A copy of the RiesgoDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            RiesgoDTO entity to update
	 * @return RiesgoDTO the persisted RiesgoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoDTO update(RiesgoDTO entity) {
		LogDeMidasEJB3.log("updating RiesgoDTO instance", Level.INFO, null);
		try {
			RiesgoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RiesgoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding RiesgoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			RiesgoDTO instance = entityManager.find(RiesgoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding RiesgoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoDTO entities.
	 * 
	 * @return List<RiesgoDTO> all RiesgoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoDTO> findAll() {
		LogDeMidasEJB3.log("finding all RiesgoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoDTO entities with depends of a filter.
	 * 
	 * @param entity
	 *            the name of the entity RiesgoDTO
	 * @return List<CoberturaDTO> RiesgoDTO found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoDTO> listarFiltrado(RiesgoDTO entity, Boolean mostrarInactivos) {
		try {
			String queryString = "select model from RiesgoDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "codigo", entity.getCodigo());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "version", entity.getVersion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "descripcion", entity.getDescripcion());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "nombreComercial", entity.getNombreComercial());
			/*sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveTipoSumaAsegurada", entity.getClaveTipoSumaAsegurada());*/
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "subRamoDTO.idTcSubRamo", entity.getSubRamoDTO().getIdTcSubRamo());
			/*sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "numeroSecuencia", entity.getNumeroSecuencia());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveActivoConfiguracion", entity.getClaveActivoConfiguracion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "claveActivoProduccion", entity.getClaveActivoProduccion());*/

			if(!mostrarInactivos) {
				sWhere += " and model.claveEstatus IN (0,1)";
			}else{
				sWhere += " and model.claveEstatus IN (1,2,3)";
			}

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarFiltrado failed at RiesgoFacade",
					Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Borra l�gicamente un registro de Riesgo, estableciendo en 0 los campos 
	 * CLAVEACTIVO y CLAVEESTATUS
	 * 
	 * @param entity
	 *            RiesgoDTO entity el registro Riesgo a borrar
	 * @throws RuntimeException
	 *             si la operacion falla
	 */
	public RiesgoDTO borradoLogico(RiesgoDTO entity){
		LogDeMidasEJB3.log("logical delete RiesgoDTO instance", Level.INFO, null);
		try {
			entity.setClaveEstatus((short)3);
			entity.setClaveActivo((short)0);
			RiesgoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("logical delete successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("logical delete failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Busca los registros de Riesgo que no hayan sido borrados logicamente
	 * 
	 * @return List<RiesgoDTO> registros de Riesgo que no han sido borrados logicamente
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoDTO> listarVigentes(){
		LogDeMidasEJB3.log("finding all active RiesgoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoDTO model where model.claveEstatus <> 3";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all active failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**M�todo listarRiesgosPorAsociarCobertura.
	 * Busca los registros VIGENTES de Riesgo que no est�n asociados a la cobertura cuyo ID se recibe.
	 * 
	 * @param BigDecimal idToCobertura. El id de la cobertura
	 * @return List<RiesgoDTO> registros de Riesgo que est�n asociados a la cobertura.
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoDTO> listarRiesgosPorAsociarCobertura(CoberturaDTO coberturaDTO,BigDecimal idToSeccion) {
		String queryString = "select model from RiesgoDTO as model ";
		queryString += "where model.idToRiesgo not in (select r.id.idtoriesgo from RiesgoCoberturaDTO r where r.id.idtocobertura = :idToCobertura" +
				" and r.id.idtoseccion = :idToSeccion)" +
				"and (model.claveEstatus <> 3) " +
				"and model.subRamoDTO.idTcSubRamo = :idTcSubRamo " +
				"and model.subRamoDTO.ramoDTO.idTcRamo = :idTcRamo order by model.codigo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCobertura", coberturaDTO.getIdToCobertura());
		query.setParameter("idTcRamo", coberturaDTO.getSubRamoDTO().getRamoDTO().getIdTcRamo());
		query.setParameter("idTcSubRamo", coberturaDTO.getSubRamoDTO().getIdTcSubRamo());
		query.setParameter("idToSeccion", idToSeccion);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
	
	/**M�todo listarRiesgosVigentesPorCoberturaSeccion.
	 * Busca los registros de Riesgo que est�n asociados a la coberturaSeccion cuyos ID se reciben.
	 * @param BigDecimal idToCobertura. El id de la cobertura
	 * @param BigDecimal idToSeccion
	 * @return List<RiesgoDTO> registros de Riesgo que est�n asociados a la cobertura.
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoDTO> listarRiesgosVigentesPorCoberturaSeccion(BigDecimal idToCobertura,BigDecimal idToSeccion) {
		String queryString = "select model from RiesgoDTO as model where model.idToRiesgo in " +
				"(select riesgoCob.id.idtoriesgo from RiesgoCoberturaDTO riesgoCob where riesgoCob.id.idtocobertura = :idToCobertura and " +
				"riesgoCob.id.idtoseccion = :idToSeccion) and (model.claveEstatus <> 3)";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCobertura", idToCobertura);
		query.setParameter("idToSeccion", idToSeccion);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}

	public Long countRiesgoCobertura(BigDecimal idToRiesgo) {
		LogDeMidasEJB3.log(
				"counting RiesgoCoberturaDTO instances with idToRiesgo: "
						+ idToRiesgo, Level.INFO, null);
		try {
			final String queryString = "select count(model.id) from RiesgoCoberturaDTO model where model.id.idtoriesgo = :idToRiesgo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToRiesgo", idToRiesgo);
			return (Long) query.getSingleResult();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("counting RiesgoCoberturaDTO instances failed",
					Level.SEVERE, re);
			throw re;
		}
	}

	public Long countRiesgoTarifa(BigDecimal idToRiesgo) {
		LogDeMidasEJB3.log(
				"counting TarifaDTO instances with idToRiesgo: "
						+ idToRiesgo, Level.INFO, null);
		try {
			final String queryString = "select count(model.id) from TarifaDTO model where model.id.idToRiesgo = :idToRiesgo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToRiesgo", idToRiesgo);
			return (Long) query.getSingleResult();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("counting TarifaDTO instances failed",
					Level.SEVERE, re);
			throw re;
		}
	}
}