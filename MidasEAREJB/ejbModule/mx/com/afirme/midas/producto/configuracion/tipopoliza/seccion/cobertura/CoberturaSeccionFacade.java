package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity CoberturaSeccionDTO.
 * @see .CoberturaSeccionDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class CoberturaSeccionFacade  implements CoberturaSeccionFacadeRemote {
	//property constants
	public static final String CLAVEOBLIGATORIEDAD = "claveobligatoriedad";

    @PersistenceContext private EntityManager entityManager;	
		/**
	 Perform an initial save of a previously unsaved CoberturaSeccionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CoberturaSeccionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CoberturaSeccionDTO entity) {
    				LogDeMidasEJB3.log("saving CoberturaSeccionDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CoberturaSeccionDTO entity.
	  @param entity CoberturaSeccionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CoberturaSeccionDTO entity) {
    				LogDeMidasEJB3.log("deleting CoberturaSeccionDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(CoberturaSeccionDTO.class, entity.getId());
            entityManager.remove(entity);
            entityManager.flush();
            LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CoberturaSeccionDTO entity and return it or a copy of it to the sender. 
	 A copy of the CoberturaSeccionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CoberturaSeccionDTO entity to update
	 @return CoberturaSeccionDTO the persisted CoberturaSeccionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CoberturaSeccionDTO update(CoberturaSeccionDTO entity) {
    				LogDeMidasEJB3.log("updating CoberturaSeccionDTO instance", Level.INFO, null);
	        try {
            CoberturaSeccionDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public CoberturaSeccionDTO findById( CoberturaSeccionDTOId id) {
    				LogDeMidasEJB3.log("finding CoberturaSeccionDTO instance with id: " + id, Level.INFO, null);
	        try {
            CoberturaSeccionDTO instance = entityManager.find(CoberturaSeccionDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all CoberturaSeccionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the CoberturaSeccionDTO property to query
	  @param value the property value to match
	  	  @return List<CoberturaSeccionDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<CoberturaSeccionDTO> findByProperty(String propertyName, final Object value) {
    				LogDeMidasEJB3.log("finding CoberturaSeccionDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from CoberturaSeccionDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	public List<CoberturaSeccionDTO> findByClaveobligatoriedad(Object claveobligatoriedad) {
		return findByProperty(CLAVEOBLIGATORIEDAD, claveobligatoriedad);
	}
	
	/**
	 * Find all CoberturaSeccionDTO entities.
	  	  @return List<CoberturaSeccionDTO> all CoberturaSeccionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaSeccionDTO> findAll() {
		LogDeMidasEJB3.log("finding all CoberturaSeccionDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from CoberturaSeccionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Lista todas las coberturas que no est�n asociadas una secci�n determinada 
	 * por el id que recibe el m�todo
	 * @param BigDecimal idToSeccion el id de la seccion.
	 * @return List<CoberturaSeccionDTO> registros CoberturaSeccion que no est�n asociados a la seccion.
	 */
	@SuppressWarnings("unchecked")
	public List<CoberturaSeccionDTO> obtenerCoberturasSinAsociar(BigDecimal idToSeccion){
		LogDeMidasEJB3.log("Encontrando las coberturas no asociadas a la seccion: " + idToSeccion, Level.INFO, null);
		try {
			final String queryString = "select model from CoberturaSeccionDTO model where " +
					"model.seccionDTO.idToSeccion <> :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fallo en encontrar las coberturas no asiciadas a la seccion: "+idToSeccion, Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Encuentra todas las entidades de CoberturaSeccionDTO relacionadas con la Seccion cuyo ID se recibe, y que adem�s est�n relacionadas
	 * s�lo con coberturas que no han sido borradas l�gicamente.  
	  @param idToSeccion
  	  @return List<CoberturaSeccionDTO> lista de registros encontrados por el query formado.
	 */
    @SuppressWarnings("unchecked")
    public List<CoberturaSeccionDTO> getVigentesPorSeccion(BigDecimal idToSeccion) {
    	LogDeMidasEJB3.log("buscando CoberturaSeccionDTO relacionadas con la seccion: " + idToSeccion + ", y relacionados con coberturas vigentes: ", Level.INFO, null);
		try {
			final String queryString = "select model from CoberturaSeccionDTO model where model.id.idtoseccion = :idToSeccion and (" +
				"model.coberturaDTO.claveActivo <> 0) order by model.coberturaDTO.codigo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

    public Long countCobeturaSeccion(BigDecimal idToCobertura) {
		LogDeMidasEJB3.log(
				"counting CoberturaSeccionDTO instances with idToCobertura: "
						+ idToCobertura, Level.INFO, null);
		try {
			final String queryString = "select count(model.id) from CoberturaSeccionDTO model where model.id.idtocobertura = :idToCobertura";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCobertura", idToCobertura);
			return (Long) query.getSingleResult();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("counting CoberturaSeccionDTO instances failed",
					Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaSeccionDTO> listCoberturaSumaAseguradaDependientes(
			BigDecimal idCoberturaSumaAsegurada, BigDecimal idSeccion) {
		try {
			String sql = "select model from CoberturaSeccionDTO model where " +
					"model.id.idtoseccion = :idToSeccion and model.coberturaDTO.idCoberturaSumaAsegurada = :idCoberturaSumaAsegurada";
			Query query = entityManager.createQuery(sql)
					.setParameter("idToSeccion", idSeccion)
					.setParameter("idCoberturaSumaAsegurada", idCoberturaSumaAsegurada);
			return (List<CoberturaSeccionDTO>)query.getResultList();
		} catch(RuntimeException re) {
			LogDeMidasEJB3.log("counting CoberturaSeccionDTO instances failed",
					Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<CoberturaSeccionDTO> listarFiltrado(CoberturaSeccionDTO entity){
		try {
			String queryString = "select model from CoberturaSeccionDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			
			if(entity.getId() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idtoseccion", entity.getId().getIdtoseccion());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idtocobertura", entity.getId().getIdtocobertura());
			}
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveObligatoriedad", entity.getClaveObligatoriedad());
			
			if(entity.getCoberturaDTO() != null){
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "coberturaDTO.claveTipoSumaAsegurada", entity.getCoberturaDTO().getClaveTipoSumaAsegurada());
				sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "coberturaDTO.idCoberturaSumaAsegurada", entity.getCoberturaDTO().getIdCoberturaSumaAsegurada());
			}
			
			if(entity.getSeccionDTO() != null){
				if(entity.getSeccionDTO().getTipoPolizaDTO() != null){
					sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "seccionDTO.tipoPolizaDTO.idToTipoPoliza", entity.getSeccionDTO().getTipoPolizaDTO().getIdToTipoPoliza());
				}
			}

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("listarFiltrado failed", Level.SEVERE, re);
			throw re;
		}
	}
}