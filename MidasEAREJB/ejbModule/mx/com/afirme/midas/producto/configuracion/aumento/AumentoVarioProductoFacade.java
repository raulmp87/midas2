package mx.com.afirme.midas.producto.configuracion.aumento;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity AumentoVarioProductoDTO.
 * @see .AumentoVarioProductoDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class AumentoVarioProductoFacade  implements AumentoVarioProductoFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved AumentoVarioProductoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AumentoVarioProductoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AumentoVarioProductoDTO entity) {
    				LogDeMidasEJB3.log("saving AumentoVarioProductoDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent AumentoVarioProductoDTO entity.
	  @param entity AumentoVarioProductoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AumentoVarioProductoDTO entity) {
    				LogDeMidasEJB3.log("deleting AumentoVarioProductoDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(AumentoVarioProductoDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved AumentoVarioProductoDTO entity and return it or a copy of it to the sender. 
	 A copy of the AumentoVarioProductoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AumentoVarioProductoDTO entity to update
	 @return AumentoVarioProductoDTO the persisted AumentoVarioProductoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public AumentoVarioProductoDTO update(AumentoVarioProductoDTO entity) {
    				LogDeMidasEJB3.log("updating AumentoVarioProductoDTO instance", Level.INFO, null);
	        try {
            AumentoVarioProductoDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public AumentoVarioProductoDTO findById( AumentoVarioProductoId id) {
    				LogDeMidasEJB3.log("finding AumentoVarioProductoDTO instance with id: " + id, Level.INFO, null);
	        try {
            AumentoVarioProductoDTO instance = entityManager.find(AumentoVarioProductoDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all AumentoVarioProductoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AumentoVarioProductoDTO property to query
	  @param value the property value to match
	  	  @return List<AumentoVarioProductoDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<AumentoVarioProductoDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding AumentoVarioProductoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from AumentoVarioProductoDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all AumentoVarioProductoDTO entities.
	  	  @return List<AumentoVarioProductoDTO> all AumentoVarioProductoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<AumentoVarioProductoDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all AumentoVarioProductoDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from AumentoVarioProductoDTO model";
								Query query = entityManager.createQuery(queryString);
								query.setHint(QueryHints.REFRESH, HintValues.TRUE);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}