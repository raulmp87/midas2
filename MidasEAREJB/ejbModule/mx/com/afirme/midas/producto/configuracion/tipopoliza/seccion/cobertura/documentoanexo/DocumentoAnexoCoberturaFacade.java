package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity DocumentoAnexoCoberturaDTO.
 * @see .DocumentoAnexoCoberturaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class DocumentoAnexoCoberturaFacade  implements DocumentoAnexoCoberturaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DocumentoAnexoCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocumentoAnexoCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocumentoAnexoCoberturaDTO entity) {
    				LogDeMidasEJB3.log("saving DocumentoAnexoCoberturaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DocumentoAnexoCoberturaDTO entity.
	  @param entity DocumentoAnexoCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocumentoAnexoCoberturaDTO entity) {
    				LogDeMidasEJB3.log("deleting DocumentoAnexoCoberturaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DocumentoAnexoCoberturaDTO.class, entity.getIdToDocumentoAnexoCobertura());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DocumentoAnexoCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocumentoAnexoCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocumentoAnexoCoberturaDTO entity to update
	 @return DocumentoAnexoCoberturaDTO the persisted DocumentoAnexoCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DocumentoAnexoCoberturaDTO update(DocumentoAnexoCoberturaDTO entity) {
    				LogDeMidasEJB3.log("updating DocumentoAnexoCoberturaDTO instance", Level.INFO, null);
	        try {
            DocumentoAnexoCoberturaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DocumentoAnexoCoberturaDTO findById( BigDecimal id) {
    				LogDeMidasEJB3.log("finding DocumentoAnexoCoberturaDTO instance with id: " + id, Level.INFO, null);
	        try {
            DocumentoAnexoCoberturaDTO instance = entityManager.find(DocumentoAnexoCoberturaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DocumentoAnexoCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocumentoAnexoCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<DocumentoAnexoCoberturaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DocumentoAnexoCoberturaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding DocumentoAnexoCoberturaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DocumentoAnexoCoberturaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all DocumentoAnexoCoberturaDTO entities.
	  	  @return List<DocumentoAnexoCoberturaDTO> all DocumentoAnexoCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DocumentoAnexoCoberturaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all DocumentoAnexoCoberturaDTO instances", Level.INFO, null);
			try {
			    	final String queryString = "select model from DocumentoAnexoCoberturaDTO model";
				Query query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DocumentoAnexoCoberturaDTO> listarAnexosPorCotizacion(BigDecimal idToCotizacion) {
		LogDeMidasEJB3.log("finding all DocumentoAnexoCoberturaDTO instances", Level.INFO, null);
		try {
			String queryString = "SELECT DISTINCT con.nombrearchivooriginal, cob.idtodocanexocobertura, cob.numerosecuencia " +
					"FROM MIDAS.todocumentoanexocobertura cob, MIDAS.tocontrolarchivo con " +
					"WHERE cob.idtocontrolarchivo = con.idtocontrolarchivo " +
					"AND cob.idtocobertura IN (" +
					"SELECT DISTINCT cot.idtocobertura " +
					"FROM MIDAS.tocoberturacot cot " +
					"WHERE cot.idtocotizacion = " + idToCotizacion + " " + 
					"AND cot.clavecontrato = 1 " +
					") order by cob.numerosecuencia";
			Query query = entityManager.createNativeQuery(queryString);
			List list = query.getResultList();
			List<DocumentoAnexoCoberturaDTO> result = new ArrayList<DocumentoAnexoCoberturaDTO>();
			if(list != null && !list.isEmpty()) {
				for(Object obj : list) {
					if(obj instanceof List) {
						BigDecimal idDoc = (BigDecimal) ((List) obj).get(1);
						DocumentoAnexoCoberturaDTO documento = this.findById(idDoc);
						result.add(documento);
					} else if(obj instanceof Object[]) {
						BigDecimal idDoc = (BigDecimal) ((Object[])obj)[1];
						DocumentoAnexoCoberturaDTO documento = this.findById(idDoc);
						result.add(documento);
					}
				}
			}
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<DocumentoAnexoCoberturaDTO> listarAnexosPorCoberturas(List<BigDecimal> lstCoberturasDtoId) {
		LogDeMidasEJB3.log("finding all DocumentoAnexoCoberturaDTO instances", Level.INFO, null);
		try {
		
			String queryString = "select model from DocumentoAnexoCoberturaDTO model where model.coberturaDTO.idToCobertura " +
					" IN :lstCoberturas";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("lstCoberturas", lstCoberturasDtoId);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 

			List<DocumentoAnexoCoberturaDTO> result = query.getResultList();
			
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
}