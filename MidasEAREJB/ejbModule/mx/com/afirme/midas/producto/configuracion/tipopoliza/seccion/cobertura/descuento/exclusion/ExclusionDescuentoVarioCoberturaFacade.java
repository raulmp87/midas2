package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.exclusion;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ExclusionDescuentoVarioCoberturaDTO.
 * @see .ExclusionDescuentoVarioCoberturaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class ExclusionDescuentoVarioCoberturaFacade  implements ExclusionDescuentoVarioCoberturaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved ExclusionDescuentoVarioCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ExclusionDescuentoVarioCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ExclusionDescuentoVarioCoberturaDTO entity) {
    				LogDeMidasEJB3.log("saving ExclusionDescuentoVarioCoberturaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent ExclusionDescuentoVarioCoberturaDTO entity.
	  @param entity ExclusionDescuentoVarioCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ExclusionDescuentoVarioCoberturaDTO entity) {
    				LogDeMidasEJB3.log("deleting ExclusionDescuentoVarioCoberturaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(ExclusionDescuentoVarioCoberturaDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved ExclusionDescuentoVarioCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExclusionDescuentoVarioCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExclusionDescuentoVarioCoberturaDTO entity to update
	 @return ExclusionDescuentoVarioCoberturaDTO the persisted ExclusionDescuentoVarioCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ExclusionDescuentoVarioCoberturaDTO update(ExclusionDescuentoVarioCoberturaDTO entity) {
    				LogDeMidasEJB3.log("updating ExclusionDescuentoVarioCoberturaDTO instance", Level.INFO, null);
	        try {
            ExclusionDescuentoVarioCoberturaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public ExclusionDescuentoVarioCoberturaDTO findById( ExclusionDescuentoVarioCoberturaId id) {
    				LogDeMidasEJB3.log("finding ExclusionDescuentoVarioCoberturaDTO instance with id: " + id, Level.INFO, null);
	        try {
            ExclusionDescuentoVarioCoberturaDTO instance = entityManager.find(ExclusionDescuentoVarioCoberturaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all ExclusionDescuentoVarioCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ExclusionDescuentoVarioCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<ExclusionDescuentoVarioCoberturaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<ExclusionDescuentoVarioCoberturaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding ExclusionDescuentoVarioCoberturaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			    	final String queryString = "select model from ExclusionDescuentoVarioCoberturaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
				query.setParameter("propertyValue", value);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all ExclusionDescuentoVarioCoberturaDTO entities.
	  	  @return List<ExclusionDescuentoVarioCoberturaDTO> all ExclusionDescuentoVarioCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionDescuentoVarioCoberturaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all ExclusionDescuentoVarioCoberturaDTO instances", Level.INFO, null);
			try {
			    	final String queryString = "select model from ExclusionDescuentoVarioCoberturaDTO model";
				Query query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	/**
	 * Find a ExclusionDescuentoVarioCoberturaDTO entity with the specific
	 * received id�s.
	 * 
	 * @param BigDecimal idToCobertura.
	 * @param BigDecimal idToDescuentoVario.
	 * @param BigDecimal idToRiesgo.
	 * 
	 * @return List<ExclusionAumentoVarioCoberturaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionDescuentoVarioCoberturaDTO> findByIDs(BigDecimal idToCobertura, BigDecimal idToDescuento, BigDecimal idToRiesgo) {
		LogDeMidasEJB3.log("finding ExclusionDescuentoVarioCoberturaDTO instance with idtocobertura: "+ idToCobertura + ", idToDescuento: "+idToDescuento+"idToRiesgo: "+idToRiesgo, Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionDescuentoVarioCoberturaDTO model where model.id.idtocobertura = :idtocobertura and model.id.idtodescuentovario = :idtodescuentovario and model.id.idtoriesgo = :idtoriesgo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idtocobertura", idToCobertura);
			query.setParameter("idtodescuentovario", idToDescuento);
			query.setParameter("idtoriesgo", idToRiesgo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Encuentra los registros de ExclusionDescuentoVarioCoberturaDTO relacionados con la CoberturaSeccionDTO cuyos ID�s se reciben y que adem�s est�n 
	 * relacionados s�lo con Riesgos que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToCobertura. El ID de la cobertura.
	  @param BigDecimal idToSeccion. El ID de la seccion.
	  @return List<ExclusionDescuentoVarioCoberturaDTO> encontrados por el query formado.
	 */
    @SuppressWarnings("unchecked")
    public List<ExclusionDescuentoVarioCoberturaDTO> getVigentesPorIdCoberturaIdSeccion(BigDecimal idToCobertura,BigDecimal idToSeccion) {
    	LogDeMidasEJB3.log("encontrando ExclusionDescuentoVarioCoberturaDTO relacionadas con la cobertura: "+idToCobertura+" perteneciente a la secci�n: "+idToSeccion+"y con Riesgos Vigentes", Level.INFO, null);
			try {
			final String queryString = "select model from ExclusionDescuentoVarioCoberturaDTO model where model.id.idtocobertura = :idToCobertura and" +
					"(model.riesgoDTO.claveEstatus <> 3 ) and "+
					"(model.id.idtocobertura in (select cobSeccion.id.idtocobertura from CoberturaSeccionDTO cobSeccion " +
					"where cobSeccion.id.idtoseccion = :idToSeccion))";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCobertura", idToCobertura);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
}