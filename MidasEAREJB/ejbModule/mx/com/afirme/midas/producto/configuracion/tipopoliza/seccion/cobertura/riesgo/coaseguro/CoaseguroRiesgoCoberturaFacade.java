package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity CoaseguroRiesgoCoberturaDTO.
 * @see .CoaseguroRiesgoCoberturaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class CoaseguroRiesgoCoberturaFacade  implements CoaseguroRiesgoCoberturaFacadeRemote {
	//property constants
	public static final String VALOR = "valor";
	public static final String CLAVE_DEFAULT = "claveDefault";

    @PersistenceContext private EntityManager entityManager;
    @Resource
	private SessionContext context;
	
		/**
	 Perform an initial save of a previously unsaved CoaseguroRiesgoCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CoaseguroRiesgoCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CoaseguroRiesgoCoberturaDTO entity) {
    	LogDeMidasEJB3.log("saving CoaseguroRiesgoCoberturaDTO instance", Level.INFO, null);
    	try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CoaseguroRiesgoCoberturaDTO entity.
	  @param entity CoaseguroRiesgoCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CoaseguroRiesgoCoberturaDTO entity) {
    				LogDeMidasEJB3.log("deleting CoaseguroRiesgoCoberturaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(CoaseguroRiesgoCoberturaDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CoaseguroRiesgoCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the CoaseguroRiesgoCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CoaseguroRiesgoCoberturaDTO entity to update
	 @return CoaseguroRiesgoCoberturaDTO the persisted CoaseguroRiesgoCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CoaseguroRiesgoCoberturaDTO update(CoaseguroRiesgoCoberturaDTO entity) {
    				LogDeMidasEJB3.log("updating CoaseguroRiesgoCoberturaDTO instance", Level.INFO, null);
	        try {
            CoaseguroRiesgoCoberturaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
            			entityManager.flush();
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public CoaseguroRiesgoCoberturaDTO findById( CoaseguroRiesgoCoberturaId id) {
    				LogDeMidasEJB3.log("finding CoaseguroRiesgoCoberturaDTO instance with id: " + id, Level.INFO, null);
	        try {
            CoaseguroRiesgoCoberturaDTO instance = entityManager.find(CoaseguroRiesgoCoberturaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all CoaseguroRiesgoCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the CoaseguroRiesgoCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<CoaseguroRiesgoCoberturaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<CoaseguroRiesgoCoberturaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding CoaseguroRiesgoCoberturaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from CoaseguroRiesgoCoberturaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	public List<CoaseguroRiesgoCoberturaDTO> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CoaseguroRiesgoCoberturaDTO> findByClaveDefault(Object claveDefault
	) {
		return findByProperty(CLAVE_DEFAULT, claveDefault
		);
	}
	
	
	/**
	 * Find all CoaseguroRiesgoCoberturaDTO entities.
	  	  @return List<CoaseguroRiesgoCoberturaDTO> all CoaseguroRiesgoCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<CoaseguroRiesgoCoberturaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all CoaseguroRiesgoCoberturaDTO instances", Level.INFO, null);
			try {
			    		final String queryString = "select model from CoaseguroRiesgoCoberturaDTO model";
					Query query = entityManager.createQuery(queryString);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<CoaseguroRiesgoCoberturaDTO> findByRiesgoCoberturaSeccion(BigDecimal idToRiesgo,BigDecimal idToCobertura,BigDecimal idToSeccion) {
		LogDeMidasEJB3.log("finding all CoaseguroRiesgoCoberturaDTO instances with idToRiesgo: "+idToRiesgo+" and idToCobertura: "+idToCobertura+" and idToSeccion: "+idToSeccion, Level.INFO, null);
		try {
			final String queryString = "select model from CoaseguroRiesgoCoberturaDTO model where model.id.idToRiesgo = :idToRiesgo and model.id.idToCobertura = :idToCobertura and model.id.idToSeccion = :idToSeccion order by model.valor";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToRiesgo",idToRiesgo);
			query.setParameter("idToCobertura",idToCobertura);
			query.setParameter("idToSeccion",idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void synchronizeCoasegurosRiesgoCobertura(BigDecimal idToRiesgo, BigDecimal idToCobertura, BigDecimal idToSeccion) {
		LogDeMidasEJB3.log(
				"synchronizing CoaseguroRiesgoCoberturaDTO instances with idToRiesgo: "
						+ idToRiesgo + " and idToCobertura: " + idToCobertura
						+ " and idToSeccion: " + idToSeccion, Level.INFO, null);
		try {
			String queryString = "select model from CoaseguroCoberturaDTO model where model.id.idToCobertura = :idToCobertura " +
					"and model.id.numeroSecuencia not in (select riesgo.id.numeroSecuencia from CoaseguroRiesgoCoberturaDTO riesgo " +
					"where riesgo.id.idToRiesgo = :idToRiesgo and riesgo.id.idToCobertura = :idToCobertura and riesgo.id.idToSeccion = :idToSeccion)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToRiesgo", idToRiesgo);
			query.setParameter("idToCobertura", idToCobertura);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			List<CoaseguroCoberturaDTO> coasegurosCobertura = (List<CoaseguroCoberturaDTO>)query.getResultList();
			
			if(!coasegurosCobertura.isEmpty()) {
				for(CoaseguroCoberturaDTO coaseguroCobertura : coasegurosCobertura) {
					CoaseguroRiesgoCoberturaId id = new CoaseguroRiesgoCoberturaId();
					id.setIdToRiesgo(idToRiesgo);
					id.setIdToCobertura(idToCobertura);
					id.setIdToSeccion(idToSeccion);
					id.setNumeroSecuencia(coaseguroCobertura.getId().getNumeroSecuencia());

					CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCobertura = new CoaseguroRiesgoCoberturaDTO();
					coaseguroRiesgoCobertura.setId(id);
					coaseguroRiesgoCobertura.setClaveDefault(coaseguroCobertura.getClaveDefault());
					coaseguroRiesgoCobertura.setValor(coaseguroCobertura.getValor());
					this.save(coaseguroRiesgoCobertura);
				}
			}
			queryString = "select model from CoaseguroRiesgoCoberturaDTO model where model.id.idToRiesgo = :idToRiesgo " +
					"and model.id.idToCobertura = :idToCobertura and model.id.idToSeccion = :idToSeccion " +
					"and model.id.numeroSecuencia not in (select cobertura.id.numeroSecuencia from CoaseguroCoberturaDTO cobertura " +
					"where cobertura.id.idToCobertura = :idToCobertura)";
			query = entityManager.createQuery(queryString);
			query.setParameter("idToRiesgo", idToRiesgo);
			query.setParameter("idToCobertura", idToCobertura);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			List<CoaseguroRiesgoCoberturaDTO> coasegurosRiesgoCobertura = (List<CoaseguroRiesgoCoberturaDTO>)query.getResultList();
			
			if(!coasegurosRiesgoCobertura.isEmpty()) {
				for(CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCobertura : coasegurosRiesgoCobertura) {
					this.delete(coaseguroRiesgoCobertura);
				}
			}
		} catch (RuntimeException re) {
			context.setRollbackOnly();
			LogDeMidasEJB3.log("synchronizing failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoaseguroRiesgoCoberturaDTO> listarFiltrado(CoaseguroRiesgoCoberturaDTO entity) {
		LogDeMidasEJB3.log(
				"filtering CoaseguroRiesgoCoberturaDTO instance",
						Level.INFO, null);
		try {
			String queryString = "select model from CoaseguroRiesgoCoberturaDTO model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			if (entity == null)
				return null;
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToSeccion", entity.getId().getIdToSeccion());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToCobertura", entity.getId().getIdToCobertura());
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "id.idToRiesgo", entity.getId().getIdToRiesgo());
						
			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
}