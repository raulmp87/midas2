package mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ExclusionRecargoVarioTipoPolizaDTO.
 * 
 * @see .ExclusionRecargoVarioTipoPolizaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class ExclusionRecargoVarioTipoPolizaFacade implements
		ExclusionRecargoVarioTipoPolizaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved
	 * ExclusionRecargoVarioTipoPolizaDTO entity. All subsequent persist actions
	 * of this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ExclusionRecargoVarioTipoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ExclusionRecargoVarioTipoPolizaDTO entity) {
		LogDeMidasEJB3.log(
				"saving ExclusionRecargoVarioTipoPolizaDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			entityManager.flush();
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent ExclusionRecargoVarioTipoPolizaDTO entity.
	 * 
	 * @param entity
	 *            ExclusionRecargoVarioTipoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ExclusionRecargoVarioTipoPolizaDTO entity) {
		LogDeMidasEJB3.log(
				"deleting ExclusionRecargoVarioTipoPolizaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					ExclusionRecargoVarioTipoPolizaDTO.class, entity.getId());
			entityManager.remove(entity);
			entityManager.flush();
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved ExclusionRecargoVarioTipoPolizaDTO entity and
	 * return it or a copy of it to the sender. A copy of the
	 * ExclusionRecargoVarioTipoPolizaDTO entity parameter is returned when the
	 * JPA persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            ExclusionRecargoVarioTipoPolizaDTO entity to update
	 * @return ExclusionRecargoVarioTipoPolizaDTO the persisted
	 *         ExclusionRecargoVarioTipoPolizaDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ExclusionRecargoVarioTipoPolizaDTO update(
			ExclusionRecargoVarioTipoPolizaDTO entity) {
		LogDeMidasEJB3.log(
				"updating ExclusionRecargoVarioTipoPolizaDTO instance",
				Level.INFO, null);
		try {
			ExclusionRecargoVarioTipoPolizaDTO result = entityManager
					.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public ExclusionRecargoVarioTipoPolizaDTO findById(
			ExclusionRecargoVarioTipoPolizaId id) {
		LogDeMidasEJB3.log(
				"finding ExclusionRecargoVarioTipoPolizaDTO instance with id: "
						+ id, Level.INFO, null);
		try {
			entityManager.flush();
			ExclusionRecargoVarioTipoPolizaDTO instance = entityManager.find(
					ExclusionRecargoVarioTipoPolizaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ExclusionRecargoVarioTipoPolizaDTO entities with a specific
	 * property value.
	 * 
	 * @param propertyName
	 *            the name of the ExclusionRecargoVarioTipoPolizaDTO property to
	 *            query
	 * @param value
	 *            the property value to match
	 * @return List<ExclusionRecargoVarioTipoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionRecargoVarioTipoPolizaDTO> findByProperty(
			String propertyName, final Object value) {
		LogDeMidasEJB3.log(
				"finding ExclusionRecargoVarioTipoPolizaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionRecargoVarioTipoPolizaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ExclusionRecargoVarioTipoPolizaDTO entities.
	 * 
	 * @return List<ExclusionRecargoVarioTipoPolizaDTO> all
	 *         ExclusionRecargoVarioTipoPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionRecargoVarioTipoPolizaDTO> findAll() {
		LogDeMidasEJB3.log(
				"finding all ExclusionRecargoVarioTipoPolizaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionRecargoVarioTipoPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * Find a ExclusionRecargoVarioTipoPolizaDTO entity with the specific
	 * received id�s.
	 * 
	 * @param BigDecimal idToTipoPoliza.
	 * @param BigDecimal idToRecargoVario.
	 * @param BigDecimal idToCobertura.
	 * 
	 * @return List<ExclusionRecargoVarioTipoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionRecargoVarioTipoPolizaDTO> findByIDs(BigDecimal idToTipoPoliza, BigDecimal idToRecargoVario, BigDecimal idToCobertura) {
		LogDeMidasEJB3.log("finding ExclusionRecargoVarioTipoPolizaDTO instance with idtotipopoliza: "+ idToTipoPoliza + ": " + idToTipoPoliza+ ", idToRecargoVario: "+idToRecargoVario+"idToCobertura: "+idToCobertura, Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionRecargoVarioTipoPolizaDTO model where model.id.idtotipopoliza = :poliza and model.id.idtocobertura = :cobertura and model.id.idtorecargovario = :recargo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("poliza", idToTipoPoliza);
			query.setParameter("cobertura", idToCobertura);
			query.setParameter("recargo", idToRecargoVario);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Encuentra los registros de ExclusionRecargoVarioTipoPolizaDTO relacionados con el TipoPolizaDTO cuyo ID se recibe y que adem�s est�n 
	 * relacionados s�lo con las coberturas que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToTipoPoliza. El ID del tipoPoliza
	  @return List<ExclusionRecargoVarioTipoPolizaDTO> encontrados por el query formado.
	 */
    @SuppressWarnings("unchecked")
    public List<ExclusionRecargoVarioTipoPolizaDTO> getVigentesPorIdTipoPoliza(BigDecimal idToTipoPoliza) {
    	LogDeMidasEJB3.log("encontrando ExclusionRecargoVarioTipoPolizaDTO relacionadas con el tipoPoliza: "+idToTipoPoliza+" y con coberturas vigentes", Level.INFO, null);
			try {
			final String queryString = "select model from ExclusionRecargoVarioTipoPolizaDTO model where model.id.idtotipopoliza = :idToTipoPoliza and" +
				"(model.coberturaDTO.claveActivo <> 0 ) and " +
				"(model.coberturaDTO.idToCobertura in (select cobSec.id.idtocobertura from CoberturaSeccionDTO cobSec where cobSec.id.idtoseccion in" +
				"(select sec.idToSeccion from SeccionDTO sec where (sec.claveEstatus <> 3 ) and " +
				"sec.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza)))";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
}