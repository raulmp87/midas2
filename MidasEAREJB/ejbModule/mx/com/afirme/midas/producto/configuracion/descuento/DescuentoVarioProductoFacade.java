package mx.com.afirme.midas.producto.configuracion.descuento;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity DescuentoPorProductoDTO.
 * 
 * @see .DescuentoPorProductoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class DescuentoVarioProductoFacade implements
		DescuentoVarioProductoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved DescuentoVarioProductoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            DescuentoVarioProductoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DescuentoVarioProductoDTO entity) {
		LogDeMidasEJB3.log("saving DescuentoVarioProductoDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent DescuentoVarioProductoDTO entity.
	 * 
	 * @param entity
	 *            DescuentoVarioProductoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DescuentoVarioProductoDTO entity) {
		LogDeMidasEJB3.log("deleting DescuentoVarioProductoDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					DescuentoVarioProductoDTO.class, entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved DescuentoVarioProductoDTO entity and return it
	 * or a copy of it to the sender. A copy of the DescuentoVarioProductoDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            DescuentoVarioProductoDTO entity to update
	 * @return DescuentoVarioProductoDTO the persisted DescuentoVarioProductoDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DescuentoVarioProductoDTO update(DescuentoVarioProductoDTO entity) {
		LogDeMidasEJB3.log("updating DescuentoVarioProductoDTO instance",
				Level.INFO, null);
		try {
			DescuentoVarioProductoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DescuentoVarioProductoDTO findById(DescuentoVarioProductoId id) {
		LogDeMidasEJB3.log(
				"finding DescuentoVarioProductoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			DescuentoVarioProductoDTO instance = entityManager.find(
					DescuentoVarioProductoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DescuentoVarioProductoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the DescuentoVarioProductoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DescuentoVarioProductoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<DescuentoVarioProductoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding DescuentoVarioProductoDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from DescuentoVarioProductoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DescuentoVarioProductoDTO entities.
	 * 
	 * @return List<DescuentoVarioProductoDTO> all DescuentoVarioProductoDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<DescuentoVarioProductoDTO> findAll() {
		LogDeMidasEJB3.log("finding all DescuentoVarioProductoDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from DescuentoVarioProductoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}