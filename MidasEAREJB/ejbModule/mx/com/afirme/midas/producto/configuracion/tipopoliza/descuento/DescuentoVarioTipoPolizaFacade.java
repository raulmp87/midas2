package mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;


/**
 * Facade for entity DescuentoVarioTipoPolizaDTO.
 * @see .DescuentoVarioTipoPolizaDTO
  * @author Jos� Luis Arellano 
 */
@Stateless
public class DescuentoVarioTipoPolizaFacade implements DescuentoVarioTipoPolizaFacadeRemote{
	@PersistenceContext private EntityManager entityManager;
	
	/**
	 Perform an initial save of a previously unsaved DescuentoVarioTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DescuentoVarioTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
	public void save(DescuentoVarioTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("saving DescuentoVarioTipoPolizaDTO instance", Level.INFO, null);
        try {
        	entityManager.persist(entity);
        	LogDeMidasEJB3.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
            throw re;
        }
	}

	/**
	 Delete a persistent DescuentoVarioTipoPolizaDTO entity.
	  @param entity DescuentoVarioTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
	public void delete(DescuentoVarioTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("deleting DescuentoVarioTipoPolizaDTO instance", Level.INFO, null);
        try {
        	entity = entityManager.getReference(DescuentoVarioTipoPolizaDTO.class, entity.getId());
        	entityManager.remove(entity);
        	LogDeMidasEJB3.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
            throw re;
        }
	}

	/**
	 Persist a previously saved DescuentoVarioTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the DescuentoVarioTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DescuentoVarioTipoPolizaDTO entity to update
	 @return DescuentoVarioTipoPolizaDTO the persisted DescuentoVarioTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DescuentoVarioTipoPolizaDTO update(DescuentoVarioTipoPolizaDTO entity) {
		LogDeMidasEJB3.log("updating DescuentoVarioTipoPolizaDTO instance", Level.INFO, null);
        try {
        	DescuentoVarioTipoPolizaDTO result = entityManager.merge(entity);
        	LogDeMidasEJB3.log("update successful", Level.INFO, null);
            return result;
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
            throw re;
        }
	}

	public DescuentoVarioTipoPolizaDTO findById( DescuentoVarioTipoPolizaId id) {
		LogDeMidasEJB3.log("finding DescuentoVarioTipoPolizaDTO instance with id: " + id, Level.INFO, null);
        try {
        	DescuentoVarioTipoPolizaDTO instance = entityManager.find(DescuentoVarioTipoPolizaDTO.class, id);
        	return instance;
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
            throw re;
        }
	}

	/**
	 * Find all DescuentoVarioTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DescuentoVarioTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<DescuentoVarioTipoPolizaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<DescuentoVarioTipoPolizaDTO> findByProperty(String propertyName, final Object value) {
		LogDeMidasEJB3.log("finding DescuentoVarioTipoPolizaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from DescuentoVarioTipoPolizaDTO model where model." 
		 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
			}
		}

	/**
	 * Find all DescuentoVarioTipoPolizaDTO entities.
	  	  @return List<DescuentoVarioTipoPolizaDTO> all DescuentoVarioTipoPolizaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DescuentoVarioTipoPolizaDTO> findAll() {
		LogDeMidasEJB3.log("finding all DescuentoVarioTipoPolizaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from DescuentoVarioTipoPolizaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
}
