package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaId;

/**
 * Facade for entity DeducibleCoberturaDTO.
 * 
 * @see .DeducibleCoberturaDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class DeducibleCoberturaFacade implements DeducibleCoberturaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved DeducibleCoberturaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            DeducibleCoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DeducibleCoberturaDTO entity) {
		LogDeMidasEJB3.log("saving DeducibleCoberturaDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent DeducibleCoberturaDTO entity.
	 * 
	 * @param entity
	 *            DeducibleCoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DeducibleCoberturaDTO entity) {
		LogDeMidasEJB3.log("deleting DeducibleCoberturaDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(DeducibleCoberturaDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved DeducibleCoberturaDTO entity and return it or
	 * a copy of it to the sender. A copy of the DeducibleCoberturaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            DeducibleCoberturaDTO entity to update
	 * @return DeducibleCoberturaDTO the persisted DeducibleCoberturaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DeducibleCoberturaDTO update(DeducibleCoberturaDTO entity) {
		LogDeMidasEJB3.log("updating DeducibleCoberturaDTO instance",
				Level.INFO, null);
		try {
			DeducibleCoberturaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public DeducibleCoberturaDTO findById(DeducibleCoberturaId id) {
		LogDeMidasEJB3.log("finding DeducibleCoberturaDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			DeducibleCoberturaDTO instance = entityManager.find(
					DeducibleCoberturaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DeducibleCoberturaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the DeducibleCoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DeducibleCoberturaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<DeducibleCoberturaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding DeducibleCoberturaDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from DeducibleCoberturaDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all DeducibleCoberturaDTO entities.
	 * 
	 * @return List<DeducibleCoberturaDTO> all DeducibleCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DeducibleCoberturaDTO> findAll() {
		LogDeMidasEJB3.log("finding all DeducibleCoberturaDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from DeducibleCoberturaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DeducibleCoberturaDTO> listarDeducibles(BigDecimal idToCobertura) {
		String queryString = "select model from DeducibleCoberturaDTO model where model.id.idToCobertura = :idToCobertura order by model.valor";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCobertura", idToCobertura);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<DeducibleCoberturaDTO> listarDeduciblesPorCobertura(BigDecimal idToCobertura) {
		String queryString = "select model from DeducibleCoberturaDTO model " +
				"where model.id.idToCobertura = :idToCobertura " +
				"and model.claveDefault = 1";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCobertura", idToCobertura);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
	
	public BigDecimal nextNumeroSecuencia(BigDecimal idToCobertura) {
		String queryString = "select max(model.id.numeroSecuencia) from DeducibleCoberturaDTO model " +
				"where model.id.idToCobertura = :idToCobertura " +
				"";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCobertura", idToCobertura);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		
		BigDecimal next = (BigDecimal)query.getSingleResult();
		if(next == null){
			next = new BigDecimal(0);
		}
		double valor = next.doubleValue();
		valor++;
		return new BigDecimal(valor);
	}
}