package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity RiesgoCoberturaDTO.
 * 
 * @see .RiesgoCoberturaDTO
 * @author Jos� Luis Arellano
 */
@Stateless
public class RiesgoCoberturaFacade implements RiesgoCoberturaFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved RiesgoCoberturaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            RiesgoCoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RiesgoCoberturaDTO entity) {
		LogDeMidasEJB3.log("saving RiesgoCoberturaDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent RiesgoCoberturaDTO entity.
	 * 
	 * @param entity
	 *            RiesgoCoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoCoberturaDTO entity) {
		LogDeMidasEJB3.log("deleting RiesgoCoberturaDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(RiesgoCoberturaDTO.class,entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved RiesgoCoberturaDTO entity and return it or a
	 * copy of it to the sender. A copy of the RiesgoCoberturaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            RiesgoCoberturaDTO entity to update
	 * @return RiesgoCoberturaDTO the persisted RiesgoCoberturaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoCoberturaDTO update(RiesgoCoberturaDTO entity) {
		LogDeMidasEJB3.log("updating RiesgoCoberturaDTO instance", Level.INFO, null);
		try {
			RiesgoCoberturaDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RiesgoCoberturaDTO findById(RiesgoCoberturaId id) {
		LogDeMidasEJB3.log("finding RiesgoCoberturaDTO instance with id: " + id, Level.INFO, null);
		try {
			RiesgoCoberturaDTO instance = entityManager.find(RiesgoCoberturaDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoCoberturaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoCoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoCoberturaDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoCoberturaDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding RiesgoCoberturaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoCoberturaDTO model where model." + propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RiesgoCoberturaDTO entities.
	 * 
	 * @return List<RiesgoCoberturaDTO> all RiesgoCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoCoberturaDTO> findAll() {
		LogDeMidasEJB3.log("finding all RiesgoCoberturaDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoCoberturaDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<RiesgoCoberturaDTO> findByCoberturaSeccion(BigDecimal idToCobertura, BigDecimal idToSeccion) {
		LogDeMidasEJB3.log("finding all RiesgoCoberturaDTO instances with idToCobertura: "+idToCobertura+" and idToSeccion: "+idToSeccion, Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoCoberturaDTO model where model.id.idtoseccion = :idToSeccion " +
					"and model.id.idtocobertura = :idToCobertura";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", idToSeccion);
			query.setParameter("idToCobertura", idToCobertura);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<RiesgoCoberturaDTO> findByCoberturaSeccionId(BigDecimal idToCobertura, BigDecimal idToSeccion) {
		LogDeMidasEJB3.log("finding all RiesgoCoberturaDTO instances with idToCobertura: "+idToCobertura+" and idToSeccion: "+idToSeccion, Level.INFO, null);
		try {
			final String queryString = "select model from RiesgoCoberturaDTO model where model.id.idtoseccion = :idToSeccion " +
					"and model.id.idtocobertura = :idToCobertura";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", idToSeccion);
			query.setParameter("idToCobertura", idToCobertura);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	/**
	 * listarVigentesPorCoberturaSeccion. lista los registros de CoberturaSeccionDTO que contengan la cobertura y la seccion 
	 * cuyos ID�s se reciben, y que adem�s est�n relacionados s�lo con riesgos vigentes.
	 * @param idToCobertura @param idToSeccion
	 * @return List<RiesgoCoberturaDTO> encontrados por el query.
	 */
	@SuppressWarnings("unchecked")
	public List<RiesgoCoberturaDTO> listarVigentesPorCoberturaSeccion(BigDecimal idToCobertura, BigDecimal idToSeccion) {
		LogDeMidasEJB3.log("finding all RiesgoCoberturaDTO instances with idToCobertura: "+idToCobertura+" and idToSeccion: "+idToSeccion, Level.INFO, null);
		try {
			/*final String queryString = "select model from RiesgoCoberturaDTO model where model.id.idtoseccion = :idToSeccion and " +
				"model.id.idtocobertura = :idToCobertura and (model.riesgoDTO.claveActivoConfiguracion <> 0 or model.riesgoDTO.claveActivoProduccion <> 0)";*/
			final String queryString = "select model from RiesgoCoberturaDTO model where model.id.idtoseccion = :idToSeccion and " +
				"model.id.idtocobertura = :idToCobertura and (model.riesgoDTO.claveEstatus <> 3) order by model.riesgoDTO.codigo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", idToSeccion);
			query.setParameter("idToCobertura", idToCobertura);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@Override
	public Map<BigDecimal, List<RiesgoCoberturaDTO>> getMapCoberturaRiesgoCoberturaVigentes(BigDecimal idToSeccion) {
		final String queryString = "select model from RiesgoCoberturaDTO model where " +
				"model.id.idtoseccion = :idToSeccion and (model.riesgoDTO.claveEstatus <> :claveEstatus) order by model.id.idtocobertura, model.riesgoDTO.codigo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToSeccion", idToSeccion);
		query.setParameter("claveEstatus", new Short("3"));
		@SuppressWarnings("unchecked")
		List<RiesgoCoberturaDTO> resultList = (List<RiesgoCoberturaDTO>) query.getResultList();
		Map<BigDecimal, List<RiesgoCoberturaDTO>> map = new HashMap<BigDecimal, List<RiesgoCoberturaDTO>>();
		if (resultList.size() == 0) {
			return map;
		}
		
		BigDecimal currentIdToCobertura = resultList.get(0).getId().getIdtocobertura();
		List<RiesgoCoberturaDTO> currentRiesgoCoberturaList = new ArrayList<RiesgoCoberturaDTO>();
		
		for (RiesgoCoberturaDTO riesgoCobertura : resultList) {
			if (!currentIdToCobertura.equals(riesgoCobertura.getId().getIdtocobertura())) {
				map.put(currentIdToCobertura, currentRiesgoCoberturaList);
				currentIdToCobertura = riesgoCobertura.getId().getIdtocobertura();
				currentRiesgoCoberturaList = new ArrayList<RiesgoCoberturaDTO>();
			}
			currentRiesgoCoberturaList.add(riesgoCobertura);
		}
		
		return map;
	}

	@SuppressWarnings("unchecked")
	public List<BigDecimal> obtenerRiesgosPorTipoPoliza(BigDecimal idToTipoPoliza) {
		LogDeMidasEJB3.log("finding idToRiesgo with idToTipoPoliza: " + idToTipoPoliza, Level.INFO, null);
		try {
			String queryString = "select distinct model.id.idtoriesgo from RiesgoCoberturaDTO model " +
                    "where model.id.idtoseccion in (" +
                                     "select seccion.idToSeccion from SeccionDTO seccion " +
                                     "where seccion.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza) " +
                     "and (model.claveObligatoriedad = 0 or model.claveObligatoriedad = 1 or model.claveObligatoriedad = 3) "+
                     "and (model.coberturaSeccionDTO.claveObligatoriedad = 1 or model.coberturaSeccionDTO.claveObligatoriedad = 2 or model.coberturaSeccionDTO.claveObligatoriedad = 3) "+
                     "and (model.coberturaSeccionDTO.seccionDTO.claveObligatoriedad = 1 or model.coberturaSeccionDTO.seccionDTO.claveObligatoriedad = 2 or model.coberturaSeccionDTO.seccionDTO.claveObligatoriedad = 3) ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<BigDecimal> obtenerRiesgosPorSeccionCotizacion(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal idToSeccion) {
		LogDeMidasEJB3.log("finding idToRiesgo with idToCotizacion: " + idToCotizacion, Level.INFO, null);
		try {
			String queryString = "select distinct model.id.idtoriesgo from RiesgoCoberturaDTO model " +
					"where model.id.idtocobertura in (" +
					"select cobertura.id.idToCobertura from CoberturaCotizacionDTO cobertura " +
					"where cobertura.id.idToCotizacion = :idToCotizacion " +
					"and cobertura.id.numeroInciso = :numeroInciso " +
					"and cobertura.id.idToSeccion = :idToSeccion)";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void borrarARDs(RiesgoCoberturaDTO riesgoCoberturaDTO)  {
		try {
			String queryString = "delete from AumentoRiesgoCotizacionDTO model " +
					"where model.id.idToSeccion = :idToSeccion and model.id.idToCobertura = :idToCobertura " +
					"and model.id.idToRiesgo = :idToRiesgo";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", riesgoCoberturaDTO.getId().getIdtoseccion());
			query.setParameter("idToCobertura", riesgoCoberturaDTO.getId().getIdtocobertura());
			query.setParameter("idToRiesgo", riesgoCoberturaDTO.getId().getIdtoriesgo());
			query.executeUpdate();

			queryString = "delete from RecargoRiesgoCotizacionDTO model " +
			"where model.id.idToSeccion = :idToSeccion and model.id.idToCobertura = :idToCobertura " +
			"and model.id.idToRiesgo = :idToRiesgo";
			query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", riesgoCoberturaDTO.getId().getIdtoseccion());
			query.setParameter("idToCobertura", riesgoCoberturaDTO.getId().getIdtocobertura());
			query.setParameter("idToRiesgo", riesgoCoberturaDTO.getId().getIdtoriesgo());
			query.executeUpdate();

			queryString = "delete from DescuentoRiesgoCotizacionDTO model " +
			"where model.id.idToSeccion = :idToSeccion and model.id.idToCobertura = :idToCobertura " +
			"and model.id.idToRiesgo = :idToRiesgo";
			query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", riesgoCoberturaDTO.getId().getIdtoseccion());
			query.setParameter("idToCobertura", riesgoCoberturaDTO.getId().getIdtocobertura());
			query.setParameter("idToRiesgo", riesgoCoberturaDTO.getId().getIdtoriesgo());
			query.executeUpdate();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
}