package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento;

import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class DescuentoCoberturaCotServiceImpl implements DescuentoCoberturaCotService {

	@PersistenceContext
	private EntityManager entityManager;
	
	
	@Override
	public void bulkDelete(Long idToCotizacion, Long numeroInciso,
			Long idToSeccion, Long idToDescuentoVario) {
		bulkDelete(idToCotizacion, numeroInciso, idToSeccion, Arrays.asList(idToDescuentoVario));
	}
	
	@Override
	public void bulkDelete(Long idToCotizacion, Long numeroInciso,
			Long idToSeccion, List<Long> idToDescuentoVarios) {
		String jpql = "delete from DescuentoCoberturaCot dc where " 
				+ "dc.id.idToCotizacion = :idToCotizacion "
				+ "and dc.id.numeroInciso = :numeroInciso " 
				+ "and dc.id.idToSeccion = :idToSeccion " 
				+ "and dc.id.idToDescuentoVario in :idToDescuentoVarios";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("idToCotizacion", idToCotizacion)
			.setParameter("numeroInciso", numeroInciso)
			.setParameter("idToSeccion", idToSeccion)
			.setParameter("idToDescuentoVarios", idToDescuentoVarios);
		query.executeUpdate();
		entityManager.getEntityManagerFactory().getCache().evict(DescuentoCoberturaCot.class);
	}
	
	

}
