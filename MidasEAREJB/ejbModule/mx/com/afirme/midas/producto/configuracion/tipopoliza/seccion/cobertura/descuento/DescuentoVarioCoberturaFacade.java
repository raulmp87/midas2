package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity AumentoVarioCoberturaDTO.
 * @see .AumentoVarioCoberturaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless
public class DescuentoVarioCoberturaFacade  implements DescuentoVarioCoberturaFacadeRemote {

    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved DescuentoVarioCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DescuentoVarioCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DescuentoVarioCoberturaDTO entity) {
    				LogDeMidasEJB3.log("saving DescuentoVarioCoberturaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            entityManager.flush();
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent DescuentoVarioCoberturaDTO entity.
	  @param entity DescuentoVarioCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DescuentoVarioCoberturaDTO entity) {
    				LogDeMidasEJB3.log("deleting DescuentoVarioCoberturaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(DescuentoVarioCoberturaDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
            			
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved DescuentoVarioCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the DescuentoVarioCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DescuentoVarioCoberturaDTO entity to update
	 @return DescuentoVarioCoberturaDTO the persisted DescuentoVarioCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public DescuentoVarioCoberturaDTO update(DescuentoVarioCoberturaDTO entity) {
    				LogDeMidasEJB3.log("updating DescuentoVarioCoberturaDTO instance", Level.INFO, null);
	        try {
            DescuentoVarioCoberturaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
            			entityManager.flush();
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public DescuentoVarioCoberturaDTO findById( DescuentoVarioCoberturaId id) {
    				LogDeMidasEJB3.log("finding DescuentoVarioCoberturaDTO instance with id: " + id, Level.INFO, null);
	        try {
            DescuentoVarioCoberturaDTO instance = entityManager.find(DescuentoVarioCoberturaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all DescuentoVarioCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DescuentoVarioCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<DescuentoVarioCoberturaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<DescuentoVarioCoberturaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding DescuentoVarioCoberturaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from DescuentoVarioCoberturaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all DescuentoVarioCoberturaDTO entities.
	  	  @return List<DescuentoVarioCoberturaDTO> all DescuentoVarioCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<DescuentoVarioCoberturaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all DescuentoVarioCoberturaDTO instances", Level.INFO, null);
			try {
			    	final String queryString = "select model from DescuentoVarioCoberturaDTO model";
				Query query = entityManager.createQuery(queryString);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}