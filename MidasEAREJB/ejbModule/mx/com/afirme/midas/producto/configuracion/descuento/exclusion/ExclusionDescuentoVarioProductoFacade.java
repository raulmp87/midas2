package mx.com.afirme.midas.producto.configuracion.descuento.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity ExclusionDescuentoVarioProductoDTO.
 * @see .ExclusionDescuentoVarioProductoDTO
  * @author MyEclipse Persistence Tools
 */
@Stateless
public class ExclusionDescuentoVarioProductoFacade implements ExclusionDescuentoVarioProductoFacadeRemote{
    @PersistenceContext private EntityManager entityManager;
	
	/**
	 Perform an initial save of a previously unsaved ExclusionAumentoVarioProductoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ExclusionAumentoVarioProductoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ExclusionDescuentoVarioProductoDTO entity) {
		LogDeMidasEJB3.log("saving ExclusionDescuentoVarioProductoDTO instance", Level.INFO, null);
        try {
        	entityManager.persist(entity);
        	LogDeMidasEJB3.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

	/**
	 Delete a persistent ExclusionDescuentoVarioProductoDTO entity.
	  @param entity ExclusionDescuentoVarioProductoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ExclusionDescuentoVarioProductoDTO entity) {
    	LogDeMidasEJB3.log("deleting ExclusionDescuentoVarioProductoDTO instance", Level.INFO, null);
        try {
        	entity = entityManager.getReference(ExclusionDescuentoVarioProductoDTO.class, entity.getId());
        	entityManager.remove(entity);
        	LogDeMidasEJB3.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

	/**
	 Persist a previously saved ExclusionDescuentoVarioProductoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExclusionDescuentoVarioProductoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExclusionDescuentoVarioProductoDTO entity to update
	 @return ExclusionDescuentoVarioProductoDTO the persisted ExclusionDescuentoVarioProductoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public ExclusionDescuentoVarioProductoDTO update(ExclusionDescuentoVarioProductoDTO entity) {
    	LogDeMidasEJB3.log("updating ExclusionDescuentoVarioProductoDTO instance", Level.INFO, null);
        try {
        	ExclusionDescuentoVarioProductoDTO result = entityManager.merge(entity);
        	LogDeMidasEJB3.log("update successful", Level.INFO, null);
            return result;
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public ExclusionDescuentoVarioProductoDTO findById( ExclusionDescuentoVarioProductoId id) {
    	LogDeMidasEJB3.log("finding ExclusionDescuentoVarioProductoDTO instance with id: " + id, Level.INFO, null);
        try {
        	ExclusionDescuentoVarioProductoDTO instance = entityManager.find(ExclusionDescuentoVarioProductoDTO.class, id);
        	return instance;
        } catch (RuntimeException re) {
        	LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

	/**
	 * Find all ExclusionDescuentoVarioProductoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ExclusionDescuentoVarioProductoDTO property to query
	  @param value the property value to match
	  	  @return List<ExclusionDescuentoVarioProductoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionDescuentoVarioProductoDTO> findByProperty(String propertyName, final Object value) {
		LogDeMidasEJB3.log("finding ExclusionDescuentoVarioProductoDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionDescuentoVarioProductoDTO model where model." 
				+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all ExclusionDescuentoVarioProductoDTO entities.
	  	  @return List<ExclusionDescuentoVarioProductoDTO> all ExclusionDescuentoVarioProductoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusionDescuentoVarioProductoDTO> findAll() {
		LogDeMidasEJB3.log("finding all ExclusionDescuentoVarioProductoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from ExclusionDescuentoVarioProductoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Encuentra los registros de ExclusionDescuentoVarioProductoDTO relacionados con el ProductoDTO cuyo ID se recibe y que adem�s est�n 
	 * relacionados s�lo con Tipos de p�liza que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToProducto. El ID del producto
	  @return List<ExclusionDescuentoVarioProductoDTO> encontrados por el query formado.
	 */
    @SuppressWarnings("unchecked")
    public List<ExclusionDescuentoVarioProductoDTO> getVigentesPorIdProducto(BigDecimal idToProducto) {
    	LogDeMidasEJB3.log("encontrando ExclusionDescuentoVarioProductoDTO relacionadas con el producto: "+idToProducto+" y con tipos de p�liza vigentes", Level.INFO, null);
			try {
			final String queryString = "select model from ExclusionDescuentoVarioProductoDTO model where model.id.idtoproducto = :idToProducto and" +
					"(model.tipoPolizaDTO.claveEstatus <> 3 )";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToProducto", idToProducto);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
}
