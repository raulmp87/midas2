package mx.com.afirme.midas.producto.configuracion.recargo;

// default package

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity RecargoVarioProductoDTO.
 * 
 * @see .RecargoVarioProductoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class RecargoVarioProductoFacade implements
		RecargoVarioProductoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved RecargoVarioProductoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            RecargoVarioProductoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RecargoVarioProductoDTO entity) {
		LogDeMidasEJB3.log("saving RecargoVarioProductoDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent RecargoVarioProductoDTO entity.
	 * 
	 * @param entity
	 *            RecargoVarioProductoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RecargoVarioProductoDTO entity) {
		LogDeMidasEJB3.log("deleting RecargoVarioProductoDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(RecargoVarioProductoDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved RecargoVarioProductoDTO entity and return it
	 * or a copy of it to the sender. A copy of the RecargoVarioProductoDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            RecargoVarioProductoDTO entity to update
	 * @return RecargoVarioProductoDTO the persisted RecargoVarioProductoDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RecargoVarioProductoDTO update(RecargoVarioProductoDTO entity) {
		LogDeMidasEJB3.log("updating RecargoVarioProductoDTO instance",
				Level.INFO, null);
		try {
			RecargoVarioProductoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RecargoVarioProductoDTO findById(RecargoVarioProductoId id) {
		LogDeMidasEJB3.log("finding RecargoVarioProductoDTO instance with id: "
				+ id, Level.INFO, null);
		try {
			RecargoVarioProductoDTO instance = entityManager.find(
					RecargoVarioProductoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RecargoVarioProductoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RecargoVarioProductoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RecargoVarioProductoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RecargoVarioProductoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding RecargoVarioProductoDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RecargoVarioProductoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RecargoVarioProductoDTO entities.
	 * 
	 * @return List<RecargoVarioProductoDTO> all RecargoVarioProductoDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<RecargoVarioProductoDTO> findAll() {
		LogDeMidasEJB3.log("finding all RecargoVarioProductoDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from RecargoVarioProductoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
}