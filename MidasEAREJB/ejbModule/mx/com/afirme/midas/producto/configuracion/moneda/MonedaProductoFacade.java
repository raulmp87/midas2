package mx.com.afirme.midas.producto.configuracion.moneda;

// default package

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity MonedaProductoDTO.
 * 
 * @see .MonedaProductoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class MonedaProductoFacade implements MonedaProductoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved MonedaProductoDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            MonedaProductoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(MonedaProductoDTO entity) {
		LogDeMidasEJB3.log("saving MonedaProductoDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent MonedaProductoDTO entity.
	 * 
	 * @param entity
	 *            MonedaProductoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(MonedaProductoDTO entity) {
		LogDeMidasEJB3.log("deleting MonedaProductoDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(MonedaProductoDTO.class, entity
					.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved MonedaProductoDTO entity and return it or a
	 * copy of it to the sender. A copy of the MonedaProductoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            MonedaProductoDTO entity to update
	 * @return MonedaProductoDTO the persisted MonedaProductoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public MonedaProductoDTO update(MonedaProductoDTO entity) {
		LogDeMidasEJB3.log("updating MonedaProductoDTO instance", Level.INFO,
				null);
		try {
			MonedaProductoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			entityManager.flush();
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public MonedaProductoDTO findById(MonedaProductoId id) {
		LogDeMidasEJB3.log("finding MonedaProductoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			MonedaProductoDTO instance = entityManager.find(
					MonedaProductoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all MonedaProductoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the MonedaProductoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<MonedaProductoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<MonedaProductoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding MonedaProductoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from MonedaProductoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all MonedaProductoDTO entities.
	 * 
	 * @return List<MonedaProductoDTO> all MonedaProductoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<MonedaProductoDTO> findAll() {
		LogDeMidasEJB3.log("finding all MonedaProductoDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from MonedaProductoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}