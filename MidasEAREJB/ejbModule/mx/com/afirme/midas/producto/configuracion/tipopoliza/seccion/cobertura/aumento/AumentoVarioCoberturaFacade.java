package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;

/**
 * Facade for entity AumentoVarioCoberturaDTO.
 * @see .AumentoVarioCoberturaDTO
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class AumentoVarioCoberturaFacade  implements AumentoVarioCoberturaFacadeRemote {




    @PersistenceContext private EntityManager entityManager;
	
		/**
	 Perform an initial save of a previously unsaved AumentoVarioCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AumentoVarioCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AumentoVarioCoberturaDTO entity) {
    				LogDeMidasEJB3.log("saving AumentoVarioCoberturaDTO instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LogDeMidasEJB3.log("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent AumentoVarioCoberturaDTO entity.
	  @param entity AumentoVarioCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AumentoVarioCoberturaDTO entity) {
    				LogDeMidasEJB3.log("deleting AumentoVarioCoberturaDTO instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(AumentoVarioCoberturaDTO.class, entity.getId());
            entityManager.remove(entity);
            			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved AumentoVarioCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the AumentoVarioCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AumentoVarioCoberturaDTO entity to update
	 @return AumentoVarioCoberturaDTO the persisted AumentoVarioCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public AumentoVarioCoberturaDTO update(AumentoVarioCoberturaDTO entity) {
    				LogDeMidasEJB3.log("updating AumentoVarioCoberturaDTO instance", Level.INFO, null);
	        try {
            AumentoVarioCoberturaDTO result = entityManager.merge(entity);
            			LogDeMidasEJB3.log("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public AumentoVarioCoberturaDTO findById( AumentoVarioCoberturaId id) {
    				LogDeMidasEJB3.log("finding AumentoVarioCoberturaDTO instance with id: " + id, Level.INFO, null);
	        try {
            AumentoVarioCoberturaDTO instance = entityManager.find(AumentoVarioCoberturaDTO.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all AumentoVarioCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AumentoVarioCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<AumentoVarioCoberturaDTO> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<AumentoVarioCoberturaDTO> findByProperty(String propertyName, final Object value
        ) {
    				LogDeMidasEJB3.log("finding AumentoVarioCoberturaDTO instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from AumentoVarioCoberturaDTO model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all AumentoVarioCoberturaDTO entities.
	  	  @return List<AumentoVarioCoberturaDTO> all AumentoVarioCoberturaDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<AumentoVarioCoberturaDTO> findAll(
		) {
					LogDeMidasEJB3.log("finding all AumentoVarioCoberturaDTO instances", Level.INFO, null);
			try {
			final String queryString = "select model from AumentoVarioCoberturaDTO model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}