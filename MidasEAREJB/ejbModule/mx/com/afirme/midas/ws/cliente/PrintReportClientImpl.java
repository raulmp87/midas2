package mx.com.afirme.midas.ws.cliente;

import java.rmi.RemoteException;

import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;

import com.afirme.insurance.services.PrintReport.PrintReport;
import com.afirme.insurance.services.PrintReport.PrintReportServiceLocator;

@Stateless
public class PrintReportClientImpl implements PrintReportClient {

	private static final Logger LOG = Logger.getLogger(PrintReportClientImpl.class);
			
	public void generateDigitalBill() {
		PrintReport service = getPrintReportService();
		try {
			service.generateDigitalBill();
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}

	public byte[] getDigitalBill(String llaveFiscal) {
		PrintReport service = getPrintReportService();
		try {
			return service.getDigitalBill(llaveFiscal);
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public byte[] getDigitalBill(String llaveFiscal, String formato) {
		PrintReport service = getPrintReportService();
		try {
			return service.getDigitalBill(llaveFiscal, formato);
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}
	
	public byte[] getAccountChargeLetter(String quotationId) {
		String contentTypeReport = "application/pdf";
		PrintReport service = getPrintReportService();
		try {
			return service.getAccountChargeLetter(quotationId, contentTypeReport);
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	private PrintReport getPrintReportService() {
		try {
			PrintReportServiceLocator locator = new PrintReportServiceLocator(
					SistemaPersistencia.URL_WS_IMPRESION,
					SistemaPersistencia.PUERTO_WS_IMPRESION);
			return locator.getPrintReport();
		
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Gets Life Insurance Policy.
	 * @param identifier. Quotation id or policy number
	 * @param contentTypeReport. Type of the report "application/pdf".
	 * @param duplicate. True or false for duplicate print
	 * @param printReceipt. True or false to eliminate the receipt
	 * @param printAccountCharge. True or false to print account charge report
	 * @param origin. Application origin invoking this method.
	 * @return
	 */ 
	public byte[] getLifeInsurancePolicy(String cotizacionID){
	    String contentTypeReport = "application/pdf";
		PrintReport service = getPrintReportService();
		try {
			return service.getLifeInsurancePolicy(cotizacionID, contentTypeReport,"false", "false", "false", "OTHER");
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Gets Auto Insurance Policy. The method will decide if national unity report
	 * is printed. Also it will decide between old style receipt report o the new
	 * WFACTURA report
	 * @param identifier. Quotation id or policy number
	 * @param contentTypeReport. Type of the report "application/pdf".
	 * @param duplicate. True or false for duplicate print
	 * @param printReceipt. True or false to eliminate the receipt
	 * @param printAccountCharge. True or false to print account charge report
	 * @param origin. Application origin invoking this method.
	 * @return
	 */
	public byte[] getAutoInsurancePolicy(String cotizacionID) {
		String contentTypeReport = "application/pdf";
		PrintReport service = getPrintReportService();
		try {
			return service.getAutoInsurancePolicy(cotizacionID, contentTypeReport, "false", "false", "false", "OTHER");
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * Gets house policy pdf
	 * @param identifier
	 * @return
	 */
	public byte[] getHousePolicy(String cotizacionID){
		PrintReport service = getPrintReportService();
		try {
			return service.getHousePolicy(cotizacionID);
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public byte[] getCFDI(String key) {
		PrintReport service = getPrintReportService();
		try {
			return service.getCFDI(key);
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public byte[] getCFDIWithAddenda(String key) {
		PrintReport service = getPrintReportService();
		try {
			return service.getCFDIWithAddenda(key);
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void cancelDigitalBill(int idUsuario) {
		PrintReport service = getPrintReportService();
		try {
			LOG.info("Manda a llamar el servicio de Cancelacion de WFactura. Le pasa como parametro idUsuario:= " +idUsuario);
			service.cancelDigitalBill(idUsuario);
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void cancelarCFDI() {
		PrintReport service = getPrintReportService();
		try {
			LOG.info("Manda a llamar el servicio de Obtener Acuse de WFactura. Cambia el estatus con la respuesta del SAT");
			service.cancelarCFDI();
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void generatePaymentComplementBill(){
		PrintReport service = getPrintReportService();
		try {
			LOG.info("Manda a llamar el servicio para timbrar facturas complemento pago");
			service.generatePaymentComplementBill();
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void generateChargeComplementBill(){
		PrintReport service = getPrintReportService();
		try {
			LOG.info("Manda a llamar el servicio para timbrar facturas complemento despago");
			service.generateChargeComplementBill();
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}
}