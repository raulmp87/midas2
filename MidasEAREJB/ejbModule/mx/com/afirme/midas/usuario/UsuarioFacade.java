package mx.com.afirme.midas.usuario;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.usuario.UsuarioDTO;
import mx.com.afirme.midas.usuario.UsuarioFacadeRemote;
@Stateless
/*
 * Fachada de entidad UsuarioDTO
 */
public class UsuarioFacade implements UsuarioFacadeRemote {
	// property constants
	public static final String USUARIONOMBRE = "usuarionombre";
	public static final String USUARIOCONTRASENA = "usuariocontrasena";

	@PersistenceContext
	private EntityManager entityManager;

	public void guardar(UsuarioDTO usuario) {
		LogDeMidasEJB3.log("Guardando UsuarioDTO", Level.FINEST, null);
		try {
			entityManager.persist(usuario);
			LogDeMidasEJB3.log(
					"El guardado se ha realizado satisfactoriamente",
					Level.FINEST, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Error en metodo guardar(UsuarioDTO usuario)",
					Level.SEVERE, re);
			throw re;
		}
	}

	public void borrar(UsuarioDTO usuario) {
		LogDeMidasEJB3.log("Borrando UsuarioDTO", Level.FINEST, null);
		try {
			usuario = entityManager.getReference(UsuarioDTO.class, usuario
					.getUsuarioid());
			entityManager.remove(usuario);
			LogDeMidasEJB3.log("Borrado satisfactorio", Level.FINEST, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Error en metodo borrar(UsuarioDTO usuario)",
					Level.SEVERE, re);
			throw re;
		}
	}

	public UsuarioDTO actualizar(UsuarioDTO usuario) {
		LogDeMidasEJB3.log("Actualizando UsuarioDTO ", Level.FINEST, null);
		try {
			UsuarioDTO result = entityManager.merge(usuario);
			LogDeMidasEJB3.log("Actualizacion satisfactoria", Level.FINEST,
					null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"Error en el metodo actualizar(UsuarioDTO usuario)",
					Level.SEVERE, re);
			throw re;
		}
	}

	public UsuarioDTO buscarPorIdentificador(Long identificador) {
		LogDeMidasEJB3.log("Localizando UsuarioDTO con el identificacor: "
				+ identificador, Level.FINEST, null);
		try {
			UsuarioDTO instance = entityManager.find(UsuarioDTO.class,
					identificador);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log(
							"Error en metodo buscarPorIdentificador(Long identificador)",
							Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<UsuarioDTO> buscarPorPropiedad(String propiedad, Object valor,
			int inicio, int cantidad) {
		LogDeMidasEJB3.log(
				"Localizando el registro de UsuarioDTO con propiedad: "
						+ propiedad + "y  valor: " + valor, Level.FINEST, null);
		try {
			final String consulta = "select model from UsuarioDTO model where model."
					+ propiedad + "= :propiedad";
			Query query = entityManager.createQuery(consulta);
			query.setParameter("propiedad", valor);
			query.setFirstResult(inicio);
			query.setMaxResults(cantidad);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log(
							"Error en metodo buscarPorPropiedad(String propiedad, Object valor, int inicio, int cantidad)",
							Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<UsuarioDTO> buscarPorPropiedad(String propiedad, Object valor,
			int inicio) {
		LogDeMidasEJB3.log(
				"Localizando el registro de UsuarioDTO con propiedad: "
						+ propiedad + "y  valor: " + valor, Level.FINEST, null);
		try {
			final String consulta = "select model from UsuarioDTO model where model."
					+ propiedad + "= :propiedad";
			Query query = entityManager.createQuery(consulta);
			query.setParameter("propiedad", valor);
			query.setFirstResult(inicio);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log(
							"Error en metodo buscarPorPropiedad(String propiedad, Object valor, int inicio)",
							Level.SEVERE, re);
			throw re;
		}
	}

	public List<UsuarioDTO> buscarPorNombreUsuario(Object usuarionombre,
			int inicio, int cantidad) {
		return buscarPorPropiedad(USUARIONOMBRE, usuarionombre, inicio,
				cantidad);
	}

	public List<UsuarioDTO> buscarPorNombreUsuario(Object usuarionombre,
			int inicio) {
		return buscarPorPropiedad(USUARIONOMBRE, usuarionombre, inicio);
	}

	public List<UsuarioDTO> buscarPorContrasena(Object usuariocontrasena,
			int inicio, int cantidad) {
		return buscarPorPropiedad(USUARIOCONTRASENA, usuariocontrasena, inicio,
				cantidad);
	}

	public List<UsuarioDTO> buscarPorContrasena(Object usuariocontrasena,
			int inicio) {
		return buscarPorPropiedad(USUARIOCONTRASENA, usuariocontrasena, inicio);
	}

	@SuppressWarnings("unchecked")
	public List<UsuarioDTO> buscaTodos(int inicio, int cantidad) {
		try {
			LogDeMidasEJB3.log("Localizando el registro de UsuarioDTO ",
					Level.FINEST, null);
			final String consulta = "select model from UsuarioDTO model";
			Query query = entityManager.createQuery(consulta);
			query.setFirstResult(inicio);
			query.setMaxResults(cantidad);
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"Error en metodo buscaTodos(int inicio, int cantidad)",
					Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<UsuarioDTO> buscaTodos(int inicio) {
		try {
			LogDeMidasEJB3.log("Localizando el registro de UsuarioDTO ",
					Level.FINEST, null);
			final String consulta = "select model from UsuarioDTO model";
						
			Query query = entityManager.createQuery(consulta);
			query.setFirstResult(inicio);
			
			//Obliga al persistence context a refrescar su cache con los datos del query en la BD
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log(
					"Error en metodo buscaTodos(int inicio, int cantidad)",
					Level.SEVERE, re);
			throw re;
		}
	}

	public UsuarioDTO validarUsuario(UsuarioDTO usuarioDTO) {
		final String consulta = "select model from UsuarioDTO model where model."
				+ "usuarionombre = :usuario and model.usuariocontrasena = :password";
		Query query = entityManager.createQuery(consulta);
		try {
			query.setParameter("usuario", usuarioDTO.getUsuarionombre());
			query.setParameter("password", usuarioDTO.getUsuariocontrasena());
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("Fall� UsuarioFacade.validarUsuario",
					Level.SEVERE, re);
			throw re;
		}
		UsuarioDTO usuario;
		try {
			usuario = (UsuarioDTO) query.getSingleResult();
		} catch (NoResultException nre) {
			LogDeMidasEJB3.log(
					"Fallo UsuarioFacade.validarUsuario al localizar el registro con el usuario : "
							+ usuarioDTO.getUsuarionombre()
							+ " Y el password: "
							+ usuarioDTO.getUsuariocontrasena(), Level.SEVERE,
					nre);
			return null;
		}
		return usuario;
	}
}