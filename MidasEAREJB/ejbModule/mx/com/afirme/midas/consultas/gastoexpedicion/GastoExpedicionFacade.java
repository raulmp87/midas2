package mx.com.afirme.midas.consultas.gastoexpedicion;

// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity GastoExpedicionDTO.
 * 
 * @see .GastoExpedicionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class GastoExpedicionFacade implements GastoExpedicionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved GastoExpedicionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            GastoExpedicionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(GastoExpedicionDTO entity) {
		LogUtil.log("saving GastoExpedicionDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent GastoExpedicionDTO entity.
	 * 
	 * @param entity
	 *            GastoExpedicionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(GastoExpedicionDTO entity) {
		LogUtil.log("deleting GastoExpedicionDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(GastoExpedicionDTO.class,
					entity.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved GastoExpedicionDTO entity and return it or a
	 * copy of it to the sender. A copy of the GastoExpedicionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            GastoExpedicionDTO entity to update
	 * @return GastoExpedicionDTO the persisted GastoExpedicionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public GastoExpedicionDTO update(GastoExpedicionDTO entity) {
		LogUtil.log("updating GastoExpedicionDTO instance", Level.INFO, null);
		try {
			GastoExpedicionDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public GastoExpedicionDTO findById(GastoExpedicionId id) {
		LogUtil.log("finding GastoExpedicionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			GastoExpedicionDTO instance = entityManager.find(
					GastoExpedicionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all GastoExpedicionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the GastoExpedicionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<GastoExpedicionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<GastoExpedicionDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding GastoExpedicionDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from GastoExpedicionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all GastoExpedicionDTO entities.
	 * 
	 * @return List<GastoExpedicionDTO> all GastoExpedicionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<GastoExpedicionDTO> findAll() {
		LogUtil.log("finding all GastoExpedicionDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from GastoExpedicionDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public GastoExpedicionDTO getGastoExpedicion(BigDecimal idMoneda,
			BigDecimal primaNeta) {
		GastoExpedicionDTO gatoExpedicion= null; 
		LogUtil.log("finding GastoExpedicionDTO instance with idMoneda: "
				+ idMoneda + ", primaNeta: " + primaNeta, Level.INFO, null);
		try {
			final String queryString = "select model from GastoExpedicionDTO model where model.id.idMoneda= :idMoneda and "
					+ ":primaNeta >= model.id.primaNetaMinima and :primaNeta < model.id.primaNetaMaxima";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idMoneda", idMoneda);
			query.setParameter("primaNeta", primaNeta);
			try{
				gatoExpedicion = (GastoExpedicionDTO) query.getSingleResult();
			}catch (javax.persistence.NoResultException e){}
			return gatoExpedicion;
		} catch (RuntimeException re) {
			LogUtil.log("getGastoExpedicion failed", Level.SEVERE, re);
			throw re;
		}
	}
}