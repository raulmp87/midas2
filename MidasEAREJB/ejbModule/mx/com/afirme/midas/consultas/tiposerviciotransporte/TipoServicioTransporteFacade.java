package mx.com.afirme.midas.consultas.tiposerviciotransporte;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity TipoServicioTransporteDTO.
 * 
 * @see .TipoServicioTransporteDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoServicioTransporteFacade implements
		TipoServicioTransporteFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoServicioTransporteDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            TipoServicioTransporteDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoServicioTransporteDTO entity) {
		LogDeMidasEJB3.log("saving TipoServicioTransporteDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoServicioTransporteDTO entity.
	 * 
	 * @param entity
	 *            TipoServicioTransporteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoServicioTransporteDTO entity) {
		LogDeMidasEJB3.log("deleting TipoServicioTransporteDTO instance",
				Level.INFO, null);
		try {
			entity = entityManager.getReference(
					TipoServicioTransporteDTO.class, entity
							.getIdTipoServicioTransporte());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoServicioTransporteDTO entity and return it
	 * or a copy of it to the sender. A copy of the TipoServicioTransporteDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoServicioTransporteDTO entity to update
	 * @return TipoServicioTransporteDTO the persisted TipoServicioTransporteDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoServicioTransporteDTO update(TipoServicioTransporteDTO entity) {
		LogDeMidasEJB3.log("updating TipoServicioTransporteDTO instance",
				Level.INFO, null);
		try {
			TipoServicioTransporteDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoServicioTransporteDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log(
				"finding TipoServicioTransporteDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoServicioTransporteDTO instance = entityManager.find(
					TipoServicioTransporteDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoServicioTransporteDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the TipoServicioTransporteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoServicioTransporteDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoServicioTransporteDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log(
				"finding TipoServicioTransporteDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoServicioTransporteDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoServicioTransporteDTO entities.
	 * 
	 * @return List<TipoServicioTransporteDTO> all TipoServicioTransporteDTO
	 *         entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoServicioTransporteDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoServicioTransporteDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from TipoServicioTransporteDTO model " +
					"order by model.descripcionTipoServicioTra";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TipoServicioTransporteDTO> listarFiltrado(
			TipoServicioTransporteDTO tipoServicioTransporteDTO) {

		try {
			String queryString = "select  model from TipoServicioTransporteDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (tipoServicioTransporteDTO == null)
				return null;

			sWhere = Utilerias
					.agregaParametroQuery(listaParametrosValidos, sWhere,
							"codigoTipoServicioTransporte",
							tipoServicioTransporteDTO
									.getCodigoTipoServicioTransporte());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "descripcionTipoServicioTra",
					tipoServicioTransporteDTO.getDescripcionTipoServicioTra());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoServicioTransporteDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoServicioTransporteDTO findById(double id) {
		return null;
	}

	public List<TipoServicioTransporteDTO> listRelated(Object id) {
		return findAll();
	}
}