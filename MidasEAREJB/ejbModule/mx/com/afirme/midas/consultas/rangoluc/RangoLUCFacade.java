package mx.com.afirme.midas.consultas.rangoluc;

// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.usuario.LogUtil;

/**
 * Facade for entity RangoLUCDTO.
 * 
 * @see .RangoLUCDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class RangoLUCFacade implements RangoLUCFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved RangoLUCDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            RangoLUCDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RangoLUCDTO entity) {
		LogUtil.log("saving RangoLUCDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent RangoLUCDTO entity.
	 * 
	 * @param entity
	 *            RangoLUCDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RangoLUCDTO entity) {
		LogUtil.log("deleting RangoLUCDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(RangoLUCDTO.class, entity
					.getIdtorangoluc());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved RangoLUCDTO entity and return it or a copy of
	 * it to the sender. A copy of the RangoLUCDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            RangoLUCDTO entity to update
	 * @return RangoLUCDTO the persisted RangoLUCDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RangoLUCDTO update(RangoLUCDTO entity) {
		LogUtil.log("updating RangoLUCDTO instance", Level.INFO, null);
		try {
			RangoLUCDTO result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RangoLUCDTO findById(BigDecimal id) {
		LogUtil.log("finding RangoLUCDTO instance with id: " + id, Level.INFO,
				null);
		try {
			RangoLUCDTO instance = entityManager.find(RangoLUCDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogUtil.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RangoLUCDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RangoLUCDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RangoLUCDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<RangoLUCDTO> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding RangoLUCDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from RangoLUCDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all RangoLUCDTO entities.
	 * 
	 * @return List<RangoLUCDTO> all RangoLUCDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<RangoLUCDTO> findAll() {
		LogUtil.log("finding all RangoLUCDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from RangoLUCDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	public RangoLUCDTO getLimiteInferior() {
		LogUtil.log("finding getLimiteInferior RangoLUCDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from RangoLUCDTO model where model.limiteInferior = (select MIN(model2.limiteInferior) from RangoLUCDTO model2)";
			Query query = entityManager.createQuery(queryString);
			return (RangoLUCDTO) query.getSingleResult();
		} catch (RuntimeException re) {
			LogUtil.log("find getLimiteInferior failed", Level.SEVERE, re);
			throw re;
		}
	}

}