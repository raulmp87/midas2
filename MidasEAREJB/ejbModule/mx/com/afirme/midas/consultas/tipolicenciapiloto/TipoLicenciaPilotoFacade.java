package mx.com.afirme.midas.consultas.tipolicenciapiloto;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity TipoLicenciaPilotoDTO.
 * 
 * @see .TipoLicenciaPilotoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoLicenciaPilotoFacade implements TipoLicenciaPilotoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoLicenciaPilotoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            TipoLicenciaPilotoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoLicenciaPilotoDTO entity) {
		LogDeMidasEJB3.log("saving TipoLicenciaPilotoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoLicenciaPilotoDTO entity.
	 * 
	 * @param entity
	 *            TipoLicenciaPilotoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoLicenciaPilotoDTO entity) {
		LogDeMidasEJB3.log("deleting TipoLicenciaPilotoDTO instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(TipoLicenciaPilotoDTO.class,
					entity.getIdTipoLicenciaPiloto());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoLicenciaPilotoDTO entity and return it or a
	 * copy of it to the sender. A copy of the TipoLicenciaPilotoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoLicenciaPilotoDTO entity to update
	 * @return TipoLicenciaPilotoDTO the persisted TipoLicenciaPilotoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoLicenciaPilotoDTO update(TipoLicenciaPilotoDTO entity) {
		LogDeMidasEJB3.log("updating TipoLicenciaPilotoDTO instance", Level.INFO, null);
		try {
			TipoLicenciaPilotoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoLicenciaPilotoDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding TipoLicenciaPilotoDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoLicenciaPilotoDTO instance = entityManager.find(
					TipoLicenciaPilotoDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoLicenciaPilotoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoLicenciaPilotoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoLicenciaPilotoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoLicenciaPilotoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TipoLicenciaPilotoDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoLicenciaPilotoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoLicenciaPilotoDTO entities.
	 * 
	 * @return List<TipoLicenciaPilotoDTO> all TipoLicenciaPilotoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoLicenciaPilotoDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoLicenciaPilotoDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from TipoLicenciaPilotoDTO model " +
					"order by model.descripcionTipoLicenciaPiloto";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoLicenciaPilotoDTO> listarFiltrado(TipoLicenciaPilotoDTO tipoLicenciaPilotoDTO){
		
		try{
			String queryString = "select  model from TipoLicenciaPilotoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if(tipoLicenciaPilotoDTO == null)
				return null;
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoTipoLicenciaPiloto", tipoLicenciaPilotoDTO.getCodigoTipoLicenciaPiloto());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionTipoLicenciaPiloto", tipoLicenciaPilotoDTO.getDescripcionTipoLicenciaPiloto());
			
			if(Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		}catch(RuntimeException re){
			throw re;
		}
	}

}