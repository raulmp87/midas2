package mx.com.afirme.midas.consultas.tipotransporte;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity TipoTransporteDTO.
 * 
 * @see .TipoTransporteDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoTransporteFacade implements TipoTransporteFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoTransporteDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoTransporteDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoTransporteDTO entity) {
		LogDeMidasEJB3.log("saving TipoTransporteDTO instance", Level.INFO,
				null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoTransporteDTO entity.
	 * 
	 * @param entity
	 *            TipoTransporteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoTransporteDTO entity) {
		LogDeMidasEJB3.log("deleting TipoTransporteDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(TipoTransporteDTO.class, entity
					.getIdTipoTransporte());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoTransporteDTO entity and return it or a
	 * copy of it to the sender. A copy of the TipoTransporteDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoTransporteDTO entity to update
	 * @return TipoTransporteDTO the persisted TipoTransporteDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoTransporteDTO update(TipoTransporteDTO entity) {
		LogDeMidasEJB3.log("updating TipoTransporteDTO instance", Level.INFO,
				null);
		try {
			TipoTransporteDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoTransporteDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding TipoTransporteDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoTransporteDTO instance = entityManager.find(
					TipoTransporteDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoTransporteDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoTransporteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoTransporteDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoTransporteDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TipoTransporteDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoTransporteDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoTransporteDTO entities.
	 * 
	 * @return List<TipoTransporteDTO> all TipoTransporteDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoTransporteDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoTransporteDTO instances",
				Level.INFO, null);
		try {
			final String queryString = "select model from TipoTransporteDTO model " +
					"order by model.descripcionTipoTransporte";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TipoTransporteDTO> listarFiltrado(
			TipoTransporteDTO tipoTransporteDTO) {

		try {
			String queryString = "select model from TipoTransporteDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (tipoTransporteDTO == null)
				return null;

			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigoTipoTransporte", tipoTransporteDTO
							.getCodigoTipoTransporte());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "descripcionTipoTransporte", tipoTransporteDTO
							.getDescripcionTipoTransporte());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoTransporteDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoTransporteDTO findById(double id) {
		return null;
	}

	public List<TipoTransporteDTO> listRelated(Object id) {
		return findAll();
	}

}