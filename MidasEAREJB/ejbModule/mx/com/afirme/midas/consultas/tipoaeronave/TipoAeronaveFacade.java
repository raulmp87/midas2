package mx.com.afirme.midas.consultas.tipoaeronave;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity TipoAeronaveDTO.
 * 
 * @see .TipoAeronaveDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoAeronaveFacade implements TipoAeronaveFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoAeronaveDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoAeronaveDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoAeronaveDTO entity) {
		LogDeMidasEJB3.log("saving TipoAeronaveDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoAeronaveDTO entity.
	 * 
	 * @param entity
	 *            TipoAeronaveDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoAeronaveDTO entity) {
		LogDeMidasEJB3.log("deleting TipoAeronaveDTO instance", Level.INFO,
				null);
		try {
			entity = entityManager.getReference(TipoAeronaveDTO.class, entity
					.getIdTipoAeronave());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoAeronaveDTO entity and return it or a copy
	 * of it to the sender. A copy of the TipoAeronaveDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoAeronaveDTO entity to update
	 * @return TipoAeronaveDTO the persisted TipoAeronaveDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoAeronaveDTO update(TipoAeronaveDTO entity) {
		LogDeMidasEJB3.log("updating TipoAeronaveDTO instance", Level.INFO,
				null);
		try {
			TipoAeronaveDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoAeronaveDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding TipoAeronaveDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoAeronaveDTO instance = entityManager.find(
					TipoAeronaveDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoAeronaveDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoAeronaveDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoAeronaveDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoAeronaveDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TipoAeronaveDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoAeronaveDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoAeronaveDTO entities.
	 * 
	 * @return List<TipoAeronaveDTO> all TipoAeronaveDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoAeronaveDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoAeronaveDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from TipoAeronaveDTO model " +
					"order by model.descripcionTipoAeronave";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TipoAeronaveDTO> listarFiltrado(TipoAeronaveDTO tipoAeronaveDTO) {

		try {
			String queryString = "select  model from TipoAeronaveDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();

			if (tipoAeronaveDTO == null)
				return null;

			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,
					sWhere, "codigoTipoAeronave", tipoAeronaveDTO
							.getCodigoTipoAeronave());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,
					sWhere, "descripcionTipoAeronave", tipoAeronaveDTO
							.getDescripcionTipoAeronave());

			if (Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);

			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public TipoAeronaveDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoAeronaveDTO findById(double id) {
		return null;
	}

	public List<TipoAeronaveDTO> listRelated(Object id) {
		return findAll();
	}

}