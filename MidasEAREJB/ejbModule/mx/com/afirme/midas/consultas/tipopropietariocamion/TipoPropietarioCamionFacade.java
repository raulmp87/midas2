package mx.com.afirme.midas.consultas.tipopropietariocamion;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * Facade for entity TipoPropietarioCamionDTO.
 * 
 * @see .TipoPropietarioCamionDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class TipoPropietarioCamionFacade implements
		TipoPropietarioCamionFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved TipoPropietarioCamionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            TipoPropietarioCamionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoPropietarioCamionDTO entity) {
		LogDeMidasEJB3.log("saving TipoPropietarioCamionDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent TipoPropietarioCamionDTO entity.
	 * 
	 * @param entity
	 *            TipoPropietarioCamionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoPropietarioCamionDTO entity) {
		LogDeMidasEJB3
				.log("deleting TipoPropietarioCamionDTO instance", Level.INFO,
						null);
		try {
			entity = entityManager.getReference(TipoPropietarioCamionDTO.class,
					entity.getIdTipoPropietarioTransporte());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved TipoPropietarioCamionDTO entity and return it or
	 * a copy of it to the sender. A copy of the TipoPropietarioCamionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoPropietarioCamionDTO entity to update
	 * @return TipoPropietarioCamionDTO the persisted TipoPropietarioCamionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoPropietarioCamionDTO update(TipoPropietarioCamionDTO entity) {
		LogDeMidasEJB3
				.log("updating TipoPropietarioCamionDTO instance", Level.INFO,
						null);
		try {
			TipoPropietarioCamionDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public TipoPropietarioCamionDTO findById(BigDecimal id) {
		LogDeMidasEJB3.log("finding TipoPropietarioCamionDTO instance with id: " + id,
				Level.INFO, null);
		try {
			TipoPropietarioCamionDTO instance = entityManager.find(
					TipoPropietarioCamionDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoPropietarioCamionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoPropietarioCamionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoPropietarioCamionDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TipoPropietarioCamionDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding TipoPropietarioCamionDTO instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from TipoPropietarioCamionDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all TipoPropietarioCamionDTO entities.
	 * 
	 * @return List<TipoPropietarioCamionDTO> all TipoPropietarioCamionDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<TipoPropietarioCamionDTO> findAll() {
		LogDeMidasEJB3.log("finding all TipoPropietarioCamionDTO instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from TipoPropietarioCamionDTO model " +
					"order by model.descripcionTipoPropietarioTra";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoPropietarioCamionDTO> listarFiltrado(TipoPropietarioCamionDTO tipoPropietarioCamionDTO){
		
		try{
			String queryString = "select  model from TipoPropietarioCamionDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if(tipoPropietarioCamionDTO == null)
				return null;
			
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "codigoTipoPropTransporte", tipoPropietarioCamionDTO.getCodigoTipoPropTransporte());
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcionTipoPropietarioTra", tipoPropietarioCamionDTO.getDescripcionTipoPropietarioTra());
			
			if(Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		}catch(RuntimeException re){
			throw re;
		}
	}

	public TipoPropietarioCamionDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	public TipoPropietarioCamionDTO findById(double id) {
		return this.findById(BigDecimal.valueOf(id));
	}

	public List<TipoPropietarioCamionDTO> listRelated(Object id) {
		return this.findAll();
	}
}