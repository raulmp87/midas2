package mx.com.afirme.midas.consultas.formapago;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.formapago.FormaPagoFacadeRemote;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SystemException;

@Stateless
public class FormaPagoInterfazServicios implements FormaPagoInterfazServiciosRemote{

	@EJB
	private FormaPagoFacadeRemote formaPagoInterfazFacade;
	
	
	public List<FormaPagoIDTO> listarFiltrado(FormaPagoIDTO formaPago, String nombreUsuario) {
		try {
			return formaPagoInterfazFacade.findByProperty(formaPago, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.CIDFORMA_PAGO");
			sb.append("|");
			sb.append("pId_Forma_Pago" + "=" + formaPago.getIdFormaPago() + ",");
			sb.append("pDescripcion" + "=" + formaPago.getDescripcion() + ",");
			sb.append("pId_Moneda" + "=" + formaPago.getIdMoneda() + ",");

			
			LogDeMidasEJB3.log("Error al consultar listado de clientes: "+sb, Level.SEVERE, e);
			return null;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error al consultar listado de clientes. ", Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}
	
	public List<FormaPagoIDTO> listarFiltradoClaveNegocio(FormaPagoIDTO formaPago, String nombreUsuario, String claveNegocio) {
		try {
			return formaPagoInterfazFacade.findByProperty(formaPago, nombreUsuario, claveNegocio);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.CIDFORMA_PAGO");
			sb.append("|");
			sb.append("pId_Forma_Pago" + "=" + formaPago.getIdFormaPago() + ",");
			sb.append("pDescripcion" + "=" + formaPago.getDescripcion() + ",");
			sb.append("pId_Moneda" + "=" + formaPago.getIdMoneda() + ",");
			sb.append("pcveOrigen" + "=" + claveNegocio + ",");
			
			LogDeMidasEJB3.log("Error al consultar la forma de pago: "+sb, Level.SEVERE, e);
			return null;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error al consultar la forma de pago. ", Level.SEVERE, e);
			throw new RuntimeException(e);
		}
	}


	public FormaPagoIDTO getPorId(Integer idFormaPago, Short idMoneda,String nombreUsuario)throws SystemException {
		if (idFormaPago == null){
			return null;
		}
		FormaPagoIDTO formaPago = new FormaPagoIDTO(idFormaPago,null,idMoneda);
		
		List<FormaPagoIDTO> listaTMP = listarFiltrado(formaPago, nombreUsuario);
		if (listaTMP != null && !listaTMP.isEmpty()){
			return listaTMP.get(0);
		}
		else{
			return null;
		}
	}
	
	public FormaPagoIDTO getPorIdClaveNegocio(Integer idFormaPago, Short idMoneda,String nombreUsuario, String claveNegocio)throws SystemException {
		if (idFormaPago == null){
			return null;
		}
		FormaPagoIDTO formaPago = new FormaPagoIDTO(idFormaPago,null,idMoneda);
		
		List<FormaPagoIDTO> listaTMP = listarFiltradoClaveNegocio(formaPago, nombreUsuario, claveNegocio);
		if (listaTMP != null && !listaTMP.isEmpty()){
			return listaTMP.get(0);
		}
		else{
			return null;
		}
	}


	public List<FormaPagoIDTO> listarFiltrado(Integer idFormaPago,String descripcion, Short idMoneda,String nombreUsuario) throws SystemException {
		FormaPagoIDTO formaPago = new FormaPagoIDTO();
		formaPago.setIdFormaPago(idFormaPago);
		formaPago.setDescripcion(descripcion);
		formaPago.setIdMoneda(idMoneda);
		
		return listarFiltrado(formaPago, nombreUsuario);
	}


	public List<FormaPagoIDTO> listarTodos(Short idMoneda,String nombreUsuario) throws SystemException {
		FormaPagoIDTO formaPago = new FormaPagoIDTO();
		formaPago.setIdFormaPago(null);
		formaPago.setDescripcion(null);
		formaPago.setIdMoneda(idMoneda);
		
		return listarFiltrado(formaPago, nombreUsuario);
	}

}
