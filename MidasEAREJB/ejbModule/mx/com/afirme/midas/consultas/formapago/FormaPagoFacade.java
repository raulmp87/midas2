package mx.com.afirme.midas.consultas.formapago;
// default package

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity FormaPagoDTO.
 * 
 * @see .FormaPagoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class FormaPagoFacade implements FormaPagoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved FormaPagoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            FormaPagoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(FormaPagoDTO entity) {
		LogDeMidasEJB3.log("saving FormaPagoDTO instance", Level.FINE, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent FormaPagoDTO entity.
	 * 
	 * @param entity
	 *            FormaPagoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(FormaPagoDTO entity) {
		LogDeMidasEJB3.log("deleting FormaPagoDTO instance", Level.FINE, null);
		try {
			entity = entityManager
					.getReference(FormaPagoDTO.class, entity.getIdFormaPago());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.FINE, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved FormaPagoDTO entity and return it or a copy of it
	 * to the sender. A copy of the FormaPagoDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            FormaPagoDTO entity to update
	 * @return FormaPagoDTO the persisted FormaPagoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public FormaPagoDTO update(FormaPagoDTO entity) {
		LogDeMidasEJB3.log("updating FormaPagoDTO instance", Level.FINE, null);
		try {
			FormaPagoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.FINE, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public FormaPagoDTO findById(FormaPagoDTO id) {
		LogDeMidasEJB3.log("finding FormaPagoDTO instance with id: " + id.getIdFormaPago(), Level.FINE,
				null);
		try {
			FormaPagoDTO instance = entityManager.find(FormaPagoDTO.class, id.getIdFormaPago());
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all FormaPagoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the FormaPagoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<FormaPagoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<FormaPagoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding FormaPagoDTO instance with property: " + propertyName
				+ ", value: " + value, Level.FINE, null);
		try {
			final String queryString = "select model from FormaPagoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all FormaPagoDTO entities.
	 * 
	 * @return List<FormaPagoDTO> all FormaPagoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<FormaPagoDTO> findAll() {
		LogDeMidasEJB3.log("finding all FormaPagoDTO instances", Level.FINE, null);
		try {
			final String queryString = "select model from FormaPagoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	
	@SuppressWarnings("unchecked")
	public List<FormaPagoDTO> listarFiltrado(FormaPagoDTO formaPagoDTO){
		
		try{
			String queryString = "select  model from FormaPagoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if(formaPagoDTO == null)
				return null;
			
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcion", formaPagoDTO.getDescripcion());
			
			if(Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		}catch(RuntimeException re){
			throw re;
		}
	}
}