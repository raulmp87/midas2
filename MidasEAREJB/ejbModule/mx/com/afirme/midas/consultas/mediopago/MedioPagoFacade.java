package mx.com.afirme.midas.consultas.mediopago;
// default package

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;

/**
 * Facade for entity MedioPagoDTO.
 * 
 * @see .MedioPagoDTO
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class MedioPagoFacade implements MedioPagoFacadeRemote {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved MedioPagoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            MedioPagoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(MedioPagoDTO entity) {
		LogDeMidasEJB3.log("saving MedioPagoDTO instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent MedioPagoDTO entity.
	 * 
	 * @param entity
	 *            MedioPagoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(MedioPagoDTO entity) {
		LogDeMidasEJB3.log("deleting MedioPagoDTO instance", Level.INFO, null);
		try {
			entity = entityManager
					.getReference(MedioPagoDTO.class, entity.getIdMedioPago());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved MedioPagoDTO entity and return it or a copy of it
	 * to the sender. A copy of the MedioPagoDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            MedioPagoDTO entity to update
	 * @return MedioPagoDTO the persisted MedioPagoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public MedioPagoDTO update(MedioPagoDTO entity) {
		LogDeMidasEJB3.log("updating MedioPagoDTO instance", Level.INFO, null);
		try {
			MedioPagoDTO result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public MedioPagoDTO findById(MedioPagoDTO id) {
		LogDeMidasEJB3.log("finding MedioPagoDTO instance with id: " + id, Level.INFO,
				null);
		try {
			MedioPagoDTO instance = entityManager.find(MedioPagoDTO.class, id.getIdMedioPago());
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all MedioPagoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the MedioPagoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<MedioPagoDTO> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<MedioPagoDTO> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding MedioPagoDTO instance with property: " + propertyName
				+ ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from MedioPagoDTO model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all MedioPagoDTO entities.
	 * 
	 * @return List<MedioPagoDTO> all MedioPagoDTO entities
	 */
	@SuppressWarnings("unchecked")
	public List<MedioPagoDTO> findAll() {
		LogDeMidasEJB3.log("finding all MedioPagoDTO instances", Level.INFO, null);
		try {
			final String queryString = "select model from MedioPagoDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<MedioPagoDTO> listarFiltrado(MedioPagoDTO medioPagoDTO){
		
		try{
			String queryString = "select  model from MedioPagoDTO AS model ";
			String sWhere = "";
			Query query;
			List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
			
			if(medioPagoDTO == null)
				return null;
			
			sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "descripcion", medioPagoDTO.getDescripcion());
			
			if(Utilerias.esAtributoQueryValido(sWhere))
				queryString = queryString.concat(" where ").concat(sWhere);
			
			query = entityManager.createQuery(queryString);
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		}catch(RuntimeException re){
			throw re;
		}
	}
	
}