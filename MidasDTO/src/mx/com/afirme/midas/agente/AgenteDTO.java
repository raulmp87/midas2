package mx.com.afirme.midas.agente;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso1Checks;

/**
 * AgenteDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "VNAGENTE", schema = "MIDAS")
public class AgenteDTO extends CacheableDTO implements Serializable, Entidad {

	private static final long serialVersionUID = 1L;
	
	//Objetos usados en el mapeo de los procedure de sycos para Obtener Agentes
	private Integer idTcAgente;  // ID_Agente  		Identificador del agente  
	private String nombre;       // Nombre 			Nombre completo del agente 
	private String tipoCedula;   // Tipo_Cedula 		tipo de cedula
	private String email;        // E_mail_agte 		correo electronico
	private Short tipoPersona;  //Tipo_Persona 		F)isica � M)oral 
	private String telefonoOficina;   //Telefono		numero telefonico
	private Short promotoria; //ID_Promotoria 		identificador de la promotoria (supervisoria)
	private String  nombrePromotoria;// Nom_Promotoria 	nombre de la promotoria (supervisoria) 
	private String idOficina;   //  ID_Oficina 		Identificador de la oficina (ejecutivo)
	private String nombreOficina;//Nom_Oficina 		nombre de la oficina (ejecutivo) 
	private String idGerencia;// ID_Gerencia 		Identificador de la gerencia 
	private String nombreGerencia;//Nom_Gerencia 		nombre de la gerencia
	private String direccion;  //Calle_numero 		domicilio
	private Integer idColonia; // Id_Colonia 		identificador de la colonia 
	private String colonia;	   // Colonia 			nombre de la colonia
	private String codigoPostal;// Codigo_Postal 		codigo postal
	private String idCiudad;//Id_Ciudad 			identificador de la ciudad
	private String ciudad;    //Ciudad 			nombre de la ciudad y/o municipio 
	private String idEstado;  //Id_Estado 			identificador del estado
	private String estado;    // Estado 			nombre del estado
	private String generaComision;// b_comision 		genera comision (Verdadero Falso)
	private String contabilizaComision;// b_contab_comis 	contabiliza comision (Verdadero Falso)
	private String generaCheque;// b_genera_cheque 		genera cheque (Verdadero Falso)
	private String imprimeEstadoCuenta;// b_imp_edo_cta 	imprime estado de cuenta (Verdadero Falso)
	
	private String pais;
	private String rfc;
	private String numeroOficina;
	private String situacionAgente;
	private Date fechaVencimientoCedula;
	private String emailOficina;
	private String emailGerencia;


	
	// Property accessors
	@Id
	@Column(name = "IDTCAGENTE", nullable = false, precision = 8, scale = 0)
	@NotNull(groups=CotizacionExpressPaso1Checks.class, message="{com.afirme.midas2.requerido}")
	public Integer getIdTcAgente() {
		return this.idTcAgente;
	}

	public void setIdTcAgente(Integer idTcAgente) {
		this.idTcAgente = idTcAgente;
	}

	@Column(name = "NOMBRE", length = 80)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "RFC", length = 20)
	public String getRfc() {
		return this.rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	@Transient
	public String getIdOficina() {
		return idOficina;
	}

	public void setIdOficina(String idOficina) {
		this.idOficina = idOficina;
	}

	/**
	 * @return the nombreOficina
	 */
	@Transient
	public String getNombreOficina() {
		return nombreOficina;
	}

	/**
	 * @param nombreOficina the nombreOficina to set
	 */
	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}

	@Column(name = "TELEFONOOFICINA", length = 12)
	public String getTelefonoOficina() {
		return this.telefonoOficina;
	}

	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}
	
	@Transient
	public String getNumeroOficina() {
		return this.numeroOficina;
	}

	public void setNumeroOficina(String numeroOficina) {
		this.numeroOficina = numeroOficina;
	}

	@Column(name = "EMAIL", length = 60)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "PROMOTORIA", precision = 4, scale = 0)
	public Short getPromotoria() {
		return this.promotoria;
	}

	public void setPromotoria(Short promotoria) {
		this.promotoria = promotoria;
	}

	@Column(name = "GENERACOMISION", length = 1)
	public String getGeneraComision() {
		return this.generaComision;
	}

	public void setGeneraComision(String generaComision) {
		this.generaComision = generaComision;
	}

	@Column(name = "DIRECCION", length = 100)
	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Column(name = "COLONIA", length = 100)
	public String getColonia() {
		return this.colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	@Column(name = "CIUDAD", length = 40)
	public String getCiudad() {
		return this.ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	@Column(name = "ESTADO", length = 40)
	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(name = "PAIS", length = 40)
	public String getPais() {
		return this.pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	/**
	 * @return the tipoCedula
	 */
	public String getTipoCedula() {
		return tipoCedula;
	}

	/**
	 * @param tipoCedula the tipoCedula to set
	 */
	public void setTipoCedula(String tipoCedula) {
		this.tipoCedula = tipoCedula;
	}

	@Override
	public String getDescription() {
		return nombre;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof AgenteDTO) {
			AgenteDTO agenteDTO = (AgenteDTO) object;
			equal = agenteDTO.getIdTcAgente().equals(this.idTcAgente);
		}
		return equal;
	}

	@Override
	public Object getId() {
		return this.idTcAgente;
	}

	@Transient
	public String getContabilizaComision() {
		return contabilizaComision;
	}

	public void setContabilizaComision(String contabilizaComision) {
		this.contabilizaComision = contabilizaComision;
	}

	@Transient
	public String getGeneraCheque() {
		return generaCheque;
	}

	public void setGeneraCheque(String generaCheque) {
		this.generaCheque = generaCheque;
	}

	@Transient
	public String getImprimeEstadoCuenta() {
		return imprimeEstadoCuenta;
	}

	public void setImprimeEstadoCuenta(String imprimeEstadoCuenta) {
		this.imprimeEstadoCuenta = imprimeEstadoCuenta;
	}

	@Transient
	public String getSituacionAgente() {
		return situacionAgente;
	}

	public void setSituacionAgente(String situacionAgente) {
		this.situacionAgente = situacionAgente;
	}

	@Transient
	public Date getFechaVencimientoCedula() {
		return fechaVencimientoCedula;
	}

	public void setFechaVencimientoCedula(Date fechaVencimientoCedula) {
		this.fechaVencimientoCedula = fechaVencimientoCedula;
	}
	
	public void setTipoPersona(Short tipoPersona) {
	    this.tipoPersona = tipoPersona;
	}
	@Transient
	public Short getTipoPersona() {
	    return tipoPersona;
	}

	public void setNombrePromotoria(String nombrePromotoria) {
	    this.nombrePromotoria = nombrePromotoria;
	}
	@Transient
	public String getNombrePromotoria() {
	    return nombrePromotoria;
	}

	public void setIdGerencia(String idGerencia) {
	    this.idGerencia = idGerencia;
	}
	@Transient
	public String getIdGerencia() {
	    return idGerencia;
	}

	public void setNombreGerencia(String nombreGerencia) {
	    this.nombreGerencia = nombreGerencia;
	}
	@Transient
	public String getNombreGerencia() {
	    return nombreGerencia;
	}

	public void setIdColonia(Integer idColonia) {
	    this.idColonia = idColonia;
	}
	@Transient
	public Integer getIdColonia() {
	    return idColonia;
	}

	public void setCodigoPostal(String codigoPostal) {
	    this.codigoPostal = codigoPostal;
	}
	@Transient
	public String getCodigoPostal() {
	    return codigoPostal;
	}

	public void setIdCiudad(String idCiudad) {
	    this.idCiudad = idCiudad;
	}
	@Transient
	public String getIdCiudad() {
	    return idCiudad;
	}

	public void setIdEstado(String idEstado) {
	    this.idEstado = idEstado;
	}
	@Transient
	public String getIdEstado() {
	    return idEstado;
	}
	@Transient
	public String getEmailOficina() {
		return emailOficina;
	}

	public void setEmailOficina(String emailOficina) {
		this.emailOficina = emailOficina;
	}
	@Transient
	public String getEmailGerencia() {
		return emailGerencia;
	}

	public void setEmailGerencia(String emailGerencia) {
		this.emailGerencia = emailGerencia;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer getKey() {
		return this.idTcAgente;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	
}