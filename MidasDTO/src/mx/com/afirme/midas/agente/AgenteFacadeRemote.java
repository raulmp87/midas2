package mx.com.afirme.midas.agente;

import java.util.List;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for AgenteDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */
public interface AgenteFacadeRemote extends MidasInterfaceBase<AgenteDTO> {
	/**
	 * Perform an initial save of a previously unsaved AgenteDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            AgenteDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(AgenteDTO entity);

	/**
	 * Delete a persistent AgenteDTO entity.
	 * 
	 * @param entity
	 *            AgenteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(AgenteDTO entity);

	/**
	 * Persist a previously saved AgenteDTO entity and return it or a copy of it
	 * to the sender. A copy of the AgenteDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            AgenteDTO entity to update
	 * @return AgenteDTO the persisted AgenteDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public AgenteDTO update(AgenteDTO entity);

	public AgenteDTO findById(AgenteDTO id);

	/**
	 * Find all AgenteDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the AgenteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<AgenteDTO> found by query
	 */
	public List<AgenteDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all AgenteDTO entities.
	 * 
	 * @return List<AgenteDTO> all AgenteDTO entities
	 */
	public List<AgenteDTO> findAll();
	
	/**
	 * Lista todos los agente con el nombre
	 * @param nombre
	 * @return
	 */
	public List<AgenteDTO> listarAgentesBusqueda(String nombre);
}