package mx.com.afirme.midas.endoso;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.rehabilitacion.EndosoRehabilitableDTO;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;

/**
 * Remote interface for EndosoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface EndosoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved EndosoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            EndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public EndosoDTO save(EndosoDTO entity);

	/**
	 * Delete a persistent EndosoDTO entity.
	 * 
	 * @param entity
	 *            EndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(EndosoDTO entity);

	/**
	 * Persist a previously saved EndosoDTO entity and return it or a copy of it
	 * to the sender. A copy of the EndosoDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            EndosoDTO entity to update
	 * @return EndosoDTO the persisted EndosoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public EndosoDTO update(EndosoDTO entity);

	public EndosoDTO findById(EndosoId id);

	/**
	 * Find all EndosoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the EndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<EndosoDTO> found by query
	 */
	public List<EndosoDTO> findByProperty(String propertyName, Object value, boolean ascendingOrder);

//	public List<EndosoDTO> findByPropertyWhitDescriptions(String propertyName,final Object value, boolean ascendingOrder); 
	
	/**
	 * Find all EndosoDTO entities.
	 * 
	 * @return List<EndosoDTO> all EndosoDTO entities
	 */
	public List<EndosoDTO> findAll();

	public EndosoDTO getUltimoEndoso(BigDecimal idToPoliza);

	public EndosoDTO getPenultimoEndoso(BigDecimal idToPoliza);

	public Map<String, String> emitirEndoso(CotizacionDTO cotizacionDTO,
			Double ivaCotizacion, boolean existeReciboPagado, List<ReciboDTO> recibos, 
			EndosoIDTO endosoIDTO);

	public void insertEndosoDePoliza(BigDecimal idToPoliza, int numeroEndoso,
			short tipoEndoso, double factorAplicacion);

	public void insertEndosoDeCotizacion(BigDecimal idToCotizacion,
			BigDecimal idToPoliza, int numeroEndoso, short tipoEndoso,
			double factorAplicacion);

	public BigDecimal obtenerCantidadEndososPoliza(BigDecimal idToPoliza);

	public void emiteEndosoCancelacionAutomatica(
			CotizacionDTO cotizacionOrigen, String usuarioCreacion,
			Double ivaCotizacion, boolean existeReciboPagado, List<ReciboDTO> recibos, 
			EndosoIDTO endosoIDTO);

	public void emiteEndosoRehabilitacion(CotizacionDTO cotizacionOrigen,
			String usuarioCreacion, Double ivaCotizacion,
			EndosoRehabilitableDTO endosoaProcesar);

	/**
	 * Obtiene las diferencias en cotizaciones de endoso
	 * 
	 * @param idToCotizacion
	 *            Id de la cotizacion
	 * @param nombreUsuario
	 *            Nombre del usuario logueado
	 * @return Listado con las diferencias en cotizaciones de endoso
	 * @throws Exception
	 */
	public List<DiferenciaCotizacionEndosoDTO> obtenerDiferenciasCotizacionEndoso(
			BigDecimal idToCotizacion, String nombreUsuario) throws Exception;
	
	public EndosoDTO buscarPorCotizacion(BigDecimal idToCotizacion);

	public EndosoDTO obtenerEndoso(BigDecimal idToPoliza,Short numeroEndoso);
	
	/**
	 * Regresa el numero de Endoso del ultimo Endoso de Tipo Cambio de Forma de Pago en la P�liza
	 * @param idPoliza Id de la p�liza
	 * @return El numero de Endoso del ultimo Endoso de Tipo Cambio de Forma de Pago en la P�liza, en caso contrario devuelve 0
	 */
	public Short buscarNumeroEndosoUltimoCFP(BigDecimal idPoliza);

	public Integer calculaTipoEndoso(CotizacionDTO cotizacionDTO);
	
	public boolean primerReciboPagado(EndosoDTO endoso,String nombreUsuario) throws Exception;
	
	public EndosoDTO getEndosoByPolizaCotizacion(BigDecimal idToPoliza, BigDecimal idToCotizacion);
	
	
	public String getNumeroEndoso(Integer numeroEndoso);
	
	public String getDescripcionClaveTipoEndoso(SolicitudDTO solicitudDTO);
	
	public String getDescripcionClaveMotivoEndoso(SolicitudDTO solicitudDTO);
}