package mx.com.afirme.midas.endoso.subinciso;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.endoso.EndosoDTO;

/**
 * SubIncisoEndosoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSUBINCISOEND", schema = "MIDAS")
public class SubIncisoEndosoDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private SubIncisoEndosoId id;
	private EndosoDTO endosoDTO;
	private Double valorSumaAsegurada;
	private Double valorPrimaNeta;

	// Constructors

	/** default constructor */
	public SubIncisoEndosoDTO() {
	}

	/** full constructor */
	public SubIncisoEndosoDTO(SubIncisoEndosoId id, EndosoDTO endosoDTO,
			Double valorSumaAsegurada, Double valorPrimaNeta) {
		this.id = id;
		this.endosoDTO = endosoDTO;
		this.valorSumaAsegurada = valorSumaAsegurada;
		this.valorPrimaNeta = valorPrimaNeta;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroEndoso", column = @Column(name = "NUMEROENDOSO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroSubInciso", column = @Column(name = "NUMEROSUBINCISO", nullable = false, precision = 22, scale = 0)) })
	public SubIncisoEndosoId getId() {
		return this.id;
	}

	public void setId(SubIncisoEndosoId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOPOLIZA", referencedColumnName = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROENDOSO", referencedColumnName = "NUMEROENDOSO", nullable = false, insertable = false, updatable = false) })
	public EndosoDTO getEndosoDTO() {
		return this.endosoDTO;
	}

	public void setEndosoDTO(EndosoDTO endosoDTO) {
		this.endosoDTO = endosoDTO;
	}

	@Column(name = "VALORSUMAASEGURADA", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}
}