package mx.com.afirme.midas.endoso.subinciso;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for SubIncisoEndosoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SubIncisoEndosoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SubIncisoEndosoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SubIncisoEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubIncisoEndosoDTO entity);

	/**
	 * Delete a persistent SubIncisoEndosoDTO entity.
	 * 
	 * @param entity
	 *            SubIncisoEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubIncisoEndosoDTO entity);

	/**
	 * Persist a previously saved SubIncisoEndosoDTO entity and return it or a
	 * copy of it to the sender. A copy of the SubIncisoEndosoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SubIncisoEndosoDTO entity to update
	 * @return SubIncisoEndosoDTO the persisted SubIncisoEndosoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubIncisoEndosoDTO update(SubIncisoEndosoDTO entity);

	public SubIncisoEndosoDTO findById(SubIncisoEndosoId id);

	/**
	 * Find all SubIncisoEndosoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SubIncisoEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubIncisoEndosoDTO> found by query
	 */
	public List<SubIncisoEndosoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SubIncisoEndosoDTO entities.
	 * 
	 * @return List<SubIncisoEndosoDTO> all SubIncisoEndosoDTO entities
	 */
	public List<SubIncisoEndosoDTO> findAll();
	
	public List<SubIncisoEndosoDTO> listarFiltrado(SubIncisoEndosoId id);
	
	public void insertSubIncisoEndosoDePoliza(BigDecimal idToCotizacion,BigDecimal idToPoliza, int numeroEndoso, double diasPorDevengar,short tipoEndoso);
}