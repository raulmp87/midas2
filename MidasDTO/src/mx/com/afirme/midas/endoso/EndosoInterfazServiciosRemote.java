package mx.com.afirme.midas.endoso;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.interfaz.endoso.EndosoCoberturaDTO;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;


public interface EndosoInterfazServiciosRemote {

	/**
	 * Emite un endoso en Seycos
	 * 
	 * @param numPolizaEndoso
	 *            (Id de la p�liza | Numero del Endoso)
	 * @param nombreUsuario
	 *            Nombre del Usuario que realiza la operacion
	 * @return Listado con referencias a los recibos del Endoso emitido
	 * @throws Exception
	 */
	public List<EndosoIDTO> emiteEndoso(String numPolizaEndoso,String nombreUsuario) throws Exception;

	/**
	 * Valida si la poliza es Cancelable a inicio de vigencia
	 * 
	 * @param numPolizaEndoso
	 *            (Id de la p�liza | Numero del Endoso)
	 * @param nombreUsuario
	 *            Nombre del Usuario que realiza la operacion
	 * @param fechaConsulta
	 *            Fecha en la que se desea cancelar la poliza
	 * @param tipoEndoso
	 *            El tipo del Endoso           
	 * @return true si la poliza es Cancelable a inicio de vigencia, false en
	 *         caso contrario. En caso de no ser Cancelable, incluye el motivo
	 *         por lo cual no lo es
	 * @throws Exception
	 */
	public EndosoIDTO validaEsCancelable(String numPolizaEndoso,String nombreUsuario, Date fechaCancelacion,Integer tipoEndoso) throws Exception;

	/**
	 * Obtiene una lista con los documentos cancelables (con recibos vencidos)
	 * de una fecha determinada
	 * 
	 * @param fechaConsulta
	 *            Fecha de consulta para documentos cancelables (con recibos
	 *            vencidos)
	 * @param nombreUsuario
	 *            Nombre del Usuario que realiza la operacion
	 * @return Lista con los documentos cancelables
	 * @throws Exception
	 */
	public List<EndosoIDTO> obtieneListaCancelables(Date fechaConsulta,String nombreUsuario) throws Exception;

	/**
	 * Obtiene una lista con los documentos rehabilitables en una fecha
	 * determinada
	 * 
	 * @param fechaConsulta
	 *            Fecha de consulta para documentos rehabilitables
	 * @param nombreUsuario
	 *            Nombre del Usuario que realiza la operacion
	 * @return Lista con los documentos rehabilitables
	 * @throws Exception
	 */
	public List<EndosoIDTO> obtieneListaRehabilitables(Date fechaConsulta,String nombreUsuario) throws Exception;

	/**
	 * Obtiene una lista con las coberturas agrupada que pertenecen a un endoso
	 * determinado
	 * 
	 * @param id
	 *            Es el id del endoso a consultar que corespoonde a el
	 *            idToPoliza y en numeroEndoso
	 * @return Lista con coberturas Agrupadas
	 * @throws Exception
	 */
	public List<EndosoCoberturaDTO> obtieneCoberturasAgrupadas(EndosoId id,String nombreUsuario) throws Exception;
}
