package mx.com.afirme.midas.endoso.agrupacion;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for AgrupacionEndosoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface AgrupacionEndosoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved AgrupacionEndosoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            AgrupacionEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(AgrupacionEndosoDTO entity);

	/**
	 * Delete a persistent AgrupacionEndosoDTO entity.
	 * 
	 * @param entity
	 *            AgrupacionEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(AgrupacionEndosoDTO entity);

	/**
	 * Persist a previously saved AgrupacionEndosoDTO entity and return it or a
	 * copy of it to the sender. A copy of the AgrupacionEndosoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            AgrupacionEndosoDTO entity to update
	 * @return AgrupacionEndosoDTO the persisted AgrupacionEndosoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public AgrupacionEndosoDTO update(AgrupacionEndosoDTO entity);

	public AgrupacionEndosoDTO findById(AgrupacionEndosoId id);

	/**
	 * Find all AgrupacionEndosoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the AgrupacionEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<AgrupacionEndosoDTO> found by query
	 */
	public List<AgrupacionEndosoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all AgrupacionEndosoDTO entities.
	 * 
	 * @return List<AgrupacionEndosoDTO> all AgrupacionEndosoDTO entities
	 */
	public List<AgrupacionEndosoDTO> findAll();
	
	public List<AgrupacionEndosoDTO> listarFiltrado(AgrupacionEndosoId id);
	
	public void insertAgrupacionEndosoDePoliza(BigDecimal idToCotizacion, BigDecimal idToPoliza, int numeroEndoso, double diasPorDevengar,short tipoEndoso);
}