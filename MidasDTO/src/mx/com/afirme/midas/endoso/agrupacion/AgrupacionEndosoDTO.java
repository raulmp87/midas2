package mx.com.afirme.midas.endoso.agrupacion;

import java.math.BigDecimal;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.endoso.EndosoDTO;

/**
 * AgrupacionEndosoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOAGRUPACIONEND", schema = "MIDAS")
public class AgrupacionEndosoDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private AgrupacionEndosoId id;
	private EndosoDTO endosoDTO;
	private Short claveTipoAgrupacion;
	private BigDecimal idToSeccion;
	private Double valorSumaAsegurada;
	private Double valorCuota;
	private Double valorPrimaNeta;

	// Constructors

	/** default constructor */
	public AgrupacionEndosoDTO() {
	}

	/** full constructor */
	public AgrupacionEndosoDTO(AgrupacionEndosoId id, EndosoDTO endosoDTO,
			Short claveTipoAgrupacion, BigDecimal idToSeccion,
			Double valorSumaAsegurada, Double valorCuota, Double valorPrimaNeta) {
		this.id = id;
		this.endosoDTO = endosoDTO;
		this.claveTipoAgrupacion = claveTipoAgrupacion;
		this.idToSeccion = idToSeccion;
		this.valorSumaAsegurada = valorSumaAsegurada;
		this.valorCuota = valorCuota;
		this.valorPrimaNeta = valorPrimaNeta;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroEndoso", column = @Column(name = "NUMEROENDOSO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroAgrupacion", column = @Column(name = "NUMEROAGRUPACION", nullable = false, precision = 4, scale = 0)) })
	public AgrupacionEndosoId getId() {
		return this.id;
	}

	public void setId(AgrupacionEndosoId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOPOLIZA", referencedColumnName = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROENDOSO", referencedColumnName = "NUMEROENDOSO", nullable = false, insertable = false, updatable = false) })
	public EndosoDTO getEndosoDTO() {
		return this.endosoDTO;
	}

	public void setEndosoDTO(EndosoDTO endosoDTO) {
		this.endosoDTO = endosoDTO;
	}

	@Column(name = "CLAVETIPOAGRUPACION", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoAgrupacion() {
		return this.claveTipoAgrupacion;
	}

	public void setClaveTipoAgrupacion(Short claveTipoAgrupacion) {
		this.claveTipoAgrupacion = claveTipoAgrupacion;
	}

	@Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "VALORSUMAASEGURADA", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Column(name = "VALORCUOTA", nullable = false, precision = 16, scale = 10)
	public Double getValorCuota() {
		return this.valorCuota;
	}

	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}
}