package mx.com.afirme.midas.endoso;

import java.math.BigDecimal;
import java.util.List;
import mx.com.afirme.midas.solicitud.SolicitudDTO;

import javax.ejb.Remote;



public interface SolicitudEndosoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SolicitudDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SolicitudDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public SolicitudDTO save(SolicitudDTO entity);

	/**
	 * Delete a persistent SolicitudEndosoDTO entity.
	 * 
	 * @param entity
	 *            SolicitudEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SolicitudDTO entity);

	/**
	 * Persist a previously saved SolicitudEndosoDTO entity and return it or a copy of
	 * it to the sender. A copy of the SolicitudEndosoDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SolicitudEndosoDTO entity to update
	 * @return SolicitudEndosoDTO the persisted SolicitudEndosoDTO entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SolicitudDTO update(SolicitudDTO entity);

	public SolicitudDTO findById(BigDecimal id);

	/**
	 * Find all SolicitudEndosoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SolicitudEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SolicitudEndosoDTO> found by query
	 */
	public List<SolicitudDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all SolicitudEndosoDTO entities.
	 * 
	 * @return List<SolicitudEndosoDTO> all SolicitudEndosoDTO entities
	 */
	public List<SolicitudDTO> findAll();

	public List<SolicitudDTO> findByUserId(String id);

	public List<SolicitudDTO> findByEstatus(Short claveEstatus, String userId);

}
