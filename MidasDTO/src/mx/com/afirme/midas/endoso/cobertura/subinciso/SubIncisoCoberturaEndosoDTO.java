package mx.com.afirme.midas.endoso.cobertura.subinciso;

import java.math.BigDecimal;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoDTO;

/**
 * SubIncisoCoberturaEndosoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSUBINCISOCOBEND", schema = "MIDAS")
public class SubIncisoCoberturaEndosoDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private SubIncisoCoberturaEndosoId id;
	private CoberturaEndosoDTO coberturaEndosoDTO;
	private BigDecimal idTcSubRamo;
	private Double valorSumaAsegurada;
	private Double valorPrimaNeta;

	// Constructors

	/** default constructor */
	public SubIncisoCoberturaEndosoDTO() {
	}

	/** full constructor */
	public SubIncisoCoberturaEndosoDTO(SubIncisoCoberturaEndosoId id,
			CoberturaEndosoDTO coberturaEndosoDTO, BigDecimal idTcSubRamo,
			Double valorSumaAsegurada, Double valorPrimaNeta) {
		this.id = id;
		this.coberturaEndosoDTO = coberturaEndosoDTO;
		this.idTcSubRamo = idTcSubRamo;
		this.valorSumaAsegurada = valorSumaAsegurada;
		this.valorPrimaNeta = valorPrimaNeta;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroEndoso", column = @Column(name = "NUMEROENDOSO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroSubInciso", column = @Column(name = "NUMEROSUBINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)) })
	public SubIncisoCoberturaEndosoId getId() {
		return this.id;
	}

	public void setId(SubIncisoCoberturaEndosoId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOPOLIZA", referencedColumnName = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROENDOSO", referencedColumnName = "NUMEROENDOSO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false) })
	public CoberturaEndosoDTO getCoberturaEndosoDTO() {
		return this.coberturaEndosoDTO;
	}

	public void setCoberturaEndosoDTO(CoberturaEndosoDTO coberturaEndosoDTO) {
		this.coberturaEndosoDTO = coberturaEndosoDTO;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubRamo() {
		return this.idTcSubRamo;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	@Column(name = "VALORSUMAASEGURADA", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}
}