package mx.com.afirme.midas.endoso.cobertura.subinciso;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for SubIncisoCoberturaEndosoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SubIncisoCoberturaEndosoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * SubIncisoCoberturaEndosoDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            SubIncisoCoberturaEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubIncisoCoberturaEndosoDTO entity);

	/**
	 * Delete a persistent SubIncisoCoberturaEndosoDTO entity.
	 * 
	 * @param entity
	 *            SubIncisoCoberturaEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubIncisoCoberturaEndosoDTO entity);

	/**
	 * Persist a previously saved SubIncisoCoberturaEndosoDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * SubIncisoCoberturaEndosoDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            SubIncisoCoberturaEndosoDTO entity to update
	 * @return SubIncisoCoberturaEndosoDTO the persisted
	 *         SubIncisoCoberturaEndosoDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubIncisoCoberturaEndosoDTO update(SubIncisoCoberturaEndosoDTO entity);

	public SubIncisoCoberturaEndosoDTO findById(SubIncisoCoberturaEndosoId id);

	/**
	 * Find all SubIncisoCoberturaEndosoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SubIncisoCoberturaEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubIncisoCoberturaEndosoDTO> found by query
	 */
	public List<SubIncisoCoberturaEndosoDTO> findByProperty(
			String propertyName, Object value);

	/**
	 * Find all SubIncisoCoberturaEndosoDTO entities.
	 * 
	 * @return List<SubIncisoCoberturaEndosoDTO> all SubIncisoCoberturaEndosoDTO
	 *         entities
	 */
	public List<SubIncisoCoberturaEndosoDTO> findAll();

	public List<SubIncisoCoberturaEndosoDTO> listarFiltrado(SubIncisoCoberturaEndosoId id);	

	public void insertSubIncisoCoberturaEndosoDePoliza(BigDecimal idToCotizacion, BigDecimal idToPoliza, int numeroEndoso, double diasPorDevengar,short tipoEndoso);
}