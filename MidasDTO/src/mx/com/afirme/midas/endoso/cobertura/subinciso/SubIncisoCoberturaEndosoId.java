package mx.com.afirme.midas.endoso.cobertura.subinciso;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * SubIncisoCoberturaEndosoId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class SubIncisoCoberturaEndosoId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal idToPoliza;
	private Short numeroEndoso;
	private BigDecimal numeroInciso;
	private BigDecimal idToSeccion;
	private BigDecimal numeroSubInciso;
	private BigDecimal idToCobertura;

	// Constructors

	/** default constructor */
	public SubIncisoCoberturaEndosoId() {
	}

	/** full constructor */
	public SubIncisoCoberturaEndosoId(BigDecimal idToPoliza,
			Short numeroEndoso, BigDecimal numeroInciso,
			BigDecimal idToSeccion, BigDecimal numeroSubInciso,
			BigDecimal idToCobertura) {
		this.idToPoliza = idToPoliza;
		this.numeroEndoso = numeroEndoso;
		this.numeroInciso = numeroInciso;
		this.idToSeccion = idToSeccion;
		this.numeroSubInciso = numeroSubInciso;
		this.idToCobertura = idToCobertura;
	}

	// Property accessors

	@Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToPoliza() {
		return this.idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@Column(name = "NUMEROENDOSO", nullable = false, precision = 4, scale = 0)
	public Short getNumeroEndoso() {
		return this.numeroEndoso;
	}

	public void setNumeroEndoso(Short numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	@Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "NUMEROSUBINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroSubInciso() {
		return this.numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SubIncisoCoberturaEndosoId))
			return false;
		SubIncisoCoberturaEndosoId castOther = (SubIncisoCoberturaEndosoId) other;

		return ((this.getIdToPoliza() == castOther.getIdToPoliza()) || (this
				.getIdToPoliza() != null
				&& castOther.getIdToPoliza() != null && this.getIdToPoliza()
				.equals(castOther.getIdToPoliza())))
				&& ((this.getNumeroEndoso() == castOther.getNumeroEndoso()) || (this
						.getNumeroEndoso() != null
						&& castOther.getNumeroEndoso() != null && this
						.getNumeroEndoso().equals(castOther.getNumeroEndoso())))
				&& ((this.getNumeroInciso() == castOther.getNumeroInciso()) || (this
						.getNumeroInciso() != null
						&& castOther.getNumeroInciso() != null && this
						.getNumeroInciso().equals(castOther.getNumeroInciso())))
				&& ((this.getIdToSeccion() == castOther.getIdToSeccion()) || (this
						.getIdToSeccion() != null
						&& castOther.getIdToSeccion() != null && this
						.getIdToSeccion().equals(castOther.getIdToSeccion())))
				&& ((this.getNumeroSubInciso() == castOther
						.getNumeroSubInciso()) || (this.getNumeroSubInciso() != null
						&& castOther.getNumeroSubInciso() != null && this
						.getNumeroSubInciso().equals(
								castOther.getNumeroSubInciso())))
				&& ((this.getIdToCobertura() == castOther.getIdToCobertura()) || (this
						.getIdToCobertura() != null
						&& castOther.getIdToCobertura() != null && this
						.getIdToCobertura()
						.equals(castOther.getIdToCobertura())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToPoliza() == null ? 0 : this.getIdToPoliza()
						.hashCode());
		result = 37
				* result
				+ (getNumeroEndoso() == null ? 0 : this.getNumeroEndoso()
						.hashCode());
		result = 37
				* result
				+ (getNumeroInciso() == null ? 0 : this.getNumeroInciso()
						.hashCode());
		result = 37
				* result
				+ (getIdToSeccion() == null ? 0 : this.getIdToSeccion()
						.hashCode());
		result = 37
				* result
				+ (getNumeroSubInciso() == null ? 0 : this.getNumeroSubInciso()
						.hashCode());
		result = 37
				* result
				+ (getIdToCobertura() == null ? 0 : this.getIdToCobertura()
						.hashCode());
		return result;
	}
}