package mx.com.afirme.midas.endoso.cobertura;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.cobertura.subinciso.SubIncisoCoberturaEndosoDTO;
import mx.com.afirme.midas.endoso.riesgo.RiesgoEndosoDTO;
import mx.com.afirme.midas.poliza.cobertura.CoberturaPolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * CoberturaEndosoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCOBERTURAEND", schema = "MIDAS")


public class CoberturaEndosoDTO implements java.io.Serializable, Entidad {

	private static final long serialVersionUID = 1L;
	private CoberturaEndosoId id;
	private EndosoDTO endosoDTO;
	private CoberturaPolizaDTO coberturaPolizaDTO;
	private BigDecimal idTcSubRamo;
	private Double valorSumaAsegurada;
	private Double valorCoaseguro;
	private Double valorDeducible;
	private Double valorCuota;
	private Double valorPrimaNeta;
	private Double valorRecargoPagoFrac;
	private Double valorDerechos;
	private Double valorBonifComision;
	private Double valorBonifComRecPagoFrac;
	private Double valorIVA;
	private Double valorPrimaTotal;
	private Double porcentajeComision;
	private Double valorComision;
	private Double valorComisionFinal;
	private Double valorComisionRecPagoFrac;
	private Double valorComisionFinalRecPagoFrac;
	private Short numeroAgrupacion;

	private Integer agrupadorTarifaId = 0;
	//@Column(name="VERAGRUPADORTARIFA_ID")
	private Integer verAgrupadorTarifaId = 0;
	//@Column(name="VERSIONCARGA_ID")
	private Integer verCargaId = 0;
	//@Column(name="VERSIONTARIFA_ID")
	private Integer verTarifaId = 0;
	//@Column(name="TARIFAEXT_ID")
	private Integer tarifaExtId = 0;
	//@Column(name="VERSIONTARIFAEXT_ID")
	private Integer verTarifaExtId = 0;
	private List<SubIncisoCoberturaEndosoDTO> subIncisoCoberturaEndosoDTOs = new ArrayList<SubIncisoCoberturaEndosoDTO>();
	private List<RiesgoEndosoDTO> riesgoEndosoDTOs = new ArrayList<RiesgoEndosoDTO>();

	//
	private Double valorSobreComisionAgente =  0D;
	private Double valorSobreComisionProm =  0D;
	private Double valorBonoAgente =  0D;
	private Double valorBonoProm =  0D;
	private Double valorCesionDerechosAgente =  0D;
	private Double valorCesionDerechosProm =  0D;
	private Double valorSobreComUDIAgente =  0D;
	private Double valorSobreComUDIProm =  0D;
	
	private Integer diasSalarioMinimo;
	
	// Constructors

	/** default constructor */
	public CoberturaEndosoDTO() {
	}

	/** minimal constructor */
	public CoberturaEndosoDTO(CoberturaEndosoId id, EndosoDTO endosoDTO,
			CoberturaPolizaDTO coberturaPolizaDTO, BigDecimal idTcSubRamo,
			Double valorSumaAsegurada, Double valorCoaseguro,
			Double valorDeducible, Double valorCuota, Double valorPrimaNeta,
			Double valorRecargoPagoFrac, Double valorDerechos,
			Double valorBonifComision, Double valorBonifComRecPagoFrac,
			Double valorIVA, Double valorPrimaTotal, Double porcentajeComision,
			Double valorComision, Double valorComisionFinal,
			Double valorComisionRecPagoFrac,
			Double valorComisionFinalRecPagoFrac, Short numeroAgrupacion) {
		this.id = id;
		this.endosoDTO = endosoDTO;
		this.coberturaPolizaDTO = coberturaPolizaDTO;
		this.idTcSubRamo = idTcSubRamo;
		this.valorSumaAsegurada = valorSumaAsegurada;
		this.valorCoaseguro = valorCoaseguro;
		this.valorDeducible = valorDeducible;
		this.valorCuota = valorCuota;
		this.valorPrimaNeta = valorPrimaNeta;
		this.valorRecargoPagoFrac = valorRecargoPagoFrac;
		this.valorDerechos = valorDerechos;
		this.valorBonifComision = valorBonifComision;
		this.valorBonifComRecPagoFrac = valorBonifComRecPagoFrac;
		this.valorIVA = valorIVA;
		this.valorPrimaTotal = valorPrimaTotal;
		this.porcentajeComision = porcentajeComision;
		this.valorComision = valorComision;
		this.valorComisionFinal = valorComisionFinal;
		this.valorComisionRecPagoFrac = valorComisionRecPagoFrac;
		this.valorComisionFinalRecPagoFrac = valorComisionFinalRecPagoFrac;
		this.numeroAgrupacion = numeroAgrupacion;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroEndoso", column = @Column(name = "NUMEROENDOSO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)) })
	public CoberturaEndosoId getId() {
		return this.id;
	}

	public void setId(CoberturaEndosoId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOPOLIZA", referencedColumnName = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROENDOSO", referencedColumnName = "NUMEROENDOSO", nullable = false, insertable = false, updatable = false) })
	public EndosoDTO getEndosoDTO() {
		return this.endosoDTO;
	}

	public void setEndosoDTO(EndosoDTO endosoDTO) {
		this.endosoDTO = endosoDTO;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOPOLIZA", referencedColumnName = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false) })
	public CoberturaPolizaDTO getCoberturaPolizaDTO() {
		return this.coberturaPolizaDTO;
	}

	public void setCoberturaPolizaDTO(CoberturaPolizaDTO coberturaPolizaDTO) {
		this.coberturaPolizaDTO = coberturaPolizaDTO;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubRamo() {
		return this.idTcSubRamo;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	@Column(name = "VALORSUMAASEGURADA", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Column(name = "VALORCOASEGURO", nullable = false, precision = 16)
	public Double getValorCoaseguro() {
		return this.valorCoaseguro;
	}

	public void setValorCoaseguro(Double valorCoaseguro) {
		this.valorCoaseguro = valorCoaseguro;
	}

	@Column(name = "VALORDEDUCIBLE", nullable = false, precision = 16)
	public Double getValorDeducible() {
		return this.valorDeducible;
	}

	public void setValorDeducible(Double valorDeducible) {
		this.valorDeducible = valorDeducible;
	}

	@Column(name = "VALORCUOTA", nullable = false, precision = 16, scale = 10)
	public Double getValorCuota() {
		return this.valorCuota;
	}

	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}

	@Column(name = "VALORRECARGOPAGOFRAC", nullable = false, precision = 16)
	public Double getValorRecargoPagoFrac() {
		return this.valorRecargoPagoFrac;
	}

	public void setValorRecargoPagoFrac(Double valorRecargoPagoFrac) {
		this.valorRecargoPagoFrac = valorRecargoPagoFrac;
	}

	@Column(name = "VALORDERECHOS", nullable = false, precision = 16)
	public Double getValorDerechos() {
		return this.valorDerechos;
	}

	public void setValorDerechos(Double valorDerechos) {
		this.valorDerechos = valorDerechos;
	}

	@Column(name = "VALORBONIFCOMISION", nullable = false, precision = 16)
	public Double getValorBonifComision() {
		return this.valorBonifComision;
	}

	public void setValorBonifComision(Double valorBonifComision) {
		this.valorBonifComision = valorBonifComision;
	}

	@Column(name = "VALORBONIFCOMRECPAGOFRAC", nullable = false, precision = 16)
	public Double getValorBonifComRecPagoFrac() {
		return this.valorBonifComRecPagoFrac;
	}

	public void setValorBonifComRecPagoFrac(Double valorBonifComRecPagoFrac) {
		this.valorBonifComRecPagoFrac = valorBonifComRecPagoFrac;
	}

	@Column(name = "VALORIVA", nullable = false, precision = 16)
	public Double getValorIVA() {
		return this.valorIVA;
	}

	public void setValorIVA(Double valorIVA) {
		this.valorIVA = valorIVA;
	}

	@Column(name = "VALORPRIMATOTAL", nullable = false, precision = 16)
	public Double getValorPrimaTotal() {
		return this.valorPrimaTotal;
	}

	public void setValorPrimaTotal(Double valorPrimaTotal) {
		this.valorPrimaTotal = valorPrimaTotal;
	}

	@Column(name = "PORCENTAJECOMISION", nullable = false, precision = 8, scale = 4)
	public Double getPorcentajeComision() {
		return this.porcentajeComision;
	}

	public void setPorcentajeComision(Double porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}

	@Column(name = "VALORCOMISION", nullable = false, precision = 16)
	public Double getValorComision() {
		return this.valorComision;
	}

	public void setValorComision(Double valorComision) {
		this.valorComision = valorComision;
	}

	@Column(name = "VALORCOMISIONFINAL", nullable = false, precision = 16)
	public Double getValorComisionFinal() {
		return this.valorComisionFinal;
	}

	public void setValorComisionFinal(Double valorComisionFinal) {
		this.valorComisionFinal = valorComisionFinal;
	}

	@Column(name = "VALORCOMISIONRECPAGOFRAC", nullable = false, precision = 16)
	public Double getValorComisionRecPagoFrac() {
		return this.valorComisionRecPagoFrac;
	}

	public void setValorComisionRecPagoFrac(Double valorComisionRecPagoFrac) {
		this.valorComisionRecPagoFrac = valorComisionRecPagoFrac;
	}

	@Column(name = "VALORCOMFINALRECPAGOFRAC", nullable = false, precision = 16)
	public Double getValorComisionFinalRecPagoFrac() {
		return this.valorComisionFinalRecPagoFrac;
	}

	public void setValorComisionFinalRecPagoFrac(
			Double valorComisionFinalRecPagoFrac) {
		this.valorComisionFinalRecPagoFrac = valorComisionFinalRecPagoFrac;
	}

	@Column(name = "NUMEROAGRUPACION", nullable = false, precision = 4, scale = 0)
	public Short getNumeroAgrupacion() {
		return this.numeroAgrupacion;
	}

	public void setNumeroAgrupacion(Short numeroAgrupacion) {
		this.numeroAgrupacion = numeroAgrupacion;
	}

	@Column(name="IDTOAGRUPADORTARIFA")
	public Integer getAgrupadorTarifaId() {
		return agrupadorTarifaId;
	}

	public void setAgrupadorTarifaId(Integer agrupadorTarifaId) {
		this.agrupadorTarifaId = agrupadorTarifaId;
	}

	@Column(name="IDVERAGRUPADORTARIFA")
	public Integer getVerAgrupadorTarifaId() {
		return verAgrupadorTarifaId;
	}

	public void setVerAgrupadorTarifaId(Integer verAgrupadorTarifaId) {
		this.verAgrupadorTarifaId = verAgrupadorTarifaId;
	}

	@Column(name="IDVERSIONCARGA")
	public Integer getVerCargaId() {
		return verCargaId;
	}

	public void setVerCargaId(Integer verCargaId) {
		this.verCargaId = verCargaId;
	}

	@Column(name="IDVERSIONTARIFA")
	public Integer getVerTarifaId() {
		return verTarifaId;
	}

	public void setVerTarifaId(Integer verTarifaId) {
		this.verTarifaId = verTarifaId;
	}

	@Column(name="IDTARIFAEXT")
	public Integer getTarifaExtId() {
		return tarifaExtId;
	}

	public void setTarifaExtId(Integer tarifaExtId) {
		this.tarifaExtId = tarifaExtId;
	}

	@Column(name="IDVERSIONTARIFAEXT")
	public Integer getVerTarifaExtId() {
		return verTarifaExtId;
	}

	public void setVerTarifaExtId(Integer verTarifaExtId) {
		this.verTarifaExtId = verTarifaExtId;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaEndosoDTO")
	public List<SubIncisoCoberturaEndosoDTO> getSubIncisoCoberturaEndosoDTOs() {
		return this.subIncisoCoberturaEndosoDTOs;
	}

	public void setSubIncisoCoberturaEndosoDTOs(
			List<SubIncisoCoberturaEndosoDTO> subIncisoCoberturaEndosoDTOs) {
		this.subIncisoCoberturaEndosoDTOs = subIncisoCoberturaEndosoDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaEndosoDTO")
	public List<RiesgoEndosoDTO> getRiesgoEndosoDTOs() {
		return this.riesgoEndosoDTOs;
	}

	public void setRiesgoEndosoDTOs(List<RiesgoEndosoDTO> riesgoEndosoDTOs) {
		this.riesgoEndosoDTOs = riesgoEndosoDTOs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CoberturaEndosoId getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CoberturaEndosoId getBusinessKey() {
		return this.id;
	}

	
	public void setValorSobreComisionAgente(Double valorSobreComisionAgente) {
		this.valorSobreComisionAgente = valorSobreComisionAgente;
	}

	@Column(name="VALORSOBRECOMISIONAGENTE")
	public Double getValorSobreComisionAgente() {
		return valorSobreComisionAgente;
	}

	public void setValorSobreComisionProm(Double valorSobreComisionProm) {
		this.valorSobreComisionProm = valorSobreComisionProm;
	}

	@Column(name="VALORSOBRECOMISIONPROM")
	public Double getValorSobreComisionProm() {
		return valorSobreComisionProm;
	}

	public void setValorBonoAgente(Double valorBonoAgente) {
		this.valorBonoAgente = valorBonoAgente;
	}

	@Column(name="VALORBONOAGENTE")
	public Double getValorBonoAgente() {
		return valorBonoAgente;
	}

	public void setValorBonoProm(Double valorBonoProm) {
		this.valorBonoProm = valorBonoProm;
	}

	@Column(name="VALORBONOPROM")
	public Double getValorBonoProm() {
		return valorBonoProm;
	}

	public void setValorCesionDerechosAgente(Double valorCesionDerechosAgente) {
		this.valorCesionDerechosAgente = valorCesionDerechosAgente;
	}

	@Column(name="VALORCESIONDERECHOSAGENTE")
	public Double getValorCesionDerechosAgente() {
		return valorCesionDerechosAgente;
	}

	public void setValorCesionDerechosProm(Double valorCesionDerechosProm) {
		this.valorCesionDerechosProm = valorCesionDerechosProm;
	}

	@Column(name="VALORCESIONDERECHOSPROM")
	public Double getValorCesionDerechosProm() {
		return valorCesionDerechosProm;
	}

	public void setValorSobreComUDIAgente(Double valorSobreComUDIAgente) {
		this.valorSobreComUDIAgente = valorSobreComUDIAgente;
	}

	@Column(name="VALORSOBRECOMUDIAGENTE")
	public Double getValorSobreComUDIAgente() {
		return valorSobreComUDIAgente;
	}

	public void setValorSobreComUDIProm(Double valorSobreComUDIProm) {
		this.valorSobreComUDIProm = valorSobreComUDIProm;
	}

	@Column(name="VALORSOBRECOMUDIPROM")
	public Double getValorSobreComUDIProm() {
		return valorSobreComUDIProm;
	}

	public void setDiasSalarioMinimo(Integer diasSalarioMinimo) {
		this.diasSalarioMinimo = diasSalarioMinimo;
	}

	@Column(name="DIASSALARIOMINIMO")
	public Integer getDiasSalarioMinimo() {
		return diasSalarioMinimo;
	}
}