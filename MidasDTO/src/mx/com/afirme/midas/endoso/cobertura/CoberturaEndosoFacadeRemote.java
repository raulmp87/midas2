package mx.com.afirme.midas.endoso.cobertura;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for CoberturaEndosoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface CoberturaEndosoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved CoberturaEndosoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CoberturaEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CoberturaEndosoDTO entity);

	/**
	 * Delete a persistent CoberturaEndosoDTO entity.
	 * 
	 * @param entity
	 *            CoberturaEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoberturaEndosoDTO entity);

	/**
	 * Persist a previously saved CoberturaEndosoDTO entity and return it or a
	 * copy of it to the sender. A copy of the CoberturaEndosoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CoberturaEndosoDTO entity to update
	 * @return CoberturaEndosoDTO the persisted CoberturaEndosoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoberturaEndosoDTO update(CoberturaEndosoDTO entity);

	public CoberturaEndosoDTO findById(CoberturaEndosoId id);

	/**
	 * Find all CoberturaEndosoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoberturaEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoberturaEndosoDTO> found by query
	 */
	public List<CoberturaEndosoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all CoberturaEndosoDTO entities.
	 * 
	 * @return List<CoberturaEndosoDTO> all CoberturaEndosoDTO entities
	 */
	public List<CoberturaEndosoDTO> findAll();
	
	public void insertCoberturaEndosoDePoliza(BigDecimal idToCotizacion, BigDecimal idToPoliza, int numeroEndoso,short tipoEndoso, 
			BigDecimal idToCotizacionAnterior);

	/**
	 * METODO QUE OBTIENE TODAS LAS COBERTURAS ENDOSO QUE APLIQUEN PRIMER RIESGO Y LUC DE UN IDTOPOLIZA DADO Y UN NUMERO DE ENDOSO
	 * @param idToPoliza
	 * @param numEndoso
	 * @return
	 */
	public List<CoberturaEndosoDTO> obtenerCoberturasPrimerRiesgoLUC(BigDecimal idToPoliza, Short numEndoso);	
	
	public List<CoberturaEndosoDTO> listarFiltrado(CoberturaEndosoId id);
	
	public int obtenerMaxNumeroCoberturaEnd(BigDecimal idToPoliza, BigDecimal numeroInciso);
}