package mx.com.afirme.midas.endoso;

import java.io.Serializable;
import java.math.BigDecimal;

public class DiferenciaCotizacionEndosoDTO implements Serializable {

	private static final long serialVersionUID = 2851262996677819541L;

	private BigDecimal idToSeccion;
	private BigDecimal idToCobertura;
	private Double originalSumaAsegurada;
	private Double originalCuota;
	private Double originalPrimaNeta;
	private Double nuevaSumaAsegurada;
	private Double nuevaCuota;
	private Double nuevaPrimaNeta;
	private Double diferenciaSumaAsegurada;
	private Double diferenciaCuota;
	private Double diferenciaPrimaNeta;
	private String tituloPrimeraColumna;
	private String padding;
	private String rowStyle;
	private String tieneCuotasDiferentes;

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public Double getOriginalSumaAsegurada() {
		return originalSumaAsegurada;
	}

	public void setOriginalSumaAsegurada(Double originalSumaAsegurada) {
		this.originalSumaAsegurada = originalSumaAsegurada;
	}

	public Double getOriginalCuota() {
		return originalCuota;
	}

	public void setOriginalCuota(Double originalCuota) {
		this.originalCuota = originalCuota;
	}

	public Double getOriginalPrimaNeta() {
		return originalPrimaNeta;
	}

	public void setOriginalPrimaNeta(Double originalPrimaNeta) {
		this.originalPrimaNeta = originalPrimaNeta;
	}

	public Double getNuevaSumaAsegurada() {
		return nuevaSumaAsegurada;
	}

	public void setNuevaSumaAsegurada(Double nuevaSumaAsegurada) {
		this.nuevaSumaAsegurada = nuevaSumaAsegurada;
	}

	public Double getNuevaCuota() {
		return nuevaCuota;
	}

	public void setNuevaCuota(Double nuevaCuota) {
		this.nuevaCuota = nuevaCuota;
	}

	public Double getNuevaPrimaNeta() {
		return nuevaPrimaNeta;
	}

	public void setNuevaPrimaNeta(Double nuevaPrimaNeta) {
		this.nuevaPrimaNeta = nuevaPrimaNeta;
	}

	public Double getDiferenciaSumaAsegurada() {
		return diferenciaSumaAsegurada;
	}

	public void setDiferenciaSumaAsegurada(Double diferenciaSumaAsegurada) {
		this.diferenciaSumaAsegurada = diferenciaSumaAsegurada;
	}

	public Double getDiferenciaCuota() {
		return diferenciaCuota;
	}

	public void setDiferenciaCuota(Double diferenciaCuota) {
		this.diferenciaCuota = diferenciaCuota;
	}

	public Double getDiferenciaPrimaNeta() {
		return diferenciaPrimaNeta;
	}

	public void setDiferenciaPrimaNeta(Double diferenciaPrimaNeta) {
		this.diferenciaPrimaNeta = diferenciaPrimaNeta;
	}

	public void setTituloPrimeraColumna(String tituloPrimeraColumna) {
		this.tituloPrimeraColumna = tituloPrimeraColumna;
	}

	public String getTituloPrimeraColumna() {
		return tituloPrimeraColumna;
	}

	public void setPadding(String padding) {
		this.padding = padding;
	}

	public String getPadding() {
		return padding;
	}

	public void setRowStyle(String rowStyle) {
		this.rowStyle = rowStyle;
	}

	public String getRowStyle() {
		return rowStyle;
	}

	public void setTieneCuotasDiferentes(String tieneCuotasDiferentes) {
		this.tieneCuotasDiferentes = tieneCuotasDiferentes;
	}

	public String getTieneCuotasDiferentes() {
		return tieneCuotasDiferentes;
	}
}
