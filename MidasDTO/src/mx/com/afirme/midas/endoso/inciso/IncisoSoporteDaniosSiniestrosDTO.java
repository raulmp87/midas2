package mx.com.afirme.midas.endoso.inciso;

import java.math.BigDecimal;
import java.util.Date;

public class IncisoSoporteDaniosSiniestrosDTO {
    
    private BigDecimal numeroInciso;
    private String direccionInciso;
    private Date fechaInicioVigencia;
    private Date fechaFinVigencia;
        
    
    public void setNumeroInciso(BigDecimal numeroInciso) {
	this.numeroInciso = numeroInciso;
    }
    public BigDecimal getNumeroInciso() {
	return numeroInciso;
    }
    public void setDireccionInciso(String direccionInciso) {
	this.direccionInciso = direccionInciso;
    }
    public String getDireccionInciso() {
	return direccionInciso;
    }
    public void setFechaInicioVigencia(Date fechaInicioVigencia) {
	this.fechaInicioVigencia = fechaInicioVigencia;
    }
    public Date getFechaInicioVigencia() {
	return fechaInicioVigencia;
    }
    public void setFechaFinVigencia(Date fechaFinVigencia) {
	this.fechaFinVigencia = fechaFinVigencia;
    }
    public Date getFechaFinVigencia() {
	return fechaFinVigencia;
    }
	
	
    

}
