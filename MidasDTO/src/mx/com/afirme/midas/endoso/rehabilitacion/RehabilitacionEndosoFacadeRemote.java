package mx.com.afirme.midas.endoso.rehabilitacion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.endoso.EndosoDTO;


public interface RehabilitacionEndosoFacadeRemote {

	public List<EndosoDTO> listarEndososRehabilitables (BigDecimal idToPoliza);
	
}
