package mx.com.afirme.midas.endoso.rehabilitacion;

import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for EndosoRehabilitableFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface EndosoRehabilitableFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved EndosoRehabilitableDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            EndosoRehabilitableDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(EndosoRehabilitableDTO entity);

	/**
	 * Delete a persistent EndosoRehabilitableDTO entity.
	 * 
	 * @param entity
	 *            EndosoRehabilitableDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(EndosoRehabilitableDTO entity);

	/**
	 * Persist a previously saved EndosoRehabilitableDTO entity and return it or
	 * a copy of it to the sender. A copy of the EndosoRehabilitableDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            EndosoRehabilitableDTO entity to update
	 * @return EndosoRehabilitableDTO the persisted EndosoRehabilitableDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public EndosoRehabilitableDTO update(EndosoRehabilitableDTO entity);

	public EndosoRehabilitableDTO findById(EndosoRehabilitableId id);

	/**
	 * Find all EndosoRehabilitableDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the EndosoRehabilitableDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<EndosoRehabilitableDTO> found by query
	 */
	public List<EndosoRehabilitableDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all EndosoRehabilitableDTO entities.
	 * 
	 * @return List<EndosoRehabilitableDTO> all EndosoRehabilitableDTO entities
	 */
	public List<EndosoRehabilitableDTO> findAll();

	public boolean notificaRehabilitacion(
			EndosoRehabilitableDTO endosoRehabilitableDTO);

}