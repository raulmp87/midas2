package mx.com.afirme.midas.endoso.rehabilitacion;

import java.io.Serializable;

public class MensajeRespuestaDTO implements Serializable {

	private static final long serialVersionUID = -5465785162194707740L;
	
	private String codigo;
	private String mensaje;
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
