package mx.com.afirme.midas.endoso.rehabilitacion;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * EndosoRehabilitableDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TTENDOSOREHABILITABLE", schema = "MIDAS")
public class EndosoRehabilitableDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 4163505556098048505L;
	private EndosoRehabilitableId id;
	private Short claveTipoEndoso;
	private Date fechaInicioVigencia;
	private Short estatusRegistro;
	private Double valorPrimaNeta;
	private Double valorRecargoPagoFrac;
	private Double valorDerechos;
	private Double valorBonifComision;
	private Double valorIVA;
	private Double valorPrimaTotal;
	private Double valorBonifComisionRPF;
	private Double valorComision;
	private Double valorComisionRPF;

	public static final short ESTATUS_REGISTRO_SIN_PROCESAR = 0;
	public static final short ESTATUS_REGISTRO_EN_PROCESO = 1;
	public static final short ESTATUS_REGISTRO_PROCESADO = 2;
	public static final short ESTATUS_EMISION_REGISTRO_FALLO = 3;

	// Constructors

	/** default constructor */
	public EndosoRehabilitableDTO() {
	}

	/** full constructor */
	public EndosoRehabilitableDTO(EndosoRehabilitableId id,
			Short claveTipoEndoso, Timestamp fechaInicioVigencia,
			Short estatusRegistro) {
		this.id = id;
		this.claveTipoEndoso = claveTipoEndoso;
		this.fechaInicioVigencia = fechaInicioVigencia;
		this.estatusRegistro = estatusRegistro;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroEndoso", column = @Column(name = "NUMEROENDOSO", nullable = false, precision = 22, scale = 0)) })
	public EndosoRehabilitableId getId() {
		return this.id;
	}

	public void setId(EndosoRehabilitableId id) {
		this.id = id;
	}

	@Column(name = "CLAVETIPOENDOSO", nullable = false, precision = 1, scale = 0)
	public Short getClaveTipoEndoso() {
		return this.claveTipoEndoso;
	}

	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAINICIOVIGENCIA", nullable = false, length = 11)
	public Date getFechaInicioVigencia() {
		return this.fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	@Column(name = "ESTATUSREGISTRO", nullable = false, precision = 1, scale = 0)
	public Short getEstatusRegistro() {
		return this.estatusRegistro;
	}

	public void setEstatusRegistro(Short estatusRegistro) {
		this.estatusRegistro = estatusRegistro;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}

	@Column(name = "VALORRECARGOPAGOFRAC", nullable = false, precision = 16)
	public Double getValorRecargoPagoFrac() {
		return this.valorRecargoPagoFrac;
	}

	public void setValorRecargoPagoFrac(Double valorRecargoPagoFrac) {
		this.valorRecargoPagoFrac = valorRecargoPagoFrac;
	}

	@Column(name = "VALORDERECHOS", nullable = false, precision = 16)
	public Double getValorDerechos() {
		return this.valorDerechos;
	}

	public void setValorDerechos(Double valorDerechos) {
		this.valorDerechos = valorDerechos;
	}

	@Column(name = "VALORBONIFCOMISION", nullable = false, precision = 16)
	public Double getValorBonifComision() {
		return this.valorBonifComision;
	}

	public void setValorBonifComision(Double valorBonifComision) {
		this.valorBonifComision = valorBonifComision;
	}

	@Column(name = "VALORIVA", nullable = false, precision = 16)
	public Double getValorIVA() {
		return this.valorIVA;
	}

	public void setValorIVA(Double valorIVA) {
		this.valorIVA = valorIVA;
	}

	@Column(name = "VALORPRIMATOTAL", nullable = false, precision = 16)
	public Double getValorPrimaTotal() {
		return this.valorPrimaTotal;
	}

	public void setValorPrimaTotal(Double valorPrimaTotal) {
		this.valorPrimaTotal = valorPrimaTotal;
	}

	@Column(name = "VALORBONIFCOMRECPAGOFRAC")
	public Double getValorBonifComisionRPF() {
		return valorBonifComisionRPF;
	}

	public void setValorBonifComisionRPF(Double valorBonifComisionRPF) {
		this.valorBonifComisionRPF = valorBonifComisionRPF;
	}

	@Column(name = "VALORCOMISION")
	public Double getValorComision() {
		return valorComision;
	}

	public void setValorComision(Double valorComision) {
		this.valorComision = valorComision;
	}

	@Column(name = "VALORCOMISIONRECPAGOFRAC")
	public Double getValorComisionRPF() {
		return valorComisionRPF;
	}

	public void setValorComisionRPF(Double valorComisionRPF) {
		this.valorComisionRPF = valorComisionRPF;
	}

}