package mx.com.afirme.midas.endoso.riesgo.subinciso;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SubIncisoRiesgoEndosoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SubIncisoRiesgoEndosoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SubIncisoRiesgoEndosoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SubIncisoRiesgoEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubIncisoRiesgoEndosoDTO entity);

	/**
	 * Delete a persistent SubIncisoRiesgoEndosoDTO entity.
	 * 
	 * @param entity
	 *            SubIncisoRiesgoEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubIncisoRiesgoEndosoDTO entity);

	/**
	 * Persist a previously saved SubIncisoRiesgoEndosoDTO entity and return it
	 * or a copy of it to the sender. A copy of the SubIncisoRiesgoEndosoDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SubIncisoRiesgoEndosoDTO entity to update
	 * @return SubIncisoRiesgoEndosoDTO the persisted SubIncisoRiesgoEndosoDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubIncisoRiesgoEndosoDTO update(SubIncisoRiesgoEndosoDTO entity);

	public SubIncisoRiesgoEndosoDTO findById(SubIncisoRiesgoEndosoId id);

	/**
	 * Find all SubIncisoRiesgoEndosoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SubIncisoRiesgoEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubIncisoRiesgoEndosoDTO> found by query
	 */
	public List<SubIncisoRiesgoEndosoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SubIncisoRiesgoEndosoDTO entities.
	 * 
	 * @return List<SubIncisoRiesgoEndosoDTO> all SubIncisoRiesgoEndosoDTO
	 *         entities
	 */
	public List<SubIncisoRiesgoEndosoDTO> findAll();
	
	public void insertSubIncisoRiesgoEndosoDeCotizacion(BigDecimal idToCotizacion, BigDecimal idToPoliza, int numeroEndoso, double diasPorDevengar,short tipoEndoso);
	/**
	 * 
	 * @param subIncisoRiesgoEndosoDTO
	 * @return
	 */
	public List<SubIncisoRiesgoEndosoDTO> listarFiltrado(SubIncisoRiesgoEndosoDTO subIncisoRiesgoEndosoDTO);	
}