package mx.com.afirme.midas.endoso.riesgo;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for RiesgoEndosoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface RiesgoEndosoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved RiesgoEndosoDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            RiesgoEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RiesgoEndosoDTO entity);

	/**
	 * Delete a persistent RiesgoEndosoDTO entity.
	 * 
	 * @param entity
	 *            RiesgoEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoEndosoDTO entity);

	/**
	 * Persist a previously saved RiesgoEndosoDTO entity and return it or a copy
	 * of it to the sender. A copy of the RiesgoEndosoDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            RiesgoEndosoDTO entity to update
	 * @return RiesgoEndosoDTO the persisted RiesgoEndosoDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoEndosoDTO update(RiesgoEndosoDTO entity);

	public RiesgoEndosoDTO findById(RiesgoEndosoId id);

	/**
	 * Find all RiesgoEndosoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoEndosoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoEndosoDTO> found by query
	 */
	public List<RiesgoEndosoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all RiesgoEndosoDTO entities.
	 * 
	 * @return List<RiesgoEndosoDTO> all RiesgoEndosoDTO entities
	 */
	public List<RiesgoEndosoDTO> findAll();

	public void insertRiesgoEndosoDePoliza(BigDecimal idToCotizacion, BigDecimal idToPoliza, int numeroEndoso,short tipoEndoso,
			BigDecimal idToCotizacionAnterior);

	public List<RiesgoEndosoDTO> listarFiltrado(RiesgoEndosoId id);
}