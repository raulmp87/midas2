package mx.com.afirme.midas.endoso.riesgo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoDTO;
import mx.com.afirme.midas.endoso.riesgo.subinciso.SubIncisoRiesgoEndosoDTO;
import mx.com.afirme.midas.poliza.riesgo.RiesgoPolizaDTO;

/**
 * RiesgoEndosoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TORIESGOEND", schema = "MIDAS")
public class RiesgoEndosoDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private RiesgoEndosoId id;
	private RiesgoPolizaDTO riesgoPolizaDTO;
	private CoberturaEndosoDTO coberturaEndosoDTO;
	private BigDecimal idTcSubRamo;
	private Double valorSumaAsegurada;
	private Double valorCoaseguro;
	private Double valorDeducible;
	private Double valorCuota;
	private Double valorPrimaNeta;
	private Double valorRecargoPagoFrac;
	private Double valorDerechos;
	private Double valorBonifComision;
	private Double valorBonifComRecPagoFrac;
	private Double valorIVA;
	private Double valorPrimaTotal;
	private Double porcentajeComision;
	private Double valorComision;
	private Double valorComisionFinal;
	private Double valorComisionRecPagoFrac;
	private Double valorComFinalRecPago;
	private List<SubIncisoRiesgoEndosoDTO> subIncisoRiesgoEndosoDTOs = new ArrayList<SubIncisoRiesgoEndosoDTO>();

	// Constructors

	/** default constructor */
	public RiesgoEndosoDTO() {
	}

	/** minimal constructor */
	public RiesgoEndosoDTO(RiesgoEndosoId id,
			RiesgoPolizaDTO riesgoPolizaDTO,
			CoberturaEndosoDTO coberturaEndosoDTO, BigDecimal idTcSubRamo,
			Double valorSumaAsegurada, Double valorCoaseguro,
			Double valorDeducible, Double valorCuota, Double valorPrimaNeta,
			Double valorRecargoPagoFrac, Double valorDerechos,
			Double valorBonifComision, Double valorBonifComRecPagoFrac,
			Double valorIVA, Double valorPrimaTotal, Double porcentajeComision,
			Double valorComision, Double valorComisionFinal,
			Double valorComisionRecPagoFrac, Double valorComFinalRecPago) {
		this.id = id;
		this.riesgoPolizaDTO = riesgoPolizaDTO;
		this.coberturaEndosoDTO = coberturaEndosoDTO;
		this.idTcSubRamo = idTcSubRamo;
		this.valorSumaAsegurada = valorSumaAsegurada;
		this.valorCoaseguro = valorCoaseguro;
		this.valorDeducible = valorDeducible;
		this.valorCuota = valorCuota;
		this.valorPrimaNeta = valorPrimaNeta;
		this.valorRecargoPagoFrac = valorRecargoPagoFrac;
		this.valorDerechos = valorDerechos;
		this.valorBonifComision = valorBonifComision;
		this.valorBonifComRecPagoFrac = valorBonifComRecPagoFrac;
		this.valorIVA = valorIVA;
		this.valorPrimaTotal = valorPrimaTotal;
		this.porcentajeComision = porcentajeComision;
		this.valorComision = valorComision;
		this.valorComisionFinal = valorComisionFinal;
		this.valorComisionRecPagoFrac = valorComisionRecPagoFrac;
		this.valorComFinalRecPago = valorComFinalRecPago;
	}

	/** full constructor */
	public RiesgoEndosoDTO(RiesgoEndosoId id,
			RiesgoPolizaDTO riesgoPolizaDTO,
			CoberturaEndosoDTO coberturaEndosoDTO, BigDecimal idTcSubRamo,
			Double valorSumaAsegurada, Double valorCoaseguro,
			Double valorDeducible, Double valorCuota, Double valorPrimaNeta,
			Double valorRecargoPagoFrac, Double valorDerechos,
			Double valorBonifComision, Double valorBonifComRecPagoFrac,
			Double valorIVA, Double valorPrimaTotal, Double porcentajeComision,
			Double valorComision, Double valorComisionFinal,
			Double valorComisionRecPagoFrac, Double valorComFinalRecPago,
			List<SubIncisoRiesgoEndosoDTO> subIncisoRiesgoEndosoDTOs) {
		this.id = id;
		this.riesgoPolizaDTO = riesgoPolizaDTO;
		this.coberturaEndosoDTO = coberturaEndosoDTO;
		this.idTcSubRamo = idTcSubRamo;
		this.valorSumaAsegurada = valorSumaAsegurada;
		this.valorCoaseguro = valorCoaseguro;
		this.valorDeducible = valorDeducible;
		this.valorCuota = valorCuota;
		this.valorPrimaNeta = valorPrimaNeta;
		this.valorRecargoPagoFrac = valorRecargoPagoFrac;
		this.valorDerechos = valorDerechos;
		this.valorBonifComision = valorBonifComision;
		this.valorBonifComRecPagoFrac = valorBonifComRecPagoFrac;
		this.valorIVA = valorIVA;
		this.valorPrimaTotal = valorPrimaTotal;
		this.porcentajeComision = porcentajeComision;
		this.valorComision = valorComision;
		this.valorComisionFinal = valorComisionFinal;
		this.valorComisionRecPagoFrac = valorComisionRecPagoFrac;
		this.valorComFinalRecPago = valorComFinalRecPago;
		this.subIncisoRiesgoEndosoDTOs = subIncisoRiesgoEndosoDTOs;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroEndoso", column = @Column(name = "NUMEROENDOSO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)) })
	public RiesgoEndosoId getId() {
		return this.id;
	}

	public void setId(RiesgoEndosoId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOPOLIZA", referencedColumnName = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTORIESGO", referencedColumnName = "IDTORIESGO", nullable = false, insertable = false, updatable = false) })
	public RiesgoPolizaDTO getRiesgoPolizaDTO() {
		return this.riesgoPolizaDTO;
	}

	public void setRiesgoPolizaDTO(RiesgoPolizaDTO riesgoPolizaDTO) {
		this.riesgoPolizaDTO = riesgoPolizaDTO;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOPOLIZA", referencedColumnName = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROENDOSO", referencedColumnName = "NUMEROENDOSO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false) })
	public CoberturaEndosoDTO getCoberturaEndosoDTO() {
		return this.coberturaEndosoDTO;
	}

	public void setCoberturaEndosoDTO(CoberturaEndosoDTO coberturaEndosoDTO) {
		this.coberturaEndosoDTO = coberturaEndosoDTO;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubRamo() {
		return this.idTcSubRamo;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	@Column(name = "VALORSUMAASEGURADA", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Column(name = "VALORCOASEGURO", nullable = false, precision = 16)
	public Double getValorCoaseguro() {
		return this.valorCoaseguro;
	}

	public void setValorCoaseguro(Double valorCoaseguro) {
		this.valorCoaseguro = valorCoaseguro;
	}

	@Column(name = "VALORDEDUCIBLE", nullable = false, precision = 16)
	public Double getValorDeducible() {
		return this.valorDeducible;
	}

	public void setValorDeducible(Double valorDeducible) {
		this.valorDeducible = valorDeducible;
	}

	@Column(name = "VALORCUOTA", nullable = false, precision = 16, scale = 10)
	public Double getValorCuota() {
		return this.valorCuota;
	}

	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}

	@Column(name = "VALORRECARGOPAGOFRAC", nullable = false, precision = 16)
	public Double getValorRecargoPagoFrac() {
		return this.valorRecargoPagoFrac;
	}

	public void setValorRecargoPagoFrac(Double valorRecargoPagoFrac) {
		this.valorRecargoPagoFrac = valorRecargoPagoFrac;
	}

	@Column(name = "VALORDERECHOS", nullable = false, precision = 16)
	public Double getValorDerechos() {
		return this.valorDerechos;
	}

	public void setValorDerechos(Double valorDerechos) {
		this.valorDerechos = valorDerechos;
	}

	@Column(name = "VALORBONIFCOMISION", nullable = false, precision = 16)
	public Double getValorBonifComision() {
		return this.valorBonifComision;
	}

	public void setValorBonifComision(Double valorBonifComision) {
		this.valorBonifComision = valorBonifComision;
	}

	@Column(name = "VALORBONIFCOMRECPAGOFRAC", nullable = false, precision = 16)
	public Double getValorBonifComRecPagoFrac() {
		return this.valorBonifComRecPagoFrac;
	}

	public void setValorBonifComRecPagoFrac(Double valorBonifComRecPagoFrac) {
		this.valorBonifComRecPagoFrac = valorBonifComRecPagoFrac;
	}

	@Column(name = "VALORIVA", nullable = false, precision = 16)
	public Double getValorIVA() {
		return this.valorIVA;
	}

	public void setValorIVA(Double valorIVA) {
		this.valorIVA = valorIVA;
	}

	@Column(name = "VALORPRIMATOTAL", nullable = false, precision = 16)
	public Double getValorPrimaTotal() {
		return this.valorPrimaTotal;
	}

	public void setValorPrimaTotal(Double valorPrimaTotal) {
		this.valorPrimaTotal = valorPrimaTotal;
	}

	@Column(name = "PORCENTAJECOMISION", nullable = false, precision = 8, scale = 4)
	public Double getPorcentajeComision() {
		return this.porcentajeComision;
	}

	public void setPorcentajeComision(Double porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}

	@Column(name = "VALORCOMISION", nullable = false, precision = 16)
	public Double getValorComision() {
		return this.valorComision;
	}

	public void setValorComision(Double valorComision) {
		this.valorComision = valorComision;
	}

	@Column(name = "VALORCOMISIONFINAL", nullable = false, precision = 16)
	public Double getValorComisionFinal() {
		return this.valorComisionFinal;
	}

	public void setValorComisionFinal(Double valorComisionFinal) {
		this.valorComisionFinal = valorComisionFinal;
	}

	@Column(name = "VALORCOMISIONRECPAGOFRAC", nullable = false, precision = 16)
	public Double getValorComisionRecPagoFrac() {
		return this.valorComisionRecPagoFrac;
	}

	public void setValorComisionRecPagoFrac(Double valorComisionRecPagoFrac) {
		this.valorComisionRecPagoFrac = valorComisionRecPagoFrac;
	}

	@Column(name = "VALORCOMFINALRECPAGOFRAC", nullable = false, precision = 16)
	public Double getValorComFinalRecPago() {
		return this.valorComFinalRecPago;
	}

	public void setValorComFinalRecPago(Double valorComFinalRecPago) {
		this.valorComFinalRecPago = valorComFinalRecPago;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "riesgoEndosoDTO")
	public List<SubIncisoRiesgoEndosoDTO> getSubIncisoRiesgoEndosoDTOs() {
		return this.subIncisoRiesgoEndosoDTOs;
	}

	public void setSubIncisoRiesgoEndosoDTOs(List<SubIncisoRiesgoEndosoDTO> subIncisoRiesgoEndosoDTOs) {
		this.subIncisoRiesgoEndosoDTOs = subIncisoRiesgoEndosoDTOs;
	}
}