package mx.com.afirme.midas.endoso.cancelacion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;


public interface CancelacionEndosoFacadeRemote {

	public List<EndosoDTO> listarEndososCancelables(BigDecimal idToPoliza);

	public List<EndosoDTO> listarEndososCancelablesPoliza(BigDecimal idToPoliza);

	public List<EndosoDTO> listarPosiblesEndososCancelables(
			BigDecimal idToPoliza, Short numeroEndoso);

	public CotizacionDTO copiaCotizacion(EndosoDTO ultimoEndoso, short motivo,
			String usuarioCreacion);

	public EndosoDTO creaEndosoCE(CotizacionDTO cotizacionEndosoCE,
			EndosoDTO endosoACancelar, EndosoDTO ultimoEndoso,
			Double ivaCotizacion, boolean existeReciboPagado,
			List<ReciboDTO> recibos, EndosoIDTO endosoIDTO,
			boolean esRehabilitacion, Short grupo);

	public EndosoDTO obtieneEndosoAnterior(EndosoId id);

	public void copiaIncisoConRelaciones(CotizacionDTO cotizacionOrigen,
			CotizacionDTO cotizacionDestino, BigDecimal numeroInciso);

}
