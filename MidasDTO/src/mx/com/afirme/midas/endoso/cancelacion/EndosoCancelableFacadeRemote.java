package mx.com.afirme.midas.endoso.cancelacion;


import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for EndosoCancelableFacade.
 * @author MyEclipse Persistence Tools
 */


public interface EndosoCancelableFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved EndosoCancelableDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EndosoCancelableDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EndosoCancelableDTO entity);
    /**
	 Delete a persistent EndosoCancelableDTO entity.
	  @param entity EndosoCancelableDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EndosoCancelableDTO entity);
   /**
	 Persist a previously saved EndosoCancelableDTO entity and return it or a copy of it to the sender. 
	 A copy of the EndosoCancelableDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EndosoCancelableDTO entity to update
	 @return EndosoCancelableDTO the persisted EndosoCancelableDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public EndosoCancelableDTO update(EndosoCancelableDTO entity);
	public EndosoCancelableDTO findById( EndosoCancelableId id);
	 /**
	 * Find all EndosoCancelableDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EndosoCancelableDTO property to query
	  @param value the property value to match
	  	  @return List<EndosoCancelableDTO> found by query
	 */
	public List<EndosoCancelableDTO> findByProperty(String propertyName, Object value
		);
	public List<EndosoCancelableDTO> findByEstatusRegistro(Object estatusRegistro
		);
	public List<EndosoCancelableDTO> findByValorPrimaNeta(Object valorPrimaNeta
		);
	public List<EndosoCancelableDTO> findByValorBonificacionComision(Object valorBonificacionComision
		);
	public List<EndosoCancelableDTO> findByValorBonifComRecargoPagoFraccionado(Object valorBonifComRecargoPagoFraccionado
		);
	public List<EndosoCancelableDTO> findByValorDerechos(Object valorDerechos
		);
	public List<EndosoCancelableDTO> findByValorRecargoPagoFraccionado(Object valorRecargoPagoFraccionado
		);
	public List<EndosoCancelableDTO> findByValorIva(Object valorIva
		);
	public List<EndosoCancelableDTO> findByValorPrimaTotal(Object valorPrimaTotal
		);
	public List<EndosoCancelableDTO> findByValorComision(Object valorComision
		);
	public List<EndosoCancelableDTO> findByValorComisionRecargoPagoFraccionado(Object valorComisionRecargoPagoFraccionado
		);
	/**
	 * Find all EndosoCancelableDTO entities.
	  	  @return List<EndosoCancelableDTO> all EndosoCancelableDTO entities
	 */
	public List<EndosoCancelableDTO> findAll(
		);	
	
	public boolean notificaCancelacion(
			EndosoCancelableDTO endoso);
}