package mx.com.afirme.midas.endoso.cancelacion;


import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * EndosoCancelableDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TTENDOSOCANCELACION"
    ,schema="MIDAS"
)

public class EndosoCancelableDTO  implements java.io.Serializable {

	//El orden de los atributos de las enumeración no pueden ser alterados. en caso de querer agregar mas atributos a 
	// enum se agregan al final del último atributo.
	public enum Modulo{ Autos,Vida,Daños
		
	};

    // Fields    

     private static final long serialVersionUID = -5507849709025255699L;
     private EndosoCancelableId id;
     private Short estatusRegistro;
     private Date fechaInicioVigencia;
     private Double valorPrimaNeta;
     private Double valorBonificacionComision;
     private Double valorBonifComRecargoPagoFraccionado;
     private Double valorDerechos;
     private Double valorRecargoPagoFraccionado;
     private Double valorIva;
     private Double valorPrimaTotal;
     private Double valorComision;
     private Double valorComisionRecargoPagoFraccionado;

     private Modulo modulo;


     public static final short ESTATUS_REGISTRO_SIN_PROCESAR = 0;
     public static final short ESTATUS_REGISTRO_EN_PROCESO = 1;
     public static final short ESTATUS_REGISTRO_PROCESADO = 2;
     public static final short ESTATUS_EMISION_REGISTRO_FALLO = 3;

    // Constructors

    /** default constructor */
    public EndosoCancelableDTO() {
    }

	/** minimal constructor */
    public EndosoCancelableDTO(EndosoCancelableId id, Short estatusRegistro, Date fechaInicioVigencia) {
        this.id = id;
        this.estatusRegistro = estatusRegistro;
        this.fechaInicioVigencia = fechaInicioVigencia;
    }
    
    /** full constructor */
    public EndosoCancelableDTO(EndosoCancelableId id, Short estatusRegistro, Date fechaInicioVigencia, Double valorPrimaNeta, Double valorBonificacionComision, Double valorBonifComRecargoPagoFraccionado, Double valorDerechos, Double valorRecargoPagoFraccionado, Double valorIva, Double valorPrimaTotal, Double valorComision, Double valorComisionRecargoPagoFraccionado) {
        this.id = id;
        this.estatusRegistro = estatusRegistro;
        this.fechaInicioVigencia = fechaInicioVigencia;
        this.valorPrimaNeta = valorPrimaNeta;
        this.valorBonificacionComision = valorBonificacionComision;
        this.valorBonifComRecargoPagoFraccionado = valorBonifComRecargoPagoFraccionado;
        this.valorDerechos = valorDerechos;
        this.valorRecargoPagoFraccionado = valorRecargoPagoFraccionado;
        this.valorIva = valorIva;
        this.valorPrimaTotal = valorPrimaTotal;
        this.valorComision = valorComision;
        this.valorComisionRecargoPagoFraccionado = valorComisionRecargoPagoFraccionado;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idToPoliza", column=@Column(name="IDTOPOLIZA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="numeroEndoso", column=@Column(name="NUMEROENDOSO", nullable=false, precision=22, scale=0) ) } )

    public EndosoCancelableId getId() {
        return this.id;
    }
    
    public void setId(EndosoCancelableId id) {
        this.id = id;
    }
    
    @Column(name="ESTATUSREGISTRO", nullable=false, precision=1, scale=0)

    public Short getEstatusRegistro() {
        return this.estatusRegistro;
    }
    
    public void setEstatusRegistro(Short estatusRegistro) {
        this.estatusRegistro = estatusRegistro;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAINICIOVIGENCIA", nullable=false, length=11)

    public Date getFechaInicioVigencia() {
        return this.fechaInicioVigencia;
    }
    
    public void setFechaInicioVigencia(Date fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }
    
    @Column(name="VALORPRIMANETA", precision=24, scale=10)

    public Double getValorPrimaNeta() {
        return this.valorPrimaNeta;
    }
    
    public void setValorPrimaNeta(Double valorPrimaNeta) {
        this.valorPrimaNeta = valorPrimaNeta;
    }
    
    @Column(name="VALORBONIFCOMISION", precision=24, scale=10)

    public Double getValorBonificacionComision() {
        return this.valorBonificacionComision;
    }
    
    public void setValorBonificacionComision(Double valorBonificacionComision) {
        this.valorBonificacionComision = valorBonificacionComision;
    }
    
    @Column(name="VALORBONIFCOMRECPAGOFRAC", precision=24, scale=10)

    public Double getValorBonifComRecargoPagoFraccionado() {
        return this.valorBonifComRecargoPagoFraccionado;
    }
    
    public void setValorBonifComRecargoPagoFraccionado(Double valorBonifComRecargoPagoFraccionado) {
        this.valorBonifComRecargoPagoFraccionado = valorBonifComRecargoPagoFraccionado;
    }
    
    @Column(name="VALORDERECHOS", precision=24, scale=10)

    public Double getValorDerechos() {
        return this.valorDerechos;
    }
    
    public void setValorDerechos(Double valorDerechos) {
        this.valorDerechos = valorDerechos;
    }
    
    @Column(name="VALORRECARGOPAGOFRAC", precision=24, scale=10)

    public Double getValorRecargoPagoFraccionado() {
        return this.valorRecargoPagoFraccionado;
    }
    
    public void setValorRecargoPagoFraccionado(Double valorRecargoPagoFraccionado) {
        this.valorRecargoPagoFraccionado = valorRecargoPagoFraccionado;
    }
    
    @Column(name="VALORIVA", precision=24, scale=10)

    public Double getValorIva() {
        return this.valorIva;
    }
    
    public void setValorIva(Double valorIva) {
        this.valorIva = valorIva;
    }
    
    @Column(name="VALORPRIMATOTAL", precision=24, scale=10)

    public Double getValorPrimaTotal() {
        return this.valorPrimaTotal;
    }
    
    public void setValorPrimaTotal(Double valorPrimaTotal) {
        this.valorPrimaTotal = valorPrimaTotal;
    }
    
    @Column(name="VALORCOMISION", precision=24, scale=10)

    public Double getValorComision() {
        return this.valorComision;
    }
    
    public void setValorComision(Double valorComision) {
        this.valorComision = valorComision;
    }
    
    @Column(name="VALORCOMISIONRECPAGOFRAC", precision=24, scale=10)

    public Double getValorComisionRecargoPagoFraccionado() {
        return this.valorComisionRecargoPagoFraccionado;
    }
    
    public void setValorComisionRecargoPagoFraccionado(Double valorComisionRecargoPagoFraccionado) {
        this.valorComisionRecargoPagoFraccionado = valorComisionRecargoPagoFraccionado;
    }
    
    
    @Enumerated(EnumType.ORDINAL)
    @Column(name="MODULO", precision=2, scale=0)
	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}




}