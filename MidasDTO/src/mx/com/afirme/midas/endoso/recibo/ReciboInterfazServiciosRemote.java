package mx.com.afirme.midas.endoso.recibo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.sistema.SystemException;


public interface ReciboInterfazServiciosRemote {

	public List<ReciboDTO> emiteRecibosPoliza(BigDecimal idPoliza, String nombreUsuario) throws Exception;

	/**
	 * Consulta la informaci�n de los Recibos de una P�liza, indicando tambi�n el numero de endoso
	 * @param numPolizaEndoso (Id de la p�liza | Numero del Endoso)
	 * @param nombreUsuario Nombre del Usuario que realiza la operacion
	 * @return Los recibos de la p�liza/endoso
	 * @throws Exception
	 */
	public List<ReciboDTO> consultaRecibos(String numPolizaEndoso, String nombreUsuario) throws Exception;

	/**
	 * Consulta la informaci�n de los Recibos de una P�liza, indicando tambi�n el numero de endoso
	 * @param idPoliza Id de la p�liza
	 * @param numeroEndoso N�mero de endoso espec�fico
	 * @return Los recibos de la p�liza/endoso
	 * @throws SystemException
	 */
	public List<ReciboDTO> consultaRecibos(BigDecimal idPoliza, Short numeroEndoso,String nombreUsuario)throws Exception;

	/**
	 * Consulta la informaci�n de los Recibos de una P�liza sin endosar
	 * @param idPoliza Id de la p�liza 
	 * @return Los recibos de la p�liza sin endosar
	 * @throws SystemException
	 */
	public List<ReciboDTO> consultaRecibos(BigDecimal idPoliza,String nombreUsuario) throws Exception;
	
	/**
	 * Genera los recibos de las polizas emitidas que tengan pendiente generarlos
	 */
	public void generarRecibos();
	
	/**
	 * Obtiene la factura de la poliza a partir de la llave fiscal de alguno de sus recibos
	 * @param llaveFiscal llave Fiscal de algun recibo de la poliza deseada
	 * @return Factura de la poliza
	 */
	public byte[] obtieneFactura (String llaveFiscal);
	
	
}
