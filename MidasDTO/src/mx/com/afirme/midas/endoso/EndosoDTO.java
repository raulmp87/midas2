package mx.com.afirme.midas.endoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.endoso.agrupacion.AgrupacionEndosoDTO;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoDTO;
import mx.com.afirme.midas.endoso.subinciso.SubIncisoEndosoDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * EndosoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOENDOSO", schema = "MIDAS")
public class EndosoDTO implements java.io.Serializable,Entidad {

	private static final long serialVersionUID = 1L;
	private EndosoId id;
	private PolizaDTO polizaDTO;
	private BigDecimal idToCotizacion;
	private Short claveTipoEndoso;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private Double valorPrimaNeta;
	private Double valorRecargoPagoFrac;
	private Double valorDerechos;
	private Double porcentajeBonifComision;
	private Double valorBonifComision;
	private Double valorIVA;
	private Double valorPrimaTotal;
	private Double valorBonifComisionRPF;
	private Double valorComision;
	private Double valorComisionRPF;
	private Double tipoCambio;
	private String llaveFiscal;
	private List<AgrupacionEndosoDTO> agrupacionEndosoDTOs = new ArrayList<AgrupacionEndosoDTO>();
	private List<SubIncisoEndosoDTO> subIncisoEndosoDTOs = new ArrayList<SubIncisoEndosoDTO>();
	private List<CoberturaEndosoDTO> coberturaEndosoDTOs = new ArrayList<CoberturaEndosoDTO>();
	private Date fechaCreacion;
	private List<ReciboDTO> recibos;
	private CotizacionDTO cotizacionDTO;
	private List<MovimientoCotizacionEndosoDTO> movimientosInciso;
	private List<MovimientoCotizacionEndosoDTO> movimientosGenerales;
	private Double factorAplicacion;
	private Short cancela;
	private Short grupo;
	private Long solictudCheque;

	private String descripcionTipoEndoso;
	private String descripcionMotivoEndoso;
	private String descripcionNumeroEndoso;
	private Date validFrom;
	private Date recordFrom;
	private Boolean checked;
	private Long idSiniestroPT;
	private String folioCorredor;
	
	private boolean existeEdicionEndoso;
	
	//Constants
	public static final short TIPO_ENDOSO_CANCELACION = 4;
	public static final short TIPO_ENDOSO_REHABILITACION = 5;
	public static final short TIPO_ENDOSO_AUMENTO = 2;
	public static final short TIPO_ENDOSO_DISMINUCION = 3;
	public static final short TIPO_ENDOSO_CAMBIO_DATOS = 1;
	public static final short TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO = 6;
	public static final short TIPO_ENDOSO_CANCELACION_ENDOSO = 7;
	public static final short TIPO_ENDOSO_REHABILITACION_ENDOSO = 8;
	public static final short TIPO_ENDOSO_CANCELACION_INCISO = 9;
	public static final short TIPO_ENDOSO_REHABILITACION_INCISO = 10;
	public static final short TIPO_ENDOSO_DESAGRUPACION_RECIBOS = 11;
	public static final short TIPO_ENDOSO_CAMBIO_IVA = 16;
	public static final short TIPO_ENDOSO_CAMBIO_CONDUCTO_COBRO = 27;
	public static final String MARCAQUITARSELECCION = "-";
	
	// Constructors

	/** default constructor */
	public EndosoDTO() {
	}

	/** minimal constructor */
	public EndosoDTO(EndosoId id, PolizaDTO polizaDTO,
			BigDecimal idToCotizacion, Short claveTipoEndoso,
			Date fechaInicioVigencia, Date fechaFinVigencia,
			Double valorPrimaNeta, Double valorRecargoPagoFrac,
			Double valorDerechos, Double porcentajeBonifComision,
			Double valorBonifComision, Double valorIVA, Double valorPrimaTotal) {
		this.id = id;
		this.polizaDTO = polizaDTO;
		this.idToCotizacion = idToCotizacion;
		this.claveTipoEndoso = claveTipoEndoso;
		this.fechaInicioVigencia = fechaInicioVigencia;
		this.fechaFinVigencia = fechaFinVigencia;
		this.valorPrimaNeta = valorPrimaNeta;
		this.valorRecargoPagoFrac = valorRecargoPagoFrac;
		this.valorDerechos = valorDerechos;
		this.porcentajeBonifComision = porcentajeBonifComision;
		this.valorBonifComision = valorBonifComision;
		this.valorIVA = valorIVA;
		this.valorPrimaTotal = valorPrimaTotal;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroEndoso", column = @Column(name = "NUMEROENDOSO", nullable = false, precision = 22, scale = 0)) })
	public EndosoId getId() {
		return this.id;
	}

	public void setId(EndosoId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false)
	public PolizaDTO getPolizaDTO() {
		return this.polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "CLAVETIPOENDOSO", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoEndoso() {
		return this.claveTipoEndoso;
	}

	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAINICIOVIGENCIA", nullable = false, length = 7)
	public Date getFechaInicioVigencia() {
		return this.fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAFINVIGENCIA", nullable = false, length = 7)
	public Date getFechaFinVigencia() {
		return this.fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}

	@Column(name = "VALORRECARGOPAGOFRAC", nullable = false, precision = 16)
	public Double getValorRecargoPagoFrac() {
		return this.valorRecargoPagoFrac;
	}

	public void setValorRecargoPagoFrac(Double valorRecargoPagoFrac) {
		this.valorRecargoPagoFrac = valorRecargoPagoFrac;
	}

	@Column(name = "VALORDERECHOS", nullable = false, precision = 16)
	public Double getValorDerechos() {
		return this.valorDerechos;
	}

	public void setValorDerechos(Double valorDerechos) {
		this.valorDerechos = valorDerechos;
	}

	@Column(name = "PORCENTAJEBONIFCOMISION", nullable = false, precision = 8, scale = 4)
	public Double getPorcentajeBonifComision() {
		return this.porcentajeBonifComision;
	}

	public void setPorcentajeBonifComision(Double porcentajeBonifComision) {
		this.porcentajeBonifComision = porcentajeBonifComision;
	}

	@Column(name = "VALORBONIFCOMISION", nullable = false, precision = 16)
	public Double getValorBonifComision() {
		return this.valorBonifComision;
	}

	public void setValorBonifComision(Double valorBonifComision) {
		this.valorBonifComision = valorBonifComision;
	}

	@Column(name = "VALORIVA", nullable = false, precision = 16)
	public Double getValorIVA() {
		return this.valorIVA;
	}

	public void setValorIVA(Double valorIVA) {
		this.valorIVA = valorIVA;
	}

	@Column(name = "VALORPRIMATOTAL", nullable = false, precision = 16)
	public Double getValorPrimaTotal() {
		return this.valorPrimaTotal;
	}

	public void setValorPrimaTotal(Double valorPrimaTotal) {
		this.valorPrimaTotal = valorPrimaTotal;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "endosoDTO")
	public List<AgrupacionEndosoDTO> getAgrupacionEndosoDTOs() {
		return this.agrupacionEndosoDTOs;
	}

	public void setAgrupacionEndosoDTOs(
			List<AgrupacionEndosoDTO> agrupacionEndosoDTOs) {
		this.agrupacionEndosoDTOs = agrupacionEndosoDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "endosoDTO")
	public List<SubIncisoEndosoDTO> getSubIncisoEndosoDTOs() {
		return this.subIncisoEndosoDTOs;
	}

	public void setSubIncisoEndosoDTOs(
			List<SubIncisoEndosoDTO> subIncisoEndosoDTOs) {
		this.subIncisoEndosoDTOs = subIncisoEndosoDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "endosoDTO")
	public List<CoberturaEndosoDTO> getCoberturaEndosoDTOs() {
		return this.coberturaEndosoDTOs;
	}

	public void setCoberturaEndosoDTOs(
			List<CoberturaEndosoDTO> coberturaEndosoDTOs) {
		this.coberturaEndosoDTOs = coberturaEndosoDTOs;
	}

	@Column(name = "TIPOCAMBIO")
	public Double getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	@Column(name = "LLAVEFISCAL", nullable = true, length = 15)
	public String getLlaveFiscal() {
		return this.llaveFiscal;
	}

	public void setLlaveFiscal(String llaveFiscal) {
		this.llaveFiscal = llaveFiscal;
	}

	@Column(name = "VALORBONIFCOMRECPAGOFRAC")
	public Double getValorBonifComisionRPF() {
		return valorBonifComisionRPF;
	}

	public void setValorBonifComisionRPF(Double valorBonifComisionRPF) {
		this.valorBonifComisionRPF = valorBonifComisionRPF;
	}

	@Column(name = "VALORCOMISION")
	public Double getValorComision() {
		return valorComision;
	}

	public void setValorComision(Double valorComision) {
		this.valorComision = valorComision;
	}

	@Column(name = "VALORCOMISIONRECPAGOFRAC")
	public Double getValorComisionRPF() {
		return valorComisionRPF;
	}

	public void setValorComisionRPF(Double valorComisionRPF) {
		this.valorComisionRPF = valorComisionRPF;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION")
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Transient
	public List<ReciboDTO> getRecibos() {
		return recibos;
	}

	public void setRecibos(List<ReciboDTO> recibos) {
		this.recibos = recibos;
	}

	@Transient
	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	@Transient
	public List<MovimientoCotizacionEndosoDTO> getMovimientosInciso() {
		return movimientosInciso;
	}

	public void setMovimientosInciso(
			List<MovimientoCotizacionEndosoDTO> movimientosInciso) {
		this.movimientosInciso = movimientosInciso;
	}

	@Transient
	public List<MovimientoCotizacionEndosoDTO> getMovimientosGenerales() {
		return movimientosGenerales;
	}

	public void setMovimientosGenerales(
			List<MovimientoCotizacionEndosoDTO> movimientosGenerales) {
		this.movimientosGenerales = movimientosGenerales;
	}

	@Column(name = "FACTORAPLICACION")
	public Double getFactorAplicacion() {
		return factorAplicacion;
	}

	public void setFactorAplicacion(Double factorAplicacion) {
		this.factorAplicacion = factorAplicacion;
	}
	
	@Column(name = "CANCELA", precision = 4, scale = 0)
	public Short getCancela() {
		return this.cancela;
	}

	public void setCancela(Short cancela) {
		this.cancela = cancela;
	}

	@Column(name = "GRUPO", precision = 4, scale = 0)
	public Short getGrupo() {
		return this.grupo;
	}

	public void setGrupo(Short grupo) {
		this.grupo = grupo;
	}

	@Column(name = "SOLICITUDCHEQUE")
	public Long getSolictudCheque() {
		return solictudCheque;
	}

	public void setSolictudCheque(Long solictudCheque) {
		this.solictudCheque = solictudCheque;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "VALIDFROM")
	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "RECORDFROM")
	public Date getRecordFrom() {
		return recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}

	@Transient
	public String getDescripcionTipoEndoso() {
		return descripcionTipoEndoso;
	}

	public void setDescripcionTipoEndoso(String descripcionTipoEndoso) {
		this.descripcionTipoEndoso = descripcionTipoEndoso;
	}
	
	@Transient
	public String getDescripcionMotivoEndoso() {
		return descripcionMotivoEndoso;
	}

	public void setDescripcionMotivoEndoso(String descripcionMotivoEndoso) {
		this.descripcionMotivoEndoso = descripcionMotivoEndoso;
	}

	@Transient
	public String getDescripcionNumeroEndoso() {
		return descripcionNumeroEndoso;
	}

	public void setDescripcionNumeroEndoso(String descripcionNumeroEndoso) {
		this.descripcionNumeroEndoso = descripcionNumeroEndoso;
	}

	@SuppressWarnings("unchecked")
	@Override
	public EndosoId getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return this.id.getIdToPoliza() + "-"+ this.id.getNumeroEndoso();
	}

	@SuppressWarnings("unchecked")
	@Override
	public EndosoId getBusinessKey() {
		return this.id;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("id: " + this.getId() + ", ");
		sb.append("descripcionTipoEndoso: " + this.getDescripcionTipoEndoso() + ", ");
		sb.append("FechaInicioVigencia: " + this.getFechaInicioVigencia() + ", ");
		sb.append("FechaFinVigencia: " + this.getFechaFinVigencia() + ", ");
		sb.append("]");
		
		return sb.toString();
	}


	@Transient
	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	@Column(name = "PT_SINIESTROCABINA_ID", nullable = true)
	public Long getIdSiniestroPT() {
		return idSiniestroPT;
	}

	public void setIdSiniestroPT(Long idSiniestroPT) {
		this.idSiniestroPT = idSiniestroPT;
	}

	@Transient
	public boolean isExisteEdicionEndoso() {
		return existeEdicionEndoso;
	}

	public void setExisteEdicionEndoso(boolean existeEdicionEndoso) {
		this.existeEdicionEndoso = existeEdicionEndoso;
	}
	
	@Column(name = "FOLIOCORREDOR", nullable = true)
	public String getFolioCorredor() {
		return folioCorredor;
	}

	public void setFolioCorredor(String folioCorredor) {
		this.folioCorredor = folioCorredor;
	}
	
}