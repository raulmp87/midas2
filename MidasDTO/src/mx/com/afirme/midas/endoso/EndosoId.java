package mx.com.afirme.midas.endoso;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * EndosoId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class EndosoId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal idToPoliza;
	private Short numeroEndoso;

	// Constructors

	/** default constructor */
	public EndosoId() {
	}

	/** full constructor */
	public EndosoId(BigDecimal idToPoliza, Short numeroEndoso) {
		this.idToPoliza = idToPoliza;
		this.numeroEndoso = numeroEndoso;
	}

	// Property accessors

	@Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToPoliza() {
		return this.idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@Column(name = "NUMEROENDOSO", nullable = false, precision = 4, scale = 0)
	public Short getNumeroEndoso() {
		return this.numeroEndoso;
	}

	public void setNumeroEndoso(Short numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof EndosoId))
			return false;
		EndosoId castOther = (EndosoId) other;

		return ((this.getIdToPoliza() == castOther.getIdToPoliza()) || (this
				.getIdToPoliza() != null
				&& castOther.getIdToPoliza() != null && this.getIdToPoliza()
				.equals(castOther.getIdToPoliza())))
				&& ((this.getNumeroEndoso() == castOther.getNumeroEndoso()) || (this
						.getNumeroEndoso() != null
						&& castOther.getNumeroEndoso() != null && this
						.getNumeroEndoso().equals(castOther.getNumeroEndoso())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToPoliza() == null ? 0 : this.getIdToPoliza()
						.hashCode());
		result = 37
				* result
				+ (getNumeroEndoso() == null ? 0 : this.getNumeroEndoso()
						.hashCode());
		return result;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("NumeroEndoso: " + this.getNumeroEndoso() + ", ");
		sb.append("IdToPoliza: " + this.getIdToPoliza());
		sb.append("]");
		
		return sb.toString();
	}
}