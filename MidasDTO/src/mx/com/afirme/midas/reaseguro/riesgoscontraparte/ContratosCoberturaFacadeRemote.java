package mx.com.afirme.midas.reaseguro.riesgoscontraparte;

import java.math.BigDecimal;
import java.util.List;

/**
 * Remote interface for ContratosCoberturaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ContratosCoberturaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * ContratosCoberturaDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ContratosCoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	void save(ContratosCoberturaDTO entity);

	/**
	 * Delete a persistent ContratosCoberturaDTO entity.
	 * 
	 * @param entity
	 *            ContratosCoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	void delete(ContratosCoberturaDTO entity);

	/**
	 * Persist a previously saved ContratosCoberturaDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * ContratosCoberturaDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            ContratosCoberturaDTO entity to update
	 * @return ContratosCoberturaDTO the persisted
	 *         ContratosCoberturaDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	ContratosCoberturaDTO update(
			ContratosCoberturaDTO entity);

	ContratosCoberturaDTO findById(BigDecimal id);

	/**
	 * Find all ContratosCoberturaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the ContratosCoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ContratosCoberturaDTO> found by query
	 */
	List<ContratosCoberturaDTO> findByProperty(
			String propertyName, Object value);

	/**
	 * Find all ContratosCoberturaDTO entities.
	 * 
	 * @return List<ContratosCoberturaDTO> all
	 *         ContratosCoberturaDTO entities
	 */
	List<ContratosCoberturaDTO> findAll();
}