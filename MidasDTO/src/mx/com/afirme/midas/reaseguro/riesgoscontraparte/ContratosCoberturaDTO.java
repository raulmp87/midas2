package mx.com.afirme.midas.reaseguro.riesgoscontraparte;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Paulo dos Santos
 */
@Entity
@Table(name="CNSF_SIN_CONTRATOS_COBERTURA", schema="MIDAS")
public class ContratosCoberturaDTO implements Serializable  {

	private static final long serialVersionUID = 1L;
	private BigDecimal idtcontrato;
	private Date fechaCorte;
	private String ramo;
	private String cveEsquema;
	private BigDecimal monto;
	private String calificacion;
	
	/** default constructor */
    public ContratosCoberturaDTO() {
    }
    
    @Id
    @SequenceGenerator(name = "IDCONTRATO_SEQ_GENERADOR1", allocationSize = 1, sequenceName = "MIDAS.CNSF_SIN_CONTRATO_COB_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCONTRATO_SEQ_GENERADOR1")
    @Column(name="ID_CONTRATO", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdcontrato() {
        return this.idtcontrato;
    }
    
    public void setIdcontrato(BigDecimal idtcontrato) {
        this.idtcontrato = idtcontrato;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHA_CORTE", nullable=false, length=10)
	public Date getFechaCorte() {
		return this.fechaCorte;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	
	@Column(name="RAMO", nullable=false, length=40)
	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	
	@Column(name="CLAVE_ESQ_REAS", nullable=false, length=25)
	public String getCveEsquema() {
		return cveEsquema;
	}

	public void setCveEsquema(String cveEsquema) {
		this.cveEsquema = cveEsquema;
	}
	
	@Column(name="MONTO", nullable=false, length=25)
	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	
	@Column(name="CALIFICACION", nullable=false, length=25)
	public String getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}

		
}