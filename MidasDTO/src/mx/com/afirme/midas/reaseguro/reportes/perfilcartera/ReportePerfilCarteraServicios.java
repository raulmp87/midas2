package mx.com.afirme.midas.reaseguro.reportes.perfilcartera;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;


public interface ReportePerfilCarteraServicios {

	List<RegistroPerfilCarteraDTO> obtenerRegistrosPerfilCartera(
			Date fechaInicial,Date fechaFinal,Double tipoCambio,
			Integer tipoReporte,String nombreUsuario) throws Exception;
	
	public List<AgrupacionSubRamoPerfilCarteraDTO> obtenerRegistrosAgrupadosPerfilCartera(Date fechaInicial, 
			Date fechaFinal, Double tipoCambio,Integer tipoReporte,String nombreUsuario) throws Exception;
}
