package mx.com.afirme.midas.reaseguro.reportes.siniestrosconreaseguro.prioridad;

// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for PrioridadSiniestroReaseguroDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface PrioridadSiniestroReaseguroFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved PrioridadSiniestroReaseguroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PrioridadSiniestroReaseguroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(PrioridadSiniestroReaseguroDTO entity);
    /**
	 Delete a persistent PrioridadSiniestroReaseguroDTO entity.
	  @param entity PrioridadSiniestroReaseguroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PrioridadSiniestroReaseguroDTO entity);
   /**
	 Persist a previously saved PrioridadSiniestroReaseguroDTO entity and return it or a copy of it to the sender. 
	 A copy of the PrioridadSiniestroReaseguroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PrioridadSiniestroReaseguroDTO entity to update
	 @return PrioridadSiniestroReaseguroDTO the persisted PrioridadSiniestroReaseguroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public PrioridadSiniestroReaseguroDTO update(PrioridadSiniestroReaseguroDTO entity);
	public PrioridadSiniestroReaseguroDTO findById( BigDecimal id);
	 /**
	 * Find all PrioridadSiniestroReaseguroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PrioridadSiniestroReaseguroDTO property to query
	  @param value the property value to match
	  	  @return List<PrioridadSiniestroReaseguroDTO> found by query
	 */
	public List<PrioridadSiniestroReaseguroDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all PrioridadSiniestroReaseguroDTO entities.
	  	  @return List<PrioridadSiniestroReaseguroDTO> all PrioridadSiniestroReaseguroDTO entities
	 */
	public List<PrioridadSiniestroReaseguroDTO> findAll(
		);	
}