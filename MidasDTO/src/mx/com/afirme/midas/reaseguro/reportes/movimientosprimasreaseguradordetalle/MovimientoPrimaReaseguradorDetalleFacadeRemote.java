package mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreaseguradordetalle;

import java.util.List;

import javax.ejb.Remote;


public interface MovimientoPrimaReaseguradorDetalleFacadeRemote {

	public List<MovimientoPrimaReaseguradorDetalleDTO> obtenerMovimientosPrimaDetalladosPorReasegurador(MovimientoPrimaReaseguradorDetalleDTO movimientoPrimaReaseguradorDetalle,String nombreUsuario) throws Exception;
	
}
