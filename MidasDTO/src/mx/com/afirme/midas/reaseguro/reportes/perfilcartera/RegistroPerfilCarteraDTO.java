package mx.com.afirme.midas.reaseguro.reportes.perfilcartera;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * @author jose luis arellano
 */
public class RegistroPerfilCarteraDTO implements Serializable {
	private static final long serialVersionUID = 1180834380930678065L;

	public static final int TIPO_REPORTE_RIESGOS = 1;
	public static final int TIPO_REPORTE_TOTAL_SINIESTROS= 2;
	
	private Integer orden;
	private Integer idTcSubRamo;
	private String descripcionSubRamo;
	private BigDecimal rangoMinimo;
	private BigDecimal rangoMaximo;
	
	private BigDecimal sumatoriaSumaAsegurada;
	private BigDecimal totalSumaAsegurada;
	private BigDecimal promedioSumaAsegurada;
	
	private BigDecimal sumatoriaPrimaNeta;
	private BigDecimal totalPrimaNeta;
	private BigDecimal promedioPrimaNeta;
	private BigDecimal porcentajePrimaNeta;
	private BigDecimal porcentajeAcumuladoPrimaNeta;
	
	private Integer cantidadCumulos;
	private Integer totalCumulos;
	private BigDecimal porcentajeCantidadCumulos;
	private BigDecimal porcentajeAcumuladoCantidadCumulos;
	
	private Integer cantidadPolizas;
	private Integer totalPolizas;
	
	private Integer cantidadSiniestros;
	private Integer totalSiniestros;
	private BigDecimal porcentajeCantidadSiniestros;
	private BigDecimal sumatoriaMontoSiniestro;
	private BigDecimal totalMontoSiniestros;
	private BigDecimal porcentajeMontoSiniestros;
	private BigDecimal porcentajeAcumuladoMontoSiniestros;
	
	private BigDecimal frecuencia;
	private BigDecimal siniestralidad;
	private BigDecimal costoPromedio;
	private BigDecimal severidad;
	
	
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	public Integer getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(Integer idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}
	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}
	public BigDecimal getRangoMinimo() {
		return rangoMinimo;
	}
	public void setRangoMinimo(BigDecimal rangoMinimo) {
		this.rangoMinimo = rangoMinimo;
	}
	public BigDecimal getRangoMaximo() {
		return rangoMaximo;
	}
	public void setRangoMaximo(BigDecimal rangoMaximo) {
		this.rangoMaximo = rangoMaximo;
	}
	public BigDecimal getSumatoriaSumaAsegurada() {
		return sumatoriaSumaAsegurada;
	}
	public void setSumatoriaSumaAsegurada(BigDecimal sumatoriaSumaAsegurada) {
		this.sumatoriaSumaAsegurada = sumatoriaSumaAsegurada;
	}
	public BigDecimal getSumatoriaPrimaNeta() {
		return sumatoriaPrimaNeta;
	}
	public void setSumatoriaPrimaNeta(BigDecimal sumatoriaPrimaNeta) {
		this.sumatoriaPrimaNeta = sumatoriaPrimaNeta;
	}
	public Integer getCantidadPolizas() {
		return cantidadPolizas;
	}
	public void setCantidadPolizas(Integer cantidadPolizas) {
		this.cantidadPolizas = cantidadPolizas;
	}
	public Integer getCantidadCumulos() {
		return cantidadCumulos;
	}
	public void setCantidadCumulos(Integer cantidadCumulos) {
		this.cantidadCumulos = cantidadCumulos;
	}
	public Integer getTotalPolizas() {
		return totalPolizas;
	}
	public void setTotalPolizas(Integer totalPolizas) {
		this.totalPolizas = totalPolizas;
	}
	public BigDecimal getTotalSumaAsegurada() {
		return totalSumaAsegurada;
	}
	public void setTotalSumaAsegurada(BigDecimal totalSumaAsegurada) {
		this.totalSumaAsegurada = totalSumaAsegurada;
	}
	public BigDecimal getTotalPrimaNeta() {
		return totalPrimaNeta;
	}
	public void setTotalPrimaNeta(BigDecimal totalPrimaNeta) {
		this.totalPrimaNeta = totalPrimaNeta;
	}
	public BigDecimal getSumatoriaMontoSiniestro() {
		return sumatoriaMontoSiniestro;
	}
	public void setSumatoriaMontoSiniestro(BigDecimal sumatoriaMontoSiniestro) {
		this.sumatoriaMontoSiniestro = sumatoriaMontoSiniestro;
	}
	public BigDecimal getPorcentajePrimaNeta() {
		return porcentajePrimaNeta;
	}
	public void setPorcentajePrimaNeta(BigDecimal porcentajePrimaNeta) {
		this.porcentajePrimaNeta = porcentajePrimaNeta;
	}
	public BigDecimal getPorcentajeAcumuladoPrimaNeta() {
		return porcentajeAcumuladoPrimaNeta;
	}
	public void setPorcentajeAcumuladoPrimaNeta(
			BigDecimal porcentajeAcumuladoPrimaNeta) {
		this.porcentajeAcumuladoPrimaNeta = porcentajeAcumuladoPrimaNeta;
	}
	public BigDecimal getPorcentajeCantidadCumulos() {
		return porcentajeCantidadCumulos;
	}
	public void setPorcentajeCantidadCumulos(BigDecimal porcentajeCantidadCumulos) {
		this.porcentajeCantidadCumulos = porcentajeCantidadCumulos;
	}
	public BigDecimal getPorcentajeAcumuladoCantidadCumulos() {
		return porcentajeAcumuladoCantidadCumulos;
	}
	public void setPorcentajeAcumuladoCantidadCumulos(
			BigDecimal porcentajeAcumuladoCantidadCumulos) {
		this.porcentajeAcumuladoCantidadCumulos = porcentajeAcumuladoCantidadCumulos;
	}
	public BigDecimal getPromedioSumaAsegurada() {
		return promedioSumaAsegurada;
	}
	public void setPromedioSumaAsegurada(BigDecimal promedioSumaAsegurada) {
		this.promedioSumaAsegurada = promedioSumaAsegurada;
	}
	public BigDecimal getPromedioPrimaNeta() {
		return promedioPrimaNeta;
	}
	public void setPromedioPrimaNeta(BigDecimal promedioPrimaNeta) {
		this.promedioPrimaNeta = promedioPrimaNeta;
	}
	public Integer getCantidadSiniestros() {
		return cantidadSiniestros;
	}
	public void setCantidadSiniestros(Integer cantidadSiniestros) {
		this.cantidadSiniestros = cantidadSiniestros;
	}
	public BigDecimal getPorcentajeMontoSiniestros() {
		return porcentajeMontoSiniestros;
	}
	public BigDecimal getPorcentajeCantidadSiniestros() {
		return porcentajeCantidadSiniestros;
	}
	public void setPorcentajeCantidadSiniestros(
			BigDecimal porcentajeCantidadSiniestros) {
		this.porcentajeCantidadSiniestros = porcentajeCantidadSiniestros;
	}
	public BigDecimal getFrecuencia() {
		return frecuencia;
	}
	public void setFrecuencia(BigDecimal frecuencia) {
		this.frecuencia = frecuencia;
	}
	public BigDecimal getSiniestralidad() {
		return siniestralidad;
	}
	public void setSiniestralidad(BigDecimal siniestralidad) {
		this.siniestralidad = siniestralidad;
	}
	public BigDecimal getCostoPromedio() {
		return costoPromedio;
	}
	public void setCostoPromedio(BigDecimal costoPromedio) {
		this.costoPromedio = costoPromedio;
	}
	public BigDecimal getSeveridad() {
		return severidad;
	}
	public void setSeveridad(BigDecimal severidad) {
		this.severidad = severidad;
	}
	public void setPorcentajeMontoSiniestros(BigDecimal porcentajeMontoSiniestros) {
		this.porcentajeMontoSiniestros = porcentajeMontoSiniestros;
	}
	public BigDecimal getPorcentajeAcumuladoMontoSiniestros() {
		return porcentajeAcumuladoMontoSiniestros;
	}
	public BigDecimal getTotalMontoSiniestros() {
		return totalMontoSiniestros;
	}
	public void setTotalMontoSiniestros(BigDecimal totalMontoSiniestros) {
		this.totalMontoSiniestros = totalMontoSiniestros;
	}
	public void setPorcentajeAcumuladoMontoSiniestros(BigDecimal porcentajeAcumuladoMontoSiniestros) {
		this.porcentajeAcumuladoMontoSiniestros = porcentajeAcumuladoMontoSiniestros;
	}
	public Integer getTotalSiniestros() {
		return totalSiniestros;
	}
	public Integer getTotalCumulos() {
		return totalCumulos;
	}
	public void setTotalCumulos(Integer totalCumulos) {
		this.totalCumulos = totalCumulos;
	}
	public void setTotalSiniestros(Integer totalSiniestros) {
		this.totalSiniestros = totalSiniestros;
	}
}
