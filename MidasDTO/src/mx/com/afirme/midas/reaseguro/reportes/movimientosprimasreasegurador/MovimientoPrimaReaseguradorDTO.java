package mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreasegurador;

import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;

public class MovimientoPrimaReaseguradorDTO implements Serializable{

	private static final long serialVersionUID = -5297160513980456639L;

	/**
	 * Variables utilizadas como datos de salida para el SP:
	 */
	private String codigoSubRamo;
	private String folioPoliza;
	private Short numeroEndoso;
//	private Date fInicioVigencia;
//	private Date fFinVigencia;
	private Integer numeroInciso;
	private Integer numeroSubInciso;
	private Double porcentajeRetencion;
	private Double porcentajeCuotaParte;
	private Double porcentajePrimerExcedente;
	private Double porcentajeFacultativo;
	private Double montoPrimaNeta;
	private String folioContrato;
	private String tipoReaseguro;
	private String nombreACuentaDe;
	private Double porcentajeParticipacion;
	private Double porcentajeComision;
	private String nombreCorredor;
	private Double porcentajeParticipacionCorredor;
	private Double porcentajeComisionCorredor;
	private String descripcionAmbito;
	private Double montoPrimaCedida;
	private Double montoComisiones;
	private String descripcionMoneda;
	private Date fechaMovimiento;
	private String descripcionSucursal;
	private Integer idAsegurado;
	private String nombreAsegurado;
	private String descripcionMonedaReporte;
	private String folioPolizaReporte;
	private Double montoSumaAsegurada;
	private Double montoSumaAseguradaReaseguro;
	private Double montoSumaAseguradaDistribuida;
	private Double porcentajePleno;
	private String descripcionOficina;
	private Integer idTipoReaseguro;
	private String identificador;
	
	/**
	 * Variables utilizadas como datos de entrada y de salida para el SP:
	 * 
	 */
	
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	
	/**
	 * Variables utilizadas como datos de entrada para el SP:
	 */
	private BigDecimal idToPoliza;
	private Integer idMoneda;
	private Integer IdRamo;
	private Integer IdSubRamo;
	
	/*
	 * Getter's y Setter's
	 */
	public String getCodigoSubRamo() {
		return codigoSubRamo;
	}
	public void setCodigoSubRamo(String codigoSubRamo) {
		this.codigoSubRamo = codigoSubRamo;
	}
	public String getFolioPoliza() {
		return folioPoliza;
	}
	public void setFolioPoliza(String folioPoliza) {
		this.folioPoliza = folioPoliza;
	}
	public Short getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Short numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public Integer getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(Integer numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	public Double getPorcentajeRetencion() {
		return porcentajeRetencion;
	}
	public void setPorcentajeRetencion(Double porcentajeRetencion) {
		this.porcentajeRetencion = porcentajeRetencion;
	}
	public Double getPorcentajeCuotaParte() {
		return porcentajeCuotaParte;
	}
	public void setPorcentajeCuotaParte(Double porcentajeCuotaParte) {
		this.porcentajeCuotaParte = porcentajeCuotaParte;
	}
	public Double getPorcentajePrimerExcedente() {
		return porcentajePrimerExcedente;
	}
	public void setPorcentajePrimerExcedente(Double porcentajePrimerExcedente) {
		this.porcentajePrimerExcedente = porcentajePrimerExcedente;
	}
	public Double getPorcentajeFacultativo() {
		return porcentajeFacultativo;
	}
	public void setPorcentajeFacultativo(Double porcentajeFacultativo) {
		this.porcentajeFacultativo = porcentajeFacultativo;
	}
	public Double getMontoPrimaNeta() {
		return montoPrimaNeta;
	}
	public void setMontoPrimaNeta(Double montoPrimaNeta) {
		this.montoPrimaNeta = montoPrimaNeta;
	}
	public String getFolioContrato() {
		return folioContrato;
	}
	public void setFolioContrato(String folioContrato) {
		this.folioContrato = folioContrato;
	}
	public String getTipoReaseguro() {
		return tipoReaseguro;
	}
	public void setTipoReaseguro(String tipoReaseguro) {
		this.tipoReaseguro = tipoReaseguro;
	}
	public String getNombreACuentaDe() {
		return nombreACuentaDe;
	}
	public void setNombreACuentaDe(String nombreACuentaDe) {
		this.nombreACuentaDe = nombreACuentaDe;
	}
	public Double getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	public void setPorcentajeParticipacion(Double porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	public Double getPorcentajeComision() {
		return porcentajeComision;
	}
	public void setPorcentajeComision(Double porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}
	public String getNombreCorredor() {
		return nombreCorredor;
	}
	public void setNombreCorredor(String nombreCorredor) {
		this.nombreCorredor = nombreCorredor;
	}
	public Double getPorcentajeParticipacionCorredor() {
		return porcentajeParticipacionCorredor;
	}
	public void setPorcentajeParticipacionCorredor(
			Double porcentajeParticipacionCorredor) {
		this.porcentajeParticipacionCorredor = porcentajeParticipacionCorredor;
	}
	public Double getPorcentajeComisionCorredor() {
		return porcentajeComisionCorredor;
	}
	public void setPorcentajeComisionCorredor(Double porcentajeComisionCorredor) {
		this.porcentajeComisionCorredor = porcentajeComisionCorredor;
	}
	public String getDescripcionAmbito() {
		return descripcionAmbito;
	}
	public void setDescripcionAmbito(String descripcionAmbito) {
		this.descripcionAmbito = descripcionAmbito;
	}
	public Double getMontoPrimaCedida() {
		return montoPrimaCedida;
	}
	public void setMontoPrimaCedida(Double montoPrimaCedida) {
		this.montoPrimaCedida = montoPrimaCedida;
	}
	public Double getMontoComisiones() {
		return montoComisiones;
	}
	public void setMontoComisiones(Double montoComisiones) {
		this.montoComisiones = montoComisiones;
	}
	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}
	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	public String getDescripcionSucursal() {
		return descripcionSucursal;
	}
	public void setDescripcionSucursal(String descripcionSucursal) {
		this.descripcionSucursal = descripcionSucursal;
	}
	public Integer getIdAsegurado() {
		return idAsegurado;
	}
	public void setIdAsegurado(Integer idAsegurado) {
		this.idAsegurado = idAsegurado;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	public String getDescripcionMonedaReporte() {
		return descripcionMonedaReporte;
	}
	public void setDescripcionMonedaReporte(String descripcionMonedaReporte) {
		this.descripcionMonedaReporte = descripcionMonedaReporte;
	}
	public String getFolioPolizaReporte() {
		return folioPolizaReporte;
	}
	public void setFolioPolizaReporte(String folioPolizaReporte) {
		this.folioPolizaReporte = folioPolizaReporte;
	}
	public Double getMontoSumaAsegurada() {
		return montoSumaAsegurada;
	}
	public void setMontoSumaAsegurada(Double montoSumaAsegurada) {
		this.montoSumaAsegurada = montoSumaAsegurada;
	}
	public Double getMontoSumaAseguradaReaseguro() {
		return montoSumaAseguradaReaseguro;
	}
	public void setMontoSumaAseguradaReaseguro(Double montoSumaAseguradaReaseguro) {
		this.montoSumaAseguradaReaseguro = montoSumaAseguradaReaseguro;
	}
	public Double getMontoSumaAseguradaDistribuida() {
		return montoSumaAseguradaDistribuida;
	}
	public void setMontoSumaAseguradaDistribuida(
			Double montoSumaAseguradaDistribuida) {
		this.montoSumaAseguradaDistribuida = montoSumaAseguradaDistribuida;
	}
	public Double getPorcentajePleno() {
		return porcentajePleno;
	}
	public void setPorcentajePleno(Double porcentajePleno) {
		this.porcentajePleno = porcentajePleno;
	}
	public String getDescripcionOficina() {
		return descripcionOficina;
	}
	public void setDescripcionOficina(String descripcionOficina) {
		this.descripcionOficina = descripcionOficina;
	}
	public Integer getIdTipoReaseguro() {
		return idTipoReaseguro;
	}
	public void setIdTipoReaseguro(Integer idTipoReaseguro) {
		this.idTipoReaseguro = idTipoReaseguro;
	}
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public Integer getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(Integer idMoneda) {
		this.idMoneda = idMoneda;
	}
	public Integer getIdRamo() {
		return IdRamo;
	}
	public void setIdRamo(Integer idRamo) {
		IdRamo = idRamo;
	}
	public Integer getIdSubRamo() {
		return IdSubRamo;
	}
	public void setIdSubRamo(Integer idSubRamo) {
		IdSubRamo = idSubRamo;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
}
