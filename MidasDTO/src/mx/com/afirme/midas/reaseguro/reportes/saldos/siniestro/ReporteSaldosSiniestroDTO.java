package mx.com.afirme.midas.reaseguro.reportes.saldos.siniestro;
// default package

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * ReporteSaldosSiniestroDTO entity. @author Jose Luis Arellano
 */
@Entity
@Table(name="TRPTSALDOSSINIESTRO",schema="MIDAS")
public class ReporteSaldosSiniestroDTO  implements java.io.Serializable {
	private static final long serialVersionUID = -1793097472406712161L;
	private Double idReporteSaldosSiniestroDTO;
    private String nombreReasegurador;
    private String nombreCorredor;
    private String numeroSiniestro;
    private Date fechaSiniestro;
    private String estatusSiniestro;
    private String codigoSubRamo;
    private Short numeroInciso;
    private String numeroPoliza;
    private Short numeroEndoso;
    private String nombreAsegurado;
    private Date fechaEmision;
    private Date fechaInicioVigencia;
    private Date fechaFinVigencia;
    private String descripcionConceptoMovimiento;
    private Short idTcTipoReaseguro;
    private String descripcionTipoContrato;
    private Date fechaMovimiento;
    private String mesMovimiento;
    private BigDecimal montoCesionSiniestro;
    private BigDecimal montoCobros;
    private BigDecimal montoSaldosPorCobrar;
    private BigDecimal tipoCambio;
    private Double idMoneda;
    private String descripcionMoneda;


    // Constructors

    /** default constructor */
    public ReporteSaldosSiniestroDTO() {
    }

	/** minimal constructor */
    public ReporteSaldosSiniestroDTO(Double idReporteSaldosSiniestroDTO) {
        this.idReporteSaldosSiniestroDTO = idReporteSaldosSiniestroDTO;
    }
   
    // Property accessors
    @Id 
    @Column(name="IDTRPTSALDOSSINIESTRO", nullable=false, precision=8, scale=0)
    public Double getIdReporteSaldosSiniestroDTO() {
        return this.idReporteSaldosSiniestroDTO;
    }
    
    public void setIdReporteSaldosSiniestroDTO(Double idReporteSaldosSiniestroDTO) {
        this.idReporteSaldosSiniestroDTO = idReporteSaldosSiniestroDTO;
    }
    
    @Column(name="NOMBREREASEGURADOR", length=100)
    public String getNombreReasegurador() {
        return this.nombreReasegurador;
    }
    
    public void setNombreReasegurador(String nombreReasegurador) {
        this.nombreReasegurador = nombreReasegurador;
    }
    
    @Column(name="NOMBRECORREDOR", length=100)
    public String getNombreCorredor() {
        return this.nombreCorredor;
    }
    
    public void setNombreCorredor(String nombreCorredor) {
        this.nombreCorredor = nombreCorredor;
    }
    
    @Column(name="NUMEROSINIESTRO", length=50)

    public String getNumeroSiniestro() {
        return this.numeroSiniestro;
    }
    
    public void setNumeroSiniestro(String numeroSiniestro) {
        this.numeroSiniestro = numeroSiniestro;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHASINIESTRO", length=7)

    public Date getFechaSiniestro() {
        return this.fechaSiniestro;
    }
    
    public void setFechaSiniestro(Date fechaSiniestro) {
        this.fechaSiniestro = fechaSiniestro;
    }
    
    @Column(name="ESTATUSSINIESTRO", length=100)

    public String getEstatusSiniestro() {
        return this.estatusSiniestro;
    }
    
    public void setEstatusSiniestro(String estatusSiniestro) {
        this.estatusSiniestro = estatusSiniestro;
    }
    
    @Column(name="CODIGOSUBRAMO", length=10)

    public String getCodigoSubRamo() {
        return this.codigoSubRamo;
    }
    
    public void setCodigoSubRamo(String codigoSubRamo) {
        this.codigoSubRamo = codigoSubRamo;
    }
    
    @Column(name="NUMEROINCISO", precision=22, scale=0)
    public Short getNumeroInciso() {
        return this.numeroInciso;
    }
    
    public void setNumeroInciso(Short numeroInciso) {
        this.numeroInciso = numeroInciso;
    }
    
    @Column(name="NUMEROPOLIZA", length=20)
    public String getNumeroPoliza() {
        return this.numeroPoliza;
    }
    
    public void setNumeroPoliza(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }
    
    @Column(name="NUMEROENDOSO", precision=4, scale=0)
    public Short getNumeroEndoso() {
        return this.numeroEndoso;
    }
    
    public void setNumeroEndoso(Short numeroEndoso) {
        this.numeroEndoso = numeroEndoso;
    }
    
    @Column(name="NOMBREASEGURADO", length=240)
    public String getNombreAsegurado() {
        return this.nombreAsegurado;
    }
    
    public void setNombreAsegurado(String nombreAsegurado) {
        this.nombreAsegurado = nombreAsegurado;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAEMISION", length=7)
    public Date getFechaEmision() {
        return this.fechaEmision;
    }
    
    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAINICIOVIGENCIA", length=7)
    public Date getFechaInicioVigencia() {
        return this.fechaInicioVigencia;
    }
    
    public void setFechaInicioVigencia(Date fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAFINVIGENCIA", length=7)
    public Date getFechaFinVigencia() {
        return this.fechaFinVigencia;
    }
    
    public void setFechaFinVigencia(Date fechaFinVigencia) {
        this.fechaFinVigencia = fechaFinVigencia;
    }

    @Column(name="DESCRIPCIONCONCEPTO", length=100)
    public String getDescripcionConceptoMovimiento() {
        return this.descripcionConceptoMovimiento;
    }
    
    public void setDescripcionConceptoMovimiento(String descripcionConceptoMovimiento) {
        this.descripcionConceptoMovimiento = descripcionConceptoMovimiento;
    }
    
    @Column(name="DESCRIPCIONTIPOCONTRATO", length=100)
    public String getDescripcionTipoContrato() {
        return this.descripcionTipoContrato;
    }
    
    public void setDescripcionTipoContrato(String descripcionTipoContrato) {
        this.descripcionTipoContrato = descripcionTipoContrato;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMOVIMIENTO", length=7)
    public Date getFechaMovimiento() {
        return this.fechaMovimiento;
    }
    
    public void setFechaMovimiento(Date fechaMovimiento) {
        this.fechaMovimiento = fechaMovimiento;
    }
    
    @Column(name="MESMOVIMIENTO", length=50)
    public String getMesMovimiento() {
        return this.mesMovimiento;
    }
    
    public void setMesMovimiento(String mesMovimiento) {
        this.mesMovimiento = mesMovimiento;
    }
    
    @Column(name="MONTOCESIONSINIESTRO", precision=24, scale=10)
    public BigDecimal getMontoCesionSiniestro() {
        return this.montoCesionSiniestro;
    }
    
    public void setMontoCesionSiniestro(BigDecimal montoCesionSiniestro) {
        this.montoCesionSiniestro = montoCesionSiniestro;
    }
    
    @Column(name="MONTOCOBROS", precision=24, scale=10)
    public BigDecimal getMontoCobros() {
        return this.montoCobros;
    }
    
    public void setMontoCobros(BigDecimal montoCobros) {
        this.montoCobros = montoCobros;
    }
    
    @Column(name="MONTOSALDOPORCOBRAR", precision=24, scale=10)
    public BigDecimal getMontoSaldosPorCobrar() {
        return this.montoSaldosPorCobrar;
    }
    
    public void setMontoSaldosPorCobrar(BigDecimal montoSaldosPorCobrar) {
        this.montoSaldosPorCobrar = montoSaldosPorCobrar;
    }
    
    @Column(name="TIPOCAMBIO", precision=18, scale=4)
    public BigDecimal getTipoCambio() {
        return this.tipoCambio;
    }
    
    public void setTipoCambio(BigDecimal tipoCambio) {
        this.tipoCambio = tipoCambio;
    }
    
    @Column(name="DESCRIPCIONMONEDA", length=100)
    public String getDescripcionMoneda() {
        return this.descripcionMoneda;
    }
    
    public void setDescripcionMoneda(String descripcionMoneda) {
        this.descripcionMoneda = descripcionMoneda;
    }
    
    @Column(name="IDMONEDA")
	public Double getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Double idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name="IDTCTIPOREASEGURO")
	public Short getIdTcTipoReaseguro() {
		return idTcTipoReaseguro;
	}

	public void setIdTcTipoReaseguro(Short idTcTipoReaseguro) {
		this.idTcTipoReaseguro = idTcTipoReaseguro;
	}
}