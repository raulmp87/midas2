package mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Remote;


@Remote
public interface ReporteRCSContraparteFacadeRemote {

	List<ReporteRCSContraparteDTO> obtenerReporte(String fechaCorte, BigDecimal tipoCambio, String nombreUsuario);
	
	void procesarReporte(String fechaCorte, BigDecimal tipoCambio, String nombreUsuario) throws Exception;
	
	/**
	 * Find all ReporteRCSContraparteDTO entities.
	 * 
	 * @return List<ReporteRCSContraparteDTO> all ReporteRCSContraparteDTO entities
	 */
	List<ReporteRCSContraparteDTO> findAll(String fechaCorte);
	
	ReporteRCSContraparteDTO findById(Integer id);
	
	/**
	 * Delete a persistent ReporteRCSContraparteDTO entity.
	 * 
	 * @param entity
	 *            ReporteRCSContraparteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	void delete(ReporteRCSContraparteDTO entity);
	
	/**
	 * Find all CargaContraParteDTO entities.
	 * 
	 * @return List<CargaContraParteDTO> all CargaContraParteDTO entities
	 */
	List<CargaContraParteDTO> findAllCarga(String fechaCorte);
	
	int obtenerRegistros(Date fechaCorte);
	
	void actualizarEstatusCarga(int idCarga, int estatusCarga);	
	
	List<String> obtenerArchivos(Date fechaFinal) throws Exception;
	
	List<Object[]> obtenerDesgloce(Date fechaFinal, BigDecimal tipoCambio) throws Exception;
	
	List<Object[]> obtenerIntegracion(Date fechaFinal, BigDecimal tipoCambio, String concepto) throws Exception;
	
	List<Object[]> obtenerIntegracionNRS(Date fechaFinal, BigDecimal tipoCambio) throws Exception;
}
