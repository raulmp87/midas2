package mx.com.afirme.midas.reaseguro.reportes.saldos.poliza;
// default package

import java.util.Date;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ReporteSaldosPolizaDTOFacade.
 * @author Jose Luis Arellano
 */

public interface ReporteSaldosPolizaFacadeRemote {
	
	public ReporteSaldosPolizaDTO findById( Double id);
	 /**
	 * Find all ReporteSaldosPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReporteSaldosPolizaDTO property to query
	  @param value the property value to match
	  	  @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the  the row index in the query result-set to begin collecting the results. rowStartIdxAndCount[1] specifies the the maximum count of results to return.  
	  	  @return List<ReporteSaldosPolizaDTO> found by query
	 */
	public List<ReporteSaldosPolizaDTO> findByProperty(String propertyName, Object value
			, int...rowStartIdxAndCount
		);
	/**
	 * Find all ReporteSaldosPolizaDTO entities.
	  	  @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the  the row index in the query result-set to begin collecting the results. rowStartIdxAndCount[1] specifies the the maximum count of results to return.  
	  	  @return List<ReporteSaldosPolizaDTO> all ReporteSaldosPolizaDTO entities
	 */
	public List<ReporteSaldosPolizaDTO> findAll(
			int...rowStartIdxAndCount
		);	
	
    public List<ReporteSaldosPolizaDTO> consultarSaldosPoliza(Date fechaInicial,Date fechaFinal,boolean soloFacultativos,boolean soloAutomaticos,boolean incluirRetencion,
    		Double idMoneda, final int...rowStartIdxAndCount);
}