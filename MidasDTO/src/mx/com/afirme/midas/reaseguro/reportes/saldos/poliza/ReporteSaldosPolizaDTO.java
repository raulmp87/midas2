package mx.com.afirme.midas.reaseguro.reportes.saldos.poliza;
// default package

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * ReporteSaldosPolizaDTO entity. @author Jose Luis Arellano
 */
@Entity
@Table(name="TRPTSALDOSPOLIZA",schema="MIDAS")
public class ReporteSaldosPolizaDTO  implements java.io.Serializable {
	private static final long serialVersionUID = 5297429061106073259L;
	private Double idReporteSaldosPolizaDTO;
    private String nombreReasegurador;
    private String nombreCorredor;
    private String numeroPoliza;
    private Short numeroEndoso;
    private String tipoEndoso;
    private Date fechaEmision;
    private Date fechaInicioVigencia;
    private Date fechaFinVigencia;
    private String nombreAsegurado;
    private String codigoSubRamo;
    private Short numeroInciso;
    private Short idTcTipoReaseguro;
    private String descripcionTipoContrato;
    private Date fechaMovimiento;
    private String mesMovimiento;
    private BigDecimal totalPrimaCedida;
    private BigDecimal totalComision;
    private BigDecimal totalPrimaMenosComision;
    private BigDecimal totalIngresosBonoPorNoSiniestro;
    private BigDecimal totalRetencionImpuestos;
    private BigDecimal totalPagos;
    private BigDecimal totalIngresosPrimaNoDevengada;
    private BigDecimal saldoTotal;
    private BigDecimal tipoCambio;
    private Double idMoneda;
    private String descripcionMoneda;


    // Constructors

    /** default constructor */
    public ReporteSaldosPolizaDTO() {
    }

	/** minimal constructor */
    public ReporteSaldosPolizaDTO(Double idReporteSaldosPolizaDTO) {
        this.idReporteSaldosPolizaDTO = idReporteSaldosPolizaDTO;
    }

   
    // Property accessors
    @Id 
    @Column(name="IDTRPTSALDOSPOLIZA", nullable=false, precision=8, scale=0)
    public Double getIdReporteSaldosPolizaDTO() {
        return this.idReporteSaldosPolizaDTO;
    }
    
    public void setIdReporteSaldosPolizaDTO(Double idReporteSaldosPolizaDTO) {
        this.idReporteSaldosPolizaDTO = idReporteSaldosPolizaDTO;
    }
    
    @Column(name="NOMBREREASEGURADOR", length=100)
    public String getNombreReasegurador() {
        return this.nombreReasegurador;
    }
    
    public void setNombreReasegurador(String nombreReasegurador) {
        this.nombreReasegurador = nombreReasegurador;
    }
    
    @Column(name="NOMBRECORREDOR", length=100)

    public String getNombreCorredor() {
        return this.nombreCorredor;
    }
    
    public void setNombreCorredor(String nombreCorredor) {
        this.nombreCorredor = nombreCorredor;
    }
    
    @Column(name="NUMEROPOLIZA", length=20)

    public String getNumeroPoliza() {
        return this.numeroPoliza;
    }
    
    public void setNumeroPoliza(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }
    
    @Column(name="NUMEROENDOSO", precision=4, scale=0)

    public Short getNumeroEndoso() {
        return this.numeroEndoso;
    }
    
    public void setNumeroEndoso(Short numeroEndoso) {
        this.numeroEndoso = numeroEndoso;
    }
    
    @Column(name="TIPOENDOSO", length=50)

    public String getTipoEndoso() {
        return this.tipoEndoso;
    }
    
    public void setTipoEndoso(String tipoEndoso) {
        this.tipoEndoso = tipoEndoso;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAEMISION", length=7)

    public Date getFechaEmision() {
        return this.fechaEmision;
    }
    
    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAINICIOVIGENCIA", length=7)

    public Date getFechaInicioVigencia() {
        return this.fechaInicioVigencia;
    }
    
    public void setFechaInicioVigencia(Date fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAFINVIGENCIA", length=7)

    public Date getFechaFinVigencia() {
        return this.fechaFinVigencia;
    }
    
    public void setFechaFinVigencia(Date fechaFinVigencia) {
        this.fechaFinVigencia = fechaFinVigencia;
    }
    
    @Column(name="NOMBREASEGURADO", length=240)

    public String getNombreAsegurado() {
        return this.nombreAsegurado;
    }
    
    public void setNombreAsegurado(String nombreAsegurado) {
        this.nombreAsegurado = nombreAsegurado;
    }
    
    @Column(name="CODIGOSUBRAMO", length=10)

    public String getCodigoSubRamo() {
        return this.codigoSubRamo;
    }
    
    public void setCodigoSubRamo(String codigoSubRamo) {
        this.codigoSubRamo = codigoSubRamo;
    }
    
    @Column(name="NUMEROINCISO", precision=22, scale=0)
    public Short getNumeroInciso() {
        return this.numeroInciso;
    }
    
    public void setNumeroInciso(Short numeroInciso) {
        this.numeroInciso = numeroInciso;
    }
    
    @Column(name="DESCRIPCIONTIPOCONTRATO", length=100)
    public String getDescripcionTipoContrato() {
        return this.descripcionTipoContrato;
    }
    
    public void setDescripcionTipoContrato(String descripcionTipoContrato) {
        this.descripcionTipoContrato = descripcionTipoContrato;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMOVIMIENTO", length=7)
    public Date getFechaMovimiento() {
        return this.fechaMovimiento;
    }
    
    public void setFechaMovimiento(Date fechaMovimiento) {
        this.fechaMovimiento = fechaMovimiento;
    }
    
    @Column(name="MESMOVIMIENTO", length=50)
    public String getMesMovimiento() {
        return this.mesMovimiento;
    }
    
    public void setMesMovimiento(String mesMovimiento) {
        this.mesMovimiento = mesMovimiento;
    }
    
    @Column(name="TOTALPRIMACEDIDA", precision=24, scale=10)
    public BigDecimal getTotalPrimaCedida() {
        return this.totalPrimaCedida;
    }
    
    public void setTotalPrimaCedida(BigDecimal totalPrimaCedida) {
        this.totalPrimaCedida = totalPrimaCedida;
    }
    
    @Column(name="TOTALCOMISION", precision=24, scale=10)
    public BigDecimal getTotalComision() {
        return this.totalComision;
    }
    
    public void setTotalComision(BigDecimal totalComision) {
        this.totalComision = totalComision;
    }
    
    @Column(name="TOTALPRIMAMENOSCOMISION", precision=24, scale=10)
    public BigDecimal getTotalPrimaMenosComision() {
        return this.totalPrimaMenosComision;
    }
    
    public void setTotalPrimaMenosComision(BigDecimal totalPrimaMenosComision) {
        this.totalPrimaMenosComision = totalPrimaMenosComision;
    }
    
    @Column(name="TOTALINGRPORNOSINIESTRO", precision=24, scale=10)
    public BigDecimal getTotalIngresosBonoPorNoSiniestro() {
        return this.totalIngresosBonoPorNoSiniestro;
    }
    
    public void setTotalIngresosBonoPorNoSiniestro(BigDecimal totalIngresosBonoPorNoSiniestro) {
        this.totalIngresosBonoPorNoSiniestro = totalIngresosBonoPorNoSiniestro;
    }
    
    @Column(name="TOTALRETENCIONIMPUESTOS", precision=24, scale=10)
    public BigDecimal getTotalRetencionImpuestos() {
        return this.totalRetencionImpuestos;
    }
    
    public void setTotalRetencionImpuestos(BigDecimal totalRetencionImpuestos) {
        this.totalRetencionImpuestos = totalRetencionImpuestos;
    }
    
    @Column(name="TOTALPAGOS", precision=24, scale=10)
    public BigDecimal getTotalPagos() {
        return this.totalPagos;
    }
    
    public void setTotalPagos(BigDecimal totalPagos) {
        this.totalPagos = totalPagos;
    }
    
    @Column(name="TOTALINGRPRIMANODEVENGADA", precision=24, scale=10)
    public BigDecimal getTotalIngresosPrimaNoDevengada() {
        return this.totalIngresosPrimaNoDevengada;
    }
    
    public void setTotalIngresosPrimaNoDevengada(BigDecimal totalIngresosPrimaNoDevengada) {
        this.totalIngresosPrimaNoDevengada = totalIngresosPrimaNoDevengada;
    }
    
    @Column(name="SALDOTOTAL", precision=24, scale=10)
    public BigDecimal getSaldoTotal() {
        return this.saldoTotal;
    }
    
    public void setSaldoTotal(BigDecimal saldoTotal) {
        this.saldoTotal = saldoTotal;
    }
    
    @Column(name="TIPOCAMBIO", precision=18, scale=4)
    public BigDecimal getTipoCambio() {
        return this.tipoCambio;
    }
    
    public void setTipoCambio(BigDecimal tipoCambio) {
        this.tipoCambio = tipoCambio;
    }
    
    @Column(name="DESCRIPCIONMONEDA", length=100)
    public String getDescripcionMoneda() {
        return this.descripcionMoneda;
    }
    
    public void setDescripcionMoneda(String descripcionMoneda) {
        this.descripcionMoneda = descripcionMoneda;
    }

    @Column(name="IDMONEDA")
	public Double getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Double idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name="IDTCTIPOREASEGURO")
	public Short getIdTcTipoReaseguro() {
		return idTcTipoReaseguro;
	}

	public void setIdTcTipoReaseguro(Short idTcTipoReaseguro) {
		this.idTcTipoReaseguro = idTcTipoReaseguro;
	}
}