package mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreasegurador;

import java.util.List;

import javax.ejb.Remote;


public interface MovimientoPrimaReaseguradorFacadeRemote {

	public List<MovimientoPrimaReaseguradorDTO> obtenerMovimientosPrimaPorReasegurador(MovimientoPrimaReaseguradorDTO movimientoPrimaReasegurador,String nombreUsuario) throws Exception;
}
