package mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * @author Paulo dos Santos
 */
@Entity
@Table(name="CNSF_SIN_MIDAS_DETALLE", schema="MIDAS")
public class DistribucionMidasDTO implements Serializable {

	private static final long serialVersionUID = 2299753824789845932L;
	
	private Integer id_detalle;
	private BigDecimal reservaInicial;
	private BigDecimal ajustemas;
	private BigDecimal ajustemenos;
	private BigDecimal indemnizacion;
	private BigDecimal reservaPendiente;
	private BigDecimal porcentajeCon;
	private BigDecimal fPorParticipacion;
	private BigDecimal reserva_pendiente_reas;
	private String reasegurador;
	private String cnsf;
	private String moneda;
	private Integer siniestro;
	private Date fCorte;
	
	/** default constructor */
    public DistribucionMidasDTO() {
    }

    @Id
    @SequenceGenerator(name = "ID_DISTRMIDAS_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.CNSF_SIN_MIDAS_DETALLE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_DISTRMIDAS_SEQ_GENERADOR")
    @Column(name="ID_DETALLE", unique=true, nullable=false, precision=22, scale=0)
	public Integer getId_detalle() {
		return id_detalle;
	}

	public void setId_detalle(Integer id_detalle) {
		this.id_detalle = id_detalle;
	}
	
	@Column(name="RESERVAINICIAL", nullable=false)
	public BigDecimal getReservaInicial() {
		return reservaInicial;
	}

	public void setReservaInicial(BigDecimal reservaInicial) {
		this.reservaInicial = reservaInicial;
	}

	@Column(name="AJUSTEMAS", nullable=false)
	public BigDecimal getAjustemas() {
		return ajustemas;
	}

	public void setAjustemas(BigDecimal ajustemas) {
		this.ajustemas = ajustemas;
	}

	@Column(name="AJUSTEMENOS", nullable=false)
	public BigDecimal getAjustemenos() {
		return ajustemenos;
	}

	public void setAjustemenos(BigDecimal ajustemenos) {
		this.ajustemenos = ajustemenos;
	}

	@Column(name="INDEMNIZACION", nullable=false)
	public BigDecimal getIndemnizacion() {
		return indemnizacion;
	}

	public void setIndemnizacion(BigDecimal indemnizacion) {
		this.indemnizacion = indemnizacion;
	}

	@Column(name="RESERVAPENDIENTE", nullable=false)
	public BigDecimal getReservaPendiente() {
		return reservaPendiente;
	}

	public void setReservaPendiente(BigDecimal reservaPendiente) {
		this.reservaPendiente = reservaPendiente;
	}

	@Column(name="PORCENTAJECON", nullable=false)
	public BigDecimal getPorcentajeCon() {
		return porcentajeCon;
	}

	public void setPorcentajeCon(BigDecimal porcentajeCon) {
		this.porcentajeCon = porcentajeCon;
	}

	@Column(name="FPORPARTICIPACION", nullable=false)
	public BigDecimal getfPorParticipacion() {
		return fPorParticipacion;
	}

	public void setfPorParticipacion(BigDecimal fPorParticipacion) {
		this.fPorParticipacion = fPorParticipacion;
	}

	@Column(name="RESERVA_PENDIENTE_REAS", nullable=false)
	public BigDecimal getReserva_pendiente_reas() {
		return reserva_pendiente_reas;
	}

	public void setReserva_pendiente_reas(BigDecimal reserva_pendiente_reas) {
		this.reserva_pendiente_reas = reserva_pendiente_reas;
	}

	@Column(name="REASEGURADOR", nullable=false)
	public String getReasegurador() {
		return reasegurador;
	}

	public void setReasegurador(String reasegurador) {
		this.reasegurador = reasegurador;
	}

	@Column(name="CNSF", nullable=false)
	public String getCnsf() {
		return cnsf;
	}

	public void setCnsf(String cnsf) {
		this.cnsf = cnsf;
	}

	@Column(name="MONEDA", nullable=false)
	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	@Column(name="SINIESTRO", nullable=false)
	public Integer getSiniestro() {
		return siniestro;
	}

	public void setSiniestro(Integer siniestro) {
		this.siniestro = siniestro;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FCORTE", nullable=false, length=10)
	public Date getfCorte() {
		return fCorte;
	}

	public void setfCorte(Date fCorte) {
		this.fCorte = fCorte;
	}	
}