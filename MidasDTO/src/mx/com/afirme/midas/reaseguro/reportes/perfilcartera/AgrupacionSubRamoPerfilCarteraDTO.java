package mx.com.afirme.midas.reaseguro.reportes.perfilcartera;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author jose luis arellano
 */
public class AgrupacionSubRamoPerfilCarteraDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer idTcSubRamo;
	private String descripcionSubRamo;
	
	private BigDecimal sumatoriaSumaAsegurada;
	private BigDecimal sumatoriaPrimaNeta;
	private BigDecimal porcentajePrimaNeta;
	
	private Integer totalCumulos;
	private BigDecimal porcentajeCumulos;
	
	private Integer totalPolizas;
	private BigDecimal promedioSumaAsegurada;
	private BigDecimal promedioPrimaNeta;
	
	private Integer totalSiniestros;
	private BigDecimal porcentajeCantidadSiniestros;
	private BigDecimal sumatoriaMontoSiniestros;
	private BigDecimal porcentajeMontoSiniestros;
	
	private BigDecimal frecuencia;
	private BigDecimal siniestralidad;
	private BigDecimal costoPromedio;
	private BigDecimal severidad;
	
	private List<RegistroPerfilCarteraDTO> listaRangosSubRamo;
	
	public AgrupacionSubRamoPerfilCarteraDTO(Integer idTcSubRamo,String descripcionSubRamo) {
		listaRangosSubRamo = new ArrayList<RegistroPerfilCarteraDTO>();
		this.idTcSubRamo = idTcSubRamo;
		this.descripcionSubRamo = descripcionSubRamo;
		sumatoriaSumaAsegurada = BigDecimal.ZERO;
		sumatoriaPrimaNeta = BigDecimal.ZERO;
		porcentajePrimaNeta = new BigDecimal("100");
	}
	
	public BigDecimal getSumatoriaSumaAsegurada() {
		return sumatoriaSumaAsegurada;
	}

	public void setSumatoriaSumaAsegurada(BigDecimal sumatoriaSumaAsegurada) {
		this.sumatoriaSumaAsegurada = sumatoriaSumaAsegurada;
	}

	public BigDecimal getSumatoriaPrimaNeta() {
		return sumatoriaPrimaNeta;
	}

	public void setSumatoriaPrimaNeta(BigDecimal sumatoriaPrimaNeta) {
		this.sumatoriaPrimaNeta = sumatoriaPrimaNeta;
	}

	public BigDecimal getPorcentajePrimaNeta() {
		return porcentajePrimaNeta;
	}

	public void setPorcentajePrimaNeta(BigDecimal porcentajePrimaNeta) {
		this.porcentajePrimaNeta = porcentajePrimaNeta;
	}

	public Integer getTotalPolizas() {
		return totalPolizas;
	}

	public void setTotalPolizas(Integer totalPolizas) {
		this.totalPolizas = totalPolizas;
	}

	public BigDecimal getPromedioSumaAsegurada() {
		return promedioSumaAsegurada;
	}

	public void setPromedioSumaAsegurada(BigDecimal promedioSumaAsegurada) {
		this.promedioSumaAsegurada = promedioSumaAsegurada;
	}

	public BigDecimal getPromedioPrimaNeta() {
		return promedioPrimaNeta;
	}

	public void setPromedioPrimaNeta(BigDecimal promedioPrimaNeta) {
		this.promedioPrimaNeta = promedioPrimaNeta;
	}

	public Integer getTotalSiniestros() {
		return totalSiniestros;
	}

	public void setTotalSiniestros(Integer totalSiniestros) {
		this.totalSiniestros = totalSiniestros;
	}

	public BigDecimal getSumatoriaMontoSiniestros() {
		return sumatoriaMontoSiniestros;
	}

	public void setSumatoriaMontoSiniestros(BigDecimal sumatoriaMontoSiniestros) {
		this.sumatoriaMontoSiniestros = sumatoriaMontoSiniestros;
	}

	public BigDecimal getPorcentajeMontoSiniestros() {
		return porcentajeMontoSiniestros;
	}

	public void setPorcentajeMontoSiniestros(BigDecimal porcentajeMontoSiniestros) {
		this.porcentajeMontoSiniestros = porcentajeMontoSiniestros;
	}

	public List<RegistroPerfilCarteraDTO> getListaRangosSubRamo() {
		return listaRangosSubRamo;
	}

	public void setListaRangosSubRamo(List<RegistroPerfilCarteraDTO> listaRangosSubRamo) {
		this.listaRangosSubRamo = listaRangosSubRamo;
	}

	public Integer getIdTcSubRamo() {
		return idTcSubRamo;
	}

	public void setIdTcSubRamo(Integer idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}

	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}

	public BigDecimal getPorcentajeCumulos() {
		return porcentajeCumulos;
	}

	public void setPorcentajeCumulos(BigDecimal porcentajeCumulos) {
		this.porcentajeCumulos = porcentajeCumulos;
	}

	public Integer getTotalCumulos() {
		return totalCumulos;
	}

	public void setTotalCumulos(Integer totalCumulos) {
		this.totalCumulos = totalCumulos;
	}

	public BigDecimal getPorcentajeCantidadSiniestros() {
		return porcentajeCantidadSiniestros;
	}

	public void setPorcentajeCantidadSiniestros(BigDecimal porcentajeCantidadSiniestros) {
		this.porcentajeCantidadSiniestros = porcentajeCantidadSiniestros;
	}

	public BigDecimal getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia(BigDecimal frecuencia) {
		this.frecuencia = frecuencia;
	}

	public BigDecimal getSiniestralidad() {
		return siniestralidad;
	}

	public void setSiniestralidad(BigDecimal siniestralidad) {
		this.siniestralidad = siniestralidad;
	}

	public BigDecimal getCostoPromedio() {
		return costoPromedio;
	}

	public void setCostoPromedio(BigDecimal costoPromedio) {
		this.costoPromedio = costoPromedio;
	}

	public BigDecimal getSeveridad() {
		return severidad;
	}

	public void setSeveridad(BigDecimal severidad) {
		this.severidad = severidad;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idTcSubRamo == null) ? 0 : idTcSubRamo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgrupacionSubRamoPerfilCarteraDTO other = (AgrupacionSubRamoPerfilCarteraDTO) obj;
		if (idTcSubRamo == null) {
			if (other.idTcSubRamo != null)
				return false;
		} else if (!idTcSubRamo.equals(other.idTcSubRamo))
			return false;
		return true;
	}
	
}
