package mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * @author Paulo dos Santos
 */
@Entity
@Table(name="CNSF_SIN_ESTATUS", schema="MIDAS")
public class ReporteRCSContraparteDTO implements Serializable {

	private static final long serialVersionUID = 2299753824789845932L;
	
	private Integer idtcontrato;
	private Date fechaCorte;
	private Date iniEjecucion;
	private Date finEjecucion;
	private String estatus;
	private String reporte;
	private String resultado;
	
	/** default constructor */
    public ReporteRCSContraparteDTO() {
    }
	
    @Id
    @SequenceGenerator(name = "IDCONTRATO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.CNSF_RR6_ESTATUS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCONTRATO_SEQ_GENERADOR")
    @Column(name="IDCONTRATO", unique=true, nullable=false, precision=22, scale=0)
    public Integer getIdcontrato() {
        return this.idtcontrato;
    }
    
    public void setIdcontrato(Integer idtcontrato) {
        this.idtcontrato = idtcontrato;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACORTE", nullable=false, length=10)
	public Date getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	
	@Temporal(TemporalType.DATE)
    @Column(name="INIEJECUCION", nullable=false, length=10)
	public Date getIniEjecucion() {
		return iniEjecucion;
	}
	public void setIniEjecucion(Date iniEjecucion) {
		this.iniEjecucion = iniEjecucion;
	}
	
	@Temporal(TemporalType.DATE)
    @Column(name="FINEJECUCION", nullable=false, length=10)
	public Date getFinEjecucion() {
		return finEjecucion;
	}
	public void setFinEjecucion(Date finEjecucion) {
		this.finEjecucion = finEjecucion;
	}
	
	@Column(name="ESTATUS", nullable=false, length=40)
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	@Column(name="REPORTE", nullable=false, length=40)
	public String getReporte() {
		return reporte;
	}
	public void setReporte(String reporte) {
		this.reporte = reporte;
	}
	
	@Transient
	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}	
	
	
}
