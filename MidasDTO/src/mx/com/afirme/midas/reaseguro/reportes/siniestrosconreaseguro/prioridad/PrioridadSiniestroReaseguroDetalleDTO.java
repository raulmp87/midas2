package mx.com.afirme.midas.reaseguro.reportes.siniestrosconreaseguro.prioridad;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;



/**
 * PrioridadSiniestroReaseguroDetalleDTO entity. @author MyEclipse Persistence Tools
 */


@Entity
@Table(name="TDPRIORIDAD",schema="MIDAS")

public class PrioridadSiniestroReaseguroDetalleDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 //Fields
	private BigDecimal idtdprioridad;
    private RamoDTO ramoDTO;
    private SubRamoDTO subRamoDTO;
    private PrioridadSiniestroReaseguroDTO prioridadSiniestroReaseguroDTO;
    private Double prioridad;
    private BigDecimal idtcmoneda;
    private BigDecimal tipocontrato;
	
	


    // Constructors

    /** default constructor */
    public PrioridadSiniestroReaseguroDetalleDTO() {
    }

	/** minimal constructor */
    public PrioridadSiniestroReaseguroDetalleDTO(BigDecimal idtdprioridad) {
        this.idtdprioridad = idtdprioridad;
    }
    
    /** full constructor */
    public PrioridadSiniestroReaseguroDetalleDTO(BigDecimal idtdprioridad, RamoDTO tcramo, SubRamoDTO tcsubramo, PrioridadSiniestroReaseguroDTO prioridadSiniestroReaseguroDTO, Double prioridad, BigDecimal idtcmoneda, BigDecimal tipocontrato) {
        this.idtdprioridad = idtdprioridad;
        this.ramoDTO = tcramo;
        this.subRamoDTO = tcsubramo;
        this.prioridadSiniestroReaseguroDTO = prioridadSiniestroReaseguroDTO;
        this.prioridad = prioridad;
        this.idtcmoneda = idtcmoneda;
        this.tipocontrato = tipocontrato;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="IDTDPRIORIDAD", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtdprioridad() {
        return this.idtdprioridad;
    }
    
    public void setIdtdprioridad(BigDecimal idtdprioridad) {
        this.idtdprioridad = idtdprioridad;
    }
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDTCRAMO")
    public RamoDTO getRamoDTO() {
        return this.ramoDTO;
    }
    
    public void setRamoDTO(RamoDTO ramoDTO) {
        this.ramoDTO = ramoDTO;
    }
	
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCSUBRAMO", nullable = false)    
    public SubRamoDTO getSubRamoDTO() {
        return this.subRamoDTO;
    }
    
    public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
        this.subRamoDTO = subRamoDTO;
    }
	
  
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDTMPRIORIDAD")

	public PrioridadSiniestroReaseguroDTO getPrioridadSiniestroReaseguroDTO() {
	    return this.prioridadSiniestroReaseguroDTO;
	}
	
	public void setPrioridadSiniestroReaseguroDTO(PrioridadSiniestroReaseguroDTO prioridadSiniestroReaseguroDTO) {
	    this.prioridadSiniestroReaseguroDTO = prioridadSiniestroReaseguroDTO;
	}
    
    @Column(name="PRIORIDAD", precision=18, scale=4)

    public Double getPrioridad() {
        return this.prioridad;
    }
    
    public void setPrioridad(Double prioridad) {
        this.prioridad = prioridad;
    }
    
    @Column(name="IDTCMONEDA", precision=22, scale=0)

    public BigDecimal getIdtcmoneda() {
        return this.idtcmoneda;
    }
    
    public void setIdtcmoneda(BigDecimal idtcmoneda) {
        this.idtcmoneda = idtcmoneda;
    }
    
    @Column(name="TIPOCONTRATO", precision=22, scale=0)

    public BigDecimal getTipocontrato() {
        return this.tipocontrato;
    }
    
    public void setTipocontrato(BigDecimal tipocontrato) {
        this.tipocontrato = tipocontrato;
    }
   








}