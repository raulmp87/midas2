package mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreaseguradordetalle;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class MovimientoPrimaReaseguradorDetalleDTO implements Serializable{
	private static final long serialVersionUID = -3018727739167741866L;

	private String codigoSubRamo;
	private String folioPoliza;
	private Short numeroEndoso;
	private String tipoEndoso;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private Integer numeroInciso;
	private Integer numeroSubInciso;
	private Double porcentajeRetencion;
	private Double porcentajeCuotaParte;
	private Double porcentajePrimerExcedente;
	private Double porcentajeFacultativo;
	private Double montoPrimaNeta;
	private String folioContrato;
	private String tipoReaseguro;
	private String nombreCuentaDe;
	private Double porcentajeParticipacion;
	private Double porcentajeComision;
	private String corredor;
	private Double porcentajeParticipacionCorredor;
	private Double porcentajeComisionCorredor;
	private String ambito;
	private Double montoPrimas;
	private Double montoComisiones;
	private String descripcionMoneda;
	private Date fechaMovto;
	private String nombreSucursal;
	private Integer idAsegurado;
	private String nombreAsegurado;
	private String monedaReporte;
	private String folioPolizaReporte;
	private Double montoSumaAsegurada;
	private Double montoSumaAseguradaReaseguro;
	private Double montoSumaAseguradaDistribuida;
	private Date fechaEmisionPoliza;
	private String seccion;
	private String cobertura;
	private String registroCNSF;
	private Double porcentajePleno;
	private String nombreOficina;
	private String claveGerencia;
	private String gerencia;
	private String claveOficina;
	private String claveSupervisoria;
	private String supervisoria;
	private Integer idTipoReaseguro;
	private String folioPolizaAnterior;
	private Double montoPrimaRetenida;
	private Double montoPrimaCuotaParte;
	private Double montoPrimaPrimerExcedente;
	private Double montoPrimaFacultativo;
	private Double montoPrimasRetenidas;
	private String identificador;
	
	/**
	 * Variables utilizadas como datos de entrada para el SP:
	 */
	private BigDecimal idToPoliza;
	private Integer idMoneda;
	private Integer IdRamo;
	private Integer IdSubRamo;
	private Integer retencion;// 0=sin retención, 1 = con retencion
	public String getCodigoSubRamo() {
		return codigoSubRamo;
	}
	public void setCodigoSubRamo(String codigoSubRamo) {
		this.codigoSubRamo = codigoSubRamo;
	}
	public String getFolioPoliza() {
		return folioPoliza;
	}
	public void setFolioPoliza(String folioPoliza) {
		this.folioPoliza = folioPoliza;
	}
	public Short getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Short numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public String getTipoEndoso() {
		return tipoEndoso;
	}
	public void setTipoEndoso(String tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public Integer getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(Integer numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	public Double getPorcentajeRetencion() {
		return porcentajeRetencion;
	}
	public void setPorcentajeRetencion(Double porcentajeRetencion) {
		this.porcentajeRetencion = porcentajeRetencion;
	}
	public Double getPorcentajeCuotaParte() {
		return porcentajeCuotaParte;
	}
	public void setPorcentajeCuotaParte(Double porcentajeCuotaParte) {
		this.porcentajeCuotaParte = porcentajeCuotaParte;
	}
	public Double getPorcentajePrimerExcedente() {
		return porcentajePrimerExcedente;
	}
	public void setPorcentajePrimerExcedente(Double porcentajePrimerExcedente) {
		this.porcentajePrimerExcedente = porcentajePrimerExcedente;
	}
	public Double getPorcentajeFacultativo() {
		return porcentajeFacultativo;
	}
	public void setPorcentajeFacultativo(Double porcentajeFacultativo) {
		this.porcentajeFacultativo = porcentajeFacultativo;
	}
	public Double getMontoPrimaNeta() {
		return montoPrimaNeta;
	}
	public void setMontoPrimaNeta(Double montoPrimaNeta) {
		this.montoPrimaNeta = montoPrimaNeta;
	}
	public String getFolioContrato() {
		return folioContrato;
	}
	public void setFolioContrato(String folioContrato) {
		this.folioContrato = folioContrato;
	}
	public String getTipoReaseguro() {
		return tipoReaseguro;
	}
	public void setTipoReaseguro(String tipoReaseguro) {
		this.tipoReaseguro = tipoReaseguro;
	}
	public String getNombreCuentaDe() {
		return nombreCuentaDe;
	}
	public void setNombreCuentaDe(String nombreCuentaDe) {
		this.nombreCuentaDe = nombreCuentaDe;
	}
	public Double getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	public void setPorcentajeParticipacion(Double porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	public Double getPorcentajeComision() {
		return porcentajeComision;
	}
	public void setPorcentajeComision(Double porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}
	public String getCorredor() {
		return corredor;
	}
	public void setCorredor(String corredor) {
		this.corredor = corredor;
	}
	public Double getPorcentajeParticipacionCorredor() {
		return porcentajeParticipacionCorredor;
	}
	public void setPorcentajeParticipacionCorredor(
			Double porcentajeParticipacionCorredor) {
		this.porcentajeParticipacionCorredor = porcentajeParticipacionCorredor;
	}
	public Double getPorcentajeComisionCorredor() {
		return porcentajeComisionCorredor;
	}
	public void setPorcentajeComisionCorredor(Double porcentajeComisionCorredor) {
		this.porcentajeComisionCorredor = porcentajeComisionCorredor;
	}
	public String getAmbito() {
		return ambito;
	}
	public void setAmbito(String ambito) {
		this.ambito = ambito;
	}
	public Double getMontoPrimas() {
		return montoPrimas;
	}
	public void setMontoPrimas(Double montoPrimas) {
		this.montoPrimas = montoPrimas;
	}
	public Double getMontoComisiones() {
		return montoComisiones;
	}
	public void setMontoComisiones(Double montoComisiones) {
		this.montoComisiones = montoComisiones;
	}
	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}
	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}
	public Date getFechaMovto() {
		return fechaMovto;
	}
	public void setFechaMovto(Date fechaMovto) {
		this.fechaMovto = fechaMovto;
	}
	public String getNombreSucursal() {
		return nombreSucursal;
	}
	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}
	public Integer getIdAsegurado() {
		return idAsegurado;
	}
	public void setIdAsegurado(Integer idAsegurado) {
		this.idAsegurado = idAsegurado;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	public String getMonedaReporte() {
		return monedaReporte;
	}
	public void setMonedaReporte(String monedaReporte) {
		this.monedaReporte = monedaReporte;
	}
	public String getFolioPolizaReporte() {
		return folioPolizaReporte;
	}
	public void setFolioPolizaReporte(String folioPolizaReporte) {
		this.folioPolizaReporte = folioPolizaReporte;
	}
	public Double getMontoSumaAsegurada() {
		return montoSumaAsegurada;
	}
	public void setMontoSumaAsegurada(Double montoSumaAsegurada) {
		this.montoSumaAsegurada = montoSumaAsegurada;
	}
	public Double getMontoSumaAseguradaReaseguro() {
		return montoSumaAseguradaReaseguro;
	}
	public void setMontoSumaAseguradaReaseguro(Double montoSumaAseguradaReaseguro) {
		this.montoSumaAseguradaReaseguro = montoSumaAseguradaReaseguro;
	}
	public Double getMontoSumaAseguradaDistribuida() {
		return montoSumaAseguradaDistribuida;
	}
	public void setMontoSumaAseguradaDistribuida(
			Double montoSumaAseguradaDistribuida) {
		this.montoSumaAseguradaDistribuida = montoSumaAseguradaDistribuida;
	}
	public Date getFechaEmisionPoliza() {
		return fechaEmisionPoliza;
	}
	public void setFechaEmisionPoliza(Date fechaEmisionPoliza) {
		this.fechaEmisionPoliza = fechaEmisionPoliza;
	}
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	public String getCobertura() {
		return cobertura;
	}
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	public String getRegistroCNSF() {
		return registroCNSF;
	}
	public void setRegistroCNSF(String registroCNSF) {
		this.registroCNSF = registroCNSF;
	}
	public Double getPorcentajePleno() {
		return porcentajePleno;
	}
	public void setPorcentajePleno(Double porcentajePleno) {
		this.porcentajePleno = porcentajePleno;
	}
	public String getNombreOficina() {
		return nombreOficina;
	}
	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}
	public String getClaveGerencia() {
		return claveGerencia;
	}
	public void setClaveGerencia(String claveGerencia) {
		this.claveGerencia = claveGerencia;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getClaveOficina() {
		return claveOficina;
	}
	public void setClaveOficina(String claveOficina) {
		this.claveOficina = claveOficina;
	}
	public String getClaveSupervisoria() {
		return claveSupervisoria;
	}
	public void setClaveSupervisoria(String claveSupervisoria) {
		this.claveSupervisoria = claveSupervisoria;
	}
	public String getSupervisoria() {
		return supervisoria;
	}
	public void setSupervisoria(String supervisoria) {
		this.supervisoria = supervisoria;
	}
	public Integer getIdTipoReaseguro() {
		return idTipoReaseguro;
	}
	public void setIdTipoReaseguro(Integer idTipoReaseguro) {
		this.idTipoReaseguro = idTipoReaseguro;
	}
	public String getFolioPolizaAnterior() {
		return folioPolizaAnterior;
	}
	public void setFolioPolizaAnterior(String folioPolizaAnterior) {
		this.folioPolizaAnterior = folioPolizaAnterior;
	}
	public Double getMontoPrimaRetenida() {
		return montoPrimaRetenida;
	}
	public void setMontoPrimaRetenida(Double montoPrimaRetenida) {
		this.montoPrimaRetenida = montoPrimaRetenida;
	}
	public Double getMontoPrimaCuotaParte() {
		return montoPrimaCuotaParte;
	}
	public void setMontoPrimaCuotaParte(Double montoPrimaCuotaParte) {
		this.montoPrimaCuotaParte = montoPrimaCuotaParte;
	}
	public Double getMontoPrimaPrimerExcedente() {
		return montoPrimaPrimerExcedente;
	}
	public void setMontoPrimaPrimerExcedente(Double montoPrimaPrimerExcedente) {
		this.montoPrimaPrimerExcedente = montoPrimaPrimerExcedente;
	}
	public Double getMontoPrimaFacultativo() {
		return montoPrimaFacultativo;
	}
	public void setMontoPrimaFacultativo(Double montoPrimaFacultativo) {
		this.montoPrimaFacultativo = montoPrimaFacultativo;
	}
	public Double getMontoPrimasRetenidas() {
		return montoPrimasRetenidas;
	}
	public void setMontoPrimasRetenidas(Double montoPrimasRetenidas) {
		this.montoPrimasRetenidas = montoPrimasRetenidas;
	}
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public Integer getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(Integer idMoneda) {
		this.idMoneda = idMoneda;
	}
	public Integer getIdRamo() {
		return IdRamo;
	}
	public void setIdRamo(Integer idRamo) {
		IdRamo = idRamo;
	}
	public Integer getIdSubRamo() {
		return IdSubRamo;
	}
	public void setIdSubRamo(Integer idSubRamo) {
		IdSubRamo = idSubRamo;
	}
	public Integer getRetencion() {
		return retencion;
	}
	public void setRetencion(Integer retencion) {
		this.retencion = retencion;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
}
