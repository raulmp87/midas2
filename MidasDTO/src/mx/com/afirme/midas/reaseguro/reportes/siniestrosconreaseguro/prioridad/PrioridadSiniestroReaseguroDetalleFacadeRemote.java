package mx.com.afirme.midas.reaseguro.reportes.siniestrosconreaseguro.prioridad;

// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for PrioridadSiniestroReaseguroDetalleDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface PrioridadSiniestroReaseguroDetalleFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved PrioridadSiniestroReaseguroDetalleDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PrioridadSiniestroReaseguroDetalleDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(PrioridadSiniestroReaseguroDetalleDTO entity);
    /**
	 Delete a persistent PrioridadSiniestroReaseguroDetalleDTO entity.
	  @param entity PrioridadSiniestroReaseguroDetalleDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PrioridadSiniestroReaseguroDetalleDTO entity);
   /**
	 Persist a previously saved PrioridadSiniestroReaseguroDetalleDTO entity and return it or a copy of it to the sender. 
	 A copy of the PrioridadSiniestroReaseguroDetalleDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PrioridadSiniestroReaseguroDetalleDTO entity to update
	 @return PrioridadSiniestroReaseguroDetalleDTO the persisted PrioridadSiniestroReaseguroDetalleDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public PrioridadSiniestroReaseguroDetalleDTO update(PrioridadSiniestroReaseguroDetalleDTO entity);
	public PrioridadSiniestroReaseguroDetalleDTO findById( BigDecimal id);
	 /**
	 * Find all PrioridadSiniestroReaseguroDetalleDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PrioridadSiniestroReaseguroDetalleDTO property to query
	  @param value the property value to match
	  	  @return List<PrioridadSiniestroReaseguroDetalleDTO> found by query
	 */
	public List<PrioridadSiniestroReaseguroDetalleDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all PrioridadSiniestroReaseguroDetalleDTO entities.
	  	  @return List<PrioridadSiniestroReaseguroDetalleDTO> all PrioridadSiniestroReaseguroDetalleDTO entities
	 */
	public List<PrioridadSiniestroReaseguroDetalleDTO> findAll(
		);	
}