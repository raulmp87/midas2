package mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * @author Paulo dos Santos
 */
@Entity
@Table(name="CNSF_SIN_ESTATUS_CARGA", schema="MIDAS")
public class CargaContraParteDTO implements Serializable {

	private static final long serialVersionUID = 2299753824789845932L;
	
	private Integer idcarga;
	private Date fechaFin;
	private String estatus;
	private String reporte;
	private String usuario;
	private Date fechaCarga;
	private Integer idArchivo;
	private Integer candado;
	
	
	/** default constructor */
    public CargaContraParteDTO() {
    }
    
    @Id
    @SequenceGenerator(name = "IDCARGA_SEQ_GENERA", allocationSize = 1, sequenceName = "MIDAS.CNSF_SIN_ESTATUS_CARGA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCARGA_SEQ_GENERA")
    @Column(name="IDCARGA", unique=true, nullable=false, precision=22, scale=0)
    public Integer getIdcarga() {
        return this.idcarga;
    }
    
    public void setIdcarga(Integer idcarga) {
        this.idcarga = idcarga;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAFIN", nullable=false, length=10)
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	@Column(name="ESTATUS", nullable=false, length=40)
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	@Column(name="REPORTE", nullable=false, length=40)
	public String getReporte() {
		return reporte;
	}
	public void setReporte(String reporte) {
		this.reporte = reporte;
	}
	
	@Column(name="USUARIO", nullable=false, length=40)
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHACARGA", nullable=false, length=10)    
    public Date getFechaCarga() {
		return fechaCarga;
	}
	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}
	
	@Column(name="IDARCHIVO", unique=true, nullable=false)
	public Integer getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(Integer idArchivo) {
		this.idArchivo = idArchivo;
	}
	
	@Column(name="CANDADO", unique=true, nullable=false)
	public Integer getCandado() {
		return candado;
	}

	public void setCandado(Integer candado) {
		this.candado = candado;
	}
	
}