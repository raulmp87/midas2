package mx.com.afirme.midas.reaseguro.reportes.siniestrosconreaseguro.prioridad;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



/**
 * PrioridadSiniestroReaseguroDTO entity. @author MyEclipse Persistence Tools
 */

@Entity
@Table(name="TMPRIORIDAD",schema="MIDAS")
public class PrioridadSiniestroReaseguroDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Fields
	private BigDecimal idtmprioridad;
    private String descripcion;
    private List<PrioridadSiniestroReaseguroDetalleDTO> prioridadSiniestroReaseguroDetalleDTOs = new ArrayList<PrioridadSiniestroReaseguroDetalleDTO>(0);


    // Constructors

    /** default constructor */
    public PrioridadSiniestroReaseguroDTO() {
    }

   
    // Property accessors
    @Id 
    
    @Column(name="IDTMPRIORIDAD", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtmprioridad() {
        return this.idtmprioridad;
    }
    
    public void setIdtmprioridad(BigDecimal idtmprioridad) {
        this.idtmprioridad = idtmprioridad;
    }
    
    @Column(name="DESCRIPCION", length=10)

    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
   
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="prioridadSiniestroReaseguroDTO")

    public List<PrioridadSiniestroReaseguroDetalleDTO> getPrioridadSiniestroReaseguroDetalleDTOs() {
        return this.prioridadSiniestroReaseguroDetalleDTOs;
    }
    
    public void setPrioridadSiniestroReaseguroDetalleDTOs(List<PrioridadSiniestroReaseguroDetalleDTO> prioridadSiniestroReaseguroDetalleDTOs) {
        this.prioridadSiniestroReaseguroDetalleDTOs = prioridadSiniestroReaseguroDetalleDTOs;
    }
   







}