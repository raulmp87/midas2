package mx.com.afirme.midas.reaseguro.reportes.saldos.siniestro;
// default package

import java.util.Date;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ReporteSaldosSiniestroDTOFacade.
 * @author Jose Luis Arellano
 */


public interface ReporteSaldosSiniestroFacadeRemote {

	public ReporteSaldosSiniestroDTO findById( Double id);
	 /**
	 * Find all ReporteSaldosSiniestroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReporteSaldosSiniestroDTO property to query
	  @param value the property value to match
	  	  @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the  the row index in the query result-set to begin collecting the results. rowStartIdxAndCount[1] specifies the the maximum count of results to return.  
	  	  @return List<ReporteSaldosSiniestroDTO> found by query
	 */
	public List<ReporteSaldosSiniestroDTO> findByProperty(String propertyName, Object value, int...rowStartIdxAndCount);
	
	/**
	 * Find all ReporteSaldosSiniestroDTO entities.
	  	  @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the  the row index in the query result-set to begin collecting the results. rowStartIdxAndCount[1] specifies the the maximum count of results to return.  
	  	  @return List<ReporteSaldosSiniestroDTO> all ReporteSaldosSiniestroDTO entities
	 */
	public List<ReporteSaldosSiniestroDTO> findAll(int...rowStartIdxAndCount);
	
	public List<ReporteSaldosSiniestroDTO> consultarSaldosSiniestro(Date fechaInicial,Date fechaFinal,boolean soloFacultativos,boolean soloAutomaticos,boolean incluirRetencion,
    		Double idMoneda, final int...rowStartIdxAndCount);
}