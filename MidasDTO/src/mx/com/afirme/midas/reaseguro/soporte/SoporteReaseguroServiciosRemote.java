package mx.com.afirme.midas.reaseguro.soporte;

import java.math.BigDecimal;
import java.util.Date;

import javax.ejb.Remote;

import mx.com.afirme.midas.reaseguro.soporte.validacion.ResultadoValidacionReaseguroDTO;


public interface SoporteReaseguroServiciosRemote {

	ResultadoValidacionReaseguroDTO validarReaseguroFacultativo(BigDecimal idToCotizacion);
	
	void notificarEmision(BigDecimal idToCotizacion,BigDecimal idToPoliza,Integer numeroEndoso, Date fechaEmision);
}
