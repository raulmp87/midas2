package mx.com.afirme.midas.reaseguro.soporte;


import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

/**
 * LineaSoporteCoberturaDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class LineaSoporteCoberturaDTOId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 5397404413592851046L;
	private BigDecimal idTdLineaSoporteCobertura;
	private BigDecimal idTmLineaSoporteReaseguro;
	private BigDecimal idToSoporteReaseguro;

	// Constructors

	/** default constructor */
	public LineaSoporteCoberturaDTOId() {
	}

	/** full constructor */
	public LineaSoporteCoberturaDTOId(BigDecimal idTdLineaSoporteCobertura,
			BigDecimal idTmLineaSoporteReaseguro,
			BigDecimal idToSoporteReaseguro) {
		this.idTdLineaSoporteCobertura = idTdLineaSoporteCobertura;
		this.idTmLineaSoporteReaseguro = idTmLineaSoporteReaseguro;
		this.idToSoporteReaseguro = idToSoporteReaseguro;
	}

	// Property accessors
	@SequenceGenerator(name = "IDTDLINEASOPREACOBERTURA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTDLINEASOPREACOBERTURA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTDLINEASOPREACOBERTURA_SEQ_GENERADOR")
	@Column(name = "IDTDLINEASOPREACOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTdLineaSoporteCobertura() {
		return this.idTdLineaSoporteCobertura;
	}

	public void setIdTdLineaSoporteCobertura(
			BigDecimal idTdLineaSoporteCobertura) {
		this.idTdLineaSoporteCobertura = idTdLineaSoporteCobertura;
	}

	@Column(name = "IDTMLINEASOPORTEREASEGURO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTmLineaSoporteReaseguro() {
		return this.idTmLineaSoporteReaseguro;
	}

	public void setIdTmLineaSoporteReaseguro(
			BigDecimal idTmLineaSoporteReaseguro) {
		this.idTmLineaSoporteReaseguro = idTmLineaSoporteReaseguro;
	}

	@Column(name = "IDTOSOPORTEREASEGURO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSoporteReaseguro() {
		return this.idToSoporteReaseguro;
	}

	public void setIdToSoporteReaseguro(BigDecimal idToSoporteReaseguro) {
		this.idToSoporteReaseguro = idToSoporteReaseguro;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof LineaSoporteCoberturaDTOId))
			return false;
		LineaSoporteCoberturaDTOId castOther = (LineaSoporteCoberturaDTOId) other;

		return ((this.getIdTdLineaSoporteCobertura() == castOther
				.getIdTdLineaSoporteCobertura()) || (this
				.getIdTdLineaSoporteCobertura() != null
				&& castOther.getIdTdLineaSoporteCobertura() != null && this
				.getIdTdLineaSoporteCobertura().equals(
						castOther.getIdTdLineaSoporteCobertura())))
				&& ((this.getIdTmLineaSoporteReaseguro() == castOther
						.getIdTmLineaSoporteReaseguro()) || (this
						.getIdTmLineaSoporteReaseguro() != null
						&& castOther.getIdTmLineaSoporteReaseguro() != null && this
						.getIdTmLineaSoporteReaseguro().equals(
								castOther.getIdTmLineaSoporteReaseguro())))
				&& ((this.getIdToSoporteReaseguro() == castOther
						.getIdToSoporteReaseguro()) || (this
						.getIdToSoporteReaseguro() != null
						&& castOther.getIdToSoporteReaseguro() != null && this
						.getIdToSoporteReaseguro().equals(
								castOther.getIdToSoporteReaseguro())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdTdLineaSoporteCobertura() == null ? 0 : this
						.getIdTdLineaSoporteCobertura().hashCode());
		result = 37
				* result
				+ (getIdTmLineaSoporteReaseguro() == null ? 0 : this
						.getIdTmLineaSoporteReaseguro().hashCode());
		result = 37
				* result
				+ (getIdToSoporteReaseguro() == null ? 0 : this
						.getIdToSoporteReaseguro().hashCode());
		return result;
	}

}