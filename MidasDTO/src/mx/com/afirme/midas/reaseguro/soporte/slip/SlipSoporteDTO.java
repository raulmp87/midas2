package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;


public class SlipSoporteDTO {
	
	private BigDecimal idSlip;
//	private BigDecimal idCotizacion;
	//private String asegurado;   // Póliza - *Asegurado (Alfanumérico) - Obligatorio
	private AseguradoSoporteDTO asegurado; // Póliza - *Asegurado (Alfanumérico) - Obligatorio
	private String direccionAsegurado;  // Póliza - *Dirección(Alfanumérico) - Obligatorio
//	private Date fechaInicialVigencia;  // Póliza - *Vigencia (Alfanumérico) - Obligatorio
//	private Date fechaFinalVigencia;    // Póliza - *Vigencia (Alfanumérico) - Obligatorio
	private String siniestralidad;      // Póliza - Siniestralidad 3 años (Alfanumérico) - Opcional
	private String informacionAdicional; // Póliza - Información adicional (Alfanumérico) - Opcional
	private ClienteDTO clienteDTO;
	private CotizacionDTO cotizacionDTO;
	
	/*
	 * Póliza - *Tipo de Póliza (Opción) - Obligatorio al menos uno de los que se indican a continuación
    			a) Cobertura anual con declaración mensual (Opción)
        			Cantidad de embarques mensuales  (Numérico)
    			b) Cobertura anual con declaración trimestral (Opción)
        			Cantidad de embarques trimestrales (Numérico)
    			c) Cobertura anual con declaración anual (Opción)
        			Cantidad de embarques anuales (Numérico)
    			d) Embarque especifico  (Opción)
	 */
//	private String tipoPoliza;  
	
	private List<DetalleSoporteDTO> listaDetalle;
	private List<IncisoSoporteDTO> listaIncisos;
	private List<SlipAnexoSoporteDTO> anexos;

	/*
	 * Póliza - Complemento en Información (Archivo Adjunto) - Opcional
	 */
	private int tipoSlip;
	private SlipAviacionSoporteDTO slipAviacion;
	private SlipBarcosSoporteDTO slipBarcos;
	private SlipEquipoContratistaSoporteDTO	 slipEquipoConstratista;
	private SlipIncendioSoporteDTO slipIncendioSoporte;
	private SlipObraCivilSoporteDTO slipObraCivilSoporte;
	private SlipTransportesSoporteDTO slipTransportesSoporte;
	private SlipGeneralSoporteDTO slipGeneralSoporte;
	private SlipPrimerRiesgoSoporteDTO slipPrimerRiesgo;
	
	private List<SlipRCConstructoresSoporteDTO> slipRCConstructoresSoporte;	
	private List<SlipRCFuncionarioSoporteDTO>  slipRCFuncionarioSoporte;
	
	public BigDecimal getIdSlip() {
		return idSlip;
	}

	public void setIdSlip(BigDecimal idSlip) {
		this.idSlip = idSlip;
	}

//	public BigDecimal getIdCotizacion() {
//		return idCotizacion;
//	}
//
//	public void setIdCotizacion(BigDecimal idCotizacion) {
//		this.idCotizacion = idCotizacion;
//	}

	public AseguradoSoporteDTO getAsegurado() {
		return asegurado;
	}

	public void setAsegurado(AseguradoSoporteDTO asegurado) {
		this.asegurado = asegurado;
	}

	public String getDireccionAsegurado() {
		return direccionAsegurado;
	}

	public void setDireccionAsegurado(String direccionAsegurado) {
		this.direccionAsegurado = direccionAsegurado;
	}

//	public Date getFechaInicialVigencia() {
//		return fechaInicialVigencia;
//	}
//
//	public void setFechaInicialVigencia(Date fechaInicialVigencia) {
//		this.fechaInicialVigencia = fechaInicialVigencia;
//	}
//
//	public Date getFechaFinalVigencia() {
//		return fechaFinalVigencia;
//	}
//
//	public void setFechaFinalVigencia(Date fechaFinalVigencia) {
//		this.fechaFinalVigencia = fechaFinalVigencia;
//	}

	public String getSiniestralidad() {
		return siniestralidad;
	}

	public void setSiniestralidad(String siniestralidad) {
		this.siniestralidad = siniestralidad;
	}

	public String getInformacionAdicional() {
		return informacionAdicional;
	}

	public void setInformacionAdicional(String informacionAdicional) {
		this.informacionAdicional = informacionAdicional;
	}

	public int getTipoSlip() {
		return tipoSlip;
	}

	public void setTipoSlip(int tipoSlip) {
		this.tipoSlip = tipoSlip;
	}

	public SlipAviacionSoporteDTO getSlipAviacion() {
		return slipAviacion;
	}

	public void setSlipAviacion(SlipAviacionSoporteDTO slipAviacion) {
		this.slipAviacion = slipAviacion;
	}

	public SlipBarcosSoporteDTO getSlipBarcos() {
		return slipBarcos;
	}

	public void setSlipBarcos(SlipBarcosSoporteDTO slipBarcos) {
		this.slipBarcos = slipBarcos;
	}

	public SlipEquipoContratistaSoporteDTO getSlipEquipoConstratista() {
		return slipEquipoConstratista;
	}

	public void setSlipEquipoConstratista(
			SlipEquipoContratistaSoporteDTO slipEquipoConstratista) {
		this.slipEquipoConstratista = slipEquipoConstratista;
	}

	public SlipIncendioSoporteDTO getSlipIncendioSoporte() {
		return slipIncendioSoporte;
	}

	public void setSlipIncendioSoporte(SlipIncendioSoporteDTO slipIncendioSoporte) {
		this.slipIncendioSoporte = slipIncendioSoporte;
	}

	public SlipObraCivilSoporteDTO getSlipObraCivilSoporte() {
		return slipObraCivilSoporte;
	}

	public void setSlipObraCivilSoporte(
			SlipObraCivilSoporteDTO slipObraCivilSoporte) {
		this.slipObraCivilSoporte = slipObraCivilSoporte;
	}

	public List<SlipRCConstructoresSoporteDTO> getSlipRCConstructoresSoporte() {
		return slipRCConstructoresSoporte;
	}

	public void setSlipRCConstructoresSoporte(
			List<SlipRCConstructoresSoporteDTO> slipRCConstructoresSoporte) {
		this.slipRCConstructoresSoporte = slipRCConstructoresSoporte;
	}

	public List<SlipRCFuncionarioSoporteDTO> getSlipRCFuncionarioSoporte() {
		return slipRCFuncionarioSoporte;
	}

	public void setSlipRCFuncionarioSoporte(
			List<SlipRCFuncionarioSoporteDTO> slipRCFuncionarioSoporte) {
		this.slipRCFuncionarioSoporte = slipRCFuncionarioSoporte;
	}

	public SlipTransportesSoporteDTO getSlipTransportesSoporte() {
		return slipTransportesSoporte;
	}

	public void setSlipTransportesSoporte(
			SlipTransportesSoporteDTO slipTransportesSoporte) {
		this.slipTransportesSoporte = slipTransportesSoporte;
	}

//	public String getTipoPoliza() {
//		return tipoPoliza;
//	}
//
//	public void setTipoPoliza(String tipoPoliza) {
//		this.tipoPoliza = tipoPoliza;
//	}

	public SlipGeneralSoporteDTO getSlipGeneralSoporte() {
		return slipGeneralSoporte;
	}

	public void setSlipGeneralSoporte(SlipGeneralSoporteDTO slipGeneralSoporte) {
		this.slipGeneralSoporte = slipGeneralSoporte;
	}

	public List<SlipAnexoSoporteDTO> getAnexos() {
		return anexos;
	}

	public void setAnexos(List<SlipAnexoSoporteDTO> anexos) {
		this.anexos = anexos;
	}

	public List<IncisoSoporteDTO> getListaIncisos() {
		return listaIncisos;
	}

	public void setListaIncisos(List<IncisoSoporteDTO> listaIncisos) {
		this.listaIncisos = listaIncisos;
	}

	public SlipPrimerRiesgoSoporteDTO getSlipPrimerRiesgo() {
		return slipPrimerRiesgo;
	}

	public void setSlipPrimerRiesgo(SlipPrimerRiesgoSoporteDTO slipPrimerRiesgo) {
		this.slipPrimerRiesgo = slipPrimerRiesgo;
	}
	
	public List<DetalleSoporteDTO> getListaDetalle() {
		return listaDetalle;
	}

	public void setListaDetalle(List<DetalleSoporteDTO> listaDetalle) {
		this.listaDetalle = listaDetalle;
	}

	public ClienteDTO getClienteDTO() {
		return clienteDTO;
	}

	public void setClienteDTO(ClienteDTO clienteDTO) {
		this.clienteDTO = clienteDTO;
	}

	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}
	
}
