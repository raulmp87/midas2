package mx.com.afirme.midas.reaseguro.soporte;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;

/**
 * LineaSoporteReaseguroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TMLINEASOPORTEREASEGURO", schema = "MIDAS")
public class LineaSoporteReaseguroDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 8381832497629005819L;
	private LineaSoporteReaseguroId id;
	private LineaDTO lineaDTO;
	private SoporteReaseguroDTO soporteReaseguroDTO;
	private BigDecimal idToCotizacion;
	private BigDecimal idToPoliza;
	private Short numeroEndoso;
	private BigDecimal montoSumaAsegurada;
	private BigDecimal montoSumaAseguradaEndoso;
	private Double tipoCambio;
	private BigDecimal idMoneda;
	private BigDecimal porcentajePleno;
	private BigDecimal  porcentajeRetencion;
	private BigDecimal porcentajeCuotaParte;
	private BigDecimal porcentajePrimerExcedente;
	private BigDecimal porcentajeFacultativo;
	private Date fechaInicioVigencia;
	private Integer estatusLineaSoporte;
	private boolean esPrimerRiesgo;
	private String notaDelSistema;
	private Date fechaModificacion;
	private Date fechaCreacion;
	private Date fechaEmision;
	private List<LineaSoporteCoberturaDTO> lineaSoporteCoberturaDTOs = new ArrayList<LineaSoporteCoberturaDTO>();
	private ContratoFacultativoDTO contratoFacultativoDTO;
	private Integer estatusFacultativo;
	private BigDecimal disPrimaPorcentajeRetencion;
	private BigDecimal disPrimaPorcentajeCuotaParte;
	private BigDecimal disPrimaPorcentajePrimerExcedente;
	private BigDecimal disPrimaPorcentajeFacultativo;
	private BigDecimal montoSumaAseguradaAcumulada;
	
	private Integer aplicaDistribucionPrima;
	
	//estatus de la linea
	public static final int COTIZACION = 1;
	public static final int EMITIDO_SIN_DISTRIBUIR = 2;
	public static final int EMITIDO_Y_DISTRIBUIDO = 3;
	public static final int ESTATUS_LINEA_EMITIDO_INCONSISTENCIAS= 4;
	public static final int EMITIDO_DISTRIBUCION_MASIVA = 22;
	
	//Estatus de Facultativo
	public static final int ESTATUS_SOPORTADO_POR_REASEGURO = 1;
	public static final int ESTATUS_REQUIERE_FACULTATIVO = 2;
	public static final int ESTATUS_FACULTATIVO_SOLICITADO = 3;
	public static final int ESTATUS_CANCELACION_FAC_SOLICITADA = 4;
	public static final int ESTATUS_COTIZACION_FAC_AUTORIZADA = 5;
	public static final int RETENCION_AUTORIZADA = 6;
	public static final int ESTATUS_FACULTATIVO_INTEGRADO = 7;
	public static final int ESTATUS_LIBERADA = 8;
	public static final int ESTATUS_AUTORIZADA_EMISION = 9;
	public static final int ESTATUS_EMITIDA = 10;
	public static final int CANCELADO = 11;
	public static final int ENDOSO_FACULTATIVO = 12;
	
	
	public static final int APLICA_DISTRIBUCION_PRIMA = 1;
	public static final int NO_APLICA_DISTRIBUCION_PRIMA = 0;

	// Constructors

	/** default constructor */
	public LineaSoporteReaseguroDTO() {
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idTmLineaSoporteReaseguro", column = @Column(name = "IDTMLINEASOPORTEREASEGURO", insertable= false,updatable= false, nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSoporteReaseguro", column = @Column(name = "IDTOSOPORTEREASEGURO",insertable= false,updatable= false, nullable = false, precision = 22, scale = 0)) })
	public LineaSoporteReaseguroId getId() {
		return this.id;
	}
 
	public void setId(LineaSoporteReaseguroId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCLINEA", nullable = false)
	public LineaDTO getLineaDTO() {
		return this.lineaDTO;
	}

	public void setLineaDTO(LineaDTO lineaDTO) {
		this.lineaDTO = lineaDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOSOPORTEREASEGURO", nullable = false, insertable = false, updatable = false)
	public SoporteReaseguroDTO getSoporteReaseguroDTO() {
		return this.soporteReaseguroDTO;
	}

	public void setSoporteReaseguroDTO(SoporteReaseguroDTO soporteReaseguroDTO) {
		this.soporteReaseguroDTO = soporteReaseguroDTO;
	}

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "IDTOPOLIZA", precision = 22, scale = 0)
	public BigDecimal getIdToPoliza() {
		return this.idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@Column(name = "MONTOSUMAASEGURADA", nullable = false, precision = 16)
	public BigDecimal getMontoSumaAsegurada() {
		return this.montoSumaAsegurada;
	}

	public void setMontoSumaAsegurada(BigDecimal montoSumaAsegurada) {
		this.montoSumaAsegurada = montoSumaAsegurada;
	}

	@Column(name = "TIPOCAMBIO", precision = 16)
	public Double getTipoCambio() {
		return this.tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	@Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name = "PORCENTAJEPLENO", nullable = false, precision = 126, scale = 0)
	public BigDecimal getPorcentajePleno() {
		return this.porcentajePleno;
	}

	public void setPorcentajePleno(BigDecimal porcentajePleno) {
		this.porcentajePleno = porcentajePleno;
	}


	@Column(name = "PORCENTAJERETENCION", nullable = false, precision = 126, scale = 0)
	public BigDecimal getPorcentajeRetencion() {
		return this.porcentajeRetencion;
	}

	public void setPorcentajeRetencion(BigDecimal porcentajeRetencion) {
		this.porcentajeRetencion = porcentajeRetencion;
	}

	@Column(name = "PORCENTAJECUOTAPARTE", nullable = false, precision = 126, scale = 0)
	public BigDecimal getPorcentajeCuotaParte() {
		return this.porcentajeCuotaParte;
	}

	public void setPorcentajeCuotaParte(BigDecimal porcentajeCuotaParte) {
		this.porcentajeCuotaParte = porcentajeCuotaParte;
	}

	@Column(name = "PORCENTAJEPRIMEREXCEDENTE", nullable = false, precision = 126, scale = 0)
	public BigDecimal getPorcentajePrimerExcedente() {
		return this.porcentajePrimerExcedente;
	}

	public void setPorcentajePrimerExcedente(BigDecimal porcentajePrimerExcedente) {
		this.porcentajePrimerExcedente = porcentajePrimerExcedente;
	}

	@Column(name = "PORCENTAJEFACULTATIVO", nullable = false, precision = 126, scale = 0)
	public BigDecimal getPorcentajeFacultativo() {
		return this.porcentajeFacultativo;
	}

	public void setPorcentajeFacultativo(BigDecimal porcentajeFacultativo) {
		this.porcentajeFacultativo = porcentajeFacultativo;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAINICIOVIGENCIA", nullable = false, length = 7)
	public Date getFechaInicioVigencia() {
		return this.fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	@Column(name = "ESTATUSLINEASOPORTE", nullable = false)
	public Integer getEstatusLineaSoporte() {
		return this.estatusLineaSoporte;
	}

	public void setEstatusLineaSoporte(Integer estatusLineaSoporte) {
		this.estatusLineaSoporte = estatusLineaSoporte;
	}

	@Column(name = "ESPRIMERRIESGO", nullable = false, precision = 22, scale = 0)
	public boolean getEsPrimerRiesgo() {
		return this.esPrimerRiesgo;
	}

	public void setEsPrimerRiesgo(boolean esPrimerRiesgo) {
		this.esPrimerRiesgo = esPrimerRiesgo;
	}

	@Column(name = "NOTADELSISTEMA", length = 500)
	public String getNotaDelSistema() {
		return this.notaDelSistema;
	}

	public void setNotaDelSistema(String notaDelSistema) {
		this.notaDelSistema = notaDelSistema;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAMODIFICACION", nullable = false, length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAEMISION", length = 7)
	public Date getFechaEmision() {
		return this.fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "lineaSoporteReaseguroDTO")
	public List<LineaSoporteCoberturaDTO> getLineaSoporteCoberturaDTOs() {
		return this.lineaSoporteCoberturaDTOs;
	}

	public void setLineaSoporteCoberturaDTOs(
			List<LineaSoporteCoberturaDTO> lineaSoporteCoberturaDTOs) {
		this.lineaSoporteCoberturaDTOs = lineaSoporteCoberturaDTOs;
	}
	
	public void setContratoFacultativoDTO(ContratoFacultativoDTO contratoFacultativoDTO) {
		this.contratoFacultativoDTO = contratoFacultativoDTO;
	}

	@ManyToOne
	@JoinColumn(name="IDTMCONTRATOFACULTATIVO", referencedColumnName="IDTMCONTRATOFACULTATIVO")
	public ContratoFacultativoDTO getContratoFacultativoDTO() {
		return contratoFacultativoDTO;
	}

	@Column(name = "ESTATUSFACULTATIVO", nullable = false)
//	@Transient
	public Integer getEstatusFacultativo() {
		return estatusFacultativo;
	}

	public void setEstatusFacultativo(Integer estatusFacultativo) {
		this.estatusFacultativo = estatusFacultativo;
	}
	
	@Column(name="DISPRIMPORCENTRETENCION")
	public BigDecimal getDisPrimaPorcentajeRetencion() {
		return disPrimaPorcentajeRetencion;
	}

	public void setDisPrimaPorcentajeRetencion(
			BigDecimal disPrimaPorcentajeRetencion) {
		this.disPrimaPorcentajeRetencion = disPrimaPorcentajeRetencion;
	}

	@Column(name="DISPRIMPORCENTCUOTAPARTE")
	public BigDecimal getDisPrimaPorcentajeCuotaParte() {
		return disPrimaPorcentajeCuotaParte;
	}

	public void setDisPrimaPorcentajeCuotaParte(
			BigDecimal disPrimaPorcentajeCuotaParte) {
		this.disPrimaPorcentajeCuotaParte = disPrimaPorcentajeCuotaParte;
	}

	@Column(name="DISPRIMPORCENTPRIMEREXCEDENTE")
	public BigDecimal getDisPrimaPorcentajePrimerExcedente() {
		return disPrimaPorcentajePrimerExcedente;
	}

	public void setDisPrimaPorcentajePrimerExcedente(
			BigDecimal disPrimaPorcentajePrimerExcedente) {
		this.disPrimaPorcentajePrimerExcedente = disPrimaPorcentajePrimerExcedente;
	}

	@Column(name="DISPRIMPORCENTRFACULTATIVO")
	public BigDecimal getDisPrimaPorcentajeFacultativo() {
		return disPrimaPorcentajeFacultativo;
	}

	public void setDisPrimaPorcentajeFacultativo(
			BigDecimal disPrimaPorcentajeFacultativo) {
		this.disPrimaPorcentajeFacultativo = disPrimaPorcentajeFacultativo;
	}
	
	@Column(name = "MONTOSUMAASEGURADAACUMULADA", nullable = false, precision = 16)
	public BigDecimal getMontoSumaAseguradaAcumulada() {
		return this.montoSumaAseguradaAcumulada;
	}

	public void setMontoSumaAseguradaAcumulada(BigDecimal montoSumaAseguradaAcumulada) {
		this.montoSumaAseguradaAcumulada = montoSumaAseguradaAcumulada;
	}

	@Column(name = "APLICADISTRIBUCION", nullable = false)
	public Integer getAplicaDistribucionPrima() {
		return aplicaDistribucionPrima;
	}

	public void setAplicaDistribucionPrima(Integer aplicaDistribucionPrima) {
		this.aplicaDistribucionPrima = aplicaDistribucionPrima;
	}

	@Column(name = "NUMEROENDOSO")
	public Short getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Short numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	@Column(name = "MONTOSUMAASEGURADAENDOSO", precision = 16)
	public BigDecimal getMontoSumaAseguradaEndoso() {
		return montoSumaAseguradaEndoso;
	}

	public void setMontoSumaAseguradaEndoso(BigDecimal montoSumaAseguradaEndoso) {
		this.montoSumaAseguradaEndoso = montoSumaAseguradaEndoso;
	}

}