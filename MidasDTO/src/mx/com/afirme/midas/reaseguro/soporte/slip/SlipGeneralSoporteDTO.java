package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;
import java.util.List;

public class SlipGeneralSoporteDTO {
	


	public static final int TIPO = 0;
	
	/**
	 * Obtenidos de DAÑOS
	 */
	private BigDecimal numeroInciso;
	private BigDecimal idToSeccion;
	private BigDecimal subInciso;
	
	private String descripcionSeccion;
	private String descripcionSubInciso;
	
	private String datosBien;
	
	/*
	 * Inciso - Coberturas(Alfanumérico) - Opcional
	 * Inciso - Suma asegurada (Moneda) - Opcional
	 * Inciso - Cuotas y primas (Alfanumérico) - Opcional con opción de cargar un archivo adjunto
	 * Inciso - Deducibles y coaseguros (Alfanumérico) - Opcional
	 * 
	 */
	private List<CoberturaSoporteDTO> listaCoberturas;	
	
	

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public BigDecimal getSubInciso() {
		return subInciso;
	}

	public void setSubInciso(BigDecimal subInciso) {
		this.subInciso = subInciso;
	}
	
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}


	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}

	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}

	public String getDescripcionSubInciso() {
		return descripcionSubInciso;
	}

	public void setDescripcionSubInciso(String descripcionSubInciso) {
		this.descripcionSubInciso = descripcionSubInciso;
	}	

	public String getDatosBien() {
		return datosBien;
	}

	public void setDatosBien(String datosBien) {
		this.datosBien = datosBien;
	}

	public List<CoberturaSoporteDTO> getListaCoberturas() {
		return listaCoberturas;
	}

	public void setListaCoberturas(List<CoberturaSoporteDTO> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	
}
