package mx.com.afirme.midas.reaseguro.soporte;
// default package



import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.reaseguro.soporte.validacion.CumuloPoliza;



/**
 * Remote interface for LineaSoporteReaseguroDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface LineaSoporteReaseguroFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved LineaSoporteReaseguroDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            LineaSoporteReaseguroDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public LineaSoporteReaseguroDTO save(LineaSoporteReaseguroDTO entity);

	/**
	 * Delete a persistent LineaSoporteReaseguroDTO entity.
	 * 
	 * @param entity
	 *            LineaSoporteReaseguroDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(LineaSoporteReaseguroDTO entity);

	/**
	 * Persist a previously saved LineaSoporteReaseguroDTO entity and return it
	 * or a copy of it to the sender. A copy of the LineaSoporteReaseguroDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            LineaSoporteReaseguroDTO entity to update
	 * @return LineaSoporteReaseguroDTO the persisted LineaSoporteReaseguroDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public LineaSoporteReaseguroDTO update(LineaSoporteReaseguroDTO entity);

	public LineaSoporteReaseguroDTO findById(LineaSoporteReaseguroId id);

	/**
	 * Find all LineaSoporteReaseguroDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the LineaSoporteReaseguroDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<LineaSoporteReaseguroDTO> found by query
	 */
	public List<LineaSoporteReaseguroDTO> findByProperty(String propertyName,
			Object value);

	public List<LineaSoporteReaseguroDTO> findByMontoSumaAsegurada(
			Object montoSumaAsegurada);

	public List<LineaSoporteReaseguroDTO> findByTipoCambio(Object tipoCambio);

	public List<LineaSoporteReaseguroDTO> findByPorcentajePleno(
			Object porcentajePleno);

	public List<LineaSoporteReaseguroDTO> findByPorcentajeRetencion(
			Object porcentajeRetencion);

	public List<LineaSoporteReaseguroDTO> findByPorcentajeCuotaParte(
			Object porcentajeCuotaParte);

	public List<LineaSoporteReaseguroDTO> findByPorcentajePrimerExcedente(
			Object porcentajePrimerExcedente);

	public List<LineaSoporteReaseguroDTO> findByPorcentajeFacultativo(
			Object porcentajeFacultativo);

	public List<LineaSoporteReaseguroDTO> findByEsPrimerRiesgo(
			Object esPrimerRiesgo);

	public List<LineaSoporteReaseguroDTO> findByNotaDelSistema(
			Object notaDelSistema);

	/**
	 * Find all LineaSoporteReaseguroDTO entities.
	 * 
	 * @return List<LineaSoporteReaseguroDTO> all LineaSoporteReaseguroDTO
	 *         entities
	 */
	public List<LineaSoporteReaseguroDTO> findAll();
	
	/**
	 * Update a List<LineaSoporteReaseguroDTO> using a transaction. This means
	 * all or nothing operation.
	 * @param List<LineaSoporteReaseguroDTO> all LineaSoporteReaseguroDTO
	 *         entities
	 */
	public void update(List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTOs);
	
	/**
	 * Elimina el registro LineaSoporteReaseguroDTO recibido. El borrado consulta primero los registros LineaSoporteCoberturaDTO
	 * para evitar la violaci�n a la llave for�nea.
	 */
	public void eliminarLineaSoporteReaseguro(LineaSoporteReaseguroDTO entity);
	
	public List<LineaSoporteReaseguroDTO> listarFiltrado(LineaSoporteReaseguroDTO entity);
	
	public void eliminarLineaSoporteReaseguro(BigDecimal idToSoporteReaseguro, Integer estatus);
	
	public List<LineaSoporteReaseguroDTO> listarLineaSoporteReaseguroPorEstatus(BigDecimal idToSoporteReaseguro, Integer estatus1,Integer estatus2,boolean comparaEstatusIgual);
	
	public int contarLineaSoporteReaseguroPorEstatus(BigDecimal idToSoporteReaseguro, Integer estatus1,boolean comparaEstatusIgual);
	
	public void actualizarEstatusLineaSoporte(BigDecimal idToSoporteReaseguro,Integer estatusOriginal,Integer estatusNuevo,String notaDelSistema);
	
	public void eliminarLineaSoportePoridSoporteReaseguro(BigDecimal idToSoporteReaseguro,List<BigDecimal> listaIdTmLineaSoporteReaseguro,boolean verificarIdTmLineaIgual);
	
	public void notificarEmisionLineaSoporteReaseguro(BigDecimal idToSoporteReaseguro,BigDecimal idToPoliza,Short numeroEndoso,Date fechaEmision,int estatusFacultativoLineaEmitida,int estatusLineaSoporte,int estatusLineaDistribuida);
	
	public int contarLineaSoporteReaseguroPorPropiedad(BigDecimal idToSoporteReaseguro, String propiedad,Object valor,boolean comparaEstatusIgual);
	
	public void actualizarEstatusFacultativoLineaSoporte(BigDecimal idToSoporteReaseguro,Integer estatusOriginal,Integer estatusNuevo);
	
	public Object obtenerCampoLineaSoporte(BigDecimal idTmLineaSoporteReaseguro,String campo);
	
	public int obtenerNumeroInciso(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO);
	
	public int obtenerNumeroSubInciso(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO);
	
	public BigDecimal obtenerIdToSeccion(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO);

	public void registrarLineasSoporteReaseguro(List<LineaSoporteReaseguroDTO> listaLineasSoporte);
	
	public List<LineaSoporteReaseguroDTO> listarLineaSoporteReaseguroPorSoporte(BigDecimal idToSoporteReaseguro);
	
	public void registrarEstatusSoporteReaseguroInvalido(BigDecimal idToSoporteReaseguro,List<String> listaErrores);
	
	public LineaSoporteReaseguroDTO obtenerLineaSoporteReaseguro(
			CumuloPoliza cumuloLinea,LineaSoporteReaseguroDTO lineaSoporteReaseguroAnteriorDTO,
			Double tipoCambio,BigDecimal idToSoporteReaseguro,Short numeroEndoso,
			boolean esSoporteEndoso,boolean calcularLineaSoporte) throws Exception;
}