package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;
import java.util.List;

public class SlipRCConstructoresSoporteDTO {
	
	public static final int TIPO = 6;

	
	/**
	 * Obtenidos de DAÑOS
	 */	
	private BigDecimal numeroInciso;
	
	// Inciso - *Descripción de la obra (Alfanumérico) - Obligatorio
	private String descripcionObra;
	
	// Inciso - *Sitio de los trabajos (Alfanumérico) - Obligatorio
	private String sitioTrabajos;
	
	// Inciso - Colindantes (Alfanumérico) - Opcional
	private String colindantes;
	
	// Inciso - Sistemas previstos para evitar daños (Alfanumérico) - Opcional
	private String sistemasPrevistos;	
	
	// Inciso - Indique experiencia con la que cuenta para la obra por realizar (Alfanumérico) - Opcional
	private String experienciaLaboral;
	
	// Inciso - *Duración de la obra (Alfanumérico) - Obligatorio
	private String duracionObra;
	
	// Inciso - *Valor estimado de la obra (Moneda) - Obligatorio
	private Double valorEstimadoObra;
	
	// Inciso - ¿Para quién es realizada la obra? (Alfanumérico) - Opcional
	private String paraQuienRealizaLaObra;
	
	// Inciso - Tipo de maquinaria de construcción a emplear (Alfanumérico) - Opcional
	// Inciso - La maquinaria es (Propia, Rentada, Con Operador, Sin Operador) (Opciones) - Opcional
	private int tipoMaquinaria;

	/*
	 * Inciso x Cobertura - Cuotas y primas (Alfanumérico) - Opcional con opción de cargar un archivo adjunto
	 * Inciso x Cobertura - Deducibles y coaseguros (Alfanumérico) - Opcional
	 */
	private List<CoberturaSoporteDTO> listaCoberturas;
	
	private List<SlipAnexoSoporteDTO> anexos;		
	
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getDescripcionObra() {
		return descripcionObra;
	}

	public void setDescripcionObra(String descripcionObra) {
		this.descripcionObra = descripcionObra;
	}

	public String getSitioTrabajos() {
		return sitioTrabajos;
	}

	public void setSitioTrabajos(String sitioTrabajos) {
		this.sitioTrabajos = sitioTrabajos;
	}

	public String getColindantes() {
		return colindantes;
	}

	public void setColindantes(String colindantes) {
		this.colindantes = colindantes;
	}

	public String getSistemasPrevistos() {
		return sistemasPrevistos;
	}

	public void setSistemasPrevistos(String sistemasPrevistos) {
		this.sistemasPrevistos = sistemasPrevistos;
	}

	public String getExperienciaLaboral() {
		return experienciaLaboral;
	}

	public void setExperienciaLaboral(String experienciaLaboral) {
		this.experienciaLaboral = experienciaLaboral;
	}

	public String getDuracionObra() {
		return duracionObra;
	}

	public void setDuracionObra(String duracionObra) {
		this.duracionObra = duracionObra;
	}

	public Double getValorEstimadoObra() {
		return valorEstimadoObra;
	}

	public void setValorEstimadoObra(Double valorEstimadoObra) {
		this.valorEstimadoObra = valorEstimadoObra;
	}

	public String getParaQuienRealizaLaObra() {
		return paraQuienRealizaLaObra;
	}

	public void setParaQuienRealizaLaObra(String paraQuienRealizaLaObra) {
		this.paraQuienRealizaLaObra = paraQuienRealizaLaObra;
	}

	public int getTipoMaquinaria() {
		return tipoMaquinaria;
	}

	public void setTipoMaquinaria(int tipoMaquinaria) {
		this.tipoMaquinaria = tipoMaquinaria;
	}

	public List<CoberturaSoporteDTO> getListaCoberturas() {
		return listaCoberturas;
	}

	public void setListaCoberturas(List<CoberturaSoporteDTO> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}

	public List<SlipAnexoSoporteDTO> getAnexos() {
		return anexos;
	}

	public void setAnexos(List<SlipAnexoSoporteDTO> anexos) {
		this.anexos = anexos;
	}
	
	
	/*
	 * TODO: Agregar anexos - copias de contratos y planos de la obra.
	 */

	
	
}
