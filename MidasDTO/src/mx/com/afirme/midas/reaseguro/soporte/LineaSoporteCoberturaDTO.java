package mx.com.afirme.midas.reaseguro.soporte;
// default package



import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * LineaSoporteCoberturaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TDLINEASOPREACOBERTURA", schema = "MIDAS")
public class LineaSoporteCoberturaDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 7932280303813597990L;
	private LineaSoporteCoberturaDTOId id;
	private LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO;
	private BigDecimal idToPersonaAsegurado;
	private BigDecimal idToPoliza;
	private BigDecimal numeroInciso;
	private BigDecimal idToSeccion;
	private BigDecimal numeroSubInciso;
	private BigDecimal idToCobertura;
	private BigDecimal porcentajeCoaseguro;
	private BigDecimal porcentajeDeducible;
	private BigDecimal montoPrimaSuscripcion;
	private BigDecimal montoPrimaFacultativo;
	private BigDecimal montoPrimaNoDevengada;
	private BigDecimal idMoneda;
	private boolean aplicaControlReclamo;
	private Date fechaModificacion;
	private Date fechaCreacion;
	private boolean esPrimerRiesgo;
	private String notaDelSistema;
	private BigDecimal montoPrimaAdicional;
	
	// Constructors

	/** default constructor */
	public LineaSoporteCoberturaDTO() {
	}

	/** minimal constructor */
	public LineaSoporteCoberturaDTO(LineaSoporteCoberturaDTOId id,
			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,
			BigDecimal montoPrimaSuscripcion, BigDecimal idMoneda,
			Date fechaModificacion, Date fechaCreacion) {
		this.id = id;
		this.lineaSoporteReaseguroDTO = lineaSoporteReaseguroDTO;
		this.montoPrimaSuscripcion = montoPrimaSuscripcion;
		this.idMoneda = idMoneda;
		this.fechaModificacion = fechaModificacion;
		this.fechaCreacion = fechaCreacion;
	}

	/** full constructor */
	public LineaSoporteCoberturaDTO(LineaSoporteCoberturaDTOId id,
			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,
			BigDecimal idToPersonaAsegurado, BigDecimal idToPoliza,
			BigDecimal numeroInciso, BigDecimal idToSeccion,
			BigDecimal numeroSubInciso, BigDecimal idToCobertura,
			BigDecimal porcentajeCoaseguro, BigDecimal porcentajeDeducible,
			BigDecimal montoPrimaSuscripcion, BigDecimal montoPrimaFacultativo,
			BigDecimal idMoneda, boolean aplicaControlReclamo,
			Date fechaModificacion, Date fechaCreacion) {
		this.id = id;
		this.lineaSoporteReaseguroDTO = lineaSoporteReaseguroDTO;
		this.idToPersonaAsegurado = idToPersonaAsegurado;
		this.idToPoliza = idToPoliza;
		this.numeroInciso = numeroInciso;
		this.idToSeccion = idToSeccion;
		this.numeroSubInciso = numeroSubInciso;
		this.idToCobertura = idToCobertura;
		this.porcentajeCoaseguro = porcentajeCoaseguro;
		this.porcentajeDeducible = porcentajeDeducible;
		this.montoPrimaSuscripcion = montoPrimaSuscripcion;
		this.montoPrimaFacultativo = montoPrimaFacultativo;
		this.idMoneda = idMoneda;
		this.aplicaControlReclamo = aplicaControlReclamo;
		this.fechaModificacion = fechaModificacion;
		this.fechaCreacion = fechaCreacion;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idTdLineaSoporteCobertura", column = @Column(name = "IDTDLINEASOPREACOBERTURA",insertable= false,updatable= false, nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idTmLineaSoporteReaseguro", column = @Column(name = "IDTMLINEASOPORTEREASEGURO",insertable= false,updatable= false, nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSoporteReaseguro", column = @Column(name = "IDTOSOPORTEREASEGURO",insertable= false,updatable= false, nullable = false, precision = 22, scale = 0)) })
	public LineaSoporteCoberturaDTOId getId() {
		return this.id;
	}

	public void setId(LineaSoporteCoberturaDTOId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns( {
			@JoinColumn(name = "IDTMLINEASOPORTEREASEGURO", referencedColumnName = "IDTMLINEASOPORTEREASEGURO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOSOPORTEREASEGURO", referencedColumnName = "IDTOSOPORTEREASEGURO", nullable = false, insertable = false, updatable = false) })
	public LineaSoporteReaseguroDTO getLineaSoporteReaseguroDTO() {
		return this.lineaSoporteReaseguroDTO;
	}

	public void setLineaSoporteReaseguroDTO(
			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) {
		this.lineaSoporteReaseguroDTO = lineaSoporteReaseguroDTO;
	}

	@Column(name = "IDTOPERSONAASEGURADO", precision = 22, scale = 0)
	public BigDecimal getIdToPersonaAsegurado() {
		return this.idToPersonaAsegurado;
	}

	public void setIdToPersonaAsegurado(BigDecimal idToPersonaAsegurado) {
		this.idToPersonaAsegurado = idToPersonaAsegurado;
	}

	@Column(name = "IDTOPOLIZA", precision = 22, scale = 0)
	public BigDecimal getIdToPoliza() {
		return this.idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@Column(name = "NUMEROINCISO", precision = 22, scale = 0)
	public BigDecimal getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "IDTOSECCION", precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "NUMEROSUBINCISO", precision = 22, scale = 0)
	public BigDecimal getNumeroSubInciso() {
		return this.numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	@Column(name = "IDTOCOBERTURA", precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	@Column(name = "POCENTAJECOASEGURO", precision = 3, scale = 10)
	public BigDecimal getPorcentajeCoaseguro() {
		return this.porcentajeCoaseguro;
	}

	public void setPorcentajeCoaseguro(BigDecimal porcentajeCoaseguro) {
		this.porcentajeCoaseguro = porcentajeCoaseguro;
	}

	@Column(name = "PORCENTAJEDECUCIBLE", precision = 3, scale = 10)
	public BigDecimal getPorcentajeDeducible() {
		return this.porcentajeDeducible;
	}

	public void setPorcentajeDeducible(BigDecimal porcentajeDeducible) {
		this.porcentajeDeducible = porcentajeDeducible;
	}

	@Column(name = "MONTOPRIMASUSCRIPCION", nullable = false, precision = 16)
	public BigDecimal getMontoPrimaSuscripcion() {
		return this.montoPrimaSuscripcion;
	}

	public void setMontoPrimaSuscripcion(BigDecimal montoPrimaSuscripcion) {
		this.montoPrimaSuscripcion = montoPrimaSuscripcion;
	}

	@Column(name = "MONTOPRIMAFACULTATIVO", precision = 16)
	public BigDecimal getMontoPrimaFacultativo() {
		return this.montoPrimaFacultativo;
	}

	public void setMontoPrimaFacultativo(BigDecimal montoPrimaFacultativo) {
		this.montoPrimaFacultativo = montoPrimaFacultativo;
	}

	public void setMontoPrimaNoDevengada(BigDecimal montoPrimaNoDevengada) {
		this.montoPrimaNoDevengada = montoPrimaNoDevengada;
	}
	
	@Column(name="MONTOPRIMANODEVENGADA", precision=16)
	public BigDecimal getMontoPrimaNoDevengada() {
		return montoPrimaNoDevengada;
	}

	@Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name = "APLICACONTROLRECLAMO", precision = 22, scale = 0)
	public boolean getAplicaControlReclamo() {
		return this.aplicaControlReclamo;
	}

	public void setAplicaControlReclamo(boolean aplicaControlReclamo) {
		this.aplicaControlReclamo = aplicaControlReclamo;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAMODIFICACION", nullable = false, length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "ESPRIMERRIESGO", nullable = true, precision = 22, scale = 0)
	public boolean getEsPrimerRiesgo() {
		return this.esPrimerRiesgo;
	}

	public void setEsPrimerRiesgo(boolean esPrimerRiesgo) {
		this.esPrimerRiesgo = esPrimerRiesgo;
	}

	@Column(name = "NOTADELSISTEMA", length = 500)
	public String getNotaDelSistema() {
		return this.notaDelSistema;
	}

	public void setNotaDelSistema(String notaDelSistema) {
		this.notaDelSistema = notaDelSistema;
	}

	@Column(name = "MONTOPRIMAADICIONAL", length = 500)
	public BigDecimal getMontoPrimaAdicional() {
		return montoPrimaAdicional;
	}

	public void setMontoPrimaAdicional(BigDecimal montoPrimaAdicional) {
		this.montoPrimaAdicional = montoPrimaAdicional;
	}
}