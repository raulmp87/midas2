package mx.com.afirme.midas.reaseguro.soporte.slip;

public class InformacionPilotoDTO {
	
	private String nombre;
	private String licencia;
	private String tipoLicencia;
	private String horasTotales;  // Inciso - Horas totales (Alfanumérico) - Opcional
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getLicencia() {
		return licencia;
	}
	public void setLicencia(String licencia) {
		this.licencia = licencia;
	}
	public String getTipoLicencia() {
		return tipoLicencia;
	}
	public void setTipoLicencia(String tipoLicencia) {
		this.tipoLicencia = tipoLicencia;
	}
	public String getHorasTotales() {
		return horasTotales;
	}
	public void setHorasTotales(String horasTotales) {
		this.horasTotales = horasTotales;
	}
	

}
