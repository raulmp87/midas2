package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;
import java.util.List;

public class SlipObraCivilSoporteDTO {


	public static final int TIPO = 5;	
	
	private String descripcionSeccion;
	private String descripcionSubInciso;	
	
	
	/**
	 * SOLICITAR
	 */
	private String tituloContrato; // Inciso - Título del contrato (Alfanumérico) - Opcional

	private String nombreContratista; // Inciso - Nombre y dirección del contratista (Alfanumérico) - Opcional
	
	private String direccionContratista; // Inciso - Nombre y dirección del contratista (Alfanumérico) - Opcional
	
	private String nombreSubContratista; // Inciso - Nombre y dirección de los subcontratistas (Alfanumérico) - Opcional
	
	private String direccionSubContratista;  // Inciso - Nombre y dirección de los subcontratistas (Alfanumérico) - Opcional
	
	private String metodosMaterialesConstruccion; // Inciso - *Métodos y materiales de construcción (Alfanumérico) - Obligatorio para facultativo
	
	private String experienciaContratista;       // Inciso - Experiencia del contratista en este tipo de trabajos (Alfanumérico) - Opcional
	
	private String fechaTerminacionPeriodoMantenimiento;  // Inciso - *Fecha de terminación del periodo de mantenimiento (Fecha) - Obligatorio si se pide la cobertura de "periodo de mantenimiento"

	private String trabajoSubContratistas;  			  // Inciso - Trabajos que harán los subcontratistas (Alfanumérico) - Opcional
	
	/* Inciso - *El diseño de las estructuras a ser aseguradas, 
	está basado en las regulaciones sobre estructuras resistentes a terremoto (si, no) (Opciones) ? 
	Obligatorio detectado por el sistema, se presenta la pregunta cuando se encuentre en zona de riesgo.
	*/			
	private String cumpleRegulacionesTerremotos;
	
	private String contratistaTienePolizaRC; // Inciso - Tiene el contratista una póliza separada de R.C. (si, no) (Opciones) - Opcional
	
	/*
	 * Inciso - *Existencia de estructuras adyacentes (edificios adyacentes en custodia, control o propios 
	 * del principal o contratista que se aseguran contra daños que puedan ocasionar los trabajos del contrato)
	 *  (Alfanumérico) - Obligatorio
	 */
	private String existenEstructurasAdyacentes;
	
	/*
	 * Inciso - *Edificios y estructuras de terceros que puedan ser afectadas por los trabajos de excavación, 
	 * vibración, tuberías subterráneas, etc. (Alfanumérico) - Obligatorio
	 */
	private String edificiosTerceros;	
	
	/*
	 * Inciso - Río, lago, mar, etc. más cercano (nombre, distancia) - Opcional
	 */
	private String aguasCercanas;
	
	/*
	 * 	Inciso - *Trabajos por contratos y valor del contrato (permanentes y temporales incluyendo 
	 *  todos los materiales a ser utilizados) (Alfanumérico) - Obligatorio
	 */
	private String trabajosPorContrato;
	
	/*
	 * 	Inciso - *Trabajos por contratos y valor del contrato (permanentes y temporales incluyendo 
	 *  todos los materiales a ser utilizados) (Alfanumérico) - Obligatorio
	 */	
	private String valorPorContrato;
	
	private String condicionesMetereologicas;	 // Inciso - Condiciones meteorológicas (periodo de lluvia, caída máxima, tempestad) - Opcional
	
	/**
	 * Obtenidos de DAÑOS
	 */
	private BigDecimal numeroInciso;
	
	private BigDecimal numeroSubInciso;
	
	private BigDecimal idToSeccion;	
	
	private String localizacionObra;    // Inciso - *Localización de la obra (Alfanumérico) - Obligatorio
	
	private String ingenieroConsultor;  // Inciso - Ingeniero consultor (Alfanumérico) - Opcional
	
	private String  descripcionTrabajo; // Inciso - *Descripción de los trabajos (Alfanumérico) - Obligatorio
	
	private BigDecimal sumaAseguradaObra; // Inciso - *Trabajos por contratos y valor del contrato (permanentes y temporales incluyendo todos los materiales a ser utilizados) (Alfanumérico) - Obligatorio
	
	private String 	maquinaria;  // Inciso - Maquinaria y equipos fijos y móviles (a valor de reposición) (Alfanumérico) - Opcional

	private BigDecimal sumaTotalAsegurada; // Inciso - *Suma total a ser asegurada de daños materiales (Moneda) - Obligatorio
	
	private String beneficiarioPreferente; // Inciso - Beneficiario preferente (Alfanumérico) - Opcional
	
	private String 	clausulaTerceros;     // Inciso - Añadir cláusula en donde se considere como 3eros a los bienes y personal de (Alfanumérico) - Opcional

	/*
	 * Inciso x Cobertura - Cuotas y primas (Alfanumérico) - Opcional con opción de cargar un archivo adjunto
	 * Inciso x Cobertura - Deducibles y coaseguros (Alfanumérico) - Opcional
	 */
	private List<CoberturaSoporteDTO> listaCoberturas;		
	
	
	public String getTituloContrato() {
		return tituloContrato;
	}

	public void setTituloContrato(String tituloContrato) {
		this.tituloContrato = tituloContrato;
	}
	
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}

	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}

	public String getDescripcionSubInciso() {
		return descripcionSubInciso;
	}

	public void setDescripcionSubInciso(String descripcionSubInciso) {
		this.descripcionSubInciso = descripcionSubInciso;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	

	public String getNombreContratista() {
		return nombreContratista;
	}

	public void setNombreContratista(String nombreContratista) {
		this.nombreContratista = nombreContratista;
	}

	public String getDireccionContratista() {
		return direccionContratista;
	}

	public void setDireccionContratista(String direccionContratista) {
		this.direccionContratista = direccionContratista;
	}

	public String getNombreSubContratista() {
		return nombreSubContratista;
	}

	public void setNombreSubContratista(String nombreSubContratista) {
		this.nombreSubContratista = nombreSubContratista;
	}

	public String getDireccionSubContratista() {
		return direccionSubContratista;
	}

	public void setDireccionSubContratista(String direccionSubContratista) {
		this.direccionSubContratista = direccionSubContratista;
	}

	public String getMetodosMaterialesConstruccion() {
		return metodosMaterialesConstruccion;
	}

	public void setMetodosMaterialesConstruccion(
			String metodosMaterialesConstruccion) {
		this.metodosMaterialesConstruccion = metodosMaterialesConstruccion;
	}

	public String getExperienciaContratista() {
		return experienciaContratista;
	}

	public void setExperienciaContratista(String experienciaContratista) {
		this.experienciaContratista = experienciaContratista;
	}

	public String getFechaTerminacionPeriodoMantenimiento() {
		return fechaTerminacionPeriodoMantenimiento;
	}

	public void setFechaTerminacionPeriodoMantenimiento(
			String fechaTerminacionPeriodoMantenimiento) {
		this.fechaTerminacionPeriodoMantenimiento = fechaTerminacionPeriodoMantenimiento;
	}

	public String getTrabajoSubContratistas() {
		return trabajoSubContratistas;
	}

	public void setTrabajoSubContratistas(String trabajoSubContratistas) {
		this.trabajoSubContratistas = trabajoSubContratistas;
	}

	public String getCumpleRegulacionesTerremotos() {
		return cumpleRegulacionesTerremotos;
	}

	public void setCumpleRegulacionesTerremotos(String cumpleRegulacionesTerremotos) {
		this.cumpleRegulacionesTerremotos = cumpleRegulacionesTerremotos;
	}

	public String getContratistaTienePolizaRC() {
		return contratistaTienePolizaRC;
	}

	public void setContratistaTienePolizaRC(String contratistaTienePolizaRC) {
		this.contratistaTienePolizaRC = contratistaTienePolizaRC;
	}

	public String getExistenEstructurasAdyacentes() {
		return existenEstructurasAdyacentes;
	}

	public void setExistenEstructurasAdyacentes(String existenEstructurasAdyacentes) {
		this.existenEstructurasAdyacentes = existenEstructurasAdyacentes;
	}

	public String getEdificiosTerceros() {
		return edificiosTerceros;
	}

	public void setEdificiosTerceros(String edificiosTerceros) {
		this.edificiosTerceros = edificiosTerceros;
	}

	public String getAguasCercanas() {
		return aguasCercanas;
	}

	public void setAguasCercanas(String aguasCercanas) {
		this.aguasCercanas = aguasCercanas;
	}

	public String getTrabajosPorContrato() {
		return trabajosPorContrato;
	}

	public void setTrabajosPorContrato(String trabajosPorContrato) {
		this.trabajosPorContrato = trabajosPorContrato;
	}

	public String getValorPorContrato() {
		return valorPorContrato;
	}

	public void setValorPorContrato(String valorPorContrato) {
		this.valorPorContrato = valorPorContrato;
	}

	public String getCondicionesMetereologicas() {
		return condicionesMetereologicas;
	}

	public void setCondicionesMetereologicas(String condicionesMetereologicas) {
		this.condicionesMetereologicas = condicionesMetereologicas;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getLocalizacionObra() {
		return localizacionObra;
	}

	public void setLocalizacionObra(String localizacionObra) {
		this.localizacionObra = localizacionObra;
	}

	public String getIngenieroConsultor() {
		return ingenieroConsultor;
	}

	public void setIngenieroConsultor(String ingenieroConsultor) {
		this.ingenieroConsultor = ingenieroConsultor;
	}

	public String getDescripcionTrabajo() {
		return descripcionTrabajo;
	}

	public void setDescripcionTrabajo(String descripcionTrabajo) {
		this.descripcionTrabajo = descripcionTrabajo;
	}

	public BigDecimal getSumaAseguradaObra() {
		return sumaAseguradaObra;
	}

	public void setSumaAseguradaObra(BigDecimal sumaAseguradaObra) {
		this.sumaAseguradaObra = sumaAseguradaObra;
	}

	public String getMaquinaria() {
		return maquinaria;
	}

	public void setMaquinaria(String maquinaria) {
		this.maquinaria = maquinaria;
	}

	public BigDecimal getSumaTotalAsegurada() {
		return sumaTotalAsegurada;
	}

	public void setSumaTotalAsegurada(BigDecimal sumaTotalAsegurada) {
		this.sumaTotalAsegurada = sumaTotalAsegurada;
	}

	public String getBeneficiarioPreferente() {
		return beneficiarioPreferente;
	}

	public void setBeneficiarioPreferente(String beneficiarioPreferente) {
		this.beneficiarioPreferente = beneficiarioPreferente;
	}

	public String getClausulaTerceros() {
		return clausulaTerceros;
	}

	public void setClausulaTerceros(String clausulaTerceros) {
		this.clausulaTerceros = clausulaTerceros;
	}

	public List<CoberturaSoporteDTO> getListaCoberturas() {
		return listaCoberturas;
	}

	public void setListaCoberturas(List<CoberturaSoporteDTO> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}

	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	


}
