package mx.com.afirme.midas.reaseguro.soporte.validacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;

public class ValidacionReaseguroFacultativoPorCatalogo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer tipoDistribucion;
	private BigDecimal idTcSubRamo;
	private BigDecimal idToCotizacion;
	private BigDecimal numeroInciso;
	private BigDecimal idToSeccion;
	private BigDecimal numeroSubInciso;
	private Boolean requiereFacultativo;
	private Double porcentajePleno;
	private List<IncisoCotizacionDTO> incisosRequierenFacultativo;
	private List<IncisoCotizacionDTO> incisosNoRequierenFacultativo;
	private Boolean permitirSolicitarFacultativo;
	
	public Integer getTipoDistribucion() {
		return tipoDistribucion;
	}
	public void setTipoDistribucion(Integer tipoDistribucion) {
		this.tipoDistribucion = tipoDistribucion;
	}
	public BigDecimal getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}
	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	public Boolean getRequiereFacultativo() {
		return requiereFacultativo;
	}
	public void setRequiereFacultativo(Boolean requiereFacultativo) {
		this.requiereFacultativo = requiereFacultativo;
	}
	public Double getPorcentajePleno() {
		return porcentajePleno;
	}
	public void setPorcentajePleno(Double porcentajePleno) {
		this.porcentajePleno = porcentajePleno;
	}
	public List<IncisoCotizacionDTO> getIncisosRequierenFacultativo() {
		return incisosRequierenFacultativo;
	}
	public void setIncisosRequierenFacultativo(
			List<IncisoCotizacionDTO> incisosRequierenFacultativo) {
		this.incisosRequierenFacultativo = incisosRequierenFacultativo;
	}
	public Boolean getPermitirSolicitarFacultativo() {
		return permitirSolicitarFacultativo;
	}
	public void setPermitirSolicitarFacultativo(Boolean permitirSolicitarFacultativo) {
		this.permitirSolicitarFacultativo = permitirSolicitarFacultativo;
	}
	public List<IncisoCotizacionDTO> getIncisosNoRequierenFacultativo() {
		return incisosNoRequierenFacultativo;
	}
	public void setIncisosNoRequierenFacultativo(
			List<IncisoCotizacionDTO> incisosNoRequierenFacultativo) {
		this.incisosNoRequierenFacultativo = incisosNoRequierenFacultativo;
	}
}
