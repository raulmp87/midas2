package mx.com.afirme.midas.reaseguro.soporte;


import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

/**
 * LineaSoporteReaseguroId entity. @author MyEclipse Persistence Tools
 */
@SuppressWarnings("serial")
@Embeddable
public class LineaSoporteReaseguroId implements java.io.Serializable {

	// Fields

	private BigDecimal idTmLineaSoporteReaseguro;
	private BigDecimal idToSoporteReaseguro;

	// Constructors

	/** default constructor */
	public LineaSoporteReaseguroId() {
	}

	/** full constructor */
	public LineaSoporteReaseguroId(BigDecimal idTmLineaSoporteReaseguro,
			BigDecimal idToSoporteReaseguro) {
		this.idTmLineaSoporteReaseguro = idTmLineaSoporteReaseguro;
		this.idToSoporteReaseguro = idToSoporteReaseguro;
	}

	// Property accessors
	@SequenceGenerator(name = "IDTMLINEASOPORTEREASEGURO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTMLINEASOPORTEREASEGURO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTMLINEASOPORTEREASEGURO_SEQ_GENERADOR")
	@Column(name = "IDTMLINEASOPORTEREASEGURO",nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTmLineaSoporteReaseguro() {
		return this.idTmLineaSoporteReaseguro;
	}

	public void setIdTmLineaSoporteReaseguro(
			BigDecimal idTmLineaSoporteReaseguro) {
		this.idTmLineaSoporteReaseguro = idTmLineaSoporteReaseguro;
	}

	@Column(name = "IDTOSOPORTEREASEGURO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSoporteReaseguro() {
		return this.idToSoporteReaseguro;
	}

	public void setIdToSoporteReaseguro(BigDecimal idToSoporteReaseguro) {
		this.idToSoporteReaseguro = idToSoporteReaseguro;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof LineaSoporteReaseguroId))
			return false;
		LineaSoporteReaseguroId castOther = (LineaSoporteReaseguroId) other;

		return ((this.getIdTmLineaSoporteReaseguro() == castOther
				.getIdTmLineaSoporteReaseguro()) || (this
				.getIdTmLineaSoporteReaseguro() != null
				&& castOther.getIdTmLineaSoporteReaseguro() != null && this
				.getIdTmLineaSoporteReaseguro().equals(
						castOther.getIdTmLineaSoporteReaseguro())))
				&& ((this.getIdToSoporteReaseguro() == castOther
						.getIdToSoporteReaseguro()) || (this
						.getIdToSoporteReaseguro() != null
						&& castOther.getIdToSoporteReaseguro() != null && this
						.getIdToSoporteReaseguro().equals(
								castOther.getIdToSoporteReaseguro())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdTmLineaSoporteReaseguro() == null ? 0 : this
						.getIdTmLineaSoporteReaseguro().hashCode());
		result = 37
				* result
				+ (getIdToSoporteReaseguro() == null ? 0 : this
						.getIdToSoporteReaseguro().hashCode());
		return result;
	}

}