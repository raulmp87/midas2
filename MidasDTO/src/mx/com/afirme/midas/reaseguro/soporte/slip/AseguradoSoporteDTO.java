package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.util.Date;

public class AseguradoSoporteDTO {
	private String nombreAsegurado;
	private String apelldoPaternoAsegurado;
	private String apelldoMaternoAsegurado;
	private String tipoPersona;
	private String descripcionTipoPersona;
	private String email;
	private String telefono;
	private String direccion;
	private Date fechaNacimiento;
	private String rfc;
	private String calle;
	private String numeroExterior;
	private String numeroInterior;
	private String entreCalles;
	private String colonia;
	private String estado;
	private String municipio;
	private String codigoPostal;

	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumeroExterior() {
		return numeroExterior;
	}

	public void setNumeroExterior(String numeroExterior) {
		this.numeroExterior = numeroExterior;
	}

	public String getNumeroInterior() {
		return numeroInterior;
	}

	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}

	public String getEntreCalles() {
		return entreCalles;
	}

	public void setEntreCalles(String entreCalles) {
		this.entreCalles = entreCalles;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getDescripcionTipoPersona() {
		return descripcionTipoPersona;
	}

	public void setDescripcionTipoPersona(String descripcionTipoPersona) {
		this.descripcionTipoPersona = descripcionTipoPersona;
	}

	public String getApelldoPaternoAsegurado() {
		return apelldoPaternoAsegurado;
	}

	public void setApelldoPaternoAsegurado(String apelldoPaternoAsegurado) {
		this.apelldoPaternoAsegurado = apelldoPaternoAsegurado;
	}

	public String getApelldoMaternoAsegurado() {
		return apelldoMaternoAsegurado;
	}

	public void setApelldoMaternoAsegurado(String apelldoMaternoAsegurado) {
		this.apelldoMaternoAsegurado = apelldoMaternoAsegurado;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

}
