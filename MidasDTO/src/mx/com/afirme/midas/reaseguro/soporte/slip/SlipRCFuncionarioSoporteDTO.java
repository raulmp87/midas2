package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class SlipRCFuncionarioSoporteDTO {
	
	public static final int TIPO = 7;
	
	/**
	 * Obtenidos de DAÑOS
	 */		
	private BigDecimal numeroInciso;
	private String interes;
	private String periodoNotificaciones;
	private Date fechaRetroactiva;
	private String retencion;
	private String limiteTerritorial;
	private String accionLegal;
	private String leyesJurisdiccion;
	private String condicionesGenerales;
	private String condicionesReaseguro;
	private String garantiaExpresa;
	private String subjetividades;
	private String terminoPrimas;
	private String comision;
	private String impuestos;
	private List<SlipAnexoSoporteDTO> anexos;	

	/*
	 * Inciso x Cobertura - Cuotas y primas (Alfanumérico) - Opcional con opción de cargar un archivo adjunto
	 * Inciso x Cobertura - Deducibles y coaseguros (Alfanumérico) - Opcional
	 */	
	private List<CoberturaSoporteDTO> listaCoberturas;	
	
	public String getInteres() {
		return interes;
	}
	public void setInteres(String interes) {
		this.interes = interes;
	}
	public String getPeriodoNotificaciones() {
		return periodoNotificaciones;
	}
	public void setPeriodoNotificaciones(String periodoNotificaciones) {
		this.periodoNotificaciones = periodoNotificaciones;
	}
	public Date getFechaRetroactiva() {
		return fechaRetroactiva;
	}
	public void setFechaRetroactiva(Date fechaRetroactiva) {
		this.fechaRetroactiva = fechaRetroactiva;
	}
	public String getRetencion() {
		return retencion;
	}
	public void setRetencion(String retencion) {
		this.retencion = retencion;
	}
	public String getLimiteTerritorial() {
		return limiteTerritorial;
	}
	public void setLimiteTerritorial(String limiteTerritorial) {
		this.limiteTerritorial = limiteTerritorial;
	}
	public String getAccionLegal() {
		return accionLegal;
	}
	public void setAccionLegal(String accionLegal) {
		this.accionLegal = accionLegal;
	}
	public String getLeyesJurisdiccion() {
		return leyesJurisdiccion;
	}
	public void setLeyesJurisdiccion(String leyesJurisdiccion) {
		this.leyesJurisdiccion = leyesJurisdiccion;
	}
	public String getCondicionesGenerales() {
		return condicionesGenerales;
	}
	public void setCondicionesGenerales(String condicionesGenerales) {
		this.condicionesGenerales = condicionesGenerales;
	}
	public String getCondicionesReaseguro() {
		return condicionesReaseguro;
	}
	public void setCondicionesReaseguro(String condicionesReaseguro) {
		this.condicionesReaseguro = condicionesReaseguro;
	}
	public String getGarantiaExpresa() {
		return garantiaExpresa;
	}
	public void setGarantiaExpresa(String garantiaExpresa) {
		this.garantiaExpresa = garantiaExpresa;
	}
	public String getSubjetividades() {
		return subjetividades;
	}
	public void setSubjetividades(String subjetividades) {
		this.subjetividades = subjetividades;
	}
	public String getTerminoPrimas() {
		return terminoPrimas;
	}
	public void setTerminoPrimas(String terminoPrimas) {
		this.terminoPrimas = terminoPrimas;
	}
	public String getComision() {
		return comision;
	}
	public void setComision(String comision) {
		this.comision = comision;
	}
	public String getImpuestos() {
		return impuestos;
	}
	public void setImpuestos(String impuestos) {
		this.impuestos = impuestos;
	}
	public List<CoberturaSoporteDTO> getListaCoberturas() {
		return listaCoberturas;
	}
	public void setListaCoberturas(List<CoberturaSoporteDTO> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	public List<SlipAnexoSoporteDTO> getAnexos() {
		return anexos;
	}
	public void setAnexos(List<SlipAnexoSoporteDTO> anexos) {
		this.anexos = anexos;
	}
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}	

}
