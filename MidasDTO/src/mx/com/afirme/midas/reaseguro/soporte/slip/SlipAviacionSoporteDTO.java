package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;
import java.util.List;

public class SlipAviacionSoporteDTO {
	
	public static final int TIPO = 1;

	/**
	 * Obtenidos de DAÑOS
	 */	
	private BigDecimal numeroInciso;


	private String marca;      // Inciso - *Marca (Alfanumérico) - Obligatorio
	private String modelo;     // Inciso - *Modelo (Alfanumérico) - Obligatorio
	private int tipoAeronave;  // Inciso - Tipo de aeronave (avión o helicóptero) (opción) - Opcional
	private int anio;          // Inciso - *Año (Numérico) - Obligatorio
	private String serie;      // Inciso - *Serie (Alfanumérico) - Obligatorio
	private String matricula;  // Inciso - *Matrícula (Alfanumérico) - Obligatorio
	private String uso;        // Inciso - *Uso (Alfanumérico) - Obligatorio
	private String capacidad;  // Inciso - *Capacidad (Alfanumérico) - Obligatorio
	private String limitesGeograficos;  // Inciso - *Límites geográficos (Alfanumérico) - Obligatorio
	private String aeropuertoBase;      // Inciso - *Aeropuerto base (Alfanumérico) - Obligatorio
	private String tipoAeropuerto;      // Inciso - *Tipos de aeropuerto (Alfanumérico) - Obligatorio
	private BigDecimal pagosVoluntariosTripulacion;  // Inciso - Pagos voluntarios TRIP (por persona) (Moneda) - Opcional
	private BigDecimal pagosMedicosPasajeros;   // Inciso - Gastos médicos pasajeros (por persona) (Moneda) - Opcional
	private BigDecimal pagosMedicosTripulacion;	// Inciso - Gastos médicos TRIP (por persona) (Moneda) - Opcional

	private List<InformacionPilotoDTO> listaInformacionPiloto; // Inciso - Información de pilotos (Alfanumérico) - Opcional
	
	/*
	 * Inciso - *Coberturas - Obligatorio
	 * Inciso x Cobertura - Cuotas y primas (Alfanumérico) - Opcional con opción de cargar un archivo adjunto
	 * Inciso x Cobertura - Deducibles (Alfanumérico) - Opcional
	 */
	private List<CoberturaSoporteDTO> listaCoberturas;	
	
	/*
	 * Inciso - *Suma asegurada (Moneda) - Obligatorio
	 */

	/**
	 * SOLICITADOS
	 */
	private String subLimites;   // Inciso - Sublímites (Alfanumérico) - Opcional
	private BigDecimal pagosVoluntariosPasajeros;  // Inciso - Pagos voluntarios pasajeros (por persona) (Moneda) - Opcional
	private String horasMarcaTipo;	// Inciso - Horas marca y tipo (Alfanumérico) - Opcional
	


	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getTipoAeronave() {
		return tipoAeronave;
	}

	public void setTipoAeronave(int tipoAeronave) {
		this.tipoAeronave = tipoAeronave;
	}
	


	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getUso() {
		return uso;
	}

	public void setUso(String uso) {
		this.uso = uso;
	}

	public String getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(String capacidad) {
		this.capacidad = capacidad;
	}

	public String getLimitesGeograficos() {
		return limitesGeograficos;
	}

	public void setLimitesGeograficos(String limitesGeograficos) {
		this.limitesGeograficos = limitesGeograficos;
	}

	public String getAeropuertoBase() {
		return aeropuertoBase;
	}

	public void setAeropuertoBase(String aeropuertoBase) {
		this.aeropuertoBase = aeropuertoBase;
	}

	public String getTipoAeropuerto() {
		return tipoAeropuerto;
	}

	public void setTipoAeropuerto(String tipoAeropuerto) {
		this.tipoAeropuerto = tipoAeropuerto;
	}

	public BigDecimal getPagosVoluntariosTripulacion() {
		return pagosVoluntariosTripulacion;
	}

	public void setPagosVoluntariosTripulacion(
			BigDecimal pagosVoluntariosTripulacion) {
		this.pagosVoluntariosTripulacion = pagosVoluntariosTripulacion;
	}

	public BigDecimal getPagosMedicosPasajeros() {
		return pagosMedicosPasajeros;
	}

	public void setPagosMedicosPasajeros(BigDecimal pagosMedicosPasajeros) {
		this.pagosMedicosPasajeros = pagosMedicosPasajeros;
	}

	public BigDecimal getPagosMedicosTripulacion() {
		return pagosMedicosTripulacion;
	}

	public void setPagosMedicosTripulacion(BigDecimal pagosMedicosTripulacion) {
		this.pagosMedicosTripulacion = pagosMedicosTripulacion;
	}


	public String getSubLimites() {
		return subLimites;
	}

	public void setSubLimites(String subLimites) {
		this.subLimites = subLimites;
	}

	public BigDecimal getPagosVoluntariosPasajeros() {
		return pagosVoluntariosPasajeros;
	}

	public void setPagosVoluntariosPasajeros(BigDecimal pagosVoluntariosPasajeros) {
		this.pagosVoluntariosPasajeros = pagosVoluntariosPasajeros;
	}

	public String getHorasMarcaTipo() {
		return horasMarcaTipo;
	}

	public void setHorasMarcaTipo(String horasMarcaTipo) {
		this.horasMarcaTipo = horasMarcaTipo;
	}

	public List<InformacionPilotoDTO> getListaInformacionPiloto() {
		return listaInformacionPiloto;
	}

	public void setListaInformacionPiloto(
			List<InformacionPilotoDTO> listaInformacionPiloto) {
		this.listaInformacionPiloto = listaInformacionPiloto;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public List<CoberturaSoporteDTO> getListaCoberturas() {
		return listaCoberturas;
	}

	public void setListaCoberturas(List<CoberturaSoporteDTO> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}


}
