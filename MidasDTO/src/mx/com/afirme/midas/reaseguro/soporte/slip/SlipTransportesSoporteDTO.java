package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;
import java.util.List;

public class SlipTransportesSoporteDTO {
	
	public static final int TIPO = 8;

	
	/**
	 * Obtenidos de DAÑOS
	 */			
	private BigDecimal numeroInciso;
	
	private String lugarOrigen;  // Inciso - *Lugar de origen (Alfanumérico) - Obligatorio
	private String lugarDestino; // Inciso - *Lugar destino (Alfanumérico) - Obligatorio
	private String bienesAsegurados; // Inciso - -*Bienes asegurados (Alfanumérico) - Obligatorio
	private String tipoEmpaque; // Inciso - *Tipo de empaque ó embalaje (Alfanumérico) - Obligatorio
	private BigDecimal limiteMaximoPorEmbarque; // Inciso - *Límite máximo por embarque en $ (Moneda) - Obligatorio
	private BigDecimal volumenAnualEmbarque;    // Inciso - *Volumen anual de embarques en $ (Moneda) - Obligatorio en pólizas anuales
	
	private String riesgosAmparados;            //TODO Verificar si este dato es necesario.
	
	private String descripcionMedioTransporte;				// Inciso - *Medios de transporte (Opciones) - Obligatorio al menos uno o más de los que se indican a continuación:
	
	/*
	 * Subinciso x Cobertura - Cuotas y primas (Alfanumérico) - Opcional con opción de cargar un archivo adjunto
	 * Subinciso x Cobertura - Deducibles y coaseguros (Alfanumérico) - Opcional
	 */
	private List<CoberturaSoporteDTO> listaCoberturas;

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getLugarOrigen() {
		return lugarOrigen;
	}

	public void setLugarOrigen(String lugarOrigen) {
		this.lugarOrigen = lugarOrigen;
	}

	public String getLugarDestino() {
		return lugarDestino;
	}

	public void setLugarDestino(String lugarDestino) {
		this.lugarDestino = lugarDestino;
	}

	public String getBienesAsegurados() {
		return bienesAsegurados;
	}

	public void setBienesAsegurados(String bienesAsegurados) {
		this.bienesAsegurados = bienesAsegurados;
	}

	public String getTipoEmpaque() {
		return tipoEmpaque;
	}

	public void setTipoEmpaque(String tipoEmpaque) {
		this.tipoEmpaque = tipoEmpaque;
	}

	public BigDecimal getLimiteMaximoPorEmbarque() {
		return limiteMaximoPorEmbarque;
	}

	public void setLimiteMaximoPorEmbarque(BigDecimal limiteMaximoPorEmbarque) {
		this.limiteMaximoPorEmbarque = limiteMaximoPorEmbarque;
	}

	public BigDecimal getVolumenAnualEmbarque() {
		return volumenAnualEmbarque;
	}

	public void setVolumenAnualEmbarque(BigDecimal volumenAnualEmbarque) {
		this.volumenAnualEmbarque = volumenAnualEmbarque;
	}

	public String getRiesgosAmparados() {
		return riesgosAmparados;
	}

	public void setRiesgosAmparados(String riesgosAmparados) {
		this.riesgosAmparados = riesgosAmparados;
	}

	public String getDescripcionMedioTransporte() {
		return descripcionMedioTransporte;
	}

	public void setDescripcionMedioTransporte(String descripcionMedioTransporte) {
		this.descripcionMedioTransporte = descripcionMedioTransporte;
	}

	public List<CoberturaSoporteDTO> getListaCoberturas() {
		return listaCoberturas;
	}

	public void setListaCoberturas(List<CoberturaSoporteDTO> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	
	
	
	
	

}
