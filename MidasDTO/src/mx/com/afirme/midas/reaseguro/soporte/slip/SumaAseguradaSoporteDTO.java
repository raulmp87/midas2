package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;

public class SumaAseguradaSoporteDTO {
	
	private String descripcion;
	
	private BigDecimal sumaAsegurada;

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

}
