

package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;
import java.util.List;

public class IncisoSoporteDTO {
	
	
	private BigDecimal numeroInciso;
	
	private String descripcionInciso;
	
	private String descripcionSubRamo;
	
	private List<SubIncisoSoporteDTO> listaSubIncisos;

	public String getDescripcionInciso() {
		return descripcionInciso;
	}

	public void setDescripcionInciso(String descripcionInciso) {
		this.descripcionInciso = descripcionInciso;
	}

	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}

	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}

	public List<SubIncisoSoporteDTO> getListaSubIncisos() {
		return listaSubIncisos;
	}

	public void setListaSubIncisos(List<SubIncisoSoporteDTO> listaSubIncisos) {
		this.listaSubIncisos = listaSubIncisos;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	

}
