package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;

public class SubIncisoSoporteDTO {

	private BigDecimal idToSeccion;
	
	private BigDecimal numeroSubInciso;	
	
	private String descripcionSeccion;
	
	private String descripcionSubInciso;

	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}

	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}

	public String getDescripcionSubInciso() {
		return descripcionSubInciso;
	}

	public void setDescripcionSubInciso(String descripcionSubInciso) {
		this.descripcionSubInciso = descripcionSubInciso;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

}
