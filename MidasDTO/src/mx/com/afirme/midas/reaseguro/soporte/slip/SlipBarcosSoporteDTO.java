package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class SlipBarcosSoporteDTO {
	
	public static final int TIPO = 2;
	
	public static final String CASCO_BARCO = "Casco Barco";
	public static final String VALOR_INCREMENTADO = "Valor Incrementado";
	public static final String RESPONSABILIDAD_CIVIL = "Responsabilidad Civil";
	
	
	/**
	 * Obtenidos de DAÑOS
	 */
	private BigDecimal numeroInciso;

	private String materialCasco;     // Inciso - *Material del casco (Alfanumérico) - Obligatorio
	private String tipoEmbarcacion;   // Inciso - *Tipo de embarcación (Alfanumérico) - Obligatorio
	private int anioConstruccion;     // Inciso - *Año de construcción (Numérico) - Obligatorio
	private String aguasNavegacion;   // Inciso - *Aguas de Navegación (Alfanumérico) - Obligatorio 
	private String uso;               // Inciso - *Uso (Alfanumérico) - Obligatorio
	private String puertoRegistro;    // Inciso - *Puerto de registro (Alfanumérico) - Obligatorio
	private String clasificacion; // Inciso - *Clasificación (Alfanumérico) - Obligatorio con opción NA (No Aplica)
	private String bandera;          // Inciso - *Bandera (Alfanumérico) - Obligatorio
	private String eslora;  // Inciso - *Eslora (Alfanumérico) - Obligatorio para facultativo
	private String manga;   // Inciso - *Manga (Alfanumérico) - Obligatorio para facultativo
	private String puntal;  // Inciso - *Puntal (Alfanumérico) - Obligatorio para facultativo
	private String tonelajeBruto;  // Inciso - Tonelaje bruto - Opcional
	private String tonelajeNeto;   // Inciso - Tonelaje neto - Opcional
	
	/*
	Inciso - *Valor del casco (Moneda) - Obligatorio
	Inciso - *Suma asegurada (Moneda) - Obligatorio
	Inciso - Casco (Moneda) - Obligatorio al menos uno: casco o rc
	Inciso - R.C. (Moneda) - Obligatorio al menos uno: casco o rc
	Inciso - V.I. (Moneda) - Opcional
	*/
	private BigDecimal valorCasco;
	private BigDecimal sumaAsegurada;
	private BigDecimal sumaAseguradaCasco;
	private BigDecimal sumaAseguradaRC;
	private BigDecimal sumaAseguradaVI;
	
	/*
	 * Inciso x Cobertura - Cuotas y primas (Alfanumérico) - Opcional con opción de cargar un archivo adjunto
	 * Inciso x Cobertura - Deducibles (Alfanumérico) - Opcional
	 */
	private List<CoberturaSoporteDTO> listaCoberturas;
		

	/**
	 * SOLICITADOS
	 */
	private String lugarConstruccion;  // Inciso - *Lugar de construcción (Alfanumérico) - Obligatorio
	private String serie;              // Inciso - Serie (Alfanumérico) - Opcional         
	private String matricula;          // Inciso - Matrícula (Alfanumérico) - Opcional
	private Date ultimaFechaMttoInspeccion;  // Inciso - Ultima fecha de mtto/inspección (Fecha) - Opcional


	public String getMaterialCasco() {
		return materialCasco;
	}
	public void setMaterialCasco(String materialCasco) {
		this.materialCasco = materialCasco;
	}
	
	
	public String getTipoEmbarcacion() {
		return tipoEmbarcacion;
	}
	public void setTipoEmbarcacion(String tipoEmbarcacion) {
		this.tipoEmbarcacion = tipoEmbarcacion;
	}
	public int getAnioConstruccion() {
		return anioConstruccion;
	}
	public void setAnioConstruccion(int anioConstruccion) {
		this.anioConstruccion = anioConstruccion;
	}
	public String getAguasNavegacion() {
		return aguasNavegacion;
	}
	public void setAguasNavegacion(String aguasNavegacion) {
		this.aguasNavegacion = aguasNavegacion;
	}
	public String getUso() {
		return uso;
	}
	public void setUso(String uso) {
		this.uso = uso;
	}
	public String getPuertoRegistro() {
		return puertoRegistro;
	}
	public void setPuertoRegistro(String puertoRegistro) {
		this.puertoRegistro = puertoRegistro;
	}
	public String getClasificacion() {
		return clasificacion;
	}
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}
	public String getBandera() {
		return bandera;
	}
	public void setBandera(String bandera) {
		this.bandera = bandera;
	}
	public String getEslora() {
		return eslora;
	}
	public void setEslora(String eslora) {
		this.eslora = eslora;
	}
	public String getManga() {
		return manga;
	}
	public void setManga(String manga) {
		this.manga = manga;
	}
	public String getPuntal() {
		return puntal;
	}
	public void setPuntal(String puntal) {
		this.puntal = puntal;
	}
	public String getTonelajeBruto() {
		return tonelajeBruto;
	}
	public void setTonelajeBruto(String tonelajeBruto) {
		this.tonelajeBruto = tonelajeBruto;
	}
	public String getTonelajeNeto() {
		return tonelajeNeto;
	}
	public void setTonelajeNeto(String tonelajeNeto) {
		this.tonelajeNeto = tonelajeNeto;
	}
	public String getLugarConstruccion() {
		return lugarConstruccion;
	}
	public void setLugarConstruccion(String lugarConstruccion) {
		this.lugarConstruccion = lugarConstruccion;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public Date getUltimaFechaMttoInspeccion() {
		return ultimaFechaMttoInspeccion;
	}
	public void setUltimaFechaMttoInspeccion(Date ultimaFechaMttoInspeccion) {
		this.ultimaFechaMttoInspeccion = ultimaFechaMttoInspeccion;
	}
	public BigDecimal getValorCasco() {
		return valorCasco;
	}
	public void setValorCasco(BigDecimal valorCasco) {
		this.valorCasco = valorCasco;
	}
	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public BigDecimal getSumaAseguradaCasco() {
		return sumaAseguradaCasco;
	}
	public void setSumaAseguradaCasco(BigDecimal sumaAseguradaCasco) {
		this.sumaAseguradaCasco = sumaAseguradaCasco;
	}
	public BigDecimal getSumaAseguradaRC() {
		return sumaAseguradaRC;
	}
	public void setSumaAseguradaRC(BigDecimal sumaAseguradaRC) {
		this.sumaAseguradaRC = sumaAseguradaRC;
	}
	public BigDecimal getSumaAseguradaVI() {
		return sumaAseguradaVI;
	}
	public void setSumaAseguradaVI(BigDecimal sumaAseguradaVI) {
		this.sumaAseguradaVI = sumaAseguradaVI;
	}
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public List<CoberturaSoporteDTO> getListaCoberturas() {
		return listaCoberturas;
	}
	public void setListaCoberturas(List<CoberturaSoporteDTO> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	
	
	
	
	

}
