package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;
import java.util.List;

public class SlipIncendioSoporteDTO {
	
	public static final int TIPO = 4;	
	
	/**
	 * Obtenidos de DAÑOS
	 */
	private BigDecimal numeroInciso;	
	
	


	private String giroAsegurado;    // Póliza - *Giro del asegurado (Alfanumérico) - Obligatorio
	
	private String zonaTerremoto;    // Inciso - Zona de terremoto (por AMIS) (Alfanumérico) - Opcional detectado por el Sistema con el CP

	private String tipoConstruccion; // Inciso - *Tipo de construcción (Alfanumérico) - Obligatorio
	
	private String direccionRiesgo;  // Inciso - *Dirección del riesgo (Alfanumérico) - Obligatorio

	private String giroUbicacion;    // Inciso - *Giro de la ubicación (Alfanumérico) - Obligatorio
	
	/*
	 * Póliza - *Suma asegurada (Moneda) - Obligatorio
	 * Sumatoria de todas las sumas aseguradas de las Coberturas del Subramo de Incendio
	 */
	private BigDecimal sumaAseguradaTotal; 
	
	
	/*
	 * Inciso - *Suma asegurada por Ubicación (Moneda) - Obligatorio
	 * Sumatoria de las sumas aseguradas de las Coberturas por Ubicación
	 */
	private BigDecimal sumaAseguradaPorUbicacion;
	
	/*
	 * Inciso x Cobertura - Cuotas y primas (Alfanumérico) - Opcional con opción de cargar un archivo adjunto
	 * Inciso x Cobertura - Deducibles y coaseguros (Alfanumérico) - Opcional
	 */
	private List<CoberturaSoporteDTO> listaCoberturas;	
	
	/**
	 * SOLICITAR
	 */	
	private String proteccionesContraIncendio; // Inciso - Protecciones contra incendio (Alfanumérico) - Opcional

	
	
	public String getGiroAsegurado() {
		return giroAsegurado;
	}

	public void setGiroAsegurado(String giroAsegurado) {
		this.giroAsegurado = giroAsegurado;
	}

	
	
	public String getZonaTerremoto() {
		return zonaTerremoto;
	}

	public void setZonaTerremoto(String zonaTerremoto) {
		this.zonaTerremoto = zonaTerremoto;
	}

	public String getTipoConstruccion() {
		return tipoConstruccion;
	}

	public void setTipoConstruccion(String tipoConstruccion) {
		this.tipoConstruccion = tipoConstruccion;
	}

	public String getDireccionRiesgo() {
		return direccionRiesgo;
	}

	public void setDireccionRiesgo(String direccionRiesgo) {
		this.direccionRiesgo = direccionRiesgo;
	}

	public String getGiroUbicacion() {
		return giroUbicacion;
	}

	public void setGiroUbicacion(String giroUbicacion) {
		this.giroUbicacion = giroUbicacion;
	}

	public BigDecimal getSumaAseguradaPorUbicacion() {
		return sumaAseguradaPorUbicacion;
	}

	public void setSumaAseguradaPorUbicacion(BigDecimal sumaAseguradaPorUbicacion) {
		this.sumaAseguradaPorUbicacion = sumaAseguradaPorUbicacion;
	}

	public String getProteccionesContraIncendio() {
		return proteccionesContraIncendio;
	}

	public void setProteccionesContraIncendio(String proteccionesContraIncendio) {
		this.proteccionesContraIncendio = proteccionesContraIncendio;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public BigDecimal getSumaAseguradaTotal() {
		return sumaAseguradaTotal;
	}

	public void setSumaAseguradaTotal(BigDecimal sumaAseguradaTotal) {
		this.sumaAseguradaTotal = sumaAseguradaTotal;
	}

	public List<CoberturaSoporteDTO> getListaCoberturas() {
		return listaCoberturas;
	}

	public void setListaCoberturas(List<CoberturaSoporteDTO> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	


}
