package mx.com.afirme.midas.reaseguro.soporte;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;


/**
 * SoporteReaseguroDTO entity. @author MyEclipse Persistence Tools
 */

@Entity
@Table(name = "TOSOPORTEREASEGURO", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"IDTOCOTIZACION", "IDTOPOLIZA" }))
@NamedQuery(name="listarSoporteReaseguroPorEstatusLineas",
		query="select distinct soporteReaseguroDTO from SoporteReaseguroDTO soporteReaseguroDTO " +
			"join soporteReaseguroDTO.lineaSoporteReaseguroDTOs lineaSoporteReaseguroDTO " +
			"join lineaSoporteReaseguroDTO.lineaSoporteCoberturaDTOs lineaSoporteCoberturaDTO " +
			"where lineaSoporteReaseguroDTO.estatusLineaSoporte = :estatusLineaSoporte")
public class SoporteReaseguroDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -2901568034476493973L;
	private BigDecimal idToSoporteReaseguro;
	private BigDecimal idToCotizacion;
	private Integer numeroEndoso;
	private BigDecimal idToPoliza;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private Date fechaEmision;
	private List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTOs = new ArrayList<LineaSoporteReaseguroDTO>();
	private SoporteReaseguroDTO soporteReaseguroDTOEndosoAnterior;
	private BigDecimal idToSoporteReaseguroEndosoAnterior;
	
	// Constructors

	
	/** default constructor */
	public SoporteReaseguroDTO() {
	}

	/** minimal constructor */
	public SoporteReaseguroDTO(BigDecimal idToSoporteReaseguro,
			BigDecimal idToCotizacion, Date fechaCreacion,
			Date fechaModificacion) {
		this.idToSoporteReaseguro = idToSoporteReaseguro;
		this.idToCotizacion = idToCotizacion;
		this.fechaCreacion = fechaCreacion;
		this.fechaModificacion = fechaModificacion;
	}

	/** full constructor */
	public SoporteReaseguroDTO(BigDecimal idToSoporteReaseguro,
			BigDecimal idToCotizacion, Integer numeroEndoso,
			BigDecimal idToPoliza, Date fechaCreacion, Date fechaModificacion,
			Date fechaEmision,
			List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTOs) {
		this.idToSoporteReaseguro = idToSoporteReaseguro;
		this.idToCotizacion = idToCotizacion;
		this.numeroEndoso = numeroEndoso;
		this.idToPoliza = idToPoliza;
		this.fechaCreacion = fechaCreacion;
		this.fechaModificacion = fechaModificacion;
		this.fechaEmision = fechaEmision;
		this.lineaSoporteReaseguroDTOs = lineaSoporteReaseguroDTOs;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOSOPORTEREASEGURO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOSOPORTEREASEGURO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOSOPORTEREASEGURO_SEQ_GENERADOR")
	@Column(name = "IDTOSOPORTEREASEGURO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSoporteReaseguro() {
		return this.idToSoporteReaseguro;
	}

	public void setIdToSoporteReaseguro(BigDecimal idToSoporteReaseguro) {
		this.idToSoporteReaseguro = idToSoporteReaseguro;
	}

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "NUMEROENDOSO", precision = 22, scale = 0)
	public Integer getNumeroEndoso() {
		return this.numeroEndoso;
	}

	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	@Column(name = "IDTOPOLIZA", precision = 22, scale = 0)
	public BigDecimal getIdToPoliza() {
		return this.idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAMODIFICACION", nullable = false, length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAEMISION", length = 7)
	public Date getFechaEmision() {
		return this.fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "soporteReaseguroDTO" )
	public List<LineaSoporteReaseguroDTO> getLineaSoporteReaseguroDTOs() {
		return lineaSoporteReaseguroDTOs;
	}

	public void setLineaSoporteReaseguroDTOs( 
			List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTOs) {
		this.lineaSoporteReaseguroDTOs = lineaSoporteReaseguroDTOs;
	}

	public void setSoporteReaseguroDTOEndosoAnterior1(SoporteReaseguroDTO soporteReaseguroDTOEndosoAnterior) {
		this.soporteReaseguroDTOEndosoAnterior = soporteReaseguroDTOEndosoAnterior;
	}

//	@OneToOne(fetch=FetchType.EAGER)
//	@JoinColumn(name="IDTOSOPORTEANTERIOR", referencedColumnName="IDTOSOPORTEREASEGURO")
	@Transient
	public SoporteReaseguroDTO getSoporteReaseguroDTOEndosoAnterior1() {
		return soporteReaseguroDTOEndosoAnterior;
	}

	@Column(name = "IDTOSOPORTEANTERIOR", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSoporteReaseguroEndosoAnterior() {
		return idToSoporteReaseguroEndosoAnterior;
	}

	public void setIdToSoporteReaseguroEndosoAnterior(
			BigDecimal idToSoporteReaseguroEndosoAnterior) {
		this.idToSoporteReaseguroEndosoAnterior = idToSoporteReaseguroEndosoAnterior;
	}

}