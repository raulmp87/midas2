package mx.com.afirme.midas.reaseguro.soporte.validacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Hashtable;

public class DetalleCobertura implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer numeroInciso;
    private BigDecimal idSeccion;
    private Integer numeroSubinciso;
    private BigDecimal idCobertura;
    private Double porcentajeCoaseguro;
    private Double montoPrimaSuscripcion;
    private Double montoPrimaNoDevengada;
    private Integer monedaPrima;
    private Double porcentajeDeducible;
    private Boolean esPrimerRiesgo;
    
    public DetalleCobertura(){
    	
    }
    
    public DetalleCobertura(Integer numeroInciso, BigDecimal idSeccion,Integer numeroSubinciso, BigDecimal idCobertura,Boolean esPrimerRiesgo) {
		this.numeroInciso = numeroInciso;
		this.idSeccion = idSeccion;
		this.numeroSubinciso = numeroSubinciso;
		this.idCobertura = idCobertura;
		this.esPrimerRiesgo = esPrimerRiesgo;
	}



	public Hashtable<String,Object> toHashTable(){
    	Hashtable<String,Object> hastable = new Hashtable<String,Object>();
    	if (numeroInciso != null)
    		hastable.put("numeroInciso",numeroInciso);
    	if (idSeccion != null)
    		hastable.put("idSeccion",idSeccion);
    	if (numeroSubinciso != null)
    		hastable.put("numeroSubinciso",numeroSubinciso);
    	if (idCobertura != null)
    		hastable.put("idCobertura",idCobertura);
    	if (porcentajeCoaseguro != null)
    		hastable.put("porcentajeCoaseguro",porcentajeCoaseguro);
    	if (montoPrimaSuscripcion != null)
    		hastable.put("montoPrimaSuscripcion",montoPrimaSuscripcion);
    	if (monedaPrima != null)
    		hastable.put("monedaPrima",monedaPrima);
    	if (porcentajeDeducible != null)
    		hastable.put("porcentajeDeducible",porcentajeDeducible);
    	if (esPrimerRiesgo != null)
    		hastable.put("esPrimerRiesgo",esPrimerRiesgo);
    	if (montoPrimaNoDevengada != null)
    		hastable.put("montoPrimaNoDevengada",montoPrimaNoDevengada);
    		
    	return hastable;
    }
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public BigDecimal getIdSeccion() {
		return idSeccion;
	}
	public void setIdSeccion(BigDecimal idSeccion) {
		this.idSeccion = idSeccion;
	}
	public Integer getNumeroSubinciso() {
		return numeroSubinciso;
	}
	public void setNumeroSubinciso(Integer numeroSubinciso) {
		this.numeroSubinciso = numeroSubinciso;
	}
	public BigDecimal getIdCobertura() {
		return idCobertura;
	}
	public void setIdCobertura(BigDecimal idCobertura) {
		this.idCobertura = idCobertura;
	}
	public Double getPorcentajeCoaseguro() {
		return porcentajeCoaseguro;
	}
	public void setPorcentajeCoaseguro(Double porcentajeCoaseguro) {
		this.porcentajeCoaseguro = porcentajeCoaseguro;
	}
	public Double getMontoPrimaSuscripcion() {
		return montoPrimaSuscripcion;
	}
	public void setMontoPrimaSuscripcion(Double montoPrimaSuscripcion) {
		this.montoPrimaSuscripcion = montoPrimaSuscripcion;
	}
	public Integer getMonedaPrima() {
		return monedaPrima;
	}
	public void setMonedaPrima(Integer monedaPrima) {
		this.monedaPrima = monedaPrima;
	}
	public Double getPorcentajeDeducible() {
		return porcentajeDeducible;
	}
	public void setPorcentajeDeducible(Double porcentajeDeducible) {
		this.porcentajeDeducible = porcentajeDeducible;
	}
	public Boolean getEsPrimerRiesgo() {
		return esPrimerRiesgo;
	}
	public void setEsPrimerRiesgo(Boolean esPrimerRiesgo) {
		this.esPrimerRiesgo = esPrimerRiesgo;
	}
	public Double getMontoPrimaNoDevengada() {
		return montoPrimaNoDevengada;
	}
	public void setMontoPrimaNoDevengada(Double montoPrimaNoDevengada) {
		this.montoPrimaNoDevengada = montoPrimaNoDevengada;
	}
}