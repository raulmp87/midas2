package mx.com.afirme.midas.reaseguro.soporte;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;


/**
 * Remote interface for SoporteReaseguroDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SoporteReaseguroFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SoporteReaseguroDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SoporteReaseguroDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public SoporteReaseguroDTO save(SoporteReaseguroDTO entity);
	
	
	public void save(BigDecimal idToSoporteReaseguroDTO,List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTO);

	/**
	 * Delete a persistent SoporteReaseguroDTO entity.
	 * 
	 * @param entity
	 *            SoporteReaseguroDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SoporteReaseguroDTO entity);

	/**
	 * Persist a previously saved SoporteReaseguroDTO entity and return it or a
	 * copy of it to the sender. A copy of the SoporteReaseguroDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SoporteReaseguroDTO entity to update
	 * @return SoporteReaseguroDTO the persisted SoporteReaseguroDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SoporteReaseguroDTO update(SoporteReaseguroDTO entity);

	public SoporteReaseguroDTO findById(BigDecimal id);

	/**
	 * Find all SoporteReaseguroDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SoporteReaseguroDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SoporteReaseguroDTO> found by query
	 */
	public List<SoporteReaseguroDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SoporteReaseguroDTO entities.
	 * 
	 * @return List<SoporteReaseguroDTO> all SoporteReaseguroDTO entities
	 */
	public List<SoporteReaseguroDTO> findAll();
	
	public LineaDTO getLineaDTO(Date fechaInicioVigencia, BigDecimal idSubramo,BigDecimal estatus);
	
	public LineaDTO getLineaDTO(BigDecimal idTmLinea);
	
	public SoporteReaseguroDTO getPorIdCotizacion(BigDecimal idToCotizacion);
	
	public List<SoporteReaseguroDTO> obtenerSoporteReaseguroPorEstatusLineas(
			Integer estatusLineaSoporte);
	
	public SoporteReaseguroDTO obtenerSoporteReaseguroPorPolizaEndoso(BigDecimal idToPoliza, Integer numeroEndoso);

	public boolean notificarEmisionEndoso(SoporteReaseguroDTO soporteReaseguroOriginal, BigDecimal idToPoliza,int numeroEndoso,
			BigDecimal idToCotizacionEndosoCancelacion,Date fechaEmisionCancelacion,Date fechaInicioVigencia,BigDecimal factorPrima,short tipoEndoso,List<String> listaErrores,boolean omitirErrorSoporteExistente);
	
	public double obtenerDiferenciaPrimaSoportePoliza(BigDecimal idToPoliza,Integer numeroEndoso);
	
	public SoporteReaseguroDTO notificarEmision(BigDecimal idToSoporteReaseguro, BigDecimal idToPoliza,int numeroEndoso,Date fechaEmision);
	
	public List<String> validarSoporteReaseguroDTOListoParaDistribucion(BigDecimal idToPoliza,int numeroEndoso);
	
	public List<String> validarSoporteReaseguroDTOListoParaDistribucion(BigDecimal idToSoporteReaseguro);
	
	public List<BigDecimal> consultarSoportesPendientesDistribucion();
	
	public boolean actualizarBanderaLineasAplicanDistribucion(BigDecimal idToSoporteReaseguro);
	
	public BigDecimal calcularFactorAplicacionEndosoCancelacionRehabilitacion(BigDecimal idToPoliza,int numeroEndosoCanceladoRehabilitado,short claveTipoEndoso);
	
	public void aplicarFactorAjusteSoporteReaseguro(EndosoDTO endosoEmitido,BigDecimal factorAjuste);
	
	public LineaDTO obtenerLineaDTOVigente(Date fechaInicioVigencia, BigDecimal idSubramo);
	
	public LineaDTO obtenerLineaDTO(Date fechaInicioVigencia, BigDecimal idSubramo);
	
	public List<SoporteReaseguroDTO> listarPorIdToCotizacion(BigDecimal idToCotizacion);
}