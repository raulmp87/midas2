package mx.com.afirme.midas.reaseguro.soporte;
// default package




import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoDTO;


/**
 * Remote interface for LineaSoporteCoberturaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface LineaSoporteCoberturaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved LineaSoporteCoberturaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            LineaSoporteCoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public LineaSoporteCoberturaDTO save(LineaSoporteCoberturaDTO entity);

	/**
	 * Delete a persistent LineaSoporteCoberturaDTO entity.
	 * 
	 * @param entity
	 *            LineaSoporteCoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(LineaSoporteCoberturaDTO entity);

	/**
	 * Persist a previously saved LineaSoporteCoberturaDTO entity and return it
	 * or a copy of it to the sender. A copy of the LineaSoporteCoberturaDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            LineaSoporteCoberturaDTO entity to update
	 * @return LineaSoporteCoberturaDTO the persisted LineaSoporteCoberturaDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public LineaSoporteCoberturaDTO update(LineaSoporteCoberturaDTO entity);

	public LineaSoporteCoberturaDTO findById(LineaSoporteCoberturaDTOId id);

	/**
	 * Find all LineaSoporteCoberturaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the LineaSoporteCoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<LineaSoporteCoberturaDTO> found by query
	 */
	public List<LineaSoporteCoberturaDTO> findByProperty(String propertyName,
			Object value);
	
	public List<LineaSoporteCoberturaDTO> obtenPorLlaveCobertura(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO);

	public List<LineaSoporteCoberturaDTO> findByPorcentajeCoaseguro(
			Object porcentajeCoaseguro);

	public List<LineaSoporteCoberturaDTO> findByPorcentajeDeducible(
			Object porcentajeDeducible);

	public List<LineaSoporteCoberturaDTO> findByMontoPrimaSuscripcion(
			Object montoPrimaSuscripcion);

	public List<LineaSoporteCoberturaDTO> findByMontoPrimaFacultativo(
			Object montoPrimaFacultativo);

	public List<LineaSoporteCoberturaDTO> findByAplicaControlReclamo(
			Object aplicaControlReclamo);

	/**
	 * Find all LineaSoporteCoberturaDTO entities.
	 * 
	 * @return List<LineaSoporteCoberturaDTO> all LineaSoporteCoberturaDTO
	 *         entities
	 */
	public List<LineaSoporteCoberturaDTO> findAll();
	
	public LineaSoporteCoberturaDTO getLineaSoporteCoberturaDTOByDetalleContratoFacultativoDTO(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO);
	
	public void eliminarLineasSoporteCoberturaPorLineaSoporteReaseguro(BigDecimal idTmLineaSoporteReaseguro);
	
	public void notificarEmisionCoberturasSoporteReaseguro(BigDecimal idToSoporteReaseguro,BigDecimal idToPoliza);
	
	public void integrarCoberturasSoporteReaseguro(BigDecimal idTmLineaSoporteReaseguro);
	
	public List<LineaSoporteCoberturaDTO> listarFiltrado(LineaSoporteCoberturaDTO entity);
}