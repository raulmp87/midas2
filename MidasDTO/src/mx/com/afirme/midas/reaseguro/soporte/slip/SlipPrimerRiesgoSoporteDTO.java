
package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.util.List;

public class SlipPrimerRiesgoSoporteDTO {
	
	public static final String DESCRIPCION = "Coberturas Primer Riesgo";
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static final int TIPO = 9;	
	
	
	private String descripcion;
	
	/*
	 * Inciso x Cobertura - Cuotas y primas (Alfanumérico) - Opcional con opción de cargar un archivo adjunto
	 * Inciso x Cobertura - Deducibles y coaseguros (Alfanumérico) - Opcional
	 */
	private List<CoberturaSoporteDTO> listaCoberturas;

	public List<CoberturaSoporteDTO> getListaCoberturas() {
		return listaCoberturas;
	}

	public void setListaCoberturas(List<CoberturaSoporteDTO> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}		

}
