package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;

public class SlipAnexoSoporteDTO {
	
	private String url;
	
	private BigDecimal idToSlipDocumentoAnexo;
	
	private BigDecimal idToControlArchivo;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public BigDecimal getIdToSlipDocumentoAnexo() {
		return idToSlipDocumentoAnexo;
	}

	public void setIdToSlipDocumentoAnexo(BigDecimal idToSlipDocumentoAnexo) {
		this.idToSlipDocumentoAnexo = idToSlipDocumentoAnexo;
	}

	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}
	
	

}
