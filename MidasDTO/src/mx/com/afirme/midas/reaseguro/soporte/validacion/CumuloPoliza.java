package mx.com.afirme.midas.reaseguro.soporte.validacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import mx.com.afirme.midas.contratos.linea.LineaDTO;

public class CumuloPoliza implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idSubRamo;
	private BigDecimal idCotizacion;
	private Double sumaAsegurada;
	private Integer numeroInciso;
	private BigDecimal idSeccion;
	private Integer numeroSubInciso;
	private Integer tipoDistribucion;
	private Double porcentajePleno;
	private Boolean aplicaPrimerRiesgo;
	private List<DetalleCobertura> detalleCoberturas;
	private Date fechaInicioVigencia;
	private BigDecimal idMoneda;
	private BigDecimal idToPersonaAsegurado;
	private LineaDTO lineaDTO;
	
	public CumuloPoliza(){
		
	}
	
	public CumuloPoliza(BigDecimal idCotizacion, BigDecimal idSubRamo,Integer numeroInciso,Integer numeroSubInciso,BigDecimal idSeccion,LineaDTO lineaDTO,Integer tipoDistribucion){
		this.idSubRamo = idSubRamo;
		this.idCotizacion = idCotizacion;
		this.numeroInciso = numeroInciso;
		this.idSeccion = idSeccion;
		this.numeroSubInciso = numeroSubInciso;
		this.lineaDTO = lineaDTO;
		this.tipoDistribucion = tipoDistribucion;
	}
	
	public Hashtable<String,Object> toHashTable(){
		Hashtable<String,Object> cumuloHashtable = new Hashtable<String,Object>();
		if (idSubRamo != null)
			cumuloHashtable.put("idSubRamo", idSubRamo);
		if (idCotizacion != null)
			cumuloHashtable.put("idCotizacion", idCotizacion);
		if (sumaAsegurada != null)
			cumuloHashtable.put("sumaAsegurada", sumaAsegurada);
		if (numeroInciso != null)
			cumuloHashtable.put("numeroInciso", numeroInciso);
		if (idSeccion != null)
			cumuloHashtable.put("idSeccion", idSeccion);
		if (numeroSubInciso != null)
			cumuloHashtable.put("numeroSubInciso", numeroSubInciso);
		if (tipoDistribucion != null)
			cumuloHashtable.put("tipoDistribucion", tipoDistribucion);
		if (porcentajePleno != null)
			cumuloHashtable.put("porcentajePleno", porcentajePleno);
		if (aplicaPrimerRiesgo != null)
			cumuloHashtable.put("aplicaPrimerRiesgo", aplicaPrimerRiesgo);
		List<Hashtable<String,Object>> listaDetalleCoberturasHashtable = new ArrayList<Hashtable<String,Object>>();
		for(DetalleCobertura detalleCobertura : detalleCoberturas){
			listaDetalleCoberturasHashtable.add(detalleCobertura.toHashTable());
		}
		cumuloHashtable.put("detalleCoberturas", listaDetalleCoberturasHashtable);
		if (fechaInicioVigencia != null)
			cumuloHashtable.put("fechaInicioVigencia", fechaInicioVigencia);
		if (idMoneda != null)
			cumuloHashtable.put("idMoneda", idMoneda);
		if (idToPersonaAsegurado != null)
			cumuloHashtable.put("idToPersonaAsegurado", idToPersonaAsegurado);
		return cumuloHashtable;
	}
	public BigDecimal getIdSubRamo() {
		return idSubRamo;
	}
	public void setIdSubRamo(BigDecimal idSubRamo) {
		this.idSubRamo = idSubRamo;
	}
	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	public Double getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public BigDecimal getIdSeccion() {
		return idSeccion;
	}
	public void setIdSeccion(BigDecimal idSeccion) {
		this.idSeccion = idSeccion;
	}
	public Integer getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(Integer numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	public Integer getTipoDistribucion() {
		return tipoDistribucion;
	}
	public void setTipoDistribucion(Integer tipoDistribucion) {
		this.tipoDistribucion = tipoDistribucion;
	}
	public Double getPorcentajePleno() {
		return porcentajePleno;
	}
	public void setPorcentajePleno(Double porcentajePleno) {
		this.porcentajePleno = porcentajePleno;
	}
	public Boolean getAplicaPrimerRiesgo() {
		return aplicaPrimerRiesgo;
	}
	public void setAplicaPrimerRiesgo(Boolean aplicaPrimerRiesgo) {
		this.aplicaPrimerRiesgo = aplicaPrimerRiesgo;
	}
	public List<DetalleCobertura> getDetalleCoberturas() {
		return detalleCoberturas;
	}
	public void setDetalleCoberturas(List<DetalleCobertura> detalleCoberturas) {
		this.detalleCoberturas = detalleCoberturas;
	}
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public BigDecimal getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}
	public BigDecimal getIdToPersonaAsegurado() {
		return idToPersonaAsegurado;
	}
	public void setIdToPersonaAsegurado(BigDecimal idToPersonaAsegurado) {
		this.idToPersonaAsegurado = idToPersonaAsegurado;
	}
	public LineaDTO getLineaDTO() {
		return lineaDTO;
	}
	public void setLineaDTO(LineaDTO lineaDTO) {
		this.lineaDTO = lineaDTO;
	}
}