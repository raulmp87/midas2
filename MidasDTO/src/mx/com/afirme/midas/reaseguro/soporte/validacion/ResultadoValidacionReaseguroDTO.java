package mx.com.afirme.midas.reaseguro.soporte.validacion;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;

/**
 * @author jose luis arellano
 */
public class ResultadoValidacionReaseguroDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String mensajeErrorCumulos;
	private List<LineaSoporteReaseguroDTO> listaCumuloPoliza;
	private List<LineaSoporteReaseguroDTO> listaCumuloInciso;
	private List<LineaSoporteReaseguroDTO> listaCumuloSubInciso;
	
	
	public String getMensajeErrorCumulos() {
		return mensajeErrorCumulos;
	}
	public void setMensajeErrorCumulos(String mensajeErrorCumulos) {
		this.mensajeErrorCumulos = mensajeErrorCumulos;
	}
	public List<LineaSoporteReaseguroDTO> getListaCumuloPoliza() {
		return listaCumuloPoliza;
	}
	public void setListaCumuloPoliza(
			List<LineaSoporteReaseguroDTO> listaCumuloPoliza) {
		this.listaCumuloPoliza = listaCumuloPoliza;
	}
	public List<LineaSoporteReaseguroDTO> getListaCumuloInciso() {
		return listaCumuloInciso;
	}
	public void setListaCumuloInciso(
			List<LineaSoporteReaseguroDTO> listaCumuloInciso) {
		this.listaCumuloInciso = listaCumuloInciso;
	}
	public List<LineaSoporteReaseguroDTO> getListaCumuloSubInciso() {
		return listaCumuloSubInciso;
	}
	public void setListaCumuloSubInciso(
			List<LineaSoporteReaseguroDTO> listaCumuloSubInciso) {
		this.listaCumuloSubInciso = listaCumuloSubInciso;
	}
	
}
