package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;
import java.util.List;

public class SlipEquipoContratistaSoporteDTO {
	

	public static final int TIPO = 3;	
	
	/**
	 * Obtenidos de DAÑOS
	 */	
	private BigDecimal numeroInciso;
	private BigDecimal numeroSubInciso;
	private BigDecimal idToSeccion;
	private String ubicacion; // Inciso - Ubicación (Alfanumérico) - Opcional con opción de cargar un archivo adjunto
	private String incisoMayorValor; // Inciso - *Inciso de mayor valor (Alfanumérico) - Obligatorio
	
	private String descripcionSeccion;
	private String descripcionSubInciso;	

	/*
	 * Inciso - Cuotas y primas (Alfanumérico) - Opcional con opción de cargar un archivo adjunto
	 * Inciso - Deducibles y coaseguros (Alfanumérico) - Opcional
	 */
	private List<CoberturaSoporteDTO> listaCoberturas;	
	private List<CoberturaSoporteDTO> listaCoberturasInciso;
	
	
	
	public List<CoberturaSoporteDTO> getListaCoberturasInciso() {
		return listaCoberturasInciso;
	}

	public void setListaCoberturasInciso(
			List<CoberturaSoporteDTO> listaCoberturasInciso) {
		this.listaCoberturasInciso = listaCoberturasInciso;
	}

	private List<SumaAseguradaSoporteDTO>  bienesAsegurados;
	private List<SumaAseguradaSoporteDTO> coberturasBienes;	
	
	private List<SlipAnexoSoporteDTO> anexos;	

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getIncisoMayorValor() {
		return incisoMayorValor;
	}

	public void setIncisoMayorValor(String incisoMayorValor) {
		this.incisoMayorValor = incisoMayorValor;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}


	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}

	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}

	public String getDescripcionSubInciso() {
		return descripcionSubInciso;
	}

	public void setDescripcionSubInciso(String descripcionSubInciso) {
		this.descripcionSubInciso = descripcionSubInciso;
	}

	
	public List<CoberturaSoporteDTO> getListaCoberturas() {
		return listaCoberturas;
	}

	public void setListaCoberturas(List<CoberturaSoporteDTO> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}

	public List<SumaAseguradaSoporteDTO> getBienesAsegurados() {
		return bienesAsegurados;
	}

	public void setBienesAsegurados(List<SumaAseguradaSoporteDTO> bienesAsegurados) {
		this.bienesAsegurados = bienesAsegurados;
	}

	public List<SumaAseguradaSoporteDTO> getCoberturasBienes() {
		return coberturasBienes;
	}

	public void setCoberturasBienes(List<SumaAseguradaSoporteDTO> coberturasBienes) {
		this.coberturasBienes = coberturasBienes;
	}

	public List<SlipAnexoSoporteDTO> getAnexos() {
		return anexos;
	}

	public void setAnexos(List<SlipAnexoSoporteDTO> anexos) {
		this.anexos = anexos;
	}
	
	

}
