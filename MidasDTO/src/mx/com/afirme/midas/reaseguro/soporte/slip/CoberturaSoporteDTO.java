package mx.com.afirme.midas.reaseguro.soporte.slip;

import java.math.BigDecimal;

public class CoberturaSoporteDTO {
    
    	//Laves para la cobertura
    	private BigDecimal idToCotizacion;	
	private BigDecimal numeroInciso;
	private BigDecimal idToSeccion;
	private BigDecimal numeroSubInciso;
	private BigDecimal idToCobertura;
	

	private String descripcionSeccion;
	
	private String descripcionSubRamo;
	
	private String descripcionCobertura;
	
	private BigDecimal sumaAsegurada;
	
	private BigDecimal prima;
	
	private BigDecimal cuota;
	
	private BigDecimal coaseguro;	
	
	private BigDecimal deducible;
	
	private Integer claveTipoSumaAsegurada;
	
	

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}

	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}

	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}

	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}

	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public BigDecimal getPrima() {
		return prima;
	}

	public void setPrima(BigDecimal prima) {
		this.prima = prima;
	}

	public BigDecimal getCuota() {
		return cuota;
	}

	public void setCuota(BigDecimal cuota) {
		this.cuota = cuota;
	}

	public BigDecimal getCoaseguro() {
		return coaseguro;
	}

	public void setCoaseguro(BigDecimal coaseguro) {
		this.coaseguro = coaseguro;
	}

	public BigDecimal getDeducible() {
		return deducible;
	}

	public void setDeducible(BigDecimal deducible) {
		this.deducible = deducible;
	}

	public Integer getClaveTipoSumaAsegurada() {
		return claveTipoSumaAsegurada;
	}

	public void setClaveTipoSumaAsegurada(Integer claveTipoSumaAsegurada) {
		this.claveTipoSumaAsegurada = claveTipoSumaAsegurada;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}

	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}
	
	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
	    this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToCotizacion() {
	    return idToCotizacion;
	}	

}
