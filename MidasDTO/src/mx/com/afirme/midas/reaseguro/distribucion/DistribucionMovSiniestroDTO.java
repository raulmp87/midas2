package mx.com.afirme.midas.reaseguro.distribucion;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDetalleDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;

/**
 * DistribucionMovSiniestroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TODISTRIBUCIONMOVSINIESTRO", schema = "MIDAS")
public class DistribucionMovSiniestroDTO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int ESTATUS_SIN_DISTRIBUIR = 1;
	public static final int ESTATUS_DISTRIBUIDO = 2;
	public static final String NOTA_SISTEMA_SIN_DISTRIBUIR = "Sin Distribuir";
	public static final String NOTA_SISTEM_DISTRIBUIDO = "Distribuido";
	// Fields

	private BigDecimal idToDistribucionMovSiniestro;
	private LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO;
	private ConceptoMovimientoDetalleDTO conceptoMovimientoDetalleDTO;
	private BigDecimal idMovimientoSiniestro;
	private Integer tipoMovimiento;
	private BigDecimal idToPoliza;
	private Integer numeroEndoso;
	private BigDecimal idToSeccion;
	private BigDecimal idToCobertura;
	private Integer numeroInciso;
	private Integer numeroSubInciso;
	private BigDecimal idTcMoneda;
	private Date fechaMovimiento;
	private BigDecimal monto;
	private BigDecimal idToReporteSiniestro;
	private Integer estatus;
	private String NotaDelSistema;
	private Date fechaModificacion;
	private Date fechaCreacion;

	// Constructors

	/** default constructor */
	public DistribucionMovSiniestroDTO() {
	}

	/** minimal constructor */
	public DistribucionMovSiniestroDTO(BigDecimal idToDistribucionMovSiniestro,
			ConceptoMovimientoDetalleDTO conceptoMovimientoDetalleDTO,
			BigDecimal idMovimientoSiniestro, Integer tipoMovimiento,
			BigDecimal idToPoliza, Integer numeroEndoso, BigDecimal idTcMoneda,
			Date fechaMovimiento, BigDecimal monto,
			BigDecimal idToReporteSiniestro, Integer estatus,
			Date fechaModificacion, Date fechaCreacion) {
		this.idToDistribucionMovSiniestro = idToDistribucionMovSiniestro;
		this.conceptoMovimientoDetalleDTO = conceptoMovimientoDetalleDTO;
		this.idMovimientoSiniestro = idMovimientoSiniestro;
		this.tipoMovimiento = tipoMovimiento;
		this.idToPoliza = idToPoliza;
		this.numeroEndoso = numeroEndoso;
		this.idTcMoneda = idTcMoneda;
		this.fechaMovimiento = fechaMovimiento;
		this.monto = monto;
		this.idToReporteSiniestro = idToReporteSiniestro;
		this.estatus = estatus;
		this.fechaModificacion = fechaModificacion;
		this.fechaCreacion = fechaCreacion;
	}

	/** full constructor */
	public DistribucionMovSiniestroDTO(BigDecimal idToDistribucionMovSiniestro,
			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,
			ConceptoMovimientoDetalleDTO conceptoMovimientoDetalleDTO,
			BigDecimal idMovimientoSiniestro, Integer tipoMovimiento,
			BigDecimal idToPoliza, Integer numeroEndoso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			Integer numeroInciso, Integer numeroSubInciso,
			BigDecimal idTcMoneda, Date fechaMovimiento, BigDecimal monto,
			BigDecimal idToReporteSiniestro, Integer estatus,
			String NotaDelSistema, Date fechaModificacion, Date fechaCreacion) {
		this.idToDistribucionMovSiniestro = idToDistribucionMovSiniestro;
		this.lineaSoporteReaseguroDTO = lineaSoporteReaseguroDTO;
		this.conceptoMovimientoDetalleDTO = conceptoMovimientoDetalleDTO;
		this.idMovimientoSiniestro = idMovimientoSiniestro;
		this.tipoMovimiento = tipoMovimiento;
		this.idToPoliza = idToPoliza;
		this.numeroEndoso = numeroEndoso;
		this.idToSeccion = idToSeccion;
		this.idToCobertura = idToCobertura;
		this.numeroInciso = numeroInciso;
		this.numeroSubInciso = numeroSubInciso;
		this.idTcMoneda = idTcMoneda;
		this.fechaMovimiento = fechaMovimiento;
		this.monto = monto;
		this.idToReporteSiniestro = idToReporteSiniestro;
		this.estatus = estatus;
		this.NotaDelSistema = NotaDelSistema;
		this.fechaModificacion = fechaModificacion;
		this.fechaCreacion = fechaCreacion;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTODISTRIBUCIONMOVSIN_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTODISTRIBUCIONMOVSIN_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTODISTRIBUCIONMOVSIN_SEQ_GENERADOR")
	@Column(name = "IDTODISTRIBUCIONMOVSINIESTRO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToDistribucionMovSiniestro() {
		return this.idToDistribucionMovSiniestro;
	}

	public void setIdToDistribucionMovSiniestro(
			BigDecimal idToDistribucionMovSiniestro) {
		this.idToDistribucionMovSiniestro = idToDistribucionMovSiniestro;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns( {
			@JoinColumn(name = "IDTMLINEASOPORTEREASEGURO", referencedColumnName = "IDTMLINEASOPORTEREASEGURO"),
			@JoinColumn(name = "IDTOSOPORTEREASEGURO", referencedColumnName = "IDTOSOPORTEREASEGURO") })
	public LineaSoporteReaseguroDTO getLineaSoporteReaseguroDTO() {
		return this.lineaSoporteReaseguroDTO;
	}

	public void setLineaSoporteReaseguroDTO(
			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) {
		this.lineaSoporteReaseguroDTO = lineaSoporteReaseguroDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCCONCEPTODETALLE", nullable = false)
	public ConceptoMovimientoDetalleDTO  getConceptoMovimientoDetalleDTO() {
		return this.conceptoMovimientoDetalleDTO;
	}

	public void setConceptoMovimientoDetalleDTO(
			ConceptoMovimientoDetalleDTO  conceptoMovimientoDetalleDTO) {
		this.conceptoMovimientoDetalleDTO = conceptoMovimientoDetalleDTO;
	}

	@Column(name = "IDMOVIMIENTOSINIESTRO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdMovimientoSiniestro() {
		return this.idMovimientoSiniestro;
	}

	public void setIdMovimientoSiniestro(BigDecimal idMovimientoSiniestro) {
		this.idMovimientoSiniestro = idMovimientoSiniestro;
	}

	@Column(name = "TIPOMOVIMIENTO", nullable = false, precision = 2, scale = 0)
	public Integer getTipoMovimiento() {
		return this.tipoMovimiento;
	}

	public void setTipoMovimiento(Integer tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	@Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToPoliza() {
		return this.idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@Column(name = "NUMEROENDOSO", nullable = false, precision = 4, scale = 0)
	public Integer getNumeroEndoso() {
		return this.numeroEndoso;
	}

	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	@Column(name = "IDTOSECCION", precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "IDTOCOBERTURA", precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	@Column(name = "NUMEROINCISO", precision = 4, scale = 0)
	public Integer getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "NUMEROSUBINCISO", precision = 4, scale = 0)
	public Integer getNumeroSubInciso() {
		return this.numeroSubInciso;
	}

	public void setNumeroSubInciso(Integer numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	@Column(name = "IDTCMONEDA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcMoneda() {
		return this.idTcMoneda;
	}

	public void setIdTcMoneda(BigDecimal idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAMOVIMIENTO", nullable = false, length = 7)
	public Date getFechaMovimiento() {
		return this.fechaMovimiento;
	}

	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}

	@Column(name = "MONTO", nullable = false, precision = 18, scale = 4)
	public BigDecimal getMonto() {
		return this.monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	@Column(name = "IDTOREPORTESINIESTRO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToReporteSiniestro() {
		return this.idToReporteSiniestro;
	}

	public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}

	@Column(name = "ESTATUS", nullable = false, precision = 22, scale = 0)
	public Integer getEstatus() {
		return this.estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	@Column(name = "NOTADELSISTEMA", length = 500)
	public String getNotaDelSistema() {
		return this.NotaDelSistema;
	}

	public void setNotaDelSistema(String NotaDelSistema) {
		this.NotaDelSistema = NotaDelSistema;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAMODIFICACION", nullable = false, length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}