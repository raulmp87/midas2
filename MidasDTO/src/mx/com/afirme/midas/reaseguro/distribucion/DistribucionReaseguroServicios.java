package mx.com.afirme.midas.reaseguro.distribucion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;


public interface DistribucionReaseguroServicios {
	/**
	 * Este metodo distribuye las primas de las coberturas del SoporteReaseguro enviado
	 * Nota: Este metodo requiere que el soporteReaseguroDTO este inicializado con sus
	 * respectivas relaciones, es decir haberlo obtenido. 
	 * 
	 * @param soporteReaseguroDTO
	 */
//	public List<MovimientoReaseguroDTO> distribuirPoliza(SoporteReaseguroDTO soporteReaseguroDTO);
	
	public void distribuirLineaSoporteReaseguro(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO);
	
	/**
	 * Este metodo distribuye las primas de las coberturas de la poliza - endoso
	 * indicados
	 * 
	 * @param idToPoliza
	 * @param numeroEndoso
	 */
//	public List<MovimientoReaseguroDTO> distribuirPoliza(BigDecimal idToPoliza, Integer numeroEndoso);
	
	/**
	 * Este metodo distribuye las primas para todas las LineasSoporteCobertura que 
	 * tengan el estatus 2. Metodo masivo de distribucion.
	 * 
	 */
	public void distribuirPrimas();
	
	
	/**
	 * Metodo que funciona como facade para englobar los dos procesos el de distribucion
	 * y el de contabilizacion. Se declara la transaccion como REQUIRES_NEW
	 * para poder evitar que cuando se llamado por el temporizador se haga rollback.
	 */
//	public void distribuirYContabilizar(BigDecimal idToPoliza, Integer numeroEndoso);
	
	/**
	 * Este Metodo distribuiye las Cuentas por Pagar que se generen relacionados
	 * a un Siniestro convirtiendolas en Cuentas por Cobrar a Reaseguro al
	 * distribuir. <br>
	 * Valores esperados<br>
	 * 
	 * @param idToPoliza
	 *            Es el identificador de la P�liza a la que se le relaciona el
	 *            movimiento de siniestro.
	 * @param numeroEndoso
	 *            Es el identificador del Endoso al que se le relaciona el
	 *            movimiento de siniestro.
	 * @param idTcSubrramo
	 *            Es el identificador del subRamo de la Cobertura.
	 * @param idToCobertura
	 *            Es el identificador al que pertenece la Cobertura.
	 * @param idToReporteSiniestro
	 *            Es el identificador del Siniestro
	 * @param conceptoMovimiento
	 *            Es la clave del conceptop del movimiento: <br>
	 *            Conceptos de Siniestros<br>
	 *            Indemnizaci�n 5<br>
	 *            Cancelaci�n de Indemnizaci�n 6<br>
	 *            Deducible 7<br>
	 *            Cancelaci�n de Decucible 8<br>
	 *            Gastos de Ajuste 9<br>
	 *            Salvamento 10<br>
	 *            Cancelar Salvamento 11<br>
	 *            Recuperaci�n Legal 12<br>
	 *            Cancelar Recuperaci�n Legal 14<br>
	 *            Cancelaci�n de Gasto de Ajuste <br>
	 *            Reserva Inicial <br>
	 *            Ajuste de m�s en Reserva <br>
	 *            Ajuste de menos en Reserva <br>
	 * 
	 *<br>
	 * @param fechaRegistroMovimiento
	 *            Es la fecha en la que se realiza el registro del Movimiento
	 * @param tipoMovimiento
	 *            Es el tipo de movimiento que se va a distribuir desde la
	 *            perspectiva de Siniestros Tipo de Movimiento<br>
	 *            Cuenta por Pagar 1<br>
	 *            Cuenta por Cobrar 2<br>
	 * @param idMovimientoSiniestro
	 *            Este es el identificador del movimiento de Siniestro que se
	 *            registra por cada distribucion en movimientos de reaseguro
	 *            como referencia.
	 * @param montoSiniestro
	 */
	public List<MovimientoReaseguroDTO> distribuirMontoSiniestro(
			BigDecimal idToPoliza, Integer numeroEndoso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			Integer numeroInciso, Integer numeroSubInciso,
			Integer conceptoMovimiento,	BigDecimal idToMoneda, 
			Date fechaMovimiento, BigDecimal idMovimientoSiniestro,
			BigDecimal montoMovimiento, Integer tipoMovimiento,
			BigDecimal idToReporteSiniestro, 
			BigDecimal idToDistribucionMovSiniestro);
	
	/** 
	 * Este metodo distribuye un SoporteCobertura y registra estos movimientos. 
	 * Este podria ser util cuando se requiere distribuir un solo SoporteCobertura. 
	 *
	 * Para distribucion de toda una poliza para un endoso dado se recomienda utiliza
	 * distribuirPoliza(BigDecimal idToPoliza, Integer numeroEndoso).
	 * 
	 * Para distribucion masiva utilizar distribuirPrimas().
	 */
	public void distribuirCobertura(BigDecimal idToPoliza, Integer numeroEndoso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			Integer numeroInciso, Integer numeroSubInciso);
	
	/**
	 * Obtiene la LineaSoporteReaseguroDTO que esta asociada 
	 * con los parametros que se estan enviando.
	 */
	public LineaSoporteReaseguroDTO getPorcentajeDistribucion(
			BigDecimal idToPoliza, Integer numeroEndoso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			Integer numeroInciso, Integer numeroSubInciso);
}
