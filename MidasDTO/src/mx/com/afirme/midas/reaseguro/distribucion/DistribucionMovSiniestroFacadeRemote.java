package mx.com.afirme.midas.reaseguro.distribucion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

/**
 * Remote interface for DistribucionMovSiniestroDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface DistribucionMovSiniestroFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * DistribucionMovSiniestroDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            DistribucionMovSiniestroDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DistribucionMovSiniestroDTO entity);

	/**
	 * Delete a persistent DistribucionMovSiniestroDTO entity.
	 * 
	 * @param entity
	 *            DistribucionMovSiniestroDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DistribucionMovSiniestroDTO entity);

	/**
	 * Persist a previously saved DistribucionMovSiniestroDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * DistribucionMovSiniestroDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            DistribucionMovSiniestroDTO entity to update
	 * @return DistribucionMovSiniestroDTO the persisted
	 *         DistribucionMovSiniestroDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DistribucionMovSiniestroDTO update(DistribucionMovSiniestroDTO entity);

	public DistribucionMovSiniestroDTO findById(BigDecimal id);

	/**
	 * Find all DistribucionMovSiniestroDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the DistribucionMovSiniestroDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DistribucionMovSiniestroDTO> found by query
	 */
	public List<DistribucionMovSiniestroDTO> findByProperty(
			String propertyName, Object value);

	public List<DistribucionMovSiniestroDTO> findByTipoMovimiento(
			Object tipoMovimiento);

	public List<DistribucionMovSiniestroDTO> findByNumeroEndoso(
			Object numeroEndoso);

	public List<DistribucionMovSiniestroDTO> findByNumeroInciso(
			Object numeroInciso);

	public List<DistribucionMovSiniestroDTO> findByNumeroSubInciso(
			Object numeroSubInciso);

	public List<DistribucionMovSiniestroDTO> findByEstatus(Object estatus);

	public List<DistribucionMovSiniestroDTO> findByNotaDelSistema(
			Object NotaDelSistema);

	/**
	 * Find all DistribucionMovSiniestroDTO entities.
	 * 
	 * @return List<DistribucionMovSiniestroDTO> all DistribucionMovSiniestroDTO
	 *         entities
	 */
	public List<DistribucionMovSiniestroDTO> findAll();
	
	@Deprecated
	public DistribucionMovSiniestroDTO registrarMovimiento(BigDecimal idToPoliza, Integer numeroEndoso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			Integer numeroInciso, Integer numeroSubInciso,
			Integer conceptoMovimiento,	BigDecimal idToMoneda, 
			Date fechaMovimiento, BigDecimal idMovimientoSiniestro,
			BigDecimal montoMovimiento, Integer tipoMovimiento,
			BigDecimal idToReporteSiniestro);
	
	public List<DistribucionMovSiniestroDTO> listarFiltrado(DistribucionMovSiniestroDTO entity );
	
	public List<DistribucionMovSiniestroDTO> obtenerMovimientosNoDistribuidos(BigDecimal idMovimientoSiniestro, BigDecimal idToReporteSiniestro,int idConceptoDetalle);
	
	public Map<String, String> registrarMovimientoDistribucionSiniestro(BigDecimal idToPoliza,
			Integer numeroEndoso, BigDecimal idToSeccion, BigDecimal idToCobertura, Integer numeroInciso,
			Integer numeroSubInciso, Integer conceptoMovimiento, BigDecimal idToMoneda, Date fechaMovimiento,
			BigDecimal idMovimientoSiniestro, BigDecimal montoMovimiento, Integer tipoMovimiento, BigDecimal idToReporteSiniestro);
}