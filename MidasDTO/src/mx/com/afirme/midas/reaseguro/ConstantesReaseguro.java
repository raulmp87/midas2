package mx.com.afirme.midas.reaseguro;

public abstract class ConstantesReaseguro {

	public static final int TIPO_CUMULO_POLIZA=1;
	public static final int TIPO_CUMULO_INCISO=2;
	public static final int TIPO_CUMULO_SUBINCISO=3;
}
