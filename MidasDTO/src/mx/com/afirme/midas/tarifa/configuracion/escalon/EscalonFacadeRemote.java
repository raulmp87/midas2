package mx.com.afirme.midas.tarifa.configuracion.escalon;
// default package

import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for EscalonDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface EscalonFacadeRemote extends MidasInterfaceBase<EscalonDTO>{
		/**
	 Perform an initial save of a previously unsaved EscalonDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EscalonDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EscalonDTO entity);
    /**
	 Delete a persistent EscalonDTO entity.
	  @param entity EscalonDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EscalonDTO entity);
   /**
	 Persist a previously saved EscalonDTO entity and return it or a copy of it to the sender. 
	 A copy of the EscalonDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EscalonDTO entity to update
	 @return EscalonDTO the persisted EscalonDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public EscalonDTO update(EscalonDTO entity);
	 /**
	 * Find all EscalonDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EscalonDTO property to query
	  @param value the property value to match
	  	  @return List<EscalonDTO> found by query
	 */
	public List<EscalonDTO> findByProperty(String propertyName, Object value
		);

	/**
	 * Find all EscalonDTO entities.
	  	  @return List<EscalonDTO> all EscalonDTO entities
	 */
	public List<EscalonDTO> findAll(
		);	
}