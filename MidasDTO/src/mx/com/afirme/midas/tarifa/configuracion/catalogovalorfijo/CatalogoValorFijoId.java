package mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo;
// default package

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * catalogoValorFijoId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class CatalogoValorFijoId  implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields    

     private int idGrupoValores;
     private int idDato;

    // Constructors

    /** default constructor */
    public CatalogoValorFijoId() {
    }

    
    /** full constructor */
    public CatalogoValorFijoId(int idGrupoValores, int idDato) {
        this.idGrupoValores = idGrupoValores;
        this.idDato = idDato;
    }

   
    // Property accessors

    @Column(name="IDGRUPOVALORES", nullable=false, precision=22, scale=0)

    public int getIdGrupoValores() {
        return this.idGrupoValores;
    }
    
    public void setIdGrupoValores(int idGrupoValores) {
        this.idGrupoValores = idGrupoValores;
    }

    @Column(name="IDDATO", nullable=false, precision=22, scale=0)

    public int getIdDato() {
        return this.idDato;
    }
    
    public void setIdDato(int idDato) {
        this.idDato = idDato;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof CatalogoValorFijoId) ) return false;
		 CatalogoValorFijoId castOther = ( CatalogoValorFijoId ) other; 
         
		 return (this.getIdGrupoValores()==castOther.getIdGrupoValores())
 && (this.getIdDato()==castOther.getIdDato());
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getIdGrupoValores();
         result = 37 * result + this.getIdDato();
         return result;
   }   





}