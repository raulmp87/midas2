package mx.com.afirme.midas.tarifa.configuracion.escalon;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;


/**
 * EscalonDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOESCALON"    ,schema="MIDAS")

public class EscalonDTO  extends CacheableDTO{


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields    

     private double idToEscalon;
     private Short claveClasificacionEscalon;
     private Double valorRangoInferior;
     private Double valorRangoSuperior;


    // Constructors

    /** default constructor */
    public EscalonDTO() {
    }
   
    // Property accessors
    @Id 
    @Column(name="IDTOESCALON", nullable=false, precision=22, scale=0)
    public double getIdToEscalon() {
        return this.idToEscalon;
    }
    
    public void setIdToEscalon(double idToEscalon) {
        this.idToEscalon = idToEscalon;
    }
    
    @Column(name="CLAVECLASIFICACIONESCALON", nullable=false, precision=4, scale=0)

    public Short getClaveClasificacionEscalon() {
        return this.claveClasificacionEscalon;
    }
    
    public void setClaveClasificacionEscalon(Short claveClasificacionEscalon) {
        this.claveClasificacionEscalon = claveClasificacionEscalon;
    }
    
    @Column(name="VALORRANGOINFERIOR", nullable=false, precision=16)
    public Double getValorRangoInferior() {
        return this.valorRangoInferior;
    }
    
    public void setValorRangoInferior(Double valorRangoInferior) {
        this.valorRangoInferior = valorRangoInferior;
    }
    
    @Column(name="VALORRANGOSUPERIOR", nullable=false, precision=16)
    public Double getValorRangoSuperior() {
        return this.valorRangoSuperior;
    }
    
    public void setValorRangoSuperior(Double valorRangoSuperior) {
        this.valorRangoSuperior = valorRangoSuperior;
    }

	@Override
	public String getDescription() {
		return ""+this.valorRangoInferior+" - "+this.valorRangoSuperior;
	}

	@Override
	public Object getId() {
		return this.idToEscalon;
	}

	@Override
	public boolean equals(Object object) {
		boolean result = false;
		if (object instanceof EscalonDTO) {
			EscalonDTO temp = (EscalonDTO) object;
			if (temp.getIdToEscalon() == this.getIdToEscalon()) {
				result = true;
			}
		}
		return result;
	}
}