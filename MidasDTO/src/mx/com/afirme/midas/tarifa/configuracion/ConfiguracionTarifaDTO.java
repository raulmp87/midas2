package mx.com.afirme.midas.tarifa.configuracion;

// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * ConfiguracionTarifaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "ConfiguracionTarifaDTO")
@Table(name = "TCCONFIGURACIONTARIFA", schema = "MIDAS")
public class ConfiguracionTarifaDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ConfiguracionTarifaId id;
	private short claveTipoControl;
	private short claveTipoValidacion;
	private short idGrupo;
	private String claseRemota;
	private String etiqueta;


	// Constructors

	/** default constructor */
	public ConfiguracionTarifaDTO() {
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idConcepto", column = @Column(name = "IDCONCEPTO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idDato", column = @Column(name = "IDDATO", nullable = false, precision = 22, scale = 0)) })
	public ConfiguracionTarifaId getId() {
		return this.id;
	}

	public void setId(ConfiguracionTarifaId id) {
		this.id = id;
	}

	@Column(name = "CLAVETIPOCONTROL", nullable = false, precision = 22, scale = 0)
	public short getClaveTipoControl() {
		return claveTipoControl;
	}

	public void setClaveTipoControl(short claveTipoControl) {
		this.claveTipoControl = claveTipoControl;
	}

	@Column(name = "DESCRIPCIONCLASEREMOTA")
	public String getClaseRemota() {
		return this.claseRemota;
	}

	public void setClaseRemota(String claseRemota) {
		this.claseRemota = claseRemota;
	}

	@Column(name = "DESCRIPCIONETIQUETA")
	public String getEtiqueta() {
		return this.etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	@Column(name = "CLAVETIPOVALIDACION", nullable = false, precision = 22, scale = 0)
	public short getClaveTipoValidacion() {
		return claveTipoValidacion;
	}

	public void setClaveTipoValidacion(short claveTipoValidacion) {
		this.claveTipoValidacion = claveTipoValidacion;
	}

	@Column(name = "IDGRUPO", nullable = false, precision = 22, scale = 0)
	public short getIdGrupo() {
		return idGrupo;
	}

	public void setIdGrupo(short idGrupo) {
		this.idGrupo = idGrupo;
	}
	
}