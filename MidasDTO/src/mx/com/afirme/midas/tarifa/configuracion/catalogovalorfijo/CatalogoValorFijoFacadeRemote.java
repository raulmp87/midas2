package mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBaseAuto;

/**
 * Remote interface for catalogoValorFijoFacade.
 * @author MyEclipse Persistence Tools
 */


public interface CatalogoValorFijoFacadeRemote extends MidasInterfaceBaseAuto<CatalogoValorFijoDTO> {
		/**
	 Perform an initial save of a previously unsaved catalogoValorFijo entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity catalogoValorFijo entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CatalogoValorFijoDTO entity);
    /**
	 Delete a persistent catalogoValorFijo entity.
	  @param entity catalogoValorFijo entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CatalogoValorFijoDTO entity);
   /**
	 Persist a previously saved catalogoValorFijo entity and return it or a copy of it to the sender. 
	 A copy of the catalogoValorFijo entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity catalogoValorFijo entity to update
	 @return catalogoValorFijo the persisted catalogoValorFijo entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CatalogoValorFijoDTO update(CatalogoValorFijoDTO entity);
	 /**
	 * Find all catalogoValorFijo entities with a specific property value.  
	 
	  @param propertyName the name of the catalogoValorFijo property to query
	  @param value the property value to match
	  	  @return List<catalogoValorFijo> found by query
	 */
	public List<CatalogoValorFijoDTO> findByProperty(String propertyName, Object value
		);
	public List<CatalogoValorFijoDTO> findByDescripcion(Object descripcion
		);
	/**
	 * Find all catalogoValorFijo entities.
	  	  @return List<catalogoValorFijo> all catalogoValorFijo entities
	 */
	public List<CatalogoValorFijoDTO> findAll();
	
	public String getDescripcionCatalogoValorFijo(int idGrupoValores,int idDato);
	
	public String getDescripcionCatalogoValorFijo(int idGrupoValores,String idDato);
	
	public CatalogoValorFijoDTO findByIdGrupoValoresAndDescripcion(int idGrupoValores, String descripcion);
}