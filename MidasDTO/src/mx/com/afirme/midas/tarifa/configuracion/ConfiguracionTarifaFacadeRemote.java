package mx.com.afirme.midas.tarifa.configuracion;
// default package

import java.util.List;

import javax.ejb.Remote;


/**
 * Remote interface for ConfiguracionTarifaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ConfiguracionTarifaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ConfiguracionTarifaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ConfiguracionTarifaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ConfiguracionTarifaDTO entity);
    /**
	 Delete a persistent ConfiguracionTarifaDTO entity.
	  @param entity ConfiguracionTarifaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ConfiguracionTarifaDTO entity);
   /**
	 Persist a previously saved ConfiguracionTarifaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ConfiguracionTarifaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ConfiguracionTarifaDTO entity to update
	 @return ConfiguracionTarifaDTO the persisted ConfiguracionTarifaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ConfiguracionTarifaDTO update(ConfiguracionTarifaDTO entity);
	public ConfiguracionTarifaDTO findById( ConfiguracionTarifaId id);
	 /**
	 * Find all ConfiguracionTarifaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ConfiguracionTarifaDTO property to query
	  @param value the property value to match
	  	  @return List<ConfiguracionTarifaDTO> found by query
	 */
	public List<ConfiguracionTarifaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ConfiguracionTarifaDTO entities.
	  	  @return List<ConfiguracionTarifaDTO> all ConfiguracionTarifaDTO entities
	 */
	public List<ConfiguracionTarifaDTO> findAll(
		);
	
	/**
	 * Find all ConfiguracionTarifaDTO entities with depends of a filter. 
	 * @param entity. the name of the entity ProductDTO
	 * @return ProductoDTO found by query
	 */
	public List<ConfiguracionTarifaDTO> listarFiltrado(ConfiguracionTarifaDTO entity);
	
}