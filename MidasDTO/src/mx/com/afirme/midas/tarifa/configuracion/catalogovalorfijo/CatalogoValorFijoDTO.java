package mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo;
// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * catalogoValorFijo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCCATALOGOVALORFIJO",schema="MIDAS")
@Cache(
		type=CacheType.FULL,
		size=100000,
		alwaysRefresh=false,
		disableHits=false,
		refreshOnlyIfNewer=true
		)
public class CatalogoValorFijoDTO  extends CacheableDTO implements Entidad{
	private static final long serialVersionUID = 1L;
	private CatalogoValorFijoId id;
     private String descripcion;
     
     
     /**
     *Constantes de IDGRUPO. Sirve para poder saber el catalogo que se esta usando. 
     */
    public static final int IDGRUPO_TIPO_CARGA = 311;
    public static final int IDGRUPO_NEGOCIO_ESTRATEGAS = 339;
    public static final int IDGRUPO_CLAVE_TIPO_DSMGVDF = 341;
    public static final int IDGRUPO_CLAVE_TIPO_CALCULO = 345;
    public static final int IDGRUPO_NEGOCIO_SEGURO_OBLIGATORIO = 344;
    public static final int IDGRUPO_PRIMA_SEGURO_OBLIGATORIO = 355;
    public static final int IDGRUPO_FECHA_SEGURO_OBLIGATORIO = 356;
    public static final int IDGRUPO_LINEA_SERVICIO_PUBLICO = 357;
    public static final int IDGRUPO_AGENTE_ESTRATEGAS = 370;
    public static final int IDGRUPO_AGENTE_CAMBIO_LINEA = 371;
    public static final int IDGRUPO_AGENTE_AUTOFIN = 372;
    public static final int IDGRUPO_AGENTE_VENTA_DIRECTA = 373;
    public static final int IDGRUPO_CVE_TIPO_ENDOSO_MIGRA = 358;
    public static final int IDGRUPO_TIPO_VALIDACION_NUM_SERIE = 504;
    public static final int IDGRUPO_POSIBLES_DEDUCIBLES = 38;
    public static final int IDGRUPO_DIAS_SALARIO = 338;
    public static final int GRUPO_CLAVE_ESTATUS_COTIZACION = 330;
    public static final int IDGRUPO_CVES_TIPOS_ENDOSO = 331;
    
    // Constructors

    /** default constructor */
    public CatalogoValorFijoDTO() {
    }

    
    /** full constructor */
    public CatalogoValorFijoDTO(CatalogoValorFijoId id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

   
    // Property accessors
    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idGrupoValores", column=@Column(name="IDGRUPOVALORES", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idDato", column=@Column(name="IDDATO", nullable=false, precision=22, scale=0) ) } )

    public CatalogoValorFijoId getId() {
        return this.id;
    }
    
    public void setId(CatalogoValorFijoId id) {
        this.id = id;
    }
    
    @Column(name="DESCRIPCIONDATO", nullable=false, length=200)

    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
	public boolean equals(Object o) {
		boolean result = false;
		if (o instanceof CatalogoValorFijoDTO) {
			CatalogoValorFijoDTO temp = (CatalogoValorFijoDTO) o;
			if (temp.getId().getIdDato() == this.getId().getIdDato()&& temp.getId().getIdGrupoValores() == this.getId().getIdGrupoValores()) {
				result = true;
			}
		}
		return result;
	}

	public int hashCode() {
		int hash = 1;
	    hash = hash * 31 + this.getId().hashCode();
	    return hash;
	}


	@Override
	public String getDescription() {
		return this.descripcion;
	}


	@SuppressWarnings("unchecked")
	@Override
	public CatalogoValorFijoId getKey() {
		return this.id;
	}


	@Override
	public String getValue() {
		return this.descripcion;
	}


	@SuppressWarnings("unchecked")
	@Override
	public CatalogoValorFijoId getBusinessKey() {
		return this.id;
	}
}