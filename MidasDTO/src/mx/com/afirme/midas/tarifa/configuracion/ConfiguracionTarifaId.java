package mx.com.afirme.midas.tarifa.configuracion;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ConfiguracionTarifaDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class ConfiguracionTarifaId implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private BigDecimal idToRiesgo;
	private BigDecimal idConcepto;
	private short idDato;

	// Constructors

	/** default constructor */
	public ConfiguracionTarifaId() {
	}

	// Property accessors

	@Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToRiesgo() {
		return this.idToRiesgo;
	}

	public void setIdToRiesgo(BigDecimal idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	@Column(name = "IDCONCEPTO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdConcepto() {
		return this.idConcepto;
	}

	public void setIdConcepto(BigDecimal idConcepto) {
		this.idConcepto = idConcepto;
	}

	@Column(name = "IDDATO", nullable = false, precision = 22, scale = 0)
	public short getIdDato() {
		return idDato;
	}

	public void setIdDato(short idDato) {
		this.idDato = idDato;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ConfiguracionTarifaId))
			return false;
		ConfiguracionTarifaId castOther = (ConfiguracionTarifaId) other;

		return ((this.getIdToRiesgo() == castOther.getIdToRiesgo()) || (this
				.getIdToRiesgo() != null
				&& castOther.getIdToRiesgo() != null && this.getIdToRiesgo()
				.equals(castOther.getIdToRiesgo())))
				&& ((this.getIdConcepto() == castOther.getIdConcepto()) || (this
						.getIdConcepto() != null
						&& castOther.getIdConcepto() != null && this
						.getIdConcepto().equals(castOther.getIdConcepto())))
				&& ((this.getIdDato() == castOther.getIdDato()) || (this
						.getIdDato() != 0
						&& castOther.getIdDato() != 0 && this.getIdDato()==castOther.getIdDato()));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToRiesgo() == null ? 0 : this.getIdToRiesgo()
						.hashCode());
		result = 37
				* result
				+ (getIdConcepto() == null ? 0 : this.getIdConcepto()
						.hashCode());
		result = 37 * result
				+ (getIdDato() == 0 ? 0 : this.getIdDato());
		return result;
	}

}