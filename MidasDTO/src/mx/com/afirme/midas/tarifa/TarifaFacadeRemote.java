package mx.com.afirme.midas.tarifa;

// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for TarifaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TarifaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved TarifaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TarifaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TarifaDTO entity);

	/**
	 * Delete a persistent TarifaDTO entity.
	 * 
	 * @param entity
	 *            TarifaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TarifaDTO entity);

	/**
	 * Persist a previously saved TarifaDTO entity and return it or a copy of it
	 * to the sender. A copy of the TarifaDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            TarifaDTO entity to update
	 * @return TarifaDTO the persisted TarifaDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TarifaDTO update(TarifaDTO entity);

	public TarifaDTO findById(TarifaId id);

	/**
	 * Find all TarifaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TarifaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TarifaDTO> found by query
	 */
	public List<TarifaDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all TarifaDTO entities.
	 * 
	 * @return List<TarifaDTO> all TarifaDTO entities
	 */
	public List<TarifaDTO> findAll();
	
	/**
	 * Find TarifaDTO entities with the received idToRiesgo and idConcepto and version. 
	 * @param idToRiesgo
	 * @param idConcepto
	 * @param version
	 * @return List<TarifaDTO> all TarifaDTO entities
	 */
	public List<TarifaDTO> findByIdToRiesgoIdConceptoVersion(BigDecimal idToRiesgo,String idConcepto, Long version);
}