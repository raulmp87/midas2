package mx.com.afirme.midas.tarifa;
// default package

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;

/**
 * TarifaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity (name="TarifaDTO")
@Table(name = "TOTARIFA", schema = "MIDAS")
public class TarifaDTO implements java.io.Serializable {

	// Fields

	private static final long serialVersionUID = 1L;	
	private TarifaId id;
	private RiesgoDTO riesgoDTO;
	private BigDecimal valor;

	
	public static final String IDTORIESGO = "idToRiesgo";
	public static final String VERSION = "version";
	public static final String IDBASE1 = "idBase1";
	public static final String IDBASE2 = "idBase2";
	public static final String IDBASE3= "idBase3";
	public static final String IDBASE4= "idBase4";
	public static final String VALOR= "valor";
	public static final String IDCONCEPTO= "idConcepto";
	
	public static final String ELEMENTOS_COMBO= "elementosCombo";
	
	// Constructors

	/** default constructor */
	public TarifaDTO() {
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idConcepto", column = @Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)),
			@AttributeOverride(name = "idBase1", column = @Column(name = "IDBASE1", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idBase2", column = @Column(name = "IDBASE2", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idBase3", column = @Column(name = "IDBASE3", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idBase4", column = @Column(name = "IDBASE4", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "version", column = @Column(name = "IDVERTARIFA", nullable = false, precision = 22, scale = 0))})
	public TarifaId getId() {
		return this.id;
	}

	public void setId(TarifaId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTORIESGO", nullable = false, insertable = false, updatable = false)
	public RiesgoDTO getRiesgoDTO() {
		return this.riesgoDTO;
	}

	public void setRiesgoDTO(RiesgoDTO riesgoDTO) {
		this.riesgoDTO = riesgoDTO;
	}

	@Column(name = "VALOR", nullable = false, precision = 16, scale = 4)
	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

}