package mx.com.afirme.midas.tarifa;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * TarifaDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class TarifaId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToRiesgo;
	private String idConcepto;
	private BigDecimal idBase1;
	private BigDecimal idBase2;
	private BigDecimal idBase3;
	private BigDecimal idBase4;
	private Long version;

	// Constructors

	/** default constructor */
	public TarifaId() {
	}

	// Property accessors

	@Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToRiesgo() {
		return this.idToRiesgo;
	}

	public void setIdToRiesgo(BigDecimal idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	@Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)
	public String getIdConcepto() {
		return this.idConcepto;
	}

	public void setIdConcepto(String idConcepto) {
		this.idConcepto = idConcepto;
	}

	@Column(name = "IDBASE1", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdBase1() {
		return this.idBase1;
	}

	public void setIdBase1(BigDecimal idBase1) {
		this.idBase1 = idBase1;
	}

	@Column(name = "IDBASE2", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdBase2() {
		return this.idBase2;
	}

	public void setIdBase2(BigDecimal idBase2) {
		this.idBase2 = idBase2;
	}

	@Column(name = "IDBASE3", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdBase3() {
		return this.idBase3;
	}

	public void setIdBase3(BigDecimal idBase3) {
		this.idBase3 = idBase3;
	}

	@Column(name = "IDBASE4", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdBase4() {
		return this.idBase4;
	}

	public void setIdBase4(BigDecimal idBase4) {
		this.idBase4 = idBase4;
	}

	@Column(name = "IDVERTARIFA", nullable = false, precision = 22, scale = 0)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idBase1 == null) ? 0 : idBase1.hashCode());
		result = prime * result + ((idBase2 == null) ? 0 : idBase2.hashCode());
		result = prime * result + ((idBase3 == null) ? 0 : idBase3.hashCode());
		result = prime * result + ((idBase4 == null) ? 0 : idBase4.hashCode());
		result = prime * result
				+ ((idConcepto == null) ? 0 : idConcepto.hashCode());
		result = prime * result
				+ ((idToRiesgo == null) ? 0 : idToRiesgo.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarifaId other = (TarifaId) obj;
		if (idBase1 == null) {
			if (other.idBase1 != null)
				return false;
		} else if (!idBase1.equals(other.idBase1))
			return false;
		if (idBase2 == null) {
			if (other.idBase2 != null)
				return false;
		} else if (!idBase2.equals(other.idBase2))
			return false;
		if (idBase3 == null) {
			if (other.idBase3 != null)
				return false;
		} else if (!idBase3.equals(other.idBase3))
			return false;
		if (idBase4 == null) {
			if (other.idBase4 != null)
				return false;
		} else if (!idBase4.equals(other.idBase4))
			return false;
		if (idConcepto == null) {
			if (other.idConcepto != null)
				return false;
		} else if (!idConcepto.equals(other.idConcepto))
			return false;
		if (idToRiesgo == null) {
			if (other.idToRiesgo != null)
				return false;
		} else if (!idToRiesgo.equals(other.idToRiesgo))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	

}