package mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ParticipacionCorredorFacultativoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ParticipacionCorredorFacultativoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * ParticipacionCorredorFacultativoDTO entity. All subsequent persist
	 * actions of this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ParticipacionCorredorFacultativoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ParticipacionCorredorFacultativoDTO entity);

	/**
	 * Delete a persistent ParticipacionCorredorFacultativoDTO entity.
	 * 
	 * @param entity
	 *            ParticipacionCorredorFacultativoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ParticipacionCorredorFacultativoDTO entity);

	/**
	 * Persist a previously saved ParticipacionCorredorFacultativoDTO entity and
	 * return it or a copy of it to the sender. A copy of the
	 * ParticipacionCorredorFacultativoDTO entity parameter is returned when the
	 * JPA persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            ParticipacionCorredorFacultativoDTO entity to update
	 * @return ParticipacionCorredorFacultativoDTO the persisted
	 *         ParticipacionCorredorFacultativoDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ParticipacionCorredorFacultativoDTO update(
			ParticipacionCorredorFacultativoDTO entity);

	public ParticipacionCorredorFacultativoDTO findById(BigDecimal id);

	/**
	 * Find all ParticipacionCorredorFacultativoDTO entities with a specific
	 * property value.
	 * 
	 * @param propertyName
	 *            the name of the ParticipacionCorredorFacultativoDTO property
	 *            to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ParticipacionCorredorFacultativoDTO> found by query
	 */
	public List<ParticipacionCorredorFacultativoDTO> findByProperty(
			String propertyName, Object value, int... rowStartIdxAndCount);

	public List<ParticipacionCorredorFacultativoDTO> findByPorcentajeParticipacion(
			Object porcentajeParticipacion, int... rowStartIdxAndCount);

	/**
	 * Find all ParticipacionCorredorFacultativoDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ParticipacionCorredorFacultativoDTO> all
	 *         ParticipacionCorredorFacultativoDTO entities
	 */
	public List<ParticipacionCorredorFacultativoDTO> findAll(
			int... rowStartIdxAndCount);
}