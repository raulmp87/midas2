package mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;

/**
 * ParticipacionCorredorFacultativoDTO entity. @author MyEclipse Persistence
 * Tools
 */
@Entity
@Table(name = "TDPARTICIPACIONCORREDORFAC", schema = "MIDAS")
public class ParticipacionCorredorFacultativoDTO implements
		java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4218918362842705328L;
	// Fields

	private BigDecimal idTdParticapacionCorredorFac;
	private ParticipacionFacultativoDTO participacionFacultativoDTO;
	private ReaseguradorCorredorDTO reaseguradorCorredorDTO;
	private BigDecimal porcentajeParticipacion;

	// Constructors

	/** default constructor */
	public ParticipacionCorredorFacultativoDTO() {
	}

	/** full constructor */
	public ParticipacionCorredorFacultativoDTO(
			BigDecimal idTdParticapacionCorredorFac,
			ParticipacionFacultativoDTO participacionFacultativoDTO,
			ReaseguradorCorredorDTO reaseguradorCorredorDTO,
			BigDecimal porcentajeParticipacion) {
		this.idTdParticapacionCorredorFac = idTdParticapacionCorredorFac;
		this.participacionFacultativoDTO = participacionFacultativoDTO;
		this.reaseguradorCorredorDTO = reaseguradorCorredorDTO;
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTDPARTICIPACIONCORREDORF_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTDPARTICIPACIONCORREDORF_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTDPARTICIPACIONCORREDORF_SEQ_GENERADOR")
	@Column(name = "IDTDPARTICIPACIONCORREDORFAC", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTdParticapacionCorredorFac() {
		return this.idTdParticapacionCorredorFac;
	}

	public void setIdTdParticapacionCorredorFac(
			BigDecimal idTdParticapacionCorredorFac) {
		this.idTdParticapacionCorredorFac = idTdParticapacionCorredorFac;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTDPARTICIPACIONFACULTATIVO", nullable = false)
	public ParticipacionFacultativoDTO getParticipacionFacultativoDTO() {
		return this.participacionFacultativoDTO;
	}

	public void setParticipacionFacultativoDTO(
			ParticipacionFacultativoDTO participacionFacultativoDTO) {
		this.participacionFacultativoDTO = participacionFacultativoDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCREASEGURADORCORREDOR", nullable = false)
	public ReaseguradorCorredorDTO getReaseguradorCorredorDTO() {
		return this.reaseguradorCorredorDTO;
	}

	public void setReaseguradorCorredorDTO(
			ReaseguradorCorredorDTO reaseguradorCorredorDTO) {
		this.reaseguradorCorredorDTO = reaseguradorCorredorDTO;
	}

	@Column(name = "PORCENTAJEPARTICIPACION", nullable = false, precision = 5, scale=10)
	public BigDecimal getPorcentajeParticipacion() {
		return this.porcentajeParticipacion;
	}

	public void setPorcentajeParticipacion(BigDecimal porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

}