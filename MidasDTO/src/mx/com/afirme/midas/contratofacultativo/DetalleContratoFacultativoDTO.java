package mx.com.afirme.midas.contratofacultativo;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;

/**
 * DetalleContratoFacultativoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TDCONTRATOFACULTATIVO", schema = "MIDAS")
public class DetalleContratoFacultativoDTO implements java.io.Serializable {

	public static int ESTATUS_INICIAL_COBERTURA = 0;
	public static int ESTATUS_COBERTURA_CANCELADA = 1;
	
	
	private static final long serialVersionUID = -6676405137031177704L;
	// Fields
	private BigDecimal contratoFacultativoDTO;
	private ContratoFacultativoDTO contratoFacultativoDTO_1;
	private BigDecimal idToCotizacion;
	private BigDecimal idTcSubramo;
	private BigDecimal idToCobertura;
	private BigDecimal numeroInciso;
	private BigDecimal numeroSubInciso;
	private BigDecimal idToSeccion;
	private BigDecimal tipoDistribucion;
	private BigDecimal porcentajeComision;
	private BigDecimal deducibleCobertura;
	private BigDecimal primaTotalCobertura;
	private BigDecimal primaFacultadaCobertura;
	private BigDecimal sumaAsegurada;
	private BigDecimal sumaFacultada;
	private Integer estatus;
	private BigDecimal montoPrimaNoDevengada;
	private BigDecimal montoPrimaAdicional;
	
	private List<ParticipacionFacultativoDTO> participacionFacultativoDTOs = 
		new ArrayList<ParticipacionFacultativoDTO>();

	// Constructors

	/** default constructor */
	public DetalleContratoFacultativoDTO() {
	}

	/** minimal constructor */
	public DetalleContratoFacultativoDTO(BigDecimal contratoFacultativoDTO,
			ContratoFacultativoDTO contratoFacultativoDTO_1,
			BigDecimal idToCotizacion, BigDecimal idTcSubramo,
			BigDecimal idToCobertura) {
		this.contratoFacultativoDTO = contratoFacultativoDTO;
		this.contratoFacultativoDTO_1 = contratoFacultativoDTO_1;
		this.idToCotizacion = idToCotizacion;
		this.idTcSubramo = idTcSubramo;
		this.idToCobertura = idToCobertura;
	}

	/** full constructor */
	public DetalleContratoFacultativoDTO(BigDecimal contratoFacultativoDTO,
			ContratoFacultativoDTO contratoFacultativoDTO_1,
			BigDecimal idToCotizacion, BigDecimal idTcSubramo,
			BigDecimal idToCobertura, BigDecimal numeroInciso,
			BigDecimal numeroSubInciso, BigDecimal idToSeccion,
			BigDecimal tipoDistribucion, BigDecimal porcentajeComision,
			BigDecimal deducibleCobertura, BigDecimal primaTotalCobertura,
			BigDecimal primaFacultadaCobertura,BigDecimal sumaAsegurada,BigDecimal sumaFacultada,
			List<ParticipacionFacultativoDTO> participacionFacultativoDTOs) {
		this.contratoFacultativoDTO = contratoFacultativoDTO;
		this.contratoFacultativoDTO_1 = contratoFacultativoDTO_1;
		this.idToCotizacion = idToCotizacion;
		this.idTcSubramo = idTcSubramo;
		this.idToCobertura = idToCobertura;
		this.numeroInciso = numeroInciso;
		this.numeroSubInciso = numeroSubInciso;
		this.idToSeccion = idToSeccion;
		this.tipoDistribucion = tipoDistribucion;
		this.porcentajeComision = porcentajeComision;
		this.deducibleCobertura = deducibleCobertura;
		this.primaTotalCobertura = primaTotalCobertura;
		this.primaFacultadaCobertura = primaFacultadaCobertura;
		this.participacionFacultativoDTOs = participacionFacultativoDTOs;
		this.sumaAsegurada = sumaAsegurada;
		this.sumaFacultada = sumaFacultada;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTDCONTRATOFACULTATIVO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTDCONTRATOFACULTATIVO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTDCONTRATOFACULTATIVO_SEQ_GENERADOR")
 	@Column(name = "IDTDCONTRATOFACULTATIVO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getContratoFacultativoDTO() {
		return this.contratoFacultativoDTO;
	}

	public void setContratoFacultativoDTO(BigDecimal contratoFacultativoDTO) {
		this.contratoFacultativoDTO = contratoFacultativoDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTMCONTRATOFACULTATIVO", nullable = false, referencedColumnName="IDTMCONTRATOFACULTATIVO")
	public ContratoFacultativoDTO getContratoFacultativoDTO_1() {
		return this.contratoFacultativoDTO_1;
	}

	public void setContratoFacultativoDTO_1(
			ContratoFacultativoDTO contratoFacultativoDTO_1) {
		this.contratoFacultativoDTO_1 = contratoFacultativoDTO_1;
	}

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubramo() {
		return this.idTcSubramo;
	}

	public void setIdTcSubramo(BigDecimal idTcSubramo) {
		this.idTcSubramo = idTcSubramo;
	}

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	@Column(name = "NUMEROINCISO", precision = 22, scale = 0)
	public BigDecimal getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "NUMEROSUBINCISO", precision = 22, scale = 0)
	public BigDecimal getNumeroSubInciso() {
		return this.numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	@Column(name = "IDTOSECCION", precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "TIPODISTRIBUCION", precision = 22, scale = 0)
	public BigDecimal getTipoDistribucion() {
		return this.tipoDistribucion;
	}

	public void setTipoDistribucion(BigDecimal tipoDistribucion) {
		this.tipoDistribucion = tipoDistribucion;
	}

	@Column(name = "PORCENTAJECOMISION", precision = 5)
	public BigDecimal getPorcentajeComision() {
		return this.porcentajeComision;
	}

	public void setPorcentajeComision(BigDecimal porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}

	@Column(name = "DEDUCIBLECOBERTURA", precision = 16, scale = 4)
	public BigDecimal getDeducibleCobertura() {
		return this.deducibleCobertura;
	}

	public void setDeducibleCobertura(BigDecimal deducibleCobertura) {
		this.deducibleCobertura = deducibleCobertura;
	}

	@Column(name = "PRIMATOTALCOBERTURA", precision = 18, scale = 4)
	public BigDecimal getPrimaTotalCobertura() {
		return this.primaTotalCobertura;
	}

	public void setPrimaTotalCobertura(BigDecimal primaTotalCobertura) {
		this.primaTotalCobertura = primaTotalCobertura;
	}

	@Column(name = "PRIMAFACULTADACOBERTURA", precision = 18, scale = 4)
	public BigDecimal getPrimaFacultadaCobertura() {
		return this.primaFacultadaCobertura;
	}

	public void setPrimaFacultadaCobertura(BigDecimal primaFacultadaCobertura) {
		this.primaFacultadaCobertura = primaFacultadaCobertura;
	}
	
	@Column(name = "SUMAASEGURADA", precision = 18, scale = 4)
	public BigDecimal getSumaAsegurada() {
		return this.sumaAsegurada;
	}

	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	
	@Column(name = "SUMAFACULTADA", precision = 18, scale = 4)
	public BigDecimal getSumaFacultada() {
		return this.sumaFacultada;
	}
 	public void setSumaFacultada(BigDecimal sumaFacultada) {
		this.sumaFacultada = sumaFacultada;
	}
 	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "detalleContratoFacultativoDTO")
	public List<ParticipacionFacultativoDTO> getParticipacionFacultativoDTOs() {
		return this.participacionFacultativoDTOs;
	}

	public void setParticipacionFacultativoDTOs(
			List<ParticipacionFacultativoDTO> participacionFacultativoDTOs) {
		this.participacionFacultativoDTOs = participacionFacultativoDTOs;
	}

//	@Column(name = "ESTATUS", nullable = true, precision = 2, scale = 0)
	@Transient
	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	@Column(name = "MONTOPRIMANODEVENGADA", nullable = true, precision = 24, scale = 10)
	public BigDecimal getMontoPrimaNoDevengada() {
		return montoPrimaNoDevengada;
	}

	public void setMontoPrimaNoDevengada(BigDecimal montoPrimaNoDevengada) {
		this.montoPrimaNoDevengada = montoPrimaNoDevengada;
	}

	@Column(name = "MONTOPRIMAADICIONAL", length = 500)
	public BigDecimal getMontoPrimaAdicional() {
		return montoPrimaAdicional;
	}

	public void setMontoPrimaAdicional(BigDecimal montoPrimaAdicional) {
		this.montoPrimaAdicional = montoPrimaAdicional;
	}
}