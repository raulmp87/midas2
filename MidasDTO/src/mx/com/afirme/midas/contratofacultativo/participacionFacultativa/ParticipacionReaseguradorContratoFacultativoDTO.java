package mx.com.afirme.midas.contratofacultativo.participacionFacultativa;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;

@SqlResultSetMapping(name = "participacionMap", entities = { @EntityResult(entityClass = ParticipacionReaseguradorContratoFacultativoDTO.class, fields = {
		@FieldResult(name = "numeroinciso", column = "NUMEROINCISO"),
		@FieldResult(name = "numeroSubInciso", column = "NUMEROSUBINCISO"),
		@FieldResult(name = "descripcionSeccion", column = "DESCRIPCIONSECCION"),
		@FieldResult(name = "descripcionCobertura", column = "DESCRIPCIONCOBERTURA"),
		@FieldResult(name = "montoPagoPrimaNeta", column = "MONTOPAGOPRIMANETA"),
		@FieldResult(name = "primaComision", column = "PRIMACOMISION"),
		@FieldResult(name = "porcentajeComision", column = "PORCENTAJECOMISION"),
		@FieldResult(name = "porcentajeRetencionImpuestos", column = "PORCENTAJERETENCIONIMPUESTOS"),
		@FieldResult(name = "primaRetenidaImpuestos", column = "PRIMARETENIDAIMPUESTOS"),
		@FieldResult(name = "numeroExhibicion", column = "NUMEROEXHIBICION"),
		@FieldResult(name = "fechaPago", column = "FECHAPAGO"),
		@FieldResult(name = "primaNetaReasegurador", column = "PRIMANETAREASEGURADOR"),
		@FieldResult(name = "porcentajeParticipacionFacultativo", column = "PORCENTAJEPARTICIPACION"),
		@FieldResult(name = "numeroEndoso", column = "NUMEROENDOSO"),
		@FieldResult(name = "numeroPoliza", column = "NUMEROPOLIZA"),
		@FieldResult(name = "nombreReasegurador", column = "NOMBREREASEGURADOR"),
		@FieldResult(name = "nombreCorredor", column = "NOMBRECORREDOR"),
		@FieldResult(name = "estatus", column = "ESTATUS") }) })
@Entity
public class ParticipacionReaseguradorContratoFacultativoDTO implements java.io.Serializable {

	private static final long serialVersionUID = 2407933556122006101L;
	@Id
	private String id;
	private String idPagoCoberturaReasegurador;
	
	private String numeroPoliza;
	private Integer numeroInciso;
	private Integer numeroSubInciso;
	private String descripcionSeccion;
	private String descripcionCobertura;
	private String nombreReasegurador;
	private String nombreCorredor;
	private Short numeroExhibicion;
	@Temporal(TemporalType.DATE)
	private Date fechaPago;
	private String estatus;
	
	private BigDecimal sumaAseguradaTotal;
	private BigDecimal porcentajeFacultativo;
	private BigDecimal sumaAseguradaFacultada;
	private SubRamoDTO subRamoDTO;
	private Integer numeroEndoso;
	private BigDecimal primaTotalPoliza;
	private BigDecimal primaFacultada;
	private BigDecimal porcentajeParticipacionFacultativo;
	private BigDecimal primaParticipacionFacultativo;
	private BigDecimal porcentajeRetencionImpuestos;
	private BigDecimal primaRetenidaImpuestos;
	private BigDecimal porcentajeComision;
	private BigDecimal primaComision;
	private BigDecimal primaNetaReasegurador;
	private BigDecimal montoPagoPrimaNeta;
	
	private Byte estatusAutorizacionExhibicionVencida;
    private Byte estatusAutorizacionFaltaPagoAsegurado;
    
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the montoPagoPrimaNeta
	 */
	public BigDecimal getMontoPagoPrimaNeta() {
		return montoPagoPrimaNeta;
	}
	/**
	 * @param montoPagoPrimaNeta the montoPagoPrimaNeta to set
	 */
	public void setMontoPagoPrimaNeta(BigDecimal montoPagoPrimaNeta) {
		this.montoPagoPrimaNeta = montoPagoPrimaNeta;
	}
	public BigDecimal getSumaAseguradaTotal() {
		return sumaAseguradaTotal;
	}
	public void setSumaAseguradaTotal(BigDecimal sumaAseguradaTotal) {
		this.sumaAseguradaTotal = sumaAseguradaTotal;
	}
	public BigDecimal getPorcentajeFacultativo() {
		return porcentajeFacultativo;
	}
	public void setPorcentajeFacultativo(BigDecimal porcentajeFacultativo) {
		this.porcentajeFacultativo = porcentajeFacultativo;
	}
	public BigDecimal getSumaAseguradaFacultada() {
		return sumaAseguradaFacultada;
	}
	public void setSumaAseguradaFacultada(BigDecimal sumaAseguradaFacultada) {
		this.sumaAseguradaFacultada = sumaAseguradaFacultada;
	}
	public SubRamoDTO getSubRamoDTO() {
		return subRamoDTO;
	}
	public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
		this.subRamoDTO = subRamoDTO;
	}
	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public BigDecimal getPrimaTotalPoliza() {
		return primaTotalPoliza;
	}
	public void setPrimaTotalPoliza(BigDecimal primaTotalPoliza) {
		this.primaTotalPoliza = primaTotalPoliza;
	}
	public BigDecimal getPrimaFacultada() {
		return primaFacultada;
	}
	public void setPrimaFacultada(BigDecimal primaFacultada) {
		this.primaFacultada = primaFacultada;
	}
	public BigDecimal getPorcentajeParticipacionFacultativo() {
		return porcentajeParticipacionFacultativo;
	}
	public void setPorcentajeParticipacionFacultativo(BigDecimal porcentajeParticipacionFacultativo) {
		this.porcentajeParticipacionFacultativo = porcentajeParticipacionFacultativo;
	}
	public BigDecimal getPrimaParticipacionFacultativo() {
		return primaParticipacionFacultativo;
	}
	public void setPrimaParticipacionFacultativo(BigDecimal primaParticipacionFacultativo) {
		this.primaParticipacionFacultativo = primaParticipacionFacultativo;
	}
	public BigDecimal getPorcentajeRetencionImpuestos() {
		return porcentajeRetencionImpuestos;
	}
	public void setPorcentajeRetencionImpuestos(BigDecimal porcentajeRetencionImpuestos) {
		this.porcentajeRetencionImpuestos = porcentajeRetencionImpuestos;
	}
	public BigDecimal getPrimaRetenidaImpuestos() {
		return primaRetenidaImpuestos;
	}
	public void setPrimaRetenidaImpuestos(BigDecimal primaRetenidaImpuestos) {
		this.primaRetenidaImpuestos = primaRetenidaImpuestos;
	}
	public BigDecimal getPorcentajeComision() {
		return porcentajeComision;
	}
	public void setPorcentajeComision(BigDecimal porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}
	public BigDecimal getPrimaComision() {
		return primaComision;
	}
	public void setPrimaComision(BigDecimal primaComision) {
		this.primaComision = primaComision;
	}
	public BigDecimal getPrimaNetaReasegurador() {
		return primaNetaReasegurador;
	}
	public void setPrimaNetaReasegurador(BigDecimal primaNetaReasegurador) {
		this.primaNetaReasegurador = primaNetaReasegurador;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public Integer getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(Integer numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}
	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}
	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}
	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}
	public String getNombreReasegurador() {
		return nombreReasegurador;
	}
	public void setNombreReasegurador(String nombreReasegurador) {
		this.nombreReasegurador = nombreReasegurador;
	}
	public String getNombreCorredor() {
		return nombreCorredor;
	}
	public void setNombreCorredor(String nombreCorredor) {
		this.nombreCorredor = nombreCorredor;
	}
	public Short getNumeroExhibicion() {
		return numeroExhibicion;
	}
	public void setNumeroExhibicion(Short numeroExhibicion) {
		this.numeroExhibicion = numeroExhibicion;
	}
	public Date getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getIdPagoCoberturaReasegurador() {
		return idPagoCoberturaReasegurador;
	}
	public void setIdPagoCoberturaReasegurador(String idPagoCoberturaReasegurador) {
		this.idPagoCoberturaReasegurador = idPagoCoberturaReasegurador;
	}
	public Byte getEstatusAutorizacionExhibicionVencida() {
		return estatusAutorizacionExhibicionVencida;
	}
	public void setEstatusAutorizacionExhibicionVencida(Byte estatusAutorizacionExhibicionVencida) {
		this.estatusAutorizacionExhibicionVencida = estatusAutorizacionExhibicionVencida;
	}
	public Byte getEstatusAutorizacionFaltaPagoAsegurado() {
		return estatusAutorizacionFaltaPagoAsegurado;
	}
	public void setEstatusAutorizacionFaltaPagoAsegurado(Byte estatusAutorizacionFaltaPagoAsegurado) {
		this.estatusAutorizacionFaltaPagoAsegurado = estatusAutorizacionFaltaPagoAsegurado;
	}
}