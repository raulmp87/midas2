package mx.com.afirme.midas.contratofacultativo.participacionFacultativa;

// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;


/**
 * Remote interface for ParticipacionFacultativoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ParticipacionFacultativoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * ParticipacionFacultativoDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ParticipacionFacultativoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ParticipacionFacultativoDTO entity);

	/**
	 * Delete a persistent ParticipacionFacultativoDTO entity.
	 * 
	 * @param entity
	 *            ParticipacionFacultativoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ParticipacionFacultativoDTO entity);

	/**
	 * Persist a previously saved ParticipacionFacultativoDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * ParticipacionFacultativoDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            ParticipacionFacultativoDTO entity to update
	 * @return ParticipacionFacultativoDTO the persisted
	 *         ParticipacionFacultativoDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ParticipacionFacultativoDTO update(ParticipacionFacultativoDTO entity);

	public ParticipacionFacultativoDTO findById(BigDecimal id);

	
	
	public List<ParticipacionFacultativoDTO> listarFiltrado(ParticipacionFacultativoDTO participacionFacultativoDTO);
	
	/**
	 * Find all ParticipacionFacultativoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the ParticipacionFacultativoDTO property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ParticipacionFacultativoDTO> found by query
	 */
	public List<ParticipacionFacultativoDTO> findByProperty(
			String propertyName, Object value, int... rowStartIdxAndCount);

	public List<ParticipacionFacultativoDTO> findByPorcentajeRetencion(
			Object porcentajeRetencion, int... rowStartIdxAndCount);

	public List<ParticipacionFacultativoDTO> findByPrirmaretencion(
			Object prirmaretencion, int... rowStartIdxAndCount);

	public List<ParticipacionFacultativoDTO> findByPorcentajeParticipacion(
			Object porcentajeParticipacion, int... rowStartIdxAndCount);

	public List<ParticipacionFacultativoDTO> findByComision(Object comision,
			int... rowStartIdxAndCount);

	/**
	 * Find all ParticipacionFacultativoDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ParticipacionFacultativoDTO> all ParticipacionFacultativoDTO
	 *         entities
	 */
	public List<ParticipacionFacultativoDTO> findAll(int... rowStartIdxAndCount);
}