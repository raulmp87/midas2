package mx.com.afirme.midas.contratofacultativo.participacionFacultativa;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
 
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.contacto.ContactoDTO;
import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo.ParticipacionCorredorFacultativoDTO;;
/**
 * ParticipacionFacultativoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TDPARTICIPACIONFACULTATIVO", schema = "MIDAS")
public class ParticipacionFacultativoDTO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5095391488761791193L;
 	private BigDecimal participacionFacultativoDTO;
	private ReaseguradorCorredorDTO reaseguradorCorredorDTO;
	private CuentaBancoDTO cuentaBancoDTOByIdtccuentabancodolares;
	private ContactoDTO contactoDTO;
    private DetalleContratoFacultativoDTO detalleContratoFacultativoDTO;
	private CuentaBancoDTO cuentaBancoDTOByIdtccuentabancopesos;
	private Double porcentajeRetencion;
	private Double prirmaretencion;
	private BigDecimal porcentajeParticipacion;
	private BigDecimal comision;
	private String tipo;
	private List<ParticipacionCorredorFacultativoDTO> participacionCorredorFacultativoDTOs = new ArrayList<ParticipacionCorredorFacultativoDTO>(0);

	// Constructors

	/** default constructor */
	public ParticipacionFacultativoDTO() {
	}
	
	public ParticipacionFacultativoDTO(
			BigDecimal participacionFacultativoDTO,
			ReaseguradorCorredorDTO reaseguradorCorredorDTO,
			CuentaBancoDTO cuentaBancoDTOByIdtccuentabancodolares,
			ContactoDTO contactoDTO,
			DetalleContratoFacultativoDTO detalleContratoFacultativoDTO,
			CuentaBancoDTO cuentaBancoDTOByIdtccuentabancopesos,
			Double porcentajeRetencion,
			Double prirmaretencion,
			BigDecimal porcentajeParticipacion,
			BigDecimal comision,String tipo,
			List<ParticipacionCorredorFacultativoDTO> participacionCorredorFacultativoDTOs) {
		this.participacionFacultativoDTO = participacionFacultativoDTO;
		this.reaseguradorCorredorDTO = reaseguradorCorredorDTO;
		this.cuentaBancoDTOByIdtccuentabancodolares = cuentaBancoDTOByIdtccuentabancodolares;
		this.contactoDTO = contactoDTO;
		this.detalleContratoFacultativoDTO = detalleContratoFacultativoDTO;
		this.cuentaBancoDTOByIdtccuentabancopesos = cuentaBancoDTOByIdtccuentabancopesos;
		this.porcentajeRetencion = porcentajeRetencion;
		this.prirmaretencion = prirmaretencion;
		this.porcentajeParticipacion = porcentajeParticipacion;
		this.comision = comision;
		this.tipo = tipo;
		this.participacionCorredorFacultativoDTOs = participacionCorredorFacultativoDTOs;
	}
	


	/** minimal constructor */

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTDPARTICIPACIONFAC_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTDPARTICIPACIONFAC_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTDPARTICIPACIONFAC_SEQ_GENERADOR")
 	@Column(name = "IDTDPARTICIPACIONFACULTATIVO",unique=true,  nullable = false, precision = 22, scale = 0)
	public BigDecimal getParticipacionFacultativoDTO() {
		return this.participacionFacultativoDTO;
	}

	public void setParticipacionFacultativoDTO(
			BigDecimal participacionFacultativoDTO) {
		this.participacionFacultativoDTO = participacionFacultativoDTO;
	}

	@ManyToOne(cascade = CascadeType.REFRESH,fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTDREASEGURADORCORREDOR", nullable = false)
	public ReaseguradorCorredorDTO getReaseguradorCorredorDTO() {
		return this.reaseguradorCorredorDTO;
	}

	public void setReaseguradorCorredorDTO(
			ReaseguradorCorredorDTO reaseguradorCorredorDTO) {
		this.reaseguradorCorredorDTO = reaseguradorCorredorDTO;
	}

	@ManyToOne(cascade = CascadeType.REFRESH,fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCCUENTABANCODOLARES", nullable = false)
	public CuentaBancoDTO getCuentaBancoDTOByIdtccuentabancodolares() {
		return this.cuentaBancoDTOByIdtccuentabancodolares;
	}

	public void setCuentaBancoDTOByIdtccuentabancodolares(
			CuentaBancoDTO cuentaBancoDTOByIdtccuentabancodolares) {
		this.cuentaBancoDTOByIdtccuentabancodolares = cuentaBancoDTOByIdtccuentabancodolares;
	}

	@ManyToOne(cascade = CascadeType.REFRESH,fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCCONTACTO")
	public ContactoDTO getContactoDTO() {
		return this.contactoDTO;
	}

	public void setContactoDTO(ContactoDTO contactoDTO) {
		this.contactoDTO = contactoDTO;
	}

	@ManyToOne(cascade=CascadeType.REFRESH,fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTDCONTRATOFACULTATIVO", nullable = false)
	public DetalleContratoFacultativoDTO getDetalleContratoFacultativoDTO() {
		return this.detalleContratoFacultativoDTO;
	}

	public void setDetalleContratoFacultativoDTO(
			DetalleContratoFacultativoDTO detalleContratoFacultativoDTO) {
		this.detalleContratoFacultativoDTO = detalleContratoFacultativoDTO;
	}

	@ManyToOne(cascade = CascadeType.REFRESH,fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCCUENTABANCOPESOS", nullable = false)
	public CuentaBancoDTO getCuentaBancoDTOByIdtccuentabancopesos() {
		return this.cuentaBancoDTOByIdtccuentabancopesos;
	}

	public void setCuentaBancoDTOByIdtccuentabancopesos(
			CuentaBancoDTO cuentaBancoDTOByIdtccuentabancopesos) {
		this.cuentaBancoDTOByIdtccuentabancopesos = cuentaBancoDTOByIdtccuentabancopesos;
	}

	@Column(name = "PORCENTAJERETENCION", precision = 5)
	public Double getPorcentajeRetencion() {
		return this.porcentajeRetencion;
	}

	public void setPorcentajeRetencion(Double porcentajeRetencion) {
		this.porcentajeRetencion = porcentajeRetencion;
	}

	@Column(name = "PRIRMARETENCION", precision = 5)
	public Double getPrirmaretencion() {
		return this.prirmaretencion;
	}

	public void setPrirmaretencion(Double prirmaretencion) {
		this.prirmaretencion = prirmaretencion;
	}

	@Column(name = "PORCENTAJEPARTICIPACION", nullable = false, precision = 5, scale=10)
	public BigDecimal getPorcentajeParticipacion() {
		return this.porcentajeParticipacion;
	}

	public void setPorcentajeParticipacion(BigDecimal porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	@Column(name = "COMISION", nullable = false, precision = 5, scale=10)
	public BigDecimal getComision() {
		return this.comision;
	}

	public void setComision(BigDecimal comision) {
		this.comision = comision;
	}

	@Column(name = "TIPO", nullable = false, precision = 5)
	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "participacionFacultativoDTO")
	public List<ParticipacionCorredorFacultativoDTO> getParticipacionCorredorFacultativoDTOs() {
		return this.participacionCorredorFacultativoDTOs;
	}

	public void setParticipacionCorredorFacultativoDTOs(List<ParticipacionCorredorFacultativoDTO> participacionCorredorFacultativoDTOs) {
	     this.participacionCorredorFacultativoDTOs = participacionCorredorFacultativoDTOs;
	}

}