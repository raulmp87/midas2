package mx.com.afirme.midas.contratofacultativo;

// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.configuracionpagos.ConfiguracionPagosFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;

/**
 * Remote interface for ContratoFacultativoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ContratoFacultativoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved ContratoFacultativoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            ContratoFacultativoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public ContratoFacultativoDTO save(ContratoFacultativoDTO entity);

	/**
	 * Delete a persistent ContratoFacultativoDTO entity.
	 * 
	 * @param entity
	 *            ContratoFacultativoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ContratoFacultativoDTO entity);

	/**
	 * Persist a previously saved ContratoFacultativoDTO entity and return it or
	 * a copy of it to the sender. A copy of the ContratoFacultativoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            ContratoFacultativoDTO entity to update
	 * @return ContratoFacultativoDTO the persisted ContratoFacultativoDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ContratoFacultativoDTO update(ContratoFacultativoDTO entity);

	public ContratoFacultativoDTO findById(BigDecimal id);

	/**
	 * Find all ContratoFacultativoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ContratoFacultativoDTO property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ContratoFacultativoDTO> found by query
	 */
	public List<ContratoFacultativoDTO> findByProperty(String propertyName,
			Object value, int... rowStartIdxAndCount);

	public List<ContratoFacultativoDTO> findByNotaCobertura(
			Object notaCobertura, int... rowStartIdxAndCount);

	public List<ContratoFacultativoDTO> findBySumaAseguradaTotal(
			Object sumaAseguradaTotal, int... rowStartIdxAndCount);

	public List<ContratoFacultativoDTO> findBySumaAseguradaFacultada(
			Object sumaAseguradaFacultada, int... rowStartIdxAndCount);

	/**
	 * Find all ContratoFacultativoDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ContratoFacultativoDTO> all ContratoFacultativoDTO entities
	 */
	public List<ContratoFacultativoDTO> findAll(int... rowStartIdxAndCount);
	
	/**
	 * Autorize the Cotizaci�n Facultativa validating Coberturas, Participaciones and its details
	 * 
	 * @param BigDecimal Id of the corresponding CotizacionFacultativaDTO 
	 * 
	 * @return boolean, true if the autorization was positive and false if it was negative 
	 */
	public String autorizarCotizacionFacultativa(BigDecimal idToCotizacion);
	
	/**
	 * Autorize the Contrato Facultativa validating Coberturas, Participaciones and its details
	 * 
	 * @param BigDecimal Id of the corresponding CotizacionFacultativaDTO 
	 * 
	 * @return boolean, true if the autorization was positive and false if it was negative 
	 */
	public boolean autorizarContratoFacultativo(BigDecimal idTmContratoFacultativo);
	
	public boolean cancelarContratoFacultativo(BigDecimal idToSlip);
	 
	public List<ContratoFacultativoDTO> buscarFiltrado(ContratoFacultativoDTO contratoFacultativoDTO);
	
	public List<ConfiguracionPagosFacultativoDTO> findConfiguracionPagosFacultativoDTOByContratoFacultativo(BigDecimal idTmContratoFacultativo);
	
	public double obtenerPrimaPorReaseguradorYContratoFacultativo(ReaseguradorCorredorDTO reaseguradorCorredorDTO, ContratoFacultativoDTO contratoFacultativoDTO,BigDecimal porcentajeFacultativo);
	
	public void deleteSlipAndDetails(BigDecimal idToSlip);
	
	public boolean modificarNotaCobertura(ContratoFacultativoDTO contratoFacultativoDTO);
	
	public int obtenerCantidadContratosPorCotizacion(BigDecimal idToCotizacion,BigDecimal estatus);
	
	public List<ContratoFacultativoDTO> obtenerContratosPorCotizacion(BigDecimal idToCotizacion,BigDecimal estatus);
	
	public ContratoFacultativoDTO duplicarContratoFacultativo(BigDecimal idTmContratoFacultativo, BigDecimal idToSlip, int numeroEndoso);
	
	public List<ContratoFacultativoDTO> notificacionEndoso(SoporteReaseguroDTO soporteNuevoEndoso, List<Object[]> slipDTO_contratoFacultativoSoportaLineaList, int tipoEndoso);
	
	public ContratoFacultativoDTO procesarMovimientosCancelacionRehabilitacionCobertura(BigDecimal idTmContratoFacultativo, int tipoMovimiento, int movimiento);

	
	/**
	 * Este metodo copia las participaciones del contrato faculativo del
	 * endoso anterior, para cada una de las coberturas y lo persiste. 
	 * Se debe de cumplir las siguientes condiciones:
	 * 1. La suma asegurada del endoso anterior sea menor a la del endoso actual
	 * 	es decir de disminucion.
	 * Al cumplirse la regla entonces solo copiara las participaciones de las coberturas
	 * que en el contrato acutal tambien existan en el contrato anterior y se persisten
	 * estos cambios.
	 * 
	 * Si lo que se desea es solo copiar las participaciones para una sola cobertura entonces
	 * se debe de invocar a:
	 * 		copiarParticipacionesFacultativoCoberturaEndosoAnterior(DetalleContratoFacultativoDTO)
	 * @param idTmContratoFacultativoActual es el id del ContratoFacultativo el cual le queremos copiar 
	 * las participaciones del endoso anterior.
	 * @return un ContratoFacultativoDTO con todas sus participaciones actualizadas para
	 * cada uno de sus coberturas (DetalleContratoFacultativo)
	 */
	public ContratoFacultativoDTO copiarParticipacionesFacultativoEndosoAnterior(
			BigDecimal idTmContratoFacultativoActual);
	
	/**
	 * Este metodo, a diferencia de copiarParticipacionesFacultativoEndosoAnterior, copia solamente
	 * las participaciones para una cobertura dada (DetalleContratoFacultativoDTO), tomando como base 
	 * las del endoso anterior.
	 * @param idTdContratoFacultativoActual es el id del DetalleContratoFacultativo el cual queremos
	 * copiar las participaciones del endoso anterior (para esta cobertura).
	 * @return un DetalleContratoFacultativo con sus participaciones actualizadas.
	 */
	public DetalleContratoFacultativoDTO copiarParticipacionesFacultativoCoberturaEndosoAnterior(
			BigDecimal idTdContratoFacultativoActual);
	
	
	/**
	 * Este metodo obtiene la prima del endoso total, es decir, el total de cada una de las primas
	 * del endoso de cada cobertura (DetalleContratoFacultativoDTO.primaTotalCobertura). 
	 * @param idTmContratoFacultativo
	 * @return la sumatoria de la prima.
	 */
	public BigDecimal getPrimaEndosoTotal(BigDecimal idTmContratoFacultativo);
	
	/**
	 * Este metodo obtiene la prima negociada total, es decir, el total de cada una de las primas
	 * negociadas para cada una de las coberturas (DetalleContratoFacultativoDTO.primaFacultadaCobertura)
	 * @param idTmContratoFacultativo
	 * @return la sumatoria de la prima.
	 */
	public BigDecimal getPrimaNegociadaTotal(BigDecimal idTmContratoFacultativo);
	
	/**
     * Metodo usado para ajustar la prima adicional del contrato a partir de la prima adicional guardada en los detalles del mismo.
     */
	public ContratoFacultativoDTO ajustarPrimaAdicionalSegunDetalles(BigDecimal idTmContratoFacultativo);
	
	/**
	 * Metodo que consulta los ajustadores nombrados de los contratos facultativos registrados para una poliza en un endoso definido.
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @return Lista de los nombres de ajustadores nombrados registrados en los contratos facultativos de la poliza.
	 */
	public List<String> obtenerAjustadorNombrado(BigDecimal idToPoliza,short numeroEndoso);

	/**
	 * Este metodo obtiene la comisi�n total sobre la prima negociada por cobertura, es decir, el total de cada una de las primas
	 * negociadas multiplicada por la comisi�n de para cada una de las coberturas (DetalleContratoFacultativoDTO.primaFacultadaCobertura)
	 * @param idTmContratoFacultativo
	 * @return la sumatoria de la comisi�n sobre la prima.
	 */
	public BigDecimal getComisionTotal(BigDecimal idTmContratoFacultativo);
	
	/**
	 * Consulta las participaciones de un 
	 * @param idTdContratoFacultativo
	 * @param idTcReaseguradorCorredor
	 * @return
	 */
	public List<ParticipacionFacultativoDTO> findParticipacionFacultativo(BigDecimal idTdContratoFacultativo,BigDecimal idTcReaseguradorCorredor);
}