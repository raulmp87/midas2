package mx.com.afirme.midas.contratofacultativo.pagocobertura;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * PagoCoberturaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TMPAGOCOBERTURA"
    ,schema="MIDAS"
)

public class PagoCoberturaDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -6067287909614975851L;
	private PagoCoberturaId id;
    private PlanPagosCoberturaDTO planPagosCobertura;
    private Date fechaPago;
    private Double montoPagoPrimaNeta;
    private Set<PagoCoberturaReaseguradorDTO> pagoCoberturaReaseguradorDTOs = new HashSet<PagoCoberturaReaseguradorDTO>(0);
     
    
    
    
    BigDecimal idToPlanPagosCobertura;
    BigDecimal idReasegurador;
    String poliza;
    Integer endoso;
    Integer inciso;
    String seccion;
    String cobertura;
    Integer subinciso;
    String reasegurador;
    String corredor;
    Double primaReaseguro;
    Double porcentajeParticipacion;
    Double participacion;
    Double comision;
    Double porcentajeImpuesto;
    Byte retenerImpuesto;
    Double impuesto;
    Double primaNetaReaseguro;
        
    // Constructors

    /** default constructor */
    public PagoCoberturaDTO() {
    }

	/** minimal constructor */
    public PagoCoberturaDTO(PagoCoberturaId id, PlanPagosCoberturaDTO planPagosCobertura, Date fechaPago) {
        this.id = id;
        this.planPagosCobertura = planPagosCobertura;
        this.fechaPago = fechaPago;
    }
    
    /** full constructor */
    public PagoCoberturaDTO(PagoCoberturaId id, PlanPagosCoberturaDTO planPagosCobertura, Date fechaPago, Double montoPagoPrimaNeta, Set<PagoCoberturaReaseguradorDTO> pagoCoberturaReaseguradorDTOs) {
        this.id = id;
        this.planPagosCobertura = planPagosCobertura;
        this.fechaPago = fechaPago;
        this.montoPagoPrimaNeta = montoPagoPrimaNeta;
        this.pagoCoberturaReaseguradorDTOs = pagoCoberturaReaseguradorDTOs;
    }

   
    // Property accessors
    @EmbeddedId    
    @AttributeOverrides( {
        @AttributeOverride(name="idToPlanPagosCobertura", column=@Column(name="IDTOPLANPAGOSCOBERTURA", nullable=false, precision=10, scale=0) ), 
        @AttributeOverride(name="numeroExhibicion", column=@Column(name="NUMEROEXHIBICION", nullable=false, precision=3, scale=0) ) } )
    public PagoCoberturaId getId() {
        return this.id;
    }
    
    public void setId(PagoCoberturaId id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOPLANPAGOSCOBERTURA", nullable=false, insertable=false, updatable=false)
    public PlanPagosCoberturaDTO getPlanPagosCobertura() {
        return this.planPagosCobertura;
    }
    
    public void setPlanPagosCobertura(PlanPagosCoberturaDTO planPagosCobertura) {
        this.planPagosCobertura = planPagosCobertura;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAPAGO", nullable=false, length=7)
    public Date getFechaPago() {
        return this.fechaPago;
    }
    
    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }
    
    @Column(name="MONTOPAGOPRIMANETA", precision=24, scale=10)
    public Double getMontoPagoPrimaNeta() {
        return this.montoPagoPrimaNeta;
    }
    
    public void setMontoPagoPrimaNeta(Double montoPagoPrimaNeta) {
        this.montoPagoPrimaNeta = montoPagoPrimaNeta;
    }
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="pagoCobertura")
    public Set<PagoCoberturaReaseguradorDTO> getPagoCoberturaReaseguradorDTOs() {
        return this.pagoCoberturaReaseguradorDTOs;
    }
    
    public void setPagoCoberturaReaseguradorDTOs(Set<PagoCoberturaReaseguradorDTO> pagoCoberturaReaseguradorDTOs) {
        this.pagoCoberturaReaseguradorDTOs = pagoCoberturaReaseguradorDTOs;
    }

    @Transient
	public BigDecimal getIdToPlanPagosCobertura() {
		return idToPlanPagosCobertura;
	}

	public void setIdToPlanPagosCobertura(BigDecimal idToPlanPagosCobertura) {
		this.idToPlanPagosCobertura = idToPlanPagosCobertura;
	}

	@Transient
	public BigDecimal getIdReasegurador() {
		return idReasegurador;
	}

	public void setIdReasegurador(BigDecimal idReasegurador) {
		this.idReasegurador = idReasegurador;
	}

	@Transient
	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	@Transient
	public Integer getEndoso() {
		return endoso;
	}

	public void setEndoso(Integer endoso) {
		this.endoso = endoso;
	}

	@Transient
	public Integer getInciso() {
		return inciso;
	}

	public void setInciso(Integer inciso) {
		this.inciso = inciso;
	}

	@Transient
	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	@Transient
	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	@Transient
	public Integer getSubinciso() {
		return subinciso;
	}

	public void setSubinciso(Integer subinciso) {
		this.subinciso = subinciso;
	}

	@Transient
	public String getReasegurador() {
		return reasegurador;
	}

	public void setReasegurador(String reasegurador) {
		this.reasegurador = reasegurador;
	}

	@Transient
	public String getCorredor() {
		return corredor;
	}

	public void setCorredor(String corredor) {
		this.corredor = corredor;
	}

	@Transient
	public Double getPrimaReaseguro() {
		return primaReaseguro;
	}

	public void setPrimaReaseguro(Double primaReaseguro) {
		this.primaReaseguro = primaReaseguro;
	}

	@Transient
	public Double getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}

	public void setPorcentajeParticipacion(Double porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	@Transient
	public Double getParticipacion() {
		return participacion;
	}

	public void setParticipacion(Double participacion) {
		this.participacion = participacion;
	}

	@Transient
	public Double getComision() {
		return comision;
	}

	public void setComision(Double comision) {
		this.comision = comision;
	}

	@Transient
	public Double getPorcentajeImpuesto() {
		return porcentajeImpuesto;
	}

	public void setPorcentajeImpuesto(Double porcentajeImpuesto) {
		this.porcentajeImpuesto = porcentajeImpuesto;
	}

	@Transient
	public Byte getRetenerImpuesto() {
		return retenerImpuesto;
	}

	public void setRetenerImpuesto(Byte retenerImpuesto) {
		this.retenerImpuesto = retenerImpuesto;
	}

	@Transient
	public Double getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(Double impuesto) {
		this.impuesto = impuesto;
	}

	@Transient
	public Double getPrimaNetaReaseguro() {
		return primaNetaReaseguro;
	}

	public void setPrimaNetaReaseguro(Double primaNetaReaseguro) {
		this.primaNetaReaseguro = primaNetaReaseguro;
	}   
}