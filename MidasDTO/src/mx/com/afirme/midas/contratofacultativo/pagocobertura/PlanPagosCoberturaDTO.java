package mx.com.afirme.midas.contratofacultativo.pagocobertura;
// default package

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoDTO;


/**
 * PlanPagosCoberturaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOPLANPAGOSCOBERTURA"
    ,schema="MIDAS"
)

public class PlanPagosCoberturaDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	 private static final long serialVersionUID = 652624576851657073L;
	 private Long idToPlanPagosCobertura;
     private DetalleContratoFacultativoDTO detalleContratoFacultativoDTO;
     private Byte estatusAutorizacionPlanPagos;
     private Date fechaSolicitudAutorizacionPlanPagos;
     private String usuarioSolicitudAutorizacionPlanPagos;
     private Date fechaAutorizacionPlanPagos;
     private String usuarioAutorizacionPlanPagos;
     private Set<PagoCoberturaDTO> pagoCoberturaDTOs = new HashSet<PagoCoberturaDTO>(0);

    // Constructors

    /** default constructor */
    public PlanPagosCoberturaDTO() {
    }

	/** minimal constructor */
    public PlanPagosCoberturaDTO(Long idToPlanPagosCobertura, DetalleContratoFacultativoDTO detalleContratoFacultativoDTO, Byte estatusAutorizacionPlanPagos) {
        this.idToPlanPagosCobertura = idToPlanPagosCobertura;
        this.detalleContratoFacultativoDTO = detalleContratoFacultativoDTO;
        this.estatusAutorizacionPlanPagos = estatusAutorizacionPlanPagos;
    }
    
    /** full constructor */
    public PlanPagosCoberturaDTO(Long idToPlanPagosCobertura, DetalleContratoFacultativoDTO detalleContratoFacultativoDTO, Byte estatusAutorizacionPlanPagos, Date fechaSolicitudAutorizacionPlanPagos, String usuarioSolicitudAutorizacionPlanPagos, Date fechaAutorizacionPlanPagos, String usuarioAutorizacionPlanPagos, Set<PagoCoberturaDTO> pagoCoberturaDTOs) {
        this.idToPlanPagosCobertura = idToPlanPagosCobertura;
        this.detalleContratoFacultativoDTO = detalleContratoFacultativoDTO;
        this.estatusAutorizacionPlanPagos = estatusAutorizacionPlanPagos;
        this.fechaSolicitudAutorizacionPlanPagos = fechaSolicitudAutorizacionPlanPagos;
        this.usuarioSolicitudAutorizacionPlanPagos = usuarioSolicitudAutorizacionPlanPagos;
        this.fechaAutorizacionPlanPagos = fechaAutorizacionPlanPagos;
        this.usuarioAutorizacionPlanPagos = usuarioAutorizacionPlanPagos;
        this.pagoCoberturaDTOs = pagoCoberturaDTOs;
    }

   
    // Property accessors
    @Id     
    @SequenceGenerator(name = "IDTOPLANPAGOSCOBERTURA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOPLANPAGOSCOBERTURA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOPLANPAGOSCOBERTURA_SEQ_GENERADOR")
    @Column(name="IDTOPLANPAGOSCOBERTURA", unique=true, nullable=false, precision=10, scale=0)
    public Long getIdToPlanPagosCobertura() {
        return this.idToPlanPagosCobertura;
    }
    
    public void setIdToPlanPagosCobertura(Long idToPlanPagosCobertura) {
        this.idToPlanPagosCobertura = idToPlanPagosCobertura;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="IDTDCONTRATOFACULTATIVO", nullable=false, unique=true)
    public DetalleContratoFacultativoDTO getDetalleContratoFacultativoDTO() {
        return this.detalleContratoFacultativoDTO;
    }
    
    public void setDetalleContratoFacultativoDTO(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO) {
        this.detalleContratoFacultativoDTO = detalleContratoFacultativoDTO;
    }
    
    @Column(name="ESTATUSAUTPLANPAGOS", nullable=false, precision=2, scale=0)
    public Byte getEstatusAutorizacionPlanPagos() {
        return this.estatusAutorizacionPlanPagos;
    }
    
    public void setEstatusAutorizacionPlanPagos(Byte estatusAutorizacionPlanPagos) {
        this.estatusAutorizacionPlanPagos = estatusAutorizacionPlanPagos;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHASOLAUTPLANPAGOS", length=7)
    public Date getFechaSolicitudAutorizacionPlanPagos() {
        return this.fechaSolicitudAutorizacionPlanPagos;
    }
    
    public void setFechaSolicitudAutorizacionPlanPagos(Date fechaSolicitudAutorizacionPlanPagos) {
        this.fechaSolicitudAutorizacionPlanPagos = fechaSolicitudAutorizacionPlanPagos;
    }
    
    @Column(name="USUARIOSOLAUTPLANPAGOS", length=20)
    public String getUsuarioSolicitudAutorizacionPlanPagos() {
        return this.usuarioSolicitudAutorizacionPlanPagos;
    }
    
    public void setUsuarioSolicitudAutorizacionPlanPagos(String usuarioSolicitudAutorizacionPlanPagos) {
        this.usuarioSolicitudAutorizacionPlanPagos = usuarioSolicitudAutorizacionPlanPagos;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAAUTPLANPAGOS", length=7)
    public Date getFechaAutorizacionPlanPagos() {
        return this.fechaAutorizacionPlanPagos;
    }
    
    public void setFechaAutorizacionPlanPagos(Date fechaAutorizacionPlanPagos) {
        this.fechaAutorizacionPlanPagos = fechaAutorizacionPlanPagos;
    }
    
    @Column(name="USUARIOAUTPLANPAGOS", length=20)
    public String getUsuarioAutorizacionPlanPagos() {
        return this.usuarioAutorizacionPlanPagos;
    }
    
    public void setUsuarioAutorizacionPlanPagos(String usuarioAutorizacionPlanPagos) {
        this.usuarioAutorizacionPlanPagos = usuarioAutorizacionPlanPagos;
    }
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="planPagosCobertura")
    public Set<PagoCoberturaDTO> getPagoCoberturaDTOs() {
        return this.pagoCoberturaDTOs;
    }
    
    public void setPagoCoberturaDTOs(Set<PagoCoberturaDTO> pagoCoberturaDTOs) {
        this.pagoCoberturaDTOs = pagoCoberturaDTOs;
    }
}