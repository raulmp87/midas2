package mx.com.afirme.midas.contratofacultativo.pagocobertura;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.IdClass;
import javax.persistence.Id;

@SqlResultSetMapping(name="coberturaReaseguradorMapping", entities={
	    @EntityResult(entityClass=PagoCoberturaDecoradoDTO.class, fields = {
	        @FieldResult(name="idToPlanPagosCobertura", column="IDTOPLANPAGOSCOBERTURA"),
	        @FieldResult(name="idReasegurador", column="IDREASEGURADOR"),
	        @FieldResult(name="poliza", column="POLIZA"),
	        @FieldResult(name="endoso", column="ENDOSO"),
	        @FieldResult(name="inciso", column="INCISO"),
	        @FieldResult(name="idToSeccion", column="IDTOSECCION"),
	        @FieldResult(name="seccion", column="SECCION"),
	        @FieldResult(name="idToCobertura", column="IDTOCOBERTURA"),
	        @FieldResult(name="cobertura", column="COBERTURA"),
	        @FieldResult(name="subinciso", column="SUBINCISO"),
	        @FieldResult(name="reasegurador", column="REASEGURADOR"),
	        @FieldResult(name="corredor", column="CORREDOR"),
	        @FieldResult(name="primaReaseguro", column="PRIMA_REASEGURO"),
	        @FieldResult(name="porcentajeParticipacion", column="PCT_PARTICIPACION"),
	        @FieldResult(name="participacion", column="PARTICIPACION"),
	        @FieldResult(name="comision", column="COMISION"),
	        @FieldResult(name="porcentajeImpuesto", column="PCT_IMPUESTO_REAL"),
	        @FieldResult(name="retenerImpuesto", column="RETENER_IMPUESTO_REAL"),
	        @FieldResult(name="impuesto", column="IMPUESTO_REAL"),
	        @FieldResult(name="primaNetaReaseguro", column="PN_REASEGURO"),
	        @FieldResult(name="idToEstadoCuenta", column="IDTOESTADOCUENTA")
	    })
	}
)

@Entity
@IdClass(PagoCoberturaDecoradoPK.class)
public class PagoCoberturaDecoradoDTO  implements java.io.Serializable {    		
	private static final long serialVersionUID = -1822328313275957014L;
	
	private Long idToPlanPagosCobertura;
	private BigDecimal idReasegurador;
	private String poliza;
	private Integer endoso;
	private Integer inciso;
	private BigDecimal idToSeccion;
	private String seccion;
	private BigDecimal idToCobertura;
	private String cobertura;
	private Integer subinciso;
	private String reasegurador;
	private String corredor;
	private Double primaReaseguro;
	private Double porcentajeParticipacion;
	private Double participacion;
	private Double comision;
	private Double porcentajeImpuesto;
	private Byte retenerImpuesto;
	private Double impuesto;
	private Double primaNetaReaseguro;        
	private BigDecimal idToEstadoCuenta;

    public PagoCoberturaDecoradoDTO() {
    }  
    
    @Id
	public Long getIdToPlanPagosCobertura() {
		return idToPlanPagosCobertura;
	}

	public void setIdToPlanPagosCobertura(Long idToPlanPagosCobertura) {
		this.idToPlanPagosCobertura = idToPlanPagosCobertura;
	}

	@Id
	public BigDecimal getIdReasegurador() {
		return idReasegurador;
	}

	public void setIdReasegurador(BigDecimal idReasegurador) {
		this.idReasegurador = idReasegurador;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public Integer getEndoso() {
		return endoso;
	}

	public void setEndoso(Integer endoso) {
		this.endoso = endoso;
	}

	public Integer getInciso() {
		return inciso;
	}

	public void setInciso(Integer inciso) {
		this.inciso = inciso;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	public Integer getSubinciso() {
		return subinciso;
	}
	
	public void setSubinciso(Integer subinciso) {
		this.subinciso = subinciso;
	}

	public String getReasegurador() {
		return reasegurador;
	}
	
	public void setReasegurador(String reasegurador) {
		this.reasegurador = reasegurador;
	}

	public String getCorredor() {
		return corredor;
	}

	public void setCorredor(String corredor) {
		this.corredor = corredor;
	}

	public Double getPrimaReaseguro() {
		return primaReaseguro;
	}

	public void setPrimaReaseguro(Double primaReaseguro) {
		this.primaReaseguro = primaReaseguro;
	}

	public Double getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}

	public void setPorcentajeParticipacion(Double porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	public Double getParticipacion() {
		return participacion;
	}

	public void setParticipacion(Double participacion) {
		this.participacion = participacion;
	}

	public Double getComision() {
		return comision;
	}

	public void setComision(Double comision) {
		this.comision = comision;
	}

	public Double getPorcentajeImpuesto() {
		return porcentajeImpuesto;
	}

	public void setPorcentajeImpuesto(Double porcentajeImpuesto) {
		this.porcentajeImpuesto = porcentajeImpuesto;
	}

	public Byte getRetenerImpuesto() {
		return retenerImpuesto;
	}

	public void setRetenerImpuesto(Byte retenerImpuesto) {
		this.retenerImpuesto = retenerImpuesto;
	}

	public Double getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(Double impuesto) {
		this.impuesto = impuesto;
	}

	public Double getPrimaNetaReaseguro() {
		return primaNetaReaseguro;
	}

	public void setPrimaNetaReaseguro(Double primaNetaReaseguro) {
		this.primaNetaReaseguro = primaNetaReaseguro;
	}

	public BigDecimal getIdToEstadoCuenta() {
		return idToEstadoCuenta;
	}

	public void setIdToEstadoCuenta(BigDecimal idToEstadoCuenta) {
		this.idToEstadoCuenta = idToEstadoCuenta;
	}   
}