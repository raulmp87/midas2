package mx.com.afirme.midas.contratofacultativo.pagocobertura;

import java.math.BigDecimal;

public class PagoCoberturaReaseguradorDecoradoPK  implements java.io.Serializable {	
	private static final long serialVersionUID = -6943312662212890542L;
	
	private Long idToPlanPagosCobertura;
	private Short numeroExhibicion;
	private BigDecimal idReasegurador;
	
    public PagoCoberturaReaseguradorDecoradoPK() {
    }
   
   public Long getIdToPlanPagosCobertura() {
		return idToPlanPagosCobertura;
	}

	public void setIdToPlanPagosCobertura(Long idToPlanPagosCobertura) {
		this.idToPlanPagosCobertura = idToPlanPagosCobertura;
	}

	public Short getNumeroExhibicion() {
		return numeroExhibicion;
	}

	public void setNumeroExhibicion(Short numeroExhibicion) {
		this.numeroExhibicion = numeroExhibicion;
	}

	public BigDecimal getIdReasegurador() {
		return idReasegurador;
	}

	public void setIdReasegurador(BigDecimal idReasegurador) {
		this.idReasegurador = idReasegurador;
	}

	public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof PagoCoberturaReaseguradorDecoradoPK) ) return false;
		 PagoCoberturaReaseguradorDecoradoPK castOther = ( PagoCoberturaReaseguradorDecoradoPK ) other; 
         
		 return ( (this.getIdToPlanPagosCobertura()==castOther.getIdToPlanPagosCobertura()) || ( this.getIdToPlanPagosCobertura()!=null && castOther.getIdToPlanPagosCobertura()!=null && this.getIdToPlanPagosCobertura().equals(castOther.getIdToPlanPagosCobertura()) ) )
		 			&& ( (this.getNumeroExhibicion()==castOther.getNumeroExhibicion()) || ( this.getNumeroExhibicion()!=null && castOther.getNumeroExhibicion()!=null && this.getNumeroExhibicion().equals(castOther.getNumeroExhibicion()) ) )
		 			&& ( (this.getIdReasegurador()==castOther.getIdReasegurador()) || ( this.getIdReasegurador()!=null && castOther.getIdReasegurador()!=null && this.getIdReasegurador().equals(castOther.getIdReasegurador()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToPlanPagosCobertura() == null ? 0 : this.getIdToPlanPagosCobertura().hashCode() );
         result = 37 * result + ( getNumeroExhibicion() == null ? 0 : this.getNumeroExhibicion().hashCode() );
         result = 37 * result + ( getIdReasegurador() == null ? 0 : this.getIdReasegurador().hashCode() );
         return result;
   }
}