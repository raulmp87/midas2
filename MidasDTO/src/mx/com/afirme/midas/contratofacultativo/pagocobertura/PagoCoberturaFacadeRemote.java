package mx.com.afirme.midas.contratofacultativo.pagocobertura;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for PagoCoberturaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface PagoCoberturaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved PagoCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PagoCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public PagoCoberturaDTO save(PagoCoberturaDTO entity);
    /**
	 Delete a persistent PagoCoberturaDTO entity.
	  @param entity PagoCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PagoCoberturaDTO entity);
   /**
	 Persist a previously saved PagoCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the PagoCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PagoCoberturaDTO entity to update
	 @return PagoCoberturaDTO the persisted PagoCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public PagoCoberturaDTO update(PagoCoberturaDTO entity);
	public PagoCoberturaDTO findById( PagoCoberturaId id);
	 /**
	 * Find all PagoCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PagoCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<PagoCoberturaDTO> found by query
	 */
	public List<PagoCoberturaDTO> findByProperty(String propertyName, Object value
		);
	
	/**
	 * Find all PagoCoberturaDTO entities.
	  	  @return List<PagoCoberturaDTO> all PagoCoberturaDTO entities
	 */
	public List<PagoCoberturaDTO> findAll(
		);	
	
	public List<PagoCoberturaDTO> buscarPorDetalleContratoFacultativo(BigDecimal idTdContratoFacultativo);
	
	public void eliminarPagosPorContrato(BigDecimal idTmContratoFacultativo);
}