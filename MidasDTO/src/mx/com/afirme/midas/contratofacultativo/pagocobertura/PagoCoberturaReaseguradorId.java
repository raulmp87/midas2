package mx.com.afirme.midas.contratofacultativo.pagocobertura;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * PagoCoberturaReaseguradorId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class PagoCoberturaReaseguradorId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 9041177227234862369L;
	private Long idToPlanPagosCobertura;
     private Short numeroExhibicion;
     private BigDecimal idReasegurador;


    // Constructors

    /** default constructor */
    public PagoCoberturaReaseguradorId() {
    }

    
    /** full constructor */
    public PagoCoberturaReaseguradorId(Long idToPlanPagosCobertura, Short numeroExhibicion, BigDecimal idReasegurador) {
        this.idToPlanPagosCobertura = idToPlanPagosCobertura;
        this.numeroExhibicion = numeroExhibicion;
        this.idReasegurador = idReasegurador;
    }

   
    // Property accessors

    @Column(name="IDTOPLANPAGOSCOBERTURA", nullable=false, precision=10, scale=0)

    public Long getIdToPlanPagosCobertura() {
        return this.idToPlanPagosCobertura;
    }
    
    public void setIdToPlanPagosCobertura(Long idToPlanPagosCobertura) {
        this.idToPlanPagosCobertura = idToPlanPagosCobertura;
    }

    @Column(name="NUMEROEXHIBICION", nullable=false, precision=3, scale=0)

    public Short getNumeroExhibicion() {
        return this.numeroExhibicion;
    }
    
    public void setNumeroExhibicion(Short numeroExhibicion) {
        this.numeroExhibicion = numeroExhibicion;
    }

    @Column(name="IDREASEGURADOR", nullable=false, precision=22, scale=0)

    public BigDecimal getIdReasegurador() {
        return this.idReasegurador;
    }
    
    public void setIdReasegurador(BigDecimal idReasegurador) {
        this.idReasegurador = idReasegurador;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof PagoCoberturaReaseguradorId) ) return false;
		 PagoCoberturaReaseguradorId castOther = ( PagoCoberturaReaseguradorId ) other; 
         
		 return ( (this.getIdToPlanPagosCobertura()==castOther.getIdToPlanPagosCobertura()) || ( this.getIdToPlanPagosCobertura()!=null && castOther.getIdToPlanPagosCobertura()!=null && this.getIdToPlanPagosCobertura().equals(castOther.getIdToPlanPagosCobertura()) ) )
 && ( (this.getNumeroExhibicion()==castOther.getNumeroExhibicion()) || ( this.getNumeroExhibicion()!=null && castOther.getNumeroExhibicion()!=null && this.getNumeroExhibicion().equals(castOther.getNumeroExhibicion()) ) )
 && ( (this.getIdReasegurador()==castOther.getIdReasegurador()) || ( this.getIdReasegurador()!=null && castOther.getIdReasegurador()!=null && this.getIdReasegurador().equals(castOther.getIdReasegurador()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToPlanPagosCobertura() == null ? 0 : this.getIdToPlanPagosCobertura().hashCode() );
         result = 37 * result + ( getNumeroExhibicion() == null ? 0 : this.getNumeroExhibicion().hashCode() );
         result = 37 * result + ( getIdReasegurador() == null ? 0 : this.getIdReasegurador().hashCode() );
         return result;
   }   





}