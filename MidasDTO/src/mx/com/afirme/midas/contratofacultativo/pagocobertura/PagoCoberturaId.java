package mx.com.afirme.midas.contratofacultativo.pagocobertura;
// default package

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * PagoCoberturaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class PagoCoberturaId  implements java.io.Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 7494361903006084640L;
	// Fields    

     private Long idToPlanPagosCobertura;
     private Short numeroExhibicion;


    // Constructors

    /** default constructor */
    public PagoCoberturaId() {
    }

    
    /** full constructor */
    public PagoCoberturaId(Long idToPlanPagosCobertura, Short numeroExhibicion) {
        this.idToPlanPagosCobertura = idToPlanPagosCobertura;
        this.numeroExhibicion = numeroExhibicion;
    }
   
    // Property accessors

    @Column(name="IDTOPLANPAGOSCOBERTURA", nullable=false, precision=10, scale=0)
    public Long getIdToPlanPagosCobertura() {
        return this.idToPlanPagosCobertura;
    }
    
    public void setIdToPlanPagosCobertura(Long idToPlanPagosCobertura) {
        this.idToPlanPagosCobertura = idToPlanPagosCobertura;
    }

    @Column(name="NUMEROEXHIBICION", nullable=false, precision=3, scale=0)
    public Short getNumeroExhibicion() {
        return this.numeroExhibicion;
    }
    
    public void setNumeroExhibicion(Short numeroExhibicion) {
        this.numeroExhibicion = numeroExhibicion;
    }
   
   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof PagoCoberturaId) ) return false;
		 PagoCoberturaId castOther = ( PagoCoberturaId ) other; 
         
		 return ( (this.getIdToPlanPagosCobertura()==castOther.getIdToPlanPagosCobertura()) || ( this.getIdToPlanPagosCobertura()!=null && castOther.getIdToPlanPagosCobertura()!=null && this.getIdToPlanPagosCobertura().equals(castOther.getIdToPlanPagosCobertura()) ) )
 && ( (this.getNumeroExhibicion()==castOther.getNumeroExhibicion()) || ( this.getNumeroExhibicion()!=null && castOther.getNumeroExhibicion()!=null && this.getNumeroExhibicion().equals(castOther.getNumeroExhibicion()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToPlanPagosCobertura() == null ? 0 : this.getIdToPlanPagosCobertura().hashCode() );
         result = 37 * result + ( getNumeroExhibicion() == null ? 0 : this.getNumeroExhibicion().hashCode() );
         return result;
   }   





}