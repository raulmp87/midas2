package mx.com.afirme.midas.contratofacultativo.pagocobertura;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.IdClass;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SqlResultSetMapping(name="exhibicionMapping", entities={
	    @EntityResult(entityClass=PagoCoberturaReaseguradorDecoradoDTO.class, fields = {
	        @FieldResult(name="idToPlanPagosCobertura", column="IDTOPLANPAGOSCOBERTURA"),
	        @FieldResult(name="numeroExhibicion", column="NUMEROEXHIBICION"),
	        @FieldResult(name="idReasegurador", column="IDREASEGURADOR"),
	        @FieldResult(name="poliza", column="POLIZA"),
	        @FieldResult(name="endoso", column="ENDOSO"),
	        @FieldResult(name="inciso", column="INCISO"),
	        @FieldResult(name="idToSeccion", column="IDTOSECCION"),
	        @FieldResult(name="seccion", column="SECCION"),
	        @FieldResult(name="idToCobertura", column="IDTOCOBERTURA"),
	        @FieldResult(name="cobertura", column="COBERTURA"),
	        @FieldResult(name="subinciso", column="SUBINCISO"),
	        @FieldResult(name="reasegurador", column="REASEGURADOR"),
	        @FieldResult(name="fechaPago", column="FECHAPAGO"),
	        @FieldResult(name="montoPagoTotal", column="MONTOPAGOTOTAL"),
	        @FieldResult(name="estatusPago", column="ESTATUSPAGO"),		        
	        @FieldResult(name="estatusAutorizacionExhibicionVencida", column="ESTATUSAUTEXHVENCIDA"),
	        @FieldResult(name="fechaSolicitudAutorizacionExhibicionVencida", column="FECHASOLAUTEXHVENCIDA"),
	        @FieldResult(name="usuarioSolicitudAutorizacionExhibicionVencida", column="USUARIOSOLAUTEXHVENCIDA"),
	        @FieldResult(name="fechaAutorizacionExhibicionVencida", column="FECHAAUTEXHVENCIDA"),
	        @FieldResult(name="usuarioAutorizacionExhibicionVencida", column="USUARIOAUTEXHVENCIDA"),
	        @FieldResult(name="estatusAutorizacionFaltaPagoAsegurado", column="ESTATUSAUTFALTAPAGOASEG"),
	        @FieldResult(name="fechaSolicitudAutorizacionFaltaPagoAsegurado", column="FECHASOLAUTFALTAPAGOASEG"),
	        @FieldResult(name="usuarioSolicitudAutorizacionFaltaPagoAsegurado", column="USUARIOSOLAUTFALTAPAGOASEG"),
	        @FieldResult(name="fechaAutorizacionFaltaPagoAsegurado", column="FECHAAUTFALTAPAGOASEG"),
	        @FieldResult(name="usuarioAutorizacionFaltaPagoAsegurado", column="USUARIOAUTFALTAPAGOASEG"),
	        @FieldResult(name="idToEstadoCuenta", column="IDTOESTADOCUENTA")
	    })
	}
)

@Entity
@IdClass(PagoCoberturaReaseguradorDecoradoPK.class)
public class PagoCoberturaReaseguradorDecoradoDTO  implements java.io.Serializable {	
	private static final long serialVersionUID = 4730666876294039868L;
		
	private Long idToPlanPagosCobertura;
	private Short numeroExhibicion;
	private BigDecimal idReasegurador;
	private String poliza;
	private Integer endoso;
	private Integer inciso;
	private BigDecimal idToSeccion;
	private String seccion;
	private BigDecimal idToCobertura;
	private String cobertura;		
	private Integer subinciso;
	private String reasegurador;
	private String fechaPago;
	private Double montoPagoTotal;
    private Byte estatusPago;
    private Byte estatusAutorizacionExhibicionVencida;
    private Date fechaSolicitudAutorizacionExhibicionVencida;
    private String usuarioSolicitudAutorizacionExhibicionVencida;
    private Date fechaAutorizacionExhibicionVencida;
    private String usuarioAutorizacionExhibicionVencida;
    private Byte estatusAutorizacionFaltaPagoAsegurado;
    private Date fechaSolicitudAutorizacionFaltaPagoAsegurado;
    private String usuarioSolicitudAutorizacionFaltaPagoAsegurado;
    private Date fechaAutorizacionFaltaPagoAsegurado;
    private String usuarioAutorizacionFaltaPagoAsegurado;
    private BigDecimal idToEstadoCuenta;
        
    public PagoCoberturaReaseguradorDecoradoDTO() {
    }

    @Id
	public Long getIdToPlanPagosCobertura() {
		return idToPlanPagosCobertura;
	}

	public void setIdToPlanPagosCobertura(Long idToPlanPagosCobertura) {
		this.idToPlanPagosCobertura = idToPlanPagosCobertura;
	}

	@Id
	public Short getNumeroExhibicion() {
		return numeroExhibicion;
	}

	public void setNumeroExhibicion(Short numeroExhibicion) {
		this.numeroExhibicion = numeroExhibicion;
	}

	@Id
	public BigDecimal getIdReasegurador() {
		return idReasegurador;
	}

	public void setIdReasegurador(BigDecimal idReasegurador) {
		this.idReasegurador = idReasegurador;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public Integer getEndoso() {
		return endoso;
	}

	public void setEndoso(Integer endoso) {
		this.endoso = endoso;
	}

	public Integer getInciso() {
		return inciso;
	}

	public void setInciso(Integer inciso) {
		this.inciso = inciso;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	public Integer getSubinciso() {
		return subinciso;
	}

	public void setSubinciso(Integer subinciso) {
		this.subinciso = subinciso;
	}

	public String getReasegurador() {
		return reasegurador;
	}

	public void setReasegurador(String reasegurador) {
		this.reasegurador = reasegurador;
	}

	public String getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}

	public Double getMontoPagoTotal() {
		return montoPagoTotal;
	}

	public void setMontoPagoTotal(Double montoPagoTotal) {
		this.montoPagoTotal = montoPagoTotal;
	}

	public Byte getEstatusPago() {
		return estatusPago;
	}

	public void setEstatusPago(Byte estatusPago) {
		this.estatusPago = estatusPago;
	}

	public Byte getEstatusAutorizacionExhibicionVencida() {
		return estatusAutorizacionExhibicionVencida;
	}

	public void setEstatusAutorizacionExhibicionVencida(
			Byte estatusAutorizacionExhibicionVencida) {
		this.estatusAutorizacionExhibicionVencida = estatusAutorizacionExhibicionVencida;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaSolicitudAutorizacionExhibicionVencida() {
		return fechaSolicitudAutorizacionExhibicionVencida;
	}

	public void setFechaSolicitudAutorizacionExhibicionVencida(
			Date fechaSolicitudAutorizacionExhibicionVencida) {
		this.fechaSolicitudAutorizacionExhibicionVencida = fechaSolicitudAutorizacionExhibicionVencida;
	}

	public String getUsuarioSolicitudAutorizacionExhibicionVencida() {
		return usuarioSolicitudAutorizacionExhibicionVencida;
	}

	public void setUsuarioSolicitudAutorizacionExhibicionVencida(
			String usuarioSolicitudAutorizacionExhibicionVencida) {
		this.usuarioSolicitudAutorizacionExhibicionVencida = usuarioSolicitudAutorizacionExhibicionVencida;
	}

	@Temporal(TemporalType.TIMESTAMP)	
	public Date getFechaAutorizacionExhibicionVencida() {
		return fechaAutorizacionExhibicionVencida;
	}

	public void setFechaAutorizacionExhibicionVencida(
			Date fechaAutorizacionExhibicionVencida) {
		this.fechaAutorizacionExhibicionVencida = fechaAutorizacionExhibicionVencida;
	}

	public String getUsuarioAutorizacionExhibicionVencida() {
		return usuarioAutorizacionExhibicionVencida;
	}

	public void setUsuarioAutorizacionExhibicionVencida(
			String usuarioAutorizacionExhibicionVencida) {
		this.usuarioAutorizacionExhibicionVencida = usuarioAutorizacionExhibicionVencida;
	}

	public Byte getEstatusAutorizacionFaltaPagoAsegurado() {
		return estatusAutorizacionFaltaPagoAsegurado;
	}

	public void setEstatusAutorizacionFaltaPagoAsegurado(
			Byte estatusAutorizacionFaltaPagoAsegurado) {
		this.estatusAutorizacionFaltaPagoAsegurado = estatusAutorizacionFaltaPagoAsegurado;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaSolicitudAutorizacionFaltaPagoAsegurado() {
		return fechaSolicitudAutorizacionFaltaPagoAsegurado;
	}

	public void setFechaSolicitudAutorizacionFaltaPagoAsegurado(
			Date fechaSolicitudAutorizacionFaltaPagoAsegurado) {
		this.fechaSolicitudAutorizacionFaltaPagoAsegurado = fechaSolicitudAutorizacionFaltaPagoAsegurado;
	}

	public String getUsuarioSolicitudAutorizacionFaltaPagoAsegurado() {
		return usuarioSolicitudAutorizacionFaltaPagoAsegurado;
	}

	public void setUsuarioSolicitudAutorizacionFaltaPagoAsegurado(
			String usuarioSolicitudAutorizacionFaltaPagoAsegurado) {
		this.usuarioSolicitudAutorizacionFaltaPagoAsegurado = usuarioSolicitudAutorizacionFaltaPagoAsegurado;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaAutorizacionFaltaPagoAsegurado() {
		return fechaAutorizacionFaltaPagoAsegurado;
	}

	public void setFechaAutorizacionFaltaPagoAsegurado(
			Date fechaAutorizacionFaltaPagoAsegurado) {
		this.fechaAutorizacionFaltaPagoAsegurado = fechaAutorizacionFaltaPagoAsegurado;
	}

	public String getUsuarioAutorizacionFaltaPagoAsegurado() {
		return usuarioAutorizacionFaltaPagoAsegurado;
	}

	public void setUsuarioAutorizacionFaltaPagoAsegurado(
			String usuarioAutorizacionFaltaPagoAsegurado) {
		this.usuarioAutorizacionFaltaPagoAsegurado = usuarioAutorizacionFaltaPagoAsegurado;
	}

	public BigDecimal getIdToEstadoCuenta() {
		return idToEstadoCuenta;
	}

	public void setIdToEstadoCuenta(BigDecimal idToEstadoCuenta) {
		this.idToEstadoCuenta = idToEstadoCuenta;
	}
}