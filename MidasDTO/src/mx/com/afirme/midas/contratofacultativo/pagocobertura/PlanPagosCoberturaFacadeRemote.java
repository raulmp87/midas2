package mx.com.afirme.midas.contratofacultativo.pagocobertura;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

/**
 * Remote interface for PlanPagosCoberturaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface PlanPagosCoberturaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved PlanPagosCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PlanPagosCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public PlanPagosCoberturaDTO save(PlanPagosCoberturaDTO entity);
    /**
	 Delete a persistent PlanPagosCoberturaDTO entity.
	  @param entity PlanPagosCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PlanPagosCoberturaDTO entity);
   /**
	 Persist a previously saved PlanPagosCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the PlanPagosCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PlanPagosCoberturaDTO entity to update
	 @return PlanPagosCoberturaDTO the persisted PlanPagosCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public PlanPagosCoberturaDTO update(PlanPagosCoberturaDTO entity);
	public PlanPagosCoberturaDTO findById( Long id);
	 /**
	 * Find all PlanPagosCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PlanPagosCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<PlanPagosCoberturaDTO> found by query
	 */
	public List<PlanPagosCoberturaDTO> findByProperty(String propertyName, Object value
		);
	
	/**
	 * Find all PlanPagosCoberturaDTO entities.
	  	  @return List<PlanPagosCoberturaDTO> all PlanPagosCoberturaDTO entities
	 */
	public List<PlanPagosCoberturaDTO> findAll(
		);	
	
	public void modificarPagosCobertura(Long idToPlanPagosCobertura, 
			List<mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDTO> pagos, boolean omitirAutorizacion, String usuarioModificacion);
	
	public void distribuirPagosPorReasegurador(BigDecimal idTdContratoFacultativo)throws ExcepcionDeLogicaNegocio;
	
	public void autorizarPlanPagosPorContratoFacultativo(BigDecimal idTdContratoFacultativo);
	
	public List<PagoCoberturaDecoradoDTO> listarExhibicionesAgrupadasPorCobertura(String idsEstadoCuenta);
	
	public List<PagoCoberturaReaseguradorDecoradoDTO> listarExhibicionesCoberturasReaseguradores(String idsCoberturasReaseguradores);
	
	public PagoCoberturaReaseguradorDTO obtenerExhibicionPorId(PagoCoberturaReaseguradorId id);
	
	public PagoCoberturaReaseguradorDTO actualizarExhibicion(PagoCoberturaReaseguradorDTO exhibicion);
	
	public List<PagoCoberturaReaseguradorDTO> obtenerExhibicionesPorEgreso(BigDecimal idToEgresoReaseguro);
	
	public List<PagoCoberturaReaseguradorDecoradoDTO> listarExhibicionesDecoradas(String idsExhibiciones);
	
	public void eliminarPlanPagosPorIdContratoFacultativo(BigDecimal idTmContratoFacultativo) throws ExcepcionDeLogicaNegocio;
}