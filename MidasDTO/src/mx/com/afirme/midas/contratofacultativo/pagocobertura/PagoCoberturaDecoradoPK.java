package mx.com.afirme.midas.contratofacultativo.pagocobertura;

import java.math.BigDecimal;

public class PagoCoberturaDecoradoPK  implements java.io.Serializable {	
	private static final long serialVersionUID = -3448979380757394658L;
	
	private Long idToPlanPagosCobertura;
    private BigDecimal idReasegurador;

    public PagoCoberturaDecoradoPK() {
    }
    
    public Long getIdToPlanPagosCobertura() {
        return this.idToPlanPagosCobertura;
    }
    
    public void setIdToPlanPagosCobertura(Long idToPlanPagosCobertura) {
        this.idToPlanPagosCobertura = idToPlanPagosCobertura;
    }

    public BigDecimal getIdReasegurador() {
		return idReasegurador;
	}

	public void setIdReasegurador(BigDecimal idReasegurador) {
		this.idReasegurador = idReasegurador;
	}
   
   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof PagoCoberturaDecoradoPK) ) return false;
		 PagoCoberturaDecoradoPK castOther = ( PagoCoberturaDecoradoPK ) other; 
         
		 return ( (this.getIdToPlanPagosCobertura()==castOther.getIdToPlanPagosCobertura()) || ( this.getIdToPlanPagosCobertura()!=null && castOther.getIdToPlanPagosCobertura()!=null && this.getIdToPlanPagosCobertura().equals(castOther.getIdToPlanPagosCobertura()) ) )
 && ( (this.getIdReasegurador()==castOther.getIdReasegurador()) || ( this.getIdReasegurador()!=null && castOther.getIdReasegurador()!=null && this.getIdReasegurador().equals(castOther.getIdReasegurador()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToPlanPagosCobertura() == null ? 0 : this.getIdToPlanPagosCobertura().hashCode() );
         result = 37 * result + ( getIdReasegurador() == null ? 0 : this.getIdReasegurador().hashCode() );
         return result;
   }
}