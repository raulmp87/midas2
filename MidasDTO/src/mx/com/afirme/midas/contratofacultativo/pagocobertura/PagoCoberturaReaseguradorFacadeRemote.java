package mx.com.afirme.midas.contratofacultativo.pagocobertura;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for PagoCoberturaReaseguradorFacade.
 * @author MyEclipse Persistence Tools
 */


public interface PagoCoberturaReaseguradorFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved PagoCoberturaReaseguradorDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PagoCoberturaReaseguradorDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public PagoCoberturaReaseguradorDTO save(PagoCoberturaReaseguradorDTO entity);
    /**
	 Delete a persistent PagoCoberturaReaseguradorDTO entity.
	  @param entity PagoCoberturaReaseguradorDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PagoCoberturaReaseguradorDTO entity);
   /**
	 Persist a previously saved PagoCoberturaReaseguradorDTO entity and return it or a copy of it to the sender. 
	 A copy of the PagoCoberturaReaseguradorDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PagoCoberturaReaseguradorDTO entity to update
	 @return PagoCoberturaReaseguradorDTO the persisted PagoCoberturaReaseguradorDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public PagoCoberturaReaseguradorDTO update(PagoCoberturaReaseguradorDTO entity);
	public PagoCoberturaReaseguradorDTO findById( PagoCoberturaReaseguradorId id);
	 /**
	 * Find all PagoCoberturaReaseguradorDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PagoCoberturaReaseguradorDTO property to query
	  @param value the property value to match
	  	  @return List<PagoCoberturaReaseguradorDTO> found by query
	 */
	public List<PagoCoberturaReaseguradorDTO> findByProperty(String propertyName, Object value
		);
		
	/**
	 * Find all PagoCoberturaReaseguradorDTO entities.
	  	  @return List<PagoCoberturaReaseguradorDTO> all PagoCoberturaReaseguradorDTO entities
	 */
	public List<PagoCoberturaReaseguradorDTO> findAll();
	
	public void eliminarExhibicionesPorDetalleContrato(BigDecimal idTdContratoFacultativo);
	
	public void eliminarExhibicionesPorContrato(BigDecimal idTmContratoFacultativo);
}