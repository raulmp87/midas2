package mx.com.afirme.midas.contratofacultativo.pagocobertura;
// default package

import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratos.egreso.EgresoReaseguroDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;


/**
 * PagoCoberturaReaseguradorDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TDPAGOCOBERTURAREASEGURADOR"
    ,schema="MIDAS"
)

public class PagoCoberturaReaseguradorDTO  implements java.io.Serializable {
	public static final byte PENDIENTE = 0;
	public static final byte PROGRAMADO = 1;
	public static final byte PAGADO = 2;
	
	public static final short AUTORIZACION_PENDIENTE_EXHIBICION_VENCIDA = (short)1;
	public static final short AUTORIZACION_PENDIENTE_EXHIBICION_SIN_PAGO_ASEGURADO = (short)2;

    // Fields    

     /**
	 * 
	 */
	 private static final long serialVersionUID = 5952486506221811716L;
	 private PagoCoberturaReaseguradorId id;
     private PagoCoberturaDTO pagoCobertura;
     private ReaseguradorCorredorDTO reasegurador;
     private ReaseguradorCorredorDTO corredor;
     private EgresoReaseguroDTO egresoReaseguro;
     private EstadoCuentaDTO estadoCuenta;
     private Double porcentajeParticipacion;
     private Double montoPagoPrimaNeta;
     private Double porcentajeComision;
     private Double montoComision;
     private Double porcentajeImpuesto;
     private Double montoImpuesto;
     private Boolean retenerImpuesto;
     private Double montoPagoTotal;
     private Byte estatusPago;
     private Byte estatusAutorizacionExhibicionVencida;
     private Date fechaSolicitudAutorizacionExhibicionVencida;
     private String usuarioSolicitudAutorizacionExhibicionVencida;
     private Date fechaAutorizacionExhibicionVencida;
     private String usuarioAutorizacionExhibicionVencida;
     private Byte estatusAutorizacionFaltaPagoAsegurado;
     private Date fechaSolicitudAutorizacionFaltaPagoAsegurado;
     private String usuarioSolicitudAutorizacionFaltaPagoAsegurado;
     private Date fechaAutorizacionFaltaPagoAsegurado;
     private String usuarioAutorizacionFaltaPagoAsegurado;

    // Constructors

    /** default constructor */
    public PagoCoberturaReaseguradorDTO() {
    }

	/** minimal constructor */
    public PagoCoberturaReaseguradorDTO(PagoCoberturaReaseguradorId id, PagoCoberturaDTO pagoCobertura, ReaseguradorCorredorDTO reasegurador, 
    		                         	Double montoPagoPrimaNeta, Double montoComision, Double porcentajeImpuesto, Double montoImpuesto, 
    		                         	Boolean retenerImpuesto, Double montoPagoTotal, Byte estatusPago, Byte estatusAutorizacionExhibicionVencida, 
    		                         	Byte estatusAutorizacionFaltaPagoAsegurado) {
        this.id = id;
        this.pagoCobertura = pagoCobertura;
        this.reasegurador = reasegurador;
        this.montoPagoPrimaNeta = montoPagoPrimaNeta;
        this.montoComision = montoComision;
        this.porcentajeImpuesto = porcentajeImpuesto;
        this.montoImpuesto = montoImpuesto;
        this.retenerImpuesto = retenerImpuesto;
        this.montoPagoTotal = montoPagoTotal;
        this.estatusPago = estatusPago;
        this.estatusAutorizacionExhibicionVencida = estatusAutorizacionExhibicionVencida;
        this.estatusAutorizacionFaltaPagoAsegurado = estatusAutorizacionFaltaPagoAsegurado;
    }
    
    /** full constructor */
    public PagoCoberturaReaseguradorDTO(PagoCoberturaReaseguradorId id, PagoCoberturaDTO pagoCobertura, ReaseguradorCorredorDTO reasegurador, 
    									ReaseguradorCorredorDTO corredor, EgresoReaseguroDTO egresoReaseguro, Double montoPagoPrimaNeta, 
    									Double montoComision, Double porcentajeImpuesto, Double montoImpuesto, Boolean retenerImpuesto, 
    									Double montoPagoTotal, Byte estatusPago, Byte estatusAutorizacionExhibicionVencida, 
    									Date fechaSolicitudAutorizacionExhibicionVencida, String usuarioSolicitudAutorizacionExhibicionVencida, 
    									Date fechaAutorizacionExhibicionVencida, String usuarioAutorizacionExhibicionVencida, 
    									Byte estatusAutorizacionFaltaPagoAsegurado, Date fechaSolicitudAutorizacionFaltaPagoAsegurado, 
    									String usuarioSolicitudAutorizacionFaltaPagoAsegurado, Date fechaAutorizacionFaltaPagoAsegurado, 
    									String usuarioAutorizacionFaltaPagAsegurado) {
        this.id = id;
        this.pagoCobertura = pagoCobertura;
        this.reasegurador = reasegurador;
        this.corredor = corredor;
        this.egresoReaseguro = egresoReaseguro;
        this.montoPagoPrimaNeta = montoPagoPrimaNeta;
        this.montoComision = montoComision;
        this.porcentajeImpuesto = porcentajeImpuesto;
        this.montoImpuesto = montoImpuesto;
        this.retenerImpuesto = retenerImpuesto;
        this.montoPagoTotal = montoPagoTotal;
        this.estatusPago = estatusPago;
        this.estatusAutorizacionExhibicionVencida = estatusAutorizacionExhibicionVencida;
        this.fechaSolicitudAutorizacionExhibicionVencida = fechaSolicitudAutorizacionExhibicionVencida;
        this.usuarioSolicitudAutorizacionExhibicionVencida = usuarioSolicitudAutorizacionExhibicionVencida;
        this.fechaAutorizacionExhibicionVencida = fechaAutorizacionExhibicionVencida;
        this.usuarioAutorizacionExhibicionVencida = usuarioAutorizacionExhibicionVencida;
        this.estatusAutorizacionFaltaPagoAsegurado = estatusAutorizacionFaltaPagoAsegurado;
        this.fechaSolicitudAutorizacionFaltaPagoAsegurado = fechaSolicitudAutorizacionFaltaPagoAsegurado;
        this.usuarioSolicitudAutorizacionFaltaPagoAsegurado = usuarioSolicitudAutorizacionFaltaPagoAsegurado;
        this.fechaAutorizacionFaltaPagoAsegurado = fechaAutorizacionFaltaPagoAsegurado;
        this.usuarioAutorizacionFaltaPagoAsegurado = usuarioAutorizacionFaltaPagAsegurado;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idToPlanPagosCobertura", column=@Column(name="IDTOPLANPAGOSCOBERTURA", nullable=false, precision=10, scale=0) ), 
        @AttributeOverride(name="numeroExhibicion", column=@Column(name="NUMEROEXHIBICION", nullable=false, precision=3, scale=0) ), 
        @AttributeOverride(name="idReasegurador", column=@Column(name="IDREASEGURADOR", nullable=false, precision=22, scale=0) ) } )
    public PagoCoberturaReaseguradorId getId() {
        return this.id;
    }
    
    public void setId(PagoCoberturaReaseguradorId id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumns( { 
        @JoinColumn(name="IDTOPLANPAGOSCOBERTURA", referencedColumnName="IDTOPLANPAGOSCOBERTURA", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="NUMEROEXHIBICION", referencedColumnName="NUMEROEXHIBICION", nullable=false, insertable=false, updatable=false) } )
    public PagoCoberturaDTO getPagoCobertura() {
        return this.pagoCobertura;
    }
    
    public void setPagoCobertura(PagoCoberturaDTO pagoCobertura) {
        this.pagoCobertura = pagoCobertura;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDREASEGURADOR", nullable=false, insertable=false, updatable=false)
    public ReaseguradorCorredorDTO getReasegurador() {
        return this.reasegurador;
    }
    
    public void setReasegurador(ReaseguradorCorredorDTO reasegurador) {
        this.reasegurador = reasegurador;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDCORREDOR")
    public ReaseguradorCorredorDTO getCorredor() {
        return this.corredor;
    }
    
    public void setCorredor(ReaseguradorCorredorDTO corredor) {
        this.corredor = corredor;
    }
	
    @ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOEGRESOREASEGURO")
    public EgresoReaseguroDTO getEgresoReaseguro() {
        return this.egresoReaseguro;
    }
    
    public void setEgresoReaseguro(EgresoReaseguroDTO egresoReaseguro) {
        this.egresoReaseguro = egresoReaseguro;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOESTADOCUENTA")
    public EstadoCuentaDTO getEstadoCuenta() {
		return estadoCuenta;
	}

	public void setEstadoCuenta(EstadoCuentaDTO estadoCuenta) {
		this.estadoCuenta = estadoCuenta;
	}

	@Column(name="PORCENTAJEPARTICIPACION", nullable=false, precision=8, scale=4)
	public Double getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}

	public void setPorcentajeParticipacion(Double porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	@Column(name="MONTOPAGOPRIMANETA", nullable=false, precision=24, scale=10)
    public Double getMontoPagoPrimaNeta() {
        return this.montoPagoPrimaNeta;
    }
    
    public void setMontoPagoPrimaNeta(Double montoPagoPrimaNeta) {
        this.montoPagoPrimaNeta = montoPagoPrimaNeta;
    }
    
    @Column(name="PORCENTAJECOMISION", nullable=false, precision=8, scale=4)
    public Double getPorcentajeComision() {
		return porcentajeComision;
	}

	public void setPorcentajeComision(Double porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}

	@Column(name="MONTOCOMISION", nullable=false, precision=24, scale=10)
    public Double getMontoComision() {
        return this.montoComision;
    }
    
    public void setMontoComision(Double montoComision) {
        this.montoComision = montoComision;
    }
    
    @Column(name="PORCENTAJEIMPUESTO", nullable=false, precision=8, scale=4)
    public Double getPorcentajeImpuesto() {
        return this.porcentajeImpuesto;
    }
    
    public void setPorcentajeImpuesto(Double porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }
    
    @Column(name="MONTOIMPUESTO", nullable=false, precision=24, scale=10)
    public Double getMontoImpuesto() {
        return this.montoImpuesto;
    }
    
    public void setMontoImpuesto(Double montoImpuesto) {
        this.montoImpuesto = montoImpuesto;
    }
    
    @Column(name="RETENERIMPUESTO", nullable=false, precision=1, scale=0)
    public Boolean getRetenerImpuesto() {
        return this.retenerImpuesto;
    }
    
    public void setRetenerImpuesto(Boolean retenerImpuesto) {
        this.retenerImpuesto = retenerImpuesto;
    }
    
    @Column(name="MONTOPAGOTOTAL", nullable=false, precision=24, scale=10)
    public Double getMontoPagoTotal() {
        return this.montoPagoTotal;
    }
    
    public void setMontoPagoTotal(Double montoPagoTotal) {
        this.montoPagoTotal = montoPagoTotal;
    }
    
    @Column(name="ESTATUSPAGO", nullable=false, precision=2, scale=0)
    public Byte getEstatusPago() {
        return this.estatusPago;
    }
    
    public void setEstatusPago(Byte estatusPago) {
        this.estatusPago = estatusPago;
    }
    
    @Column(name="ESTATUSAUTEXHVENCIDA", nullable=false, precision=2, scale=0)
    public Byte getEstatusAutorizacionExhibicionVencida() {
        return this.estatusAutorizacionExhibicionVencida;
    }
    
    public void setEstatusAutorizacionExhibicionVencida(Byte estatusAutorizacionExhibicionVencida) {
        this.estatusAutorizacionExhibicionVencida = estatusAutorizacionExhibicionVencida;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHASOLAUTEXHVENCIDA", length=7)
    public Date getFechaSolicitudAutorizacionExhibicionVencida() {
        return this.fechaSolicitudAutorizacionExhibicionVencida;
    }
    
    public void setFechaSolicitudAutorizacionExhibicionVencida(Date fechaSolicitudAutorizacionExhibicionVencida) {
        this.fechaSolicitudAutorizacionExhibicionVencida = fechaSolicitudAutorizacionExhibicionVencida;
    }
    
    @Column(name="USUARIOSOLAUTEXHVENCIDA", length=20)
    public String getUsuarioSolicitudAutorizacionExhibicionVencida() {
        return this.usuarioSolicitudAutorizacionExhibicionVencida;
    }
    
    public void setUsuarioSolicitudAutorizacionExhibicionVencida(String usuarioSolicitudAutorizacionExhibicionVencida) {
        this.usuarioSolicitudAutorizacionExhibicionVencida = usuarioSolicitudAutorizacionExhibicionVencida;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAAUTEXHVENCIDA", length=7)
    public Date getFechaAutorizacionExhibicionVencida() {
        return this.fechaAutorizacionExhibicionVencida;
    }
    
    public void setFechaAutorizacionExhibicionVencida(Date fechaAutorizacionExhibicionVencida) {
        this.fechaAutorizacionExhibicionVencida = fechaAutorizacionExhibicionVencida;
    }
    
    @Column(name="USUARIOAUTEXHVENCIDA", length=20)
    public String getUsuarioAutorizacionExhibicionVencida() {
        return this.usuarioAutorizacionExhibicionVencida;
    }
    
    public void setUsuarioAutorizacionExhibicionVencida(String usuarioAutorizacionExhibicionVencida) {
        this.usuarioAutorizacionExhibicionVencida = usuarioAutorizacionExhibicionVencida;
    }
    
    @Column(name="ESTATUSAUTFALTAPAGOASEG", nullable=false, precision=2, scale=0)
    public Byte getEstatusAutorizacionFaltaPagoAsegurado() {
        return this.estatusAutorizacionFaltaPagoAsegurado;
    }
    
    public void setEstatusAutorizacionFaltaPagoAsegurado(Byte estatusAutorizacionFaltaPagoAsegurado) {
        this.estatusAutorizacionFaltaPagoAsegurado = estatusAutorizacionFaltaPagoAsegurado;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHASOLAUTFALTAPAGOASEG", length=7)
    public Date getFechaSolicitudAutorizacionFaltaPagoAsegurado() {
        return this.fechaSolicitudAutorizacionFaltaPagoAsegurado;
    }
    
    public void setFechaSolicitudAutorizacionFaltaPagoAsegurado(Date fechaSolicitudAutorizacionFaltaPagoAsegurado) {
        this.fechaSolicitudAutorizacionFaltaPagoAsegurado = fechaSolicitudAutorizacionFaltaPagoAsegurado;
    }
    
    @Column(name="USUARIOSOLAUTFALTAPAGOASEG", length=20)
    public String getUsuarioSolicitudAutorizacionFaltaPagoAsegurado() {
        return this.usuarioSolicitudAutorizacionFaltaPagoAsegurado;
    }
    
    public void setUsuarioSolicitudAutorizacionFaltaPagoAsegurado(String usuarioSolicitudAutorizacionFaltaPagoAsegurado) {
        this.usuarioSolicitudAutorizacionFaltaPagoAsegurado = usuarioSolicitudAutorizacionFaltaPagoAsegurado;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAAUTFALTAPAGOASEG", length=7)
    public Date getFechaAutorizacionFaltaPagoAsegurado() {
        return this.fechaAutorizacionFaltaPagoAsegurado;
    }
    
    public void setFechaAutorizacionFaltaPagoAsegurado(Date fechaAutorizacionFaltaPagoAsegurado) {
        this.fechaAutorizacionFaltaPagoAsegurado = fechaAutorizacionFaltaPagoAsegurado;
    }
    
    @Column(name="USUARIOAUTFALTAPAGOASEG", length=20)
    public String getUsuarioAutorizacionFaltaPagoAsegurado() {
        return this.usuarioAutorizacionFaltaPagoAsegurado;
    }
    
    public void setUsuarioAutorizacionFaltaPagoAsegurado(String usuarioAutorizacionFaltaPagAsegurado) {
        this.usuarioAutorizacionFaltaPagoAsegurado = usuarioAutorizacionFaltaPagAsegurado;
    }
}