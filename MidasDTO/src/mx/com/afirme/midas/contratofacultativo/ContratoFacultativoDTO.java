package mx.com.afirme.midas.contratofacultativo;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;

/**
 * ContratoFacultativoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TMCONTRATOFACULTATIVO", schema = "MIDAS")
public class ContratoFacultativoDTO extends LogBaseDTO {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -3495274202181609848L;
	private BigDecimal idTmContratoFacultativo;
	private SlipDTO slipDTO;
	private Date fechaInicial;
	private Date fechaFinal;
	private BigDecimal estatus;
	private BigDecimal idFormaDePago;
	private String notaCobertura;
	private BigDecimal sumaAseguradaTotal;
	private BigDecimal sumaAseguradaFacultada;
	private BigDecimal requiereControlReclamos;
	private List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOs = new ArrayList<DetalleContratoFacultativoDTO>(0);
	private int estadosCuentaCreados;     
 	private List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTOs =  new ArrayList<LineaSoporteReaseguroDTO>();
 	private Integer numeroEndosoFacultativo;
 	private BigDecimal idTmContratoFacultativoAnterior;
 	private BigDecimal montoPrimaAdicional;
 	private String ajustadorNombrado;
 	private Short esBonoPorNoSiniestro;

	// Constructors

	/** default constructor */
	public ContratoFacultativoDTO() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTMCONTRATOFACULTATIVO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTMCONTRATOFACULTATIVO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTMCONTRATOFACULTATIVO_SEQ_GENERADOR")
	@Column(name = "IDTMCONTRATOFACULTATIVO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTmContratoFacultativo() {
		return this.idTmContratoFacultativo;
	}

	public void setIdTmContratoFacultativo(
			BigDecimal idTmContratoFacultativo) {
		this.idTmContratoFacultativo = idTmContratoFacultativo;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOSLIP", nullable = false)
	public SlipDTO getSlipDTO() {
		return this.slipDTO;
	}

	public void setSlipDTO(SlipDTO slipDTO) {
		this.slipDTO = slipDTO;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAINICIAL", nullable = false, length = 7)
	public Date getFechaInicial() {
		return this.fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAFINAL", nullable = false, length = 7)
	public Date getFechaFinal() {
		return this.fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	@Column(name = "ESTATUS", nullable = false, precision = 22, scale = 0)
	public BigDecimal getEstatus() {
		return this.estatus;
	}

	public void setEstatus(BigDecimal estatus) {
		this.estatus = estatus;
	}

	@Column(name = "IDFORMADEPAGO", nullable = true, precision = 22, scale = 0)
	public BigDecimal getIdFormaDePago() {
		return this.idFormaDePago;
	}

	public void setIdFormaDePago(BigDecimal idFormaDePago) {
		this.idFormaDePago = idFormaDePago;
	}

	@Column(name = "NOTADELACOBERTURA")
	public String getNotaCobertura() {
		return this.notaCobertura;
	}

	public void setNotaCobertura(String notaCobertura) {
		this.notaCobertura = notaCobertura;
	}

	@Column(name = "SUMAASEGURADATOTAL", nullable = false, precision = 18, scale = 4)
	public BigDecimal getSumaAseguradaTotal() {
		return this.sumaAseguradaTotal;
	}

	public void setSumaAseguradaTotal(BigDecimal sumaAseguradaTotal) {
		this.sumaAseguradaTotal = sumaAseguradaTotal;
	}

	@Column(name = "SUMAASEGURADAFACULTADA", nullable = false, precision = 18, scale = 4)
	public BigDecimal getSumaAseguradaFacultada() {
		return this.sumaAseguradaFacultada;
	}

	public void setSumaAseguradaFacultada(BigDecimal sumaAseguradaFacultada) {
		this.sumaAseguradaFacultada = sumaAseguradaFacultada;
	}

	@Column(name = "REQUIERECONTROLRECLAMOS", nullable = false, precision = 22, scale = 0)
	public BigDecimal getRequiereControlReclamos() {
		return this.requiereControlReclamos;
	}

	public void setRequiereControlReclamos(BigDecimal requiereControlReclamos) {
		this.requiereControlReclamos = requiereControlReclamos;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "contratoFacultativoDTO_1")
	public List<DetalleContratoFacultativoDTO> getDetalleContratoFacultativoDTOs() {
		return this.detalleContratoFacultativoDTOs;
	}

	public void setDetalleContratoFacultativoDTOs(
			List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOs) {
		this.detalleContratoFacultativoDTOs = detalleContratoFacultativoDTOs;
	}
	
	@Column(name = "ESTADOSCUENTACREADOS", precision = 1, scale = 0)
	public int getEstadosCuentaCreados() {
		return this.estadosCuentaCreados;
	}

	public void setEstadosCuentaCreados(int estadosCuentaCreados) {
		this.estadosCuentaCreados = estadosCuentaCreados;
	}
	
	public void setLineaSoporteReaseguroDTOs(
			List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTOs) {
		this.lineaSoporteReaseguroDTOs = lineaSoporteReaseguroDTOs;
	}
	
	@OneToMany(mappedBy="contratoFacultativoDTO")
	public List<LineaSoporteReaseguroDTO> getLineaSoporteReaseguroDTOs() {
		return lineaSoporteReaseguroDTOs;
	}
	
	public ContratoFacultativoDTO duplicate(){
		ContratoFacultativoDTO contratoFacultativoDTO = new ContratoFacultativoDTO();
		
		contratoFacultativoDTO.setSlipDTO(this.slipDTO);
		contratoFacultativoDTO.setFechaInicial(this.fechaInicial);
		contratoFacultativoDTO.setFechaFinal(this.fechaFinal);
		contratoFacultativoDTO.setEstatus(this.estatus);
		contratoFacultativoDTO.setIdFormaDePago(this.idFormaDePago);
		contratoFacultativoDTO.setNotaCobertura(this.notaCobertura);
		contratoFacultativoDTO.setSumaAseguradaTotal(this.sumaAseguradaTotal);
		contratoFacultativoDTO.setSumaAseguradaFacultada(this.sumaAseguradaFacultada);
		contratoFacultativoDTO.setRequiereControlReclamos(this.requiereControlReclamos);
		contratoFacultativoDTO.setDetalleContratoFacultativoDTOs(this.detalleContratoFacultativoDTOs);
		contratoFacultativoDTO.setEstadosCuentaCreados(this.estadosCuentaCreados);
		contratoFacultativoDTO.setLineaSoporteReaseguroDTOs(this.lineaSoporteReaseguroDTOs);
		contratoFacultativoDTO.setAjustadorNombrado(this.ajustadorNombrado);
		
		return contratoFacultativoDTO;
	}
	
	@Column(name = "IDTMCONTRATOANTERIOR", nullable = true, precision = 22, scale = 0)
//	@Transient
	public BigDecimal getIdTmContratoFacultativoAnterior() {
		return this.idTmContratoFacultativoAnterior;
	}
	public void setIdTmContratoFacultativoAnterior(BigDecimal idTmContratoFacultativoAnterior) {
		this.idTmContratoFacultativoAnterior = idTmContratoFacultativoAnterior;
	}

	@Column(name = "NUMEROENDOSOFACULTATIVO", nullable = true, precision = 4, scale = 0)
//	@Transient
	public Integer getNumeroEndosoFacultativo() {
		return numeroEndosoFacultativo;
	}

	public void setNumeroEndosoFacultativo(Integer numeroEndosoFacultativo) {
		this.numeroEndosoFacultativo = numeroEndosoFacultativo;
	}

	@Column(name = "MONTOPRIMAADICIONAL", length = 500)
	public BigDecimal getMontoPrimaAdicional() {
		return montoPrimaAdicional;
	}

	public void setMontoPrimaAdicional(BigDecimal montoPrimaAdicional) {
		this.montoPrimaAdicional = montoPrimaAdicional;
	}

	@Column(name = "AJUSTADORNOMBRADO", length = 100)
	public String getAjustadorNombrado() {
		return ajustadorNombrado;
	}

	public void setAjustadorNombrado(String ajustadorNombrado) {
		this.ajustadorNombrado = ajustadorNombrado;
	}

	@Column(name = "ESBONOPORNOSINIESTRO", precision = 1, scale = 0, nullable=true)
	public Short getEsBonoPorNoSiniestro() {
		return esBonoPorNoSiniestro;
	}

	public void setEsBonoPorNoSiniestro(Short esBonoPorNoSiniestro) {
		this.esBonoPorNoSiniestro = esBonoPorNoSiniestro;
	}
}