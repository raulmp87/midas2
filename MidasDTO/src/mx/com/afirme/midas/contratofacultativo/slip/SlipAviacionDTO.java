package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * SlipAviacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSLIPAVIACION", schema = "MIDAS")
public class SlipAviacionDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToSlip;
	private SlipDTO slipDTO;
	private String subLimites;
	private Double pagosPasajeros;
	private String horasMarcaTipo;

	// Constructors

	/** default constructor */
	public SlipAviacionDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTOSLIP", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSlip() {
		return this.idToSlip;
	}

	public void setIdToSlip(BigDecimal idToSlip) {
		this.idToSlip = idToSlip;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOSLIP", unique = true, nullable = false, insertable = false, updatable = false)
	public SlipDTO getSlipDTO() {
		return this.slipDTO;
	}

	public void setSlipDTO(SlipDTO slipDTO) {
		this.slipDTO = slipDTO;
	}

	@Column(name = "SUBLIMITES", length = 240)
	public String getSubLimites() {
		return this.subLimites;
	}

	public void setSubLimites(String subLimites) {
		this.subLimites = subLimites;
	}

	@Column(name = "PAGOSPASAJEROS", precision = 16)
	public Double getPagosPasajeros() {
		return this.pagosPasajeros;
	}

	public void setPagosPasajeros(Double pagosPasajeros) {
		this.pagosPasajeros = pagosPasajeros;
	}

	@Column(name = "HORASMARCATIPO", length = 240)
	public String getHorasMarcaTipo() {
		return this.horasMarcaTipo;
	}

	public void setHorasMarcaTipo(String horasMarcaTipo) {
		this.horasMarcaTipo = horasMarcaTipo;
	}

}