package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * SlipObraCivilDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSLIPOBRACIVIL", schema = "MIDAS")
public class SlipObraCivilDTO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private BigDecimal idToSlip;
	private SlipDTO slipDTO;
	private String metodosYMaterialesConstruccion;
	private Date fechaTerminacionMtto;
	private String cumpleRegulacionesTerremotos;
	private String edificiosTerceros;
	private String existenEstructurasAdyacentes;
	private String tituloContrato;
	private String ingenieroConsultor;
	private String nombreContratista;
	private String direccionContratista;
	private String experienciaContratista;
	private String trabajoSubContratistas;
	private String condicionesMetereologicas;
	private String trabajosPorContrato;
	private String valorPorContrato;
	private String maquinaria;
	private String beneficiario;
	private String polizaSeparadaRF;
	private String aguasCercanas;
	private String sumaAseguradas;
	private String clausulaTerceros;
	private String nombreSubContratista;
	private String direccionSubContratista;	

	// Constructors

	/** default constructor */
	public SlipObraCivilDTO() {
	}


	// Property accessors
	@Id
	@Column(name = "IDTOSLIP", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSlip() {
		return this.idToSlip;
	}

	public void setIdToSlip(BigDecimal idToSlip) {
		this.idToSlip = idToSlip;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOSLIP", unique = true, nullable = false, insertable = false, updatable = false)
	public SlipDTO getSlipDTO() {
		return this.slipDTO;
	}

	public void setSlipDTO(SlipDTO slipDTO) {
		this.slipDTO = slipDTO;
	}

	@Column(name = "METODOSYMATERIALESCONSTRUCCION", nullable = false)
	public String getMetodosYMaterialesConstruccion() {
		return this.metodosYMaterialesConstruccion;
	}

	public void setMetodosYMaterialesConstruccion(
			String metodosYMaterialesConstruccion) {
		this.metodosYMaterialesConstruccion = metodosYMaterialesConstruccion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHATERMINACIONMTTO", nullable = false, length = 7)
	public Date getFechaTerminacionMtto() {
		return this.fechaTerminacionMtto;
	}

	public void setFechaTerminacionMtto(Date fechaTerminacionMtto) {
		this.fechaTerminacionMtto = fechaTerminacionMtto;
	}

	@Column(name = "CUMPLEREGULACIONESTERREMOTOS", nullable = false, length = 1)
	public String getCumpleRegulacionesTerremotos() {
		return this.cumpleRegulacionesTerremotos;
	}

	public void setCumpleRegulacionesTerremotos(
			String cumpleRegulacionesTerremotos) {
		this.cumpleRegulacionesTerremotos = cumpleRegulacionesTerremotos;
	}

	@Column(name = "EDIFICIOSTERCEROS", nullable = false)
	public String getEdificiosTerceros() {
		return this.edificiosTerceros;
	}

	public void setEdificiosTerceros(String edificiosTerceros) {
		this.edificiosTerceros = edificiosTerceros;
	}

	@Column(name = "EXISTENESTRUCTURASADYACENTES", nullable = false)
	public String getExistenEstructurasAdyacentes() {
		return this.existenEstructurasAdyacentes;
	}

	public void setExistenEstructurasAdyacentes(
			String existenEstructurasAdyacentes) {
		this.existenEstructurasAdyacentes = existenEstructurasAdyacentes;
	}

	@Column(name = "TITULOCONTRATO", length = 240)
	public String getTituloContrato() {
		return this.tituloContrato;
	}

	public void setTituloContrato(String tituloContrato) {
		this.tituloContrato = tituloContrato;
	}

	@Column(name = "INGENIEROCONSULTOR", length = 240)
	public String getIngenieroConsultor() {
		return this.ingenieroConsultor;
	}

	public void setIngenieroConsultor(String ingenieroConsultor) {
		this.ingenieroConsultor = ingenieroConsultor;
	}

	@Column(name = "NOMBRECONTRATISTA", length = 240)
	public String getNombreContratista() {
		return this.nombreContratista;
	}

	public void setNombreContratista(String nombreContratista) {
		this.nombreContratista = nombreContratista;
	}

	@Column(name = "DIRECCIONCONTRATISTA", length = 240)
	public String getDireccionContratista() {
		return this.direccionContratista;
	}

	public void setDireccionContratista(String direccionContratista) {
		this.direccionContratista = direccionContratista;
	}

	@Column(name = "EXPERIENCIACONTRATISTA", length = 240)
	public String getExperienciaContratista() {
		return this.experienciaContratista;
	}

	public void setExperienciaContratista(String experienciaContratista) {
		this.experienciaContratista = experienciaContratista;
	}

	@Column(name = "TRABAJOSUBCONTRATISTAS", length = 240)
	public String getTrabajoSubContratistas() {
		return this.trabajoSubContratistas;
	}

	public void setTrabajoSubContratistas(String trabajoSubContratistas) {
		this.trabajoSubContratistas = trabajoSubContratistas;
	}

	@Column(name = "CONDICIONESMETEREOLOGICAS", length = 240)
	public String getCondicionesMetereologicas() {
		return this.condicionesMetereologicas;
	}

	public void setCondicionesMetereologicas(String condicionesMetereologicas) {
		this.condicionesMetereologicas = condicionesMetereologicas;
	}

	@Column(name = "TRABAJOSPORCONTRATO", nullable = false)
	public String getTrabajosPorContrato() {
		return this.trabajosPorContrato;
	}

	public void setTrabajosPorContrato(String trabajosPorContrato) {
		this.trabajosPorContrato = trabajosPorContrato;
	}

	@Column(name = "VALORPORCONTRATO", length = 240)
	public String getValorPorContrato() {
		return this.valorPorContrato;
	}

	public void setValorPorContrato(String valorPorContrato) {
		this.valorPorContrato = valorPorContrato;
	}

	@Column(name = "MAQUINARIA", length = 240)
	public String getMaquinaria() {
		return this.maquinaria;
	}

	public void setMaquinaria(String maquinaria) {
		this.maquinaria = maquinaria;
	}

	@Column(name = "BENEFICIARIO", length = 240)
	public String getBeneficiario() {
		return this.beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	@Column(name = "POLIZASEPARADARC", length = 1)
	public String getPolizaSeparadaRF() {
		return this.polizaSeparadaRF;
	}

	public void setPolizaSeparadaRF(String polizaSeparadaRF) {
		this.polizaSeparadaRF = polizaSeparadaRF;
	}

	@Column(name = "AGUASCERCANAS", length = 240)
	public String getAguasCercanas() {
		return this.aguasCercanas;
	}

	public void setAguasCercanas(String aguasCercanas) {
		this.aguasCercanas = aguasCercanas;
	}

	@Column(name = "SUMASASEGURADAS", length = 240)
	public String getSumaAseguradas() {
		return this.sumaAseguradas;
	}

	public void setSumaAseguradas(String sumaAseguradas) {
		this.sumaAseguradas = sumaAseguradas;
	}

	@Column(name = "CLAUSULATERCEROS", length = 240)
	public String getClausulaTerceros() {
		return this.clausulaTerceros;
	}

	public void setClausulaTerceros(String clausulaTerceros) {
		this.clausulaTerceros = clausulaTerceros;
	}
	
	@Column(name = "NOMBRESUBCONTRATISTA", length = 240)
	public String getNombreSubContratista() {
		return this.nombreSubContratista;
	}

	public void setNombreSubContratista(String nombreSubContratista) {
		this.nombreSubContratista = nombreSubContratista;
	}

	@Column(name = "DIRECCIONSUBCONTRATISTA", length = 240)
	public String getDireccionSubContratista() {
		return this.direccionSubContratista;
	}

	public void setDireccionSubContratista(String direccionSubContratista) {
		this.direccionSubContratista = direccionSubContratista;
	}	

}