package mx.com.afirme.midas.contratofacultativo.slip;
// default package


import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * SlipAnexoEquipoContratistaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSLIPANEXOEQUIPOCONTRATISTA", schema = "MIDAS")
public class SlipAnexoEquipoContratistaDTO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private BigDecimal idToSlipDocumentoAnexo;
	private SlipDTO slipDTO;
	private BigDecimal idToControlArchivo;

	// Constructors

	/** default constructor */
	public SlipAnexoEquipoContratistaDTO() {
	}



	// Property accessors
	@Id
    @SequenceGenerator(name = "IDTOSLIPANEXO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOSLIPDOCUMENTOANEXO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOSLIPANEXO_SEQ_GENERADOR")	
	@Column(name = "IDTOSLIPDOCUMENTOANEXO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSlipDocumentoAnexo() {
		return this.idToSlipDocumentoAnexo;
	}

	public void setIdToSlipDocumentoAnexo(BigDecimal idToSlipDocumentoAnexo) {
		this.idToSlipDocumentoAnexo = idToSlipDocumentoAnexo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOSLIP", nullable = false)
	public SlipDTO getSlipDTO() {
		return this.slipDTO;
	}

	public void setSlipDTO(SlipDTO slipDTO) {
		this.slipDTO = slipDTO;
	}

	@Column(name = "IDTOCONTROLARRCHIVO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToControlArchivo() {
		return this.idToControlArchivo;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

}