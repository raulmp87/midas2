package mx.com.afirme.midas.contratofacultativo.slip;
// default package



//import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
//import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * SlipFuncionarioDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSLIPFUNCIONARIOS", schema = "MIDAS")
public class SlipFuncionarioDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//private BigDecimal idToSlip;
	
	private SlipFuncionarioDTOId id;	
	
	private SlipDTO slipDTO;
	private String interes;
	private String periodoNotificaciones;
	private Date fechaRetroactiva;
	private String retencion;
	private String limiteTerritorial;
	private String accionLegal;
	private String leyesJurisdiccion;
	private String condicionesGenerales;
	private String condicionesReaseguro;
	private String garantiaExpresa;
	private String subjetividades;
	private String terminoPrimas;
	private String comision;
	private String impuestos;
	
	private Set<SlipFuncionarioAnexoDTO> slipFuncionarioAnexoDTOs = new HashSet<SlipFuncionarioAnexoDTO>(
			0);	

	// Constructors

	/** default constructor */
	public SlipFuncionarioDTO() {
	}



	// Property accessors
	/*@Id
	@Column(name = "IDTOSLIP", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSlip() {
		return this.idToSlip;
	}

	public void setIdToSlip(BigDecimal idToSlip) {
		this.idToSlip = idToSlip;
	}*/
	
	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToSlip", column = @Column(name = "IDTOSLIP", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)) })
	public SlipFuncionarioDTOId getId() {
		return this.id;
	}

	public void setId(SlipFuncionarioDTOId id) {
		this.id = id;
	}	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOSLIP", unique = true, nullable = false, insertable = false, updatable = false)
	public SlipDTO getSlipDTO() {
		return this.slipDTO;
	}

	public void setSlipDTO(SlipDTO slipDTO) {
		this.slipDTO = slipDTO;
	}

	@Column(name = "INTERES", length = 240)
	public String getInteres() {
		return this.interes;
	}

	public void setInteres(String interes) {
		this.interes = interes;
	}

	@Column(name = "PERIODONOTIFICACIONES", length = 240)
	public String getPeriodoNotificaciones() {
		return this.periodoNotificaciones;
	}

	public void setPeriodoNotificaciones(String periodoNotificaciones) {
		this.periodoNotificaciones = periodoNotificaciones;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHARETROACTIVA", length = 7)
	public Date getFechaRetroactiva() {
		return this.fechaRetroactiva;
	}

	public void setFechaRetroactiva(Date fechaRetroactiva) {
		this.fechaRetroactiva = fechaRetroactiva;
	}

	@Column(name = "RETENCION", length = 240)
	public String getRetencion() {
		return this.retencion;
	}

	public void setRetencion(String retencion) {
		this.retencion = retencion;
	}

	@Column(name = "LIMITETERRITORIAL", length = 240)
	public String getLimiteTerritorial() {
		return this.limiteTerritorial;
	}

	public void setLimiteTerritorial(String limiteTerritorial) {
		this.limiteTerritorial = limiteTerritorial;
	}

	@Column(name = "ACCIONLEGAL", length = 240)
	public String getAccionLegal() {
		return this.accionLegal;
	}

	public void setAccionLegal(String accionLegal) {
		this.accionLegal = accionLegal;
	}

	@Column(name = "LEYESJURISDICCION", length = 240)
	public String getLeyesJurisdiccion() {
		return this.leyesJurisdiccion;
	}

	public void setLeyesJurisdiccion(String leyesJurisdiccion) {
		this.leyesJurisdiccion = leyesJurisdiccion;
	}

	@Column(name = "CONDICIONESGENERALES", length = 240)
	public String getCondicionesGenerales() {
		return this.condicionesGenerales;
	}

	public void setCondicionesGenerales(String condicionesGenerales) {
		this.condicionesGenerales = condicionesGenerales;
	}

	@Column(name = "CONDICIONESREASEGURO", length = 240)
	public String getCondicionesReaseguro() {
		return this.condicionesReaseguro;
	}

	public void setCondicionesReaseguro(String condicionesReaseguro) {
		this.condicionesReaseguro = condicionesReaseguro;
	}

	@Column(name = "GARANTIAEXPRESA", length = 240)
	public String getGarantiaExpresa() {
		return this.garantiaExpresa;
	}

	public void setGarantiaExpresa(String garantiaExpresa) {
		this.garantiaExpresa = garantiaExpresa;
	}

	@Column(name = "SUBJETIVIDADES", length = 240)
	public String getSubjetividades() {
		return this.subjetividades;
	}

	public void setSubjetividades(String subjetividades) {
		this.subjetividades = subjetividades;
	}

	@Column(name = "TERMINOPRIMAS", length = 240)
	public String getTerminoPrimas() {
		return this.terminoPrimas;
	}

	public void setTerminoPrimas(String terminoPrimas) {
		this.terminoPrimas = terminoPrimas;
	}

	@Column(name = "COMISION", length = 240)
	public String getComision() {
		return this.comision;
	}

	public void setComision(String comision) {
		this.comision = comision;
	}

	@Column(name = "IMPUESTOS", length = 240)
	public String getImpuestos() {
		return this.impuestos;
	}

	public void setImpuestos(String impuestos) {
		this.impuestos = impuestos;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "slipFuncionarioDTO")
	public Set<SlipFuncionarioAnexoDTO> getSlipFuncionarioAnexoDTOs() {
		return this.slipFuncionarioAnexoDTOs;
	}

	public void setSlipFuncionarioAnexoDTOs(
			Set<SlipFuncionarioAnexoDTO> slipFuncionarioAnexoDTOs) {
		this.slipFuncionarioAnexoDTOs = slipFuncionarioAnexoDTOs;
	}	

}