package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SlipObraCivilFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SlipObraCivilFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SlipObraCivilDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SlipObraCivilDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipObraCivilDTO entity);

	/**
	 * Delete a persistent SlipObraCivilDTO entity.
	 * 
	 * @param entity
	 *            SlipObraCivilDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipObraCivilDTO entity);

	/**
	 * Persist a previously saved SlipObraCivilDTO entity and return it or a
	 * copy of it to the sender. A copy of the SlipObraCivilDTO entity parameter
	 * is returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipObraCivilDTO entity to update
	 * @return SlipObraCivilDTO the persisted SlipObraCivilDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipObraCivilDTO update(SlipObraCivilDTO entity);

	public SlipObraCivilDTO findById(BigDecimal id);

	/**
	 * Find all SlipObraCivilDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipObraCivilDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipObraCivilDTO> found by query
	 */
	public List<SlipObraCivilDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SlipObraCivilDTO entities.
	 * 
	 * @return List<SlipObraCivilDTO> all SlipObraCivilDTO entities
	 */
	public List<SlipObraCivilDTO> findAll();
}