package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * SlipIncendioDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSLIPINCENDIO", schema = "MIDAS")
public class SlipIncendioDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToSlip;
	private SlipDTO slipDTO;
	private String proteccionesIncendios;

	// Constructors

	/** default constructor */
	public SlipIncendioDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTOSLIP", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSlip() {
		return this.idToSlip;
	}

	public void setIdToSlip(BigDecimal idToSlip) {
		this.idToSlip = idToSlip;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOSLIP", unique = true, nullable = false, insertable = false, updatable = false)
	public SlipDTO getSlipDTO() {
		return this.slipDTO;
	}

	public void setSlipDTO(SlipDTO slipDTO) {
		this.slipDTO = slipDTO;
	}

	@Column(name = "PROTECCIONESINCENDIOS", length = 240)
	public String getProteccionesIndencios() {
		return this.proteccionesIncendios;
	}

	public void setProteccionesIndencios(String proteccionesIndencios) {
		this.proteccionesIncendios = proteccionesIndencios;
	}

}