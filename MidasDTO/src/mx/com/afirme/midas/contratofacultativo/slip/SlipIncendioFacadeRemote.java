package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SlipIncendioFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SlipIncendioFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SlipIncendioDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SlipIncendioDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipIncendioDTO entity);

	/**
	 * Delete a persistent SlipIncendioDTO entity.
	 * 
	 * @param entity
	 *            SlipIncendioDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipIncendioDTO entity);

	/**
	 * Persist a previously saved SlipIncendioDTO entity and return it or a copy
	 * of it to the sender. A copy of the SlipIncendioDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipIncendioDTO entity to update
	 * @return SlipIncendioDTO the persisted SlipIncendioDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipIncendioDTO update(SlipIncendioDTO entity);

	public SlipIncendioDTO findById(BigDecimal id);

	/**
	 * Find all SlipIncendioDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipIncendioDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipIncendioDTO> found by query
	 */
	public List<SlipIncendioDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SlipIncendioDTO entities.
	 * 
	 * @return List<SlipIncendioDTO> all SlipIncendioDTO entities
	 */
	public List<SlipIncendioDTO> findAll();
}