package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SlipAnexoConstructoresFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SlipAnexoConstructoresFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SlipAnexoConstructoresDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SlipAnexoConstructoresDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipAnexoConstructoresDTO entity);

	/**
	 * Delete a persistent SlipAnexoConstructoresDTO entity.
	 * 
	 * @param entity
	 *            SlipAnexoConstructoresDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipAnexoConstructoresDTO entity);

	/**
	 * Persist a previously saved SlipAnexoConstructoresDTO entity and return it
	 * or a copy of it to the sender. A copy of the SlipAnexoConstructoresDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipAnexoConstructoresDTO entity to update
	 * @return SlipAnexoConstructoresDTO the persisted SlipAnexoConstructoresDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipAnexoConstructoresDTO update(SlipAnexoConstructoresDTO entity);

	public SlipAnexoConstructoresDTO findById(BigDecimal id);

	/**
	 * Find all SlipAnexoConstructoresDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SlipAnexoConstructoresDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipAnexoConstructoresDTO> found by query
	 */
	public List<SlipAnexoConstructoresDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SlipAnexoConstructoresDTO entities.
	 * 
	 * @return List<SlipAnexoConstructoresDTO> all SlipAnexoConstructoresDTO
	 *         entities
	 */
	public List<SlipAnexoConstructoresDTO> findAll();
	
	
	/**
	 * @param constructor
	 * @return
	 */
	public List<SlipAnexoConstructoresDTO> buscarAnexos(BigDecimal idToSlip, BigDecimal numeroInciso);
}