package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SlipAnexoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SlipAnexoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SlipAnexoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SlipAnexoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public SlipAnexoDTO save(SlipAnexoDTO entity);

	/**
	 * Delete a persistent SlipAnexoDTO entity.
	 * 
	 * @param entity
	 *            SlipAnexoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipAnexoDTO entity);

	/**
	 * Persist a previously saved SlipAnexoDTO entity and return it or a copy of
	 * it to the sender. A copy of the SlipAnexoDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SlipAnexoDTO entity to update
	 * @return SlipAnexoDTO the persisted SlipAnexoDTO entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipAnexoDTO update(SlipAnexoDTO entity);

	public SlipAnexoDTO findById(BigDecimal id);

	/**
	 * Find all SlipAnexoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipAnexoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipAnexoDTO> found by query
	 */
	public List<SlipAnexoDTO> findByProperty(String propertyName, Object value);
	
	
	public List<SlipAnexoDTO> encontrarSlipAnaxoDTO(BigDecimal idToSlip);

	/**
	 * Find all SlipAnexoDTO entities.
	 * 
	 * @return List<SlipAnexoDTO> all SlipAnexoDTO entities
	 */
	public List<SlipAnexoDTO> findAll();
}