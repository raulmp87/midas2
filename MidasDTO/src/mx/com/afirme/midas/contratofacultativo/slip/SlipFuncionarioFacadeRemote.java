package mx.com.afirme.midas.contratofacultativo.slip;
// default package



import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SlipFuncionarioFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SlipFuncionarioFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SlipFuncionarioDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SlipFuncionarioDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipFuncionarioDTO entity);

	/**
	 * Delete a persistent SlipFuncionarioDTO entity.
	 * 
	 * @param entity
	 *            SlipFuncionarioDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipFuncionarioDTO entity);

	/**
	 * Persist a previously saved SlipFuncionarioDTO entity and return it or a
	 * copy of it to the sender. A copy of the SlipFuncionarioDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipFuncionarioDTO entity to update
	 * @return SlipFuncionarioDTO the persisted SlipFuncionarioDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipFuncionarioDTO update(SlipFuncionarioDTO entity);

	//public SlipFuncionarioDTO findById(BigDecimal id);
	public SlipFuncionarioDTO findById(SlipFuncionarioDTOId id);
	/**
	 * Find all SlipFuncionarioDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipFuncionarioDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipFuncionarioDTO> found by query
	 */
	public List<SlipFuncionarioDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SlipFuncionarioDTO entities.
	 * 
	 * @return List<SlipFuncionarioDTO> all SlipFuncionarioDTO entities
	 */
	public List<SlipFuncionarioDTO> findAll();
}