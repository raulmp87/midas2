package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SlipAnexoEquipoContratistaFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SlipAnexoEquipoContratistaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * SlipAnexoEquipoContratistaDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            SlipAnexoEquipoContratistaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipAnexoEquipoContratistaDTO entity);

	/**
	 * Delete a persistent SlipAnexoEquipoContratistaDTO entity.
	 * 
	 * @param entity
	 *            SlipAnexoEquipoContratistaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipAnexoEquipoContratistaDTO entity);

	/**
	 * Persist a previously saved SlipAnexoEquipoContratistaDTO entity and
	 * return it or a copy of it to the sender. A copy of the
	 * SlipAnexoEquipoContratistaDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            SlipAnexoEquipoContratistaDTO entity to update
	 * @return SlipAnexoEquipoContratistaDTO the persisted
	 *         SlipAnexoEquipoContratistaDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipAnexoEquipoContratistaDTO update(
			SlipAnexoEquipoContratistaDTO entity);

	public SlipAnexoEquipoContratistaDTO findById(BigDecimal id);

	/**
	 * Find all SlipAnexoEquipoContratistaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SlipAnexoEquipoContratistaDTO property to
	 *            query
	 * @param value
	 *            the property value to match
	 * @return List<SlipAnexoEquipoContratistaDTO> found by query
	 */
	public List<SlipAnexoEquipoContratistaDTO> findByProperty(
			String propertyName, Object value);

	/**
	 * Find all SlipAnexoEquipoContratistaDTO entities.
	 * 
	 * @return List<SlipAnexoEquipoContratistaDTO> all
	 *         SlipAnexoEquipoContratistaDTO entities
	 */
	public List<SlipAnexoEquipoContratistaDTO> findAll();
}