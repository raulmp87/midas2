package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;

/**
 * Remote interface for SlipDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SlipFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SlipDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SlipDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public SlipDTO save(SlipDTO entity);

	/**
	 * Delete a persistent SlipDTO entity.
	 * 
	 * @param entity
	 *            SlipDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipDTO entity);

	/**
	 * Persist a previously saved SlipDTO entity and return it or a copy of it
	 * to the sender. A copy of the SlipDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SlipDTO entity to update
	 * @return SlipDTO the persisted SlipDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipDTO update(SlipDTO entity);

	public SlipDTO findById(BigDecimal id);

	/**
	 * Find all SlipDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipDTO> found by query
	 */
	public List<SlipDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all SlipDTO entities.
	 * 
	 * @return List<SlipDTO> all SlipDTO entities
	 */
	public List<SlipDTO> findAll();
	
	
	public List<SlipDTO> listarCotizacionesEstatus(String estatus);
	
	public SlipDTO obtenerSlipLineaSoporte(LineaSoporteReaseguroDTO lineaSoporteReaseguro);
	
	public SlipDTO obtenerSlipLineaSoporteDuplicado(LineaSoporteReaseguroDTO lineaSoporteReaseguro);

	public List<SlipDTO> listarSlipPorEstatus(BigDecimal idTocotizacion, String estatusCotizacion);
	
	public void cancelarCotizacionFacultativo(BigDecimal idToCotizacion);
	
	public void modificarPorcentajesContratos(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO, SlipDTO slipDTO, ContratoFacultativoDTO contratoFacultativoDTO);
	
	public int obtenerCantidadSlipsPorCotizacion(BigDecimal idToCotizacion);
	
	public List<SlipDTO> listarFiltrado(SlipDTO entity);
	
	public SlipDTO llenarSlip(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO);
	
	public SlipDTO establecerTipoSlip(SlipDTO slipDTO);
}