package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SlipAviacionFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SlipAviacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SlipAviacionDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SlipAviacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipAviacionDTO entity);

	/**
	 * Delete a persistent SlipAviacionDTO entity.
	 * 
	 * @param entity
	 *            SlipAviacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipAviacionDTO entity);

	/**
	 * Persist a previously saved SlipAviacionDTO entity and return it or a copy
	 * of it to the sender. A copy of the SlipAviacionDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipAviacionDTO entity to update
	 * @return SlipAviacionDTO the persisted SlipAviacionDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipAviacionDTO update(SlipAviacionDTO entity);

	public SlipAviacionDTO findById(BigDecimal id);

	/**
	 * Find all SlipAviacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipAviacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipAviacionDTO> found by query
	 */
	public List<SlipAviacionDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SlipAviacionDTO entities.
	 * 
	 * @return List<SlipAviacionDTO> all SlipAviacionDTO entities
	 */
	public List<SlipAviacionDTO> findAll();
}