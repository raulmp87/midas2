package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SlipFuncionarioAnexoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SlipFuncionarioAnexoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SlipFuncionarioAnexoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SlipFuncionarioAnexoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipFuncionarioAnexoDTO entity);

	/**
	 * Delete a persistent SlipFuncionarioAnexoDTO entity.
	 * 
	 * @param entity
	 *            SlipFuncionarioAnexoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipFuncionarioAnexoDTO entity);

	/**
	 * Persist a previously saved SlipFuncionarioAnexoDTO entity and return it
	 * or a copy of it to the sender. A copy of the SlipFuncionarioAnexoDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipFuncionarioAnexoDTO entity to update
	 * @return SlipFuncionarioAnexoDTO the persisted SlipFuncionarioAnexoDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipFuncionarioAnexoDTO update(SlipFuncionarioAnexoDTO entity);

	public SlipFuncionarioAnexoDTO findById(BigDecimal id);

	/**
	 * Find all SlipFuncionarioAnexoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipFuncionarioAnexoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipFuncionarioAnexoDTO> found by query
	 */
	public List<SlipFuncionarioAnexoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SlipFuncionarioAnexoDTO entities.
	 * 
	 * @return List<SlipFuncionarioAnexoDTO> all SlipFuncionarioAnexoDTO
	 *         entities
	 */
	public List<SlipFuncionarioAnexoDTO> findAll();
	
	/**
	 * Busca todos los registros SlipFuncionarioAnexoDTO registrados para el idToSlip y numeroInciso Recibidos.
	 * @param idToSlip
	 * @param numeroInciso
	 * @return lista de instancias SlipFuncionarioAnexoDTO registradas para el slip y numero inciso recibidos.
	 */
	public List<SlipFuncionarioAnexoDTO> obtenerAnexosPorSlipInciso(BigDecimal idToSlip,BigDecimal numeroInciso);
}