package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * SlipBarcoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSLIPBARCO", schema = "MIDAS")
public class SlipBarcoDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToSlip;
	private SlipDTO slipDTO;
	private String lugarConstruccion;
	private String serie;
	private String matricula;
	private Date ultimaFechaMtto;

	// Constructors

	/** default constructor */
	public SlipBarcoDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTOSLIP", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSlip() {
		return this.idToSlip;
	}

	public void setIdToSlip(BigDecimal idToSlip) {
		this.idToSlip = idToSlip;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOSLIP", unique = true, nullable = false, insertable = false, updatable = false)
	public SlipDTO getSlipDTO() {
		return this.slipDTO;
	}

	public void setSlipDTO(SlipDTO slipDTO) {
		this.slipDTO = slipDTO;
	}

	@Column(name = "LUGARCONSTRUCCION", nullable = false)
	public String getLugarConstruccion() {
		return this.lugarConstruccion;
	}

	public void setLugarConstruccion(String lugarConstruccion) {
		this.lugarConstruccion = lugarConstruccion;
	}

	@Column(name = "SERIE", length = 240)
	public String getSerie() {
		return this.serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	@Column(name = "MATRICULA", length = 240)
	public String getMatricula() {
		return this.matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "ULTIMAFECHAMTTO", length = 7)
	public Date getUltimaFechaMtto() {
		return this.ultimaFechaMtto;
	}

	public void setUltimaFechaMtto(Date ultimaFechaMtto) {
		this.ultimaFechaMtto = ultimaFechaMtto;
	}

}