package mx.com.afirme.midas.contratofacultativo.slip;
// default package


import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * SlipFuncionarioAnexoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSLIPFUNCIONARIOSANEXOS", schema = "MIDAS")
public class SlipFuncionarioAnexoDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idSlipDocumentoAnexo;
	private SlipFuncionarioDTO slipFuncionarioDTO;
	private BigDecimal idControlArchivo;

	// Constructors

	/** default constructor */
	public SlipFuncionarioAnexoDTO() {
	}

	/** full constructor */
	public SlipFuncionarioAnexoDTO(BigDecimal idSlipDocumentoAnexo,
			SlipFuncionarioDTO slipFuncionarioDTO, BigDecimal idControlArchivo) {
		this.idSlipDocumentoAnexo = idSlipDocumentoAnexo;
		this.slipFuncionarioDTO = slipFuncionarioDTO;
		this.idControlArchivo = idControlArchivo;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOSLIPANEXO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOSLIPDOCUMENTOANEXO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOSLIPANEXO_SEQ_GENERADOR")	
 	@Column(name = "IDTOSLIPDOCUMENTOANEXO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdSlipDocumentoAnexo() {
		return this.idSlipDocumentoAnexo;
	}

	public void setIdSlipDocumentoAnexo(BigDecimal idSlipDocumentoAnexo) {
		this.idSlipDocumentoAnexo = idSlipDocumentoAnexo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOSLIP", referencedColumnName = "IDTOSLIP", nullable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false) })
	public SlipFuncionarioDTO getSlipFuncionarioDTO() {
		return this.slipFuncionarioDTO;
	}

	public void setSlipFuncionarioDTO(SlipFuncionarioDTO slipFuncionarioDTO) {
		this.slipFuncionarioDTO = slipFuncionarioDTO;
	}


	@Column(name = "IDTOCONTROLARCHIVO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdControlArchivo() {
		return this.idControlArchivo;
	}

	public void setIdControlArchivo(BigDecimal idControlArchivo) {
		this.idControlArchivo = idControlArchivo;
	}

}