package mx.com.afirme.midas.contratofacultativo.slip;
// default package


import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * SlipConstructoresDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSLIPRCCONSTRUCTORES", schema = "MIDAS")
public class SlipConstructoresDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private SlipConstructoresDTOId id;	
	
	//private BigDecimal idToSlip;	
	
	
	private SlipDTO slipDTO;
	private String sitioTrabajos;
	private Double valorEstimadoObra;
	private String colindantes;
	private String sistemasPrevistos;
	private String experienciaLaboral;
	private String paraQuienRealizaLaObra;
	private String tipoMaquinaria;
	
	private Set<SlipAnexoConstructoresDTO> slipAnexoConstructoresDTOs = new HashSet<SlipAnexoConstructoresDTO>(
			0);	

	// Constructors

	/** default constructor */
	public SlipConstructoresDTO() {
	}



	// Property accessors
	/*@Id
	@Column(name = "IDTOSLIP", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSlip() {
		return this.idToSlip;
	}

	public void setIdToSlip(BigDecimal idToSlip) {
		this.idToSlip = idToSlip;
	}*/
	
	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToSlip", column = @Column(name = "IDTOSLIP", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)) })
	public SlipConstructoresDTOId getId() {
		return this.id;
	}

	public void setId(SlipConstructoresDTOId id) {
		this.id = id;
	}	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOSLIP", unique = true, nullable = false, insertable = false, updatable = false)
	public SlipDTO getSlipDTO() {
		return this.slipDTO;
	}

	public void setSlipDTO(SlipDTO slipDTO) {
		this.slipDTO = slipDTO;
	}

	@Column(name = "SITIOTRABAJOS", nullable = false)
	public String getSitioTrabajos() {
		return this.sitioTrabajos;
	}

	public void setSitioTrabajos(String sitioTrabajos) {
		this.sitioTrabajos = sitioTrabajos;
	}

	@Column(name = "VALORESTIMADOOBRA", nullable = false, precision = 16, scale = 4)
	public Double getValorEstimadoObra() {
		return this.valorEstimadoObra;
	}

	public void setValorEstimadoObra(Double valorEstimadoObra) {
		this.valorEstimadoObra = valorEstimadoObra;
	}

	@Column(name = "COLINDANTES", length = 240)
	public String getColindantes() {
		return this.colindantes;
	}

	public void setColindantes(String colindantes) {
		this.colindantes = colindantes;
	}

	@Column(name = "SISTEMASPREVISTOS", length = 240)
	public String getSistemasPrevistos() {
		return this.sistemasPrevistos;
	}

	public void setSistemasPrevistos(String sistemasPrevistos) {
		this.sistemasPrevistos = sistemasPrevistos;
	}

	@Column(name = "EXPERIENCIALABORAL")
	public String getExperienciaLaboral() {
		return this.experienciaLaboral;
	}

	public void setExperienciaLaboral(String experienciaLaboral) {
		this.experienciaLaboral = experienciaLaboral;
	}

	@Column(name = "PARAQUIENREALIZALAOBRA")
	public String getParaQuienRealizaLaObra() {
		return this.paraQuienRealizaLaObra;
	}

	public void setParaQuienRealizaLaObra(String paraQuienRealizaLaObra) {
		this.paraQuienRealizaLaObra = paraQuienRealizaLaObra;
	}

	@Column(name = "TIPOMAQUINARIA")
	public String getTipoMaquinaria() {
		return this.tipoMaquinaria;
	}

	public void setTipoMaquinaria(String tipoMaquinaria) {
		this.tipoMaquinaria = tipoMaquinaria;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "slipConstructoresDTO")
	public Set<SlipAnexoConstructoresDTO> getSlipAnexoConstructoresDTOs() {
		return this.slipAnexoConstructoresDTOs;
	}

	public void setSlipAnexoConstructoresDTOs(
			Set<SlipAnexoConstructoresDTO> slipAnexoConstructoresDTOs) {
		this.slipAnexoConstructoresDTOs = slipAnexoConstructoresDTOs;
	}	
	

}