package mx.com.afirme.midas.contratofacultativo.slip;
// default package



import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SlipConstructoresFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SlipConstructoresFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SlipConstructoresDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SlipConstructoresDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipConstructoresDTO entity);

	/**
	 * Delete a persistent SlipConstructoresDTO entity.
	 * 
	 * @param entity
	 *            SlipConstructoresDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipConstructoresDTO entity);

	/**
	 * Persist a previously saved SlipConstructoresDTO entity and return it or a
	 * copy of it to the sender. A copy of the SlipConstructoresDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SlipConstructoresDTO entity to update
	 * @return SlipConstructoresDTO the persisted SlipConstructoresDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipConstructoresDTO update(SlipConstructoresDTO entity);

	//public SlipConstructoresDTO findById(BigDecimal id);
	public SlipConstructoresDTO findById(SlipConstructoresDTOId id);	

	/**
	 * Find all SlipConstructoresDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipConstructoresDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipConstructoresDTO> found by query
	 */
	public List<SlipConstructoresDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SlipConstructoresDTO entities.
	 * 
	 * @return List<SlipConstructoresDTO> all SlipConstructoresDTO entities
	 */
	public List<SlipConstructoresDTO> findAll();
}