package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;


/**
 * SlipDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSLIP", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"IDTOCOTIZACION", "IDTCSUBRAMO", "NUMEROINCISO", "IDTOSECCION",
		"NUMEROSUBINCISO", "NUMEROENDOSO" }))
public class SlipDTO implements java.io.Serializable {

	public static final int INCENDIO  = 101 ; 
	public static final int TRANSPORTES_CARGA  = 201 ; 
	public static final int CASCO_AVION  = 202 ; 
	public static final int CASCO_BARCO  = 203 ; 
	public static final int EQUIPO_CONTRATISTA_MAQUINARIA_PESADA  = 310 ; 
	public static final int OBRA_CIVIL_CONSTRUCCION  = 312 ; 
	public static final int RESPONSABILIDAD_CIVIL_AVIONES  = 502 ; 
	public static final int RESPONSABILIDAD_CIVIL_BARCOS  = 503 ; 
	public static final int RESPONSABILIDAD_CIVIL_CONSEJEROS_FUNCIONARIOS  = 506 ;
	public static final int RESPONSABILIDAD_CIVIL_GENERAL  = 501 ;	
	// Fields
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private BigDecimal idToSlip;
	private BigDecimal idToCotizacion;
	private BigDecimal idTcSubRamo;
	private BigDecimal idTmLineaSoporteReaseguro;
	private BigDecimal idToSoporteReaseguro;
	private BigDecimal numeroInciso;
	private BigDecimal idToSeccion;
	private BigDecimal numeroSubInciso;
	private String tipoDistribucion;
	private String siniestrabilidad;
	private String informacionAdicional;
	private String tipoSlip;
	private BigDecimal estatus;
	private BigDecimal estatusCotizacion;
    private List<SlipAnexoDTO> slipDocumentosAnexosDTO = new ArrayList<SlipAnexoDTO>(0);
    private Integer numeroEndoso;
    private Integer tipoEndoso;


	// Constructors

	/** default constructor */
	public SlipDTO() {
	}

	// Property accessors
	@Id
    @SequenceGenerator(name = "IDTOSLIP_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOSLIP_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOSLIP_SEQ_GENERADOR")	
	@Column(name = "IDTOSLIP", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSlip() {
		return this.idToSlip;
	}

	public void setIdToSlip(BigDecimal idToSlip) {
		this.idToSlip = idToSlip;
	}

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubRamo() {
		return this.idTcSubRamo;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	@Column(name = "IDTMLINEASOPORTEREASEGURO", precision = 22, scale = 0)
	public BigDecimal getIdTmLineaSoporteReaseguro() {
		return this.idTmLineaSoporteReaseguro;
	}

	public void setIdTmLineaSoporteReaseguro(
			BigDecimal idTmLineaSoporteReaseguro) {
		this.idTmLineaSoporteReaseguro = idTmLineaSoporteReaseguro;
	}

	@Column(name = "IDTOSOPORTEREASEGURO", precision = 22, scale = 0)
	public BigDecimal getIdToSoporteReaseguro() {
		return this.idToSoporteReaseguro;
	}

	public void setIdToSoporteReaseguro(BigDecimal idToSoporteReaseguro) {
		this.idToSoporteReaseguro = idToSoporteReaseguro;
	}

	@Column(name = "NUMEROINCISO", precision = 22, scale = 0)
	public BigDecimal getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "IDTOSECCION", precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "NUMEROSUBINCISO", precision = 22, scale = 0)
	public BigDecimal getNumeroSubInciso() {
		return this.numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	@Column(name = "TIPODISTRIBUCION", nullable = false, length = 1)
	public String getTipoDistribucion() {
		return this.tipoDistribucion;
	}

	public void setTipoDistribucion(String tipoDistribucion) {
		this.tipoDistribucion = tipoDistribucion;
	}

	@Column(name = "SINIESTRALIDAD", length = 240)
	public String getSiniestrabilidad() {
		return this.siniestrabilidad;
	}

	public void setSiniestrabilidad(String siniestralidad) {
		this.siniestrabilidad = siniestralidad;
	}

	@Column(name = "INFORMACIONADICIONAL", length = 240)
	public String getInformacionAdicional() {
		return this.informacionAdicional;
	}

	public void setInformacionAdicional(String informacionAdicional) {
		this.informacionAdicional = informacionAdicional;
	}

	@Column(name = "TIPOSLIP", nullable = false, length = 1)
	public String getTipoSlip() {
		return this.tipoSlip;
	}

	public void setTipoSlip(String tipoSlip) {
		this.tipoSlip = tipoSlip;
	}

	@Column(name = "ESTATUS", nullable = false, precision = 22, scale = 0)
	public BigDecimal getEstatus() {
		return this.estatus;
	}

	public void setEstatus(BigDecimal estatus) {
		this.estatus = estatus;
	}

	@Column(name = "ESTATUSCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getEstatusCotizacion() {
		return this.estatusCotizacion;
	}

	public void setEstatusCotizacion(BigDecimal estatusCotizacion) {
		this.estatusCotizacion = estatusCotizacion;
	}

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="slipDTO")
    public List<SlipAnexoDTO> getSlipDocumentosAnexosDTO() {
		return slipDocumentosAnexosDTO;
	}

	public void setSlipDocumentosAnexosDTO(List<SlipAnexoDTO> slipDocumentosAnexosDTO) {
		this.slipDocumentosAnexosDTO = slipDocumentosAnexosDTO;
	}

	@Column(name = "NUMEROENDOSO", nullable = true, precision = 4, scale = 0)
	//@Transient
	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	
//	@Column(name = "TIPOENDOSO", nullable = true, precision = 4, scale = 0)
	@Transient
	public Integer getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Integer tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}
	
}