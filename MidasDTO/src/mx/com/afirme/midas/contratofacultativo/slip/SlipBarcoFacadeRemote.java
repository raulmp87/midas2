package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SlipBarcoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SlipBarcoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SlipBarcoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SlipBarcoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SlipBarcoDTO entity);

	/**
	 * Delete a persistent SlipBarcoDTO entity.
	 * 
	 * @param entity
	 *            SlipBarcoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SlipBarcoDTO entity);

	/**
	 * Persist a previously saved SlipBarcoDTO entity and return it or a copy of
	 * it to the sender. A copy of the SlipBarcoDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SlipBarcoDTO entity to update
	 * @return SlipBarcoDTO the persisted SlipBarcoDTO entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SlipBarcoDTO update(SlipBarcoDTO entity);

	public SlipBarcoDTO findById(BigDecimal id);

	/**
	 * Find all SlipBarcoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SlipBarcoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SlipBarcoDTO> found by query
	 */
	public List<SlipBarcoDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all SlipBarcoDTO entities.
	 * 
	 * @return List<SlipBarcoDTO> all SlipBarcoDTO entities
	 */
	public List<SlipBarcoDTO> findAll();
}