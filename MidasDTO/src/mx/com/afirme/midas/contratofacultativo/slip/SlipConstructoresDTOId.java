package mx.com.afirme.midas.contratofacultativo.slip;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * SlipConstructoresDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class SlipConstructoresDTOId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToSlip;
	private BigDecimal numeroInciso;

	// Constructors

	/** default constructor */
	public SlipConstructoresDTOId() {
	}

	/** full constructor */
	public SlipConstructoresDTOId(BigDecimal idToSlip, BigDecimal numeroInciso) {
		this.idToSlip = idToSlip;
		this.numeroInciso = numeroInciso;
	}

	// Property accessors

	@Column(name = "IDTOSLIP", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSlip() {
		return this.idToSlip;
	}

	public void setIdToSlip(BigDecimal idToSlip) {
		this.idToSlip = idToSlip;
	}

	@Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SlipConstructoresDTOId))
			return false;
		SlipConstructoresDTOId castOther = (SlipConstructoresDTOId) other;

		return ((this.getIdToSlip() == castOther.getIdToSlip()) || (this
				.getIdToSlip() != null
				&& castOther.getIdToSlip() != null && this.getIdToSlip()
				.equals(castOther.getIdToSlip())))
				&& ((this.getNumeroInciso() == castOther.getNumeroInciso()) || (this
						.getNumeroInciso() != null
						&& castOther.getNumeroInciso() != null && this
						.getNumeroInciso().equals(castOther.getNumeroInciso())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getIdToSlip() == null ? 0 : this.getIdToSlip().hashCode());
		result = 37
				* result
				+ (getNumeroInciso() == null ? 0 : this.getNumeroInciso()
						.hashCode());
		return result;
	}

}