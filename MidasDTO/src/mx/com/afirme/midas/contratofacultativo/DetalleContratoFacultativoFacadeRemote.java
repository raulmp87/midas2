package mx.com.afirme.midas.contratofacultativo;

// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for DetalleContratoFacultativoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface DetalleContratoFacultativoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * DetalleContratoFacultativoDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            DetalleContratoFacultativoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DetalleContratoFacultativoDTO entity);

	/**
	 * Delete a persistent DetalleContratoFacultativoDTO entity.
	 * 
	 * @param entity
	 *            DetalleContratoFacultativoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DetalleContratoFacultativoDTO entity);

	/**
	 * Persist a previously saved DetalleContratoFacultativoDTO entity and
	 * return it or a copy of it to the sender. A copy of the
	 * DetalleContratoFacultativoDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            DetalleContratoFacultativoDTO entity to update
	 * @return DetalleContratoFacultativoDTO the persisted
	 *         DetalleContratoFacultativoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DetalleContratoFacultativoDTO update(
			DetalleContratoFacultativoDTO entity);

	public DetalleContratoFacultativoDTO findById(BigDecimal id);

	/**
	 * Find all DetalleContratoFacultativoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the DetalleContratoFacultativoDTO property to
	 *            query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<DetalleContratoFacultativoDTO> found by query
	 */
	public List<DetalleContratoFacultativoDTO> findByProperty(
			String propertyName, Object value, int... rowStartIdxAndCount);

	public List<DetalleContratoFacultativoDTO> findByPorcentajeComision(
			Object porcentajeComision, int... rowStartIdxAndCount);

	public List<DetalleContratoFacultativoDTO> findByDeducibleCobertura(
			Object deducibleCobertura, int... rowStartIdxAndCount);

	public List<DetalleContratoFacultativoDTO> findByPrimaTotalCobertura(
			Object primaTotalCobertura, int... rowStartIdxAndCount);

	public List<DetalleContratoFacultativoDTO> findByPrimaFacultadaCobertura(
			Object primaFacultadaCobertura, int... rowStartIdxAndCount);

	/**
	 * Find all DetalleContratoFacultativoDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<DetalleContratoFacultativoDTO> all
	 *         DetalleContratoFacultativoDTO entities
	 */
	public List<DetalleContratoFacultativoDTO> findAll(
			int... rowStartIdxAndCount);
	
	/**
	 * Find DetalleContratoFacultativoDTO entity by natural key.
	 * @param idTmContratoFacultativo
	 * @param idTcSubramo
	 * @param idToCobertura
	 * @param numeroInciso
	 * @param numeroSubInciso
	 * @param idToSeccion
	 * @return a DetalleContratoFacultativoDTO. May return null if nothing was found.
	 * @exception throws RuntimeException if more than one entity is returned.
	 */
	public DetalleContratoFacultativoDTO findByNaturalKey(BigDecimal idTmContratoFacultativo, BigDecimal idTcSubramo,
			BigDecimal idToCobertura, BigDecimal numeroInciso, BigDecimal numeroSubInciso, BigDecimal idToSeccion);

	/**
	 * Find DetalleContratoFacultativoDTO entity by natural key.
	 * The difference between this one and findByNaturalKey is that an entity must be found, if not
	 * an exception is thrown.
	 * @param idTmContratoFacultativo
	 * @param idTcSubramo
	 * @param idToCobertura
	 * @param numeroInciso
	 * @param numeroSubInciso
	 * @param idToSeccion
	 * @return a DetalleContratoFacultativoDTO.
	 * @exception throws RuntimeException if more than one entity is returned or nothing is found.
	 */
	public DetalleContratoFacultativoDTO findUniqueByNaturalKey(BigDecimal idTmContratoFacultativo, BigDecimal idTcSubramo,
			BigDecimal idToCobertura, BigDecimal numeroInciso, BigDecimal numeroSubInciso, BigDecimal idToSeccion);

	/**
	 * Clone configuration of one DetalleContratoFacultativoDTO to all the DetalleContratoFacultativoDTO entities
	 * of a given ContratoFacultativoDTO entity taking as reference the parameter sent.
	 * 
	 * @param DetalleContratoFacultativoDTO
	 * @return boolean, true if the clone process ends fine or false if it did not ended correctly
	 */
	public List<DetalleContratoFacultativoDTO> aplicarConfiguracionATodasLasCoberturas(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO);
	public boolean validarDetalleParticipacion(BigDecimal id);
	public boolean validarDetalleParticipacionPorContrato(BigDecimal id);
	
	public List<DetalleContratoFacultativoDTO> listarFiltrado(DetalleContratoFacultativoDTO entity);
}