package mx.com.afirme.midas.contratofacultativo.configuracionpagos;
// default package

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;


/**
 * ConfiguracionPagosFacultativoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOCONFIGPAGOFACULTATIVO"
    ,schema="MIDAS"
)

public class ConfiguracionPagosFacultativoDTO  implements java.io.Serializable {


    // Fields    
	private static final long serialVersionUID = -2210574689868870459L;
	private BigDecimal idToConfiguracionPagosFacultativo;
	private ContratoFacultativoDTO contratoFacultativoDTO;
	private ReaseguradorCorredorDTO reaseguradorCorredorDTO;
	private Integer formaPago;
	private Double monto;
	private Date fechaPago;


    public ConfiguracionPagosFacultativoDTO() {
    }
   
    // Property accessors
    @Id
    @SequenceGenerator(name = "IDTOCONFIGPAGOFACULTATIVO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOCONFIGPAGOFACULTATIVO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOCONFIGPAGOFACULTATIVO_SEQ_GENERADOR")
    @Column(name="IDTOCONFPAGOFACULTATIVO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdToConfiguracionPagosFacultativo() {
        return this.idToConfiguracionPagosFacultativo;
    }
    
    public void setIdToConfiguracionPagosFacultativo(BigDecimal idToConfiguracionPagosFacultativo) {
        this.idToConfiguracionPagosFacultativo = idToConfiguracionPagosFacultativo;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTMCONTRATOFACULTATIVO")
    public ContratoFacultativoDTO getContratoFacultativoDTO() {
        return this.contratoFacultativoDTO;
    }
    
    public void setContratoFacultativoDTO(ContratoFacultativoDTO contratoFacultativoDTO) {
        this.contratoFacultativoDTO = contratoFacultativoDTO;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCREASEGURADORCORREDOR")
    public ReaseguradorCorredorDTO getReaseguradorCorredorDTO() {
        return this.reaseguradorCorredorDTO;
    }
    
    public void setReaseguradorCorredorDTO(ReaseguradorCorredorDTO reaseguradorCorredorDTO) {
        this.reaseguradorCorredorDTO = reaseguradorCorredorDTO;
    }
    
    @Column(name="IDFORMAPAGO", precision=1, scale=0)
    public Integer getFormaPago() {
        return this.formaPago;
    }
    
    public void setFormaPago(Integer formaPago) {
        this.formaPago = formaPago;
    }
    
    @Column(name="MONTO", precision=18, scale=4)
    public Double getMonto() {
        return this.monto;
    }
    
    public void setMonto(Double monto) {
        this.monto = monto;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAPAGO", length=7)
    public Date getFechaPago() {
        return this.fechaPago;
    }
    
    public void setFechaPago(Date fechapago) {
        this.fechaPago = fechapago;
    }

}