package mx.com.afirme.midas.contratofacultativo.configuracionpagos;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ConfiguracionPagosFacultativoDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ConfiguracionPagosFacultativoFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ConfiguracionPagosFacultativoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ConfiguracionPagosFacultativoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ConfiguracionPagosFacultativoDTO entity);
    /**
	 Delete a persistent ConfiguracionPagosFacultativoDTO entity.
	  @param entity ConfiguracionPagosFacultativoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ConfiguracionPagosFacultativoDTO entity);
   /**
	 Persist a previously saved ConfiguracionPagosFacultativoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ConfiguracionPagosFacultativoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ConfiguracionPagosFacultativoDTO entity to update
	 @return ConfiguracionPagosFacultativoDTO the persisted ConfiguracionPagosFacultativoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ConfiguracionPagosFacultativoDTO update(ConfiguracionPagosFacultativoDTO entity);
	public ConfiguracionPagosFacultativoDTO findById( BigDecimal id);
	 /**
	 * Find all ConfiguracionPagosFacultativoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ConfiguracionPagosFacultativoDTO property to query
	  @param value the property value to match
	  	  @return List<ConfiguracionPagosFacultativoDTO> found by query
	 */
	public List<ConfiguracionPagosFacultativoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ConfiguracionPagosFacultativoDTO entities.
	  	  @return List<ConfiguracionPagosFacultativoDTO> all ConfiguracionPagosFacultativoDTO entities
	 */
	public List<ConfiguracionPagosFacultativoDTO> findAll(
		);
	
	public List<ConfiguracionPagosFacultativoDTO> listarPorReasegurador(BigDecimal idtcreaseguradorcorredor, BigDecimal idTmContratoFacultativo);
}