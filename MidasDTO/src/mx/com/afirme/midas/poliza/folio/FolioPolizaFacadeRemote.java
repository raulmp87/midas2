package mx.com.afirme.midas.poliza.folio;

// default package

import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for FolioPolizaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface FolioPolizaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved FolioPolizaDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            FolioPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(FolioPolizaDTO entity);

	/**
	 * Delete a persistent FolioPolizaDTO entity.
	 * 
	 * @param entity
	 *            FolioPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(FolioPolizaDTO entity);

	/**
	 * Persist a previously saved FolioPolizaDTO entity and return it or a copy
	 * of it to the sender. A copy of the FolioPolizaDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            FolioPolizaDTO entity to update
	 * @return FolioPolizaDTO the persisted FolioPolizaDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public FolioPolizaDTO update(FolioPolizaDTO entity);

	public FolioPolizaDTO findById(FolioPolizaId id);

	/**
	 * Find all FolioPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the FolioPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<FolioPolizaDTO> found by query
	 */
	public List<FolioPolizaDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all FolioPolizaDTO entities.
	 * 
	 * @return List<FolioPolizaDTO> all FolioPolizaDTO entities
	 */
	public List<FolioPolizaDTO> findAll();

	public Integer getSiguienteNumeroPoliza(String codigoProducto,
			String codigoTipoPoliza);
}