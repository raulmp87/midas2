package mx.com.afirme.midas.poliza.folio;
// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * FolioPolizaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOFOLIOPOL", schema = "MIDAS")
public class FolioPolizaDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private FolioPolizaId id;

	// Constructors

	/** default constructor */
	public FolioPolizaDTO() {
	}

	/** full constructor */
	public FolioPolizaDTO(FolioPolizaId id) {
		this.id = id;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "codigoProducto", column = @Column(name = "CODIGOPRODUCTO", nullable = false, length = 8)),
			@AttributeOverride(name = "codigoTipoPoliza", column = @Column(name = "CODIGOTIPOPOLIZA", nullable = false, length = 8)),
			@AttributeOverride(name = "numeroPoliza", column = @Column(name = "NUMEROPOLIZA", nullable = false, precision = 8, scale = 0)) })
	public FolioPolizaId getId() {
		return this.id;
	}

	public void setId(FolioPolizaId id) {
		this.id = id;
	}

}