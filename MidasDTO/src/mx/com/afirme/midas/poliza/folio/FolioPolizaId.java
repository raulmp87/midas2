package mx.com.afirme.midas.poliza.folio;
// default package

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * FolioPolizaDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class FolioPolizaId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codigoProducto;
	private String codigoTipoPoliza;
	private Integer numeroPoliza;

	// Constructors

	/** default constructor */
	public FolioPolizaId() {
	}

	/** full constructor */
	public FolioPolizaId(String codigoProducto, String codigoTipoPoliza,
			Integer numeroPoliza) {
		this.codigoProducto = codigoProducto;
		this.codigoTipoPoliza = codigoTipoPoliza;
		this.numeroPoliza = numeroPoliza;
	}

	// Property accessors

	@Column(name = "CODIGOPRODUCTO", nullable = false, length = 8)
	public String getCodigoProducto() {
		return this.codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	@Column(name = "CODIGOTIPOPOLIZA", nullable = false, length = 8)
	public String getCodigoTipoPoliza() {
		return this.codigoTipoPoliza;
	}

	public void setCodigoTipoPoliza(String codigoTipoPoliza) {
		this.codigoTipoPoliza = codigoTipoPoliza;
	}

	@Column(name = "NUMEROPOLIZA", nullable = false, precision = 8, scale = 0)
	public Integer getNumeroPoliza() {
		return this.numeroPoliza;
	}

	public void setNumeroPoliza(Integer numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof FolioPolizaId))
			return false;
		FolioPolizaId castOther = (FolioPolizaId) other;

		return ((this.getCodigoProducto() == castOther.getCodigoProducto()) || (this
				.getCodigoProducto() != null
				&& castOther.getCodigoProducto() != null && this
				.getCodigoProducto().equals(castOther.getCodigoProducto())))
				&& ((this.getCodigoTipoPoliza() == castOther
						.getCodigoTipoPoliza()) || (this.getCodigoTipoPoliza() != null
						&& castOther.getCodigoTipoPoliza() != null && this
						.getCodigoTipoPoliza().equals(
								castOther.getCodigoTipoPoliza())))
				&& ((this.getNumeroPoliza() == castOther.getNumeroPoliza()) || (this
						.getNumeroPoliza() != null
						&& castOther.getNumeroPoliza() != null && this
						.getNumeroPoliza().equals(castOther.getNumeroPoliza())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getCodigoProducto() == null ? 0 : this.getCodigoProducto()
						.hashCode());
		result = 37
				* result
				+ (getCodigoTipoPoliza() == null ? 0 : this
						.getCodigoTipoPoliza().hashCode());
		result = 37
				* result
				+ (getNumeroPoliza() == null ? 0 : this.getNumeroPoliza()
						.hashCode());
		return result;
	}

}