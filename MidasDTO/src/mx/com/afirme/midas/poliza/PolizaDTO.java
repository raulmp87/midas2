package mx.com.afirme.midas.poliza;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.PaginadoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.agrupacion.AgrupacionPolizaDTO;
import mx.com.afirme.midas.poliza.inciso.IncisoPolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoValidacion;
import mx.com.afirme.midas2.dto.IdDynamicRow;
import mx.com.afirme.midas2.dto.poliza.VistaConsultaPolizasDanios;
import mx.com.afirme.midas2.dto.poliza.VistaconsultaPolizasAutos;

/**
 * PolizaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOPOLIZA", schema = "MIDAS")
public class PolizaDTO extends PaginadoDTO implements Entidad {
	
	//En caso de agregar mas atributos al estatus se tiene que agregar al final. no se puede cambiar el orden de los atributos.
	public enum Status{ Cancelada, Vigente    };
	
	private static final long serialVersionUID = 1L;
	private BigDecimal idToPoliza;
	private CotizacionDTO cotizacionDTO;
	private String codigoProducto;
	private String codigoTipoPoliza;
	private Integer numeroPoliza;
	private Integer numeroRenovacion;
	private String numeroRenovacionStr;
	private String codigoProductoAsoc;
	private String codigoTipoPolizaAsoc;
	private Integer numeroPolizaAsoc;
	private Integer numeroRenovacionAsoc;
	private BigDecimal idToSolicitud;
	private Double porcentajeBonifComision;
	private Short claveEstatus;
	private Date fechaCreacion;
	private String codigoUsuarioCreacion;
	private Date fechaModificacion;
	private String codigoUsuarioModificacion;
	private Double tipoCambio;
	private List<IncisoPolizaDTO> incisoPolizaDTOs = new ArrayList<IncisoPolizaDTO>(1);
	private List<EndosoDTO> endosoDTOs = new ArrayList<EndosoDTO>(1);
	private List<AgrupacionPolizaDTO> agrupacionPolizaDTOs = new ArrayList<AgrupacionPolizaDTO>(1);
	private Double primaNeta;
	private Double pctPagoFraccionado;
	private String folioPoliza;
	private String claveSeleccion;
	private String nombreAsegurado;
	private String nombreProducto;
	private String esFacultativa;
	private String conSiniestro;
	private String numeroSiniestro;
	private BigDecimal totalSiniestro;
	private Double montoReclamado;
	private String statusRenovacion;
	private String numeroSerie;
	private String codigoAgente;
	private Integer idCentroEmisor;
	private BigDecimal numeroPolizaSeycos;	
	private String clavePolizaSeycos;
	
	private String numeroPolizaFormateada;

	//Transient busqueda
	private Date fechaCreacionHasta;
	private Long idToNegProducto;
	private BigDecimal idToNegTipoPoliza;
	private BigDecimal idToNegSeccion;
	private Integer numeroIncisos;
	private Integer limiteInferiorIncisos;
	private Integer limiteSuperiorIncisos;
	private String descripcionTipo; //Individual o Flotilla
	private BigDecimal idToOrdenRenovacion;
	
	private BigDecimal idAjustadorDefault;
	private String usuarioSolAutAsigAjustador;
	private Date fechaSolAutAsigAjustador;
	private Short ClaveAutAsigAjustador;
	private String usuarioAutAsigAjustador;
	private Date fechaAutAsigAjustador;

	private Short claveAnexa;
	
	public static final Short CLAVE_POLIZA_ANEXA = 1;

	public static final String MENSAJE_EMISON = "mensaje";
	public static final String IDPOLIZA_EMISON = "idpoliza";
	public static final String ICONO_EMISION = "icono";
	public static final String ICONO_ERROR  = "10";
	public static final String ICONO_CONFIRM  = "30";
	
	public static final Short CLAVE_POLIZA_CANCELADA = 0;
	public static final Short CLAVE_POLIZA_VIGENTE = 1;
	
	private Boolean esServicioPublico;
	
	// Constructors

	/** default constructor */
	public PolizaDTO() {
	}

	/** minimal constructor */
	public PolizaDTO(BigDecimal idToPoliza, CotizacionDTO cotizacionDTO,
			String codigoProducto, String codigoTipoPoliza,
			Integer numeroPoliza, Integer numeroRenovacion,
			String codigoProductoAsoc, String codigoTipoPolizaAsoc,
			Integer numeroPolizaAsoc, Integer numeroRenovacionAsoc,
			BigDecimal idToSolicitud, Double porcentajeBonifComision,
			Short claveEstatus, Date fechaCreacion, String codigoUsuarioCreacion) {
		this.idToPoliza = idToPoliza;
		this.cotizacionDTO = cotizacionDTO;
		this.codigoProducto = codigoProducto;
		this.codigoTipoPoliza = codigoTipoPoliza;
		this.numeroPoliza = numeroPoliza;
		this.numeroRenovacion = numeroRenovacion;
		this.codigoProductoAsoc = codigoProductoAsoc;
		this.codigoTipoPolizaAsoc = codigoTipoPolizaAsoc;
		this.numeroPolizaAsoc = numeroPolizaAsoc;
		this.numeroRenovacionAsoc = numeroRenovacionAsoc;
		this.idToSolicitud = idToSolicitud;
		this.porcentajeBonifComision = porcentajeBonifComision;
		this.claveEstatus = claveEstatus;
		this.fechaCreacion = fechaCreacion;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOPOLIZA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOPOLIZA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOPOLIZA_SEQ_GENERADOR")
	@Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)
	@IdDynamicRow
	public BigDecimal getIdToPoliza() {
		return this.idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCOTIZACION", nullable = false)
	public CotizacionDTO getCotizacionDTO() {
		return this.cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	@Column(name = "CODIGOPRODUCTO", nullable = false, length = 8)
	public String getCodigoProducto() {
		return this.codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	@Column(name = "CODIGOTIPOPOLIZA", nullable = false, length = 8)
	public String getCodigoTipoPoliza() {
		return this.codigoTipoPoliza;
	}

	public void setCodigoTipoPoliza(String codigoTipoPoliza) {
		this.codigoTipoPoliza = codigoTipoPoliza;
	}

	@Column(name = "NUMEROPOLIZA", nullable = false, precision = 10, scale = 0)
	public Integer getNumeroPoliza() {
		return this.numeroPoliza;
	}

	public void setNumeroPoliza(Integer numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Column(name = "NUMERORENOVACION", nullable = false, precision = 8, scale = 0)
	public Integer getNumeroRenovacion() {
		return this.numeroRenovacion;
	}

	public void setNumeroRenovacion(Integer numeroRenovacion) {
		this.numeroRenovacion = numeroRenovacion;
	}

	@Column(name = "CODIGOPRODUCTOASOC", nullable = false, length = 8)
	public String getCodigoProductoAsoc() {
		return this.codigoProductoAsoc;
	}

	public void setCodigoProductoAsoc(String codigoProductoAsoc) {
		this.codigoProductoAsoc = codigoProductoAsoc;
	}

	@Column(name = "CODIGOTIPOPOLIZAASOC", nullable = false, length = 8)
	public String getCodigoTipoPolizaAsoc() {
		return this.codigoTipoPolizaAsoc;
	}

	public void setCodigoTipoPolizaAsoc(String codigoTipoPolizaAsoc) {
		this.codigoTipoPolizaAsoc = codigoTipoPolizaAsoc;
	}

	@Column(name = "NUMEROPOLIZAASOC", nullable = false, precision = 8, scale = 0)
	public Integer getNumeroPolizaAsoc() {
		return this.numeroPolizaAsoc;
	}

	public void setNumeroPolizaAsoc(Integer numeroPolizaAsoc) {
		this.numeroPolizaAsoc = numeroPolizaAsoc;
	}

	@Column(name = "NUMERORENOVACIONASOC", nullable = false, precision = 8, scale = 0)
	public Integer getNumeroRenovacionAsoc() {
		return this.numeroRenovacionAsoc;
	}

	public void setNumeroRenovacionAsoc(Integer numeroRenovacionAsoc) {
		this.numeroRenovacionAsoc = numeroRenovacionAsoc;
	}

	@Column(name = "IDTOSOLICITUD", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSolicitud() {
		return this.idToSolicitud;
	}

	public void setIdToSolicitud(BigDecimal idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	@Column(name = "PORCENTAJEBONIFCOMISION", nullable = false, precision = 8, scale = 4)
	public Double getPorcentajeBonifComision() {
		return this.porcentajeBonifComision;
	}

	public void setPorcentajeBonifComision(Double porcentajeBonifComision) {
		this.porcentajeBonifComision = porcentajeBonifComision;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return this.claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	@DynamicControl(atributoMapeo="fechaCreacion",tipoControl=TipoControl.TEXTBOX,etiqueta="Fecha Emisi\u00F3n",secuencia="4",
			vistas={VistaConsultaPolizasDanios.class,VistaconsultaPolizasAutos.class},tipoValidacion = TipoValidacion.NINGUNA,
			editable=true,claveCascada="AGENTES")
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "CODIGOUSUARIOCREACION", nullable = false, length = 8)
	public String getCodigoUsuarioCreacion() {
		return this.codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAMODIFICACION", length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name = "CODIGOUSUARIOMODIFICACION", length = 8)
	public String getCodigoUsuarioModificacion() {
		return this.codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "polizaDTO")
	public List<IncisoPolizaDTO> getIncisoPolizaDTOs() {
		return this.incisoPolizaDTOs;
	}

	public void setIncisoPolizaDTOs(List<IncisoPolizaDTO> incisoPolizaDTOs) {
		this.incisoPolizaDTOs = incisoPolizaDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "polizaDTO")
	public List<EndosoDTO> getEndosoDTOs() {
		return this.endosoDTOs;
	}

	public void setEndosoDTOs(List<EndosoDTO> endosoDTOs) {
		this.endosoDTOs = endosoDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "polizaDTO")
	public List<AgrupacionPolizaDTO> getAgrupacionPolizaDTOs() {
		return this.agrupacionPolizaDTOs;
	}

	public void setAgrupacionPolizaDTOs(
			List<AgrupacionPolizaDTO> agrupacionPolizaDTOs) {
		this.agrupacionPolizaDTOs = agrupacionPolizaDTOs;
	}

	@Column(name = "TIPOCAMBIO")
	public Double getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	@Transient
	public Double getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(Double primaNeta) {
		this.primaNeta = primaNeta;
	}

	@Transient
	public Double getPctPagoFraccionado() {
		return pctPagoFraccionado;
	}

	public void setPctPagoFraccionado(Double pctPagoFraccionado) {
		this.pctPagoFraccionado = pctPagoFraccionado;
	}

	@Transient
	@DynamicControl(atributoMapeo="folioPoliza",tipoControl=TipoControl.TEXTBOX,etiqueta="P\u00F3liza",secuencia="1",
			vistas={VistaConsultaPolizasDanios.class,VistaconsultaPolizasAutos.class,javax.validation.groups.Default.class},
			tipoValidacion = TipoValidacion.NINGUNA,javaScriptOnFocus="new Mask('####-######-##', 'string').attach(this)",
			javaScriptOnKeyPress="return stopRKey(event)",editable=true)
	public String getFolioPoliza() {
		if(folioPoliza == null){
			this.folioPoliza = getNumeroPolizaFormateada();
		}
		return folioPoliza;
	}

	public void setFolioPoliza(String folioPoliza) {
		if(folioPoliza != null){
			if(folioPoliza.length() >= 2){
				if(codigoProducto == null){
					codigoProducto = folioPoliza.substring(0, 2);
				}
				if(folioPoliza.length()>=4){
					if(codigoTipoPoliza == null){
						codigoTipoPoliza = folioPoliza.substring(2, 4);
					}
					if(folioPoliza.length()>=11){
						if(numeroPoliza == null){
							try{
								numeroPoliza = new Integer(folioPoliza.substring(5, 11));
							}catch(Exception e){}
						}
					}
					if(folioPoliza.length()==14){
						if(numeroRenovacion == null){
							try{
								numeroRenovacion = new Integer(folioPoliza.substring(12, 14));
							}catch(Exception e){}
						}
					}
				}
			}
		}
		this.folioPoliza = folioPoliza;
	}

	@Transient
	public String getClaveSeleccion() {
		return claveSeleccion;
	}

	public void setClaveSeleccion(String claveSeleccion) {
		this.claveSeleccion = claveSeleccion;
	}
	
	@Transient
	public String getNumeroPolizaFormateada() {
		String numeroPoliza = "";
		if ((this.codigoProducto != null && this.codigoProducto.length() > 0)
				&& (this.codigoTipoPoliza != null && this.codigoTipoPoliza
						.length() > 0) && (this.numeroPoliza != null)
				&& (this.numeroRenovacion != null)) {
			try {
				numeroPoliza += String.format("%02d", Integer
						.parseInt(this.codigoProducto));
				numeroPoliza += String.format("%02d", Integer
						.parseInt(this.codigoTipoPoliza));
				numeroPoliza += "-";
				if(this.numeroPoliza.toString().length() <= 6){
					numeroPoliza += String.format("%06d", this.numeroPoliza);
				}else{
					numeroPoliza += String.format("%08d", this.numeroPoliza);
				}
				numeroPoliza += "-";
				numeroPoliza += String.format("%02d", this.numeroRenovacion);
			} catch (Exception e) {
				return "";
			}

		}
		return numeroPoliza;
	}
	public void setNumeroPolizaFormateada(String numeroPolizaFormateada) {
		this.numeroPolizaFormateada = numeroPolizaFormateada;
	}

	@Transient
	public String getEstatus() {
		String estatus = "";
		if(this.claveEstatus != null && this.claveEstatus.compareTo((short)0) == 0) {
			estatus = "CANCELADA";
		} else {
			estatus = "VIGENTE";
		}
		return estatus;
	}
	
	@Transient
	@DynamicControl(atributoMapeo="nombreAsegurado",tipoControl=TipoControl.TEXTBOX,etiqueta="Asegurado",secuencia="2",
			vistas={VistaConsultaPolizasDanios.class,javax.validation.groups.Default.class},tipoValidacion = TipoValidacion.NINGUNA,editable=true)
	public String getNombreAsegurado() {
		if(nombreAsegurado== null || nombreAsegurado.trim().equals("") && cotizacionDTO != null){
			nombreAsegurado = cotizacionDTO.getNombreAsegurado();
		}
		return nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		if(cotizacionDTO == null){
			setCotizacionDTO(new CotizacionDTO());
			cotizacionDTO.setNombreAsegurado(nombreAsegurado);
		}
		else if(cotizacionDTO.getNombreAsegurado() == null){
			cotizacionDTO.setNombreAsegurado(nombreAsegurado);
		}
		this.nombreAsegurado = nombreAsegurado;
	}
	@Transient
	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	@Transient	
	public String getEsFacultativa() {
		return esFacultativa;
	}

	public void setEsFacultativa(String esFacultativa) {
		this.esFacultativa = esFacultativa;
	}
	
	@Transient	
	public String getConSiniestro() {
		return conSiniestro;
	}

	public void setConSiniestro(String conSiniestro) {
		this.conSiniestro = conSiniestro;
	}
	
	@Transient	
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	@Transient	
	public Double getMontoReclamado() {
		return montoReclamado;
	}

	public void setMontoReclamado(Double montoReclamado) {
		this.montoReclamado = montoReclamado;
	}

	@Transient	
	public String getStatusRenovacion() {
		return statusRenovacion;
	}

	public void setStatusRenovacion(String statusRenovacion) {
		this.statusRenovacion = statusRenovacion;
	}

	@Transient
	@DynamicControl(atributoMapeo="numeroSerie",tipoControl=TipoControl.TEXTBOX,etiqueta="N\u00FAmero Serie",secuencia="2",
			vistas=VistaconsultaPolizasAutos.class,tipoValidacion = TipoValidacion.NINGUNA)
	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	@Transient
	@DynamicControl(atributoMapeo="codigoAgente",tipoControl=TipoControl.SELECT,etiqueta="Agente",secuencia="3",
			vistas={VistaConsultaPolizasDanios.class,VistaconsultaPolizasAutos.class},tipoValidacion = TipoValidacion.NINGUNA,
			editable=true,className="mx.com.afirme.midas.interfaz.agente.AgenteFacadeRemote")
	public String getCodigoAgente() {
		return codigoAgente;
	}

	public void setCodigoAgente(String codigoAgente) {
		this.codigoAgente = codigoAgente;
	}
	
	
	@Transient
	public Date getFechaCreacionHasta() {
		return fechaCreacionHasta;
	}

	public void setFechaCreacionHasta(Date fechaCreacionHasta) {
		this.fechaCreacionHasta = fechaCreacionHasta;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idToPoliza;
	}

	@Override
	public String getValue() {
		return this.folioPoliza;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void setIdToNegProducto(Long idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	@Transient
	public Long getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setIdToNegTipoPoliza(BigDecimal idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}

	@Transient
	public BigDecimal getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}

	public void setIdToNegSeccion(BigDecimal idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	@Transient
	public BigDecimal getIdToNegSeccion() {
		return idToNegSeccion;
	}
	
	
	@Transient
	public Integer getNumeroIncisos() {
		return numeroIncisos;
	}

	public void setNumeroIncisos(Integer numeroIncisos) {
		this.numeroIncisos = numeroIncisos;
	}

	@Transient
	public Integer getLimiteInferiorIncisos() {
		return limiteInferiorIncisos;
	}

	public void setLimiteInferiorIncisos(Integer limiteInferiorIncisos) {
		this.limiteInferiorIncisos = limiteInferiorIncisos;
	}

	@Transient
	public Integer getLimiteSuperiorIncisos() {
		return limiteSuperiorIncisos;
	}

	public void setLimiteSuperiorIncisos(Integer limiteSuperiorIncisos) {
		this.limiteSuperiorIncisos = limiteSuperiorIncisos;
	}
		
	@Transient
	public String getDescripcionTipo() {
		return descripcionTipo;
	}

	public void setDescripcionTipo(String descripcionTipo) {
		this.descripcionTipo = descripcionTipo;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("IdToPoliza: " + this.getIdToPoliza() + ", ");
		sb.append("NumeroPolizaFormateada: " + this.getNumeroPolizaFormateada() + ", ");
		sb.append("CotizacionDTO: " + this.getCotizacionDTO() + "");
		sb.append("]");
		
		return sb.toString();
	}

	/**
	 * @return the idCentroEmisor
	 */
	@Column(name="IDTCCENTROEMISOR",nullable = true, precision = 8, scale = 0)
	public Integer getIdCentroEmisor() {
		return idCentroEmisor;
	}

	/**
	 * @param idCentroEmisor the idCentroEmisor to set
	 */
	public void setIdCentroEmisor(Integer idCentroEmisor) {
		this.idCentroEmisor = idCentroEmisor;
	}
	
	@Column(name = "NUMPOLIZASEYCOS", nullable = true, precision = 22, scale = 0)
	public BigDecimal getNumeroPolizaSeycos() {
		return numeroPolizaSeycos;
	}

	public void setNumeroPolizaSeycos(BigDecimal numeroPolizaSeycos) {
		this.numeroPolizaSeycos = numeroPolizaSeycos;
	}

	@Column(name = "CVEPOLIZASEYCOS", nullable = true, length = 20)
	public String getClavePolizaSeycos() {
		return clavePolizaSeycos;
	}

	public void setClavePolizaSeycos(String clavePolizaSeycos) {
		this.clavePolizaSeycos = clavePolizaSeycos;
	}

	public void setTotalSiniestro(BigDecimal totalSiniestro) {
		this.totalSiniestro = totalSiniestro;
	}

	@Transient
	public BigDecimal getTotalSiniestro() {
		return totalSiniestro;
	}

	public void setNumeroRenovacionStr(String numeroRenovacionStr) {
		this.numeroRenovacionStr = numeroRenovacionStr;
	}

	@Transient
	public String getNumeroRenovacionStr() {
		return numeroRenovacionStr;
	}
	
	@Transient
	public String numeroPolizaMidasSeycos(){
		String numeroPolizaSeycos = "";
		try{
			if(getClavePolizaSeycos() != null && !getClavePolizaSeycos().isEmpty()){
				numeroPolizaSeycos = getClavePolizaSeycos();
			}else{
				String numeroPoliza = "";
				if ((this.codigoProducto != null && this.codigoProducto.length() > 0)
						&& (this.codigoTipoPoliza != null && this.codigoTipoPoliza
								.length() > 0) && (this.numeroPoliza != null)
						&& (this.numeroRenovacion != null)) {
					try {
						numeroPoliza = String.format("%02d", Integer.parseInt(this.codigoProducto));
						numeroPoliza += String.format("%02d", Integer.parseInt(this.codigoTipoPoliza));
						numeroPoliza += String.format("%08d", this.numeroPoliza);
					} catch (Exception e) {
					}
				}
				numeroPolizaSeycos = String.format("%03d", getIdCentroEmisor())+"-"+numeroPoliza+"-"+String.format("%02d", getNumeroRenovacion());
			}
		}catch(Exception e){
		}
		return numeroPolizaSeycos;
	}

	public void setIdToOrdenRenovacion(BigDecimal idToOrdenRenovacion) {
		this.idToOrdenRenovacion = idToOrdenRenovacion;
	}

	@Transient
	public BigDecimal getIdToOrdenRenovacion() {
		return idToOrdenRenovacion;
	}

	@Column(name = "IDAJUSTADOR")
	public BigDecimal getIdAjustadorDefault() {
		return idAjustadorDefault;
	}

	public void setIdAjustadorDefault(BigDecimal idAjustadorDefault) {
		this.idAjustadorDefault = idAjustadorDefault;
	}

	@Column(name = "USUARIOSOLAUTASIGAJUSTADOR")
	public String getUsuarioSolAutAsigAjustador() {
		return usuarioSolAutAsigAjustador;
	}

	public void setUsuarioSolAutAsigAjustador(String usuarioSolAutAsigAjustador) {
		this.usuarioSolAutAsigAjustador = usuarioSolAutAsigAjustador;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHASOLAUTASIGAJUSTADOR", length = 7)
	public Date getFechaSolAutAsigAjustador() {
		return fechaSolAutAsigAjustador;
	}

	public void setFechaSolAutAsigAjustador(Date fechaSolAutAsigAjustador) {
		this.fechaSolAutAsigAjustador = fechaSolAutAsigAjustador;
	}

	@Column(name = "CLAVEAUTASIGAJUSTADOR")
	public Short getClaveAutAsigAjustador() {
		return ClaveAutAsigAjustador == null ? 0 : ClaveAutAsigAjustador;
	}

	public void setClaveAutAsigAjustador(Short claveAutAsigAjustador) {
		ClaveAutAsigAjustador = claveAutAsigAjustador;
	}

	@Column(name = "USUARIOAUTASIGAJUSTADOR")
	public String getUsuarioAutAsigAjustador() {
		return usuarioAutAsigAjustador;
	}

	public void setUsuarioAutAsigAjustador(String usuarioAutAsigAjustador) {
		this.usuarioAutAsigAjustador = usuarioAutAsigAjustador;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAAUTASIGAJUSTADOR", length = 7)
	public Date getFechaAutAsigAjustador() {
		return fechaAutAsigAjustador;
	}

	public void setFechaAutAsigAjustador(Date fechaAutAsigAjustador) {
		this.fechaAutAsigAjustador = fechaAutAsigAjustador;
	}

	public void setClaveAnexa(Short claveAnexa) {
		this.claveAnexa = claveAnexa;
	}

	@Column(name = "CLAVEANEXA", nullable = false, precision = 4, scale = 0)
	public Short getClaveAnexa() {
		return claveAnexa;
	}
	
	@Transient
	public Boolean getEsServicioPublico() {
		return esServicioPublico;
	}

	public void setEsServicioPublico(Boolean esServicioPublico) {
		this.esServicioPublico = esServicioPublico;
	}

}
