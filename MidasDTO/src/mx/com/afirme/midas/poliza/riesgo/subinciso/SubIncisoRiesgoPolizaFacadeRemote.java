package mx.com.afirme.midas.poliza.riesgo.subinciso;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SubIncisoRiesgoPolizaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SubIncisoRiesgoPolizaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SubIncisoRiesgoPolizaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SubIncisoRiesgoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubIncisoRiesgoPolizaDTO entity);

	/**
	 * Delete a persistent SubIncisoRiesgoPolizaDTO entity.
	 * 
	 * @param entity
	 *            SubIncisoRiesgoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubIncisoRiesgoPolizaDTO entity);

	/**
	 * Persist a previously saved SubIncisoRiesgoPolizaDTO entity and return it
	 * or a copy of it to the sender. A copy of the SubIncisoRiesgoPolizaDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SubIncisoRiesgoPolizaDTO entity to update
	 * @return SubIncisoRiesgoPolizaDTO the persisted SubIncisoRiesgoPolizaDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubIncisoRiesgoPolizaDTO update(SubIncisoRiesgoPolizaDTO entity);

	public SubIncisoRiesgoPolizaDTO findById(SubIncisoRiesgoPolizaId id);

	/**
	 * Find all SubIncisoRiesgoPolizaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SubIncisoRiesgoPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubIncisoRiesgoPolizaDTO> found by query
	 */
	public List<SubIncisoRiesgoPolizaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SubIncisoRiesgoPolizaDTO entities.
	 * 
	 * @return List<SubIncisoRiesgoPolizaDTO> all SubIncisoRiesgoPolizaDTO
	 *         entities
	 */
	public List<SubIncisoRiesgoPolizaDTO> findAll();
	
	public void insertSubIncisoRiesgoPolizaPorCotizacion(BigDecimal idToCotizacion, BigDecimal idToPoliza);
}