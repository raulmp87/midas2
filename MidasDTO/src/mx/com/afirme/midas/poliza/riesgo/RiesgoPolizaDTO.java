package mx.com.afirme.midas.poliza.riesgo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import mx.com.afirme.midas.endoso.riesgo.RiesgoEndosoDTO;
import mx.com.afirme.midas.poliza.cobertura.CoberturaPolizaDTO;
import mx.com.afirme.midas.poliza.riesgo.subinciso.SubIncisoRiesgoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;

/**
 * RiesgoPolizaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TORIESGOPOL", schema = "MIDAS")
public class RiesgoPolizaDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private RiesgoPolizaId id;
	private RiesgoCoberturaDTO riesgoCoberturaDTO;
	private CoberturaPolizaDTO coberturaPolizaDTO;
	private BigDecimal idTcSubRamo;
	private Double valorSumaAsegurada;
	private Double valorCoaseguro;
	private Double valorDeducible;
	private Double valorCuota;
	private Double valorPrimaNeta;
	private Double valorRecargoPagoFrac;
	private Double valorDerechos;
	private Double valorBonifComision;
	private Double valorBonifComRecPagoFrac;
	private Double valorIVA;
	private Double valorPrimaTotal;
	private Double porcentajeComision;
	private Double valorComision;
	private Double valorComisionFinal;
	private Double valorComisionRecPagoFrac;
	private Double valorComFinalRecPagoFrac;
	private Short claveEstatus;
	private List<SubIncisoRiesgoPolizaDTO> subIncisoRiesgoPolizaDTOs = new ArrayList<SubIncisoRiesgoPolizaDTO>();
	private List<RiesgoEndosoDTO> riesgoEndosoDTOs = new ArrayList<RiesgoEndosoDTO>();

	// Constructors

	/** default constructor */
	public RiesgoPolizaDTO() {
	}

	/** minimal constructor */
	public RiesgoPolizaDTO(RiesgoPolizaId id,
			RiesgoCoberturaDTO riesgoCoberturaDTO,
			CoberturaPolizaDTO coberturaPolizaDTO, BigDecimal idTcSubRamo,
			Double valorSumaAsegurada, Double valorCoaseguro,
			Double valorDeducible, Double valorCuota, Double valorPrimaNeta,
			Double valorRecargoPagoFrac, Double valorDerechos,
			Double valorBonifComision, Double valorBonifComRecPagoFrac,
			Double valorIVA, Double valorPrimaTotal, Double porcentajeComision,
			Double valorComision, Double valorComisionFinal,
			Double valorComisionRecPagoFrac, Double valorComFinalRecPagoFrac,
			Short claveEstatus) {
		this.id = id;
		this.riesgoCoberturaDTO = riesgoCoberturaDTO;
		this.coberturaPolizaDTO = coberturaPolizaDTO;
		this.idTcSubRamo = idTcSubRamo;
		this.valorSumaAsegurada = valorSumaAsegurada;
		this.valorCoaseguro = valorCoaseguro;
		this.valorDeducible = valorDeducible;
		this.valorCuota = valorCuota;
		this.valorPrimaNeta = valorPrimaNeta;
		this.valorRecargoPagoFrac = valorRecargoPagoFrac;
		this.valorDerechos = valorDerechos;
		this.valorBonifComision = valorBonifComision;
		this.valorBonifComRecPagoFrac = valorBonifComRecPagoFrac;
		this.valorIVA = valorIVA;
		this.valorPrimaTotal = valorPrimaTotal;
		this.porcentajeComision = porcentajeComision;
		this.valorComision = valorComision;
		this.valorComisionFinal = valorComisionFinal;
		this.valorComisionRecPagoFrac = valorComisionRecPagoFrac;
		this.valorComFinalRecPagoFrac = valorComFinalRecPagoFrac;
		this.claveEstatus = claveEstatus;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)) })
	public RiesgoPolizaId getId() {
		return this.id;
	}

	public void setId(RiesgoPolizaId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTORIESGO", referencedColumnName = "IDTORIESGO", nullable = false, insertable = false, updatable = false) })
	public RiesgoCoberturaDTO getRiesgoCoberturaDTO() {
		return this.riesgoCoberturaDTO;
	}

	public void setRiesgoCoberturaDTO(RiesgoCoberturaDTO riesgoCoberturaDTO) {
		this.riesgoCoberturaDTO = riesgoCoberturaDTO;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOPOLIZA", referencedColumnName = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false) })
	public CoberturaPolizaDTO getCoberturaPolizaDTO() {
		return this.coberturaPolizaDTO;
	}

	public void setCoberturaPolizaDTO(CoberturaPolizaDTO coberturaPolizaDTO) {
		this.coberturaPolizaDTO = coberturaPolizaDTO;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubRamo() {
		return this.idTcSubRamo;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	@Column(name = "VALORSUMAASEGURADA", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Column(name = "VALORCOASEGURO", nullable = false, precision = 16)
	public Double getValorCoaseguro() {
		return this.valorCoaseguro;
	}

	public void setValorCoaseguro(Double valorCoaseguro) {
		this.valorCoaseguro = valorCoaseguro;
	}

	@Column(name = "VALORDEDUCIBLE", nullable = false, precision = 16)
	public Double getValorDeducible() {
		return this.valorDeducible;
	}

	public void setValorDeducible(Double valorDeducible) {
		this.valorDeducible = valorDeducible;
	}

	@Column(name = "VALORCUOTA", nullable = false, precision = 16, scale = 10)
	public Double getValorCuota() {
		return this.valorCuota;
	}

	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}

	@Column(name = "VALORRECARGOPAGOFRAC", nullable = false, precision = 16)
	public Double getValorRecargoPagoFrac() {
		return this.valorRecargoPagoFrac;
	}

	public void setValorRecargoPagoFrac(Double valorRecargoPagoFrac) {
		this.valorRecargoPagoFrac = valorRecargoPagoFrac;
	}

	@Column(name = "VALORDERECHOS", nullable = false, precision = 16)
	public Double getValorDerechos() {
		return this.valorDerechos;
	}

	public void setValorDerechos(Double valorDerechos) {
		this.valorDerechos = valorDerechos;
	}

	@Column(name = "VALORBONIFCOMISION", nullable = false, precision = 16)
	public Double getValorBonifComision() {
		return this.valorBonifComision;
	}

	public void setValorBonifComision(Double valorBonifComision) {
		this.valorBonifComision = valorBonifComision;
	}

	@Column(name = "VALORBONIFCOMRECPAGOFRAC", nullable = false, precision = 16)
	public Double getValorBonifComRecPagoFrac() {
		return this.valorBonifComRecPagoFrac;
	}

	public void setValorBonifComRecPagoFrac(Double valorBonifComRecPagoFrac) {
		this.valorBonifComRecPagoFrac = valorBonifComRecPagoFrac;
	}

	@Column(name = "VALORIVA", nullable = false, precision = 16)
	public Double getValorIVA() {
		return this.valorIVA;
	}

	public void setValorIVA(Double valorIVA) {
		this.valorIVA = valorIVA;
	}

	@Column(name = "VALORPRIMATOTAL", nullable = false, precision = 16)
	public Double getValorPrimaTotal() {
		return this.valorPrimaTotal;
	}

	public void setValorPrimaTotal(Double valorPrimaTotal) {
		this.valorPrimaTotal = valorPrimaTotal;
	}

	@Column(name = "PORCENTAJECOMISION", nullable = false, precision = 8, scale = 4)
	public Double getPorcentajeComision() {
		return this.porcentajeComision;
	}

	public void setPorcentajeComision(Double porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}

	@Column(name = "VALORCOMISION", nullable = false, precision = 16)
	public Double getValorComision() {
		return this.valorComision;
	}

	public void setValorComision(Double valorComision) {
		this.valorComision = valorComision;
	}

	@Column(name = "VALORCOMISIONFINAL", nullable = false, precision = 16)
	public Double getValorComisionFinal() {
		return this.valorComisionFinal;
	}

	public void setValorComisionFinal(Double valorComisionFinal) {
		this.valorComisionFinal = valorComisionFinal;
	}

	@Column(name = "VALORCOMISIONRECPAGOFRAC", nullable = false, precision = 16)
	public Double getValorComisionRecPagoFrac() {
		return this.valorComisionRecPagoFrac;
	}

	public void setValorComisionRecPagoFrac(Double valorComisionRecPagoFrac) {
		this.valorComisionRecPagoFrac = valorComisionRecPagoFrac;
	}

	@Column(name = "VALORCOMFINALRECPAGOFRAC", nullable = false, precision = 16)
	public Double getValorComFinalRecPagoFrac() {
		return this.valorComFinalRecPagoFrac;
	}

	public void setValorComFinalRecPagoFrac(Double valorComFinalRecPagoFrac) {
		this.valorComFinalRecPagoFrac = valorComFinalRecPagoFrac;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return this.claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "riesgoPolizaDTO")
	public List<SubIncisoRiesgoPolizaDTO> getSubIncisoRiesgoPolizaDTOs() {
		return this.subIncisoRiesgoPolizaDTOs;
	}

	public void setSubIncisoRiesgoPolizaDTOs(List<SubIncisoRiesgoPolizaDTO> subIncisoRiesgoPolizaDTOs) {
		this.subIncisoRiesgoPolizaDTOs = subIncisoRiesgoPolizaDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "riesgoPolizaDTO")
	public List<RiesgoEndosoDTO> getRiesgoEndosoDTOs() {
		return this.riesgoEndosoDTOs;
	}

	public void setRiesgoEndosoDTOs(List<RiesgoEndosoDTO> riesgoEndosoDTOs) {
		this.riesgoEndosoDTOs = riesgoEndosoDTOs;
	}
}