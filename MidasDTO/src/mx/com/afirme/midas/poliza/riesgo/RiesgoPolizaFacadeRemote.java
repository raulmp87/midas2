package mx.com.afirme.midas.poliza.riesgo;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for RiesgoPolizaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface RiesgoPolizaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved RiesgoPolizaDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            RiesgoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public RiesgoPolizaDTO save(RiesgoPolizaDTO entity);

	/**
	 * Delete a persistent RiesgoPolizaDTO entity.
	 * 
	 * @param entity
	 *            RiesgoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoPolizaDTO entity);

	/**
	 * Persist a previously saved RiesgoPolizaDTO entity and return it or a copy
	 * of it to the sender. A copy of the RiesgoPolizaDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            RiesgoPolizaDTO entity to update
	 * @return RiesgoPolizaDTO the persisted RiesgoPolizaDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoPolizaDTO update(RiesgoPolizaDTO entity);

	public RiesgoPolizaDTO findById(RiesgoPolizaId id);

	/**
	 * Find all RiesgoPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoPolizaDTO> found by query
	 */
	public List<RiesgoPolizaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all RiesgoPolizaDTO entities.
	 * 
	 * @return List<RiesgoPolizaDTO> all RiesgoPolizaDTO entities
	 */
	public List<RiesgoPolizaDTO> findAll();
	
	public List<RiesgoPolizaDTO> listarFiltrado(RiesgoPolizaId id);
	
	public void insertRiesgoPolizaPorCotizacion(BigDecimal idToCotizacion, BigDecimal idToPoliza);
}