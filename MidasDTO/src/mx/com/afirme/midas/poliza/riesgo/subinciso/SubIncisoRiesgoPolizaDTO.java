package mx.com.afirme.midas.poliza.riesgo.subinciso;

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.poliza.riesgo.RiesgoPolizaDTO;

/**
 * SubIncisoRiesgoPolizaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSUBINCISORGOPOL", schema = "MIDAS")
public class SubIncisoRiesgoPolizaDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private SubIncisoRiesgoPolizaId id;
	private RiesgoPolizaDTO riesgoPolizaDTO;
	private BigDecimal idTcSubRamo;
	private Double valorSumaAsegurada;
	private Double valorPrimaNeta;

	// Constructors

	/** default constructor */
	public SubIncisoRiesgoPolizaDTO() {
	}

	/** full constructor */
	public SubIncisoRiesgoPolizaDTO(SubIncisoRiesgoPolizaId id,
			RiesgoPolizaDTO riesgoPolizaDTO, BigDecimal idTcSubRamo,
			Double valorSumaAsegurada, Double valorPrimaNeta) {
		this.id = id;
		this.riesgoPolizaDTO = riesgoPolizaDTO;
		this.idTcSubRamo = idTcSubRamo;
		this.valorSumaAsegurada = valorSumaAsegurada;
		this.valorPrimaNeta = valorPrimaNeta;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroSubInciso", column = @Column(name = "NUMEROSUBINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)) })
	public SubIncisoRiesgoPolizaId getId() {
		return this.id;
	}

	public void setId(SubIncisoRiesgoPolizaId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOPOLIZA", referencedColumnName = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTORIESGO", referencedColumnName = "IDTORIESGO", nullable = false, insertable = false, updatable = false) })
	public RiesgoPolizaDTO getRiesgoPolizaDTO() {
		return this.riesgoPolizaDTO;
	}

	public void setRiesgoPolizaDTO(RiesgoPolizaDTO riesgoPolizaDTO) {
		this.riesgoPolizaDTO = riesgoPolizaDTO;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubRamo() {
		return this.idTcSubRamo;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	@Column(name = "VALORSUMAASEGURADA", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}
}