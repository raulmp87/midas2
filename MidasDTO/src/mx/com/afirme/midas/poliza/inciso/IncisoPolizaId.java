package mx.com.afirme.midas.poliza.inciso;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * IncisoPolizaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class IncisoPolizaId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal idToPoliza;
	private BigDecimal numeroInciso;

	// Constructors

	/** default constructor */
	public IncisoPolizaId() {
	}

	/** full constructor */
	public IncisoPolizaId(BigDecimal idToPoliza, BigDecimal numeroInciso) {
		this.idToPoliza = idToPoliza;
		this.numeroInciso = numeroInciso;
	}

	// Property accessors

	@Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToPoliza() {
		return this.idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof IncisoPolizaId))
			return false;
		IncisoPolizaId castOther = (IncisoPolizaId) other;

		return ((this.getIdToPoliza() == castOther.getIdToPoliza()) || (this
				.getIdToPoliza() != null
				&& castOther.getIdToPoliza() != null && this.getIdToPoliza()
				.equals(castOther.getIdToPoliza())))
				&& ((this.getNumeroInciso() == castOther.getNumeroInciso()) || (this
						.getNumeroInciso() != null
						&& castOther.getNumeroInciso() != null && this
						.getNumeroInciso().equals(castOther.getNumeroInciso())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToPoliza() == null ? 0 : this.getIdToPoliza()
						.hashCode());
		result = 37
				* result
				+ (getNumeroInciso() == null ? 0 : this.getNumeroInciso()
						.hashCode());
		return result;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("NumeroInciso: " + this.getNumeroInciso() + ", ");
		sb.append("IdToPoliza: " + this.getIdToPoliza());
		sb.append("]");
		
		return sb.toString();
	}
}