package mx.com.afirme.midas.poliza.inciso;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for IncisoPolizaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface IncisoPolizaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved IncisoPolizaDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            IncisoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public IncisoPolizaDTO save(IncisoPolizaDTO entity);

	/**
	 * Delete a persistent IncisoPolizaDTO entity.
	 * 
	 * @param entity
	 *            IncisoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(IncisoPolizaDTO entity);

	/**
	 * Persist a previously saved IncisoPolizaDTO entity and return it or a copy
	 * of it to the sender. A copy of the IncisoPolizaDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            IncisoPolizaDTO entity to update
	 * @return IncisoPolizaDTO the persisted IncisoPolizaDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public IncisoPolizaDTO update(IncisoPolizaDTO entity);

	public IncisoPolizaDTO findById(IncisoPolizaId id);

	/**
	 * Find all IncisoPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the IncisoPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<IncisoPolizaDTO> found by query
	 */
	public List<IncisoPolizaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all IncisoPolizaDTO entities.
	 * 
	 * @return List<IncisoPolizaDTO> all IncisoPolizaDTO entities
	 */
	public List<IncisoPolizaDTO> findAll();

	public void insertIncisosPorCotizacion(BigDecimal idToCotizacion,
			BigDecimal idToPoliza);
	
}