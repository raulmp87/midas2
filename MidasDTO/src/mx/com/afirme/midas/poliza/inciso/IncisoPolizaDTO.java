package mx.com.afirme.midas.poliza.inciso;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.seccion.SeccionPolizaDTO;

/**
 * IncisoPolizaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOINCISOPOL", schema = "MIDAS")
public class IncisoPolizaDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private IncisoPolizaId id;
	private PolizaDTO polizaDTO;
	private Short claveEstatus;
	private List<SeccionPolizaDTO> seccionPolizaDTOs = new ArrayList<SeccionPolizaDTO>();

	// Constructors

	/** default constructor */
	public IncisoPolizaDTO() {
	}

	/** minimal constructor */
	public IncisoPolizaDTO(IncisoPolizaId id, PolizaDTO polizaDTO,
			Short claveEstatus) {
		this.id = id;
		this.polizaDTO = polizaDTO;
		this.claveEstatus = claveEstatus;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)) })
	public IncisoPolizaId getId() {
		return this.id;
	}

	public void setId(IncisoPolizaId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false)
	public PolizaDTO getPolizaDTO() {
		return this.polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return this.claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "incisoPolizaDTO")
	public List<SeccionPolizaDTO> getSeccionPolizaDTOs() {
		return this.seccionPolizaDTOs;
	}

	public void setSeccionPolizaDTOs(List<SeccionPolizaDTO> seccionPolizaDTOs) {
		this.seccionPolizaDTOs = seccionPolizaDTOs;
	}
	
	
	/**
	 * Metodo de conveniencia que retorna el primer elemento de la lista <code>seccionPolizaDTOs</code>. Util para productos como autos 
	 * que solo utilizan una seccion.
	 * @return
	 */
	@Transient
	public SeccionPolizaDTO getSeccionPolizaDTO(){
		SeccionPolizaDTO  seccion = null;
		if(!this.getSeccionPolizaDTOs().isEmpty()){
			seccion = this.getSeccionPolizaDTOs().get(0);
		}
		return seccion;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("Id: " + this.getId());
		sb.append("]");
		
		return sb.toString();
	}
}