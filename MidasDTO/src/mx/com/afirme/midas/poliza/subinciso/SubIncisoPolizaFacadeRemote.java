package mx.com.afirme.midas.poliza.subinciso;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SubIncisoPolizaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SubIncisoPolizaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SubIncisoPolizaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SubIncisoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubIncisoPolizaDTO entity);

	/**
	 * Delete a persistent SubIncisoPolizaDTO entity.
	 * 
	 * @param entity
	 *            SubIncisoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubIncisoPolizaDTO entity);

	/**
	 * Persist a previously saved SubIncisoPolizaDTO entity and return it or a
	 * copy of it to the sender. A copy of the SubIncisoPolizaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SubIncisoPolizaDTO entity to update
	 * @return SubIncisoPolizaDTO the persisted SubIncisoPolizaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubIncisoPolizaDTO update(SubIncisoPolizaDTO entity);

	public SubIncisoPolizaDTO findById(SubIncisoPolizaId id);

	/**
	 * Find all SubIncisoPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SubIncisoPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubIncisoPolizaDTO> found by query
	 */
	public List<SubIncisoPolizaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SubIncisoPolizaDTO entities.
	 * 
	 * @return List<SubIncisoPolizaDTO> all SubIncisoPolizaDTO entities
	 */
	public List<SubIncisoPolizaDTO> findAll();
	
	public void insertSubIncisoPolizaPorCotizacion(BigDecimal idToCotizacion,BigDecimal idToPoliza);
}