package mx.com.afirme.midas.poliza.subinciso;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.poliza.seccion.SeccionPolizaDTO;

/**
 * SubIncisoPolizaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSUBINCISOPOL", schema = "MIDAS")
public class SubIncisoPolizaDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private SubIncisoPolizaId id;
	private SeccionPolizaDTO seccionPolizaDTO;
	private Double valorSumaAsegurada;
	private Double valorPrimaNeta;
	private Short claveEstatus;

	// Constructors

	/** default constructor */
	public SubIncisoPolizaDTO() {
	}

	/** full constructor */
	public SubIncisoPolizaDTO(SubIncisoPolizaId id,
			SeccionPolizaDTO seccionPolizaDTO, Double valorSumaAsegurada,
			Double valorPrimaNeta, Short claveEstatus) {
		this.id = id;
		this.seccionPolizaDTO = seccionPolizaDTO;
		this.valorSumaAsegurada = valorSumaAsegurada;
		this.valorPrimaNeta = valorPrimaNeta;
		this.claveEstatus = claveEstatus;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroSubInciso", column = @Column(name = "NUMEROSUBINCISO", nullable = false, precision = 22, scale = 0)) })
	public SubIncisoPolizaId getId() {
		return this.id;
	}

	public void setId(SubIncisoPolizaId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOPOLIZA", referencedColumnName = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false) })
	public SeccionPolizaDTO getSeccionPolizaDTO() {
		return this.seccionPolizaDTO;
	}

	public void setSeccionPolizaDTO(SeccionPolizaDTO seccionPolizaDTO) {
		this.seccionPolizaDTO = seccionPolizaDTO;
	}

	@Column(name = "VALORSUMAASEGURADA", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return this.claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
}