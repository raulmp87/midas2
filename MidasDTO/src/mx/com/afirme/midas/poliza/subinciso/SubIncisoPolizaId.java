package mx.com.afirme.midas.poliza.subinciso;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * SubIncisoPolizaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class SubIncisoPolizaId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal idToPoliza;
	private BigDecimal numeroInciso;
	private BigDecimal idToSeccion;
	private BigDecimal numeroSubInciso;

	// Constructors

	/** default constructor */
	public SubIncisoPolizaId() {
	}

	/** full constructor */
	public SubIncisoPolizaId(BigDecimal idToPoliza, BigDecimal numeroInciso,
			BigDecimal idToSeccion, BigDecimal numeroSubInciso) {
		this.idToPoliza = idToPoliza;
		this.numeroInciso = numeroInciso;
		this.idToSeccion = idToSeccion;
		this.numeroSubInciso = numeroSubInciso;
	}

	// Property accessors

	@Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToPoliza() {
		return this.idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "NUMEROSUBINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroSubInciso() {
		return this.numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SubIncisoPolizaId))
			return false;
		SubIncisoPolizaId castOther = (SubIncisoPolizaId) other;

		return ((this.getIdToPoliza() == castOther.getIdToPoliza()) || (this
				.getIdToPoliza() != null
				&& castOther.getIdToPoliza() != null && this.getIdToPoliza()
				.equals(castOther.getIdToPoliza())))
				&& ((this.getNumeroInciso() == castOther.getNumeroInciso()) || (this
						.getNumeroInciso() != null
						&& castOther.getNumeroInciso() != null && this
						.getNumeroInciso().equals(castOther.getNumeroInciso())))
				&& ((this.getIdToSeccion() == castOther.getIdToSeccion()) || (this
						.getIdToSeccion() != null
						&& castOther.getIdToSeccion() != null && this
						.getIdToSeccion().equals(castOther.getIdToSeccion())))
				&& ((this.getNumeroSubInciso() == castOther
						.getNumeroSubInciso()) || (this.getNumeroSubInciso() != null
						&& castOther.getNumeroSubInciso() != null && this
						.getNumeroSubInciso().equals(
								castOther.getNumeroSubInciso())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToPoliza() == null ? 0 : this.getIdToPoliza()
						.hashCode());
		result = 37
				* result
				+ (getNumeroInciso() == null ? 0 : this.getNumeroInciso()
						.hashCode());
		result = 37
				* result
				+ (getIdToSeccion() == null ? 0 : this.getIdToSeccion()
						.hashCode());
		result = 37
				* result
				+ (getNumeroSubInciso() == null ? 0 : this.getNumeroSubInciso()
						.hashCode());
		return result;
	}
}