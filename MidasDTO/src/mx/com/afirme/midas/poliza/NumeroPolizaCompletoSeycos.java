package mx.com.afirme.midas.poliza;

import org.apache.commons.lang.StringUtils;

public class NumeroPolizaCompletoSeycos extends NumeroPolizaCompleto {

	private Integer idCentroEmisor;
	private Long numeroPoliza;
	private Integer numeroRenovacion;
	
	public Integer getIdCentroEmisor() {
		return idCentroEmisor;
	}
	
	public void setIdCentroEmisor(Integer idCentroEmisor) {
		this.idCentroEmisor = idCentroEmisor;
	}
	
	public Long getNumeroPoliza() {
		return numeroPoliza;
	}
	
	public void setNumeroPoliza(Long numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	
	public Integer getNumeroRenovacion() {
		return numeroRenovacion;
	}
	
	public void setNumeroRenovacion(Integer numeroRenovacion) {
		this.numeroRenovacion = numeroRenovacion;
	}
	
	//added if this wants to be used as an Embeddable in JPA
	public NumeroPolizaCompletoSeycos() {
		
	}
	
	public NumeroPolizaCompletoSeycos(Integer idCentroEmisor, Long numeroPoliza, Integer numeroRenovacion) {
		this.idCentroEmisor = idCentroEmisor;
		this.numeroPoliza = numeroPoliza;
		this.numeroRenovacion = numeroRenovacion;
	}
	
	public static NumeroPolizaCompletoSeycos parseNumeroPolizaCompleto(String strNumeroPolizaCompleto) {	
		if (strNumeroPolizaCompleto == null || StringUtils.isBlank(strNumeroPolizaCompleto)) {
			throw new IllegalArgumentException("Nulo o vacio.");
		}
		strNumeroPolizaCompleto = StringUtils.trim(strNumeroPolizaCompleto).replace(SEPARADOR, "");
		if (!strNumeroPolizaCompleto.matches("\\d+")) {
			throw new IllegalArgumentException("Contiene caracteres invalidos.");			
		}

		final int TOTAL_DIGITOS_FORMATO_SEYCOS = 15;
		
		Integer idCentroEmisor;
		Long numeroPoliza;
		Integer numeroRenovacion;
		
		if (strNumeroPolizaCompleto.length() == TOTAL_DIGITOS_FORMATO_SEYCOS) {
			idCentroEmisor = Integer.parseInt(strNumeroPolizaCompleto.substring(0, 3));
			numeroPoliza = Long.parseLong(strNumeroPolizaCompleto.substring(3,13));
			numeroRenovacion = Integer.parseInt(strNumeroPolizaCompleto.substring(13, 15));
		}else {
			throw new IllegalArgumentException("Cantidad de digitos invalido");
		}
		
		return new NumeroPolizaCompletoSeycos(idCentroEmisor, numeroPoliza, numeroRenovacion);
	}
	
	/**
	 * Obtiene el Numero de Poliza cuando esta se origina de Midas.
	 * @return numeroPolizaCompletoMidas o null cuando no es una poliza que se origino en Midas.
	 */
	public NumeroPolizaCompletoMidas getOrigenNumeroPolizaCompletoMidas() {
		if (numeroPoliza > 999999) {
			String strNumeroPoliza =  String.format("%010d",numeroPoliza);
			String codigoProducto = strNumeroPoliza.substring(0,2);
			String codigoTipoPoliza = strNumeroPoliza.substring(2, 4);
			//TODO revisar si llegan numeros de póliza con numeroRenovacion, si no, se debe cambiar el 10 por strNumerlPoliza.length
			Integer midasNumeroPoliza = Integer.parseInt(strNumeroPoliza.substring(4, 10)); 
			return new NumeroPolizaCompletoMidas(codigoProducto, codigoTipoPoliza, midasNumeroPoliza, numeroRenovacion);
		}
		return null;
	}
		
	@Override
	public String toString() {
		return String.format("%03d", idCentroEmisor) + SEPARADOR + String.format("%010d",numeroPoliza) + SEPARADOR + String.format("%02d", numeroRenovacion); 
	}

}
