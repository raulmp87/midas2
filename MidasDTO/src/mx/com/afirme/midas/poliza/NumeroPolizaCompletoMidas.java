package mx.com.afirme.midas.poliza;

import org.apache.commons.lang.StringUtils;

public class NumeroPolizaCompletoMidas extends NumeroPolizaCompleto {

private static final long serialVersionUID = -4397998444357063560L;
	
	public static final String SEPARADOR = "-";
	
	private String codigoProducto;
	private String codigoTipoPoliza;
	private Integer numeroPoliza;
	private Integer numeroRenovacion;
	
	//added if this wants to be used as an Embeddable in JPA
	public NumeroPolizaCompletoMidas() {
		
	}
	
	public NumeroPolizaCompletoMidas(String codigoProducto, String codigoTipoPoliza, Integer numeroPoliza, Integer numeroRenovacion) {
		this.codigoProducto = codigoProducto;
		this.codigoTipoPoliza = codigoTipoPoliza;
		this.numeroPoliza = numeroPoliza;
		this.numeroRenovacion = numeroRenovacion;
	}
	
	
	public String getCodigoProducto() {
		return codigoProducto;
	}
	
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	
	public String getCodigoTipoPoliza() {
		return codigoTipoPoliza;
	}
	
	public void setCodigoTipoPoliza(String codigoTipoPoliza) {
		this.codigoTipoPoliza = codigoTipoPoliza;
	}
	
	public Integer getNumeroPoliza() {
		return numeroPoliza;
	}
	
	public void setNumeroPoliza(Integer numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	
	public Integer getNumeroRenovacion() {
		return numeroRenovacion;
	}
	
	public void setNumeroRenovacion(Integer numeroRenovacion) {
		this.numeroRenovacion = numeroRenovacion;
	}

	public static NumeroPolizaCompletoMidas parseNumeroPolizaCompleto(String strNumeroPolizaCompleto) {	
		if (StringUtils.isBlank(strNumeroPolizaCompleto)) {
			throw new IllegalArgumentException("Nulo o vacio.");
		}
		strNumeroPolizaCompleto = StringUtils.trim(strNumeroPolizaCompleto).replace(SEPARADOR, "");
		if (!strNumeroPolizaCompleto.matches("\\d+")) {
			throw new IllegalArgumentException("Contiene caracteres invalidos.");			
		}
		
		String codigoProducto;
		String codigoTipoPoliza;
		Integer numeroPoliza;
		Integer numeroRenovacion;
		if (strNumeroPolizaCompleto.length() == NumeroPolizaCompleto.OLD_TOTAL_DIGITOS_FORMATO_MIDAS) {
			codigoProducto = strNumeroPolizaCompleto.substring(0, 2);
			codigoTipoPoliza = strNumeroPolizaCompleto.substring(2, 4);
			numeroPoliza = Integer.parseInt(strNumeroPolizaCompleto.substring(4, 10));
			numeroRenovacion = Integer.parseInt(strNumeroPolizaCompleto.substring(10, 12));
		} else if (strNumeroPolizaCompleto.length() == NumeroPolizaCompleto.TOTAL_DIGITOS_FORMATO_MIDAS ) {
			codigoProducto = strNumeroPolizaCompleto.substring(0, 2);
			codigoTipoPoliza = strNumeroPolizaCompleto.substring(2, 4);
			numeroPoliza = Integer.parseInt(strNumeroPolizaCompleto.substring(4, 12));
			numeroRenovacion = Integer.parseInt(strNumeroPolizaCompleto.substring(12, 14));
		} else { 
			throw new IllegalArgumentException("Cantidad de digitos invalido");
		}
		
		return new NumeroPolizaCompletoMidas(codigoProducto, codigoTipoPoliza, numeroPoliza, numeroRenovacion);
	}
		
	
	@Override
	public String toString() {
		return codigoProducto + codigoTipoPoliza + SEPARADOR + String.format("%08d",numeroPoliza) + SEPARADOR + String.format("%02d", numeroRenovacion); 
	}
	
}
