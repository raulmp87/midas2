package mx.com.afirme.midas.poliza;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.domain.emision.conductoresAdicionales.PolizaAutoSeycos;

/**
 * Remote interface for PolizaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface PolizaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved PolizaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            PolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	PolizaDTO save(PolizaDTO entity);

	/**
	 * Delete a persistent PolizaDTO entity.
	 * 
	 * @param entity
	 *            PolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	void delete(PolizaDTO entity);

	/**
	 * Persist a previously saved PolizaDTO entity and return it or a copy of it
	 * to the sender. A copy of the PolizaDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            PolizaDTO entity to update
	 * @return PolizaDTO the persisted PolizaDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	PolizaDTO update(PolizaDTO entity);

	PolizaDTO findById(BigDecimal id);

	/**
	 * Find all PolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the PolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<PolizaDTO> found by query
	 */
	List<PolizaDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all PolizaDTO entities.
	 * 
	 * @return List<PolizaDTO> all PolizaDTO entities
	 */
	List<PolizaDTO> findAll();

	List<PolizaDTO> buscarFiltrado(PolizaDTO polizaDTO);
	
	List<Object[]> buscarFiltrado(PolizaDTO polizaDTO, Date fechaInicial, Date fechaFinal);

	Long obtenerTotalFiltrado(PolizaDTO polizaDTO);
	
	Map<String, String> emitirPoliza(BigDecimal idToCotizacion,
			PolizaDTO polizaDTO,Double ivaCotizacion);

	List<PolizaDTO> selectPolizasByEstatus(short cveEstatus);

	List<PolizaDTO> selectPolizasByDatosNumPolizaFormato(String numPoliza);
	
	PolizaDTO buscaPrimerRegistro();
	
	List<PolizaDTO> selectPolizasFacultativo();
	
	boolean existeReciboPagado(BigDecimal idToPoliza) throws Exception;
	
	CotizacionDTO setMontosRFPAcumulados(BigDecimal idToPoliza,double diasPorDevengar, CotizacionDTO cotizacion);
	
	/**
	 * Genera los recibos de la p�liza recibida, con el n�mero de endoso recibido.
	 * El nombre del archivo queda configurado como 'Recibo'+Id de la poliza+numeroEndoso y la extensi�n PDF.
	 * @param polizaDTO, la p�liza cuyos recibos se desean imprimir.
	 * @param Integer numeroEndoso, el endoso cuyos recibos se desean imprimir.
	 * @return true, si la operaci�n es exitosa, false de lo contrario.
	 */
	boolean generarRecibosEndoso(PolizaDTO polizaDTO, Short numeroEndoso);
	
	Long obtenerTotalPaginacion(PolizaDTO polizaDTO, Date fechaInicial, Date fechaFinal);
	
	List<PolizaDTO> buscarFiltradoRenovacion(PolizaDTO polizaDTO, Date fechaInicial, Date fechaFinal, Boolean isCount,
			Boolean isFixEndoso,Boolean exclusionRenovacion);
	
	Long obtieneTotalFiltradoRenovacion(PolizaDTO polizaDTO, Date fechaInicial, Date fechaFinal, Boolean isCount,
			Boolean exclusionRenovacion);
	
	PolizaDTO find(String codigoProducto, String codigoTipoPoliza, Integer numeroPoliza, Integer numeroRenovacion);
	
	PolizaDTO find(String clavePolizaSeycos);
	
	PolizaDTO find(NumeroPolizaCompleto numeroPolizaCompleto);
	
	PolizaDTO find(NumeroPolizaCompletoMidas numeroPolizaCompletoMidas);
	
	PolizaDTO find(NumeroPolizaCompletoSeycos numeroCompletoSeycos);
	
	boolean esPolizaAutoplazo(BigDecimal idToPoliza, List<SeccionDTO> seccionList);
	
	BigDecimal obtenerNumeroCotizacionSeycos(BigDecimal idToPoliza);
	
	String obtenerXMLPrimerReciboPoliza(BigDecimal idToPoliza);
	
	boolean esPolizaCambioIVA(BigDecimal idToPoliza);
	
	boolean esPolizaCasa(BigDecimal idToPoliza);
	
	BigDecimal obtenerIdDoctoVersSeycos(BigDecimal idToPoliza, BigDecimal numeroEndoso);
	
	BigDecimal obtenerNumeroCotizacionSeycos(String numeroPoliza, int numeroRenovacion, int numeroEndoso);
	
	List<Object[]> buscarPolizaSeycos(Map<String, Object> filtros);
	
	int guardarConductoresAdicionales(PolizaAutoSeycos polizaAutoSeycos);
}