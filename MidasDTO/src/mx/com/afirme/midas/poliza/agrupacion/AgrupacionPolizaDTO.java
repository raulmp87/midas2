package mx.com.afirme.midas.poliza.agrupacion;

import java.math.BigDecimal;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.poliza.PolizaDTO;

/**
 * AgrupacionPolizaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOAGRUPACIONPOL", schema = "MIDAS")
public class AgrupacionPolizaDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private AgrupacionPolizaId id;
	private PolizaDTO polizaDTO;
	private Short claveTipoAgrupacion;
	private BigDecimal idToSeccion;
	private Double valorSumaAsegurada;
	private Double valorCuota;
	private Double valorPrimaNeta;

	// Constructors

	/** default constructor */
	public AgrupacionPolizaDTO() {
	}

	/** full constructor */
	public AgrupacionPolizaDTO(AgrupacionPolizaId id, PolizaDTO polizaDTO,
			Short claveTipoAgrupacion, BigDecimal idToSeccion,
			Double valorSumaAsegurada, Double valorCuota, Double valorPrimaNeta) {
		this.id = id;
		this.polizaDTO = polizaDTO;
		this.claveTipoAgrupacion = claveTipoAgrupacion;
		this.idToSeccion = idToSeccion;
		this.valorSumaAsegurada = valorSumaAsegurada;
		this.valorCuota = valorCuota;
		this.valorPrimaNeta = valorPrimaNeta;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroAgrupacion", column = @Column(name = "NUMEROAGRUPACION", nullable = false, precision = 4, scale = 0)) })
	public AgrupacionPolizaId getId() {
		return this.id;
	}

	public void setId(AgrupacionPolizaId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false)
	public PolizaDTO getPolizaDTO() {
		return this.polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	@Column(name = "CLAVETIPOAGRUPACION", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoAgrupacion() {
		return this.claveTipoAgrupacion;
	}

	public void setClaveTipoAgrupacion(Short claveTipoAgrupacion) {
		this.claveTipoAgrupacion = claveTipoAgrupacion;
	}

	@Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "VALORSUMAASEGURADA", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Column(name = "VALORCUOTA", nullable = false, precision = 16, scale = 10)
	public Double getValorCuota() {
		return this.valorCuota;
	}

	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}
}