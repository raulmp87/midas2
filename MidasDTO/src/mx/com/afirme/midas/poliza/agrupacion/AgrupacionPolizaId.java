package mx.com.afirme.midas.poliza.agrupacion;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * AgrupacionPolizaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class AgrupacionPolizaId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal idToPoliza;
	private Short numeroAgrupacion;

	// Constructors

	/** default constructor */
	public AgrupacionPolizaId() {
	}

	/** full constructor */
	public AgrupacionPolizaId(BigDecimal idToPoliza, Short numeroAgrupacion) {
		this.idToPoliza = idToPoliza;
		this.numeroAgrupacion = numeroAgrupacion;
	}

	// Property accessors

	@Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToPoliza() {
		return this.idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@Column(name = "NUMEROAGRUPACION", nullable = false, precision = 4, scale = 0)
	public Short getNumeroAgrupacion() {
		return this.numeroAgrupacion;
	}

	public void setNumeroAgrupacion(Short numeroAgrupacion) {
		this.numeroAgrupacion = numeroAgrupacion;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof AgrupacionPolizaId))
			return false;
		AgrupacionPolizaId castOther = (AgrupacionPolizaId) other;

		return ((this.getIdToPoliza() == castOther.getIdToPoliza()) || (this
				.getIdToPoliza() != null
				&& castOther.getIdToPoliza() != null && this.getIdToPoliza()
				.equals(castOther.getIdToPoliza())))
				&& ((this.getNumeroAgrupacion() == castOther
						.getNumeroAgrupacion()) || (this.getNumeroAgrupacion() != null
						&& castOther.getNumeroAgrupacion() != null && this
						.getNumeroAgrupacion().equals(
								castOther.getNumeroAgrupacion())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToPoliza() == null ? 0 : this.getIdToPoliza()
						.hashCode());
		result = 37
				* result
				+ (getNumeroAgrupacion() == null ? 0 : this
						.getNumeroAgrupacion().hashCode());
		return result;
	}
}