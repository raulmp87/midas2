package mx.com.afirme.midas.poliza.agrupacion;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for AgrupacionPolizaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface AgrupacionPolizaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved AgrupacionPolizaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            AgrupacionPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(AgrupacionPolizaDTO entity);

	/**
	 * Delete a persistent AgrupacionPolizaDTO entity.
	 * 
	 * @param entity
	 *            AgrupacionPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(AgrupacionPolizaDTO entity);

	/**
	 * Persist a previously saved AgrupacionPolizaDTO entity and return it or a
	 * copy of it to the sender. A copy of the AgrupacionPolizaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            AgrupacionPolizaDTO entity to update
	 * @return AgrupacionPolizaDTO the persisted AgrupacionPolizaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public AgrupacionPolizaDTO update(AgrupacionPolizaDTO entity);

	public AgrupacionPolizaDTO findById(AgrupacionPolizaId id);

	/**
	 * Find all AgrupacionPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the AgrupacionPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<AgrupacionPolizaDTO> found by query
	 */
	public List<AgrupacionPolizaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all AgrupacionPolizaDTO entities.
	 * 
	 * @return List<AgrupacionPolizaDTO> all AgrupacionPolizaDTO entities
	 */
	public List<AgrupacionPolizaDTO> findAll();
	
	public void insertAgrupacionesPorCotizacion(BigDecimal idToCotizacion,BigDecimal idToPoliza);
}