package mx.com.afirme.midas.poliza;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;


public abstract class NumeroPolizaCompleto implements Serializable {
	
	public static final String SEPARADOR = "-";
	public static final int OLD_TOTAL_DIGITOS_FORMATO_MIDAS = 12;
	public static final int TOTAL_DIGITOS_FORMATO_MIDAS = 14;
	public static final int TOTAL_DIGITOS_FORMATO_SEYCOS_UNO = 15;
	public static final int TOTAL_DIGITOS_FORMATO_SEYCOS_DOS = 17;
	
	public static NumeroPolizaCompleto parseNumeroPolizaCompleto(String strNumeroPolizaCompleto) {
		if (strNumeroPolizaCompleto == null || StringUtils.isBlank(strNumeroPolizaCompleto)) {
			throw new IllegalArgumentException("Nulo o vacio.");
		}
		strNumeroPolizaCompleto = StringUtils.trim(strNumeroPolizaCompleto).replace(SEPARADOR, "");
		if (!strNumeroPolizaCompleto.matches("\\d+")) {
			throw new IllegalArgumentException("Contiene caracteres invalidos.");			
		}

		NumeroPolizaCompleto numeroPolizaCompleto = null;
		if (strNumeroPolizaCompleto.length() == OLD_TOTAL_DIGITOS_FORMATO_MIDAS || 
				strNumeroPolizaCompleto.length() == TOTAL_DIGITOS_FORMATO_MIDAS) {
			numeroPolizaCompleto = NumeroPolizaCompletoMidas.parseNumeroPolizaCompleto(strNumeroPolizaCompleto);
		} else if (strNumeroPolizaCompleto.length() == 15) {
			numeroPolizaCompleto = NumeroPolizaCompletoSeycos.parseNumeroPolizaCompleto(strNumeroPolizaCompleto);
		} else {
			throw new RuntimeException("Formato incorrecto.");
		}
		
		return numeroPolizaCompleto;		
	}
}
