package mx.com.afirme.midas.poliza.seccion;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SeccionPolizaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SeccionPolizaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SeccionPolizaDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SeccionPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public SeccionPolizaDTO save(SeccionPolizaDTO entity);

	/**
	 * Delete a persistent SeccionPolizaDTO entity.
	 * 
	 * @param entity
	 *            SeccionPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SeccionPolizaDTO entity);

	/**
	 * Persist a previously saved SeccionPolizaDTO entity and return it or a
	 * copy of it to the sender. A copy of the SeccionPolizaDTO entity parameter
	 * is returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            SeccionPolizaDTO entity to update
	 * @return SeccionPolizaDTO the persisted SeccionPolizaDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SeccionPolizaDTO update(SeccionPolizaDTO entity);

	public SeccionPolizaDTO findById(SeccionPolizaId id);

	/**
	 * Find all SeccionPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SeccionPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SeccionPolizaDTO> found by query
	 */
	public List<SeccionPolizaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SeccionPolizaDTO entities.
	 * 
	 * @return List<SeccionPolizaDTO> all SeccionPolizaDTO entities
	 */
	public List<SeccionPolizaDTO> findAll();

	public void insertSeccionesPorCotizacion(BigDecimal idToCotizacion,
			BigDecimal idToPoliza);
}