package mx.com.afirme.midas.poliza.seccion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import mx.com.afirme.midas.poliza.cobertura.CoberturaPolizaDTO;
import mx.com.afirme.midas.poliza.inciso.IncisoPolizaDTO;
import mx.com.afirme.midas.poliza.subinciso.SubIncisoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;

/**
 * SeccionPolizaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSECCIONPOL", schema = "MIDAS")
public class SeccionPolizaDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private SeccionPolizaId id;
	private SeccionDTO seccionDTO;
	private IncisoPolizaDTO incisoPolizaDTO;
	private Double valorPrimaNeta;
	private List<SubIncisoPolizaDTO> subIncisoPolizaDTOs = new ArrayList<SubIncisoPolizaDTO>();
	private List<CoberturaPolizaDTO> coberturaPolizaDTOs = new ArrayList<CoberturaPolizaDTO>();

	// Constructors

	/** default constructor */
	public SeccionPolizaDTO() {
	}

	/** minimal constructor */
	public SeccionPolizaDTO(SeccionPolizaId id, SeccionDTO seccionDTO,
			IncisoPolizaDTO incisoPolizaDTO, Double valorPrimaNeta) {
		this.id = id;
		this.seccionDTO = seccionDTO;
		this.incisoPolizaDTO = incisoPolizaDTO;
		this.valorPrimaNeta = valorPrimaNeta;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)) })
	public SeccionPolizaId getId() {
		return this.id;
	}

	public void setId(SeccionPolizaId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOSECCION", nullable = false, insertable = false, updatable = false)
	public SeccionDTO getSeccionDTO() {
		return this.seccionDTO;
	}

	public void setSeccionDTO(SeccionDTO seccionDTO) {
		this.seccionDTO = seccionDTO;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOPOLIZA", referencedColumnName = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false) })
	public IncisoPolizaDTO getIncisoPolizaDTO() {
		return this.incisoPolizaDTO;
	}

	public void setIncisoPolizaDTO(IncisoPolizaDTO incisoPolizaDTO) {
		this.incisoPolizaDTO = incisoPolizaDTO;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "seccionPolizaDTO")
	public List<SubIncisoPolizaDTO> getSubIncisoPolizaDTOs() {
		return this.subIncisoPolizaDTOs;
	}

	public void setSubIncisoPolizaDTOs(List<SubIncisoPolizaDTO> subIncisoPolizaDTOs) {
		this.subIncisoPolizaDTOs = subIncisoPolizaDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "seccionPolizaDTO")
	public List<CoberturaPolizaDTO> getCoberturaPolizaDTOs() {
		return this.coberturaPolizaDTOs;
	}

	public void setCoberturaPolizaDTOs(List<CoberturaPolizaDTO> coberturaPolizaDTOs) {
		this.coberturaPolizaDTOs = coberturaPolizaDTOs;
	}
}