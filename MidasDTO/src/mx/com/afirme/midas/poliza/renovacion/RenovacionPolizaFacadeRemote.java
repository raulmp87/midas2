package mx.com.afirme.midas.poliza.renovacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import mx.com.afirme.midas.poliza.PolizaDTO;


public interface RenovacionPolizaFacadeRemote {

	/**
	 * Perform an initial save of a previously unsaved Torenovacionpoliza
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Torenovacionpoliza entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RenovacionPolizaDTO entity);

	/**
	 * Delete a persistent Torenovacionpoliza entity.
	 * 
	 * @param entity
	 *            Torenovacionpoliza entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RenovacionPolizaDTO entity);

	/**
	 * Persist a previously saved Torenovacionpoliza entity and return it or a
	 * copy of it to the sender. A copy of the Torenovacionpoliza entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            Torenovacionpoliza entity to update
	 * @return Torenovacionpoliza the persisted Torenovacionpoliza entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RenovacionPolizaDTO update(RenovacionPolizaDTO entity);

	public RenovacionPolizaDTO findById(BigDecimal id);

	/**
	 * Find all Torenovacionpoliza entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Torenovacionpoliza property to query
	 * @param value
	 *            the property value to match
	 * @return List<Torenovacionpoliza> found by query
	 */
	public List<RenovacionPolizaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all Torenovacionpoliza entities.
	 * 
	 * @return List<Torenovacionpoliza> all Torenovacionpoliza entities
	 */
	public List<RenovacionPolizaDTO> findAll();

	public Map<String, String> procesaRenovacionPoliza(BigDecimal idTopoliza,
			String usuarioCreacion, String usuarioAsigacion);

	public List<PolizaDTO> buscarFiltrado(PolizaDTO polizaDTO,
			boolean esRenovacion, boolean esReporte);

	public void agregarDetalleSeguimiento(BigDecimal idToPoliza,
			SeguimientoRenovacionDTO seguimientoRenovacionDTO);

	public RenovacionPolizaDTO buscarDetalleRenovacionPoliza(
			BigDecimal idToPoliza);

	public List<RenovacionPolizaDTO> buscarFiltrado(
			RenovacionPolizaDTO renovacionPolizaDTO, boolean esNotificacion);
	
	public Integer getSiguienteNumeroNotificacion(BigDecimal idToPoliza);

	public SeguimientoRenovacionDTO getUltimoDetalleRenovacionPoliza(
			BigDecimal idToPoliza);
	
}
