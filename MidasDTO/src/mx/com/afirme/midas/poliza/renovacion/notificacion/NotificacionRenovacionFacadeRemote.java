package mx.com.afirme.midas.poliza.renovacion.notificacion;

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for TtNotificacionRenovacionFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface NotificacionRenovacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved TtNotificacionRenovacion
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            TtNotificacionRenovacion entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(NotificacionRenovacionDTO entity);

	/**
	 * Delete a persistent TtNotificacionRenovacion entity.
	 * 
	 * @param entity
	 *            TtNotificacionRenovacion entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(NotificacionRenovacionDTO entity);

	/**
	 * Persist a previously saved TtNotificacionRenovacion entity and return it
	 * or a copy of it to the sender. A copy of the TtNotificacionRenovacion
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TtNotificacionRenovacion entity to update
	 * @return TtNotificacionRenovacion the persisted TtNotificacionRenovacion
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public NotificacionRenovacionDTO update(NotificacionRenovacionDTO entity);

	/**
	 * Find all TtNotificacionRenovacion entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the TtNotificacionRenovacion property to query
	 * @param value
	 *            the property value to match
	 * @return List<TtNotificacionRenovacion> found by query
	 */
	public List<NotificacionRenovacionDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all TtNotificacionRenovacion entities.
	 * 
	 * @return List<TtNotificacionRenovacion> all TtNotificacionRenovacion
	 *         entities
	 */
	public List<NotificacionRenovacionDTO> findAll();
}