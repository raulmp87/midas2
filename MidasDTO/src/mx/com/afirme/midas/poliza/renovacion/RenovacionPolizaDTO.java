package mx.com.afirme.midas.poliza.renovacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;

/**
 * TORENOVACIONPOLIZA entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TORENOVACIONPOLIZA", schema = "MIDAS")
public class RenovacionPolizaDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToRenovacionPoliza;
	private PolizaDTO polizaDTO;
	private String codigoUsuarioCreacion;
	private String codigoUsuarioAsignacion;
	private Date fechaCreacion;
	private CotizacionDTO cotizacionDTO;
	private List<SeguimientoRenovacionDTO> seguimientoRenovacion = new ArrayList<SeguimientoRenovacionDTO>();
	private String claveSeleccion;

	private Date fechaCreacionDesde;
	private Date fechaCreacionHasta;	
	// Constructors

	/** default constructor */
	public RenovacionPolizaDTO() {
	}

	/** minimal constructor */
	public RenovacionPolizaDTO(BigDecimal idToRenovacionPoliza,
			PolizaDTO polizaDTO) {
		this.idToRenovacionPoliza = idToRenovacionPoliza;
		this.polizaDTO = polizaDTO;
	}

	/** full constructor */
	public RenovacionPolizaDTO(BigDecimal idToRenovacionPoliza,
			PolizaDTO polizaDTO, String codigoUsuarioCreacion,
			String codigoUsuarioAsignacion, Date fechaCreacion,
			List<SeguimientoRenovacionDTO> seguimientoRenovacion) {
		this.idToRenovacionPoliza = idToRenovacionPoliza;
		this.polizaDTO = polizaDTO;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.codigoUsuarioAsignacion = codigoUsuarioAsignacion;
		this.fechaCreacion = fechaCreacion;
		this.seguimientoRenovacion = seguimientoRenovacion;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTORENOVACIONPOLIZA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTORENOVACIONPOLIZA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTORENOVACIONPOLIZA_SEQ_GENERADOR")
	@Column(name = "IDTORENOVACIONPOLIZA", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToRenovacionPoliza() {
		return this.idToRenovacionPoliza;
	}

	public void setIdToRenovacionPoliza(BigDecimal idToRenovacionPoliza) {
		this.idToRenovacionPoliza = idToRenovacionPoliza;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOPOLIZA", nullable = false)
	public PolizaDTO getPolizaDTO() {
		return this.polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	@Column(name = "CODIGOUSUARIOCREACION", length = 8)
	public String getCodigoUsuarioCreacion() {
		return this.codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@Column(name = "CODIGOUSUARIOASIGNACION", length = 8)
	public String getCodigoUsuarioAsignacion() {
		return this.codigoUsuarioAsignacion;
	}

	public void setCodigoUsuarioAsignacion(String codigoUsuarioAsignacion) {
		this.codigoUsuarioAsignacion = codigoUsuarioAsignacion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHACREACION", length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "renovacionPolizaDTO")
	public List<SeguimientoRenovacionDTO> getSeguimientoRenovacion() {
		return this.seguimientoRenovacion;
	}

	public void setSeguimientoRenovacion(
			List<SeguimientoRenovacionDTO> seguimientoRenovacion) {
		this.seguimientoRenovacion = seguimientoRenovacion;
	}
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCOTIZACION", nullable = false)
	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	@Transient
	public String getClaveSeleccion() {
		return claveSeleccion;
	}

	public void setClaveSeleccion(String claveSeleccion) {
		this.claveSeleccion = claveSeleccion;
	}
	
	@Transient
	public Date getFechaCreacionDesde() {
		return fechaCreacionDesde;
	}

	public void setFechaCreacionDesde(Date fechaCreacionDesde) {
		this.fechaCreacionDesde = fechaCreacionDesde;
	}

	@Transient
	public Date getFechaCreacionHasta() {
		return fechaCreacionHasta;
	}

	public void setFechaCreacionHasta(Date fechaCreacionHasta) {
		this.fechaCreacionHasta = fechaCreacionHasta;
	}

}