package mx.com.afirme.midas.poliza.renovacion.notificacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.poliza.PolizaDTO;

@Entity
@Table(name = "TTNOTIFICACIONRENOVACION", schema = "MIDAS")
public class NotificacionRenovacionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idTtNotificacionRenovacion;
	private PolizaDTO polizaDTO;
	private Short estatusRegistro;
	private Date fechaMovimiento;
	private String descripcionMovimiento;
	private Short tipoDestinatario;
	
	//Constantes
    public static final short ESTATUS_REGISTRO_SIN_PROCESAR = 0;
    public static final short ESTATUS_REGISTRO_EN_PROCESO = 1;
    public static final short ESTATUS_REGISTRO_PROCESADO = 2;
    public static final short ESTATUS_EMISION_REGISTRO_FALLO = 3;	

	// Constructors

	/** default constructor */
	public NotificacionRenovacionDTO() {
	}

	/** full constructor */
	public NotificacionRenovacionDTO(BigDecimal idTtNotificacionRenovacion,
			PolizaDTO polizaDTO, Short estatusRegistro, Date fechaMovimiento,
			String descripcionMovimiento, Short tipoDestinatario) {
		this.idTtNotificacionRenovacion = idTtNotificacionRenovacion;
		this.polizaDTO = polizaDTO;
		this.estatusRegistro = estatusRegistro;
		this.fechaMovimiento = fechaMovimiento;
		this.descripcionMovimiento = descripcionMovimiento;
		this.tipoDestinatario = tipoDestinatario;
	}

	@Id
	@SequenceGenerator(name = "IDTTNOTIFICACIONRENOVACION_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTTNOTIFICACIONRENOVACION_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTTNOTIFICACIONRENOVACION_SEQ_GENERADOR")
	@Column(name = "IDTTNOTIFICACIONRENOVACION", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTtNotificacionRenovacion() {
		return idTtNotificacionRenovacion;
	}

	public void setIdTtNotificacionRenovacion(
			BigDecimal idTtNotificacionRenovacion) {
		this.idTtNotificacionRenovacion = idTtNotificacionRenovacion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOPOLIZA", nullable = false)
	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	@Column(name = "ESTATUSREGISTRO", nullable = false, precision = 1, scale = 0)
	public Short getEstatusRegistro() {
		return estatusRegistro;
	}

	public void setEstatusRegistro(Short estatusRegistro) {
		this.estatusRegistro = estatusRegistro;
	}

	@Column(name = "FECHAMOVIMIENTO")
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}

	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}

	@Column(name = "DESCRIPCIONMOVIMIENTO", length = 8)
	public String getDescripcionMovimiento() {
		return descripcionMovimiento;
	}

	public void setDescripcionMovimiento(String descripcionMovimiento) {
		this.descripcionMovimiento = descripcionMovimiento;
	}

	@Column(name = "TIPODESTINATARIO", length = 8)
	public Short getTipoDestinatario() {
		return tipoDestinatario;
	}

	public void setTipoDestinatario(Short tipoDestinatario) {
		this.tipoDestinatario = tipoDestinatario;
	}

}
