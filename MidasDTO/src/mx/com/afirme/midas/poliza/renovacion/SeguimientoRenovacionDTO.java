package mx.com.afirme.midas.poliza.renovacion;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.PaginadoDTO;

/**
 * SeguimientoRenovacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSEGUIMIENTORENOVACION", schema = "MIDAS")
public class SeguimientoRenovacionDTO extends PaginadoDTO implements
		Comparable<SeguimientoRenovacionDTO> {

	// Fields
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToTeguimientoRenovacion;
	private Date fechaMovimiento;
	private BigDecimal tipoMovimiento;
	private String descripcionMovimiento;
	private RenovacionPolizaDTO renovacionPolizaDTO;
	private String descripcionTipoMovimiento;
	public static final BigDecimal TIPO_ASIGNACION_SUSCRIPTOR = BigDecimal
			.valueOf(10);
	public static final BigDecimal TIPO_SOLICITUD_EN_PROCESO = BigDecimal
			.valueOf(11);
	public static final BigDecimal TIPO_ODT_ASIGNADA = BigDecimal.valueOf(12);
	public static final BigDecimal TIPO_COT_LIBERADA = BigDecimal.valueOf(13);
	public static final BigDecimal TIPO_COT_LISTA_EMISION = BigDecimal
			.valueOf(14);
	public static final BigDecimal TIPO_RENOVACION_ACEPTADA = BigDecimal
			.valueOf(15);
	public static final BigDecimal TIPO_ODT_PROCESO = BigDecimal.valueOf(16);
	public static final BigDecimal TIPO_ACEPTA_CON_SINIESTRO = BigDecimal
			.valueOf(17);
	public static final BigDecimal TIPO_ENVIO_EMAIL = BigDecimal.valueOf(20);
	public static final BigDecimal TIPO_ENVIO_ALERTAMIENTO = BigDecimal
			.valueOf(30);
	public static final BigDecimal TIPO_VENCIMIENTO_PLAZO = BigDecimal
			.valueOf(40);
	public static final BigDecimal TIPO_POLIZA_RENOVADA = BigDecimal
			.valueOf(50);

	// Constructors

	/** default constructor */
	public SeguimientoRenovacionDTO() {
	}

	/** full constructor */
	public SeguimientoRenovacionDTO(BigDecimal idToTeguimientoRenovacion,
			Date fechaMovimiento, BigDecimal tipoMovimiento,
			String descripcionMovimiento,
			RenovacionPolizaDTO renovacionPolizaDTO) {
		this.idToTeguimientoRenovacion = idToTeguimientoRenovacion;
		this.fechaMovimiento = fechaMovimiento;
		this.tipoMovimiento = tipoMovimiento;
		this.descripcionMovimiento = descripcionMovimiento;
		this.renovacionPolizaDTO = renovacionPolizaDTO;
	}

	@Id
	@SequenceGenerator(name = "IDTOSEGUIMIENTORENOVACION_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOSEGUIMIENTORENOVACION_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOSEGUIMIENTORENOVACION_SEQ_GENERADOR")
	@Column(name = "IDTOSEGUIMIENTORENOVACION", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToTeguimientoRenovacion() {
		return idToTeguimientoRenovacion;
	}

	public void setIdToTeguimientoRenovacion(
			BigDecimal idToTeguimientoRenovacion) {
		this.idToTeguimientoRenovacion = idToTeguimientoRenovacion;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTORENOVACIONPOLIZA")
	public RenovacionPolizaDTO getRenovacionPolizaDTO() {
		return this.renovacionPolizaDTO;
	}

	public void setRenovacionPolizaDTO(RenovacionPolizaDTO renovacionPolizaDTO) {
		this.renovacionPolizaDTO = renovacionPolizaDTO;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAMOVIMIENTO", length = 7)
	public Date getFechaMovimiento() {
		return this.fechaMovimiento;
	}

	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}

	@Column(name = "TIPOMOVIMIENTO", precision = 22, scale = 0)
	public BigDecimal getTipoMovimiento() {
		return this.tipoMovimiento;
	}

	public void setTipoMovimiento(BigDecimal tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	@Column(name = "DESCRIPCIONMOVIMIENTO", length = 2000)
	public String getDescripcionMovimiento() {
		return this.descripcionMovimiento;
	}

	public void setDescripcionMovimiento(String descripcionMovimiento) {
		this.descripcionMovimiento = descripcionMovimiento;
	}

	@Transient
	public String getDescripcionTipoMovimiento() {
		if (this.tipoMovimiento.intValue() == TIPO_ASIGNACION_SUSCRIPTOR
				.intValue()) {
			this.descripcionTipoMovimiento = "Se asignó renovación a Suscriptor";
		} else if (this.tipoMovimiento.intValue() == TIPO_RENOVACION_ACEPTADA
				.intValue()) {
			this.descripcionTipoMovimiento = "El cliente acepto la renovación";
		} else if (this.tipoMovimiento.intValue() == TIPO_ENVIO_EMAIL
				.intValue()) {
			this.descripcionTipoMovimiento = "Se envió email";
		} else if (this.tipoMovimiento.intValue() == TIPO_ENVIO_ALERTAMIENTO
				.intValue()) {
			String noNotificacion = this.getDescripcionMovimiento().substring(
					this.getDescripcionMovimiento().length() - 1,
					this.getDescripcionMovimiento().length());
			this.descripcionTipoMovimiento = "Se envió notificación: "
					+ noNotificacion;
		} else if (this.tipoMovimiento.intValue() == TIPO_VENCIMIENTO_PLAZO
				.intValue()) {
			this.descripcionTipoMovimiento = "Se venció el plazo para renovar";
		} else if (this.tipoMovimiento.intValue() == TIPO_POLIZA_RENOVADA
				.intValue()) {
			this.descripcionTipoMovimiento = "Se renovo la póliza";
		} else if (this.tipoMovimiento.intValue() == TIPO_SOLICITUD_EN_PROCESO
				.intValue()) {
			this.descripcionTipoMovimiento = "Solicitud en proceso";
		} else if (this.tipoMovimiento.intValue() == TIPO_ODT_ASIGNADA
				.intValue()) {
			this.descripcionTipoMovimiento = "Orden de Trabajo Asignada";
		} else if (this.tipoMovimiento.intValue() == TIPO_COT_LIBERADA
				.intValue()) {
			this.descripcionTipoMovimiento = "Cotización Liberada";
		} else if (this.tipoMovimiento.intValue() == TIPO_COT_LISTA_EMISION
				.intValue()) {
			this.descripcionTipoMovimiento = "Cotización Lista para Emisión";
		} else if (this.tipoMovimiento.intValue() == TIPO_ODT_PROCESO
				.intValue()) {
			this.descripcionTipoMovimiento = "Orden de Trabajo en Proceso";
		} else if (this.tipoMovimiento.intValue() == TIPO_ACEPTA_CON_SINIESTRO
				.intValue()) {
			this.descripcionTipoMovimiento = "Se aceptó la Cotización con Siniestro";
		}


		return this.descripcionTipoMovimiento;
	}

	public void setDescripcionTipoMovimiento(String descripcionTipoMovimiento) {
		this.descripcionTipoMovimiento = descripcionTipoMovimiento;
	}

	public int compareTo(SeguimientoRenovacionDTO o) {
		return this.getFechaMovimiento().compareTo(o.getFechaMovimiento());
	}

}