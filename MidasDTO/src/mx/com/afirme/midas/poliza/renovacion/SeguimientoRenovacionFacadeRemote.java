package mx.com.afirme.midas.poliza.renovacion;

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ToseguimientorenovacionFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SeguimientoRenovacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved Toseguimientorenovacion
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Toseguimientorenovacion entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SeguimientoRenovacionDTO entity);

	/**
	 * Delete a persistent Toseguimientorenovacion entity.
	 * 
	 * @param entity
	 *            Toseguimientorenovacion entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SeguimientoRenovacionDTO entity);

	/**
	 * Persist a previously saved Toseguimientorenovacion entity and return it
	 * or a copy of it to the sender. A copy of the Toseguimientorenovacion
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            Toseguimientorenovacion entity to update
	 * @return Toseguimientorenovacion the persisted Toseguimientorenovacion
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SeguimientoRenovacionDTO update(SeguimientoRenovacionDTO entity);

	/**
	 * Find all Toseguimientorenovacion entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Toseguimientorenovacion property to query
	 * @param value
	 *            the property value to match
	 * @return List<Toseguimientorenovacion> found by query
	 */
	public List<SeguimientoRenovacionDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all Toseguimientorenovacion entities.
	 * 
	 * @return List<Toseguimientorenovacion> all Toseguimientorenovacion
	 *         entities
	 */
	public List<SeguimientoRenovacionDTO> findAll();
}