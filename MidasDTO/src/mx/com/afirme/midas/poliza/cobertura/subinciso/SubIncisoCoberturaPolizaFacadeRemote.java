package mx.com.afirme.midas.poliza.cobertura.subinciso;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SubIncisoCoberturaPolizaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SubIncisoCoberturaPolizaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * SubIncisoCoberturaPolizaDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            SubIncisoCoberturaPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubIncisoCoberturaPolizaDTO entity);

	/**
	 * Delete a persistent SubIncisoCoberturaPolizaDTO entity.
	 * 
	 * @param entity
	 *            SubIncisoCoberturaPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubIncisoCoberturaPolizaDTO entity);

	/**
	 * Persist a previously saved SubIncisoCoberturaPolizaDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * SubIncisoCoberturaPolizaDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            SubIncisoCoberturaPolizaDTO entity to update
	 * @return SubIncisoCoberturaPolizaDTO the persisted
	 *         SubIncisoCoberturaPolizaDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubIncisoCoberturaPolizaDTO update(SubIncisoCoberturaPolizaDTO entity);

	public SubIncisoCoberturaPolizaDTO findById(SubIncisoCoberturaPolizaId id);

	/**
	 * Find all SubIncisoCoberturaPolizaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SubIncisoCoberturaPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubIncisoCoberturaPolizaDTO> found by query
	 */
	public List<SubIncisoCoberturaPolizaDTO> findByProperty(
			String propertyName, Object value);

	/**
	 * Find all SubIncisoCoberturaPolizaDTO entities.
	 * 
	 * @return List<SubIncisoCoberturaPolizaDTO> all SubIncisoCoberturaPolizaDTO
	 *         entities
	 */
	public List<SubIncisoCoberturaPolizaDTO> findAll();

	public void insertSubIncisoCoberturaPolizaPorCotizacion(
			BigDecimal idToCotizacion, BigDecimal idToPoliza);
}