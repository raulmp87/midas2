package mx.com.afirme.midas.poliza.cobertura;

import java.math.BigDecimal;

public class CoberturaSoporteDaniosSiniestroDTO {
    	

	private BigDecimal numeroInciso;
	private BigDecimal idToSeccion;
	private BigDecimal numeroSubInciso;
	private BigDecimal idToCobertura;
	private String nombreComercial;
	private Integer claveTipoSumaAsegurada;
	private BigDecimal sumaAsegurada;
	private BigDecimal sumaAseguradaPrimerRiesgoLUC;
	
	
	public void setNumeroInciso(BigDecimal numeroInciso) {
	    this.numeroInciso = numeroInciso;
	}
	public BigDecimal getNumeroInciso() {
	    return numeroInciso;
	}
	public void setIdToSeccion(BigDecimal idToSeccion) {
	    this.idToSeccion = idToSeccion;
	}
	public BigDecimal getIdToSeccion() {
	    return idToSeccion;
	}
	public void setIdToCobertura(BigDecimal idToCobertura) {
	    this.idToCobertura = idToCobertura;
	}
	public BigDecimal getIdToCobertura() {
	    return idToCobertura;
	}
	public void setClaveTipoSumaAsegurada(Integer claveTipoSumaAsegurada) {
	    this.claveTipoSumaAsegurada = claveTipoSumaAsegurada;
	}
	public Integer getClaveTipoSumaAsegurada() {
	    return claveTipoSumaAsegurada;
	}
	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
	    this.sumaAsegurada = sumaAsegurada;
	}
	public BigDecimal getSumaAsegurada() {
	    return sumaAsegurada;
	}
	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
	public String getNombreComercial() {
		return nombreComercial;
	}
	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
	    this.numeroSubInciso = numeroSubInciso;
	}
	public BigDecimal getNumeroSubInciso() {
	    return numeroSubInciso;
	}
	public BigDecimal getSumaAseguradaPrimerRiesgoLUC() {
		return sumaAseguradaPrimerRiesgoLUC;
	}
	public void setSumaAseguradaPrimerRiesgoLUC(
			BigDecimal sumaAseguradaPrimerRiesgoLUC) {
		this.sumaAseguradaPrimerRiesgoLUC = sumaAseguradaPrimerRiesgoLUC;
	}
	
    

}
