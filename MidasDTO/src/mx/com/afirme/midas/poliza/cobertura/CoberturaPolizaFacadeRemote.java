package mx.com.afirme.midas.poliza.cobertura;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for CoberturaPolizaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface CoberturaPolizaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved CoberturaPolizaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CoberturaPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public CoberturaPolizaDTO save(CoberturaPolizaDTO entity);

	/**
	 * Delete a persistent CoberturaPolizaDTO entity.
	 * 
	 * @param entity
	 *            CoberturaPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoberturaPolizaDTO entity);

	/**
	 * Persist a previously saved CoberturaPolizaDTO entity and return it or a
	 * copy of it to the sender. A copy of the CoberturaPolizaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CoberturaPolizaDTO entity to update
	 * @return CoberturaPolizaDTO the persisted CoberturaPolizaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoberturaPolizaDTO update(CoberturaPolizaDTO entity);

	public CoberturaPolizaDTO findById(CoberturaPolizaId id);

	/**
	 * Find all CoberturaPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoberturaPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoberturaPolizaDTO> found by query
	 */
	public List<CoberturaPolizaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all CoberturaPolizaDTO entities.
	 * 
	 * @return List<CoberturaPolizaDTO> all CoberturaPolizaDTO entities
	 */
	public List<CoberturaPolizaDTO> findAll();
	
	public void insertCoberturaPolizaPorCotizacion(BigDecimal idToCotizacion,BigDecimal idToPoliza);
	
	public Double obtenerPrimaNetaPoliza(BigDecimal idToPoliza);
}