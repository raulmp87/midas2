package mx.com.afirme.midas.usuario;

import java.util.List;
import javax.ejb.Remote;

/**
 * Interfaz Remota para UsuarioDTO
 * 
 * @author Christian Ceballos
 * @since 18 de junio de 2009
 */

public interface UsuarioFacadeRemote {
	
	/**
	 * Metodo para Almacenar una entidad de tipo UsuarioDTO
	 * @param usuario Entidad de tipo UsuarioDTO que se va a registrar
	 */
	public void guardar(UsuarioDTO usuario);

	/**
	 * Metodo para Borrar una determinada entidad de tipo UsuarioDTO
	 * @param usuario Entidad de tipo UsuarioDTO que se va a borrar
	 */
	public void borrar(UsuarioDTO usuario);

	/**
	 * Metodo que actualiza una determinada entidad de tipo UsuarioDTO
	 * @param usuario Entidad de tipo UsuarioDTO que se va a actualizar
	 * @return
	 */
	public UsuarioDTO actualizar(UsuarioDTO usuario);

	
	/**
	 * Metodo que localiza una entidad de tipo UsuarioDTO
	 * por medio de su Identificador
	 * 
	 * @param identificador Identificador de alguna entidad UsuarioDTO
	 * @return
	 */
	public UsuarioDTO buscarPorIdentificador(Long identificador);
	
	/**
	 * Metodo encargado de localizar todas aquellas entidades
	 * de tipo UsuarioDTO que coinciden con un cierto parametro de busqueda
	 * @param propiedad El parametro de busqueda, es decir, el nombre de algun campo
	 * 					 perteneciente a UsuarioDTO
	 * @param valor El valor del parametro de busqueda
	 * @param inicio Indentifica el indice del conjunto de resultados desde el cual
	 * 					se van a obtener los registros
	 * @param cantidad Identifica el numero maximo de registros a obtener
	 * @return Una lista conteniendo los registros de la entidad UsuarioDTO localizados
	 */
	public List<UsuarioDTO> buscarPorPropiedad(String propiedad, Object valor,
			int inicio, int cantidad);
	
	/**
	 * Metodo encargado de localizar todas aquellas entidades
	 * de tipo UsuarioDTO que coinciden con un cierto parametro de busqueda
	 * @param propiedad El parametro de busqueda, es decir, el nombre de algun campo
	 * 					 perteneciente a UsuarioDTO
	 * @param valor El valor del parametro de busqueda
	 * @param inicio Indentifica el indice del conjunto de resultados desde el cual
	 * 					se van a obtener los registros
	 * @return Una lista conteniendo los registros de la entidad UsuarioDTO localizados
	 */
	public List<UsuarioDTO> buscarPorPropiedad(String propiedad, Object valor,
			int inicio);
	
	/**
	 * Metodo encargado de localizar todas aquellas entidades
	 * de tipo UsuarioDTO cuyo campo usuarionombre coincida con el parametro recibido
	 * @param usuarionombre Valor a localizar
	 * @param inicio Indentifica el indice del conjunto de resultados desde el cual
	 * 					se van a obtener los registros
	 * @param cantidad Identifica el numero maximo de registros a obtener
	 * @return Una lista conteniendo los registros de la entidad UsuarioDTO localizados
	 */
	public List<UsuarioDTO> buscarPorNombreUsuario(Object usuarionombre,
			int inicio, int cantidad);
	
	/**
	 * Metodo encargado de localizar todas aquellas entidades
	 * de tipo UsuarioDTO cuyo campo usuarionombre coincida con el parametro recibido
	 * @param usuarionombre Valor a localizar
	 * @param inicio Indentifica el indice del conjunto de resultados desde el cual
	 * 					se van a obtener los registros
	 * @return Una lista conteniendo los registros de la entidad UsuarioDTO localizados
	 */
	public List<UsuarioDTO> buscarPorNombreUsuario(Object usuarionombre,
			int inicio);
	
	
	/**
	 * Metodo encargado de localizar todas aquellas entidades
	 * de tipo UsuarioDTO cuyo campo usuariocontrase�a coincida con el parametro recibido
	 * @param usuariocontrase�a Valor a localizar
	 * @param inicio Indentifica el indice del conjunto de resultados desde el cual
	 * 					se van a obtener los registros
	 * @param cantidad Identifica el numero maximo de registros a obtener
	 * @return Una lista conteniendo los registros de la entidad UsuarioDTO localizados
	 */
	public List<UsuarioDTO> buscarPorContrasena(Object usuariocontrasena,
			int inicio, int cantidad);
	
	/**
	 * Metodo encargado de localizar todas aquellas entidades
	 * de tipo UsuarioDTO cuyo campo usuariocontrase�a coincida con el parametro recibido
	 * @param usuariocontrase�a Valor a localizar
	 * @param inicio Indentifica el indice del conjunto de resultados desde el cual
	 * 					se van a obtener los registros
	 * @return Una lista conteniendo los registros de la entidad UsuarioDTO localizados
	 */
	public List<UsuarioDTO> buscarPorContrasena(Object usuariocontrasena,
			int inicio);
	
	/**
	 * Metodo Encargado de Cargar todos aquellos registros de tipo UsuarioDTO
	 * @param inicio Indentifica el indice del conjunto de resultados desde el cual
	 * 					se van a obtener los registros
	 * @param cantidad Identifica el numero maximo de registros a obtener
	 * @return Una lista conteniendo los registros de la entidad UsuarioDTO cargados
	 */
	public List<UsuarioDTO> buscaTodos(int inicio, int cantidad);
	
	/**
	 * Metodo Encargado de Cargar todos aquellos registros de tipo UsuarioDTO
	 * @param inicio Indentifica el indice del conjunto de resultados desde el cual
	 * 					se van a obtener los registros
	 * @return Una lista conteniendo los registros de la entidad UsuarioDTO cargados
	 */
	public List<UsuarioDTO> buscaTodos(int inicio);
	
	/**
	 * Metodo encargado de validar si los datos de la entidad UsuarioDTO
	 * recibidos como parametro son validos.
	 * Extrae los valores usuarionombre y usuariocontrase�a y los compara
	 * contra los existentes en la base de datos, en caso de que los datos
	 * extraidos coincidan con los de la base de datos, regresa la entidad
	 * localizada con estos valores, en caso contrario regresa un valor nulo
	 * @param TcusuarioDTODTO
	 * @return Una entidad de tipo UsuarioDTO o un valor nulo
	 */
	public UsuarioDTO validarUsuario(UsuarioDTO TcusuarioDTODTO);
		
}