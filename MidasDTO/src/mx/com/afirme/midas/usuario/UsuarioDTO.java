package mx.com.afirme.midas.usuario;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.siniestro.Cabina;

/**
 * Tcusuario entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCUSUARIO", schema = "MIDAS")
public class UsuarioDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 7854644991819791555L;
	private Long usuarioid;
	private String usuarionombre;
	private String usuariocontrasena;
	private Cabina cabina;
	
	// Constructors

	/** default constructor */
	public UsuarioDTO() {
	}

	/** full constructor */
	public UsuarioDTO(Long usuarioid, String usuarionombre,
			String usuariocontrasena) {
		this.usuarioid = usuarioid;
		this.usuarionombre = usuarionombre;
		this.usuariocontrasena = usuariocontrasena;
	}

	// Property accessors
	@Id
	@Column(name = "USUARIOID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getUsuarioid() {
		return this.usuarioid;
	}

	public void setUsuarioid(Long usuarioid) {
		this.usuarioid = usuarioid;
	}

	@Column(name = "USUARIONOMBRE", nullable = false, length = 50)
	public String getUsuarionombre() {
		return this.usuarionombre;
	}

	public void setUsuarionombre(String usuarionombre) {
		this.usuarionombre = usuarionombre;
	}

	@Column(name = "USUARIOCONTRASENA", nullable = false, length = 50)
	public String getUsuariocontrasena() {
		return this.usuariocontrasena;
	}

	public void setUsuariocontrasena(String usuariocontrasena) {
		this.usuariocontrasena = usuariocontrasena;
	}
	
	@ManyToOne
	@JoinColumn(name="CABINA_ID")
	public Cabina getCabina() {
		return cabina;
	}
	
	public void setCabina(Cabina cabina) {
		this.cabina = cabina;
	}

}