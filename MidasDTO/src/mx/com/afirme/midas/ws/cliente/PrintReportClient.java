package mx.com.afirme.midas.ws.cliente;



public interface PrintReportClient {
		
	public void generateDigitalBill();
	
	public byte[] getDigitalBill (String llaveFiscal);
	
	public byte[] getDigitalBill (String llaveFiscal, String formato);
	
	public byte[] getAccountChargeLetter(String quotationId);
	
	public byte[] getAutoInsurancePolicy(String cotizacionID);
	
	public byte[] getHousePolicy(String cotizacionID);
	
	public byte[] getLifeInsurancePolicy(String cotizacionID);
	
	public byte[] getCFDI(String key);
	
	public byte[] getCFDIWithAddenda(String key);
	/**
	 ** Manda a llamar el servicio de Cancelacion de WFactura. Le pasa como parametro la referencia.
	 **/
	public void cancelDigitalBill(int idUsuario);
	/**
	 ** Manda a llamar el servicio de Obtener Acuse de WFactura.
	 **/
	public void cancelarCFDI();	
	
	public abstract void generatePaymentComplementBill();
	
	public void generateChargeComplementBill();
}
