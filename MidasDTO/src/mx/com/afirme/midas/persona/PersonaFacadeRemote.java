package mx.com.afirme.midas.persona;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for PersonaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface PersonaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved PersonaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PersonaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public PersonaDTO save(PersonaDTO entity);
    /**
	 Delete a persistent PersonaDTO entity.
	  @param entity PersonaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PersonaDTO entity);
   /**
	 Persist a previously saved PersonaDTO entity and return it or a copy of it to the sender. 
	 A copy of the PersonaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PersonaDTO entity to update
	 @return PersonaDTO the persisted PersonaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public PersonaDTO update(PersonaDTO entity);
	public PersonaDTO findById( BigDecimal id);
	 /**
	 * Find all PersonaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PersonaDTO property to query
	  @param value the property value to match
	  	  @return List<PersonaDTO> found by query
	 */
	public List<PersonaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all PersonaDTO entities.
	  	  @return List<PersonaDTO> all PersonaDTO entities
	 */
	public List<PersonaDTO> findAll(
		);
	
	/**
	 * Find a PersonaDTO entity related with a specific CotizacionDTO.
	 * CotizacionDTO contains two PersonaDTO records. The found record is indicated by
	 * the param int personaOrden in the next order:
	 * 1 - Persona contratante.
	 * 2 - Persona asegurado.    
	 * @param int personaOrden
	 * @param BigDecimal idToCotizacion
	  	  @return List<PersonaDTO> found by query
	 */
	public PersonaDTO findByIdCotizacion(BigDecimal idToCotizacion,int personaOrden);
	
	/**
	 * Find a PersonaDTO entity related with a specific ProveedorInspeccionDTO.
	 * @param BigDecimal idToProveedorInspeccion
	  	  @return DireccionDTO found by query
	 */
	public PersonaDTO findByIdProveedorInspeccion(BigDecimal idToProveedorInspeccion);
}