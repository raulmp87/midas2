package mx.com.afirme.midas.persona;
// default package

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * Abstra la tabla depersona dentro del esquema midas
 */
@Entity
@Table(name="TOPERSONA"
    ,schema="MIDAS"
)
public class PersonaDTO  implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	//Constantes
	public static final short CLAVE_PERSONA_FISICA = (short) 1;
	public static final short CLAVE_PERSONA_MORAL = (short) 2;
	// Fields    

     private BigDecimal idToPersona;
     private Short claveTipoPersona;
     private String nombre;
     private String apellidoPaterno;
     private String apellidoMaterno;
     private Date fechaNacimiento;
     private String codigoRFC;
     private String telefono;
     private String email;


    // Constructors

    /** default constructor */
    public PersonaDTO() {
    }

	/** minimal constructor */
    public PersonaDTO(BigDecimal idToPersona, Short claveTipoPersona, String nombre) {
        this.idToPersona = idToPersona;
        this.claveTipoPersona = claveTipoPersona;
        this.nombre = nombre;
    }
   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTOPERSONA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOPERSONA_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOPERSONA_SEQ_GENERADOR")
    @Column(name="IDTOPERSONA")
    public BigDecimal getIdToPersona() {
        return this.idToPersona;
    }
    
    public void setIdToPersona(BigDecimal idToPersona) {
        this.idToPersona = idToPersona;
    }
    
    @Column(name="CLAVETIPOPERSONA", nullable=false, precision=4, scale=0)

    public Short getClaveTipoPersona() {
        return this.claveTipoPersona;
    }
    
    public void setClaveTipoPersona(Short claveTipoPersona) {
        this.claveTipoPersona = claveTipoPersona;
    }
    
    @Column(name="NOMBREPERSONA", nullable=false, length=200)

    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Column(name="APELLIDOPATERNO", length=20)

    public String getApellidoPaterno() {
        return this.apellidoPaterno;
    }
    
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }
    
    @Column(name="APELLIDOMATERNO", length=20)

    public String getApellidoMaterno() {
        return this.apellidoMaterno;
    }
    
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHANACIMIENTO", length=7)

    public Date getFechaNacimiento() {
        return this.fechaNacimiento;
    }
    
    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
  
    @Column(name="CODIGORFC", length=20)
    public String getCodigoRFC() {
        return this.codigoRFC;
    }    
    public void setCodigoRFC(String codigoRFC) {
        this.codigoRFC = codigoRFC;
    }

    @Column(name="TELEFONOPERSONA")
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Column(name="EMAILPERSONA")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}