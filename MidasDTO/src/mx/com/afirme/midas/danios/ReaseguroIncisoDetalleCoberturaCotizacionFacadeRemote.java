package mx.com.afirme.midas.danios;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ReaseguroIncisoDetalleCoberturaCotizacionDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ReaseguroIncisoDetalleCoberturaCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * ReaseguroIncisoDetalleCoberturaCotizacionDTO entity. All subsequent
	 * persist actions of this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ReaseguroIncisoDetalleCoberturaCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public ReaseguroIncisoDetalleCoberturaCotizacionDTO save(ReaseguroIncisoDetalleCoberturaCotizacionDTO entity);

	/**
	 * Delete a persistent ReaseguroIncisoDetalleCoberturaCotizacionDTO entity.
	 * 
	 * @param entity
	 *            ReaseguroIncisoDetalleCoberturaCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ReaseguroIncisoDetalleCoberturaCotizacionDTO entity);

	/**
	 * Persist a previously saved ReaseguroIncisoDetalleCoberturaCotizacionDTO
	 * entity and return it or a copy of it to the sender. A copy of the
	 * ReaseguroIncisoDetalleCoberturaCotizacionDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ReaseguroIncisoDetalleCoberturaCotizacionDTO entity to update
	 * @return ReaseguroIncisoDetalleCoberturaCotizacionDTO the persisted
	 *         ReaseguroIncisoDetalleCoberturaCotizacionDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ReaseguroIncisoDetalleCoberturaCotizacionDTO update(
			ReaseguroIncisoDetalleCoberturaCotizacionDTO entity);

	public ReaseguroIncisoDetalleCoberturaCotizacionDTO findById(
			ReaseguroIncisoDetalleCoberturaCotizacionId id);

	/**
	 * Find all ReaseguroIncisoDetalleCoberturaCotizacionDTO entities with a
	 * specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ReaseguroIncisoDetalleCoberturaCotizacionDTO
	 *            property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> found by query
	 */
	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> findByProperty(
			String propertyName, Object value, int... rowStartIdxAndCount);
	/**
	 * Find all ReaseguroIncisoDetalleCoberturaCotizacionDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> all
	 *         ReaseguroIncisoDetalleCoberturaCotizacionDTO entities
	 */
	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> findAll(
			int... rowStartIdxAndCount);
	
	/**
	 * Filter all ReaseguroIncisoDetalleCoberturaCotizacionDTO entities.
	 * 
	 * @param ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO
	 * @return List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> all
	 *         ReaseguroIncisoDetalleCoberturaCotizacionDTO entities
	 */
	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> listarFiltrado(ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO);
	
	/**
	 * Find ReaseguroIncisoDetalleCoberturaCotizacionDTO entities by idCotizacion, idSubRamo and numeroInciso 
	 * 
	 * @param BigDecimal idCotizacion
	 * @param BigDecimal idSubramo
	 * @param BigDecimal numeroInciso
	 * @return List<ReaseguroIncisoDetalleCoberturaCotizacionDTO>
	 */
	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> obtenerDetalleIncisoCoberturasCotizacion(BigDecimal idToCotizacion, BigDecimal idSubramo, BigDecimal numeroInciso);
}