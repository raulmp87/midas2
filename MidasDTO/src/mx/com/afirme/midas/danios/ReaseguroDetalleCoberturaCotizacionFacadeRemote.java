package mx.com.afirme.midas.danios;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ReaseguroDetalleCoberturaCotizacionDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ReaseguroDetalleCoberturaCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * ReaseguroDetalleCoberturaCotizacionDTO entity. All subsequent persist
	 * actions of this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ReaseguroDetalleCoberturaCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public ReaseguroDetalleCoberturaCotizacionDTO save(ReaseguroDetalleCoberturaCotizacionDTO entity);

	/**
	 * Delete a persistent ReaseguroDetalleCoberturaCotizacionDTO entity.
	 * 
	 * @param entity
	 *            ReaseguroDetalleCoberturaCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ReaseguroDetalleCoberturaCotizacionDTO entity);

	/**
	 * Persist a previously saved ReaseguroDetalleCoberturaCotizacionDTO entity
	 * and return it or a copy of it to the sender. A copy of the
	 * ReaseguroDetalleCoberturaCotizacionDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ReaseguroDetalleCoberturaCotizacionDTO entity to update
	 * @return ReaseguroDetalleCoberturaCotizacionDTO the persisted
	 *         ReaseguroDetalleCoberturaCotizacionDTO entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ReaseguroDetalleCoberturaCotizacionDTO update(
			ReaseguroDetalleCoberturaCotizacionDTO entity);

	public ReaseguroDetalleCoberturaCotizacionDTO findById(
			ReaseguroDetalleCoberturaCotizacionId id);

	/**
	 * Find all ReaseguroDetalleCoberturaCotizacionDTO entities with a specific
	 * property value.
	 * 
	 * @param propertyName
	 *            the name of the ReaseguroDetalleCoberturaCotizacionDTO
	 *            property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ReaseguroDetalleCoberturaCotizacionDTO> found by query
	 */
	public List<ReaseguroDetalleCoberturaCotizacionDTO> findByProperty(
			String propertyName, Object value, int... rowStartIdxAndCount);

	/**
	 * Find all ReaseguroDetalleCoberturaCotizacionDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ReaseguroDetalleCoberturaCotizacionDTO> all
	 *         ReaseguroDetalleCoberturaCotizacionDTO entities
	 */
	public List<ReaseguroDetalleCoberturaCotizacionDTO> findAll(
			int... rowStartIdxAndCount);
	
	/**
	 * Filter all ReaseguroDetalleCoberturaCotizacionDTO entities.
	 * 
	 * @param ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO
	 * @return List<ReaseguroDetalleCoberturaCotizacionDTO> all
	 *         ReaseguroDetalleCoberturaCotizacionDTO entities
	 */
	public List<ReaseguroDetalleCoberturaCotizacionDTO> listarFiltrado(ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO);
	
	/**
	 * Find ReaseguroDetalleCoberturaCotizacionDTO entities by idCotizacion and idSubRamo 
	 * 
	 * @param BigDecimal idCotizacion
	 * @param BigDecimal idSubramo
	 * @return List<ReaseguroDetalleCoberturaCotizacionDTO>
	 */
	public List<ReaseguroDetalleCoberturaCotizacionDTO> obtenerDetalleCoberturasCotizacion(BigDecimal idCotizacion, BigDecimal idSubramo);
	
}