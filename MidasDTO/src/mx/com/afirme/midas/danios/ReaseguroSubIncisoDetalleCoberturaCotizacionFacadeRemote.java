package mx.com.afirme.midas.danios;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ReaseguroSubIncisoDetalleCoberturaCotizacionDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ReaseguroSubIncisoDetalleCoberturaCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity. All subsequent
	 * persist actions of this entity should use the #update() method.
	 * 
	 * @param entity
	 *            ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity to
	 *            persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public ReaseguroSubIncisoDetalleCoberturaCotizacionDTO save(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity);

	/**
	 * Delete a persistent ReaseguroSubIncisoDetalleCoberturaCotizacionDTO
	 * entity.
	 * 
	 * @param entity
	 *            ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity to
	 *            delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity);

	/**
	 * Persist a previously saved
	 * ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity and return it or a
	 * copy of it to the sender. A copy of the
	 * ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity to
	 *            update
	 * @return ReaseguroSubIncisoDetalleCoberturaCotizacionDTO the persisted
	 *         ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ReaseguroSubIncisoDetalleCoberturaCotizacionDTO update(
			ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entity);

	public ReaseguroSubIncisoDetalleCoberturaCotizacionDTO findById(
			ReaseguroSubIncisoDetalleCoberturaCotizacionId id);

	/**
	 * Find all ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entities with a
	 * specific property value.
	 * 
	 * @param propertyName
	 *            the name of the
	 *            ReaseguroSubIncisoDetalleCoberturaCotizacionDTO property to
	 *            query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> found by
	 *         query
	 */
	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> findByProperty(
			String propertyName, Object value, int... rowStartIdxAndCount);

	/**
	 * Find all ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> all
	 *         ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entities
	 */
	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> findAll(
			int... rowStartIdxAndCount);
	
	/**
	 * Filter all ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entities.
	 * 
	 * @param ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO
	 * @return List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> all
	 *         ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entities
	 */
	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> listarFiltrado(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO);
	
	/**
	 * Find ReaseguroSubIncisoDetalleCoberturaCotizacionDTO entities by idCotizacion, idSubRamo, numeroInciso, idSeccion, numeroSubinciso
	 * 
	 * @param BigDecimal idCotizacion
	 * @param BigDecimal idSubramo
	 * @param BigDecimal numeroInciso
	 * @param BigDecimal idSeccion
	 * @param BigDecimal numeroSubinciso
	 * @return List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO>
	 */
	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> obtenerDetalleSubIncisosCoberturasCotizacion(BigDecimal idCotizacion, BigDecimal idSubramo,
			BigDecimal numeroInciso, BigDecimal idSeccion, BigDecimal numeroSubinciso);
}