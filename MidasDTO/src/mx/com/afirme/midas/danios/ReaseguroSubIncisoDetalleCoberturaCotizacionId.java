package mx.com.afirme.midas.danios;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ReaseguroSubIncisoDetalleCoberturaCotizacionDTOId entity. @author MyEclipse
 * Persistence Tools
 */
@Embeddable
public class ReaseguroSubIncisoDetalleCoberturaCotizacionId implements
		java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -6470969299424723471L;
	private BigDecimal idToCotizacion;
	private BigDecimal numeroInciso;
	private BigDecimal idToSeccion;
	private BigDecimal numeroSubInciso;
	private BigDecimal idTcSubRamo;
	private BigDecimal idToCobertura;

	// Constructors

	/** default constructor */
	public ReaseguroSubIncisoDetalleCoberturaCotizacionId() {
	}

	/** full constructor */
	public ReaseguroSubIncisoDetalleCoberturaCotizacionId(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToSeccion, BigDecimal numeroSubInciso,
			BigDecimal idTcSubRamo, BigDecimal idToCobertura) {
		this.idToCotizacion = idToCotizacion;
		this.numeroInciso = numeroInciso;
		this.idToSeccion = idToSeccion;
		this.numeroSubInciso = numeroSubInciso;
		this.idTcSubRamo = idTcSubRamo;
		this.idToCobertura = idToCobertura;
	}

	// Property accessors

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "NUMEROSUBINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroSubInciso() {
		return this.numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubRamo() {
		return this.idTcSubRamo;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ReaseguroSubIncisoDetalleCoberturaCotizacionId))
			return false;
		ReaseguroSubIncisoDetalleCoberturaCotizacionId castOther = (ReaseguroSubIncisoDetalleCoberturaCotizacionId) other;

		return ((this.getIdToCotizacion() == castOther.getIdToCotizacion()) || (this
				.getIdToCotizacion() != null
				&& castOther.getIdToCotizacion() != null && this
				.getIdToCotizacion().equals(castOther.getIdToCotizacion())))
				&& ((this.getNumeroInciso() == castOther.getNumeroInciso()) || (this
						.getNumeroInciso() != null
						&& castOther.getNumeroInciso() != null && this
						.getNumeroInciso().equals(castOther.getNumeroInciso())))
				&& ((this.getIdToSeccion() == castOther.getIdToSeccion()) || (this
						.getIdToSeccion() != null
						&& castOther.getIdToSeccion() != null && this
						.getIdToSeccion().equals(castOther.getIdToSeccion())))
				&& ((this.getNumeroSubInciso() == castOther
						.getNumeroSubInciso()) || (this.getNumeroSubInciso() != null
						&& castOther.getNumeroSubInciso() != null && this
						.getNumeroSubInciso().equals(
								castOther.getNumeroSubInciso())))
				&& ((this.getIdTcSubRamo() == castOther.getIdTcSubRamo()) || (this
						.getIdTcSubRamo() != null
						&& castOther.getIdTcSubRamo() != null && this
						.getIdTcSubRamo().equals(castOther.getIdTcSubRamo())))
				&& ((this.getIdToCobertura() == castOther.getIdToCobertura()) || (this
						.getIdToCobertura() != null
						&& castOther.getIdToCobertura() != null && this
						.getIdToCobertura()
						.equals(castOther.getIdToCobertura())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToCotizacion() == null ? 0 : this.getIdToCotizacion()
						.hashCode());
		result = 37
				* result
				+ (getNumeroInciso() == null ? 0 : this.getNumeroInciso()
						.hashCode());
		result = 37
				* result
				+ (getIdToSeccion() == null ? 0 : this.getIdToSeccion()
						.hashCode());
		result = 37
				* result
				+ (getNumeroSubInciso() == null ? 0 : this.getNumeroSubInciso()
						.hashCode());
		result = 37
				* result
				+ (getIdTcSubRamo() == null ? 0 : this.getIdTcSubRamo()
						.hashCode());
		result = 37
				* result
				+ (getIdToCobertura() == null ? 0 : this.getIdToCobertura()
						.hashCode());
		return result;
	}

}