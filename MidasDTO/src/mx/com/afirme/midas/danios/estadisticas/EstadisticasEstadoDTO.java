package mx.com.afirme.midas.danios.estadisticas;
// default package

import java.math.BigDecimal;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * EstadisticasEstado entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOESTADISTICASESTADO", schema = "MIDAS")
public class EstadisticasEstadoDTO implements java.io.Serializable {

    // Fields

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private EstadisticasEstadoId id;
    private BigDecimal idBase1;
    private BigDecimal idBase2;
    private BigDecimal idBase3;
    private BigDecimal idBase4;
    private String valorVariable1;
    private String valorVariable2;
    private String valorVariable3;
    private String valorVariable4;

    // Constructors

    /** default constructor */
    public EstadisticasEstadoDTO() {
    }

    /** minimal constructor */
    public EstadisticasEstadoDTO(EstadisticasEstadoId id, BigDecimal idBase1,
	    BigDecimal idBase2, BigDecimal idBase3, BigDecimal idBase4) {
	this.id = id;
	this.idBase1 = idBase1;
	this.idBase2 = idBase2;
	this.idBase3 = idBase3;
	this.idBase4 = idBase4;
    }

    /** full constructor */
    public EstadisticasEstadoDTO(EstadisticasEstadoId id, BigDecimal idBase1,
	    BigDecimal idBase2, BigDecimal idBase3, BigDecimal idBase4,
	    String valorVariable1, String valorVariable2,
	    String valorVariable3, String valorVariable4) {
	this.id = id;
	this.idBase1 = idBase1;
	this.idBase2 = idBase2;
	this.idBase3 = idBase3;
	this.idBase4 = idBase4;
	this.valorVariable1 = valorVariable1;
	this.valorVariable2 = valorVariable2;
	this.valorVariable3 = valorVariable3;
	this.valorVariable4 = valorVariable4;
    }

    // Property accessors
    @EmbeddedId
    @AttributeOverrides( {
	    @AttributeOverride(name = "idCodigoEstadistica", column = @Column(name = "IDCODIGOESTADISTICA", nullable = false, precision = 22, scale = 0)),
	    @AttributeOverride(name = "idSubCodigoEstadistica", column = @Column(name = "IDSUBCODIGOESTADISTICA", nullable = false, precision = 22, scale = 0)),
	    @AttributeOverride(name = "fechaHora", column = @Column(name = "FECHAHORA", nullable = false, length = 11)) })
    public EstadisticasEstadoId getId() {
	return this.id;
    }

    public void setId(EstadisticasEstadoId id) {
	this.id = id;
    }

    @Column(name = "IDBASE1", nullable = false, precision = 22, scale = 0)
    public BigDecimal getIdBase1() {
	return this.idBase1;
    }

    public void setIdBase1(BigDecimal idBase1) {
	this.idBase1 = idBase1;
    }

    @Column(name = "IDBASE2", nullable = false, precision = 22, scale = 0)
    public BigDecimal getIdBase2() {
	return this.idBase2;
    }

    public void setIdBase2(BigDecimal idBase2) {
	this.idBase2 = idBase2;
    }

    @Column(name = "IDBASE3", nullable = false, precision = 22, scale = 0)
    public BigDecimal getIdBase3() {
	return this.idBase3;
    }

    public void setIdBase3(BigDecimal idBase3) {
	this.idBase3 = idBase3;
    }

    @Column(name = "IDBASE4", nullable = false, precision = 22, scale = 0)
    public BigDecimal getIdBase4() {
	return this.idBase4;
    }

    public void setIdBase4(BigDecimal idBase4) {
	this.idBase4 = idBase4;
    }

    @Column(name = "VALORVARIABLE1", length = 256)
    public String getValorVariable1() {
	return this.valorVariable1;
    }

    public void setValorVariable1(String valorVariable1) {
	this.valorVariable1 = valorVariable1;
    }

    @Column(name = "VALORVARIABLE2", length = 256)
    public String getValorVariable2() {
	return this.valorVariable2;
    }

    public void setValorVariable2(String valorVariable2) {
	this.valorVariable2 = valorVariable2;
    }

    @Column(name = "VALORVARIABLE3", length = 256)
    public String getValorVariable3() {
	return this.valorVariable3;
    }

    public void setValorVariable3(String valorVariable3) {
	this.valorVariable3 = valorVariable3;
    }

    @Column(name = "VALORVARIABLE4", length = 256)
    public String getValorVariable4() {
	return this.valorVariable4;
    }

    public void setValorVariable4(String valorVariable4) {
	this.valorVariable4 = valorVariable4;
    }

}