package mx.com.afirme.midas.danios.estadisticas;
// default package

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * EstadisticasEstadoId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class EstadisticasEstadoId implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    // Fields

    private BigDecimal idCodigoEstadistica;
    private BigDecimal idSubCodigoEstadistica;
    private Date fechaHora;

    // Constructors

    /** default constructor */
    public EstadisticasEstadoId() {
    }

    /** full constructor */
    public EstadisticasEstadoId(BigDecimal idCodigoEstadistica,
	    BigDecimal idSubCodigoEstadistica, Date fechaHora) {
	this.idCodigoEstadistica = idCodigoEstadistica;
	this.idSubCodigoEstadistica = idSubCodigoEstadistica;
	this.fechaHora = fechaHora;
    }

    // Property accessors

    @Column(name = "IDCODIGOESTADISTICA", nullable = false, precision = 22, scale = 0)
    public BigDecimal getIdCodigoEstadistica() {
	return this.idCodigoEstadistica;
    }

    public void setIdCodigoEstadistica(BigDecimal idCodigoEstadistica) {
	this.idCodigoEstadistica = idCodigoEstadistica;
    }

    @Column(name = "IDSUBCODIGOESTADISTICA", nullable = false, precision = 22, scale = 0)
    public BigDecimal getIdSubCodigoEstadistica() {
	return this.idSubCodigoEstadistica;
    }

    public void setIdSubCodigoEstadistica(BigDecimal idSubCodigoEstadistica) {
	this.idSubCodigoEstadistica = idSubCodigoEstadistica;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHAHORA", nullable = false,length=11)
    public Date getFechaHora() {
	return this.fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
	this.fechaHora = fechaHora;
    }

    public boolean equals(Object other) {
	if ((this == other))
	    return true;
	if ((other == null))
	    return false;
	if (!(other instanceof EstadisticasEstadoId))
	    return false;
	EstadisticasEstadoId castOther = (EstadisticasEstadoId) other;

	return ((this.getIdCodigoEstadistica() == castOther
		.getIdCodigoEstadistica()) || (this.getIdCodigoEstadistica() != null
		&& castOther.getIdCodigoEstadistica() != null && this
		.getIdCodigoEstadistica().equals(
			castOther.getIdCodigoEstadistica())))
		&& ((this.getIdSubCodigoEstadistica() == castOther
			.getIdSubCodigoEstadistica()) || (this
			.getIdSubCodigoEstadistica() != null
			&& castOther.getIdSubCodigoEstadistica() != null && this
			.getIdSubCodigoEstadistica().equals(
				castOther.getIdSubCodigoEstadistica())))
		&& ((this.getFechaHora() == castOther.getFechaHora()) || (this
			.getFechaHora() != null
			&& castOther.getFechaHora() != null && this
			.getFechaHora().equals(castOther.getFechaHora())));
    }

    public int hashCode() {
	int result = 17;

	result = 37
		* result
		+ (getIdCodigoEstadistica() == null ? 0 : this
			.getIdCodigoEstadistica().hashCode());
	result = 37
		* result
		+ (getIdSubCodigoEstadistica() == null ? 0 : this
			.getIdSubCodigoEstadistica().hashCode());
	result = 37 * result
		+ (getFechaHora() == null ? 0 : this.getFechaHora().hashCode());
	return result;
    }

}