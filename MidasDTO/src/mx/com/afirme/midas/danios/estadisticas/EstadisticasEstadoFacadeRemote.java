package mx.com.afirme.midas.danios.estadisticas;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;

/**
 * Remote interface for EstadisticasEstadoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface EstadisticasEstadoFacadeRemote {
    /**
     * Perform an initial save of a previously unsaved EstadisticasEstado
     * entity. All subsequent persist actions of this entity should use the
     * #update() method.
     * 
     * @param entity
     *            EstadisticasEstado entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(EstadisticasEstadoDTO entity);

    /**
     * Delete a persistent EstadisticasEstado entity.
     * 
     * @param entity
     *            EstadisticasEstado entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(EstadisticasEstadoDTO entity);

    /**
     * Persist a previously saved EstadisticasEstado entity and return it or a
     * copy of it to the sender. A copy of the EstadisticasEstado entity
     * parameter is returned when the JPA persistence mechanism has not
     * previously been tracking the updated entity.
     * 
     * @param entity
     *            EstadisticasEstado entity to update
     * @return EstadisticasEstado the persisted EstadisticasEstado entity
     *         instance, may not be the same
     * @throws RuntimeException
     *             if the operation fails
     */
    public EstadisticasEstadoDTO update(EstadisticasEstadoDTO entity);

    public EstadisticasEstadoDTO findById(EstadisticasEstadoId id);

    /**
     * Find all EstadisticasEstado entities with a specific property value.
     * 
     * @param propertyName
     *            the name of the EstadisticasEstado property to query
     * @param value
     *            the property value to match
     * @return List<EstadisticasEstado> found by query
     */
    public List<EstadisticasEstadoDTO> findByProperty(String propertyName,
	    Object value);

    /**
     * Find all EstadisticasEstado entities.
     * 
     * @return List<EstadisticasEstado> all EstadisticasEstado entities
     */
    public List<EstadisticasEstadoDTO> findAll();
    
    public  EstadisticasEstadoDTO  buscarParaActualizarPorRetornoEstatus(BigDecimal idToSolicitud, BigDecimal idToCotizacion);
    
    public void registraEstadistica(SolicitudDTO solicitud, SolicitudDTO.ACCION accion);
    
    public void registraEstadistica(CotizacionDTO cotizacion, CotizacionDTO.ACCION accion);
}