package mx.com.afirme.midas.danios;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ReaseguroIncisoDetalleCoberturaCotizacionDTOId entity. @author MyEclipse
 * Persistence Tools
 */
@Embeddable
public class ReaseguroIncisoDetalleCoberturaCotizacionId implements
		java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 6025863243148963322L;
	private BigDecimal idToCotizacion;
	private BigDecimal numeroInciso;
	private BigDecimal idTcSubRamo;
	private BigDecimal idToCobertura;

	// Constructors

	/** default constructor */
	public ReaseguroIncisoDetalleCoberturaCotizacionId() {
	}

	/** full constructor */
	public ReaseguroIncisoDetalleCoberturaCotizacionId(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcSubRamo, BigDecimal idToCobertura) {
		this.idToCotizacion = idToCotizacion;
		this.numeroInciso = numeroInciso;
		this.idTcSubRamo = idTcSubRamo;
		this.idToCobertura = idToCobertura;
	}

	// Property accessors

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubRamo() {
		return this.idTcSubRamo;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ReaseguroIncisoDetalleCoberturaCotizacionId))
			return false;
		ReaseguroIncisoDetalleCoberturaCotizacionId castOther = (ReaseguroIncisoDetalleCoberturaCotizacionId) other;

		return ((this.getIdToCotizacion() == castOther.getIdToCotizacion()) || (this
				.getIdToCotizacion() != null
				&& castOther.getIdToCotizacion() != null && this
				.getIdToCotizacion().equals(castOther.getIdToCotizacion())))
				&& ((this.getNumeroInciso() == castOther.getNumeroInciso()) || (this
						.getNumeroInciso() != null
						&& castOther.getNumeroInciso() != null && this
						.getNumeroInciso().equals(castOther.getNumeroInciso())))
				&& ((this.getIdTcSubRamo() == castOther.getIdTcSubRamo()) || (this
						.getIdTcSubRamo() != null
						&& castOther.getIdTcSubRamo() != null && this
						.getIdTcSubRamo().equals(castOther.getIdTcSubRamo())))
				&& ((this.getIdToCobertura() == castOther.getIdToCobertura()) || (this
						.getIdToCobertura() != null
						&& castOther.getIdToCobertura() != null && this
						.getIdToCobertura()
						.equals(castOther.getIdToCobertura())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToCotizacion() == null ? 0 : this.getIdToCotizacion()
						.hashCode());
		result = 37
				* result
				+ (getNumeroInciso() == null ? 0 : this.getNumeroInciso()
						.hashCode());
		result = 37
				* result
				+ (getIdTcSubRamo() == null ? 0 : this.getIdTcSubRamo()
						.hashCode());
		result = 37
				* result
				+ (getIdToCobertura() == null ? 0 : this.getIdToCobertura()
						.hashCode());
		return result;
	}

}