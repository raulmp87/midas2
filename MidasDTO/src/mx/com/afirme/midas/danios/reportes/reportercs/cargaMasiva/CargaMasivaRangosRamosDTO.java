package mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CNSF_RANGO_RAMO", schema = "MIDAS")
public class CargaMasivaRangosRamosDTO implements java.io.Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	// Fields 
	private BigDecimal idRango;
	private String ramo;
	private String cobertura;
	private String ts;
	private String giro;
	private String calculo;
	private List<CargaMasivaDetalleRanDTO> cargaMasivaDetalleRanDTOs;
	
	@Id
	@Column(name = "ID_RANGO", nullable = false, precision = 22, scale = 0)
	@SequenceGenerator(name = "ID_SEQ_GENERADOR2", allocationSize = 1, sequenceName = "MIDAS.CNSF_RAMOS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQ_GENERADOR2")
	public BigDecimal getIdRango() {
		return idRango;
	}

	public void setIdRango(BigDecimal id) {
		this.idRango = id;
	}
	
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "CNSF_RANGO_DETALLE2", schema = "MIDAS",
			joinColumns = {
				@JoinColumn(name="ID_RANGO",referencedColumnName="ID_RANGO")
			},
			inverseJoinColumns = {
				@JoinColumn(name="ID_LIMITE",referencedColumnName="ID_LIMITE")
			}
	)
	public List<CargaMasivaDetalleRanDTO> getCargaMasivaDetalleRanDTOs() {
		return cargaMasivaDetalleRanDTOs;
	}

	public void setCargaMasivaDetalleRanDTOs(
			List<CargaMasivaDetalleRanDTO> cargaMasivaDetalleRanDTOs) {
		this.cargaMasivaDetalleRanDTOs = cargaMasivaDetalleRanDTOs;
	}
	
	@Column(name = "RAMO", nullable = true, length = 10)
	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	
	@Column(name = "COBERTURA", nullable = true, length = 10)
	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	
	@Column(name = "TIPO_SEGURO", nullable = true, length = 10)
	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}
	
	@Column(name = "GIRO", nullable = true, length = 10)
	public String getGiro() {
		return giro;
	}

	public void setGiro(String giro) {
		this.giro = giro;
	}
	
	@Column(name = "CALCULO", nullable = true, length = 10)
	public String getCalculo() {
		return calculo;
	}

	public void setCalculo(String calculo) {
		this.calculo = calculo;
	}	
	
	 public CargaMasivaRangosRamosDTO(BigDecimal idRango, String ramo,
			String cobertura, String ts, String giro, String calculo,
			List<CargaMasivaDetalleRanDTO> cargaMasivaDetalleRanDTOs) {
		super();
		this.idRango = idRango;
		this.ramo = ramo;
		this.cobertura = cobertura;
		this.ts = ts;
		this.giro = giro;
		this.calculo = calculo;
		this.cargaMasivaDetalleRanDTOs = cargaMasivaDetalleRanDTOs;
	}
	 
	public CargaMasivaRangosRamosDTO(){}

	public CargaMasivaRangosRamosDTO clone()   {
		CargaMasivaRangosRamosDTO clon = new CargaMasivaRangosRamosDTO(this.idRango,this.ramo, this.cobertura, this.ts, this.giro, this.calculo, this.cargaMasivaDetalleRanDTOs);
	    return clon;
	}

	@Override
	public String toString() {
		return "CargaMasivaRangosRamosDTO [idRango=" + idRango + ", ramo="
				+ ramo + ", cobertura=" + cobertura + ", ts=" + ts + ", giro="
				+ giro + ", calculo=" + calculo
				+ ", cargaMasivaDetalleRanDTOs=" + cargaMasivaDetalleRanDTOs.size()
				+ "]";
	}
	
	
}
