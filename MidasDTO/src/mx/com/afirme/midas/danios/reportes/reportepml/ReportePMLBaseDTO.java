package mx.com.afirme.midas.danios.reportes.reportepml;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public abstract class ReportePMLBaseDTO implements Serializable{
	private static final long serialVersionUID = -5054456942207079943L;
	
	private Date fechaCorte;
	private Double tipoCambio;
	
	private Integer idRegistro;
	private String numeroPoliza;
	private Integer numeroRegistro;
	private Date fechaInicio;
	private Date fechaFin;
	private BigDecimal inmValorAsegurable;
	private BigDecimal contValorAsegurable;
	private BigDecimal consecValorAsegurable;
	
	private BigDecimal consecLimiteMaximo;
	private BigDecimal inmDeducible;
	private BigDecimal contDeducible;
	private BigDecimal consecDeducible;
	private BigDecimal inmCoaseguro;
	private BigDecimal contCoaseguro;
	private BigDecimal consecCoaseguro;
	private Integer claveEstado;
	private Integer codigoPostal;
	private BigDecimal longitud;
	private BigDecimal latitud;
	private Integer numeroPisos;
	private BigDecimal valorAsegurable;
	private BigDecimal valorRetenido;
	private BigDecimal prima;
	private BigDecimal cedida;
	private BigDecimal retenida;
	private String moneda;
	private String rsrt;
	private String ofiEmi;
	private String inciso;
	private String zonaAmis;
	private String tipoPoliza;
	private String numCapa;
	private BigDecimal limiteMaximo;
	private BigDecimal coaseguro;
	public Date getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	public Double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public Integer getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(Integer idRegistro) {
		this.idRegistro = idRegistro;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public Integer getNumeroRegistro() {
		return numeroRegistro;
	}
	public void setNumeroRegistro(Integer numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public BigDecimal getInmValorAsegurable() {
		return inmValorAsegurable;
	}
	public void setInmValorAsegurable(BigDecimal inmValorAsegurable) {
		this.inmValorAsegurable = inmValorAsegurable;
	}
	public BigDecimal getContValorAsegurable() {
		return contValorAsegurable;
	}
	public void setContValorAsegurable(BigDecimal contValorAsegurable) {
		this.contValorAsegurable = contValorAsegurable;
	}
	public BigDecimal getConsecValorAsegurable() {
		return consecValorAsegurable;
	}
	public void setConsecValorAsegurable(BigDecimal consecValorAsegurable) {
		this.consecValorAsegurable = consecValorAsegurable;
	}
	public BigDecimal getConsecLimiteMaximo() {
		return consecLimiteMaximo;
	}
	public void setConsecLimiteMaximo(BigDecimal consecLimiteMaximo) {
		this.consecLimiteMaximo = consecLimiteMaximo;
	}
	public BigDecimal getInmDeducible() {
		return inmDeducible;
	}
	public void setInmDeducible(BigDecimal inmDeducible) {
		this.inmDeducible = inmDeducible;
	}
	public BigDecimal getContDeducible() {
		return contDeducible;
	}
	public void setContDeducible(BigDecimal contDeducible) {
		this.contDeducible = contDeducible;
	}
	public BigDecimal getConsecDeducible() {
		return consecDeducible;
	}
	public void setConsecDeducible(BigDecimal consecDeducible) {
		this.consecDeducible = consecDeducible;
	}
	public BigDecimal getInmCoaseguro() {
		return inmCoaseguro;
	}
	public void setInmCoaseguro(BigDecimal inmCoaseguro) {
		this.inmCoaseguro = inmCoaseguro;
	}
	public BigDecimal getContCoaseguro() {
		return contCoaseguro;
	}
	public void setContCoaseguro(BigDecimal contCoaseguro) {
		this.contCoaseguro = contCoaseguro;
	}
	public BigDecimal getConsecCoaseguro() {
		return consecCoaseguro;
	}
	public void setConsecCoaseguro(BigDecimal consecCoaseguro) {
		this.consecCoaseguro = consecCoaseguro;
	}
	public Integer getClaveEstado() {
		return claveEstado;
	}
	public void setClaveEstado(Integer claveEstado) {
		this.claveEstado = claveEstado;
	}
	public Integer getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(Integer codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public BigDecimal getLongitud() {
		return longitud;
	}
	public void setLongitud(BigDecimal longitud) {
		this.longitud = longitud;
	}
	public BigDecimal getLatitud() {
		return latitud;
	}
	public void setLatitud(BigDecimal latitud) {
		this.latitud = latitud;
	}
	public Integer getNumeroPisos() {
		return numeroPisos;
	}
	public void setNumeroPisos(Integer numeroPisos) {
		this.numeroPisos = numeroPisos;
	}
	public BigDecimal getValorAsegurable() {
		return valorAsegurable;
	}
	public void setValorAsegurable(BigDecimal valorAsegurable) {
		this.valorAsegurable = valorAsegurable;
	}
	public BigDecimal getValorRetenido() {
		return valorRetenido;
	}
	public void setValorRetenido(BigDecimal valorRetenido) {
		this.valorRetenido = valorRetenido;
	}
	public BigDecimal getPrima() {
		return prima;
	}
	public void setPrima(BigDecimal prima) {
		this.prima = prima;
	}
	public BigDecimal getCedida() {
		return cedida;
	}
	public void setCedida(BigDecimal cedida) {
		this.cedida = cedida;
	}
	public BigDecimal getRetenida() {
		return retenida;
	}
	public void setRetenida(BigDecimal retenida) {
		this.retenida = retenida;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getRsrt() {
		return rsrt;
	}
	public void setRsrt(String rsrt) {
		this.rsrt = rsrt;
	}
	public String getOfiEmi() {
		return ofiEmi;
	}
	public void setOfiEmi(String ofiEmi) {
		this.ofiEmi = ofiEmi;
	}
	public String getInciso() {
		return inciso;
	}
	public void setInciso(String inciso) {
		this.inciso = inciso;
	}
	public String getZonaAmis() {
		return zonaAmis;
	}
	public void setZonaAmis(String zonaAmis) {
		this.zonaAmis = zonaAmis;
	}
	public String getTipoPoliza() {
		return tipoPoliza;
	}
	public void setTipoPoliza(String tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}
	public String getNumCapa() {
		return numCapa;
	}
	public void setNumCapa(String numCapa) {
		this.numCapa = numCapa;
	}
	public BigDecimal getLimiteMaximo() {
		return limiteMaximo;
	}
	public void setLimiteMaximo(BigDecimal limiteMaximo) {
		this.limiteMaximo = limiteMaximo;
	}
	public BigDecimal getCoaseguro() {
		return coaseguro;
	}
	public void setCoaseguro(BigDecimal coaseguro) {
		this.coaseguro = coaseguro;
	}
	
	
}
