package mx.com.afirme.midas.danios.reportes.reportepmltev;

import java.util.List;

import javax.ejb.Remote;


public interface ReportePMLTEVFacadeRemote {

	@Deprecated
	public List<ReportePMLTEVDTO> obtieneReportePMLTEV (ReportePMLTEVDTO filtroReporte, String nombreUsuario) 
	throws Exception;

	public boolean calcularReportePMLTEV(ReportePMLTEVDTO filtroReporte, String nombreUsuario);

	public List<ReportePMLTEVDTO> obtenerReportePMLTEVIndependientesCreditosHipotecarios(Integer claveTipoReporte,String nombreUsuario)throws Exception;
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVIndependientesGrandesRiesgos(Integer claveTipoReporte,String nombreUsuario)throws Exception;
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVIndependientesRiesgosNormales(Integer claveTipoReporte,String nombreUsuario)throws Exception;
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVSemiAgrupadoCreditosHipotecarios(Integer claveTipoReporte,String nombreUsuario)throws Exception;
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVSemiAgrupadoGrandesRiesgos(Integer claveTipoReporte,String nombreUsuario)throws Exception;
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVSemiAgrupadoRiesgosNormales(Integer claveTipoReporte,String nombreUsuario)throws Exception;
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVTecnicos(Integer claveTipoReporte,String nombreUsuario)throws Exception;
}
