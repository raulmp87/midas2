package mx.com.afirme.midas.danios.reportes.reportebasesemision;

import java.util.List;

import javax.ejb.Remote;


public interface ReporteBasesEmisionFacadeRemote {

	public List<ReporteBasesEmisionDTO> obtieneMovimientosEmision(ReporteBasesEmisionDTO movimientoEmision, String nombreUsuario) 
		throws Exception;
	
	
}
