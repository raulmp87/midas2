package mx.com.afirme.midas.danios.reportes.reportercs.documento;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * DocumentoDigitalSolicitudDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TODOCDIGITALRAN", schema = "MIDAS")
public class DocumentoDigitalRangosDTO implements java.io.Serializable, Entidad {

	private static final long serialVersionUID = 1L;
	private BigDecimal idToDocumentoDigitalSolicitud;
	private BigDecimal idToControlArchivo;
	private ControlArchivoDTO controlArchivo;
	private Date fechaCreacion;
	private String codigoUsuarioCreacion;
	private String nombreUsuarioCreacion;
	private Date fechaModificacion;
	private String codigoUsuarioModificacion; 
	private String nombreUsuarioModificacion;
	private Short esCartaCobertura;
	private byte[] files;
	private String extension;
	private String descripcion;
	private String cveNegocio;
	
	public DocumentoDigitalRangosDTO() {
		// TODO Apéndice de constructor generado automáticamente
	}
	
	/** minimal constructor */
	public DocumentoDigitalRangosDTO(
			BigDecimal idToDocumentoDigitalSolicitud,
			SolicitudDTO solicitudDTO, BigDecimal idToControlArchivo,
			Date fechaCreacion, String codigoUsuarioCreacion,
			String nombreUsuarioCreacion) {
		this.idToDocumentoDigitalSolicitud = idToDocumentoDigitalSolicitud;
		this.idToControlArchivo = idToControlArchivo;
		this.fechaCreacion = new Date(fechaCreacion.getTime());
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.nombreUsuarioCreacion = nombreUsuarioCreacion;
	}

	/** full constructor */
	public DocumentoDigitalRangosDTO(
			BigDecimal idToDocumentoDigitalSolicitud,
			SolicitudDTO solicitudDTO, BigDecimal idToControlArchivo,
			Date fechaCreacion, String codigoUsuarioCreacion,
			String nombreUsuarioCreacion, Date fechaModificacion,
			String codigoUsuarioModificacion, String nombreUsuarioModificacion) {
		this.idToDocumentoDigitalSolicitud = idToDocumentoDigitalSolicitud;
		this.idToControlArchivo = idToControlArchivo;
		this.fechaCreacion = fechaCreacion;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.nombreUsuarioCreacion = nombreUsuarioCreacion;
		this.fechaModificacion = new Date(fechaModificacion.getTime());
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
		this.nombreUsuarioModificacion = nombreUsuarioModificacion;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTODOCDIGITALSOL_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTODOCDIGITALSOL_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTODOCDIGITALSOL_SEQ_GENERADOR")
	@Column(name = "IDTODOCDIGITALRAN", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToDocumentoDigitalSolicitud() {
		return this.idToDocumentoDigitalSolicitud;
	}

	public void setIdToDocumentoDigitalSolicitud(
			BigDecimal idToDocumentoDigitalSolicitud) {
		this.idToDocumentoDigitalSolicitud = idToDocumentoDigitalSolicitud;
	}

	

	@Column(name = "IDTOCONTROLARCHIVO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToControlArchivo() {
		return this.idToControlArchivo;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCONTROLARCHIVO", nullable = false, insertable = false, updatable = false)
	public ControlArchivoDTO getControlArchivo() {
		return controlArchivo;
	}

	public void setControlArchivo(ControlArchivoDTO controlArchivoDTO) {
		this.controlArchivo = controlArchivoDTO;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return new Date(this.fechaCreacion.getTime());
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = new Date(fechaCreacion.getTime());
	}

	@Column(name = "CODIGOUSUARIOCREACION", nullable = false, length = 8)
	public String getCodigoUsuarioCreacion() {
		return this.codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@Column(name = "NOMBREUSUARIOCREACION", nullable = false, length = 200)
	public String getNombreUsuarioCreacion() {
		return this.nombreUsuarioCreacion;
	}

	public void setNombreUsuarioCreacion(String nombreUsuarioCreacion) {
		this.nombreUsuarioCreacion = nombreUsuarioCreacion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAMODIFICACION", length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name = "CODIGOUSUARIOMODIFICACION", length = 8)
	public String getCodigoUsuarioModificacion() {
		return this.codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	@Column(name = "NOMBREUSUARIOMODIFICACION", length = 200)
	public String getNombreUsuarioModificacion() {
		return this.nombreUsuarioModificacion;
	}

	public void setNombreUsuarioModificacion(String nombreUsuarioModificacion) {
		this.nombreUsuarioModificacion = nombreUsuarioModificacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.getIdToDocumentoDigitalSolicitud();
	}

	@Override
	public String getValue() {
		
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

	@Column(name = "ESCARTACOBERTURA")	
	public Short getEsCartaCobertura() {
		if(esCartaCobertura==null){
			esCartaCobertura = 0;
		}
		return esCartaCobertura;
	}

	public void setEsCartaCobertura(Short esCartaCobertura) {
		this.esCartaCobertura = esCartaCobertura;
	}

	@Column(name = "FILES")
	public byte[] getFiles() {
		return files;
	}

	public void setFiles(byte[] files) {
		this.files = files;
	}

	@Column(name = "EXTENSION")
	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	@Column(name = "DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@Column(name = "CVENEGOCIO")
	public String getCveNegocio() {
		return cveNegocio;
	}

	public void setCveNegocio(String cveNegocio) {
		this.cveNegocio = cveNegocio;
	}
}