package mx.com.afirme.midas.danios.reportes.reportepmlhidro;

import java.util.List;

import javax.ejb.Remote;


public interface ReportePMLHidroFacadeRemote {
	
	@Deprecated
	public List<ReportePMLHidroDTO> obtieneReportePMLHidro (ReportePMLHidroDTO filtroReporte, String nombreUsuario) 
	throws Exception;

	public boolean calcularReportePMLHidro(ReportePMLHidroDTO filtroReporte, String nombreUsuario);
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroIndependientes(Integer claveTipoReporte,String nombreUsuario)throws Exception;
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroSemiAgrupadoIncisos(Integer claveTipoReporte,String nombreUsuario)throws Exception;
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroSemiAgrupadoDatosGenerales(Integer claveTipoReporte,String nombreUsuario)throws Exception;
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroSemiAgrupadoDatosFinancieros(Integer claveTipoReporte,String nombreUsuario)throws Exception;
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroTecnicos(Integer claveTipoReporte,String nombreUsuario)throws Exception;
	
	
}
