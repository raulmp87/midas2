package mx.com.afirme.midas.danios.reportes.bean.pago;

import java.io.Serializable;

public class EsquemaPagosDTO extends ReciboDTO implements Serializable{

	private static final long serialVersionUID = -8213345162179954379L;

	private ReciboDTO primerRecibo;
	private ReciboDTO recibosSubSecuentes;
	
	private Integer cantidadRecibos;
	private String descripcionFormaPago;

	public ReciboDTO getPrimerRecibo() {
		return primerRecibo;
	}

	public void setPrimerRecibo(ReciboDTO primerRecibo) {
		this.primerRecibo = primerRecibo;
	}

	public ReciboDTO getRecibosSubSecuentes() {
		return recibosSubSecuentes;
	}

	public void setRecibosSubSecuentes(ReciboDTO recibosSubSecuentes) {
		this.recibosSubSecuentes = recibosSubSecuentes;
	}

	public Integer getCantidadRecibos() {
		return cantidadRecibos;
	}

	public void setCantidadRecibos(Integer cantidadRecibos) {
		this.cantidadRecibos = cantidadRecibos;
	}

	public String getDescripcionFormaPago() {
		return descripcionFormaPago;
	}

	public void setDescripcionFormaPago(String descripcionFormaPago) {
		this.descripcionFormaPago = descripcionFormaPago;
	}
}
