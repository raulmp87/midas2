package mx.com.afirme.midas.danios.reportes.reportemonitoreosol;

import java.util.List;

import javax.ejb.Remote;


public interface ReporteMonitoreoSolicitudesFacadeRemote {

	public List<ReporteMonitoreoSolicitudesDTO> obtieneReporteMonitoreoSolicitudes (ReporteMonitoreoSolicitudesDTO filtroReporte, String nombreUsuario) 
	throws Exception;
	
}
