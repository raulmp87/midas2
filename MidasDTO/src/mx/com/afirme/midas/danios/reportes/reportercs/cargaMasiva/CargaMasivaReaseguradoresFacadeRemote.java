package mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva;

import java.util.List;

import mx.com.afirme.midas.catalogos.reaseguradorcnsf.ReaseguradorCargaCNSF;


public interface CargaMasivaReaseguradoresFacadeRemote {
	
	
	public void save(ReaseguradorCargaCNSF entity);
	
	public void delete(ReaseguradorCargaCNSF entity);
	
	public void agregar( 
			List<ReaseguradorCargaCNSF> direccionesValidas) ;
	
	
	public int delete(
			String anio, String cve_negocio) ;
}