package mx.com.afirme.midas.danios.reportes.reportercs.log;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CNSF_SOLVENCIA_LOG", schema = "MIDAS")
public class SolvenciaLogDTO implements java.io.Serializable {

	private static final long serialVersionUID = -8906665891118977602L;
	@Id
	private BigDecimal id;
	@Column(name = "CORTE", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date corte;
	@Column(name = "RAMO_SUBRAMO", nullable = true)
	private String ramo;
	@Column(name = "ORIGEN", nullable = true)
	private String origen;
	@Column(name = "CLAVEPROCESO", nullable = true)
	private BigDecimal cveProceso;
	@Column(name = "DESCRIPCIONERROR", nullable = true)
	private String error;
		
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	@SequenceGenerator(name = "ID_SEQ_GENERADORESQ", allocationSize = 1, sequenceName = "MIDAS.CNSF_ESQREAS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQ_GENERADORESQ")
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	
	public Date getCorte() {
		return corte;
	}
	public void setCorte(Date corte) {
		this.corte = corte;
	}
	
	public String getRamo() {
		return ramo;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	
	public BigDecimal getCveProceso() {
		return cveProceso;
	}
	public void setCveProceso(BigDecimal cveProceso) {
		this.cveProceso = cveProceso;
	}
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	
}
